#!/bin/bash

#
# fw2 no longer acts as a firewall and IP forwarding is now turned off.
#
# We still keep the internal interface just so that we could get to it
# from the internal network.


#
# Make sure Source Routing is turned off
#
echo "0" > /proc/sys/net/ipv4/conf/all/accept_source_route
echo "0" > /proc/sys/net/ipv4/conf/default/accept_source_route
echo "0" > /proc/sys/net/ipv4/conf/eth0/accept_source_route
echo "0" > /proc/sys/net/ipv4/conf/eth1/accept_source_route
echo "0" > /proc/sys/net/ipv4/conf/lo/accept_source_route

#
# Disable icmp broadcast echo and bad error responses
#
echo "1" > /proc/sys/net/ipv4/icmp_echo_ignore_all

#
# Disable IP forwarding
#
echo "0" > /proc/sys/net/ipv4/ip_forward


#!/bin/bash

#
# Firewall rules for the incoming SMTP relay server in
# the routefree.com DMZ.
#
# This machine has only one interface, but is a bastion
# host.  The rules allow only SSH from inside on port 22
# and port 25 from anywhere.
#

DMZ_IFC="eth0"
DMZ_SUBNET="172.16.0.0/24"
DMZ_BCAST="172.16.0.255"

LAN_GW="172.16.0.99"

IPTABLES="/sbin/iptables"

#
# Kernel has this stuff built in
# /sbin/depmod -a
# /sbin/modprobe ipt_LOG
# /sbin/modprobe ipt_REJECT
# /sbin/modprobe ipt_MASQUERADE

#
# Start by setting default to DROP and flushing all rule sets
#
$IPTABLES -P INPUT DROP
$IPTABLES -P OUTPUT DROP
$IPTABLES -P FORWARD DROP
$IPTABLES -F
$IPTABLES -t nat -P PREROUTING ACCEPT
$IPTABLES -t nat -P OUTPUT ACCEPT
$IPTABLES -t nat -P POSTROUTING ACCEPT
$IPTABLES -t nat -F
$IPTABLES -t mangle -F

#
# IP forwarding not required (only one IFC)
#
echo "0" > /proc/sys/net/ipv4/ip_forward

#
# Make sure that source routing is turned off
#
echo "0" > /proc/sys/net/ipv4/conf/default/accept_source_route

#
# Be very explicit about what kind of connections are allowed
#
$IPTABLES -A INPUT -i $DMZ_IFC -p tcp -m state --state NEW --dport 25 -j ACCEPT
$IPTABLES -A INPUT -i $DMZ_IFC -p tcp -m state --state NEW --dport 22 \
	-s $DMZ_SUBNET -j ACCEPT

#
# Connections on localhost are allowed
#
$IPTABLES -A INPUT -i lo -m state --state NEW -j ACCEPT

#
# Block all other incoming connections
#
$IPTABLES -A INPUT -i $DMZ_IFC -p tcp -m state --state NEW,INVALID -j DROP

#
# Allow outgoing connections on any interface (even 'lo')
#
$IPTABLES -A OUTPUT  -p tcp -m state --state NEW -j ACCEPT

#
# Allow packets for established connections
#
$IPTABLES -A INPUT   -m state --state ESTABLISHED,RELATED -j ACCEPT
$IPTABLES -A OUTPUT  -m state --state ESTABLISHED,RELATED -j ACCEPT

#
# Disallow handling for SMB:  disallow any across the firewall
#
$IPTABLES -A INPUT   -i $DMZ_IFC -p udp --dport 137:138 -j DROP
$IPTABLES -A OUTPUT  -o $DMZ_IFC -p udp --sport 137:138 -j DROP
$IPTABLES -A OUTPUT  -o $DMZ_IFC -p udp --dport 137:138 -j DROP

#
# Allow any other outbound UDP
#
$IPTABLES -A OUTPUT  -p udp -j ACCEPT

#
# Allow all ICMP for now
#
$IPTABLES -A INPUT   -p ICMP -j ACCEPT
$IPTABLES -A OUTPUT  -p ICMP -j ACCEPT

#
# Log anything that falls through the bottom of any of the chains
# since it's about to get dropped
#
$IPTABLES -A INPUT   -j LOG --log-prefix "IPtables INPUT: "
$IPTABLES -A FORWARD -j LOG --log-prefix "IPtables FORWARD: "
$IPTABLES -A OUTPUT  -j LOG --log-prefix "IPtables OUTPUT: "

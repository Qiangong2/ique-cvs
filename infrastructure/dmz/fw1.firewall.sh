#!/bin/bash

#
# IPTABLES initialization for external gateway on routefree.com DMZ
#
# This DMZ contains the external web server and DNS server and
# sits in front of therouter.
#
# In-bound SSH gets forwarded to the inner LAN gateway, as
# does NTP traffic.
#
# Incoming mail gets forwarded to the mail forwarding server
# in the DMZ.
#

WAN_IFC="eth0"
# primary public IP (target of www.broadon.com)
WAN_IP="66.166.204.114"
# public IP alias #1 (target of ns1.broadon.com)
WAN_IP1="66.166.204.117"
DMZ_IFC="eth1"
DMZ_SUBNET="172.16.0.0/24"
DMZ_BCAST="172.16.0.255"

DMZ_WWW="172.16.0.10"
DMZ_DNS="172.16.0.10"
DMZ_FTP="172.16.0.10"
DMZ_SFTP="172.16.0.15"
DMZ_MAIL="172.16.0.12"	  # relay host
DMZ_BUGZ="172.16.0.14"	  # external bugzilla host

LAN_GW="172.16.0.97"

#
# Someone in Taiwan who has been trying to do DNS updates
# to our server for a long time.  It may be an SME box at
# Acer or one of their customers.
#
EVIL="61.30.12.99"

IPTABLES="/sbin/iptables"

#
# Kernel has this stuff built in
# /sbin/depmod -a
# /sbin/modprobe ipt_LOG
# /sbin/modprobe ipt_REJECT
# /sbin/modprobe ipt_MASQUERADE

#
# Start by setting default to DROP and deleting all rule sets
#
$IPTABLES -P INPUT DROP
$IPTABLES -P OUTPUT DROP
$IPTABLES -P FORWARD DROP
$IPTABLES -F
$IPTABLES -X
$IPTABLES -t nat -P PREROUTING ACCEPT
$IPTABLES -t nat -P OUTPUT ACCEPT
$IPTABLES -t nat -P POSTROUTING ACCEPT
$IPTABLES -t nat -F
$IPTABLES -t mangle -F

#
# User defined chains to allow logging of activity.
#
$IPTABLES -N tcp_forward
$IPTABLES -N udp_forward
$IPTABLES -N icmp_input
$IPTABLES -N port_fwd
$IPTABLES -N log_drop
$IPTABLES -N log_accept

#
# User-defined chain for port forwarding, log the transaction and accept
#
$IPTABLES -A port_fwd -j LOG --log-level alert --log-prefix "IPtables ACCEPT PORTFWD: "
$IPTABLES -A port_fwd -j ACCEPT

#
# User-defined chain for logging and dropping things
#
$IPTABLES -A log_drop -j LOG --log-level alert --log-prefix "IPtables DROP EXPLICIT: "
$IPTABLES -A log_drop -j DROP

#
# User-defined chain for logging and accepting things
#
$IPTABLES -A log_accept -j LOG --log-level alert --log-prefix "IPtables ACCEPT EXPLICIT: "
$IPTABLES -A log_accept -j ACCEPT

#
# Make sure Source Routing is turned off
#
echo "0" > /proc/sys/net/ipv4/conf/all/accept_source_route
echo "0" > /proc/sys/net/ipv4/conf/default/accept_source_route
echo "0" > /proc/sys/net/ipv4/conf/eth0/accept_source_route
echo "0" > /proc/sys/net/ipv4/conf/eth1/accept_source_route
echo "0" > /proc/sys/net/ipv4/conf/lo/accept_source_route

#
# Disable icmp broadcast echo and bad error responses
#
echo "1" > /proc/sys/net/ipv4/icmp_echo_ignore_broadcasts
echo "1" > /proc/sys/net/ipv4/icmp_ignore_bogus_error_responses

#
# Enable IP forwarding
#
echo "1" > /proc/sys/net/ipv4/ip_forward

#
# Masquerade outgoing packets
#
$IPTABLES -t nat -A POSTROUTING -o $WAN_IFC -j MASQUERADE

#
# Drop and log all fragments on INPUT and FORWARD
#
$IPTABLES -A INPUT   -i $WAN_IFC -f -j log_drop
$IPTABLES -A FORWARD -i $WAN_IFC -f -j log_drop

#
# Allow packets on established connections early in the rules for efficiency
#
$IPTABLES -A INPUT   -m state --state ESTABLISHED,RELATED -j ACCEPT
$IPTABLES -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT
$IPTABLES -A OUTPUT  -m state --state ESTABLISHED,RELATED -j ACCEPT

#
# Port forwarding rules
#
$IPTABLES -t nat -A PREROUTING -p tcp --dport 21 -i $WAN_IFC \
	-j DNAT --to $DMZ_FTP:21
$IPTABLES -t nat -A PREROUTING -p tcp --dport 25 -i $WAN_IFC \
	-j DNAT --to $DMZ_MAIL:25
$IPTABLES -t nat -A PREROUTING -p udp --dport 6277 -i $WAN_IFC \
	-j DNAT --to $DMZ_MAIL:6277
$IPTABLES -t nat -A PREROUTING -p tcp --dport 80 -i $WAN_IFC \
	-j DNAT --to $DMZ_WWW:80
$IPTABLES -t nat -A PREROUTING -p tcp --dport 53 -i $WAN_IFC \
	-j DNAT --to $DMZ_DNS:53
$IPTABLES -t nat -A PREROUTING -p udp --dport 53 -i $WAN_IFC \
	-j DNAT --to $DMZ_DNS:53
$IPTABLES -t nat -A PREROUTING -p udp --dport 123 -i $WAN_IFC \
	-j DNAT --to $LAN_GW:123
$IPTABLES -t nat -A PREROUTING -p tcp --dport 8384 -i $WAN_IFC \
	-j DNAT --to $LAN_GW:8384
$IPTABLES -t nat -A PREROUTING -p tcp --dport 12047 -i $WAN_IFC \
	-j DNAT --to $DMZ_SFTP:12047
$IPTABLES -t nat -A PREROUTING -p tcp --dport 49049 -i $WAN_IFC \
	-j DNAT --to $DMZ_BUGZ:49049
$IPTABLES -t nat -A PREROUTING -p tcp --dport 49050 -i $WAN_IFC \
	-j DNAT --to $DMZ_BUGZ:49050

#
# Chain for inbound TCP FORWARD traffic (INPUT is dropped)
#
# We are getting DNS update attempts from someone in Taiwan.  Explicitly block them.
#
$IPTABLES -A tcp_forward -p tcp -m state --state NEW -s $EVIL -d $DMZ_DNS --dport 53 -j log_drop
$IPTABLES -A tcp_forward -p tcp -m state --state NEW -d $DMZ_FTP --dport 21 -j port_fwd
$IPTABLES -A tcp_forward -p tcp -m state --state NEW -d $DMZ_MAIL --dport 25 -j port_fwd
$IPTABLES -A tcp_forward -p tcp -m state --state NEW -d $DMZ_WWW --dport 80 -j port_fwd
$IPTABLES -A tcp_forward -p tcp -m state --state NEW -d $DMZ_DNS --dport 53 -j port_fwd
$IPTABLES -A tcp_forward -p tcp -m state --state NEW -d $LAN_GW --dport 8384 -j port_fwd
$IPTABLES -A tcp_forward -p tcp -m state --state NEW -d $DMZ_SFTP --dport 12047 -j port_fwd
$IPTABLES -A tcp_forward -p tcp -m state --state NEW -d $DMZ_BUGZ --dport 49049 -j port_fwd
$IPTABLES -A tcp_forward -p tcp -m state --state NEW -d $DMZ_BUGZ --dport 49050 -j port_fwd
$IPTABLES -A tcp_forward -p tcp --dport 139 -j log_drop
$IPTABLES -A tcp_forward -m state --state NEW,INVALID -j log_drop
$IPTABLES -A tcp_forward -j LOG --log-level alert --log-prefix "IPtables DROP IMPLICIT: "
$IPTABLES -A tcp_forward -j DROP

#
# Chain for inbound UDP FORWARD traffic (INPUT traffic is dropped)
#
$IPTABLES -A udp_forward -p udp -m state --state NEW -s $EVIL -d $DMZ_DNS --dport 53 -j log_drop
$IPTABLES -A udp_forward -p udp -m state --state NEW -d $DMZ_DNS --dport 53 -j port_fwd
$IPTABLES -A udp_forward -p udp -m state --state NEW -d $LAN_GW --dport 123 -j port_fwd
$IPTABLES -A udp_forward -p udp --dport 137:138 -j DROP
$IPTABLES -A udp_forward -m state --state NEW,INVALID -j log_drop
$IPTABLES -A udp_forward -j LOG --log-level alert --log-prefix "IPtables DROP IMPLICIT: "
$IPTABLES -A udp_forward -j DROP

#
# ICMP input chain for both INPUT and FORWARD traffic:
#
# Accept responses to outgoing ICMP requests, but only allow
# the following types on input:
#
# 8 - Echo
#
# Note that the ESTABLISHED,RELATED rule handles echo replies,
# time exceeded (11) from outgoing traceroute, redirect and
# dest unreachable RELATED to outgoing connections.
#
$IPTABLES -A icmp_input -p icmp -m state --state ESTABLISHED,RELATED -j log_accept
$IPTABLES -A icmp_input -p icmp --icmp-type 8 -j ACCEPT
$IPTABLES -A icmp_input -j LOG --log-level alert --log-prefix "IPtables DROP IMPLICIT: "
$IPTABLES -A icmp_input -j DROP

#
# Direct packets to the appropriate chains
#
$IPTABLES -A INPUT   -i $WAN_IFC -p tcp -j log_drop
$IPTABLES -A FORWARD -i $WAN_IFC -p tcp -j tcp_forward
$IPTABLES -A INPUT   -i $WAN_IFC -p udp -j log_drop
$IPTABLES -A FORWARD -i $WAN_IFC -p udp -j udp_forward
$IPTABLES -A INPUT   -i $WAN_IFC -p icmp -j icmp_input
$IPTABLES -A FORWARD -i $WAN_IFC -p icmp -j icmp_input

#
# Allow outgoing connections
#
# Log outgoing connections to port 80 for debugging of the
# errors we are seeing in the logs
#
$IPTABLES -A OUTPUT  -o $WAN_IFC -p tcp -m state --state NEW -j ACCEPT
$IPTABLES -A FORWARD -o $WAN_IFC -p tcp --dport 80 -m state --state NEW -j log_accept
$IPTABLES -A FORWARD -o $WAN_IFC -p tcp -m state --state NEW -j ACCEPT

#
# Allow DNS to DMZ DNS server
#
$IPTABLES -A OUTPUT -o $DMZ_IFC -p tcp --dport 53 -m state --state NEW -j ACCEPT
$IPTABLES -A OUTPUT -o $DMZ_IFC -p udp --dport 53 -j ACCEPT

#
# Allow incoming to some special ports from inside only
#
$IPTABLES -A INPUT  -i $DMZ_IFC -p tcp -s $LAN_GW --dport 22 -m state --state NEW -j log_accept
$IPTABLES -A OUTPUT -o $DMZ_IFC -p tcp -j ACCEPT

#
# Special handling for SMB: disallow any across the firewall either direction.
# Note that the FORWARD case is handled by tcp_forward and udp_forward chains.
#
$IPTABLES -A OUTPUT -o $WAN_IFC -p udp --sport 137:138 -j log_drop
$IPTABLES -A OUTPUT -o $WAN_IFC -p udp --dport 137:138 -j log_drop
$IPTABLES -A OUTPUT -o $WAN_IFC -p tcp --sport 139 -j log_drop
$IPTABLES -A OUTPUT -o $WAN_IFC -p tcp --dport 139 -j log_drop

#
# Allow any other outbound UDP
#
$IPTABLES -A FORWARD -o $WAN_IFC -p udp -j ACCEPT
$IPTABLES -A OUTPUT  -o $WAN_IFC -p udp -j ACCEPT

#
# Allow all outbound ICMP and anything input from the DMZ
#
$IPTABLES -A OUTPUT -p icmp -j ACCEPT
$IPTABLES -A INPUT   -i $DMZ_IFC -s $DMZ_SUBNET -p icmp -j ACCEPT
$IPTABLES -A FORWARD -i $DMZ_IFC -s $DMZ_SUBNET -p icmp -j ACCEPT

#
# Log anything that falls through the bottom of any of the chains
# since it's about to get dropped
#

$IPTABLES -A INPUT   -j LOG --log-level alert --log-prefix "IPtables DROP INPUT: "
$IPTABLES -A FORWARD -j LOG --log-level alert --log-prefix "IPtables DROP FORWARD: "
$IPTABLES -A OUTPUT  -j LOG --log-level alert --log-prefix "IPtables DROP OUTPUT: "

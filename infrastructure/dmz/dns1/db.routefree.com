;
; Master file for external version of routefree.com domain
;

$TTL 3h
$ORIGIN com.
routefree	IN	SOA	ns1.routefree.com. hostmaster.routefree.com. (
	2000056324	; Serial
	3h		; Refresh
	1h		; Retry
	1w		; Expire
	1d )		; Negative cache TTL

	IN	A	66.166.204.114
	IN	MX	0	mail.routefree.com.
;
; Name servers
;
$ORIGIN routefree.com.
	IN	NS	ns1.routefree.com.
	IN	NS	ns1.ikuni.com.
	
ns1	IN	A	66.166.204.117
smtp	IN	CNAME	mail.routefree.com.
mail	IN	A	66.166.204.114
www	IN	CNAME	routefree.com.

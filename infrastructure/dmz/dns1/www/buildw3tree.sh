#!/bin/sh
#
#
# Copy various files from therouter:/home/httpd/html/rf/ directory
#
# This script should only be run on "therouter" since it depends
# on various directories under /home/httpd/html/rf to build this
# staging area for web site on "dns1". Please note that the crontab
# (from david blythe) executes exporttree every 5 minutes to check
# out the following directories from the source tree:
#    - rf/doc
#    - rf/infrastructure
#    - rf/src/pcclient/help
#
# $Revision: 1.12 $
# $Date: 2002/11/26 04:38:53 $
# 

# Copy the top level HTML file

SRCDIR=/home/httpd/html/rf
DSTDIR=/home/cnguyen/stage
ROOT_SRCDIR=$SRCDIR/infrastructure/dmz/dns1/www
DOC_SRCDIR=$SRCDIR/doc
GUIDES_SRCDIR=$SRCDIR/doc/guides
PRODUCTS_SRCDIR=$SRCDIR/doc/release/
RELNOTES_SRCDIR=$SRCDIR/doc/release
WEBHELP_SRCDIR=$SRCDIR/src/pcclient/help

#
# The hierarchy is 
#    $DOCROOT/index.html
#            jobs.html
#            |_ customer/index.html
#            |_ images/*.*
#            |_ support/
#                      |_ features/
#                                 /FileShareNotes.html
#                                 /firewall.html
#                                 /firewall.doc
#                                 /SME2.2-acer.html
#                                 /stat_schema.xml
#                      |_ guides/
#                               /SME_UserGuide.pdf
#                               /HR_UserGuide.pdf
#                               /BackupServerAdmin.html
#                               /StandardOperatingProcedure.html
#                               /TroubleShooting.html
#                               /VPNSetupGuide.html
#                               /WirelessSiteGuide.html
#                               /RMS_AdminGuide.pdf
#                               /KnownProblems.html
#                               /compress_gateway_backups.sh
#                               /pure-ftpd.xinetd
#                               /pureftpd.sh
#                               |- TroubleShooting_files/*
#                      |_ products/
#                                 /SME-Wireless-ProductSheet.doc
#                                 /HR-Exp-ProductSheet.doc
#                                 /MediaExp-ProductSheet.doc
#                                 /RMS-ProductSheet.doc
#                      |_ relnotes/
#                                 /PatchRelnotes.251.html
#                                 /HRReleaseNotes.25.html
#                                 /RMS_ReleaseNotes.25.html
#                                 /PatchRelnotes.242.html
#                                 /PatchRelnotes.241.html
#                                 /HRReleaseNotes.24.html
#                                 /RMS_ReleaseNotes.24.html
#                                 /RMS_ReleaseNotes.233.html
#                                 /RMS_ReleaseNotes.232.html
#                                 /RMS_ReleaseNotes.231.html
#                                 /HRReleaseNotes.23.html
#                                 /RMS_ReleaseNotes.23.html
#                                 /HRReleaseNotes.222.html
#                                 /RMS_ReleaseNotes.222.html
#                                 /RMS_ReleaseNotes.221.html
#                                 /HRReleaseNotes.22.html
#                                 /RMS_ReleaseNotes.22.html
#                                 /PatchRelnotes.212.html
#                                 /HRReleaseNotes.21.html
#                                 /RMS_ReleaseNotes.21.html
#                                 /HRReleaseNotes.html
#                                 /RMS_ReleaseNotes.html
#                      |_ webhelp/SME/*
#                                /HR/*

#
# Clean up staging directories & re-create directories
#
if [ ! -d $DSTDIR ] ; then
	echo creating $DSTDIR
	mkdir $DSTDIR || exit
fi

cd $DSTDIR || exit
# rm -rf customer images support
rm -rf *
mkdir customer images support
mkdir support/features support/products support/relnotes
mkdir -p support/guides/TroubleShooting_files
# mkdir support/webhelp support/webhelp/SME support/webhelp/HR

cp $ROOT_SRCDIR/index.html $DSTDIR/.
cp $ROOT_SRCDIR/jobs.html $DSTDIR/.
cp $ROOT_SRCDIR/customer/* $DSTDIR/customer/.
cp $ROOT_SRCDIR/images/* $DSTDIR/images/.
# cp -R $ROOT_SRCDIR/support/* $DSTDIR/support/.

# Update support/features directory
#
cp $DOC_SRCDIR/guides/2.2/FileShareNotes.html $DSTDIR/support/features/.
cp $DOC_SRCDIR/security/firewall.doc $DSTDIR/support/features/.
cp $DOC_SRCDIR/security/firewall.html $DSTDIR/support/features/.
cp $DOC_SRCDIR/release/2.2/SME2.2-acer.html $DSTDIR/support/features/.
cp $DOC_SRCDIR/hronline/broadon_stats_schema.xml $DSTDIR/support/features/.
cp $DOC_SRCDIR/hronline/ams_2_3.html $DSTDIR/support/features/.


# Update support/guides directory
#
GUIDES_FILES="2.5/UserGuide/SME_UserGuide.pdf \
	2.2/HR/UserGuide/HR_UserGuide.pdf \
	2.5/ServerAdminGuide/RMS_AdminGuide.pdf \
	2.5/StandardOperatingProcedure.html \
	2.5/TroubleShooting.html \
	2.5/VPNSetupGuide.html \
	2.4/WirelessSiteGuide.html \
	2.4/BackupServerAdmin.html \
	2.4/compress_gateway_backups.sh \
	2.4/pureftpd.sh \
	2.4/pure-ftpd.xinetd "

for file in $GUIDES_FILES; do
  cp $GUIDES_SRCDIR/$file $DSTDIR/support/guides/.
done
cp $GUIDES_SRCDIR/2.5/TroubleShooting_files/* $DSTDIR/support/guides/TroubleShooting_files
cp $DOC_SRCDIR/release/2.3/KnownProblems.html $DSTDIR/support/guides/.

# Update support/products directory
#
PRODUCTS_FILES="2.5/ProductDocs/SME-Wireless-ProductSheet.doc \
       2.2/ProductDocs/HR-Exp-ProductSheet.doc \
       2.2/ProductDocs/MediaExp-ProductSheet.doc \
       2.5/ProductDocs/RMS-ProductSheet.doc"

for file in $PRODUCTS_FILES; do
  cp $PRODUCTS_SRCDIR/$file $DSTDIR/support/products/.
done

# Update support/relnotes directory
#
RELNOTES_FILES="2.5/PatchRelnotes.251.html \
       2.5/RMS_ReleaseNotes.25.html \
       2.5/HRReleaseNotes.25.html \
       2.4/RMS_ReleaseNotes.24.html \
       2.4/PatchRelnotes.242.html \
       2.4/PatchRelnotes.241.html \
       2.4/HRReleaseNotes.24.html \
       2.3/RMS_ReleaseNotes.233.html \
       2.3/RMS_ReleaseNotes.232.html \
       2.3/RMS_ReleaseNotes.231.html \
       2.3/RMS_ReleaseNotes.23.html \
       2.3/HRReleaseNotes.23.html \
       2.2/RMS_ReleaseNotes.222.html \
       2.2/HRReleaseNotes.222.html \
       2.2/RMS_ReleaseNotes.221.html \
       2.2/HRReleaseNotes.22.html \
       2.2/RMS_ReleaseNotes.22.html \
       2.0/PatchRelnotes.212.html \
       2.0/HRReleaseNotes.21.html \
       2.0/RMS_ReleaseNotes.21.html \
       2.0/HRReleaseNotes.html \
       2.0/RMS_ReleaseNotes.html"

for file in $RELNOTES_FILES; do
  cp $RELNOTES_SRCDIR/$file $DSTDIR/support/relnotes/.
done

# Update support/webhelp directory
#
# cp -R $WEBHELP_SRCDIR/WebHelp/* $DSTDIR/support/webhelp/SME/.
# cp -R $WEBHELP_SRCDIR/HR/WebHelp/* $DSTDIR/support/webhelp/HR/.

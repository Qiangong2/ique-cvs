;
; Master file for external version of routefree.ws domain
;

$TTL 3h
$ORIGIN ws.
routefree	IN	SOA	ns1.routefree.net. hostmaster.routefree.ws. (
	2000056316	; Serial
	3h		; Refresh
	1h		; Retry
	1w		; Expire
	1d )		; Negative cache TTL

	IN	A	66.166.204.115
	IN	MX	0	smtp.routefree.tv.
;
; Name servers
;
$ORIGIN routefree.ws.
	IN	NS	ns1.routefree.net.
	IN	NS	ns1.ikuni.com.
	
ns1	IN	CNAME	ns1.routefree.net.
www	IN	CNAME	routefree.ws.

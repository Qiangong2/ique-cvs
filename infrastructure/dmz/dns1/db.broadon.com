;
; Master file for external version of broadon.com domain
;

$TTL 3h
$ORIGIN com.
broadon	IN	SOA	ns1.routefree.net. hostmaster.broadon.com. (
	2006081414	; Serial
	3h		; Refresh
	1h		; Retry
	1w		; Expire
	1d )		; Negative cache TTL

	IN	A	66.166.204.114
	IN	MX	0	mail.broadon.com.
;
; Name servers
;
$ORIGIN broadon.com.
	IN	NS	ns1.broadon.com.
	IN	NS	ns1.routefree.net.
	IN	NS	ns1.ikuni.com.
	
ns1	IN	A	66.166.204.117
www	IN	CNAME	broadon.com.
mail	IN	A	66.166.204.114
newmail	IN	A	66.166.204.114
oldmail	IN	A	66.166.204.115
dmz	IN	A	66.166.204.119
chips	IN	A	66.166.204.122

;
; Make sure all our addresses have a forward map
;
h-112	IN	A	66.166.204.112
h-113	IN	A	66.166.204.113
cvdrtr	IN	CNAME	h-113.broadon.com.
h-118	IN	A	66.166.204.118
h-122	IN	A	66.166.204.122
h-123	IN	A	66.166.204.123
h-124	IN	A	66.166.204.124
h-125	IN	A	66.166.204.125
h-126	IN	A	66.166.204.126
h-127	IN	A	66.166.204.127

;
; Covad delegates reverse maps using CNAMEs
; of the following form
;
h-66-166-204-112	IN	PTR	h-112.broadon.com.
h-66-166-204-113	IN	PTR	cvdrtr.broadon.com.
h-66-166-204-114	IN	PTR	www.broadon.com.
h-66-166-204-115	IN	PTR	gw2.routefree.net.
h-66-166-204-116	IN	PTR	isp.routefree.net.
h-66-166-204-117	IN	PTR	ns1.routefree.com.
h-66-166-204-118	IN	PTR	h-118.broadon.com.
h-66-166-204-119	IN	PTR	dmz.broadon.com.
h-66-166-204-120	IN	PTR	ns1.ikuni.com.
h-66-166-204-121	IN	PTR	depot.broadon.com.
h-66-166-204-122	IN	PTR	chips.broadon.com.
h-66-166-204-123	IN	PTR	h-123.broadon.com.
h-66-166-204-124	IN	PTR	h-124.broadon.com.
h-66-166-204-125	IN	PTR	h-125.broadon.com.
h-66-166-204-126	IN	PTR	h-126.broadon.com.
h-66-166-204-127	IN	PTR	h-127.broadon.com.

;
; top-level domain for the entire BroadOn VPN
;

$ORIGIN vpn.broadon.com.

bcc-beta        IN      NS      dns.bcc-beta.vpn.broadon.com.
ems-beta        IN      NS      dns.ems-beta.vpn.broadon.com.
idc-beta        IN      NS      dns.idc-beta.vpn.broadon.com.

dns.bcc-beta    IN      A       172.16.9.22
dns.idc-beta    IN      A       172.16.10.22
dns.ems-beta    IN      A       172.16.11.22

;
; SPF records
;
;broadon.com.		IN	TXT	"v=spf1 ip4=66.166.204.115 a mx -all"
;mail.broadon.com.	IN	TXT	"v=spf1 a -all"

$ORIGIN wii.broadon.com.

s10	43200	IN	A	172.16.127.10
www	43200	IN	CNAME	s10.wii.broadon.com.
oss	43200	IN	CNAME	s10.wii.broadon.com.
bms	43200	IN	CNAME	s10.wii.broadon.com.
cvs	43200	IN	CNAME	s10.wii.broadon.com.
ftp	43200	IN	CNAME	s10.wii.broadon.com.
s11	43200	IN	A	172.16.127.11
bugs	43200	IN	CNAME	s11.wii.broadon.com.

;
; Master file for external version of idc-beta.broadon.com domain
;

$TTL 3h
$ORIGIN broadon.com.
idc-beta	IN	SOA	ns1.broadon.com. hostmaster.broadon.com. (
	2006022216	; Serial
	3h		; Refresh
	1h		; Retry
	1w		; Expire
	1d )		; Negative cache TTL

	IN	A	66.166.204.124
	IN	MX	0	mail.broadon.com.
;
; Name servers
;
$ORIGIN idc-beta.broadon.com.
	IN	NS	ns1.broadon.com.
	IN	NS	ns1.routefree.net.
	IN	NS	ns1.ikuni.com.
	
ns1	IN	CNAME	ns1.broadon.com.

rms             IN      A       66.166.204.124
activate        IN      CNAME   rms.idc-beta.broadon.com.
download        IN      CNAME   rms.idc-beta.broadon.com.
status          IN      CNAME   rms.idc-beta.broadon.com.
update          IN      CNAME   rms.idc-beta.broadon.com.
register        IN      CNAME   rms.idc-beta.broadon.com.
support         IN      CNAME   rms.idc-beta.broadon.com.

ntp             IN      CNAME   rms.idc-beta.broadon.com.

cds	IN	CNAME	rms.idc-beta.broadon.com.
xs	IN	CNAME	rms.idc-beta.broadon.com.
osc	IN	CNAME	rms.idc-beta.broadon.com.
ogs	IN	CNAME	rms.idc-beta.broadon.com.
ccs	IN	CNAME	rms.idc-beta.broadon.com.
ecs	IN	CNAME	rms.idc-beta.broadon.com.

;
; Master file for bcc.broadon.com domain
;

$TTL 1h
$ORIGIN broadon.com.
bcc	IN	SOA	ns1.broadon.com. hostmaster.broadon.com. (
	2000112015	; Serial
	3h		; Refresh
	1h		; Retry
	1w		; Expire
	1d )		; Negative cache TTL

	IN	A	66.166.204.118
	IN	MX	0	mail.broadon.com.
;
; Name servers
;
$ORIGIN bcc.broadon.com.
	IN	NS	ns1.broadon.com.
	IN	NS	ns1.routefree.net.
	IN	NS	ns1.ikuni.com.
	
ns1	IN	CNAME	ns1.broadon.com.

xs		IN	A	66.166.204.118
cds		IN	A	66.166.204.118



rms		IN	A	66.166.204.118
activate	IN	CNAME	rms.bcc.broadon.com.
download	IN	CNAME	rms.bcc.broadon.com.
status		IN	CNAME	rms.bcc.broadon.com.
update		IN	CNAME	rms.bcc.broadon.com.
register	IN	CNAME	rms.bcc.broadon.com.
support		IN	CNAME	rms.bcc.broadon.com.
ntp		IN	CNAME	rms.bcc.broadon.com.

upload		IN	CNAME	chips.broadon.com.


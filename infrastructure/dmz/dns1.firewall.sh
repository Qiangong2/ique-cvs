#!/bin/bash

#
# Firewall rules for the DNS and Web server machine in
# the routefree.com DMZ.
#
# This machine has only one interface, but is a bastion
# host.  The rules allow only DNS and Web traffic and
# maintenance logins from the inside net.
#

DMZ_IFC="eth0"
DMZ_SUBNET="172.16.0.0/24"
DMZ_BCAST="172.16.0.255"

DMZ_WWW="172.16.0.10"
DMZ_DNS="172.16.0.10"

LAN_GW="172.16.0.97"
LAN_GW2="172.16.0.98"

IPTABLES="/sbin/iptables"

#
# Load required helper modules
#
/sbin/depmod -a
/sbin/modprobe ip_conntrack_ftp
/sbin/modprobe ip_nat_ftp

#
# Kernel has this stuff built in
#
# /sbin/modprobe ipt_LOG
# /sbin/modprobe ipt_REJECT
# /sbin/modprobe ipt_MASQUERADE

#
# Start by setting default to DROP and flushing all rule sets
#
$IPTABLES -P INPUT DROP
$IPTABLES -P OUTPUT DROP
$IPTABLES -P FORWARD DROP
$IPTABLES -F
$IPTABLES -X
$IPTABLES -t nat -P PREROUTING ACCEPT
$IPTABLES -t nat -P OUTPUT ACCEPT
$IPTABLES -t nat -P POSTROUTING ACCEPT
$IPTABLES -t nat -F
$IPTABLES -t mangle -F

#
# User defined chains to allow logging of activity.
#
$IPTABLES -N log_drop
$IPTABLES -N log_accept

#
# User-defined chain for logging and dropping things
#
$IPTABLES -A log_drop -j LOG --log-level alert --log-prefix "IPtables DROP EXPLICIT: "
$IPTABLES -A log_drop -j DROP

#
# User-defined chain for logging and accepting things
#
$IPTABLES -A log_accept -j LOG --log-level alert --log-prefix "IPtables ACCEPT EXPLICIT: "
$IPTABLES -A log_accept -j ACCEPT

#
# IP forwarding not required (only one IFC)
#
echo "0" > /proc/sys/net/ipv4/ip_forward

#
# Make sure that source routing is turned off
#
echo "0" > /proc/sys/net/ipv4/conf/default/accept_source_route

#
# Be very explicit about what kind of connections are allowed
#
$IPTABLES -A INPUT -i $DMZ_IFC -p tcp -m state --state NEW --dport 80 -j log_accept
$IPTABLES -A INPUT -i $DMZ_IFC -p tcp -m state --state NEW --dport 443 -j log_accept
$IPTABLES -A INPUT -i $DMZ_IFC -p tcp -m state --state NEW --dport 53 -j log_accept
$IPTABLES -A INPUT -i $DMZ_IFC -p udp -m state --state NEW --dport 53 -j log_accept
$IPTABLES -A INPUT -i $DMZ_IFC -p tcp -m state --state NEW --dport 21 -j log_accept
$IPTABLES -A INPUT -i $DMZ_IFC -p tcp -m state --state NEW --dport 22 -s $LAN_GW -j log_accept
$IPTABLES -A INPUT -i $DMZ_IFC -p tcp -m state --state NEW --dport 22 -s $LAN_GW2 -j log_accept

#
# Connections on localhost are allowed
#
$IPTABLES -A INPUT -i lo -m state --state NEW -j ACCEPT

#
# Block all other incoming connections
#
$IPTABLES -A INPUT   -i $DMZ_IFC -p tcp -m state --state NEW,INVALID -j log_drop

#
# Allow outgoing connections on any interface (even 'lo')
#
$IPTABLES -A OUTPUT  -p tcp -m state --state NEW -j ACCEPT

#
# Allow packets for established connections
#
$IPTABLES -A INPUT   -m state --state ESTABLISHED,RELATED -j ACCEPT
$IPTABLES -A OUTPUT  -m state --state ESTABLISHED,RELATED -j ACCEPT

#
# Disallow handling for SMB:  disallow any across the firewall
#
$IPTABLES -A INPUT   -i $DMZ_IFC -p udp --dport 137:138 -j log_drop
$IPTABLES -A OUTPUT  -o $DMZ_IFC -p udp --sport 137:138 -j log_drop
$IPTABLES -A OUTPUT  -o $DMZ_IFC -p udp --dport 137:138 -j log_drop

#
# Allow any other outbound UDP
#
$IPTABLES -A OUTPUT  -p udp -j ACCEPT

#
# Allow all ICMP for now
#
$IPTABLES -A INPUT   -p ICMP -j ACCEPT
$IPTABLES -A OUTPUT  -p ICMP -j ACCEPT

#
# Log anything that falls through the bottom of any of the chains
# since it's about to get dropped
#
$IPTABLES -A INPUT   -j LOG --log-level alert --log-prefix "DROP IMPLICIT: "
$IPTABLES -A FORWARD -j LOG --log-level alert --log-prefix "DROP IMPLICIT: "
$IPTABLES -A OUTPUT  -j LOG --log-level alert --log-prefix "DROP IMPLICIT: "

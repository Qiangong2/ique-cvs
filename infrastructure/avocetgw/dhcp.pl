#!/usr/bin/perl

while ( <> ) {
	chomp;
	my ($ip, $mac, $name) = split '\s+';
	if ( $name ) {
		print "\thost $name.routefree.com {\n";
		print "\t\thardware ethernet $mac;\n";
		print "\t\tfixed-address $ip;\n";
		print "\t\toption host-name \"$name\";\n";
		print "\t}\n";
	}
}

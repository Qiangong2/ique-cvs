#include <stdio.h>

char buf[512];
char mac[16];
unsigned char ip[4];
char name[512];

unsigned char
hexval(char c)
{
	if (c >= 'A' && c <= 'F') return c - 'A' + 10;
	if (c >= 'a' && c <= 'f') return c - 'a' + 10;
	if (c >= '0' && c <= '9') return c - '0';
	return 0;
}

main(int argc, char **argv)
{
	int i;
	char *p;

	while (gets(buf)) {
		memset(mac, 0, sizeof mac);
		memset(ip, 0, sizeof ip);
		memset(name, 0, sizeof name);
		strncpy(mac, &buf[0], 12);
		for (i = 0, p = &buf[12]; i < 4; i++) {
			ip[i] = hexval(*p++) << 4;
			ip[i] += hexval(*p++);
		}
		strncpy(name, &buf[20], sizeof name);
		printf("%u.%u.%u.%u %c%c:%c%c:%c%c:%c%c:%c%c:%c%c %s\n",
			ip[0], ip[1], ip[2], ip[3],
			mac[0], mac[1], mac[2], mac[3], mac[4], mac[5], mac[6],
			mac[7], mac[8], mac[9], mac[10], mac[11], 
			name);
	}
}

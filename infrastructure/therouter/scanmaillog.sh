#!/bin/bash

#
# check for "Relaying denied" messages in /var/log/maillog
# and send them to postmaster@routefree.com
#

OUTFILE=/tmp/scanmail.out.$$
TMPFILE=/tmp/scanmail.tmp.$$
SAVEFILE=/root/bin/scanmail.last

fgrep "Relaying denied" /var/log/maillog > $OUTFILE

if [ -s $SAVEFILE ]
then
	diff $OUTFILE $SAVEFILE > $TMPFILE 2>&1
	if [ -s $TMPFILE ]
	then
		cp $OUTFILE $SAVEFILE
	else
		rm $OUTFILE
	fi
else
	cp $OUTFILE $SAVEFILE
fi

if [ -s $OUTFILE ]
then
	cat $OUTFILE | mail -s "Relaying Denied" postmaster@routefree.com
	rm $OUTFILE
fi


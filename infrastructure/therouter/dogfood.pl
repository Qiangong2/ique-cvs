#!/usr/bin/perl

#
# Perl script to format status messages about avocetgw
#
$ifconfig = 0;
$psout = 0;
$meminfo = 0;
$lvmout = 0;

while (<>) {

	@fld = split(' ');	

	if ( /IFCONFIG$/ ) {
		printf "Interface packet counts:\n";
		$ifconfig = 1;
		next;
	}
	if ( $ifconfig ) {
		if ( /IFCONFIG_END/ ) {
			$ifconfig = 0;
			next;
		}
		if ( ! /^ / ) {
			$cureth = $fld[0];
			next;
		}
		if ( /RX packets/ ) {
			@subfld = split(':', $fld[1]);
			$cureth_rx = $subfld[1];
			next;
		}
		if ( /TX packets/ ) {
			@subfld = split(':', $fld[1]);
			$cureth_tx = $subfld[1];
			printf "\t%s: RX %d, TX %d\n",
				$cureth, $cureth_rx, $cureth_tx;
			next;
		}
	}
	if ( /PSOUT$/ ) {
		printf "Ps output:\n";
		$psout = 1;
		next;
	}
	if ( $psout ) {
		if ( /PSOUT_END/ ) {
			$psout = 0;
			next;
		}
		printf "\t%s", $_;
	}
	if ( /LVMOUT$/ ) {
		printf "lvdisplay output:\n";
		$lvmout = 1;
		next;
	}
	if ( $lvmout ) {
		if ( /LVMOUT_END/ ) {
			$lvmout = 0;
			next;
		}
		printf "\t%s", $_;
	}
	if ( /IPSECOUT$/ ) {
		printf "IPSEC tunnels:\n";
		$ipsecout = 1;
		next;
	}
	if ( $ipsecout ) {
		if ( /IPSECOUT_END/ ) {
			$ipsecout = 0;
			next;
		}
		printf "\t%s", $_;
	}
	if ( /IPTABLESOUT$/ ) {
		printf "iptables rule for IKE:\n";
		$iptablesout = 1;
		$ikerulemissing = 1;
		next;
	}
	if ( $iptablesout ) {
		if ( /IPTABLESOUT_END/ ) {
			$iptablesout = 0;
			if ( $ikerulemissing ) {
				printf "\tMISSING\n";
			}
			next;
		}
		if ( /spt:500/ ) {
			printf "\t%s", $_;
			$ikerulemissing = 0;
		}
	}
	if ( /MEMINFO$/ ) {
		printf "cat /proc/meminfo:\n";
		$meminfo = 1;
		next;
	}
	if ( $meminfo ) {
		if ( /MEMINFO_END/ ) {
			$meminfo = 0;
			next;
		}
		printf "\t%s", $_;
	}
	if ( /VERSION/ ) {
		@subfld = split('-', $fld[3]);
		$build = $subfld[1];
		$version = $fld[17] . " " . $fld[18];
		printf "avocetgw is running %s build %s\n", $version, $build;
		next;
	}
	if ( /UPTIME/ ) {
		my $rv = "";
		$uptime = $fld[1];
		printf "current uptime %s (%d seconds)\n",
			 pretty_time($uptime), $uptime;
                $curtime = time();
                $rv = localtime($curtime - $uptime);
		printf "last boot %s\n", $rv;
		next;
	}
	if ( /DATE/ ) {
		$mon = $fld[2];
		$day = $fld[3];
		$tod = $fld[4];
		$year = $fld[6];
		printf "current time of day is %s (UTC)\n", $tod; 
		next;
	}
}

open DATE, "/bin/date -u |" or die "date explodes on impact ($!)\n";
while (<DATE>) {
	@fld = split(' ');
	$curmon = $fld[1];
	$curday = $fld[2];
	$curtod = $fld[3];
	$curyear = $fld[5];
}

printf "current time on therouter is %s (UTC)\n", $curtod;

#
# calculate time drift if it's not too far off
#
if ( $year != $curyear or $mon != $curmon or $day != $curday ) {
	printf "date is hopelessly different from time on therouter\n";
} else {
	@fld = split(':', $tod);
	$todsec = $fld[2] + 60 * $fld[1] + 3600 * $fld[0];
	@fld = split(':', $curtod);
	$curtodsec = $fld[2] + 60 * $fld[1] + 3600 * $fld[0];
	printf "Time drift relative to therouter is %s\n",
		pretty_time($curtodsec - $todsec);
}

sub pretty_time() {
    my $sec = shift;
    my $rv = "";

    my $days = int($sec/(60*60*24));
    if ($days >= 1) {
        $rv = sprintf("%d days ", $days);
        $sec -= $days*60*60*24;
    }

    my $hour = int($sec/(60*60));
    $sec -= $hour*60*60;
    my $min = int($sec/(60));
    $sec -= $min*60;
    $rv = sprintf("$rv%02d:%02d:%02d", $hour, $min, $sec);

    return $rv
}

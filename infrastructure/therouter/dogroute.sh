#!/bin/sh

#
# Raise an alarm if dogfood reboots or if the static routes
# are missing
#

TMPFILE=/tmp/dogroute.tmp.$$
OUTFILE=/tmp/dogroute.out.$$
UUT=avocetgw

function die() {
	echo "Routes are missing on avocetgw" > $OUTFILE
	echo "Output of route -n:" >> $OUTFILE
	cat $OUTFILE $TMPFILE | \
		mail -s "avocetgw rebooted" paulm@routefree.com;
	rm $TMPFILE
	rm $OUTFILE
	exit 1;
}

#
# Get routing table and raise an alarm if the static routes
# are missing
#
wget -T 15  -O $TMPFILE \
	"http://$UUT/cgi-bin/tsh?pcmd=route%20-n" > /dev/null 2>&1
if [ ! -s $TMPFILE ]
then
	echo wget route failed | \
		mail -s "dogroute cron job errs" paulm@routefree.com
	exit 1
fi

grep 172.16.0.0 $TMPFILE  > /dev/null 2>&1 || die
grep 172.16.10.0 $TMPFILE > /dev/null 2>&1 || die
grep 10.253.0.0 $TMPFILE  > /dev/null 2>&1 || die
grep 10.254.0.0 $TMPFILE  > /dev/null 2>&1 || die
grep 10.255.0.0 $TMPFILE  > /dev/null 2>&1 || die

rm $TMPFILE

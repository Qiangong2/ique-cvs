#!/usr/bin/perl

#
# Perl script to convert canonical ipchains syslog messages to pretty form with
# ip addresses translated to names where possible
#
# The input records look like this (although date stuff may be missing)
#
# ACCEPT eth1 output tcp 80 216.115.104.253 May 12 21:06:12 (#6)
#
# action ifc direction proto port ip mm dd hhmmss rule#
# 

#
# Read in the /etc/services file and build a hash of the text names
# of various port/protocol combinations
#
open SERVICES, "/etc/services" or die "Can't open /etc/services: $!\n";

while ( <SERVICES> ) {	
	chomp;
	next if ( /^\s*#/ );
	next if ( /^\s*$/ );
	s/\s+/ /g;
	@fld = split(' ');
	$services{ $fld[1] } = $fld[0];
}

#
# Add a few custom entries known not to be in /etc/services
#
$services{ "5190/tcp" } = "aim";
$services{ "6346/tcp" } = "gnutella";
$services{ "6688/tcp" } = "napster";
$services{ "6699/tcp" } = "napster";
$services{ "7777/tcp" } = "napster";
$services{ "8875/tcp" } = "napster";
$services{ "8888/tcp" } = "napster";


#
# Now process the input records, adding names for IP addresses and
# names for ports
#
while (<>) {
	@fld = split(' ');	
	$target = $fld[5];
	open NSLOOK, "/usr/bin/nslookup $target |" or die "nslookup explodes on impact ($!)\n";
	$name = "unknown";
	while (<NSLOOK>) {
		@line = split(' ');
		$name = $line[1] if ($line[0] eq "Name:");
	}
	$svcname = $services{"$fld[4]/$fld[3]"};
	$svcname = "???" if ( ! $svcname );
	printf "%s %s %s %s/%s (%s) %s (%s)\n", $fld[0], $fld[1], $fld[2],
	   $fld[3], $fld[4], $svcname, $target, $name;
}

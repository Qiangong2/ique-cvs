#!/usr/bin/perl

@monthname = (
	"Jan",
	"Feb",
	"Mar",
	"Apr",
	"May",
	"Jun",
	"Jul",
	"Aug",
	"Sep",
	"Oct",
	"Nov",
	"Dec",
);

opendir LISTDIR, "/home/httpd/html/firewall/logs" or die "opendir fails $!\n";

while ( $entry = readdir LISTDIR ) {
	if ( $entry =~ /200[\d]\.[\d]{2}\./ ) {
		$names .= " " . $entry;
	}
}

chdir "/home/httpd/html/firewall/logs" or die "chdir fails $!\n";

print "Content-Type: text/html\n\n";
print "<HTML>\n";
print "<BL>\n";
foreach $entry (reverse sort split ' ', $names) {
	#
	# Check to see if there is an 'avocetgw.raw' file here
	#
	open RAWFILE, "$entry/avocetgw.raw" or next;
	close RAWFILE;
	@prts = split '\.', $entry;
	$showdate = $prts[2]." ".$monthname[$prts[1]-1]." ".$prts[0]." at ".$prts[3]." hours";
	print "<LI>$showdate: <A href=\"$entry/avocetgw.raw\"> raw, </A>";
	print "<A href=\"$entry/avocetgw.nocache\"> without proxy msgs, </A>";
	print "<A href=\"$entry/avocetgw.fw.raw\"> raw firewall, </A>";
	print "<A href=\"$entry/avocetgw.fw.canon\"> processed firewall,</A>";
	print "<A href=\"$entry/avocetgw.fw.unique\"> unique firewall</A>";
	print "</LI>\n";
}
print "</BL>\n";
print "</HTML>\n";

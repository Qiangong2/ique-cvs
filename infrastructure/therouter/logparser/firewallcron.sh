#!/bin/sh

#
# Cron script to extract the ipchains, iptables and avocet log messages 
# from syslog and save them
#

DATE=`date`
TMPFILE1=/tmp/fwlog_work.$$.1
TMPFILE2=/tmp/fwlog_work.$$.2
TMPFILE3=/tmp/fwlog_work.$$.3
TMPFILE4=/tmp/fwlog_work.$$.4
TMPFILE5=/tmp/fwlog_work.$$.5

cd /home/httpd/html/firewall/logs

#
# Insert a log record to mark the end of the batch this run
# will collect
#
logger -t IPCHAINS_digest ipchains log collector ran $DATE

#
# Put together the current and last rotated syslog file
# in case the logs have been rotated since the last
# time this script ran
#
if test -f /var/log/messages.1; then
	cp /var/log/messages.1 $TMPFILE1
fi
cat /var/log/messages >> $TMPFILE1

#
# Extract the last two log markers we created
#
fgrep IPCHAINS_digest $TMPFILE1 | tail -2 > $TMPFILE2

#
# Filter the log files between those two markers,
# first looking for ipchains records, then looking for
# records from avocetgw
#
echo "Packet log" > $TMPFILE3
cat $TMPFILE2 >> $TMPFILE3
./rawlogextract.pl $TMPFILE3 < $TMPFILE1 > $TMPFILE4
echo "avocet" > $TMPFILE3
cat $TMPFILE2 >> $TMPFILE3
./rawlogextract.pl $TMPFILE3 < $TMPFILE1 > $TMPFILE5

#
# Stash the resulting raw ipchains records in a separate
# subdirectory and then run the filtering and rearrangement scripts
# to create the processed data files.
#
LOGDIRNAME=`./makelogdir.pl`
cp $TMPFILE4 $LOGDIRNAME/raw
grep -v "/var/log/messages open No such" < $TMPFILE5 > $LOGDIRNAME/avocetgw.raw
rm $TMPFILE1 $TMPFILE2 $TMPFILE3 $TMPFILE4 $TMPFILE5

cd $LOGDIRNAME

#
# Process firewall logs from therouter
#
../parse.pl < raw | ../filter.sh > canon
../stripdate.pl < canon | sort | uniq > unique
../fancy.pl < unique > fancy

#
# Process Avocetgw messages
#
# Remove proxy cache messages
#
grep -v "expire_cache" avocetgw.raw > avocetgw.nocache
grep "Firewall" avocetgw.nocache > avocetgw.fw.raw
../av_fw_parse.pl < avocetgw.fw.raw > avocetgw.fw.canon
../av_stripdate.pl < avocetgw.fw.canon | sort | uniq > avocetgw.fw.unique


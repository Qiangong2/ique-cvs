#!/usr/bin/perl

#
# Perl script to convert ipchains syslog messages to a canonical form for further processing
#
# Raw ipchains log record looks like this:
#
# May 12 21:06:13 firewall kernel: Packet log: output ACCEPT eth1 PROTO=6 171.66.175.204:1357 216.32.74.64:80 L=60 S=0x00 I=17398 F=0x0000 T=64 SYN (#6) 
#  0   1    2        3       4       5     6    7        8    9     10           11                12          13    14     15      16      17   18   19
#
# The output records are rearranged in this form:
#
# ACCEPT eth1 output tcp 80 216.115.104.253 May 12 21:06:12 (#6)
#
# action ifc direction proto port ip mm dd hhmmss rule#
#
# If the action is REDIRECT, then the format of the input record has one extra field:  the redirect port number is in position 9
# and everything else shifts down by one.  The redirect port number is not that interesting, so we discard it.
#
while (<>) {
	@fld = split(' ');	
	if ( $fld[8] eq "REDIRECT" ) {
		#
		# $fld[9] is the redirect port number, so we just slide the rest of the array left by one position
		#
		for ( $idx = 9; $idx < $#fld; $idx += 1 ) {
			$fld[$idx] = $fld[$idx+1];
		}
	}
	$proto = $fld[10] eq "PROTO=6" ? "tcp" : $fld[10] eq "PROTO=17" ? "udp" : $fld[10] eq "PROTO=1" ? "icmp" : $fld[10] eq "PROTO=2" ? "igmp" : $fld[10];
	$rule = $fld[$#fld];
	if ( $fld[7] eq "input" ) {
		#
		# For an input record, the interesting bits are
		# the source ip address and destination port
		#
		@addrport = split(':', $fld[11]);
		$targetip = $addrport[0];
		@addrport = split(':', $fld[12]);
		$targetport = $addrport[1];
	} else {
		#
		# for output, record destination IP and port
		#
		@addrport = split(':', $fld[12]);
		$targetip = $addrport[0];
		$targetport = $addrport[1];
	}
	printf "%s %s %s %s %s %s %s %s %s %s\n", $fld[8], $fld[9], $fld[7], $proto, $targetport, $targetip, $fld[0], $fld[1], $fld[2], $rule;
}

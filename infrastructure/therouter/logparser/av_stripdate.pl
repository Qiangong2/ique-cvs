#!/usr/bin/perl

#
# Perl script to convert canonical iptables syslog messages by
# stripping the date, so that sort | uniq will
# generate one record per unique rule signature.
#

while (<>) {
	@fld = split(' ');	
	printf "%s %s %s %s %s %s %s %s\n", $fld[0], $fld[1], $fld[2], $fld[3], $fld[4], $fld[5], $fld[6], $fld[7];
}

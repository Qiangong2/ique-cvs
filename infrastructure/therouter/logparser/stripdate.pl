#!/usr/bin/perl

#
# Perl script to convert canonical ipchains syslog messages by
# stripping the date and rule numbers, so that sort | uniq will
# generate one record per unique rule signature.
#
# The input records look like this:
#
# ACCEPT eth1 output tcp 80 216.115.104.253 May 12 21:06:12 (#6)
#
# action ifc direction proto port ip mm dd hhmmss rule#
# 
#
while (<>) {
	@fld = split(' ');	
	printf "%s %s %s %s %s %s %s\n", $fld[0], $fld[1], $fld[2], $fld[3], $fld[4], $fld[5];
}

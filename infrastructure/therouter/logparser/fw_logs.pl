#!/usr/bin/perl

@monthname = (
	"Jan",
	"Feb",
	"Mar",
	"Apr",
	"May",
	"Jun",
	"Jul",
	"Aug",
	"Sep",
	"Oct",
	"Nov",
	"Dec",
);

opendir LISTDIR, "/home/httpd/html/firewall/logs" or die "opendir fails $!\n";

while ( $entry = readdir LISTDIR ) {
	if ( $entry =~ /200[\d]\.[\d]{2}\./ ) {
		$names .= " " . $entry;
	}
}

print "Content-Type: text/html\n\n";
print "<HTML>\n";
print "<BL>\n";
foreach $entry (reverse sort split ' ', $names) {
	@prts = split '\.', $entry;
	$showdate = $prts[2]." ".$monthname[$prts[1]-1]." ".$prts[0]." at ".$prts[3]." hours";
	print "<LI><A href=\"$entry/fancy\">", $showdate, "</A><A href=\"$entry/canon\"> (raw)</A></LI>\n";
}
print "</BL>\n";
print "</HTML>\n";

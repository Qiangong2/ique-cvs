#!/usr/bin/perl

#
# Create a directory to hold the files for a particular
# run of the ipchains log parsing tools
#
# Root is /home/httpd/html/firewall/logs
#
# Directory names are YYmmmDD.hhmmss
#

%monthnum = (
	"Jan" => "01",
	"Feb" => "02",
	"Mar" => "03",
	"Apr" => "04",
	"May" => "05",
	"Jun" => "06",
	"Jul" => "07",
	"Aug" => "08",
	"Sep" => "09",
	"Oct" => "10",
	"Nov" => "11",
	"Dec" => "12",
);

$rootdir = "/home/httpd/html/firewall/logs";

chdir $rootdir || die "Unable to find starting directory $rootdir\n";

$loops = 0;
while ( $loops < 20 ) {

	$datestr = localtime ( time() );

	@str = split ' ', $datestr;

	if (length($str[2]) < 2) {
		$day = "0".$str[2];
	} else {
		$day = $str[2];
	}
	$dirname = $str[4].".".$monthnum{$str[1]}.".".$day;
	$datestr = $str[3];
	@str = split ':', $datestr;
	$dirname .= ".".$str[0].$str[1].$str[2];

	#
	# Try to create the directory
	#
	if ( mkdir $dirname, 0755 ) {
		print $dirname;
		exit 0;
	}

	# Wait for the time to change
	sleep(1);
	$loops += 1;
}

print STDERR "Looped $loops times with no success\n";
print "NULL";
exit 1;

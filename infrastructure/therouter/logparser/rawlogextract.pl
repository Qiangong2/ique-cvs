#!/usr/bin/perl

#
# The first argument is a file containing either two
# or three lines.  The first line gives the pattern
# that identifies an interesting line of the log file.
#
# If there are three, then the second and third represent
# the patterns for the start and end of the region of
# the syslog file to filter for interesting records.
#
# If two, then this is the first time the program
# has been run and we process the whole file up to
# the line that matches the last line of the argument
# file.
#

while (<>) {
 	$searchpat = $_ if ( $. == 1 );
 	$startpat = $_ if ( $. == 2 );
	$endpat = $_ if ( $. == 3 );
}

if ( $. == 2 ) {
	$endpat = $startpat;
	$startfound = 1;
} else {
	$startfound = 0;
}

chop $searchpat;

#
# Now we're ready to process STDIN
#

$total = 0;
$match = 0;

while (<>) {
	$total += 1;
	if ( $startfound == 0 ) {
		$startfound = 1 if ( /$startpat/ );
	}
	if ( /$endpat/ ) {
		print STDERR "Total lines = $total\n";
		print STDERR "Matched lines = $match\n";
		exit;
	}
	#
	# We're in the zone, look for the search string
	#
	if ( $startfound ) {
		if ( /$searchpat/ ) {
			$match += 1;
			print $_;
		}
	}
}

#!/usr/bin/perl

#
# Perl script to convert iptables syslog messages to a canonical form for further processing
#
# Raw iptables log record looks like this:
#
# Jul  4 13:01:06 avocetgw klogd: FirewallDrop:IN=eth2 OUT= MAC=00:50:c2:0e:69:aa:00:00:c5:88:31:94:08:00 SRC=171.66.179.35
#  0   1    2        3       4            5              6                  7                                     8
#
# DST=66.166.204.115 LEN=48 TOS=0x00 PREC=0x00 TTL=116 ID=41710 DF PROTO=TCP SPT=1170 DPT=23 WINDOW=8192 RES=0x00 SYN URGP=0 
#         9            10      11        12       13      14    15    16        17      18       19         20     21  22
#
# The output records are rearranged in this form:
#
# DROP   eth1 tcp   216.115.104.253  1156  209.21.47.227   80   May 12 21:06:12
#
# action ifc proto     source ip     sport      dest ip   dport  mm dd hhmmss
#

while (<>) {
	@fld = split(' ');

	$mm = $fld[0];
	$dd = $fld[1];
	$time = $fld[2];

	foreach $token (@fld) {

		if ( $token =~ /^Firewall/ ) {
			@scratch = split(':', $token);
			$in_ifc = $scratch[1];
		}
		elsif ( $token =~ /^OUT=/ ) {
			$out_ifc = $token;
		}
		elsif ( $token =~ /^PROTO=/ ) {
			$proto = $token;
		}
		elsif ( $token =~ /^SRC=/ ) {
			$source = $token;
		}
		elsif ( $token =~ /^SPT=/ ) {
			$sport = $token;
		}
		elsif ( $token =~ /^DST=/ ) {
			$dest = $token;
		}
		elsif ( $token =~ /^DPT=/ ) {
			$dport = $token;
		}

	}

	printf "DROP %s %s %s %s %s %s %s %s %s %s %s %s %s\n", $in_ifc, $out_ifc, $proto, $source, $sport, $dest,
		$dport, $mm, $dd, $time;
}

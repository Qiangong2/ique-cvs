#!/bin/sh

#
# Use sed to filter the canonical ipchains output for
# interesting records by deleting uninteresting ones.
#

sed -e '/ACCEPT eth1/d' \
-e '/ACCEPT wvlan0/d' \
-e '/wvlan0 input udp/d' \
-e '/udp 53/d' \
-e '/output icmp/d' \
-e '/lo input udp/d' \
$1

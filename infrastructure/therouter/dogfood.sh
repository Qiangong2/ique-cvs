#!/bin/sh

#
# Extract information from the dogfood machine
#

TMPFILE=/tmp/dogfood.tmp.$$
OUTFILE=/tmp/dogfood.out.$$
ERRFILE=/tmp/dogfood.err.$$
UUT=avocetgw

wget -T 15  -O $TMPFILE \
	"http://$UUT/cgi-bin/tsh?pcmd=cat%20/proc/version" > /dev/null 2>&1
if [ -s $TMPFILE ]
then
	echo -n "VERSION " >> $OUTFILE
	grep "^Linux" $TMPFILE >> $OUTFILE
else
	echo wget /proc/version failed >> $ERRFILE
fi

wget -T 15  -O $TMPFILE \
	"http://$UUT/cgi-bin/tsh?pcmd=cat%20/proc/uptime" > /dev/null 2>&1
if [ -s $TMPFILE ]
then
	echo -n "UPTIME " >> $OUTFILE
	cat $TMPFILE >> $OUTFILE
else
	echo wget uptime failed >> $ERRFILE
fi

wget -T 15  -O $TMPFILE \
	"http://$UUT/cgi-bin/tsh?pcmd=ifconfig" > /dev/null 2>&1
if [ -s $TMPFILE ]
then
	echo "IFCONFIG" >> $OUTFILE
	cat $TMPFILE >> $OUTFILE
	echo "IFCONFIG_END" >> $OUTFILE
else
	echo wget ifconfig failed >> $ERRFILE
fi

wget -T 15  -O $TMPFILE \
	"http://$UUT/cgi-bin/tsh?pcmd=ps" > /dev/null 2>&1
if [ -s $TMPFILE ]
then
	echo "PSOUT" >> $OUTFILE
	cat $TMPFILE >> $OUTFILE
	echo "PSOUT_END" >> $OUTFILE
else
	echo wget date failed >> $ERRFILE
fi

wget -T 15  -O $TMPFILE \
	"http://$UUT/cgi-bin/tsh?pcmd=cat%20/proc/meminfo" > /dev/null 2>&1
if [ -s $TMPFILE ]
then
	echo "MEMINFO" >> $OUTFILE
	cat $TMPFILE >> $OUTFILE
	echo "MEMINFO_END" >> $OUTFILE
else
	echo wget uptime failed >> $ERRFILE
fi

wget -T 15 -O $TMPFILE \
	"http://$UUT/cgi-bin/tsh?pcmd=/d1/utils/lvm/lvdisplay%20/dev/vg0/snap" > /dev/null 2>&1
if [ -s $TMPFILE ]
then
    echo "LVMOUT" >> $OUTFILE
    cat $TMPFILE >> $OUTFILE
    wget -T 15 -O $TMPFILE "http://$UUT/cgi-bin/tsh?pcmd=df%20-h%20/incoming" > /dev/null 2>&1
    cat $TMPFILE >> $OUTFILE
    echo "LVMOUT_END" >> $OUTFILE
else
    echo wget lvdisplay failed >> $ERRFILE
fi    

wget -T 15 -O $TMPFILE \
	"http://$UUT/cgi-bin/tsh?pcmd=/usr/sbin/whack%20--status" > /dev/null 2>&1
if [ -s $TMPFILE ]
then
    echo "IPSECOUT" >> $OUTFILE
    cat $TMPFILE >> $OUTFILE
    echo "IPSECOUT_END" >> $OUTFILE
else
    echo wget whack failed >> $ERRFILE
fi    

wget -T 15 -O $TMPFILE \
	"http://$UUT/cgi-bin/tsh?pcmd=/sbin/iptables%20-vnL" > /dev/null 2>&1
if [ -s $TMPFILE ]
then
    echo "IPTABLESOUT" >> $OUTFILE
    cat $TMPFILE >> $OUTFILE
    echo "IPTABLESOUT_END" >> $OUTFILE
else
    echo wget iptables failed >> $ERRFILE
fi    

wget -T 15  -O $TMPFILE \
	"http://$UUT/cgi-bin/tsh?pcmd=date" > /dev/null 2>&1
if [ -s $TMPFILE ]
then
	echo -n "DATE " >> $OUTFILE
	cat $TMPFILE >> $OUTFILE
else
	echo wget date failed >> $ERRFILE
fi

if [ -s $ERRFILE ]
then
	cat $ERRFILE | mail -s "dogfood cron job errs" paulm@routefree.com
	rm $ERRFILE
fi

/root/bin/dogfood.pl < $OUTFILE | mail -s "avocetgw status" testresults@routefree.com

rm $TMPFILE
rm $OUTFILE

#!/bin/bash

#
# IPTABLES initialization for new replacement for therouter and
# euler.
#

DMZ1_IFC="eth0"
DMZ1_IP="172.16.0.97"
DMZ1_SUBNET="172.16.0.0/24"
DMZ1_BCAST="172.16.0.255"
DMZ1_RELAY="172.16.0.12"

DMZ2_IFC="eth1"

LAN_IFC=eth2
LAN_IP="10.0.0.6"
LAN_ALL="10.0.0.0/16"
NEWMAIL="10.0.0.15"
MAIL_RF="10.0.0.10"
MAIL_BO="10.0.0.9"

IPTABLES="/sbin/iptables"

#
# Kernel has this stuff built in
# /sbin/depmod -a
# /sbin/modprobe ipt_LOG
# /sbin/modprobe ipt_REJECT
# /sbin/modprobe ipt_MASQUERADE

#
# Start by setting default to DROP and deleting all rule sets
#
$IPTABLES -P INPUT DROP
$IPTABLES -P OUTPUT DROP
$IPTABLES -P FORWARD DROP
$IPTABLES -F
$IPTABLES -X
$IPTABLES -t nat -P PREROUTING ACCEPT
$IPTABLES -t nat -P OUTPUT ACCEPT
$IPTABLES -t nat -P POSTROUTING ACCEPT
$IPTABLES -t nat -F
$IPTABLES -t mangle -F

#
# User defined chains to allow logging of activity.
#
$IPTABLES -N tcp_forward
$IPTABLES -N udp_forward
$IPTABLES -N icmp_input
$IPTABLES -N port_fwd
$IPTABLES -N log_drop
$IPTABLES -N log_reject
$IPTABLES -N log_accept

#
# User-defined chain for port forwarding, log the transaction and accept
#
$IPTABLES -A port_fwd -j LOG --log-level alert --log-prefix "IPtables ACCEPT PORTFWD: "
$IPTABLES -A port_fwd -j ACCEPT

#
# User-defined chain for logging and dropping things
#
$IPTABLES -A log_drop -j LOG --log-level alert --log-prefix "IPtables DROP EXPLICIT: "
$IPTABLES -A log_drop -j DROP

#
# User-defined chain for logging and rejecting things
#
$IPTABLES -A log_reject -j LOG --log-level alert --log-prefix "IPtables REJECT EXPLICIT: "
$IPTABLES -A log_reject -j REJECT

#
# User-defined chain for logging and accepting things
#
$IPTABLES -A log_accept -j LOG --log-level alert --log-prefix "IPtables ACCEPT EXPLICIT: "
$IPTABLES -A log_accept -j ACCEPT

#
# Make sure Source Routing is turned off
#
echo "0" > /proc/sys/net/ipv4/conf/all/accept_source_route
echo "0" > /proc/sys/net/ipv4/conf/default/accept_source_route
echo "0" > /proc/sys/net/ipv4/conf/eth0/accept_source_route
#echo "0" > /proc/sys/net/ipv4/conf/eth1/accept_source_route
echo "0" > /proc/sys/net/ipv4/conf/eth2/accept_source_route
echo "0" > /proc/sys/net/ipv4/conf/lo/accept_source_route

#
# Disable icmp broadcast echo and bad error responses
#
echo "1" > /proc/sys/net/ipv4/icmp_echo_ignore_broadcasts
echo "1" > /proc/sys/net/ipv4/icmp_ignore_bogus_error_responses

#
# Enable IP forwarding
#
echo "1" > /proc/sys/net/ipv4/ip_forward

#
# Masquerade outgoing packets
#
$IPTABLES -t nat -A POSTROUTING -o $DMZ1_IFC -j MASQUERADE
$IPTABLES -t nat -A POSTROUTING -o $LAN_IFC -s ! $LAN_ALL -j MASQUERADE

#
# Drop and log all fragments on INPUT and FORWARD
#
$IPTABLES -A INPUT   -i $DMZ1_IFC -f -j log_drop
$IPTABLES -A FORWARD -i $DMZ1_IFC -f -j log_drop

#
# Allow packets on established connections early in the rules for efficiency
#
$IPTABLES -A INPUT   -m state --state ESTABLISHED,RELATED -j ACCEPT
$IPTABLES -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT
$IPTABLES -A OUTPUT  -m state --state ESTABLISHED,RELATED -j ACCEPT

#
# Allow any IN/OUT on localhost
#
$IPTABLES -A INPUT   -i lo -j ACCEPT
$IPTABLES -A OUTPUT  -o lo -j ACCEPT

#
# Port forwarding rules
#
$IPTABLES -t nat -A PREROUTING -p tcp -d $DMZ1_IP --dport 8384 -i $DMZ1_IFC -j DNAT --to $NEWMAIL:443

#
# Chain for inbound TCP FORWARD traffic (INPUT is dropped)
#
$IPTABLES -A tcp_forward -p tcp -m state --state NEW -s $DMZ1_RELAY -d $NEWMAIL --dport 25 -j log_accept
$IPTABLES -A tcp_forward -p tcp -m state --state NEW -s $DMZ1_RELAY -d $MAIL_RF --dport 25 -j log_accept
$IPTABLES -A tcp_forward -p tcp -m state --state NEW -s $DMZ1_RELAY -d $MAIL_BO --dport 25 -j log_accept
$IPTABLES -A tcp_forward -p tcp -m state --state NEW -d $NEWMAIL --dport 443 -j log_accept
$IPTABLES -A tcp_forward -p tcp --dport 139 -j log_drop
$IPTABLES -A tcp_forward -m state --state NEW,INVALID -j log_drop
$IPTABLES -A tcp_forward -j LOG --log-level alert --log-prefix "IPtables DROP IMPLICIT: "
$IPTABLES -A tcp_forward -j DROP

#
# Chain for inbound UDP FORWARD traffic (INPUT traffic is dropped)
#
$IPTABLES -A udp_forward -p udp --dport 137:138 -j DROP
$IPTABLES -A udp_forward -m state --state NEW,INVALID -j log_drop
$IPTABLES -A udp_forward -j LOG --log-level alert --log-prefix "IPtables DROP IMPLICIT: "
$IPTABLES -A udp_forward -j DROP

#
# ICMP input chain for both INPUT and FORWARD traffic:
#
# Accept responses to outgoing ICMP requests, but only allow
# the following types on input:
#
# 8 - Echo
#
# Note that the ESTABLISHED,RELATED rule handles echo replies,
# time exceeded (11) from outgoing traceroute, redirect and
# dest unreachable RELATED to outgoing connections.
#
$IPTABLES -A icmp_input -p icmp -m state --state ESTABLISHED,RELATED -j log_accept
$IPTABLES -A icmp_input -p icmp --icmp-type 8 -j ACCEPT
$IPTABLES -A icmp_input -j LOG --log-level alert --log-prefix "IPtables DROP IMPLICIT: "
$IPTABLES -A icmp_input -j DROP

#
# Direct packets to the appropriate chains
#
$IPTABLES -A INPUT   -i $DMZ1_IFC -s $DMZ1_SUBNET -p tcp --dport 113 -j log_reject
$IPTABLES -A INPUT   -i $DMZ1_IFC -p tcp -j log_drop
$IPTABLES -A FORWARD -i $DMZ1_IFC -p tcp -j tcp_forward
$IPTABLES -A INPUT   -i $DMZ1_IFC -p udp -j log_drop
$IPTABLES -A FORWARD -i $DMZ1_IFC -p udp -j udp_forward
$IPTABLES -A INPUT   -i $DMZ1_IFC -p icmp -j icmp_input
$IPTABLES -A FORWARD -i $DMZ1_IFC -p icmp -j icmp_input

#
# Allow outgoing connections
#
# Log outgoing connections to port 80 for debugging of the
# errors we are seeing in the logs
#
$IPTABLES -A OUTPUT  -o $DMZ1_IFC -p tcp -m state --state NEW -j ACCEPT
$IPTABLES -A FORWARD -o $DMZ1_IFC -p tcp --dport 80 -m state --state NEW -j log_accept
$IPTABLES -A FORWARD -o $DMZ1_IFC -p tcp -m state --state NEW -j ACCEPT

#
# Allow anything from/to the LAN
#
$IPTABLES -A INPUT   -i $LAN_IFC -p tcp -m state --state NEW -j ACCEPT
$IPTABLES -A FORWARD -i $LAN_IFC -p tcp -m state --state NEW -j ACCEPT
$IPTABLES -A OUTPUT  -o $LAN_IFC -p tcp -m state --state NEW -j ACCEPT
$IPTABLES -A INPUT   -i $LAN_IFC -p udp -j ACCEPT
$IPTABLES -A FORWARD -i $LAN_IFC -p udp -j ACCEPT
$IPTABLES -A OUTPUT  -o $LAN_IFC -p udp -j ACCEPT

#
# Special handling for SMB: disallow any across the firewall either direction.
# Note that the FORWARD case is handled by tcp_forward and udp_forward chains.
#
$IPTABLES -A OUTPUT -o $DMZ1_IFC -p udp --sport 137:138 -j log_drop
$IPTABLES -A OUTPUT -o $DMZ1_IFC -p udp --dport 137:138 -j log_drop
$IPTABLES -A OUTPUT -o $DMZ1_IFC -p tcp --sport 139 -j log_drop
$IPTABLES -A OUTPUT -o $DMZ1_IFC -p tcp --dport 139 -j log_drop

#
# Allow any other outbound UDP
#
$IPTABLES -A FORWARD -o $DMZ1_IFC -p udp -j ACCEPT
$IPTABLES -A OUTPUT  -o $DMZ1_IFC -p udp -j ACCEPT

#
# Allow all outbound ICMP and anything input from the DMZ
#
$IPTABLES -A OUTPUT -p icmp -j ACCEPT
$IPTABLES -A INPUT   -i $LAN_IFC -p icmp -j ACCEPT
$IPTABLES -A FORWARD -i $LAN_IFC -p icmp -j ACCEPT

#
# If there is a reply packet from the LAN that is supposed to be
# handled by another gateway, but somehow landed here (because this is the
# default gateway), we need to pass it along so that and ICMP redirect could
# be generated.
$IPTABLES -A FORWARD -o $LAN_IFC -i $LAN_IFC -j ACCEPT

#
# Log anything that falls through the bottom of any of the chains
# since it's about to get dropped
#

$IPTABLES -A INPUT   -j LOG --log-level alert --log-prefix "IPtables DROP INPUT: "
$IPTABLES -A FORWARD -j LOG --log-level alert --log-prefix "IPtables DROP FORWARD: "
$IPTABLES -A OUTPUT  -j LOG --log-level alert --log-prefix "IPtables DROP OUTPUT: "

#!/bin/bash
#
# dhcpd           This shell script takes care of starting and stopping
#                 dhcpd (dynamic boot parameter server).
#
# chkconfig: 345 55 45
# description: dhcpd dynamically assigned network \
# parameters to clients at boot time.
# probe: true

# Source function library.
. /etc/rc.d/init.d/functions

# Source networking configuration.
. /etc/sysconfig/network

# Check that networking is up.
[ "${NETWORKING}" = "no" ] && exit 0

#[ -f /etc/sysconfig/named ] && . /etc/sysconfig/named

[ -f /usr/sbin/dhcpd ] || exit 0

# set the chroot directory
chrootdir="/"

[ -f /etc/dhcpd.conf ] || exit 0

RETVAL=0
prog="dhcpd"

start() {
        # Start daemons.
        echo "Starting $prog: "
        daemon $prog -q eth2
        RETVAL=$?
        [ $RETVAL -eq 0 ] && touch /var/lock/subsys/dhcpd
        echo
        return $RETVAL
}
stop() {
        # Stop daemons.
        echo "Stopping $prog: "
        pid=`pidofproc $prog`
        kill -TERM $pid
        RETVAL=$?
        [ $RETVAL -eq 0 ] && success "dhcpd shutdown" || failure "dhcpd shutdown"
        [ $RETVAL -eq 0 ] && rm -f $chrootdir/var/run/dhcpd.pid
        [ $RETVAL -eq 0 ] && rm -f /var/lock/subsys/dhcpd
        echo
        return $RETVAL
}
status() {
        status dhcpd
}
restart() {
        stop
        start
}
reload() {
        stop
        start
}

# See how we were called.
case "$1" in
        start)
                start
                ;;
        stop)
                stop
                ;;
        status)
                status
                ;;
        restart)
                restart
                ;;
        condrestart)
                [ -f /var/lock/subsys/dhcpd ] && restart
                ;;
        reload)
                reload
                ;;
        *)
                echo "Usage: $0 {start|stop|status|restart|condrestart|reload}\n"
                exit 1
esac

exit $?

;BIND DUMP V8
$ORIGIN broadon.com.
@	43200	IN	NS	ns1int.routefree.com.
@	43200	IN	SOA	ns1.broadon.com.	hostmaster.therouter.routefree.com. (
			2006081414 ; serial
			3600 ; refresh
			900 ; retry
			1209600 ; expire
			43200 ; default_ttl
			)

	43200	IN	MX	0	mail.broadon.com.

www	43200	IN	A	66.166.204.114
chips	43200	IN	A	66.166.204.122
ns1	43200	IN	A	10.0.0.6
mail	43200	IN	CNAME	newmail.broadon.com.
mail-bo	43200	IN	A	10.0.0.9
mail-rf	43200	IN	A	10.0.0.10
newmail	43200	IN	A	10.0.0.15
filestore	43200	IN	CNAME	filestore.routefree.com.
relay	43200	IN	A	172.16.0.12
relaytest	43200	IN	A	10.0.0.44
lab1-upload	43200	IN	A	172.16.0.22
intwww        43200   IN      CNAME   intwww.routefree.com.

$ORIGIN vpn.broadon.com.

bcc-beta        IN      NS      dns.bcc-beta.vpn.broadon.com.
ems-beta        IN      NS      dns.ems-beta.vpn.broadon.com.
idc-beta        IN      NS      dns.idc-beta.vpn.broadon.com.

dns.bcc-beta    IN      A       172.16.9.22
dns.idc-beta    IN      A       172.16.10.22
dns.ems-beta    IN      A       172.16.11.22

$ORIGIN wii.broadon.com.
s10	43200	IN	A	172.16.127.10
www	43200	IN	CNAME	s10.wii.broadon.com.
ftp	43200	IN	CNAME	s10.wii.broadon.com.
oss	43200	IN	CNAME	oss.lab2.routefree.com.
bms	43200	IN	CNAME	bms.lab2.routefree.com.
s11	43200	IN	A	172.16.127.11
bugs	43200	IN	CNAME	s11.wii.broadon.com.

ecs	43200	IN	A	10.12.3.69

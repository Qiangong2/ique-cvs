;BIND DUMP V8
$ORIGIN com.
routefree	43200	IN	SOA	ns1int.routefree.com. hostmaster.therouter.routefree.com. (
		2006051114 3600 900 1209600 43200 )

	43200	IN	A	10.0.0.6
	43200	IN	NS	ns1int.routefree.com.
	43200	IN	MX	0	mail.routefree.com.

$ORIGIN routefree.com.

;static hosts

therouter IN	CNAME	intwww.routefree.com.
tdev	IN	A	10.0.0.2
intwww	IN	CNAME	bigbelly.routefree.com.
puma	IN	A	10.0.0.3
ap1	IN	A	10.0.0.4
bigbelly IN	A	10.0.0.5
filestore IN	CNAME	bigbelly.routefree.com.
intdns	IN	A	10.0.0.6
ns1int	IN	A	10.0.0.6
gw2dmz	IN	CNAME	intdns.routefree.com.
printer	IN	A	10.0.0.7
panther	IN	A	10.0.0.8
mail-bo	IN	A	10.0.0.9
mail	IN	A	10.0.0.10
smtp	IN	CNAME	mail.routefree.com.
mail-rf	IN	CNAME	mail.routefree.com.
newmail	IN	CNAME	mail.routefree.com.
imap	IN	CNAME	mail.broadon.com.
pop	IN	CNAME	mail.broadon.com.
gw2ncl	IN	A	10.0.0.12
bigbird	IN	A	10.0.0.13
gw2beta	IN	A	10.0.0.17
pserver	IN	A	10.0.0.18
hawk	IN	A	10.0.0.19
ntp	IN	CNAME	hawk.routefree.com.
source	IN	CNAME	hawk.routefree.com.
avocetgw	IN	A	10.0.0.20
foolio	IN	A	10.0.0.21
foolio1	IN	A	10.0.0.22
elitest	IN	A	10.0.0.23
diag	IN	A	10.0.0.24
diag1	IN	A	10.0.0.25
dolphin1	IN	A	10.0.0.26
jchang	IN	A	10.0.0.28
gw2lab2	IN	A	10.0.0.29
osprey	IN	A	10.0.0.30
controller	IN	A	10.0.0.30
antix	IN	A	10.0.0.31
o2	IN	A	10.0.0.32
ss1	IN	A	10.0.0.33
ss1	IN	TXT	"Netgear FS726T Smart Switch"
ss2	IN	A	10.0.0.34
ss2	IN	TXT	"Netgear FS726T Smart Switch"
cheddarpdc	IN	A	10.0.0.35
cheddar	IN	A	10.0.0.35
masterpc	IN	A	10.0.0.36
necmaster	IN	A	10.0.0.36
ddh1	IN	A	10.0.0.37
falcon	IN	A	10.0.0.38
ttyserver	IN	A	10.0.0.40
builder	IN	A	10.0.0.41
beaver	IN	A	10.0.0.42
gw2lab1	IN	CNAME	beaver.routefree.com.
w2kserver	IN	A	10.0.0.43
relaytest	IN	A	10.0.0.44
mailtest	IN	A	10.0.0.45

; logic analyzer
la		IN	A	10.0.0.46

; relay host within the beta net dmz
euler	IN	A	172.16.10.99

fw2	IN	A	172.16.0.7
dns1	IN	A	172.16.0.10
wario	IN	A	172.16.0.15
fw1	IN	A	172.16.0.1
relay	IN	A	172.16.0.12
ns1	IN	A	66.166.204.117
www	IN	A	66.166.204.114

lab1gw	IN	A	10.40.0.2
nclgw	IN	A	172.16.32.1

;
; old names for machines that are now moved to lab1.routefree.com. sub-domain.
;

lab1-xs		0	IN	CNAME	xs.bbu.lab1.routefree.com.
lab1-signer	0	IN	CNAME	signer.bcc.lab1.routefree.com.
lab1-db1	0	IN	CNAME	db1.bcc.lab1.routefree.com.
lab1-db2	0	IN	CNAME	db2.bcc.lab1.routefree.com.
lab1-rmscds	0	IN	CNAME	root.bcc.lab1.routefree.com.
lab1-depot1	0	IN	CNAME	depot1.lab1.routefree.com.

;
; old names for machines that are now moved to beta.broadon.com. sub-domain.
;

test-db1	0	IN	CNAME	db1.beta.broadon.com.

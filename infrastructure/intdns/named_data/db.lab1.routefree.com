;BIND DUMP V8
$ORIGIN routefree.com.
$TTL	7200
lab1	43200	IN	SOA	ns1int.routefree.com. 	hostmaster.therouter.routefree.com. (
			2005110209 ; serial
			3600 ; refresh
			900 ; retry
			1209600 ; expire
			43200 ; default_ttl
			)
lab1	43200	IN	NS	ns1int.routefree.com.

;; ----------------------------------------------------------------------

$ORIGIN lab1.routefree.com.
@		IN	NS	ns1int.routefree.com.

;
; gateway machine
;
gw	43200	IN	A	10.40.0.1

;
; sub domains
;

bcc	43200	IN	NS	dns.bcc.lab1.routefree.com.
bbu	43200	IN	NS	dns.bbu.lab1.routefree.com.
ems	43200	IN	NS	dns.ems.lab1.routefree.com.

dns.bcc		IN	A	10.40.1.22
dns.bbu		IN	A	10.40.2.22
dns.ems		IN	A	10.40.3.22

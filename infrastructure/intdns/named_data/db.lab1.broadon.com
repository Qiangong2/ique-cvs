;BIND DUMP V8
$ORIGIN broadon.com.
lab1	43200	IN	SOA	ns1.broadon.broadon.com.	hostmaster.therouter.routefree.com. (
			2000057012 ; serial
			3600 ; refresh
			900 ; retry
			1209600 ; expire
			43200 ; default_ttl
			)
$ORIGIN lab1.broadon.com.
@		IN	NS	ns1.lab1.broadon.com.
ns1	43200	IN	A	10.0.0.6
root	43200	IN	A	10.0.0.36
rms	43200	IN	CNAME	root.lab1.broadon.com.
;
; Standard service names
;
status	43200	IN	CNAME	root.lab1.broadon.com.
update	43200	IN	CNAME	root.lab1.broadon.com.
register	43200	IN	CNAME	root.lab1.broadon.com.
download	43200	IN	CNAME	root.lab1.broadon.com.
activate	43200	IN	CNAME	root.lab1.broadon.com.
support	43200	IN	CNAME	root.lab1.broadon.com.
time	43200	IN	A	10.0.0.1

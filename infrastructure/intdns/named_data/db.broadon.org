;BIND DUMP V8
$ORIGIN broadon.org.
@	43200	IN	NS	ns1int.routefree.com.
@	43200	IN	SOA	ns1.broadon.org.	hostmaster.therouter.routefree.com. (
			2000057025 ; serial
			3600 ; refresh
			900 ; retry
			1209600 ; expire
			43200 ; default_ttl
			)

	43200	IN	MX	0	relay.broadon.com.

ns1	43200	IN	A	10.0.0.6
www	43200	IN	CNAME	www.routefree.com.
newmail	43200	IN	A	10.0.0.9

;
; Master file for ems.broadon.com domain
;

$TTL 1h
$ORIGIN broadon.com.
ems	IN	SOA	ns1.broadon.com. hostmaster.broadon.com. (
	2000081821	; Serial
	3h		; Refresh
	1h		; Retry
	1w		; Expire
	1d )		; Negative cache TTL

	IN	A	66.166.204.125
	IN	MX	0	mail.broadon.com.
;
; Name servers
;
$ORIGIN ems.broadon.com.
	IN	NS	ns1.broadon.com.
	IN	NS	ns1.routefree.net.
	IN	NS	ns1.ikuni.com.
	
ns1	IN	CNAME	ns1.broadon.com.

ems	IN	A	66.166.204.125
cds	IN	A	66.166.204.125


; redirect all RMS traffic to prod.broadon.com.

rms		IN	CNAME	rms.prod.broadon.com.
activate	IN	CNAME	rms.prod.broadon.com.
download	IN	CNAME	rms.prod.broadon.com.
status		IN	CNAME	rms.prod.broadon.com.
update		IN	CNAME	rms.prod.broadon.com.
register	IN	CNAME	rms.prod.broadon.com.
support		IN	CNAME	rms.prod.broadon.com.

ntp		IN	CNAME	rms.prod.broadon.com.


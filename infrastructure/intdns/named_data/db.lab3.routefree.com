$ORIGIN .
$TTL 86400	; 1 day
lab3.routefree.com	IN SOA	dns.lab3.routefree.com. hostmaster.broadon.com. (
				2006082116 ; serial
				10800      ; refresh (3 hours)
				3600       ; retry (1 hour)
				604800     ; expire (1 week)
				86400      ; minimum (1 day)
				)
			NS	dns.lab3.routefree.com.
			MX	5 mail.lab3.routefree.com.
$ORIGIN lab3.routefree.com.
bms			A	10.40.6.32
ccs			A	10.40.6.25
cps			A	10.40.6.31
db			CNAME	db1
db-vip			A	10.40.6.141
db1			A	10.40.6.144
db1-vip			A	10.40.6.142
db2			A	10.40.6.145
db2-vip			A	10.40.6.143
dns			A	10.40.6.22
ecs			A	10.40.6.27
ets			A	10.40.6.26
gw			A	10.40.6.1
ias			A	10.40.6.29
lb			A	10.40.6.24
mail			CNAME	mail.broadon.com.
ntp			CNAME	ntp.routefree.com.
nus			A	10.40.6.35
ogs			A	10.40.6.30
oss			A	10.40.6.33
pcis			A	10.40.6.34
pas			A	10.40.6.28
server100		A	10.40.6.100
server101		A	10.40.6.101
server102		A	10.40.6.102
server103		A	10.40.6.103
server104		A	10.40.6.104
server105		A	10.40.6.105
server106		A	10.40.6.106
server107		A	10.40.6.107
server108		A	10.40.6.108
server109		A	10.40.6.109
server110		A	10.40.6.110
server111		A	10.40.6.111
server112		A	10.40.6.112
server113		A	10.40.6.113
server114		A	10.40.6.114
server115		A	10.40.6.115
server116		A	10.40.6.116
server117		A	10.40.6.117
server118		A	10.40.6.118
server119		A	10.40.6.119
server120		A	10.40.6.120
server121		A	10.40.6.121
server122		A	10.40.6.122
server123		A	10.40.6.123
server124		A	10.40.6.124
server125		A	10.40.6.125
server126		A	10.40.6.126
server192		A	10.40.6.192
server193		A	10.40.6.193
server194		A	10.40.6.194
server195		A	10.40.6.195
server196		A	10.40.6.196
server197		A	10.40.6.197
server198		A	10.40.6.198
server199		A	10.40.6.199
server200		A	10.40.6.200
server201		A	10.40.6.201
server202		A	10.40.6.202
server203		A	10.40.6.203
server204		A	10.40.6.204
server205		A	10.40.6.205
server206		A	10.40.6.206
server207		A	10.40.6.207
server208		A	10.40.6.208
server209		A	10.40.6.209
server210		A	10.40.6.210
server211		A	10.40.6.211
server212		A	10.40.6.212
server213		A	10.40.6.213
server214		A	10.40.6.214
server215		A	10.40.6.215
server216		A	10.40.6.216
server217		A	10.40.6.217
server218		A	10.40.6.218
server219		A	10.40.6.219
server220		A	10.40.6.220
server221		A	10.40.6.221
server222		A	10.40.6.222
server223		A	10.40.6.223
server224		A	10.40.6.224
server225		A	10.40.6.225
server226		A	10.40.6.226
server227		A	10.40.6.227
server228		A	10.40.6.228
server229		A	10.40.6.229
server230		A	10.40.6.230
server231		A	10.40.6.231
server232		A	10.40.6.232
server233		A	10.40.6.233
server234		A	10.40.6.234
server235		A	10.40.6.235
server236		A	10.40.6.236
server237		A	10.40.6.237
server238		A	10.40.6.238
server239		A	10.40.6.239
server240		A	10.40.6.240
server241		A	10.40.6.241
server242		A	10.40.6.242
server243		A	10.40.6.243
server244		A	10.40.6.244
server245		A	10.40.6.245
server246		A	10.40.6.246
server247		A	10.40.6.247
server248		A	10.40.6.248
server249		A	10.40.6.249
server250		A	10.40.6.250
server251		A	10.40.6.251
server252		A	10.40.6.252
server253		A	10.40.6.253
server254		A	10.40.6.254
server64		A	10.40.6.64
server65		A	10.40.6.65
server66		A	10.40.6.66
server67		A	10.40.6.67
server68		A	10.40.6.68
server69		A	10.40.6.69
server70		A	10.40.6.70
server71		A	10.40.6.71
server72		A	10.40.6.72
server73		A	10.40.6.73
server74		A	10.40.6.74
server75		A	10.40.6.75
server76		A	10.40.6.76
server77		A	10.40.6.77
server78		A	10.40.6.78
server79		A	10.40.6.79
server80		A	10.40.6.80
server81		A	10.40.6.81
server82		A	10.40.6.82
server83		A	10.40.6.83
server84		A	10.40.6.84
server85		A	10.40.6.85
server86		A	10.40.6.86
server87		A	10.40.6.87
server88		A	10.40.6.88
server89		A	10.40.6.89
server90		A	10.40.6.90
server91		A	10.40.6.91
server92		A	10.40.6.92
server93		A	10.40.6.93
server94		A	10.40.6.94
server95		A	10.40.6.95
server96		A	10.40.6.96
server97		A	10.40.6.97
server98		A	10.40.6.98
server99		A	10.40.6.99

#!/bin/sh

####
#
# RSYNC backup for mail
#
####

day=`date +%A`
today=`date +%m%d%y`

tmp_file=/tmp/backup.txt
timestamp=`date`
uid=`id`

mail=/usr/bin/email
mail_list="backup@broadon.com"
log_file="/tmp/logs/mailsync-$today.log"

remote_host="falcon.routefree.com"
src_dir=/cygdrive/c/IMail/
dst_dir="falcon.routefree.com:/d3/backup/mail/"
log_dir="falcon.routefree.com:/d3/logs/"

bkup_subdir=`date +%A`
bkup_dstdir="/d3/backup/mail-inc/"
bkup_dir=$bkup_dstdir$bkup_subdir


# Clears last week incremental directories
emptydir()
{
    tbk_dir=$1
    if [ "tbk_dir" = "" ]; then
        echo "Usage: emptydir bkup_dir"
        exit 1
    fi

    echo "$ Emptying remote backup directory '$tbk_dir'..." >> $tmp_file
    rsh $remote_host "rm -rf" $tbk_dir

}


echo "$timestamp: Started RSYNC Mail Backup" > $tmp_file
echo "$timestamp: Started RSYNC Mail Backup" > $log_file

echo "ID: $uid" >> $tmp_file
echo "ID: $uid" >> $log_file

emptydir $bkup_dir

opts="-av --force --delete --backup --backup-dir=$bkup_dir"

cd $src_dir

echo "$ Syncing users directory..." >> $tmp_file
rsync $opts -e "rsh --" --exclude 'Spam*' --exclude 'Junk*' --exclude 'Trash*' --exclude 'test*' users $dst_dir >> $log_file 2>&1
    rsync_exit=$?
    timestamp=`date`
    if [ "$rsync_exit" -ge 1 ]
    then
        echo "$timestamp: BACKUP ****ERROR**** - '$log_file'" >> $tmp_file
        echo "$timestamp: BACKUP ****ERROR**** - '$log_file'" >> $log_file
    fi

echo "$ Syncing routefree directory..." >> $tmp_file
rsync $opts -e "rsh --" --exclude 'Spam*' --exclude 'Junk*' --exclude 'Trash*' --exclude 'test*' --exclude 'Web' routefree $dst_dir >> $log_file 2>&1
    rsync_exit=$?
    timestamp=`date`
    if [ "$rsync_exit" -ge 1 ]
    then
        echo "$timestamp: BACKUP ****ERROR**** - '$log_file'" >> $tmp_file
        echo "$timestamp: BACKUP ****ERROR**** - '$log_file'" >> $log_file
    fi

echo "$ Syncing aliases directory..." >> $tmp_file
rsync $opts -e "rsh --" aliases $dst_dir >> $log_file 2>&1
    rsync_exit=$?
    timestamp=`date`
    if [ "$rsync_exit" -ge 1 ]
    then
        echo "$timestamp: BACKUP ****ERROR**** - '$log_file'" >> $tmp_file
        echo "$timestamp: BACKUP ****ERROR**** - '$log_file'" >> $log_file
    fi

echo "$ Syncing OpenLDAP directory..." >> $tmp_file
rsync $opts -e "rsh --" --exclude '*.exe' --exclude '*.dll' OpenLDAP $dst_dir >> $log_file 2>&1
    rsync_exit=$?
    timestamp=`date`
    if [ "$rsync_exit" -ge 1 ]
    then
        echo "$timestamp: BACKUP ****ERROR**** - '$log_file'" >> $tmp_file
        echo "$timestamp: BACKUP ****ERROR**** - '$log_file'" >> $log_file
    fi

echo "$ Syncing rules, antispam, and DB files..." >> $tmp_file
rsync $opts -e "rsh --" *.txt *.ima *.reg maildb1.* $dst_dir >> $log_file 2>&1
    rsync_exit=$?
    timestamp=`date`
    if [ "$rsync_exit" -ge 1 ]
    then
        echo "$timestamp: BACKUP ****ERROR**** - '$log_file'" >> $tmp_file
        echo "$timestamp: BACKUP ****ERROR**** - '$log_file'" >> $log_file
    fi

echo "$timestamp: RSYNC Mail Backup Done!" >> $tmp_file
echo "$timestamp: RSYNC Mail Backup Done!" >> $log_file

rcp $log_file $log_dir/. >> $log_file 2>&1

$mail -s "Mail RSYNC Backup" $mail_list < $tmp_file

# rm -f $tmp_file $log_file


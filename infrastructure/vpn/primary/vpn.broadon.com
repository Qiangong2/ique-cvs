;
; Master file for vpn.broadon.com domain
;

$TTL 1h
$ORIGIN broadon.com.
vpn	IN	SOA	ns1.vpn.broadon.com. hostmaster.broadon.com. (
	2003091011	; Serial
	3h		; Refresh
	1h		; Retry
	1w		; Expire
	1d )		; Negative cache TTL

	IN	MX	0	mail.broadon.com.
;
; Name servers
;
$ORIGIN vpn.broadon.com.
	IN	NS	ns1.vpn.broadon.com.
	IN	NS	ns2.vpn.broadon.com.
	
ns1	IN	A	192.168.16.16
ns2	IN	A	192.168.17.16

mail	IN	CNAME	mail.broadon.com.

prod	IN	NS	dns.prod.vpn.broadon.com.
ems	IN	NS	dns.ems.vpn.broadon.com.
bcc	IN	NS	dns.bcc.vpn.broadon.com.

bcc-beta	IN	NS	dns.bcc-beta.vpn.broadon.com.
ems-beta	IN	NS	dns.ems-beta.vpn.broadon.com.
idc-beta	IN	NS	dns.idc-beta.vpn.broadon.com.

dns.prod	IN	A	192.168.16.16
dns.ems		IN	A	192.168.17.16
dns.bcc		IN	A	192.168.16.16

dns.bcc-beta	IN	A	172.16.9.16
dns.idc-beta	IN	A	172.16.10.16
dns.ems-beta	IN	A	172.16.11.16

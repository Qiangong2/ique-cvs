#!/bin/bash

#
# update the forward and reverse maps with new versions
# with the logic to start and stop the daemons as appropriate
#

if [ -f db.routefree.com.new ] && [ -f db.10.0.0.new ]; then

    /etc/rc.d/init.d/dhcpd stop
    /etc/rc.d/init.d/named stop

    mv db.routefree.com db.routefree.com.bak
    mv db.routefree.com.new db.routefree.com
    chown named.named db.routefree.com
    chmod 444 db.routefree.com
    mv db.10.0.0 db.10.0.0.bak
    mv db.10.0.0.new db.10.0.0
    chown named.named db.10.0.0
    chmod 444 db.10.0.0

    /etc/rc.d/init.d/named start
    /etc/rc.d/init.d/dhcpd start

    echo map files updated

else
    echo no new map files, do nothing
fi

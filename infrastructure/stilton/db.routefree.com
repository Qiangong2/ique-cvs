;BIND DUMP V8
$ORIGIN com.
routefree	43200	IN	SOA	backup.routefree.com. hostmaster.therouter.routefree.com. (
		2001093778 3600 900 1209600 43200 )

	43200	IN	A	10.0.0.1
	43200	IN	NS	backup.routefree.com.
	43200	IN	MX	0	avocetgw.routefree.com.

$ORIGIN routefree.com.

www		43200	IN	A	66.166.204.114

backup		43200	IN	A	10.0.0.14
avocetgw	43200	IN	A	10.0.0.20

perfclient	3600	IN	A	10.253.0.117
avocet44-b	0	IN	A	10.255.0.109
viper2	43200	IN	A	10.255.0.100
teststation1	0	IN	A	10.255.0.1
avocet44	0	IN	A	10.255.0.108

dns1	0	IN	A	172.16.0.10
fw1	0	IN	A	172.16.0.1

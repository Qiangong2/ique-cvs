;BIND DUMP V8
$ORIGIN broadon.com.
@	43200	IN	NS	backup.routefree.com.
@	43200	IN	SOA	ns1.broadon.com.	hostmaster.therouter.routefree.com. (
			2000057016 ; serial
			3600 ; refresh
			900 ; retry
			1209600 ; expire
			43200 ; default_ttl
			)

	43200	IN	MX	0	avocetgw.routefree.com.

www	43200	IN	A	66.166.204.114
ns1	43200	IN	A	10.0.0.14
mail	43200	IN	CNAME	avocetgw.routefree.com.

;BIND DUMP V8
$ORIGIN routefree.com.
$TTL	7200
exchdomain	43200	IN	SOA	backup.routefree.com. 	hostmaster.therouter.routefree.com. (
			2001091703 ; serial
			3600 ; refresh
			900 ; retry
			1209600 ; expire
			43200 ; default_ttl
			)
exchdomain	43200	IN	NS	backup.routefree.com.
exchdomain	43200	IN	MX	0	avocetgw.routefree.com.

$ORIGIN exchdomain.routefree.com.

w2kserver	43200	IN	A	10.0.0.43

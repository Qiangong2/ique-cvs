#!/usr/bin/perl -w

#
# Perl script to convert /proc/net/ip_conntrack entries into useful web page
#

use Getopt::Std;

undef $opt_P; undef $opt_v; undef $Usage;

$Usage="Usage: conntrack_print.pl [-Pv] conntrackfile\n"; 

getopts('Pv');

$verbose = 0;
$verbose = 1 if (defined $opt_v);

while($#ARGV >= 0) {
    $ctrkfile = $ARGV[0];
    shift @ARGV;
}

die "conntrack file not defined\n$Usage" if (!defined $ctrkfile);

open CTRK, $ctrkfile or die "Can't open conntrack file $ctrkfile\n"; 

#
# Read in the /etc/services file and build a hash of the text names
# of various port/protocol combinations
#
open SERVICES, "/etc/services" or die "Can't open /etc/services: $!\n";

while ( <SERVICES> ) {  
        chomp;
        next if ( /^\s*#/ );
        next if ( /^\s*$/ );
        s/\s+/ /g;
        @fld = split(' ');
        $services{ $fld[1] } = $fld[0];
}

#
# Add a few custom entries known not to be in /etc/services
#
$services{ "5190/tcp" } = "aim";
$services{ "6346/tcp" } = "gnutella";
$services{ "7777/tcp" } = "carrierscan";
$services{ "1723/tcp" } = "pptp";
$services{ "1755/tcp" } = "ms-streaming";
$services{ "1755/udp" } = "ms-streaming";
$services{ "1863/tcp" } = "msnp";
$services{ "1863/udp" } = "msnp";
$services{ "1900/tcp" } = "ssdp";
$services{ "1900/udp" } = "ssdp";
$services{ "3389/tcp" } = "ms-wbt-server";
$services{ "3389/udp" } = "ms-wbt-server";
$services{ "5050/tcp" } = "mmcc";
$services{ "5050/udp" } = "mmcc";

#
# Build protocol hash for recognizing non-tcp/udp/icmp by
# reading /etc/protocols
#
open PROTOCOLS, "/etc/protocols" or die "Can't open /etc/protocols: $!\n";

while ( <PROTOCOLS> ) {  
        chomp;
        next if ( /^\s*#/ );
        next if ( /^\s*$/ );
        s/\s+/ /g;
        @fld = split(' ');
        $protocols{ $fld[1] } = $fld[0];
}

#
# Main routine
#
printf "Parse $ctrkfile and print by %s\n",
	(defined $opt_P) ? "Protocol" : "SourceIP" if ($verbose);

while (<CTRK>) {
	
	if ( /^tcp/ ) { parse_tcp(); }
	if ( /^udp/ ) { parse_udp(); }
	if ( /^icmp/ ) { parse_icmp(); }
	if ( /^unknown/ ) { parse_generic(); }
	
}

close CTRK;

if (defined $opt_P) {
	print_byprotocol();
} else {
	print_bysource();
}

exit;

## End of main ##

sub print_bysource {
	print "Content-Type: text/html\n\n";
	print "<html>\n";
	printf "<bl>";

	#
	# Print the associative table by source ip address on the originating
	# side of the connection
	#
	foreach $addr (sort keys(%bysource)) {

		@targets = split(';', $bysource{$addr});

		foreach $target (sort @targets) {
			printf "<li>from %s port %s</li>\n", $addr, $target;
		}
	}

	printf "</bl>";
	printf "</html>";
}

sub print_byprotocol {
	my $desc;

	print "Content-Type: text/html\n\n";
	print "<html>\n";
	printf "<bl>";

	#
	# Print the associative table by protocol
	#
	foreach $prot (sort keys(%byprotocol)) {

		@targets = split(';', $byprotocol{$prot});

		$desc = (defined $services{$prot}) ? " ($services{$prot})" : "";

		foreach $target (sort @targets) {
			printf "<li>%s%s from %s</li>\n",
				$prot, $desc,  $target;
		}
	}

	printf "</bl>";
	printf "</html>";
}

sub parse_tcp {
	my $desc;

	@fld = split(' ');	

	($srcip = $fld[4]) =~ s/src=([0-9.]+)/$1/;
	($sport = $fld[6]) =~ s/sport=([0-9]+)/$1/;
	($dstip = $fld[5]) =~ s/dst=([0-9.]+)/$1/;
	($dport = $fld[7]) =~ s/dport=([0-9]+)/$1/;

	printf "TCP proto %s timeout %s status %s source %s/%s dest %s/%s\n",
		 $fld[1], $fld[2], $fld[3], $srcip, $sport, $dstip, $dport if ($verbose);

	$desc = $services{$dport . "/tcp"};
	$desc = ($desc) ? " ($desc)" : "";

	$bysource{$srcip} .= "$sport to $dstip $dport/tcp$desc $fld[3];";
	$byprotocol{"$dport/tcp"} .= "$srcip/$sport to $dstip $fld[3];"; 
}

sub parse_udp {
	my $desc;

	@fld = split(' ');	

	($srcip = $fld[3]) =~ s/src=([0-9.]+)/$1/;
	($sport = $fld[5]) =~ s/sport=([0-9]+)/$1/;
	($dstip = $fld[4]) =~ s/dst=([0-9.]+)/$1/;
	($dport = $fld[6]) =~ s/dport=([0-9]+)/$1/;

	printf "UDP proto %s timeout %s source %s/%s dest %s/%s\n",
		 $fld[1], $fld[2], $srcip, $sport, $dstip, $dport if ($verbose);

	$desc = $services{$dport . "/udp"};
	$desc = ($desc) ? " ($desc)" : "";

	$bysource{$srcip} .= "$sport to $dstip udp/$dport$desc;";
	$byprotocol{"$dport/udp"} .= "$srcip/$sport to $dstip;"; 
}

sub parse_icmp {

	@fld = split(' ');	

	($srcip = $fld[3]) =~ s/src=([0-9.]+)/$1/;
	($sport = $fld[5]) =~ s/sport=([0-9]+)/$1/;
	($dstip = $fld[4]) =~ s/dst=([0-9.]+)/$1/;
	($dport = $fld[6]) =~ s/dport=([0-9]+)/$1/;

	printf "ICMP proto %s timeout %s source %s/%s dest %s/%s\n",
		 $fld[1], $fld[2], $srcip, $sport, $dstip, $dport if ($verbose);

	$bysource{$srcip} .= "$dstip icmp/$dport;";
	$byprotocol{"$dport/icmp"} .= "$srcip/$sport to $dstip;"; 
}

sub parse_generic {
	my $name;

	@fld = split(' ');	

	($srcip = $fld[3]) =~ s/src=([0-9.]+)/$1/;
	($dstip = $fld[4]) =~ s/dst=([0-9.]+)/$1/;
	$name = $protocols{$fld[1]};

	printf "Other proto %s timeout %s source %s dest %s\n",
		 $name, $fld[2], $srcip, $dstip if ($verbose);

	$bysource{$srcip} .= "$dstip  $name/$fld[1];";
	$byprotocol{"$fld[1]/$name"} .= "$srcip to $dstip;"; 
}

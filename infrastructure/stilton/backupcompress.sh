#!/bin/sh

#
# Compress backup files generated from SME backup/restore facility
#

TMPFILE=/tmp/backupcompress.tmp.$$
LOGFILE=/tmp/backupcompress.log.$$
ERRFILE=/tmp/backupcompress.err.$$

FTPDIR=/big/ftp/home

cd $FTPDIR
find . -name "*.tar" -print | sort > $TMPFILE

while read tarfile
do
	basefile=`echo $tarfile | sed -e 's/\.tar$//'`
	snapfile=$basefile.snap
	if [ ! -f $snapfile ]
	then
		echo tar file with no snap file: $snapfile >> $LOGFILE
	else
		echo found tar file and matching snap file $tarfile >> $LOGFILE
		gzip $snapfile
		gzip $tarfile
		mv $basefile.tar.gz $basefile.tgz
	fi
done < $TMPFILE

if [ -s $ERRFILE ]
then
	cat $ERRFILE | mail -s "backupcompress cron job errs" paulm@routefree.com
	rm $ERRFILE
fi

cat $LOGFILE | mail -s "backup compression results" paulm@routefree.com

rm $TMPFILE
rm $LOGFILE

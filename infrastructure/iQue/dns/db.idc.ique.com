;
; Master file for idc.ique.com domain
;

$TTL 1h
$ORIGIN ique.com.
idc	IN	SOA	ns1.idc.ique.com. hostmaster.ique.com. (
	2000092515	; Serial
	3h		; Refresh
	1h		; Retry
	1w		; Expire
	1d )		; Negative cache TTL

	IN	A	210.22.195.84
	IN	MX	0	mail.ique.com.
;
; Name servers
;
$ORIGIN idc.ique.com.
	IN	NS	ns1.idc.ique.com.
	IN	NS	ns2.idc.ique.com.
	
ns1	IN	A	210.22.195.84
ns2	IN	A	210.22.195.83


xs		IN	A	210.22.195.84
cds		IN	A	210.22.195.84
ops		IN	A	210.22.195.84

rms		IN	A	210.22.195.84
activate	IN	CNAME	rms
download	IN	CNAME	rms
status		IN	CNAME	rms
update		IN	CNAME	rms
register	IN	CNAME	rms
support		IN	CNAME	rms
ntp		IN	CNAME	rms


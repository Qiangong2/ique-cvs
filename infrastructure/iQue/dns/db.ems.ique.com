;
; Master file for ems.ique.com domain
;

$TTL 1h
$ORIGIN ique.com.
ems	IN	SOA	ns1.ems.ique.com. hostmaster.ique.com. (
	2000092517	; Serial
	3h		; Refresh
	1h		; Retry
	1w		; Expire
	1d )		; Negative cache TTL

	IN	A	210.22.195.84
	IN	MX	0	mail.ique.com.
;
; Name servers
;
$ORIGIN ems.ique.com.
	IN	NS	ns1.idc.ique.com.
	IN	NS	ns2.idc.ique.com.
	
ns1	IN	A	210.22.195.84
ns2	IN	A	210.22.195.83


ems		IN	A	210.22.195.83
cds		IN	A	210.22.195.83


USB,SERIAL,IR  Revised: Tuesday, August 08, 2000
PEANUT PCI I/O Board          Revision: 01

RouteFree, Inc.
3400 Hillview Ave. Bldg5
Palo Alto, CA 94304
Bill Saperstein


Bill Of Materials         August 8,2000      15:01:02	Page1

Item	Quantity	Reference		Part	Package	Vendor	Part Number
______________________________________________

1	1	BT1		BATTERY	coin		
2	11	C1		.01U	0805	AVX			080553c103mat2a
		C17		.01U	0805	AVX	
		C19		.01U	0805	AVX	
		C39		.01U	0805	AVX	
		C73		.01U	0805	AVX	
		C74		.01U	0805	AVX	
		C91		.01U	0805	AVX	
		C92		.01U	0805	AVX	
		C102		.01U	0805	AVX	
		C104		.01U	0805	AVX	
		C105		.01U	0805	AVX	
3	1	C2		100U	D-CASE	AVX			tajd107m010r
4	2	C3		470P	0805	AVX			08055a471jat4a	
		C4		470P	0805	AVX	
5	1	C5		.001U	0805	AVX			08055a102jat2a
6	45	C6		.1U	0805	AVX	 			08053e104mat2a
		C7		.1U	0805	AVX	
		C8		.1U	0805	AVX	
		C13		.1U	0805	AVX	
		C14		.1U	0805	AVX	
		C15		.1U	0805	AVX	
		C20		.1U	0805	AVX	
		C21		.1U	0805	AVX	
		C22		.1U	0805	AVX	
		C23		.1U	0805	AVX	
		C24		.1U	0805	AVX	
		C35		.1U	0805	AVX	
		C36		.1U	0805	AVX	
		C37		.1U	0805	AVX	
		C38		.1U	0805	AVX	
		C40		.1U	0805	AVX	
		C41		.1U	0805	AVX	
		C42		.1U	0805	AVX	
		C43		.1U	0805	AVX	
		C44		.1U	0805	AVX	
		C45		.1U	0805	AVX	
		C46		.1U	0805	AVX	
		C49		.1U	0805	AVX	
		C65		.1U	0805	AVX	
		C66		.1U	0805	AVX	
		C67		.1U	0805	AVX	
		C68		.1U	0805	AVX	
		C69		.1U	0805	AVX	
		C70		.1U	0805	AVX	
		C71		.1U	0805	AVX	
		C72		.1U	0805	AVX	
		C85		.1U	0805	AVX	
		C86		.1U	0805	AVX	
		C87		.1U	0805	AVX	
		C88		.1U	0805	AVX	
		C89		.1U	0805	AVX	
		C90		.1U	0805	AVX	
		C95		.1U	0805	AVX	
		C96		.1U	0805	AVX	
		C97		.1U	0805	AVX	
		C98		.1U	0805	AVX	
		C99		.1U	0805	AVX	
		C100		.1U	0805	AVX	
		C101		.1U	0805	AVX	
		C103		.1U	0805	AVX	
7	1	C9		6.8U	B-Case	AVX			tajb685m010r
8	3	C10		.47U	0805	AVX			0805yg474zat2a
		C11		.47U	0805	AVX	
		C18		.47U	0805	AVX	
9	1	C12		220P	0805	AVX			08055a221jat2a
10	1	C16		4700P	0805	AVX			08055c472kat2a
11	19	C25		22U	D-Case	AVX				tajd226m016r
		C57		22U	D-Case	AVX	
		C58		22U	D-Case	AVX	
		C59		22U	D-Case	AVX	
		C60		22U	D-Case	AVX	
		C61		22U	D-Case	AVX	
		C62		22U	D-Case	AVX	
		C63		22U	D-Case	AVX	
		C64		22U	D-Case	AVX	
		C75		22U	D-Case	AVX	
		C76		22U	D-Case	AVX	
		C77		22U	D-Case	AVX	
		C78		22U	D-Case	AVX	
		C79		22U	D-Case	AVX	
		C80		22U	D-Case	AVX	
		C81		22U	D-Case	AVX	
		C82		22U	D-Case	AVX	
		C83		22U	D-Case	AVX	
		C84		22U	D-Case	AVX	
12	1	C26		.047U	0805	AVX			08055ca473kat2a
13	2	C28		.22U	0805	AVX			0805yg224zat2a
		C27		.22U	0805	AVX	
14	2	C29		680P	0805	AVX			08055a680jat2a
		C30		680P	0805	AVX	
15	3	C31		10U	B-Case	AVX				tajb106m010r	
		C32		10U	B-Case	AVX	
		C50		10U	B-Case	AVX	
16	4	C33		10P	0805	AVX				08055a100gat2a
		C34		10P	0805	AVX	
		C93		10P	0805	AVX	
		C94		10P	0805	AVX	
17	2	C48		32P	0805	AVX				08055a330jat2a
		C47		32P	0805	AVX	
18	2	C51		12P	0805	AVX				08055a120jat2a	
		C52		12P	0805	AVX	
19	4	C53		47P	0805	AVX				0805a470jat2a	
		C54		47P	0805	AVX	
		C55		47P	0805	AVX	
		C56		47P	0805	AVX	
20	2	C107		27P	0805	AVX			08055a270jat2a	
		C106		27P	0805	AVX	
21	3	D1		DIODE	0805		
		D2		DIODE	0805		
		D7		DIODE	0805		
22	2	D3		GREEN	0805	Optoelectronics	
		D5		GREEN	0805	Optoelectronics	
23	1	D4		RED	0805	Optoelectronics
24	1	D6		YELLOW	0805	Optoelectronics	
25	1	F1		1.5A	SMD100	Raychem	SMD100-2
26	1	JP1		HEADER 3			
27	1	JP2		HEADER 8X2			
28	1	JP3		HEADER 6X2			
29	1	JP4		HEADER 3X2			
30	1	J1		AMP	RA-Thru-hole		787617-1
31	1	J2		PCIFINGERS			
32	2	J3		IDE CON.		AMP	104338-8
		J4		IDE CON.		AMP	104338-8
33	1	J5		PHONEJACK STEREO			
34	2	J7		RCA JACK			
		J6		RCA JACK			
35	1	J8		AMP_555164-1	8P_PHJK_UNSHIELD	AMP	555164-1
36	3	L1		F.B.	0805		
		L2		F.B.	0805		
		L3		F.B.	0805		
37	1	P1		CONNECTOR 2XDB9	Thru-hole	AMP	787920-1
38	2	R2		560K	0805	Phillips	     9c8052a5603jlrt/r
		R1		560K	0805	Phillips	
39	1	R3		10	0805	Phillips	         9c8052a10r0jlrt/r
40	1	R4		4.7	0805	Phillips	         9c8052a4r70jlrt/r
41	1	R5		560	0805	Phillips	         
42	2	R9		5.6K	0805	Phillips	     9c8052a5601jlrt/r
		R6		5.6K	0805	Phillips	
43	5	R7		200	0805	Phillips	         9c8052a2000jlrt/r
		R10		200	0805	Phillips	
		R28		200	0805	Phillips	
		R29		200	0805	Phillips	
		R43		200	0805	Phillips	
44	6	R8		330	0805	Phillips	         9c8052a3300jlrt/r
		R11		330	0805	Phillips	
		R110		330	0805	Phillips	
		R111		330	0805	Phillips	
		R112		330	0805	Phillips	
		R113		330	0805	Phillips	
45	22	R12		33	0805	Phillips	         9c8052a33r0jlrt/r
		R13		33	0805	Phillips	
		R59		33	0805	Phillips	
		R60		33	0805	Phillips	
		R61		33	0805	Phillips	
		R62		33	0805	Phillips	
		R63		33	0805	Phillips	
		R64		33	0805	Phillips	
		R65		33	0805	Phillips	
		R66		33	0805	Phillips	
		R67		33	0805	Phillips	
		R68		33	0805	Phillips	
		R77		33	0805	Phillips	
		R78		33	0805	Phillips	
		R79		33	0805	Phillips	
		R80		33	0805	Phillips	
		R81		33	0805	Phillips	
		R82		33	0805	Phillips	
		R83		33	0805	Phillips	
		R84		33	0805	Phillips	
		R85		33	0805	Phillips	
		R86		33	0805	Phillips	
46	2	R14		100K	0805	Phillips	     9c8052a1003jlrt/r
		R15		100K	0805	Phillips	
47	7	R16		10M	0805	Phillips	         9c8052a1005jlrt/r
		R33		10M	0805	Phillips	
		R34		10M	0805	Phillips	
		R35		10M	0805	Phillips	
		R94		10M	0805	Phillips	
		R115		10M	0805	Phillips	
		R124		10M	0805	Phillips	
48	39	R17		10K	0805	Phillips	          9c8052a1002jlrt/r
		R18		10K	0805	Phillips	
		R19		10K	0805	Phillips	
		R20		10K	0805	Phillips	
		R21		10K	0805	Phillips	
		R22		10K	0805	Phillips	
		R23		10K	0805	Phillips	
		R24		10K	0805	Phillips	
		R25		10K	0805	Phillips	
		R26		10K	0805	Phillips	
		R37		10K	0805	Phillips	
		R38		10K	0805	Phillips	
		R39		10K	0805	Phillips	
		R40		10K	0805	Phillips	
		R41		10K	0805	Phillips	
		R42		10K	0805	Phillips	
		R45		10K	0805	Phillips	
		R71		10K	0805	Phillips	
		R72		10K	0805	Phillips	
		R73		10K	0805	Phillips	
		R74		10K	0805	Phillips	
		R75		10K	0805	Phillips	
		R76		10K	0805	Phillips	
		R95		10K	0805	Phillips	
		R96		10K	0805	Phillips	
		R97		10K	0805	Phillips	
		R98		10K	0805	Phillips	
		R99		10K	0805	Phillips	
		R100		10K	0805	Phillips	
		R101		10K	0805	Phillips	
		R102		10K	0805	Phillips	
		R103		10K	0805	Phillips	
		R104		10K	0805	Phillips	
		R105		10K	0805	Phillips	
		R106		10K	0805	Phillips	
		R107		10K	0805	Phillips	
		R108		10K	0805	Phillips	
		R109		10K	0805	Phillips	
		R116		10K	0805	Phillips	
49	1	R27		27K	0805	Phillips	              9c0
50	3	R30		470	0805	Phillips	
		R31		470	0805	Phillips	
		R32		470	0805	Phillips	
51	2	R36		1K	0805	Phillips	
		R114		1K	0805	Phillips	
52	1	R44		20K	0805	Phillips	
53	4	R46		27	0805	Phillips	
		R48		27	0805	Phillips	
		R49		27	0805	Phillips	
		R50		27	0805	Phillips	
54	4	R47		47	SOE16	CTS	766163470-GPS
		R55		47	SOE16	CTS	766163470-GPS
		R69		47	SOE16	CTS	766163470-GPS
		R70		47	SOE16	CTS	766163470-GPS
55	4	R51		15K	0805	Phillips	
		R52		15K	0805	Phillips	
		R53		15K	0805	Phillips	
		R54		15K	0805	Phillips	
56	3	R56		0	0805	Phillips	
		R57		0	0805	Phillips	
		R58		0	0805	Phillips	
57	7	R87		4.7K	0805	Phillips	
		R88		4.7K	0805	Phillips	
		R89		4.7K	0805	Phillips	
		R90		4.7K	0805	Phillips	
		R91		4.7K	0805	Phillips	
		R92		4.7K	0805	Phillips	
		R93		4.7K	0805	Phillips	
58	2	R117		10.5	0805	Phillips	
		R121		10.5	0805	Phillips	
59	2	R118		16.9	0805	Phillips	
		R120		16.9	0805	Phillips	
60	1	R119		82.5	0805	Phillips	
61	2	R122		49.9	0805	Phillips	
		R123		49.9	0805	Phillips	
62	1	S1		plunger - SPDT		C&K	8215D9A2GE
63	1	T1		20F001N	16P DIP	YCL	20F001N
64	2	U1		DS14185	20P SOIC	National	DS14185
		U3		DS14185	20P SOIC	National	DS14185
65	1	U2		HSDL-2100	SMT	Hewlett-Packard	HSDL-2100
66	1	U4		74AC04	14P-SOIC	TI	SN74AC04
67	1	U5		78L05-SO8	SO-8	National	
68	1	U6		ES1869	100P PQFP	ESS	ESS1869
69	1	U7		RTL8019	100L QFP	RealTek	RTL8019AS
70	1	U8		93C06	TSSOP-8	Fairchild	FM93C06
71	1	U9		M5819D	24P-SOP	ALI	M5819P
72	1	U10		M1543_2	328P-BGA	ALI	M1543C
73	1	U11		74LS245	20P-SOIC	Fairchild	
74	1	U12		M1543_1	328P-BGA	ALI	M1543C
75	1	U13		14.318MHz	8P DIP	Fox	H5C-2E
76	1	U14		48MHz	8P DIP	Fox	H5C-2E
77	1	U15		DP83907	132P-PQFP	National	
78	2	U16		CY7C185	28P-SOIC	Cypress	CY7C185
		U17		CY7C185	28P-SOIC	Cypress	CY7C185
79	1	Y1		14.318MHz	SMD	Fox	FPX
80	1	Y2		20MHz	SMD	Fox	FPX
81	1	Y3		32.768K	SMD	Fox	FSM327
82	1	Y4		32.768K	SMD	Fox	FSM327
83	1	Y5		20MHz		FOX	FPX

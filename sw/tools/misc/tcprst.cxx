/* Test OSC with client cancel (TCP RST ) */

#include <stddef.h>    // NULL
#include <stdio.h>     // fopen, ...
#include <stdarg.h>    // va_list
#include <syslog.h>    // openlog, vsyslog, closelog
#include <stdlib.h>    // exit
#include <unistd.h>    // unlink
#include <time.h>      // asctime_r
#include <string.h>    // index
#include <netinet/in.h>            // sockaddr_in
#include <sys/types.h>             // connect
#include <sys/stat.h>              // open
#include <sys/socket.h>            // connect
#include <netdb.h>
#include <netinet/in.h>


#include <iostream>
#include <string>

using namespace std;


bool dbg = true;


#define MSG_EMERG   0
#define MSG_ALERT   1
#define MSG_CRIT    2
#define MSG_ERR     3
#define MSG_WARNING 4
#define MSG_NOTICE  5
#define MSG_INFO    6
#define MSG_DEBUG   7
#define MSG_ALL     8

void msglog(int level, char *fmt, ...)
{
    va_list ap;
    va_start (ap, fmt);
    vfprintf (stderr, fmt, ap);
    va_end (ap);
    fflush (stderr);
} 


int parseURL(const char *url_str, bool& secure, string& server, int& port, string& uri)
{
    const char *hostname;
    secure = false;
    if ((hostname = strstr(url_str, "https://")) != NULL) {
	secure = true;
	hostname += strlen("https://");
    } else if ((hostname = strstr(url_str, "http://")) != NULL) {
	secure = false;
	hostname += strlen("http://");
    } else {
	msglog(MSG_ERR, "parseURL: url protocol is neither http nor https\n");
	return -1;
    }
    
    const char *hostname_end = hostname;
    while (*hostname_end) {
	if (*hostname_end == ':' ||
	    *hostname_end == '/' ||
	    *hostname_end == '?')
	    break;
	hostname_end++;
    }
    server = string(hostname, hostname_end - hostname);

    port = 80;
    if (*hostname_end == ':') 
	port = atoi(++hostname_end);
    if (port == 0) {
	msglog(MSG_ERR, "parseURL: port number is 0\n");
	return -1;
    }

    uri = "";
    const char *suffix = hostname_end;
    while (*suffix) {
	if (*suffix == '/' ||
	    *suffix == '?') {
	    uri = suffix;
	    break;
	}
	suffix++;
    }

    return 0;
}


#define HTTP_VERSION        " HTTP/1.1\r\n"

string make_get_msg(const string& host, const string& url, const string& getmsg) 
{
    return 
	string("GET ") +
	url +
	getmsg +
	string(HTTP_VERSION) +
	string("Host: ") + host + string("\r\n") +
	string("Accept: text/html, image/gif, image/jpeg, */*\r\n") +
	string("\r\n");
}


bool dbg_ssl_socket = true;

int connect(int& skt, const char *host, int port)
{
    struct hostent* h;
    if ((h = gethostbyname(host)) == 0) {
	if (dbg_ssl_socket) 
	    fprintf(stderr, "ssl_socket.C:connect:gethostbyname %s failed\n",
		    host);
	return -1;
    }
    struct sockaddr_in sa;
    sa.sin_family = AF_INET;
    sa.sin_port = htons(port);
    for(int i = 0; h->h_addr_list[i]; i++) {
	sa.sin_addr = *(struct in_addr*)h->h_addr_list[i];
	if (connect(skt, (struct sockaddr*)&sa, sizeof sa) >= 0) {
	    return 0;
	}
    }
    if (dbg_ssl_socket) 
	fprintf(stderr, "ssl_socket.C:connect failed\n");
    return -1;
}




int main(int argc, char *argv[])
{
    if (argc != 2) {
	cerr << "Usage: tcprl url" << endl;
	exit(1);
    }
	
    bool secure;
    string server;
    string uri;
    int port;
    string url = argv[1];

    if (parseURL(url.c_str(), secure, server, port, uri) != 0) {
	exit(1);
    }

    if (dbg) {
	cerr << "server: " << server << endl;
	cerr << "port: " << port << endl;
	cerr << "uri: " << uri << endl;
    }

    string getmsg = make_get_msg(server, url, "");
    if (dbg) {
	cerr << "getmsg: " << getmsg << endl;
    }

    int skt = socket(PF_INET, SOCK_STREAM, 0);
    int err;

    if (connect(skt, server.c_str(), port) != 0) {
	exit(1);
    }

    int n = write(skt, getmsg.c_str(), getmsg.length());
    if (dbg) 
	cerr << "wrote " << n << " bytes" << endl;

#if 0
    char buf[16384];
    n = read(skt, buf, sizeof(buf));
    if (dbg) 
	cerr << "read " << n << " bytes" << endl;
#endif

    struct linger linger;
    linger.l_onoff = 1;
    linger.l_linger = 0;

    err = setsockopt(skt, SOL_SOCKET, SO_LINGER, &linger, sizeof(linger));
    if (err != 0) {
	cerr << "setsockopt returns " << err << endl;
	perror("setsockopt:");
    }

    close(skt);
}

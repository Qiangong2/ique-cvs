<html>
<body>
<h1>Build and Test Report</h1>
<?
  $startweek = 1;
  $names[0] = "Raymond";
  $names[1] = "Wilson";
  $names[2] = "Vaibhav";
  $names[3] = "Jim";
  $names[4] = "Eli";
  $names[5] = "James";
  $names[6] = "Don";
  $names[7] = "Angel";
?>
<p>
Current time is <?= date('r') ?>.<br>
This week <?= $names[(date("W") - $startweek) % count($names)] ?> is on duty.

<? 
function firstday($nweeks)
{
  $x = time() + (1 - date('w')) * 24 * 3600; 
  $x += $nweeks * 7 * 24 * 3600;
  return date("M d", $x);
}
?>

<h3>Watch Schedule</h3>
<table width=30%>
<?
for ($i = 0; $i < count($names); $i++) {
?>
<tr><td>Week of <?= firstday($i)?></td><td>
<?= $names[(date("W") + $i - $startweek) % count($names)]?></td></tr>
<?
}
?>
</table>

<h3>Test Results</h3>
<?
 function show_testlog()
 {
    $fname = "/home/build/testlogs-1.4/latest/runtests.log";
    $res = stat($fname);
    echo "<p>Logfile is at builder:" . $fname . ".</br>";
    echo "Last modified time is " . date('r', $res["mtime"]) . ".</p>";
    $res = file($fname);
    foreach ($res as $linea) {
      if (strncmp("***", $linea, 3) == 0) {
        if (strstr($linea, "*** BEGIN") ||
            strstr($linea, "*** END")   ||
            strstr($linea, "*** IDCMON")) {
           echo "<b>" . $linea . "</b><br>";
        } else {
           if (strstr($linea, "FAILED") ||
               strstr($linea, "failed")) {
             echo '<font style="color: red">';
           } else {
             echo '<font>';
           }
           echo $linea . "</font></br>";
        }
      }
    } 
 }
 show_testlog();
?>

</body>
</html>

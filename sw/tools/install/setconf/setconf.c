#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "config.h"

int
main(int argc, char* argv[]) {
    int rval = 0;
    char buffer[MAX_CONFIG_SIZE];

    if (strstr(argv[0], "unsetconf") && argc != 1) {
	while (argc > 1) {
	    unsetconf(argv[1]);
	    argc--; argv++;
	}
    } else if (strstr(argv[0], "printconf")) {
	int quote = 0;
	int raw = 0;
	if (argc > 1 && strcmp(argv[1], "-q") == 0) {
	    quote = 1;
	    argc--; argv++;
	}
	if (argc > 1 && strcmp(argv[1], "-r") == 0) {
	    raw = 1;
	    argc--; argv++;
	}
	if (raw) {
	    if (argc == 1) {
		char buf[MAX_CONFIG_SIZE*2];
		char *p = buf;
		dumpconf(buf, sizeof(buf), NULL, 0);
		while (*p) {
		    printf("%s\n", p);
		    p += strlen(p) + 1;
		}
	    } else 
		if (getconfraw(argv[1], buffer, sizeof buffer)) {
		    printf("%s\n", buffer);
		} else
		    rval = 1;
	} else {
	    if (argc == 1)
		printconf(quote);
	    else if (getconf(argv[1], buffer, sizeof buffer)) {
		if (quote)
		    putquoted(buffer);
		else
		    printf("%s", buffer);
		putchar('\n');
	    }
	    else rval = 1;
	}
    } else if (argc == 1) {
        printconf(0);
    } else if (argc == 2 && strcmp(argv[1], "-e") == 0) {
    	eraseconf();
    } else {
	int raw = 0;
	if (argc > 2 && strcmp(argv[1], "-r") == 0) {
	    raw = 1;
	    argc--; argv++;
	}
	if (raw) 
	    rval = setconfraw(argv[1], 0);
	else
	    rval = setconf(argv[1], argv[2], 1);
    }
    if (rval < 0) rval =- rval;
    exit(rval);
}

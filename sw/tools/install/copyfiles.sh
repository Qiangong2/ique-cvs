#!/bin/sh
#
# copy files under "install" to release directory
#
DIR_LIST="bin files ks"
if [ "$2" = "" ] || [ ! -d $1 ]; then
    echo "usage: $0 src_dir dest_dir"
    exit 1
fi
cd $1
echo "Copying install/* from CVS tree to release $1"
tar cf - --exclude CVS $DIR_LIST | (mkdir -p $2; cd $2; tar xvf -)
cp Makefile.install $2/Makefile

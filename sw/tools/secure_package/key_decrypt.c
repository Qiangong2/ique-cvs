
/*
 * Application to unpack a encryption key
 */

#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <openssl/bio.h>
#include <openssl/x509.h>
#include <openssl/pkcs7.h>
#include <openssl/err.h>

int main(int argc, char *argv[]) {

  PKCS7 *p7 = NULL;

  BIO *in = NULL;
  BIO *p7bio = NULL;

  char buf[1024*4];
  char *keyFile = NULL;
  char *recipientFile = NULL;
  char *rootFile = NULL;
  char *inputFile = NULL;
  char *outFile = NULL;

  X509 *rootX509 = NULL;

  EVP_PKEY *recipientKey = NULL;
  X509 *recipientCert = NULL;

  struct stat statBuf;
  FILE *inFilePtr = NULL;
  FILE *outFilePtr = NULL;

  int i;
  int length;
  unsigned char *berBuffer;

  OpenSSL_add_all_algorithms();
  
  if (argc < 7) {
    printf("Usage: key_decrypt -k <recipient_key (DER format)> -r <recipient_cert (DER format)> -i <input_file> -o <output_file>\n");
    exit(-1);
  }

  OpenSSL_add_all_algorithms();

  while(argc > 1) {
    if ((strcmp(argv[1],"-R") == 0) && (argc >= 2)) {
      /*
       * Trusted root certificate
       */
      rootFile = argv[2];
      argc-=2;
      argv+=2;
      if (!(in=BIO_new_file(rootFile, "r"))) {
	goto err;
      }
      if ((rootX509 = d2i_X509_bio(in, &rootX509)) == NULL) {
	goto err;
      }
      BIO_free(in);
    } else if ((strcmp(argv[1],"-k") == 0) && (argc >= 2)) {
      /*
       * Recipient private key - only 1 of these
       */
      keyFile = argv[2];
      argc -= 2;
      argv += 2;
    } else if ((strcmp(argv[1],"-r") == 0) && (argc >= 2)) {
      /*
       * Recipient certificate - only 1
       */
      recipientFile = argv[2];
      argc -= 2;
      argv += 2;

    } else if ((strcmp(argv[1],"-i") == 0) && (argc >= 2)) {
      inputFile = argv[2];
      argc -= 2;
      argv += 2;
    } else if ((strcmp(argv[1],"-o") == 0) && (argc >= 2)) {
      outFile = argv[2];
      argc -= 2;
      argv += 2;
    } else {
      break;
    }
  }

  /*
   * Get the recipient private and certificate
   */
  if (!(in=BIO_new_file(keyFile, "r"))) {
    goto err;
  }
  if ((recipientKey=d2i_PrivateKey_bio(in, &recipientKey)) == NULL) {
    goto err;
  }
  BIO_free(in);

  if (!(in=BIO_new_file(recipientFile, "r"))) {
    goto err;
  }
  if ((recipientCert = d2i_X509_bio(in, &recipientCert)) == NULL) {
    goto err;
  }
  BIO_free(in);

  /*
   * Load PKCS7 object from a file
   */
  if (stat(inputFile, &statBuf) < 0) {
    fprintf(stderr, "Can't stat input file %s\n", inputFile);
    goto err;
  }
  length = statBuf.st_size;

  if ((berBuffer = (unsigned char *) malloc(length)) == NULL) {
    goto err;
  }

  if ((inFilePtr = fopen(inputFile, "r")) == 0) {
    fprintf(stderr, "Can't open input file %s\n", inputFile);
    goto err;
  }
  fread(berBuffer, 1, length, inFilePtr);

  if ((p7 = d2i_PKCS7(NULL, &berBuffer, length)) == NULL) {
    goto err;
  }

  if ((outFilePtr = fopen(outFile, "w")) == 0) {
    fprintf(stderr, "Can't open output file %s\n", outFile);
    goto err;
  }

  p7bio=PKCS7_dataDecode(p7, recipientKey, 0, recipientCert);
  if (p7bio == NULL) {
    fprintf(stderr, "Can't decode PKCS7 file\n");
    goto err;
  }

  /* 
   * We now have to 'read' from p7bio to calculate digests, decrypt etc. 
   */
  for (;;) {
    i=BIO_read(p7bio, buf, sizeof(buf));
    /* print it? */
    if (i <= 0) {
      break;
    }
    fwrite(buf, 1, i, outFilePtr);
  }
  
  exit(0);

 err:
  ERR_load_crypto_strings();
  ERR_print_errors_fp(stderr);
  exit(1);
}

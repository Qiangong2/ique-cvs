
/*
 * Application to encrypt a file using openssl symmetric encryption
 * algorithms.
 * The key used is generated randomly and output to a separate file.
 * The encryption can be done in place, so output and input file are the same.
 */

#include <stdio.h>
#include <string.h>
#include <openssl/evp.h>
#include <openssl/rand.h>
#include <openssl/err.h>

#define BUFFER_SIZE 1024

int main(int argc, char *argv[]) {
  
  char *okeyFile = NULL, *ikeyFile = NULL;
  char *inputFile;
  char *outFile = NULL;
  FILE *ikeyFilePtr = NULL;
  FILE *okeyFilePtr = NULL;
  FILE *outFilePtr = NULL;
  FILE *inFilePtr = NULL;
  int outLength;
  int inLength;

  unsigned char key[EVP_MAX_KEY_LENGTH];
  int keyLength;
  unsigned char inBuffer[BUFFER_SIZE];
  unsigned char outBuffer[BUFFER_SIZE];

  EVP_CIPHER_CTX ctx;

  /*
   * Change cipher here
   */
  EVP_CIPHER *cipher;

#ifdef WEAK_CRYPTO
  cipher = EVP_des_cbc();
#else
  cipher = EVP_des_ede3_cbc();
#endif

  OpenSSL_add_all_algorithms();
  memset((void *) &ctx, 0, sizeof(ctx));
  
  if (argc < 5) {
    printf("Usage: encrypt -k <output_key_file> -K <input_key_file>  -i <input_file> -o <output_file>\n");
    exit(-1);
  }

  while(argc > 1) {
    if ((strcmp(argv[1],"-k") == 0) && (argc >= 2)) {
      /*
       * output key file
       */
      okeyFile = argv[2];
      argc -= 2;
      argv += 2;
    } else if ((strcmp(argv[1],"-K") == 0) && (argc >= 2)) {
      /*
       * input key file
       */
      ikeyFile = argv[2];
      argc -= 2;
      argv += 2;
    } else if ((strcmp(argv[1],"-i") == 0) && (argc >= 2)) {
      inputFile = argv[2];
      argc -= 2;
      argv += 2;
    } else if ((strcmp(argv[1],"-o") == 0) && (argc >= 2)) {
      outFile = argv[2];
      argc -= 2;
      argv += 2;
    } else {
      break;
    }
  }

  /*
   * Read or generate the key and initialization vector
   */

  if (!ikeyFile && !okeyFile) goto err;

  if (ikeyFile) {
      int n;
      if ((ikeyFilePtr = fopen(ikeyFile, "rb")) == NULL) {
	fprintf(stderr, "Can't open input key file %s\n", ikeyFile);
	goto err;
      }
      if ((n = fread(key, 1, sizeof key, ikeyFilePtr)) > 0)
      	keyLength = n;
      else goto err;
      fclose(ikeyFilePtr);
  } else {
      keyLength = EVP_CIPHER_key_length(cipher);
      if (RAND_bytes(key, keyLength) <= 0) {
        fprintf(stderr, "Error generating key\n");
        goto err;
      }
  }

  if (!EVP_EncryptInit(&ctx, cipher, key, NULL)) {
    fprintf(stderr, "Error initializing cipher context\n");
    goto err;
  }

  /*
   * Now encrypt
   */
  if ((outFilePtr = fopen(outFile, "wb")) == NULL) {
    fprintf(stderr, "Can't open output file %s\n", outFile);
    goto err;
  }
  if ((inFilePtr = fopen(inputFile, "rb")) == NULL) {
    fprintf(stderr, "Can't open input file %s\n", inputFile);
    goto err;
  }
  
  while ((inLength = fread(inBuffer, 1, BUFFER_SIZE, inFilePtr)) > 0) {
      EVP_EncryptUpdate(&ctx, outBuffer, &outLength, inBuffer, inLength);
      fwrite(outBuffer, 1, outLength, outFilePtr);
  }
  EVP_EncryptFinal(&ctx, outBuffer, &outLength);
  fwrite(outBuffer, 1, outLength, outFilePtr);
  
  if (okeyFile) {
      if ((okeyFilePtr = fopen(okeyFile, "wb")) == NULL) {
	fprintf(stderr, "Can't open key file %s\n", okeyFile);
	goto err;
      }

      fwrite(key, 1, keyLength, okeyFilePtr);
  }

  exit(0);

 err:
  ERR_load_crypto_strings();
  ERR_print_errors_fp(stderr);
  exit(1);
}

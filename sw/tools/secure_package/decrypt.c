
/*
 * Application to decrypt a file using openssl symmetric encryption
 * algorithms.
 * The key used is from a separate file.
 * The decryption can be done in place, so output and input file are the same.
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <openssl/evp.h>
#include <openssl/rand.h>
#include <openssl/err.h>

#define BUFFER_SIZE 1024

int main(int argc, char *argv[]) {
  
  char *keyFile;
  char *inputFile;
  char *outFile = NULL;
  FILE *keyFilePtr = NULL;
  FILE *outFilePtr = NULL;
  FILE *inFilePtr = NULL;
  int outLength;
  int inLength;

  unsigned char key[EVP_MAX_KEY_LENGTH];
  int keyLength;
  unsigned char inBuffer[BUFFER_SIZE];
  unsigned char outBuffer[BUFFER_SIZE];
  int readPos, writePos;
  int totalLength = 0;

  EVP_CIPHER_CTX ctx;

  /*
   * Change cipher here
   */
  EVP_CIPHER *cipher;

#ifdef WEAK_CRYPTO
  cipher = EVP_des_cbc();
#else
  cipher = EVP_des_ede3_cbc();
#endif

  OpenSSL_add_all_algorithms();
  memset((void *) &ctx, 0, sizeof(ctx));

  if (argc < 5) {
    printf("Usage: decrypt -k <key_file> -i <input_file> [-o <output_file>]\n");
    exit(-1);
  }

  while(argc > 1) {
    if ((strcmp(argv[1],"-k") == 0) && (argc >= 2)) {
      /*
       * input key file
       */
      keyFile = argv[2];
      argc -= 2;
      argv += 2;
    } else if ((strcmp(argv[1],"-i") == 0) && (argc >= 2)) {
      inputFile = argv[2];
      argc -= 2;
      argv += 2;
    } else if ((strcmp(argv[1],"-o") == 0) && (argc >= 2)) {
      outFile = argv[2];
      argc -= 2;
      argv += 2;
    } else {
      break;
    }
  }

  keyLength = EVP_CIPHER_key_length(cipher);

  if ((keyFilePtr = fopen(keyFile, "rb")) == NULL) {
    fprintf(stderr, "Can't open key file %s\n", keyFile);
    goto err;
  }

  inLength = fread(key, 1, keyLength, keyFilePtr);
  if (inLength != keyLength) {
    fprintf(stderr, "Error reading key\n");
    goto err;
  }

  if (!EVP_DecryptInit(&ctx, cipher, key, NULL)) {
    fprintf(stderr, "Error initializing cipher context\n");
    goto err;
  }

  /*
   * Now Decrypt
   */
  if (outFile != NULL) {
    if ((outFilePtr = fopen(outFile, "w")) == NULL) {
      fprintf(stderr, "Can't open output file %s\n", outFile);
      goto err;
    }
  }

  if ((inFilePtr = fopen(inputFile, (outFile == NULL) ? "r+" : "r")) == NULL) {
    fprintf(stderr, "Can't open input file %s\n", inputFile);
    goto err;
  }

  writePos = 0;
  readPos = 0;
  for (;;) {
    fseek(inFilePtr, readPos, SEEK_SET);
    inLength = fread(inBuffer, 1, BUFFER_SIZE, inFilePtr);
    readPos += inLength;
    if (inLength < BUFFER_SIZE) {
      EVP_DecryptUpdate(&ctx, outBuffer, &outLength, inBuffer, inLength);
      if (outFilePtr != NULL) {
	fwrite(outBuffer, 1, outLength, outFilePtr);
      } else {
	fseek(inFilePtr, writePos, SEEK_SET);
	fwrite(outBuffer, 1, outLength, inFilePtr);
      }
      writePos += outLength;
      EVP_DecryptFinal(&ctx, outBuffer, &outLength);
      if (outFilePtr != NULL) {
	fwrite(outBuffer, 1, outLength, outFilePtr);
	fclose(outFilePtr);
      } else {
	fwrite(outBuffer, 1, outLength, inFilePtr);
	writePos += outLength;
	fclose(inFilePtr);
      }
      break;
    } else {
      EVP_DecryptUpdate(&ctx, outBuffer, &outLength, inBuffer, inLength);
      totalLength += outLength;
      if (outFilePtr != NULL) {
	fwrite(outBuffer, 1, outLength, outFilePtr);
      } else {
	fseek(inFilePtr, writePos, SEEK_SET);
	fwrite(outBuffer, 1, outLength, inFilePtr);
	writePos += outLength;
      }
    }
    
  }

  if (outFilePtr == NULL) {
    if (truncate(inputFile, writePos)) {
      goto err;
    }
  }

  exit(0);

 err:
  ERR_load_crypto_strings();
  ERR_print_errors_fp(stderr);
  exit(1);
}

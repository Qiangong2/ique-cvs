
/*
 * Application to verify the signature using a PKCS7 file.
 * The data file is always "detached", that is, separate from the PKCS7
 * file containing the signatures and certificates
 */

#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <openssl/bio.h>
#include <openssl/x509.h>
#include <openssl/x509v3.h>
#include <openssl/pkcs7.h>
#include <openssl/pem.h>
#include <openssl/err.h>

#define DEBUG 1

static int verify_callback(int ok, X509_STORE_CTX *ctx);

int main(int argc, char *argv[]) {
  X509 *x509 = NULL;
  PKCS7 *p7 = NULL;
  PKCS7_ISSUER_AND_SERIAL *ias = NULL;
  STACK_OF(X509) *certs = NULL;
  X509_STORE_CTX ctx;
  X509_STORE *certStore = NULL;

  BIO *in = NULL;
  BIO *data = NULL;
  BIO *p7bio = NULL;

  char buf[1024*4];
  char *rootFile = NULL;
  char *inputFile = NULL;
  char *dataFile = NULL;

  X509 *rootX509 = NULL;

  STACK_OF(PKCS7_SIGNER_INFO) *sk = NULL;
  PKCS7_SIGNER_INFO *si = NULL;
  struct stat statBuf;
  FILE *inFilePtr = NULL;

  int i;
  int length;
  unsigned char *berBuffer = NULL;

  if (argc < 7) {
    printf("Usage: verify -s <signature_file> -R <root_cert (PEM format)> -d <data_file>\n");
    exit(-1);
  }

  OpenSSL_add_all_algorithms();

  while(argc > 1) {
    if ((strcmp(argv[1],"-R") == 0) && (argc >= 2)) {
      /*
       * Trusted root certificate
       */
      rootFile = argv[2];
      argc-=2;
      argv+=2;
      if (!(in=BIO_new_file(rootFile, "r"))) {
	goto err;
      }
      if (!(rootX509=PEM_read_bio_X509(in, NULL, NULL, NULL))) {
	goto err;
      }
      BIO_free(in);
    } else if ((strcmp(argv[1],"-s") == 0) && (argc >= 2)) {
      inputFile = argv[2];
      argc -= 2;
      argv += 2;
    } else if ((strcmp(argv[1],"-d") == 0) && (argc >= 2)) {
      dataFile = argv[2];
      argc -= 2;
      argv += 2;
    } else {
      break;
    }
  }

  /*
   * Load PKCS7 object from a file
   */
  if (stat(inputFile, &statBuf) < 0) {
    fprintf(stderr, "Can't stat input file %s\n", inputFile);
    goto err;
  }
  length = statBuf.st_size;

  if ((berBuffer = (unsigned char *) malloc(length)) == NULL) {
    goto err;
  }

  if ((inFilePtr = fopen(inputFile, "r")) == 0) {
    fprintf(stderr, "Can't open input file %s\n", inputFile);
    goto err;
  }
  fread(berBuffer, 1, length, inFilePtr);

  if ((p7 = d2i_PKCS7(NULL, &berBuffer, length)) == NULL) {
    goto err;
  }

  if (!(data=BIO_new_file(dataFile, "r"))) {
    fprintf(stderr, "Can't open data file %s\n", dataFile);
    goto err;
  }

  p7bio=PKCS7_dataInit(p7, data);

  if (p7bio == NULL) {
    fprintf(stderr, "Can't decode PKCS7 file\n");
    goto err;
  }

  /* 
   * We now have to 'read' from p7bio to calculate digests, decrypt etc. 
   */
  for (;;) {
    i=BIO_read(p7bio, buf, sizeof(buf));
    if (i <= 0) {
      break;
    }
  }

  /* 
   * Verify signatures 
   */
  if (PKCS7_type_is_signed(p7)) {
    certs = p7->d.sign->cert;
  } else {
    fprintf(stderr, "Type is not signed\n");
    goto err;
  }

  /*
   * Put the ROOT certificate on the certificate stack
   */
  sk_X509_push(certs, rootX509);

  sk=PKCS7_get_signer_info(p7);
  if (sk == NULL) {
    fprintf(stderr, "there are no signatures\n");
    goto err;
  } else {
    /*
     * Should only be one signer, but loop anyway
     */
    for (i=0; i<sk_PKCS7_SIGNER_INFO_num(sk); i++) {
      si = sk_PKCS7_SIGNER_INFO_value(sk,i);
      ias = si->issuer_and_serial;
      x509 = X509_find_by_issuer_and_serial(certs, ias->issuer, ias->serial);
      i = PKCS7_signatureVerify(p7bio, p7, si, x509);
      if (i<=0) {
	fprintf(stderr, "Signature did not verify\n");
	goto err;
      } 

      /*
       * Now I need to verify the certificate itself against
       * the Root certificate passed in on the command line and whatever
       * other certificates are given in the package
       */
      certStore=X509_STORE_new();
      X509_STORE_set_verify_cb_func(certStore,verify_callback); 

      /*
       * Add self-signed root to store
       */
      X509_STORE_add_cert(certStore, rootX509);
      ERR_clear_error();

      X509_STORE_CTX_init(&ctx, certStore, x509, certs);

      i=X509_verify_cert(&ctx);
      if (i <= 0) {
	fprintf(stderr, "Error verifying certificate chain\n");
	X509_STORE_CTX_cleanup(&ctx);
	goto err;
      }
      X509_STORE_CTX_cleanup(&ctx);

    }
  }

  exit(0);

 err:
  ERR_load_crypto_strings();
  ERR_print_errors_fp(stderr);
  exit(1);
}

static int verify_callback(int ok, X509_STORE_CTX *ctx)
{
  char buf[256];
  char cn[256];
  X509 *err_cert;
  int err,depth;
  char *signer0 = "signer";
  char *signer1 = "Software Signer";
  
  err_cert = X509_STORE_CTX_get_current_cert(ctx);
  err = X509_STORE_CTX_get_error(ctx);
  depth = X509_STORE_CTX_get_error_depth(ctx);

  X509_NAME_oneline(X509_get_subject_name(err_cert), buf, 256);
#ifdef DEBUG
  fprintf(stderr,"depth=%d %s CA_check=%d\n", depth, buf, 
	  (int)(err_cert->ex_flags & EXFLAG_CA));
#endif

  if (!ok) {
    fprintf(stderr,"verify error:num=%d:%s\n",err,
	       X509_verify_cert_error_string(err));
    if (depth < 6) {
      ok=1;
      X509_STORE_CTX_set_error(ctx,X509_V_OK);
    } else {
      ok=0;
      X509_STORE_CTX_set_error(ctx,X509_V_ERR_CERT_CHAIN_TOO_LONG);
    }
  }

  if (depth > 0) {
    if ((err_cert->ex_flags & EXFLAG_CA) == 0) {
      /*
       * This is not a CA certificate so we should deny
       */
#ifdef DEBUG
      fprintf(stderr, "Not a CA!\n");
#endif
      ok = 0;
      X509_STORE_CTX_set_error(ctx, X509_V_ERR_INVALID_CA);
    }
  } else {
    X509_NAME_get_text_by_NID(X509_get_subject_name(err_cert), NID_commonName, 
			      cn, 256); 
    if (strncmp(signer0, cn, strlen(signer0)) &&
    	strncmp(signer1, cn, strlen(signer1))) {
#ifdef DEBUG
      fprintf(stderr, "Not a signer!\n");
#endif
      ok = 0;
      X509_STORE_CTX_set_error(ctx, X509_V_ERR_APPLICATION_VERIFICATION);
    }
    
  }

  switch (ctx->error) {
    case X509_V_ERR_UNABLE_TO_GET_ISSUER_CERT:
      X509_NAME_oneline(X509_get_issuer_name(ctx->current_cert), buf, 256);
#ifdef DEBUG
      fprintf(stderr,"issuer= %s\n",buf);
#endif
      break;
    case X509_V_ERR_CERT_NOT_YET_VALID:
    case X509_V_ERR_ERROR_IN_CERT_NOT_BEFORE_FIELD:
#ifdef DEBUG
      fprintf(stderr,"Not yet valid\n");
#endif
      break;
    case X509_V_ERR_CERT_HAS_EXPIRED:
    case X509_V_ERR_ERROR_IN_CERT_NOT_AFTER_FIELD:
#ifdef DEBUG
      fprintf(stderr,"Expired\n");
#endif
      break;
  }
#ifdef DEBUG
  fprintf(stderr,"verify return: %d\n", ok);
#endif
  return(ok);
}

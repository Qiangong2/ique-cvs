
/*
 * Application to generate a secure package from a set of certificates, private
 * key and input data. Output format is DER encoded PKCS7
 */

#include <stdio.h>
#include <string.h>
#include <openssl/bio.h>
#include <openssl/x509.h>
#include <openssl/pkcs7.h>
#include <openssl/pem.h>
#include <openssl/err.h>

int main(int argc, char *argv[]) {

  X509 *x509 = NULL;
  PKCS7 *p7 = NULL;

  BIO *in;
  BIO *data;
  BIO *p7bio;

  char buf[1024*4];
  char *recipientFile = NULL;
  char *signerFile = NULL;
  char *certFile = NULL;
  char *inputFile = NULL;
  char *outFile = NULL;

  STACK_OF(X509) *recipients = NULL;
  STACK_OF(X509) *extraCerts = NULL;

  EVP_PKEY *signerKey = NULL;

  int i;
  int length;
  unsigned char *berBuffer = NULL;
  unsigned char *p;
  FILE *outFilePtr;

  OpenSSL_add_all_algorithms();
  
  if (argc < 9) {
    printf("Usage: secure_pack -s <key_file> -r <recipient_cert> -i <input_file> -o <output_file> [-c <extra_cert>]\n");
    exit(-1);
  }

  OpenSSL_add_all_algorithms();

  while(argc > 1) {
    if ((strcmp(argv[1],"-r") == 0) && (argc >= 2)) {
      /*
       * Add a recipient - can be many of these
       */
      recipientFile = argv[2];
      argc-=2;
      argv+=2;
      if (!(in=BIO_new_file(recipientFile, "r"))) {
	goto err;
      }
      if (!(x509=PEM_read_bio_X509(in, NULL, NULL, (void *) NULL))) {
	goto err;
      }

      if(!recipients) {
	recipients = sk_X509_new_null();
      }
      sk_X509_push(recipients, x509);
      BIO_free(in);
    } else if ((strcmp(argv[1],"-s") == 0) && (argc >= 2)) {
      /*
       * Signer certificate and private key - only 1 of these
       */
      signerFile = argv[2];
      argc -= 2;
      argv += 2;

    } else if ((strcmp(argv[1],"-c") == 0) && (argc >= 2)) {
      /*
       * Extra certificates, aside from the signer - can be as many as you want.
       * These are used to provide a chain to a trusted CA (which is HR root CA)
       */
      certFile = argv[2];
      argc -= 2;
      argv += 2;
      if (!(in=BIO_new_file(certFile, "r"))) {
	goto err;
      }

      if (!(x509=PEM_read_bio_X509(in, NULL, NULL, NULL))) {
	goto err;
      }
      if(!extraCerts) {
	extraCerts = sk_X509_new_null();
      }
      sk_X509_push(extraCerts, x509);
      BIO_free(in);
    } else if ((strcmp(argv[1],"-i") == 0) && (argc >= 2)) {
      inputFile = argv[2];
      argc -= 2;
      argv += 2;
    } else if ((strcmp(argv[1],"-o") == 0) && (argc >= 2)) {
      outFile = argv[2];
      argc -= 2;
      argv += 2;
    } else {
      break;
    }
  }

  /*
   * Get the signer information
   */
  if (!(in=BIO_new_file(signerFile, "r"))) {
    goto err;
  }
  if ((signerKey=PEM_read_bio_PrivateKey(in, NULL, NULL, NULL)) == NULL) {
    goto err;
  }
  BIO_reset(in);

  if (!(x509=PEM_read_bio_X509(in, NULL, NULL, NULL))) {
    goto err;
  }
  BIO_free(in);

  /*
   * Now build the internal PKCS7 structure
   */
  p7 = PKCS7_new();
  PKCS7_set_type(p7, NID_pkcs7_signedAndEnveloped);
  if (PKCS7_add_signature(p7, x509, signerKey, EVP_sha1()) == NULL) {
    goto err;
  }

  /*
   * Add the signers certificate
   */
  PKCS7_add_certificate(p7, x509);

  /*
   * Add the extra certificates
   */
  if (extraCerts != NULL) {
    for(i=0; i<sk_X509_num(extraCerts); i++) {
      if (!PKCS7_add_certificate(p7, sk_X509_value(extraCerts, i))) {
	goto err;
      }
    }
    sk_X509_pop_free(extraCerts, X509_free);
  }

  /*
   * Using triple DES for now
   */
  if (!PKCS7_set_cipher(p7, EVP_des_ede3_cbc())) {
    goto err;
  }

  /*
   * Add all the recipient information, their certificates
   */
  for(i = 0; i < sk_X509_num(recipients); i++) {
    if (!PKCS7_add_recipient(p7, sk_X509_value(recipients, i))) {
      goto err;
    }
  }
  sk_X509_pop_free(recipients, X509_free);

  if (!(data=BIO_new_file(inputFile, "r"))) {
    goto err;
  }

  /*
   * Start of data processing - actually does the encryption and signing
   */
  if ((p7bio=PKCS7_dataInit(p7, NULL)) == NULL) {
    goto err;
  }

  for (;;) {
    i=BIO_read(data, buf, sizeof(buf));
    if (i <= 0) {
      break;
    }
    BIO_write(p7bio, buf, i);
  }
  BIO_flush(p7bio);
  if (!PKCS7_dataFinal(p7, p7bio)) {
    goto err;
  }
  BIO_free(p7bio);

  /*
   * Dump the output file
   */
  if ((length = i2d_PKCS7(p7, NULL)) == 0) {
    goto err;
  }

  if ((berBuffer = (unsigned char *) malloc(length)) == NULL) {
    goto err;
  }

  p = berBuffer;
  i2d_PKCS7(p7, &p);
  if ((outFilePtr = fopen(outFile, "wb")) == NULL) {
    goto err;
  }
  i = fwrite(berBuffer, 1, length, outFilePtr);

  fclose(outFilePtr);
  free(berBuffer);
  PKCS7_free(p7);

  exit(0);

 err:
  ERR_load_crypto_strings();
  ERR_print_errors_fp(stderr);
  exit(1);
}

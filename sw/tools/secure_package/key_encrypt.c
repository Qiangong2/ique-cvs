
/*
 * Application to encrypt a key for a set of recipients. Uses PKCS7.
 */

#include <stdio.h>
#include <string.h>
#include <openssl/bio.h>
#include <openssl/x509.h>
#include <openssl/pkcs7.h>
#include <openssl/pem.h>
#include <openssl/err.h>

int main(int argc, char *argv[]) {

  X509 *x509 = NULL;
  PKCS7 *p7 = NULL;

  BIO *in = NULL;
  BIO *data = NULL;
  BIO *p7bio = NULL;

  char buf[1024*4];
  char *recipientFile = NULL;
  char *inputFile = NULL;
  char *outFile = NULL;

  STACK_OF(X509) *recipients = NULL;

  int i;
  int length;
  unsigned char *berBuffer = NULL;
  unsigned char *p;
  FILE *outFilePtr;

  OpenSSL_add_all_algorithms();
  
  if (argc < 7) {
    printf("Usage: key_encrypt -r <recipient_cert (PEM format)> -i <input_file> -o <output_file>\n");
    exit(-1);
  }

  OpenSSL_add_all_algorithms();

  while(argc > 1) {
    if ((strcmp(argv[1],"-r") == 0) && (argc >= 2)) {
      /*
       * Add a recipient - can be many of these
       */
      recipientFile = argv[2];
      argc-=2;
      argv+=2;
      if (!(in=BIO_new_file(recipientFile, "r"))) {
	goto err;
      }
      if (!(x509=PEM_read_bio_X509(in, NULL, NULL, (void *) NULL))) {
	goto err;
      }

      if(!recipients) {
	recipients = sk_X509_new_null();
      }
      sk_X509_push(recipients, x509);
      BIO_free(in);
    } else if ((strcmp(argv[1],"-i") == 0) && (argc >= 2)) {
      inputFile = argv[2];
      argc -= 2;
      argv += 2;
    } else if ((strcmp(argv[1],"-o") == 0) && (argc >= 2)) {
      outFile = argv[2];
      argc -= 2;
      argv += 2;
    } else {
      break;
    }
  }

  /*
   * Now build the internal PKCS7 structure
   */
  p7 = PKCS7_new();
  PKCS7_set_type(p7, NID_pkcs7_enveloped);

  /*
   * Using blowfish for now
   */
  if (!PKCS7_set_cipher(p7, EVP_des_ede3_cbc())) {
    goto err;
  }

  /*
   * Add all the recipient information, their certificates
   */
  for(i = 0; i < sk_X509_num(recipients); i++) {
    if (!PKCS7_add_recipient(p7, sk_X509_value(recipients, i))) {
      goto err;
    }
  }
  sk_X509_pop_free(recipients, X509_free);

  if (!(data=BIO_new_file(inputFile, "r"))) {
    goto err;
  }

  /*
   * Start of data processing - actually does the encryption
   */
  if ((p7bio=PKCS7_dataInit(p7, NULL)) == NULL) {
    goto err;
  }

  for (;;) {
    i=BIO_read(data, buf, sizeof(buf));
    if (i <= 0) {
      break;
    }
    BIO_write(p7bio, buf, i);
  }
  BIO_flush(p7bio);
  if (!PKCS7_dataFinal(p7, p7bio)) {
    goto err;
  }
  BIO_free(p7bio);

  /*
   * Dump the output file
   */
  if ((length = i2d_PKCS7(p7, NULL)) == 0) {
    goto err;
  }

  if ((berBuffer = (unsigned char *) malloc(length)) == NULL) {
    goto err;
  }

  p = berBuffer;
  i2d_PKCS7(p7, &p);
  if ((outFilePtr = fopen(outFile, "wb")) == NULL) {
    goto err;
  }
  i = fwrite(berBuffer, 1, length, outFilePtr);

  fclose(outFilePtr);
  free(berBuffer);
  PKCS7_free(p7);

  exit(0);

 err:
  ERR_load_crypto_strings();
  ERR_print_errors_fp(stderr);
  exit(1);
}

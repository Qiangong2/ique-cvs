
/*
 * Application to unpack a secure package
 */

#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <openssl/bio.h>
#include <openssl/x509.h>
#include <openssl/pkcs7.h>
#include <openssl/pem.h>
#include <openssl/err.h>

int main(int argc, char *argv[]) {

  X509 *x509 = NULL;
  PKCS7 *p7 = NULL;
  PKCS7_ISSUER_AND_SERIAL *ias = NULL;
  STACK_OF(X509) *certs = NULL;

  BIO *in;
  BIO *p7bio;

  char buf[1024*4];
  char *keyFile = NULL;
  char *recipientFile = NULL;
  char *rootFile = NULL;
  char *inputFile = NULL;
  char *outFile = NULL;

  X509 *rootX509 = NULL;

  EVP_PKEY *recipientKey = NULL;
  X509 *recipientCert = NULL;

  STACK_OF(PKCS7_SIGNER_INFO) *sk = NULL;
  PKCS7_SIGNER_INFO *si = NULL;
  struct stat statBuf;
  FILE *inFilePtr;
  FILE *outFilePtr;

  int i;
  int length;
  unsigned char *berBuffer;

  OpenSSL_add_all_algorithms();
  
  if (argc < 9) {
    printf("Usage: secure_unpack -k <key_file> -r <recipient_cert> -R <root_cert> -i <input_file> -o <output_file>\n");
    exit(-1);
  }

  OpenSSL_add_all_algorithms();

  while(argc > 1) {
    if ((strcmp(argv[1],"-R") == 0) && (argc >= 2)) {
      /*
       * Trusted root certificate
       */
      rootFile = argv[2];
      argc-=2;
      argv+=2;
      if (!(in=BIO_new_file(rootFile, "r"))) {
	goto err;
      }
      if (!(rootX509=PEM_read_bio_X509(in, NULL, NULL, NULL))) {
	goto err;
      }
      BIO_free(in);
    } else if ((strcmp(argv[1],"-k") == 0) && (argc >= 2)) {
      /*
       * Recipient private key - only 1 of these
       */
      keyFile = argv[2];
      argc -= 2;
      argv += 2;
    } else if ((strcmp(argv[1],"-r") == 0) && (argc >= 2)) {
      /*
       * Recipient certificate - only 1
       */
      recipientFile = argv[2];
      argc -= 2;
      argv += 2;

    } else if ((strcmp(argv[1],"-i") == 0) && (argc >= 2)) {
      inputFile = argv[2];
      argc -= 2;
      argv += 2;
    } else if ((strcmp(argv[1],"-o") == 0) && (argc >= 2)) {
      outFile = argv[2];
      argc -= 2;
      argv += 2;
    } else {
      break;
    }
  }

  /*
   * Get the recipient private and certificate
   */
  if (!(in=BIO_new_file(keyFile, "r"))) {
    goto err;
  }
  if ((recipientKey=PEM_read_bio_PrivateKey(in, NULL, NULL, NULL)) == NULL) {
    goto err;
  }
  BIO_free(in);

  if (!(in=BIO_new_file(recipientFile, "r"))) {
    goto err;
  }
  if (!(recipientCert=PEM_read_bio_X509(in, NULL, NULL, NULL))) {
    goto err;
  }
  BIO_free(in);

  /*
   * Load PKCS7 object from a file
   */
  if (stat(inputFile, &statBuf) < 0) {
    fprintf(stderr, "Can't stat input file %s\n", inputFile);
    goto err;
  }
  length = statBuf.st_size;

  if ((berBuffer = (unsigned char *) malloc(length)) == NULL) {
    goto err;
  }

  if ((inFilePtr = fopen(inputFile, "r")) == 0) {
    fprintf(stderr, "Can't open input file %s\n", inputFile);
    goto err;
  }
  fread(berBuffer, 1, length, inFilePtr);

  if ((p7 = d2i_PKCS7(NULL, &berBuffer, length)) == NULL) {
    goto err;
  }

  if ((outFilePtr = fopen(outFile, "w")) == 0) {
    fprintf(stderr, "Can't open output file %s\n", outFile);
    goto err;
  }

  p7bio=PKCS7_dataDecode(p7, recipientKey, 0, recipientCert);
  if (p7bio == NULL) {
    fprintf(stderr, "Can't decode PKCS7 file\n");
    goto err;
  }

  /* 
   * We now have to 'read' from p7bio to calculate digests, decrypt etc. 
   */
  for (;;) {
    i=BIO_read(p7bio, buf, sizeof(buf));
    /* print it? */
    if (i <= 0) {
      break;
    }
    fwrite(buf, 1, i, outFilePtr);
  }

  /* 
   * Verify signatures 
   */
  if (PKCS7_type_is_signedAndEnveloped(p7)) {
    certs = p7->d.signed_and_enveloped->cert;
  } else {
    fprintf(stderr, "Type is not signed and enveloped\n");
    goto err;
  }

  sk=PKCS7_get_signer_info(p7);
  if (sk == NULL) {
    fprintf(stderr, "there are no signatures on this data\n");
  } else {
    /*
     * Should only be one signer, but loop anyway
     */
    for (i=0; i<sk_PKCS7_SIGNER_INFO_num(sk); i++) {
      si = sk_PKCS7_SIGNER_INFO_value(sk,i);
      ias = si->issuer_and_serial;
      x509 = X509_find_by_issuer_and_serial(certs, ias->issuer, ias->serial);
      i=PKCS7_signatureVerify(p7bio, p7, si, x509);
      if (i<=0) {
	goto err;
      } 

      /*
       * XXX Now I need to verify the certificate itself against
       * the Root certificate passed in on the command line and whatever
       * other certificates are given in the package
       */
    }
  }
  
  exit(0);

 err:
  ERR_load_crypto_strings();
  ERR_print_errors_fp(stderr);
  exit(1);
}

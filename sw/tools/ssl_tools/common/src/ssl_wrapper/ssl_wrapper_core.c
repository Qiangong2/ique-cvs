#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <openssl/ssl.h>
#include <openssl/rand.h>

#include "ssl_wrapper.h"

#ifdef TRACE_SSL
static void
ssl_info_callback (SSL* s, int where, int ret)
{
    const char *str;
    int w;

    w=where& ~SSL_ST_MASK;

    if (w & SSL_ST_CONNECT) str="SSL_connect";
    else if (w & SSL_ST_ACCEPT) str="SSL_accept";
    else str="undefined";

    if (where & SSL_CB_EXIT) {
	if (ret == 0)
	    fprintf(stderr,"%s:failed in %s\n", str,SSL_state_string_long(s));
	else if (ret < 0) {
	    fprintf(stderr,"%s:error in %s\n",
		    str,SSL_state_string_long(s));
	}
    }
}
#endif /* TRACE_SSL */


SSL_CTX*
ssl_get_context (const char* cert_file, const char* ca_chain,
		 const char* key_file, const char* key_pw, const char* CA_file)
{
    SSL_CTX* ctx;

    /* seed the random number generator */
    if (RAND_status() != 1) {
	RAND_load_file("/dev/urandom", 512);
    }

    /* SSL initialization */

    OpenSSL_add_ssl_algorithms();
    ctx = SSL_CTX_new(SSLv23_method());
    if (ctx == 0)
	return 0;
    
    SSL_CTX_set_options(ctx, SSL_OP_NO_SSLv2);

    /* Limit the cipher list */
#ifdef WEAK_CRYPTO
    SSL_CTX_set_cipher_list(ssl_wrapper->ctx, "LOW");
#endif

#ifdef TRACE_SSL
    SSL_CTX_set_info_callback(ctx, (void (*) ())ssl_info_callback);
#endif

    /* set up trusted sites */
    if (CA_file) {
	if (! SSL_CTX_load_verify_locations (ctx, CA_file, 0) ||
	    ! (SSL_CTX_set_default_verify_paths (ctx))) {

	    SSL_CTX_free (ctx);
	    return 0;
	}
    }
    
    /* set up private key and cert */
    if (cert_file || key_file) {
	int cert_type = SSL_FILETYPE_ASN1;
	int key_type = SSL_FILETYPE_ASN1;
	if (cert_file == 0 || key_file == 0) {
	    SSL_CTX_free (ctx);
	    return 0;
	}

	/* store the password to be used by password_callback */
	SSL_CTX_set_default_passwd_cb_userdata (ctx, (void*) key_pw);

	if (strstr(cert_file, ".pem")) cert_type = SSL_FILETYPE_PEM;
	if (strstr(key_file, ".pem")) key_type = SSL_FILETYPE_PEM;
	if ((SSL_CTX_use_certificate_file (ctx, cert_file, cert_type) <= 0) ||
	    (SSL_CTX_use_RSAPrivateKey_file (ctx, key_file, key_type) <= 0) ||
	    ! SSL_CTX_check_private_key (ctx)) {

	    SSL_CTX_free (ctx);
	    return 0;
	}
    }

    /* add additional CA chain */
    if (ca_chain) {
	/* we ignore the return code from add_chain_cert because some of
	   the old units do not have a manufacturer cert.  So if their
	   swupd would break if we kick them out here.  We leave it up to
	   the server to reject such connections */

	ssl_add_chain_cert (ctx, ca_chain, 1);
    }

    return ctx;
} /* ssl_get_context */


void
ssl_remove_context (SSL_CTX* ctx)
{
    if (ctx != 0)
	SSL_CTX_free (ctx);
} /* ssl_remove_context */

int
ssl_add_chain_cert (SSL_CTX* ctx, const char* certfile, int use_PEM)
{
    if (! use_PEM)
	return 0;			/* not yet implemented */

    return SSL_CTX_load_verify_locations (ctx, certfile, 0);
    
} /* ssl_add_chain_cert */


/* get a new socket with common attributes set */
static int
get_socket (void)
{
    int nodelay = 1;
    struct linger l = {1, 30};

    int fd = socket (PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (fd == -1)
	return -1;

    if (setsockopt (fd, SOL_SOCKET, SO_LINGER, (const char*) &l,
		    sizeof(struct linger)) == -1) { 
           close(fd);
	   return -1;
    }

    if (setsockopt (fd,  IPPROTO_TCP, TCP_NODELAY, &nodelay, sizeof(nodelay)) == -1) {
	close(fd);
	return -1;
    }

    return fd;

} /* get_socket */


/* (Non-SSL) TCP connection */
static int
tcp_connect (const char* host, short port, struct sockaddr_in *sin)
{
    int socket_fd = -1;
    struct hostent hostname;
    char buf[1024];
    struct sockaddr_in host_addr;
    int i;
    int r;

    /* normal TCP stuff */
    if (host == 0)
	return -1;
    else {
	struct hostent *entlist;
	int my_errno;
#ifdef SOLARIS
	if (!(entlist = gethostbyname_r (host, &hostname, buf, 1024, &my_errno)))
#else
	if (gethostbyname_r (host, &hostname, buf, 1024, &entlist,
			     &my_errno) != 0)
#endif
	    return -1;
    }

    host_addr.sin_family = AF_INET;
    host_addr.sin_port = htons(port);

    /* Attempt to connect() to each of the IP-address aliases listed
     * by gethostbyname(), until we get a TCP connection.
     * This adds reliability, because if one of
     * the host's IP addresses happens to be out of service at this
     * moment, we will continue to try until we find one that works.
     * It also facilitates load-balancing:  the upload/download server 
     * name can be mapped by DNS to multiple machines, and DNS-Round 
     * Robin will return the addresses in a different order at each 
     * request.  
     * Note: we still use gethostbyname_r() so this code retains its
     * thread-safe-ness properties.
     */
    for (i=0; hostname.h_addr_list[i] != NULL; i++)  {

	socket_fd = get_socket();
	if (socket_fd == -1)
	    continue;

	(void) memcpy (&host_addr.sin_addr,
                     hostname.h_addr_list[i],
                     hostname.h_length);

	if (sin) {
	    r = bind(socket_fd, (struct sockaddr *)sin, sizeof(struct sockaddr_in));
	    if (r != 0) {
		fprintf(stderr, "bind: returns %d\n", r);
		return -1;
	    }
	}
	
	if (connect (socket_fd, (struct sockaddr*) &host_addr,
		     sizeof(host_addr)) == -1) {
	    close(socket_fd);
	    socket_fd = -1;
	    continue;
        } else 
	    break;
    }

    return socket_fd;
} /* tcp_connect */


SSL*
ssl_connect (SSL_CTX* ctx, SSL* ssl, const char* host, short port, struct sockaddr_in *sin)
{
    int socket_fd;
    int need_new_ssl = (ssl == 0);

    if (ctx == 0 || host == 0)
	return 0;

    if (need_new_ssl) {
	ssl = SSL_new(ctx);
	if (ssl == 0)
	    return 0;
    } 

    /* client always authenticate the server */
    SSL_set_verify(ssl, SSL_VERIFY_PEER, 0);

    socket_fd = tcp_connect(host, port, sin);
    if (socket_fd < 0) {
	if (need_new_ssl)
	    SSL_free(ssl);
	return 0;
    }

    SSL_set_fd (ssl, socket_fd);
    if (SSL_connect (ssl) != 1) {
	close (socket_fd);
	if (need_new_ssl)
	    SSL_free(ssl);
	return 0;
    }

    return ssl;
} /* ssl_connect */


int
ssl_get_server_socket (short port)
{
    struct sockaddr_in my_addr;

    int serverSocket = get_socket();
    if (serverSocket == -1)
	return -1;

    {
	int on = 1;
	if (setsockopt (serverSocket, SOL_SOCKET, SO_REUSEADDR,
			(const char*) &on, sizeof(on)) == -1) {
	    close(serverSocket);
	    return -1;
	}
    }

    my_addr.sin_family = AF_INET;	// host byte order
    my_addr.sin_port = htons (port);	// network byte order
    my_addr.sin_addr.s_addr = INADDR_ANY; // auto-fill my IP
    bzero(&(my_addr.sin_zero), 8);

    if (bind (serverSocket, (struct sockaddr*) &my_addr,
	      sizeof(struct sockaddr)) == -1) {
	close(serverSocket);
	return -1;
    }

    if (listen (serverSocket, 5) == -1) {
	close(serverSocket);
	return -1;
    }
    return serverSocket;
} /* ssl_get_server_socket */
    

SSL*
ssl_accept (SSL_CTX* ctx, int serverSocket, int needClientAuth)
{
    struct sockaddr_in their_addr;
    socklen_t sin_size = sizeof(struct sockaddr_in);
    int sock_fd;
    SSL* ssl;

    if (ctx == 0 || serverSocket < 0)
	return 0;

    sock_fd = accept (serverSocket, (struct sockaddr*)&their_addr, &sin_size);
    if (sock_fd < 0)
	return 0;

    ssl = SSL_new(ctx);
    if (ssl == 0) {
	close(sock_fd);
	return 0;
    }
    SSL_set_verify(ssl,
		   (needClientAuth ?
		    (SSL_VERIFY_PEER|SSL_VERIFY_FAIL_IF_NO_PEER_CERT) :
		    SSL_VERIFY_NONE),
		   0);

    SSL_set_fd (ssl, sock_fd);
    if (SSL_accept(ssl) < 0) {
	SSL_free(ssl);
	close(sock_fd);
	return 0;
    }

    return ssl;
} /* ssl_accept */


int
ssl_read (SSL* ssl, void* buf, int len)
{
    if (ssl == 0 || buf == 0)
	return -1;
    return SSL_read(ssl, buf, len);
} /* ssl_read */


int
ssl_write (SSL* ssl, const void* buf, int len)
{
    int count = 0;
    const char* p = (char*)buf;

    if (ssl == 0 || buf == 0)
	return -1;

    while (count < len) {
	int n = SSL_write(ssl, p, len - count);
	if (n < 0)
	    return -1;
	else if (n == 0)
	    break;
	count += n;
	p += n;
    }
    return count;
} /* ssl_write */


void
ssl_close (SSL* ssl)
{
    int fd;

    if (ssl == 0)
	return;
    fd = SSL_get_fd(ssl);
    SSL_shutdown (ssl);
    close(fd);
    SSL_free(ssl);
    
} /* ssl_close */
    

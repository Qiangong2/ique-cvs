#!/bin/sh
#
# To update a release from a build.  If a package in the release directory is removed, then it will
# promote the package from build directory to release.  It will in turn update distributions that
# are based on this release (see bottom section of this script).
#
# Parameter src_tree is needed only for creating the install directory in the release.  Similar to 
# packages, if there is changes in install that needs to be promoted to a release, simply remove
# the install directory in the release.
#
if [ "$2" = "" ]; then
    echo "usage: $0 src_tree build_dir release_dir"
    echo "e.g. $0 /home/build/bcc-2.0 /home/build/sysimage-2.0/latest /home/build/sysimage-2.0/cur_release"
    echo "NOTE: if release_dir is a symlink, don't include trailing slash"
    exit 1
fi
BUILD_TOOLS=/opt/buildtools/bin
SRC_TREE=$1
BUILD_DIR=$2
REL_DIR=$3
CHANGED=0
#
# Full release modules (include all for now, but should eliminate obsoleted modules)
#
###FULL_REL_MODULES="bbdepot bbserver-setup bccs bin bms buildtools bus cas ccs cds cert cls competitions cps ebus eCard ecs ems etc ets gatewayos gtk gwos-mgmt hsm httpd ias idcmon iqahc javalib jikes jre kmodules lb netutil nfast nfast-pci oci ogs ogs_vn openssl ops osc oss pas postgres pubcli serveros smartcard svcdrv sysconfig sysutil tarball tomcat tunnel video wslib xs"
FULL_REL_MODULES="bms bus ccs cds cert cls cps ecs ets hsm httpd ias javalib jikes jre kmodules lb netutil nfast nfast-pci nus oci ogs ogs_vn openssl ops osc oss pas pcis svcdrv sysconfig sysutil tomcat wslib xs wstest ccsimu"

if [ ! -d $BUILD_DIR ]; then
    echo "$BUILD_DIR not found"
    exit 1
fi
if [ ! -d $REL_DIR ]; then
    echo "$REL_DIR not found"
    exit 1
fi
if [ -L $REL_DIR ]; then
    CUR_REL=`ls -l $REL_DIR | sed "s/.* -> //"`
else
    CUR_REL=`basename $REL_DIR`
fi
echo "======================================================"
echo `date`
echo "Update Release $CUR_REL from $BUILD_DIR"
echo -e "Packages: $FULL_REL_MODULES\n"
$BUILD_TOOLS/update_release $SRC_TREE $BUILD_DIR $REL_DIR "$FULL_REL_MODULES"

#
# Make IQUE distribution on /opt/repository_lab1
#
echo "------------------------------------------------------"
DIST_IQUE="bms bus ccs cds cert cls cps ecs ets hsm httpd ias javalib jikes jre kmodules lb netutil nfast nus oci ogs ogs_vn openssl ops osc oss pas pcis svcdrv sysconfig sysutil tomcat wslib xs ccsimu"
DIST_IQUE_DIR=/opt/repository_lab1/sw/${CUR_REL}-IQUE
echo "Make distribution $DIST_IQUE_DIR"
echo -e "Packages: $DIST_IQUE\n"
mkdir -p $DIST_IQUE_DIR
$BUILD_TOOLS/make_dist $REL_DIR $DIST_IQUE_DIR "$DIST_IQUE"
if [ $? -eq 2 ]; then
    $BUILD_TOOLS/update_servers bbu.lab1.routefree.com /opt/repository_lab1
    CHANGED=2
fi

#
# Make NOA distribution
#
echo "------------------------------------------------------"
DIST_NOA="bms ccs cert cls cps ecs ets httpd ias javalib jikes jre kmodules lb netutil nfast-pci nus oci openssl oss pas pcis svcdrv sysutil tomcat wslib ccsimu"

#
# /opt/repository_lab2
#
DIST_LAB2_DIR=/opt/repository_lab2/sw/${CUR_REL}-NOA
echo "Make distribution $DIST_LAB2_DIR (lab2)"
echo -e "Packages: $DIST_NOA\n"
mkdir -p $DIST_LAB2_DIR
$BUILD_TOOLS/make_dist $REL_DIR $DIST_LAB2_DIR "$DIST_NOA"
#if [ $? -eq 2 ]; then
#    $BUILD_TOOLS/update_servers lab2.routefree.com /opt/repository_lab2
#    CHANGED=2
#fi

#
# /opt/repository_lab3
#
echo "------------------------------------------------------"
DIST_LAB3_DIR=/opt/repository_lab3/sw/${CUR_REL}-NOA
echo "Make distribution $DIST_LAB3_DIR (lab3)"
echo -e "Packages: $DIST_NOA\n"
mkdir -p $DIST_LAB3_DIR
$BUILD_TOOLS/make_dist $REL_DIR $DIST_LAB3_DIR "$DIST_NOA"
if [ $? -eq 2 ]; then
    $BUILD_TOOLS/update_servers lab3.routefree.com /opt/repository_lab3
    CHANGED=2
fi
exit $CHANGED

/*
 * command line command to post new software release description
 * to the big brain over HTTP over SSL.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include "httplib.h"

#define URL_OPTION	"-url"
#define CERT_OPTION	"-cert"
#define CA_OPTION	"-ca"
#define KEY_OPTION	"-key"
#define PASSWORD_OPTION	"-password"
#define CNAME_OPTION	"-ou"

/* http post body */
#define	DOC_NAME	"release_info"
#define	BOUNDARY	"ThIsIsHiRoBoUnDaRy"

/* Servlet Site Definitions */
#define	SERVICE_NAME	"Status Receiver"

static char *url = (char *)NULL;
static char *cert_file = (char *)NULL;
static char *key_file = (char *)NULL;
static char *key_password = (char *)NULL;
static char *ca_file = (char *)NULL;
static char *ou = "no-check";

/* forward declaration */
extern int send_post_header(HttpLibHandle *handle, char *uri, unsigned long clen);
extern int parse_args(int argc, char *argv[]);
extern void ticker(int tick);

void
print_usage(char *cmd)
{
    printf("Usage: %s <HTTPS options> <SW Release Doc> [ <pathname> <pathname> ... <pathname> ]\n\n", cmd);
    printf("     <HTTPS Options> (all mandatory)\n");
    printf("         %s <servlet URL>\n", URL_OPTION);
    printf("         %s <certificate file name>\n", CERT_OPTION);
    printf("         %s <key file name (pem encoded)>\n", KEY_OPTION);
    printf("         %s <certificate authority file name>\n", CA_OPTION);
    printf("         %s <password for the key file>\n", PASSWORD_OPTION);
    printf("\n");
    printf("     <SW Release Doc>: path name of the XML document describing\n");
    printf("                       the software release\n");
    printf("\n");
    printf("     <pathname>: A list of path names consisting this release.\n");
    printf("                 # of pathnames and their filenames must match to\n");
    printf("                 the one in the SW Release Doc.\n");
    printf("\n");
}

int
main(int argc, char *argv[])
{
    int nargs, st, i, ret, j, header_cnt, port;
    unsigned long clen;
    char uri[1024], hostname[256], buf[1024], *sw_rel_doc;
    char **headers;
    HttpLibHandle *handle;
    FILE *swf;
    struct stat statbuf;

    nargs = parse_args(argc, argv);

    if ((argc-nargs) < 1) {
        print_usage(argv[0]);
        printf("\nAt least, one SW Release Doc must be specified.\n");
        exit(-1);
    }

    if (!url || !cert_file || !key_file || !ca_file) {
        print_usage(argv[0]);
        printf("\nSome security information is not specified.\n");
        exit(-1);
    }

    headers = malloc((argc-nargs) * sizeof(char *));

    /* check if all files are readable, and its size
     * construct headers for each file
     */
    clen = 0;
    sw_rel_doc = argv[nargs++];
    st = stat(sw_rel_doc, &statbuf);
    if (st < 0 || !(statbuf.st_mode & 0444)) {
	printf("Unabel to access to the release document %s\n", sw_rel_doc);
	exit(-1);
    }
    clen += statbuf.st_size;
    sprintf(buf, "--%s\r\nContent-Disposition: form-data; name=\"%s\"\r\nContent-Type: text/xml\r\n\r\n", BOUNDARY, DOC_NAME);
    headers[0] = malloc(strlen(buf) + 1);
    strcpy(headers[0], buf);
    clen += strlen(buf);

    for (i = nargs; i<argc; i++) {
	st = stat(argv[i], &statbuf);
	if (st < 0 || !(statbuf.st_mode & 0444)) {
	    printf("Unable to open file %s\n", argv[i]);
	    exit(-1);
	}
	clen += statbuf.st_size;

	/* extract only filename */
	j = strlen(argv[i]);
	while (j>0 && *(argv[i] + j) != '/' && *(argv[i] + j) != '\\')
	    j--;
	if (*(argv[i] + j) == '/' || *(argv[i] + j) == '\\') {
	    j++;
	}
	sprintf(buf, "\r\n--%s\r\nContent-Disposition: form-data; name=\"%s\"\r\nContent-Type: application/octet-stream\r\n\r\n", BOUNDARY, argv[i]+j);
	headers[i-nargs+1] = malloc(strlen(buf) + 1);
	strcpy(headers[i-nargs+1], buf);
	clen += strlen(buf);
    }

    /* last separator */
    sprintf(buf, "\r\n--%s--\r\n", BOUNDARY);
    headers[i-nargs+1] = malloc(strlen(buf) + 1);
    strcpy(headers[i-nargs+1], buf);
    clen += strlen(buf);
    printf("content-length=%ld\n", clen);

    /* now, connect to the server and send data
     */
    url_extract(url, hostname, uri, &port);

    handle = httplib_connect_ssl(hostname, port, 
                                     cert_file,
                                     ca_file,
                                     key_file,
                                     key_password,
                                     ou,
                                     FALSE);   /* keep-alive false */

    if (!handle) {
        /* connection cannot be made */
        printf("Unable to establish a connection to %s.\n", url);
        exit(-1);
    }

    /* send header */
    st = send_post_header(handle, uri, clen);
    printf("sent http header %d\n", st);

    /* send release document */
    swf = fopen(sw_rel_doc, "r");
    if (swf == NULL) {
	printf("Unable to open software release file\n");
	httplib_close(handle);
	exit(-1);
    }
    header_cnt = 0;
    st = httplib_send(handle, headers[header_cnt], strlen(headers[header_cnt]));
    header_cnt++;
    printf("sent release_info header %d\n", st);
    st = send_file(handle, swf);
    fclose(swf);
    printf("sent release document %d\n", st);

    /* send binary files */
    for (i = nargs; i<argc; i++) {
	FILE *ff = fopen(argv[i], "r");
	if (ff == NULL) {
	    printf("critical error: unable to open %s\n", argv[i]);
	    httplib_close(handle);
	    exit(-1);
	}

	/* extract only filename */
	j = strlen(argv[i]);
	while (j>0 && *(argv[i] + j) != '/' && *(argv[i] + j) != '\\')
	    j--;
	if (*(argv[i] + j) == '/' || *(argv[i] + j) == '\\') {
	    j++;
	}

	st = httplib_send(handle,
			headers[header_cnt], strlen(headers[header_cnt]));
	header_cnt++;
	printf("sent a header for file %s %d\n", argv[i]+j, st);

	st = send_file(handle, ff);
	fclose(ff);
	printf("sent a file %s %d\n", argv[i]+j, st);
    }

    /* send last separator */
    st = httplib_send(handle, headers[header_cnt], strlen(headers[header_cnt]));
    printf("sent the final separator %d\n", st);

    st = read_response(handle, ticker, 1, 60*60, -1);

    printf("\n");
    ret = 0;
    switch (st) {
        case 200: /* success */
            printf("The software release has been registered at %s\n", url);
            break;

        default: /* server error */
            printf("Error: Attempt to register S/W release at %s failed due to %d\n", url, st);
            break;
    }

    httplib_close(handle);

    exit(ret);
}


/*************************
 *  post data
 *************************/
int
send_post_header(HttpLibHandle *handle, char *uri, unsigned long clen)
{
    char header[1024];

    /* send POST message to the server: The message is:
     *  "POST <URI> HTTP/1.0\r\n" +
     *      "Accept: text/html, image/gif ...\r\n" +
     *      "Host: <host>\r\n" +
     *      "Content-Type: multipart/form-data, boundary=%s\r\n" +
     *      "Content-Length: NNN\r\n" +
     *      "Connection: close\r\n" +
     *      "\r\n" +    <- end of HTTP header
     */

    /* send header */
    sprintf(header, "POST %s HTTP/1.0\r\nAccept: text/html, image/gif, image/jpeg, */*\r\nHost: %s\r\nContent-Type: multipart/form-data, boundary=%s\r\nContent-Length: %ld\r\nConnection: close\r\n\r\n",
        uri, handle->hostname, BOUNDARY, clen);

    return httplib_send(handle, header, strlen(header));
}


void
ticker(int tick)
{
    printf("*");
    fflush(stdout);
}

int
parse_args(int argc, char *argv[])
{
    int i;

    for (i=1; i<argc; i++) {
        if (strcmp(argv[i], CERT_OPTION) == 0) {
            cert_file = argv[++i];

        } else if (strcmp(argv[i], CA_OPTION) == 0) {
            ca_file = argv[++i];

        } else if (strcmp(argv[i], KEY_OPTION) == 0) {
            key_file = argv[++i];

        } else if (strcmp(argv[i], PASSWORD_OPTION) == 0) {
            key_password = argv[++i];

        } else if (strcmp(argv[i], URL_OPTION) == 0) {
            url = argv[++i];

        } else if (strcmp(argv[i], CNAME_OPTION) == 0) {
            ou = argv[++i];

        } else {
            break;
        }
    }

    return i;
}



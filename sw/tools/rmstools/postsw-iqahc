#!/bin/sh

export PATH=/opt/buildtools/bin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

usage()
{
    echo "postsw [beta|prod] "
    echo "       <server>"
    echo "       [hw|release|<pkg-name>]*"
}


init()
{
    RMS_TYPE=$1
    RMS_SERVER=$2
    URL=https://${RMS_SERVER}:8443/hron_sw_rel/entry
    CERTDIR=/opt/buildtools/rms_certs/${RMS_TYPE}
    CERTFILE=$CERTDIR/identity.pem
    KEYFILE=$CERTDIR/private_key.pem
    CAFILE=$CERTDIR/root_and_ca_chain.pem
    PASSWORD=`cat $CERTDIR/password`

    logfile=postsw-$(date '+%Y%m%d%H')-$RMS_TYPE.log

    ( echo "***********************";
      date | tee -a $logfile;
      echo "RMS_TYPE=$1";
      echo "RMS_SERVER=$2";
      echo "***********************") | tee -a $logfile
}


post_files()
{
    echo "Post Files to $URL: $*" | tee -a $logfile
    postrel \
	-ou no-check \
	-url $URL \
	-cert $CERTFILE \
	-key $KEYFILE \
	-ca $CAFILE \
	-password $PASSWORD \
	$* 2>&1 | tee -a $logfile
}


post_hw()
{
    printf "==============\nPost Hardware Release\n==============\n" | tee -a $logfile

    post_files \
	./hw_rel.xml
}


post_release()
{
    printf "==============\nPost Release\n=============\n" | tee -a $logfile

    post_files \
	./sw_rel.xml \
	./release.dsc
    post_files \
	./model_rel.xml
}


post_pkg()
{
    printf "=================\nPost Package $1\n================\n" | tee -a $logfile

    if [ ! -e ./$1.zip ]; then
	usage
	echo "postsw: can't find ./$1.zip"
	exit 1
    fi
    if [ ! -e ./$1_rel.xml ]; then
	usage
	echo "postsw: can't find ./$1_rel.xml"
	exit 1
    fi

    post_files \
	./$1_rel.xml \
	./release.dsc \
	./$1.zip
}


if [ $# -lt 3 ]; then
    usage
    exit 1
fi

init $*
shift 2

for pkg in $@; do
    case $pkg in
	hw)         post_hw;;
	release)    post_release;;
	*)          post_pkg $pkg;;
    esac
done

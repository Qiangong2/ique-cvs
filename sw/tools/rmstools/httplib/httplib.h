/*
 * header file for httplib users
 */

#ifndef TRUE
#define	TRUE	(-1)
#endif
#ifndef FALSE
#define	FALSE	(0)
#endif

typedef struct _HttpLibHandle {

    char hostname[128];
    int is_ssl;
    void *ssl;
    int socket_fd;
    int keep_alive;

} HttpLibHandle;

typedef struct _HttpLibAttribute {

    char *name;
    char *value;

} HttpLibAttribute;

extern HttpLibHandle *httplib_connect_ssl(
                                char *hostname,
                                int portnumber,
                                char *cert_filename,
                                char *ca_filename,
                                char *key_filename,
                                char *keyfile_passwd,
                                char *cname,
                                int keep_alive);

extern HttpLibHandle *httplib_connect(
                                char *hostname,
                                int portnumber,
                                int keep_alive);

extern int httplib_send(HttpLibHandle *handle, void *buffer, int len);

extern int httplib_recv(HttpLibHandle *handle, void *buffer, int len);

extern void httplib_close(HttpLibHandle *handle);

extern int httplib_post(HttpLibHandle *handle, char *uri, int nattrs, HttpLibAttribute *attrs);

extern int httplib_post_timeout(HttpLibHandle *handle, char *uri, int nattrs, HttpLibAttribute *attrs, void (*tick_callback)(int), int tick, int tick_timeout);

extern int read_response(HttpLibHandle *handle, void (*tick_callback)(int), int tick, int tick_timeout, int show_res);

extern int read_line(HttpLibHandle *handle, void (*tick_callback)(int), int tick, int tick_timeout, char *buff /* OUT */, int buflen);

extern void url_extract(const char *url, char *hostname, char *uri, int *port);

extern int send_file(HttpLibHandle *handle, FILE *ff);



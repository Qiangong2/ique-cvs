#!/bin/sh

# Install all packages under a directory according to the module ordering.
#
# See /etc/init.d/pkgswup for similar functions
#

PKGDIR=$1

if [ ! -d "$PKGDIR" ]; then
    echo "Usage: $0 <package directory>"
    exit 1
fi

cd $PKGDIR

pkglist=`grep -s module.order */release.dsc | sed -e 's/\/release.dsc.*=/ /' | sort -k 2 | cut -d' ' -f 1`

if [ $? != 0 ]; then
    echo "No package to be installed under $PKGDIR"
    exit 0
fi    

for i in $pkglist; do
    echo -n "Installing $i ... "
    pkinst $i/$i.pkg
    if [ $? = 0 ]; then
	echo "OK"
    else
	echo "FAILED"
	exit 1
    fi
done    

#!/bin/sh

# Install all packages under a directory according to the module ordering.
#
# See /etc/init.d/pkgswup for similar functions
#

CONF_FILE=/etc/broadon.conf

parse_attributes() {
        OLDVALUE=$VALUE
        # remove the leading part up to the first ';'
        VALUE=${VALUE#*;}
        if [ "$OLDVALUE" = "$VALUE" ]; then
            return;
        fi

	oldIFS=$IFS
	IFS=';'
	# for each token of the form x=y, replace the '=' by a space and then
	# set the config variable to it.
	for i in $VALUE; do
	    IFS=$oldIFS
	    kv=(`echo ${i/=/' '}`)
	    setconf ${kv[0]} "${kv[1]//@@/' '}"
	    echo ${kv[0]}="${kv[1]//@@/' '}" >> $CONF_FILE
	    IFS=';'
	done
	IFS=$oldIFS
}

PKGDIR=$1

if [ ! -d "$PKGDIR" ]; then
    echo "Usage: $0 <package directory>"
    exit 1
fi

cd $PKGDIR

pkglist=`grep -s module.order */release.dsc | sed -e 's/\/release.dsc.*=/ /' | sort -k 2 | cut -d' ' -f 1`

if [ $? != 0 ]; then
    echo "No package to be installed under $PKGDIR"
    exit 0
fi    

rm -f $CONF_FILE

for i in $pkglist; do
    echo -n "Activating $i ... "
    VAR=`grep type $i/sw_rel.xml | sed -e 's/<[^>]*>//g' | sed -e 's/[[:space:]]//g'`
    VALUE=`grep param $i/sw_rel.xml | sed -e 's/<[^>]*>//g' | sed -e 's/[[:space:]]//g'`
    setconf $VAR "${VALUE%%;*}"
    echo $VAR="${VALUE%%;*}" >> $CONF_FILE
    parse_attributes
    if [ $? = 0 ]; then
	echo "OK"
    else
	echo "FAILED"
	exit 1
    fi
done    

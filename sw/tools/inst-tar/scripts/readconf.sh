#!/bin/sh

# Update conf space using file as input
#

FILE=$1

if [ ! -f "$FILE" ]; then
    echo "Usage: $0 <conf file>"
    exit 1
fi

exec < $FILE
while read line
do

line=`echo ${line/=/' '}`
echo setconf $line
setconf $line

done


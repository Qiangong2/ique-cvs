#!/bin/sh

# Fill in DNS template and output to file
#

INPUT=$1
OUTPUT=$2

if [ ! -f "$INPUT" -o -z "$OUTPUT" ]; then
    echo "Usage: $0 <input template> <output file>"
    exit 1
fi

echo Please enter the following values:
echo ---------------------------------
echo "FQDN without the hostname (e.g. vpn.broadon.com)"
read -e -p "DOMAIN_NAME? " DOMAIN_NAME
echo "FQDN of the mail relay for this domain. (e.g. mail.broadon.com)"
read -e -p "MAIL_RELAY? " MAIL_RELAY
echo Address of this subnet, e.g. "192.168.0"
read -e -p "IP_PREFIX? " IP_PREFIX

SERIAL=`date +%Y%m%d%H`
IFS=.
kv=($IP_PREFIX)
IFS=
REVERSE_IP="${kv[2]}.${kv[1]}.${kv[0]}"

sed "s/@@DOMAIN_NAME@@/$DOMAIN_NAME/;s/@@SERIAL@@/$SERIAL/;s/@@MAIL-RELAY@@/$MAIL_RELAY/;s/@@IP_PREFIX@@/$IP_PREFIX/;s/@@REVERSE_IP@@/$REVERSE_IP/" $INPUT > $OUTPUT

echo
echo Written $OUTPUT OK

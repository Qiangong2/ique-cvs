#!/bin/sh

# Generate a Broadon Package

function fatal
{
    echo "$progname: $*"
    rm -fr /usr/tmp/pkgen.$$
    exit 1
}

get_dsc()
{
    grep "$1=" $2 | sed -e 's/module.*=[ 	]*//' -e 's:[ 	]*$::'
}

function init
{
    usage="Usage: pkgen --indir <build-dir> --outfile <pkg-file>"

    progname=$0
    while [ $# -gt 0 ]; do
	case $1 in
	    --indir) 
		indir=$2
		shift 2;;
	    --outfile)
		outfile=$2
		shift 2;;
	    *)
		fatal $usage
	esac
    done

    if [ -z "$indir" ] || [ -z "$outfile" ]; then
	fatal $usage
    fi

    if ! [ -x "$indir" ]; then
	fatal "input directory $indir not found"
    fi

    if ! [ -e $indir/control/release.dsc ]; then
	fatal "can't find control/release.dsc"
    fi

    pkgid=`get_dsc module.pkgid $indir/control/release.dsc`
    userid=`get_dsc module.userid $indir/control/release.dsc`
    uid=`get_dsc module.uid $indir/control/release.dsc`
    revision=`get_dsc module.revision $indir/control/release.dsc`
    release=`get_dsc module.release $indir/control/release.dsc`
    major_version=`get_dsc module.major_version $indir/control/release.dsc`

    if [ -z "$pkgid" ]; then
	fatal "module.pkgid not found in release.dsc"
    fi
    if [ -z "$userid" ]; then
	fatal "module.uerid not found in release.dsc"
    fi
    if [ -z "$uid" ]; then
	fatal "module.uid not found in release.dsc"
    fi
    if [ -z "$major_version" ]; then
	fatal "module.major_version not found in release.dsc"
    fi
    if [ -z "$release" ]; then
	fatal "module.release not found in release.dsc"
    fi
    if [ -z "$revision" ]; then
	fatal "module.revision not found in release.dsc"
    fi

    echo -e "Package ID:\t$pkgid"
    echo -e "User ID:\t$userid"
    echo -e "Uid:\t\t$uid"
    echo -e "Major_version:\t$major_version"
    echo -e "Revision:\t$revision"
    echo -e "Release:\t$release"
}

function generate
{
    tmpdir=/usr/tmp/pkgen.$$
    mkdir -p $tmpdir

    echo "Generating CMD ..."
    (cd $indir/cmd; tar czf $tmpdir/cmd.tgz --exclude=CVS .)

    echo "Generating CONTROL ..."
    (cd $indir/control; tar czf $tmpdir/control.tgz --exclude=CVS .)
    
    echo "Generating PACKAGE ..."
    (cd $indir/package; tar czf $tmpdir/package.tgz --exclude=CVS .)
    
    (cd $tmpdir; tar cf all.tar control.tgz cmd.tgz package.tgz)
    
    cp $tmpdir/all.tar $outfile

    rm -fr $tmpdir
}


function fini
{
    echo "Done"
}

init $*
generate
fini

// LinkMenu.cpp : implementation file
//

#include "stdafx.h"
#include "vba.h"
#include "LinkMenu.h"
//#include "LinkWait.h"
#include "../lce.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

////////////////////////////////////////////////////////////////////////////
// LinkMenu dialog


LinkMenu::LinkMenu(CWnd* pParent /*=NULL*/)
	: CDialog(LinkMenu::IDD, pParent)
{
	//{{AFX_DATA_INIT(LinkMenu)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
    m_linklog = false;
    m_lan = 0;
    m_numplayers = 2;
    m_linklog = 0;
    m_uselce = false;
    m_romhack = false;
    m_debug = false;
    m_queueType = 0;
    m_master = 0;
}


void LinkMenu::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(LinkMenu)
	DDX_Control(pDX, IDC_TIMEOUT, m_timeout);
	DDX_Check(pDX, IDC_LINKLOG, m_linklog);
	DDX_Radio(pDX, IDC_LINK_LAN, m_lan);
	DDX_Radio(pDX, IDC_LINK2P, m_numplayers);
	DDX_Control(pDX, IDC_SERVERIP, m_serverip);
    DDX_Check(pDX, IDC_USE_LCE, m_uselce);
    DDX_Check(pDX, IDC_LCE_HACK, m_romhack);
    DDX_Check(pDX, IDC_LCE_DEBUG, m_debug);
	DDX_Radio(pDX, IDC_LCE_MASTER, m_master);
    DDX_Control(pDX, IDC_LCE_FRAMELEN, m_framelen);
	DDX_Radio(pDX, IDC_LCE_SYNC, m_queueType);
    DDX_Control(pDX, IDC_LCE_DELAY, m_delay);
	DDX_Control(pDX, IDC_VNG_SERVER, m_vngServer);
	DDX_Control(pDX, IDC_VNG_PORT, m_vngPort);
	DDX_Control(pDX, IDC_VNG_USER, m_vngUser);
	DDX_Control(pDX, IDC_VNG_PASSWD, m_vngPasswd);
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(LinkMenu, CDialog)
	//{{AFX_MSG_MAP(LinkMenu)
	ON_BN_CLICKED(ID_OK, OnOk)
	ON_BN_CLICKED(IDC_LINKLOG, OnLinklog)
	ON_BN_CLICKED(IDC_LINK_LAN, OnLinkLan)
	ON_BN_CLICKED(IDC_LINKCONNECT, OnLinkConnect)
	ON_BN_CLICKED(IDC_LINK2P, OnLink2p)
	ON_BN_CLICKED(IDC_LINK3P, OnLink3p)
	ON_BN_CLICKED(ID_CANCEL, OnCancel)
	ON_BN_CLICKED(IDC_LINK_ONECOMP, OnLinkLan)
	ON_BN_CLICKED(IDC_LINK4P, OnLink4p)
	ON_BN_CLICKED(IDC_USE_LCE, OnUseLce)
	ON_BN_CLICKED(IDC_LCE_HACK, OnLceHack)
	ON_BN_CLICKED(IDC_LCE_DEBUG, OnLceDebug)
	ON_BN_CLICKED(IDC_LCE_SYNC, OnLceSync)
	ON_BN_CLICKED(IDC_LCE_ASYNC, OnLceAsync)
	ON_BN_CLICKED(IDC_LCE_STREAM, OnLceStream)
	ON_BN_CLICKED(IDC_LCE_MASTER, OnLceMaster)
	ON_BN_CLICKED(IDC_LCE_SLAVE, OnLceSlave)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
 

/////////////////////////////////////////////////////////////////////////////
// LinkMenu message handlers

BOOL LinkMenu::OnInitDialog(){
	CDialog::OnInitDialog();

	m_numplayers = lanlink.numgbas-1;
	m_timeout.LimitText(5);
	m_linklog = linklog != 0;

	CString buffer;
	buffer.Format("%d", linktimeout);
	m_timeout.SetWindowText(buffer);

    m_uselce = use_lce != 0;
    m_romhack = rom_hack != 0;
    m_debug = lce_debug != 0;
	m_framelen.LimitText(2);
	buffer.Format("%d", framelen);
	m_framelen.SetWindowText(buffer);
	m_delay.LimitText(2);
	buffer.Format("%d", delay);
	m_delay.SetWindowText(buffer);
    m_queueType = queueType-1;
    m_master = linkid != 0;
    m_vngServer.LimitText(100);
    m_vngServer.SetWindowText(vngServer);
    m_vngPort.LimitText(5);
    m_vngPort.SetWindowText(vngPort);
    m_vngUser.LimitText(100);
    m_vngUser.SetWindowText(vngUser);
    m_vngPasswd.LimitText(100);
    m_vngPasswd.SetWindowText(vngPasswd);

	if(linkid&&!lanlink.active) GetDlgItem(IDC_LINKLOG)->EnableWindow(FALSE);
	if(!lanlink.active){
		m_lan = 1;
		GetDlgItem(IDC_LINKCONNECT)->EnableWindow(FALSE);
		//GetDlgItem(IDC_LINK2P)->EnableWindow(FALSE);
		//GetDlgItem(IDC_LINK3P)->EnableWindow(FALSE);
		//GetDlgItem(IDC_LINK4P)->EnableWindow(FALSE);
		GetDlgItem(IDC_SERVERIP)->EnableWindow(FALSE);
	} else {
		m_lan = 0;
		GetDlgItem(IDC_LINKCONNECT)->EnableWindow(TRUE);
		//GetDlgItem(IDC_LINK2P)->EnableWindow(TRUE);
		//GetDlgItem(IDC_LINK3P)->EnableWindow(TRUE);
		//GetDlgItem(IDC_LINK4P)->EnableWindow(TRUE);
		GetDlgItem(IDC_SERVERIP)->EnableWindow(TRUE);
	}

  UpdateData(FALSE);
  CenterWindow();
  
  return TRUE;
}
	

void LinkMenu::OnOk() 
{
	CString buffer;

	if(linklog&&jjj==NULL){
			if((jjj=fopen("vbalog.txt", "wt"))==NULL){
				PostQuitMessage(0);
			}
			fprintf(jjj, "GBA0 GBA1 GBA2 GBA3 clocks between transfers\n");
	}

	if(!linklog&&jjj!=NULL){
		fclose(jjj);
		jjj = NULL;
	}
	
	m_framelen.GetWindowText(buffer);
	framelen = atoi(buffer);
	m_delay.GetWindowText(buffer);
	delay = atoi(buffer);
    use_lce = m_uselce;
    rom_hack = m_romhack;
    lce_debug = m_debug;
    linkid = m_master;
    m_vngServer.GetWindowText(vngServer, 100);
    m_vngPort.GetWindowText(vngPort, 100);
    m_vngUser.GetWindowText(vngUser, 100);
    m_vngPasswd.GetWindowText(vngPasswd, 100);

    m_timeout.GetWindowText(buffer);
	linktimeout = atoi(buffer);
	EndDialog(linktimeout);
}

void LinkMenu::OnCancel()
{
	EndDialog(-1);
}

void LinkMenu::OnLinklog()
{
	linklog ^= 1;
}

void LinkMenu::OnUseLce()
{
	m_uselce ^= 1;
}

void LinkMenu::OnLceHack()
{
	m_romhack ^= 1;
}

void LinkMenu::OnLceDebug()
{
	m_debug ^= 1;
}

void LinkMenu::OnLinkLan()
{
	lanlink.wasactive ^= 1;

	systemMessage(0, "Setting will be effective the next time you start the emulator");      
	GetDlgItem(IDC_LINKCONNECT)->EnableWindow(FALSE);
	//GetDlgItem(IDC_LINK2P)->EnableWindow(FALSE);
	//GetDlgItem(IDC_LINK3P)->EnableWindow(FALSE);
	//GetDlgItem(IDC_LINK4P)->EnableWindow(FALSE);
	GetDlgItem(IDC_SERVERIP)->EnableWindow(FALSE);

}


void LinkMenu::OnLinkConnect() 
{
	CString buffer;
	int nret;

	m_serverip.GetWindowText(buffer);
	lanlink.serveraddress.S_un.S_addr = inet_addr(buffer);
	
	if((nret=LanConnect())!=0){
		char errror[100];
		sprintf(errror, "An error occured, please try again.\nError code: %d", nret);
		MessageBox(errror, "Error!", MB_OK);
	}
	return;
}



void LinkMenu::OnLink2p() 
{
	lanlink.numgbas = 1;	
}

void LinkMenu::OnLink3p() 
{
	lanlink.numgbas = 2;	
}

void LinkMenu::OnLink4p() 
{
	lanlink.numgbas = 3;	
}

void LinkMenu::OnLceMaster() 
{
	m_master = 0;
}

void LinkMenu::OnLceSlave() 
{
	m_master = 1;
}

void LinkMenu::OnLceSync() 
{
	queueType = LCE_TS_SYNC_MODE;	
}

void LinkMenu::OnLceAsync() 
{
	queueType = LCE_TS_ASYNC_MODE;	
}

void LinkMenu::OnLceStream() 
{
	queueType = LCE_TS_STREAM_MODE;	
}

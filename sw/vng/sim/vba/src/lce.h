#ifndef _LCE_H
#define _LCE_H

#pragma pack(push, 4)

// Emulation modes
//
#define  LCE_NORMAL_MODE        0   // normal mode
#define  LCE_TS_SYNC_MODE       1   // time-shifted sync comm.
#define  LCE_TS_ASYNC_MODE      2   // time-shifted async comm.
#define  LCE_TS_STREAM_MODE     3   // time-shifted data stream

extern int use_lce;
extern int rom_hack;
extern int lce_debug;
extern int framelen;
extern int delay;
extern int queueType;
extern int toggleEnable;
extern int toggleMode;
#if 1
extern HANDLE sio_intr;
#endif
extern char vngServer[100];
extern char vngPort[100];
extern char vngUser[100];
extern char vngPasswd[100];

typedef     unsigned char           u8;
typedef     unsigned short int      u16;
typedef     unsigned int            u32;
typedef     unsigned long long int  u64;
typedef     volatile unsigned char           vu8;
typedef     volatile unsigned short int      vu16;
typedef     volatile unsigned int            vu32;
typedef     volatile unsigned long long int  vu64;

typedef struct {
    vu32 reg_ie_if;
    vu32 emulated_intr;
    vu32 frame_counter;
    vu32 timer_counter;
    vu32 frame_counter_limit;
    vu32 timer_counter_limit;
    vu32 frame_intr_skipped;
    vu32 timer_intr_skipped;
    vu32 vcount_intr_skipped;
    vu32 mode;
    vu16 delay;
    vu16 siomulti0;
    vu16 siomulti1;
    vu16 siomulti2;
    vu16 siomulti3;
    vu16 siocnt;
    vu16 siomulti_send;
    vu16 enable;
    vu16 linkid;
    vu16 main_loop_done;
    vu16 game_mode;
    /* private */
    vu32 tmp;
    vu32 tmp2;
} lce_type;

enum {
    LCE_PROC_ID_INIT = 43,
    LCE_PROC_ID_SETDUMMYDATA = 44,
    LCE_PROC_ID_SETDELAY = 45,
    LCE_PROC_ID_SETFRAMELEN = 46,
    LCE_PROC_ID_ENABLE = 47,
    LCE_PROC_ID_DISABLE = 48,
    LCE_PROC_ID_SWITCHMODE = 49,
    LCE_PROC_ID_TRIGGER = 50,
    LCE_PROC_ID_SCHEDSIOINTR = 51,
    LCE_PROC_ID_VBLANK = 52,
};

void checkCartIntr();

void checkSWI();

void checkModeChange();

void callRPC(int procId, int input = 0);

// Return pointer to LCE shared buffer
lce_type* getLCEDataArea();

void LCE_Trigger();

void callInit(int debug,
              int linkid,
              int numgbas,
              char vngServer[100],
              char vngPort[100],
              char vngUser[100],
              char vngPasswd[100]);

#pragma pack(pop)

#endif /* _LCE_H */


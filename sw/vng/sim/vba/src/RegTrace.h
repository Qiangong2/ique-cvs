#ifndef REGTRACE_H
#define REGTRACE_H

#define TRACE_REG_ACCESS

#ifdef TRACE_REG_ACCESS
extern void RegTraceLog(int reg, int value, int nextpc, int size, int wr);
extern void RegTraceInit();
extern void RegTraceFini();
#else
#define RegTraceLog(a, b, c, d, e)
#define RegTraceInit()
#define RegTraceFini()
#endif

#endif

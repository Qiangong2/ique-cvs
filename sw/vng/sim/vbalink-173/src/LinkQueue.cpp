#include "GBA.h"
#include <stdio.h>
#include "win32/stdafx.h"
#include "port.h"
#include "Link.h"
#include "win32/vba.h"
#include "win32/MainWnd.h"
#include "win32/LinkOptions.h"
#include "win32/Reg.h"

extern unsigned int frameid;
extern bool queueMode;
extern u16 linkdata[4];
extern u16 linkout;
extern u16 d1valid;
extern bool toggle;

void lserver::PushData(u16 data, int index)
{
    LinkData tmpData;
    tmpData.frame = currFrame;
    tmpData.data = data;
    linkQ[index].push_back(tmpData);
}

LinkData dummyData = { 0, 0x00 };

void lserver::SyncSend(u16* d0, u16* d1, u16* d1Valid)
{
    static unsigned int lastQueueFrame = 0;
    bool currNewFrame = false;
    bool queueNewFrame = false;

    if (lastFrame != currFrame) {
        if (toggle) {
            queueMode = !queueMode;
            toggle = false;

            char buf[64];
            sprintf(buf, "Sync mode: %d", queueMode);
            systemScreenMessage(buf);
        }

        currNewFrame = true;
        if (linkQ[0].size() != 0 && queuedFrames < queueSize)
            queuedFrames++;
    }

    /* Not in queue mode or have no data to send */
    if (queuedFrames < queueSize) {
        *d0 = linkout;
        *d1Valid = 0;
    } else {
        if (lastQueueFrame != linkQ[0].front().frame) {
            queueNewFrame = true;
        }

        if (currNewFrame == queueNewFrame) {
            /* Frame sizes match.  Just send the top of queue */
            *d0 = linkQ[0].front().data;
            *d1 = linkQ[1].front().data;
            *d1Valid = 1;
            lastQueueFrame = linkQ[0].front().frame;
        } else if (currNewFrame && !queueNewFrame) {
            /* GBA1 has sent a new frame, but the queue is not yet a new frame.
            We should skip packets on the queue until a new frame is reached. */
            while (lastQueueFrame == linkQ[0].front().frame) {
                linkQ[0].pop_front();
                linkQ[1].pop_front();
            }
            *d0 = linkQ[0].front().data;
            *d1 = linkQ[1].front().data;
            *d1Valid = 1;
            lastQueueFrame = linkQ[0].front().frame;
        } else {
            /* We ran out of packets in the queue's frame, but GBA1 is still in the
            same frame.  We'll just insert dummy data and not touch the queue.*/
            linkQ[0].push_front(dummyData);
            linkQ[1].push_front(dummyData);
            *d0 = linkQ[0].front().data;
            *d1 = linkQ[1].front().data;
            *d1Valid = 1;
        }
    }
}

void lserver::AsyncSend(u16* d0, u16* d1, u16* d1Valid)
{
    bool currNewFrame = false;
    bool queueNewFrame = false;
    static int lastPktIndex = 0;
    static u16 lastPktQueue[100];

    if (linkout == 0xfefe) {
        if (toggle) {
            queueMode = !queueMode;
            toggle = false;

            char buf[64];
            sprintf(buf, "Async mode: %d", queueMode);
            systemScreenMessage(buf);
        }

        currNewFrame = true;
        if (linkQ[0].size() != 0 && queuedFrames <= queueSize)
            queuedFrames++;
        lastPktIndex = 0;
    }
    if (!queueMode) {
        queuedFrames = 0;
        lastPktQueue[lastPktIndex] = linkout;
    }

    /* Not in queue mode or have no data to send */
    if (queuedFrames <= queueSize) {
        *d0 = lastPktQueue[lastPktIndex];
        *d1Valid = 0;
    } else {
        if (linkQ[0].front().data == 0xfefe) {
            queueNewFrame = true;
        }

        if (currNewFrame == queueNewFrame) {
            /* Frame sizes match.  Just send the top of queue */
            *d0 = linkQ[0].front().data;
            *d1 = linkQ[1].front().data;
            *d1Valid = 1;
        } else if (currNewFrame && !queueNewFrame) {
            /* GBA1 has sent a new frame, but the queue is not yet a new frame.
            We should skip packets on the queue until a new frame is reached. */
            while (linkQ[0].front().data != 0xfefe) {
                linkQ[0].pop_front();
                linkQ[1].pop_front();
            }
            *d0 = linkQ[0].front().data;
            *d1 = linkQ[1].front().data;
            *d1Valid = 1;
        } else {
            /* We ran out of packets in the queue's frame, but GBA1 is still in the
            same frame.  We'll just insert dummy data and not touch the queue.*/
            linkQ[0].push_front(dummyData);
            linkQ[1].push_front(dummyData);
            *d0 = linkQ[0].front().data;
            *d1 = linkQ[1].front().data;
            *d1Valid = 1;
        }
    }
    //*d1Valid = 0; //XXX uncomment to queue GBA1 only
    lastPktIndex++;
}

void lserver::StreamSend(u16* d0, u16* d1, u16* d1Valid)
{
    if (toggle) {
        queueMode = !queueMode;
        toggle = false;

        char buf[64];
        sprintf(buf, "Stream mode: %d", queueMode);
        systemScreenMessage(buf);
    }

    if (queuedFrames < queueSize) {
        *d0 = linkout;
    } else {
        *d0 = linkQ[0].front().data;
    }
    *d1Valid = 0;
}

void lserver::FillSendData(u16* d0, u16* d1, u16* d1Valid)
{
    lastFrame = currFrame;
    currFrame = frameid;

    switch (queueType) {
        case SYNC:
            SyncSend(d0, d1, d1Valid);
            break;
        case ASYNC:
            AsyncSend(d0, d1, d1Valid);
            break;
        case STREAM:
            StreamSend(d0, d1, d1Valid);
            break;
        default:
            /* Send current data */
            *d0 = linkout;
            *d1Valid = 0;
            break;
    }

    if (!queueMode) {
        /* Send current data */
        *d0 = linkout;
        *d1Valid = 0;
    }
}

void lserver::SyncRcv(u16* d0, u16* d1)
{
    if (queuedFrames < queueSize) 
    {
        /* Queue not filled yet - send just-pushed data */
        *d0 = linkQ[0].back().data;
        *d1 = linkQ[1].back().data;
    } else
    {
        *d0 = linkQ[0].front().data;
        linkQ[0].pop_front();
        *d1 = linkQ[1].front().data;
        linkQ[1].pop_front();
    }
}

void lserver::AsyncRcv(u16* d0, u16* d1)
{
    if (queuedFrames < queueSize) 
    {
        /* Queue not filled yet - send just-pushed data */
        *d0 = linkQ[0].back().data;
        *d1 = linkQ[1].back().data;
    } else
    {
        *d0 = linkQ[0].front().data;
        linkQ[0].pop_front();
        *d1 = linkQ[1].front().data;
        //*d1 = linkQ[1].back().data; //XXX uncomment to queue GBA1 only
        linkQ[1].pop_front();
    }
}

void lserver::StreamRcv(u16* d0, u16* d1)
{
    if (queuedFrames < queueSize) 
    {
        /* Queue not filled yet - send just-pushed data */
        *d0 = linkQ[0].back().data;
        *d1 = linkQ[1].back().data;
        ++queuedFrames;
    } else
    {
        *d0 = linkQ[0].back().data;
        linkQ[0].pop_front();
        *d1 = linkQ[1].front().data;
        linkQ[1].pop_front();
    }
}

void lserver::FillRcvData(u16* d0, u16* d1)
{
    /* Just pushed recent data into queue */

    switch (queueType) {
        case SYNC:
            SyncRcv(d0, d1);
            break;
        case ASYNC:
            AsyncRcv(d0, d1);
            break;
        case STREAM:
            StreamRcv(d0, d1);
            break;
        default:
            *d0 = linkQ[0].front().data;
            linkQ[0].pop_front();
            *d1 = linkQ[1].front().data;
            linkQ[1].pop_front();
            break;
    }

    if (!queueMode) {
        /* Send current data */
        *d0 = linkQ[0].back().data;
        *d1 = linkQ[1].back().data;
    }
}


#ifndef SCRPC_H
#define SCRPC_H

#ifdef WIN32  /* defines for emulation */

/****************  SCRPC common defs   *********************/

#define SC_RPC_MAX_IF          7          /* max number of DLLs allowed */
#define SC_RPC_MAX             64         /* max number of RPC calls allowed */

/************* VBA/winsce Specific Defs ********************/

#define SC_RPC_SERVER_NAME     "winsce.exe"
#define SC_RPC_DIR             "scrpc"
#define SC_RPC_INIT_PROC       "sc_rpc_init"
#define SC_RPC_SM_NAME         "GBA_RPC_SHARED_MEMORY_FILE"
#define GBA_ROM_BASE_0         0x08000000
#define GBA_ROM_BASE_1         0x0a000000
#define GBA_ROM_BASE_2         0x0c000000
#define SC_RPC_SM_SIZE         0x2000400
#define SC_MAILBOX_OFFSET      0x1FFFFF0
#define SC_VBA_CMD_OFFSET      0x2000000
#define VBA_EVENT              "VBA_EVENT"
#define SC_EVENT               "SC_EVENT"
#define MAILBOX_ACK_EVENT      "MAILBOX_ACK_EVENT"
#define SC_DISPATCH_EVENT      "SC_DISPATCH_EVENT"
#define SC_DISPATCH_ACK_EVENT  "SC_DISPATCH_ACK_EVENT"
#define SC_RPC_STATUS_EVENT    "SC_RPC_STATUS_EVENT"

#define VBA_RPC_IF_ID          0
#define VBA_RPC_LOAD_DLL       1
#define VBA_RPC_UNLOAD_DLL     2
#define VBA_RPC_SHUTDOWN       3

/* each DLL has to implement an exit proc with procId 0 */
#define SC_RPC_EXIT_PROC_ID    0  

/* CMD used for run time loading of DLLs */
typedef struct {
    RPCHeader header;
    char      path[MAX_PATH]; /* path for loading DLL */
    int       retval;
} VBA_RPC_CMD;

/* interface map data structure used by winsce */
typedef struct {
    u16       id;          /* interface id */
    HINSTANCE hinf;        /* handle to the DLL */
    u32*      mq;          /* message queue for the interface */
    HANDLE    dispevent;   /* dispatch event from winsce to DLL for message queue */
    HANDLE    ackevent;    /* ack event from DLL to winsce for message queue */
    HANDLE    statusevent; /* status event from DLL to VBA for RPC status field */
    int       parent_pid;  /* unique id */
} SCRpcIfMap;

typedef void (*SCRPCPROC) (void*);
typedef void (*SCRPCREGISTERPROC) (SCRpcIfMap*);

#else    /* defines for GBA ROM */
#define SC_MAILBOX_OFFSET  0x1FFFFF0
#define SC_RPC_RAM_OFFSET  0x1000000
#endif

#endif

/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */
#ifndef __SC_RPC_H__
#define __SC_RPC_H__

#ifdef WIN32
#include "AgbTypes.h"
#endif

#define RPC_OK                  0
#define RPC_PENDING             1
#define RPC_ERR                 2
#define RPC_ERR_INVALID_PROC_ID 3
#define RPC_ERR_INVALID_IF_ID   4

typedef struct {
    vu16 ifId;    /* Unique ID for this interface */
    vu16 procId;  /* Unique ID, within the interface, for this routine */
    vu32 inLen;   /* Length of all IN parameters (in bytes) */
    vu32 outLen;  /* Length of all OUT parameters (in bytes) */
    vu32 status;  /* Used to pass RPC return value back to GBA */
} RPCHeader;

#endif /*__SC_RPC_H__*/

#ifndef __CONFIG_VAR_H__
#define __CONFIG_VAR_H__

#include "configvar.h"
#include "systemfiles.h"

/************************************************************************
	     Config Variables
************************************************************************/



#define CONFIG_LOG_ID            "bbdepot.log.id"
#define CONFIG_LOG_SYSLOG_LEVEL  "bbdepot.log.syslog.level"
#define CONFIG_LOG_FILE_LEVEL    "bbdepot.log.file.level"
#define CONFIG_LOG_CONSOLE_LEVEL "bbdepot.log.console.level"
#define CONFIG_COMM_VERBOSE      "bbdepot.comm.verbose"
#define COMM_HTTP11              1


#define BBDEPOT_DATADIR(leaf)    (getDataDir(leaf))
#define BBDEPOT_TEMPDIR(leaf)    (getTempDir(leaf))

#define LOGDIR                   BBDEPOT_DATADIR ("logs/")
#define ERROR_FILE               "error.log"
#define COMM_REQUEST_LOG         "request.log"
#define COMM_RESPONSE_LOG        "response.log"

#define CDS_INCOMING_DIR         BBDEPOT_DATADIR ("incoming/")
#define DB_CACHE_DIR             BBDEPOT_DATADIR ("cache/")


#endif

#ifndef __SYSCLNT_H__
#define __SYSCLNT_H__

#include <sys/types.h>

#include "config_var.h"

#ifndef CONFIG_DIRECT_FILE
#define CONFIG_RMSD               1
#endif

#ifdef __cplusplus
extern "C" {
#endif

/** The sys_strerror() returns a string describing the error code
    passed in the argument errno */
const char *sys_strerror(int errno);

/** @defgroup sys_services GatewayOS Services
    @{ */
    
/** Search the config list for a variable.  If found, copies its value
    to buf.  @param config_var the configuration variable.  @param buf
    getconf copies up to config variable value to buf.  @param size at
    most size character can be written to buf.  @return 0 if
    successful.  -1 if unsuccessful.  */
int sys_getconf(const char *config_var, char *buf, size_t size);

/** Add or modify a config variable.  @param config_var the config
    variable name.  @param buf the new value of the config variable.
    @param overwrite if overwrite is 0, do not overwrite existing
    config variable.  @return 0 if successful. -1 if unsuccessful. */
int sys_setconf(const char *config_var, const char *buf, int overwrite);

/** Delete a config variable.  @param config_var the config
    variable name. */
int sys_unsetconf(const char *config_var);

/** Write to a file */
int sys_putfile(const char *fname, const char *buf, size_t len);

/** @} */

#ifdef __cplusplus
}
#endif

#endif

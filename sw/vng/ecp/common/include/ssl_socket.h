/************************************************************************
	     Socket and Secure Socket
************************************************************************/

#ifndef __SSL_SOCKET_H__
# define __SSL_SOCKET_H__

#include <string>

using namespace std;


#ifndef _WIN32
    #include <unistd.h>                // read
    #include <ext/rope>                // crope
    #include <netinet/in.h>            // sockaddr_in
#else
    typedef string crope;
#endif

#include <fcntl.h>                 // open
#include <sys/types.h>             // connect
#include <sys/stat.h>              // open
#ifndef _WIN32
    #include <sys/socket.h>            // connect
    #include <openssl/ssl.h>           // ssl_wrapper.h
    #include <openssl/rand.h>          // ssl_wrapper.h
    #include "ssl_wrapper.h"           // SSL_WRAPPER
#endif

#include "common.h"

using namespace std;
#ifndef _WIN32
    using namespace __gnu_cxx;
#endif

typedef int          Socket;         // regular UNIX socket is "int"

struct SSLparm {
    // SSL parameters
    const char* cert_file; 
    const char* ca_chain;
    const char* key_file;
    const char* CA_file;
    const char* key_file_passwd;
};

#ifndef _WIN32
    typedef SSL_WRAPPER* SecureSocket;  // secure socket based on SSL
#else
    typedef Socket SecureSocket;  // secure socket based on SSL
#endif

/* 
   Adaptors for regular sockets 
*/

#ifndef _WIN32
INLINE int init_skt(Socket& skt)
{
    skt = socket(PF_INET, SOCK_STREAM, 0);
    return skt;
}

INLINE int init_skt(Socket& skt, const SSLparm &c)
{
    return init_skt(skt);
}

INLINE void fini_skt(Socket& skt)
{
    close(skt);
}

EXTERN int connect(Socket& skt, const char *host, int port);

/*
  Adaptors for Secure Socket 
*/

EXTERN int init_skt(SecureSocket& skt, const SSLparm& c);

INLINE void fini_skt(SecureSocket& skt)
{
    delete_SSL_wrapper(skt);
}

INLINE int connect(SecureSocket& skt, const char *host, int port)
{
    return (SSL_wrapper_connect_saddr(skt, host, port, NULL) == 1) ? 0 : -1;
}

INLINE int connect(SecureSocket& skt, const char *host, int port, struct sockaddr_in *sin)
{
    return (SSL_wrapper_connect_saddr(skt, host, port, sin) == 1) ? 0 : -1;
}

INLINE int read(SecureSocket& skt, char *buf, size_t size)
{
    return SSL_wrapper_read(skt, buf, size);
}

INLINE int write(SecureSocket& skt, const char *buf, size_t size)
{
    return SSL_wrapper_write(skt, buf, size);
}

/*
  recv and send are template functions that will be instantiated with
  Socket and SecureSocket.

  recv is further overloadded with 2 output parameter types: crope and
  fname.   
*/
template <class SKT>
int recv(SKT& skt, crope& buf)
{
    char tmp[257];
    int n;
    crope rcvbuf;
    while((n = read(skt, tmp, sizeof(tmp)-1)) > 0) {
	tmp[n] = '\0';
	rcvbuf += crope(tmp);
    }
    buf = rcvbuf;
    return buf.size();
}

template <class SKT>
int recv(SKT& skt, FileObj& fobj)
{
    char tmp[4096];
    int n;
    int total = 0;
    while((n = read(skt, tmp, sizeof(tmp))) > 0) {
	total += n;
	fobj.write(tmp, n);
    }
    return total;
}

template <class SKT>
int send(SKT& skt, const crope& msg) 
{
    int size = msg.size();
    crope tmp; 
    tmp = msg;
    while (tmp.size() > 0) {
	int n;
	n = write(skt, tmp.c_str(), tmp.size());
	if (n < 0)
	    return -1;
	if ((unsigned) n < tmp.size()) 
	    tmp.erase(0, n);
	else
	    break;
    }
    return size;
}

INLINE const char *errmsg(Socket& skt)
{
    return strerror(errno);
}

INLINE const char *errmsg(SecureSocket& stk)
{
    return "SSL_wrapper failed";
}

#endif
#endif 

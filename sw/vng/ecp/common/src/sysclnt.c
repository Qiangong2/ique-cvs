/* System Services Client Function */

#include <sys/types.h>   // socket

#ifndef _WIN32
    #include <sys/socket.h>  // socket
    #include <sys/un.h>      // sockaddr_un
    #include <unistd.h>      // close
#else
    #include <string.h>      // strerror
    #include <process.h>     // execl
    #include <io.h>          // open write close
#endif

#include <stdlib.h>      // malloc
#include <sys/stat.h>    // open
#include <fcntl.h>       // open

#include "syslib.h"
#include "sysclnt.h"

#include "config.h"
#include "configvar.h"

static const char *error_str_tab[] = {
    "config variable not exist",
    "message body too big",
    "buffer too small",
    "cannot connect to sys server",
    "ran out of config space",
    "cannot send message",
    "didn't receive reply",
    "syscall not implemented",
    "remote method invocation failed",
    "resource not available", 
};


const char *sys_strerror(int errno)
{
    errno = -errno;
    if (errno < SYS_ERRNO_BEGIN)
        return strerror(errno);
    errno -= SYS_ERRNO_BEGIN;
    if (errno < sizeof(error_str_tab)/sizeof(char *))
        return error_str_tab[errno];
    return "Unknown Error";
}




int sys_getconf(const char *config_var, char *buf, size_t size)
{
    if (size <= 0) 
        return -EBUFTOOSMALL;
    buf[size-1] = '\0';
    if (getconf(config_var, buf, (int)size) == NULL) 
        return -ECVARNOTEXIST;
    if (buf[size-1] != '\0') 
        return -EBUFTOOSMALL;

    return 0;
}




int sys_setconf(const char *config_var, const char *buf, int overwrite)
{
    if (setconf(config_var, buf, overwrite) < 0) 
        return -ENOCFGSPACE;
    return 0;
}


int sys_unsetconf(const char *config_var)
{
    unsetconf(config_var);
    return 0;
}




int sys_putfile(const char *file, const char *buf, size_t size)
{
    int fd, n;
    if ((fd = open(file, O_WRONLY|O_CREAT, 0755)) < 0)
        return -1;
    
    while (size > 0) {
        n = write(fd, buf, (unsigned) size);
        if (n <= 0) return -1;
        buf += n;
        size -= n;
    }
    close(fd);
    return 0;
}


#include <windows.h>
#include <io.h>
#include "porting.h"


char* stpcpy(char *d, const char *s)
{
    strcpy(d,s);
    return d + strlen(d);
}


int isFuncSupported (const char *dll, const char *func)
{
    HMODULE module = GetModuleHandleA(dll);
    if (!module)
        return 0;
    return (GetProcAddress(module, func) ? 1 : 0);
}

int isLockFileExSupported ()
{
    return isFuncSupported ("kernel32", "LockFileEx");
}

int fcntl(int fd, int cmd, struct flock *lock)
{
    // Partial implementation for F_SETLKW, F_SETLK, F_GETLK

    #define lockSupport 1  // win98 doesn't support LockFileEx
    #if lockSupport == 1
        int    i;
    #endif
    int    err = -1;
    HANDLE osfh;
    DWORD  flags = 0;

    OVERLAPPED ov;
    memset (&ov, 0, sizeof(ov));

    osfh = (HANDLE)_get_osfhandle (fd);
    if (osfh == INVALID_HANDLE_VALUE) {
        return err;
    }

    ov.Offset = lock->l_start;

    switch (lock->l_whence)
    {
        case SEEK_SET:
            break;
        case SEEK_CUR:
            ov.Offset += SetFilePointer (osfh, 0, 0, FILE_CURRENT);
            break;
        case SEEK_END:
            ov.Offset += GetFileSize (osfh, NULL);
            break;
        default:
            SetLastError (ERROR_INVALID_HANDLE);
            return err;
    }

    #if lockSupport == 2  // win98 doesn't support LockFileEx
        if (!isLockFileExSupported()) {
            // fatal ("fcntl: Internal Error: Can't lock file\n");
            exit (-1);
            return err;        
        }
    #endif

    if (lock->l_type == F_WRLCK)
        flags |= LOCKFILE_EXCLUSIVE_LOCK;
        
    if (cmd != F_GETLK && lock->l_type == F_UNLCK) {
        #if lockSupport == 0  // win98 doesn't support LockFileEx
            err = 0;
        #elif lockSupport == 1
            err = UnlockFile (osfh, ov.Offset, 0, lock->l_len, 0) ? 0 : -1;
        #else
            err = UnlockFileEx (osfh, 0, lock->l_len, 0, &ov) ? 0 : -1;
        #endif
    }
    else switch (cmd) {

        case F_SETLK:
            flags |= LOCKFILE_FAIL_IMMEDIATELY;
            // fall through
        case F_SETLKW:
            /***   maybe if win98
            ***    flags |= LOCKFILE_FAIL_IMMEDIATELY;
            ***/
            #if lockSupport == 0  // win98 doesn't support LockFileEx
                err = 0;
            #elif lockSupport == 1
                for (i=0; i<1000; ++i) {
                    err = LockFile (osfh, ov.Offset, 0, lock->l_len, 0) ? 0 : -1;
                    if (!err)
                        break;
                    Sleep (10);
                }
            #else
                err = LockFileEx (osfh, flags, 0, lock->l_len, 0, &ov) ? 0 : -1;
            #endif
            break;

        case F_GETLK:
            // not implmented
            // fatal ("fcntl: Internal Error\n");
            exit (-1);
            break;

        default:
            break;
    }

    return err;
}

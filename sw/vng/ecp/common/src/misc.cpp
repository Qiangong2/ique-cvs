
#include "common.h"

#include "shr_trace.h"

#include <sstream>
#include <iomanip>

#ifndef _WIN32
   #include <sys/mman.h>   // mmap
   #include <openssl/md5.h>        // MD5 functions
#else
    #include <windows.h>
    #include "md5.h"
#endif


string tostring(unsigned i)
{
    ostringstream o;
    o << i;
    return o.str();
}

string tohexstring (unsigned long long int i, int width)
{
    string rv;
    ostringstream o;
    o << setfill('0') << setw (width);
    o << hex << nouppercase << right << i;
    return o.str();
}

string quotestring(const string& s)
{
    string res;
    res = "'";
    res += s;
    res += "'";
    return res;
}

static char *hextbl = "0123456789abcdef";

template <class ForwardIterator, class InsertIterator>
static int hex_encode(ForwardIterator begin, ForwardIterator end, 
                      InsertIterator ins)
{
    int count = 0;
    while (begin != end) {
        int v = *begin++;
        *ins++ = hextbl[(v >> 4) & 0xf];
        *ins++ = hextbl[v & 0xf];
        count+=2;
    }
    return count;
}

template <class ForwardIterator, class InsertIterator>
static int hex_decode(ForwardIterator begin, ForwardIterator end, 
                      InsertIterator ins)
{
    int  count = 0;
    int  byte = 0;
    int  inserted = 0;
    while (begin != end) {
        char c = *begin++;
        int val = 0;
        if (c >= '0' && c <= '9')
            val = c - '0';
        else if (c >= 'a' && c <= 'f')
            val = c - 'a' + 10;
        else if (c >= 'A' && c <= 'F')
            val = c - 'A' + 10;
        if (count % 2 == 0) 
            byte = val << 4;
        else {
            byte |= val;
            *ins++ = byte;
            inserted++;
        }
        count++;
    }
    if (count % 2 != 0) {
        *ins++ = byte;
        inserted++;
    }
    return inserted;
}

int hex_encode(const string& in, string& out)
{
    insert_iterator<string> ins(out, out.end());
    return hex_encode(in.begin(), in.end(), ins);
}

int hex_encode(const unsigned char *data, size_t len, string& out)
{
    insert_iterator<string> ins(out, out.end());
    return hex_encode(data, data + len, ins);
}


int hex_decode(const string& in, unsigned char *data, size_t len)
{
    vector<char> out;
    insert_iterator<vector<char> > ins(out, out.end());
    if (hex_decode(in.begin(), in.end(), ins) < 0)
        return -1;
    bcopy(&out[0], data, len);
    return (int)((out.size() > len) ? len : out.size());
}


int md5_sum(const void *buf, int size, string& chksum)
{
    MD5_CTX ctx;
    unsigned char md[MD5_DIGEST_LENGTH];

    MD5_Init(&ctx);
    MD5_Update(&ctx, (unsigned char *)buf, size);
    MD5_Final(md, &ctx);
    if (hex_encode(md, MD5_DIGEST_LENGTH, chksum) < 0)
        return -1;
    return 0;
}


int md5_sum(int ifd, string& chksum)
{
    MD5_CTX ctx;
    unsigned char md[MD5_DIGEST_LENGTH];
    unsigned char buf[4096];
    int n;
    
    MD5_Init(&ctx);
    while ((n = read(ifd, buf, sizeof(buf))) > 0) {
        MD5_Update(&ctx, buf, n);
    }
    if (n < 0) return -1;
    MD5_Final(md, &ctx);
    if (hex_encode(md, MD5_DIGEST_LENGTH, chksum) < 0)
        return -1;
    return 0;
}


int md5_sum(const string& filename, string& chksum)
{
    int ifd = open(filename.c_str(), O_RDONLY|_O_BINARY);
    if (ifd < 0) 
        return -1;
    int rval = md5_sum(ifd, chksum);
    close(ifd);
    return rval;
}

static inline void msglog(int level, char *fmt, ...)
{
    va_list ap;

    va_start (ap, fmt);
   _SHR_vtrace (level, 4, 0, fmt, ap);
    va_end (ap);
}



int load_string(const string& infile, string& out)
{
    int res = 0;
    unsigned filesize;

    #ifdef _WIN32
        HANDLE hFile = CreateFile(infile.c_str(),GENERIC_READ, 0, NULL,
                                      OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
        if (hFile == INVALID_HANDLE_VALUE) {
            msglog(MSG_INFO, "load_string: CreateFile returned %lu when opening file %s\n",
                              GetLastError(), infile.c_str());
            return -1;
        }

        HANDLE hmFile = CreateFileMapping(hFile,NULL,PAGE_READONLY,0,0,NULL);
        DWORD sizeHigh;
        filesize = GetFileSize(hFile, &sizeHigh);
        CloseHandle(hFile);
        if (!hmFile) {
            msglog(MSG_INFO, "load_string: CreateFileMapping error %lu for file %s\n",
                              GetLastError(), infile.c_str());
            return -1;
        }
        void* mapped = MapViewOfFile (hmFile,FILE_MAP_READ,0,0,0);
        CloseHandle(hmFile);
        if (sizeHigh) {
            res = -1; // should not happen.
            goto end;
        }
    #else
        int fd = open(infile.c_str(), O_RDONLY);
        if (fd < 0) {
            msglog(MSG_ERR, "load_string: can't open %s\n", infile.c_str());
            return -1;
        }
        struct stat sbuf;
        if (fstat(fd, &sbuf) != 0) {
            msglog(MSG_ERR, "load_string: %s fstat error\n", infile.c_str());
            return -1;
        }
    
        unsigned filesize = sbuf.st_size;

        void *mapped = 
            mmap(NULL, filesize, PROT_READ, MAP_PRIVATE, fd, 0);
        close(fd);
    
        if (mapped == NULL) {
            msglog(MSG_ERR, "load_string: can't map infile\n");
            res = -1;
            goto end;
        }
    #endif

    out.assign((const char*)mapped, filesize);

end:
    #ifdef _WIN32
        UnmapViewOfFile (mapped);
    #else
        munmap(mapped, filesize);
    #endif

    return res;
}


int save_buffer(const string& outfile, const void *buf, size_t bufsize)
{
    FILE *fp;
    if ((fp = fopen(outfile.c_str(), "wb")) == NULL) {
        msglog(MSG_ERR, "save_string: can't write to %s\n", outfile.c_str());
        return -1;
    }
    if (fwrite(buf, bufsize, 1, fp) != 1) {
        msglog(MSG_ERR, "save_string: fwrite to %s failed\n", outfile.c_str());
        return -1;
    }
    fclose(fp);
    return 0;
}



int save_string(const string& outfile, const string& in)
{
    return save_buffer(outfile, in.data(), in.size());
}


/*
  <  --  &lt
  >  --  &gt
  "  --  &quot
  '  --  &apos
  &  --  &amp
*/
int xml_escape(const string& in, string& out)
{
    out = "";
    for (unsigned i = 0; i < in.size(); i++) {
        switch (in[i]) {
        case '<':
            out += "&lt;";
            break;
        case '>':
            out += "&gt;";
            break;
        case '"':
            out += "&quot;";
            break;
        case '\'':
            out += "&apos;";
            break;
        case '&':
            out += "&amp;";
            break;
        default:
            out += in[i];
        }
    }
    return 0;
}


/* Endian conversion */

int rd_int_BE(const char *buf)
{
    const unsigned char *p = (const unsigned char *)buf;
    return 
        (p[0] << 24) +
        (p[1] << 16) +
        (p[2] << 8) +
        p[3];
}


void wr_int_BE(char *buf, int value)
{
    buf[0] = (value >> 24) & 0xff;
    buf[1] = (value >> 16) & 0xff;
    buf[2] = (value >> 8)  & 0xff;
    buf[3] = value & 0xff;
}


#ifdef _WIN32

int utf8ToWchar (LPCSTR utf8_in, wstring& wout)
{
    WCHAR *buf = NULL;
    int wlen;

    wout = L"";
    if (!utf8_in[0]) {
        return 0;
    }

    wlen = MultiByteToWideChar( CP_UTF8, 0, utf8_in, -1,  buf, 0);

    if (!wlen) {
        msglog (MSG_ERR, "utf8ToWchar: MultiByteToWideChar(---,0) failed: %d\n", GetLastError());
        return -1;
    }

    buf = (WCHAR*) malloc (wlen * sizeof(WCHAR));
    if (buf == NULL)
        return -1;

    wlen = MultiByteToWideChar( CP_UTF8, 0, utf8_in, -1, buf, wlen);
    if (!wlen) {
        msglog (MSG_ERR, "utf8ToWchar: MultiByteToWideChar failed: %d\n", GetLastError());
        free(buf);
        return -1;
    }

    wout = buf;
    free(buf);
    return 0;
}

int wcharToUtf8 (LPCWSTR w_in, string& utf8_out)
{
    char *buf = NULL;
    int  len;

    utf8_out = "";
    if (!w_in[0]) {
        return 0;
    }

    len = WideCharToMultiByte( CP_UTF8, 0, w_in, -1,  buf, 0, NULL, NULL);

    if (!len) {
        msglog (MSG_ERR, "wcharToUtf8: WideCharToMultiByte(---,0,0,0) failed: %d\n", GetLastError());
        return -1;
    }

    buf = (char*) malloc (len);
    if (buf == NULL)
        return -1;

    len = WideCharToMultiByte( CP_UTF8, 0, w_in, -1, buf, len, NULL, NULL);
    if (!len) {
        msglog (MSG_ERR, "wcharToUtf8: WideCharToMultiByte failed: %d\n", GetLastError());
        free(buf);
        return -1;
    }

    utf8_out = buf;
    free(buf);
    return 0;
}




#endif


int xlateChar (string& s, char toReplace, char replacement)
{
    int count = 0;
    string::size_type pos = 0;

    while ((pos=s.find(toReplace, pos)) != string::npos) {
        s[pos++] = replacement;
        ++count;
    }
    return count;
}




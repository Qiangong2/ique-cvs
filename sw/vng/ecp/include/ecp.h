 /*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __ECP_H__
#define __ECP_H__

#include "vng.h"


#if !defined(_GBA) && !defined(_SC)
#pragma pack(push, 4)
#endif


#ifdef __cplusplus
extern "C" {
#endif



#define ECP_GAME_ID          0x80000001
#define ECP_TITLE_ID         0  /* an invalid titleId */
#define ECP_GAME_KEYWORD     "ECP GAME KEYWORD"
#define ECP_GAME_ATTR_ID     6
#define ECP_GAME_ATTR_VALUE  10023989
#define ECP_GAME_STATUS      60248698
#define ECP_NUM_PLAYERS      9

/* Max 5 comma separated ecard strings */
#define ECP_ECARD_LEN        (6 + 10 + 10)
#define ECP_ECARD_BUF_SIZE   (ECP_ECARD_LEN + 1)



/* ECPInfoType definitions */
#define ECP_IT_CID                  100
#define ECP_IT_EC_META              200
#define ECP_IT_LIST_TITLES          300
#define ECP_IT_TITLE_DETAILS        400
#define ECP_IT_LIST_SUB_PRICES      500
#define ECP_IT_SUBSCRIBE            600
#define ECP_IT_UPDATE_STATUS        700
#define ECP_IT_REDEEM               800
#define ECP_IT_SYNC_TICKETS         900
#define ECP_IT_PURCHASE_TITLE      1000

#define NUM_ECP_INFO_TYPES  10

/* ECPRequest definitions */
#define ECP_RQ_GET_STAT               1
#define ECP_RQ_CACHE                  2
#define ECP_RQ_OPEN                   3
#define ECP_RQ_READ                   4
#define ECP_RQ_SEEK                   5
#define ECP_RQ_CLOSE                  6
#define ECP_RQ_REFRESH                7
#define ECP_RQ_DELETE                 8
#define ECP_RQ_CANCEL                 9

#define NUM_ECP_REQUESTS    9

/* VNServiceTag is computed as ECPInfoType + ECPRequest */

/* Convenience VNServiceTag definitions */
#define ECP_ST_GET_CACHE_STATUS_CID (ECP_IT_CID + ECP_RQ_GET_STAT)
#define ECP_ST_CACHE_CID            (ECP_IT_CID + ECP_RQ_CACHE   )
#define ECP_ST_OPEN_CID             (ECP_IT_CID + ECP_RQ_OPEN    )
#define ECP_ST_READ_CID             (ECP_IT_CID + ECP_RQ_READ    )
#define ECP_ST_SEEK_CID             (ECP_IT_CID + ECP_RQ_SEEK    )
#define ECP_ST_CLOSE_CID            (ECP_IT_CID + ECP_RQ_CLOSE   )
#define ECP_ST_REFRESH_CID          (ECP_IT_CID + ECP_RQ_REFRESH )
#define ECP_ST_DELETE_CID           (ECP_IT_CID + ECP_RQ_DELETE  )
#define ECP_ST_CANCEL_CID           (ECP_IT_CID + ECP_RQ_CANCEL  )

/* RPC service tags not specific to ECPInfoType's */
#define ECP_ST_CANCEL_WS_OPS          1
#define ECP_ST_ENABLE_PRELOAD         2


/* definition of data passed between gba/nc libec and pc ncp (i.e. ecp) */


typedef u32 ECPInfoType;

typedef struct {
    ECTitleKind      titleKind;
    ECPricingKind    pricingKind;
    s32              channelId;
    u32              nTitles;
    u32              skip;

} ECPListTitlesArg;


typedef struct {
    ESTitleId  titleId;

} ECPTitleDetailsArg;


typedef struct {
    u32            retBufSize;
    char           eCard[ECP_ECARD_BUF_SIZE];
    s32            channelId;
    s32            subscriptionLength;
    ECTimeUnit     subscriptionTimeUnit;
    s32            itemId;
    ECPrice        price;
    u32            deviceCertLen;

    /* device certificate follows deviceCertLen */

} ECPSubscribeArg;


typedef struct {
    u32            retBufSize;
    char           eCard[ECP_ECARD_BUF_SIZE];
    u32            deviceCertLen;

    /* device certificate follows deviceCertLen */

} ECPRedeemArg;


typedef struct {
    u32               retBufSize;
    ESTitleId         titleId;
    s32               itemId;
    ECPrice           price;
    ECPayment         payment;
    ESLpEntry         limits[ES_MAX_LIMIT_TYPE];
    u32               nLimits;
    ECAccountPayment  account;
    u32               deviceCertLen;

    /* device certificate follows deviceCertLen */

} ECPPurchaseTitleArg;


typedef struct {
    u32            retBufSize;
    u32            deviceCertLen;

    /* device certificate follows deviceCertLen */

} ECPSyncTicketsArg;


/* The NC only reads sizeof ECPTickeRet on the 1st read
 * of the cached response data for EC_Subscribe, EC_Redeem, or EC_SyncTickets.
 *
 * It then reads  sizeof Eticket and writes it directly
 * to *ECSubscrbeOp.ticket from the VN_SendRPC ret.
 *
 * It then reads certsSize and directs it to *ECSubscribeOp.certs.
 *
 * Finally, it stores certsSize at *ECSubscrie.certsSize
 */
typedef struct {
    u32           nTickets;  // Only expected value is 1
    u32           certsSize;
/*
    u8            tickets [1 * sizeof ETicket];
    u8            certs [certsSize];
*/

} ECPTickeRet;






typedef struct {
    u32  nConsumptions; /* only this many actually transnmitted */
    ECConsumption consumptions [EC_MAX_CONSUMPTIONS];

} ECPUpdateStatusArg;



typedef struct {
    ECContentId cid;

} ECPOpenCachedContentArg;



typedef struct {
    ECContentId cid;
    u32         count;   /* max amount to return */

} ECPReadCachedContentArg;

typedef struct {
    u8    buf /*[]*/;

} ECPReadCachedContenRet;


typedef struct {
    ECContentId cid;
    u32         offset;

} ECPSeekCachedContentArg;


typedef struct {
    ECContentId cid;

} ECPCloseCachedContentArg;


typedef struct {
    ECContentId cid;

} ECPUnlinkCachedContentArg;


typedef struct {
    ECContentId cid /*[nCids]*/;

} ECPCacheContentArg;


typedef struct {
    ECContentId cid /*[nCids]*/;

} ECPCancelCacheContentArg;


typedef struct {
    ECContentId cid /*[nCids]*/;

} ECPGetCachedContentStatusArg;

typedef struct {
    ECCacheStatus cacheStatus /*[nCids]*/;

} ECPGetCachedContentStatusRet;


typedef struct {
    ECContentId cid;

} ECPRefreshCachedContentArg;


#ifdef __cplusplus
}
#endif //__cplusplus



#if !defined(_GBA) && !defined(_SC)
#pragma pack(pop)
#endif


#endif /*__ECP_H__*/

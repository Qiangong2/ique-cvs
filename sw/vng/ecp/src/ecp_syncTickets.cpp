/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#include "ecp_i.h"
#include "ecp_ecsIfc.h"
#include "common.h"




int ECPSyncTicketsCache::cacheSyncTickets ()
{
    ECPEnv   &ecp = __ecpEnv;
    int      rv = 0;

    netReq = new ECPSyncTicketsReq;

    if (!netReq) {
        rv = EC_ERROR_NOMEM;
        goto end;
    }

    ECPSyncTicketsReq *x = (ECPSyncTicketsReq*) netReq;

    ecs__SyncETicketsRequestType&  req  = x->req;
    ecs__SyncETicketsResponseType& resp = x->resp;

    initECSReq (req, deviceId);
    
    req.LastSyncTime = NULL; // set optional element to 0

    x->deviceCert = deviceCert;
    deviceCert = NULL;
    x->deviceCertLen = cached.deviceCertLen;
    
    req.DeviceCert.__ptr = x->deviceCert;
    req.DeviceCert.__size = x->deviceCertLen;

    // start thread to call service in background

    status.cachedSize = 0;
    expireTime = 0;

    if ((rv = _SHR_thread_create (&netThread, NULL,
                                    (_SHRThreadFunc) syncTicketsThread, x))) {
        trace (ERR, ECWS, "cacheSyncTickets _SHR_thread_create returned %d", rv);
        netThread = 0;
        rv = EC_ERROR_ECP;
        goto end;
    }

    ecp.isWebServiceActive = true;

end:
    if (rv && netReq) {
        if (x->deviceCert) {
            delete [] x->deviceCert;
        }
        delete netReq;
        netReq = NULL;
    }

    return rv;
}



// static member
ECError ECPSyncTicketsCache::syncTicketsThread (ECPSyncTicketsReq *netReq)
{
    return __ecpEnv.syncTicketsCache.accessNet (netReq);
}


ECError ECPSyncTicketsCache::accessNet (ECPSyncTicketsReq *ws)
{
    ECPEnv     &ecp = __ecpEnv;
    unsigned    i;
    size_t      nCerts;
    unsigned    nTickets;
    int         nNullPtrs;
    int         rv;
    const char *msg = "<No message provided>";
    string      res;
    string      tmp;

    ECommerceSOAPBinding& ecs = *ws->ecs;

    ecs__SyncETicketsRequestType&  req  =  ws->req;
    ecs__SyncETicketsResponseType& resp =  ws->resp;

    rv = ecs.__ecs__SyncETickets(&req, &resp);

    lock(ecp.mutex);

    if (netReq != ws) {
        rv = EC_ERROR_NOT_BUSY;
        goto canceled;
    }

    if (rv != SOAP_OK) {
        soap_print_fault (ecs.soap, stdout);
        fflush(stdout);
        rv = EC_ERROR_ECS_NA;
        goto end;
    }

    if (resp.ErrorCode != 0) {
        if (resp.ErrorMessage && !resp.ErrorMessage->empty()) {
            msg = resp.ErrorMessage->c_str();
        }
        trace (INFO, ECWS, "SyncETickets resp.ErrorCode %d: %s\n",
                resp.ErrorCode, msg);
        rv = EC_ECS_ERROR_RANGE_START - resp.ErrorCode;
        goto end;
    }

    trace (INFO, ECWS, "Got response to soap SyncETickets request\n");

    nCerts =  resp.Certs.size();
    nTickets = (unsigned) resp.ETickets.size();

    /* gsoap automatically converts ticket and cert from base64 to binary */
    
    for (nNullPtrs = i = 0;  i < nTickets;  ++i) {
        if (!resp.ETickets[i].__ptr) {
            trace (WARN, ECWS, "resp.Certs[i].__ptr is NULL\n", i);
            ++nNullPtrs;
            continue;
        }
        tmp.clear();
        tmp.assign((const char*)resp.ETickets[i].__ptr, resp.ETickets[i].__size);
        res += tmp;        
    }

    size_t headLen = sizeof(ECPTickeRet);
    size_t ticketLen = res.size();
    nTickets -= nNullPtrs;

    for (i = 0;  i < nCerts;  ++i) {
        if (!resp.Certs[i].__ptr) {
            trace (WARN, ECWS, "resp.Certs[i].__ptr is NULL\n", i);
            continue;
        }
        tmp.clear();
        tmp.assign((const char*)resp.Certs[i].__ptr, resp.Certs[i].__size);
        res += tmp;        
    }

    size_t certsLen = res.size() - ticketLen;
    retbufLen = headLen + ticketLen + certsLen;
    
    // Expect zero or 1 tickets.
    // More than 1 is not currently expected but possible in the wsdl

    if (nTickets && !certsLen) {
        trace (WARN, ECWS, "SyncETickets expected certs but "
               "gsoap Certs vector size is %d and certsLen is 0\n", nCerts);
    }

    char *b = new char [retbufLen];
    
    res.copy (&b[headLen], retbufLen - headLen);

    retbuf = (ECPTickeRet*) b;
    
    retbuf->nTickets = nTickets;
    retbuf->certsSize = (u16) certsLen;
    
    status.totalSize = status.cachedSize = (u32) retbufLen;
    cachedTime = now();
    expireTime = cachedTime + cacheDur;

    trace (INFO, ECWS, "Successfully processed soap SyncETickets request\n    "
           "soap ticket vector size %u  nTickets %u  ticketLen %u  "
           "nCerts %d  certsLen %u  retbufLen %u\n",
           (unsigned) resp.ETickets.size(),
           retbuf->nTickets, ticketLen, nCerts, retbuf->certsSize, retbufLen);

end:

    if (rv < 0) {
        status.cachedSize = rv;
        if (retbuf) {
            delete [] retbuf;
            retbuf = NULL;
        }
    }

    if (ws->deviceCert) {
        delete [] ws->deviceCert;
    }

    if (netReq == ws) {
        netReq = NULL;
    }
    ecp.isWebServiceActive = false;

canceled:
    delete ws;
    unlock(ecp.mutex);

    return 0; // doesn't go anywhere
}






ECError ECPSyncTicketsCache::getStat(ECPSyncTicketsArg& id, ECCacheStatus& stat)
{
    if (netThread && !netReq) {
        ecpCloseThreadHandle (netThread);
        netThread = 0;
    }

    if (status.cachedSize != EC_ERROR_NOT_CACHED
            && (expireTime == 0 || now() < expireTime)) {
        stat = status;
    } else {
        stat.cachedSize = EC_ERROR_NOT_CACHED;
        stat.totalSize = EC_ERROR_INVALID;
    }

    return EC_ERROR_OK;
}

ECError ECPSyncTicketsCache::cache(ECPSyncTicketsArg& id)
{
    // delete current cached SyncTickets response
    // SyncTickets via web service
    // cache result for client to retrieve

    ECError   rv = EC_ERROR_OK;

    unlink(id);
    cached = id;
    deviceCert = new unsigned char [id.deviceCertLen];
    memcpy (deviceCert, &id + 1, id.deviceCertLen);
    rv = cacheSyncTickets();
        
    return rv;
}


ECError ECPSyncTicketsCache::open    (ECPSyncTicketsArg& id)
{
    ECError        rv;
    ECCacheStatus  stat;

    if (!(rv = getStat (id, stat))) {
        // it is either all there, or not cached
        if (stat.cachedSize < 0) {
            rv = EC_ERROR_NOT_CACHED;
        }
        else {
            isOpen = true;
            offset = 0;
        }
    }

    return rv;
}


ECError ECPSyncTicketsCache::read    (ECPSyncTicketsArg& id, void *ret, size_t *retLen)
{
    if (!isOpen) {
        return EC_ERROR_NOT_OPEN;
    }

    // It is either all there or none is there.
    if (status.cachedSize < 0) {
        return EC_ERROR_NOT_CACHED;
    }

    if (offset >= retbufLen) {
        *retLen = 0;
        return EC_ERROR_OK;
    }
    
    char *b = (char *) retbuf;
    
    size_t n = retbufLen - offset;
    
    if (n > VN_MAX_MSG_LEN) {
        n = VN_MAX_MSG_LEN;
    }

    if (n > id.retBufSize) {
        n = id.retBufSize;
    }
    
    memcpy (ret, &b[offset], n);
    
    offset += n;
    *retLen = n;

    return EC_ERROR_OK;
}


ECError ECPSyncTicketsCache::seek    (ECPSyncTicketsArg& id)
{
    // This is never called in actual practice
    
    return EC_ERROR_NOT_SUPPORTED;
}


ECError ECPSyncTicketsCache::close   (ECPSyncTicketsArg& id)
{
    if (!isOpen) {
        return EC_ERROR_NOT_OPEN;
    }

    isOpen = false;

    if (expireOnClose) {
        expireTime = 1;
    }

    return EC_ERROR_OK;
}



ECError ECPSyncTicketsCache::unlink  (ECPSyncTicketsArg& id)
{
    // This is never called from the NC in actual practice

    close(id);

    if (deviceCert) {
        delete [] deviceCert;
        deviceCert = NULL;
    }

    if (retbuf) {
        delete [] retbuf;
        retbuf = NULL;
    }

    status.cachedSize = EC_ERROR_NOT_CACHED;
    status.totalSize = EC_ERROR_INVALID;
    isOpen = false;
    offset = 0;

    return EC_ERROR_OK;
}





int32_t  ecpSyncTickets_GetStat (VN         *vn,
                                VNMsgHdr   *hdr,
                                const void *arg,
                                size_t      argLen,
                                void       *ret,
                                size_t     *retLen)
{
    ECError   rv;
    ECPEnv   &ecp = __ecpEnv;

    if (!arg || !ret || !retLen || argLen != sizeof (ECPSyncTicketsArg)) {
        rv = EC_ERROR_ECP;
        goto end;
    }

    rv = ecp.syncTicketsCache.getStat (*(ECPSyncTicketsArg*) arg,
                                        *(ECCacheStatus*) ret);

    if (!rv) {
        *retLen = sizeof(ECCacheStatus);
    }

end:
    return rv;
}


/*
 *  get etickets via ecs web service and cache response for
 *  retrieval by client
 */
int32_t  ecpSyncTickets_Cache (VN         *vn,
                              VNMsgHdr   *hdr,
                              const void *arg,
                              size_t      argLen,
                              void       *ret,
                              size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;
    VNGErrCode ec;
    ECPSyncTicketsArg* a= (ECPSyncTicketsArg*) arg;

    if (ecp.isWebServiceActive) {
        rv = EC_ERROR_BUSY;
    }
    else if (!arg || argLen != sizeof(ECPSyncTicketsArg) + a->deviceCertLen) {
        rv = EC_ERROR_ECP;
    }
    else if ((ec = VN_GetMemberDeviceId(vn, hdr->sender,
                                          &ecp.syncTicketsCache.deviceId))) {
        trace (ERR, ECWS,
               "list titles VN_GetMemberDeviceId returned error %d\n", ec);
        rv = EC_ERROR_ECP;
    }
    else {
        rv = ecp.syncTicketsCache.cache(*a);
    }

    return rv;
}

int32_t  ecpSyncTickets_Open (VN         *vn,
                             VNMsgHdr   *hdr,
                             const void *arg,
                             size_t      argLen,
                             void       *ret,
                             size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (!arg || argLen != sizeof(ECPSyncTicketsArg)) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.syncTicketsCache.open (*(ECPSyncTicketsArg*) arg);
    }

    return rv;
}

int32_t  ecpSyncTickets_Read (VN         *vn,
                             VNMsgHdr   *hdr,
                             const void *arg,
                             size_t      argLen,
                             void       *ret,
                             size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (!ret || !retLen || !arg || argLen != sizeof(ECPSyncTicketsArg)) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.syncTicketsCache.read (*(ECPSyncTicketsArg*)arg, ret, retLen);
    }

    return rv;
}

int32_t  ecpSyncTickets_Seek (VN         *vn,
                             VNMsgHdr   *hdr,
                             const void *arg,
                             size_t      argLen,
                             void       *ret,
                             size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (!arg || argLen != sizeof(ECPSyncTicketsArg)) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.syncTicketsCache.seek (*(ECPSyncTicketsArg*) arg);
    }

    return rv;
}

int32_t  ecpSyncTickets_Close (VN         *vn,
                              VNMsgHdr   *hdr,
                              const void *arg,
                              size_t      argLen,
                              void       *ret,
                              size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (!arg || argLen != sizeof(ECPSyncTicketsArg)) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.syncTicketsCache.close (*(ECPSyncTicketsArg*) arg);
    }

    return rv;
}

int32_t  ecpSyncTickets_Refresh (VN         *vn,
                                VNMsgHdr   *hdr,
                                const void *arg,
                                size_t      argLen,
                                void       *ret,
                                size_t     *retLen)
{
    return EC_ERROR_NOT_SUPPORTED;
}

/* Not very useful for SyncTickets */
int32_t  ecpSyncTickets_Unlink (VN         *vn,
                               VNMsgHdr   *hdr,
                               const void *arg,
                               size_t      argLen,
                               void       *ret,
                               size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (!arg || argLen != sizeof(ECPSyncTicketsArg)) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.syncTicketsCache.unlink (*(ECPSyncTicketsArg*) arg);
    }

    return rv;
}

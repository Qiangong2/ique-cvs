/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __ECP_CACHE_H__
#define __ECP_CACHE_H__

#pragma once

#include "ecp.h"
#include "ecsClientIfc.h"
#include "NetContent.h"





// CCId - cache content id (not same as ESContentId).  A type, struct,
//        or class passed to ECPCache member functions as a filter on
//        the cached info.  For example a list of ESContentIds or the parameter
//        values of a list titles request (i.e. title kind - game, manual,
//        pricing kind, etc).
//

template <typename CCId,
          typename CCIdCache,
          typename CCIdCount,
          typename CCIdList>

class ECPCache {

public:

    virtual ECError getStat (CCId      id, ECCacheStatus& stat) = 0;
    virtual ECError cache   (CCIdCache id) = 0;
    virtual ECError open    (CCId      id) = 0;
    virtual ECError read    (CCIdCount id, void *ret, size_t *retLen) = 0;
    virtual ECError seek    (CCIdCount id) = 0;
    virtual ECError close   (CCId      id) = 0;
    virtual ECError unlink  (CCId      id) = 0;
    virtual ECError cancel  (CCIdList *id) = 0;

    virtual ECError refresh (CCId id, CCIdCache ca)
    {
        ECError rv;

        if (!(rv = close(id))
                 && !(rv = unlink(id))) {
            rv = cache(ca);
        }
        return rv;
    }

};




struct ECPContentListArg{
    bool          fg;
    int           count;
    ECContentId  *cids;
};

struct ECPContentCountArg {
    ECContentId  cid;
    u32          count;
};




class ECPContentCache :
        public ECPCache<ECContentId,
                        ECPContentListArg&,
                        ECPContentCountArg&,
                        ECPContentListArg> {
public:

    // contents are cached in files

    string  isOpen;
    int     position;


    NetContent*  activeNetContentBg;
    NetContent*  activeNetContentFg;

    bool   activeNetContentFgWaiting;
    time_t timeItsOkToStartBg;
    int    secAfterFgFinishesTilOkToStartBg;

    deque <string> bgNetCids;  // to get from cds
    typedef deque<string>::iterator bgNetCids_iterator;
    vector <string> fgNetCids; // to get from cds before bgNetCids
    typedef vector<string>::iterator fgNetCids_iterator;

    hash_map<string, int>  lastNetResult;
    typedef hash_map<string, int>::iterator lastNetResult_iterator;
    ECError getLastNetResult (const string&  content_id, const string& abspath);

    ECError  monitorNetContent();

    int bgCacheNetContent (const char* content_id, bool front=false);
    bgNetCids_iterator bgNetCidsFind (string& content_id);
    fgNetCids_iterator fgNetCidsFind (string& content_id);

    int getCachedContents(IDSET& cachedContents);


    ECError getStat (ECContentId         cid, ECCacheStatus& cs);
    ECError cache   (ECPContentListArg&  arg);
    ECError open    (ECContentId         cid);
    ECError read    (ECPContentCountArg& arg, void *ret, size_t *retLen);
    ECError seek    (ECPContentCountArg& arg);
    ECError close   (ECContentId         cid);
    ECError unlink  (ECContentId         cid);
    ECError cancel  (ECPContentListArg  *cid);

    ECError refresh (ECContentId cid, ECPContentListArg ca)
    {
        ECError rv;

        if (!(rv = close(cid))
                 && !(rv = unlink(cid))) {
            ECPContentListArg ca;
            ca.fg = true;
            ca.count = 1;
            ca.cids = &cid;
            rv = cache(ca);
        }
        return rv;
    }

    ECPContentCache () :
        activeNetContentBg (NULL),
        activeNetContentFg (NULL),
        activeNetContentFgWaiting (false),
        timeItsOkToStartBg (0),
        secAfterFgFinishesTilOkToStartBg (30)
    {
    }
};







class BgWebSvcThArg  {

  public:
    ECommerceSOAPBinding *ecs;
    ECError               status;

    BgWebSvcThArg ();

    virtual ~BgWebSvcThArg ();

};



template <typename CCId,
          typename CCIdCache,
          typename CCIdCount,
          typename CCIdList>

class BgWebSvcCache :

        public ECPCache<CCId,
                        CCIdCache,
                        CCIdCount,
                        CCIdList> {

public:

    ECCacheStatus   status;
    int64_t         cachedTime;
    int64_t         expireTime;
    int64_t         cacheDur;
    bool            expireOnClose;

    bool            isOpen;
    size_t          offset;

    uint64_t        deviceId;
   _SHRThread       netThread;
    BgWebSvcThArg  *netReq;
    
    BgWebSvcCache ()
    {
        status.cachedSize = EC_ERROR_NOT_CACHED;  // means cached is not valid
        status.totalSize  = EC_ERROR_INVALID;
        cachedTime = 0;
        expireTime = 0;
        cacheDur = 1000 * 60 * 15; // default 15 min
        expireOnClose = true;
        isOpen = false;
        offset = 0;

        deviceId = VNP_INVALID_DEVICE_ID;
        netThread = 0;
        netReq = NULL;
    }

    virtual ~BgWebSvcCache ()
    {
        if (netReq) {
            delete netReq;
        }
    }

    ECError cancel  (CCIdList *id); // virtual func redefinition
};



class ECPListTitlesReq : public BgWebSvcThArg {

  public:

    ecs__ListTitlesRequestType  req;
    ecs__ListTitlesResponseType resp;

    ecs__TitleKindType    titleKind;
    ecs__PricingKindType  pricingKind;
    string                channelId;
};


class ECPListTitlesCache :
   public BgWebSvcCache <ECPListTitlesArg&,
                         ECPListTitlesArg&,
                         ECPListTitlesArg&,
                         ECPListTitlesArg> {
public:

    // Cache only most recent request in memory and file
    
    ECPListTitlesArg  cached;

    vector <ECListTitlesInfo*> titles;

    ECPListTitlesCache ()
    {
        memset (&cached, 0, sizeof cached);
        cacheDur = 1000 * 60 * 60 * 24 * 1;   // 1 day
    }

    ECError cacheListTitles();

    ECError accessNet (ECPListTitlesReq *netReq);
    static ECError listTitlesThread (ECPListTitlesReq *netReq); // calls accessNet

    ECError getStat (ECPListTitlesArg&  id, ECCacheStatus& stat);
    ECError cache   (ECPListTitlesArg&  id);
    ECError open    (ECPListTitlesArg&  id);
    ECError read    (ECPListTitlesArg&  id, void *ret, size_t *retLen);
    ECError seek    (ECPListTitlesArg&  id);
    ECError close   (ECPListTitlesArg&  id);
    ECError unlink  (ECPListTitlesArg&  id);
 // ECError cancel  (ECPListTitlesArg  *id);
};






class ECPTitleDetailsReq : public BgWebSvcThArg {

  public:

    ecs__GetTitleDetailsRequestType req;
    ecs__GetTitleDetailsResponseType resp;

};


class ECPGetTitleDetailsCache :
   public BgWebSvcCache <ECPTitleDetailsArg,
                         ECPTitleDetailsArg,
                         ECPTitleDetailsArg,
                         ECPTitleDetailsArg> {
public:

    // Cache only most recent request in memory
    
    ECPTitleDetailsArg  cached;

    ECTitleDetails    details;

    ECPGetTitleDetailsCache ()
    {
        memset (&cached, 0, sizeof cached);
        cacheDur = 1000 * 60 * 60 * 24 * 1;   // 1 day
    }

    ECError cacheTitleDetails();

    ECError accessNet (ECPTitleDetailsReq *netReq);
    static ECError titleDetailsThread (ECPTitleDetailsReq *netReq); // calls accessNet

    ECError getStat (ECPTitleDetailsArg  id, ECCacheStatus& stat);
    ECError cache   (ECPTitleDetailsArg  id);
    ECError open    (ECPTitleDetailsArg  id);
    ECError read    (ECPTitleDetailsArg  id, void *ret, size_t *retLen);
    ECError seek    (ECPTitleDetailsArg  id);
    ECError close   (ECPTitleDetailsArg  id);
    ECError unlink  (ECPTitleDetailsArg  id);
 // ECError cancel  (ECPTitleDetailsArg *id);
};






// There is no Id (i.e. filter) for cached meta data as there is only
// one type of meta data and it is either cached or not.
// So the CCId for meta data is only to match the 
// general scheme of an ECPCache descendant
//
typedef const void* ECPMetaCacheId;

class ECPGetMetaReq : public BgWebSvcThArg {

  public:

    ecs__GetSystemUpdateRequestType req;
    ecs__GetSystemUpdateResponseType resp;

};


class ECPGetMetaCache :
   public BgWebSvcCache <ECPMetaCacheId,
                         ECPMetaCacheId,
                         ECPMetaCacheId,
                         ECPMetaCacheId> {
public:

    // Cache only most recent request in memory

    ECMeta   ecMeta;
    
    string                           contentPrefixURL;
    string                           uncachedContentPrefixURL;
    vector<ecs__TitleVersionType * > sysSoft;
    vector<string>                   preloadContentIds; // titleId.contentId hex

    ECPGetMetaCache ()
    {
        memset (&ecMeta, 0, sizeof ecMeta);
    }

    ECError cacheECMeta();

    ECError accessNet (ECPGetMetaReq *netReq);
    static ECError getECMetaThread (ECPGetMetaReq *netReq); // calls accessNet

    ECError getStat (ECPMetaCacheId  id, ECCacheStatus& stat);
    ECError cache   (ECPMetaCacheId  id);
    ECError open    (ECPMetaCacheId  id);
    ECError read    (ECPMetaCacheId  id, void *ret, size_t *retLen);
    ECError seek    (ECPMetaCacheId  id);
    ECError close   (ECPMetaCacheId  id);
    ECError unlink  (ECPMetaCacheId  id);
 // ECError cancel  (ECPMetaCacheId *id);
};





// There is no Id (i.e. filter) for cached subscription pricings.
// It is either cached or not for a specific deviceId.
// So the CCId for ECPListSubPricingsCache is only to match the 
// general scheme of an ECPCache descendant
//
typedef const void* ECPListSubPricingsId;

class ECPListSubPricingsReq : public BgWebSvcThArg {

  public:

    ecs__ListSubscriptionPricingsRequestType req;
    ecs__ListSubscriptionPricingsResponseType resp;

};


class ECPListSubPricingsCache :
   public BgWebSvcCache <ECPListSubPricingsId,
                         ECPListSubPricingsId,
                         ECPListSubPricingsId,
                         ECPListSubPricingsId> {
public:

    // Cache only most recent request in memory

    vector<ECSubscriptionPricing>   pricings;
    
    ECPListSubPricingsCache ()
    {
        cacheDur = 1000 * 60 * 60 * 24 * 1;   // 1 day
    }

    ECError cacheSubPricings();

    ECError accessNet (ECPListSubPricingsReq *netReq);
    static ECError subPricingsThread (ECPListSubPricingsReq *netReq); // calls accessNet

    ECError getStat (ECPListSubPricingsId  id, ECCacheStatus& stat);
    ECError cache   (ECPListSubPricingsId  id);
    ECError open    (ECPListSubPricingsId  id);
    ECError read    (ECPListSubPricingsId  id, void *ret, size_t *retLen);
    ECError seek    (ECPListSubPricingsId  id);
    ECError close   (ECPListSubPricingsId  id);
    ECError unlink  (ECPListSubPricingsId  id);
 // ECError cancel  (ECPListSubPricingsId *id);
};







class ECPSubscribeReq : public BgWebSvcThArg {

  public:

    ecs__SubscribeRequestType  req;
    ecs__SubscribeResponseType resp;

    ecs__PriceType price;
    ecs__PaymentType payment;
    ecs__ECardPaymentType eCardPayment;
    ecs__TimeDurationType subscriptionLength;
    unsigned char *deviceCert;
    u32 deviceCertLen;
};


class ECPSubscribeCache :
   public BgWebSvcCache <ECPSubscribeArg&,
                         ECPSubscribeArg&,
                         ECPSubscribeArg&,
                         ECPSubscribeArg> {
public:

    // Cache only most recent request in memory
    
    ECPSubscribeArg  cached;
    
    unsigned char *deviceCert;
    
    ECPTickeRet  *retbuf; /* plus ticket and certs */
    size_t            retbufLen;

    ECPSubscribeCache ()
    {
        memset (&cached, 0, sizeof cached);
    }

    ECError cacheSubscribe();

    ECError accessNet (ECPSubscribeReq *netReq);
    static ECError subscribeThread (ECPSubscribeReq *netReq); // calls accessNet

    ECError getStat (ECPSubscribeArg&  id, ECCacheStatus& stat);
    ECError cache   (ECPSubscribeArg&  id);
    ECError open    (ECPSubscribeArg&  id);
    ECError read    (ECPSubscribeArg&  id, void *ret, size_t *retLen);
    ECError seek    (ECPSubscribeArg&  id);
    ECError close   (ECPSubscribeArg&  id);
    ECError unlink  (ECPSubscribeArg&  id);
 // ECError cancel  (ECPSubscribeArg  *id);
};






class ECPRedeemReq : public BgWebSvcThArg {

  public:

    ecs__RedeemECardRequestType  req;
    ecs__RedeemECardResponseType resp;

    ecs__ECardPaymentType eCardPayment;
    unsigned char *deviceCert;
    u32 deviceCertLen;
};


class ECPRedeemCache :
   public BgWebSvcCache <ECPRedeemArg&,
                         ECPRedeemArg&,
                         ECPRedeemArg&,
                         ECPRedeemArg> {
public:

    // Cache only most recent request in memory
    
    ECPRedeemArg  cached;
    
    unsigned char *deviceCert;
    
    ECPTickeRet   *retbuf; /* plus ticket and certs */
    size_t         retbufLen;

    ECPRedeemCache ()
    {
        memset (&cached, 0, sizeof cached);
    }

    ECError cacheRedeem();

    ECError accessNet (ECPRedeemReq *netReq);
    static ECError redeemThread (ECPRedeemReq *netReq); // calls accessNet

    ECError getStat (ECPRedeemArg&  id, ECCacheStatus& stat);
    ECError cache   (ECPRedeemArg&  id);
    ECError open    (ECPRedeemArg&  id);
    ECError read    (ECPRedeemArg&  id, void *ret, size_t *retLen);
    ECError seek    (ECPRedeemArg&  id);
    ECError close   (ECPRedeemArg&  id);
    ECError unlink  (ECPRedeemArg&  id);
 // ECError cancel  (ECPRedeemArg  *id);
};






class ECPPurchaseTitleReq : public BgWebSvcThArg {

  public:

    ecs__PurchaseTitleRequestType  req;
    ecs__PurchaseTitleResponseType resp;

    ecs__PriceType price;
    ecs__PaymentType payment;

    ecs__ECardPaymentType eCardPayment;
    ecs__AccountPaymentType accountPayment;
    ecs__CreditCardPaymentType creditCardPayment;

    ecs__LimitType limits[ES_MAX_LIMIT_TYPE];

    unsigned char *deviceCert;
    u32 deviceCertLen;
};


class ECPPurchaseTitleCache :
   public BgWebSvcCache <ECPPurchaseTitleArg&,
                         ECPPurchaseTitleArg&,
                         ECPPurchaseTitleArg&,
                         ECPPurchaseTitleArg> {
public:

    // Cache only most recent request in memory
    
    ECPPurchaseTitleArg  cached;
    
    unsigned char *deviceCert;
    
    ECPTickeRet  *retbuf; /* plus ticket and certs */
    size_t            retbufLen;

    ECPPurchaseTitleCache ()
    {
        memset (&cached, 0, sizeof cached);
    }

    ECError cachePurchaseTitle();

    ECError accessNet (ECPPurchaseTitleReq *netReq);
    static ECError purchaseTitleThread (ECPPurchaseTitleReq *netReq); // calls accessNet

    ECError getStat (ECPPurchaseTitleArg&  id, ECCacheStatus& stat);
    ECError cache   (ECPPurchaseTitleArg&  id);
    ECError open    (ECPPurchaseTitleArg&  id);
    ECError read    (ECPPurchaseTitleArg&  id, void *ret, size_t *retLen);
    ECError seek    (ECPPurchaseTitleArg&  id);
    ECError close   (ECPPurchaseTitleArg&  id);
    ECError unlink  (ECPPurchaseTitleArg&  id);
 // ECError cancel  (ECPPurchaseTitleArg  *id);
};






class ECPSyncTicketsReq : public BgWebSvcThArg {

  public:

    ecs__SyncETicketsRequestType  req;
    ecs__SyncETicketsResponseType resp;

    unsigned char *deviceCert;
    u32 deviceCertLen;
};


class ECPSyncTicketsCache :
   public BgWebSvcCache <ECPSyncTicketsArg&,
                         ECPSyncTicketsArg&,
                         ECPSyncTicketsArg&,
                         ECPSyncTicketsArg> {
public:

    // Cache only most recent request in memory
    
    ECPSyncTicketsArg  cached;
    
    unsigned char *deviceCert;
    
    ECPTickeRet   *retbuf; /* plus tickets and certs */
    size_t         retbufLen;

    ECPSyncTicketsCache ()
    {
        memset (&cached, 0, sizeof cached);
    }

    ECError cacheSyncTickets();

    ECError accessNet (ECPSyncTicketsReq *netReq);
    static ECError syncTicketsThread (ECPSyncTicketsReq *netReq); // calls accessNet

    ECError getStat (ECPSyncTicketsArg&  id, ECCacheStatus& stat);
    ECError cache   (ECPSyncTicketsArg&  id);
    ECError open    (ECPSyncTicketsArg&  id);
    ECError read    (ECPSyncTicketsArg&  id, void *ret, size_t *retLen);
    ECError seek    (ECPSyncTicketsArg&  id);
    ECError close   (ECPSyncTicketsArg&  id);
    ECError unlink  (ECPSyncTicketsArg&  id);
 // ECError cancel  (ECPSyncTicketsArg  *id);
};






class ECPUpdateStatusReq : public BgWebSvcThArg {

  public:

    ecs__UpdateStatusRequestType  req;
    ecs__UpdateStatusResponseType resp;
    
    ecs__ConsumptionType consumptions[EC_MAX_CONSUMPTIONS];

};


class ECPUpdateStatusCache :
   public BgWebSvcCache <ECPUpdateStatusArg&,
                         ECPUpdateStatusArg&,
                         ECPUpdateStatusArg&,
                         ECPUpdateStatusArg> {
public:

    // Cache only most recent request in memory until read by NC
    
    ECPUpdateStatusArg  update;
    
    ECPUpdateStatusCache ()
    {
        memset (&update, 0, sizeof update);
    }

    ECError cacheUpdateStatus();

    ECError accessNet (ECPUpdateStatusReq *netReq);
    static ECError updateStatusThread (ECPUpdateStatusReq *netReq); // calls accessNet

    ECError getStat (ECPUpdateStatusArg&  id, ECCacheStatus& stat);
    ECError cache   (ECPUpdateStatusArg&  id);
    ECError open    (ECPUpdateStatusArg&  id);
    ECError read    (ECPUpdateStatusArg&  id, void *ret, size_t *retLen);
    ECError seek    (ECPUpdateStatusArg&  id);
    ECError close   (ECPUpdateStatusArg&  id);
    ECError unlink  (ECPUpdateStatusArg&  id);
 // ECError cancel  (ECPUpdateStatusArg  *id);
};





#endif /*__ECP_CACHE_H__*/

/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#include "ecp_i.h"
#include "ecp_ecsIfc.h"

static const char slash  = '/';


int ECPGetTitleDetailsCache::cacheTitleDetails ()
{
    ECPEnv   &ecp = __ecpEnv;
    int      rv = 0;

    netReq = new ECPTitleDetailsReq;

    if (!netReq) {
        rv = EC_ERROR_NOMEM;
        goto end;
    }

    ECPTitleDetailsReq *x = (ECPTitleDetailsReq*) netReq;

    ecs__GetTitleDetailsRequestType&  req  = x->req;
    ecs__GetTitleDetailsResponseType& resp = x->resp;

    initECSReq (req, deviceId);

    req.TitleId   = tohexstring(cached.titleId, 16);

    // start thread to call service in background

    status.cachedSize = 0;
    expireTime = 0;

    if ((rv = _SHR_thread_create (&netThread, NULL,
                                    (_SHRThreadFunc) titleDetailsThread, x))) {
        trace (ERR, ECWS, "_SHR_thread_create returned %d", rv);
        netThread = 0;
        rv = EC_ERROR_ECP;
        goto end;
    }

    ecp.isWebServiceActive = true;

end:
    if (rv && netReq) {
        delete netReq;
        netReq = NULL;
    }

    return rv;
}



// static member
ECError ECPGetTitleDetailsCache::titleDetailsThread (ECPTitleDetailsReq *netReq)
{
    return __ecpEnv.titleDetailsCache.accessNet (netReq);
}


ECError ECPGetTitleDetailsCache::accessNet (ECPTitleDetailsReq *ws)
{
    ECPEnv   &ecp = __ecpEnv;
    unsigned i, j, k;
    size_t   n;
    int      rv;
    const char *msg = "<No message provided>";

    ECommerceSOAPBinding& ecs         = *ws->ecs;
    ecs__GetTitleDetailsRequestType&  req  =  ws->req;
    ecs__GetTitleDetailsResponseType& resp =  ws->resp;

    rv = ecs.__ecs__GetTitleDetails(&req, &resp);

    lock(ecp.mutex);

    if (netReq != ws) {
        rv = EC_ERROR_NOT_BUSY;
        goto canceled;
    }

    if (rv != SOAP_OK) {
        soap_print_fault (ecs.soap, stdout);
        fflush(stdout);
        rv = EC_ERROR_ECS_NA;
        goto end;
    }

    if (resp.ErrorCode != 0) {
        if (resp.ErrorMessage) {
            msg = resp.ErrorMessage->c_str();
        }
        trace (INFO, ECWS, "TitleDetails resp.ErrorCode %d: %s\n",
                resp.ErrorCode, msg);
        rv = EC_ECS_ERROR_RANGE_START - resp.ErrorCode;
        goto end;
    }

    if (!resp.TitleInfo) {
        trace (INFO, ECWS,
            "TitleDetails soap resp missing TitleInfo\n");
        rv = EC_ERROR_WS_RESP;
        goto end;
    }

    ECTitleInfo &t = details.info;
    ecs__TitleInfoType &g = *resp.TitleInfo;

    t.titleId = _strtoui64(g.TitleId.c_str(), NULL, 16);
    t.fsSize = g.FsSize;  // Use approx size
    t.version = g.Version;

    if (g.TitleKind) {
        t.titleKind = soap2ecTitleKind (*g.TitleKind);
    }

    n = 0;
    if (g.TitleName) {
        n = g.TitleName->copy(t.titleName,  sizeof t.titleName - 1, 0);
    }
    t.titleName[n] = 0;

    n = 0;
    if (g.TitleDescription) {
        n = g.TitleDescription->copy(t.description, sizeof t.description - 1, 0);
    }
    t.description[n] = 0;

    n = 0;
    if (g.Category) {
        n = g.Category->copy(t.category, sizeof t.category - 1, 0);
    }
    t.category[n] = 0;

    size_t nContents = g.Contents.size();

    for (i = k = 0;  k < nContents
                 && i < EC_MAX_CONTENTS_PER_TITLE; ++k) {

        if (!g.Contents[k]) {
            trace (WARN, ECWS, "EC_GetTitleDetails has a NULL content pointer\n");
            continue;
        }

        if ((rv = ecp.saveContentInfo (g.Contents[k]->ContentId,
                                       g.Contents[k]->ContentSize,
                                       t.contents[i],
                                       true,
                                       t.titleId))) {

            trace (ERR, ECWS, "EC_GetTitleDetails: "
                              "ecp.saveContentInfo returned %d\n", rv);
        }

        ecp.contentCache.getStat (t.contents[i].id, t.cacheStatus[i]);

        if (t.cacheStatus[i].totalSize != g.Contents[k]->ContentSize) {
            trace (WARN, ECWS, "EC_GetTitleDetails: "
                "cacheStatus totalSize %d not same as soap ContentSize %d "
                "for contentId %s\n",
                t.cacheStatus[i].totalSize, (s32) g.Contents[k]->ContentSize,
                g.Contents[k]->ContentId.c_str());
        }

        ++i;
    }

    t.nContents = i;

    ECRating *r = &details.ratings[0];
    vector<ecs__RatingType * > &v = resp.Ratings;

    for (i = k = 0; i < v.size() && k < EC_MAX_RATINGS;  ++i) {
        if (!v[i]) {
            continue;
        }
        n = v[i]->Name.copy(r[k].name, sizeof r[k].name -1, 0);
        r[k].name[n] = 0;
        n = v[i]->Rating.copy(r[k].rating, sizeof r[k].rating -1, 0);
        r[k].rating[n] = 0;
        ++k;
    }
    details.nRatings = k;

    ECPricing *e = &details.pricings[0];
    vector<ecs__PricingType * > &p = resp.TitlePricings;

    for (i = k = 0;  i < p.size() && k < EC_MAX_TITLE_PRICINGS;  ++i) {
        // filter all but subscriptions
        if (!p[i]
        || p[i]->PricingCategory != ecs__PricingCategoryType__Subscription) {
            continue;
        }
        e[k].itemId = p[i]->ItemId;
        bool isInvalidLimitCode = false;
        for (j = 0; j < p[i]->Limits.size() && j < ES_MAX_LIMIT_TYPE; ++j) {
            string limitKind = p[i]->Limits[j]->LimitKind;
            s64 limit = p[i]->Limits[j]->Limits;
            s32 code  = soap2ecLimitCode(limitKind);
            trace (INFO, ECWS, "Tiltle Details limitKind %s  code %d  limit %I64d\n",
                                limitKind.c_str(), code, limit);
            if (!code) {
                break;
            }
            if (code == EC_ERROR_INVALID) {
                trace (WARN, ECWS, "Unknown LimitKind %s\n", limitKind.c_str());
                isInvalidLimitCode = true;
                break;
            }
            e[k].limits[j].code = code;
            e[k].limits[j].limit = (u32) limit;
        }
        if (isInvalidLimitCode) {
            continue;
        }
        e[k].nLimits = j;

        e[k].price.amount = strtoul (p[i]->Price->Amount.c_str(), NULL, 0);
        n = p[i]->Price->Currency.copy (e[k].price.currency,
                                        sizeof e[k].price.currency -1, 0);
        e[k].price.currency[n] = 0;

        e[k].pricingCategory = soap2ecpPricingCategory (p[i]->PricingCategory);
        ++k;
    }
    details.nPricings = k;

    
    status.totalSize = status.cachedSize = sizeof ECTitleDetails;
    cachedTime = now();
    expireTime = cachedTime + cacheDur;

    bool dump = true;

    if (dump) {
        ECTitleDetails& d = details;

        trace (INFO, ECWS, "\n\nEC_GetTitleDetails\n"
                           "titleName %s\n"
                           "description %s\n"
                           "category %s\n"
                           "titleId 0x%08x.0x%08x (%u.%u)  titleKind %d fsSize %I64d\n"
                           "version %u\n",
            d.info.titleName, d.info.description, d.info.category,
            (u32)(d.info.titleId >> 32), (u32)(d.info.titleId),
            (u32)(d.info.titleId >> 32), (u32)(d.info.titleId),
            d.info.titleKind,
            d.info.fsSize,  d.info.version);
        
        trace (INFO, ECWS, "%d contents:\n", d.info.nContents);
        for (i = 0;  i < d.info.nContents;  ++i) {
            reportContentInfo(d.info.contents[i]);
            trace (INFO, ECWS, "        cacheSize %d totalSize %d\n",
                d.info.cacheStatus[i].cachedSize, d.info.cacheStatus[i].totalSize);
        }

        trace (INFO, ECWS, "%d ratings: ", d.nRatings);
        for (i = 0;  i < d.nRatings;  ++i) {
            trace (INFO, ECWS, "Rating name %s, Rating %s\n",
                d.ratings[i].name, d.ratings[i].rating);
        }

        trace (INFO, ECWS, "%d pricings:\n", d.nPricings);
        for (i = 0;  i < d.nPricings;  ++i) {
            trace (INFO, ECWS, "itemId %d category %d  amount %d  currency %s  nLimits %u\n",
                                d.pricings[i].itemId,
                                d.pricings[i].pricingCategory,
                                d.pricings[i].price.amount,
                                d.pricings[i].price.currency,
                                d.pricings[i].nLimits);
            for (k =0; k < d.pricings[i].nLimits; ++k) {
                trace (INFO, ECWS, "limits[%u]  code %u  limit %u\n",
                   k,  d.pricings[i].limits[k].code, d.pricings[i].limits[k].limit);
            }
        }
        trace (INFO, ECWS, "\n");
    }

end:

    if (rv < 0) {
        status.cachedSize = rv;
    }

    if (netReq == ws) {
        netReq = NULL;
    }
    ecp.isWebServiceActive = false;

canceled:
    delete ws;
    unlock(ecp.mutex);

    return 0; // doesn't go anywhere
}






ECError ECPGetTitleDetailsCache::getStat(ECPTitleDetailsArg id, ECCacheStatus& stat)
{
    if (netThread && !netReq) {
        ecpCloseThreadHandle (netThread);
        netThread = 0;
    }

    // Currently only cache one,
    // but probably should cache a vector of title details
    // Don't need an expire time, but its there anyway
    if (status.cachedSize != EC_ERROR_NOT_CACHED
            && id.titleId == cached.titleId
            && (expireTime == 0 || now() < expireTime)) {
        stat = status;
    } else {
        stat.cachedSize = EC_ERROR_NOT_CACHED;
        stat.totalSize = EC_ERROR_INVALID;
    }

    return EC_ERROR_OK;
}

ECError ECPGetTitleDetailsCache::cache(ECPTitleDetailsArg id)
{
    // delete current cache
    // get details from web service
    // Allow cache, refresh, unlink even if open,
    // but don't refresh on cache request if already cached

    ECError   rv = EC_ERROR_OK;

    ECCacheStatus  stat;

    if (!(rv = getStat (id, stat))) {
        // it is either all there, or not cached
        if (stat.cachedSize < 0 || stat.cachedSize != stat.totalSize) {

            unlink(id);
            cached = id;
            rv = cacheTitleDetails();
        }
    }

    return rv;
}


ECError ECPGetTitleDetailsCache::open    (ECPTitleDetailsArg id)
{
    ECError        rv;
    ECCacheStatus  stat;

    if (!(rv = getStat (id, stat))) {
        // it is either all there, or not cached
        if (stat.cachedSize < 0) {
            rv = EC_ERROR_NOT_CACHED;
        }
        else {
            isOpen = true;
        }
    }

    return rv;
}


ECError ECPGetTitleDetailsCache::read    (ECPTitleDetailsArg id, void *ret, size_t *retLen)
{
    if (!isOpen
            || id.titleId != cached.titleId) {

        return EC_ERROR_NOT_OPEN;
    }

    if (status.cachedSize < 0) {
        return EC_ERROR_NOT_CACHED;
    }

    /* Normally the caching is just so the client doesn't need to wait
     * for the net operation.  Since the client provides a buffer for the
     * whole thing, just send the whole thing.
     */
    memcpy (ret, &details, sizeof details);

    *retLen = sizeof details;

    return EC_ERROR_OK;
}


ECError ECPGetTitleDetailsCache::seek    (ECPTitleDetailsArg id)
{
    // This is never called in actual practice

    return EC_ERROR_NOT_SUPPORTED;
}


ECError ECPGetTitleDetailsCache::close   (ECPTitleDetailsArg id)
{
    if (!isOpen) {
        return EC_ERROR_NOT_OPEN;
    }

    if (id.titleId != cached.titleId) {

        trace (WARN, ECWS, "Mismatched arg on list title details close\n");
    }

    isOpen = false;

    if (expireOnClose) {
        expireTime = 1;
    }

    return EC_ERROR_OK;
}



ECError ECPGetTitleDetailsCache::unlink  (ECPTitleDetailsArg id)
{
    // This is never called from the NC in actual practice

    close(id);

    status.cachedSize = EC_ERROR_NOT_CACHED;
    status.totalSize = EC_ERROR_INVALID;
    isOpen = false;

    return EC_ERROR_OK;
}






int32_t  ecpTitleDetails_GetStat (VN         *vn,
                                  VNMsgHdr   *hdr,
                                  const void *arg,
                                  size_t      argLen,
                                  void       *ret,
                                  size_t     *retLen)
{
    ECError   rv;
    ECPEnv   &ecp = __ecpEnv;

    if (!arg || !ret || !retLen || argLen != sizeof (ECPTitleDetailsArg)) {
        rv = EC_ERROR_ECP;
        goto end;
    }

    rv = ecp.titleDetailsCache.getStat (*(ECPTitleDetailsArg*) arg,
                                        *(ECCacheStatus*) ret);

    if (!rv) {
        *retLen = sizeof(ECCacheStatus);
    }

end:
    return rv;
}


/*
 *  if !cached
 *      get from ecs web service
 *  return 0
 */
int32_t  ecpTitleDetails_Cache (VN         *vn,
                                VNMsgHdr   *hdr,
                                const void *arg,
                                size_t      argLen,
                                void       *ret,
                                size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;
    VNGErrCode ec;

    if (ecp.isWebServiceActive) {
        rv = EC_ERROR_BUSY;
    }
    else if (!arg || argLen != sizeof(ECPTitleDetailsArg)) {
        rv = EC_ERROR_ECP;
    }
    else if ((ec = VN_GetMemberDeviceId(vn, hdr->sender,
                                          &ecp.titleDetailsCache.deviceId))) {
        trace (ERR, ECWS,
               "title details VN_GetMemberDeviceId returned error %d\n", ec);
        rv = EC_ERROR_ECP;
    }
    else {
        rv = ecp.titleDetailsCache.cache(*(ECPTitleDetailsArg*) arg);
    }

    return rv;
}

int32_t  ecpTitleDetails_Open (VN         *vn,
                               VNMsgHdr   *hdr,
                               const void *arg,
                               size_t      argLen,
                               void       *ret,
                               size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (!arg || argLen != sizeof(ECPTitleDetailsArg)) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.titleDetailsCache.open (*(ECPTitleDetailsArg*) arg);
    }

    return rv;
}

int32_t  ecpTitleDetails_Read (VN         *vn,
                               VNMsgHdr   *hdr,
                               const void *arg,
                               size_t      argLen,
                               void       *ret,
                               size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (!ret || !retLen || !arg || argLen != sizeof(ECPTitleDetailsArg)) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.titleDetailsCache.read (*(ECPTitleDetailsArg*)arg, ret, retLen);
    }

    return rv;
}

int32_t  ecpTitleDetails_Seek (VN         *vn,
                               VNMsgHdr   *hdr,
                               const void *arg,
                               size_t      argLen,
                               void       *ret,
                               size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (!arg || argLen != sizeof(ECPTitleDetailsArg)) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.titleDetailsCache.seek (*(ECPTitleDetailsArg*) arg);
    }

    return rv;
}

int32_t  ecpTitleDetails_Close (VN         *vn,
                                VNMsgHdr   *hdr,
                                const void *arg,
                                size_t      argLen,
                                void       *ret,
                                size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (!arg || argLen != sizeof(ECPTitleDetailsArg)) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.titleDetailsCache.close (*(ECPTitleDetailsArg*) arg);
    }

    return rv;
}

int32_t  ecpTitleDetails_Refresh (VN         *vn,
                                  VNMsgHdr   *hdr,
                                  const void *arg,
                                  size_t      argLen,
                                  void       *ret,
                                  size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (!arg || argLen != sizeof(ECPTitleDetailsArg)) {
        rv = EC_ERROR_ECP;
    } else {
        ECPTitleDetailsArg  *ca = (ECPTitleDetailsArg*) arg;
        rv = ecp.titleDetailsCache.refresh (*ca, *ca);
    }

    return rv;
}


int32_t  ecpTitleDetails_Unlink (VN         *vn,
                                 VNMsgHdr   *hdr,
                                 const void *arg,
                                 size_t      argLen,
                                 void       *ret,
                                 size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (!arg || argLen != sizeof(ECPTitleDetailsArg)) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.titleDetailsCache.unlink (*(ECPTitleDetailsArg*) arg);
    }

    return rv;
}



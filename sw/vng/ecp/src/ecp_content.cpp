/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#include "ecp_i.h"
#include "config_var.h"
#include "common.h"
#include "content.h"
#include "getahdir.h"





static
void unlinkContent (const string& content_id)
{
    string       abspath;
    int          content_size = 0;
    off_t        cursize = 0;

    if ((0 <= checkCacheID (DB_CACHE_DIR, content_id,
                                content_size, abspath, &cursize)) && 0 <= cursize) {
        ::unlink (abspath.c_str());
    }
    if ((0 <= checkCacheID (CDS_INCOMING_DIR, content_id,
                                content_size, abspath, &cursize)) && 0 <= cursize) {
        ::unlink (abspath.c_str());
    }
}



bool isAlwaysDownloaded(string& content_id)
{
    bool rv;

    ECContentType type = contentTypeFromId (content_id);

    if (type < 0) {
        trace (ERR, CTNT, "isAlwaysDownloaded was passed invalid content_id %s\n",
            (content_id.size() ? content_id.c_str() : "<empty>"));
    }

    switch (type) {
        case EC_TMD:
        case EC_MET:
        case EC_CRL:
        case EC_CETK:
            rv = true;
            break;

        default:
            rv = false;
            break;
    }

    return rv;
}


/* getLastNetResult expectes hex96 content_id
*/
ECError ECPContentCache::getLastNetResult (const string&  content_id, const string& abspath)
{
    ECError rv;

    lastNetResult_iterator  it = lastNetResult.find(content_id);
    if (it != lastNetResult.end()) {
        rv = iah2ecErr (it->second);
        if (rv == EC_ERROR_OK  && it->second != 100) {
            // Don't expect to get here. Means a progress
            // other than 100% done was stored in lastNetResult
            rv = EC_ERROR_NET_CONTENT;
            unlinkContent (content_id); // remove because unknown state
            lastNetResult.erase(it);
        }
    }
    else {
        rv = EC_ERROR_NOT_FOUND;
    }

    return rv;
}




ECError  ECPContentCache::monitorNetContent()
{
    // if activeNetContentFg and not done
    //     return
    // if activeNetContentFg
    //     get final result and map it
    //     delete activeNetContentFg
    //     activeNetContentFg = NULL
    // if fg q not empty && !activeNetContentFgWaiting
    //     activeNetContentFgWaiting = ture;
    //     if activeNetContentBg && not done
    //         cancel
    // if activeNetContentFgWaiting (for canceled bg to be done)
    //     if activeNetContentBg and it is done
    //         get final result and map it
    //         delete activeNetContentBg
    //         activeNetContentBg = NULL
    //     if ! activeNetContentBg
    //         activeNetContentFgWaiting = false;
    //         aciveNetContentFg = new pop_back
    //         activeNetContentFgWaiting = true;
    //         start fg request
    // else
    //     if activeNetContentBg and done
    //         get final result and map it
    //         delete activeNetContentBg
    //         activeNetContentBg = NULL
    //     if bg q not empty and !activeNetContentBg
    //         start bg cache net content
    //

    ECPEnv&   ecp = __ecpEnv;
    int       progress;
    string    content_id;
    bool      isUnlinkOnCancel;

    // call ThreadWTO::monitor directly because App::SetTimer doesn't work
    // without a message loop in the thread that set the timer.
    // The thread that set the timer is not the main thread.  Can change
    // timers to work diffently (e.g. use multimedia timer or just check
    // for expire in main loop), but we currently have no other need for
    // timers. Do this for now, fix timers later.

    if (activeNetContentFg) {
        activeNetContentFg->monitor(activeNetContentFg->monitorTimerId());
    } else if (activeNetContentBg) {
        activeNetContentBg->monitor(activeNetContentBg->monitorTimerId());
    }

    if (activeNetContentFg && !activeNetContentFg->isDone()) {
        return EC_ERROR_OK;
    }

    if (activeNetContentFg) {
        progress = activeNetContentFg->getProgress();
        content_id = activeNetContentFg->content_id();
        if (activeNetContentFg->isCanceled (isUnlinkOnCancel)
                && isUnlinkOnCancel) {
            unlinkContent (content_id);
        } else {
            lastNetResult[content_id] = progress;
        }
        delete activeNetContentFg;
        activeNetContentFg = NULL;
        // give user a chance to start read before start bg
        timeItsOkToStartBg = time(NULL) + secAfterFgFinishesTilOkToStartBg;
    }

    if (fgNetCids.size()) {
        if (!activeNetContentFgWaiting) {
            activeNetContentFgWaiting = true;
            if (activeNetContentBg && !activeNetContentBg->isDone()) {
                activeNetContentBg->cancel();
            }
        }
    } else {
        activeNetContentFgWaiting = false; // just in case it isn't already
    }

    if (activeNetContentFgWaiting) {
        if (activeNetContentBg && activeNetContentBg->isDone()) {
            progress = activeNetContentBg->getProgress();
            content_id = activeNetContentBg->content_id();
            if (activeNetContentBg->isCanceled (isUnlinkOnCancel)
                    && isUnlinkOnCancel) {
                unlinkContent (content_id);
            } else {
                lastNetResult[content_id] = progress;
            }
            delete activeNetContentBg;
            activeNetContentBg = NULL;
        }
        if (!activeNetContentBg) {
            activeNetContentFgWaiting = false;
            content_id = fgNetCids.back();
            fgNetCids.pop_back();
            lastNetResult_iterator  it = lastNetResult.find(content_id);
            if (it != lastNetResult.end()) {
                lastNetResult.erase(it);
            }
            const char* url_prefix = contentTypeFromId(content_id) ?
                                        ecp.ucsURL.c_str() : ecp.ccsURL.c_str();
            activeNetContentFg = new NetContent (ecp.hmWnd,
                                                 ecp.netContentTimeout,
                                                 url_prefix,
                                                 content_id.c_str(),
                                                 0, true,
                                                 isAlwaysDownloaded(content_id));
        }
    }
    else {
        if (activeNetContentBg && activeNetContentBg->isDone()) {
            progress = activeNetContentBg->getProgress();
            content_id = activeNetContentBg->content_id();
            if (activeNetContentBg->isCanceled (isUnlinkOnCancel)
                    && isUnlinkOnCancel) {
                unlinkContent (content_id);
            } else {
                lastNetResult[content_id] = progress;
            }
            delete activeNetContentBg;
            activeNetContentBg = NULL;
        }
        if (timeItsOkToStartBg && time(NULL) >= timeItsOkToStartBg) {
            timeItsOkToStartBg = 0;
        }

        if (bgNetCids.size() && !activeNetContentBg
                             && isOpen.empty()
                             && !ecp.isWebServiceActive
                             && !timeItsOkToStartBg) {
            content_id = bgNetCids.front();
            bgNetCids.pop_front();
            lastNetResult_iterator  it = lastNetResult.find(content_id);
            if (it != lastNetResult.end()) {
                lastNetResult.erase(it);
            }
            activeNetContentBg = new NetContent (ecp.hmWnd,
                                                 ecp.netContentTimeout,
                                                 content_id.c_str(),
                                                 0, true,
                                                 isAlwaysDownloaded(content_id));
        }
    }

    return EC_ERROR_OK;
}



int ECPContentCache::bgCacheNetContent (const char* content_id, bool front)
{
    if (!isValidContentIdFormat(content_id)) {
        return -1;
    }
    bgNetCids_iterator it;
    for (it = bgNetCids.begin(); it != bgNetCids.end(); ++it) {
        if ((*it) == content_id) {
            if (front && it != bgNetCids.begin()) {
                bgNetCids.erase(it);
                bgNetCids.push_front (content_id);
            }
            return 0;
        }
    }
    if (front)
        bgNetCids.push_front (content_id);
    else
        bgNetCids.push_back (content_id);
    return 0;
}


// finds content_id in bgNetCids or returns bgNetCids.end()
//
ECPContentCache::bgNetCids_iterator
ECPContentCache::bgNetCidsFind (string& content_id)
{
    bgNetCids_iterator it;

    for (it = bgNetCids.begin();  it != bgNetCids.end();  ++it) {
        if ((*it) == content_id) {
            break;
        }
    }
    return it;
}


// finds content_id in fgNetCids or returns fgNetCids.end()
//
ECPContentCache::fgNetCids_iterator
ECPContentCache::fgNetCidsFind (string& content_id)
{
    fgNetCids_iterator it;

    for (it = fgNetCids.begin();  it != fgNetCids.end();  ++it) {
        if ((*it) == content_id) {
            break;
        }
    }
    return it;
}








streamsize readSomeBytes (const char *filename, int offset, char *buf, streamsize  nBytes)
{
    streamsize n = 0;

    ifstream is (filename, ios::in | ios::binary);
    if (!is) {
        return -1;
    }

    if (!is.is_open()) {
        trace (ERR, CTNT, "file %s is not open\n", filename);
        return -1;
    }

    is.seekg(offset);

    if (is.eof()) {
        trace  (INFO, CTNT, "file %s is at eof\n", filename);
    }

    if (is.good()) {
        is.read(buf, nBytes);
        n = is.gcount();
    }
    is.close();
    return n;
}





ECError ECPContentCache::getStat(ECContentId cid, ECCacheStatus& cstat)
{
    ECPEnv   &ecp = __ecpEnv;
    ECError   rv  = EC_ERROR_OK;
        
    /* Have forground queue and bg queue
     * Only one active at a time.
     * For status:
     *     check if in progress
     *     if in cache dir
     *          fully downloaded, get size
     *          check lastNetResult for error
     *     else if in incomming
     *          partially downloaded, get size
     *          check lastNetResult for error
     */

    NetContent*  active = NULL;
    bool         waiting = false;
    string content_id;
    string abspath;
    int    content_size = 0;
    off_t  cursize = 0;
    int    retv;
    bool   isCanceled;
    bool   isUnlinkOnCancel;

    cstat.totalSize = EC_ERROR_INVALID;

    if ((rv = cid96FromECContentId (cid, content_id))) {
        cstat.cachedSize = rv;
        return rv;
    }


    if (activeNetContentFg && activeNetContentFg->content_id() == content_id) {
        active = activeNetContentFg;
        trace (FINEST, CTNT, "getStat: getting %u from net in fg\n", cid.contentId);
    } else if (activeNetContentBg
                  &&  activeNetContentBg->content_id() == content_id) {
        active = activeNetContentBg;
        trace (FINEST, CTNT, "getStat: getting %u from net in bg\n", cid.contentId);
    }
    if ( fgNetCidsFind(content_id) != fgNetCids.end()) {
        waiting = true;
        trace (FINEST, CTNT, "getStat: waiting to get %u from net\n", cid.contentId);
    } else if (!active) {
        trace (FINEST, CTNT, "activeNetContentFg is %s\n", activeNetContentFg ?
            activeNetContentFg->content_id().substr(16).c_str() : "NULL");
        trace (FINEST, CTNT, "activeNetContentBg is %s\n", activeNetContentBg ?
            activeNetContentBg->content_id().substr(16).c_str() : "NULL");
    }

    if (active
            && (isCanceled = active->isCanceled(isUnlinkOnCancel))
                    && isUnlinkOnCancel) {
        if (waiting) {
            cstat.cachedSize = 0;
        } else {
            cstat.cachedSize = EC_ERROR_NOT_CACHED;
        }
    }
    else if ((0>(retv=checkCacheID (DB_CACHE_DIR, content_id, content_size,
                                                   abspath, &cursize)))
                    ||  (retv==0 && cursize < 0)) {
        // not fully cached.  might be partially downloaded
        if ((0>(retv=checkCacheID (CDS_INCOMING_DIR, 
                                   content_id, content_size, abspath,
                                   &cursize)))
                        ||  (retv==0 && cursize <= 0)) {
            if ((active && !isCanceled) || waiting) {
                cstat.cachedSize = 0;
            } else {
                cstat.cachedSize = getLastNetResult (content_id, abspath);
                if (cstat.cachedSize == EC_ERROR_NOT_FOUND) {
                    cstat.cachedSize = EC_ERROR_NOT_CACHED;
                }
            }
        } else if ((active && !isCanceled) || waiting) {
            cstat.cachedSize = cursize;
            if (cursize)
                --cstat.cachedSize; // Wait for copy incoming -> cache
        } else if (active) {
            // active but canceled and not re-requested (i.e. not waiting)
            cstat.cachedSize = EC_ERROR_NOT_CACHED;
        }
        else {
            // partially downloaded but not in progress.
            // Return EC_ERROR_NOT_CACHED or other error to indicate not in progress
            cstat.cachedSize = getLastNetResult (content_id, abspath);
            if (cstat.cachedSize == EC_ERROR_NOT_FOUND) {
                cstat.cachedSize = EC_ERROR_NOT_CACHED;
            }
        }
    }
    else if (active || waiting) {
        cstat.cachedSize = cursize;
        if (cursize) {
            --cstat.cachedSize; // wait for done
        }
    }
    else {
        // file is there, not active or waiting, so must be done 
        // if no lastNetResult or lastNetResult is 100%
        //      cachedSize = totalSize = actual file size
        cstat.cachedSize = getLastNetResult (content_id, abspath);
        if (cstat.cachedSize == EC_ERROR_NOT_FOUND
               || cstat.cachedSize == EC_ERROR_OK) {        // 100%
            // Done successfully
            cstat.cachedSize = cursize;
            cstat.totalSize = cstat.cachedSize;
        }
    }

    // Provide totalSize if not already there
    if (ecp.contents.find(content_id) != ecp.contents.end()) {
        ECContentInfo& content = ecp.contents[content_id];
        if (cstat.totalSize > 0 && cstat.totalSize != content.size) {
            trace (WARN, CTNT, "ECPContentCache::getStat: "
                "cacheStatus totalSize %d not same as mapped ECContentInfo.size %d "
                "for contentId %s\n",
                cstat.totalSize, content.size,
                content_id.c_str());
        }
        else if (content.size) {
            cstat.totalSize = content.size;
        }
    }
    else if (cstat.totalSize == EC_ERROR_INVALID && active) {
        ECContentInfo content;

        content_size = active->content_size_per_hdr ();
        if (content_size) {
            cstat.totalSize = content_size;
            // don't remember special files like crl, met, tmd
            if (contentTypeFromId(content_id) == EC_CONTENT) {
                if ((rv = ecp.saveContentInfo (content_id,
                                               content_size,
                                               content))) {

                    trace (ERR, ECWS, "ECPContentCache::getStat: "
                                "ecp.saveContentInfo returned %d\n", rv);
                }
            }
        }
    }

    return rv;
}


ECError ECPContentCache::cache(ECPContentListArg& arg)
{
    ECError   rv = EC_ERROR_OK;
    ECError   ece;
    int       i;

    bool          isUnlinkOnCancel;
    bool          fg = arg.fg;
    int           count = arg.count;
    ECContentId  *cids = arg.cids;
    string        content_id;

    if (!fg) {
        for (i = 0;  i < count;  ++i) {   
            if ((ece = cid96FromECContentId (cids[i], content_id))) {
                rv = ece;
                continue;
            }
            bgCacheNetContent (content_id.c_str());
        }
    }
    else {
        for (i = count;  i > 0;  ) {
            if ((ece = cid96FromECContentId (cids[--i], content_id))) {
                rv = ece;
                continue;
            }
            if (activeNetContentFg
                    && activeNetContentFg->content_id() == content_id
                    && !activeNetContentFg->isCanceled(isUnlinkOnCancel)) {
                continue;
            }

            bgNetCids_iterator it;
            if ((it = bgNetCidsFind(content_id)) != bgNetCids.end()) {
                bgNetCids.erase(it);
            }

            if (fgNetCidsFind(content_id) == fgNetCids.end()) {
                fgNetCids.push_back (content_id);
            }
        }
    }

    monitorNetContent ();

    return rv;
}


ECError ECPContentCache::open    (ECContentId cid)
{
    ECError        rv;
    ECCacheStatus  cstat;
    string         content_id;

    if (!(rv = getStat (cid, cstat))) {
        // require copy from incoming to cache before say cached
        if (cstat.cachedSize != cstat.totalSize) {
            rv = EC_ERROR_NOT_CACHED;
        }
        else if (!(rv = cid96FromECContentId (cid, content_id))) {
            isOpen = content_id;
            position = 0;
            if (activeNetContentBg) {
                activeNetContentBg->cancel();
            }
        }
    }

    return rv;
}


ECError ECPContentCache::read    (ECPContentCountArg& arg, void *ret, size_t *retLen)
{
    ECError  rv;
    string   content_id;

    if ((rv = cid96FromECContentId (arg.cid, content_id))) {
        return rv;
    }

    if (isOpen != content_id) {

        return EC_ERROR_NOT_OPEN;
    }

    streamsize n = 0;

    string abspath;
    int    content_size = 0;
    off_t  cursize = 0;
    int retv;

    if ((0>(retv=checkCacheID (DB_CACHE_DIR, content_id, content_size, abspath, &cursize)))
                    ||  (retv==0 && cursize <= 0)) {
        rv = EC_ERROR_ECP;
        goto end;
    }


    int bufLen = arg.count;
    if (arg.count > VN_MAX_MSG_LEN) {
        bufLen = VN_MAX_MSG_LEN;
    } else {
        bufLen = arg.count;
    }

    n = readSomeBytes (abspath.c_str(), position, (char *)ret, bufLen);

    if (n>= 0) {
        position += n;
        rv = EC_ERROR_OK;
    } else {
        rv = EC_ERROR_ECP;
        n = 0;
    }

end:
    *retLen = n;

    return rv;
}


ECError ECPContentCache::seek    (ECPContentCountArg& arg)
{
    ECError  rv;
    string   content_id;

    if ((rv = cid96FromECContentId (arg.cid, content_id))) {
        return rv;
    }

    if (isOpen != content_id) {

        return EC_ERROR_NOT_OPEN;
    }

    position = arg.count;  // if past end of file, reads will return zero bytes

    return EC_ERROR_OK;
}


ECError ECPContentCache::close   (ECContentId cid)
{
    ECError  rv;
    string   content_id;

    if ((rv = cid96FromECContentId (cid, content_id))) {
        return rv;
    }

    if (isOpen != content_id) {
        return EC_ERROR_NOT_OPEN;
    }

    isOpen.clear();

    return EC_ERROR_OK;
}



ECError ECPContentCache::unlink  (ECContentId cid)
{
    ECError  rv;

    NetContent*  active = NULL;
    string       content_id;

    close(cid);

    if ((rv = cid96FromECContentId (cid, content_id))) {
        return rv;
    }

    trace (INFO, CTNT, "Delete net content of %s\n", content_id.c_str());
    if (activeNetContentFg && activeNetContentFg->content_id() == content_id) {
        active = activeNetContentFg;
        activeNetContentFg->cancel(true /*unlink on cancel*/);
    }
    else if (activeNetContentBg && activeNetContentBg->content_id() == content_id) {
        active = activeNetContentBg;
        activeNetContentBg->cancel(true /*unlink on cancel*/);
    }
    else {
        lastNetResult_iterator  it = lastNetResult.find(content_id);
        if (it != lastNetResult.end()) {
            lastNetResult.erase(it);
        }
        unlinkContent (content_id);
    }

    {
        bgNetCids_iterator it;
        if ((it = bgNetCidsFind(content_id)) != bgNetCids.end()) {
            bgNetCids.erase(it);
        }
    }
    
    {
        fgNetCids_iterator it;
        if ((it = fgNetCidsFind(content_id)) != fgNetCids.end()) {
            fgNetCids.erase(it);
        }
    }

    if (fgNetCids.size() == 0) {
        activeNetContentFgWaiting = false;
    }

    return EC_ERROR_OK;
}



ECError ECPContentCache::cancel  (ECPContentListArg *list)
{
    ECError   rv;
    string    content_id;
    int       i;

    // list == NULL means cancel all fg and bg
    // list.count == 0 means cancel all list.fg ? fg : bg 

    if (!list) {
        trace (INFO, CTNT, "Cancel all fg and bg net content download\n");

        fgNetCids.clear();
        bgNetCids.clear();

        if (activeNetContentFg) {
            activeNetContentFg->cancel();
        }
        
        if (activeNetContentBg) {
            activeNetContentBg->cancel();
        }
    }
    else if (!list->count) {
        trace (INFO, CTNT, "Cancel all %s net content download\n", (list->fg ? "fg" : "bg"));

        if (list->fg) {
            fgNetCids.clear();

            if (activeNetContentFg) {
                activeNetContentFg->cancel();
            }
        }
        else {
            bgNetCids.clear();

            if (activeNetContentBg) {
                activeNetContentBg->cancel();
            }
        }
    }
    else for (i = 0;   i < list->count;  ++i) {

        if ((rv = cid96FromECContentId (list->cids[i], content_id))) {
            return rv;
        }
        trace (INFO, CTNT, "Cancel net content download of %s\n", content_id.c_str());

        if (activeNetContentFg && activeNetContentFg->content_id() == content_id) {
            activeNetContentFg->cancel();
        } else if (activeNetContentBg && activeNetContentBg->content_id() == content_id) {
            activeNetContentBg->cancel();
        }

        if (list->fg) {
            fgNetCids_iterator it;
            if ((it = fgNetCidsFind(content_id)) != fgNetCids.end()) {
                fgNetCids.erase(it);
            }

            if (fgNetCids.size() == 0) {
                activeNetContentFgWaiting = false;
            }
        }
        else {
            bgNetCids_iterator it;
            if ((it = bgNetCidsFind(content_id)) != bgNetCids.end()) {
                bgNetCids.erase(it);
            }
        }
    }

    return EC_ERROR_OK;
}






int32_t  ecpContent_GetStat (VN         *vn,
                             VNMsgHdr   *hdr,
                             const void *arg,
                             size_t      argLen,
                             void       *ret,
                             size_t     *retLen)
{
    ECError   rv;
    ECPEnv   &ecp = __ecpEnv;
    size_t    nStats = argLen / sizeof(ECContentId);
    unsigned  i;

    if (!arg || !ret || !retLen || !argLen  || argLen % sizeof(ECContentId)) {
        rv = EC_ERROR_ECP;
        goto end;
    }

    if ((nStats * sizeof(ECCacheStatus)) > VN_MAX_MSG_LEN) {
        nStats = VN_MAX_MSG_LEN / sizeof(ECCacheStatus);
    }

    ECContentId   *cids = (ECContentId*) arg;
    ECCacheStatus *r = (ECCacheStatus*) ret;

    for (i = 0;  i < nStats;  ++i) {
        rv = ecp.contentCache.getStat (cids[i], r[i]);
        
        if (!rv) {
            *retLen += sizeof(ECCacheStatus);
        } else {
            break;
        }
    }


end:
    return rv;
}


/*
 *  if !cached
 *      get from content distribution server
 *  return 0
 */
int32_t  ecpContent_Cache (VN         *vn,
                           VNMsgHdr   *hdr,
                           const void *arg,
                           size_t      argLen,
                           void       *ret,
                           size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (!arg || !argLen || argLen % sizeof(ECContentId)) {
        rv = EC_ERROR_ECP;
    }  else {
        ECPContentListArg  a;
        a.fg = true;
        a.count = (int) (argLen / sizeof(ECContentId));
        a.cids = (ECContentId*) arg;
        rv = ecp.contentCache.cache(a);
    }

    return rv;
}

int32_t  ecpContent_Open (VN         *vn,
                          VNMsgHdr   *hdr,
                          const void *arg,
                          size_t      argLen,
                          void       *ret,
                          size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (!arg || argLen != sizeof(ECPOpenCachedContentArg)) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.contentCache.open (*(ECContentId*) arg);
    }

    return rv;
}

int32_t  ecpContent_Read (VN         *vn,
                          VNMsgHdr   *hdr,
                          const void *arg,
                          size_t      argLen,
                          void       *ret,
                          size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (!ret || !retLen || !arg || argLen != sizeof(ECPReadCachedContentArg)) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.contentCache.read (*(ECPContentCountArg*)arg, ret, retLen);
    }

    return rv;
}

int32_t  ecpContent_Seek (VN         *vn,
                          VNMsgHdr   *hdr,
                          const void *arg,
                          size_t      argLen,
                          void       *ret,
                          size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (!arg || argLen != sizeof(ECPSeekCachedContentArg)) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.contentCache.seek (*(ECPContentCountArg*) arg);
    }

    return rv;
}

int32_t  ecpContent_Close (VN         *vn,
                           VNMsgHdr   *hdr,
                           const void *arg,
                           size_t      argLen,
                           void       *ret,
                           size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (!arg || argLen != sizeof(ECPCloseCachedContentArg)) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.contentCache.close (*(ECContentId*) arg);
    }

    return rv;
}

int32_t  ecpContent_Refresh (VN         *vn,
                             VNMsgHdr   *hdr,
                             const void *arg,
                             size_t      argLen,
                             void       *ret,
                             size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (!arg || argLen != sizeof(ECPRefreshCachedContentArg)) {
        rv = EC_ERROR_ECP;
    } else {
        ECPContentListArg ca;
        ca.fg = true;
        ca.count = 1;
        ca.cids = (ECContentId*) arg;
        rv = ecp.contentCache.refresh (*ca.cids, ca);
    }

    return rv;
}


int32_t  ecpContent_Unlink (VN         *vn,
                            VNMsgHdr   *hdr,
                            const void *arg,
                            size_t      argLen,
                            void       *ret,
                            size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (!arg || argLen != sizeof(ECPUnlinkCachedContentArg)) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.contentCache.unlink (*(ECContentId*) arg);
    }

    return rv;
}


int32_t  ecpContent_Cancel (VN         *vn,
                            VNMsgHdr   *hdr,
                            const void *arg,
                            size_t      argLen,
                            void       *ret,
                            size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    // argLen = 0 means cancel all

    if (!arg && argLen) {
        rv = EC_ERROR_ECP;
    } else {
        ECPContentListArg  a;
        a.fg = true;
        a.count = (int) (argLen / sizeof(ECContentId));
        a.cids = (ECContentId*) arg;
        rv = ecp.contentCache.cancel(&a);
    }

    return rv;
}



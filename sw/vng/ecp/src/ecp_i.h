/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __ECP_I_H__
#define __ECP_I_H__

#pragma once

#include "shr_plat.h"
#include "shr_th.h"
#include "shr_trace.h"
#include "vng.h"
#include "vng_priv.h"
#include "vng_p.h"
#include "vng_server.h"
#include "ec.h"
#include "ec_common.h"
#include "ecp.h"
#include "ecp_config.h"
#include "ecp_stl.h"
#include "ecp_rpc.h"
#include <deque>
#include <set>
typedef set<string> IDSET;
#include "ecp_cache.h"
#include "app.h"



// Trace sub-group definitions
//
//     0  - default miscellaneous group
//     1  - ECommerceWebService
//     2  - EC Proxy VN
//     3  - Content caching
//     4  - Before and after every RPC

#define SG_MISC     0
#define SG_ECWS     1
#define SG_ECPVN    2
#define SG_CTNT     3
#define SG_ALLRPC   4

#undef  MISC
#define MISC    _TRACE_ECP, SG_MISC
#define ECWS    _TRACE_ECP, SG_ECWS
#define ECPVN   _TRACE_ECP, SG_ECPVN
#define CTNT    _TRACE_ECP, SG_CTNT
#define ALLRPC  _TRACE_ECP, SG_ALLRPC

#define _TRACE_ECP _TRACE_APP



#define APP_NAME  "NC ECommerce Proxy"

/*
  If any communication protocol is changed, the ECP_VERSION must be updated.*/
#define ECP_VERSION   "1.0.0"

#define MAX_EUNITS    999999

#define MAX_CONTENT_ID 99999999

#define ECP_32_BIT_HEX_STRING_LEN         8
#define ECP_32_BIT_HEX_STRING_BUF_SIZE    9
#define ECP_64_BIT_HEX_STRING_LEN         16
#define ECP_64_BIT_HEX_STRING_BUF_SIZE    17

#define ECP_NUM_RPC_THREADS   4

#define ECP_MAIN_LOOP_INTERVAL_MS  250

#define lock(mutex)\
    do { \
       _SHRError  er; \
        if ((er = _SHR_mutex_lock (mutex))) { \
            assert (!er); \
            trace (ERR, MISC, "lock: _SHR_mutex_lock returned %d\n", er); \
            rv = EC_ERROR_ECP; \
            goto end; \
        } \
    } while (false)

#define unlock(mutex)\
    do { \
       _SHRError  er; \
        if ((er = _SHR_mutex_unlock (mutex))) { \
            assert (!er); \
            trace (ERR, MISC, "unlock: _SHR_mutex_unlock returned %d\n", er); \
            rv = EC_ERROR_ECP; \
            goto end; \
        } \
    } while (false)




class NetContent;

typedef hash_map<string, ECContentInfo> ECPContents;

class ECPEnv : public App {

    UINT netContentTimer;
    static void onNetContentTimer_s (int timerID, ECPEnv *p);
           void onNetContentTimer   (int timerID);

    public:

    string          conf_file;
    HashMapString   conf;
    string          confAsString;
   _SHRMutex       *mutex;
    VNGTimeout      vngTimeout;  // Default VNG API timeout
    int             netContentTimeout;
    string          ecsURL;
    string          ccsURL;
    string          ucsURL;
    char           *endpoint;
    bool            allowMetaCCS;
    string          pki;
    VNG            *vng;
    VN             *ncProxyVN;
    VNId            ncProxyVNId;
    _SHRThread      servRPCThreads[ECP_NUM_RPC_THREADS];
    int             nRPCThreads;
    int             traceLevel;
    bool            isWebServiceActive;
    bool            isEnablePreload;

    ECPContents     contents; /* All contents ecp knows about via any
                               * source.  Key is contentId96 as hex string */

    char           *contentTypeStrs[EC_KNOWN_CONTENT_TYPES];

    ECPContentCache          contentCache;
    ECPGetMetaCache          ecMetaCache;
    ECPListTitlesCache       listTitlesCache;
    ECPGetTitleDetailsCache  titleDetailsCache;
    ECPListSubPricingsCache  subPricingsCache;
    ECPSubscribeCache        subscribeCache;
    ECPRedeemCache           redeemCache;
    ECPPurchaseTitleCache    purchaseTitleCache;
    ECPSyncTicketsCache      syncTicketsCache;
    ECPUpdateStatusCache     updateStatusCache;

    ECError saveContentInfo (const string&    contentIdhex96,
                             s64              contentSize,
                             ECContentInfo&   contentInfo,
                             bool             isExpectedTitleId = false,
                             ESTitleId        expectedTitleId = 0);


    ECPEnv ();
};


template<VNRemoteProcedure proc>
int32_t remoteProcedure ( VN         *vn,
                          VNMsgHdr   *hdr,
                          const void *arg,
                          size_t      argLen,
                          void       *ret,
                          size_t     *retLen)
{
    ECError   rv = VNGERR_NOT_SUPPORTED;
    ECPEnv   &ecp = __ecpEnv;
    size_t    rLen = retLen ? *retLen : 0;

    *retLen = 0;    /* beacuase lock might jmp to end */

    lock(ecp.mutex);

    trace (FINEST, ALLRPC, "Before rpc proc %u  argLen %u\n",
                            hdr->serviceTag, argLen);

    rv = proc (vn, hdr, arg, argLen, ret, &rLen);

    trace (FINEST, ALLRPC, "After rpc proc %u  retLen\n",
                            hdr->serviceTag, retLen);

    *retLen = rLen;

    unlock(ecp.mutex);

end:
    return rv;
}






extern ECPEnv __ecpEnv;


int ECP_run ();

ECError ecpCloseThreadHandle (HANDLE th);


/*  Ecard is composed of (type,serial-number,secret-number).   
    Ecard can be reformatted into
     (type,serial-number,md5(secret-number)
    or the parsed components  
*/
int reformatEcard(const string& in, string& out);

/* reformat and parse ecard into type, serial-number, and hash */
int reformatEcard (const string&  in,
                         string&  eCardType,    // out
                         string&  eCardNumber,  // out
                         string&  eCardHash);   // out

ECError cid96FromECContentId (ECContentId& cid, string& cid96);

ECError reportContentInfo (ECContentInfo& content);

ECContentType  contentTypeFromId (const string& content_id);

ECError iah2ecErr (int iahErrCode);

isValidContentIdFormat(const string& content_id);



#endif /*__ECP_I_H__*/

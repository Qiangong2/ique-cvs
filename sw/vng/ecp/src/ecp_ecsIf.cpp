/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#include "ecp_ecsIfc.h"



ecs__TitleKindType
ec2soapTitleKind (ECTitleKind titleKind)
{

    ecs__TitleKindType rv;

    switch (titleKind) {
        case EC_TITLE_KIND_GAME:
            rv = ecs__TitleKindType__Games;
            break;

        case EC_TITLE_KIND_MANUAL:
            rv = ecs__TitleKindType__Manuals;
            break;

        // EC_TITLE_KIND_GAME_AND_MANUAL is not a valid wsdl value
        // it is obtained by not providing a title kind filter to gsoap

        default:
            rv = (ecs__TitleKindType) EC_ERROR_INVALID;
            break;
    }

    return rv;
}

ECTitleKind
soap2ecTitleKind (ecs__TitleKindType titleKind)
{

    ECTitleKind rv;

    switch (titleKind) {
        case ecs__TitleKindType__Games:
            rv = EC_TITLE_KIND_GAME;
            break;

        case ecs__TitleKindType__Manuals:
            rv = EC_TITLE_KIND_MANUAL;
            break;

        default:
            rv = (ECTitleKind) EC_ERROR_INVALID;
            break;
    }

    return rv;
}

ecs__PricingKindType
ec2soapPricingKindType (ECPricingKind pricingKind)
{

    ecs__PricingKindType rv;

    switch (pricingKind) {
        case EC_PK_SUBSCRIPTION:
            rv = ecs__PricingKindType__Subscription;
            break;

        case EC_PK_PURCHASE:
            rv = ecs__PricingKindType__Purchase;
            break;

        default:
            rv = (ecs__PricingKindType) EC_ERROR_INVALID;
            break;
    }

    return rv;
}


ECPricingKind
ec2soapPricingKindType (ecs__PricingKindType pricingKind)
{

    ECPricingKind rv;

    switch (pricingKind) {
        case ecs__PricingKindType__Subscription:
            rv = EC_PK_SUBSCRIPTION;
            break;

        case ecs__PricingKindType__Purchase:
            rv = EC_PK_PURCHASE;
            break;

        default:
            rv = (ECPricingKind) EC_ERROR_INVALID;
            break;
    }

    return rv;
}






ecs__PricingCategoryType
ec2soapPricingCategory (ECPricingCategory pricingKind)
{

    ecs__PricingCategoryType rv;

    switch (pricingKind) {
        case EC_PC_SUBSCRIPTION:
            rv = ecs__PricingCategoryType__Subscription;
            break;

        case EC_PC_PERMANENT:
            rv = ecs__PricingCategoryType__Permanent;
            break;

        case EC_PC_RENTAL:
            rv = ecs__PricingCategoryType__Rental;
            break;

        case EC_PC_BONUS:
            rv = ecs__PricingCategoryType__Bonus;
            break;

        case EC_PC_TRIAL:
            rv = ecs__PricingCategoryType__Trial;
            break;

        default:
            rv = (ecs__PricingCategoryType) EC_ERROR_INVALID;
            break;
    }

    return rv;
}


ECPricingCategory
soap2ecpPricingCategory (ecs__PricingCategoryType pricingKind)
{
    ECPricingCategory rv;

    switch (pricingKind) {
        case ecs__PricingCategoryType__Subscription:
            rv = EC_PC_SUBSCRIPTION;
            break;

        case ecs__PricingCategoryType__Permanent:
            rv = EC_PC_PERMANENT;
            break;

        case ecs__PricingCategoryType__Rental:
            rv = EC_PC_RENTAL;
            break;

        case ecs__PricingCategoryType__Bonus:
            rv = EC_PC_BONUS;
            break;

        case ecs__PricingCategoryType__Trial:
            rv = EC_PC_TRIAL;
            break;

        default:
            rv = (ECPricingCategory) EC_ERROR_INVALID;
            break;
    }

    return rv;
}



#define ECP_LC_PERMANENT  0

string
ec2soapLimitKind (ECLimitCode limitCode)
{
    string rv;

    switch (limitCode) {

        case ECP_LC_PERMANENT:
            rv = "PR";
            break;

        case ES_LC_DURATION_TIME:
            rv = "TR";
            break;

        case ES_LC_ABSOLUTE_TIME:
            rv = "DR";
            break;

        case ES_LC_NUM_TITLES:
            rv = "SR";
            break;

        default:
            break;
    }

    return rv;
}



s32
soap2ecLimitCode (string& limitKind)
{
    s32 rv;

    if (limitKind == "PR") {
        rv = ECP_LC_PERMANENT;
    } else if (limitKind == "TR") {
        rv = ES_LC_DURATION_TIME;
    } else if (limitKind == "DR") {
        rv = ES_LC_ABSOLUTE_TIME;
    } else if (limitKind == "SR") {
        rv = ES_LC_NUM_TITLES;
    } else {
        rv = EC_ERROR_INVALID;
    }
    return rv;
}




ecs__TimeUnitType
ec2soapTimeUnit (ECTimeUnit timeUnit)
{
    ecs__TimeUnitType rv;

    switch (timeUnit) {
        case EC_DAY:
            rv = ecs__TimeUnitType__day;
            break;

        case EC_MONTH:
            rv = ecs__TimeUnitType__month;
            break;

       default:
            rv = (ecs__TimeUnitType) EC_ERROR_INVALID;
            break;
    }

    return rv;
}



ECTimeUnit
soap2ecTimeUnit (ecs__TimeUnitType timeUnit)
{
    ECTimeUnit rv;

    switch (timeUnit) {
        case ecs__TimeUnitType__day:
            rv = EC_DAY;
            break;

        case ecs__TimeUnitType__month:
            rv = EC_MONTH;
            break;

        default:
            rv = (ECTimeUnit) EC_ERROR_INVALID;
            break;
    }

    return rv;
}



ecs__PaymentMethodType
ec2soapPaymentMethod (ECPaymentMethod   paymentMethod,
                      int              &unionDiscriminant)
{
    ecs__PaymentMethodType rv;

    switch (paymentMethod) {
        case EC_PaymentMethod_ECard:
            rv = ecs__PaymentMethodType__ECARD;
            unionDiscriminant = SOAP_UNION_ecs__union_1_ECardPayment;
            break;

        case EC_PaymentMethod_Account:
            rv = ecs__PaymentMethodType__ACCOUNT;
            unionDiscriminant = SOAP_UNION_ecs__union_1_AccountPayment;
            break;

        case EC_PaymentMethod_CreditCard:
            rv = ecs__PaymentMethodType__CCARD;
            unionDiscriminant = SOAP_UNION_ecs__union_1_CreditCardPayment;
            break;

       default:
            rv = (ecs__PaymentMethodType) EC_ERROR_INVALID;
            unionDiscriminant = EC_ERROR_INVALID;
            break;
    }

    return rv;
}



ECPaymentMethod
soap2ecTimeUnit (ecs__PaymentMethodType paymentMethod)
{
    ECPaymentMethod rv;

    switch (paymentMethod) {
        case ecs__PaymentMethodType__ECARD:
            rv = EC_PaymentMethod_ECard;
            break;

        case ecs__PaymentMethodType__ACCOUNT:
            rv = EC_PaymentMethod_Account;
            break;

        case ecs__PaymentMethodType__CCARD:
            rv = EC_PaymentMethod_CreditCard;
            break;

        default:
            rv = (ECPaymentMethod) EC_ERROR_INVALID;
            break;
    }

    return rv;
}






/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#include "ecp_i.h"
#include "ecp_ecsIfc.h"
#include "common.h"





int ECPRedeemCache::cacheRedeem ()
{
    ECPEnv   &ecp = __ecpEnv;
    int      rv = 0;

    netReq = new ECPRedeemReq;

    if (!netReq) {
        rv = EC_ERROR_NOMEM;
        goto end;
    }

    ECPRedeemReq *x = (ECPRedeemReq*) netReq;

    ecs__RedeemECardRequestType&  req  = x->req;
    ecs__RedeemECardResponseType& resp = x->resp;

    initECSReq (req, deviceId);
    
    x->deviceCert = deviceCert;
    deviceCert = NULL;
    x->deviceCertLen = cached.deviceCertLen;
    
    if (0>reformatEcard(string(cached.eCard),
                        x->eCardPayment.ECardType,
                        x->eCardPayment.ECardNumber,
                        x->eCardPayment.ECardHash)) {
        rv = EC_ERROR_ECARD;
        goto end;
    }
    
    req.DeviceCert.__ptr = x->deviceCert;
    req.DeviceCert.__size = x->deviceCertLen;

    req.TitleId = NULL; // optional element not used
    req.ECard = &x->eCardPayment;
    req.Account = NULL; // optional element not used
    req.RedeemKind = NULL; // optional element not used


    // start thread to call service in background

    status.cachedSize = 0;
    expireTime = 0;

    if ((rv = _SHR_thread_create (&netThread, NULL,
                                    (_SHRThreadFunc) redeemThread, x))) {
        trace (ERR, ECWS, "cacheRedeem _SHR_thread_create returned %d", rv);
        netThread = 0;
        rv = EC_ERROR_ECP;
        goto end;
    }

    ecp.isWebServiceActive = true;

end:
    if (rv && netReq) {
        if (x->deviceCert) {
            delete [] x->deviceCert;
        }
        delete netReq;
        netReq = NULL;
    }

    return rv;
}



// static member
ECError ECPRedeemCache::redeemThread (ECPRedeemReq *netReq)
{
    return __ecpEnv.redeemCache.accessNet (netReq);
}


ECError ECPRedeemCache::accessNet (ECPRedeemReq *ws)
{
    ECPEnv     &ecp = __ecpEnv;
    unsigned    i;
    size_t      nCerts;
    int         rv;
    const char *msg = "<No message provided>";
    string      res;
    string      tmp;

    ECommerceSOAPBinding& ecs = *ws->ecs;

    ecs__RedeemECardRequestType&  req  =  ws->req;
    ecs__RedeemECardResponseType& resp =  ws->resp;

    rv = ecs.__ecs__RedeemECard(&req, &resp);

    lock(ecp.mutex);

    if (netReq != ws) {
        rv = EC_ERROR_NOT_BUSY;
        goto canceled;
    }

    if (rv != SOAP_OK) {
        soap_print_fault (ecs.soap, stdout);
        fflush(stdout);
        rv = EC_ERROR_ECS_NA;
        goto end;
    }

    if (resp.ErrorCode != 0) {
        if (resp.ErrorMessage && !resp.ErrorMessage->empty()) {
            msg = resp.ErrorMessage->c_str();
        }
        trace (INFO, ECWS, "Redeem resp.ErrorCode %d: %s\n",
                resp.ErrorCode, msg);
        rv = EC_ECS_ERROR_RANGE_START - resp.ErrorCode;
        goto end;
    }

    trace (INFO, ECWS, "Got response to soap Redeem request\n");
    
    nCerts =  resp.Certs.size();
    if (resp.ETickets.size() != 1) {
        trace (ERR, ECWS, "Redeem expected a Ticket but vector size is 0\n");
        rv = EC_ERROR_ECP;
        goto end;
    }

    if (!resp.ETickets[0].__ptr) {
        trace (ERR, ECWS, "Redeem expected a Ticket but resp.ETickets[0].__ptr is NULL\n");
        rv = EC_ERROR_ECP;
        goto end;
    }

    if (nCerts < 1) {
        trace (ERR, ECWS, "Redeem expected certs but vector size is 0\n");
        rv = EC_ERROR_ECP;
        goto end;
    }

    /* gsoap automatically converts ticket and certs from base64 to binary */
    res.assign((const char*)resp.ETickets[0].__ptr, resp.ETickets[0].__size);

    size_t headLen = sizeof(ECPTickeRet);
    size_t ticketLen = res.size();

    for (i = 0;  i < nCerts;  ++i) {
        if (!resp.Certs[i].__ptr) {
            trace (WARN, ECWS, "resp.Certs[i].__ptr is NULL\n", i);
            continue;
        }
        tmp.clear();
        tmp.assign((const char*)resp.Certs[i].__ptr, resp.Certs[i].__size);

        res += tmp;        
    }
    size_t certsLen = res.size() - ticketLen;
    retbufLen = headLen + ticketLen + certsLen;
    
    if (!certsLen) {
        trace (ERR, ECWS, "Redeem expected certs but certsLen is 0\n");
        rv = EC_ERROR_ECP;
        goto end;
    }

    char *b = new char [retbufLen];
    
    res.copy (&b[headLen], retbufLen - headLen);

    retbuf = (ECPTickeRet*) b;
    
    retbuf->nTickets = 1;
    retbuf->certsSize = (u16) certsLen;
    
    status.totalSize = status.cachedSize = (u32) retbufLen;
    cachedTime = now();
    expireTime = cachedTime + cacheDur;

    trace (INFO, ECWS, "Successfully processed soap Redeem request\n    "
           "nTickets %u  ticketLen %u  nCerts %d  certsLen %u  retbufLen %u\n",
           retbuf->nTickets, ticketLen, nCerts, retbuf->certsSize, retbufLen);
    
end:

    if (rv < 0) {
        status.cachedSize = rv;
        if (retbuf) {
            delete [] retbuf;
            retbuf = NULL;
        }
    }

    if (ws->deviceCert) {
        delete [] ws->deviceCert;
    }

    if (netReq == ws) {
        netReq = NULL;
    }
    ecp.isWebServiceActive = false;

canceled:
    delete ws;
    unlock(ecp.mutex);

    return 0; // doesn't go anywhere
}






ECError ECPRedeemCache::getStat(ECPRedeemArg& id, ECCacheStatus& stat)
{
    if (netThread && !netReq) {
        ecpCloseThreadHandle (netThread);
        netThread = 0;
    }

    if (status.cachedSize != EC_ERROR_NOT_CACHED
            && strncmp(id.eCard, cached.eCard, sizeof (id.eCard)) == 0
            && (expireTime == 0 || now() < expireTime)) {
        stat = status;
    } else {
        stat.cachedSize = EC_ERROR_NOT_CACHED;
        stat.totalSize = EC_ERROR_INVALID;
    }

    return EC_ERROR_OK;
}

ECError ECPRedeemCache::cache(ECPRedeemArg& id)
{
    // delete current cached redeem response
    // redeem via web service
    // cache result for client to retrieve

    ECError   rv = EC_ERROR_OK;

    unlink(id);
    cached = id;
    deviceCert = new unsigned char [id.deviceCertLen];
    memcpy (deviceCert, &id + 1, id.deviceCertLen);
    rv = cacheRedeem();
        
    return rv;
}


ECError ECPRedeemCache::open    (ECPRedeemArg& id)
{
    ECError        rv;
    ECCacheStatus  stat;

    if (!(rv = getStat (id, stat))) {
        // it is either all there, or not cached
        if (stat.cachedSize < 0) {
            rv = EC_ERROR_NOT_CACHED;
        }
        else {
            isOpen = true;
            offset = 0;
        }
    }

    return rv;
}


ECError ECPRedeemCache::read    (ECPRedeemArg& id, void *ret, size_t *retLen)
{
    if (!isOpen) {
        return EC_ERROR_NOT_OPEN;
    }

    if (status.cachedSize < 0
            || strncmp(id.eCard, cached.eCard, sizeof (id.eCard)) != 0) {
        return EC_ERROR_NOT_CACHED;
    }

    if (offset >= retbufLen) {
        *retLen = 0;
        return EC_ERROR_OK;
    }
    
    char *b = (char *) retbuf;
    
    size_t n = retbufLen - offset;
    
    if (n > VN_MAX_MSG_LEN) {
        n = VN_MAX_MSG_LEN;
    }

    if (n > id.retBufSize) {
        n = id.retBufSize;
    }
    
    memcpy (ret, &b[offset], n);
    
    offset += n;
    *retLen = n;

    return EC_ERROR_OK;
}


ECError ECPRedeemCache::seek    (ECPRedeemArg& id)
{
    // This is never called in actual practice
    
    return EC_ERROR_NOT_SUPPORTED;
}


ECError ECPRedeemCache::close   (ECPRedeemArg& id)
{
    if (!isOpen) {
        return EC_ERROR_NOT_OPEN;
    }

    isOpen = false;

    if (expireOnClose) {
        expireTime = 1;
    }

    return EC_ERROR_OK;
}



ECError ECPRedeemCache::unlink  (ECPRedeemArg& id)
{
    // This is never called from the NC in actual practice

    close(id);

    if (deviceCert) {
        delete [] deviceCert;
        deviceCert = NULL;
    }
    
    if (retbuf) {
        delete [] retbuf;
        retbuf = NULL;
    }

    status.cachedSize = EC_ERROR_NOT_CACHED;
    status.totalSize = EC_ERROR_INVALID;
    isOpen = false;
    offset = 0;

    return EC_ERROR_OK;
}





int32_t  ecpRedeem_GetStat (VN         *vn,
                                VNMsgHdr   *hdr,
                                const void *arg,
                                size_t      argLen,
                                void       *ret,
                                size_t     *retLen)
{
    ECError   rv;
    ECPEnv   &ecp = __ecpEnv;

    if (!arg || !ret || !retLen || argLen != sizeof (ECPRedeemArg)) {
        rv = EC_ERROR_ECP;
        goto end;
    }

    rv = ecp.redeemCache.getStat (*(ECPRedeemArg*) arg,
                                        *(ECCacheStatus*) ret);

    if (!rv) {
        *retLen = sizeof(ECCacheStatus);
    }

end:
    return rv;
}


/*
 *  purchase via ecs web service and cache response for
 *  retrieval by client
 */
int32_t  ecpRedeem_Cache (VN         *vn,
                              VNMsgHdr   *hdr,
                              const void *arg,
                              size_t      argLen,
                              void       *ret,
                              size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;
    VNGErrCode ec;
    ECPRedeemArg* a= (ECPRedeemArg*) arg;

    if (ecp.isWebServiceActive) {
        rv = EC_ERROR_BUSY;
    }
    else if (!arg || argLen != sizeof(ECPRedeemArg) + a->deviceCertLen) {
        rv = EC_ERROR_ECP;
    }
    else if ((ec = VN_GetMemberDeviceId(vn, hdr->sender,
                                          &ecp.redeemCache.deviceId))) {
        trace (ERR, ECWS,
               "list titles VN_GetMemberDeviceId returned error %d\n", ec);
        rv = EC_ERROR_ECP;
    }
    else {
        trace (INFO, ECWS, "Redeem eCard %s\n", a->eCard);
        rv = ecp.redeemCache.cache(*a);
    }

    return rv;
}

int32_t  ecpRedeem_Open (VN         *vn,
                             VNMsgHdr   *hdr,
                             const void *arg,
                             size_t      argLen,
                             void       *ret,
                             size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (!arg || argLen != sizeof(ECPRedeemArg)) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.redeemCache.open (*(ECPRedeemArg*) arg);
    }

    return rv;
}

int32_t  ecpRedeem_Read (VN         *vn,
                             VNMsgHdr   *hdr,
                             const void *arg,
                             size_t      argLen,
                             void       *ret,
                             size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (!ret || !retLen || !arg || argLen != sizeof(ECPRedeemArg)) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.redeemCache.read (*(ECPRedeemArg*)arg, ret, retLen);
    }

    return rv;
}

int32_t  ecpRedeem_Seek (VN         *vn,
                             VNMsgHdr   *hdr,
                             const void *arg,
                             size_t      argLen,
                             void       *ret,
                             size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (!arg || argLen != sizeof(ECPRedeemArg)) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.redeemCache.seek (*(ECPRedeemArg*) arg);
    }

    return rv;
}

int32_t  ecpRedeem_Close (VN         *vn,
                              VNMsgHdr   *hdr,
                              const void *arg,
                              size_t      argLen,
                              void       *ret,
                              size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (!arg || argLen != sizeof(ECPRedeemArg)) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.redeemCache.close (*(ECPRedeemArg*) arg);
    }

    return rv;
}

int32_t  ecpRedeem_Refresh (VN         *vn,
                                VNMsgHdr   *hdr,
                                const void *arg,
                                size_t      argLen,
                                void       *ret,
                                size_t     *retLen)
{
    return EC_ERROR_NOT_SUPPORTED;
}

/* Not very useful for redeem */
int32_t  ecpRedeem_Unlink (VN         *vn,
                               VNMsgHdr   *hdr,
                               const void *arg,
                               size_t      argLen,
                               void       *ret,
                               size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (!arg || argLen != sizeof(ECPRedeemArg)) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.redeemCache.unlink (*(ECPRedeemArg*) arg);
    }

    return rv;
}

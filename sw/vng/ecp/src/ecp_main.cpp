/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#include "ecp_i.h"
#include "shr_getopt.h"



static const char * ecsDef         = ECP_DEF_ECS_URL;
static const char * ccsDefPrefix   = ECP_DEF_CCS_URL_PREFIX;
static const char * ucsDefPrefix   = ECP_DEF_UCS_URL_PREFIX;

static const char * ecsLab1        = ECP_LAB1_ECS_URL;
static const char * ccsLab1Prefix  = ECP_LAB1_CCS_URL_PREFIX;
static const char * ucsLab1Prefix  = ECP_LAB1_UCS_URL_PREFIX;

static const char * ecsLocalhost   = ECP_LOCALHOST_URL;

static const char * ecsTCPMonDef   = ECP_DEF_TCPMON_URL;



static void usage()
{
    cout << endl;

    cout << "ecp [options] \n\n";

    cout << "  Run the NC ECommerce proxy.\n\n";

    cout << "  Default ecs url is:         " << ecsDef        << "\n";
    cout << "  Default ccs url prefix is:  " << ccsDefPrefix << "\n\n";

    cout << "  Default ecs loclhost url:   " << ecsLocalhost  << "\n";
    cout << "  Default ecs tcpmon url:     " << ecsTCPMonDef  << "\n\n";

    cout << "  In the following, <arg> is required and [arg] is optional.\n"
            "  When an optional arg value is supplied there must be no space\n"
            "  between the option name or letter and the optional value.\n"
            "  (e.g., \"-n\"  for default port, \"-n1683\" to specify port 1683)\n\n";

    cout << "     [--conf          | -f] <conf_file>  // Config file. Default is ncecp.conf\n";
    cout << "     [--ecsURL        | -e] <url>        // Use specified Web service URL\n";
    cout << "     [--ecsLab1       | -b]              // Use lab1 Web service URL\n";
    cout << "     [--localhost     | -o]              // Use localhost URL, port 8080\n";
    cout << "     [--tcpmon[=port] | -n[port]         // Use localhost URL with [port]\n";
    cout << "                                         //  (default " DEF_TCPMON_PORT ")\n\n";

    cout << "     [--ccsURLPrefix  | -c] <url prefix> // Use specified ccs url prefix.\n\n";
    cout << "     [--ccsLab1Prefix | -s]              // Use lab1 CCS URL Prefix\n";

    cout << "     [--allowMetaCCS[=false] | -a[false] // Allow overide of ccs url prefix by\n";
    cout << "                                         // GetMeta. Default is true unless -c\n";
    cout << "                                         // or -s specified. No arg means true\n";
    cout << "     [--pki          | -p] <pki>         // pki is beta or prod (default beta)\n";
    cout << "     [--vngTimeout   | -t] <millisec>    // Set default VNG timeout.\n";
    cout << "     [--traceLevel   | -l] <level>       // ERROR  = 1\n"
            "                                         // WARN   = 2\n"
            "                                         // INFO   = 3 (default)\n"
            "                                         // FINE   = 4\n"
            "                                         // FINER  = 5\n"
            "                                         // FINEST = 6\n\n";

    cout << "     [--help         | -h]               // Show this help.";

    cout << endl;
}






static int processCmdLine (int argc, char* argv[]);



int main(int argc, char* argv[])
{
    _SHR_set_trace_level(INFO);

    // modifiable parameter defaults set in ECPEnv constructor

    processCmdLine (argc, argv);

    ECP_run();

    return 0;
}



bool boolOptarg (const char *optarg, bool defaultValue = true)
{
    bool result = defaultValue;
    if (optarg && *optarg) {
        if (stricmp(optarg, "false") == 0
            || strcmp(optarg, "0") == 0) {
            result = false;
        }
        else if (stricmp(optarg, "true") == 0
                    || strcmp(optarg, "1") == 0) {
            result = true;
        }
        else {
            usage();
            exit(1);
        }
    }
    return result;
}


bool isBoolOptarg (const char *optarg, bool &out)
{
    bool result = false;
    if (optarg && *optarg) {
        if (stricmp(optarg, "false") == 0
            || strcmp(optarg, "0") == 0) {
            out = false;
            result = true;
        }
        else if (stricmp(optarg, "true") == 0
                    || strcmp(optarg, "1") == 0) {
            out = true;
            result = true;
        }
    }
    return result;
}


int processCmdLine (int argc, char* argv[])
{
    ECPEnv &ecp = __ecpEnv;;
    string conf_file;

    bool ecsURL_ocl = false;  // indicates if opt specified on cmd line
    bool ccsURLPrefix_ocl = false;
    bool allowMetaCCS_ocl = false;
    bool pki_ocl = false;
    bool vngTimout_ocl = false;
    bool vngTraceLevel_ocl = false;

    string space = " ";

    ostringstream clo;


    static struct option long_options[] = {
        { "help",         no_argument,       0, 'h'},
        { "conf",         required_argument, 0, 'f'},
        { "ecsURL",       required_argument, 0, 'e'},
        { "ecsLab1",      no_argument,       0, 'b'},
        { "localhost",    no_argument,       0, 'o'},
        { "tcpmon",       optional_argument, 0, 'n'},
        { "ccsURLPrefix", required_argument, 0, 'c'},
        { "ccsLab1Prefix",no_argument,       0, 's'},
        { "allowMetaCCS", optional_argument, 0, 'a'},
        { "pki",          required_argument, 0, 'p'},
        { "vngTimeout",   required_argument, 0, 't'},
        { "traceLevel",   required_argument, 0, 'l'},
        { NULL },
    };

    int c;
    while ((c = getopt_long(argc, argv, "hbsof:e:c:t:l:p:n::a::",
                            long_options, NULL)) >= 0) {
        switch (c) {
        case 'h':
            usage();
            exit(0);
        case 'f':
            conf_file = optarg;
            clo << "conf_file = " << optarg  << endl;
            break;
        case 'e':
            ecp.ecsURL = optarg;
            ecsURL_ocl = true;
            clo << "ecsURL = " << ecp.ecsURL << endl;
            break;
        case 'b':
            ecp.ecsURL = ecsLab1;
            ecsURL_ocl = true;
            clo << "ecsURL = " << ecp.ecsURL << endl;
            break;
        case 's':
            ecp.ccsURL = ccsLab1Prefix;
            ecp.ucsURL = ccsLab1Prefix;
            ccsURLPrefix_ocl = true;
            clo << "ccsURLPrefix = " << ecp.ccsURL << endl;
            ecp.allowMetaCCS = false;
            allowMetaCCS_ocl = true;
            clo << "allowMetaCCS = false" << endl;
            break;
        case 'o':
            ecp.ecsURL = ecsLocalhost;
            ecsURL_ocl = true;
            clo << "ecsURL = " << ecp.ecsURL << endl;
            break;
        case 'n':
            ecp.ecsURL = LOCALHOST_PREFIX;
            if (optarg)
                ecp.ecsURL += optarg;
            else
                ecp.ecsURL += DEF_TCPMON_PORT;

            ecp.ecsURL += ECS_URL_TAIL;
            ecsURL_ocl = true;
            clo << "ecsURL = " << ecp.ecsURL << endl;
            break;
        case 'c':
            ecp.ccsURL = optarg;
            ecp.ucsURL = optarg;
            ccsURLPrefix_ocl = true;
            clo << "ccsURLPrefix = " << ecp.ccsURL << endl;
            ecp.allowMetaCCS = false;
            allowMetaCCS_ocl = true;
            clo << "allowMetaCCS = false" << endl;
            break;
        case 'a':
            ecp.allowMetaCCS = boolOptarg (optarg);
            allowMetaCCS_ocl = true;
            clo << "allowMetaCCS = " << (ecp.allowMetaCCS ? "true":"false") << endl;
            break;
        case 'p':
            ecp.pki = optarg;
            if (ecp.pki != "beta" && ecp.pki != "prod") {
                usage();
                trace (ERR, MISC, "pki can only be beta or prod\n");
                exit(1);
            }
            pki_ocl = true;
            clo << "pki = " << optarg  << endl;
            break;
        case 't':
            if (!isdigit(optarg[0])) {
                usage();
                exit(1);
            }
            ecp.vngTimeout = strtoul(optarg, NULL, 0);
            vngTimout_ocl = true;
            clo << "vngTimeout = " << ecp.vngTimeout << endl;
            break;
        case 'l':
            if (!isdigit(optarg[0])) {
                usage();
                exit(1);
            }
            ecp.traceLevel = strtoul(optarg, NULL, 0);
            vngTraceLevel_ocl = true;
            clo << "traceLevel = " << ecp.traceLevel << endl;
            break;
        default:
            usage();
            exit(1);
        }
    }

    bool more_args = (optind < argc);

    if (more_args) {
        usage();
        exit(1);
    }


    string alt_conf_file;
    string tried_conf_file;

    if (!conf_file.empty()) {
        ecp.conf_file = conf_file;
        size_t len = conf_file.length();
        if (len < 5 || conf_file.substr(conf_file.length() - 5) != ".conf" )
            alt_conf_file = conf_file + ".conf";
    }

    tried_conf_file = ecp.conf_file;

    int rv = loadConfFile (ecp.conf_file.c_str(), ecp.conf);

    if (rv && !alt_conf_file.empty()) {
        rv = loadConfFile (alt_conf_file.c_str(), ecp.conf);
        if (rv)
            tried_conf_file += " or " + alt_conf_file;
        else
            ecp.conf_file = alt_conf_file;
    }

    if (rv) {
        if (ecp.conf_file == conf_file) {
            trace (ERR, MISC, "NC ECommerce Proxy could'nt read %s\n", tried_conf_file.c_str());
        }
        trace (INFO, MISC, "NC ECommerce Proxy not using a configuration file\n");
    }
    else {
        trace (INFO, MISC, "NC ECommerce Proxy using configuration file: %s\n\n", ecp.conf_file.c_str());
        ostringstream confAsString;
        dumpHashMapString(ecp.conf, confAsString);
        ecp.confAsString = confAsString.str();
        trace (INFO, MISC, "NC ECommerce Proxy conf file settings:\n%s\n", ecp.confAsString.c_str());
        string conf;

        if (!ecsURL_ocl) {
            if (ecp.conf.find("ecsURL") != ecp.conf.end()) {
                ecp.ecsURL = ecp.conf["ecsURL"];
            } else if (ecp.conf.find("ecsLab1") != ecp.conf.end()) {
                conf = ecp.conf["ecsLab1"];
                if (boolOptarg(conf.c_str())) {
                    ecp.ecsURL = ecsLab1;
                }
            } else if (ecp.conf.find("localhost") != ecp.conf.end()) {
                conf = ecp.conf["localhost"];
                if (boolOptarg(conf.c_str())) {
                    ecp.ecsURL = ecsLocalhost;
                }
            } else if (ecp.conf.find("tcpmon") != ecp.conf.end()) {
                conf = ecp.conf["tcpmon"];
                bool boolResult = false;
                if (!isBoolOptarg(conf.c_str(), boolResult) || boolResult == true) {
                    ecp.ecsURL = LOCALHOST_PREFIX;
                    if (!conf.empty() && !boolResult)
                        ecp.ecsURL += conf;
                    else
                        ecp.ecsURL += DEF_TCPMON_PORT;
                    ecp.ecsURL += ECS_URL_TAIL;
                }
            }
        }

        if (!ccsURLPrefix_ocl) {
            if (ecp.conf.find("ccsURLPrefix") != ecp.conf.end()) {
                ecp.ccsURL = ecp.conf["ccsURLPrefix"];
                ecp.ucsURL = ecp.ccsURL;
                if (ecp.conf.find("allowMetaCCS") == ecp.conf.end()) {
                    ecp.allowMetaCCS = false;
                    trace (INFO, MISC, "allowMetaCCS=false due to ccsURLPrefix in conf file \n");
                }
            } else if (ecp.conf.find("ccsLab1Prefix") != ecp.conf.end()) {
                conf = ecp.conf["ccsLab1Prefix"];
                if (boolOptarg(conf.c_str())) {
                    ecp.ccsURL = ccsLab1Prefix;
                    ecp.ucsURL = ecp.ccsURL;
                    if (ecp.conf.find("allowMetaCCS") == ecp.conf.end()) {
                        ecp.allowMetaCCS = false;
                        trace (INFO, MISC, "allowMetaCCS=false due to ccsLab1Prefix in conf file \n");
                    }
                }
            }
        }

        if (!allowMetaCCS_ocl && ecp.conf.find("allowMetaCCS") != ecp.conf.end()) {
            ecp.allowMetaCCS = boolOptarg (ecp.conf["allowMetaCCS"].c_str());
        }

        if (!pki_ocl && ecp.conf.find("pki") != ecp.conf.end()) {
            conf = ecp.conf["pki"];
            if (!conf.empty() && conf == "prod" || conf == "beta") {
                ecp.pki = conf;
            }
            else {
                trace (ERR, MISC, "pki can only be beta or prod. conf file has %s\n", conf.c_str());
                exit(1);
            }
        }

        if (!vngTimout_ocl) {
            conf = ecp.conf["vngTimeout"];
            if (!conf.empty())
                ecp.vngTimeout = strtoul(conf.c_str(), NULL, 0);
        }

        if (!vngTraceLevel_ocl) {
            conf = ecp.conf["traceLevel"];
            if (!conf.empty())
                ecp.traceLevel = strtoul(conf.c_str(), NULL, 0);
        }

    }

    if (clo.str().length()) {
        trace (INFO, MISC, "NC ECommerce Proxy options set by cmd line arg:\n%s\n", clo.str().c_str());
    }

    return 0;
}




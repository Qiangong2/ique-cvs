/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#include "ecp_i.h"
#include <wininet.h>


/* ecp global environment is shared by all ecp threads
 * Before accessing, must lock __ecpEnv.mutex.
 */

ECPEnv __ecpEnv;

// The one and only application object
ECPEnv&  app = __ecpEnv;;




static _SHRMutex  __globalMutex;

static VNG  __vng;
static VN   __ncProxyVN = { _VNG_INVALID_VNGVNID,   
                            _VNG_INVALID_VNGID,     
                            _VNG_INVALID_VNGNETADDR };

char  __ecsEndpoint[EC_URL_BUF_SIZE];





ECPEnv::ECPEnv () :
    App (APP_NAME),
    netContentTimer (0),          // updated onCreate() in iQue@Home
    conf_file ("ncecp.conf"),
    vngTimeout (10*1000),
    netContentTimeout (ECP_NET_CONTENT_TO),
    ecsURL (ECP_DEF_ECS_URL),
    ccsURL (ECP_DEF_CCS_URL_PREFIX),
    ucsURL (ECP_DEF_UCS_URL_PREFIX),
    allowMetaCCS (true),
    nRPCThreads (ECP_NUM_RPC_THREADS),
    traceLevel (INFO),
    isWebServiceActive (false),
    isEnablePreload (true)
{
    mutex = &__globalMutex;
    vng   = &__vng;
    ncProxyVN = &__ncProxyVN;
    ncProxyVNId.deviceId = VNP_INVALID_DEVICE_ID;
    ncProxyVNId.netId = VNP_INVALID_NET;

    endpoint = __ecsEndpoint;

    for (int i = 0;  i < ECP_NUM_RPC_THREADS;  ++i) {
        servRPCThreads[i] = 0;
    }
    contentTypeStrs[EC_CONTENT] = "ec_content";
    contentTypeStrs[EC_TMD]     = "tmd";
    contentTypeStrs[EC_MET]     = "met";
    contentTypeStrs[EC_CRL]     = "crl";
    contentTypeStrs[EC_CETK]    = "cetk";
}

static bool __exitRequested;

static
_SHRThreadRT  _SHRThreadCC  servRPCs (ECPEnv& ecp);

static
_SHRThreadRT  _SHRThreadCC  getEvents (ECPEnv& ecp);




static
VNRemoteProcedure rpcProcs [NUM_ECP_INFO_TYPES][NUM_ECP_REQUESTS] = {
{ 
    remoteProcedure<ecpContent_GetStat>,          /* ECP_IT_CID           + ECP_RQ_GET_STAT */
    remoteProcedure<ecpContent_Cache>,            /* ECP_IT_CID           + ECP_RQ_CACHE    */
    remoteProcedure<ecpContent_Open>,             /* ECP_IT_CID           + ECP_RQ_OPEN     */
    remoteProcedure<ecpContent_Read>,             /* ECP_IT_CID           + ECP_RQ_READ     */
    remoteProcedure<ecpContent_Seek>,             /* ECP_IT_CID           + ECP_RQ_SEEK     */
    remoteProcedure<ecpContent_Close>,            /* ECP_IT_CID           + ECP_RQ_CLOSE    */
    remoteProcedure<ecpContent_Refresh>,          /* ECP_IT_CID           + ECP_RQ_REFRESH  */
    remoteProcedure<ecpContent_Unlink>,           /* ECP_IT_CID           + ECP_RQ_DELETE   */
    remoteProcedure<ecpContent_Cancel>,           /* ECP_IT_CID           + ECP_RQ_CANCEL   */
},
{
    remoteProcedure<ecpECMeta_GetStat>,           /* ECP_IT_EC_META       + ECP_RQ_GET_STAT */
    remoteProcedure<ecpECMeta_Cache>,             /* ECP_IT_EC_META       + ECP_RQ_CACHE    */
    remoteProcedure<ecpECMeta_Open>,              /* ECP_IT_EC_META       + ECP_RQ_OPEN     */
    remoteProcedure<ecpECMeta_Read>,              /* ECP_IT_EC_META       + ECP_RQ_READ     */
    remoteProcedure<ecpECMeta_Seek>,              /* ECP_IT_EC_META       + ECP_RQ_SEEK     */
    remoteProcedure<ecpECMeta_Close>,             /* ECP_IT_EC_META       + ECP_RQ_CLOSE    */
    remoteProcedure<ecpECMeta_Refresh>,           /* ECP_IT_EC_META       + ECP_RQ_REFRESH  */
    remoteProcedure<ecpECMeta_Unlink>,            /* ECP_IT_EC_META       + ECP_RQ_DELETE   */
    remoteProcedure<ecpCancelWebSvcOps>,          /* ECP_IT_EC_META       + ECP_RQ_CANCEL   */
},
{
    remoteProcedure<ecpListTitles_GetStat>,       /* ECP_IT_LIST_TITLES   + ECP_RQ_GET_STAT */
    remoteProcedure<ecpListTitles_Cache>,         /* ECP_IT_LIST_TITLES   + ECP_RQ_CACHE    */
    remoteProcedure<ecpListTitles_Open>,          /* ECP_IT_LIST_TITLES   + ECP_RQ_OPEN     */
    remoteProcedure<ecpListTitles_Read>,          /* ECP_IT_LIST_TITLES   + ECP_RQ_READ     */
    remoteProcedure<ecpListTitles_Seek>,          /* ECP_IT_LIST_TITLES   + ECP_RQ_SEEK     */
    remoteProcedure<ecpListTitles_Close>,         /* ECP_IT_LIST_TITLES   + ECP_RQ_CLOSE    */
    remoteProcedure<ecpListTitles_Refresh>,       /* ECP_IT_LIST_TITLES   + ECP_RQ_REFRESH  */
    remoteProcedure<ecpListTitles_Unlink>,        /* ECP_IT_LIST_TITLES   + ECP_RQ_DELETE   */
    remoteProcedure<ecpCancelWebSvcOps>,          /* ECP_IT_LIST_TITLES   + ECP_RQ_CANCEL   */
},
{                                
    remoteProcedure<ecpTitleDetails_GetStat>,     /* ECP_IT_TITLE_DETAILS + ECP_RQ_GET_STAT */
    remoteProcedure<ecpTitleDetails_Cache>,       /* ECP_IT_TITLE_DETAILS + ECP_RQ_CACHE    */
    remoteProcedure<ecpTitleDetails_Open>,        /* ECP_IT_TITLE_DETAILS + ECP_RQ_OPEN     */
    remoteProcedure<ecpTitleDetails_Read>,        /* ECP_IT_TITLE_DETAILS + ECP_RQ_READ     */
    remoteProcedure<ecpTitleDetails_Seek>,        /* ECP_IT_TITLE_DETAILS + ECP_RQ_SEEK     */
    remoteProcedure<ecpTitleDetails_Close>,       /* ECP_IT_TITLE_DETAILS + ECP_RQ_CLOSE    */
    remoteProcedure<ecpTitleDetails_Refresh>,     /* ECP_IT_TITLE_DETAILS + ECP_RQ_REFRESH  */
    remoteProcedure<ecpTitleDetails_Unlink>,      /* ECP_IT_TITLE_DETAILS + ECP_RQ_DELETE   */
    remoteProcedure<ecpCancelWebSvcOps>,          /* ECP_IT_TITLE_DETAILS + ECP_RQ_CANCEL   */
},
{                                
    remoteProcedure<ecpListSubPricings_GetStat>,  /* ECP_IT_LIST_SUB_PRICES + ECP_RQ_GET_STAT */
    remoteProcedure<ecpListSubPricings_Cache>,    /* ECP_IT_LIST_SUB_PRICES + ECP_RQ_CACHE    */
    remoteProcedure<ecpListSubPricings_Open>,     /* ECP_IT_LIST_SUB_PRICES + ECP_RQ_OPEN     */
    remoteProcedure<ecpListSubPricings_Read>,     /* ECP_IT_LIST_SUB_PRICES + ECP_RQ_READ     */
    remoteProcedure<ecpListSubPricings_Seek>,     /* ECP_IT_LIST_SUB_PRICES + ECP_RQ_SEEK     */
    remoteProcedure<ecpListSubPricings_Close>,    /* ECP_IT_LIST_SUB_PRICES + ECP_RQ_CLOSE    */
    remoteProcedure<ecpListSubPricings_Refresh>,  /* ECP_IT_LIST_SUB_PRICES + ECP_RQ_REFRESH  */
    remoteProcedure<ecpListSubPricings_Unlink>,   /* ECP_IT_LIST_SUB_PRICES + ECP_RQ_DELETE   */
    remoteProcedure<ecpCancelWebSvcOps>,          /* ECP_IT_LIST_SUB_PRICES + ECP_RQ_CANCEL   */
},
{                                
    remoteProcedure<ecpSubscribe_GetStat>,        /* ECP_IT_SUBSCRIBE     + ECP_RQ_GET_STAT */
    remoteProcedure<ecpSubscribe_Cache>,          /* ECP_IT_SUBSCRIBE     + ECP_RQ_CACHE    */
    remoteProcedure<ecpSubscribe_Open>,           /* ECP_IT_SUBSCRIBE     + ECP_RQ_OPEN     */
    remoteProcedure<ecpSubscribe_Read>,           /* ECP_IT_SUBSCRIBE     + ECP_RQ_READ     */
    remoteProcedure<ecpSubscribe_Seek>,           /* ECP_IT_SUBSCRIBE     + ECP_RQ_SEEK     */
    remoteProcedure<ecpSubscribe_Close>,          /* ECP_IT_SUBSCRIBE     + ECP_RQ_CLOSE    */
    remoteProcedure<ecpSubscribe_Refresh>,        /* ECP_IT_SUBSCRIBE     + ECP_RQ_REFRESH  */
    remoteProcedure<ecpSubscribe_Unlink>,         /* ECP_IT_SUBSCRIBE     + ECP_RQ_DELETE   */
    remoteProcedure<ecpCancelWebSvcOps>,          /* ECP_IT_SUBSCRIBE     + ECP_RQ_CANCEL   */
},
{                                 
    remoteProcedure<ecpUpdateStatus_GetStat>,     /* ECP_IT_UPDATE_STATUS  + ECP_RQ_GET_STAT */
    remoteProcedure<ecpUpdateStatus_Cache>,       /* ECP_IT_UPDATE_STATUS  + ECP_RQ_CACHE    */
    remoteProcedure<ecpUpdateStatus_Open>,        /* ECP_IT_UPDATE_STATUS  + ECP_RQ_OPEN     */
    remoteProcedure<ecpUpdateStatus_Read>,        /* ECP_IT_UPDATE_STATUS  + ECP_RQ_READ     */
    remoteProcedure<ecpUpdateStatus_Seek>,        /* ECP_IT_UPDATE_STATUS  + ECP_RQ_SEEK     */
    remoteProcedure<ecpUpdateStatus_Close>,       /* ECP_IT_UPDATE_STATUS  + ECP_RQ_CLOSE    */
    remoteProcedure<ecpUpdateStatus_Refresh>,     /* ECP_IT_UPDATE_STATUS  + ECP_RQ_REFRESH  */
    remoteProcedure<ecpUpdateStatus_Unlink>,      /* ECP_IT_UPDATE_STATUS  + ECP_RQ_DELETE   */
    remoteProcedure<ecpCancelWebSvcOps>,          /* ECP_IT_UPDATE_STATUS  + ECP_RQ_CANCEL   */
},
{                                
    remoteProcedure<ecpRedeem_GetStat>,           /* ECP_IT_REDEEM         + ECP_RQ_GET_STAT */
    remoteProcedure<ecpRedeem_Cache>,             /* ECP_IT_REDEEM         + ECP_RQ_CACHE    */
    remoteProcedure<ecpRedeem_Open>,              /* ECP_IT_REDEEM         + ECP_RQ_OPEN     */
    remoteProcedure<ecpRedeem_Read>,              /* ECP_IT_REDEEM         + ECP_RQ_READ     */
    remoteProcedure<ecpRedeem_Seek>,              /* ECP_IT_REDEEM         + ECP_RQ_SEEK     */
    remoteProcedure<ecpRedeem_Close>,             /* ECP_IT_REDEEM         + ECP_RQ_CLOSE    */
    remoteProcedure<ecpRedeem_Refresh>,           /* ECP_IT_REDEEM         + ECP_RQ_REFRESH  */
    remoteProcedure<ecpRedeem_Unlink>,            /* ECP_IT_REDEEM         + ECP_RQ_DELETE   */
    remoteProcedure<ecpCancelWebSvcOps>,          /* ECP_IT_REDEEM         + ECP_RQ_CANCEL   */
},
{                                
    remoteProcedure<ecpSyncTickets_GetStat>,      /* ECP_IT_SYNC_TICKETS   + ECP_RQ_GET_STAT */
    remoteProcedure<ecpSyncTickets_Cache>,        /* ECP_IT_SYNC_TICKETS   + ECP_RQ_CACHE    */
    remoteProcedure<ecpSyncTickets_Open>,         /* ECP_IT_SYNC_TICKETS   + ECP_RQ_OPEN     */
    remoteProcedure<ecpSyncTickets_Read>,         /* ECP_IT_SYNC_TICKETS   + ECP_RQ_READ     */
    remoteProcedure<ecpSyncTickets_Seek>,         /* ECP_IT_SYNC_TICKETS   + ECP_RQ_SEEK     */
    remoteProcedure<ecpSyncTickets_Close>,        /* ECP_IT_SYNC_TICKETS   + ECP_RQ_CLOSE    */
    remoteProcedure<ecpSyncTickets_Refresh>,      /* ECP_IT_SYNC_TICKETS   + ECP_RQ_REFRESH  */
    remoteProcedure<ecpSyncTickets_Unlink>,       /* ECP_IT_SYNC_TICKETS   + ECP_RQ_DELETE   */
    remoteProcedure<ecpCancelWebSvcOps>,          /* ECP_IT_SYNC_TICKETS   + ECP_RQ_CANCEL   */
},
{                                
    remoteProcedure<ecpPurchaseTitle_GetStat>,    /* ECP_IT_PURCHASE_TITLE + ECP_RQ_GET_STAT */
    remoteProcedure<ecpPurchaseTitle_Cache>,      /* ECP_IT_PURCHASE_TITLE + ECP_RQ_CACHE    */
    remoteProcedure<ecpPurchaseTitle_Open>,       /* ECP_IT_PURCHASE_TITLE + ECP_RQ_OPEN     */
    remoteProcedure<ecpPurchaseTitle_Read>,       /* ECP_IT_PURCHASE_TITLE + ECP_RQ_READ     */
    remoteProcedure<ecpPurchaseTitle_Seek>,       /* ECP_IT_PURCHASE_TITLE + ECP_RQ_SEEK     */
    remoteProcedure<ecpPurchaseTitle_Close>,      /* ECP_IT_PURCHASE_TITLE + ECP_RQ_CLOSE    */
    remoteProcedure<ecpPurchaseTitle_Refresh>,    /* ECP_IT_PURCHASE_TITLE + ECP_RQ_REFRESH  */
    remoteProcedure<ecpPurchaseTitle_Unlink>,     /* ECP_IT_PURCHASE_TITLE + ECP_RQ_DELETE   */
    remoteProcedure<ecpCancelWebSvcOps>,          /* ECP_IT_PURCHASE_TITLE + ECP_RQ_CANCEL   */
},
};




static
VNGErrCode registerRPCs(VN* vn,
                        VNRemoteProcedure procs[NUM_ECP_INFO_TYPES][NUM_ECP_REQUESTS])
{
    VNGErrCode ec;
    int        i, j;

    for (i = 0;  i < NUM_ECP_INFO_TYPES;  ++i) {
        for (j = 0; j < NUM_ECP_REQUESTS; ++j) {
            if ((ec = VN_RegisterRPCService (vn,
                                             (i+1)*100 + (j+1),
                                             procs[i][j]))) {
                trace (ERR, ECPVN,
                       "VN_RegisterRPCService procs[%d][%d] returned %d\n", i, j, ec);
                break;
            }
        }
    }
    if (!ec &&
        (ec = VN_RegisterRPCService (vn, ECP_ST_CANCEL_WS_OPS,
                                         remoteProcedure<ecpCancelWebSvcOps>))) {
        trace (ERR, ECPVN,
              "VN_RegisterRPCService ECP_ST_CANCEL_WS_OPS returned %d\n", ec);
    }
    if (!ec &&
        (ec = VN_RegisterRPCService (vn, ECP_ST_ENABLE_PRELOAD,
                                         remoteProcedure<ecpEnablePreload>))) {
        trace (ERR, ECPVN,
              "VN_RegisterRPCService ECP_ST_ENABLE_PRELOAD returned %d\n", ec);
    }

    return ec;
}

static
_SHRThreadRT  _SHRThreadCC  servRPCs (ECPEnv& ecp)
{
    VNG* vng = ecp.vng;
    VNGErrCode ec;

    while ((ec = VNG_ServeRPC(vng, VNG_WAIT)) == VNG_OK) {
        ;
    }
    trace (INFO, ECPVN, "servRPCs exiting with ec %d", ec); 
    return NULL;
}





static
VNGErrCode  setupECProxyVN ()
{
    VNGErrCode   ec;
    ECPEnv      &ecp = __ecpEnv;
    VNG         *vng = ecp.vng;
    VN          *vn  = ecp.ncProxyVN;
    VNId         vnId;
    VNGGameInfo  info;
    int          i;
    char         comments[]  = "";

    VNPolicies     policies = VN_ABORT_ON_OWNER_EXIT; /* not auto accept */

    if ((ec = VNG_NewVN(vng, vn, VN_CLASS_2, VN_DOMAIN_ADHOC, policies))) {
        trace (ERR, ECPVN, "VNG_NewVN() Failed %d\n", ec);
        return ec;
    }

    trace (INFO, ECPVN, "VNG_NewVN() Success\n");

    if ((ec = VN_GetVNId(vn, &vnId))) {
        trace (ERR, ECPVN, "VN_GetVNId() returned %d\n", ec);
        goto end;
    }

    ecp.ncProxyVNId = vnId;

    trace (INFO, ECPVN, "Created EC Proxy vn %p  vn->id %d  "
                 "deviceId: 0x%I64x   netId: 0x%x\n",
                 vn, vn->id, vnId.deviceId, vnId.netId);

    // register nc proxy vn

    info.vnId = vnId;
    info.owner = VNG_MyUserId(vng);   /* will be VNG_INVALID_USER_ID */
    info.gameId = ECP_GAME_ID;
    info.titleId = ECP_TITLE_ID;
    info.accessControl = VNG_GAME_PUBLIC;  // should be called viewability control
    info.netZone = 0;      // server will decide this value
    info.totalSlots = 2;
    info.buddySlots = 0;   // nothing reserved for buddies
    info.maxLatency = 100; // 100ms RTT 
    info.attrCount = ECP_GAME_ATTR_ID + 1;
    strncpy(info.keyword, ECP_GAME_KEYWORD, sizeof info.keyword);
    for (i = ECP_GAME_ATTR_ID;  i >= 0;  --i) {
        info.gameAttr[i] = ECP_GAME_ATTR_VALUE - i;
    }

    if ((ec = VNG_RegisterGame (vng, &info, comments, ecp.vngTimeout))) {
        trace (ERR, ECPVN, "VNG_RegisterGame() Failed %d\n", ec);
        goto end;
    }

    trace (INFO, ECPVN, "Register EC Proxy Success\n");

    ec = VNG_UpdateGameStatus (vng, vnId,
                               ECP_GAME_STATUS, ECP_NUM_PLAYERS,
                               ecp.vngTimeout);
    if (ec) {
        trace (ERR, ECPVN, "VNG_UpdateGameStatus Failed %d\n", ec);
        goto end;
    }

    if ((ec = registerRPCs (vn, rpcProcs))) {
        trace (ERR, ECPVN, "registerRPCs() Failed %d\n", ec);
        goto end;
    }

    trace (INFO, ECPVN, "registerRPCs() Success\n");

end:
    if (ec) {
        VNG_DeleteVN (vng, vn);
    }

    return ec;
}



static
void setupEnv(string& pki)
{
    static const char* env_nv_device_name   = "NV_DEVICE_NAME";
    static const char* env_nv_device_pki    = "NV_DEVICE_PKI";
    static const char* env_vn_cert_sys_file = "VN_CERT_SYS_FILE";
    static const char* env_root             = "ROOT";
    static const char* sdk_cert_path        = "/usr/etc/pki_data/";
    static const char* prod_cert_file       = "pccert_qpki.sys";
    static const char* beta_cert_file       = "pccert_bpki.sys";

    char* nv_device_name;
    char* nv_device_pki;
    char* vn_cert_sys_file;
    char* root;
    string cert_dir;
    static string env_string;

    nv_device_name   = getenv(env_nv_device_name);
    nv_device_pki    = getenv(env_nv_device_pki);
    vn_cert_sys_file = getenv(env_vn_cert_sys_file);
    root             = getenv(env_root);

    trace (FINER, MISC, "%s on entry: %s\n", env_nv_device_name, nv_device_name);
    trace (FINER, MISC, "%s on entry: %s\n", env_nv_device_pki, nv_device_pki);
    trace (FINER, MISC, "%s on entry: %s\n", env_vn_cert_sys_file, vn_cert_sys_file);
    trace (FINER, MISC, "%s on entry: %s\n", env_root, root);

    if (root) {
        cert_dir  = root;
        cert_dir += sdk_cert_path;
    }
    else {
        cert_dir = "./";
    }

    if (!pki.empty() || nv_device_name == NULL) {
        if (pki == "prod") {
            putenv ("NV_DEVICE_NAME=PC00000116");
        }
        else {
            putenv ("NV_DEVICE_NAME=PC00000033");
        }
        trace (FINER, MISC, "%s changed to: %s\n",
                        env_nv_device_name, getenv(env_nv_device_name));
    }

    if (!pki.empty() || nv_device_pki == NULL) {
        if (pki == "prod") {
            putenv ("NV_DEVICE_PKI=NC_PPKI");
        }
        else {
            putenv ("NV_DEVICE_PKI=NC_BPKI");
        }
        trace (FINER, MISC, "%s changed to: %s\n",
                        env_nv_device_pki, getenv(env_nv_device_pki));
    }

    if (!pki.empty() || vn_cert_sys_file == NULL) {
        env_string  = env_vn_cert_sys_file;
        env_string += "=" + cert_dir;
        if (pki == "prod") {
            env_string += prod_cert_file;
        } else {
            env_string += beta_cert_file;
        }
        putenv (env_string.c_str());
        trace (FINER, MISC, "%s changed to: %s\n",
                env_vn_cert_sys_file, getenv(env_vn_cert_sys_file));
    }
}


#define ECP_APP_UNIQUE_MUTEX_NAME   "ECP_APP_UNIQUE_MUTEX_NAME"

static
bool isAlreadyRunning()
{
    HANDLE hMutex;

    hMutex = CreateMutex (NULL, true, ECP_APP_UNIQUE_MUTEX_NAME);

    if (hMutex != NULL && GetLastError() == ERROR_ALREADY_EXISTS)
    {
        CloseHandle(hMutex);
        return true;
    }
    return false;
}


int ECP_run ()
{
   _SHRError     er;
    ECError      rv = EC_ERROR_OK;
    VNGErrCode   ec;
    ECPEnv      &ecp = __ecpEnv;
    VNG         *vng = ecp.vng;


    // Doesn't call inherited App::run(). Just do required parts here.
    // (later, delete App::run() entirely. Maybe delete App class)

    _SHR_set_trace_level(ecp.traceLevel);

    if (isAlreadyRunning()) {
        trace (ERR, MISC, "NC ECommerce proxy is already running\n");
        rv = EC_ERROR_ECP;
        goto end;
    }

    if ((rv = _SHR_mutex_init(ecp.mutex, _SHR_MUTEX_RECURSIVE, 0, 1))) {
        trace (ERR, MISC, "_SHR_mutex_init returned %d\n", rv);
        rv = EC_ERROR_ECP;
        goto end;
    }

    trace (INFO, MISC, "NC ECommerce service: %s\n", ecp.ecsURL.c_str());

    setupEnv (ecp.pki);

    if ((ec = VNG_Init(vng, NULL))) {
        trace (ERR, ECPVN, "VNG_Init returned %d\n", ec);
        rv = EC_VngToECError (ec);
        goto end;
    }

    if ((ec = setupECProxyVN())) {
        trace (ERR, ECPVN, "setupECProxyVN returned %d\n", ec);
        rv = EC_VngToECError (ec);
        goto fini;
    }

    for (int i = 0; i < ecp.nRPCThreads; i++) {
         if ((rv = _SHR_thread_create (&ecp.servRPCThreads[i], NULL,
                                        (_SHRThreadFunc) servRPCs, &ecp))) {
            trace (ERR, ECPVN, "_SHR_thread_create returned %d", rv);
            for (; i < ecp.nRPCThreads;  i++) {
                ecp.servRPCThreads[i] = 0;
            }
            rv = EC_ERROR_ECP;
            goto fini;
        }
    }

    size_t n = ecp.ecsURL.copy(__ecsEndpoint, sizeof __ecsEndpoint, 0);

    if (n == sizeof __ecsEndpoint) {
        rv = EC_ERROR_ECP;
        goto fini;
    }

    __ecsEndpoint[n] = '\0';

    {
        VNId vnId = ecp.ncProxyVNId;
        VN            *vn_hosted;
        VNGUserInfo    userInfo;
        char           joinReason[VNG_JOIN_REQ_MSG_BUF_SIZE];
        uint64_t       requestId;

        while (1) {

            if ((er = _SHR_mutex_unlock (ecp.mutex))) {
                assert (!er);
                trace (ERR, MISC, "unlock: _SHR_mutex_unlock returned %d\n", er);
                rv = EC_ERROR_ECP;
            }

            if (rv) {
                break;
            }

            ec = VNG_GetJoinRequest(vng,
                                    vnId,  // only accept ncProxyVN requests
                                    &vn_hosted,  // vn out will always be ncProxyVN
                                    &requestId,
                                    &userInfo,
                                    joinReason,
                                    sizeof joinReason,
                                    ECP_MAIN_LOOP_INTERVAL_MS);
                                        
            if (ec && ec != VNGERR_TIMEOUT) {
                trace (ERR, ECPVN, "VNG_GetJoinRequest returned %d\n", ec);
                continue;
            }

            if (ec == VNG_OK) {

                if ((ec = VNG_AcceptJoinRequest(vng, requestId))) {
                    trace (ERR, ECPVN, "VNG_AcceptJoinRequest returned %d\n", ec);
                } else {
                    trace (INFO, ECPVN, "Got and accepted join Request\n");
                }
            }

            if (__exitRequested) {
                break;
            }

            if ((er = _SHR_mutex_lock (ecp.mutex))) {
                assert (!er);
                trace (ERR, MISC, "lock: _SHR_mutex_lock returned %d\n", er);
                rv = EC_ERROR_ECP;
                break;
            }

            rv = ecp.contentCache.monitorNetContent();
        }
    }

fini:
    if ((ec = VNG_Fini(vng))) {
        trace (ERR, ECPVN, "VNG_Fini returned %d", ec);
        if (!rv) {
            rv = EC_VngToECError (ec);
        }
    }

end:
    trace (INFO, MISC, "NC ECommerce proxy exiting.\n");
    return rv;
}




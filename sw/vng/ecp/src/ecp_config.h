/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __ECP_CONFIG_H__
#define __ECP_CONFIG_H__

#pragma once


#define ECP_DEF_ECS_URL          "https://www.idc.ique.com:16981/ecs/services/ECommerceSOAP"
#define ECP_DEF_CCS_URL_PREFIX   "http://www.idc.ique.com:16983/ccs/download"
#define ECP_DEF_UCS_URL_PREFIX    ECP_DEF_CCS_URL_PREFIX

#define LOCALHOST_PREFIX  "http://localhost:"
#define DEF_TCPMON_PORT   "4333"
#define ECS_URL_TAIL      "/ecs/services/ECommerceSOAP"

#define ECP_LAB1_ECS_URL         "http://ecs.bbu.lab1.routefree.com:17105/ecs/services/ECommerceSOAP"
#define ECP_LAB1_CCS_URL_PREFIX  "http://ccs.bbu.lab1.routefree.com:16983/ccs/download"
#define ECP_LAB1_UCS_URL_PREFIX   ECP_LAB1_CCS_URL_PREFIX
#define ECP_LOCALHOST_URL        "http://localhost:8080/ecs/services/ECommerceSOAP"

#define ECP_DEF_TCPMON_URL       LOCALHOST_PREFIX  DEF_TCPMON_PORT  ECS_URL_TAIL

/* wget \
http://ccs.bbu.lab1.routefree.com:16983/ccs/download\?titleId=00000000000d9038&contentId=054c55ec
*/





#define ECP_WININET_HttpOpenRequest_FLAGS     (INTERNET_FLAG_KEEP_CONNECTION \
                                              | INTERNET_FLAG_IGNORE_CERT_CN_INVALID)

#ifndef ECP_WININET_HttpOpenRequest_FLAGS
    #define ECP_WININET_HttpOpenRequest_FLAGS  NULL
#endif


#define ECP_STRING(x)   #x

/* From config_var.h */
/* 0 means no timeout */
#define ECP_NET_CONTENT_TO       0
#define DEF_NET_CONTENT_TO       ECP_STRING(ECP_NET_CONTENT_TO)



#endif /*__ECP_CONFIG_H__*/

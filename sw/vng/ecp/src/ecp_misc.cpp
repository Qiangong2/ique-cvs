/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */




#include "ecp_i.h"
#include "common.h"
#include "md5.h"





int32_t  ecpNotSupported (VN         *vn,
                            VNMsgHdr   *hdr,
                            const void *arg,
                            size_t      argLen,
                            void       *ret,
                            size_t     *retLen)
{
    return EC_ERROR_NOT_SUPPORTED;
}






int32_t  ecpEnablePreload (VN         *vn,
                           VNMsgHdr   *hdr,
                           const void *arg,
                           size_t      argLen,
                           void       *ret,
                           size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;
    bool       enablePreload;

    if (!arg || argLen != sizeof(bool32)) {
        rv = EC_ERROR_ECP;
    } else {
        enablePreload = (*((bool32*) arg)) ? true : false;

        if (!ecp.isEnablePreload && enablePreload) {
            size_t nCids = ecp.ecMetaCache.preloadContentIds.size();
            for (size_t i = 0; i < nCids; ++i) {
                ecp.contentCache.bgCacheNetContent (
                        ecp.ecMetaCache.preloadContentIds[i].c_str());
            }
        }
        else if (ecp.isEnablePreload && !enablePreload) {
            // since only preload is done as bg cache, cancel all
            ECPContentListArg  a;
            a.fg = false;
            a.count = 0; // count == 0 means cancel all a.fg ? fg : bg
            a.cids = NULL;
            rv = ecp.contentCache.cancel(&a);
        }

        ecp.isEnablePreload = enablePreload;
    }

    return rv;
}






ECError ecpCloseThreadHandle (HANDLE th)
{
    ECError rv = EC_ERROR_ECP;
    DWORD   exit_code;

    if (!::GetExitCodeThread (th, &exit_code)) {
        trace (ERR, MISC,
                "GetExitCodeThread Failed GetLastError %d\n", ::GetLastError());
    }
    else if (exit_code != STILL_ACTIVE) {
        if (!::CloseHandle (th)) {
            trace (ERR, MISC,
                    "CloseHandle(th) Failed GetLastError %d\n", ::GetLastError());
        } else {
            rv = EC_ERROR_OK;
        }
    } else {
        rv = EC_ERROR_BUSY;
    }
    
    return rv;
}





#define ECARD_TYPE_LEN     6
#define ECARD_SERIAL_LEN  10
#define ECARD_SECRET_LEN  10


int reformatEcard (const string&  in,
                         string&  eCardType,    // out
                         string&  eCardNumber,  // out
                         string&  eCardHash)    // out
{
    bool dump = false;

    if (in.size() != ECARD_TYPE_LEN + ECARD_SERIAL_LEN + ECARD_SECRET_LEN) {
        trace (INFO, MISC, "reformatEcard: invalid ecard %s\n", in.c_str());
        return EC_ERROR_ECARD;
    }

    eCardType = in.substr(0, ECARD_TYPE_LEN);
    eCardNumber = in.substr(ECARD_TYPE_LEN, ECARD_SERIAL_LEN);
    string secret = in.substr(ECARD_TYPE_LEN + ECARD_SERIAL_LEN, ECARD_SECRET_LEN);
    string md5secret;

    MD5_CTX ctx;
    unsigned char md[MD5_DIGEST_LENGTH];
    MD5_Init(&ctx);
    MD5_Update(&ctx, (unsigned char *)secret.c_str(), ECARD_SECRET_LEN);
    MD5_Final(md, &ctx);
    hex_encode(md, MD5_DIGEST_LENGTH, md5secret);

    eCardHash = md5secret;

    if (dump) {
        trace (INFO, MISC, "reformatEcard %s to %s\n", in.c_str(), eCardHash.c_str());
    }

    return 0;
}

int reformatEcard(const string& in, string& out)
{
    string  eCardType;
    string  eCardNumber;
    string  eCardHash;
    
    if (!reformatEcard (in, eCardType, eCardNumber, eCardHash)) {
        return EC_ERROR_ECARD;
    }
    
    out = eCardType + eCardNumber + eCardHash;
    
    return 0;
}





/* returns negative value if content_id is not a valid hex96 content type */

ECContentType  contentTypeFromId (const string& content_id)
{
    ECPEnv        &ecp = __ecpEnv;
    ECContentType  rv = (ECContentType) -1;
    string         contentId32;
    string         typeStr;

    if (content_id.size() < 17) {

        trace (ERR, CTNT, "contentTypeFromId was passed invalid content_id %s\n",
                (content_id.size() ? content_id.c_str() : "<empty>"));
        goto end;

    }

    typeStr = content_id.substr(16);

    size_t  n = sizeof ecp.contentTypeStrs / sizeof ecp.contentTypeStrs[0];
    size_t  i;

    for (i = 0;  i < n;  ++i) {
        if (typeStr == ecp.contentTypeStrs[i]) {
            rv = (ECContentType) i;
            break;
        }
    }

    if (i == n) {
        contentId32  =  content_id.substr(16);
        int hexDigits = 0;

        for (i = 0; i < contentId32.size();  ++i) {
            if (i > 7 || !isxdigit(contentId32[i])) {
                break;
            }
            ++hexDigits;
        }
        if (hexDigits == 8 && contentId32.size() == 8) {
            rv = EC_CONTENT;
        }
        else {
            trace (ERR, MISC, "contentTypeFromId: "
                "titleId 0x%s  ContentId32 %s isn't 8 digit hex number "
                "and isn't recognized ECContentType string\n",
                content_id.c_str(), contentId32.c_str());
        }
    }


end:
    return rv;
}



isValidContentIdFormat(const string& content_id)
{
    return (contentTypeFromId(content_id) >= 0);
}


// saveContentInfo expects an EC_CONTENT hex id string
ECError ECPEnv::saveContentInfo (const string&    contentIdhex96,
                                 s64              contentSize,
                                 ECContentInfo&   contentInfo,
                                 bool             isExpectedTitleId,
                                 ESTitleId        expectedTitleId)
{
    ECError  rv = EC_ERROR_OK;
    unsigned i;

    string     titleIdHi    =  contentIdhex96.substr(0,8);
    string     titleIdLow   =  contentIdhex96.substr(8,8);
    string     titleId64    =  contentIdhex96.substr(0,16);
    string     contentId32  =  contentIdhex96.substr(16);

    contentInfo.size = (s32) contentSize;
    contentInfo.id.titleId = _strtoui64(titleId64.c_str(), NULL, 16);
    contentInfo.id.contentId = strtoul(contentId32.c_str(), NULL, 16);
    contentInfo.id.contentType = contentTypeFromId(contentIdhex96);

    if (contentInfo.id.contentType < 0) {
        rv = contentInfo.id.contentType;
        trace (ERR, ECWS,
            "titleId 0x%s.%s  ContentId32 %s  contentInfo returned %d\n",
            titleIdHi.c_str(), titleIdLow.c_str(), contentId32.c_str(), rv);
        goto end;
    }
    else if (contentInfo.id.contentType == EC_CONTENT) {
        if (isExpectedTitleId && contentInfo.id.titleId != expectedTitleId
            && titleIdHi != "00000000" && expectedTitleId >= 0x100000000) {
            trace (ERR, ECWS, "saveContentInfo: "
                "ContentId32 %s (%u), titleId 0x%s.%s (%u.%u), "
                "expected 0x%08x.%08x (%u.%u)\n",
                contentId32.c_str(), contentInfo.id.contentId,
                titleIdHi.c_str(), titleIdLow.c_str(),
                (u32)(contentInfo.id.titleId >> 32), (u32)(contentInfo.id.titleId),
                (u32)(expectedTitleId >> 32), (u32)(expectedTitleId),
                (u32)(expectedTitleId >> 32), (u32)(expectedTitleId));
            rv = EC_ERROR_ECP;
            goto end;
        }

        for (i = 0; i < contentId32.size();  ++i) {
            if (i > 7 || !isxdigit(contentId32[i])) {
                trace (ERR, ECWS, "saveContentInfo: "
                    "titleId 0x%s.%s  ContentId32 %s isn't 8 digit hex number\n",
                    titleIdHi.c_str(), titleIdLow.c_str(), contentId32.c_str());
                rv = EC_ERROR_ECP;
                goto end;
            }
        }
    }

    contents[contentIdhex96] = contentInfo;

end:
    return rv;
}



ECError reportContentInfo (ECContentInfo& content)
{
    ECPEnv   &ecp = __ecpEnv;
    char     *type = "<unknown>";

    if (content.id.contentType <
            (sizeof ecp.contentTypeStrs/sizeof ecp.contentTypeStrs[0])) {
        type = ecp.contentTypeStrs[content.id.contentType];
    }
    trace (INFO, ECWS,
        "    titleId %u.%u  type %s  id %u   size %d\n",
        (u32)(content.id.titleId >> 32), (u32)(content.id.titleId),
        type, content.id.contentId, content.size);

    if (content.id.contentType != EC_CONTENT) {
        trace (ERR, ECWS, "found unexpected content type %d\n",
                    content.id.contentType);
        return EC_ERROR_ECP;
    }
    return EC_ERROR_OK;
}



// returns titleID as hex string catenated with either contentId as hex string
// or "tmd", "met", etc.
//
ECError  cid96FromECContentId(ECContentId& cid, string& cid96)
{
    ECPEnv   &ecp = __ecpEnv;
    ECError  rv = EC_ERROR_OK;

    char hexTitleId   [ECP_64_BIT_HEX_STRING_BUF_SIZE];
    char hexContentId [ECP_32_BIT_HEX_STRING_BUF_SIZE];
    string cidStr;

    snprintf(hexTitleId, ECP_64_BIT_HEX_STRING_BUF_SIZE, "%016I64x",
        cid.titleId);


    if (cid.contentType == EC_CONTENT) {
        snprintf(hexContentId, ECP_32_BIT_HEX_STRING_BUF_SIZE, "%08x",
                                    cid.contentId);
        cidStr = hexContentId;
    }
    else if (cid.contentType <
                (sizeof ecp.contentTypeStrs/sizeof ecp.contentTypeStrs[0])) {
            cidStr = ecp.contentTypeStrs[cid.contentType];
    }
    else {
        cidStr = "unknown";
        rv = EC_ERROR_INVALID;
    }

    cid96 = hexTitleId + cidStr;

    return rv;
}







ECError iah2ecErr (int iahErrCode)
{
    ECError rv = EC_ERROR_FAIL;

    if (iahErrCode < 0) {
        rv = EC_ERROR_FAIL; // Not valid IAH_ERR code
    } else if (iahErrCode <= 100) {
        rv = EC_ERROR_OK; // progress percent
    } else if (iahErrCode < 500) {
        rv = EC_ERROR_FAIL; // IAH_ERR code not meaningfull in ecp context
    } else if (iahErrCode < IAH_BASE_HTTP_STATUS)  switch (iahErrCode) {

        // IAH cacheContent errors

        case IAH_ERR_NetContent:            // 500
            // A non-specific failure while getting content via net
            rv = EC_ERROR_NET_CONTENT;
            break;

        case IAH_ERR_NetContent_NET_NA:     // 553
            // Attempt to cacheContent/Title when net is not available
            rv = EC_ERROR_NET_NA;
            break;

        case IAH_ERR_NetContentTimeout:     // 555
            // Timeout while cacheContent/Title
            rv = EC_ERROR_NET_CONTENT;  // we don't use a timeout
            break;

        case IAH_ERR_ContentNotAvailable:   // 558
            // Couldn't get content from CCS server,reason unspecific
            rv = EC_ERROR_NET_CONTENT;
            break;

        case IAH_ERR_CantCreateCacheFile:   // 559
            // Couldn't create Cache file
            rv = EC_ERROR_NET_CONTENT_FILE;
            break;

        case IAH_ERR_OPERATION_CANCELED:    // 992
            // NetContent operation was cancled
            rv = EC_ERROR_NOT_CACHED;
            break;

        case IAH_ERR_NET_NA:                // 993
            // Internet is not available
            rv = EC_ERROR_NET_NA;
            break;

        default:
            rv = EC_ERROR_NET_CONTENT;
            break;

    } else if (iahErrCode >= IAH_BASE_HTTP_STATUS
                 && iahErrCode <= IAH_MAX_HTTP_STATUS) {
            // return actual http status code (only negative) if > 200
            rv = iahErrCode - IAH_BASE_HTTP_STATUS;
            if (rv > 200) {
                rv = -rv;
            } else {
                rv = EC_ERROR_NET_CONTENT;
            }
    } else {
        rv = EC_ERROR_FAIL; // IAH_ERR code not meaningfull in ecp context
    }

    return rv;
}




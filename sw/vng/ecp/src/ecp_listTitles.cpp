/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#include "ecp_i.h"
#include "ecp_ecsIfc.h"



int ECPListTitlesCache::cacheListTitles ()
{
    ECPEnv   &ecp = __ecpEnv;
    int      rv = 0;

    netReq = new ECPListTitlesReq;

    if (!netReq) {
        rv = EC_ERROR_NOMEM;
        goto end;
    }

    ECPListTitlesReq *x = (ECPListTitlesReq*) netReq;

    ecs__ListTitlesRequestType&  req  = x->req;
    ecs__ListTitlesResponseType& resp = x->resp;

     initECSReq (req, deviceId);

    x->titleKind   = ec2soapTitleKind (cached.titleKind);
    x->pricingKind = ec2soapPricingKindType (cached.pricingKind);

    if (cached.channelId < 0) {
        req.ChannelId = NULL;
    } else {
        x->channelId  = tohexstring (cached.channelId, 8);
        req.ChannelId = &x->channelId;
    }

    if (!cached.titleKind || cached.titleKind == EC_TITLE_KIND_GAME_AND_MANUAL) {
        req.TitleKind = NULL;
    } else if (x->titleKind >= 0) {
        req.TitleKind = &x->titleKind;
    } else {
        rv = EC_ERROR_INVALID;
        goto end;
    }

    if (!cached.pricingKind) {
        req.PricingKind = NULL;
    } else if (x->pricingKind >= 0
                  && x->pricingKind == ecs__PricingKindType__Subscription) {
        // treat anything other than subscription as an error
        req.PricingKind  = &x->pricingKind;
    } else {
        rv = EC_ERROR_INVALID;
        goto end;
    }

    // start thread to call service in background

    status.cachedSize = 0;
    expireTime = 0;

    if ((rv = _SHR_thread_create (&netThread, NULL,
                                    (_SHRThreadFunc) listTitlesThread, x))) {
        trace (ERR, ECWS, "_SHR_thread_create returned %d", rv);
        netThread = 0;
        rv = EC_ERROR_ECP;
        goto end;
    }

    ecp.isWebServiceActive = true;

end:
    if (rv && netReq) {
        delete netReq;
        netReq = NULL;
    }

    return rv;
}



// static member
ECError ECPListTitlesCache::listTitlesThread (ECPListTitlesReq *netReq)
{
    return __ecpEnv.listTitlesCache.accessNet (netReq);
}


ECError ECPListTitlesCache::accessNet (ECPListTitlesReq *ws)
{
    ECPEnv   &ecp = __ecpEnv;
    unsigned i;
    size_t   n;
    int      rv;
    const char *msg = "<No message provided>";

    ECommerceSOAPBinding& ecs         = *ws->ecs;
    ecs__ListTitlesRequestType&  req  =  ws->req;
    ecs__ListTitlesResponseType& resp =  ws->resp;

    rv = ecs.__ecs__ListTitles(&req, &resp);

    lock(ecp.mutex);

    if (netReq != ws) {
        rv = EC_ERROR_NOT_BUSY;
        goto canceled;
    }

    if (rv != SOAP_OK) {
        soap_print_fault (ecs.soap, stdout);
        fflush(stdout);
        rv = EC_ERROR_ECS_NA;
        goto end;
    }

    if (resp.ErrorCode != 0) {
        if (resp.ErrorMessage) {
            msg = resp.ErrorMessage->c_str();
        }
        trace (INFO, ECWS, "ListTitles resp.ErrorCode %d: %s\n",
                resp.ErrorCode, msg);
        rv = EC_ECS_ERROR_RANGE_START - resp.ErrorCode;
        goto end;
    }

    ECListTitlesInfo *info;
    ecs__TitleType   *title;

    for (i = 0;  i < resp.Titles.size(); ++i) {
        title = resp.Titles[i];
        if (!title) {
            continue;
        }
        info = new ECListTitlesInfo;
        info->id = _strtoui64(title->TitleId.c_str(), NULL, 16);
        n = title->TitleName.copy(info->name, sizeof info->name -1, 0);
        info->name[n] = 0;
        info->fsSize = title->FsSize;
        titles.push_back(info);
        status.cachedSize += sizeof ECListTitlesInfo;

        trace (INFO, ECWS, "ListTitles[%d]  TitleId 0x%s (%u.%u)  size %I64d  name %s\n",
                i,
                resp.Titles[i]->TitleId.c_str(),
                (u32)(info->id >> 32), (u32)(info->id),
                resp.Titles[i]->FsSize,
                resp.Titles[i]->TitleName.c_str());
    }

    status.totalSize = status.cachedSize;
    cachedTime = now();
    expireTime = cachedTime + cacheDur;

end:

    if (rv < 0) {
        status.cachedSize = rv;
        titles.clear();
    }

    if (netReq == ws) {
        netReq = NULL;
    }
    ecp.isWebServiceActive = false;

canceled:
    delete ws;
    unlock(ecp.mutex);

    return 0; // doesn't go anywhere
}






ECError ECPListTitlesCache::getStat(ECPListTitlesArg& id, ECCacheStatus& stat)
{
    if (netThread && !netReq) {
        ecpCloseThreadHandle (netThread);
        netThread = 0;
    }

    if (status.cachedSize != EC_ERROR_NOT_CACHED
            && id.titleKind == cached.titleKind
            && id.pricingKind == cached.pricingKind
            && id.channelId == cached.channelId
            && (expireTime == 0 || now() < expireTime)) {
        stat = status;
    } else {
        stat.cachedSize = EC_ERROR_NOT_CACHED;
        stat.totalSize = EC_ERROR_INVALID;
    }

    return EC_ERROR_OK;
}

ECError ECPListTitlesCache::cache(ECPListTitlesArg& id)
{
    // delete current cache
    // get tiles from web service
    // Allow cache, refresh, unlink even if open,
    // but don't refresh on cache request if already cached

    ECError   rv = EC_ERROR_OK;

    ECCacheStatus  stat;

    if (!(rv = getStat (id, stat))) {
        // it is either all there, or not cached
        if (stat.cachedSize < 0 || stat.cachedSize != stat.totalSize) {

            unlink(id);
            cached = id;
            rv = cacheListTitles();
        }
    }

    return rv;
}


ECError ECPListTitlesCache::open    (ECPListTitlesArg& id)
{
    ECError        rv;
    ECCacheStatus  stat;

    if (!(rv = getStat (id, stat))) {
        // it is either all there, or not cached
        if (stat.cachedSize < 0) {
            rv = EC_ERROR_NOT_CACHED;
        }
        else {
            isOpen = true;
            offset = 0;
            cached.skip = id.skip;
        }
    }

    return rv;
}


ECError ECPListTitlesCache::read    (ECPListTitlesArg& id, void *ret, size_t *retLen)
{
    if (!isOpen
            || id.titleKind != cached.titleKind
            || id.pricingKind != cached.pricingKind) {

        return EC_ERROR_NOT_OPEN;
    }

    if (status.cachedSize < 0) {
        return EC_ERROR_NOT_CACHED;
    }

    if (id.skip != cached.skip) {
        offset = cached.skip = id.skip;
    }

    size_t infoLen = sizeof(ECListTitlesInfo);
    size_t nCached = titles.size();
    size_t maxRet  = VN_MAX_MSG_LEN / infoLen;
    size_t nRet;
    uint8_t *r = (uint8_t*) ret;

    if (offset >= nCached)
        nRet = 0;
    else
        nRet = nCached - offset;

    if (nRet > cached.nTitles)
        nRet = cached.nTitles;

    if (nRet > maxRet)
        nRet = maxRet;

    for (size_t i = 0;  i < nRet;  ++i) {
        memcpy (r, titles[offset + i], infoLen);
        r += infoLen;
    }

    offset += nRet;
    *retLen = nRet * infoLen;

    return EC_ERROR_OK;
}


ECError ECPListTitlesCache::seek    (ECPListTitlesArg& id)
{
    // This is never called in actual practice

    if (!isOpen
            || id.titleKind != cached.titleKind
            || id.pricingKind != cached.pricingKind) {

        return EC_ERROR_NOT_OPEN;
    }

    if (status.cachedSize < 0) {
        return EC_ERROR_NOT_CACHED;
    }

    offset = cached.skip = id.skip;

    return EC_ERROR_OK;
}


ECError ECPListTitlesCache::close   (ECPListTitlesArg& id)
{
    if (!isOpen) {
        return EC_ERROR_NOT_OPEN;
    }

    if (id.titleKind != cached.titleKind
            || id.pricingKind != cached.pricingKind) {

        trace (WARN, ECWS, "Mismatched arg on list titles close\n");
    }

    isOpen = false;

    if (expireOnClose) {
        expireTime = 1;
    }

    return EC_ERROR_OK;
}



ECError ECPListTitlesCache::unlink  (ECPListTitlesArg& id)
{
    // This is never called from the NC in actual practice
    // but is called from other APIs

    close(id);

    for (size_t i = 0;  i < titles.size();  ++i) {
        delete titles[i];
    }

    titles.clear();
    status.cachedSize = EC_ERROR_NOT_CACHED;
    status.totalSize = EC_ERROR_INVALID;
    isOpen = false;
    offset = 0;

    return EC_ERROR_OK;
}







int32_t  ecpListTitles_GetStat (VN         *vn,
                                VNMsgHdr   *hdr,
                                const void *arg,
                                size_t      argLen,
                                void       *ret,
                                size_t     *retLen)
{
    ECError   rv;
    ECPEnv   &ecp = __ecpEnv;

    if (!arg || !ret || !retLen || argLen != sizeof (ECPListTitlesArg)) {
        rv = EC_ERROR_ECP;
        goto end;
    }

    rv = ecp.listTitlesCache.getStat (*(ECPListTitlesArg*) arg,
                                        *(ECCacheStatus*) ret);

    if (!rv) {
        *retLen = sizeof(ECCacheStatus);
    }

end:
    return rv;
}


/*
 *  if !cached
 *      get from ecs web service
 *  return 0
 */
int32_t  ecpListTitles_Cache (VN         *vn,
                              VNMsgHdr   *hdr,
                              const void *arg,
                              size_t      argLen,
                              void       *ret,
                              size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;
    VNGErrCode ec;

    if (ecp.isWebServiceActive) {
        rv = EC_ERROR_BUSY;
    }
    else if (!arg || argLen != sizeof(ECPListTitlesArg)) {
        rv = EC_ERROR_ECP;
    }
    else if ((ec = VN_GetMemberDeviceId(vn, hdr->sender,
                                          &ecp.listTitlesCache.deviceId))) {
        trace (ERR, ECWS,
               "list titles VN_GetMemberDeviceId returned error %d\n", ec);
        rv = EC_ERROR_ECP;
    }
    else {
        rv = ecp.listTitlesCache.cache(*(ECPListTitlesArg*) arg);
    }

    return rv;
}

int32_t  ecpListTitles_Open (VN         *vn,
                             VNMsgHdr   *hdr,
                             const void *arg,
                             size_t      argLen,
                             void       *ret,
                             size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (!arg || argLen != sizeof(ECPListTitlesArg)) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.listTitlesCache.open (*(ECPListTitlesArg*) arg);
    }

    return rv;
}

int32_t  ecpListTitles_Read (VN         *vn,
                             VNMsgHdr   *hdr,
                             const void *arg,
                             size_t      argLen,
                             void       *ret,
                             size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (!ret || !retLen || !arg || argLen != sizeof(ECPListTitlesArg)) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.listTitlesCache.read (*(ECPListTitlesArg*)arg, ret, retLen);
    }

    return rv;
}

int32_t  ecpListTitles_Seek (VN         *vn,
                             VNMsgHdr   *hdr,
                             const void *arg,
                             size_t      argLen,
                             void       *ret,
                             size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (!arg || argLen != sizeof(ECPListTitlesArg)) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.listTitlesCache.seek (*(ECPListTitlesArg*) arg);
    }

    return rv;
}

int32_t  ecpListTitles_Close (VN         *vn,
                              VNMsgHdr   *hdr,
                              const void *arg,
                              size_t      argLen,
                              void       *ret,
                              size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (!arg || argLen != sizeof(ECPListTitlesArg)) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.listTitlesCache.close (*(ECPListTitlesArg*) arg);
    }

    return rv;
}

int32_t  ecpListTitles_Refresh (VN         *vn,
                                VNMsgHdr   *hdr,
                                const void *arg,
                                size_t      argLen,
                                void       *ret,
                                size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (!arg || argLen != sizeof(ECPListTitlesArg)) {
        rv = EC_ERROR_ECP;
    } else {
        ECPListTitlesArg *ca = (ECPListTitlesArg*) arg;
        rv = ecp.listTitlesCache.refresh (*ca, *ca);
    }

    return rv;
}


int32_t  ecpListTitles_Unlink (VN         *vn,
                               VNMsgHdr   *hdr,
                               const void *arg,
                               size_t      argLen,
                               void       *ret,
                               size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (!arg || argLen != sizeof(ECPListTitlesArg)) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.listTitlesCache.unlink (*(ECPListTitlesArg*) arg);
    }

    return rv;
}


/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __ECSCLIENTIFC_H__

#include "ecsECommerceSOAPBindingProxy.h"
#include "gsoapWinInet.h"


/*   getECommerceSOAPBinding()
 *
 *   Gets a pointer to an ECommerceSOAPBinding for use with a request.
 *   When done with the request, should return the
 *   the ECommerceSOAPBinding by calling putECommerceSOAPBinding().
 *
 */
ECommerceSOAPBinding *getECommerceSOAPBinding ();


/*   putECommerceSOAPBinding()
 *
 *   Releases an ECommerceSOAPBinding for use in another request.
 *
 *   For information only, returns true if another ECommerceSOAPBinding
 *   had not already been allocated.  If another has been already
 *   allocated, the one passed in is deleted and false is returned.
 *
 *   In any case the caller has released use of the ECommerceSOAPBinding
 *   and must not reference it anymore.
 *
 */
bool putECommerceSOAPBinding (ECommerceSOAPBinding *ecs);


void initECSReq(ecs__AbstractRequestType&  req,
                LONG64                     deviceId);

LONG64 now();


using namespace std;


#endif  /* __ECSCLIENTIFC_H__ */



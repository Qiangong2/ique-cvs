
#include "ecsClientIfc.h"

#include <stdio.h>
#include <string>
#include "ec.h"
#include "ecp_config.h"
#include "shr_getopt.h"
#include <wininet.h>





const char * ecsLab1        = ECP_LAB1_ECS_URL;
const char * ecsLocalhost   = ECP_LOCALHOST_URL;

const char * ecsTCPMonDef   = ECP_DEF_TCPMON_URL;

const char * ecsEndpoint    = ecsLab1;


static void usage()
{
    cout << endl;

    cout << "  ecsClientTest [options] \n\n";

    cout << "    Calls the eCommerce web service APIs for a simple sanity check.\n\n";

    cout << "    Default ecs url is lab1:   " << ecsLab1       << "\n";
    cout << "    Default ecs loclhost url:  " << ecsLocalhost  << "\n";
    cout << "    Default ecs tcpmon url:    " << ecsTCPMonDef  << "\n";

    cout << "    In following, <arg> is required, [arg] is optional.\n\n";

    cout << "       [--ecsURL     | -e] <url>    // Use specified Web service URL\n";
    cout << "       [--lab1       | -b]          // Use default lab1 Web service URL\n";
    cout << "       [--localhost  | -l]          // Use localhost URL, port 8080\n";
    cout << "       [--tcpmon     | -n] [port]   // Use localhost URL with port other than 8080,";
    cout << "                                    //     default " DEF_TCPMON_PORT "\n";
    cout << "       [--help       | -h]          // Show this help.\n\n";

    cout << endl;
}






int processCmdLine (int argc, char* argv[])
{

    static struct option long_options[] = {
        { "help",      no_argument,       0, 'h'},
        { "ecsURL",    required_argument, 0, 'e'},
        { "lab1",      no_argument,       0, 'b'},
        { "localhost", no_argument,       0, 'l'},
        { "tcpmon",    optional_argument, 0, 'n'},
        { NULL },
    };

    static string tcpmonUrl;

    int c;
    while ((c = getopt_long(argc, argv, "hble:n::",
                            long_options, NULL)) >= 0) {
        switch (c) {
        case 'h':
            usage();
            exit(0);
        case 'e':
            ecsEndpoint = optarg;
            break;
        case 'b':
            ecsEndpoint = ecsLab1;
            break;
        case 'l':
            ecsEndpoint = ecsLocalhost;
            break;
        case 'n':
            tcpmonUrl = LOCALHOST_PREFIX;
            if (optarg)
                tcpmonUrl += optarg;
            else
                tcpmonUrl += DEF_TCPMON_PORT;

            tcpmonUrl += ECS_URL_TAIL;
            ecsEndpoint = tcpmonUrl.c_str();
            break;
        default:
            usage();
            exit(1);
        }
    }

    bool more_args = (optind < argc);

    if (more_args) {
        usage();
        exit(1);
    }

    return 0;
}






int checkListTitles (LONG64 deviceId, ECommerceSOAPBinding&  ecs)
{
    unsigned i;
    int rv;

    ecs__ListTitlesRequestType  req;
    ecs__ListTitlesResponseType resp;

    initECSReq (req, deviceId);

    ecs__TitleKindType    titleKind   = ecs__TitleKindType__Games;
    ecs__PricingKindType  pricingKind = ecs__PricingKindType__Subscription;
    int                   ChannelId   = NULL;


    req.TitleKind       = &titleKind;
    req.PricingKind     = &pricingKind;
    req.ChannelId       = NULL;

    cout << "\n####  ListTitles  ####\n" << endl;
    rv = ecs.__ecs__ListTitles(&req, &resp);
    if (rv != SOAP_OK) {
        soap_print_fault (ecs.soap, stdout);
        goto end;
    }
    if (resp.ErrorCode != 0) {
        cout << "ListTitles resp.ErrorCode " << resp.ErrorCode << endl;
        rv = resp.ErrorCode;
        goto end;
    }

    for (i = 0;  i < resp.Titles.size(); ++i) {
        cout << "ListTitles[" << i << "]"
                << "  TitlesId " << resp.Titles[i]->TitleId
                << "  size " << resp.Titles[i]->TitleSize
                << "  name " << resp.Titles[i]->TitleName << endl;
    }

end:
    return rv;
}




int checkGetTitleDetails (LONG64 deviceId, ECommerceSOAPBinding&  ecs)
{
    int rv;
    size_t i;

    ecs__GetTitleDetailsRequestType req;
    ecs__GetTitleDetailsResponseType resp;

    initECSReq (req, deviceId);

    req.TitleId = "0002000100080001";

    cout << "\n####  GetTitleDetails  ####\n" << endl;
    rv = ecs.__ecs__GetTitleDetails(&req, &resp);
    if (rv != SOAP_OK) {
        soap_print_fault (ecs.soap, stdout);
        goto end;
    }
    if (resp.ErrorCode != 0) {
        cout << "GetTitleDetails resp.ErrorCode " << resp.ErrorCode << endl;
        rv = resp.ErrorCode;
        goto end;
    }

    if (!resp.TitleInfo) {
        cout << "No TitleInfo" << endl;
    } else if (!resp.TitleInfo->TitleDescription) {
        cout << "No TitleDescription" << endl;
    } else {
        cout << "resp.TitleInfo->TitleDescription: "
                <<  *resp.TitleInfo->TitleDescription  << endl;

        printf ("titleName %s\n", resp.TitleInfo->TitleName->c_str());
        printf ("description %s\n", resp.TitleInfo->TitleDescription->c_str());
        printf ("category %s\n", resp.TitleInfo->Category->c_str());
        printf ("titleId %I64u  titleKind %d titleSize %I64d  fsSize %I64d\n",
                    resp.TitleInfo->TitleId, *resp.TitleInfo->TitleKind,
                    resp.TitleInfo->TitleSize, resp.TitleInfo->FsSize);
        printf ("version 0x%08x.0x%08x\n",
                 (int)(resp.TitleInfo->Version >> 32), (int)resp.TitleInfo->Version);

        ecs__ContentInfoType * content;
        size_t nContents = resp.TitleInfo->Contents.size();

        printf ("%d contents:\n", nContents);
        for (i = 0;  i < nContents;  ++i) {
            content = resp.TitleInfo->Contents[i];
            printf ("    id %s   size %i64d\n", content->ContentId.c_str(), content->ContentSize);
        }

        printf ("%d ratings: ", resp.Ratings.size());
        for (i = 0;  i < resp.Ratings.size();  ++i) {
            printf ("Rating name %s, Rating %s\n",
                resp.Ratings[i]->Name.c_str(), resp.Ratings[i]->Rating.c_str());
        }

        printf ("%d pricings:\n", resp.TitlePricings.size());
        for (i = 0;  i < resp.TitlePricings.size();  ++i) {
            ecs__PricingType &d = *resp.TitlePricings[i];
            
            printf ("itemId %d  category %d  limits %d  amount %s  currency %s\n",
                d.ItemId, d.PricingCategory,
                d.Limits, d.Price->Amount.c_str(), d.Price->Currency.c_str());
        }
   }





end:
    return rv;
}




int checkSubscribe (LONG64 deviceId, ECommerceSOAPBinding&  ecs)
{
    int rv;

    ecs__SubscribeRequestType  req;
    ecs__SubscribeResponseType resp;

    initECSReq (req, deviceId);

    ecs__PriceType price;
    ecs__PaymentType payment;
    ecs__ECardPaymentType eCardPayment;
    ecs__TimeDurationType subscriptionLength;
    char *deviceCert = "AFakeDeviceCert1";

    req.ItemId = 5;
    req.ChannelId = "00020001";
    req.DeviceCert.__ptr = (uint8_t*) deviceCert;
    req.DeviceCert.__size = (int) strlen(deviceCert);
    req.Price = &price;
    req.Discount = NULL;
    req.Taxes = NULL;
    req.PurchaseInfo = NULL;
    req.Payment = &payment;
    req.SubscriptionLength = &subscriptionLength;

    subscriptionLength.Length = 1;
    subscriptionLength.Unit = ecs__TimeUnitType__month;

    price.Amount = "0";
    price.Currency = "EUNITS";
    payment.PaymentMethod = ecs__PaymentMethodType__ECARD;
    payment.__union_1 = SOAP_UNION_ecs__union_1_ECardPayment;
    payment.union_1.ECardPayment = &eCardPayment;
    eCardPayment.ECardNumber = "8f60adfe62";
    eCardPayment.ECardHash = "fe8c750e8421a567";
    eCardPayment.ECardType = "305ebc";


    cout << "\n####  Subscribe  ####\n" << endl;
    rv = ecs.__ecs__Subscribe (&req, &resp);
    if (rv != SOAP_OK) {
        soap_print_fault (ecs.soap, stdout);
        goto end;
    }
    if (resp.ErrorCode != 0) {
        cout << "Subscribe resp.ErrorCode " << resp.ErrorCode << ": "
             << *resp.ErrorMessage << endl;
        rv = resp.ErrorCode;
        goto end;
    }

    cout << "Subscribe returned " << (unsigned)resp.ETickets.size() << " ETickets and  "
            << (unsigned) resp.Certs.size() << " Certs  with PaymentErrCode ";

            
    cout << "  and ChannelId  ";
            
    if (resp.ChannelId) {
        cout << resp.ChannelId;
    } else {
        cout << "<none>";
    }
    cout << endl;

end:
    return rv;
}




int checkPurchaseTitle (LONG64 deviceId, ECommerceSOAPBinding&  ecs)
{
    int rv;

    ecs__PurchaseTitleRequestType  req;
    ecs__PurchaseTitleResponseType resp;

    initECSReq (req, deviceId);


    ecs__LimitType limit[2];
    limit[0].LimitKind = "TR";
    limit[0].Limits = 1000;
    limit[1].LimitKind = "DR";
    limit[1].Limits = 2000;

    req.Limits.push_back (&limit[0]);
    req.Limits.push_back (&limit[1]);

    req.TitleId = "0002000100080021";

    char *deviceCert = "AFakeDeviceCert1";
    req.DeviceCert.__ptr = (uint8_t*) deviceCert;
    req.DeviceCert.__size = (int) strlen(deviceCert);

    req.ItemId = 8;

    ecs__PriceType price;
    req.Price = &price;
    req.Discount = NULL;
    req.Taxes = NULL;
    req.PurchaseInfo = NULL;
    price.Amount = "48";
    price.Currency = "POINTS";

    ecs__PaymentType payment;
    req.Payment = &payment;
    payment.PaymentMethod = ecs__PaymentMethodType__ACCOUNT;
    payment.__union_1 = SOAP_UNION_ecs__union_1_AccountPayment;

    ecs__AccountPaymentType account;
    payment.union_1.AccountPayment = &account;

    account.AccountNumber = "123345";
    account.Pin = "67890";

    req.Account = NULL; // optional element not used

    cout << "\n####  PurchaseTitle  ####\n" << endl;
    rv = ecs.__ecs__PurchaseTitle (&req, &resp);
    if (rv != SOAP_OK) {
        soap_print_fault (ecs.soap, stdout);
        goto end;
    }
    if (resp.ErrorCode != 0) {
        cout << "PurchaseTitle resp.ErrorCode " << resp.ErrorCode << ": "
             << *resp.ErrorMessage << endl;
        rv = resp.ErrorCode;
        goto end;
    }

    cout << "PurchseTitle returned " << (unsigned)resp.ETickets.size() << " ETickets and  "
            << (unsigned) resp.Certs.size() << " Certs  with SyncTime ";

    if (resp.SyncTime) {
        cout <<  resp.SyncTime;
    } else {
        cout <<  "<none>";
    }

    cout << endl;

end:
    return rv;
}




int checkGetMeta (LONG64 deviceId, ECommerceSOAPBinding&  ecs)
{
    unsigned i;
    int rv;

    ecs__GetMetaRequestType req;
    ecs__GetMetaResponseType resp;

    initECSReq (req, deviceId);

    // has no parameters

    cout << "\n####  GetMeta  ####\n" << endl;
    rv = ecs.__ecs__GetMeta (&req, &resp);
    if (rv != SOAP_OK) {
        soap_print_fault (ecs.soap, stdout);
        goto end;
    }
    if (resp.ErrorCode != 0) {
        cout << "GetMeta resp.ErrorCode " << resp.ErrorCode << endl;
        rv = resp.ErrorCode;
        goto end;
    }

    {
        string contentPrefixURL;

        if (resp.ContentPrefixURL) {
            contentPrefixURL = *resp.ContentPrefixURL;
        }

        cout << "GetMeta returned "
                << "contentPrefixURL " << contentPrefixURL << "\n" << endl;

        vector <ecs__ContentInfoType *>& cids = resp.PreloadContents;

        unsigned numPreloadCids = (unsigned) cids.size();

        for (i  = 0;  i < numPreloadCids;  ++i) {
            cout << "Preload:  cid " << cids[i]->ContentId << "  size " << cids[i]->ContentSize << "\n";
        }

        cout << endl;
    }

end:
    return rv;
}




int checkListSubscriptionPricings (LONG64 deviceId, ECommerceSOAPBinding&  ecs)
{
    unsigned i;
    int rv;

    ecs__ListSubscriptionPricingsRequestType  req;
    ecs__ListSubscriptionPricingsResponseType resp;

    initECSReq (req, deviceId);

    // has no parameters

    cout << "\n####  ListSubscriptionPricings  ####\n" << endl;
    rv = ecs.__ecs__ListSubscriptionPricings (&req, &resp);
    if (rv != SOAP_OK) {
        soap_print_fault (ecs.soap, stdout);
        goto end;
    }
    if (resp.ErrorCode != 0) {
        cout << "ListSubscriptionPricings resp.ErrorCode " << resp.ErrorCode << endl;
        rv = resp.ErrorCode;
        goto end;
    }

    vector<ecs__SubscriptionPricingType *>& sps = resp.SubscriptionPricings;

    unsigned numSubPricings = (unsigned) sps.size();

    for (i  = 0;  i < numSubPricings;  ++i) {
        ecs__SubscriptionPricingType& sp = *sps[i];
        string channelName = sp.ChannelName ? *sp.ChannelName : "<none>";
        string channelDescription = sp.ChannelDescription ? *sp.ChannelDescription : "<none>";
        int maxCheckouts = sp.MaxCheckouts ? *sp.MaxCheckouts : 0;
        cout << "\n"
                << "ItemId " << sp.ItemId << "\n"
                << "ChannelId " << sp.ChannelId << "\n"
                << "ChannelName " << channelName << "\n"
                << "ChannelDescription " << channelDescription << "\n"
                << "Price->Amount " << sp.Price->Amount << "\n"
                << "Price->Currency " << sp.Price->Currency << "\n"
                << "MaxCheckouts " << maxCheckouts << "\n"
                << "Subscription Length " << sp.SubscriptionLength->Length << "\n"
                << "Subscription Length Units " << sp.SubscriptionLength->Unit << "\n";
    }

    cout << endl;

end:
    return rv;
}




int checkUpdateStatus (LONG64 deviceId, ECommerceSOAPBinding&  ecs)
{
    int rv;

    ecs__UpdateStatusRequestType   req;
    ecs__UpdateStatusResponseType  resp;

    initECSReq (req, deviceId);

    ecs__ConsumptionType  c1;
    ecs__ConsumptionType  c2;

    c1.TitleId = "0002000100080001";
    c1.Consumption = 3;
    req.Consumptions.push_back (&c1);

    c2.TitleId = 2;
    c2.Consumption = 5;
    req.Consumptions.push_back (&c2);

    cout << "\n####  UpdateStatus  ####\n" << endl;
    rv = ecs.__ecs__UpdateStatus (&req, &resp);
    if (rv != SOAP_OK) {
        soap_print_fault (ecs.soap, stdout);
        goto end;
    }
    if (resp.ErrorCode != 0) {
        cout << "UpdateStatus resp.ErrorCode " << resp.ErrorCode << endl;
        rv = resp.ErrorCode;
        goto end;
    }

    cout << "UpdateStatus success with DeviceId " << resp.DeviceId << endl;

end:
    return rv;
}





int main(int argc, char **argv)
{
    ECommerceSOAPBinding  *ecs;
    unsigned int device_type = 2;
    unsigned int chip_id = 1;
    LONG64       deviceId = (((LONG64)device_type) << 32) + chip_id;

    int rv = 0;

    ecs = getECommerceSOAPBinding ();

    soap_imode(ecs->soap, SOAP_IO_KEEPALIVE);
    soap_omode(ecs->soap, SOAP_IO_KEEPALIVE);

    // INTERNET_FLAG_KEEP_CONNECTION is automatic when
    // soap_imode/omode are set to SOAP_IO_KEEPALIVE.
    rv = 
    soap_register_plugin_arg( ecs->soap, wininet_plugin,
                              (void*) ECP_WININET_HttpOpenRequest_FLAGS);

    if (rv != SOAP_OK) {
        return rv;
    }

    ecs->soap->connect_timeout = 10;

    processCmdLine (argc, argv);

    ecs->endpoint = ecsEndpoint;

    cout << "\nusing  " << ecs->endpoint << "\n" << endl;

    rv |= checkPurchaseTitle (deviceId, *ecs);
#if 0
    rv |= checkListTitles (deviceId, *ecs);
    rv |= checkGetTitleDetails (deviceId, *ecs);
    rv |= checkSubscribe (deviceId, *ecs);
    rv |= checkGetMeta (deviceId, *ecs);
    rv |= checkListSubscriptionPricings (deviceId, *ecs);
    rv |= checkUpdateStatus (deviceId, *ecs);
#endif

    putECommerceSOAPBinding(ecs);

    return rv;
}


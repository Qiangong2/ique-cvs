/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#include "ecsClientIfc.h"
#include <sstream>

#define NC_DEVICE_TYPE    2

static ECommerceSOAPBinding *__ecs;

// nsmap can only be included once 
#include "ECommerceSOAPBinding.nsmap"

void initECSReq(ecs__AbstractRequestType&  req,
                LONG64                     deviceId)
{
    static unsigned int messageId;
    ostringstream o;
    o << ++messageId;

    req.Version = "1.0";
    req.MessageId = o.str();
    req.DeviceId = ((LONG64)NC_DEVICE_TYPE << 32) + ((int)deviceId);
    req.RegionId = "IQUE";
    req.CountryCode = "CN";
    req.VirtualDeviceType = NULL;
    req.Language = NULL;
    req.SerialNo = NULL;
}


LONG64 now()
{
    struct _timeb t; 
    _ftime(&t);
    return ((LONG64) t.time) * 1000 + t.millitm;
}




/*   getECommerceSOAPBinding()
 *
 *   Gets a pointer to an ECommerceSOAPBinding for use with a request.
 *   When done with the request, should return the
 *   the ECommerceSOAPBinding by calling putECommerceSOAPBinding().
 *
 */
ECommerceSOAPBinding *getECommerceSOAPBinding ()
{
    ECommerceSOAPBinding *rv = __ecs;

    if (!rv) {
        rv = new ECommerceSOAPBinding;
    }

    __ecs = NULL;

    return rv;
}



/*   putECommerceSOAPBinding()
 *
 *   Releases an ECommerceSOAPBinding for use in another request.
 *
 *   For information only, returns true if another ECommerceSOAPBinding
 *   had not already been allocated.  If another has been already
 *   allocated, the one passed in is deleted and false is returned.
 *
 *   In any case the caller has released use of the ECommerceSOAPBinding
 *   and must not reference it anymore.
 *
 */
bool putECommerceSOAPBinding (ECommerceSOAPBinding *ecs)
{
    bool rv;

    if (__ecs) {
        delete ecs;
        rv = false;
    } else {
        __ecs = ecs;
        rv = true;
    }

    return rv;
}

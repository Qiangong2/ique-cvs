/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#include "ecp_i.h"



int loadConfFile (const char *filename, HashMapString& var)
{
    size_t   pos;
    string   line;
    string   key;
    string   value;

    ifstream is (filename);
    if (!is) {
        return -1;
    }
    while(is.good()) {
        getline (is, line);
        if (!is.eof() && is.fail())
            cerr << "loadConfFile: fail reading: " << filename << endl;
        if ( line.empty() ||
               ((pos=line.find_first_not_of(" \t\n\r\v\f"))==string::npos) ||
                   line[pos]=='#') {
            continue;
        }
        istringstream is_line (line.substr (pos, string::npos));
        getline(is_line,key,'=');
        pos = key.find_last_not_of(" \t\n\r\v\f");
        if (pos!=string::npos)
            key.erase (pos+1);
        value.clear();
        getline (is_line, value);
        if (value.empty() && (line.find('=') == string::npos)) {
            value = "true";
        } else {
            pos = value.find_first_not_of(" \t\n\r\v\f");
            if (pos)
                value.erase (0, pos);
        }
        var[key] = value;
    }
    is.close();
    return 0;
}

void dumpHashMapString (HashMapString& pairs, ostream& out)
{
    HashMapString::iterator pair;
    for (pair = pairs.begin(); pair != pairs.end(); ++pair)
        out << pair->first << "=" << pair->second << endl;
}



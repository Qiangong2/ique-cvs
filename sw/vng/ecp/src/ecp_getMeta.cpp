/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#include "ecp_i.h"
#include "ecp_ecsIfc.h"


int ECPGetMetaCache::cacheECMeta ()
{
    ECPEnv   &ecp = __ecpEnv;
    int      rv = 0;

    netReq = new ECPGetMetaReq;

    if (!netReq) {
        rv = EC_ERROR_NOMEM;
        goto end;
    }

    ECPGetMetaReq *x = (ECPGetMetaReq*) netReq;

    ecs__GetSystemUpdateRequestType&  req  = x->req;
    ecs__GetSystemUpdateResponseType& resp = x->resp;

    initECSReq (req, deviceId);

    req.TitleVersion.clear();  // optional element
    req.Attribute = NULL; // set optional element to 0
    req.AuditData = NULL; // set optional element to 0

    // start thread to call service in background

    status.cachedSize = 0;
    expireTime = 0;

    if ((rv = _SHR_thread_create (&netThread, NULL,
                                    (_SHRThreadFunc) getECMetaThread, x))) {
        trace (ERR, ECWS, "_SHR_thread_create returned %d", rv);
        netThread = 0;
        rv = EC_ERROR_ECP;
        goto end;
    }

    ecp.isWebServiceActive = true;

end:
    if (rv && netReq) {
        delete netReq;
        netReq = NULL;
    }

    return rv;
}



// static member
ECError ECPGetMetaCache::getECMetaThread (ECPGetMetaReq *netReq)
{
    return __ecpEnv.ecMetaCache.accessNet (netReq);
}


ECError ECPGetMetaCache::accessNet (ECPGetMetaReq *ws)
{
    ECPEnv   &ecp = __ecpEnv;
    unsigned i;
    size_t   n;
    int      rv;
    const char *msg = "<No message provided>";
    bool dump = true;
    bool preloadTraceStart = true;

    ECommerceSOAPBinding& ecs         = *ws->ecs;
    ecs__GetSystemUpdateRequestType&  req  =  ws->req;
    ecs__GetSystemUpdateResponseType& resp =  ws->resp;

    rv = ecs.__ecs__GetSystemUpdate(&req, &resp);

    lock(ecp.mutex);

    if (netReq != ws) {
        rv = EC_ERROR_NOT_BUSY;
        goto canceled;
    }

    if (rv != SOAP_OK) {
        soap_print_fault (ecs.soap, stdout);
        fflush(stdout);
        rv = EC_ERROR_ECS_NA;
        goto end;
    }

    if (resp.ErrorCode != 0) {
        if (resp.ErrorMessage) {
            msg = resp.ErrorMessage->c_str();
        }
        trace (INFO, ECWS, "GetECMeta resp.ErrorCode %d: %s\n",
                resp.ErrorCode, msg);
        rv = EC_ECS_ERROR_RANGE_START - resp.ErrorCode;
        goto end;
    }
    if (resp.ContentPrefixURL) {
        contentPrefixURL = *resp.ContentPrefixURL;
        if (ecp.allowMetaCCS) {
            if (ecp.ccsURL == contentPrefixURL) {
                trace (INFO, ECWS, "ccs prefix same as current %s\n",
                                    ecp.ccsURL.c_str());
            } else {
                trace (INFO, ECWS, "ccs prefix overridden by GetSystemtUpdate. was %s is %s\n",
                                    ecp.ccsURL.c_str(), contentPrefixURL.c_str());
                ecp.ccsURL = contentPrefixURL;
            }
        }
    }

    if (resp.UncachedContentPrefixURL) {
        uncachedContentPrefixURL = *resp.UncachedContentPrefixURL;
        if (ecp.allowMetaCCS) {
            if (ecp.ucsURL == uncachedContentPrefixURL) {
                trace (INFO, ECWS, "ucs prefix same as current %s\n",
                                    ecp.ucsURL.c_str());
            } else {
                trace (INFO, ECWS, "ucs prefix overridden by GetSystemUpdate. was %s is %s\n",
                                    ecp.ucsURL.c_str(), uncachedContentPrefixURL.c_str());
                ecp.ucsURL = uncachedContentPrefixURL;
            }
        }
    }
    
    sysSoft.assign (resp.TitleVersion.begin(), resp.TitleVersion.end());
    
    /****  init ecMeta boot and sys_app latest version  ****/

    n = sysSoft.size();
    
    ecMeta.bootVersion = 0;
    ecMeta.sys_appVersion = 0;

    for (i = 0;  i < n;  ++i) {
        if (sysSoft[i]) {
            if (sysSoft[i]->TitleId == NC_BOOT_TITLE_ID_STR) {
                if ((u32)sysSoft[i]->Version > ecMeta.bootVersion) {
                    ecMeta.bootVersion = sysSoft[i]->Version;
                }
            } else if (sysSoft[i]->TitleId == NC_SYS_APP_TITLE_ID_STR) {
                if ((u32)sysSoft[i]->Version > ecMeta.sys_appVersion) {
                    ecMeta.sys_appVersion = sysSoft[i]->Version;
                }
            }
        }
    }
    
    status.totalSize = status.cachedSize = sizeof ecMeta;
    cachedTime = now();
    expireTime = cachedTime + cacheDur;

    if (dump) {
        ECMeta& m = ecMeta;

        trace (INFO, ECWS, "EC_GetECMeta\n");

        trace (INFO, ECWS, "ecMeta.bootVersion %u\n", ecMeta.bootVersion);

        trace (INFO, ECWS, "ecMets.sys_appVersion %u\n", ecMeta.sys_appVersion);
    }
end:

    if (rv < 0) {
        status.cachedSize = rv;
    }

    if (netReq == ws) {
        netReq = NULL;
    }
    ecp.isWebServiceActive = false;

canceled:
    delete ws;
    unlock(ecp.mutex);

    return 0; // doesn't go anywhere
}






// There is no Id (i.e. filter) for cached meta data as there is only
// one type of meta data and it is either cached or not.
// So the CCId for meta data is only to match the 
// general scheme of an ECPCache descendant
//
ECError ECPGetMetaCache::getStat(ECPMetaCacheId id, ECCacheStatus& stat)
{
    if (netThread && !netReq) {
        ecpCloseThreadHandle (netThread);
        netThread = 0;
    }

    if (status.cachedSize != EC_ERROR_NOT_CACHED
            && (expireTime == 0 || now() < expireTime)) {
        stat = status;
    } else {
        stat.cachedSize = EC_ERROR_NOT_CACHED;
        stat.totalSize = EC_ERROR_INVALID;
    }

    return EC_ERROR_OK;
}

ECError ECPGetMetaCache::cache(ECPMetaCacheId id)
{
    // delete current cache
    // get ec meta data from web service
    // Allow cache, refresh, unlink even if open,
    // but don't refresh on cache request if already cached

    ECError   rv = EC_ERROR_OK;

    ECCacheStatus  stat;

    if (!(rv = getStat (id, stat))) {
        // it is either all there, or not cached
        if (stat.cachedSize < 0 || stat.cachedSize != stat.totalSize) {

            unlink(id);
            rv = cacheECMeta();
        }
    }

    return rv;
}


ECError ECPGetMetaCache::open    (ECPMetaCacheId id)
{
    ECError        rv;
    ECCacheStatus  stat;

    if (!(rv = getStat (id, stat))) {
        // it is either all there, or not cached
        if (stat.cachedSize < 0) {
            rv = EC_ERROR_NOT_CACHED;
        }
        else {
            isOpen = true;
        }
    }

    return rv;
}


ECError ECPGetMetaCache::read    (ECPMetaCacheId id, void *ret, size_t *retLen)
{
    if (!isOpen) {

        return EC_ERROR_NOT_OPEN;
    }

    if (status.cachedSize < 0) {
        return EC_ERROR_NOT_CACHED;
    }

    /* Normally the caching is just so the client doesn't need to wait
     * for the net operation.  Since the client provides a buffer for the
     * whole thing, just send the whole thing.
     */
    memcpy (ret, &ecMeta, sizeof ecMeta);

    *retLen = sizeof ecMeta;

    return EC_ERROR_OK;
}


ECError ECPGetMetaCache::seek    (ECPMetaCacheId id)
{
    // This is never called in actual practice

    return EC_ERROR_NOT_SUPPORTED;
}


ECError ECPGetMetaCache::close   (ECPMetaCacheId id)
{
    if (!isOpen) {
        return EC_ERROR_NOT_OPEN;
    }

    isOpen = false;

    if (expireOnClose) {
        expireTime = 1;
    }

    return EC_ERROR_OK;
}



ECError ECPGetMetaCache::unlink  (ECPMetaCacheId id)
{
    // This is never called from the NC in actual practice

    close(id);

    status.cachedSize = EC_ERROR_NOT_CACHED;
    status.totalSize = EC_ERROR_INVALID;
    isOpen = false;
    memset (&ecMeta, 0, sizeof ecMeta);

    return EC_ERROR_OK;
}






int32_t  ecpECMeta_GetStat (VN         *vn,
                            VNMsgHdr   *hdr,
                            const void *arg,
                            size_t      argLen,
                            void       *ret,
                            size_t     *retLen)
{
    ECError   rv;
    ECPEnv   &ecp = __ecpEnv;

    if (argLen || !ret || !retLen) {
        rv = EC_ERROR_ECP;
        goto end;
    }

    rv = ecp.ecMetaCache.getStat (arg, *(ECCacheStatus*) ret);

    if (!rv) {
        *retLen = sizeof(ECCacheStatus);
    }

end:
    return rv;
}


/*
 *  if !cached
 *      get from ecs web service
 *  return 0
 */
int32_t  ecpECMeta_Cache (VN         *vn,
                          VNMsgHdr   *hdr,
                          const void *arg,
                          size_t      argLen,
                          void       *ret,
                          size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;
    VNGErrCode ec;

    if (ecp.isWebServiceActive) {
        rv = EC_ERROR_BUSY;
    }
    else if (argLen) {
        rv = EC_ERROR_ECP;
    }
    else if ((ec = VN_GetMemberDeviceId(vn, hdr->sender,
                                          &ecp.ecMetaCache.deviceId))) {
        trace (ERR, ECWS,
               "ecMeta VN_GetMemberDeviceId returned error %d\n", ec);
        rv = EC_ERROR_ECP;
    }
    else {
        rv = ecp.ecMetaCache.cache(arg);
    }

    return rv;
}

int32_t  ecpECMeta_Open (VN         *vn,
                         VNMsgHdr   *hdr,
                         const void *arg,
                         size_t      argLen,
                         void       *ret,
                         size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (argLen) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.ecMetaCache.open (arg);
    }

    return rv;
}

int32_t  ecpECMeta_Read (VN         *vn,
                             VNMsgHdr   *hdr,
                             const void *arg,
                             size_t      argLen,
                             void       *ret,
                             size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (!ret || !retLen || argLen) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.ecMetaCache.read (arg, ret, retLen);
    }

    return rv;
}

int32_t  ecpECMeta_Seek (VN         *vn,
                         VNMsgHdr   *hdr,
                         const void *arg,
                         size_t      argLen,
                         void       *ret,
                         size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (argLen) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.ecMetaCache.seek (arg);
    }

    return rv;
}

int32_t  ecpECMeta_Close (VN         *vn,
                          VNMsgHdr   *hdr,
                          const void *arg,
                          size_t      argLen,
                          void       *ret,
                          size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (argLen) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.ecMetaCache.close (arg);
    }

    return rv;
}

int32_t  ecpECMeta_Refresh (VN         *vn,
                            VNMsgHdr   *hdr,
                            const void *arg,
                            size_t      argLen,
                            void       *ret,
                            size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (argLen) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.ecMetaCache.refresh (arg, arg);
    }

    return rv;
}


int32_t  ecpECMeta_Unlink (VN         *vn,
                           VNMsgHdr   *hdr,
                           const void *arg,
                           size_t      argLen,
                           void       *ret,
                           size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (argLen) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.ecMetaCache.unlink (arg);
    }

    return rv;
}


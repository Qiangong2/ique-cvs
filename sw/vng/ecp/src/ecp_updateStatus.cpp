/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#include "ecp_i.h"
#include "ecp_ecsIfc.h"
#include "common.h"





int ECPUpdateStatusCache::cacheUpdateStatus ()
{
    ECPEnv   &ecp = __ecpEnv;
    int      rv = 0;

    netReq = new ECPUpdateStatusReq;

    if (!netReq) {
        rv = EC_ERROR_NOMEM;
        goto end;
    }

    ECPUpdateStatusReq *x = (ECPUpdateStatusReq*) netReq;

    ecs__UpdateStatusRequestType&  req  = x->req;
    ecs__UpdateStatusResponseType& resp = x->resp;

    initECSReq (req, deviceId);

    for (size_t i =0;  i < update.nConsumptions;  ++i) {
        x->consumptions[i].TitleId = tohexstring(update.consumptions[i].titleId, 16);
        x->consumptions[i].Consumption = update.consumptions[i].consumption;
        req.Consumptions.push_back (&x->consumptions[i]);
    }
    
    // start thread to call service in background

    status.cachedSize = 0;
    expireTime = 0;

    if ((rv = _SHR_thread_create (&netThread, NULL,
                                    (_SHRThreadFunc) updateStatusThread, x))) {
        trace (ERR, ECWS, "_SHR_thread_create returned %d", rv);
        netThread = 0;
        rv = EC_ERROR_ECP;
        goto end;
    }

    ecp.isWebServiceActive = true;

end:
    if (rv && netReq) {
        delete netReq;
        netReq = NULL;
    }

    return rv;
}



// static member
ECError ECPUpdateStatusCache::updateStatusThread (ECPUpdateStatusReq *netReq)
{
    return __ecpEnv.updateStatusCache.accessNet (netReq);
}


ECError ECPUpdateStatusCache::accessNet (ECPUpdateStatusReq *ws)
{
    ECPEnv   &ecp = __ecpEnv;
    int      rv;
    const char *msg = "<No message provided>";
    string res;
    string tmp;

    ECommerceSOAPBinding& ecs         = *ws->ecs;
    ecs__UpdateStatusRequestType&  req  =  ws->req;
    ecs__UpdateStatusResponseType& resp =  ws->resp;

    rv = ecs.__ecs__UpdateStatus(&req, &resp);

    lock(ecp.mutex);

    if (netReq != ws) {
        rv = EC_ERROR_NOT_BUSY;
        goto canceled;
    }

    if (rv != SOAP_OK) {
        soap_print_fault (ecs.soap, stdout);
        fflush(stdout);
        rv = EC_ERROR_ECS_NA;
        goto end;
    }

    if (resp.ErrorCode != 0) {
        if (resp.ErrorMessage) {
            msg = resp.ErrorMessage->c_str();
        }
        trace (INFO, ECWS, "Subscribe resp.ErrorCode %d: %s\n",
                resp.ErrorCode, msg);
        rv = EC_ECS_ERROR_RANGE_START - resp.ErrorCode;
        goto end;
    }
    
    // There is no retuned info other than ecs__AbstractResponseType
    
    status.totalSize = status.cachedSize = 0;
    cachedTime = now();
    expireTime = cachedTime + cacheDur;

end:

    if (rv < 0) {
        status.cachedSize = rv;
    }

    if (netReq == ws) {
        netReq = NULL;
    }
    ecp.isWebServiceActive = false;

canceled:
    delete ws;
    unlock(ecp.mutex);

    return 0; // doesn't go anywhere
}






ECError ECPUpdateStatusCache::getStat(ECPUpdateStatusArg& id, ECCacheStatus& stat)
{
    if (netThread && !netReq) {
        ecpCloseThreadHandle (netThread);
        netThread = 0;
    }

    if (status.cachedSize != EC_ERROR_NOT_CACHED
            && id.nConsumptions == update.nConsumptions
            && (expireTime == 0 || now() < expireTime)) {
        stat = status;
    } else {
        stat.cachedSize = EC_ERROR_NOT_CACHED;
        stat.totalSize = EC_ERROR_INVALID;
    }

    return EC_ERROR_OK;
}

ECError ECPUpdateStatusCache::cache(ECPUpdateStatusArg& id)
{
    // delete current cached update status response
    //  (not really anything to delete)
    // send updated status to web service
    // cache result for client to retrieve
    //  (only result is success or fail. no data returned)

    ECError   rv = EC_ERROR_OK;

    unlink(id);
    update = id;
    rv = cacheUpdateStatus();
        
    return rv;
}

// There is no return data, so no data cached
// but open, seek, read, close must be defined for ECPCache descendant
ECError ECPUpdateStatusCache::open    (ECPUpdateStatusArg& id)
{
    ECError        rv;
    ECCacheStatus  stat;

    if (!(rv = getStat (id, stat))) {
        // it is either all there, or not cached
        if (stat.cachedSize < 0) {
            rv = EC_ERROR_NOT_CACHED;
        }
        else {
            isOpen = true;
        }
    }

    return rv;
}



// There is no return data, so no data cached
// but open, seek, read, close must be defined for ECPCache descendant
ECError ECPUpdateStatusCache::read    (ECPUpdateStatusArg& id, void *ret, size_t *retLen)
{
    if (!isOpen) {
        return EC_ERROR_NOT_OPEN;
    }

    if (status.cachedSize < 0) {
        return EC_ERROR_NOT_CACHED;
    }
    
    *retLen = 0;
    return EC_ERROR_OK;
}



// There is no return data, so no data cached
// but open, read, close must be defined for ECPCache descendant
ECError ECPUpdateStatusCache::seek    (ECPUpdateStatusArg& id)
{
    // This is never called in actual practice
    
    return EC_ERROR_NOT_SUPPORTED;
}



// There is no return data, so no data cached
// but open, seek, read, close must be defined for ECPCache descendant
ECError ECPUpdateStatusCache::close   (ECPUpdateStatusArg& id)
{
    if (!isOpen) {
        return EC_ERROR_NOT_OPEN;
    }

    isOpen = false;

    if (expireOnClose) {
        expireTime = 1;
    }

    return EC_ERROR_OK;
}



ECError ECPUpdateStatusCache::unlink  (ECPUpdateStatusArg& id)
{
    // This is never called from the NC in actual practice

    close(id);

    status.cachedSize = EC_ERROR_NOT_CACHED;
    status.totalSize = EC_ERROR_INVALID;
    isOpen = false;

    return EC_ERROR_OK;
}





int32_t  ecpUpdateStatus_GetStat (VN         *vn,
                                  VNMsgHdr   *hdr,
                                  const void *arg,
                                  size_t      argLen,
                                  void       *ret,
                                  size_t     *retLen)
{
    ECError   rv;
    ECPEnv   &ecp = __ecpEnv;
    ECPUpdateStatusArg* a= (ECPUpdateStatusArg*) arg;

    if (!arg || !ret || !retLen || argLen != sizeof(a->nConsumptions)) {
        rv = EC_ERROR_ECP;
        goto end;
    }

    rv = ecp.updateStatusCache.getStat (*a, *(ECCacheStatus*) ret);

    if (!rv) {
        *retLen = sizeof(ECCacheStatus);
    }

end:
    return rv;
}


/*
 *  update via ecs web service and cache response for
 *  retrieval by client
 */
int32_t  ecpUpdateStatus_Cache (VN         *vn,
                                VNMsgHdr   *hdr,
                                const void *arg,
                                size_t      argLen,
                                void       *ret,
                                size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;
    VNGErrCode ec;
    ECPUpdateStatusArg* a= (ECPUpdateStatusArg*) arg;

    if (ecp.isWebServiceActive) {
        rv = EC_ERROR_BUSY;
    }
    else if (!arg || argLen != (sizeof(a->nConsumptions)
                             + a->nConsumptions * sizeof(ECConsumption))) {
        rv = EC_ERROR_ECP;
    }
    else if ((ec = VN_GetMemberDeviceId(vn, hdr->sender,
                                          &ecp.updateStatusCache.deviceId))) {
        trace (ERR, ECWS,
               "list titles VN_GetMemberDeviceId returned error %d\n", ec);
        rv = EC_ERROR_ECP;
    }
    else {
        rv = ecp.updateStatusCache.cache(*a);
    }

    return rv;
}

int32_t  ecpUpdateStatus_Open (VN         *vn,
                               VNMsgHdr   *hdr,
                               const void *arg,
                               size_t      argLen,
                               void       *ret,
                               size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (!arg || argLen != sizeof(ECPUpdateStatusArg)) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.updateStatusCache.open (*(ECPUpdateStatusArg*) arg);
    }

    return rv;
}

int32_t  ecpUpdateStatus_Read (VN         *vn,
                               VNMsgHdr   *hdr,
                               const void *arg,
                               size_t      argLen,
                               void       *ret,
                               size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (!ret || !retLen || !arg || argLen != sizeof(ECPUpdateStatusArg)) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.updateStatusCache.read (*(ECPUpdateStatusArg*)arg, ret, retLen);
    }

    return rv;
}

int32_t  ecpUpdateStatus_Seek (VN         *vn,
                               VNMsgHdr   *hdr,
                               const void *arg,
                               size_t      argLen,
                               void       *ret,
                               size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (!arg || argLen != sizeof(ECPUpdateStatusArg)) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.updateStatusCache.seek (*(ECPUpdateStatusArg*) arg);
    }

    return rv;
}

int32_t  ecpUpdateStatus_Close (VN         *vn,
                                VNMsgHdr   *hdr,
                                const void *arg,
                                size_t      argLen,
                                void       *ret,
                                size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (!arg || argLen != sizeof(ECPUpdateStatusArg)) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.updateStatusCache.close (*(ECPUpdateStatusArg*) arg);
    }

    return rv;
}

int32_t  ecpUpdateStatus_Refresh (VN         *vn,
                                  VNMsgHdr   *hdr,
                                  const void *arg,
                                  size_t      argLen,
                                  void       *ret,
                                  size_t     *retLen)
{
    return EC_ERROR_NOT_SUPPORTED;
}

int32_t  ecpUpdateStatus_Unlink (VN         *vn,
                                 VNMsgHdr   *hdr,
                                 const void *arg,
                                 size_t      argLen,
                                 void       *ret,
                                 size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (!arg || argLen != sizeof(ECPUpdateStatusArg)) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.updateStatusCache.unlink (*(ECPUpdateStatusArg*) arg);
    }

    return rv;
}

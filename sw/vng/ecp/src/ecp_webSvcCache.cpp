/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */




#include "ecp_i.h"
#include <wininet.h>



BgWebSvcThArg::BgWebSvcThArg ()
{
    ECPEnv&  ecp = __ecpEnv;
    int      gsrv = SOAP_OK; // gsoap rv

    ecs = getECommerceSOAPBinding ();

    if (!ecs) {
        status = EC_ERROR_NET_NA;
        goto end;
    }

    soap_imode(ecs->soap, SOAP_IO_KEEPALIVE);
    soap_omode(ecs->soap, SOAP_IO_KEEPALIVE);

    // INTERNET_FLAG_KEEP_CONNECTION is automatic when
    // soap_imode/omode are set to SOAP_IO_KEEPALIVE.
    gsrv = soap_register_plugin_arg( ecs->soap, wininet_plugin,
                            (void*) ECP_WININET_HttpOpenRequest_FLAGS);
    if (gsrv != SOAP_OK) {
        status = EC_ERROR_NET_NA;
        putECommerceSOAPBinding (ecs);
        goto end;
    }
    ecs->soap->connect_timeout = 60; // 60 seconds
    ecs->soap->send_timeout = 20;    // 20 seconds
    ecs->soap->recv_timeout = 60;    // 60 seconds 
    ecs->endpoint = ecp.endpoint;

    status = EC_ERROR_OK;

end:
    return;
}



BgWebSvcThArg::~BgWebSvcThArg () {
    if (ecs) {
        putECommerceSOAPBinding (ecs);
    }
}



// virtual func redefinition


template <typename CCId,
          typename CCIdCache,
          typename CCIdCount,
          typename CCIdList>
ECError
BgWebSvcCache  <CCId,
                CCIdCache,
                CCIdCount,
                CCIdList> :: cancel  (CCIdList *id)
{
    // Default is to ignore id and cancel any op
    // If redefined, id == NULL means cancel any

    ECPEnv&  ecp = __ecpEnv;

    if (netReq) {
        ecp.isWebServiceActive = false;
        netReq = NULL; // indicate to bg thread that op was canceled
        status.cachedSize = EC_ERROR_NOT_CACHED;
        status.totalSize  = EC_ERROR_INVALID;
    }

    return EC_ERROR_OK;
}


// Get unresolved references if I don't do explicit instantiation

template BgWebSvcCache<ECPListTitlesArg&,
                       ECPListTitlesArg&,
                       ECPListTitlesArg&,
                       ECPListTitlesArg> :: cancel (ECPListTitlesArg *id);

template BgWebSvcCache<ECPTitleDetailsArg,
                       ECPTitleDetailsArg,
                       ECPTitleDetailsArg,
                       ECPTitleDetailsArg> :: cancel (ECPTitleDetailsArg *id);

template BgWebSvcCache<ECPMetaCacheId,
                       ECPMetaCacheId,
                       ECPMetaCacheId,
                       ECPMetaCacheId> :: cancel (ECPMetaCacheId *id);

template BgWebSvcCache<ECPListSubPricingsId,
                       ECPListSubPricingsId,
                       ECPListSubPricingsId,
                       ECPListSubPricingsId> :: cancel (ECPListSubPricingsId *id);

template BgWebSvcCache<ECPSubscribeArg&,
                       ECPSubscribeArg&,
                       ECPSubscribeArg&,
                       ECPSubscribeArg> :: cancel (ECPSubscribeArg *id);

template BgWebSvcCache<ECPRedeemArg&,
                       ECPRedeemArg&,
                       ECPRedeemArg&,
                       ECPRedeemArg> :: cancel (ECPRedeemArg *id);

template BgWebSvcCache<ECPPurchaseTitleArg&,
                       ECPPurchaseTitleArg&,
                       ECPPurchaseTitleArg&,
                       ECPPurchaseTitleArg> :: cancel (ECPPurchaseTitleArg *id);

template BgWebSvcCache<ECPSyncTicketsArg&,
                       ECPSyncTicketsArg&,
                       ECPSyncTicketsArg&,
                       ECPSyncTicketsArg> :: cancel (ECPSyncTicketsArg *id);

template BgWebSvcCache<ECPUpdateStatusArg&,
                       ECPUpdateStatusArg&,
                       ECPUpdateStatusArg&,
                       ECPUpdateStatusArg> :: cancel (ECPUpdateStatusArg *id);



int32_t  ecpCancelWebSvcOps (VN         *vn,
                             VNMsgHdr   *hdr,
                             const void *arg,
                             size_t      argLen,
                             void       *ret,
                             size_t     *retLen)
{
    // This should only be called if an operation does not compltete
    // Reset to a state where operation can be started.
    // Cancel any active op to make sure client can start another.

    ECPEnv&    ecp = __ecpEnv;

    ecp.ecMetaCache.cancel ((ECPMetaCacheId*) NULL);
    ecp.listTitlesCache.cancel ((ECPListTitlesArg*) NULL);
    ecp.subPricingsCache.cancel ((ECPListSubPricingsId*) NULL);
    ecp.updateStatusCache.cancel ((ECPUpdateStatusArg*) NULL);
    ecp.titleDetailsCache.cancel ((ECPTitleDetailsArg*) NULL);
    ecp.subscribeCache.cancel ((ECPSubscribeArg*) NULL);
    ecp.redeemCache.cancel ((ECPRedeemArg*) NULL);
    ecp.syncTicketsCache.cancel ((ECPSyncTicketsArg*) NULL);

    ecp.isWebServiceActive = false;

    return EC_ERROR_OK;
}




/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#include "ecp_i.h"
#include "ecp_ecsIfc.h"


int ECPListSubPricingsCache::cacheSubPricings ()
{
    ECPEnv   &ecp = __ecpEnv;
    int      rv = 0;

    netReq = new ECPListSubPricingsReq;

    if (!netReq) {
        rv = EC_ERROR_NOMEM;
        goto end;
    }

    ECPListSubPricingsReq *x = (ECPListSubPricingsReq*) netReq;

    ecs__ListSubscriptionPricingsRequestType&  req  = x->req;
    ecs__ListSubscriptionPricingsResponseType& resp = x->resp;

    initECSReq (req, deviceId);

    // start thread to call service in background

    status.cachedSize = 0;
    expireTime = 0;

    if ((rv = _SHR_thread_create (&netThread, NULL,
                                    (_SHRThreadFunc) subPricingsThread, x))) {
        trace (ERR, ECWS, "_SHR_thread_create returned %d", rv);
        netThread = 0;
        rv = EC_ERROR_ECP;
        goto end;
    }

    ecp.isWebServiceActive = true;


end:
    if (rv && netReq) {
        delete netReq;
        netReq = NULL;
    }

    return rv;
}



// static member
ECError ECPListSubPricingsCache::subPricingsThread (ECPListSubPricingsReq *netReq)
{
    return __ecpEnv.subPricingsCache.accessNet (netReq);
}


ECError ECPListSubPricingsCache::accessNet (ECPListSubPricingsReq *ws)
{
    ECPEnv   &ecp = __ecpEnv;
    unsigned i;
    size_t   n;
    int      rv;
    const char *msg = "<No message provided>";

    ECommerceSOAPBinding& ecs = *ws->ecs;
    ecs__ListSubscriptionPricingsRequestType&  req  =  ws->req;
    ecs__ListSubscriptionPricingsResponseType& resp =  ws->resp;

    rv = ecs.__ecs__ListSubscriptionPricings(&req, &resp);

    lock(ecp.mutex);

    if (netReq != ws) {
        rv = EC_ERROR_NOT_BUSY;
        goto canceled;
    }

    if (rv != SOAP_OK) {
        soap_print_fault (ecs.soap, stdout);
        fflush(stdout);
        rv = EC_ERROR_ECS_NA;
        goto end;
    }

    if (resp.ErrorCode != 0) {
        if (resp.ErrorMessage) {
            msg = resp.ErrorMessage->c_str();
        }
        trace (INFO, ECWS, "ListSubscriptionPricings resp.ErrorCode %d: %s\n",
                resp.ErrorCode, msg);
        rv = EC_ECS_ERROR_RANGE_START - resp.ErrorCode;
        goto end;
    }
    
    

    vector<ecs__SubscriptionPricingType *>& sps = resp.SubscriptionPricings;

    unsigned numSubPricings = (unsigned) sps.size();

    bool dump = true;

    for (i  = 0;  i < numSubPricings;  ++i) {
        if (!sps[i]) {
            continue;
        }
        ECSubscriptionPricing esp;
        ecs__SubscriptionPricingType& sp = *sps[i];
        if (dump) {
            string none = "<none>";
            string channelName = sp.ChannelName ? *sp.ChannelName : none;
            string channelDescription = sp.ChannelDescription ? *sp.ChannelDescription : none;
            string priceAmount = sp.Price ? sp.Price->Amount : none;
            string priceCurrency = sp.Price ? sp.Price->Currency : none;
            int length = sp.SubscriptionLength ? sp.SubscriptionLength->Length : 0;
            int unit = sp.SubscriptionLength ? sp.SubscriptionLength->Unit : -1;
            string unitName = "<unknown>";
            if (unit == ecs__TimeUnitType__day) unitName = "day";
            else if (unit == ecs__TimeUnitType__month) unitName = "month";
            else if (unit == -1) unitName = none;
            int maxCheckouts = sp.MaxCheckouts ? *sp.MaxCheckouts : -1;
            trace (INFO, ECWS,  "\n\n"
                                "ItemId %d\n"
                                "ChannelId %s\n"
                                "ChannelName %s\n"
                                "ChannelDescription %s\n"
                                "Price->Amount %s\n"
                                "Price->Currency %s\n"
                                "MaxCheckouts %d\n"
                                "Subscription Length %d\n"
                                "Subscription Length Unit %d - %s\n\n",
                sp.ItemId,  sp.ChannelId.c_str(),
                channelName.c_str(), channelDescription.c_str(),
                priceAmount.c_str(), priceCurrency.c_str(),
                maxCheckouts, length, unit, unitName.c_str());                              
        }
        
        esp.itemId = sp.ItemId;
        esp.channelId = strtoul(sp.ChannelId.c_str(), NULL, 16);

        n = 0;
        if (sp.ChannelName) {
            n = sp.ChannelName->copy (esp.channelName, sizeof esp.channelName - 1);
        }
        esp.channelName[n] = 0;

        n = 0;
        if (sp.ChannelDescription) {
            n = sp.ChannelDescription->copy (esp.channelDescription,
                                             sizeof esp.channelDescription - 1);
        }
        esp.channelDescription[n] = 0;

        if (sp.SubscriptionLength) {
            esp.subscriptionLength = sp.SubscriptionLength->Length;
            esp.subscriptionTimeUnit = soap2ecTimeUnit (sp.SubscriptionLength->Unit);
        } else {
            trace (ERR, ECWS,
                "itemId %d ecs__SubscriptionPricingType has no SubscriptionLength\n",
                 sp.ItemId);
            rv = EC_ERROR_ECP;
            goto end;
        }

        if (sp.Price) {
            esp.price.amount = strtoul (sp.Price->Amount.c_str(), NULL, 0);
            n = sp.Price->Currency.copy (esp.price.currency,
                                        sizeof esp.price.currency - 1);
            esp.price.currency[n] = 0;
        } else {
            trace (ERR, ECWS,
                "itemId %d ecs__SubscriptionPricingType has no Price\n", sp.ItemId);
            rv = EC_ERROR_ECP;
            goto end;
        }

        if (sp.MaxCheckouts) {
            esp.maxCheckouts = *sp.MaxCheckouts;
        } else {
            esp.maxCheckouts = -1;
        }

        pricings.push_back (esp);
    }

    status.cachedSize = (s32) (pricings.size() * sizeof pricings[0]);
    status.totalSize = status.cachedSize;

    cachedTime = now();
    expireTime = cachedTime + cacheDur;

end:

    if (rv < 0) {
        status.cachedSize = rv;
        pricings.clear();
    }

    if (netReq == ws) {
        netReq = NULL;
    }
    ecp.isWebServiceActive = false;

canceled:
    delete ws;
    unlock(ecp.mutex);

    return 0; // doesn't go anywhere
}






// There is no Id (i.e. filter) for subscription pricings.
// It is either cached or not.
// So the CCId for meta data is only to match the 
// general scheme of an ECPCache descendant
//
ECError ECPListSubPricingsCache::getStat(ECPListSubPricingsId id, ECCacheStatus& stat)
{
    if (netThread && !netReq) {
        ecpCloseThreadHandle (netThread);
        netThread = 0;
    }

    if (status.cachedSize != EC_ERROR_NOT_CACHED
            && (expireTime == 0 || now() < expireTime)) {
        stat = status;
    } else {
        stat.cachedSize = EC_ERROR_NOT_CACHED;
        stat.totalSize = EC_ERROR_INVALID;
    }

    return EC_ERROR_OK;
}

ECError ECPListSubPricingsCache::cache(ECPListSubPricingsId id)
{
    // delete current cache
    // get subscription pricings from web service
    // Allow cache, refresh, unlink even if open,
    // but don't refresh on cache request if already cached

    ECError   rv = EC_ERROR_OK;

    ECCacheStatus  stat;

    if (!(rv = getStat (id, stat))) {
        // it is either all there, or not cached
        if (stat.cachedSize < 0 || stat.cachedSize != stat.totalSize) {

            unlink(id);
            rv = cacheSubPricings();
        }
    }

    return rv;
}


ECError ECPListSubPricingsCache::open    (ECPListSubPricingsId id)
{
    ECError        rv;
    ECCacheStatus  stat;

    if (!(rv = getStat (id, stat))) {
        // it is either all there, or not cached
        if (stat.cachedSize < 0) {
            rv = EC_ERROR_NOT_CACHED;
        }
        else {
            isOpen = true;
        }
    }

    return rv;
}


ECError ECPListSubPricingsCache::read    (ECPListSubPricingsId id, void *ret, size_t *retLen)
{
    ECError rv = EC_ERROR_OK;
    
    if (!isOpen) {

        return EC_ERROR_NOT_OPEN;
    }

    if (status.cachedSize < 0) {
        return EC_ERROR_NOT_CACHED;
    }

    /* Normally the caching is just so the client doesn't need to wait
     * for the net operation.  Since the client provides a buffer for the
     * whole thing, just send the whole thing.
     *
     * This assumes that the max number of subscription pricings will
     * fit in one packet.  If that becomes not true, revise this to be
     * like list titles (which can return the info with multiple reads).
     */
     
    size_t infoLen = sizeof(ECSubscriptionPricing);
    size_t nCached = pricings.size();
    size_t maxRet  = VN_MAX_MSG_LEN / infoLen;
    size_t nRet;
    uint8_t *r = (uint8_t*) ret;

    if (nCached > maxRet) {
        nRet = maxRet;
        rv = EC_ERROR_OVERFLOW;
    } else {
        nRet = nCached;
    }

    for (size_t i = 0;  i < nRet;  ++i) {
        memcpy (r, &pricings[i], infoLen);
        r += infoLen;
    }

    *retLen = nRet * infoLen;

    return rv;
}


ECError ECPListSubPricingsCache::seek    (ECPListSubPricingsId id)
{
    // This is never called in actual practice

    return EC_ERROR_NOT_SUPPORTED;
}


ECError ECPListSubPricingsCache::close   (ECPListSubPricingsId id)
{
    if (!isOpen) {
        return EC_ERROR_NOT_OPEN;
    }

    isOpen = false;

    if (expireOnClose) {
        expireTime = 1;
    }

    return EC_ERROR_OK;
}



ECError ECPListSubPricingsCache::unlink  (ECPListSubPricingsId id)
{
    // This is never called from the NC in actual practice

    close(id);

    pricings.clear();
    status.cachedSize = EC_ERROR_NOT_CACHED;
    status.totalSize = EC_ERROR_INVALID;
    isOpen = false;

    return EC_ERROR_OK;
}






int32_t  ecpListSubPricings_GetStat (VN         *vn,
                                     VNMsgHdr   *hdr,
                                     const void *arg,
                                     size_t      argLen,
                                     void       *ret,
                                     size_t     *retLen)
{
    ECError   rv;
    ECPEnv   &ecp = __ecpEnv;

    if (argLen || !ret || !retLen) {
        rv = EC_ERROR_ECP;
        goto end;
    }

    rv = ecp.subPricingsCache.getStat (arg, *(ECCacheStatus*) ret);

    if (!rv) {
        *retLen = sizeof(ECCacheStatus);
    }

end:
    return rv;
}


/*
 *  if !cached
 *      get from ecs web service
 *  return 0
 */
int32_t  ecpListSubPricings_Cache (VN         *vn,
                                   VNMsgHdr   *hdr,
                                   const void *arg,
                                   size_t      argLen,
                                   void       *ret,
                                   size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;
    VNGErrCode ec;

    if (ecp.isWebServiceActive) {
        rv = EC_ERROR_BUSY;
    }
    else if (argLen) {
        rv = EC_ERROR_ECP;
    }
    else if ((ec = VN_GetMemberDeviceId(vn, hdr->sender,
                                          &ecp.subPricingsCache.deviceId))) {
        trace (ERR, ECWS,
               "list sub pricings VN_GetMemberDeviceId returned error %d\n", ec);
        rv = EC_ERROR_ECP;
    }
    else {
        rv = ecp.subPricingsCache.cache(arg);
    }

    return rv;
}

int32_t  ecpListSubPricings_Open (VN         *vn,
                                  VNMsgHdr   *hdr,
                                  const void *arg,
                                  size_t      argLen,
                                  void       *ret,
                                  size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (argLen) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.subPricingsCache.open (arg);
    }

    return rv;
}

int32_t  ecpListSubPricings_Read (VN         *vn,
                                      VNMsgHdr   *hdr,
                                      const void *arg,
                                      size_t      argLen,
                                      void       *ret,
                                      size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (!ret || !retLen || argLen) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.subPricingsCache.read (arg, ret, retLen);
    }

    return rv;
}

int32_t  ecpListSubPricings_Seek (VN         *vn,
                                  VNMsgHdr   *hdr,
                                  const void *arg,
                                  size_t      argLen,
                                  void       *ret,
                                  size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (argLen) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.subPricingsCache.seek (arg);
    }

    return rv;
}

int32_t  ecpListSubPricings_Close (VN         *vn,
                                   VNMsgHdr   *hdr,
                                   const void *arg,
                                   size_t      argLen,
                                   void       *ret,
                                   size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (argLen) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.subPricingsCache.close (arg);
    }

    return rv;
}

int32_t  ecpListSubPricings_Refresh (VN         *vn,
                                     VNMsgHdr   *hdr,
                                     const void *arg,
                                     size_t      argLen,
                                     void       *ret,
                                     size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (argLen) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.subPricingsCache.refresh (arg, arg);
    }

    return rv;
}


int32_t  ecpListSubPricings_Unlink (VN         *vn,
                                    VNMsgHdr   *hdr,
                                    const void *arg,
                                    size_t      argLen,
                                    void       *ret,
                                    size_t     *retLen)
{
    ECError    rv;
    ECPEnv&    ecp = __ecpEnv;

    if (argLen) {
        rv = EC_ERROR_ECP;
    } else {
        rv = ecp.subPricingsCache.unlink (arg);
    }

    return rv;
}



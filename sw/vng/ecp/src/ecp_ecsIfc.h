/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __ECP_ECSIFC_H__

#include "ecsClientIfc.h"
#include "ec.h"



ecs__TitleKindType
ec2soapTitleKind (ECTitleKind titleKind);

ECTitleKind
soap2ecTitleKind (ecs__TitleKindType titleKind);



ecs__PricingKindType
ec2soapPricingKindType (ECPricingKind pricingKind);

ECPricingKind
ec2soapPricingKindType (ecs__PricingKindType pricingKind);


ecs__PricingCategoryType
soap2ecpPricingCategory (ECPricingCategory pricingKind);

ECPricingCategory
soap2ecpPricingCategory (ecs__PricingCategoryType pricingKind);


ecs__TimeUnitType
ec2soapTimeUnit (ECTimeUnit timeUnit);

ECTimeUnit
soap2ecTimeUnit (ecs__TimeUnitType timeUnit);


string
ec2soapLimitKind (ECLimitCode limitCode);

s32
soap2ecLimitCode (string& limitKind);

ecs__PaymentMethodType
ec2soapPaymentMethod (ECPaymentMethod   paymentMethod,
                      int              &unionDiscriminant);

ECPaymentMethod
soap2ecTimeUnit (ecs__PaymentMethodType paymentMethod);


using namespace std;


#endif  /* __ECP_ECSIFC_H__ */




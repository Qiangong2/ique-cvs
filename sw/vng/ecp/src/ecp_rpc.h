/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __ECP_RPC_H__
#define __ECP_RPC_H__

#pragma once

#include "ecp_i.h"


#if !defined(_GBA) && !defined(_SC)
#pragma pack(push, 4)
#endif



#define DECL_ECP_RP(name) extern int32_t name(VN *vn, VNMsgHdr *hdr, const void *arg, size_t argLen, void *ret, size_t *retLen);


DECL_ECP_RP(ecpNotSupported)
DECL_ECP_RP(ecpCancelWebSvcOps)
DECL_ECP_RP(ecpEnablePreload)

/* CID */

DECL_ECP_RP(ecpContent_GetStat)
DECL_ECP_RP(ecpContent_Cache)
DECL_ECP_RP(ecpContent_Open)
DECL_ECP_RP(ecpContent_Read)
DECL_ECP_RP(ecpContent_Seek)
DECL_ECP_RP(ecpContent_Close)
DECL_ECP_RP(ecpContent_Refresh)
DECL_ECP_RP(ecpContent_Unlink)
DECL_ECP_RP(ecpContent_Cancel)

/* EC META */

DECL_ECP_RP(ecpECMeta_GetStat)
DECL_ECP_RP(ecpECMeta_Cache)
DECL_ECP_RP(ecpECMeta_Open)
DECL_ECP_RP(ecpECMeta_Read)
DECL_ECP_RP(ecpECMeta_Seek)
DECL_ECP_RP(ecpECMeta_Close)
DECL_ECP_RP(ecpECMeta_Refresh)
DECL_ECP_RP(ecpECMeta_Unlink)
/* DECL_ECP_RP(ecpECMeta_Cancel) - ecpCancelWebSvcOps */

/* LIST_TITLES */

DECL_ECP_RP(ecpListTitles_GetStat)
DECL_ECP_RP(ecpListTitles_Cache)
DECL_ECP_RP(ecpListTitles_Open)
DECL_ECP_RP(ecpListTitles_Read)
DECL_ECP_RP(ecpListTitles_Seek)
DECL_ECP_RP(ecpListTitles_Close)
DECL_ECP_RP(ecpListTitles_Refresh)
DECL_ECP_RP(ecpListTitles_Unlink)
/* DECL_ECP_RP(ecpListTitles_Cancel) - ecpCancelWebSvcOps */

/* TITLE DETAILS */

DECL_ECP_RP(ecpTitleDetails_GetStat)
DECL_ECP_RP(ecpTitleDetails_Cache)
DECL_ECP_RP(ecpTitleDetails_Open)
DECL_ECP_RP(ecpTitleDetails_Read)
DECL_ECP_RP(ecpTitleDetails_Seek)
DECL_ECP_RP(ecpTitleDetails_Close)
DECL_ECP_RP(ecpTitleDetails_Refresh)
DECL_ECP_RP(ecpTitleDetails_Unlink)
/* DECL_ECP_RP(ecpTitleDetails_Cancel) - ecpCancelWebSvcOps */

/* LIST_SUBSCRIPTION_PRICINGS */

DECL_ECP_RP(ecpListSubPricings_GetStat)
DECL_ECP_RP(ecpListSubPricings_Cache)
DECL_ECP_RP(ecpListSubPricings_Open)
DECL_ECP_RP(ecpListSubPricings_Read)
DECL_ECP_RP(ecpListSubPricings_Seek)
DECL_ECP_RP(ecpListSubPricings_Close)
DECL_ECP_RP(ecpListSubPricings_Refresh)
DECL_ECP_RP(ecpListSubPricings_Unlink)
/* DECL_ECP_RP(ecpListSubPricings_Cancel) - ecpCancelWebSvcOps */

/* SUBSCRIBE */

DECL_ECP_RP(ecpSubscribe_GetStat)
DECL_ECP_RP(ecpSubscribe_Cache)
DECL_ECP_RP(ecpSubscribe_Open)
DECL_ECP_RP(ecpSubscribe_Read)
DECL_ECP_RP(ecpSubscribe_Seek)
DECL_ECP_RP(ecpSubscribe_Close)
DECL_ECP_RP(ecpSubscribe_Refresh)
DECL_ECP_RP(ecpSubscribe_Unlink)
/* DECL_ECP_RP(ecpSubscribe_Cancel)  - ecpCancelWebSvcOps */

/* CHECK IN/OUT */

DECL_ECP_RP(ecpUpdateStatus_GetStat)
DECL_ECP_RP(ecpUpdateStatus_Cache)
DECL_ECP_RP(ecpUpdateStatus_Open)
DECL_ECP_RP(ecpUpdateStatus_Read)
DECL_ECP_RP(ecpUpdateStatus_Seek)
DECL_ECP_RP(ecpUpdateStatus_Close)
DECL_ECP_RP(ecpUpdateStatus_Refresh)
DECL_ECP_RP(ecpUpdateStatus_Unlink)
/* DECL_ECP_RP(ecpUpdateStatus_Cancel) - ecpCancelWebSvcOps */

/* REDEEM */

DECL_ECP_RP(ecpRedeem_GetStat)
DECL_ECP_RP(ecpRedeem_Cache)
DECL_ECP_RP(ecpRedeem_Open)
DECL_ECP_RP(ecpRedeem_Read)
DECL_ECP_RP(ecpRedeem_Seek)
DECL_ECP_RP(ecpRedeem_Close)
DECL_ECP_RP(ecpRedeem_Refresh)
DECL_ECP_RP(ecpRedeem_Unlink)
/* DECL_ECP_RP(ecpRedeem_Cancel)  - ecpCancelWebSvcOps */

/* SYNC_TICKETS */

DECL_ECP_RP(ecpSyncTickets_GetStat)
DECL_ECP_RP(ecpSyncTickets_Cache)
DECL_ECP_RP(ecpSyncTickets_Open)
DECL_ECP_RP(ecpSyncTickets_Read)
DECL_ECP_RP(ecpSyncTickets_Seek)
DECL_ECP_RP(ecpSyncTickets_Close)
DECL_ECP_RP(ecpSyncTickets_Refresh)
DECL_ECP_RP(ecpSyncTickets_Unlink)
/* DECL_ECP_RP(ecpSyncTickets_Cancel)  - ecpCancelWebSvcOps */

/* PURCHASE_TITLE */

DECL_ECP_RP(ecpPurchaseTitle_GetStat)
DECL_ECP_RP(ecpPurchaseTitle_Cache)
DECL_ECP_RP(ecpPurchaseTitle_Open)
DECL_ECP_RP(ecpPurchaseTitle_Read)
DECL_ECP_RP(ecpPurchaseTitle_Seek)
DECL_ECP_RP(ecpPurchaseTitle_Close)
DECL_ECP_RP(ecpPurchaseTitle_Refresh)
DECL_ECP_RP(ecpPurchaseTitle_Unlink)
/* DECL_ECP_RP(ecpPurchaseTitle_Cancel)  - ecpCancelWebSvcOps */





#if !defined(_GBA) && !defined(_SC)
#pragma pack(pop)
#endif


#endif /*__ECP_RPC_H__*/

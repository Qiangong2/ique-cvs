/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __ECP_STL_H__
#define __ECP_STL_H__

#pragma once

#include <hash_map>
using namespace std;
using namespace stdext;

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>


struct eqstr {
    bool operator()(const char* s1, const char* s2) const
    {
        return strcmp(s1, s2) == 0;
    }
};


// used by map, hash_map, hash_set, etc.
template<>
class hash_compare<const char*>
{
    public:

    enum { bucket_size = 4, min_buckets = 8 };

    size_t operator()(const char* s) const
    {
        size_t h = 0;
        for (; *s; ++s)
            h = 5 * h + *s;
        return h;
    }

    bool operator()(const char* s1, const char* s2) const
    {
        return strcmp(s1, s2) < 0;
    }
};

typedef stdext::hash_compare<const char*> ltstr;

struct eqstring {
    bool operator()(const string& s1, const string& s2) const
    {
        return s1 == s2;
    }
};

template<>
class hash_compare<string>
{
    public:

    enum { bucket_size = 4, min_buckets = 8 };

    size_t operator() (const string& str) const
    {
        size_t h = 0;
        const char* s = str.c_str();
        for (; *s; ++s)
            h = 5 * h + *s;
        return (h);
    }

    bool operator()(const string& s1, const string& s2) const {
        return s1 < s2;
    }
};

typedef stdext::hash_compare<string> ltstring;


struct lt_numstring {
    bool operator()(const string& a, const string& b)
    {
        return atoi(a.c_str()) < atoi(b.c_str());
    }
};

typedef hash_map<string, string, ltstring> HashMapString;

int loadConfFile (const char *filename, HashMapString& var);

void dumpHashMapString (HashMapString& pairs, ostream& out = cout);



#endif /*__ECP_STL_H__*/


#include <windows.h>
#include <tchar.h>
#include <crtdbg.h>
#include <sstream>
#include <set>
#include <wininet.h>
#include "App.h"
#define   USE_AH_STRING_FUNCTIONS
#include "apputil.h"
#include "getahdir.h"
//#include "atlconv.h"
//#include "db.h"



// From configvar.h
#define CFG_LANGUAGE            "sys.language"


App* pap;

App::App (const char* appName)
    :  _appName (appName),
       hmWnd (0),
       langid (LANGID_en_US),
       no_popup (false),
       catchUnhandledExceptions (true),
       logUnhandledExceptions (true)
{
    WSADATA info;
    if (WSAStartup(MAKEWORD(2,0), &info) != 0)
      msglog (MSG_ERR, "App::App  Cannot initialize WinSock\n");

    pap = this;
}


App::~App ()
{
    WSACleanup();
}


int App::run (HINSTANCE hInstance,
              LPTSTR    lpstrCmdLine,
              LPCTSTR   appUniqueMutexName,
              LPCTSTR   wndClassName)
{
    startTime = time(NULL);
    hExe = hInstance;

    initLocale();

    return 0;
}

static void modPath (string& pathAddition)
{
   string envpath;

   envpath  = _T("PATH=");
   envpath += getenv( "PATH" );

   envpath += _T(";");
   envpath += pathAddition;

   _putenv( envpath.c_str() );

   envpath = getenv( "PATH" );
}

// constants assoiated with locale
const TCHAR en_US[]        = _T("en_US");
const TCHAR en_US_DllStr[] = _T("409");
const TCHAR zh_CN[]        = _T("zh_CN");
const TCHAR zh_CN_DllStr[] = _T("804");

void App::initLocale (unsigned forcedLocale)
{
    char buf[1024];

    if (forcedLocale) {
        langid = forcedLocale;
    }
    else if (!*getConf(CFG_LANGUAGE, buf, sizeof(buf))) {
        // Use current langid except when overridden by config variable or forcedLocale
        // Constructor default langid is LANGID_zh_CN;
        // To use PC default lang:
        //  langid = GetSystemDefaultLangID();
    }
    else {
        langid = strtoul(buf, NULL, 0);
    }

    switch (langid) {
        case LANGID_en_US:
            localeStdStr = en_US;
            localeDllStr = en_US_DllStr;
            break;
        case LANGID_zh_CN:
            localeStdStr = zh_CN;
            localeDllStr = zh_CN_DllStr;
            break;
        default:
            if (buf[0])
                msglog (MSG_ERR, "initLocale: Unsupported system default language: %u\n", langid);
            else
                msglog (MSG_ERR, "initLocale: Unsupported LANGID specified in CFG_LANGUAGE: %s\n", buf);
            langid       = LANGID_en_US;
            localeStdStr = en_US;
            localeDllStr = en_US_DllStr;
            break;
    }
}










int App::nextTimerID ()
{
    static int id = 0;

    int start = id ? id : INT_MAX;

    for (++id;  id != start;  ++id) {
        if (id == INT_MAX) {
            id = 0;
            continue;
        }

        if (timers.find (id) == timers.end()) {
            break;
        }
    }
    // ignores error case where no timer available
    return id;
}


// setTimer() returns timer event id or 0 on fail
// interval == 0 means create timer but don't start

int App::setTimer (HWND      hWnd,
                   UINT      interval,
                   TimerFunc func,
                   void*     timerFuncArg)
{
    int id = nextTimerID ();
    Timer* timer = new Timer (hWnd, id, interval, func, timerFuncArg);
    timers[id] = timer;
    if (interval && !SetTimer (hWnd, id, interval, NULL)) {
        releaseTimer (id);
        id = 0;
    }
    return id;
}


// resetTimer() returns timer event id or 0 on fail
// interval == 0 means no timeout, but timer still exists

int App::resetTimer (int       id,  // as returned by setTimer
                     UINT      interval,
                     HWND      hWnd,
                     TimerFunc func,
                     void*     timerFuncArg)
{
    if (timers.find (id) == timers.end())
        return 0;

    Timer* timer = timers[id];
    timer->interval = interval;
    if (hWnd)
        timer->hWnd = hWnd;
    if (func)
        timer->func = func;
    if (timerFuncArg)
        timer->arg = timerFuncArg;

    if (!interval) {
        ::KillTimer (timer->hWnd, id);
        timer->enabled = false;
    }
    else if (!SetTimer (timer->hWnd, id, interval, NULL)) {
        releaseTimer (id);
        id = 0;
    }
    else
        timer->enabled = true;

    return id;
}


// setTimerEnable() returns timer event id or 0 if timer doesnt exist

int App::setTimerEnable (int id, bool enable)
{
    if (timers.find (id) == timers.end())
        return 0;

    Timer* timer = timers[id];
    timer->enabled = enable;
    return id;
}


// getTimer() returns NULL if no Timer associated with timerID

const Timer* App::getTimer (int id)
{
    if (timers.find (id) == timers.end())
        return NULL;
    else
        return timers[id];
}


// releaseTimer returns  0 if timer released without error
//                       1 if timer doesn't exist
//                            (i.e. it's ok to call multiple times)
//                      -1 error (none currently returned)

int App::releaseTimer (int id, HWND* hWnd)
{
    if (timers.find (id) == timers.end())
        return 1;

    Timer* timer = timers[id];
    if (hWnd)
        *hWnd = timer->hWnd;
    timers.erase (id);
    delete timer;
    return 0;
}


// killTimer returns 0 if timer killed without error
//                   1 if timer doesn't exist
//                       (i.e. it's ok to call multiple times)
//                  -1 if ::KillTimer returns fail (can call GetLastError())

int App::killTimer (int id)
{
    HWND hWnd;
    int err = releaseTimer (id, &hWnd);

    if (!err) 
        err = ::KillTimer (hWnd, id) ? 0 : -1;

    return err;
}











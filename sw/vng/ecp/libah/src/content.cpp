

#include <windows.h>
#include <direct.h>
#include "getahdir.h"
#include <sys/types.h>
#include <sys/stat.h>
#include "porting.h"
#include "io.h" // for _chmod in win32 difine of mkdir in porting .h
#include "content.h"

#define CONTENT_BUFSIZE   (256*1024)

/*
 *  Create a cache path/filename using the cached files hierarchy 
 *  param dir - cache directory base, must exist
 *  param fileId - string that uniquely identifies the file
 *                 If < 17 chars, will it be used as a filename
 *                 If > 16 chars, subdirs will be created
 *                 using chars 0-7 and 8-15.  chars 16 and up
 *                 will be used as the filename. 
 *  param cache_id - the path of the cache file
 *  param createdir - if true, create the cache dir if necessary
*/
int makeCacheID(const string& dir,
                const string& fileId,
                string& cache_id,
                bool createdir)
{
    if (fileId.size() < 1 || dir.size() < 1)
        return -1;

    char dirend = dir[dir.size() - 1];

    cache_id = dir;

    if (dirend != '/' &&  dirend != '\\') {
        cache_id += '/';
    }

    if (fileId.size() < 17) {

        if (fileId.size() < 3)
            cache_id += fileId;
        else  {
            cache_id += fileId[0];
            cache_id += fileId[1];

            if (fileId.size() > 3) {

                if (createdir)
                    mkdir (cache_id.c_str(), 0755);

                cache_id += '/';
                cache_id += fileId[2];
                cache_id += fileId[3];
            }
        }

        if (createdir)
            mkdir (cache_id.c_str(), 0755);

        cache_id += '/';
        cache_id += fileId;
    }
    else {
        cache_id +=  fileId.substr(0, 8);
        if (createdir) {
            mkdir (cache_id.c_str(), 0755);
        }
        cache_id += '/' + fileId.substr(8, 8);
        if (createdir) {
            mkdir (cache_id.c_str(), 0755);
        }
        cache_id += '/' + fileId.substr(16);
    }
    return 0;
}


int checkCacheID(const string& dir,
                 const string& fileId,
                 off_t fsize,
                 string& cache_id,
                 off_t *outsize)
{
    struct stat sbuf;
    if (outsize) {
        *outsize = -1;
    }
    if (makeCacheID(dir, fileId, cache_id, false) != 0)
        return -1;
    if (stat(cache_id.c_str(), &sbuf) == 0) {
        if (outsize) {
            *outsize = sbuf.st_size;
        }
        if (sbuf.st_size == fsize) {
            return 1;
        }
    }
    return 0;
}


int copyContentObj(const string& infile, const string& outfile, Progress pg)
{
    string tmpfile = outfile + ".tmp";
    int ifd = open(infile.c_str(), O_RDONLY|_O_BINARY);
    if (ifd < 0) {
        msglog(MSG_ERR, "copyContentObj: can't read %s\n", infile.c_str());
        return -1;
    }

    int ofd = open(tmpfile.c_str(), O_CREAT | O_WRONLY | O_TRUNC | _O_BINARY, 0755);
    if (ofd < 0) {
        msglog(MSG_ERR, "copyContentObj: can't write to %s\n", tmpfile.c_str());
        return -1;
    }

    int n;
    unsigned total = 0;
    char buf[CONTENT_BUFSIZE];
    while ((n = read(ifd, buf, sizeof(buf))) > 0) {
        write(ofd, buf, n);
        total += n;
        pg(total);
    }

    close(ofd);
    close(ifd);

    if (rename(tmpfile.c_str(), outfile.c_str()) != 0) {
        msglog(MSG_ERR, "copyContentObj: can't rename to %s\n", outfile.c_str());
        return -1;
    }

    return 0;
}


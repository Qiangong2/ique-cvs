#include <direct.h>     // _mkdir
#include <crtdbg.h>
#include "common.h"
#include "getahdir.h"
#include <set>
// #include <tchar.h>
// May be dificult to use TCHAR vs CHAR because
// old code assumes char.  To switch to unicode
// might require many changes to old code.

typedef set<string> Directory;

static Directory dir;
static string ahDir;

static const char slash  = '/';
static const char bslash = '\\';
static string coreBin      = _T("pkgs/core/bin");
static string coreBinUpper = _T("PKGS/CORE/BIN"); // when started at Win98 MS-DOS command prompt



extern "C" int deleteAhDirStrC (const char* ahDirStr)
{
    return dir.erase(ahDirStr) ?  true : false;
}

bool deleteAhDirStr (const char* ahDirStr)
{
    return dir.erase(ahDirStr) ?  true : false;
}

const char* getDataDir (const char* leaf, bool leafIsDir, bool createDirs)
{
    return getAhDir ("data", leaf, leafIsDir, createDirs);
}


const char* getTempDir (const char* leaf, bool leafIsDir, bool createDirs)
{
    return getAhDir ("data/tmp", leaf, leafIsDir, createDirs);
}


const char* getEtcDir (const char* leaf, bool leafIsDir, bool createDirs)
{
    return getAhDir ("data/etc", leaf, leafIsDir, createDirs);
}


extern "C"
const char* getEtcDirC (const char* leaf, bool leafIsDir, bool createDirs)
{
    return getAhDir ("data/etc", leaf, leafIsDir, createDirs);
}


const char* getShareDir (const char* leaf, bool leafIsDir, bool createDirs)
{
    return getAhDir ("pkgs/core/share", leaf, leafIsDir, createDirs);
}


const char* getBinDir (const char* leaf, bool leafIsDir, bool createDirs)
{
    return getAhDir (coreBin.c_str(), leaf, leafIsDir, createDirs);
}


const char* getResDir (const char* leaf, bool leafIsDir, bool createDirs)
{
    return getAhDir ("pkgs/loc/res", leaf, leafIsDir, createDirs);
}

const char* getDiagDir (const char* leaf, bool leafIsDir, bool createDirs)
{
    return getAhDir ("pkgs/diag", leaf, leafIsDir, createDirs);
}


int createDirs (string& path)
{
    // construct vector of dirs that need to be created
    struct stat sbuf;
    string::size_type pos;
    string w = path;
    vector<string> tc;  // to create

    while (true) {
        if ((pos=w.find_last_of(_T("\\/")))!=tstring::npos) {
            if ((pos+1) < w.length()) {
                if (stat(w.c_str(), &sbuf) == 0)
                    break;
                tc.push_back(w.substr(pos));
            }
            w.erase(pos);
            continue;
        }
        // no more slashes
        // possible w's:  "c:", "c:b"  "b"  empty
        if (!w.empty() && w[w.length()-1]!=_T(':')) {
            if (stat(w.c_str(), &sbuf) == 0)
                break;
            tc.push_back(w);
            w = "";
        }
        break;
    }

    // if tc is empty, there is nothing to do

    vector<string>::reverse_iterator p;

    for (p = tc.rbegin();  p  != tc.rend();  ++p) {
        w += *p;
        if (_mkdir(w.c_str()) != 0) {
            fatal("can't create directory %s\n", w.c_str());
        }
    }
    return 0;
}


const char* getAhDir (const char* branch, const char* leaf,
                      bool leafIsDir,
                      bool createDirsIfDontExist)
{
    size_t i;
    string::size_type pos;

    if (ahDir.empty()) {
        /*** cant use sys_getconf or msglog as they will also call this function */
        _TCHAR full[_MAX_PATH];
        _tfullpath (full, __targv[0], _MAX_PATH);
        string exedir = full;
        pos = 0;
        while ((pos=exedir.find(bslash, pos)) != string::npos) {
            exedir[pos++] = slash;
        }
        if ((pos=exedir.find_last_of(slash)) != string::npos) {
            exedir.erase(pos);
        }
        i = exedir.length() - coreBin.length();
        if ((pos=exedir.rfind(coreBin,i))==i || (pos=exedir.rfind(coreBinUpper,i))==i) {
            exedir.erase(pos);
        }
        ahDir = exedir;
    }

    string path = ahDir;

    if (branch && *branch) {
        char last = path[path.size()-1];
        if (last != slash  &&  last != bslash
               &&  branch[0] != slash  &&  branch[0] != bslash)
            path += slash;
        pos = path.length();
        path += branch;
        while ((pos=path.find(bslash, pos)) != string::npos)
            path[pos++] = slash;
    }

    /* Regarless of passed leafIsDir, if leaf ends in '\' or '/'
       it will be considered a directory */

    if (!leafIsDir && leaf && *leaf) {
        char last = leaf[strlen(leaf)-1];
        if (last == slash  ||  last == bslash)
            leafIsDir = true;
    }

    if (!leafIsDir && createDirsIfDontExist) {
        createDirs (path);
    }

    if (leaf && *leaf) {
        char last = path[path.size()-1];
        if (last != slash  &&  last != bslash
               &&  leaf[0] != slash  &&  leaf[0] != bslash)
            path += slash;
        pos = path.length();
        path += leaf;
        while ((pos=path.find(bslash, pos)) != string::npos)
            path[pos++] = slash;
    }

    if (leafIsDir && createDirsIfDontExist && leaf && *leaf) {
        createDirs (path);
    }

    /* dir is used as a cache of stings so a char ptr can be returned
       from the function.
       If the string is already in dir, a pointer to the existing string is
       returned (i.e. it doesn't do needless deallocation/allocation */

    const char * result;

    try {
        result = dir.insert(path).first->c_str();
    }
    catch (...) {
        result = 0;
    }
    _ASSERT (result!=0);

    return result;
}
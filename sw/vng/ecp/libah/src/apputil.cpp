
#define USE_AH_STRING_FUNCTIONS

#include <fstream>
#include <sstream>

#include <process.h>
#include "apputil.h"
#include "sysclnt.h"





/*   Note: 'configvar=' is not the same as an unset configvar !
 *     If configvar is unset, it is an undefined config variable
 *     and sys_getconf will return non-zero.
 *     If configvar is set to nothing (i.e. 'configvar='), it is a
 *     defined config variable and sys_getconf returns 0.  It is
 *     equivalent to a variable set to an empty string (i.e. "").
 *
 *     The default value "def" will be returned if the
 *     config variable is unset or if useDefForDefinedButEmpty==true
 *     and the config variable is defined but empty.
 *     An empty string (i.e. "") will be retured for a defined but
 *     empty config variable when useDefForDefinedButEmpty==false
 */
char * getConf (const char *name, char *buf, size_t size,
                const char *def, bool useDefForDefinedButEmpty)
{
#ifdef _WIN32
    char tmpbuf[4096];
    if (size > sizeof tmpbuf)
        size = sizeof tmpbuf;
#else
    char tmpbuf[size];
#endif
    if (sys_getconf (name, tmpbuf, size) == 0) {
        if (!*tmpbuf && useDefForDefinedButEmpty)
            strcpy (tmpbuf, def);
        tmpbuf[size-1] = '\0';
        strncpy (buf, tmpbuf, size);
    } else if (!def) {
        buf[0] = '\0';
    } else if (strlen(def) < size) {
            strcpy (buf, def);
    } else {
        strncpy (buf, def, size);
        buf[size-1] = '\0';
    }
    return buf;
}

int setConf (const char *name, const char *value, int overwrite, bool unsetIfNull)
{
    int retv = 0;
    if (unsetIfNull && (!value || *value == '\0')) {
        sys_unsetconf(name);
    } else {
        retv =sys_setconf(name, value, overwrite);
        if (retv) {
            msglog(MSG_ALERT, "gui: setConf: sys_setconf returned %d setting config-var %s\n", retv, name);
        }
    }
    return retv;
}



int setConfn (const char* name[], const char* value[], int n, int overwrite, bool unsetIfNull)
{
    int retv = 0;
    while (n-- > 0 && retv == 0) {
        retv = setConf(name[n], value[n], overwrite, unsetIfNull);
    }
    return retv;
}

bool isGOS()
{
    int fd;
    bool isgos = (getenv("THIS_IS_NOT_GOS")==NULL) ? true:false;

    if (isgos) {
        const char* file = GWOS_MODEL;
        if ((fd = open(file, O_RDONLY)) < 0) {
            isgos = false;
        } else {
            close(fd);
        }
    }

    return isgos;
}

bool isRmsAvailable()
{
    return false;
}


bool isThreadRunning (Thread thread, DWORD* exitCode, int* lastError)
{
    bool result = false;
    int  err    = 0;
    DWORD code;
    DWORD& exit_code = exitCode ? *exitCode : code;

    if (thread.handle) {
        if (!GetExitCodeThread ((HANDLE)thread.handle, &exit_code))
            err = GetLastError();
        else if (exit_code == STILL_ACTIVE)
            result = true;
        if (!result) {
            if (!err) {
                CloseHandle ((HANDLE)thread.handle);
                thread.clear();
            }
            else if (err != ERROR_INVALID_HANDLE)
                cancelThread (thread);
        }
    }

    if (lastError)
        *lastError = err;

    return result;
}

int cancelThread (Thread& thread)
{
    /*
    Warning  The TerminateThread and TerminateProcess functions should
    be used only in extreme circumstances, since they do not allow
    threads to clean up, do not notify attached DLLs, and do not free
    the initial stack. The following steps provide a better solution:

        - Create an event object using the CreateEvent function. 
        - Create the threads. 
        - Each thread monitors the event state by calling the
          WaitForSingleObject function. Use a wait time-out interval
          of zero. 
        - Each thread terminates its own execution when the event is
          set to the signaled state
          (WaitForSingleObject returns WAIT_OBJECT_0). 
    */
    
    int err = 0;
    DWORD exitCode = 1;

    if (thread.handle) {
        if (!(err=TerminateThread((HANDLE)thread.handle, exitCode))) {
            msglog(MSG_ERR, "cancelThread: TerminateThread failed with error %d\n", GetLastError());
        }
        if (err != ERROR_INVALID_HANDLE)
           CloseHandle ((HANDLE)thread.handle);
    }
    thread.handle = 0;
    thread.id = 0;
    return err;
}

#if 0

int loadConfFile (const char *filename, HashMapString& var)
{
    unsigned pos;
    string   line;
    string   key;
    string   value;

    ifstream is (filename);
    if (!is) {
        return -1;
    }
    while(is.good()) {
        getline (is, line);
        if (!is.eof() && is.fail())
            msglog(MSG_INFO, "loadConfFile: fail reading: %s\n", filename);
        if ( line.empty() ||
               ((pos=line.find_first_not_of(" \t\n\r\v\f"))==string::npos) ||
                   line[pos]=='#') {
            continue;
        }
        istringstream is_line (line.substr (pos, string::npos));
        getline(is_line,key,'=');
        pos = key.find_last_not_of(" \t\n\r\v\f");
        if (pos!=string::npos)
            key.erase (pos+1);
        getline (is_line, value);
        pos = value.find_first_not_of(" \t\n\r\v\f");
        if (pos)
            value.erase (0, pos);
        var[key] = value;
    }
    is.close();
    return 0;
}

void dumpHashMapString (HashMapString& pairs)
{
    HashMapString::iterator pair;
    for (pair = pairs.begin(); pair != pairs.end(); ++pair)
        cerr << pair->first << "=" << pair->second << endl;
}

#endif

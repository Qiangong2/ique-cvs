

#pragma once


#include "ExOp.h"
#include "comm.h"





// Download content from internet Operation

class NetContent : public  ExOp {

    static unsigned netContent_s (NetContent *pt);
           unsigned netContent ();

    enum OpProgress {
        NOT_STARTED,
        READY,   /* ready to start */
        GET_FROM_NET,
        DONE
    };


    OpProgress   _doing;

    Comm         _com;

    HWND         _hWnd;
    string       _url_prefix;
    string       _content_id;
    string       _abspath;

    bool         _replace_cached;

    int          _maxTime_net;

    // since content size may be unknown,
    // _download_size and _total_size may be faked
    // to show reasonable progress
    ulong        _content_size;  // may be unknown (i.e. 0)
    ulong        _cached_size;   // previous downloaded
    ulong        _download_size; // to actually download
    ulong        _total_size;    // including previously downloaded
    ulong        _content_size_per_hdr; // set by downloadContentObj
    ulong        _size_so_far;          // set by downloadContentObj
    int          _http_status;          // set by downloadContentObj
    bool         _isKnownContentSize;


    time_t       _start_time;
    time_t       _last_change_time;

    int          _last_st_size;
    int          _bytes_per_sec;
    int          _min_bytes_per_sec;

    time_t       _netContentStartTime;
    time_t       _netContentEndTime;
    int          _duration;
    int          _rate;

    const char*  _appName;

    // _netTmpFile is NULL untill just before it is opened.
    // It is synchronized with cs_pg.
    // It will go back to NULL when the thread finishes download
    // from net.
    //
    // When the thread sets _netTmpFile, _total_size is
    // guaranteed to be already set but may be fake.
    // 
    const char*  _netTmpFile;

    string       _tmpfile; // holds *_netTmpFile

    int  getContentFromNet (string& content_id,
                            int     content_size,
                            int&    err_code);

    void setProgressParam (int bytes_so_far, time_t now);

  public:

    NetContent (HWND           hWnd,
                int            maxTime_net,
                const char*    url_prefix,
                const char*    content_id,
                int            content_size = 0,  /* unknown size */
                bool           start_now    = true,
                bool           replace_cached = false);

    virtual ~NetContent ();

    void updateProgress ();

    string& content_id() {return _content_id;}

    ulong content_size_per_hdr () {return _content_size_per_hdr;}

    bool isCanceled (bool& isUnlinkOnCancel)
    {
        isUnlinkOnCancel = _unlinkOnCancel;
        return _cancel;
    }

    // methods inherited from ExOp include:
    //  
    //    int  getProgress ();
    //    bool isDone ();
    //    void updateProgress (); -- redefined by NetContent
    //    void setProgress (int progress);
    //    void checkFinishedWork ();

    // methods also inherited from ThreadWTO
    // methods inherited from ThreadWTO include:
    //
    // start ();
    // stop ();
    // startMonitor ();


    // DANGER !!!!!
    //
    // If isDone() returns false, the only aspect
    // of an a NetContent that can be accessed  is
    // getProgress().  The NetContent thread owns
    // access to member variables utill it isDone().
};

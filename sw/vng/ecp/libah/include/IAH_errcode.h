
#pragma once

/*** Purchase failures ***/

// A conflicting operation was requested
// while a purchase is already active
#define IAH_ERR_PurAlreadyActive             151

// Attempt to get purchase progress when no purchase active
#define IAH_ERR_NoActivePur                  152

// Purchase kind was not UNLIMITED or TRIAL
#define IAH_ERR_PurchaseInvalidKind          153

// A non-specific purchase thread failure
#define IAH_ERR_PurchaseTitle                154

// A purchase did not complete in allowed time
#define IAH_ERR_PurchaseTimeout              155

// A attempt to do an operation that requires an iQue Player
// when no iQue Player is connected.
#define IAH_ERR_NO_IQUE_PLAYER               156

// A attempt to do an operation that requires an iQue Player
// when the connected iQue Player has a failure.
// NO LONGER IN USE.   BBC err codes used instead
// #define IAH_ERR_IQUE_PLAYER_FAIL             157

// A attempt to do an operation that requires a bbcard
// when no bbcard is avaiable.
// NO LONGER IN USE. IAH_ERR_PlayerRemoved ocurrs instesd
// #define IAH_ERR_NO_IQUE_CARD                 158

// A attempt to do an operation that requires a bbcard
// when the bbcard has a failure.
// NO LONGER IN USE.  BBC err codes used instead
// #define IAH_ERR_IQUE_CARD_FAIL               159

// Attemped an operation when not logged in
// NO LONGER IN USE.
// #define IAH_ERR_NO_SESSION_COOKIE            160

// Open HTTP failed during purchase
#define IAH_ERR_PURCHASE_OPEN_HTTP           161


// Error preparing or sending purchase request
#define IAH_ERR_PURCHASE_TITLE_REQUEST       162

// Failed to update iQue Player with Etickets after purchase
#define IAH_ERR_PURCHASE_UPDATE_ETICKETS     163

// Fail while reading iQue Card info after update Etickets after purchase
// NO LONGER IN USE
// #define IAH_ERR_PURCHASE_GET_IQUE_CARD_INFO  164



/*** Retrieve failures ***/

// A non-specific failure while storing content to an iQue Card
#define IAH_ERR_RetrieveTitleRequest         200

// BBC err code while storing content to an iQue Card are computed as
// IAH_ERR_RetrieveTitleRequest + abs(BBC err code)

#define IAH_MIN_BBC_ERR_CODE                   1
#define IAH_MAX_BBC_ERR_CODE                  48

// Set if the BBC err code is outside min/max limits
#define IAH_ERR_RETRIEVE_BBC_CODE            249


// A conflicting operation was requested
// while a retrieve is already active
#define IAH_ERR_RetrieveAlreadyActive         251
 
// Attempt to get retrieve progress when no retrieve active
#define IAH_ERR_NoActiveRetrieve              252

// Attempt to retrieve a title not owned
#define IAH_ERR_RETRIEVE_DOESNT_OWN           253

// Attempt to retrieve a title that is not cached and net is not available
#define IAH_RETRIEVE_NET_NA                   254


// Timeout while retrieving a title
#define IAH_ERR_RetrieveTimeout               255

// Fail while reading iQue Card info after retrieve a title
// NO LONGER IN USE
// #define IAH_ERR_RETRIEVE_GET_IQUE_CARD_INFO   256

// Fail while getting content from CDS server
#define IAH_ERR_RETRIEVE_FROM_CDS             257

// Fail while getting content from CDS server
#define IAH_ERR_RetriveContentNotAvailable    258

// Fail while getting content from CDS server
#define IAH_ERR_RetriveCantCreateCacheFile    259


/***  Remove failures ***/

// A non-specific failure while removeing title from iQue Card
#define IAH_ERR_RemoveTitleRequest            300

// BBC err code while removeing title from iQue Card are computed as
// IAH_ERR_RemoveTitleRequest + abs(BBC err code)

// Set if the BBC err code is outside min/max limits
#define IAH_ERR_REMOVE_BBC_CODE               349


// A conflicting operation was requested
// while a remove is already active
#define IAH_ERR_RemoveAlreadyActive           351


// Attempt to get remove progress when no remove active
#define IAH_ERR_NoActiveRemove                352

// Request to remove a title not owned
#define IAH_ERR_REMOVE_DOESNT_OWN             353

// Request to remove a title not on the card
#define IAH_ERR_REMOVE_NOT_ON_CARD            354

// Timeout while removing a title from an iQue Card
#define IAH_ERR_RemoveTimeout                 355

// Fail while reading iQue Card info after remove a title
// NO LONGER IN USE
// #define IAH_ERR_REMOVE_GET_IQUE_CARD_INFO     356



// BBPlayer Or Card error
#define IAH_ERR_BBPlayerOrCard                400

// BBC err code while check iQue Player or Card are computed as
// IAH_ERR_BBPlayerOrCard + abs(BBC err code)

// Set if the BBC err code is outside min/max limits
#define IAH_ERR_BBPlayerOrCard_BBC_CODE       449



/*** cacheContent/cacheTitle failures ***/

/** These codes apply to both cacheContent and cacheTitle requests **/

// A non-specific failure while getting content via net
#define IAH_ERR_NetContent                      500

// A conflicting operation was requested
// while a cacheContent/Title is already active
#define IAH_ERR_NetContentAlreadyActive         551
 
// Attempt to get cacheContent/Title progress when no cacheContent/Title active
#define IAH_ERR_NoActiveNetContent              552

// Attempt to cacheContent/Title when net is not available
#define IAH_ERR_NetContent_NET_NA               553

// Timeout while cacheContent/Title
#define IAH_ERR_NetContentTimeout               555

// Couldn't get content from CDS server,reason unspecific
#define IAH_ERR_ContentNotAvailable             558

// Couldn't create Cache file
#define IAH_ERR_CantCreateCacheFile             559


/*** Get Meta data failures ***/

// A non-specific failure while getting meta data via net
#define IAH_ERR_NetMeta                      600

// A conflicting operation was requested
// while a get Meta data is already active
#define IAH_ERR_NetMetaAlreadyActive         651
 
// Attempt to get metadata progress when no get meta data active
#define IAH_ERR_NoActiveNetMeta              652

// Attempt to get meta data when net is not available
#define IAH_ERR_NetMeta_NET_NA               653

// Problem with format of xml data
#define IAH_ERR_NetMeta_XmlDataFormat        654

// Timeout while getting meta data
#define IAH_ERR_NetMetaTimeout               655


/*** Get Etickets via Net status codes ***/

// A non-specific failure while getting Etickets via net
#define IAH_ERR_NetEtickets                  700

// A conflicting operation was requested
// while a getNetEtickets is already active
#define IAH_ERR_NetEticketsAlreadyActive     701
 
// Attempt to get getNetEtickets progress when not active
#define IAH_ERR_NoActiveNetEtickets          702

// Attempt getNetEtickets when net is not available
#define IAH_ERR_NetEtickets_NET_NA           703

// Timeout during getNetEtickets operation
#define IAH_ERR_NetEticketsTimeout           704

// Error preparing or sending Eticket request
#define IAH_ERR_NetEticketsRequest           705

// A non-OSC err getting syncEticketsResponse
#define IAH_ERR_NetEticketsResponse          706



/*** Update card etickets or game content status codes ***/

// A non-specific failure while updating card etickets or game content
#define IAH_ERR_UpdateCard                   750

// A conflicting operation was requested
// while an update etickets or update game content is already active
#define IAH_ERR_UpdateCardAlreadyActive      751
 
// Attempt to get update progress when not active
#define IAH_ERR_NoActiveUpdateCard           752

// Timeout while writing game content or etickets to iQue card
#define IAH_ERR_UpdateCardTimeout            753

// Failed to update iQue Player with Etickets
#define IAH_ERR_UpdateEtickets               754

// Request to update card content when content is not cached
#define IAH_ERR_ContentNotCached             755

// Requested to update content when have no eticket
#define IAH_ERR_NoTicketForContent           756

// Non-specific failure doing upgrade request to server
#define IAH_ERR_NetUpgradeRequest            757


/*** status codes while running Diagnostics ***/

// A non-specific failure while running Diagnostics
#define IAH_ERR_Diag                         770

// A conflicting operation was requested
// while Diagnostics is already active
#define IAH_ERR_DiagAlreadyActive            771
 
// Attempt to get Diagnostics progress when not active
#define IAH_ERR_NoActiveDiag                 772


/*** status codes while sending logs email ***/

// A non-specific failure while sending logs email
#define IAH_ERR_LogsEmail                    780

// A logs email is already active
#define IAH_ERR_LogsEmailAlreadyActive       781
 
// Attempt to get logs email (i.e. IAH_sendLogs) progress when not active
#define IAH_ERR_NoActiveLogsEmail            782


/*** status codes while reading and sending game state  ***/

// A non-specific failure while reading game state on card
#define IAH_ERR_ReadGameState                790

// An conflicting operation was requested
// while SendGameState is already active
#define IAH_ERR_SendGameStateAlreadyActive   791
 
// Attempt to get SendGameState progress when not active
#define IAH_ERR_NoActiveSendGameState        792

// Attempt to read or send game state for a title not owned
#define IAH_ERR_GameStateDoesntOwn           793

// Game state not present on card
#define IAH_ERR_GameStateNotOnCard           794
                                               
// Problem reading game state from card        
#define IAH_ERR_CantReadGameState            795
                                               
// Problem with game state signature           
#define IAH_ERR_InvalidGameStateSig          796
                                               
// Timeout while reading game state            
#define IAH_ERR_ReadGameStateTimeout         797
                                               
// Timeout while uploading game state to server  
#define IAH_ERR_UploadGameStateTimeout       798
                                               
// Non-specific failure uploading game state to server
#define IAH_ERR_UploadGameState              799



/* Fatal Errors */

// Unable to initialize a Web Browser control
#define IAH_ERR_CouldNotSetupWBC             901

// Unable to initilize the Web Browser event sink
#define IAH_ERR_CouldNotSetupWBS             902

// Could not LoadLibrary for resource DLL
#define IAH_ERR_LoadLibraryRes               903

// Could not create main window
#define IAH_ERR_CreateMainWnd                904

// Could not create a synchronization event
#define IAH_ERR_CreateEvent                  905

// Unable to Create a Mutex
#define IAH_ERR_CreateMutex                  906
 



/*** miscelaneous failures ***/

// Unrecognized func name in call to
// window.external.IAH_status (func name)
#define IAH_ERR_INVALID_WE_PROGRESS_ID       999


// A purchase or retrieve requested when there
// is not enough room on the card.
#define IAH_ERR_NotRoomOnCard                998


// Unable to obtain lock on a BBPlayer object
#define IAH_ERR_CantLockBBPlayer             997

// Attempted operation for a playerID other than on the current card
#define IAH_ERR_PlayerIDMismatch             996

// Player removed during an operation that requires the player
#define IAH_ERR_PlayerRemoved                995

// An internal software problem
#define IAH_ERR_InternalError                994

// Internet is not available
#define IAH_ERR_NET_NA                       993

// An extended operation (i.e. IAH_cacheContent) was cancled
#define IAH_ERR_OPERATION_CANCELED           992





/* Error codes returned by OSC */

// A non-OSC err getting purchase title response
#define IAH_ERR_PURCHASE_TITLE_RESPONSE      1000

// OSC purchase response errs are computed as
// IAH_ERR_PURCHASE_TITLE_RESPONSE + status returned by OSC in response

#define IAH_MIN_PURCHASE_RESPONSE_STATUS        1
#define IAN_MAX_PURCHASE_RESPONSE_STATUS      998

// Set if the status returned by OSC is outside min/max limits
#define IAH_PURCHASE_TITLE_RESPONSE_STATUS   1999

// ERRORS indicated by HTTP status code
#define IAH_BASE_HTTP_STATUS                 2000
#define IAH_MAX_HTTP_STATUS                  2599


#define IAH_ERR_OSC_BASE                     1000

// OSC response errs are computed as
// IAH_ERR_OSC_BASE + status returned by OSC in response

#define IAH_MIN_OSC_STATUS                      1
#define IAN_MAX_OSC_STATUS                    998

// Set if the status returned by OSC is outside min/max limits
#define IAH_OSC_STATUS                       1999



// ERRORS indicated by HTTP status code
#define IAH_BASE_HTTP_STATUS                 2000
#define IAH_MAX_HTTP_STATUS                  2599

#define IAH_BASE_NAV_HTTP_STATUS             3000
#define IAH_MAX_NAV_HTTP_STATUS              3599


// Open HTTP failed 
#define IAH_ERR_OPEN_HTTP                    2600


inline
int oscStatus (int status)
{
    if (status >= IAH_MIN_OSC_STATUS
          && status <= IAN_MAX_OSC_STATUS)
        return IAH_ERR_OSC_BASE + status;
    else if (status >= IAH_BASE_HTTP_STATUS
                && status <= IAH_MAX_HTTP_STATUS)
        return status;
    else
        return IAH_OSC_STATUS;
}

                                                             

inline
int errBBPlayerBBC_ErrCode (int code)
{
    code = -code;
    if (code >= IAH_MIN_BBC_ERR_CODE
          && code <= IAH_MAX_BBC_ERR_CODE)
        return IAH_ERR_BBPlayerOrCard + code;
    else
        return IAH_ERR_BBPlayerOrCard_BBC_CODE;
}


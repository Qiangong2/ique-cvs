#ifndef __GETAHDIR_H__
#define __GETAHDIR_H__


#ifdef __cplusplus

// examples:  getDataDir ("");              // returns  <ahDir>/data
//            getDataDir ("/");             // returns  <ahDir>/data/
//            getDataDir ("myfile");        // returns  <ahDir>/data/myfile
//            getDataDir ("/myfile");       // returns  <ahDir>/data/myfile
//            getDataDir ("mydir/myfile");  // returns  <ahDir>/data/mydir/myfile
//            getDataDir ("mydir",true);    // returns  <ahDir>/data/mydir
//            getDataDir ("mydir/");        // returns  <ahDir>/data/mydir/
//            getDataDir ("/mydir/");       // returns  <ahDir>/data/mydir/

    /* Regardless of passed leafIsDir, if leaf ends in '\' or '/'
       it will be considered a directory */

const char* getDataDir  (const char* leaf="", bool leafIsDir=false, bool createDirs=true);
const char* getTempDir  (const char* leaf="", bool leafIsDir=false, bool createDirs=true);
const char* getEtcDir   (const char* leaf="", bool leafIsDir=false, bool createDirs=true);
const char* getShareDir (const char* leaf="", bool leafIsDir=false, bool createDirs=true);
const char* getBinDir   (const char* leaf="", bool leafIsDir=false, bool createDirs=true);
const char* getResDir   (const char* leaf="", bool leafIsDir=false, bool createDirs=false);
const char* getDiagDir  (const char* leaf="", bool leafIsDir=false, bool createDirs=true);


// examples:  getAhDir ("data/cache/", "myfile");  // returns  <ahDir>/data/cache/myfile
//            getAhDir ("data/cache",  "myfile");  // returns  <ahDir>/data/cache/myfile
//            getAhDir ("data/cache/",  "");       // returns  <ahDir>/data/cache/
//            getAhDir ("data/cache",   "");       // returns  <ahDir>/data/cache
const char* getAhDir (const char* branch, const char* leaf="",
                      bool leafIsDir=false,
                      bool createDirsIfDontExist=true);


// Delete a string returned by one of the getXxxDir functions.
// The strings are cached and remain for the session unless uncached.
// Returns true if string was found, else false
bool deleteAhDirStr (const char* ahDirStr);

#else
    #define getEtcDir(leaf,leafIsDir,createDirs) getEtcDirC(leaf,leafIsDir,createDirs)

    typedef enum { false, true } bool;
    const char* getEtcDirC   (const char* leaf, bool leafIsDir, bool createDirs);

    #define deleteAhDirStr(leaf) deleteAhDirStrC(leaf)
    int deleteAhDirStrC (const char* ahDirStr);
#endif


#endif /*__GETAHDIR_H__*/

#ifndef __COMM_H__
# define __COMM_H__

#include <stddef.h>   // NULL

#include <vector>
#include <string>

#include "ssl_socket.h"
#include "errcode.h"

#if !defined(_WININET_)
    typedef void* HINTERNET;
    typedef HINTERNET* LPHINTERNET;
#endif


using namespace std;
#ifndef _WIN32
    using namespace __gnu_cxx;
    #define HTTP_USER_AGENT ""
#else
    #define HTTP_USER_AGENT "NC ECommerce Proxy"
#endif

struct FileObj;

/** Control to use HTTP GET or HTTP POST for the server
    transactions */
enum COMM_PROTOCOLS {
    COMM_HTTP_GET  = 0,
    COMM_HTTP_POST = 1,
};


class Http {
 public:
    enum {
        NEW       = 0,
        CONNECTED = 1,
        BUSY      = 2,
        DATAREADY = 4,
        HTTPERROR = 8,
    };
 private:
    bool    _secure;
    bool    _async_flag;
    bool    _verbose;
    #ifndef _WIN32
        bool    _saddr_set;
    #endif
    string  _host;
    int     _port;
    int     _state;
    #ifndef _WIN32
        struct  sockaddr_in _saddr;
    #endif
    string _user_agent;

    // use either skt or sskt, not at the same time.

    Socket  _skt;
    SecureSocket _sskt;

    #ifndef _WIN32
        template <class SKT>
            int send_tmpl(SKT& skt, int method, 
                          const string& abspath, const string& msg);
        template <class SKT>
            int recv_tmpl(SKT& skt, string& hdr, string& response, int& status);
        template <class SKT>
            int terminate_tmpl(SKT& skt);
    #else
        int setup_wi();
        int send_wi (int method, const string& abspath, const string& msg);
        int recv_wi (string& hdr, string& response, int& status);
        int terminate_wi ();

        HINTERNET _hwiInternet;
        HINTERNET _hwiSession;
        HINTERNET _hwiRequest;
    #endif

 public:
     Http(const char* user_agent=HTTP_USER_AGENT) :
        _state       (NEW),
        _verbose     (false),
        #ifndef _WIN32
            saddr_set   (false),
        #endif
        _hwiInternet (NULL),
        _hwiSession  (NULL),
        _hwiRequest  (NULL),
        _user_agent  (user_agent)
        {}
    ~Http() { if (_state & CONNECTED) terminate(); }

    #ifndef _WIN32
        void bind(struct sockaddr_in *sin) { _saddr = *sin; _saddr_set = true; }
    #endif
    int setup(const string& host, int port, bool async, const SSLparm& ssl);
    int setup(const string& host, int port, bool async);
    int sendRequest(int method, const string& abspath, const string& msg);
    int recvResponse(string& hdr, string& response, int& status);
    int terminate();
    int check(const string& host, int port, bool secure) const;
    bool async() { return _async_flag; }
    bool dataReady() { return (_state & DATAREADY) != 0; }
    void initState(int v) { _state = v; }
    int  State() const { return _state; }
    void setState(int v) { _state |= v; }
    void clrState(int v) { _state &= ~v; }
    void setVerbose() { _verbose = true; }
    void clrVerbose() { _verbose = false; }
};


class Comm {


 public:    
    int   verbose;

    // SSL parameters - Not used unless we switch from wininet to open ssl
    SSLparm ssl_parm;

    Comm();
    ~Comm() {}

    };


/** Break down a URL string into components */
int parseURL(const char *url_str,
             bool& secure,
             string& server,
             int& port,
             string& uri);


/** Perform a custom remote procedure call based on HTTP[S] protocol.
    The second parameter is the function index.  The function index is
    used to index into the Comm structure to determine the complete
    URL.
    
     @param c the communcation credentials
     @param func_idx the function to be invoked
     @param flags use POST or GET
     @param post the HTTP message to be sent
     @param resp the HTTP response
     @param http_stat if pointer is non NULL, return HTTP response status or -1 if status not available
*/
int RemoteProcCall(Comm& c, 
                   int func_idx, 
                   int flags, 
                   crope& post, 
                   string& response,
                   int *http_stat=NULL);


/************************************************************************
      Statistics
************************************************************************/

/** @defgroup statistics Interface to Statistics Uploader
    @{ */

class DepotStat;

enum StatType {
    STAT_TYPE_NONE,
    CDS_CONTENTS_COUNT,
    CDS_CONTENTS_TIMESTAMP,
    CUSTOMER_SESSION,
    CUSTOMER_ETICKET_SYNC,
    CUSTOMER_PURCHASE,
    CUSTOMER_DOWNLOAD,
    CDS_DOWNLOAD_SPEED,
    CDS_SYNC_TIME,
    INSTALLER_SESSION,
    RETAILER_SESSION,
    MAX_STAT_TYPE
};


#ifndef _WIN32
    int recordStat(StatType type, double value);
    int recordError(StatType type);
#else
    inline int recordStat(StatType type, double value) {return 0;}
    inline int recordError(StatType type) {return 0;}
#endif

int printStat(const DepotStat& s);

int printCurStat();

int reportStat();

int eraseStat();

/** @} */




/** @} */
/** @} */

#endif

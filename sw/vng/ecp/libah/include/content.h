

#pragma once

#include <string>
using namespace std;

#include "comm.h"

// from depot.h ***********************************************

template <class CALLER_DATA, class RETURN_DATA>
struct Callback {
    typedef void (*FuncType)(CALLER_DATA, RETURN_DATA);
    CALLER_DATA caller_data;
    FuncType func;

    void operator()(RETURN_DATA& d) { if (func) (*func)(caller_data, d); }

    Callback():func(NULL) {}
    Callback(FuncType f, CALLER_DATA c):caller_data(c),func(f) {}
    ~Callback() {}
};

typedef Callback<void*,unsigned> Progress;

int downloadContentObj(Comm& c,
                       const string& url,
                       const string& outfile,
		       ulong offset,
                       ulong& out_content_size, // from hdr
                       ulong& out_size_so_far,
                       ulong& out_size_final,
                       int&   out_http_status,
		       string& checksum,
		       bool* interrupted);

/**
   Create a filename using the cached files hierarchy 
   @param dirname - cache directory
   @param filename - base-name of the file to be created
   @param outfile - the path of the cache file
   @param createdir - if true, create the cache dir if necessary
*/
int makeCacheID(const string& dirname, const string& filename,
                string& outfile, bool createdir);

/**
   Check if the content is cached
   @param dir the cache directory
   @param chksum md5 checksum of the content
   @param fsize expected size of the content file
   @param cache_id name of the cached file
   @param outsize actual size of the content file, that might be partially downloaded
   @return 1 if content is found in cache,
           0 if content is not found
           -1 if error
*/
extern int checkCacheID(const string& dir,
                        const string& chksum,
                        off_t fsize, 
                        string& cache_id,
                        off_t *outsize=NULL);

/** Copy the content object file
 */
int copyContentObj(const string& infile, const string& outfile, Progress pg);



// end of from depot.h **************************************




#pragma once

#include "ThreadWTO.h"
// #include "BBPlayer.h"

#define DEF_PROGRESS_INC_AMT  1

enum  initHttpStatus { initHttp_NotTried,
                       initHttp_Trying,
                       initHttp_OK,
                       initHttp_Err,
                       initHttp_ReOpenErr };

// class ExOp - An extended (i.e. over time) operation

// DANGER !!!!!
//
// If isDone() returns false, the only aspects of
// an ExpOp or descendent that can be accessed
// are getProgress() and updateProgress ()


class ExOp : public ThreadWTO {

  protected:

    CRITICAL_SECTION pg_cs;

    /* The thread should set _progress values 0 ->99 via setProgress()
     * Successful completion and errors (100 or greater) should only
     * be set in _result. _result will be processed after thread exits.*/

    double _pg_inc_amt;
    double _pg;

    int    _progress;
    int    _maxProgress;
    int    _cycleValue;
    int    _result;
    int    _timeout_err_code;
    int    _non_specific_op_fail_code;

    bool   _cancel;  // if true, thread should cleanup and exit
                     // at earliest opportunity
    bool   _unlinkOnCancel; // if true, remove file on cancel

    virtual void updateProgress ();
    void setProgress (int progress);
    void setProgress (int progress, double pg_inc_amt);
    void setResult   (int result) {_result = result;}
    virtual void checkFinishedWork ();
#if 0
    // bbp must be locked when checkState() is called
    int checkState(BBPlayer& bbp, string& playerID);
#endif

  public:

    ExOp (const char*   name,
          ThreadWTOFunc work,
          int           maxTime,
          HWND          hWnd,
          int           monitorInterval,
          int           timeout_err_code,
          int           non_specific_op_fail_code,
          double        pg_inc_amt = DEF_PROGRESS_INC_AMT);

    virtual ~ExOp ();

    virtual int getProgress ();
    virtual bool isDone ();
    virtual int cancel (bool unlink = false);
};

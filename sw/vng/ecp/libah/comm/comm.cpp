/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */

//#include <iostream>
//#include <time.h>
//#include <set>
//#include <vector>
//#include <functional>
//#include "common.h"

#include "comm.h"



#ifndef _WIN32
    #include <unistd.h>
    #include <sys/file.h>
#else
   #include "common.h"
   #include "getahdir.h"
   #include "md5.h"
   #include "IAH_errcode.h"
   #include "App.h"
#endif

using namespace std;

//#include <zlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "config_var.h"
#include "comm.h"
//#include "xml_container.h"
#include "http.h"
#if 0
#include "depot.h"
#include "util.h"
#endif
#include "sysclnt.h"

#ifndef _WIN32
    #include "ssl_socket.h"
    #include "securemsg.h"
    #include "schema.h"

    #include <openssl/md5.h>        // MD5 functions
    #include <ext/rope>
    /** GNU extension namespace.  Needed for crope */
    using namespace __gnu_cxx;   
#endif





Comm::Comm()
{
    char buf[1024];
    verbose = false;
    if (sys_getconf(CONFIG_COMM_VERBOSE, buf, sizeof(buf)) == 0) {
        if (atoi(buf) != 0)
            verbose = true;
    }
}




static __int64
ntohll(unsigned char* p)
{
    if (ntohl(1) == 1) {
	// no swapping needed
	return *((__int64*) p);
    } else {
	__int64 result = 0;
	for (int i = 0; i < 8; ++i) {
	    result = (result << 8) | p[i];
	}
	return result;
    }
}
    

static int
parse_content_header(const FileObj& fileobj,
		     ulong& chunk_size,
		     ulong& content_size,
		     string& md5sum)
{
    unsigned char buf[48];
    lseek(fileobj.fd(), 0L, SEEK_SET);
    if (fileobj.read(buf, sizeof(buf)) != sizeof(buf))
	return -1;

    // check magic number
    static unsigned char magic[8] = { 0x1c, 0x2c, 0x3c, 0x4c, 0x5c, 0x6c, 0x7c, 0x8c };
    for (int i = 0; i < sizeof(magic); ++i) {
	if (magic[i] != buf[i])
	    return -1;
    }

    // read chunksize
    int offset = 12;
    chunk_size = ntohl(*((ulong*) (buf + offset)));
    
    // read content size
    offset += 4;
    content_size = (ulong) ntohll(buf + offset);

    // read md5 check sum (skipping extra 8 bytes for content id)
    offset += 16;
    return (hex_encode(buf + offset, 16, md5sum) == 32) ? 0 : -1;
} // parse_content_header
    

static int
downloadContentHeader(Comm& c,
		      const string& url,
		      const string& fname,
		      ulong& chunk_size,
		      ulong& content_size,
                      int&   out_http_status,
		      string& md5sum,
		      bool* interrupted)
{
    string url_hdr(url + "?offset=0&chunksize=0&getheader");
    FileObj fileobj(fname.c_str(), O_CREAT|O_TRUNC|O_RDWR|_O_BINARY);
    if (fileobj.fd() < 0)
	return -1;
    fileobj.set_interrupted(interrupted);

    out_http_status = 0;
    string hdr;
    int rval = HTTP_Get(c, url_hdr.c_str(), out_http_status, hdr, fileobj);

    if (rval != 0)
	return rval;

    return parse_content_header(fileobj, chunk_size, content_size, md5sum);
} // downloadContentHeader

#ifdef _WIN32
#define FTRUNCATE _chsize
#else
#define FTRUNCATE ftruncate
#endif // _WIN32    
				 
int downloadContentObj(Comm& c,
                       const string& url,
                       const string& fname,
		       ulong offset,
                       ulong& out_content_size,
                       ulong& out_size_so_far,
                       ulong& out_size_final,
                       int&   out_http_status,
		       string& md5sum,
		       bool* interrupted)
{
    time_t start_time = time(NULL);

    string hdr_file(fname + ".hdr");
    int rval;
    string hdr;
    ulong chunk_size;
    ulong content_size;
    out_content_size = 0;
    out_size_so_far = 0;
    out_size_final = 0;
    out_http_status = 0;

    rval = downloadContentHeader(c, url, hdr_file, chunk_size, content_size,
				 out_http_status, md5sum, interrupted);
    unlink(hdr_file.c_str());

    if (rval == 0) {
        out_content_size = content_size;
	FileObj fileobj(fname.c_str(), O_CREAT|O_WRONLY|_O_BINARY);
	if (fileobj.fd() < 0)
	    return -1;
	fileobj.set_interrupted(interrupted);

	if ((offset % chunk_size) != 0) {
	    // misalign, truncate the file back to multiple of chunk size
	    offset -= (offset % chunk_size);
	    rval = FTRUNCATE(fileobj.fd(), offset);
	}

	lseek(fileobj.fd(), offset, SEEK_SET);

	string chunk_param("&chunksize=" + tostring(chunk_size));
	
	while (rval == 0 && offset < content_size) {
	    // check for misaligned file size, which could be a result of a
	    // previous incomplete download
	    if (offset % chunk_size != 0) {
		rval = -1;
		break;
	    }

	    string chunk_url(url + "?offset=" + tostring(offset) + chunk_param);
	    out_http_status = 0;
	    rval = HTTP_Get(c, chunk_url.c_str(), out_http_status, hdr, fileobj);
	    offset = fileobj.size();
            out_size_so_far = offset;
	}

	out_size_final = fileobj.size();
    }


    if (rval == 0) {
        double speed = out_size_final / (time(NULL) - start_time + 0.25);
        recordStat(CDS_DOWNLOAD_SPEED, speed);
    } else
        recordError(CDS_DOWNLOAD_SPEED);
    return rval;
}




#include <sc/ios.h>
#include <rm.h>
#include <rm_jt.h>

typedef struct {
    IOSIoVector       read;
    IOSIoVector       write;
} VOICE_VECTOR;

static VOICE_ARG inBuf  __attribute__((section(".data")));
static VNGErrCode outBuf  __attribute__((section(".data")));
static VOICE_VECTOR vector __attribute__((section(".data")));

static IOSFd fd = -1;


VNGErrCode callIoctlv(u32 cmd)
{
    IOSIoVector *v = (IOSIoVector*)&vector;
    IOSError err;
    int i;

    if (fd < 0) {
        while ((fd = IOS_Open(VOICE_DEV, 0)) < 0) {
            for (i=0; i<10000; i++);
        }

    }

    vector.read.base = (u8*)&inBuf;
    vector.read.length = sizeof(VOICE_ARG);
    vector.write.base = (u8*)&outBuf;
    vector.write.length = sizeof(VNGErrCode);

    err = IOS_Ioctlv(fd, cmd, 1, 0, v);

    if (err) {
        return err;
    } else {
        return outBuf;
    }
}

LIBVNG_API VNGErrCode VNG_ActivateVoice(VN       *vn,
                                        int32_t   what,
                                        VNMember  mem)
{
    ACTIVATE_VOICE_ARG* a = &inBuf.args.activate;

    VN_GetVNId(vn, &a->vnid);
    a->what = what;
    a->mem = mem;
    
    return callIoctlv(VOICE_FI_ACTIVATEVOICE);
}

LIBVNG_API VNGErrCode VNG_AdjustVolume(VNG     *vng,
                                       int32_t  what,
                                       int32_t  volume)
{
    ADJUST_VOLUME_ARG* a = &inBuf.args.adjust;
    a->vng = vng;
    a->what = what;
    a->volume = volume;
    
    return callIoctlv(VOICE_FI_ADJUSTVOLUME);
}

LIBVNG_API VNGErrCode VNG_SelectVoiceCodec(VNG     *vng,
                                           int32_t  codec)
{
    SELECT_VOICE_CODEC_ARG* a = &inBuf.args.select;
    a->vng = vng;
    a->codec = codec;
    
    return callIoctlv(VOICE_FI_SELECTVOICECODEC);
}

#include <stdlib.h>
#include "vng.h"
#include "g726_clean.h"
#include "shr_th.h"
#include "shr_trace.h"
#include "platform.h"
#include "internal.h"

//***********
// CONSTANTS
//***********

#define JITTER_BUF_LENGTH   16
#define NUM_PEERS           3 // 4 total players
#define VAD_THRESHOLD       0xF800
#define JITTER_THRESHHOLD   3
#define SILENT_PERIOD       30

int codec_bitrate[2] = {
    16000, // VNG_VOICE_CODEC_ADPCM16
    32000, // VNG_VOICE_CODEC_ADPCM32
};
int codec_compression[2] = {
    8, // VNG_VOICE_CODEC_ADPCM16
    4, // VNG_VOICE_CODEC_ADPCM32
};


//******************
// GLOBAL VARIABLES
//******************
int __threadStop = 0;
int initialized = 0;
int enabled = 0;
VN* groupVn = NULL;
VNMember groupMember;

int channelEnabled[NUM_PEERS];
unsigned int expSeqNum[NUM_PEERS];

PlaybackBuffer playbackBuffers[NUM_PEERS][JITTER_BUF_LENGTH];
PlaybackBuffer* freeBufferList[NUM_PEERS];
PlaybackBuffer* jitterBufferList[NUM_PEERS];

char tmpDecodeBuf[NUM_PEERS][MAX_PLAYBACK_BUF];

XmitData tmpXmitData;
unsigned int mySeqNum;
int mySilenceMode;
int currCoder;
int currDecoder[NUM_PEERS];

AVCodecContext decodeContext[NUM_PEERS];
AVG726Context decodeG726Context[NUM_PEERS];
AVCodecContext encodeContext;
AVG726Context encodeG726Context;

_SHRMutex mutex;

//**********************
// FUNCTION DECLARATION
//**********************
int isEmpty(PlaybackBuffer* list);
int getLength(PlaybackBuffer* list);
PlaybackBuffer* popFront(PlaybackBuffer** list);
void push(PlaybackBuffer** list, PlaybackBuffer* item);
int addToJitterBuffer(int peerId, PlaybackBuffer* playBuf);
int doJitterControl();
int mixChannels(char* outBuf, int size);
void resetBuffers();
void resetDecoder(int peerId, int newCoder);


//**********************
// FUNCTION DEFINITIONS
//**********************

int acquireMutex()
{
	return _SHR_mutex_lock(&mutex);
}

int releaseMutex()
{
	return _SHR_mutex_unlock(&mutex);
}

int isEmpty(PlaybackBuffer* list)
{
    int retval;

    acquireMutex();

    retval = (list == NULL);

    releaseMutex();

    return retval;
}

int getLength(PlaybackBuffer* list)
{
    int retval = 0;
    PlaybackBuffer* tmp = list;

    acquireMutex();

    while (tmp != NULL) {
        retval++;
        tmp = tmp->next;
    }

    releaseMutex();

    return retval;
}

PlaybackBuffer* popFront(PlaybackBuffer** list)
{
    PlaybackBuffer* retval;

    acquireMutex();

    if (*list == NULL) {
        retval = NULL;
    } else {
        retval = *list;
        *list = (*list)->next;
    }

    releaseMutex();

    return retval;
}

void push(PlaybackBuffer** list, PlaybackBuffer* item)
{
    PlaybackBuffer* tmp;

    acquireMutex();

    tmp = *list;
    *list = item;
    item->next = tmp;

    releaseMutex();
}

int makeXmitBuf(char* recordBuf, int recordSize,
                void* outBuf, unsigned int* outSize)
{
    short *samples = (short*)recordBuf;
    short tmp;
    int signalFound = false;
    int i;
    int dataSize;
    XmitData *outXmitBuf = (XmitData*)outBuf;

    if (!enabled) {
        *outSize = 0;
        return 0;
    }

#ifndef _NO_CODEC_
    dataSize = recordSize/codec_compression[currCoder];
#else
    dataSize = recordSize;
#endif

    if (*outSize < sizeof(XmitDataHdr)+dataSize) {
        _SHR_trace(TRACE_ERROR, _TRACE_APP, 0,
                   "makeXmitBuf() - outSize:%d < %d",
                   *outSize, sizeof(XmitDataHdr)+dataSize);
        return -1;
    }

    // VAD
    for (i=0; i<recordSize >> 1; i++) {
        tmp = *samples;
        if ( tmp < 0 )
            tmp = -tmp;
        if (tmp & VAD_THRESHOLD) {
            signalFound = 1;
            break;
        }
        samples++;
    }
    if (!signalFound && mySilenceMode) {
        // If no signal found and already in VAD silence,
        // do nothing.
        *outSize = 0;
        return 0;
    } else {
        mySilenceMode = !signalFound;
    }

    // Compress 16 bit audio to 2 bit g726
#ifndef _NO_CODEC_
    g726_encode_frame(&encodeContext, outXmitBuf->encodedData, 
        recordSize >> 1, recordBuf);
#else
    memcpy(&outXmitBuf->encodedData, recordBuf, recordSize);
#endif

	// setup transmission pkt
    outXmitBuf->hdr.seqNum = mySeqNum;
    outXmitBuf->hdr.silenceMode = mySilenceMode;
    outXmitBuf->hdr.codec = currCoder;
    outXmitBuf->hdr.dataSize = dataSize;
    mySeqNum++;

    *outSize = sizeof(XmitDataHdr)+dataSize;

    return 0;
}

// recorded data from mic
int gotRecordBuf(char* buf, int size)
{
    int xmitSize = sizeof(tmpXmitData);
    VNGErrCode ec;

    makeXmitBuf(buf, size, (char*)&tmpXmitData, &xmitSize);
    if (xmitSize > 0) {
        // Transmit data to all peers
        ec = VN_SendMsg (groupVn, groupMember, VN_TAG_VOICE,
            &tmpXmitData,
            sizeof(XmitDataHdr)+tmpXmitData.hdr.dataSize, 
            VN_ATTR_UNRELIABLE|VN_ATTR_NOENCRYPT, VNG_NOWAIT);
    }

    return 0;
}

// precond - only one thread may call at a time
int onNetRecv(int peerId, XmitData* xmitBuf)
{
    PlaybackBuffer* freeBuf;

    acquireMutex();

    // Drop out of sequence packets:
    // Allow some pkts of seqNum syncronization, in silence
    if (xmitBuf->hdr.seqNum > SILENT_PERIOD &&
        xmitBuf->hdr.seqNum < expSeqNum[peerId]) 
    {
        releaseMutex();
        return 0;
    }

    freeBuf = popFront(&freeBufferList[peerId]);

    if (freeBuf == NULL) {
        // No more free bufs
        releaseMutex();
        return -1;
    }

    freeBuf->seqNum = xmitBuf->hdr.seqNum;
    freeBuf->codec = xmitBuf->hdr.codec;
    freeBuf->next = NULL;
    freeBuf->dataSize = xmitBuf->hdr.dataSize;
    memcpy(freeBuf->encodedData, xmitBuf->encodedData, xmitBuf->hdr.dataSize);

    // If we received VAD silence mode, enable this channel,
    // so that Jitter Buffer can empty since the stream is now on pause.
    if (xmitBuf->hdr.silenceMode) {
        channelEnabled[peerId] = true;
        _SHR_trace(TRACE_FINE, _TRACE_APP, 0, "onNetRecv() - channel[%d] enabled\n", peerId);
    }

    addToJitterBuffer(peerId, freeBuf);

    releaseMutex();

    return 0;
}

// get next sound data to play out
int getNextPlayBuf(char* outBuf, int size)
{
    acquireMutex();

    doJitterControl();

    mixChannels(outBuf, size);

    releaseMutex();

    return 0;
}

// Precond: has mutex
int mixChannels(char* outBuf, int size)
{
    // if none available in jitter buffer, silence
    int channels = 0;
    int i, j;
    PlaybackBuffer* tmpBuf;
    short *src[NUM_PEERS];
    short *dst;
    int tmp;
    int decodedSize;

    for (i=0; i<NUM_PEERS; i++) {
        if (channelEnabled[i]) {
            tmpBuf = popFront(&(jitterBufferList[i]));

            // Reset decoder if changed
            if (currDecoder[i] != tmpBuf->codec) {
                resetDecoder(i, tmpBuf->codec);
            }
            
            // Decode into temp buf
#ifndef _NO_CODEC_
            g726_decode_frame(&decodeContext[i],
                              (void*)&tmpDecodeBuf[channels], &decodedSize,
                              tmpBuf->encodedData, tmpBuf->dataSize);
#else
            memcpy(&tmpDecodeBuf[channels], tmpBuf->encodedData, tmpBuf->hdr.dataSize);
            decodedSize = tmpBuf->hdr.dataSize);
#endif           

            expSeqNum[i] = tmpBuf->seqNum + 1;
            push(&freeBufferList[i], tmpBuf);
            if (size != decodedSize) {
                _SHR_trace(TRACE_ERROR, _TRACE_APP, 0,
                    "mixChannels() - size:%d > playBuf:%d\n", size, tmpBuf->dataSize);
                return -1;
            }

            if (tmpBuf->seqNum < SILENT_PERIOD) {
                continue;
            }

            src[channels] = (short*)&tmpDecodeBuf[channels];
            channels++;
        }
    }

    dst = (short*)outBuf;
    size = size >> 1; // 16 bit samples
    for (i=0; i<size; i++) {
        tmp = 0;
        for (j=0; j<channels; j++) {
            tmp += (int)*(src[j]);
            src[j]++;
        }
        if (channels) {
            tmp = tmp/channels;
        }
        *dst = (short)tmp;
        dst++;
    }

    return 0;
}

// Precond: has mutex
int doJitterControl()
{
    int i;

    for (i=0; i < NUM_PEERS; i++) {
		// Disable channel if we run out of buffers
        if (channelEnabled[i] == true &&
            isEmpty(jitterBufferList[i])) {
            channelEnabled[i] = false;
            _SHR_trace(TRACE_FINE, _TRACE_APP, 0, "doJitterControl() - channel[%d] disabled\n", i);
        } else {
            // Enable channel if buffer fill to Jitter Threshhold
            if (channelEnabled[i] == false &&
                getLength(jitterBufferList[i]) > JITTER_THRESHHOLD) {
                channelEnabled[i] = true;
                _SHR_trace(TRACE_FINE, _TRACE_APP, 0, "doJitterControl() - channel[%d] enabled\n", i);
            }
        }
    }

    return 0;
}

// precond - has mutex
int addToJitterBuffer(int peerId, PlaybackBuffer* playBuf)
{
    PlaybackBuffer** ptr;

	// search peer-indexed jitter buffer for insertion point
    ptr = &jitterBufferList[peerId];
    while(1) {
        if (*ptr == NULL) {
            // Reached end
            playBuf->next = NULL;
            *ptr = playBuf;
            break;
        } else if ((*ptr)->seqNum == playBuf->seqNum) {
            // Duplicate
            push(&freeBufferList[peerId], playBuf);
            break;
        } else if ((*ptr)->seqNum > playBuf->seqNum) {
            // Found - insert into linked list
            playBuf->next = *ptr;
            *ptr = playBuf;
            break;
        }
        ptr = &((*ptr)->next);
    }

    return 0;
}

// Map VNMember to array index
int getPeerId(VNMember sender)
{
    static VNMember peers[NUM_PEERS];
    static int nextIndex = 0;
    int i;
    int retVal = 0;

    for (i=0; i<NUM_PEERS; i++) {
        if (peers[i] == sender) {
            retVal = i;
            break;
        }
    }

    if (i == NUM_PEERS) {
        retVal = nextIndex;
        peers[nextIndex] = sender;
        nextIndex = (nextIndex+1)%NUM_PEERS;
    }

    return retVal;
}

#define TIMEOUT_RECV_MSG 1000
void netRecvThread()
{
    VNGErrCode ec;
    XmitData xmitBuf;
    char* msg = (char*)&xmitBuf;
    size_t msglen;
    VNMsgHdr hdr;
    int peerId;

    while (1) {
        msglen = sizeof(xmitBuf);
        ec = VN_RecvMsg(groupVn, groupMember, VN_TAG_VOICE, 
            msg, &msglen, &hdr, TIMEOUT_RECV_MSG);

        if (__threadStop) {
            __threadStop = 0;
            break;
        }

        if (ec != VNG_OK) {
            if (ec != VNGERR_TIMEOUT) {
                _SHR_thread_sleep(TIMEOUT_RECV_MSG);
            }
            continue;
        }

        if (!enabled) {
            continue;
        }

#ifdef _RAND_DROP_
#define DROP_RATE 0.05
        if (rand() < (DROP_RATE * RAND_MAX)) {
            continue;
        }
#endif

        if (msglen < sizeof(XmitDataHdr) ||
            msglen != sizeof(XmitDataHdr) +
            xmitBuf.hdr.dataSize) {
            continue;
        }

        peerId = getPeerId(hdr.sender);

#ifdef _RAND_REORG_
        {
            static XmitData prevXmitBuf; 
            static int prevDelayed = false;
            if (prevDelayed) {
                onNetRecv(peerId, &xmitBuf);
                onNetRecv(peerId, &prevXmitBuf);
                prevDelayed = false;
                continue;
            } else if ((rand() < (.10 * RAND_MAX))) {
                memcpy(&prevXmitBuf, &xmitBuf, sizeof(xmitBuf));
                prevDelayed = true;
                continue;
            }
        }
#endif

        onNetRecv(peerId, &xmitBuf);
    }
}

// precond - has mutex
void resetCoder(int newCoder)
{
    currCoder = newCoder;

    encodeContext.priv_data = &encodeG726Context;
    encodeContext.sample_rate = SAMPLE_RATE;
    encodeContext.channels = 1;
    encodeContext.bit_rate = codec_bitrate[currCoder];
    g726_init(&encodeContext);

    mySilenceMode = true;
    mySeqNum = 0;
}

// precond - has mutex
void resetDecoder(int peerId, int newCoder)
{
    currDecoder[peerId] = newCoder;
    decodeContext[peerId].priv_data = &decodeG726Context[peerId];
    decodeContext[peerId].sample_rate = SAMPLE_RATE;
    decodeContext[peerId].channels = 1;
    decodeContext[peerId].bit_rate = codec_bitrate[currDecoder[peerId]];
    g726_init(&decodeContext[peerId]);
}

// precond - has mutex
void initCodec()
{
    int i;

    resetCoder(VNG_VOICE_CODEC_ADPCM32);

    for (i=0; i<NUM_PEERS; i++) {
        resetDecoder(i, VNG_VOICE_CODEC_ADPCM32);
    }
}

// precond - has mutex
void resetBuffers()
{
    int i,j;

    for (i=0; i<NUM_PEERS; i++) {
        channelEnabled[i] = false;
        expSeqNum[i] = 0;
        
        jitterBufferList[i] = NULL;
        // Add all playbufs to free list
        for (j=0; j<JITTER_BUF_LENGTH; j++) {
            push(&freeBufferList[i], &playbackBuffers[i][j]);
        }
    }
}

// precond - init stage
void initVoice(int startThreads)
{
    //_SHR_set_sg_trace_level (_TRACE_APP, 0, FINE);

    // init mutex to unlocked state
    _SHR_mutex_init(&mutex, _SHR_MUTEX_RECURSIVE, 0 /* XXX pIdMask */, 0);
    
    // Initialize codec
    initCodec();

    // Reset Buffers
    resetBuffers();

    // start audio and network receiver threads
    if (startThreads) {
        initPlatform();
    } else {
        // Used by LCE
        enabled = 1;
    }

    initialized = 1;
}

// precond - initialized
void deinit()
{
    int ec;

    __threadStop = 1;

    // stop audio process
    if ( (ec = deinitPlatform()) != 0 ) {
        _SHR_trace(TRACE_ERROR, _TRACE_APP, 0, "libvoice:deinit: unable to stop audio, ec = %d\n",ec);
    }

    // all threads stopped, destroy mutex
    if ( (ec = _SHR_mutex_destroy(&mutex)) != _SHR_ERR_OK ) {
        _SHR_trace(TRACE_ERROR, _TRACE_APP, 0, "libvoice:deinit: unable to destroy mutex, ec = %d\n",ec);
    }

    enabled = 0;
    initialized = 0;
}

//*******************
// EXPOSED FUNCTIONS
//*******************

// If mem is VN_MEMBER_OTHERS, new VN members are
// automatically included in the voice broadcast.
//
// what can be VNG_VOICE_ENABLE or VNG_VOICE_DISABLE
//
LIBVNG_API VNGErrCode VNG_ActivateVoice(VN       *vn,
                                        int32_t   what,
                                        VNMember  mem)
{
    if (enabled && what == VNG_VOICE_ENABLE) {
        return VNGERR_VOICE_IN_USE;
    }

    if (what == VNG_VOICE_DISABLE) {
        enabled = false;
    } else if (what == VNG_VOICE_ENABLE) {
        groupVn = vn;
        groupMember = mem;
        enabled = true;

        if (!initialized) {
            initVoice(1); // start threads
        }
    }

    return VNG_OK;
}

// Adjust voice volume
//
// what can be VNG_VOL_SET, VNG_VOL_INCREMENT, or VNG_VOL_DECREMENT
// volume is 
//
LIBVNG_API VNGErrCode VNG_AdjustVolume(VNG     *vng,
                                       int32_t  what,
                                       int32_t  volume)
{
#ifdef _SC
    int ec = 0;
    int v;
    
    switch (what) {
    case VNG_VOL_SET:
        ec = setVolume(volume);
        break;
    case VNG_VOL_INCREMENT:
        if ( (ec = getVolume(&v)) >= 0 ) {
            int minVolume = getMinVolume();
            int maxVolume = getMaxVolume();
            int muteVolume = getMuteVolume();
            v = ec;
            if ( v == muteVolume ) {
                v = minVolume;
            } else {
                if ( ++v > maxVolume )
                    v = maxVolume;
            }
            ec = setVolume(v);
        }
        break;
    case VNG_VOL_DECREMENT:
        if ( (ec = getVolume(&v)) >= 0 ) {
            int minVolume = getMinVolume();
            int muteVolume = getMuteVolume();
            v = ec;
            if ( v == minVolume || v == muteVolume ) {
                v = muteVolume;
            } else {
                if ( --v < minVolume )
                    v = minVolume;
            }
            ec = setVolume(v);
        }
        break;
    default:
        _SHR_trace(TRACE_ERROR, _TRACE_APP, 0, "VNG_AdjustVolume: invalid arg, what = %d\n",what);
        break;
    }
    
    return ec == 0 ? VNG_OK : VNGERR_INVALID_ARG;
#else
    return VNG_OK;
#endif
}

// Select codec
//
// The default codec is ADPCM16. VNG allows the application to select
// the codec.   Parameters of the codec, such as voice activation
// detection, jitter buffering, distribution topology are determined
// automatically.
//
LIBVNG_API VNGErrCode VNG_SelectVoiceCodec(VNG     *vng,
                                           int32_t  codec)
{
    if (codec < 0 || codec > VNG_VOICE_CODEC_ADPCM32) {
        return VNGERR_INVALID_ARG;
    }

    acquireMutex();

    resetCoder(codec);

    releaseMutex();

    return VNG_OK;
}



/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */

#ifndef __VOICE_RM_H__
#define __VOICE_RM_H__  1

#if !defined(_GBA) && !defined(_SC)
#pragma pack(push, 4)
#endif

#include "vng.h"


#define VOICE_DEV "/dev/voice"

typedef struct {
    VN* vn;
    VNId vnid;
    int what;
    VNMember mem;
} ACTIVATE_VOICE_ARG;

typedef struct {
    VNG* vng;
    int what;
    int volume;
} ADJUST_VOLUME_ARG;

typedef struct {
    VNG* vng;
    int codec;
} SELECT_VOICE_CODEC_ARG;

typedef struct {
    union {
        ACTIVATE_VOICE_ARG activate;
        ADJUST_VOLUME_ARG adjust;
        SELECT_VOICE_CODEC_ARG select;
    } args;
} VOICE_ARG;

void deinit();

#if !defined(_GBA) && !defined(_SC)
#pragma pack(pop)
#endif

#endif /* __VOICE_RM_H__ */

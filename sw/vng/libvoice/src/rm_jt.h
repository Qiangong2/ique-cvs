/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */

#ifndef __VOICE_RM_JT_H__
#define __VOICE_RM_JT_H__

#define VOICE_RM_FUNC_ID_MIN                  1001

#define VOICE_FI_ACTIVATEVOICE                1001
#define VOICE_FI_ADJUSTVOLUME                 1002
#define VOICE_FI_SELECTVOICECODEC             1003

#define VOICE_RM_FUNC_ID_MAX                  1003


#ifndef _GBA
#include <sc/ios.h>
#include <rm.h>
typedef void (*VOICERmApiWrapper) (VOICE_ARG* a);

VOICERmApiWrapper  _voice_rm_getApiWrapper (u32 func_id);
#endif


#endif /*  __VOICE_RM_JT_H__ */





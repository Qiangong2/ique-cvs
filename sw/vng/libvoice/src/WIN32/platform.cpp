#include <windows.h> 
#include <windowsx.h> 
#include <stdlib.h> 
#include <mmsystem.h> 
#include <mmreg.h>
#include "platform.h"
#include "internal.h"

#define NUM_BLOCKS 4
#define NUM_SAMPLES         256
#define BLOCK_SIZE          (NUM_SAMPLES * SAMPLE_SIZE)

class CSendBuffer 
{
public:
    WAVEHDR m_WaveHeader;    // wave header for the buffer 
    BYTE m_recData[BLOCK_SIZE];

    MMRESULT Prepare(HWAVEIN hWaveIn) 
    {

        // Prepare for playback 
        ZeroMemory(&m_WaveHeader, sizeof(m_WaveHeader));
        m_WaveHeader.dwBufferLength = BLOCK_SIZE;
        m_WaveHeader.lpData = (char*)(m_recData);

        m_WaveHeader.dwUser = (DWORD)this;
        return waveInPrepareHeader(hWaveIn, &m_WaveHeader, sizeof(m_WaveHeader));
    }
    MMRESULT Unprepare(HWAVEIN hWaveIn) 
    {
        return waveInUnprepareHeader(hWaveIn, &m_WaveHeader, sizeof(m_WaveHeader));
    }
    MMRESULT Add(HWAVEIN hWaveIn) 
    {
        return waveInAddBuffer(hWaveIn, &m_WaveHeader, sizeof(m_WaveHeader));
    }
};
class CRecvBuffer 
{
public:
    WAVEHDR m_WaveHeader;
    BYTE m_playData[BLOCK_SIZE];
    MMRESULT Prepare(HWAVEOUT hWaveOut) 
    {
        ZeroMemory(&m_WaveHeader, sizeof(m_WaveHeader));
        m_WaveHeader.dwBufferLength = BLOCK_SIZE;
        m_WaveHeader.lpData = (char*)(m_playData);
        m_WaveHeader.dwUser = (DWORD)this;
        return waveOutPrepareHeader(hWaveOut, &m_WaveHeader, sizeof(m_WaveHeader));
    }
    MMRESULT Unprepare(HWAVEOUT hWaveOut) 
    {
        return waveOutUnprepareHeader(hWaveOut, &m_WaveHeader, sizeof(m_WaveHeader));
    }
    MMRESULT Add(HWAVEOUT hWaveOut) 
    {
        return waveOutWrite(hWaveOut, &m_WaveHeader, sizeof(m_WaveHeader));
    }
};


HWAVEIN m_hWaveIn;    // Handle to capture device 
HWAVEOUT m_hWaveOut;    // Handle to playback device 

CSendBuffer m_aInBlocks[NUM_BLOCKS];    // Capture bufs 
CRecvBuffer m_aOutBlocks[NUM_BLOCKS];    // Playback bufs 

void CALLBACK waveOutProc(
  HWAVEOUT hwo,      
  UINT uMsg,         
  DWORD_PTR dwInstance,  
  DWORD dwParam1,    
  DWORD dwParam2     
);
void CALLBACK waveInProc(
  HWAVEIN hwi,       
  UINT uMsg,         
  DWORD dwInstance,  
  DWORD dwParam1,    
  DWORD dwParam2     
);
void OnWomOpen(HWAVEOUT hwo);
void OnWimOpen(HWAVEIN hwi);


DWORD WINAPI __netRecvThread( LPVOID lpParam ) 
{
    netRecvThread();

    return 0;
}

int deinitPlatform()
{
    return 0;
}

int initPlatform()
{
    int retVal = 0;

    // Open sound devices
    PCMWAVEFORMAT WaveFormatPCM;
    MMRESULT mmRC;

    WaveFormatPCM.wf.wFormatTag = WAVE_FORMAT_PCM;
    WaveFormatPCM.wf.nChannels = 1;
    WaveFormatPCM.wf.nSamplesPerSec = SAMPLE_RATE;
    WaveFormatPCM.wf.nAvgBytesPerSec = SAMPLE_RATE*2;
    WaveFormatPCM.wf.nBlockAlign = 2;
    WaveFormatPCM.wBitsPerSample = 16;

    mmRC = waveOutOpen(&m_hWaveOut, (UINT)WAVE_MAPPER, 
        (LPWAVEFORMATEX)&(WaveFormatPCM), (DWORD)waveOutProc, (DWORD)NULL, CALLBACK_FUNCTION);
    if (mmRC != MMSYSERR_NOERROR) { // Error opening wave playback device
        retVal = -1;
    } else {
        waveOutReset(m_hWaveOut);
    }
    mmRC = waveInOpen(&m_hWaveIn, (UINT)WAVE_MAPPER, 
        (LPWAVEFORMATEX)&(WaveFormatPCM), (DWORD)waveInProc, (DWORD)NULL, CALLBACK_FUNCTION);
    if (mmRC != MMSYSERR_NOERROR) { // Unable to open wave input device
        retVal = -1;
    } else {
        waveInReset(m_hWaveIn);
        waveInStart(m_hWaveIn);
    }

    OnWimOpen(m_hWaveIn);
    OnWomOpen(m_hWaveOut);

    CreateThread(NULL, 0, __netRecvThread, NULL, 0, NULL);

    return retVal;
}

void OnWimData(HWAVEIN hwi, WAVEHDR *pHdrWave) 
{
    CSendBuffer *pAudioBuffer;        // pointer to the wave buffer
    pAudioBuffer = (CSendBuffer*)(pHdrWave->dwUser);

    // Unlink the buffer from the capture device 
    pAudioBuffer->Unprepare(hwi);

    gotRecordBuf((char*)pAudioBuffer->m_recData, pHdrWave->dwBytesRecorded);

    pAudioBuffer->Prepare(hwi);
    pAudioBuffer->Add(hwi);
}

void OnWomDone(HWAVEOUT hwo, WAVEHDR *pHdrWave) 
{
    CRecvBuffer *pBuffer;

    pBuffer = (CRecvBuffer*)(pHdrWave->dwUser);
    pBuffer->Unprepare(hwo);

    getNextPlayBuf((char*)pBuffer->m_playData, sizeof(pBuffer->m_playData));

    pBuffer->Prepare(hwo);
    pBuffer->Add(hwo);
}

void OnWimOpen(HWAVEIN hwi) 
{
    // prime n-tuple buffer 
    for (int i = 0;
        i < NUM_BLOCKS;
        i++) 
    {

        // prepare and add blocks to capture device queue 
        m_aInBlocks[i].Prepare(hwi);
        m_aInBlocks[i].Add(hwi);
    }
}

void OnWomOpen(HWAVEOUT hwo) 
{
    // prime n-tuple buffer 
    for (int i = 0;
        i < NUM_BLOCKS;
        i++) 
    {
        getNextPlayBuf((char*)m_aOutBlocks[i].m_playData, sizeof(m_aOutBlocks[i].m_playData));

        m_aOutBlocks[i].Prepare(hwo);
        m_aOutBlocks[i].Add(hwo);
    }
}

void CALLBACK waveOutProc(
                          HWAVEOUT hwo,      
                          UINT uMsg,         
                          DWORD_PTR dwInstance,  
                          DWORD dwParam1,    
                          DWORD dwParam2     
                          )
{
    switch(uMsg)
    {
    case MM_WOM_DONE: OnWomDone(hwo, (WAVEHDR*)dwParam1);
        break;
    //case MM_WOM_OPEN:
    //    break;
    }
}

void CALLBACK waveInProc(
                          HWAVEIN hwi,      
                          UINT uMsg,         
                          DWORD_PTR dwInstance,  
                          DWORD dwParam1,    
                          DWORD dwParam2     
                          )
{
    switch(uMsg)
    {
    case MM_WIM_DATA: OnWimData(hwi, (WAVEHDR*)dwParam1);
        break;
    //case MM_WIM_OPEN:
    //    break;
    }
}


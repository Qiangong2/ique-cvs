/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#include "rm_jt.h"
#include "vng.h"
#include "rm.h"


#define WRAP_FUNC(func_name, func_call) \
static void \
F_##func_name (VOICE_ARG* a) \
{ \
    func_call; \
}



WRAP_FUNC (VNG_ActivateVoice,
           VNG_ActivateVoice ((VN*)a->args.activate.vn, (int)a->args.activate.what, (VNMember)a->args.activate.mem))

WRAP_FUNC (VNG_AdjustVolume,
           VNG_AdjustVolume ((VNG*)a->args.adjust.vng, (int)a->args.adjust.what, (int)a->args.adjust.volume))

WRAP_FUNC (VNG_SelectVoiceCodec,
           VNG_SelectVoiceCodec ((VNG*)a->args.select.vng, (int)a->args.select.codec))



static
VOICERmApiWrapper __funcWrapper [(VOICE_RM_FUNC_ID_MAX + 1) - VOICE_RM_FUNC_ID_MIN] = {

/*  Wrapper Func                       Func ID */
/*  __________________________         _______ */
    F_VNG_ActivateVoice,                    /*  1001   */
    F_VNG_AdjustVolume,                     /*  1002   */
    F_VNG_SelectVoiceCodec,                 /*  1003   */
};


VOICERmApiWrapper  _voice_rm_getApiWrapper (u32 func_id)
{
    VOICERmApiWrapper rv = NULL;

    if (func_id >= VOICE_RM_FUNC_ID_MIN && func_id <= VOICE_RM_FUNC_ID_MAX) {
        rv = __funcWrapper [func_id - VOICE_RM_FUNC_ID_MIN ];
    }

    return rv;
}

/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#include <sc/sc.h>
#include <sc/ios.h>
#include <sc/priority.h>
#include "rm.h"
#include "rm_jt.h"
#include "shr_trace.h"
#include "shr_th.h"

#define RM       _TRACE_APP, 0
#define SHUTDOWN (IOS_OPEN+100)

#define STACK_SIZE (16*1024)
u8 rmThreadStack[STACK_SIZE] __attribute__((aligned(4)));
static _SHRThread rmThread;
static _SHRThreadAttr rmThreadAttr;

static VNG vng;
static VN vn;

static IOSMessageQueueId  rmMq = -1;
static int rmStarted = 0;

static IOSError
voiceIoctlv(IOSResourceRequest *req)
{
    IOSError           rv = IOS_ERROR_OK;
    IOSResourceIoctlv *ioctlv = (IOSResourceIoctlv *) &(req->args.ioctlv);
    VOICERmApiWrapper    funcWrapper;
    VOICE_ARG* a = (VOICE_ARG*)ioctlv->vector[0].base;
    u32 cmd;
    int i, j;
#define NUM_VNS 5
    VN vns[NUM_VNS];
    VNId tmpid;

    cmd = ioctlv->cmd;

    funcWrapper = _voice_rm_getApiWrapper (cmd);
    trace (FINER, RM, "VOICE RM voiceIoctlv Just before call API %d\n", cmd);
    switch (cmd) {
        case VOICE_FI_ACTIVATEVOICE:
            // Get vn
            i = NUM_VNS;
            VNG_GetVNs (&vng, vns, &i) ;
            for (j=0; j<i; j++) {
                VN_GetVNId(&vns[j], &tmpid);
                if (tmpid.netId == a->args.activate.vnid.netId) {
                    trace (FINER, RM, "VOICE RM found VN\n");
                    VNG_CloneVN(&vng, &vn, &vns[j]);
                    a->args.activate.vn = &vn;
                    break;
                }
            }
            if (j == i) {
                trace(ERR, RM, "VOICE RM Cannot find VN!\n");
                a->args.activate.vn = 0;
            }
            funcWrapper (a);
            break;
        case VOICE_FI_ADJUSTVOLUME:
            funcWrapper (a);
            break;
        case VOICE_FI_SELECTVOICECODEC:
            funcWrapper (a);
            break;
        default:
            break;
    }
    trace (FINER, RM, "VOICE RM voiceIoctlv API %d returned %d\n", cmd, rv);

    return rv;
}


static IOSError
waitForRMCmds ()
{
    IOSError    rv;
    IOSMessage  msg;
    u32 cmd;

    while (1) {

        trace (FINEST, RM, "VOICE RM waiting for commands\n");
        if ((rv = IOS_ReceiveMessage (rmMq, &msg, IOS_MESSAGE_BLOCK)) != IOS_ERROR_OK) {
            trace (ERR, RM, "VOICE RM IOS_ReceiveMessage returned %d\n", rv);
            goto end;
        }
        trace (FINEST, RM, "VOICE RM received request 0x%08x\n", msg);

        if (msg == SHUTDOWN) {
            deinit();

            trace (INFO, RM, "VOICE RM shutdown!\n");
            goto end;
        }

        IOSResourceRequest *req = (IOSResourceRequest*) msg;
        cmd = req->cmd;
        switch (cmd) {
            case IOS_OPEN:
                trace (FINEST, RM, "VOICE RM IOS_OPEN\n");
                rv = IOS_ERROR_OK;
                break;
            case IOS_CLOSE:
                trace (FINEST, RM, "VOICE RM IOS_CLOSE\n");
                rv = IOS_ERROR_OK;
                break;
            case IOS_IOCTLV:
                trace (FINEST, RM, "VOICE RM IOS_IOCTLV\n");
                rv = voiceIoctlv (req);
                break;
            default:
                trace (ERR, RM, "VOICE RM cmd %u not supported\n", req->cmd);
                rv = IOS_ERROR_INVALID;
                break;
        }
        trace (FINEST, RM, "VOICE RM Returning %d\n", rv);
        IOS_ResourceReply(req, rv);
    }


end:
    return rv;
}

static _SHRThreadRT _SHRThreadCC rmThreadFunc(void* lpParam ) 
{
    IOSError           rv;
    IOSMessage         msgArray[3];

//    _SHR_set_trace_show_time (0);
//    _SHR_set_trace_show_thread (0);
//    _SHR_set_sg_trace_level (_TRACE_APP, 0, FINEST);
    trace (INFO, RM, "VOICE RM starting\n");

    if (rmMq < 0) {
        VNG_Init(&vng, NULL);

        /* Create a message queue */
        if ((rmMq = IOS_CreateMessageQueue (msgArray, sizeof(msgArray)/sizeof(msgArray[0]))) < 0) {
            rv = rmMq;
            trace (ERR, RM, "VOICE RM IOS_CreateMessageQueue for resource manager returned %d\n", rv);
            goto end;
        }

        /* Register the message queue with the kernel */
        if ((rv = IOS_RegisterResourceManager (VOICE_DEV, rmMq)) != IOS_ERROR_OK) {
            trace (ERR, RM, "VOICE RM IOS_RegisterResourceManager returned %d\n", rv);
            goto end;
        }
    }

    rv = waitForRMCmds (rmMq);

end:
    /* should never get here */
    trace (INFO, RM, "VOICE RM exiting with error %d\n", rv);

    return (void*)rv;
}

int VOICE_StartRM(void)
{
    if (rmStarted) {
        trace (ERR, RM, "VOICE_StartRM() - already started!\n");
        return -1;
    }

    // Start net receiver thread
    rmThreadAttr.stack = rmThreadStack;
    rmThreadAttr.stackSize = STACK_SIZE;
    rmThreadAttr.priority = IOS_PRIORITY_LIBVOICE_RM;
    rmThreadAttr.start = 1;
    rmThreadAttr.attributes = 0;
    _SHR_thread_create(&rmThread, &rmThreadAttr, rmThreadFunc, NULL);

    rmStarted = 1;

    return 0;
}

int VOICE_StopRM(void)
{
    if (!rmStarted) {
        trace (ERR, RM, "VOICE_StopRM() - RM not started!\n");
        return -1;
    }

    while (IOS_SendMessage(rmMq, SHUTDOWN, IOS_MESSAGE_NOBLOCK) !=
           IOS_ERROR_OK) {;
           _SHR_thread_sleep(100);
    }
    _SHR_thread_join(rmThread, 0 /* *thread_return */, 0 /* timeout */);

    rmStarted = 0;

    return 0;
}

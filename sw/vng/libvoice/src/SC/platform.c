#define PLAT_IGNORE_UNDERFLOW
#define PLAT_IGNORE_OVERFLOW
/*
 *               Copyright (C) 2006, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */

#include <sc/ios.h>
#include <ioslibc.h>
#include <sc/aud.h>
#include <sc/priority.h>
#include "vng.h"
#include "shr_th.h"
#include "internal.h"

#define NUM_SAMPLES         256

/*
 * audio thread data
 */
  
#define AUD_THREAD_STACK_SIZE (16*1024)
u8 _audThreadStack[AUD_THREAD_STACK_SIZE];
#define AUD_THREAD_PRIORITY IOS_PRIORITY_LIBVOICE_AUDIO
static _SHRThread _audThreadId = -1;  /* on SC, valid thread ID's are always >= 0 */
static u32 audThreadControl;  

#define AUD_THREAD_STOP  1    /* token message telling audio thread to stop */

/*
 * network receive thread data
 */

#define NET_THREAD_STACK_SIZE (16*1024)
u8 _netThreadStack[NET_THREAD_STACK_SIZE];
#define NET_THREAD_PRIORITY IOS_PRIORITY_LIBVOICE_RECV
static _SHRThread _netThreadId = -1;  /* on SC, valid thread ID's are always >= 0 */

/* debugging macros */
//#define PLAT_DEBUG

#ifdef PLAT_DEBUG
 #define PLAT_LOG(...) printf("PLAT: " __VA_ARGS__)
 #define PLAT_LOG_CONT(...) printf(__VA_ARGS__)
 #define PLAT_ERROR(msg) printf("PLAT: Error: %s failed\n", msg)
#else
 #define PLAT_LOG(...)
 #define PLAT_LOG_CONT(...)
 #define PLAT_ERROR(msg) /* printf("PLAT: Error: %s failed\n", msg) */
#endif

/* misc defines */

#define O_RDWR  0x2              /* read/write flag for IOS_Open() call */

#define AUDBUF_MAX_AUDIO_OPS  2  /* max of 2 reads & 2 writes at once */

/* global variables */

IOSMessageQueueId audMq;       /* message queue for async audio operations */
/* message array for async audio operations,
 * allocate one extra msg for thread control
 */
IOSMessage audMsgArray[2*AUDBUF_MAX_AUDIO_OPS+1];
/* responses for completed async operations */
IOSResourceRequest audReadReply[AUDBUF_MAX_AUDIO_OPS];
IOSResourceRequest audWriteReply[AUDBUF_MAX_AUDIO_OPS];
/* audio i/o buffers
 * libvoice data has only 1 channel, but the audio driver requires
 * 2 channels of data
 */
u8 audReadBuf[AUDBUF_MAX_AUDIO_OPS][2*NUM_SAMPLES*SAMPLE_SIZE] __attribute__((aligned(16)));
u8 audWriteBuf[AUDBUF_MAX_AUDIO_OPS][2*NUM_SAMPLES*SAMPLE_SIZE] __attribute__((aligned(16)));
u8 nWrite = 0;     /* number of async writes in flight */
u8 nRead = 0;      /* number of async reads in flight */
IOSFd audFd = -1;  /* audio driver handle, -1 mean not open */

/* voiceLoop
 *
 * standalone thread started by initPlatform()
 *
 * loops waiting for audio read/write operations to complete; on read
 * completion, sends the new (microphone) data to be processed, then
 * starts another read; on writes, requests more output (speaker) data
 * and starts another write.
 *
 * looping continues until a THREAD_STOP control message is received
 */

_SHRThreadFunc
voiceLoop(void *arg)
{
    IOSError rv;
    IOSMessage m;
    u16 *p;
    int l;
    int i;
    
    PLAT_LOG("voiceLoop starting up\n");
    while ( 1 ) {
        PLAT_LOG("voiceLoop waiting for a msg\n");
        if ( (rv = IOS_ReceiveMessage(audMq, &m, IOS_MESSAGE_BLOCK)) == IOS_ERROR_OK ) {
            PLAT_LOG("received msg m = 0x%08x\n",(u32)m);
            if ( m == (u32) &audThreadControl ) {
                PLAT_LOG("received thread control msg\n");
                if ( *((u32 *)m) == AUD_THREAD_STOP ) {
                    PLAT_LOG("voiceLoop closing down\n");
                    _SHR_thread_exit(0);
                }
            } else if ( m >= (u32) audReadReply && m < (u32) audReadReply+sizeof(audReadReply) ) {
                u32 ndx = (m-(int)audReadReply)/sizeof(IOSResourceRequest);
                PLAT_LOG("received read complete msg, ndx = %d\n",ndx);
                nRead--;
                IOSResourceRequest *rp = (IOSResourceRequest *)m;
#ifdef PLAT_IGNORE_OVERFLOW
                if ( rp->status == IOS_ERROR_NOTREADY )
                    rp->status = sizeof(audReadBuf[0]);
#endif
                if ( rp->status < 0 ) {
                    PLAT_ERROR("audio read");
                    PLAT_LOG("audio read error, status = %d\n",rp->status);
                } else {
                    // convert data from 2-channel to 1-channel
                    p = (u16 *) audReadBuf[ndx];
                    l = rp->status/sizeof(u16); // num channels * num samples
                    l /= 2;  // number of samples
                    for (i=0; i<l; i++) {
                        p[i] = p[2*i];
                    }
                    // consume input data
                    gotRecordBuf(audReadBuf[ndx], l*sizeof(u16) /* num bytes */);
                }
                // read new data from audio driver
                if ( (rv = IOS_ReadAsync(audFd, audReadBuf[ndx], sizeof(audReadBuf[0]), audMq, &audReadReply[ndx]))
                     == IOS_ERROR_OK ) {
                    PLAT_LOG("new readAsync successful, ndx = %d\n",ndx);
                    nRead++;
                } else {
                    PLAT_ERROR("new readAsync");
                    PLAT_LOG("new readAsync failed, rv = %d, ndx = %d\n",rv,ndx);
                }
            } else if ( m >= (u32) audWriteReply && m < (u32) audWriteReply+sizeof(audWriteReply) ) {
                u32 ndx = (m-(int)audWriteReply)/sizeof(IOSResourceRequest);
                PLAT_LOG("received write complete msg, ndx = %d\n",ndx);
                nWrite--;
                IOSResourceRequest *rp = (IOSResourceRequest *)m;
#ifdef PLAT_IGNORE_UNDERFLOW
                if ( rp->status == IOS_ERROR_NOTREADY )
                    rp->status = sizeof(audWriteBuf[0]);
#endif
                if ( rp->status < 0 ) {
                    PLAT_ERROR("audio write");
                    PLAT_LOG("audio write error, status = %d\n",rp->status);
                }
                // generate new output data
                getNextPlayBuf(audWriteBuf[ndx], NUM_SAMPLES*SAMPLE_SIZE /* num bytes */);
#ifdef PLAT_DEBUG
                {
                    int i;
                    u16 *g = (u16 *) audWriteBuf[ndx];
                    for ( i=0; i<NUM_SAMPLES*SAMPLE_SIZE/sizeof(u16); i++ ) 
                        PLAT_LOG(" audWriteBuf[%d] = 0x%08x\n",i,g[i]);
                }
#endif
                    
                // convert data from 1-channel to 2-channel
                p = (u16 *) audWriteBuf[ndx];
                for (i=NUM_SAMPLES-1; i>=0; i--) {
                    p[2*i+1] = p[i];
                    p[2*i] = p[i];
                }
                // write new data to audio driver
                if ( (rv = IOS_WriteAsync(audFd, audWriteBuf[ndx], sizeof(audWriteBuf[0]), audMq, &audWriteReply[ndx]))
                     == IOS_ERROR_OK ) {
                    PLAT_LOG("new writeAsync successful, ndx = %d\n",ndx);
                    nWrite++;
                } else {
                    PLAT_ERROR("new writeAsync");
                    PLAT_LOG("new writeAsync failed, rv = %d, ndx = %d\n",rv,ndx);
                }
            } else {
                IOSResourceRequest *rp;
                rp = (IOSResourceRequest *)m;
                PLAT_LOG("received unknown audio message, dropping\n");
                PLAT_LOG(" cmd = %d, status = %d, handle = %d (0x%08x)\n",rp->cmd, rp->status, rp->handle);
            }
        } else {
            PLAT_ERROR("voiceLoop message receive");
            PLAT_LOG("voiceLoop message receive failed with rv = %d\n",rv);
        }
    }
    
}

/* deinitPlatform
 *
 * shuts down the audio driver and released related system resources
 *
 * returns 0 on success, 1 on error
 */

int deinitPlatform()
{
    IOSError rv = IOS_ERROR_OK;
    IOSError rv2;
    IOSMessage m;

    PLAT_LOG("deinit'ing platform\n");

    if ( audFd >= 0 ) {
        // mute audio to avoid audible artifacts while shutting down
        u32 mute = AUD_VOLUME_MUTE;
        IOS_Ioctl(audFd,AUD_SET_VOLUME,&mute,sizeof(mute),0,0);
        // terminate thread
        if ( _audThreadId >= 0 ) {
            // notify thread to stop
            audThreadControl = AUD_THREAD_STOP;
            rv = IOS_SendMessage(audMq, (u32) &audThreadControl, IOS_MESSAGE_NOBLOCK);
            assert(rv == IOS_ERROR_OK);
            // no thread return value; timeout isn't used on SC
            int status;
            status = _SHR_thread_join(_audThreadId, 0 /* *thread_return */, 0 /* timeout */);
            assert(status == _SHR_ERR_OK);
            _audThreadId = -1;
        }
        // complete any outstanding reads/writes, ignoring returned status
        while ( nRead || nWrite ) {
            if ( IOS_ReceiveMessage(audMq, &m, IOS_MESSAGE_BLOCK) == IOS_ERROR_OK ) {
                if ( m >= (u32) audReadReply && m < (u32) audReadReply+sizeof(audReadReply) ) {
                    nRead--;
                } else if ( m >= (u32) audWriteReply && m < (u32) audWriteReply+sizeof(audWriteReply) ) {
                    nWrite--;
                }
            }
        }
        // close audio device
        if ( (rv = IOS_Close(audFd)) == IOS_ERROR_OK ) {
            audFd = -1;
        }
    }
    /* clean up audio queue */
    if ( audMq >= 0 ) {
        if ( (rv2 = IOS_DestroyMessageQueue(audMq)) == IOS_ERROR_OK ) {
            audMq = -1;
        } else {
            PLAT_ERROR("IOS_DestroyMessageQueue audio");
            if ( rv == IOS_ERROR_OK )
                rv = rv2;
        }
    }

    if (_netThreadId >= 0) {
        extern VN* groupVn;
        VN_SendMsg(groupVn, VN_MEMBER_SELF, VN_TAG_VOICE, 0, 0, VN_ATTR_NOENCRYPT, VNG_WAIT);

        _SHR_thread_join(_netThreadId, 0 /* *thread_return */, 0 /* timeout */);
        _netThreadId = -1;
    }

    return rv == IOS_ERROR_OK ? 0 : 1;
}

/* initPlatform
 *
 * opens and configures audio driver, primes read/write data queues, and
 * starts a separate thread to ensure that the data queues remain full
 *
 * returns 0 on success, 1 on error
 */

int initPlatform()
{
    IOSError rv = IOS_ERROR_OK;
    _SHRThreadAttr ta;
    u32 volume;

    PLAT_LOG("initPlatform: entry\n");
    
    // start networking thread, if it's not already running
    PLAT_LOG("_netThreadId = %d\n",_netThreadId);
    if ( _netThreadId == -1 ) {
        PLAT_LOG("Starting netRecv thread\n");
        ta.stack = _netThreadStack;
        ta.stackSize = sizeof(_netThreadStack);
        ta.priority = NET_THREAD_PRIORITY;
        ta.start = 1;
        ta.attributes = IOS_THREAD_CREATE_JOINABLE;
        PLAT_LOG("calling _SHR_thread_create\n");
        if ( _SHR_thread_create( &_netThreadId, &ta, (_SHRThreadFunc) netRecvThread, 0 ) == _SHR_ERR_FAIL ) {
            PLAT_ERROR("_SHR_thread_create for net thread\n");
            _netThreadId = -1;
            rv = -1;
            goto out;
        }
        PLAT_LOG("netRecv thread started successfully\n");
    }

    // create message queue for async communication with audio driver
    PLAT_LOG("creating message queue\n");
    audMq = rv = IOS_CreateMessageQueue(audMsgArray, sizeof(audMsgArray)/sizeof(audMsgArray[0]));
    if ( rv < 0 ) {
        PLAT_ERROR("create write message queue");
        goto out;
    }
    // open audio device
    PLAT_LOG("opening audio\n");
    if ( (audFd = rv = IOS_Open(AUD_DEV "0", O_RDWR)) < 0 ) {
        PLAT_ERROR("headset audio open");
        goto out;
    }
    PLAT_LOG("audio opened successfully, audFd = %d\n",audFd);
    // mute audio volume until playback begins
    volume = 0;
    if ( (rv = IOS_Ioctl(audFd,AUD_SET_VOLUME,&volume,sizeof(volume),0,0)) != IOS_ERROR_OK ) {
        PLAT_ERROR("initial audio mute");
        goto out;
    }
    // set configuration
    PLAT_LOG("configuring audio\n");
    u32 config = AUD_CONFIG_ADC_8_DAC_8;
    if ( (rv = IOS_Ioctl(audFd,AUD_SET_CONFIG,&config,sizeof(config),0,0)) < 0 ) {
        PLAT_ERROR("audio config");
        goto out;
    }
    volume = 100;
    if ( (rv = IOS_Ioctl(audFd,AUD_SET_VOLUME,&volume,sizeof(volume),0,0)) != IOS_ERROR_OK ) {
        PLAT_ERROR("initial audio volume");
        goto out;
    }
    // prime audio in
    while ( nRead < AUDBUF_MAX_AUDIO_OPS ) {
        // submit read
        PLAT_LOG("Reading buf %d\n",nRead);
        if ( (rv = IOS_ReadAsync(audFd, audReadBuf[nRead], sizeof(audReadBuf[0]), audMq, &audReadReply[nRead])) < 0 ) {
            PLAT_ERROR("read buf");
            goto out;
        }
        nRead++;
    }
    // prime audio out
    while ( nWrite < AUDBUF_MAX_AUDIO_OPS ) {
        // get data
        PLAT_LOG("Getting Data for write buf %d\n",nWrite);
        getNextPlayBuf(audWriteBuf[nWrite], NUM_SAMPLES*SAMPLE_SIZE);
        // submit write
        PLAT_LOG("Writing buf %d\n",nWrite);
        if ( (rv = IOS_WriteAsync(audFd, audWriteBuf[nWrite], sizeof(audWriteBuf[0]), audMq, &audWriteReply[nWrite])) < 0 ) {
            PLAT_ERROR("write buf");
            goto out;
        }
        nWrite++;
    }
    PLAT_LOG("message ranges\n");
    PLAT_LOG("   control = 0x%08x\n", (u32) &audThreadControl);
    PLAT_LOG("   read = 0x%08x to 0x%08x\n", (u32) audReadReply, (u32) audReadReply + sizeof(audReadReply));
    PLAT_LOG("   write = 0x%08x to 0x%08x\n", (u32) audWriteReply, (u32) audWriteReply + sizeof(audWriteReply));
    
    // start audio thread
    PLAT_LOG("Starting voiceLoop thread\n");
    ta.stack = _audThreadStack;
    ta.stackSize = sizeof(_audThreadStack);
    ta.priority = AUD_THREAD_PRIORITY;
    ta.start = 1;
    ta.attributes = IOS_THREAD_CREATE_JOINABLE; // IOS_THREAD_CREATE_DETACHED; // IOS_THREAD_CREATE_JOINABLE;
    if ( _SHR_thread_create( &_audThreadId, &ta, (_SHRThreadFunc) voiceLoop, 0 ) == _SHR_ERR_FAIL ) {
        PLAT_ERROR("_SHR_thread_create for aud thread\n");
        _audThreadId = -1;
        rv = -1;
        goto out;
    }
    PLAT_LOG("audLoop thread started successfully\n");

out:
    PLAT_LOG("initialization complete: rv = %d\n",rv);
    
    if ( rv < 0 )
        deinitPlatform();  // clean up
    
    return rv == IOS_ERROR_OK ? 0 : 1;
}

/* setVolume
 *
 * set headset volume to the specified value
 *
 * return 0 on success, 1 on error
 */

int
setVolume(int volume)
{
    IOSError rv = IOS_ERROR_OK;

    if ( audFd >= 0 ) {
        PLAT_LOG("setVolume: calling ioctl for volume = %d\n",volume);
        rv = IOS_Ioctl(audFd,AUD_SET_VOLUME,&volume,sizeof(volume),0,0);
        PLAT_LOG("setVolume: ioctl returns rv = %d\n",rv);
    } else {
        PLAT_LOG("setVolume: audio not open\n");
    }
    
    return rv == IOS_ERROR_OK ? 0 : 1;
}

/* setVolume
 *
 * get current headset volume
 *
 * return 0 on success, 1 on error
 */

int
getVolume(int *volume)
{
    IOSError rv = IOS_ERROR_OK;
    
    if ( audFd >= 0 ) {
        PLAT_LOG("getVolume: calling ioctl\n");
        rv = IOS_Ioctl(audFd,AUD_GET_VOLUME,0,0,volume,sizeof(*volume));
        PLAT_LOG("getVolume: ioctl returns rv = %d, volume = %d\n",rv,*volume);
    } else {
        PLAT_LOG("setVolume: audio not open\n");
    }
    
    return rv == IOS_ERROR_OK ? 0 : 1;
}

/* getMinVolume
 *
 * return lowest possible non-mute volume setting
 */

int getMinVolume(void)
{
    return AUD_VOLUME_MIN;
}

/* getMaxVolume
 *
 * return highest possible volume setting
 */

int getMaxVolume(void)
{
    return AUD_VOLUME_MAX;
}

/* getMuteVolume
 *
 * return volume setting corresponding to mute
 */

int getMuteVolume(void)
{
    return AUD_VOLUME_MUTE;
}

#ifdef __cplusplus
 extern "C" {
#endif 

int initPlatform();
int deinitPlatform();
int setVolume(int volume);
int getVolume(int *volume);
int getMinVolume(void);
int getMaxVolume(void);
int getMuteVolume(void);
     
#ifdef __cplusplus
 }
#endif

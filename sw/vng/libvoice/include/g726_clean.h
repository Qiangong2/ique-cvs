#ifdef __cplusplus
 extern "C" {
#endif 

typedef struct Float11 {
	int sign;   /**< 1bit sign */
	int exp;    /**< 4bit exponent */
	int mant;   /**< 6bit mantissa */
} Float11;

typedef struct G726Tables {
	int  bits;            /**< bits per sample */
	int* quant;           /**< quantization table */
	int* iquant;          /**< inverse quantization table */
	int* W;               /**< special table #1 ;-) */
	int* F;               /**< special table #2 */
} G726Tables;

typedef struct G726Context {
	 G726Tables* tbls;    /**< static tables needed for computation */
	 
	 Float11 sr[2];       /**< prev. reconstructed samples */
	 Float11 dq[6];       /**< prev. difference */
	 int a[2];            /**< second order predictor coeffs */
	 int b[6];            /**< sixth order predictor coeffs */
	 int pk[2];           /**< signs of prev. 2 sez + dq */
	 
	 int ap;              /**< scale factor control */
	 int yu;              /**< fast scale factor */
	 int yl;              /**< slow scale factor */
	 int dms;             /**< short average magnitude of F[i] */
	 int dml;             /**< long average magnitude of F[i] */
	 int td;              /**< tone detect */

	 int se;              /**< estimated signal for the next iteration */
	 int sez;             /**< estimated second order prediction */
	 int y;               /**< quantizer scaling factor for the next iteration */
} G726Context;

typedef struct AVG726Context {
   G726Context c;
   int bits_left;
   int bit_buffer;
   int code_size;
} AVG726Context;

typedef struct AVCodecContext {
    void *priv_data;
    /* audio only */
    int sample_rate; ///< samples per sec 
    int channels;
    int bit_rate;
} AVCodecContext;

int g726_init(AVCodecContext * avctx);
int g726_encode_frame(AVCodecContext *avctx,
                            uint8_t *dst, int buf_size, void *data);
int g726_decode_frame(AVCodecContext *avctx,
                             void *data, int *data_size,
                             uint8_t *buf, int buf_size);

#ifdef __cplusplus
 }
#endif

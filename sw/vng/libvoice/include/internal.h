#ifdef __cplusplus
 extern "C" {
#endif 

//#define _NO_CODEC_ // XXX 
//#define _RAND_DROP_ // XXX
//#define _RAND_REORG_ // XXX

#define SAMPLE_RATE         8000
#define SAMPLE_SIZE         2 // 16 bits
#define MAX_SAMPLES         4000 // .5 second
#define G726_BIT_RATE_LIMIT     32000 // highest bit rate
#define MAX_PLAYBACK_BUF    (MAX_SAMPLES * SAMPLE_SIZE)
#ifndef _NO_CODEC_
#define MAX_TRANSMIT_BUF        ((MAX_SAMPLES * (G726_BIT_RATE_LIMIT/SAMPLE_RATE))/8)
#else
#define MAX_TRANSMIT_BUF        (MAX_SAMPLES * SAMPLE_SIZE)
#endif
#define VN_TAG_VOICE        VN_SERVICE_TAG_MAX

//*****************
// DATA STRUCTURES
//*****************

typedef struct _PlaybackBuffer {
    unsigned int seqNum;
    int dataSize;
    int codec;
    struct _PlaybackBuffer* next;
    char encodedData[MAX_TRANSMIT_BUF];
} PlaybackBuffer;

typedef struct {
    int codec;
    unsigned int seqNum;
    int silenceMode;
    int dataSize;
} XmitDataHdr;

typedef struct {
    XmitDataHdr hdr;
    char encodedData[MAX_TRANSMIT_BUF];
} XmitData;



int gotRecordBuf(char* buf, int size);
int getNextPlayBuf(char* outBuf, int size);
void netRecvThread();

int makeXmitBuf(char* recordBuf, int recordSize,
                void* outBuf, unsigned int* outSize);
int onNetRecv(int peerId, XmitData* xmitBuf);
void initVoice(int startThreads);

#ifdef __cplusplus
 }
#endif

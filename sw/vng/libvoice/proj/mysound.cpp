// mysound.cpp by Yoginder Dandass, September 2000 
// Win32 dialog based application to demonstrate the 
// full-duplex capture and playback of live audio 
// over the Internet in real-time. This application 
// allows full-duplex conversation if the audio devices 
// at the two computers are capable of simultaneous 
// capture and playback. 

#include <winsock2.h> 
#include <windows.h> 
#include <windowsx.h> 
#include <stdlib.h> 
#include <mmsystem.h> 
#include <mmreg.h> 
#include <list> 
#include <queue> 

#include "proj.h"
#include "g726.h"

#define WM_USR_SOCKIO (WM_USER+1) // Socket notification msg 
#define PORT_NUMBER 1500 // Socket port number 
#define BLOCK_ALIGN 2 // Min block size for PCM 
#define NUM_SAMPLES 256
#define BLOCK_SIZE ((NUM_SAMPLES * BLOCK_ALIGN)) // Multiple of BLOCK_ALIGN 
#define G726_BLOCK_SIZE ((BLOCK_SIZE / 8)) // G.726 compresses 16 bits into 2
#define AVG_BYTES_PER_SEC 2000 //  data rate 
// 1 second worth of blocks 
#define NUM_BLOCKS ((AVG_BYTES_PER_SEC / G726_BLOCK_SIZE)) 
// For jitter control 
#define PLAYBACK_NUM_BLOCKS 4
#define THRESHOLD 3 // Delay threshold 
#define PLAYBACK_THRESHOLD 1 // Playback threshold 
#define MAX_CHANNELS 4
#define VAD_THRESHOLD 0xF200
typedef WORD T_BSIZE;

// Type for size of block 
typedef struct 
{
    BYTE id;
    BYTE vad;
    DWORD m_dwSeq;
    BYTE m_abData[G726_BLOCK_SIZE];
}
XMITDATA;
class CSendBuffer 
{
public:
    WAVEHDR m_WaveHeader;    // wave header for the buffer 
    XMITDATA m_xmitData;    // Data block to be transmitted over UDP 
    BYTE m_recData[BLOCK_SIZE];

    MMRESULT Prepare(HWAVEIN hWaveIn) 
    {

        // Prepare for playback 
        ZeroMemory(&m_WaveHeader, sizeof(m_WaveHeader));
        m_WaveHeader.dwBufferLength = BLOCK_SIZE;
        m_WaveHeader.lpData = (char*)(m_recData);

        m_WaveHeader.dwUser = (DWORD)this;
        return waveInPrepareHeader(hWaveIn, &m_WaveHeader, sizeof(m_WaveHeader));
    }
    MMRESULT Unprepare(HWAVEIN hWaveIn) 
    {
        return waveInUnprepareHeader(hWaveIn, &m_WaveHeader, sizeof(m_WaveHeader));
    }
    MMRESULT Add(HWAVEIN hWaveIn) 
    {
        return waveInAddBuffer(hWaveIn, &m_WaveHeader, sizeof(m_WaveHeader));
    }
};
class CRecvBuffer 
{
public:
    WAVEHDR m_WaveHeader;
    XMITDATA m_xmitData;
    BYTE m_playData[BLOCK_SIZE];
    MMRESULT Prepare(HWAVEOUT hWaveOut) 
    {
        ZeroMemory(&m_WaveHeader, sizeof(m_WaveHeader));
        m_WaveHeader.dwBufferLength = BLOCK_SIZE;
        m_WaveHeader.lpData = (char*)(m_playData);
        m_WaveHeader.dwUser = (DWORD)this;
        return waveOutPrepareHeader(hWaveOut, &m_WaveHeader, sizeof(m_WaveHeader));
    }
    MMRESULT Unprepare(HWAVEOUT hWaveOut) 
    {
        return waveOutUnprepareHeader(hWaveOut, &m_WaveHeader, sizeof(m_WaveHeader));
    }
    MMRESULT Add(HWAVEOUT hWaveOut) 
    {
        return waveOutWrite(hWaveOut, &m_WaveHeader, sizeof(m_WaveHeader));
    }
};
typedef std::queue<CSendBuffer*> CSendBufQ;
typedef std::list<CRecvBuffer*> CRecvBufL;
typedef CRecvBufL::iterator CBufLIter;
class CSoundDialog 
{
protected:
    AVCodecContext* pDecodeContext;
    AVCodecContext* pEncodeContext;

    HWND m_hWnd;    // Dialog handle 
    bool m_fInClosing;    // Stopping wave capture? 
    bool m_fOutClosing;    // Stopping playback? 
    HWAVEIN m_hWaveIn;    // Handle to capture device 
    HWAVEOUT m_hWaveOut;    // Handle to playback device 
    CSendBuffer m_aInBlocks[NUM_BLOCKS];    // Capture bufs 
    CRecvBuffer m_aOutBlocks[NUM_BLOCKS*2];    // Playback bufs 
    CRecvBuffer m_aPlaybackBlocks[PLAYBACK_NUM_BLOCKS];    // Playback bufs 
    SOCKET m_Socket;    // UDP socket 
    struct sockaddr_in m_SockAddr;    // Remote address 
    DWORD m_dwOutSeq;    // Sequence of next out buffer
    int m_iCountIn;    // Items in capture queue 
    int m_iCountOut;    // Items in playback queue 
    CRecvBufL m_lpFreeBufs;    // List of free recv buffers 
    CSendBufQ m_qpXmitBufs;    // Transmission queue 
    CRecvBufL m_lpMixerBufs;    // List of free mixer buffers 
    bool m_fExiting;    // Shutting down?
    bool m_fVAD;  // In VAD quiet mode?

    CRecvBufL m_lpPlayBufs[MAX_CHANNELS];    // List of playback buffers
    bool m_fDelay[MAX_CHANNELS];    // In delay mode? 
    DWORD m_dwSeqExp[MAX_CHANNELS];     // Sequence counter 

    void Report(char *pszBuffer) 
    {
        HWND hWndEdit = GetDlgItem(m_hWnd, IDC_EDIT_DETAILS);
        SendMessage(hWndEdit, EM_SETSEL, 64000, 64000);
        SendMessage(hWndEdit, EM_REPLACESEL, FALSE, (long)pszBuffer);
        return;
    }
    void WaveOutError(MMRESULT mmRes)
    {
        char str[MAXERRORLENGTH];
        waveOutGetErrorText(mmRes,str,MAXERRORLENGTH);
        Report(str);
    }
    BOOL OnInit(HWND hWnd) 
    {
        pEncodeContext = new struct AVCodecContext;
        pEncodeContext->priv_data = new struct AVG726Context;
        pEncodeContext->sample_rate = 8000;
        pEncodeContext->channels = 1;
        pEncodeContext->bit_rate = 16000;
        g726_init(pEncodeContext);
        pDecodeContext = new struct AVCodecContext;
        pDecodeContext->priv_data = new struct AVG726Context;
        pDecodeContext->sample_rate = 8000;
        pDecodeContext->channels = 1;
        pDecodeContext->bit_rate = 16000;
        g726_init(pDecodeContext);

        // Initialize various member variables 
        m_hWnd = hWnd;
        m_fExiting = false;

        // Not exiting
        m_fInClosing = m_fOutClosing = true;

        // Devices are closed 
        m_hWaveIn = 0;

        // Capture device 
        m_hWaveOut = 0;

        // Playback device
        for (int i=0; i<MAX_CHANNELS; i++) {
            m_fDelay[i] = false;        // Playback delay off for now 
        }
        m_fVAD = false;
        EnableWindow(GetDlgItem(hWnd, IDC_BUTTON_DISCONNECT), FALSE);
        return TRUE;
    }
    void OnCancel() 
    {
        if ((m_hWaveOut != 0) || (m_hWaveIn != 0)) 
        {
            OnDisconnect();

            // Close socket/devices before exiting
            m_fExiting = true;

            // Set exit indicator 
        }
        else EndDialog(m_hWnd, 0);

        // Exit if devices closed 
    }
    void OnConnect() 
    {
        char szIPAddress[128];
        unsigned long ulAddrIP;
        struct hostent *pHostEnt;
        PCMWAVEFORMAT WaveFormatPCM;
        MMRESULT mmRC;
        ZeroMemory(&m_SockAddr, sizeof(m_SockAddr));

        // Initialize size of previous buffer 
        // Obtain remote host's IP address 
        GetDlgItemText(m_hWnd, IDC_EDIT_REMOTEIPADDR, szIPAddress, sizeof(szIPAddress));
        ulAddrIP = inet_addr(szIPAddress);
        if (ulAddrIP != INADDR_NONE) // Is it dotted decimal? 
            memcpy(&(m_SockAddr.sin_addr), &ulAddrIP, sizeof(m_SockAddr.sin_addr));
        else 
        {

            // Use DNS to get IP address 
            pHostEnt = gethostbyname(szIPAddress);
            if (pHostEnt == NULL) 
            {
                MessageBox(m_hWnd, "Error resolving remote name", "Error", MB_OK | MB_ICONSTOP);
                return;
            }
            memcpy(&(m_SockAddr.sin_addr), pHostEnt->h_addr, pHostEnt->h_length);
        }

        // Create and bind the local socket to a port 
        m_Socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
        m_SockAddr.sin_family = AF_INET;
        m_SockAddr.sin_port = htons(PORT_NUMBER);
        bind(m_Socket, (sockaddr*)&m_SockAddr, sizeof(m_SockAddr));

        // Set the remote address for future communication 
        connect(m_Socket, (struct sockaddr*)&m_SockAddr, sizeof(m_SockAddr));

        // Open wave capture and playback device for PCM
        WaveFormatPCM.wf.wFormatTag = WAVE_FORMAT_PCM;
        WaveFormatPCM.wf.nChannels = 1;
        WaveFormatPCM.wf.nSamplesPerSec = 8000;
        WaveFormatPCM.wf.nAvgBytesPerSec = 16000;
        WaveFormatPCM.wf.nBlockAlign = 2;
        WaveFormatPCM.wBitsPerSample = 16;
        mmRC = waveOutOpen(&m_hWaveOut, (UINT)WAVE_MAPPER, (LPWAVEFORMATEX)&(WaveFormatPCM), (DWORD)m_hWnd, (DWORD)NULL, CALLBACK_WINDOW);
        if (mmRC != MMSYSERR_NOERROR) Report("Error opening wave playback device\r\n");
        else m_fOutClosing = false;
        mmRC = waveInOpen(&m_hWaveIn, (UINT)WAVE_MAPPER, (LPWAVEFORMATEX)&(WaveFormatPCM), (DWORD)m_hWnd, (DWORD)NULL, CALLBACK_WINDOW);
        if (mmRC != MMSYSERR_NOERROR) Report("Unable to open wave input device\r\n");
        else 
        {
            m_fInClosing = false;
            waveInStart(m_hWaveIn);
        }
        if (!(m_fInClosing && m_fOutClosing)) 
        {

            // If at least one of the devices was started
            EnableWindow(GetDlgItem(m_hWnd, IDC_BUTTON_CONNECT), FALSE);
            EnableWindow(GetDlgItem(m_hWnd, IDC_BUTTON_DISCONNECT), TRUE);
        }
    }

    void OnDisconnect() 
    {
        if (m_hWaveOut != 0) 
        {

            // do if playback device is open 
            m_fOutClosing = true;

            // Disable notification and close socket 
            WSAAsyncSelect(m_Socket, m_hWnd, 0, 0);
            closesocket(m_Socket);

            // Reset playback and close if all buffers are returned
            waveOutReset(m_hWaveOut);

            // Needed because we can be in delay mode 
            if (m_iCountOut == 0) waveOutClose(m_hWaveOut);
        }
        if (m_hWaveIn != 0) 
        {

            // do if capture device is open 
            m_fInClosing = true;
            waveInReset(m_hWaveIn);
            if (m_iCountIn == 0) waveInClose(m_hWaveIn);
        }
        return;
    }
    void OnWimData(WAVEHDR *pHdrWave) 
    {
        CSendBuffer *pAudioBuffer;        // pointer to the wave buffer
        XMITDATA *pXmitData;        // ptr to the portion to send 
        m_iCountIn--;
        pAudioBuffer = (CSendBuffer*)(pHdrWave->dwUser);

        // Unlink the buffer from the capture device 
        pAudioBuffer->Unprepare(m_hWaveIn);
        if (!m_fInClosing) 
        {
            // Check values
            short *samples = (short*)pAudioBuffer->m_recData;
            short tmp;
            int signal_found = 0;
            for (int i=0; i<pHdrWave->dwBytesRecorded >> 1; i++) {
                tmp = *samples;
                tmp = abs(tmp);
                if (tmp & VAD_THRESHOLD) {
                    signal_found = 1;
                    break;
                }
                samples++;
            }
            if (!signal_found) {
                if (m_fVAD == true) {
                    pAudioBuffer->Prepare(m_hWaveIn);
                    pAudioBuffer->Add(m_hWaveIn);
                    m_iCountIn++;
                    return;
                } else {
                    m_fVAD = true;
                }
            } else {
                m_fVAD = false;
            }

            // Compress 16 bit audio to 2 bit g726
            g726_encode_frame(pEncodeContext, pAudioBuffer->m_xmitData.m_abData, 
                pHdrWave->dwBytesRecorded >> 1, pAudioBuffer->m_recData);

            pXmitData = &(pAudioBuffer->m_xmitData);

            // Set the buffer data size, sequence, redundant data 
            pXmitData->id = 0;
            pXmitData->vad = m_fVAD;
            pXmitData->m_dwSeq = m_dwOutSeq++;

            // add to the transmission queue 
            m_qpXmitBufs.push(pAudioBuffer);
            OnSocketWrite();

            // Try to send queued buffers 
        }
        else 
        {

            // close is requested, don't recycle 
            // If all buffers have been returned, close the device 
            if (m_iCountIn == 0) waveInClose(m_hWaveIn);
        }
    }
    void OnSocketWrite() 
    {
        CSendBuffer *pBuffer;
        if (m_fInClosing) return;

        // Don't transmit when closing 
        while (!m_qpXmitBufs.empty()) 
        {

            // Send as many as you can
            pBuffer = m_qpXmitBufs.front();

            // Send data over socket 
            if (send(m_Socket, (char*)&(pBuffer->m_xmitData), sizeof(XMITDATA), 0) == SOCKET_ERROR) 
            {
                Report("Error sending data\r\n");
                break;

                // Stop when the UDP buffers fill up 
            }

            // Remover & recycle the sent buffer 
            m_qpXmitBufs.pop();
            pBuffer->Prepare(m_hWaveIn);
            pBuffer->Add(m_hWaveIn);
            m_iCountIn++;
        }
    }

    void JitterControl(int id)
    {
        if (m_fDelay[id]) 
        {
            // Need to fill device buffer + Jitter Buffer
            if (m_lpPlayBufs[id].size() >= THRESHOLD + PLAYBACK_NUM_BLOCKS)
            {
                // Start playback if enough buffers received 
                Report("Delay off\r\n");
                m_fDelay[id] = false;
            }
        } else if (m_lpPlayBufs[id].size() == 0) 
        {

            // Start delay mode if we run out of buffers
            m_fDelay[id] = true;
            Report("Delay on\r\n");
        }
    }

    void PlayBuffers() 
    {
        CRecvBuffer *pMixerBuffer;
        CRecvBuffer *pTmpBuffer;
        while (m_iCountOut < PLAYBACK_NUM_BLOCKS)
        {
            int channels = 0;
            for (int i=0; i<MAX_CHANNELS; i++) {
                if (!m_fDelay[i] && !m_lpPlayBufs[i].empty()) {
                    channels++;
                }
            }
            if (channels == 0) return;

            pMixerBuffer = m_lpMixerBufs.front();
            ZeroMemory(pMixerBuffer->m_playData, BLOCK_SIZE);
            
            for (int i=0; i<MAX_CHANNELS; i++) {
                if (!m_fDelay[i] && !m_lpPlayBufs[i].empty()) {
                    pTmpBuffer = m_lpPlayBufs[i].front();
                    m_dwSeqExp[i] = pTmpBuffer->m_xmitData.m_dwSeq + 1;
                    m_lpPlayBufs[i].pop_front();
                    m_lpFreeBufs.push_back(pTmpBuffer);

                    short *src = (short*)pTmpBuffer->m_playData;
                    short *dst = (short*)pMixerBuffer->m_playData;
                    for (int i=0; i<NUM_SAMPLES; i++) {
                        *dst += *src / channels;
                        dst++;
                        src++;
                    }

                }
            }

            pMixerBuffer->Prepare(m_hWaveOut);
            pMixerBuffer->Add(m_hWaveOut);
            m_iCountOut++;
            m_lpMixerBufs.pop_front();
        }
    }
    void OnWomDone(WAVEHDR *pHdrWave) 
    {
        CRecvBuffer *pBuffer;

        // Playback done -- Unprepare buffer and add to free list
        pBuffer = (CRecvBuffer*)(pHdrWave->dwUser);
        pBuffer->Unprepare(m_hWaveOut);
        m_iCountOut--;
        m_lpMixerBufs.push_back(pBuffer);
        if (!m_fOutClosing) {
            for (int i=0; i<MAX_CHANNELS; i++) {
                JitterControl(i); // Do jitter control if not exiting 
            }
            PlayBuffers();
        } else if (m_iCountOut == 0) {
            waveOutClose(m_hWaveOut);
        }
    }

    void OnSocketRead() 
    {
        CRecvBuffer *pBuffer;
        XMITDATA *pData;
        if (m_fOutClosing) // Ignore data if playback is closing 
            return;
        if (m_lpFreeBufs.empty()) 
        {

            // Overflow 
            XMITDATA Data;
            recv(m_Socket, (char*)&Data, sizeof(Data), 0);
            Report("No free buffers (discarding block)\r\n");
            return;
        }
        pBuffer = (CRecvBuffer*)(m_lpFreeBufs.front());
        pData = &(pBuffer->m_xmitData);
        if (recv(m_Socket, (char*)pData, sizeof(*pData), 0) == SOCKET_ERROR)
            Report("Error receiving data\r\n");
        else 
        {
            int id = pData->id;
            if (id >= MAX_CHANNELS) {
                Report("invalid channel ID\r\n");
                return;
            }

            if (pData->m_dwSeq == 0) m_dwSeqExp[id] = 0; // Reset the expected sequence 

            if (pData->m_dwSeq >= m_dwSeqExp[id]) 
            {
                if (pData->vad && m_fDelay[id]) { // vad activated, play all buffers
                    Report("Delay off\r\n");
                    m_fDelay[id] = false;
                }

                CBufLIter Iter;
                // Search for appropriate position
                for (Iter = m_lpPlayBufs[id].begin();
                    Iter != m_lpPlayBufs[id].end();
                    Iter++) 
                {
                    if ((*Iter)->m_xmitData.m_dwSeq == pData->m_dwSeq) {
                        return; // Duplicate buffer - don't insert 
                    } else if ((*Iter)->m_xmitData.m_dwSeq >
                        pData->m_dwSeq) 
                    {
                        break; // Found the insertion point! 
                    }
                }

                // Remove from Free list Insert into Playback list 
                m_lpFreeBufs.pop_front();
                m_lpPlayBufs[id].insert(Iter, pBuffer);

                // Decompress packet from g726 to 16 bit PCM
                int x;
                g726_decode_frame(pDecodeContext, pBuffer->m_playData, &x, 
                    pBuffer->m_xmitData.m_abData, G726_BLOCK_SIZE);

                JitterControl(id);
                PlayBuffers();
            }
        }
    }
    void OnWimOpen() 
    {
        m_dwOutSeq = 0;

        // reset sequence for sent blocks 
        m_iCountIn = 0;

        // reset count of data blocks in queue 
        for (int i = 0;
            i < NUM_BLOCKS;
            i++) 
        {

            // prepare and add blocks to capture device queue 
            m_aInBlocks[i].Prepare(m_hWaveIn);
            m_aInBlocks[i].Add(m_hWaveIn);
            m_iCountIn++;
        }
    }
    void OnWimClose() 
    {
        m_hWaveIn = 0;
        if (m_hWaveOut == 0) 
        {

            // If both devices are closed 
            EnableWindow(GetDlgItem(m_hWnd, IDC_BUTTON_DISCONNECT), FALSE);
            EnableWindow(GetDlgItem(m_hWnd, IDC_BUTTON_CONNECT), TRUE);
            if (m_fExiting) EndDialog(m_hWnd, 0);
        }
    }
    void OnWomOpen() 
    {
        m_iCountOut = 0;
        for (int i=0; i<MAX_CHANNELS; i++) {
            m_dwSeqExp[i] = 0;
        }
        for (int i = 0;
            i < NUM_BLOCKS*2;
            i++) 
        {

            // Setup free list 
            m_lpFreeBufs.push_back(&(m_aOutBlocks[i]));
        }
        for (int i = 0; i<PLAYBACK_NUM_BLOCKS; i++) {
            m_lpMixerBufs.push_back(&(m_aPlaybackBlocks[i]));
        }
        WSAAsyncSelect(m_Socket, m_hWnd, WM_USR_SOCKIO, FD_READ | FD_WRITE);

        // Non-blocking socket 
    }
    void OnWomClose() 
    {
        m_hWaveOut = 0;
        if (m_hWaveIn == 0) 
        {

            // If both devices are closed 
            EnableWindow(GetDlgItem(m_hWnd, IDC_BUTTON_DISCONNECT), FALSE);
            EnableWindow(GetDlgItem(m_hWnd, IDC_BUTTON_CONNECT), TRUE);
            if (m_fExiting) EndDialog(m_hWnd, 0);
        }
    }
public:
    BOOL static CALLBACK SoundDialogProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) 
    {
        CSoundDialog *pSoundDlg;
        pSoundDlg = (CSoundDialog *)GetWindowLong(hWnd, DWL_USER);
        switch(uMsg) 
        {
        case MM_WIM_DATA: pSoundDlg->OnWimData((WAVEHDR*)lParam);
            break;
        case MM_WOM_DONE: pSoundDlg->OnWomDone((WAVEHDR*)lParam);
            break;
        case WM_USR_SOCKIO: if (WSAGETSELECTEVENT(lParam) == FD_READ) pSoundDlg->OnSocketRead();
            if (WSAGETSELECTEVENT(lParam) == FD_WRITE) pSoundDlg->OnSocketWrite();
            break;
        case WM_COMMAND: if (GET_WM_COMMAND_CMD(wParam, lParam) == BN_CLICKED) 
                         {
                             switch (GET_WM_COMMAND_ID(wParam, lParam)) 
                             {
                             case IDCANCEL: pSoundDlg->OnCancel();
                                 break;
                             case IDC_BUTTON_CONNECT: 
                                 pSoundDlg->OnConnect();
                                 break;
                             case IDC_BUTTON_DISCONNECT: pSoundDlg->OnDisconnect();
                                 break;
                             }
                         }
                         break;
        case MM_WIM_OPEN: pSoundDlg->OnWimOpen();
            break;
        case MM_WIM_CLOSE: pSoundDlg->OnWimClose();
            break;
        case MM_WOM_OPEN: pSoundDlg->OnWomOpen();
            break;
        case MM_WOM_CLOSE: pSoundDlg->OnWomClose();
            break;
        case WM_INITDIALOG: SetWindowLong(hWnd, DWL_USER, lParam);
            pSoundDlg = (CSoundDialog *)lParam;
            return pSoundDlg->OnInit(hWnd);
        }
        return 0;
    }
};

int _stdcall WinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPSTR szCmdLine, int iShow) 
{
    CSoundDialog SoundDlg;
    WORD wVersionRequested;
    WSADATA wsaData;
    if (hPrevInst) return -1;
    wVersionRequested = MAKEWORD(1, 1);
    WSAStartup(wVersionRequested, &wsaData);

    // setup WinSock 
    // start dialog window
    DialogBoxParam(hInst, MAKEINTRESOURCE(IDD_PROJ_DIALOG), NULL, SoundDlg.SoundDialogProc, (long)&SoundDlg);
    WSACleanup();

    // cleanup WinSock 
    return 0;
}

//End of File 

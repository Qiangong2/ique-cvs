ReadMe.txt
----------
This is a prototype project to implement VNG voice-chat 
on a Windows system.  It uses Win32 WaveForm API's to
capture and playback 16 bit PCM samples from the microphone
to the speaker.  It compresses the voice samples using the
G726 ADPCM codec into 2 bit samples (16kbps) and packages
them into UDP packages to send and receive over windows
sockets.

The mysound.cpp source originated from a paper written by
Yoginder Dandass, as well as soundeco.cpp.  The latter
file implements an echo server with simulated variable
packet loss.  The g726.cpp source originated from the
open-source "ffmpeg" project.

mysound.cpp
-----------
Dandass' source has been modfied with the following:
. Implemented 4-channel jitter-buffer.
. Simple Voice Activity Detection
. 4-channel conferencing
. Use "ffmpeg" project's G726 codec.
. Removed redundant packet transmission.

soundeco.cpp
------------
. Duplicate copies of packets are delayed and echoed back
  on second channel.

The end goal is to integrate this code into libvng.dll in
the VBA-SC development enviornment on windows.  The 
following is the Voice API I have in mind:
    - VoiceEnable(VN)
    - VoiceDisable(VN)
    - VoiceSetTargets(VN, list) // default all
    - VoiceSetPlaybackVolume(int) // control SC hardware
    - get/set configs?
    	. compression
    	. VAD sensibility

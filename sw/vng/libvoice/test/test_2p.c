#include <stdio.h>
#include <windows.h>
#include "vng.h"
#include "testcfg.h"

#define VOICE_GAME_ID   74114

VNG vng;
VN vn;
VNId vnid;

int SetupVNG(void);
int hostNewGame();
int searchAndJoinGame();

int SetupVNG(void)
{
    VNGErrCode ec;
    uint32_t   cstat;
    uint32_t   expected_cstat_bits = (VNG_ST_USB_CONNECTED   |  
                                      VNG_ST_PROXY_CONNECTED |
                                      VNG_ST_GATEWAY_DEFINED |
                                      VNG_ST_VNG_INITIALIZED);

    if ((ec = VNG_Init(&vng, NULL))) {
        return -1;
    }

    cstat = VNG_ConnectStatus(&vng);

    if ((cstat & expected_cstat_bits) != expected_cstat_bits) {
        VNG_Fini(&vng);
        return -1;
    }

    if ((ec = VNG_Login (&vng, VNGS_HOST, VNGS_PORT, TESTER01, TESTER01PW, VNG_USER_LOGIN, VNG_TIMEOUT))) {
        VNG_Fini(&vng);
        return -1;
    }

    return 0;
}

int hostNewGame()
{
    VNGErrCode err;
    VNGGameInfo info;

    // Create a new VN
    err = VNG_NewVN(&vng, &vn, VN_DEFAULT_CLASS, VN_DOMAIN_INFRA, VN_ABORT_ON_ANY_EXIT);
    if (err != VNG_OK)
        return -1;

    // Describe the game information for matchmaking
    memset((void*)&info, 0, sizeof(VNGGameInfo));
    VN_GetVNId(&vn, &info.vnId);
    info.owner = VNG_MyUserId(&vng);
    info.gameId = VOICE_GAME_ID;
    info.titleId = VOICE_GAME_ID;
    info.accessControl = VNG_GAME_PUBLIC;
    info.totalSlots = 2;
    info.buddySlots = 0;   // nothing reserved for buddies
    info.maxLatency = 500; // 500ms RTT 
    info.netZone = 0;      // server will decide this value
    info.attrCount = 0;
    strcpy(info.keyword, "Voice test!");

    // Register the VN to the directory for matchmaking
    err = VNG_RegisterGame(&vng, &info, NULL, VNG_TIMEOUT);
    if (err != VNG_OK)
        return -1;

    vnid = info.vnId;

    printf("OK: VNG_RegisterGame\n");

    return 0;
}

int waitForPlayers()
{
    VNGUserInfo userInfo;
    uint64_t requestId;
    VNGErrCode err;

    err = VNG_GetJoinRequest(&vng, vnid, NULL, 
        &requestId, &userInfo, NULL, 0, VNG_WAIT);
    if (err == VNG_OK) {
        VNG_AcceptJoinRequest(&vng, requestId);
        printf("OK: VNG_AcceptJoinRequest\n");
    }

    return 0;
}

int searchAndJoinGame()
{
    VNGErrCode err;
    VNGSearchCriteria criteria;
    VNGGameStatus gameStatus[100];
    int numGameStatus;
    int i;
    char joinStr[16];

    memset((void*)&criteria, 0, sizeof(VNGSearchCriteria));

    criteria.gameId = VOICE_GAME_ID;   
    criteria.domain = VNG_SEARCH_INFRA;   
    criteria.maxLatency = 500;  // 300ms RTT
    criteria.cmpKeyword = VNG_CMP_DONTCARE;

    strcpy(joinStr, "hello");

    numGameStatus = 100;
    VNG_SearchGames(&vng, &criteria, gameStatus, &numGameStatus, 0, VNG_TIMEOUT);

    if (numGameStatus > 0) {
        for (i = 0; i < numGameStatus; i++) {
            err = VNG_JoinVN(&vng,
                gameStatus[i].gameInfo.vnId,
                joinStr,
                &vn,
                NULL,
                0, 
                3*1000);
            if (err == VNG_OK) return 0;
        }
    }

    return -1;
}

int main(int argc, char** argv)
{
    int server = 0;
    if (argc > 1) {
        server = atoi(argv[1]);
    }

    if (SetupVNG() == 0) {
        printf("OK: SetupVNG()\n");

        if (server) {
            if (hostNewGame() == 0) {
                printf("OK: hostNewGame()\n");
            }
        } else {
            printf("searching.");
            while (searchAndJoinGame() != 0) printf(".");
            printf("\nOK: searchAndJoinGame()\n");
        }

    }

    VNG_ActivateVoice(&vn, VNG_VOICE_ENABLE, VN_MEMBER_ALL);
    VNG_AdjustVolume(&vng, VNG_VOL_SET, 0);
    VNG_SelectVoiceCodec(&vng, VNG_VOICE_CODEC_ADPCM32);

    while (1) {
        if ((VN_Owner(&vn) == VN_Self(&vn))
            && VN_NumMembers(&vn) < 4) 
        {
            printf("waitForPlayers()...\n");
            waitForPlayers();
        } else {
            Sleep(5000);
        }
    }

    return 0;
}

/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __EC_I_H__
#define __EC_I_H__

#include "ec.h"
#include "shr_plat.h"
#include "shr_th.h"
#include "shr_trace.h"
#include "ec_common.h"
#include "vng_p.h"
#include "vng_priv.h"
#include "ecp.h"



#if !defined(_GBA) && !defined(_SC)
#pragma pack(push, 4)
#endif


#ifdef __cplusplus
extern "C" {
#endif


#define EC_NO_AUTO_CANCEL


#define ECP_DEFAULT_PROXY_VN_JOIN_TIMEOUT  (15*1000)

/* RPCs do not wait for internet access, but do
 * need to wait for locks and file access on windows
 *
 * Normally they return in 10s of millisecs, but we
 * don't want to timeout if one is going to finish, but
 * windows delays it for a few secods for some reason.
 * 
 */
#define ECP_DEFAULT_PROXY_VN_RPC_TIMEOUT  (15*1000)




// Trace sub-group definitions
//
//     0  - default miscellaneous group
//     1  - find and join ECP VN
//     2  - async operation

#define SG_MISC  0
#define SG_ECPVN 1
#define SG_AOP    2

#define MISC     _TRACE_EC, SG_MISC
#define ECPVN    _TRACE_EC, SG_ECPVN
#define AOP      _TRACE_EC, SG_AOP

#define _TRACE_EC _TRACE_APP




typedef s32 (*ECAsyncOpFunc) (void *args);
typedef s32 (*ECAsyncCancelFunc) (void *args);

typedef enum {
    EC_NOT_BUSY = 0,  /* state initial value is zero */
    EC_BUSY_LIST_TITLES,
    EC_BUSY_GET_TITLE_DETAILS,
    EC_BUSY_GET_EC_META,
    EC_BUSY_LIST_SUB_PRICINGS,
    EC_BUSY_SUBSCRIBE,
    EC_BUSY_REDEEM,
    EC_BUSY_SYNC_TICKETS,
    EC_BUSY_UPDATE_STATUS,
    EC_BUSY_PURCHASE_TITLE,

} ECState;

typedef enum {
    G_START = 0,
    G_CHK_CACHE,
    G_START_OP,
    G_OPEN,
    G_READ,
    G_CLOSE,
    G_DONE

} ECGetCachedInfoState;

typedef struct {
    u16 len;
 /* u8  chars[len]; */

} ECContentName;


/*  When libec uses the term file in symbol names it refers to
 *  a real or virtual file maintained by the ec proxy on the attached PC.
 *
 *  In the case of content and other data that is persistent
 *  across executions of the ec proxy, they are are real files
 *  in the PC files system.
 *
 *  In other cases (like check in/out data) they are virtual files
 *  that are managed by the ec proxy but not actually ever written
 *  to the PC file system.
 */
typedef struct {
    u32                      dontCloseOnFull; /* don't close on ret.buf full */
    ECCacheStatus            cacheStatus;
    u32                      nCacheStatus; /* 0 means haven't got cacheSatus */
    ECGetCachedInfoState     state;
} FileStatus;


typedef struct {
    const void *head;
    u32         headLen;    /* size of *head */
    const void *body;
    u32         bodyLen;    /* size of *body */
    bool32      needAlloc;  /* true means copy to NC accessable mem needed when
                               only head or body is specified */
    bool32      headOnly;   /* true means only send head even if bodyLen != 0 */
} SendInfo;


typedef struct {
    void  *buf;         /* where to return info */
    u32    nBufRecs;    /* num data records that will fit at *buf */
    u32    recLen;      /* size of a record */
    u32   *nRecs;       /* where to store num of returned records */
    u32    len;         /* total bytes returned in buf */
    s32    lenLast;     /* *retLen from most recent VN_SendRPC
                         * < 0 means VN_SendRPC not started or failed */
} ReturnInfo;


typedef struct {
    ECPInfoType  infoType;  /* initialize before sndRpc or start async Op */
    SendInfo     snd;       /* ditto for all snd members                */
    ReturnInfo   ret;       /* ditto except for ret.len and ret.lenLast */
} RPCInfo;
/* RCPInfo.ret.len is running total across multiple sndRpc's indicating
 * how much of ret->buf has been filled.  It is set to 0 by initAsyncOp.
 * When used for non-async operation, it should be set to zero before
 * the first(or only) call to sndRpc() to incrementally fill ret->buf.
 */



/*  ECAsyncOp contains preserved state common to all async operations.
 *  Each specific operation persistent state is the catenation of an
 *  ECAsyncOp and op specific info.
 *
 *  rpc.type identifies the type of info associated with the async op.
 *  e.g., List of titles to cache, get cache status, open, read, close
 *  rpc.snd describes what to get and how to get it via rpc
 *  rpc.ret describes what, where, and what status to return
 *  file maintains the cache/open/read/close state of associated ECP info
 */
typedef struct {
    RPCInfo            rpc;
    FileStatus         file;
    u32                size;
    s32                retval;
    ECAsyncOpFunc      asyncOpFunc;
    ECAsyncCancelFunc  cancelFunc;
    ECState            state;

} ECAsyncOp;


/*  Definitions of persistent data associated with specific async ops
 *
 *  Each async op persistent data type begins with an ECAsyncOp.
*/

typedef struct {
    ECAsyncOp         op;
    ECPListTitlesArg  snd;

} ECListTitlesOp;

typedef struct {
    ECAsyncOp           op;
    ECPTitleDetailsArg  snd;

} ECTitleDetailsOp;

typedef struct {
    ECAsyncOp   op;
    /* no head or body data */

} ECMetaOp;

typedef struct {
    ECAsyncOp         op;
    /* no head or body data */

} ECListSubscriptionPricingsOp;


/* ECSubscribe.ret defines the start of the ret data.  NC requests only
 * sizeof ECPTickeRet on the 1st read of the cached response data.
 * After receiving ECPSubscribeRet, it knows how much to request to be
 * directly written to *ticket and *certs and what to write to *certsSize.
 *
 * nRetRecs is used as tmp storage.
 */
typedef enum {
    TRS_GetRet = 0, TRS_GetTicket, TRS_GetCerts 
} ECTicketRetState;


typedef struct {
    ECAsyncOp         op;
    ECTicketRetState  state;
    void             *tickets;
    u32              *nTicketsPtr;    // NULL if nTickets not returned to caller
    u32               nTicketsValue;  // num tickets that can be stored at tickets
    u32               minTickets;     // 1 for redeem, subscribe,purchase, 0 for sync
    void             *certs;
    u32              *certsSizePtr;   // where to return actual certsSize value
    u32               certsSizeValue; // bytes that can be stored at certs
    u32               nRetRecs;       // rpc.ret.nRecs points here
    u32              *retBufSize;     // points to snd.retBufSize to tell ecp 
                                      // max amount to return
    ECPTickeRet       ret;

} ECTicketsAndCertsOp;

typedef struct {
    ECTicketsAndCertsOp tac;
    ECPSubscribeArg     snd;

} ECSubscribeOp;



typedef struct {
    ECTicketsAndCertsOp tac;
    ECPRedeemArg        snd;

} ECRedeemECardOp;


typedef struct {
    ECTicketsAndCertsOp tac;
    ECPPurchaseTitleArg snd;

} ECPurchaseTitleOp;



typedef struct {
    ECTicketsAndCertsOp tac;
    ECPSyncTicketsArg   snd;

} ECSyncTicketsOp;




typedef struct {
    ECAsyncOp    op;
    u32          nConsumptions;
    /* Consumptions are sent per ECPUpdateStatusArg */

} ECUpdateStatusOp;







#ifdef __cplusplus
}
#endif //__cplusplus



#if !defined(_GBA) && !defined(_SC)
#pragma pack(pop)
#endif


#endif /*__EC_I_H__*/

/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#include "ec_i.h"


ECError EC_VngToECError (VNGErrCode ec)
{
    ECError rv;

    switch (ec) {
        case VNG_OK:
            rv = EC_ERROR_OK;
            break;
        default:
            rv = -(ec + 1000);
            break;
    }
    return rv;
}


char* vngErrStr (VNGErrCode ec,  char *buf, size_t bufLen)
{
    VNGErrCode  rc;

    buf[0] = '\0';
    if ((rc = VNG_ErrMsg (ec, buf, bufLen))) {
        trace (INFO, MISC, "\nVNG_ErrMsg(%d) returned %d: %s\n", ec, rc, buf);
    }
    return buf;
}



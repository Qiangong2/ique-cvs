/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */

#include "ec_i.h"

static
s32  genericAsyncCancel (ECAsyncOp *op);

static
s32  defaultAsyncOpFunc (ECAsyncOp *op);



static VN __ecProxyVN = { _VNG_INVALID_VNGVNID,   
                          _VNG_INVALID_VNGID,     
                          _VNG_INVALID_VNGNETADDR };

static VNId __ecProxyVNId = { VNP_INVALID_DEVICE_ID, VNP_INVALID_NET};
static bool32 __ecProxyConnected;
static VNGTimeout __ecProxyJoinTimeout = ECP_DEFAULT_PROXY_VN_JOIN_TIMEOUT;
static VNGTimeout __ecProxyRPCTimeout  = ECP_DEFAULT_PROXY_VN_RPC_TIMEOUT;

static ECAsyncOp __defaultAsyncOp = { {0}, {0}, sizeof(ECAsyncOp),
                                       EC_ERROR_NOT_BUSY,
                                      (ECAsyncOpFunc) defaultAsyncOpFunc,
                                      (ECAsyncCancelFunc) genericAsyncCancel,
                                       EC_NOT_BUSY};

static ECAsyncOp *__curAsyncOp = &__defaultAsyncOp;


 
/* Buffer big enough for any async op state data */
static u8 __ecAOPBuf[2048] = {1};

void *allocAOP (size_t size)
{
    /* only one async op can be active */
    return (void*) __ecAOPBuf;
}

void freeAOP (ECAsyncOp* op)
{
    return;
}

const char* getItText(ECPInfoType  infoType)
{
    static char* text[] = {
        "cid",
        "EC meta data",
        "List Titles",
        "Title Details",
        "List subscription prices",
        "Subscribe",
        "Update status",
        "Redeem",
        "Sync Tickets",
        "Purchase Title",
    };

    int i = infoType/100 - 1;

    char *rv = "<unknown>";

    if (i > (sizeof text/sizeof text[0])) {
        trace (ERR, MISC, "infoType %d  unknown\n", infoType);
    } else {
        rv = text[i];
    }

    return rv;
}

#ifdef _GBA

static
ECError lock()
{
    return EC_ERROR_OK;
}

ECError unlock()
{
    return EC_ERROR_OK;
}

#else

static bool32 __ec_mutex_initialized;
static _SHRMutex  __ec_mutex;
static _SHRMutex *__mutex = &__ec_mutex;

static
ECError lock ()
{
    ECError     rv = EC_ERROR_OK;
   _SHRError    er;

    if (!__ec_mutex_initialized) {
        /* initialize mutex to locked */
        if ((er = _SHR_mutex_init(__mutex, _SHR_MUTEX_RECURSIVE, 0, true))) {
            trace (ERR, MISC, "lock: _SHR_mutex_init returned %d\n", rv);
            rv = EC_ERROR_FAIL;
        } else {
            __ec_mutex_initialized = true;
        }
    }
    else {
        er = _SHR_mutex_lock (__mutex);
        assert (!er);
        if (er) {
            /* Should never occur. */
            trace (ERR, MISC, "lock: _SHR_mutex_lock returned %d\n", er);
            rv = EC_ERROR_FAIL;
        }
    }

    return rv;
}


static
ECError unlock ()
{
    ECError rv = EC_ERROR_OK;

    if (!__ec_mutex_initialized) {
       trace (ERR, MISC, "unlock: mutex not initialized\n");
       rv = EC_ERROR_FAIL;
    }
    else {
       _SHRError er = _SHR_mutex_unlock (__mutex);
        assert (!er);
        if (er) {
            /* Should never occur. */
            trace (ERR, MISC, "unlock: _SHR_mutex_unlock returned %d\n", er);
            rv = EC_ERROR_FAIL;
        }
    }

    return rv;
}


#endif /* ifndef _GBA */



static
void  initAsyncOp (ECAsyncOp          *op,
                   ECAsyncOpFunc       asyncOpFunc,
                   ECAsyncCancelFunc   cancelFunc,
                   ECState             newState,
                   u32                 asyncOpDataLen)
{
    memset(op, 0, asyncOpDataLen);
    op->rpc.snd.headOnly = true;  /* for all async op rpcs except cache */
    op->file.cacheStatus.cachedSize = EC_ERROR_INVALID;
    op->file.cacheStatus.totalSize = EC_ERROR_INVALID;
    op->file.state = G_START_OP;
    op->size = asyncOpDataLen;
    op->retval = EC_ERROR_NOT_DONE;
    op->asyncOpFunc = asyncOpFunc;
    op->cancelFunc = cancelFunc;
    op->state = newState;

    __curAsyncOp = op;
}


static
ECAsyncOp* curAsyncOp ()
{
    return __curAsyncOp;
}



static
bool32 isActiveAsyncOp ()
{
    ECAsyncOp* op = curAsyncOp();
    return op->retval == EC_ERROR_NOT_DONE;
}




static
s32  defaultAsyncOpFunc (ECAsyncOp *op)
{
    return EC_ERROR_NOT_BUSY;
}


static VNG  __ec_vng = {_VNG_INVALID_VNGID};
static VNG  *__vng;


static
ECError getVng (VNG **vng)
{

    ECError    rv = EC_ERROR_OK;
    VNGErrCode ec;

    if (!__vng) {
        if ((ec = VNG_Init(&__ec_vng, NULL))) {
            trace (ERR, MISC, "getVng: VNG_Init returned %d\n", ec);
            rv = EC_VngToECError (ec);
        } else {
            __vng = &__ec_vng;
        }
    }

    *vng = __vng;

    return rv;
}


static
ECError  joinECProxyVN ()
{
    ECError     rv;
    VNGErrCode  ec;
    VNG        *vng;
    char        denyReason [64];
    int         i;
    u32         n;

    VNGSearchCriteria  crit;
#ifdef _GBA
    /*   gameStatus[] Does not need to be thread safe on GBA,
     *  but does need to be initialized global mem for GBA.
     */
    static
    VNGGameStatus  gameStatus[2] = {{{{VNP_INVALID_DEVICE_ID}}}};
#else
    VNGGameStatus  gameStatus[2];
#endif

    VNGGameStatus     *ncp = &gameStatus[0];

    if (__ecProxyConnected && VN_State(&__ecProxyVN) == VN_OK) {
        rv = EC_ERROR_OK;
        goto end;
    }

    if ((rv = getVng(&vng))) {
        goto end;
    }

    crit.domain = VNG_SEARCH_ADHOC;
    crit.gameId = ECP_GAME_ID;
    crit.maxLatency = 30000;
    crit.cmpKeyword = VNG_CMP_STR;
    memcpy(crit.keyword, ECP_GAME_KEYWORD, sizeof ECP_GAME_KEYWORD);

    for (i = 0; i < VNG_MAX_PREDICATES; i++) {
        crit.pred[i].cmp = VNG_CMP_DONTCARE;
    }

    for (i = ECP_GAME_ATTR_ID;  i >= 0;  --i) {
        crit.pred[i].attrId = i;
        crit.pred[i].attrValue = ECP_GAME_ATTR_VALUE - i;
        crit.pred[i].cmp = VNG_CMP_EQ;
    }

    n = sizeof gameStatus / sizeof gameStatus[0];

    /* timeout is not used for adhoc searches */
    if ((ec = VNG_SearchGames(vng, &crit, gameStatus, &n, 0, VNG_NOWAIT))) {
        trace (ERR, ECPVN, "VNG_SearchGames()  Failed: %d\n", ec);
        rv = EC_VngToECError (ec);
        goto end;
    }

    if (n != 1) {
        int trace_level;
        if (!n) {
            rv = EC_ERROR_NO_PROXY;
            trace_level = INFO;
        } else {
            rv = EC_ERROR_FAIL;
            trace_level = ERR;
        }
        trace (trace_level, ECPVN, "VNG_SearchGames() found %d NC Proxy services\n", n);
        goto end;
    }

    if (ncp->gameStatus != ECP_GAME_STATUS
            || ncp->numPlayers != ECP_NUM_PLAYERS) {

        rv = EC_ERROR_FAIL;
        trace (ERR, ECPVN,
               "VNG_SearchGames() found ECProxy service with game status %d "
               "and num players %d\n", ncp->gameStatus, ncp->numPlayers);
        goto end;
    }

    trace (INFO, ECPVN, "Found NC Proxy service\n");
    __ecProxyVNId = ncp->gameInfo.vnId;

    if ((ec = VNG_JoinVN (vng, __ecProxyVNId, "Please let me join !",
                               &__ecProxyVN, denyReason, sizeof denyReason,
                               __ecProxyJoinTimeout))) {
        trace (ERR, ECPVN, "VNG_JoinVN(__ecProxyVNId) Failed %d  "
                            "deviceId: 0x%08x.0x%08x   netId: 0x%08x\n", ec,
                            (int)(__ecProxyVNId.deviceId >> 32),
                            (int)__ecProxyVNId.deviceId, __ecProxyVNId.netId);
        rv = EC_VngToECError (ec);
        goto end;
    }

    trace (INFO, ECPVN, "VNG_JoinVN(__ecProxyVNId) Success  "
                        "deviceId: 0x%08x.0x%08x   netId: 0x%08x\n",
                         (int)(__ecProxyVNId.deviceId >> 32),
                         (int)__ecProxyVNId.deviceId, __ecProxyVNId.netId);

    __ecProxyConnected = true;

end:
    return rv;
}



static
ECError  getECProxyVN(VN **vn)
{
    /*  If ECProxy vn already connected, return it.
     *  else try to connect.
     */
    ECError rv = EC_ERROR_OK;

    if (__ecProxyConnected && VN_State(&__ecProxyVN) != VN_OK) {
        __ecProxyConnected = false;
    }
    if (__ecProxyConnected || !(rv = joinECProxyVN())) {
        *vn = &__ecProxyVN;
    } else {
        *vn = NULL;
    }

    return rv;
}


static
ECError  leaveECProxyVN ()
{
    VNGErrCode ec;
    ECError rv = EC_ERROR_OK;

    if (__ecProxyConnected) {
        ec = VNG_LeaveVN(__vng, &__ecProxyVN);
        rv = EC_VngToECError (ec);
        __ecProxyConnected = false;
    }

    return rv;
}


ECError EC_Init ()
{
    VN  *vn;

    return getECProxyVN(&vn);
}


ECError EC_Fini ()
{
    VNGErrCode ec;
    ECError rv = EC_ERROR_OK;

    if (__vng) {
        rv = leaveECProxyVN();
        ec = VNG_Fini (__vng);
        if (ec) {
            rv = EC_VngToECError (ec);
        }
        __vng = NULL;
    }

    return rv;
}
                 




/*  Mem allocated with VNG_GBAlloc must be freed before 
 *  returning from the func that allocated the mem and any
 *  free on the gba, frees all allocated mem.
 *
 *  Therefore it can not be used for mem that needs to be
 *  kept during ongoing asynchronous operaton. And it can
 *  not be used in routines called by a routine that uses
 *  VNG_GBAlloc.
 *
 *  In libec, it will only be used in sndRPC
 */
ECError  sndRPC (VNMember       toMemb,
                 VNServiceTag   serviceTag,
                 SendInfo      *snd,
                 ReturnInfo    *ret)
{
    ECError         rv = EC_ERROR_OK;
    VNGErrCode      ec;
    char            msg[80];
    char            nullChar = '\0';
    size_t          msgLen = sizeof msg;
    bool32          isBufAllocated = false;
    u8             *sBuf;
    void           *rBuf;
    size_t          sndLen;
    size_t          bodyLen;
    size_t         *retLen;
    size_t          retBufLen;
    int32_t         optData = 0;
    VN             *vn;

    if (ret) {
        ret->lenLast = -1;
        if (ret->nRecs && ret->len == 0) {
            /* init num returned records on 1st call. But, async api should
             * do this before starting op as errors before getting here (like
             * in getNetContent on a getCachedStatus) might result in return
             * before *ret->nRecs has been zeored
             */
            *ret->nRecs = 0;
        }
    }

    if ((rv = getECProxyVN(&vn))) {
        return rv;
    }

    if (!snd) {
        sBuf = NULL;
        sndLen = 0;
    }
    else {
        if (snd->headOnly) {
            bodyLen = 0;
        } else {
            bodyLen = snd->bodyLen;
        }
        sndLen = snd->headLen + bodyLen;

        if (sndLen > VN_MAX_MSG_LEN) {
            return EC_ERROR_INVALID;
        }

        if ((snd->headLen && bodyLen)
                || ((snd->headLen || bodyLen) && snd->needAlloc)) {

            if (!(sBuf = VNG_GBAlloc (sndLen))) {
                return EC_ERROR_NOMEM;
            }
            isBufAllocated = true;

            if (snd->headLen) {
                memcpy(sBuf, snd->head, snd->headLen);
            }
            if (bodyLen) {
                memcpy(&sBuf[snd->headLen],  snd->body, bodyLen);
            }
        }
        else {
            sBuf = (u8*)(snd->headLen ? snd->head : snd->body);
        }
    }

    if (!ret) {
        rBuf = NULL;
        retLen = 0;
    } else {
        rBuf = (u8*)ret->buf + ret->len;
        retBufLen = ret->nBufRecs * ret->recLen;
        if (ret->len > retBufLen) {
            trace (ERR, MISC, "sndRPC ret->len > retBufLen\n");
            rv = EC_ERROR_FAIL;
            goto end;
        }
        retBufLen -= ret->len;
        retLen = &retBufLen;
    }

    if ((rv = unlock())) {
        trace (ERR, MISC, "unlock() returned %d\n", rv);
        goto end; /* should never happen */
    }
    ec = VN_SendRPC (vn, toMemb,
                     serviceTag,
                     sBuf, sndLen,
                     rBuf, retLen,
                     &optData,
                     __ecProxyRPCTimeout);
    if ((rv = lock())) {
        trace (ERR, MISC, "lock() returned %d\n", rv);
        goto end; /* should never happen */
    }
    if (ec) {
        rv = EC_VngToECError(ec);
        trace (INFO, MISC, "VN_SendRPC returned %d: %s\n",
                            ec, vngErrStr(ec, msg, msgLen));
        goto end;
    }

    if (optData) {
        if (optData > 0) {
            rv = EC_VngToECError(optData);
            vngErrStr(optData, msg, msgLen);
        } else {
            rv = optData;
            memcpy(msg, &nullChar, sizeof nullChar);
        }
        trace (INFO, MISC, "VN_SendRPC returned optData %d  %s\n",
                              optData, msg);
    }

    if (ret) {
        ret->lenLast = (s32) *retLen;
        ret->len += ret->lenLast;
        if (ret->nRecs) {
            *ret->nRecs = ret->len / ret->recLen;
        }
    }

end:
    if (isBufAllocated) {
        VNG_GBFree (sBuf);
    }
    return rv;
}



ECError  getCachedStatus (ECPInfoType     infoType,
                          SendInfo       *snd,          /* head,body mba */
                          ECCacheStatus  *cacheStatus,  /* mba */
                          u32            *nCacheStatus)
{
    ECError         rv = EC_ERROR_OK;
    u32             i;
    ReturnInfo      ret;

    if ( !cacheStatus || !nCacheStatus || !*nCacheStatus) {
        rv = EC_ERROR_INVALID;
        goto end;
    }

    for (i = 0;  i < *nCacheStatus;  ++i) {
        cacheStatus[i].cachedSize = EC_ERROR_NOT_DONE;
        cacheStatus[i].totalSize = EC_ERROR_INVALID;
    }

    ret.buf = cacheStatus;
    ret.nBufRecs = *nCacheStatus;
    ret.recLen = sizeof *cacheStatus;
    ret.nRecs = nCacheStatus;
    ret.len = 0;

    rv = sndRPC (VN_MEMBER_OWNER,
                 ECP_RQ_GET_STAT + infoType,
                 snd,
                 &ret);

end:
    return rv;
}



ECError getNetInfo (ECAsyncOp *op)
{
    ECError                  rv = EC_ERROR_OK;
    ECError                  ece;
    u32                      retBufLen;
    SendInfo                *snd;
    ReturnInfo              *ret;
    ECPInfoType              infoType;
    ECGetCachedInfoState     state;
    ECCacheStatus           *cacheStatus;
    u32                     *nCacheStatus;
    bool                     cacheStarted = false;

    state = op->file.state;
    cacheStatus = &op->file.cacheStatus;
    nCacheStatus = &op->file.nCacheStatus;
    snd = &op->rpc.snd;
    ret = &op->rpc.ret;
    infoType = op->rpc.infoType;
    retBufLen = ret->nBufRecs * ret->recLen;

    if (state == G_START) {
        state = op->file.state = G_CHK_CACHE;
    } else if (state == G_CHK_CACHE) {
        cacheStarted = true;
    }
            
    do switch (state) {
        case G_CHK_CACHE:
            *nCacheStatus = 1;
            rv = getCachedStatus (infoType, snd, cacheStatus, nCacheStatus);
            if (0>rv) {
                trace (ERR, AOP,
                       "getNetInfo getCachedStatus %s returned %d\n",
                       getItText(infoType), rv);
                state = G_DONE;
            } else if (*nCacheStatus != 1) {
                trace (ERR, AOP,
                       "getNetInfo getcachedStatus %s returned %d nCacheStatus\n",
                       getItText(infoType), *nCacheStatus);
                rv = EC_ERROR_FAIL;
            } else if (cacheStatus->cachedSize < 0) {
                if (cacheStatus->cachedSize == EC_ERROR_NOT_CACHED ) {
                    if (cacheStarted) {
                        trace (ERR, AOP, "getNetInfo %s cache started, but "
                            "cacheStatus->cachedSize is EC_ERROR_NOT_CACHED\n",
                            getItText(infoType));
                        rv = EC_ERROR_FAIL;
                        state = G_DONE;
                    } else {
                        state = G_START_OP;
                    }
                }
                else if (cacheStarted) {
                    trace (ERR, AOP, "getNetInfo %s cacheStatus->cachedSize is %d\n",
                        getItText(infoType), cacheStatus->cachedSize);
                    rv = cacheStatus->cachedSize; 
                    state = G_DONE;
                }
                else {
                    state = G_START_OP;
                }
            } else if (cacheStatus->cachedSize == cacheStatus->totalSize) {
                if (cacheStatus->totalSize == 0) {
                    ret->lenLast = 0;
                    ret->len = 0;
                    if (ret->nRecs) {
                        *ret->nRecs = 0;
                    }
                    state = G_DONE;
                }  else {
                    state = G_OPEN;
                }
            } /* Else file is bein downloaded and
               * cacheStatus->cachedSize of cacheStatus->totalSize
               * have been downloaded.
               */
            break;
            
        case G_START_OP:
            /* tell ECP to do something and cache the response */
            snd->headOnly = false;
            rv = sndRPC (VN_MEMBER_OWNER,
                            ECP_RQ_CACHE + infoType,
                            snd,
                            NULL);
            snd->headOnly = true; /* for all async rpcs except cache request */
            if (0>rv) {
                trace (ERR, AOP,
                        "getNetInfo cache request sndRPC for %s returned %d\n",
                        getItText(infoType), rv);
                state = G_DONE;
            } else {
                cacheStarted = true;
                cacheStatus->cachedSize = 0;
                cacheStatus->totalSize = EC_ERROR_INVALID;
                state = G_CHK_CACHE;
            }
            break;

        case G_OPEN:
            /* tell ECP to open a cached info file */
            rv = sndRPC (VN_MEMBER_OWNER,
                         ECP_RQ_OPEN + infoType,
                         snd,
                         NULL);
            if (0>rv) {
                trace (ERR, AOP,
                       "getNetInfo sndRPC to open %s returned %d\n",
                       getItText(infoType), rv);
                state = G_DONE;
            } else {
                state = G_READ;
            }
            break;

        case G_READ:
            rv = sndRPC (VN_MEMBER_OWNER,
                         ECP_RQ_READ + infoType,
                         snd,
                         ret);
            if (0>rv) {
                trace (ERR, AOP,
                       "getNetInfo sndRPC to read %s returned %d\n",
                       getItText(infoType), rv);
                state = G_CLOSE;
            } else if (ret->lenLast == 0) {
                state = G_CLOSE;
            } else {
                op->file.state = G_READ;
                if (ret->len == cacheStatus->totalSize
                    || (ret->len == retBufLen
                            && !op->file.dontCloseOnFull)) { 
                    state = G_CLOSE;
                }
            }
            break;

        case G_CLOSE:
            /* tell ECP to close a cached info file */
            ece = sndRPC (VN_MEMBER_OWNER,
                          ECP_RQ_CLOSE + infoType,
                          snd,
                          NULL);
            if (0>ece) {
                trace (ERR, AOP,
                       "getNetInfo sndRPC to close %s returned %d\n",
                       getItText(infoType), ece);
                if (!rv) { rv = ece; }
                state = G_DONE;
            } else {
                state = G_DONE;
            }
            break;

        case G_DONE:
            rv = EC_ERROR_FAIL; /* unexpected */
            break;

        default:
            trace (ERR, AOP, "getNetInfo %s invalid state %d\n",
                              getItText(infoType), state);
            state = G_DONE;
            rv = EC_ERROR_FAIL;
            break;

    }
    while (state != G_DONE && state != op->file.state);

    if (state == G_DONE) {
        op->file.state = state;
    } else {
        rv = EC_ERROR_NOT_DONE;
    }
    return rv;
}



/* must be locked on entry */
static
s32 cancelOperation (ECAsyncOp *op)
{
    ECError rv = EC_ERROR_OK;

    if (op && op != &__defaultAsyncOp) {
        if (op->cancelFunc) {
            rv = op->cancelFunc(op);
        }
        freeAOP (op);
    }
    else {
        SendInfo snd;
     
        memset (&snd, 0, sizeof snd);

        rv = sndRPC (VN_MEMBER_OWNER,
                     ECP_ST_CANCEL_WS_OPS,
                     &snd,
                     NULL);
    }
    __curAsyncOp = &__defaultAsyncOp;

    return rv;
}

/*  genericAsyncCancel() can be used as the op->canceFunc
 *  when there is no action needed that is specific to the
 *  async op type. Non specific actions are performed by
 *  cancelOperation() after it calls op->cancelFunc().
 *
 *  must be locked on entry
 *
 */
static
s32  genericAsyncCancel (ECAsyncOp *op)
{
    ECError rv;

    rv = sndRPC (VN_MEMBER_OWNER,
                ECP_RQ_CANCEL + op->rpc.infoType,
                &op->rpc.snd,
                NULL);

    return rv;
}


ECError EC_CancelOperation ()
{
    ECError rv = EC_ERROR_OK;
    if (!lock()) {
        rv = cancelOperation (curAsyncOp());
        unlock();
    }
    return rv;
}



s32 EC_GetETicketSize ()
{
    return ES_TICKET_SIZE;
}


s32  EC_GetReturnValue ()
{
    ECError     rv;
    ECAsyncOp  *op;

    if (!(rv = lock())) {
        op = curAsyncOp();
        if (op->retval == EC_ERROR_NOT_DONE) {
            op->retval = op->asyncOpFunc (op);
        }
        rv = op->retval;
        unlock();
    }
    return rv;
}


static
ECError  ecListTitles (ECAsyncOp *op)
{
    return getNetInfo (op);
}


ECError  EC_ListTitles (ECTitleKind        titleKind,
                        ECPricingKind      pricingKind,
                        ECListTitlesInfo  *titles,
                        u32               *nTitles)
{
    ECError          rv;
    VNG             *vng;
    ECListTitlesOp  *a; 
    ECAsyncOp       *op;

    u32  n = nTitles ? *nTitles : 0;
    u32  skip = 0;  /* currently not used */
    int  channelId = -1; /* optional arg, -1 means not provided */

    if (!titles || !nTitles) {
        return EC_ERROR_INVALID;
    }

    if ((rv = lock())) {
        return rv; 
    }

    if (isActiveAsyncOp()) {
        rv = EC_ERROR_BUSY;
        goto end;
    }

    if ((rv = getVng(&vng))) {
        goto end;
    }

    a = allocAOP (sizeof *a);
    op = &a->op;
    initAsyncOp (op, (ECAsyncOpFunc) ecListTitles,
                     (ECAsyncCancelFunc) genericAsyncCancel,
                      EC_BUSY_LIST_TITLES,
                      sizeof *a);

    op->file.state = G_START;
    op->rpc.infoType = ECP_IT_LIST_TITLES;
    op->rpc.snd.head = &a->snd;
    op->rpc.snd.headLen = sizeof a->snd;

    op->rpc.ret.buf = titles;
    op->rpc.ret.nBufRecs = n;
    op->rpc.ret.recLen = sizeof *titles;
    op->rpc.ret.nRecs = nTitles;
    *nTitles = 0;

    a->snd.titleKind = titleKind;
    a->snd.pricingKind = pricingKind;
    a->snd.channelId = channelId;
    a->snd.nTitles = n;
    a->snd.skip = skip;

    rv = op->retval = op->asyncOpFunc (op);

end:
    unlock();

    return rv;   
}


static
ECError  ecTitleDetails (ECAsyncOp *op)
{
    return getNetInfo (op);
}


ECError  EC_GetTitleDetails (ESTitleId          titleId,
                             ECTitleDetails    *details)
{
    ECError            rv;
    VNG               *vng;
    ECTitleDetailsOp  *a; 
    ECAsyncOp         *op;

    if (!details) {
        return EC_ERROR_INVALID;
    }

    if ((rv = lock())) {
        return rv; 
    }

    if (isActiveAsyncOp()) {
        rv = EC_ERROR_BUSY;
        goto end;
    }

    if ((rv = getVng(&vng))) {
        goto end;
    }

    a = allocAOP (sizeof *a);
    op = &a->op;
    initAsyncOp (op, (ECAsyncOpFunc) ecTitleDetails,
                     (ECAsyncCancelFunc) genericAsyncCancel,
                      EC_BUSY_GET_TITLE_DETAILS,
                      sizeof *a);

    op->file.state = G_START;
    op->rpc.infoType = ECP_IT_TITLE_DETAILS;
    op->rpc.snd.head = &a->snd;
    op->rpc.snd.headLen = sizeof a->snd;

    op->rpc.ret.buf = details;
    op->rpc.ret.nBufRecs = 1;
    op->rpc.ret.recLen = sizeof *details;

    a->snd.titleId = titleId;

    rv = op->retval = op->asyncOpFunc (op);

end:
    unlock();

    return rv;   
}



static
ECError  ecECMeta (ECAsyncOp *op)
{
    return getNetInfo (op);
}


ECError  EC_GetECMeta (ECMeta  *meta)
{
    ECError            rv;
    VNG               *vng;
    ECAsyncOp         *op;

    if (!meta) {
        return EC_ERROR_INVALID;
    }

    if ((rv = lock())) {
        return rv; 
    }

    if (isActiveAsyncOp()) {
        rv = EC_ERROR_BUSY;
        goto end;
    }

    if ((rv = getVng(&vng))) {
        goto end;
    }

    op = allocAOP (sizeof *op);
    initAsyncOp (op, (ECAsyncOpFunc) ecECMeta,
                     (ECAsyncCancelFunc) genericAsyncCancel,
                      EC_BUSY_GET_EC_META,
                      sizeof *op);

    op->rpc.infoType = ECP_IT_EC_META;

    op->rpc.ret.buf = meta;
    op->rpc.ret.nBufRecs = 1;
    op->rpc.ret.recLen = sizeof *meta;

    rv = op->retval = op->asyncOpFunc (op);

end:
    unlock();

    return rv;   
}



static
ECError  ecListSubscriptionPricings (ECAsyncOp *op)
{
    return getNetInfo (op);
}


ECError  EC_ListSubscriptionPricings (ECSubscriptionPricing  *pricings,
                                      u32                    *nPricings)
{
    ECError            rv;
    VNG               *vng;
    ECAsyncOp         *op;

    u32  n = nPricings ? *nPricings : 0;

    if (!pricings || !nPricings) {
        return EC_ERROR_INVALID;
    }

    if ((rv = lock())) {
        return rv; 
    }

    if (isActiveAsyncOp()) {
        rv = EC_ERROR_BUSY;
        goto end;
    }

    if ((rv = getVng(&vng))) {
        goto end;
    }

    op = allocAOP (sizeof *op);
    initAsyncOp (op, (ECAsyncOpFunc) ecListSubscriptionPricings,
                     (ECAsyncCancelFunc) genericAsyncCancel,
                      EC_BUSY_LIST_SUB_PRICINGS,
                      sizeof *op);

    op->file.state = G_START;
    op->rpc.infoType = ECP_IT_LIST_SUB_PRICES;

    op->rpc.ret.buf = pricings;
    op->rpc.ret.nBufRecs = n;
    op->rpc.ret.recLen = sizeof *pricings;
    op->rpc.ret.nRecs = nPricings;
    *nPricings = 0;

    rv = op->retval = op->asyncOpFunc (op);

end:
    unlock();

    return rv;   
}




static
ECError  ecGetTicketsAndCerts (ECAsyncOp *op)
{
    ECError rv;
    ECTicketsAndCertsOp *tac = (ECTicketsAndCertsOp*) op;
    
    while (1) {
        rv = getNetInfo (op);
        if (tac->state == TRS_GetRet) {

            if (rv && rv != EC_ERROR_NOT_DONE) {
                break;
            }

            if (rv == EC_ERROR_OK) {
                if (tac->nRetRecs != 1) {
                    rv = EC_ERROR_FAIL; // should never occur
                    break;
                }
            }
            else if (tac->nRetRecs < 1) {
                continue;
            }
            else if (tac->nRetRecs > 1) {
                rv = EC_ERROR_FAIL; // should never occur
                break;
            }

            tac->state = TRS_GetTicket;
            if (tac->ret.nTickets < tac->minTickets) {
                trace (ERR, MISC, "Expected %u tickets, "
                    "EC Proxy returned %u tickets and %u certsSize\n",
                    tac->minTickets, tac->ret.nTickets, tac->ret.certsSize);
                rv = EC_ERROR_FAIL;
                break;
            }
            if (tac->ret.nTickets > tac->nTicketsValue
                    || tac->ret.certsSize > tac->certsSizeValue) {
                trace (ERR, MISC, "App provided buffers for %u tickets and certs[%u], "
                    "but EC Proxy says %u tickets and certs[%u] are required\n",
                    tac->nTicketsValue, tac->certsSizeValue, tac->ret.nTickets, tac->ret.certsSize);
                if (tac->nTicketsPtr) {
                    *tac->nTicketsPtr = tac->ret.nTickets;
                }
                if (tac->certsSizePtr) {
                    *tac->certsSizePtr = tac->ret.certsSize;
                }
                rv = EC_ERROR_OVERFLOW;
                break;
            }

            tac->nRetRecs = 0;
            if (tac->ret.nTickets) {
                tac->op.rpc.ret.buf = tac->tickets;
                tac->op.rpc.ret.nBufRecs = tac->nTicketsValue;
                tac->op.rpc.ret.recLen = ES_TICKET_SIZE;
                tac->op.rpc.ret.len = 0;
                tac->op.rpc.ret.lenLast = 0;
                // set snd.retBufSize to limit amount ecp will return
                *tac->retBufSize = ES_TICKET_SIZE * tac->nTicketsValue;
                continue;
            }
        }

        if (tac->state == TRS_GetTicket) {

            if (rv && rv != EC_ERROR_NOT_DONE) {
                break;
            }

            // assumes all tickets are returned in one vn msg

            if (rv == EC_ERROR_OK) {
                if (tac->nRetRecs != tac->ret.nTickets) {
                    rv = EC_ERROR_FAIL; // should never occur
                    break;
                }
            }
            else if (tac->nRetRecs < tac->ret.nTickets) {
                continue;
            }
            else if (tac->nRetRecs > tac->ret.nTickets) {
                rv = EC_ERROR_FAIL; // should never occur
                break;
            }

            if (tac->nTicketsPtr) {
                *tac->nTicketsPtr = tac->nRetRecs;
            }
            tac->state = TRS_GetCerts;
            tac->nRetRecs = 0;
            if (tac->ret.certsSize) {
                tac->op.rpc.ret.buf = tac->certs;
                tac->op.rpc.ret.nBufRecs = tac->certsSizeValue;
                tac->op.rpc.ret.recLen = 1;
                tac->op.rpc.ret.len = 0;
                tac->op.rpc.ret.lenLast = 0;
                // set snd.retBufSize to limit amount ecp will return
                *tac->retBufSize = tac->certsSizeValue;
                continue;
            }
        }

        if (tac->state == TRS_GetCerts) {

            if (rv && rv != EC_ERROR_NOT_DONE) {
                break;
            }

            if (rv == EC_ERROR_OK) {
                if (tac->nRetRecs != tac->ret.certsSize) {
                    rv = EC_ERROR_FAIL; // should never occur
                    break;
                }
            }
            else if (tac->nRetRecs < tac->ret.certsSize) {
                // set snd.retBufSize to limit amount ecp will return
                *tac->retBufSize = tac->certsSizeValue - tac->nRetRecs;
                continue;
            }
            else if (tac->nRetRecs > tac->ret.certsSize) {
                rv = EC_ERROR_FAIL; // should never occur
                break;
            }

            if (tac->certsSizePtr) {
                *tac->certsSizePtr = tac->nRetRecs;
            }

            tac->op.file.state = G_DONE;
            rv = EC_ERROR_OK;

            break;
        }
        if (rv == EC_ERROR_OK) {
            /* should not get here with EC_ERROR_OK */
            rv = EC_ERROR_FAIL;
        }
        break;
    }

    return rv;
}


ECError  EC_Subscribe  (s32          itemId,
                        s32          channelId,
                        ECPrice      price,
                        const char  *eCard,
                        s32          subscriptionLength,
                        ECTimeUnit   subscriptionTimeUnit,
                        const u8     deviceCert[ES_DEVICE_CERT_SIZE],
                        void        *ticket,
                        void        *certs,
                        u32         *certsSize)
{
    ECError        rv;
    VNG           *vng;
    ECSubscribeOp  *a; 
    ECAsyncOp      *op;

    u32        curLen;
    u32        eCardLen;

    if (!eCard || !ticket  || !certs || !certsSize) {

        return EC_ERROR_INVALID;
    }

    curLen   = (u32) strnlen (price.currency, EC_CURRENCY_BUF_SIZE);
    eCardLen = (u32) strnlen (eCard, ECP_ECARD_BUF_SIZE);

    if (curLen  == EC_CURRENCY_BUF_SIZE ||
        eCardLen != ECP_ECARD_LEN) {

        return EC_ERROR_INVALID;
    }

    if ((rv = lock())) {
        return rv; 
    }

    if (isActiveAsyncOp()) {
        rv = EC_ERROR_BUSY;
        goto end;
    }

    if ((rv = getVng(&vng))) {
        goto end;
    }

    a = allocAOP (sizeof *a);
    op = &a->tac.op;
    initAsyncOp (op, (ECAsyncOpFunc) ecGetTicketsAndCerts,
                     (ECAsyncCancelFunc) genericAsyncCancel,
                      EC_BUSY_SUBSCRIBE,
                      sizeof *a);

    op->rpc.infoType = ECP_IT_SUBSCRIBE;
    op->rpc.snd.head = &a->snd;
    op->rpc.snd.headLen = sizeof a->snd;
    op->rpc.snd.body = deviceCert;
    op->rpc.snd.bodyLen = ES_DEVICE_CERT_SIZE;

    op->rpc.ret.buf = &a->tac.ret;
    op->rpc.ret.nBufRecs = 1;
    op->rpc.ret.recLen = sizeof a->tac.ret;
    op->rpc.ret.nRecs = &a->tac.nRetRecs;
    op->file.dontCloseOnFull = true;

    a->tac.state = TRS_GetRet;
    a->tac.tickets = ticket;
    a->tac.nTicketsPtr = NULL;
    a->tac.nTicketsValue = 1;
    a->tac.minTickets = 1;
    a->tac.certs = certs;
    a->tac.certsSizePtr = certsSize;
    a->tac.certsSizeValue = *certsSize;
    *certsSize = 0;
    a->tac.nRetRecs = 0;
    a->tac.retBufSize = &a->snd.retBufSize;
    a->snd.retBufSize = sizeof a->tac.ret; // max amount ecp should ret for 1st rpc
    a->snd.channelId = channelId;
    a->snd.subscriptionLength = subscriptionLength;
    a->snd.subscriptionTimeUnit = subscriptionTimeUnit;
    a->snd.itemId = itemId;
    a->snd.price = price;
    memcpy (a->snd.eCard, eCard, eCardLen + 1);
    a->snd.deviceCertLen = ES_DEVICE_CERT_SIZE;


    rv = op->retval = op->asyncOpFunc (op);

end:
    unlock();

    return rv;   
}





ECError  EC_RedeemECard  (const char *eCard,
                          const u8    deviceCert[ES_DEVICE_CERT_SIZE],
                          void       *ticket,
                          void       *certs,
                          u32        *certsSize)
{
    ECError          rv;
    VNG             *vng;
    ECRedeemECardOp  *a; 
    ECAsyncOp        *op;

    u32        eCardLen;

    if (!eCard || !ticket  || !certs || !certsSize) {

        return EC_ERROR_INVALID;
    }

    eCardLen = (u32) strnlen (eCard, ECP_ECARD_BUF_SIZE);

    if (eCardLen != ECP_ECARD_LEN) {

        return EC_ERROR_INVALID;
    }

    if ((rv = lock())) {
        return rv; 
    }

    if (isActiveAsyncOp()) {
        rv = EC_ERROR_BUSY;
        goto end;
    }

    if ((rv = getVng(&vng))) {
        goto end;
    }

    a = allocAOP (sizeof *a);
    op = &a->tac.op;
    initAsyncOp (op, (ECAsyncOpFunc) ecGetTicketsAndCerts,
                     (ECAsyncCancelFunc) genericAsyncCancel,
                      EC_BUSY_REDEEM,
                      sizeof *a);

    op->rpc.infoType = ECP_IT_REDEEM;
    op->rpc.snd.head = &a->snd;
    op->rpc.snd.headLen = sizeof a->snd;
    op->rpc.snd.body = deviceCert;
    op->rpc.snd.bodyLen = ES_DEVICE_CERT_SIZE;

    op->rpc.ret.buf = &a->tac.ret;
    op->rpc.ret.nBufRecs = 1;
    op->rpc.ret.recLen = sizeof a->tac.ret;
    op->rpc.ret.nRecs = &a->tac.nRetRecs;
    op->file.dontCloseOnFull = true;

    a->tac.state = TRS_GetRet;
    a->tac.tickets = ticket;
    a->tac.nTicketsPtr = NULL;
    a->tac.nTicketsValue = 1;
    a->tac.minTickets = 1;
    a->tac.certs = certs;
    a->tac.certsSizePtr = certsSize;
    a->tac.certsSizeValue = *certsSize;
    *certsSize = 0;
    a->tac.nRetRecs = 0;
    a->tac.retBufSize = &a->snd.retBufSize;
    a->snd.retBufSize = sizeof a->tac.ret; // max amount ecp should ret for 1st rpc
    memcpy (a->snd.eCard, eCard, eCardLen + 1);
    a->snd.deviceCertLen = ES_DEVICE_CERT_SIZE;

    rv = op->retval = op->asyncOpFunc (op);

end:
    unlock();

    return rv;   
}



ECError  EC_PurchaseTitle (ESTitleId          titleId,
                           s32                itemId,
                           ECPrice            price,
                           const ECPayment   *payment,
                           const ESLpEntry   *limits,
                           u32                nLimits,
                           const u8           deviceCert[ES_DEVICE_CERT_SIZE],
                           void              *ticket,
                           void              *certs,
                           u32               *certsSize)
{
    ECError   rv;
    VNG      *vng;
    u32       eCardLen;
    u32       curLen;
    u32       i;

    ECPurchaseTitleOp  *a; 
    ECAsyncOp          *op;

    if (!ticket  || !certs || !certsSize
            || (nLimits && !limits) || nLimits > ES_MAX_LIMIT_TYPE) {

        return EC_ERROR_INVALID;
    }

    /* Check range of limits[i].code in ecp
    *  Check credit card or account strings in ecp.
    *  Check currency and ecard len here because currenty is
    *  always provided and eCard is ony current payment method
    *  and has expected length.
    */

    curLen   = (u32) strnlen (price.currency, EC_CURRENCY_BUF_SIZE);
    if (curLen  == EC_CURRENCY_BUF_SIZE) {
        return EC_ERROR_INVALID;
    }

    if (payment && payment->method == EC_PaymentMethod_ECard) {
        eCardLen = (u32) strnlen (payment->info.eCard.number, ECP_ECARD_BUF_SIZE);
        if (eCardLen != ECP_ECARD_LEN) {

            return EC_ERROR_INVALID;
        }
    }

    if ((rv = lock())) {
        return rv; 
    }

    if (isActiveAsyncOp()) {
        rv = EC_ERROR_BUSY;
        goto end;
    }

    if ((rv = getVng(&vng))) {
        goto end;
    }

    a = allocAOP (sizeof *a);
    op = &a->tac.op;
    initAsyncOp (op, (ECAsyncOpFunc) ecGetTicketsAndCerts,
                     (ECAsyncCancelFunc) genericAsyncCancel,
                      EC_BUSY_PURCHASE_TITLE,
                      sizeof *a);

    op->rpc.infoType = ECP_IT_PURCHASE_TITLE;
    op->rpc.snd.head = &a->snd;
    op->rpc.snd.headLen = sizeof a->snd;
    op->rpc.snd.body = deviceCert;
    op->rpc.snd.bodyLen = ES_DEVICE_CERT_SIZE;

    op->rpc.ret.buf = &a->tac.ret;
    op->rpc.ret.nBufRecs = 1;
    op->rpc.ret.recLen = sizeof a->tac.ret;
    op->rpc.ret.nRecs = &a->tac.nRetRecs;
    op->file.dontCloseOnFull = true;

    a->tac.state = TRS_GetRet;
    a->tac.tickets = ticket;
    a->tac.nTicketsPtr = NULL;
    a->tac.nTicketsValue = 1;
    a->tac.minTickets = 1;
    a->tac.certs = certs;
    a->tac.certsSizePtr = certsSize;
    a->tac.certsSizeValue = *certsSize;
    *certsSize = 0;
    a->tac.nRetRecs = 0;
    a->tac.retBufSize = &a->snd.retBufSize;
    a->snd.retBufSize = sizeof a->tac.ret; // max amount ecp should ret for 1st rpc
    a->snd.titleId = titleId;
    a->snd.itemId = itemId;
    a->snd.price = price;

    if (payment) {
        a->snd.payment = *payment;
    } else {
        a->snd.payment.method = EC_PaymentMethod_Account;
        // accountId and passwd were zeroed by initAsyncOp
    }

    a->snd.nLimits = nLimits;

    for (i = 0;  i < nLimits;  ++i) {
        a->snd.limits[i] = limits[i];
        // limits array was zeroed by initAsycnOp
    }

    // a->snd.account was zeroed by initAsyncOp

    a->snd.deviceCertLen = ES_DEVICE_CERT_SIZE;

    rv = op->retval = op->asyncOpFunc (op);

end:
    unlock();

    return rv;   
}









ECError  EC_SyncTickets (const u8     deviceCert[ES_DEVICE_CERT_SIZE],
                         void        *tickets,
                         u32         *nTickets,
                         void        *certs,
                         u32         *certsSize)
{
    ECError          rv;
    VNG             *vng;
    ECSyncTicketsOp  *a; 
    ECAsyncOp        *op;

    if (!tickets  || !nTickets || !certs || !certsSize) {
        return EC_ERROR_INVALID;
    }

    if ((rv = lock())) {
        return rv; 
    }

    if (isActiveAsyncOp()) {
        rv = EC_ERROR_BUSY;
        goto end;
    }

    if ((rv = getVng(&vng))) {
        goto end;
    }

    a = allocAOP (sizeof *a);
    op = &a->tac.op;
    initAsyncOp (op, (ECAsyncOpFunc) ecGetTicketsAndCerts,
                     (ECAsyncCancelFunc) genericAsyncCancel,
                      EC_BUSY_SYNC_TICKETS,
                      sizeof *a);

    op->rpc.infoType = ECP_IT_SYNC_TICKETS;
    op->rpc.snd.head = &a->snd;
    op->rpc.snd.headLen = sizeof a->snd;
    op->rpc.snd.body = deviceCert;
    op->rpc.snd.bodyLen = ES_DEVICE_CERT_SIZE;

    op->rpc.ret.buf = &a->tac.ret;
    op->rpc.ret.nBufRecs = 1;
    op->rpc.ret.recLen = sizeof a->tac.ret;
    op->rpc.ret.nRecs = &a->tac.nRetRecs;
    op->file.dontCloseOnFull = true;

    a->tac.state = TRS_GetRet;
    a->tac.tickets = tickets;
    a->tac.certs = certs;
    a->tac.nTicketsPtr = nTickets;
    a->tac.nTicketsValue = *nTickets;
    *nTickets = 0;
    a->tac.minTickets = 0;
    a->tac.certsSizePtr = certsSize;
    a->tac.certsSizeValue = *certsSize;
    *certsSize = 0;
    a->tac.nRetRecs = 0;
    a->tac.retBufSize = &a->snd.retBufSize;
    a->snd.retBufSize = sizeof a->tac.ret; // max amount ecp should ret for 1st rpc
    a->snd.deviceCertLen = ES_DEVICE_CERT_SIZE;

    rv = op->retval = op->asyncOpFunc (op);

end:
    unlock();

    return rv;   
}






static
ECError  ecUpdateStatus (ECAsyncOp *op)
{
    return getNetInfo (op);
}




ECError  EC_UpdateStatus (const ECConsumption  *consumptions,        /* r !mba */
                          u32                   nConsumptions)
{
    ECError            rv;
    VNG               *vng;
    ECUpdateStatusOp  *a;
    ECAsyncOp         *op;

    if (!consumptions || !nConsumptions) {
        return EC_ERROR_INVALID;
    }

    if ((rv = lock())) {
        return rv; 
    }

    if (isActiveAsyncOp()) {
        rv = EC_ERROR_BUSY;
        goto end;
    }

    if ((rv = getVng(&vng))) {
        goto end;
    }

    a = allocAOP (sizeof *a);
    op = &a->op;
    initAsyncOp (op, (ECAsyncOpFunc) ecUpdateStatus,
                     (ECAsyncCancelFunc) genericAsyncCancel,
                      EC_BUSY_UPDATE_STATUS,
                      sizeof *a);

    op->rpc.infoType = ECP_IT_UPDATE_STATUS;
    op->rpc.snd.head = &a->nConsumptions;
    op->rpc.snd.headLen = sizeof a->nConsumptions;
    op->rpc.snd.body = consumptions;
    op->rpc.snd.bodyLen = nConsumptions * sizeof *consumptions;

    a->nConsumptions = nConsumptions;

    rv = op->retval = op->asyncOpFunc (op);

end:
    unlock();

    return rv;   
}






ECError EC_OpenCachedContent (ECContentId cid)
{
    ECError rv;

    SendInfo snd;
 
    memset (&snd, 0, sizeof snd);

    snd.head = &cid;
    snd.headLen = sizeof cid;
    snd.body = NULL;
    snd.bodyLen = 0;
    snd.needAlloc = true;
    snd.headOnly = true;

    if (!(rv = lock())) {
        rv = sndRPC (VN_MEMBER_OWNER, ECP_ST_OPEN_CID, &snd, NULL);
        unlock();
    }

    return rv;
}




s32   EC_ReadCachedContent (ECContentId   cid,
                            u8           *buf,
                            u32           count)
{
    ECError rv;

    ECPReadCachedContentArg a;

    SendInfo     snd;
    ReturnInfo   ret;
    u32          retLen;

    if (!buf || !count) {
        rv = EC_ERROR_INVALID;
        goto end;
    }

    memset (&a,   0, sizeof a);
    memset (&snd, 0, sizeof snd);
    memset (&ret, 0, sizeof ret);

    a.cid = cid;
    a.count = count;

    snd.head = &a;
    snd.headLen = sizeof a;
    snd.body = NULL;
    snd.bodyLen = 0;
    snd.needAlloc = true;
    snd.headOnly = true;

    ret.buf = buf;
    ret.nBufRecs = count;
    ret.recLen = sizeof *buf;
    ret.nRecs = &retLen;

    if (!(rv = lock())) {
        rv = sndRPC (VN_MEMBER_OWNER, ECP_ST_READ_CID, &snd, &ret);
        if (!rv) {
            rv = retLen;
        }
        unlock();
    }

end:
    return rv;
}


ECError  EC_SeekCachedContent (ECContentId   cid,
                               u32           offset)
{
    ECError rv;

    ECPSeekCachedContentArg a;

    SendInfo snd;

    memset (&a,   0, sizeof a);
    memset (&snd, 0, sizeof snd);

    a.cid = cid;
    a.offset = offset;

    snd.head = &a;
    snd.headLen = sizeof a;
    snd.body = NULL;
    snd.bodyLen = 0;
    snd.needAlloc = true;
    snd.headOnly = true;

    if (!(rv = lock())) {
        rv = sndRPC (VN_MEMBER_OWNER, ECP_ST_SEEK_CID, &snd, NULL);
        unlock();
    }

    return rv;
}


ECError EC_CloseCachedContent (ECContentId   cid)
{
    ECError rv;

    SendInfo snd;
 
    memset (&snd, 0, sizeof snd);

    snd.head = &cid;
    snd.headLen = sizeof cid;
    snd.body = NULL;
    snd.bodyLen = 0;
    snd.needAlloc = true;
    snd.headOnly = true;

    if (!(rv = lock())) {
        rv = sndRPC (VN_MEMBER_OWNER, ECP_ST_CLOSE_CID, &snd, NULL);
        unlock();
    }

    return rv;
}

ECError  EC_DeleteCachedContent (ECContentId  cid)
{
    ECError rv;

    SendInfo snd;
 
    memset (&snd, 0, sizeof snd);

    snd.head = &cid;
    snd.headLen = sizeof cid;
    snd.body = NULL;
    snd.bodyLen = 0;
    snd.needAlloc = true;
    snd.headOnly = true;

    if (!(rv = lock())) {
        rv = sndRPC (VN_MEMBER_OWNER, ECP_ST_DELETE_CID, &snd, NULL);
        unlock();
    }

    return rv;
}

ECError  EC_CancelCacheContent (const ECContentId  *cids,  /* mba */
                                u32                 nCids)
{
    ECError rv;

    SendInfo snd;
 
    memset (&snd, 0, sizeof snd);

    snd.head = NULL;
    snd.headLen = 0;
    snd.body = cids;
    snd.bodyLen = nCids * sizeof *cids;
    snd.needAlloc = nCids < 100 ? true : false;
    snd.headOnly = false;

    if (!(rv = lock())) {
        rv = sndRPC (VN_MEMBER_OWNER, ECP_ST_CANCEL_CID, &snd, NULL);
        unlock();
    }

    return rv;
}

ECError EC_CacheContent (const ECContentId  *cids,  /* mba */
                         u32                 nCids)
{
    ECError rv;

    SendInfo snd;
 
    memset (&snd, 0, sizeof snd);

    snd.head = NULL;
    snd.headLen = 0;
    snd.body = cids;
    snd.bodyLen = nCids * sizeof *cids;
    snd.needAlloc = false;
    snd.headOnly  = false;

    if (!(rv = lock())) {
        rv = sndRPC (VN_MEMBER_OWNER, ECP_ST_CACHE_CID, &snd, NULL);
        unlock();
    }

    return rv;
}

ECError EC_GetCachedContentStatus (const ECContentId *cids,        /* mba */
                                   u32                nCids,
                                   ECCacheStatus     *cacheStatus) /* mba */
{
    ECError rv;

    u32 nRet = nCids;

    SendInfo rpcSendInfo;
 
    memset (&rpcSendInfo, 0, sizeof rpcSendInfo);

    rpcSendInfo.head = cids;
    rpcSendInfo.headLen = nCids * sizeof *cids;
    rpcSendInfo.body = NULL;
    rpcSendInfo.bodyLen = 0;
    rpcSendInfo.needAlloc = false;
    rpcSendInfo.headOnly  = true;

    if (!(rv = (rv = lock()))) {
        rv = getCachedStatus (ECP_IT_CID, &rpcSendInfo, cacheStatus, &nRet);
        unlock();
    }

    return rv;
}


ECError  EC_EnableContentPreload (bool32   enablePreload)
{
    ECError rv;

    SendInfo snd;
 
    memset (&snd, 0, sizeof snd);

    snd.head = &enablePreload;
    snd.headLen = sizeof enablePreload;
    snd.body = NULL;
    snd.bodyLen = 0;
    snd.needAlloc = true;
    snd.headOnly = true;

    if (!(rv = lock())) {
        rv = sndRPC (VN_MEMBER_OWNER, ECP_ST_DELETE_CID, &snd, NULL);
        unlock();
    }

    return rv;
}



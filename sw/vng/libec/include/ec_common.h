/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __EC_COMMON_H__
#define __EC_COMMON_H__



#ifdef __cplusplus
extern "C" {
#endif

#if !defined(_GBA) && !defined(_SC)
#pragma pack(push, 4)
#endif



/* Move these to a VNP public dir or such */

#define VNP_INVALID_NET         (-1)
#define VNP_INVALID_DEVICE_ID   (-1)



/******************************************/




ECError EC_VngToECError (VNGErrCode ec);

char* vngErrStr (VNGErrCode ec, char *buf, size_t bufLen);




/*  EC_EnableContentPreload
 *
 *  Enables or disables background caching of the preload contents
 *  specified in the server response to EC_GetECMeta(). The list
 *  of preload contents are extracted from the EC_GetECMeta()
 *  response by the EC Proxy.
 *
 *  The default state of preloadEnable is true.
 *
 *  Background caching is started or resumed whenever the state of
 *  preloadEnable changes from false to true.  Preload caching is
 *  stopped whenever the state changes from true to false.
 *
 *  Caching of preload contents can be disabled/enabled before or after
 *  EC_GetECMeta() is called.  If the state of preloadEnable is true
 *  before calling EC_GetECMeta(), caching of preload content is started
 *  as soon as the EC_GetECMeta() response is received.
 *
 *  EC_EnableContentPreload does not delete already cached content when
 *  preloadEnable is set to false.
 *
 *  Error codes include:
 *
 *      EC_ERROR_OK      - operation was successful
 *
 */
ECError  EC_EnableContentPreload (bool32   enablePreload);


#if !defined(_GBA) && !defined(_SC)
#pragma pack(pop)
#endif





#ifdef __cplusplus
}
#endif //__cplusplus



#endif /*__EC_COMMON_H__*/

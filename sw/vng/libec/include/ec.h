/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __EC_H__
#define __EC_H__

#include "vng.h"

#ifdef __cplusplus
extern "C" {
#endif

#include "estypes.h"


#if !defined(_GBA) && !defined(_SC)
#pragma pack(push, 4)
#endif



/* ECError values */

#define EC_ERROR_RANGE_START    -4000

#define EC_ERROR_OK                        0  /* No error */
#define EC_ERROR_FAIL                  -4001  /* Generic error */
#define EC_ERROR_NOT_SUPPORTED         -4002  /* Feature not implemented */
#define EC_ERROR_INSUFICIENT_RESOURCE  -4003
#define EC_ERROR_INVALID               -4004
#define EC_ERROR_NOMEM                 -4005
#define EC_ERROR_NOT_FOUND             -4006
#define EC_ERROR_NOT_BUSY              -4007  /* no active async operation */
#define EC_ERROR_BUSY                  -4008
#define EC_ERROR_NOT_DONE              -4009
#define EC_ERROR_NO_PROXY              -4010  /* can't contact ECommerce proxy */
#define EC_ERROR_NOT_CACHED            -4011  
#define EC_ERROR_NOT_OPEN              -4012  
#define EC_ERROR_NET_NA                -4013  /* Internet access not available */
#define EC_ERROR_ECS_NA                -4014  /* ECommerce service not available */

#define EC_ERROR_ECARD                 -4017  /* Invalid eCard */
#define EC_ERROR_OVERFLOW              -4018  /* Output too big for buf provided */
#define EC_ERROR_NET_CONTENT           -4019  /* non specific error getting
                                               * content from CCS */
#define EC_ERROR_ECP                   -4021  /* EC proxy reports an error */
#define EC_ERROR_NET_CONTENT_FILE      -4022  /* Error creating or opening PC file */

#define EC_ERROR_WS_RESP               -4034  /* invalid web service response */

/* -200 to -599 correspond to http status codes 200 to 599
*
*  -1001 to -(1000 + VNGERR_MAX) correspond to VNGErrCode 1 to VNGERR_MAX
*
*/

/*  EC_ERROR_ECS_RESP -15   ECS reports a problem is no longer used.
*   Instead ECS error codes will be passed through
*   starting at EC_ECS_ERROR_RANGE_START
*/

#define EC_ECS_ERROR_RANGE_START    -10000

#define ECS_STATUS_TICKETS_ALREADY_IN_SYNC  EC_ECS_ERROR_RANGE_START - 641



/* ECTitleKind values */
#define EC_TITLE_KIND_GAME              1
#define EC_TITLE_KIND_MANUAL            2
#define EC_TITLE_KIND_GAME_AND_MANUAL   3

/* ECPricingKind values
 *      - only EC_PK_SUBSCRIPTION is applicable NetC
 */
#define EC_PK_SUBSCRIPTION      1
#define EC_PK_PURCHASE          2

/* ECPricingCategory values
 *      - only EC_PC_SUBSCRIPTION is applicable NetC
 */
#define EC_PC_SUBSCRIPTION      1
#define EC_PC_PERMANENT         2
#define EC_PC_RENTAL            3
#define EC_PC_BONUS             4
#define EC_PC_TRIAL             5

/* ECTimeUnit values */
#define EC_DAY      1
#define EC_MONTH    2

/* miscellaneous defines */
#define EC_TITLE_NAME_BUF_SIZE         64
#define EC_TITLE_DESC_BUF_SIZE        256
#define EC_TITLE_CAT_BUF_SIZE          32
#define EC_MAX_CONTENTS_PER_TITLE      10
#define EC_MAX_TITLE_PRICINGS          10
#define EC_MAX_RATINGS                  5
#define EC_MAX_CONSUMPTIONS           (VN_MAX_MSG_LEN/sizeof(ECConsumption))

#define EC_MAX_URL_LEN                  255
#define EC_URL_BUF_SIZE                (EC_MAX_URL_LEN + 1)
#define EC_MAX_CURRENCY_LEN             31
#define EC_CURRENCY_BUF_SIZE           (EC_MAX_CURRENCY_LEN + 1)

#define EC_MAX_SUB_CHANNEL_NAME_LEN     31  
#define EC_SUB_CHANNEL_NAME_BUF_SIZE   (EC_MAX_SUB_CHANNEL_NAME_LEN + 1)
#define EC_MAX_SUB_CHANNEL_DESC_LEN     31
#define EC_SUB_CHANNEL_DESC_BUF_SIZE   (EC_MAX_SUB_CHANNEL_DESC_LEN + 1)

#define EC_MAX_RATING_NAME_LEN          15
#define EC_RATING_NAME_BUF_SIZE        (EC_MAX_RATING_NAME_LEN + 1)
#define EC_MAX_RATING_LEN               15
#define EC_RATING_BUF_SIZE             (EC_MAX_RATING_LEN + 1)

typedef s32 ECTitleKind;
typedef s32 ECPricingKind;
typedef s32 ECPricingCategory;
typedef u32 ECLimitCode;
typedef s64 ECTimeStamp;         /* msec since 1970-01-01 00:00:00 UTC */
typedef s32 ECError;
typedef s32 ECTimeUnit;

#if !defined(bool32) && !defined(BOOL32)
#define BOOL32  s32
typedef s32     bool32;
#endif





/*  ECCacheStatus values refer to the cache status of content files
 *  downloaded via the internet and cached on the USB attached PC.
 *
 *  They are returned by EC_GetCachedContentStatus().
 *
 *  An ECCacheStatus data structure contains an indication of
 *  cachedSize and totalSize for the associated content file.
 *
 *  When the totalSize member of an ECCacheStatus is 0 or above,
 *  it is the total size that the content file will be when it is
 *  fully cached.  Zero is a legitimate file size.
 *
 *  When the totalSize is negative, the totalSize is not yet known.
 *  The specific negative value used is EC_ERROR_INVALID.  That is not
 *  an error, just an indication that we don't know the totalSize at
 *  the moment.  No other negative values are used for totalSize, so
 *  totalSize is either unknown (< 0) or known (>= 0).
 *
 *  When the cachedSize is 0 or above it indicates the number of bytes
 *  that have been received so far.
 *
 *  If the cachedSize is > 0 and equal to the totalSize, the file
 *  is fully cached and ready to be opened.
 *
 *  When the cachedSize is negative, it is an ECError code indicating
 *  the status of the last cache attempt for that content file.
 *
 *  If a cache operation has been requested and has not been canceled,
 *  cachedSize will be positive unless an error has occured.
 *
 *  Any negative value in cachedSize indicates that the content
 *  is not cached and no cache operation is in progress. 
 *
 *  If a content is not cached and cache of the file is not in process
 *  and no specific error status is available from a previous cache attempt,
 *  the cachedSize will be the negative value EC_ERROR_NOT_CACHED.
 *
 *  HTTP errors and other specific failures are indicated in cachedSize
 *  when possible.
 *
 *  When getting content from a server via the internet, the status
 *  of the cached info goes through several stages.
 *
 *  1.  content is not cached and caching is not in progress:
 *          cachedSize < 0
 *          totalSize may or may not be known:
 *              totalSize < 0 means not known
 *              totalSize >= 0 means totalSize is the expected total size
 *
 *  2.  cache of the content has been requested and therefore is in
 *      progress, but no bytes have been received:
 *          cachedSize == 0
 *          totalSize may or may not be known:
 *              totalSize < 0 means not known
 *              totalSize >= 0 means totalSize is the expected total size
 *
 *  3.  cache in progress, file size value has been obtained from the server.
 *          cachedSize >= 0
 *          totlaSize >= 0 and is the expected total size.
 *
 *  4.  cache has successfully completed and file is ready to be opened
 *          cachedSize >= 0 and cachedSize == totalSize
 *
 *  ECCacheStatus values have the following meaning:
 *
 *      - totalSize is the size the file will be when fully downloaded or
 *        EC_ERROR_INVALID if the totalSize is not yet known.
 *
 *      - cachedSize >= 0 and cachedSize != totalSize indicates the file
 *        is currently being downloaded.  The number of bytes that have
 *        been downloaded is the cachedSize.
 *
 *      - cachedSize >= 0 and cachedSize == totalSize indicates the file
 *        is in the cache and available to be opened.
 *
 *      - cachedSize < 0 is either the ECError code from the last cache
 *        attempt for the file or EC_ERROR_NOT_CACHED if not cached and
 *        no specific ECError code is available.
 *
 */

typedef struct {
    s32     cachedSize;
    s32     totalSize;
} ECCacheStatus;


typedef struct {
    s64             fsSize;
    ESTitleId       id;
    char            name[EC_TITLE_NAME_BUF_SIZE];

} ECListTitlesInfo;


/* The only valid NetC currency value is "EUNITS"
 *
 */
typedef struct {
    s32        amount;
    char       currency[EC_CURRENCY_BUF_SIZE];

} ECMoney;

typedef ECMoney ECPrice;


/* ECLimitCode values
 *
 * - only ES_LC_NUM_TITLES is present in NetC title details pricings
 *
 *    ES_LC_DURATION_TIME  = 1
 *    ES_LC_ABSOLUTE_TIME  = 2
 *    ES_LC_NUM_TITLES     = 3
 */

typedef struct
{
    ECLimitCode   code;   /* limited play algorithm */
    u32           limit;

} ECPlayLimit;


typedef struct
{
    s32                itemId;
    u32                nLimits;
    ECPlayLimit        limits[ES_MAX_LIMIT_TYPE];
    ECPrice            price;
    ECPricingCategory  pricingCategory;

} ECPricing;


typedef enum {
    EC_CONTENT = 0,
    EC_TMD     = 1,
    EC_MET     = 2,
    EC_CRL     = 3,
    EC_CETK    = 4

} ECContentType;

#define EC_KNOWN_CONTENT_TYPES 5


typedef struct {
    ESTitleId       titleId;
    ECContentType   contentType;
    ESContentId     contentId;   /* applies to EC_CONTENT only */

} ECContentId;


typedef struct {
    ECContentId  id;
    s32          size;

} ECContentInfo;


typedef struct {
    ESTitleId       titleId;
    ECTitleKind     titleKind;
    char            titleName   [EC_TITLE_NAME_BUF_SIZE];
    char            description [EC_TITLE_DESC_BUF_SIZE];
    char            category    [EC_TITLE_CAT_BUF_SIZE];
    u32             nContents;
    ECContentInfo   contents  [EC_MAX_CONTENTS_PER_TITLE];
    ECCacheStatus   cacheStatus [EC_MAX_CONTENTS_PER_TITLE];
    s64             fsSize;
    u32             version;

} ECTitleInfo;


typedef struct {
    char            name[EC_RATING_NAME_BUF_SIZE];
    char            rating[EC_RATING_BUF_SIZE];
} ECRating;


typedef struct {
    ECTitleInfo     info;
    u32             nRatings;
    ECRating        ratings[EC_MAX_RATINGS];
    u32             nPricings;
    ECPricing       pricings[EC_MAX_TITLE_PRICINGS];

} ECTitleDetails;



/* System Software TitleId Assignments */
#define NC_BOOT_TITLE_ID         0x0000000200000001LL
#define NC_SYS_APP_TITLE_ID      0x0000000200000002LL

#define NC_BOOT_TITLE_ID_STR      "0000000200000001"
#define NC_SYS_APP_TITLE_ID_STR   "0000000200000002"

/* Most recent System Software Versions */

typedef struct {
    u32               bootVersion;
    u32               sys_appVersion;

} ECMeta;



typedef struct {
    ESTitleId       titleId;
    s32             consumption;

} ECConsumption;

      
typedef struct {
    s32            itemId;
    s32            channelId;
    char           channelName [EC_SUB_CHANNEL_NAME_BUF_SIZE];
    char           channelDescription [EC_SUB_CHANNEL_DESC_BUF_SIZE];
    s32            subscriptionLength;
    ECTimeUnit     subscriptionTimeUnit;
    ECPrice        price;
    s32            maxCheckouts; /* -1 means no max specified */

} ECSubscriptionPricing;


typedef enum {
    EC_PaymentMethod_ECard      = 0,
    EC_PaymentMethod_Account    = 1,
    EC_PaymentMethod_CreditCard = 2

} ECPaymentMethod;


    #define EC_MAX_CC_NUMBER_LEN        16
    #define EC_CC_NUMBER_BUF_SIZE       20


/* cc number is zero terminated utf-8 string. */

typedef struct
{
    char  number [EC_CC_NUMBER_BUF_SIZE];

}  ECCreditCardPayment;


#define EC_ECARD_LEN        (6 + 10 + 10)
#define EC_ECARD_BUF_SIZE   (((EC_ECARD_LEN + 5) / 4) *4 )

// eCard is a zero terminated string
//
typedef struct
{
    char   number [EC_ECARD_BUF_SIZE];

}  ECECardPayment;


#define EC_MAX_ACCOUNT_ID_LEN          31
#define EC_ACCOUNT_ID_BUF_SIZE        (EC_MAX_ACCOUNT_ID_LEN + 1)

#define EC_MAX_ACCOUNT_PW_LEN          31
#define EC_ACCOUNT_PW_BUF_SIZE        (EC_MAX_ACCOUNT_PW_LEN + 1)

typedef struct
{
    //  The accountId and passwd should be set to empty strings
    //  (i.e., accountId[0] = passwd[0] = 0;) to use the default
    //  account for the device.
    //

    char accountId[EC_ACCOUNT_ID_BUF_SIZE];
    char passwd[EC_ACCOUNT_PW_BUF_SIZE];

} ECAccountPayment;

typedef struct
{
    ECPaymentMethod method;

    union {
        ECCreditCardPayment  cc;
        ECECardPayment       eCard;
        ECAccountPayment     account;

    } info;

} ECPayment;








/*
 *  Asynchronous vs. Synchronous EC APIs
 *
 *  The eCommerce library functions are either synchronous or asynchronous.
 *
 *  Synchronous functions perform an operation and do not return until the
 *  operation completes or returns due to an error.  Asynchronous functions
 *  start an operation and return without waiting for completion.
 *
 *  The return value of an asynchronous function can be
 *      1. an ECError,
 *      2. an integer value that is an ECError when negative and
 *         a call specific return value when positve.
 *
 *  A zero return value indicates successful completion and may convey other
 *  call specific information like "no data returned".
 *
 *  If an asynchronous function returns before the operation is complete,
 *  it will return EC_ERROR_NOT_DONE.
 *
 *  The caller is expected to poll for completion until the
 *  operation completes or is terminated by an error.
 *
 *  Only one eCommerce asynchronous operation can be active at a time.
 *
 *  Completion status of an active asynchronous operation is retrieved by
 *  calling EC_GetReturnValue().  The integer return value of
 *  EC_GetReturnValue() will have the same meaning as the most recently
 *  called asynchronous function.
 *
 *  For example, if the function would return a positive count on success
 *  when called synchronously, EC_GetReturnValue() will return a positive
 *  count on successful completion of the asynchronous operation.
 *  EC_GetReturnValue() will return EC_ERROR_NOT_DONE if the operation
 *  has not finished.
 *
 *  EC_ERROR_BUSY will be returned by a call to start an asynchronous
 *  operation if the current one has not finished.  The existing
 *  operation will continue.  Use EC_CancelOperation() to terminate an
 *  on-going asynchronous operation before it has completed.
 *
 *  A call to EC_CancelOperation is not needed if EC_GetReturnValue
 *  returns other than EC_ERROR_NOT_DONE.
 *
 */


/*  EC_GetETicketSize
 *
 *  Returns the size of an ETicket.
 *
 */
s32 EC_GetETicketSize ();



/*  EC_GetReturnValue
 *
 *  Gets the return value or status of an on-going asynchronous operation.
 *
 *  Only one eCommerce asynchronous operation can be active at a time.
 *
 *  The current operation is complete if EC_GetReturnValue returns other
 *  than EC_ERROR_NOT_DONE.  EC_GetReturnValue will retrieve the return
 *  value of the last asynchronous operation until a new one is started.
 *
 *  EC_ERROR_BUSY will be returned by a call to start an asynchronous
 *  operation if the current one has not finished.  The existing
 *  operation will continue.  EC_CancelOperation() can be called to
 *  discard an on-going asynchronous operation before it has completed.
 *
 *  The return value of an asynchronous function can be
 *       1. an ECError value,
 *       2. an integer value that is an ECError when negative and
 *          a call specific value when positive.
 *
 *  A zero return value indicates successful completion of the
 *  asynchronous operation and may convey additional
 *  call specific information like no data returned.
 *
 *  If the current asynchronous operation has completed,
 *     the return value of the asynchronous operation is returned.
 *
 *  If the operation associated is still in progress,
 *      EC_ERROR_NOT_DONE is returned.
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_NOT_BUSY  - No asynchronous operation is active.
 *
 */
ECError  EC_GetReturnValue ();



/*  EC_CancelOperation
 *
 *  Cancels the current asynchronous operation.
 *
 *  Only one eCommerce asynchronous operation can be active at a time.
 *
 *  EC_ERROR_BUSY will be returned by a call to start an asynchronous
 *  operation if the current one has not finished.  The existing
 *  operation will continue.  EC_CancelOperation() can be used to
 *  terminate an on-going asynchronous operation before it has completed.
 *
 *  Consider EC_CancelOperation as a last resort to be taken if an
 *  operation does not complete.  It should never need to be called.
 *
 *  A call to EC_CancelOperation is not needed if EC_GetReturnValue
 *  returns other than EC_ERROR_NOT_DONE.
 *
 *  EC_CancelOperation indicates that no further actions associated
 *  with the current asynchronous operation are required and the
 *  return value is not needed.  It will not actually terminate
 *  network activity, but will allow another operation to be started.
 *
 *  To cancel content caching, use EC_CancelContentCache().
 *
 *  After EC_CancelOperation(), calls to EC_GetReturnValue will
 *  return EC_ERROR_NOT_BUSY until another asynchronous operation
 *  is started.
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_OK
 *      Error codes that indicate a communication or system problem
 *
 */
ECError EC_CancelOperation ();



/*  EC_Init
 *
 *  Optional function which establishes connection to ECommerce proxy
 *  on attached PC.
 *
 *  Can be used at any time to determine if ECommerce proxy is available.
 *
 *  If it is not called before other ECommerce APIs, the first ECommerce
 *  API that is called will do the equivalent of EC_Init().
 *
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_OK        - operation was successful
 *      EC_ERROR_NO_PROXY  - ECommerce proxy could not be found
 *
 */
ECError EC_Init ();




/*  EC_Fini
 *
 *  Optional function which exits the ECommerce VN and closes the
 *  ECommerce VNG session.
 *
 *  Can be used to free resources used by ECommerce.
 *
 *  If an ECommerce API was called before VNG_Init() in the application,
 *  this will close the applications VNG session as well.
 *
 *  If that is not desired, be sure to call VNG_Init() before
 *  calling any ECommerce API.  That will ensure that the applications
 *  VNG session is not closed.
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_OK        - operation was successful
 *
 */
ECError EC_Fini ();




/*  NAME
 *
 *      EC_ListTitles
 *
 *  Fills in a list of data structures describing titles that match
 *  the specified title kind and pricing category.
 *
 *  The caller must provide a buffer at *titles and set *nTitles to
 *  the number of titles that can be returned in the buffer.
 *
 *  On completion the actual number of titles returned at *titles will
 *  be stored at *nTitles.
 *
 *  For NetC, the only valid value for pricingKind is EC_PK_SUBSCRIPTION.
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_OK       - operation was successful or started successfully
 *      EC_ERROR_BUSY     - previous async operation has not finished
 *      EC_ERROR_INVALID  - an argument was not valid
 *      EC_ERROR_NOT_DONE - asynchronous operation is not complete.
 *
 *  If EC_ERROR_NOT_DONE is returned, the caller is expected to poll
 *  for completion by calling EC_GetReturnValue().
 *
 */
ECError  EC_ListTitles (ECTitleKind         titleKind,
                        ECPricingKind       pricingKind,
                        ECListTitlesInfo   *titles,
                        u32                *nTitles);

                                              
/*  NAME
 *
 *      EC_GetTitleDetails
 *
 *  Fills a data structure describing the title indicated by titleId.
 *
 *  On completion the title details will be returned at *details.
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_OK       - operation was successful
 *      EC_ERROR_BUSY     - previous async operation has not finished
 *      EC_ERROR_INVALID  - an argument was not valid
 *      EC_ERROR_NOT_DONE - asynchronous operation is not complete.
 *
 *  If EC_ERROR_NOT_DONE is returned, the caller is expected to poll
 *  for completion by calling EC_GetReturnValue().
 *
 */
ECError  EC_GetTitleDetails (ESTitleId        titleId,
                             ECTitleDetails  *details);

                                              
/*  NAME
 *
 *      EC_GetECMeta
 *
 *  Fills in a data structure with information from the
 *  infrastructure server.  See the definition of ECMeta.
 *
 *  On completion the EC meta data will be stored at *meta.
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_OK       - operation was successful
 *      EC_ERROR_BUSY     - previous async operation has not finished
 *      EC_ERROR_INVALID  - an argument was not valid
 *      EC_ERROR_NOT_DONE - asynchronous operation is not complete.
 *
 *  If EC_ERROR_NOT_DONE is returned, the caller is expected to poll
 *  for completion by calling EC_GetReturnValue().
 *  
 */
ECError  EC_GetECMeta (ECMeta        *meta);



/*  NAME
 *
 *      EC_RedeemECard
 *
 *  Requests that the subscription for the device be
 *  extended for the amount of time associated with the eCard.
 *
 *  The deviceCert can be obtained by calling ES_GetDeviceCert().
 *  It must be in memory readable by the NetC.
 *
 *  On completion returns the ETicket, cert chain, and cert chain size.
 *  The buffers provided for ticket and certs must be in memory 
 *  writable by the NetC.
 *
 *  The ETicket is returned at *ticket and has the length returned by
 *  EC_GetETicketSize().  The byte array at *tickets must be at
 *  least EC_GetETicketSize() bytes.
 *
 *  The certificate chain is returned at *certs.  The caller should initialize
 *  *certsSize to the buffer size at *certs.   On completion *certsSize
 *  will be set to the size of the certs returned.
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_OK       - operation was successful
 *      EC_ERROR_BUSY     - previous async operation has not finished
 *      EC_ERROR_ECARD    - Invalid eCard
 *      EC_ERROR_INVALID  - an argument was not valid
 *      EC_ERROR_NOT_DONE - asynchronous operation is not complete.
 *
 *  If EC_ERROR_NOT_DONE is returned, the caller is expected to poll
 *  for completion by calling EC_GetReturnValue().
 *  
 */
ECError  EC_RedeemECard  (const char  *eCard,
                          const u8     deviceCert[ES_DEVICE_CERT_SIZE],
                          void        *ticket,
                          void        *certs,
                          u32         *certsSize);




/*  NAME
 *
 *      EC_ListSubscriptionPricings
 *
 *  Fills in a list of data structures describing subscription
 *  pricing options.
 *
 *  The caller must provide a buffer at *pricings and set *nPricings
 *  to the number of ECSubscriptionPricing data structures that can be
 *  returned in the buffer.
 *
 *  On completion the actual number of pricings returned at *pricings will
 *  be stored at *nPricings.
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_OK       - operation was successful or started successfully
 *      EC_ERROR_BUSY     - previous async operation has not finished
 *      EC_ERROR_INVALID  - an argument was not valid
 *      EC_ERROR_NOT_DONE - asynchronous operation is not complete.
 *
 *  If EC_ERROR_NOT_DONE is returned,the caller is expected to poll
 *  for completion by calling EC_GetReturnVlaue().
 *
 */
ECError  EC_ListSubscriptionPricings (ECSubscriptionPricing  *pricings,
                                      u32                    *nPricings);



/*  NAME
 *
 *      EC_Subscribe
 *
 *  Requests that the subscription for the device be
 *  extended for the amount of time indicated using the
 *  ECard provided to pay for the subscription.
 *
 *  The time can be indicated by months or days.  The
 *  time units is indicated by subscriptionTimeUnit and
 *  the number of units is indicated by subscriptionLength.
 *
 *  The itemId, channelId, price, currency, subscriptionLength,
 *  and subscriptionTimeUnit should correspond to the values returned in
 *  an ECSubscriptionPricing returned by EC_ListSubscriptionPricings.
 *
 *  On completion returns the ETicket, certs, and certsSize.
 *
 *  The ETicket is returned at *ticket and has the length returned by
 *  EC_GetETicketSize().  The byte array at *tickets must be at
 *  least EC_GetETicketSize() bytes.
 *
 *  The certificate chain is returned at *certs.  The caller should initialize
 *  *certsSize to the buffer size at *certs.   On completion *certsSize
 *  will be set to the size of the certs returned.
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_OK       - operation was successful
 *      EC_ERROR_BUSY     - previous async operation has not finished
 *      EC_ERROR_<TBD>    - TBD ECard eror codes
 *      EC_ERROR_INVALID  - an argument was not valid
 *      EC_ERROR_NOT_DONE - asynchronous operation is not complete.
 *
 *  If EC_ERROR_NOT_DONE is returned,the caller is expected to poll
 *  for completion by calling EC_GetReturnVlaue().
 *  
 */
ECError  EC_Subscribe  (s32          itemId,
                        s32          channelId,
                        ECPrice      price,
                        const char  *eCard,
                        s32          subscriptionLength,
                        ECTimeUnit   subscriptionTimeUnit,
                        const u8     deviceCert[ES_DEVICE_CERT_SIZE],
                        void        *ticket,
                        void        *certs,
                        u32         *certsSize);








/*  NAME
 *
 *      EC_PurchaseTitle
 *
 *  Requests puchase of the title indicated using the specified
 *  payment method.
 *
 *  TitleId, ItemId, and price payment must be provided for NC.
 *
 *  The only payment.method currently supported for NC is
 *  payment payment.method == EC_PaymentMethod_ECard.
 *
 *  Other types of payment are possible but not currently supported.
 *
 *  If EC_PaymentMethod_Account was supported, he payment pointer
 *  argument could be NULL for purchase using points from a default
 *  account associtated with the device.
 *
 *  For normal purchases (not limited by time duration or absoulute date)
 *  the nLimits argument should be 0.  When nLimits is passed as 0
 *  the limits pointer argument is ignored and can be NULL.
 *
 *  nLimits is limited to ES_MAX_LIMIT_TYPE.
 *
 *  Returns a positive operation id or negative ECError code.
 *
 *  This function starts the operation. The operation id should be
 *  passed to EC_GetProgress() to get the status of the operation.
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_BUSY     - previous async operation has not finished
 *      EC_ERROR_POINTS   - Insufficient points to purchase title
 *      EC_ERROR_INVALID  - titleId was not valid
 *
 *  If a positive opId is returned,the caller is expected to poll
 *  for completion by calling EC_GetProgress(opId).
 *  
 */
ECError  EC_PurchaseTitle (ESTitleId          titleId,
                           s32                itemId,
                           ECPrice            price,
                           const ECPayment   *payment,
                           const ESLpEntry   *limits,
                           u32                nLimits,
                           const u8           deviceCert[ES_DEVICE_CERT_SIZE],
                           void              *ticket,
                           void              *certs,
                           u32               *certsSize);





/*  NAME
 *
 *      EC_SyncTickets
 *
 *  Requests that the subscription ETickets and cert chain for the device be
 *  retrieved from the servers via the internet.
 *
 *  The deviceCert can be obtained by calling ES_GetDeviceCert().
 *  It must be in memory readable by the NetC.
 *
 *  On completion returns the ETickets, cert chain, and cert chain size.
 *  The buffers provided for tickets and certs must be in memory 
 *  writable by the NetC.
 *
 *  The ETickets are returned at *tickets and each ticket has the length
 *  returned by EC_GetETicketSize().  The caller should initialize
 *  *nTickets to indicate the number of tickets that can be returned at
 *  *tickets.  The byte array at *tickets must be large enough to hold
 *  (*nTickets * EC_GetETicketSize()).
 *
 *  On successful completion *nTickets will be set to the number of tickets
 *  returned.  If EC_ERROR_OVERFLOW is returned, the required values for
 *  nTickets and certSize will be stored at *nTickets and *certsSize.
 *
 *  The certificate chain is returned at *certs.  The caller should initialize
 *  *certsSize to the buffer size at *certs.   On completion *certsSize
 *  will be set to the size of the certs returned.
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_OK       - operation was successful
 *      EC_ERROR_BUSY     - previous async operation has not finished
 *      EC_ERROR_INVALID  - an argument was not valid
 *      EC_ERROR_NOT_DONE - asynchronous operation is not complete.
 *      EC_ERROR_OVERFLOW - buffers provided for tickets or certs
 *                          are not large enough. See comment above.
 *
 *  If EC_ERROR_NOT_DONE is returned, the caller is expected to poll
 *  for completion by calling EC_GetReturnValue().
 *  
 */
ECError  EC_SyncTickets (const u8     deviceCert[ES_DEVICE_CERT_SIZE],
                         void        *tickets,
                         u32         *nTickets,
                         void        *certs,
                         u32         *certsSize);






/*  NAME
 *
 *      EC_UpdateStatus
 *
 *  Provides consumption information on played games.
 *
 *  The caller passes an array of consumption information consisting
 *  of a titleId and the number of minutes played for each played game.
 *
 *  The number of elements in the consumption array is specified
 *  in nConsumptions.
 *
 *  Error codes returned include:
 *
 *      EC_ERROR_OK       - operation was successful
 *      EC_ERROR_BUSY     - previous async operation has not finished
 *      EC_ERROR_INVALID  - an argument was not valid
 *      EC_ERROR_NOT_DONE - asynchronous operation is not complete.
 *
 *  If EC_ERROR_NOT_DONE is returned, the caller is expected to poll
 *  for completion by calling EC_GetReturnValue().
 *  
 */
ECError  EC_UpdateStatus (const ECConsumption  *consumptions,
                          u32                   nConsumptions);







/*  EC_OpenCachedContent
 *
 *  Opens a read only content file cached on a USB attached PC.
 *
 *  Only one cached content file can be opened at a time.
 *
 *  If the file is present on a PC attached via USB, it will be used
 *  as the source of subsequent EC_ReadCachedContent requests.
 *
 *  If the file is not present on the attached PC, an error is returned.
 *
 *  Download of content files from an infrastructure server can be
 *  initiated via EC_CacheContent().  Status of cached content
 *  files can be retrieved via EC_GetCachedContentStatus().
 *
 *  Error codes include:
 *
 *      EC_ERROR_OK         - operation was successful
 *      EC_ERROR_PATH       - the path is not valid
 *      EC_ERROR_NOT_FOUND  - the filename did not correspond to an
 *                            existing file on a USB attached PC.
 *      EC_ERROR_BUSY       - A cached file is already open.
 *                            Only one can be open at a time.
 *      EC_ERROR_INVALID    - an argument was not valid
 *
 */
ECError  EC_OpenCachedContent (ECContentId cid);




/*  EC_ReadCachedContent
 *
 *  Attempt to read count bytes into buf from the current file position
 *  of the cached content file indicated by cid.  The content cache file
 *  associated with cid must be currently opened.
 *
 *  On success returns the number of bytes copied to buf and
 *  updates the file position by that amount.
 *
 *  If the operation completes with less then count bytes read, it is not
 *  an error.
 *
 *  EC_ReadCachedContent should be called repeatedly until the end of file or desired
 *  number of bytes have been read.
 *
 *  If the end of file is reached, the number of bytes that were copied
 *  to buf is returned.
 *
 *  If 0 is returned, the file position is at end of file.
 *
 *  Error codes returned include
 *
 *      EC_ERROR_NOT_OPEN  - cached content file is not open
 *      EC_ERROR_INVALID   - an argument was not valid
 */
s32 EC_ReadCachedContent (ECContentId   cid,
                          u8           *buf,
                          u32           count);





/*  EC_SeekCachedContent
 *
 *  Set the file position for the cached content file indicated by cid
 *  to offset from the start of file.   The content cache file associated
 *  with cid must currently be opened.
 *
 *  If the file position is set past the end of file, subsequent reads
 *  will indicate zero bytes read.
 *
 *  Error codes include:
 *
 *      EC_ERROR_OK        - operation was successful
 *      EC_ERROR_NOT_OPEN  - cached content file is not open
 *
 */
ECError  EC_SeekCachedContent (ECContentId  cid,
                               u32          offset);



/*  EC_CloseCachedContent
 *
 *  Close the cached content file indicated by cid.
 *
 *  Resources will be released.
 *
 *  Error codes include:
 *
 *      EC_ERROR_OK        - operation was successful
 *      EC_ERROR_NOT_OPEN  - cached content file is not open
 *
 */
ECError EC_CloseCachedContent (ECContentId  cid);


/*  EC_DeleteCachedContent
 *
 *  Delete the content file cached on the USB attached PC
 *  that is associated with cid.
 *
 *  It is not treated as an error if the file is not present.
 *
 *  The file does not need to be open.  If the file is open,
 *  it will be closed and removed.
 *
 *  Error codes include:
 *
 *      EC_ERROR_OK       - operation was successful
 *
 */
ECError  EC_DeleteCachedContent (ECContentId  cid);



/*  EC_CacheContent
 *
 *  Start download of a set of content files from an
 *  infrastructure server to the USB attached PC file cache.
 * 
 *  For each content id in cids[]
 *      if the content file associated with cids[i] is not
 *      in the cache of the attached PC, download it from an
 *      infrastructure server and store it in the PC content cache.
 *
 *  nCids is the number of content ids in cids[]
 *
 *  This function starts the operation of checking and
 *  downloading the content files but does not wait for completion.
 *
 *  Use EC_GetCachedContentStatus() to get the cache status of the files.
 *
 *  Error codes include:
 *
 *      EC_ERROR_OK       - operation was successfully started
 *
 */
ECError EC_CacheContent (const ECContentId  *cids,
                         u32                 nCids);



/*  EC_GetCachedContentStatus
 *
 *  Get the status of content files cached on the USB attached PC.
 *
 *  Stores the ECCacheStatus value corresponding to the content id
 *  passed in at cids[i] in the caller provided output buffer at
 *  cacheStatus[i].
 *
 *  The number of content ids in cids[] is indicated by nCids.
 *
 *  A buffer large enough to hold status for nCids must be provided
 *  at *cacheStatus.
 *
 *  See the description of ECCacheStatus for the meaning of the
 *  cache status values.
 *
 *  Error codes include:
 *
 *      EC_ERROR_OK      - operation was successful
 *      EC_ERROR_INVALID - an argument was not valid
 *                         cids, nCids, and cacheStatus must be non-zero
 *
 */
ECError EC_GetCachedContentStatus (const ECContentId  *cids,
                                   u32                 nCids,
                                   ECCacheStatus      *cacheStatus);


/*  EC_CancelCacheContent
 *
 *  Cancel cache of the specified contents.
 *
 *  The number of content ids in cids[] is indicated by nCids.
 *
 *  EC_CancelCacheContent does not delete already cached content, but
 *  cancels retrieval of the specified contents from the content caching
 *  servers.
 *
 *  If nCids is 0, all content caching is canceled.  If nCids is 0,
 *  cids can be NULL.
 *
 *  Error codes include:
 *
 *      EC_ERROR_OK      - operation was successful
 *      EC_ERROR_INVALID - an argument was not valid
 *                         cids must be non-null if nCids != 0
 *
 */
ECError  EC_CancelCacheContent (const ECContentId  *cids,
                                u32                 nCids);




#if !defined(_GBA) && !defined(_SC)
#pragma pack(pop)
#endif


#ifdef __cplusplus
}
#endif //__cplusplus



#endif /*__EC_H__*/

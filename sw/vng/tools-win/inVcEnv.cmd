
:# This cmd script sets the vc6 or vc7 environment variables
:# and runs the remaining passed arguments as a command and arguments.

@echo off
:# use verify to init errorlevel to non-zero
verify other 2>nul
setlocal enabledelayedexpansion
if errorlevel 1 (
    echo Unable to enable delayed expansion of cmd env variables
    exit 1
)

set args=
set vc_version=

for %%a in ( %* ) do (
    if "%%a"=="-vc6" (
        set vc_version=-vc6
    ) else if "%%a"=="-vc7" (
        set vc_version=
    ) else if "!args!"=="" (
        set args=%%a
    ) else (
        set args=!args! %%a
    )
)

echo call %~dp0\setVcEnv.cmd %vc_version%
call %~dp0\setVcEnv.cmd %vc_version%
echo %args%
%args%

endlocal

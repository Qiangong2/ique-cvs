/*
*                Copyright (C) 2005, BroadOn Communications Corp.
* 
*   These coded instructions, statements, and computer programs contain
*   unpublished  proprietary information of BroadOn Communications Corp.,
*   and  are protected by Federal copyright law. They may not be disclosed
*   to  third  parties or copied or duplicated in any form, in whole or in
*   part, without the prior written consent of BroadOn Communications Corp.
* 
*/


#ifndef __SHR_MEM_H__
#define __SHR_MEM_H__  1


#ifdef _WIN32
    #if _MSC_VER > 1000    
        #pragma once
    #endif
#endif

#include "sc/ios.h"

#if defined(_SC) || defined(SC)
    #ifndef __cplusplus
        #include "ioslibc.h"
    #endif
#endif

#include <stddef.h>
#include "shr_th.h"



#ifdef __cplusplus
extern "C" {
#endif


/* Heap allocation functions */

typedef struct __SHRFreeBlk {
    u32                   size;
    struct __SHRFreeBlk  *next;
} _SHRFreeBlk;


typedef struct __SHRHeap _SHRHeap;

struct __SHRHeap {
    u32                flags;
   _SHRMutex           mutex;
   _SHRHeap          **top;
   _SHRFreeBlk        *bottomFreeBlk;
   _SHRFreeBlk        *topFreeBlk;
   _SHRFreeBlk        *lastToAllocate;
   _SHRFreeBlk         nullBlk; /* never allocated 0 size first free blk */
};


_SHRHeap* _SHR_heap_init    (u32 flags, void *heap, size_t size);
int       _SHR_heap_destroy (_SHRHeap *heap);

void* _SHR_heap_alloc (_SHRHeap *heap, size_t size);
int   _SHR_heap_free  (_SHRHeap *heap, void  *ptr);


/*  IOS_Alloc(0, size) will allocate cache aligned shared memory on the SC
 *
 *  On windows or linux it will just be ordinary word aligned memory
 *
 *  You can allocate a block of shared mem with IOS_Alloc and then use
 *  IOS_CreateHeap to turn it into a shared memory heap.
 *
 *  Or you can just do all your shared mem allocation using IOS_Alloc(0, size).
 *
 *   void *IOS_Alloc(IOSHeapId id, u32 size);
 *   IOSError IOS_Free(IOSHeapId id, void *ptr);
 *   IOSHeapId IOS_CreateHeap(void *ptr, u32 size);
 *   IOSError IOS_DestroyHeap(IOSHeapId id);
 *
 */

#if !defined (_SC)

    IOSHeapId IOS_CreateHeap (void *ptr, u32 size);

    #define IOS_DestroyHeap(id) \
                (_SHR_heap_destroy((_SHRHeap*)(id)) ? IOS_ERROR_INVALID : 0)

    #define IOS_Alloc(id, size) \
                   ((id) ? _SHR_heap_alloc((_SHRHeap*)(id), (size)) : malloc((size)))

    #define IOS_Free(id, ptr) \
                   (((id) ? _SHR_heap_free((_SHRHeap*)(id), (ptr)) \
                         : (free((ptr)),0)) ? IOS_ERROR_INVALID : 0)

#endif






#ifdef  __cplusplus
}
    
#endif


#endif  /* __SHR_MEM_H__ */


/*
*                Copyright (C) 2005, BroadOn Communications Corp.
* 
*   These coded instructions, statements, and computer programs contain
*   unpublished  proprietary information of BroadOn Communications Corp.,
*   and  are protected by Federal copyright law. They may not be disclosed
*   to  third  parties or copied or duplicated in any form, in whole or in
*   part, without the prior written consent of BroadOn Communications Corp.
* 
*/


#ifndef __SHR_PLAT_SC_H__
#define __SHR_PLAT_SC_H__  1

#include <stdarg.h>
#include "sc/ios.h"
#ifndef __cplusplus
#include "ioslibc.h"
#endif


#ifdef __cplusplus
    extern "C" {
#endif


#define _SHR_MSEC_2_IOSTIME(msec)   (msec*500)


#ifndef __cplusplus
size_t strnlen(const char *s, size_t maxlen);
#endif

#undef  assert

#ifdef  NDEBUG
    #define assert(exp)     ((void)0)
#else
    void _assert(const char *exp, const char *file, unsigned line);
    #define assert(exp) (void)( (exp) || (_assert(#exp, __FILE__, __LINE__), 0) )
#endif



#ifdef  __cplusplus
}
#endif
 
#endif  /* __SHR_PLAT_SC_H__ */

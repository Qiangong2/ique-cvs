/*
*                Copyright (C) 2005, BroadOn Communications Corp.
* 
*   These coded instructions, statements, and computer programs contain
*   unpublished  proprietary information of BroadOn Communications Corp.,
*   and  are protected by Federal copyright law. They may not be disclosed
*   to  third  parties or copied or duplicated in any form, in whole or in
*   part, without the prior written consent of BroadOn Communications Corp.
* 
*/


#ifndef __SHR_TH_LINUX_H__
#define __SHR_TH_LINUX_H__


#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <errno.h>
#include <pthread.h>
#include <semaphore.h>

#ifdef __cplusplus
extern "C" {
#endif

#define _SHRSemaphore           sem_t
#define _SHR_sem_init(s,m,c,v)  sem_init(s,0,0)
#define _SHR_sem_wait           sem_wait
#define _SHR_sem_trywait        sem_trywait
#define _SHR_sem_post           sem_post
#define _SHR_sem_destroy        sem_destroy



/* Mutex functions */

typedef struct {
    pthread_mutex_t  pth_mtx;
    u32              type;  /*_SHR_MUTEX_[NON_RECURSIVE|RECURSIVE|ERRORCHECK]*/
    pthread_t        lock_tid;
    s32              lock_count;
} _SHRMutex;

extern pthread_mutexattr_t  _SHR_linux_mutex_attr[_SHR_NUM_MUTEX_TYPES];


/* Threads */

typedef pthread_t    _SHRThread;   /* Handle returned by _SHR_thread_create  */
typedef pthread_t    _SHRThreadId; /* Thread id returned by _SHR_thread_self */
typedef void*        _SHRThreadRT; /* Thread return type                     */
#define _SHRThreadCC               /* Thread calling convention              */
#define _SHRThreadAttr pthread_attr_t /* Thread attr used by pthreads    */

#define _SHR_thread_create      pthread_create
#define _SHR_thread_join(th, th_ret, timeout)  pthread_join(th, th_ret)
#define _SHR_thread_exit        pthread_exit
#define _SHR_thread_sleep(msec) usleep(msec*1000)
#define _SHR_thread_self        pthread_self


#ifdef  __cplusplus
}
#endif
 
#endif /* __SHR_TH_LINUX_H__ */

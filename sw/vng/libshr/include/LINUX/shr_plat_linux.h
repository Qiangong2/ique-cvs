/*
*                Copyright (C) 2005, BroadOn Communications Corp.
* 
*   These coded instructions, statements, and computer programs contain
*   unpublished  proprietary information of BroadOn Communications Corp.,
*   and  are protected by Federal copyright law. They may not be disclosed
*   to  third  parties or copied or duplicated in any form, in whole or in
*   part, without the prior written consent of BroadOn Communications Corp.
* 
*/


#ifndef __SHR_PLAT_LINUX_H__
#define __SHR_PLAT_LINUX_H__


#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif


size_t strnlen(const char *s, size_t maxlen);


#ifdef  __cplusplus
}
#endif
 
#endif /* __SHR_PLAT_LINUX_H__ */

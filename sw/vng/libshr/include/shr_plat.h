/*
*                Copyright (C) 2005, BroadOn Communications Corp.
* 
*   These coded instructions, statements, and computer programs contain
*   unpublished  proprietary information of BroadOn Communications Corp.,
*   and  are protected by Federal copyright law. They may not be disclosed
*   to  third  parties or copied or duplicated in any form, in whole or in
*   part, without the prior written consent of BroadOn Communications Corp.
* 
*/


#ifndef __SHR_PLAT_H__
#define __SHR_PLAT_H__  1


/* Commanly used and miscellaneous defines and functions that
*  are defined or implememted differently on different platforms.
*
*  These are intended to be applicable to any lib or application
*  using libshr.
*
*  The actual apis are specified in platform specific headers.
*
*  There are also platform specific features that a lib or app
*  using libshr can choose to use or not like malloc/free in
*  shr_mem.h and thread and multi-thread sync apis in shr_th.h
*/
  
#ifdef _WIN32
    #if _MSC_VER > 1000    
        #pragma once
    #endif
#endif

#ifdef __cplusplus
    extern "C" {
#endif

/* All platforms can make use of ios defines and types */
#ifndef _GBA
    /* Should be able to include $ROOT/usr/include/gba/ios.h, but
     * it doesn't include types.h, doesn't have IOSError defines
     * and can't include iostypes.h.  It needs to be fixed, but for
     * now do special stuff in libshr/include/GBA/shr_plat_gba.h
     */

    #include "sc/ios.h"
#endif

#ifdef  __cplusplus
}
#endif
 
/*  FAIL values for system funtions implemented for multiple platforms
*   These are returned by funtions in platform specific directories
*/
typedef int _SHRError;

#define _SHR_ERR_FAIL          -255  /* unspecified failure */
#define _SHR_ERR_OK             0
#define _SHR_ERR_EPERM         -1    /* not permitted */
#define _SHR_ERR_ACCESS        _SHR_ERR_EPERM    
#define _SHR_ERR_MAX           -5    /* A limited resource has reached its max */
#define _SHR_ERR_EAGAIN        -11   /* not ready try again */
#define _SHR_ERR_EBUSY         -16   /* not avaiable */
#define _SHR_ERR_EDEADLK       -35   /* deadlock would occur */


#if  !defined(_SHR_DONT_DEFINE_BOOL) && \
     !defined(__VNG_TYPES_H__) && \
     !defined(__VN_TYPES_H__)
                         
    #ifndef __cplusplus
        #if !defined(bool)
            typedef unsigned char bool;
        #endif
        #define false 0
        #define true  1
    #endif
#endif



#ifdef _WIN32
    #include "WIN32/shr_plat_win32.h"
#elif defined (_SC) || defined(SC)
    #include "SC/shr_plat_sc.h"
#elif defined (_GBA) || defined (GBA)
    #include "GBA/shr_plat_gba.h"
#else
    #include "LINUX/shr_plat_linux.h"
#endif



#endif  /* __SHR_PLAT_H__ */


/*
*                Copyright (C) 2005, BroadOn Communications Corp.
* 
*   These coded instructions, statements, and computer programs contain
*   unpublished  proprietary information of BroadOn Communications Corp.,
*   and  are protected by Federal copyright law. They may not be disclosed
*   to  third  parties or copied or duplicated in any form, in whole or in
*   part, without the prior written consent of BroadOn Communications Corp.
* 
*/


#ifndef __SHR_GETOPT_H__
#define __SHR_GETOPT_H__  1


#ifdef _WIN32
    #if _MSC_VER > 1000    
        #pragma once
    #endif
#endif

#ifdef __cplusplus
extern "C" {
#endif

/*
*   Only ascii chars are allowed in options
*   Stops scanning at first non-option argument
*   Stops scanning if "--" argument found
*   Doesn't support '-' as first char of optstring
*   Posix implementaion reserved option "-W" is not treated specially
*   When specifying optional arg in opt string, use "::". (e.g.  o::)
*   A required or optional arg missing after an '='
*   is treated as an arg equal to an empty string.
*   Allows short option -f with required arg bar to be specified like:
*            -fbar or -f=bar or -f=  or -f bar
*   Allows short option -f with optional arg bar to be specified like:
*      -f or -fbar or -f=bar or -f=
*   Allows long option --foo with required arg bar to be specified like:
*               --foo=bar or --foo= or --foo bar
*   Allows long option --foo with optional arg bar to be specified like:
*      --foo or --foo=bar or --foo=
*/

/*  set option has_arg to: */
#define  no_argument        0
#define  required_argument  1
#define  optional_argument  2


struct option {
    const char *name;
    int has_arg;
    int *flag;
    int val;
};


extern char  *optarg;
extern int    optind;
extern int    opterr;
extern int    optopt;
extern char  *optcur;
/* optcur is option string being processed on return from getopt or getopt_log
*  e.g.  -r or -rxg or -t=MyName or --title or --title=MyName
*/


void reinit_getopt ();

int getopt (int argc, char* const *argv, const char *optstring);

int getopt_long (int argc, char* const *argv, const char *optstring,
                 const struct option *longopts, int *longindex);



#ifdef  __cplusplus
}
#endif


#endif //  __SHR_GETOPT_H__

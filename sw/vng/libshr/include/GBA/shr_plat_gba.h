/*
*                Copyright (C) 2005, BroadOn Communications Corp.
* 
*   These coded instructions, statements, and computer programs contain
*   unpublished  proprietary information of BroadOn Communications Corp.,
*   and  are protected by Federal copyright law. They may not be disclosed
*   to  third  parties or copied or duplicated in any form, in whole or in
*   part, without the prior written consent of BroadOn Communications Corp.
* 
*/


#ifndef __SHR_PLAT_GBA_H__
#define __SHR_PLAT_GBA_H__


#ifdef __cplusplus
extern "C" {
#endif


#include <ioslibc.h>

#define assert(exp) ((void)0)

#define _SHR_thread_self()  0


/* GBA ios.h and types.h are different from sc and
 * don't incldue error definitions.  That needs to be fixed.
 * For now, copy defines here.
 */

#ifndef __IOSTYPES_H__

#define IOS_ERROR_OK                     0
#define IOS_ERROR_ACCESS                -1
#define IOS_ERROR_EXISTS                -2
#define IOS_ERROR_INTR                  -3
#define IOS_ERROR_INVALID               -4
#define IOS_ERROR_MAX                   -5
#define IOS_ERROR_NOEXISTS              -6
#define IOS_ERROR_QEMPTY                -7
#define IOS_ERROR_QFULL                 -8
#define IOS_ERROR_UNKNOWN               -9
#define IOS_ERROR_NOTREADY             -10
#define IOS_ERROR_ECC                  -11
#define IOS_ERROR_ECC_CRIT             -12
#define IOS_ERROR_BADBLOCK             -13

#define IOS_ERROR_INVALID_OBJTYPE      -14
#define IOS_ERROR_INVALID_RNG          -15
#define IOS_ERROR_INVALID_FLAG         -16
#define IOS_ERROR_INVALID_FORMAT       -17
#define IOS_ERROR_INVALID_VERSION      -18
#define IOS_ERROR_INVALID_SIGNER       -19
#define IOS_ERROR_FAIL_CHECKVALUE      -20
#define IOS_ERROR_FAIL_INTERNAL        -21
#define IOS_ERROR_FAIL_ALLOC           -22
#define IOS_ERROR_INVALID_SIZE         -23

#endif

#include "types.h"

typedef s32 IOSError;



/* Should be able to include $ROOT/usr/include/gba/ios.h, but
 *   it doesn't include types.h, doesn't have IOSError defines
 * and can't include iostypes.h.  It needs to be fixed, but for
 * now use sc ios.h
 */

#include "ios.h"


#ifdef  __cplusplus
}
#endif
 
#endif /* __SHR_PLAT_GBA_H__ */

/*
*                Copyright (C) 2005, BroadOn Communications Corp.
* 
*   These coded instructions, statements, and computer programs contain
*   unpublished  proprietary information of BroadOn Communications Corp.,
*   and  are protected by Federal copyright law. They may not be disclosed
*   to  third  parties or copied or duplicated in any form, in whole or in
*   part, without the prior written consent of BroadOn Communications Corp.
* 
*/


#ifndef __SHR_PLAT_WIN32_H__
#define __SHR_PLAT_WIN32_H__  1

#define WIN32_LEAN_AND_MEAN 1

#if _MSC_VER >= 1400
    #if defined(__cplusplus) && !defined(_CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES)
        #define _CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES 1
        #define _CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES_COUNT 1
    #elif !defined(_CRT_SECURE_NO_DEPRECATE)
        #define _CRT_SECURE_NO_DEPRECATE 1
    #endif
#endif


#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <tchar.h>
#include <io.h>
#include <assert.h>


#ifdef __cplusplus
    extern "C" {
#endif

#ifndef __cplusplus
    #define inline __inline
#endif


#define snprintf _snprintf
#define bzero(s, n)  memset (s, '\0', n)
#define bcopy(s, d, n)  memcpy (d, s, n)
#define asctime_r(tm,buf)  (strcpy(buf,asctime(tm)))

char* stpcpy(char *d, const char *s);

#if _MSC_VER < 1400    
    size_t strnlen(const char *s, size_t maxlen);
#endif



#ifdef  __cplusplus
}
#endif
 
#endif  /* __SHR_PLAT_WIN32_H__ */

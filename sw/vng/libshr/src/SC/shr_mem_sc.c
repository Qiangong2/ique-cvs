/*
*                Copyright (C) 2005, BroadOn Communications Corp.
* 
*   These coded instructions, statements, and computer programs contain
*   unpublished  proprietary information of BroadOn Communications Corp.,
*   and  are protected by Federal copyright law. They may not be disclosed
*   to  third  parties or copied or duplicated in any form, in whole or in
*   part, without the prior written consent of BroadOn Communications Corp.
* 
*/


#include "shr_mem.h"
#include "shr_trace.h"
#include "shr_trace_i.h"

#if 0
/*  kernel doesn't currently support IOS_GetSdramPageSize */
static u32 _shr_GetSdramPageSize()
{
    return 16384;
}
#endif

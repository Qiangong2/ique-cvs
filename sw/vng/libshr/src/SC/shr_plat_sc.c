/*
*                Copyright (C) 2005, BroadOn Communications Corp.
* 
*   These coded instructions, statements, and computer programs contain
*   unpublished  proprietary information of BroadOn Communications Corp.,
*   and  are protected by Federal copyright law. They may not be disclosed
*   to  third  parties or copied or duplicated in any form, in whole or in
*   part, without the prior written consent of BroadOn Communications Corp.
* 
*/


#include "shr_plat.h"


#ifndef  NDEBUG
    void _assert(const char *exp, const char *file, unsigned line)
    {
        printf ("File: %s  Line: %u  Assertion failed: %s  Thread %u\n", file, line, exp, IOS_GetThreadId());
        exit (1);
    }
#endif


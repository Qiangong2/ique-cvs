/*
*                Copyright (C) 2005, BroadOn Communications Corp.
* 
*   These coded instructions, statements, and computer programs contain
*   unpublished  proprietary information of BroadOn Communications Corp.,
*   and  are protected by Federal copyright law. They may not be disclosed
*   to  third  parties or copied or duplicated in any form, in whole or in
*   part, without the prior written consent of BroadOn Communications Corp.
* 
*/

#include "shr_th.h"
#include "shr_trace.h"
#include "shr_trace_i.h"
#include "shr_mem.h"



static _SHRError ios2shrErr (IOSError ios_err)
{
    _SHRError rv;

    if (ios_err >= 0) {
        rv = _SHR_ERR_OK;
    }
    else switch (ios_err) {
        case IOS_ERROR_ACCESS:
            rv = _SHR_ERR_ACCESS;
            break;
        case IOS_ERROR_MAX:
            rv = _SHR_ERR_MAX;
            break;
        default:
            rv = _SHR_ERR_FAIL;
            break;
    }

    trace (INFO, TH, "ios2shrErr ios err: %d   shr err %d\n", ios_err, rv);

    return rv;
}




int _SHR_sem_init (_SHRSemaphore *sem,
                   u32            pIdMask,
                   u32            maxCount,
                   u32            value)
{
    int i;

    int max_def_count = sizeof(sem->msg_array) / sizeof(sem->msg_array[0]);

    if (maxCount <= max_def_count) {
        sem->msgs = sem->msg_array;
    }
    else if (!sem->msgs) {
        trace (ERR, TH,
                "_SHR_sem_init  sem->msgs NULL and maxCount(%u) > "
                "num entries(%u) in sem->msg_array\n",
                maxCount, max_def_count);
        return _SHR_ERR_FAIL;
    }

    if (value > maxCount) {
        trace (ERR, TH, "_SHR_sem_init  initial value %d > maxCount %d\n",
               value, maxCount);
        return _SHR_ERR_FAIL;
    }

    sem->ignoreQFull = false;

    sem->mq = IOS_CreateMessageQueue (sem->msgs, maxCount);

    if (sem->mq < 0) {
        trace (ERR, TH, "_SHR_sem_init  IOS_CreateMessageQueue returns %d\n",
               sem->mq);
        return ios2shrErr (sem->mq);
    }

    for (i = 0;  i < value;  ++i) {
        IOSError rc =
        IOS_SendMessage  (sem->mq, IOS_GetThreadId(), IOS_MESSAGE_NOBLOCK);
        if (rc < 0) {
            trace (ERR, TH, "_SHR_sem_init  IOS_SendMessage returns %d\n", rc);
            IOS_DestroyMessageQueue  (sem->mq);
            return _SHR_ERR_FAIL;
        }
    }
    return _SHR_ERR_OK;
}

int _SHR_sem_wait (_SHRSemaphore* sem)
{
    IOSMessage  msg;

    IOSError rc = IOS_ReceiveMessage(sem->mq, &msg, IOS_MESSAGE_BLOCK);

    if (rc < 0) {
        trace (ERR, TH, "_SHR_sem_wait  IOS_ReceiveMessage returns %d\n", rc);
        return _SHR_ERR_FAIL;
    }

    return _SHR_ERR_OK;
}

int _SHR_sem_trywait (_SHRSemaphore* sem)
{
    IOSMessage  msg;

    IOSError rc = IOS_ReceiveMessage(sem->mq, &msg, IOS_MESSAGE_NOBLOCK);

    if (rc == IOS_ERROR_QEMPTY) {
        return _SHR_ERR_EAGAIN;
    }

    if (rc < 0) {
        trace (ERR, TH, "_SHR_sem_trywait  IOS_ReceiveMessage returns %d\n", rc);
        return _SHR_ERR_FAIL;
    }

    return _SHR_ERR_OK;
}

int _SHR_sem_post (_SHRSemaphore* sem)
{
    IOSError rc;

    if(0>(rc = IOS_SendMessage  (sem->mq, IOS_GetThreadId(), IOS_MESSAGE_NOBLOCK))) {
        if (sem->ignoreQFull) {
            return _SHR_ERR_OK;
        }
        trace (ERR, TH, "_SHR_sem_post  IOS_SendMessage returns %d\n", rc);
        return _SHR_ERR_FAIL;
    }

    return _SHR_ERR_OK;
}

int _SHR_sem_destroy (_SHRSemaphore* sem)
{
    IOSError rc = IOS_DestroyMessageQueue  (sem->mq);

    if (rc < 0) {
        trace (ERR, TH, "_SHR_sem_destroy  IOS_DestroyMessageQueue returns %d\n", rc);
        return _SHR_ERR_FAIL;
    }

    return _SHR_ERR_OK;
}



int _SHR_mutex_init(_SHRMutex  *mutex,
                    u32         type,
                    u32         pIdMask,
                    s32         lock)
{
    int     rv = _SHR_ERR_OK;
    IOSError rc;

    assert(mutex);
    mutex->type = type;
    mutex->lock_tid = lock ? _SHR_thread_self() : 0;
    mutex->lock_count = lock ? 1 : 0;

    mutex->mq = IOS_CreateMessageQueue (&mutex->msg, 1);

    if (mutex->mq < 0) {
        trace (ERR, TH, "_SHR_mutex_init  IOS_CreateMessageQueue returns %d\n",
               mutex->mq);
        mutex->lock_tid = 0;
        mutex->lock_count = 0;
        rv = _SHR_ERR_FAIL;
        goto end;
    }

    if (lock) {
        rc = IOS_SendMessage  (mutex->mq, mutex->lock_tid, IOS_MESSAGE_NOBLOCK);
        if (rc != IOS_ERROR_OK) {
            trace (ERR, TH,
                   "_SHR_mutex_init  IOS_SendMessage returns %d\n", rc);
            rv = _SHR_ERR_FAIL;
        }
    }

end:
    if (rv == _SHR_ERR_FAIL) {
        mutex->lock_tid = 0;
        mutex->lock_count = 0;
    }

    return rv;
}

int _SHR_mutex_lock(_SHRMutex* mutex) 
{
    IOSError     rc;
    int         rv = _SHR_ERR_OK;
   _SHRThreadId self = _SHR_thread_self();

    assert(mutex);
    assert(mutex->type != _SHR_MUTEX_NON_RECURSIVE
              || !_SHR_mutex_locked_self(mutex, self));

    if (_SHR_mutex_locked_self(mutex, self))  {
        if (mutex->type == _SHR_MUTEX_RECURSIVE) {
            ++mutex->lock_count;
            goto end;
        } else {
            trace (ERR, TH, "Attempt to lock non-recursive mutex "
                            "already locked by current thread");
            rv = _SHR_ERR_EDEADLK;
            goto end;
        }
    }

    rc = IOS_SendMessage  (mutex->mq, self, IOS_MESSAGE_BLOCK);

    if (rc != IOS_ERROR_OK) {
        trace (ERR, TH,
               "_SHR_mutex_lock  IOS_SendMessage returns %d\n", rc);
        rv = _SHR_ERR_FAIL;
        goto end;
    }

    mutex->lock_tid = self;
    ++mutex->lock_count;

end:
    return rv;
}


int _SHR_mutex_trylock(_SHRMutex* mutex) 
{
    IOSError     rc;
    int         rv = _SHR_ERR_OK;
   _SHRThreadId self = _SHR_thread_self();

    assert(mutex);
    rc = IOS_SendMessage  (mutex->mq, self, IOS_MESSAGE_NOBLOCK);

    if (rc == IOS_ERROR_OK) {
        mutex->lock_tid = self;
        ++mutex->lock_count;
    }
    if (rc == IOS_ERROR_QFULL) {
        rv = _SHR_ERR_EBUSY;
    }
    else {
        trace (ERR, TH,
               "_SHR_mutex_trylock  IOS_SendMessage returns %d\n", rc);
        rv = _SHR_ERR_FAIL;
    }

    return rv;
}


int _SHR_mutex_unlock(_SHRMutex* mutex)
{
    IOSMessage   msg;
    IOSError     rc;
    int         rv = _SHR_ERR_OK;
   _SHRThreadId self = _SHR_thread_self();
    assert(mutex);
    assert(mutex->type != _SHR_MUTEX_ERRORCHECK
             || _SHR_mutex_locked_self(mutex, self));

    if (!_SHR_mutex_locked_self(mutex, self)) {
        trace (ERR, TH, "Attempt to unlock mutex "
                         "not locked by current thread");
        rv = _SHR_ERR_EPERM;
    }
    else if (--mutex->lock_count == 0) {
        mutex->lock_tid = 0;
        rc = IOS_ReceiveMessage(mutex->mq, &msg, IOS_MESSAGE_NOBLOCK);
        if (rc != IOS_ERROR_OK) {
            trace (ERR, TH,
                    "_SHR_mutex_unlock  IOS_ReceiveMessage returns %d\n", rc);
            rv = _SHR_ERR_FAIL;
        }
    }  

    return rv;
}

int _SHR_mutex_destroy(_SHRMutex* mutex)
{
    int     rv = _SHR_ERR_OK;
    IOSError rc;
    
    assert(mutex);
    rc = IOS_DestroyMessageQueue  (mutex->mq);

    if (rc != IOS_ERROR_OK) {
        trace (ERR, TH,
               "_SHR_mutex_destroy  IOS_DestroyMessageQueue returns %d\n", rc);
        rv = _SHR_ERR_FAIL;
    }

    return rv;
}


/* Thread functions */

int _SHR_thread_create (_SHRThread      *thread,
                        _SHRThreadAttr  *attr,
                        _SHRThreadFunc   start_routine,
                         void           *arg)
{
    int       res = _SHR_ERR_OK;
    IOSError   rv;
    void     *stack;

    assert (thread);
    assert (attr);   // stack must be specified externally for SC

    if (attr) {
        assert(attr->stack);
        assert(attr->stackSize);
        stack = ((u8*)attr->stack) + attr->stackSize;
    }
    else {
        trace (ERR, TH, "_SHR_thread_create  attr->stack required for SC threads\n");
        res = _SHR_ERR_FAIL;
        goto end;
    }

    *thread = (_SHRThread) IOS_CreateThread ((IOSEntryProc) start_routine, arg, stack,
                                            attr->stackSize,
                                            attr->priority,
                                            attr->attributes); 

    if (*thread < 0) { 
        trace (ERR, TH, "_SHR_thread_create  IOS_CreateThread returns %d\n", *thread);
        res = _SHR_ERR_FAIL;
        goto end;
    }

    trace (FINER, TH, "_SHR_thread_create() thread id %u\n", *thread);

    if (attr->start && 0 > (rv = IOS_StartThread(*thread))) {
        trace (ERR, TH, "_SHR_thread_create  IOS_StartThread returns %d\n", rv);
        res = _SHR_ERR_FAIL;
    }

end:
    return res;
}



int _SHR_thread_join (_SHRThread    thread,
                      _SHRThreadRT *thread_return,
                       u32          timeout)

{
    int      rv = _SHR_ERR_OK;
    IOSError  rc;

    rc = IOS_JoinThread(thread, thread_return);
    if (rc != IOS_ERROR_OK) {
        trace (ERR, TH,
               "_SHR_thread_join  IOS_JoinThread returns %d\n", rc);
        rv = _SHR_ERR_FAIL;
    }

    return rv;
}



int _SHR_thread_sleep  (unsigned long msec)
{
    IOSMessageQueueId mq;
    IOSMessage        marray;
    IOSMessage        msg;
    IOSTimerId        timer;
    IOSError          err;
    int              rv = _SHR_ERR_FAIL;


    if(0>(mq = IOS_CreateMessageQueue (&marray, 1))) {
        trace (ERR, TH,
               "_SHR_thread_sleep  IOS_CreateMessageQueue returns %d\n", mq);
        goto end;
    }

    if (0>(timer = IOS_CreateTimer(_SHR_MSEC_2_IOSTIME (msec), 0, mq, 0xDEAFBEEF))) {
        trace (ERR, TH,
               "_SHR_thread_sleep  IOS_CreateTimer returns %d\n", timer);
    }
    else {
        if (0>(err = IOS_ReceiveMessage(mq, &msg, IOS_MESSAGE_BLOCK))) {
            trace (ERR, TH,
                "_SHR_thread_sleep  IOS_ReceiveMessage returns %d\n", err);
        }
        else {
            rv = _SHR_ERR_OK;
        }
        IOS_DestroyTimer(timer);
    }

    IOS_DestroyMessageQueue(mq);

end:
    return rv;
}

/*
*                Copyright (C) 2005, BroadOn Communications Corp.
* 
*   These coded instructions, statements, and computer programs contain
*   unpublished  proprietary information of BroadOn Communications Corp.,
*   and  are protected by Federal copyright law. They may not be disclosed
*   to  third  parties or copied or duplicated in any form, in whole or in
*   part, without the prior written consent of BroadOn Communications Corp.
* 
*/


#include "shr_plat.h"



#ifdef _WIN32
    #if _MSC_VER < 1400    
        size_t strnlen(const char *s, size_t maxlen)
        {
            const char *p = s;
            const char *e = s + maxlen;
            while (p != e && *p)
                ++p;

            return p - s;
        }
    #endif
#endif

/*
*                Copyright (C) 2005, BroadOn Communications Corp.
* 
*   These coded instructions, statements, and computer programs contain
*   unpublished  proprietary information of BroadOn Communications Corp.,
*   and  are protected by Federal copyright law. They may not be disclosed
*   to  third  parties or copied or duplicated in any form, in whole or in
*   part, without the prior written consent of BroadOn Communications Corp.
* 
*/


#define _GNU_SOURCE

#include "shr_th.h"
#include "shr_trace.h"
#include "shr_trace_i.h"


pthread_mutexattr_t _SHR_linux_mutex_attr[_SHR_NUM_MUTEX_TYPES] = {
    {PTHREAD_MUTEX_FAST_NP},
    {PTHREAD_MUTEX_RECURSIVE_NP},
    {PTHREAD_MUTEX_ERRORCHECK_NP},
};



int _SHR_mutex_init(_SHRMutex  *mutex,
                    u32         type,
                    u32         pIdMask,
                    s32         lock)
{
    int rv = _SHR_ERR_OK;
    int rc;

    assert(mutex);
    mutex->type = type;
    mutex->lock_tid = lock ? _SHR_thread_self() : 0;
    mutex->lock_count = lock ? 1 : 0;

    rc = pthread_mutex_init (&mutex->pth_mtx, &_SHR_linux_mutex_attr[type]);

    if (rc) {
        // pthread_mutex_init always returns 0, so this isn't really needed
        trace (ERR, TH, "_SHR_mutex_init  pthread_mutex_init returns %d\n",rc);
        mutex->lock_tid = 0;
        mutex->lock_count = 0;
        rv = _SHR_ERR_FAIL;
        goto end;
    }

    if (lock) {
        rc = pthread_mutex_lock (&mutex->pth_mtx);
        if (rc) {
            trace (ERR, TH,
                   "_SHR_mutex_init  pthread_mutex_lock returns %d\n", rc);
            rv = _SHR_ERR_FAIL;
        }
    }

end:
    if (rv == _SHR_ERR_FAIL) {
        mutex->lock_tid = 0;
        mutex->lock_count = 0;
    }

    return rv;
}


int _SHR_mutex_lock(_SHRMutex* mutex) 
{
    int         rc;
    int         rv = _SHR_ERR_OK;
   _SHRThreadId self = _SHR_thread_self();

    assert(mutex);
    assert(mutex->type != _SHR_MUTEX_NON_RECURSIVE
              || !_SHR_mutex_locked_self(mutex, self));

    if (_SHR_mutex_locked_self(mutex, self))  {
        if (mutex->type == _SHR_MUTEX_RECURSIVE) {
            ++mutex->lock_count;
            goto end;
        } else {
            trace (ERR, TH, "Attempt to lock non-recursive mutex "
                            "already locked by current thread");
            rv = _SHR_ERR_EDEADLK;
            goto end;
        }
    }

    rc = pthread_mutex_lock (&mutex->pth_mtx);

    if (rc) {
        trace (ERR, TH,
               "_SHR_mutex_lock  pthread_mutex_lock returns %d\n", rc);
        rv = _SHR_ERR_FAIL;
        goto end;
    }

    mutex->lock_tid = self;
    ++mutex->lock_count;

end:
    return rv;
}


int _SHR_mutex_trylock(_SHRMutex* mutex)
{
    int rv = _SHR_ERR_OK;
    int rc;

    assert(mutex);
    rc = pthread_mutex_trylock (&mutex->pth_mtx);

    if (!rc) {
        mutex->lock_tid = _SHR_thread_self();
        ++mutex->lock_count;
    }
    else if (rc == EBUSY) {
        rv = _SHR_ERR_EBUSY;
    }
    else {
        trace (ERR, TH, 
               "_SHR_mutex_trylock  pthread_mutex_trylock returns %d\n", rc);
        rv = _SHR_ERR_FAIL;
    }

    return rv;
}


int _SHR_mutex_unlock(_SHRMutex* mutex)
{
    int         rc;
    int         rv = _SHR_ERR_OK;
   _SHRThreadId self = _SHR_thread_self();

    assert(mutex);
    assert(mutex->type != _SHR_MUTEX_ERRORCHECK
             || _SHR_mutex_locked_self(mutex, self));

    if (!_SHR_mutex_locked_self(mutex, self)) {
        trace (ERR, TH, "Attempt to unlock mutex "
                         "not locked by current thread");
        rv = _SHR_ERR_EPERM;
    }
    else if (--mutex->lock_count == 0) {
        mutex->lock_tid = 0;
        rc = pthread_mutex_unlock(&mutex->pth_mtx);
        if (rc) {
            trace (ERR, TH,
                   "_SHR_mutex_unlock  pthread_mutex_unlock returns %d\n", rc);
            rv = _SHR_ERR_FAIL;
        }
    }

    return rv;
}

int _SHR_mutex_destroy(_SHRMutex* mutex)
{
    int         rv = _SHR_ERR_OK;
    int         rc;

    assert(mutex);
    rc = pthread_mutex_destroy (&mutex->pth_mtx);

    if (rc) {
        trace (ERR, TH,
               "_SHR_mutex_destroy  pthread_mutex_destroy returns %d\n", rc);
        if ((rc = EBUSY))
            rv = _SHR_ERR_EBUSY;
        else
            rv = _SHR_ERR_FAIL;
    }

    return rv;
}


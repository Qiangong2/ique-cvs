/*
*                Copyright (C) 2005, BroadOn Communications Corp.
* 
*   These coded instructions, statements, and computer programs contain
*   unpublished  proprietary information of BroadOn Communications Corp.,
*   and  are protected by Federal copyright law. They may not be disclosed
*   to  third  parties or copied or duplicated in any form, in whole or in
*   part, without the prior written consent of BroadOn Communications Corp.
* 
*/

#include "shr_th.h"
#include "shr_trace.h"

#ifdef _SC
    #include "sc/ios.h"
    #include "ioslibc.h"
    #include "sc/sc.h"
    #define IOS_GetTimer()    IO_READ(SYS_TIMER_REG)
#elif !defined(_GBA)
    #include <stdlib.h>
    #include <assert.h>
    #include <time.h>
    #include <string.h>
    #include <sys/timeb.h>
#endif



#define ctime_r(timep,buf)    (strcpy(buf,ctime(timep)))

#ifdef _WIN32
    #define snprintf  _snprintf
    #define vsnprintf _vsnprintf
#endif


#define DEF_TRACE_ENABLE                  true
#define DEF_TRACE_LEVEL                   TRACE_WARN
#ifdef _SC
    #define DEF_TRACE_SHOW_TIME           true
#else
    #define DEF_TRACE_SHOW_TIME           true
#endif
#define DEF_TRACE_FILE                    NULL
#define DEF_TRACE_SHOW_THREAD             true

#define DEF_VN_TRACE_ENABLE               true
#define DEF_VN_TRACE_ENABLE_MASK          0xFFFFFFFF
#define DEF_VN_TRACE_DISABLE_MASK         0
#define DEF_VN_TRACE_LEVEL                0
#define DEF_VN_TRACE_FILE                 NULL

#define DEF_VNG_LIB_TRACE_ENABLE          true
#define DEF_VNG_LIB_TRACE_ENABLE_MASK     0xFFFFFFFF
#define DEF_VNG_LIB_TRACE_DISABLE_MASK    0
#define DEF_VNG_LIB_TRACE_LEVEL           0
#define DEF_VNG_LIB_TRACE_FILE            NULL

#define DEF_SHR_LIB_TRACE_ENABLE          true
#define DEF_SHR_LIB_TRACE_ENABLE_MASK     0xFFFFFFFF
#define DEF_SHR_LIB_TRACE_DISABLE_MASK    0
#define DEF_SHR_LIB_TRACE_LEVEL           0
#define DEF_SHR_LIB_TRACE_FILE            NULL

#define DEF_TEST_TRACE_ENABLE             true
#define DEF_TEST_TRACE_ENABLE_MASK        0xFFFFFFFF
#define DEF_TEST_TRACE_DISABLE_MASK       0
#define DEF_TEST_TRACE_LEVEL              0
#define DEF_TEST_TRACE_FILE               NULL

#define DEF_APP_TRACE_ENABLE             true
#define DEF_APP_TRACE_ENABLE_MASK        0xFFFFFFFF
#define DEF_APP_TRACE_DISABLE_MASK       0
#define DEF_APP_TRACE_LEVEL              0
#define DEF_APP_TRACE_FILE               NULL



typedef struct {
    bool        enable;
    const char *enable_evn;    /*  env var name */
    unsigned    enable_mask;
    const char *enable_mask_evn;
    unsigned    disable_mask;
    const char *disable_mask_evn;
    int         level;
    const char *level_evn;
    const char *filename;
    const char *filename_evn;
    int         levels[32];
    const char *filenames[32];

} _VNGTraceGrp;



static int         trace_initialized;
static int         trace_enable      = DEF_TRACE_ENABLE;
static int         trace_show_time   = DEF_TRACE_SHOW_TIME;
static int         trace_show_thread = DEF_TRACE_SHOW_THREAD;
static const char *trace_filename    = DEF_TRACE_FILE;
static char        prefix[32];

int  _SHR_trace_level  =  DEF_TRACE_LEVEL;


/* Env var names for base trace options */

#define ENV_BASE_TRACE_ENABLE  "VNG_TRACE_ENABLE"  /* 0 or not 0 */
#define ENV_BASE_TRACE_LEVEL   "VNG_TRACE_LEVEL"   /* 0 or above */
#define ENV_BASE_TRACE_TIME    "VNG_TRACE_TIME"    /* 0 or not 0 */
#define ENV_BASE_TRACE_THREAD  "VNG_TRACE_THREAD"  /* 0 or not 0 */
#define ENV_BASE_TRACE_FILE    "VNG_TRACE_FILE"    /* 0, filename, or stdout */

/* ENV_BASE_TRACE_TIME=1 specifies to display timestamped messages */
/* ENV_BASE_TRACE_THREAD=1 specifies to display thread id */


/*  Env var names for Groups and Sub-groups
*
*   Group environment variable names are specified in
*   the initialization of grps[] below.
*
*   Bit 0-31 of the enbable/disable masks refer to sub-groups 0-31
*
*   Sub-group env vars are available for setting
*       sub-group trace level
*       sub-group trace log filename
*
*   Sub-group env var names are formulated as
*
*          GroupEnvVarName_sg
*
*  where  GroupEnvVarName is the group env var name
*  and sg is the sub-group number.  For example:
*
*      "VN_TRACE_LEVEL"    -- specifies VN group trace level
*      "VN_TRACE_LEVEL_2"  -- specifies VN sub-group 2 trace level
*
*      "VN_TRACE_FILE"     -- specifies VN group trace log filename
*      "VN_TRACE_FILE_0"   -- specifies VN sub-group 0 trace log filename
*/


static _VNGTraceGrp grps[] = {

    { DEF_VN_TRACE_ENABLE,             "VN_TRACE_ENABLE",
      DEF_VN_TRACE_ENABLE_MASK,        "VN_TRACE_ENABLE_MASK",
      DEF_VN_TRACE_DISABLE_MASK,       "VN_TRACE_DISABLE_MASK",
      DEF_VN_TRACE_LEVEL,              "VN_TRACE_LEVEL",
      DEF_VN_TRACE_FILE,               "VN_TRACE_FILE" },

    { DEF_VNG_LIB_TRACE_ENABLE,        "VNG_LIB_TRACE_ENABLE",
      DEF_VNG_LIB_TRACE_ENABLE_MASK,   "VNG_LIB_TRACE_ENABLE_MASK",
      DEF_VNG_LIB_TRACE_DISABLE_MASK,  "VNG_LIB_TRACE_DISABLE_MASK",
      DEF_VNG_LIB_TRACE_LEVEL,         "VNG_LIB_TRACE_LEVEL",
      DEF_VNG_LIB_TRACE_FILE,          "VNG_LIB_TRACE_FILE" },

    { DEF_SHR_LIB_TRACE_ENABLE,        "SHR_LIB_TRACE_ENABLE",
      DEF_SHR_LIB_TRACE_ENABLE_MASK,   "SHR_LIB_TRACE_ENABLE_MASK",
      DEF_SHR_LIB_TRACE_DISABLE_MASK,  "SHR_LIB_TRACE_DISABLE_MASK",
      DEF_SHR_LIB_TRACE_LEVEL,         "SHR_LIB_TRACE_LEVEL",
      DEF_SHR_LIB_TRACE_FILE,          "SHR_LIB_TRACE_FILE" },

    { DEF_TEST_TRACE_ENABLE,           "TEST_TRACE_ENABLE",
      DEF_TEST_TRACE_ENABLE_MASK,      "TEST_TRACE_ENABLE_MASK",
      DEF_TEST_TRACE_DISABLE_MASK,     "TEST_TRACE_DISABLE_MASK",
      DEF_TEST_TRACE_LEVEL,            "TEST_TRACE_LEVEL",
      DEF_TEST_TRACE_FILE,             "TEST_TRACE_FILE" },

    { DEF_APP_TRACE_ENABLE,            "APP_TRACE_ENABLE",
      DEF_APP_TRACE_ENABLE_MASK,       "APP_TRACE_ENABLE_MASK",
      DEF_APP_TRACE_DISABLE_MASK,      "APP_TRACE_DISABLE_MASK",
      DEF_APP_TRACE_LEVEL,             "APP_TRACE_LEVEL",
      DEF_APP_TRACE_FILE,              "APP_TRACE_FILE" },
};


static const char* trace_lev_str[] =
{
    "OFF ",
    "ERROR ",
    "WARN ",
    "INFO ",
    "FINE ",
    "FINER ",
    "FINEST "
};

static const char* trace_grp_str[] =
{
    "VN ",
    "VNG ",
    "SHR ",
    "Test ",
    "App "
};



// Return time in ms since the first invocation
static 
unsigned int _shr_get_timestamp()
{
#ifdef _SC
    return IOS_GetTimer()/500;
#elif defined(_GBA)
    return 0;
#else
    static u32 earliest_time = 0;
    int t;
    struct timeb timebuffer;
    // use 'obsoleted' ftime to otbain ms resolution for Unix and Windows
    //  - gettimeofday is not available on Windows.
    ftime(&timebuffer);
    t = (unsigned int) (timebuffer.time * 1000 + timebuffer.millitm);
    if (earliest_time == 0) earliest_time = t;
    return t - earliest_time;
#endif
}


void _SHR_trace_init ()
{

#if !defined(_SC) && !defined(_GBA)

    char* env_str;
    int   num_grps = sizeof(grps)/sizeof(grps[0]);
    int   i,j, len;
    char  l_evn[256];
    char  f_evn[256];

    for (i = 0;  i < num_grps;  ++i) {
        if ((env_str = getenv(grps[i].enable_evn)) != NULL) {
            grps[i].enable = strtol(env_str, NULL, 0) ? 1 : 0;
        }
        if ((env_str = getenv(grps[i].enable_mask_evn)) != NULL) {
            grps[i].enable_mask = strtoul(env_str, NULL, 0);
        }
        if ((env_str = getenv(grps[i].disable_mask_evn)) != NULL) {
            grps[i].disable_mask = strtoul(env_str, NULL, 0);
        }
        if ((env_str = getenv(grps[i].level_evn)) != NULL) {
            grps[i].level = strtoul(env_str, NULL, 0);
        }
        if ((env_str = getenv(grps[i].filename_evn)) != NULL) {
            grps[i].filename = env_str; /* Note: not copied */
        }

        for (j = 0;  j < 32;  ++j) {
            len = snprintf (l_evn, sizeof l_evn, "%s_%d", grps[i].level_evn, j);
            assert (len > 0 && (size_t)len < sizeof l_evn);
            len = snprintf (f_evn, sizeof f_evn, "%s_%d", grps[i].filename_evn, j);
            assert (len > 0 && (size_t)len < sizeof f_evn);
            if ((env_str = getenv(l_evn)) != NULL) {
                grps[i].levels[j] = strtoul(env_str, NULL, 0);
            }
            if ((env_str = getenv(f_evn)) != NULL) {
                grps[i].filenames[j] = env_str; /* Note: not copied */
            }
        }
    }

    if ((env_str = getenv(ENV_BASE_TRACE_ENABLE)) != NULL) {
        trace_enable = strtol(env_str, NULL, 0);
    }

    if ((env_str = getenv(ENV_BASE_TRACE_LEVEL)) != NULL) {
        _SHR_trace_level = strtoul(env_str, NULL, 0);
    }

    if ((env_str = getenv(ENV_BASE_TRACE_TIME)) != NULL) {
        trace_show_time = strtol(env_str, NULL, 0);
    }

    if ((env_str = getenv(ENV_BASE_TRACE_THREAD)) != NULL) {
        trace_show_thread = strtol(env_str, NULL, 0);
    }

    if ((env_str = getenv(ENV_BASE_TRACE_FILE)) != NULL) {
        trace_filename = env_str;  /* Note: not copied */
    }

#endif

    trace_initialized = 1;
    _shr_get_timestamp(); // call for the side-effects of initializing start time
}


const char* _SHR_trace_lev_str (int level)
{
    int index = level;
    if ((index < 0) || (index >= sizeof(trace_lev_str)/sizeof(trace_lev_str[0])))
        return "      ";
    else return trace_lev_str[index];
}

const char* _SHR_trace_grp_str (int grp_num)
{
    int index = grp_num;
    if ((index < 0) || (index >= sizeof(trace_grp_str)/sizeof(trace_grp_str[0])))
        return "      ";
    else return trace_grp_str[index];
}


static
void _shr_vfprintf (FILE* fp,
                    const char* level,
                    const char* group,
                    unsigned    sub_grp,
                    bool        show_time, 
                    bool        show_thread, 
                    const char* fmt, va_list ap)
{ 
    char   buf[80];
    size_t msglen = 0;

    buf[0] = '\0';
 
    // string functions, such as puts and strcpy, are not available in SC 
    // use printf and snprintf that is available for all platforms
    // it is better to minimize the use of #ifdef for clarity.
    //
    if (prefix[0]) {
        msglen += snprintf(&buf[msglen], sizeof(buf)-msglen, "%s", prefix);
    }
 
    if (show_time) {
        msglen += snprintf(&buf[msglen], sizeof(buf)-msglen, "%06u ", _shr_get_timestamp());
    }

    if (show_thread) {
        msglen += snprintf(&buf[msglen], sizeof(buf)-msglen, "%08lx ", _SHR_thread_self());
    }

    if (level) {
        msglen += snprintf(&buf[msglen], sizeof(buf)-msglen, "%s", level);
    }

    if (group) {
        msglen += snprintf(&buf[msglen], sizeof(buf)-msglen, "%s%u ", group, sub_grp);
    }
 
#if !defined(_SC) && !defined(_GBA)
    assert (fp);
    fprintf(fp, "%s", buf);
    vfprintf(fp, fmt, ap);
#else
    printf("%s", buf);
    vprintf(fmt, ap);
#endif

}


void _SHR_set_trace_prefix(const char *str)
{
    memcpy(prefix, str, strnlen(str,sizeof(prefix)));
    prefix[sizeof(prefix)-1] = '\0';
}

static
void _shr_vflog (const char* filename,
                 const char* level,
                 const char* group,
                 unsigned    sub_grp,
                 bool        show_time,
                 bool        show_thread,
                 const char* fmt, va_list ap)
{
    FILE* log_fp = NULL;

#if !defined(_SC) && !defined(_GBA)
        int   file = 0;

        log_fp = stdout;
        if (filename && *filename && strcmp(filename, "stdout")) {
            log_fp = fopen(filename, "a");
            if (log_fp == NULL) {
                log_fp = stdout;
            }
            else {
                file = 1;
            }
        }
    #endif

   _shr_vfprintf(log_fp, level, group, sub_grp, show_time, show_thread, fmt, ap);

#if !defined(_SC) && !defined(_GBA)
        if (file) {
            fclose(log_fp);
        }
        else {
            fflush (stdout);
        }
    #endif
}



void _SHR_vtrace (int level, int grp_num, int sub_grp,
                  const char* fmt, va_list ap)
{
   _VNGTraceGrp *grp;
    const char  *filename;

    grp  =  & grps [grp_num];

    if (grp->filenames[sub_grp] && *grp->filenames[sub_grp])
        filename = grp->filenames[sub_grp];
    else if (grp->filename && *grp->filename)
        filename = grp->filename;
    else
        filename = trace_filename;

   _shr_vflog (filename,
              _SHR_trace_lev_str(level),
              _SHR_trace_grp_str(grp_num),
              sub_grp,
              trace_show_time,
              trace_show_thread,
              fmt, ap);
}



void _SHR_trace (int level, int grp, int sub_grp,  const char* fmt, ...)
{
    va_list ap;

    if (level > TRACE_MAX_LEVEL)
        level = TRACE_MAX_LEVEL;

    if (grp > _TRACE_MAX_GRP || sub_grp > _TRACE_MAX_SUBGRP)
        return;

    if (!_SHR_trace_on(level, grp, sub_grp))
        return;

    va_start (ap, fmt);
   _SHR_vtrace (level, grp, sub_grp, fmt, ap);
    va_end (ap);
}



int _SHR_trace_on (int level, int grp_num, int sub_grp)
{
    unsigned     sub_grp_mask =  1 << sub_grp;
   _VNGTraceGrp *grp          =  & grps [grp_num];
    int          maxlevel;

    if (!trace_initialized)
        _SHR_trace_init();

    if (grp->levels[sub_grp])
        maxlevel = grp->levels[sub_grp];
    else if (grp->level)
        maxlevel = grp->level;
    else
        maxlevel = _SHR_trace_level;

    if (    !trace_enable
         || !grp->enable
         || !(sub_grp_mask & grp->enable_mask)
         ||  (sub_grp_mask & grp->disable_mask)
         ||  !maxlevel
         ||  level > maxlevel ) {
        return false;
    }

    return true;
}


int _SHR_set_trace_enable (int enable) {
    bool prev;
    if (!trace_initialized)
        _SHR_trace_init();
    prev = trace_enable;
    trace_enable = enable;
    return prev;
}

int _SHR_set_trace_level (int level) {
    int prev;

    if (level > TRACE_MAX_LEVEL)
        level = TRACE_MAX_LEVEL;

    if (!trace_initialized)
        _SHR_trace_init();
    prev = _SHR_trace_level;
    _SHR_trace_level = level;
    return prev;
}


int _SHR_set_trace_show_time (int show_time) {
    bool prev;
    if (!trace_initialized)
        _SHR_trace_init();
    prev = trace_show_time;
    trace_show_time = show_time;
    return prev;
}

int _SHR_set_trace_show_thread (int show_thread) {
    bool prev;
    if (!trace_initialized)
        _SHR_trace_init();
    prev = trace_show_thread;
    trace_show_thread = show_thread;
    return prev;
}

const char* _SHR_set_trace_file (const char* filename) {
    const char *prev;
    if (!trace_initialized)
        _SHR_trace_init();
    prev = trace_filename;
    trace_filename = filename;
    return prev;
}

int _SHR_set_grp_trace_enable (int grp, int enable) {
    bool prev;
    if (!trace_initialized)
        _SHR_trace_init();
    prev = grps[grp].enable;
    grps[grp].enable = enable;
    return prev;
}

int _SHR_set_grp_trace_level (int grp, int level) {
    int prev;

    if (level > TRACE_MAX_LEVEL)
        level = TRACE_MAX_LEVEL;
    
    if (grp > _TRACE_MAX_GRP)
        return _SHR_ERR_FAIL;

    if (!trace_initialized)
        _SHR_trace_init();
    prev = grps[grp].level;
    grps[grp].level = level;
    return prev;
}

int _SHR_set_sg_trace_level (int grp, int sub_grp, int level) {
    int prev;

    if (level > TRACE_MAX_LEVEL)
        level = TRACE_MAX_LEVEL;
    
    if (grp > _TRACE_MAX_GRP || sub_grp > _TRACE_MAX_SUBGRP)
        return _SHR_ERR_FAIL;

    if (!trace_initialized)
        _SHR_trace_init();
    prev = grps[grp].levels[sub_grp];
    grps[grp].levels[sub_grp] = level;
    return prev;
}

unsigned  _SHR_set_grp_trace_enable_mask  (int grp, unsigned mask)
{
    unsigned prev;

    if (grp > _TRACE_MAX_GRP)
        return 0;

    if (!trace_initialized)
        _SHR_trace_init();
    prev = grps[grp].enable_mask;
    grps[grp].enable_mask = mask;
    return prev;
}

unsigned  _SHR_set_grp_trace_disable_mask (int grp, unsigned mask)
{
    unsigned prev;

    if (grp > _TRACE_MAX_GRP)
        return 0;

    if (!trace_initialized)
        _SHR_trace_init();
    prev = grps[grp].disable_mask;
    grps[grp].disable_mask = mask;
    return prev;
}

const char* _SHR_set_grp_trace_file (int grp, const char* filename) {
    const char *prev;

    if (grp > _TRACE_MAX_GRP)
        return 0;

    if (!trace_initialized)
        _SHR_trace_init();
    prev =  grps[grp].filename;
    grps[grp].filename = filename;
    return prev;
}

const char* _SHR_set_sg_trace_file (int grp, int sub_grp, const char* filename) {
    const char *prev;

    if (grp > _TRACE_MAX_GRP || sub_grp > _TRACE_MAX_SUBGRP)
        return 0;

    if (!trace_initialized)
        _SHR_trace_init();
    prev =  grps[grp].filenames[sub_grp];
    grps[grp].filenames[sub_grp] = filename;
    return prev;
}


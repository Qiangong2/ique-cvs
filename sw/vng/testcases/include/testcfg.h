/* This file contains the configuration used by 
   examples programs.  Modify VNGS_HOST to 
   access your own instance of VNG server */

/* Use beta test env */
// #define VNGS_HOST "172.16.10.65"
// #define VNGS_HOST "vngs.idc-beta.broadon.com"
// #define VNGS_HOST "server65.idc-beta.vpn.broadon.com"
// #define VNGS_HOST "vngs.idc-beta.vpn.broadon.com"
// #define VNGS_HOST "server71.bbu.lab1.routefree.com"
#define VNGS_HOST "ogs.bbu.lab1.routefree.com"

/* Dedicated port for VNG server - do not change */
#define VNGS_PORT 16978

/* Use default 5 seconds timeout for blocking VNG calls */
#define VNG_TIMEOUT  5000 

/* Tester account */
#define TESTER01   "10001"

/* Tester password */
#define TESTER01PW   "10001"

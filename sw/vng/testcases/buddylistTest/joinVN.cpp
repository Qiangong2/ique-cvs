//
//               Copyright (C) 2005, iQue .
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of iQue,They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of iQue.
//
//


#include "stdafx.h"
#include "vng.h"
#include "../include/testcfg.h"
#include "budyList.h"
#include "ptpSendMsg.h"


using namespace std;
#include <iostream>
#include <time.h>
#include <set>
#include <hash_set>
using namespace stdext;

bool trace = true;

void report(const char *testname, int pass)
{
    if (pass >= 0) 
        fprintf(stdout, "*** LIBVNG %s TEST PASSED\n", testname);
    else
        fprintf(stdout, "*** LIBVNG %s TEST FAILED\n", testname);
}

void dumpVN(VN *vn)
{
    VNId vid;
    VNGErrCode err = VN_GetVNId(vn, &vid);
    if (err) {
        fprintf(stderr, "VNG_GetVNId returns %d\n", err);
        return;
    }
    fprintf(stdout, "VNId is 0x%08x.%08x\n", vid.devId, vid.vnetId);

    VNMember mem = VN_Self(vn);
    VNMember owner = VN_Owner(vn);
    VNState state = VN_State(vn);
    int num = VN_NumMembers(vn);

    fprintf(stdout, "VN Self is %d\n", mem);
    fprintf(stdout, "VN Owner is %d\n", owner);
    fprintf(stdout, "VN State is %d\n", (int)state);
    fprintf(stdout, "VN member count is %d\n", num);

    VNMember mems[100];
    int total_mem = VN_GetMembers(vn, mems, 100);
    for (int i = 0; i < total_mem; i++) {
        fprintf(stdout, "mem[%d] is %d\n", i, mems[i]);
    }
}

//flag :
//	true--打印出捕获的事件类型
//	false--不打印
void dumpEvents(VNG *vng, bool flag)
{
    // Wait for all events
    while (1) {
        VNGEvent event;
        VNGErrCode err = VNG_GetEvent(vng, &event, VNG_NOWAIT);
        if (err == VNG_OK) {
            if (flag) {
           fprintf(stdout, "Get event %d", event.eid);
           if (event.eid == VNG_EVENT_PEER_STATUS) {
               VNId vid;
               VN_GetVNId(event.evt.peerStatus.vn, &vid);
               fprintf(stdout, " -- 0x%08x.%08x %d", vid.devId, vid.vnetId, event.evt.peerStatus.memb);
           }    
           fprintf(stdout, "\n");
            }
        } else if (err == VNGERR_NOWAIT || err == VNGERR_TIMEOUT)
           break;
    }
}



int testNewVN(const char *testname, VNDomain domain)
{
    VNGErrCode err;
    VNG vng;
    char vngbuf[16*1024];

    VNG_Init(&vng, vngbuf, sizeof(vngbuf));

    err = VNG_Login(&vng, VNGS_HOST, VNGS_PORT,
 		TESTER01, TESTER01PW, 
            	VNG_USER_LOGIN, VNG_TIMEOUT);

    if (trace) {
        fprintf(stdout, "VNG_Login returns %d\n", err);
    }
    if (err != VNG_OK) {
        fprintf(stderr, "VNG_Login returns %d\n", err);
        return -1;
    }

    VN vn;
    err = VNG_NewVN(&vng, &vn,VN_DEFAULT_CLASS, domain, VN_DEFAULT_POLICY);
    if (trace) {
        fprintf(stdout, "VNG_NewVN returns %d\n", err);
    }
    if (err) {
        fprintf(stderr, "VNG_NewVN returns %d\n", err);
        return -1;
    }

    VNG_DeleteVN(&vng, &vn);
    VNG_Fini(&vng);

    return 0;
}


#define MAX_ERROR_INFO_SIZE	256
#define ERROR_INFO_NUM      100
int testErrorHandling()
{
	char msgBuf[MAX_ERROR_INFO_SIZE];
	//char *msgBuf=NULL;
	VNGErrCode err;
	int r=1;
	
	char definedMsg[ERROR_INFO_NUM][MAX_ERROR_INFO_SIZE]={
	"",
	"A failure occured that does not have a specific error message.",
	"Non-blocking call returns immediately.",
	"Timeout before operation could complete.",
	"A VN server connection was not available.",
	"An action was attempted that requires login.",
	"Output value buffer overflow.",
	"Out of memory.",
	"The VNG session is not initialized or has been terminated",
	"Info being retrieved was not found.",
	"An API failed because an internal buffer was full.",
	"",
	"",
	"",
	"",
	"",
	"A connection was refused.",
	"A peer or server was not reachable.",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"The login name is not recognized.",
	"The passwd is not valid for the login name.",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",	
	"Attempted reject/accept without invite.",
	"Unknown subscriber name.",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",		
	"The game ended or was aborted",
	"Host/server denied a join request.",
	"Missing RPC procedure.",
	"Missing session status value.",
	"The API is only valid for the VN owner.",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"An argument to an API was invalid.",
	"The VNG associated with an API was invalid.",
	"The VN associated with an API was invalid.",
	"A member value associated with an API was invalid.",
	"A length argument to an API was invalid.",
	};
	



	for(int i=0;i<100;i++)
	{
		err=(VNGErrCode)i;
		VNG_ErrMsg(err,msgBuf,sizeof(msgBuf));
		fprintf(stdout,"ErrorHanding code %i : %s \n",err,msgBuf);
		if(strcmp(msgBuf,definedMsg[err])!=0)
			r=-1;
		//fprintf(stdout,"ErrorHanding code %i : %s \n",err,definedMsg[err]);
		
	}

	return r;
}

void dumpErrorInfo(VNGErrCode errCode){
	char msg[MAX_ERROR_INFO_SIZE];
	if(errCode<0||errCode>100){
		fprintf(stdout,"Error:parameter invalid!");
		return;
		}
	VNG_ErrMsg(errCode, msg, MAX_ERROR_INFO_SIZE);
	fprintf(stdout,"ErrorCode %i : %s  \n",errCode,msg);
}
#if 0
#define TIMEOUTFORTEST1   1
#define LOGINNAMEFORTEST2   "DFSDGSDF"
#define LOGINPASSFORTEST3    "DFSGDD"

//create a new thread to join the VN  and blocking to get events
DWORD WINAPI Test4ThreadPro(LPVOID pParam){

	VNGErrCode err;
    VNG vng;
    char vngbuf[16*1024];
	int * p=NULL;

    p=(int*)pParam;
    fprintf(stdout,"thread begin!\n");
    VNG_Init(&vng, vngbuf, sizeof(vngbuf));
   err = VNG_Login(&vng, VNGS_HOST, VNGS_PORT,
 		TESTER01, TESTER01PW, 
            	VNG_USER_LOGIN, VNG_TIMEOUT);
    fprintf(stdout,"thread login finish!\n");
   if(err==VNG_OK){
   	fprintf(stdout,"login ok in the thread!\n");
	//dumpEvents(&vng);
	err=VNG_Logout(&vng, VNG_TIMEOUT);
	
	}
   else 
   	dumpErrorInfo(err);
   fprintf(stdout,"thread exit!\n");
  *p=1;
	return 1;
}





int testJoinVNnew(const char *testname, VNDomain domain){
VNGErrCode err;
    VNG vng;
    char vngbuf[16*1024];

    VNG_Init(&vng, vngbuf, sizeof(vngbuf));

   //test 1: input a  timeout value to make a VNGERR_TIMEOUT error;
    fprintf(stdout,"test 1: input a  invalid server port  to make a VNGERR_TIMEOUT error;\n");
   err = VNG_Login(&vng, VNGS_HOST, 234,
 		TESTER01, TESTER01PW, 
            	VNG_USER_LOGIN, TIMEOUTFORTEST1);
   if(err==VNG_OK){
	err=VNG_Logout(&vng, VNG_TIMEOUT);
	dumpErrorInfo(err);
   	}
   else 
   	dumpErrorInfo(err);

   //test 2: input a invalid loginName
    fprintf(stdout,"test 2: input a invalid loginName. \n");
    err = VNG_Login(&vng, VNGS_HOST, VNGS_PORT,
 		LOGINNAMEFORTEST2, TESTER01PW, 
            	VNG_USER_LOGIN, VNG_TIMEOUT);
   if(err==VNG_OK){
	err=VNG_Logout(&vng, VNG_TIMEOUT);
	dumpErrorInfo(err);
   	}
   else 
   	dumpErrorInfo(err);
      //test 3: input a invalid login PASS
    fprintf(stdout,"test 3: input a invalid login PASS. \n");
    err = VNG_Login(&vng, VNGS_HOST, VNGS_PORT,
 		TESTER01, LOGINPASSFORTEST3, 
            	VNG_USER_LOGIN, VNG_TIMEOUT);
   if(err==VNG_OK){
	err=VNG_Logout(&vng, VNG_TIMEOUT);
	dumpErrorInfo(err);
   	}
   else 
   	dumpErrorInfo(err);
   //test 4: create a thread to join the VN and blocking to get events.
	 fprintf(stdout,"test 4: create a thread to join the VN and blocking to get events.\n");
	HANDLE myThread;
	DWORD threadID;
	int  p=0;
	myThread=CreateThread(NULL,0,Test4ThreadPro,&p,0,(LPDWORD)&threadID);
	//test 4: login again after the thread has login using this logingName/password.
	//Sleep(10000);
	while(p==0){
			Sleep(100);
		}
#if 0
	fprintf(stdout,"test 4: login again .\n");
    err = VNG_Login(&vng, VNGS_HOST, VNGS_PORT,
 		TESTER01, TESTER01PW, 
            	VNG_USER_LOGIN, VNG_TIMEOUT);
   if(err==VNG_OK){
	   fprintf(stdout,"login ok .\n");
	err=VNG_Logout(&vng, VNG_TIMEOUT);
	if(err==0)fprintf(stdout,"logout ok.\n");
   	}
   else 
   	dumpErrorInfo(err);
#endif
return 1;

}


int testJoinVN(const char *testname, VNDomain domain)
{
    VNGErrCode err;
    VNG vng;
    char vngbuf[16*1024];

    VNG_Init(&vng, vngbuf, sizeof(vngbuf));

    err = VNG_Login(&vng, VNGS_HOST, VNGS_PORT,
 		TESTER01, TESTER01PW, 
            	VNG_USER_LOGIN, VNG_TIMEOUT);

    if (trace) {
        fprintf(stdout, "VNG_Login returns %d\n", err);
    }
    if (err != VNG_OK) {
        fprintf(stderr, "VNG_Login returns %d\n", err);
        return -1;
    }

    VNGUserId uid = VNG_MyUserId(&vng);

    fprintf(stdout, "VNGUserId 0x%08x.%08x\n", (int)(uid >> 32), (int)uid);

    VNGUserInfo uinfo;
    err = VNG_GetUserInfo(&vng, uid, &uinfo,VNG_TIMEOUT);
    if (err == VNG_OK) {
        fprintf(stdout, "login is %s\n", uinfo.login);
        fprintf(stdout, "nickname is %s\n", uinfo.nickname);
    } else {
        fprintf(stderr, "VNG_GetUserInfo returns %d\n", err);
        return -1;
    }


    VN vn;
    err = VNG_NewVN(&vng, &vn,VN_DEFAULT_CLASS, domain, VN_DEFAULT_POLICY);
    if (trace) {
        fprintf(stdout, "VNG_NewVN returns %d\n", err);
    }
    if (err) {
        fprintf(stderr, "VNG_NewVN returns %d\n", err);
        return -1;
    }

    VNId vid;
    err = VN_GetVNId(&vn, &vid);
    if (err) {
        fprintf(stdout, "VNG_GetVNId returns %d\n", err);
        return -1;
    }

    dumpVN(&vn);
 
    VN vnx[100];
    for (int i = 0; i < 100; i++) {
        if (trace)
            fprintf(stdout, "%d -- Entering VNG_JoinVN %d\n", time(NULL), err);
        err = VNG_JoinVN(&vng, vid, "Hello", &vnx[i], NULL, 0, 10*1000);
        if (trace)
            fprintf(stdout, "%d -- VNG_JoinVN returns %d\n", time(NULL), err);
        if (err == VNG_OK) {
            int n = VN_NumMembers(&vnx[i]);
            if (n != i+2) {
                fprintf(stdout, "Incorrect number of members %d for Join[%d]\n", n, i);
                return -1;
            }
        } else {
            if (i != 15) {
                fprintf(stdout, "It is expected add another 15 VNs\n");
                return -1;
            }
            break;
        }
    }

    fprintf(stdout, "Dump VN[14]:\n");
    dumpVN(&vnx[14]);

    dumpEvents(&vng,false);
     
    VNG_DeleteVN(&vng, &vn);

    dumpEvents(&vng,false);

    VNG_Fini(&vng);

    return 0;
}

void testGetUserInfo(){
   //如果不登陆就调用VNG_MyUserId ，会返回一个什么:VNG_INVALID_USER_ID
	    VNGErrCode err;
    VNG vng;
    char vngbuf[16*1024];
   //char *vngbuf;
	VNGUserInfo  uinfo;
	VNGUserId uid;

       VNG_Init(&vng, vngbuf, sizeof(vngbuf));
	uid = VNG_MyUserId(&vng);
	if(uid!=VNG_INVALID_USER_ID){
		fprintf(stdout, "testGetUserInfo:ERROR: uid err!");
	}
	fprintf(stdout,"testGetUserInfo:uid=%i\r\n",uid);
//如果不登陆就调用VNG_GetUserInfo ，会返回一个什么?
	err=VNG_GetUserInfo(&vng,  uid, &uinfo,VNG_TIMEOUT);
	fprintf(stdout,"testGetUserInfo:err=%i\r\n",err);
	if (err == VNG_OK) {
        fprintf(stdout, "testGetUserInfo:login is %s\n", uinfo.login);
        fprintf(stdout, "testGetUserInfo:nickname is %s\n", uinfo.nickname);
    } else {
        fprintf(stderr, "testGetUserInfo:VNG_GetUserInfo returns %d\n", err);
        return;
    }

}


#endif

/////////////////////////////////////////////////////////////////////////////////
//关于VN   的一些测试函数
///////////////////////////////////////////////////////////////////////////////








 int createVN(VNG* vng,VN **vn,VNDomain domain){

	    VNGErrCode err;
	    VN * tempVN=NULL;
	    int vn_class_type=-1;

	     tempVN = (VN*)malloc(sizeof(VN));
		while(vn_class_type<0||vn_class_type>3)
					{
						fprintf(stdout,"Please choose the VN domain type:(VN_CLASS_1=0, VN_CLASS_2=1,  VN_CLASS_3=2, VN_CLASS_4=3)\r\n");
						scanf("%d",&vn_class_type);
					}
	    err = VNG_NewVN(vng, tempVN, (VNClass)vn_class_type,domain, VN_DEFAULT_POLICY);

	    if (err!=VNG_OK) {
	        fprintf(stderr, "VNG_NewVN returns %d\n", err);
	        return -1;
	    }

	    VNId vid;
	    err = VN_GetVNId(tempVN, &vid);
	    if (err!=VNG_OK) {
	        fprintf(stdout, "VNG_GetVNId returns %d\n", err);
	        return -1;
	    }	
	
		*vn=tempVN;
	    return 1;
    }

 int registVN_updateStatus(VNG * vng,VN *vn ){
	//把一个本地创建的vn 注册到数据库中。
	 VNGErrCode err;
	 VNGGameInfo  info;
	 char  comments[100];
	  VNId vid;
	  VNGUserInfo uinfo;
	  
       err = VN_GetVNId(vn, &vid);
       if (err) {
	        fprintf(stdout, "Err:VNG_GetVNId failure! returns %d\n", err);
	        return -1;
       }

       dumpVN(vn);

	//初始化游戏信息。
	info.vnId.devId=vid.devId;
	info.vnId.vnetId=vid.vnetId;
	info.owner=VNG_MyUserId(vng);
	info.gameId=12;
	info.titleId=12;

	info.accessControl = VNG_GAME_PUBLIC;
       info.totalSlots = 2;
       info.buddySlots = 0;   // nothing reserved for buddies
       info.maxLatency = 500; // 500ms RTT 
	info.netZone = 0;      // server will decide this value
	info.attrCount = 1;
	
	err = VNG_GetUserInfo(vng, info.owner, &uinfo,VNG_TIMEOUT);
   	 if (err != VNG_OK) {
       	 fprintf(stderr, "VNG_GetUserInfo returns %d\n", err);
       	 return -1;
   		 }
	sprintf(info.keyword,"%s created game!",uinfo.login);
	//fprintf(stdout,"%s\n",info.keyword);
	sprintf(comments,"%s created game comments!",uinfo.login);
	//fprintf(stdout,"%s\n",comments);

	//在注册游戏到数据库的时候加入游戏信息
	err=VNG_RegisterGame(vng, &info, comments, VNG_TIMEOUT);
	 if (err != VNG_OK) {
       	 fprintf(stderr, "Err:VNG_RegisterGame failure! returns %d\n", err);
       	 return -1;
   		 }

	err=VNG_UpdateStatus(vng, VNG_STATUS_GAMING, info.gameId, vid, VNG_TIMEOUT);
	if (err != VNG_OK) {
       	 fprintf(stderr, "Err:VNG_UpdateStatus failure! returns %d\n", err);
       	 return -1;
   		 }
		
		return 1;
}

/*
打印出本用户创建的所有vn 信息
和本用户参加的所有vn 信息。
*/

void dumpVN_owned_joined(){

}


/*
反复加入一个vn  。

*/

 
/*

// Buddy status
//
typedef struct {
   VNGUserID             uid;
   VNGUserOnlineStatus   onlineStatus;
   GameId                gameId;
   VNId                  vnId;    
} VNGBuddyStatus;



// Update user’s own status
//
extern VNG_UpdateStatus(VNG                  *vng,
                        VNGUserOnlineStatus  status,                        
                        GameId               gameId,
                        VNId                 vnId,
                        VNGTimeout           timeout);


*/
 int UnregistVN(VNG * vng,VN *vn ){

	 VNGErrCode err;
//	 VNGGameInfo  info;
	// char  comments[100];
	  VNId vid;
	 // VNGUserInfo uinfo;
	  
       err = VN_GetVNId(vn, &vid);
       if (err) {
	        fprintf(stdout, "Err:VNG_GetVNId failure! returns %d\n", err);
	        return -1;
       }
	
	   err=VNG_UnregisterGame(vng,vid,VNG_TIMEOUT);
	    if (err != VNG_OK) {
       	 fprintf(stderr, "Err:VNG_UNRegisterGame failure! returns %d\n", err);
       	 return -1;
   		 }

	
	 return 1;
 }

int  searchVN_joinVN(VNG * vng,VN ** vn){
  //从服务器上获VN  信息

       VNGSearchCriteria criteria;
	VNGErrCode err;
	VNGGameStatus gameStatus[100];
	unsigned int numGameStatus=100;
	unsigned int i=0,j=0,nTimes=1,research,operate_code=0;
	VN *remoteVN=NULL;
	 TestData tdata;
	     DWORD rtid;
	HANDLE rthread ;
	
	criteria.gameId = 12;   
	criteria.maxLatency = 300;  // 300ms RTT
	criteria.cmpKeyword= VNG_CMP_DONTCARE;
	for ( i = 0; i < VNG_MAX_PREDICATES; i++) 
	   criteria.pred[i].cmp = VNG_CMP_DONTCARE;

	research=10;
     while(research--){
		err=VNG_SearchGames(vng, &criteria, gameStatus, &numGameStatus,0, VNG_TIMEOUT);
		if(err!=VNG_OK){
			fprintf(stderr, "Err:VNG_SearchGames failure! returns %d\n", err);
	       	// return -1;
			}
		if (numGameStatus >0) {
			//  fprintf(stdout,"No game session in server! \n");
			break;
			}
		Sleep(1000);
      	}

	  if (numGameStatus <=0) {
		       fprintf(stdout,"No game session in server! \n");
			return -1;
			}
  //打印出所有游戏信息。以便用户选择一个游戏加入。
  fprintf(stdout,"List all game session in server! \n");
	for( i=0;i<numGameStatus;i++){
		fprintf(stdout,"GameNumber=%d\n",i);
		VNGGameStatus * s=&gameStatus[i];
		dumpGameInfo(s);
		fprintf(stdout,"------------------------------------\n");
		}

 //要求用户输入想要操作的游戏的编号
	fprintf(stdout,"Please input GameNumber of the game that you want to operate:\n");
 	scanf("%d",&i);

//要求用户选择对游戏做什么操作
	fprintf(stdout,"--------------------------------------------\n");
	fprintf(stdout,"1.join this game.\n");
	fprintf(stdout,"2.leave this game.\n");
	fprintf(stdout,"3.destory this game.\n");
	fprintf(stdout,"4.invite a user to this game.\n");
	fprintf(stdout,"Please input operate number  :\n");
	scanf("%d",&operate_code);

	if(operate_code==1)
	{
		 //要求用户输入想加入游戏多少次。
		fprintf(stdout,"Please input you want to join into this game how many times?:\n");
	 	scanf("%d",&nTimes);
		for(j=0;j<nTimes;j++)
			{
			  //加入游戏vn 。
			  	VNGGameStatus * pGameStatus=&gameStatus[i];
			  	VNGGameInfo * pGameInfo=&(pGameStatus->gameInfo);
				char message[100],denyReason[100];
				int denyReasonLen=100;
				VNGUserId uid = VNG_MyUserId(vng);
			       VNGUserInfo uinfo;
				//VN *gameVN=NULL;
				   
			       err = VNG_GetUserInfo(vng, uid, &uinfo,VNG_TIMEOUT);
			       if (err != VNG_OK) {
			           fprintf(stderr, "VNG_GetUserInfo returns %d\n", err);
			           return -1;
			       }
				sprintf(message,"%s want to join.",uinfo.login);
				remoteVN=(VN*)malloc(sizeof(VN));
			  	err=VNG_JoinVN(vng, pGameInfo->vnId, message, remoteVN, denyReason,  denyReasonLen, VNG_TIMEOUT);
				if (err != VNG_OK) {
			           fprintf(stderr, "Error:VNG_JoinVN failure! returns %d\n", err);
			           return -1;
			       }
				if(remoteVN==NULL)
					{
						fprintf(stdout,"Error:Join game failure! denyInfo=%s.\n",denyReason);
					}
				Sleep(1000);
			}	
		 dumpVN(remoteVN);
		 *vn=remoteVN;
		//创建线程来接收数据。
		    tdata.vn   = remoteVN;
		    tdata.done = false;
		    tdata.attr = VN_DEFAULT_ATTR;
		    tdata.revent = CreateEvent(NULL, FALSE, FALSE, "RecvFini");
		    if (tdata.revent == NULL) 
		        cerr << "Failed to create RecvFini event" << endl;
			 rthread = CreateThread(NULL, 0, RecvThread2, &tdata, 0, &rtid);
		    if (rthread == NULL)
		        cerr << "Failed to create Send thread" << endl;
	}
	return 1;
}




int  searchVN_dumpVN(VNG * vng,VN ** vn){
  //从服务器上获VN  信息

       VNGSearchCriteria criteria;
	VNGErrCode err;
	VNGGameStatus gameStatus[100];
	unsigned int numGameStatus=100;
	unsigned int i=0,j=0,nTimes=1,research;
	VN *remoteVN=NULL;
	
	criteria.gameId = 12;   
	criteria.maxLatency = 300;  // 300ms RTT
	criteria.cmpKeyword= VNG_CMP_DONTCARE;
	for ( i = 0; i < VNG_MAX_PREDICATES; i++) 
	   criteria.pred[i].cmp = VNG_CMP_DONTCARE;

	research=10;
     while(research--){
		err=VNG_SearchGames(vng, &criteria, gameStatus, &numGameStatus,0, VNG_TIMEOUT);
		if(err!=VNG_OK){
			fprintf(stderr, "Err:VNG_SearchGames failure! returns %d\n", err);
	       	// return -1;
			}
		if (numGameStatus >0) {
			//  fprintf(stdout,"No game session in server! \n");
			break;
			}
		Sleep(1000);
      	}

	  if (numGameStatus <=0) {
		       fprintf(stdout,"No game session in server! \n");
			return -1;
			}
  //打印出所有游戏信息。以便用户选择一个游戏加入。
  fprintf(stdout,"List all game session in server! \n");
	for( i=0;i<numGameStatus;i++){
		fprintf(stdout,"GameNumber=%d\n",i);
		VNGGameStatus * s=&gameStatus[i];
		dumpGameInfo(s);
		fprintf(stdout,"------------------------------------\n");
		}

 //要求用户输入想加入游戏的编号
	fprintf(stdout,"Please input GameNumber of the game that you want to join into:\n");
 	scanf("%d",&i);

	 //要求用户输入想加入游戏多少次。
	fprintf(stdout,"Please input you want to join into this game how many times?:\n");
 	scanf("%d",&nTimes);
for(j=0;j<nTimes;j++)
	{
	  //加入游戏vn 。
	  	VNGGameStatus * pGameStatus=&gameStatus[i];
	  	VNGGameInfo * pGameInfo=&(pGameStatus->gameInfo);
		char message[100],denyReason[100];
		int denyReasonLen=100;
		VNGUserId uid = VNG_MyUserId(vng);
	       VNGUserInfo uinfo;
		//VN *gameVN=NULL;
		   
	       err = VNG_GetUserInfo(vng, uid, &uinfo,VNG_TIMEOUT);
	       if (err != VNG_OK) {
	           fprintf(stderr, "VNG_GetUserInfo returns %d\n", err);
	           return -1;
	       }
		sprintf(message,"%s want to join.",uinfo.login);
		remoteVN=(VN*)malloc(sizeof(VN));
	  	err=VNG_JoinVN(vng, pGameInfo->vnId, message, remoteVN, denyReason,  denyReasonLen, VNG_TIMEOUT);
		if (err != VNG_OK) {
	           fprintf(stderr, "Error:VNG_JoinVN failure! returns %d\n", err);
	           return -1;
	       }
		if(remoteVN==NULL)
			{
				fprintf(stdout,"Error:Join game failure! denyInfo=%s.\n",denyReason);
			}
		Sleep(1000);
	}	
	dumpVN(remoteVN);
	*vn=remoteVN;
	return 1;
}



int  searchVN_leaveVN(VNG * vng,VN ** vn){
  //从服务器上获VN  信息

       VNGSearchCriteria criteria;
	VNGErrCode err;
	VNGGameStatus gameStatus[100];
	unsigned int numGameStatus=100;
	unsigned int i=0,j=0,nTimes=1,research;
	VN *remoteVN=NULL;
	
	criteria.gameId = 12;   
	criteria.maxLatency = 300;  // 300ms RTT
	criteria.cmpKeyword= VNG_CMP_DONTCARE;
	for ( i = 0; i < VNG_MAX_PREDICATES; i++) 
	   criteria.pred[i].cmp = VNG_CMP_DONTCARE;

	research=10;
     while(research--){
		err=VNG_SearchGames(vng, &criteria, gameStatus, &numGameStatus,0, VNG_TIMEOUT);
		if(err!=VNG_OK){
			fprintf(stderr, "Err:VNG_SearchGames failure! returns %d\n", err);
	       	// return -1;
			}
		if (numGameStatus >0) {
			//  fprintf(stdout,"No game session in server! \n");
			break;
			}
		Sleep(1000);
      	}

	  if (numGameStatus <=0) {
		       fprintf(stdout,"No game session in server! \n");
			return -1;
			}
  //打印出所有游戏信息。以便用户选择一个游戏加入。
  fprintf(stdout,"List all game session in server! \n");
	for( i=0;i<numGameStatus;i++){
		fprintf(stdout,"GameNumber=%d\n",i);
		VNGGameStatus * s=&gameStatus[i];
		dumpGameInfo(s);
		fprintf(stdout,"------------------------------------\n");
		}

 //要求用户输入想加入游戏的编号
	fprintf(stdout,"Please input GameNumber of the game that you want to leave from:\n");
 	scanf("%d",&i);

	
	  //加入游戏vn 。
	  	VNGGameStatus * pGameStatus=&gameStatus[i];
	  	VNGGameInfo * pGameInfo=&(pGameStatus->gameInfo);
		char message[100],denyReason[100];
		int denyReasonLen=100;
		VNGUserId uid = VNG_MyUserId(vng);
	       VNGUserInfo uinfo;
		//VN *gameVN=NULL;
		   
	       err = VNG_GetUserInfo(vng, uid, &uinfo,VNG_TIMEOUT);
	       if (err != VNG_OK) {
	           fprintf(stderr, "VNG_GetUserInfo returns %d\n", err);
	           return -1;
	       }
		sprintf(message,"%s want to join.",uinfo.login);
		remoteVN=(VN*)malloc(sizeof(VN));
	  	err=VNG_JoinVN(vng, pGameInfo->vnId, message, remoteVN, denyReason,  denyReasonLen, VNG_TIMEOUT);
		if (err != VNG_OK) {
	           fprintf(stderr, "Error:VNG_JoinVN failure! returns %d\n", err);
	           return -1;
	       }
		if(remoteVN==NULL)
			{
				fprintf(stdout,"Error:Join game failure! denyInfo=%s.\n",denyReason);
			}
		
	dumpVN(remoteVN);
	*vn=remoteVN;
	return 1;
}

 int  getGameInfoByVN(VNG *vng,VN *vn){

	//vnid对应了一个VN ，也就是对应一个游戏session
	VNGErrCode err;
	 VNGGameStatus gameStatu;
	 char  comments[100];
	 size_t    commentsSize=100;



	  
       err = VN_GetVNId(vn, &(gameStatu.gameInfo.vnId));
       if (err != VNG_OK) {
	        fprintf(stdout, "Err:VNG_GetVNId failure! returns %d\n", err);
	        return -1;
       }
	//通过vnid 可以从数据库取回对应的游戏信息
	   err=VNG_GetGameStatus(vng,&gameStatu,1,VNG_TIMEOUT); 
	    if (err != VNG_OK) {
	        fprintf(stdout, "Err:VNG_GetGameStatus failure! returns %d\n", err);
	        return -1;
       }
	//这些游戏信息是游戏创建者在注册游戏到数据库的时候加入的,把他打印出来：
	dumpGameInfo(&gameStatu);

	//获取comments 信息，并打印出来；
	err=VNG_GetGameComments(vng, gameStatu.gameInfo.vnId, comments, &commentsSize, VNG_TIMEOUT);
	 if (err != VNG_OK) {
	        fprintf(stdout, "Err:VNG_GetGameComments failure! returns %d\n", err);
	        return -1;
       }
	 fprintf(stdout,"Game Comments:%s\n",comments);

	return 1;
 	}



DWORD WINAPI RecvThread2(LPVOID lpParam)
{
//	VNGErrCode err;
    int msg;
    size_t msglen = sizeof msg;

    TestData *tdata = (TestData *) lpParam;
    VN *vn = tdata->vn;
    Stats *s = &tdata->s;
    VNAttr attr = tdata->attr;
    int last_msg = 0;
    int next_msg = 0;
//    VNMsgHdr hdr;
	//while(true){
	//	Sleep(100);
	//fprintf(stdout,"RecvThread thread begin !\n");
	//}
    while (true) 
    	{

		ReceiveMessageFormVN(vn);

    	}
	 SetEvent(tdata->revent);
    return 0;
}




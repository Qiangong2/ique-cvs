//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "stdafx.h"
#include "vng.h"
#include "joinVN.h"
#include "budyList.h"
#include "../include/testcfg.h"
#include "ptpSendMsg.h"

using namespace std;
#include <iostream>
#include <time.h>
#include <set>
#include <hash_set>
using namespace stdext;







void Report(const char *testname, int seconds, const Stats s)
{
    cout << endl;
    cout << testname << " :" << endl;
    cout << " " << s.pkgsent << " packets sent" << endl;
    cout << " " << s.pkgrecv << " packets recv" << endl;
    cout << " " << s.pkgsent / seconds << " packets/sec" << endl;
    cout << " " << s.recverr << " failed to recv" << endl;
    cout << " " << s.senderr << " failed to send" << endl;
    cout << " " << s.buffull << " buffer full" << endl;
    cout << " " << s.nomem << " out of memory" << endl;
    if (!strncmp (testname, "Multi", 5)) {
        cout << " " << s.dropped << " dropped" << endl;
        cout << " " << s.duplicate << " duplicate" << endl;
        cout << " " << s.timeout << " timeout" << endl;
    }
    cout << " " << s.outofseq << " out of sequence" << endl;

    cout << "*** EXAMPLES loopback." << testname;
    if (s.pkgrecv < 1000 * test_duration ||             // at least 1000 packets/sec.
        s.recverr > 0 ||
        s.senderr > s.buffull ||        // do not consider buffer full errors
        s.buffull * 10 > s.pkgsent ||   // allow 10% buffer full
        s.nomem > 0 ||
        s.outofseq > 0) {
       cout << " TEST FAILED" << endl;
    } else
       cout << " TEST PASSED" << endl;

    cout << endl;
}



void LoopbackSingleThread(VNG *vng,VN *vn,const char *testname, int seconds, VNAttr attr)
{
   // VNGErrCode err;
    
    //char vngbuf[16*1024];

    Stats s;
    VNMsgHdr hdr;


    clock_t start = clock();
    clock_t finish = start + seconds * CLOCKS_PER_SEC;

	//获取vn中的其他member信息，并打印出来。
	dumpVN(vn);

	//获取vn的创建者。
	 VNMember owner = VN_Owner(vn);
    while (clock() < finish) {
        VNGErrCode err;
        int msg;
        size_t msglen;
        for (int i = 0; i < BATCH_SIZE; i++) {
			//发送信息给vn的创建者。
            err = VN_SendMsg(vn, owner, 2034, &i, sizeof(i), attr,40);
            if (err == VNGERR_BUF_FULL) {
                s.buffull++;
            }
            else if (err == VNGERR_NOMEM) {
                s.nomem++;
            }
            if (err != VNG_OK)  {
                s.senderr++;
            }
            else {
                s.pkgsent++;
                msglen = sizeof msg;
				//接收从vn创建者发来的信息。
				//注意，我们在vn创建者端作如下处理，开一个线程用于接收消息，每收到一个消息后就把他发回给法消息者。
                err = VN_RecvMsg(vn, owner, VN_SERVICE_TAG_ANY, 
                                     &msg, &msglen, &hdr, 100);
                if (err != VNG_OK)
                    s.recverr++;
                else {
                    s.pkgrecv++;
                    // verify data are in sequence
                    if (i != msg)
                        s.outofseq++;
                }
            }
        }
    }

    Report(testname, seconds, s);

}





DWORD WINAPI RecvThread(LPVOID lpParam)
{
    TestData *tdata = (TestData *) lpParam;
    VN *vn = tdata->vn;
    Stats *s = &tdata->s;
    int last_msg = 0;
    int next_msg = 0;
    hash_set<int>  oos;
    bool wasOos  = false; // was out of sequence
    int outOos = 0;
    VNMsgHdr hdr;

    while (true) {
        VNGErrCode err;
        int msg;
        size_t msglen = sizeof msg;
        err = VN_RecvMsg(vn, VN_MEMBER_ANY, VN_SERVICE_TAG_ANY, 
                             &msg, &msglen, &hdr, 40 /* 40ms wait */);
        if (err == VNGERR_TIMEOUT) {
            if (tdata->done) break;
            s->timeout++;
            continue;
        }
        if (err != VNG_OK)
            s->recverr++;
        else {
            s->pkgrecv++;
            // record data out of sequence, duplicates, and dropped
            if (msg != next_msg) {
                if (!s->outofseq++) {
                    cout << "\nFirst few out of sequence:\n";
                }
                if (outOos < 10) {
                    ++outOos;
                    if (!wasOos) {
                        wasOos = true;
                        cout << last_msg << " " << msg;
                    } else
                        cout << " " << msg;
                }
                if (next_msg < msg) {
                    for (int i = next_msg;  i < msg && (i-next_msg) < 500;  ++i) {
                        if (oos.find (i) == oos.end()) {
                            oos.insert (i);
                        }
                        else {
                            cout << "\n      oos next_msg < msg and already in oos: line: "
                                 << __LINE__ << endl << endl;
                            exit (1); // Should never get here
                        }
                    }
                }
                else if (oos.find (msg) != oos.end()) {
                    oos.erase (msg);
                }
                else {
                    s->duplicate++;
                }
            }
            else if (wasOos) {
                wasOos = false;
                cout << " " << msg << "\n";
            }
            last_msg = msg;
            if (msg >= next_msg)
                next_msg = msg +1;
        }
    }
    if ((s->dropped = (int) oos.size())) {
        set <int> dropped (oos.begin(), oos.end());
        int num = 0;
        int max = 20;
        cout << "\nFirst " << max << " dropped msgs:\n";
        set<int>::iterator it;
        for (it = dropped.begin(); it != dropped.end();  ++it) {
            if (++num > max)
                break;
            cout << *it << " "; 
        }
        if (s->dropped > num) {
            cout << "...";
        }
        cout << endl;
    }
    SetEvent(tdata->revent);
    return 0;
}


DWORD WINAPI SendThread(LPVOID lpParam)
{
    TestData *tdata = (TestData *) lpParam;
    VN *vn = tdata->vn;
    VNAttr attr = tdata->attr;
    Stats *s = &tdata->s;

    while (!tdata->done) {
        VNGErrCode err;
        int msg = s->pkgsent;

        err = VN_SendMsg(vn, VN_MEMBER_ANY, VN_SERVICE_TAG_ANY, 
                             &msg, sizeof(msg), attr,40);
        if (err != VNG_OK) {
            s->senderr++;
            if (err == VNGERR_BUF_FULL) {
                // Force context-switch to run the recvThread
                Sleep (5);
                s->buffull++;
            }
            else if (err == VNGERR_NOMEM) {
                Sleep (5);
                s->nomem++;
            }
        }
        else {
            s->pkgsent++;
        }
    }
    SetEvent(tdata->sevent);
    return 0;
}



void LoopbackMultiThread(VNG *localvng,const char *testname, int duration, VNAttr attr)
{
   // VNG localvng;
    //char vngbuf[16*1024];
    VNGErrCode  err;
    VN localvn;


	
	// err= VNG_Init(localvng, vngbuf, sizeof(vngbuf));
	//fprintf(stdout,"VNG_Init err=%d\n",err);

//	 err = VNG_Login(localvng, VNGS_HOST, VNGS_PORT,
  //              "shengsen", TESTER01PW,
    //            VNG_USER_LOGIN, VNG_TIMEOUT);

  //  if (err != VNG_OK) {
    //    fprintf(stderr, "VNG_Login returns %d\n", err);
      //  return ;
//	}else{
//		fprintf(stderr, "VNG_Login success! \n");
//	}

	err=VNG_NewVN(localvng, &localvn,VN_DEFAULT_CLASS, VN_DOMAIN_ADHOC, VN_DEFAULT_POLICY);
	fprintf(stdout,"VNG_NewVN err=%d\n",err);
	
    TestData tdata;
    tdata.vn   = &localvn;
    tdata.done = false;
    tdata.attr = attr;

    DWORD rtid, stid;

    tdata.revent = CreateEvent(NULL, FALSE, FALSE, "RecvFini");
    if (tdata.revent == NULL) 
        cerr << "Failed to create RecvFini event" << endl;
    
    tdata.sevent = CreateEvent(NULL, FALSE, FALSE, "SendFini");
    if (tdata.sevent == NULL)
        cerr << "Failed to create SendFini event" << endl;  

    HANDLE rthread = CreateThread(NULL, 0, RecvThread, &tdata, 0, &rtid);
    if (rthread == NULL)
        cerr << "Failed to create Send thread" << endl;

    HANDLE sthread = CreateThread(NULL, 0, SendThread, &tdata, 0, &stid);
    if (rthread == NULL)
        cerr << "Failed to create Send thread" << endl;
        
    Sleep(duration*1000);
    tdata.done = true;

    WaitForSingleObject(tdata.sevent, 300000);
    WaitForSingleObject(tdata.revent, 300000);
    CloseHandle(tdata.sevent);
    CloseHandle(tdata.revent);

    CloseHandle(rthread);
    CloseHandle(sthread);

    Report(testname, duration, tdata.s);
//	VNG_Logout(localvng, VNG_TIMEOUT);
    VNG_DeleteVN(localvng, &localvn);
 //   VNG_Fini(localvng);
}

/*
send test message to a user in a VN! 
*/
void SendMessageToPeer(VN* pstVN){
	 VNMember *pstMember = NULL;
    uint32_t uMemberNum;
	VNGErrCode stErrCode;
    char * pstMsg="test";
    int iMsgLen=4;
    uint32_t i=0;
	VNGUserId uid;
    
    uMemberNum = VN_NumMembers(pstVN);
    
    pstMember =(VNMember*) malloc(uMemberNum*sizeof(VNMember));
    
    uMemberNum = VN_GetMembers (pstVN,pstMember,uMemberNum);

	for(i=0;i<uMemberNum;i++)
    {
	    	uid=VN_MemberUserId(pstVN,pstMember[i],5000);
	    	fprintf(stdout,"  %d :   VNGUserId  :0x%08x.%08x\n", i,(int)(uid >> 32), (int)uid);  
    }
	fprintf(stdout," Please choose a peer to send message : ");
	scanf("%d",&i);
	
	

			
	    	    stErrCode = VN_SendMsg (pstVN,
	                            pstMember[i], 
	                            VN_SERVICE_TAG_ANY,
	                            pstMsg,
	                            iMsgLen,
	                            VN_ATTR_UNRELIABLE,
	                            5000);                   
            	
	    	

}


void SendMessageToVNhost(){


}



/*process service*/
void ReceiveMessageFormVN(VN *pstVN)
{
    char *pstrMsg = NULL;
    VNMsgHdr *pstMsgHdr = NULL;
    size_t uMsgLen = 4;
    VNGUserId stUid;

    VNServiceTypeSet *pstServiceType = NULL;
	
        if(VNG_OK == VN_Select ( pstVN, VN_SERVICE_TYPE_ANY, pstServiceType, VN_MEMBER_ALL, VNG_NOWAIT))
        {
        	//pstServiceType为null ，表示没有发现任何通道有数据到达。
        	 if(pstServiceType == NULL||pstVN == NULL)
	    {
	        return;	
	    }
            pstrMsg =(char*) malloc(4);
            memset(pstrMsg,0,4);
            pstMsgHdr =(VNMsgHdr*) malloc(sizeof(VNMsgHdr));
            if(VNG_OK==VN_RecvMsg ( pstVN, VN_MEMBER_ALL, VN_SERVICE_TAG_ANY,pstrMsg,&uMsgLen, pstMsgHdr,VNG_NOWAIT))
            {
	        stUid=VN_MemberUserId(pstVN,pstMsgHdr->sender,VNG_WAIT);	    
	 
		fprintf(stdout,"Message received: %s \r\n",pstrMsg);
		fprintf(stdout, "     VNGUserId  :0x%08x.%08x\n", (int)(stUid >> 32), (int)stUid);
             }          
     	 }

}




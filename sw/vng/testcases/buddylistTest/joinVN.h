#ifndef __joinvn_ 
#define __joinvn_ 

void report(const char *testname, int pass);
void dumpVN(VN *vn);
void dumpEvents(VNG *vng,bool flag);
void dumpErrorInfo(VNGErrCode errCode);
int testErrorHandling();
int registVN_updateStatus(VNG * vng,VN *vn );
int  searchVN_joinVN(VNG * vng,VN ** vn);
int  searchVN_dumpVN(VNG * vng,VN ** vn);
int  searchVN_leaveVN(VNG * vng,VN ** vn);

#endif
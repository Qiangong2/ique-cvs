//
//               Copyright (C) 2005, iQue .
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of iQue,They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of iQue.
//
//
#include "stdafx.h"
#include "vng.h"
#include "joinVN.h"
#include "../include/testcfg.h"
#include "budyList.h"
#include "ptpSendMsg.h"

//#include <math.h>
//#include "../pong/data.h"
//#include "gba/scrpc.h"

using namespace std;
#include <iostream>
#include <time.h>
#include <set>
#include <hash_set>
using namespace stdext;


/*
void *SM_Malloc(size_t size)
{
    static int sm_ptr;
    void *ret;
    if (!sm_ptr)
        sm_ptr = 0x08000000+SC_RPC_RAM_OFFSET+0x40000;

    ret = (void*)sm_ptr;
    sm_ptr += size;
    if (sm_ptr & 3)
        sm_ptr = (sm_ptr+0x4)&(0-4);
    return ret;
}
*/

VNG * loginServer(char * loginName,char * loginPW){
	
    VNGErrCode err;
    VNG *vng;
    char vngbuf[16*1024];

	vng=(VNG*)malloc(sizeof(VNG));
    VNG_Init(vng, vngbuf, sizeof(vngbuf));

    err = VNG_Login(vng, VNGS_HOST, VNGS_PORT,
                loginName, loginPW,
                VNG_USER_LOGIN, VNG_TIMEOUT);

    if (err != VNG_OK) {
   	fprintf(stderr, "VNG_Login returns %d\n", err);
	fprintf(stdout, "Entering fini VNG\n");
	VNG_Fini(vng);
	free(vng);
    return NULL;
    }

   return vng;
}

typedef enum{
	MANUAL=0,		
	AUTORUN
}MENU_ITEM_STATUE;

typedef struct {
	int     itemNum;
	char  itemStr[128];
	MENU_ITEM_STATUE itemStatu;
	
}MENU_ITEM;

MENU_ITEM BuddyList_menu[]={
{0," Login using a new account.    ",MANUAL},
{1," Show account info.            ",MANUAL},
{2," List buddyList.               ",MANUAL},
{3," Delete a buddy by loginName.  ",MANUAL},
{4," Add a buddy by loginName.     ",MANUAL},
{5," Block a buddy.                ",MANUAL},
{6," Unblock a buddy.              ",MANUAL},
{7," Tracking the buddy status ", MANUAL},
{8," Untracking the buddy status ", MANUAL},
{9," accept all buddy invite                       ",MANUAL},
{10," reject all buddy invite                       ",MANUAL},
{11," Logout.                       ",MANUAL},
{12," login 1000 times test! ", MANUAL},

{31,"Create and Register a VN.                ",MANUAL},
{32,"Search and Join a VN.                ",MANUAL},
{33,"Display all VN information In server .",MANUAL},
{34,"Leave a VN    ", MANUAL},
{33,"Send msg to VN owner test.        ",MANUAL},
{20," error code test.                       ",MANUAL},
{100,"Exit.                        ",MANUAL},
{-1,"",MANUAL}
};

void dumpGameInfo(VNGGameStatus *s)
{
    VNGGameInfo *gi = &s->gameInfo;
    
    fprintf(stdout, "num players %d\n", s->numPlayers);
    fprintf(stdout, "game status %d\n", s->gameStatus);
    fprintf(stdout, "vnId 0x%08x.%08x\n", gi->vnId.devId, gi->vnId.vnetId);
    fprintf(stdout, "owner 0x%08x.%08x\n", (int) gi->owner >> 32, (int) gi->owner & ~0);
    fprintf(stdout, "gameId %d\n", gi->gameId);
    fprintf(stdout, "titleId %d\n", gi->titleId);
    fprintf(stdout, "netZone %d\n", gi->netZone);
    fprintf(stdout, "maxLatency %d\n", gi->maxLatency);
    fprintf(stdout, "accessControl %d\n", gi->accessControl);
    fprintf(stdout, "totalSlots %d\n", gi->totalSlots);
    fprintf(stdout, "buddySlots %d\n", gi->buddySlots);
    fprintf(stdout, "keyword %s\n", gi->keyword);
    fprintf(stdout, "attrCount %d\n", gi->attrCount);
    for (int i = 0; i < gi->attrCount; i++) {
        fprintf(stdout, "attr[%d] is %d\n", i, gi->gameAttr[i]);
    }
}
int dumpMenu(MENU_ITEM * pMenuBuf,char * menuName){
	MENU_ITEM * p;
	int getItem=-1;
	
	fprintf(stdout,"\n\r%s\n",menuName);
	p=pMenuBuf;
	while(p->itemNum>=0){
			fprintf(stdout,"  %d : %s   ItemStatu=%d\n", p->itemNum,p->itemStr,(int)p->itemStatu);
			p++;
		}
	fprintf(stdout,"Please choose a item:\n");
	scanf("%d",&getItem);
	return getItem;
}


void dumpUserInfo(VNGUserInfo uinfo){
	fprintf(stdout, " -------------------------------------------\n");
	fprintf(stdout, "  Show User information:\n");
	fprintf(stdout, "     VNGUserId  :0x%08x.%08x\n", (int)(uinfo.uid >> 32), (int)uinfo.uid);
    fprintf(stdout, "     login      :%s\n", uinfo.login);
    fprintf(stdout, "     nickname   :%s\n", uinfo.nickname);
	fprintf(stdout, " -------------------------------------------\n");
}

typedef struct{
	VNGUserOnlineStatus  statusNum;
	char   statusStr[30];
} StatusName;

StatusName nameBuf[]={
   {VNG_STATUS_ONLINE,"VNG_STATUS_ONLINE"},
   {VNG_STATUS_OFFLINE,"VNG_STATUS_OFFLINE"},
   {VNG_STATUS_AWAY,"VNG_STATUS_AWAY"},
   {VNG_STATUS_GAMING,"VNG_STATUS_GAMING"},
   {VNG_STATUS_INVITE_SENT,"VNG_STATUS_INVITE_SENT"},
   {VNG_STATUS_INVITE_RECEIVED,"VNG_STATUS_INVITE_RECEIVED"},
   {VNG_STATUS_BLOCKED_FRIEND,"VNG_STATUS_BLOCKED_FRIEND"},
   {VNG_STATUS_BLOCKED_STRANGER,"VNG_STATUS_BLOCKED_STRANGER"}
};

void dumpBuddyStatus(VNGBuddyStatus *bs){
	fprintf(stdout, "     VNGUserId      :0x%08x.%08x\n", (int)(bs->uid >> 32), (int)bs->uid);
	fprintf(stdout, "     onlineStatues  :%s\n", nameBuf[bs->onlineStatus].statusStr);
	fprintf(stdout, "	gameId         :0x%08x\n", (int)bs->gameId);
	fprintf(stdout, "	vnId           :0x%08x.%08x\n", (int)(bs->vnId.devId),(int)(bs->vnId.vnetId));
}

void dumpBuddyPrim(VNGUserInfo *pBuddyPrimeInfo){
    VNGUserId uid = pBuddyPrimeInfo->uid;
    fprintf(stdout, "    uid      :0x%08x.%08x\n", (int)(uid >> 32), (int)uid);
    fprintf(stdout, "    login    :%s\n", pBuddyPrimeInfo->login);
    fprintf(stdout, "    nickname :%s\n", pBuddyPrimeInfo->nickname);
}







void inputStr(char * buf, char * prompt){
	char inputBuf[128];
	//要求用户输入loginUser/password
	fprintf(stdout,"%s",prompt);
	scanf("%s",inputBuf);
	strcpy(buf,inputBuf);
}

bool UIAcceptUser(){
	char answerStr[10];
	while(1){
	fprintf(stdout,"Accept this user?(yes/no)");
	scanf("%s",answerStr);
	if(strcmp(answerStr,"yes")==0)return true;
	if(strcmp(answerStr,"no")==0)return false;
	}
}




int joinRequestEventProcess(VNId vid ,VNG * vng){
	VNGErrCode err;
	VN  *vn;
	VNGGameStatus gameStatu;
	VNGUserInfo userInfo;
        uint64_t reqId;

	char joinReson[100];
	size_t joinResonLen=100;
	
	//收到一个join request 事件的处理
     fprintf(stdout,"Receive a JoinRequest to game");
     //获取游戏信息
	//通过vnid 可以从数据库取回对应的游戏信息
	gameStatu.gameInfo.vnId.devId=vid.devId;
	 gameStatu.gameInfo.vnId.vnetId=vid.vnetId;
	   err=VNG_GetGameStatus(vng,&gameStatu,1,VNG_TIMEOUT); 
	    if (err != VNG_OK) {
	        fprintf(stdout, "Err:VNG_GetGameStatus failure! returns %d\n", err);
	        return -1;
       }

	 //获取请求发出者信息
	 err=VNG_GetJoinRequest(vng,vid,&vn,&reqId,&userInfo,joinReson,joinResonLen,VNG_TIMEOUT);
	 if (err != VNG_OK) {
	        fprintf(stdout, "Err:VN_GetJoinRequest  failure! returns %d\n", err);
	        return -1;
       }

	//打印获取的 信息
	fprintf(stdout,"----------------Game info:--------------\n");
	dumpGameInfo(&gameStatu);
	fprintf(stdout,"----------------user who want to join:----------\n");
	dumpUserInfo(userInfo);
	 
	 //要求用户决定是否允许对方加入游戏
	// Accept this user?
        char reason[100];
        if (UIAcceptUser()) 
        	{
	            	err =VNG_AcceptJoinRequest(vng, reqId);
			 if (err != VNG_OK) {
			        fprintf(stdout, "Err:VNG_AcceptJoinRequest  failure! returns %d\n", err);
			        return -1;
	       	}
        	}
        else
        	{
	        	fprintf(stdout,"Please input Reject reason:\n");
			scanf("%s",reason);
	            	err=VNG_RejectJoinRequest(vng, reqId, reason);
			 if (err != VNG_OK) {
			        fprintf(stdout, "Err:VNG_RejectJoinRequest  failure! returns %d\n", err);
			        return -1;
	       	}
        	}
	return 1;
}




//create a new thread to blocking to get events
DWORD WINAPI buddyListEventPro(LPVOID pParam){

	VNGErrCode err;
	VNG*  vng;
	VNGBuddyStatus  buddyStatus;
	
       vng=(VNG*)pParam;
       fprintf(stdout,"event processing thread begin!\n");
	// Wait for all events
	    while (1) {
	        VNGEvent event;
			 fprintf(stdout,"-");
	        err = VNG_GetEvent(vng, &event, VNG_WAIT);
	        if (err == VNG_OK) {
				
	           fprintf(stdout, "Get event %d \r\n", event.eid);

		    if (event.eid== VNG_EVENT_BUDDY_STATUS) {
				//获取给我发送事件的buddy 的用户id 。
				VNGUserId uid=event.evt.buddyStatus.uid;
				//把该buddy 的信息打出来。
				 VNGBuddyStatus * d= &buddyStatus;
				 d->uid=uid;
				 err=VNG_GetBuddyStatus(vng, d, 1, VNG_TIMEOUT);
				    if (err != VNG_OK) {
				        fprintf(stdout, "Get buddy list  detail info failure!\n");
				        return -1;
				    }
				//打印一个buddy的状态信息 
				dumpBuddyStatus(d);
		    	}
	           if (event.eid == VNG_EVENT_PEER_STATUS) {
	               VNId vid;
	               err= VN_GetVNId(event.evt.peerStatus.vn, &vid);
				   if(err!=VNG_OK)
				   	fprintf(stdout,"Error:get vnid error !\n");
				   else 
	               		fprintf(stdout, " --VNid is : 0x%08x.%08x  member in VN: %d", vid.devId, vid.vnetId, event.evt.peerStatus.memb);

			
	           }    
	          

		   if (event.eid== VNG_EVENT_JOIN_REQUEST) {
				fprintf(stdout,"owner status event!\n");
				joinRequestEventProcess(event.evt.joinRequest.vnId,vng);
		    	}
		    if (event.eid== VNG_EVENT_RECV_INVITE) {
				fprintf(stdout,"VNG_EVENT_JOIN_REQUEST event!\n");

		    	}
		      if (event.eid== VNG_EVENT_LOGOUT) {

				fprintf(stdout,"VNG_EVENT_LOGOUT event!\n");
		    	}
			
	           fprintf(stdout, "\n");
	            
	        } else if (err == VNGERR_NOWAIT || err == VNGERR_TIMEOUT)
	        	{
	        	    fprintf(stdout,"get event err! err=%d .\n",err);
	        	}
	    }
 
	return 1;
}


int login( VNG *vng){
	
    VNGErrCode err;
    char loginName[30],passWD[30];
	
    inputStr(loginName,"Please input LoginName:\n");
    inputStr(passWD,"Please input passWD:\n");
	
    err = VNG_Login(vng, VNGS_HOST, VNGS_PORT,
                loginName, passWD,
                VNG_USER_LOGIN, VNG_TIMEOUT);

    if (err != VNG_OK) {
        fprintf(stderr, "VNG_Login returns %d\n", err);
        return -1;
	}else{
		fprintf(stderr, "VNG_Login success! \n");
	}

    VNGUserId uid = VNG_MyUserId(vng);
    VNGUserInfo uinfo;
    err = VNG_GetUserInfo(vng, uid, &uinfo,VNG_TIMEOUT);
    if (err != VNG_OK) {
        fprintf(stderr, "VNG_GetUserInfo returns %d\n", err);
        return -1;
    }

	//打印出用户信息
	dumpUserInfo( uinfo);

	//更新自己的状态
	VNId vnid;
	vnid.devId=0;
	vnid.vnetId=0;
	
	err=VNG_UpdateStatus(vng, VNG_STATUS_ONLINE, 0, vnid, VNG_TIMEOUT);
	 if (err != VNG_OK) {
        fprintf(stderr, "Error: update status of  VNG_STATUS_ONLINE returns %d\n", err);
       // return -1;
    	}
	 
	//开一个线程监视event
	HANDLE myThread;
	DWORD threadBuddyListEventID;
	myThread=CreateThread(NULL,0,buddyListEventPro,vng,0,(LPDWORD)&threadBuddyListEventID);
	
	return 1;
}


int  logOut(VNG * vng){

	VNGErrCode err;

	err=VNG_Logout(vng, VNG_TIMEOUT);
	if(err!=VNG_OK){
		fprintf(stdout,"Error:logout error!");
		return -1;
		}
	return 1;
}


int default_login( VNG *vng,int * failed_count){
	
    VNGErrCode err;

    err = VNG_Login(vng, VNGS_HOST, VNGS_PORT,
               TESTER01 ,TESTER01PW ,
                VNG_USER_LOGIN, VNG_TIMEOUT);

    if (err != VNG_OK) {
        fprintf(stderr, "VNG_Login returns %d\n", err);
	 (*failed_count)++;
        return -1;
	}else{
		fprintf(stderr, "VNG_Login success! \n");
	}

    VNGUserId uid = VNG_MyUserId(vng);
    VNGUserInfo uinfo;
    err = VNG_GetUserInfo(vng, uid, &uinfo,VNG_TIMEOUT);
    if (err != VNG_OK) {
        fprintf(stderr, "VNG_GetUserInfo returns %d\n", err);
		(*failed_count)++;
        return -1;
    }

	//打印出用户信息
	dumpUserInfo( uinfo);
	 logOut(vng);
	 //Sleep(1000);
	return 1;
}

int showUserInfo(VNG * vng){
	VNGErrCode err;
	VNGUserId uid = VNG_MyUserId(vng);
    	VNGUserInfo uinfo;
    	err = VNG_GetUserInfo(vng, uid, &uinfo,VNG_TIMEOUT);
   	 if (err != VNG_OK) {
       	 fprintf(stderr, "VNG_GetUserInfo returns %d\n", err);
       	 return -1;
   		 }

	//打印出用户信息
	dumpUserInfo( uinfo);
	return 1;
}

/////////////////////////////////////////////////////////////////////////////////
//关于buddy list 的一些测试函数
////////////////////////////////////////////////////////////////////////////////

int rejectAllInvite(VNG * vng){
 //获取并打印该用户的buddylist内容 。
    VNGErrCode err;
    VNGUserInfo buddyinfo[100];
    VNGBuddyStatus  buddyStatus;
    uint32_t nbuddies = 100;

    fprintf(stderr, "VNG_GetBuddyList :  \r\n");
    err = VNG_GetBuddyList(vng, 0, buddyinfo, &nbuddies, VNG_TIMEOUT);
    if (err != VNG_OK) {
        fprintf(stdout, "Get buddy list failure!\n");
        return -1;
    }

    if(nbuddies==0)
		fprintf(stdout,"I have not buddy!\n");
    else fprintf(stdout,"List all my buddies now:\n");

   
	
    for (int i = 0; i < (int) nbuddies; i++) {
		VNGUserInfo *p = &buddyinfo[i];
		 VNGBuddyStatus * d= &buddyStatus;
		 d->uid=p->uid;
		fprintf(stdout,"buddy[%d]:\n",i);
		//打印一个buddy的基本信息
		
		dumpBuddyPrim(p);
		 err=VNG_GetBuddyStatus(vng, d, 1, VNG_TIMEOUT);
		    if (err != VNG_OK) {
		        fprintf(stdout, "Get buddy list  detail info failure!\n");
		        return -1;
		    }
			fprintf(stdout,"-----------old status info-----------\n");
		//打印一个buddy的状态信息 
		dumpBuddyStatus(d);
		//如果收到了一个邀请，那就拒绝他
		if(d->onlineStatus==VNG_STATUS_INVITE_RECEIVED){
			err=VNG_RejectUser(vng, p->uid, VNG_TIMEOUT);
			  if (err != VNG_OK) {
				        fprintf(stdout, "VNG_AcceptUser failure!\n");
				        return -1;
				    }
			      fprintf(stdout,"-----------new status info-----------\n");
			      err=VNG_GetBuddyStatus(vng, d, 1, VNG_TIMEOUT);
			    if (err != VNG_OK) {
			        fprintf(stdout, "Get buddy list  detail info failure!\n");
			        return -1;
			    }
				dumpBuddyStatus(d);
		}
    }
   
	return 1;


}

int acceptAllInvite(VNG *vng ){
 //获取并打印该用户的buddylist内容 。
    VNGErrCode err;
    VNGUserInfo buddyinfo[100];
    VNGBuddyStatus  buddyStatus;
    uint32_t nbuddies = 100;

    fprintf(stderr, "VNG_GetBuddyList :  \r\n");
    err = VNG_GetBuddyList(vng, 0, buddyinfo, &nbuddies, VNG_TIMEOUT);
    if (err != VNG_OK) {
        fprintf(stdout, "Get buddy list failure!\n");
        return -1;
    }

    if(nbuddies==0)
		fprintf(stdout,"I have not buddy!\n");
    else fprintf(stdout,"List all my buddies now:\n");

   
	
    for (int i = 0; i < (int) nbuddies; i++) {
		VNGUserInfo *p = &buddyinfo[i];
		 VNGBuddyStatus * d= &buddyStatus;
		 d->uid=p->uid;
		fprintf(stdout,"buddy[%d]:\n",i);
		//打印一个buddy的基本信息
		
		dumpBuddyPrim(p);
		 err=VNG_GetBuddyStatus(vng, d, 1, VNG_TIMEOUT);
		    if (err != VNG_OK) {
		        fprintf(stdout, "Get buddy list  detail info failure!\n");
		        return -1;
		    }
			fprintf(stdout,"-----------old status info-----------\n");
		//打印一个buddy的状态信息 
		dumpBuddyStatus(d);
		//如果收到了一个邀请，那就接受他
		if(d->onlineStatus==VNG_STATUS_INVITE_RECEIVED){
			err=VNG_AcceptUser(vng, p->uid, VNG_TIMEOUT);
			  if (err != VNG_OK) {
				        fprintf(stdout, "VNG_AcceptUser failure!\n");
				        return -1;
				    }
			      fprintf(stdout,"-----------new status info-----------\n");
			      err=VNG_GetBuddyStatus(vng, d, 1, VNG_TIMEOUT);
			    if (err != VNG_OK) {
			        fprintf(stdout, "Get buddy list  detail info failure!\n");
			        return -1;
			    }
				dumpBuddyStatus(d);
		}
    }
   
	return 1;


}


int listBuddyList(VNG * vng){
  //获取并打印该用户的buddylist内容 。
    VNGErrCode err;
    VNGUserInfo buddyinfo[100];
    VNGBuddyStatus  buddyStatus;
    uint32_t nbuddies = 100;

    fprintf(stderr, "VNG_GetBuddyList :  \r\n");
    err = VNG_GetBuddyList(vng, 0, buddyinfo, &nbuddies, VNG_TIMEOUT);
    if (err != VNG_OK) {
        fprintf(stdout, "Get buddy list failure!\n");
        return -1;
    }

    if(nbuddies==0)
		fprintf(stdout,"I have not buddy!\n");
    else fprintf(stdout,"List all my buddies now:\n");

   
	
    for (int i = 0; i < (int) nbuddies; i++) {
		VNGUserInfo *p = &buddyinfo[i];
		 VNGBuddyStatus * d= &buddyStatus;
		 d->uid=p->uid;
		fprintf(stdout,"buddy[%d]:\n",i);
		//打印一个buddy的基本信息
		dumpBuddyPrim(p);
		 err=VNG_GetBuddyStatus(vng, d, 1, VNG_TIMEOUT);
		    if (err != VNG_OK) {
		        fprintf(stdout, "Get buddy list  detail info failure!\n");
		        return -1;
		    }
		//打印一个buddy的状态信息 
		dumpBuddyStatus(d);
    }
   
	return 1;
}

int deleteBuddyByName(VNG * vng){

	    VNGErrCode err;
	    VNGUserInfo buddyinfo[100];
	    uint32_t nbuddies = 100;
		char tempName[30];

	    err = VNG_GetBuddyList(vng, 0, buddyinfo, &nbuddies, VNG_TIMEOUT);
	    if (err != VNG_OK) {
	        fprintf(stdout, "Get buddy list failure!\n");
	        return -1;
	    }
	//如果发现buddy list 里面有好友，把一个好友remove掉 
	
	if(nbuddies!=0)
		{
			fprintf(stdout,"Please input the loginName of the buddy you want to remove:\n");
			scanf("%s",&tempName);

			 for (int i = 0; i < (int) nbuddies; i++) 
			 {
				VNGUserInfo *p = &buddyinfo[i];
				if(strcmp(p->login,tempName)==0)
					{
						err=VNG_RemoveUser(vng, p->uid, VNG_TIMEOUT);
						 if (err != VNG_OK) {
						 	fprintf(stdout,"remove a Buddy from buddylist err! \n");
							dumpErrorInfo(err);
						        return -1;
						    }
					}
	    		  }
		}
	else{
		fprintf(stdout,"buddy list is null!\n");
		return 0;
		}
		
	return 1; 

}

int addBuddyByName(VNG * vng){

 	//添加好友。
 	char tempName[30];
 	VNGErrCode err;

	fprintf(stdout,"Please input buddy's loginName: \r\n");
 	scanf("%s",tempName);
		
	 err=VNG_InviteUser(vng,tempName, VNG_TIMEOUT);
		if (err != VNG_OK) {
	 	fprintf(stdout,"readd the removed  Buddy  err! \n");
		dumpErrorInfo(err);
	        return -1;
	    }
		
	return 1;


}

int  blockBuddyByName(VNG * vng){
  //输入名字，把一个好友加入黑名单
   		    VNGErrCode err;
	    VNGUserInfo buddyinfo[100];
	    uint32_t nbuddies = 100;
		char tempName[30];

	    err = VNG_GetBuddyList(vng, 0, buddyinfo, &nbuddies, VNG_TIMEOUT);
	    if (err != VNG_OK) {
	        fprintf(stdout, "Get buddy list failure!\n");
	        return -1;
	    }
	//如果发现buddy list 里面有好友，把一个好友block 掉 
	
	if(nbuddies!=0)
		{
			//打印好友列表
			int i=0;
			for( i=0;i<(int)nbuddies;i++){
				fprintf(stdout,"%d : %s\n",i,buddyinfo[i].login);
				}
			//要求输入名称
			fprintf(stdout,"Please input the loginName of the buddy you want to block:\n");
			scanf("%s",&tempName);

			 for ( i = 0; i < (int) nbuddies; i++) 
			 {
				VNGUserInfo *p = &buddyinfo[i];
				if(strcmp(p->login,tempName)==0)
					{
						err=VNG_BlockUser(vng, p->uid, VNG_TIMEOUT);
						 if (err != VNG_OK) {
						 	fprintf(stdout,"block a Buddy in buddylist err! \n");
							dumpErrorInfo(err);
						        return -1;
						    }
					}
	    		  }
		}
	else{
		fprintf(stdout,"buddy list is null!\n");
		return 0;
		}
		
	return 1; 
}


int  unblockBuddyByName(VNG * vng){
  //输入名字，把一个好友退出黑名单
   		    VNGErrCode err;
	    VNGUserInfo buddyinfo[100];
	    uint32_t nbuddies = 100;
		char tempName[30];

	//检查黑名单
	    err = VNG_GetBuddyList(vng, 0, buddyinfo, &nbuddies, VNG_TIMEOUT);
	    if (err != VNG_OK) {
	        fprintf(stdout, "Get buddy list failure!\n");
	        return -1;
	    }
	//如果发现buddy list 里面有好友，把一个好友unblock 掉 
	
	if(nbuddies!=0)
		{
			//打印好友列表
			int i=0;
			for( i=0;i<(int)nbuddies;i++){
				fprintf(stdout,"%d : %s\n",i,buddyinfo[i].login);
				}
			fprintf(stdout,"Please input the loginName of the buddy you want to block:\n");
			scanf("%s",&tempName);

			 for ( i = 0; i < (int) nbuddies; i++) 
			 {
				VNGUserInfo *p = &buddyinfo[i];
				if(strcmp(p->login,tempName)==0)
					{
						err=VNG_UnblockUser(vng, p->uid, VNG_TIMEOUT);
						 if (err != VNG_OK) {
						 	fprintf(stdout,"unblock a Buddy in buddylist err! \n");
							dumpErrorInfo(err);
						        return -1;
						    }
					}
	    		  }
		}
	else{
		fprintf(stdout,"buddy list is null!\n");
		return 0;
		}
		
	return 1; 
}














 
int testListBuddy(const char *testname)
{

    VNG *vng;
    VN *vn=NULL, * remoteVN=NULL;
	
    char vngbuf[16*1024];
	VNGData       dataType=VNG_DATA_BUDDYSTATUS;
   VNGErrCode err;
   int error,vn_domain_type=0;
	
	fprintf(stdout,"******************************************************\n");
       fprintf(stdout,"*             buddy list test begin                  *\n");
	fprintf(stdout,"******************************************************\n");

	  vng=(VNG*)malloc(sizeof(VNG));
	  
       VNG_Init(vng, vngbuf, sizeof(VNG));
	   
	 TestData tdata;
	   

	    DWORD rtid;
		HANDLE rthread ;

	while(1){
		//打印 buddyList_main_menu 
		int tempnum=dumpMenu(BuddyList_menu,"BuddyList Test Menu:");
		switch(tempnum){
			case 0:  
				login(vng);
				break;
			case 1:
				if(VNG_GetLoginType(vng)==VNG_NOT_LOGIN)
					{
						fprintf(stdout,"Please login first!\n");
						break;
					}
				showUserInfo(vng);
				break;
			case 2:
				if(VNG_GetLoginType(vng)==VNG_NOT_LOGIN)
					{
						fprintf(stdout,"Please login first!\n");
						break;
					}
				listBuddyList(vng);
				break;
			case 3:
				if(VNG_GetLoginType(vng)==VNG_NOT_LOGIN)
					{
						fprintf(stdout,"Please login first!\n");
						break;
					}
				deleteBuddyByName(vng);
				break;
			case 4:
				if(VNG_GetLoginType(vng)==VNG_NOT_LOGIN)
					{
						fprintf(stdout,"Please login first!\n");
						break;
					}
				addBuddyByName(vng);
				break;
			case 5:
				if(VNG_GetLoginType(vng)==VNG_NOT_LOGIN)
					{
						fprintf(stdout,"Please login first!\n");
						break;
					}
				blockBuddyByName(vng);
				break;
			case 6:
				if(VNG_GetLoginType(vng)==VNG_NOT_LOGIN)
					{
						fprintf(stdout,"Please login first!\n");
						break;
					}
				unblockBuddyByName(vng);
				break;
			case 7:
				if(VNG_GetLoginType(vng)==VNG_NOT_LOGIN)
					{
						fprintf(stdout,"Please login first!\n");
						break;
					}
				// Enable tracking of buddy state changes
				err=VNG_EnableTracking(vng,  dataType, VNG_TIMEOUT);
				if(err!=VNG_OK)
					{
						fprintf(stdout,"enable tracking failure!\n");
						break;
					}
				
				break;
			case 8:
				if(VNG_GetLoginType(vng)==VNG_NOT_LOGIN)
					{
						fprintf(stdout,"Please login first!\n");
						break;
					}
				// Disable tracking of buddy state changes
				err=VNG_DisableTracking(vng,  dataType, VNG_TIMEOUT);
				if(err!=VNG_OK)
					{
						fprintf(stdout,"enable tracking failure!\n");
						break;
					}
				break;
			case 9:
				if(VNG_GetLoginType(vng)==VNG_NOT_LOGIN)
					{
						fprintf(stdout,"Please login first!\n");
						break;
					}
				acceptAllInvite(vng);
				break;	
			case 10:
				if(VNG_GetLoginType(vng)==VNG_NOT_LOGIN)
					{
						fprintf(stdout,"Please login first!\n");
						break;
					}
				rejectAllInvite(vng);
				break;		
			case 11:
				if(VNG_GetLoginType(vng)==VNG_NOT_LOGIN)
					{
						fprintf(stdout,"Please login first!\n");
						break;
					}
				if(logOut(vng)==-1)
					fprintf(stdout,"\r\n LogOut failure!\r\n");
				else 
					fprintf(stdout,"\r\n LogOut sucess!\r\n");
				break;
			case 12:
				/*登陆测试，反复登陆100 次，看是否失败*/
				{
				int fail_time=0;
				for(int i=0;i<1000;i++)
					{
if(VNG_GetLoginType(vng)==VNG_NOT_LOGIN)							{								
								default_login(vng,&fail_time);							
							}
					}
				fprintf(stdout,"\r\nLogin 1000 times,  Failed times = %d.\r\n",fail_time);
				}
				break;
			case 20:
				testErrorHandling();
				break;
			case 100:
				logOut(vng);
				VNG_Fini(vng);
				return 0;
				break;

			case 31:
			if(VNG_GetLoginType(vng)==VNG_NOT_LOGIN)					
				{
						fprintf(stdout,"Please login first!\n");
						break;
					}
				while(vn_domain_type!=1&&vn_domain_type!=2&&vn_domain_type!=3)
					{
						fprintf(stdout,"Please choose the VN domain type:(VN_DOMAIN_LOCAL = 1,VN_DOMAIN_ADHOC = 2,VN_DOMAIN_INFRA = 3)\r\n");
						scanf("%d",&vn_domain_type);
					}
				error=createVN(vng,&vn,(VNDomain)vn_domain_type);
				if(error!=1)
					{
					fprintf(stdout,"createVN() error!\r\n");
				}
				error=registVN_updateStatus(vng, vn);
				if(error!=1)
					{
					fprintf(stdout,"registVN_updateStatus() error!\r\n");
				}
				//创建一个线程用于接收刚创建的vn 中的成员发送的消息。
	
				    tdata.vn   = vn;
				    tdata.done = false;
				    tdata.attr = VN_DEFAULT_ATTR;
				    tdata.revent = CreateEvent(NULL, FALSE, FALSE, "RecvFini");
				    if (tdata.revent == NULL) 
				        cerr << "Failed to create RecvFini event" << endl;

					 rthread = CreateThread(NULL, 0, RecvThread2, &tdata, 0, &rtid);
				    if (rthread == NULL)
				        cerr << "Failed to create Send thread" << endl;

					  //  Sleep(3*1000);
				  //  tdata.done = true;
	  //  WaitForSingleObject(tdata.revent, 300000);
	  //  CloseHandle(tdata.revent);
	  //  CloseHandle(rthread);
	  
		fprintf(stdout,"registVN_updateStatus finish !\n");
				break;
			case 32:
                        if(VNG_GetLoginType(vng)==VNG_NOT_LOGIN)					
					{
						fprintf(stdout,"Please login first!\n");
						break;
					}
				searchVN_joinVN(vng, &remoteVN);
				break;
				case 33:
                        if(VNG_GetLoginType(vng)==VNG_NOT_LOGIN)					
					{
						fprintf(stdout,"Please login first!\n");
						break;
					}
			if(remoteVN!=NULL)
				SendMessageToPeer(remoteVN);
			else if(vn!=NULL)
				SendMessageToPeer(vn);
			
				break;
				case 34:
                        if(VNG_GetLoginType(vng)==VNG_NOT_LOGIN)					
					{
						fprintf(stdout,"Please login first!\n");
						break;
					}
				//searchVN_leaveVN(vng, &remoteVN);
				break;
			case 36:
			if(VNG_GetLoginType(vng)==VNG_NOT_LOGIN)
					{
						fprintf(stdout,"Please login first!\n");
						break;
					}
			/**/	LoopbackSingleThread(vng, 
					remoteVN,
					"SingleThread.Reliable", 
                         		test_duration, 
                         		VN_DEFAULT_ATTR);
			
				/* LoopbackMultiThread(vng,"MultiThread.Reliable", 
                        3,
                        VN_DEFAULT_ATTR);
				LoopbackSingleThread(&vng, 
					remoteVN,
					"SingleThread.Unreliable", 
                        		 test_duration, 
                        		 VN_ATTR_UNRELIABLE);

				LoopbackSingleThread(&vng, 
					remoteVN,
					"SingleThread.Timestamp", 
                        		 test_duration, 
                         		VN_ATTR_TIMESTAMP);

    				LoopbackSingleThread(&vng, 
					remoteVN,
					"SingleThread.Noencrypt", 
                         		test_duration, 
                         		VN_ATTR_NOENCRYPT);*/
				break;	

				
			default:
				break;
			}
		}

   
}

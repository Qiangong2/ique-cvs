/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */

#ifndef __RPC_JMPTAB_H__
#define __RPC_JMPTAB_H__

#include "nc_func_id.h"
#include "vng.h"
#include "vng_p.h"
#include "shr_trace.h"

typedef struct {
    u16 id;
    void* func;
} JTAB;

static JTAB jtab[] = {
    /* VNG library */
    { FI_VNG_INIT, VNG_Init },				/* 1001 */
    { FI_VNG_FINI, VNG_Fini },          		/* 1002 */
    { FI_VNG_LOGIN, VNG_Login },                     	/* 1003 */
    { FI_VNG_LOGOUT, VNG_Logout },                    	/* 1004 */
    { FI_VNG_GETLOGINTYPE, VNG_GetLoginType },         	/* 1005 */
    { FI_VNG_GETUSERINFO, VNG_GetUserInfo },            /* 1006 */
    { FI_VNG_NEWVN, VNG_NewVN },			/* 1007 */
    { FI_VNG_DELETEVN, VNG_DeleteVN },                  /* 1008 */
    { FI_VNG_JOINVN, VNG_JoinVN },			/* 1009 */
    { FI_VNG_GETJOINREQUEST, VNG_GetJoinRequest },	/* 1010 */
    { FI_VNG_ACCEPTJOINREQUEST, VNG_AcceptJoinRequest }, /* 1011 */
    { FI_VNG_REJECTJOINREQUEST, VNG_RejectJoinRequest }, /* 1012 */
    { FI_VNG_LEAVEVN, VNG_LeaveVN },			/* 1013 */
    { FI_VNG_NUMMEMBERS, VN_NumMembers },		/* 1014 */
    { FI_VNG_GETMEMBERS, VN_GetMembers },		/* 1015 */
    { FI_VNG_MEMBERSTATE, VN_MemberState },		/* 1016 */
    { FI_VNG_MEMBERUSERID, VN_MemberUserId },		/* 1017 */
    { FI_VNG_MEMBERLATENCY, VN_MemberLatency },		/* 1018 */
    { FI_VNG_MEMBERDEVICETYPE, VN_MemberDeviceType },	/* 1019 */
    { FI_VNG_GETVNID, VN_GetVNId },			/* 1020 */
    { FI_VNG_SELECT, VN_Select },			/* 1021 */
    { FI_VNG_SENDMSG, VN_SendMsg },			/* 1022 */
    { FI_VNG_RECVMSG, VN_RecvMsg },			/* 1023 */
    { FI_VNG_SENDREQ, VN_SendReq },			/* 1024 */
    { FI_VNG_RECVREQ, VN_RecvReq },			/* 1025 */
    { FI_VNG_SENDRESP, VN_SendResp },                   /* 1026 */
    { FI_VNG_RECVRESP, VN_RecvResp },                   /* 1027 */
    { FI_VNG_REGISTERRPCSERVICE, VN_RegisterRPCService }, /* 1028 */
    { FI_VNG_UNREGISTERRPCSERVICE, VN_UnregisterRPCService }, /* 1029 */
    { FI_VNG_SENDRPC, VN_SendRPC },			/* 1030 */
    { FI_VNG_RECVRPC, VNG_RecvRPC },			/* 1031 */
    { FI_VNG_GETEVENT, VNG_GetEvent },                  /* 1032 */
    { FI_VNG_INVITE, VNG_Invite },			/* 1033 */
    { FI_VNG_GETINVITATION, VNG_GetInvitation },	/* 1034 */
    { FI_VNG_REGISTERGAME, VNG_RegisterGame },		/* 1035 */
    { FI_VNG_UNREGISTERGAME, VNG_UnregisterGame },	/* 1036 */
    { FI_VNG_UPDATEGAMESTATUS, VNG_UpdateGameStatus },	/* 1037 */
    { FI_VNG_GETGAMESTATUS, VNG_GetGameStatus },	/* 1038 */
    { FI_VNG_GETGAMECOMMENTS, VNG_GetGameComments },	/* 1039 */
    { FI_VNG_SEARCHGAMES, VNG_SearchGames },		/* 1040 */
    { FI_VNG_ADDSERVERTOADHOCDOMAIN, VNG_AddServerToAdhocDomain }, /* 1041 */
    { FI_VNG_RESETADHOCDOMAIN, VNG_ResetAdhocDomain },	/* 1042 */
    { FI_VNG_CONNECTSTATUS, VNG_ConnectStatus },	/* 1043 */
    { FI_VNG_MYUSERID, VNG_MyUserId },                  /* 1044 */
    { FI_VNG_SELF, VN_Self },				/* 1045 */
    { FI_VNG_OWNER, VN_Owner },				/* 1046 */
    { FI_VNG_STATE, VN_State },				/* 1047 */

    /* SHR library */
    { FI_SHR_TRACEINIT, _SHR_trace_init },		/* 1400 */
    { FI_SHR_SETTRACEENABLE, _SHR_set_trace_enable },	/* 1401 */
    { FI_SHR_SETTRACELEVEL, _SHR_set_trace_level },	/* 1402 */
    { FI_SHR_SETTRACESHOWTIME, _SHR_set_trace_show_time }, /* 1403 */
    { FI_SHR_SETTRACESHOWTHREAD, _SHR_set_trace_show_thread }, /* 1404 */
    { FI_SHR_SETGRPTRACEENABLE, _SHR_set_grp_trace_enable }, /* 1405 */
    { FI_SHR_SETGRPTRACELEVEL, _SHR_set_grp_trace_level }, /* 1406 */
    { FI_SHR_SETSGTRACELEVEL, _SHR_set_sg_trace_level }, /* 1407 */
    { FI_SHR_SETGRPTRACEENABLEMASK, _SHR_set_grp_trace_enable_mask }, /* 1408 */
    { FI_SHR_SETGRPTRACEDISABLEMASK, _SHR_set_grp_trace_disable_mask },	/* 1409 */
    { FI_SHR_SETTRACEFILE, _SHR_set_trace_file },	/* 1410 */
    { FI_SHR_SETGRPTRACEFILE, _SHR_set_grp_trace_file }, /* 1411 */
    { FI_SHR_SETSGTRACEFILE, _SHR_set_sg_trace_file },	/* 1412 */
    { FI_SHR_TRACELEVSTR, _SHR_trace_lev_str },		/* 1413 */
    { FI_SHR_TRACEGRPSTR, _SHR_trace_grp_str },		/* 1414 */
    { FI_SHR_TRACEON, _SHR_trace_on },			/* 1415 */
    { FI_SHR_VTRACE, _SHR_vtrace }			/* 1416 */
};

#endif /*  __RPC_JMPTAB_H__ */

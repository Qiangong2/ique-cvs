/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */
#include "sk.h"

#include "rpc_jmptab.h"

static int jtab_order_checked = 0;

/* assert that the ID's in the jump table is in strict increasing order */
static int
check_jtab(void)
{
    int i;
    int id = 0;
    for (i = 0; i < sizeof(jtab)/sizeof(JTAB); ++i) {
	if (jtab[i].id > id)
	    id = jtab[i].id;
	else
	    return 0;
    }
    return 1;
} /* check_jtab */


static void*
binary_search_jtab(u16 id)
{
    int n = sizeof(jtab) / sizeof(JTAB);
    int hi = n;
    int lo = 0;

    while (hi > lo) {
	int mid = lo + (hi - lo) / 2;
	u16 value = jtab[mid].id;
	if (value == id)
	    return jtab[mid].func;
	else if (value > id)
	    hi = mid;
	else
	    lo = mid + 1;
    }
    return NULL;
} /* binary_search_jtab */


/*
 * Search for the address of an exported function based on the function ID.
 * Assert that the jump table has been sorted, then use binary search to
 * locate the entry.
 */
void*
jtab_get_addr(u16 id)
{
    if (! jtab_order_checked) {
	if (check_jtab())
	    jtab_order_checked = 1;
	else
	    return NULL;
    }

    return binary_search_jtab(id);
} /* jtab_get_addr */

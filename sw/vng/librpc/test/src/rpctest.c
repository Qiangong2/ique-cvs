#include "sc/sc.h"
#include "sc/ios.h"
#include "sc/libc.h"

static void* (*get_addr)(u16) = 0;


static int
dotest(u16 n, int pos)
{
    void* addr = (*get_addr)(n);
    int ret;
    if (addr != 0) {
	ret = pos;
	printf("%d: 0x%08x%s\n", n, addr, pos ? "" : " (should be NULL)");
    } else {
	ret = !pos;
	printf("%d: MISSING%s\n", n, pos ? "" : " (expected)");
    }
    return ret;
} /* dotest */


u16 neg_tests[] = { 0, 1000, 1043, 1399, 1417, 2000 };

void
runtest(void* handle)
{
    u16 i;
    int missed = 0;

    get_addr = (void*(*)(u16)) handle;

    /* positive tests */
    for (i = 1001; i <= 1042; ++i) {
	if (! dotest(i, 1))
	    ++missed;
    }

    for (i = 1400; i <= 1416; ++i) {
	if (! dotest(i, 1))
	    ++missed;
    }

    /* negative tests */
    for (i = 0; i < sizeof(neg_tests)/sizeof(u16); ++i) {
	if (! dotest(neg_tests[i], 0))
	    ++missed;
    }

    if (missed)
	printf("Test failed:  %d misses\n", missed);
    else
	printf("Test passed\n");
}

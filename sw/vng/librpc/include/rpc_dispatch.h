/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */

#ifndef __RPC_DISPATCH_H__
#define __RPC_DISPATCH_H__

#ifdef __cplusplus
extern "C" {
#endif

/* jump table function ID to address conversion */
extern void*
jtab_get_addr(u16 id); 

#ifdef __cplusplus
}
#endif

#endif /*  __RPC_DISPATCH_H__ */


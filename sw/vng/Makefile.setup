#
# BUILD_HOST:   It is the platform of host compiler
#               Choices are LINUX, WIN32.
#
# TARGET_HOST:  The platform that the compiled program runs on
#                Choices are LINUX, WIN32, GBA, SC, DS, NNGC, SPU
#
#  
#

ifeq ($(findstring CYGWIN,$(shell uname)),)
  BUILD_HOST=LINUX
else
  BUILD_HOST=WIN32
endif

ifndef ROOT
  $(error ROOT env variable is not defined)
endif

# If TARGET_HOST not defined, use the BUILD_HOST as the default
#
ifndef TARGET_HOST
  TARGET_HOST:=$(BUILD_HOST)
endif

ifeq ($(TARGET_HOST),LINUX)
  PROCESSOR=i386
  OPT_FLAG=-O
  TOOLROOT = 
  CC = $(TOOLROOT)/usr/bin/cc
  CXX = $(TOOLROOT)/usr/bin/c++
  LD = $(TOOLROOT)/usr/bin/ld
  AR = $(TOOLROOT)/usr/bin/ar
  DYN_LIB_EXT = so
  LIB_EXT     = a
  EXTRA_CFLAGS = -D_LINUX -I$(ROOT)/usr/include
endif

# This is the Nintendo GBA SDK
ifeq ($(TARGET_HOST),NINTENDO_GBA)
  PROCESSOR=ARM7TDMI
  OPT_FLAG=-O
  USE_STATIC_LIBS=1
  TOOLROOT=/Cygnus/thumbelf-000512/H-i686-cygwin32
  CC = PATH=$(TOOLROOT)/bin $(TOOLROOT)/bin/gcc
  CXX = $(TOOLROOT)/bin/g++
  AS = $(TOOLROOT)/bin/as
  LD = $(TOOLROOT)/bin/ld
  OBJCOPY = $(TOOLROOT)/bin/objcopy
  OBJDUMP = $(TOOLROOT)/bin/objdump
  AGBINC     =   $(AGBDIR)/include
  AGBLIB     =   $(AGBDIR)/lib
  ASFLAGS    =   --gstabs -mthumb-interwork
  EXTRA_CFLAGS  =  -g -I$(AGBINC) -mthumb-interwork -nostdlib -DSC_RPC -D_GBA 
  DYN_LIB_EXT  = not-supported
  LIB_EXT      = a
endif

ifeq ($(TARGET_HOST),GBA)
  PROCESSOR=ARM7TDMI
  OPT_FLAG=-O
  USE_STATIC_LIBS=1
  TOOLROOT=/opt/crosstool
  CC = $(TOOLROOT)/bin/arm-elf-gcc
  CXX = $(TOOLROOT)/bin/arm-elf-g++
  AS = $(TOOLROOT)/bin/arm-elf-as 
  LD = $(TOOLROOT)/bin/arm-elf-ld
  OBJCOPY = $(TOOLROOT)/bin/arm-elf-objcopy
  OBJDUMP = $(TOOLROOT)/bin/arm-elf-objdump
  CONVERT = $(TOOLROOT)/bin/arm-elf-strip -O binary
  MERGE = $(ROOT)/usr/bin/x86/arm-elf-merge
  PUBLISH = env PATH="$(ROOT)/usr/bin/x86:$(PATH)" publish.py

  # SC/GBA tree COPTs from $(ROOT)/usr/include/make/GBAdefs

  GCOPTS  = -mcpu=arm7tdmi -mthumb-interwork -fomit-frame-pointer -fno-builtin
  GCDEFS = -D_GBA -DGBA
  GCINCS = -I$(ROOT)/usr/include/gba -I$(ROOT)/usr/include
  EXTRA_CFLAGS = $(GCDEFS) $(GCINCS) $(GCOPTS)

  # SC/GBA tree assembler variables from $(ROOT)/usr/include/make/GBAdefs
  GASDEFS =
  GASOPTS = -mcpu=arm7tdmi -mthumb-interwork 
  GASINCS = -I$(ROOT)/usr/include/gba -I$(ROOT)/usr/include

  LASFLAGS = $(LASDEFS) $(LASINCS) $(LASOPTS)
  GASFLAGS = $(GASDEFS) $(GASINCS) $(GASOPTS)
  ASFLAGS = $(LASFLAGS) $(GASFLAGS)

  # Linker variables
  GLDOPTS = -mthumb-interwork -L$(ROOT)/usr/lib/gba -nostartfiles -nostdlib -Xlinker --warn-common #-Xlinker --fatal-warnings
  GLDLIBS = -lgcc

  DYN_LIB_EXT  = not-supported
  LIB_EXT      = a
endif

ifeq ($(TARGET_HOST),SC)
  PROCESSOR=ARM7TDMI
  OPT_FLAG=-O
  USE_STATIC_LIBS=1
  TOOLROOT=/opt/crosstool
  CC = $(TOOLROOT)/bin/arm-elf-gcc
  CXX = $(TOOLROOT)/bin/arm-elf-g++
  AS = $(TOOLROOT)/bin/arm-elf-gcc -x assembler-with-cpp -c
  LD = $(TOOLROOT)/bin/arm-elf-ld
  OBJCOPY = $(TOOLROOT)/bin/arm-elf-objcopy
  OBJDUMP = $(TOOLROOT)/bin/arm-elf-objdump
  CONVERT = $(TOOLROOT)/bin/arm-elf-strip -O binary
  MERGE = $(ROOT)/usr/bin/x86/arm-elf-merge
  PUBLISH = env PATH="$(ROOT)/usr/bin/x86:$(PATH)" publish.py
  SDRAM_LINK = $(ROOT)/usr/lib/sc/sdram.link
  DA_LINK = $(ROOT)/usr/lib/sc/da.link
  # SC/GBA tree COPTs from $(ROOT)/usr/include/make/SCdefs

  GCOPTS  = -mcpu=arm7tdmi -mthumb-interwork -fomit-frame-pointer -fno-builtin
  GCDEFS = -D_SC
  GCINCS = -I$(ROOT)/usr/include -I$(ROOT)/usr/include/sc
  EXTRA_CFLAGS = $(GCDEFS) $(GCINCS) $(GCOPTS)

  # SC/GBA tree assembler variables from $(ROOT)/usr/include/make/SCdefs
  GASDEFS =
  GASOPTS = -mcpu=arm7tdmi -mthumb-interwork -Wall -Werror
  GASINCS = -I$(ROOT)/usr/include

  # SC/GBA tree linker variables from $(ROOT)/usr/include/make/SCdefs 
  GLDOPTS = -L$(ROOT)/usr/lib/sc -nostartfiles -nostdlib -Xlinker --warn-common #-Xlinker --fatal-warnings
  GLDLIBS = -lgcc
  CRT0 = $(ROOT)/usr/lib/sc/crt0.o
  
  LASFLAGS = $(LASDEFS) $(LASINCS) $(LASOPTS)
  GASFLAGS = $(GASDEFS) $(GASINCS) $(GASOPTS)
  ASFLAGS = $(LASFLAGS) $(GASFLAGS)

  include $(ROOT)/usr/include/make/SCaddrs

  DYN_LIB_EXT  = not-supported
  LIB_EXT      = a
endif

ifeq ($(TARGET_HOST),WIN32)
  UPNP = 0
  VCC=$(SRC_BASE)/tools-win/inVcEnv.cmd
  ifeq ($(BUILD_HOST),WIN32)
    LIBVNG_PATH:=$(shell cygpath -u '$(SC_SDK)'/usr/bin/win32)
  endif
  DYN_LIB_EXT  = dll
  LIB_EXT      = lib
else
  ifeq ($(BUILD_HOST),WIN32)
    ROOT:=$(shell cygpath -u '$(ROOT)')
  endif
endif

ifdef USE_STATIC_LIBS
  LIB_TYPE=$(DYN_LIB_EXT)
else
  LIB_TYPE=$(LIB_EXT)
endif


CXXFLAGS = $(OPT_FLAG) $(EXTRA_CFLAGS) -Wall 
CFLAGS   = $(OPT_FLAG) $(EXTRA_CFLAGS) -Wall 
LDFLAGS  = $(OPT_FLAG) 


# Install Directory
INSTALL.SDK = $(SRC_BASE)/build
INSTALL.DOC = $(SRC_BASE)/build/usr/doc/vng
INSTALL.SC_LIB = $(SRC_BASE)/build/usr/lib/sc
INSTALL.SC_BIN = $(SRC_BASE)/build/usr/bin/sc
INSTALL.SC_INC = $(SRC_BASE)/build/usr/include
INSTALL.WIN32_LIB = $(SRC_BASE)/build/usr/lib/win32
INSTALL.WIN32_BIN = $(SRC_BASE)/build/usr/bin/win32
INSTALL.WIN32_INC = $(SRC_BASE)/build/usr/include
INSTALL.GBA_LIB = $(SRC_BASE)/build/usr/lib/gba
INSTALL.GBA_BIN = $(SRC_BASE)/build/usr/bin/gba
INSTALL.GBA_INC = $(SRC_BASE)/build/usr/include
INSTALL.LINUX_LIB = $(SRC_BASE)/build/usr/lib/x86
INSTALL.LINUX_BIN = $(SRC_BASE)/build/usr/lib/bin
INSTALL.LINUX_INC = $(SRC_BASE)/build/usr/include

# Export list
export CC
export CXX
export CFLAGS
export LDFLAGS
export BUILD_HOST
export TARGET_HOST

#
MAKE = /usr/bin/make
INSTALL = /usr/bin/install -c

# Packaging utils
#
PKGEN=$(SERVER_BASE)/devroot/pktools/pkgen

RELGEN = @sed -e 's:@@MODULE_NAME@@:$(MODULE_NAME):g'           \
	-e 's:@@DESC@@:$(DESC):'                            \
	-e 's:@@MAJOR_VERSION@@:$(MAJOR_VERSION):g'         \
	-e 's:@@REVISION@@:$(REVISION):g'                   \
	-e 's:@@RELEASE@@:$(RELEASE):g'

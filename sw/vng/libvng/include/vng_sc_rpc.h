#include "vng.h"
#include "server_rpc.h"
#include "sc/rpc.h"

#ifndef _GBA
#pragma pack(push, 4)
#endif


#define VNG_INTERFACE_ID       777

enum VNGLIB_RPC {
    VNG_PROC_ID_EXIT,
    VNG_PROC_ID_INIT,
    VNG_PROC_ID_FINI,
    VNG_PROC_ID_LOGIN,
    VNG_PROC_ID_LOGOUT,
    VNG_PROC_ID_GETLOGINTYPE,
    VNG_PROC_ID_GETUSERINFO,
    VNG_PROC_ID_NEWVN,
    VNG_PROC_ID_DELETEVN,
    VNG_PROC_ID_JOINVN,
    VNG_PROC_ID_GETJOINREQUEST,
    VNG_PROC_ID_ACCEPTJOINREQUEST,
    VNG_PROC_ID_REJECTJOINREQUEST,
    VNG_PROC_ID_LEAVEVN,
    VNG_PROC_ID_NUMMEMBERS,
    VNG_PROC_ID_GETMEMBERS,
    VNG_PROC_ID_MEMBERSTATE,
    VNG_PROC_ID_MEMBERUSERID,
    VNG_PROC_ID_MEMBERLATENCY,
    VNG_PROC_ID_MEMBERDEVICETYPE,
    VNG_PROC_ID_GETVNID,
    VNG_PROC_ID_SELECT,
    VNG_PROC_ID_SENDMSG,
    VNG_PROC_ID_RECVMSG,
    VNG_PROC_ID_SENDREQ,
    VNG_PROC_ID_RECVREQ,
    VNG_PROC_ID_SENDRESP,
    VNG_PROC_ID_RECVRESP,
    VNG_PROC_ID_REGISTERRPCSERVICE,
    VNG_PROC_ID_UNREGISTERRPCSERVICE,
    VNG_PROC_ID_SENDRPC,
    VNG_PROC_ID_RECVRPC,
    VNG_PROC_ID_GETEVENT,
    VNG_PROC_ID_INVITE,
    VNG_PROC_ID_GETINVITE,
    VNG_PROC_ID_REGISTERGAME,
    VNG_PROC_ID_UNREGISTERGAME,
    VNG_PROC_ID_UPDATEGAMESTATUS,
    VNG_PROC_ID_GETGAMESTATUS,
    VNG_PROC_ID_GETGAMECOMMENTS,
    VNG_PROC_ID_SEARCHGAMES,
    VNG_PROC_ID_ADDSERVERTOADHOCDOMAIN,
    VNG_PROC_ID_RESETADHOCDOMAIN,
    LCE_PROC_ID_INIT,
    LCE_PROC_ID_SETDUMMYDATA,
    LCE_PROC_ID_SETDELAY,
    LCE_PROC_ID_SETFRAMELEN,
    LCE_PROC_ID_ENABLE,
    LCE_PROC_ID_DISABLE,
    LCE_PROC_ID_SWITCHMODE,
    LCE_PROC_ID_TRIGGER,
    LCE_PROC_ID_SCHEDSIOINTR,
    LCE_PROC_ID_VBLANK,
    VNG_PROC_ID_CONNECTSTATUS,
    VNG_PROC_ID_MYUSERID,
    VNG_PROC_ID_SELF,
    VNG_PROC_ID_OWNER,
    VNG_PROC_ID_STATE,
};

typedef struct {
    VNG     *vng;
} VNG_INIT_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_INIT_ARG    in;
    // output
    VNGErrCode      out;
} VNG_INIT_CMD;

typedef struct {
    VNG     *vng;
} VNG_FINI_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_FINI_ARG    in;
    // output
    VNGErrCode      out;
} VNG_FINI_CMD;

typedef struct {
    VNG        *vng;
    char        serverName[256];
    VNGPort     serverPort;
    char        loginName[MAX_USERNAME_LEN];
    char        passwd[MAX_PASSWD_LEN];
    int32_t     loginType;
    VNGTimeout  timeout;
} VNG_LOGIN_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_LOGIN_ARG   in;
    // output
    VNGErrCode      out;
} VNG_LOGIN_CMD;

typedef struct {
    VNG        *vng;
    VNGTimeout  timeout;
} VNG_LOGOUT_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_LOGOUT_ARG   in;
    // output
    VNGErrCode      out;
} VNG_LOGOUT_CMD;

typedef struct {
    VNG     *vng;
} VNG_GETLOGINTYPE_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_GETLOGINTYPE_ARG    in;
    // output
    int32_t         out;
} VNG_GETLOGINTYPE_CMD;

typedef struct {
    VNG         *vng;
    VNGUserId    uid;
    VNGUserInfo *uinfo;
    VNGTimeout   timeout;
} VNG_GETUSERINFO_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_GETUSERINFO_ARG in;
    // output
    VNGErrCode      out;
} VNG_GETUSERINFO_CMD;

typedef struct {
    VNG       *vng;
    VN        *vn;
    VNClass    vnClass;
    VNDomain   domain;
    VNPolicies policies;
} VNG_NEWVN_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_NEWVN_ARG   in;
    // output
    VNGErrCode      out;
} VNG_NEWVN_CMD;

typedef struct {
    VNG       *vng;
    VN        *vn;
} VNG_DELETEVN_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_DELETEVN_ARG   in;
    // output
    VNGErrCode      out;
} VNG_DELETEVN_CMD;

typedef struct {
    VNG         *vng;
    VNId         vnId;
    const char  *msg;
    VN          *vn;
    char        *denyReason;
    size_t       denyReasonLen;
    VNGTimeout   timeout;
} VNG_JOINVN_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_JOINVN_ARG   in;
    // output
    VNGErrCode      out;
} VNG_JOINVN_CMD;

typedef struct {
    VNG         *vng;
    VNId         vnId;
    VN          *vn;
    uint64_t     requestId;
    VNGUserInfo *userInfo;
    char        *joinReason;
    size_t       joinReasonLen;
    VNGTimeout   timeout;
} VNG_GETJOINREQUEST_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_GETJOINREQUEST_ARG   in;
    // output
    VNGErrCode      out;
} VNG_GETJOINREQUEST_CMD;

typedef struct {
    VNG        *vng;
    uint64_t    requestId;
} VNG_ACCEPTJOINREQUEST_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_ACCEPTJOINREQUEST_ARG   in;
    // output
    VNGErrCode      out;
} VNG_ACCEPTJOINREQUEST_CMD;

typedef struct {
    VNG        *vng;
    uint64_t    requestId;   
    const char *reason;
} VNG_REJECTJOINREQUEST_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_REJECTJOINREQUEST_ARG   in;
    // output
    VNGErrCode      out;
} VNG_REJECTJOINREQUEST_CMD;

typedef struct {
    VNG *vng;
    VN *vn;
} VNG_LEAVEVN_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_LEAVEVN_ARG   in;
    // output
    VNGErrCode      out;
} VNG_LEAVEVN_CMD;

typedef struct {
    VNG *vng;
    VN *vn;
} VNG_NUMMEMBERS_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_NUMMEMBERS_ARG   in;
    // output
    uint32_t      out;
} VNG_NUMMEMBERS_CMD;

typedef struct {
    VN       *vn;
    VNMember *members;
    uint32_t  nMembers;
} VNG_GETMEMBERS_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_GETMEMBERS_ARG   in;
    // output
    uint32_t      out;
} VNG_GETMEMBERS_CMD;

typedef struct {
    VN *vn;
    VNMember memb;
} VNG_MEMBERSTATE_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_MEMBERSTATE_ARG   in;
    // output
    VNGErrCode      out;
} VNG_MEMBERSTATE_CMD;

typedef struct {
    VN *vn;
    VNMember memb;
    VNGTimeout timeout;
} VNG_MEMBERUSERID_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_MEMBERUSERID_ARG   in;
    // output
    VNGUserId      out;
} VNG_MEMBERUSERID_CMD;

typedef struct {
    VN *vn;
    VNMember memb;
} VNG_MEMBERLATENCY_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_MEMBERLATENCY_ARG   in;
    // output
    VNGMillisec      out;
} VNG_MEMBERLATENCY_CMD;

typedef struct {
    VN *vn;
    VNMember memb;
} VNG_MEMBERDEVICETYPE_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_MEMBERDEVICETYPE_ARG   in;
    // output
    VNGDeviceType      out;
} VNG_MEMBERDEVICETYPE_CMD;

typedef struct {
    VN      *vn;
    VNId    vnId;
} VNG_GETVNID_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_GETVNID_ARG   in;
    // output
    VNGErrCode      out;
} VNG_GETVNID_CMD;

typedef struct {
    VN               *vn;
    VNServiceTypeSet  selectFrom;
    VNServiceTypeSet  ready;
    VNMember          memb;
    VNGTimeout        timeout;
} VNG_SELECT_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_SELECT_ARG   in;
    // output
    VNGErrCode      out;
} VNG_SELECT_CMD;

typedef struct {
    VN            *vn;
    VNMember       memb; 
    VNServiceTag   serviceTag;
    const void    *msg;
    size_t         msglen;
    VNAttr         attr;
    VNGTimeout     timeout;
} VNG_SENDMSG_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_SENDMSG_ARG   in;
    // output
    VNGErrCode      out;
} VNG_SENDMSG_CMD;

typedef struct {
    VN           *vn; 
    VNMember      memb;  
    VNServiceTag  serviceTag;
    void         *msg;
    size_t        msglen;
    VNMsgHdr     *hdr;
    VNGTimeout    timeout;
} VNG_RECVMSG_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_RECVMSG_ARG   in;
    // output
    VNGErrCode      out;
} VNG_RECVMSG_CMD;

typedef struct {
    VN           *vn; 
    VNMember      memb;
    VNServiceTag  serviceTag;
    VNCallerTag   callerTag;
    const void   *msg;
    size_t        msglen;
    int32_t       optData;
    VNGTimeout    timeout;
} VNG_SENDREQ_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_SENDREQ_ARG   in;
    // output
    VNGErrCode      out;
} VNG_SENDREQ_CMD;

typedef struct {
    VN           *vn;
    VNMember      memb;  
    VNServiceTag  serviceTag;
    void         *msg;
    size_t        msglen;
    VNMsgHdr     *hdr;
    VNGTimeout    timeout;
} VNG_RECVREQ_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_RECVREQ_ARG   in;
    // output
    VNGErrCode      out;
} VNG_RECVREQ_CMD;

typedef struct {
    VN           *vn; 
    VNMember      memb;
    VNServiceTag  serviceTag;
    VNCallerTag   callerTag;
    const void   *msg;
    size_t        msglen;
    int32_t       optData;
    VNGTimeout    timeout;
} VNG_SENDRESP_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_SENDRESP_ARG   in;
    // output
    VNGErrCode      out;
} VNG_SENDRESP_CMD;

typedef struct {
    VN           *vn;
    VNMember      memb;  
    VNServiceTag  serviceTag;
    VNCallerTag   callerTag;  
    void         *msg;
    size_t        msglen;
    VNMsgHdr     *hdr;
    VNGTimeout    timeout;
} VNG_RECVRESP_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_RECVRESP_ARG   in;
    // output
    VNGErrCode      out;
} VNG_RECVRESP_CMD;

typedef struct {
    VN               *vn;
    VNServiceTag      procId;
    VNRemoteProcedure proc;
} VNG_REGISTERRPCSERVICE_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_REGISTERRPCSERVICE_ARG   in;
    // output
    VNGErrCode      out;
} VNG_REGISTERRPCSERVICE_CMD;

typedef struct {
    VN               *vn;
    VNServiceTag      procId;
} VNG_UNREGISTERRPCSERVICE_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_UNREGISTERRPCSERVICE_ARG   in;
    // output
    VNGErrCode      out;
} VNG_UNREGISTERRPCSERVICE_CMD;

typedef struct {
    VN           *vn; 
    VNMember      memb;
    VNServiceTag  serviceTag;
    const void   *args;
    size_t        argsLen;
    void         *ret;
    size_t        retLen;
    int32_t       optData;
    VNGTimeout    timeout;
} VNG_SENDRPC_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_SENDRPC_ARG   in;
    // output
    VNGErrCode      out;
} VNG_SENDRPC_CMD;

typedef struct {
    VNG               *vng; 
    VNRemoteProcedure *proc;
    VN               **vn;
    VNMsgHdr          *hdr;
    void              *args;
    size_t            *argsLen;
    VNGTimeout         timeout;
} VNG_RECVRPC_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_RECVRPC_ARG   in;
    // output
    VNGErrCode      out;
} VNG_RECVRPC_CMD;

typedef struct {
    VNG        *vng;
    VNGEvent   *event;
    VNGTimeout  timeout;
} VNG_GETEVENT_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_GETEVENT_ARG   in;
    // output
    VNGErrCode      out;
} VNG_GETEVENT_CMD;

typedef struct {
    VNG          *vng;
    VNGGameInfo  *gameInfo;
    VNGUserId    *uid;
    uint32_t      nUsers;
    const char   *inviteMsg;
} VNG_INVITE_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_INVITE_ARG in;
    // output
    VNGErrCode      out;
} VNG_INVITE_CMD;

typedef struct {
    VNG          *vng;
    VNGUserInfo  *userInfo;
    VNGGameInfo  *gameInfo;
    char         *inviteMsg;
    size_t        inviteMsgLen;
    VNGTimeout    timeout;
} VNG_GETINVITE_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_GETINVITE_ARG in;
    // output
    VNGErrCode      out;
} VNG_GETINVITE_CMD;

typedef struct {
    VNG          *vng;
    VNGGameInfo  *info;
    char         *comments;
    VNGTimeout    timeout;
} VNG_REGISTERGAME_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_REGISTERGAME_ARG in;
    // output
    VNGErrCode      out;
} VNG_REGISTERGAME_CMD;

typedef struct {
    VNG          *vng;
    VNId          vnId;
    VNGTimeout    timeout;
} VNG_UNREGISTERGAME_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_UNREGISTERGAME_ARG in;
    // output
    VNGErrCode      out;
} VNG_UNREGISTERGAME_CMD;

typedef struct {
    VNG          *vng;
    VNId          vnId;
    uint32_t      gameStatus;
    uint32_t      numPlayers;
    VNGTimeout    timeout;
} VNG_UPDATEGAMESTATUS_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_UPDATEGAMESTATUS_ARG in;
    // output
    VNGErrCode      out;
} VNG_UPDATEGAMESTATUS_CMD;

typedef struct {
    VNG            *vng;
    VNGGameStatus  *gameStatus;
    uint32_t        nGameStatus;
    VNGTimeout      timeout;
} VNG_GETGAMESTATUS_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_GETGAMESTATUS_ARG in;
    // output
    VNGErrCode      out;
} VNG_GETGAMESTATUS_CMD;

typedef struct {
    VNG          *vng;
    VNId          vnId;
    char         *comments;
    size_t        commentSize;
    VNGTimeout    timeout;
} VNG_GETGAMECOMMENTS_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_GETGAMECOMMENTS_ARG in;
    // output
    VNGErrCode      out;
} VNG_GETGAMECOMMENTS_CMD;

typedef struct {
    VNG                *vng;
    VNGSearchCriteria  *searchCriteria;
    VNGGameStatus      *gameStatus;
    uint32_t            nGameStatus;
    uint32_t            skipN;
    VNGTimeout          timeout;
} VNG_SEARCHGAMES_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_SEARCHGAMES_ARG in;
    // output
    VNGErrCode      out;
} VNG_SEARCHGAMES_CMD;

typedef struct {
    VNG          *vng;
    const char   *serverName;
    VNGPort       serverPort;
} VNG_ADDSERVERTOADHOCDOMAIN_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_ADDSERVERTOADHOCDOMAIN_ARG in;
    // output
    VNGErrCode      out;
} VNG_ADDSERVERTOADHOCDOMAIN_CMD;

typedef struct {
    VNG          *vng;
} VNG_RESETADHOCDOMAIN_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_RESETADHOCDOMAIN_ARG in;
    // output
    VNGErrCode      out;
} VNG_RESETADHOCDOMAIN_CMD;

typedef struct {
    VNG     *vng;
} VNG_CONNECTSTATUS_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_CONNECTSTATUS_ARG    in;
    // output
    uint32_t        out;
} VNG_CONNECTSTATUS_CMD;

typedef struct {
    VNG     *vng;
} VNG_MYUSERID_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_MYUSERID_ARG    in;
    // output
    VNGUserId       out;
} VNG_MYUSERID_CMD;

typedef struct {
    VN     *vn;
} VNG_SELF_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_SELF_ARG    in;
    // output
    VNMember        out;
} VNG_SELF_CMD;

typedef struct {
    VN     *vn;
} VNG_OWNER_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_OWNER_ARG    in;
    // output
    VNMember        out;
} VNG_OWNER_CMD;

typedef struct {
    VN     *vn;
} VNG_STATE_ARG;

typedef struct {
    RPCHeader       header;
    // input
    VNG_STATE_ARG    in;
    // output
    VNState         out;
} VNG_STATE_CMD;

typedef struct {
    RPCHeader header;
    u32 input;
} LCE_RPC_CMD;


typedef struct {
    LCE_RPC_CMD cmd;
    u32 debug;
    u32 numgbas;
    char vngServer[100];
    char vngPort[100];
    char vngUser[100];
    char vngPasswd[100];
} LCE_INIT_CMD;

#ifndef _GBA
#pragma pack(pop)
#endif

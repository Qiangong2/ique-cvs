/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */

#ifndef __VNG_PRIV_H__
#define __VNG_PRIV_H__  1


#ifdef __cplusplus
extern "C" {
#endif


// See pack comments in vng.h
//
#if !defined(_GBA) && !defined(_SC)
#pragma pack(push, 4)
#endif


#define  _VNG_INVALID_VNGVNID      ( 0)
#define  _VNG_INVALID_VNGID        (-1)
#define  _VNG_INVALID_VNGNETADDR   (0xffffffff)


typedef uint32_t _VNGVnId;
typedef  int32_t _VNGId;
typedef uint32_t _VNGNetAddr;


struct __VN {
   _VNGVnId        id;       // unique per join/create, 0 is an invalid id
   _VNGId          vng_id;
   _VNGNetAddr     addr;     // identifies virtual net and self member
};



struct __VNG {

    _VNGId         id;
};


#if !defined(_GBA) && !defined(_SC)
#pragma pack(pop)
#endif


#ifdef  __cplusplus
}
#endif

#endif // __VNG_PRIV_H__

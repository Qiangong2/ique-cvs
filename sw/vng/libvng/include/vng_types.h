//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#ifndef __VNG_TYPES_H__
#define __VNG_TYPES_H__

#ifdef _LINUX

    #ifdef __cplusplus
        #ifndef __STDC_LIMIT_MACROS
        #define __STDC_LIMIT_MACROS
        #endif
        #ifndef __STDC_CONSTANT_MACROS
        #define __STDC_CONSTANT_MACROS
        #endif
    #endif

    #include <stdint.h>                 // ISO C99
    // #include <stdbool.h>             // ISO C99 boolean type

#else // WIN32, GBA, && SC

    // MSVC isn't C99 compliant
    // MSVC has no stdint.h

    // The following would normally be defined in stdint.h

#ifndef __VNP_I_H__
    typedef signed char             int8_t;
    typedef short int               int16_t;
    typedef int                     int32_t;
    typedef long long int           int64_t;

    typedef unsigned char           uint8_t;
    typedef unsigned short int      uint16_t;
    typedef unsigned int            uint32_t;
    typedef unsigned long long int  uint64_t;
#endif 
    #define UINT32_C(c)             c ## U
    #define UINT64_C(c)             c ## ULL
    #define __INT64_C(c)            c ## LL
    #define UINT16_MAX              (65535)
    #define UINT8_MAX               (255)
    #define UINT32_MAX              (4294967295)
    #define UINT64_MAX              (__UINT64_C(18446744073709551615))
    #define INT8_MAX                (127)
    #define INT16_MAX               (32767)
    #define INT32_MAX               (2147483647)
    #define INT64_MAX               (__INT64_C(9223372036854775807))
    #define INT8_MIN                (-128)
    #define INT16_MIN               (-32767-1)
    #define INT32_MIN               (-2147483647-1)
    #define INT64_MIN               (-__INT64_C(9223372036854775807)-1)

    // MSVC does not support the C99 inline keyword but does
    // support the MS specific __inline keyword
    // MSVC++ does support inline keyword

    #ifndef __cplusplus
        #define inline __inline
    #endif

#endif

#ifndef __VNP_I_H__
#ifndef __cplusplus

    typedef unsigned char bool;

    #define false 0
    #define true  1
#endif
#endif


#define  _VNG_VN_MEMBER_INVALID        0xffff
#define  _VNG_VN_MEMBER_SELF           0xfffe
#define  _VNG_VN_MEMBER_OTHERS         0xfffd
#define  _VNG_VN_MEMBER_ALL_OR_ANY     0xfffc
#define  _VNG_VN_MEMBER_OWNER          0xfffb

#define  _VNG_VN_MEMBER_MAX            0xffef



#endif /* __VNG_TYPES_H__ */

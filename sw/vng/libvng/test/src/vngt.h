#ifndef __VNGT_H__
#define __VNGT_H__  1

#ifdef _WIN32
    #pragma once
#endif

//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vngt_plat.h"
#include "shr_trace.h"
#include "vngt_test.h"
#include "vngt_join.h"

#define SG_MISC  0

#undef  MISC
#define MISC _TRACE_TEST, SG_MISC


#define VNGT_VN_AUTO_CREATED    1
#define VNGT_REQ_EXIT          'x'
#define VNGT_DEF_USERID         2005

#define VNGT_DEF_SHOW_CORRECT   1
#define VNGT_VN_ST_INITIALIZED  0x01
#define VNGT_ST_FINI            VNG_ST_RM_OK

#define VNGT_ST_INIT        (VNG_ST_RM_OK | \
                             VNGT_VN_ST_INITIALIZED | \
                             VNG_ST_VNG_INITIALIZED | \
                             VNG_ST_GATEWAY_DEFINED)

#define VNGT_ST_INIT_LOGIN  (VNGT_ST_INIT | VNG_ST_USER_LOGIN)

#define VNGT_ST_USB         (VNG_ST_USB_CONNECTED   | \
                             VNG_ST_PROXY_RUNNING   | \
                             VNG_ST_PROXY_CONNECTED | \
                             VNG_ST_ADHOC_SUPPORTED)


#ifdef __cplusplus

    #include "vngt_select.h"
    #include "vngt_pi.h"
    #include "vngt_adhoc.h"


extern "C" {
#endif

    // test roles

    typedef enum {
        NO_ROLE = 0,
        GAME_HOST,
        GAME_JOINER,
        NUM_ROLES
    } Role;

    extern const char *vngt_role_name[NUM_ROLES];
    extern VNDomain  domain;

    // user id definition for some tests

    typedef union {
        VNGUserId  vngUserId;
        struct {
            int16_t vn_num;
            int16_t player_num;
        };
    } VNGTUserId;


#ifdef  __cplusplus
}
#endif



//#########################################################
//
//    NOTE:   The rest of this header is C++ only
//
//#########################################################

#ifdef __cplusplus


int vng_msgs     (TestEnv& tests);
int loopbackRPCs (TestEnv& tests);
int serverAPIs   (TestEnv& tests);


class TestEnv {
    public:

    string        conf_file;
    HashMapString conf;
    string        confAsString;

    bool          iamClient;
    bool          iamServer;
    bool          iamGameHost;
    bool          iamGameJoiner;
    uint32_t      host;        // mask of vn_num to host
    uint32_t      join;        // mask of vn_num to join
    uint32_t      cStatInit;
    uint32_t      cStatInitLogin;
    VNGUserId     uid;         // my usid
    int           maxFailBeforeQuit;
    int           iterations;
    VNGTimeout    timeout;  // Default VNG API timeout
    VNGPort       serverPort;
    string        server;
    string        h_loginName;
    string        j_loginName;
    string        h_passwd;
    string        j_passwd;

    Tests            toRun;
    Tests            all;
    Tests            passed;
    Tests            failed;
    TestsByName      byName;
    TestsByShortName byShortName;

    Test         *cur;

    Test          vng_msgsTest;
    Test          loopbackRPCsTest;
    Test          serverAPIsTest;
    CTestProxy    joinAPIsTest;
    SelectTest    selectTest;
    PiTest        piTest;
    AdhocTest     adhocTest;

    TestEnv () :
        conf_file ("vngt.conf"),
        iamClient (true),
        iamServer (true),
        iamGameHost (true),
        iamGameJoiner (true),
        host (0),
        join (0),
        cStatInit(VNGT_ST_INIT),
        cStatInitLogin(VNGT_ST_INIT_LOGIN),
        maxFailBeforeQuit (3),
        iterations (1),
        timeout (10*1000),
        serverPort (VNG_DEF_SERVER_PORT),
        server ("localhost"),
        h_loginName ("10001"),
        j_loginName ("10002"),
        h_passwd ("10001"),
        j_passwd ("10002"),
        cur (NULL),

        vng_msgsTest     (vng_msgs,     "vng_msgs",      "msgs"),
        loopbackRPCsTest (loopbackRPCs, "loopbackRPCs",  "lbrpc"),
        serverAPIsTest   (serverAPIs,   "serverAPIs",    "sapi"),
        joinAPIsTest     (joinAPIs,     "joinAPIs",      "join",
                              sizeof(JoinAPIsTest), newJoinAPIsTest),
        selectTest       (doSelectTest, "select",        "sel"),
        piTest           (doPiTest,     "playerInvite",  "pi"),
        adhocTest        (doAdhocTest,  "adhoc",         "adhoc")

    {
        #ifdef _WIN32
            iamServer = false;
        #endif

        #if !defined(_LINUX) || defined(_VN_RPC_DEVICE)
            cStatInit      |= VNGT_ST_USB;
            cStatInitLogin |= VNGT_ST_USB;
        #endif


        all.push_back (&vng_msgsTest);
        all.push_back (&loopbackRPCsTest);
        all.push_back (&serverAPIsTest);
        all.push_back (&joinAPIsTest);
        all.push_back (&selectTest);
        all.push_back (&piTest);
        all.push_back (&adhocTest);

        for (size_t i = 0;   i < all.size();  ++i) {
            Test *test = all[i];
            byName [test->name.c_str()] = test;
            byShortName [test->shortName.c_str()] = test;
        }

        // To run a subset of all by default,
        // init toRun to that subset.

        toRun = all;
    }

    void initCTestEnv (CTestEnv& cTestEnv)
    {
        cTestEnv.conf              = confAsString.c_str();
        cTestEnv.iamClient         = iamClient;
        cTestEnv.iamServer         = iamServer;
        cTestEnv.iamGameHost       = iamGameHost;
        cTestEnv.iamGameJoiner     = iamGameJoiner;
        cTestEnv.host              = host;
        cTestEnv.join              = join;
        cTestEnv.cStatInit         = cStatInit;
        cTestEnv.cStatInitLogin    = cStatInitLogin;
        cTestEnv.maxFailBeforeQuit = maxFailBeforeQuit;
        cTestEnv.iterations        = iterations;
        cTestEnv.timeout           = timeout;
        cTestEnv.serverPort        = serverPort;
        cTestEnv.server            = server.c_str();
        cTestEnv.h_loginName       = h_loginName.c_str();
        cTestEnv.j_loginName       = j_loginName.c_str();
        cTestEnv.h_passwd          = h_passwd.c_str();
        cTestEnv.j_passwd          = j_passwd.c_str();
        if (!cur)
            cTestEnv.cur = NULL;
        else {
            CTestProxy *cTestProxy = static_cast<CTestProxy*>(cur);
            cTestEnv.cur = cTestProxy->cTest;
        }
    }
};


int checkConnectStatus (Test&      test,
                        uint32_t   expected,
                        uint32_t   dontCare = 0,
                        bool       showCorrect = VNGT_DEF_SHOW_CORRECT);


void showTestPassFail (Test& test);
void showTests (Tests tests);
void showTestName (const string& test_name);
void showTestName (Test& test);

void show (Test& test, int ec);
void show (Test& test, Player& player, const char *api, int ec);
void show (Test& test, Player& player, const string& api_msg, int ec);
void show (Test& test, Player& player, ostringstream& api_msg, int ec);
void show (Test& test, const char *api, int ec);
void show (Test& test, const string& api_msg, int ec);
void show (Test& test, ostringstream& api_msg, int ec);

void show (Test& test, Player& player, const char     *msg);
void show (Test& test, Player& player, const string&   msg);
void show (Test& test, Player& player, ostringstream&  msg);
inline
void show (Test& test, Player& player, const char     *msg, const char *more_msg)
{
    show (test, player, string(msg) + more_msg);
}

void show (Test& test, const char     *msg);
void show (Test& test, const string&   msg);
void show (Test& test, ostringstream&  msg);
inline
void show (Test& test, const char     *msg, const char *more_msg)
{
    show (test, string(msg) + more_msg);
}

inline void show (const char     *msg)   {showTM (NULL, msg);}
inline void show (const string&   msg)   {showTM (NULL, msg.c_str());}
inline void show (ostringstream&  msg)   {showTM (NULL, msg.str().c_str());}
inline void show (const char     *msg, const char *more_msg)
{
    show (string(msg) + more_msg);
}

extern TestEnv *curTestEnv;


#endif  // __cplusplus

#endif  // __VNGT_H__


//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//


#include "vngt.h"



static _vng_register_game_arg  registeredGame;

static VNGUserId  fakeUserId = 1911;

static bool       logged_in  = false;

static bool joinRequestEventReceived = false;
static bool joinInviteEventReceived  = false;

static bool exit_servRPC_requested   = false;
static bool exit_test_requested      = false;
static bool wait_before_exit_servRPC = false; 

static int logins;

static _VNGVnId   auto_created_vn_id;


static
_SHRThreadRT  _SHRThreadCC  servRPCs(CTestEnv *tests);

static
_SHRThreadRT  _SHRThreadCC  getEvents(CTestEnv *tests);

static
_SHRThreadRT  _SHRThreadCC  gameHost(CTestEnv *tests);

static
_SHRThreadRT  _SHRThreadCC  gameJoiner(CTestEnv *tests);

static
int32_t rpcLogin (VN            *vn,
                  VNMsgHdr      *hdr,
                  const void    *args,
                  size_t         args_len,
                  void          *ret,
                  size_t        *ret_len);
static
int32_t rpcLogout (VN          *vn,
                   VNMsgHdr    *hdr,
                   const void  *args,
                   size_t       args_len,
                   void        *ret,
                   size_t      *ret_len);

static
int32_t rpcRegisterGame (VN          *vn,
                         VNMsgHdr    *hdr,
                         const void  *args,
                         size_t       args_len,
                         void        *ret,
                         size_t      *ret_len);

static
int32_t rpcLetter (VN          *vn,
                   VNMsgHdr    *hdr,
                   const void  *args,
                   size_t       args_len,
                   void        *ret,
                   size_t      *ret_len);

static
int32_t rpcGetBuddyStatus (VN          *vn,
                           VNMsgHdr    *hdr,
                           const void  *args,
                           size_t       args_len,
                           void        *ret,
                           size_t      *ret_len);

static
VNGErrCode VNGT_RegisterGame (VNG          *vng,
                              VNGGameInfo  *info,
                              char         *comments,
                              VNGTimeout    timeout);

static
VNGErrCode VNGT_GetBuddyStatus (VNG            *vng,
                                VNGBuddyStatus *buddyStatus,
                                uint32_t        nBuddies,
                                VNGTimeout      timeout);


static
bool isAutoCreated (VN* vn)
{
    return vn->id == auto_created_vn_id;
}



/*****************************************************

Notes:

    - It doesn't make sense to have more than
      one events loop and one serve RPCs loop in a
      process even if server, game host and game joiner
      are all being simulated in the process.
      We can only have one VNG.  getEvents/serverRPC
      return events/RPCs for any VN.
      
   -  Only makes sence to start serve RPCs loop if
      the process is simulating a node that serves RPCs.
      So if joiner doesn't serve RPCs but server
      and game host serve RPCs, create servRPCs
      thread if server or game host.
      .

Main:

    VNG_INIT

    create GetEvents Thread

    create servRPCsThread

    if game host,
        create gameHostThread

    if game joiner,
        create gameJoinerThread

    if gameHostThread
        wait for gameHostThread to finish

    if gameJoinerThread
        wait for gameJoinerThread to finish

    VNG_Fini

    if gevThread
        wait for gevThread

    if servRPCsThread
        wait for servRPCsThread

    return result




servrRPCs thread:

    if server,
        create vn any

        Register RPCs appropriate to Server
        (rpcs for game host/joiner registered elsewhere)

        Do VN_Listen to create new nets on sconnect

    serve RPCs

    if iamServer

        VNG_DeleteVN (vn_any);


game host thread:

    create joinable game VN

    send VNId to server via VNG_RegisterGame

    Do actions indicated in hostCmds.  Default:
        wait for join request
        accept join request
        wait for no more players
        exit

    VNG_DeleteVN

joiner thread:

    
    Get VNId via VNG_GetBuddyStatus  (Server will return the game host)

    Do actions indicated in joinerCmds.  Default:
        VNG_JoinVN
        sleep for a while
        VNG_LeaveVN
        exit
    



*****************************************************/

int joinAPIs (CTestEnv *tests)
{

    int         rv = 0;
    VNGErrCode  ec;
    VNG         vng;
    _SHRThread  servRPCsThread = 0;
    _SHRThread  gameHostThread = 0;
    _SHRThread  gameJoinerThread = 0;
    _SHRThread  gevThread = 0;

    JoinAPIsTest  *test = (JoinAPIsTest*) tests->cur;

    showTestName (test->name);

    registeredGame.info.vnId.deviceId  = _VNGT_GUID_INVALID;
    registeredGame.info.vnId.netId = _VNGT_NET_INVALID;

    trace(INFO, MISC, "This is vngt_join.c %d", 11);

    if (checkConnectStatus(test->name, &vng, VNGT_ST_FINI, 0, VNGT_DEF_SHOW_CORRECT)) {
        rv = -1;
        goto end;
    }

    if (tests->iamServer) {
        if ((ec = VNG_InitServer(&vng, NULL, VNG_DEF_SERVER_PORT, NULL))) {
            fprintf (stdout, "VNG joinAPIs Test: VNG_InitServer Failed: %d\n", ec);
            rv = -1;
            goto end;
        }
        fprintf (stdout, "\nVNG joinAPIs Test: VNG_InitServer successful.\n");
    }
    else {
        if ((ec = VNG_Init(&vng, NULL))) {
            fprintf (stdout, "VNG joinAPIs Test: VNG_Init Failed: %d\n", ec);
            rv = -1;
            goto end;
        }
        fprintf (stdout, "\nVNG joinAPIs Test: VNG_Init successful.\n");
    }

    test->vng = &vng;

    if (checkConnectStatus(test->name, &vng, tests->cStatInit, 0, VNGT_DEF_SHOW_CORRECT)) {
        rv = -1;
        goto Fini;
    }

    if ((rv = _SHR_thread_create (&gevThread, NULL,
                                  (_SHRThreadFunc) getEvents, tests))) {
        fprintf (stdout, "VNG joinAPIs Test: "
                         "Failed to create getEvents thread\n");
        gevThread = 0;
        rv = -1;
        goto Fini;
    }

    if (tests->iamServer) {
        // currently no rpc's registered if only client
        if ((rv = _SHR_thread_create (&servRPCsThread, NULL,
                                        (_SHRThreadFunc) servRPCs, tests))) {
            fprintf (stdout, "VNG joinAPIs Test: "
                                "Failed to create servRPCs thread\n");
            servRPCsThread = 0;
            rv = -1;
            goto Fini;
        }
    }

    if (tests->iamGameHost) {
        if ((rv=_SHR_thread_create (&gameHostThread, NULL,
                                     (_SHRThreadFunc) gameHost,
                                     tests))) {

            fprintf (stdout, "VNG joinAPIs Test: "
                             "Failed to create game host thread\n");
            rv = -1;
            gameHostThread = 0;
            goto Fini;
        }
    }


    if (tests->iamGameJoiner) {
        if ((rv=_SHR_thread_create (&gameJoinerThread, NULL,
                                     (_SHRThreadFunc) gameJoiner,
                                     tests))) {

            fprintf (stdout, "VNG joinAPIs Test: "
                             "Failed to create game joiner thread\n");
            rv = -1;
            gameJoinerThread = 0;
        }
    }

    if (gameHostThread) {
        if (_SHR_thread_join (gameHostThread, NULL, 300000)) {
            fprintf (stdout, "VNG joinAPIs Test: "
                             "_SHR_thread_join gameHostThread Failed.\n");
            rv = -1;
        }
        else {
            gameHostThread = 0;
            rv = test->gameHostResult;
        }
    }

    if (gameJoinerThread) {
        if (_SHR_thread_join (gameJoinerThread, NULL, 300000)) {
            fprintf (stdout, "VNG joinAPIs Test: "
                             "_SHR_thread_join gameJoinerThread Failed.\n");
            rv = -1;
        }
        else {
            gameJoinerThread = 0;
            rv = test->gameJoinerResult;
        }
    }

    if (tests->iamServer && !tests->iamClient && servRPCsThread) {
        if (_SHR_thread_join (servRPCsThread, NULL, 300000)) {
            fprintf (stdout, "VNG joinAPIs Test: "
                             "1st _SHR_thread_join servRPCsThread Timed out.\n");
            rv =-1;
        }
        else if (!rv) {
            servRPCsThread = 0;
            rv = test->servRPCsResult;
        }
    }

Fini:
    fprintf (stdout, "\nVNG joinAPIs Test:  Starting VNG_Fini.\n");

    if ((ec = VNG_Fini(&vng))) {
        fprintf (stdout, "\nVNG joinAPIs Test:  VNG_Fini Failed: %d\n", ec);
        rv = -1;
    }
    else {
        fprintf (stdout, "\nVNG joinAPIs Test:  VNG_Fini success\n");
        if (!rv)
            rv = checkConnectStatus(test->name, &vng, VNGT_ST_FINI, 0, VNGT_DEF_SHOW_CORRECT);
    }
    fflush (stdout);

    if (gevThread) {
        if (_SHR_thread_join (gevThread, NULL, 10000)) {
            fprintf (stdout, "VNG joinAPIs Test: "
                             "_SHR_thread_join gevThread Failed.\n");
            rv =-1;
        }
        else if (!rv) {
            rv = test->gevThreadResult;
        }
    }

    if (servRPCsThread) {
        if (_SHR_thread_join (servRPCsThread, NULL, 10000)) {
            fprintf (stdout, "VNG joinAPIs Test: "
                             "_SHR_thread_join servRPCsThread Failed.\n");
            rv =-1;
        }
        else if (!rv) {
            rv = test->servRPCsResult;
        }
    }
end:
    return rv;
}


static
_SHRThreadRT  _SHRThreadCC  servRPCs(CTestEnv *tests)
{
    JoinAPIsTest  *test = (JoinAPIsTest*) tests->cur;
    VNG           *vng  = test->vng;

    VNGErrCode  ec;
    VN          vn_any;
    VNPolicies  policies = VN_ANY | VN_AUTO_ACCEPT_JOIN;

    test->servRPCsResult = 0;

    if (tests->iamServer) {

        // Create vn_any

        if ((ec = VNG_NewVN(vng, &vn_any, VN_DEFAULT_CLASS, VN_DOMAIN_LOCAL, policies))) {
            fprintf (stdout, "VNG joinAPIs Test: "
                            "servRPCs: VNG_NewVN Failed: %d\n", ec);
            goto end;
        }

        // Register RPCs

        if ((ec = VN_RegisterRPCService (&vn_any, _VNG_LOGIN, rpcLogin))) {
            fprintf (stdout, "VNG joinAPIs Test: "
                            "servRPCs: _VNG_LOGIN Failed: %d\n", ec);
            goto end;
        }
        if (( ec = VN_RegisterRPCService (&vn_any, _VNG_LOGOUT,
                                                rpcLogout))) {
            fprintf (stdout, "VNG joinAPIs Test: "
                            "servRPCs: _VNG_LOGOUT Failed: %d\n", ec);
            goto end;
        }

        if (( ec = VN_RegisterRPCService (&vn_any, _VNG_REGISTER_GAME,
                                            rpcRegisterGame))) {
            fprintf (stdout, "VNG joinAPIs Test: "
                            "servRPCs: _VNGT_GET_VNID Failed: %d\n", ec);
            goto end;
        }

        if (( ec = VN_RegisterRPCService (&vn_any, _VNGT_GET_BUDDY_STATUS,
                                            rpcGetBuddyStatus))) {
            fprintf (stdout, "VNG joinAPIs Test: "
                            "servRPCs: _VNGT_PUT_VNID Failed: %d\n", ec);
            goto end;
        }

        if ((ec = VN_RegisterRPCService (&vn_any, 'x', rpcLetter))) {
            showTF (test->name, "VN_RegisterRPCService rpcLetter x", ec);
            goto end;
        }

        // Create new nets on sconnect

        if ((ec = VN_Listen (&vn_any, true))) {
            fprintf (stdout, "VNG joinAPIs Test: "
                            "servRPCs: VN_Listen Failed: %d\n", ec);
            goto cleanup;
        }

    }


    // serve RPCs

    fprintf (stdout, "VNG joinAPIs Test: "
                     "servRPCs: Waiting for RPCs\n");

    while (!(ec=VNG_ServeRPC(vng, VNG_WAIT))) {
        if (exit_servRPC_requested || test->servRPCsResult)
            break;
    }

    if (wait_before_exit_servRPC) {
        _SHR_thread_sleep(1000);
    }

    fprintf (stdout, "VNG joinAPIs Test: "
                     "servRPCs: exited serve loop due to %sec: %d\n",
                     (exit_servRPC_requested ? "exit_servRPC_requested with "
                                             : ""), ec);

cleanup:    // cleanup on exit

    if (ec != VNGERR_FINI
          && tests->iamServer
          && (ec = VNG_DeleteVN(vng, &vn_any))) {

            fprintf (stdout, "VNG joinAPIs Test: "
                        "servRPCs: VNG_DeleteVN (vn_any) Failed: %d\n", ec);
            test->servRPCsResult = -1;
    }

    fprintf (stdout, "VNG joinAPIs Test: "
                     "servRPCs: Done waiting for RPCs\n");

end:
    exit_test_requested = true;
    return (_SHRThreadRT) 0;
}


static
_SHRThreadRT  _SHRThreadCC  getEvents(CTestEnv *tests)
{
    JoinAPIsTest  *test = (JoinAPIsTest*) tests->cur;
    VNG           *vng  = test->vng;


    VNGErrCode ec;
    VNGEvent   ev;
    VN        *vn;
    VNId       vnId;
    VNState    state;
    VNMember   self;
    VNMember   owner;
    VNMember   peer = 0;
    char       caught[] = "Caught event: ";
    char      *event = NULL;

    test->gevThreadResult = 0;

    fprintf (stdout, "\nVNG joinAPIs Test: Waiting for game events.\n");

    while (!(ec=VNG_GetEvent(vng, &ev, VNG_WAIT))) {
        switch (ev.eid) {

            case VNG_EVENT_LOGOUT:
                event = "VNG_EVENT_LOGOUT";
                fprintf (stdout, "\n%s%s\n", caught, event);
                break;

            case VNG_EVENT_RECV_NOTIFY:
                event = "VNG_EVENT_RECV_NOTIFY";
                fprintf (stdout, "\n%s%s\n", caught, event);
                joinInviteEventReceived = true;
                break;

            case VNG_EVENT_PEER_STATUS:
                vn = ev.evt.peerStatus.vn;
                peer = ev.evt.peerStatus.memb;
                event = "VNG_EVENT_PEER_STATUS";
                state = VN_State (vn);
                self = VN_Self (vn);
                owner = VN_Owner (vn);

                if ((ec = VN_GetVNId(vn, &vnId))) {
                    if (!(ec == VNGERR_INVALID_VN && state == VN_EXITED)) {
                        fprintf (stdout, "VNG joinAPIs Test: %s%s: "
                                        "  vn: %p"
                                        " VN_GetVNId failed: %d\n",
                                        caught, event, vn, ec);
                    }
                }

                fprintf (stdout, "\n%s%s"
                            "  vn: %p"
                            "  netid: 0x%x"
                            "  vn state: %d"
                            "  self: %d"
                            "  owner: %d"
                            "  peer: %d\n",
                                caught, event, vn,
                                vnId.netId,
                                state,
                                self,
                                owner,
                                peer);

                if (state == VN_EXITED) {
                    bool ac = isAutoCreated(vn);
                    char *ac_str = ac ? "  auto create" : "  not auto create";
                    if ((ec = VNG_DeleteVN (vng, vn)) && ac) {
                        // only an error if was auto create
                        fprintf (stdout, "%s%s VNG_DeleteVN %p %s Failed %d\n",
                            caught, event, vn, ac_str, ec);
                        test->gevThreadResult = -1;
                        break;
                    }
                    fprintf (stdout, "%s%s VNG_DeleteVN %p %s  ec: %d\n",
                        caught, event, vn, ac_str, ec);
                }
                break;

            case VNG_EVENT_JOIN_REQUEST:
                vnId = ev.evt.joinRequest.vnId;
                event = "VNG_EVENT_JOIN_REQUEST";
                fprintf (stdout, "\n%s%s  "
                       "chipId: 0x%x  deviceType: %d  netid: 0x%x\n",
                        caught, event,
                        deviceIdToChipId(vnId.deviceId),
                        deviceIdToDeviceType(vnId.deviceId),
                        vnId.netId);
                joinRequestEventReceived = true;
                break;

            default:
                fprintf (stdout, "\n%s%d\n", caught, ev.eid);
                break;
        }
        fflush (stdout);
}

    fprintf (stdout, "\nDone waiting for game events.\n");

    return (_SHRThreadRT) 0;
}







static
_SHRThreadRT  _SHRThreadCC  gameHost (CTestEnv *tests)
{
    JoinAPIsTest  *test = (JoinAPIsTest*) tests->cur;
    VNG           *vng  = test->vng;

    VN             vn_game;
    VNId           vnId_game;
    VNPolicies     autoAcceptPolicies = VN_DEFAULT_POLICY  |   VN_AUTO_ACCEPT_JOIN;
    VNPolicies     needAcceptPolicies = VN_DEFAULT_POLICY  &  ~VN_AUTO_ACCEPT_JOIN;
    VNPolicies     defaultPolicies    = VN_DEFAULT_POLICY;
    VNPolicies     policies;
    bool           autoAcceptJoin = false;
    bool           requireAcceptJoin = true;
    bool           gotJoinRequestEvent = false;
    VNGTimeout     joinRequestTimeout;
    VNGGameInfo    info;
    char           comments[]  = "No comment";

    VN            *vn_hosted;
    VNGUserInfo    userInfo;
    char           joinReason[_VNGT_MAX_JOIN_REQ_MSG];
    uint64_t       requestId;


    VNGErrCode     ec;
    int            res = 0;
    size_t         i;

    size_t           mi;
    VNServiceTypeSet ready;
    const char*      role = "gameHost";
    char             rcvdMsg[4];
    size_t           rcvdMsgLen = sizeof rcvdMsg;
    VNMsgHdr         hdr;
    VNMember         vn_game_self;
    VNMember         members[16];
    uint32_t         maxMembers = (sizeof members)/(sizeof members[0]);
    VNMember         peer = VN_MEMBER_INVALID;
    uint32_t         nMemb;
    char             msg[4];


    const char  *serverName   = tests->server;
    VNGPort      serverPort   = tests->serverPort;
    const char  *loginName    = tests->h_loginName;
    const char  *passwd       = tests->h_passwd;
    VNGTimeout   timeout      = tests->timeout;
    const char  *cmds         = test->hostCmds;

    fprintf (stdout, "\nVNG joinAPIs Test: gameHost: start VNG_Login.\n");
    if ((ec = VNG_Login (vng, serverName, serverPort,
                             loginName, passwd,
                             timeout))) {
        fprintf (stdout, "VNG joinAPIs Test: gameHost :"
                         "VNG_Login Failed: %d\n", ec);
        res = -1;
        goto end;
    }
    fprintf (stdout, "\nVNG joinAPIs Test: gameHost: VNG_Login complete.\n");
    logged_in = true;

    if (checkConnectStatus(test->name, vng, tests->cStatInitLogin, 0, VNGT_DEF_SHOW_CORRECT)) {
        res = -1;
        goto Fini;
    }

    // Create vn for client to join

    if (autoAcceptJoin)
        policies = autoAcceptPolicies;
    else if (requireAcceptJoin)
        policies = needAcceptPolicies;
    else
        policies = defaultPolicies;

    if ((ec = VNG_NewVN(vng, &vn_game, VN_CLASS_2, VN_DOMAIN_INFRA, policies))) {
        fprintf (stdout, "VNG joinAPIs Test: gameHost: "
                         "VNG_NewVN Failed: %d\n", ec);
        res = -1;
        goto end;
    }
        
    if ((ec = VN_GetVNId(&vn_game, &vnId_game))) {
        fprintf (stdout, "VNG joinAPIs Test: gameHost: "
                         "VN_GetVNId Failed: %d\n", ec);
        res = -1;
        goto Fini;
    }
    fprintf (stdout, "\nVNG joinAPIs Test: gameHost: "
                     "VN for joining created.  vn: %p\n",
                     &vn_game);
    fprintf (stdout, "\nVNG joinAPIs Test: gameHost:  "
                     "chipId: 0x%x  deviceType: %d   netId: 0x%x\n",
                        deviceIdToChipId(vnId_game.deviceId),
                        deviceIdToDeviceType(vnId_game.deviceId),
                        vnId_game.netId);

    fflush(stdout);


    // send VNId to server via VNG_RegisterGame

    info.vnId = vnId_game;
    info.owner = fakeUserId; // VNG_MyUserId(vng);
    info.gameId = POKEMON;
    info.titleId = POKEMON_LEAFGREEN;
    info.accessControl = VNG_GAME_PRIVATE;  // should be called viewability control
    info.netZone = 0;      // server will decide this value
    info.totalSlots = 2;
    info.buddySlots = 0;   // nothing reserved for buddies
    info.maxLatency = 500; // 500ms RTT 
    info.attrCount = 1;
    strncpy(info.keyword, "World Champion Cup", VNG_GAME_KEYWORD_LEN);
    info.keyword[VNG_GAME_KEYWORD_LEN-1] = '\0';
    info.gameAttr[POKEMON_SUBGAME] = COLLOSSEUM;

    if ((ec = VNGT_RegisterGame (vng, &info, comments, timeout))) {
        fprintf (stdout, "VNG joinAPIs Test: gameHost: "
                         "VNGT_RegisterGame Failed: %d\n", ec);
        res = -1;
        goto Fini;
    }
    fprintf (stdout, "\nVNG joinAPIs Test: gameHost: game registered.\n");

    // Do actions indicated in hostCmds.

    cmds = test->hostCmds;
    for  (i=0;  cmds && i < strlen(cmds); ++i) {
        char cmd = cmds[i];
        switch (cmd) {
            default:
                fprintf (stdout, "\nVNG joinAPIs Test: %s: "
                                 "Unrecognized test command: %c  i: %u  cmds: %s\n",
                                 role, cmd, i, cmds);
                break;
            case '0':
                cmds = NULL;
                fprintf (stdout, "\nVNG joinAPIs Test: %s:  "
                                    "Exiting on command.\n", role);
                break;
            case 'z':
                _SHR_thread_sleep(_VNGT_BIDE_TIME);
                break;
            case '1':
                if (VN_State(&vn_game) != VN_EXITED) {
                    fprintf (stdout, "\nVNG joinAPIs Test: gameHost: "
                                    "Leaving on commeand.  vn: %p\n", &vn_game);
                    ec = VNG_LeaveVN (vng, &vn_game);
                    fprintf (stdout, "\nVNG joinAPIs Test: gameHost: "
                                    "VNG_LeaveVN: returned: %d\n", ec);
                }
                else {
                    fprintf (stdout, "\nVNG joinAPIs Test: gameHost: "
                                    "Not leaving on commeand, because VN_EXITED\n");
                }
                break;
            case '2':
                if (VN_State(&vn_game) != VN_EXITED) {
                    fprintf (stdout, "\nVNG joinAPIs Test:   gameHost:  "
                                    "Deleting on command.  "
                                    "VNG_DeleteVN (vn_game:%p).\n", &vn_game);
                    if ((ec = VNG_DeleteVN (vng, &vn_game))) {
                        fprintf (stdout, "VNG joinAPIs Test:  gameHost:  "
                                        "VNG_DeleteVN Failed: %d\n", ec);
                        res = -1;
                        goto Fini;
                    }
                }
                else {
                    fprintf (stdout, "\nVNG joinAPIs Test: gameHost: "
                                    "Not deleting VN on commeand, because VN_EXITED\n");
                }
                break;

            case 'X':
                if (ready & VN_SERVICE_TYPE_RELIABLE_MSG) {
                    if ((ec = VN_RecvMsg (&vn_game,
                                          VN_MEMBER_ANY, VN_SERVICE_TAG_ANY,
                                           rcvdMsg, &rcvdMsgLen, &hdr, timeout))) {
                        fprintf (stdout, "VNG joinAPIs Test:  %s:  "
                                        "cmd X VN_RecvMsg Failed: %d\n",
                                        role, ec);
                        res = -1;
                        goto Fini;
                    }
                }
                else {
                    fprintf (stdout, "\nVNG joinAPIs Test: %s: "
                                     "cmd X no ready service channel.  ready: %u\n",
                                     role, ready);
                    res = -1;
                    goto Fini;
                }
                
                if ((ec = VN_SendMsg (&vn_game,
                                      hdr.sender, hdr.serviceTag,
                                      rcvdMsg, sizeof rcvdMsg,
                                      VN_DEFAULT_ATTR, VNG_NOWAIT))) {
                    fprintf (stdout, "\nVNG joinAPIs Test: %s: "
                                     "cmd %c VN_SendMsg error.  ec: %d\n",
                                     role, cmd, ec);
                    res = -1;
                    goto Fini;
                }
                fprintf (stdout, "\nVNG joinAPIs Test: %s: "
                                 "cmd X VN_SendMsg successful: %c\n",
                                  role, rcvdMsg[0]);
                break;

            case 'm':
            case 'n':
                peer = VN_MEMBER_INVALID;
                nMemb =  VN_GetMembers (&vn_game, members, maxMembers);
                msg[0] = cmd;
                msg[1] = '\0';
                vn_game_self = VN_Self(&vn_game);
                for (mi = 0; mi < nMemb; ++mi) {
                    if (members[mi] != vn_game_self) {
                        peer = members[mi];
                        break;
                    }
                }
                if (peer == VN_MEMBER_INVALID) {
                    fprintf (stdout, "\nVNG joinAPIs Test: %s: "
                                     "cmd %c no other member.  nMembs: %u\n",
                                     role, cmd, nMemb);
                    res = -1;
                    goto Fini;
                }
                if ((ec = VN_SendMsg (&vn_game, peer, cmd, msg, sizeof msg,
                                      VN_DEFAULT_ATTR, VNG_NOWAIT))) {
                    fprintf (stdout, "\nVNG joinAPIs Test: %s: "
                                     "cmd %c VN_SendMsg error.  ec: %d\n",
                                     role, cmd, ec);
                    res = -1;
                    goto Fini;
                }
                fprintf (stdout, "\nVNG joinAPIs Test: %s: "
                                 "cmd %c VN_SendMsg successful: %c\n",
                                  role, cmd, msg[0]);
                break;

            case 'M':
            case 'N':
                if ((ec = VN_RecvMsg (&vn_game,
                                        VN_MEMBER_ANY, tolower(cmd),
                                        rcvdMsg, &rcvdMsgLen, &hdr, timeout))) {
                    fprintf (stdout, "VNG joinAPIs Test:  %s:  "
                                    "cmd %c VN_RecvMsg Failed: %d\n", role, cmd, ec);
                    res = -1;
                    goto Fini;
                }
                fprintf (stdout, "\nVNG joinAPIs Test: %s: "
                                 "cmd %c VN_RecvMsg successful: %c\n",
                                  role, cmd, rcvdMsg[0]);
                break;

            case '*':
                fprintf (stdout, "\nVNG joinAPIs Test: %s:  "
                                    "Waiting for msg on any svc channel for any memb.\n",
                                    role);
                fflush(stdout);
                if ((ec = VN_Select (&vn_game,
                                     VN_SERVICE_TYPE_ANY_MASK,
                                     &ready,   // out 
                                     VN_MEMBER_ANY,
                                     timeout))) {
                    fprintf (stdout, "VNG joinAPIs Test: %s: "
                                    "VN_Select Failed: %d\n", role, ec);
                    res = -1;
                    goto Fini;
                }
                fprintf (stdout, "\nVNG joinAPIs Test: %s: "
                                 "VN_Select says ready channel: %u\n",
                                  role, ready);
                break;


            case 'a':
                fprintf (stdout, "\nVNG joinAPIs Test: gameHost:  "
                                    "Waiting for join request Event.\n");
                gotJoinRequestEvent = false;
                while (1) {
                    if (joinRequestEventReceived) {
                        fprintf (stdout, "\nVNG joinAPIs Test: gameHost:  "
                                            "Got join request Event.\n");
                        gotJoinRequestEvent = true;
                        break;
                    }
                    _SHR_thread_sleep(1000);
                }
                break;
            case 'b':
                fprintf (stdout, "\nVNG joinAPIs Test: gameHost:  "
                                    "waiting for join request.\n");
                fflush(stdout);
                strcpy (userInfo.login, "No login");
                strcpy (userInfo.nickname, "No nickname");
                userInfo.uid = 0;
                joinRequestTimeout = gotJoinRequestEvent ? VNG_NOWAIT : timeout;
                if ((ec = VNG_GetJoinRequest (vng,
                                              vnId_game,    // in
                                              &vn_hosted,   // out 
                                              &requestId,
                                              &userInfo,
                                              joinReason,
                                              sizeof joinReason,
                                              joinRequestTimeout))) {
                    fprintf (stdout, "VNG joinAPIs Test: gameHost: "
                                    "VNG_GetJoinRequest Failed: %d\n", ec);
                    res = -1;
                    goto Fini;
                }
                fprintf (stdout, "\nVNG joinAPIs Test: gameHost: "
                                 "got join request: %s: %s\n",
                                  userInfo.nickname, joinReason);
                break;
            case 'c':
                ec = VNG_AcceptJoinRequest (vng, requestId);
                fprintf (stdout, "\nVNG joinAPIs Test: gameHost: "
                                 "VNG_AcceptJoinRequest: returned: %d\n", ec);
                break;
            case 'd':
                ec = VNG_RejectJoinRequest (vng, requestId,
                                 "Because you are too good for this game!");
                fprintf (stdout, "\nVNG joinAPIs Test: gameHost: "
                                 "VNG_RejectJoinRequest: returned: %d\n", ec);
                break;
        }
    }


Fini:
    if (VN_State(&vn_game) != VN_EXITED) {
        fprintf (stdout, "\nVNG joinAPIs Test:   gameHost:  "
                         "start VNG_DeleteVN (vn_game).\n");
        if ((ec = VNG_DeleteVN (vng, &vn_game))) {
            fprintf (stdout, "VNG joinAPIs Test:  gameHost:  "
                             "VNG_DeleteVN Failed: %d\n", ec);
            res = -1;
        }
    }
    
    if (logged_in) {
        if (!res)
            res = checkConnectStatus(test->name, vng, tests->cStatInitLogin,
                                      0, VNGT_DEF_SHOW_CORRECT);
        fprintf (stdout, "\nVNG joinAPIs Test: gameHost: start VNG_Logout.\n");
        if ((ec = VNG_Logout (vng, timeout))) {
            fprintf (stdout, "VNG joinAPIs Test:  gameHost:  "
                            "VNG_Logout Failed: %d\n", ec);
            res = -1;
        }
    }

    if (!res)
        res = checkConnectStatus(test->name, vng, tests->cStatInit,
                                  0, VNGT_DEF_SHOW_CORRECT);

end:
    test->gameHostResult =res;
    return (_SHRThreadRT) 0;
}


static
_SHRThreadRT  _SHRThreadCC  gameJoiner (CTestEnv *tests)
{
    JoinAPIsTest  *test = (JoinAPIsTest*) tests->cur;
    VNG           *vng  = test->vng;

    char           denyReason [_VNGT_MAX_JOIN_DNY_MSG];
    VN             vn_j;
    VNId           vnId_game;
    VNGBuddyStatus bs;

    VNGErrCode     ec;
    int            res = 0;
    size_t         i;

    size_t           mi;
    VNServiceTypeSet ready;
    const char*      role = "gameJoiner";
    char             rcvdMsg[4];
    size_t           rcvdMsgLen = sizeof rcvdMsg;
    VNMsgHdr         hdr;
    VNMember         vn_j_self = VN_Self(&vn_j);
    VNMember         members[16];
    uint32_t         maxMembers = (sizeof members)/(sizeof members[0]);
    VNMember         peer = VN_MEMBER_INVALID;
    uint32_t         nMemb;
    char             msg[4];


    const char *serverName   = tests->server;
    VNGPort     serverPort   = tests->serverPort;
    const char *loginName    = tests->j_loginName;
    const char *passwd       = tests->j_passwd;
    VNGTimeout  timeout      = tests->timeout;
    const char *cmds         = test->joinerCmds;

    bool    joiner_logged_in = false;
    bool    joined           = false;



    vnId_game.deviceId  = _VNGT_GUID_INVALID;
    vnId_game.netId = _VNGT_NET_INVALID;


    // If i am not also game host, login.  else wait for login

    if (tests->iamGameHost) {
        fprintf (stdout, "\nVNG joinAPIs Test: gameJoiner: wait for gameHost login.\n");
        for (i=0; i < 100 && !logged_in;  ++i) {
            _SHR_thread_sleep(100);
        }
        if (!logged_in) {
            fprintf (stdout, "VNG joinAPIs Test: gameJoiner: "
                             "Timed out waiting for gameHost to login\n");
            res = -1;
            goto end;
        }
        fprintf (stdout, "\nVNG joinAPIs Test: gameJoiner: detected login complete.\n");
    }
    else {
        fprintf (stdout, "\nVNG joinAPIs Test: gameJoiner: start VNG_Login.\n");
        if ((ec = VNG_Login (vng, serverName, serverPort,
                                loginName, passwd,
                                timeout))) {
            fprintf (stdout, "VNG joinAPIs Test: gameJoiner: "
                             "VNG_Login Failed: %d\n", ec);
            res = -1;
            goto Fini;
        }
        fprintf (stdout, "\nVNG joinAPIs Test: gameJoiner: VNG_Login complete.");
        logged_in = true;
        joiner_logged_in = true;

        if (checkConnectStatus(test->name, vng, tests->cStatInitLogin, 0, VNGT_DEF_SHOW_CORRECT)) {
            res = -1;
            goto Fini;
        }

    }

    fflush(stdout);
    _SHR_thread_sleep(1000);


    // Do VNG_GetBuddyStatus to get game VNId

    bs.uid = fakeUserId;

    if ((ec = VNGT_GetBuddyStatus (vng, &bs, 1, timeout))) {
        fprintf (stdout, "\nVNG joinAPIs Test: gameJoiner:  "
                            "VNG_GetBuddyStatus Failed: %d\n", ec);
        res = -1;
        goto Fini;
    }
    fprintf (stdout, "\nVNG joinAPIs Test: gameJoiner:  "
                     "From buddy status:   "
                     "chipId: 0x%x  deviceType: %d  netId: 0x%x\n",
                     deviceIdToChipId(bs.vnId.deviceId),
                     deviceIdToDeviceType(bs.vnId.deviceId),
                     bs.vnId.netId);

    vnId_game = bs.vnId;

    if (   vnId_game.deviceId == _VNGT_GUID_INVALID
        || vnId_game.netId == _VNGT_NET_INVALID) {
        fprintf (stdout, "\nVNG joinAPIs Test: gameJoiner:  "
                         "Exiting because vnId_game not avaiable.\n");
        res = -1;
        goto Fini;
    }

    // Do actions indicated in hostCmds.

    cmds = test->joinerCmds;
    for  (i=0;  cmds && i < strlen(cmds); ++i) {
        char cmd = cmds[i];
        switch (cmd) {
            default:
                fprintf (stdout, "\nVNG joinAPIs Test: %s: "
                                 "Unrecognized test command: %c  i: %u  cmds: %s\n",
                                 role, cmd, i, cmds);
                break;
            case '0':
                cmds = NULL;
                fprintf (stdout, "\nVNG joinAPIs Test: %s:  "
                                    "Exiting on command.\n", role);
                break;
            case 'z':
                _SHR_thread_sleep(_VNGT_BIDE_TIME);
                break;
            case '1':
                if (joined) {
                    fprintf (stdout, "\nVNG joinAPIs Test: gameJoiner: "
                                    "Leaving on commeand.  vn: %p\n", &vn_j);
                    ec = VNG_LeaveVN (vng, &vn_j);
                    fprintf (stdout, "\nVNG joinAPIs Test: gameJoiner: "
                                    "VNG_LeaveVN: returned: %d\n", ec);
                    joined = false;
                }
                else {
                    fprintf (stdout, "\nVNG joinAPIs Test: gameJoiner: "
                                    "Not leaving on commeand, because did not join\n");
                }
                break;
            case '2':
                if (VN_State(&vn_j) != VN_EXITED) {
                    fprintf (stdout, "\nVNG joinAPIs Test:   gameJoiner:  "
                                    "Deleting on command.  "
                                    "VNG_DeleteVN (vn_j:%p).\n", &vn_j);
                    if ((ec = VNG_DeleteVN (vng, &vn_j)) != VNGERR_VN_NOT_HOST) {
                        fprintf (stdout, "VNG joinAPIs Test:  gameJoiner:  "
                                        "VNG_DeleteVN should have returned "
                                        "VNGERR_VN_NOT_HOST: %d\n", ec);
                        res = -1;
                    }
                    joined = false;
                }
                else {
                    fprintf (stdout, "\nVNG joinAPIs Test: gameJoiner: "
                                    "Not deleting VN on commeand, because VN_EXITED\n");
                }
                break;

            case 'X':
                if (ready & VN_SERVICE_TYPE_RELIABLE_MSG) {
                    if ((ec = VN_RecvMsg (&vn_j,
                                          VN_MEMBER_ANY, VN_SERVICE_TAG_ANY,
                                           rcvdMsg, &rcvdMsgLen, &hdr, timeout))) {
                        fprintf (stdout, "VNG joinAPIs Test:  %s:  "
                                        "cmd X VN_RecvMsg Failed: %d\n",
                                        role, ec);
                        res = -1;
                        goto Fini;
                    }
                }
                else {
                    fprintf (stdout, "\nVNG joinAPIs Test: %s: "
                                     "cmd X no ready service channel.  ready: %u\n",
                                     role, ready);
                    res = -1;
                    goto Fini;
                }
                
                if ((ec = VN_SendMsg (&vn_j,
                                      hdr.sender, hdr.serviceTag,
                                      rcvdMsg, sizeof rcvdMsg,
                                      VN_DEFAULT_ATTR, VNG_NOWAIT))) {
                    fprintf (stdout, "\nVNG joinAPIs Test: %s: "
                                     "cmd %c VN_SendMsg error.  ec: %d\n",
                                     role, cmd, ec);
                    res = -1;
                    goto Fini;
                }
                fprintf (stdout, "\nVNG joinAPIs Test: %s: "
                                 "cmd X VN_SendMsg successful: %c\n",
                                  role, rcvdMsg[0]);
                break;

            case 'm':
            case 'n':
                peer = VN_MEMBER_INVALID;
                nMemb =  VN_GetMembers (&vn_j, members, maxMembers);
                msg[0] = cmd;
                msg[1] = '\0';
                vn_j_self = VN_Self(&vn_j);
                for (mi = 0; mi < nMemb; ++mi) {
                    if (members[mi] != vn_j_self) {
                        peer = members[mi];
                        break;
                    }
                }
                if (peer == VN_MEMBER_INVALID) {
                    fprintf (stdout, "\nVNG joinAPIs Test: %s: "
                                     "cmd %c no other member.  nMembs: %u\n",
                                     role, cmd, nMemb);
                    res = -1;
                    goto Fini;
                }
                if ((ec = VN_SendMsg (&vn_j, peer, cmd, msg, sizeof msg,
                                      VN_DEFAULT_ATTR, VNG_NOWAIT))) {
                    fprintf (stdout, "\nVNG joinAPIs Test: %s: "
                                     "cmd %c VN_SendMsg error.  ec: %d\n",
                                     role, cmd, ec);
                    res = -1;
                    goto Fini;
                }
                fprintf (stdout, "\nVNG joinAPIs Test: %s: "
                                 "cmd %c VN_SendMsg successful: %c\n",
                                  role, cmd, msg[0]);
                break;

            case 'M':
            case 'N':
                if ((ec = VN_RecvMsg (&vn_j,
                                        VN_MEMBER_ANY, tolower(cmd),
                                        rcvdMsg, &rcvdMsgLen, &hdr, timeout))) {
                    fprintf (stdout, "VNG joinAPIs Test:  %s:  "
                                    "cmd %c VN_RecvMsg Failed: %d\n", role, cmd, ec);
                    res = -1;
                    goto Fini;
                }
                fprintf (stdout, "\nVNG joinAPIs Test: %s: "
                                 "cmd %c VN_RecvMsg successful: %c\n",
                                  role, cmd, rcvdMsg[0]);
                break;

            case '*':
                fprintf (stdout, "\nVNG joinAPIs Test: %s:  "
                                    "Waiting for msg on any svc channel for any memb.\n",
                                    role);
                fflush(stdout);
                if ((ec = VN_Select (&vn_j,
                                     VN_SERVICE_TYPE_ANY_MASK,
                                     &ready,   // out 
                                     VN_MEMBER_ANY,
                                     timeout))) {
                    fprintf (stdout, "VNG joinAPIs Test: %s: "
                                    "VN_Select Failed: %d\n", role, ec);
                    res = -1;
                    goto Fini;
                }
                fprintf (stdout, "\nVNG joinAPIs Test: %s: "
                                 "VN_Select says ready channel: %u\n",
                                  role, ready);
                break;


            case 'a':
                // ignore for now
                break;
            case 'b':
                fprintf (stdout, "\nVNG joinAPIs Test: gameJoiner:  "
                                    "start VNG_JoinVN.\n");
                fflush(stdout);
                if ((ec = VNG_JoinVN (vng, vnId_game, "Please let me join !",
                                      &vn_j, denyReason, sizeof denyReason,
                                      timeout))) {
                    if (ec == VNGERR_VN_JOIN_DENIED) {
                        fprintf (stdout, "\nVNG joinAPIs Test: gameJoiner:  "
                                         "VNG_JoinVN denied: %s\n",
                                         denyReason);
                    }
                    else {
                        fprintf (stdout, "\nVNG joinAPIs Test: gameJoiner:  "
                                         "VNG_JoinVN Failed: %d  %s\n",
                                         ec, denyReason);
                        res = -1;
                        goto Fini;
                   }
                }
                else {
                    joined = true;
                    fprintf (stdout, "\nVNG joinAPIs Test: gameJoiner:  "
                                     "VNG_JoinVN complete.  vn: %p\n", &vn_j);
                }
                break;
        }
    }


Fini:
    if (joiner_logged_in) {
        if (!res)
            res = checkConnectStatus(test->name, vng, tests->cStatInitLogin,
                                      0, VNGT_DEF_SHOW_CORRECT);
        fprintf (stdout, "\nVNG joinAPIs Test: gameJoiner: start VNG_Logout.\n");
        if ((ec = VNG_Logout (vng, timeout))) {
            fprintf (stdout, "VNG joinAPIs Test:  gameJoiner:  "
                            "VNG_Logout Failed: %d\n", ec);
            res = -1;
        }
        if (!res)
            res = checkConnectStatus(test->name, vng, tests->cStatInit,
                                      0, VNGT_DEF_SHOW_CORRECT);
    }

    if (!res)
        res = checkConnectStatus(test->name, vng, tests->cStatInit,
                                  VNG_ST_USER_LOGIN, VNGT_DEF_SHOW_CORRECT);

end:
    if (joined && VN_State(&vn_j) != VN_EXITED) {
        fprintf (stdout, "\nVNG joinAPIs Test: gameJoiner:  "
                         "start VNG_LeaveVN (vn_j:%p).\n", &vn_j);
        if ((ec = VNG_LeaveVN (vng, &vn_j))) {
            fprintf (stdout, "\nVNG joinAPIs Test: gameJoiner:  "
                             "VNG_LeaveVN Failed: %d\n", ec);
            res = -1;
        }
    }

    test->gameJoinerResult =res;
    return (_SHRThreadRT) 0;
}





static
int32_t rpcLogin(VN            *vn,
                 VNMsgHdr      *hdr, 
                 const void    *args,
                 size_t         args_len, 
                 void          *ret,       // out
                 size_t        *ret_len)   // out
{
    VNGErrCode     ec;
    VNId           vnId;

   _vng_login_arg  la;
   _vng_login_ret  *user;

    char nickname[] = "Funny nickname";

    unsigned  argsLen;
    unsigned  retLen;

    if ((ec = VN_GetVNId(vn, &vnId))) {
        fprintf(stdout, "\nVNG joinAPIs Test: rpcLogin requested on vn %p"
                "  VN_GetVNId failed: %d\n", vn, ec);
    }
    else {
        fprintf(stdout, "VNG joinAPIs Test: rpcLogin requested on vn %p"
                "  device_type: %d"
                "  chip_id: 0x%08x"
                "  netid: 0x%08x\n",
            vn,
            (uint32_t)(vnId.deviceId >> 32),
            (uint32_t)vnId.deviceId,
            vnId.netId);
    }

    argsLen = sizeof la;
    if (argsLen > args_len) {
       *ret_len = 0;
       fprintf (stdout, "\nVNG joinAPIs Test: rpcLogin Failed:  vn: %p  "
                             "args_len too small: %u  "
                             "sizeof _vng_login_arg: %u\n",
                             vn, (unsigned) args_len, argsLen);
        return VNGERR_CONN_REFUSED;
    }

    retLen = sizeof *user;
    if (retLen > VN_MAX_MSG_LEN) {
       *ret_len = 0;
        fprintf (stdout, "\nVNG joinAPIs Test: rpcLogin Failed:  vn: %p  "
                             "*ret_len too small: %u  "
                             "sizeof _vng_login_ret: %u\n",
                             vn, (unsigned) *ret_len, retLen);
        return VNGERR_CONN_REFUSED;
    }

    la = *((_vng_login_arg*)args);

    la.user[VNG_MAX_LOGIN_LEN] = '\0';
    la.passwd[VNG_MAX_PASSWD_LEN] = '\0';

    fprintf (stdout, "\nVNG joinAPIs Test: rpcLogin:  vn: %p  "
                            "login Name:  %s  "
                            "    passwd:: %s\n",
                            vn, la.user, la.passwd);

    user = (_vng_login_ret*)ret;

    user->uInfo.uid = VNGT_DEF_USERID;
    strncpy (user->uInfo.nickname, nickname, VNG_MAX_NICKNAME_LEN);
    user->uInfo.nickname [VNG_MAX_NICKNAME_LEN] = '\0';
    *ret_len = retLen;

    ++logins;

    fprintf (stdout, "Num logins: %d\n", logins);

    auto_created_vn_id = vn->id;

    return VNG_OK;
}



static
int32_t  rpcLogout (VN          *vn,
                    VNMsgHdr    *hdr,
                    const void  *args,
                    size_t       args_len,
                    void        *ret,
                    size_t      *ret_len)

{
    VNGErrCode     ec;
    VNId           vnId;

    if ((ec = VN_GetVNId(vn, &vnId))) {
        fprintf(stdout, "\nVNG joinAPIs Test: rpcLogout requested on vn %p"
                "  VN_GetVNId failed: %d\n", vn, ec);
    }
    else {
        fprintf(stdout, "VNG joinAPIs Test: rpcLogout requested on vn %p"
                "  device_type: %d"
                "  chip_id: 0x%08x" 
                "  netid: 0x%08x\n",
            vn,
            (uint32_t)(vnId.deviceId >> 32),
            (uint32_t)vnId.deviceId,
            vnId.netId);
    }


    if (ret_len) {
        *ret_len = 0;
    }

    --logins;

    fprintf (stdout, "Num logins: %d\n", logins);

    if (logins <= 0) {
        exit_servRPC_requested = true;
        wait_before_exit_servRPC = true;
    }

    return VNG_OK;
}







static
int32_t rpcRegisterGame (VN          *vn,
                         VNMsgHdr    *hdr,
                         const void  *args,
                         size_t       args_len,
                         void        *ret,
                         size_t      *ret_len)
{
    unsigned  minLen;
    size_t    iend;

    _vng_register_game_arg rg;

    // Ok if no comments
    minLen = offsetof(_vng_register_game_arg, comments);
    if (minLen > args_len) {
       *ret_len = 0;
        fprintf (stdout, "\nVNG joinAPIs Test: rpcRegisterGame Failed:  vn: %p  "
                             "args_len too small: %u  "
                             "minLen _vng_register_game_args: %u\n",
                             vn, (unsigned) args_len, minLen);
        return VNGERR_UNKNOWN;
    }

    memcpy (&rg,  args, sizeof rg);

    if (args_len > minLen) {
        if (args_len > sizeof rg)
            iend = sizeof(rg.comments) - 1;
        else
            iend = args_len - minLen - 1;
    }
    else {
        iend = 0;
    }

    rg.comments [iend] = '\0';

    fprintf (stdout, "\nVNG joinAPIs Test:  vn: %p  "
                     "rpcRegisterGame:  "
                     "chipId: 0x%x  deviceType: %d  netId: 0x%x\n",
                      vn,
                      deviceIdToChipId(rg.info.vnId.deviceId),
                      deviceIdToDeviceType(rg.info.vnId.deviceId),
                      rg.info.vnId.netId);

    fprintf (stdout, "\nVNG joinAPIs Test:  rpcRegisterGame: comments:\n    %s\n",
                      rg.comments);

    *ret_len = 0;   // no return data

    registeredGame = rg;

    return VNG_OK;
}



static
int32_t rpcLetter (VN          *vn,
                   VNMsgHdr    *hdr,
                   const void  *args,
                   size_t       args_len,
                   void        *ret,
                   size_t      *ret_len)
{
    VNGTUserId      *uid;

    size_t  minLen;
    size_t  retLen;

    uid = (VNGTUserId*) args;

    minLen = sizeof *uid;
    if (minLen > args_len) {
        *ret_len = 0;
        fprintf (stdout, "VNG joinAPIs Test: rpcLetter %c Failed:  vn: %p  "
                             "args_len too small: %u  "
                             "minLen sizeof VNGTUserId: %u\n",
                             hdr->serviceTag,
                             vn, (unsigned) args_len, (unsigned) minLen);
        return VNGERR_UNKNOWN;
    }

    fprintf (stdout, "VNG joinAPIs Test: rpcLetter %c received from:  vn: %p  "
                            "vn_num  %u  "
                            "player_num %u\n",
                            hdr->serviceTag,
                            vn, uid->vn_num, uid->player_num);
    retLen = 0;

    *ret_len = retLen;

    if (hdr->serviceTag == 'x')
        exit_servRPC_requested = true;

    return VNG_OK;
}






#define MAX_BUDDIES_STATUS  1



static
int32_t rpcGetBuddyStatus (VN          *vn,
                           VNMsgHdr    *hdr,
                           const void  *args,
                           size_t       args_len,
                           void        *ret,
                           size_t      *ret_len)
{
   VNGBuddyStatus  *bs;
   VNGUserId       *uid;
   size_t           nBuddies;
   unsigned         i;

    size_t  minLen;
    size_t  retLen;

    minLen = sizeof(VNGUserId);
    if (minLen > args_len) {
       *ret_len = 0;
        fprintf (stdout, "\nVNG joinAPIs Test: rpcGetBuddyStatus Failed:  vn: %p  "
                             "args_len too small: %u  "
                             "minLen _vng_register_game_args: %u\n",
                             vn, (unsigned) args_len, minLen);
        return VNGERR_UNKNOWN;
    }

    uid = (VNGUserId*) args;
    bs = (VNGBuddyStatus*) ret;

    nBuddies = args_len/(sizeof(VNGUserId));


    fprintf (stdout, "\nVNG joinAPIs Test:  vn: %p  "
                     "rpcGetBuddyStatus: uid: %u\n",
                     vn, uid[0]);

    retLen = nBuddies * sizeof(VNGBuddyStatus);
    if (retLen > VN_MAX_MSG_LEN) {
       *ret_len = 0;
        fprintf (stdout, "\nVNG joinAPIs Test:  vn: %p  "
                         "rpcGetBuddyStatus Failed:  "
                         "*ret_len too small: %u  "
                         "required: %u\n",
                         vn, (unsigned) *ret_len, retLen);
        return VNGERR_UNKNOWN;
    }

    for (i=0; i<nBuddies; ++i) {
        bs[i].uid  = uid[i];
        bs[i].vnId = registeredGame.info.vnId;
        bs[i].gameId = registeredGame.info.gameId;
        bs[i].onlineStatus = VNG_STATUS_ONLINE;
        *ret_len = retLen;
    }

    return VNG_OK;
}




static
VNGErrCode VNGT_RegisterGame (VNG          *vng,
                              VNGGameInfo  *info,
                              char         *comments,
                              VNGTimeout    timeout)
{
    _vng_register_game_arg rg;
    size_t              rga_len;
    size_t              msg_len;
    int32_t             optData;

    VNGErrCode ec;
    int        rv;
    VN         server;

    if ((ec = VNG_InitInfraServerVN (vng, &server))) {
        fprintf (stdout, "VNG joinAPIs Test:  VNGT_RegisterGame: "
                         "VNG_InitInfraServerVN Failed: %d\n", ec);
        rv = -1;
        goto end;
    }

    rg.info = *info;
    strncpy(rg.comments, comments, VNG_GAME_COMMENTS_LEN);
    rg.comments[VNG_GAME_COMMENTS_LEN-1] = '\0';
    msg_len = strlen (rg.comments);
    rga_len = offsetof(_vng_register_game_arg, comments)
            + msg_len + 1;

    ec = VN_SendRPC (&server, VN_MEMBER_OWNER,
                    _VNG_REGISTER_GAME,
                     &rg, rga_len,
                     NULL, NULL,
                     &optData,
                     timeout);

    if (ec) {
        fprintf (stdout, "VNG joinAPIs Test:  VNGT_RegisterGame: "
                         "VN_SendRPC Failed: %d\n", ec);
        rv = -1;
    }
    else if (optData) {
        fprintf (stdout, "VNG joinAPIs Test:  VNGT_RegisterGame: "
                         "VN_SendRPC optdata: %d\n", optData);
        rv = -1;
    }
    else {
        fprintf (stdout, "VNG joinAPIs Test:  VNGT_RegisterGame: "
                         "successful\n");
        rv = 0;
    }

end:
    return rv;
}




static
VNGErrCode VNGT_GetBuddyStatus (VNG            *vng,
                               VNGBuddyStatus *buddyStatus,
                               uint32_t        nBuddies,
                               VNGTimeout      timeout)
{
    size_t      arglen;
    size_t      retlen;
    int32_t     optData;
    size_t      i;

    VNGUserId   uid[16];

    VNGErrCode ec;
    int        rv;
    VN         server;

    if ((ec = VNG_InitInfraServerVN (vng, &server))) {
        fprintf (stdout, "VNG joinAPIs Test:  VNGT_GetBuddyStatus: "
                         "VNG_InitInfraServerVN Failed: %d\n", ec);
        rv = -1;
        goto end;
    }

    arglen = sizeof(VNGUserId) * nBuddies;
    retlen = sizeof(VNGBuddyStatus) * nBuddies;

    for (i=0; i < sizeof uid && i < nBuddies;  ++i)
        uid[i] = buddyStatus[i].uid;

    ec = VN_SendRPC (&server, VN_MEMBER_OWNER,
                    _VNGT_GET_BUDDY_STATUS,
                     uid, arglen,
                     buddyStatus, &retlen,
                     &optData,
                     timeout);

    if (ec) {
        fprintf (stdout, "VNG joinAPIs Test:  VNGT_GetBuddyStatus: "
                         "VN_SendRPC Failed: %d\n", ec);
        rv = -1;
    }
    else if (optData) {
        fprintf (stdout, "VNG joinAPIs Test:  VNGT_GetBuddyStatus: "
                         "VN_SendRPC optdata: %d\n", optData);
        rv = -1;
    }
    else {
        fprintf (stdout, "VNG joinAPIs Test:  VNGT_GetBuddyStatus: "
                         "successful\n");
        rv = 0;
    }

end:
    return rv;
}








/* comn cmds: 0  - exit now (also at end of cmd string)
              1  - leave vn
              2  - delete vn
              3  - login
              4  - logout

              g-k  - send req ascii g-k
              G-K  - wait for req ascii g-k and send resp
              l-q  - send msg ascii l-q
              L-Q  - wait for msg ascii g-q
              t-v  - send rpc t-v
              T=V  - wait for rpc t-v
              z    - sleep for _VNGT_BIDE_TIME

              *  -   wait for msg on any svc channel for any memb
              X  -   respond to ready channel indicated by *
              W  -   wait for msg on selectFrom svc channel for any memb
              w  -   wait for msg on selectFrom svc channel for any memb
                     and send corresponding response (w for msg, resp or rpc resp)
              R  -   wait for msg on selectFrom svc channel for selectMemb
                     and send cooresponding response (r for msg, resp or rpc resp)
              r  -   wait for msg on selectFrom svc channel for selectMemb

   hostCmds:  a  - wait for join request event
              b  - wait for join request
              c  - accept join request
              d  - reject join request


  joinerCmds: a  - get game host VNId
              b  - request join
*/

void initJoinAPIsTest (JoinAPIsTest *test,
                       CTestProc     cProc,
                       const char   *name,
                       const char   *shortName)
{
    initCTest ((CTest*) test, cProc, name, shortName);

    test->hostCmds    = "abc*Xnzz0"; // "abc*Xn*Xz0"
    test->joinerCmds  =  "abzmMNz0"; // "abzmMNuz0"

    test->servRPCsResult   = 0;
    test->gevThreadResult  = 0;
    test->gameHostResult   = 0;
    test->gameJoinerResult = 0;
}



void* newJoinAPIsTest (size_t       cTestLen,
                       CTestProc    cProc,
                       const char  *name,
                       const char  *shortName)
{
    JoinAPIsTest  *test;

    if (offsetof(JoinAPIsTest, EndOfCTest) != offsetof(CTest, EndOfCTest)) {
        fprintf (stderr, "\n\n  vngt Internal Error: start of JoinAPIsTest not like CTest\n\n");
        exit (1);
    }

    test = vngt_malloc (cTestLen);
    initJoinAPIsTest (test, cProc, name, shortName);
    test->free = vngt_free;

    return test;
}




#ifndef __VNGT_JOIN_H__
#define __VNGT_JOIN_H__  1

#ifdef _WIN32
    #pragma once
#endif

//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vngt_ctest.h"


#ifdef __cplusplus
extern "C" {
#endif

typedef struct __JoinAPIsTest {

    // Warning: must begin with same members as CTest
    CTestProc      proc;
    const char*    name;
    const char*    shortName;
    bool           passed;
    char*          failReason;
    VNG           *vng;
    CTestFreeProc  free;
    int            EndOfCTest;
    // Warning: end of CTest members

    const char*   hostCmds;
    const char*   joinerCmds;
    
    int servRPCsResult;
    int gevThreadResult;
    int gameHostResult;
    int gameJoinerResult;

} JoinAPIsTest;


int joinAPIs (CTestEnv *tests);

void* newJoinAPIsTest (size_t       cTestLen,
                        CTestProc    cProc,
                        const char  *name,
                        const char  *shortName);



#ifdef  __cplusplus
}
#endif

#endif  // __VNGT_JOIN_H__

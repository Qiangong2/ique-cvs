//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#if defined(_SC)
    #include "sc/ios.h"
    #include "ioslibc.h"

    #define STACK_SIZE (48*1024)
    const u8 _initStack[STACK_SIZE];
    const u32 _initStackSize = sizeof(_initStack);
    const u32 _initPriority =9;
#endif
#include "vng.h"
#include "vng_p.h"
#include "shr_trace.h"
#include "shr_getopt.h"
#include "shr_th.h"
#include "shr_mem.h"


#if defined(_LINUX) && defined(_VN_RPC_DEVICE)
    typedef uint16_t _vn_inport_t;
    void _vn_netif_set_usb_proxy_port(_vn_inport_t port);
    void _vn_netif_set_usb_proxy_host(const char* host);
#endif


#define SG_MISC     0
#define SG_TEST_1   1
#define SG_TEST_2   2
#define SG_TEST_3   3

#undef  MISC
#define MISC     _TRACE_TEST, SG_MISC
#define TEST_1   _TRACE_TEST, SG_TEST_1
#define TEST_2   _TRACE_TEST, SG_TEST_2
#define TEST_3   _TRACE_TEST, SG_TEST_3

#define DEFAULT_IS_GAME_HOST     false

#define DEFALUT_VNGS_HOST       "ogs.bbu.lab1.routefree.com"
#define DEFAULT_VNGS_PORT       16978
#define DEFAULT_USB_PROXY_PORT  20010
#define DEFAULT_VN_DOMAIN       VN_DOMAIN_INFRA;
#define DEFAULT_VNG_TIMEOUT     (20*1000)

#define TESTER01     "10001"
#define TESTER01PW   "10001"
#define TESTER02     "10002"
#define TESTER02PW   "10002"

#define VNGT_VN_ST_INITIALIZED  0x01
#define VNGT_ST_FINI            VNG_ST_RM_OK

#if !defined(_LINUX) || defined(_VN_RPC_DEVICE)
    #define VNGT_ST_USB (VNG_ST_USB_CONNECTED   | \
                         VNG_ST_PROXY_RUNNING   | \
                         VNG_ST_PROXY_CONNECTED | \
                         VNG_ST_ADHOC_SUPPORTED)
#else
    #define VNGT_ST_USB 0
#endif

#define VNGT_ST_INIT (VNG_ST_RM_OK | \
                      VNGT_VN_ST_INITIALIZED | \
                      VNG_ST_VNG_INITIALIZED | \
                      VNG_ST_GATEWAY_DEFINED | \
                      VNGT_ST_USB)

#define VNGT_ST_INIT_LOGIN  (VNGT_ST_INIT | VNG_ST_USER_LOGIN)

#define GAME_ID     1011
#define GAME_TITLE_ID 2
#define SUBGAME 0
#define SUBGAME_ATTR_ID  0
#define SUBGAME_VAL  3
#define GAME_STATUS   20060120
#define NUM_PLAYERS   7
#define GAME_KEYWORD "Test Game Keyword"


bool        __isGameHost  = DEFAULT_IS_GAME_HOST;
bool        __isJoiner    = !DEFAULT_IS_GAME_HOST;
VNGTimeout  __vng_timeout = DEFAULT_VNG_TIMEOUT;
VNDomain    __domain      = DEFAULT_VN_DOMAIN;
const char *__vngs_host   = DEFALUT_VNGS_HOST;
VNGPort     __vngs_port   = DEFAULT_VNGS_PORT;
const char *__usb_proxy_host  = NULL;
VNGPort     __usb_proxy_port  = DEFAULT_USB_PROXY_PORT;

VNG         __vng;
VN          __vn_game;
VN          __vn_joiner;





static void usage (char *executable_name)
{
#ifndef _SC
    printf ("\n");
#endif

    printf ("\n%s  [options]\n\n", executable_name);

    printf ("    [--gamehost   | -g]             // Act as game host\n\n");
    printf ("    [--joiner     | -j]             // Act as game joiner\n\n");
    printf ("    [--timeout    | -t] <millisec>  // Set default timeout\n");
    printf ("    [--domain     | -d] <domain>    // 1, 2, or 3 for local, adhoc, or infra. Default is infra\n");
    printf ("    [--vngs_host  | -s] <server>    // Use specified infrastructure server\n");
    printf ("    [--vngs_port  | -p] <port>      // Use specified infrastructure server port\n");
    printf ("    [--proxy_host | -y] <host>      // Use specified host as USP proxy\n\n");
    printf ("    [--proxy_port | -r] <port>      // Use specified port of USP proxy host\n\n");

    printf ("    [--help       | -h]             // Show this help.\n\n\n");
}





int processCmdLine (int argc, char* argv[])
{
    static struct option long_options[] = {
        { "help",       no_argument,       0, 'h'},
        { "gamehost",   no_argument,       0, 'g'},
        { "joiner",     no_argument,       0, 'j'},
        { "timeout",    required_argument, 0, 't'},
        { "domain",     required_argument, 0, 'd'},
        { "vngs_host",  required_argument, 0, 's'},
        { "vngs_port",  required_argument, 0, 'p'},
        { "proxy_host", required_argument, 0, 'y'},
        { "proxy_port", required_argument, 0, 'r'},
        { NULL },
    };

    bool more_args;
    int c;

    while ((c = getopt_long(argc, argv, "hgjt:d:s:p:y:r:",
                            long_options, NULL)) >= 0) {
        switch (c) {
        case 'h':
            usage(argv[0]);
            exit(0);
        case 'g':
            __isGameHost = true;
            __isJoiner = false;
            break;
        case 'j':
            __isGameHost = false;
            __isJoiner = true;
            break;
        case 't':
            #if !defined(_SC) && !defined(_GBA)
                __vng_timeout = atoi(optarg);
            #endif
            break;
        case 'd':
            #if !defined(_SC) && !defined(_GBA)
                __domain = atoi(optarg);
                if (__domain < 1 || __domain > 3) {
                    usage(argv[0]);
                    exit(1);
                }
            #endif
            break;
        case 's':
            __vngs_host = optarg;
            break;
        case 'p':
            #if !defined(_SC) && !defined(_GBA)
                __vngs_port = atoi(optarg);
            #endif
            break;
        case 'y':
            __usb_proxy_host = optarg;
            break;
        case 'r':
            #if !defined(_SC) && !defined(_GBA)
                __usb_proxy_port = atoi(optarg);
            #endif
            break;
        default:
            usage(argv[0]);
            exit(1);
        }
    }

    more_args = (optind < argc);

    if (more_args) {
        usage (argv[0]);
        exit(1);
    }

    return 0;
}


const char* passFailStr(status)
{
    return (status == 0) ? "PASSED" : "FAILED";
}


int checkConnectStatus (VNG    *vng,
                        u32     expected,
                        u32     dontCare,
                        bool    showCorrect)
{
    int            rv = 0;
    u32            cstat = VNG_ConnectStatus(vng);

    if ((cstat & ~dontCare) != (expected & ~dontCare)) {
        trace (INFO, TEST_1,
               "VNG_ConnectStatus compare Failed. Status != Expected: "
               "(0x%08x & ~0x%08x) != (0x%08x & ~0x%08x)\n", 
                  cstat,  dontCare, expected,  dontCare);
        rv = -1;
    } else if (showCorrect) {
        trace (INFO, TEST_1,
               "VNG_ConnectStatus correct  0x%08x\n", cstat);
    }

    return rv;
}


void dumpGameInfo(VNGGameStatus *s)
{
    VNGGameInfo *gi = &s->gameInfo;
    int i;

    trace (INFO, TEST_1, "  num players %d\n", s->numPlayers);
    trace (INFO, TEST_1, "  game status %d\n", s->gameStatus);
    trace (INFO, TEST_1, "  vnId 0x%016llx.%08x\n", gi->vnId.deviceId, gi->vnId.netId);
    trace (INFO, TEST_1, "  owner %0d\n", gi->owner);
    trace (INFO, TEST_1, "  gameId %d\n", gi->gameId);
    trace (INFO, TEST_1, "  titleId %d\n", gi->titleId);
    trace (INFO, TEST_1, "  netZone %d\n", gi->netZone);
    trace (INFO, TEST_1, "  maxLatency %d\n", gi->maxLatency);
    trace (INFO, TEST_1, "  accessControl %d\n", gi->accessControl);
    trace (INFO, TEST_1, "  totalSlots %d\n", gi->totalSlots);
    trace (INFO, TEST_1, "  buddySlots %d\n", gi->buddySlots);
    trace (INFO, TEST_1, "  keyword \"%s\"\n", gi->keyword);
    trace (INFO, TEST_1, "  attrCount %d\n", gi->attrCount);
    for (i = 0; i < gi->attrCount; i++) {
        trace (INFO, TEST_1, "  attr[%d] is %d\n", i, gi->gameAttr[i]);
    }
}


char* cmpTypes[] = {
   "VNG_CMP_DONTCARE",
   "VNG_CMP_STR",
   "VNG_CMP_EQ", 
   "VNG_CMP_NEQ", 
   "VNG_CMP_LT", 
   "VNG_CMP_GT", 
   "VNG_CMP_LE", 
   "VNG_CMP_GE", 
};

void dumpSearchCriteria(VNGSearchCriteria *c)
{
    int i;

    trace (INFO, TEST_1, "Search Criteria:\n");
    trace (INFO, TEST_1, "  gameId %u\n", c->gameId);
    trace (INFO, TEST_1, "  domain %u\n", c->domain);
    trace (INFO, TEST_1, "  maxLatency %d\n", c->maxLatency);
    trace (INFO, TEST_1, "  cmpKeyword %u %s\n", c->cmpKeyword, cmpTypes[c->cmpKeyword]);
    if (c->cmpKeyword != VNG_CMP_DONTCARE) {
        trace (INFO, TEST_1, "  keyword \"%s\"\n", c->keyword);
    }
    for (i = 0; i < VNG_MAX_PREDICATES; i++) {
        if (c->pred[i].cmp == VNG_CMP_DONTCARE) {
            continue;
        }
        trace (INFO, TEST_1, "  c->pred[%d].cmp is %u  %s\n",
                         i, c->pred[i].cmp, cmpTypes[c->pred[i].cmp]);
        trace (INFO, TEST_1, "  c->pred[%d].attrId is %u\n",
                         i, c->pred[i].attrId);
        trace (INFO, TEST_1, "  c->pred[%d].attrValue is %u\n",
                         i, c->pred[i].attrValue);
    }
}


int dumpEvents(VNG *vng, VNGEventID  mustFindEvent)
{
    int  rv = 0;
    bool eventFound = false;

    // Wait for all events
    while (1) {
        VNGEvent event;
        VNGErrCode ec = VNG_GetEvent(vng, &event, VNG_NOWAIT);
        if (ec == VNG_OK) {
           trace (INFO, TEST_1, "Got event %d", event.eid);
           if (event.eid == VNG_EVENT_PEER_STATUS) {
               VNId vid;
               VN_GetVNId(event.evt.peerStatus.vn, &vid);
               trace (INFO, TEST_1, " -- 0x%08x.%08x.%08x %d",  (int)(vid.deviceId >> 32), (int)vid.deviceId, vid.netId, event.evt.peerStatus.memb);
           }    
           trace (INFO, TEST_1, "\n");
           if (event.eid == mustFindEvent) {
               eventFound = true;
           }
        }
        else {
            if (ec != VNGERR_NOWAIT && ec != VNGERR_TIMEOUT)
                trace (ERR, TEST_1, "VNG_GetEvent returns %d\n", ec);
                rv = -1;
            }
            break;
    }

    if (mustFindEvent && !eventFound) {
        trace (ERR, TEST_1, "Didn't find event %d\n", mustFindEvent);
        rv = -1;
    }

    return rv;
}



void dumpTrop (VNGTraceOpt  trop) {
    trace (INFO, MISC, "\ntrop.mask 0x0%08x\n"
                       "trop.global.enable %d\n"
                       "trop.global.level %d\n"
                       "trop.global.prefix %s\n"
                       "trop.group.id %d\n"
                       "trop.group.enable %d\n"
                       "trop.group.level %d\n"
                       "trop.group.subGrpEnableMask 0x0%08x\n"
                       "trop.group.subGrpDisableMask 0x0%08x\n"
                       "trop.group.sub.bit %d\n"
                       "trop.group.sub.level %d\n",

                        trop.mask,
                        trop.global.enable,
                        trop.global.level,
                        trop.global.prefix,
                        trop.group.id,
                        trop.group.enable,
                        trop.group.level,
                        trop.group.subGrpEnableMask,
                        trop.group.subGrpDisableMask,
                        trop.group.sub.bit,
                        trop.group.sub.level);

}


int checkSetTraceOpt (VNG *vng)
{

//    To change the global trace level to INFO, the VN trace
//    level to FINE, the VN events sub group 11 to FINEST, and
//    disable all vn sub groups except sub group 11:
//
//      trop.global.level = 3; // = INFO from shr_trace.h
//      trop.mask = VNG_TROP_GLOBAL_LEVEL
//
//      trop.group.id     = 0; // = _TRACE_VN   from shr_trace.h
//      trop.group.level  = 4; // = FINE from shr_trace.h
//      trop.mask |= VNG_TROP_GRP_LEVEL;
//      
//      trop.group.sub.bit = 11;  // _VN_SG_EVENTS from vndebug.h
//      trop.group.sub.level = 6; // FINEST  from shr_trace.h
//      trop.mask |= VNG_TROP_SUBGRP_LEVEL;
//
//      trop.group.subGrpDisableMask = (~(1 << 11));
//      trop.mask |= VNG_TROP_SUBGRP_DISABLE_MASK;

    int rv = 0;
    VNGErrCode  ec;
    
    VNGTraceOpt trop = {
        VNG_TROP_GLOBAL_LEVEL |
        VNG_TROP_GRP_LEVEL    |
        VNG_TROP_SUBGRP_LEVEL |
        VNG_TROP_SUBGRP_DISABLE_MASK,

        { false, INFO, {0} },
        { _TRACE_VN, 0, FINE, 0, (~(1 << 11)), { 11, FINEST } }
    };


    trace (INFO, MISC, "change the global trace level to INFO, "
           "the VN trace level to FINE, the VN events sub group 11 to FINEST, "
           "and disable all vn sub groups except sub group 11\n");
    dumpTrop(trop);

    if ((ec = VNG_SetTraceOpt (vng, trop))) {
        trace (ERR, TEST_1, "VNG_SetTraceOpt returned %d\n", ec);
        rv = -1;
    }

    return rv;
}



int checkBuddyStatus (VNG                 *vng,
                      VNGUserId            buddy_uid,
                      VNGUserOnlineStatus  expectedOnlineStatus,
                      VNGGameId            expectedGameId,
                      VNId                 expectedVNId,
                      VNGTimeout           timeout)
{
    int rv = 0;
    u32 i;
    VNGErrCode  ec;
    VNGUserInfo uinfo;
    VNGUserId   uid;
    char       *loggedInText = "";
    VNGBuddyStatus bs[2];
    VNGUserInfo    buddyinfo[100];
    uint32_t       nbuddies = 100;

    if (buddy_uid == VNG_INVALID_USER_ID) {
        loggedInText = "Logged in ";
        uid = VNG_MyUserId (vng);
        if (uid == VNG_INVALID_USER_ID) {
            trace (INFO, TEST_1, "VNG_MyUserId returned VNG_INVALID_USER_ID\n");
            rv = -1;
            goto end;
        }
    } else {
        uid = buddy_uid;
    }

    trace (INFO, TEST_1, "Checking buddy status of %sVNGUserId %d\n", loggedInText, uid);

    if ((ec = VNG_GetUserInfo (vng, uid, &uinfo, timeout))) {
        trace (ERR, TEST_1, "VNG_GetUserInfo returns %d for VNGUserId %d\n",
                            ec, uid);
        rv = -1;
        goto end;
    }

    trace (INFO, TEST_1, "VNGUserId %d login is %s\n", uid, uinfo.login);
    trace (INFO, TEST_1, "VNGUserId %d nickname is %s\n", uid, uinfo.nickname);

    bs[0].uid = uid;
    bs[1].uid = 0;
    if ((ec = VNG_GetBuddyStatus (vng, bs, 1, timeout))) {
        trace (ERR, TEST_1, "VNG_GetBuddyStatus returns %d\n", ec);
        rv = -1;
        goto end;
    }

    trace (INFO, TEST_1, "Buddy    VNGUserId %d"
                         "  onlineStatus %d   gameId %d   deviceId: 0x%llx   netId: 0x%x\n",
                         bs[0].uid,
                         bs[0].onlineStatus,  bs[0].gameId,
                         bs[0].vnId.deviceId, bs[0].vnId.netId);

    trace (INFO, TEST_1, "Expected VNGUserId %d"
                         "  onlineStatus %d   gameId %d   deviceId: 0x%llx   netId: 0x%x\n",
			 uid,
                         expectedOnlineStatus,  expectedGameId,
                         expectedVNId.deviceId, expectedVNId.netId);

    if (bs[0].uid != uid ||
        bs[0].onlineStatus != expectedOnlineStatus ||
        bs[0].gameId != expectedGameId ||
        bs[0].vnId.deviceId != expectedVNId.deviceId ||
        bs[0].vnId.netId != expectedVNId.netId) {

        trace (ERR, TEST_1, "VNG_GetBuddyStatus not as expeced\n");
        rv = -1;
        goto end;
    }

    if ((ec = VNG_GetBuddyList(vng, 0, buddyinfo, &nbuddies, timeout))) {
        trace (ERR, TEST_1, "VNG_GetBuddyList returns %d\n", ec);
        rv = -1;
        goto end;
    }

    if (!nbuddies) {
        trace (INFO, TEST_1, "VNGUserId %d has no buddies\n", uid);
    }
    else for (i = 0; i < nbuddies; i++) {
        VNGUserInfo *p = &buddyinfo[i];
        VNGUserId uid = p->uid;
        trace (INFO, TEST_1, "buddy[%d] uid is %d\n", i, uid);
        trace (INFO, TEST_1, "login is %s\n", p->login);
        trace (INFO, TEST_1, "nickname is %s\n", p->nickname);
    }

end:
    return rv;
}

#define CACHE_LINE  16

#ifdef _WIN32
    #define CACHE_ALIGN(d) __declspec(align(CACHE_LINE)) d;
#else
    #define CACHE_ALIGN(d) d __attribute__((aligned(16)));
#endif

#define TEST_HEAP_SIZE  16384

CACHE_ALIGN( u8 buf[TEST_HEAP_SIZE] )

// u8 buf[TEST_HEAP_SIZE] __attribute__((aligned(16)));

static
VNGErrCode chkMemAlloc()
{


    IOSHeapId  id;
    u32        heapSize = sizeof buf;
    u32        allocSize = 10;
    void      *ptr = &buf[0]; 
    void      *subPtr;
    IOSError   rv;

    id = IOS_CreateHeap(ptr, heapSize);

    trace (INFO, TEST_1, "Non-Shared IOS_CreateHeap(%p,%d) returns %d\n", ptr, heapSize, id);
    if (id<0) {
        return VNGERR_UNKNOWN;
    }

    subPtr = IOS_Alloc(id, allocSize);

    trace (INFO, TEST_1, "Non-Shared IOS_Alloc(%d,%d) returns %p\n", id, allocSize, subPtr);
    if (!subPtr) {
        return VNGERR_UNKNOWN;
    }

    rv = IOS_Free(id, subPtr);

    trace (INFO, TEST_1, "Non-Shared IOS_Free(%d,%p) returns %d\n", id, subPtr, rv);
    if (rv<0) {
        return VNGERR_UNKNOWN;
    }

    rv = IOS_DestroyHeap(id);

    trace (INFO, TEST_1, "Non-Shared IOS_destroyHeap(%d) returns %d\n", id, rv);
    if (rv<0) {
        return VNGERR_UNKNOWN;
    }

#if defined(_LINUX)
    /* Test shared heap, only works on SC if in a driver */

    ptr = IOS_Alloc(0, heapSize);

    trace (INFO, TEST_1, "Shared IOS_Alloc(0,%d) returns %p\n", heapSize, ptr);
    if (!ptr) {
        return VNGERR_UNKNOWN;
    }

    id = IOS_CreateHeap(ptr, heapSize);

    trace (INFO, TEST_1, "Shared IOS_CreateHeap(%p,%d) returns %d\n", ptr, heapSize, id);
    if (id<0) {
        return VNGERR_UNKNOWN;
    }

    subPtr = IOS_Alloc(id, allocSize);

    trace (INFO, TEST_1, "Shared IOS_Alloc(%d,%d) returns %p\n", id, allocSize, subPtr);
    if (!subPtr) {
        return VNGERR_UNKNOWN;
    }

    rv = IOS_Free(id, subPtr);

    trace (INFO, TEST_1, "Shared IOS_Free(%d,%p) returns %d\n", id, subPtr, rv);
    if (rv<0) {
        return VNGERR_UNKNOWN;
    }

    rv = IOS_DestroyHeap(id);

    trace (INFO, TEST_1, "Shared IOS_destroyHeap(%d) returns %d\n", id, rv);
    if (rv<0) {
        return VNGERR_UNKNOWN;
    }

    rv = IOS_Free(0, ptr);

    trace (INFO, TEST_1, "Shared Non-Shared IOS_Free(0,%p) returns %d\n", ptr, rv);
    if (rv<0) {
        return VNGERR_UNKNOWN;
    }

#endif    /* _LINUX */

    return VNG_OK;
}



VNGErrCode checkGetVNs(VNG *vng, u32 numExpected, VNMember other)
{
    VNGErrCode ec = VNG_OK;
    VN         vn[3];
    VN        *vn_found;
    uint32_t   nVNs = 0;
    char       msg[256];
    uint32_t   msgLen;

    if ((ec = VNG_GetVNs (vng, NULL, &nVNs)) && ec != VNGERR_OVERFLOW) {
        trace (ERR, TEST_1, "VNG_GetVNs() Failed: %d with nVNs %u\n", ec, nVNs);
        goto end;
    }

    if (ec == VNGERR_OVERFLOW  &&  nVNs != numExpected) {
        trace (ERR, TEST_1,
            "VNG_GetVNs() indicates there are %u VNs, expected %u\n",
                nVNs, numExpected);
        goto end;
    }

    trace (INFO, TEST_1, "VNG_GetVNs() indicates there are %u VNs\n", nVNs);
    nVNs = 1;

    if ((ec = VNG_GetVNs (vng, vn, &nVNs)) && ec != VNGERR_OVERFLOW) {
        trace (ERR, TEST_1, "VNG_GetVNs() Failed: %d with nVNs %u\n", ec, nVNs);
        goto end;
    }

    if (ec == VNGERR_OVERFLOW  &&  nVNs != numExpected) {
        trace (ERR, TEST_1,
            "VNG_GetVNs(vng, vn, 1) indicates there are %u VNs, expected %u\n",
                nVNs, numExpected);
        goto end;
    }

    trace (INFO, TEST_1, "VNG_GetVNs() returned %u VNs\n", nVNs);

    if (!nVNs || other == VN_MEMBER_INVALID) {
        trace (INFO, TEST_1, "checkGetVNs: Success without using a returned VN.\n");
        ec = VNG_OK;
        goto end;
    }

    strncpy (msg, "Hello from checkGetVNs !", sizeof msg);
    msgLen = strnlen (msg, sizeof msg);
    vn_found = &vn[0];

    if ((ec = VN_SendMsg (vn_found, other, VN_SERVICE_TAG_ANY,
                            msg, msgLen, VN_DEFAULT_ATTR, VNG_NOWAIT))) {
        trace (ERR, TEST_1, "checkGetVNs:  VN_SendMsg()  Failed: %d\n", ec);
        goto end;
    }
    trace (INFO, TEST_1, "checkGetVNs:  VN_SendMsg() Success\n");

end:

    return ec;
}






VNGErrCode vng_Api_1 (VNDomain    domain,
                         const char *vngs_host,
                         VNGPort     vngs_port,
                         bool        isGameHost,
                         VNGTimeout  timeout)
{
    VNGErrCode  ec = VNG_OK;
    VNGErrCode  rv = VNG_OK;
    VNG        *vng     = &__vng;
    VN         *vn_game = &__vn_game;
    VN         *vn_j    = &__vn_joiner;
    VNId        vnId_game;
    VNGGameId   joined_gameId = 0;
    char        msg[256];
    size_t      rcvdMsgLen;
    VNMsgHdr    hdr;
    VNMember    joiner;
    VNGUserInfo userInfo;
    VNGTimeout  recvMsgTimeout = 30*1000;
    VNGTimeout  joinTimeout = 30*1000;
    bool        showCorrect = false;
    bool        exitOnConnectStatusFail = false;
    bool        ignoreMissingJoiner = true;
    bool        ignoreMissingGameHost = false;
    bool        isCheckTraceOpt = false;
    char       *username;
    char       *passwd;
    u32         cstatDontCare;

    VNGGameStatus gameStatus[100];



    if (isGameHost) {
        username = TESTER01;
        passwd = TESTER01PW;
        cstatDontCare = 0;
    } else {
        username = TESTER02;
        passwd = TESTER02PW;
        cstatDontCare = VNG_ST_ADHOC_SUPPORTED;
    }


    printf ("VNG Api.test.1\n");
    printf ("---------------\n");

    trace (INFO, TEST_1, "I am the %s\n", isGameHost ? "Game Host" : "Joiner");

    if (checkConnectStatus (vng, VNGT_ST_FINI, 0, showCorrect)) {
        trace (ERR, TEST_1, "checkConnectStatus(VNGT_ST_FINI) Failed\n");
        if (exitOnConnectStatusFail) {
            rv = -1;
            goto end;
        }
    }

    if ((ec = VNG_Init (vng, NULL))) {
        trace (ERR, TEST_1, "VNG_Init() Failed %d\n", ec);
        goto end;
    } else {
        trace (INFO, TEST_1, "VNG_Init() Success\n");
    }

    if (checkConnectStatus (vng, VNGT_ST_INIT, cstatDontCare, showCorrect)) {
        trace (ERR, TEST_1, "checkConnectStatus(VNGT_ST_INIT) Failed\n");
        if (exitOnConnectStatusFail) {
            rv = -1;
            goto fini;
        }
    }

    if (isCheckTraceOpt) {
        if (checkSetTraceOpt (vng)) {
            trace (ERR, TEST_1, "checkSetTraceOpt Failed\n");
            rv = -1;
            goto fini;
        }
    }
    else {
        VNGTraceOpt trop = {
            VNG_TROP_GLOBAL_ENABLE |
            VNG_TROP_GLOBAL_LEVEL  |
            VNG_TROP_GRP_LEVEL,
            { true, FINE, {0} },
                { _TRACE_VNG, 0, FINER, 0, 0, { 0, 0 } }
        };

        trace (INFO, MISC, "Enable trace, set trace_level FINE, VNG trace_level FINER\n");
        dumpTrop(trop);

        if ((ec = VNG_SetTraceOpt (vng, trop))) {
            trace (ERR, TEST_1, "VNG_SetTraceOpt returned %d\n", ec);
            rv = -1;
            goto end;
        }
    }

    if (domain == VN_DOMAIN_INFRA) {
        if ((ec = VNG_Login (vng, vngs_host, vngs_port,
                            username, passwd, timeout))) {
            trace (ERR, TEST_1, "VNG_Login() Failed %d\n", ec);
            goto fini;
        } else {
            trace (INFO, TEST_1, "VNG_Login() Success\n");
        }

        if (checkConnectStatus (vng, VNGT_ST_INIT_LOGIN, cstatDontCare, showCorrect)) {
            trace (ERR, TEST_1, "checkConnectStatus(VNGT_ST_INIT_LOGIN) Failed\n");
            if (exitOnConnectStatusFail) {
                rv = -1;
                goto logout;
            }
        }

        joined_gameId = 0;
        vnId_game.deviceId = 0;
        vnId_game.netId = 0;

        if ((ec = VNG_UpdateStatus (vng, VNG_STATUS_AWAY, joined_gameId, vnId_game, timeout))) {
            trace (ERR, TEST_1, "VNG_UpdateStatus Failed %d\n", ec);
            rv = -1;
            goto logout;
        }

        trace (INFO, TEST_1, "Updated my buddy status\n");

        if (checkBuddyStatus (vng, VNG_INVALID_USER_ID,
                                   VNG_STATUS_AWAY,
                                   joined_gameId,
                                   vnId_game,
                                   timeout)) {
            trace (ERR, TEST_1, "checkBuddyStatus Failed\n");
            rv = -1;
            goto logout;
        }

    }

    if (isGameHost) {

        // Create vn for client to join

        VNPolicies     autoAcceptPolicies = VN_DEFAULT_POLICY  |   VN_AUTO_ACCEPT_JOIN;
        VNPolicies     needAcceptPolicies = VN_DEFAULT_POLICY  &  ~VN_AUTO_ACCEPT_JOIN;
        VNPolicies     defaultPolicies    = VN_DEFAULT_POLICY;
        VNPolicies     policies;
        bool           autoAcceptJoin = false;
        bool           requireAcceptJoin = true;
        //bool           gotJoinRequestEvent = false;
        //VNGTimeout     joinRequestTimeout;
        VNGGameInfo    info;
        char           comments[]  = "No comment";

        if (autoAcceptJoin)
            policies = autoAcceptPolicies;
        else if (requireAcceptJoin)
            policies = needAcceptPolicies;
        else
            policies = defaultPolicies;


        if ((ec = VNG_NewVN(vng, vn_game, VN_DEFAULT_CLASS,
                                          domain, policies))) {
            trace (ERR, TEST_1, "VNG_NewVN() Failed %d\n", ec);
            goto logout;
        } else {
            trace (INFO, TEST_1, "VNG_NewVN() Success\n");
        }

        if ((ec = VN_GetVNId(vn_game, &vnId_game))) {
            trace (ERR, TEST_1, "VN_GetVNId() Failed %d for game host\n", ec);
            goto logout;
        }

        trace (INFO, TEST_1, "gameHost created game vn %p  "
                     "deviceId: 0x%llx   netId: 0x%x\n",
                     vn_game, vnId_game.deviceId, vnId_game.netId);

        // register game

        info.vnId = vnId_game;
        info.owner = VNG_MyUserId(vng);
        info.gameId = GAME_ID;
        info.titleId = GAME_TITLE_ID;
        info.accessControl = VNG_GAME_PUBLIC;  // should be called viewability control
        info.netZone = 0;      // server will decide this value
        info.totalSlots = 4;
        info.buddySlots = 0;   // nothing reserved for buddies
        info.maxLatency = 100; // 100ms RTT 
        info.attrCount = 1;
        strncpy(info.keyword, GAME_KEYWORD, sizeof info.keyword);
        info.gameAttr[SUBGAME_ATTR_ID] = SUBGAME_VAL;

        if ((ec = VNG_RegisterGame (vng, &info, comments, timeout))) {
            trace (ERR, TEST_1, "VNG_RegisterGame() Failed %d\n", ec);
            goto logout;
        }
        trace (INFO, TEST_1, "VNG_RegisterGame() Success\n");


        gameStatus[0].gameInfo.vnId = vnId_game;
        gameStatus[0].numPlayers = 45;
        gameStatus[0].gameStatus = 314310;

        ec = VNG_GetGameStatus (vng, gameStatus, 1, timeout);
        if (ec) {
            trace (ERR, TEST_1, "VNG_GetGameStatus Failed %d\n", ec);
            goto logout;
        }
        dumpGameInfo(&gameStatus[0]);
        if (gameStatus[0].gameStatus != 0 || gameStatus[0].numPlayers != 0) {
            trace (ERR, TEST_1, "gameStatus[0].gameStatus != 0 || gameStatus[0].numPlayers != 0\n");
            goto logout;
        }

        ec = VNG_UpdateGameStatus (vng, vnId_game, GAME_STATUS, NUM_PLAYERS, timeout);
        if (ec) {
            trace (ERR, TEST_1, "VNG_UpdateGameStatus Failed %d\n", ec);
            goto logout;
        }

        ec = VNG_GetGameStatus (vng, gameStatus, 1, timeout);
        if (ec) {
            trace (ERR, TEST_1, "VNG_GetGameStatus Failed %d\n", ec);
            goto logout;
        }

        dumpGameInfo(&gameStatus[0]);
        if (gameStatus[0].gameStatus != GAME_STATUS || gameStatus[0].numPlayers != NUM_PLAYERS) {
            trace (ERR, TEST_1,
            "gameStatus[0].gameStatus != GAME_STATUS || gameStatus[0].numPlayers != NUM_PLAYERS\n");
            goto logout;
        }

        joiner = VN_MEMBER_ANY;

        if (requireAcceptJoin) {
            trace (INFO, TEST_1, "Game host waiting for join request.\n");
            while (VN_NumMembers(vn_game) <= 1) {
                uint64_t requestId;
                ec = VNG_GetJoinRequest(vng, info.vnId, NULL, 
                    &requestId, &userInfo, NULL, 0, 120*1000 /*ms*/);
                trace (INFO, TEST_1, "VNG_GetJoinRequest returned %d\n", ec);
                if (ec != VNG_OK) continue;
                
                VNG_AcceptJoinRequest(vng, requestId);
            }
        }

        rcvdMsgLen = sizeof msg;
        if ((ec = VN_RecvMsg (vn_game, joiner, VN_SERVICE_TAG_ANY,
                                  msg, &rcvdMsgLen, &hdr, recvMsgTimeout))) {
            trace (ERR, TEST_1, "Game Host VN_RecvMsg() Failed %d\n", ec);
            if (ignoreMissingJoiner && ec == VNGERR_TIMEOUT) {
                ec = 0;
            }
        } else {
            trace (INFO, TEST_1, "Game Host VN_RecvMsg() Success\n");
            joiner = hdr.sender;
        }

        if (ec) {
            goto logout;
        }

        rcvdMsgLen = sizeof msg; /* Get messge from checkGetVNs() */
        if ((ec = VN_RecvMsg (vn_game, joiner, VN_SERVICE_TAG_ANY,
                                  msg, &rcvdMsgLen, &hdr, recvMsgTimeout))) {
            trace (ERR, TEST_1, "Game Host VN_RecvMsg() from checkGetVNs() Failed %d\n", ec);
            if (ignoreMissingJoiner && ec == VNGERR_TIMEOUT) {
                ec = VNG_OK;
            }
        } else {
            trace (INFO, TEST_1, "Game Host VN_RecvMsg() from checkGetVNs() Success\n");
        }

        if ((ec = checkGetVNs(vng, 1, joiner))) {
            trace (ERR, TEST_1, "Game Host checkGetVNs() Failed\n");
            if (ignoreMissingJoiner)
                ec = VNG_OK;
            else
                goto logout;
        }

        if ((ec = VNG_UnregisterGame(vng, vnId_game, timeout))) {
            trace (ERR, TEST_1, "VNG_UnregisterGame() Failed %d\n", ec);
            goto logout;
        }

        if ((ec = VNG_DeleteVN (vng, vn_game))) {
            trace (ERR, TEST_1, "VNG_DeleteVN()  Failed: %d\n", ec);
            goto logout;
        }
        trace (INFO, TEST_1, "VNG_DeleteVN() Success\n");
    }
    else {
        VNGSearchCriteria  crit;
        char     denyReason [1024];
        int      i;
        size_t   msgLen;
        int      match;
        uint32_t n;

        if (domain == VN_DOMAIN_INFRA)
            crit.domain = VNG_SEARCH_INFRA;
        else
            crit.domain = VNG_SEARCH_ADHOC;

        crit.gameId = GAME_ID;
        crit.maxLatency = 30000;
#if 0
        crit.cmpKeyword = VNG_CMP_DONTCARE;
#else
        crit.cmpKeyword = VNG_CMP_STR;
        strncpy(crit.keyword, GAME_KEYWORD, sizeof crit.keyword);
#endif

        for (i = 0; i < VNG_MAX_PREDICATES; i++) {
            crit.pred[i].cmp = VNG_CMP_DONTCARE;
        }
        crit.pred[SUBGAME_ATTR_ID].attrId = SUBGAME_ATTR_ID;
        crit.pred[SUBGAME_ATTR_ID].attrValue = SUBGAME_VAL;
        crit.pred[SUBGAME_ATTR_ID].cmp = VNG_CMP_EQ;


        dumpSearchCriteria(&crit);

        n = sizeof gameStatus / sizeof gameStatus[0];

        if ((ec = VNG_SearchGames(vng, &crit, gameStatus, &n, 0, timeout))) {
            trace (ERR, TEST_1, "VNG_SearchGames()  Failed: %d\n", ec);
            goto logout;
        }

        if (n < 1) {
            trace (ERR, TEST_1, "VNG_SearchGames()  returned num games found: %d\n", n);
            rv = -1;
            goto logout;
        }

        match = 0;
        trace (INFO, TEST_1, "Found %d games:\n", n);
        for (i = 0; i < (int) n; i++) {
            if (gameStatus[i].gameStatus == GAME_STATUS && gameStatus[i].numPlayers == 7)
                match++;
            dumpGameInfo(&gameStatus[i]);
        }

        if (match != n) {
            trace (ERR, TEST_1, "number of matches %d\n", match);
        }


        for (i = n-1;  i >= 0;  ++i) {
            vnId_game     = gameStatus[i].gameInfo.vnId;
            joined_gameId = gameStatus[i].gameInfo.gameId;

            if ((ec = VNG_JoinVN (vng, vnId_game, "Please let me join !",
                                    vn_j, denyReason, sizeof denyReason,
                                    joinTimeout))) {
                if (ec == VNGERR_TIMEOUT) {
                    continue;
                }
                trace (ERR, TEST_1, "VNG_JoinVN() Failed: %d\n", ec);
                goto logout;
            }
            break;
        }

        if (i == n) {
            trace (ERR, TEST_1, "VNG_JoinVN() Timed out attempting to join %d games\n", n);
            goto logout;
        }

        trace (INFO, TEST_1, "VNG_JoinVN() Success\n");

        strncpy (msg, "Hello from joiner !", sizeof msg);
        msgLen = strnlen (msg, sizeof msg);

        if ((ec = VN_SendMsg (vn_j, VN_Owner(vn_j), VN_SERVICE_TAG_ANY,
                              msg, msgLen, VN_DEFAULT_ATTR, VNG_NOWAIT))) {
            trace (ERR, TEST_1, "VN_SendMsg()  Failed: %d\n", ec);
            goto logout;
        }
        trace (INFO, TEST_1, "Joiner VN_SendMsg() Success\n");

        if ((ec = checkGetVNs(vng, 1, VN_MEMBER_OWNER))) {
            trace (ERR, TEST_1, "Joiner checkGetVNs() Failed\n");
            goto logout;
        }

        rcvdMsgLen = sizeof msg; /* Get messge from checkGetVNs() */
        if ((ec = VN_RecvMsg (vn_j, VN_MEMBER_OWNER, VN_SERVICE_TAG_ANY,
                                  msg, &rcvdMsgLen, &hdr, recvMsgTimeout))) {
            trace (ERR, TEST_1,
                "Joiner VN_RecvMsg() from checkGetVNs() Failed %d\n", ec);
            if (ignoreMissingGameHost && ec == VNGERR_TIMEOUT) {
                ec = VNG_OK;
            }
        } else {
            trace (INFO, TEST_1, "Joiner VN_RecvMsg() from checkGetVNs() Success\n");
        }

        {
            bool       done = false;
            VNGEvent   ev;
            VN        *vn;
            VNMember   peer = 0;
            VNState    state;
            char      *str;

            while (!(ec=VNG_GetEvent(vng, &ev, VNG_WAIT))) {
                switch (ev.eid) {

                case VNG_EVENT_LOGOUT:
                    trace (INFO, TEST_1, "Joiner caught VNG_EVENT_LOGOUT");
                    done = true;
                    break;

                case VNG_EVENT_PEER_STATUS:
                    vn = ev.evt.peerStatus.vn;
                    peer = ev.evt.peerStatus.memb;
                    state = VN_State(vn);
                    if (state == VN_EXITED) {
                        str = "Exited";
                        done = true;
                    } else {
                        str = "OK";
                    }
                    trace (INFO, TEST_1,
                           "Joiner caught VNG_EVENT_PEER_STATUS for peer %d  "
                           "vn %p  vn->id  %d   vn->vng_id  %d  vn->addr  0x0%08x  state %s\n",
                           peer, vn, vn->id, vn->vng_id, vn->addr, str);
                    break;

                default:
                    trace (INFO, TEST_1, "Joiner caught event: %d\n", ev.eid);
                    break;
                }
                if (done) {
                    break;
                }
            }
         }
    }

    dumpEvents(vng, (VNGEventID)0);

logout:
    if (checkConnectStatus(vng, VNGT_ST_INIT_LOGIN, cstatDontCare, showCorrect)) {
        trace (ERR, TEST_1, "checkConnectStatus(VNGT_ST_INIT_LOGIN) Failed\n");
        if (exitOnConnectStatusFail) {
            if (!rv && !ec) {
                rv = -1;
            }
        }
    }
    if (!rv)
        rv = ec;
    if ((ec = VNG_Logout (vng, timeout))) {
        trace (ERR, TEST_1, "VNG_Logout()  Failed: %d\n", ec);
    } else {
        trace (INFO, TEST_1, "VNG_Logout() Success\n");
        if (checkConnectStatus(vng, VNGT_ST_INIT, cstatDontCare, showCorrect)) {
            trace (ERR, TEST_1, "checkConnectStatus(VNGT_ST_INIT) Failed\n");
            if (exitOnConnectStatusFail) {
                if (!rv && !ec) {
                    rv = -1;
                }
            }
        }
    }

fini:
    if (!rv)
        rv = ec;
    if ((ec = VNG_Fini (vng))) {
        trace (ERR, TEST_1, "VNG_Fini()  Failed: %d\n", ec);
    } else {
        trace (INFO, TEST_1, "VNG_Fini() Success\n");
        if (checkConnectStatus(vng, VNGT_ST_FINI, 0, showCorrect)) {
            trace (ERR, TEST_1, "checkConnectStatus(VNGT_ST_FINI) Failed\n");
            if (exitOnConnectStatusFail) {
                if (!rv && !ec) {
                    rv = -1;
                }
            }
        }
    }

end:
    if (!rv)
        rv = ec;

    if (rv > 0) {
        if ((ec = VNG_ErrMsg (rv, msg, sizeof msg))) {
            if (VNG_ErrMsg (ec, msg, sizeof msg)) {
                msg[0] = '\0';
            }
            trace (INFO, TEST_1, "VNG_ErrMsg(%d)  Failed %d: %s\n", rv, ec, msg);
        } else {
            trace (INFO, TEST_1, "VNG_ErrMsg(%d):  %s\n", rv, msg);
        }
    }

    printf ("*** VNG Api.test.1 TEST  %s\n", passFailStr(rv));
    return rv;
}


#ifdef _SC

#define MAXCMDLINE 512
#define MAXARGS 20

static char cmdline[MAXCMDLINE];
char* argv[MAXARGS];


void setArgs (char * executable_name, int *argc_out, char*** argv_out)
{
    char* ptr;
    int   argc = 0;

    #if defined(_SCE)
        get_cmdline(cmdline, (unsigned long) sizeof(cmdline));
    #endif
    if (cmdline[0]) {
        trace (INFO, MISC, "cmdline %s\n", cmdline);
    } else {
        trace (INFO, MISC, "No command line arguments passed.\n");
    }

    /* Put args in argc, argv for use with getopt */
    argc = 1;
    argv[0] = executable_name;
    ptr = cmdline;
    while (*ptr) {
        if (*ptr == ' ') { 
            *ptr = '\0';
        }
        else {
            if ((ptr == cmdline) || (*(ptr-1) == '\0')) {
                argv[argc] = ptr;
                argc++;
            }
        }
        ptr++;
    }

    *argc_out = argc;
    *argv_out = argv;
}

#endif /* _SC */



#ifndef _SC
    int main(int argc, char** argv)
    {
#else
    int main()
    {
        int        argc;
        char     **argv;    
        char       *executable_name = "vngt_api";
#endif
    VNGErrCode ec;

    printf ("*** BEGIN VNG.Api.tests\n");
    _SHR_set_trace_level( FINE );
    trace (INFO, MISC, "Set trace_level FINE\n");
    _SHR_set_grp_trace_level(_TRACE_SHR, TRACE_FINE);
    trace (INFO, MISC, "Set _TRACE_SHR trace_levedl FINE\n");
    _SHR_thread_sleep (1*1000);

    #ifdef _SC
        setArgs (executable_name, &argc, &argv);
    #endif

    processCmdLine(argc, argv);

    #if defined(_LINUX) && defined(_VN_RPC_DEVICE)
        if (__usb_proxy_host) {
            _vn_netif_set_usb_proxy_host(__usb_proxy_host);
        }
        if (__usb_proxy_port) {
            _vn_netif_set_usb_proxy_port(__usb_proxy_port);
        }
    #endif

    if ((ec = chkMemAlloc())) {
        goto end;
    }

    if ((ec = vng_Api_1(__domain,
                           __vngs_host, __vngs_port,
                           __isGameHost,
                           __vng_timeout))) {
        goto end;
    }


end:
    printf ("*** END VNG.Api.tests %s\n", passFailStr(ec));

    exit(ec);
    return 0;
}

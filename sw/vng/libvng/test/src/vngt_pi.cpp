//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//


#include "vngt.h"




/*****************************************************

Test player invite

Run a set of test cases with different paramaters.

See vngt_test.cpp for general info about tests.

Test with owner and joiner on same PC and different PCs
Test with single VN and multiple VNs

For each test case:

    1 or more owners create a vn and register a game
    1 or more joiners get the game id by geting\
        buddy status of game owner and joining vn
        indicated by vnId in VNGBuddyStatus

    confirm:
    - all invitations receved and responded to as expected for test case

PiPlayer specific features

    - tests VNG_Invite and VNG_GetInvitation

    - Each owner/player has
        set of cmds to do

A typical player invite test case overview:

    owners  registers games
    joiners register buddy status
    joiners wait for invitation event

    owner gets buddy status of one or more buddies
    owner sends invitation to buddies
    owner waits for join requests
    joiners receive invitation event
    joiners request join game

    owner accepts join requests
    joiners send a msg to owner
    owner responds to msg
    owner sends msg to joiners

    joiners recv msg response
    joinesr recv msg from owner
    joiners exit with pass/fail
    owner waits till no members
    owner exits with pass/fail


*****************************************************/




int doPiTest (TestEnv& tests)
{
    int rv;

    PiTest&  test = tests.piTest;

    test.iters = tests.iterations;

    if ((rv = test.initPiTest (tests)))
        return rv;
    TestCase c;

    
    return doTest (tests);
}








/* comn cmds: 0  - exit now (also at end of cmd string)
              1  - leave vn
              2  - delete vn
              3  - login
              4  - logout
              5  - wait for buddies on line

              h-k  - send req ascii h-k
              H-K  - wait for req ascii h-k and send resp
              l-q  - send msg ascii l-q
              L-Q  - wait for msg ascii l-q
              t-v  - send rpc t-v
              T-V  - register rpc t-v or if immediately followd by t-v, wait fore req t-v
              x    - send rpc x to server to cmd vng fini
              z    - sleep for _VNGT_BIDE_TIME

              *  -   wait for msg on any svc channel for any memb
              X  -   respond to ready channel indicated by * and retrieve all other msgs
              W  -   wait for msg on selectFrom svc channel for any memb
              w  -   wait for msg on selectFrom svc channel for any memb
                     and send corresponding response (w for msg, resp or rpc resp)
              R  -   wait for msg on selectFrom svc channel for selectMemb
                     and send cooresponding response (r for msg, resp or rpc resp)
              r  -   wait for msg on selectFrom svc channel for selectMemb

   hostCmds:  a  - wait for join request event
              b  - wait for join request
              c  - accept join request
              d  - reject join request
              e  - send game invitaion
              f  - wait for no other players


  joinerCmds: a  - get game host VNId
              b  - request join
              c  - wait for game invite event
              d  - get game invite info

*/




int PiTest::initPiTest (TestEnv& tests)
{
    // TODO: if there are cmd line args for test, adjust test accordingly

    static TestCase case1 ("case");
    static TestCase case2 ("case");
    static TestCase case3 ("case");

    static PiPlayer   gameHost_1 (tests, "eabc*Xnzzf0");
    static PiPlayer   joiner_1_1 (tests, "cdbzmMNz0", 1);

    static PiPlayer   gameHost_2 (tests, "eabcbcbc*Xnzzf0");
    static PiPlayer   joiner_1_2 (tests, "dbzmMNz0", 1);
    static PiPlayer   joiner_2_2 (tests, "azbzzz0",  2);
    static PiPlayer   joiner_3_2 (tests, "azzbzzzz0",3);

    static PiPlayer   gameHost_3 (tests, "eabcbcbc*Xnzzf0");
    static PiPlayer   joiner_1_3 (tests, "dbzMNzzzz0", 1);
    static PiPlayer   joiner_2_3 (tests, "dzb5zzzmz0",  2);
    static PiPlayer   joiner_3_3 (tests, "5dzzbzzzztzz0",3);

    case1.players.push_back(&gameHost_1);
    case1.players.push_back(&joiner_1_1);
    //cases.push_back(case1);

    case2.players.push_back(&gameHost_2);
    case2.players.push_back(&joiner_1_2);
    case2.players.push_back(&joiner_2_2);
    case2.players.push_back(&joiner_3_2);
    //cases.push_back(case2);

    case3.players.push_back(&gameHost_3);
    case3.players.push_back(&joiner_1_3);
    case3.players.push_back(&joiner_2_3);
    case3.players.push_back(&joiner_3_3);
    cases.push_back(case3);

    return 0;
}

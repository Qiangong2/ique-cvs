#ifndef __VNGT_PLATFORM_H__
#define __VNGT_PLATFORM_H__  1

#ifdef _WIN32
    #pragma once
#endif

//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#define _SHR_DONT_DEFINE_BOOL 1
#include "shr_plat.h"
#include "vng.h"
#include "shr_th.h"
#include "shr_mem.h"


#ifdef _WIN32
    #if _MSC_VER > 1000    
        #pragma once
    #endif
    #ifdef __cplusplus
        #include <hash_map>
        using namespace std;
        using namespace stdext;
    #endif
#elif defined(_LINUX)
    #ifdef __cplusplus
        #include <ext/hash_map>// hash
        using namespace std;
        using namespace __gnu_cxx;
    #endif
#elif defined (_SC)
    #ifdef __cplusplus
        #include <ext/hash_map>// hash
        using namespace std;
        using namespace __gnu_cxx;
    #else
        #define fprintf(stream, fmt, args...) do {printf(fmt, ## args);} while (0)
        #define fflush(stream)
    #endif
#endif

#ifdef __cplusplus
    #include <string>
    #include <iostream>
#elif defined(_WIN32) || defined(_LINUX)
    #include <stdio.h>
#endif

#include <ctype.h>


#ifdef __cplusplus
extern "C" {
#endif

void *vngt_malloc (size_t size);
int   vngt_free (void *ptr);


#ifdef  __cplusplus
}
#endif


// The following are only available in c++

#ifdef __cplusplus

/*
  eqstr:

  A function object used for string hash_map.  strings (i.e., const
  char *) are considered equal when the strcmp returns 0.

  namespace __gnu_cxx
{
template<> struct hash<const Key>
{
size_t operator()(const Key &x) const
{
const char *tempBuffer = x.buffer;
hash<const char *> hs;
return hash<size_t>()(hs(tempBuffer));
}
};
}


*/

struct eqstr {
    bool operator()(const char* s1, const char* s2) const
    {
        return strcmp(s1, s2) == 0;
    }
};

#ifndef _WIN32
    /* used by ordered set<> */
    struct ltstr
    {
        bool operator()(const char* s1, const char* s2) const {
                return strcmp(s1, s2) < 0;
        }
    };
#else
    // used by map, hash_map, hash_set, etc.
    template<>
    class hash_compare<const char*>
    {
      public:

        enum { bucket_size = 4, min_buckets = 8 };

        size_t operator()(const char* s) const
        {
            size_t h = 0;
            for (; *s; ++s)
                h = 5 * h + *s;
            return h;
        }

        bool operator()(const char* s1, const char* s2) const
        {
            return strcmp(s1, s2) < 0;
        }
    };

    typedef stdext::hash_compare<const char*> ltstr;

#endif

struct eqstring {
    bool operator()(const string& s1, const string& s2) const
    {
        return s1 == s2;
    }
};

#ifndef _WIN32
    struct ltstring
    {
        bool operator()(const string& s1, const string& s2) const {
            return s1 < s2;
        }
    };
#else
    template<>
    class hash_compare<string>
    {
      public:

        enum { bucket_size = 4, min_buckets = 8 };
      
        size_t operator() (const string& str) const
        {
            size_t h = 0;
            const char* s = str.c_str();
            for (; *s; ++s)
                h = 5 * h + *s;
            return (h);
        }
      
        bool operator()(const string& s1, const string& s2) const {
            return s1 < s2;
        }
    };

    typedef stdext::hash_compare<string> ltstring;

#endif

struct lt_numstring {
    bool operator()(const string& a, const string& b)
    {
        return atoi(a.c_str()) < atoi(b.c_str());
    }
};

#ifndef _WIN32
    namespace __gnu_cxx
    {
        template<> struct hash<string> {
            size_t operator()(const string& s) const {
                return ::hash<const char *>()(s.c_str());
            }
        };

        template<> struct hash<uint64_t> {
            size_t operator()(uint64_t ull) const {
                return (size_t) (ull >> 16 | ull);
            }
        };

    }

    struct equll {
        bool operator()(uint64_t s1, uint64_t s2) const
        {
            return s1 == s2;
        }
    };

#endif


#ifndef _WIN32
    typedef hash_map<string, string, hash<string>, eqstring> HashMapString;
#else
    typedef hash_map<string, string, ltstring> HashMapString;
#endif

int loadConfFile (const char *filename, HashMapString& var);

void dumpHashMapString (HashMapString& pairs, ostream& out = cout);



#endif /* __cplusplus */

#endif  /* __VNGT_PLATFORM_H__ */


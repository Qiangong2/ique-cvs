#if defined(_SC)
    extern "C" {
    #include "sc/ios.h"
    #include "ioslibc.h"
    }

    #define STACK_SIZE (64*1024)
    u8 _initStack[STACK_SIZE];
    u32 _initStackSize = sizeof(_initStack);
    u32 _initPriority = 15;
    extern "C" int sscanf(char *, char *, ...);
#else
    #include <stdio.h>
    #include <stdlib.h>
#endif

char *s = "123 abc";

class Foo {
    int a;
public:
    Foo(void) {a = 10; b = 20;}
    int b;
    void bar(int c) {printf("a = %d, b = %d, c = %d\n", a, b++, c);}
    void baz(void) {int i; char str[4];sscanf(s, "%d %s", &i, str);printf("i = %d, str = %s\n", i, str);}
};

int main(void)
{
    Foo f;
    printf("Hello\n");
    f.bar(30);
    printf("b = %d\n", f.b);
    f.baz();
    exit(0);
    return 0;
}



#include <Agb.h>
#include "scrpc.h"
#include "vng_sc_rpc.h"
#include "vng.h"

#define GBA_SC_MB_OUT_ADDR   (0x08000000+SC_MAILBOX_OFFSET)
#define GBA_SC_MB_CTRL_ADDR  (0x08000000+SC_MAILBOX_OFFSET+0x8)
#define GBA_SM_BUF_BASE (0x08000000+SC_RPC_RAM_OFFSET+0x10000)


static void scrpc(void *pCmd)
{
    RPCHeader *hdr = (RPCHeader*)pCmd;
    *(uint32_t*)GBA_SC_MB_OUT_ADDR = (uint32_t)pCmd;
    *(uint16_t*)GBA_SC_MB_CTRL_ADDR = 1;
    while (hdr->status==RPC_PENDING);
}


VNGErrCode VNG_Init (VNG *vng)
{
    VNG_INIT_CMD *pCmd = (VNG_INIT_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_INIT;
    pCmd->header.inLen = sizeof(VNG_INIT_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vng = vng;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        return pCmd->out;
    } else {
        return VNGERR_UNKNOWN;
    }
}

VNGErrCode VNG_Fini (VNG *vng)
{
    VNG_FINI_CMD *pCmd = (VNG_FINI_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_FINI;
    pCmd->header.inLen = sizeof(VNG_FINI_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vng = vng;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        return pCmd->out;
    } else {
        return VNGERR_UNKNOWN;
    }
}

VNGErrCode VNG_Login (VNG        *vng,
                      const char *serverName, 
                      VNGPort     serverPort,
                      const char *loginName,
                      const char *passwd,
                      int32_t     loginType,
                      VNGTimeout  timeout)
{
    VNG_LOGIN_CMD *pCmd = (VNG_LOGIN_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_LOGIN;
    pCmd->header.inLen = sizeof(VNG_LOGIN_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vng = vng;
    strncpy(pCmd->in.serverName, serverName, sizeof(pCmd->in.serverName));;
    pCmd->in.serverPort = serverPort;
    strncpy(pCmd->in.loginName,loginName, sizeof(pCmd->in.loginName));
    strncpy(pCmd->in.passwd, passwd, sizeof(pCmd->in.passwd));
    pCmd->in.loginType = loginType;
    pCmd->in.timeout = timeout;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        return pCmd->out;
    } else {
        return VNGERR_UNKNOWN;
    }
}

VNGErrCode VNG_Logout (VNG        *vng, 
                       VNGTimeout  timeout)
{
    VNG_LOGOUT_CMD *pCmd = (VNG_LOGOUT_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_LOGOUT;
    pCmd->header.inLen = sizeof(VNG_LOGOUT_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vng = vng;
    pCmd->in.timeout = timeout;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        return pCmd->out;
    } else {
        return VNGERR_UNKNOWN;
    }
}

int32_t VNG_GetLoginType (VNG *vng)
{
    VNG_GETLOGINTYPE_CMD *pCmd = (VNG_GETLOGINTYPE_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_GETLOGINTYPE;
    pCmd->header.inLen = sizeof(VNG_GETLOGINTYPE_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vng = vng;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        return pCmd->out;
    } else {
        return 0;
    }
}

VNGErrCode VNG_GetUserInfo (VNG         *vng,
                            VNGUserId    uid,
                            VNGUserInfo *uinfo,  // out 
                            VNGTimeout   timeout)
{
    VNG_GETUSERINFO_CMD *pCmd = (VNG_GETUSERINFO_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_GETUSERINFO;
    pCmd->header.inLen = sizeof(VNG_GETUSERINFO_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vng = vng;
    pCmd->in.uid = uid;
    pCmd->in.uinfo = uinfo;
    pCmd->in.timeout = timeout;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        return pCmd->out;
    } else {
        return VNGERR_UNKNOWN;
    }
}

VNGErrCode VNG_NewVN (VNG       *vng,
                      VN        *vn,
                      VNClass    vnClass,
                      VNDomain   domain,
                      VNPolicies policies)
{
    VNG_NEWVN_CMD *pCmd = (VNG_NEWVN_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_NEWVN;
    pCmd->header.inLen = sizeof(VNG_NEWVN_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vng = vng;
    pCmd->in.vn = vn;
    pCmd->in.vnClass = vnClass;
    pCmd->in.domain = domain;
    pCmd->in.policies = policies;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        return pCmd->out;
    } else {
        return VNGERR_UNKNOWN;
    }
}

VNGErrCode VNG_DeleteVN (VNG *vng, VN *vn)
{
    VNG_DELETEVN_CMD *pCmd = (VNG_DELETEVN_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_DELETEVN;
    pCmd->header.inLen = sizeof(VNG_DELETEVN_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vng = vng;
    pCmd->in.vn = vn;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        return pCmd->out;
    } else {
        return VNGERR_UNKNOWN;
    }
}

VNGErrCode VNG_JoinVN (VNG         *vng,        // in
                       VNId         vnId,       // in
                       const char  *msg,        // in
                       VN          *vn,         // out
                       char        *denyReason, // out
                       size_t       denyReasonLen, // in
                       VNGTimeout   timeout)
{
    VNG_JOINVN_CMD *pCmd = (VNG_JOINVN_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_JOINVN;
    pCmd->header.inLen = sizeof(VNG_JOINVN_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vng = vng;
    pCmd->in.vnId = vnId;
    pCmd->in.msg = msg;
    pCmd->in.vn = vn;
    pCmd->in.denyReason = denyReason;
    pCmd->in.denyReasonLen = denyReasonLen;
    pCmd->in.timeout = timeout;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        return pCmd->out;
    } else {
        return VNGERR_UNKNOWN;
    }
}

VNGErrCode VNG_GetJoinRequest (VNG         *vng,
                               VNId         vnId, // in
                               VN         **vn,   // out 
                               uint64_t    *requestId,
                               VNGUserInfo *userInfo,
                               char        *joinReason,
                               size_t       joinReasonLen,
                               VNGTimeout   timeout)
{
    VNG_GETJOINREQUEST_CMD *pCmd = (VNG_GETJOINREQUEST_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_GETJOINREQUEST;
    pCmd->header.inLen = sizeof(VNG_GETJOINREQUEST_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vng = vng;
    pCmd->in.vnId = vnId;
    pCmd->in.requestId = *requestId;
    pCmd->in.userInfo = userInfo;
    pCmd->in.joinReason = joinReason;
    pCmd->in.joinReasonLen = joinReasonLen;
    pCmd->in.timeout = timeout;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        if (vn) *vn = pCmd->in.vn;
        *requestId = pCmd->in.requestId;
        return pCmd->out;
    } else {
        return VNGERR_UNKNOWN;
    }
}

VNGErrCode VNG_AcceptJoinRequest (VNG        *vng, 
                                  uint64_t    requestId)
{
    VNG_ACCEPTJOINREQUEST_CMD *pCmd = (VNG_ACCEPTJOINREQUEST_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_ACCEPTJOINREQUEST;
    pCmd->header.inLen = sizeof(VNG_ACCEPTJOINREQUEST_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vng = vng;
    pCmd->in.requestId = requestId;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        return pCmd->out;
    } else {
        return VNGERR_UNKNOWN;
    }
}

VNGErrCode VNG_RejectJoinRequest (VNG        *vng, 
                                  uint64_t    requestId,      
                                  const char *reason)
{
    VNG_REJECTJOINREQUEST_CMD *pCmd = (VNG_REJECTJOINREQUEST_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_REJECTJOINREQUEST;
    pCmd->header.inLen = sizeof(VNG_REJECTJOINREQUEST_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vng = vng;
    pCmd->in.requestId = requestId;
    pCmd->in.reason = reason;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        return pCmd->out;
    } else {
        return VNGERR_UNKNOWN;
    }
}

VNGErrCode VNG_LeaveVN (VNG *vng, VN *vn)
{
    VNG_LEAVEVN_CMD *pCmd = (VNG_LEAVEVN_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_LEAVEVN;
    pCmd->header.inLen = sizeof(VNG_LEAVEVN_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vng = vng;
    pCmd->in.vn = vn;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        return pCmd->out;
    } else {
        return VNGERR_UNKNOWN;
    }
}

uint32_t  VN_NumMembers (VN *vn)
{
    VNG_NUMMEMBERS_CMD *pCmd = (VNG_NUMMEMBERS_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_NUMMEMBERS;
    pCmd->header.inLen = sizeof(VNG_NUMMEMBERS_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vn = vn;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        return pCmd->out;
    } else {
        return 0;
    }
}

uint32_t  VN_GetMembers (VN       *vn,
                         VNMember *members,
                         uint32_t  nMembers)
{
    VNG_GETMEMBERS_CMD *pCmd = (VNG_GETMEMBERS_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_GETMEMBERS;
    pCmd->header.inLen = sizeof(VNG_GETMEMBERS_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vn = vn;
    pCmd->in.members = members;
    pCmd->in.nMembers = nMembers;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        return pCmd->out;
    } else {
        return 0;
    }
}

VNGErrCode  VN_MemberState (VN *vn, VNMember memb)
{
    VNG_MEMBERSTATE_CMD *pCmd = (VNG_MEMBERSTATE_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_MEMBERSTATE;
    pCmd->header.inLen = sizeof(VNG_MEMBERSTATE_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vn = vn;
    pCmd->in.memb = memb;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        return pCmd->out;
    } else {
        return VNGERR_UNKNOWN;
    }
}

VNGUserId  VN_MemberUserId (VN        *vn,
                            VNMember   memb,
                            VNGTimeout timeout)
{
    VNG_MEMBERUSERID_CMD *pCmd = (VNG_MEMBERUSERID_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_MEMBERUSERID;
    pCmd->header.inLen = sizeof(VNG_MEMBERUSERID_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vn = vn;
    pCmd->in.memb = memb;
    pCmd->in.timeout = timeout;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        return pCmd->out;
    } else {
        return VNG_INVALID_USER_ID;
    }
}

VNGMillisec  VN_MemberLatency (VN *vn, VNMember memb)  
{
    VNG_MEMBERLATENCY_CMD *pCmd = (VNG_MEMBERLATENCY_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_MEMBERLATENCY;
    pCmd->header.inLen = sizeof(VNG_MEMBERLATENCY_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vn = vn;
    pCmd->in.memb = memb;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        return pCmd->out;
    } else {
        return VN_MEMB_LATENCY_NA;
    }
}

VNGDeviceType    VN_MemberDeviceType (VN *vn, VNMember memb)
{
    VNG_MEMBERDEVICETYPE_CMD *pCmd = (VNG_MEMBERDEVICETYPE_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_MEMBERDEVICETYPE;
    pCmd->header.inLen = sizeof(VNG_MEMBERDEVICETYPE_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vn = vn;
    pCmd->in.memb = memb;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        return pCmd->out;
    } else {
        return 0;
    }
}

VNGErrCode VN_GetVNId (VN *vn, VNId *vnId)
{
    VNG_GETVNID_CMD *pCmd = (VNG_GETVNID_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_GETVNID;
    pCmd->header.inLen = sizeof(VNG_GETVNID_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vn = vn;
    pCmd->in.vnId = *vnId;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        *vnId = pCmd->in.vnId;
        return pCmd->out;
    } else {
        return VNGERR_UNKNOWN;
    }
}

VNGErrCode VN_Select (VN               *vn,
                      VNServiceTypeSet  selectFrom,
                      VNServiceTypeSet *ready,
                      VNMember          memb,
                      VNGTimeout        timeout)
{
    VNG_SELECT_CMD *pCmd = (VNG_SELECT_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_SELECT;
    pCmd->header.inLen = sizeof(VNG_SELECT_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vn = vn;
    pCmd->in.selectFrom = selectFrom;
    pCmd->in.ready = *ready;
    pCmd->in.memb = memb;
    pCmd->in.timeout = timeout;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        *ready =  pCmd->in.ready;
        return pCmd->out;
    } else {
        return VNGERR_UNKNOWN;
    }
}

VNGErrCode VN_SendMsg (VN            *vn,
                       VNMember       memb, 
                       VNServiceTag   serviceTag,
                       const void    *msg,
                       size_t         msglen,
                       VNAttr         attr,
                       VNGTimeout     timeout)
{
    VNG_SENDMSG_CMD *pCmd = (VNG_SENDMSG_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_SENDMSG;
    pCmd->header.inLen = sizeof(VNG_SENDMSG_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vn = vn;
    pCmd->in.memb = memb;
    pCmd->in.serviceTag = serviceTag;
    pCmd->in.msg = msg;
    pCmd->in.msglen = msglen;
    pCmd->in.attr = attr;
    pCmd->in.timeout = timeout;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        return pCmd->out;
    } else {
        return VNGERR_UNKNOWN;
    }
}

VNGErrCode VN_RecvMsg (VN           *vn, 
                       VNMember      memb,  
                       VNServiceTag  serviceTag,
                       void         *msg,
                       size_t       *msglen,
                       VNMsgHdr     *hdr,
                       VNGTimeout    timeout)
{
    VNG_RECVMSG_CMD *pCmd = (VNG_RECVMSG_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_RECVMSG;
    pCmd->header.inLen = sizeof(VNG_RECVMSG_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vn = vn;
    pCmd->in.memb = memb;
    pCmd->in.serviceTag = serviceTag;
    pCmd->in.msg = msg;
    pCmd->in.msglen = *msglen;
    pCmd->in.hdr = hdr;
    pCmd->in.timeout = timeout;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        *msglen = pCmd->in.msglen;
        return pCmd->out;
    } else {
        return VNGERR_UNKNOWN;
    }
}

VNGErrCode VN_SendReq (VN           *vn, 
                       VNMember      memb,
                       VNServiceTag  serviceTag,  // function id
                       VNCallerTag  *callerTag,
                       const void   *msg,
                       size_t        msglen,
                       int32_t       optData,
                       VNGTimeout    timeout)
{
    VNG_SENDREQ_CMD *pCmd = (VNG_SENDREQ_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_SENDREQ;
    pCmd->header.inLen = sizeof(VNG_SENDREQ_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vn = vn;
    pCmd->in.memb = memb;
    pCmd->in.serviceTag = serviceTag;
    pCmd->in.callerTag = *callerTag;
    pCmd->in.msg = msg;
    pCmd->in.msglen = msglen;
    pCmd->in.optData = optData;
    pCmd->in.timeout = timeout;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        *callerTag = pCmd->in.callerTag;
        return pCmd->out;
    } else {
        return VNGERR_UNKNOWN;
    }
}

VNGErrCode VN_RecvReq (VN           *vn,
                       VNMember      memb,  
                       VNServiceTag  serviceTag,
                       void         *msg,
                       size_t       *msglen,
                       VNMsgHdr     *hdr,
                       VNGTimeout    timeout)
{
    VNG_RECVREQ_CMD *pCmd = (VNG_RECVREQ_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_RECVREQ;
    pCmd->header.inLen = sizeof(VNG_RECVREQ_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vn = vn;
    pCmd->in.memb = memb;
    pCmd->in.serviceTag = serviceTag;
    pCmd->in.msg = msg;
    pCmd->in.msglen = *msglen;
    pCmd->in.hdr = hdr;
    pCmd->in.timeout = timeout;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        *msglen = pCmd->in.msglen;
       return pCmd->out;
    } else {
        return VNGERR_UNKNOWN;
    }
}

VNGErrCode VN_SendResp (VN           *vn, 
                        VNMember      memb,
                        VNServiceTag  serviceTag,
                        VNCallerTag   callerTag,
                        const void   *msg,
                        size_t        msglen,
                        int32_t       optData,
                        VNGTimeout    timeout)
{
    VNG_SENDRESP_CMD *pCmd = (VNG_SENDRESP_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_SENDRESP;
    pCmd->header.inLen = sizeof(VNG_SENDRESP_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vn = vn;
    pCmd->in.memb = memb;
    pCmd->in.serviceTag = serviceTag;
    pCmd->in.callerTag = callerTag;
    pCmd->in.msg = msg;
    pCmd->in.msglen = msglen;
    pCmd->in.optData = optData;
    pCmd->in.timeout = timeout;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        return pCmd->out;
    } else {
        return VNGERR_UNKNOWN;
    }
}

VNGErrCode VN_RecvResp (VN           *vn,
                        VNMember      memb,  
                        VNServiceTag  serviceTag,
                        VNCallerTag   callerTag,  
                        void         *msg,
                        size_t       *msglen,
                        VNMsgHdr     *hdr,
                        VNGTimeout    timeout)
{
    VNG_RECVRESP_CMD *pCmd = (VNG_RECVRESP_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_RECVRESP;
    pCmd->header.inLen = sizeof(VNG_RECVRESP_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vn = vn;
    pCmd->in.memb = memb;
    pCmd->in.serviceTag = serviceTag;
    pCmd->in.callerTag = callerTag;
    pCmd->in.msg = msg;
    pCmd->in.msglen = *msglen;
    pCmd->in.hdr = hdr;
    pCmd->in.timeout = timeout;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        *msglen = pCmd->in.msglen;
        return pCmd->out;
    } else {
        return VNGERR_UNKNOWN;
    }
}

VNGErrCode VN_RegisterRPCService (VN               *vn,
                                  VNServiceTag      procId,
                                  VNRemoteProcedure proc)
{
    VNG_REGISTERRPCSERVICE_CMD *pCmd = (VNG_REGISTERRPCSERVICE_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_REGISTERRPCSERVICE;
    pCmd->header.inLen = sizeof(VNG_REGISTERRPCSERVICE_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vn = vn;
    pCmd->in.procId = procId;
    pCmd->in.proc = proc;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        return pCmd->out;
    } else {
        return VNGERR_UNKNOWN;
    }
}

VNGErrCode VN_UnregisterRPCService (VN               *vn,
                                    VNServiceTag      procId)
{
    VNG_UNREGISTERRPCSERVICE_CMD *pCmd = (VNG_UNREGISTERRPCSERVICE_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_UNREGISTERRPCSERVICE;
    pCmd->header.inLen = sizeof(VNG_UNREGISTERRPCSERVICE_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vn = vn;
    pCmd->in.procId = procId;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        return pCmd->out;
    } else {
        return VNGERR_UNKNOWN;
    }
}

VNGErrCode VN_SendRPC (VN           *vn, 
                       VNMember      memb,
                       VNServiceTag  serviceTag,
                       const void   *args,
                       size_t        argsLen,
                       void         *ret,
                       size_t       *retLen,
                       int32_t      *optData,
                       VNGTimeout    timeout)
{
    VNG_SENDRPC_CMD *pCmd = (VNG_SENDRPC_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_SENDRPC;
    pCmd->header.inLen = sizeof(VNG_SENDRPC_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vn = vn;
    pCmd->in.memb = memb;
    pCmd->in.serviceTag = serviceTag;
    pCmd->in.args = args;
    pCmd->in.argsLen = argsLen;
    pCmd->in.ret = ret;
    pCmd->in.retLen = (retLen) ? *retLen : 0;
    pCmd->in.optData = (optData) ? *optData : 0;
    pCmd->in.timeout = timeout;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        if (retLen) *retLen = pCmd->in.retLen;
        if (optData) *optData = pCmd->in.optData;
        return pCmd->out;
    } else {
        return VNGERR_UNKNOWN;
    }
}

VNGErrCode VNG_RecvRPC (VNG               *vng, 
                        VNRemoteProcedure *proc,
                        VN               **vn,
                        VNMsgHdr          *hdr,
                        void              *args,
                        size_t            *argsLen,
                        VNGTimeout         timeout)
{
    VNG_RECVRPC_CMD *pCmd = (VNG_RECVRPC_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_RECVRPC;
    pCmd->header.inLen = sizeof(VNG_RECVRPC_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vng = vng;
    pCmd->in.proc = proc;
    pCmd->in.vn = vn;
    pCmd->in.hdr = hdr;
    pCmd->in.args = args;
    pCmd->in.argsLen = argsLen;
    pCmd->in.timeout = timeout;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        return pCmd->out;
    } else {
        return VNGERR_UNKNOWN;
    }
}

VNGErrCode VNG_GetEvent (VNG        *vng,
                         VNGEvent   *event,
                         VNGTimeout  timeout)
{
    VNG_GETEVENT_CMD *pCmd = (VNG_GETEVENT_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_GETEVENT;
    pCmd->header.inLen = sizeof(VNG_GETEVENT_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vng = vng;
    pCmd->in.event = event;
    pCmd->in.timeout = timeout;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        return pCmd->out;
    } else {
        return VNGERR_UNKNOWN;
    }
}

VNGErrCode VNG_Invite (VNG          *vng,
                       VNGGameInfo  *gameInfo,
                       VNGUserId    *uid,
                       uint32_t      nUsers,
                       const char   *inviteMsg)
{
    VNG_INVITE_CMD *pCmd = (VNG_INVITE_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_INVITE;
    pCmd->header.inLen = sizeof(VNG_INVITE_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vng = vng;
    pCmd->in.gameInfo = gameInfo;
    pCmd->in.uid = uid;
    pCmd->in.nUsers = nUsers;
    pCmd->in.inviteMsg = inviteMsg;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        return pCmd->out;
    } else {
        return VNGERR_UNKNOWN;
    }
}

VNGErrCode VNG_GetInvitation (VNG          *vng,
                              VNGUserInfo  *userInfo,
                              VNGGameInfo  *gameInfo,
                              char         *inviteMsg,
                              size_t        inviteMsgLen,
                              VNGTimeout    timeout)
{
    VNG_GETINVITE_CMD *pCmd = (VNG_GETINVITE_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_GETINVITE;
    pCmd->header.inLen = sizeof(VNG_GETINVITE_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vng = vng;
    pCmd->in.userInfo = userInfo;
    pCmd->in.gameInfo = gameInfo;
    pCmd->in.inviteMsg = inviteMsg;
    pCmd->in.inviteMsgLen = inviteMsgLen;
    pCmd->in.timeout = timeout;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        return pCmd->out;
    } else {
        return VNGERR_UNKNOWN;
    }
}

VNGErrCode VNG_RegisterGame (VNG          *vng,
                             VNGGameInfo  *info,
                             char         *comments,
                             VNGTimeout    timeout)
{
    VNG_REGISTERGAME_CMD *pCmd = (VNG_REGISTERGAME_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_REGISTERGAME;
    pCmd->header.inLen = sizeof(VNG_REGISTERGAME_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vng = vng;
    pCmd->in.info = info;
    pCmd->in.comments = comments;
    pCmd->in.timeout = timeout;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        return pCmd->out;
    } else {
        return VNGERR_UNKNOWN;
    }
}

VNGErrCode VNG_UnregisterGame (VNG            *vng,
                               VNId            vnId,
                               VNGTimeout      timeout)
{
    VNG_UNREGISTERGAME_CMD *pCmd = (VNG_UNREGISTERGAME_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_UNREGISTERGAME;
    pCmd->header.inLen = sizeof(VNG_UNREGISTERGAME_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vng = vng;
    pCmd->in.vnId = vnId;
    pCmd->in.timeout = timeout;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        return pCmd->out;
    } else {
        return VNGERR_UNKNOWN;
    }
}

VNGErrCode VNG_UpdateGameStatus (VNG            *vng,
                                 VNId            vnId,
                                 uint32_t        gameStatus,
                                 uint32_t        numPlayers,
                                 VNGTimeout      timeout)
{
    VNG_UPDATEGAMESTATUS_CMD *pCmd = (VNG_UPDATEGAMESTATUS_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_UPDATEGAMESTATUS;
    pCmd->header.inLen = sizeof(VNG_UPDATEGAMESTATUS_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vng = vng;
    pCmd->in.vnId = vnId;
    pCmd->in.gameStatus = gameStatus;
    pCmd->in.numPlayers = numPlayers;
    pCmd->in.timeout = timeout;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        return pCmd->out;
    } else {
        return VNGERR_UNKNOWN;
    }
}

VNGErrCode VNG_GetGameStatus (VNG            *vng,
                              VNGGameStatus  *gameStatus,
                              uint32_t        nGameStatus, 
                              VNGTimeout      timeout)
{
    VNG_GETGAMESTATUS_CMD *pCmd = (VNG_GETGAMESTATUS_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_GETGAMESTATUS;
    pCmd->header.inLen = sizeof(VNG_GETGAMESTATUS_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vng = vng;
    pCmd->in.gameStatus = gameStatus;
    pCmd->in.nGameStatus = nGameStatus;
    pCmd->in.timeout = timeout;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        return pCmd->out;
    } else {
        return VNGERR_UNKNOWN;
    }
}

VNGErrCode VNG_GetGameComments (VNG            *vng,
                                VNId            vnId,
                                char           *comments,
                                size_t         *commentSize, 
                                VNGTimeout      timeout)
{
    VNG_GETGAMECOMMENTS_CMD *pCmd = (VNG_GETGAMECOMMENTS_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_GETGAMECOMMENTS;
    pCmd->header.inLen = sizeof(VNG_GETGAMECOMMENTS_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vng = vng;
    pCmd->in.vnId = vnId;
    pCmd->in.comments = comments;
    pCmd->in.commentSize = (commentSize) ? *commentSize : 0;
    pCmd->in.timeout = timeout;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        if (commentSize) *commentSize = pCmd->in.commentSize;
        return pCmd->out;
    } else {
        return VNGERR_UNKNOWN;
    }
}

VNGErrCode VNG_SearchGames (VNG                 *vng,
                            VNGSearchCriteria   *searchCriteria,
                            VNGGameStatus       *gameStatus,
                            uint32_t            *nGameStatus,  // in & out
                            uint32_t             skipN,
                            VNGTimeout           timeout)
{
    VNG_SEARCHGAMES_CMD *pCmd = (VNG_SEARCHGAMES_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_SEARCHGAMES;
    pCmd->header.inLen = sizeof(VNG_SEARCHGAMES_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vng = vng;
    pCmd->in.searchCriteria = searchCriteria;
    pCmd->in.gameStatus = gameStatus;
    pCmd->in.nGameStatus = (nGameStatus) ? *nGameStatus : 0;
    pCmd->in.skipN = skipN;
    pCmd->in.timeout = timeout;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        if (nGameStatus) *nGameStatus = pCmd->in.nGameStatus;
        return pCmd->out;
    } else {
        return VNGERR_UNKNOWN;
    }
}

VNGErrCode VNG_AddServerToAdhocDomain(VNG        *vng,
                                             const char *serverName,
                                             VNGPort     serverPort)
{
    VNG_ADDSERVERTOADHOCDOMAIN_CMD *pCmd = (VNG_ADDSERVERTOADHOCDOMAIN_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_ADDSERVERTOADHOCDOMAIN;
    pCmd->header.inLen = sizeof(VNG_ADDSERVERTOADHOCDOMAIN_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vng = vng;
    pCmd->in.serverName = serverName;
    pCmd->in.serverPort = serverPort;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        return pCmd->out;
    } else {
        return VNGERR_UNKNOWN;
    }
}

VNGErrCode VNG_ResetAdhocDomain(VNG *vng)
{
    VNG_RESETADHOCDOMAIN_CMD *pCmd = (VNG_RESETADHOCDOMAIN_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_RESETADHOCDOMAIN;
    pCmd->header.inLen = sizeof(VNG_RESETADHOCDOMAIN_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    scrpc(pCmd);

    while (pCmd->header.status==RPC_PENDING);

    if (pCmd->header.status == RPC_OK) {
        return pCmd->out;
    } else {
        return VNGERR_UNKNOWN;
    }
}

uint32_t VNG_ConnectStatus (VNG *vng)
{
    VNG_CONNECTSTATUS_CMD *pCmd = (VNG_CONNECTSTATUS_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_CONNECTSTATUS;
    pCmd->header.inLen = sizeof(VNG_CONNECTSTATUS_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vng = vng;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        return pCmd->out;
    } else {
        return 0;
    }
}

VNGUserId VNG_MyUserId (VNG *vng)
{
    VNG_MYUSERID_CMD *pCmd = (VNG_MYUSERID_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_MYUSERID;
    pCmd->header.inLen = sizeof(VNG_MYUSERID_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vng = vng;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        return pCmd->out;
    } else {
        return 0;
    }
}

VNMember VN_Self (VN *vn)
{
    VNG_SELF_CMD *pCmd = (VNG_SELF_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_SELF;
    pCmd->header.inLen = sizeof(VNG_SELF_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vn = vn;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        return pCmd->out;
    } else {
        return 0;
    }
}

VNMember VN_Owner (VN *vn)
{
    VNG_OWNER_CMD *pCmd = (VNG_OWNER_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_OWNER;
    pCmd->header.inLen = sizeof(VNG_OWNER_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vn = vn;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        return pCmd->out;
    } else {
        return 0;
    }
}

VNState VN_State (VN *vn)
{
    VNG_STATE_CMD *pCmd = (VNG_STATE_CMD*)GBA_SM_BUF_BASE;
    pCmd->header.ifId = VNG_INTERFACE_ID;
    pCmd->header.procId = VNG_PROC_ID_STATE;
    pCmd->header.inLen = sizeof(VNG_STATE_ARG);
    pCmd->header.outLen = sizeof pCmd->out;
    pCmd->header.status = RPC_PENDING;

    pCmd->in.vn = vn;

    scrpc(pCmd);

    if (pCmd->header.status == RPC_OK) {
        return pCmd->out;
    } else {
        return 0;
    }
}




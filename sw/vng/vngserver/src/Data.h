#ifndef __VNG_DATA_H__
#define __VNG_DATA_H__  1

#include <string>
#include <vector>
#include <list>
#include <map>
#include <pthread.h>
#include "vng.h"
#include "vng_types.h"
#include <arpa/inet.h>

VNGUserId 
getVNGUserId(const unsigned long long& membershipId, const unsigned long& memberId);

unsigned long long
getMembershipId(const VNGUserId& id);

unsigned long
getMemberId(const VNGUserId& id);

std::string
toString(const VNId& id);

VNId
toVNId(const std::string& id);

class lessId {
public:
    bool operator()(const VNGUserId& _Left, const VNGUserId& _Right) const {
        unsigned long long *l = (unsigned long long*)&_Left;
        unsigned long long *r = (unsigned long long*)&_Right;
        return *l < *r;
    }
    bool operator()(const VNId& _Left, const VNId& _Right) const {
        return _Left.netId < _Right.netId;
    }
};

class Flags
{
public:
    int     buddy_flag;
    int     reverse_flag;
};

class Player
{
public:
    Player();

    VNGUserId   userId;
    VNGUserOnlineStatus onlineStatus;
    int         gameId;
    VNId        vnId;
    bool        listening;

    std::map<VNGUserId,Flags,lessId> buddylist;
};

class Game
{
public:
    Game();

    VNId        vnId;
    int         numPlayers;
    int         gameStatus;
};



class MsgCache
{
public:
    void clear ()
    {
        msg.clear();
        msg.reserve(0);
        msgId = 0;
    }

    std::string  msg;
    uint32_t     msgId;
};



class StoreMsg
{
public:
    StoreMsg();

    VNGErrCode append (uint8_t *chunk, uint32_t chunkLen);

    const char *mediaTypeCstr (uint32_t  mediaType);

    void clear (uint32_t msgReserve = 0)
    {
        msg.clear();
        msg.reserve(msgReserve);
        subject.clear();
        recipients.clear();

        seqId = 0;
        expectedSeqNum = 0;
        msgLen = 0;
        mediaType = 0;
        replyMsgid = 0;
        recipsLen = 0;
        recipsLenCached = 0;
    }

    uint16_t       seqId;
    uint16_t       expectedSeqNum;
    uint32_t       msgLen;
    uint32_t       mediaType;
    uint32_t       replyMsgid;
    uint16_t       recipsLen;
    uint32_t       recipsLenCached;

    std::string              msg;
    std::string              subject;
    std::vector<std::string> recipients;
};

class Session
{
public:
    Session();

    VN          *vn;
    VNMember    vnmember;

    bool        hasPlayer;
    Player      player;
    StoreMsg    storeMsg;  // only one at a time cached for store
    MsgCache    msgCache;  // only one at a time cached for retrieve
    in_addr     membIPAddr;
    std::string     cookie;
    std::map<VNId,int,lessId>  hosted_games;
};

class VNGStore
{
public:
    VNGStore();

    /* true if session returned */
    bool getSession(VNGUserId userId, Session* session);
    /* true if session returned */
    bool getSession(VN* vnPtr, Session* session);
    /* true if session returned.  
       session parameter is both input & output.
       use default session for first session */
    bool getNextSession(Session* session);

    /* true if game returned */
    bool getGame(VNId id, Game* game);
    /* true if game returned.  
       game parameter is both input & output.
       use default game for first game */
    bool getNextGame(Game* game);

    /* add a new session */
    void addSession(Session s);
    /* update existing session */
    void updateSession(Session s);
    /* remove session associated with VN */
    void removeSession(VN* vnPtr);

    /* add a new game */
    void addGame(Game g);
    /* update existing game */
    void updateGame(Game g);
    /* remove Game associated with VNId */
    void removeGame(VNId id);

private:
    pthread_rwlock_t rwlock;
    std::map<VNGUserId, Session*, lessId> id_session_map;
    std::map<VN*, Session*> vn_session_map;
    std::map<VNId, Game*, lessId> id_game_map;
};

#endif // __VNG_DATA_H__

#include <iostream>
#include "Data.h"
#include "DB.h"
#include "server_rpc.h"
#include "RPC.h"
#include "vng.h"
#include "Logger.h"
using namespace std;

VNGStore store;
DB db;
Logger logger;

//string server_url = "http://osc.bbu.lab1:10080/osc/internal/vng";
string server_url = "http://10.0.0.149:10080/osc/internal/vng";

/* test keep alive */
int test4()
{
    VNMsgHdr hdr;
    int retval;

    _vng_login_arg login_arg;
    _vng_login_ret login_ret;
    login_arg.type = VNG_USER_LOGIN;
    strcpy (login_arg.user, "tester01");
    strcpy (login_arg.passwd, "tester01");

    size_t ret_size = sizeof(login_ret);

    retval = RPCLogin((VN*)0, &hdr, &login_arg, sizeof(login_arg), &login_ret, &ret_size);

    if (retval == VNG_OK) {
        cout << "Logged in as: " << login_ret.nickname << endl;
    } else {
        cout << "Cannot log in!" << endl;
    }

    login_arg.type = VNG_USER_LOGIN;
    strcpy (login_arg.user, "eli");
    strcpy (login_arg.passwd, "tester01");

    ret_size = sizeof(login_ret);

    retval = RPCLogin((VN*)1, &hdr, &login_arg, sizeof(login_arg), &login_ret, &ret_size);

    if (retval == VNG_OK) {
        cout << "Logged in as: " << login_ret.nickname << endl;
    } else {
        cout << "Cannot log in!" << endl;
    }

    keepAlivePlayers();

    return 0;
}

/* test score */
int test1()
{
    int retval = 0;
    size_t ret_size;
    VNMsgHdr hdr;
    hdr.sender = 1;

    _vng_login_arg login_arg;
    login_arg.type = VNG_USER_LOGIN;
    strcpy (login_arg.user, "tester01");
    strcpy (login_arg.passwd, "tester01");
    _vng_login_ret login_ret;

    ret_size = sizeof(login_ret);

    retval = RPCLogin((VN*)0, &hdr, &login_arg, sizeof(login_arg), &login_ret, &ret_size);

    if (retval == VNG_OK) {
        cout << "Logged in as: " << login_ret.nickname << endl;
    } else {
        cout << "Cannot log in!" << endl;
    }

    _vng_submit_score_arg submit_score_arg;
    memset(&submit_score_arg, 0, sizeof(submit_score_arg));
    submit_score_arg.keyAndScore.key.gameId = 1;
    submit_score_arg.keyAndScore.key.scoreId = 1;
    submit_score_arg.keyAndScore.titleId = 1;
    submit_score_arg.keyAndScore.timeOfDay = 1;
    submit_score_arg.keyAndScore.score = 1;
    submit_score_arg.keyAndScore.info[0] = 'a';
    submit_score_arg.keyAndScore.info[1] = 'b';
    submit_score_arg.keyAndScore.info[2] = 'c';
    _vng_submit_score_ret submit_score_ret;

    ret_size = sizeof(submit_score_ret);

    retval = RPCSubmitScore((VN*)0, &hdr, &submit_score_arg, sizeof(submit_score_arg), &submit_score_ret, &ret_size);

    if (retval == VNG_OK) {
        cout << "RPCSubmitScore ok"<< endl;
    } else {
        cout << "Cannot RPCSubmitScore!" << endl;
    }

    _vng_submit_score_obj_arg submit_score_obj_arg;
    memset(&submit_score_obj_arg, 0, sizeof(submit_score_obj_arg));
    submit_score_obj_arg.keyAndScore = submit_score_arg.keyAndScore;
    submit_score_obj_arg.keyAndScore.objSize = 3;
    submit_score_obj_arg.object[0] = 'e';
    submit_score_obj_arg.object[1] = 'l';
    submit_score_obj_arg.object[2] = 'i';

    retval = RPCSubmitScoreObject((VN*)0, &hdr, &submit_score_obj_arg, sizeof(submit_score_obj_arg), NULL, &ret_size);

    if (retval == VNG_OK) {
        cout << "RPCSubmitScoreObject ok"<< endl;
    } else {
        cout << "Cannot RPCSubmitScoreObject!" << endl;
    }

    _vng_get_score_arg get_score_arg;
    memset(&get_score_arg, 0, sizeof(get_score_arg));
    get_score_arg.key.uid = login_ret.userId;
    get_score_arg.key.gameId = 1;
    get_score_arg.key.scoreId = 1;
    _vng_get_score_ret _vng_get_score_ret;

    ret_size = sizeof(_vng_get_score_ret);

    retval = RPCGetScore((VN*)0, &hdr, &get_score_arg, sizeof(get_score_arg), &_vng_get_score_ret, &ret_size);

    if (retval == VNG_OK) {
        cout << "RPCGetScore ok"<< endl;
    } else {
        cout << "Cannot RPCGetScore!" << endl;
    }

    _vng_get_score_obj_arg get_score_obj_arg;
    memset(&get_score_obj_arg, 0, sizeof(get_score_obj_arg));
    get_score_obj_arg.key = get_score_arg.key;
    _vng_get_score_obj_ret get_score_obj_ret;

    ret_size = sizeof(get_score_obj_ret);

    retval = RPCGetScoreObject((VN*)0, &hdr, &get_score_obj_arg, sizeof(get_score_obj_arg), &get_score_obj_ret, &ret_size);

    if (retval == VNG_OK) {
        cout << "RPCGetScoreObject ok"<< endl;
    } else {
        cout << "Cannot RPCGetScoreObject!" << endl;
    }

    _vng_get_ranked_scores_arg get_ranked_scores_arg;
    memset(&get_ranked_scores_arg, 0, sizeof(get_ranked_scores_arg));
    get_ranked_scores_arg.gameId = 1;
    get_ranked_scores_arg.scoreId = 1;
    get_ranked_scores_arg.rankBegin = 0;
    get_ranked_scores_arg.count = 100;
    _vng_get_ranked_scores_ret get_ranked_scores_ret;

    ret_size = sizeof(get_ranked_scores_ret);

    retval = RPCGetRankedScore((VN*)0, &hdr, &get_ranked_scores_arg, sizeof(get_ranked_scores_arg), &get_ranked_scores_ret, &ret_size);

    if (retval == VNG_OK) {
        cout << "RPCGetRankedScore ok"<< endl;
    } else {
        cout << "Cannot RPCGetRankedScore!" << endl;
    }

    return 0;
}

/* test game registration */
int test2()
{
    VNMsgHdr hdr;
    int retval;
    size_t ret_size;

    _vng_login_arg login_arg;
    _vng_login_ret login_ret;
    login_arg.type = VNG_USER_LOGIN;
    strcpy (login_arg.user, "eli");
    strcpy (login_arg.passwd, "tester01");

    ret_size = sizeof(login_ret);

    retval = RPCLogin((VN*)0, &hdr, &login_arg, sizeof(login_arg), &login_ret, &ret_size);

    if (retval == VNG_OK) {
        cout << "Logged in as: " << login_ret.nickname << endl;
    } else {
        cout << "Cannot log in!" << endl;
    }

    _vng_register_game_arg register_game_arg;
    memset(&register_game_arg, 0, sizeof(register_game_arg));

    register_game_arg.info.vnId.devId = 1;
    register_game_arg.info.vnId.vnetId = 3;
    register_game_arg.info.titleId = 1;
    register_game_arg.info.gameId = 1;
    register_game_arg.info.buddySlots = 1;
    register_game_arg.info.attrCount = 5;

    retval = RPCRegisterGame((VN*)0, &hdr, &register_game_arg, sizeof(register_game_arg), NULL, &ret_size);

    if (retval == VNG_OK) {
        cout << "Registered game"<< endl;
    } else {
        cout << "Cannot register game!" << endl;
    }

    _vng_update_game_status_arg update_game_status_arg;
    update_game_status_arg.vnId.devId = 1;
    update_game_status_arg.vnId.vnetId = 3;
    update_game_status_arg.gameStatus = 11;
    update_game_status_arg.numPlayers = 111;

    retval = RPCUpdateGameStatus((VN*)0, &hdr, &update_game_status_arg, sizeof(update_game_status_arg), NULL, &ret_size);

    if (retval == VNG_OK) {
        cout << "updated game"<< endl;
    } else {
        cout << "Cannot update game!" << endl;
    }

    _vng_get_game_status_arg get_game_status_arg;
    get_game_status_arg.count = 1;
    get_game_status_arg.vnId[0].devId = 1;
    get_game_status_arg.vnId[0].vnetId = 3;
    _vng_get_game_status_ret get_game_status_ret;
    ret_size = sizeof(get_game_status_ret);

    retval = RPCGetGameStatus((VN*)0, &hdr, &get_game_status_arg, sizeof(get_game_status_arg), &get_game_status_ret, &ret_size);

    if (retval == VNG_OK) {
        cout << "RPCGetGameStatus OK"<< endl;
    } else {
        cout << "Cannot RPCGetGameStatus!" << endl;
    }

    _vng_match_game_arg match_game_arg;
    memset(&match_game_arg, 0, sizeof(match_game_arg));
    match_game_arg.count = MAX_GAME_MATCH;
    match_game_arg.skipN = 0;
    match_game_arg.searchCriteria.gameId = 1;
    _vng_match_game_ret match_game_ret;
    ret_size = sizeof(match_game_ret);

    retval = RPCMatchGame((VN*)0, &hdr, &match_game_arg, sizeof(match_game_arg), &match_game_ret, &ret_size);

    if (retval == VNG_OK) {
        cout << "RPCMatchGame OK"<< endl;
    } else {
        cout << "Cannot RPCMatchGame!" << endl;
    }

    _vng_unregister_game_arg unregister_game_arg;
    unregister_game_arg.vnId.devId = 1;
    unregister_game_arg.vnId.vnetId = 3;

    retval = RPCUnregisterGame((VN*)0, &hdr, &unregister_game_arg, sizeof(unregister_game_arg), NULL, &ret_size);

    if (retval == VNG_OK) {
        cout << "RPCUnregisterGame OK"<< endl;
    } else {
        cout << "Cannot RPCUnregisterGame!" << endl;
    }

    Game g;
    while (store.getNextGame(&g)) {
        cout << "Game: " << g.vnId.vnetId << " : " << g.numPlayers << " : " << g.gameStatus << endl;
    }

    return 0;
}

int test3()
{
    VNMsgHdr hdr;
    int retval;

    _vng_login_arg login_arg;
    _vng_login_ret login_ret;
    login_arg.type = VNG_USER_LOGIN;
    strcpy (login_arg.user, "tester01");
    strcpy (login_arg.passwd, "tester01");

    size_t ret_size = sizeof(login_ret);

    retval = RPCLogin((VN*)0, &hdr, &login_arg, sizeof(login_arg), &login_ret, &ret_size);

    if (retval == VNG_OK) {
        cout << "Logged in as: " << login_ret.nickname << endl;
    } else {
        cout << "Cannot log in!" << endl;
    }

    login_arg.type = VNG_USER_LOGIN;
    strcpy (login_arg.user, "eli");
    strcpy (login_arg.passwd, "tester01");

    ret_size = sizeof(login_ret);

    retval = RPCLogin((VN*)1, &hdr, &login_arg, sizeof(login_arg), &login_ret, &ret_size);

    if (retval == VNG_OK) {
        cout << "Logged in as: " << login_ret.nickname << endl;
    } else {
        cout << "Cannot log in!" << endl;
    }

    RPCEnableBuddyTracking((VN*)0, &hdr, NULL, 0, NULL, &ret_size);
    RPCEnableBuddyTracking((VN*)1, &hdr, NULL, 0, NULL, &ret_size);

    _vng_update_status_arg update_status_arg;
    update_status_arg.gameId = 0;
    update_status_arg.status = VNG_STATUS_AWAY;
    RPCUpdateStatus((VN*)0, &hdr, &update_status_arg, sizeof update_status_arg, NULL, &ret_size);

    _vng_get_user_info_arg get_user_info_arg;
    get_user_info_arg.userId = login_ret.userId;
    _vng_get_user_info_ret get_user_info_ret;
    ret_size = sizeof(get_user_info_ret);

    retval = RPCGetUserInfo((VN*)0, &hdr, &get_user_info_arg, sizeof(get_user_info_arg), &get_user_info_ret, &ret_size);

    if (retval == VNG_OK) {
        cout << "RPCGetUserInfo OK"<< endl;
    } else {
        cout << "Cannot RPCGetBuddylist!" << endl;
    }

    _vng_invite_buddy_arg invite_buddy_arg;
    strcpy(invite_buddy_arg.login, "tester1");
    if (RPCInviteBuddy((VN*)1, &hdr, &invite_buddy_arg, sizeof invite_buddy_arg, NULL, &ret_size))
        cout << "RPCInviteBuddy rejected!" << endl;

    //_vng_accept_buddy_arg accept_buddy_arg;
    //accept_buddy_arg.userId = getVNGUserId(18863514322,1);
    //if (RPCAcceptBuddy((VN*)1, &hdr, &accept_buddy_arg, sizeof accept_buddy_arg, NULL, &ret_size))
    //    cout << "RPCAcceptBuddy rejected!" << endl;

    //_vng_reject_buddy_arg reject_buddy_arg;
    //reject_buddy_arg.userId = getVNGUserId(18863514322,5);
    //if (RPCRejectBuddy((VN*)1, &hdr, &reject_buddy_arg, sizeof reject_buddy_arg, NULL, &ret_size))
    //     cout << "RPCRejectBuddy rejected!" << endl;

    //_vng_remove_buddy_arg remove_buddy_arg;
    //remove_buddy_arg.userId = getVNGUserId(18863514322,5);
    //if (RPCRemoveBuddy((VN*)1, &hdr, &remove_buddy_arg, sizeof remove_buddy_arg, NULL, &ret_size))
    //    cout << "RPCRemoveBuddy rejected!" << endl;

    //_vng_block_buddy_arg block_buddy_arg;
    //block_buddy_arg.userId = getVNGUserId(18863514322,5);
    //if (RPCBlockBuddy((VN*)0, &hdr, &block_buddy_arg, sizeof block_buddy_arg, NULL, &ret_size))
    //    cout << "RPCBlockBuddy rejected!" << endl;

    //_vng_unblock_buddy_arg unblock_buddy_arg;
    //unblock_buddy_arg.userId = getVNGUserId(18863514322,5);
    //if (RPCUnblockBuddy((VN*)0, &hdr, &unblock_buddy_arg, sizeof unblock_buddy_arg, NULL, &ret_size))
    //    cout << "RPCUnblockBuddy rejected!" << endl;

    _vng_get_buddylist_arg get_buddylist_arg;
    get_buddylist_arg.nBuddies = MAX_BUDDIES;
    get_buddylist_arg.skipN = 0;
    _vng_get_buddylist_ret get_buddylist_ret;
    ret_size = sizeof(get_buddylist_ret);

    retval = RPCGetBuddylist((VN*)0, &hdr, &get_buddylist_arg, sizeof(get_buddylist_arg), &get_buddylist_ret, &ret_size);

    if (retval == VNG_OK) {
        cout << "RPCGetBuddylist OK" << endl;
    } else {
        cout << "Cannot RPCGetBuddylist!" << endl;
    }

    _vng_get_buddy_status_arg get_buddy_status_arg;
    get_buddy_status_arg.count = (uint32_t) ret_size / sizeof(VNGUserInfo);
    for (unsigned int i = 0; i < get_buddy_status_arg.count; i++) {
        get_buddy_status_arg.buddies[i] = get_buddylist_ret.buddies[i].uid;
    }
    _vng_get_buddy_status_ret get_buddy_status_ret;
    ret_size = sizeof(get_buddy_status_ret);

    retval = RPCGetBuddyStatus((VN*)0, &hdr, &get_buddy_status_arg, sizeof(get_buddy_status_arg), &get_buddy_status_ret, &ret_size);

    if (retval == VNG_OK) {
        cout << "RPCGetBuddyStatus OK" << endl;
        cout << "# buddies: " << get_buddy_status_arg.count << endl;
        for (unsigned int i=0; i<get_buddy_status_arg.count; i++) {
            cout << i << " : " << get_buddylist_ret.buddies[i].login << 
                " : " << getMemberId(get_buddylist_ret.buddies[i].uid) <<
                " : " << get_buddy_status_ret.statuses[i].onlineStatus << endl;
        }
    } else {
        cout << "Cannot RPCGetBuddyStatus!" << endl;
    }

    ret_size = sizeof(get_buddylist_ret);

    retval = RPCGetBuddylist((VN*)1, &hdr, &get_buddylist_arg, sizeof(get_buddylist_arg), &get_buddylist_ret, &ret_size);

    if (retval == VNG_OK) {
        cout << "RPCGetBuddylist OK" << endl;
    } else {
        cout << "Cannot RPCGetBuddylist!" << endl;
    }

    get_buddy_status_arg.count = (uint32_t) ret_size / sizeof(VNGUserInfo);
    for (unsigned int i = 0; i < get_buddy_status_arg.count; i++) {
        get_buddy_status_arg.buddies[i] = get_buddylist_ret.buddies[i].uid;
    }
    ret_size = sizeof(get_buddy_status_ret);

    retval = RPCGetBuddyStatus((VN*)1, &hdr, &get_buddy_status_arg, sizeof(get_buddy_status_arg), &get_buddy_status_ret, &ret_size);

    if (retval == VNG_OK) {
        cout << "RPCGetBuddyStatus OK" << endl;
        cout << "# buddies: " << get_buddy_status_arg.count << endl;
        for (unsigned int i=0; i<get_buddy_status_arg.count; i++) {
            cout << i << " : " << get_buddylist_ret.buddies[i].login << 
                " : " << getMemberId(get_buddylist_ret.buddies[i].uid) <<
                " : " << get_buddy_status_ret.statuses[i].onlineStatus << endl;
        }
    } else {
        cout << "Cannot RPCGetBuddyStatus!" << endl;
    }

    RPCLogout((VN*)0, &hdr, NULL, 0, NULL, &ret_size);
    RPCLogout((VN*)1, &hdr, NULL, 0, NULL, &ret_size);
    return 0;
}

int test5()
{
    Session s;

    s.vn = (VN*)1;
    s.hasPlayer = true;
    s.player.userId = getVNGUserId(1,1);

    store.updateSession(s);

    s.vn = (VN*)2;
    s.hasPlayer = true;
    s.player.userId = getVNGUserId(2,1);

    store.updateSession(s);

    s.vn = (VN*)2;
    s.hasPlayer = false;

    store.updateSession(s);

    s.vn = (VN*)3;
    s.hasPlayer = true;
    s.player.userId = getVNGUserId(1,1);

    store.updateSession(s);

    store.removeSession((VN*)1);

    s.vn = (VN*)1;
    s.hasPlayer = true;
    s.player.userId = getVNGUserId(1,1);

    store.updateSession(s);

    if (store.getSession(getVNGUserId(1,1), &s)) {
        cout << "Session: " << s.vn;
        if (s.hasPlayer) {
            cout << " Player: " << getMembershipId(s.player.userId);
        }
        cout << endl;
    }

    if (store.getSession((VN*)2, &s)) {
        cout << "Session: " << s.vn;
        if (s.hasPlayer) {
            cout << " Player: " << getMembershipId(s.player.userId);
        }
        cout << endl;
    }

    store.removeSession((VN*)3);

    s = Session();
    while (store.getNextSession(&s)) {
        cout << "Session: " << s.vn;
        if (s.hasPlayer) {
            cout << " Player: " << getMembershipId(s.player.userId);
        }
        cout << endl;
    }

    Game g;

    g.vnId.devId = 1;
    g.vnId.vnetId = 1;
    g.numPlayers = 11;
    g.gameStatus = 111;
    store.updateGame(g);

    g.vnId.devId = 2;
    g.vnId.vnetId = 2;
    g.numPlayers = 22;
    g.gameStatus = 222;
    store.updateGame(g);

    g.vnId.devId = 3;
    g.vnId.vnetId = 3;
    g.numPlayers = 33;
    g.gameStatus = 333;
    store.updateGame(g);

    Game g_tmp;
    if (store.getGame(g.vnId, &g_tmp)) {
        cout << "Game: " << g_tmp.vnId.vnetId << " : " << g_tmp.numPlayers << " : " << g_tmp.gameStatus << endl;
    }

    store.removeGame(g.vnId);

    g_tmp = Game();
    while (store.getNextGame(&g_tmp)) {
        cout << "Game: " << g_tmp.vnId.vnetId << " : " << g_tmp.numPlayers << " : " << g_tmp.gameStatus << endl;
    }

    return 0;
}

int main(int argc, char* argv[])
{
    logger.setLevel(DEBUG);
    db.setUrl(server_url);

    test1();
    test2();
    test3();
    test4();
    test5();

    return 0;
}



#ifndef __VNG_DB_H__
#define __VNG_DB_H__  1

#include <string>
#include <sstream>
#include <curl/curl.h>
#include "vng.h"
using namespace std;

class DBRequest
{
public:
    void begin();
    template<typename T> void addParam(const string& tag, const T& value) {
            oss << "<" << tag << ">" << value << "</" << tag << ">";
    }
    void addString(const string& tag, const string& value);
    void addOperator(uint32_t op);
    void end();

    friend class DB;

    string cookie;

private:
    string toXmlString(string buf);
    static string begin_tag;
    static string end_tag;
    ostringstream oss;
};

class NotFoundException
{
};

class DBResponse
{
public:
    string getNode(string tag, size_t& index);
    string getCookie();

    friend class DB;

private:
    DBResponse(string header, string body);
    string _header;
    string _body;
};

class DB
{
public:
    DB();
    ~DB();
    void setUrl(string url);
    DBResponse submit(DBRequest& req);

private:
    string db_url;
};

#endif // __VNG_DB_H__

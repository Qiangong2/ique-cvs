#include <pthread.h>
#include "Data.h"
#include "server_rpc.h"
#include "Logger.h"

extern Logger logger;


/* VNGUserId consists of:
 * membershipId: 40 most-significant bits
 * memberId: 24 least-significant bits
 */
VNGUserId 
getVNGUserId(const unsigned long long& membershipId, const unsigned long& memberId)
{
    return (membershipId << 24) | (memberId & 0xffffff);
}

unsigned long long
getMembershipId(const VNGUserId& id)
{
    return id >> 24;
}

unsigned long
getMemberId(const VNGUserId& id)
{
    return (unsigned long)(id & 0xffffff);
}

std::string
toString(const VNId& id)
{
    char retval[31]; // 30 digits
    
    sprintf(retval, "%010u%020llu", id.netId, id.deviceId);

    return retval;
}

VNId
toVNId(const std::string& id)
{
    VNId retval;
    int indexDevId = id.size() - 20;

    if (indexDevId<0) indexDevId=0;

    retval.netId =
        strtoul(id.substr(0, indexDevId).c_str(), NULL, 10);
    retval.deviceId =
        strtoull(id.substr(indexDevId, 20).c_str(), NULL, 10);

    return retval;
}

Player::Player()
: userId(0), gameId(0), listening(false)
{
}

Game::Game()
: numPlayers(0), gameStatus(0)
{
    vnId.deviceId = vnId.netId = 0;
}

StoreMsg::StoreMsg()
{
    clear();
}

const char*  StoreMsg::mediaTypeCstr (uint32_t  mediaType)
{
    static char *mediaTypeAsCstr [] = {
        "",        // VNG_MEDIA_TYPE_UNKNOWN
        "text",    // VNG_MEDIA_TYPE_TEXT
        "voice"    // VNG_MEDIA_TYPE_VOICE
    };

    const char *txt;

    if (mediaType < sizeof(mediaTypeAsCstr)/sizeof(mediaTypeAsCstr[0]))
        txt = mediaTypeAsCstr [mediaType];
    else
        txt = "";

    return txt;
}

VNGErrCode StoreMsg::append (uint8_t *chunk, uint32_t chunkLen)
{
    // Recipients are { uint16_t len, char login[] } blobs.
    // Everything after recipients is the message.
    uint8_t    *ptr = chunk;
    uint8_t    *top = chunk + chunkLen;
    uint32_t    len;
    std::string s;
    VNGErrCode  ec = VNG_OK;
    size_t      n;

    while (recipsLenCached < recipsLen && ptr < top) {
        if ((size_t)(top - ptr) < MIN_STORE_MSG_RECIPIENTS_LEN) {
            logger.printf(LOG_ERR, "Recipient blob too short: %d", top - ptr);
            ec = VNGERR_INFRA_NS;
            goto end;
        }
        n = strnlen((char*) ptr, VNG_LOGIN_BUF_SIZE);
        if (n > VNG_MAX_LOGIN_LEN) {
            logger.printf(LOG_INFO, "recipient login to long %u", n);
            ec = VNGERR_INFRA_NS;
            goto end;
        }
        s = (char*) ptr;
        recipients.push_back (s);
        recipsLenCached += (n + 1);
        ptr += (n + 1);
    }

    if (ptr < top) {
        len  = top - ptr;
        if (msg.length() > msgLen) {
            logger.printf(LOG_ERR,
                "Received %d bytes for stored message with msgLen %d",
                 msg.length(), msgLen);
            ec = VNGERR_INFRA_NS;
            goto end;
        }
        msg.append((char*)ptr, len);
    }

end:
    return ec;
}

Session::Session()
: hasPlayer(false)
{
}

class ReadLock
{
public:
    ReadLock(pthread_rwlock_t* rwlock): _rwlock(rwlock) { pthread_rwlock_rdlock(_rwlock); }
    ~ReadLock() { pthread_rwlock_unlock(_rwlock); }
    pthread_rwlock_t* _rwlock;
};

class WriteLock
{
public:
    WriteLock(pthread_rwlock_t* rwlock): _rwlock(rwlock) { pthread_rwlock_wrlock(_rwlock); }
    ~WriteLock() { pthread_rwlock_unlock(_rwlock); }
    pthread_rwlock_t* _rwlock;
};

VNGStore::VNGStore()
{
    pthread_rwlock_init(&rwlock, NULL);
}

/* true if session returned */
bool  
VNGStore::getSession(VNGUserId userId, Session* session)
{
    ReadLock lock(&rwlock);

    std::map<VNGUserId, Session*, lessId>::iterator it = id_session_map.find(userId);
    if (it != id_session_map.end()) {
        *session = *(it->second);
        return true;
    } else {
        return false;
    }
}

/* true if session returned */
bool  
VNGStore::getSession(VN* vnPtr, Session* session)
{
    ReadLock lock(&rwlock);

    std::map<VN*, Session*>::iterator it = vn_session_map.find(vnPtr);
    if (it != vn_session_map.end()) {
        *session = *(it->second);
        return true;
    } else {
        return false;
    }
}

/* true if session returned.  
   session parameter is both input & output.
   use default session for first session */
bool 
VNGStore::getNextSession(Session* session)
{
    ReadLock lock(&rwlock);

    std::map<VN*, Session*>::iterator it = vn_session_map.upper_bound(session->vn);
    if (it != vn_session_map.end()) {
        *session = *(it->second);
        return true;
    } else {
        return false;
    }
}

/* true if game returned */
bool 
VNGStore::getGame(VNId id, Game* game)
{
    ReadLock lock(&rwlock);

    std::map<VNId, Game*, lessId>::iterator it = id_game_map.find(id);
    if (it != id_game_map.end()) {
        *game =  *(it->second);
        return true;
    } else {
        return false;
    }
}

/* true if game returned.  
   game parameter is both input & output.
   use default game for first game */
bool
VNGStore::getNextGame(Game* game)
{
    ReadLock lock(&rwlock);

    std::map<VNId, Game*, lessId>::iterator it = id_game_map.upper_bound(game->vnId);
    if (it != id_game_map.end()) {
        *game =  *(it->second);
        return true;
    } else {
        return false;
    }
}

/* add a new session */
void
VNGStore::addSession(Session s)
{
    WriteLock lock(&rwlock);

    std::map<VN*, Session*>::iterator it = vn_session_map.find(s.vn);
    if (it == vn_session_map.end()) {
        Session* sess = new Session(s);

        vn_session_map[s.vn] = sess;
        if (s.hasPlayer) {
            id_session_map[s.player.userId] = sess;
        }
    }
}

/* update existing session */
void
VNGStore::updateSession(Session s)
{
    WriteLock lock(&rwlock);

    std::map<VN*, Session*>::iterator it = vn_session_map.find(s.vn);
    if (it != vn_session_map.end()) {
        *(it->second) = s;
    }
}

/* remove session associated with VN */
void
VNGStore::removeSession(VN* vnPtr)
{
    WriteLock lock(&rwlock);

    std::map<VN*, Session*>::iterator it = vn_session_map.find(vnPtr);
    if (it != vn_session_map.end()) {
        Session* sPtr = it->second;
        if (sPtr->hasPlayer) {
            std::map<VNGUserId, Session*, lessId>::iterator it2 = id_session_map.find(sPtr->player.userId);
            if (it2 != id_session_map.end()) {
                if (sPtr == it2->second)
                    id_session_map.erase(sPtr->player.userId);
            }
        }
        vn_session_map.erase(vnPtr);
        free(sPtr);
    }
}

/* add a new game */
void
VNGStore::addGame(Game g)
{
    WriteLock lock(&rwlock);

    std::map<VNId, Game*, lessId>::iterator it = id_game_map.find(g.vnId);
    if (it == id_game_map.end()) {
        Game* game = new Game(g);

        id_game_map[g.vnId] = game;
    }
}

/* update existing game */
void
VNGStore::updateGame(Game g)
{
    WriteLock lock(&rwlock);

    std::map<VNId, Game*, lessId>::iterator it = id_game_map.find(g.vnId);
    if (it != id_game_map.end()) {
        *(it->second) = g;
    }
}

/* remove Game associated with VNId */
void
VNGStore::removeGame(VNId id)
{
    WriteLock lock(&rwlock);

    std::map<VNId, Game*, lessId>::iterator it = id_game_map.find(id);
    if (it != id_game_map.end()) {
        Game* gPtr = it->second;
        id_game_map.erase(it);
        free(gPtr);
    }
}

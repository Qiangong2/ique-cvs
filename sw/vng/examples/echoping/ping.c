//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "porting.h"
#include "vng.h"
#include "../include/testcfg.h"

#define RTT_TEST_COUNT 1024
#define RCV_TIMEOUT  10000  // in ms

#define MSG_SIZE 160

// Allocate VN data structure from heap
VNG      *vng;
VN       *vn;
VNMsgHdr *hdr;
int      *smsg;
int      *rmsg;
VNGSearchCriteria *criteria;
VNGGameStatus     *gameStatus;

void
checkResult (int value, int expected, const char *name)
{
    if (value == expected) {
	printf ("  %s ok ...\n", name);
	return;			// OK
    }
    printf ("%s returns %d, expected %d\n", name, value, expected);
    printf ("vng sanity test end\n");
    printf ("VNG TEST FAILED\n");
    exit (0);
}


void
joinEchoServer ()
{
    VNGErrCode err;
    int numGameStatus;
    int i;
    int rv;
    char *joinStr = "ping";

    rv = VNG_Login (vng, VNGS_HOST, VNGS_PORT, TESTER01, TESTER01PW,
		    VNG_WAIT);
    checkResult (rv, VNG_OK, "VNG_Login");

    memset((void *) criteria, 0, sizeof (VNGSearchCriteria));

    criteria->gameId = 7;
    criteria->domain = VNG_SEARCH_INFRA;
    criteria->maxLatency = 500;	// 300ms RTT
    criteria->cmpKeyword = VNG_CMP_DONTCARE;

    while (1) {
	numGameStatus = 64;
	err =
	    VNG_SearchGames (vng, criteria, gameStatus, &numGameStatus, 0,
			     VNG_WAIT);
	checkResult (err, VNG_OK, "VNG_SearchGames");
	printf ("VNG_SearchGames returns %d - numGames = %d\n", err,
		numGameStatus);
	if (numGameStatus > 0) {
	    for (i = numGameStatus-1; i >= 0; i--) {
		printf ("try to join %d ...\n", i);
		err = VNG_JoinVN (vng,
				  gameStatus[i].gameInfo.vnId,
				  joinStr, vn, NULL, 0, 60 * 1000);
		if (err == VNG_OK) {
		    printf ("joined game\n");
		    return;
		}
	    }
	}
	exit (0);
    }
}


void
doPing ()
{
    int rv;
    int i, j, m, n;
    size_t msglen;
    int attr = VN_DEFAULT_ATTR;

    m = 4;
    n = 4;
    for (i = 0; i < m; i++) {
	for (j = 0; j < n; j++) {
	    smsg[0] = i;
	    smsg[1] = j;
	    rv = VN_SendMsg (vn, VN_MEMBER_OWNER, 2034, smsg, MSG_SIZE, attr,
			     VNG_WAIT);
	    checkResult (rv, VNG_OK, "VN_SendMsg");
	}
	for (j = 0; j < n; j++) {
	    msglen = MSG_SIZE;
	    rv = VN_RecvMsg (vn, VN_MEMBER_ANY, VN_SERVICE_TAG_ANY, rmsg,
			     &msglen, hdr, RCV_TIMEOUT);
	    checkResult (rv, VNG_OK, "VN_RecvMsg");
	}
    }
}


void
estimateRTT (int testcount)
{
    int rv;
    int i;
    size_t msglen;
    int attr = VN_DEFAULT_ATTR;
    unsigned int ts;

    ts = getTimestamp();
    for (i = 0; i < testcount; i++) {
	 smsg[0] = i;
	 rv = VN_SendMsg (vn, VN_MEMBER_OWNER, 2034, smsg, MSG_SIZE, attr,
			     VNG_WAIT);
	 if (rv != VNG_OK) break;
	 msglen = MSG_SIZE;
	 rv = VN_RecvMsg (vn, VN_MEMBER_ANY, VN_SERVICE_TAG_ANY, rmsg,
			     &msglen, hdr, RCV_TIMEOUT);
	 if (rv != VNG_OK) break;
    }
    checkResult(i, testcount, "estimate RTT");
    ts = getTimestamp() - ts;
    printf("estimate RTT: %d roundtrips, total time %u ms, RTT %u ms\n",
	   testcount, ts, (ts+testcount-1)/testcount);
}


void
doTests ()
{
    int rv;
    int count = 1;
    VNGEvent event;

    vng = (VNG*)SM_Malloc(sizeof(VNG));
    vn  = (VN*)SM_Malloc(sizeof(VN));
    hdr = (VNMsgHdr*)SM_Malloc(sizeof(VNMsgHdr));
    smsg = (int *) SM_Malloc(MSG_SIZE);
    rmsg = (int *) SM_Malloc(MSG_SIZE);
    criteria = (VNGSearchCriteria *)SM_Malloc(sizeof(VNGSearchCriteria));
    gameStatus = (VNGGameStatus *)SM_Malloc(sizeof(VNGGameStatus)*100);
    
    checkResult( vng != NULL && vn != NULL && hdr != NULL &&
		 criteria != NULL && gameStatus != NULL ? 1 : 0, 1, 
		 "Allocate Data Structure");

    printf ("vng ping test begin\n");
    rv = VNG_Init (vng, NULL);
    checkResult (rv, VNG_OK, "VNG_Init");

    joinEchoServer ();
    doPing();

    while (count <= RTT_TEST_COUNT) {
      estimateRTT(count);
      count *= 2;
    }

    /* dump events */
    while ((rv = VNG_GetEvent (vng, &event, VNG_NOWAIT)) == VNG_OK) {
	checkResult (rv, VNG_OK, "VNG_GetEvent - OK");
    }
    checkResult (rv, VNGERR_NOWAIT, "VNG_GetEvent - NOWAIT");

    rv = VNG_Fini (vng);
    checkResult (rv, VNG_OK, "VNG_Fini");

    printf ("vng ping test end\n");
    printf ("VNG TEST PASSED\n");
}

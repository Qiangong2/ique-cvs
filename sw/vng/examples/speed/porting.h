#if defined(_SC)
#include "sc/ios.h"
#include "ioslibc.h"
#else
#include <stdio.h>
#include <stdlib.h>
#endif

extern void *SM_Malloc(size_t size);
extern unsigned int getTimestamp();

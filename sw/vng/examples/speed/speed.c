//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "porting.h"
#include "vng.h"
#include "../include/testcfg.h"

#define RTT_TEST_COUNT 1024
#define RCV_TIMEOUT    1000  // in ms
#define JOIN_TIMEOUT   (30*1000) // in ms

#define MSG_SIZE 160
#define BUFFER_LIMIT 60 /* 60 packets */

// Allocate VN data structure from heap
VNG      *vng;
VN       *vn;
VNMsgHdr *hdr;
int      *smsg;
int      *rmsg;
VNGSearchCriteria *criteria;
VNGGameStatus     *gameStatus;

void
checkResult (int value, int expected, const char *name)
{
    if (value == expected) {
        printf ("  %s ok ...\n", name);
        return;			// OK
    }
    printf ("%s returns %d, expected %d\n", name, value, expected);
    printf ("vng speed test end\n");
    printf ("VNG TEST FAILED\n");
    exit (0);
}


void
joinEchoServer ()
{
    VNGErrCode err;
    int numGameStatus;
    int i;
    int rv;
    char *joinStr = "ping";

    rv = VNG_Login (vng, VNGS_HOST, VNGS_PORT, TESTER01, TESTER01PW,
		    VNG_WAIT);
    checkResult (rv, VNG_OK, "VNG_Login");

    memset((void *) criteria, 0, sizeof (VNGSearchCriteria));

    criteria->gameId = 7;
    criteria->domain = VNG_SEARCH_INFRA;
    criteria->maxLatency = 500;	// 300ms RTT
    criteria->cmpKeyword = VNG_CMP_DONTCARE;

    while (1) {
	numGameStatus = 64;
	err =
	    VNG_SearchGames (vng, criteria, gameStatus, &numGameStatus, 0,
			     VNG_WAIT);
	checkResult (err, VNG_OK, "VNG_SearchGames");
	printf ("VNG_SearchGames returns %d - numGames = %d\n", err,
		numGameStatus);
	if (numGameStatus > 0) {
	    for (i = numGameStatus-1; i >= 0; i--) {
		printf ("try to join %d ...\n", i);
		err = VNG_JoinVN (vng,
				  gameStatus[i].gameInfo.vnId,
				  joinStr, vn, NULL, 0, JOIN_TIMEOUT);
		if (err == VNG_OK) {
		    printf ("joined game\n");
		    return;
		}
	    }
	}
	exit (0);
    }
}

int
measureSpeed (int attr)
{
    int rv;
    size_t msglen;
    int scount;
    int rcount;
    int ts1, ts2, ts3;
    int limit;

    scount = 0;
    rcount = 0;

    ts1 = getTimestamp();

    // send until buffer is full!
    limit = BUFFER_LIMIT;
    while ((rv = VN_SendMsg (vn, VN_MEMBER_OWNER, 2034, smsg, MSG_SIZE, attr,
			     VNG_NOWAIT)) == VNG_OK) {
      scount++;
      if (--limit <= 0) break;
    }

    if ((rv != VNG_OK) && (rv != VNGERR_BUF_FULL)) {
        return rv;
    }

    ts2 = getTimestamp();

    // recv all packets
    msglen = MSG_SIZE;
    while ((rv = VN_RecvMsg (vn, VN_MEMBER_ANY, VN_SERVICE_TAG_ANY, rmsg,
			     &msglen, hdr, RCV_TIMEOUT)) == VNG_OK) {
      msglen = MSG_SIZE;
      rcount++;
      if (rcount >= scount) {
          break;
      }
    }

    if ((rv != VNG_OK) && (rv != VNGERR_TIMEOUT)) {
        return rv;
    }

    ts3 = getTimestamp();

    printf("speed: sent %d packets in %d ms\n", scount, ts2 - ts1);
    printf("speed: recv %d packets in %d ms\n", rcount, ts3 - ts2);
    return VNG_OK;
}


void doTests ()
{
    int rv;
    VNGEvent event;
    int i;

    vng = (VNG*)SM_Malloc(sizeof(VNG));
    vn  = (VN*)SM_Malloc(sizeof(VN));
    hdr = (VNMsgHdr*)SM_Malloc(sizeof(VNMsgHdr));
    smsg = (int *) SM_Malloc(MSG_SIZE);
    rmsg = (int *) SM_Malloc(MSG_SIZE);
    criteria = (VNGSearchCriteria *)SM_Malloc(sizeof(VNGSearchCriteria));
    gameStatus = (VNGGameStatus *)SM_Malloc(sizeof(VNGGameStatus)*100);
    
    checkResult( vng != NULL && vn != NULL && hdr != NULL &&
		 criteria != NULL && gameStatus != NULL ? 1 : 0, 1, 
		 "Allocate Data Structure");

    printf ("vng speed test begin\n");
    rv = VNG_Init (vng, NULL);
    checkResult (rv, VNG_OK, "VNG_Init");

    joinEchoServer ();
    
#if 0
    // default is reliable, no timestamp, with encryption
    for (i = 0; i < 5; i++) 
      measureSpeed(VN_DEFAULT_ATTR);
#endif

#ifdef GBA
#define COUNT 100000
#else
#define COUNT 10
#endif
    // reliable, no timestamp, no encryption
    for (i = 0; i < COUNT; i++) {
      printf("Pass %d\n", i);
      rv = measureSpeed(VN_ATTR_NOENCRYPT);
      checkResult (rv, VNG_OK, "measureSpeed");
    }

    /* dump events */
    while ((rv = VNG_GetEvent (vng, &event, VNG_NOWAIT)) == VNG_OK) {
        checkResult (rv, VNG_OK, "VNG_GetEvent - OK");
    }
    checkResult (rv, VNGERR_NOWAIT, "VNG_GetEvent - NOWAIT");

    rv = VNG_Fini (vng);
    checkResult (rv, VNG_OK, "VNG_Fini");

    printf ("vng speed test end\n");
    printf ("VNG TEST PASSED\n");
}

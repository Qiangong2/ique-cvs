//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "../include/lobby_api.h"
#include "../../include/testcfg.h"



// registerServer creates a VN, registers an array of remote procedures, and
// calls VNG_RegisterGame() to register the specified gameId using serverName
// as the VNGGameInfo keyword.
//
// The procId for the VNRemoteProcedures passed in the procs[] argument to
// registerServer() will be automatically assigned starting from 0 and
// incremented by one for each VMRemoteProcedure in procs[]
//
VNGErrCode registerServer(VNG               *vng, 
                          uint32_t           gameId, 
                          const char        *serverName, 
                          VNRemoteProcedure  procs[],
                          uint32_t           nProcs,
                          VN                *vn)    // VN created
{
    VNPolicies   policies = (VN_ABORT_ON_OWNER_EXIT | VN_AUTO_ACCEPT_JOIN);
    VNGErrCode   ec       = VNG_OK;
    VNId         vnId;
    VNGGameInfo  info;
    char         comments[]  = "Viewer lobby";
    uint32_t     i;

    if (strnlen(serverName, VNG_GAME_KEYWORD_LEN) == VNG_GAME_KEYWORD_LEN) {
        return VNGERR_INVALID_LEN;
    }

    if ((ec = VNG_NewVN(vng, vn, VN_CLASS_3, VN_DOMAIN_INFRA, policies))) {
        return ec;
    }

    for (i = 0;  i < nProcs;  ++i) {
        if (( ec = VN_RegisterRPCService (vn, i, procs[i]))) {
            goto end;
        }
    }

    if ((ec = VN_GetVNId(vn, &vnId))) {
        goto end;
    }

    info.vnId = vnId;
    info.owner = VNG_MyUserId(vng);
    info.gameId = gameId;
    info.titleId = 0;
    info.accessControl = VNG_GAME_PUBLIC;
    info.netZone = 0;      // server will decide this value
    info.maxLatency = 500; // 500ms RTT 
    info.totalSlots = 100; // allow 100 users in the lobby
    info.buddySlots = 0;   // nothing reserved for buddies
    info.attrCount = 0;
    strncpy(info.keyword, serverName, VNG_GAME_KEYWORD_LEN);
    info.keyword[VNG_GAME_KEYWORD_LEN-1] = '\0';

    if ((ec = VNG_RegisterGame (vng, &info, comments, VNG_TIMEOUT))) {
        goto end;
    }

end:
    if (ec) {
        VNG_DeleteVN (vng, vn);
    }

    return ec;
}



// Destroy the registered server
//
VNGErrCode unregisterServer(VNG *vng, 
                            VN  *vn)
{
    VNGErrCode   ec = VNG_OK;
    VNGErrCode   rc = VNG_OK;
    VNId         vnId;

    if ((ec = VN_GetVNId(vn, &vnId))) {
        goto end;
    }

    ec = VNG_UnregisterGame (vng, vnId, VNG_TIMEOUT);

    rc = VNG_DeleteVN (vng, vn);

    if (!ec)
        ec = rc;

end:
    return ec;
}

  

  
// Find a server that matches gameId/serverName
// and join the associated VN.
//
VNGErrCode findServer(VNG        *vng, 
                      uint32_t    gameId, 
                      const char *serverName, 
                      VN         *vn, 
                      VNGTimeout  timeout)
{
    uint32_t           i;
    uint32_t           n = 1;
    VNGGameStatus      gameStatus[1];
    VNGSearchCriteria  crit;
    char               denyReason [128];
    VNGErrCode         ec = VNG_OK;

    crit.gameId = gameId;
    crit.domain = VNG_SEARCH_INFRA;
    crit.maxLatency = 30000;
    crit.cmpKeyword = VNG_CMP_STR;
    strncpy(crit.keyword, serverName, VNG_GAME_KEYWORD_LEN);
    crit.keyword[VNG_GAME_KEYWORD_LEN-1] = '\0';

    for (i = 0; i < VNG_MAX_PREDICATES; i++)
        crit.pred[i].cmp = VNG_CMP_DONTCARE;

    ec = VNG_SearchGames(vng, &crit, gameStatus, &n, 0, VNG_TIMEOUT);
    if (ec) {
        goto end;
    }

    if (!n) {
        ec = VNGERR_NOT_FOUND;
        goto end;
    }

    if ((ec = VNG_JoinVN (vng,
                          gameStatus[0].gameInfo.vnId,
                          NULL,
                          vn,
                          denyReason,
                          sizeof denyReason,
                          VNG_TIMEOUT))) {
        goto end;
    }


end:
    return ec;
}

  
  
  
// getServerList returns the set of server names that match strPat
// where strPat is a VNGSearchCriteria keyword string match pattern.
//
// The caller sets nServerNames to the max names that can be
// returned in serverNames.  On return nServerNames will
// indicate the number of names returned.
// 
extern VNGErrCode getServerList(VNG          *vng, 
                                uint32_t      gameId, 
                                const char   *strPat, 
                                uint32_t      skip, 
                                VNGServerName serverNames[], 
                                uint32_t     *nServerNames, 
                                VNGTimeout    timeout)
{
    uint32_t           i;
    VNGGameStatus      gameStatus[100];
    uint32_t           n = sizeof gameStatus / sizeof gameStatus[0];
    VNGSearchCriteria  crit;
    VNGErrCode         ec = VNG_OK;

    crit.gameId = gameId;
    crit.domain = VNG_SEARCH_INFRA;
    crit.maxLatency = 30000;
    crit.cmpKeyword = VNG_CMP_STR;
    strncpy(crit.keyword, strPat, VNG_GAME_KEYWORD_LEN);
    crit.keyword[VNG_GAME_KEYWORD_LEN-1] = '\0';

    for (i = 0; i < VNG_MAX_PREDICATES; i++)
        crit.pred[i].cmp = VNG_CMP_DONTCARE;

    if (*nServerNames > n)
        *nServerNames = n;

    ec = VNG_SearchGames(vng, &crit, gameStatus, nServerNames, 0, VNG_TIMEOUT);
    if (ec) {
        nServerNames = 0;
        goto end;
    }

    if (!*nServerNames) {
        ec = VNGERR_NOT_FOUND;
        goto end;
    }

    for (i = 0;  i < *nServerNames;  ++i) {
        strncpy (serverNames[i],
                 gameStatus->gameInfo.keyword, VNG_GAME_KEYWORD_LEN);
    }

end:
    return ec;
}




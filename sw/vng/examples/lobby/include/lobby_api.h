//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vng.h"


#define VIEWER_GAMEID 1


// registerServer creates a VN and registers an array of remote procedures 
//
// The procId for the VNRemoteProcedures passed in the procs[] argument to
// registerServer() will be automatically assigned starting from 0 and
// incremented by one for each VMRemoteProcedure in procs[]
//
VNGErrCode registerServer(VNG               *vng, 
                          uint32_t           gameId, 
                          const char        *serverName, 
                          VNRemoteProcedure  procs[],
                          uint32_t           nProcs,
                          VN                *vn);    // VN created



// Destroy the registered server
//
VNGErrCode unregisterServer(VNG *vng, 
                            VN  *vn);

  

// Find a server that matches gameId/serverName
// and join the associated VN.
//
VNGErrCode findServer(VNG        *vng, 
                      uint32_t    gameId, 
                      const char *serverName, 
                      VN         *vn, 
                      VNGTimeout  timeout);

  
  
  
typedef char VNGServerName [VNG_GAME_KEYWORD_LEN]; 
  
// getServerList returns the set of server names that match strPat
// where strPat is a VNGSearchCriteria keyword string match pattern.
//
// The caller sets nServerNames to the max names that can be
// returned in serverNames.  On return nServerNames will
// indicate the number of names returned.
// 
extern VNGErrCode getServerList(VNG          *vng, 
                                uint32_t      gameId, 
                                const char   *strPat, 
                                uint32_t      skip, 
                                VNGServerName serverNames[], 
                                uint32_t     *nServerNames, 
                                VNGTimeout    timeout);


//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "../include/lobby_api.h"
#include "../include/lobby_server.h"
#include "../../include/testcfg.h"
#include "stdio.h"

#if defined(_WIN32)
    #include <windows.h>
#elif defined(_LINUX)
    #include <unistd.h>
    #include <pthread.h>
    #define Sleep(msec) usleep(msec*1000)
#endif


#define CLIENT_TIMEOUT_SECS    30



static
int32_t rpcLobby0 (VN            *vn,
                  VNMsgHdr      *hdr,
                  const void    *args,
                  size_t         args_len,
                  void          *ret,
                  size_t        *ret_len);
static
int32_t rpcLobby1 (VN            *vn,
                  VNMsgHdr      *hdr,
                  const void    *args,
                  size_t         args_len,
                  void          *ret,
                  size_t        *ret_len);
static
int32_t rpcLobby2 (VN          *vn,
                   VNMsgHdr    *hdr,
                   const void  *args,
                   size_t       args_len,
                   void        *ret,
                   size_t      *ret_len);


VNRemoteProcedure  lobbyProcs[NUM_LOBBY_RPCS] = {
    rpcLobby0,
    rpcLobby1,
    rpcLobby2,
};



static bool rpcLobby0_received;
static bool rpcLobby1_received;
static bool rpcLobby2_received;




int rpcServeThread(void *arg)
{
    VNG        *vng  = (VNG*) arg;

    VNGErrCode  ec;

    // serve RPCs

    while (!(ec=VNG_ServeRPC(vng, VNG_WAIT))) {
        ;
    }

    if (ec != VNGERR_FINI) {
        printf ("rpcServeThread exiting because ec %d\n", ec);
    }

    return 0;
}



VNGErrCode  serveRPCs(VNG *vng)
{
    VNGErrCode  ec = VNG_OK;
    int         i;

    #ifdef _WIN32
        HANDLE      th;
        DWORD       tid;

        if (!(th = CreateThread(NULL, 0,
                                (LPTHREAD_START_ROUTINE) rpcServeThread,
                                vng, 0, &tid))) {
            printf ("serveRPCs  CreateThread failed\n");
            ec = VNGERR_UNKNOWN;
        }
    #elif defined (_LINUX)
        pthread_t th;
        int       rv;

        if ((rv = pthread_create (
                        &th, NULL,
                        (void * (*)(void *)) rpcServeThread,
                        vng))) {
            printf ("startRPCServe  pthread_create failed %d\n", rv);
            ec = VNGERR_UNKNOWN;
        }
    #else
        return VNGERR_UNKNOWN;
    #endif

    if (!ec) {
        ec = VNGERR_TIMEOUT;

        for (i = 0;  i < CLIENT_TIMEOUT_SECS;  ++i) {
            Sleep(1000);
            if (rpcLobby0_received &&
                    rpcLobby1_received &&
                        rpcLobby2_received) {
                ec = VNG_OK;
                break;
            }
        }
    }

    return ec;
}



int main(int argc, char* argv[])
{
    VNGErrCode  ec = VNG_OK;
    VNGErrCode  rc = VNG_OK;
    VNG         vng;
    VN          vn;
    uint32_t    nProcs;
    char        msg[256];

    printf ("LIBVNG Lobby.server\n");
    printf ("-------------------\n");

    if ((ec = VNG_Init (&vng, NULL))) {
        printf ("VNG_Init() Failed %d\n", ec);
        goto end;
    } else {
        printf ("VNG_Init() successful\n");
    }

    if ((ec = VNG_Login (&vng, VNGS_HOST, VNGS_PORT,
                        TESTER01, TESTER01PW, VNG_TIMEOUT))) {
        printf ("VNG_Login() Failed %d\n", ec);
        goto fini;
    } else {
        printf ("VNG_Login() successful\n");
    }

    nProcs = sizeof lobbyProcs / sizeof lobbyProcs[0];

    if ((ec = registerServer (&vng,
                              VIEWER_GAMEID,
                              "lobby_main",
                              lobbyProcs,
                              nProcs,
                              &vn))) {
        printf ("registerServer() Failed %d\n", ec);
        goto logout;
    } else {
        printf ("registerServer() successful\n");
    }

    // returns from serveRPCs when test done or failed
    if ((ec = serveRPCs(&vng))) {
        printf ("serveRPCs() Failed %d\n", ec);
    } else {
        printf ("serveRPCs() successful\n");
    }
    
    if ((rc = unregisterServer (&vng, &vn))) {
        printf ("unregisterServer() Failed %d\n", rc);
        if (!ec)
            ec = rc;
        goto logout;
    } else {
        printf ("unregisterServer() successful\n");
    }

logout:
    if ((rc = VNG_Logout (&vng, VNG_TIMEOUT))) {
        printf ("VNG_Logout()  Failed: %d\n", rc);
        if (!ec)
            ec = rc;
    } else {
        printf ("VNG_Logout() successful\n");
    }

fini:
    if ((rc = VNG_Fini (&vng))) {
        printf ("VNG_Fini()  Failed: %d\n", rc);
        if (!ec)
            ec = rc;
    } else {
        printf ("VNG_Fini() successful\n");
    }

end:
    if (ec) {
        if ((rc = VNG_ErrMsg (ec, msg, sizeof msg))) {
            printf ("VNG_ErrMsg(%d)  Failed %d\n", ec, rc);
            if (!ec)
                ec = rc;
        } else {
            printf ("VNG_ErrMsg(%d):  %s\n", ec, msg);
        }
    }

    printf ("*** LIBVNG Lobby.server TEST %s\n", (ec == 0) ? "PASSED" : "FAILED");
    return ec;
}



static
int32_t rpcLobby0 (VN            *vn,
                  VNMsgHdr      *hdr,
                  const void    *args,
                  size_t         args_len,
                  void          *ret,
                  size_t        *ret_len)
{
    rpcLobby0_received = true;
    return 0;
}

static
int32_t rpcLobby1 (VN          *vn,
                   VNMsgHdr    *hdr,
                   const void  *args,
                   size_t       args_len,
                   void        *ret,
                   size_t      *ret_len)
{
    rpcLobby1_received = true;
    return 0;
}

static
int32_t rpcLobby2 (VN          *vn,
                   VNMsgHdr    *hdr,
                   const void  *args,
                   size_t       args_len,
                   void        *ret,
                   size_t      *ret_len)
{
    rpcLobby2_received = true;
    return 0;
}






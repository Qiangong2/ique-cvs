//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "../include/lobby_api.h"
#include "../include/lobby_server.h"
#include "../../include/testcfg.h"
#include "stdio.h"

#if defined(_WIN32)
    #include <windows.h>
#endif

  




VNGErrCode client (VNG *vng)
{
    VNGServerName  serverNames [10];
    uint32_t       nServerNames = (sizeof serverNames/sizeof serverNames[0]);
    VN             vn;
    int32_t        optData;
    VNGErrCode     ec = VNG_OK;
    VNGErrCode     rc = VNG_OK;
    VNServiceTag   serviceTag;

    if ((ec = getServerList (vng,
                             VIEWER_GAMEID,
                             "lobby*",
                             0, // skip
                             serverNames,
                             &nServerNames,
                             VNG_TIMEOUT))) {
        printf ("getServerList() Failed %d\n", ec);
        goto end;
    }
    else if (!nServerNames) {
        printf ("getServerList() did not find any serverNames\n");
        ec = VNGERR_NOT_FOUND;
        goto end;
    }
    else {
        printf ("getServerList() found %u serverNames\n", nServerNames);
    }


     if ((ec = findServer (vng,
                           VIEWER_GAMEID,
                           serverNames[0],
                           &vn,
                           VNG_TIMEOUT))) {
        printf ("findServer() Failed %d\n", ec);
        goto end;
    } else {
        printf ("findServer(%s) successful\n", serverNames[0]);
    }


    for (serviceTag = 0; serviceTag < NUM_LOBBY_RPCS;   ++serviceTag) {
        if ((ec = VN_SendRPC (&vn, VN_Owner(&vn),
                            serviceTag,
                            NULL, 0,
                            NULL, 0,
                            &optData,
                            VNG_TIMEOUT))) {
            printf ("VN_SendRPC(%u) Failed %d\n", serviceTag, ec);
        } else {
            printf ("VN_SendRPC(%u) successful\n", serviceTag);
        }
    }


    if ((rc = VNG_LeaveVN (vng, &vn))) {
        printf ("VNG_LeaveVN()  Failed: %d\n", rc);
        if (!ec)
            ec = rc;
    } else {
        printf ("VNG_LeaveVN() successful\n");
    }

end:
    return ec;
}






int main(int argc, char* argv[])
{
    VNGErrCode  ec = VNG_OK;
    VNGErrCode  rc = VNG_OK;
    VNG         vng;
    char        msg[256];

    printf ("LIBVNG Lobby.client\n");
    printf ("-------------------\n");

    if ((ec = VNG_Init (&vng, NULL))) {
        printf ("client VNG_Init() Failed %d\n", ec);
        goto end;
    } else {
        printf ("client VNG_Init() successful\n");
    }

    if ((ec = VNG_Login (&vng, VNGS_HOST, VNGS_PORT,
                        TESTER01, TESTER01PW, VNG_TIMEOUT))) {
        printf ("client VNG_Login() Failed %d\n", ec);
        goto fini;
    } else {
        printf ("client VNG_Login() successful\n");
    }

    if ((ec = client(&vng))) {
        printf ("client Failed %d\n", ec);
    } else {
        printf ("client() successful\n");
    }
    
    if ((rc = VNG_Logout (&vng, VNG_TIMEOUT))) {
        printf ("client client VNG_Logout()  Failed: %d\n", rc);
        if (!ec)
            ec = rc;
    } else {
        printf ("VNG_Logout() successful\n");
    }

fini:
    if ((rc = VNG_Fini (&vng))) {
        printf ("client VNG_Fini()  Failed: %d\n", rc);
        if (!ec)
            ec = rc;
    } else {
        printf ("client VNG_Fini() successful\n");
    }

end:
    if (ec) {
        if ((rc = VNG_ErrMsg (ec, msg, sizeof msg))) {
            printf ("client VNG_ErrMsg(%d)  Failed %d\n", ec, rc);
            if (!ec)
                ec = rc;
        } else {
            printf ("client VNG_ErrMsg(%d):  %s\n", ec, msg);
        }
    }

    printf ("*** LIBVNG Lobby.client TEST %s\n", (ec == 0) ? "PASSED" : "FAILED");
    return ec;
}






#include <sc/sc.h>
#include <sc/ios.h>
#include <ioslibc.h>
#include <vng.h>

#define STACK_SIZE   (16*1024)
const u8  _initStack [STACK_SIZE];
const u32 _initStackSize = sizeof(_initStack);
const u32 _initPriority = 5;

int main(void)
{
    IOSMessage         msg;
    IOSError           rv;
    IOSMessageQueueId  mq;

    VOICE_StartRM();

    /* Create a message queue */
    if ((mq = IOS_CreateMessageQueue (&msg, 1)) < 0) {
        rv = mq;
        printf("Test RM IOS_CreateMessageQueue for resource manager returned %d\n", rv);
    }

    while (1) {
        IOS_ReceiveMessage(mq, &msg, IOS_MESSAGE_BLOCK);
        printf("shouldn't be here\n");
    }
    printf("shouldn't be here\n");
}

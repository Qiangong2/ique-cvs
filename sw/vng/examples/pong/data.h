#ifndef _DATA_H
#define _DATA_H

#include <Agb.h>


/*-------------------------- Data -------------------------------*/

extern u32 CharData_Sample[8*0xe0];

extern u16 PlttData_Sample[16][16];

extern u16 BgScData_Sample[32*20];

extern u32 OamData_Sample[1][2];

extern u8  SoundBak[256];

#endif /* _DATA_H */

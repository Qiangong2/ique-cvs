TEST SERVER
-----------

The default test server is defined in include/test.cfg. 
It must be modified to point to your local server.


VNG Examples
------------

The sanity and echoping can be compiled for NC, GBA-NC and LINUX.  The
pong program can only be compiled for GBA-NC.

The executable for Linux are is called REL/LINUX/<program-name> The
executable for a pure NC application is called
REL/SC/<program-name>.mrg.  The executable for a GBA application to
called REL/SC/<program-name>.wad.


make TARGET_HOST=LINUX # build a Linux application
make TARGET_HOST=SC    # build a NC application
make TARGET_HOST=GBA   # build a GBA application and NC launcher


To build a LINUX executable, you must do the build on LINUX.  NC and GBA
builds can be done in a cygwin shell window on a Windows PC.

To force a re-build of VNG example executables, first do:

make clean TARGET_HOST=<target>  # where <target> is LINUX, SC, or GBA

sanity: It tests very basic VNG functions to demostrate that the USB
driver and the NCPROXY can communicate with the VN daemon running on
NC.

echoping: It contains a echo server and a ping server.  It estimates
the round-trip delay for sending and recieving messages using VNG.  It
demostrates connection between VNG client to the VNG server and
peer-to-peer message deliveries.

pong: A network game demonstrates simple VNG features.  It shows the
basic elements of a VNG game.  It does a first-come-first-served
matchmaking for demo purposes.  Real matchmaking will be done by the
viewer in the product.


When sanity and echoping is running on NC or GBA-NC, the output is
sent to the ncproxy.  You might use ncprint to display the output, or
add -p option to ncproxy to display the output from ncproxy.


How to run
----------

Follow the sdk instruction to "import" <program>.wad into the NC.
Launch <program.wad> from the NC.

Make sure ncproxy is running on the PC, and the USB cable connects NC
to the PC.

  $ROOT/usr/bin/win32/ncproxy -p

You should see the following when the NC device is recognized by
ncproxy.


PRXY: 000500 00001328 FINE App 2 Connect real USB 0 to printf
PRXY: 000515 00001380 FINE App 2 Starting printf thread for USB 0
PRXY: 000515 00000f04 FINE App 0 Attempting to open real USB connection
PRXY: 000515 00000f04 FINE VN 2 Attempting to open USB \\?\usb#vid_bb3d&pid_bbdb#5&33d83d18&0&2#{9
c37855e-3d67-487a-b70e-eb4ea357a969}
PRXY: 000515 00000f04 FINE VN 2 USB DeviceName = (\\?\usb#vid_bb3d&pid_bbdb#5&33d83d18&0&2#{9c3785
5e-3d67-487a-b70e-eb4ea357a969}\PIPE01)
PRXY: 000515 00000f04 FINE VN 2 USB \\?\usb#vid_bb3d&pid_bbdb#5&33d83d18&0&2#{9c37855e-3d67-487a-b
70e-eb4ea357a969}\PIPE01 Opened successfully.
PRXY: 000531 00000f04 FINE VN 2 Attempting to open USB \\?\usb#vid_bb3d&pid_bbdb#5&33d83d18&0&2#{9
c37855e-3d67-487a-b70e-eb4ea357a969}
PRXY: 000531 00000f04 FINE VN 2 USB DeviceName = (\\?\usb#vid_bb3d&pid_bbdb#5&33d83d18&0&2#{9c3785
5e-3d67-487a-b70e-eb4ea357a969}\PIPE00)
PRXY: 000531 00000f04 FINE VN 2 USB \\?\usb#vid_bb3d&pid_bbdb#5&33d83d18&0&2#{9c37855e-3d67-487a-b
70e-eb4ea357a969}\PIPE00 Opened successfully.
PRXY: 000531 00000f04 FINE App 0 Putting NC at USB 0 into ready state...
PRXY: 000531 00000f04 FINE App 0 Waiting for NC response for USB 0...
PRXY: 000531 00000f04 FINE App 0 Attaching USB MUX 0
PRXY: 000531 00000f04 FINE App 2 Connect real USB 0 to VNProxy
PRXY: 000531 000015fc FINE App 2 Starting USB MUX recv dispatcher 0
PRXY: 000625 00000a8c FINE App 0 Listening for simulated USB connections on TCP port 20010
PRXY: 001000 00001468 FINE App 2 Starting USB MUX send dispatcher 0
PRXY: 001140 00000a8c FINE App 0 Accept USB connection from 127.0.0.1:3274
PRXY: 001140 00000acc FINE App 0 Waiting for VN client 1 to connect

PRXY: 001140 000009f8 FINE VN 3 Starting USB RPC dispatcher 1
PRXY: 001140 000017cc FINE VN 3 Starting USB RPC server 1



GBA or NC printf are redirected to Windows console.  e.g, the
following is the output from sanity.



USB0: GBA program launched ...
USB0:   Allocate Data Structure ok ...
USB0: vng sanity test begin
PRXY: 015937 00001690 FINE VN 8 Using VN port from 0 to 0
USB0:   VNG_Init ok ...
USB0:   VNG_NewVN ok ...
USB0:   VN_SendMsg ok ...
USB0:   VN_RecvMsg ok ...
USB0:   Send/Recv Sequences ok ...
USB0:   VNG_DeleteVN ok ...
USB0:   VNG_GetEvent - NOWAIT ok ...
USB0:   VNG_Fini ok ...
USB0: vng sanity test end
USB0: VNG TEST PASSED

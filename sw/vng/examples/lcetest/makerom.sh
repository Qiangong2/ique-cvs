#!/bin/sh

#  Create a 16M rom image from lcetest.bin so that this test can run on GBA.
#  The swi entry point is at 0x0AFFFFFC, i.e., the last word of
#  the 16M image.
#  

BRINST='\x89\x00\xc0\xea' 

cp lcetest.bin lcerom.bin
dd if=/dev/zero bs=1k count=16384 >> lcerom.bin
printf $BRINST | dd of=lcerom.bin bs=1 seek=16777212



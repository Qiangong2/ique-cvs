@********************************************************************
@*          crt0.s                                                  *
@*            Start Up Routine (For GAS)                            *
@*                                                                  *
@*          Copyright (C) 1999-2001 NINTENDO Co.,Ltd.               *
@********************************************************************
    .INCLUDE    "AgbDefine.s"
    .INCLUDE    "AgbMemoryMap.s"
    .INCLUDE    "AgbSyscallDefine.s"
    .INCLUDE    "AgbMacro.s"
    .TEXT

    .GLOBAL     _start
_start:
    .INCLUDE    "rom_header.s"

@--------------------------------------------------------------------
@-                          Reset                                   -
@--------------------------------------------------------------------
    .EXTERN     AgbMain
    .GLOBAL     start_vector
    .CODE 32
start_vector:
        mov     r0, #PSR_IRQ_MODE       @ Switch to IRQ Mode
        msr     cpsr, r0
        ldr     sp, sp_irq              @ Set SP_irq 
        mov     r0, #PSR_SYS_MODE       @ Switch to System Mode
        msr     cpsr, r0
        ldr     sp, sp_usr              @ Set SP_usr
        ldr     r1, =INTR_VECTOR_BUF    @ Set Interrupt Address
        adr     r0, intr_main
        str     r0, [r1]
        ldr     r1, =AgbMain            @ Start & Switch to 16bit Code
        mov     lr, pc
        bx      r1
        b       start_vector            @ Reset

    .ALIGN
sp_usr: .word   WRAM_END - 0x100
sp_irq: .word   WRAM_END - 0x60
intr_suspend_addr:	.word  intr_suspend
intr_counter_addr:	.word  intr_counter
miss_counter_addr:  	.word  miss_counter
swi_counter_addr: 	.word	swi_counter

	.GLOBAL intr_intercept
	.ALIGN
	.CODE 32
intr_intercept:
	ldr	r0, intr_counter_addr
	ldr	r1, [r0]
	add	r1, r1, #1
	str	r1, [r0]
        mov     r3, #REG_BASE           @ Check IE/IF
        add     r3, r3, #OFFSET_REG_IE  @ r3: REG_IE
        ldr     r2, [r3]
        and     r1, r2, r2, lsr #16     @ r1: IE & IF
	ldr 	r0, intr_suspend_addr
	ldr	r0, [r0]
	ands	r1, r1, r0
	beq	intr_main
@
@ intr is suspended
@
        strh    r1, [r3, #2]            @ IF Clear          
	ldr	r0, miss_counter_addr
	ldr	r1, [r0]
	add	r1, r1, #1
	str	r1, [r0]
        bx      lr
		

@--------------------------------------------------------------------
@-       Interrupt Branch Process (Table Lookup) 32Bit        28-63c-
@--------------------------------------------------------------------
    .EXTERN     IntrTable
    .GLOBAL     intr_main
    .ALIGN
    .CODE 32
intr_main:
        mov     r3, #REG_BASE           @ Check IE/IF
        add     r3, r3, #OFFSET_REG_IE  @ r3: REG_IE
        ldr     r2, [r3]
        and     r1, r2, r2, lsr #16     @ r1: IE & IF
	ands    r0, r1, #CASSETTE_INTR_FLAG  @ Game Pak interrupt
        strneb  r0, [r3, #REG_SOUNDCNT_X - REG_IE]  @ Sound stop
loop:   bne     loop
        mov     r2, #0
        ands    r0, r1, #V_BLANK_INTR_FLAG   @ V Blank interrupt
        bne     jump_intr
        add     r2, r2, #4
        ands    r0, r1, #H_BLANK_INTR_FLAG   @ H Blank interrupt
        bne     jump_intr
        add     r2, r2, #4
        ands    r0, r1, #V_COUNT_INTR_FLAG   @ V Counter interrupt
        bne     jump_intr
        add     r2, r2, #4
        ands    r0, r1, #TIMER0_INTR_FLAG    @ Timer 0 interrupt
        bne     jump_intr
        add     r2, r2, #4
        ands    r0, r1, #TIMER1_INTR_FLAG    @ Timer 1 interrupt
        bne     jump_intr
        add     r2, r2, #4
        ands    r0, r1, #TIMER2_INTR_FLAG    @ Timer 2 interrupt
        bne     jump_intr
        add     r2, r2, #4
        ands    r0, r1, #TIMER3_INTR_FLAG    @ Timer 3 interrupt
        bne     jump_intr
        add     r2, r2, #4
        ands    r0, r1, #SIO_INTR_FLAG       @ Serial Communication interrupt
        bne     jump_intr
        add     r2, r2, #4
        ands    r0, r1, #DMA0_INTR_FLAG      @ DMA0 interrupt
        bne     jump_intr
        add     r2, r2, #4
        ands    r0, r1, #DMA1_INTR_FLAG      @ DMA1 interrupt
        bne     jump_intr
        add     r2, r2, #4
        ands    r0, r1, #DMA2_INTR_FLAG      @ DMA2 interrupt
        bne     jump_intr
        add     r2, r2, #4
        ands    r0, r1, #DMA3_INTR_FLAG      @ DMA3 interrupt
        bne     jump_intr
        add     r2, r2, #4
        ands    r0, r1, #KEY_INTR_FLAG       @ Key interrupt
jump_intr:
        strh    r0, [r3, #2]            @ IF Clear           11c
        ldr     r1, =IntrTable          @ Jump to user IRQ process
        add     r1, r1, r2
        ldr     r0, [r1]
        bx      r0

    .GLOBAL     swi_entry
    .ALIGN
    .CODE 32
swi_entry_main:
	ldr	r11, =swi_main
	mov	pc, r11
swi_entry:
	b	swi_entry_main

    .GLOBAL     swi_main
    .ALIGN
    .CODE 32
swi_main:
	ldr	r11, swi_counter_addr
	ldr	r12, [r11]
	add	r12, r12, #1
	str	r12, [r11]
	bx 	lr

@   .ORG    0x200

	.section .bss
	.GLOBAL  intr_suspend
	.GLOBAL  intr_counter
	.GLOBAL	 miss_counter
	.GLOBAL  swi_counter
intr_suspend:  	.word	 0
intr_counter:	.word	0
miss_counter:	.word	0
swi_counter:	.word	0

    .END


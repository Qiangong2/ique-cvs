//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vng.h"
#include "../include/testcfg.h"

#include "windows.h"

#include <iostream>
#include <time.h>
#include <string>
#include <vector>
using namespace std;

bool trace = true;
bool dumpHdrs_clearMsgs = true;
bool dumpHdrs_test = true;

void report(const char *testname, int pass)
{
    if (pass >= 0) 
        fprintf(stdout, "*** LIBVNG %s TEST PASSED\n", testname);
    else
        fprintf(stdout, "*** LIBVNG %s TEST FAILED\n", testname);
}

int dumpEvents(VNG *vng, VNGEventID  mustFindEvent)
{
    bool eventFound = false;

    // Wait for all events
    while (1) {
        VNGEvent event;
        VNGErrCode err = VNG_GetEvent(vng, &event, 1000);
        if (err == VNG_OK) {
           fprintf(stdout, "Got event %d", event.eid);
           if (event.eid == VNG_EVENT_PEER_STATUS) {
               VNId vid;
               VN_GetVNId(event.evt.peerStatus.vn, &vid);
               fprintf(stdout, " -- 0x%08x.%08x.%08x %d",  (int)(vid.deviceId >> 32), (int)vid.deviceId, vid.netId, event.evt.peerStatus.memb);
           }    
           fprintf(stdout, "\n");
           if (event.eid == mustFindEvent) {
               eventFound = true;
           }
        } else if (err == VNGERR_NOWAIT || err == VNGERR_TIMEOUT)
           break;
    }

    if (mustFindEvent && !eventFound) {
        fprintf(stderr, "Didn't find event %d\n", mustFindEvent);
    }

    return 0;
}

void dumpStoredMsgHdr(VNGMessageHdr *s)
{
#ifdef _WIN32
    fprintf(stdout, "\nsendTime %I64d\n", s->sendTime);
#else
    fprintf(stdout, "\nsendTime %lld\n", s->sendTime);
#endif
    fprintf(stdout, "msgId %d\n", s->msgId);
    fprintf(stdout, "subject %s\n", s->subject);
    fprintf(stdout, "senderName %s\n", s->senderName);
    fprintf(stdout, "recipientName %s\n", s->recipientName);
    fprintf(stdout, "status %u\n", s->status);
    fprintf(stdout, "mediaType %u\n", s->mediaType);
    fprintf(stdout, "msgLen %u\n", s->msgLen);
    fprintf(stdout, "replyMsgid %u\n\n", s->replyMsgid);
}

const char*  stmRecipientNames[] = {
    TESTER01,
    "10002",
};

const char*  stmRecipientPasswds[] = {
    TESTER01PW,
    "10002",
};

const char msg1[] = "Message 1";
const char msg2[] = "Message 2";
const char msg3[] = "Message 3";
char bigMsg[VNG_MAX_STORED_MSG_LEN+1];
char offset;

typedef enum {
    SINGLE_RECIP,
    MULT_RECIP,
    NOT_LOGGED_IN,
    SINGLE_RECIP_MAX,
    SINGLE_RECIP_MAX_M1_VOICE,
    SINGLE_RECIP_MAX_P1,
    MULT_RECIP_BIG,
    NUM_TEST_IDS
} TestId;

typedef struct {
    TestId         id;
    const char    *testname;
    const char   **recipientNames;
    const char   **recipientPasswds;
    uint32_t       nRecipients;
    const char    *subject;
    uint32_t       mediaType;
    const char    *msg;
    size_t         msgLen;
    uint32_t       replyMsgid;
    uint32_t       msgBufferLen;

} TestInfo;

static TestInfo tests[] = {
    {SINGLE_RECIP,
        "StoredMsg single",
        stmRecipientNames, stmRecipientPasswds,
        1, "Single recipient",
        VNG_MEDIA_TYPE_TEXT, msg1,
        sizeof msg1, 0, sizeof msg1},

    {MULT_RECIP,
        "StoredMsg mult",
        stmRecipientNames, stmRecipientPasswds,
        2, "Multiple recipient",
        VNG_MEDIA_TYPE_TEXT, msg2,
        sizeof msg2, 0, sizeof msg2 + 16},

    {NOT_LOGGED_IN,
        "StoredMsg NotLoggedIn",
        stmRecipientNames, stmRecipientPasswds,
        1, "Not Logged In",
        VNG_MEDIA_TYPE_TEXT, msg3,
        sizeof msg3, 0, sizeof msg3},

    {SINGLE_RECIP_MAX,
        "StoredMsg single max",
        stmRecipientNames, stmRecipientPasswds,
        1, "Single recipient max",
        VNG_MEDIA_TYPE_TEXT, bigMsg,
        VNG_MAX_STORED_MSG_LEN, 0, VNG_MAX_STORED_MSG_LEN},

    {SINGLE_RECIP_MAX_M1_VOICE,
        "StoredMsg single voice max -1",
        stmRecipientNames, stmRecipientPasswds,
        1, "Single recipient voice max - 1",
        VNG_MEDIA_TYPE_VOICE, bigMsg,
        VNG_MAX_STORED_MSG_LEN-1, 0, VNG_MAX_STORED_MSG_LEN},

    {SINGLE_RECIP_MAX_P1,
        "StoredMsg single max + 1",
        stmRecipientNames, stmRecipientPasswds,
        1, "Single recipient max -+ 1",
        VNG_MEDIA_TYPE_TEXT, bigMsg,
        VNG_MAX_STORED_MSG_LEN+1, 0, VNG_MAX_STORED_MSG_LEN+1},

    {MULT_RECIP_BIG,
        "StoredMsg mult big",
        stmRecipientNames, stmRecipientPasswds,
        2, "Multiple recipient big",
        VNG_MEDIA_TYPE_TEXT, bigMsg,
        VN_MAX_MSG_LEN, 0, VN_MAX_MSG_LEN},
};



#define MAX_STORED_MSGS 1000

vector<uint32_t>  deleteAllMsgs;

int clearMsgs(VNG *vng, vector<uint32_t>& deleteId = deleteAllMsgs)
{
    VNGErrCode err;
    int        res = 0;
    unsigned   i, k;

    VNGMessageHdr msgHdr[MAX_STORED_MSGS];
    uint32_t      nMsgHdr;

    uint32_t msgId[MAX_STORED_MSGS];
    uint32_t nMsgHdrMax = sizeof(msgHdr) / sizeof(msgHdr[0]);
    uint32_t nMsgId = 0;

    if (deleteId == deleteAllMsgs) {
        nMsgHdr = nMsgHdrMax;
        err = VNG_GetMessageList (vng, 0, msgHdr, &nMsgHdr, VNG_TIMEOUT);

        if (err) {
            fprintf(stderr, "clearMsgs VNG_GetMessageList 1 returns %d\n", err);
            res = -1;
            goto fini;
        }

        for (i = 0;   i < nMsgHdr;   ++i) {
            if (dumpHdrs_clearMsgs)
                dumpStoredMsgHdr(&msgHdr[i]);
            msgId[i] = msgHdr[i].msgId;
            ++nMsgId;
        }
    }
    else {
        for (i = 0;  i < deleteId.size();  ++i) {
            msgId[i] = deleteId[i];
        }
        ++nMsgId;
    }

    if (nMsgId) {
        err = VNG_DeleteMessage (vng, msgId, nMsgId, VNG_TIMEOUT);
        if (err) {
            fprintf(stderr, "VNG_DeleteMessage returns %d\n", err);
            res = -1;
            goto fini;
        }

        nMsgHdr = nMsgHdrMax;
        err = VNG_GetMessageList (vng, 0, msgHdr, &nMsgHdr, VNG_TIMEOUT);

        if (err) {
            fprintf(stderr, "clearMsgs VNG_GetMessageList 2 returns %d\n", err);
            res = -1;
            goto fini;
        }

        for (k = 0;  k < nMsgId;  ++k) {
            for (i = 0;  i < nMsgHdr;  ++i) {
                if (msgHdr[i].msgId == msgId[k]) {
                    fprintf(stderr, "clearMsgs failed to delete %d\n", msgId[k]);
                    res = -1;
                    goto fini;
                }
            }
            fprintf(stdout, "%s%u", (k ? " ":"Deleted msgId "), msgId[k]);
        }
        fprintf(stdout, "\n");
    }

fini:
    return res;
}

int clearUserMsgs (VNG *vng, const char *username, const char *passwd)
{
    VNGErrCode err;
    int        res = 0;

    err = VNG_Login(vng, VNGS_HOST, VNGS_PORT,
                username, passwd,
                VNG_TIMEOUT);

    if (err != VNG_OK) {
        fprintf(stderr, "clearUserMsgs VNG_Login %s returns %d\n", username, err);
        res = -1;
        goto end;
    }
    else if (trace) {
        fprintf(stdout, "clearUserMsgs VNG_Login %s successful\n", username);
    }

    if (clearMsgs(vng)) {
        fprintf(stderr, "clearMsgs failed\n");
        res = -1;
        goto end;
    }

    err = VNG_Logout(vng, VNG_TIMEOUT);

    if (err != VNG_OK) {
        fprintf(stderr, "clearUserMsgs VNG_Logout %s returns %d\n", username, err);
        res = -1;
    }
    else if (trace) {
        fprintf(stdout, "clearUserMsgs VNG_Logout %s successful\n", username);
    }

end:
    return res;
}


int testStoredMsgs (TestInfo&  test,
                    bool       clearMsgsFirst = false)
{
    VNGErrCode err;
    int res = 0;
    VNG vng;
    VNDomain domain = VN_DOMAIN_INFRA;
    unsigned  i;

    uint32_t          results[100];
    vector<uint32_t>  deleteIds;



    if (test.id == NOT_LOGGED_IN)
        domain = VN_DOMAIN_ADHOC;

    fprintf(stdout, "\n*** LIBVNG %s TEST START\n", test.testname);

    VNG_Init(&vng, NULL);

    if (domain == VN_DOMAIN_INFRA) {

        if (clearMsgsFirst && test.nRecipients > 1) {
            for (i = 0; i < test.nRecipients; ++i) {
                if (strcmp (test.recipientNames[i], TESTER01)) {
                    if (clearUserMsgs (&vng, test.recipientNames[i],
                                            test.recipientPasswds[i])) {
                        fprintf(stderr, "clearUserMsgs failed\n");
                        res = -1;
                        goto fini;
                    }

                }
            }
        }

        err = VNG_Login(&vng, VNGS_HOST, VNGS_PORT,
                    TESTER01, TESTER01PW,
                    VNG_TIMEOUT);

        if (err != VNG_OK) {
            fprintf(stderr, "testStoredMsgs VNG_Login returns %d\n", err);
            res = -1;
            goto fini;
        }
        else if (trace) {
            fprintf(stdout, "testStoredMsgs VNG_Login successful\n");
        }

        if (clearMsgsFirst && clearMsgs(&vng)) {
            fprintf(stderr, "clearMsgs failed\n");
            res = -1;
            goto fini;
        }
    }

    if (test.msg == bigMsg) {
        char c = offset++;
        for (i = 0;  i < sizeof bigMsg;  ++i) {
            if (test.mediaType == VNG_MEDIA_TYPE_TEXT) {
                if (c < 'a' || c > 'z') {
                    c = 'a';
                }
            }
            bigMsg[i] = c++;
        }
        if (test.mediaType == VNG_MEDIA_TYPE_TEXT) {
           bigMsg[test.msgLen - 1] = '\0';
        }
    }

    err = VNG_StoreMessage (&vng,
                            test.recipientNames,
                            test.nRecipients,
                            test.subject,
                            test.mediaType,
                            test.msg,
                            test.msgLen,
                            test.replyMsgid,
                            results,
                            VNG_TIMEOUT);
    if (err) {
        if (!(test.id == NOT_LOGGED_IN && err == VNGERR_NOT_LOGGED_IN) &&
            !(test.id == SINGLE_RECIP_MAX_P1 && err == VNGERR_INVALID_LEN)) {
            fprintf(stderr, "VNG_StoreMessage returns %d\n", err);
            res = -1;
        }
        goto fini;
    }

    for (i = 0;   i < test.nRecipients;  ++i) {
        if (results[i] != VNG_OK) {
            fprintf(stderr, "VNG_StoreMessage result %d is %d\n", i, results[i]);
            res = -1;
            goto fini;
        }
    }

    if (trace) {
        fprintf(stdout, "message \"%s\" stored successfully\n", test.subject);
    }

    VNGMessageHdr msgHdr[100];
    uint32_t      nMsgHdr = sizeof(msgHdr) / sizeof(msgHdr[0]);

    err = VNG_GetMessageList (&vng, 0, msgHdr, &nMsgHdr, VNG_TIMEOUT);

    if (err) {
        fprintf(stderr, "VNG_GetMessageList returns %d\n", err);
        res = -1;
        goto fini;
    }

    int  found = -1;

    for (i = 0;   i < nMsgHdr;   ++i) {
        if (trace)
            dumpStoredMsgHdr(&msgHdr[i]);

        if (!strncmp(msgHdr[i].subject, test.subject, VNG_SUBJECT_BUF_SIZE) &&
                !strncmp(msgHdr[i].senderName, TESTER01, VNG_LOGIN_BUF_SIZE) &&
                !strncmp(msgHdr[i].recipientName, TESTER01, VNG_LOGIN_BUF_SIZE) &&
                msgHdr[i].mediaType == test.mediaType && 
                msgHdr[i].replyMsgid == test.replyMsgid) {

            size_t len = test.msgLen;
            if (msgHdr[i].mediaType == VNG_MEDIA_TYPE_TEXT) {
                len = strnlen (test.msg, len) + 1;
            }

            if (msgHdr[i].msgLen == len)
                found = i;
        }
    }

    if (found < 0) {
        fprintf(stderr, "Didn't find self as recipient\n", err);
        res = -1;
        goto fini;
    }

    char msgBuffer[VNG_MAX_STORED_MSG_LEN + 1024];
    size_t msgBufferLen = test.msgBufferLen;

    memset(msgBuffer, 'x', sizeof msgBuffer);

    err = VNG_RetrieveMessage (&vng,
                               msgHdr[found].msgId,
                               msgBuffer,
                               &msgBufferLen,
                               VNG_TIMEOUT); 
    if (err) {
        fprintf(stderr, "VNG_RetrieveMessage returns %d\n", err);
        res = -1;
        goto fini;
    }

    size_t len = test.msgLen;
    if (test.mediaType == VNG_MEDIA_TYPE_TEXT) {
        len = strnlen(test.msg, len) + 1;
    }

    if (msgBufferLen != len) {
        fprintf(stderr,
            "VNG_RetrieveMessage returned %d bytes instead of %d\n",
             msgBufferLen, len);
        res = -1;
        goto fini;
    }

    if (test.mediaType == VNG_MEDIA_TYPE_TEXT) {
        if (strncmp(msgBuffer, test.msg, msgBufferLen)) {
            msgBuffer[msgBufferLen - 1] = '\0';
            fprintf(stderr,
                "VNG_RetrieveMessage returned %s instead of %s\n",
                msgBuffer, test.msg);
            res = -1;
            goto fini;
        }
        msgBuffer[1024] = '\0';
        fprintf(stdout, "VNG_RetrieveMessage returned %s\n", msgBuffer);
    }
    else if (memcmp(msgBuffer, test.msg, len)) {
        fprintf(stderr,
            "VNG_RetrieveMessage retrieved binary msg doesn't match sent\n");
        res = -1;
        goto fini;
    }
    else {
        fprintf(stdout, "VNG_RetrieveMessage returned correct bin message\n");
    }


    deleteIds.push_back(msgHdr[found].msgId);

    clearMsgs (&vng, deleteIds);

    err = VNG_RetrieveMessage (&vng,
                               msgHdr[found].msgId,
                               msgBuffer,
                               &msgBufferLen,
                               VNG_TIMEOUT); 
    if (err != VNGERR_NOT_FOUND) {
        fprintf(stderr, "VNG_RetrieveMessage returns %d instead of VNGERR_NOT_FOUND\n", err);
        res = -1;
        goto fini;
    }


    res = dumpEvents(&vng, VNG_EVENT_NEW_STORED_MSG);
     
fini:
    fprintf(stdout, "Entering fini VN\n");
    VNG_Fini(&vng);

    return res;
}


int main(int argc, char* argv[])
{
    cout << "VNG StoredMsg Test" << endl;
    cout << "-----------------------" << endl;

    int res;

    unsigned int seed = (unsigned)time( NULL );
    srand( seed );
    cout << "seed = " << seed << endl;

    TestInfo  test;
    unsigned  i;

    unsigned  firstTest = 0;
    unsigned  numTests  = (sizeof tests / sizeof tests[0]) - firstTest;

    for (i = 0;  i < numTests;   ++i) {
        test = tests[firstTest + i];
        res = testStoredMsgs(test, ((i == 0 || i == 1) ? true : false));
        report(test.testname, res);
        if (res) {
            return 0;
        }
    }
    return 0;
}


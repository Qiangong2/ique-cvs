#include <sc/sc.h>
#include <sc/ios.h>
#include <ioslibc.h>
#include <sc/audbuf.h>
#include <internal.h> // libvoice
#include <vng.h>

#define STACK_SIZE   (64*1024)
const u8  _initStack [STACK_SIZE];
const u32 _initStackSize = sizeof(_initStack);
const u32 _initPriority = 15;

int debug = 0;
#define trace if (debug) printf

u8 buf[1024] __attribute__((aligned(16)));
u8 inbuf[4096] __attribute__((aligned(16)));
u8 outbuf[4096] __attribute__((aligned(16)));

static IOSError
lceOpen(IOSResourceRequest *req, IOSMessageQueueId mq)
{
    IOSError           rv = IOS_ERROR_OK;

    u32 config = AUD_CONFIG_ADC_8_DAC_8;
    u32 readSize = 2048;
    u32 inOv = AUDBUF_OVERFLOW_DISCARD_OLD;
    u32 outOv = AUDBUF_OVERFLOW_DISCARD_OLD;

    trace("audbufOpen\n");
    if ( (rv = audbufOpen(O_RDWR, config, readSize,
                          inbuf,sizeof(inbuf),inOv,
                          outbuf,sizeof(outbuf),outOv)) != IOS_ERROR_OK ) {
        printf("audbufOpen failed, rv = %d\n",rv);
        goto out;
    }
    audbufSetVolume(0x68);

    trace("libVoice init\n");
    initVoice(0);
    VNG_SelectVoiceCodec(0, VNG_VOICE_CODEC_ADPCM32);

    trace ("RM in lceOpen succeeded\n");

out:
    return rv;
}

XmitData tmpXmitBuf;

static IOSError
lceIoctlv(IOSResourceRequest *req, IOSMessageQueueId mq)
{
    IOSError           rv = IOS_ERROR_OK;
    int ret;
    int xmitSize;

    rv = audbufRead(buf, sizeof(buf), MONO, 0);
    if ( rv < 0 ) {
        printf("audbufRead failed, rv = %d\n",rv);
        goto out;
    }
    if ( rv == 0 ) goto out;

    xmitSize = sizeof(tmpXmitBuf);
    ret = makeXmitBuf(buf, sizeof(buf), &tmpXmitBuf, &xmitSize);
    if (ret < 0) {
        printf("makeXmitBuf failed, ret = %d\n",ret);
        goto out;
    }

    ret = onNetRecv(0, &tmpXmitBuf);
    if (ret < 0) {
        printf("onNetRecv failed, ret = %d\n",ret);
        goto out;
    }

    ret = getNextPlayBuf(buf, sizeof(buf));
    if (ret < 0) {
        printf("getNextPlayBuf failed, ret = %d\n",ret);
        goto out;
    }

    rv = audbufWrite(buf, sizeof(buf), MONO);
    if ( rv < 0 ) {
        printf("audbufWrite failed, rv = %d\n",rv);
        goto out;
    }

    trace ("RM in lceIoctlv succeeded\n");

out:
    return rv;
}



static IOSError
waitForRMCmds (IOSMessageQueueId mq)
{
    IOSError    rv;
    IOSMessage  msg;
    u32 cmd;

    while (1) {

        trace("Test RM waiting for commands\n");
        if ((rv = IOS_ReceiveMessage (mq, &msg, IOS_MESSAGE_BLOCK)) != IOS_ERROR_OK) {
            printf("Test RM IOS_ReceiveMessage returned %d\n", rv);
            goto end;
        }
        trace("Test RM received request 0x%08x\n", msg);


        IOSResourceRequest *req = (IOSResourceRequest*) msg;
        cmd = req->cmd;
        switch (cmd) {
            case IOS_OPEN:
                trace("Test RM IOS_OPEN\n");
                rv = lceOpen (req, mq); //IOS_ERROR_OK;
                break;
            case IOS_CLOSE:
                trace("Test RM IOS_CLOSE\n");
                rv = IOS_ERROR_OK;
                break;
            case IOS_READ:
                rv = IOS_ERROR_OK;
                break;
            case IOS_IOCTLV:
                rv = lceIoctlv (req, mq);
                break;
            default:
                printf("Test RM cmd %u not supported\n", req->cmd);
                rv = IOS_ERROR_INVALID;
                break;
        }
        trace("Test RM Returning %d\n", rv);
        IOS_ResourceReply(req, rv);
    }


end:
    return rv;
}


int main(void)
{
    IOSError           rv;
    IOSMessageQueueId  mq;
    IOSMessage         msgArray[3];

    trace("Test RM starting\n");

    /* Create a message queue */
    if ((mq = IOS_CreateMessageQueue (msgArray, sizeof(msgArray)/sizeof(msgArray[0]))) < 0) {
        rv = mq;
        printf("Test RM IOS_CreateMessageQueue for resource manager returned %d\n", rv);
        goto end;
    }

    /* Register the message queue with the kernel */
    if ((rv = IOS_RegisterResourceManager ("/dev/lcechat", mq)) != IOS_ERROR_OK) {
        printf("Test RM IOS_RegisterResourceManager returned %d\n", rv);
        goto end;
    }

    rv = waitForRMCmds (mq);

end:
    /* should never get here */
    printf("Test RM exiting with error %d\n", rv);

    return rv;
}

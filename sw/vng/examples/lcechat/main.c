#include "Agb.h"
#include "sc/ios.h"

typedef void    (*IntrFuncp) (void);
void
IntrDummy(void)
{
}
void VBlankIntr(void);

const IntrFuncp IntrTable[13] = {
    IntrDummy,
    VBlankIntr,                 // V-blank interrupt 
    IntrDummy,                 // V-counter match interrupt 
    IntrDummy,                  // H-blank interrupt 
    IntrDummy,                  // DMA0 interrupt 
    IntrDummy,                  // DMA1 interrupt 
    IntrDummy,                  // DMA2 interrupt 
    IntrDummy,                  // DMA3 interrupt 
    IntrDummy,                  // Key interrupt 
    IntrDummy,                 // Cart interrupt 
};

u32      IntrMainBuf[0x140/4];                // Interrupt main routine buffer
IntrFuncp IntrTableBuf[14];
extern void intr_main(void);

int vbc=0;

void testMain()
{
    static IOSFd fd = -1;
    u16 vc1, vc2;
  
    if (fd < 0) {
        while ((fd = IOS_Open("/dev/lcechat", 0)) < 0) {
            ;
        }
    }

    while (1) {
        VBlankIntrWait();
        VBlankIntrWait();
        vbc = 0;
        vc1 = *(vu16*)REG_VCOUNT;
        IOS_Ioctlv(fd, 100, 0, 0, 0);
        vc2 = *(vu16*)REG_VCOUNT;
        printf("vc1: %03d vc2: %03d vbc:%d\n", vc1, vc2, vbc);
    }
}

void
AgbMain(void)
{
    *(vu16 *)REG_WAITCNT = CST_ROM0_1ST_4WAIT | CST_ROM0_2ND_2WAIT;
    DmaCopy(3, IntrTable, IntrTableBuf,sizeof(IntrTableBuf),32);// Sets interrupt table
    DmaCopy(3, intr_main, IntrMainBuf, sizeof(IntrMainBuf), 32);// Sets interrupt main routine
    *(vu32 *)INTR_VECTOR_BUF = (vu32 )IntrMainBuf;

    *(vu16 *)REG_IE   = V_BLANK_INTR_FLAG;              // Enable V-blank interrupt
    *(vu16 *)REG_STAT = STAT_V_BLANK_IF_ENABLE;
    *(vu16 *)REG_IME  = 1;                             // Sets IME 

    testMain();
}

void VBlankIntr(void)
{
    *(vu16 *)REG_IME = 0;
    *(vu16 *)INTR_CHECK_BUF |= V_BLANK_INTR_FLAG;   // Sets V-blank interrupt c heck 
    *(vu16 *)REG_IME = 1;
    
    vbc++;
}

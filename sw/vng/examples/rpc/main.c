//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

//  VNG Sanity Test
//   - this test does very simple VNG operation. 


#if defined(_SC)
#include "sc/ios.h"
#include "ioslibc.h"
#include "sc/apputils.h"
#include "sc/sc.h"
#define IOS_GetTimer()                         IO_READ(SYS_TIMER_REG)
#else
#include <stdio.h>
#include <stdlib.h>
#include <sys/timeb.h>
#endif

#if defined(_SC)
#define STACK_SIZE (128*1024)
const u8 _initStack[STACK_SIZE];
const u32 _initStackSize = sizeof (_initStack);
const u32 _initPriority = 9;
#endif

#ifdef _SC
#define MAIN int main ()
#endif

#ifdef _GBA
#define MAIN int AgbMain(void)
#endif

#ifdef _LINUX
#define MAIN int main (int argc, char **argv)
#endif

extern void doTests ();


#ifdef _LINUX

void *SM_Malloc(size_t size) {
  return malloc(size);
}

#else

// GBA or SC
char heap[64*1024] = { 1 };
void *SM_Malloc(size_t size)
{
    static int      sm_ptr;
    void           *ret;
    if (!sm_ptr)
	sm_ptr = (int)heap;

    ret = (void *) sm_ptr;
    sm_ptr += size;
    if (sm_ptr & 3)
	sm_ptr = (sm_ptr+0x4)&(0-4);
    return ret;
}

#endif


#ifdef _GBA

/*------------------------------------------------------------------*/
/*                      Interrupt Dummy Routine                     */
/*------------------------------------------------------------------*/

void
IntrDummy (void)
{
}

/*------------------------------------------------------------------*/
/*                      Interrupt Table                             */
/*------------------------------------------------------------------*/
typedef void (*IntrFuncp) (void);

const IntrFuncp IntrTable[13] = {
    IntrDummy,			// V Blank interrupt
    IntrDummy,			// H Blank interrupt
    IntrDummy,			// V Counter interrupt
    IntrDummy,			// Timer 0 interrupt
    IntrDummy,			// Timer 1 interrupt
    IntrDummy,			// Timer 2 interrupt
    IntrDummy,			// Timer 3 interrupt
    IntrDummy,			// Serial communication interrupt
    IntrDummy,			// DMA 0 interrupt
    IntrDummy,			// DMA 1 interrupt
    IntrDummy,			// DMA 2 interrupt
    IntrDummy,			// DMA 3 interrupt
    IntrDummy,			// Key interrupt
};
#endif



// Return time in ms since the first invocation
unsigned int getTimestamp()
{
#ifdef _SC
    return IOS_GetTimer()/500;
#else
#ifdef _GBA
    return 0; // GBA has no timer yet
#else
    static unsigned int earliest_time = 0;
    int t;
    struct timeb timebuffer;
    // use 'obsoleted' ftime to otbain ms resolution for Unix and Windows
    //  - gettimeofday is not available on Windows.
    ftime(&timebuffer);
    t = (unsigned int) (timebuffer.time * 1000 + timebuffer.millitm);
    if (earliest_time == 0) earliest_time = t;
    return t - earliest_time;
#endif
#endif
}

#ifdef _GBA
#define MEM0_START              0x08000000

void exit(int value) 
{
  while (1);
}
#endif


MAIN 
{
    printf("main() launched ...\n");
    doTests ();

#ifdef _GBA
    exit(0);  // don't return to crt0 - which will execute this program again
#endif

    return 0;
}

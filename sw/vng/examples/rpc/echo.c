//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "porting.h"
#include "vng.h"
#include "../include/testcfg.h"
#include "config.h"

#define MSG_SIZE 160

// Allocate VN data structure from heap
VNG      *vng;
VN       *vn;
VNMsgHdr *hdr;
int      *smsg;
int      *rmsg;
VNGGameInfo *info;

void
checkResult (int value, int expected, const char *name)
{
    if (value == expected) {
	printf ("  %s ok ...\n", name);
	return;			// OK
    }
    printf ("%s returns %d, expected %d\n", name, value, expected);
    printf ("vng sanity test end\n");
    printf ("VNG TEST FAILED\n");
}


#define MSG_SIZE 160

int32_t echo_rpc(VN *vn, VNMsgHdr *hdr, const void *args, size_t argsLen, void *ret, size_t *retLen)
{
    static int i;
    static int count;
    static int testcount;
    static unsigned int ts;

    if (!count) {
        ts = getTimestamp();
        count = 1;
        testcount = count;
    } else if (i == testcount) {
        ts = getTimestamp() - ts;
        printf("estimate RTT: %d roundtrips, total time %u ms, RTT %u ms\n",
	    testcount, ts, (ts+testcount-1)/testcount);
        ts = getTimestamp();
        count *= 2;
        testcount = count;
        i = 0;
        if ((testcount = count) == RTT_TEST_COUNT)
            --testcount;
    }
    ++i;

    *retLen = argsLen;
    return (memcpy(ret, args, argsLen) != NULL);
}

void
doEcho ()
{
    int rv;

    rv = VNG_Login (vng, VNGS_HOST, VNGS_PORT, TESTER01, TESTER01PW,
		    VNG_WAIT);
    checkResult (rv, VNG_OK, "VNG_Login");

    rv = VNG_NewVN (vng, vn, VN_DEFAULT_CLASS, VN_DOMAIN_INFRA,
		    VN_DEFAULT_POLICY);
    checkResult (rv, VNG_OK, "VNG_NewVN");

    // Describe the game information for matchmaking
    memset ((void *) info, 0, sizeof (VNGGameInfo));
    VN_GetVNId (vn, &info->vnId);
    info->owner = VNG_MyUserId (vng);
    info->gameId = GAMEID;
    info->titleId = 107;
    info->accessControl = VNG_GAME_PUBLIC;
    info->totalSlots = 2;
    info->buddySlots = 0;	// nothing reserved for buddies
    info->maxLatency = 500;	// 500ms RTT
    info->netZone = 0;		// server will decide this value
    info->attrCount = 0;
    memcpy(info->keyword, "x86 echo server!", sizeof("x86 echo server!")+1);

    // Register the VN to the directory for matchmaking
    rv = VNG_RegisterGame (vng, info, NULL, VNG_WAIT);
    checkResult (rv, VNG_OK, "VNG_RegisterGame");

    VN_RegisterRPCService(vn, 80, echo_rpc);

    while (1) {
        rv = VNG_ServeRPC(vng, VNG_WAIT);
        if (rv)
            printf("VNG_ServeRPC returns %d\n", rv);
    }

    rv = VNG_DeleteVN (vng, vn);
    checkResult (rv, VNG_OK, "VNG_DeleteVN");
}

void
doTests ()
{
    int rv;
    VNGEvent event;

    vng = (VNG*)SM_Malloc(sizeof(VNG));
    vn  = (VN*)SM_Malloc(sizeof(VN));
    hdr = (VNMsgHdr*)SM_Malloc(sizeof(VNMsgHdr));
    smsg = (int *) SM_Malloc(MSG_SIZE);
    rmsg = (int *) SM_Malloc(MSG_SIZE);
    info = (VNGGameInfo *) SM_Malloc(sizeof(VNGGameInfo));
    
    checkResult( vng != NULL && vn != NULL && hdr != NULL &&
		 info != NULL ? 1 : 0, 1, 
		 "Allocate Data Structure");


    printf ("vng echo test begin\n");
    rv = VNG_Init (vng, NULL);
    checkResult (rv, VNG_OK, "VNG_Init");

    doEcho ();

    /* dump events */
    while ((rv = VNG_GetEvent (vng, &event, VNG_NOWAIT)) == VNG_OK) {
	checkResult (rv, VNG_OK, "VNG_GetEvent - OK");
    }
    checkResult (rv, VNGERR_NOWAIT, "VNG_GetEvent - NOWAIT");

    rv = VNG_Fini (vng);
    checkResult (rv, VNG_OK, "VNG_Fini");

    printf ("vng echo test end\n");
    printf ("VNG TEST PASSED\n");
}

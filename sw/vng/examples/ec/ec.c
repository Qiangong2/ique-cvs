/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */

#include "es.h"


#include "porting.h"
#include "vng.h"
#include "vng_p.h"
#include "../include/testcfg.h"
#include "ec.h"


#ifdef _WIN32
#include <windows.h>
    #define msleep(msec) Sleep(msec)
    #define __attribute__(a)
#elif defined (_LINUX)
    #include <unistd.h>
    #define msleep(msec) usleep(msec*1000)
#else
    #define msleep(msec)
#endif

#if !defined(GBA) && !defined(_SC)

#undef  ES_SHA_ALIGN
#define ES_SHA_ALIGN

ESError  ES_GetDeviceCert(void* deviceCert)
{
    char fakeCert[] = "AFakeDeviceCert1";

    memcpy (deviceCert, fakeCert, sizeof(fakeCert));

    return ES_ERR_OK;
}

ESError  ES_InitLib(void)
{
    return ES_ERR_OK;
}

ESError  ES_CloseLib(void)
{
    return ES_ERR_OK;
}

ESError
ES_GetTmdSize(void* tmd, u32* size)
{
    ESError rv = ES_ERR_OK;

    if (tmd==NULL) {
        rv = ES_ERR_INVALID;
        goto out;
    }

    // this is just to allow running on windows/linux
    *size = 520;  // don't have ES except on NC or GBA

out:
    return rv;
}



#endif /* !defined(GBA) && !defined(_SC) */




#define NET_CONTENT_TIMEOUT   (60*1000)

bool trace = true;


VNG  __vng __attribute__((section(".data")));

u8  __contentBuf [1024*1024];




typedef enum {
    CANCEL_OP,
    LIST_TITLES,
    GET_TITLE_DETAILS,
    GET_META,
    UPDATE_STATUS,
    REDEEM,
    SYNC_TICKETS,
    LIST_SUBSCRIPTION_PRICINGS,
    SUBSCRIBE,
    PURCHASE_TITLE,
    DELETE_CACHED_CONTENT,
    DELETE_CACHED_CONTENT_NOT_THERE,
    CANCEL_CACHE_CONTENT,
    GET_NET_CONTENT,
    GET_TMD,
    GET_CETK,
    GET_CRL,
    GET_MET,
    GET_BOOT_TMD,
    GET_SYS_APP_TMD,
    DELETE_CACHED_CONTENT_THERE,
    SEEK,

} TestId;


typedef struct _TestInfo TestInfo;


typedef int (*TestFunc)(VNG *vng, TestInfo *test);

int listTitles (VNG *vng, TestInfo *test);
int getTitleDetails (VNG *vng, TestInfo *test);
int getMeta (VNG *vng, TestInfo *test);
int redeemECard (VNG *vng, TestInfo *test);
int updateStatus (VNG *vng, TestInfo *test);
int listSubPrice (VNG *vng, TestInfo *test);
int subscribe (VNG *vng, TestInfo *test);
int purchaseTitle (VNG *vng, TestInfo *test);
int deleteCachedContent (VNG *vng, TestInfo *test);
int getTMD (VNG *vng, TestInfo *test);
int getNetContent (VNG *vng, TestInfo *test);
int getNetContentSeek (VNG *vng, TestInfo *test);
int cancelOp (VNG *vng, TestInfo *test);
int cancelCacheContent (VNG *vng, TestInfo *test);
int syncTickets (VNG *vng, TestInfo *test);

struct _TestInfo {
    TestId            id;
    const char*       testname;
    TestFunc          func;
    ESTitleId         titleIds[2];
    ECContentId       cid;

};

#define NA 0

static TestInfo tests[] __attribute__((section(".data"))) = {
    {CANCEL_OP,
        "Start a list titles, then cancel it.", cancelOp,
        {NA}, {NA} },

    {LIST_TITLES,
        "List titles", listTitles,
        {NA}, {NA} },

    {GET_TITLE_DETAILS,
        "Get title details", getTitleDetails,
        {0x0002000100080033LL, 0x0002000100080031LL}, {NA} },

    {GET_META,
        "Get EC meta data from server", getMeta,
        {NA}, {NA} },

    {UPDATE_STATUS,
        "Update consumption", updateStatus,
        {0x0002000100080033LL, 0x0002000100080031LL}, {NA} },
         
    {REDEEM,
        "Redeem eCard", redeemECard,
        {NA}, {NA} },

    {SYNC_TICKETS,
        "Sync Tickets", syncTickets,
        {NA}, {NA} },

    {LIST_SUBSCRIPTION_PRICINGS,
        "List subscription pricings", listSubPrice,
        {NA},{NA} },

    {SUBSCRIBE,
        "Subscribe", subscribe,
        {NA}, {NA} },

    {PURCHASE_TITLE,
        "Purchase Title", purchaseTitle,
        {0x0002000100080033LL}, {NA} },

    {DELETE_CACHED_CONTENT,
        "Delete cached net content on PC", deleteCachedContent,
        {NA}, {0x0002000100080033LL, EC_CONTENT, 0} },

    {DELETE_CACHED_CONTENT_NOT_THERE,
        "Delete cached net content that is already deleted", deleteCachedContent,
        {NA}, {0x0002000100080033LL, EC_CONTENT, 0} },

    {CANCEL_CACHE_CONTENT,
        "Start to cache a content, then cancel it", cancelCacheContent,
        {NA}, {0x0002000100080033LL, EC_CONTENT, 0} },

    {GET_NET_CONTENT,
        "Get content from server", getNetContent,
        {NA}, {0x0002000100080033LL, EC_CONTENT, 0} },

    {GET_TMD,
        "Get tmd from server", getTMD,
        {NA}, {0x0002000100080033LL, EC_TMD, NA} },

    {GET_CETK,
        "Get boot common eticket from server", getNetContent,
        {NA}, {0x0000000200000001LL, EC_CETK, NA} },

    {GET_CRL,
        "Get crl from server", getNetContent,
        {NA}, {0x0000000200000001LL, EC_CRL, NA} },

    {GET_MET,
        "Get sys app met from server", getNetContent,
        {NA}, {0x0000000200000002LL, EC_MET, NA} },

    {GET_BOOT_TMD,
        "Get BOOT tmd from server", getNetContent,
        {NA}, {0x0000000200000001LL, EC_TMD, NA} },

    {GET_SYS_APP_TMD,
        "Get sys app tmd from server", getNetContent,
        {NA}, {0x0000000200000002LL, EC_TMD, NA} },

    {DELETE_CACHED_CONTENT_THERE,
        "Delete cached net content on PC", deleteCachedContent,
        {NA}, {0x0002000100080033LL, EC_CONTENT, 0} },

    {SEEK,
        "Get content from server in two pieces", getNetContentSeek,
        {NA}, {0x0002000100080033LL, EC_CONTENT, 0} },
};


void report(const char *testname, int pass)
{
    if (pass >= 0) 
        printf("*** EC %s TEST PASSED\n", testname);
    else
        printf("*** EC %s TEST FAILED\n", testname);
}

int dumpEvents(VNG *vng, VNGEventID  mustFindEvent)
{
    bool eventFound = false;

    // Wait for all events
    while (1) {
        VNGEvent event;
        VNGErrCode ec = VNG_GetEvent(vng, &event, VNG_NOWAIT);
        if (ec == VNG_OK) {
           printf("Got event %d", event.eid);
           if (event.eid == VNG_EVENT_PEER_STATUS) {
               VNId vid;
               VN_GetVNId(event.evt.peerStatus.vn, &vid);
               printf(" -- 0x%08x.%08x.%08x %d",  (u32)(vid.deviceId >> 32), (u32)vid.deviceId, vid.netId, event.evt.peerStatus.memb);
           }    
           printf("\n");
           if (event.eid == mustFindEvent) {
               eventFound = true;
           }
        } else if (ec == VNGERR_NOWAIT || ec == VNGERR_TIMEOUT)
           break;
    }

    if (mustFindEvent && !eventFound) {
        printf("Didn't find event %d\n", mustFindEvent);
        return -1;
    }

    return 0;
}

char *contentTypeStrs[] = {"ec_content", "tmd", "met", "crl", "cetk"};

int reportContentInfo (ECContentInfo *content)
{
    char *type =  "<unknown>";
    if (content->id.contentType <
            (sizeof contentTypeStrs/sizeof contentTypeStrs[0])) {
        type = contentTypeStrs[content->id.contentType];
    }
    printf ("    titleId %08xx%08x (%u.%u)  type %s  cid 0x%08x (%u)  size %d\n",
        (u32)(content->id.titleId >> 32), (u32)(content->id.titleId),
        (u32)(content->id.titleId >> 32), (u32)(content->id.titleId),
        type, content->id.contentId, content->id.contentId, content->size);
    if (content->id.contentType != EC_CONTENT) {
        printf ("found unexpected content type %d\n",
                    content->id.contentType);
        return -1;
    }
    return 0;
}




char *vngErrMsg (VNGErrCode ec)
{
    static char vemBuf[256];
    VNGErrCode vemEc;

    if ((vemEc = VNG_ErrMsg (ec, vemBuf, sizeof vemBuf))) {
        snprintf(vemBuf, sizeof vemBuf, "VNG_ErrMsg(%d, vemBuf, sizeof vemBuf) returned %d", ec, vemEc);
    }    
    return vemBuf;
}


/* login makes no difference */
static VNDomain domain = VN_DOMAIN_INFRA;

static int isDoFini = false;
static int isDontInit = false;
static int isSetTrace = false;
static int isLoggedIn = true;  // don't login
static int isInitEC = true;
static int isFiniEC = true;


int testECommerce (TestInfo *test)
{
    VNGErrCode ec;
    ECError    rv;
    int res = 0;
    VNG  *vng = &__vng;
    VNGInitParm  initParm;

    printf("\n*** LIBVNG %s TEST START\n", test->testname);

    if (!isDontInit) {
        initParm.vnPortMin = 4000;
        initParm.vnPortMax = 4300;

        if ((ec = VNG_Init(vng, &initParm))) {
            printf ("VNG_Init returned %d\n", ec);
            res = -1;
            goto fini;
        }
        printf ("VNG_Init successful\n");
        isDontInit = true;
    }


    if (isSetTrace) {

        VNGTraceOpt trop = {
            VNG_TROP_GLOBAL_ENABLE |
            VNG_TROP_GLOBAL_LEVEL  |
            VNG_TROP_GRP_LEVEL,
            { true, 3, {0} },
                { 0, 0, 4, 0, 0, { 0, 0 } }
        };

        printf ("Enable trace, set trace_level INFO, VNG trace_level FINE\n");

        if ((ec = VNG_SetTraceOpt (vng, trop))) {
            printf ("VNG_SetTraceOpt returned %d\n", ec);
            res = -1;
            goto fini;
        }
    }

    if (domain == VN_DOMAIN_INFRA && !isLoggedIn) {
        VNGTimeOfDay  serverTime;

        ec = VNG_Login(vng, VNGS_HOST, VNGS_PORT,
                       TESTER01, TESTER01PW,
                       VNG_TIMEOUT);

        if (trace) {
            printf("VNG_Login returns %d\n", ec);
        }
        if (ec != VNG_OK) {
            printf("VNG_Login returns %d\n", ec);
            res = -1;
            goto fini;
        }
        isLoggedIn = true;
        if ((ec = VNG_GetServerTime (vng, &serverTime, VNG_TIMEOUT))) {
            printf("VNG_GetServerTime returned %d\n", ec);
        } else {
            printf("Server time is 0x%08x.%08x ms  %u sec\n",
                    (u32)(serverTime >> 32), (u32)(serverTime),
                    (u32)(serverTime/1000));
        }
    }

    if (isInitEC) {
        if ((rv = EC_Init())) {
            printf("EC_Init() returns %d\n", rv);
        } else {
            printf("EC_Init() success\n");
        }
    }

    res = test->func (vng, test);

    if (res) {
        printf("%s returns %d\n", test->testname, res);
        goto fini;
    }

    res = dumpEvents(vng, (VNGEventID)0);
     
    if (isFiniEC) {
        if ((rv = EC_Fini())) {
            printf("EC_Fini() returns %d\n", rv);
        } else {
            printf("EC_Fini() success\n");
        }
    }

fini:
    if (isDoFini) {
        printf("Entering VNG fini\n");
        VNG_Fini(vng);
    }

    return res;
}



int getTMD (VNG *vng, TestInfo *test)
{
    int res;
    ESError rv;
    u32 tmdSize;
    u8 *tmd = __contentBuf;
    u8 *tmdCerts;

    if (!(res = getNetContent (vng, test))) {
        printf("getTMD: getNetContent returned %d\n", res);
    }

    if ((rv = ES_GetTmdSize ((void*) tmd, &tmdSize))) {
        res = -1;
    }
    else {
        tmdCerts = tmd + tmdSize;
    }

    return res;
}



typedef enum { T_DONE, T_CACHE, T_OPEN, T_SEEK, T_READ, T_CLOSE } TestState;

typedef enum { TV_NORMAL, TV_SEEK_1, TV_SEEK_2,
               TV_CANCEL_CONTENT_CACHE_1, TV_CANCEL_CONTENT_CACHE_2

             } TestVariation;

int getNetContentTest (VNG *vng, TestInfo *test, TestVariation *test_variation);


int getNetContent (VNG *vng, TestInfo *test)
{
    TestVariation test_variation = TV_NORMAL;

    if (test->cid.contentType != EC_CONTENT) {
        /* always want to get special files from ccs */
        deleteCachedContent(vng, test);
    }

    return getNetContentTest (vng, test, &test_variation);
}

int getNetContentSeek (VNG *vng, TestInfo *test)
{
    int res;
    TestVariation test_variation = TV_SEEK_1;

    if ((res = getNetContentTest (vng, test, &test_variation))) {
        printf("getNetContentSeek: getNetContentTest returned %d\n", res);
        return res;
    }
    test_variation = TV_SEEK_2;
    return getNetContentTest (vng, test, &test_variation);
}

int cancelCacheContent (VNG *vng, TestInfo *test)
{
    int res;
    TestVariation test_variation = TV_CANCEL_CONTENT_CACHE_1;

    if (!(res = getNetContentTest (vng, test, &test_variation))) {
        printf("cancelCacheContent: getNetContentTest returned %d\n", res);
    }
    if (test_variation != TV_CANCEL_CONTENT_CACHE_2) {
        printf("cancelCacheContent: expected test_variation == "
            "TV_CANCEL_CONTENT_CACHE_2 but got %d\n", test_variation);
    }
    return res;
}




int getNetContentTest (VNG *vng, TestInfo *test, TestVariation *test_variation)
{
    static ECCacheStatus cacheStatus __attribute__((section(".data")));
    int           res = 0;
    ECError       ece;
    s32           n;
    static u8     buf [64*1024] __attribute__((section(".data")));
    TestState     state = T_CACHE;
    bool          cacheStarted = false;
    int           total  = 0;
    static int    seek_1_amount;
    int           pfiltSkip = 70;
    int           pfilt  = pfiltSkip;

    do switch (state) {
        case T_CACHE:
            ece = EC_GetCachedContentStatus (&test->cid, 1, &cacheStatus);
            if (0>ece) {
                printf("getNetContent:EC_GetCachedContentStatus returned %d\n", ece);
                res = -1;
                state = T_DONE;
            } else if (cacheStatus.cachedSize == EC_ERROR_NOT_CACHED ) {
                if (cacheStarted) {
                    printf("getNetContent: cache started, "
                      "but EC_GetCachedContentStatus says  EC_ERROR_NOT_CACHED\n");
                    if (*test_variation == TV_CANCEL_CONTENT_CACHE_2) {
                        printf("But thats OK because cache content was canceled\n");
                    } else {
                        res = -1;
                    }
                    state = T_DONE;
                } else {
                    pfilt = pfiltSkip;
                    ece = EC_CacheContent (&test->cid, 1);
                    if (0>ece) {
                        printf("getNetContent:EC_CacheContent returned %d\n", ece);
                        res = -1;
                        state = T_DONE;
                    } else {
                        cacheStarted = true;
                    }
               }
            } else if (cacheStatus.cachedSize < 0) {
                printf("getNetContent:EC_GetCachedContentStatus returned cachedSize %d\n",
                        cacheStatus.cachedSize);
                res = cacheStatus.cachedSize;
                state = T_DONE;
            } else if (cacheStatus.cachedSize == cacheStatus.totalSize) {
                if (cacheStatus.totalSize == 0) {
                    state = T_DONE;  /* zero size file */
                } else {
                    state = T_OPEN;
                }
                printf("getNetContent:EC_GetCachedContentStatus says "
                        "%d of %d bytes are cached\n",
                        cacheStatus.cachedSize, cacheStatus.totalSize);

            } else {
                /* file is being downloaded to PC from net */
                if (++pfilt >= pfiltSkip) {
                    pfilt = 0;
                    if (cacheStatus.totalSize < 0) {
                        printf("getNetContent:EC_GetCachedContentStatus says "
                                "%d of <unknown> bytes are cached\n",
                                cacheStatus.cachedSize);
                    } else {
                        printf("getNetContent:EC_GetCachedContentStatus says "
                                "%d of %d bytes are cached\n",
                                cacheStatus.cachedSize, cacheStatus.totalSize);
                    }
                    if (*test_variation == TV_CANCEL_CONTENT_CACHE_1
                        && cacheStatus.cachedSize > 2000000) {
                        ece = EC_CancelCacheContent(&test->cid, 1);
                        if (ece != EC_ERROR_OK) {
                            printf("getNetContent:EC_CancelCacheContent returned %d\n",
                                    ece);
                            res = -1;
                            state = T_DONE;
                        } else {
                            printf("getNetContent:EC_CancelCacheContent with size %d\n",
                                    cacheStatus.cachedSize);
                        }
                        *test_variation = TV_CANCEL_CONTENT_CACHE_2;
                    }
                    #ifdef _WIN32
                        fflush(stdout);
                    #endif
                }
            }
            break;

        case T_OPEN:
            total = 0;
            pfilt = pfiltSkip;
            ece = EC_OpenCachedContent (test->cid);
            if (0>ece) {
                printf("getNetContent:EC_OpenCachedContent returned %d\n", ece);
                res = -1;
                state = T_DONE;
            } else if (*test_variation == TV_SEEK_2) {
                state = T_SEEK;
                total = seek_1_amount;
            } else {
                state = T_READ;
            }
            break;

        case T_SEEK:
            ece = EC_SeekCachedContent (test->cid, total);
            if (0>ece) {
                printf("getNetContent:EC_SeekCachedContent returned %d\n", ece);
                res = -1;
                state = T_DONE;
            } else {
                state = T_READ;
            }
            break;



        case T_READ:
            n = EC_ReadCachedContent (test->cid, buf, sizeof buf);
            if (0>n) {
                printf("getNetContent:EC_ReadCachedContent returned %d\n", n);
                res = -1;
                state = T_CLOSE;
            } else if (n == 0) {
                state = T_CLOSE;
                printf("EC_ReadCachedContent read %d total bytes\n", total);
            } else {
                /* write buf to file */
                total += n;
                if (++pfilt >= pfiltSkip) {
                    pfilt = 0;
                    printf("EC_ReadCachedContent "
                           "read %d bytes this time, %d so far\n", n, total);
                    if (*test_variation == TV_SEEK_1 && total > 2000000) {
                        seek_1_amount = total;
                        state = T_CLOSE;
                        printf("Closing content before seek and read the rest\n");
                    }

                    #ifdef _WIN32
                        fflush(stdout);
                    #endif

                }
            }
            break;

        case T_CLOSE:
            ece = EC_CloseCachedContent (test->cid);
            if (0>ece) {
                printf("getNetContent:EC_CloseCachedContent returned %d\n", ece);
                res = -1;
                state = T_DONE;
            } else {
                state = T_DONE;
            }
            break;

        case T_DONE:
            break;

        default:
            printf("invalid state %d\n", state);
            state = T_DONE;
            res = -1;
            break;

    }  while (state != T_DONE);

    return res;
}



int deleteCachedContent (VNG *vng, TestInfo *test)
{
    ECError rv;
    ECError ece;
    static ECCacheStatus cacheStatus __attribute__((section(".data")));

    rv = EC_DeleteCachedContent (test->cid);

    if (0>rv) {
        printf ("EC_DeleteCachedContent returned %d\n", rv);
    }
    else {
        printf ("EC_DeleteCachedContent returned success, checking if still cached\n");
        ece = EC_GetCachedContentStatus (&test->cid, 1, &cacheStatus);
        if (0>ece) {
            printf("deleteCachedContent:EC_GetCachedContentStatus returned %d\n", ece);
            rv = EC_ERROR_FAIL;
        } else if (cacheStatus.cachedSize != EC_ERROR_NOT_CACHED ) {
            printf("deleteCachedContent expeceted EC_GetCachedContentStatus to return "
                   "EC_ERROR_NOT_CACHED but it returned cacheSize %d\n", cacheStatus.cachedSize);
            rv = EC_ERROR_FAIL;
        } else {
            printf("deleteCachedContent EC_GetCachedContentStatus got expected "
                        "EC_ERROR_NOT_CACHED\n");
        }
    }

    if (rv == EC_ERROR_OK) {
        printf ("EC_DeleteCachedContent success.\n");
    }
    return rv;
}



static ECListTitlesInfo titles[200] __attribute__((section(".data")));

int listTitles (VNG *vng, TestInfo *test)
{
    ECError rv;
    u32 nTitles = sizeof titles / sizeof titles[0];
    u32 i;

    rv = EC_ListTitles(EC_TITLE_KIND_GAME, EC_PK_SUBSCRIPTION, titles, &nTitles);
    while (rv) {
        if (rv == EC_ERROR_NOT_DONE) {
            msleep(250);
        }
        else {
            printf ("EC_ListTitles returned %d\n", rv);
            break;
        }
        rv = EC_GetReturnValue();
    };

    if (rv == EC_ERROR_OK) {
        printf ("EC_ListTitles success %d titles found\n", nTitles);
        for (i = 0;  i < nTitles;  ++i) {
            printf ("ListTitles[%u]  titleId %08xx%08x (%u.%u)  fsSize %u.%u  name %s\n",
                     i,
                    (u32)(titles[i].id >> 32), (u32)(titles[i].id),
                    (u32)(titles[i].id >> 32), (u32)(titles[i].id),
                    (u32)(titles[i].fsSize >> 32), (u32)(titles[i].fsSize),
                    titles[i].name);
        }
    }
    return rv;
}




int getTitleDetails (VNG *vng, TestInfo *test)
{
    ECError rv;
    static ECTitleDetails d __attribute__((section(".data")));
    ESTitleId  titleId;
    u32 nTitles = sizeof test->titleIds / sizeof test->titleIds[0];
    u32 n, i, k;

    for (n = 0;  n < nTitles;  ++n) {
        titleId = test->titleIds[n];
        rv = EC_GetTitleDetails(titleId, &d);
        while (rv) {
            if (rv == EC_ERROR_NOT_DONE) {
                msleep(1000);
            }
            else {
                printf ("EC_GetTitleDetails returned %d\n", rv);
                break;
            }
            rv = EC_GetReturnValue();
        };

        if (rv == EC_ERROR_OK) {
            // gba printf doesn't seem to work with 64 bit format
            printf ("EC_GetTitleDetails success\n");
            printf ("titleName %s\n", d.info.titleName);
            printf ("description %s\n", d.info.description);
            printf ("category %s\n", d.info.category);
            printf ("titleId %08xx%08x (%u.%u)  titleKind %d fsSize %u.%u\n",
                    (u32)(d.info.titleId >> 32), (u32)(d.info.titleId),
                    (u32)(d.info.titleId >> 32), (u32)(d.info.titleId),
                        d.info.titleKind,
                    (u32)(d.info.fsSize >> 32), (u32)(d.info.fsSize));
            printf ("version %u\n",  d.info.version);

            printf ("%d contents:\n", d.info.nContents);
            for (i = 0;  i < d.info.nContents;  ++i) {
                if ((rv =reportContentInfo (&d.info.contents[i]))) {
                    goto end;
                }
                printf ("        cacheSize %d totalSize %d\n",
                            d.info.cacheStatus[i].cachedSize,
                            d.info.cacheStatus[i].totalSize);
            }

            printf ("%d ratings: ", d.nRatings);
            for (i = 0;  i < d.nRatings;  ++i) {
                printf ("Rating name %s, Rating %s\n",
                    d.ratings[i].name, d.ratings[i].rating);
            }

            printf ("%d pricings:\n", d.nPricings);
            for (i = 0;  i < d.nPricings;  ++i) {
                printf ("itemId %d  category %d  amount %d  "
                                   "currency %s  nLimits %u\n",
                        d.pricings[i].itemId,
                        d.pricings[i].pricingCategory,
                        d.pricings[i].price.amount,
                        d.pricings[i].price.currency,
                        d.pricings[i].nLimits);
                for (k =0; k < d.pricings[i].nLimits; ++k) {
                    printf ("limits[%u]  code %u  limit %u\n",
                             k,  d.pricings[i].limits[k].code,
                                 d.pricings[i].limits[k].limit);
                }
            }
        }
    }
end:
    return rv;
}

int getMeta (VNG *vng, TestInfo *test)
{
    ECError rv;
    static ECMeta m __attribute__((section(".data")));

    rv = EC_GetECMeta (&m);
    while (rv) {
        if (rv == EC_ERROR_NOT_DONE) {
            msleep(1000);
        }
        else {
            printf ("EC_GetECMeta returned %d\n", rv);
            break;
        }
        rv = EC_GetReturnValue();
    };

    if (rv == EC_ERROR_OK) {
        printf ("EC_GetECMeta success\n");

        printf ("boot program:\n");
        printf ("    version %u\n", m.bootVersion);

        printf ("sys_app:\n");
        printf ("    version %u\n", m.sys_appVersion);

    }

    return rv;
}



int redeemECard (VNG *vng, TestInfo *test)
{
    ECError     rv;
    ESError     ese;
    char       *eCard = "00010300100284380006229818";
    s32         ticketSize;
    u8         *ticket;
    static u8   certs[4096]  __attribute__((section(".data")));
    u32         certsSize = sizeof certs;
    static u8   deviceCert[ES_DEVICE_CERT_SIZE] ES_SHA_ALIGN __attribute__((section(".data")));

    ticketSize = EC_GetETicketSize();
    ticket = SM_Malloc(ticketSize); 
    if (!ticket) {
        printf ("redeemECard Failed: NO MEM\n");
        return -1;
    }

    if ((ese = ES_GetDeviceCert(deviceCert))) {
        printf ("ES_GetDeviceCert returned %d\n", ese);
        return -1;
    } else {
        printf ("ES_GetDeviceCert success\n");
    }

    printf ("Redeeming ECard %s\n", eCard);

    rv = EC_RedeemECard (eCard,
                         deviceCert,
                         ticket,
                         certs,
                        &certsSize);

    while (rv) {
        if (rv == EC_ERROR_NOT_DONE) {
            msleep(1000);
        }
        else {
            printf ("EC_RedeemECard returned %d\n", rv);
            break;
        }
        rv = EC_GetReturnValue();
    };

    if (rv == EC_ERROR_OK) {
        printf ("EC_RedeemECard success.  certsSize %d\n", certsSize);
    }
    printf ("ticketSize %d\n", ticketSize);
    return rv;
}


int updateStatus (VNG *vng, TestInfo *test)
{
    ECError rv;
    ECConsumption consumptions[2];
    u32           nConsumptions = sizeof consumptions / sizeof consumptions[0];
    u32           i;
    s32           consumption = 5;


    for (i = 0;  i < nConsumptions; ++i) {
        ECConsumption *c = &consumptions[i];
        c->consumption = ++consumption;
        c->titleId = test->titleIds[i];
        printf ("Sending consumption:  titleId %08xx%08x (%u.%u)  consumption %d\n",
                (u32)(c->titleId >> 32), (u32)(c->titleId),
                (u32)(c->titleId >> 32), (u32)(c->titleId),
                consumption);
    }
    
    rv = EC_UpdateStatus (consumptions, nConsumptions);

    while (rv) {
        if (rv == EC_ERROR_NOT_DONE) {
            msleep(1000);
        }
        else {
            printf ("EC_UpdateStatus returned %d\n", rv);
            break;
        }
        rv = EC_GetReturnValue();
    };

    if (rv == EC_ERROR_OK) {
        printf ("EC_UpdateStatus success.\n");
    }
    return rv;
}


int cancelOp (VNG *vng, TestInfo *test)
{
    ECError  rv;
    u32 nTitles = sizeof titles / sizeof titles[0];

    rv = EC_ListTitles(EC_TITLE_KIND_GAME, EC_PK_SUBSCRIPTION, titles, &nTitles);
    if (rv == EC_ERROR_NOT_DONE) {
        rv = EC_CancelOperation ();
        if (rv != EC_ERROR_OK) {
            printf ("EC_CancelOperation expeced EC_ERROR_OK, got %d\n", rv);
            return rv;
        }
    }
    else {
        printf ("cancelOp EC_ListTitles returned %d\n", rv);
        return -1;
    }
    rv = EC_GetReturnValue();

    if (rv != EC_ERROR_NOT_BUSY) {
        printf ("cancelOp EC_GetReturnValue expeced "
                "EC_ERROR_NOT_BUSY, got %d\n", rv);
        return rv;
    }

    rv = listTitles (vng, &tests[LIST_TITLES]);
                       
    if (rv == EC_ERROR_OK) {
        printf ("EC_CancelOperation() success.\n");
    } else {
        printf ("EC_CancelOperation() listTitles returns %d\n", rv);
    }

    return rv;
}




int syncTickets (VNG *vng, TestInfo *test)
{
    ECError     rv;
    ESError     ese;
    s32         ticketsSize;
    u8         *tickets;
    u32         nTickets = 10;
    static u8   certs[16000]  __attribute__((section(".data")));
    u32         certsSize = sizeof certs;
    static u8   deviceCert[ES_DEVICE_CERT_SIZE] ES_SHA_ALIGN __attribute__((section(".data")));

    ticketsSize = EC_GetETicketSize();
    tickets = SM_Malloc(nTickets * ticketsSize);
    if (!tickets) {
        printf ("syncTickets Failed: NO MEM\n");
        return -1;
    }

    memset (tickets, 0, ticketsSize);

    if ((ese = ES_GetDeviceCert(deviceCert))) {
        printf ("ES_GetDeviceCert returned %d\n", ese);
        return -1;
    } else {
        printf ("ES_GetDeviceCert success\n");
    }

    rv = EC_SyncTickets (deviceCert,
                         tickets,
                         &nTickets,
                         certs,
                         &certsSize);

    while (rv) {
        if (rv == EC_ERROR_NOT_DONE) {
            msleep(1000);
        }
        else {
            printf ("EC_SyncTickets returned %d\n", rv);
            break;
        }
        rv = EC_GetReturnValue();
    };

    if (rv == ECS_STATUS_TICKETS_ALREADY_IN_SYNC) {
        printf ("EC_SyncTickets success. Tickets already in Sync\n");
        rv = EC_ERROR_OK;
    }
    else if (rv == EC_ERROR_OK) {
        printf ("EC_SyncTickets success.  nTickets %u  certsSize %u\n", nTickets, certsSize);
    }
    return rv;
}


int listSubPrice (VNG *vng, TestInfo *test)
{
    static ECSubscriptionPricing  pricings[10] __attribute__((section(".data")));
    ECError  rv;
    u32      nPricings = sizeof pricings / sizeof pricings[0];
    u32      i;
    
    rv = EC_ListSubscriptionPricings (pricings, &nPricings);
                       
    while (rv) {
        if (rv == EC_ERROR_NOT_DONE) {
            msleep(1000);
        }
        else {
            printf ("EC_ListSubscriptionPricings returned %d\n", rv);
            break;
        }
        rv = EC_GetReturnValue();
    };

    if (rv == EC_ERROR_OK) {
        printf ("EC_ListSubscriptionPricings success.  nPricings %d\n", nPricings);
    }

    for (i = 0; i < nPricings; ++i) {
        ECSubscriptionPricing  *p = &pricings[i];
        printf ("Pricings[%d]:\n  itemId %d\n  channelId 0x%08x\n"
                "  channelName %s\n  description %s\n  subscriptionLength %d\n"
                "  subscriptionTimeUnit %d\n  price.Amount %d\n  price.currency %s\n"
                "  maxCheckouts %d\n",
                i,
                p->itemId,
                p->channelId,
                p->channelName,
                p->channelDescription,
                p->subscriptionLength,
                p->subscriptionTimeUnit,
                p->price.amount,
                p->price.currency,
                p->maxCheckouts);
                
    }
    return rv;
}


int subscribe (VNG *vng, TestInfo *test)
{
    ECError            rv;
    ESError            ese;
    s32                itemId = 5;
    s32                channelId = 0x00020001;
    ECPrice            price = { 400, "POINTS" };
    char              *eCard = "00010300100284390025246738";
    s32                subscriptionLength = 60;
    ECTimeUnit         subscriptionTimeUnit = EC_DAY;
    u8                *ticket;
    static u8   certs[16000]  __attribute__((section(".data")));
    u32         certsSize = sizeof certs;
    static u8   deviceCert[ES_DEVICE_CERT_SIZE] ES_SHA_ALIGN __attribute__((section(".data")));

    ticket = SM_Malloc(EC_GetETicketSize()); 

    if ((ese = ES_GetDeviceCert(deviceCert))) {
        printf ("ES_GetDeviceCert returned %d\n", ese);
        return -1;
    } else {
        printf ("ES_GetDeviceCert success\n");
    }

    printf ("About to Subscribe %s\n", eCard);

    rv = EC_Subscribe (itemId,
                       channelId,
                       price,
                       eCard,
                       subscriptionLength,
                       subscriptionTimeUnit,
                       deviceCert,
                       ticket,
                       certs,
                       &certsSize);
                       
    while (rv) {
        if (rv == EC_ERROR_NOT_DONE) {
            msleep (1000);
        }
        else {
            printf ("EC_Subscribe returned %d\n", rv);
            break;
        }
        rv = EC_GetReturnValue();
    };

    if (rv == EC_ERROR_OK) {
        printf ("EC_Subscribe success.  certsSize %d\n", certsSize);
    }
    return rv;
}


int purchaseTitle (VNG *vng, TestInfo *test)
{
    ECError       rv = EC_ERROR_OK;
    ESError       ese;
    ESTitleId     titleId;
    s32           itemId;
    ECPrice       price;
    ECPayment     payment;
    ESLpEntry     limits[ES_MAX_LIMIT_TYPE];
    u32           nLimits = 0;
    static char   points[] = "POINTS";
    static char   eunits[] = "EUNITS";
    static char   eCard[] = "00010100100059180060070904";
    u8           *ticket;
    static u8     certs[16000]  __attribute__((section(".data")));
    u32           certsSize = sizeof certs;
    static u8     deviceCert[ES_DEVICE_CERT_SIZE] ES_SHA_ALIGN __attribute__((section(".data")));

    if (true) {
        // titleId = 0x0002000002c6bc00LL;
        titleId = 0x0002000100080021LL;
        itemId = 12;
        price.amount = 0;
        strncpy (price.currency, points, sizeof price.currency);
    }
    else {
        titleId = 0x0002000100080033LL;
        itemId = 141;
        price.amount = 0;
        strncpy (price.currency, points, sizeof price.currency);
    }

    payment.method = EC_PaymentMethod_ECard;
    strncpy (payment.info.eCard.number, eCard,
             sizeof payment.info.eCard.number);

    ticket = SM_Malloc(EC_GetETicketSize()); 

    if ((ese = ES_GetDeviceCert(deviceCert))) {
        printf ("ES_GetDeviceCert returned %d\n", ese);
        return -1;
    } else {
        printf ("ES_GetDeviceCert success\n");
    }

    printf ("Purchase title %08x%08x\n",
             (u32)(titleId >> 32), (u32)(titleId));


    rv = EC_PurchaseTitle (titleId,
                           itemId,
                           price,
                           &payment,
                           limits,
                           nLimits,
                           deviceCert,
                           ticket,
                           certs,
                           &certsSize);
                       
    while (rv) {
        if (rv == EC_ERROR_NOT_DONE) {
            msleep(1000);
        }
        else {
            printf ("EC_PurchaseTitle returned %d\n", rv);
            break;
        }
        rv = EC_GetReturnValue();
    };

    if (rv == EC_ERROR_OK) {
        printf ("EC_PurchaseTitle success.  certsSize %d\n", certsSize);
    }

    return rv;
}




void doTests()
{
    int res;

    TestInfo*  test;
    unsigned   i, firstTest;
    ESError    ese;

    unsigned  numTests  = (sizeof tests / sizeof tests[0]);

    printf("VNG ECommerce Test\n");
    printf("------------------\n");

    if ((ese = ES_InitLib())) {
        printf ("ES_InitLib Failed %d\n", ese);
    } else {
        printf ("ES_InitLib Success\n");
    }

    firstTest = 0;
    for (i = firstTest;  i < numTests;   ++i) {
        test = &tests[i];
        if ( (i + 1) == numTests) {
            isDoFini = true;
        }
        res = testECommerce(test);
        report(test->testname, res);
    }

    if (!ese) {
        if ((ese = ES_CloseLib())) {
            printf ("ES_CloseLib Failed %d\n", ese);
        } else {
            printf ("ES_CloseLib Success\n");
        }
    }

}


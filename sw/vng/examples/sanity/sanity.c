//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

//  VNG Sanity Test
//   - this test does very simple VNG operation. 


#include "porting.h"
#include "vng.h"

#define MSG_SIZE 160

// Allocate VN data structure from heap
VNG      *vng;
VN       *vn;
VNMsgHdr *hdr;
int      *smsg;
int      *rmsg;

void
checkResult (int value, int expected, const char *name)
{
    if (value == expected) {
	printf ("  %s ok ...\n", name);
	return;			// OK
    }
    printf ("%s returns %d, expected %d\n", name, value, expected);
    printf ("vng sanity test end\n");
    printf ("VNG TEST FAILED\n");
    exit(0);
}


void
doSanityTest ()
{
    int rv;
    int i, j, m, n;
    size_t msglen;
    int attr = VN_DEFAULT_ATTR;
    int errcount = 0;

    rv = VNG_NewVN (vng, vn, VN_DEFAULT_CLASS, VN_DOMAIN_LOCAL,
		    VN_DEFAULT_POLICY);
    checkResult (rv, VNG_OK, "VNG_NewVN");

    rv = VN_SendMsg (vn, VN_MEMBER_SELF, 2034, smsg, MSG_SIZE, attr,
		     VNG_WAIT);
    checkResult (rv, VNG_OK, "VN_SendMsg");
    rv = VN_RecvMsg (vn, VN_MEMBER_ANY, VN_SERVICE_TAG_ANY, rmsg, &msglen,
		     hdr, 100);
    checkResult (rv, VNG_OK, "VN_RecvMsg");

    m = 4;
    n = 4;
    for (i = 0; i < m; i++) {
	for (j = 0; j < n; j++) {
	    smsg[0] = i;
	    smsg[1] = j;
	    rv = VN_SendMsg (vn, VN_MEMBER_SELF, 2034, smsg, MSG_SIZE, attr,
			     VNG_WAIT);
	    if (rv != VNG_OK)
		errcount++;
	}
	for (j = 0; j < n; j++) {
	    msglen = MSG_SIZE;
	    rv = VN_RecvMsg (vn, VN_MEMBER_ANY, VN_SERVICE_TAG_ANY, rmsg,
			     &msglen, hdr, 100);
	    if (rv != VNG_OK)
		errcount++;
	}
    }
    checkResult (errcount, 0, "Send/Recv Sequences");

    rv = VNG_DeleteVN (vng, vn);
    checkResult (rv, VNG_OK, "VNG_DeleteVN");
}

void
doTests ()
{
    int rv;
    VNGEvent event;

    vng = (VNG*)SM_Malloc(sizeof(VNG));
    vn  = (VN*)SM_Malloc(sizeof(VN));
    hdr = (VNMsgHdr*)SM_Malloc(sizeof(VNMsgHdr));
    smsg = (int *) SM_Malloc(MSG_SIZE);
    rmsg = (int *) SM_Malloc(MSG_SIZE);
    
    checkResult( vng != NULL && vn != NULL && hdr != NULL ? 1 : 0, 1, 
		 "Allocate Data Structure");

    printf ("vng sanity test begin\n");
    rv = VNG_Init (vng, NULL);
    checkResult (rv, VNG_OK, "VNG_Init");

    doSanityTest ();

    /* dump events */
    while ((rv = VNG_GetEvent (vng, &event, VNG_NOWAIT)) == VNG_OK) {
	checkResult (rv, VNG_OK, "VNG_GetEvent - OK");
    }
    checkResult (rv, VNGERR_NOWAIT, "VNG_GetEvent - NOWAIT");

    rv = VNG_Fini (vng);
    checkResult (rv, VNG_OK, "VNG_Fini");

    printf ("vng sanity test end\n");
    printf ("VNG TEST PASSED\n");
}



#ifdef _WIN32
    #define snprintf _snprintf
    #if _MSC_VER >= 1400
        #if defined(__cplusplus) && !defined(_CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES)
            #define _CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES 1
            #define _CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES_COUNT 1
        #elif !defined(_CRT_SECURE_NO_DEPRECATE)
            #define _CRT_SECURE_NO_DEPRECATE 1
        #endif
    #endif
#endif


#if defined(_SC)
#include "sc/ios.h"
#include "ioslibc.h"
#elif defined(_GBA)
#include "gba/libc.h"
#else
#include <stdio.h>
#include <stdlib.h>
#endif

extern void *SM_Malloc(size_t size);
extern unsigned int getTimestamp();

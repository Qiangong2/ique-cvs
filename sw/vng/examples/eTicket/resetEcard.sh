#!/bin/sh

if [ "$1" = "lab1" ]; then
    CONFIG=LAB1
elif [ "$1" = "beta" ]; then
    CONFIG=BETA
else
    echo "Usage: resetEcard [lab1|beta]"
    exit
fi

scriptfile=/tmp/opennms.cmd.$$

sed -e "s/CONFIG/$CONFIG/" > $scriptfile << EOF
cmd -s CONFIG-BCCDB "update pas.ecards set last_used=null, balance='31', balance_authorized='0' where ecard_id between 10000000 and 10000009"
cmd -s CONFIG-BCCDB "update pas.ecards set last_used=null, balance='92', balance_authorized='0' where ecard_id between 10010000 and 10010009"
cmd -s CONFIG-BCCDB "update pas.ecards set last_used=null, balance='365', balance_authorized='0' where ecard_id between 10020000 and 10020009"
EOF

echo "Prepare to execute ..."
cat $scriptfile
ssh oracle@opennms < $scriptfile
rm -fr $scriptfile


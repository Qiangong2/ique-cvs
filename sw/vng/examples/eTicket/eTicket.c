#include "porting.h"
#include "vng.h"
#include "ec.h"

#ifdef _WIN32
#include <io.h>
#include <fcntl.h>
#include <windows.h>
#define msleep(msec) Sleep(msec)
#define __attribute__(a)
#define _vn_set_my_chip_id(id)

#elif defined (_LINUX)
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#define msleep(msec) usleep(msec*1000)
void _vn_set_my_chip_id (u32 id);

#elif defined (_GBA)
#define msleep(msec) { int i; volatile int j; for (i=0;i<msec;i++) { j=500; while (j-- > 0); } }
#endif

#if defined (_LINUX) || defined (_WIN32)
#include "sc/iostypes.h"
#include "iosc.h"
#include "es.h"
#include "es_int.h"
extern void _iosInitCrypto ();

#elif defined(_GBA)
#include "types.h"
#include "sc/iostypes.h"
#include "es.h"
#endif

#ifndef htonl
#define htonl(x) ((((x)&0xff)<<24) | (((x)&0xff00)<<8) | (((x)&0xff0000)>>8) | (((x)&0xff000000)>>24) )
#endif


// Allocate on .data section
VNG vng __attribute__ ((section (".data")));
ECMeta m __attribute__ ((section (".data")));
ECListTitlesInfo titles[100] __attribute__ ((section (".data")));
ECCacheStatus status __attribute__ ((section (".data")));
ECContentId ecCid __attribute__ ((section (".data")));
char tmdBuf[16 * 1024] __attribute__ ((section (".data")));
u8 deviceCert[ES_DEVICE_CERT_SIZE] ES_SHA_ALIGN __attribute__ ((section (".data")));
u8 certs[8 * 1024] __attribute__ ((section (".data")));
ESTmdView tmdView __attribute__ ((section (".data")));

#define BUFSIZE  (1024*1024)

     char buf[BUFSIZE]
     ES_SHA_ALIGN __attribute__ ((section (".data")));


     void report (const char *testName, int pass)
{
    printf ("*** %s %s\n", testName, pass ? "PASSED" : "FAILED");
}


void
printBuf (const char *buf, u32 size)
{
    int i, j;
    for (i = 0; i < size / 40; i++) {
        for (j = 0; j < 40; j++)
            printf ("%02x", buf[i * 40 + j] & 0xff);
        printf ("\n");
    }
    for (j = i * 40; j < size; j++) {
        printf ("%02x", buf[i * 40 + j] & 0xff);
    }
    printf ("\n");
}


#if defined(_LINUX) || defined(_WIN32)
static void
writeBuf2File (const char *fname, const void *buf, u32 bufSize)
{
    int fd = open (fname, O_WRONLY | O_CREAT | O_TRUNC, 0755);
    if (fd >= 0) {
        write (fd, buf, bufSize);
        close (fd);
    }
}


static int
importTicket (const void *ticket, const void *certs, u32 certsSize)
{
    writeBuf2File ("ticket.sys", ticket, EC_GetETicketSize ());
    if (certs && certsSize > 0)
        writeBuf2File ("xs.certs", certs, certsSize);
    return 0;
}


static int
importTitleInit (u64 tid,
                 const void *tmd,
                 u32 tmdSize, const void *certs, u32 certsSize)
{
    char fname[256];
    snprintf (fname, 256, "%08x-%08x.tmd",
              (int) (tid >> 32), (int) (tid & 0xffffffff));

    printf ("### import %s tmdSize=%d certsSize=%d\n", fname, tmdSize,
            certsSize);
    writeBuf2File (fname, tmd, tmdSize);
    writeBuf2File ("cp.certs", certs, certsSize);

    return 0;
}


static int
importTitleDone ()
{
    return 0;
}


static int
importContentBegin (u64 tid, u32 cid)
{
    char fname[256];
    int fd;

    snprintf (fname, 256, "%08x-%08x-%08x.app",
              (int) (tid >> 32), (int) (tid & 0xffffffff), cid);

    printf ("### import content %s\n", fname);

    fd = open (fname, O_CREAT | O_WRONLY, 0755);
    if (fd < 0) {
        perror (fname);
    }
    return fd;
}


static int
importContentEnd (int fd)
{
    return close (fd);
}


static int
importContentData (int fd, const void *buf, u32 size)
{
    printf ("### importContentData %d\n", size);
    return write (fd, buf, size);
}


#endif


#ifdef _GBA

static int
importTicket (const void *ticket, const void *certs, u32 certsSize)
{
    int rv;
    //  rv = ES_ImportTicket(ticket, certs, certsSize, 0, 0, ES_TRANSFER_SERVER);
    rv = ES_ImportTicket (ticket, certs, certsSize, 0, 0, ES_TRANSFER_SERVER);
    printf ("### ES_ImportTicket returns %d\n", rv);
    report ("ES_ImportTicket", rv == 0);
    return rv;
}


static int
importTitleInit (u64 tid,
                 const void *tmd,
                 u32 tmdSize, const void *certs, u32 certsSize)
{
    int rv;
    rv = ES_ImportTitleInit (tmd, tmdSize, certs, certsSize, 0, 0,
                             ES_TRANSFER_SERVER, 0);
    printf ("### ES_ImportTitleInit returns %d\n", rv);
    report ("ES_ImportTitleInit", rv == 0);
    return rv;
}


static int
importTitleDone ()
{
    int rv;
    rv = ES_ImportTitleDone ();
    printf ("### ES_ImportTitleDone returns %d\n", rv);
    report ("ES_ImportTitleDone", rv == 0);
    return rv;
}


static int
importContentBegin (ESTitleId titleId, ESContentId cid)
{
    int rv = ES_ImportContentBegin (titleId, cid);
    printf ("### ES_ImportContentBegin returns %d\n", rv);
    report ("ES_ImportContentBegin", rv >= 0);
    return rv;
}


static int
importContentEnd (int fd)
{
    int rv = ES_ImportContentEnd (fd);
    printf ("### ES_ImportContentEnd returns %d\n", rv);
    report ("ES_ImportContentEnd", rv >= 0);
    return rv;
}


static int
importContentData (int fd, void *buf, u32 size)
{
    if (fd >= 0) {
        int rv = ES_ImportContentData (fd, buf, size);
        printf ("### ES_ImportContentData fd=%d size=%d returns %d\n", fd,
                size, rv);
        report ("ES_ImportContentData", rv == 0);
        return rv;
    }
    return -1;
}


#endif


// Download TMD, and Certificates
//
int
downloadSpecialFiles (u64 tid, int contentType, char *buf, u32 * bufSize)
{
    ECError rv;
    int n;
    int totalSize;

    // ecCid must be allocated from shared memory area 
    //  (used by EC_CacheContent)
    ecCid.titleId = tid;
    ecCid.contentType = contentType;
    ecCid.contentId = 0;

    rv = EC_DeleteCachedContent (ecCid);
    report ("EC_DeleteCachedContent", rv == 0);

    rv = EC_CacheContent (&ecCid, 1);
    report ("EC_CacheContent", rv == 0);

    while (1) {
        rv = EC_GetCachedContentStatus (&ecCid, 1, &status);
        report ("EC_GetCachedContentStatus", rv == 0);
        if (rv != 0) return EC_ERROR_FAIL;

        // Content cannot be cached
        if (status.cachedSize < 0) {
            printf("### Download failed: status.cachedSize=%d\n", status.cachedSize);
            return EC_ERROR_FAIL;
        }
        
        // display % downloaded
        // Note that totalSize == EC_ERROR_INVALID means the total size
        // is not known yet - it is not an error.
        printf ("%d%% done, cachedSize=%d, totalSize=%d, ...\n",
                status.totalSize > 0 ? status.cachedSize * 100 / status.totalSize : 0,
                status.cachedSize, status.totalSize);
        
        // Content successfully cached
        if (status.cachedSize >= 0 && status.cachedSize == status.totalSize)
            break;

        msleep (1000);
    }

    rv = EC_OpenCachedContent (ecCid);
    report ("EC_OpenCachedContent", rv == 0);
    if (rv != 0)
        return rv;

    totalSize = 0;
    do {
        n = EC_ReadCachedContent (ecCid, buf + totalSize,
                                  *bufSize - totalSize);
        if (n < 0) {
            report ("EC_ReadCachedContent", n >= 0);
            return EC_ERROR_FAIL;
        }
        totalSize += n;
    } while (n > 0);

    rv = EC_CloseCachedContent (ecCid);
    report ("EC_CloseCachedContent", rv == 0);

    printf ("### totalSize is %d\n", totalSize);
    *bufSize = totalSize;
    return EC_ERROR_OK;
}



// Download Title Contents
//
int
downloadContents (u64 tid, u32 cid)
{
    ECError rv;
    int n;
    int totalSize;
    int size;
    int fd = -1;

    // ecCid must be allocated from shared memory area
    ecCid.titleId = tid;
    ecCid.contentType = EC_CONTENT;
    ecCid.contentId = cid;

    printf ("### downloadContent starts ...\n");

    rv = EC_CacheContent (&ecCid, 1);
    report ("EC_CacheContent", rv == 0);

    while (1) {
        rv = EC_GetCachedContentStatus (&ecCid, 1, &status);
        report ("EC_GetCachedContentStatus", rv == 0);
        if (rv != 0) return EC_ERROR_FAIL;

        // Content cannot be cached
        if (status.cachedSize < 0) {
            printf("### Download failed: status.cachedSize=%d\n", status.cachedSize);
            return EC_ERROR_FAIL;
        }
        
        // display % downloaded
        // Note that totalSize == EC_ERROR_INVALID means the total size
        // is not known yet - it is not an error.
        printf ("%d%% done, cachedSize=%d, totalSize=%d, ...\n",
                status.totalSize > 0 ? status.cachedSize * 100 / status.totalSize : 0,
                status.cachedSize, status.totalSize);
        
        // Content successfully cached
        if (status.cachedSize >= 0 && status.cachedSize == status.totalSize)
            break;

        msleep (1000);
    }

    fd = importContentBegin (tid, ecCid.contentId);

    rv = EC_OpenCachedContent (ecCid);
    report ("EC_OpenCachedContent", rv == 0);
    if (rv != 0)
        return rv;

    totalSize = 0;
    do {
        // Fill up a big buffer and call ES_ImportContentData
        size = 0;
        do {
            // EC_ReadCachedContent returns up to VN_MAX_MSG_LEN
            //  which is around 15K bytes
            //
            int maxSize = sizeof (buf) - size;
            if (maxSize > VN_MAX_MSG_LEN)
                maxSize = VN_MAX_MSG_LEN;
            n = EC_ReadCachedContent (ecCid, buf + size, maxSize);
            // printf ("### EC_ReadCachedContent returns %d\n", n);
            if (n < 0) {
                report ("EC_ReadCachedContent", 0);
                return EC_ERROR_FAIL;
            }
            size += n;
        } while (n > 0 && size < sizeof (buf));

        if (size > 0) {
            rv = importContentData (fd, buf, size);
            if (rv < 0) {
                printf ("### Skip import content\n");
                break;
            }
        }
        totalSize += size;
        printf ("%d\n", totalSize);     /* GBA needs \n */
    } while (size > 0);

    rv = EC_CloseCachedContent (ecCid);
    report ("EC_CloseCachedContent", rv == 0);

    importContentEnd (fd);

    return EC_ERROR_OK;
}


// List Subscription Titles
int
listTitles (ECListTitlesInfo * titles, u32 * nTitles)
{
    ECError rv;

    rv = EC_ListTitles (EC_TITLE_KIND_GAME, EC_PK_SUBSCRIPTION, titles,
                        nTitles);
    printf ("### EC_ListTitles returns %d\n", rv);

    while ((rv = EC_GetReturnValue ()) == EC_ERROR_NOT_DONE) {
        printf ("### EC_GetReturnValue returns %d\n", rv);
        msleep (1000);
    }
    return rv;
}


/*
SQL statement to reset ecards 
update ecards set last_used=null, balance='101', balance_authorized='0' where ecard_id between 10000000 and 10000009
*/


int
redeem ()
{
    ECError rv;
    char *eCard101[] = {
        "00000000100000000014264647",
        "00000000100000010065066807",
        "00000000100000020091798645",
        "00000000100000030018185098",
        "00000000100000040097222208",
        "00000000100000050083326699",
        "00000000100000060012120508",
        "00000000100000070002641842",
        "00000000100000080006486755",
        "00000000100000090015357451",
    };

    char *eCard102[] = {
        "00000000100100000096086042",
        "00000000100100010045544608",
        "00000000100100020031511859",
        "00000000100100030020597115",
        "00000000100100040053562294",
        "00000000100100050059898613",
        "00000000100100060075814956",
        "00000000100100070017114261",
        "00000000100100080024078454",
        "00000000100100090099258120",
    };

    char *eCard103[] = {
        "00000000100200000095877286",
        "00000000100200010055976277",
        "00000000100200020090505852",
        "00000000100200030030519295",
        "00000000100200040042459083",
        "00000000100200050039298649",
        "00000000100200060008178167",
        "00000000100200070006870209",
        "00000000100200080024018463",
        "00000000100200090022371897",
    };

    static u8 ticket[4096] = { 1 };
    u32 certsSize = sizeof certs;

    // Obtain eTicket
    rv = EC_RedeemECard (eCard102[8], deviceCert, ticket, certs, &certsSize);

    printf ("### EC_RedeemEcard returns %d\n", rv);

    if (rv != 0) {
        while ((rv = EC_GetReturnValue ()) == EC_ERROR_NOT_DONE) {
            printf ("### EC_GetReturnValue returns %d\n", rv);
            msleep (1000);
        }
    }
    if (rv != EC_ERROR_OK)
        return rv;

    {
#ifdef DUMP_ETICKET
        printf("### eTicket size %d, sizeof %d\n", ES_TICKET_SIZE, sizeof(ESTicket));
        ESTicket *p = (ESTicket *) ticket;
        u64 tid = htonll (p->titleId);
        u32 did = htonl (p->deviceId);
        printf ("### EC_RedeemECard success.  certsSize %d\n", certsSize);
        printf ("### titleId=%08x-%08x\n", (int) (tid >> 32),
                (int) (tid & 0xffffffff));
        printf ("### deviceId=%08x\n", did);
        printBuf(ticket, EC_GetETicketSize());
#endif
        // Pass ticket and XS certs to ES lib
        importTicket (ticket, certs, certsSize);
    }
    return rv;
}



int
getMeta (ECMeta * m)
{
    ECError rv;

    printf ("### EC_GetECMeta starts ...\n");
    rv = EC_GetECMeta (m);
    printf ("### EC_GetECMeta returns %d\n", rv);

    while ((rv = EC_GetReturnValue ()) == EC_ERROR_NOT_DONE) {
        printf ("### EC_GetReturnValue returns %d\n", rv);
        msleep (1000);
    }
    return rv;
}


#if defined(_LINUX)  || defined(_WIN32)

// Use ES_GetTmdView on GBA to determine
// the number of contents and the content ids.
//
static int
tmdGetNumContents (char *tmd)
{
    return ntohs (*(u16 *) (tmd + 478));
}

static int
tmdGetContentId (char *tmd, u32 idx)
{
    return ntohl (*(u32 *) (tmd + 484 + idx * 32));
}
#endif


void
downloadTitle (ECListTitlesInfo * title)
{
    u64 tid = title->id;
    s32 size = (s32) title->fsSize;
    char *name = &title->name[0];
    u32 numContents;
    u32 cid;
    u32 tmdCertSize;
    u32 certsSize;
    u32 actualTmdSize;
    u32 rv;

    printf ("### ListTitles TitlesId %08x-%08x  size %d  name %s\n",
            (int) (tid >> 32), (int) (tid & 0xffffffff), size, name);

    tmdCertSize = sizeof (tmdBuf);
    if ((rv = downloadSpecialFiles (tid, EC_TMD, tmdBuf, &tmdCertSize)) != EC_ERROR_OK) {
        printf("### downloadSpecialFiles returns %d\n", rv);
        return;
    }
    if ((rv = ES_GetTmdSize(tmdBuf, &actualTmdSize)) != ES_ERR_OK) {
        printf("### ES_GetTmdSize returns %d\n", rv);
        return;
    }

    certsSize = tmdCertSize - actualTmdSize;
    memcpy(certs, tmdBuf + actualTmdSize, certsSize);
    
    importTitleInit (tid, tmdBuf, actualTmdSize, certs, certsSize);

    // printBuf(tmdBuf, tmdCertSize);
    // printBuf(certs, certsSize);

#if defined( _LINUX) || defined(_WIN32)
    cid = tmdGetContentId (tmdBuf, 0);
    numContents = tmdGetNumContents (tmdBuf);
#else
    // Need to close TitleInit to get tmdView.
    importTitleDone ();

    {
        u32 tmdViewSize = sizeof (tmdView);
        int rv = ES_GetTmdView (tid, &tmdView, &tmdViewSize);
        printf ("### ES_GetTmDView returns %d, tmdViewSize = %d\n", rv,
                tmdViewSize);
        numContents = tmdView.head.numContents;
    }

    // Reopen to import content.
    importTitleInit (tid, tmdBuf, actualTmdSize, certs, certsSize);
#endif

    printf ("### NumContents %d\n", numContents);

    // continue if numContents is reasonable
    if (numContents <= 8) {
        u32 idx;
        for (idx = 0; idx < numContents; idx++) {
#if defined( _LINUX) || defined(_WIN32)
            cid = tmdGetContentId (tmdBuf, idx);
#else
            cid = tmdView.contents[0].cid;
#endif
            printf ("### contents[%d]=%08x\n", idx, cid);
            downloadContents (tid, cid);
        }
    }
    importTitleDone ();
}


void
doTests ()
{
    VNGErrCode ec;
    ECError rv;
    int downloadAll = 0;
    int downloadOne = 1;
    int downloadCount = 0;

#if defined (_LINUX) || defined (_WIN32)
    ESTitleMeta *tmd = NULL;

    // force chip id to be 1
    _vn_set_my_chip_id (1);

    printf ("tmd numContent offset = %d\n", (int) &tmd->head.numContents);
    printf ("tmd first cid offset = %d\n", (int) &tmd->contents[0].cid);
    printf ("tmd second cid offset = %d\n", (int) &tmd->contents[1].cid);
    _iosInitCrypto ();
    IOSC_GetDeviceCertificate (&deviceCert);
    writeBuf2File ("device.cert", &deviceCert, sizeof (deviceCert));
#endif

#ifdef _GBA
    ES_InitLib ();
    ES_GetDeviceCert (&deviceCert);
#endif

    ec = VNG_Init (&vng, NULL);
    report ("VNG_Init", ec == VNG_OK);

    rv = EC_Init ();
    printf ("### EC_Init returns %d\n", rv);
    report ("EC_Init", rv == EC_ERROR_OK);

    rv = getMeta (&m);
    report ("GetMeta", rv == EC_ERROR_OK);

    rv = redeem ();
    report ("Redeem", rv == EC_ERROR_OK);

    if (downloadAll) {
        u32 nTitles = 100;

        rv = listTitles (titles, &nTitles);
        report ("ListTitles", rv == EC_ERROR_OK);

        if (rv == EC_ERROR_OK) {
            int i;
            printf ("### EC_ListTitles success %d tiles found\n", nTitles);
            for (i = 0; i < nTitles; ++i) {
                downloadTitle (&titles[i]);
            }
        }
    }

    if (downloadOne) {
        // download one test title
        u64 tid = 0x0002000100080033ll;
        u32 tmdCertSize = sizeof (tmdBuf);
        u32 certsSize = sizeof (certs);
        u32 cid;
        u32 actualTmdSize;

        printf("*** Test download %d time\n", ++downloadCount);

#if 0
        printf ("### download MET\n");
        downloadSpecialFiles (tid, EC_MET, tmdBuf, &tmdCertSize);
#endif

        printf ("### download TMD\n");
        tmdCertSize = sizeof (tmdBuf);
        if ((rv = downloadSpecialFiles (tid, EC_TMD, tmdBuf, &tmdCertSize)) != EC_ERROR_OK) {
            printf("### downloadSpecialFiles returns %d\n", rv);
            return;
        }
        if ((rv = ES_GetTmdSize(tmdBuf, &actualTmdSize)) != ES_ERR_OK) {
            printf("### ES_GetTmdSize returns %d\n", rv);
            return;
        }
        
        certsSize = tmdCertSize - actualTmdSize;
        memcpy(certs, tmdBuf + actualTmdSize, certsSize);
        
        importTitleInit (tid, tmdBuf, actualTmdSize, certs, certsSize);

#if defined( _LINUX) || defined(_WIN32)
        cid = tmdGetContentId (tmdBuf, 0);
#else
        // Need to close TitleInit and reopen to get tmdView.
        importTitleDone ();

        {
            u32 tmdViewSize = sizeof (tmdView);
            rv = ES_GetTmdView (tid, &tmdView, &tmdViewSize);
            printf ("### ES_GetTmDView returns %d, tmdViewSize = %d\n", rv,
                    tmdViewSize);
            cid = tmdView.contents[0].cid;
        }

        // Reopen to import content.
        importTitleInit (tid, tmdBuf, actualTmdSize, certs, certsSize);
#endif
        printf ("### contents[%d]=%08x\n", 0, cid);
        downloadContents (tid, cid);

        importTitleDone ();
    }

#ifdef _GBA
    ES_CloseLib ();
#endif

    rv = EC_Fini ();
    report ("EC_Fini", rv == EC_ERROR_OK);

    ec = VNG_Fini (&vng);
    report ("VNG_Fini", ec == VNG_OK);
}

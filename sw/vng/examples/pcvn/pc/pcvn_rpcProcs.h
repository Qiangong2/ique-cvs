


#ifndef __PCVN_RPCPROCS_H__
#define __PCVN_RPCPROCS_H__  1


#pragma once

#include "pcvnOwner.h"


#pragma pack(push, 4)



#define DECL_RP(name)                                \
        extern "C" int32_t name(VN          *vn,     \
                                VNMsgHdr    *hdr,    \
                                const void  *arg,    \
                                size_t       argLen, \
                                void        *ret,    \
                                size_t      *retLen);


DECL_RP(pcvnRP_RunRequestFromGBA)
DECL_RP(pcvnRP_ExitRequestFromGBA)
DECL_RP(pcvnRP_RequestFromGBA)


#pragma pack(pop)


#endif  /* __PCVN_RPCPROCS_H__ */

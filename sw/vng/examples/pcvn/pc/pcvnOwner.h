

#pragma once

#define WIN32_LEAN_AND_MEAN 1

#if _MSC_VER >= 1400
    #if defined(__cplusplus) && !defined(_CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES)
        #define _CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES 1
        #define _CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES_COUNT 1
    #elif !defined(_CRT_SECURE_NO_DEPRECATE)
        #define _CRT_SECURE_NO_DEPRECATE 1
    #endif
#endif

#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <process.h>

#include "vng.h"
#include "pcvn_rpcProcs.h"


#define PCVN_MAIN_LOOP_INTERVAL_MS  250
#define PCVN_NUM_RPC_THREADS        4

typedef unsigned (__stdcall *ThreadFunc) (void *arg);



class TheEnv  {

    public:

    HANDLE          mutex;
    VNGTimeout      vngTimeout;  // Default VNG API timeout
    VNG             pcVng;
    VNG            *vng;
    VN              pcVN;
    VN             *vn;
    VNId            vnId;
    uintptr_t       servRPCThreads[PCVN_NUM_RPC_THREADS];
    unsigned        dwThreadId[PCVN_NUM_RPC_THREADS];

    int             nRPCThreads;

    int             pcvnOwnerRun ();
    VNGErrCode      setupPCVN ();
    int             runMainLoop ();
    int             doInMainLoop ();

    TheEnv ();
};


extern TheEnv __theEnv;


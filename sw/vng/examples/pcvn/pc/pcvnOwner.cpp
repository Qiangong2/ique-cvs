


#include "pcvn.h"
#include "pcvnOwner.h"



TheEnv __theEnv;

static bool __exitRequested;


TheEnv::TheEnv ()
{
    vngTimeout = 10*1000;
    nRPCThreads = PCVN_NUM_RPC_THREADS;
    vng = &pcVng;
    vn  = &pcVN;
    vnId.deviceId = PCVN_INVALID_DEVICE_ID;
    vnId.netId = PCVN_INVALID_NET;

    for (int i = 0;  i < nRPCThreads;  ++i) {
        servRPCThreads[i] = 0;
    }
}


static
VNRemoteProcedure rpcProcs [] = {
    pcvnRP_RunRequestFromGBA,
    pcvnRP_ExitRequestFromGBA,
    pcvnRP_RequestFromGBA,
};


static
VNGErrCode registerRPCs(VN* vn,
                        VNRemoteProcedure *procs,
                        int nProcs)
{
    VNGErrCode   ec;
    VNServiceTag serviceTag;
    int        i;

    for (i = 0;  i < nProcs;  ++i) {
        serviceTag = i + 1;
        if ((ec = VN_RegisterRPCService (vn, serviceTag, procs[i]))) {
            printf ("VN_RegisterRPCService procs[%d] returned %d\n", i, ec);
            break;
        }
    }

    return ec;
}

static
unsigned  __stdcall  servRPCs (TheEnv& the)
{
    VNG* vng = the.vng;
    VNGErrCode ec;

    while ((ec = VNG_ServeRPC(vng, VNG_WAIT)) == VNG_OK) {
        ;
    }
    printf ("servRPCs exiting with ec %d", ec); 
    return NULL;
}





#define PCVN_APP_UNIQUE_MUTEX_NAME   "PCVN_APP_UNIQUE_MUTEX_NAME"

static
bool isAlreadyRunning()
{
    HANDLE hMutex;

    hMutex = CreateMutex (NULL, true, PCVN_APP_UNIQUE_MUTEX_NAME);

    if (hMutex != NULL && GetLastError() == ERROR_ALREADY_EXISTS)
    {
        CloseHandle(hMutex);
        return true;
    }
    return false;
}




int TheEnv::pcvnOwnerRun ()
{
    int          rv = PCVN_ERR_OK;
    VNGErrCode   ec;

    if (isAlreadyRunning()) {
        printf ("PC VN owner is already running\n");
        rv = PCVN_ERR_PC;
        goto end;
    }

    mutex = CreateMutex(NULL, 1, NULL);

    if (mutex == NULL) {
        printf ("CreateMutex returned NULL\n");
        rv = PCVN_ERR_PC;
        goto end;
    }

    if ((ec = VNG_Init(vng, NULL))) {
        printf ("VNG_Init returned %d\n", ec);
        rv = PCVN_ERR_FAIL;
        goto end;
    }

    if ((ec = setupPCVN())) {
        printf ("setupPCVN returned %d\n", ec);
        rv = PCVN_ERR_FAIL;
        goto fini;
    }

    for (int i = 0; i < nRPCThreads; i++) {
         if (!(servRPCThreads[i] = _beginthreadex (NULL, 0,
                                                   (ThreadFunc) servRPCs,
                                                   this, 0,
                                                   &dwThreadId[i]))) {
            printf ("_beginthreadex returned NULL");
            rv = PCVN_ERR_PC;
            goto fini;
        }
    }

    rv = runMainLoop ();


fini:
    if ((ec = VNG_Fini(vng))) {
        printf ("VNG_Fini returned %d", ec);
        if (rv != PCVN_ERR_OK) {
            rv = PCVN_ERR_PC;
        }
    }

end:
    printf ("PC VN owner exiting with retun value %d\n", rv);
    return rv;
}





VNGErrCode  TheEnv::setupPCVN ()
{
    VNGErrCode   ec;
    VNGGameInfo  info;
    int          i;
    char         comments[]  = "";

    VNPolicies     policies = VN_ABORT_ON_OWNER_EXIT; /* not auto accept */

    if ((ec = VNG_NewVN(vng, vn, VN_CLASS_2, VN_DOMAIN_ADHOC, policies))) {
        printf ("VNG_NewVN() Failed %d\n", ec);
        return ec;
    }

    printf ("VNG_NewVN() Success\n");

    if ((ec = VN_GetVNId(vn, &vnId))) {
        printf ("VN_GetVNId() returned %d\n", ec);
        goto end;
    }

    printf ("Created PC vn %p  vn->id %d  "
                 "deviceId: 0x%I64x   netId: 0x%x\n",
                 vn, vn->id, vnId.deviceId, vnId.netId);

    // register pc vn

    info.vnId = vnId;
    info.owner = VNG_MyUserId(vng);   /* will be VNG_INVALID_USER_ID */
    info.gameId = PCVN_GAME_ID;
    info.titleId = PCVN_TITLE_ID;
    info.accessControl = VNG_GAME_PUBLIC;
    info.netZone = 0;      // server will decide this value
    info.totalSlots = 2;
    info.buddySlots = 0;   // nothing reserved for buddies
    info.maxLatency = 100; // 100ms RTT 
    info.attrCount = PCVN_GAME_ATTR_ID + 1;
    strncpy(info.keyword, PCVN_GAME_KEYWORD, sizeof info.keyword);

    for (i = PCVN_GAME_ATTR_ID;  i >= 0;  --i) {
        info.gameAttr[i] = PCVN_GAME_ATTR_VALUE - i;
    }

    if ((ec = VNG_RegisterGame (vng, &info, comments, vngTimeout))) {
        printf ("VNG_RegisterGame() Failed %d\n", ec);
        goto end;
    }

    printf ("Register PC VN Success\n");

    ec = VNG_UpdateGameStatus (vng, vnId,
                               PCVN_GAME_STATUS, PCVN_NUM_PLAYERS,
                               vngTimeout);
    if (ec) {
        printf ("VNG_UpdateGameStatus Failed %d\n", ec);
        goto end;
    }

    int nProcs = sizeof rpcProcs / sizeof rpcProcs [0];

    if ((ec = registerRPCs (vn, rpcProcs, nProcs))) {
        printf ("registerRPCs() Failed %d\n", ec);
        goto end;
    }

    printf ("registerRPCs() Success\n");
    fflush(stdout);

end:
    if (ec) {
        VNG_DeleteVN (vng, vn);
    }

    return ec;
}



int TheEnv::runMainLoop ()
{
    int            rv = PCVN_ERR_OK;
    VNGErrCode     ec;
    VN            *vn_hosted;
    VNGUserInfo    userInfo;
    char           joinReason[VNG_JOIN_REQ_MSG_BUF_SIZE];
    uint64_t       requestId;
    BOOL           rc;

    while (1) {

        if (!(rc = ReleaseMutex (mutex))) {
            printf ("ReleaseMutex returned %d\n", rc);
            rv = PCVN_ERR_PC;
        }

        if (rv) {
            break;
        }

        ec = VNG_GetJoinRequest(vng,
                                vnId,  // only accept pcVN requests
                                &vn_hosted,  // vn out will always be pcVN
                                &requestId,
                                &userInfo,
                                joinReason,
                                sizeof joinReason,
                                PCVN_MAIN_LOOP_INTERVAL_MS);
                                    
        if (ec && ec != VNGERR_TIMEOUT) {
            printf ("VNG_GetJoinRequest returned %d\n", ec);
            continue;
        }

        if (ec == VNG_OK) {
            if ((ec = VNG_AcceptJoinRequest(vng, requestId))) {
                printf ("VNG_AcceptJoinRequest returned %d\n", ec);
            } else {
                printf ("Got and accepted join Request\n");
            }
        }

        if (__exitRequested) {
            break;
        }

        rc = WaitForSingleObject(mutex, INFINITE);

        if (rc != WAIT_OBJECT_0) {
            printf ("WaitForSingleObject returns %u\n",rc);
            rv = PCVN_ERR_PC;
            break;
        }

        if ((rv = doInMainLoop ())) {
            break;
        }
    }

    return rv;
}


int TheEnv::doInMainLoop()
{

    // check other things

    return PCVN_ERR_OK;

}



static runRequestFromGBA (char * cmd,
                          void   *ret,
                          size_t *retLen)
{
    // Should be much more elaborate using WIN32 CreateProcess()
    // so can monitior and communicate with created process
    // see sw/clients/packages/iqahc/iqahclib/Diag.cpp for example

    printf ("Executing process(%s)\n", cmd);
    int rv = system (cmd);
    printf ("process(%s) returned %d\n", cmd, rv);

    if (rv < 0) {
        printf ("runRequestFromGBA process(%s) FAILED\n", cmd);
        rv = PCVN_ERR_FAIL;
    }
    else {
        rv = PCVN_ERR_OK;
    }

    char *returnMsg = "Program started successfully";

    strcpy ((char*)ret, returnMsg);
    *retLen = strlen (returnMsg) + 1;

    return rv;
}



int32_t pcvnRP_RunRequestFromGBA (VN         *vn,
                                  VNMsgHdr   *hdr,
                                  const void *args,
                                  size_t      argsLen,
                                  void       *ret,
                                  size_t     *retLen)
{
    int32_t rv = PCVN_ERR_NOT_FOUND;

    if (argsLen) {
        rv = runRequestFromGBA ((char*) args, ret, retLen);
    }
    return rv;
}



int32_t pcvnRP_ExitRequestFromGBA (VN         *vn,
                                   VNMsgHdr   *hdr,
                                   const void *args,
                                   size_t      argsLen,
                                   void       *ret,
                                   size_t     *retLen)
{
    int32_t rv = PCVN_ERR_OK;

    printf ("Exiting on request\n");
    __exitRequested = true;
    *retLen = 0;
    return rv;
}






static requestFromGBA (char   *requestString,
                       void   *ret,
                       size_t *retLen)
{
    int32_t rv = PCVN_ERR_NOT_FOUND;

    // Parse requestString string and respond to specific request

    char *returnMsg = "Found and executed cmd";
    rv = PCVN_ERR_OK;

    strcpy ((char*)ret, returnMsg);
    *retLen = strlen (returnMsg) + 1;

    return rv;
}



int32_t pcvnRP_RequestFromGBA (VN         *vn,
                                  VNMsgHdr   *hdr,
                                  const void *args,
                                  size_t      argsLen,
                                  void       *ret,
                                  size_t     *retLen)
{
    int32_t rv = PCVN_ERR_NOT_FOUND;

    *retLen = 0;
    if (argsLen) {
        rv = requestFromGBA ((char*) args, ret, retLen);
    }
    return rv;
}




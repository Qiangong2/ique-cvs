
#include "es.h"


#include "porting.h"
#include "vng.h"
#include "vng_p.h"
#include "testcfg.h"
#include "pcvn.h"


#define bool32  s32


#ifdef _WIN32
#include <windows.h>
    #define msleep(msec) Sleep(msec)
    #define __attribute__(a)
#elif defined (_LINUX)
    #include <unistd.h>
    #define msleep(msec) usleep(msec*1000)
#else
    #define msleep(msec) { int i; volatile int j; for (i=0;i<msec;i++) { j=500; while (j-- > 0); } }
#endif



#define NET_CONTENT_TIMEOUT   (60*1000)

VNG  __vng __attribute__((section(".data")));
u8   sBuf[1024] __attribute__((section(".data")));
u8   rBuf [1024] __attribute__((section(".data")));

static VN __pcVN = { _VNG_INVALID_VNGVNID,   
                          _VNG_INVALID_VNGID,     
                          _VNG_INVALID_VNGNETADDR };

static VNId __pcVNId = { PCVN_INVALID_DEVICE_ID, PCVN_INVALID_NET};
static bool32 __pcVNConnected;
static VNGTimeout __pcVNJoinTimeout = (15*1000);
static VNGTimeout __pcVNRPCTimeout  = (15*1000);





void report(const char *testname, int pass)
{
    if (pass >= 0) 
        printf("*** PCVN %s TEST PASSED\n", testname);
    else
        printf("*** PCVN %s TEST FAILED\n", testname);
}





char *vngErrMsg (VNGErrCode ec)
{
    static char vemBuf[256];
    VNGErrCode vemEc;

    if ((vemEc = VNG_ErrMsg (ec, vemBuf, sizeof vemBuf))) {
        snprintf(vemBuf, sizeof vemBuf, "VNG_ErrMsg(%d, vemBuf, sizeof vemBuf) returned %d", ec, vemEc);
    }    
    return vemBuf;
}



static
int  find_pcvn ()
{
    int         rv = PCVN_ERR_OK;
    VNGErrCode  ec;
    VNG        *vng = &__vng;
    char        denyReason [64];
    int         i;
    u32         n;

    VNGSearchCriteria  crit;
#ifdef _GBA
    /*   gameStatus[] Does not need to be thread safe on GBA,
     *  but does need to be initialized global mem for GBA.
     */
    static
    VNGGameStatus  gameStatus[2] = {{{{PCVN_INVALID_DEVICE_ID}}}};
#else
    VNGGameStatus  gameStatus[2];
#endif

    VNGGameStatus     *pc = &gameStatus[0];

    crit.domain = VNG_SEARCH_ADHOC;
    crit.gameId = PCVN_GAME_ID;
    crit.maxLatency = 30000;
    crit.cmpKeyword = VNG_CMP_STR;
    memcpy(crit.keyword, PCVN_GAME_KEYWORD, sizeof PCVN_GAME_KEYWORD);

    for (i = 0; i < VNG_MAX_PREDICATES; i++) {
        crit.pred[i].cmp = VNG_CMP_DONTCARE;
    }

    for (i = PCVN_GAME_ATTR_ID;  i >= 0;  --i) {
        crit.pred[i].attrId = i;
        crit.pred[i].attrValue = PCVN_GAME_ATTR_VALUE - i;
        crit.pred[i].cmp = VNG_CMP_EQ;
    }

    n = sizeof gameStatus / sizeof gameStatus[0];

    /* timeout is not used for adhoc searches */
    if ((ec = VNG_SearchGames(vng, &crit, gameStatus, &n, 0, VNG_NOWAIT))) {
        printf ("VNG_SearchGames()  Failed: %d\n", ec);
        rv = ec;
        goto end;
    }

    if (n != 1) {
        if (!n) {
            rv = PCVN_ERR_NOT_FOUND;
        } else {
            rv = PCVN_ERR_FAIL;
        }
        printf ("VNG_SearchGames() found %d PC VN services\n", n);
        goto end;
    }

    if (pc->gameStatus != PCVN_GAME_STATUS
            || pc->numPlayers != PCVN_NUM_PLAYERS) {

        rv = PCVN_ERR_FAIL;
        printf ("VNG_SearchGames() found PC VN service with game status %d "
                "and num players %d\n", pc->gameStatus, pc->numPlayers);
        goto end;
    }

    printf ("Found PC VN service\n");
    __pcVNId = pc->gameInfo.vnId;

    if ((ec = VNG_JoinVN (vng, __pcVNId, "Please let me join !",
                              &__pcVN, denyReason, sizeof denyReason,
                               __pcVNJoinTimeout))) {

        printf ( "VNG_JoinVN(__pcVNId) Failed %d  "
                            "deviceId: 0x%08x.0x%08x   netId: 0x%08x\n", ec,
                            (int)(__pcVNId.deviceId >> 32),
                            (int)__pcVNId.deviceId, __pcVNId.netId);
        rv = ec;
        goto end;
    }

    printf ("VNG_JoinVN(__pcVNId) Success  "
                        "deviceId: 0x%08x.0x%08x   netId: 0x%08x\n",
                         (int)(__pcVNId.deviceId >> 32),
                         (int)__pcVNId.deviceId, __pcVNId.netId);

    __pcVNConnected = true;

end:
    return rv;
}





int sendGenericRequestToPC (VN *vn)
{
    VNGErrCode   ec;
    int          res = 0;
    size_t       sndLen;
    size_t       retLen = sizeof rBuf;
    int32_t      optData;
    char        *cmdMsg = "cmd arg1 arg2";

    VNServiceTag serviceTag = PCVN_ST_GBA_TO_PC_REQUEST;

    sndLen = strnlen(cmdMsg, sizeof sBuf);
    if (sndLen == sizeof sBuf) {
        res = PCVN_ERR_FAIL;
        goto end;
    }
    ++sndLen;
    memcpy (sBuf, cmdMsg, sndLen);
    retLen = sizeof rBuf;
    optData = 0;

    ec = VN_SendRPC (vn, VN_MEMBER_OWNER,
                     serviceTag,
                     sBuf, sndLen,
                     rBuf, &retLen,
                     &optData,
                     __pcVNRPCTimeout);

    if (ec) {
        res = ec;
        printf ("VN_SendRPC returned %d: %s\n",
                         ec, vngErrMsg (ec));
        goto end;
    }

    if (optData) {
        res = optData;
        printf ("VN_SendRPC returned optData %d\n", optData);
        goto end;
    }

    rBuf[(sizeof rBuf) - 1] = 0;
    printf ("GBA to PC request returned msgLen %d, msg: \"%s\"\n", retLen, rBuf);

end:

    printf ("sendGenericRequestToPC returning %d\n", res);

    return res;
}


int sendRunRequestToPC (VN *vn)
{
    VNGErrCode   ec;
    int          res = 0;
    size_t       sndLen;
    size_t       retLen = sizeof rBuf;
    int32_t      optData;
    char        *cmdMsg = "helloWorld.exe";

    VNServiceTag serviceTag = PCVN_ST_GBA_TO_PC_RUN_REQUEST;

    sndLen = strnlen(cmdMsg, sizeof sBuf);
    if (sndLen == sizeof sBuf) {
        res = PCVN_ERR_FAIL;
        goto end;
    }
    ++sndLen;
    memcpy (sBuf, cmdMsg, sndLen);
    retLen = sizeof rBuf;
    optData = 0;

    ec = VN_SendRPC (vn, VN_MEMBER_OWNER,
                     serviceTag,
                     sBuf, sndLen,
                     rBuf, &retLen,
                     &optData,
                     __pcVNRPCTimeout);

    if (ec) {
        res = ec;
        printf ("VN_SendRPC run request returned %d: %s\n",
                         ec, vngErrMsg (ec));
        goto end;
    }

    if (optData) {
        res = optData;
        printf ("VN_SendRPC run request returned optData %d\n", optData);
        goto end;
    }

    rBuf[(sizeof rBuf) - 1] = 0;
    printf ("GBA to PC run request returned msgLen %d, msg: \"%s\"\n", retLen, rBuf);

end:

    printf ("sendRunRequestToPC returning %d\n", res);

    return res;
}


int sendExitRequestToPC (VN *vn)
{
    VNGErrCode   ec;
    int          res = 0;
    int32_t      optData = 0;

    VNServiceTag serviceTag = PCVN_ST_GBA_TO_PC_EXIT_REQUEST;

    ec = VN_SendRPC (vn, VN_MEMBER_OWNER,
                     serviceTag,
                     NULL, 0,
                     NULL, NULL,
                     &optData,
                     __pcVNRPCTimeout);

    if (ec) {
        res = ec;
        printf ("VN_SendRPC exit request returned %d: %s\n",
                         ec, vngErrMsg (ec));
        goto end;
    }

    if (optData) {
        res = optData;
        printf ("VN_SendRPC exit request returned optData %d\n", optData);
        goto end;
    }

    printf ("GBA to PC exit request success\n");

end:

    printf ("sendExitRequestToPC returning %d\n", res);

    return res;
}



void sendRequestsToPC ()
{
    VNGErrCode ec;
    int        res = 0;
    VNG       *vng = &__vng;
    VN        *vn  = &__pcVN;

    printf("\n*** LIBVNG SendCmdToPC TEST START\n");

    if ((ec = VNG_Init(vng, NULL))) {
        printf ("VNG_Init returned %d\n", ec);
        res = -1;
        goto end;
    }
    else {
        printf ("VNG_Init successful\n");
    }

    while (1) {
        res =  find_pcvn ();
        printf("find_pcvn returns %d\n", res);

        if (res) {
            msleep (1000);
            continue;
        }
        else {
            break;
        }

    }

    res = sendGenericRequestToPC(vn);
    report ("Send Generic Request to PC", res);
    if (res) {
        goto end;
    }

    res = sendRunRequestToPC(vn);
    report ("Send Run Request to PC", res);
    if (res) {
        goto end;
    }

    res = sendExitRequestToPC(vn);
    report ("Send Exit Request to PC", res);

end:
    report ("LIBVNG SendCmdToPC", res);
    VNG_Fini(vng);

}








#ifndef __PCVN_H__
#define __PCVN_H__  1



#define PCVN_GAME_ID          0x80000002
#define PCVN_TITLE_ID         0  /* an invalid titleId */
#define PCVN_GAME_KEYWORD     "PCVN GAME KEYWORD"
#define PCVN_GAME_ATTR_ID     3
#define PCVN_GAME_ATTR_VALUE  76173989
#define PCVN_GAME_STATUS      21090291
#define PCVN_NUM_PLAYERS      23


#define PCVN_INVALID_NET         (-1)
#define PCVN_INVALID_DEVICE_ID   (-1)

#define PCVN_ERR_OK              0
#define PCVN_ERR_FAIL           -1
#define PCVN_ERR_INVALID        -4
#define PCVN_ERR_NOT_FOUND      -6
#define PCVN_ERR_PC             -16

#define PCVN_ST_GBA_TO_PC_RUN_REQUEST    1
#define PCVN_ST_GBA_TO_PC_EXIT_REQUEST   2
#define PCVN_ST_GBA_TO_PC_REQUEST        3

#endif  /* __PCVN_H__ */

#include <sc/sc.h>
#include <sc/ios.h>
#include <ioslibc.h>

#define STACK_SIZE 1024
const u8 _initStack[STACK_SIZE];
const u32 _initStackSize = sizeof(_initStack);
const u32 _initPriority = 5;

int
main(void)
{
    printf("Switching GBA to run the user program at 0x80004000\n");

    IO_WRITE(MEM0_START, 0xea000ffe);   /* Branch to 0x08004000 */
    SCWriteback((void *)MEM0_START, 4);
    CCInvalidate((void *)MEM0_START, 4);

    return (0);
}

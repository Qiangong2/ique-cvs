#include "../include/testcfg.h"
#include "gba/scrpc.h"
#include "vng.h"
#include "vng_p.h"

typedef void    (*IntrFuncp) (void);
void
IntrDummy(void)
{
}

const IntrFuncp IntrTable[13] = {
    IntrDummy,			// V Blank interrupt
    IntrDummy,			// H Blank interrupt
    IntrDummy,			// V Counter interrupt
    IntrDummy,			// Timer 0 interrupt
    IntrDummy,			// Timer 1 interrupt
    IntrDummy,			// Timer 2 interrupt
    IntrDummy,			// Timer 3 interrupt
    IntrDummy,			// Serial communication interrupt
    IntrDummy,			// DMA 0 interrupt
    IntrDummy,			// DMA 1 interrupt
    IntrDummy,			// DMA 2 interrupt
    IntrDummy,			// DMA 3 interrupt
    IntrDummy,			// Key interrupt
};

char heap[64*1024] = { 1 };
void *SM_Malloc(size_t size)
{
    static int      sm_ptr;
    void           *ret;
    if (!sm_ptr)
	sm_ptr = (int)heap;

    ret = (void *) sm_ptr;
    sm_ptr += size;
    if (sm_ptr & 3)
	sm_ptr = (sm_ptr+0x4)&(0-4);
    return ret;
}


VNGErrCode      ec;
VNG            *vng;
VN             *vn;
VNGTimeout      timeout;
char           *buffer;
int             tmp_int;
long long       tmp_llong;
VNId            tmp_vnid;


void
testBuddies()
{
    printf("testBuddies\n");
    {
        static VNGUserInfo     buddies[10] = {1};
	uint32_t        nBuddies = 10;

        printf("calling VNG_GetBuddyList with VNG_WAIT\n");
	if ((ec = VNG_GetBuddyList(vng, 0, buddies, &nBuddies, VNG_WAIT))) {
	    ;
	}
        printf("VNG_GetBuddyList returns %d\n", ec);

	if ((ec = VNG_InviteUser(vng, "eli", timeout))) {
	    ;
	}
        printf("VNG_InviteUser returns %d\n", ec);

	if ((ec = VNG_AcceptUser(vng, buddies[0].uid, timeout))) {
	    ;
	}
        printf("VNG_AcceptUser returns %d\n", ec);

	if ((ec = VNG_RejectUser(vng, buddies[0].uid, timeout))) {
	    ;
	}
        printf("VNG_RejectUser returns %d\n", ec);

	if ((ec = VNG_BlockUser(vng, buddies[0].uid, timeout))) {
	    ;
	}
        printf("VNG_BlockUser returns %d\n", ec);

	if ((ec = VNG_UnblockUser(vng, buddies[0].uid, timeout))) {
	    ;
	}
        printf("VNG_UnblockUser returns %d\n", ec);

	if ((ec = VNG_RemoveUser(vng, buddies[0].uid, timeout))) {
	    ;
	}
        printf("VNG_RemoveUser returns %d\n", ec);
    }

    {
	if ((ec =
	     VNG_UpdateStatus(vng, VNG_STATUS_GAMING, 123, tmp_vnid,
			      timeout))) {
	    ;
	}
        printf("VNG_UpdateStatus returns %d\n", ec);
    }

    {
        static VNGBuddyStatus  buddy_status[10] = {1};
	if ((ec = VNG_GetBuddyStatus(vng, buddy_status, 10, timeout))) {
	    ;
	}
        printf("VNG_GetBuddyStatus returns %d\n", ec);
    }

    {
	if ((ec = VNG_EnableTracking(vng, VNG_DATA_BUDDYSTATUS, timeout))) {
	    ;
	}
        printf("VNG_EnableTracking returns %d\n", ec);

	if ((ec = VNG_DisableTracking(vng, VNG_DATA_BUDDYSTATUS, timeout))) {
	    ;
	}
        printf("VNG_DisableTracking returns %d\n", ec);
    }
}


#define GAME_STATUS   20060120
#define NUM_PLAYERS   7

void
testGameStatus()
{
    printf("testGameStatus\n");
    {

        VNId        vnId_game = {0,0};

        printf ("Calling VNG_UpdateGameStatus\n");
        if ((ec = VNG_UpdateGameStatus (vng, vnId_game, GAME_STATUS,
                                        NUM_PLAYERS, timeout))) {
            ;
        }
        printf ("VNG_UpdateGameStatus returns %d\n", ec);

        static VNGGameStatus   gameStatus[10] = {1};
	if ((ec = VNG_GetGameStatus(vng, gameStatus, 10, timeout))) {
	    ;
	}
    }

    {
        static char            buf[1024] = {1};
	size_t          commentSize = 1024;
	if ((ec =
	     VNG_GetGameComments(vng, tmp_vnid, buf, &commentSize,
				 timeout))) {
	    ;
	}
    }
}


void
testVN()
{
    VNMember self;
    VNMember owner;
    VNState  state;
    static VN       vns[2] = {1};
    uint32_t nVNs = sizeof vns/sizeof vns[0];

    printf("testVN\n");
    if ((ec =
	 VNG_NewVN(vng, vn, VN_DEFAULT_CLASS, VN_DOMAIN_INFRA,
		   VN_DEFAULT_POLICY))) {
	;
    }


    self = VN_Self(vn);
    printf ("VN_Self returns %u\n", self);
    owner = VN_Owner(vn);
    printf ("VN_Owner returns %u\n", owner);
    state = VN_State(vn);
    printf ("VN_State returns %u\n", state);

    if ((ec = VNG_GetVNs (vng, vns, &nVNs))) {
	;
    }
    printf ("VN_GetVNs returns %d  nVNs  %u\n", ec, nVNs);

    if ((ec = VNG_DeleteVN(vng, vn))) {
	;
    }
}


void
testGameRegistration()
{
    VNGGameInfo     info;
    static char            comments[] = "test comment";
    printf("testGameRegistration\n");
    if ((ec = VNG_RegisterGame(vng, &info, comments, timeout))) {
	;
    }
    printf("VNG_RegisterGame returns %d\n", ec);

    if ((ec = VNG_UnregisterGame(vng, info.vnId, timeout))) {
	;
    }
    printf("VNG_UnregisterGame returns %d\n", ec);
}


void
testJoinVN()
{
    uint64_t        requestId;
    VNGUserId      *uid = SM_Malloc(sizeof(VNGUserId));
    VNGUserInfo    *userInfo = SM_Malloc(sizeof(VNGUserInfo));
    VNGGameInfo    *gameInfo = SM_Malloc(sizeof(VNGGameInfo));
    VNMember       *members = SM_Malloc(sizeof(VNMember) * 10);
    VNGDeviceType   type;
    VNGMillisec     ms;
    VN             *tmp_vn;

    printf("testJoinVN\n");
    if ((ec = VNG_JoinVN(vng, tmp_vnid, NULL, vn, NULL, 0, timeout))) {
	;
    }

    if ((ec =
	 VNG_GetJoinRequest(vng, tmp_vnid, &tmp_vn, &requestId,
			    userInfo, NULL, 0, timeout))) {
	;
    }

    if ((ec = VNG_AcceptJoinRequest(vng, requestId))) {
	;
    }

    if ((ec = VNG_RejectJoinRequest(vng, requestId, NULL))) {
	;
    }

    if ((ec = VNG_LeaveVN(vng, vn))) {
	;
    }

    printf ("Calling VNG_Invite\n");
    *uid = 1;
    if ((ec = VNG_Invite(vng, gameInfo, uid, 1, NULL))) {
	;
    }
    printf ("VNG_Invite returns %d\n", ec);

    if ((ec =
	 VNG_GetInvitation(vng, userInfo, gameInfo, NULL, 0, timeout))) {
	;
    }

    tmp_int = VN_NumMembers(vn);

    tmp_int = VN_GetMembers(vn, members, 10);

    if ((ec = VN_MemberState(vn, members[0]))) {
	;
    }

    *uid = VN_MemberUserId(vn, members[0], timeout);
    ms = VN_MemberLatency(vn, members[0]);

    type = VN_MemberDeviceType(vn, members[0]);

    if ((ec = VN_GetVNId(vn, &tmp_vnid))) {
	;
    }
}


void
testMsgPassing()
{
    VNServiceTypeSet ready;
    VNMember        memb;
    char           *msg = SM_Malloc(10);
    size_t          msglen = 10;
    VNMsgHdr       *hdr = SM_Malloc(sizeof(VNMsgHdr));
    VNCallerTag     callerTag;

    printf("testMsgPassing\n");
    if ((ec = VN_Select(vn, VN_SERVICE_TYPE_ANY, &ready, memb, timeout))) {
	;
    }

    if ((ec =
	 VN_SendMsg(vn, memb, VN_SERVICE_TAG_ANY, msg, 10,
		    VN_ATTR_UNRELIABLE, VNG_WAIT))) {
	;
    }

    if ((ec =
	 VN_RecvMsg(vn, memb, VN_SERVICE_TAG_ANY, msg, &msglen, hdr,
		    timeout))) {
	;
    }

    if ((ec =
	 VN_SendReq(vn, memb, VN_SERVICE_TAG_ANY, &callerTag, msg, 10, 0,
		    VNG_WAIT))) {
	;
    }

    if ((ec =
	 VN_RecvReq(vn, memb, VN_SERVICE_TAG_ANY, msg, &msglen, hdr,
		    timeout))) {
	;
    }

    printf("Calling VN_SendResp\n");
    if ((ec =
	 VN_SendResp(vn, memb, VN_SERVICE_TAG_ANY, 50000, msg, 10, 0,
		     VNG_WAIT))) {
	;
    }
    printf("VN_SendResp returns %d\n", ec);

    if ((ec =
	 VN_RecvResp(vn, memb, VN_SERVICE_TAG_ANY, 0, msg, &msglen,
		     hdr, timeout))) {
	;
    }
}


void
testRPC()
{
    char           *args = SM_Malloc(10);
    char           *ret = SM_Malloc(10);
    size_t          retLen = 10;
    int32_t         optData;

    printf("testRPC\n");
    if ((ec = VN_RegisterRPCService(vn, VN_SERVICE_TAG_ANY, 0))) {
	;
    }

    if ((ec = VN_UnregisterRPCService(vn, VN_SERVICE_TAG_ANY))) {
	;
    }

    if ((ec =
	 VN_SendRPC(vn, 0, VN_SERVICE_TAG_ANY, args, 10, ret, &retLen,
		    &optData, timeout))) {
	;
    }

    if ((ec = VNG_ServeRPC(vng, timeout))) {
	;
    }
    printf("VNG_ServeRPC returns %d\n", ec);
}


void
testSearchGames()
{
    VNGSearchCriteria searchCriteria;
    static VNGGameStatus   gameStatus = {1};
    int32_t         numGameStatus;
    static const char *serverName = "mars.routefree.com";
    VNGPort     serverPort = 16978;

    printf ("Calling VNG_AddServerToAdhocDomain\n");
    if ((ec = VNG_AddServerToAdhocDomain (vng, serverName, serverPort))) {
	;
    }
    printf ("VNG_AddServerToAdhocDomain returns %d\n", ec);

    printf ("Calling VNG_ResetAdhocDomain\n");
    if ((ec = VNG_ResetAdhocDomain (vng))) {
	;
    }
    printf ("VNG_ResetAdhocDomain returns %d\n", ec);

    printf("testSearchGames\n");
    numGameStatus = 1;
    if ((ec =
	 VNG_SearchGames(vng, &searchCriteria, &gameStatus,
			 &numGameStatus, 0, timeout))) {
	;
    }
    printf("VNG_SearchGames returns %d\n", ec);
}

void
testGameScore()
{
    VNGScoreKey     key;
    VNGScore        score;
    VNGScoreItem    item[10];
    uint32_t        nItems = 10;
    uint8_t         info[16];
    char            object[10];
    size_t          objectSize = 10;
    VNGRankedScoreResult        results[10];
    uint32_t                    nResults = 10;

    printf("testGameScore\n");
    if ((ec = VNG_SubmitScore(vng, &key, info, item, nItems, timeout))) {
	;
    }

    if ((ec =
	 VNG_SubmitScoreObject(vng, &key, object, objectSize, timeout))) {
	;
    }

    if ((ec = VNG_GetGameScore(vng, &key, &score, item, &nItems, timeout))) {
	;
    }

    if ((ec =
	 VNG_GetGameScoreObject(vng, &key, object,
				&objectSize, timeout))) {
	;
    }

    if ((ec =
	 VNG_GetRankedScores(vng, 1, 2, 3, tmp_llong, tmp_llong, 1, results, &nResults,
			     timeout))) {
	;
    }
}



void
testStoreMessage()
{
    static uint32_t   results[100] = {1};

    const char*  recipientNames[] = {

        TESTER01,
        "tester02",
    };

    const char subject[] = "GBA VNG API Test";
    const char msg[] = "GBA VNG API Test Message";
    
    size_t         msgLen = sizeof msg;
    uint32_t       nRecipients = 2;
    uint32_t       mediaType = VNG_MEDIA_TYPE_TEXT;
    uint32_t       replyMsgid = 1;

    printf ("Calling VNG_StoreMessage\n");
    if ((ec =
        VNG_StoreMessage (vng,
                          recipientNames,
                          nRecipients,
                          subject,
                          mediaType,
                          msg,
                          msgLen,
                          replyMsgid,
                          results,
                          timeout))) {
        ;
    }
    printf("VNG_StoreMessage returns %d\n", ec);

    {
        static VNGMessageHdr msgHdr[100] = {1};
        static char msgBuffer[1024] = {1};
        uint32_t    nMsgHdr = sizeof(msgHdr) / sizeof(msgHdr[0]);
        uint32_t    nMessages;
        uint32_t    msgId;
        size_t      msgBufferLen = sizeof msgBuffer;
        uint32_t    msgIds[1];
        uint32_t    nMsgIds = 1;

        printf ("Calling VNG_GetMessageList\n");
        if ((ec =
            VNG_GetMessageList (vng, 0, msgHdr, &nMsgHdr, &nMessages, timeout))) {
            ;
        }
        printf("VNG_GetMessageList returns %d\n", ec);

        msgId = msgHdr[0].msgId ? msgHdr[0].msgId : 1;

        printf ("Calling VNG_RetrieveMessage\n");
        if ((ec =
            VNG_RetrieveMessage (vng,
                                 msgId,
                                 msgBuffer,
                                 &msgBufferLen,
                                 timeout))) { 
            ;
        }
        printf("VNG_RetrieveMessage returns %d\n", ec);

        msgIds[0] = 56789;
        nMsgIds = 1;
        printf ("Calling VNG_DeleteMessage\n");
        if ((ec = VNG_DeleteMessage (vng, msgIds, nMsgIds, timeout))) {
            ;
        }
        printf("VNG_DeleteMessage returns %d\n", ec);
    }
}



void
AgbMain(void)
{
    vng = SM_Malloc(sizeof(VNG));
    vn = SM_Malloc(sizeof(VN));
    timeout = (10 * 1000);

    printf ("Got to AbgMain\n");

    {
	char           *errbuf = SM_Malloc(1024);
	if ((ec = VNG_ErrMsg(VNGERR_TIMEOUT, errbuf, 1024))) {
            ;
	}
        printf("errbuf:%s\n", errbuf);
    }

    {
	if ((ec = VNG_Init(vng, NULL))) {
	    printf("VNG_Init returned %d\n", ec);
	}
    }

    {
        VNGTraceOpt trop = {
            VNG_TROP_GLOBAL_ENABLE |
            VNG_TROP_GLOBAL_LEVEL  |
            VNG_TROP_GRP_LEVEL,
            { true, 4, {0} },
                { 1, 0, 5, 0, 0, { 0, 0 } }
        };

        printf ("Enable trace, set trace_level FINE, VNG trace_level FINER\n");

        if ((ec = VNG_SetTraceOpt (vng, trop))) {
            printf ("VNG_SetTraceOpt returned %d\n", ec);
        }
    }

    {
	char            serverName[] = VNGS_HOST;
	VNGPort         serverPort = VNGS_PORT;
	char            loginName[] = TESTER01;
	char            passwd[] = TESTER01PW;
	if ((ec =
	     VNG_Login(vng, serverName, serverPort, loginName, passwd,
		       timeout))) {
	    ;
	}
    }

    {
	tmp_int = VNG_GetLoginType(vng);
	tmp_llong = VNG_MyUserId(vng);
    }

    {
	VNGUserInfo    *uinfo = SM_Malloc(sizeof(VNGUserInfo));
	if ((ec =
	     VNG_GetUserInfo(vng, VNG_MyUserId(vng), uinfo, VNG_WAIT))) {
	    ;
	}
    }

    {
         printf ("Calling VNG_ConnectStatus\n");
         uint32_t cstat = VNG_ConnectStatus(vng);
         printf ("VNG_ConnectStatus returns 0x%08x\n", cstat);
    }

    testBuddies();

    testGameStatus();

    testVN();

    testGameRegistration();

    testJoinVN();

    testMsgPassing();

    testRPC();

    testSearchGames();

    testGameScore();

    testStoreMessage();

    {
	VNGEvent       *event = SM_Malloc(sizeof(VNGEvent));

	if ((ec = VNG_GetEvent(vng, event, timeout))) {
	    ;
	}
    }

    {
	if ((ec = VNG_Logout(vng, timeout))) {
	    ;
	}
    }

    {
	if ((ec = VNG_Fini(vng))) {
	    ;
	}
    }

  end:
    while (1) {
	;
    }
}

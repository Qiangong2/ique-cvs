//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "stdafx.h"
#include "vng.h"
#include "../include/testcfg.h"

using namespace std;
#include <iostream>
#include <time.h>
#include <set>
#include <hash_set>
using namespace stdext;

bool trace = true;

void report(const char *testname, int pass, int cmpKwCase)
{
    fprintf(stdout, "*** LIBVNG %s TEST %s  cmKwCase %d\n",
        testname, (pass >= 0 ? "PASSED" : "FAILED"), cmpKwCase);
}

void dumpVN(VN *vn)
{
    VNId vid;
    VNGErrCode err = VN_GetVNId(vn, &vid);
    if (err) {
        fprintf(stderr, "VNG_GetVNId returns %d\n", err);
        return;
    }
    fprintf(stdout, "VN:\n");
    fprintf(stdout, "  VNId is 0x%08x.%08x.%08x\n",  (int)(vid.deviceId >> 32), (int)vid.deviceId, vid.netId);

    VNMember mem = VN_Self(vn);
    VNMember owner = VN_Owner(vn);
    VNMember state = VN_State(vn);
    int num = VN_NumMembers(vn);

    fprintf(stdout, "  VN Self is %d\n", mem);
    fprintf(stdout, "  VN Owner is %d\n", owner);
    fprintf(stdout, "  VN State is %d\n", state);
    fprintf(stdout, "  VN member count is %d\n", num);

    VNMember mems[100];
    int total_mem = VN_GetMembers(vn, mems, 100);
    for (int i = 0; i < total_mem; i++) {
        fprintf(stdout, "  mem[%d] is %d\n", i, mems[i]);
    }
}

void dumpEvents(VNG *vng)
{
    // Wait for all events
    while (1) {
        VNGEvent event;
        VNGErrCode err = VNG_GetEvent(vng, &event, VNG_NOWAIT);
        if (err == VNG_OK) {
           fprintf(stdout, "Get event %d", event.eid);
           if (event.eid == VNG_EVENT_PEER_STATUS) {
               VNId vid;
               VN_GetVNId(event.evt.peerStatus.vn, &vid);
               fprintf(stdout, " -- 0x%08x.%08x.%08x %d",  (int)(vid.deviceId >> 32), (int)vid.deviceId, vid.netId, event.evt.peerStatus.memb);
           }    
           fprintf(stdout, "\n");
        } else if (err == VNGERR_NOWAIT || err == VNGERR_TIMEOUT)
           break;
    }
}

void dumpGameInfo(VNGGameStatus *s)
{
    VNGGameInfo *gi = &s->gameInfo;

    fprintf(stdout, "  num players %d\n", s->numPlayers);
    fprintf(stdout, "  game status %d\n", s->gameStatus);
    fprintf(stdout, "  vnId 0x%08x.%08x.%08x\n",  (int)(gi->vnId.deviceId >> 32), (int)gi->vnId.deviceId, gi->vnId.netId);
    fprintf(stdout, "  owner 0x%08x.%08x\n", (int) gi->owner >> 32, (int) gi->owner & ~0);
    fprintf(stdout, "  gameId %d\n", gi->gameId);
    fprintf(stdout, "  titleId %d\n", gi->titleId);
    fprintf(stdout, "  netZone %d\n", gi->netZone);
    fprintf(stdout, "  maxLatency %d\n", gi->maxLatency);
    fprintf(stdout, "  accessControl %d\n", gi->accessControl);
    fprintf(stdout, "  totalSlots %d\n", gi->totalSlots);
    fprintf(stdout, "  buddySlots %d\n", gi->buddySlots);
    fprintf(stdout, "  keyword \"%s\"\n", gi->keyword);
    fprintf(stdout, "  attrCount %d\n", gi->attrCount);
    for (int i = 0; i < gi->attrCount; i++) {
        fprintf(stdout, "  attr[%d] is %d\n", i, gi->gameAttr[i]);
    }
}

char* cmpTypes[] = {
   "VNG_CMP_DONTCARE",
   "VNG_CMP_STR",
   "VNG_CMP_EQ", 
   "VNG_CMP_NEQ", 
   "VNG_CMP_LT", 
   "VNG_CMP_GT", 
   "VNG_CMP_LE", 
   "VNG_CMP_GE", 
};

void dumpSearchCriteria(VNGSearchCriteria *c)
{
    fprintf(stdout, "Search Criteria:\n");
    fprintf(stdout, "  gameId %u\n", c->gameId);
    fprintf(stdout, "  domain %u\n", c->domain);
    fprintf(stdout, "  maxLatency %d\n", c->maxLatency);
    fprintf(stdout, "  cmpKeyword %u %s\n", c->cmpKeyword, cmpTypes[c->cmpKeyword]);
    if (c->cmpKeyword != VNG_CMP_DONTCARE) {
        fprintf(stdout, "  keyword \"%s\"\n", c->keyword);
    }
    for (int i = 0; i < VNG_MAX_PREDICATES; i++) {
        if (c->pred[i].cmp == VNG_CMP_DONTCARE) {
            continue;
        }
        fprintf(stdout, "  c->pred[%d].cmp is %u  %s\n",
                         i, c->pred[i].cmp, cmpTypes[c->pred[i].cmp]);
        fprintf(stdout, "  c->pred[%d].attrId is %u\n",
                         i, c->pred[i].attrId);
        fprintf(stdout, "  c->pred[%d].attrValue is %u\n",
                         i, c->pred[i].attrValue);
    }
}


int checkBuddyStatus (VNG                 *vng,
                      VNGUserId            buddy_uid,
                      VNGUserOnlineStatus  expectedOnlineStatus,
                      VNGGameId            expectedGameId,
                      VNId                 expectedVNId)
{
    int rv = 0;
    uint32_t i;
    VNGErrCode  ec;
    VNGUserInfo uinfo;
    VNGUserId   uid;
    uint32_t    membershipId;
    uint32_t    memberId;
    char       *loggedInText = "";
    VNGBuddyStatus bs[2];
    VNGUserInfo    buddyinfo[100];
    uint32_t       nbuddies = 100;

    if (buddy_uid == VNG_INVALID_USER_ID) {
        loggedInText = "Logged in ";
        uid = VNG_MyUserId (vng);
        if (uid == VNG_INVALID_USER_ID) {
            fprintf(stderr, "VNG_MyUserId returned VNG_INVALID_USER_ID\n");
            rv = -1;
            goto end;
        }
    } else {
        uid = buddy_uid;
    }

    fprintf(stderr, "Checking buddy status of %sVNGUserId %d\n",
                         loggedInText, uid);

    if ((ec = VNG_GetUserInfo (vng, uid, &uinfo, VNG_TIMEOUT))) {
        fprintf(stderr, "VNG_GetUserInfo returns %d for VNGUserId %d\n",
                            ec, uid);
        rv = -1;
        goto end;
    }

    fprintf(stderr, "VNGUserId %d login is %s\n", uid, uinfo.login);
    fprintf(stderr, "VNGUserId %d nickname is %s\n", uid, uinfo.nickname);

    bs[0].uid = uid;
    bs[1].uid = 0;
    if ((ec = VNG_GetBuddyStatus (vng, bs, 1, VNG_TIMEOUT))) {
        fprintf(stderr, "VNG_GetBuddyStatus returns %d\n", ec);
        rv = -1;
        goto end;
    }

    fprintf(stderr, "Buddy    VNGUserId %d"
                         "  onlineStatus %d   gameId %d   deviceId: 0x%llx   netId: 0x%x\n",
                         bs[0].uid,
                         bs[0].onlineStatus,  bs[0].gameId,
                         bs[0].vnId.deviceId, bs[0].vnId.netId);

    fprintf(stderr, "Expected VNGUserId %d"
                         "  onlineStatus %d   gameId %d   deviceId: 0x%llx   netId: 0x%x\n",
			 uid,
                         expectedOnlineStatus,  expectedGameId,
                         expectedVNId.deviceId, expectedVNId.netId);

    if (bs[0].uid != uid ||
        bs[0].onlineStatus != expectedOnlineStatus ||
        bs[0].gameId != expectedGameId ||
        bs[0].vnId.deviceId != expectedVNId.deviceId ||
        bs[0].vnId.netId != expectedVNId.netId) {

        fprintf(stderr, "VNG_GetBuddyStatus not as expeced\n");
        rv = -1;
        goto end;
    }

    if ((ec = VNG_GetBuddyList(vng, 0, buddyinfo, &nbuddies, VNG_TIMEOUT))) {
        fprintf(stderr, "VNG_GetBuddyList returns %d\n", ec);
        rv = -1;
        goto end;
    }

    if (!nbuddies) {
        fprintf(stderr, "VNGUserId %d has no buddies\n", uid);
    }
    else for (i = 0; i < nbuddies; i++) {
        VNGUserInfo *p = &buddyinfo[i];
        VNGUserId uid = p->uid;
        membershipId = (uint32_t) (uid >> 32);
        memberId     = (uint32_t) uid;
        fprintf(stderr, "buddy[%d] uid is 0x%08x.%08x\n", i, membershipId, memberId);
        fprintf(stderr, "login is %s\n", p->login);
        fprintf(stderr, "nickname is %s\n", p->nickname);
    }

end:
    return rv;
}

typedef struct {
    char *gi_keyword;
    char *search_pat;
} KwCmpCase;

KwCmpCase  cmpCases [] = {
    { "abxyccDarfg"                    , "Ab*cd?rf*"                       },
    { "aBcd"                           , "aBcd*"                           },
    { "abr"                            , "ab*r"                            },
    { "abcdcd"                         , "ab*?cd"                          },
    { "abcecdghcdefgefrgefeg"          , "Ab*cd*efR*eg"                    },
    { "@'abcdefghijklmnopqrstuvwxyz](0", "@'ABCDEFGHIJKLMNOPQRSTUVWXYZ](0" },
    { " Hello Dolly "                  , "*?ll*"                           },
    { " Hello Dolly "                  , " H* D* "                         },
    { " Hello Dolly "                  , "*He?lo*"                         },
    { " Hello Dolly "                  , "* Dolly *"                       },
    { " Hello Dolly "                  , "* Dolly*?"                       },
    { " Hello Dolly "                  , "?* D*ly?"                        },
    { " Hello Dolly "                  , " Hello Dolly "                   },
    { " Hello Dolly "                  , "*"                               },
    { " Hello Dolly "                  , NULL                              },
};

const int numCmpCases = sizeof cmpCases / sizeof cmpCases[0] ;


int testMatchMaking(const char *testname, VNDomain domain, int cmpKwCase)
{
    VNGErrCode err;
    int res = 0;
    VNG vng;
    VNId vid;

    fprintf(stdout, "\n*** LIBVNG %s TEST START cmKwCase %d\n",
                                  testname, cmpKwCase);

    VNG_Init(&vng, NULL);

    if (domain == VN_DOMAIN_INFRA) {
        err = VNG_Login(&vng, VNGS_HOST, VNGS_PORT,
                    TESTER01, TESTER01PW,
                    VNG_TIMEOUT);

        if (trace) {
            fprintf(stdout, "VNG_Login returns %d\n", err);
        }
        if (err != VNG_OK) {
            fprintf(stderr, "VNG_Login returns %d\n", err);
            res = -1;
            goto fini;
        }


        vid.deviceId = 0;
        vid.netId = 0;

        if ((err = VNG_UpdateStatus (&vng, VNG_STATUS_AWAY, 0, vid, VNG_TIMEOUT))) {
            fprintf (stderr, "VNG_UpdateStatus Failed %d\n", err);
            res = -1;
            goto fini;
        }

        fprintf (stderr, "Updated my buddy status\n");

        if (checkBuddyStatus (&vng, VNG_INVALID_USER_ID,
                                   VNG_STATUS_AWAY,
                                   0,
                                   vid)) {
            fprintf(stderr, "checkBuddyStatus Failed\n");
            res = -1;
            goto fini;
        }
    }

    VNGUserId uid = VNG_MyUserId(&vng);

    fprintf(stdout, "VNGUserId 0x%08x.%08x\n", (int)(uid >> 32), (int)uid);

    VNGUserInfo uinfo;
    err = VNG_GetUserInfo(&vng, uid, &uinfo, VNG_TIMEOUT);
    if (domain != VN_DOMAIN_INFRA) {
        if (err != VNGERR_NOT_LOGGED_IN) {
            fprintf(stderr, "VNG_GetUserInfo returns %d for adhoc domain\n", err);
            res = -1;
            goto fini;
        }
    }
    else if (err == VNG_OK) {
        fprintf(stdout, "login is %s\n", uinfo.login);
        fprintf(stdout, "nickname is %s\n", uinfo.nickname);
    } else {
        fprintf(stderr, "VNG_GetUserInfo returns %d\n", err);
        res = -1;
        goto fini;
    }


    VN vn;
    err = VNG_NewVN(&vng, &vn, VN_DEFAULT_CLASS, domain, VN_DEFAULT_POLICY);
    if (trace) {
        fprintf(stdout, "VNG_NewVN returns %d\n", err);
    }
    if (err) {
        fprintf(stderr, "VNG_NewVN returns %d\n", err);
        res = -1;
        goto fini;
    }

    err = VN_GetVNId(&vn, &vid);
    if (err) {
        fprintf(stdout, "VNG_GetVNId returns %d\n", err);
        res = -1;
        goto fini;
    }

    dumpVN(&vn);

    VNGGameInfo info;
    info.vnId = vid;
    info.gameId = 1011;
    info.titleId = 1;
    info.netZone = 0;
    info.maxLatency = 100;
    info.accessControl = VNG_GAME_PUBLIC;
    info.totalSlots = 4;
    info.buddySlots = 0;
    info.attrCount = 3;
    strncpy(info.keyword, cmpCases[cmpKwCase].gi_keyword, sizeof info.keyword);
    info.gameAttr[0] = 100;
    info.gameAttr[1] = 200;
    info.gameAttr[2] = 300;

    err = VNG_RegisterGame(&vng, &info, "Interesting Game", VNG_TIMEOUT);
    if (err) {
        fprintf(stdout, "VNG_RegisterGame returns %d\n", err);
        res = -1;
        goto fini;
    }

    VNGGameStatus gameStatus[100];
    gameStatus[0].gameInfo.vnId = vid;
    gameStatus[0].numPlayers = 45;
    gameStatus[0].gameStatus = 314310;

    err = VNG_GetGameStatus (&vng, gameStatus, 1, VNG_TIMEOUT);
    if (err) {
        fprintf(stdout, "VNG_GetGameStatus returns %d\n", err);
        res = -1;
        goto fini;
    }
    fprintf(stdout, "game status before updated:\n");
    dumpGameInfo(&gameStatus[0]);
    if (gameStatus[0].gameStatus != 0 || gameStatus[0].numPlayers != 0) {
        fprintf(stdout, "gameStatus[0].gameStatus != 0 || gameStatus[0].numPlayers != 0\n");
        res = -1;
        goto fini;
    }

    VNGSearchCriteria  crit;
    VNGSearchCriteria *criteria = &crit;

    if (domain == VN_DOMAIN_INFRA)
        crit.domain = VNG_SEARCH_INFRA;
    else
        crit.domain = VNG_SEARCH_ADHOC;

    crit.gameId = 1011;
    crit.maxLatency = 30000;
    crit.cmpKeyword = VNG_CMP_STR;
    if (cmpKwCase == (numCmpCases - 1)) {
        crit.cmpKeyword = VNG_CMP_DONTCARE;
    }
    else {
        strncpy(crit.keyword, cmpCases[cmpKwCase].search_pat,
                sizeof crit.keyword);
    }

    for (int i = 0; i < VNG_MAX_PREDICATES; i++) {
        crit.pred[i].cmp = VNG_CMP_DONTCARE;
    }

    crit.pred[0].attrId = 0;
    crit.pred[0].cmp = VNG_CMP_EQ;
    crit.pred[0].attrValue = 100;

    crit.pred[1].attrId = 1;
    crit.pred[1].cmp = VNG_CMP_GT;
    crit.pred[1].attrValue = 199;

    crit.pred[2].attrId = 1;
    crit.pred[2].cmp = VNG_CMP_LT;
    crit.pred[2].attrValue = 201;

    crit.pred[3].attrId = 2;
    crit.pred[3].cmp = VNG_CMP_GE;
    crit.pred[3].attrValue = 300;

    crit.pred[4].attrId = 2;
    crit.pred[4].cmp = VNG_CMP_LE;
    crit.pred[4].attrValue = 301;

    crit.pred[5].attrId = 0;
    crit.pred[5].cmp = VNG_CMP_NEQ;
    crit.pred[5].attrValue = 0;

    err = VNG_UpdateGameStatus (&vng, vid, 20050622, 7, VNG_TIMEOUT);
    if (err) {
        fprintf(stdout, "VNG_UpdateGameStatus returns %d\n", err);
        res = -1;
        goto fini;
    }

    dumpSearchCriteria(criteria);

    uint32_t n = 100;
    err = VNG_SearchGames(&vng, criteria, gameStatus, &n, 0, VNG_TIMEOUT);
    if (err) {
        fprintf(stdout, "VNG_SearchGames returns %d\n", err);
        res = -1;
        goto fini;
    }

    int match = 0;
    fprintf(stdout, "Found %d games:\n", n);
    for (int i = 0; i < (int) n; i++) {
        if (memcmp(&gameStatus[i].gameInfo.vnId, &vid, sizeof(VNId)) == 0
            || gameStatus[0].gameStatus != 20050622 || gameStatus[0].numPlayers != 7)
            match++;
        dumpGameInfo(&gameStatus[i]);
    }

    if (match != 1) {
        fprintf(stdout, "wrong number of matches %d\n", match);
        res = -1;
        goto fini;
    }

    err = VNG_UnregisterGame(&vng, vid, VNG_TIMEOUT);
    if (err) {
        fprintf(stdout, "VNG_UnregisterGame returns %d\n", err);
    }

    // dumpEvents(&vng);
     
    fprintf(stdout, "Entering delete VN\n");
    VNG_DeleteVN(&vng, &vn);

    // dumpEvents(&vng);

fini:
    fprintf(stdout, "Entering fini VN\n");
    VNG_Fini(&vng);

    return res;
}


int _tmain(int argc, _TCHAR* argv[])
{
    cout << "VNG MatchMaking Test" << endl;
    cout << "-----------------------" << endl;

    int res;

    const char *testname;

    for (int cmpKwCase = 0;  cmpKwCase < numCmpCases;  ++cmpKwCase) {

        testname = "MatchMaking infra";
        res = testMatchMaking(testname, VN_DOMAIN_INFRA, cmpKwCase);
        report(testname, res, cmpKwCase);
        if (res < 0) {
            break;
        }

        testname = "MatchMaking adhoc";
        res = testMatchMaking(testname, VN_DOMAIN_ADHOC, cmpKwCase);
        report(testname, res, cmpKwCase);
        if (res < 0) {
            break;
        }
    }
 
    return 0;
}


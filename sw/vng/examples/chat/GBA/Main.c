/********************************************************************/
/*      Main.c      Ver.1.05                                        */
/*          AGB Backup Library Sample Main                          */
/*                                                                  */
/*              Copyright (C) 1999-2003 NINTENDO Co.,Ltd.           */
/********************************************************************/
//#define CODE32
#include <Agb.h>
#include "mylib.h"
#include "Main.h"
#include <sc/ios.h>
#include <libc.h>
#include <fs.h>
#include <sc/mp3.h>

// define --------------------------------------
#define     BG0_SCBLK_1 20
#define     BG0_CHBLK_1 3

// extern --------------------------------------
extern void intr_main(void);
static int interrupts = 0;

extern const u16 mychar[(0x180*32)*8/16];
extern const u16 textPaletteData[16][16];

// function's prototype ------------------------
void InitMainSequence(u8 c_p);
void MainSequence();
void VblankIntr();
void CassetteIntr();
void IntrDummy();

// const data  ---------------------------------
static const pos_obj posListMn[]={  {0x0001*8,0x0003*8},
                                    {0x0001*8,0x0004*8},
                                    {0x0001*8,0x0005*8},
                                    {0x0001*8,0x0006*8},
                                    {0x0001*8,0x0007*8},
                                    {0x0001*8,0x0008*8},
                                    {0x0001*8,0x0009*8},
                                    {0x0001*8,0x000a*8},
                                    {0x0001*8,0x000b*8},
                                    {0x0001*8,0x000c*8},
                                    {0x0001*8,0x000d*8},
                                    {0x0001*8,0x000e*8},
                                    {0x0001*8,0x000f*8},
                                    {0x000e*8,0x0003*8},
                                    {0x000e*8,0x0004*8},
                                    {0x000e*8,0x0005*8},
                                    {0x000e*8,0x0006*8},
                                    {0x000e*8,0x0007*8},
                                    {0x000e*8,0x0008*8},
                                    {0x000e*8,0x0009*8},
                                    {0x000e*8,0x000a*8},
                                    {0x000e*8,0x000b*8},
                                    {0x000e*8,0x000c*8},
                                    {0x000e*8,0x000d*8},
                                    {0x000e*8,0x000e*8},
                                    {0x000e*8,0x000f*8},
                                    };
static const u16 charListMn[]={     0x00a0,0x00a1,0x00a2,0x00a1};
static const csrDefine csr0={       posListMn,
                                    charListMn,
                                    26,
                                    127,
                                    OBJ_PLTT_WHITE,
                                    };

// static variable -----------------------------
static  u32 IntrMainBuf[0x200];         // Interrupt Main Routine Buffer
static  u16 Bg0Bak[32*32];              // BG0 Buffer
static  u16 OamBak[4*128];          // OAM Buffer
static  u8  numMain;                // Hold main sequence cursor position

// global variable -----------------------------
void (*IntrTable[13])();            // Interrupt table
/*  * From crt0.s
     0: V-Blank interrupt
     1: H-Blank interrupt
     2: V-Counter interrupt
     3: Timer 0 interrupt
     4: Timer 1 interrupt
     5: Timer 2 interrupt
     6: Timer 3 interrupt
     7: Serial communication interrupt
     8: DMA0 interrupt
     9: DMA1 interrupt
    10: DMA2 interrupt
    11: DMA3 interrupt
    12: Key interrupt
*/
void (*ActiveProcess)();

/*==================================================================*/
/*                      Main Routine                                */
/*==================================================================*/

void AgbMain(void)
{
    u32 i;
    
    RegisterRamReset(RESET_EX_WRAM_FLAG);           // Clear CPU external RAM
    CpuClear(0x00, (u16 *)0x03000000, 0x7c00, 32);  // Clear CPU internal RAM
    
    *(vu16 *)REG_WAITCNT=CST_PREFETCH_ENABLE                        // Pre-fetch ON
                        | CST_ROM0_1ST_3WAIT | CST_ROM0_2ND_1WAIT;  // Set 3-1 wait access 
    
    DmaCopy(3, intr_main, IntrMainBuf, sizeof(IntrMainBuf), 16);    // Set interrupt main routine
    *(vu32 *)INTR_VECTOR_BUF=(vu32)IntrMainBuf;
    
    // Initialize interrupt table
    for(i=0;i<13;i++)                       
        IntrTable[i]=IntrDummy;
    IntrTable[0]=CassetteIntr;
    IntrTable[1]=VblankIntr;
    
    // Initialize funtions you made (No functions in library)
    InitBg();
    
    // Set OBJ
    SetOamBuffer(OamBak,sizeof(OamBak));
    TrmCharData(selObj,mychar,sizeof(mychar),0);
    TrmPaletteData(selObj,(u16 *)textPaletteData,8,8);
    // Set BG
    *(vu16 *)REG_BG0CNT= BG_SCREEN_SIZE_0
                        |BG0_SCBLK_1<<BG_SCREEN_BASE_SHIFT
                        |BG_COLOR_16
                        |BG_MOS_ON
                        |BG0_CHBLK_1<<BG_CHAR_BASE_SHIFT
                        |BG_PRIORITY_0;
    SetBgxBuffer(0,Bg0Bak,sizeof(Bg0Bak));
    TrmCharData(selBg,mychar,sizeof(mychar),BG0_CHBLK_1);
    TrmPaletteData(selBg,(u16 *)textPaletteData,8,8);
    
    // DISP ON
    *(vu16 *)REG_DISPCNT=DISP_MODE_0 | DISP_BG0_ON | DISP_OBJ_ON;
    
    // Set interrupt
    *(vu16 *)REG_IME=1;     // Set IME
    //*(vu16 *)REG_IE=V_BLANK_INTR_FLAG | CASSETTE_INTR_FLAG; 
    *(vu16 *)REG_IE=V_BLANK_INTR_FLAG; 
                        // Allow V-Blank & Game Pak interrupt
    *(vu16 *)REG_STAT=STAT_V_BLANK_IF_ENABLE;
        
    ////////////////////////////////////////
    // MAIN LOOP
    ////////////////////////////////////////

    // Set main loop process in main sequence 
    InitMainSequence(0);
    ActiveProcess=MainSequence;
    
    while(1) {
        VBlankIntrWait();
        ReadKeyPlus();
        ActiveProcess();
    }
}

u32 count = 0;

void InitMainSequence(u8 c_p)
{
    *(vu16 *)REG_DISPCNT|=DISP_OBJ_ON;      // OBJ on
    
    if(c_p==0)
        numMain=0;
    SetCursorStatus(&csr0,numMain);         // Set cursor data
    
    ClearBgxBuffer(0,0x8000);               // Clear BG0 buffer
    DrawAsciiStringOnBgx(0,0x0021,BG_PLTT_BLUE,"Voice Chat");

}

char buf[32];
void DrawDisplay(void)
{
    snprintf(buf, sizeof(buf), "%x            ", count);
    DrawAsciiStringOnBgx(0,0x002c,BG_PLTT_YELLOW,buf);
}

void MainSequence()
{
    count++;
    
    DrawDisplay();

}


/*==================================================================*/
/*              Interrupt routine                                   */
/*==================================================================*/
static int cart = 1;

void CassetteIntr()
{
    cart = 0;
    DrawAsciiStringOnBgx(0,0x01c3,BG_PLTT_RED,"CASSETTE");
    *(vu16 *)REG_IF=CASSETTE_INTR_FLAG; 
}

void VblankIntr()
{
    if (interrupts++ == 60) {
        interrupts = 0;
    }

    //DrawHexOnBgx(0,0x0220,BG_PLTT_WHITE,&interrupts,2);
    DmaAllBgxBuffer();
    DmaOamBuffer();
//    *(vu16 *)REG_IF=V_BLANK_INTR_FLAG; 
}

void IntrDummy()
{
}

/********************************************************************/
/*      Main.h                                                      */
/*          Library Header Include and                              */
/*         Global Symbol External Declaration                       */
/*                                                                  */
/*              Copyright (C) 1999-2003 NINTENDO Co.,Ltd.           */
/********************************************************************/
#ifndef	__MAIN_H__
#define	__MAIN_H__

#define	COMMON_BUFFER		0x02010000

extern void (*IntrTable[13])();
extern void (*ActiveProcess)();
extern void ReturnMain();

#endif	/* __MAIN_H__ */

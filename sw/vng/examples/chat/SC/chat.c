#ifdef _WIN
 #include <stdio.h>
#endif
#ifdef _SC
 #include <ioslibc.h>
#endif

#include "vng.h"
#include "shr_th.h"
#include "testcfg.h"

/* configure stack size, priority, etc., for this process */
#define STACK_SIZE (16*1024)
const uint8_t _initStack[STACK_SIZE];
const uint32_t _initStackSize = sizeof(_initStack);
const uint32_t _initPriority = 10;

#define GAME_ID   12345

VNG vng;
VN vn;
VNId vnid;

int SetupVNG(void);
int hostNewGame();
int searchAndJoinGame();

int SetupVNG(void)
{
    VNGErrCode ec;
    uint32_t   cstat;
    uint32_t   expected_cstat_bits = (VNG_ST_USB_CONNECTED   |  
                                      VNG_ST_PROXY_CONNECTED |
                                      VNG_ST_GATEWAY_DEFINED |
                                      VNG_ST_VNG_INITIALIZED);
    if ((ec = VNG_Init(&vng, NULL))) {
        return -1;
    }

    cstat = VNG_ConnectStatus(&vng);

    if ((cstat & expected_cstat_bits) != expected_cstat_bits) {
        VNG_Fini(&vng);
        return -1;
    }

    if ((ec = VNG_Login (&vng, VNGS_HOST, VNGS_PORT, TESTER01, TESTER01PW, VNG_TIMEOUT))) {
        VNG_Fini(&vng);
        return -1;
    }

    return 0;
}

int hostNewGame()
{
    VNGErrCode err;
    VNGGameInfo info;

    // Create a new VN
    err = VNG_NewVN(&vng, &vn, VN_DEFAULT_CLASS, VN_DOMAIN_INFRA, VN_ABORT_ON_ANY_EXIT);
    if (err != VNG_OK)
        return -1;

    // Describe the game information for matchmaking
    memset((void*)&info, 0, sizeof(VNGGameInfo));
    VN_GetVNId(&vn, &info.vnId);
    info.owner = VNG_MyUserId(&vng);
    info.gameId = GAME_ID;
    info.titleId = GAME_ID;
    info.accessControl = VNG_GAME_PUBLIC;
    info.totalSlots = 2;
    info.buddySlots = 0;   // nothing reserved for buddies
    info.maxLatency = 500; // 500ms RTT 
    info.netZone = 0;      // server will decide this value
    info.attrCount = 0;
    strncpy(info.keyword, "eli's vng test!",sizeof(info.keyword));

    // Register the VN to the directory for matchmaking
    err = VNG_RegisterGame(&vng, &info, NULL, VNG_TIMEOUT);
    if (err != VNG_OK)
        return -1;

    vnid = info.vnId;

    return 0;
}

int waitForPlayers()
{
    VNGUserInfo userInfo;
    uint64_t requestId;
    VNGErrCode err;

    err = VNG_GetJoinRequest(&vng, vnid, NULL, 
        &requestId, &userInfo, NULL, 0, VNG_WAIT);
    if (err == VNG_OK) {
        printf("got join request!\n");
        VNG_AcceptJoinRequest(&vng, requestId);
    }

    return 0;
}

int searchAndJoinGame()
{
    VNGErrCode err;
    VNGSearchCriteria criteria;
    VNGGameStatus gameStatus[100];
    int numGameStatus;
    int i;
    char joinStr[16];

    memset((void*)&criteria, 0, sizeof(VNGSearchCriteria));

    criteria.gameId = GAME_ID;   
    criteria.domain = VNG_SEARCH_INFRA;   
    criteria.maxLatency = 500;  // 300ms RTT
    criteria.cmpKeyword = VNG_CMP_DONTCARE;

    strncpy(joinStr, "hello", sizeof(joinStr));

    numGameStatus = 100;
    VNG_SearchGames(&vng, &criteria, gameStatus, &numGameStatus, 0, VNG_TIMEOUT);

    if (numGameStatus > 0) {
        for (i = 0; i < numGameStatus; i++) {
            err = VNG_JoinVN(&vng,
                gameStatus[i].gameInfo.vnId,
                joinStr,
                &vn,
                NULL,
                0, 
                10*1000);
            if (err == VNG_OK) return 0;
        }
    }

    return -1;
}

int main(void)
{
    int rv;
    
    printf("chat: host=%s\n",VNGS_HOST);

    if ( (rv = SetupVNG()) == 0) {
        printf("attempting to join existing game\n");
        if (searchAndJoinGame() == 0) {
            printf("existing game found and joined\n");
        } else {
            printf("no existing game found, try hosting a new game\n");
            if ( (rv = hostNewGame()) == 0 ) {
                printf("hosting a new game succeeded\n");
            } else {
                printf("hosting a new game also failed\n");//debug
                goto out;
            }
        }
    } else {
        printf("SetupVNG() failed\n");
        goto out;
    }

    printf("activating voice chat\n");
    
    VNG_ActivateVoice(&vn, VNG_VOICE_ENABLE, VN_MEMBER_OTHERS);
    VNG_AdjustVolume(&vng, VNG_VOL_SET, 0x68);

    while (1) {
        if ((VN_Owner(&vn) == VN_Self(&vn))
            && VN_NumMembers(&vn) < 2) 
        {
            printf("waitForPlayers()...\n");
            waitForPlayers();
        } else {
            _SHR_thread_sleep(5000);
        }
    }

out:
    printf("exiting\n");
    return 0;
}

/********************************************************************/
/*          types.h                                                 */
/*            Type declaration                                      */
/*                                                                  */
/*          Copyright (C) 1999-2001 NINTENDO Co.,Ltd.               */
/********************************************************************/
#ifndef _TYPES_H
#define _TYPES_H

#include <Agb.h>


typedef void (*VoidFuncp)(void);
typedef void (*IntrFuncp)(void);


#endif /* _TYPES_H */

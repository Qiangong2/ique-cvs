/*********************************************************************/
/*          MultiSio.c                                               */
/*            Multi-play communication library                       */
/*(Timer interrupt transmission & communication data synchronization)*/
/*             *exclusively for multiple interrupts)                 */
/*                                                                   */
/*          Copyright (C) 1999-2002 NINTENDO Co.,Ltd.                */
/*********************************************************************/
//#define CODE32
#include "MultiSio.h"


/*-------------------- Global variable  ----------------------------*/

MultiSioArea     Ms;            // Multi-play communication work area

#ifdef MULTI_SIO_DI_FUNC_FAST
u32 RecvFuncBuf[0x40/4];        // Program buffer for updating receive data bufffer 
u32 IntrFuncBuf[0x200/4];       // Interrupt routine RAM execute buffer
                                // (If -O2 is specified, the size can be reduced to 0x120 Bytes.)
#endif

static const u8 MultiSioLib_Var[]="MultiSioSync020820";

printLCE()
{
    lce_type* lce = getLCEDataArea();

    *(vu16 *)REG_IME = 0;
    printf("reg_ie_if| %x\n",lce->reg_ie_if);
    printf("emulated_intr| %d\n",lce->emulated_intr);
    printf("frame_counter| %d\n",lce->frame_counter);
    printf("timer_counter| %d\n",lce->timer_counter);
    printf("frame_counter_limit| %d\n",lce->frame_counter_limit);
    printf("timer_counter_limit| %d\n",lce->timer_counter_limit);
    printf("frame_intr_skipped| %d\n",lce->frame_intr_skipped);
    printf("timer_intr_skipped| %d\n",lce->timer_intr_skipped);
    printf("vcount_intr_skipped| %d\n",lce->vcount_intr_skipped);
    printf("mode| %d\n",lce->mode);
    printf("delay| %d\n",lce->delay);
    printf("siomulti0| %x\n",lce->siomulti0);
    printf("siomulti1| %x\n",lce->siomulti1);
    printf("siomulti2| %x\n",lce->siomulti2);
    printf("siomulti3| %x\n",lce->siomulti3);
    printf("siocnt| %x\n",lce->siocnt);
    printf("siomulti_send| %x\n",lce->siomulti_send);
    printf("enable| %d\n",lce->enable);
    printf("linkid| %d\n",lce->linkid);
    printf("main_loop_done| %d\n",lce->main_loop_done);
    printf("game_mode| %d\n",lce->game_mode);
    /* private */
    printf("tmp| %d\n",lce->tmp);
    printf("tmp2| %d\n",lce->tmp2);
    *(vu16 *)REG_IME = 1;
}

/*------------------------------------------------------------------*/
/*                      Initialize multi-play communication         */
/*------------------------------------------------------------------*/
extern u32 MultiSioRecvBufChange(void);

void MultiSioInit(void)
{
    int     i;

    *(vu16 *)REG_IME = 0;
    *(vu16 *)REG_IE &= ~(SIO_INTR_FLAG                  // Disable SIO & timer interrupt 
                       | MULTI_SIO_TIMER_INTR_FLAG);
    *(vu16 *)REG_IME = 1;

    *(vu16 *)REG_RCNT  = R_SIO_MASTER_MODE;
    //*(vu16 *)REG_SIOCNT  = SIO_MULTI_MODE;
    getLCEDataArea()->siocnt = SIO_MULTI_MODE;
    //*(vu16 *)REG_SIOCNT |= SIO_IF_ENABLE | MULTI_SIO_BAUD_RATE_NO;
    getLCEDataArea()->siocnt |= SIO_IF_ENABLE | MULTI_SIO_BAUD_RATE_NO;
    getLCEDataArea()->siocnt &= 0xffbb;
    getLCEDataArea()->siocnt |= (getLCEDataArea()->linkid ? 0xc : 8);

    CpuClear(0, &Ms, sizeof(Ms), 32);                   // Clear multi-play communication work area 

#ifdef MULTI_SIO_DI_FUNC_FAST                           // Copy of function
    CpuCopy(MultiSioRecvBufChange, RecvFuncBuf, sizeof(RecvFuncBuf), 32);
    CpuCopy(MultiSioIntr,          IntrFuncBuf, sizeof(IntrFuncBuf), 32);
#endif

    Ms.SendBufCounter = sizeof(MultiSioPacket)/2;       // Initialize buffer counter
    Ms.RecvBufCounter = sizeof(MultiSioPacket)/2;

    Ms.NextSendBufp    = (u16 *)&Ms.SendBuf[0];         // Set send buffer pointer 
    Ms.CurrentSendBufp = (u16 *)&Ms.SendBuf[1];

    Ms.CurrentRecvBufp = (u16 *)&Ms.RecvBuf[0][0];      // Set receive buffer pointer 
    Ms.LastRecvBufp    = (u16 *)&Ms.RecvBuf[1][0];
    Ms.RecvCheckBufp   = (u16 *)&Ms.RecvBuf[2][0];

    *(vu16 *)REG_IME = 0;
    *(vu16 *)REG_IE |= SIO_INTR_FLAG;                   // Permit SIO interrupt 
    *(vu16 *)REG_IME = 1;

    printLCE();
    LCE_SetFrameLen(11);
    if (getLCEDataArea()->linkid) {
        LCE_Enable();
    }
}


/*------------------------------------------------------------------*/
/*                      Start multi-play communication              */
/*------------------------------------------------------------------*/

void MultiSioStart(void)
{
    if (Ms.Type) {
        Ms.StartFlag = 1;                   // Set start flag
        LCE_SwitchMode(LCE_TS_SYNC_MODE);
        getLCEDataArea()->timer_counter_limit = 11;
        LCE_Enable();
    }
}

/*------------------------------------------------------------------*/
/*                      Stop multi-play communication               */
/*------------------------------------------------------------------*/

void MultiSioStop(void)
{
    *(vu16 *)REG_IME = 0;
    *(vu16 *)REG_IE &= ~(SIO_INTR_FLAG                  // Disable SIO & timer interrupt 
                       | MULTI_SIO_TIMER_INTR_FLAG);
    *(vu16 *)REG_IME = 1;

    //*(vu16 *)REG_SIOCNT = SIO_MULTI_MODE                // Stop SIO 
    //                    | MULTI_SIO_BAUD_RATE_NO;
    getLCEDataArea()->siocnt = SIO_MULTI_MODE                // Stop SIO 
        | MULTI_SIO_BAUD_RATE_NO;

    *(vu32 *)REG_MULTI_SIO_TIMER                        // Stop timer
                        = MULTI_SIO_TIMER_COUNT;

    *(vu16 *)REG_IF = SIO_INTR_FLAG                     // Reset IF 
                    | MULTI_SIO_TIMER_INTR_FLAG;

    Ms.StartFlag = 0;                                   // Reset start flag 

    LCE_Disable();
}


/*------------------------------------------------------------------*/
/*                      Multi-play communication main               */
/*------------------------------------------------------------------*/

u32 MultiSioMain(void *Recvp)
{
    SioMultiCnt     SioCntBak;
    int             i, ii;
    SioCntBak = *(SioMultiCnt *)&getLCEDataArea()->siocnt;

    switch (Ms.State) {
        case 0: if (!SioCntBak.ID) {         // Check connection
                    
                    if (!SioCntBak.SD || SioCntBak.Enable)    break;
                    if (!SioCntBak.SI && Ms.SendBufCounter == sizeof(MultiSioPacket)/2) {
                        *(vu16 *)REG_IME = 0;
                        *(vu16 *)REG_IE &= ~SIO_INTR_FLAG;              // Disable SIO interrupt 
                        *(vu16 *)REG_IE |=  MULTI_SIO_TIMER_INTR_FLAG;  // Permit timer interrupt 
                        *(vu16 *)REG_IME = 1;

                        //((SioMultiCnt *)REG_SIOCNT)->IF_Enable = 0;     // Reset SIO-IFE 
                        getLCEDataArea()->siocnt &= 0xbfff; // Reset SIO-IFE 

                        *(vu32 *)REG_MULTI_SIO_TIMER                    // Initialize timer 
                                         = MULTI_SIO_TIMER_COUNT;

                        *(vu16 *)REG_IF  =  SIO_INTR_FLAG               // Reset IF  
                                         |  MULTI_SIO_TIMER_INTR_FLAG;

                        Ms.Type = SIO_MULTI_PARENT;
                    }
                }
                Ms.State = 1;
        case 1: if (Ms.ConnectedFlags)       // Waiting period to stabilize initialization
                    if (Ms.RecvFlagsAvailableCounter < 8)
                        Ms.RecvFlagsAvailableCounter++;
                    else    Ms.State = 2;
        case 2: MultiSioRecvDataCheck(Recvp); // Check receiving data
                break;
    }

    Ms.SendFrameCounter++;

    return      Ms.RecvSuccessFlags
              | (Ms.Type == SIO_MULTI_PARENT) << 7
              | Ms.ConnectedFlags << 8
              | (Ms.HardError != 0) << 12
              | (SioCntBak.ID >= MULTI_SIO_PLAYERS_MAX) << 13
              | (Ms.RecvFlagsAvailableCounter >> 3) << 15;
}


/*------------------------------------------------------------------*/
/*                      Set send data                               */
/*------------------------------------------------------------------*/

void MultiSioSendDataSet(void *Sendp)
{
    s32     CheckSum = 0;
    int     i;

    ((MultiSioPacket *)Ms.NextSendBufp)->FrameCounter = (u8 )Ms.SendFrameCounter;
    ((MultiSioPacket *)Ms.NextSendBufp)->RecvErrorFlags =  Ms.ConnectedFlags ^ Ms.RecvSuccessFlags;
    ((MultiSioPacket *)Ms.NextSendBufp)->CheckSum = 0;

    CpuCopy(Sendp, (u8 *)&Ms.NextSendBufp[2], MULTI_SIO_BLOCK_SIZE, 32);  // Set send data

    for (i=0; i<sizeof(MultiSioPacket)/2 - 2; i++)      // Calculate check sum for send data 
        CheckSum += Ms.NextSendBufp[i];
    ((MultiSioPacket *)Ms.NextSendBufp)->CheckSum = ~CheckSum - sizeof(MultiSioPacket)/2;

    Ms.SyncSendFlag = 1;
}

/*------------------------------------------------------------------*/
/*                      Check receive data                          */
/*------------------------------------------------------------------*/

u32 MultiSioRecvDataCheck(void *Recvp)
{
#ifdef MULTI_SIO_DI_FUNC_FAST                           // Update receive data/check buffer 
    u32 (*MultiSioRecvBufChangeOnRam)(void) = (u32 (*)(void))RecvFuncBuf;
#endif
    s32      CheckSum;
    vu32     RecvCheck = 0;
    u8       SyncRecvFlagBak;
    u8       CounterDiff;
    u16     *BufpTmp;
    int      i, ii;


#ifdef MULTI_SIO_DI_FUNC_FAST                           // Update receive data/check buffer 
    SyncRecvFlagBak = MultiSioRecvBufChangeOnRam();
#else
    *(vu16 *)REG_IME = 0;                               // Disable interrupt (30 clocks approx.) 

    BufpTmp = Ms.RecvCheckBufp;                         // Update receive data/check buffer 
    Ms.RecvCheckBufp = Ms.LastRecvBufp;
    Ms.LastRecvBufp = BufpTmp;

    SyncRecvFlagBak = Ms.SyncRecvFlag;                  // Copy receive verify flag 
    Ms.SyncRecvFlag = 0;

    *(vu16 *)REG_IME = 1;                               // Permit interrupt
#endif

    Ms.RecvSuccessFlags = 0;

    if (SyncRecvFlagBak) {                              // Verify if receive succeeded
        for (i=0; i<MULTI_SIO_PLAYERS_MAX; i++) {
            BufpTmp = Ms.RecvCheckBufp + i * sizeof(MultiSioPacket)/2;
            CheckSum = 0;                               // Calculate check sum for receive data 
            for (ii=0; ii<sizeof(MultiSioPacket)/2 - 2; ii++)
                CheckSum +=  BufpTmp[ii];

            if ((s16 )CheckSum == (s16 )(-1 - sizeof(MultiSioPacket)/2)) {
                CpuCopy(&((u8 *)BufpTmp)[4], &((u8 *)Recvp)[i*MULTI_SIO_BLOCK_SIZE],
                        MULTI_SIO_BLOCK_SIZE, 32);
                Ms.RecvSuccessFlags |= 1 << i;
            }

            CpuClear(0, &((u8 *)BufpTmp)[4], MULTI_SIO_BLOCK_SIZE, 32);
        }
    }

    Ms.ConnectedFlags |= Ms.RecvSuccessFlags;           // Set connection end flag 

    return Ms.RecvSuccessFlags;
}


/*==================================================================*/
/*              Multi-play communication V-blank interrupt process  */
/*==================================================================*/

void MultiSioVSync(void)
{
    int i;
    u16     *BufpTmp;

    if (Ms.Type) {                                  // Master
        if (Ms.SyncSendFlag && Ms.State && Ms.StartFlag) {
            Ms.RecvBufCounter = -1;

            BufpTmp = Ms.LastRecvBufp;                  // Change receive buffer 
            Ms.LastRecvBufp = Ms.CurrentRecvBufp;
            Ms.CurrentRecvBufp = BufpTmp;

            BufpTmp = Ms.CurrentSendBufp;               // Change send buffer 
            Ms.CurrentSendBufp = Ms.NextSendBufp;
            Ms.NextSendBufp = BufpTmp;
            Ms.SyncSendFlag = 0;
            Ms.SendBufCounter = 0;

            //Ms.HardError = ((SioMultiCnt *)REG_SIOCNT)->Error;
            Ms.HardError = ((SioMultiCnt *)&getLCEDataArea()->siocnt)->Error;
                                                        // Detect hardware error 

            ((SioMultiCnt *)REG_SIOCNT)->Data = MULTI_SIO_SYNC_DATA;
                                                        // Set synchronous data 

            //*(vu16 *)REG_SIOCNT |= SIO_ENABLE;          // Start communication 
            LCE_Trigger();

            *(vu16 *)REG_MULTI_SIO_TIMER_H              // Start timer
                                 = (TMR_PRESCALER_1CK
                                 |  TMR_IF_ENABLE | TMR_ENABLE) >> 16;
            }
    } else {                                        // Slave
        if (Ms.TimeOutCounter < MULTI_SIO_TIMEOUT_FRAMES) {
            Ms.TimeOutCounter++;                        // Check communication time-out
        } else {
            *(vu16 *)REG_IME = 0;
            *(vu16 *)INTR_CHECK_BUF |= SIO_INTR_FLAG;   // Set SIO interrupt check flag
            *(vu16 *)REG_IME = 1;
        }
    }
}


/*==================================================================*/
/*                  Multi-play communication interrupt routine      */
/*==================================================================*/

void MultiSioIntr(void)
{
    u16      RecvTmp[4];
    u16     *BufpTmp;
    int     i, ii;


    // Save receive data 

    *(u64 *)RecvTmp = *(u64 *)&getLCEDataArea()->siomulti0;


    // Detect hard error

    //Ms.HardError = ((SioMultiCnt *)REG_SIOCNT)->Error;
    Ms.HardError = ((SioMultiCnt *)&getLCEDataArea()->siocnt)->Error;


    // Switch buffer

    if (RecvTmp[0] == MULTI_SIO_SYNC_DATA
     && Ms.RecvBufCounter > (s32 )(sizeof(MultiSioPacket)/2 - 3)) {
        Ms.RecvBufCounter = -1;

        BufpTmp = Ms.LastRecvBufp;                      // Change receive buffer 
        Ms.LastRecvBufp = Ms.CurrentRecvBufp;
        Ms.CurrentRecvBufp = BufpTmp;

        if (Ms.SyncSendFlag) {
            BufpTmp = Ms.CurrentSendBufp;               // Change send buffer
            Ms.CurrentSendBufp = Ms.NextSendBufp;
            Ms.NextSendBufp = BufpTmp;
            Ms.SyncSendFlag = 0;
            Ms.SendBufCounter = 0;
        }

        *(vu16 *)REG_IME = 0;
        *(vu16 *)INTR_CHECK_BUF |= SIO_INTR_FLAG;       // Set SIO interrupt check flag 
        *(vu16 *)REG_IME = 1;
    }


    // Send data process 

    if (Ms.SendBufCounter <= (s32 )(sizeof(MultiSioPacket)/2 - 3))
        ((SioMultiCnt *)REG_SIOCNT)->Data = Ms.CurrentSendBufp[Ms.SendBufCounter];
                                                        // Set send data 
    if (Ms.SendBufCounter < (s32 )(sizeof(MultiSioPacket)/2 - 1))   Ms.SendBufCounter++;


    // Receive data process 

    if (Ms.RecvBufCounter >= 0) {
        for (i=0; i<MULTI_SIO_PLAYERS_MAX; i++)
            (Ms.CurrentRecvBufp + i * sizeof(MultiSioPacket)/2)[Ms.RecvBufCounter] = RecvTmp[i];
                                                        // Store receive data 
        if (Ms.RecvBufCounter == (s32 )(sizeof(MultiSioPacket)/2 - 3)) {
            Ms.SyncRecvFlag = 1;                        // Receive end
        }
    }
    if (Ms.RecvBufCounter < (s32 )(sizeof(MultiSioPacket)/2 - 1))   Ms.RecvBufCounter++;


    // Start master send 

    if (Ms.Type)
        *(vu16 *)REG_MULTI_SIO_TIMER_H = 0;             // Stop timer 

    if (Ms.SendBufCounter < (s32 )(sizeof(MultiSioPacket)/2 - 1)) {
        if (Ms.Type) {
            //*(vu16 *)REG_SIOCNT |= SIO_ENABLE;          // Resume send 
            LCE_Trigger();
            *(vu16 *)REG_MULTI_SIO_TIMER_H              // Resume timer
                                 = (TMR_PRESCALER_1CK
                                 |  TMR_IF_ENABLE | TMR_ENABLE) >> 16;
        }
    }

    // Clear communication time-out counter

    Ms.TimeOutCounter = 0;
}



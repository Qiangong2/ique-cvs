/* This file contains the configuration used by 
   examples programs.  Modify VNGS_HOST to 
   access your own instance of VNG server */

/* Use beta test env */
/* For accessing beta network from outside broadon  */
// #define VNGS_HOST "ogs.idc-beta.broadon.com"
/* For accessing beta network from within broadon */
// #define VNGS_HOST "ogs.idc-beta.vpn.broadon.com"
/* For accessing lab1 network */
#define VNGS_HOST "ogs.bbu.lab1.routefree.com"
// #define VNGS_HOST    "ogs.idc-beta.broadon.com"

/* Dedicated port for VNG server - do not change */
#define VNGS_PORT 16978

/* Use default 10 seconds timeout for blocking VNG calls */
#define VNG_TIMEOUT  10000

/* Tester account */
#define TESTER01   "10001"

/* Tester password */
#define TESTER01PW   "10001"

/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */

#ifndef __NC_API_H__
#define __NC_API_H__

#ifdef __cplusplus
extern "C" {
#endif

extern void
api_init(void* handle);

#ifdef __cplusplus
}
#endif
#endif /* __NC_API_H__ */

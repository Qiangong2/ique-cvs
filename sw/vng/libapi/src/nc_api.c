/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */

#include "sk.h"

#ifdef _SC

typedef void* (*HANDLE)(u16);

extern void api_init_vng(HANDLE get_addr);
extern void api_init_shr(HANDLE get_addr);

void
api_init(void* handle)
{
    api_init_vng((HANDLE)handle);
    api_init_shr((HANDLE)handle);
}
#endif /* _SC */


#ifdef _GBA

extern void api_init_vng(u16*);
extern void api_init_shr(u16*);

void
api_init(void* handle)
{
    api_init_vng((u16*) handle);
    api_init_shr((u16*) handle);
}

#endif /* _GBA */

/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */

#include "sk.h"

#include "shr_trace.h"
#include "nc_func_id.h"

/* _SHR_trace_init */
typedef void (*_SHR_trace_init_t)(void);
static _SHR_trace_init_t _SHR_trace_init_p = NULL;

void
_SHR_trace_init(void)
{
    (*_SHR_trace_init_p)();
} 


/* _SHR_set_trace_enable */
typedef int (*_SHR_set_trace_enable_t)(int);
static _SHR_set_trace_enable_t _SHR_set_trace_enable_p = NULL;

int
_SHR_set_trace_enable(int enable)
{
    return (*_SHR_set_trace_enable_p)(enable);
} 


/* _SHR_set_trace_level */
typedef int (*_SHR_set_trace_level_t)(int);
static _SHR_set_trace_level_t _SHR_set_trace_level_p = NULL;

int
_SHR_set_trace_level(int level)
{
    return (*_SHR_set_trace_level_p)(level);
} 


/* _SHR_set_trace_show_time */
typedef int (*_SHR_set_trace_show_time_t)(int);
static _SHR_set_trace_show_time_t _SHR_set_trace_show_time_p = NULL;

int
_SHR_set_trace_show_time(int show_time)
{
    return (*_SHR_set_trace_show_time_p)(show_time);
} 


/* _SHR_set_trace_show_thread */
typedef int (*_SHR_set_trace_show_thread_t)(int);
static _SHR_set_trace_show_thread_t _SHR_set_trace_show_thread_p = NULL;

int
_SHR_set_trace_show_thread(int show_thread)
{
    return (*_SHR_set_trace_show_thread_p)(show_thread);
}


/* _SHR_set_grp_trace_enable */
typedef int (*_SHR_set_grp_trace_enable_t)(int, int);
static _SHR_set_grp_trace_enable_t _SHR_set_grp_trace_enable_p = NULL;

int
_SHR_set_grp_trace_enable(int grp, int enable)
{
    return (*_SHR_set_grp_trace_enable_p)(grp, enable);
} 


/* _SHR_set_grp_trace_level */
typedef int (*_SHR_set_grp_trace_level_t)(int, int);
static _SHR_set_grp_trace_level_t _SHR_set_grp_trace_level_p = NULL;

int
_SHR_set_grp_trace_level(int grp, int level)
{
    return (*_SHR_set_grp_trace_level_p)(grp, level);
} 


/* _SHR_set_sg_trace_level */
typedef int (*_SHR_set_sg_trace_level_t)(int, int, int);
static _SHR_set_sg_trace_level_t _SHR_set_sg_trace_level_p = NULL;

int
_SHR_set_sg_trace_level(int grp, int sub_grp, int level)
{
    return (*_SHR_set_sg_trace_level_p)(grp, sub_grp, level);
}


/* _SHR_set_grp_trace_enable_mask */
typedef unsigned (*_SHR_set_grp_trace_enable_mask_t)(int, unsigned);
static _SHR_set_grp_trace_enable_mask_t _SHR_set_grp_trace_enable_mask_p = NULL;

unsigned
_SHR_set_grp_trace_enable_mask(int grp, unsigned mask)
{
    return (*_SHR_set_grp_trace_enable_mask_p)(grp, mask);
}


/* _SHR_set_grp_trace_disable_mask */
typedef unsigned (*_SHR_set_grp_trace_disable_mask_t)(int, unsigned);
static _SHR_set_grp_trace_disable_mask_t _SHR_set_grp_trace_disable_mask_p = NULL;

unsigned
_SHR_set_grp_trace_disable_mask(int grp, unsigned mask)
{
    return (*_SHR_set_grp_trace_disable_mask_p)(grp, mask);
} 


/* _SHR_set_trace_file */
typedef const char* (*_SHR_set_trace_file_t)(const char*);
static _SHR_set_trace_file_t _SHR_set_trace_file_p = NULL;

const char*
_SHR_set_trace_file(const char* filename)
{
    return (*_SHR_set_trace_file_p)(filename);
}


/* _SHR_set_grp_trace_file */
typedef const char* (*_SHR_set_grp_trace_file_t)(int, const char*);
static _SHR_set_grp_trace_file_t _SHR_set_grp_trace_file_p = NULL;

const char*
_SHR_set_grp_trace_file(int grp, const char* filename)
{
    return (*_SHR_set_grp_trace_file_p)(grp, filename);
} 


/* _SHR_set_sg_trace_file */
typedef const char* (*_SHR_set_sg_trace_file_t)(int, int, const char*);
static _SHR_set_sg_trace_file_t _SHR_set_sg_trace_file_p = NULL;

const char*
_SHR_set_sg_trace_file(int grp, int sub_grp, const char* filename)
{
    return (*_SHR_set_sg_trace_file_p)(grp, sub_grp, filename);
} 


/* _SHR_trace_lev_str */
typedef const char* (*_SHR_trace_lev_str_t)(int);
static _SHR_trace_lev_str_t _SHR_trace_lev_str_p = NULL;

const char*
_SHR_trace_lev_str(int level)
{
    return (*_SHR_trace_lev_str_p)(level);
}


/* _SHR_trace_grp_str */
typedef const char* (*_SHR_trace_grp_str_t)(int);
static _SHR_trace_grp_str_t _SHR_trace_grp_str_p = NULL;

const char*
_SHR_trace_grp_str(int grp_num)
{
    return (*_SHR_trace_grp_str_p)(grp_num);
} 


/* _SHR_trace_on */
typedef int (*_SHR_trace_on_t)(int, int, int);
static _SHR_trace_on_t _SHR_trace_on_p = NULL;

int
_SHR_trace_on(int level, int grp, int sub_grp)
{
    return (*_SHR_trace_on_p)(level, grp, sub_grp);
} 


/* _SHR_vtrace */
typedef void (*_SHR_vtrace_t)(int, int, int, const char*, va_list);
static _SHR_vtrace_t _SHR_vtrace_p = NULL;

void
_SHR_vtrace(int level, int grp, int sub_grp, const char* fmt, va_list ap)
{
    (*_SHR_vtrace_p)(level, grp, sub_grp, fmt, ap);
} 


/* _SHR_trace */
void
_SHR_trace(int level, int grp, int sub_grp,  const char* fmt, ...)
{
    va_list ap;

    if (!(*_SHR_trace_on)(level, grp, sub_grp))
        return;

    va_start (ap, fmt);
    (*_SHR_vtrace_p) (level, grp, sub_grp, fmt, ap);
    va_end (ap);
} 



/*----------------------------------------------------------------------*/

void
api_init_shr(void* (*get_addr)(u16))
{
#define GET_ADDR(f,i) f##_p = (f##_t) (*get_addr)(i)

    GET_ADDR(_SHR_trace_init, FI_SHR_TRACEINIT);
    GET_ADDR(_SHR_set_trace_enable, FI_SHR_SETTRACEENABLE);
    GET_ADDR(_SHR_set_trace_level, FI_SHR_SETTRACELEVEL);
    GET_ADDR(_SHR_set_trace_show_time, FI_SHR_SETTRACESHOWTIME);
    GET_ADDR(_SHR_set_trace_show_thread, FI_SHR_SETTRACESHOWTHREAD);
    GET_ADDR(_SHR_set_grp_trace_enable, FI_SHR_SETGRPTRACEENABLE);
    GET_ADDR(_SHR_set_grp_trace_level, FI_SHR_SETGRPTRACELEVEL);
    GET_ADDR(_SHR_set_sg_trace_level, FI_SHR_SETSGTRACELEVEL);
    GET_ADDR(_SHR_set_grp_trace_enable_mask, FI_SHR_SETGRPTRACEENABLEMASK);
    GET_ADDR(_SHR_set_grp_trace_disable_mask, FI_SHR_SETGRPTRACEDISABLEMASK);
    GET_ADDR(_SHR_set_trace_file, FI_SHR_SETTRACEFILE);
    GET_ADDR(_SHR_set_grp_trace_file, FI_SHR_SETGRPTRACEFILE);
    GET_ADDR(_SHR_set_sg_trace_file, FI_SHR_SETSGTRACEFILE);
    GET_ADDR(_SHR_trace_lev_str, FI_SHR_TRACELEVSTR);
    GET_ADDR(_SHR_trace_grp_str, FI_SHR_TRACEGRPSTR);
    GET_ADDR(_SHR_trace_on, FI_SHR_TRACEON);
    GET_ADDR(_SHR_vtrace, FI_SHR_VTRACE);
} /* api_init_shr */

/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#include "vng.h"
#include "vng_p.h"
#include "server_rpc.h"
#include "vna_misc.h"





/* See IMPORTANT comments on using VNG_GBAlloc()
 * and VNG_GBFree() in vng_p.h */


LIBVNG_API VNGErrCode VNG_GetServerTime (VNG          *vng,
                                   VNGTimeOfDay *serverTime,
                                   VNGTimeout    timeout)
{
    VNGErrCode ec;

    struct {
        VN  svn;
       _VNGGetServerTimeRet  r;

    } *buf;
    
    VN *svn;
   _VNGGetServerTimeRet *r;
    int32_t optData = 0;
    size_t  retLen;

    if (!serverTime) {
        return VNGERR_INVALID_ARG;
    }

    if (!(buf = VNG_GBAlloc (sizeof *buf))) {
        return VNGERR_NOMEM;
    }

    svn = &buf->svn;
    r   = &buf->r;

    if ((ec = VNG_InitInfraServerVN (vng, svn))) {
        goto end;
    }

    retLen = sizeof(_VNGGetServerTimeRet);

    if ((ec = VN_SendRPC (svn, VN_MEMBER_OWNER,
                               _VNG_GET_SERVER_TIME,
                               NULL, 0,
                               r, &retLen,
                               &optData,
                               timeout)))
    {
        goto end;
    }

    if (optData != VNG_OK)
        ec = optData;
    else
        *serverTime = r->serverTime;

end:
    VNG_GBFree (buf);

    return ec;
}





LIBVNG_API VNGErrCode VNG_GetUserId(VNG        *vng,
                                    const char *loginName,
                                    VNGUserId  *uid,
                                    VNGTimeout  timeout)
{
    VNGErrCode ec;
    struct {
        VN  svn;
        _VNGGetUserIdArg a;
        _VNGGetUserIdRet r;
    } *buf;
    VN *svn;
    _VNGGetUserIdArg *a;
    _VNGGetUserIdRet *r;
    size_t len;
    int32_t optData = 0;
    size_t retLen;
    
    if (!loginName)
        return VNGERR_INVALID_ARG;
    
    len = strnlen(loginName, VNG_LOGIN_BUF_SIZE);
    if (len == VNG_LOGIN_BUF_SIZE)
        return VNGERR_INVALID_ARG;

    if (!(buf = VNG_GBAlloc(sizeof *buf))) {
        return VNGERR_NOMEM;
    }

    svn = &buf->svn;
    a   = &buf->a;
    r   = &buf->r;

    if ((ec = VNG_InitInfraServerVN (vng, svn))) {
        goto end;
    }

    memcpy (a->loginName, loginName, len+1);
    retLen = sizeof(_VNGGetUserIdRet);
    
    if ((ec = VN_SendRPC (svn, VN_MEMBER_OWNER, _VNG_GET_USER_ID,
                          a, sizeof(*a),
                          r, &retLen,
                          &optData,
                          timeout))) {
        goto end;
    }

    if (optData != VNG_OK)
        ec = optData;
    else
        *uid = r->userId;

  end:
    VNG_GBFree(buf);

    return ec;
}
    

LIBVNG_API VNGErrCode VNG_SetPassword (VNG          *vng,
                                       const char   *oldPasswd,
                                       const char   *newPasswd,
                                       VNGTimeout    timeout)
{
    VNGErrCode ec;

    struct {
        VN  svn;
       _VNGSetPasswordArg  a;

    } *buf;
    
    VN *svn;
   _VNGSetPasswordArg *a;
    int32_t optData = 0;
    size_t  retLen;

    size_t  oLen;
    size_t  nLen;

    if (!oldPasswd || !*oldPasswd || !newPasswd || !*newPasswd) {
        return VNGERR_INVALID_ARG;
    }

    oLen =strnlen(oldPasswd, VNG_PASSWD_BUF_SIZE);
    nLen =strnlen(newPasswd, VNG_PASSWD_BUF_SIZE);

    if (oLen == VNG_PASSWD_BUF_SIZE || nLen == VNG_PASSWD_BUF_SIZE) {
        return VNGERR_INVALID_ARG;
    }

    if (!(buf = VNG_GBAlloc (sizeof *buf))) {
        return VNGERR_NOMEM;
    }

    svn = &buf->svn;
    a   = &buf->a;

    if ((ec = VNG_InitInfraServerVN (vng, svn))) {
        goto end;
    }

    memcpy (a->oldPassword, oldPasswd, oLen+1);
    memcpy (a->newPassword, newPasswd, nLen+1);

    retLen = 0;

    if (!(ec = VN_SendRPC (svn, VN_MEMBER_OWNER,
                                _VNG_SET_PASSWORD,
                                a, sizeof(*a),
                                NULL, &retLen,
                                &optData,
                                timeout)))
    {
        ec = optData;
    }

end:
    VNG_GBFree (buf);

    return ec;
}





LIBVNG_API VNGErrCode VNG_UpdateUserInfo (VNG                 *vng,
                                          const VNGUserInfo   *uinfo,
                                          VNGTimeout           timeout)
{
    VNGErrCode ec;

     struct {
        VN  svn;
        VNGUserInfo uinfo;
    } *buf;

    VN *svn;
    int32_t optData = 0;
    size_t  retLen;

    if (!uinfo) {
        return VNGERR_INVALID_ARG;
    }

    if (!(buf = VNG_GBAlloc (sizeof *buf))) {
        return VNGERR_NOMEM;
    }

    svn = &buf->svn;
    memcpy(&buf->uinfo, uinfo, sizeof(VNGUserInfo));

    if ((ec = VNG_InitInfraServerVN (vng, svn))) {
        goto end;
    }

    retLen = 0;

    if (!(ec = VN_SendRPC (svn, VN_MEMBER_OWNER,
                                _VNG_UPDATE_USER_INFO,
                                &buf->uinfo, sizeof(VNGUserInfo),
                                NULL, &retLen,
                                &optData,
                                timeout)))
    {
        ec = optData;
    }

end:
    VNG_GBFree (svn);

    return ec;
}





//-----------------------------------------------------------
//                  Buddy List and Status
//-----------------------------------------------------------


LIBVNG_API VNGErrCode VNG_GetBuddyList (VNG          *vng,
                                        uint32_t      skipN, // skip N records
                                        VNGUserInfo  *userInfo,
                                        uint32_t     *nBuddies,
                                        VNGTimeout    timeout)
{
    VNGErrCode ec;

    struct {
        VN  svn;
       _vng_get_buddylist_arg  a;

    } *buf;
    
    VN *svn;
   _vng_get_buddylist_arg *a;
    int32_t optData = 0;
    size_t  retLen;

    if (!nBuddies || !userInfo) {
        return VNGERR_INVALID_ARG;
    }

    if (!(buf = VNG_GBAlloc (sizeof *buf))) {
        return VNGERR_NOMEM;
    }

    svn = &buf->svn;
    a   = &buf->a;

    if ((ec = VNG_InitInfraServerVN (vng, svn))) {
        goto end;
    }

    retLen = *nBuddies * sizeof(VNGUserInfo);

    a->nBuddies = *nBuddies;
    a->skipN = skipN;

    if ((ec = VN_SendRPC (svn, VN_MEMBER_OWNER,
        _VNG_GET_BUDDYLIST,
        a, sizeof(*a),
        userInfo, &retLen,
        &optData,
        timeout)))
    {
        goto end;
    }

    if (optData != VNG_OK) {
        ec = optData;
    } else {
        *nBuddies = (uint32_t) retLen / sizeof(VNGUserInfo);
    }

end:
    VNG_GBFree (buf);

    return ec;
}


LIBVNG_API VNGErrCode VNG_InviteUser (VNG         *vng,
                                      const char  *loginName,
                                      VNGTimeout   timeout)
{
    VNGErrCode ec;

    struct {
        VN  svn;
       _vng_invite_buddy_arg  a;

    } *buf;

    VN *svn;
   _vng_invite_buddy_arg *a;
    int32_t optData = 0;

    if (!loginName) {
        return VNGERR_INVALID_ARG;
    }

    if (strnlen(loginName, VNG_LOGIN_BUF_SIZE) == VNG_LOGIN_BUF_SIZE) {
        return VNGERR_INVALID_LEN;
    }

    if (!(buf = VNG_GBAlloc (sizeof *buf))) {
        return VNGERR_NOMEM;
    }
    
    svn = &buf->svn;
    a   = &buf->a;

    if ((ec = VNG_InitInfraServerVN (vng, svn))) {
        goto end;
    }

    memcpy(a->login, loginName, VNG_LOGIN_BUF_SIZE);

    if (!(ec = VN_SendRPC (svn, VN_MEMBER_OWNER,
                                _VNG_INVITE_BUDDY,
                                a, sizeof(*a),
                                NULL, NULL,
                                &optData,
                                timeout)))
    {
        ec = optData;
    }

end:
    VNG_GBFree (buf);

    return ec;
}


LIBVNG_API VNGErrCode VNG_AcceptUser (VNG         *vng,
                                      VNGUserId    uid,
                                      VNGTimeout   timeout)
{
    VNGErrCode ec;

    struct {
        VN  svn;
       _vng_accept_buddy_arg  a;

    } *buf = VNG_GBAlloc (sizeof *buf);

    VN *svn = &buf->svn;
   _vng_accept_buddy_arg *a = &buf->a;
    int32_t optData = 0;

    if (!buf) {
        return VNGERR_NOMEM;
    }
    
    if ((ec = VNG_InitInfraServerVN (vng, svn))) {
        goto end;
    }

    a->userId = uid;

    if (!(ec = VN_SendRPC (svn, VN_MEMBER_OWNER,
                                _VNG_ACCEPT_BUDDY,
                                a, sizeof(*a),
                                NULL, NULL,
                                &optData,
                                timeout)))
    {
        ec = optData;
    }

end:
    VNG_GBFree (buf);

    return ec;
}


LIBVNG_API VNGErrCode VNG_RejectUser (VNG         *vng,
                                      VNGUserId    uid,
                                      VNGTimeout   timeout)
{
    VNGErrCode ec;

    struct {
        VN  svn;
       _vng_reject_buddy_arg  a;

    } *buf = VNG_GBAlloc (sizeof *buf);

    VN *svn = &buf->svn;
   _vng_reject_buddy_arg *a = &buf->a;
    int32_t optData = 0;

    if (!buf) {
        return VNGERR_NOMEM;
    }
    
    if ((ec = VNG_InitInfraServerVN (vng, svn))) {
        goto end;
    }

    a->userId = uid;

    if (!(ec = VN_SendRPC (svn, VN_MEMBER_OWNER,
                                _VNG_REJECT_BUDDY,
                                a, sizeof(*a),
                                NULL, NULL,
                                &optData,
                                timeout)))
    {
        ec = optData;
    }

end:
    VNG_GBFree (buf);

    return ec;
}


LIBVNG_API VNGErrCode VNG_BlockUser (VNG         *vng,
                                     VNGUserId    uid,
                                     VNGTimeout   timeout)
{
    VNGErrCode ec;

    struct {
        VN  svn;
       _vng_block_buddy_arg  a;

    } *buf = VNG_GBAlloc (sizeof *buf);

    VN *svn = &buf->svn;
   _vng_block_buddy_arg *a = &buf->a;
    int32_t optData = 0;

    if (!buf) {
        return VNGERR_NOMEM;
    }
    
    if ((ec = VNG_InitInfraServerVN (vng, svn))) {
        goto end;
    }

    a->userId = uid;

    if (!(ec = VN_SendRPC (svn, VN_MEMBER_OWNER,
                                _VNG_BLOCK_BUDDY,
                                a, sizeof(*a),
                                NULL, NULL,
                                &optData,
                                timeout)))
    {
        ec = optData;
    }

end:
    VNG_GBFree (buf);

    return ec;
}


LIBVNG_API VNGErrCode VNG_UnblockUser (VNG        *vng,
                                       VNGUserId   uid,
                                       VNGTimeout  timeout)
{
    VNGErrCode ec;

    struct {
        VN  svn;
       _vng_unblock_buddy_arg  a;

    } *buf = VNG_GBAlloc (sizeof *buf);

    VN *svn = &buf->svn;
    _vng_unblock_buddy_arg *a = &buf->a;
    int32_t optData = 0;

    if (!buf) {
        return VNGERR_NOMEM;
    }
    
    if ((ec = VNG_InitInfraServerVN (vng, svn))) {
        goto end;
    }

    a->userId = uid;

    if (!(ec = VN_SendRPC (svn, VN_MEMBER_OWNER,
                                _VNG_UNBLOCK_BUDDY,
                                a, sizeof(*a),
                                NULL, NULL,
                                &optData,
                                timeout)))
    {
        ec = optData;
    }

end:
    VNG_GBFree (buf);

    return ec;
}


LIBVNG_API VNGErrCode VNG_RemoveUser (VNG        *vng,
                                      VNGUserId   uid,
                                      VNGTimeout  timeout)
{
    VNGErrCode ec;

    struct {
        VN  svn;
       _vng_remove_buddy_arg  a;

    } *buf = VNG_GBAlloc (sizeof *buf);

    VN *svn = &buf->svn;
   _vng_remove_buddy_arg *a = &buf->a;
    int32_t optData = 0;

    if (!buf) {
        return VNGERR_NOMEM;
    }
    
    if ((ec = VNG_InitInfraServerVN (vng, svn))) {
        goto end;
    }

    a->userId = uid;

    if (!(ec = VN_SendRPC (svn, VN_MEMBER_OWNER,
                                _VNG_REMOVE_BUDDY,
                                a, sizeof(*a),
                                NULL, NULL,
                                &optData,
                                timeout)))
    {
        ec = optData;
    }

end:
    VNG_GBFree (buf);

    return ec;
}


LIBVNG_API VNGErrCode VNG_UpdateStatus(VNG                  *vng,
                                       VNGUserOnlineStatus  status,                        
                                       VNGGameId            gameId,
                                       VNId                 vnId,
                                       VNGTimeout           timeout)
{
    VNGErrCode ec;

    struct {
        VN  svn;
       _vng_update_status_arg  a;

    } *buf = VNG_GBAlloc (sizeof *buf);

    VN *svn = &buf->svn;
   _vng_update_status_arg *a = &buf->a;
    int32_t optData = 0;

    if (!buf) {
        return VNGERR_NOMEM;
    }
    
    if ((ec = VNG_InitInfraServerVN (vng, svn))) {
        goto end;
    }

    a->status = status;
    a->gameId = gameId;
    a->vnId = vnId;

    if (!(ec = VN_SendRPC (svn, VN_MEMBER_OWNER,
                                _VNG_UPDATE_STATUS,
                                a, sizeof(*a),
                                NULL, NULL,
                                &optData,
                                timeout)))
    {
        ec = optData;
    }

end:
    VNG_GBFree (buf);

    return ec;
}

LIBVNG_API VNGErrCode VNG_GetBuddyStatus (VNG            *vng,
                                          VNGBuddyStatus *buddyStatus,
                                          uint32_t        nBuddies, 
                                          VNGTimeout      timeout)
{
    VNGErrCode ec;
    
    struct {
        VN  svn;
       _vng_get_buddy_status_arg  a;

    } *buf;

    VN *svn;
   _vng_get_buddy_status_arg *a;
    size_t retLen = nBuddies * sizeof(VNGBuddyStatus);
    int32_t optData = 0;
    uint32_t i;

    if (nBuddies > MAX_BUDDIES) {
        return  VNGERR_INVALID_LEN;
    }

    if (!(buf = VNG_GBAlloc (sizeof *buf))) {
        return VNGERR_NOMEM;
    }
    
    svn = &buf->svn;
    a   = &buf->a;

    if ((ec = VNG_InitInfraServerVN (vng, svn))) {
        goto end;
    }

    a->count = nBuddies;
    for (i = 0; i < nBuddies; i++) {
        a->buddies[i] = buddyStatus[i].uid;
    }

    if (!(ec = VN_SendRPC (svn, VN_MEMBER_OWNER,
                                _VNG_GET_BUDDY_STATUS,
                                a, sizeof(*a),
                                buddyStatus, &retLen,
                                &optData,
                                timeout)))
    {
        ec = optData;
    }

end:
    VNG_GBFree (buf);

    return ec;
}


LIBVNG_API VNGErrCode VNG_EnableTracking (VNG        *vng,
                                          VNGData     dataType, 
                                          VNGTimeout  timeout)
{
    VNGErrCode ec;
    int32_t optData = 0;
    VN *svn = VNG_GBAlloc (sizeof *svn);

    if (!svn) {
        return VNGERR_NOMEM;
    }

    if ((ec = VNG_InitInfraServerVN (vng, svn))) {
        goto end;
    }

    if (!(ec = VN_SendRPC (svn, VN_MEMBER_OWNER,
                                _VNG_ENABLE_BUDDY_TRACKING,
                                NULL, 0,
                                NULL, NULL,
                                &optData,
                                timeout)))
    {
        ec = optData;
    }

end:
    VNG_GBFree (svn);

    return ec;
}


LIBVNG_API VNGErrCode VNG_DisableTracking (VNG      *vng,
                                          VNGData    dataType, 
                                          VNGTimeout  timeout)
{
    VNGErrCode ec;
    int32_t optData = 0;
    VN *svn = VNG_GBAlloc (sizeof *svn);

    if (!svn) {
        return VNGERR_NOMEM;
    }

    if ((ec = VNG_InitInfraServerVN (vng, svn))) {
        goto end;
    }

    if (!(ec = VN_SendRPC (svn, VN_MEMBER_OWNER,
                                _VNG_DISABLE_BUDDY_TRACKING,
                                NULL, 0,
                                NULL, NULL,
                                &optData,
                                timeout)))
    {
        ec = optData;
    }

end:
    VNG_GBFree (svn);

    return ec;
}



//-----------------------------------------------------------
//                  Score Management
//-----------------------------------------------------------


LIBVNG_API VNGErrCode VNG_SubmitScore(VNG          *vng,
                                      VNGScoreKey  *key,
                                      uint8_t       info[16],
                                      VNGScoreItem *scoreItems,
                                      uint32_t      nItems,
                                      VNGTimeout    timeout)
{
    VNGErrCode ec;
    
    struct {
        VN  svn;
       _vng_submit_score_arg  a;
       _vng_submit_score_ret  r;

    } *buf;

    VN *svn;
   _vng_submit_score_arg *a;
   _vng_submit_score_ret *r;
    size_t retLen = sizeof(*r);
    int32_t optData = 0;
    uint32_t i,j;

    if( nItems > VNG_MAX_SCORE_ITEMS ) {
        return VNGERR_INVALID_ARG;
    }

    if (!(buf = VNG_GBAlloc (sizeof *buf))) {
        return VNGERR_NOMEM;
    }
    
    svn = &buf->svn;
    a   = &buf->a;
    r   = &buf->r;

    if ((ec = VNG_InitInfraServerVN (vng, svn)))   {
        goto end;
    }
    
    a->key = *key;
    if(info == NULL) memset(a->info, 0, 16);
    else memcpy(a->info, info, 16);
    
    a->nItems = nItems;
    memcpy(a->scoreItems, scoreItems, nItems*sizeof(VNGScoreItem));
    
    if((ec = VN_SendRPC(svn, 
                        VN_MEMBER_OWNER, 
                        _VNG_SUBMIT_SCORE, 
                        a, sizeof(*a), 
                        r, &retLen, 
                        &optData, 
                        timeout)))
    {
        goto end;
    }
    
    if( optData != VNG_OK )
    {
        ec = optData;
    }else
    {
        if( retLen != nItems * sizeof(VNGScoreItem) ) return VNGERR_UNKNOWN;
        for(i=0;i<nItems;i++)
            for(j=0;j<nItems;j++)
                if( r->scoreItems[j].itemId == scoreItems[i].itemId )
                {
                    scoreItems[i].rank = r->scoreItems[j].rank;
                    break;
                }
    }

end:
    VNG_GBFree (buf);

    return ec;
}


LIBVNG_API VNGErrCode VNG_SubmitScoreObject(VNG         *vng,
                                            VNGScoreKey *objKey,
                                            const char  *object,
                                            size_t       objectSize,
                                            VNGTimeout   timeout)
{
    VNGErrCode ec;
    
    struct {
        VN  svn;
       _vng_submit_score_obj_arg  a;

    } *buf;

    VN *svn;
   _vng_submit_score_obj_arg *a;
    int32_t optData = 0;

    if( objectSize > VNG_MAX_SCORE_OBJ_SIZE ) {
        return VNGERR_INVALID_ARG;
    }

    if (!(buf = VNG_GBAlloc (sizeof *buf))) {
        return VNGERR_NOMEM;
    }
    
    svn = &buf->svn;
    a   = &buf->a;

    if ((ec = VNG_InitInfraServerVN (vng, svn)))   {
        goto end;
    }
    
    a->key = *objKey;
    memcpy(a->object, object, objectSize);
    
    a->objectSize = objectSize;
    
    if(!(ec = VN_SendRPC(svn, 
                         VN_MEMBER_OWNER, 
                         _VNG_SUBMIT_SCORE_OBJ, 
                         a, (char *)&a->object[objectSize] - (char *)a, 
                         NULL, NULL, 
                         &optData, 
                         timeout)))
    {
        ec = optData;
    }
    
end:
    VNG_GBFree (buf);

    return ec;
}


LIBVNG_API VNGErrCode VNG_GetGameScore(VNG          *vng, 
                                       VNGScoreKey  *scoreKey,
                                       VNGScore     *score,
                                       VNGScoreItem *scoreItems,
                                       uint32_t     *nItems,
                                       VNGTimeout    timeout)
{
    VNGErrCode ec;
    
    struct {
        VN  svn;
       _vng_get_score_arg  a;
       _vng_get_score_ret  r;

    } *buf;

    VN *svn;
   _vng_get_score_arg *a;
   _vng_get_score_ret *r;
    size_t retLen = sizeof(*r);
    int32_t optData = 0;
    uint32_t k, n;

    if( *nItems > VNG_MAX_SCORE_ITEMS ) {
        return VNGERR_INVALID_ARG;
    }

    if (!(buf = VNG_GBAlloc (sizeof *buf))) {
        return VNGERR_NOMEM;
    }

    svn = &buf->svn;
    a   = &buf->a;
    r   = &buf->r;
    
    if ((ec = VNG_InitInfraServerVN (vng, svn)))   {
        goto end;
    }
    
    a->key = *scoreKey;
    a->nItems = *nItems;
    
    if((ec = VN_SendRPC(svn, 
                        VN_MEMBER_OWNER, 
                        _VNG_GET_SCORE, 
                        a, sizeof(*a), 
                        r, &retLen, 
                        &optData, 
                        timeout)))
    {
        goto end;
    }
    
    if( optData != VNG_OK )
    {
        ec = optData;
    }else
    {
        if( retLen > sizeof(*r) ) {
            ec = VNGERR_UNKNOWN;
            goto end;
        }
        score->uInfo.uid = r->score.uInfo.uid;
        score->timeOfDay = r->score.timeOfDay;
        score->nItems = r->score.nItems;
        score->objSize = r->score.objSize;
        memcpy(score->info, r->score.info, 16);
        if( r->score.nItems < *nItems ) *nItems = r->score.nItems;
        n = *nItems;
        for(k=0;k<n;k++)
        {
            scoreItems[k].itemId = r->scoreItems[k].itemId;
            scoreItems[k].score = r->scoreItems[k].score;
            scoreItems[k].rank = r->scoreItems[k].rank;
        }
        ec = VNG_OK;
    }
    
end:
    VNG_GBFree (buf);

    return ec;
}


LIBVNG_API VNGErrCode VNG_GetGameScoreObject(VNG         *vng, 
                                             VNGScoreKey *scoreKey,
                                             char        *object,
                                             size_t      *objectSize,
                                             VNGTimeout   timeout)
{
    VNGErrCode ec;
    
    struct {
        VN  svn;
       _vng_get_score_obj_arg  a;
       _vng_get_score_obj_ret  r;

    } *buf = VNG_GBAlloc (sizeof *buf);

    VN *svn = &buf->svn;
   _vng_get_score_obj_arg *a = &buf->a;
   _vng_get_score_obj_ret *r = &buf->r;
    size_t retLen = sizeof(*r);
    int32_t optData = 0;

    if (!buf) {
        return VNGERR_NOMEM;
    }
    
    if ((ec = VNG_InitInfraServerVN (vng, svn)))   {
        goto end;
    }
    
    a->key = *scoreKey;
    
    if((ec = VN_SendRPC(svn, 
                        VN_MEMBER_OWNER, 
                        _VNG_GET_SCORE_OBJ, 
                        a, sizeof(*a), 
                        r, &retLen, 
                        &optData, 
                        timeout)))
    {
        goto end;
    }
    
    if( optData != VNG_OK )
    {
        ec = optData;
    }else
    {
        if( *objectSize < r->objectSize ) {
            ec = VNGERR_INVALID_LEN;
            goto end;
        }
        memcpy(object, r->object, r->objectSize);
        *objectSize = r->objectSize;
    }

end:
    VNG_GBFree (buf);
    
    return ec;
}


LIBVNG_API VNGErrCode VNG_DeleteGameScore(VNG          *vng, 
                                          VNGScoreKey  *scoreKey,
                                          VNGTimeout    timeout)
{
    VNGErrCode ec;
    
    struct {
        VN  svn;
       _vng_delete_score_arg  a;

    } *buf = VNG_GBAlloc (sizeof *buf);

    VN *svn = &buf->svn;
   _vng_delete_score_arg *a = &buf->a;
    int32_t optData = 0;

    if (!buf) {
        return VNGERR_NOMEM;
    }
    
    if ((ec = VNG_InitInfraServerVN (vng, svn)))   {
        goto end;
    }
    
    a->key = *scoreKey;
    
    if(!(ec = VN_SendRPC(svn, 
                         VN_MEMBER_OWNER, 
                         _VNG_DELETE_SCORE, 
                         a, sizeof(*a), 
                         NULL, NULL, 
                         &optData, 
                         timeout)))
    {
        ec = optData;
    }
    
end:
    VNG_GBFree (buf);

    return ec;
}


LIBVNG_API VNGErrCode VNG_GetRankedScores(VNG          *vng, 
                                          VNGGameId     gameId,
                                          uint32_t      scoreId,
                                          uint32_t      itemId,
                                          VNGUserId     optUid,
                                          uint32_t      rankBegin,
                                          VNGRankedScoreResult *scoreResult,
                                          uint32_t       *nResults,
                                          VNGTimeout      timeout)
{
    VNGErrCode ec;
    
    struct {
        VN  svn;
       _vng_get_ranked_scores_arg  a;
       _vng_get_ranked_scores_ret  r;

    } *buf;

    VN *svn;
   _vng_get_ranked_scores_arg *a;
   _vng_get_ranked_scores_ret *r;
    size_t retLen = sizeof(*r);
    int32_t optData = 0;

    if( !nResults || *nResults > VNG_MAX_RANKED_SCORE_RESULTS ) {
        return VNGERR_INVALID_ARG;
    }

    if (!(buf = VNG_GBAlloc (sizeof *buf))) {
        return VNGERR_NOMEM;
    }
    
    svn = &buf->svn;
    a   = &buf->a;
    r   = &buf->r;

    if ((ec = VNG_InitInfraServerVN (vng, svn)))   {
        goto end;
    }
    
    a->gameId = gameId;
    a->scoreId = scoreId;
    a->itemId = itemId;
    a->optUid = optUid;
    a->rankBegin = rankBegin;
    a->count = *nResults;
    
    if((ec = VN_SendRPC(svn, 
                        VN_MEMBER_OWNER, 
                        _VNG_GET_RANKED_SCORES, 
                        a, sizeof(*a), 
                        r, &retLen, 
                        &optData, 
                        timeout)))
    {
        goto end;
    }
    
    if( optData != VNG_OK )
    {
        ec = optData;
    }else
    {
        size_t count = retLen / sizeof(VNGRankedScoreResult);
        if( count < *nResults ) *nResults = (uint32_t) count;
        memcpy(scoreResult, r, retLen);
    }

end:
    VNG_GBFree (buf);
    
    return ec;
}



LIBVNG_API VNGErrCode VNG_GetScoreKeys(VNG            *vng,
                                       VNGUserId       uid,
                                       uint32_t        optGameId,
                                       uint32_t        optScoreId,
                                       VNGScoreKey    *scoreKeys,
                                       uint32_t       *nScoreKeys,
                                       VNGTimeout      timeout)
{
    VNGErrCode ec;
    
    struct {
        VN  svn;
        _VNGGetScoreKeysArg  a;
        _VNGGetScoreKeysRet  r;
    } *buf;

    VN *svn;
    _VNGGetScoreKeysArg *a;
    _VNGGetScoreKeysRet *r;
    size_t retLen = sizeof(*r);
    int32_t optData = 0;

    if( *nScoreKeys > VNG_MAX_SCORE_RESULTS ) return VNGERR_INVALID_ARG;
    if( uid == 0 ) return VNGERR_INVALID_ARG;
    
    if (!(buf = VNG_GBAlloc (sizeof *buf))) {
        return VNGERR_NOMEM;
    }
    
    svn = &buf->svn;
    a   = &buf->a;
    r   = &buf->r;
    
    if ((ec = VNG_InitInfraServerVN (vng, svn)))   {
        goto end;
    }

    a->uid = uid;
    a->gameId = optGameId;
    a->scoreId = optScoreId;
    a->nScoreKeys = *nScoreKeys;
    
    if((ec = VN_SendRPC(svn, 
                        VN_MEMBER_OWNER, 
                        _VNG_GET_SCORE_KEYS, 
                        a, sizeof(*a), 
                        r, &retLen, 
                        &optData, 
                        timeout)))
    {
        goto end;
    }
    
    if( optData != VNG_OK )
    {
        ec = optData;
    }else
    {
	    memcpy(scoreKeys, r->scoreKeys, retLen);
        *nScoreKeys = (uint32_t)(retLen/sizeof(VNGScoreKey));
    }

end:
    VNG_GBFree (buf);
    
    return ec;
}



//-----------------------------------------------------------
//                  Stored Messages
//-----------------------------------------------------------


LIBVNG_API VNGErrCode VNG_StoreMessage(VNG           *vng,
                                       const char    *recipients[],
                                       uint32_t       nRecipients,
                                       const char    *subject,
                                       uint32_t       mediaType,
                                       const void    *msgBuffer,
                                       size_t         msgLen,
                                       uint32_t       replyMsgid,
                                       uint32_t      *results,
                                       VNGTimeout     timeout)
{
    VNGErrCode   ec = VNG_OK;
    uint32_t     i;
    size_t       subjectLen;
    size_t       recipLen;
    size_t       recipsLen   =  0;
    uint32_t     nRecipsSent =  0;
    size_t       msgSentLen  =  0;
    size_t       argsLen     =  0;
    size_t       maxArgsLen  =  VN_MAX_MSG_LEN;
    size_t       retLen;
    size_t       totalMsgLen =  msgLen;
    uint8_t     *msg         = (uint8_t*) msgBuffer;
    uint16_t     seqId;
    uint16_t     seqNum;
   _VNGOptData   optData;

    struct {
        VN  svn;
        union {
           _VNGStoreMessageArg  a;
            uint8_t             max [VN_MAX_MSG_LEN];
        } msg;

    } *buf;

    VN *svn;
   _VNGStoreMessageArg *a;
    uint8_t *args;

    if (!nRecipients || !recipients || !results
            || nRecipients > VNG_MAX_STORE_MSG_N_RECIP) {
        return VNGERR_INVALID_LEN;
    }

    for (i = 0;  i < nRecipients;  ++i) {
        results[i] = VNGERR_STM_SEND;
        recipLen = strnlen(recipients[i], VNG_LOGIN_BUF_SIZE);
        if (recipLen > VNG_MAX_LOGIN_LEN) {
            ec = VNGERR_INVALID_LEN;
        }
        recipsLen += (recipLen + 1);
    }

    if (ec) {
        return ec;
    }

    if (subject)
        subjectLen = strnlen(subject, VNG_SUBJECT_BUF_SIZE);
    else
        subjectLen = 0;

    if ((msgLen && !msgBuffer) || msgLen > VNG_MAX_STORED_MSG_LEN
            || subjectLen > VNG_MAX_SUBJECT_LEN
            || recipsLen > UINT16_MAX) {
        return VNGERR_INVALID_LEN;
    }

    if (!msgLen || mediaType != VNG_MEDIA_TYPE_TEXT) {
        totalMsgLen = msgLen;
    }
    else {
        totalMsgLen = strnlen(msgBuffer, msgLen);
        if (totalMsgLen == msgLen) {
            return VNGERR_INVALID_LEN;
        }
        totalMsgLen += 1; // include '\0'
    }

    if (!vng) {
        return VNGERR_INVALID_VNG;
    }

    seqId  = 0;
    seqNum = 0;

    if (!(buf = VNG_GBAlloc (sizeof *buf))) {
        return VNGERR_NOMEM;
    }

    svn  = &buf->svn;
    a    = &buf->msg.a;
    args = (uint8_t*) a;

    if ((ec = VNG_InitInfraServerVN (vng, svn))) {
        goto end;
    }

    a->mediaType  =  mediaType;
    a->replyMsgid =  replyMsgid;
    memcpy (&a->subjectLen, &subjectLen, sizeof a->subjectLen);  // 16 bit writes from gba
    memcpy (&a->recipsLen, &recipsLen, sizeof a->recipsLen);     // to sdram are unreliable
    a->msgLen     =  (uint32_t) totalMsgLen;

    argsLen = sizeof *a;

    memcpy(&args[argsLen], subject, subjectLen);

    argsLen += subjectLen;

    while (nRecipsSent != nRecipients || msgSentLen != totalMsgLen) {
        size_t n, len;

        while (nRecipsSent < nRecipients) {
            len = maxArgsLen - argsLen;
            n = strnlen(recipients[nRecipsSent], VNG_LOGIN_BUF_SIZE);
            if (n > VNG_MAX_LOGIN_LEN) {
                ec = VNGERR_UNKNOWN;
                goto end;
            }
            if (++n > len) {
                break;
            }
            memcpy(&args[argsLen], recipients[nRecipsSent], n);
            argsLen += n;
            ++nRecipsSent;
        }

        if (nRecipsSent == nRecipients
                    && msgSentLen < totalMsgLen
                            && argsLen < maxArgsLen) {
            len = maxArgsLen - argsLen;
            n = totalMsgLen - msgSentLen;
            if (n > len) {
                n = len;
            }
            memcpy(&args[argsLen], &msg[msgSentLen], n);
            argsLen += n;
            msgSentLen += n;
        }

        optData.seq.id  = seqId;
        optData.seq.num = seqNum;
        retLen =  nRecipients * (sizeof *results);

        if ((ec = VN_SendRPC (svn, VN_MEMBER_OWNER,
                              _VNG_STORE_MESSAGE,
                              args, argsLen,
                              results, &retLen,
                              &optData.retval,
                              timeout)))
        {
            break;
        }

        if (optData.retval > 0) {
            ec = optData.retval;
            break;
        }
        else {
            optData.retval = -optData.retval;
        }

        if (!seqId) {
            seqId = optData.seq.id;
        }

        if (retLen != 0) {
            if (nRecipsSent != nRecipients || msgSentLen != totalMsgLen
                    || retLen != nRecipients * (sizeof *results)) {
                ec = VNGERR_UNKNOWN;
            }
            break;
        }

        ++seqNum;
        argsLen = 0;
    }

end:
    VNG_GBFree (buf);

    return ec;
}




LIBVNG_API VNGErrCode VNG_GetMessageList(VNG            *vng,
                                         uint32_t        skipN,     // in
                                         VNGMessageHdr  *msgHdr,    // out
                                         uint32_t       *nMsgHdr,   // in/out
                                         VNGTimeout      timeout)
{
    VNGErrCode  ec;
    size_t      retLen;
    int32_t     optData = 0;
    uint32_t    totalToGet;
    uint32_t    nToGet;

    struct {
        VN  svn;
       _VNGGetMessageListArg  a;

    } *buf;

    VN *svn;
   _VNGGetMessageListArg *a;

    if (!msgHdr || !nMsgHdr) {
        return VNGERR_INVALID_ARG;
    }

    if (!*nMsgHdr) {
        return VNGERR_INVALID_LEN;
    }

    if (!(buf = VNG_GBAlloc (sizeof *buf))) {
        return VNGERR_NOMEM;
    }

    svn = &buf->svn;
    a   = &buf->a;

    if ((ec = VNG_InitInfraServerVN (vng, svn))) {
        goto end;
    }

    totalToGet = *nMsgHdr;
    *nMsgHdr = 0;

    while (*nMsgHdr < totalToGet) {
        nToGet = totalToGet - *nMsgHdr;
        retLen = nToGet * sizeof(VNGMessageHdr);
        a->count  = nToGet;
        a->skipN  = skipN + *nMsgHdr;

        if ((ec = VN_SendRPC (svn, VN_MEMBER_OWNER,
                              _VNG_GET_MESSAGE_LIST,
                              a, sizeof(*a),
                              &msgHdr[*nMsgHdr], &retLen,
                              &optData,
                              timeout))) {
            goto end;
        }

        if (optData > 0) {
            ec = optData;
            break;
        } else {
            *nMsgHdr += (uint32_t) retLen / sizeof *msgHdr;
            if (retLen < sizeof *msgHdr)
                break;
        }
    }

end:
    VNG_GBFree (buf);

    return ec;
}


LIBVNG_API VNGErrCode VNG_RetrieveMessage(VNG        *vng,
                                          uint32_t    msgId,
                                          char       *msgBuffer,
                                          size_t     *msgLen,
                                          VNGTimeout  timeout)
{
    VNGErrCode  ec;
    size_t      retLen;
    int32_t     optData = 0;
    size_t      maxMsgLen;
    size_t      maxToGet;
    size_t      toGet;

    struct {
        VN  svn;
       _VNGRetrieveMessageArg  a;

    } *buf;

    VN *svn;
   _VNGRetrieveMessageArg *a;

    if (msgLen && *msgLen && !msgBuffer) {
        return VNGERR_INVALID_ARG;
    }

    if (!(buf = VNG_GBAlloc (sizeof *buf))) {
        return VNGERR_NOMEM;
    }
    
    svn = &buf->svn;
    a   = &buf->a;

    if ((ec = VNG_InitInfraServerVN (vng, svn))) {
        goto end;
    }

    maxMsgLen = *msgLen;
    maxToGet  = maxMsgLen;
    *msgLen = 0;

    while (*msgLen < maxToGet) {
        toGet = maxToGet - *msgLen;
        retLen = toGet;
        a->msgId = msgId;
        a->offset    = (uint32_t) *msgLen;
        a->maxRetLen = (uint32_t) toGet;

        if ((ec = VN_SendRPC (svn, VN_MEMBER_OWNER,
                              _VNG_RETRIEVE_MESSAGE,
                              a, sizeof(*a),
                              &msgBuffer[*msgLen], &retLen,
                              &optData,
                              timeout))) {
            goto end;
        }

        if (optData > 0) {
            ec = optData;
            break;
        } else {
            *msgLen += retLen;
            maxToGet = -optData; // Actual strored msg len
            if (maxToGet > maxMsgLen) {
                maxToGet = maxMsgLen;
            }
        }
    }

end:
    VNG_GBFree (buf);

    return ec;
}


LIBVNG_API VNGErrCode VNG_DeleteMessage(VNG        *vng,
                                        uint32_t   *msgId,
                                        uint32_t    nMsgId,
                                        VNGTimeout  timeout)
{
    VNGErrCode  ec;
    int32_t     optData = 0;

    struct {
        VN  svn;
        union {
           _VNGDeleteMessageArg  a;
            uint8_t              max [VN_MAX_MSG_LEN];
        } msg;

    } *buf;

    VN *svn;
   _VNGDeleteMessageArg *a;

    if (!nMsgId) {
        return VNG_OK;
    }

    if (!msgId || (sizeof *a + nMsgId * sizeof *msgId) > VN_MAX_MSG_LEN) {
        return VNGERR_INVALID_LEN;
    }

    if (!(buf = VNG_GBAlloc (sizeof *buf))) {
        return VNGERR_NOMEM;
    }
    
    svn = &buf->svn;
    a   = &buf->msg.a;

    if ((ec = VNG_InitInfraServerVN (vng, svn))) {
        goto end;
    }

    a->nMsgIds = nMsgId;
    memcpy((char*)(a + 1), msgId, nMsgId * sizeof *msgId);

    if (!(ec = VN_SendRPC (svn, VN_MEMBER_OWNER,
                           _VNG_DELETE_MESSAGE,
                           a, sizeof *a + nMsgId * sizeof *msgId,
                           NULL, NULL,
                           &optData,
                           timeout))) {
        ec = optData;
    }

end:
    VNG_GBFree (buf);

    return ec;
}







//-----------------------------------------------------------
//                  Error Messgages
//-----------------------------------------------------------


LIBVNG_API VNGErrCode  VNG_ErrMsg (VNGErrCode  errcode,
                                   char       *msg,
                                   size_t      msgLen)
{
    return vnaErrMsg (errcode, msg, msgLen);
}




//-----------------------------------------------------------
//                  ServeRPC
//-----------------------------------------------------------



LIBVNG_API VNGErrCode VNG_ServeRPC (VNG        *vng,
                                    VNGTimeout  timeout)
{
    VNGErrCode    ec;
    VNMember      memb;
    VNServiceTag  serviceTag;
    VNCallerTag   callerTag;
    int32_t       optData;

    struct RecvArgs {
        size_t            argsLen;
        uint8_t           args [VN_MAX_MSG_LEN];
        VNMsgHdr          hdr;
        VN               *vn;
        VNRemoteProcedure proc;

    };

    struct SendArgs {
        size_t          retLen;
        uint8_t         ret [VN_MAX_MSG_LEN];

    };

    struct {
        struct RecvArgs rcv;
        struct SendArgs snd;

    } *buf;

    struct RecvArgs *rcv;
    struct SendArgs *snd;

    buf = VNG_GBAlloc(sizeof *buf);

    if (!buf) {
        return VNGERR_NOMEM;
    }
    
    rcv = &buf->rcv;
    snd = &buf->snd;

    rcv->argsLen = sizeof rcv->args;
    snd->retLen = 0;

    if ((ec = VNG_RecvRPC (vng, &rcv->proc,
                                &rcv->vn,
                                &rcv->hdr,
                                &rcv->args,
                                &rcv->argsLen,
                                timeout))) {
        goto end;
    }

    memb = rcv->hdr.sender;
    serviceTag = rcv->hdr.serviceTag;
    callerTag = rcv->hdr.callerTag;

    optData = rcv->proc ( rcv->vn,
                          &rcv->hdr,
                          rcv->args,
                          rcv->argsLen,
                          snd->ret,
                          &snd->retLen);

    ec = VN_SendResp ( rcv->vn,
                       memb,
                       serviceTag,
                       callerTag,
                       snd->ret,
                       snd->retLen,
                       optData,
                       timeout); // No way to calc remaining timeout
end:
    VNG_GBFree (buf);

    if (ec && ec != VNGERR_FINI && ec != VNGERR_INVALID_VNG)
        ec = VNG_OK;

    return ec;
}



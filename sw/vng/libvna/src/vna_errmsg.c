/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#include "vng.h"
#include "vna_misc.h"


/* This file contains definitions of VNG Error Message stings and the
 * type definitions, data structures, and routines to support VNG_ErrMsg.
 */


typedef enum {
    _VNG_EN_US,
    _VNG_ZH_CN,

} _VNG_LOCALE;


#define _VNG_DEFAULT_LOCALE   _VNG_ZH_CN




static VNGErrCode __err_codes [] = {
    VNGERR_UNKNOWN,
    VNGERR_NOWAIT,
    VNGERR_TIMEOUT,
    VNGERR_NO_VN_SERVER,
    VNGERR_NOT_LOGGED_IN,
    VNGERR_OVERFLOW,
    VNGERR_NOMEM,
    VNGERR_FINI,
    VNGERR_NOT_FOUND,
    VNGERR_BUF_FULL,
    VNGERR_NOT_SUPPORTED,
    VNGERR_INFRA_NS,
    VNGERR_CONN_REFUSED,
    VNGERR_UNREACHABLE,
    VNGERR_INVALID_LOGIN,
    VNGERR_NOT_INVITED,
    VNGERR_UNKNOWN_NAME,
    VNGERR_VN_ENDED,
    VNGERR_VN_JOIN_DENIED,
    VNGERR_VN_MISSING_PROC,
    VNGERR_VN_MISSING_STATUS,
    VNGERR_VN_NOT_HOST,
    VNGERR_INVALID_ARG,
    VNGERR_INVALID_VNG,
    VNGERR_INVALID_VN,
    VNGERR_INVALID_MEMB,
    VNGERR_INVALID_LEN,
    VNGERR_MAILBOX_FULL,
    VNGERR_OUTBOX_FULL,
    VNGERR_STM_SEND,
    VNGERR_STM_DELETE,
    VNGERR_VOICE_IN_USE,
    VNGERR_RM_API,
    VNGERR_ACCESS,
    VNGERR_MAX_REACHED,
};

static const char*  __msgs_en_us [sizeof __err_codes] = {
/*  VNGERR_UNKNOWN            */  "A failure occured that does not have a specific error message.",
/*  VNGERR_NOWAIT             */  "Non-blocking call returns immediately.",
/*  VNGERR_TIMEOUT            */  "Timeout before operation completed.",
/*  VNGERR_NO_VN_SERVER       */  "A VN server connection was not available.",
/*  VNGERR_NOT_LOGGED_IN      */  "An action was attempted that requires login.",
/*  VNGERR_OVERFLOW           */  "Output value buffer overflow.",
/*  VNGERR_NOMEM              */  "Out of memory.",
/*  VNGERR_FINI               */  "The VNG session is not initialized or has been terminated.",
/*  VNGERR_NOT_FOUND          */  "Info being retrieved was not found.",
/*  VNGERR_BUF_FULL           */  "An API failed because an internal buffer was full.",
/*  VNGERR_NOT_SUPPORTED      */  "A feature is not currently supported.",
/*  VNGERR_INFRA_NS           */  "VNG infrastructure server returned a non-specific error.",
/*  VNGERR_CONN_REFUSED       */  "A connection was refused.",
/*  VNGERR_UNREACHABLE        */  "A peer or server was not reachable.",
/*  VNGERR_INVALID_LOGIN      */  "The login name or passwd is not valid.",
/*  VNGERR_NOT_INVITED        */  "Attempted reject/accept without invite.",
/*  VNGERR_UNKNOWN_NAME       */  "Unknown subscriber name.",
/*  VNGERR_VN_ENDED           */  "The game ended or was aborted",
/*  VNGERR_VN_JOIN_DENIED     */  "Host/server denied a join request.",
/*  VNGERR_VN_MISSING_PROC    */  "Missing RPC procedure.",
/*  VNGERR_VN_MISSING_STATUS  */  "Missing session status value.",
/*  VNGERR_VN_NOT_HOST        */  "The API is only valid for the VN owner.",
/*  VNGERR_INVALID_ARG        */  "An argument to an API was invalid.",
/*  VNGERR_INVALID_VNG        */  "The VNG associated with an API was invalid.",
/*  VNGERR_INVALID_VN         */  "The VN associated with an API was invalid.",
/*  VNGERR_INVALID_MEMB       */  "A member value associated with an API was invalid.",
/*  VNGERR_INVALID_LEN        */  "A length argument to an API was invalid.",
/*  VNGERR_MAILBOX_FULL       */  "Recipient has the max allowed stored messages.",
/*  VNGERR_OUTBOX_FULL        */  "Sender has sent the max allowed stored messages.",
/*  VNGERR_STM_SEND           */  "Message not stored because of send failure.",
/*  VNGERR_STM_DELETE         */  "Attempt to delete message for which login was not a recipient.",
/*  VNGERR_VOICE_IN_USE       */  "Voice is already in use by the user.",
/*  VNGERR_RM_API             */  "An error occurred attempting to access a VNG API.",
/*  VNGERR_ACCESS             */  "Calling thread does not have permission.",
/*  VNGERR_MAX_REACHED        */  "A limited resource has reached its max.",
};

static const char*  __msgs_zh_cn [sizeof __err_codes] = {
/*  VNGERR_UNKNOWN            */  "A failure occured that does not have a specific error message.",
/*  VNGERR_NOWAIT             */  "Non-blocking call returns immediately.",
/*  VNGERR_TIMEOUT            */  "Timeout before operation completed.",
/*  VNGERR_NO_VN_SERVER       */  "A VN server connection was not available.",
/*  VNGERR_NOT_LOGGED_IN      */  "An action was attempted that requires login.",
/*  VNGERR_OVERFLOW           */  "Output value buffer overflow.",
/*  VNGERR_NOMEM              */  "Out of memory.",
/*  VNGERR_FINI               */  "The VNG session is not initialized or has been terminated",
/*  VNGERR_NOT_FOUND          */  "Info being retrieved was not found.",
/*  VNGERR_BUF_FULL           */  "An API failed because an internal buffer was full.",
/*  VNGERR_NOT_SUPPORTED      */  "A feature is not currently supported.",
/*  VNGERR_INFRA_NS           */  "VNG infrastructure server returned a non-specific error.",
/*  VNGERR_CONN_REFUSED       */  "A connection was refused.",
/*  VNGERR_UNREACHABLE        */  "A peer or server was not reachable.",
/*  VNGERR_INVALID_LOGIN      */  "The login name or passwd is not valid.",
/*  VNGERR_NOT_INVITED        */  "Attempted reject/accept without invite.",
/*  VNGERR_UNKNOWN_NAME       */  "Unknown subscriber name.",
/*  VNGERR_VN_ENDED           */  "The game ended or was aborted",
/*  VNGERR_VN_JOIN_DENIED     */  "Host/server denied a join request.",
/*  VNGERR_VN_MISSING_PROC    */  "Missing RPC procedure.",
/*  VNGERR_VN_MISSING_STATUS  */  "Missing session status value.",
/*  VNGERR_VN_NOT_HOST        */  "The API is only valid for the VN owner.",
/*  VNGERR_INVALID_ARG        */  "An argument to an API was invalid.",
/*  VNGERR_INVALID_VNG        */  "The VNG associated with an API was invalid.",
/*  VNGERR_INVALID_VN         */  "The VN associated with an API was invalid.",
/*  VNGERR_INVALID_MEMB       */  "A member value associated with an API was invalid.",
/*  VNGERR_INVALID_LEN        */  "A length argument to an API was invalid.",
/*  VNGERR_MAILBOX_FULL       */  "Recipient has the max allowed stored messages.",
/*  VNGERR_OUTBOX_FULL        */  "Sender has sent the max allowed stored messages.",
/*  VNGERR_STM_SEND           */  "Message not stored because of send failure.",
/*  VNGERR_STM_DELETE         */  "Attempt to delete message for which login was not a recipient.",
/*  VNGERR_VOICE_IN_USE       */  "Voice is already in use by the user.",
/*  VNGERR_RM_API             */  "An error occurred attempting to access a VNG API.",
/*  VNGERR_ACCESS             */  "Calling thread does not have permission.",
/*  VNGERR_MAX_REACHED        */  "A limited resource has reached its max.",
};


static const char*  *_vng_msgs_by_locale[] = {
    __msgs_en_us,    /* _VNG_EN_US */
    __msgs_zh_cn,    /* _VNG_ZH_CN */
};


static _VNG_LOCALE  current_locale = _VNG_DEFAULT_LOCALE;





VNGErrCode  vnaErrMsg (VNGErrCode  errcode,
                       char       *msg,
                       size_t      msgLen)
{
    const char *res = NULL;
    int  msgs_len = (sizeof __err_codes)/(sizeof __err_codes[0]);
    int  i;
    char nullChar = '\0';

    const char  **msgs = _vng_msgs_by_locale [current_locale];

    if (!msg && msgLen)
        return VNGERR_INVALID_LEN;

    if (errcode > 0  && errcode < VNGERR_MAX) {

        for (i = 0;  i < msgs_len;  ++i) {
            if (__err_codes[i] == errcode) {
                res = msgs[i];
                break;
            }
        }
    }

    if (res == NULL) {
        if (msgLen) {
            memcpy(msg, &nullChar, sizeof nullChar);
        }
        return VNGERR_NOT_FOUND;
    }

    if (msgLen) {
        size_t resLen = strnlen(res, msgLen);
        size_t cpyLen = resLen;
        if(resLen < msgLen) {
            ++cpyLen;
        }
        memcpy(msg,res,cpyLen);
        if (resLen == msgLen) {
            memcpy(&msg[msgLen-1], &nullChar, sizeof nullChar);
            return VNGERR_INVALID_LEN;
        }
    }

    return VNG_OK;
}




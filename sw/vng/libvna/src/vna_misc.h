/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#ifndef __VNA_MISC_H__
#define __VNA_MISC_H__  1


#include "vng.h"

#ifdef __cplusplus
extern "C" {
#endif



VNGErrCode  vnaErrMsg (VNGErrCode  errcode,
                       char       *msg,
                       size_t      msgLen);




#ifdef  __cplusplus
}
#endif


#endif // __VNA_MISC_H__

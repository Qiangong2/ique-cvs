/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */

#ifndef __LCE_RM_JT_H__
#define __LCE_RM_JT_H__

#define LCE_RM_FUNC_ID_MIN                  1001

#define LCE_FI_INIT                         1001
#define LCE_FI_SETDUMMYDATA                 1002
#define LCE_FI_SETDELAY                     1003
#define LCE_FI_SETFRAMELEN                  1004
#define LCE_FI_ENABLE                       1005
#define LCE_FI_DISABLE                      1006
#define LCE_FI_SWITCHMODE                   1007
#define LCE_FI_TRIGGER                      1008
#define LCE_FI_SCHEDSIOINTR                 1009
#define LCE_FI_VBLANK                       1010

#define LCE_RM_FUNC_ID_MAX                  1010


#ifndef _GBA
#include <sc/ios.h>
typedef void (*LCERmApiWrapper) (int in);

LCERmApiWrapper  _lce_rm_getApiWrapper (u32 func_id);
#endif



#endif /*  __LCE_RM_JT_H__ */





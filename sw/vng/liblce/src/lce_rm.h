/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */

#ifndef __LCE_RM_H__
#define __LCE_RM_H__  1

#include "vng.h"

#if !defined(_GBA) && !defined(_SC)
#pragma pack(push, 4)
#endif


#define RM_STACK_SIZE   (64*1024)
#define LCE_DEV "/dev/lce"

typedef struct {
    int in;
} LCE_ARG;

typedef struct {
    VN* vn;
    VNMember target;
    VNServiceTag tag;
    int reliable;
    const void *msg;
    size_t msglen;
} VN_TASK;

#define MAX_VN_TASKS 3

#if !defined(_GBA) && !defined(_SC)
#pragma pack(pop)
#endif

#endif /* __LCE_RM_H__ */

/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#include "lce_rm_jt.h"
#include "lce.h"
#include "lce_rm.h"


#define WRAP_FUNC(func_name, func_call) \
static void \
F_##func_name (int in) \
{ \
    func_call; \
}



WRAP_FUNC (LCE_Init,
           LCE_Init ())

WRAP_FUNC (LCE_SetDummyData,
           LCE_SetDummyData ((void*)in))

WRAP_FUNC (LCE_SetDelay,
           LCE_SetDelay ((int)in))

WRAP_FUNC (LCE_SetFrameLen,
           LCE_SetFrameLen ((int)in))

WRAP_FUNC (LCE_Enable,
           LCE_Enable ())

WRAP_FUNC (LCE_Disable,
           LCE_Disable ())

WRAP_FUNC (LCE_SwitchMode,
           LCE_SwitchMode ((int)in))

WRAP_FUNC (LCE_Trigger,
           LCE_Trigger ())

WRAP_FUNC (LCE_SchedSIOIntr,
           LCE_SchedSIOIntr ())

WRAP_FUNC (LCE_Vblank,
           LCE_Vblank ())



static
LCERmApiWrapper __funcWrapper [(LCE_RM_FUNC_ID_MAX + 1) - LCE_RM_FUNC_ID_MIN] = {

/*  Wrapper Func                       Func ID */
/*  __________________________         _______ */
    F_LCE_Init,                     /*  1001   */
    F_LCE_SetDummyData,             /*  1002   */
    F_LCE_SetDelay,                 /*  1003   */
    F_LCE_SetFrameLen,              /*  1004   */
    F_LCE_Enable,                   /*  1005   */
    F_LCE_Disable,                  /*  1006   */
    F_LCE_SwitchMode,               /*  1007   */
    F_LCE_Trigger,                  /*  1008   */
    F_LCE_SchedSIOIntr,             /*  1009   */
    F_LCE_Vblank,                   /*  1010   */
};


LCERmApiWrapper  _lce_rm_getApiWrapper (u32 func_id)
{
    LCERmApiWrapper rv = NULL;

    if (func_id >= LCE_RM_FUNC_ID_MIN && func_id <= LCE_RM_FUNC_ID_MAX) {
        rv = __funcWrapper [func_id - LCE_RM_FUNC_ID_MIN ];
    }

    return rv;
}

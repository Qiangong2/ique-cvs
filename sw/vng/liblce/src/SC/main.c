#include <sc/ios.h>
#include "lce_rm.h"

const u8  _initStack [RM_STACK_SIZE];
const u32 _initStackSize = sizeof(_initStack);
const u32 _initPriority = 20;

int main(void)
{
    rmThreadFunc(0);

    return -1;
}

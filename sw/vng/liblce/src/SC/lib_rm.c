#include "lce_rm.h"
#include "shr_th.h"
#include <sc/priority.h>

extern _SHRThreadRT _SHRThreadCC rmThreadFunc(void* lpParam );

u8  rmStack [RM_STACK_SIZE];
u32 rmStackSize = sizeof(rmStack);
u32 rmPriority = IOS_PRIORITY_LCE_RM;

static _SHRThread rmThread;
static _SHRThreadAttr rmThreadAttr;

int LCE_StartRM(void)
{
    // Start lce receiver thread
    rmThreadAttr.stack = rmStack;
    rmThreadAttr.stackSize = RM_STACK_SIZE;
    rmThreadAttr.priority = rmPriority;
    rmThreadAttr.start = 1;
    rmThreadAttr.attributes = 0;
    _SHR_thread_create(&rmThread, &rmThreadAttr, rmThreadFunc, NULL);

    return 0;
}

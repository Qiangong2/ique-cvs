/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#include <sc/sc.h>
#include <sc/ios.h>
#include <fs.h>
#include "lce_rm.h"
#include "lce_rm_jt.h"
#include "shr_trace.h"
#include "shr_th.h"
#include "lce.h"
#include "vng.h"//XXX
#include "../../examples/include/testcfg.h"//XXX
static char host[64];
static int port;
static char user[64];
static char passwd[64];
static VNG vng;//XXX
static VN vn;//XXX


#define RM       _TRACE_APP, 0

extern VN_TASK vnTasks[MAX_VN_TASKS];
extern int vnTaskCount;
extern IOSTimerId intrTimer;
IOSMessageQueueId  LCErmMq;

static IOSError
lceIoctlv(IOSResourceRequest *req)
{
    IOSError           rv = IOS_ERROR_OK;
    IOSResourceIoctlv *ioctlv = (IOSResourceIoctlv *) &(req->args.ioctlv);
    LCERmApiWrapper    funcWrapper;
    LCE_ARG* a = (LCE_ARG*)ioctlv->vector[0].base;
    extern lce_type* lce;
    u32 cmd;
    int in;

    cmd = ioctlv->cmd;
    in = a->in;

    funcWrapper = _lce_rm_getApiWrapper (cmd);
    trace (FINER, RM, "LCE RM lceIoctlv Just before call API %d\n", cmd);
    memcpy(lce, ioctlv->vector[1].base, ioctlv->vector[1].length);
    funcWrapper (in);
    memcpy(ioctlv->vector[2].base, (void*)&lce->emulated_intr, ioctlv->vector[2].length);
    trace (FINER, RM, "LCE RM lceIoctlv API %d returned %d\n", cmd, rv);

    return rv;
}


static void SendMsg(VN* vn_ptr, VNMember target, VNServiceTag tag,
                    int reliable, const void *msg, size_t msglen)
{
    VNGErrCode ec;
    VNAttr attr;
    
    if (reliable) {
        attr = VN_ATTR_NOENCRYPT;
    } else {
        attr = VN_ATTR_UNRELIABLE|VN_ATTR_NOENCRYPT;
    }

#ifdef DROP_PACKETS
#define DROP_INTERVAL 2
    if (!reliable) {
        static int curr_drop = 0;
        if (++curr_drop > DROP_INTERVAL) {
            trace(INFO, RM, "dropped!\n");
            curr_drop = 0;
            return;
        }
    }
#endif

    ec = VN_SendMsg (vn_ptr, target, tag, msg, msglen, attr, VNG_NOWAIT);

    if (ec != VNG_OK) {
        trace(ERR, RM, "VN_SendMsg() returns err:%d\n", ec);
    }
}


static IOSError
waitForRMCmds ()
{
    IOSError    rv;
    IOSMessage  msg;
    u32 cmd;
    int i;

    while (1) {

        trace (FINEST, RM, "LCE RM waiting for commands\n");
        if ((rv = IOS_ReceiveMessage (LCErmMq, &msg, IOS_MESSAGE_BLOCK)) != IOS_ERROR_OK) {
            trace (ERR, RM, "LCE RM IOS_ReceiveMessage returned %d\n", rv);
            goto end;
        }
        trace (FINEST, RM, "LCE RM received request 0x%08x\n", msg);

        if (msg == (IOSMessage)LCE_Trigger) {
            // Alarm event to set GBA interrupt
            trace (FINEST, RM, "LCE RM GBA Interrupt\n");
            IOS_StopTimer(intrTimer);
            IOS_SendMailbox(0);
            continue;
        }

        IOSResourceRequest *req = (IOSResourceRequest*) msg;
        cmd = req->cmd;
        switch (cmd) {
            case IOS_OPEN:
                trace (FINEST, RM, "LCE RM IOS_OPEN\n");
                rv = IOS_ERROR_OK;
                break;
            case IOS_CLOSE:
                trace (FINEST, RM, "LCE RM IOS_CLOSE\n");
                rv = IOS_ERROR_OK;
                break;
            case IOS_IOCTLV:
                trace (FINEST, RM, "LCE RM IOS_IOCTLV\n");
                rv = lceIoctlv (req);
                break;
            default:
                trace (ERR, RM, "LCE RM cmd %u not supported\n", req->cmd);
                rv = IOS_ERROR_INVALID;
                break;
        }
        trace (FINEST, RM, "LCE RM Returning %d\n", rv);
        IOS_ResourceReply(req, rv);
        for (i=0; i<vnTaskCount; i++) {
            SendMsg(vnTasks[i].vn,
                    vnTasks[i].target,
                    vnTasks[i].tag,
                    vnTasks[i].reliable,
                    vnTasks[i].msg,
                    vnTasks[i].msglen);
        }
        vnTaskCount = 0;
    }


end:
    return rv;
}



int setupVNG()
{
    VNGErrCode ec;
    IOSFd fsFd=-1;
    IOSError           rv;

    printf("reading vng prefs ...\n");

    memset(host, 0, sizeof(host));
    rv = fsFd = ISFS_Open("/host", ISFS_READ_ACCESS);
    if (rv >= 0) {
        printf("reading /host\n");
        rv = ISFS_Read(fsFd, host, sizeof(host));
    } else {
        memcpy(host, "ogs.bbu.lab1.routefree.com", sizeof(host));
    }
    ISFS_Close(fsFd);
    port = VNGS_PORT;
    rv = fsFd = ISFS_Open("/port", ISFS_READ_ACCESS);
    if (rv >= 0) {
        printf("reading /port\n");
        rv = ISFS_Read(fsFd, (void*)&port, sizeof(port));
    }
    ISFS_Close(fsFd);
    memset(user, 0, sizeof(user));
    rv = fsFd = ISFS_Open("/user", ISFS_READ_ACCESS);
    if (rv >= 0) {
        printf("reading /user\n");
        rv = ISFS_Read(fsFd, user, sizeof(user));
    } else {
        memcpy(user, "10001", 6);
    }
    ISFS_Close(fsFd);
    memset(passwd, 0, sizeof(passwd));
    rv = fsFd = ISFS_Open("/passwd", ISFS_READ_ACCESS);
    if (rv >= 0) {
        printf("reading /passwd\n");
        rv = ISFS_Read(fsFd, passwd, sizeof(passwd));
    } else {
        memcpy(passwd, "10001", 6);
    }
    ISFS_Close(fsFd);

    if ((ec = VNG_Init(&vng, NULL))) {
        printf("VNG_Init() failed with %d\n", ec);//debug
        return -1;
    }

#ifndef LOOPBACK
    printf("logging in with - host:%s port:%d user:%s passwd:%s\n",
           host, port, user, passwd);//debug
    if ((ec = VNG_Login (&vng, host, port, user, passwd, VNG_TIMEOUT))) {
        printf("VNG_Login() failed with %d\n", ec);//debug
        VNG_Fini(&vng);
        return -1;
    }
#endif

    return 0;
}

void hostNewGame()
{
    VNGErrCode err;
    VNGGameInfo info;
    VNGUserInfo userInfo;

    // Create a new VN
#ifndef LOOPBACK
    err = VNG_NewVN(&vng, &vn, VN_DEFAULT_CLASS, VN_DOMAIN_INFRA, VN_ABORT_ON_OWNER_EXIT);
#else
    err = VNG_NewVN(&vng, &vn, VN_DEFAULT_CLASS, VN_DOMAIN_LOCAL, VN_ABORT_ON_OWNER_EXIT);
#endif
    if (err != VNG_OK) {
        printf("VNG_NewVN() failed with %d\n", err);//debug
        return;
    }

    // Describe the game information for matchmaking
    memset((void*)&info, 0, sizeof(VNGGameInfo));
    VN_GetVNId(&vn, &info.vnId);
    printf("\n**********devId:%llx netId:%x\n\n", info.vnId.deviceId, info.vnId.netId);//debug
    info.owner = VNG_MyUserId(&vng);
    info.gameId = 327;
    info.titleId = 327;
    info.accessControl = VNG_GAME_PUBLIC;
    info.totalSlots = 2;
    info.buddySlots = 0;   // nothing reserved for buddies
    info.maxLatency = 500; // 500ms RTT 
    info.netZone = 0;      // server will decide this value
    info.attrCount = 0;
    memcpy(&info.keyword, "LCE test!", 11);

    // Register the VN to the directory for matchmaking
    err = VNG_RegisterGame(&vng, &info, NULL, VNG_TIMEOUT);
    if (err != VNG_OK) {
        printf("VNG_RegisterGame failed with %d\n", err);//debug
        return;
    }

    // This game starts when there're two players
#ifdef LOOPBACK
    while (VN_NumMembers(&vn) < 0) { //XXX fast start
#else
    while (VN_NumMembers(&vn) < 2) {
#endif
        uint64_t requestId;
        err = VNG_GetJoinRequest(&vng, info.vnId, NULL, 
            &requestId, &userInfo, NULL, 0, VNG_WAIT /*ms*/);
        if (err != VNG_OK) {
            printf("VNG_GetJoinRequest failed with %d\n", err);//debug
            continue;
        }
        
        err = VNG_AcceptJoinRequest(&vng, requestId);
        if (err != VNG_OK) {
            printf("VNG_AcceptJoinRequest failed with %d\n", err);//debug
        }
    }
}

int searchAndJoinGame()
{
    VNGErrCode err;
    VNGSearchCriteria criteria;
    VNGGameStatus gameStatus[10];
    int numGameStatus;
    int i;
    char joinStr[16];

#ifdef LOOPBACK
    return -1;
#endif
    memset((void*)&criteria, 0, sizeof(VNGSearchCriteria));

    criteria.gameId = 327;   
    criteria.domain = VNG_SEARCH_INFRA;   
    criteria.maxLatency = 500;  // 300ms RTT
    criteria.cmpKeyword = VNG_CMP_DONTCARE;

    memcpy(joinStr, "hello", 6);

    numGameStatus = 10;
    err = VNG_SearchGames(&vng, &criteria, gameStatus, &numGameStatus, 0, VNG_TIMEOUT);
    if (err != VNG_OK) {
        printf("VNG_SearchGames failed with %d\n", err);//debug
    }

    if (numGameStatus > 0) {
	for (i = numGameStatus-1; i >= 0; i--) {
            printf("\n**********devId:%llx netId:%x\n\n", gameStatus[i].gameInfo.vnId.deviceId, gameStatus[i].gameInfo.vnId.netId);//debug
            err = VNG_JoinVN(&vng,
                             gameStatus[i].gameInfo.vnId,
                             joinStr,
                             &vn,
                             NULL,
                             0, 
                             10000);
            if (err == VNG_OK) return 0;
            printf("VNG_JoinVN failed with %d\n", err);//debug
        }
    }

    return -1;
}

_SHRThreadRT _SHRThreadCC rmThreadFunc(void* lpParam ) 
{
    IOSError           rv;
    IOSMessage         msgArray[3];
    IOSFd fsFd=-1; // XXX

    _SHR_set_trace_show_time (0);
    _SHR_set_trace_show_thread (0);
//    _SHR_set_sg_trace_level (_TRACE_APP, 0, FINEST);
    trace (INFO, RM, "LCE RM starting\n");

    // XXX remove
    if ((fsFd = ISFS_Open(LCE_OPTS_FILE, ISFS_READ_ACCESS)) < 0)
    {
    lce_opts opts;

    printf("\n*** Not launched by Viewer or no lce_opts file found ***\n\n");
    trace (INFO, RM, "Setting up VN\n");
    if (setupVNG()<0) {
        printf("vng setup error\n");
    }
    if (searchAndJoinGame() < 0) {
        printf("hostNewGame\n");//debug
        hostNewGame();
    }

    printf("Writing LCE preferences file\n");//debug
    opts.delay = 10;
    VN_GetVNId(&vn, &opts.vnid);
    opts.voice_enable = 1;
    opts.voice_codec = VNG_VOICE_CODEC_ADPCM32;
    opts.voice_volume = 105;

    rv = ISFS_CreateFile(LCE_OPTS_FILE, 0,
                    ISFS_RW_ACCESS, ISFS_RW_ACCESS, ISFS_RW_ACCESS);
    if (rv < 0)  {
        printf("Cannot create file\n");
        goto end;
    }
    rv = fsFd = ISFS_Open(LCE_OPTS_FILE, ISFS_WRITE_ACCESS);
    if (fsFd < 0) {
        printf("cannot open file for write\n");
        goto end;
    }
    rv = ISFS_Write(fsFd, (void*)&opts, sizeof(opts));
    if (rv < 0) {
        printf("File write failed \n");
        goto end;
    }
    ISFS_Close(fsFd);
    } else {
        ISFS_Close(fsFd);
        printf("\n*** Assuming launched by Viewer ***\n\n");
    }

    /* Create a message queue */
    if ((LCErmMq = IOS_CreateMessageQueue (msgArray, sizeof(msgArray)/sizeof(msgArray[0]))) < 0) {
        rv = LCErmMq;
        trace (ERR, RM, "LCE RM IOS_CreateMessageQueue for resource manager returned %d\n", rv);
        goto end;
    }

    /* Register the message queue with the kernel */
    if ((rv = IOS_RegisterResourceManager (LCE_DEV, LCErmMq)) != IOS_ERROR_OK) {
        trace (ERR, RM, "LCE RM IOS_RegisterResourceManager returned %d\n", rv);
        goto end;
    }

    rv = waitForRMCmds (LCErmMq);

end:
    /* should never get here */
    trace (ERR, RM, "LCE RM exiting with error %d\n", rv);

    return (void*)rv;
}

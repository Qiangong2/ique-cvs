#include "lce.h"
#include "vng.h"
#include "shr_trace.h"
#include "shr_th.h"
#include "sc/ios.h"
#include "fs.h"
#include "lce_rm.h"
#include <sc/audbuf.h>
#include <sc/priority.h>
#include "internal.h" // libvoice

/* macros */
/* ******************* */
#define LCE _TRACE_APP, 1
#ifdef DEBUG
#define TRACE(x) trace x
#else
#define TRACE(x)
#endif
//#define PRINTF1(x) printf x
#define PRINTF1(x)
//#define PRINTF(x) printf x
#define PRINTF(x)
/* ******************* */

/* defines */
/* ******************* */
#define MAX32 0xffffffff
#define MAX_PEERS 4
#define MAX_FRAMELEN 20
#define MAX_DELAY 60
#define BULKFRAMES 2
/* ******************* */

/* enums */
/* ******************* */
enum {
    ENABLE,
    DISABLE,
    MODE,
    DATA,
    BULK,
    INIT,
} PKT_TYPE;
/* ******************* */

/* structs */
/* ******************* */
typedef struct {
    u32 frameLen;
} enable_pkt;

typedef struct {
    u32 mode;
    u32 delay;
    u32 target_frame;
} mode_pkt;

typedef struct {
    u32 target_frame;
    u16 frame_size;
    u16 mode;
} data_pkt_hdr;

typedef struct {
    data_pkt_hdr hdr;
    u16 data[MAX_FRAMELEN];
} data_pkt;

typedef struct {
    u32 size;
    u32 num_frames;
} bulk_pkt_hdr;

typedef struct {
    bulk_pkt_hdr hdr;
    data_pkt frame[BULKFRAMES];
} bulk_pkt;

typedef struct {
    u32 num_gbas;
    VNMember vnMembers[MAX_PEERS];
} init_pkt;

typedef struct {
    u16 data;
    u32 frame;
} linkdata;
/* ******************* */

/* private variables */
/* ******************* */
#ifdef _SC
#define STACK_SIZE 16*1024
u8 recvThreadStack[STACK_SIZE];
u8 voiceThreadStack[STACK_SIZE];
#endif
static _SHRMutex hMutex;
static _SHRThread recvThread;
static _SHRThreadAttr recvThreadAttr;
static _SHRThread voiceThread;
static _SHRThreadAttr voiceThreadAttr;
static int waiting[MAX_PEERS];
static IOSMessageQueueId notifyMq[MAX_PEERS];
static IOSMessageQueueId readyMq;
static IOSMessageQueueId sendMq;
static IOSMessage notifyMsgArray[MAX_PEERS][1];
static IOSMessage readyMsgArray[1];
static IOSMessage sendMsgArray[1];
static VNG vng;
static VN vn;
static VNMember vnMembers[MAX_PEERS];
static u16 default_dummy_data[MAX_PEERS][2][MAX_FRAMELEN];
static u16* dummy_data[MAX_PEERS][2];
static int my_link_id = 0;
static unsigned int num_gbas;
static bool enabled[MAX_PEERS] = { false, false, false, false };
static int frameLen = 0;
static int send_counter = 0;
static int send_frame = 0;
static int switchToMode = LCE_NORMAL_MODE;
static int switchToModeFrame = 0;
static int switchModeSent = 0;
static data_pkt jitterQ[MAX_PEERS][MAX_DELAY*2];
static char __send_buf[sizeof(bulk_pkt)+sizeof(XmitData)];
static char __reliable_buf[sizeof(bulk_pkt_hdr)+
                           MAX_DELAY*sizeof(data_pkt)];
static char recv_buf[sizeof(bulk_pkt_hdr)+
                     MAX_DELAY*sizeof(data_pkt)+
                     sizeof(XmitData)];
static struct  {
    bulk_pkt_hdr hdr;
    XmitData data;
} tmpXmitBuf;
static int tmpXmitSize = 0;
u8 recBuf[1024] __attribute__((aligned(16)));
u8 playBuf[1024] __attribute__((aligned(16)));
u8 voiceInbuf[4096] __attribute__((aligned(16)));
u8 voiceOutbuf[4096] __attribute__((aligned(16)));
static data_pkt* send_buf;
static data_pkt* reliable_buf;
static int reliable_count;
static int prevVblank = 0;
static volatile int initialized = 0;
static lce_type nc_lce;
/* ******************* */

/* shared variables */
/* ******************* */
extern IOSMessageQueueId LCErmMq;
VN_TASK vnTasks[MAX_VN_TASKS];
int vnTaskCount = 0;
IOSTimerId intrTimer = -1;
lce_type* lce = &nc_lce;
/* ******************* */


/* private functions */
/* ******************* */
static void switchMode(int mode);
static void scheduleSIOs();
static void* getNextSendBuf(data_pkt* p);
static void sendBulkPkt();
static int isEmptyJQ(int id);
/* ******************* */

static void __SendMsg(VNMember target, VNServiceTag tag, int reliable,
                      const void *msg, size_t msglen)
{
    VNGErrCode ec;
    VNAttr attr;
    
    if (reliable) {
        attr = VN_ATTR_NOENCRYPT;
    } else {
        attr = VN_ATTR_UNRELIABLE|VN_ATTR_NOENCRYPT;
    }

#ifdef DROP_PACKETS
#define DROP_INTERVAL 2
    if (!reliable) {
        static int curr_drop = 0;
        if (++curr_drop > DROP_INTERVAL) {
            PRINTF(("dropped! fcl:%d\n", lce->frame_counter_limit));
            curr_drop = 0;
            return;
        }
    }
#endif

    ec = VN_SendMsg (&vn, target, tag, msg, msglen, attr, VNG_NOWAIT);

    if (ec != VNG_OK) {
        TRACE((TRACE_ERROR, LCE, "VN_SendMsg() returns err:%d\n", ec));
    }
}

static void SendMsg(VNMember target, VNServiceTag tag,
                    int reliable, int now,
                    const void *msg, size_t msglen)
{
    if (now) {
        __SendMsg(target, tag, reliable, msg, msglen);
        return;
    }

    if (vnTaskCount >= MAX_VN_TASKS) {
        TRACE ((ERR, LCE, "SendMsg - too many vn tasks:%d\n", vnTaskCount));
        return;
    }

    vnTasks[vnTaskCount].vn = &vn;
    vnTasks[vnTaskCount].target = target;
    vnTasks[vnTaskCount].tag = tag;
    vnTasks[vnTaskCount].reliable = reliable;
    vnTasks[vnTaskCount].msg = msg;
    vnTasks[vnTaskCount].msglen = msglen;

    ++vnTaskCount;
}


static void setIntrTimer(IOSTime time)
{
    if (intrTimer < 0) {
        intrTimer = IOS_CreateTimer(time, 0, LCErmMq, (IOSMessage)LCE_Trigger);
        if (intrTimer < 0) {
            TRACE ((ERR, LCE, "IOS_CreateTimer returns %d\n", intrTimer));
        }
    } else {
        IOS_RestartTimer(intrTimer, time, 0);
    }
}

static int getLinkId(VNMember mbr) 
{
    int i;

    for (i=0; i<MAX_PEERS; i++) {
        if (vnMembers[i] == mbr) {
            return i;
        }
    }

    return 0;// error
}

static void checkEnable()
{
    unsigned int i;
    bool allEnabled = true;

    //  if rcv'ed SYNC from all peers,
    //      lce->frame_counter_limit = 0
    if (!lce->enable) {
        for (i=0; i<num_gbas; i++) {
            if (!enabled[i]) {
                allEnabled = false;
                break;
            }
        }
        if (allEnabled) {
            TRACE((TRACE_FINER, LCE, "ENABLED!\n"));
            lce->frame_counter = 0;
            lce->frame_counter_limit = 0;
            lce->enable = 1;
        }
    }
}

static void checkMode()
{
    mode_pkt msg;

    if (lce->mode != switchToMode) {
        if (!my_link_id && 
            !switchModeSent) {
            if (lce->mode == LCE_NORMAL_MODE) {
                switchToModeFrame = lce->frame_counter_limit;
            } else {
                switchToModeFrame = lce->frame_counter_limit + lce->delay;
            }
            PRINTF1(("Set switchToMode:%d switchToModeFrame%d\n",
                     switchToMode, switchToModeFrame));

            // Send switch mode control pkt to all slaves
            msg.mode = switchToMode;
            msg.delay = lce->delay;
            msg.target_frame = switchToModeFrame;
            SendMsg (VN_MEMBER_OTHERS, MODE, 1, 1, &msg, sizeof(msg));
            PRINTF1(("Sent mode_pkt\n"));
            switchModeSent = 1;
        }

        if (switchToModeFrame == lce->frame_counter) {
            switchMode(switchToMode);
        }
    }
}

static void pushJQ(int id, u16 mode, u32 frame, int offset, u16 data)
{
    int index;
    data_pkt* dpkt;

    index = frame % (MAX_DELAY*2);
    dpkt = &jitterQ[id][index];

    dpkt->hdr.target_frame = frame;
    dpkt->hdr.frame_size = offset+1;
    dpkt->hdr.mode = mode;
    dpkt->data[offset] = data;
}

static void insertJQ(int id, data_pkt* dpkt)
{
    int tmp_frame = dpkt->hdr.target_frame;
    int index = tmp_frame % (MAX_DELAY*2);
    
    if (tmp_frame < lce->frame_counter_limit) {
        PRINTF1(("insertJQ() - target_frame:%d < frame_counter_limit:%d\n",tmp_frame, lce->frame_counter_limit));
        return;
    } else if (tmp_frame > 
               lce->frame_counter_limit + lce->delay*2) {
        PRINTF1(("insertJQ() - target_frame:%d > lce->frame_counter_limit:%d + delay:%d * 2\n", tmp_frame, lce->frame_counter_limit, lce->delay));
        return;
    } else if ((jitterQ[id][index].hdr.target_frame == 
               tmp_frame) &&
               jitterQ[id][index].hdr.mode == LCE_NORMAL_MODE &&
               dpkt->hdr.mode != LCE_NORMAL_MODE) {
        PRINTF1(("insertJQ() - Shouldn't overwite NORMAL mode\n"));
        PRINTF1(("target_frame:%d frame_size:%d\n", tmp_frame, dpkt->hdr.frame_size));
    }
    
    PRINTF(("target_frame:%d index:%d\n", tmp_frame, index));
    memcpy(&jitterQ[id][index], dpkt, sizeof(data_pkt));

    // Check whether to notify
    if (waiting[id] && dpkt->hdr.mode == lce->mode) {
        if (tmp_frame == lce->frame_counter) {
            waiting[id] = 0;
            PRINTF(("notify popJQ id:%d\n", id));
            // notify popJQ
            IOS_SendMessage(notifyMq[id], 0, IOS_MESSAGE_NOBLOCK);
        }
    }
    
    if (my_link_id && id == 0) { // SLAVE
        if ((tmp_frame == lce->frame_counter ||
             tmp_frame == lce->frame_counter + 1) &&
            lce->frame_counter == lce->frame_counter_limit) {
            // notify sched sio intrs
            PRINTF(("notify sched sio intrs\n"));
            scheduleSIOs();
        }
    }
}

static data_pkt* getJQ(int id, u32 frame)
{
    int index;
    data_pkt* dpkt;

    index = frame % (MAX_DELAY*2);
    dpkt = &jitterQ[id][index];

    if (dpkt->hdr.target_frame != frame) {
        dpkt = 0;
    }

    return dpkt;
}

static u16 popJQ(int id)
{
    data_pkt* dpkt;
    IOSMessage  msg;

    while (isEmptyJQ(id)) {
        waiting[id] = 1;
        PRINTF1(("popJQ - Data not available - id:%d fcl:%d\n", id, lce->frame_counter_limit));
        data_pkt* dpkt = getJQ(id, lce->frame_counter_limit);
        if (dpkt == 0) {
            PRINTF1(("dpkt: 0\n"));
        } else {
            PRINTF1(("dpkt->hdr.mode:%d dpkt->hdr.frame_size:%d send_counter:%d next_dpkt:%x\n",
                     dpkt->hdr.mode, dpkt->hdr.frame_size, send_counter, getJQ(id, lce->frame_counter_limit+1)));
        }

        // Data not available
        _SHR_mutex_unlock(&hMutex);
        IOS_ReceiveMessage(notifyMq[id], &msg, IOS_MESSAGE_BLOCK);
        _SHR_mutex_lock(&hMutex);
    }

    dpkt = getJQ(id, lce->frame_counter_limit);
    
    if (dpkt->hdr.frame_size <= send_counter) {
        // No data available
        return 0;
    }

    return dpkt->data[send_counter];
}

static void emptyJQ(int id)
{
    int i;
    for (i=0; i<MAX_DELAY*2; i++) {
        jitterQ[id][i].hdr.target_frame = MAX32;
    }
}


static int isEmptyJQ(int id)
{
    data_pkt* dpkt = getJQ(id, lce->frame_counter_limit);

    if (dpkt) {
        if (dpkt->hdr.mode != lce->mode) {
            return 1;
        }
        if (dpkt->hdr.mode == LCE_NORMAL_MODE) {
            return (dpkt->hdr.frame_size < send_counter+1);
        } else {
            return 0;
        }
    } else {
        return 1;
    }
}


static void disableAll()
{
    int i;

    lce->emulated_intr = 0;
    lce->frame_counter = 0;
    lce->frame_counter_limit = MAX32;
    lce->mode = LCE_NORMAL_MODE;
    lce->siomulti0 = 0xffff;
    lce->siomulti1 = 0xffff;
    lce->siomulti2 = 0xffff;
    lce->siomulti3 = 0xffff;
    lce->enable = false;

    for (i=0; i<MAX_PEERS; i++) {
        enabled[i] = false;
        emptyJQ(i);
    }
}

static void parseInitPkt(char* pkt, size_t size)
{
    init_pkt* initPkt = (init_pkt*)pkt;
    int i;

    if (size != sizeof(init_pkt)) {
        TRACE((TRACE_FINEST, LCE, "parseInitPkt: invalid len!!! sizeof(init_pkt):%d size:%d\n", 
               sizeof(init_pkt), size));
        /* invalid len */
        return;
    }

    TRACE((TRACE_FINEST, LCE, "Received init pkt - num_gbas:%d mem[0]:%d mem[1]:%d mem[2]:%d mem[3]:%d\n", initPkt->num_gbas, initPkt->vnMembers[0], initPkt->vnMembers[1], initPkt->vnMembers[2], initPkt->vnMembers[3]));

    num_gbas = initPkt->num_gbas;
    for (i=0; i<num_gbas; i++) {
        vnMembers[i] = initPkt->vnMembers[i];
        if (VN_Self(&vn) == vnMembers[i]) {
            my_link_id = i;
            lce->linkid = i;
        }
    }

    initialized = 1;
}


static void parseBulkPkt(int link_id, char* pkt, size_t size)
{
    bulk_pkt* bpkt = (bulk_pkt*)pkt;
    int nFrames;
    int i;
    data_pkt* p;
    
    PRINTF(("parseBulkPkt(%d, %x, %d)\n", link_id, pkt, size));
    _SHR_mutex_lock(&hMutex);

    if (size < sizeof(bulk_pkt_hdr)) {
        TRACE((TRACE_WARN, LCE, "parseBulkPkt: invalid len!!! sizeof(bulk_pkt_hdr):%d size:%d\n", 
               sizeof(bulk_pkt_hdr), size));
        _SHR_mutex_unlock(&hMutex);
        return;
    }

    PRINTF(("*** bulk - size:%d num_frames:%d\n", bpkt->hdr.size, bpkt->hdr.num_frames));
#ifdef LOOPBACK
    link_id = 1;
#endif

    nFrames = bpkt->hdr.num_frames;
    p = &bpkt->frame[0];

    // Add to Q
    for (i=0; i<nFrames; i++) {
        insertJQ(link_id, (void*)p);
        p = getNextSendBuf(p);
    }

    _SHR_mutex_unlock(&hMutex);

    if (size > bpkt->hdr.size) {
        int tmp;
        XmitData* xmitBuf = (XmitData*)(pkt + bpkt->hdr.size);
        // check xmitBuf size
        tmp = size - bpkt->hdr.size;
        if (tmp >= sizeof(XmitDataHdr) &&
            tmp == sizeof(XmitDataHdr) +
            xmitBuf->hdr.dataSize) {
            onNetRecv(link_id, xmitBuf);
        } else {
            TRACE((TRACE_WARN, LCE, "voice XmitData: invalid len!!! size:%d bpkt->hdr.size:%d expected:%d\n", size, bpkt->hdr.size, sizeof(XmitDataHdr) + xmitBuf->hdr.dataSize));
        }
    }
}

_SHRThreadRT _SHRThreadCC voiceThreadFunc(void* lpParam ) 
{
    IOSError    rv;
    IOSMessage  msg;

    audbufRead(recBuf, sizeof(recBuf), MONO, 0);// prime buffers

    while (1) {
        rv = audbufRead(recBuf, sizeof(recBuf), MONO, 1);
        if (rv > 0) {
            tmpXmitSize = sizeof(XmitData);
            makeXmitBuf(recBuf, sizeof(recBuf), &tmpXmitBuf.data, &tmpXmitSize);
            if (tmpXmitSize > 0) {
                IOS_SendMessage(readyMq, 0, IOS_MESSAGE_BLOCK);
                IOS_ReceiveMessage (sendMq, &msg, IOS_MESSAGE_BLOCK);
            }
            getNextPlayBuf(playBuf, sizeof(playBuf));
            audbufWrite(playBuf, sizeof(playBuf), MONO);
        }
    }
}

_SHRThreadRT _SHRThreadCC recvThreadFunc(void* lpParam ) 
{
    //On Receive:
    //  If CONTROL SYNC pkt
    //      if lce->enabled && rcv'ed SYNC from all peers,
    //          lce->frame_counter_limit = 0
    //  if DATA pkg
    //      push to peer queue
    //

    VNGErrCode ec;
    size_t msglen;
    VNMsgHdr hdr;

    while (1) {
        msglen = sizeof(recv_buf);
        TRACE((TRACE_FINEST, LCE, "Waiting for Msg...\n"));
#if 0
        ec = VN_RecvMsg(&vn, VN_MEMBER_ANY, VN_SERVICE_TAG_ANY, 
            &recv_buf, &msglen, &hdr, VNG_WAIT);
#else
        ec = VN_RecvMsg(&vn, VN_MEMBER_ANY, VN_SERVICE_TAG_ANY, 
            &recv_buf, &msglen, &hdr, 1000);
#endif
        if (ec != VNG_OK) {
            if (ec != VNGERR_TIMEOUT) {
                TRACE((TRACE_ERROR, LCE, "VN_RecvMsg() returns err:%d\n", ec));
            }
            PRINTF1(("NC-LCE q0empty:%d q1empty:%d tc:%d tcl:%d fc:%d fcl:%d\n",
                   isEmptyJQ(0), isEmptyJQ(1),
                   lce->timer_counter,
                   lce->timer_counter_limit, 
                   lce->frame_counter, 
                   lce->frame_counter_limit));
            continue;
        }
        TRACE((TRACE_FINEST, LCE, "Received Msg!\n"));

        TRACE((TRACE_FINEST, LCE, "VN_RecvMsg - tag:%d\n", hdr.serviceTag));
        switch (hdr.serviceTag) {
            case ENABLE:
                _SHR_mutex_lock(&hMutex);
                enabled[getLinkId(hdr.sender)] = true;
                if (my_link_id) {
                    frameLen = ((enable_pkt*)recv_buf)->frameLen;
                }
                _SHR_mutex_unlock(&hMutex);
                break;
            case DISABLE:
                _SHR_mutex_lock(&hMutex);
                if (lce->enable) {
                    disableAll();
                }
                _SHR_mutex_unlock(&hMutex);
                break;
            case MODE:
                _SHR_mutex_lock(&hMutex);
                if (my_link_id) {
                    lce->delay = ((mode_pkt*)recv_buf)->delay;
                    switchToMode = ((mode_pkt*)recv_buf)->mode;
                    switchToModeFrame = ((mode_pkt*)recv_buf)->target_frame;
                    PRINTF1(("received MODE - switchToMode:%d switchToModeFrame:%d\n,", switchToMode, switchToModeFrame));
                    if (switchToModeFrame == lce->frame_counter &&
                        switchToMode != lce->mode) {
                        switchMode(switchToMode);
                        scheduleSIOs();
                    } else if (switchToModeFrame == lce->frame_counter+1
                               && !lce->emulated_intr) {
                        lce->frame_counter_limit++;
                        TRACE((TRACE_FINER, LCE, "new frame_counter_limit: %d\n", lce->frame_counter_limit));
                        PRINTF1(("mode - new frame_counter_limit: %d\n", lce->frame_counter_limit));
                    }
                }
                _SHR_mutex_unlock(&hMutex);
                break;
            case DATA:
                parseBulkPkt(getLinkId(hdr.sender), recv_buf, msglen);
                break;
            case INIT:
                _SHR_mutex_lock(&hMutex);
                parseInitPkt(recv_buf, msglen);
                _SHR_mutex_unlock(&hMutex);
                break;
            default:
                /* invalid pkt */
                TRACE((TRACE_ERROR, LCE, "recvThreadFunc() - invalid pkt %d\n", hdr.serviceTag));
                break;
        }
    }
}


// Initialize LCE library
//   LCE_Init should be called by viewer before launching 
//   multiplayer game.
//   The LCE is initialized to LCE_NORMAL_MODE by default.
//
void LCE_Init()
{
    int i, j;
    init_pkt initPkt;
    lce_opts opts;
    IOSFd fsFd=1;
#define NUM_VNS 5
    VN vns[NUM_VNS];
    VNId tmpid;
    TRACE((TRACE_FINE, LCE, "LCE_Init!\n"));

    // Set up LCE Shared memory
    memset(lce, 0, sizeof(lce_type));
    disableAll();

    // Read preferences
    if ((fsFd = ISFS_Open(LCE_OPTS_FILE, ISFS_READ_ACCESS)) >= 0) {
        if(ISFS_Read(fsFd, (void*)&opts, sizeof(lce_opts))
           != sizeof(lce_opts)) {
            TRACE((TRACE_ERROR, LCE, "size of %s does not match sizeof(lce_opts):%d\n", LCE_OPTS_FILE, sizeof(lce_opts)));
        }
        ISFS_Close(fsFd);
    }
    if ((fsFd = ISFS_Open(LCE_DEBUG_FILE, ISFS_READ_ACCESS)) >= 0) {
        if(ISFS_Read(fsFd, (void*)&i, sizeof(i))
           == sizeof(i)) {
            _SHR_set_sg_trace_level (_TRACE_APP, 1, i);
        }
        ISFS_Close(fsFd);
    }
    //_SHR_set_sg_trace_level (_TRACE_APP, 1, 5);//debug

    // Get vn
    VNG_Init(&vng, NULL);
    i = NUM_VNS;
    VNG_GetVNs (&vng, vns, &i) ;
    for (j=0; j<i; j++) {
        VN_GetVNId(&vns[j], &tmpid);
        if (tmpid.netId == opts.vnid.netId) {
            VNG_CloneVN(&vng, &vn, &vns[j]);
            break;
        }
    }
    if (j == i) {
        TRACE((TRACE_ERROR, LCE, "Cannot find VN!\n"));
        while (1) _SHR_thread_sleep(100);
    }

    // Set vn info
    my_link_id = (VN_Self(&vn) != VN_Owner(&vn));
    num_gbas = VN_NumMembers(&vn);
    if (num_gbas > MAX_PEERS) num_gbas = MAX_PEERS;
    lce->linkid = my_link_id;

    // Master sends vn->gba mapping to everyone
    if (!my_link_id) {
        VN_GetMembers(&vn, vnMembers, num_gbas);
        initPkt.num_gbas = num_gbas;
        initPkt.vnMembers[0] = VN_Self(&vn);
        for (i=1, j=0; j<num_gbas; j++) {
            if (vnMembers[j] != initPkt.vnMembers[0]) {
                initPkt.vnMembers[i] = vnMembers[j];
                i++;
            }
        }
        SendMsg(VN_MEMBER_ALL, INIT, 1, 1, &initPkt, sizeof(initPkt));
    }

    // Set default dummy data
    for (i=0; i<MAX_PEERS; i++) {
        for (j=0; j<2; j++) {
            memset(default_dummy_data[i][j], 0, MAX_FRAMELEN);
            dummy_data[i][j] = default_dummy_data[i][j];
        }
    }

    // Init mutex and events
    for (i=0; i<MAX_PEERS; i++) {
        if ((notifyMq[i] = IOS_CreateMessageQueue(notifyMsgArray[i], 1)) < 0) {
            TRACE((TRACE_ERROR, LCE, "IOS_CreateMessageQueue for notifyMq returned %d\n", notifyMq[i]));
        }
    }
    _SHR_mutex_init(&hMutex, _SHR_MUTEX_RECURSIVE, 0, 0);

    LCE_SetDelay(opts.delay);

    // Init libVoice
    if (opts.voice_enable) {
        IOSError rv;
        u32 config = AUD_CONFIG_ADC_8_DAC_8;
        u32 readSize = 2048;
        u32 inOv = AUDBUF_OVERFLOW_DISCARD_OLD;
        u32 outOv = AUDBUF_OVERFLOW_DISCARD_OLD;
        
        if ((rv = audbufOpen(O_RDWR, config, readSize,
                             voiceInbuf,sizeof(voiceInbuf),inOv,
                             voiceOutbuf,sizeof(voiceOutbuf),outOv)) 
            != IOS_ERROR_OK ) 
        {
            TRACE((TRACE_ERROR, LCE, "audbufOpen failed, rv = %d\n",rv));
        }
        audbufSetVolume(opts.voice_volume);
        initVoice(0);
        VNG_SelectVoiceCodec(0, opts.voice_codec);
    }

    // Start net receiver thread
    recvThreadAttr.stack = recvThreadStack;
    recvThreadAttr.stackSize = STACK_SIZE;
    recvThreadAttr.priority = IOS_PRIORITY_LCE_RECV;
    recvThreadAttr.start = 1;
    recvThreadAttr.attributes = 0;
    _SHR_thread_create(&recvThread, &recvThreadAttr, recvThreadFunc, NULL);

    // Create sender thread mq's
    if ((readyMq = IOS_CreateMessageQueue(readyMsgArray, 1)) < 0) {
        TRACE((TRACE_ERROR, LCE, "IOS_CreateMessageQueue for readyMq returned %d\n", readyMq));
    }
    if ((sendMq = IOS_CreateMessageQueue(sendMsgArray, 1)) < 0) {
        TRACE((TRACE_ERROR, LCE, "IOS_CreateMessageQueue for senderMq returned %d\n", sendMq));
    }
    
    // Start voice thread
    if (opts.voice_enable) {
        voiceThreadAttr.stack = voiceThreadStack;
        voiceThreadAttr.stackSize = STACK_SIZE;
        voiceThreadAttr.priority = IOS_PRIORITY_LCE_VOICE;
        voiceThreadAttr.start = 1;
        voiceThreadAttr.attributes = 0;
        _SHR_thread_create(&voiceThread, &voiceThreadAttr, voiceThreadFunc, NULL);
    }

    // Wait for vn->gba mapping to be received
    while (!initialized) {
        _SHR_thread_sleep(100);
    }
}

// Specify dummy data to initialize delay queue.
//   buf: Pointer to 1 frame of dummy data.
//
void LCE_SetDummyData(u16* buf[4][2])
{
    int i, j;

    _SHR_mutex_lock(&hMutex);

    TRACE((TRACE_FINE, LCE, "LCE_SetDummyData!\n"));

    for (i=0; i<MAX_PEERS; i++) {
        for (j=0; j<2; j++) {
            dummy_data[i][j] = buf[i][j];
        }
    }

    _SHR_mutex_unlock(&hMutex);
}

// Set length of delay queue in frames for TS mode.
//   ignored in RT mode
//
void LCE_SetDelay(int delay)
{
    _SHR_mutex_lock(&hMutex);

    TRACE((TRACE_FINE, LCE, "LCE_SetDelay(%d)\n", delay));

    if (delay % 2) {
        delay++; // must be even
    }
    if (delay < 4) {
        delay = 4; // min
    } else if (delay > MAX_DELAY) {
        delay = MAX_DELAY; // max
    }
    lce->delay = delay;

    _SHR_mutex_unlock(&hMutex);
}

// Specify number of 16-bit transfers per frame.  This must
// be exact.
//   size: # of transfers between VBLANKS.
void LCE_SetFrameLen(int size)
{
    _SHR_mutex_lock(&hMutex);

    TRACE((TRACE_FINE, LCE, "LCE_SetFrameLen(%d)\n", size));

    frameLen = size;

    _SHR_mutex_unlock(&hMutex);
}

// Enable use of LCE
//   Enable when player selects link play from game menu.
//
void LCE_Enable()
{
    enable_pkt msg;

    _SHR_mutex_lock(&hMutex);

    TRACE((TRACE_FINE, LCE, "LCE_Enable!\n"));

    if (lce->enable) goto exit;

    // Send SYNC control packet to all peers
    msg.frameLen = frameLen;
    SendMsg (VN_MEMBER_OTHERS, ENABLE, 1, 1, &msg, sizeof(msg));

    send_buf = &(((bulk_pkt*)__send_buf)->frame[0]);
    send_buf->hdr.frame_size = 0;
    send_buf->hdr.mode = lce->mode;
    send_frame = 0;
    send_counter = 0;
    reliable_buf = &(((bulk_pkt*)__reliable_buf)->frame[0]);
    ((bulk_pkt*)__reliable_buf)->hdr.num_frames = 0;
    reliable_count = 0;

    enabled[my_link_id] = true;

exit:
    _SHR_mutex_unlock(&hMutex);
}

// Disable use of LCE
//   Disable when player exits multiplayer mode.
//   This is needed to stop frame syncing when link is no longer needed.
void LCE_Disable()
{
    _SHR_mutex_lock(&hMutex);

    TRACE((TRACE_FINE, LCE, "LCE_Disable!\n"));

    if (lce->enable) {
        // Send DISABLE control pkt to all peers
        SendMsg (VN_MEMBER_OTHERS, DISABLE, 1, 1, NULL, 0);
        
        disableAll();
    }

    _SHR_mutex_unlock(&hMutex);
}

static void switchMode(int mode)
{
    int i;
    int j;
    int k;
    linkdata tmp;

    TRACE((TRACE_FINER, LCE, "Switch Mode to: %d\n", mode));
    PRINTF1(("Switch Mode to: %d switchToModeFrame:%d\n", mode,switchToModeFrame ));

    //If LCE_TS_SYNC_MODE || LCE_TS_AYNC_MODE
    //	Fill all queues with D copies of dummy data
    //If LCE_TS_STREAM_MODE
    //	Only fill peer queues with dummy data (leave own empty)
    switch (mode) {
        case LCE_NORMAL_MODE:
            break;
        case LCE_TS_SYNC_MODE:
        case LCE_TS_ASYNC_MODE:
            for (i=0; i<MAX_PEERS; i++) {
                for (j=0; j<lce->delay; j++) {
                   for (k=0; k<frameLen; k++) {
                        tmp.frame = switchToModeFrame+j;
                        tmp.data = dummy_data[i][tmp.frame%2][k];
                        pushJQ(i, mode, tmp.frame, k, tmp.data);
                    }
                }
            }
            break;
        case LCE_TS_STREAM_MODE:
            for (i=0; i<MAX_PEERS; i++) {
                if (i != my_link_id) {
                    for (j=0; j<lce->delay; j++) {
                        for (k=0; k<frameLen; k++) {
                            tmp.frame = switchToModeFrame+j;
                            tmp.data = dummy_data[i][tmp.frame%2][k];
                            pushJQ(i, mode, tmp.frame, k, tmp.data);
                        }
                    }
                }
            }
            break;
    }

    if (mode == LCE_NORMAL_MODE) {
        bulk_pkt* reliable_bulk = (bulk_pkt*)__reliable_buf;
        if (send_frame) {
            PRINTF1(("switching to NORMAL, empty bulk buffers\n"));
            sendBulkPkt();
        }
        if (reliable_count) {
            int size;

            size = (int)reliable_buf-(int)__reliable_buf;
            reliable_bulk->hdr.size = size;
            reliable_bulk->hdr.num_frames = reliable_count;
            PRINTF1(("switching to NORMAL, empty reliable buffers\n"));
            PRINTF1(("num_frames:%d size:%d\n", reliable_bulk->hdr.num_frames, size));
#if 1
#ifndef LOOPBACK
            SendMsg (VN_MEMBER_OTHERS, DATA, 1, 0, __reliable_buf, size);
#else
            SendMsg (VN_MEMBER_SELF, DATA, 1, 0, __reliable_buf, size);
#endif
#endif
            reliable_buf = &(((bulk_pkt*)__reliable_buf)->frame[0]);
            reliable_count = 0;
        }
    }

    lce->mode = mode;
}

// Switch LCE emulation mode
//   can only be called by master.
//
//   mode:  LCE_NORMAL_MODE or LCE_TS_MODE
//
void LCE_SwitchMode(int mode)
{
    _SHR_mutex_lock(&hMutex);

    TRACE((TRACE_FINE, LCE, "LCE_SwitchMode(%d)\n", mode));

    if (lce->linkid) goto exit;

    if (lce->mode != mode &&
        switchToModeFrame <= lce->frame_counter_limit) {
        switchToMode = mode;
        switchModeSent = 0;
    }

exit:
    _SHR_mutex_unlock(&hMutex);
}

static void* getNextSendBuf(data_pkt* p)
{
    int ret;
    
    ret = (int)&p->data + p->hdr.frame_size*2;
    if ((int)ret & 2) {
        // align to 32 bit boundry
        ret += 2;
    }
    
    return (void*)ret;
}

static void sendBulkPkt()
{
    bulk_pkt* bulk = (bulk_pkt*)__send_buf;
    bulk_pkt* reliable_bulk = (bulk_pkt*)__reliable_buf;
    IOSMessage msg;
    IOSError rv;
    int size;
    int reliable;

    if (lce->mode == LCE_NORMAL_MODE) {
        bulk->hdr.size = sizeof(bulk_pkt_hdr) + sizeof(data_pkt);
        bulk->hdr.num_frames = 1;
        reliable = 1;
    } else {
        bulk->hdr.size = (void*)send_buf-(void*)bulk;
        bulk->hdr.num_frames = send_frame;
        size = bulk->hdr.size - sizeof(bulk_pkt_hdr);
        memcpy(reliable_buf, &bulk->frame[0], size);
        reliable_buf = (data_pkt*)((int)reliable_buf+size);
        reliable_count = reliable_count + send_frame;
        reliable = 0;
    }
    if (bulk->hdr.size & 2) {
        // align to 32 bit boundry
        bulk->hdr.size += 2;
    }

    rv = IOS_ReceiveMessage (readyMq, &msg, IOS_MESSAGE_NOBLOCK);
    if (rv == IOS_ERROR_OK) {
        memcpy((char*)bulk + bulk->hdr.size, &tmpXmitBuf.data, tmpXmitSize);
        size = bulk->hdr.size + tmpXmitSize;
        IOS_SendMessage(sendMq, 0, IOS_MESSAGE_NOBLOCK);
    } else {
        size = bulk->hdr.size;
    }

    TRACE((TRACE_FINER, LCE, "send buffer - frames:%d frame_size:%d target frame:%d\n", bulk->hdr.num_frames, bulk->frame[0].hdr.frame_size, bulk->frame[0].hdr.target_frame));

#if 1
#ifndef LOOPBACK
    SendMsg (VN_MEMBER_OTHERS, DATA, reliable, reliable, bulk, size);
#else
    SendMsg (VN_MEMBER_SELF, DATA, reliable, reliable, bulk, size);
#endif
#endif

    // every <delay> frames send reliable data
    if (reliable_count >= 6 ||
        reliable_count >= lce->delay) {
        size = (int)reliable_buf-(int)__reliable_buf;
        reliable_bulk->hdr.size = size;
        reliable_bulk->hdr.num_frames = reliable_count;
        PRINTF(("Send reliable - num_frames:%d size:%d\n", reliable_bulk->hdr.num_frames, size));
#if 1
#ifndef LOOPBACK
        SendMsg (VN_MEMBER_OTHERS, DATA, 1, 0, __reliable_buf, size);
#else
        SendMsg (VN_MEMBER_SELF, DATA, 1, 0,__reliable_buf, size);
#endif
#endif
        reliable_buf = &(((bulk_pkt*)__reliable_buf)->frame[0]);
        reliable_count = 0;
    }    

    // Reset global variables
    bulk = (bulk_pkt*)__send_buf;
    send_buf = &bulk->frame[0];
    send_frame = 0;
}

static void addToSendBuffer()
{
    TRACE((TRACE_FINER, LCE, "addToSendBuffer() - siomulti_send: %04x\n", lce->siomulti_send));

    if (lce->mode == LCE_NORMAL_MODE) {
        send_buf = &(((bulk_pkt*)__send_buf)->frame[0]);
        send_buf->hdr.frame_size = send_counter+1;
        send_buf->hdr.mode = lce->mode;
        send_buf->hdr.target_frame = lce->frame_counter;
        send_buf->data[send_counter] = lce->siomulti_send;
        sendBulkPkt();
    } else {
        send_buf->data[send_counter] = lce->siomulti_send;

        if (send_counter+1 == frameLen) 
        {
            send_buf->hdr.frame_size = frameLen;
            send_buf->hdr.mode = lce->mode;
            send_buf->hdr.target_frame = lce->frame_counter+lce->delay;
            ++send_frame;
            send_buf = getNextSendBuf(send_buf);
            if (send_frame >= BULKFRAMES) {
                sendBulkPkt();
            }
        }
    }
}

static void getPeerData()
{
    //   update shadow regs with front of queues
    //   pop all queues
    // XXX selection depending on mode
    lce->siomulti0 = popJQ(0);
    lce->siomulti1 = popJQ(1);
    if (num_gbas >= 3) lce->siomulti2 = popJQ(2);
    if (num_gbas == 4) lce->siomulti3 = popJQ(3);

    default_dummy_data[0][lce->frame_counter_limit%2][send_counter] = lce->siomulti0;
    default_dummy_data[1][lce->frame_counter_limit%2][send_counter] = lce->siomulti1;
    if (num_gbas >= 3) 
        default_dummy_data[2][lce->frame_counter_limit%2][send_counter] = lce->siomulti2;
    if (num_gbas == 4) 
        default_dummy_data[3][lce->frame_counter_limit%2][send_counter] = lce->siomulti3;

    TRACE((TRACE_FINE, LCE, "siomulti0: %04x siomulti1: %04x siomulti2: %04x siomulti3: %04x\n",
        lce->siomulti0, lce->siomulti1, lce->siomulti2, lce->siomulti3));
}

// Trigger data exchange among players
//  - invoked by master when SIOCNT SIO_START is enabled
//
void LCE_Trigger()
{
    linkdata tmp;

    _SHR_mutex_lock(&hMutex);

    TRACE((TRACE_FINE, LCE, "LCE_Trigger!\n"));
    TRACE((TRACE_FINER, LCE, "lce->emulated_intr: %d\n", lce->emulated_intr));

    if (!lce->enable) goto exit;

    TRACE((TRACE_FINER, LCE, "send_counter: %d\n", send_counter));

    if (!my_link_id && switchToMode == LCE_NORMAL_MODE &&
        switchToModeFrame > lce->frame_counter_limit) {
        // Don't send data when switching from TS to NORMAL
        getPeerData();
    } else {
        //push siosend to own queue
        PRINTF(("push siosend to own queue\n"));
        tmp.frame = (lce->mode == LCE_NORMAL_MODE) ? 
            lce->frame_counter_limit : lce->frame_counter_limit + lce->delay;
        tmp.data = lce->siomulti_send;
        pushJQ(my_link_id, lce->mode, tmp.frame, send_counter, tmp.data);

        addToSendBuffer();

        getPeerData();
    }

    if (my_link_id) { // SLAVE
        --lce->emulated_intr;
    } else {
        if (lce->siocnt & 0x4000) { // Interrupt enable
            lce->emulated_intr = 1;
        }
        prevVblank = 0;
    }

    ++send_counter;
    if (!my_link_id)  // MASTER
    {
        if (send_counter >= frameLen) {
            lce->frame_counter_limit = lce->frame_counter+1;
            TRACE((TRACE_FINER, LCE, "new frame_counter_limit: %d\n", lce->frame_counter_limit));
        }
    } else { // SLAVE
        if (lce->emulated_intr == 0) {
            data_pkt* dpkt = getJQ(0,lce->frame_counter+1);
            if ((dpkt && dpkt->hdr.mode == lce->mode) ||
                switchToModeFrame == lce->frame_counter+1) {
                lce->frame_counter_limit++;
                TRACE((TRACE_FINER, LCE, "new frame_counter_limit: %d\n", lce->frame_counter_limit));
                PRINTF1(("trigger - new frame_counter_limit: %d\n", lce->frame_counter_limit));
            }
        }
    }
    
    if (lce->emulated_intr) {
        if (!my_link_id) {
            IOS_SendMailbox(0);
        } else {
            setIntrTimer(220);
        }
    }

exit:
    _SHR_mutex_unlock(&hMutex);
}

static void scheduleSIOs()
{
    //If SLAVE &&
    //pkts in this frame > 0,
    //    set SIO interrupt
    //    emulated_intr = # pkts in this frame
    if (my_link_id) {
        data_pkt* dpkt = getJQ(0, lce->frame_counter);
        int nPkts = 0;

        if (dpkt && dpkt->hdr.mode == lce->mode) {
            nPkts = dpkt->hdr.frame_size-send_counter;
            lce->emulated_intr = nPkts;
            if (nPkts) {
                setIntrTimer(220);
            } else {
                // if data is already in next frame, just advance
                dpkt = getJQ(0, lce->frame_counter_limit+1);
                if ((dpkt && dpkt->hdr.mode == lce->mode) ||
                    switchToModeFrame == lce->frame_counter_limit+1) {
                    ++lce->frame_counter_limit;
                    TRACE((TRACE_FINER, LCE, "new frame_counter_limit: %d\n", lce->frame_counter_limit));
                    PRINTF1(("sched - new frame_counter_limit: %d\n", lce->frame_counter_limit));
                }
            }
        }

        TRACE((TRACE_FINER, LCE, "nPkts: %d", nPkts));
    }
}

// Send out voice data, if any
void checkVoice()
{
    IOSError    rv;
    IOSMessage  msg;

    rv = IOS_ReceiveMessage (readyMq, &msg, IOS_MESSAGE_NOBLOCK);
    if (rv == IOS_ERROR_OK) {
        tmpXmitBuf.hdr.size = 8;
        tmpXmitBuf.hdr.num_frames = 0;
#ifndef LOOPBACK
        SendMsg (VN_MEMBER_OTHERS, DATA, 0, 0, (void*)&tmpXmitBuf, 8+tmpXmitSize);
#else
        SendMsg (VN_MEMBER_SELF, DATA, 0, 0, (void*)&tmpXmitBuf, 8+tmpXmitSize);
#endif
        IOS_SendMessage(sendMq, 0, IOS_MESSAGE_NOBLOCK);
    }    
}

// Ask LCE to schedule a sequence of SIO interrupts to deliver
// the data for the current frame
//   - usually called at VBLANK intr
//
void LCE_SchedSIOIntr()
{
    _SHR_mutex_lock(&hMutex);

    if (!lce->enable) checkVoice();

    if (lce->enable) lce->frame_counter++;

    TRACE((TRACE_FINE, LCE, "**************** LCE_SchedSIOIntr! ****************\n"));
    TRACE((TRACE_FINER, LCE, "lce->frame_counter: %d lce->frame_counter_limit %d\n", lce->frame_counter, lce->frame_counter_limit));

    checkEnable();

    if (!lce->enable) goto exit;

    if (send_counter == 0 && lce->frame_counter != 0) {
        // need to send empty frame data
        if (lce->mode == LCE_NORMAL_MODE) {
            send_buf = &(((bulk_pkt*)__send_buf)->frame[0]);
            send_buf->hdr.frame_size = 0;
            send_buf->hdr.mode = lce->mode;
            send_buf->hdr.target_frame = lce->frame_counter-1;
            sendBulkPkt();
            pushJQ(my_link_id, lce->mode, lce->frame_counter-1, 0, 0);
        } else {
            send_buf->hdr.frame_size = 0;
            send_buf->hdr.mode = lce->mode;
            send_buf->hdr.target_frame = lce->frame_counter+lce->delay-1;
            ++send_frame;
            send_buf = getNextSendBuf(send_buf);
            if (send_frame >= BULKFRAMES) {
                sendBulkPkt();
            }
            pushJQ(my_link_id, lce->mode, lce->frame_counter+lce->delay-1, 0, 0);
       }
    }

    send_counter = 0;

    checkMode();

    scheduleSIOs();

exit:
    _SHR_mutex_unlock(&hMutex);
}

// Notify LCE of VBLANK interrupt
void LCE_Vblank()
{
    _SHR_mutex_lock(&hMutex);

    TRACE((TRACE_FINE, LCE, "LCE_Vblank!\n"));

    if (!lce->enable) goto exit;

    checkVoice();

    if (!my_link_id) {
#if 0
        // FIX for Advance Wars 2:
        // If NORMAL mode MASTER, two VBLANKs mean there are no more data
        // to send in this frame.  Thus we move on to the next.
        if (send_counter && prevVblank > 0) {
            lce->frame_counter_limit = lce->frame_counter+1;
            TRACE((TRACE_FINER, LCE, "new frame_counter_limit: %d\n", lce->frame_counter_limit));
        }
#endif

        if (prevVblank > 10) {
            PRINTF1(("fis:%d tc:%d tcl:%d fc:%d fcl:%d\n",lce->frame_intr_skipped, lce->timer_counter,lce->timer_counter_limit, lce->frame_counter, lce->frame_counter_limit));
            ++lce->frame_counter_limit;
            TRACE((TRACE_FINER, LCE, "new frame_counter_limit: %d\n", lce->frame_counter_limit));
            prevVblank = 0;
        }
        ++prevVblank;
    }

exit:
    _SHR_mutex_unlock(&hMutex);
}

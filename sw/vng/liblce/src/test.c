#include <sc/sc.h>
#include <sc/ios.h>
#include <sc/libc.h>
#include "lce_rm_jt.h"
#include "lce_rm.h"

#define STACK_SIZE 1024
const u8 _initStack[STACK_SIZE];
const u32 _initStackSize = sizeof(_initStack);
const u32 _initPriority = 5;

#define APP_LOG(...) printf("APP: " __VA_ARGS__)
#define APP_LOG_CONT(...) printf(__VA_ARGS__)

void error(char *str)
{
    APP_LOG("Error: %s\n", str);
    exit(-1);
}

int main(void)
{
    APP_LOG("LCE RM test application\n");

    IOSFd fd;
    IOSError rv;
    LCE_ARG a;
    int cmd = LCE_RM_FUNC_ID_MIN;

    APP_LOG("Before IOS_Open(\"/dev/lce\")\n");
    fd = IOS_Open("/dev/lce", 0);
    APP_LOG("After IOS_Open(): %d\n\n", fd);
    if (fd != 0) error("open");

// LCE_FI_INIT                         1001
    APP_LOG("Before IOS_Ioctl(%d)\n", cmd);
    a.in = 0; a.wait = 1;
    rv = IOS_Ioctl(fd, cmd, &a, sizeof(a), NULL, 0);
    APP_LOG("After IOS_Ioctl(%d): %d\n\n", cmd, rv);
    if (rv != IOS_ERROR_OK) error("ioctl");
    ++cmd;

// LCE_FI_SETDUMMYDATA                 1002
    {
        u16* dummy_data[4][2];
        u16 default_dummy_data[4][2][20];
        int i, j;
        for (i=0; i<4; i++) {
            for (j=0; j<2; j++) {
                memset(default_dummy_data[i][j], 0, 20);
                dummy_data[i][j] = default_dummy_data[i][j];
            }
        }
        APP_LOG("Before IOS_Ioctl(%d)\n", cmd);
        a.in = (int)dummy_data; a.wait = 1;
        rv = IOS_Ioctl(fd, cmd, &a, sizeof(a), NULL, 0);
        APP_LOG("After IOS_Ioctl(%d): %d\n\n", cmd, rv);
        if (rv != IOS_ERROR_OK) error("ioctl");
        ++cmd;
    }

// LCE_FI_SETDELAY                     1003
    APP_LOG("Before IOS_Ioctl(%d)\n", cmd);
    a.in = 10; a.wait = 1;
    rv = IOS_Ioctl(fd, cmd, &a, sizeof(a), NULL, 0);
    APP_LOG("After IOS_Ioctl(%d): %d\n\n", cmd, rv);
    if (rv != IOS_ERROR_OK) error("ioctl");
    ++cmd;

// LCE_FI_SETFRAMELEN                  1004
    APP_LOG("Before IOS_Ioctl(%d)\n", cmd);
    a.in = 10; a.wait = 1;
    rv = IOS_Ioctl(fd, cmd, &a, sizeof(a), NULL, 0);
    APP_LOG("After IOS_Ioctl(%d): %d\n\n", cmd, rv);
    if (rv != IOS_ERROR_OK) error("ioctl");
    ++cmd;

// LCE_FI_ENABLE                       1005
    APP_LOG("Before IOS_Ioctl(%d)\n", cmd);
    a.in = 0; a.wait = 1;
    rv = IOS_Ioctl(fd, cmd, &a, sizeof(a), NULL, 0);
    APP_LOG("After IOS_Ioctl(%d): %d\n\n", cmd, rv);
    if (rv != IOS_ERROR_OK) error("ioctl");
    ++cmd;

// LCE_FI_DISABLE                      1006
    APP_LOG("Before IOS_Ioctl(%d)\n", cmd);
    a.in = 0; a.wait = 1;
    rv = IOS_Ioctl(fd, cmd, &a, sizeof(a), NULL, 0);
    APP_LOG("After IOS_Ioctl(%d): %d\n\n", cmd, rv);
    if (rv != IOS_ERROR_OK) error("ioctl");
    ++cmd;

// LCE_FI_SWITCHMODE                   1007
    APP_LOG("Before IOS_Ioctl(%d)\n", cmd);
    a.in = 1; a.wait = 1;
    rv = IOS_Ioctl(fd, cmd, &a, sizeof(a), NULL, 0);
    APP_LOG("After IOS_Ioctl(%d): %d\n\n", cmd, rv);
    if (rv != IOS_ERROR_OK) error("ioctl");
    ++cmd;

// LCE_FI_TRIGGER                      1008
    APP_LOG("Before IOS_Ioctl(%d)\n", cmd);
    a.in = 0; a.wait = 0;
    rv = IOS_Ioctl(fd, cmd, &a, sizeof(a), NULL, 0);
    APP_LOG("After IOS_Ioctl(%d): %d\n\n", cmd, rv);
    if (rv != IOS_ERROR_OK) error("ioctl");

// LCE_FI_TRIGGER                      1008
    APP_LOG("Before IOS_Ioctl(%d)\n", cmd);
    a.in = 0; a.wait = 1;
    rv = IOS_Ioctl(fd, cmd, &a, sizeof(a), NULL, 0);
    APP_LOG("After IOS_Ioctl(%d): %d\n\n", cmd, rv);
    if (rv != IOS_ERROR_OK) error("ioctl");
    ++cmd;

// LCE_FI_SCHEDSIOINTR                 1009
    APP_LOG("Before IOS_Ioctl(%d)\n", cmd);
    a.in = 0; a.wait = 1;
    rv = IOS_Ioctl(fd, cmd, &a, sizeof(a), NULL, 0);
    APP_LOG("After IOS_Ioctl(%d): %d\n\n", cmd, rv);
    if (rv != IOS_ERROR_OK) error("ioctl");
    ++cmd;

// LCE_FI_TRIGGER                      1008
    APP_LOG("Before IOS_Ioctl(%d)\n", 1008);
    a.in = 0; a.wait = 0;
    rv = IOS_Ioctl(fd, 1008, &a, sizeof(a), NULL, 0);
    APP_LOG("After IOS_Ioctl(%d): %d\n\n", 1008, rv);
    if (rv != IOS_ERROR_OK) error("ioctl");

// LCE_FI_VBLANK                       1010
    APP_LOG("Before IOS_Ioctl(%d)\n", cmd);
    a.in = 0; a.wait = 1;
    rv = IOS_Ioctl(fd, cmd, &a, sizeof(a), NULL, 0);
    APP_LOG("After IOS_Ioctl(%d): %d\n\n", cmd, rv);
    if (rv != IOS_ERROR_OK) error("ioctl");
    ++cmd;

    APP_LOG("Before IOS_Close()\n");
    rv = IOS_Close(fd);
    APP_LOG("After IOS_Close(): %d\n\n", rv);
    if (rv != IOS_ERROR_OK) error("close");

   exit(0);
    return 0;
}

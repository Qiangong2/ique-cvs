#include "AgbMemoryMap.h"
#include "AgbDefine.h"
#include "AgbSystemCall.h"
#include "types.h"
#include "ioslibc.h"
#include "sc/ios.h"
#include "lce_rm_jt.h"
#include "lce_rm.h"
#include "lce.h"

//#define VBA_LCE

#define REG_BASE                0x04000000          // Registers
#define REG_IME         (REG_BASE + 0x208)  // Interrupt Master Enable

#ifdef VBA_LCE
#define IO_WRITE(addr, val)
#else
#define IO_WRITE(addr, val)     (*(volatile u32 *)(addr)) = (val)
#endif

#define IPC_SP_START 0x09fffff0
#define IPC_CTRL_REG    IPC_SP_START + 0x8
#define IPC_CTRL_MASK_WE        0x00008000
#define IPC_CTRL_MASK_MSG       0x00000100
#define IPC_CTRL_IN_MSG         0x00000004

typedef struct {
    IOSIoVector in;
    IOSIoVector lce;
} LceReadVector;

typedef struct {
    IOSIoVector lce;
} LceWriteVector;

typedef struct {
    LceReadVector       read;
    LceWriteVector      write;
} LCE_VECTOR;

static LCE_ARG inBuf  __attribute__((section(".data")));
static LCE_VECTOR vector __attribute__((section(".data")));
static lce_type gba_lce __attribute__((section(".data")));

static IOSFd fd = -1;

lce_type* getLCEDataArea()
{
    if (fd < 0) LCE_Init();

#ifdef VBA_LCE
    gba_lce.linkid = (*(vu16 *)REG_SIOCNT & 0030)>>4;
#endif

    return &gba_lce;
}

//#define PROFILE_IPC
#ifdef PROFILE_IPC
int dvc[20];
int tvc[20];
int ndvc = 0;
int cdvc = 0;
#endif

void callIoctl(u32 cmd, u32 input, int wait)
{
    LCE_ARG *a = &inBuf;
    IOSIoVector *v = (IOSIoVector*)&vector;
    u16 ime_bak;
    u16 ie_bak;
    IOSError err;
    int i;

#ifdef VBA_LCE
    return;
#endif
    ime_bak = *(vu16 *)REG_IME;
    *(vu16 *)REG_IME = 0;
    ie_bak = *(vu16 *)REG_IE;
    *(vu16 *)REG_IE &= ~(V_BLANK_INTR_FLAG | CASSETTE_INTR_FLAG | TIMER3_INTR_FLAG | SIO_INTR_FLAG);
    *(vu16 *)REG_IME = ime_bak;

    if (fd < 0) {
        int readLen, writeLen;

        while ((fd = IOS_Open(LCE_DEV, 0)) < 0) {
            for (i=0; i<10000; i++);
        }

        // Need to do this only once
        readLen = (int)&(gba_lce.emulated_intr) - (int)&gba_lce;
        writeLen = sizeof(lce_type)-readLen;
        vector.read.in.base = (u8*)a;
        vector.read.in.length = sizeof(LCE_ARG);
        vector.read.lce.base = (u8*)&gba_lce;
        vector.read.lce.length = readLen;
        vector.write.lce.base = (u8*)&(gba_lce.emulated_intr);
        vector.write.lce.length = writeLen;
    }

    a->in = input;
    err = IOS_Ioctlv(fd, cmd, 2, 1, v);

#ifdef PROFILE_IPC
    {
        int vc = *(vu16*)REG_VCOUNT;

        if (cmd == LCE_FI_SCHEDSIOINTR) {
            int i;
            ndvc = 0;
            cdvc++;
            if (cdvc > 60) {
                cdvc = 0;
                for (i=0; i<20; i++) {
                    printf("i:%02d tvc:%d dvc:%x\n", i, tvc[i], dvc[i]);
                }
            }
            memset(tvc, 0, sizeof(tvc));
            memset(dvc, 0, sizeof(dvc));
        }
        if (ndvc < 20) {
            dvc[ndvc] = vc;
            tvc[ndvc] = cmd;
            ndvc++;
        }
    }
#endif

    ime_bak = *(vu16 *)REG_IME;
    *(vu16 *)REG_IME = 0;
    *(vu16 *)REG_IE = ie_bak;
    *(vu16 *)REG_IME = ime_bak;
}


void LCE_Init()
{
#ifdef VBA_LCE
    fd = 0x69;
    memset(&gba_lce, 0, sizeof(gba_lce));
    gba_lce.frame_counter_limit = 1;
#endif

    // Clear Cassete intr
    IO_WRITE(IPC_CTRL_REG, IPC_CTRL_IN_MSG);

    // Enable Cassette intr
    IO_WRITE(IPC_CTRL_REG, IPC_CTRL_MASK_WE|IPC_CTRL_MASK_MSG);

    callIoctl(LCE_FI_INIT, 0, 1);
}

void LCE_SetDummyData(u16* buf[4][2])
{
    if (fd < 0) LCE_Init();

    callIoctl(LCE_FI_SETDUMMYDATA, (u32)buf, 1);
}

void LCE_SetDelay(int delay)
{
    if (fd < 0) LCE_Init();

    callIoctl(LCE_FI_SETDELAY, (u32)delay, 1);
}

void LCE_SetFrameLen(int size)
{
    if (fd < 0) LCE_Init();

    callIoctl(LCE_FI_SETFRAMELEN, (u32)size, 1);
}

void LCE_Enable()
{
    if (fd < 0) LCE_Init();

    callIoctl(LCE_FI_ENABLE, 0, 1);
}

void LCE_Disable()
{
    if (fd < 0) LCE_Init();

    callIoctl(LCE_FI_DISABLE, 0, 1);
}

void LCE_SwitchMode(int mode)
{
    if (fd < 0) LCE_Init();

    callIoctl(LCE_FI_SWITCHMODE, (u32)mode, 1);
}

void LCE_Trigger()
{
    if (fd < 0) LCE_Init();

    gba_lce.siomulti_send =  *(u16 *)REG_SIOMLT_SEND;

    // clear Cassete intr
    IO_WRITE(IPC_CTRL_REG, IPC_CTRL_IN_MSG);

    callIoctl(LCE_FI_TRIGGER, 0, 1);
    gba_lce.siocnt = (gba_lce.siocnt & 0xff0f) | (gba_lce.linkid << 4);
}

void LCE_SchedSIOIntr()
{
    if (fd < 0) LCE_Init();

    callIoctl(LCE_FI_SCHEDSIOINTR, 0, 1);
}

void LCE_Vblank()
{
    if (fd < 0) LCE_Init();

    callIoctl(LCE_FI_VBLANK, 0, 1);
}

//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnhash.h"
#include "vn.h"
#include "vndebug.h"

#define GOLDEN_RATIO    0x9E3779B9U  /* 2/(1+sqrt(5))*(2^32) */

/*
 * Compute the log of the least power of 2 greater than or equal to n
 */
uint32_t _vn_ht_ceilinglog2(uint32_t n)
{
    uint32_t log2 = 0;
    if (n & (n-1))
        log2++;
    if (n >> 16)
        log2 += 16, n >>= 16;
    if (n >> 8)
        log2 += 8, n >>= 8;
    if (n >> 4)
        log2 += 4, n >>= 4;
    if (n >> 2)
        log2 += 2, n >>= 2;
    if (n >> 1)
        log2++;
    return log2;
}

_vn_ht_bucket_t* _vn_ht_create_bucket(_vn_ht_hash_t hash, _vn_ht_key_t key,
                                      void* value, _vn_ht_bucket_t* next)
{
    _vn_ht_bucket_t* bucket;
    bucket = _vn_malloc(sizeof(_vn_ht_bucket_t));
    if (bucket != NULL) {
        bucket->hash = hash;
        bucket->key = key;
        bucket->value = value;
        bucket->next = next;
    }
    return bucket;
}
 
void _vn_ht_free_bucket(_vn_ht_bucket_t* bucket)
{
    if (bucket) {
        /* NOTE: Does not free key or value */
        _vn_free(bucket);
    }
}

int _vn_ht_init(_vn_ht_table_t* ht,  uint32_t nBuckets, uint32_t nMax,
                bool resize, _vn_ht_func_t hash_func, _vn_ht_comp_t key_comp)
{
    uint32_t nBits, nBucketBytes;

    assert(ht);
    if (nBuckets <= _VN_HT_MINBUCKETS) {
        nBuckets = _VN_HT_MINBUCKETS;
        nBits = _VN_HT_MINBUCKETSLOG2;
    }
    else {
        nBits = _vn_ht_ceilinglog2(nBuckets);
        nBuckets = 1 << nBits;
    }

    /* Allocate memory for buckets */
    nBucketBytes = nBuckets*sizeof(_vn_ht_bucket_t*);
    ht->buckets = _vn_malloc(nBucketBytes);

    if (ht->buckets == NULL) {
        return _VN_ERR_NOMEM;
    }
    
    memset(ht->buckets, 0, nBucketBytes);

    /* Initialize hashtable */
    ht->resize = resize;
    ht->shift = 32 - nBits;
    ht->nBuckets = nBuckets;
    ht->nEntries = 0;
    ht->nMaxEntries = nMax;
    ht->hash_func = hash_func;
    ht->key_comp = key_comp;
    return _VN_ERR_OK;
}

_vn_ht_table_t* _vn_ht_create(uint32_t nBuckets, uint32_t nMax, bool resize,
                              _vn_ht_func_t hash_func, _vn_ht_comp_t key_comp)
{
    _vn_ht_table_t* ht;
    
    /* Allocate memory for hashtable */
    ht = _vn_malloc(sizeof(_vn_ht_table_t));
    if (ht == NULL) {
        return NULL;
    }

    if (_vn_ht_init(ht, nBuckets, nMax, resize, hash_func, key_comp) < 0) {
        _vn_free(ht);
        return NULL;
    }

    return ht;
}

void _vn_ht_clear(_vn_ht_table_t* ht, _vn_free_func_t free_value,
                  bool free_buckets)
{
    _vn_ht_bucket_t *bucket, *next;
    uint32_t i;

    if (ht) {
        if (ht->buckets) {
            for (i = 0; i < ht->nBuckets; i++) {
                bucket = ht->buckets[i];
                while (bucket) {
                    if (bucket->value && free_value) {
                        (*free_value)(bucket->value);
                    }
                    next = bucket->next;
                    _vn_free(bucket);
                    bucket = next;
                }
                ht->buckets[i] = 0;
            }
            if (free_buckets) {
                _vn_free(ht->buckets);
                ht->nBuckets = 0;
            }            
        }
        ht->nEntries = 0;
    }
}

void _vn_ht_destroy(_vn_ht_table_t* ht, _vn_free_func_t free_value)
{
    if (ht != NULL) {
        _vn_ht_clear(ht, free_value, true);
        _vn_free(ht);
    }    
}

int _vn_ht_resize(_vn_ht_table_t* ht, uint32_t new_size, uint32_t new_shift)
{
    uint32_t i, n;
    _vn_ht_bucket_t *bucket, *next, **oldBuckets, **pBucket;
    size_t nBytes;

    assert(ht);
    assert(ht->resize);
    _VN_TRACE(TRACE_FINER, _VN_SG_MISC,
              "_vn_ht_resize: bucket %u, entries %u, new size %u\n",
              ht->nBuckets, ht->nEntries, new_size);

    n = ht->nBuckets;
    oldBuckets = ht->buckets;
    nBytes = new_size * sizeof(_vn_ht_bucket_t *);
    ht->buckets = (_vn_ht_bucket_t**) _vn_malloc(nBytes);
    if (!ht->buckets) {
        ht->buckets = oldBuckets;
        return _VN_ERR_NOMEM;
    }
    memset(ht->buckets, 0, nBytes);
    ht->nBuckets = new_size;
    ht->shift = new_shift;

    for (i = 0; i < n; i++) {
        bucket = oldBuckets[i];
        while (bucket) {
            next = bucket->next;
            pBucket = _vn_ht_lookup_bucket(ht, bucket->hash, bucket->key);
            assert(*pBucket == NULL);
            bucket->next = NULL;
            *pBucket = bucket;
            bucket = next;
        }
    }

    _vn_free(oldBuckets);
    return _VN_ERR_OK;
}
 
/* Looks up bucket, returns pointer in table to the bucket of interest */
_vn_ht_bucket_t**
_vn_ht_lookup_bucket(_vn_ht_table_t* ht, _vn_ht_hash_t hash, _vn_ht_key_t key)
{
    _vn_ht_bucket_t *bucket, **pBucket;
    _vn_ht_hash_t h;

    assert(ht);
    assert(ht->buckets);
    assert(ht->key_comp);

    h = hash * GOLDEN_RATIO;
    h >>= ht->shift;
    pBucket = &ht->buckets[h];
    while ((bucket = *pBucket) != NULL) {
        if ((bucket->hash == hash) && (*ht->key_comp)(key, bucket->key)) {
            break;
        }
        pBucket = &(bucket->next);
    }
    return pBucket;
}

_vn_ht_bucket_t*
_vn_ht_add_bucket(_vn_ht_table_t* ht, _vn_ht_bucket_t** pBucket,
                  _vn_ht_hash_t hash, _vn_ht_key_t key, void* value)
{
    _vn_ht_bucket_t *bucket;

    assert(ht);

    /* Grow the table if it is overloaded */
    if (ht->resize && ht->nEntries >= _VN_HT_OVERLOADED(ht->nBuckets)) {
        int rv = _vn_ht_resize(ht, 2*ht->nBuckets, ht->shift-1);
        if (rv < 0) {
            return NULL;
        }

        pBucket = _vn_ht_lookup_bucket(ht, hash, key);
    }

    /* Make a new key value entry */
    assert(pBucket);
    bucket = _vn_ht_create_bucket(hash, key, value, *pBucket);
    if (bucket != NULL) {
        *pBucket = bucket;
        ht->nEntries++;
    }
    return bucket;
}

void
_vn_ht_remove_bucket(_vn_ht_table_t *ht, _vn_ht_bucket_t **pBucket, 
                     _vn_ht_bucket_t* bucket)
{
    assert(ht);
    assert(pBucket);

    *pBucket = bucket->next;
    _vn_ht_free_bucket(bucket);
    ht->nEntries--;

    /* Shrink table if it's underloaded */
    if (ht->resize && ht->nEntries < _VN_HT_UNDERLOADED(ht->nBuckets)) {
        _vn_ht_resize(ht, ht->nBuckets/2, ht->shift+1);
    }
}

int
_vn_ht_add(_vn_ht_table_t* ht, _vn_ht_key_t key, void *value)
{
    _vn_ht_hash_t hash;
    _vn_ht_bucket_t* bucket, **pBucket;

    assert(ht);
    assert(ht->hash_func);

    hash = (*ht->hash_func)(key);
    pBucket = _vn_ht_lookup_bucket(ht, hash, key);

    assert(pBucket);    
    if ((bucket = *pBucket) != NULL) {
        /* Entry already exists!!!! */
        return _VN_ERR_DUPENTRY;
    }
    else {
        if (ht->nMaxEntries && ht->nEntries >= ht->nMaxEntries) {
            return _VN_ERR_FULL;
        }

        bucket = _vn_ht_add_bucket(ht, pBucket, hash, key, value);
        if (bucket == NULL) {
            return _VN_ERR_NOMEM;
        }
        else {
            return _VN_ERR_OK;
        }
    }
}

void*
_vn_ht_remove(_vn_ht_table_t* ht, _vn_ht_key_t key)
{
    _vn_ht_hash_t hash;
    _vn_ht_bucket_t **pBucket;
    void* value;

    assert(ht);
    assert(ht->hash_func);
    hash = (*ht->hash_func)(key);
    pBucket = _vn_ht_lookup_bucket(ht, hash, key);
    if ((pBucket == NULL) || (*pBucket == NULL)) {
        return NULL;
    }

    /* Hit; remove element */
    value = (*pBucket)->value;
    _vn_ht_remove_bucket(ht, pBucket, *pBucket);
    return value;
}

void* 
_vn_ht_lookup(_vn_ht_table_t* ht, _vn_ht_key_t key)
{
    _vn_ht_hash_t hash;
    _vn_ht_bucket_t **pBucket;

    assert(ht);
    assert(ht->hash_func);
    hash = (*ht->hash_func)(key);
    pBucket = _vn_ht_lookup_bucket(ht, hash, key);
    if (pBucket && (*pBucket)) {
        return (*pBucket)->value;
    }
    return NULL;
}

uint32_t _vn_ht_get_size(_vn_ht_table_t* ht)
{
    assert(ht);
    return ht->nEntries;
}

_vn_ht_hash_t
_vn_ht_hash_int(_vn_ht_key_t key)
{
    return (_vn_ht_hash_t) key;
}

int
_vn_ht_key_comp_int(_vn_ht_key_t key1, _vn_ht_key_t key2)
{
    return (key1 == key2);
}

void _vn_ht_print(_vn_ht_table_t* ht, FILE* fp, const char* name,
                  void (*print_value)(FILE* fp, void*))
{
    _vn_ht_bucket_t* bucket;
    uint32_t i = 0, bi = 0, ei = 0;
    assert(ht != NULL);
    assert(fp != NULL);
    fprintf(fp, "%s hashtable, buckets %u, entries %u, resize %u\n", 
            name, ht->nBuckets, ht->nEntries, ht->resize);
    for (i = 0; i < ht->nBuckets; i++) {
        bucket = ht->buckets[i];
        bi = 0;
        while (bucket != NULL) {
            fprintf(fp, "  %s bucket %d.%d, entry %d: ", name, i, bi, ei);
            (*print_value)(fp, bucket->value);
            bi++; ei++;
            bucket = bucket->next;
        }
    }
}

/* Initializes a iterator for the specified hashtable */
void _vn_ht_iterator_init(_vn_ht_iter_t* iter, _vn_ht_table_t* ht)
{
    assert(ht != NULL);
    assert(iter != NULL);
    iter->ht = ht;
    if (ht->nBuckets > 0) {
        /* Find the first non-null bucket */
        for (iter->i = 0; iter->i < iter->ht->nBuckets; iter->i++) {
            iter->next = ht->buckets[iter->i];
            if (iter->next) {
                break;
            }
        }
    }
    else {
        iter->i = 0;   
        iter->next = NULL;
    }
}

/* Returns the next entry in the interator 
   (hashtable must not be modified while iterating) */
void* _vn_ht_iterator_next(_vn_ht_iter_t* iter)
{
    _vn_ht_bucket_t* bucket;
    assert(iter);
    assert(iter->ht);
    bucket = iter->next;    
    if (iter->next != NULL) {
        iter->next = iter->next->next;        
        /* Go to the next bucket if this is the end of the chain */
        while ((iter->next == NULL) && (iter->i < iter->ht->nBuckets)) {
            iter->i++;
            if (iter->i < iter->ht->nBuckets) {
                iter->next = iter->ht->buckets[iter->i];
            }
        }
    }
    return (bucket != NULL)? bucket->value: NULL;
}

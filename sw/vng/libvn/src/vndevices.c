//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnlocal.h"

#define _VN_DEVICE_TIMEOUT_MSECS 20*1000  

/* TODO: Limit size of device table for consoles */

/* Hashtable of all devices that I am interacting with */
/* To be populated during host discovery and as handshake sessions
   are established */
_vn_ht_table_t _vn_devices;

_vn_ht_table_t* _vn_get_device_table()
{
    return &_vn_devices;
}

/* Socket creation/destruction routines */

_vn_sockaddr_t* _vn_sockaddr_create(uint8_t family, uint8_t flags,
                                    _vn_inport_t port, _vn_inaddr_t addr)
{
    _vn_sockaddr_t* sockaddr = _vn_malloc(sizeof(_vn_sockaddr_t));
    if (sockaddr) {
        sockaddr->family = family;
        sockaddr->flags = flags;
        sockaddr->port = port;
        sockaddr->addr = addr;
    }
    return sockaddr;
}

void _vn_sockaddr_destroy(_vn_sockaddr_t* sockaddr)
{
    if (sockaddr) {
        _vn_free(sockaddr);
    }
}

/* Routines for looking up the connection information */

/* Looks up connection information from device table */
int _vn_device_lookup_connect_info(_VN_guid_t guid, _VN_net_t net_id, 
                                   _VN_addr_t* addr,
                                   _vn_inaddr_t* ip, _vn_inport_t* port)
{
    _vn_device_info_t* device = _vn_lookup_device(guid);
    _vn_sockaddr_t* sockaddr;

    if ((device == NULL) || (device->inaddrs == NULL)) {
        if (addr) (*addr = _VN_ADDR_INVALID);
        if (ip)   (*ip = _VN_INADDR_INVALID);
        if (port) (*port = _VN_INPORT_INVALID);
        return _VN_ERR_NOTFOUND;
    }
    
#ifndef _VN_RPC_PROXY
    if (addr) (*addr = device->vnaddr);
#else
    if (addr) (*addr = _VN_INADDR_INVALID);
#endif

    sockaddr = _vn_dlist_peek_front(device->inaddrs);
    if (sockaddr) {
        if (ip)   (*ip = sockaddr->addr);
        if (port) (*port = sockaddr->port);
    }
    else {
        if (ip)   (*ip = _VN_INADDR_INVALID);
        if (port) (*port = _VN_INPORT_INVALID);
    }

    return _VN_ERR_OK;
}

int _vn_device_lookup_connect_info_list(_VN_guid_t guid, _VN_net_t net_id, 
                                        _VN_addr_t* addr,
                                        _vn_dlist_t** list)
{
    _vn_device_info_t* device;

    device = _vn_lookup_device(guid);
    if (device == NULL) {
        if (addr) (*addr = _VN_ADDR_INVALID);
        if (list) (*list = NULL);
        return _VN_ERR_NOTFOUND;
    }

#ifndef _VN_RPC_PROXY    
    if (addr) (*addr = device->vnaddr);
#else
    if (addr) (*addr = _VN_ADDR_INVALID);
#endif
    if (list) (*list = device->inaddrs);
    return _VN_ERR_OK;
}

/* Adds connection info for this device */
int _vn_device_add_connect_info(_vn_device_info_t* device, uint8_t flags,
                                _vn_inaddr_t addr, _vn_inport_t port)
{
    _vn_dnode_t* node;
    _vn_sockaddr_t* sockaddr;
    _vn_inaddr_t loopback;
    assert(device);

    if ((addr == _VN_INADDR_INVALID) || (port == _VN_INPORT_INVALID)) {
        return _VN_ERR_INVALID;
    }

    loopback = htonl(INADDR_LOOPBACK);

    /* See if we already have an entry for this ip/port */
    if (device->inaddrs) {
        _vn_dlist_for(device->inaddrs, node)
        {
            sockaddr = (_vn_sockaddr_t*) _vn_dnode_value(node);
            if (sockaddr) {
                /* Don't add loopback address if we already have a 
                   entry with the same port.  
                   If we have a loop back entry with the same port,
                   replace it */
                if (sockaddr->port == port) {
                    if ((sockaddr->addr == addr) || 
                        (sockaddr->addr == loopback) ||
                        (addr == loopback)) {
                        if (sockaddr->addr == loopback) {
                            sockaddr->addr = addr;
                        }
                        sockaddr->flags = flags;
                        return _VN_ERR_OK;
                    }
                }
            }
        }
    }
    else {
        device->inaddrs = _vn_dlist_create();
        if (device->inaddrs == NULL) {
            return _VN_ERR_NOMEM;
        }
    }

    /* Not found, add entry */
    sockaddr = _vn_sockaddr_create(AF_INET, flags, port, addr);
    if (sockaddr) {
        int rv;
        rv = _vn_dlist_add(device->inaddrs, sockaddr);
        if (rv < 0) {
            _vn_sockaddr_destroy(sockaddr);
            return rv;
        }
        return _VN_ERR_OK;
    }
    else return _VN_ERR_NOMEM;
}

/* Service routines */

/* Initializes service */
void _vn_service_init(_vn_service_t* service, uint32_t service_id,
                      uint8_t flags, const void* msg, uint16_t msglen)
{
    if (service) {
        /* NOTE: We make a copy of the msg here */
        service->msg = _vn_malloc(msglen);
        if (service->msg) {
            memcpy((void*) service->msg, msg, msglen);
            service->msglen = msglen;
        }
        else {
            service->msglen = 0;
        }

        service->service_id = service_id;
        service->flags = flags;
    }
}

/* Allocates memory for a new service */
_vn_service_t* _vn_service_create(uint32_t service_id, uint8_t flags,
                                  const void* msg, uint16_t msglen)
{
    _vn_service_t* service = _vn_malloc(sizeof(_vn_service_t));
    if (service) {
        _vn_service_init(service, service_id, flags, msg, msglen);
        if (msg && !service->msg) {
            _vn_free(service);
            return NULL;
        }
    }
    return service;
}

/* Frees memory for service */
void _vn_service_destroy(_vn_service_t* service)
{
    if (service) {
        if (service->msg) {
            _vn_free((void*) service->msg);
        }
        _vn_free(service);
    }
}

/* Finds specified service */
_vn_service_t* _vn_device_get_service(_vn_device_info_t* device,
                                      uint32_t service_id)
{
    if (device && device->services) {
        _vn_dnode_t* node;
        _vn_dlist_for(device->services, node)
        {
            if (_vn_dnode_value(node)) {
                _vn_service_t* service = (_vn_service_t*) 
                    _vn_dnode_value(node);
                if (service->service_id == service_id) {
                    return service;
                }
            }
        }
    }
    return NULL;
}

/* Adds service for the specified device */
int _vn_device_add_service(_vn_device_info_t* device, 
                           uint32_t service_id, uint8_t flags,
                           const void* msg, uint16_t msglen)
{
    _vn_service_t* service;

    assert(device);
    
    if ((service = _vn_device_get_service(device, service_id)) == NULL) {
        /* Not found, add entry */
        if (device->services == NULL) {
            device->services = _vn_dlist_create();
            if (device->services == NULL) {
                return _VN_ERR_NOMEM;
            }
        }
        service = _vn_service_create(service_id, flags, msg, msglen);
        if (service) {
            _vn_dlist_add(device->services, service);
            return _VN_ERR_OK;
        }
        else return _VN_ERR_NOMEM;
    }
    else {
        /* Update service to new information */
        if ((msglen != service->msglen) ||
            (memcpy((void*) service->msg, msg, msglen) != 0)) {
            if (service->msg) {
                _vn_free((void*) service->msg);
            }
            _vn_service_init(service, service_id, flags, msg, msglen);
            if (msg && !service->msg) {
                return _VN_ERR_NOMEM;
            }
        }
        else if (service->flags != flags) {
            service->flags = flags;
        }
        return _VN_ERR_OK;
    }
}

/* Remove service from the specified device */
int _vn_device_delete_service(_vn_device_info_t* device,
                              uint32_t service_id)
{
    if (device && device->services) {
        _vn_dnode_t *node, *temp;        
        _vn_dlist_delete_for(device->services, node, temp)
        {
            if (_vn_dnode_value(node)) {
                _vn_service_t* service = (_vn_service_t*) 
                    _vn_dnode_value(node);
                if (service->service_id == service_id) {
                    _vn_dlist_delete(device->services, node);
                    _vn_service_destroy(service);
                    return _VN_ERR_OK;
                }
            }
        }
    }
    return _VN_ERR_NOTFOUND;
}

/* Clears the service list for the given device */
int _vn_device_clear_services(_vn_device_info_t* device)
{
    if (device && device->services) {
        _vn_dlist_destroy(device->services, (void*) &_vn_service_destroy);
        device->services = NULL;
    }
    return _VN_ERR_OK;
}

/* Returns service with service_id for device */
int _vn_device_get_service_count(_vn_device_info_t *device)
{
    assert(device);
    if (device->services) {
        return _vn_dlist_size(device->services);
    }
    else return 0;
}

/* Returns service with service_id for device */
int _vn_get_service_count(_VN_guid_t guid)
{
    _vn_device_info_t* device = _vn_lookup_device(guid);
    if (device) {
        return _vn_device_get_service_count(device);
    }
    else return _VN_ERR_NOTFOUND; 
}

/* Returns service ids */
int _vn_device_get_service_ids(_vn_device_info_t* device,
                               uint32_t* services, int nservices)
{
    int i = 0, rv;

    assert(device);
    if (device->services) {
        _vn_dnode_t* node;
        _vn_dlist_for(device->services, node)
        {
            if (_vn_dnode_value(node)) {
                _vn_service_t* service = (_vn_service_t*) 
                    _vn_dnode_value(node);
                
                services[i] = service->service_id;
                i++;
                if (i >= nservices)
                    break;
            }
        }
        rv = _vn_dlist_size(device->services);
    }
    else {
        rv = 0;
    }

    return rv;
}

/* Returns service ids */
int _vn_get_service_ids(_VN_guid_t guid, uint32_t* services, int nservices)
{
    _vn_device_info_t* device;
    int rv;

    assert(services);
    device = _vn_lookup_device(guid);
    if (device != NULL) {
        rv = _vn_device_get_service_ids(device, services, nservices);
    }
    else {
        rv = _VN_ERR_NOTFOUND;
    }

    return rv;
}

/* Looks up service */
_vn_service_t* _vn_lookup_service(_VN_guid_t guid, uint32_t service_id)
{
    _vn_device_info_t* device = _vn_lookup_device(guid);
    return _vn_device_get_service(device, service_id);
}

/* Adds service */
int _vn_add_service(_VN_guid_t guid, uint32_t service_id, uint8_t flags,
                    const void* msg, uint16_t msglen)
{
    _vn_device_info_t* device = _vn_lookup_device(guid);
    if (device) {
        return _vn_device_add_service(device, service_id, flags, msg, msglen);
    }
    else return _VN_ERR_NOTFOUND;
}

/* Removes service */
int _vn_remove_service(_VN_guid_t guid, uint32_t service_id)
{
    _vn_device_info_t* device = _vn_lookup_device(guid);
    if (device) {
        return _vn_device_delete_service(device, service_id);
    }
    else return _VN_ERR_NOTFOUND;
}

/* Removes a list of services */
int _vn_remove_services(_VN_guid_t guid, 
                        uint32_t services[], uint32_t nservices)
{
    _vn_device_info_t* device = _vn_lookup_device(guid);
    if (device) {
        int rv, n = 0;
        uint32_t i;
        for (i = 0; i < nservices; i++) {
            rv = _vn_device_delete_service(device, services[i]);
            if (rv == _VN_ERR_OK) {
                n++;
            }
        }
        return n;
    }
    else return _VN_ERR_NOTFOUND;
}

/* Device table access routines */

/**
 * Looks up the device from table.
 */
_vn_device_info_t* _vn_lookup_device(_VN_guid_t guid)
{
    _vn_device_info_t* device;
    device = _vn_ht_lookup(&_vn_devices, (_vn_ht_key_t) &guid);
    _VN_TRACE(TRACE_FINEST, _VN_SG_DEVICES, 
              "Look up device 0x%08x %u, got 0x%08x\n",
              guid.device_type, guid.chip_id, device);
    return device;
}

/**
 * Allocates memory for a new device.
 */
_vn_device_info_t* _vn_create_device(_VN_guid_t guid,
                                     uint8_t flags)
{
    _vn_device_info_t* device = _vn_malloc(sizeof(_vn_device_info_t));
    if (device) {
        device->flags = flags;
        device->guid = guid;
        device->inaddrs = NULL;
        device->services = NULL;
        device->timer = NULL;
#ifndef _VN_RPC_PROXY
        device->vnaddr = _VN_ADDR_INVALID;
        device->all_vnaddrs = NULL;
#else
        device->clients = 0;
#endif  /* _VN_RPC_PROXY */

        if (!_VN_DEVICE_IS_LOCAL(device) && !_VN_DEVICE_IS_ADHOC(device)) {
            /* For remote, non-adhoc devices, cancel timeout 
               once there is a new VN attached */
            _vn_device_set_timeout(device, _VN_DEVICE_TIMEOUT_MSECS);
        }
    }
    return device;
}

/**
 * Frees the memory used by device.
 */
void _vn_destroy_device(_vn_device_info_t* device)
{
    if (device) {
#ifndef _VN_RPC_PROXY
        if (device->all_vnaddrs) {
            _vn_dlist_destroy(device->all_vnaddrs, NULL);
        }
#endif  /* _VN_RPC_PROXY */
        if (device->inaddrs) {
            _vn_dlist_destroy(device->inaddrs, (void*) &_vn_sockaddr_destroy);
        }
        if (device->services) {
            _vn_dlist_destroy(device->services, (void*) &_vn_service_destroy);
        }
        if (device->timer) {
            _vn_timer_cancel(device->timer);
            _vn_timer_delete(device->timer);
        }

        _vn_free(device);
    }
}

/**
 * Adds device to table, creating new device if not already in table.
 * Returns _VN_ERR_DUPENTRY if device already exists.
 */
int _vn_add_device(_VN_guid_t guid, uint8_t flags)
{
    _vn_device_info_t* device;

    device = _vn_lookup_device(guid);
    if (device != NULL) {
        if (device->guid.device_type != guid.device_type) {
            _VN_TRACE(TRACE_WARN, _VN_SG_DEVICES,
                      "Device type for device %u differs: "
                      " existing 0x%08x, to add 0x%08x\n",
                      guid.chip_id,
                      device->guid.device_type, guid.device_type);
            if ((device->guid.device_type & ~_VN_DEVICE_TYPE_MASK) 
                 == _VN_DEVICE_TYPE_UNKNOWN)  {
                _VN_TRACE(TRACE_WARN, _VN_SG_DEVICES,
                          "Replacing existing device type for device %u\n",
                          guid.chip_id);
                device->guid.device_type = guid.device_type;
            }
        }
        device->flags |= flags;
        return _VN_ERR_DUPENTRY;
    }
    device = _vn_create_device(guid, flags);
    if (device == NULL) return _VN_ERR_NOMEM;

    if (device->guid.device_type == _VN_DEVICE_TYPE_UNKNOWN) {
        _VN_TRACE(TRACE_WARN, _VN_SG_DEVICES,
                  "Adding device %u of unknown type\n",
                  device->guid.chip_id);
    }
    return _vn_ht_add(&_vn_devices,
                      (_vn_ht_key_t) &device->guid, (void*) device);
}

/**
 * Removes device from table.
 * @param guid Guid of device to remove
 * @param reason Reason why the device is to be removed 
 *               Also used as a flag to indicate if vns should be disconnected.
 * Returns _VN_ERR_NOTFOUND if device not present.
 */
int _vn_remove_device(_VN_guid_t guid, uint8_t reason)
{
    _vn_device_info_t* device;
    _VN_TRACE(TRACE_FINER, _VN_SG_DEVICES,
              "Removing device 0x%08x %d, reason %d\n",
              guid.device_type, guid.chip_id, reason);
    device = _vn_ht_remove(&_vn_devices, (_vn_ht_key_t) &guid);
    if (device == NULL) return _VN_ERR_NOTFOUND;

#ifndef _VN_RPC_PROXY
/* Uncomment this if we decide to disconnect from nets if device is gone */
/*
    if (reason) {
        _vn_device_disconnect_nets(device, reason);
    }
*/
#endif

    _vn_destroy_device(device);
    return _VN_ERR_OK;
}

/**
 * A device is being disconnected. If device discovered via host discovery, 
 * post message that device is disconnected.  Remove device from table.
 */
int _vn_disconnect_device(_VN_guid_t guid)
{
    int rv;

#ifndef _VN_RPC_PROXY
    _vn_post_device_event(guid, _VN_EVT_R_DEVICE_DISCONNECTED);
#else
    _vn_proxy_device_removed_sync(guid, _VN_EVT_R_DEVICE_DISCONNECTED);
#endif  /* _VN_RPC_PROXY */

    rv = _vn_remove_device(guid, _VN_EVT_R_DEVICE_DISCONNECTED);

#ifdef _VN_RPC_DEVICE
    /* Sync with VNProxy */
    if (rv != _VN_ERR_NOTFOUND) {
        _vn_netif_remove_device(guid);
    }
#endif

    return rv;
}

/**
 * Returns the number of devices matching mask
 */
int _vn_get_device_count(uint8_t mask)
{
    if (mask == 0) {
        return _vn_ht_get_size(&_vn_devices);
    }
    else {
        return _vn_get_guids(mask, NULL, 0);
    }
}

/**
 * Retrieves the guids of devices that matches the specified mask. 
 * and returns them in the devices buffer.
 * @param mask      Bitmask of what devices to match
 *                  If mask is 0, all known guids are returned.
 * @param devices   Output buffer of guids 
 *                  If NULL, no guids will be returned.
 * @param ndevices  Number of available entries in devices
 * @return Number of guids matching mask (can be larger than ndevices)
 */ 
int _vn_get_guids(uint8_t mask, _VN_guid_t* devices, uint16_t ndevices)
{
    _vn_ht_iter_t iter;
    _vn_device_info_t* device;
    int i = 0;
    bool match;
    _vn_ht_iterator_init(&iter, &_vn_devices);
    while ((device = (_vn_device_info_t*) (_vn_ht_iterator_next(&iter)))) {
        if (mask) {
            /* Match if there is a bit in common */
            match = (mask & device->flags);        
        }
        else {
            match = true;
        }
        
        if (match) {
            if (i < ndevices) {
                devices[i] = device->guid;
            }
            i++;
        }
    }

    return i;
}

/**
 * Clears the service list of all devices matching the device_mask
 * @param match_mask   Bitmask of what devices to match
 *                     If mask is 0, all devices are matched.
 * @param filter_mask  Bitmask of what devices not to match
 *                     If mask is 0, no devices are filtered.
 * @return _VN_ERR_OK if successful
 */
int _vn_clear_services(uint8_t match_mask, uint8_t filter_mask)
{
    _vn_ht_iter_t iter;
    _vn_device_info_t* device;
    bool match;
    _vn_ht_iterator_init(&iter, &_vn_devices);
    while ((device = (_vn_device_info_t*) (_vn_ht_iterator_next(&iter)))) {
        if (match_mask) {
            /* Match if there is a bit in common */
            match = (match_mask & device->flags);        
        }
        else {
            match = true;
        }
        
        if (match) {
            if (!(device->flags & filter_mask)) {
                _vn_device_clear_services(device);
            }
        }
    }

    return _VN_ERR_OK;
}

/**
 * Clears flag from all devices
 * @param flag Flag to clear
 * @return _VN_ERR_OK if successful
 */
int _vn_clear_device_flag(uint8_t flag)
{
    _vn_ht_iter_t iter;
    _vn_device_info_t* device;

    _vn_ht_iterator_init(&iter, &_vn_devices);
    while ((device = (_vn_device_info_t*) (_vn_ht_iterator_next(&iter)))) {
        device->flags &= ~flag;
    }

    return _VN_ERR_OK;
}

int _vn_init_device_table()
{
    int rv;
#ifdef _SC
    rv = _vn_ht_init(&_vn_devices, 
                     _VN_DEVICE_TABLE_SIZE, _VN_DEVICE_TABLE_SIZE, false,
                     &_vn_ht_hash_guid, &_vn_ht_key_comp_guid);
#else
    rv = _vn_ht_init(&_vn_devices, _VN_DEVICE_TABLE_SIZE, 0, true,
                     &_vn_ht_hash_guid, &_vn_ht_key_comp_guid);
#endif
    if (rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_DEVICES, 
                  "Failed to initialize table for devices: %d\n", rv);
        return rv;        
    }

    return _VN_ERR_OK;
}

int _vn_clear_device_table()
{
    _vn_ht_clear(&_vn_devices, (void*) _vn_destroy_device, false);
    return _VN_ERR_OK;
}

int _vn_destroy_device_table()
{
    _vn_ht_clear(&_vn_devices, (void*) _vn_destroy_device, true);
    return _VN_ERR_OK;
}

/* Device timeout */

void _vn_device_timer_cb(_vn_timer_t* timer)
{
    _vn_device_info_t* device;
    _VN_guid_t guid;
    int rv;

    assert(timer);
    device = (_vn_device_info_t*) timer->data;
    assert(device);
    guid = device->guid;
    _VN_TRACE(TRACE_FINER, _VN_SG_DEVICES,
              "_vn_device_timer_cb: Device %u timed out\n",
              guid);

#ifndef _VN_RPC_PROXY
    _vn_post_device_event(guid, _VN_EVT_R_DEVICE_TIMEOUT);
#else
    _vn_proxy_device_removed_sync(guid, _VN_EVT_R_DEVICE_TIMEOUT);
#endif /* _VN_RPC_PROXY */

    rv = _vn_remove_device(guid, _VN_EVT_R_DEVICE_TIMEOUT);

#ifdef _VN_RPC_DEVICE
    /* Sync with VNProxy */
    if (rv != _VN_ERR_NOTFOUND) {
        _vn_netif_remove_device(guid);
    }
#endif
}

int _vn_device_set_timeout(_vn_device_info_t* device, uint32_t msecs)
{
    assert(device);
    if (msecs != _VN_TIMEOUT_NONE) {
        if (device->timer == NULL) {
            device->timer =_vn_timer_create(msecs, device, 
                                            _vn_device_timer_cb);
            if (device->timer == NULL) {
                return _VN_ERR_NOMEM;
            }
            
            _vn_timer_add(device->timer);
        }
        else {
            int rv = _vn_timer_mod(device->timer, msecs, true);
            if (rv == _VN_ERR_INACTIVE) {
                _vn_timer_add(device->timer);
            }
        }
    }
    else if (device->timer) {
        _vn_timer_cancel(device->timer);
    }
    return _VN_ERR_OK;
}

#ifndef _VN_RPC_PROXY

/* Function for looking up guid */

int _vn_get_guid(_VN_addr_t addr, _VN_guid_t* guid)
{
    _VN_net_t net_id;
    _VN_host_t host_id;
    _vn_net_info_t* net_info;
    _vn_host_info_t* host_info;
    assert(guid);
    _vn_guid_set_invalid(guid);

    net_id = _VN_addr2net(addr);
    host_id = _VN_addr2host(addr);
    net_info = _vn_lookup_net_info(net_id);
    if (net_info == NULL) return _VN_ERR_NETID;
    host_info = _vn_lookup_host_info(net_info, host_id);
    if (host_info == NULL) return _VN_ERR_HOSTID;
    *guid = host_info->guid;
    return _VN_ERR_OK;
}

#else /* _VN_RPC_PROXY */

int _vn_device_set_client(_VN_guid_t guid, int client)
{
    _vn_device_info_t* device;
    assert((client >= 0) && (client < _VN_PROXY_MAX_DEVICES));
    device = _vn_lookup_device(guid);
    if (device && !_VN_DEVICE_IS_ADHOC(device)) {
        int cflags = _VN_CLIENT_TO_FLAG(client+1);
        device->clients |= cflags;
        _vn_device_set_timeout(device, _VN_TIMEOUT_NONE);
        return _VN_ERR_OK;
    }
    else {
        return _VN_ERR_NOTFOUND;
    }    
}

int _vn_device_clear_client(_VN_guid_t guid, int client, bool remove_device)
{
    _vn_device_info_t* device;
    assert((client >= 0) && (client < _VN_PROXY_MAX_DEVICES));
    device = _vn_lookup_device(guid);
    if (device && !_VN_DEVICE_IS_ADHOC(device)) {
        int cflags = _VN_CLIENT_TO_FLAG(client+1);
        device->clients &= ~cflags;
        if (remove_device && (device->clients == 0)) {
            /* No more clients interested */
            _vn_device_set_timeout(device, 0);
        }
        return _VN_ERR_OK;
    }
    else {
        return _VN_ERR_NOTFOUND;
    }    
    
}

int _vn_devices_clear_client(int client, bool remove_device)
{
    _vn_ht_iter_t iter;
    _vn_device_info_t* device;
    _vn_ht_iterator_init(&iter, &_vn_devices);
    while ((device = (_vn_device_info_t*) (_vn_ht_iterator_next(&iter)))) {
        if (!_VN_DEVICE_IS_ADHOC(device)) {
            int cflags = _VN_CLIENT_TO_FLAG(client+1);
            device->clients &= ~cflags;
            if (remove_device && (device->clients == 0)) {
                /* No more clients interested */
                _vn_device_set_timeout(device, 0);
            }
        }
    }
    return _VN_ERR_OK;
}
#endif  /* _VN_RPC_PROXY */

/* Functions for managing the device type */

#ifndef _VN_RPC_PROXY
/* Sets the default virtual net for this device */
int _vn_device_set_vnaddr(_VN_guid_t guid, _VN_addr_t vnaddr)
{
    _vn_device_info_t* device;

    device = _vn_lookup_device(guid);
    if (device) {
        device->vnaddr = vnaddr;
        _vn_device_add_vnaddr(guid, vnaddr);
        return _VN_ERR_OK;
    }
    else return _VN_ERR_NOTFOUND;
}

int _vn_device_add_vnaddr(_VN_guid_t guid, _VN_addr_t vnaddr)
{
    _vn_device_info_t* device;

    device = _vn_lookup_device(guid);
    if (device) {
        if (device->all_vnaddrs) {
            _vn_dnode_t* node;            
            _vn_dlist_for(device->all_vnaddrs, node) 
            {
                _VN_addr_t tmp_vnaddr = (_VN_addr_t) _vn_dnode_value(node);
                if (tmp_vnaddr == vnaddr) {
                    return _VN_ERR_DUPENTRY;
                }
            }
        }
        else {
            device->all_vnaddrs = _vn_dlist_create();
            if (device->all_vnaddrs == NULL) {
                return _VN_ERR_NOMEM;
            }

            if (!_VN_DEVICE_IS_LOCAL(device) && !_VN_DEVICE_IS_ADHOC(device)) {
                /* For remote, non-adhoc devices, cancel timeout 
                   once there is a new VN attached */
                _vn_device_set_timeout(device, _VN_TIMEOUT_NONE);
            }
        }

        /* Not found, add entry */
        _vn_dlist_add(device->all_vnaddrs, (void*) vnaddr);
        return _VN_ERR_OK;
    }
    else return _VN_ERR_NOTFOUND;
}

int _vn_device_remove_vnaddr(_VN_guid_t guid, _VN_addr_t vnaddr)
{
    _vn_device_info_t* device;

    device = _vn_lookup_device(guid);
    if (device && device->all_vnaddrs) {
        _vn_dnode_t *node, *temp;            
        _vn_dlist_delete_for(device->all_vnaddrs, node, temp) 
        {
            _VN_addr_t tmp_vnaddr = (_VN_addr_t) _vn_dnode_value(node);
            if (tmp_vnaddr == vnaddr) {
                _vn_dlist_delete(device->all_vnaddrs, node);
                if (!_VN_DEVICE_IS_LOCAL(device) && 
                    !_VN_DEVICE_IS_ADHOC(device)) {
                    /* For remote, non-adhoc devices, timeout
                       when there is no longer any VNs attached */
                    if (_vn_dlist_size(device->all_vnaddrs) == 0) {
                        _vn_device_set_timeout(device, 
                                               _VN_DEVICE_TIMEOUT_MSECS);
                    }
                }
                return _VN_ERR_OK;
            }
        }
        return _VN_ERR_NOTFOUND;
    }
    else return _VN_ERR_NOTFOUND;
}

int _vn_devices_remove_net(_VN_net_t net_id)
{
    _vn_net_info_t* net_info;
    net_info = _vn_lookup_net_info(net_id);
    if (net_info != NULL) {
        _vn_dnode_t* node;
        _vn_dlist_for(net_info->peer_hosts, node)
        {
            if (_vn_dnode_value(node)) {
                _vn_host_info_t* host_info = (_vn_host_info_t*) 
                    _vn_dnode_value(node);
                _VN_addr_t vnaddr = _VN_make_addr(net_info->net_id,
                                                  host_info->host_id);
                _vn_device_remove_vnaddr(host_info->guid, vnaddr);
            }
        }
        return _VN_ERR_OK;
    }
    else return _VN_ERR_NETID;
}

#ifndef _VN_RPC_PROXY
/**
 * Precondition: The device should already be removed from the device table
 * Disconnects the leaving device from VNs for which  I am the owner
 */
int _vn_device_disconnect_nets(_vn_device_info_t* device, uint8_t reason)
{
    int n = 0;

    if (device && device->all_vnaddrs) {
        _vn_dnode_t *node, *temp;            
        _vn_dlist_delete_for(device->all_vnaddrs, node, temp) 
        {
            _VN_addr_t vnaddr = (_VN_addr_t) _vn_dnode_value(node);
            _vn_net_info_t* net;                

            _vn_dlist_delete(device->all_vnaddrs, node);

            net =  _vn_lookup_net_info(_VN_addr2net(vnaddr));
            if (net && _VN_NET_IS_OWNER(net)) {
                _vn_host_info_t *to_host, *from_host;
                
                _VN_TRACE(TRACE_FINER, _VN_SG_NETCORE,
                          "Disconnect net for device 0x%08x %u: "
                          "Processing vnaddr 0x%08x\n", 
                          device->guid.device_type, device->guid.chip_id, vnaddr);
                to_host = _vn_lookup_local_host_info(net, net->master_host);
                from_host = _vn_lookup_host_info(net, _VN_addr2host(vnaddr));
                
                if (to_host && from_host && 
                    _vn_guid_eq(&from_host->guid, &device->guid)) {
                    _vn_owner_notified_host_left(net, from_host, to_host, 
                                                 reason);
                    n++;
                }
            }
        }
    }
    return n;
}
#endif

/* Adds this device */
int _vn_device_add_local()
{
    _vn_device_info_t* device;
    _VN_guid_t guid;
    _vn_netif_t* netif;
    _vn_inport_t port;
    _vn_inaddr_t ipaddr;

    guid.chip_id = _vn_get_my_chip_id();
    guid.device_type = _vn_get_my_device_type();
    device = _vn_lookup_device(guid);
    if (device == NULL) {
        int rv = _vn_add_device(guid, _VN_DEVICE_PROP_LOCAL);
        if (rv < 0) {
            return rv;
        }
        device = _vn_lookup_device(guid);
    }

    assert(device != NULL);

    _vn_get_default_localaddr(_VN_NET_DEFAULT);

    netif = _vn_netif_get_instance();
    ipaddr = _vn_netif_getlocalip(netif);
    port = _vn_netif_getlocalport(netif);

    if (netif->reqip == _VN_INADDR_INVALID) {
        _vn_inaddr_t* ipaddrs;
        int i, n;

        /* Add list of all valid non-loopback IP */
        n =  _vn_netif_getlocalips(netif, &ipaddrs);
        for (i = 0; i < n; i++) {
            _vn_device_add_connect_info(device, _VN_INADDR_LOCAL,
                                        ipaddrs[i], port);
        }
        
        if (ipaddrs) {
            _vn_free(ipaddrs);
        }
    }
    else {
        /* Just add main VN IP */
        _vn_device_add_connect_info(device, _VN_INADDR_LOCAL,
                                    ipaddr, port);
    }

    return _VN_ERR_OK;
}
#endif  /* _VN_RPC_PROXY */

/* Encodes one service into a binary buffer */
int _vn_device_encode_service(_vn_service_t* service, uint8_t* buf, size_t buflen)
{
    size_t reqlen;

    assert(service);
    reqlen = service->msglen + sizeof(service->msglen) 
        + sizeof(service->service_id) + sizeof(service->flags);
 
    if (buf) {
        if (buflen < reqlen) {
            return _VN_ERR_TOO_SHORT;
        }
        else {
            uint32_t service_id = htonl(service->service_id);
            uint16_t msglen = htons(service->msglen);
               
            memcpy(buf, &service_id, sizeof(service_id));
            buf += sizeof(service_id);
            memcpy(buf, &msglen, sizeof(msglen));
            buf += sizeof(msglen);
            memcpy(buf, &service->flags, sizeof(service->flags));
            buf += sizeof(service->flags);
            memcpy(buf, service->msg, service->msglen);
        }        
    }

    return (int) reqlen;
}

/* Encodes all services of the device into a binary buffer */
int _vn_device_encode_service_list(_vn_device_info_t *device, uint8_t mask,
                                   uint8_t* buf, size_t buflen)
{
    uint8_t* start = buf;
    size_t reqlen;
    uint32_t nservices = 0;
    int rv;

    reqlen = sizeof(nservices);
    if (buf) {
        if (buflen < reqlen) {
            return _VN_ERR_TOO_SHORT;
        }
        buf += reqlen;
        buflen -= reqlen;
    }

    if (device && device->services) {                
        _vn_dnode_t* node;

        _VN_TRACE(TRACE_FINER, _VN_SG_ADHOC,
                  "Encoding service list for device 0x%08x %u\n",
                  device->guid.device_type, device->guid.chip_id);

        _vn_dlist_for(device->services, node)
        {
            if (_vn_dnode_value(node)) {
                _vn_service_t* service = (_vn_service_t*) 
                    _vn_dnode_value(node);

                if (service && ((mask == 0) || (service->flags & mask))) {
                    rv = _vn_device_encode_service(service, buf, buflen);
                    if (rv < 0) {
                        return rv;
                    }

                    if (buf) {
                        assert((size_t) rv <= buflen);
                        buf += rv;
                        buflen -= rv;
                    }

                    nservices++;                    
                    reqlen += rv;
                }
            }
        }
    }

    if (buf) {
        /* Fill in number of services */
        nservices = htonl(nservices);
        memcpy(start, &nservices, sizeof(nservices));
    }

    return (int) reqlen;
}

/* Decodes a binary represention of a service into the service data structure */
int _vn_device_decode_service(_vn_service_t* service, uint8_t* buf, size_t buflen)
{
    uint32_t service_id;
    uint16_t msglen;
    uint8_t flags;
    size_t reqlen;

    assert(service);
    assert(buf);

    reqlen = sizeof(service_id) + sizeof(msglen) + sizeof(flags);

    if (buflen < reqlen) {
        return _VN_ERR_TOO_SHORT;
    }

    memcpy(&service_id, buf, sizeof(service_id));
    buf += sizeof(service_id);
    service_id = ntohl(service_id);

    memcpy(&msglen, buf, sizeof(msglen));
    buf += sizeof(msglen);
    msglen = ntohs(msglen);

    memcpy(&flags, buf, sizeof(flags));
    buf += sizeof(flags);

    reqlen += msglen;
    if (buflen < reqlen) {
        return _VN_ERR_TOO_SHORT;
    }

    service->msglen = msglen;
    service->service_id = service_id;
    service->msg = buf;
    service->flags = flags;

    return (int) reqlen;
}

/* Decodes a binary represention of a list of services 
   and adds them to the device.  The list of service ids is returned
   in pServices and pServicesCount. */
int _vn_device_decode_service_list(_vn_device_info_t *device,
                                   uint8_t* buf, size_t buflen,
                                   uint32_t** pServices,
                                   uint32_t* pServicesCount)
{
    _vn_service_t service;
    uint32_t i, nservices;
    size_t reqlen;
    int rv;

    assert(device);
    assert(buf);

    _VN_TRACE(TRACE_FINER, _VN_SG_ADHOC,
              "Decoding service list for device 0x%08x %u\n",
              device->guid.device_type, device->guid.chip_id);

    reqlen = sizeof(nservices);
    if (buflen < reqlen) {
        _VN_TRACE(TRACE_WARN, _VN_SG_ADHOC,
                  "Service list for device 0x%08x %u too short\n",
                  device->guid.device_type, device->guid.chip_id);

        if (pServices) { *pServices = NULL; }
        if (pServicesCount) { *pServicesCount = 0; }
        return _VN_ERR_TOO_SHORT;
    }

    memcpy(&nservices, buf, sizeof(nservices));
    buf += sizeof(nservices);
    buflen -= sizeof(nservices);
    nservices = ntohl(nservices);

    if (pServicesCount) {
        *pServicesCount = nservices;
        if (pServices) {
            *pServices = _vn_malloc(sizeof(uint32_t)*nservices);
        }
    }

    _VN_TRACE(TRACE_FINER, _VN_SG_ADHOC,
              "Got %d services for device 0x%08x %u\n", nservices,
              device->guid.device_type, device->guid.chip_id);
    for (i = 0; i < nservices; i++) {
        _VN_TRACE(TRACE_FINER, _VN_SG_ADHOC,
                  "Decode %d/%d service for device 0x%08x %u\n", 
                  i+1, nservices,
                  device->guid.device_type, device->guid.chip_id);

        rv = _vn_device_decode_service(&service, buf, buflen);
        if (rv < 0) {
            _VN_TRACE(TRACE_WARN, _VN_SG_ADHOC,
                      "Error %d decoding %d/%d service for device 0x%08x %u\n",
                      rv, i+1, nservices,
                      device->guid.device_type, device->guid.chip_id);
            return rv;
        }

        if (pServices && *pServices) {
            (*pServices)[i] = service.service_id;
        }

        assert((size_t) rv <= buflen);
        buf += rv;
        buflen -= rv;
        reqlen += rv;

        rv = _vn_device_add_service(device, service.service_id, service.flags,
                                    service.msg, service.msglen);
        if (rv < 0) {
            if (rv == _VN_ERR_DUPENTRY) {
                _VN_TRACE(TRACE_WARN, _VN_SG_ADHOC,
                          "Duplicate service %d for device 0x%08x %u\n",
                          service.service_id,
                          device->guid.device_type, device->guid.chip_id);
            }
            else {
                _VN_TRACE(TRACE_WARN, _VN_SG_ADHOC,
                          "Unable to add service %d to device 0x%08x %u, rv = %d\n",
                          service.service_id,
                          device->guid.device_type, device->guid.chip_id, rv);
            }
        }
    }

    return (int) reqlen;
}

#define _VN_SOCKADDR_SIZE      8

/* Encodes inaddrs into a binary buffer */
int _vn_device_encode_inaddrs(_vn_device_info_t* device,
                              uint8_t* buf, size_t buflen)
{
    size_t reqlen;
    uint8_t n_inaddrs;

    assert(device);

    reqlen = sizeof(n_inaddrs);

    if (device->inaddrs) {
        assert(_vn_dlist_size(device->inaddrs) < _VN_INADDR_MAX);
        n_inaddrs = (uint8_t) _vn_dlist_size(device->inaddrs);
        reqlen += n_inaddrs*_VN_SOCKADDR_SIZE;
    }
    else {
        n_inaddrs = 0;
    }

    if (buf) {
        if (buflen < reqlen) {
            return _VN_ERR_TOO_SHORT;
        }

        /* encode inaddrs */
        memcpy(buf, &n_inaddrs, sizeof(n_inaddrs));
        buf += sizeof(n_inaddrs);

        if (device->inaddrs) {
            _vn_dnode_t* node;
            _vn_sockaddr_t* sockaddr;
            _vn_inport_t port;

            _vn_dlist_for(device->inaddrs, node)
            {
                sockaddr = (_vn_sockaddr_t*) _vn_dnode_value(node);
                assert(sockaddr);
                *buf++ = sockaddr->flags;
                *buf++ = sockaddr->family;
                port = htons(sockaddr->port);
                memcpy(buf, &port, sizeof(port));
                buf += sizeof(port);
                memcpy(buf, &sockaddr->addr, sizeof(sockaddr->addr));
                buf += sizeof(sockaddr->addr);
            }
        }        
    }

    return (int) reqlen;
}

/* Decodes inaddrs from a binary buffer and adds them to the device */
int _vn_device_decode_inaddrs(_vn_device_info_t* device,
                              uint8_t* buf, size_t buflen)
{
    _vn_sockaddr_t sockaddr;
    size_t reqlen;
    uint8_t n_inaddrs;
    int i;

    assert(device);
    assert(buf);

    reqlen = sizeof(n_inaddrs);

    if (buflen < reqlen) {
        return _VN_ERR_TOO_SHORT;
    }
    
    memcpy(&n_inaddrs, buf, sizeof(n_inaddrs));
    buf += sizeof(n_inaddrs);

    reqlen += n_inaddrs*_VN_SOCKADDR_SIZE;
    if (buflen < reqlen) {
        return _VN_ERR_TOO_SHORT;
    }

    for (i = 0; i < n_inaddrs; i++) {
        sockaddr.flags = *buf++;
        sockaddr.family = *buf++;
        assert(sockaddr.family == AF_INET);

        memcpy(&sockaddr.port, buf, sizeof(sockaddr.port));
        buf += sizeof(sockaddr.port);
        sockaddr.port = ntohs(sockaddr.port);
        memcpy(&sockaddr.addr, buf, sizeof(sockaddr.addr));
        buf += sizeof(sockaddr.addr);
        _vn_device_add_connect_info(device, sockaddr.flags,
                                    sockaddr.addr, sockaddr.port);
    }

    return (int) reqlen;
}

/* Encodes one device into a binary buffer */
int _vn_device_encode_device(_vn_device_info_t* device,
                             uint8_t* buf, size_t buflen)
{
    size_t reqlen;
    int rv;
    _VN_guid_t guid;
    uint32_t expires;

    assert(device);

    reqlen = sizeof(device->guid) + sizeof(device->flags) + sizeof(expires);


    if (buf) {
        if (buflen < reqlen) {
            return _VN_ERR_TOO_SHORT;
        }

        /* encode misc fields */
        guid.chip_id = htonl(device->guid.chip_id);
        guid.device_type = htonl(device->guid.device_type);
        expires = htonl(_vn_timer_get_remaining(device->timer));

        memcpy(buf, &guid, sizeof(guid));
        buf += sizeof(guid);
        memcpy(buf, &expires, sizeof(expires));
        buf += sizeof(expires);
        memcpy(buf, &device->flags, sizeof(device->flags));
        buf += sizeof(device->flags);

        /* subtract from buflen */
        buflen -= reqlen;
    }

    /* encode inaddrs */
    rv = _vn_device_encode_inaddrs(device, buf, buflen);
    if (rv < 0) { return rv; }
    reqlen += rv;

    if (buf) {
        buf += rv;
        buflen -= rv;
    }

    /* encode services */
    rv = _vn_device_encode_service_list(device, 0, buf, buflen);
    if (rv < 0) { return rv; }
    reqlen += rv;

    return (int) reqlen;
}

/* Decodes one device from a binary buffer */
int _vn_device_decode_device(_vn_device_info_t* device,
                             uint8_t* buf, size_t buflen)
{
    size_t reqlen;
    int rv;
    uint32_t expires;
    int8_t flags;

    assert(device);
    assert(buf);

    reqlen = sizeof(device->guid) + sizeof(device->flags) + sizeof(expires);

    if (buflen < reqlen) {
        return _VN_ERR_TOO_SHORT;
    }

    /* decode misc fields */
    memcpy(&device->guid, buf, sizeof(device->guid));
    device->guid.device_type = ntohl(device->guid.device_type);
    device->guid.chip_id = ntohl(device->guid.chip_id);
    buf += sizeof(device->guid);

    memcpy(&expires, buf, sizeof(expires));
    expires = ntohl(expires);
    buf += sizeof(expires);
    _vn_device_set_timeout(device, expires);

    memcpy(&flags, buf, sizeof(flags));
    buf += sizeof(flags);
#ifdef _VN_RPC_DEVICE
    /* Got device flags from proxy, keep it */
    device->flags |= flags;
#else
    /* Got flags from remote device, ignore, need to figure out our own flags */
#endif
           
    /* subtract from buflen */
    buflen -= reqlen;

    /* decode inaddrs */
    rv = _vn_device_decode_inaddrs(device, buf, buflen);
    if (rv < 0) { return rv; }
    reqlen += rv;
    buf += rv;
    buflen -= rv;

    /* decode services */
    rv = _vn_device_decode_service_list(device, buf, buflen, NULL, NULL);
    if (rv < 0) { return rv; }
    reqlen += rv;

    return (int) reqlen;
}

/* Decode service and add it to the list of services for the device */
int _vn_add_encoded_service(_VN_guid_t guid, uint8_t* buf, size_t buflen,
                            uint32_t* pService)
{
    _vn_device_info_t *device;
    _vn_service_t service;
    int rv;

    assert(buf);

    if (_vn_guid_is_self(&guid)) {
        /* Don't bother updating ourselves */
        return _VN_ERR_OK;
    }

    device = _vn_lookup_device(guid);
    if (device == NULL) {
        return _VN_ERR_NOTFOUND;
    }

    rv = _vn_device_decode_service(&service, buf, buflen);
    if (rv < 0) {
        return rv;
    }

    rv = _vn_device_add_service(device, service.service_id, service.flags,
                                service.msg, service.msglen);

    if (pService) {
        *pService = service.service_id;
    }

    return rv;
}

/* Decode device and add it to the device table */
int _vn_add_encoded_device(uint8_t* buf, size_t buflen, int* decoded_len,
                           _VN_guid_t* pDeviceId, uint8_t *reason)
{
    _VN_guid_t guid;
    int rv;
    _vn_device_info_t *device;

    assert(buf);

    if (reason) { *reason = 0; }

    if (decoded_len) *decoded_len = 0;

    /* Don't know the guid yet, peek in buf */
    if (buflen < sizeof(_VN_guid_t)) {
        if (pDeviceId) { 
            _vn_guid_set_invalid(pDeviceId);
        }
        return _VN_ERR_TOO_SHORT;
    }
    memcpy(&guid, buf, sizeof(guid));
    guid.device_type = ntohl(guid.device_type);
    guid.chip_id = ntohl(guid.chip_id);
     
    if (pDeviceId) {
        pDeviceId->device_type = guid.device_type;
        pDeviceId->chip_id = guid.chip_id;
    }
   
    if (_vn_guid_is_self(&guid)) {
        /* Don't bother updating ourselves */
        return _VN_ERR_OK;
    }    

    /* Check if we already have this device */
    device = _vn_lookup_device(guid);
    if (device == NULL) {
        device = _vn_create_device(guid, 0);
        if (device) {
            rv = _vn_device_decode_device(device, buf, buflen);
            if (rv >= 0) {
                if (decoded_len) *decoded_len = rv;

                if (device->guid.device_type == _VN_DEVICE_TYPE_UNKNOWN) {
                    _VN_TRACE(TRACE_WARN, _VN_SG_DEVICES,
                              "Adding device %u of unknown type\n",
                              device->guid.chip_id);
                }

                rv = _vn_ht_add(_vn_get_device_table(), 
                                (_vn_ht_key_t) &device->guid, 
                                (void*) device);
                if (reason) { *reason = _VN_EVT_R_DEVICE_NEW; }
            }
            else {
                _vn_destroy_device(device);
            }
        }
        else {
            rv = _VN_ERR_NOMEM;
        }
    }
    else {
        /* TODO: DO we really want to overwrite existing device? */
        int old_service_count = 0;

        if (reason) {
            old_service_count = _vn_device_get_service_count(device);
        }

        rv = _vn_device_decode_device(device, buf, buflen);
        if (rv >= 0) {
            if (decoded_len) *decoded_len = rv;
        }

        if (reason) {
            /* Check if services have been added */
            if (old_service_count != _vn_device_get_service_count(device)) {
                *reason = _VN_EVT_R_DEVICE_UPDATE; 
            }
        }
    }

    if (rv >=0) {
        rv = _VN_ERR_OK;
    }

    return rv;
}

/* Encodes device list into a binary buffer */
int _vn_device_encode_device_list(_VN_guid_t* guids, int nguids,
                                  uint8_t* buf, size_t buflen)
{
    uint8_t* start = buf;
    size_t reqlen;
    int i, rv, ndevices = 0;

    if ((guids == NULL) || (nguids == 0)) {
        return 0;
    }

    reqlen = sizeof(ndevices);
    if (buf) {
        if (buflen < reqlen) {
            return _VN_ERR_TOO_SHORT;
        }
        buf += reqlen;
        buflen -= reqlen;
    }

    _VN_TRACE(TRACE_FINER, _VN_SG_ADHOC,
              "Encoding device list for %u devices\n", nguids);

    for (i = 0; i < nguids; i++) {
        _vn_device_info_t* device;
        device = _vn_lookup_device(guids[i]);
        if (device) {
            rv = _vn_device_encode_device(device, buf, buflen);
            if (rv < 0) {
                return rv;
            }
            
            if (buf) {
                assert((size_t) rv <= buflen);
                buf += rv;
                buflen -= rv;
            }
            
            ndevices++;
            reqlen += rv;
        }
    }

    if (buf) {
        /* Fill in number of devices */
        ndevices = htonl(ndevices);
        memcpy(start, &ndevices, sizeof(ndevices));
    }

    return (int) reqlen;
}

/* Decodes a binary represention of a list of devices 
   and adds them to the device table. */
int _vn_device_decode_device_list(uint8_t* buf, size_t buflen,
                                  _VN_guid_t** pDevices, uint8_t** pReasons,
                                  int* pDevicesCount)
{
    _VN_guid_t guid;
    uint8_t reason;
    uint32_t ndevices, i;
    size_t reqlen;
    int rv, decoded_len;

    assert(buf);

    _VN_TRACE(TRACE_FINER, _VN_SG_ADHOC, "Decoding device list\n");

    reqlen = sizeof(ndevices);
    if (buflen < reqlen) {
        _VN_TRACE(TRACE_WARN, _VN_SG_ADHOC,
                  "Encoded device list too short\n");

        if (pDevicesCount) { *pDevicesCount = 0; }
        if (pDevices) { *pDevices = NULL; }
        if (pReasons) { *pReasons = NULL; }

        return _VN_ERR_TOO_SHORT;
    }

    memcpy(&ndevices, buf, sizeof(ndevices));
    buf += sizeof(ndevices);
    buflen -= sizeof(ndevices);
    ndevices = ntohl(ndevices);

    if (pDevicesCount) {
        *pDevicesCount = ndevices;

        if (pDevices) {
            *pDevices = _vn_malloc(sizeof(_VN_guid_t)*ndevices);
        }

        if (pReasons) {
            *pReasons = _vn_malloc(sizeof(uint8_t)*ndevices);
        }
    }
    else {
        if (pDevices) { *pDevices = NULL; }
        if (pReasons) { *pReasons = NULL; }
    }

    _VN_TRACE(TRACE_FINER, _VN_SG_ADHOC,
              "Got %d devices\n", ndevices);

    for (i = 0; i < ndevices; i++) {
        _VN_TRACE(TRACE_FINER, _VN_SG_ADHOC,
                  "Decode %d/%d device\n", i+1, ndevices);

        rv = _vn_add_encoded_device(buf, buflen, &decoded_len,
                                    &guid, &reason);
        if (decoded_len <= 0) {
            _VN_TRACE(TRACE_WARN, _VN_SG_ADHOC,
                      "Error %d decoding %d/%d device \n", 
                      rv, i+1, ndevices);
            return rv;
        }

        if (pDevices && *pDevices) {
            (*pDevices)[i] = guid;
        }

        if (pReasons && *pReasons) {
            (*pReasons)[i] = reason;
        }
                
        assert((size_t) decoded_len <= buflen);
        buf += decoded_len;
        buflen -= decoded_len;
        reqlen += decoded_len;
    }

    return (int) reqlen;
}




//
//               Copyright (C) 2006, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "../vnusb.h"
#include "../vndebug.h"

#define _VN_USB_DEFAULT_PATH "/dev/usb/bbrdb"

_vn_usb_handle_t _vn_lookup_usb_handle(int usbNum)
{
    int i;
    _vn_usb_t *usb;

    for (i = 0; i < _VN_USB_MAX; i++) {
        usb = _vn_usb_get(i);
        if (usb && usb->handle != _VN_USB_HANDLE_INVALID) {
            if (usb->mode == _VN_USB_MODE_REAL) {
                if (usb->usb.ureal.usbNum == usbNum) {
                    return usb->handle;
                }
            }
        }
    }
    return _VN_USB_HANDLE_INVALID;
}

int _vn_open_next_usb(int * pNum)
{
    char usbPath[32];
    int i, rv;

    assert(pNum);
    for (i = 0; i < _VN_USB_MAX; i++) {
        if (_vn_lookup_usb_handle(i) == _VN_USB_HANDLE_INVALID) {
            /* Not associated with any VN USB Handle yet */
            break;
        }
    }
    *pNum = i;
 
    sprintf(usbPath, "%s%d", _VN_USB_DEFAULT_PATH, i);
    rv = open(usbPath, O_RDWR);
    if (rv < 0) {
        _vn_errno_t err = _vn_sys_get_errno();
        _vn_errstr_t errstr = _vn_sys_get_errstr(err);
        _VN_TRACE((err == ENODEV)? TRACE_FINER:TRACE_ERROR, _VN_SG_USB, 
                  "Error opening USB device %s: %s\n", 
                  usbPath, errstr);
        _vn_sys_free_errstr(errstr);
        *pNum = -1;
    }
    return rv;
}

int _vn_init_usb_real(_vn_usb_t* usb)
{
    _vn_usb_real_t* usb_real;
    int rv = _VN_ERR_OK;

    assert(usb);
    assert(usb->mode == _VN_USB_MODE_REAL);

    usb_real = &usb->usb.ureal;

    /* Initialize USB communication */
    usb_real->fd = _vn_open_next_usb(&usb_real->usbNum);
    if (usb_real->fd < 0) {
        rv = _VN_ERR_USB;
    } else {     
        if (fcntl(usb_real->fd, F_SETFL, O_NONBLOCK) < 0) {
            _vn_errstr_t errstr = _vn_sys_get_errstr(_vn_sys_get_errno());
            _VN_TRACE(TRACE_ERROR, _VN_SG_USB, 
                      "Error setting USB to nonblocking: %s\n", errstr);
            _vn_sys_free_errstr(errstr);        
        }
    }

    return rv;
}

int _vn_shutdown_usb_real(_vn_usb_t* usb)
{
    _vn_usb_real_t* usb_real;

    assert(usb);
    assert(usb->mode == _VN_USB_MODE_REAL);

    usb_real = &usb->usb.ureal;

    if (usb_real->fd >= 0) {
        close(usb_real->fd);
    }

    return _VN_ERR_OK;
}

/* Blocking read */
int _vn_read_usb_real(_vn_usb_t* usb, void* buf, size_t len, uint32_t timeout)
{
    fd_set rset;
    _vn_usb_real_t* usb_real;
    int rv = 0;

    assert(usb);
    assert(usb->mode == _VN_USB_MODE_REAL);

    usb_real = &usb->usb.ureal;

    /* Assumes there is only one reader thread (no lock around read) */
    if (usb_real->fd < 0) {
        return _VN_ERR_CLOSED;
    }

    FD_ZERO(&rset);
    FD_SET(usb_real->fd, &rset);
    _VN_TRACE(TRACE_FINEST, _VN_SG_USB, "USB waiting for %d bytes\n", len);
    rv = _vn_select((int) usb_real->fd + 1, &rset, NULL, NULL, timeout);
    if (rv >= 0) {
        if (FD_ISSET(usb_real->fd, &rset)) {
            _VN_TRACE(TRACE_FINEST, _VN_SG_USB,
                      "USB reading %d bytes\n", len);
again:
            rv = read(usb_real->fd, buf, len);
            if (rv >= 0) {
                /* 0 indicates end of stream */
                /*if (rv == 0) {
                  rv = _VN_ERR_CLOSED;
                  } */
            } else {
                _vn_errno_t err = _vn_sys_get_errno();
                _vn_errstr_t errstr = _vn_sys_get_errstr(err);
                if (err == EAGAIN) {
                    goto again;
                }
                _VN_TRACE(TRACE_ERROR, _VN_SG_USB,
                          "USB read failed: %s\n", errstr);
                _vn_sys_free_errstr(errstr);
                rv = _VN_ERR_USB;
            }
        } else {
            rv = _VN_ERR_TIMEOUT;
        }
    }
    return rv;

}

extern _VN_time_t _vn_get_timestamp();

/* Blocking write */
int _vn_write_usb_real(_vn_usb_t* usb,
                       const void* buf, size_t len, uint32_t timeout)
{
    _vn_usb_real_t* usb_real;
    int rv = _VN_ERR_OK;
    int wlen = 0;
    _VN_time_t starttime = _vn_get_timestamp();
    _VN_time_t endtime = starttime + timeout;

    assert(usb);
    assert(usb->mode == _VN_USB_MODE_REAL);

    usb_real = &usb->usb.ureal;

    if (usb_real->fd < 0) {
        return _VN_ERR_CLOSED;
    }

    /* Lock around write, there may be multiple write threads  */
    _vn_mutex_lock(&usb->mutex);
    _VN_TRACE(TRACE_FINEST, _VN_SG_USB, "USB writing %d bytes\n", len);
    while (wlen < len) {
        rv = write(usb_real->fd, buf + wlen, len - wlen);
        if (rv >= 0) {
            wlen += rv;
        } else {
            _vn_errno_t err = _vn_sys_get_errno();
            if (err != EAGAIN || _vn_get_timestamp() >= endtime) {
                _vn_errstr_t errstr = _vn_sys_get_errstr(err);
                
                _VN_TRACE(TRACE_ERROR, _VN_SG_USB,
                          "USB write failed: %s\n", errstr);
                _vn_sys_free_errstr(errstr);        
                rv = _VN_ERR_USB;
                break;
            }
        }
    }

    _vn_mutex_unlock(&usb->mutex);

    return rv;
}

int _vn_check_usb_status_real(_vn_usb_t* usb)
{
    /* Checks USB status */
    _vn_usb_real_t* usb_real;
    assert(usb);
    assert(usb->mode == _VN_USB_MODE_REAL);

    usb_real = &usb->usb.ureal;

    _vn_mutex_lock(&usb->mutex);
    if (usb_real->fd < 0) {
        usb->state = _VN_USB_STATE_CLOSED;
    } else {
        usb->state = _VN_USB_STATE_READY;
    }
    _vn_mutex_unlock(&usb->mutex);
    return _VN_ERR_OK;
}

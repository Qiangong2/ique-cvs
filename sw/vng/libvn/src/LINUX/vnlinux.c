//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnlinux.h"
#include "vn.h"
#include "../vnlist.h"
#include "../vndebug.h"
#include "../vntimer.h"

int _vn_close_socket(_vn_socket_t sockfd)
{
    shutdown(sockfd, SHUT_RDWR);
    return close(sockfd);
}

/* Convert system error to VN error code */
int _vn_sys_convert_code(int sys_code)
{
    /* Converts system error code into VN error code */
    switch (sys_code) {
    case 0:
        return _VN_ERR_OK;
    case ENOMEM:
        return _VN_ERR_NOMEM;
    case ETIMEDOUT:
        return _VN_ERR_TIMEOUT;
    default:
        return _VN_ERR_FAIL;
    }
}

/* Returns random number */
uint32_t _vn_rand()
{
    return rand();
}

_VN_time_t _vn_get_timestamp()
{
    /* Linux implementation */
    struct timeval tv;
    if (gettimeofday(&tv, NULL) == 0) {
        _VN_time_t time;
        time = ((_VN_time_t) tv.tv_sec)*1000 + tv.tv_usec/1000;
        return time;
    }
    else {
        return _VN_TIMESTAMP_INVALID;
    }
}
/* Mutex functions */

int _vn_mutex_init(_vn_mutex_t* pMutex)
{
    int rv;
    rv = pthread_mutex_init(pMutex, &_vn_mutexattr_default);
    if (rv == 0) {
        return _VN_ERR_OK;
    }
    else {
        /* Not really needed - pthread_mutex_init always returns 0 */
        _VN_TRACE(TRACE_ERROR, _VN_SG_MISC,
                  "Error initializing mutex: %s", _vn_sys_get_errstr(rv));
        return _vn_sys_convert_code(rv);
    }
}

/* Condition functions */

int _vn_cond_init(_vn_cond_t* pCond)
{
    int rv;
    rv = pthread_cond_init(pCond, NULL);
    if (rv == 0) {
        return _VN_ERR_OK;
    }
    else {
        _VN_TRACE(TRACE_ERROR, _VN_SG_MISC,
                  "Error initializing condition variable: %s",
                  _vn_sys_get_errstr(rv));
        return _vn_sys_convert_code(rv);
    }
}

/* time is absolute time in ms */
int _vn_cond_abstimedwait(_vn_cond_t *cptr, _vn_mutex_t *mptr, uint64_t time)
{
    int rv;
    assert(cptr);
    assert(mptr);
    if (time == _VN_TIMEOUT_NONE) {
        rv = pthread_cond_wait(cptr, mptr);
    }
    else {
        struct timespec ts;
        ts.tv_sec = time / 1000;
        ts.tv_nsec = ( time % 1000 ) * 1000000;  
        rv = pthread_cond_timedwait(cptr, mptr, &ts);
    }
    return _vn_sys_convert_code(rv);
}

/* time is relative time in ms */
int _vn_cond_timedwait(_vn_cond_t *cptr, _vn_mutex_t *mptr, uint32_t time)
{
    assert(cptr);
    assert(mptr);
    if (time == _VN_TIMEOUT_NONE) {
        int rv = pthread_cond_wait(cptr, mptr);
        return _vn_sys_convert_code(rv);
    }
    else {
        struct timespec ts;
        struct timeval tv;
        uint32_t usec;
        if (gettimeofday(&tv, NULL) == 0) {
            int rv;
            usec = (time % 1000)*1000 + tv.tv_usec;
            ts.tv_sec = tv.tv_sec + (time/1000) + (usec/1000000);
            ts.tv_nsec = (usec % 1000000)*1000;
            rv = pthread_cond_timedwait(cptr, mptr, &ts);
            return _vn_sys_convert_code(rv);
        }
        else {
            /* Can't get time!! */
            return _VN_ERR_TIME;
        }
    }
}

/* Wrapper functions for threads */
int _vn_thread_create(_vn_thread_t* thread, _vn_thread_attr_t* attr,
                      void * (*start_routine)(void *), void * arg)
{
    int rv;
    rv = pthread_create(thread, attr, start_routine, arg);
    if (rv == 0) {
        return _VN_ERR_OK;
    }
    else {
        _VN_TRACE(TRACE_ERROR, _VN_SG_MISC,
                  "Error creating thread: %s", _vn_sys_get_errstr(rv));
        return _vn_sys_convert_code(rv);
    }
}

int _vn_thread_join (_vn_thread_t     thread,
                     void **          thread_return)
{
    int rv;
    rv = pthread_join(thread, thread_return);
    if (rv == 0) {
        return _VN_ERR_OK;
    }
    else {
        _VN_TRACE(TRACE_ERROR, _VN_SG_MISC,
                  "Error joining thread: %s", _vn_sys_get_errstr(rv));
        return _vn_sys_convert_code(rv);
    }
}

/* Linux specific part for timers */

int _vn_timer_done = 1;
_vn_thread_t _vn_timer_thread;
uint32_t _vn_timer_msecs = 0;

int _vn_sys_timer_init()
{
    sigset_t timer_sigset;
    
    /* Block SIGALRM signals used for timer (do before any thread started?) */
    sigemptyset(&timer_sigset);
    sigaddset(&timer_sigset, SIGALRM);
    pthread_sigmask(SIG_BLOCK, &timer_sigset, 0);
    return _VN_ERR_OK;
}

int _vn_sys_timer_prepare(uint32_t msecs)
{
    struct itimerval timer;

    /* Configure the timer to expire after every time interval ... */
    timer.it_value.tv_sec = msecs/1000;
    timer.it_value.tv_usec = (msecs % 1000)*1000;
    timer.it_interval.tv_sec = msecs/1000;
    timer.it_interval.tv_usec = (msecs % 1000)*1000;

    setitimer (ITIMER_REAL, &timer, NULL);
    return _VN_ERR_OK;
}

void _vn_sys_timer_run(void* data)
{
    int sig, err;
    uint32_t msecs;
    sigset_t timer_sigset;
    
    /* Set signals to wait for */
    sigemptyset(&timer_sigset);
    sigaddset(&timer_sigset, SIGALRM);

    /* Set timer for every msecs */
    /* Do this in the timer thread so we will get the SIGALRM signal
       (makes RedHat 8 happy) */
    msecs = (uint32_t) data;
    err = _vn_sys_timer_prepare(msecs);

    /* TODO: use synchronized object to signal thread should finish */
    while (!_vn_timer_done) {
        err = sigwait(&timer_sigset, &sig);
        if (err) {
            /* TODO: handle error */
        }
        else {
            /* Call timer */
            /* NOTE: this function cannot be called from a signal handler 
               since it uses pthread functions (e.g. lock/unlock) */
            _vn_timer_handler_locked();
        }
    }
}

int _vn_sys_timer_start(uint32_t msecs)
{
    int rv;

    /* Start timer thread */
    _vn_timer_done = 0;
    _vn_timer_msecs = msecs;
    rv = _vn_thread_create(&_vn_timer_thread, NULL, 
                           (void*) &_vn_sys_timer_run, 
                           (void*) _vn_timer_msecs);
    if (rv != 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TIMER,
                  "Unable to start timer: %d\n", rv);
    }

    return rv;
}

int _vn_sys_timer_stop()
{
    /* TODO: Should we wait until all timers have been triggered? */
    _vn_timer_done = 1;
    _vn_thread_join(_vn_timer_thread, NULL);
    _VN_TRACE(TRACE_FINE, _VN_SG_TIMER, "Timer thread stopped\n");
    return 0;
}

/* System (i.e. LINUX) initialization and cleanup */
pthread_mutexattr_t _vn_mutexattr_default;

int _vn_sys_init()
{
    pthread_mutexattr_init(&_vn_mutexattr_default);
    pthread_mutexattr_settype(&_vn_mutexattr_default, 
                              PTHREAD_MUTEX_RECURSIVE_NP);
    
    /* Ignore SIGPIPE */
    signal(SIGPIPE,SIG_IGN);

    /* Initialize random number generator with seed */
    srand(_vn_get_timestamp());

    /* Do timer initialization */
    _vn_sys_timer_init();
    return _VN_ERR_OK;
}

void _vn_sys_cleanup()
{
    /* Nothing to do */
}


//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnlocal.h"
#include "vnlist.h"

extern uint32_t _vn_retx_max_pkts;

/* Queue Manager */
static _vn_dlist_t _vn_qm_queue;
static _vn_dlist_t _vn_qm_retx_queue;

/* Maximum number of packets that we can queue up */
#ifdef _SC
static uint32_t _vn_qm_max_pkts = 128;
#else
static uint32_t _vn_qm_max_pkts = 50000;
#endif

/* Maximum number of packets to buffer per port (per hostid/netid) */
static int _vn_qm_done = 1;

_vn_send_stats_t _vn_send_stats;

/* Used as temporary buffer for storing encrypted packet to be sent */
static _vn_wbuf_t* _vn_qm_send_buf = NULL;

#if _VN_USE_SERVICE_THREAD
static int _vn_qm_svc_msgs = 0;
#define _VN_QM_LOCK()      
#define _VN_QM_UNLOCK()    
#else
static _vn_mutex_t _vn_qm_mutex; 
static _vn_cond_t  _vn_qm_notempty;
#define _VN_QM_LOCK()      _vn_mutex_lock(&_vn_qm_mutex)
#define _VN_QM_UNLOCK()    _vn_mutex_unlock(&_vn_qm_mutex)
#endif


#define _VN_QM_INC_QUEUED(send_info, attr)           \
{                                                    \
    if (send_info) {                                 \
        if ((attr) & _VN_MSG_RELIABLE) {             \
            send_info->rqueued++;                    \
        } else {                                     \
            send_info->lqueued++;                    \
        }                                            \
    }                                                \
}

#define _VN_QM_DEC_QUEUED(send_info, attr)           \
{                                                    \
    if (send_info) {                                 \
        if ((attr) & _VN_MSG_RELIABLE) {             \
            send_info->rqueued--;                    \
        } else {                                     \
            send_info->lqueued--;                    \
        }                                            \
    }                                                \
}

/* Function prototypes */
void _vn_qm_move_ack(_vn_msg_t* ack_msg, _vn_msg_t* msg);
_vn_msg_t* _vn_qm_deq_ack_only_msg(_VN_net_t net_id,
                                   _VN_host_t from, _VN_host_t to);

_vn_send_stats_t* _vn_get_send_stats()
{
    return &_vn_send_stats;
}

/* NOTE: This function should be called while holding _vn_qm_mutex */
uint32_t _vn_qm_get_buffered_count()
{
    return _vn_dlist_size(&_vn_qm_queue) + 
        _vn_dlist_size(&_vn_qm_retx_queue);
}

/* Formats and prepares messages in VN format and sends it to the queue
   manager for processing */
int _vn_qm_send(int32_t call_id, _VN_net_t net_id,
                _VN_host_t from, _VN_host_t to,
                _VN_port_t port, const void* msg, _VN_msg_len_t len, 
                const void* opthdr, uint8_t hdr_len, uint32_t attr,
                bool unblock /* Do not block this message */)
{
    _vn_msg_t *new_msg = NULL;
    _vn_net_info_t* vn_net = NULL;
    _vn_host_info_t* myhost_info = NULL;
    _vn_host_info_t* host_info = NULL;
    int nhosts = 0;
    _VN_host_t to_id;
    int rv;


    _VN_TRACE(TRACE_FINER, _VN_SG_QM,
              "_vn_qm_send: call %u, net 0x%08x, host %u to %u, "
              "port %u, len %u, hdr_len %u, attr 0x%08x, unblock %u\n",
              call_id, net_id, from, to, port, len, hdr_len, attr, unblock);
                             
    if (net_id < _VN_NET_MAX_RESERVED) {
        if (net_id != _VN_NET_DEFAULT) {
            return _VN_ERR_NETID;
        }
    }
    
    if (to > _VN_HOST_MAX) {
        if ((to != _VN_HOST_OTHERS) && (to != _VN_HOST_SELF)
            && (to != _VN_HOST_ANY)) {
            return _VN_ERR_HOSTID;
        }
    }

    if (hdr_len > _VN_MAX_HDR_LEN) {
        return _VN_ERR_LENGTH;
    }

    if ((hdr_len > 0) && (opthdr == NULL)) {
        return _VN_ERR_INVALID;
    }

    if (len > _VN_MAX_MSG_LEN) {
        return _VN_ERR_LENGTH;
    }

    if ((len > 0) && (msg == NULL)) {
        return _VN_ERR_INVALID;
    }

    vn_net = _vn_lookup_net_info(net_id);
    if (vn_net == NULL) {
        /* Can't find this net */
        return _VN_ERR_NETID;
    }

    /* Make sure this is not a net that I have marked for deletion */
    if (_VN_NET_IS_DONE(vn_net)) {
        return _VN_ERR_NETID;
    }

    /* Make sure the from id is valid */
    if (from == _VN_HOST_SELF) {
        myhost_info = _vn_get_first_localhost(vn_net);
        if (myhost_info != NULL) {
            from = myhost_info->host_id;
        }
        else {
            return _VN_ERR_HOSTID;
        }
    }
    else {
        myhost_info = _vn_lookup_local_host_info(vn_net, from);
        if ((myhost_info == NULL) || !_VN_HOST_IS_LOCAL(myhost_info)
            || _VN_HOST_IS_LEAVING(myhost_info)) {
            return _VN_ERR_HOSTID;
        }
    }

    if ((to == _VN_HOST_OTHERS) || (to == _VN_HOST_ANY)) {
        _vn_dnode_t* node;
        /* Send to our peers */
        if (vn_net->peer_hosts) {
            _vn_dlist_for(vn_net->peer_hosts, node) {
                host_info = (_vn_host_info_t*) _vn_dnode_value(node);
                /* Make sure we are not sending to ourselves (needed?)  */
                /* Only send to host if we are not blocking send to it */
                if (host_info && (host_info->host_id != from)) {

                    /* Skip this host if it's marked as nosend */
                    if (_VN_HOST_IS_NOSEND(host_info) && !unblock) {
                        continue;
                    }

                    /* Prepare message */
                    new_msg = _vn_create_msg(call_id, vn_net->key,
                                             vn_net->net_id, from,
                                             host_info->host_id,
                                             port, msg, len, opthdr, hdr_len, attr);
                    if (new_msg == NULL) {
                        return _VN_ERR_NOMEM;
                    }
                    
                    /* TODO: Is there more stuff we need to send? */
                    rv = _vn_enqueue(new_msg, false);
                    if (rv < 0)
                    {
                        /* Can't enqueue */
                        /* Free msg */
                        _vn_free_msg(new_msg, true);
                        return rv;
                    }
                    _vn_net_add_pending(vn_net);
                    _vn_host_add_pending_from(vn_net, myhost_info);
                    _vn_host_add_pending_to(vn_net, host_info);
                    nhosts++;
                }
            }
        }
        
        /* If sending to all hosts, set host_id to send to ourselves next 
         * otherwise, done */
        to_id = (to == _VN_HOST_ANY)? from: _VN_HOST_INVALID;
        if (to_id == from) {
            host_info = myhost_info;
        }
    }
    else {
        if (to == _VN_HOST_SELF) {
            host_info = myhost_info;
            to_id = from;
        }
        else if (to == from) {
            host_info = myhost_info;
            to_id = to;
        }
        else {
            /* Verify that this host is part of this virtual network */
            host_info = _vn_lookup_host_info(vn_net, to);
            if (host_info == NULL) {
                /* Can't find this host */
                return _VN_ERR_HOSTID;
            }
            to_id = to;        
        }
    }

    assert(host_info);

    if (!unblock && _VN_HOST_IS_NOSEND(host_info)) {
        /* Not allowed to send to this host yet */
        return _VN_ERR_HOSTID;
    }

    /* Send to host directly */
    if (to_id != _VN_HOST_INVALID) {
        /* Prepare message */
        new_msg = _vn_create_msg(call_id, vn_net->key, vn_net->net_id,
                                 from, to_id, 
                                 port, msg, len, opthdr, hdr_len, attr);
        if (new_msg == NULL) {
            return _VN_ERR_NOMEM;
        }

        rv = _vn_enqueue(new_msg, false);
        if (rv < 0)
        {
            /* Can't enqueue */
            /* Free msg */
            _vn_free_msg(new_msg, true);
            return rv;
        }
        _vn_net_add_pending(vn_net);
        _vn_host_add_pending_from(vn_net, myhost_info);
        _vn_host_add_pending_to(vn_net, host_info);
        nhosts++;
    }
    _VN_TRACE(TRACE_FINER, _VN_SG_QM,
              "_vn_qm_send: sent to net 0x%08x, host %u to %u, "
              "%d hosts\n", net_id, from, to, nhosts);
    return _VN_ERR_OK;
}

/* For now, implement simple FIFO scheme */
int _vn_enqueue(_vn_msg_t* msg, bool retx)
{
    int rv;
    _vn_msg_t* ack_msg;
    _vn_send_info_t* send_info;
    assert(msg);
    assert(msg->pkt);
    if (retx)
        msg->flags |= _VN_MSG_FLAGS_RETX;
    else msg->flags &= ~_VN_MSG_FLAGS_RETX;

    _VN_TRACE(TRACE_FINER, _VN_SG_QM,
              "VNQM: Enqueuing message to send to net 0x%08x, "
              "from host %u to %u, port %u, %u bytes, flags 0x%08x (QSize = %u)\n",
              msg->net_id, msg->from, msg->to, msg->port,
              msg->pkt->len, msg->flags, _vn_qm_get_buffered_count());

    rv = _vn_lookup_create_send_info(&send_info,
                                     msg->net_id, msg->from, 
                                     msg->to, msg->port);

    if (rv < 0) {
        return rv;
    }

    assert(send_info);
    if (msg->attr & _VN_MSG_RELIABLE) {
        int nretx = 0;
        if (send_info->retx && send_info->retx->retx_msgs) {
            nretx = _vn_dlist_size(send_info->retx->retx_msgs);
        }

        if (send_info->rqueued + nretx >= _VN_QM_MAX_RELIABLE_PER_PORT) {
            return _VN_ERR_FULL;
        } else if (send_info->lqueued >= _VN_QM_MAX_LOSSY_PER_PORT) {
            return _VN_ERR_FULL;
        } else if (send_info->rqueued + nretx + send_info->lqueued
                   >= _VN_QM_MAX_PKTS_PER_PORT) {
            return _VN_ERR_FULL;
        }
    }

    _VN_QM_LOCK();
    ack_msg = _vn_qm_deq_ack_only_msg(msg->net_id, msg->from, msg->to);
    if (ack_msg) {
        _vn_qm_move_ack(ack_msg, msg);
        _vn_free_msg(ack_msg, true);
    }
    /* Make sure we have room for all the buffered retransmitted packets 
       plus one more retx to come back onto the queue */
    if (_vn_qm_get_buffered_count() + _vn_retx_get_buffered_count()
        + (retx? 0:1) < _vn_qm_max_pkts) 
    {
        if (_vn_qm_get_buffered_count() == 0) {
#if _VN_USE_SERVICE_THREAD
            if (_vn_qm_svc_msgs < 2) {
                _vn_service_signal(_VN_QM_MSG);
                _vn_qm_svc_msgs++;
            }
#else
            _vn_cond_signal(&_vn_qm_notempty);
#endif
        }
        /* TODO: make sure these messages are in order */
        if (retx) {
            /* Enqueue retransmission in separate queue, since they
               were suppose to have been sent already */
            _vn_qm_update_ack(msg->pkt, true, 0, 0 ,0);            
            rv = _vn_dlist_enq_back(&_vn_qm_retx_queue, msg);
        }
        else {
            /* Regular packets go in the back of _vn_qm_queue */
            rv = _vn_dlist_enq_back(&_vn_qm_queue, msg);
        }
    }
    else {
        assert(!ack_msg);
        /* Oops, queue full */
        rv = _VN_ERR_FULL;
    }
    _VN_QM_UNLOCK();

    _VN_QM_INC_QUEUED(send_info, msg->attr);

    return rv;
}

_vn_msg_t* _vn_dequeue()
{
    _vn_msg_t* msg;
    _VN_QM_LOCK();
    if (!_vn_dlist_empty(&_vn_qm_retx_queue)) {
        msg = (_vn_msg_t*) _vn_dlist_deq_front(&_vn_qm_retx_queue);
    }
    else {
        msg = (_vn_msg_t*) _vn_dlist_deq_front(&_vn_qm_queue);
    }
#if _VN_USE_SERVICE_THREAD
    if (msg == NULL) {
        _vn_qm_svc_msgs--;
    }
#endif
    _VN_QM_UNLOCK();
    return msg;
}

/* Finds and dequeues ack only message for the given net, from and to hosts */
_vn_msg_t* _vn_qm_deq_ack_only_msg(_VN_net_t net_id,
                                   _VN_host_t from, _VN_host_t to)
{
    _vn_dnode_t* node, *temp;    
    /* Don't need to bother with retx queue, only look in normal queue */
    _vn_dlist_delete_for(&_vn_qm_queue, node, temp)
    {
        _vn_msg_t* msg = (_vn_msg_t*) _vn_dnode_value(node);
        if (msg && msg->flags & _VN_MSG_FLAGS_ACK_ONLY) {
            if ((msg->net_id == net_id) && (msg->from == from) &&
                (msg->to == to))
            {        
                _vn_send_info_t *send_info;
                _vn_dlist_delete(&_vn_qm_queue, node);

                send_info = _vn_lookup_send_info(net_id, from, to, msg->port);
                _VN_QM_DEC_QUEUED(send_info, msg->attr);

                return msg;
            }
        }
    }
    return NULL;
}

/*
 *  Move ack to another message 
 *  If msg is NULL, then tries finding another message in the QM
 *  to piggyback on and re-enqueues the ack.  
 *  Otherwise, moves the ack to msg.
 */
void _vn_qm_move_ack(_vn_msg_t* ack_msg, _vn_msg_t* msg)
{
    _vn_buf_t* pkt;
    _vn_msg_header_t* hdr;
    uchar_t* buf;
    _VN_msg_len_t buflen;
    uint8_t  ack_mask;
    uint8_t* pAckPort;
    uint8_t* pAckSeq;

    assert(ack_msg);
    pkt = ack_msg->pkt;

    assert(pkt);
    assert(pkt->buf);
    assert(pkt->len >= _VN_HEADER_SIZE); 

    hdr = (_vn_msg_header_t*) pkt->buf;
    ack_mask = _VN_PKT_OPT_ACK_MASK(hdr);
    assert(pkt->len == _VN_PKT_SIZE(hdr) - _VN_AUTHCODE_SIZE);

    if (ack_mask == 0) {
        /* Nothing to do - no ack */
        return;
    }

    _VN_TRACE(TRACE_FINER, _VN_SG_QM,
              "Moving ack to msg 0x%08x\n", ack_msg, msg);

    /* At this point, len should point to end of ack */
    buf = pkt->buf;
    buflen = pkt->len;
    pAckPort = buf + buflen - 2;
    pAckSeq  = buf + buflen - 1;

    if (msg) {
        assert(ack_msg->net_id == msg->net_id);
        assert(ack_msg->from == msg->from);
        assert(ack_msg->to == msg->to);

        _vn_qm_update_ack(msg->pkt, true, ack_mask, *pAckSeq, *pAckPort);
    }
    else {
        _vn_qm_enqueue_ack(ack_msg->net_id, ack_msg->from, ack_msg->to,
                           *pAckPort, ack_mask, *pAckSeq);
    }
}

int _vn_qm_cancel_retx_msgs_seq_le(_vn_retx_info_t* retx_info,
                                   _VN_net_t net_id, 
                                   _VN_host_t from, _VN_host_t to,
                                   _VN_port_t port, uint8_t seq)
{
    /* Look for message in queue */
    int canceled = 0;
    _vn_dnode_t *node, *temp;
    _vn_send_info_t *send_info;

    send_info = _vn_lookup_send_info(net_id, from, to, port);

    _VN_QM_LOCK();
    _vn_dlist_delete_for(&_vn_qm_retx_queue, node, temp)
    {
        _vn_msg_t* msg = (_vn_msg_t*) _vn_dnode_value(node);
        /* Cancel retx messages only */
        assert(msg);
        assert(msg->flags & _VN_MSG_FLAGS_RETX);
        if ((msg->net_id == net_id) && (msg->from == from) && (msg->to == to))
        {
            uint8_t msgSeq = msg->seq;                
            /* Cancel all retx messages with msgSeq <= seq? */
            if ((msg->port == port) &&                     
                !_vn_seqno_larger_than(msgSeq, seq)) {
                _vn_dlist_delete(&_vn_qm_retx_queue, node);
                _VN_QM_DEC_QUEUED(send_info, msg->attr);
                _vn_qm_move_ack(msg, NULL);
                _vn_retx_free_msg(msg, retx_info);
                canceled++;
            }
        }
    }
    _VN_QM_UNLOCK();

    _VN_TRACE(TRACE_FINER, _VN_SG_QM,
              "_vn_qm_cancel_retx_msgs_seq_le: cancelled %u msgs,"
              " for net 0x%08x, from host %u to %u, port %u, seqno <= %u\n",
              canceled, net_id, from, to, port, seq);
        
    return canceled;
}

_vn_msg_t* _vn_qm_find_msg_for_ack_from(_vn_dlist_t* queue, _VN_net_t net_id, 
                                        _VN_host_t from, _VN_host_t to,
                                        _VN_port_t ack_port)
{
    /* Finds packet to be sent next to net_id, host_id */
    _vn_dnode_t* node;
    assert(queue);
    _vn_dlist_for(queue, node)
    {
        _vn_msg_t* msg = (_vn_msg_t*) _vn_dnode_value(node);
        if (msg) {
            if ((msg->net_id == net_id) && (msg->from == from) &&
                (msg->to == to))
            {
                uchar_t* buf;
                _vn_msg_header_t* hdr;
                _VN_msg_len_t buflen;
                uint8_t ackMask, ackPort;

                assert(msg->pkt);
                assert(msg->pkt->buf);
                assert(msg->pkt->len >= _VN_HEADER_SIZE);

                hdr = (_vn_msg_header_t*) (msg->pkt->buf);

                buf = msg->pkt->buf;
                buflen = msg->pkt->len;
                
                ackMask = _VN_PKT_OPT_ACK_MASK(hdr);
                if (ackMask == 0) {
                    return msg;
                }
                
                /* The authcode has not been added, so the ack should be
                   the last two bytes of the current packet */
                assert(msg->pkt->len == _VN_PKT_SIZE(hdr) - _VN_AUTHCODE_SIZE);
                ackPort = buf[buflen - 2];

                if (ackPort == ack_port) {
                    return msg;
                }
            }
        }
    }        
    return NULL;
}

/* NOTE: This function should be called while holding _vn_qm_mutex */
_vn_msg_t* _vn_qm_find_msg_for_ack(_VN_net_t net_id, 
                                   _VN_host_t from, _VN_host_t to,
                                   _VN_port_t ack_port)
{
    _vn_msg_t* msg = NULL;
    /* Try retransmission queue first */
    msg = _vn_qm_find_msg_for_ack_from(&_vn_qm_retx_queue, net_id,
                                       from, to, ack_port);

    if (msg == NULL) {
        /* Try normal queue */
        msg = _vn_qm_find_msg_for_ack_from(&_vn_qm_queue, net_id, 
                                           from, to, ack_port);
    }
    return msg;
}

void _vn_qm_update_ack(_vn_buf_t* pkt, bool force,
                       uint8_t ack_mask, uint8_t ack_seq, _VN_port_t ack_port)
{
    _vn_msg_header_t* hdr;
    uchar_t* buf;
    _VN_msg_len_t buflen;
    uint8_t  old_ack_mask;
    uint8_t* pAckPort;
    uint8_t* pAckSeq;

    assert(pkt);
    assert(pkt->buf);
    assert(pkt->len >= _VN_HEADER_SIZE); 

    hdr = (_vn_msg_header_t*) pkt->buf;
    old_ack_mask = _VN_PKT_OPT_ACK_MASK(hdr);
    assert(pkt->len == _VN_PKT_SIZE(hdr) - _VN_AUTHCODE_SIZE);

    if (old_ack_mask == 0) {
        if (ack_mask) {
            /* Make room for ack field */
            assert(pkt->len + 2 <= pkt->max_len);
            hdr->opt_size += 2;
            pkt->len += 2;
        }
        else {
            /* Nothing to do - no ack */
            return;
        }
    }

    /* At this point, len should point to end of ack */
    buf = pkt->buf;
    buflen = pkt->len;
    pAckPort = buf + buflen - 2;
    pAckSeq  = buf + buflen - 1;

    if ((force) || (old_ack_mask == 0) ||
        _vn_seqno_larger_than(ack_seq, *pAckSeq)) 
    {
        /* Update ack_info  */
        if (old_ack_mask != ack_mask) {
            hdr->opt_field_mask &= ~_VN_OPT_ACK;
            hdr->opt_field_mask |= ack_mask;
            if (ack_mask == 0) {
                /* Make sure ack field size is not counted */
                hdr->opt_size -= 2;
                pkt->len -= 2;
            }
        }
        *pAckSeq = ack_seq;
        *pAckPort = ack_port;
    }
}

void _vn_qm_enqueue_ack(_VN_net_t net_id, _VN_host_t from, _VN_host_t to,
                        _VN_port_t port, uint8_t ack_mask, uint8_t ack_seq)
{
    _vn_msg_t* msg;
    _VN_QM_LOCK();
    msg = _vn_qm_find_msg_for_ack(net_id, from, to, port);
    if (msg == NULL) {
        int attr = 0, rv;
        /* Need net info to lookup key for this net */
        _vn_net_info_t* net_info;

        _VN_QM_UNLOCK();
        
        net_info = _vn_lookup_net_info(net_id);
        assert(net_info);
        assert(_vn_islocalhost(net_info, from));

        msg = _vn_create_empty_msg(net_info->key, net_id,
                                   from, to, _VN_PORT_NET_CTRL, attr);

        if (msg == NULL) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_QM, "_vn_qm_enqueue_ack: "
                      "Error creating ack only message\n");
            return;
        }

        msg->flags |= _VN_MSG_FLAGS_ACK_ONLY;
        _vn_qm_update_ack(msg->pkt, false, ack_mask, ack_seq, port);
        rv = _vn_enqueue(msg, false);
        if (rv < 0) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_QM, "_vn_qm_enqueue_ack: "
                      "Error %d enqueuing ack only message\n", rv);
            /* Free pkt (not enqueued) */
            _vn_free_msg(msg, true);
            return;
        }
    }
    else {
        _vn_qm_update_ack(msg->pkt, false, ack_mask, ack_seq, port);
        _VN_QM_UNLOCK();
    }
}

void _vn_qm_enqueue_keep_alive(_VN_net_t net_id, _VN_host_t to)
{
    _vn_msg_t* msg;
    int attr = 0, rv;
    /* Need net info to lookup key and my host id for this net */
    _vn_net_info_t* net_info = _vn_lookup_net_info(net_id);
    _vn_host_info_t* localhost;
    _VN_host_t from;
    assert(net_info);

    localhost = _vn_get_first_localhost(net_info);
    assert(localhost);
    
    from = localhost->host_id;

    _VN_TRACE(TRACE_FINER, _VN_SG_QM, "_vn_qm_enqueue_keep_alive: "
             "Enqueuing keep alive for net 0x%08x, from host %u to %u\n",
             net_id, from, to);

    msg = _vn_create_empty_msg(net_info->key, net_id,
                               from, to, _VN_PORT_NET_CTRL, attr);
    if (msg == NULL) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_QM, "_vn_qm_enqueue_keep_alive: "
                 "Error creating keep alive message\n");
        return;
    }

    msg->flags |= _VN_MSG_FLAGS_KA;        
    rv = _vn_enqueue(msg, false);
    if (rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_QM, "_vn_qm_enqueue_keep_alive: "
                 "Error %d enqueuing keep alive message\n", rv);
        /* Free pkt (not enqueued) */
        _vn_free_msg(msg, true);
        return;
    }
}

extern int _vn_netif_send_vn_pkt_nocopy(_vn_netif_t *netif,
                                        _VN_net_t net_id, _VN_host_t host_id,
                                        _vn_wbuf_t *wbuf);
/**
 * msg - Original message to send
 * enc_pkt - Optionally hashed and encrypted packet
 */
int _vn_qm_send_msg(_vn_msg_t* msg, _vn_wbuf_t* enc_pkt)
{
    int rv;

    assert(msg);
    assert(msg->pkt);
    assert(msg->pkt->buf);
    assert(msg->pkt->len);
    assert(enc_pkt);
    assert(enc_pkt->buf);
    assert(enc_pkt->len);

    _VN_TRACE(TRACE_FINER, _VN_SG_QM, "_vn_qm_send_msg: sending %d bytes "
              "for net 0x%08x, from host %u to %u, port %u, seq %u, "
              "attr 0x%08x, flags 0x%08x\n",
              msg->pkt->len, msg->net_id, msg->from, msg->to,
              msg->port, msg->seq, msg->attr, msg->flags);

#ifdef _VN_RPC_DEVICE
/*#if 0 */
    /* On SC, reuse buffer (don't copy to USB) */
    /* On SC, _vn_netif_send_vn_pkt will be SC RPC call */
    rv = _vn_netif_send_vn_pkt_nocopy(_vn_netif_get_instance(),
                                      msg->net_id, msg->to, enc_pkt);
#else
    rv = _vn_netif_send_vn_pkt(_vn_netif_get_instance(),
                               msg->net_id, msg->to,
                               enc_pkt->buf + enc_pkt->offset, enc_pkt->len);
#endif

    _VN_TRACE(TRACE_FINER, _VN_SG_QM,
              "_vn_qm_send_msg: send returned %d\n", rv);

    return rv;
}

void _vn_qm_drop_msg(_vn_msg_t* msg, int errcode, 
                     _vn_send_info_t* send_info)
{
    /* Post event that we had to drop this packet */
    assert(msg);
    if (send_info && send_info->retx) {
        /* Clear any reconnecting flag */
        if (msg->flags & _VN_MSG_FLAGS_SYN) {
            send_info->retx->flags &= ~_VN_PROP_PORT_RESYNCING;
        }
    }
    _vn_post_msg_err_event(msg, errcode);
    _vn_free_msg(msg, true);
}

/* The main queue manager */
int _vn_qm_proc_msg(_vn_msg_t* msg, _vn_send_stats_t* stats)
{
    _vn_host_info_t* host_info;
    _vn_host_info_t* myhost_info;
    _vn_net_info_t* net_info;
    _vn_channel_t* channel;
    _VN_port_t port;
    bool update_pending;

    assert(msg != NULL);
    assert(msg->pkt);
    assert(msg->pkt->buf);
    assert(msg->pkt->len >= _VN_HEADER_SIZE);

    update_pending = (msg->flags == 0);

    if (stats) stats->total_pkts++;
    if (stats) stats->total_bytes += (msg->pkt->len);
    port = msg->port;
    _VN_TRACE(TRACE_FINER, _VN_SG_QM,
              "_vn_qm_proc_msg: attempting to send message for "
              "net 0x%08x, from host %u to %u, port %u, flags 0x%08x\n", 
              msg->net_id, msg->from, msg->to, port, msg->flags);
    
    net_info = _vn_lookup_net_info(msg->net_id);
    if (net_info == NULL) {
        _VN_TRACE(TRACE_FINER, _VN_SG_QM,
                  "_vn_qm_proc_msg: cannot find mapping for "
                  "net 0x%08x\n", msg->net_id);
        /* Dropping packet - bad net */
        if (stats) stats->dropped_pkts++;
        if (stats) stats->unknown_net++;
        _vn_qm_drop_msg(msg, _VN_ERR_NETID, NULL);
        return _VN_ERR_NETID;
    }

    myhost_info = _vn_lookup_host_info(net_info, msg->from);
    host_info = _vn_lookup_host_info(net_info, msg->to);
    if (host_info && myhost_info) {
        _vn_msg_header_t* hdr;
        _vn_send_info_t* send_info;
        int rv = _VN_ERR_OK;
        uint8_t ack_mask;

        assert(_VN_HOST_IS_LOCAL(myhost_info));
        
#if 0
        if (_VN_HOST_IS_NOSEND(host_info)) {
            /* Oops, we are actually blocking sending to this host */
            _VN_TRACE(TRACE_WARN, _VN_SG_QM,
                      "_vn_qm_proc_msg: sending to host %u "
                      "net 0x%08x is blocked\n", msg->to, msg->net_id);
            /* Dropping packet - blocking packets to host */
            if (stats) stats->dropped_pkts++;
            if (stats) stats->blocked++;
            if (update_pending) {
                _vn_host_del_pending_from(net_info, myhost_info);
                _vn_host_del_pending_to(net_info, host_info);
                _vn_net_del_pending(net_info);
            }            
            _vn_qm_drop_msg(msg, _VN_ERR_BLOCKED, NULL);
            return _VN_ERR_BLOCKED;
        }
#endif

        channel = _vn_find_channel(host_info, msg->from);
        if (channel == NULL) {
            channel = _vn_channel_create(msg->from);
            if (channel) {
                rv = _vn_add_channel(host_info, channel);
            }
            else {
                rv = _VN_ERR_NOMEM;
            }
            if (rv < 0) {
                _VN_TRACE(TRACE_ERROR, _VN_SG_QM,
                          "_vn_qm_proc_msg: Error %d creating channel\n", rv);
                if (channel) {
                    _vn_channel_destroy(channel);
                }

                /* Dropping packet - probably no memory */
                if (stats) stats->dropped_pkts++;
                if (rv == _VN_ERR_NOMEM) {
                    if (stats) stats->nomem++;
                }
                if (update_pending) {
                    _vn_host_del_pending_from(net_info, myhost_info);
                    _vn_host_del_pending_to(net_info, host_info);
                    _vn_net_del_pending(net_info);
                }
                _vn_qm_drop_msg(msg, rv, NULL);
                return rv;
            }
        }

        send_info = _vn_find_send_info(channel, port);
        if (send_info == NULL) {
            send_info = _vn_send_info_create(port, _VN_PRIORITY_DEFAULT );
            if (send_info) {
                rv = _vn_add_send_info(channel, send_info);
            }
            else {
                rv = _VN_ERR_NOMEM;
            }
            if (rv < 0) {
                _VN_TRACE(TRACE_ERROR, _VN_SG_QM,
                          "_vn_qm_proc_msg: Error %d creating send info\n", rv);

                if (send_info) {
                    _vn_send_info_destroy(send_info);
                }

                /* Dropping packet - probably no memory */
                if (stats) stats->dropped_pkts++;
                if (rv == _VN_ERR_NOMEM) {
                    if (stats) stats->nomem++;
                }
                if (update_pending) {
                    _vn_host_del_pending_from(net_info, myhost_info);
                    _vn_host_del_pending_to(net_info, host_info);
                    _vn_net_del_pending(net_info);
                }
                _vn_qm_drop_msg(msg, rv, NULL);
                return rv;
            }
        }

        _VN_QM_DEC_QUEUED(send_info, msg->attr);

        /* Update global seq_no */
        hdr = (_vn_msg_header_t*) msg->pkt->buf;
        hdr->gseq = htonl(channel->gseq_s++);

#if 1
        /* For reliable messages to self, don't need to buffer 
           this packet or have it acked since we are sending
           to dispatcher directly */
        if (_VN_HOST_IS_LOCAL(host_info)) {
            /* Clear reliable flags */
            if (msg->attr & _VN_MSG_RELIABLE) {
                msg->attr &= ~_VN_MSG_RELIABLE;
                msg->pkt->buf[_VN_PKT_OPT_MASK_OFFSET] &= ~_VN_OPT_RELIABLE;
            }
        }
#endif

        /* Put in SYN option if needed */
        if ((msg->attr & _VN_MSG_RELIABLE) &&
            _VN_PORT_SYNC_NEEDED(send_info)) {
            msg->flags |= _VN_MSG_FLAGS_SYN;
            msg->pkt->buf[_VN_PKT_OPT_MASK_OFFSET] |= _VN_OPT_SYN;
            send_info->retx->flags |= _VN_PROP_PORT_RESYNCING;
        }

        if (!(msg->flags & _VN_MSG_FLAGS_RETX)) {

            /* Update port seq_no */ 
            msg->seq = (msg->attr & _VN_MSG_RELIABLE)? 
                send_info->rseq_no++: send_info->lseq_no++;
            msg->pkt->buf[_VN_PKT_PSEQ_OFFSET] = msg->seq;
        }
        else {
            if (stats) stats->retx++;
        }

        /* Update sent statistics */
        if (stats) {
            ack_mask = _VN_PKT_OPT_ACK_MASK((_vn_msg_header_t*) msg->pkt->buf);
            if (ack_mask & _VN_OPT_POS_ACK) {
                stats->acks++;
            }
            else if (ack_mask & _VN_OPT_NEG_ACK) {
                stats->nacks++;
            }
            if (msg->flags & _VN_MSG_FLAGS_SYN) {
                stats->syns++;
            }
            if ((msg->pkt->len == 0) && (msg->port == _VN_PORT_NET_CTRL)) {
                stats->zero_ctrl++;
            } 
            if (msg->flags & _VN_MSG_FLAGS_KA) {
                stats->keep_alive++;
            }
            stats->sent++;
        }
        
        /* Copy packet to send buffer, hash and encrypt pkt in send buffer 
           so that the original unencrypted pkt is retained */
        assert(_vn_qm_send_buf);
        assert(_vn_qm_send_buf->max_len >= _vn_qm_send_buf->offset + msg->pkt->len);
        memset(_vn_qm_send_buf->buf, 0, _vn_qm_send_buf->offset);
        memcpy(_vn_qm_send_buf->buf +  _vn_qm_send_buf->offset, 
               msg->pkt->buf, msg->pkt->len);
        _vn_qm_send_buf->len = msg->pkt->len;        
        _vn_hash_encrypt_pkt(_vn_qm_send_buf, net_info->key);

        /* Update the sendtime */
        msg->sendtime = _vn_get_timestamp();
        if (_VN_HOST_IS_LOCAL(host_info)) {
            uint8_t* buf = _vn_qm_send_buf->buf + _vn_qm_send_buf->offset;
            _VN_msg_len_t len = _vn_qm_send_buf->len;

            /* Send to dispatcher directly
             * msg buffer passed directly to dispatcher */

            /* Save message for debugging */
            _vn_dbg_save_buf(_VN_DBG_SEND_PKT, buf, len);
            _vn_dbg_save_buf(_VN_DBG_RECV_PKT, buf, len);

            /* TODO: Avoid copying of  buffer */
            /* Do we care about return value from dispatcher? */
            rv = _vn_dispatcher_recv(buf, len, 0, 0);
            if (stats) stats->self++;
        }
        else {
            /* Send out the network */
            rv = _vn_qm_send_msg(msg, _vn_qm_send_buf);
            /* TODO: Handle error? */
            if (stats) stats->other++;
        }

        if (msg->attr & _VN_MSG_RELIABLE) {
            /* Reliable - send to retransmission manager for buffering */
            if (send_info->retx == NULL) {
                _VN_addr_t toaddr = _VN_make_addr(net_info->net_id, msg->to);
                send_info->retx =
                    _vn_retx_info_create(toaddr, send_info->port);
            }
            if (send_info->retx) {
                rv = _vn_retx_buffer_msg(msg, send_info->retx);
            }
            else {
                rv = _VN_ERR_NOMEM;
            }
            if (rv < 0) {
                /* Can't buffer this packet */
                /* TODO: What really should we do here? */
                /* TODO: if send failed and cannot buffer packet,
                   post event that we had to drop this packet */
                if (stats) stats->noretxbuf++;
                _vn_free_msg(msg, true);
                _VN_TRACE(TRACE_ERROR, _VN_SG_QM,
                          "Error %d buffering packet for retx\n", rv);
            }
            if (stats) stats->rsent++;
        }
        else { 
            /* Non-reliable - can free msg now */
            _vn_free_msg(msg, true);
            if (stats) stats->lsent++;
        }

        if (update_pending) { /* Not a special packet */
            /* TODO: Only decrement pending reliable packet when the memory
                     is actually freed? */
            _vn_host_del_pending_from(net_info, myhost_info);
            _vn_host_del_pending_to(net_info, host_info);
            _vn_net_del_pending(net_info);
        }

        return rv;
    }
    else {        
        if (myhost_info == NULL) {
            _VN_TRACE(TRACE_INFO, _VN_SG_QM,
                      "_vn_qm_proc_msg: cannot find from host %u"
                      " for net 0x%08x\n", msg->from, msg->net_id);
        }
        if (host_info == NULL) {
            _VN_TRACE(TRACE_INFO, _VN_SG_QM,
                      "_vn_qm_proc_msg: cannot find to host %u"
                      " for net 0x%08x\n", msg->to, msg->net_id);
        }
        if (stats) stats->dropped_pkts++;
        if (stats) stats->unknown_host++;
        if (update_pending) {
            if (myhost_info) {
                _vn_host_del_pending_from(net_info, myhost_info);
            }   
            if (host_info) {
                _vn_host_del_pending_to(net_info, host_info);
            }
            _vn_net_del_pending(net_info);
        }
        _vn_qm_drop_msg(msg, _VN_ERR_HOSTID, NULL);
        return _VN_ERR_HOSTID;
    }
}

void _vn_qm_print_stats(FILE* fp)
{
    fprintf(fp, "QM has %d packets\n", _vn_qm_get_buffered_count());
    _vn_print_send_stats(fp, _vn_get_send_stats());
}

/* Processes next message in QM (called by service thread) */
bool _vn_qm_proc_next()
{
    _vn_msg_t* msg;
    bool res;
    _vn_net_lock();
    msg = _vn_dequeue();
    if (msg) {
        _vn_qm_proc_msg(msg, _vn_get_send_stats());
        res = true;
    } else {
        res = false;
    }
    _vn_net_unlock();
    return res;
}

#if !_VN_USE_SERVICE_THREAD
void _vn_qm_run()
{
    int rv;
    int timeout = 1000; /* 1 second timeout */

    _VN_TRACE(TRACE_FINE, _VN_SG_QM, "Starting VN Queue Manager\n");

    while(!_vn_qm_done) {        
        _vn_msg_t* msg;

        _VN_QM_LOCK();
        /* Wait for the queue to be not empty */
        if (_vn_qm_get_buffered_count() == 0) {
            _vn_cond_timedwait(&_vn_qm_notempty, &_vn_qm_mutex, timeout);
        }
        _VN_QM_UNLOCK();

        while ((msg = _vn_dequeue()))
        {
            /* Got a message */
            assert(msg->pkt);
            _VN_TRACE(TRACE_FINER, _VN_SG_QM,
                      "VNQM: Dequeued message to send to "
                      "net 0x%08x, from host %u to %u, %u bytes (QSize = %u)\n",
                      msg->net_id, msg->from, msg->to, msg->pkt->len,
                      _vn_qm_get_buffered_count());

            /* Dump message for debugging */
            if (_VN_TRACE_ON(TRACE_FINEST, _VN_SG_QM))
                _vn_dbg_dump_buf(stdout, msg->pkt->buf, msg->pkt->len);

            _vn_net_lock();
            rv = _vn_qm_proc_msg(msg, _vn_get_send_stats());
#ifndef _VN_RPC_DEVICE
            /* Process any packets buffered to dispatcher */
            _vn_dispatcher_proc_queue();
#endif
            _vn_net_unlock();
            _VN_TRACE(TRACE_FINER, _VN_SG_QM,
                      "VNQM: _vn_qm_proc_msg returns %d\n", rv);
        }
    }
    if (_VN_TRACE_ON(TRACE_INFO, _VN_SG_QM)) {
        _vn_qm_print_stats(stdout);
    }
    _VN_TRACE(TRACE_FINE, _VN_SG_QM,
              "Finished running VN Queue Manager\n");
}
#endif

int _vn_qm_clear_invalid_from_queue(_vn_dlist_t* queue)
{
    _vn_dnode_t* node, *temp;
    int i = 0;
    assert(queue);
    _vn_dlist_delete_for(queue, node, temp)
    {
        _vn_msg_t* msg = (_vn_msg_t*) _vn_dnode_value(node);
        if (msg) {
            _vn_net_info_t* net_info;
            net_info = _vn_lookup_net_info(msg->net_id);
            if (net_info == NULL) {
                _vn_dlist_delete(queue, node);
                i++;
            }
        }
    }
    return i;
}

/* Clears invalid messages (those with a invalid net id) from the queue */
int _vn_qm_clear_invalid()
{
    int i = 0;
    _VN_QM_LOCK();
    i += _vn_qm_clear_invalid_from_queue(&_vn_qm_queue);
    i += _vn_qm_clear_invalid_from_queue(&_vn_qm_retx_queue);
    _VN_QM_UNLOCK();
    _VN_TRACE(TRACE_FINE, _VN_SG_QM, "%d messages cleared from the QM\n", i);
    return i;
}

#if !_VN_USE_SERVICE_THREAD
#ifdef _SC
static uint8_t  _vn_qm_thread_stack[_VN_THREAD_STACKSIZE];
#endif
#endif

_vn_thread_t _vn_qm_thread;

int _vn_qm_init()
{
    int rv;
    
    _vn_dlist_init(&_vn_qm_queue);
    _vn_dlist_init(&_vn_qm_retx_queue);
    memset(&_vn_send_stats, 0, sizeof(_vn_send_stats));

#if !_VN_USE_SERVICE_THREAD
    rv = _vn_mutex_init(&_vn_qm_mutex);
    if (rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_QM, 
                  "Failed to initialize mutex for QM: %d\n", rv);
        return rv;
    }

    rv = _vn_cond_init(&_vn_qm_notempty);
    if (rv < 0) {
        _vn_mutex_destroy(&_vn_qm_mutex);

        _VN_TRACE(TRACE_ERROR, _VN_SG_QM, 
                  "Failed to initialize condition variable for QM: %d\n", rv);
        return rv;
    }
#endif

    /* TODO: Alloc send buf from usb memory - leave room for usb header */
    _vn_qm_send_buf = _vn_get_vn_buffer(_VN_MAX_TOTAL_MSG_LEN);
    if (_vn_qm_send_buf == NULL) {
#if !_VN_USE_SERVICE_THREAD
        _vn_mutex_destroy(&_vn_qm_mutex);
        _vn_cond_destroy(&_vn_qm_notempty);
#endif

        rv = _VN_ERR_NOMEM;        
        _VN_TRACE(TRACE_ERROR, _VN_SG_QM, 
                  "Failed to allocate buffer for QM: %d\n", rv);
        return rv;
    }

    return _VN_ERR_OK;
}

int _vn_qm_reset()
{
    _VN_QM_LOCK();
    _vn_dlist_clear(&_vn_qm_queue, (void*) &_vn_free_msg);
    _vn_dlist_clear(&_vn_qm_retx_queue, (void*) &_vn_free_msg);
    _VN_QM_UNLOCK();
    return _VN_ERR_OK;
}

int _vn_qm_cleanup()
{
    /* Free all resources uses by the QM */
    _vn_qm_reset();
#if !_VN_USE_SERVICE_THREAD
    _vn_mutex_destroy(&_vn_qm_mutex);
    _vn_cond_destroy(&_vn_qm_notempty);
#endif
    if (_vn_qm_send_buf) {
        _vn_free_msg_buffer_aligned(_vn_qm_send_buf);
    }
    return _VN_ERR_OK;
}

int _vn_qm_start()
{
    int rv;
#if !_VN_USE_SERVICE_THREAD
    _vn_thread_attr_t* pAttr = NULL;
#ifdef _SC
    _vn_thread_attr_t thread_attr;
    thread_attr.stack = _vn_qm_thread_stack;
    thread_attr.stackSize = sizeof(_vn_qm_thread_stack);
    thread_attr.priority = _VN_THREAD_PRIORITY;
    thread_attr.attributes = IOS_THREAD_CREATE_JOINABLE;
    thread_attr.start = true;
    pAttr = &thread_attr;
#endif
#endif

    rv = _vn_qm_init();

    if (rv >= 0) {
        _vn_qm_done = 0;
#if !_VN_USE_SERVICE_THREAD
        rv = _vn_thread_create(&_vn_qm_thread, pAttr, 
                               (void*) &_vn_qm_run, NULL);
#endif
    }

    if (rv < 0) {
        _vn_qm_done = 1;
        _VN_TRACE(TRACE_ERROR, _VN_SG_QM,
                  "Unable to start VN Queue Manager: %d\n", rv);
    }    
    return rv;
}

int _vn_qm_stop()
{
    if (!_vn_qm_done) {
        /* TODO: use synchronized object to signal thread should finish */
        _vn_qm_done = 1;
#if !_VN_USE_SERVICE_THREAD
        _vn_thread_join(_vn_qm_thread, NULL);
#endif
    }
    return _VN_ERR_OK;
}


//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnlocal.h"

_vn_recv_stats_t _vn_recv_stats;
static _vn_dlist_t _vn_disp_queue; /* Dispatcher Queue of packets to process */

/* If service thread (_VN_USE_SERVICE_THREAD) is the only thread that
   access disp queue, then don't need lock.  But to avoid extra copy, USB
   thread can also access queue, so use lock. REVIEW: Avoiding copy may
   not be worth lock, but due to limited memory on NC, copying of large
   packets may cause out of memory error.  May need better solution. */
#define _VN_DISP_USE_LOCK    1

#if !_VN_DISP_USE_LOCK
#define _VN_DISP_LOCK()   
#define _VN_DISP_UNLOCK()
#else
static _vn_mutex_t _vn_disp_mutex; 
#define _VN_DISP_LOCK()   _vn_mutex_lock(&_vn_disp_mutex)
#define _VN_DISP_UNLOCK()   _vn_mutex_unlock(&_vn_disp_mutex)
#endif

#if !_VN_USE_SERVICE_THREAD
static _vn_cond_t  _vn_disp_notempty;
#endif

/* Network packet with  IP address and port */
typedef struct {
    _vn_wbuf_t  *wbuf;
    _vn_inaddr_t addr;
    _vn_inport_t port;
} _vn_netpkt_t;

_vn_recv_stats_t* _vn_get_recv_stats()
{
    return &_vn_recv_stats;
}

/* Dispatcher (on SC) */

#if _VN_RECV_TIMER

/* Locking? */
#define _VN_RECV_TIMER_MSECS              2*1000
#define _VN_RECV_TIMER_MAX               60*1000
#define _VN_RECV_TIMER_TIMEOUT         1*60*1000

typedef struct {
    _VN_net_t net_id;
    _VN_host_t from;
    _VN_host_t to;
    _VN_port_t port;
    uint8_t rseq_no;
} _vn_recv_timer_info_t;

uint32_t _vn_recv_timeout = _VN_RECV_TIMER_TIMEOUT;

void _vn_recv_set_timeout(uint32_t timeout)
{
    _vn_recv_timeout = timeout;
}

uint32_t _vn_recv_get_timeout()
{
    return _vn_recv_timeout;
}

_vn_timer_t* _vn_recv_timer_create(_VN_net_t net_id, 
                                   _VN_host_t from, _VN_host_t to,
                                   _VN_port_t port, uint8_t rseq_no)
{
    _vn_recv_timer_info_t* recv_info;
    _vn_timer_t* timer;

    recv_info = _vn_malloc(sizeof(_vn_recv_timer_info_t));
    if (recv_info == NULL) return NULL;
    recv_info->net_id = net_id;
    recv_info->from = from;
    recv_info->to = to;
    recv_info->port = port;
    recv_info->rseq_no = rseq_no;
    timer =_vn_timer_create(_VN_RECV_TIMER_MSECS, recv_info,
                            _vn_recv_timer_cb);
    if (timer == NULL) {
        _vn_free(recv_info);
    }

    return timer;
}

void _vn_recv_timer_update(_vn_timer_t* timer, uint8_t rseq_no)
{
    _vn_recv_timer_info_t* recv_info;
    assert(timer);
    assert(timer->data);
    recv_info = (_vn_recv_timer_info_t*) timer->data;
    recv_info->rseq_no = rseq_no;    
}

void _vn_recv_timer_destroy(_vn_timer_t* timer)
{
    if (timer) {
        /* Remove from timer list */
        _vn_timer_cancel(timer);
        if (timer->data)
            _vn_free(timer->data);
        _vn_timer_delete(timer);
    }
}

void _vn_recv_timer_cb(_vn_timer_t* timer)
{
    _vn_recv_timer_info_t* recv_info;
    uint32_t total, timeout;
    assert(timer);
    recv_info = (_vn_recv_timer_info_t*) timer->data;
    assert(recv_info);
    /* Timeout */
    total = _vn_timer_get_total(timer);

    /* In callback, the timer has expired and been removed from list */
    timeout = _vn_recv_get_timeout();
    if (total < timeout) {
        uint32_t delay, new_delay, max;
        
        /* If total delay still small, reset timer with longer delay 
           and resend nack */
        _VN_TRACE(TRACE_FINE, _VN_SG_TIMER, 
                  "_vn_recv_timer_cb: Sending nack to "
                  "net 0x%08x, from host %u to %u, port %u, rseq %u\n",
                  recv_info->net_id, recv_info->from, recv_info->to,
                  recv_info->port, recv_info->rseq_no);
        _vn_qm_enqueue_ack(recv_info->net_id, recv_info->from, recv_info->to,
                           recv_info->port, _VN_OPT_NEG_ACK, 
                           recv_info->rseq_no);

        max = MIN(_VN_RECV_TIMER_MAX, timeout - total);
        delay = _vn_timer_get_delay(timer);
        new_delay = MIN(delay*2, max);

        _vn_timer_mod(timer, new_delay, false);
        _vn_timer_retrigger(timer, false);
    }
    else {
        /* TODO: If too long, give up on host altogether */        
        _VN_TRACE(TRACE_WARN, _VN_SG_TIMER,
                  "_vn_recv_timer_cb: delay %u exceeds max recv timer %u "
                  "for net 0x%08x, from host %u to %u, port %u, rseq %u\n",
                  total, timeout, recv_info->net_id, 
                  recv_info->from, recv_info->to,
                  recv_info->port, recv_info->rseq_no);
        /* _vn_timer_reset_total(timer); */
    }
}
#endif

#if _VN_INACTIVITY_TIMER

#define _VN_INACTIVITY_TIMER_MSECS   12*1000
#define _VN_INACTIVITY_TIMER_TIMEOUT 1*60*1000

uint32_t _vn_inactivity_timeout = _VN_INACTIVITY_TIMER_TIMEOUT;
uint32_t _vn_inactivity_msecs = _VN_INACTIVITY_TIMER_MSECS;

void _vn_inactivity_set_timeout(uint32_t timeout)
{
    _vn_inactivity_timeout = timeout;
    /* Trigger timer to ping at 1/5th of timeout */
    _vn_inactivity_msecs = timeout/5;
}

uint32_t _vn_inactivity_get_timeout()
{
    return _vn_inactivity_timeout;
}

_vn_timer_t* _vn_inactivity_timer_create(_VN_net_t net_id, _VN_host_t host)
{
    _VN_addr_t addr = _VN_make_addr(net_id, host);
    _vn_timer_t* timer;
    uint32_t timeout;

    timeout = _vn_inactivity_msecs;
    timer = _vn_timer_create(timeout, (void*) addr, _vn_inactivity_timer_cb);

    return timer;
}

void _vn_inactivity_timer_destroy(_vn_timer_t* timer)
{
    if (timer) {
        /* Remove from timer list */
        _vn_timer_cancel(timer);
        _vn_timer_delete(timer);
    }
}

void _vn_inactivity_timer_cb(_vn_timer_t* timer)
{
    _VN_addr_t addr;
    _VN_net_t net_id;
    _VN_host_t host_id;
    uint32_t total;

    assert(timer);

    addr = (_VN_addr_t) timer->data;
    net_id = _VN_addr2net(addr);
    host_id = _VN_addr2host(addr);

    total = _vn_timer_get_total(timer);

    if (total < _vn_inactivity_timeout) {
        /* If total delay still small, ping host and retrigger timer */
        _vn_send_ping(_VN_MSG_ID_INVALID, _VN_MSG_PING_REQ, 0,
                      net_id, _VN_HOST_SELF, host_id);
        _vn_timer_retrigger(timer, false);       
    }
    else {
        _VN_TRACE(TRACE_WARN, _VN_SG_TIMER,
                  "_vn_inactivity_timer_cb: Removing host %u from net 0x%08x "
                  "after %u ms of inactivitity\n", host_id, net_id, total);
        
        _vn_timeout_host(net_id, host_id);
    }
}
#endif

_vn_netpkt_t* _vn_new_netpkt(const void *msg, _VN_msg_len_t len, 
                             _vn_inaddr_t addr, _vn_inport_t port)
{
    _vn_netpkt_t* npkt;
    npkt = _vn_malloc(sizeof(_vn_netpkt_t));
    if (npkt) {
        /* make my own copy of the message buf */
        npkt->wbuf = _vn_new_vn_buffer(msg, len);
        if (npkt->wbuf == NULL) {
            _vn_free(npkt);
            return NULL;
        }
        npkt->addr = addr;
        npkt->port = port;
    }
    return npkt;
}

void _vn_free_netpkt(_vn_netpkt_t* npkt)
{
    if (npkt) {
        _vn_free_msg_buffer_aligned(npkt->wbuf);
        _vn_free(npkt);
    }
}

/* Called directly by qm from qm thread */
int _vn_dispatcher_recv(const void *msg, _VN_msg_len_t len, 
                        _vn_inaddr_t addr, _vn_inport_t port)
{
    int rv;
    _vn_netpkt_t* npkt;

    assert(msg);
    
    /* make my own copy of the message buf */
    npkt = _vn_new_netpkt(msg, len, addr, port);
    if (npkt == NULL) {
        return _VN_ERR_NOMEM;
    }        

    /* Got packet - buffer in dispatcher queue */
    _VN_DISP_LOCK();
    if (_vn_dlist_empty(&_vn_disp_queue)) {
#if _VN_USE_SERVICE_THREAD
        _vn_service_signal(_VN_DISP_MSG);
#else
        _vn_cond_signal(&_vn_disp_notempty);
#endif
    }

    rv = _vn_dlist_add(&_vn_disp_queue, npkt);

    _VN_DISP_UNLOCK();

    if (rv < 0) {
        /* Error adding packet to queue */
        _vn_free_netpkt(npkt);
    }

    return rv;
}

_vn_netpkt_t* _vn_dispatcher_dequeue()
{
    _vn_netpkt_t* npkt; 
    _VN_DISP_LOCK();
    npkt = (_vn_netpkt_t*) _vn_dlist_deq_front(&_vn_disp_queue); 

#if _VN_USE_SERVICE_THREAD
    /* Make sure that dispatcher gets called again */
    if (!_vn_dlist_empty(&_vn_disp_queue)) {
        _vn_service_signal(_VN_DISP_MSG);
    }
#endif

    _VN_DISP_UNLOCK();    
    return npkt;
}

#if !_VN_USE_SERVICE_THREAD
/* Processes packets in queue (called by qm from qm thread) */
int _vn_dispatcher_proc_queue()
{
    _vn_netpkt_t* npkt;
    int i = 0;
    while ((npkt = _vn_dispatcher_dequeue())) {
        _vn_dispatcher_recv_pkt(npkt->wbuf, _vn_get_recv_stats(),
                                npkt->addr, npkt->port);
        npkt->wbuf = NULL;
        _vn_free_netpkt(npkt);
        i++;
    }
    return i;
}
#endif

/* Processes next packet in dispatcher queue (called by service thread) */
bool _vn_dispatcher_proc_next()
{
    _vn_netpkt_t* npkt;
    bool res;
    
    _vn_net_lock();
    npkt = _vn_dispatcher_dequeue();
    if (npkt) {
        _vn_dispatcher_recv_pkt(npkt->wbuf, _vn_get_recv_stats(),
                                npkt->addr, npkt->port);
        npkt->wbuf = NULL;
        _vn_free_netpkt(npkt);
        res = true;
    } else {
        res = false;
    }
    _vn_net_unlock();

    return res;
}

int _vn_dispatcher_recv_pkt(_vn_wbuf_t* pkt, _vn_recv_stats_t* stats,
                            _vn_inaddr_t addr, _vn_inport_t port)
{
    int rv;

    _VN_TRACE(TRACE_FINEST, _VN_SG_DISP, "_vn_dispatcher_proc_pkt: start\n");
    _vn_net_lock();

    if (stats) stats->total_pkts++;
    if (stats) stats->total_bytes += pkt->len;


    rv = _vn_dispatcher_proc_pkt(pkt, stats, addr, port);
    if (rv >= 0) {
        /* Packet successfully processed */
    }
    else {
        if (stats) stats->dropped_pkts++;
        _vn_free_msg_buffer_aligned(pkt);
    }
    
    _vn_net_unlock();
    _VN_TRACE(TRACE_FINEST, _VN_SG_DISP,
              "_vn_dispatcher_proc_pkt: rv=%d\n", rv);
    return rv;
}

int _vn_dispatcher_proc_pkt(_vn_wbuf_t* pkt, _vn_recv_stats_t* stats,
                            _vn_inaddr_t addr, _vn_inport_t port)
{
    _vn_msg_t msg;
    _vn_msg_header_t* hdr;
    uint8_t seq, syn_mask, ack_mask, ack_port = 0, ack_seq = 0, opt;
    uint8_t *buf;
    uint16_t len;
    _vn_port_info_t* port_info;
    _vn_port_seq_no_t* port_seq;
    int rv = _VN_ERR_OK;
    assert(pkt);

    len = pkt->len;
    buf = pkt->buf + pkt->offset;
    if (len > _VN_PKT_VERSION_OFFSET) {
        /* Verify packet is good packet */
        uint8_t version = buf[_VN_PKT_VERSION_OFFSET];

        if ( version == _VN_HS_VERSION_ID ) {
            /* Handshake message - send to handshake processor */
            /* TODO: Eliminate copy */
            _vn_buf_t *new_buf = 
                _vn_new_msg_buffer(pkt->buf + pkt->offset, pkt->len);
            if (stats) stats->handshake++;
            rv = _vn_hs_recv_pkt(new_buf, addr, port);
            if (rv >= 0) {
                _vn_free_msg_buffer_aligned(pkt);
            } else {
                _vn_free_msg_buffer(new_buf);
            }
            return rv;
        } 
        
        if ( version != _VN_VERSION) 
        {
            /* Bad version */
            if (stats) stats->badversion++;
            return _VN_ERR_BAD_VERSION;
        }
    }
    else {
        /* Invalid message, drop and indicate error */
        if (len == 0) {
            if (stats) stats->empty++;
        }
        else {
            _VN_TRACE(TRACE_WARN, _VN_SG_DISP,
                      "Message too short, only %d bytes\n", len);
            if (stats) stats->short_pkts++;
        }
        return _VN_ERR_TOO_SHORT;
    }

    /* Parse header */
    msg.attr = 0;
    msg.pkt = NULL; 
    rv = _vn_parse_msg(&msg, buf, len);

    if (rv >= _VN_ERR_OK) {
        _vn_net_info_t* net_info;
        _vn_host_info_t* host_info;
        _vn_channel_t* channel;
        uint32_t gseq;

        hdr = (_vn_msg_header_t*) buf;
        
        if (len > _VN_MAX_TOTAL_MSG_LEN)
        {
            /* This datagram is awfully big */
            if (stats) stats->oversized_pkts++;
        }

        /* Look up net and host info */
        net_info = _vn_lookup_net_info(msg.net_id);
        if (net_info == NULL) {
            _VN_TRACE(TRACE_INFO, _VN_SG_DISP,
                      "Unknown net 0x%08x\n", msg.net_id);
            if (stats) stats->unknown_net++;
            return _VN_ERR_NETID;
        }

        host_info = _vn_lookup_host_info(net_info, msg.from);
        if (host_info == NULL) 
        {
            _VN_TRACE(TRACE_INFO, _VN_SG_DISP,
                      "Unknown sender %u for net 0x%08x\n", 
                      msg.from, msg.net_id);
            if (stats) stats->unknown_from++;
            rv = _VN_ERR_HOSTID;
            return rv;
        }

        channel = _vn_find_channel(host_info, msg.to);
        if (channel == NULL) {
            channel = _vn_channel_create(msg.to);
            if (channel == NULL) {
                _VN_TRACE(TRACE_ERROR, _VN_SG_DISP, "_vn_dispatcher_proc_pkt: "
                          "Error creating channel");
                if (stats) stats->nomem++;
                rv = _VN_ERR_NOMEM;
                return rv;
            }
            _vn_add_channel(host_info, channel);
        }

        /* Check gseq - assumes that gseq never rolls over 
           (< 2^32 pkts between host pairs)*/
        gseq = ntohl(hdr->gseq);
        if (gseq >= (channel->gseq_r+1) ) {
            /* Authenticate packet */
            if (!_vn_verify_pkt(pkt, net_info->key)) {
                _VN_TRACE(TRACE_WARN, _VN_SG_DISP,
                          "Error authenticating packet\n", rv);
                if (stats) stats->bad_auth++;
                return _VN_ERR_AUTH;
            }
            
            /* Message authenticated */
            /* Okay - accept packet */
            _VN_TRACE(TRACE_FINER, _VN_SG_DISP,
                      "Accepting packet with gseq %u, prev gseq %u\n",
                      gseq, channel->gseq_r);
            channel->gseq_r = gseq;
        }
        else {
            /* Bad GSeq */
            _VN_TRACE(TRACE_WARN, _VN_SG_DISP,
                      "Bad gseq %u from %u, net 0x%08x, "
                      "last received gseq %u\n",
                      gseq, msg.from, msg.net_id, channel->gseq_r);
            if (stats) stats->bad_gseq++;
            return _VN_ERR_GSEQ;
        }

        /* Check netid, from, to, port active
         * Check seq number
         * Drop firewall keep-alives, and ack onlys */

        msg.seq = seq = hdr->pseq;

        opt = hdr->opt_field_mask;
        ack_mask = _VN_PKT_OPT_ACK_MASK(hdr);
        syn_mask = _VN_PKT_OPT_SYN_MASK(hdr);

#if _VN_INACTIVITY_TIMER
        /* Retrigger inactivity timer */
        if (host_info->inactivity_timer) {
            _vn_timer_retrigger(host_info->inactivity_timer, true);
        }
#endif

        if (syn_mask) {
            if (stats) stats->syns++;
        }

        if (ack_mask) {
            /* There is an ack mask - forward to retransmission manager */
            uint8_t* ack_optfields = buf + len + _VN_PKT_OPT_ACK_OFFSET_R(hdr);
            ack_port = ack_optfields[0];
            ack_seq = ack_optfields[1];

            _vn_retx_proc_ack(msg.net_id, msg.from, msg.to, ack_port,
                              ack_mask, ack_seq);
            if (stats) {
                if (ack_mask == _VN_OPT_POS_ACK) {
                    stats->acks++;
                }
                else if (ack_mask == _VN_OPT_NEG_ACK) {
                    stats->nacks++;
                }
            }
        }

        /* Filtering step: do we really want this packet? */

        /* First verify that this packet is actually for me */        
        if (!_vn_islocalhost(net_info, msg.to)) {
            /* Okay, this packet is for someone else */
            _VN_TRACE(TRACE_WARN, _VN_SG_DISP, "Packet not for me: "
                      "net=0x%08x, from=%u, to=%u, port-%u\n", 
                      net_info->net_id, msg.from, msg.to, msg.port);
            if (stats) stats->unknown_to++;
            rv = _VN_ERR_HOSTID;
            /* Return don't need to check sequence number */
            return rv;
        }

        _vn_host_status_update_received(net_info->net_id, msg.from, &msg,
                                        addr, port);
        
        if ((msg.port == _VN_PORT_NET_CTRL) && (msg.data_size == 0)) {
            /* Empty control packet - ignore */
            if (stats) stats->zero_ctrl++;
            rv = _VN_ERR_EMPTYPKT;
        }
        
        port_info = _vn_lookup_port_from_net(net_info, 
                                             _VN_make_addr(net_info->net_id, msg.to),
                                             msg.port);
        if (port_info) {
            port_seq = _vn_lookup_port_seq_no(port_info, msg.from);
            if (port_seq == NULL) {
                /* first packet from host */
                port_seq = _vn_add_port_seq_no(port_info, 
                                               msg.from);
                /* TODO: what if error adding port seq no? */
                _VN_TRACE(TRACE_FINER, _VN_SG_DISP,
                          "New seq entry for addr 0x%08x, "
                          "port %u, to %u from %u\n", port_info->addr, 
                          port_info->port, msg.to, msg.from);
                
            }
        }
        else { port_seq = NULL; }

        if (port_info && port_info->active) {
            /* Okay, I actually want this packet, now check seq_no */
            if (opt & _VN_OPT_RELIABLE) {
                msg.attr |= _VN_MSG_RELIABLE;
                /* Reliable */
                if (port_seq) 
                {
                    uint8_t exp_seq = port_seq->rseq_no + 1;
                    if (syn_mask) {
                        /* Only sync to larger seq numbers */
                        if (_vn_seqno_larger_than(seq, exp_seq)) {
                            _VN_TRACE(TRACE_FINE, _VN_SG_DISP,
                                      "Syncing to rseq %u for addr 0x%08x, "
                                      "port %u, to %u from %u\n", 
                                      seq, port_info->addr, 
                                      port_info->port, msg.to, msg.from);
                            exp_seq = seq;
                        }
                    }
                    if (exp_seq == seq) {
                        /* Been expecting this packet */
                        /* Accept packet and notify queue manager
                           to send ack based on net_id, host_id, port */
                        
                        _vn_qm_enqueue_ack(msg.net_id, msg.to, msg.from,
                                           msg.port, _VN_OPT_POS_ACK, seq);
                        
                        /* Update sequence number */
                        port_seq->rseq_no = seq;
                        if (stats) stats->rinseq++;
                        
#if _VN_RECV_TIMER
                        if (port_seq->recv_timer) {
                            _vn_timer_cancel(port_seq->recv_timer);
                            _vn_timer_reset_total(port_seq->recv_timer);
                        }
#endif                            
                    }
                    else if (_vn_seqno_larger_than(seq, exp_seq)) {
                        _VN_TRACE(TRACE_FINE, _VN_SG_DISP,
                                  "Out of RSeq: net 0x%08x, "
                                  "to %u from %u, port %u, "
                                  "exp rseq_no %u, seq %u\n", 
                                  msg.net_id, msg.to, msg.from,
                                  msg.port, exp_seq, seq);
                        /* seq # greater than expected */
                        /* Notify queue manager to send nack */
                        _vn_qm_enqueue_ack(msg.net_id, msg.to, msg.from, 
                                           msg.port, _VN_OPT_NEG_ACK, exp_seq);
                        
#if _VN_RECV_TIMER                            
                        /* Start timer waiting for retransmission */
                        if (port_seq->recv_timer) {
                            _vn_timer_cancel(port_seq->recv_timer);
                            _vn_recv_timer_update(port_seq->recv_timer,
                                                  port_seq->rseq_no);
                        }
                        else {
                            port_seq->recv_timer = 
                                _vn_recv_timer_create(msg.net_id,
                                                      msg.to,
                                                      msg.from,
                                                      msg.port,
                                                      port_seq->rseq_no);
                            
                        }
                        
                        if (port_seq->recv_timer)
                            _vn_timer_retrigger(port_seq->recv_timer, false);
#endif
                        if (stats) stats->routseq++;
                        rv = _VN_ERR_OUTOFSEQ;
                    }
                    else {
                        _VN_TRACE(TRACE_FINER, _VN_SG_DISP,
                                  "Duplicate RSeq: net 0x%08x, "
                                  "to %u from %u, port %u, "
                                  "exp rseq_no %u, seq %u\n",
                                  msg.net_id, msg.to, msg.from,
                                  msg.port, exp_seq, seq);
                        /* seq # less than expected */
                        /* Duplicate packet, drop */
                        
                        /* Maybe duplicate because ack was missed,
                           resend ack */
                        _vn_qm_enqueue_ack(msg.net_id, msg.to, msg.from,
                                           msg.port, _VN_OPT_POS_ACK,
                                           port_seq->rseq_no);
                        
                        if (stats) stats->rdup++;
                        rv = _VN_ERR_DUPPKT;
                    }
                }
            }
            else {
                /* Nonreliable */
                if (port_seq &&
                    _vn_seqno_larger_than(seq, port_seq->lseq_no)) {
                    /* In sequence */
                    /* Update lseq_no */
                    port_seq->lseq_no = seq;
                    if (stats) stats->linseq++;
                }
                else {
                    /* Out of sequence */
                    _VN_TRACE(TRACE_FINE, _VN_SG_DISP,
                              "Out of LSeq: net 0x%08x, "
                              "to %u from %u, port %u, "
                              "last lseq_no %u, seq %u\n",
                              msg.net_id, msg.to, msg.from,
                              msg.port, port_seq->lseq_no, seq);
                    
                    if (stats) stats->loutseq++;
                    rv = _VN_ERR_OUTOFSEQ;
                }
            }
        }
        else {
            /* Well, I never activated this port */
            _VN_TRACE(TRACE_WARN, _VN_SG_DISP,
                      "%s port, net 0x%08x, port %u, \n", 
                      port_info? "Inactive":"Unknown", 
                      msg.net_id, msg.port);
            if (port_info) {
                if (stats) stats->inactive_port++;
                rv = _VN_ERR_PORT_STATE;
                if (opt & _VN_OPT_RELIABLE) {
                    /* DO NOT update port seq no */
                }
                else {
                    /* Keep up with lossy port seq no so
                       when the port is activiated, we are in
                       sync with the sender */
                    if (port_seq &&
                        _vn_seqno_larger_than(seq, port_seq->lseq_no)) {
                        /* In sequence */
                        /* Update lseq_no */
                        port_seq->lseq_no = seq;
                    }
                }
            }
            else {
                if (stats) stats->unknown_port++;
                rv = _VN_ERR_PORT;
            }
        }

        if (rv >= 0) {
            /* Packet with valid header - Queue packet */

            /* Decrypt here while we still have AES key */
            if (opt & _VN_OPT_ENC) {
                uint8_t *iv;
                msg.attr |= _VN_MSG_ENCRYPTED;
                iv = _vn_new_iv(net_info->key, buf, len);
                if (iv) {
                    rv = _vn_decrypt_msg(buf + _VN_PKT_DATA_OFFSET(hdr), 
                                         _VN_RND_AES_SIZE(msg.data_size),
                                         iv, net_info->key);
                    _vn_free_iv(iv);
                } else {
                    rv = _VN_ERR_NOMEM;
                }
            }
        }

        if (rv >= 0) {
            /* Process control packet */
            if (msg.port == _VN_PORT_NET_CTRL) {

                if (_VN_TRACE_ON(TRACE_FINEST, _VN_SG_CTRL)) {
                    _VN_TRACE(TRACE_FINEST, _VN_SG_CTRL, 
                              "Got control packet:\n");
                    _vn_dbg_print_pkt(stdout, buf, len);
                }

                _vn_proc_ctrl_msg(&msg, addr, port);
                _vn_free_msg_buffer_aligned(pkt);
            }
            else if (msg.port > _VN_PORT_MAX_RESERVED) {
                /* TODO: Eliminate copy */
                /* Send to message headers processor */
                msg.pkt = _vn_new_msg_buffer(pkt->buf + pkt->offset,
                                             pkt->len);
                if (msg.pkt) {
                    rv = _vn_mhp_dispatch(&msg);
                    if (rv >= 0) {
                        _vn_free_msg_buffer_aligned(pkt);
                    }
                } else {
                    rv = _VN_ERR_NOMEM;
                }
                if (rv < 0) {
                    _VN_TRACE(TRACE_ERROR, _VN_SG_DISP,
                              "Error %d dispatching to mhp: "
                              "net 0x%08x, from %u to %u, port %u\n", 
                              rv, msg.net_id, msg.from, msg.to, msg.port);
                }
            }
            else {
                _VN_TRACE(TRACE_ERROR, _VN_SG_DISP,
                          "Ignoring packet on reserved port: "
                          "net 0x%08x, from %u to %u, port %u\n",
                          msg.net_id, msg.from, msg.to, msg.port);
                _vn_free_msg_buffer_aligned(pkt);
            }
        }

        return rv;
    }
    else {
        /* Invalid message, drop and indicate error */
        _VN_TRACE(TRACE_WARN, _VN_SG_DISP,
                  "Message too short, only %d bytes\n", len);
        if (stats) stats->short_pkts++;
        return _VN_ERR_TOO_SHORT;
    }
}

void _vn_dispatcher_print_stats(FILE* fp) 
{
    _vn_print_recv_stats(fp, _vn_get_recv_stats());
}

int _vn_dispatcher_done = 1;

#if !_VN_USE_SERVICE_THREAD
_vn_thread_t _vn_dispatcher_thread;

#ifdef _VN_RPC_DEVICE
void _vn_dispatcher_run()
{
    int rv;
    int timeout = 1000; /* 1 second timeout */

    _VN_TRACE(TRACE_FINE, _VN_SG_QM, "Starting VN Dispatcher\n");
    while(!_vn_dispatcher_done) {
        /* Wait for the queue to be not empty */
        _VN_DISP_LOCK();
        if (_vn_dlist_empty(&_vn_disp_queue)) {
            _vn_cond_timedwait(&_vn_disp_notempty, &_vn_disp_mutex, timeout);
        }
        _VN_DISP_UNLOCK();

        rv = _vn_dispatcher_proc_queue();
    }
    if (_VN_TRACE_ON(TRACE_INFO, _VN_SG_QM)) {
        _vn_dispatcher_print_stats(stdout);
    }
    _VN_TRACE(TRACE_FINE, _VN_SG_QM,
              "Finished running VN Dispatcher\n");
}
#else
void _vn_dispatcher_run()
{
    _vn_buf_t* pkt;
    _vn_recv_stats_t* stats;
    _vn_netif_t *netif;
    uint32_t timeout = 1000; /* 1 second timeout on read */
    int rv;
    _vn_inaddr_t addr;
    _vn_inport_t port;

    _VN_TRACE(TRACE_FINE, _VN_SG_DISP, "Starting VN Dispatcher\n");
    stats = _vn_get_recv_stats();

    netif = _vn_netif_get_instance();
    assert(netif);

    pkt = _vn_get_msg_buffer(_VN_MSG_BUF_SIZE);
    if (pkt == NULL) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_DISP,
                  "Error starting dispatcher: Out of memory\n");
        return;        
    }

    while(!_vn_dispatcher_done) {
        rv = _vn_recv_wait(netif->sockfd, 
                           pkt->buf,
                           pkt->max_len,
                           timeout,
                           &addr, &port);
        
        if (rv >= 0) {
            _vn_wbuf_t *vn_pkt;
            pkt->len = (_VN_msg_len_t) rv;
            /* _vn_dbg_print_pkt(stdout, pkt->buf, pkt->len);  */
                
            /* Copies packet from recv buffer to newly allocated buffer */
            vn_pkt = _vn_new_vn_buffer(pkt->buf, pkt->len);
            if (vn_pkt) {
                /* Got packet - send to dispatcher */
                rv = _vn_dispatcher_recv_pkt(vn_pkt, stats, addr, port);
            } else {
                /* Oops, out of buffers */
                if (stats) stats->nobufs++;
                if (stats) stats->dropped_pkts++;
                
                /* Wait a bit and hope memory frees up */
                _vn_thread_sleep(timeout);
            }
        }
        else if (rv != _VN_ERR_TIMEOUT) {
            /* TODO: handle error */
            _VN_TRACE(TRACE_WARN, _VN_SG_DISP,
                      "Error %d getting packet\n", rv);
        }
    }
    
    if (pkt) {
        _vn_free_msg_buffer(pkt);
    }

    if (_VN_TRACE_ON(TRACE_INFO, _VN_SG_DISP)) {
        _vn_dispatcher_print_stats(stdout);
    }
    _VN_TRACE(TRACE_FINE, _VN_SG_QM,
              "Finished running VN Dispatcher\n");
}
#endif
#endif

int _vn_dispatcher_init()
{
    int rv;

    memset(&_vn_recv_stats, 0, sizeof(_vn_recv_stats));
    _vn_dlist_init(&_vn_disp_queue);

#if _VN_DISP_USE_LOCK
    rv = _vn_mutex_init(&_vn_disp_mutex);
    if (rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_QM, 
                  "Failed to initialize mutex for Dispatcher: %d\n", rv);
        return rv;
    }
#endif

#if !_VN_USE_SERVICE_THREAD
    rv = _vn_cond_init(&_vn_disp_notempty);
    if (rv < 0) {
        _vn_mutex_destroy(&_vn_disp_mutex);

        _VN_TRACE(TRACE_ERROR, _VN_SG_QM, 
                  "Failed to initialize condition variable for Dispatcher: %d\n", rv);
        return rv;
    }
#endif

    rv = _vn_mhp_init();
    if (rv < 0) {
#if _VN_DISP_USE_LOCK
        _vn_mutex_destroy(&_vn_disp_mutex);
#endif

#if !_VN_USE_SERVICE_THREAD
        _vn_cond_destroy(&_vn_disp_notempty);
#endif

        return rv;
    }

    return _VN_ERR_OK;
}

int _vn_dispatcher_cleanup()
{
    /* Free all resources uses by the dispatcher */
    _vn_mhp_cleanup();
#if _VN_DISP_USE_LOCK
    _vn_mutex_destroy(&_vn_disp_mutex);
#endif

#if !_VN_USE_SERVICE_THREAD
    _vn_cond_destroy(&_vn_disp_notempty);
#endif
    _vn_dlist_clear(&_vn_disp_queue, (void*) _vn_free_netpkt);
    return _VN_ERR_OK;
}

int _vn_dispatcher_start()
{
    int rv = _VN_ERR_OK;
    _vn_dispatcher_init();

    _vn_dispatcher_done = 0;

#if !_VN_USE_SERVICE_THREAD
    rv = _vn_thread_create(&_vn_dispatcher_thread, NULL, 
                           (void*) &_vn_dispatcher_run, NULL);
#endif

    if (rv < 0) {
        _vn_dispatcher_done = 1;
        _vn_dispatcher_cleanup();
        _VN_TRACE(TRACE_ERROR, _VN_SG_DISP,
                  "Unable to start VN Dispatcher: %d\n", rv);
    }    

    return rv;
}

int _vn_dispatcher_stop()
{
    /* TODO: use synchronized object to signal thread should finish */
    if (!_vn_dispatcher_done) {
        _vn_dispatcher_done = 1;
#if !_VN_USE_SERVICE_THREAD
        _vn_thread_join(_vn_dispatcher_thread, NULL);
#endif
    }

    return _VN_ERR_OK;
}

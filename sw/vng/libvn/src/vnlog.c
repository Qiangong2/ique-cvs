#include "vnlocal.h"

static const char* _vn_errstrs[] = 
{
    "Okay",
    "Failed",
    "No memory",
    "Not found",
    "Bad net id",
    "Bad host id",
    "Invalid time",
    "Invalid length"
    "Invalid value",
    "Invalid port",
    "Invalid port state",
    "Too short",
    "Bad version",
    "No support",
    "Out of sequence",
    "Unknown server",
    "Operation has timed out",
    "Operation is still pending",
    "Operation was canceled",
    "Bad handle",
    "Duplicate packet",
    "Duplicate entry",
    "Invalid priority",
    "Authentication error",
    "No more resources",
    "Empty packet",
    "Bad gseq",
    "Socket error",
    "No more space",
    "Not owner",
    "Configuration error",
    "Blocked",
    "Rejected",
    "Closed",
    "Unreachable",
    "USB Error"
};

const char* _vn_errorstr(int rv)
{
    int index = -rv;
    if ((index < 0) || (index >= sizeof(_vn_errstrs)/sizeof(_vn_errstrs[0])))
        return "Unknown error";
    else return _vn_errstrs[index];
}

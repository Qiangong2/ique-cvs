//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnlocal.h"

/* Table of netid <-> net_info (GBA)
 *
 * On the GBA will have simple array of VNs
 * (participated in only a couple of VN at a time)
 */
_vn_ht_table_t _vn_net_info_table;

void _vn_net_info_destroy(_vn_net_info_t* net);
void _vn_net_info_clear_all();

_vn_ht_table_t* _vn_get_net_info_table()
{
    return &_vn_net_info_table;
}

/* Functions for controlling access to the VN core */

/* Assumes mutex already initialized */
int _vn_core_init()
{
    int rv;

    /* Initialize hash table */

    /* TODO: For client, do not allow resize, limit max number of entries */
    /* TODO: For server, start with larger table */
#ifdef _SC
    rv = _vn_ht_init(&_vn_net_info_table, 
                     _VN_NET_TABLE_SIZE, _VN_NET_TABLE_SIZE,
                     false /* resize? */,
                     _vn_ht_hash_int, _vn_ht_key_comp_int);
#else
    rv = _vn_ht_init(&_vn_net_info_table, 
                     _VN_NET_TABLE_SIZE, 0, true /* resize? */,
                     _vn_ht_hash_int, _vn_ht_key_comp_int);
#endif
    if (rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_NETCORE, 
                  "Failed to initialize table for VN core: %d\n", rv);
        return rv;
    }
    

    /* Start Queue manager and Dispatcher */
    if ((rv = _vn_qm_start()) < 0) {
        goto out;
    }

    if ((rv = _vn_dispatcher_start()) < 0) {
        _vn_qm_stop();
        _vn_qm_cleanup();
        goto out;
    }
    
  out:
    if (rv < 0) {
        _vn_ht_clear(&_vn_net_info_table, (void*) _vn_net_info_destroy, true);
        return rv;
    }
    return _VN_ERR_OK;
}

int _vn_core_cleanup()
{
    /* Clear the VN tables */
    _vn_dispatcher_stop();
    _vn_qm_stop();
    _vn_dispatcher_cleanup();
    _vn_qm_cleanup();
    _vn_ht_clear(&_vn_net_info_table, (void*) _vn_net_info_destroy, true);
    return _VN_ERR_OK;
}

int _vn_core_reset()
{
    _vn_qm_reset();
    _vn_net_info_clear_all();
    return _VN_ERR_OK;
}

/* Functions for net_info table (GBA) - Part of the VN core */

_vn_host_info_t* _vn_host_info_create(_VN_guid_t guid, _VN_host_t host_id,
                                      int64_t clock_delta, uint16_t mask)
{
    /* TODO: Make sure host id is valid */
    _vn_host_info_t* host_info = _vn_malloc(sizeof(_vn_host_info_t));
    if (host_info) {
        host_info->guid = guid;
        host_info->host_id = host_id;
        host_info->clock_delta = clock_delta;        
        host_info->channels = NULL;
#if _VN_INACTIVITY_TIMER
        host_info->inactivity_timer = NULL;
#endif
        host_info->flags = mask;
        host_info->npending_from = 0;
        host_info->npending_to = 0;
        
        if (_VN_HOST_IS_LOCAL(host_info)) {
            /* Set connection state to self as reachable, and rtt as 0 */
            host_info->status.conn_state = _VN_HOST_ST_REACHABLE;
            host_info->status.rtt = 0;
        }
        else {
            host_info->status.conn_state = _VN_HOST_ST_UNKNOWN;
            host_info->status.rtt = _VN_TIMESTAMP_INVALID;
        }
    }
    return host_info;
}

void _vn_host_info_destroy(_vn_host_info_t* host_info)
{
    if (host_info) {
        if (host_info->channels) {
            _vn_dlist_destroy(host_info->channels, 
                              (void*) &_vn_channel_destroy);
        }

#if _VN_INACTIVITY_TIMER
        if (host_info->inactivity_timer) {
            _vn_inactivity_timer_destroy(host_info->inactivity_timer);
        }
#endif

        _vn_free(host_info);
    }
}

int _vn_net_set_daemon(_VN_net_t net_id, bool daemon)
{
    _vn_net_info_t* net = _vn_lookup_net_info(net_id);
    if (net) {
        if (daemon) {
            net->flags |= _VN_PROP_DAEMON;
        }
        else {
            net->flags &= ~_VN_PROP_DAEMON;
        }
        return _VN_ERR_OK;
    }
    else return _VN_ERR_NETID;
}

/* Creates a new net */
_vn_net_info_t* _vn_net_info_create(_VN_net_t net_id, _VN_host_t myhost,
                                    _VN_host_t master_host, _vn_key_t key)
{
    _vn_net_info_t* net = _vn_malloc(sizeof(_vn_net_info_t));
    if (net) {
        uint16_t myhost_flags = _VN_PROP_LOCAL;

        net->net_id = net_id;
        net->master_host = master_host;

#if _VN_USE_IOSC
        net->key.Ak = key.Ak;
        net->key.Hk = key.Hk;
#else
        assert(key);
        memcpy(net->key, key, sizeof(_vn_key_t));
#endif

        net->peer_hosts = NULL;
        net->localhosts = 0;

        /* Information kept by master */
        net->npending = 0;
        net->listen_call = _VN_MSG_ID_INVALID;
        net->host_id_hint = MAX(myhost, master_host) + 1;
        net->flags = 0;
        if (myhost == master_host) {
            /* I'm the owner! Set owner bit */
            net->flags |= _VN_PROP_OWNER;
            myhost_flags |= _VN_PROP_OWNER;
        }

        /* Add myself to the net */
        if (_vn_add_host_info_to_net(net, myhost, _vn_get_myguid(), 
                                     0, myhost_flags) < 0) {
            _vn_free(net);
            return NULL;
        }

        net->ports = _vn_dlist_create();
    }
    return net;
}

void _vn_net_info_destroy(_vn_net_info_t* net)
{    
    if (net) {
        if (net->peer_hosts) {
            _vn_dlist_destroy(net->peer_hosts, (void*) &_vn_host_info_destroy);
        }
        if (net->ports) {
            _vn_dlist_destroy(net->ports, (void*) &_vn_destroy_port_info);
        }

        /* We are deleting the default net, reset the default net id */
        if (net->net_id == _vn_get_default_net_id()) {
            _vn_set_default_net_id(_VN_NET_DEFAULT);
        }

#if _VN_USE_IOSC
        if (_VN_KEY_HANDLE_IS_VALID(net->key.Ak)) {
            IOSC_DeleteObject(net->key.Ak);
        }

        if (_VN_KEY_HANDLE_IS_VALID(net->key.Hk)) {
            IOSC_DeleteObject(net->key.Hk);
        }
#endif
            
        _vn_free(net);
    }
}

/* Add the net to the list of nets we know about */
/* Assumes net is not already in table */
_vn_net_info_t* _vn_add_net_info(_VN_net_t net_id, _VN_host_t myhost,
                                 _VN_host_t master_host, _vn_key_t key)
{
    _vn_net_info_t* net_info;
    if (net_id == _VN_NET_DEFAULT)
        net_id = _vn_get_default_net_id();

    net_info = _vn_net_info_create(net_id, myhost, master_host, key);
    if (net_info == NULL) return NULL;

    if (_vn_ht_add(&_vn_net_info_table, (_vn_ht_key_t) net_id, net_info) < 0) {
        /* Failed to add entry */
        _vn_net_info_destroy(net_info);
        return NULL;
    }

    return net_info;
}

int _vn_delete_net_info(_VN_net_t net_id)
{
    /* Delete the entry for the net */
    _vn_net_info_t* net_info;

    if (net_id == _VN_NET_DEFAULT)
        net_id = _vn_get_default_net_id();

    net_info = _vn_ht_remove(&_vn_net_info_table, (_vn_ht_key_t) net_id);
    if (net_info) {
        _vn_net_info_destroy(net_info);
        return _VN_ERR_OK;        
    }
    else return _VN_ERR_NETID;
}

void _vn_net_info_clear_all()
{
    _vn_ht_clear(&_vn_net_info_table, (void*) _vn_net_info_destroy, false);
}

/* NOTE: Include hosts in the midst of leaving or joining */
int _vn_get_peer_size(_vn_net_info_t* net_info)
{
    assert(net_info);
    return (net_info->peer_hosts)? _vn_dlist_size(net_info->peer_hosts): 0;
}

/* NOTE: Include hosts in the midst of leaving or joining */
int _vn_get_size(_vn_net_info_t* net_info)
{
    return _vn_get_peer_size(net_info);
}

int _vn_get_new_peer_id(_vn_net_info_t* net_info)
{
    _VN_host_t new_id;
    int npeers;
    uint16_t maxhost;

    if (net_info == NULL) return _VN_ERR_NETID;
    if (!_VN_NET_IS_OWNER(net_info)) return _VN_ERR_NOT_OWNER;

    npeers = _vn_get_size(net_info);
    maxhost = _vn_get_maxhost(net_info->net_id) + 1;
    if (npeers >= maxhost) return _VN_ERR_FULL;

    new_id = net_info->host_id_hint % maxhost;
    while (_vn_lookup_host_info(net_info, new_id) != NULL) {
        new_id = (new_id + 1) % maxhost;
    }
    
    return new_id;
}

/* Looks up net by net_id */
_vn_net_info_t* _vn_lookup_net_info(_VN_net_t net_id)
{
    if (net_id == _VN_NET_DEFAULT)
        net_id = _vn_get_default_net_id();
    return _vn_ht_lookup(&_vn_net_info_table, (_vn_ht_key_t) net_id);
}

/* Returns first localhost of net */
_vn_host_info_t* _vn_get_first_localhost(_vn_net_info_t* net)
{
    if (net && net->peer_hosts) {
        _vn_dnode_t* node;
        _vn_dlist_for(net->peer_hosts, node)
        {
            if (_vn_dnode_value(node)) {
                _vn_host_info_t* host_info = (_vn_host_info_t*) 
                    _vn_dnode_value(node);
                if (!_VN_HOST_IS_LOCAL(host_info)) {
                    break;
                }
                if (!_VN_HOST_IS_LEAVING(host_info))
                {
                    return host_info;
                }
            }
        }
        return NULL;
    }
    else {
        return NULL;
    }
}

/* Only search local hosts for host_id */
_vn_host_info_t* _vn_lookup_local_host_info(_vn_net_info_t* net, _VN_host_t host_id)
{
    if (net && net->peer_hosts) {
        _vn_dnode_t* node;
        _vn_dlist_for(net->peer_hosts, node)
        {
            if (_vn_dnode_value(node)) {
                _vn_host_info_t* host_info = (_vn_host_info_t*) 
                    _vn_dnode_value(node);
                if (!_VN_HOST_IS_LOCAL(host_info)) {
                    break;
                }
                if (host_info->host_id == host_id) {
                    return host_info;
                }
            }
        }
    }
    return NULL;
}

/* Search all hosts in net for host_id */
_vn_host_info_t* _vn_lookup_host_info(_vn_net_info_t* net, _VN_host_t host_id)
{
    if (net && net->peer_hosts) {
        _vn_dnode_t* node;
        _vn_dlist_for(net->peer_hosts, node)
        {
            if (_vn_dnode_value(node)) {
                _vn_host_info_t* host_info = (_vn_host_info_t*) 
                    _vn_dnode_value(node);
                if (host_info->host_id == host_id) {
                    return host_info;
                }
            }
        }
    }
    return NULL;
}

int _vn_delete_host_info(_VN_net_t net_id, _VN_host_t host_id)
{
    _vn_net_info_t* net_info = _vn_lookup_net_info(net_id);
    if (net_info) {
        _vn_dnode_t* node = NULL, *temp;
        _vn_host_info_t* host_info = NULL;
        if (net_info->peer_hosts) {
            _vn_dlist_delete_for(net_info->peer_hosts, node, temp)
            {
                host_info = (_vn_host_info_t*) _vn_dnode_value(node);
                if (host_info && (host_info->host_id == host_id)) 
                {
                    _vn_dlist_delete(net_info->peer_hosts, node);
                    if (_VN_HOST_IS_LOCAL(host_info)) {
                        assert(net_info->localhosts > 0);
                        net_info->localhosts--;
                        _vn_delete_all_channels_for_host(net_info, host_info->host_id);
                    }
                    _vn_host_info_destroy(host_info);
                    return _VN_ERR_OK;
                }
            }
        }
        return _VN_ERR_HOSTID;
    }
    else return _VN_ERR_NETID;
}

int _vn_add_host_info_to_net(_vn_net_info_t* net_info, 
                             _VN_host_t host_id, _VN_guid_t guid,
                             int64_t clock_delta, uint16_t mask)
{
    if (net_info) {
        _vn_host_info_t* host_info;
        /* Make sure host doesn't already exist */
        host_info = _vn_lookup_host_info(net_info, host_id);
        if (host_info == NULL) {
            int rv;

            /* Host not in net yet, add host to list of peers */
            if (net_info->peer_hosts == NULL) {
                net_info->peer_hosts = _vn_dlist_create();
                if (net_info->peer_hosts == NULL) {
                    return _VN_ERR_NOMEM;
                }
            }

            host_info = _vn_host_info_create(guid, host_id, clock_delta, mask);
            if (host_info == NULL) {
                return _VN_ERR_NOMEM;
            }

            /* Make sure owner flag set correctly */
            if (host_id == net_info->master_host) {
                host_info->flags |= _VN_PROP_OWNER;
            }
            else {
                host_info->flags &= ~_VN_PROP_OWNER;
            }

#if _VN_INACTIVITY_TIMER
            /* Set inactivity timer if master (but only for remote hosts) */
            /* Owner has an inactivity timer to each host, and each host has
               inactivity timer back to owner */
            if (!_VN_HOST_IS_LOCAL(host_info)) {
                if (_VN_NET_IS_OWNER(net_info) || _VN_HOST_IS_OWNER(host_info))
                {
                    host_info->inactivity_timer = 
                        _vn_inactivity_timer_create(net_info->net_id, host_id);
                }
            }
#endif
            net_info->host_id_hint = host_id + 1;

            if (mask & _VN_PROP_LOCAL) {
                /* Add local hosts to front of list */
                rv = _vn_dlist_enq_front(net_info->peer_hosts, host_info);
                if (rv >= 0) {
                    net_info->localhosts++;
                }
            }
            else {
                /* Add non-local hosts to back of list */
                rv = _vn_dlist_enq_back(net_info->peer_hosts, host_info);
            }
            if (rv < 0) {
                /* Error adding host, free memory */
                _vn_host_info_destroy(host_info);
            }
            return rv;
        }
        else return _VN_ERR_HOSTID;
    }
    else return _VN_ERR_NETID;
}

int _vn_add_host_info(_VN_net_t net_id, _VN_host_t host_id, _VN_guid_t guid,
                      int64_t clock_delta, uint16_t mask)
{
    _vn_net_info_t* net_info = _vn_lookup_net_info(net_id);
    return _vn_add_host_info_to_net(net_info, host_id, guid, 
                                    clock_delta, mask);
}

void _vn_net_add_pending(_vn_net_info_t* net_info)
{
    assert(net_info);
    net_info->npending++;
}

void _vn_net_del_pending(_vn_net_info_t* net_info)
{
    assert(net_info);
    assert(net_info->npending > 0);

    net_info->npending--;

    /* Check if this net should be deleted */
    if (_VN_NET_IS_DONE(net_info) && (net_info->npending == 0)) {
        _VN_TRACE(TRACE_FINER, _VN_SG_NETCORE,
                  "_vn_net_dec_pending: No more pending messages "
                  "for net 0x%08x, deleting net\n", net_info->net_id);
        /* TODO: Can optimize by deleting net_info directly, and
                 then deleting host mapping on PC */
        _vn_disconnect_net(_VN_MSG_ID_INVALID, net_info->net_id);
    }
}

void _vn_net_mark_delete(_vn_net_info_t* net_info)
{
    assert(net_info);

    /* Marks net for deletion, deletes it if no pending packets */
    /* Not part of this net anymore, delete this net from our 
       internal tables if we don't have any outgoing packets */
    if (net_info->npending == 0) {
        _VN_TRACE(TRACE_FINER, _VN_SG_NETCORE,
                  "_vn_net_mark_delete: No more pending messages "
                  "for net 0x%08x, deleting net\n", net_info->net_id);
        _vn_disconnect_net(_VN_MSG_ID_INVALID, net_info->net_id);
    }
    else {
        net_info->flags |= _VN_PROP_DONE;
    }
}

void _vn_host_add_pending_to(_vn_net_info_t* net_info, _vn_host_info_t* host_info)
{
    assert(host_info);
    host_info->npending_to++;
}

void _vn_host_del_pending_to(_vn_net_info_t* net_info, _vn_host_info_t* host_info)
{
    assert(net_info);
    assert(host_info);
    assert(host_info->npending_to > 0);

    host_info->npending_to--;

    /* Check if this host should be deleted */
    if (_VN_HOST_IS_LEAVING(host_info) && (host_info->npending_from == 0)
        && (host_info->npending_to == 0)) {
        _VN_TRACE(TRACE_FINER, _VN_SG_NETCORE,
                  "_vn_host_dec_pending_to: No more pending messages "
                  "for %u, net 0x%08x, deleting host\n",
                  host_info->host_id, net_info->net_id);
        _vn_delete_peer(net_info->net_id, host_info->host_id);
    }
}

void _vn_host_add_pending_from(_vn_net_info_t* net_info, _vn_host_info_t* host_info)
{
    assert(host_info);
    host_info->npending_from++;
}

void _vn_host_del_pending_from(_vn_net_info_t* net_info, _vn_host_info_t* host_info)
{
    assert(net_info);
    assert(host_info);
    assert(host_info->npending_from > 0);

    host_info->npending_from--;

#if 0
    /* Check if this host should be deleted */
    if (_VN_HOST_IS_LEAVING(host_info) && (host_info->npending_from == 0)
        && (host_info->npending_to == 0)) {
        _VN_TRACE(TRACE_FINER, _VN_SG_NETCORE,
                  "_vn_host_dec_pending: No more pending messages "
                  "for %u, net 0x%08x, deleting host\n",
                  host_info->host_id, net_info->net_id);
        _vn_delete_peer(net_info->net_id, host_info->host_id);
    }
#endif
}

void _vn_host_mark_leaving(_vn_net_info_t* net_info, _vn_host_info_t* host_info)
{
    assert(host_info);
    if (host_info->npending_from == 0 && host_info->npending_to == 0) {
        _VN_TRACE(TRACE_FINER, _VN_SG_NETCORE,
                  "_vn_host_mark_leaving: No more pending messages "
                  "for %u, net 0x%08x, deleting host\n",
                  host_info->host_id, net_info->net_id);
        _vn_delete_peer(net_info->net_id, host_info->host_id);
    }
    else {
        host_info->flags |= _VN_PROP_LEAVING;
        /* host_info->flags |= _VN_PROP_NOSEND;*/
        _VN_TRACE(TRACE_FINER, _VN_SG_NETCORE,
                  "_vn_host_mark_leaving: net 0x%08x, host %u "
                  "flags 0x%08x\n", net_info->net_id,
                  host_info->host_id, host_info->flags);
    }
}


/* Returns if host_id is a local host of net */
bool _vn_islocalhost(_vn_net_info_t* net_info, _VN_host_t host_id)
{
    _vn_host_info_t* host_info;
    host_info = _vn_lookup_local_host_info(net_info, host_id);
    return (host_info)? true: false;
}

/* Host status functions */

int _vn_host_status_update_rtt(_VN_net_t net_id, _VN_host_t host_id,
                               uint32_t rtt)
{
    _vn_net_info_t *net_info = _vn_lookup_net_info(net_id);
    if (net_info) {
        _vn_host_info_t *host_info = _vn_lookup_host_info(net_info, host_id);
        if (host_info) {
            if (host_info->status.rtt == _VN_TIMESTAMP_INVALID) {
                /* RTT has not been initialized yet */
                host_info->status.rtt = rtt;
            }
            else {
                /* Calculate smoothed rtt */
                host_info->status.rtt = host_info->status.rtt*7/8 + rtt*1/8;
            }
            return _VN_ERR_OK;
        }
        else {
            return _VN_ERR_HOSTID;
        }
    }
    else {
        return _VN_ERR_NETID;
    }
}

int _vn_host_status_update_received(_VN_net_t net_id, _VN_host_t host_id,
                                    _vn_msg_t *msg, 
                                    _vn_inaddr_t addr, _vn_inport_t port)
{
    _vn_net_info_t *net_info = _vn_lookup_net_info(net_id);
    if (net_info) {
        _vn_host_info_t *host_info = _vn_lookup_host_info(net_info, host_id);
        if (host_info) {
            /* TODO: Shouldn't be doing this for every packet */
            /* Need algorithm for discovering correct addr and port */
            if (addr && port) {
#ifndef _VN_RPC_DEVICE
                _vn_set_host_mapping(net_id, host_id, addr, port);
#else
                /* Only set host mapping if needed, use this if doing
                   set host mapping for every packet is too expensive on SC.
                   Will not accommodate changes in IP as well */
                if ((host_info->status.conn_state != _VN_HOST_ST_REACHABLE) ||
                    !(host_info->flags & _VN_PROP_MAPPING_SYNCED)) {
                    int rv;
                    /* On SC, RPC call to VNProxy */
                    rv = _vn_set_host_mapping(net_id, host_id, addr, port);

                    if (rv >= 0) {
                        host_info->flags |= _VN_PROP_MAPPING_SYNCED;
                    }
                }
#endif
            }

            if (host_info->status.conn_state != _VN_HOST_ST_REACHABLE) {
                host_info->status.conn_state = _VN_HOST_ST_REACHABLE;
            }
            
            return _VN_ERR_OK;
        }
        else {
            return _VN_ERR_HOSTID;
        }
    }
    else {
        return _VN_ERR_NETID;
    }
}

int _vn_host_status_mark_unreachable(_VN_net_t net_id, _VN_host_t host_id)
{
    _vn_net_info_t *net_info = _vn_lookup_net_info(net_id);
    if (net_info) {
        _vn_host_info_t *host_info = _vn_lookup_host_info(net_info, host_id);
        if (host_info) {
            host_info->status.conn_state = _VN_HOST_ST_UNREACHABLE;
            return _VN_ERR_OK;
        }
        else {
            return _VN_ERR_HOSTID;
        }
    }
    else {
        return _VN_ERR_NETID;
    }
}

_VN_host_status* _vn_lookup_host_status(_VN_net_t net_id, _VN_host_t host_id)
{
    _vn_net_info_t *net_info = _vn_lookup_net_info(net_id);
    if (net_info) {
        _vn_host_info_t *host_info = _vn_lookup_host_info(net_info, host_id);
        if (host_info) {
            return &host_info->status;
        }
    }
    return NULL;
}

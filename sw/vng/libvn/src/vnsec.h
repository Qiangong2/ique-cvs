//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#ifndef __VN_SEC_H__
#define __VN_SEC_H__

#include "vnhs.h"

#ifdef __cplusplus
extern "C" {
#endif

#define _VN_KEY_HANDLE_INVALID             0xffffffff
#define _VN_KEY_HANDLE_IS_VALID(h)         ((h) != _VN_KEY_HANDLE_INVALID)
#define _VN_KEY_HANDLE_IS_INVALID(h)       ((h) == _VN_KEY_HANDLE_INVALID)

int  _vn_iosc_init();
int  _vn_iosc_cleanup();

_vn_cert_t* _vn_hs_get_cert_c();
_vn_cert_t* _vn_hs_get_cert_s();

int  _vn_hs_gen_hash(_vn_hash_t hash,
                      _vn_hs_info_t* hs_info, const uint8_t* buf, size_t len);
int  _vn_hs_gen_client_sig(_vn_hs_sign_c_t sig, _vn_hs_sigkey_c_t key,
                            _vn_hs_info_t* hs_info,
                            const uint8_t* buf, size_t len);
bool _vn_hs_verify_client_sig(_vn_hs_sign_c_t sig, _vn_hs_verkey_c_t key,
                              _vn_hs_info_t* hs_info,
                              const uint8_t* buf, size_t len);
int  _vn_hs_gen_server_sig(_vn_hs_sign_s_t sig, _vn_hs_sigkey_s_t key,
                           _vn_hs_info_t* hs_info,
                           const uint8_t* buf, size_t len);
bool _vn_hs_verify_server_sig(_vn_hs_sign_s_t sig, _vn_hs_verkey_s_t key,
                              _vn_hs_info_t* hs_info,
                              const uint8_t* buf, size_t len);
bool _vn_hs_verify_server_cert(_VN_guid_t ID_s,
                               _vn_hs_verkey_s_t *cert_key_s,
                               uint8_t* cert, size_t certLen);
bool _vn_hs_verify_client_cert(_VN_guid_t ID_c,
                               _vn_hs_verkey_c_t *cert_key_c,
                               uint8_t* cert, size_t certLen);
int _vn_hs_dh_generate_key(_vn_hs_dh_key_handle_t *handle, 
                           uint8_t **pCert);
int _vn_hs_compute_shared_key(_vn_hs_dh_key_handle_t *phSharedKey,
                              _vn_hs_dh_key_handle_t hYprivate,
                              _vn_hs_dh_key_handle_t hYpublic);
int _vn_hs_compute_vnkey(_vn_key_t *key, 
                         _vn_hs_dh_key_handle_t hSharedKey,
                         uint32_t rand_c, uint32_t rand_s);
int _vn_hs_import_vnkey(const uint8_t *in_keybuf, uint32_t keylen, 
                        _vn_key_t *vn_key, _vn_hs_dh_key_handle_t hSharedKey,
                        uint32_t rand_c, uint32_t rand_s, _vn_iv_t iv);
int _vn_hs_export_vnkey(uint8_t *out_keybuf, uint32_t keylen, 
                        _vn_key_t *vn_key, _vn_hs_dh_key_handle_t hSharedKey,
                        uint32_t rand_c, uint32_t rand_s, _vn_iv_t iv);
int _vn_hs_delete_keys(_vn_hs_info_t* hs_info);

int _vn_generate_vnkey(_vn_key_t *key);
int _vn_delete_vnkey(_vn_key_t *key);

#if _VN_USE_IOSC
int _vn_import_cert(uint8_t* certData, size_t certLen,
                    IOSCPublicKeyHandle signerKey,
                    IOSCPublicKeyHandle* pCertKey);

int _vn_verify_device_cert(_VN_guid_t guid,
                           uint8_t* certData, size_t certLen,
                           IOSCPublicKeyHandle* pCertKey);

bool _vn_hs_verify_cert(uint8_t* certData, size_t certLen,
                        IOSCPublicKeyHandle signerKey,
                        IOSCPublicKeyHandle* pCertKey);
#endif

#endif /* __VN_SEC_H__ */
 

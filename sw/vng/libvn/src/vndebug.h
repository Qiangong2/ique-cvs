//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#ifndef __VN_DEBUG_H__
#define __VN_DEBUG_H__

#include "vnsys.h"
#include "vnlocaltypes.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Debugging stuff */
#include "shr_trace.h"

#define _VN_TRACE _SHR_trace

/* Subgroups for VN tracing */
#define _VN_SG_MISC      _TRACE_VN, 0
#define _VN_SG_SOCKETS   _TRACE_VN, 1
#define _VN_SG_USB       _TRACE_VN, 2
#define _VN_SG_RPC       _TRACE_VN, 3
#define _VN_SG_QM        _TRACE_VN, 4
#define _VN_SG_TIMER     _TRACE_VN, 5
#define _VN_SG_API       _TRACE_VN, 6
#define _VN_SG_DISP      _TRACE_VN, 7
#define _VN_SG_NETIF     _TRACE_VN, 8
#define _VN_SG_NETCORE   _TRACE_VN, 9
#define _VN_SG_CTRL      _TRACE_VN, 10
#define _VN_SG_EVENTS    _TRACE_VN, 11
#define _VN_SG_RETX      _TRACE_VN, 12
#define _VN_SG_DEVICES   _TRACE_VN, 13
#define _VN_SG_HS        _TRACE_VN, 14
#define _VN_SG_UPNP      _TRACE_VN, 15
#define _VN_SG_NAT       _TRACE_VN, 16
#define _VN_SG_ADHOC     _TRACE_VN, 17
#define _VN_SG_IPC       _TRACE_VN, 18
#define _VN_SG_IOSC      _TRACE_VN, 19
    
#define _VN_TRACE_ON             _SHR_trace_on
#define _vn_set_trace_enable     _SHR_set_trace_enable
#define _vn_trace_level          _SHR_trace_level
#define _vn_set_trace_level      _SHR_set_trace_level
#define _vn_set_grp_trace_level  _SHR_set_grp_trace_level
#define _vn_set_sg_trace_level   _SHR_set_sg_trace_level
#define _vn_tracelevel           _SHR_trace_lev_str

#define _VN_DBG_SEND_PKT             0x1
#define _VN_DBG_RECV_PKT             0x2
#define _VN_DBG_SEND_DATA            0x3
#define _VN_DBG_RECV_DATA            0x4

#ifdef _VN_NO_TRACE

#define _VN_PRINT(fmt, ...)
#define _vn_trace(level, fmt, ...)

#define _vn_inet_ntop(src, dst, cnt)

#define _vn_dbg_dump_buf(fp,  buf, buflen)
#define _vn_dbg_write_buf(fp,  buf, buflen)
#define _vn_dbg_print_pkt(fp,  buf, buflen)
#define _vn_dbg_save_buf(type,  buf, buflen)
#define _vn_dbg_write_timed_buf(fp, t, buf, buflen)

#define _vn_print_stats(fp)
#define _vn_print_send_stats(fp, data)
#define _vn_print_recv_stats(fp, data)
#define _vn_print_timer(fp, data)
#define _vn_print_seq_no(fp, data)
#define _vn_print_send_info(fp, data)
#define _vn_print_retx_info(fp, data)
#define _vn_print_msg_hdr(fp, data) 
#define _vn_print_host_conn(fp, data)
#define _vn_print_port_info(fp, data)
#define _vn_print_host_info(fp, data)
#define _vn_print_net_info(fp, data)
#define _vn_print_net_conn(fp, data)
#define _vn_print_event(fp, data)
#define _vn_print_channel(fp, data)
#define _vn_print_hs_info(fp, data)
#define _vn_print_device(fp, data)

#define _vn_dbg_set_pkt_logs(send_fp, recv_fp)
#define _vn_dbg_set_data_logs(send_fp, recv_fp)
#define _vn_dbg_setup_logs(sent_logfile, recv_logfile)
#define _vn_dbg_close_logs()

#define _vn_dump_net_tables(fp, handle)
#define _vn_dump_hs_tables(fp)
#define _vn_dump_devices(fp)
#define _vn_dump_state(fp, handle)
#define _vn_dump_netif(fp, handle)

#define _vn_stat_clear(stat)
#define _vn_stat_add(stat, value)
#define _vn_stat_print(fp, stat, name)

#ifdef UPNP
#define _vn_upnp_dump_nats(fp)
#define _vn_upnp_dump_devices(fp)
#define _vn_upnp_dump_state(fp)
#endif

#else

#define _VN_PRINT printf

void _vn_trace(int level, const char* fmt, ...);

const char* _vn_inet_ntop(const void *src, char *dst, size_t cnt);

void _vn_dbg_dump_buf(FILE* fp, const uint8_t* buf, size_t buflen);
void _vn_dbg_write_buf(FILE* fp, const uint8_t* buf, size_t buflen);
void _vn_dbg_print_pkt(FILE* fp, const uint8_t* buf, size_t buflen);
void _vn_dbg_save_buf(int type, const uint8_t* buf, size_t buflen);
void _vn_dbg_write_timed_buf(FILE* fp, uint64_t t, 
                             const uint8_t* buf, size_t buflen);

void _vn_print_stats(FILE* fp);
void _vn_print_send_stats(FILE* fp, void* data);
void _vn_print_recv_stats(FILE* fp, void* data);
void _vn_print_timer     (FILE* fp, void* data);
void _vn_print_seq_no    (FILE* fp, void* data);
void _vn_print_send_info (FILE* fp, void* data);
void _vn_print_retx_info (FILE* fp, void* data);
void _vn_print_msg_hdr   (FILE* fp, void* data); 
void _vn_print_host_conn (FILE* fp, void* data);
void _vn_print_port_info (FILE* fp, void* data);
void _vn_print_host_info (FILE* fp, void* data);
void _vn_print_net_info  (FILE* fp, void* data);
void _vn_print_net_conn  (FILE* fp, void* data);
void _vn_print_event     (FILE* fp, void* data);
void _vn_print_channel   (FILE* fp, void* data);
void _vn_print_hs_info   (FILE* fp, void* data);
void _vn_print_device    (FILE* fp, void* data);

void _vn_dbg_set_pkt_logs(FILE* send_fp, FILE* recv_fp);
void _vn_dbg_set_data_logs(FILE* send_fp, FILE* recv_fp);
bool _vn_dbg_setup_logs(const char* sent_logfile, const char* recv_logfile);
void _vn_dbg_close_logs();

void _vn_dump_net_tables(FILE* fp, int handle);
void _vn_dump_hs_tables(FILE* fp);
void _vn_dump_devices(FILE* fp);
void _vn_dump_state(FILE* fp, int handle);
void _vn_dump_netif(FILE* fp, int handle);

void _vn_stat_clear(_vn_stat_t* stat);
void _vn_stat_add(_vn_stat_t* stat, uint64_t value);
void _vn_stat_print(FILE* fp, _vn_stat_t* stat, char* name);

#ifdef UPNP
void _vn_upnp_dump_nats(FILE* fp);
void _vn_upnp_dump_devices(FILE* fp);
void _vn_upnp_dump_state(FILE* fp);
#endif

#endif

#ifdef  __cplusplus
}
#endif

#endif /* __VN_DEBUG_H__ */
 

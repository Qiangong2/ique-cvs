//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#ifndef __VN_LOCALTYPES_H__
#define __VN_LOCALTYPES_H__

#include "vntypes.h"

#if !defined(_VN_RPC_PROXY) 
#define _VN_USE_IOSC                        1
#endif

#if _VN_USE_IOSC
#include "csl.h"
#include "sc/iostypes.h"
#include "iosc.h" /* From NC Tree */
#endif

#ifdef __cplusplus
extern "C" {
#endif

#define ntohll(x) ( ( (uint64_t) ntohl ((uint32_t)( x )) << 32 ) |  \
                     ntohl ((uint32_t)(x >> 32))) 
#define htonll(x) ntohll(x)

#define MIN(a,b)  ((a) < (b)? (a):(b)) 
#define MAX(a,b)  ((a) > (b)? (a):(b)) 

/* Max number of queued packets for sending per port */
#define _VN_QM_MAX_PKTS_PER_PORT           96
#ifdef _SC
#define _VN_QM_MAX_LOSSY_PER_PORT          64
#define _VN_QM_MAX_RELIABLE_PER_PORT       64
#else
#define _VN_QM_MAX_LOSSY_PER_PORT          96
#define _VN_QM_MAX_RELIABLE_PER_PORT       96
#endif

/* Max number of devices for one vnproxy to support */
#define _VN_PROXY_MAX_DEVICES  8

/* AES constants */
#define _VN_AES_ALIGNMENT      16
#define _VN_AES_SIZE           16
#define _VN_RND_AES_SIZE(x)    (((x) + _VN_AES_SIZE - 1) & ~(_VN_AES_SIZE - 1))
#define _VN_AES_KEY_SIZE       16
#define _VN_AES_IV_SIZE        16

/* HMAC constants */
#define _VN_MAC_ALIGNMENT     64
#define _VN_MAC_BLOCK_SIZE    64
#define _VN_RND_MAC_BLOCK_SIZE(x)    \
   (((x) + _VN_MAC_BLOCK_SIZE - 1) & ~(_VN_MAC_BLOCK_SIZE - 1))

#define _VN_AUTHCODE_SIZE         10

#define _VN_NET_TABLE_SIZE        16

/* Cert constants */
#define _VN_CERT_ALIGNMENT    64

/* Internet address and port */
typedef uint32_t _vn_inaddr_t;
typedef uint16_t _vn_inport_t;

#define _VN_INADDR_INVALID        0
#define _VN_INPORT_INVALID        0

#define _VN_MAX_INTERFACES        8

typedef uchar_t _vn_authcode_t[_VN_AUTHCODE_SIZE];

/* TODO: VN key should be composed of a encryption key and a hash key */
#if _VN_USE_IOSC

#ifdef _SC
#define _VN_IOSC_ALIGN_NEEDED     1
#else
#define _VN_IOSC_ALIGN_NEEDED     0
#endif

typedef struct {
    IOSCSecretKeyHandle Ak;
    IOSCSecretKeyHandle Hk;
} _vn_key_t;
typedef IOSCHash _vn_hash_t;

#define _VN_HASH_KEY_SIZE       20
#define _VN_KEY_LENGTH          (_VN_AES_KEY_SIZE + _VN_HASH_KEY_SIZE)

#define _VN_ENC_AES_KEY_SIZE    (_VN_RND_AES_SIZE(_VN_AES_KEY_SIZE))
#define _VN_ENC_HASH_KEY_SIZE   (_VN_RND_AES_SIZE(_VN_HASH_KEY_SIZE))
#define _VN_ENC_KEY_LENGTH      (_VN_ENC_AES_KEY_SIZE + _VN_ENC_HASH_KEY_SIZE)

#else

#ifndef SHA1_DIGESTSIZE
#define SHA1_DIGESTSIZE         20
#endif

typedef uchar_t _vn_key_t[_VN_AES_KEY_SIZE];
typedef uchar_t _vn_hash_t[SHA1_DIGESTSIZE];

#define _VN_KEY_LENGTH          _VN_AES_KEY_SIZE
#define _VN_ENC_KEY_LENGTH      (_VN_RND_AES_SIZE(_VN_KEY_LENGTH))

#endif

typedef uchar_t _vn_iv_t[_VN_AES_IV_SIZE];

typedef void (*_vn_free_func_t)(void *ptr);

/* Req ID macros */
#define _VN_REQID(id1, id2)                   \
        (((uint64_t) (id1) << 32) | (id2))
#define _VN_REQID_GET_ID1(request_id)         \
        ((uint32_t) ((request_id) >> 32))
#define _VN_REQID_GET_ID2(request_id)         \
        ((uint32_t) ((request_id) & 0xffffffff))

/* Data Structures */

/* Buffer with length information */
typedef struct
{
    uint16_t          max_len;  /* Allocated length */
    uint16_t          len;      /* Actual length */
    uint8_t           buf[];
} _vn_buf_t;

#define _VN_PKT_TYPE_VN                0x01
#define _VN_PKT_TYPE_USBRPC            0x02

/* Buffer that starts at a certain offset (used when alignment is needed) */
typedef struct
{
    uint16_t          max_len;  /* Allocated length */
    uint16_t          len;      /* Actual length (starting from offset) */

    uint16_t          offset;   /* Offset where data actually starts */
    uint8_t           type;     /* Format of buffer data */
    uint8_t           reserved;

    uint8_t*          buf; /* Pointer to actual buffer of data */
} _vn_wbuf_t;

typedef struct
{
    uint32_t          netmask;
    _VN_net_t         min;
    _VN_net_t         max;
    _VN_net_t         cur;
} _vn_net_range_t;
 
/* Send statistics */
typedef struct
{
    uint32_t         total_pkts;   
    uint32_t         total_bytes;
    uint32_t         dropped_pkts;  /* Dropped due to error */
    uint32_t         sent;          /* Number of packets sent */
    uint32_t         rsent;         /* Reliable (sent) */
    uint32_t         lsent;         /* Lossy (sent) */
    uint32_t         retx;          /* retransmissions sent */ 
    uint32_t         noretxbuf;     /* Can't buffer */
    uint32_t         self;          /* For self */
    uint32_t         other;         /* For other host */
    uint32_t         acks;          /* acks sent */
    uint32_t         nacks;         /* nacks sent */
    uint32_t         syns;          /* syns sent */
    uint32_t         keep_alive;    /* keep alives sent */
    uint32_t         zero_ctrl;     /* 0 length control pkt */
    uint32_t         empty;         /* Empty packet (no UDP data) */
    uint32_t         handshake;     /* Handshake packet */
    uint32_t         unknown_net;   /* Net unknown */
    uint32_t         unknown_host;  /* Host not in net */
    uint32_t         blocked;       /* Host is blocked */
    uint32_t         nomem;         /* No more memory */
#if defined(_VN_RPC_PROXY) || defined(_VN_RPC_DEVICE)
    uint32_t         rpc_req;       /* RPC requests sent */
    uint32_t         rpc_resp;      /* RPC responses sent */
#endif
} _vn_send_stats_t;

/* Receive statistics */
typedef struct
{
    uint32_t         total_pkts;
    uint32_t         total_bytes;   
    uint32_t         oversized_pkts;/* Bigger than maximum size packet */
    uint32_t         short_pkts;    /* Length too small */
    uint32_t         dropped_pkts;  /* Dropped packets */
    uint32_t         unknown_net;   /* Net unknown */
    uint32_t         unknown_from;  /* From Host not in net */
    uint32_t         unknown_to;    /* Not for me */
    uint32_t         unknown_port;  /* Port unknown */
    uint32_t         inactive_port; /* Port inactive */
    uint32_t         zero_ctrl;     /* 0 length control pkt */
    uint32_t         empty;         /* Empty packet (no UDP data) */
    uint32_t         handshake;     /* Handshake packet */
    uint32_t         badversion;    /* Bad version */
    uint32_t         rinseq;        /* Reliable in sequence */
    uint32_t         routseq;       /* Reliable out of sequence */
    uint32_t         rdup;          /* Reliable duplicate */
    uint32_t         linseq;        /* Lossy, in sequence */
    uint32_t         loutseq;       /* Lossy, out of sequence */
    uint32_t         nobufs;        /* No more memory buffers */
    uint32_t         acks;          /* Received acks */
    uint32_t         nacks;         /* Received nacks */
    uint32_t         syns;          /* Received syns */
    uint32_t         bad_auth;      /* Cannot authenticate packet */
    uint32_t         bad_gseq;      /* Bad gseq number */
    uint32_t         nomem;         /* No more memory */
#if defined(_VN_RPC_PROXY) || defined(_VN_RPC_DEVICE)
    uint32_t         rpc_req;       /* RPC requests received */
    uint32_t         rpc_resp;      /* RPC responses received */
#endif
} _vn_recv_stats_t;

typedef struct {
    uint64_t sum;
    uint64_t cnt;
    uint64_t max;
    uint64_t min;
    uint64_t last;
    uint64_t ave;
} _vn_stat_t;

extern _vn_send_stats_t _vn_send_stats;
extern _vn_recv_stats_t _vn_recv_stats;

#ifdef  __cplusplus
}
#endif

#endif /* __VN_LOCALTYPES_H__ */
 

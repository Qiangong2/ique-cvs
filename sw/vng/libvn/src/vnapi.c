//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnlocal.h"

/***** Functions for managing call IDs */

/* Returns a unique call id */
_VN_callid_t _vn_call_id = 0;

_VN_callid_t _vn_new_call_id()
{
    if (_vn_call_id == 0) {
        _vn_call_id = _vn_rand();
        _VN_TRACE(TRACE_FINE, _VN_SG_MISC,
                  "Starting _vn_call_id at %d\n", _vn_call_id);
    }
    else { 
        _vn_call_id++;
    }

    if (_vn_call_id <= 0) _vn_call_id = 1;
    return _vn_call_id;
}

_VN_callid_t _vn_get_call_id()
{
    return _vn_call_id;
}

/* convert guid to external representation */
#define _VN_CONVERT_GUID(guid)                                            \
{                                                                         \
    if ((guid)->device_type != _VN_DEVICE_TYPE_UNKNOWN) {                 \
        (guid)->device_type = (guid)->device_type & _VN_DEVICE_TYPE_MASK; \
    }                                                                     \
}

/* API functions */

/* Time functions */
_VN_time_t _VN_get_timestamp()
{
    _VN_time_t timestamp;
    assert(_vn_initialized());
    _vn_net_lock();
    /* Call to platform specific implementation */
    timestamp = _vn_get_timestamp();
    _vn_net_unlock();
    return timestamp;
}

_VN_time_t _VN_conv_timestamp(_VN_addr_t addr,
                              _VN_time_t remote_timestamp)
{
    _VN_time_t local_timestamp;
    assert(_vn_initialized());
    _vn_net_lock();
    local_timestamp = _vn_conv_timestamp(_VN_addr2net(addr), 
                                         _VN_addr2host(addr),
                                         remote_timestamp);
    _vn_net_unlock();
    return local_timestamp;
}

/* Performs clock calibration */
int _VN_calibrate_clock(_VN_addr_t addr)
{
    int rv;
    assert(_vn_initialized());
    _vn_net_lock();
    rv = _vn_calibrate_clock(_VN_addr2net(addr), _VN_addr2host(addr));
    _vn_net_unlock();
    return rv;
}

/* Events */

void _VN_cancel_get_events()
{
    assert(_vn_initialized());
    _vn_cancel_wait_event();
}

int _VN_get_events(void* evt_buf, int evt_size, int timeout)
{
    assert(_vn_initialized());
    return _vn_get_events(evt_buf, evt_size, 0, timeout);
}

/* Retrieves message associated with an event */
int  _VN_get_event_msg(const void* handle, void* msg, uint16_t msg_len)
{
    int len = 0;
    _vn_buf_t* pkt;

    assert(_vn_initialized());

    if (handle == NULL) return _VN_ERR_HANDLE;
    if ((msg == NULL) && (msg_len > 0)) return _VN_ERR_LENGTH;

    /* Get event message - entire buffer is the message */
    pkt = _vn_mhp_remove_pkt(handle);
    if (pkt == NULL) return _VN_ERR_HANDLE;

    if (msg) {
        len = MIN(msg_len, ntohs(pkt->len));
        memcpy(msg, pkt->buf, len);
    }

    /* free packet */
    _vn_free_msg_buffer(pkt);

    return len;
}

/* Virtual Network Status */
uint32_t _VN_get_status()
{
    uint32_t status = 0;
    if (!_vn_initialized()) {
        return status;
    }
    status |= _VN_ST_INITIALIZED;
    
    _vn_net_lock();
    status |= _vn_netif_get_status(_vn_netif_get_instance());
    _vn_net_unlock();
    
    return status;        
}

/* Virtual Network Initialization */
int _VN_init(const _VN_config_t* config)
{
    int rv;
    if (config) {
#ifdef UPNP
        if (config->upnp_port) {
            _vn_upnp_set_port(config->upnp_port);
        }
#endif
        if (config->connect_timeout) {
            if (config->connect_timeout < _VN_MIN_CONFIG_TIMEOUT ||
                config->connect_timeout > _VN_MAX_CONFIG_TIMEOUT) {
                return _VN_ERR_INVALID;
            }
            _vn_hs_set_timeout(config->connect_timeout);
        }

        if (config->peer_timeout) {
            if (config->peer_timeout < _VN_MIN_CONFIG_TIMEOUT ||
                config->peer_timeout > _VN_MAX_CONFIG_TIMEOUT) {
                return _VN_ERR_INVALID;
            }
            _vn_inactivity_set_timeout(config->peer_timeout);
        }

        if (config->retx_timeout) {
            if (config->retx_timeout < _VN_MIN_CONFIG_TIMEOUT ||
                config->retx_timeout > _VN_MAX_CONFIG_TIMEOUT) {
                return _VN_ERR_INVALID;
            }
            _vn_retx_set_timeout(config->retx_timeout);
            _vn_recv_set_timeout(config->retx_timeout);
        }

        rv = _vn_init_common(NULL, config->vn_port_min, config->vn_port_max);
    } else {
        rv = _vn_init_common(NULL, _VN_CLIENT_PORT, _VN_CLIENT_PORT);
    }
    return rv;
}

/* 
 * Deletes all VNs and reset all network buffers if reset_all is true.
 * If reset_all is false, deletes all VNs and network buffers 
 * except for daemon VNs 
 */
void _VN_reset(bool reset_all)
{
    assert(_vn_initialized());
    _vn_net_lock();
    _vn_reset(reset_all);
    _vn_net_unlock();
}

/* 
 * Marks the specified VN as "daemon" and not to be cleared upon _VN_reset
 * Return values:
 *  _VN_ERR_NETID:      Invalid net id.
 *  _VN_ERR_OK:         Success
 */
int _VN_set_daemon_net(_VN_net_t net_id)
{
    int rv;
    assert(_vn_initialized());
    _vn_net_lock();
    rv = _vn_net_set_daemon(net_id, true);
    _vn_net_unlock();
    return rv;
}

/* 
 * Establishes VN with the virtual network server 
 *
 * The parameters msg and msglen can be used to pass an optional
 * application specific buffer to the VN server.  Typically,
 * information allowing the VN server to decide whether to accept the
 * connection or not.
 *
 * Return values:
 * If handshake succesfully initiated, returns a positive call id that is used 
 *   to identify the _VN_connection_accepted_event or _VN_err_event
 *   triggered by this call.
 * 
 * Possible error codes:
 *  _VN_ERR_SERVER:      Cannot resolve vn_server.
 *  _VN_ERR_INVALID:     Other input parameters are invalid
 *  _VN_ERR_NOMEM:       No more memory
 */
int _VN_sconnect(const char* vn_server, uint16_t udp_port,
                 const void* msg, uint16_t msglen)
{
    int rv, call_id;
    _vn_inaddr_t ipaddr;
    _VN_guid_t guid;

    assert(_vn_initialized());

    if ((msglen > 0) && (msg == NULL)) {
        return _VN_ERR_INVALID;
    }

    _vn_net_lock(); 

    ipaddr = _vn_netif_getaddr(vn_server);
    call_id = _vn_new_call_id();
    if (ipaddr != _VN_INADDR_INVALID) {
        _vn_guid_set_invalid(&guid);
        rv = _vn_hs_connect(guid, true, call_id, ipaddr, udp_port,
                            _VN_NET_DEFAULT, msg, msglen);
    } else {
        rv = _VN_ERR_SERVER;
    }

    _vn_net_unlock();

    if (rv >= 0) { 
        rv = call_id;
    }
    return rv;
}

/* Virtual Network Management */

/* Creates a new virtual network and make the calling host the owner
 * is_local - If set to true, assign a virtual network ID from the local 
 *            network ID space.  Otherwise, request a globally unique
 *            virtual network ID from the virtual network server.
 *
 * Return values:
 * If successful, returns a positive call id that is used to identify the
 *                _VN_new_network_event or _VN_err_event 
 *                triggered by this call.
 *
 * Possible error codes:
 *  _VN_ERR_SERVER:      vn_server was invalid
 *  _VN_ERR_NETID:       unable to get a new netid
 *  _VN_ERR_NOMEM:       No more memory
 */
int _VN_new_net(uint32_t netmask, bool is_local, _VN_addr_t vn_server,
                const void* msg, uint16_t msglen)
{
    int rv, call_id;

    /* TODO: Check netmask */
    assert(_vn_initialized());
    _vn_net_lock();
    call_id = _vn_new_call_id();
    if (is_local || _vn_isserver()) {
        /* Game devices can create local VN ids independent of the server */
        _VN_net_t new_net_id = _vn_net_newid(netmask, is_local); 
        if (new_net_id == _VN_NET_DEFAULT) {
            /* Couldn't get a new net id */
            rv = _VN_ERR_NETID;
        }
        else {
            rv = _vn_create_net(call_id, new_net_id);
        }
    }
    else {
        _VN_net_t server_net_id;
        _VN_host_t server_host_id;

        /* Okay, there is probably a server that I'm connected to
           from whom I can get a good global net id */

        server_net_id = _VN_addr2net(vn_server);
        server_host_id = _VN_addr2host(vn_server);

        /* Talk to server to get new global net id */
        rv = _vn_net_gid_request(call_id, netmask,
                                 server_net_id, server_host_id,
                                 msg, msglen);
        if ((rv == _VN_ERR_NETID) || (rv == _VN_ERR_HOSTID)) {
            rv = _VN_ERR_SERVER;
        }
    }

    _vn_net_unlock();

    if (rv >= 0) { 
        rv = call_id;
    }
    return rv;
}

/* 
 * Allow or disallow new game devices to connect to the specified VN 
 *
 * Return values:
 * If successful, returns a positive call id that is used to identify the
 *                _VN_new_connection_event or _VN_connection_request_event
 *                triggered by this call.
 *
 * Possible error codes:
 *  _VN_ERR_NETID:       Unknown net
 *  _VN_ERR_NOT_OWNER:   Not owner of the net
 */
int _VN_listen_net(_VN_net_t net_id, bool flag)
{
    int rv;
    _VN_callid_t call_id;

    assert(_vn_initialized());
    _vn_net_lock();
    call_id = _vn_new_call_id();
    rv = _vn_hs_listen_net(call_id, net_id, flag);
    /* TODO: Setup firewall/NAT router */
    _vn_net_unlock();
    if (rv == _VN_ERR_OK) { 
        rv = call_id;
    }
    return rv;
}

/* Returns net id associated with the request_id 
 * (request_id should be from a _VN_connection_request_event) */
int _VN_lookup_connection_request(uint64_t request_id, _VN_net_t *pNet)
{
    _vn_hs_info_t* hs_info;
    int rv;

    assert(_vn_initialized());
    _vn_net_lock();
    hs_info = _vn_hs_lookup_request(request_id);
    if (hs_info == NULL) {
        rv = _VN_ERR_NOTFOUND;
    } else {
        if (pNet) { *pNet = hs_info->vninfo.net_id; }
        rv = _VN_ERR_OK;
    }
    _vn_net_unlock();
    return rv;
}

/* 
 * Accept or reject connection request request_id 
 * (request_id should be from a _VN_connection_request_event)
 *
 * Return values:
 * If successful, returns _VN_ERR_OK
 *
 * Possible error codes:
 *  _VN_ERR_NOTFOUND:    Invalid request_id
 *  _VN_ERR_INVALID:     Other input parameters (msg, msglen) were invalid
 *  _VN_ERR_NETID:       request_id corresponds to an unknown net
 *  _VN_ERR_NOT_OWNER:   Not owner of the net
 */
int _VN_accept(uint64_t request_id, bool flag, 
               const void* msg, uint16_t msglen)
{
    int rv;

    assert(_vn_initialized());
    if ((msglen > 0) && (msg == NULL)) {
        return _VN_ERR_INVALID;
    }

    _vn_net_lock();
    rv = _vn_hs_accept(request_id, flag, msg, msglen);
    _vn_net_unlock();
    return rv;    
}

/* Accept or reject new net request request_id */
/* (request_id should be from a  _VN_new_net_request_event) */
int _VN_accept_net(uint64_t request_id, uint32_t netmask,
                   bool flag, const void* msg, uint16_t msglen)
{
    int rv;
    _VN_addr_t to_addr;
    _vn_ctrl_msgid_t msg_id;

    assert(_vn_initialized());
    if ((msglen > 0) && (msg == NULL)) {
        return _VN_ERR_INVALID;
    }

    to_addr = _VN_REQID_GET_ID1(request_id);
    msg_id = _VN_REQID_GET_ID2(request_id);
    
    _vn_net_lock();
    rv = _vn_net_gid_accept(msg_id, netmask,
                            _VN_addr2net(to_addr), _VN_addr2host(to_addr),
                            flag, msg, msglen);
    _vn_net_unlock();
    return rv;    
}

/* 
 * Attempts to connect to the game device with guid on net net_id.
 * If net_id is not _VN_NET_DEFAULT, the game device should be the 
 * owner of the net.
 *
 * The parameters msg and msglen can be used to pass an optional
 * application specific buffer to the owner of the net.  Typically,
 * information allowing the owner to decide whether to accept the
 * connection or not.
 *
 * If vn_server's host id is _VN_HOST_INVALID, then vn_server 
 * is not contacted to determine IP/port of game device.
 * Otherwise, the vn_server will be contacted if the information
 * was not found in our internal device tables.
 *
 * Return values:
 * If successful, returns a positive call id that is used to identify the
 *                _VN_connection_accepted_event or _VN_err_event
 *                triggered by this call.
 *
 * Possible error codes:
 *  _VN_ERR_INVALID:      One of the input parameters were invalid
 *  _VN_ERR_NOTFOUND:     No entry was found for device guid, and
 *                        the VN server was not contacted.
 *  _VN_ERR_SERVER:       No entry found for device guid, and
 *                        we were unable to contact the VN server
 *                        (vn_server refered to an unknown VN address) 
 *  _VN_ERR_NOMEM:        No more memory
 */
int _VN_connect(_VN_guid_t guid, _VN_net_t net_id, _VN_addr_t vn_server,
                const void* msg, uint16_t msglen)
{
    int rv, call_id;
    _vn_dlist_t* addr_list;

    _VN_net_t server_net_id;
    _VN_host_t server_host_id;

    assert(_vn_initialized());
    if ((msglen > 0) && (msg == NULL)) {
        return _VN_ERR_INVALID;
    }

    _vn_net_lock();
    call_id = _vn_new_call_id();
    server_net_id = _VN_addr2net(vn_server);
    server_host_id = _VN_addr2host(vn_server);

    /* Look up IP/port from guid + net_id, 
       ask vn_server if necessary */

    rv = _vn_get_connect_info_list(call_id, guid, net_id,
                                   server_net_id, server_host_id,
                                   &addr_list);

    if (rv >= 0) {
        /* Got IP/port immediately, connect now */
        rv = _vn_hs_connect_list(guid, true, call_id, addr_list,
                                 net_id, msg, msglen);
    }
    else if (rv == _VN_ERR_PENDING) {
        /* Contacting server for IP/Port */
        /* Prepare new handshake session so msg and msglen saved for
           when we actually get the IP/Port and can start the handshake */
        rv = _vn_hs_client_new_session(call_id, net_id, msg, msglen, NULL);
    }
    _vn_net_unlock();

    if ((rv >= 0) || (rv == _VN_ERR_PENDING)) { 
        rv = call_id;
    }
    return rv;
}

/* Leaves a net */
int _VN_leave_net(_VN_addr_t addr, _VN_addr_t vn_server)
{
    int rv, call_id;
    _VN_net_t net_id, server_net_id;
    _VN_host_t host_id, server_host_id;

    assert(_vn_initialized());
    _vn_net_lock();
    call_id = _vn_new_call_id();

    net_id = _VN_addr2net(addr);
    host_id = _VN_addr2host(addr);

    server_net_id = _VN_addr2net(vn_server);
    server_host_id = _VN_addr2host(vn_server);

    rv = _vn_leave_net(call_id, net_id, host_id,
                       server_net_id, server_host_id);

    _vn_net_unlock();
    if (rv == _VN_ERR_PENDING) { 
        rv = call_id;
    }
    return rv;
}

/* Host Management */

/* Retrieves the list of local hosts for a given VN */
int _VN_get_local_hostids(_VN_net_t net_id,
                          _VN_host_t* hosts, uint8_t hosts_size)
{
    int rv;

    assert(_vn_initialized());
    if (hosts == NULL) return _VN_ERR_INVALID;
    
    _vn_net_lock();
    rv = _vn_get_local_hostids(net_id, hosts, hosts_size);
    _vn_net_unlock();

    return rv;
}

/* Returns the host id of the VN owner */
_VN_host_t _VN_get_owner_host_id(_VN_net_t net_id)
{ 
    _VN_host_t host;

    assert(_vn_initialized());
    _vn_net_lock();
    host = _vn_get_owner(net_id);
    _vn_net_unlock();

    return host;
}

/* TODO: Do not count hosts in the midst of leaving or joining */
int _VN_get_vn_size(_VN_net_t net_id)
{
    _vn_net_info_t* vn_net;
    int rv;

    assert(_vn_initialized());
    _vn_net_lock();
    vn_net = _vn_lookup_net_info(net_id);
    if (vn_net != NULL) {
        rv = _vn_get_size(vn_net);
    }
    else {
        rv = _VN_ERR_NETID;
    }
    _vn_net_unlock();

    return rv;
}

/* Retrieves list of hosts belonging to a given VN */
int _VN_get_vn_hostids(_VN_net_t net_id, _VN_host_t* hosts, int hosts_size)
{
    int rv;

    assert(_vn_initialized());
    if (hosts == NULL) return _VN_ERR_INVALID;
    
    _vn_net_lock();
    rv = _vn_get_hostids(net_id, hosts, hosts_size);
    _vn_net_unlock();

    return rv;
}

/* Retrieves information about a host, such as latency and connection status */
int _VN_get_host_status(_VN_addr_t addr, _VN_host_status* status)
{
    int rv;
    assert(_vn_initialized());
    _vn_net_lock();
    rv = _vn_get_host_status(addr, status);
    _vn_net_unlock();
    return rv;
}

int _VN_get_host_ip(_VN_addr_t addr, uint32_t* ip)
{
    int rv;
    assert(_vn_initialized());
    if (ip == NULL) return _VN_ERR_INVALID;
    _vn_net_lock();
    rv = _vn_get_host_mapping(_VN_addr2net(addr), _VN_addr2host(addr),
                              ip, NULL);
    _vn_net_unlock();
    return rv;
}

/* Host discovery */

/* Start or stops discovery of other hosts on the LAN */
int _VN_discover_hosts(bool flag)
{
    int rv;
    assert(_vn_initialized());
    _vn_net_lock();
    rv = _vn_netif_discover_hosts(flag);
    _vn_net_unlock();
    return rv;
}

/* Queries remote device for device id and what services it is offering */
/* Also adds the remote device to a list of hosts to periodically query */
/* The port number should be the TCP port number used by the remote device
 * for UPNP */
int _VN_query_host(const char* hostname, uint16_t port)
{
    int rv;
    assert(_vn_initialized());
    _vn_net_lock();
    rv = _vn_netif_query_host(_vn_netif_get_instance(), hostname, port);
    _vn_net_unlock();
    return rv;
}

/* Clears list of hosts to query */
int _VN_clear_query_list()
{
    int rv;
    assert(_vn_initialized());
    _vn_net_lock();
    /* TODO: Is there a race condition (very unlikely) where we request 
             the query list be cleared, and new services are added before 
             the clearing process is completed?  */
    rv = _vn_netif_clear_query_list(_vn_netif_get_instance());
    /* Clears services from queried devices (but not LAN hosts) */
    _vn_clear_services(_VN_DEVICE_PROP_QUERIED, _VN_DEVICE_PROP_LAN);
    _vn_clear_device_flag(_VN_DEVICE_PROP_QUERIED);
    _vn_net_unlock();
    return rv;
}

/* 
 * Registers service for peer discovery.  If a service with service_id
 * is already registered, then the service is updated with the new information.
 *
 * The parameters msg and msglen can be used to provide an optional
 * application specific buffer describing the service.
 *
 * If public is true, then the registered service will be announced
 * during peer discovery.  Interested hosts will be notified of new service.
 *
 * If public is false, then the registered service will NOT be announced
 * during peer discovery.  If the service used to be public, 
 * interested hosts will be notified that the service is no longer available.
 *
 * Return values:
 * If successful, returns _VN_ERR_OK.
 *
 * Possible error codes:
 *  _VN_ERR_INVALID:      One of the input parameters were invalid
 *  _VN_ERR_NOMEM:        No more memory
 */
int _VN_register_service(uint32_t service_id, bool public,
                         const void* msg, uint16_t msglen)
{
    int rv;
    uint8_t flags;
    _VN_guid_t guid;
    _vn_service_t* service;
    bool public_old;

    assert(_vn_initialized());
    if ((msglen > 0) && (msg == NULL)) {
        return _VN_ERR_INVALID;
    }

    flags = public? _VN_SERVICE_PROP_PUBLIC: 0;

    _vn_net_lock();

    guid = _vn_get_myguid();

    /* Determine if the service used to be public */
    service = _vn_lookup_service(guid, service_id);
    public_old = (service)? _VN_SERVICE_IS_PUBLIC(service): false;

    /* Replace service */
    rv = _vn_add_service(guid, service_id, flags,
                         msg, msglen);

    /* Send message updating hosts of new service */
    if (rv == _VN_ERR_OK) {
        if (public) {
#ifdef UPNP
            _vn_upnp_notify_service_added(guid, service_id);
#endif

#ifdef _VN_RPC_DEVICE
            _vn_netif_add_service(guid, service_id);
#endif
        }
        else {
            if (public_old) {
                /* Public service now private */
#ifdef UPNP
                _vn_upnp_notify_service_removed(guid, service_id);
#endif

#ifdef _VN_RPC_DEVICE
                _vn_netif_remove_service(guid, service_id);
#endif
            }
        }
    }

    _vn_net_unlock();
    return rv;
}

/* 
 * Unregisters service for peer discovery.
 *
 * The registered service is removed and interested hosts will be notified 
 * that the service is no longer available.
 *
 * Return values:
 * If successful, returns _VN_ERR_OK.
 *
 * Possible error codes:
 *  _VN_ERR_NOTFOUND:     Unknown service_id
 *  _VN_ERR_NOMEM:        No more memory
 */
int _VN_unregister_service(uint32_t service_id)
{
    int rv;
    _VN_guid_t guid;

    assert(_vn_initialized());
    _vn_net_lock();
    guid = _vn_get_myguid();
    rv = _vn_remove_service(guid, service_id);

    if (rv == _VN_ERR_OK) {
#ifdef _VN_RPC_DEVICE
        _vn_netif_remove_service(guid, service_id);
#endif

#ifdef UPNP
        _vn_upnp_notify_service_removed(guid, service_id);
#endif
    }

    _vn_net_unlock();
    return rv;
}

/** 
 * Returns number of devices matching one or more 
 * of the criterions in the mask 
 */  
int _VN_get_device_count(uint8_t mask)
{
    int rv;
    assert(_vn_initialized());
    _vn_net_lock();
    rv = _vn_get_device_count(mask);
    _vn_net_unlock();
    return rv;
}

/**
 *  Returns list of guids matching one or more of the criterions in the mask 
 *  The number of matching devices is returned (can be larger than ndevices) 
 */
int _VN_get_guids(uint8_t mask, _VN_guid_t* devices, uint16_t ndevices)
{
    int i, rv;
    assert(_vn_initialized());
    _vn_net_lock();
    rv = _vn_get_guids(mask, devices, ndevices);
    _vn_net_unlock();
    for (i = 0; i < rv; i++) {
        _VN_CONVERT_GUID(&devices[i]);
    }
    return rv;
}

/* Returns number of services offered by the device */
int _VN_get_service_count(_VN_guid_t guid)
{
    int rv;
    assert(_vn_initialized());
    _vn_net_lock();
    rv = _vn_get_service_count(guid);
    _vn_net_unlock();
    return rv;
}

/* Returns list of services offered by the device */
int _VN_get_service_ids(_VN_guid_t guid,
                        uint32_t* services, uint16_t nservices)
{
    int rv;
    assert(_vn_initialized());
    if (services == NULL) {
        return _VN_ERR_INVALID;
    }

    _vn_net_lock();
    rv = _vn_get_service_ids(guid, services, nservices);
    _vn_net_unlock();
    return rv;
}

/* Returns application specific data about service */
int _VN_get_service(_VN_guid_t guid, uint32_t service_id,
                    void* msg, uint16_t msglen)
{
    int rv;
    _vn_service_t* service;

    assert(_vn_initialized());
    if (msg == NULL) {
        return _VN_ERR_INVALID;
    }

    _vn_net_lock();
    service = _vn_lookup_service(guid, service_id);
    if (service != NULL) {
        rv = MIN(msglen, service->msglen);
        memcpy(msg, service->msg, rv);
    }
    else rv = _VN_ERR_NOTFOUND;            
    _vn_net_unlock();
    return rv;    
}

/* Virtual Network Operations */

int _VN_config_port(_VN_addr_t addr, _VN_port_t port, int port_state)
{
    int rv;

    assert(_vn_initialized());
    /* Verify port is not a system port */
    if (_VN_PORT_IS_SYSTEM(port)) {
        return _VN_ERR_PORT;
    }

    _vn_net_lock();
    rv = _vn_config_port(addr, port, port_state);
    _vn_net_unlock();
    return rv;
}

int _VN_set_priority(_VN_addr_t addr, _VN_port_t port, int priority)
{
    int rv;
    _VN_net_t net_id;
    _VN_host_t host_id;

    assert(_vn_initialized());
    /* Verify port is not a system port */
    if (_VN_PORT_IS_SYSTEM(port)) {
        return _VN_ERR_PORT;
    }

    net_id = _VN_addr2net(addr);
    host_id = _VN_addr2host(addr);

    _vn_net_lock();
    rv = _vn_set_priority(net_id, host_id, port, priority);
    _vn_net_unlock();

    return rv;
}

int _VN_recv(const void* handle, void* msg, _VN_msg_len_t msg_len) 
{
    int len = 0;
    _vn_buf_t* pkt;

    assert(_vn_initialized());
    if (handle == NULL) return _VN_ERR_HANDLE;
    if ((msg == NULL) && (msg_len > 0)) return _VN_ERR_LENGTH;

    /* Get data (already authenticated and decrypted) */
    pkt = _vn_mhp_remove_pkt(handle);
    if (pkt == NULL) return _VN_ERR_HANDLE;

    if (msg) {
        _vn_msg_header_t* header = (_vn_msg_header_t*) pkt->buf;
        len = MIN(msg_len, ntohs(header->data_size));
        memcpy(msg, pkt->buf + _VN_HEADER_SIZE, len);
    }

    /* free packet */
    _vn_free_msg_buffer(pkt);

    return len;    
}

/* 
 * _VN_send prepares and buffers VN messages to be sent to another peer host
 *  on the specified virtual network. If to_host is _VN_HOST_OTHERS, a copy of 
 *  the message will be sent to all other peer hosts on the VN.  If to_host
 *  is _VN_HOST_ANY, a copy of the message will be sent to all peer hosts on
 *  the VN (including the sender).
 * 
 * Return values:
 * If successful, a positive call id will be returned.  If any of the messages
 *    associated with this call cannot be delivered after it has been scheduled,
 *    a _VN_msg_err_event with the returned call id will be thrown.
 *
 * Possible errors are:
 * _VN_ERR_PORT:    Specified port is a reserved VN system port
 * _VN_ERR_NETID:   Specified net is not found
 * _VN_ERR_HOSTID:  If from_host is not a localhost or to_host is not
 *                  a member of the VN
 * _VN_ERR_LENGTH:  The message length (len) is too large.
 * _VN_ERR_INVALID: If any other parameter is invalid
 * _VN_ERR_FULL:    Send queue is full
 * _VN_ERR_NOMEM:   No more memory
 *
 * NOTE: When sending to multiple hosts (i.e. _VN_HOST_OTHERS or _VN_HOST_ANY),
 *       upon _VN_ERR_FULL, _VN_ERR_NOMEM, some of the messages may have
 *       already been scheduled for delivery.  
 *
 *       TODO: How do I know those messages are associated with this failed
 *             call since I didn't get the call id?
 */
int _VN_send(_VN_net_t net_id, _VN_host_t from_host, _VN_host_t to_host,
             _VN_port_t port, const void* msg, _VN_msg_len_t len, 
             const void* opthdr, uint8_t hdr_len, uint32_t attr)
{
    int rv, call_id;

    assert(_vn_initialized());
    /* Don't allow user applications to send on VN system ports */
    if (_VN_PORT_IS_SYSTEM(port)) {
        return _VN_ERR_PORT;
    }

    _vn_net_lock();
    call_id = _vn_new_call_id();
    rv = _vn_qm_send(call_id, net_id, from_host, to_host, port, msg, len,
                     opthdr, hdr_len, attr, false);
    _vn_net_unlock();

    if (rv >= 0) { 
        rv = call_id;
    }

    return rv;
}

/* Device information */

/* Returns the guid of the specified game host */
int _VN_get_guid(_VN_addr_t addr, _VN_guid_t* guid)
{ 
    int rv;
    assert(_vn_initialized());
    _vn_net_lock();
    rv = _vn_get_guid(addr, guid);
    _vn_net_unlock(); 
    _VN_CONVERT_GUID(guid);
    return rv;
}

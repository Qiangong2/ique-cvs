//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnlocal.h"

/* Maximum number of packets to buffer per port (per hostid/netid) */
uint32_t _vn_retx_max_pkts = _VN_QM_MAX_RELIABLE_PER_PORT;
/* Total number of pkts buffered for retransmission */
uint32_t _vn_retx_buffered = 0;

/* TODO: Replace simplistic timer owith better algorithm
   taking into account RTT of host */
#define _VN_RETX_TIMER_MSECS              2*1000
#define _VN_RETX_TIMER_MIN                1*1000
#define _VN_RETX_TIMER_MAX             1*60*1000
#define _VN_RETX_TIMER_TIMEOUT         1*60*1000
#define _VN_RETX_TIMER                         1

uint32_t _vn_retx_timeout = _VN_RETX_TIMER_TIMEOUT;

void _vn_retx_set_timeout(uint32_t timeout)
{
    _vn_retx_timeout = timeout;
}

uint32_t _vn_retx_get_timeout()
{
    return _vn_retx_timeout;
}

uint32_t _vn_retx_get_buffered_count()
{
    return _vn_retx_buffered;
}

uint32_t _vn_retx_estimate_rto(_vn_retx_info_t* retx_info, 
                               uint32_t cur_rto, bool timeout)
{
    uint32_t rto;
    if (timeout) {
        rto = cur_rto*2;
    }
    else {
        /* RTO = srtt + max(G, 4*rttvar) where G is the clock granularity */
        rto = retx_info->srtt + 4*(retx_info->rttvar);
        if (rto < _VN_RETX_TIMER_MIN) {
            rto = _VN_RETX_TIMER_MIN;
        }
    }
    return MIN(rto, _VN_RETX_TIMER_MAX);
} 

void _vn_retx_free_msg(_vn_msg_t* msg, _vn_retx_info_t* retx_info)
{
    assert(msg);
    if (msg->flags & _VN_MSG_FLAGS_SYN) {
        /* Clear any reconnecting flag */
        if (retx_info) {
            retx_info->flags &= ~_VN_PROP_PORT_RESYNCING;
        }
    }
    _vn_free_msg(msg, true);
}

void _vn_retx_free_msg_cb(_vn_msg_t* msg)
{
    assert(msg);
    if (msg->flags & _VN_MSG_FLAGS_SYN) {
        /* Clear any reconnecting flag */
        _vn_send_info_t* send_info;
        send_info = _vn_lookup_send_info(msg->net_id, msg->to /* local */,
                                         msg->from /*remote*/, msg->port);
        if (send_info && send_info->retx) {
            send_info->retx->flags  &= ~_VN_PROP_PORT_RESYNCING;
        }
    }
    _vn_free_msg(msg, true);
}

int _vn_retx_retransmit_msgs(_vn_dlist_t* retx_q)
{
    int nretx = 0, rv;
    _vn_msg_t* msg;
    assert(retx_q);
    /* TODO: Limit the number of messages to retransmit at a time */
    while ( (msg = (_vn_msg_t*) _vn_dlist_deq_front(retx_q)) != NULL)
    {
        /* Tell Queue manager to retransmit our buffered packets */
        /* Moves msg off from queue, requires msg to be requeued */
        assert(_vn_retx_buffered > 0);
        _vn_retx_buffered--;
        rv = _vn_enqueue(msg, true);
        if (rv < 0) {
            /* Oops, can't enqueue this message */
            _VN_TRACE(TRACE_ERROR, _VN_SG_RETX,
                      "_vn_retx_retransmit_msgs: Cannot enqueue "
                      "retx due to %d\n", rv);
            /* TODO: What if error enqueuing message? */
            /* For now, put packet back on retransmission queue and
               hope for the best. */
            _vn_retx_buffered++;
            _vn_dlist_enq_front(retx_q, msg);
            return nretx;
        }
        nretx++;
    }        
    return nretx;
}

void _vn_retx_timer_cb(_vn_timer_t* timer)
{
    _vn_retx_info_t* retx_info;
    int nretx = 0, ndel = 0;
    uint32_t delay, new_delay, total, timeout;

    _VN_TRACE(TRACE_FINER, _VN_SG_RETX,
              "_vn_retx_timer_cb (%d): started\n", timer);
    assert(timer);
    retx_info = (_vn_retx_info_t*) timer->data;
    assert(retx_info);
    assert(retx_info->retx_timer == timer);
    /* Timeout */
    delay = _vn_timer_get_delay(timer);
    total = _vn_timer_get_total(timer);
    new_delay = delay;
    timeout = _vn_retx_get_timeout();
    /* In callback, the timer has expired and been removed from list */
    if (total < timeout) {
        /* If total delay still small, 
           set longer delay and retransmit packets */
        new_delay = _vn_retx_estimate_rto(retx_info, delay, true);
        new_delay = MIN(new_delay, timeout - total);
        _vn_timer_mod(timer, new_delay, false);
        nretx = _vn_retx_retransmit_msgs(retx_info->retx_msgs);
    }
    else {
        /* If too long, give up on host altogether */
        _vn_msg_t* msg;
        _VN_TRACE(TRACE_WARN, _VN_SG_RETX,
                  "_vn_retx_timer_cb (%d, addr=0x%08x, port=%u): "
                  "delay %u exceeds max retx timer %u\n",
                  timer, retx_info->addr, retx_info->port, total, timeout);

        /* REVIEW: Handling of packets after a host become unreachable */
        /* After dropping packets, mark host as unreachable.
           Lossy packets are still be sent to the host (may be lost).
           If reliable packets are sent to the host, the first reliable
           packet for a port is marked with a SYN to resync the rseq no. */

        retx_info->flags |= _VN_PROP_PORT_SYNC_LOST;
        _vn_host_status_mark_unreachable(_VN_addr2net(retx_info->addr),
                                         _VN_addr2host(retx_info->addr));

        /* Just drop theses messages */
        while ( (msg = (_vn_msg_t*) 
                 _vn_dlist_deq_front(retx_info->retx_msgs)) != NULL)
        {
            assert(_vn_retx_buffered > 0);
            _vn_retx_buffered--;
            /* Post event that we had to drop this packet */
            _vn_post_msg_err_event(msg, _VN_ERR_TIMEOUT);
            _vn_retx_free_msg(msg, retx_info);
            ndel++;
        }
        /* _vn_timer_reset_total(timer); */
        _vn_timer_mod(timer, _VN_RETX_TIMER_MIN, false);
    }
    
    _VN_TRACE(TRACE_FINER, _VN_SG_RETX,
              "_vn_retx_timer_cb (%d, addr=0x%08x, port=%u): "
              "%d packets retransmitted, "
              "%d packets dropped, rto old %u, new %u, total %u\n", 
              timer, retx_info->addr, retx_info->port, 
              nretx, ndel, delay, new_delay, total);
}

_vn_retx_info_t* _vn_retx_info_create(_VN_addr_t addr, _VN_port_t port)
{
    _vn_retx_info_t* pRetx = _vn_malloc(sizeof(_vn_retx_info_t));
    if (pRetx) {
        pRetx->addr = addr;
        pRetx->port = port;
        pRetx->retx_msgs = NULL;
        pRetx->flags = 0;
        pRetx->last_acked = -1;
#if _VN_RETX_TIMER
        pRetx->retx_timer =_vn_timer_create(_VN_RETX_TIMER_MSECS,
                                            pRetx, _vn_retx_timer_cb);
#else 
        pRetx->retx_timer = NULL;
#endif
        pRetx->srtt = 0;
        pRetx->rttvar = 0;

    }
    return pRetx;
}

void _vn_retx_info_destroy(_vn_retx_info_t* pRetx)
{
    if (pRetx) {
        if (pRetx->retx_msgs) {
            /* TODO: Post event that these messages are being dropped? */
            assert(_vn_retx_buffered >= _vn_dlist_size(pRetx->retx_msgs));
            _vn_retx_buffered -= _vn_dlist_size(pRetx->retx_msgs);
            _vn_dlist_destroy(pRetx->retx_msgs, (void*) &_vn_retx_free_msg_cb);
        }
        if (pRetx->retx_timer) {
            _vn_timer_cancel(pRetx->retx_timer);
            _vn_timer_delete(pRetx->retx_timer);
        }
        _vn_free(pRetx);
    }
}

int _vn_retx_buffer_msg(_vn_msg_t* msg, _vn_retx_info_t* retx_info)
{
    /* Buffers messages for retransmission in the future */
    /* Returns error if retransmission buffer full */
    uint8_t seq;
    assert(msg);
    assert(retx_info);

    seq = msg->seq;
    
    /* Try to prevent acked packet from being rebuffered */
    if (!_vn_seqno_larger_than(seq, retx_info->last_acked)) {
        /* Seqno <= last acked seq, ignore msg */
        /* Drop msg */
        _vn_retx_free_msg(msg, retx_info);
        return _VN_ERR_OK;
    }

    if (retx_info->retx_msgs == NULL) {
        retx_info->retx_msgs = _vn_dlist_create();
        if (retx_info->retx_msgs == NULL) {
            return _VN_ERR_NOMEM;
        }
    }

#if _VN_RETX_TIMER
    /* If retx timer not set (i.e. retx_msgs empty), set timer */
    if (_vn_dlist_empty(retx_info->retx_msgs)) {
        _vn_timer_retrigger(retx_info->retx_timer, false);
    }
#endif
    
    if (_vn_dlist_size(retx_info->retx_msgs) < _vn_retx_max_pkts) {
        /* Make sure our packets are inserted in order */
        _vn_dnode_t* node;
        int rv = _VN_ERR_OK;

        if (_vn_dlist_empty(retx_info->retx_msgs)) {
            rv = _vn_dlist_enq_back(retx_info->retx_msgs, msg);
        }
        else {
            /* Make sure we insert at the right place (start from back) */
            bool done = false;
            _vn_dlist_reverse_for(retx_info->retx_msgs, node) 
            {
                _vn_msg_t* curmsg = _vn_dnode_value(node);
                if (curmsg) {
                    uint8_t curseq = curmsg->seq;
                    if (_vn_seqno_larger_than(seq, curseq)) {
                        /* Stick msg in here after this node */
                        rv = _vn_dlist_insert(retx_info->retx_msgs, node, msg);
                        done = true;
                        break;
                    }
                }
                else {
                    assert(false);
                }
            }
            if (!done) {
                rv = _vn_dlist_enq_front(retx_info->retx_msgs, msg);
            }
        }
        if (rv >= 0) {
            _vn_retx_buffered++;
            _VN_TRACE(TRACE_FINEST, _VN_SG_RETX,
                      "_vn_retx_buffer_msg: Packet buffered "
                      "for net 0x%08x, from host %u to %u, port %u\n",
                      msg->net_id, msg->from, msg->to, msg->port);
            return _VN_ERR_OK;
        }
        else {
            return rv;
        }
    }
    else {
        _VN_TRACE(TRACE_WARN, _VN_SG_RETX,
                  "_vn_retx_buffer_msg: Cannot buffer packet, retx queue "
                  "for net 0x%08x, from host %u to %u, port %u full"
                  " (max %d packets)\n",
                  msg->net_id, msg->from, msg->to, msg->port, 
                  _vn_retx_max_pkts);
        return _VN_ERR_FULL;
    }
}

/*
 * Processes a ACK or NACK packet
 * @param from - host id of remote host that sent the ack
 * @param to - host id of local host receiving the ack
 */
void _vn_retx_proc_ack(_VN_net_t net_id, _VN_host_t from, _VN_host_t to,
                       _VN_port_t port, int8_t ack_mask, int8_t ack_seq)
{
    _vn_send_info_t* send_info;
    _vn_retx_info_t* retx_info;
    uint8_t last_acked, seq;
    int nretx = 0, ndel = 0;
    bool retx_needed, resyncing;

    switch (ack_mask) {
        case 0:   /* Didn't get an ack at all */
            return;
        case _VN_OPT_POS_ACK:               
            last_acked = ack_seq;
            retx_needed = false;  /* Don't need to retransmit packets */
            break;
        case _VN_OPT_NEG_ACK:
            last_acked = ack_seq - 1;
            retx_needed = true;  /* Retransmit packets */
            break;
        default:
            /* Weird ack mask */
            _VN_TRACE(TRACE_WARN, _VN_SG_RETX,
                      "_vn_retx_proc_ack: Ignoring ack packet from %u to %u, "
                      "for net 0x%08x, port %u, ack_seq %u, with unknown "
                      "ackmask %u\n",
                      from, to, net_id, port, ack_seq, ack_mask);
            return;    
    }

    send_info = _vn_lookup_send_info(net_id, to /*local*/,
                                     from /*remote*/, port);
    retx_info = (send_info)? send_info->retx: NULL;

    if (retx_info) {
        resyncing = retx_info->flags & _VN_PROP_PORT_RESYNCING;
            
        if (_vn_seqno_larger_than(last_acked, retx_info->last_acked)) {
            /* Cancel message that local host (to) send to remote host (from) */
            _vn_qm_cancel_retx_msgs_seq_le(retx_info,
                                           net_id, to, from, port, last_acked);
            retx_info->last_acked = last_acked;
        }

        if (retx_info->retx_msgs) {
            /* Can remove all packets with seq no <= last_acked */
            _vn_dlist_t* retx_q = retx_info->retx_msgs;
            _vn_msg_t* msg;
            uint32_t delay, new_delay;
            while ( (msg = (_vn_msg_t*) _vn_dlist_peek_front(retx_q)) != NULL)
            {
                seq = msg->seq;
                /* Assumes that buffered packets are in sequence */
                if (_vn_seqno_larger_than(seq, last_acked))
                {
                    /* Done */
                    break;
                }
                else {
                    if ((seq == last_acked) && 
                        ((msg->flags & _VN_MSG_FLAGS_RETX) == 0))
                    {
                        /* Make sure sendtime is set */
                        uint32_t rtt;
                        assert(msg->sendtime != _VN_TIMESTAMP_INVALID);
                        rtt = (uint32_t) (_vn_get_timestamp() - msg->sendtime);

                        _vn_host_status_update_rtt(net_id, from, rtt);

                        if ((retx_info->srtt == 0) && (retx_info->rttvar == 0))
                        {
                            /* RTT has not been initialized yet */
                            retx_info->srtt = rtt;
                            retx_info->rttvar = rtt/2;
                        }
                        else {
                            retx_info->rttvar = retx_info->rttvar*3/4
                                + abs(retx_info->srtt - rtt)*1/4;
                            retx_info->srtt = retx_info->srtt*7/8 + rtt*1/8;
                        }
                        _VN_TRACE(TRACE_FINER, _VN_SG_RETX,
                                  "rtt for net 0x%08x, from %u to %u, "
                                  "port %u: last %u, srtt %u, rttvar %u\n", 
                                  net_id, from, to, port, rtt, 
                                  retx_info->srtt, retx_info->rttvar);
                        
                    }
                    msg = (_vn_msg_t*) _vn_dlist_deq_front(retx_q);
                    assert(_vn_retx_buffered > 0);
                    _vn_retx_buffered--;
                    _vn_retx_free_msg(msg, retx_info);
                    ndel++;
                }
            }

            if (resyncing) {
                resyncing = retx_info->flags & _VN_PROP_PORT_RESYNCING;
                if (!resyncing) {
                    /* State changed from resyncing to not resyncing */
                    /* ACK must have been received */
                    retx_info->flags &= ~_VN_PROP_PORT_SYNC_LOST;
                }
            }

            if (retx_needed) {
                nretx += _vn_retx_retransmit_msgs(retx_q);
            }
    
#if _VN_RETX_TIMER
            /* Estimate new RTO and cancel retx timer
             * retransmit messages 
             */
            delay = _vn_timer_get_delay(retx_info->retx_timer);
            if (retx_needed) {
                /* Keep old delay for nacks */
                new_delay = delay;
            }
            else {
                /* Get new delay based on rtt for acks */
                new_delay = _vn_retx_estimate_rto(retx_info, delay, false);
            }
            _VN_TRACE(TRACE_FINER, _VN_SG_RETX,
                      "rto for net 0x%08x, from %u to %u, "
                      "port %u: old %u, new %u\n",
                      net_id, from, to, port, delay, new_delay);


            if (_vn_dlist_empty(retx_q)) {
                /* timer will be retriggered when
                 * messages are buffered */
                _vn_timer_cancel(retx_info->retx_timer);
                _vn_timer_mod(retx_info->retx_timer, new_delay, true);
            }
            else {
                _vn_timer_mod(retx_info->retx_timer, new_delay, true);
/*                _vn_timer_retrigger(retx_info->retx_timer); */
            }
#endif

        }

        _VN_TRACE(TRACE_FINER, _VN_SG_RETX,
                  "_vn_retx_proc_ack: Got ack packet from "
                  "%u to %u, for net 0x%08x, port %u, ack_mask %u, ack_seq %u,"
                  " %u packets deleted, %u packets retx, last_acked %u,"
                  " %u remaining\n",
                  from, to, net_id, port, ack_mask, ack_seq, ndel, nretx, 
                  last_acked, _vn_dlist_size(retx_info->retx_msgs));
    }
    else {
        _VN_TRACE(TRACE_WARN, _VN_SG_RETX,
                  "_vn_retx_proc_ack: Cannot proc ack packet from %u to %u, "
                  "for net 0x%08x, port %u, ack_mask %u, ack_seq %u\n",
                  from, to, net_id, port, ack_mask, ack_seq);
    }
}

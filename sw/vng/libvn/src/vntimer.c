//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnlocal.h"

/* Timers */
/* TODO: Change timer data structure on platforms with limited space */
/*       Can just have one list of timers (will be small) 
         2(nets) * 4(peers) * 3(keepalive, retx, nack) */

/* INTERVAL is in milliseconds */
#define _VN_MIN_TIME_INTERVAL           100  /* Each time slot is 100 msec */
#define _VN_MAX_TIME_INTERVAL    10*60*1000  /* Max time interval is 10 min */

#ifdef _SC
/* less memory */
#define _VN_MAX_TIMER_SLOTS      100
#else
/* If lots of memory */
#define _VN_MAX_TIMER_SLOTS      (_VN_MAX_TIME_INTERVAL / _VN_MIN_TIME_INTERVAL)
#endif

#define _VN_TIMER_EXPIRED                 0
#define _VN_TIMER_NOSLOT                (-1)

_vn_dlist_t* _vn_timer_list[_VN_MAX_TIMER_SLOTS];
_VN_time_t _vn_timer_starttime = 0;
uint32_t _vn_timer_cur = 0;

#if _VN_USE_SERVICE_THREAD
#define _VN_TIMER_LOCK_INIT()
#define _VN_TIMER_LOCK_FREE()
#define _VN_TIMER_LOCK()
#define _VN_TIMER_UNLOCK()
#else
static _vn_mutex_t _vn_timer_mutex;
#define _VN_TIMER_LOCK_INIT() _vn_mutex_init(&_vn_timer_mutex)
#define _VN_TIMER_LOCK_FREE() _vn_mutex_destroy(&_vn_timer_mutex)
#define _VN_TIMER_LOCK()      _vn_mutex_lock(&_vn_timer_mutex)
#define _VN_TIMER_UNLOCK()    _vn_mutex_unlock(&_vn_timer_mutex)
#endif

uint32_t _vn_timer_get_current()
{
    return _vn_timer_cur;
}

_vn_timer_t* _vn_timer_create(uint32_t delay_msecs,
                              void* data, void (*cbfunc) (_vn_timer_t* timer))
{
    _vn_timer_t* timer = _vn_malloc(sizeof(_vn_timer_t));
    _vn_timer_init(timer, delay_msecs, data, cbfunc);
    return timer;
}

void _vn_timer_reset_total(_vn_timer_t* timer)
{
    if (timer) {
        timer->total_msecs = 0;
    }
}

uint32_t _vn_timer_get_total(_vn_timer_t* timer)
{
    if (timer) {
        return timer->total_msecs;
    }
    else return 0;
}

uint32_t _vn_timer_get_delay(_vn_timer_t* timer)
{
    if (timer) {
        return timer->delay_msecs;
    }
    else return _VN_TIMEOUT_NONE;
}

uint32_t _vn_timer_get_remaining(_vn_timer_t* timer)
{
    if (timer && (timer->slot != _VN_TIMER_NOSLOT)) {
        uint32_t passed;
        passed = _VN_MIN_TIME_INTERVAL*(_vn_timer_cur - timer->start);
        if (passed < timer->delay_msecs) {
            return timer->delay_msecs - passed;
        }
        else {
            return 0;
        }
    }
    else return _VN_TIMEOUT_NONE;
}

void _vn_timer_init(_vn_timer_t* timer, uint32_t delay_msecs,
                    void* data,  _vn_timer_cb_t cbfunc)
{
    assert(cbfunc);
    if (timer) {
        timer->start = 0;
        timer->total_msecs = 0;
        timer->delay_msecs = delay_msecs;
        timer->expires = _VN_TIMER_EXPIRED;
        timer->slot = _VN_TIMER_NOSLOT;
        timer->data = data;
        timer->cbfunc = cbfunc;
    }
}

void _vn_timer_delete(_vn_timer_t* timer)
{
    /* Assumes timer has already been removed from timer list */
    if (timer) {
        _vn_free(timer);
    }
}

int _vn_timer_add(_vn_timer_t* timer)
{
    /* Assumes timer is not already on timer list */
    uint32_t i;
    _vn_dnode_t* node;
    int rv = _VN_ERR_OK;

    assert(timer);
    /* Make sure there is a callback function - 
       what use is a timer without that? */
    assert(timer->cbfunc);

    _VN_TIMER_LOCK();
    assert(timer->slot == _VN_TIMER_NOSLOT);
    i = timer->delay_msecs/_VN_MIN_TIME_INTERVAL;
    timer->start = _vn_timer_cur;
    timer->slot = _vn_timer_cur + i;
    timer->slot = timer->slot % _VN_MAX_TIMER_SLOTS;
    timer->expires = _vn_timer_cur + i;

    i = timer->slot;
    if (_vn_timer_list[i] == NULL) {
        /* No timers for this slot yet */
        _vn_timer_list[i] = _vn_dlist_create();
        if (_vn_timer_list[i] == NULL) {
            _VN_TIMER_UNLOCK();
            return _VN_ERR_NOMEM;
        }
    }

    /* Add timer in sequence */
    if (_vn_dlist_empty(_vn_timer_list[i])) {
        rv = _vn_dlist_enq_back(_vn_timer_list[i], timer);
    }
    else {
        /* Make sure we insert at the right place (start from back) */
        /* Most of the time, probably just insert at end */
        bool done = false;
        _vn_dlist_reverse_for(_vn_timer_list[i], node) 
        {
            _vn_timer_t* curtimer = _vn_dnode_value(node);
            if (curtimer) {
                /* TODO: handle wrap around */
                if (timer->expires >= curtimer->expires) {
                    /* Stick timer in here after this node */
                    rv = _vn_dlist_insert(_vn_timer_list[i], node, timer);
                    done = true;
                    break;
                }
            }
            else {
                assert(false);
            }
        }
        if (!done) {
            rv = _vn_dlist_enq_front(_vn_timer_list[i], timer);
        }
    }

    _VN_TIMER_UNLOCK();
    return rv;
}

int _vn_timer_mod(_vn_timer_t* timer, uint32_t delay_msecs, bool reset)
{
    int rv;
    assert(timer);
    rv = _vn_timer_cancel(timer);
    if (reset) {
        timer->total_msecs = 0;
    }
    timer->delay_msecs = delay_msecs;
    if (rv >= 0) {
        /* timer used to be active, reactivate */
        rv = _vn_timer_add(timer);
    }
    return rv;
}

int _vn_timer_retrigger(_vn_timer_t* timer, bool reset)
{
    int rv;
    assert(timer);
    rv = _vn_timer_cancel(timer);    
    if (reset) {
        timer->total_msecs = 0;
    }
    rv = _vn_timer_add(timer);
    return rv;
}

void _vn_timer_expire(_vn_timer_t* timer)
{
    if (timer) {
        if (timer->slot != _VN_TIMER_NOSLOT) {
            timer->total_msecs += 
                _VN_MIN_TIME_INTERVAL*(_vn_timer_cur - timer->start);
        }
        timer->expires = _VN_TIMER_EXPIRED;
        timer->slot = _VN_TIMER_NOSLOT;
    }
}

int _vn_timer_cancel(_vn_timer_t* timer)
{
    uint32_t i;
    _vn_dnode_t *node;
    int rv;

    assert(timer);
    _VN_TIMER_LOCK();

    if (timer->slot == _VN_TIMER_NOSLOT) {
        /* Okay, this timer is not active */
        _VN_TIMER_UNLOCK();
        return _VN_ERR_INACTIVE;
    }
    
    i = timer->slot;
    assert(i < _VN_MAX_TIMER_SLOTS);
    /* Find timer on list and remove it from the list */
    /* TODO: once list links in _vn_timer_t, can skip looking
       for timer on list and just remove from list directly */
    if (_vn_timer_list[i] == NULL) {
        /* Timer not found - out of sync with expires flag!!! */
        _VN_TIMER_UNLOCK();
        _VN_TRACE(TRACE_ERROR, _VN_SG_TIMER,
                  "_vn_timer_cancel: Cannot find active timer\n");
        return _VN_ERR_NOTFOUND;
    }
    
    _vn_dlist_for(_vn_timer_list[i], node) 
    {
        if (timer == _vn_dnode_value(node))
            break;        
    }

    if (timer == _vn_dnode_value(node)) {
        /* Found our timer - remove from list */
        _vn_dlist_delete(_vn_timer_list[i], node);
        _vn_timer_expire(timer);
        rv = _VN_ERR_OK;
    }
    else {
        /* Timer not found - out of sync with slot flag!!! */
        rv = _VN_ERR_NOTFOUND;
        _VN_TRACE(TRACE_ERROR, _VN_SG_TIMER,
                  "_vn_timer_cancel: Cannot find active timer\n");
    }

    _VN_TIMER_UNLOCK();
    return rv;
}

void _vn_timer_handler()
{
    uint32_t i, j;
    _VN_time_t t = 0;
    _vn_timer_t* timer;

    _VN_TIMER_LOCK();

    /* Readjust timer to real time once in a while */
    if (_vn_timer_cur % 20 == 0) {
        t = _vn_get_timestamp() - _vn_timer_starttime;
    }
#if 0
    _VN_TRACE(TRACE_FINE, _VN_SG_TIMER, "_vn_timer_handler: %u, %llu\n",
              _vn_timer_cur, t);
#endif
    /* Adjust _vn_timer_cur for actual time passing */
    i = _vn_timer_cur % _VN_MAX_TIMER_SLOTS;
    if (t) {
        _vn_timer_cur = (uint32_t) (t/_VN_MIN_TIME_INTERVAL);
    } else {
        _vn_timer_cur++;
    }
    j = _vn_timer_cur % _VN_MAX_TIMER_SLOTS;
    assert(i < _VN_MAX_TIMER_SLOTS);
    assert(j < _VN_MAX_TIMER_SLOTS);
    
    while (i != j) {
        if (_vn_timer_list[i] != NULL) {
            /* Go through list and call timer callback functions */
            /* Assumes timers are in order */
            while ((timer = (_vn_timer_t*)
                    _vn_dlist_peek_front(_vn_timer_list[i]))) {
                /* TODO: Handle wrap around */
#if 0
                _VN_TRACE(TRACE_FINE, _VN_SG_TIMER, 
                          "_vn_timer_handler: %u, %u, %u\n",
                          _vn_timer_cur, timer->expires, timer->delay_msecs);
#endif
                if (timer->expires <= _vn_timer_cur) {
                    /* Don't need to keep timer mutex when calling callback function */
                    timer = _vn_dlist_deq_front(_vn_timer_list[i]);
                    _vn_timer_expire(timer);
                    _VN_TIMER_UNLOCK();
                    assert(timer->cbfunc);

                    /* Net assumed to be locked */
                    (*timer->cbfunc)(timer);
                    
                    _VN_TIMER_LOCK();
                }
                else {
                    break;
                }
            }
        }
        i = (i + 1) % _VN_MAX_TIMER_SLOTS;
    }
    _VN_TIMER_UNLOCK();
}

/* Locked version of _vn_timer_handler */
void _vn_timer_handler_locked()
{
    _vn_net_lock();
    _vn_timer_handler();
    _vn_net_unlock();
}

int _vn_timer_prepare()
{
    _VN_TIMER_LOCK_INIT();
    _VN_TIMER_LOCK();
    memset(_vn_timer_list, 0, sizeof(_vn_timer_list));
    _vn_timer_starttime = _vn_get_timestamp();
    _vn_timer_cur = 0;
    _VN_TIMER_UNLOCK();
    return _VN_ERR_OK;
}

int _vn_timer_cleanup()
{
    int i;
    /* Clean up timer resources */
    for (i = 0; i < _VN_MAX_TIMER_SLOTS; i++) {
        /* Don't delete timers, just mark them as being no longer on list */
        if (_vn_timer_list[i]) {
            _vn_dlist_destroy(_vn_timer_list[i], 
                              (void*) &_vn_timer_expire);
            _vn_timer_list[i] = NULL;
        }
    }
    _VN_TIMER_LOCK_FREE();
    return 0;
}

int _vn_timer_start()
{
    _vn_timer_prepare();
    return _vn_sys_timer_start(_VN_MIN_TIME_INTERVAL);
}

int _vn_timer_stop()
{
    return _vn_sys_timer_stop();
}

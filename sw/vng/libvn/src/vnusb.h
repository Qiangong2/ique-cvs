//
//               Copyright (C) 2006, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#ifndef __VN_USB_H__
#define __VN_USB_H__

#include "vnlocaltypes.h"
#include "vntimer.h"
#include "vnlist.h"
#include "vnhash.h"
#include "vnmsg.h"

#ifdef _VN_RPC_PROXY
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
#include "sc/usb.h" /* From NC Tree */
#endif

/* USB sockets option so it can be compiled out for final SC version */
#ifdef _SC
/* Change to 1 if we want to support SCE */
#define _VN_USB_SOCKETS                0
#else
#define _VN_USB_SOCKETS                1
#endif

#ifdef _VN_USB_SOCKETS
#ifndef _SC
#include "vnsocket.h"
#endif
#endif

#include "vn.h"

#ifdef __cplusplus
extern "C" {
#endif

#define _VN_USB_PORT_DEVICE            0
#define _VN_USB_PORT_PROXY         20010

#define _VN_USB_PORT_MUX           20020

#ifdef _SC
#define _VN_USB_MAX_DATA_LEN       16384
#else
#define _VN_USB_MAX_DATA_LEN       _VN_TCP_FRAME_MAX_DATA_LEN
#endif

#define _VN_USB_MAX_LEN            (_VN_USB_MAX_DATA_LEN + USB_MUX_HD_SIZE)

#define _VN_USB_HANDLE_INVALID    (-1)

#ifdef _VN_RPC_PROXY
#define _VN_USB_MAX                  _VN_PROXY_MAX_DEVICES
#else
#define _VN_USB_MAX                  1
#endif


typedef int32_t _vn_usb_handle_t;

/* Supported USB modes */
typedef enum {
    _VN_USB_MODE_REAL = 0,
#ifdef _VN_USB_SOCKETS
    _VN_USB_MODE_SOCKETS,
#endif
    _VN_USB_MODE_MAX
} _vn_usb_mode_t;

typedef enum {
    _VN_USB_STATE_INIT = 0,     /* USB is open and initialized */
    _VN_USB_STATE_ENUM_DONE,    /* USB enumeration is complete */
    _VN_USB_STATE_READY,        /* USB is ready to send data*/
    _VN_USB_STATE_UNPLUG,       /* USB is unplugged */
    _VN_USB_STATE_CLOSED        /* USB is closed */
} _vn_usb_state_t;

extern _vn_usb_mode_t _vn_usb_mode;

#ifdef _VN_USB_SOCKETS
/* Define parameters for USB using sockets */

/* SC does not support sockets, use USB Hack */
typedef struct {
#ifndef _SC
    _vn_inaddr_t device_addr;
    _vn_inport_t device_port;
    _vn_inaddr_t proxy_addr;
    _vn_inaddr_t proxy_port;
    _vn_socket_t sockfd;
#endif
} _vn_usb_socket_t;
#endif

/* Define parameters for real USB */
#ifdef _SC
#define _VN_USB_MAX_ASYNC_RMSGS       2
#define _VN_USB_MAX_ASYNC_WMSGS       2
#endif

typedef struct {
#if defined(_WIN32)
    char* devName; /* Device Name */
    HANDLE uwh; /* USB write handle */
    HANDLE urh; /* USB read handle */
#elif defined(_SC)
    IOSFd devfd;
    IOSFd vnfd;
    /* USB read buffers */
    uint32_t rbuflen;
    uint8_t *rbufs[_VN_USB_MAX_ASYNC_RMSGS];
    uint8_t  nrbufs;
    int8_t   active_rbuf; /* Which of the bufs have data */
    uint8_t *ioctl_buf; /* Temporary buffer used for ioctl calls */
    /* Message Queue for async read */
    IOSMessageQueueId async_rmq;
    IOSMessage async_rmsgs[_VN_USB_MAX_ASYNC_RMSGS];
    IOSResourceRequest* async_rrds[_VN_USB_MAX_ASYNC_RMSGS];
    uint8_t  async_rqueued[_VN_USB_MAX_ASYNC_RMSGS];
    /* Message Queue for async write */
    IOSMessageQueueId async_wmq;
    IOSMessage async_wmsgs[_VN_USB_MAX_ASYNC_WMSGS];
    IOSResourceRequest* async_wrds[_VN_USB_MAX_ASYNC_WMSGS];
#elif defined(_LINUX)
    int fd;
    int usbNum; 
#endif
} _vn_usb_real_t;


/* Define generic USB structure */
typedef struct {
    _vn_usb_handle_t handle;
    _vn_mutex_t      mutex;
    _vn_usb_mode_t   mode;
    _vn_usb_state_t  state;
    void *data;
    _vn_free_func_t free_data_func;
    union {
        _vn_usb_real_t ureal;
#ifdef _VN_USB_SOCKETS
        _vn_usb_socket_t usocket;
#endif
    } usb;
} _vn_usb_t;


/* USB prototypes */
int _vn_init_usb_real(_vn_usb_t* usb);
int _vn_shutdown_usb_real(_vn_usb_t* usb);
int _vn_read_usb_real(_vn_usb_t* usb, void* buf, size_t len, uint32_t timeout);
int _vn_write_usb_real(_vn_usb_t* usb, const void* buf, size_t len, uint32_t timeout);
int _vn_check_usb_status_real(_vn_usb_t* usb);

#ifdef _VN_RPC_PROXY
int _vn_read_usb_mux(_vn_usb_t* usb, uint16_t* proto,
                     void* buf, size_t len, uint32_t timeout);
int _vn_write_usb_mux(_vn_usb_t* usb, uint16_t proto,
                      const void* buf, size_t len, uint32_t timeout);
int _vn_write_usb_mux_wh(_vn_usb_t* usb, uint16_t proto,
                         const void* buf, size_t buflen, size_t datalen,
                         uint32_t timeout);
#endif

#ifdef _VN_USB_SOCKETS
int _vn_init_usb_socket(_vn_usb_t* usb);
int _vn_shutdown_usb_socket(_vn_usb_t* usb);
int _vn_read_usb_socket(_vn_usb_t* usb, void* buf, size_t len, uint32_t timeout);
int _vn_write_usb_socket(_vn_usb_t* usb, const void* buf, size_t len, uint32_t timeout);
int _vn_check_usb_status_socket(_vn_usb_t* usb);

int _vn_init_usb(_vn_usb_t* usb);
int _vn_shutdown_usb(_vn_usb_t* usb);
int _vn_read_usb(_vn_usb_t* usb, void* buf, size_t len, uint32_t timeout);
int _vn_write_usb(_vn_usb_t* usb, const void* buf, size_t len, uint32_t timeout);
int _vn_check_usb_status(_vn_usb_t* usb);

typedef struct {
    int (*init_usb)(_vn_usb_t* usb);
    int (*shutdown_usb) (_vn_usb_t* usb);
    int (*read_usb) (_vn_usb_t* usb, void* buf, size_t len, uint32_t timeout);
    int (*write_usb) (_vn_usb_t* usb, const void* buf, size_t len, uint32_t timeout);
    int (*check_usb_status) (_vn_usb_t* usb);
} _vn_usb_funcs_t;

#else

#define _vn_init_usb _vn_init_usb_real
#define _vn_shutdown_usb _vn_shutdown_usb_real
#define _vn_read_usb _vn_read_usb_real
#define _vn_write_usb _vn_write_usb_real
#define _vn_check_usb_status _vn_check_usb_status_real

#endif


int _vn_usb_destroy(_vn_usb_handle_t handle);
_vn_usb_t* _vn_usb_get(_vn_usb_handle_t handle);
void _vn_usb_init_table();

_vn_usb_handle_t _vn_usb_reserve(_vn_usb_mode_t mode);
_vn_usb_handle_t _vn_usb_create(_vn_usb_mode_t mode, void* config);

int _vn_usb_check_ready(_vn_usb_handle_t handle);

#ifdef  __cplusplus
}
#endif

#endif /* __VN_USB_H__ */

//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#ifndef __VN_TIMER_H__
#define __VN_TIMER_H__

#include "vntypes.h"
#include "vnsys.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct __vn_timer_t   _vn_timer_t;
typedef void (*_vn_timer_cb_t)(_vn_timer_t* timer);

/* Timer */
struct __vn_timer_t {
    /* TODO: Have list prev/next be part of this data structure 
       (should also do for anything else that goes on linked lists)
       _vn_timer_t *next */

    uint32_t slot;        /* Used as index into timer list 
                             (lock with timer mutex before accessing) */
    uint32_t delay_msecs; /* timeout in milliseconds */
    uint32_t total_msecs; /* total time the timer was on */
    uint32_t start;       /* When did the timer start? */
    uint32_t expires;     /* When does the timer expire? */
    void* data;
    _vn_timer_cb_t cbfunc;
};

uint32_t _vn_timer_get_current();
uint32_t _vn_timer_get_delay(_vn_timer_t* timer);
uint32_t _vn_timer_get_total(_vn_timer_t* timer);
uint32_t _vn_timer_get_remaining(_vn_timer_t* timer);
void     _vn_timer_reset_total(_vn_timer_t* timer);

_vn_timer_t* _vn_timer_create(uint32_t delay_msecs, void* data, 
                              void (*cbfunc) (_vn_timer_t* timer));
void _vn_timer_init(_vn_timer_t* timer, uint32_t delay_msecs,
                    void* data, _vn_timer_cb_t cbfunc);
void _vn_timer_delete(_vn_timer_t* timer);

int _vn_timer_add(_vn_timer_t* timer);
int _vn_timer_retrigger(_vn_timer_t* timer, bool reset);
int _vn_timer_mod(_vn_timer_t* timer, uint32_t delay_msecs, bool reset);
int _vn_timer_cancel(_vn_timer_t* timer);

/* Start timer thread */
int _vn_timer_start();
int _vn_timer_stop();
int _vn_timer_cleanup();

/* Callback function for timer */
void _vn_timer_handler();
void _vn_timer_handler_locked();
    
#ifdef  __cplusplus
}
#endif
 
#endif /* __VN_TIMER_H__ */

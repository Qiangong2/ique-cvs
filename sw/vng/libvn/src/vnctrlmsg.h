//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#ifndef __VN_CTRLMSG_H__
#define __VN_CTRLMSG_H__

#include "vnlocaltypes.h"
#include "vnnetcore.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Constants */
#define _VN_MSG_NET_LEAVE         0x02
#define _VN_MSG_NET_GET_HOSTS     0x03
#define _VN_MSG_NET_NEW_ID_REQ    0x04
#define _VN_MSG_NET_NEW_ID_RESP   0x84
#define _VN_MSG_NET_RELEASE       0x05
#define _VN_MSG_NET_UPDATE        0x06
#define _VN_MSG_NET_JOIN_REQ      0x07
#define _VN_MSG_NET_JOIN_RESP     0x87
#define _VN_MSG_PING_REQ          0x10
#define _VN_MSG_PING_RESP         0x90
#define _VN_MSG_STATUS            0x11
#define _VN_MSG_CONNECT_REQ       0x12
#define _VN_MSG_CONNECT_RESP      0x92
#define _VN_MSG_CLOCK_SYNC        0x13

#define _VN_MSG_ID_INVALID           0

#define _VN_PDU_PEER_NEW          0x01
#define _VN_PDU_PEER_DEL          0x02
#define _VN_PDU_ADDR              0x03
#define _VN_PDU_ADDR_OWNER        0x04
#define _VN_PDU_ADDR_REQUESTOR    0x05

#define _VN_CTRL_MSG_VERSION 0

typedef _VN_callid_t       _vn_ctrl_msgid_t;
typedef uint16_t           _vn_ctrl_msgtype_t;
typedef uint8_t            _vn_ctrl_pdutype_t;

/* Internal Data Types */
typedef struct {
    uint8_t   type;             /* Owner IP, Join Requestor IP */
    uint8_t   family;           /* 2 for IPv4 (same as AF_INET) */
    uint16_t  port;
    uint32_t  ipaddr;
} _vn_ctrl_pdu_addr_t;

typedef struct
{
    uint8_t    type;             /* New peer, Delete peer */
    uint8_t    naddrs;           /* Number of addresses that follows */
    uint16_t   hostid;           /* Host ID */
    _VN_guid_t guid;             /* Guid (device type and id) */
    int32_t    clock_delta;      /* Clock delta (from sender) */

    /* For NAT Tranversal, send list of addresses to try.
       IP/Port follow, number of IP/Port addresses specified in naddrs */
    /* _vn_ctrl_pdu_addr_t addrs[]; */
} _vn_ctrl_pdu_peer_t;

typedef struct {
    int16_t  status;
    uint16_t msglen;
    uint8_t  msg[];
} _vn_ctrl_pdu_status_t;

/* Control message header */
typedef struct
{
    uint8_t  version;          /* VN control message version */
    uint8_t  flags;            /* Message specific flags */
    uint16_t type;             /* Message type */
    uint32_t msg_id;           /* Message id */
} _vn_ctrl_msg_header_t;

/* Structure used for _VN_MSG_NET_UPDATE */
typedef struct
{
    _vn_ctrl_msg_header_t header;
    uint16_t  npeers;  /* Number of peers */
    uint8_t   reason;  /* Reason for this message */
    uint8_t   pad;
    /* Peer pdus follow */
    /* _vn_ctrl_pdu_peer_t peers[]; */
} _vn_ctrl_msg_net_update_t;

/* Generic net message */
typedef struct
{
    _vn_ctrl_msg_header_t header;
    _VN_net_t             netid;
} _vn_ctrl_msg_net_t;

/* Generic status message */
typedef struct
{
    _vn_ctrl_msg_header_t header;
    _vn_ctrl_pdu_status_t status;
} _vn_ctrl_msg_status_t;

/* Structure used for _VN_MSG_NET_JOIN_REQ, _VN_MSG_NET_JOIN_RESP */
/* For now, same as status message */
typedef _vn_ctrl_msg_status_t _vn_ctrl_msg_net_join_t;

/* Structures used for _VN_MSG_NET_NEW_ID_REQ, _VN_MSG_NET_NEW_ID_RESP */
typedef struct
{
    _vn_ctrl_msg_header_t header;
    _VN_net_t             netid;
    _vn_ctrl_pdu_status_t status;
} _vn_ctrl_msg_net_new_id_t;

/* Structure used for _VN_MSG_CONNECT_REQ and _VN_MSG_CONNECT_RESP */
typedef struct
{
    _vn_ctrl_msg_header_t header;
    _VN_net_t             netid;
    _VN_guid_t requestor;
    _VN_guid_t owner;
    /* IP/Port follow, number of IP/Port addresses specified in header.flags */
    /* _vn_ctrl_pdu_addr_t addrs[];  */
} _vn_ctrl_msg_connect_t;

/* Structure used for _VN_MSG_CLOCK_SYNC */
typedef struct
{
    _vn_ctrl_msg_header_t header;
    /* TODO: Use 32 bit quantities? */
    uint64_t sendtime;  /* Time packet was sent */
    int64_t  delta;     /* Clock delta on sender */
} _vn_ctrl_msg_clock_sync_t;

/* Function Prototypes */

/* Network control messages */
int _vn_send_clock_sync(_vn_ctrl_msgid_t msgid, uint8_t flags, 
                        _VN_net_t net_id, 
                        _VN_host_t from_host, _VN_host_t to_host);
int _vn_send_net_id_request(_vn_ctrl_msgid_t msgid, 
                            _VN_net_t dest_net, 
                            _VN_host_t from_host, _VN_host_t to_host,
                            _VN_net_t netmask, 
                            const uint8_t* msg, uint16_t msglen);
int _vn_send_net_id_response(_vn_ctrl_msgid_t msgid, 
                             _VN_net_t dest_net, 
                             _VN_host_t from_host, _VN_host_t to_host,
                             _VN_net_t net_id, int16_t status, 
                             const uint8_t* msg, uint16_t msglen);
int _vn_send_net_update_peers(_vn_ctrl_msgid_t msgid, uint8_t reason,
                              _VN_net_t net_id, 
                              _VN_host_t from_host, _VN_host_t to_host,
                              _vn_ctrl_pdutype_t pdutype,
                              _vn_net_info_t* net_info);
int _vn_send_net_update_peer(_vn_ctrl_msgid_t msgid, uint8_t reason,
                             _VN_net_t net_id, _VN_host_t from_host, 
                             _VN_host_t to_host, _vn_ctrl_pdutype_t pdutype, 
                             _vn_host_info_t* host);
int _vn_send_join_net_request(_vn_ctrl_msgid_t msgid, _VN_net_t net_id,
                              _VN_host_t from_host, _VN_host_t to_host,
                              const uint8_t* msg, uint16_t msglen);
int _vn_send_join_net_response(_vn_ctrl_msgid_t msgid, _VN_net_t net_id,
                               _VN_host_t from_host, _VN_host_t to_host,
                               int16_t status,
                               const uint8_t* msg, uint16_t msglen);
int _vn_send_connect_request(_vn_ctrl_msgid_t msgid, _VN_net_t dest_net,
                             _VN_host_t from_host, _VN_host_t to_host,
                             _VN_net_t req_net_id,
                             _VN_guid_t requestor, _VN_guid_t owner);
int _vn_send_connect_response(_vn_ctrl_msgid_t msgid, _VN_net_t dest_net,
                              _VN_host_t from_host, _VN_host_t to_host,
                              _VN_net_t req_net_id,
                              _VN_guid_t requestor, _VN_guid_t owner,
                              _vn_ctrl_pdutype_t pdu_type,
                              _vn_inaddr_t ip, _vn_inport_t port);
int _vn_send_connect_response_list(_vn_ctrl_msgid_t msgid,
                                   _VN_net_t dest_net, 
                                   _VN_host_t from_host,
                                   _VN_host_t to_host,
                                   _VN_net_t req_net_id,
                                   _VN_guid_t requestor, 
                                   _VN_guid_t owner,
                                   _vn_ctrl_pdutype_t pdu_type,
                                   _vn_dlist_t* pInAddrs);
int _vn_send_status_msg(_vn_ctrl_msgid_t msgid, _vn_ctrl_msgtype_t msgtype,
                        uint8_t msgflags, _VN_net_t net_id,
                        _VN_host_t from_host, _VN_host_t to_host,
                        int16_t status,
                        const uint8_t* msg, uint16_t msglen, bool unblock);
int _vn_send_ping(_vn_ctrl_msgid_t msgid,
                  _vn_ctrl_msgtype_t msgtype, uint8_t msgflags,
                  _VN_net_t dest_net,
                  _VN_host_t from_host, _VN_host_t to_host);
int _vn_send_ctrl_msg(_vn_ctrl_msgid_t msgid, 
                      _vn_ctrl_msgtype_t msgtype, uint8_t msgflags,
                      _VN_net_t dest_net, 
                      _VN_host_t from_host, _VN_host_t to_host,
                      _VN_net_t* req_net_id);
int _vn_proc_ctrl_msg(_vn_msg_t* vn_msg, _vn_inaddr_t addr, _vn_inport_t port);

#ifdef  __cplusplus
}
#endif
 
#endif /* __VN_CTRLMSG_H__ */

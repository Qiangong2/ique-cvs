//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnlocal.h"

/* Platform specific implementation for network communications */
#define _VN_CONNECT_TIMEOUT    10000     /* 10 second timeout */

/* Logs a system error in the socket layer */
void _vn_socket_log_error(int level, const char* msg, _vn_errno_t err,
                          _vn_inaddr_t ip, _vn_inport_t port)
{
    char addr[_VN_INET_ADDRSTRLEN];
    _vn_errstr_t errstr;
    _vn_inet_ntop(&(ip), addr, sizeof(addr));
    errstr = _vn_sys_get_errstr(err);
    if (errstr) {
        _VN_TRACE(level, _VN_SG_SOCKETS,
                  "%s (%s:%u): %s (%d)\n", msg, addr, port, errstr, err);
        _vn_sys_free_errstr(errstr);
    }
    else {
        _VN_TRACE(level, _VN_SG_SOCKETS,
                  "%s (%s:%u): Unknown error %d\n", 
                  msg, addr, port, err);
    }
}

/* Looks up localhost name and stores it in name */
int _vn_getlocalhostname(char* name, size_t len)
{
    int rv;
    assert(name);
#ifdef _WIN32
    rv = gethostname(name, (int) len);
#else
    rv = gethostname(name, len);
#endif
    if (rv == _VN_SOCKET_ERROR) {
        _vn_socket_log_error(TRACE_ERROR, 
                             "_vn_getlocalhostname: Error looking up localhost",
                             _vn_socket_get_errno(), 0, 0);
        return _VN_ERR_SOCKET;
    }
    else return _VN_ERR_OK;
}

/* Looks up the local port that sockfd corresponds to */
_vn_inport_t _vn_getport(_vn_socket_t sockfd)
{
    struct sockaddr_in sin;
    socklen_t socklen;
    int rv;

    memset(&sin, 0, sizeof(sin));
    socklen = sizeof(sin);

    rv = getsockname(sockfd, (struct sockaddr*) &sin, &socklen);
    if ((rv == _VN_SOCKET_ERROR) || (sin.sin_port == 0)) {
        /* Try to bind port if there was error getting port number */
        _vn_socket_bind(sockfd, _VN_INADDR_INVALID, 0);
        rv = getsockname(sockfd, (struct sockaddr*) &sin, &socklen);        
    }
    if (rv == _VN_SOCKET_ERROR) {
        _vn_socket_log_error(TRACE_ERROR,
                             "_vn_getlocalport: Error looking up port",
                             _vn_socket_get_errno(), 0, 0);
        return _VN_INPORT_INVALID;
    }

    return ntohs(sin.sin_port);
}

/* 
 * Returns the IP address of the local machine 
 * On windows - returns the IP address corresponding to the local hostname
 * On linux - returns the IP address of the first non-loopback interface
 */
_vn_inaddr_t _vn_getlocaladdr() 
{
#ifdef _WIN32
    char name[255];
    int rv = gethostname(name, (int) sizeof(name));
    if (rv == _VN_SOCKET_ERROR) {
        return _VN_INADDR_INVALID;
    }
    else {
        return _vn_getaddr(name);
    }
#else
    char szBuffer[_VN_MAX_INTERFACES * sizeof( struct ifreq )];
    _vn_socket_t sockfd;
    struct ifconf ifConf;
    struct ifreq ifReq;
    struct sockaddr_in local_addr;
    int rv, i;

    // Create an unbound datagram socket to do the SIOCGIFADDR ioctl on. 
    sockfd = socket( AF_INET, SOCK_DGRAM, IPPROTO_UDP );
    if (sockfd == _VN_SOCKET_INVALID) {
        _vn_socket_log_error(TRACE_ERROR, 
                             "_vn_getlocaladdr: Error creating socket",
                             _vn_socket_get_errno(), 0, 0);
        return _VN_INADDR_INVALID;
    }

    // Get the interface configuration information... 
    ifConf.ifc_len = sizeof szBuffer;
    ifConf.ifc_ifcu.ifcu_buf = ( caddr_t ) szBuffer;
    rv = ioctl( sockfd, SIOCGIFCONF, &ifConf );

    if( rv == _VN_SOCKET_ERROR ) {
        _vn_socket_log_error(TRACE_ERROR, 
                             "_vn_getlocaladdr: SIOGIFCONF error",
                             rv, 0, 0);
        return _VN_INADDR_INVALID;
    }

    memset(&local_addr, 0, sizeof(local_addr));
    // Cycle through the list of interfaces looking for IP addresses. 
    for( i = 0; i < ifConf.ifc_len; ) {
        struct ifreq *pifReq =
            ( struct ifreq * )( ( caddr_t ) ifConf.ifc_req + i );
        i += sizeof *pifReq;

        // See if this is the sort of interface we want to deal with.
        strcpy( ifReq.ifr_name, pifReq->ifr_name );
        if( ioctl( sockfd, SIOCGIFFLAGS, &ifReq ) == _VN_SOCKET_ERROR ) {
            _VN_TRACE(TRACE_WARN, _VN_SG_SOCKETS,
                      "Can't get interface flags for %s:\n",
                      ifReq.ifr_name );            
        }

        // Skip loopback, point-to-point and down interfaces, 
        // except don't skip down interfaces
        // if we're trying to get a list of configurable interfaces. 
        if( ( ifReq.ifr_flags & IFF_LOOPBACK )
            || ( !( ifReq.ifr_flags & IFF_UP ) ) ) {
            continue;
        }
        else if( pifReq->ifr_addr.sa_family == AF_INET ) {
            // Get a pointer to the address...
            memcpy( &local_addr, &pifReq->ifr_addr,
                    sizeof pifReq->ifr_addr );

            // Stop if it's not the loopback interface. 
            if( local_addr.sin_addr.s_addr &&
                local_addr.sin_addr.s_addr != htonl( INADDR_LOOPBACK ) ) {
                break;
            }
            
        }
    }

    return local_addr.sin_addr.s_addr;
#endif
}

/* 
 * Returns the list of IP addresses of the local machine 
 */
int _vn_getlocaladdrlist_impl(_vn_inaddr_t** pp_addrs,
                              _vn_inaddr_t** pp_netmasks)
{
	int tempresults[_VN_MAX_INTERFACES];
	int tempnetmasks[_VN_MAX_INTERFACES];
	int ctr=0;

#ifdef _WIN32
    MIB_IPADDRTABLE*   pft;
    DWORD  tableSize = 0;
    
    GetIpAddrTable (NULL, &tableSize, FALSE);
    pft = (MIB_IPADDRTABLE*) _vn_malloc(tableSize);
    if (pft) { 
        if (GetIpAddrTable (pft, &tableSize, TRUE) == NO_ERROR) {
            unsigned int i;

            for (i = 0; i < pft->dwNumEntries; i++) {
                if (pft->table[i].dwAddr &&
                    pft->table[i].dwAddr != htonl( INADDR_LOOPBACK )) {
                    tempresults[ctr] = pft->table[i].dwAddr;
                    tempnetmasks[ctr] = pft->table[i].dwMask;
                    ctr++;
                }
            }
        }
        _vn_free(pft);
    }
#else
	//
	// Posix will use an Ioctl call to get the IPAddress List
	//
	char szBuffer[_VN_MAX_INTERFACES*sizeof(struct ifreq)];
	struct ifconf ifConf;
	struct ifreq ifReq;
	int nResult;
	int LocalSock;
	struct sockaddr_in LocalAddr;
	int i;

    assert(pp_addrs);

	/* Create an unbound datagram socket to do the SIOCGIFADDR ioctl on. */
	if ((LocalSock = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
	{
        _vn_socket_log_error(TRACE_ERROR, 
                             "_vn_getlocaladdrlist: Error creating socket",
                             _vn_socket_get_errno(), 0, 0);
        return _VN_ERR_SOCKET;
	}
	/* Get the interface configuration information... */
	ifConf.ifc_len = sizeof szBuffer;
	ifConf.ifc_ifcu.ifcu_buf = (caddr_t)szBuffer;
	nResult = ioctl(LocalSock, SIOCGIFCONF, &ifConf);
	if (nResult == _VN_SOCKET_ERROR)
	{
        _vn_socket_log_error(TRACE_ERROR, 
                             "_vn_getlocaladdrlist: SIOGIFCONF error",
                             _vn_socket_get_errno(), 0, 0);
        close(LocalSock);
        return _VN_ERR_SOCKET;
	}
	/* Cycle through the list of interfaces looking for IP addresses. */
	for (i = 0;(i < ifConf.ifc_len);)
	{
		struct ifreq *pifReq = (struct ifreq *)((caddr_t)ifConf.ifc_req + i);
		i += sizeof *pifReq;
		/* See if this is the sort of interface we want to deal with. */
		strcpy (ifReq.ifr_name, pifReq -> ifr_name);
		if (ioctl (LocalSock, SIOCGIFFLAGS, &ifReq) == _VN_SOCKET_ERROR)
		{
            _VN_TRACE(TRACE_WARN, _VN_SG_SOCKETS,
                      "Can't get interface flags for %s:\n",
                      ifReq.ifr_name );            
            return _VN_ERR_SOCKET;
		}
		/* Skip loopback, point-to-point and down interfaces, */
		/* except don't skip down interfaces */
		/* if we're trying to get a list of configurable interfaces. */
		if ((ifReq.ifr_flags & IFF_LOOPBACK) || (!(ifReq.ifr_flags & IFF_UP)))
		{
			continue;
		}	
        if (pp_netmasks) {
            if (ioctl (LocalSock, SIOCGIFNETMASK, &ifReq) == _VN_SOCKET_ERROR)
            {
                _VN_TRACE(TRACE_WARN, _VN_SG_SOCKETS,
                          "Can't get netmask for %s:\n",
                          ifReq.ifr_name );            
                return _VN_ERR_SOCKET;
            }
        }
		if (pifReq->ifr_addr.sa_family == AF_INET)
		{
			/* Get a pointer to the address... */
			memcpy (&LocalAddr, &pifReq -> ifr_addr, sizeof pifReq -> ifr_addr);
			if (LocalAddr.sin_addr.s_addr &&
                LocalAddr.sin_addr.s_addr != htonl (INADDR_LOOPBACK))
			{
				tempresults[ctr] = LocalAddr.sin_addr.s_addr;

                if (pp_netmasks) {
                    if (ifReq.ifr_netmask.sa_family == AF_INET) {
                        memcpy (&LocalAddr, &ifReq.ifr_netmask, 
                                sizeof ifReq.ifr_netmask);
                        tempnetmasks[ctr] = LocalAddr.sin_addr.s_addr;
                    }
                    else {
                        tempnetmasks[ctr] = 0xffffffff;
                    }
                }

				++ctr;
			}
		}
	}
	close(LocalSock);
#endif

	*pp_addrs = (_vn_inaddr_t*) _vn_malloc(sizeof(_vn_inaddr_t)*(ctr));
    if (*pp_addrs == NULL) {
        return _VN_ERR_NOMEM;
    }
	memcpy(*pp_addrs,tempresults,sizeof(_vn_inaddr_t)*ctr);
    if (pp_netmasks) {
        *pp_netmasks = (_vn_inaddr_t*) _vn_malloc(sizeof(_vn_inaddr_t)*(ctr));
        if (*pp_netmasks) {
            memcpy(*pp_netmasks,tempnetmasks,sizeof(_vn_inaddr_t)*ctr);
        }
    }
	return(ctr);
}

/** 
 * Returns the list of IP addresses of the local machine 
 */
int _vn_getlocaladdrlist(_vn_inaddr_t** pp_addrs)
{
    return _vn_getlocaladdrlist_impl(pp_addrs, NULL);
}


/**
 * Returns the local ip of interface with the same subnet as  
 * the destination address
 */
_vn_inaddr_t _vn_choose_local_ip(_vn_inaddr_t remote_ip)
{
    _vn_inaddr_t local_ip = _VN_INADDR_INVALID;
    char local_addr[_VN_INET_ADDRSTRLEN];
    char remote_addr[_VN_INET_ADDRSTRLEN];
    _vn_inaddr_t *addrs = NULL, *netmasks = NULL;
    int rv, i;

    _vn_inet_ntop(&(remote_ip), remote_addr, sizeof(remote_addr));

    if (remote_ip == htonl( INADDR_LOOPBACK )) {
        local_ip = remote_ip;
    } else {
        rv = _vn_getlocaladdrlist_impl(&addrs, &netmasks);
        if (rv < 0) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_SOCKETS,
                      "Error %d getting local ip to use for remote host %s\n",
                      remote_addr);
            
            return rv;
        }
        
        if (addrs && netmasks) {
            for (i = 0; i < rv; i++) {
                _VN_TRACE(TRACE_FINER, _VN_SG_SOCKETS, "_vn_choose_local_ip: "
                          "local ip 0x%08x, netmask 0x%08x, remote_ip 0x%08x\n", 
                          addrs[i], netmasks[i], remote_ip);
                if (addrs[i]) {
                    if ((addrs[i] & netmasks[i]) == (remote_ip & netmasks[i])) {
                        local_ip = addrs[i];
                        break;
                    }
                }
            }
        }
        
        if (addrs) {
            _vn_free(addrs);
        }
        if (netmasks) {
            _vn_free(netmasks);
        }
    }
    
    _vn_inet_ntop(&(local_ip), local_addr, sizeof(local_addr));

    _VN_TRACE(TRACE_FINE, _VN_SG_SOCKETS,
              "_vn_choose_local_ip: Got local ip %s for remote host %s\n", 
              local_addr, remote_addr);
    
    return local_ip;
}

/**
 * Returns the default gateway
 */
_vn_inaddr_t _vn_get_default_gateway()
{
    _vn_inaddr_t gw = _VN_INADDR_INVALID;
#if defined(_WIN32)
    MIB_IPFORWARDTABLE*   pft;
    DWORD  tableSize = 0;
 
    GetIpForwardTable (NULL, &tableSize, FALSE);
    pft = (MIB_IPFORWARDTABLE*) _vn_malloc(tableSize);
    if (pft) { 
        if (GetIpForwardTable (pft, &tableSize, TRUE) == NO_ERROR) {
            unsigned int i;
            for (i = 0; i < pft->dwNumEntries; i++) {
                if (pft->table[i].dwForwardDest == 0) {
                    gw = pft->table[i].dwForwardNextHop;
                }
            }
        }
        _vn_free(pft);
    }
#elif defined(_LINUX)
    /* Linux: read from /proc/net/route */
    FILE *in;
    char line[256];

    if (!(in = fopen("/proc/net/route", "r"))) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_SOCKETS,
                  "Cannot open /proc/net/route\n");
        return gw;
    }

    while (fgets(line, sizeof(line), in)) {
        struct rtentry  rtent;
        struct rtentry  *rt;
        char            rtent_name[32];
        int             refcnt, flags, metric;
        unsigned        use;
        struct sockaddr_in *sockaddr;

        rt = &rtent;
        memset((char *) rt, (0), sizeof(*rt));
        rt->rt_dev = rtent_name;

        /*
         * as with 1.99.14:
         * Iface Dest GW Flags RefCnt Use Metric Mask MTU Win IRTT
         * eth0 0A0A0A0A 00000000 05 0 0 0 FFFFFFFF 1500 0 0 
         */
        if (8 != sscanf(line, "%s %x %x %x %u %d %d %x %*d %*d %*d\n",
                        rt->rt_dev,
                        &(((struct sockaddr_in *) &(rtent.rt_dst))->
                          sin_addr.s_addr),
                        &(((struct sockaddr_in *) &(rtent.rt_gateway))->
                          sin_addr.s_addr),
                        /*
                         * XXX: fix type of the args 
                         */
                        &flags, &refcnt, &use, &metric,
                        &(((struct sockaddr_in *) &(rtent.rt_genmask))->
                          sin_addr.s_addr))) {
            continue;
        }

        rt->rt_flags = flags;
        rt->rt_metric = metric;
    
        sockaddr = ((struct sockaddr_in *) &(rtent.rt_gateway));
        if (sockaddr->sin_addr.s_addr) {
            gw = sockaddr->sin_addr.s_addr;
        }
    }
#endif
    return gw;
}

/* Returns ipv4 address  based on hostname */
_vn_inaddr_t _vn_getaddr(const char* hostname)
{
    struct addrinfo ai_hints;
    struct addrinfo *ai_res = NULL, *ai = NULL;
    int rv;
    _vn_inaddr_t addr = _VN_INADDR_INVALID;

    if (hostname == NULL) {
        return _VN_INADDR_INVALID;
    }

    memset(&ai_hints, 0, sizeof(ai_hints));
    ai_hints.ai_family = PF_INET;
    
    rv = getaddrinfo(hostname, "", &ai_hints, &ai_res);
    if (rv == _VN_SOCKET_ERROR) {
        return _VN_INADDR_INVALID;
    }

    ai = ai_res;
    while (ai != NULL) {
        if (ai->ai_family == PF_INET) {
            struct sockaddr_in * sockAddrPtr = 
                (struct sockaddr_in *) ai->ai_addr;
            
            if (sockAddrPtr) {
                addr = sockAddrPtr->sin_addr.s_addr;
                if (addr != htonl( INADDR_LOOPBACK )) {
                    break;
                }
            }
        }
        ai = ai->ai_next;
    }
    freeaddrinfo(ai_res);
    return addr;
}

/* Binds socket to specified port */
int _vn_socket_bind(_vn_socket_t sockfd, _vn_inaddr_t addr, _vn_inport_t port)
{
    int rv;
    struct sockaddr_in sin;

    sin.sin_family = PF_INET;
    sin.sin_port = htons(port);
    if (addr == _VN_INADDR_INVALID) {
        sin.sin_addr.s_addr = htonl(INADDR_ANY);
    }
    else {
        sin.sin_addr.s_addr = addr;
    }
    if (_VN_TRACE_ON(TRACE_FINER, _VN_SG_SOCKETS)) {
        char addrstr[_VN_INET_ADDRSTRLEN];
        _vn_inet_ntop(&(addr), addrstr, sizeof(addrstr));
        _VN_TRACE(TRACE_FINE, _VN_SG_SOCKETS,
                  "Attempting to bind to %s:%u\n", addrstr, port);
    }
    rv = bind(sockfd, (struct sockaddr*) &sin, sizeof(sin));
    if (rv == _VN_SOCKET_ERROR) {
        _vn_socket_log_error(TRACE_ERROR,
                             "_vn_socket_bind: Error binding socket",
                             _vn_socket_get_errno(),
                             sin.sin_addr.s_addr, port);
        rv = _VN_ERR_SOCKET;
    }
    return rv;
}

/* Opens a new socket and binds it */
_vn_socket_t _vn_socket(int type, _vn_inaddr_t addr, _vn_inport_t port,
                        int nonblock)
{
    _vn_socket_t sockfd;
 
    sockfd = socket(PF_INET, type, 0);
    if (sockfd == _VN_SOCKET_INVALID) {
        _vn_socket_log_error(TRACE_ERROR, 
                             "_vn_socket: Error creating socket",
                             _vn_socket_get_errno(), addr, port);
        return sockfd;
    }
#ifdef _WIN32
    /* From: http://support.microsoft.com/kb/263823
     * Disable "new behavior" using IOCTL: SIO_UDP_CONNRESET. Without this call
     * recvfrom() can fail, repeatedly, after a bad sendto() call.     
     */
    else {
        /* TODO: Need to check window version?
                 Problem introduced in Windows 2000 and fix was added 
                 for Windows 2000 SP2 and above. */
        DWORD dwBytesReturned = 0;
        BOOL  bNewBehavior = FALSE;
        DWORD status = WSAIoctl(sockfd, SIO_UDP_CONNRESET,
                                &bNewBehavior, sizeof(bNewBehavior),
                                NULL, 0, &dwBytesReturned,NULL,NULL);
    }
#endif

    /* Set socket to nonblocking */
    if (nonblock) {
#ifdef _WIN32
        ioctlsocket(sockfd, FIONBIO, (u_long FAR*) &nonblock);
#else
        int oldopts;
        oldopts = fcntl(sockfd, F_GETFL, 0);
        fcntl(sockfd, F_SETFL, oldopts | O_NONBLOCK);
#endif
    }

    return sockfd;
}

/* Opens a new UDP socket and binds it */
_vn_socket_t _vn_socket_udp(_vn_inaddr_t addr, _vn_inport_t port)
{
    _vn_socket_t sockfd;
    int nonblock = 1;

    sockfd = _vn_socket(SOCK_DGRAM, addr, port, nonblock);

    if (sockfd != _VN_SOCKET_INVALID) {
        if ((addr != _VN_INADDR_INVALID) || (port != _VN_INADDR_INVALID)) {
            int rv = _vn_socket_bind(sockfd, addr, port);
            if (rv < 0) {
                _vn_close_socket(sockfd);
                return _VN_SOCKET_INVALID;
            }
        }
    }

    return sockfd;
}


/* Opens a new TCP socket and binds it */
_vn_socket_t _vn_socket_tcp(_vn_inaddr_t addr, _vn_inport_t port)
{
    _vn_socket_t sockfd;
    int nonblock = 1;
    int reuse = 1;
    int nodelay = 1;

    sockfd = _vn_socket(SOCK_STREAM, addr, port, nonblock);

    if (sockfd != _VN_SOCKET_INVALID) {
        /* Set socket reuse addr */
        setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, 
                   (void*) &reuse, sizeof(reuse));

        /* Set TCP no delay */
        setsockopt(sockfd, IPPROTO_TCP, TCP_NODELAY, 
                   (void*) &nodelay, sizeof(nodelay));

        if ((addr != _VN_INADDR_INVALID) || (port != _VN_INADDR_INVALID)) {
            int rv = _vn_socket_bind(sockfd, addr, port);
            if (rv < 0) {
                _vn_close_socket(sockfd);
                return _VN_SOCKET_INVALID;
            }
        }
    }

    return sockfd;
}

/* Receive functions */
int _vn_recv(_vn_socket_t sockfd, void* buf, size_t len, 
             _vn_inaddr_t* sender_addr, _vn_inport_t* sender_port){
    int rv;
    struct sockaddr_in sin;
    socklen_t size = sizeof(sin);
    char addr[_VN_INET_ADDRSTRLEN];

    assert(sockfd != _VN_SOCKET_INVALID);
    memset(&sin, 0, sizeof(sin));
#ifdef _WIN32
    rv = recvfrom(sockfd, buf, (int) len, 0, (struct sockaddr*) &sin, &size);
#else
    rv = recvfrom(sockfd, buf, len, 0, (struct sockaddr*) &sin, &size);
#endif
    if (rv == _VN_SOCKET_ERROR) {
        _vn_errno_t err = _vn_socket_get_errno();
        _vn_socket_log_error(TRACE_ERROR, "_vn_recv: Error reading",
                             err, sin.sin_addr.s_addr,
                             ntohs(sin.sin_port));
        return _VN_ERR_SOCKET;
    }
    _vn_inet_ntop(&(sin.sin_addr), addr, sizeof(addr));
    _VN_TRACE(TRACE_FINEST, _VN_SG_SOCKETS,
              "_vn_recv: got %d bytes from %s:%u\n",
              rv, addr, ntohs(sin.sin_port));

    if (sender_addr) {
        *sender_addr = sin.sin_addr.s_addr;
    }
    if (sender_port) {
        *sender_port = ntohs(sin.sin_port);
    }
    if (rv > 0)
        _vn_dbg_save_buf(_VN_DBG_RECV_PKT, buf, rv);
    return rv;
}

/* Version of select that takes in number of seconds as timeout */
int _vn_select(int n, fd_set *readfds, fd_set *writefds, fd_set *exceptfds,
               uint32_t msecs)
{
    struct timeval timeout;
    int rv;

    if (msecs == _VN_TIMEOUT_NONE)
    {
        /* Wait forever */
        rv = select(n, readfds, writefds, exceptfds, NULL);
    }
    else {
        /* Just wait for so many seconds */
        timeout.tv_sec = msecs/1000;
        timeout.tv_usec = (msecs % 1000)*1000;
        rv = select(n, readfds, writefds, exceptfds, &timeout);
    }

    if (rv == 0) {
        return _VN_ERR_TIMEOUT;
    }
    else if (rv == _VN_SOCKET_ERROR) {
        _vn_socket_log_error(TRACE_ERROR, "_vn_select: Error doing select",
                             _vn_socket_get_errno(), 0, 0);
        return _VN_ERR_SOCKET;
    }
    return rv;
}

/* _vn_recv with timeout */
int _vn_recv_wait(_vn_socket_t sockfd, void* buf, size_t len, uint32_t msecs,
                  _vn_inaddr_t* sender_addr, _vn_inport_t* sender_port)
{
    fd_set rset;
    int maxfd;
    int rv;
    
    assert(sockfd != _VN_SOCKET_INVALID);
    FD_ZERO(&rset);
    FD_SET(sockfd, &rset);
    maxfd = (int) sockfd + 1;

    rv = _vn_select(maxfd, &rset, NULL, NULL, msecs);
    
    if (rv > 0) {
        if (FD_ISSET(sockfd, &rset)) {
            rv = _vn_recv(sockfd, buf, len, sender_addr, sender_port);
        }
        else {
            rv = _VN_ERR_TIMEOUT;
        }
    }
    return rv;
}

/* Attempt to read len bytes from sockets (for TCP) */
int _vn_read_socket(_vn_socket_t sockfd, 
                    void* buf, size_t len, uint32_t timeout)
{
    int rv = _VN_ERR_OK;
    size_t bytesread = 0; /* Bytes read so far */

    while (bytesread < len) {
        rv = _vn_recv_wait(sockfd, 
                           (char*) buf + bytesread, len - bytesread,
                           timeout, NULL, NULL);
        if (rv > 0) {
            bytesread += rv;
        }
        else {
            break;
        }
    }
    if (rv >= 0) {
        rv = (int) bytesread;
    }

    _VN_TRACE(TRACE_FINEST, _VN_SG_SOCKETS,
              "Read %d bytes from socket (rv = %d)\n", bytesread, rv);

    return rv;
}

/* Attempt to write len bytes to sockets (for TCP) */
int _vn_send_socket(_vn_socket_t sockfd, const void* msg, _VN_msg_len_t len,
                    uint32_t timeout)
{
    int rv;
    _VN_time_t starttime = _vn_get_timestamp();
    _VN_time_t endtime = starttime + timeout;

    assert(sockfd != _VN_SOCKET_INVALID);
again:
    rv = send(sockfd, msg, len, 0);
    if (rv == _VN_SOCKET_ERROR) {
        _vn_errno_t err = _vn_socket_get_errno();
        if (err == _VN_ERRNO_EAGAIN && _vn_get_timestamp() < endtime) {
            /* Try to send again */
            goto again;
        }
        _vn_socket_log_error(TRACE_ERROR, "_vn_send: Error sending packet",
                             err, 0, 0);
        return _VN_ERR_SOCKET;
    }
    else return rv;
}

static uint8_t _VN_TCP_FRAME_MAGIC[] = { 0x41, 0x67 };

/* Read from socket, with simple framing (for TCP) */
int _vn_read_socket_wframing(_vn_socket_t sockfd, 
                             void* buf, size_t len, uint32_t timeout)
{
    int rv;
    _vn_tcp_frame_header_t header;
    
    /* Simple framing: 1st 2 bytes is magic number, next 16-bit 
       is number of bytes to follow */
    rv = _vn_read_socket(sockfd, &header, sizeof(header), timeout);

    if (rv > 0) {
        /* Check magic */
        if (header.magic[0] != _VN_TCP_FRAME_MAGIC[0] ||
            header.magic[1] != _VN_TCP_FRAME_MAGIC[1]) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_SOCKETS,
                      "Invalid TCP frame header magic: "
                      "got 0x%x%x, expected 0x%x%x\n", 
                      header.magic[0], header.magic[1], 
                      _VN_TCP_FRAME_MAGIC[0], _VN_TCP_FRAME_MAGIC[1]);
            return _VN_ERR_FAIL;
        }
        header.length = ntohs(header.length);
        _VN_TRACE(TRACE_FINEST, _VN_SG_SOCKETS, 
                  "Reading %d bytes (socket with framing)\n", header.length);
        if (header.length <= len) {
            /* Blocking read */
            rv = _vn_read_socket(sockfd, buf, header.length, _VN_TIMEOUT_NONE);
        } else {
            _VN_TRACE(TRACE_ERROR, _VN_SG_SOCKETS,
                      "TCP packet too large: %d bytes for buffer of %d bytes\n",
                      header.length, len);
            return _VN_ERR_FAIL;
        }
    }

    return rv;
}

/* Write to socket, with simple framing (for TCP) */
int _vn_write_socket_wframing(_vn_socket_t sockfd, 
                              const void* buf, size_t len, uint32_t timeout)
{
    int rv;
    _vn_tcp_frame_header_t header;

    _VN_TRACE(TRACE_FINEST, _VN_SG_SOCKETS, 
              "Writing %d bytes (socket with framing)\n", len);

    /* Simple framing: 1st 2 bytes is magic number, next 16-bit 
       is number of bytes to follow */
    assert(len <= _VN_TCP_FRAME_MAX_DATA_LEN);

    header.magic[0] = _VN_TCP_FRAME_MAGIC[0];
    header.magic[1] = _VN_TCP_FRAME_MAGIC[1];
    header.length = htons( (uint16_t) len);
   
    rv = _vn_send_socket(sockfd, &header, sizeof(header), timeout);

    /* Send actual data */
    if (rv >= 0) {
        rv = _vn_send_socket(sockfd, buf, (_VN_msg_len_t) len, timeout);
    }
    return rv;
}

/* Send functions */
int _vn_sendto(_vn_socket_t sockfd, _vn_inaddr_t ip_addr, _vn_inport_t udp_port,
               const void* msg, _VN_msg_len_t len)
{
    struct sockaddr_in sin;
    int rv;
    
    assert(sockfd != _VN_SOCKET_INVALID);
    memset(&sin, 0, sizeof(sin));
    sin.sin_family = AF_INET;
    sin.sin_port = htons(udp_port);
    sin.sin_addr.s_addr = ip_addr;
    rv = sendto(sockfd, msg, len, 0,
                (struct sockaddr*) &sin, sizeof(sin));

    if (rv == _VN_SOCKET_ERROR) {
        _vn_socket_log_error(TRACE_ERROR, "_vn_sendto: Error sending packet",
                             _vn_socket_get_errno(), ip_addr, udp_port);
        return _VN_ERR_SOCKET;
    }
    else return rv;
}

/* Sends the buffer to a list of addresses */
int _vn_sendto_list(_vn_socket_t sockfd, _vn_dlist_t* list, 
                    const void* msg, _VN_msg_len_t len)
{
    int rv = 0;
    if (list) {
        _vn_dnode_t* node;
        _vn_dlist_for(list, node)
        {
            _vn_sockaddr_t* sockaddr;
            sockaddr = _vn_dnode_value(node);
            if (sockaddr) {
                if (_vn_sendto(sockfd, sockaddr->addr, sockaddr->port,
                               msg, len) >= 0) {
                    rv++;
                }
            }
        }
    }
    return rv;
}

int _vn_send(_vn_socket_t sockfd, const void* msg, _VN_msg_len_t len)
{
    int rv;
    assert(sockfd != _VN_SOCKET_INVALID);
    rv = send(sockfd, msg, len, 0);
    if (rv == _VN_SOCKET_ERROR) {
        _vn_socket_log_error(TRACE_ERROR, "_vn_send: Error sending packet",
                             _vn_socket_get_errno(), 0, 0);
        return _VN_ERR_SOCKET;
    }
    else return rv;
}

/* Listen */
int _vn_listen(_vn_socket_t sockfd)
{
    int rv;

    assert(sockfd != _VN_SOCKET_INVALID);
    rv = listen(sockfd, 10 /* default backlog */);
    if (rv == _VN_SOCKET_ERROR) {
        _vn_socket_log_error(TRACE_ERROR, "_vn_listen: ",
                             _vn_socket_get_errno(), 0, 0);
        return _VN_ERR_SOCKET;
    }
    return _VN_ERR_OK;
}

/* Accept */               
_vn_socket_t _vn_accept(_vn_socket_t sockfd, uint32_t msecs,
                        _vn_inaddr_t* addr, _vn_inport_t* port)
{
    fd_set rfds;
    struct sockaddr_in cliaddr;
    socklen_t clilen;
    _vn_socket_t connfd = _VN_SOCKET_INVALID;
    int rv;

    assert(sockfd != _VN_SOCKET_INVALID);

    FD_ZERO(&rfds);
    FD_SET(sockfd, &rfds);

    rv = _vn_select((int) sockfd + 1, &rfds, NULL, NULL, msecs);
    if (rv >= 0) {
        if (FD_ISSET(sockfd, &rfds)) {
            memset(&cliaddr, 0, sizeof(cliaddr));
            clilen = sizeof(cliaddr);
            connfd = accept(sockfd, (struct sockaddr*) &cliaddr, &clilen);
            if (connfd == _VN_SOCKET_ERROR) {
                _vn_socket_log_error(TRACE_ERROR, "_vn_accept: ",
                                     _vn_socket_get_errno(), 0, 0);
                
                connfd = _VN_SOCKET_INVALID;;
            }
            else {
                if (addr) {
                    *addr = cliaddr.sin_addr.s_addr;
                }
                if (port) {
                    *port = ntohs(cliaddr.sin_port);
                }
            }
        }
    }
    return connfd;
}

/* Accept from multiple sockets */
_vn_socket_t _vn_accept_multi(_vn_socket_t sockfds[], int nfds, int32_t msecs,
                              _vn_socket_t* selected_sock,
                              _vn_inaddr_t* addr, _vn_inport_t* port)
{
    fd_set rfds;
    struct sockaddr_in cliaddr;
    socklen_t clilen;
    _vn_socket_t sockfd, maxfd = _VN_SOCKET_INVALID;
    _vn_socket_t connfd = _VN_SOCKET_INVALID;
    int rv, i, n = 0;


    FD_ZERO(&rfds);
    for (i = 0; i < nfds; i++) {
        sockfd = sockfds[i];
        if (sockfd != _VN_SOCKET_INVALID) {
            n++;
            FD_SET(sockfd, &rfds);
            if ((n == 0) || (sockfd > maxfd)) {
                maxfd = sockfd;
            }
        }
    }

    if (n == 0) {
        /* Just sleep and return */
        _vn_thread_sleep(msecs);
        return _VN_SOCKET_INVALID;
    }

    rv = _vn_select((int) maxfd + 1, &rfds, NULL, NULL, msecs);
    if (rv >= 0) {
        for (i = 0; i < nfds; i++) {
            sockfd = sockfds[i];
            if (sockfd != _VN_SOCKET_INVALID && FD_ISSET(sockfd, &rfds)) {
                memset(&cliaddr, 0, sizeof(cliaddr));
                clilen = sizeof(cliaddr);
                connfd = accept(sockfd, (struct sockaddr*) &cliaddr, &clilen);
                if (connfd == _VN_SOCKET_ERROR) {
                    _vn_socket_log_error(TRACE_ERROR, "_vn_accept: ",
                                         _vn_socket_get_errno(), 0, 0);
                    
                    connfd = _VN_SOCKET_INVALID;;
                }
                else {
                    if (addr) {
                        *addr = cliaddr.sin_addr.s_addr;
                    }
                    if (port) {
                        *port = ntohs(cliaddr.sin_port);
                    }
                    if (selected_sock) {
                        *selected_sock = sockfd;
                    }
                    break;
                }
            }
        }
    }
    return connfd;
}

/* Connect (blocking) */
int _vn_connect(_vn_socket_t sockfd,
                _vn_inaddr_t ipaddr, _vn_inport_t port)
{
    struct sockaddr_in sin;
    int rv;

    assert(sockfd != _VN_SOCKET_INVALID);

    memset(&sin, 0, sizeof(sin));
    sin.sin_family = PF_INET;
    sin.sin_port = htons(port);
    sin.sin_addr.s_addr = ipaddr;

    rv = connect(sockfd, (struct sockaddr*) &sin, sizeof(sin));
    if (rv != _VN_SOCKET_ERROR) {
        if (_VN_TRACE_ON(TRACE_FINER, _VN_SG_SOCKETS)) {
            char addr[_VN_INET_ADDRSTRLEN];
            _vn_inet_ntop(&(ipaddr), addr, sizeof(addr));
            _VN_TRACE(TRACE_FINE, _VN_SG_SOCKETS,
                      "Connected to %s:%u\n", addr, port);
        }
        return _VN_ERR_OK;
    }
    else {
        _vn_errno_t err = _vn_socket_get_errno();
#ifdef _WIN32
        if (err == WSAEWOULDBLOCK) {
#else
        if (err == EINPROGRESS) {
#endif
            /* For non-blocking socket, need to check for completion */
            fd_set wfds;
            socklen_t optlen = sizeof(err);


            FD_ZERO(&wfds);
            FD_SET(sockfd, &wfds);

            rv = _vn_select((int) sockfd + 1, NULL, &wfds, NULL, 
                            _VN_CONNECT_TIMEOUT);
            rv = getsockopt(sockfd, SOL_SOCKET, SO_ERROR,
                            (void*) &err, &optlen);
        }
        
        if (err) {
            _vn_socket_log_error(TRACE_ERROR, "_vn_connect: Error connecting ",
                                 err, ipaddr, port);
            return _VN_ERR_SOCKET;
        }
        else {
            return _VN_ERR_OK;
        }
    }
}

//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#ifdef UPNP

#include "upnp.h"
#include "upnptools.h"

#ifndef __VN_UPNP_PRIV_H__
#define __VN_UPNP_PRIV_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Private definitions for UPNP support */

/* Constants */
#define IN
#define OUT

#define  _VN_UPNP_NAT_SERVICES                    1
#define  _VN_UPNP_NAT_SERVICE_WANIPCONN           0

#define _VN_UPNP_VN_SERVICES                      1
#define _VN_UPNP_VN_SERVICE_CONTROL               0

#define _VN_UPNP_PORT                 0
#define _VN_UPNP_WEB_DIR              "./web"

#define _VN_UPNP_UDN_SIZE                       32

#define _VN_UPNP_NAT_SEARCH_TIMEOUT_SECS        60

#define _VN_UPNP_DEVICE_SEARCH_TIMEOUT_SECS     20
#define _VN_UPNP_DEVICE_QUERY_TIMEOUT_SECS      60
#define _VN_UPNP_DEVICE_ADVERTISE_TIMEOUT_SECS  300 /*1800 */
#define _VN_UPNP_DEVICE_SUBSCRIBE_TIMEOUT_SECS  300 /*1800 */
#define _VN_UPNP_DEVICE_LINGER_SECS             120

#define _VN_UPNP_VN_MAXACTIONS                 1
#define _VN_UPNP_VN_MAXVARS                    6

#define _VN_UPNP_MAX_DEVICES           _VN_NETIF_MAX

#define _VN_UPNP_FLAG_ADVERTISE        0x01
#define _VN_UPNP_FLAG_DISCOVER         0x02

#define _VN_STR_GET_SERVICES          "GetServiceList"
#define _VN_STR_UDN                   "UDN"
#define _VN_STR_GUID                  "GUID"
#define _VN_STR_ADDED_SERVICES        "AddedServices"
#define _VN_STR_REMOVED_SERVICES      "RemovedServices"
#define _VN_STR_SERVICE_LIST          "ServiceList"

#define _VN_STR_ADDED_DEVICES         "AddedDevices"
#define _VN_STR_REMOVED_DEVICES       "RemovedDevices"

/* Data structures for acting as UPnP device */

typedef struct {
    char* deviceType;
    char* friendlyName;
    char* manufacturer;
    char* manufacturerURL;
} _vn_upnp_device_desc_t;

typedef struct {
    char* modelName;
    char* modelDescription;
    char* modelNumber;
    char* modelURL;
} _vn_upnp_device_model_desc_t;

typedef struct {
    char serialNumber[24];
    char UDN[_VN_UPNP_UDN_SIZE];
    char UPC[24];
} _vn_upnp_chip_id_t;

/* Generic UPNP Service */
typedef struct __vn_upnp_service_t _vn_upnp_service_t;

struct __vn_upnp_service_t {
    Upnp_SID* Sid;
    char* serviceType;
    char* serviceId;
    char* controlURL;
    char* eventSubURL;
    char* SCPDURL;
};

/* Generic UPNP Device */

struct __vn_upnp_device_t {
    _vn_upnp_device_t *next;
    _vn_upnp_device_desc_t desc;
    _vn_upnp_device_model_desc_t model;
    _vn_upnp_chip_id_t id;

    _vn_upnp_device_t *devices;

    int nservices;
    _vn_upnp_service_t services[];
};

/* UPNP Device information for a VN device */
typedef struct {
    char UDN[_VN_UPNP_UDN_SIZE];

    _VN_guid_t     guid;        /* Unique device id */

    int flags;
    int nservices;
    _vn_upnp_service_t services[_VN_UPNP_VN_SERVICES];
    _vn_timer_t*   timer;       /* Timer for how long the entry is valid */

    _vn_dlist_t*   desc_urls;   /* List of desc URLs */

    int nguids;
    _VN_guid_t*    guids;       /* List of real VN devices attached */
    SOCKADDRIN     addr;        /* UPNP IP/Port at which host was discovered */
} _vn_upnp_vndevice_info_t;

/* UPNP NAT device */
typedef struct {
    char UDN[NAME_SIZE];
    int expires;
    int nservices;
    _vn_inaddr_t nat_ip;    /* UPNP IP at which host was discovered */
    _vn_inaddr_t local_ip;  /* Local IP to use for nat port mappings */
    char local_addr[_VN_INET_ADDRSTRLEN];
    _vn_upnp_service_t services[_VN_UPNP_NAT_SERVICES];
} _vn_nat_device_t;

/* Query host list member */
typedef struct {    
    const char* desc_doc_url;   /* URL to query */
    int flags;        /* Flags of clients registered to query host */
    _VN_guid_t   guid;
} _vn_upnp_query_host_t;

typedef int (*upnp_action) (_VN_guid_t guid, IXML_Document *request, 
                            IXML_Document **out, char **errorString);

/* Structure for storing VN Service
   identifiers and state table */
typedef struct {
  
    char ServiceId[NAME_SIZE];
    char ServiceType[NAME_SIZE];
    int  VarCount;
    char *VarNames[_VN_UPNP_VN_MAXVARS]; 
    char *ActionNames[_VN_UPNP_VN_MAXACTIONS];
    upnp_action actions[_VN_UPNP_VN_MAXACTIONS];
} _vn_upnp_vn_service_t;

/* Function Prototypes */
char* _vn_strdup(const char *s);
char* _vn_upnp_ixml_GetFirstElementItem( IN IXML_Element * element,
                                         IN const char *item );
char* _vn_upnp_ixml_GetFirstDocumentItem( IN IXML_Document * doc,
                                          IN const char *item );
int _vn_upnp_ixml_FindAndParseService( IN IXML_Document * DescDoc,
                                       IN char *location,
                                       IN char *deviceId,
                                       IN char *serviceType,
                                       OUT char **serviceId,
                                       OUT char **eventURL,
                                       OUT char **controlURL );

char* mem2hex(char *mem, char *buf, size_t count);
char* hex2mem(char *buf, char *mem, size_t count);

void _vn_upnp_vndevice_info_init(_vn_upnp_vndevice_info_t* device,
                                 struct Upnp_Download_Xml *d_event);
_vn_upnp_vndevice_info_t* 
_vn_upnp_vndevice_info_create(const char* UDN,
                              _VN_guid_t guid,
                              struct Upnp_Download_Xml *d_event);
void _vn_upnp_vndevice_info_free(void* info);

_vn_nat_device_t* _vn_upnp_find_nat_device(char *UDN);
void _vn_upnp_destroy_nat_device(_vn_nat_device_t* device);
_vn_nat_device_t* _vn_upnp_remove_nat_device(char *UDN);


int _vn_upnp_ctrlpt_init();
int _vn_upnp_ctrlpt_shutdown();
int _vn_upnp_ctrlpt_detect_nat();
int _vn_upnp_ctrlpt_callback_event_handler(Upnp_EventType EventType,
                                           void* event, void* cookie);
int _vn_upnp_ctrlpt_subscribe_all();
int _vn_upnp_ctrlpt_unsubscribe_all(int match_mask, int filter_mask);

int _vn_upnp_device_init();
int _vn_upnp_device_shutdown();
int _vn_upnp_device_register_root();
int _vn_upnp_device_unregister_root();
int _vn_upnp_device_advertise();
int _vn_upnp_device_state_table_init();

int _vn_upnp_add_device(struct Upnp_Download_Xml *d_event, void* cookie);

int _vn_upnp_set_nat_device(struct Upnp_Download_Xml *d_event);

int _vn_upnp_format_udn(char* udn);
int _vn_upnp_peer_parse_udn(char* udn, _VN_guid_t *guid, _vn_inport_t *port);
int _vn_upnp_parse_guid(char* guid_str, _VN_guid_t *guid);
int _vn_upnp_peer_discovered(struct Upnp_Download_Xml *d_event,
                             void* cookie);
int _vn_upnp_update_vndevice(_VN_guid_t guid,
                             _vn_inaddr_t ipaddr, _vn_inport_t port,
                             int expires, uint8_t flags, bool flags_changed,
                             _vn_upnp_vndevice_info_t* upnp_info);
int _vn_upnp_query_services(char* udn,
                            _vn_upnp_vndevice_info_t *upnp_info);
int _vn_upnp_subscribe_services(char* udn,
                                _vn_upnp_vndevice_info_t *upnp_info);
int _vn_upnp_unsubscribe_services(_vn_upnp_vndevice_info_t *upnp_info);

void _vn_upnp_query_host_timer_cb(_vn_timer_t* timer);
void _vn_upnp_query_host_list_init();
void _vn_upnp_query_host_list_destroy();

#ifdef  __cplusplus
}
#endif
 
#endif /* __VN_UPNP_PRIV_H__ */

#endif /* UPNP */

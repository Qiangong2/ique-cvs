//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnlocal.h"

/* Security related functions for the VN handshake */

#define _VN_DH_GEN_CERT                     1
#define _VN_EXPAND_KEY                      0

int _vn_hs_dh_init_keys();
int _vn_hs_dh_clear_keys();
extern int _vn_iosc_init_certs();
extern int _vn_clear_certs();

extern void _iosInitCrypto();

int _vn_iosc_init()
{
    
    int rv = _VN_ERR_OK;
#if _VN_USE_IOSC
#ifndef _SC
    _iosInitCrypto();
#endif

    /* Init my guid */
    _vn_init_myguid();

    rv = _vn_iosc_init_certs();
    if (rv < 0) {
        return rv;
    }
    rv = _vn_hs_dh_init_keys();

    /* Pre generate a DH key */
    _vn_hs_dh_generate_key(NULL, NULL);
#else

    /* Init my guid */
    _vn_init_myguid();

#endif

    return rv;
}

/* TODO: Cleanup iosc structures */
int _vn_iosc_cleanup()
{
#if _VN_USE_IOSC
    _vn_hs_dh_clear_keys();
#endif
    return _VN_ERR_OK;
}

#if _VN_USE_IOSC
extern _vn_cert_t _vn_devcert;

_vn_cert_t* _vn_hs_get_cert_c()
{
    return &_vn_devcert;
}

_vn_cert_t* _vn_hs_get_cert_s()
{
    return &_vn_devcert;
}
#else
_vn_cert_t* _vn_hs_get_cert_c()
{
    return NULL;
}

_vn_cert_t* _vn_hs_get_cert_s()
{
    return NULL;
}

#endif

#if _VN_USE_IOSC
int  _vn_hs_gen_hash(_vn_hash_t hash,
                     _vn_hs_info_t* hs_info, const uint8_t* buf, size_t len)
{
    _vn_buf_t *init, *init_ack;
#if _VN_IOSC_ALIGN_NEEDED
    uint8_t *aligned_buf;
    int total_len, remaining;
#endif
    IOSCHashContext hash_ctx;
    IOSCError ios_rv;

    /* Generate hash using INIT | INIT_ACK | buf */
    assert(hs_info);

    init = hs_info->pkts[_VN_HS_MSG_INIT - 1];
    init_ack = hs_info->pkts[_VN_HS_MSG_INIT_ACK - 1];
    
    assert(init && init->buf);
    assert(init_ack && init_ack->buf);

    /* Use IOSC_GenerateHash */
#if _VN_IOSC_ALIGN_NEEDED
    total_len = init->len + init_ack->len + len;
    remaining = total_len % _VN_MAC_BLOCK_SIZE;
    aligned_buf = _vn_malloc_aligned(total_len, _VN_MAC_ALIGNMENT);
    if (aligned_buf) {
        memcpy(aligned_buf, init->buf, init->len);
        memcpy(aligned_buf + init->len, init_ack->buf, init_ack->len);
        memcpy(aligned_buf + init->len + init_ack->len, buf, len);
        ios_rv = IOSC_GenerateHash(hash_ctx, aligned_buf,
                                   total_len - remaining,
                                   IOSC_HASH_FIRST, 0);
        ios_rv = IOSC_GenerateHash(hash_ctx, 
                                   aligned_buf + total_len - remaining,
                                   remaining,
                                   IOSC_HASH_LAST, hash);
        _vn_free_aligned(aligned_buf);
    } else {
        return _VN_ERR_NOMEM;
    }
#else
    ios_rv = IOSC_GenerateHash(hash_ctx, init->buf, init->len, 
                               IOSC_HASH_FIRST, 0); 
    ios_rv = IOSC_GenerateHash(hash_ctx, init_ack->buf, init_ack->len,
                               IOSC_HASH_MIDDLE, 0);
    ios_rv = IOSC_GenerateHash(hash_ctx, (uint8_t*) buf, 
                               (int) len, IOSC_HASH_LAST, hash);
#endif
    return _VN_ERR_OK;
}
#endif /* _VN_USE_IOSC */

int  _vn_hs_gen_client_sig(_vn_hs_sign_c_t sig, _vn_hs_sigkey_c_t key,
                           _vn_hs_info_t* hs_info,
                           const uint8_t* buf, size_t len)
{
#if _VN_USE_IOSC
    IOSCError ios_rv;
    _vn_hash_t hash;
    _VN_TRACE(TRACE_FINEST, _VN_SG_IOSC, "Client signature\n");
    if (_vn_hs_gen_hash(hash, hs_info, buf, len) < 0) {
        return _VN_ERR_SEC;
    }

    if (_VN_TRACE_ON(TRACE_FINER, _VN_SG_IOSC)) {
        _VN_TRACE(TRACE_FINER, _VN_SG_IOSC, "Generating client sig for hash\n");
        _vn_dbg_dump_buf(stdout, hash, sizeof(hash));
    }

    ios_rv = IOSC_GeneratePublicKeySign(hash, sizeof(hash), key, sig);
    if (ios_rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC,
                  "IOS error %d generating client signature\n", ios_rv);
        return _VN_ERR_SEC;
    }
#else
    memset(sig, 0, sizeof(_vn_hs_sign_c_t));
#endif /* _VN_USE_IOSC */

    return _VN_ERR_OK;
}

bool _vn_hs_verify_client_sig(_vn_hs_sign_c_t sig, _vn_hs_verkey_c_t key,
                              _vn_hs_info_t* hs_info,
                              const uint8_t* buf, size_t len)
{
#if _VN_USE_IOSC
    _vn_hash_t hash;
    IOSCError ios_rv;

    if (_vn_hs_gen_hash(hash, hs_info, buf, len) < 0) {
        return false;
    }

    if (_VN_TRACE_ON(TRACE_FINER, _VN_SG_IOSC)) {
        _VN_TRACE(TRACE_FINER, _VN_SG_IOSC, "Verifying client sig for hash\n");
        _vn_dbg_dump_buf(stdout, hash, sizeof(hash));
    }

    ios_rv = IOSC_VerifyPublicKeySign(hash, sizeof(hash), key, sig);
    if (ios_rv < 0) {
        _VN_TRACE(TRACE_WARN, _VN_SG_IOSC,
                  "IOS error %d verifying client signature\n", ios_rv);
        return false;
    } else {
        return true;
    }
#else
    return true;
#endif
}

int _vn_hs_gen_server_sig(_vn_hs_sign_s_t sig, _vn_hs_sigkey_s_t key,
                          _vn_hs_info_t* hs_info, const uint8_t* buf, size_t len)
{
#if _VN_USE_IOSC
    IOSCError ios_rv;
    _vn_hash_t hash;
    _VN_TRACE(TRACE_FINEST, _VN_SG_IOSC, "Server signature\n");
    if (_vn_hs_gen_hash(hash, hs_info, buf, len) < 0) {
        return _VN_ERR_SEC;
    }

    if (_VN_TRACE_ON(TRACE_FINER, _VN_SG_IOSC)) {
        _VN_TRACE(TRACE_FINER, _VN_SG_IOSC, "Generating server sig for hash\n");
        _vn_dbg_dump_buf(stdout, hash, sizeof(hash));
    }

    ios_rv = IOSC_GeneratePublicKeySign(hash, sizeof(hash), key, sig);

    if (ios_rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC,
                  "IOS error %d generating server signature\n", ios_rv);
        return _VN_ERR_SEC;
    }
#else
    memset(sig, 0, sizeof(_vn_hs_sign_s_t));
#endif /* _VN_USE_IOSC */
    return _VN_ERR_OK;
}

bool _vn_hs_verify_server_sig(_vn_hs_sign_s_t sig, _vn_hs_verkey_s_t key,
                              _vn_hs_info_t* hs_info,
                              const uint8_t* buf, size_t len)
{
#if _VN_USE_IOSC
    _vn_hash_t hash;
    IOSCError ios_rv;
    if (_vn_hs_gen_hash(hash, hs_info, buf, len) < 0) {
        return false;
    }

    if (_VN_TRACE_ON(TRACE_FINER, _VN_SG_IOSC)) {
        _VN_TRACE(TRACE_FINER, _VN_SG_IOSC, "Verifying server sig for hash\n");
        _vn_dbg_dump_buf(stdout, hash, sizeof(hash));
    }

    ios_rv = IOSC_VerifyPublicKeySign(hash, sizeof(hash), key, sig);
    if (ios_rv < 0) {
        _VN_TRACE(TRACE_WARN, _VN_SG_IOSC,
                  "IOS error %d verifying server signature\n", ios_rv);
        return false;
    } else {
        return true;
    }
#else
    return true;
#endif
}

#if _VN_USE_IOSC
bool _vn_hs_verify_cert(uint8_t* certData, size_t certLen,
                        IOSCPublicKeyHandle signerKey,
                        IOSCPublicKeyHandle* pCertKey)
{
    return (_vn_import_cert(certData, certLen, signerKey, pCertKey) == _VN_ERR_OK);
}
#endif

bool _vn_hs_verify_server_cert(_VN_guid_t ID_s,
                               _vn_hs_verkey_s_t *cert_key_s,
                               uint8_t *cert, size_t certSize)
{
#if _VN_USE_IOSC
    return (_vn_verify_device_cert(ID_s, cert, certSize,
                                   cert_key_s) == _VN_ERR_OK);
#else     
    return true;
#endif
}

bool _vn_hs_verify_client_cert(_VN_guid_t ID_c,
                               _vn_hs_verkey_c_t *cert_key_c,
                               uint8_t *cert, size_t certSize)
{
#if _VN_USE_IOSC
    return (_vn_verify_device_cert(ID_c, cert, certSize,
                                   cert_key_c) == _VN_ERR_OK);
#else     
    return true;
#endif
}

#if _VN_USE_IOSC

#ifdef _SC
#define _VN_HS_DH_KEY_MAX_USE             32
#define _VN_HS_DH_KEY_MAX                 4
#else
#define _VN_HS_DH_KEY_MAX_USE             64
#define _VN_HS_DH_KEY_MAX                 128
#endif

typedef struct {
    _vn_hs_dh_key_handle_t handle;
    IOSCEccSignedCert *cert;
    uint16_t used;    /* Number of times this key has been used */
    int16_t refcnt;  /* Currently in use */
} _vn_hs_dh_key_t;

_vn_hs_dh_key_t* _vn_hs_dh_keys[_VN_HS_DH_KEY_MAX];

int _vn_hs_dh_init_keys()
{
    int i; 
    for (i = 0; i < _VN_HS_DH_KEY_MAX; i++) {
        _vn_hs_dh_keys[i] = NULL;
    }
    return _VN_ERR_OK;
}

/* Finds a specific key */
_vn_hs_dh_key_t* _vn_hs_dh_find_key(_vn_hs_dh_key_handle_t handle)
{
    int i;
    for (i = 0; i < _VN_HS_DH_KEY_MAX; i++) {
        if (_vn_hs_dh_keys[i] && 
            (_vn_hs_dh_keys[i]->handle == handle)) {
            return _vn_hs_dh_keys[i];
        }
    }
    return NULL;
}

void _vn_hs_dh_destroy_key(_vn_hs_dh_key_t* dh_key)
{
    _VN_TRACE(TRACE_FINE, _VN_SG_IOSC, "Destroying DH key\n");
    if (dh_key) {
        if (_VN_KEY_HANDLE_IS_VALID(dh_key->handle)) {
            IOSC_DeleteObject(dh_key->handle);
        }
        if (dh_key->cert) { 
            _vn_free_aligned(dh_key->cert); 
        }
        _vn_free(dh_key);
    }
}

int _vn_hs_dh_clear_keys()
{
    int i; 
    for (i = 0; i < _VN_HS_DH_KEY_MAX; i++) {
        if (_vn_hs_dh_keys[i]) {
            _vn_hs_dh_destroy_key(_vn_hs_dh_keys[i]);
            _vn_hs_dh_keys[i] = NULL;
        }
    }
    return _VN_ERR_OK;
}

_vn_hs_dh_key_t* _vn_hs_dh_new_key()
{
    _vn_hs_dh_key_t *dh_key;
    _VN_TRACE(TRACE_FINE, _VN_SG_IOSC, "Generating new DH key\n");
    dh_key = _vn_malloc(sizeof(_vn_hs_dh_key_t));
    if (dh_key) {
        IOSCError ios_rv;

#if _VN_DH_GEN_CERT
        IOSCCertName certname;
        memset(certname, 0, sizeof(certname));
        snprintf(certname, sizeof(certname), "VN%08x", _vn_rand());

        dh_key->cert = _vn_malloc_aligned(sizeof(IOSCEccSignedCert), 
                                          _VN_CERT_ALIGNMENT);
        if (dh_key->cert == NULL) {
            _vn_free(dh_key);
            return NULL;
        }
        memset(dh_key->cert, 0, sizeof(IOSCEccSignedCert));
#else
        dh_key->cert = NULL;
#endif

        dh_key->used = 0;
        dh_key->refcnt = 0;
        dh_key->handle = _VN_KEY_HANDLE_INVALID;

        ios_rv = IOSC_CreateObject(&dh_key->handle, 
                                   IOSC_KEYPAIR_TYPE, IOSC_ECC233_SUBTYPE);
        if (ios_rv < 0) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC,
                      "IOS error %d creating ECC key handle\n", ios_rv);
            _vn_hs_dh_destroy_key(dh_key);
            return NULL;
        }

        ios_rv = IOSC_GenerateKey(dh_key->handle);
        if (ios_rv < 0) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC,
                      "IOS error %d generating ECC key\n", ios_rv);
            _vn_hs_dh_destroy_key(dh_key);
            return NULL;
        }

        _VN_TRACE(TRACE_FINE, _VN_SG_IOSC, "Generating certificate\n");
#if _VN_DH_GEN_CERT
        ios_rv = IOSC_GenerateCertificate(dh_key->handle,
                                          certname,
                                          dh_key->cert);
        if (ios_rv < 0) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC,
                      "IOS error %d generating certificate\n", ios_rv);
            _vn_hs_dh_destroy_key(dh_key);
            return NULL;
        } 

        if (_VN_TRACE_ON(TRACE_FINER, _VN_SG_IOSC)) {
            _VN_TRACE(TRACE_FINER, _VN_SG_IOSC, "generated cert\n");
            _vn_dbg_dump_buf(stdout, *dh_key->cert, sizeof(IOSCEccSignedCert));
        }

#endif
    }
    return dh_key;
}

/* Generate a DH key pair (reuse key pairs for several sessions) */
int _vn_hs_dh_generate_key(_vn_hs_dh_key_handle_t *handle, 
                           uint8_t**pCert)
{
    _vn_hs_dh_key_t* key = NULL;
    int i, avail = -1;

    for (i = 0; i < _VN_HS_DH_KEY_MAX; i++) {
        if (_vn_hs_dh_keys[i]) {
            if (_vn_hs_dh_keys[i]->used < _VN_HS_DH_KEY_MAX_USE) {
                key = _vn_hs_dh_keys[i];
                break;
            }
        } else {
            if (avail < 0) {
                avail = i;
            }
        }
    }

    if (!key) {
        if (avail >= 0) {
            _vn_hs_dh_keys[avail] = _vn_hs_dh_new_key();
            if (_vn_hs_dh_keys[avail]) {
                key = _vn_hs_dh_keys[avail];
            } else {
                return _VN_ERR_FAIL;
            }
        } else {
            return _VN_ERR_NOSPACE;
        }
    }

    if (handle || pCert) {
        key->used++;
        key->refcnt++;
    }
    _VN_TRACE(TRACE_FINE, _VN_SG_IOSC, "DH key used %d, refcnt %d\n", 
              key->used, key->refcnt);
    if (handle) { *handle = key->handle; }
    if (pCert) { *pCert = *key->cert; }
    return _VN_ERR_OK;
}

/* Indicate a DH key pair is no longer in use 
   (update counter for keys that are used multiple times) */
int _vn_hs_dh_delete_key(_vn_hs_dh_key_handle_t handle)
{
    _vn_hs_dh_key_t* dh_key = NULL;
    int i;
    for (i = 0; i < _VN_HS_DH_KEY_MAX; i++) {
        if (_vn_hs_dh_keys[i] && 
            (_vn_hs_dh_keys[i]->handle == handle)) {
            dh_key = _vn_hs_dh_keys[i];
            break;
        }
    }

    if (dh_key) {
        assert(dh_key->refcnt > 0);
        dh_key->refcnt--;
        if (dh_key->used < _VN_HS_DH_KEY_MAX_USE || dh_key->refcnt > 0) {
            return _VN_ERR_OK;
        } else {
            _vn_hs_dh_destroy_key(dh_key);
            _vn_hs_dh_keys[i] = NULL;
        }
    } else {
        IOSCError ios_rv;
        ios_rv = IOSC_DeleteObject(handle);
    }
    return _VN_ERR_OK;
}

#else /* ! _VN_USE_IOSC */

int _vn_hs_dh_generate_key(_vn_hs_dh_key_handle_t *handle, 
                           uint8_t** pCert)
{
    if (pCert) { *pCert = NULL; }
    return _VN_ERR_OK;
}

/* TODO: Implement: Indicate a DH key pair is no longer in use 
                    (update counter for keys that are used multiple times) */
int _vn_hs_dh_delete_key(_vn_hs_dh_key_handle_t handle)
{
    return _VN_ERR_OK;
}

#endif /* _VN_USE_IOSC */

int _vn_hs_compute_shared_key(_vn_hs_dh_key_handle_t *phSharedKey,
                              _vn_hs_dh_key_handle_t hYprivate,
                              _vn_hs_dh_key_handle_t hYpublic)
{
#if _VN_USE_IOSC
    /* TODO: return error */
    /* Calculate common key from Ys and and our private key 
       (the one that matches the Yc that we choose) 
       (IOSC_ComputeSharedKey) */
    IOSCError ios_rv;
    IOSCSecretKeyHandle hSharedKey;

    assert(phSharedKey);
    assert(_VN_KEY_HANDLE_IS_INVALID(*phSharedKey));

    /* Compute shared key */
    ios_rv = IOSC_CreateObject(&hSharedKey, IOSC_SECRETKEY_TYPE, 
                               IOSC_ENC_SUBTYPE);
    if (ios_rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC,
                  "IOS error %d creating shared key handle\n", ios_rv);
        return _VN_ERR_SEC;
    }

    ios_rv = IOSC_ComputeSharedKey(hYprivate, hYpublic, hSharedKey);
    if (ios_rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC,
                  "IOS error %d computing shared key\n", ios_rv);
        IOSC_DeleteObject(hSharedKey);
        return _VN_ERR_SEC;
    }

    *phSharedKey = hSharedKey;

    return _VN_ERR_OK;
#else
    return _VN_ERR_OK;
#endif
}


int _vn_hs_compute_vnkey(_vn_key_t *key, 
                         _vn_hs_dh_key_handle_t hSharedKey,
                         uint32_t rand_c, uint32_t rand_s)
{
#if _VN_USE_IOSC
    /* TODO: Return error */
    /* TODO: Master keys for the VN should be derived from the 
             Diffie Helman shared secret and rand_c and rand_s 
             (IOSC_ExpandKey) */
    IOSCError ios_rv;
    uint8_t keybuf[_VN_ENC_KEY_LENGTH];
    uint8_t seed[8];

    assert(key);

    memcpy(seed, &rand_c, sizeof(uint32_t));
    memcpy(seed + sizeof(uint32_t), &rand_s, sizeof(uint32_t));
    /* TODO: expand key */
/*    ios_rv = IOSC_ExpandKey(hSharedKey, keybuf, sizeof(keybuf), seed); */
    
    /* For now, use random numbers as first 8 bytes of key */
    memset(keybuf, 0, sizeof(keybuf));
    memcpy(keybuf, &rand_c, sizeof(uint32_t));
    memcpy(keybuf + sizeof(uint32_t), &rand_s, sizeof(uint32_t));
    memcpy(keybuf + _VN_ENC_AES_KEY_SIZE, &rand_c, sizeof(uint32_t));
    memcpy(keybuf + _VN_ENC_AES_KEY_SIZE + sizeof(uint32_t),
           &rand_s, sizeof(uint32_t));
        
    assert(_VN_KEY_HANDLE_IS_INVALID(key->Ak));
    ios_rv = IOSC_CreateObject(&key->Ak, 
                               IOSC_SECRETKEY_TYPE, IOSC_ENC_SUBTYPE);
    if (ios_rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC,
                  "IOS error %d creating Ak key handle\n", ios_rv);
        return _VN_ERR_SEC;
    }
    
    ios_rv = IOSC_ImportSecretKey(key->Ak, 0, hSharedKey, IOSC_NOSIGN_NOENC,
                                  0, 0, keybuf);
    if (ios_rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC,
                  "IOS error %d importing Ak\n", ios_rv);
        return _VN_ERR_SEC;
    }
    
    assert(_VN_KEY_HANDLE_IS_INVALID(key->Hk));
    ios_rv = IOSC_CreateObject(&key->Hk, 
                               IOSC_SECRETKEY_TYPE, IOSC_MAC_SUBTYPE);
    if (ios_rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC,
                  "IOS error %d creating Hk key handle\n", ios_rv);
        return _VN_ERR_SEC;
    }
    
    ios_rv = IOSC_ImportSecretKey(key->Hk, 0, hSharedKey, IOSC_NOSIGN_NOENC,
                                  0, 0, keybuf + _VN_ENC_AES_KEY_SIZE);
    if (ios_rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC,
                  "IOS error %d importing Hk\n", ios_rv);
        return _VN_ERR_SEC;
    }
#else
    assert(key);
    memset(*key, 0, sizeof(_vn_key_t));
    /* For now, use random numbers as first 8 bytes of key */
    memcpy(*key, &rand_c, sizeof(uint32_t));
    memcpy(*key + sizeof(uint32_t), &rand_s, sizeof(uint32_t));
#endif
    return _VN_ERR_OK;
}

int _vn_hs_import_vnkey(const uint8_t *in_keybuf, uint32_t keylen, 
                        _vn_key_t *vn_key, 
                        _vn_hs_dh_key_handle_t hSharedKey,
                        uint32_t rand_c, uint32_t rand_s, _vn_iv_t in_iv)
{
#if _VN_USE_IOSC
    int rv;
    IOSCError ios_rv;
    uint8_t *keybuf = (uint8_t*) in_keybuf;
    uint8_t *iv = NULL;

#if _VN_EXPAND_KEY
    _vn_key_t svn_key;

    /* Initialize key handles */
    svn_key.Ak = _VN_KEY_HANDLE_INVALID;
    svn_key.Hk = _VN_KEY_HANDLE_INVALID;
#endif

    assert(in_iv);
    assert(vn_key);
    assert(keybuf);
    assert(keylen >= _VN_ENC_KEY_LENGTH);

    /* Copy iv, make sure iv is aligned */
    iv = _vn_malloc_aligned(sizeof(_vn_iv_t), _VN_AES_ALIGNMENT);
    if (!iv) {
        rv = _VN_ERR_NOMEM;
        goto out;
    }

#if _VN_IOSC_ALIGN_NEEDED
    /* Make sure keybuf is aligned */
    if ((uint32_t) keybuf & (_VN_AES_ALIGNMENT-1)) {
        keybuf = _vn_malloc_aligned(keylen, _VN_AES_ALIGNMENT);
        if (keybuf) {
            memcpy(keybuf, in_keybuf, keylen);
        } else {
            rv = _VN_ERR_NOMEM;
            goto out;
        }
    }
#endif

#if _VN_EXPAND_KEY
    /* Compute shared vn key first */
    rv = _vn_hs_compute_vnkey(&svn_key, hSharedKey, rand_c, rand_s);
    if (rv < 0) {
        goto out;
    }
#endif
    
    if (_VN_TRACE_ON(TRACE_FINER, _VN_SG_IOSC)) {
        _VN_TRACE(TRACE_FINER, _VN_SG_IOSC, "import vnkey:\n");
        _vn_dbg_dump_buf(stdout, keybuf, keylen);
        _VN_TRACE(TRACE_FINER, _VN_SG_IOSC, "import vnkey iv:\n");
        _vn_dbg_dump_buf(stdout, in_iv, sizeof(_vn_iv_t));
    }

    /* Decrypt key using shared master secret */

    rv = _VN_ERR_SEC;
    assert(_VN_KEY_HANDLE_IS_INVALID(vn_key->Ak));
    ios_rv = IOSC_CreateObject(&vn_key->Ak, 
                               IOSC_SECRETKEY_TYPE, IOSC_ENC_SUBTYPE);
    if (ios_rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC,
                  "IOS error %d creating Ak key handle\n", ios_rv);
        goto out;
    }

    /* Reset iv */
    memcpy(iv, in_iv, sizeof(_vn_iv_t));
    ios_rv = IOSC_ImportSecretKey(vn_key->Ak, 0, hSharedKey/*svn_key.Ak*/, IOSC_NOSIGN_ENC,
                                  0, iv, (uint8_t*) keybuf);
    if (ios_rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC,
                  "IOS error %d importing Ak\n", ios_rv);
        goto out;
    }

    assert(_VN_KEY_HANDLE_IS_INVALID(vn_key->Hk));
    ios_rv = IOSC_CreateObject(&vn_key->Hk, 
                               IOSC_SECRETKEY_TYPE, IOSC_MAC_SUBTYPE);
    if (ios_rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC,
                  "IOS error %d creating Hk key handle\n", ios_rv);
        goto out;
    }

    /* Reset iv */
    memcpy(iv, in_iv, sizeof(_vn_iv_t));
    ios_rv = IOSC_ImportSecretKey(vn_key->Hk, 0, hSharedKey/*svn_key.Ak*/, IOSC_NOSIGN_ENC,
                                  0, iv, (uint8_t*) keybuf + _VN_ENC_AES_KEY_SIZE);
    if (ios_rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC,
                  "IOS error %d importing Hk\n", ios_rv);
        goto out;
    }

    rv = _VN_ERR_OK;

out:
#if _VN_IOSC_ALIGN_NEEDED
    /* Free aligned buf */
    if (keybuf && keybuf != in_keybuf) {
        _vn_free_aligned(keybuf);
    }     
#endif
    /* Free iv */
    if (iv) {
        _vn_free_aligned(iv);
    }

#if _VN_EXPAND_KEY
    _vn_delete_vnkey(&svn_key);    
#endif

    return rv;

#else
    assert(vn_key);
    assert(in_keybuf);
    assert(keylen >= sizeof(_vn_key_t));
    memcpy(vn_key, in_keybuf, sizeof(_vn_key_t));
    return _VN_ERR_OK;
#endif
}

int _vn_hs_export_vnkey(uint8_t *out_keybuf, uint32_t keylen, 
                        _vn_key_t *vn_key, 
                        _vn_hs_dh_key_handle_t hSharedKey,
                        uint32_t rand_c, uint32_t rand_s, _vn_iv_t in_iv)
{
#if _VN_USE_IOSC
    int rv;
    IOSCError ios_rv;
    uint8_t *keybuf = out_keybuf;
    uint8_t *iv = NULL;

#if _VN_EXPAND_KEY
    _vn_key_t svn_key;

    /* Initialize key handles */
    svn_key.Ak = _VN_KEY_HANDLE_INVALID;
    svn_key.Hk = _VN_KEY_HANDLE_INVALID;
#endif

    assert(in_iv);
    assert(vn_key);
    assert(keybuf);
    assert(keylen >= _VN_ENC_KEY_LENGTH);

    /* Copy iv, make sure iv is aligned */
    iv = _vn_malloc_aligned(sizeof(_vn_iv_t), _VN_AES_ALIGNMENT);
    if (!iv) {
        rv = _VN_ERR_NOMEM;
        goto out;
    }

#if _VN_IOSC_ALIGN_NEEDED
    /* Make sure keybuf is aligned */
    if ((uint32_t) keybuf & (_VN_AES_ALIGNMENT-1)) {
        keybuf = _vn_malloc_aligned(keylen, _VN_AES_ALIGNMENT);
        if (keybuf) {
            memset(keybuf, 0, keylen);
        } else {
            rv = _VN_ERR_NOMEM;
            goto out;
        }
    }
#endif

#if _VN_EXPAND_KEY
    /* Compute shared vn key first */
    rv = _vn_hs_compute_vnkey(&svn_key, hSharedKey, rand_c, rand_s);
    if (rv < 0) {
        goto out;
    }
#endif
    
    /* Encrypt VN key with key derived from master secret */
    rv = _VN_ERR_SEC;
    /* Get VN key */
    /* Reset iv */
    memcpy(iv, in_iv, sizeof(_vn_iv_t));
    ios_rv = IOSC_ExportSecretKey(vn_key->Ak, 0, hSharedKey/*svn_key.Ak*/, IOSC_NOSIGN_ENC,
                                  0, iv, keybuf);
    if (ios_rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC,
                  "IOS error %d exporting Ak\n", ios_rv);
        goto out;
    }

    /* Reset iv */
    memcpy(iv, in_iv, sizeof(_vn_iv_t));
    ios_rv = IOSC_ExportSecretKey(vn_key->Hk, 0, hSharedKey/*svn_key.Ak*/, IOSC_NOSIGN_ENC,
                                  0, iv, keybuf + _VN_ENC_AES_KEY_SIZE);

    if (ios_rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC,
                  "IOS error %d exporting Hk\n", ios_rv);
        goto out;
    }

    if (_VN_TRACE_ON(TRACE_FINER, _VN_SG_IOSC)) {
        _VN_TRACE(TRACE_FINER, _VN_SG_IOSC, "export vnkey:\n");
        _vn_dbg_dump_buf(stdout, keybuf, keylen); 
        _VN_TRACE(TRACE_FINER, _VN_SG_IOSC, "export vnkey iv:\n");
        _vn_dbg_dump_buf(stdout, in_iv, sizeof(_vn_iv_t));
    }
    
    rv = _VN_ERR_OK;

out:    
#if _VN_IOSC_ALIGN_NEEDED
    /* Free aligned buf */
    if (keybuf && keybuf != out_keybuf) {
        memcpy(out_keybuf, keybuf, keylen);
        _vn_free_aligned(keybuf);
    }        
#endif
    /* Free iv */
    if (iv) {
        _vn_free_aligned(iv);
    }        
#if _VN_EXPAND_KEY
    _vn_delete_vnkey(&svn_key);
#endif
    return rv;
#else
    assert(vn_key);
    assert(out_keybuf);
    assert(keylen >= sizeof(_vn_key_t));
    memset(out_keybuf, 0, keylen);
    memcpy(out_keybuf, *vn_key, sizeof(_vn_key_t));
    return _VN_ERR_OK;
#endif
}

int _vn_hs_delete_keys(_vn_hs_info_t* hs_info)
{
    assert(hs_info);
#if _VN_USE_IOSC
    /* Free key contexts (IOSC_DeleteObject) */
    if (_VN_KEY_HANDLE_IS_VALID(hs_info->Yc)) {
        _vn_hs_dh_delete_key(hs_info->Yc);
        hs_info->Yc = _VN_KEY_HANDLE_INVALID;
    }
    if (_VN_KEY_HANDLE_IS_VALID(hs_info->Ys)) {
        _vn_hs_dh_delete_key(hs_info->Ys);
        hs_info->Ys = _VN_KEY_HANDLE_INVALID;
    }
    if (_VN_KEY_HANDLE_IS_VALID(hs_info->DH_shared_handle)) {
        IOSC_DeleteObject(hs_info->DH_shared_handle);
        hs_info->DH_shared_handle = _VN_KEY_HANDLE_INVALID;
    }
    if (_VN_KEY_HANDLE_IS_VALID(hs_info->cert_key_c)) {
        IOSC_DeleteObject(hs_info->cert_key_c);
        hs_info->cert_key_c = _VN_KEY_HANDLE_INVALID;
    }
    if (_VN_KEY_HANDLE_IS_VALID(hs_info->cert_key_s)) {
        IOSC_DeleteObject(hs_info->cert_key_s);;
        hs_info->cert_key_s = _VN_KEY_HANDLE_INVALID;
    }    
#else
#endif
    return _VN_ERR_OK;
}

/* Generates a key for the VN */
int _vn_generate_vnkey(_vn_key_t *key)
{
#if _VN_USE_IOSC
    IOSCError ios_rv;
    assert(key);
    
    ios_rv = IOSC_CreateObject(&key->Ak, IOSC_SECRETKEY_TYPE, IOSC_ENC_SUBTYPE);
    if (ios_rv != IOSC_ERROR_OK) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC, 
                  "IOS error %d while creating Ak key handle\n", ios_rv);
        goto err;
    }
    ios_rv = IOSC_GenerateKey(key->Ak);
    if (ios_rv != IOSC_ERROR_OK) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC, 
                  "IOS error %d while generating Ak\n", ios_rv);
        goto err;
    }
    
    ios_rv = IOSC_CreateObject(&key->Hk, IOSC_SECRETKEY_TYPE, IOSC_MAC_SUBTYPE);
    if (ios_rv != IOSC_ERROR_OK) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC, 
                  "IOS error %d while creating Hk key handle\n", ios_rv);
        goto err;
    }

    ios_rv = IOSC_GenerateKey(key->Hk);
    if (ios_rv != IOSC_ERROR_OK) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC, 
                  "IOS error %d while generating Hk\n", ios_rv);
        goto err;
    }

    return _VN_ERR_OK;
err:
    _vn_delete_vnkey(key);
    return _VN_ERR_SEC;
#else
    int i;
    assert(key);
    /* Get a reasonable key for network */
    for (i = 0; i < sizeof(_vn_key_t); i++) {
        (*key)[i] = _vn_rand();
    }
    return _VN_ERR_OK;
#endif
}

int _vn_delete_vnkey(_vn_key_t *key)
{
#if _VN_USE_IOSC
    assert(key);
    if (_VN_KEY_HANDLE_IS_VALID(key->Ak)) {
        IOSC_DeleteObject(key->Ak);
        key->Ak = _VN_KEY_HANDLE_INVALID;
    }
    if (_VN_KEY_HANDLE_IS_VALID(key->Hk)) {
        IOSC_DeleteObject(key->Hk);
        key->Hk = _VN_KEY_HANDLE_INVALID;
    }
#endif
    return _VN_ERR_OK;
}


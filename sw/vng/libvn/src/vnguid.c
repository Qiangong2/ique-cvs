//
//               Copyright (C) 2006, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnlocal.h"

static _VN_guid_t _vn_myguid = _VN_GUID_INVALID;
static _VN_guid_t _vn_guid_invalid = _VN_GUID_INVALID;

/* Functions for comparing guids */

int _vn_guid_eq(_VN_guid_t *pGuid1, _VN_guid_t *pGuid2)
{
    int rv;
    assert(pGuid1);
    assert(pGuid2);

    rv = ((pGuid1->chip_id == pGuid2->chip_id) &&
          (_VN_DEVICE_IS_PROXY(pGuid1->device_type) ==
           _VN_DEVICE_IS_PROXY(pGuid2->device_type)) &&
          (((_VN_DEVICE_TYPE_MASK & pGuid1->device_type) == 
            (_VN_DEVICE_TYPE_MASK & pGuid2->device_type))));


    _VN_TRACE(TRACE_FINEST, _VN_SG_MISC,
              "comparing guid 0x%08x %u with 0x%08x %u, got %d\n",
              pGuid1->device_type, pGuid2->chip_id,
              pGuid2->device_type, pGuid2->chip_id, rv);

    return rv;
}

int _vn_guid_match(_VN_guid_t *pGuid1, _VN_guid_t *pGuid2)
{
    int rv;
    assert(pGuid1);
    assert(pGuid2);

    rv = ((pGuid1->chip_id == pGuid2->chip_id) &&
          (((_VN_DEVICE_IS_PROXY(pGuid1->device_type) ==
            _VN_DEVICE_IS_PROXY(pGuid2->device_type)) &&
            (_VN_DEVICE_TYPE_MASK & pGuid1->device_type) == 
            (_VN_DEVICE_TYPE_MASK & pGuid2->device_type)) ||
           (pGuid1->device_type == _VN_DEVICE_TYPE_UNKNOWN) ||
           (pGuid2->device_type == _VN_DEVICE_TYPE_UNKNOWN)));

    _VN_TRACE(TRACE_FINEST, _VN_SG_MISC,
              "matching guid 0x%08x %u with 0x%08x %u, got %d\n",
              pGuid1->device_type, pGuid2->chip_id,
              pGuid2->device_type, pGuid2->chip_id, rv);

    return rv;
}

bool _vn_guid_is_invalid(_VN_guid_t *pGuid)
{
    return _vn_guid_eq(pGuid, &_vn_guid_invalid);
}

/* Set guid to be invalid */
void _vn_guid_set_invalid(_VN_guid_t *pGuid)
{
    assert(pGuid);
    pGuid->device_type = _vn_guid_invalid.device_type;
    pGuid->chip_id = _vn_guid_invalid.chip_id;
}

/* Functions for figuring out my guid */

void _vn_init_myguid()
{
    _vn_myguid.device_type = _vn_get_my_device_type();
    _vn_myguid.chip_id = _vn_get_my_chip_id();
    _VN_TRACE(TRACE_INFO, _VN_SG_MISC, "Got guid 0x%08x-%08x (%u)\n",
              _vn_myguid.device_type, _vn_myguid.chip_id, _vn_myguid.chip_id);
}

_VN_guid_t _vn_get_myguid()
{
    return _vn_myguid;
}

bool _vn_guid_is_self(_VN_guid_t *pGuid)
{
    return _vn_guid_eq(pGuid, &_vn_myguid);
}

/* Returns my chip id */
_VN_chip_id_t _vn_get_my_chip_id()
{
#if _VN_USE_IOSC
    if (_vn_myguid.chip_id == _VN_CHIP_ID_INVALID) {
        IOSCError ios_rv;
        ios_rv = IOSC_GetData(IOSC_DEV_ID_HANDLE, &_vn_myguid.chip_id);
        if (ios_rv != IOSC_ERROR_OK) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_MISC, 
                      "IOSC Error %d getting my chip id\n", ios_rv);
        }

        if (_vn_myguid.chip_id == 1) {
            _VN_TRACE(TRACE_WARN, _VN_SG_MISC,
                      "Ignoring chip id of %d\n", _vn_myguid.chip_id);
            _vn_myguid.chip_id = _VN_CHIP_ID_INVALID;
        }
    }
#endif

    /* For now, just generate a random number as my chip id */
    while (_vn_myguid.chip_id == _VN_CHIP_ID_INVALID) {
        _vn_myguid.chip_id = _vn_rand();
    }
    return _vn_myguid.chip_id;
}

/* Sets my chip id */
void _vn_set_my_chip_id(_VN_chip_id_t chip_id) {
    _vn_myguid.chip_id = chip_id;
}

/* Get my device type */
_VN_device_type_t _vn_get_my_device_type()
{
    _VN_device_type_t device_type;

#ifdef _VN_RPC_PROXY
    device_type =  _VN_DEVICE_TYPE | _VN_DEVICE_PROXY | _VN_DEVICE_RPC_MASK;
#else
    if (_vn_isserver()) {
        device_type = _VN_DEVICE_SERVER | _VN_DEVICE_VS;
    } else {
        device_type = _VN_DEVICE_CLIENT | _VN_DEVICE_TYPE;
    }

#ifdef _VN_RPC_DEVICE
    device_type |= _VN_DEVICE_RPC_MASK;
#endif
  
#endif

#ifdef UPNP
    device_type |= _VN_DEVICE_UPNP_MASK;
#endif
    
    return device_type;
}

/* Functions to support using guid as a hash key */
_vn_ht_hash_t
_vn_ht_hash_guid(_vn_ht_key_t key)
{
    _VN_guid_t *pGuid;
    assert(key);
    pGuid = (_VN_guid_t*) key;
    /* Use chip id as hash - should be fairly unique */
    return (_vn_ht_hash_t) pGuid->chip_id;
}

int
_vn_ht_key_comp_guid(_vn_ht_key_t key1, _vn_ht_key_t key2)
{
    return _vn_guid_match( (_VN_guid_t*) key1, (_VN_guid_t*) key2 );
}

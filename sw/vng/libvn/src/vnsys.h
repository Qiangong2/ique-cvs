//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#ifndef __VN_SYS_H__
#define __VN_SYS_H__

#if defined(_WIN32)
#include "WIN32/vnwin32.h"
#elif defined(_SC)
#include "SC/vnsc.h"
#elif defined(_LINUX)
#include "LINUX/vnlinux.h"
#endif

#endif /* __VN_SYS_H__ */

//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#ifndef __VN_LOCAL_H__
#define __VN_LOCAL_H__

#define _VN_RECV_TIMER                      1
#define _VN_INACTIVITY_TIMER                1

#include "vn.h"
#include "vnlist.h"
#include "vnhash.h"
#include "vntimer.h"
#include "vndebug.h"

#include "vnlocaltypes.h"

#ifdef UPNP
#include "vnupnp.h"
#endif

#include "vndevices.h"

#ifndef _VN_RPC_PROXY
#include "vnmsg.h"
#include "vnctrlmsg.h"
#include "vnhs.h"
#include "vnsec.h"
#include "vnnetcore.h"
#include "vnnet.h"
#endif

#ifndef _VN_RPC_DEVICE
#include "vnsocket.h"

#endif

#if defined(_VN_RPC_PROXY) || defined(_VN_RPC_DEVICE)
#include "vnrpc.h"
#endif

#include "vnnetif.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Local functions common to VN Proxy and SC */
_VN_callid_t  _vn_new_call_id();
_VN_callid_t  _vn_get_call_id();

/* Messaging functions */
void _vn_free_msg_buffer(_vn_buf_t* pkt);
_vn_buf_t* _vn_get_msg_buffer(_VN_msg_len_t len);
_vn_buf_t* _vn_new_msg_buffer(const void *msg, _VN_msg_len_t len);
_vn_wbuf_t* _vn_new_vn_buffer(const void *msg, _VN_msg_len_t len);

/* Timestamp */
_VN_time_t _vn_get_timestamp();

/* Netmask */
uint32_t _vn_get_netmask(_VN_addr_t addr);
uint16_t _vn_get_maxhost(_VN_addr_t addr);

/* Dispatcher */


/* Virtual network management */
int _vn_net_lock();
int _vn_net_unlock();
int _vn_net_lock_init();
int _vn_net_lock_destroy();

/* VN Proxy functions */
#ifdef _VN_RPC_PROXY
int _vn_proxy_lookup_client_index(_VN_guid_t guid);
int _vn_proxy_get_client_guids(_VN_guid_t *guids, int nguids);
int _vn_proxy_enqueue_keep_alive(_vn_netif_t* netif, _VN_addr_t vnaddr);
int _vn_proxy_discover_hosts(_vn_usb_handle_t handle, int flag);

int _vn_proxy_device_discovered_sync(int upnp_device_flags,
                                     _vn_device_info_t* device);
int _vn_proxy_device_timeout_changed_sync(int upnp_device_flags,
                                          _vn_device_info_t* device);
int _vn_proxy_device_removed_sync(_VN_guid_t guid, uint8_t reason);
int _vn_proxy_service_added_sync(int upnp_device_flags,
                                 _VN_guid_t guid, uint32_t service_id);
int _vn_proxy_service_removed_sync(int upnp_device_flags,
                                   _VN_guid_t guid, uint32_t service_id);
int _vn_proxy_services_added_sync_raw(int upnp_device_flags,
                                      _VN_guid_t guid,
                                      uint8_t* buf, size_t buflen);
int _vn_proxy_services_removed_sync_raw(int upnp_device_flags,
                                        _VN_guid_t guid,
                                        uint8_t* buf, size_t buflen);
#endif

#ifdef  __cplusplus
}
#endif
 
#endif /* __VN_LOCAL_H__ */
 

//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#ifndef __VN_NETCORE_H__
#define __VN_NETCORE_H__

#include "vnlocaltypes.h"
#include "vntimer.h"
#include "vnlist.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Host/Network information - stored on SC */

/* Properties for the network */
#define _VN_PROP_OWNER          0x01   /* This device is the owner of the
                                          network */
#define _VN_PROP_PUBLIC         0x08   /* This net has been announced to
                                          the application (via event) */
#define _VN_PROP_ACCEPT         0x10   /* This net has been opened for 
                                          incoming connections */
#define _VN_PROP_DEFAULT        0x20   /* This net is a default net and should
                                          be deleted when only the owner is
                                          left */
#define _VN_PROP_DAEMON         0x40   /* This net is a daemon net and not
                                          to be deleted during reset */
#define _VN_PROP_DONE           0x80   /* The application is done with this net
                                          it can be removed once all messages
                                          have been sent from the net */

#define _VN_NET_IS_OWNER(net)           (net->flags & _VN_PROP_OWNER)
#define _VN_NET_IS_PUBLIC(net)          (net->flags & _VN_PROP_PUBLIC)
#define _VN_NET_IS_ACCEPTING(net)       (net->flags & _VN_PROP_ACCEPT)
#define _VN_NET_IS_DEFAULT(net)         (net->flags & _VN_PROP_DEFAULT)
#define _VN_NET_IS_DAEMON(net)          (net->flags & _VN_PROP_DAEMON)
#define _VN_NET_IS_DONE(net)            (net->flags & _VN_PROP_DONE)

/* Properties for the host */
#define _VN_PROP_LOCAL              0x02  /* Host is local to this game device */
#define _VN_PROP_LAN                0x04  /* Host is on the same LAN */

#define _VN_PROP_NORECV             0x10  /* Do not accept message from host */
#define _VN_PROP_NOSEND             0x20  /* Do not send messages to host */
#define _VN_PROP_JOINING            0x40  /* Host is in the middle of joining
                                             the net */
#define _VN_PROP_LEAVING            0x80  /* Host is in the middle of leaving
                                             the net */
#define _VN_PROP_MAPPING_SYNCED   0x0200  /* Host mapping has been updated */

#define _VN_HOST_IS_NORECV(host)       (host->flags & _VN_PROP_NORECV)
#define _VN_HOST_IS_NOSEND(host)       (host->flags & _VN_PROP_NOSEND)
#define _VN_HOST_IS_JOINING(host)      (host->flags & _VN_PROP_JOINING)
#define _VN_HOST_IS_LEAVING(host)      (host->flags & _VN_PROP_LEAVING)
#define _VN_HOST_IS_UNREACHABLE(host)  \
 (host_info->status.conn_state == _VN_HOST_ST_UNREACHABLE)

#define _VN_HOST_IS_OWNER(host)        (host->flags & _VN_PROP_OWNER)
#define _VN_HOST_IS_LOCAL(host)        (host->flags & _VN_PROP_LOCAL)
#define _VN_HOST_IS_ONLAN(host)        (host->flags & _VN_PROP_LAN)

/* Retx Port flags */
#define _VN_PROP_PORT_SYNC_LOST   0x01    /* Port seq out of sync */
#define _VN_PROP_PORT_RESYNCING   0x02    /* Resyncing port seq */

#define _VN_PORT_SYNC_NEEDED(send_info)                         \
(send_info->retx?                                               \
 ((send_info->retx->flags & _VN_PROP_PORT_SYNC_LOST) &&         \
  !(send_info->retx->flags & _VN_PROP_PORT_RESYNCING)): false )

/* Data structures */

typedef struct
{
    _VN_guid_t       guid;              /* Device ID */
    _VN_host_t       host_id;           /* host ID */
    uint16_t         flags;             /* Bitmask of host properties */

    int64_t          clock_delta;       /* difference between the clock of
                                           this host and my local clock */
    _vn_dlist_t*     channels;          /* connection status of each localhost
                                           to this host (_vn_channel_t) */

#if _VN_INACTIVITY_TIMER
    _vn_timer_t*     inactivity_timer;  /* Inactivity timer */
#endif
    
    uint16_t         npending_to;       /* # of pending messages being sent to this host */
    uint16_t         npending_from;     /* # of pending messages sent from this host */
                                        /* Only used for localhosts */

    _VN_host_status  status;            /* Host status */

} _vn_host_info_t;

typedef struct
{
    _VN_net_t        net_id;            /* network ID */
    _vn_key_t        key;               /* VN key composed of 128 bit AES key
                                            and 160 bit HMAC key  */
    _VN_host_t       master_host;       /* ID of host that created this VN */
    uint16_t         localhosts;        /* number of local hosts */
    _vn_dlist_t*     peer_hosts;        /* pointer to list of peer hosts 
                                           (_vn_host_info_t) */
    _vn_dlist_t*     ports;             /* Receive port info for this net 
                                           (list of _vn_port_info_t) */

    /* TODO: Move into separate structure? */
    _VN_callid_t     listen_call;       /* Call ID of _VN_listen */
    uint16_t         npending;          /* Number of pending message */
    _VN_host_t       host_id_hint;      /* Hint for determining next host id */
    uint8_t          flags;             /* Bitmask of VN properties */
} _vn_net_info_t;

typedef struct
{
    _VN_addr_t       addr;              /* VN address of destination host */
    _VN_port_t       port;              /* Port of this retx info */
    uint8_t          flags;             /* Retx info flags */
    uint8_t          last_acked;        /* Last acked packet */
    _vn_timer_t*     retx_timer;        /* Retransmission timer */
    _vn_dlist_t*     retx_msgs;         /* Buffered messages for retrans */
                                        /* (list of _vn_msg_t) */
    uint32_t         srtt;              /* Smoothed Round Trip Time */
    uint32_t         rttvar;            /* RTT Variance */
} _vn_retx_info_t;


typedef struct
{
    _VN_host_t       host;              /* Local host */ 
    uint32_t         gseq_s;            /* last global sequence number send */
    uint32_t         gseq_r;            /* last global sequence number received */
    _vn_dlist_t*     send_ports;        /* send information (_vn_send_info_t)
                                           for each port for this local host */
} _vn_channel_t;


typedef struct
{
    _VN_port_t       port;              /* Destination port */
    uint8_t          rseq_no;           /* Sequence number for reliable
                                           transmission */
    uint8_t          lseq_no;           /* Sequence number for lossy
                                           transmission */
    uint8_t          pri;               /* Priority */

    /* Used by queue manager */
    uint16_t         rqueued;           /* Reliable packets queued */
    uint16_t         lqueued;           /* Lossy packets queued */

    /* Used by retransmission manager */
    _vn_retx_info_t* retx;
} _vn_send_info_t;

typedef struct
{
    _VN_host_t       host;              /* peer host ID */
    uint8_t          rseq_no;           /* sequence number of last reliably
                                           transmitted packet from this host */
    uint8_t          lseq_no;           /* sequence number of last lossy
                                           transmitted packet from this host */
#if _VN_RECV_TIMER
    /* Recv timer */
    _vn_timer_t*     recv_timer;
#endif
} _vn_port_seq_no_t;

typedef struct
{
    _VN_addr_t       addr;              /* VN address of the local host */
    _VN_port_t       port;              /* port number */
    bool             active;            /* true if this port is currently 
                                            active */
    _vn_dlist_t*     seq;               /* pointer to the peer host and
                                           sequence number info 
                                           (list of _vn_port_seq_no_t) */    
} _vn_port_info_t;


/* Function prototypes */

/* SC net info stuff */
int _vn_core_init();
int _vn_core_cleanup();
int _vn_core_reset();

_vn_ht_table_t* _vn_get_net_info_table();

_VN_net_t _vn_net_newid(uint32_t netmask, bool islocal);
void _vn_net_mark_delete(_vn_net_info_t* net_info);
bool _vn_islocalhost(_vn_net_info_t* net_info, _VN_host_t host_id);

_vn_net_info_t*  _vn_net_info_create(_VN_net_t net_id, _VN_host_t myhost,
                                     _VN_host_t master_host,_vn_key_t key);
_vn_net_info_t*  _vn_add_net_info(_VN_net_t net_id, _VN_host_t myhost,
                                  _VN_host_t master_host, _vn_key_t key);
_vn_net_info_t*  _vn_lookup_net_info(_VN_net_t net_id);
int              _vn_delete_net_info(_VN_net_t net_id);
int              _vn_net_info_find_index(_VN_net_t net_id);
int              _vn_add_host_info_to_net(_vn_net_info_t* net_info, 
                                          _VN_host_t host_id, _VN_guid_t guid,
                                          int64_t clock_delta, uint16_t mask);
int              _vn_add_host_info(_VN_net_t net_id, _VN_host_t host,
                                   _VN_guid_t guid,
                                   int64_t clock_delta, uint16_t mask);
int              _vn_delete_host_info(_VN_net_t net_id, _VN_host_t host_id);
_vn_host_info_t* _vn_lookup_host_info(_vn_net_info_t* net, _VN_host_t host_id);
_vn_host_info_t* _vn_lookup_local_host_info(_vn_net_info_t* net, _VN_host_t host_id);
_vn_host_info_t* _vn_get_first_localhost(_vn_net_info_t* net);

int _vn_get_peer_size(_vn_net_info_t* net_info);
int _vn_get_size(_vn_net_info_t* net_info);
void _vn_net_add_pending(_vn_net_info_t* net_info);
void _vn_net_del_pending(_vn_net_info_t* net_info);
void _vn_host_add_pending_from(_vn_net_info_t* net_info, _vn_host_info_t* host_info);
void _vn_host_del_pending_from(_vn_net_info_t* net_info, _vn_host_info_t* host_info);
void _vn_host_add_pending_to(_vn_net_info_t* net_info, _vn_host_info_t* host_info);
void _vn_host_del_pending_to(_vn_net_info_t* net_info, _vn_host_info_t* host_info);
void _vn_host_mark_leaving(_vn_net_info_t* net_info, _vn_host_info_t* host_info);

/* Receiving port and sequence number */
_vn_port_info_t*   _vn_create_port_info(_VN_addr_t addr, _VN_port_t port_id);
void               _vn_destroy_port_info(_vn_port_info_t* port_info);
int                _vn_add_port_to_net(_vn_net_info_t* net_info,
                                       _vn_port_info_t* port_info);
_vn_port_info_t*   _vn_lookup_port(_VN_addr_t addr, _VN_port_t port);
_vn_port_info_t*   _vn_lookup_port_from_net(_vn_net_info_t* net_info,
                                            _VN_addr_t addr,
                                            _VN_port_t port_id);
_vn_port_seq_no_t* _vn_add_port_seq_no(_vn_port_info_t* port_info,
                                       _VN_host_t host_id);
_vn_port_seq_no_t* _vn_remove_port_seq_no(_vn_port_info_t* port_info,
                                          _VN_host_t host_id);
_vn_port_seq_no_t* _vn_lookup_port_seq_no(_vn_port_info_t* port_info,
                                          _VN_host_t host_id);
int _vn_remove_host_port_seq_no(_VN_net_t net_id, _VN_host_t host_id);

bool _vn_seqno_larger_than(uint8_t x, uint8_t y);

/* Port configuration/priority stuff */
int _vn_activate_default_ports(_VN_addr_t addr);
int _vn_config_port(_VN_addr_t addr, _VN_port_t port, int port_state);
int _vn_set_priority_for_channel(_vn_channel_t* channel, 
                                 _VN_port_t port, int priority);
int _vn_set_priority_for_host(_vn_host_info_t* host_info, _VN_host_t localhost,
                              _VN_port_t port, int priority);
int _vn_set_priority_for_host_all(_vn_net_info_t* net_info, 
                                  _vn_host_info_t* host_info,
                                  _VN_port_t port, int priority);
int _vn_set_priority(_VN_net_t net_id, _VN_host_t host_id, 
                     _VN_port_t port, int priority);

/* Send info stuff */
_vn_channel_t* _vn_channel_create(_VN_host_t localhost);
void _vn_channel_destroy(_vn_channel_t* channel);
_vn_channel_t* _vn_lookup_channel(_VN_net_t net_id, 
                                    _VN_host_t local, _VN_host_t remote);
_vn_channel_t* _vn_find_channel(_vn_host_info_t* host_info, 
                                  _VN_host_t localhost);
int _vn_add_channel(_vn_host_info_t* host_info, _vn_channel_t* channel);
_vn_channel_t* _vn_remove_channel(_vn_host_info_t* host_info, _VN_host_t localhost);
void _vn_delete_all_channels_for_host(_vn_net_info_t* net_info, _VN_host_t localhost);

_vn_send_info_t* _vn_send_info_create(_VN_port_t port, uint8_t priority);
void _vn_send_info_destroy(_vn_send_info_t* send_info);
_vn_send_info_t* _vn_find_send_info(_vn_channel_t* channel, _VN_port_t port);
int _vn_add_send_info(_vn_channel_t* channel, _vn_send_info_t* send_info);
_vn_send_info_t* _vn_lookup_send_info(_VN_net_t net_id, 
                                      _VN_host_t local, _VN_host_t remote,
                                      _VN_port_t port_id);
int _vn_lookup_create_send_info(_vn_send_info_t** p_send_info,
                                _VN_net_t net_id, 
                                _VN_host_t local, _VN_host_t remote,
                                _VN_port_t port_id);

/* Host status */
_VN_host_status* _vn_lookup_host_status(_VN_net_t net_id, _VN_host_t host_id);
int _vn_host_status_update_rtt(_VN_net_t net_id, _VN_host_t host_id,
                               uint32_t rtt);
int _vn_host_status_update_received(_VN_net_t net_id, _VN_host_t host_id,
                                    _vn_msg_t *msg,
                                    _vn_inaddr_t addr, _vn_inport_t port);
int _vn_host_status_mark_unreachable(_VN_net_t net_id, _VN_host_t host_id);

#ifdef  __cplusplus
}
#endif

#endif /* __VN_NETCORE_H__ */

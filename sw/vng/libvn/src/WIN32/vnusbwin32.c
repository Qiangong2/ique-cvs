//
//               Copyright (C) 2006, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "../vnusb.h"
#include "../vndebug.h"

#include <windows.h>
#include <conio.h>
#include <basetyps.h>
#include <setupapi.h>
#include "usbdi.h"

#define SC_IN_PIPE      0
#define SC_OUT_PIPE     1

#include <initguid.h>

// {9C37855E-3D67-487a-B70E-EB4EA357A969} for BBRDB
DEFINE_GUID( GUID_CLASS_BBRDB, 
0x9c37855e, 0x3d67, 0x487a, 0xb7, 0xe, 0xeb, 0x4e, 0xa3, 0x57, 0xa9, 0x69);

// {B881AF97-51A7-4bc1-9BC5-736E7497B93E} for IQUE NC
DEFINE_GUID( GUID_CLASS_IQUE_NC,
0xb881af97, 0x51a7, 0x4bc1, 0x9b, 0xc5, 0x73, 0x6e, 0x74, 0x97, 0xb9, 0x3e);

/* Returns the USB device name */
char* GetDeviceName (HDEVINFO HardwareDeviceInfo, 
                     PSP_INTERFACE_DEVICE_DATA  DeviceInfoData)
{
    PSP_INTERFACE_DEVICE_DETAIL_DATA     functionClassDeviceData = NULL;
    ULONG predictedLength = 0;
    ULONG requiredLength = 0;
    char* devName = NULL;

    SetupDiGetInterfaceDeviceDetail (
            HardwareDeviceInfo,
            DeviceInfoData,
            NULL, // probing so no output buffer yet
            0, // probing so output buffer length of zero
            &requiredLength,
            NULL); // not interested in the specific dev-node


    predictedLength = requiredLength;

    functionClassDeviceData = (PSP_INTERFACE_DEVICE_DETAIL_DATA) malloc (predictedLength);
    functionClassDeviceData->cbSize = sizeof (SP_INTERFACE_DEVICE_DETAIL_DATA);

    if (! SetupDiGetInterfaceDeviceDetail (
               HardwareDeviceInfo,
               DeviceInfoData,
               functionClassDeviceData,
               predictedLength,
               &requiredLength,
               NULL)) {
        free( functionClassDeviceData );
        return NULL;
    }

    devName = strdup(functionClassDeviceData->DevicePath);

    free( functionClassDeviceData );

    return devName;
}

/* Returns the number of matching USB devices found with their device names */
ULONG EnumerateUsbDevices( LPGUID  pGuid, char** usbDevNames, 
                           ULONG maxDevNames)
{
   ULONG NumberDevices;
   HDEVINFO                 hardwareDeviceInfo;
   SP_INTERFACE_DEVICE_DATA deviceInfoData;
   ULONG                    i;
   BOOLEAN                  done;
   PUSB_DEVICE_DESCRIPTOR   usbDeviceInst;
   PUSB_DEVICE_DESCRIPTOR   *UsbDevices = &usbDeviceInst;
   ULONG  nDevNames;

   *UsbDevices = NULL;
   NumberDevices = 0;
   nDevNames = 0;

   hardwareDeviceInfo = SetupDiGetClassDevs (
                           pGuid,
                           NULL, // Define no enumerator (global)
                           NULL, // Define no
                           (DIGCF_PRESENT | // Only Devices present
                            DIGCF_INTERFACEDEVICE));

   NumberDevices = 4;
   done = FALSE;
   deviceInfoData.cbSize = sizeof (SP_INTERFACE_DEVICE_DATA);

   i=0;
   while (!done) {
      NumberDevices *= 2;

      if (*UsbDevices) {
         *UsbDevices = (PUSB_DEVICE_DESCRIPTOR)
               realloc(*UsbDevices, (NumberDevices * sizeof (USB_DEVICE_DESCRIPTOR)));
      } else {
         *UsbDevices = (PUSB_DEVICE_DESCRIPTOR) 
             calloc (NumberDevices, sizeof (USB_DEVICE_DESCRIPTOR));
      }

      if (NULL == *UsbDevices) {  
         SetupDiDestroyDeviceInfoList (hardwareDeviceInfo);
         return nDevNames;
      }

      usbDeviceInst = *UsbDevices + i;
      for (; i < NumberDevices; i++) {
         if (SetupDiEnumDeviceInterfaces (hardwareDeviceInfo,
                                         0, // We don't care about specific PDOs
                                         pGuid,
                                         i,
                                         &deviceInfoData)) {
             usbDevNames[nDevNames] = GetDeviceName (hardwareDeviceInfo, 
                                                     &deviceInfoData);
             if (usbDevNames[nDevNames]) {
                 nDevNames++;
                 if (nDevNames >= maxDevNames) {
                     done = TRUE;
                     break;
                 }
             }
         } else {
            if (ERROR_NO_MORE_ITEMS == GetLastError()) {
               done = TRUE;
               break;
            }
         }  
      }
   }

   NumberDevices = i;
   SetupDiDestroyDeviceInfoList (hardwareDeviceInfo);
   free ( *UsbDevices );

   return nDevNames;
}

_vn_usb_handle_t _vn_lookup_usb_handle(char* devName)
{
    int i;
    _vn_usb_t *usb;
    assert(devName);
    for (i = 0; i < _VN_USB_MAX; i++) {
        usb = _vn_usb_get(i);
        if (usb && usb->handle != _VN_USB_HANDLE_INVALID) {
            if (usb->mode == _VN_USB_MODE_REAL) {
                if (usb->usb.ureal.devName) {
                    if (strcmp(usb->usb.ureal.devName, devName) == 0) {
                        return usb->handle;
                    }
                }
            }
        }
    }
    return _VN_USB_HANDLE_INVALID;
}

char* GetUsbDeviceFileName( LPGUID  pGuid )
{
    char* usbDevNames[_VN_USB_MAX];
    char* devFileName = NULL;
    ULONG i, nDevices;
    
    nDevices = EnumerateUsbDevices( pGuid, usbDevNames, _VN_USB_MAX);
    for (i = 0; i < nDevices; i++) {
        if (usbDevNames[i]) {
            if (devFileName == NULL) {
                if (_vn_lookup_usb_handle(usbDevNames[i])
                    == _VN_USB_HANDLE_INVALID) {
                    /* Not associated with any VN USB Handle yet */
                    devFileName = strdup(usbDevNames[i]);
                }
            }
            free(usbDevNames[i]);
        }
    }

    _VN_TRACE(TRACE_FINEST, _VN_SG_USB, 
              "Got %d USB devices: unassociated USB device %s\n",
              nDevices, (devFileName? devFileName:"none"));

    return devFileName;
}

HANDLE OpenDevice (char *devName)
{
    HANDLE hOut = INVALID_HANDLE_VALUE;

    _VN_TRACE( TRACE_FINER, _VN_SG_USB, 
               "Attempting to open USB %s\n", devName );
    hOut = CreateFile (
                  devName,
                  GENERIC_READ | GENERIC_WRITE,
                  FILE_SHARE_READ | FILE_SHARE_WRITE,
                  NULL, // no SECURITY_ATTRIBUTES structure
                  OPEN_EXISTING, // No special create flags
                  0, // No special attributes
                  NULL); // No template file

    if (INVALID_HANDLE_VALUE == hOut) {
        LPVOID errstr = _vn_sys_get_errstr(GetLastError());
        _VN_TRACE( TRACE_ERROR, _VN_SG_USB, 
                   "FAILED to open USB %s: %s\n", devName, errstr );
        _vn_sys_free_errstr(errstr);        
    }

    return hOut;
}

HANDLE OpenUsbPipe(char* usbDevName, int which)
{
    HANDLE h, hDev;
    char pipename[8], usbPipeName[512];

    strcpy(usbPipeName, usbDevName);
    strcat(usbPipeName, "\\");
    sprintf(pipename, "PIPE%02d", which);
    strcat (usbPipeName, pipename);

    _VN_TRACE( TRACE_FINER, _VN_SG_USB, 
               "USB DeviceName = (%s)\n", usbDevName);

    /* Check device */
    hDev = OpenDevice( usbDevName );
    if ( hDev == INVALID_HANDLE_VALUE )
    {
        return INVALID_HANDLE_VALUE;
    }

    CloseHandle( hDev );

    h = CreateFile(usbPipeName,
        GENERIC_WRITE | GENERIC_READ,
        FILE_SHARE_WRITE | FILE_SHARE_READ,
        NULL,
        OPEN_EXISTING,
        0, 
        NULL);

    if (h == INVALID_HANDLE_VALUE) {
        LPVOID errstr = _vn_sys_get_errstr(GetLastError());
        _VN_TRACE( TRACE_ERROR, _VN_SG_USB,
                   "Failed to open USB (%s): %s", usbPipeName, errstr);
        _vn_sys_free_errstr(errstr);        
    } else {
        _VN_TRACE( TRACE_FINER, _VN_SG_USB,
                  "USB %s Opened successfully.\n", usbPipeName);
    }       

    return h;
}

/* USB Functions for VN */

int _vn_init_usb_real(_vn_usb_t* usb)
{
    _vn_usb_real_t* usb_real;
    int rv = _VN_ERR_OK;

    assert(usb);
    assert(usb->mode == _VN_USB_MODE_REAL);

    usb_real = &usb->usb.ureal;

    usb_real->devName = GetUsbDeviceFileName( (LPGUID) &GUID_CLASS_IQUE_NC );
    if (usb_real->devName == NULL) {
        usb_real->devName = GetUsbDeviceFileName( (LPGUID) &GUID_CLASS_BBRDB );
        if (usb_real->devName == NULL) {
            return _VN_ERR_USB;
        }
    }

    /* Initialize USB communication */
    usb_real->uwh = OpenUsbPipe(usb_real->devName, SC_OUT_PIPE);
    usb_real->urh = OpenUsbPipe(usb_real->devName, SC_IN_PIPE);

    if (usb_real->uwh == INVALID_HANDLE_VALUE || 
        usb_real->urh == INVALID_HANDLE_VALUE) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_USB, "Error opening USB device\n");
        rv = _VN_ERR_USB;
    }

    return rv;
}

int _vn_shutdown_usb_real(_vn_usb_t* usb)
{
    _vn_usb_real_t* usb_real;

    assert(usb);
    assert(usb->mode == _VN_USB_MODE_REAL);

    usb_real = &usb->usb.ureal;

    if (usb_real->uwh != INVALID_HANDLE_VALUE) {
        CloseHandle(usb_real->uwh);
    }
    if (usb_real->urh != INVALID_HANDLE_VALUE) {
        CloseHandle(usb_real->urh);
    }

    if (usb_real->devName) {
        free(usb_real->devName);
    }
    return _VN_ERR_OK;
}

/* Blocking read */
int _vn_read_usb_real(_vn_usb_t* usb, void* buf, size_t len, uint32_t timeout)
{
    _vn_usb_real_t* usb_real;
    unsigned long rlen;
    int rv = 0;

    assert(usb);
    assert(usb->mode == _VN_USB_MODE_REAL);

    usb_real = &usb->usb.ureal;

    /* TODO: Wait for something to read */
    /* TODO: Take into account timeout */

    /* Assumes there is only one reader thread (no lock around read) */

    if (ReadFile(usb_real->urh, buf, len, &rlen, NULL)) {
        rv = rlen;

        /* 0 indicates end of stream */
        /*if (rv == 0) {
            rv = _VN_ERR_CLOSED;
            } */
    } else {
        LPVOID errstr = _vn_sys_get_errstr(GetLastError());
        _VN_TRACE(TRACE_ERROR, _VN_SG_USB,
                  "USB read failed: %s\n", errstr);
        _vn_sys_free_errstr(errstr);        
        rv = _VN_ERR_USB;
    }

    return rv;

}

/* Blocking write */
int _vn_write_usb_real(_vn_usb_t* usb,
                       const void* buf, size_t len, uint32_t timeout)
{
    _vn_usb_real_t* usb_real;
    unsigned long wlen;
    int rv;

    assert(usb);
    assert(usb->mode == _VN_USB_MODE_REAL);

    usb_real = &usb->usb.ureal;

    /* Lock around write, there may be multiple write threads  */
    _vn_mutex_lock(&usb->mutex);
    _VN_TRACE(TRACE_FINEST, _VN_SG_USB, "USB Writing %d bytes\n", len);
    if (WriteFile(usb_real->uwh, buf, len, &wlen, NULL)) {
        rv = (int) wlen;
    } else {
        LPVOID errstr = _vn_sys_get_errstr(GetLastError());
        _VN_TRACE(TRACE_ERROR, _VN_SG_USB,
                  "USB write failed: %s\n", errstr);
        _vn_sys_free_errstr(errstr);        
        rv = _VN_ERR_USB;
    }

    _vn_mutex_unlock(&usb->mutex);

    return rv;
}

int _vn_check_usb_status_real(_vn_usb_t* usb)
{
    /* Checks USB status */
    _vn_usb_real_t* usb_real;
    assert(usb);
    assert(usb->mode == _VN_USB_MODE_REAL);

    usb_real = &usb->usb.ureal;

    _vn_mutex_lock(&usb->mutex);
    if (usb_real->uwh == INVALID_HANDLE_VALUE || 
        usb_real->urh == INVALID_HANDLE_VALUE) {
        usb->state = _VN_USB_STATE_CLOSED;
    } else {
        usb->state = _VN_USB_STATE_READY;
    }
    _vn_mutex_unlock(&usb->mutex);
    return _VN_ERR_OK;
}

//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnusbrpc.h"
#include "vndebug.h"

#define _VN_USB_RPC_TABLE_SIZE      16

/* RPC interface on top of USB */

_vn_ht_table_t _vn_usb_rpc_table;
_vn_mutex_t _vn_usb_rpc_mutex;

#if _VN_USB_RPC_STATS
_vn_stat_t _vn_usb_rpc_wait_time;
#endif

/* Internal helper functions */

_vn_usb_rpc_t* _vn_usb_rpc_create(_vn_usb_rpc_service_t service,
                                  _vn_usb_rpc_proc_id_t proc_id,
                                  _vn_usb_rpc_proc_id_t proc_flags,
                                  _vn_usb_rpc_proc_t proc)
{
    _vn_usb_rpc_t *rpc = _vn_malloc(sizeof(_vn_usb_rpc_t));
    if (rpc != NULL) {
        rpc->proc_id = proc_id;
        rpc->proc_flags = proc_flags;
        rpc->proc = proc;
    }
    return rpc;
}

void _vn_usb_rpc_destroy(_vn_usb_rpc_t *rpc)
{
    if (rpc != NULL) {
        _vn_free(rpc);
    }
}

_vn_usb_rpc_table_t* _vn_usb_rpc_table_create(_vn_usb_rpc_service_t service)
{
    _vn_usb_rpc_table_t *rpc_table = _vn_malloc(sizeof(_vn_usb_rpc_table_t));
    if (rpc_table != NULL) {
        rpc_table->service = service;
        _vn_ht_init(&rpc_table->procs, _VN_USB_RPC_TABLE_SIZE, 0,
                    true /* resize? */,
                    _vn_ht_hash_int, _vn_ht_key_comp_int);
    }
    return rpc_table;
}

void _vn_usb_rpc_table_destroy(_vn_usb_rpc_table_t *rpc_table)
{
    if (rpc_table) {
        _vn_ht_clear(&rpc_table->procs, (void*) _vn_usb_rpc_destroy, true);
        _vn_free(rpc_table);
    }
}

_vn_usb_rpc_table_t* _vn_usb_rpc_get_table(_vn_usb_rpc_service_t service)
{
    return (_vn_usb_rpc_table_t*) _vn_ht_lookup(&_vn_usb_rpc_table,
                                                (_vn_ht_key_t) ((uint32_t) service));
}

_vn_usb_rpc_waiter_t* _vn_usb_rpc_create_waiter(uint32_t msgid,
                                                _vn_usb_rpc_service_t service,
                                                _vn_usb_rpc_proc_id_t proc_id,
                                                void       *ret,
                                                size_t     *retlen)
{
    _vn_usb_rpc_waiter_t *waiter = _vn_malloc(sizeof(_vn_usb_rpc_waiter_t));
    if (waiter) {
        _vn_cond_init(&waiter->cond);
        waiter->msgid = msgid;
        waiter->service = service;
        waiter->proc_id = proc_id;
        waiter->ret = ret;
        waiter->retlen = retlen;
        waiter->status = _VN_ERR_PENDING;
        waiter->done = false;
    }
    return waiter;
}

void _vn_usb_rpc_destroy_waiter(_vn_usb_rpc_waiter_t *waiter)
{
    if (waiter) {
        _vn_cond_destroy(&waiter->cond);
        _vn_free(waiter);
    }
}

int _vn_usb_rpc_signal_waiter(_vn_usb_rpc_waiter_t *waiter)
{
    assert(waiter);
    return _vn_cond_signal(&waiter->cond);
}

int _vn_usb_rpc_wait_waiter(_vn_usb_rpc_waiter_t *waiter, uint32_t timeout)
{
    int rv;
#if _VN_USB_RPC_STATS
    _VN_time_t t1, t2;
#endif
    assert(waiter);
#if _VN_USB_RPC_STATS
    t1 = _vn_get_timestamp();   
#endif
    rv = _vn_cond_timedwait(&waiter->cond, &_vn_usb_rpc_mutex, timeout);
#if _VN_USB_RPC_STATS
    t2 = _vn_get_timestamp();
    _vn_stat_add(&_vn_usb_rpc_wait_time, t2 - t1 );
    _vn_stat_print(stdout, &_vn_usb_rpc_wait_time, "USB RPC WAIT TIME");
#endif
    return rv;
}

int _vn_usb_rpc_cancel_waiters(_vn_dlist_t *waiters)
{
    _vn_dnode_t *node;
    assert(waiters);
    _vn_dlist_for(waiters, node)
    {
        _vn_usb_rpc_waiter_t* waiter;
        waiter = (_vn_usb_rpc_waiter_t*) _vn_dnode_value(node);
        if (waiter) {
            _vn_usb_rpc_signal_waiter(waiter);
        }
    }
    return _VN_ERR_OK;
}

/* Returns the waiter that is waiting for specified RPC response */
_vn_usb_rpc_waiter_t* _vn_usb_rpc_find_waiter(_vn_usb_t *usb, uint32_t msgid,
                                              _vn_usb_rpc_service_t service,
                                              _vn_usb_rpc_proc_id_t proc_id)
                                            
{
    _vn_usb_rpc_state_t *rpc_state;
    _vn_dnode_t *node;

    assert(usb);
    
    rpc_state = (_vn_usb_rpc_state_t*) usb->data;
    assert(rpc_state);

    _vn_dlist_for(&rpc_state->waiters, node)
    {
        _vn_usb_rpc_waiter_t* waiter;
        waiter = (_vn_usb_rpc_waiter_t*) _vn_dnode_value(node);
        if (waiter) {
            if ((waiter->msgid == msgid) &&
                (waiter->service == service) &&
                (waiter->proc_id == proc_id)) {
                return waiter;
            }
        }
    }
    return NULL;
}

/* Adds a new waiter */
int  _vn_usb_rpc_add_waiter(_vn_usb_t *usb,
                            _vn_usb_rpc_waiter_t *waiter)
{
    _vn_usb_rpc_state_t *rpc_state;
    assert(usb);
    assert(waiter);
    
    rpc_state = (_vn_usb_rpc_state_t*) usb->data;
    assert(rpc_state);

    if (rpc_state->state != _VN_STATE_RUNNING) {
        return _VN_ERR_CLOSED;
    }

    if (_vn_usb_rpc_find_waiter(usb, waiter->msgid,
                                waiter->service, waiter->proc_id) != NULL) 
    {
        return _VN_ERR_DUPENTRY;
    }
    else {
        return _vn_dlist_add(&rpc_state->waiters, waiter);
    }
}

/* Removes waiter */
int  _vn_usb_rpc_remove_waiter(_vn_usb_t *usb,
                               _vn_usb_rpc_waiter_t *waiter)
{
    _vn_usb_rpc_state_t *rpc_state;
    _vn_dnode_t *node, *temp;

    assert(usb);
    assert(waiter);
    
    rpc_state = (_vn_usb_rpc_state_t*) usb->data;
    assert(rpc_state);

    _vn_dlist_delete_for(&rpc_state->waiters, node, temp)
    {
        if (waiter == _vn_dnode_value(node)) {
            _vn_dlist_delete(&rpc_state->waiters, node);
            return _VN_ERR_OK;
        }
    }
    return _VN_ERR_NOTFOUND;
}

/**
 * Processes the RPC response by finding an outstanding waiter that is
 * waiting for the message (by message id, service, procedure id).
 * If no waiter is found, the RPC response is ignored.
 *
 * @param usb      Pointer to usb device on which the response was received
 * @param msg      Pointer to RPC message structure
 * @param args     Pointer to buffer containing the return arguments
 *
 * @return         _VN_ERR_OK if response successfully processed
 *                 _VN_ERR_NOTFOUND if no waiter for the RPC response
 *                 _VN_ERR_DUPENTRY if waiter found, but no longer waiting
 */
int _vn_usb_rpc_proc_response(_vn_usb_t *usb,
                              _vn_usb_rpc_msg_t *msg,
                              const void* args)
{
    _vn_usb_rpc_waiter_t *waiter;

    assert(usb);
    assert(msg);

    _VN_TRACE(TRACE_FINER, _VN_SG_RPC, "_vn_usb_rpc_proc_response: "
              "usb handle %d, msgid %u, service %u, proc 0x%04x\n",
              usb->handle, msg->msgid, msg->service, msg->proc_id);

    waiter = _vn_usb_rpc_find_waiter(usb, msg->msgid,
                                     msg->service, msg->proc_id);
    if (waiter != NULL) {
        if (waiter->done) {
            return _VN_ERR_DUPENTRY;
        }
        waiter->done = true;
        waiter->status = msg->status;
        if (args) {
            if (waiter->retlen) { *waiter->retlen = msg->argslen; }
            if (waiter->ret) { 
                memcpy(waiter->ret, args, msg->argslen); 
            }
        }
        else {
            if (waiter->retlen) { *waiter->retlen = 0; }
        }
        _vn_usb_rpc_signal_waiter(waiter);
        return _VN_ERR_OK;
    }
    else {
        return _VN_ERR_NOTFOUND;
    }
}

/**
 * Wait for a RPC response to complete
 * @param usb      Pointer to usb device on which to expect the response
 * @param msgid    Message ID of RPC response to expect
 * @param service  Service to which the RPC request was directed to
 * @param proc_id  Procedure ID of the RPC request
 * @param ret      Pointer to buffer to store the return values
 * @param retlen   Pointer to the length of the ret buffer (in bytes)
 *                 Used to return the number of bytes written to ret.
 * @param timeout  Number of ms to wait before timing out
 *
 * @return         Return value of the RPC call
 *                 _VN_ERR_NOMEM if there is no more memory 
 *                 _VN_ERR_TIMEOUT if the wait timed out
 */
int _vn_usb_wait_response(_vn_usb_t *usb, uint32_t msgid,
                          _vn_usb_rpc_service_t service,
                          _vn_usb_rpc_proc_id_t proc_id,
                          void       *ret,
                          size_t     *retlen,
                          uint32_t timeout)
{
    /* Check if message have arrived */
    _vn_usb_rpc_waiter_t *waiter = NULL;
    int rv;

    _VN_TRACE(TRACE_FINER, _VN_SG_RPC, "_vn_usb_wait_response: usb handle %d, "
              "msgid %u, service %u, proc 0x%04x, timeout %u\n",
              usb->handle, msgid, service, proc_id, timeout);
    
    waiter = _vn_usb_rpc_create_waiter(msgid, service, proc_id, ret, retlen);
    if (waiter == NULL) {
        return _VN_ERR_NOMEM;
    }

    rv = _vn_usb_rpc_add_waiter(usb, waiter);
    if (rv >= 0) {        
        _vn_usb_rpc_wait_waiter(waiter, timeout);
        _vn_usb_rpc_remove_waiter(usb, waiter);
    } else {
        _VN_TRACE(TRACE_WARN, _VN_SG_RPC,
                  "_vn_usb_wait_response: Error adding waiter %d\n", rv);
    }

    if (!waiter->done) {
        rv = _VN_ERR_TIMEOUT;
        if (waiter->retlen) { *waiter->retlen = 0; }

        if (timeout > 0) {
            _VN_TRACE(TRACE_FINER, _VN_SG_RPC, 
                      "_vn_usb_wait_response timed out: usb handle %d, "
                      "msgid %u, service %u, proc 0x%08x, timeout %u\n",
                      usb->handle, msgid, service, proc_id, timeout);
        }
    }
    else {
        rv  = waiter->status;
    }

    _vn_usb_rpc_destroy_waiter(waiter);

    return rv;
}

/**
 * Copies and enqueues RPC request for processing later
 */
int _vn_usb_queue_rpc(_vn_usb_rpc_state_t* rpc_state,
                      _vn_usb_rpc_msg_t* rpc_msg, 
                      const void* args)
{
    int rv;
    _vn_usb_rpc_msg_t *new_rpc_msg;

    assert(rpc_state);
    assert(rpc_msg);

    new_rpc_msg = _vn_malloc(
        sizeof(_vn_usb_rpc_msg_t) + rpc_msg->argslen + rpc_msg->argsoffset);
    if (new_rpc_msg) {
        if (args == NULL) { args = rpc_msg->args; }
        memcpy(new_rpc_msg, rpc_msg, sizeof(_vn_usb_rpc_msg_t));
        memcpy(new_rpc_msg->args,
               args,
               rpc_msg->argslen + rpc_msg->argsoffset);
        _vn_mutex_lock(&_vn_usb_rpc_mutex);
        if (_vn_dlist_size(&rpc_state->requests) == 0) {
#if _VN_USE_SERVICE_THREAD
            _vn_service_signal(_VN_RPC_MSG);
#else
            _vn_cond_signal(&rpc_state->req_notempty);
#endif
        }
        rv = _vn_dlist_enq_back(&rpc_state->requests, new_rpc_msg);
        _vn_mutex_unlock(&_vn_usb_rpc_mutex);

        if (rv < 0) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_RPC,
                      "_vn_usb_queue_rpc: Error %d enqueuing RPC request\n", rv);
        }
    }
    else {
        _VN_TRACE(TRACE_ERROR, _VN_SG_RPC,
                  "_vn_usb_queue_rpc: Cannot allocate memory for RPC request\n");
        rv = _VN_ERR_NOMEM;
    }            
    return rv;
}

/**
 * Processes the RPC request by calling the registered RPC function
 * @param service  Service to which the RPC request was directed to
 * @param proc_id  Procedure ID of the RPC request
 * @param args     Pointer to buffer containing the input arguments
 * @param argslen  Length of the args buffer (in bytes)
 * @param ret      Pointer to buffer to store the return values
 * @param retlen   Pointer to the length of the ret buffer (in bytes)
 *                 Used to return the number of bytes written to ret.
 * @param timeout  Number of ms to wait before timing out
 * @param proc_fast_only Only process RPC functions with 
 *                       _VN_USB_RPC_FLAGS_PROC_FAST set.
 *
 * @return         Result of a call to the registered RPC function
 *                 _VN_ERR_NOTFOUND if no corresponding RPC function
 *                 _VN_ERR_RPC_NOTFAST if proc_fast_only and the RPC function
 *                     does not have _VN_USB_RPC_FLAGS_PROC_FAST set
 */
int _vn_usb_proc_rpc(_vn_usb_t *usb,
                     _vn_usb_rpc_service_t service,
                     _vn_usb_rpc_proc_id_t proc_id,
                     const void *args,
                     size_t      argslen, 
                     void       *ret,
                     size_t     *retlen,
                     bool proc_fast_only)
{
    int rv;
    _vn_ht_key_t key = (_vn_ht_key_t) ((uint32_t) proc_id);
    _vn_usb_rpc_table_t *rpc_table;
    _vn_usb_rpc_t *rpc;

    _vn_mutex_lock(&_vn_usb_rpc_mutex);
    rpc_table = _vn_usb_rpc_get_table(service);
    if (rpc_table) {
        rpc = _vn_ht_lookup(&rpc_table->procs, key);
        _vn_mutex_unlock(&_vn_usb_rpc_mutex);
        
        if (rpc != NULL) {
            if (!proc_fast_only || (rpc->proc_flags & _VN_USB_RPC_FLAGS_PROC_FAST)) {
                /* RPC Request should be processed immediately */
                rv = (rpc->proc)(usb, args, argslen, ret, retlen);
            } else {
                /* RPC Request should be copied, queued and then processed */
                return _VN_ERR_RPC_NOTFAST;
            }
        }
        else {
            if (retlen) { *retlen = 0; }
            rv = _VN_ERR_NOTFOUND;
        }
    }
    else {
        _vn_mutex_unlock(&_vn_usb_rpc_mutex);
        if (retlen) { *retlen = 0; }
        rv = _VN_ERR_NOTFOUND;
    }

    return rv;
}

/**
 * Sends a RPC message (response or response)
 *
 * @param usb      Pointer to usb device to use for sending the RPC message
 * @param msgid    RPC message id 
 * @param msgtype  RPC message type
 *                 (_VN_USB_RPC_REQUEST or _VN_USB_RPC_RESPONSE)
 * @param status   Return status of the RPC call (ignored for RPC requests)
 * @param service  Service of the RPC message
 * @param proc_id  Procedure ID of the RPC message
 * @param rpc_buf  Buffer to use for rpc msg (will allocate new buffer if NULL)
 * @param args     Pointer to buffer containing the RPC arguments
 * @param argslen  Length of the args buffer (in bytes)
 *
 * @return         _VN_ERR_OK if message successfully sent.
 *                 Otherwise, a negative error code.
 */
int _vn_usb_send_rpc(_vn_usb_t* usb, uint32_t msgid,
                     uint8_t msgtype, int32_t status,
                     _vn_usb_rpc_service_t service,
                     _vn_usb_rpc_proc_id_t proc_id,
                     _vn_wbuf_t *rpc_buf,
                     const void *args,
                     size_t      argslen)
{
    int rv;
    size_t rpc_msg_len;
    _vn_usb_rpc_msg_t *rpc_msg = NULL;
    uint16_t offset = 0;

    assert(usb);

    if (rpc_buf) {
        if (sizeof(_vn_usb_rpc_msg_t) >= rpc_buf->offset) {
            return _VN_ERR_TOO_SHORT;
        }
        
        assert(args == NULL);
        assert(argslen == 0);

        args = rpc_buf->buf + rpc_buf->offset;
        argslen = rpc_buf->len;
        offset = rpc_buf->offset - sizeof(_vn_usb_rpc_msg_t);
        rpc_msg = (_vn_usb_rpc_msg_t*) rpc_buf->buf;
    }

    _VN_TRACE(TRACE_FINER, _VN_SG_RPC, 
              "_vn_usb_send_rpc: Sending RPC %s %u for "
              "service %u, method 0x%04x, status %d, argslen %d\n", 
              (msgtype == _VN_USB_RPC_REQUEST)? "request": "response",
              msgid, service, proc_id, status, argslen);

    assert(argslen <= _VN_USB_RPC_MAX_ARGSLEN);

    rpc_msg_len = sizeof(_vn_usb_rpc_msg_t) + argslen + offset;
    if (rpc_msg == NULL) {
        /* SC: Make sure rpc_msg is 16 byte aligned, required by _vn_write_usb */
        _VN_TRACE(TRACE_FINER, _VN_SG_RPC, 
                  "_vn_usb_send_rpc: Allocating memory (usb 0x%08x)\n", usb);
        rpc_msg = _vn_malloc_shared(rpc_msg_len);
        if (rpc_msg == NULL) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_RPC, 
                      "_vn_usb_send_rpc: no memory\n");
            return _VN_ERR_NOMEM;
        }

        assert(offset == 0);
        /* Copy args */
        memcpy(rpc_msg->args, args, argslen);
    } 

    /* TODO: Check rpc_msg is aligned */
    
    rpc_msg->version = _VN_USB_RPC_VERSION;
    rpc_msg->msgtype = msgtype;
    rpc_msg->argslen = htons((uint16_t) argslen);
    rpc_msg->argsoffset = htons(offset);
    rpc_msg->flags = htons(0);
    rpc_msg->status = htonl(status);
    rpc_msg->msgid = htonl(msgid);
    rpc_msg->service = htons(service);
    rpc_msg->proc_id = htons(proc_id);

    _VN_TRACE(TRACE_FINER, _VN_SG_RPC, 
              "_vn_usb_send_rpc: Write USB  0x%08x, 0x%08x, %d\n",
              usb, rpc_msg, rpc_msg_len);
    rv = _vn_write_usb(usb, (const void*) rpc_msg, rpc_msg_len, _VN_TIMEOUT_NONE);

    if (rpc_buf == NULL) {
        _vn_free_shared(rpc_msg);
    }
    return rv;
}

/**
 * Parses a incoming RPC packet and stores the result in res
 */
int _vn_usb_parse_rpc(_vn_usb_rpc_msg_t *res, const void* buf, size_t len)
{
    _vn_usb_rpc_msg_t* rpc_msg;
    size_t exp_len;
    assert(res);
    assert(buf);
    
    if (len < sizeof(_vn_usb_rpc_msg_t)) {
        return _VN_ERR_TOO_SHORT;
    }
    rpc_msg = (_vn_usb_rpc_msg_t*) buf;
    if (rpc_msg->version != _VN_USB_RPC_VERSION) {
        return _VN_ERR_BAD_VERSION;
    }
     
    res->version = rpc_msg->version;
    res->msgtype = rpc_msg->msgtype;
    res->argslen = ntohs(rpc_msg->argslen);
    res->argsoffset = ntohs(rpc_msg->argsoffset);
    res->flags = ntohs(rpc_msg->flags);
    res->msgid = ntohl(rpc_msg->msgid);
    res->service = ntohs(rpc_msg->service);
    res->proc_id = ntohs(rpc_msg->proc_id);
    res->status = ntohl(rpc_msg->status);

    exp_len = sizeof(_vn_usb_rpc_msg_t) + res->argslen + res->argsoffset;
    if (len != exp_len) {
        _VN_TRACE(TRACE_WARN, _VN_SG_RPC,
                  "_vn_usb_parse_rpc: Received %d, expected %d\n",
                  len, exp_len);
    }

    if (len < exp_len) {
        return _VN_ERR_TOO_SHORT;
    }

    return _VN_ERR_OK;
}

/* External functions to be called by RPC server/client programs */

/* Initializes the RPC module */
int _vn_usb_init_rpc()
{
    int rv;
    _vn_usb_init_table();

#if _VN_USB_RPC_STATS
    _vn_stat_clear(&_vn_usb_rpc_wait_time);
#endif
    rv = _vn_mutex_init(&_vn_usb_rpc_mutex);
    if (rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_RPC, 
                  "Failed to initialize mutex for USB RPC: %d\n", rv);
        return rv;
    }

    rv = _vn_ht_init(&_vn_usb_rpc_table, 
                     _VN_USB_RPC_TABLE_SIZE, 0,
                     true /* resize? */,
                     _vn_ht_hash_int, _vn_ht_key_comp_int);
    if (rv < 0) {
        _vn_mutex_destroy(&_vn_usb_rpc_mutex);

        _VN_TRACE(TRACE_ERROR, _VN_SG_RPC, 
                  "Failed to initialize table for USB RPC: %d\n", rv);
        return rv;
    }

    return _VN_ERR_OK;
}

/* Shutdown the RPC module */
int _vn_usb_shutdown_rpc()
{
    _vn_ht_clear(&_vn_usb_rpc_table, (void*) _vn_usb_rpc_table_destroy, true);
    _vn_mutex_destroy(&_vn_usb_rpc_mutex);
    return _VN_ERR_OK;
}

/* Registers a RPC service */
int _vn_usb_rpc_register_service(_vn_usb_rpc_service_t service)
{
    int rv;
    _vn_ht_key_t key = (_vn_ht_key_t) ((uint32_t) service);
    _vn_usb_rpc_table_t *rpc_table;

    rpc_table = _vn_usb_rpc_table_create(service);
    if (rpc_table == NULL) {
        return _VN_ERR_NOMEM;
    }

    _vn_mutex_lock(&_vn_usb_rpc_mutex);
    rv = _vn_ht_add(&_vn_usb_rpc_table, key, rpc_table);
    _vn_mutex_unlock(&_vn_usb_rpc_mutex);

    if (rv < 0) {
        /* Failed to add entry */
        _vn_usb_rpc_table_destroy(rpc_table);
    }
    return rv;
}

/* Unregisters a RPC service */
int _vn_usb_rpc_unregister_service(_vn_usb_rpc_service_t service)
{
    _vn_ht_key_t key = (_vn_ht_key_t) ((uint32_t) service);
    _vn_usb_rpc_table_t *rpc_table;

    _vn_mutex_lock(&_vn_usb_rpc_mutex);
    rpc_table = _vn_ht_remove(&_vn_usb_rpc_table, key);
    _vn_mutex_unlock(&_vn_usb_rpc_mutex);
    
    if (rpc_table == NULL) return _VN_ERR_NOTFOUND;
    _vn_usb_rpc_table_destroy(rpc_table);
    return _VN_ERR_OK;
}

/* Registers a RPC method */
int _vn_usb_register_rpc(_vn_usb_rpc_service_t service,
                         _vn_usb_rpc_proc_id_t proc_id,
                         uint16_t proc_flags,
                         _vn_usb_rpc_proc_t proc)
{
    int rv;
    _vn_ht_key_t key = (_vn_ht_key_t) ((uint32_t) proc_id);
    _vn_usb_rpc_table_t *rpc_table;
    _vn_usb_rpc_t *rpc;

    _vn_mutex_lock(&_vn_usb_rpc_mutex);
    rpc_table = _vn_usb_rpc_get_table(service);
    if (rpc_table == NULL) {
        rv = _vn_usb_rpc_register_service(service);
        if (rv < 0) {
            _vn_mutex_unlock(&_vn_usb_rpc_mutex);
            return rv;
        }
        rpc_table = _vn_usb_rpc_get_table(service);
    }
    
    assert(rpc_table);

    rpc = _vn_usb_rpc_create(service, proc_id, proc_flags, proc);
    if (rpc) {
        rv = _vn_ht_add(&rpc_table->procs, key, rpc);
        if (rv < 0) {
            /* Failed to add entry */
            _vn_usb_rpc_destroy(rpc);
        }
    }
    else {
        rv = _VN_ERR_NOMEM;
    }
    _vn_mutex_unlock(&_vn_usb_rpc_mutex);
    return rv;
}

/* Unregisters a RPC method */
int _vn_usb_unregister_rpc(_vn_usb_rpc_service_t service,
                           _vn_usb_rpc_proc_id_t proc_id)
{
    _vn_ht_key_t key = (_vn_ht_key_t) ((uint32_t) proc_id);
    _vn_usb_rpc_table_t *rpc_table;
    _vn_usb_rpc_t *rpc;
    int rv;

    _vn_mutex_lock(&_vn_usb_rpc_mutex);
    rpc_table = _vn_usb_rpc_get_table(service);
    if (rpc_table) { 
        rpc = _vn_ht_remove(&rpc_table->procs, key);    
        if (rpc) {
            _vn_usb_rpc_destroy(rpc);
            rv = _VN_ERR_OK;
        }
        else {
            rv = _VN_ERR_NOTFOUND;
        }
    }
    else {
        rv = _VN_ERR_NOTFOUND;
    }
    _vn_mutex_unlock(&_vn_usb_rpc_mutex);

    return rv;
}

/* 
 * Calls a RPC method.  This call blocks until a response is received or
 * the call times out.
 *
 * @param handle   USB handle to use for RPC call
 * @param service  Service of the RPC call
 * @param proc_id  Procedure ID of the RPC call
 * @param rpc_buf  Buffer to use for RPC call (will allocate new buffer if NULL)
 * @param args     Pointer to buffer containing the RPC arguments
 * @param argslen  Length of the args buffer (in bytes)
 * @param ret      Pointer to buffer to store the return values
 * @param retlen   Pointer to the length of the ret buffer (in bytes)
 *                 Used to return the number of bytes written to ret.
 * @param timeout  Number of ms to wait before timing out
 *
 * @return         Result of a call to the registered RPC function
 *                 _VN_ERR_TIMEOUT if call timed out
 */
int _vn_usb_call_rpc_impl(_vn_usb_handle_t handle,
                          _vn_usb_rpc_service_t service,
                          _vn_usb_rpc_proc_id_t proc_id,
                          _vn_wbuf_t *rpc_buf,
                          const void *args,
                          size_t      argslen, 
                          void       *ret,
                          size_t     *retlen,
                          uint32_t timeout)
{
    int rv;
    _vn_usb_t *usb = _vn_usb_get(handle);
    _vn_usb_rpc_state_t *rpc_state;
    uint32_t reqid;

    if (usb == NULL) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_RPC,
                  "_vn_usb_call_rpc: usb handle %d not connected\n", handle);
        return _VN_ERR_CLOSED;
    }

     _vn_mutex_lock(&_vn_usb_rpc_mutex);
    rpc_state = (_vn_usb_rpc_state_t*) usb->data;

    if (rpc_state == NULL) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_RPC,
                  "_vn_usb_call_rpc: usb %d rpc state is null\n", handle);
        _vn_mutex_unlock(&_vn_usb_rpc_mutex);
        return _VN_ERR_CLOSED;
    }

    _VN_TRACE(TRACE_FINER, _VN_SG_RPC, "_vn_usb_call_rpc: usb handle %d, "
              "service %u, proc 0x%04x, timeout %u\n",
              handle, service, proc_id, timeout);
    
    /* Send RPC message */
    reqid = rpc_state->reqid;
    rv = _vn_usb_send_rpc(usb, reqid, _VN_USB_RPC_REQUEST, 
                          /* Only need response if timeout is not zero */
                          (timeout == 0)? 0:1,
                          service, proc_id, rpc_buf, args, argslen);
    rpc_state->reqid++;

    if (rv >= 0) {
        if (timeout == 0) {
            /* Don't bother waiting */
            rv = _VN_ERR_TIMEOUT;
        } else {
            /* Wait for RPC response */
            rv = _vn_usb_wait_response(usb, reqid, service, proc_id,
                                       ret, retlen, timeout);
        }
    }

    _vn_mutex_unlock(&_vn_usb_rpc_mutex);

    return rv;
}

int _vn_usb_call_rpc(_vn_usb_handle_t handle,
                     _vn_usb_rpc_service_t service,
                     _vn_usb_rpc_proc_id_t proc_id,
                     const void *args,
                     size_t      argslen, 
                     void       *ret,
                     size_t     *retlen,
                     uint32_t timeout)
{
    return _vn_usb_call_rpc_impl(handle, service, proc_id, 
                                 NULL, args, argslen, ret, retlen, timeout);
}

int _vn_usb_call_rpc_nocopy(_vn_usb_handle_t handle,
                            _vn_usb_rpc_service_t service,
                            _vn_usb_rpc_proc_id_t proc_id,
                            _vn_wbuf_t *rpc_buf,
                            void       *ret,
                            size_t     *retlen,
                            uint32_t timeout)
{
    return _vn_usb_call_rpc_impl(handle, service, proc_id, rpc_buf, 
                                 NULL, 0, ret, retlen, timeout);
}

/* Process RPC request and sends response */
int _vn_usb_service_rpc(_vn_usb_t* usb,
                        _vn_usb_rpc_msg_t *rpc_msg,
                        const void *args, size_t argslen, 
                        char* resp_args, size_t resp_argsmaxlen,
                        bool proc_fast_only)
{
    int rv;
    size_t resp_argslen;

    assert(usb);
    assert(rpc_msg);
    assert(resp_args || resp_argsmaxlen == 0);

    resp_argslen = resp_argsmaxlen;

    /* Process request */
    if (args == NULL) {
        args = rpc_msg->args + rpc_msg->argsoffset;
        argslen = rpc_msg->argslen;
    }
    rv = _vn_usb_proc_rpc(usb, rpc_msg->service, rpc_msg->proc_id,
                          args, argslen,
                          resp_args, &resp_argslen, proc_fast_only);
        
    /* Send response */
    if ((rv != _VN_ERR_RPC_NOTFAST) && rpc_msg->status) {
        rv = _vn_usb_send_rpc(usb, rpc_msg->msgid, _VN_USB_RPC_RESPONSE,
                              rv, rpc_msg->service, rpc_msg->proc_id,
                              NULL, resp_args, resp_argslen);
    }

    return rv;
}

/* Processes next RPC request (called by service thread) */
bool _vn_usb_rpc_proc_next(_vn_usb_handle_t handle,
                           char* resp_args, size_t resp_argsmaxlen)
{
    _vn_usb_rpc_msg_t *rpc_msg;
    _vn_usb_rpc_state_t *rpc_state;
    _vn_usb_t* usb;

    usb = _vn_usb_get(handle);

    assert(usb);    
    assert(resp_args);

    rpc_state = (_vn_usb_rpc_state_t*) usb->data;
    assert(rpc_state);

    _vn_mutex_lock(&_vn_usb_rpc_mutex);    
    rpc_msg = _vn_dlist_deq_front(&rpc_state->requests);

#if _VN_USE_SERVICE_THREAD
    /* Make sure RPC gets called again */
    if (!_vn_dlist_empty(&rpc_state->requests)) {
        _vn_service_signal(_VN_RPC_MSG);
    }
#endif

    _vn_mutex_unlock(&_vn_usb_rpc_mutex);

    if (rpc_msg) {
        int rv;
        rv = _vn_usb_service_rpc(usb, rpc_msg, NULL, 0,
                                 resp_args, resp_argsmaxlen, false);
        _vn_free(rpc_msg);
        return true;
    }
    return false;
}

#if !_VN_USE_SERVICE_THREAD
/**
 * RPC server loop for processing RPC requests
 * Separate from RPC dispatcher so we can process responses (non-blocking)
 *  and still process requests (possibly blocking)
 */ 
void _vn_usb_rpc_server(_vn_usb_t *usb)
{
    int rv;
    size_t resp_argsmaxlen = _VN_USB_RPC_MAX_ARGSLEN+1;
    _vn_usb_rpc_state_t *rpc_state;
    char* resp_args;
    int handle;

    assert(usb);    

    resp_args = _vn_malloc(resp_argsmaxlen);
    if (resp_args == NULL) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_RPC,
                  "Error starting USB RPC server: Out of memory\n");
        return;
    }

    handle = usb->handle;
    _VN_TRACE(TRACE_FINE, _VN_SG_RPC, "Starting USB RPC server %u\n", handle);

    _vn_mutex_lock(&_vn_usb_rpc_mutex);
    rpc_state = (_vn_usb_rpc_state_t*) usb->data;
    assert(rpc_state);

    while (rpc_state->state == _VN_STATE_RUNNING) {
        _vn_usb_rpc_msg_t *rpc_msg;

        /* Wait for the request queue to be not empty */
        if (_vn_dlist_size(&rpc_state->requests) == 0) {
            _vn_cond_timedwait(&rpc_state->req_notempty,
                               &_vn_usb_rpc_mutex, rpc_state->timeout);
        }
        
        /* Process RPC messages */
        while ((rpc_msg = _vn_dlist_deq_front(&rpc_state->requests))) {
            _vn_mutex_unlock(&_vn_usb_rpc_mutex);
            rv = _vn_usb_service_rpc(usb, rpc_msg, NULL, 0,
                                     resp_args, resp_argsmaxlen, false);
            _vn_free(rpc_msg);
            _vn_mutex_lock(&_vn_usb_rpc_mutex);
        }
    }

    _vn_mutex_unlock(&_vn_usb_rpc_mutex);
    _vn_free(resp_args);

    _VN_TRACE(TRACE_FINE, _VN_SG_RPC, "Stopping USB RPC server %u\n", handle);
}
#endif

_vn_usb_rpc_state_t* _vn_usb_rpc_state_create()
{
    _vn_usb_rpc_state_t *rpc_state;
    rpc_state = _vn_malloc(sizeof(_vn_usb_rpc_state_t));
    if (rpc_state) {
        rpc_state->reqid = 0;
        _vn_dlist_init(&rpc_state->waiters);

        _vn_dlist_init(&rpc_state->requests);
#if !_VN_USE_SERVICE_THREAD
        _vn_cond_init(&rpc_state->req_notempty);
#endif
        rpc_state->state = 0;

        rpc_state->timeout = 0;
        rpc_state->disp_thread = 0;
        rpc_state->server_thread = 0;
        rpc_state->disp_stop_cb = NULL;
    }
    return rpc_state;
}

void _vn_usb_rpc_state_destroy(_vn_usb_rpc_state_t *rpc_state)
{
    if (rpc_state) {
        _vn_dlist_clear(&rpc_state->waiters, 
                        (void*) _vn_usb_rpc_destroy_waiter);
        _vn_dlist_clear(&rpc_state->requests, (void*) _vn_free);
#if !_VN_USE_SERVICE_THREAD
        _vn_cond_destroy(&rpc_state->req_notempty);
#endif
        _vn_free(rpc_state);
    }
}

int _vn_usb_rpc_recv(_vn_usb_t *usb, void* buf, size_t buflen,
                     uint32_t timeout)
{
    int len;
    int rv;
    _vn_usb_rpc_msg_t rpc_msg;
    _vn_usb_rpc_state_t *rpc_state;

    assert(usb);
#ifdef _SC
    assert(buf || usb->mode == _VN_USB_MODE_REAL);
#else
    assert(buf);
#endif
    rpc_state = (_vn_usb_rpc_state_t*) usb->data;
    assert(rpc_state);
    
    len = _vn_read_usb(usb, buf, buflen, timeout);
    if (len <= 0) {
        return len;
    }

#ifdef _SC
    if (!buf) {
        _vn_usb_real_t* usb_real;
        usb_real = &usb->usb.ureal;
        buf = usb_real->rbufs[usb_real->active_rbuf];
    }
#endif

/*    _VN_TRACE(TRACE_FINE, _VN_SG_RPC, "received usb rpc\n");
      _vn_dbg_dump_buf(stdout, buf, len); */
    
    rv = _vn_usb_parse_rpc(&rpc_msg, buf, len);
    if (rv < 0) return rv;

    if (rpc_msg.msgtype == _VN_USB_RPC_REQUEST) {
        const uint8_t* args = (uint8_t*) buf + sizeof(_vn_usb_rpc_msg_t);

        _VN_TRACE(TRACE_FINER, _VN_SG_RPC,
                  "_vn_usb_rpc_recv: RPC request %u for service %u, "
                  "method 0x%04x, status %d, argslen %d, offset %d\n", 
                  rpc_msg.msgid, rpc_msg.service, rpc_msg.proc_id,
                  rpc_msg.status, rpc_msg.argslen, rpc_msg.argsoffset);


#if 1
#if 0
        if (rpc_msg.proc_id == 1/*_VN_USB_RPC_DEVICE_PROC_UDP_PKT*/) {
            rv = _vn_device_rpc_proc_udp_pkt(usb,
                                             args + rpc_msg.argsoffset,
                                             rpc_msg.argslen,
                                             NULL, 0);
        } else {
            /* Need to copy and queue RPC */
            rv = _vn_usb_queue_rpc(rpc_state, &rpc_msg, args);
        }
#endif

        rv = _vn_usb_service_rpc(usb, &rpc_msg, 
                                 args + rpc_msg.argsoffset,
                                 rpc_msg.argslen,
                                 NULL, 0, true);
        if (rv == _VN_ERR_RPC_NOTFAST) {
            /* Need to copy and queue RPC */
            rv = _vn_usb_queue_rpc(rpc_state, &rpc_msg, args);
        }

#else
        /* Need to copy and queue RPC */
        rv = _vn_usb_queue_rpc(rpc_state, &rpc_msg, args);
#endif
    }
    else if (rpc_msg.msgtype == _VN_USB_RPC_RESPONSE) {
        _VN_TRACE(TRACE_FINER, _VN_SG_RPC,
                  "_vn_usb_rpc_recv: RPC response %u for service %u, "
                  "method 0x%04x, status %d, argslen %d, offset %d\n", 
                  rpc_msg.msgid, rpc_msg.service, rpc_msg.proc_id,
                  rpc_msg.status, rpc_msg.argslen, rpc_msg.argsoffset);

        _vn_mutex_lock(&_vn_usb_rpc_mutex);
        rv = _vn_usb_rpc_proc_response(
            usb, &rpc_msg,
            (char*) buf + rpc_msg.argsoffset + sizeof(_vn_usb_rpc_msg_t));
        _VN_TRACE(TRACE_FINER, _VN_SG_RPC,
                  "_vn_usb_rpc_recv: _vn_usb_rpc_proc_response returned %d\n",
                  rv);
        _vn_mutex_unlock(&_vn_usb_rpc_mutex);
    } else {

        _VN_TRACE(TRACE_ERROR, _VN_SG_RPC,
                  "_vn_usb_rpc_recv: Invalid RPC message type %d\n",
                  rpc_msg.msgtype);                  
        rv = _VN_ERR_INVALID;
    }

    return rv;
}

/**
 * RPC dispatcher
 */ 
void _vn_usb_rpc_dispatcher(_vn_usb_t *usb)
{
    size_t buflen = _VN_USB_RPC_MAX_MSGLEN+4;
    char* buf = NULL; 

    int rv;
    _vn_usb_rpc_state_t *rpc_state;
    uint32_t timeout;
    int handle;

    assert(usb);

#ifdef _SC
    if (usb->mode == _VN_USB_MODE_REAL) {
        buflen = 0; /* Using usb buffers, don't allocate more bufs */
    }
#endif

    /* Make sure rpc_msg is 16 byte aligned, required by _vn_write_usb */
    if (buflen > 0) {
        buf = _vn_malloc_shared(buflen);
        if (buf == NULL) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_RPC,
                      "Error starting USB RPC dispatcher: Out of memory\n");
            return;
        }
    }

    handle = usb->handle;

    _VN_TRACE(TRACE_FINE, _VN_SG_RPC,
              "Starting USB RPC dispatcher %u\n", handle);

    _vn_mutex_lock(&_vn_usb_rpc_mutex);

    rpc_state = (_vn_usb_rpc_state_t*) usb->data;
    assert(rpc_state);

    timeout = rpc_state->timeout;

    while (rpc_state->state == _VN_STATE_RUNNING) {
        _vn_mutex_unlock(&_vn_usb_rpc_mutex);
        rv = _vn_usb_rpc_recv(usb, buf, buflen, timeout);

        _vn_mutex_lock(&_vn_usb_rpc_mutex);
        if (rv == _VN_ERR_CLOSED || rv == _VN_ERR_SOCKET || rv == _VN_ERR_USB) {
            _VN_TRACE(TRACE_FINE, _VN_SG_RPC,
                      "USB handle %u closed (rv = %d)\n",
                      usb->handle, rv);
            rpc_state->state = _VN_STATE_STOP;
        }
    }

    /* Thread stopping, unblock all waiters */
    _vn_usb_rpc_cancel_waiters(&rpc_state->waiters);

    _vn_mutex_unlock(&_vn_usb_rpc_mutex);

    if (rpc_state->disp_stop_cb) {
        (*rpc_state->disp_stop_cb)(usb);
    }

    if (buf) {
        _vn_free_shared(buf);
    }

    _VN_TRACE(TRACE_FINE, _VN_SG_RPC,
              "Stopping USB RPC dispatcher %u\n", handle);
}

#ifdef _SC
static uint8_t  _vn_usb_rpc_thread_stack[_VN_USB_MAX][_VN_THREAD_STACKSIZE];
#endif

/**
 * Starts RPC threads
 * RPC has two threads
 * 1. RPC dispatcher thread - Responsible for retrieving packets,
 *                            processing RPC responses, and sending
 *                            RPC requests (which may be blocking)
 *                            to the server processing thread
 * 2. RPC server processing thread - Responsible for processing RPC requests
 */ 
int _vn_usb_rpc_start(_vn_usb_handle_t handle, uint32_t timeout)
{
    _vn_usb_t *usb = _vn_usb_get(handle);
    _vn_usb_rpc_state_t *rpc_state;
    bool cancel_dispatcher = false;
    bool wait_thread = false;
    int rv = _VN_ERR_OK;
    _vn_thread_attr_t* pDispAttr = NULL;
#ifdef _SC
    _vn_thread_attr_t thread_attr;
    thread_attr.stack = _vn_usb_rpc_thread_stack[handle];
    thread_attr.stackSize = _VN_THREAD_STACKSIZE;
    thread_attr.priority = _VN_THREAD_PRIORITY + 1;
    thread_attr.attributes = IOS_THREAD_CREATE_JOINABLE;
    thread_attr.start = true;
    pDispAttr = &thread_attr;
#endif

    if (usb == NULL) return _VN_ERR_INVALID;

    _vn_mutex_lock(&_vn_usb_rpc_mutex);
    rpc_state = (_vn_usb_rpc_state_t*) usb->data;

    /* Create new RPC state */
    if (rpc_state == NULL) {
        rpc_state = _vn_usb_rpc_state_create();
        if (rpc_state) {
            usb->data = rpc_state;
            usb->free_data_func = (_vn_free_func_t) _vn_usb_rpc_state_destroy;
        }
        else {
            rv = _VN_ERR_NOMEM;
        }
    }

    if (rpc_state) {
        if (rpc_state->state != _VN_STATE_RUNNING) {
            rpc_state->timeout = timeout;
            rpc_state->state = _VN_STATE_RUNNING;

            rv = _vn_thread_create(&rpc_state->disp_thread, pDispAttr, 
                                   (void*) _vn_usb_rpc_dispatcher, usb);

            if (rv < 0) {
                rpc_state->state = _VN_STATE_STOP;
                _VN_TRACE(TRACE_ERROR, _VN_SG_RPC,
                          "Unable to start USB RPC dispatcher thread: %d\n",
                          rv);
                goto out;
            }
            
#if !_VN_USE_SERVICE_THREAD
            rv = _vn_thread_create(&rpc_state->server_thread, NULL, 
                                   (void*) _vn_usb_rpc_server, usb);

            if (rv < 0) {
                rpc_state->state = _VN_STATE_STOP;
                cancel_dispatcher = true;
                _VN_TRACE(TRACE_ERROR, _VN_SG_RPC,
                          "Unable to start USB RPC server thread: %d\n", rv);
                goto out;
            }
#endif

            wait_thread = true;
        }
        else {
            /* RPC threads already running? */
            rv = _VN_ERR_OK;
        }
    }

out:
    _vn_mutex_unlock(&_vn_usb_rpc_mutex);

#ifdef _SC
    if (wait_thread) {
        /* Yield and give USB thread chance to start */
        IOS_YieldThread();
    }
#endif

    if (cancel_dispatcher) {
        _vn_thread_join(rpc_state->disp_thread, NULL); 
    }
    return rv;
}

/**
 * Stops the RPC threads
 */
int _vn_usb_rpc_stop(_vn_usb_handle_t handle)
{
    _vn_usb_t *usb = _vn_usb_get(handle);
    _vn_usb_rpc_state_t *rpc_state;

    if (usb == NULL) return _VN_ERR_INVALID;

    _vn_mutex_lock(&_vn_usb_rpc_mutex);
    rpc_state = (_vn_usb_rpc_state_t*) usb->data;

    if (rpc_state) {
        bool wait_threads;
        if (rpc_state->state == _VN_STATE_RUNNING) {
            rpc_state->state = _VN_STATE_STOP;
        }
        else {
            /* RPC threads not running */
        }
        
        wait_threads = (rpc_state->state == _VN_STATE_STOP);

        _vn_mutex_unlock(&_vn_usb_rpc_mutex);

        if (wait_threads) {
            _vn_thread_join(rpc_state->disp_thread, NULL); 
#if !_VN_USE_SERVICE_THREAD
            _vn_thread_join(rpc_state->server_thread, NULL); 
#endif
        }
    }
    else {
        _vn_mutex_unlock(&_vn_usb_rpc_mutex);
    }

    return _VN_ERR_OK;
}

/**
 * Registers callback functions for RPC state changes
 */
int _vn_usb_rpc_register_callback(_vn_usb_handle_t handle, int state,
                                  _vn_usb_rpc_callback_t callback)
{
    _vn_usb_t *usb = _vn_usb_get(handle);
    _vn_usb_rpc_state_t *rpc_state;
    int rv = _VN_ERR_OK;

    if (usb == NULL) return _VN_ERR_INVALID;

    _vn_mutex_lock(&_vn_usb_rpc_mutex);
    rpc_state = (_vn_usb_rpc_state_t*) usb->data;

    /* Create new RPC state */
    if (rpc_state == NULL) {
        rpc_state = _vn_usb_rpc_state_create();
        if (rpc_state) {
            usb->data = rpc_state;
            usb->free_data_func = (_vn_free_func_t) _vn_usb_rpc_state_destroy;
        }
        else {
            rv = _VN_ERR_NOMEM;
        }
    }

    switch (state) {
    case _VN_USB_ST_CLOSED:
        rpc_state->disp_stop_cb = callback;
        break;
    default:
        rv = _VN_ERR_INVALID;
    }
    _vn_mutex_unlock(&_vn_usb_rpc_mutex);

    return rv;
}

//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#ifndef __VN_RPC_H__
#define __VN_RPC_H__

#include "vnusbrpc.h"
#include "vndevices.h"

#ifdef __cplusplus
extern "C" {
#endif

#define _VN_USB_RPC_SERVICE                           1

/* RPC Procedure IDs for methods on the Device (SC) */
#define _VN_USB_RPC_DEVICE_PROC_UDP_PKT          0x0001
#define _VN_USB_RPC_DEVICE_SEND_KEEP_ALIVE       0x0002
#define _VN_USB_RPC_DEVICE_GET_INFO              0x0003
#define _VN_USB_RPC_DEVICE_SYNC                  0x0004

#define _VN_USB_RPC_DEVICE_SET_TRACE             0x0101

/* RPC Procedure IDs for methods on the VN Proxy */
#define _VN_USB_RPC_PROXY_ADD_NET                0x1001
#define _VN_USB_RPC_PROXY_DEL_NET                0x1002
#define _VN_USB_RPC_PROXY_ADD_HOST               0x1003
#define _VN_USB_RPC_PROXY_DEL_HOST               0x1004
#define _VN_USB_RPC_PROXY_SET_HOST               0x1005
#define _VN_USB_RPC_PROXY_GET_HOST               0x1006

#define _VN_USB_RPC_PROXY_SEND_VN_PKT            0x1101
#define _VN_USB_RPC_PROXY_SEND_UDP_PKT           0x1102

#define _VN_USB_RPC_PROXY_NETIF_GETADDR          0x1201
#define _VN_USB_RPC_PROXY_NETIF_GETLOCALHOSTNAME 0x1202
#define _VN_USB_RPC_PROXY_NETIF_GETLOCALPORT     0x1203
#define _VN_USB_RPC_PROXY_NETIF_GETLOCALIP       0x1204
#define _VN_USB_RPC_PROXY_NETIF_GETLOCALIPS      0x1205
#define _VN_USB_RPC_PROXY_NETIF_GETSTATUS        0x1206

#define _VN_USB_RPC_PROXY_FIREWALL_OPEN          0x1301

#define _VN_USB_RPC_PROXY_NETIF_RESET            0x1401
#define _VN_USB_RPC_PROXY_NETIF_INIT             0x1402

#define _VN_USB_RPC_PROXY_QUERY_HOST             0x1501
#define _VN_USB_RPC_PROXY_CLEAR_QUERY_LIST       0x1502
#define _VN_USB_RPC_PROXY_DISCOVER_HOSTS         0x1503

/* RPC Procedure IDs for methods common to the SC and the VN Proxy */
#define _VN_USB_RPC_COMMON_ADD_DEVICE            0x2001
#define _VN_USB_RPC_COMMON_REMOVE_DEVICE         0x2002
#define _VN_USB_RPC_COMMON_SET_DEVICE_TIMEOUT    0x2009
#define _VN_USB_RPC_COMMON_ADD_SERVICE           0x2011
#define _VN_USB_RPC_COMMON_REMOVE_SERVICE        0x2012
#define _VN_USB_RPC_COMMON_ADD_SERVICES          0x2013
#define _VN_USB_RPC_COMMON_REMOVE_SERVICES       0x2014

/* Used to specify trace setting */
typedef struct {
    int32_t    group;
    int32_t    subgroup;
    int32_t    level;
} _vn_usb_rpc_set_trace_req_t;
 
/* Used to specify what port range the device wants */
typedef struct {
    uint32_t   ipaddr;
    uint16_t   min_port;
    uint16_t   max_port;
} _vn_usb_rpc_ipportrange_t;

/* RPC arg for_VN_USB_RPC_DEVICE_GET_INFO */  
typedef struct {
    _VN_guid_t   guid; /* Device guid (device type and id) */
} _vn_usb_rpc_device_info_t;

/* RPC arg for _VN_USB_RPC_DEVICE_PROC_UDP_PKT, 
                _VN_USB_RPC_PROXY_SEND_UDP_PKT  */
typedef struct {
    uint32_t ipaddr;
    uint16_t port;
    uint16_t msglen;
    uint8_t msg[];
} _vn_usb_rpc_udp_pkt_t;

/* RPC arg for _VN_USB_RPC_PROXY_SEND_VN_PKT */
typedef struct {
    _VN_addr_t vnaddr;
    uint16_t msglen;
    uint8_t reserved[2];
    uint8_t msg[];
} _vn_usb_rpc_vn_pkt_t;

/* RPC arg for _VN_USB_RPC_PROXY_FIREWALL_OPEN, 
   return val for _VN_USB_RPC_PROXY_GET_HOST */
typedef struct {
    uint32_t   ipaddr;
    uint16_t   port;
    uint8_t    reserved[2];
} _vn_usb_rpc_ipport_t;

/* RPC arg for _VN_USB_RPC_PROXY_ADD_NET */
typedef struct {
    _VN_net_t  net_id;
} _vn_usb_rpc_add_net_arg_t;

/* RPC arg for _VN_USB_RPC_PROXY_DEL_NET */
typedef struct {
    _VN_net_t  net;
} _vn_usb_rpc_del_net_arg_t;

/* RPC arg for _VN_USB_RPC_PROXY_ADD_HOST */
typedef struct {
    _VN_addr_t vnaddr;
    _VN_guid_t guid;
    uint32_t   ipaddr;
    uint16_t   port;
    bool       keep_alive;
    uint8_t    reserved[1];
} _vn_usb_rpc_add_host_arg_t;

/* RPC arg for _VN_USB_RPC_PROXY_SET_HOST */
typedef struct {
    _VN_addr_t vnaddr;
    uint32_t   ipaddr;
    uint16_t   port;
    uint8_t    reserved[2];
} _vn_usb_rpc_set_host_arg_t;

/* RPC arg for _VN_USB_RPC_PROXY_GET_HOST */
typedef struct {
    _VN_addr_t vnaddr;
} _vn_usb_rpc_del_host_arg_t;

/* RPC arg for _VN_USB_RPC_PROXY_QUERY_HOST */
typedef struct {
    uint16_t port;
    uint16_t hostname_len;
    char hostname[];
} _vn_usb_rpc_query_host_arg_t;

/* RPC arg for _VN_USB_RPC_PROXY_DISCOVER_HOSTS */
typedef struct {
    bool flag;
} _vn_usb_rpc_discover_hosts_arg_t;

/* RPC arg for _VN_USB_RPC_COMMON_ADD_DEVICE */
typedef struct {
    _VN_guid_t guid;
    uint8_t buf[];    /* Binary buffer containing device details */
} _vn_usb_rpc_add_device_arg_t;

/* RPC arg for _VN_USB_RPC_COMMON_REMOVE_DEVICE */
typedef struct {
    _VN_guid_t guid;
    uint8_t    reason;
    uint8_t    reserved[3];
} _vn_usb_rpc_del_device_arg_t;

/* RPC arg for  _VN_USB_RPC_COMMON_ADD_SERVICE, 
                _VN_USB_RPC_COMMON_ADD_SERVICES */
typedef struct {
    _VN_guid_t guid;
    uint8_t buf[];    /* Binary buffer containing service details */
} _vn_usb_rpc_add_service_arg_t;

/* RPC arg for _VN_USB_RPC_COMMON_REMOVE_SERVICE */
typedef struct {
    _VN_guid_t guid;
    uint32_t   service_id;
} _vn_usb_rpc_del_service_arg_t;

/* RPC arg for _VN_USB_RPC_COMMON_REMOVE_SERVICES */
typedef struct {
    _VN_guid_t guid;
    uint32_t   nservices;
    uint32_t   service_ids[];
} _vn_usb_rpc_del_services_arg_t;

/* RPC arg for _VN_USB_RPC_COMMON_SET_DEVICE_TIMEOUT */
typedef struct {
    _VN_guid_t guid;
    uint32_t   msecs;
} _vn_usb_rpc_set_device_timeout_arg_t;

/* RPC arg for _VN_USB_RPC_PROXY_NETIF_INIT Request */
typedef struct {
    _vn_usb_rpc_ipportrange_t ipport; /* Requested IP/Port */
    _vn_usb_rpc_device_info_t device_info;   /* Device info */
} _vn_usb_rpc_netif_init_req_arg_t;

/* RPC arg for _VN_USB_RPC_PROXY_NETIF_INIT Response */
typedef struct {
    _vn_usb_rpc_ipport_t ipport; /* Local IP/Port */
    int32_t              trace_level; /* Trace level */
} _vn_usb_rpc_netif_init_resp_arg_t;
 
/* RPC calls */
int _vn_register_common_rpcs(_vn_usb_rpc_service_t service);

int _vn_rpc_call_add_device(_vn_usb_handle_t handle,
                            _vn_usb_rpc_service_t rpc_service,
                            uint32_t rpc_timeout,
                            _vn_device_info_t* device);
int _vn_rpc_call_remove_device(_vn_usb_handle_t handle,
                               _vn_usb_rpc_service_t rpc_service,
                               uint32_t rpc_timeout,
                               _VN_guid_t guid, uint8_t reason);
int _vn_rpc_call_add_service(_vn_usb_handle_t handle,
                             _vn_usb_rpc_service_t rpc_service,
                             uint32_t rpc_timeout,
                             _VN_guid_t guid,
                             _vn_service_t* service);
int _vn_rpc_call_remove_service(_vn_usb_handle_t handle,
                                _vn_usb_rpc_service_t rpc_service,
                                uint32_t rpc_timeout,
                                _VN_guid_t guid,
                                uint32_t service_id);
int _vn_rpc_call_set_device_timeout(_vn_usb_handle_t handle,
                                    _vn_usb_rpc_service_t rpc_service,
                                    uint32_t rpc_timeout,
                                    _VN_guid_t guid,                                    
                                    uint32_t msecs);


#ifdef  __cplusplus
}
#endif

#endif /* __VN_RPC_H__ */

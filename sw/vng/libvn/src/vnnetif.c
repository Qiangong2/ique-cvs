//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnlocal.h"

_vn_netif_t* _vn_netif_table[_VN_NETIF_MAX];

/* Functions for controlling access to the VN network interface */

/* Functions that operate on a specific instance of the VN network interface */

int _vn_netif_init_instance(_vn_netif_t *netif, _vn_inaddr_t addr,
                            _vn_inport_t min_port, _vn_inport_t max_port)
{
    int rv;
    assert(netif);
    assert((min_port == _VN_INPORT_INVALID) || (max_port >= min_port));

    _VN_TRACE(TRACE_FINE, _VN_SG_NETIF,
              "Using VN port from %u to %u\n", min_port, max_port);

    netif->reqip = addr;
    netif->localip = addr;
    netif->min_port = min_port;
    netif->max_port = max_port;
    netif->localport = min_port;
    netif->sockfd = _VN_SOCKET_INVALID;
#ifdef _VN_RPC_PROXY
    netif->recv_stats = NULL;
    netif->send_stats = NULL;
    _vn_guid_set_invalid(&netif->guid);
#else
    netif->recv_stats = _vn_get_recv_stats();
    netif->send_stats = _vn_get_send_stats();
#endif

    /* TODO: For client, do not allow resize, limit max number of entries */
    /* TODO: For server, start with larger table */
    rv = _vn_ht_init(&netif->net_table, _VN_NET_TABLE_SIZE, 0, true /* resize? */,
                     _vn_ht_hash_int, _vn_ht_key_comp_int);
    if (rv < 0) {
        memset(&netif->net_table, 0, sizeof(netif->net_table));
        return rv;
    }    

    while ((rv = _vn_netif_init_sockets(netif)) != 0) {
        if ((netif->localport == _VN_INPORT_INVALID) ||
            (netif->localport >= max_port)) {
            return rv;
        }
        else {
            /* Try next port */
            netif->localport++;
        }
    }

    return _VN_ERR_OK;
}

void _vn_netif_free_instance(_vn_netif_t* netif)
{
    if (netif) {
        _vn_netif_close_sockets(netif);
        _vn_ht_clear(&netif->net_table, (void*) _vn_net_conn_destroy, true);
        
        _vn_free(netif);
    }
}

void _vn_netif_clear_instance(_vn_netif_t *netif)
{
    assert(netif);
    _vn_ht_clear(&netif->net_table, (void*) _vn_net_conn_destroy, false);
}

/**** Functions for net_conn table (PC) - Part of the VN network interface ****/

/* Functions for managing the _vn_host_conn_t data structure */

void _vn_host_conn_init(_vn_host_conn_t *host_conn,
                        _VN_net_t net_id, _VN_host_t host_id, _VN_guid_t guid,
                        _vn_inaddr_t ip_addr, _vn_inport_t udp_port,
                        bool keep_alive)
{
    if (host_conn) {
        host_conn->addr = _VN_make_addr(net_id, host_id);
        host_conn->guid = guid;
        host_conn->ip_addr = ip_addr;
        host_conn->udp_port = udp_port;
        host_conn->sockfd   = _VN_SOCKET_INVALID;

        host_conn->keep_alive = keep_alive;
        host_conn->kalive_timer = NULL;
    }
}

_vn_host_conn_t* _vn_host_conn_create(_VN_net_t net_id, _VN_host_t host_id,
                                      _VN_guid_t guid,
                                      _vn_inaddr_t ip_addr,
                                      _vn_inport_t udp_port, bool keep_alive)
{
    /* TODO: Verify host_id valid */
    _vn_host_conn_t* host_conn = _vn_malloc(sizeof(_vn_host_conn_t));
    _vn_host_conn_init(host_conn, net_id, host_id, guid,
                       ip_addr, udp_port, keep_alive);
    return host_conn;
}

void _vn_host_conn_cleanup(_vn_host_conn_t* host_conn)
{
    if (host_conn) {
        _vn_firewall_destroy_timer(host_conn);
    }
}

void _vn_host_conn_destroy(_vn_host_conn_t* host_conn)
{
    if (host_conn) {
        _vn_host_conn_cleanup(host_conn);
        _vn_free(host_conn);
    }    
}

/* Functions for managing the _vn_net_conn_t data structure */

/* Adds host_conn to net using the given _vn_net_conn_t data structure
   This function adds the host_conn directly to the net without 
   checking if a host with same net_id/host_id exists */
int _vn_net_conn_add_host(_vn_net_conn_t *net_conn, 
                          _vn_host_conn_t *host_conn)
{
    assert(host_conn);
    if (net_conn) {
        return _vn_dlist_add(net_conn->peer_hosts, host_conn);
    }
    else return _VN_ERR_NETID;
}

_vn_net_conn_t* _vn_net_conn_create(_VN_net_t net_id)
{
    _vn_net_conn_t* net_conn = _vn_malloc(sizeof(_vn_net_conn_t));
    if (net_conn) {
        net_conn->net_id = net_id;
        net_conn->peer_hosts = _vn_dlist_create();
        if (net_conn->peer_hosts == NULL) {
            /* Oops, no memory */
            _vn_net_conn_destroy(net_conn);
            return NULL;
        }
    }
    return net_conn;
}

void _vn_net_conn_destroy(_vn_net_conn_t* net_conn)
{
    if (net_conn) {
        if (net_conn->peer_hosts) {
            _vn_dlist_destroy(net_conn->peer_hosts, 
                              (void*) &_vn_host_conn_destroy);
        }
        _vn_free(net_conn);
    }    
}

/* Lookup functions */

/* Returns the _vn_net_conn_t with the given net_id */
_vn_net_conn_t* _vn_netif_lookup_net(_vn_netif_t *netif,
                                     _VN_net_t net_id)
{
    assert(netif);
    return _vn_ht_lookup(&netif->net_table, (_vn_ht_key_t) net_id);
}

/**
 * Returns the _vn_host_conn_t with the given host_id, 
 * from the given _vn_net_conn_t
 */
_vn_host_conn_t* _vn_net_conn_lookup_host(_vn_net_conn_t *net_conn, 
                                          _VN_host_t host_id)
{
    if (net_conn) {        
        _VN_addr_t addr = _VN_make_addr(net_conn->net_id, host_id);
        _vn_dnode_t *node;
        /* Now look through the rest of my peer */
        _vn_dlist_for(net_conn->peer_hosts, node)
        {
            if (_vn_dnode_value(node)) {
                _vn_host_conn_t *host_conn = (_vn_host_conn_t*) 
                    _vn_dnode_value(node);
                if (host_conn->addr == addr) {
                    return host_conn;
                }
            }
        }
    }
    return NULL;
}

/* Returns the _vn_host_conn_t with the given net_id and host_id */
_vn_host_conn_t* _vn_netif_lookup_host(_vn_netif_t *netif,
                                       _VN_net_t net_id, _VN_host_t host_id)
{
    _vn_net_conn_t* net_conn = _vn_netif_lookup_net(netif, net_id);
    return _vn_net_conn_lookup_host(net_conn, host_id);
}


/* Functions for adding and removing nets from netif table */

/* Create a new net_conn object and adds it to the table
   Assumes net not already in table */
_vn_net_conn_t* _vn_netif_add_new_net(_vn_netif_t *netif, _VN_net_t net_id)
{
    _vn_net_conn_t* net_conn;

    _VN_TRACE(TRACE_FINER, _VN_SG_NETIF, "_vn_add_new_net_conn: "
              "Adding new entry net 0x%08x\n", net_id);
    net_conn = _vn_net_conn_create(net_id);

    if (net_conn == NULL) return NULL;

    if (_vn_ht_add(&netif->net_table, (_vn_ht_key_t) net_id, net_conn) < 0) {
        /* Failed to add entry */
        _vn_net_conn_destroy(net_conn);
        return NULL;
    }

    return net_conn;
}

/** 
 * Creates and add net to table 
 * Return values:
 * - _VN_ERR_NETID  if net already in table
 * - _VN_ERR_NOMEM  if out of memory
 */
int _vn_netif_add_net(_vn_netif_t *netif, _VN_net_t net_id)
{
    /* See if this net already in table */
    _vn_net_conn_t *net_conn;

    net_conn = _vn_netif_lookup_net(netif, net_id);
    if (net_conn != NULL) { 
        return _VN_ERR_NETID; 
    }

    net_conn = _vn_netif_add_new_net(netif, net_id);
    if (net_conn != NULL) {
        return _VN_ERR_OK;
    }
    else {
        return _VN_ERR_NOMEM;
    }
}
    
/**
 * Deletes all host mappings for a net
 * Return values:
 * - _VN_ERR_NETID  if specified net not found
 */
int _vn_netif_delete_net(_vn_netif_t *netif, 
                         _VN_net_t net_id)
{
    /* Delete the entry for the net */
    _vn_net_conn_t *net_conn;

    assert(netif);

    net_conn = _vn_ht_remove(&netif->net_table, (_vn_ht_key_t) net_id);
    if (net_conn) {
        _vn_net_conn_destroy(net_conn);
        return _VN_ERR_OK;        
    }
    else return _VN_ERR_NETID;
}

/* Functions for adding and removing hosts from netif table */

/* Adds host_conn to net with the given the net_id
   This functions adds the host_conn directly to the net without 
   checking if a host with same net_id/host_id exists */
int _vn_netif_add_host_conn(_vn_netif_t *netif,
                            _VN_net_t net_id, _vn_host_conn_t *host_conn)
{
    _vn_net_conn_t *net_conn;
    assert(host_conn);
    net_conn = _vn_netif_lookup_net(netif, net_id);
    return _vn_net_conn_add_host(net_conn, host_conn);
}

/** 
 * Creates and add host mapping to table (add net if needed)
 * Return values:
 * - _VN_ERR_HOSTID if there is already a host with the give host_id 
 * - _VN_ERR_NOMEM  if out of memory
 */
int _vn_netif_add_host(_vn_netif_t *netif,
                       _VN_net_t net_id, _VN_host_t host_id, _VN_guid_t guid,
                       _vn_inaddr_t ip_addr, _vn_inport_t udp_port,
                       bool keep_alive)
{
    _vn_net_conn_t *net_conn;
    _vn_host_conn_t *host_conn;

    net_conn = _vn_netif_lookup_net(netif, net_id);
    if (net_conn == NULL) {
        net_conn = _vn_netif_add_new_net(netif, net_id);
        if (net_conn == NULL) {
            return _VN_ERR_NOMEM;
        }
    }

    host_conn = _vn_net_conn_lookup_host(net_conn, host_id);
    if (host_conn == NULL) {
        host_conn = _vn_host_conn_create(net_conn->net_id, host_id, guid,
                                         ip_addr, udp_port, keep_alive);
        if (host_conn) {
            return _vn_net_conn_add_host(net_conn, host_conn);
        }
        else return _VN_ERR_NOMEM;
    }
    else return _VN_ERR_HOSTID;
}

/**
 * Creates and add host mapping to table 
 * Return values:
 * - _VN_ERR_NETID  if specified net not found
 * - _VN_ERR_HOSTID if there is already a host with the give host_id 
 * - _VN_ERR_NOMEM  if out of memory
 */
int _vn_netif_delete_host(_vn_netif_t *netif,
                          _VN_net_t net_id, _VN_host_t host_id)
{
    _vn_net_conn_t *net_conn = _vn_netif_lookup_net(netif, net_id);
    if (net_conn) {
        _VN_addr_t addr = _VN_make_addr(net_conn->net_id, host_id);
        _vn_dnode_t *node = NULL, *temp;
        _vn_host_conn_t *host_conn = NULL;
        if (net_conn->peer_hosts) {
            _vn_dlist_delete_for(net_conn->peer_hosts, node, temp)
            {
                host_conn = (_vn_host_conn_t*) _vn_dnode_value(node);
                if (host_conn && (host_conn->addr == addr)) 
                {
                    _vn_dlist_delete(net_conn->peer_hosts, node);
                    _vn_host_conn_destroy(host_conn);
                    return _VN_ERR_OK;
                }
            }
        }
        return _VN_ERR_HOSTID;
    }
    else return _VN_ERR_NETID;
}

/**
 * Retrieves the host mapping for the specifed net and host
 */
int _vn_netif_get_host_mapping(_vn_netif_t *netif,
                               _VN_net_t net_id, _VN_host_t host_id, 
                               _vn_inaddr_t *addr, _vn_inport_t *port)
{
    _vn_host_conn_t *host_conn;
    _vn_net_conn_t *net_conn;
    net_conn = _vn_netif_lookup_net(netif, net_id);
    if (net_conn) {
        host_conn = _vn_net_conn_lookup_host(net_conn, host_id);
        if (host_conn != NULL) {
            if (addr) *addr = host_conn->ip_addr;
            if (port) *port = host_conn->udp_port;
            return _VN_ERR_OK;
        }
        else {
            return _VN_ERR_HOSTID;
        }
    }
    else {
        return _VN_ERR_NETID;
    }
}

/**
 * Sets the host mapping for the specified net and host
 */
int _vn_netif_set_host_mapping(_vn_netif_t *netif, 
                               _VN_net_t net_id, _VN_host_t host_id, 
                               _vn_inaddr_t addr, _vn_inport_t port)
{
    _vn_host_conn_t *host_conn;
    _vn_net_conn_t *net_conn;
    net_conn = _vn_netif_lookup_net(netif, net_id);
    if (net_conn) {
        host_conn = _vn_net_conn_lookup_host(net_conn, host_id);
        if (host_conn) {
            if ((host_conn->ip_addr != addr) || 
                (host_conn->udp_port != port)) {
                char old_addrstr[_VN_INET_ADDRSTRLEN];
                char new_addrstr[_VN_INET_ADDRSTRLEN];
                _vn_inet_ntop(&(host_conn->ip_addr), 
                              old_addrstr, sizeof(old_addrstr));
                _vn_inet_ntop(&(addr), new_addrstr, sizeof(new_addrstr));
                _VN_TRACE(TRACE_FINE, _VN_SG_NETIF,
                          "Updating host mapping for net 0x%08x, "
                          "host %u from %s:%u to %s:%u\n", net_id, host_id, 
                          old_addrstr, host_conn->udp_port, new_addrstr, port);
                host_conn->ip_addr = addr;
                host_conn->udp_port = port;
            }
            return _VN_ERR_OK;    
        }
        else {
            return _VN_ERR_HOSTID;
        }
    }
    else {
        return _VN_ERR_NETID;
    }
}

/** Main access functions for managing nets of host mappings **/

/* Functions that operate on the entire VN network interface module */

/**
 * Initializes the interface to the network
 * Assumes mutex already initialized.
 */
#ifdef _VN_RPC_PROXY
int _vn_netif_init()
{
    memset(_vn_netif_table, 0, sizeof(_vn_netif_table));
    return _VN_ERR_OK;
}
#else
int _vn_netif_init(_vn_inaddr_t addr,
                   _vn_inport_t min_port, _vn_inport_t max_port)
{
    int rv;
    memset(_vn_netif_table, 0, sizeof(_vn_netif_table));

    rv = _vn_netif_create(addr, min_port, max_port);
    if (rv < 0) {
        return rv;
    }

    return _VN_ERR_OK;
}
#endif


int _vn_netif_reset()
{
    int i;
    _vn_net_lock();
    for (i = 0; i < _VN_NETIF_MAX; i++) {
        if (_vn_netif_table[i]) {
            _vn_netif_clear_instance(_vn_netif_table[i]);
        }
    }
    _vn_net_unlock();
    return _VN_ERR_OK;
}

int _vn_netif_cleanup()
{
    int i;
    for (i = 0; i < _VN_NETIF_MAX; i++) {
        _vn_netif_destroy(i);
    }

    return _VN_ERR_OK;
}

int _vn_netif_create_at(int handle, _vn_inaddr_t addr, 
                        _vn_inport_t min_port, _vn_inport_t max_port)
{
    int rv;
    _vn_net_lock();
    if ((handle >= 0) && (handle < _VN_NETIF_MAX)) {
        if (_vn_netif_table[handle] == NULL) {
            _vn_netif_t *netif = _vn_malloc(sizeof(_vn_netif_t));
            if (netif != NULL) {
                rv = _vn_netif_init_instance(netif, addr,
                                             min_port, max_port);
                if (rv >= 0) {
                    _vn_netif_table[handle] = netif;
                    netif->handle = handle;
                    rv = handle;
                }
                else {
                    _vn_netif_free_instance(netif);
                }
            }
            else {
                rv = _VN_ERR_NOMEM;
            }
        }
        else {
            rv = _VN_ERR_DUPENTRY;
        }
    }
    else {
        rv = _VN_ERR_INVALID;
    }
    _vn_net_unlock();
    return rv;
}

int _vn_netif_create(_vn_inaddr_t addr,
                     _vn_inport_t min_port, _vn_inport_t max_port) 
{
    /* Look for empty spot */
    int i, rv = _VN_ERR_FULL;
    _vn_net_lock();
    for (i = 0; i < _VN_NETIF_MAX; i++) {
        if (_vn_netif_table[i] == NULL) {
            _vn_netif_t *netif = _vn_malloc(sizeof(_vn_netif_t));
            if (netif != NULL) {
                rv = _vn_netif_init_instance(netif, addr, min_port, max_port);
                if (rv >= 0) {
                    _vn_netif_table[i] = netif;
                    netif->handle = i;
                    rv = i;
                }
                else {
                    _vn_netif_free_instance(netif);
                }
            }
            else {
                rv = _VN_ERR_NOMEM;
            }
            break;                
        }
    }   
    _vn_net_unlock();
    return rv;
}

int _vn_netif_destroy(int handle)
{
    _vn_netif_t *netif = NULL;
    int rv;

    _vn_net_lock();
    if ((handle >= 0) && (handle < _VN_NETIF_MAX)) {
        netif = _vn_netif_table[handle];
    }

    if (netif) {
        _vn_netif_table[handle] = NULL;
        _vn_netif_free_instance(netif);

        rv = _VN_ERR_OK;
    }
    else {
        rv = _VN_ERR_NOTFOUND;
    }
    _vn_net_unlock();
    return rv;
}

#ifdef _VN_RPC_PROXY
_vn_netif_t* _vn_netif_get_instance(int handle)
{
    if ((handle >= 0) && (handle < _VN_NETIF_MAX)) {
        return _vn_netif_table[handle];
    }
    else return NULL;
}

#else

_vn_netif_t* _vn_netif_get_instance()
{
    return _vn_netif_table[0];
}

/** 
 * Creates and add net to table 
 * Return values:
 * - _VN_ERR_NETID  if net already in table
 * - _VN_ERR_NOMEM  if out of memory
 */
int _vn_add_host_mapping_net(_VN_net_t net_id)
{
    return _vn_netif_add_net(_vn_netif_get_instance(), net_id);
}
    
/**
 * Deletes all host mappings for a net
 * Return values:
 * - _VN_ERR_NETID  if specified net not found
 */
int _vn_delete_host_mapping_net(_VN_net_t net_id)
{
    return _vn_netif_delete_net(_vn_netif_get_instance(), net_id);
}

/** Main access functions for managing host mappings **/

/**
 * Retrieves the host mapping for the specifed net and host
 */
int _vn_get_host_mapping(_VN_net_t net_id, _VN_host_t host_id, 
                         _vn_inaddr_t *addr, _vn_inport_t *port)
{
    return _vn_netif_get_host_mapping(_vn_netif_get_instance(),
                                      net_id, host_id,
                                      addr, port);
}

/**
 * Sets the host mapping for the specified net and host
 */
int _vn_set_host_mapping(_VN_net_t net_id, _VN_host_t host_id, 
                         _vn_inaddr_t addr, _vn_inport_t port)
{
    return _vn_netif_set_host_mapping(_vn_netif_get_instance(),
                                      net_id, host_id,
                                      addr, port);
}

/** 
 * Creates and add host mapping to table (add new net if needed)
 * Return values:
 * - _VN_ERR_HOSTID if there is already a host with the give host_id 
 * - _VN_ERR_NOMEM  if out of memory
 */
int _vn_add_host_mapping(_VN_net_t net_id, _VN_host_t host_id, _VN_guid_t guid,
                         _vn_inaddr_t ip_addr, _vn_inport_t udp_port,
                         bool keep_alive)
{
    return _vn_netif_add_host(_vn_netif_get_instance(), net_id, host_id,
                              guid, ip_addr, udp_port, keep_alive);
}

/**
 * Creates and add host mapping to table 
 * Return values:
 * - _VN_ERR_NETID  if specified net not found
 * - _VN_ERR_HOSTID if there is already a host with the give host_id 
 * - _VN_ERR_NOMEM  if out of memory
 */
int _vn_delete_host_mapping(_VN_net_t net_id, _VN_host_t host_id)
{
    return _vn_netif_delete_host(_vn_netif_get_instance(), net_id, host_id);
}

#endif

/* Function calls for host discovery */
int _vn_netif_query_host(_vn_netif_t *netif,
                         const char* hostname, _vn_inport_t port)
{
#ifdef UPNP
    assert(netif);
    return _vn_upnp_query_host(netif->handle, hostname, port);
#else
    return _VN_ERR_NOSUPPORT;
#endif
}

int _vn_netif_clear_query_list(_vn_netif_t *netif)
{
#ifdef UPNP
    assert(netif);
    return _vn_upnp_clear_query_list(netif->handle);
#else
    return _VN_ERR_NOSUPPORT;
#endif
}

int _vn_netif_discover_hosts(bool flag)
{
#ifdef UPNP
    if (flag) {
        return _vn_upnp_start_host_discovery();
    }
    else {
        return _vn_upnp_stop_host_discovery();
    }
#else
    return _VN_ERR_NOSUPPORT;
#endif
}

//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//
#include "vnlocal.h"

/* VN Init handshake */
#define _VN_HS_HEADER_SIZE          sizeof(_vn_hs_msg_header_t) 
#define _VN_HS_MSG_INIT_SIZE        sizeof(_vn_hs_msg_init_t)
#define _VN_HS_MSG_INIT_ACK_SIZE    sizeof(_vn_hs_msg_init_ack_t)
#define _VN_HS_MSG_AUTH_SIZE        sizeof(_vn_hs_msg_auth_t)
#define _VN_HS_MSG_AUTH_ACK_SIZE    sizeof(_vn_hs_msg_auth_ack_t)

#define _VN_HS_RETX_TIMER_MIN          2*1000
#define _VN_HS_RETX_TIMER_MAX         60*1000
#define _VN_HS_RETX_TIMER_TIMEOUT   1*60*1000

/* TODO: For server make into a real hash table */
/* TODO: Have the incoming handshake table be a hash table of 
         GUID <-> List of handshake sessions  */

/* Handshakes for which I'm acting as the server */
_vn_dlist_t _vn_hs_table_incoming;
/* Handshakes initiated by me */
_vn_dlist_t _vn_hs_table_outgoing;

uint32_t _vn_hs_retx_timeout = _VN_HS_RETX_TIMER_TIMEOUT;

void _vn_hs_set_timeout(uint32_t timeout)
{
    _vn_hs_retx_timeout = timeout;
}

uint32_t _vn_hs_get_timeout()
{
    return _vn_hs_retx_timeout;
}

/* Are we interested in new handshakes for default net? */
bool _vn_hs_listen_default = false;
_VN_callid_t _vn_hs_listen_default_call = _VN_MSG_ID_INVALID;

_VN_callid_t _vn_hs_get_default_listen_call_id()
{
    return _vn_hs_listen_default_call;
}

/* Look up handshake status with client based on client ID and net id */
_vn_hs_info_t* _vn_hs_lookup_state_by_net(_vn_dlist_t* hs_table,
                                          _VN_guid_t client_id, 
                                          _VN_net_t net_id)
{
    _vn_dnode_t* node;
    assert(hs_table);
    _vn_dlist_for(hs_table, node)
    {
       _vn_hs_info_t* hs_info = 
           (_vn_hs_info_t*) _vn_dnode_value(node);
       if (hs_info) {
           if (hs_info->net_id == net_id) {
               if (_vn_guid_match(&hs_info->ID_c, &client_id)) {
                   return hs_info;
               }
           }
       }
    }
    return NULL;
}

/* Look up handshake status with client based on client ID and session id */
_vn_hs_info_t* _vn_hs_lookup_state_by_session(_vn_dlist_t* hs_table,
                                              _VN_guid_t client_id,
                                              uint32_t session_id)
{
    _vn_dnode_t* node;
    assert(hs_table);
    _VN_TRACE(TRACE_FINEST, _VN_SG_HS, 
              "Looking up HS session for guid 0x%08x %u, session %u\n",
              client_id.device_type, client_id.chip_id, session_id);
    _vn_dlist_for(hs_table, node)
    {
       _vn_hs_info_t* hs_info = 
           (_vn_hs_info_t*) _vn_dnode_value(node);
       if (hs_info) {
           if (hs_info->session_id == session_id) {
               if (_vn_guid_match(&hs_info->ID_c, &client_id)) {
                   _VN_TRACE(TRACE_FINEST, _VN_SG_HS, 
                             "Found HS session for guid 0x%08x %u, session %u\n",
                             hs_info->ID_c.device_type, hs_info->ID_c.chip_id, session_id);
                   return hs_info;
               }
           }
       }
    }
    return NULL;
}

/* Returns the appropriate handshake table to use depending on whether
   I'm the initiator or not */
_vn_dlist_t* _vn_hs_get_table(_VN_guid_t guid, bool initiator)
{
    if (initiator) {
        /* I'm the initiator, look in outgoing table */
        return &_vn_hs_table_outgoing;
    }
    else {
        /* I'm not the initiator, look in incoming table */
        return &_vn_hs_table_incoming;
    }
}

/*
 * Converts direction of received message to if I'm the initiator or not 
 * 
 * If direction is _VN_HS_MSG_SC_DIR:
 * - Message is from server, I must have initiated this connection
 * Otherwise, direction is _VN_HS_MSG_CS_DIR:
 * - Message is from the initiator (i.e. client) who is attempting
 *   to connect to me.
 */
bool _vn_hs_dir2initiator(uint8_t direction)
{
    /* Did I initiate this handshake? */
    bool initiator = (direction == _VN_HS_MSG_SC_DIR);
    return initiator;
}

/*
 * Lookup handshake state based on if I'm the initiator in addition to
 * client_id and session_id, this is mainly for supporting handshaking
 * on the same device
 */
_vn_hs_info_t* _vn_hs_get_state(_VN_guid_t client_id, uint32_t session_id, 
                                bool initiator)
{
    _vn_dlist_t* hs_table;
    hs_table = _vn_hs_get_table(client_id, initiator);

    if (hs_table == NULL) return NULL;
    return _vn_hs_lookup_state_by_session(hs_table, client_id, session_id);
}

/* Returns the corresponding state for the _VN_NET_DEFAULT request for guid */
_vn_hs_info_t* _vn_hs_get_default_state(_VN_guid_t client_id)
{    
    _vn_dlist_t* hs_table;
    hs_table = _vn_hs_get_table(client_id, _vn_guid_is_self(&client_id));

    if (hs_table == NULL) return NULL;

    return _vn_hs_lookup_state_by_net(hs_table, client_id, _VN_NET_DEFAULT);
}

int _vn_hs_lookup_connect_info(_VN_guid_t guid, _VN_net_t net_id, _VN_addr_t* addr,
                               _vn_inaddr_t* ip, _vn_inport_t* port)
{
    _vn_hs_info_t* hs_info;
    _vn_hs_vninfo_t* vninfo;

    hs_info = _vn_hs_get_default_state(guid);

    if (hs_info == NULL) {
        if (addr) { *addr = _VN_ADDR_INVALID; }
        if (ip) { *ip = _VN_INADDR_INVALID; }
        if (port) { *port = _VN_INPORT_INVALID; }
        
        return _VN_ERR_NOTFOUND;
    }
    if (hs_info->state != _VN_ST_ESTABLISHED && hs_info->state != _VN_ST_AUTH) 
    {
        if (addr) { *addr = _VN_ADDR_INVALID; }
        if (ip) { *ip = _VN_INADDR_INVALID; }
        if (port) { *port = _VN_INPORT_INVALID; }

        return _VN_ERR_PENDING;
    }
    
    /* guid corresponds to client of handshake */
    vninfo = &(hs_info->vninfo);
    if (addr) {
        if (vninfo->net_id) {
            *addr = _VN_make_addr(vninfo->net_id, vninfo->host_c);
        }
        else {
            *addr = _VN_ADDR_INVALID;
        }
    }

    if (ip) {
        *ip = vninfo->addr_c;
    }

    if (port) {
        *port = vninfo->port_c;
    }

    return _VN_ERR_OK;
}

void _vn_hs_change_state(_vn_hs_info_t* hs_info, uint8_t new_state)
{
    assert(hs_info);
    hs_info->state = new_state;
}

void _vn_hs_init_state(_vn_hs_info_t* hs_info, _VN_guid_t ID_c,
                       uint32_t session_id, uint16_t seq_no, bool initiator)
{
    if (hs_info) {
        hs_info->state = _VN_ST_CLOSED;
        hs_info->net_id = _VN_NET_DEFAULT;
        hs_info->rand_c = 0;
        hs_info->rand_s = 0;
        hs_info->ID_c = ID_c;
        if (initiator) {
            _vn_guid_set_invalid(&hs_info->ID_s);
        } else {
            hs_info->ID_s = _vn_get_myguid();
        }
        hs_info->session_id = session_id;
        hs_info->seq_no = seq_no;
        hs_info->hs_addr = _VN_INADDR_INVALID;
        hs_info->hs_port = _VN_INPORT_INVALID;
        hs_info->vn_addr_c = _VN_INADDR_INVALID;
        hs_info->vn_port_c = _VN_INPORT_INVALID;
        hs_info->my_hs_addr = _VN_INADDR_INVALID;
        hs_info->my_hs_port = _VN_INPORT_INVALID;
        hs_info->last_pkt = NULL;
        hs_info->initiator = initiator; /* Client = initiator, Server responds */
        hs_info->abort_reason = 0;
        hs_info->error = _VN_ERR_OK;
        hs_info->request_data = NULL;
        memset(&hs_info->pkts, 0, sizeof(hs_info->pkts));
        if (!initiator) {
            uint32_t timeout =  _vn_hs_get_timeout();
            _vn_timer_init(&hs_info->timer, timeout, hs_info,
                           _vn_hs_server_timer_cb);
        }
        else {
            uint32_t timeout =  _vn_hs_get_timeout();
            _vn_timer_init(&hs_info->timer, timeout, hs_info,
                           _vn_hs_client_timer_cb);
        }
        memset(&hs_info->vninfo, 0, sizeof(hs_info->vninfo));
#if _VN_USE_IOSC
        hs_info->Yc = _VN_KEY_HANDLE_INVALID;
        hs_info->Ys = _VN_KEY_HANDLE_INVALID;
        hs_info->DH_shared_handle = _VN_KEY_HANDLE_INVALID;
        hs_info->cert_key_c = _VN_KEY_HANDLE_INVALID;
        hs_info->cert_key_s = _VN_KEY_HANDLE_INVALID;
        hs_info->vninfo.key.Ak = _VN_KEY_HANDLE_INVALID;
        hs_info->vninfo.key.Hk = _VN_KEY_HANDLE_INVALID;
#endif        
    }
}

void _vn_hs_clear_vninfo(_vn_hs_vninfo_t* vninfo, bool freeKey)
{
#if _VN_USE_IOSC
    if (freeKey && (vninfo->flags & _VN_HS_FLAG_KEY_KEEP) == 0) {
        /* Free VN key contexts */
        if (_VN_KEY_HANDLE_IS_VALID(vninfo->key.Ak)) {
            _VN_TRACE(TRACE_FINE, _VN_SG_HS, "Removing Ak %d\n", vninfo->key.Ak);
            IOSC_DeleteObject(vninfo->key.Ak);
        }

        if (_VN_KEY_HANDLE_IS_VALID(vninfo->key.Hk)) {
            _VN_TRACE(TRACE_FINE, _VN_SG_HS, "Removing Hk %d\n", vninfo->key.Hk);
            IOSC_DeleteObject(vninfo->key.Hk);
        }
    }

    memset(vninfo, 0, sizeof(_vn_hs_vninfo_t));

    vninfo->key.Ak = _VN_KEY_HANDLE_INVALID;
    vninfo->key.Hk = _VN_KEY_HANDLE_INVALID;
#else
    memset(vninfo, 0, sizeof(_vn_hs_vninfo_t));
#endif
}

void _vn_hs_cleanup_state(_vn_hs_info_t* hs_info)
{
    /* Cleans up resources used by hs_info */
    if (hs_info) {
        int i;
        for (i = 0; i < _VN_HS_MSG_COUNT; i++) {
            if (hs_info->pkts[i]) {
                _vn_free_msg_buffer(hs_info->pkts[i]);
                hs_info->pkts[i] = NULL;
            }
        }
        hs_info->last_pkt = NULL;
        _vn_timer_cancel(&hs_info->timer);

        /* Free key contexts */
        _vn_hs_delete_keys(hs_info);

        if (hs_info->state != _VN_ST_ESTABLISHED) {
            hs_info->state = _VN_ST_CLOSED;

            _vn_hs_clear_vninfo(&hs_info->vninfo, true);
        }

        if (hs_info->request_data) {
            _vn_free_msg_buffer(hs_info->request_data);
            hs_info->request_data = NULL;
        }
    }
}

void _vn_hs_destroy_state(_vn_hs_info_t* hs_info)
{
    if (hs_info) {
        _vn_hs_clear_vninfo(&hs_info->vninfo, true);
        _vn_hs_cleanup_state(hs_info);
        _vn_free(hs_info);
    }
}

int _vn_hs_add_state(_vn_hs_info_t* hs_info)
{
    _vn_dlist_t* hs_table;
    assert(hs_info);
    hs_table = _vn_hs_get_table(hs_info->ID_c, hs_info->initiator);
    assert(hs_table);
    if (hs_info->ID_c.device_type == _VN_DEVICE_TYPE_UNKNOWN) {
        _VN_TRACE(TRACE_WARN, _VN_SG_HS,
                  "Adding HS entry for ID_c %u with unknown device type\n",
                  hs_info->ID_c.chip_id);
    }
    return _vn_dlist_add(hs_table, hs_info);
}

void _vn_hs_remove_state(_vn_hs_info_t* hs_info)
{
    _vn_dnode_t* node, *temp;
    _vn_dlist_t* hs_table;
    assert(hs_info);
    hs_table = _vn_hs_get_table(hs_info->ID_c, hs_info->initiator);
    if (hs_table) {
        _vn_dlist_delete_for(hs_table, node, temp) {
            if (hs_info == _vn_dnode_value(node)) {
                _vn_dlist_delete(hs_table, node);
                break;
            }
        }
    }
    _vn_hs_destroy_state(hs_info);
}

int _vn_hs_terminate_by_net(_VN_net_t net_id)
{
    /* Terminate all outstanding handshakes corresponding to the net_id
       that I've accepted */
    _vn_dnode_t* node, *temp;
    _vn_dlist_t* hs_table = &_vn_hs_table_incoming;
    _vn_dlist_delete_for(hs_table, node, temp)
    {
        _vn_hs_info_t* hs_info = 
            (_vn_hs_info_t*) _vn_dnode_value(node);
        if (hs_info) {
            if (hs_info->net_id == net_id && !hs_info->initiator) {
                if (hs_info->state != _VN_ST_ESTABLISHED) {
                    /* Let peer know that we are no longer accepting connections */
                    _vn_hs_abort(hs_info, _VN_HS_ERR_CLIENT_REJECT, 
                                 _VN_ERR_INACTIVE, NULL, 0);
#if 0 /* Should the entry be allowed to time out? */
                    /* Remove entry from table */
                    _vn_dlist_delete(&hs_table, node);
                    _vn_hs_destroy_state(hs_info);
#endif
                }
            }
        }
    }
    return _VN_ERR_OK;
}

_vn_hs_info_t* _vn_hs_create_state(_VN_guid_t ID_c,
                                   uint32_t session_id, uint16_t seq_no,
                                   bool initiator)
{
    _vn_hs_info_t* hs_info = _vn_malloc(sizeof(_vn_hs_info_t));
    _vn_hs_init_state(hs_info, ID_c, session_id, seq_no, initiator);
    return hs_info;
}

int _vn_hs_listen_net(_VN_callid_t call_id, _VN_net_t net_id, bool listen)
{
    if (net_id == _VN_NET_DEFAULT) {
        _vn_hs_listen_default = listen;
        _vn_hs_listen_default_call = call_id;
    }
    else {
        _vn_net_info_t* net;
        net = _vn_lookup_net_info(net_id);
        if (net == NULL) return _VN_ERR_NETID;
        if (!_VN_NET_IS_OWNER(net)) return _VN_ERR_NOT_OWNER;
        if (listen) {
            net->flags |= _VN_PROP_ACCEPT;
        }
        else {
            net->flags &= ~_VN_PROP_ACCEPT;
        }
        net->listen_call = call_id;
    }
    /* If turning off listen, also need to terminate outstanding HS requests */
    if (!listen) {
        _vn_hs_terminate_by_net(net_id);
    }
    else {
        char localhostname[255];
        char localaddr[_VN_INET_ADDRSTRLEN];
        _vn_inaddr_t ipaddr;
        _vn_inport_t port;
        ipaddr = _vn_netif_getlocalhost(_vn_netif_get_instance(),
                                        localhostname, sizeof(localhostname));
        port = _vn_netif_getlocalport(_vn_netif_get_instance());
        _vn_inet_ntop(&(ipaddr), localaddr, sizeof(localaddr));
        _VN_TRACE(TRACE_INFO, _VN_SG_API,
                  "Listening on %s(%s):%u for net 0x%08x\n",
                  localhostname, localaddr, port, net_id);
    }
    
    return _VN_ERR_OK;
}

int _vn_hs_init(bool server)
{
    _vn_dlist_init(&_vn_hs_table_outgoing);
    _vn_dlist_init(&_vn_hs_table_incoming);

    if (server) {
        _vn_hs_listen_default = true;
    }
    else {
        _vn_hs_listen_default = false;
    }
    return _VN_ERR_OK;
}

int _vn_hs_reset()
{
    _vn_dlist_clear(&_vn_hs_table_outgoing, (void*) &_vn_hs_destroy_state);
    _vn_dlist_clear(&_vn_hs_table_incoming, (void*) &_vn_hs_destroy_state);
    return _VN_ERR_OK;
}

int _vn_hs_cleanup()
{
    _vn_hs_reset();
    return _VN_ERR_OK;
}

int _vn_hs_client_do_handshake(_vn_hs_info_t* hs_info)
{
    int rv;

    assert(hs_info);

    /* start handshake */
    /* Send INIT packet - will also trigger timer */
    rv = _vn_hs_send_init(hs_info);
    return rv;
}

int _vn_hs_established(_vn_hs_info_t* hs_info)
{
    _vn_hs_vninfo_t* vninfo = &hs_info->vninfo;
    _vn_inaddr_t addr_c, addr_s;
    _vn_inport_t port_c, port_s;
    _VN_addr_t vnaddr;
    int rv;
    
    assert(hs_info);
    assert(hs_info->state == _VN_ST_ESTABLISHED);

    /* Currently, nothing to do unless you initiated the connection */
    if (!hs_info->initiator) {
        return _VN_ERR_OK;
    }

    if (vninfo->flags & _VN_HS_FLAG_COMMITED) {
        /* Oh, we've already created the net and added the peer */
        return _VN_ERR_OK;
    }

    addr_c = vninfo->addr_c? vninfo->addr_c: hs_info->my_hs_addr;
    port_c = vninfo->port_c? vninfo->port_c: hs_info->my_hs_port;
    addr_s = vninfo->addr_s? vninfo->addr_s: hs_info->hs_addr;
    port_s = vninfo->port_s? vninfo->port_s: hs_info->hs_port;
    vnaddr = _VN_make_addr(vninfo->net_id, vninfo->host_c);

    _VN_TRACE(TRACE_FINE, _VN_SG_HS,
              "HS established between 0x%08x %u and 0x%08x %u for net 0x%08x\n",
              hs_info->ID_c.device_type, hs_info->ID_c.chip_id,
              hs_info->ID_s.device_type, hs_info->ID_s.chip_id, hs_info->net_id);

    if (_vn_guid_eq(&hs_info->ID_c, &hs_info->ID_s)) {
        /* Ooh, I was talking to myself */
        /* No need to create net then */

        /* Make sure my VN ports are properly activated */
        rv = _vn_activate_default_ports(vnaddr);
        vninfo->flags |= _VN_HS_FLAG_COMMITED;
    }
    else if (hs_info->net_id == _VN_NET_DEFAULT) {
        /* Okay, we requested default net */
        
        /* Only set the default net id if we don't have one yet */
        if (_vn_get_default_net_id() == _VN_NET_DEFAULT) {
            _vn_set_default_net_id(vninfo->net_id);
        }

        /* Check if we already have this net */ 
        _VN_TRACE(TRACE_FINER, _VN_SG_HS,
                  "Check if net 0x%08x already exists with host %d\n", 
                  vninfo->net_id, vninfo->host_s);
 
        rv = _vn_check_net(vninfo->net_id, vninfo->host_s, vninfo->key);
        if (rv >= 0) {

            _VN_TRACE(TRACE_FINER, _VN_SG_HS,
                      "Adding peer %d to net 0x%08x\n",
                      vninfo->host_c, vninfo->net_id);

            /* Okay, that's our net - add ourselves to it */
            rv = _vn_add_peer(hs_info->ID_c, vninfo->net_id, vninfo->host_c, 
                              0 /* clock delta */, addr_c, port_c,
                              _VN_PROP_LOCAL);
            
            _vn_activate_default_ports(vnaddr);
            vninfo->flags |= _VN_HS_FLAG_COMMITED;

            /* Don't need vninfo->key anymore */
        }
        else if (rv == _VN_ERR_NETID) {

            _VN_TRACE(TRACE_FINER, _VN_SG_HS,
                      "Adding new net 0x%08x\n", vninfo->net_id);

            /* Don't have this net yet - add it */
            rv = _vn_add_default_net(vninfo->net_id, false /* i'm not master */,
                                     vninfo->host_c, hs_info->ID_s,
                                     vninfo->host_s, 0,
                                     /* Client IP and port (me) */
                                     addr_c, port_c,
                                     /* Server IP and port */
                                     addr_s, port_s,
                                     vninfo->key, 0 /* clock delta */ );
            vninfo->flags |= _VN_HS_FLAG_COMMITED;
            /* vninfo key passed to net_info, does not need to be deleted */
            vninfo->flags |= _VN_HS_FLAG_KEY_KEEP;
        }
        else { 
            /* We already have different net of the same net id - 
               this vninfo is bogus */
            rv = _VN_ERR_NETID; 
        }
    }
    else {
        /* Same net as we requested? */
        if (vninfo->net_id == hs_info->net_id) {
            /* Check if we already have this net */
            rv = _vn_check_net(vninfo->net_id, vninfo->host_s, vninfo->key);
            if (rv >= 0) {
                /* Okay, that's our net - add ourselves to it */
                rv = _vn_add_peer(hs_info->ID_c, 
                                  vninfo->net_id, vninfo->host_c, 
                                  0 /* clock delta */, addr_c, port_c,
                                  _VN_PROP_LOCAL);
                
                _vn_activate_default_ports(vnaddr);
                vninfo->flags |= _VN_HS_FLAG_COMMITED;

                /* Don't need vninfo->key anymore */
            }
            else if (rv == _VN_ERR_NETID) {
                /* Don't have this net yet - add it */
                rv = _vn_add_normal_net(vninfo->net_id, false /* i'm not master */,
                                        vninfo->host_c, hs_info->ID_s,
                                        vninfo->host_s, 0,
                                        /* Client IP and port (me) */
                                        addr_c, port_c,
                                        /* Server IP and port */
                                        addr_s, port_s,
                                        vninfo->key, 0 /* clock delta */);
                vninfo->flags |= _VN_HS_FLAG_COMMITED;
                /* vninfo key passed to net_info, does not need to be deleted */
                vninfo->flags |= _VN_HS_FLAG_KEY_KEEP;
            }
            else { 
                /* We already have different net of the same net id - 
                   this vninfo is bogus */
                rv = _VN_ERR_NETID; 
            }
        }
        else {
            /* What happened here?  What to do? */
            rv = _VN_ERR_NETID;
        }
    }

    /* Ask server for permission to fully join the net */
    if (rv >= 0) {
        const void* msg = NULL;
        uint16_t msglen = 0;

        _VN_TRACE(TRACE_FINER, _VN_SG_HS,
                  "Asking server for permission to join net 0x%08x\n",
                  hs_info->net_id);

        if (hs_info->request_data) {
            msg = hs_info->request_data->buf;
            msglen = hs_info->request_data->len;
        }

        /* Do clock calibration with host */
        _vn_calibrate_clock(vninfo->net_id, vninfo->host_s);

        rv = _vn_send_join_net_request(hs_info->session_id,
                                       vninfo->net_id, 
                                       vninfo->host_c, vninfo->host_s,
                                       msg, msglen);

        /* Retrigger timer so that this entry will get cleaned up even
           if we never hear back from server */
        _vn_timer_mod(&hs_info->timer, _vn_hs_get_timeout(), false);
        _vn_timer_retrigger(&hs_info->timer, true);
    }

    _VN_TRACE(TRACE_FINER, _VN_SG_HS,
              "HS established for net 0x%08x finished with rv %d\n",
              hs_info->net_id, rv);

    if (rv >= 0) {
        /* Everything good, nothing to do */
    }
    else {
        /* Oops: Error while creating net */
        _vn_hs_abort(hs_info, _VN_HS_ERR_SERVER_REJECT, rv, NULL, 0);
        _vn_hs_proc_state(hs_info);
    }
    
    return rv;
}

int _vn_hs_reject(uint32_t session_id, int code, _vn_hs_vninfo_t* vninfo,
                  const void* msg, uint16_t msglen)
{
    /* Connection rejected, remove peer and handshake */
    int rv;
    _vn_net_info_t* net_info;
    _vn_host_info_t* host_info;
    
    assert(vninfo);
    /* Mark peer as leaving, delete default net if needed */
    net_info = _vn_lookup_net_info(vninfo->net_id);
    if (net_info) {
        /* Make sure I'm the owner */
        if (_VN_NET_IS_OWNER(net_info)) {
            /* Send reject message */
            rv = _vn_send_join_net_response(session_id, vninfo->net_id, 
                                            vninfo->host_s, vninfo->host_c,
                                            code, msg, msglen);

            host_info = _vn_lookup_host_info(net_info, vninfo->host_c);

            /* Do not mark localhost as leaving, they should only
               be deleted after the localhost response is processed */
            if (host_info && !_VN_HOST_IS_LOCAL(host_info)) {
                int nhosts = _vn_get_size(net_info);
                _vn_host_mark_leaving(net_info, host_info);
                    
                if (_VN_NET_IS_DEFAULT(net_info) && (nhosts <= 2)) {
                    if (!_vn_isserver()) {
                        assert(_VN_NETID_IS_LOCAL(net_info->net_id));
                    }
                        
                    /* This was a default net, it should be deleted when
                       only the owner is left */
                    _vn_post_localhosts_leave_net(_VN_MSG_ID_INVALID,
                                                  net_info->net_id);
                    /* Net will not be deleted until reject message
                       is sent */
                    _vn_net_mark_delete(net_info);
                    
                }
            }
        }
        else {
            rv = _VN_ERR_NOT_OWNER;
        }
    }
    else {
        rv = _VN_ERR_NETID;
    }

    return rv;
}

/* Updates device entry with information from vninfo */
int _vn_hs_device_update_client(_vn_hs_info_t* hs_info)
{
    _vn_device_info_t* device;
    _VN_addr_t vnaddr;
    _VN_guid_t guid;
    _vn_hs_vninfo_t* vninfo;
    int rv;
            
    assert(hs_info);
    assert(!hs_info->initiator);

    /* Add new device to table */
    guid = hs_info->ID_c;
    rv = _vn_add_device(guid, 0);
    if ((rv < 0) && (rv != _VN_ERR_DUPENTRY)) {
        return rv;
    }
    device = _vn_lookup_device(guid);

    assert(device != NULL);
    vninfo = &hs_info->vninfo;

    /* Add IP address to device */
    _vn_device_add_connect_info(device, _VN_INADDR_EXTERNAL,
                                hs_info->hs_addr, hs_info->hs_port);
    _vn_device_add_connect_info(device, _VN_INADDR_LOCAL,
                                hs_info->vn_addr_c, hs_info->vn_port_c);

    /* TODO: If UPNP, try mixure of external IP, local port */
    _vn_device_add_connect_info(device, _VN_INADDR_EXTERNAL,
                                hs_info->hs_addr, hs_info->vn_port_c);

    vnaddr = _VN_make_addr(vninfo->net_id, vninfo->host_c);
    if (hs_info->net_id == _VN_NET_DEFAULT) {
        /* TODO: What if there are multiple default VNs to this guid? */
        device->vnaddr = vnaddr;
    }

    _vn_device_add_vnaddr(guid, vnaddr);

    /* TODO: When do we delete this device entry? */
    return _VN_ERR_OK;
}

/* Find the handshake session associated with the request_id */
_vn_hs_info_t* _vn_hs_lookup_request(uint64_t request_id)
{
    _VN_guid_t guid;
    uint32_t session_id;
    _vn_hs_info_t* hs_info;

    /* Look up the handshake session associated with the request_id */
    guid.chip_id = _VN_HS_REQID_GET_CHIP_ID(request_id);
    guid.device_type = _VN_DEVICE_TYPE_UNKNOWN;
    session_id = _VN_HS_REQID_GET_SESSION_ID(request_id);

    _VN_TRACE(TRACE_FINE, _VN_SG_API,
              "_vn_hs_lookup_request: looking up request for "
              "(guid 0x%08x %u, session %u) ",
              guid.device_type, guid.chip_id, session_id);

    hs_info = _vn_hs_get_state(guid, session_id, false /* not initiator */);

    /* Make sure handshake in established phase */
    if (hs_info && hs_info->state == _VN_ST_ESTABLISHED) {
        /* Not in established phase - error */
        return hs_info;
    } else {
        return NULL;
    }
}


/* Upper layer is accepting or rejecting this new host */
int _vn_hs_accept(uint64_t request_id, bool flag, 
                  const void* msg, uint16_t msglen)
{
    _VN_guid_t guid;
    uint32_t session_id;
    _vn_hs_info_t* hs_info;
    _vn_hs_vninfo_t* vninfo;
    int rv;
    
    /* Look up which handshake is being accepted or rejected */
    guid.chip_id = _VN_HS_REQID_GET_CHIP_ID(request_id);
    guid.device_type = _VN_DEVICE_TYPE_UNKNOWN;
    session_id = _VN_HS_REQID_GET_SESSION_ID(request_id);

    _VN_TRACE(TRACE_FINE, _VN_SG_API,
              "_vn_hs_accept: request (guid 0x%08x %u, session %u), "
              "accepted %u\n", guid.device_type, guid.chip_id, session_id, flag);

    hs_info = _vn_hs_get_state(guid, session_id, false /* not initiator */);
    if (hs_info == NULL) {
        _VN_TRACE(TRACE_WARN, _VN_SG_API,
                  "_vn_hs_accept cannot find state for "
                  "(guid 0x%08x %u, session %u)\n",
                  guid.device_type, guid.chip_id, session_id); 
        return _VN_ERR_NOTFOUND;
    }

    guid.device_type = hs_info->ID_c.device_type;

    /* Make sure handshake in established phase */
    if (hs_info->state != _VN_ST_ESTABLISHED) {
        _VN_TRACE(TRACE_WARN, _VN_SG_API,
                  "_vn_hs_accept: state not established for "
                  "(guid 0x%08x %u, session %u)\n",
                  guid.device_type, guid.chip_id, session_id); 
        return _VN_ERR_FAIL;
    }

    vninfo = &hs_info->vninfo;

    /* TODO: Update HS state appropriately */
    if (flag) {
        rv = _vn_accept_host(session_id, vninfo->net_id,
                             vninfo->host_c, vninfo->host_s);

        if (rv < 0) {
            _VN_TRACE(TRACE_WARN, _VN_SG_API,
                      "_vn_hs_accept: _vn_accept_host failed %d "
                      " for (guid 0x%08x %u, session %u)\n",
                      rv, guid.device_type, guid.chip_id, session_id);
            return rv;
        }

        rv = _vn_send_join_net_response(session_id, vninfo->net_id, 
                                        vninfo->host_s, vninfo->host_c,
                                        _VN_ERR_OK, msg, msglen);

        /* Update device entry for this client guid */
        _vn_hs_device_update_client(hs_info);
    }
    else {
        rv = _vn_hs_reject(session_id, _VN_ERR_REJECTED, vninfo, msg, msglen);
    }

    /* Clean up entry */
    _vn_hs_remove_state(hs_info);

    return rv;
}

int _vn_hs_client_new_session(uint32_t session_id, _VN_net_t net_id,
                              const void* msg, uint16_t msglen,
                              _vn_hs_info_t** pSession)
{
    _vn_hs_info_t* hs_info;
    _VN_guid_t guid;
    int rv;

    /* NOTE: session id should be different every time */
    guid = _vn_get_myguid();
    hs_info = _vn_hs_get_state(guid, session_id, true);
    if (hs_info == NULL) {
        hs_info = _vn_hs_create_state(guid, session_id, 0, true);
        if (hs_info == NULL) {
            if (pSession) (*pSession = NULL);
            return _VN_ERR_NOMEM;
        }
    }
    else {
        /* Hmm, already have an entry for this handshake */
        if (pSession) (*pSession = hs_info);
        return _VN_ERR_DUPENTRY;
    }

    assert(hs_info != NULL);
    assert(hs_info->state == _VN_ST_CLOSED);

    if (msg && msglen) {
        hs_info->request_data = _vn_get_msg_buffer(msglen);
        if (hs_info->request_data == NULL) {
            _vn_hs_destroy_state(hs_info);
            if (pSession) (*pSession = NULL);
            return _VN_ERR_NOMEM;
        }
        
        memcpy(hs_info->request_data->buf, msg, msglen);
        hs_info->request_data->len = msglen;
    }

    rv = _vn_hs_add_state(hs_info);
    if (rv >= 0) {
        hs_info->net_id = net_id;
        
        /* Start timer in case we never do anything this with handshake */
        _vn_timer_mod(&hs_info->timer, _vn_hs_get_timeout(), false);
        _vn_timer_retrigger(&hs_info->timer, true);
        
        if (pSession) (*pSession = hs_info);
    }
    else {
        /* Failed to add state, free memory */
        _vn_hs_destroy_state(hs_info);
        if (pSession) (*pSession = NULL);
    }
    return rv;
}

int _vn_hs_client_setup(_VN_guid_t guid_s,
                        uint32_t session_id, _VN_net_t net_id,
                        _vn_inaddr_t hs_addr_s, _vn_inport_t hs_port_s,
                        _vn_inaddr_t vn_addr_c, _vn_inport_t vn_port_c,
                        bool new_session,
                        const void* msg, uint16_t msglen)
{
    _vn_hs_info_t* hs_info;
    int rv;

    if (new_session) {
        /* Starting a new session */
        rv = _vn_hs_client_new_session(session_id, net_id,
                                       msg, msglen, &hs_info);
        if (rv < 0) {
            return rv;
        }
    }
    else {
        /* Use an existing session that was previously set up */
        _VN_guid_t guid;
        guid = _vn_get_myguid();
        hs_info = _vn_hs_get_state(guid, session_id, true);
        if (hs_info == NULL) {
            return _VN_ERR_NOTFOUND;
        }
    }

    assert(hs_info != NULL);
    assert(hs_info->state == _VN_ST_CLOSED);
    assert(hs_info->net_id == net_id);

    _vn_hs_change_state(hs_info, _VN_ST_INIT);

    hs_info->ID_s = guid_s;
    hs_info->net_id = net_id;
    hs_info->hs_addr = hs_addr_s;
    hs_info->hs_port = hs_port_s;    
    hs_info->my_hs_addr = _vn_netif_getlocalip(_vn_netif_get_instance());
    hs_info->my_hs_port = _vn_netif_getlocalport(_vn_netif_get_instance());

    /* If connecting to self and the IP/port for the VN was not specified,
       then use our local IP/port (configured IP or first non-loopback) */
    if ((vn_addr_c == _VN_INADDR_INVALID) &&
        ((hs_addr_s == hs_info->my_hs_addr) 
         || (hs_addr_s == htonl(INADDR_LOOPBACK))))
    {
        hs_info->vn_addr_c = hs_info->my_hs_addr; 
        hs_info->vn_port_c = hs_info->my_hs_port;
    }
    else {
        hs_info->vn_addr_c = vn_addr_c;
        hs_info->vn_port_c = vn_port_c;
    }

    hs_info->seq_no++;

    /* Do actual handshake to get VN information and set up default VN */
    rv = _vn_hs_client_do_handshake(hs_info);

    if (rv < 0) {
        hs_info->error = rv;
        _vn_hs_change_state(hs_info, _VN_ST_ERROR);
        _vn_hs_proc_state(hs_info);
    }

    return rv;
}

int _vn_hs_connect(_VN_guid_t guid_s, bool new_session,
                   uint32_t session_id, uint32_t IPaddr,
                   uint16_t port, _VN_net_t net_id, 
                   const void* msg, uint16_t msglen)
{
    return _vn_hs_client_setup(guid_s, session_id, net_id, IPaddr, port,
                               _vn_netif_getlocalip(_vn_netif_get_instance()), 
                               _vn_netif_getlocalport(_vn_netif_get_instance()),
                               new_session, msg, msglen);
}

int _vn_hs_connect_list(_VN_guid_t guid_s,
                        bool new_session, uint32_t session_id, 
                        _vn_dlist_t* list, _VN_net_t net_id, 
                        const void* msg, uint16_t msglen)
{
    int rv = _VN_ERR_OK;;
    if (list) {
        if (_vn_dlist_size(list) == 1) {
            _vn_sockaddr_t* sockaddr;
            sockaddr = _vn_dlist_peek_front(list);
            if (sockaddr) {
                rv = _vn_hs_connect(guid_s, new_session, session_id,
                                    sockaddr->addr, sockaddr->port,
                                    net_id, msg, msglen);
            }
        }
        else {
            rv = _vn_hs_connect(guid_s, new_session, session_id, 
                                _VN_INADDR_INVALID, _VN_INPORT_INVALID,
                                net_id, msg, msglen);
        }
    }
    return rv;
}

int _vn_hs_parse_auth_ack(_vn_hs_info_t* hs_info,
                          const uint8_t* msg_data, size_t msg_len)
{
    _vn_hs_msg_auth_ack_head_t* msg_head;
    _vn_hs_msg_auth_ack_tail_t* msg_tail;
    _VN_addr_t vnaddr_c;
    _vn_hs_vninfo_t* vninfo;
    size_t expected_key_len;
    const uint8_t* keys;
    int rv = _VN_ERR_OK;

    /* Auth Ack message format: | Header | Keys (optional) | Trailer (Ys, sig_s) | */

    assert(hs_info);
    vninfo = &(hs_info->vninfo);
    assert(vninfo);
    assert(msg_data);
    if (msg_len < _VN_HS_MSG_AUTH_ACK_SIZE) {
        return _VN_ERR_TOO_SHORT;
    }

    msg_head = (_vn_hs_msg_auth_ack_head_t*) msg_data;
    /* VN secret keys should be right after the auth ack header */
    keys = msg_data + sizeof(_vn_hs_msg_auth_ack_head_t);
    if (msg_head->key) {
        expected_key_len = _VN_ENC_KEY_LENGTH;
    }
    else {
        expected_key_len = 0;
    }

    if (msg_len < _VN_HS_MSG_AUTH_ACK_SIZE + expected_key_len) {
        return _VN_ERR_TOO_SHORT;
    }
    msg_tail = (_vn_hs_msg_auth_ack_tail_t*) (keys + expected_key_len);

#if _VN_USE_IOSC    
    /* Import Ys */
    if (!_vn_hs_verify_cert(msg_tail->cert_Ys,
                            sizeof(_vn_hs_cert_s_t),
                            hs_info->cert_key_s,
                            &hs_info->Ys)) {
        return _VN_ERR_AUTH;
    }
#endif
    

    /* Verify msg->sig_s */
    if (_vn_hs_verify_server_sig(msg_tail->sig_s,
                                 hs_info->Ys,
                                 hs_info, msg_data, 
                                 msg_len - sizeof(msg_tail->sig_s))) {
        vnaddr_c = ntohl(msg_head->vnaddr_c);
        vninfo->net_id = _VN_addr2net(vnaddr_c);
        vninfo->host_c = _VN_addr2host(vnaddr_c);
        vninfo->host_s = ntohs(msg_head->host_s);
        vninfo->addr_s = msg_head->addr_s;  /* IP stored in network order */
        vninfo->port_s = ntohs(msg_head->port_s);

        /*  Calculate common key from msg->Ys, our private key,
            and rand_c | rand_s */
        rv = _vn_hs_compute_shared_key(&hs_info->DH_shared_handle,
                                       hs_info->Yc,
                                       hs_info->Ys);
        
        if (rv < 0) {
            return rv;
        }

        /* See if key attached */
        if (expected_key_len > 0) {
            /* Use msg auth header as iv */
            uint8_t* iv = (uint8_t*) msg_data;
            /* Figure out vn key */
            rv = _vn_hs_import_vnkey(keys, expected_key_len, &vninfo->key,
                                     hs_info->DH_shared_handle,
                                     hs_info->rand_c, hs_info->rand_s, iv);
        } else {
            rv = _vn_hs_compute_vnkey(&vninfo->key, 
                                      hs_info->DH_shared_handle,
                                      hs_info->rand_c, hs_info->rand_s);

        }
        return rv;
    }
    else return _VN_ERR_AUTH;
}

int _vn_hs_proc_new_init(_vn_hs_info_t** phs_info, _VN_guid_t ID_c,
                         _vn_inaddr_t hs_addr_c, _vn_inport_t hs_port_c,
                         uint32_t session_id, uint16_t seq_no,
                         const uint8_t* msg_data, size_t msg_len)
{
    _vn_hs_verkey_c_t cert_key_c;
    _vn_hs_msg_init_t* init_msg;
    _vn_hs_info_t* hs_info;
    int rv = _VN_ERR_OK;
    _VN_net_t net_id = _VN_NET_DEFAULT;
    uint8_t abort_reason = _VN_HS_ERR_CLIENT_BAD_INIT;

#if _VN_USE_IOSC
    cert_key_c = _VN_KEY_HANDLE_INVALID;
#endif

    assert(msg_data);
    assert(phs_info);
    
    if (msg_len < _VN_HS_MSG_INIT_SIZE) {
        return _VN_ERR_TOO_SHORT;
    }

    hs_info = *phs_info;
    init_msg = (_vn_hs_msg_init_t*) msg_data;
    if (init_msg->version_c != _VN_HS_VERSION) {
        rv = _VN_ERR_BAD_VERSION;
    }
    else {
        /* Check if I'm accepting connection requests for this net */
        net_id = ntohl(init_msg->net_id);
        if (net_id == _VN_NET_DEFAULT) {
            if (!_vn_hs_listen_default) {
                rv = _VN_ERR_INACTIVE;
            }
        }
        else {
            /* Look up this net, verify that I'm the owner 
               and accepting init requests */
            _vn_net_info_t* net = _vn_lookup_net_info(net_id);
            if (net) {
                if (!_VN_NET_IS_OWNER(net)) {
                    rv = _VN_ERR_NOT_OWNER;
                }
                else if (!_VN_NET_IS_ACCEPTING(net)) {
                    rv = _VN_ERR_INACTIVE;
                }
            }
            else {
                rv = _VN_ERR_NETID;
            }
        }
        if (rv < 0) {
            /* Not accepting this init */
            _VN_TRACE(TRACE_WARN, _VN_SG_HS,
                      "Rejecting new init message from client 0x%08x %u, "
                      " session %u, seq %u, net 0x%08x: error %d\n",
                      ID_c.device_type, ID_c.chip_id, 
                      session_id, seq_no, net_id, rv);
            /* TODO: Do we want to send client more informative error codes? */
            abort_reason = _VN_HS_ERR_CLIENT_REJECT;
        }
    }


    if (rv >= 0) {
        /* TODO: Validate client ID and public Diffie Helman Key */
        /* TODO: lookup exp_Yc from ID_c and compare against Yc in init_msg->cert_c */
        /* Verify certificate */
        uint32_t cert_size;
        cert_size = msg_len - _VN_HS_MSG_INIT_SIZE;
        if (!_vn_hs_verify_client_cert(
                ID_c, &cert_key_c, init_msg->cert_c, cert_size)) {
            rv = _VN_ERR_AUTH;
            abort_reason = _VN_HS_ERR_CLIENT_BAD_INIT;
        }
    }
    
    if (rv < 0) {
        /* Init message not good - abort handshake */
        _vn_hs_info_t tmp_hsinfo;
        if (hs_info == NULL) {
            hs_info = &tmp_hsinfo;
            _vn_hs_init_state(&tmp_hsinfo, ID_c, session_id, seq_no, false);

            tmp_hsinfo.hs_addr = hs_addr_c;
            tmp_hsinfo.hs_port = hs_port_c;
        }
        _vn_hs_abort(hs_info, abort_reason, rv, NULL, 0);
        goto out;
    }

    if (hs_info == NULL) {
        hs_info = _vn_hs_create_state(ID_c, session_id, seq_no, false);
        if (hs_info == NULL) {
            rv = _VN_ERR_NOMEM;
            goto out;
        }

        /* Add entry for ID_c, session_id to table */
        rv = _vn_hs_add_state(hs_info);
        if (rv < 0) {
            /* Failed to add state, free memory */
            _vn_hs_destroy_state(hs_info);
            goto out;
        }
        *phs_info = hs_info;
    }
    else {
        /* Restart handshake */
        _vn_hs_cleanup_state(hs_info);

        hs_info->session_id = session_id;
        hs_info->seq_no = seq_no;
    }

#if _VN_USE_IOSC
    /* Remember client cert key */
    hs_info->cert_key_c = cert_key_c;
    cert_key_c = _VN_KEY_HANDLE_INVALID;
#endif
        
    hs_info->net_id = net_id;
    hs_info->hs_addr = hs_addr_c;
    hs_info->hs_port = hs_port_c;

    hs_info->vn_addr_c = init_msg->addr_c; /* IP stored in network order */
    hs_info->vn_port_c = ntohs(init_msg->port_c);
    hs_info->my_hs_addr = init_msg->addr_s; /* IP stored in network order */
    hs_info->my_hs_port = ntohs(init_msg->port_s);

    hs_info->rand_c = ntohl(init_msg->rand_c);

    rv = _vn_hs_server_proc_init(hs_info, msg_data, msg_len);

out:
#if _VN_USE_IOSC
    if (_VN_KEY_HANDLE_IS_VALID(cert_key_c)) {
        /* Need to clean up key */
        IOSC_DeleteObject(cert_key_c);
    }
#endif
    return rv;
}

/* Process duplicate request to the server */
int _vn_hs_server_proc_dupreq(_vn_hs_info_t* hs_info, _vn_buf_t* new_pkt, 
                              uint8_t msg_type)
{
    _vn_buf_t* old_pkt;
    bool retx = false;

    /* Verify this message is same as original */
    assert(hs_info);
    assert(!hs_info->initiator);
    assert(new_pkt);

    /* Do we need to retransmit in response to this request? */
    switch (msg_type) {
    case _VN_HS_MSG_INIT:
        retx = (hs_info->state == _VN_ST_INIT);
        break;
    case _VN_HS_MSG_AUTH:
        retx = (hs_info->state == _VN_ST_AUTH);
        break;
    default:
        retx = false;
        break;
    }

    /* Server - only expecting client requests */
    if (!retx)
    {
        _VN_TRACE(TRACE_INFO, _VN_SG_HS,
                  "Ignoring duplicate HS message %d, session %u, "
                  "seq %u, state %d\n", msg_type, hs_info->session_id,
                  hs_info->seq_no, hs_info->state);
        return _VN_ERR_DUPPKT;
    }
 
    old_pkt = hs_info->pkts[msg_type-1];
    assert(old_pkt);
    assert(hs_info->last_pkt);
    assert(hs_info->last_pkt == hs_info->pkts[msg_type]);

    if ((new_pkt->len == old_pkt->len) &&
        (memcmp(new_pkt->buf, old_pkt->buf, new_pkt->len) == 0))
    {
        /* Resend response */
        /* Retrigger timer */
        _vn_timer_cancel(&hs_info->timer);
        _vn_hs_resend(hs_info);
        _vn_timer_retrigger(&hs_info->timer, true);
        return _VN_ERR_DUPPKT;
    }
    else {
        /* No match */
        /* TODO: Send abort with error? */
        return _VN_ERR_FAIL;
    }
}

/* Called only if you are a server and accepting incoming connections */
int _vn_hs_server_proc_init(_vn_hs_info_t* hs_info, const uint8_t* msg_data,
                            size_t msg_len)
{
    int rv;
    assert(hs_info);
    switch (hs_info->state) {
    case _VN_ST_CLOSED:
        /* At this point, the init message has already been validated 
           (see _vn_proc_new_init) */
        /* Send init ack message - also triggers inactivity timer */
        rv = _vn_hs_send_init_ack(hs_info);
        if (rv >= 0) {
            _vn_hs_change_state(hs_info, _VN_ST_INIT);
        }
        else {
            /* Error sending init ack message, send abort instead. */
            _vn_hs_abort(hs_info, _VN_HS_ERR_CLIENT_REJECT, rv, NULL, 0);
        }
        break;
    case _VN_ST_INIT:
        /* TODO: Remove - unused? */
        /* Resend init_ack message */
        /* Retrigger timer */
        _vn_timer_cancel(&hs_info->timer);
        _vn_hs_resend(hs_info);
        _vn_timer_retrigger(&hs_info->timer, true);
        rv = _VN_ERR_DUPPKT;
        break;
    default:
        /* Ignore unexpected init message */
        rv = _VN_ERR_DUPPKT;
        break;
    }
    return rv;
}

/* Called only if you are a server and accepting incoming connections */
int _vn_hs_server_proc_auth(_vn_hs_info_t* hs_info, const uint8_t* msg_data,
                            size_t msg_len)
{
    _vn_hs_msg_auth_t* auth_msg;
    int rv;

    if (msg_len < _VN_HS_MSG_AUTH_SIZE) {
        return _VN_ERR_TOO_SHORT;
    }

    assert(hs_info);
    switch (hs_info->state) {
    case _VN_ST_INIT:
        /* Authenticate client */
        auth_msg = (_vn_hs_msg_auth_t*) msg_data;
        /* Check auth_msg->sig_c by using client's public key
           (TODO: lookup from database)
           to decrypt pkt and match against expected hash */
#if _VN_USE_IOSC    
        /* Import Yc */
        if (!_vn_hs_verify_cert(auth_msg->cert_Yc,
                                sizeof(_vn_hs_cert_c_t),
                                hs_info->cert_key_c,
                                &hs_info->Yc)) {
            rv = _VN_ERR_AUTH;
            _vn_hs_abort(hs_info, _VN_HS_ERR_CLIENT_BAD_AUTH, rv, NULL, 0);
            break;
        }
#endif

        if (_vn_hs_verify_client_sig(auth_msg->sig_c, 
                                     hs_info->Yc,
                                     hs_info,
                                     msg_data, 
                                     msg_len - sizeof(_vn_hs_sign_c_t))) 
        {
            _vn_timer_cancel(&hs_info->timer);
            /* Send AUTH_ACK - also triggers inactivity timer */
            rv = _vn_hs_send_auth_ack(hs_info);
            if (rv >= 0) {
                _vn_hs_change_state(hs_info, _VN_ST_AUTH);
            }
            else {                
                /* Error sending AUTH_ACK, send abort instead. */
                /* Clear hs_info->vninfo (no longer valid) */
                _vn_hs_clear_vninfo(&hs_info->vninfo, true);
                _vn_hs_abort(hs_info, _VN_HS_ERR_CLIENT_REJECT, rv, NULL, 0);
            }
        }
        else {
            rv = _VN_ERR_AUTH;
            _vn_hs_abort(hs_info, _VN_HS_ERR_CLIENT_BAD_AUTH, rv, NULL, 0);
        }
        break;
    case _VN_ST_AUTH:
        /* TODO: Remove - unused? */
        /* Resend auth_ack message */
        /* Retrigger timer */
        _vn_timer_cancel(&hs_info->timer);
        _vn_hs_resend(hs_info);
        _vn_timer_retrigger(&hs_info->timer, true);
        rv = _VN_ERR_DUPPKT;
        break;
    default:
        /* Ignore duplicate/invalid auth message */
        rv = _VN_ERR_DUPPKT;
        break;
    }
    return rv;
}

int _vn_hs_client_proc_init_ack(_vn_hs_info_t* hs_info,
                                const uint8_t* msg_data, size_t msg_len)
{
    _vn_hs_msg_init_ack_t* init_ack_msg;
    int rv;
    uint32_t cert_size;

    assert(hs_info);

    if (msg_len < _VN_HS_MSG_INIT_ACK_SIZE) {
        return _VN_ERR_TOO_SHORT;
    }

    cert_size = msg_len - _VN_HS_MSG_INIT_ACK_SIZE;

    switch (hs_info->state) {
    case _VN_ST_INIT:
        init_ack_msg = (_vn_hs_msg_init_ack_t*) msg_data;
        /* Cancel timer for retransmitting INIT packet */
        _vn_timer_cancel(&hs_info->timer);
        /* Verify certificate */
        hs_info->ID_s.device_type = ntohl(init_ack_msg->ID_s.device_type);
        hs_info->ID_s.chip_id = ntohl(init_ack_msg->ID_s.chip_id);
#if _VN_USE_IOSC
        if (_vn_hs_verify_server_cert(hs_info->ID_s, &hs_info->cert_key_s, 
                                      init_ack_msg->cert_s, cert_size)) {
#else
        if (_vn_hs_verify_server_cert(hs_info->ID_s,
                                      NULL, init_ack_msg->cert_s, cert_size)) {
#endif
            hs_info->rand_s = ntohl(init_ack_msg->rand_s);
            _vn_hs_change_state(hs_info, _VN_ST_AUTH);
            /* Send AUTH packet - will also retrigger timer */
            hs_info->seq_no++;
            rv = _vn_hs_send_auth(hs_info);
            if (rv < 0) {
                /* Error sending ACK, send abort instead. */
                _vn_hs_abort(hs_info, _VN_HS_ERR_SERVER_REJECT, rv, NULL, 0);
            }
        }
        else {
            rv = _VN_ERR_AUTH;
            _vn_hs_abort(hs_info, _VN_HS_ERR_SERVER_BAD_CERT, rv, NULL, 0);
        }
        break;
    default:
        /* Ignore duplicate/invalid init_ack message */
        rv = _VN_ERR_DUPPKT;
        break;
    }
    return rv;
}

int _vn_hs_client_proc_auth_ack(_vn_hs_info_t* hs_info,
                                const uint8_t* msg_data, size_t msg_len)
{
    int rv;
    assert(hs_info);

    if (msg_len < _VN_HS_MSG_AUTH_ACK_SIZE) {
        return _VN_ERR_TOO_SHORT;
    }

    switch (hs_info->state) {
    case _VN_ST_AUTH:
        /* Cancel timer for retransmitting AUTH packet */
        _vn_timer_cancel(&hs_info->timer);
        /* Parse packet for VN settings */
        rv = _vn_hs_parse_auth_ack(hs_info, msg_data, msg_len);
        if (rv >= 0) {
            _vn_hs_change_state(hs_info, _VN_ST_ESTABLISHED);
        }
        else {
            _vn_hs_abort(hs_info, _VN_HS_ERR_SERVER_BAD_AUTH, rv, NULL, 0);
        }
        break;
    default:
        /* Ignore unexpected auth_ack message */
        rv = _VN_ERR_DUPPKT;
        break;
    }
    return rv;
}

/* Called by both server and client */
int _vn_hs_proc_abort(_vn_hs_info_t* hs_info, const uint8_t* msg_data,
                      size_t msg_len)
{
    assert(hs_info);
    assert(msg_data);
    assert(msg_len >= 1);

    /* Got abort message */
    if (hs_info->state == _VN_ST_ESTABLISHED) {
        _VN_TRACE(TRACE_INFO, _VN_SG_HS,
                  "Cannot abort completed HS for session %u",
                  hs_info->session_id);        
    }
    else {
        _VN_TRACE(TRACE_INFO, _VN_SG_HS,
                  "Aborting HS for session %u, seq %u\n",
                  hs_info->session_id, hs_info->seq_no);
        _vn_hs_change_state(hs_info, _VN_ST_ERROR);
        hs_info->abort_reason = msg_data[0];
        hs_info->error = (int8_t) msg_data[1];
    }
    return _VN_ERR_OK;
}

int _vn_hs_client_proc_msg(_vn_hs_info_t* hs_info, uint8_t msg_type,
                           const uint8_t* msg_data, size_t msg_len)
{
    int rv;
    assert(hs_info);
    assert(hs_info->initiator);
    assert(msg_data);
    assert(msg_len);

    /* Client - only expecting server responses */
    switch (msg_type) {
    case _VN_HS_MSG_ABORT:
        rv = _vn_hs_proc_abort(hs_info, msg_data, msg_len);
        break;
    case _VN_HS_MSG_INIT_ACK:
        rv = _vn_hs_client_proc_init_ack(hs_info, msg_data, msg_len);
        break;
    case _VN_HS_MSG_AUTH_ACK:
        rv =_vn_hs_client_proc_auth_ack(hs_info, msg_data, msg_len);
        break;
    case _VN_HS_MSG_INIT:
    case _VN_HS_MSG_AUTH:
        /* Unexpected message type */
        _VN_TRACE(TRACE_WARN, _VN_SG_HS,
                  "Unexpected HS request %d, session %u, seq %u\n", 
                  msg_type, hs_info->session_id, hs_info->seq_no);
        /* Ignore */
        rv = _VN_ERR_FAIL;
        break;
    default:
        /* Invalid message type */
        _VN_TRACE(TRACE_WARN, _VN_SG_HS,
                  "Invalid HS message %d, session %u, seq %u\n", 
                  msg_type, hs_info->session_id, hs_info->seq_no);
        /* Ignore? */
        rv = _VN_ERR_INVALID;
        break;
    }
    return rv;
}

int _vn_hs_server_proc_msg(_vn_hs_info_t* hs_info, uint8_t msg_type,
                           const uint8_t* msg_data, size_t msg_len)
{
    int rv;
    assert(hs_info);
    assert(!hs_info->initiator);
    assert(msg_data);
    assert(msg_len);

    /* Server - only expecting client requests */
    switch (msg_type) {
    case _VN_HS_MSG_ABORT:
        rv = _vn_hs_proc_abort(hs_info, msg_data, msg_len);
        break;
    case _VN_HS_MSG_INIT:
        rv = _vn_hs_server_proc_init(hs_info, msg_data, msg_len);
        break;
    case _VN_HS_MSG_AUTH:
        rv = _vn_hs_server_proc_auth(hs_info, msg_data, msg_len);
        break;
    case _VN_HS_MSG_INIT_ACK:
    case _VN_HS_MSG_AUTH_ACK:
        /* Unexpected message type */
        _VN_TRACE(TRACE_WARN, _VN_SG_HS,
                  "Unexpected HS response %d, session %u, seq %u\n", 
                  msg_type, hs_info->session_id, hs_info->seq_no);
        /* Ignore */
        rv = _VN_ERR_FAIL;
         break;
    default:
        /* Invalid message type */
        _VN_TRACE(TRACE_WARN, _VN_SG_HS,
                  "Invalid HS message %d, session %u, seq %u\n", 
                  msg_type, hs_info->session_id, hs_info->seq_no);
        /* Ignore? */
        rv = _VN_ERR_INVALID;
        break;
    }
    return rv;
}

/* Callback function for receiving a handshake packet */
int _vn_hs_recv_pkt(_vn_buf_t* pkt,
                    _vn_inaddr_t hs_addr, _vn_inport_t hs_port)
{
    int rv;

    /* Save message for debugging (saved in _vn_recv) */
    /* _vn_dbg_save_buf(_VN_DBG_RECV_PKT, msg->pkt->buf, msg->pkt->len); */
    if (_VN_TRACE_ON(TRACE_FINER, _VN_SG_HS)) {
        char addr[_VN_INET_ADDRSTRLEN];
        _vn_inet_ntop(&(hs_addr), addr, sizeof(addr));
        _VN_TRACE(TRACE_FINER, _VN_SG_HS,
                  "_vn_hs_recv_pkt: received handshake from %s:%d\n",
                  addr, hs_port);

        if (_VN_TRACE_ON(TRACE_FINEST, _VN_SG_HS)) {
            _vn_dbg_dump_buf(stdout, pkt->buf, pkt->len);
        }
    }

    rv = _vn_hs_proc_pkt(pkt, hs_addr, hs_port);
    return rv;
}

int _vn_hs_parse_header(_vn_hs_msg_header_t* msg_header, const uint8_t* buf, size_t buflen)
{
    _vn_hs_msg_header_t* pHeader;
    assert(msg_header);
    if (buflen >= _VN_HS_HEADER_SIZE) {
        assert(buf);
        pHeader = (_vn_hs_msg_header_t*) buf;
        msg_header->version = pHeader->version;
        msg_header->type = pHeader->type;
        msg_header->ID_c.device_type = ntohl(pHeader->ID_c.device_type);
        msg_header->ID_c.chip_id = ntohl(pHeader->ID_c.chip_id);
        msg_header->session_id = ntohl(pHeader->session_id);
        msg_header->seq_no = ntohs(pHeader->seq_no);
        return _VN_ERR_OK;;
    }
    else {
        /* Invalid message, drop and indicate error */
        _VN_TRACE(TRACE_WARN,_VN_SG_HS,
                  "Message too short for HS header, "
                  "only %d bytes\n", buflen);
        return _VN_ERR_TOO_SHORT;
    }
}

bool _vn_hs_seqno_larger_than(uint16_t x, uint16_t y)
{
    if (x <= y)
        return (y - x) > 32768;
    else return (x - y) < 32768;
}

void _vn_hs_backout_host(_vn_hs_info_t* hs_info)
{
    assert(hs_info);
    _VN_TRACE(TRACE_FINER, _VN_SG_HS,
              "Backing out net 0x%08x, host %u, flags 0x%08x\n",
              hs_info->vninfo.net_id, hs_info->vninfo.host_c,
              hs_info->vninfo.flags);
    if (hs_info->vninfo.flags & _VN_HS_FLAG_COMMITED) {
        /* Net id initialized - so entry is valid */
        _VN_net_t net_id = hs_info->vninfo.net_id;
        _VN_host_t host_id = hs_info->vninfo.host_c;
        if (hs_info->initiator) {
            /* I'm the client - just remove net */
            /* Just back out this local host - only remove net if
               that's the last local host */
            _vn_delete_peer(net_id, host_id);
            if (_vn_get_local_size(net_id) == 0) {
                /* No more localhost */
                _vn_delete_net(net_id);
            }
        }
        else {
            /* I'm the server (owner) - remove client */
            if (hs_info->net_id == _VN_NET_DEFAULT) {
                /* Default net requested, can remove entire net */
                _vn_delete_net(net_id);
            }
            else {
                /* Just remove peer */
                _vn_delete_peer(net_id, host_id);
            }
            
        }    
    }
}

void _vn_hs_proc_state(_vn_hs_info_t* hs_info)
{
    assert(hs_info);

    switch (hs_info->state) {
    case _VN_ST_ERROR:
        /* Post error event */
        if (hs_info->initiator) {
            _vn_post_err_event(hs_info->session_id, hs_info->error, NULL, 0);
        }

        /* Fall through */
    case _VN_ST_CLOSED:
        /* Also clean up any net created by HS, remove peers */
        _vn_hs_backout_host(hs_info);

        /* Clean up entry */
        _vn_hs_remove_state(hs_info);
        break;
    case _VN_ST_ESTABLISHED:
        _vn_hs_established(hs_info);
        break;
    }
}

int _vn_hs_proc_pkt(_vn_buf_t* pkt, _vn_inaddr_t hs_addr, _vn_inport_t hs_port)
{
    _vn_hs_msg_header_t msg_header;
    _vn_hs_info_t* hs_info;
    int rv = _VN_ERR_OK;
    const uint8_t* hs_msg_data;
    size_t hs_msg_len;
    uint8_t msg_type;
    uint8_t msg_dir;
    bool packet_saved = false;
    bool initiator;
    _VN_guid_t guid; /* Guid for looking up HS request */

    assert(pkt);
    
    rv = _vn_hs_parse_header(&msg_header, pkt->buf, pkt->len);
    if (rv < 0) return rv;
    
    msg_type = _VN_HS_MSG_TYPE(msg_header.type);
    msg_dir  = _VN_HS_MSG_DIR(msg_header.type);

    initiator = _vn_hs_dir2initiator(msg_dir);
    guid.device_type = _VN_DEVICE_TYPE_UNKNOWN;
    guid.chip_id = msg_header.ID_c.chip_id;
    hs_info = _vn_hs_get_state(guid,
                               msg_header.session_id, initiator);
    hs_msg_data = pkt->buf + _VN_HS_HEADER_SIZE;
    hs_msg_len = pkt->len - _VN_HS_HEADER_SIZE;

    /* TODO: separate client/server specific processing */
    if (hs_info == NULL) {
        /* Peer acting purely as client should never get NULL */
        /* No state - _VN_ST_CLOSED */
        if (msg_type == _VN_HS_MSG_INIT) {
            rv = _vn_hs_proc_new_init(&hs_info, msg_header.ID_c,
                                      hs_addr, hs_port,
                                      msg_header.session_id, msg_header.seq_no,
                                      hs_msg_data, hs_msg_len);
            if (rv >= 0) {
                /* Save packet */
                assert(hs_info->pkts[msg_type-1] == NULL);
                hs_info->pkts[msg_type-1] = pkt;
                packet_saved = true;
            }

        }
        else {
            /* Ignore message */
            _VN_TRACE(TRACE_WARN, _VN_SG_HS,
                      "Unexpected HS message 0x%x from client 0x%08x %u, "
                      "session %u, seq %u, not in HS phase\n", 
                      msg_header.type, msg_header.ID_c.device_type,
                      msg_header.ID_c.chip_id,
                      msg_header.session_id, msg_header.seq_no);
            rv = _VN_ERR_DUPPKT;
        }
    }
    else if (!_vn_guid_eq(&hs_info->ID_c, &msg_header.ID_c)) {        
        /* Make sure handshake is for this client (with correct device_type) */
        _vn_hs_info_t tmp_hsinfo;

        _VN_TRACE(TRACE_WARN, _VN_SG_HS,
                  "Outstanding HS session %u with client 0x%08x %u differs "
                  "from request for session %u from client 0x%08 %u\n",
                  hs_info->session_id,
                  hs_info->ID_c.device_type, hs_info->ID_c.chip_id,
                  msg_header.session_id,
                  msg_header.ID_c.device_type, msg_header.ID_c.chip_id);
        
        rv = _VN_ERR_DUPENTRY;
        hs_info = NULL;

        /* Send reject message? */
        _vn_hs_init_state(&tmp_hsinfo, msg_header.ID_c, 
                          msg_header.session_id, msg_header.seq_no, false);

        tmp_hsinfo.hs_addr = hs_addr;
        tmp_hsinfo.hs_port = hs_port;
        _vn_hs_abort(&tmp_hsinfo, _VN_HS_ERR_CLIENT_BAD_INIT, rv, NULL, 0);
    }
    else {
        /* Update HS address and port, always send reply back to sender */
        hs_info->hs_addr = hs_addr;
        hs_info->hs_port = hs_port;        
        
        /* Temporarily save message if there is empty spot for it */
        if ((msg_type > 0) && (msg_type <= _VN_HS_MSG_COUNT)) {
            if (hs_info->pkts[msg_type-1] == NULL) {
                hs_info->pkts[msg_type-1] = pkt;
                packet_saved = true;
            }
        }

        /* Check if this is a valid connection */
        if ((msg_header.session_id == hs_info->session_id) && 
            (msg_header.seq_no == hs_info->seq_no)) 
        {
            /* Initiator (i.e. client will accept packet) but server should
               treat packet as duplicate packet */
            if (hs_info->initiator) {
                rv = _vn_hs_client_proc_msg(hs_info, msg_type,
                                            hs_msg_data, hs_msg_len);
            }
            else {
                rv = _vn_hs_server_proc_dupreq(hs_info, pkt, msg_type);
            }
        }
        else if (_vn_hs_seqno_larger_than(msg_header.seq_no, hs_info->seq_no)) 
        {
            if (!hs_info->initiator) {
                /* Server */
                if (msg_type == _VN_HS_MSG_INIT) {
                    rv = _vn_hs_proc_new_init(&hs_info, msg_header.ID_c,
                                              hs_addr, hs_port,
                                              msg_header.session_id, msg_header.seq_no,
                                              hs_msg_data, hs_msg_len);
                    if (rv >= 0) {
                        /* Save packet */
                        assert(hs_info->pkts[msg_type-1] == NULL);
                        hs_info->pkts[msg_type-1] = pkt;
                        packet_saved = true;
                    }
                }
                else {
                    hs_info->seq_no = msg_header.seq_no;
                    rv = _vn_hs_server_proc_msg(hs_info, msg_type,
                                                hs_msg_data, hs_msg_len);                
                }
            }
            else {
                /* Client */
                hs_info->seq_no = msg_header.seq_no;
                /* Totally unexpected packet - server and client out of sync*/
                rv = _VN_ERR_FAIL;
                _vn_hs_abort(hs_info, _VN_HS_ERR_SERVER_BAD_SEQ, rv, NULL, 0);
            }
        }
        else {
            /* Sequence number smaller, ignore message (Duplicate packet) */
            _VN_TRACE(TRACE_FINE, _VN_SG_HS,
                      "Sequence number %u less than %u, ignoring handshake "
                      "from client 0x%08x %u, session %u\n",
                      msg_header.seq_no, hs_info->seq_no,
                      msg_header.ID_c.device_type, msg_header.ID_c.chip_id,
                      msg_header.session_id);
            rv = _VN_ERR_DUPPKT;
        }
    }

    if (rv >= 0) {
        /* Good message - delete if not saved */
        if (!packet_saved) {
            _VN_TRACE(TRACE_FINE, _VN_SG_HS,
                      "Deleting unsaved HS message 0x%08x from "
                      "client 0x%08x %u, session %u, seq %u, rv %d\n",
                      msg_header.type, msg_header.ID_c.device_type,
                      msg_header.ID_c.chip_id,
                      msg_header.session_id, msg_header.seq_no, rv);
            _vn_free_msg_buffer(pkt);
        }
    }
    else {
        /* Bad message - don't save it */
        if (packet_saved && hs_info && 
            (msg_type > 0) && (msg_type <= _VN_HS_MSG_COUNT)) {
            hs_info->pkts[msg_type-1] = NULL;
        }

        _VN_TRACE(TRACE_FINE, _VN_SG_HS,
                  "Ignoring HS message 0x%08x from client 0x%08x %u, "
                  "session %u, seq %u, rv %d\n",
                  msg_header.type, msg_header.ID_c.device_type,
                  msg_header.ID_c.chip_id,
                  msg_header.session_id, msg_header.seq_no, rv);
    }

    if (hs_info) {
        _vn_hs_proc_state(hs_info);
    }

    return rv;
}

int _vn_hs_header_init(bool initiator, uint8_t msgtype, _VN_guid_t ID_c,
                       uint32_t session_id, uint16_t seq_no,
                       char* buf, size_t buflen)
{
    _vn_hs_msg_header_t* header;
    uint8_t dir;
    assert(buf);
    assert(buflen >= _VN_HS_HEADER_SIZE);
    header = (_vn_hs_msg_header_t*) buf;
    header->version = _VN_VERSION + _VN_HS_VERSION_ID;
    dir = initiator? _VN_HS_MSG_CS_DIR: _VN_HS_MSG_SC_DIR;
    header->type = msgtype | dir;
    header->ID_c.device_type = htonl(ID_c.device_type);
    header->ID_c.chip_id = htonl(ID_c.chip_id);
    header->session_id = htonl(session_id);
    header->seq_no = htons(seq_no);
    return _VN_ERR_OK;
}

_vn_buf_t* _vn_hs_create_abort_msg(bool initiator,
                                   _VN_guid_t ID_c, uint32_t session_id,
                                   uint16_t seq_no, uint8_t reason,
                                   uint8_t error, void* errmsg, size_t msglen)
{
    _vn_buf_t* pkt;
    _VN_msg_len_t pktlen;

    assert(msglen < _VN_MAX_MSG_LEN);
    pktlen = _VN_HS_HEADER_SIZE + sizeof(_vn_hs_msg_abort_t) 
        + (errmsg? (_VN_msg_len_t) msglen: 0);
    pkt = _vn_get_msg_buffer(pktlen);
    if (pkt != NULL) {
        _vn_hs_msg_abort_t* msg;
        _vn_hs_header_init(initiator, _VN_HS_MSG_ABORT, ID_c, session_id,
                           seq_no, pkt->buf, pkt->max_len);
        msg = (_vn_hs_msg_abort_t*) (pkt->buf + _VN_HS_HEADER_SIZE);
        msg->err_reason = reason;
        msg->err_code = error;
        if (errmsg) {
            msg->size = htons((_VN_msg_len_t) msglen);
            memcpy(msg->err_str, errmsg, msglen);
        }
        else {
            msg->size = 0;
        }
        pkt->len = pktlen;
    }
    return pkt;
}

int _vn_hs_send_abort(_vn_hs_info_t* hs_info, uint8_t reason,
                      int8_t error, void* msg, size_t msglen)
{
    int rv;
    _vn_buf_t* pkt;
    assert(hs_info);    
    pkt = _vn_hs_create_abort_msg(hs_info->initiator, hs_info->ID_c,
                                  hs_info->session_id, hs_info->seq_no,
                                  reason, error, msg, msglen);
    if (pkt == NULL) {
        return _VN_ERR_NOMEM;
    }

    rv = _vn_hs_send_many(hs_info, pkt->buf, pkt->len);
    _vn_free_msg_buffer(pkt);
    return rv;
}

_vn_buf_t* _vn_hs_create_init_msg(bool initiator, _VN_guid_t ID_c, 
                                  uint32_t session_id, uint16_t seq_no,
                                  _VN_net_t net_id, uint32_t rand_c, 
                                  _vn_cert_t* cert,
                                  _vn_inaddr_t addr_c, _vn_inport_t port_c,
                                  _vn_inaddr_t addr_s, _vn_inport_t port_s)
{
    _VN_msg_len_t pktlen = _VN_HS_HEADER_SIZE + _VN_HS_MSG_INIT_SIZE + 
        (cert? (cert->certSize + cert->certChainSize): 0);
    _vn_buf_t* pkt = _vn_get_msg_buffer(pktlen);
    
    if (pkt != NULL) {
        _vn_hs_msg_init_t* msg;
        
        _vn_hs_header_init(initiator, _VN_HS_MSG_INIT, ID_c, session_id,
                           seq_no, pkt->buf, pkt->max_len);

        msg = (_vn_hs_msg_init_t*) (pkt->buf + _VN_HS_HEADER_SIZE);
        msg->version_c = _VN_HS_VERSION;
        memset(msg->pad, 0, sizeof(msg->pad));
        msg->net_id = htonl(net_id);
        msg->rand_c = htonl(rand_c);
        msg->addr_c = addr_c; /* IP stored in network order */
        msg->port_c = htons(port_c);
        msg->addr_s = addr_s; /* IP stored in network order */
        msg->port_s = htons(port_s);
        if (cert) {
            memcpy(msg->cert_c, cert->cert, cert->certSize);
            memcpy(msg->cert_c + cert->certSize, 
                   cert->certChain, cert->certChainSize);
        } 
        pkt->len = pktlen;
    }
    return pkt;
}

int _vn_hs_send_init(_vn_hs_info_t* hs_info)
{
    int rv;
    _vn_buf_t* pkt;

    assert(hs_info);
#if _VN_USE_IOSC
    IOSC_GenerateRand ((uint8_t*) &hs_info->rand_c, 4);
#else
    hs_info->rand_c = _vn_rand();
#endif
    pkt = _vn_hs_create_init_msg(hs_info->initiator,
                                 hs_info->ID_c,
                                 hs_info->session_id,
                                 hs_info->seq_no,
                                 hs_info->net_id,
                                 hs_info->rand_c, 
                                 _vn_hs_get_cert_c(),
                                 hs_info->vn_addr_c,
                                 hs_info->vn_port_c,
                                 hs_info->hs_addr,
                                 hs_info->hs_port);
    if (pkt == NULL) {
        return _VN_ERR_NOMEM;
    }

    rv = _vn_hs_send_many(hs_info, pkt->buf, pkt->len);
    if (rv < 0) {
        _vn_free_msg_buffer(pkt);
        return rv;
    }

    /* Save this packet for reference */
    assert(hs_info->pkts[_VN_HS_MSG_INIT-1] == NULL);
    hs_info->pkts[_VN_HS_MSG_INIT-1] = pkt;
    /* Save request and set timer for retransmission */
    hs_info->last_pkt = pkt;
    _vn_timer_mod(&hs_info->timer, _VN_HS_RETX_TIMER_MIN, false);
    _vn_timer_retrigger(&hs_info->timer, true);
    return rv;    
}                      

_vn_buf_t* _vn_hs_create_init_ack_msg(bool initiator, _VN_guid_t ID_c,
                                      uint32_t session_id, uint16_t seq_no,
                                      uint32_t rand_s, _vn_cert_t* cert)
{ 
    _VN_msg_len_t pktlen = _VN_HS_HEADER_SIZE + _VN_HS_MSG_INIT_ACK_SIZE + 
        (cert? (cert->certSize+cert->certChainSize): 0);
    _vn_buf_t* pkt = _vn_get_msg_buffer(pktlen);

    if (pkt != NULL) {
        _vn_hs_msg_init_ack_t* msg;
        _VN_guid_t myguid = _vn_get_myguid();
        
        _vn_hs_header_init(initiator, _VN_HS_MSG_INIT_ACK, ID_c, session_id,
                           seq_no, pkt->buf, pkt->max_len);

        msg = (_vn_hs_msg_init_ack_t*) (pkt->buf + _VN_HS_HEADER_SIZE);
        msg->ID_s.device_type = htonl(myguid.device_type);
        msg->ID_s.chip_id = htonl(myguid.chip_id);
        msg->rand_s = htonl(rand_s);
        if (cert) {
            memcpy(msg->cert_s, cert->cert, cert->certSize);
            memcpy(msg->cert_s + cert->certSize, 
                   cert->certChain, cert->certChainSize);
        }
        pkt->len = pktlen;
    }
    return pkt;
}

int _vn_hs_send_init_ack(_vn_hs_info_t* hs_info)
{
    int rv;
    _vn_buf_t* pkt;

    assert(hs_info);
#if _VN_USE_IOSC
    IOSC_GenerateRand ( (uint8_t*) &hs_info->rand_s, 4);
#else
    hs_info->rand_s = _vn_rand();
#endif
    pkt = _vn_hs_create_init_ack_msg(hs_info->initiator,
                                     hs_info->ID_c,
                                     hs_info->session_id,
                                     hs_info->seq_no,
                                     hs_info->rand_s,
                                     _vn_hs_get_cert_s());
    if (pkt == NULL) {
        return _VN_ERR_NOMEM;
    }

    rv = _vn_hs_send(hs_info->hs_addr, hs_info->hs_port, pkt->buf, pkt->len);
    if (rv < 0) {
        _vn_free_msg_buffer(pkt);            
        return rv;
    }

    /* Save this packet for reference */
    assert(hs_info->pkts[_VN_HS_MSG_INIT_ACK-1] == NULL);
    hs_info->pkts[_VN_HS_MSG_INIT_ACK-1] = pkt;
    /* Save response for retransmission */
    hs_info->last_pkt = pkt;
    _vn_timer_retrigger(&hs_info->timer, true);

    return rv;    
}

int _vn_hs_create_auth_msg(_vn_buf_t **pResult,
                           bool initiator, _VN_guid_t ID_c,
                           uint32_t session_id, uint16_t seq_no,
                           _vn_hs_cert_c_t cert_Yc,
                           _vn_hs_info_t* hs_info)
{ 
    _VN_msg_len_t pktlen = _VN_HS_HEADER_SIZE + _VN_HS_MSG_AUTH_SIZE;
    _vn_buf_t* pkt = _vn_get_msg_buffer(pktlen);
    int rv = _VN_ERR_OK;

    assert(pResult);
    *pResult = NULL;

    if (pkt != NULL) {
        _vn_hs_msg_auth_t* msg;
        
        _vn_hs_header_init(initiator, _VN_HS_MSG_AUTH, ID_c, session_id,
                           seq_no, pkt->buf, pkt->max_len);

        msg = (_vn_hs_msg_auth_t*) (pkt->buf + _VN_HS_HEADER_SIZE);
        if (cert_Yc) {
            memcpy(msg->cert_Yc, cert_Yc, sizeof(_vn_hs_cert_c_t));
        } else {
            memset(msg->cert_Yc, 0, sizeof(_vn_hs_cert_c_t));
        }

        /* Sign hash of INIT | INIT_ACK | Yc with private ECC key */
        rv = _vn_hs_gen_client_sig(msg->sig_c, 
                                   hs_info->Yc, /* private ECC key */
                                   hs_info,
                                   (uint8_t*) msg, 
                                   _VN_HS_MSG_AUTH_SIZE - sizeof(_vn_hs_sign_c_t));
        if (rv < 0) {
            _vn_free_msg_buffer(pkt);
            return rv;
        }
        
        pkt->len = pktlen;
        *pResult = pkt;
    } else {
        rv = _VN_ERR_NOMEM;
    }
    return rv;
}

int _vn_hs_send_auth(_vn_hs_info_t* hs_info)
{
    uint8_t *cert_Yc;
    int rv;
    _vn_buf_t* pkt;

    assert(hs_info);

    /* Figure out Yc */
    rv = _vn_hs_dh_generate_key(&hs_info->Yc, &cert_Yc);
    if (rv < 0) {
        return rv;
    }

    rv = _vn_hs_create_auth_msg(&pkt,
                                hs_info->initiator,
                                hs_info->ID_c,
                                hs_info->session_id,
                                hs_info->seq_no, cert_Yc,
                                hs_info);

    if (rv < 0) {
        return rv;
    }

    rv = _vn_hs_send(hs_info->hs_addr, hs_info->hs_port, pkt->buf, pkt->len);
    if (rv < 0) {
        _vn_free_msg_buffer(pkt);
        return rv;
    }

    /* Save this packet for reference */
    assert(hs_info->pkts[_VN_HS_MSG_AUTH-1] == NULL);
    hs_info->pkts[_VN_HS_MSG_AUTH-1] = pkt;
    /* Save request and set timer for retransmission of auth */
    hs_info->last_pkt = pkt;
    _vn_timer_mod(&hs_info->timer, _VN_HS_RETX_TIMER_MIN, false);
    _vn_timer_retrigger(&hs_info->timer, true);
    return rv;    
}

int  _vn_hs_create_auth_ack_msg(_vn_buf_t** pResult,
                                bool initiator, _VN_guid_t ID_c,
                                uint32_t session_id, uint16_t seq_no,
                                _vn_hs_cert_s_t cert_Ys,
                                _VN_net_t netID,
                                _VN_host_t hostID_c, _VN_host_t hostID_s,
                                _vn_inaddr_t addr_s, _vn_inport_t port_s,
                                _vn_hs_info_t* hs_info, _vn_key_t *vn_key)
{ 
    _VN_msg_len_t pktlen, keylen;
    _vn_buf_t* pkt;
    int rv = _VN_ERR_OK;

    assert(pResult);
    keylen = (vn_key)? _VN_ENC_KEY_LENGTH: 0;
    pktlen = _VN_HS_HEADER_SIZE + _VN_HS_MSG_AUTH_ACK_SIZE + keylen;
    pkt = _vn_get_msg_buffer(pktlen);

    *pResult = NULL;
    if (pkt != NULL) {
        _vn_hs_msg_auth_ack_head_t* msg_head;
        _vn_hs_msg_auth_ack_tail_t* msg_tail;
        uint8_t* keybuf;

        _vn_hs_header_init(initiator, _VN_HS_MSG_AUTH_ACK, ID_c, session_id,
                           seq_no, pkt->buf, pkt->max_len);        

        msg_head = (_vn_hs_msg_auth_ack_head_t*) (pkt->buf + _VN_HS_HEADER_SIZE);
        msg_head->vnaddr_c = htonl( _VN_make_addr(netID, hostID_c) );
        msg_head->addr_s = addr_s; /* IP stored in network order */
        msg_head->port_s = htons(port_s);
        msg_head->host_s = htons(hostID_s);
        msg_head->key = (vn_key)? 1:0;
        memset(msg_head->pad, 0, sizeof(msg_head->pad));

        keybuf = (uint8_t*) (msg_head + 1);
        if (vn_key) {
            /* Use msg auth header as iv */
            uint8_t* iv = (uint8_t*) msg_head;
            /* Put encrypted vn key in keybuf */
            rv = _vn_hs_export_vnkey(keybuf, keylen, vn_key, 
                                     hs_info->DH_shared_handle,
                                     hs_info->rand_c, hs_info->rand_s, iv);
            if (rv < 0) {
                _vn_free_msg_buffer(pkt);
                return rv;
            }
        }

        msg_tail = (_vn_hs_msg_auth_ack_tail_t*) (keybuf + keylen);
        if (cert_Ys) {
            memcpy(msg_tail->cert_Ys, cert_Ys, sizeof(_vn_hs_cert_s_t));
        } else {
            memset(msg_tail->cert_Ys, 0, sizeof(_vn_hs_cert_s_t));
        }

        /* Sign hash of INIT | INIT_ACK | AUTH_ACK with private ECC key */
        rv = _vn_hs_gen_server_sig(msg_tail->sig_s, 
                                   hs_info->Ys, /* private ECC key */
                                   hs_info,
                                   (uint8_t*) msg_head,
                                   _VN_HS_MSG_AUTH_ACK_SIZE + keylen 
                                   - sizeof(_vn_hs_sign_s_t));
        if (rv < 0) {
            _vn_free_msg_buffer(pkt);
            return rv;
        }

        pkt->len = pktlen;
        *pResult = pkt;
    } else {
        rv = _VN_ERR_NOMEM;
    }
    return rv;
}

int _vn_hs_send_auth_ack(_vn_hs_info_t* hs_info)
{
    uint8_t *cert_Ys;
    int rv = _VN_ERR_OK;
    _vn_buf_t* pkt;
    _vn_hs_vninfo_t* vninfo;
    uint16_t host_mask;

    assert(hs_info);

    /* Use vninfo to store what we used to create this vn */
    vninfo = &hs_info->vninfo;

    /* Figure out Ys */
    rv = _vn_hs_dh_generate_key(&hs_info->Ys, &cert_Ys);
    if (rv < 0) {
        return rv;
    }

    /* Server IP/Port */
    vninfo->addr_s = hs_info->my_hs_addr;
    vninfo->port_s = hs_info->my_hs_port;
    
    /* Client IP/Port */
    /* TODO: What if there is both HS addr and VN addr? */
#if 0
    vninfo->addr_c = hs_info->vn_addr_c? hs_info->vn_addr_c: hs_info->hs_addr;
    vninfo->port_c = hs_info->vn_port_c? hs_info->vn_port_c: hs_info->hs_port;
#endif
    vninfo->addr_c = hs_info->hs_addr;
    vninfo->port_c = hs_info->hs_port;

    /* Mark host state as joining */
    host_mask = _VN_PROP_JOINING;
    if (_vn_guid_is_self(&hs_info->ID_c)) {
        /* Oh, I'm talking to myself */
        host_mask |= _VN_PROP_LOCAL;
    }

    /* Select new VN id, client and server ids */
    if (hs_info->net_id == _VN_NET_DEFAULT) {
        _vn_net_info_t* net_info;

        vninfo->net_id = _vn_net_newid(_VN_NET_MASK1, !_vn_isserver());
        if (vninfo->net_id == _VN_NET_DEFAULT) {
            /* Oops, error getting new network id */
            return _VN_ERR_NETID;
        }

        vninfo->host_c = _VN_HOST_CLIENT;
        vninfo->host_s = _VN_HOST_SERVER;   

        /* Figure out VN key */
        rv = _vn_hs_compute_shared_key(&hs_info->DH_shared_handle,
                                       hs_info->Ys,
                                       hs_info->Yc);

        if (rv < 0) {
            return rv;
        }

#if _VN_EXPAND_KEY
        rv = _vn_hs_compute_vnkey(&vninfo->key, 
                                  hs_info->DH_shared_handle,
                                  hs_info->rand_c, hs_info->rand_s);
#else
        rv = _vn_generate_vnkey(&vninfo->key);
#endif

        if (rv < 0) {
            return rv;
        }

        rv = _vn_hs_create_auth_ack_msg(&pkt,
                                        hs_info->initiator,
                                        hs_info->ID_c,
                                        hs_info->session_id,
                                        hs_info->seq_no,
                                        cert_Ys, vninfo->net_id, 
                                        vninfo->host_c, vninfo->host_s,
                                        vninfo->addr_s, vninfo->port_s,
#if _VN_EXPAND_KEY
                                        hs_info, NULL);
#else
                                        hs_info, &vninfo->key);
#endif
        
        if (rv < 0) {
            return rv;
        }

        rv = _vn_add_default_net(vninfo->net_id, true /* i am master */,
                                 vninfo->host_s, hs_info->ID_c,
                                 vninfo->host_c,
                                 host_mask,
                                 /* Server IP and port (me) */
                                 vninfo->addr_s, vninfo->port_s,
                                 /* Client IP and port */
                                 vninfo->addr_c, vninfo->port_c,
                                 vninfo->key, 0);

        net_info = _vn_lookup_net_info(vninfo->net_id);
        assert(net_info);
        net_info->listen_call = _vn_hs_listen_default_call;

        if (rv < 0) {
            _vn_free_msg_buffer(pkt);
            return rv;
        }

        /* vninfo key passed to net_info, does not need to be deleted */
        vninfo->flags |= _VN_HS_FLAG_KEY_KEEP;

        rv = _vn_hs_send(hs_info->hs_addr, hs_info->hs_port, pkt->buf, pkt->len);
        if (rv < 0) {
            /* Cannot send message to peer, handshake will be aborted,
               so need to remove net */
            _vn_delete_net(vninfo->net_id);
            _vn_free_msg_buffer(pkt);
            return rv;
        }
    }
    else {
        _vn_net_info_t* net;
        vninfo->net_id = hs_info->net_id;
        net = _vn_lookup_net_info(vninfo->net_id);

        /* Mark host so that we can receive packets, but not send to host */
        host_mask |= _VN_PROP_NOSEND;        

        /* Handle error conditions (really shouldn't happen) */
        if (net == NULL) {
            return _VN_ERR_NETID;
        }
        
        if (!_VN_NET_IS_OWNER(net)) {
            return _VN_ERR_NOT_OWNER;
        }
        
        if (!_VN_NET_IS_ACCEPTING(net)) {
            return _VN_ERR_INACTIVE;
        }

#if _VN_USE_IOSC
        vninfo->key.Ak = net->key.Ak;
        vninfo->key.Hk = net->key.Hk;
        /* vninfo key taken from net_info, does not need to be deleted */
        vninfo->flags |= _VN_HS_FLAG_KEY_KEEP;
#else
        memcpy(vninfo->key, net->key, sizeof(vninfo->key));
#endif
        rv = _vn_get_new_peer_id(net);
        if (rv >= 0) {
            vninfo->host_c = rv;
            vninfo->host_s = net->master_host;

            rv = _vn_hs_compute_shared_key(&hs_info->DH_shared_handle,
                                           hs_info->Ys,
                                           hs_info->Yc);
            if (rv < 0) {
                return rv;
            }
            
            rv = _vn_hs_create_auth_ack_msg(&pkt,
                                            hs_info->initiator,
                                            hs_info->ID_c,
                                            hs_info->session_id,
                                            hs_info->seq_no,
                                            cert_Ys, vninfo->net_id, 
                                            vninfo->host_c, vninfo->host_s,
                                            vninfo->addr_s, vninfo->port_s,
                                            hs_info, &net->key);
            
            if (rv < 0) {
                return rv;
            }

            /* Add client to this VN, mark state as joining
               (i.e. can receive packets but don't send) */
            rv = _vn_add_peer(hs_info->ID_c, 
                              vninfo->net_id, vninfo->host_c, 
                              0 /* clock delta */,
                              vninfo->addr_c, vninfo->port_c,
                              host_mask );
            if (rv < 0) {
                _vn_free_msg_buffer(pkt);
                return rv;
            }

            rv = _vn_hs_send(hs_info->hs_addr, hs_info->hs_port, pkt->buf, pkt->len);
            if (rv < 0) {
                /* Cannot send message to peer, handshake will be aborted,
                   so need to remove peer from net */
                _vn_delete_peer(vninfo->net_id, vninfo->host_c);
                _vn_free_msg_buffer(pkt);
                return rv;
            }

            /* TODO: 
               Wait for ACK from client signifying they have gotten this
               message. Server will then mark client state as accepted
               and send peer update to other peers in VN.
               Client should also ask server (i.e. VN owner) for information
               on other members of the VN network */
        }
        else {
            /* Error getting new peer id!!!!  */
            return rv;
        }

    }

    vninfo->flags |= _VN_HS_FLAG_COMMITED;

    /* Save this packet for reference */
    assert(hs_info->pkts[_VN_HS_MSG_AUTH_ACK-1] == NULL);
    hs_info->pkts[_VN_HS_MSG_AUTH_ACK-1] = pkt;
    /* Save response for retransmission */
    hs_info->last_pkt = pkt;    
    _vn_timer_retrigger(&hs_info->timer, true);
    return rv;    
}

int _vn_hs_abort(_vn_hs_info_t* hs_info, uint8_t reason, int8_t error,
                 void* msg, size_t msglen)
{
    assert(hs_info);
    _VN_TRACE(TRACE_FINE, _VN_SG_HS,
              "Aborting handshake for client 0x%08x %u, "
              "server 0x%08x %u, session %u, due to reason %d, error %d\n",
              hs_info->ID_c.device_type, hs_info->ID_c.chip_id,
              hs_info->ID_s.device_type, hs_info->ID_s.chip_id,
              hs_info->session_id, reason, error);

    _vn_hs_change_state(hs_info, _VN_ST_ERROR);
    hs_info->abort_reason = reason;
    hs_info->error = error;
    /* Increment request seq if client */
    if (hs_info->initiator) hs_info->seq_no++;  
    _vn_hs_send_abort(hs_info, reason, error, msg, msglen);
    return _VN_ERR_OK;
}

/* Sends the buffer to one IP/Port address */
int _vn_hs_send(_vn_inaddr_t ip_addr, _vn_inport_t port, 
                const uint8_t* buf, _VN_msg_len_t buflen)
{
    int rv;

    if (_VN_TRACE_ON(TRACE_FINER, _VN_SG_HS)) {
        char addr[_VN_INET_ADDRSTRLEN];
        _vn_inet_ntop(&(ip_addr), addr, sizeof(addr));
        _VN_TRACE(TRACE_FINER, _VN_SG_HS,
                  "_vn_hs_send: sending handshake to %s:%d\n",
                  addr, port);
        if (_VN_TRACE_ON(TRACE_FINEST, _VN_SG_HS)) {
            _vn_dbg_dump_buf(stdout, buf, buflen);
        }
    }

    /* TODO: Send message to QM instead directly out of socket */
    /* On SC, _vn_netif_send_udp_pkt will be SC RPC call */
    rv = _vn_netif_send_udp_pkt(_vn_netif_get_instance(),
                                ip_addr, port, buf, buflen);

    /* Ignore timeout error - don't care */
    if (rv == _VN_ERR_TIMEOUT) {
#if 0
        /* Always times out, since timeout is 0 */
        _VN_TRACE(TRACE_FINER, _VN_SG_HS,
                  "_vn_hs_send: Send UDP packet timeout\n");
#endif
        rv = _VN_ERR_OK;
    }
    if (rv >= 0) {
        _vn_send_stats.handshake++;
    }
    return rv;
}

/* Sends the buffer to a list of addresses */
int _vn_hs_send_list(_vn_dlist_t* list, 
                     const uint8_t* buf, _VN_msg_len_t buflen)
{
    int rv = 0;
    if (list) {
        _vn_dnode_t* node;
        _vn_dlist_for(list, node)
        {
            _vn_sockaddr_t* sockaddr;
            sockaddr = _vn_dnode_value(node);
            if (sockaddr) {
                if (_vn_hs_send(sockaddr->addr, sockaddr->port,
                                buf, buflen) >= 0) {
                    rv++;
                }
            }
        }
    }
    return rv;
}

/** 
 * Sends the buffer to one or more addresses 
 * @param hs_info      Handshake information detailing where the buffer 
 *                     is to be sent
 */
int _vn_hs_send_many(_vn_hs_info_t* hs_info, 
                     const uint8_t* buf, _VN_msg_len_t buflen)
{
    int rv;
    assert(hs_info);
    
    /* Sends buffer to specified address if available */
    if ((hs_info->hs_addr != _VN_INADDR_INVALID) &&
        (hs_info->hs_port != _VN_INPORT_INVALID)) {
        rv = _vn_hs_send(hs_info->hs_addr, hs_info->hs_port, buf, buflen);
    }
    else {
        _vn_dlist_t* addr_list;
        _VN_guid_t guid;

        /* Use list of addresses corresponding to the client or server
           device as appropriate */
        
        if (hs_info->initiator) {
            guid = hs_info->ID_s;
        }
        else {
            guid = hs_info->ID_c;
        }

        rv = _vn_device_lookup_connect_info_list(guid, 
                                                 hs_info->net_id, 
                                                 NULL, &addr_list);

        if (rv >= 0) {
            rv = _vn_hs_send_list(addr_list, buf, buflen);
            _VN_TRACE(TRACE_FINER, _VN_SG_HS, 
                      "_vn_hs_send_list: sent hs to %d addrs for device"
                      " 0x%08x %d\n", rv, guid.device_type, guid.chip_id);

        } else {
            _VN_TRACE(TRACE_WARN, _VN_SG_HS, 
                      "_vn_hs_send_many: no addrs found for device "
                      " 0x%08x %d, rv %d\n",
                      guid.device_type, guid.chip_id, rv);
        }
    }
    return rv;
}

/* Resend last packet of handshake */
void _vn_hs_resend(_vn_hs_info_t* hs_info)
{
    assert(hs_info);
    assert(hs_info->last_pkt);
    assert(hs_info->last_pkt->buf);
    assert(hs_info->last_pkt->len);
        
    _vn_hs_send_many(hs_info, 
                     hs_info->last_pkt->buf, hs_info->last_pkt->len);
}

/* Client timer callback */
void _vn_hs_client_timer_cb(_vn_timer_t* timer)
{
    _vn_hs_info_t* hs_info;
    uint32_t total, timeout;

    assert(timer);
    hs_info = (_vn_hs_info_t*) timer->data;
    assert(hs_info);

    /* Timeout */
    total = _vn_timer_get_total(timer);
    /* In callback, the timer has expired and been removed from list */
    timeout =  _vn_hs_get_timeout();
    if (hs_info->state == _VN_ST_ESTABLISHED) {
        /* Timeout when the _VN_ST_ESTABLISHED - ERROR */
        _vn_hs_change_state(hs_info, _VN_ST_ERROR);
        hs_info->abort_reason = _VN_HS_ERR_SERVER_UNREACHABLE;
        hs_info->error = _VN_ERR_TIMEOUT;

        /* Tell server that we are leaving net */
        _vn_leave_net(hs_info->session_id, hs_info->vninfo.net_id,
                      hs_info->vninfo.host_c, 
                      _VN_NET_DEFAULT, _VN_HOST_INVALID);
        /* Make sure that this host is not deleted before message is sent */
        hs_info->vninfo.flags &= ~_VN_HS_FLAG_COMMITED;
    }
    else if (hs_info->state == _VN_ST_CLOSED) {
        /* Nothing to do */
    }
    else if (total < timeout) {
        /* If delay still small, set longer delay and retransmit packet */
        uint32_t delay, new_delay, max;
        _vn_hs_resend(hs_info);
        delay = _vn_timer_get_delay(timer);
        max = MIN(_VN_HS_RETX_TIMER_MAX, timeout - total);
        new_delay = MIN(2*delay, max);

        _vn_timer_mod(timer, new_delay, false);
        _vn_timer_retrigger(timer, false);
    }
    else {
        /* too long - give up, still send an abort just in case */
        _vn_hs_abort(hs_info, _VN_HS_ERR_SERVER_UNREACHABLE,
                     _VN_ERR_TIMEOUT, NULL, 0);
        _vn_timer_reset_total(timer);
    }
    _vn_hs_proc_state(hs_info);
}

/* Server callback */
void _vn_hs_server_timer_cb(_vn_timer_t* timer)
{
    _vn_hs_info_t* hs_info;

    assert(timer);
    hs_info = (_vn_hs_info_t*) timer->data;
    /* Timeout */
    /* In callback, the timer has expired and been removed from list */
    /* too long - give up, still send an abort just in case */
    if (hs_info->state == _VN_ST_ESTABLISHED) {
        /* TODO: cleanup handshake resources */
        _vn_hs_change_state(hs_info, _VN_ST_CLOSED);
        
        /* Try to tell client that server side accept timed out */
        _vn_hs_reject(hs_info->session_id, _VN_ERR_TIMEOUT,
                      &(hs_info->vninfo), NULL, 0);

        /* Make sure that this host is not deleted before message is sent */
        hs_info->vninfo.flags &= ~_VN_HS_FLAG_COMMITED;

        _vn_hs_cleanup_state(hs_info);
    }
    else {
        /* too long - give up, still send an abort just in case */
        _vn_hs_abort(hs_info, _VN_HS_ERR_CLIENT_UNREACHABLE,
                     _VN_ERR_TIMEOUT, NULL, 0);
    }

    _vn_hs_proc_state(hs_info); 
}

/* Following three functions called after handshake messages exchanged,
   during stage where host is being accepted/rejected */

/* Called by the server upon getting a Join Request */
int _vn_hs_mark_established(_VN_guid_t guid, uint32_t session_id,
                             bool initiator)
{
    _vn_hs_info_t* hs_info;
    hs_info = _vn_hs_get_state(guid, session_id, initiator);
    if (hs_info == NULL) {
        return _VN_ERR_NOTFOUND;
    }

    _vn_hs_change_state(hs_info, _VN_ST_ESTABLISHED);
    _vn_hs_cleanup_state(hs_info);
    _vn_timer_retrigger(&hs_info->timer, true);
    return _VN_ERR_OK;
}

/* Called by client upon getting Join Response (reject) */
int _vn_hs_mark_error(_VN_guid_t guid, uint32_t session_id, 
                      bool initiator)
{
    _vn_hs_info_t* hs_info;
    _vn_net_info_t* net_info;
    hs_info = _vn_hs_get_state(guid, session_id, initiator);
    if (hs_info == NULL) {
        return _VN_ERR_NOTFOUND;
    }

    _vn_hs_change_state(hs_info, _VN_ST_ERROR);
    _vn_hs_backout_host(hs_info);

    /* If local host was owner, and default net,
       then will need to delete net also */
    net_info = _vn_lookup_net_info(hs_info->vninfo.net_id);
    if (net_info && _VN_NET_IS_DEFAULT(net_info) 
        && _VN_NET_IS_OWNER(net_info)) {
        if (_vn_get_size(net_info) == 1) {
            if (!_vn_isserver()) {
                assert(_VN_NETID_IS_LOCAL(net_info->net_id));
            }
            
            /* This was a default net, it should be deleted when
               only the owner is left */
            _vn_post_localhosts_leave_net(_VN_MSG_ID_INVALID,
                                          net_info->net_id);
            _vn_net_mark_delete(net_info);
        }
    }

    /* Clean up entry */
    _vn_hs_remove_state(hs_info);
    return _VN_ERR_OK;
}

/* Called by client upon getting a Join Response (accept) */
/* Closes a handshake, removes entry if appropriate */
int _vn_hs_close(_VN_guid_t guid, uint32_t session_id, 
                 bool initiator)
{
    _vn_hs_info_t* hs_info;
    hs_info = _vn_hs_get_state(guid, session_id, initiator);
    if (hs_info == NULL) {
        return _VN_ERR_NOTFOUND;
    }

    /* Clean up entry */
    _vn_hs_remove_state(hs_info);
    return _VN_ERR_OK;
}

//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#ifndef __VN_USBRPC_H__
#define __VN_USBRPC_H__

#include "vnusb.h"

#ifdef __cplusplus
extern "C" {
#endif

#define _VN_USB_RPC_NOBLOCK_TIMEOUT                 0
#define _VN_USB_RPC_BLOCK_TIMEOUT    _VN_TIMEOUT_NONE

/* Default timeout for RPC calls returning important information */
#define _VN_USB_RPC_MAX_TIMEOUT         5000
/* Default timeout for other RPC calls */
#define _VN_USB_RPC_MIN_TIMEOUT         1000

#define _VN_USB_RPC_VERSION          1

/* Allow room for maximum size buffer plus a bit more */
#define _VN_USB_RPC_MAX_ARGSLEN  _VN_MSG_BUF_SIZE + 100
#define _VN_USB_RPC_MAX_MSGLEN   (_VN_USB_RPC_MAX_ARGSLEN + sizeof(_vn_usb_rpc_msg_t))

/* Flag to indicate RPC with this proc_id should be processed without being queued */
/* Currently, can only for set if there are no RPC return args */
#define _VN_USB_RPC_FLAGS_PROC_FAST  0x01

#define _VN_USB_RPC_REQUEST          0
#define _VN_USB_RPC_RESPONSE         1

/* Data structures */

typedef uint16_t _vn_usb_rpc_service_t;
typedef uint16_t _vn_usb_rpc_proc_id_t;
typedef int32_t (*_vn_usb_rpc_proc_t) (_vn_usb_t *usb,
                                       const void *args,
                                       size_t      argslen, 
                                       void       *ret,
                                       size_t     *retlen);
typedef int32_t (*_vn_usb_rpc_callback_t) (_vn_usb_t *usb);

typedef struct {
    _vn_usb_rpc_proc_id_t   proc_id;
    uint16_t                proc_flags;
    _vn_usb_rpc_proc_t      proc;
} _vn_usb_rpc_t;

typedef struct {
    uint8_t   version;     /* Version */
    uint8_t   msgtype;     /* Request or response */
    uint16_t  argsoffset;  /* Offset from args where the args data starts */
    uint16_t  argslen;     /* Length of args (excluding offset) */
    uint16_t  flags;       /* Flags (not used) */
    uint32_t  msgid;       /* Message id */
    int32_t   status;      /* If request: 0 = don't wait for response */
                           /* If response: return status of call */
    _vn_usb_rpc_service_t service; /* Service ID */
    _vn_usb_rpc_proc_id_t proc_id; /* Remote procedure ID */    
    char args[];
} _vn_usb_rpc_msg_t;

typedef struct {
    _vn_ht_table_t          procs;
    _vn_usb_rpc_service_t   service;
} _vn_usb_rpc_table_t;

typedef struct {
    _vn_cond_t cond;
    uint32_t   msgid;
    int32_t    status;
    bool       done;
    void      *ret;
    size_t    *retlen;
    _vn_usb_rpc_service_t service;
    _vn_usb_rpc_proc_id_t proc_id;
} _vn_usb_rpc_waiter_t;

#define _VN_USB_ST_CLOSED         0

#define _VN_STATE_INIT            0
#define _VN_STATE_RUNNING         1
#define _VN_STATE_STOPPING        2
#define _VN_STATE_STOP            3
#define _VN_STATE_WAIT            4

typedef struct {
    /* State for outgoing RPC calls */
    uint32_t         reqid;    /* Next request ID to use */
    _vn_dlist_t      waiters;  /* List of RPC calls waiting for responses */
                               /* List of _vn_usb_rpc_waiter_t */

    /* State for incoming RPC calls */
    _vn_dlist_t      requests; /* Queue of incoming requests to be handled */
                               /* List of _vn_usb_rpc_msg_t */
#if !_VN_USE_SERVICE_THREAD
    _vn_cond_t       req_notempty;  /* Signals request queue is not empty */
#endif
    
    uint32_t         timeout;
    uint32_t         state;    
    _vn_thread_t     disp_thread;
    _vn_thread_t     server_thread;

    _vn_usb_rpc_callback_t disp_stop_cb;
} _vn_usb_rpc_state_t;

/* RPC over USB prototypes */
int _vn_usb_init_rpc();
int _vn_usb_shutdown_rpc();
int _vn_usb_rpc_register_service(_vn_usb_rpc_service_t service);
int _vn_usb_rpc_unregister_service(_vn_usb_rpc_service_t service);
int _vn_usb_register_rpc(_vn_usb_rpc_service_t service,
                         _vn_usb_rpc_proc_id_t proc_id,
                         uint16_t proc_flags,
                         _vn_usb_rpc_proc_t proc);
int _vn_usb_unregister_rpc(_vn_usb_rpc_service_t service,
                           _vn_usb_rpc_proc_id_t proc_id);
int _vn_usb_call_rpc(_vn_usb_handle_t handle,
                     _vn_usb_rpc_service_t service,
                     _vn_usb_rpc_proc_id_t proc_id,
                     const void *args,
                     size_t      argslen, 
                     void       *ret,
                     size_t     *retlen,
                     uint32_t timeout);
int _vn_usb_call_rpc_nocopy(_vn_usb_handle_t handle,
                            _vn_usb_rpc_service_t service,
                            _vn_usb_rpc_proc_id_t proc_id,
                            _vn_wbuf_t *rpc_buf,
                            void       *ret,
                            size_t     *retlen,
                            uint32_t timeout);

int _vn_usb_rpc_start(_vn_usb_handle_t handle, uint32_t timeout);
int _vn_usb_rpc_stop(_vn_usb_handle_t handle);
int _vn_usb_rpc_register_callback(_vn_usb_handle_t handle, int state,
                                  _vn_usb_rpc_callback_t callback);

bool _vn_usb_rpc_proc_next(_vn_usb_handle_t handle,
                           char* resp_args, size_t resp_argsmaxlen);

#ifdef  __cplusplus
}
#endif

#endif /* __VN_USBRPC_H__ */

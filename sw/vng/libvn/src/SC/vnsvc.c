//
//               Copyright (C) 2006, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "../vnlocal.h"

/* Service thread for VN */
/* Handles Timer, QM, Dispatcher, and USB RPC requests */

#if _VN_USE_SERVICE_THREAD

#define _VN_MAX_SERVICE_MSGS                   8

IOSMessage _vn_service_msgs[_VN_MAX_SERVICE_MSGS];
IOSMessageQueueId _vn_service_mq = -1;

IOSMessageQueueId _vn_service_get_mq()
{
    return _vn_service_mq;
}

void _vn_service_set_mq(IOSMessageQueueId mq)
{
    _vn_service_mq = mq;
}

int _vn_service_init_mq()
{
    _vn_service_mq = IOS_CreateMessageQueue(
        _vn_service_msgs, _VN_MAX_SERVICE_MSGS);
    if (_vn_service_mq < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TIMER,
                  "Unable to create service mq: IOS error %d\n", 
                  _vn_service_mq);
        return _VN_ERR_FAIL;
    }
    return _VN_ERR_OK;
}

int _vn_service_destroy_mq()
{
    IOSError rv;
    if (_vn_service_mq >= 0) {
        rv = IOS_DestroyMessageQueue(_vn_service_mq);
        _vn_service_mq = -1;
    }
    return _VN_ERR_OK;
}

int _vn_service_signal(IOSMessage ios_msg)
{
    IOSError rv;
    rv = IOS_SendMessage(_vn_service_mq, ios_msg, 
                         IOS_MESSAGE_NOBLOCK);
    if (rv != IOS_ERROR_OK) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_MISC,
                  "Error signalling service 0x%08x, IOS error %d\n",
                  ios_msg, rv);
        return _VN_ERR_FAIL;
    }
    return _VN_ERR_OK;
}

void _vn_service_run()
{
    IOSMessage ios_msg;
    IOSError rv = IOS_ERROR_OK;

    /* Buffer for USB RPC */
    size_t resp_argsmaxlen = _VN_USB_RPC_MAX_ARGSLEN+1;
    static char* resp_args;
    
    resp_args = _vn_malloc(resp_argsmaxlen);
    if (resp_args == NULL) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_RPC,
                  "Error getting buffer for USB RPC: Out of memory\n");
        return;
    }

    _VN_TRACE(TRACE_FINE, _VN_SG_MISC, "Starting service thread\n");

    while (rv != IOS_ERROR_INTR) {
        rv = IOS_ReceiveMessage(_vn_service_mq, &ios_msg, IOS_MESSAGE_BLOCK);
        if (rv == IOS_ERROR_OK) {
            switch (ios_msg) {

            case _VN_TIMER_MSG:
                _vn_timer_handler_locked();
                _vn_sys_timer_retrigger();
                break;

            case _VN_QM_MSG:
                if (_vn_qm_proc_next()) {
                    /* Make sure that QM gets called again */
                    _vn_service_signal(ios_msg);
                }
                break;

            case _VN_DISP_MSG: 
                _vn_dispatcher_proc_next();
                break;

            case _VN_RPC_MSG:
                _vn_usb_rpc_proc_next(0, resp_args, resp_argsmaxlen);
                break;

            default:
                _VN_TRACE(TRACE_WARN, _VN_SG_MISC,
                          "Unexpected service message 0x%08x\n", ios_msg);
                break;
            }
        }
        else {
            /* TODO: Handle error */
            _VN_TRACE(TRACE_FINE, _VN_SG_TIMER,
                      "Error waiting for service message: IOS error %d\n", rv);
        }
        
    }

    _vn_free(resp_args);

    _VN_TRACE(TRACE_FINE, _VN_SG_MISC, "Stopping service thread\n");
}

#endif

//
//               Copyright (C) 2006, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "../vnusb.h"
#include "../vndebug.h"

/* Real USB - using IOS resource manager */

int _vn_init_usb_real(_vn_usb_t* usb)
{
    IOSError ios_rv;
    _vn_usb_real_t* usb_real;
    int rv = _VN_ERR_OK;
    int i;

    /* Initialize USB communication */

    assert(usb);
    assert(usb->mode == _VN_USB_MODE_REAL);

    usb_real = &usb->usb.ureal;

    usb_real->ioctl_buf = _vn_malloc_shared(16);
    if (usb_real->ioctl_buf == NULL) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_USB, 
                  "USB open: Cannot get memory for ioctl buffer\n");
        return _VN_ERR_NOMEM;        
    }

    /* Open USB device */
    _VN_TRACE(TRACE_FINE, _VN_SG_USB, "Opening USB device %s\n", USB_DEV);
    usb_real->devfd = IOS_Open(USB_DEV, 0);
    if (usb_real->devfd < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_USB, 
                  "USB open: IOS Error %d\n", usb_real->devfd);
        return _VN_ERR_USB;
    }
    
	/* Set device type to bulk */
    usb_real->ioctl_buf[0] = USB_DEV_MUX_BULK_ID;
    _VN_TRACE(TRACE_FINER, _VN_SG_USB, "Setting USB device to bulk\n");
    ios_rv  = IOS_Ioctl(usb_real->devfd, USB_SET_DEV_TYPE, 
                        usb_real->ioctl_buf, 1, NULL, 0);

    /* Okay if IOS_ERROR_EXISTS - Someone already set it to bulk */
    if ((ios_rv < 0) && (ios_rv != IOS_ERROR_EXISTS)) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_USB, 
                  "USB set bulk failed: IOS Error %d\n", rv);
        return _VN_ERR_USB;
    }

    /* Open USB device for VN */
    _VN_TRACE(TRACE_FINER, _VN_SG_USB, "Setting USB device for VN\n");
    usb_real->vnfd = IOS_Open(USB_DEV_MUX_BULK_VN, 0);
    if (usb_real->vnfd < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_USB, 
                  "USB open VN: IOS Error %d\n", usb_real->vnfd);
        return _VN_ERR_USB;
    }

    /* Close devfd (don't need it anymore) */
    IOS_Close(usb_real->devfd);
    usb_real->devfd = IOS_ERROR_INVALID;

    /* TODO: Abort if error */
    usb_real->async_rmq = IOS_CreateMessageQueue(
        usb_real->async_rmsgs, _VN_USB_MAX_ASYNC_RMSGS);
    if (usb_real->async_rmq < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_USB,
                  "IOS Error %d creating message queue for USB async read\n",
                  usb_real->async_rmq);
    }

    usb_real->async_wmq = IOS_CreateMessageQueue(
        usb_real->async_wmsgs, _VN_USB_MAX_ASYNC_WMSGS);

    if (usb_real->async_wmq < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_USB,
                  "IOS Error %d creating message queue for USB async write\n",
                  usb_real->async_wmq);
    }

    /* Allocate buffers for async reads */
    usb_real->rbuflen = _VN_USB_MAX_LEN;
    usb_real->nrbufs = _VN_USB_MAX_ASYNC_RMSGS;
    usb_real->active_rbuf = -1;
    for (i = 0; i < usb_real->nrbufs; i++) {
        usb_real->rbufs[i] = _vn_malloc_shared(usb_real->rbuflen);
    }

    for (i = 0; i < _VN_USB_MAX_ASYNC_RMSGS; i++) {
        usb_real->async_rrds[i] = _vn_malloc_shared(sizeof(IOSResourceRequest));
    }

    for (i = 0; i < _VN_USB_MAX_ASYNC_WMSGS; i++) {
        usb_real->async_wrds[i] = _vn_malloc_shared(sizeof(IOSResourceRequest));
    }

    return rv;
}

int _vn_check_usb_status_real(_vn_usb_t* usb)
{
    /* Checks USB status */
    _vn_usb_real_t* usb_real;
    IOSError ios_rv;
    assert(usb);
    assert(usb->mode == _VN_USB_MODE_REAL);

    usb_real = &usb->usb.ureal;

    _VN_TRACE(TRACE_FINER, _VN_SG_USB, "Checking USB status\n");

    _vn_mutex_lock(&usb->mutex);
    ios_rv  = IOS_Ioctl(usb_real->vnfd, USB_GET_DEV_STATUS, NULL, 0, 
                        usb_real->ioctl_buf, 1);
    if (ios_rv < 0) {
        _vn_mutex_unlock(&usb->mutex);
        _VN_TRACE(TRACE_ERROR, _VN_SG_USB,
                  "IOS Error %d checking USB status", ios_rv);
        return _VN_ERR_USB;
    }
    
    /* Convert from SC USB status codes to general USB codes */
    switch (usb_real->ioctl_buf[0]) {
    case USB_STATUS_NOT_OPEN:
        usb->state = _VN_USB_STATE_CLOSED;
        break;
    case USB_STATUS_UNPLUG:
        usb->state = _VN_USB_STATE_UNPLUG;
        break;
    case USB_STATUS_ENUM_DONE:
        usb->state = _VN_USB_STATE_ENUM_DONE;
        break;
    case USB_STATUS_ENUM:
        usb->state = _VN_USB_STATE_INIT;
        break;
    case USB_STATUS_READY:
        usb->state = _VN_USB_STATE_READY;
        break;
    }
    _vn_mutex_unlock(&usb->mutex);
    
    return _VN_ERR_OK;  
}

int _vn_shutdown_usb_real(_vn_usb_t* usb)
{
    _vn_usb_real_t* usb_real;
    int i;
    assert(usb);
    assert(usb->mode == _VN_USB_MODE_REAL);

    usb_real = &usb->usb.ureal;

    _VN_TRACE(TRACE_FINE, _VN_SG_USB, "Closing USB\n");
    if (usb_real->ioctl_buf) {
        _vn_free_shared(usb_real->ioctl_buf);
        usb_real->ioctl_buf = NULL;
    }
    if (usb_real->devfd >= 0) {
        IOS_Close(usb_real->devfd);
        usb_real->devfd = IOS_ERROR_INVALID;
    }
    if (usb_real->vnfd >= 0) {
        IOS_Close(usb_real->vnfd);
        usb_real->vnfd = IOS_ERROR_INVALID;
    }
    for (i = 0; i < _VN_USB_MAX_ASYNC_WMSGS; i++) {
        if (usb_real->async_wrds[i]) {
            _vn_free_shared(usb_real->async_wrds[i]);
            usb_real->async_wrds[i] = NULL;
        }
    }
    for (i = 0; i < _VN_USB_MAX_ASYNC_RMSGS; i++) {
        if (usb_real->async_rrds[i]) {
            _vn_free_shared(usb_real->async_rrds[i]);
            usb_real->async_rrds[i] = NULL;
        }
    }
    if (usb_real->async_rmq >= 0) {
        IOS_DestroyMessageQueue(usb_real->async_rmq);
        usb_real->async_rmq = IOS_ERROR_INVALID;
    }
    if (usb_real->async_wmq >= 0) {
        IOS_DestroyMessageQueue(usb_real->async_wmq);
        usb_real->async_wmq = IOS_ERROR_INVALID;
    }   
    for (i = 0; i < usb_real->nrbufs; i++) {
        if (usb_real->rbufs[i]) {
            _vn_free_shared(usb_real->rbufs[i]);
            usb_real->rbufs[i] = NULL;
        }
    }

    return _VN_ERR_OK;
}

/* USB read - using async read */

static int _vn_read_usb_real_raw_async(
    IOSFd fd, void* buf, size_t len,
    IOSMessageQueueId mq, IOSResourceRequest* rd)
{
    IOSError ios_rv;

    ios_rv = IOS_ReadAsync(fd, buf, len, mq, rd);

    if (ios_rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_USB, 
                  "USB async read failed for fd %d, mq %d "
                  "buf 0x%08x, len %d: IOS error %d\n",
                  fd, mq, buf, len, ios_rv);
        return _VN_ERR_USB;
    }

    return _VN_ERR_OK;
}


static int _vn_read_usb_real_raw_async_check(
    IOSFd fd, IOSMessageQueueId mq, IOSResourceRequest** prd)
{
    IOSMessage m;
    IOSError ios_rv;
    IOSResourceRequest* rd;

    assert(rd);
    _VN_TRACE(TRACE_FINEST, _VN_SG_USB, "USB wait read completion\n");

    ios_rv = IOS_ReceiveMessage(mq, &m, IOS_MESSAGE_BLOCK);
    if (ios_rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_USB, 
                  "USB async read recv message failed for fd %d, mq %d: "
                  "IOS error %d\n", fd, mq, ios_rv);
        return _VN_ERR_USB;
    }

    rd = (IOSResourceRequest*) m;
    ios_rv = rd->status;
    if (prd) { *prd = rd; }
    if (ios_rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_USB, 
                  "USB async read completed with failure for fd %d, mq %d: "
                  "IOS error %d\n",
                  fd, mq, ios_rv);
        return _VN_ERR_USB;
    }

    _VN_TRACE(TRACE_FINEST, _VN_SG_USB, 
              "USB async read got %d bytes\n", ios_rv);
    return ios_rv;
}

/* USB write - using async write*/
static int _vn_write_usb_real_raw_async(
    IOSFd fd, const void* buf, size_t len,
    IOSMessageQueueId mq, IOSResourceRequest* rd)
{
    IOSError ios_rv;

    ios_rv = IOS_WriteAsync(fd, (void*) buf, len, mq, rd);
    if (ios_rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_USB, 
                  "USB async write failed for fd %d, mq %d "
                  "buf 0x%08x, len %d: IOS error %d\n",
                  fd, mq, buf, len, ios_rv);

        return _VN_ERR_USB;
    }

    return _VN_ERR_OK;
}

static int _vn_write_usb_real_raw_async_check(
    IOSFd fd, const void* buf, size_t len,
    IOSMessageQueueId mq, IOSResourceRequest* rd)
{
    IOSMessage m;
    IOSError ios_rv;

    assert(rd);
    _VN_TRACE(TRACE_FINEST, _VN_SG_USB, 
              "USB wait write completion %d bytes, buf 0x%08x\n",
              len, buf);
    ios_rv = IOS_ReceiveMessage(mq, &m, IOS_MESSAGE_BLOCK);
    if (ios_rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_USB, 
                  "USB async write recv message failed for fd %d, mq %d "
                  "buf 0x%08x, len %d: IOS error %d\n",
                  fd, mq, buf, len, ios_rv);
        return _VN_ERR_USB;
    }

    if ((IOSMessage) rd != m) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_USB, 
                  "USB async write recv message differs from expected\n");
    }

    ios_rv = rd->status;
    if (ios_rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_USB, 
                  "USB async write completed with failure for fd %d, mq %d "
                  "buf 0x%08x, len %d: IOS error %d\n",
                  fd, mq, buf, len, ios_rv);
        return _VN_ERR_USB;
    }

    _VN_TRACE(TRACE_FINEST, _VN_SG_USB, 
              "USB async wrote %d bytes\n", ios_rv);

    return ios_rv;
}

int _vn_write_usb_ready(_vn_usb_real_t* usb_real)
{
    /* Empty write, indicating ready */           
    int wrv;
    _VN_TRACE(TRACE_FINER, _VN_SG_USB, "Sending USB read ready\n");
    
    wrv = _vn_write_usb_real_raw_async(usb_real->vnfd, NULL, 0,
                                       usb_real->async_wmq,
                                       usb_real->async_wrds[1]);
    
    if (wrv >= 0) {
        wrv = _vn_write_usb_real_raw_async_check(usb_real->vnfd, NULL, 0,
                                                 usb_real->async_wmq,
                                                 usb_real->async_wrds[1]);
    }

    return wrv;
}

/* Implements USB read used by VN */
int _vn_read_usb_real_buffered(_vn_usb_t* usb, uint32_t timeout)
{
    _vn_usb_real_t* usb_real;
    int rv = _VN_ERR_OK;
    int i, active;

    assert(usb);
    assert(usb->mode == _VN_USB_MODE_REAL);

    usb_real = &usb->usb.ureal;

    /* TODO: Wait for something to read */
    /* TODO: Take into account timeout */

    /* Lock around write, so no write is going on */
    _vn_mutex_lock(&usb->mutex);

    _vn_check_usb_status_real(usb);
    /* Wait for USB to be ready */
    if (usb->state != _VN_USB_STATE_READY) {
        if (usb->state == _VN_USB_STATE_UNPLUG || 
            usb->state == _VN_USB_STATE_CLOSED) {
            rv = _VN_ERR_CLOSED;
        } else {
            rv = _VN_ERR_NOTREADY;
        }
        _vn_mutex_unlock(&usb->mutex);
        _VN_TRACE(TRACE_ERROR, _VN_SG_USB,
                  "USB read aborted: USB not ready\n");
        return rv;
    }

    /* Async read */
    if (usb_real->active_rbuf < 0) {
        /* No data available - Set active buf to 0th buffer */
        active = 0;
    } else {
        active = usb_real->active_rbuf;
    }

    /* Try to submit available read bufs for async reads (in order) */
    i = active;
    while (usb_real->async_rqueued[i] == 0) {
        rv = _vn_read_usb_real_raw_async(usb_real->vnfd,
                                         usb_real->rbufs[i],
                                         usb_real->rbuflen,
                                         usb_real->async_rmq,
                                         usb_real->async_rrds[i]);
        if (rv >= 0) {
            usb_real->async_rqueued[i] = 1;
            _vn_write_usb_ready(usb_real);                    
        } else {
            break;
        }
        i = (i+1) % usb_real->nrbufs;
    }
  
        
    /* Set active to current index */
    active = i;

    _vn_mutex_unlock(&usb->mutex);    

    /* Wait for read on active buf (if there is one) to finish */
    if (usb_real->async_rqueued[active]) {
        IOSResourceRequest* rd;

        rv = _vn_read_usb_real_raw_async_check(usb_real->vnfd, 
                                               usb_real->async_rmq,
                                               &rd);
        
        if (rv >= 0) {
            if (rd == usb_real->async_rrds[active]) {
                usb_real->active_rbuf = active;
                usb_real->async_rqueued[active] = 0;
            } else {
                /* Find which one this is for */
                for (i = 0; i < usb_real->nrbufs; i++) {
                    if (rd == usb_real->async_rrds[i]) {
                        active = i;
                        usb_real->active_rbuf = active;
                        usb_real->async_rqueued[active] = 0;
                        break;
                    }
                        
                }
            }
        }
    } else {
        rv = _VN_ERR_USB;
    }

    return rv;
}

int _vn_read_usb_real(_vn_usb_t* usb, void* buf, size_t len, uint32_t timeout)
{
    _vn_usb_real_t* usb_real;
    int rv = _VN_ERR_OK;

    assert(usb);
    assert(usb->mode == _VN_USB_MODE_REAL);

    usb_real = &usb->usb.ureal;

    _VN_TRACE(TRACE_FINER, _VN_SG_USB, "USB Reading %d bytes, buf 0x%08x\n",
              len, buf);
    
    rv = _vn_read_usb_real_buffered(usb, timeout);

    if (buf != NULL) {
        if (rv >= 0) {
            rv = MIN(len, rv);
            memcpy(buf, usb_real->rbufs[usb_real->active_rbuf], rv);
        }
    }

    return rv;
}

/* Implements USB write used by VN */
int _vn_write_usb_real(_vn_usb_t* usb,
                       const void* buf, size_t len, uint32_t timeout)
{
    _vn_usb_real_t* usb_real;
    int rv;

    _VN_TRACE(TRACE_FINER, _VN_SG_USB, "USB Write\n");

    assert(usb);
    assert(usb->mode == _VN_USB_MODE_REAL);

    _VN_TRACE(TRACE_FINER, _VN_SG_USB, "USB Writing %d bytes, buf 0x%08x\n",
              len, buf);

    usb_real = &usb->usb.ureal;

    /* Lock around write, so no other write is going on */
    _vn_mutex_lock(&usb->mutex);
    _vn_check_usb_status_real(usb);
    if (usb->state != _VN_USB_STATE_READY) {
        if (usb->state == _VN_USB_STATE_UNPLUG || 
            usb->state == _VN_USB_STATE_CLOSED) {
            rv = _VN_ERR_CLOSED;
        } else {
            rv = _VN_ERR_NOTREADY;
        }
        _vn_mutex_unlock(&usb->mutex);
        _VN_TRACE(TRACE_ERROR, _VN_SG_USB,
                  "USB write aborted: USB not ready\n");
        return rv;
    }

    rv = _vn_write_usb_real_raw_async(usb_real->vnfd, buf, len,
                                      usb_real->async_wmq,
                                      usb_real->async_wrds[0]);
    
    if (rv >= 0) {
        rv = _vn_write_usb_real_raw_async_check(usb_real->vnfd, buf, len,
                                                usb_real->async_wmq,
                                                usb_real->async_wrds[0]);
    }

    _vn_mutex_unlock(&usb->mutex);
    return rv;
}

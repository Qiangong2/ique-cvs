//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnlocal.h"

_VN_time_t _vn_conv_timestamp(_VN_net_t net_id, _VN_host_t host_id,
                              _VN_time_t remote_timestamp)
{
    int64_t delta = _vn_get_clock_delta(net_id, host_id);
    return remote_timestamp + delta;
}

int _vn_calibrate_clock(_VN_net_t net_id, _VN_host_t host_id)
{
    _vn_net_info_t *net_info;
    _VN_host_t from_host;
    int rv;
    net_info = _vn_lookup_net_info(net_id);
    if (net_info != NULL) {
        /* Only do clock calibration for non-localhosts */
        if (!_vn_islocalhost(net_info, host_id)) {
            from_host = _vn_get_default_localhost(net_id);
            rv = _vn_send_clock_sync(_VN_MSG_ID_INVALID, 0,
                                     net_id, from_host, host_id);
        }
        else {
            rv = _VN_ERR_OK;
        }
    }
    else {
        rv = _VN_ERR_NETID;
    }
    return rv;
}

/* sendtime is the sendtime field in the VN packet header.
   recv_time is the packet receive time
   clock_delta is the corresponding clock difference in _vn_host_info. 
*/
_VN_time_t _vn_reconstruct_timestamp(uint32_t sendtime, _VN_time_t recv_time,
                                     int64_t clock_delta)
{
    _VN_time_t mask = 0xffffffffLL; /* max value of uint32_t, data type of
                                      sendtime */
    _VN_time_t t1 = (recv_time & ~mask) | ((sendtime + clock_delta) & mask);
    _VN_time_t t2 = t1 - (mask + 1);
    
    if (recv_time >= t1)
        return t1;
    else if (recv_time <= t2)
        return t2;
    else return ((t1 - recv_time) >= (recv_time -t2))? t2: t1;
}

int64_t _vn_get_clock_delta(_VN_net_t net_id, _VN_host_t host_id)
{
    _vn_net_info_t* net_info;
    _vn_host_info_t* host_info;
    int64_t cd = 0;
    net_info = _vn_lookup_net_info(net_id);
    if (net_info) { 
        host_info = _vn_lookup_host_info(net_info, host_id);
        if (host_info) {
            cd = host_info->clock_delta; 
        }
    }
    return cd;
}

int _vn_set_clock_delta(_VN_net_t net_id, _VN_host_t host_id, int64_t delta)
{
    _vn_net_info_t* net_info;
    _vn_host_info_t* host_info;
    net_info = _vn_lookup_net_info(net_id);
    if (net_info) { 
        host_info = _vn_lookup_host_info(net_info, host_id);
        if (host_info) {
            host_info->clock_delta = delta;
            return _VN_ERR_OK;
        }
        else {
            return _VN_ERR_HOSTID;
        }
    }
    else {
        return _VN_ERR_NETID;
    }    
}

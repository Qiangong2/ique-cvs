//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#ifndef __VN_DEVICES_H__
#define __VN_DEVICES_H__

#include "vnhash.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Device definitions */

#define _VN_DEVICE_RPC_MASK    0x80000000
#define _VN_DEVICE_UPNP_MASK   0x40000000

#define _VN_DEVICE_KIND_MASK   0x0f000000
#define _VN_DEVICE_CLIENT      0x01000000
#define _VN_DEVICE_SERVER      0x02000000
#define _VN_DEVICE_PROXY       0x03000000

#define _VN_DEVICE_TYPE_MASK   0x000000ff

#define _VN_DEVICE_IS_RPC(type)                                  \
    ((type != _VN_DEVICE_TYPE_UNKNOWN) &&                        \
     ((type) & _VN_DEVICE_RPC_MASK))

#define _VN_DEVICE_IS_UPNP(type)                                 \
    ((type != _VN_DEVICE_TYPE_UNKNOWN) &&                        \
     ((type) & _VN_DEVICE_UPNP_MASK))

#define _VN_DEVICE_IS_SERVER(type)                               \
    ((type != _VN_DEVICE_TYPE_UNKNOWN) &&                        \
     (((type) & _VN_DEVICE_TYPE_MASK) == _VN_DEVICE_SERVER))
#define _VN_DEVICE_IS_CLIENT(type)                               \
    ((type != _VN_DEVICE_TYPE_UNKNOWN) &&                        \
     (((type) & _VN_DEVICE_TYPE_MASK) == _VN_DEVICE_CLIENT))     
#define _VN_DEVICE_IS_PROXY(type)                                \
    ((type != _VN_DEVICE_TYPE_UNKNOWN) &&                        \
     (((type) & _VN_DEVICE_TYPE_MASK) == _VN_DEVICE_PROXY))

#ifdef _LINUX
#define _VN_DEVICE_TYPE         _VN_DEVICE_PC
#endif

#ifdef _WIN32
#define _VN_DEVICE_TYPE         _VN_DEVICE_PC
#endif

#ifdef _SC
#define _VN_DEVICE_TYPE         _VN_DEVICE_NC
#endif

/* Data structures for the device table */

#define _VN_DEVICE_TABLE_SIZE   16

/* Socket/Connection information */
#define _VN_INADDR_LOCAL     0x01     /* local ip address (within fw) */
#define _VN_INADDR_EXTERNAL  0x02     /* external ip address (from server) */

#define _VN_INADDR_MAX       0xff     /* Maximum number of inaddrs */

typedef struct {
    uint8_t   flags;       /* What do I know about this address? */
    uint8_t   family;      /* Address type (should be IPv4) */
    _vn_inport_t port;     /* Port number */
    _vn_inaddr_t addr;     /* Address */
} _vn_sockaddr_t;

/* Service */
#define _VN_SERVICE_PROP_PUBLIC  0x01  /* service is public to other devices */

#define _VN_SERVICE_IS_PUBLIC(serv)    (serv->flags & _VN_SERVICE_PROP_PUBLIC)

typedef struct {
    uint32_t     service_id;
    const void*  msg;
    uint16_t     msglen;
    uint8_t      flags;
} _vn_service_t;

/* Device */
#define _VN_DEVICE_IS_LOCAL(device)    (device->flags & _VN_DEVICE_PROP_LOCAL)
#define _VN_DEVICE_IS_LAN(device)      (device->flags & _VN_DEVICE_PROP_LAN)
#define _VN_DEVICE_IS_QUERIED(device)  (device->flags & _VN_DEVICE_PROP_QUERIED)
#define _VN_DEVICE_IS_ADHOC(device)    (device->flags & _VN_DEVICE_PROP_ADHOC)

#ifdef _VN_RPC_PROXY
#define _VN_CLIENT_TO_FLAG(client)     ((client)? (1 << ((client)-1)): 0)
#endif

typedef struct {
    /* Device information (i.e. device type, etc) */
    _VN_guid_t     guid;        /* Unique device id */
#ifndef _VN_RPC_PROXY
    _VN_addr_t     vnaddr;      /* Default VN addr for this device */
#else
    uint8_t        clients;     /* Bitmask of clients interested in device */
                                /* Used to determine when to timeout
                                   a non-adhoc, remote device */
#endif

    uint8_t        flags;       /* Bitmask of device properties */
    
#ifndef _VN_RPC_PROXY
    /* TODO: Do we need to keep track of the active handshake sessions
             and VN addrs for this devices? */
    _vn_dlist_t*   all_vnaddrs; /* List of all VN addrs for this device */
#endif

    _vn_dlist_t*   inaddrs;     /* List of internet address for this device */
    _vn_dlist_t*   services;    /* List of services offered by this device */

    _vn_timer_t*   timer;       /* Timer for how long the entry is valid */
} _vn_device_info_t;

/* Function prototypes */

_vn_ht_table_t* _vn_get_device_table();
 
/* Socket/Connection Information */
int _vn_device_lookup_connect_info(_VN_guid_t guid, _VN_net_t net_id, 
                                   _VN_addr_t* addr,
                                   _vn_inaddr_t* ip, _vn_inport_t* port);
int _vn_device_lookup_connect_info_list(_VN_guid_t guid, _VN_net_t net_id, 
                                        _VN_addr_t* addr,
                                        _vn_dlist_t** list);
int _vn_device_add_connect_info(_vn_device_info_t* device, uint8_t flags,
                                _vn_inaddr_t addr, _vn_inport_t port);

/* Services */
int _vn_get_service_count(_VN_guid_t guid);
int _vn_get_service_ids(_VN_guid_t guid, uint32_t* services, int nservices);
_vn_service_t* _vn_lookup_service(_VN_guid_t guid, uint32_t service_id);
int _vn_add_service(_VN_guid_t guid, uint32_t service_id, uint8_t flags,
                    const void* msg, uint16_t msglen);
int _vn_remove_service(_VN_guid_t guid, uint32_t service_id);
int _vn_remove_services(_VN_guid_t guid, 
                        uint32_t services[], uint32_t nservices);
int _vn_clear_services(uint8_t match_mask, uint8_t filter_mask);
int _vn_clear_device_flag(uint8_t flag);

_vn_service_t* _vn_device_get_service(_vn_device_info_t* device,
                                      uint32_t service_id);
int _vn_device_get_service_count(_vn_device_info_t *device);
int _vn_device_get_service_ids(_vn_device_info_t* device,
                               uint32_t* services, int nservices);

/* Device information */
int _vn_get_device_count(uint8_t mask);
int _vn_get_guids(uint8_t mask, _VN_guid_t* devices, uint16_t ndevices);

int _vn_add_device(_VN_guid_t guid, uint8_t flags);
int _vn_remove_device(_VN_guid_t guid, uint8_t reason);
int _vn_disconnect_device(_VN_guid_t guid);

int _vn_get_guid(_VN_addr_t addr, _VN_guid_t* guid);

int _vn_device_set_timeout(_vn_device_info_t* device, uint32_t msecs);

int _vn_init_device_table();
int _vn_clear_device_table();
int _vn_destroy_device_table();

_vn_device_info_t* _vn_lookup_device(_VN_guid_t guid);

#ifndef _VN_RPC_PROXY
int _vn_device_add_local();
int _vn_device_set_vnaddr(_VN_guid_t guid, _VN_addr_t vnaddr);
int _vn_device_add_vnaddr(_VN_guid_t guid, _VN_addr_t vnaddr);
int _vn_device_remove_vnaddr(_VN_guid_t guid, _VN_addr_t vnaddr);
int _vn_devices_remove_net(_VN_net_t net_id);
int _vn_device_disconnect_nets(_vn_device_info_t *device, uint8_t reason);
#else
int _vn_device_set_client(_VN_guid_t guid, int client);
int _vn_device_clear_client(_VN_guid_t guid, int client, bool remove_device);
int _vn_devices_clear_client(int client, bool remove_device);
#endif /* _VN_RPC_PROXY */

int _vn_device_encode_service(_vn_service_t* service, uint8_t* buf, size_t buflen);
int _vn_device_encode_service_list(_vn_device_info_t *device, uint8_t mask,
                                   uint8_t* buf, size_t buflen);
int _vn_device_encode_inaddrs(_vn_device_info_t* device,
                              uint8_t* buf, size_t buflen);
int _vn_device_encode_device(_vn_device_info_t* device,
                             uint8_t* buf, size_t buflen);

int _vn_device_decode_service(_vn_service_t* service,
                              uint8_t* buf, size_t buflen);
int _vn_device_decode_service_list(_vn_device_info_t *device,
                                   uint8_t* buf, size_t buflen,
                                   uint32_t** pServices,
                                   uint32_t* pServicesCount);
int _vn_device_decode_inaddrs(_vn_device_info_t* device,
                              uint8_t* buf, size_t buflen);
int _vn_device_decode_device(_vn_device_info_t* device,
                             uint8_t* buf, size_t buflen);

int _vn_add_encoded_service(_VN_guid_t guid, uint8_t* buf, size_t buflen,
                            uint32_t* pService);
int _vn_add_encoded_device(uint8_t* buf, size_t buflen, int* decoded_len,
                           _VN_guid_t* pDeviceId, uint8_t* pReason);

int _vn_device_encode_device_list(_VN_guid_t* guids, int nguids,
                                  uint8_t* buf, size_t buflen);
int _vn_device_decode_device_list(uint8_t* buf, size_t buflen,
                                  _VN_guid_t** pDevices, uint8_t** pReasons,
                                  int* pDevicesCount);

/* Guid variables and functions */
void _vn_init_myguid();
_vn_ht_hash_t _vn_ht_hash_guid(_vn_ht_key_t key);
int _vn_ht_key_comp_guid(_vn_ht_key_t key1, _vn_ht_key_t key2);
_VN_guid_t _vn_get_myguid();
_VN_chip_id_t _vn_get_my_chip_id();
void _vn_set_my_chip_id(_VN_chip_id_t chip_id);
_VN_device_type_t _vn_get_my_device_type();
int _vn_guid_eq(_VN_guid_t *pGuid1, _VN_guid_t *pGuid2);
int _vn_guid_match(_VN_guid_t *pGuid1, _VN_guid_t *pGuid2);
bool _vn_guid_is_invalid(_VN_guid_t *pGuid);
bool _vn_guid_is_self(_VN_guid_t *pGuid);
void _vn_guid_set_invalid(_VN_guid_t *pGuid);

#ifdef  __cplusplus
}
#endif
 
#endif /* __VN_DEVICES_H__ */

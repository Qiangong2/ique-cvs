//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnlocal.h"

#if _VN_USE_IOSC

#include "iosccert.h"

#ifdef _SC
#include <fs.h>
#define _VN_CERT_SYS_FILE  "/sys/cert.sys"
#else
#define _VN_CERT_SYS_FILE  "pccert_bpki.sys"

#define _VN_CERT_DIR       "/usr/etc/pki_data"
#define _VN_CERT_ENV       "VN_CERT_SYS_FILE"
#define _VN_CERT_DIR_ENV   "VN_CERT_DIR"
#define _VN_ROOT_DIR_ENV   "ROOT"
#endif

#if _VN_IOSC_ALIGN_NEEDED
static IOSCEccSignedCert _vn_iosc_devcert __attribute__((aligned(_VN_CERT_ALIGNMENT)));
#else
static IOSCEccSignedCert _vn_iosc_devcert; 
#endif

_vn_cert_t _vn_devcert;

typedef struct {
    IOSCName certName;
    IOSCPublicKeyHandle hPubKey;
} _vn_cert_key_t;

#define _VN_MAX_CERT_KEYS                  8
static _vn_cert_key_t _vn_cert_keys[_VN_MAX_CERT_KEYS];

_vn_cert_key_t* _vn_lookup_cert_key(const IOSCName name)
{
    int i;
    for (i = 0; i < _VN_MAX_CERT_KEYS; i++) {
        _vn_cert_key_t* key = &_vn_cert_keys[i];
        if (_VN_KEY_HANDLE_IS_VALID(key->hPubKey)) {
            if (strncmp(key->certName, name, sizeof(IOSCName)) == 0) {
                return key;
            }
        }
    }
    return NULL;
}

int _vn_add_cert_key(const IOSCName name, IOSCPublicKeyHandle hPubKey)
{
    int i, avail = -1;
    for (i = 0; i < _VN_MAX_CERT_KEYS; i++) {
        _vn_cert_key_t* key = &_vn_cert_keys[i];
        if (_VN_KEY_HANDLE_IS_VALID(key->hPubKey)) {
            if (strncmp(key->certName, name, sizeof(IOSCName)) == 0) {
                return _VN_ERR_DUPENTRY;
            }
        } else { 
            if (avail < 0) {
                avail = i;
            }
        }
    }

    if (avail >= 0) {
        _vn_cert_keys[avail].hPubKey = hPubKey;
        memcpy(_vn_cert_keys[avail].certName, name, sizeof(IOSCName));
        return _VN_ERR_OK;
    } else {
        return _VN_ERR_NOSPACE;
    }
} 

int _vn_init_cert_keys()
{
    int i;
    memset(_vn_cert_keys, 0, sizeof(_vn_cert_keys));
    for (i = 0; i < _VN_MAX_CERT_KEYS; i++) {
        _vn_cert_key_t* key = &_vn_cert_keys[i];
        key->hPubKey = _VN_KEY_HANDLE_INVALID;
    }
    return _VN_ERR_OK;
}

/* Remove all cert keys */
int _vn_clear_cert_keys()
{
    int i;
    for (i = 0; i < _VN_MAX_CERT_KEYS; i++) {
        _vn_cert_key_t* key = &_vn_cert_keys[i];
        if (_VN_KEY_HANDLE_IS_VALID(key->hPubKey)) {
            IOSC_DeleteObject(key->hPubKey);
            memset(key->certName, 0, sizeof(IOSCName));
            key->hPubKey = _VN_KEY_HANDLE_INVALID;
        }
    }
    return _VN_ERR_OK;
}

/* Security related functions for the VN handshake */

/* TODO: Eliminate */
#define _VN_HS_CHECK_CERT                   1
#define _VN_HS_CHECK_GUID                   0

/* Converts hex string to integer */
int _vn_hex2int(const char* s, size_t len, uint32_t *value)
{
    uint32_t hexValue;
    *value = 0;
    while (*s && len > 0) {
        if (*s>='0' && *s<='9') {
            hexValue = *s - '0';
        } else if (*s>='A' && *s<='F') {
            hexValue = *s - 'A' + 10;
        } else if (*s>='a' && *s<='f') {
            hexValue = *s - 'a' + 10;
        } else {
            return _VN_ERR_INVALID;
        }
        (*value) = ((*value) << 4) | hexValue;
        s++;
        len--;
    }
    return _VN_ERR_OK;
}

/* Converts device id in cert to guid (returns VN error code) */
int _vn_cert_devid2guid(IOSCDeviceId device_id, _VN_guid_t *pGuid)
{
    int rv, i;
    _VN_guid_t guid;
    assert(device_id);
    assert(pGuid);    
    
    /* Check to see if NC, RV, etc */
    if (device_id[0] == 'N' && device_id[1] == 'C') {
        guid.device_type = _VN_DEVICE_NC;
        i = 2;
    } else if (device_id[0] == 'V' && device_id[1] == 'S') {
        guid.device_type = _VN_DEVICE_VS;
        i = 2;
    } else if (device_id[0] == 'P' && device_id[1] == 'C') {
        guid.device_type = _VN_DEVICE_PC;
        i = 2;
    } else if (device_id[0] == 'R' && device_id[1] == 'V') {
        guid.device_type = _VN_DEVICE_RV;
        i = 2;
    } else if (device_id[0] == 'B' && device_id[1] == 'B') {
        guid.device_type = _VN_DEVICE_BB;
        i = 2;
    } else {
        return _VN_ERR_INVALID;
    }

    rv = _vn_hex2int(device_id + i, sizeof(IOSCDeviceId) - i, &guid.chip_id);
    if (rv >= 0 && pGuid) {
        pGuid->device_type = guid.device_type;
        pGuid->chip_id = guid.chip_id;
    }
     
    return rv;
}

/* Extract guid from cert (return VN error code) */
/* Requires cert be a ECC signed cert */
int _vn_get_cert_guid(uint8_t* certData, size_t certLen, _VN_guid_t *pGuid)
{
    int rv;
    IOSCEccEccCert* peccecc;
    IOSCCertSigType sigType;
    
    assert(certData);
    assert(pGuid);

    if (certLen < sizeof(IOSCEccEccCert)) {
        return _VN_ERR_INVALID;
    }

    peccecc = (IOSCEccEccCert *)certData;
    sigType = ntohl((peccecc->sig).sigType); 
    
    switch (sigType) {
    case IOSC_SIG_ECC:
        rv = _vn_cert_devid2guid(peccecc->head.name.deviceId, pGuid);
        break;
    default:
        rv = _VN_ERR_INVALID;
        break;
    }
    
    return rv;
}

/**
 * Imports a public key from a cert without verifying the cert.
 * Used when we don't care if the cert is valid or not.
 *
 * @param certData  Pointer to cert (should be aligned)
 * @param certLen   Size of cert
 * @param signerKey Public key of the signer (used in verifying the cert)
 * @param pCertKey  Pointer to handle in which the public key of the cert 
 *                  is to be stored
 * @returns IOSC error code
 */
IOSCError _vn_iosc_import_cert_public_key(uint8_t* certData, size_t certLen,
                                          IOSCPublicKeyHandle* pCertKey)
{
    IOSCError ios_rv = IOSC_ERROR_OK;
    IOSCRsa2048EccCert* prsa2ecc;
    IOSCRsa4096RsaCert* prsa4rsa;
    IOSCRsa2048RsaCert* prsa2rsa;
    IOSCEccEccCert* peccecc;
    IOSCCertSigType sigType;
    IOSCCertPubKeyType pubKeyType;
    IOSCPublicKeyHandle pubKey = _VN_KEY_HANDLE_INVALID;
    
    prsa2ecc =  (IOSCRsa2048EccCert *)certData;
    peccecc = (IOSCEccEccCert *)certData;
    prsa4rsa = (IOSCRsa4096RsaCert *) certData;
    prsa2rsa = (IOSCRsa2048RsaCert *) certData;
    sigType = ntohl((prsa2ecc->sig).sigType); 

    switch (sigType) {
    case IOSC_SIG_RSA4096:
        ios_rv = IOSC_CreateObject(&pubKey, 
                                   IOSC_PUBLICKEY_TYPE, 
                                   IOSC_RSA4096_SUBTYPE);
        if (ios_rv == IOS_ERROR_OK) {
            ios_rv = IOSC_ImportPublicKey (prsa4rsa->pubKey, 
                                           prsa4rsa->exponent, pubKey);
        }
        break;
    case IOSC_SIG_RSA2048:
        pubKeyType = ntohl((prsa2ecc->head).pubKeyType); 
        switch (pubKeyType) {
        case IOSC_PUBKEY_RSA2048:
            ios_rv = IOSC_CreateObject(&pubKey, 
                                       IOSC_PUBLICKEY_TYPE, 
                                       IOSC_RSA2048_SUBTYPE);
            if (ios_rv == IOS_ERROR_OK) {
                ios_rv = IOSC_ImportPublicKey (prsa2rsa->pubKey, 
                                               prsa2rsa->exponent, pubKey);
            }
            break;
        case IOSC_PUBKEY_ECC:
            ios_rv = IOSC_CreateObject(&pubKey, 
                                       IOSC_PUBLICKEY_TYPE, 
                                       IOSC_ECC233_SUBTYPE);
            if (ios_rv == IOS_ERROR_OK) {
                ios_rv = IOSC_ImportPublicKey (prsa2ecc->pubKey, 0, pubKey);
            }
            break;
        default:
            break;
        }
        break;
    case IOSC_SIG_ECC:
        ios_rv = IOSC_CreateObject(&pubKey, 
                                   IOSC_PUBLICKEY_TYPE, 
                                   IOSC_ECC233_SUBTYPE);
        if (ios_rv == IOS_ERROR_OK) {
            ios_rv = IOSC_ImportPublicKey (peccecc->pubKey, 0, pubKey);
        }
        break;
    default:
        ios_rv = IOSC_ERROR_INVALID_FORMAT;
    }
    
    if (ios_rv == IOS_ERROR_OK) {
        if (pCertKey) { *pCertKey = pubKey; }
    } else {
        if (_VN_KEY_HANDLE_IS_VALID(pubKey)) {
            IOSC_DeleteObject(pubKey);
        }
    }

    return ios_rv;
}

/**
 * Returns the expected size of the cert
 */
int _vn_get_cert_size(void* cert, int32_t certSize)
{ 
    IOSCCertSigType sigType;
    IOSCCertPubKeyType pubKeyType;

    if (certSize < sizeof(IOSCCertSigType)) {
        return _VN_ERR_INVALID;
    }

    sigType = ntohl((((IOSCRsa2048EccCert *) cert)->sig).sigType);
    switch (sigType) {
    case IOSC_SIG_RSA4096:
        return sizeof(IOSCRsa4096RsaCert);
    case IOSC_SIG_RSA2048:
        pubKeyType = ntohl((((IOSCRsa2048EccCert *) cert)->head).pubKeyType); 
        switch (pubKeyType) {
        case IOSC_PUBKEY_RSA2048:
            return sizeof(IOSCRsa2048RsaCert);
        case IOSC_PUBKEY_ECC:
            return sizeof(IOSCRsa2048EccCert);
        default:
            return _VN_ERR_INVALID;
        }
        break;
    case IOSC_SIG_ECC:
        return sizeof(IOSCEccEccCert);
    default:
        return _VN_ERR_INVALID;
    }
}

/**
 * Returns the IOSCObjectSubType for the public key 
 */
int _vn_get_pubkey_subtype(void* cert, int32_t certSize)
{ 
    IOSCCertSigType sigType;
    IOSCCertPubKeyType pubKeyType;

    if (certSize < sizeof(IOSCCertSigType)) {
        return _VN_ERR_INVALID;
    }

    sigType = ntohl((((IOSCRsa2048EccCert *) cert)->sig).sigType);
    switch (sigType) {
    case IOSC_SIG_RSA4096:
        pubKeyType = ntohl((((IOSCRsa4096RsaCert *) cert)->head).pubKeyType); 
        break;
    case IOSC_SIG_RSA2048:
        pubKeyType = ntohl((((IOSCRsa2048EccCert *) cert)->head).pubKeyType); 
        break;
    case IOSC_SIG_ECC:
        pubKeyType = ntohl((((IOSCEccEccCert *) cert)->head).pubKeyType); 
        break;
    default:
        return _VN_ERR_INVALID;
    }

    switch (pubKeyType) {
    case IOSC_PUBKEY_RSA4096:
        return IOSC_RSA4096_SUBTYPE;
    case IOSC_PUBKEY_RSA2048:
        return IOSC_RSA2048_SUBTYPE;
    case IOSC_PUBKEY_ECC:
        return IOSC_ECC233_SUBTYPE;
    default: 
        return _VN_ERR_INVALID;
    }
}


/**
 * Finds and returns the cert corresponding to id
 * @param id - Null terminated string of the form "issuer-deviceid"
 * @param certList - List of certs to search
 * @param certListSize - Size of certList
 * @param cert - Where the pointer to the cert should be stored
 * @param certSize - Where the certSize should be stored
 * @param certIssuer - Where the pointer to the cert issuer should be stored
 * @returns VN error code: _VN_ERR_NOTFOUND if cert with id is not found
 *                         _VN_ERR_INVALID if a cert in certList has
 *                                         invalid format.
 *                         _VN_ERR_OK if the cert with id is found
 */
int _vn_find_cert(const char* id, const void* certList, u32 certListSize, 
                  void** cert, u32* certSize, char** certIssuer)
{
    int rv = _VN_ERR_NOTFOUND;
    u8* ptr = (u8*)certList;
    IOSCCertSigType sigType;
    IOSCCertPubKeyType keyType;
    char* pIssuer;
    char* pCert;
    IOSCName name;

    while (ptr<((u8*)certList+certListSize)) {

        sigType = ntohl(((IOSCSigRsa4096*)ptr)->sigType);
        switch (sigType) {
            case IOSC_SIG_RSA4096:
                pIssuer = ((IOSCSigRsa4096*)ptr)->issuer;
                pCert = ((IOSCRsa4096RsaCert*)ptr)->head.name.serverId;
                *certSize = sizeof(IOSCRsa4096RsaCert);
                break;
            case IOSC_SIG_RSA2048:
                pIssuer = ((IOSCSigRsa2048*)ptr)->issuer;
                keyType = ntohl(((IOSCRsa2048RsaCert*)ptr)->head.pubKeyType);
                switch (keyType) {
                    case IOSC_PUBKEY_RSA2048:
                        pCert =  ((IOSCRsa2048RsaCert*)ptr)->head.name.serverId;
                        *certSize = sizeof(IOSCRsa2048RsaCert);
                        break;
                    case IOSC_PUBKEY_ECC:
                        pCert =  ((IOSCRsa2048EccCert*)ptr)->head.name.serverId;
                        *certSize = sizeof(IOSCRsa2048EccCert);
                        break;
                    default:
                        rv = _VN_ERR_INVALID;
                        goto out;
                }
                break;
            case IOSC_SIG_ECC:
                pIssuer = ((IOSCSigEcc*)ptr)->issuer;
                pCert = ((IOSCEccEccCert*)ptr)->head.name.deviceId;
                *certSize = sizeof(IOSCEccEccCert);
                break;
            default:
                rv = _VN_ERR_INVALID;
                goto out;
        }
        
        snprintf(name, sizeof(IOSCName), "%s-%s", pIssuer, pCert);
        if (!strncmp(id, name, sizeof(IOSCName))) {
            *cert = ptr;
            *certIssuer = pIssuer;
            rv = _VN_ERR_OK;
            break;
        }

        ptr += (*certSize);
    }

out:
    if (rv!=_VN_ERR_OK) {
        *certSize = 0;
        *cert = NULL;
        *certIssuer = NULL;
    }
    return rv;
}

int _vn_get_issuer_key(const char* issuer,
                       const void* certList, u32 certSizeInBytes,
                       IOSCPublicKeyHandle *pIssuerPubKey, 
                       void** pCertChain, uint32_t *pCertChainLen)
{
    IOSCError ios_rv;
    int rv = _VN_ERR_OK;
    char* caId;
    char* rootId;
    void* caCert = NULL;
    void* issuerCert = NULL;
    u32 issuerCertSize = 0;
    u32 caCertSize = 0;

    IOSCPublicKeyHandle hCaPubKey = _VN_KEY_HANDLE_INVALID;
    IOSCPublicKeyHandle hIssuerPubKey = _VN_KEY_HANDLE_INVALID;
    IOSCObjectSubType caPubKeyType, issuerPubKeyType;

    assert(pIssuerPubKey);

    /* search issuer cert */
    rv = _vn_find_cert(issuer, certList, certSizeInBytes,
                       (void**)&issuerCert, &issuerCertSize, &caId);
    if (rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC,
                  "Cannot find issuer cert rv=%d, issuer %s\n",
                  rv, issuer);
        goto out;
    }

    issuerPubKeyType = _vn_get_pubkey_subtype(issuerCert, issuerCertSize);
    if (issuerPubKeyType < 0) {
        rv = issuerPubKeyType;
        _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC,
                  "Error getting issuer public key type rv=%d, issuer %s\n",
                  rv, issuer);
        goto out;
    }

    /* search CA cert */ 
    rv = _vn_find_cert(caId, certList, certSizeInBytes, 
                       (void**)&caCert, &caCertSize, &rootId);
    if (rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC,
                  "Cannot find CA cert rv=%d, caId %s\n", rv, caId);
        goto out;
    }

    caPubKeyType = _vn_get_pubkey_subtype(caCert, caCertSize);
    if (caPubKeyType < 0) {
        rv = caPubKeyType;
        _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC,
                  "Error getting CA public key type rv=%d, caId %s\n",
                  rv, caId);
        goto out;
    }

    /* verify CA cert */
    rv = _VN_ERR_SEC;   
    ios_rv = IOSC_CreateObject(&hCaPubKey,
                               IOSC_PUBLICKEY_TYPE, caPubKeyType);
    if (ios_rv != IOS_ERROR_OK) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC, 
                  "Create CA pubKey (subtype %d) handle " 
                  "failed with IOS error %d\n", 
                  caPubKeyType, ios_rv);
        goto out;
    }

    ios_rv = IOSC_ImportCertificate((void*)caCert, IOSC_ROOT_KEY_HANDLE, hCaPubKey);
    if (ios_rv != IOS_ERROR_OK) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC,
                  "Verify CA pubKey (subtype %d) handle "
                  "failed with IOS error %d\n",
                  caPubKeyType, ios_rv);
        goto out;
    }

    _VN_TRACE(TRACE_FINE, _VN_SG_IOSC, "Verified CA cert %s\n", caId);

    /* verify issuer cert */
    ios_rv = IOSC_CreateObject(&hIssuerPubKey,
                               IOSC_PUBLICKEY_TYPE, issuerPubKeyType);
    if (ios_rv != IOS_ERROR_OK) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC,
                  "Create issuer pubKey (subtype %d) handle "
                  "failed with IOS error %d\n",
                  issuerPubKeyType, ios_rv);
        goto out;
    }

    ios_rv = IOSC_ImportCertificate((void*)issuerCert, hCaPubKey, hIssuerPubKey);
    if (ios_rv != IOS_ERROR_OK) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC,
                  "Verify issuer pubKey (subtype %d) handle "
                  "failed with IOS error %d\n",
                  issuerPubKeyType, ios_rv);
        goto out;
    }

    _VN_TRACE(TRACE_FINE, _VN_SG_IOSC, "Verified issuer cert %s\n", issuer);
    rv = _VN_ERR_OK;

    /* Save cert chain for future use */
    if (pCertChain && pCertChainLen) {
        *pCertChainLen = caCertSize + issuerCertSize;
        *pCertChain = _vn_malloc_aligned(*pCertChainLen, _VN_CERT_ALIGNMENT);
        if (*pCertChain) {
            memcpy(*pCertChain, issuerCert, issuerCertSize);
            memcpy( (uint8_t*) *pCertChain + issuerCertSize, 
                    caCert, caCertSize);
        }
    }
out:
    if (rv == _VN_ERR_OK) {
        *pIssuerPubKey = hIssuerPubKey;
    } else {
        if (_VN_KEY_HANDLE_IS_VALID(hIssuerPubKey)) {
            IOSC_DeleteObject(hIssuerPubKey);
        }
    }

    if (_VN_KEY_HANDLE_IS_VALID(hCaPubKey)) {
        IOSC_DeleteObject(hCaPubKey);
    }

    return rv;
}

/**
 * Verifies a cert, creating and importing the public key from the cert.
 * Aligns the cert (by copying it) if needed 
 *
 * @param certData  Pointer to cert
 * @param certLen   Size of cert
 * @param signerKey Public key of the signer (used in verifying the cert)
 * @param pCertKey  Pointer to handle in which the public key of the cert 
 *                  is to be stored
 */
int _vn_import_cert(uint8_t* certData, size_t certLen,
                    IOSCPublicKeyHandle signerKey,
                    IOSCPublicKeyHandle* pCertKey)
{
    IOSCError ios_rv;
    uint8_t *cert = certData;
    int rv = _VN_ERR_OK; 
    IOSCPublicKeyHandle pubkeyHandle = _VN_KEY_HANDLE_INVALID;

    assert(cert);
    assert(pCertKey);
    assert(_VN_KEY_HANDLE_IS_INVALID(*pCertKey));

#if _VN_IOSC_ALIGN_NEEDED
    if ((uint32_t) cert & (_VN_CERT_ALIGNMENT-1)) {
        cert = _vn_malloc_aligned(certLen, _VN_CERT_ALIGNMENT);
        if (cert) {
            memcpy(cert, certData, certLen);
        } else {
            _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC,
                      "Error verifying cert: out of memory\n");
            rv = _VN_ERR_NOMEM;
            goto out;
        }
    }
#endif

    /* Check cert is a valid cert */
    ios_rv = IOSC_CreateObject(&pubkeyHandle, 
                               IOSC_PUBLICKEY_TYPE, IOSC_ECC233_SUBTYPE);
    if (ios_rv < 0) {
        _VN_TRACE(TRACE_WARN, _VN_SG_IOSC,
                  "IOS error %d creating cert key\n", ios_rv);
        rv = _VN_ERR_SEC;
        goto out;
    }

    ios_rv = IOSC_ImportCertificate(cert, signerKey, pubkeyHandle);
    if (ios_rv < 0) {
        _VN_TRACE(TRACE_WARN, _VN_SG_IOSC,
                  "IOS error %d importing cert\n", ios_rv);
        rv = _VN_ERR_SEC;
        goto out;
    }

    _VN_TRACE(TRACE_FINE, _VN_SG_IOSC, "Verified ECC cert\n");
out:
    if (rv == _VN_ERR_OK) {
        *pCertKey = pubkeyHandle;
    } else {
        if (_VN_KEY_HANDLE_IS_VALID(pubkeyHandle)) {
            IOSC_DeleteObject(pubkeyHandle);
        }
    }

#if _VN_IOSC_ALIGN_NEEDED
    if (cert && cert != certData) {
        _vn_free_aligned(cert);
    }
#endif
    return rv;
}

#ifdef _SC
/** 
 * Read in list of certs from a file (SC)
 */
int _vn_read_certs(const char* cert_filename, uint8_t** pCerts, 
                   uint32_t* pCertSize)
{
    ISFSFileStats* stats=NULL;
    uint8_t* certs = NULL;
    uint32_t certSize = 0;
    IOSFd fd = IOS_ERROR_INVALID;
    int rv;

    stats = IOS_Alloc(0, sizeof(ISFSFileStats));
    if (stats == NULL) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC, "Cannot allocate %d bytes for stats\n",
                  sizeof(ISFSFileStats));
        rv = _VN_ERR_NOMEM;
        goto out;
    }

    fd = ISFS_Open(cert_filename, ISFS_READ_ACCESS);
    if (fd < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC, "IOS error %d opening file %s\n",
                  fd, cert_filename);
        rv = _VN_ERR_FS;
        goto out;
    }

    rv = ISFS_GetFileStats(fd, stats);
    if (rv != ISFS_ERROR_OK) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC,
                  "IOS error %d getting stats for file %s\n",
                  rv, cert_filename);
        rv = _VN_ERR_FS;
        goto out;
    }

    certSize = stats->size;
    certs = IOS_AllocAligned(0, certSize, _VN_CERT_ALIGNMENT);
    if (certs == NULL) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC, "Cannot allocate %d bytes for certs\n",
                  certSize);
        rv = _VN_ERR_NOMEM;
        goto out;
    }

    rv = ISFS_Read(fd, certs, certSize);
    if (rv != stats->size) {
        if (rv < 0) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC, "IOS error %d reading file %s\n",
                      rv, cert_filename);
        } else {
            _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC, 
                      "Only read %d of %d bytes for file %s\n",
                      rv, certSize, cert_filename);
        }
        rv = _VN_ERR_FS;
        goto out;
    }

    rv = _VN_ERR_OK;

out:
    if (rv < 0) {
        if (certs) IOS_Free(0, certs);
        *pCerts = NULL;
        *pCertSize = 0;
    } else {
        *pCerts = certs;
        *pCertSize = certSize;
    }
    if (stats) IOS_Free(0, stats);
    if (fd >= 0) ISFS_Close(fd);
    return rv;
}
#else
/** 
 * Read in list of certs from a file (LINUX/WIN32)
 */
int _vn_read_certs(const char* cert_filename, uint8_t** pCerts, 
                   uint32_t* pCertSize)
{
    struct stat stats;
    uint8_t* certs = NULL;
    uint32_t certSize = 0;
    FILE *fptr = NULL;
    int rv;

    rv = stat(cert_filename, &stats);
    if (rv < 0) {
        _vn_errno_t err = _vn_sys_get_errno();
        _vn_errstr_t errstr = _vn_sys_get_errstr(err);
        _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC,
                  "Error getting stats for file %s: %s", 
                  cert_filename, errstr? errstr:"");
        _vn_sys_free_errstr(errstr);
        rv = _VN_ERR_FS;
        goto out;
    }

    fptr = fopen(cert_filename, "rb");
    if (fptr == NULL) {
        _vn_errno_t err = _vn_sys_get_errno();
        _vn_errstr_t errstr = _vn_sys_get_errstr(err);
        _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC,
                  "Error opening file %s: %s", 
                  cert_filename, errstr? errstr:"");
        _vn_sys_free_errstr(errstr);
        rv = _VN_ERR_FS;
        goto out;
    }

    certSize = stats.st_size;
    certs = _vn_malloc_aligned(certSize, _VN_CERT_ALIGNMENT);
    if (certs == NULL) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC, 
                  "Cannot allocate %d bytes for certs\n",
                  certSize);
        rv = _VN_ERR_NOMEM;
        goto out;
    }

    rv = fread(certs, 1, certSize, fptr);
    if (rv != stats.st_size) {
        if (rv < 0) {
            _vn_errno_t err = _vn_sys_get_errno();
            _vn_errstr_t errstr = _vn_sys_get_errstr(err);
            _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC,
                      "Error reading file %s: %s", 
                      cert_filename, errstr? errstr:"");
            _vn_sys_free_errstr(errstr);
        } else {
            _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC, 
                      "Only read %d of %d bytes for file %s\n",
                      rv, certSize, cert_filename);
        }
        rv = _VN_ERR_FS;
        goto out;
    }

    rv = _VN_ERR_OK;

out:
    if (rv < 0) {
        if (certs) _vn_free_aligned(certs);
        *pCerts = NULL;
        *pCertSize = 0;
    } else {
        *pCerts = certs;
        *pCertSize = certSize;
    }
    if (fptr != NULL) fclose(fptr);
    return rv;
}
#endif

/**
 * Verifies a cert, caches issuer key
 */
int _vn_verify_cert(IOSCEccEccCert *deviceCert,
                    IOSCPublicKeyHandle *pCertKey,
                    uint8_t *certs, size_t certSize,
                    void** pCertChain, uint32_t *pCertChainLen)
{
    int rv = _VN_ERR_OK;
    IOSCPublicKeyHandle issuerKey = _VN_KEY_HANDLE_INVALID;
    _vn_cert_key_t *pIssuerCertKey = NULL;

    pIssuerCertKey = _vn_lookup_cert_key(deviceCert->sig.issuer);
    if (pIssuerCertKey) {
        _VN_TRACE(TRACE_FINE, _VN_SG_IOSC, "Use cached issuer key\n");
        issuerKey = pIssuerCertKey->hPubKey;
    } else if (certs && certSize) {
        _VN_TRACE(TRACE_FINE, _VN_SG_IOSC, "Validate issuer using certs\n");

        rv = _vn_get_issuer_key(deviceCert->sig.issuer, 
                                certs, certSize, &issuerKey,
                                pCertChain, pCertChainLen);

        if (rv < 0) {
            goto out;
        }
        
        _vn_add_cert_key(deviceCert->sig.issuer, issuerKey);
    } else {
        _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC, 
                  "Cannot verify device cert without issuer cert\n");
        rv = _VN_ERR_NOTFOUND;
    }

    if (rv >= 0) {
        rv = _vn_import_cert((uint8_t*) deviceCert, sizeof(IOSCEccEccCert), 
                             issuerKey, pCertKey);
    }
      
out:
    return rv;
}


/**
 * Extracts guid from cert and checks it against given guid.
 */
int _vn_check_cert_guid(_VN_guid_t guid, uint8_t* cert, size_t certLen)
{
    _VN_guid_t cert_guid;
    int rv;

    /* Check guid matches cert guid */
    rv = _vn_get_cert_guid(cert, certLen, &cert_guid);
    if (rv < 0) {
        _VN_TRACE(TRACE_WARN, _VN_SG_IOSC,
                  "Error %d extracting guid from cert\n", rv);
        return rv;
    }

    if (cert_guid.chip_id != guid.chip_id ||
        (cert_guid.device_type & _VN_DEVICE_TYPE_MASK) != 
        (guid.device_type & _VN_DEVICE_TYPE_MASK)) {
        _VN_TRACE(TRACE_WARN, _VN_SG_IOSC,
                  "Guid from cert (0x%08x %u) does not match"
                  " expected guid (0x%08x %u)\n",
                  cert_guid.device_type, cert_guid.chip_id,
                  guid.device_type, guid.chip_id);
#if _VN_HS_CHECK_GUID
        rv = _VN_ERR_FAIL;
#endif
    }

    return rv;
}

/**
 * Verifies a device cert, cert chain needed to verify the device cert 
 * is expected to follow.
 * Checks the guid in the cert against a expected guid.  
 * Also creates and imports the public key from the cert.
 * Aligns the cert (by copying it) if needed 
 *
 * @param guid      Expected guid
 * @param certData  Pointer to cert (cert chain may follows)
 * @param certLen   Size of cert chain
 * @param pCertKey  Pointer to handle in which the public key of the cert 
 *                  to be stored
 */
int _vn_verify_device_cert(_VN_guid_t guid,
                           uint8_t* certData, size_t certLen,
                           IOSCPublicKeyHandle* pCertKey)
{
    uint8_t *cert = certData;
    uint8_t *remainingCerts = NULL;
    size_t remainingCertLen = 0;
    int rv = _VN_ERR_OK;

    assert(cert);
    assert(pCertKey);
    assert(_VN_KEY_HANDLE_IS_INVALID(*pCertKey));

#if _VN_IOSC_ALIGN_NEEDED
    if ((uint32_t) cert & (_VN_CERT_ALIGNMENT-1)) {
        cert = _vn_malloc_aligned(certLen, _VN_CERT_ALIGNMENT);
        if (cert) {
            memcpy(cert, certData, certLen);
        } else {
            _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC,
                      "Error verifying cert: out of memory\n");
            rv = _VN_ERR_NOMEM;
            goto out;
        }
    }
#endif

    /* Check guid matches cert guid */
    rv = _vn_check_cert_guid(guid, cert, certLen);
    if (rv < 0) {
        goto out;
    }

    /* Cert should have been checked as a IOSCEccEccCert */
    assert(certLen >= sizeof(IOSCEccEccCert));
    remainingCerts = cert + sizeof(IOSCEccEccCert);
    remainingCertLen = certLen - sizeof(IOSCEccEccCert);

#if _VN_HS_CHECK_CERT
    rv = _vn_verify_cert((IOSCEccEccCert *) cert, pCertKey,
                         remainingCerts, remainingCertLen,
                         NULL, 0);
#else
    {
        IOSCError ios_rv;
        ios_rv = _vn_iosc_import_cert_public_key(cert, certLen, pCertKey);
        if (ios_rv < 0) {
            _VN_TRACE(TRACE_WARN, _VN_SG_IOSC,
                      "IOS error %d importing device public key from cert\n",
                      ios_rv);
            rv = _VN_ERR_SEC;
            goto out;
        }
        rv = _VN_ERR_OK;
    }
#endif


out:

#if _VN_IOSC_ALIGN_NEEDED
    if (cert && cert != certData) {
        _vn_free_aligned(cert);
    }
#endif
    return rv;
}


#ifndef _SC
static char* _vn_iosc_certchain_filename = NULL;

/* Get cert filenames from environment variables */
int _vn_iosc_get_env()
{
    char* cert_dir;
    char* root_dir;

    _vn_iosc_certchain_filename = getenv(_VN_CERT_ENV);
    cert_dir = getenv(_VN_CERT_DIR_ENV);
    root_dir = getenv(_VN_ROOT_DIR_ENV);

    if (cert_dir == NULL) {
        if (root_dir) {
            cert_dir = _vn_malloc(strlen(root_dir) + strlen(_VN_CERT_DIR) + 1);
            strcpy(cert_dir, root_dir);
            strcat(cert_dir, _VN_CERT_DIR);
        } else {
            /* Look for certs in current directory if root is not defined */
            cert_dir = ".";
        }
    }

    if (_vn_iosc_certchain_filename == NULL) {
        _vn_iosc_certchain_filename = _vn_malloc(
            strlen(cert_dir) + strlen(_VN_CERT_SYS_FILE) + 2);
        strcpy(_vn_iosc_certchain_filename, cert_dir);
        strcat(_vn_iosc_certchain_filename, "/");
        strcat(_vn_iosc_certchain_filename, _VN_CERT_SYS_FILE);
    }

    _VN_TRACE(TRACE_INFO, _VN_SG_IOSC,
              "Using cert chain at: %s\n", _vn_iosc_certchain_filename);

    return _VN_ERR_OK; 
}

int _vn_iosc_read_cert(const char* filename, u8* cert)
{
    FILE *fptr;

    assert(filename);
    assert(cert);

    fptr = fopen(filename, "rb");
    if (fptr == NULL) {
        _vn_errno_t err;
        _vn_errstr_t errstr;
        err = _vn_sys_get_errno();
        errstr = _vn_sys_get_errstr(err);
        _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC,
                  "Error reading certificate %s: %s (error %d)\n",
                  filename, errstr? errstr:"", err);
        return _VN_ERR_SEC;
    }

    fread((u8 *) cert, 1, sizeof(IOSCGenericCert), fptr);
    fclose(fptr);
    return _VN_ERR_OK;
}
#endif

int _vn_iosc_init_certs()
{
    IOSCPublicKeyHandle myPubKey = _VN_KEY_HANDLE_INVALID;  
    IOSCError ios_rv;
    int rv = _VN_ERR_OK;
    
    uint8_t* certs = NULL;
    uint32_t certSize = 0;

    _vn_init_cert_keys();
    /* read /sys/cert.sys */
#ifdef _SC
    rv = _vn_read_certs(_VN_CERT_SYS_FILE, &certs, &certSize);
#else
    _vn_iosc_get_env();
    rv = _vn_read_certs(_vn_iosc_certchain_filename, &certs, &certSize);
#endif

    if (rv < 0) {
        goto err;
    }

    rv = _VN_ERR_SEC;

    /* Get Device Certificate */
    ios_rv = IOSC_GetDeviceCertificate(&_vn_iosc_devcert);
    if (ios_rv != IOSC_ERROR_OK) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC, 
                  "IOS error %d while getting device certificate\n", ios_rv);
        goto err;
    } else {
        if (_VN_TRACE_ON(TRACE_FINEST, _VN_SG_IOSC)) {        
            _VN_TRACE(TRACE_FINEST, _VN_SG_IOSC, "My device certificate:\n");
            _vn_dbg_dump_buf(stdout, _vn_iosc_devcert, sizeof(IOSCEccSignedCert));
        }
    }

    _vn_devcert.cert = _vn_iosc_devcert;
    _vn_devcert.certSize = 
        _vn_get_cert_size(_vn_iosc_devcert, sizeof(_vn_iosc_devcert));
    if (_vn_devcert.certSize < 0) {
        rv = _vn_devcert.certSize;
        _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC,
                  "Error %d getting my cert size\n", rv);
        goto out;
    }

    /* Check my guid matches my cert guid */
    rv = _vn_check_cert_guid(_vn_get_myguid(),
                             _vn_devcert.cert, _vn_devcert.certSize);
    if (rv < 0) {
        goto out;
    }

    /* Make sure we can validate our own device cert */
    rv = _vn_verify_cert((IOSCEccEccCert *) _vn_iosc_devcert, &myPubKey,
                         certs, certSize, 
                         (void**) &_vn_devcert.certChain, 
                         &_vn_devcert.certChainSize);

    if (rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_IOSC,
                  "Error %d importing my own device cert\n", rv);
    }

    if (_VN_KEY_HANDLE_IS_VALID(myPubKey)) {
        IOSC_DeleteObject(myPubKey);
    }

    goto out;

err:
out:
    if (certs) {
#ifdef _SC
        IOS_Free(0, certs);
        _vn_free_aligned(certs);
#else
#endif
    }
    return rv;    
}

int _vn_clear_certs()
{
    return _vn_clear_cert_keys();
}

#endif /* _VN_USE_IOSC */

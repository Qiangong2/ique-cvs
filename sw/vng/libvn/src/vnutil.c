//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnlocal.h"

/* Utility functions for the VN layer */

/* Support functions for locking the VN */
_vn_mutex_t  _vn_net_mutex;

int _vn_net_lock()
{
    /* Locks access to the VN  */
    return _vn_mutex_lock(&_vn_net_mutex);
}

int _vn_net_unlock()
{
    /* Unlocks access to the VN */
    return _vn_mutex_unlock(&_vn_net_mutex);
}

int _vn_net_lock_init()
{
    int rv;

    rv = _vn_mutex_init(&_vn_net_mutex);
    if (rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_NETIF, 
                  "Failed to initialize mutex for VN: %d\n", rv);
        return rv;
    }

    return _VN_ERR_OK;
}

int _vn_net_lock_destroy()
{
    return _vn_mutex_destroy(&_vn_net_mutex);
}

/**** Functions for allocating/deallocating message buffers */

_vn_wbuf_t* _vn_get_msg_buffer_aligned(_VN_msg_len_t len, 
                                       uint8_t type,
                                       uint8_t offset,
                                       uint32_t alignment)
{
    _vn_wbuf_t* pkt;
    assert(offset < len);
    pkt = _vn_malloc(sizeof(_vn_wbuf_t));
    if (pkt) {
        pkt->buf = _vn_malloc_aligned(len, alignment);
        if (pkt->buf) {
            memset(pkt->buf, 0, offset);
            pkt->max_len = len;
            pkt->offset = offset;
            pkt->type = type;
            pkt->len = 0;
        } else {
            _vn_free(pkt);
            pkt = NULL;
        }
    }
    return pkt;
}

void _vn_free_msg_buffer_aligned(_vn_wbuf_t* pkt)
{
    if (pkt) {
        if (pkt->buf) {
            _vn_free_aligned(pkt->buf);
        }
        _vn_free(pkt);
    }
}

_vn_wbuf_t* _vn_get_vn_buffer(_VN_msg_len_t len)
{
    uint8_t offset = _VN_MAC_BLOCK_SIZE - _VN_HEADER_SIZE;
    return _vn_get_msg_buffer_aligned(
        len + offset,
        _VN_PKT_TYPE_VN, 
        offset,
        _VN_MAC_ALIGNMENT);
}

void _vn_free_msg_buffer(_vn_buf_t* pkt)
{
    if (pkt) {
        _vn_free(pkt);
    }
}

_vn_buf_t* _vn_get_msg_buffer(_VN_msg_len_t len)
{
    _vn_buf_t* pkt;
    pkt = _vn_malloc(sizeof(_vn_buf_t) + len);
    if (pkt) {
        pkt->len = 0;
        pkt->max_len = len;
    }
    return pkt;
}

_vn_wbuf_t* _vn_new_vn_buffer(const void *msg, _VN_msg_len_t len)
{
    if (msg && len) {
        _vn_wbuf_t* new_buf = _vn_get_vn_buffer(len);
        if (new_buf) {
            new_buf->len = len;
            memcpy(new_buf->buf + new_buf->offset, msg, len);
        }
        return new_buf;
    }
    else return NULL;
}

_vn_buf_t* _vn_new_msg_buffer(const void *msg, _VN_msg_len_t len)
{
    if (msg && len) {
        _vn_buf_t* new_buf = _vn_get_msg_buffer(len);
        if (new_buf) {
            new_buf->len = len;
            memcpy(new_buf->buf, msg, len);
        }
        return new_buf;
    }
    else return NULL;
}

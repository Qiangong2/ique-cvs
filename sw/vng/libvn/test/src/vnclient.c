//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnlocal.h"
#include "vntest.h"

#ifdef _SC

#define STACK_SIZE 1024*(32)
const uint8_t _initStack[STACK_SIZE];
const uint32_t _initStackSize = sizeof(_initStack);
const uint32_t _initPriority = _VN_THREAD_PRIORITY;
uint8_t  _vnt_recv_events_thread_stack[32*1024];

#endif

_vn_thread_t recv_thread;
int recv_thread_keep_running = 1;

void recv_events_loop()
{
    size_t bufsize = 32*1024;
    uint8_t* events;
    _VN_event_t* event;
    events = _vn_malloc(bufsize);
    if (events == NULL) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "Error allocating memory for events\n");
        return;
    }
    while (recv_thread_keep_running) {
        /* Check if I got any events */        
        int n;
        _VN_TRACE(TRACE_FINER, _VN_SG_TEST, "Waiting for event\n");
        n = _VN_get_events(events, bufsize, 10000);
        if (n == _VN_ERR_CANCELED) {
             _VN_TRACE(TRACE_FINE, _VN_SG_TEST,
                       "_VN_get_events canceled\n", n);
        }
        event = (_VN_event_t*) events;
        while (n > 0) {
#ifdef _SC
            _vnt_proc_event(event, true, false);
#else
            _vnt_proc_event(event, true, false);
#endif
            event = event->next;
            n--;
        }
    }
    if (events) {
        _vn_free(events);
    }
}

void recv_events_start()
{
    _vn_thread_attr_t* pAttr = NULL;
#ifdef _SC
    _vn_thread_attr_t thread_attr;
    thread_attr.stack = _vnt_recv_events_thread_stack;
    thread_attr.stackSize = sizeof(_vnt_recv_events_thread_stack);
    thread_attr.priority = _VN_THREAD_PRIORITY;
    thread_attr.attributes = IOS_THREAD_CREATE_JOINABLE;
    thread_attr.start = true;
    pAttr = &thread_attr;
#endif

    _VN_TRACE(TRACE_FINE, _VN_SG_TEST, "Starting get events thread\n");
    _vn_thread_create(&recv_thread, NULL, (void*) &recv_events_loop, pAttr);
}

void recv_events_stop()
{
    _VN_TRACE(TRACE_FINE, _VN_SG_TEST, "Stopping get events thread\n");
    recv_thread_keep_running = 0;
    _VN_cancel_get_events();
    _vn_thread_join(recv_thread, NULL);
}

/* Host discovery tests */
bool testSimpleHostDiscoveryClient1()
{
    char cmdbuf[1024];
    int lineno;
    int timeout = 500;
    int netmask = _VN_NET_MASK3;
    int callid;
    bool isLocal = true;
    bool okay = true;
    _VN_guid_t guid = { _VN_DEVICE_TYPE_UNKNOWN, 1 };
    _VN_guid_t joiner_guid = { _VN_DEVICE_TYPE_UNKNOWN, 2 };
    _VN_addr_t myaddr;
    _VN_host_t myhost;
    char* testname = "testSimpleHostDiscoveryClient1";
    
    /* Test with 2 hosts, Registering and unregistering a service */

    /* Client 1 */

    /* Don't automatically accept connections */
    sprintf(cmdbuf, "set accept 0");
    if (!_vnt_test_command(cmdbuf, _VN_ERR_OK, NULL)) {
        lineno = __LINE__; goto fail;
    }

    /* Create a new net and listen on it */
    if (!_vnt_test_create_command_succeeded(timeout, netmask, isLocal,
                                            &myaddr, &myhost)) 
    {
        lineno = __LINE__; goto fail;
    }

    sprintf(cmdbuf, "listen 0x%08x", _VN_addr2net(myaddr));
    if (!_vnt_test_command(cmdbuf, _VN_RV_CALLID, &callid)) {
        lineno = __LINE__; goto fail;
    }

    /* Registers a new private service */
    sprintf(cmdbuf, "private net");
    if (!_vn_test_register_service(_VN_ERR_OK, __LINE__, 1000, false,
                                   cmdbuf, strlen(cmdbuf) + 1))
    {
        lineno = __LINE__; goto fail;
    }

    /* Registers a new public service with my guid and the new net id */
    sprintf(cmdbuf, "public vnaddr 0x%08x, guid 0x%08x %u",
            myaddr, guid.device_type, guid.chip_id);
    if (!_vn_test_register_service(_VN_ERR_OK, __LINE__, 1001, true,
                                   cmdbuf, strlen(cmdbuf) + 1))
    {
        lineno = __LINE__; goto fail;
    }

    /* Registers another public service */
    sprintf(cmdbuf, "second public service");
    if (!_vn_test_register_service(_VN_ERR_OK, __LINE__, 1002, true,
                                   cmdbuf, strlen(cmdbuf) + 1))
    {
        lineno = __LINE__; goto fail;
    }

    /* Wait for connection from Client 2 */
    timeout = 15000;
    if (!_vnt_test_connect_command_succeeded(guid,
                                             joiner_guid,
                                             _VN_addr2net(myaddr),
                                             callid,
                                             myaddr,
                                             1, 1, &myhost,
                                             NULL, NULL,
                                             timeout))
    {
        lineno = __LINE__; goto fail;
    }

    timeout = 1000;
    _vn_thread_sleep(timeout);

    /* Unregisters the service */
    if (!_vn_test_unregister_service(_VN_ERR_OK, __LINE__, 1002)) {
        lineno = __LINE__; goto fail;
    }

    goto done;

fail:
    okay = false;
    _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
              "%s: Failed in %s, line %d\n", testname, __FILE__, lineno);

done:
    _VN_TRACE(TRACE_INFO, _VN_SG_TEST,
              "%s: Test %s\n", testname, okay? "PASS": "FAILED");
    return okay;
}

bool testSimpleHostDiscoveryClient2(bool discover, bool query,
                                    bool clear_query_list)
{
    char cmdbuf[1024];
    char tmpbuf[1024];
    int rv, lineno;
    int timeout = 10000;
    int n = 0;
    bool okay = true, lan;
    _VN_guid_t host_guid = { _VN_DEVICE_TYPE_UNKNOWN, 1 };
    _VN_guid_t guid;
    _VN_addr_t myaddr, owner_addr;
    _VN_host_t myhost, owner_host;
    char* testname;
    uint8_t evt_buf[1024];
    _VN_event_t* event = NULL;
    uint32_t exp_services[] = { 1001, 1002 };
    uint32_t services[2];
    _VN_guid_t guids[2];
#ifdef UPNP
    _vn_inport_t query_port = _vn_upnp_get_server_port() - 1;
#else
    _vn_inport_t query_port = 49152;
#endif

    /* Test with 2 hosts, Discovering a service + Joining a net */

    /* Client 2 */
    if (clear_query_list) {
        testname = "testClearQueryListClient2";
    }
    else if (query) {
        testname = "testSimpleHostQueryClient2";
    }
    else {
        testname = "testSimpleHostDiscoveryClient2";
    }

    /* Don't automatically accept connections */
    sprintf(cmdbuf, "set accept 0");
    if (!_vnt_test_command(cmdbuf, _VN_ERR_OK, NULL)) {
        lineno = __LINE__; goto fail;
    }

    /* Start/Stop host discovery */
    sprintf(cmdbuf, "discoverhosts %d", discover);
    if (!_vnt_test_command(cmdbuf, _VN_ERR_OK, NULL)) {
        lineno = __LINE__; goto fail;
    }

    if (query) {
        sprintf(cmdbuf, "queryhost localhost %u", query_port);
        if (!_vnt_test_command(cmdbuf, _VN_ERR_OK, NULL)) {
            lineno = __LINE__; goto fail;
        }
    }

    /* Wait to discover a new device with two services */
    event = _vnt_get_next_event(event, &n, evt_buf, sizeof(evt_buf), timeout);
    if (event == NULL) { lineno = __LINE__; goto fail; }

    /* Check if one event contains both new device and 2 services */
    if (_vnt_check_device_update_event(__LINE__, event, _VN_MSG_ID_INVALID,
                                        host_guid, _VN_EVT_R_DEVICE_NEW,
                                        2, 0, exp_services)) {
        goto test_api;
    }

    /* Check if 2 events (one for device, one for services) */
    if (!_vnt_check_device_update_event(__LINE__, event, _VN_MSG_ID_INVALID,
                                        host_guid, _VN_EVT_R_DEVICE_NEW,
                                        0, 0, NULL)) {
        lineno = __LINE__; goto fail;
    }

    /* Wait for service to be discovered */
    /* Test two services are discovered (the public services) */

    event = _vnt_get_next_event(event, &n, evt_buf, sizeof(evt_buf), timeout);
    if (event == NULL) { lineno = __LINE__; goto fail; }

    if (!_vnt_check_device_update_event(__LINE__, event, _VN_MSG_ID_INVALID,
                                        host_guid, _VN_EVT_R_DEVICE_UPDATE,
                                        2, 0, exp_services)) {
        lineno = __LINE__; goto fail;
    }


test_api:
    /* Test API for getting ADHOC device */
    if (!_vn_test_get_guids(1, __LINE__, _VN_DEVICE_PROP_ADHOC,
                            &host_guid, guids, 1)) {
        lineno = __LINE__; goto fail;
    }

    if (!_vn_test_get_guids(query, __LINE__, 
                            _VN_DEVICE_PROP_QUERIED,
                            query? &host_guid:NULL, guids, query)) {
        lineno = __LINE__; goto fail;
    }    

#ifdef _VN_RPC_DEVICE
    lan = query | discover;
#else
    lan = discover;
#endif

    if (!_vn_test_get_guids(lan, __LINE__,
                            _VN_DEVICE_PROP_LAN,
                            lan? &host_guid:NULL, guids, lan)) {
        lineno = __LINE__; goto fail;
    }

    /* Test API for getting service from device */
    if (!_vn_test_get_service_ids(2, __LINE__, host_guid, 
                                  exp_services, services, 2)) {
        lineno = __LINE__; goto fail;
    }

    if (clear_query_list) {
        /* Clear query list */
        sprintf(cmdbuf, "clearqueryhosts");
        if (!_vnt_test_command(cmdbuf, _VN_ERR_OK, NULL)) {
            lineno = __LINE__; goto fail;
        }
        
        if (!_vn_test_get_guids(0, __LINE__, 
                                _VN_DEVICE_PROP_QUERIED,
                                NULL, guids, 0)) {
            lineno = __LINE__; goto fail;
        }
    }
    
    /* Use service details to figure out client 1 guid and net id */
    memset(tmpbuf, 0, sizeof(tmpbuf));
    rv = _VN_get_service(host_guid, exp_services[0], tmpbuf, sizeof(tmpbuf));
    if (rv < 0) { lineno = __LINE__; goto fail; }

    n = sscanf(tmpbuf, "public vnaddr 0x%x, guid 0x%x %u", 
               &owner_addr, &guid.device_type, &guid.chip_id);
    if (n != 3) { 
        _VN_TRACE(TRACE_FINE, _VN_SG_TEST, "Service description: %s\n",
                  tmpbuf);
        lineno = __LINE__; goto fail; 
    }

    if (!_vn_guid_match(&guid, &host_guid)) 
    { lineno = __LINE__; goto fail; }    

    /* Connect to Client 1 */
    timeout = 500;
    owner_host = _VN_addr2host(owner_addr);
    if (!_vnt_test_connect_command_succeeded(host_guid,
                                             _vn_get_myguid(),
                                             _VN_addr2net(owner_addr),
                                             _VN_MSG_ID_INVALID,
                                             owner_addr,
                                             0, 1, &owner_host,
                                             &myaddr, &myhost,
                                             timeout))
    {
        lineno = __LINE__; goto fail;
    }

    if (clear_query_list && !discover) {
        goto done;
    }

    /* Wait for notification service 1002 is unregistered */
    n = 0;
    timeout = 20000;
    event = _vnt_get_next_event(event, &n, evt_buf, sizeof(evt_buf), timeout);
    if (event == NULL) { lineno = __LINE__; goto fail; }

    if (!_vnt_check_device_update_event(__LINE__, event, _VN_MSG_ID_INVALID,
                                        host_guid, _VN_EVT_R_DEVICE_UPDATE,
                                        0, 1, &exp_services[1])) {
        lineno = __LINE__; goto fail;
    }

    /* Wait for device to be unregistered */
    event = _vnt_get_next_event(event, &n, evt_buf, sizeof(evt_buf), timeout);
    if (event == NULL) { lineno = __LINE__; goto fail; }

    if (!_vnt_check_device_update_event(__LINE__, event, 
                                        _VN_MSG_ID_INVALID, host_guid,
                                        _VN_EVT_R_DEVICE_DISCONNECTED,
                                        0, 1, &exp_services[0])) {
        lineno = __LINE__; goto fail;
    }

    /* Should not have any adhoc devices left */
    if (!_vn_test_get_guids(0, __LINE__, _VN_DEVICE_PROP_ADHOC,
                            NULL, guids, 0)) {
        lineno = __LINE__; goto fail;
    }

    goto done;

fail:
    okay = false;
    _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
              "%s: Failed in %s, line %d\n", testname, __FILE__, lineno);

done:
    _VN_TRACE(TRACE_INFO, _VN_SG_TEST,
              "%s: Test %s\n", testname, okay? "PASS": "FAILED");
    return okay;
}

void usage()
{
    printf("Usage: vnclient [-g guid] [-i mode] [-l level] [-p port]\n");
    printf("                [server_host] [server_port]\n");
#ifdef _VN_USB_SOCKETS
#ifndef _SC
    printf("                [-u usbhost:port]\n");
    printf("-u usbhost:port TCP host and port to use for USB connections\n");
    printf("                (default localhost:%u)\n", _VN_USB_PORT_PROXY);
#endif
#endif
    printf("-l level   Set trace level\n");
    printf("-p port    Local port to bind to (default %u)\n", _VN_CLIENT_PORT);
    printf("-i mode    Level of interaction\n");
    printf("           0 = No interaction (used for running tests)\n");
    printf("           1 = Sending input messages to test net (default)\n");
    printf("           2 = Interactive mode with commands\n");
    printf("-g guid    Use guid for my guid\n");           
#ifndef _SC
    printf("-r logfile Uses logfile for binary log of received packets\n");
    printf("-s logfile Uses logfile for binary log of sent packets\n");
#endif
}

#ifdef _SC
#define MAXCMDLINE 80
#define MAXARGS 20

int main()
#else
int main(int argc, char** argv)
#endif
{
    const char* server = "127.0.0.1";
    _vn_inport_t server_port = _VN_SERVER_TEST_PORT;
    _vn_inport_t client_port = _VN_CLIENT_PORT;
    int rv;
    char line[MAXLINE];
    _VN_port_t port;
    int i, test = 0;
    _VN_chip_id_t chip_id = _VN_CHIP_ID_INVALID;
#ifdef _SC
    int interactive = 0;
#else
    int interactive = 1;
#endif
    int trace_level = TRACE_FINE;

#ifdef _SC
    int argc = 0;
    char cmdline[MAXCMDLINE];
    char* argv[MAXARGS];
    char* ptr;
#else
    const char* recv_logfile = "/tmp/vnclient.recv";
    const char* sent_logfile = "/tmp/vnclient.sent";
#endif

#ifdef _SCE
    get_cmdline(cmdline, (unsigned long) sizeof(cmdline));

    /* Put args in argc, argv for use with getopt */
    argc = 1;
    argv[0] = "vnclient.c";
    ptr = cmdline;
    while (*ptr) {
        if (*ptr == ' ') { 
            *ptr = '\0';
        }
        else {
            if ((ptr == cmdline) || (*(ptr-1) == '\0')) {
                argv[argc] = ptr;
                argc++;
            }
        }
        ptr++;
    }
#endif

#if defined(_VN_USB_SOCKETS) && !defined(_SC)
    while ((rv = getopt(argc, argv, "g:i:r:s:t:l:p:u:")) != -1) {
#else
    while ((rv = getopt(argc, argv, "g:i:r:s:t:l:p:")) != -1) {
#endif
        switch (rv) {
        case 'l':
            trace_level = atoi(optarg);
            break;
        case 'p':
            client_port = atoi(optarg);
            break;
        case 'i':
            interactive = atoi(optarg);
            break;
        case 'g':
            chip_id = atoi(optarg);
            break;
#ifndef _SC
        case 'r':
            recv_logfile = optarg;
            break;
        case 's':
            sent_logfile = optarg;
            break;
#endif
        case 't':
            test = atoi(optarg);
            break;
#ifdef _VN_USB_SOCKETS
#ifndef _SC
        case 'u':
            {
                char* portstr = strchr(optarg, ':');
                if (portstr) {
                    *portstr = '\0';
                    portstr++;                
                    _vn_netif_set_usb_proxy_port(atoi(portstr));
                }
                _vn_netif_set_usb_proxy_host(optarg);
            }
            break;
#endif
#endif                       
        default:
            usage();
            exit(1);
        }
    }

    _vn_set_trace_level(trace_level);

    if (chip_id != _VN_CHIP_ID_INVALID) {
        _vn_set_my_chip_id(chip_id);
    }

    if (optind < argc) {
        server = argv[optind];
        if (optind + 1 < argc) {
            server_port = atoi(argv[optind+1]);
        }
    }

#ifndef _SC
    _vn_dbg_setup_logs(sent_logfile, recv_logfile);
#endif

    if (test || (interactive == 2)) {
        if ((rv = _vn_init_common(NULL, client_port, client_port)) < 0) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                      "_VN_init failed with %d\n", rv);
            exit(-1);
        }
    }
    else {
        _VN_TRACE(TRACE_FINE, _VN_SG_TEST,
                  "Connecting to %s:%u\n", server, server_port);
        if ((rv = _vn_init_client(server, server_port, client_port)) < 0) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                      "_vn_init_client to %s:%u failed with %d\n",
                      server, server_port, rv);
            exit(-1);
        }
    }

    port = _VN_PORT_TEST;

    if (test) {
        bool okay;
        switch (test) {
        case 1:
            if (chip_id == 1) {
                okay = testSimpleHostDiscoveryClient1();
            }
            else if (chip_id == 2) {
                okay = testSimpleHostDiscoveryClient2(true, false, false);
            }
            else {
                _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                          "Unknown client number %d for test %d\n",
                          chip_id, test);
                okay = false;
            }
            break;

        case 2:
            if (chip_id == 1) {
                okay = testSimpleHostDiscoveryClient1();
            }
            else if (chip_id == 2) {
                okay = testSimpleHostDiscoveryClient2(false, true, false);
            }
            else {
                _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                          "Unknown client number %d for test %d\n",
                          chip_id, test);
                okay = false;
            }
            break;

        case 3:
            if (chip_id == 1) {
                okay = testSimpleHostDiscoveryClient1();
            }
            else if (chip_id == 2) {
                okay = testSimpleHostDiscoveryClient2(true, true, false);
            }
            else {
                _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                          "Unknown client number %d for test %d\n",
                          chip_id, test);
                okay = false;
            }
            break;

        case 4:
            if (chip_id == 1) {
                okay = testSimpleHostDiscoveryClient1();
            }
            else if (chip_id == 2) {
                okay = testSimpleHostDiscoveryClient2(false, true, true);
            }
            else {
                _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                          "Unknown client number %d for test %d\n",
                          chip_id, test);
                okay = false;
            }
            break;

        default:
            _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                      "Unknown test number %d\n", test);
            okay = false;
        }
        rv = okay? 0: 1;
    }
    else if (interactive) {
        recv_events_start();

        i = 0;
        if (interactive == 2) {
            _VN_PRINT("> ");
        }
#if defined(_SCE)
        while (!_vnt_exit && gets(line) != NULL) {
#elif defined(_SC)
        while (!_vnt_exit) {
#else
        while (!_vnt_exit && fgets(line, MAXLINE, stdin) != NULL) {
#endif
            if (interactive == 2) {
                rv = _vnt_proc_command(line);
                if (!_vnt_exit) {
#ifdef _SC
                    /* Sleep before calling gets again so other threads
                       get a chance to run */
                    _vn_thread_sleep(500);
#endif
                    _VN_PRINT("> ");
                }
            }
            else {
                /* Send this */
                rv = _VN_send(_vnt_test_net_all, 
                              _vn_get_default_localhost(_vnt_test_net_all),
                              _VN_HOST_ANY, 
                              port, line, (_VN_msg_len_t) strlen(line), 
                              NULL, 0,
                              _VN_MSG_RELIABLE | _VN_MSG_ENCRYPTED);
                if (rv < 0) {
                    _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                              "VN_send failed due to %d\n", rv);
                }                       
                
                i++;

#ifdef _SC
                /* Sleep before calling gets again so other threads
                   get a chance to run */
                _vn_thread_sleep(500);
#endif
            }
        }

        _vn_leave_all();

        recv_events_stop();
        rv = 0;
    }
    else {
        recv_events_start();

        while (!_vnt_exit) {
            _VN_TRACE(TRACE_FINER, _VN_SG_TEST, "Main loop\n");
            _vn_thread_sleep(5000);
        }

        recv_events_stop();
        rv = 0;
    }

    _vn_cleanup();
    _vn_dbg_close_logs();

    exit(rv);
    return rv;
}

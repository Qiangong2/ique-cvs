//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnlocal.h"
#include "vntest.h"

/* Test message formatting */

bool _vn_test_auth_save_pkt(FILE* fp, _vn_buf_t* pkt, _vn_key_t key)
{
    bool okay = true;
    static int count = 0;
    _vn_wbuf_t* tmp_pkt;
    _VN_time_t t = 0;

    count++;
    tmp_pkt = _vn_conv_msg_buffer(pkt);
    assert(tmp_pkt);

    /* Encrypt and add auth code */
    _vn_hash_encrypt_pkt(tmp_pkt, key);
    /* Save packet for reference */
    _vn_dbg_write_timed_buf(fp, t, tmp_pkt->buf + tmp_pkt->offset, tmp_pkt->len);
    
    /* Check packet verifies and decrypts correctly */
    if (_vn_verify_pkt(tmp_pkt, key)) {
        /* Decrypt packet */
        _vn_decrypt_pkt(tmp_pkt, key);
        assert(tmp_pkt->len = pkt->len + _VN_AUTHCODE_SIZE);
        if (memcmp(tmp_pkt->buf + tmp_pkt->offset, pkt->buf, pkt->len) == 0) {
            /* Good - decryption gave exact same packet as before */
        }
        else {
            fprintf(stderr, "ERROR: Decrypted packet differs for packet %d\n", count);
            okay = false;
        }
    }
    else {
        fprintf(stderr, "ERROR: Authentication failed for packet %d\n", count);
        okay = false;
    }

    _vn_free_msg_buffer_aligned(tmp_pkt);
    return okay;
}

bool _vn_test_gen_pkts(FILE* fp)
{
    uint8_t test_data[_VN_MAX_MSG_LEN];
    _vn_buf_t* pkt;
    _vn_key_t key;
    bool okay = true;

    assert(fp);
    pkt = _vn_get_msg_buffer(_VN_MAX_TOTAL_MSG_LEN);
    assert(pkt);
#if _VN_USE_IOSC
    key.Ak = IOSC_FS_ENC_HANDLE;
    key.Hk = IOSC_FS_MAC_HANDLE;
#else
    memset(key, 0, sizeof(key));
#endif
    /* Format unencrypted message with no optional headers */
    sprintf(test_data, "Unencrypted testdata");
    _vn_format_pkt(pkt, 0, key, 0xabcd0000, 12, 23,
                   1, 15, test_data, (_VN_msg_len_t) strlen(test_data), 
                   NULL, 0, 0);
    if (!_vn_test_auth_save_pkt(fp, pkt, key)) 
        okay = false;

    /* Test adding ack */

    /* Add ack fields */
    _vn_qm_update_ack(pkt, 0, _VN_OPT_POS_ACK, 1, 5);
    if (!_vn_test_auth_save_pkt(fp, pkt, key))
        okay = false;

    /* Update ack field - should update */
    _vn_qm_update_ack(pkt, 0, _VN_OPT_POS_ACK, 11, 5);
    if (!_vn_test_auth_save_pkt(fp, pkt, key))
        okay = false;

    /* Update ack field - should not update */
    _vn_qm_update_ack(pkt, 0, _VN_OPT_POS_ACK, 6, 5);
    if (!_vn_test_auth_save_pkt(fp, pkt, key))
        okay = false;

    /* Update ack field - should update */
    _vn_qm_update_ack(pkt, 0, _VN_OPT_NEG_ACK, 50, 5);
    if (!_vn_test_auth_save_pkt(fp, pkt, key))
        okay = false;

    /* Update ack field - should update */
    _vn_qm_update_ack(pkt, 1, 0, 0, 0);
    if (!_vn_test_auth_save_pkt(fp, pkt, key))
        okay = false;

    /* Test zero length packet, reliable */
    _vn_format_pkt(pkt, 55, key, 0xa4210000, 69, 45,
                   9, 80, NULL, 0, NULL, 0, _VN_MSG_RELIABLE);
    if (!_vn_test_auth_save_pkt(fp, pkt, key))
        okay = false;

    /* Test encryption */
    sprintf(test_data, "This message should be encrypted and unreadable");
    _vn_format_pkt(pkt, 55, key, 0xa4210000, 69, 45,
                   9, 80, test_data, (_VN_msg_len_t) strlen(test_data), 
                   NULL, 0,
                   _VN_MSG_RELIABLE | _VN_MSG_ENCRYPTED);
    if (!_vn_test_auth_save_pkt(fp, pkt, key))
        okay = false;

    /* Test optional header */
    _vn_format_pkt(pkt, 55, key, 0xc8730000, 243, 255, 
                   1, 9, test_data, (_VN_msg_len_t) strlen(test_data),
                   "optional", 8, _VN_MSG_ENCRYPTED);
    if (!_vn_test_auth_save_pkt(fp, pkt, key))
        okay = false;

    /* Test optional header + ack */
    _vn_qm_update_ack(pkt, 0, _VN_OPT_NEG_ACK, 250, 19);
    if (!_vn_test_auth_save_pkt(fp, pkt, key))
        okay = false;

    _vn_qm_update_ack(pkt, 0, _VN_OPT_POS_ACK, 2, 19);
    if (!_vn_test_auth_save_pkt(fp, pkt, key))
        okay = false;

    _vn_free_msg_buffer(pkt);
    return true;
}

#ifdef _VN_NO_TRACE
#define _vn_dbg_read_pkt_fp(fp, t)   0
#else
extern _vn_buf_t* _vn_dbg_read_pkt_fp(FILE* fp, _VN_time_t* t);
#endif

bool _vn_test_cmp_pktfiles(FILE* fp, FILE* rfp)
{
    _VN_time_t t1, t2;
    _vn_buf_t* refpkt, *pkt;
    int count = 0;
    bool okay = true;
    while ( (refpkt = _vn_dbg_read_pkt_fp(rfp, &t1)) ) {
        count++;
        pkt = _vn_dbg_read_pkt_fp(fp, &t2);
        if (pkt) {
            if ((pkt->len == refpkt->len) && 
                (memcmp(pkt->buf, refpkt->buf, pkt->len) == 0)) {
                /* Okay, packet matches */
            }
            else {
                fprintf(stderr, "ERROR: Packet %d differs from reference packet\n", count);
                okay = false;
            }
            _vn_free_msg_buffer(pkt);
        }
        else {
            fprintf(stderr, "ERROR: Packet %d not found\n", count);
            okay = false;
        }
        _vn_free_msg_buffer(refpkt);
    }
    while ( (pkt = _vn_dbg_read_pkt_fp(fp, &t2)) ) {
        count++;
        fprintf(stderr, "ERROR: Reference packet %d not found\n", count);
        okay = false;
        _vn_free_msg_buffer(pkt);
    }
    return okay;
}

bool _vn_test_pkt_format(const char* reffile, const char* pktfile)
{
    FILE* rfp = NULL, *pfp = NULL;
    bool okay = true;
    assert(reffile);
    assert(pktfile);

    pfp = fopen(pktfile, "wb"); 
    if (pfp == NULL) {
        fprintf(stderr, "Unable to open output file %s\n", pktfile);
        return false;
    }

    rfp = fopen(reffile, "rb"); 
    if (rfp == NULL) {
        fprintf(stderr, "Unable to open reference file %s\n", reffile);
    }
    
    /* Generate some representative packets and save to file */
    okay = _vn_test_gen_pkts(pfp);

    /* Compare packets in files */
    if (rfp) {
        fclose(pfp);
        pfp = fopen(pktfile, "rb"); 
        if (pfp == NULL) {
            fprintf(stderr, "Unable to open output file %s for reading\n", pktfile);
            okay = false;
        }
        else {
            if (!_vn_test_cmp_pktfiles(pfp, rfp)) 
                okay = false;
        }
    }
    else { 
        fprintf(stderr, "Cannot compare output packets, no reference file\n");
        okay = false;
    }
    
    if (rfp) fclose(rfp);
    if (pfp) fclose(pfp);
    return okay;
}

int main(int argc, char** argv)
{
    const char* reffile = "vntestpkt.ref";
    const char* pktfile = "vntestpkt.pkt";

    if (argc > 1) {
        pktfile = argv[1];
    }

    if (argc > 2) {
        reffile = argv[2];
    }
    _vn_iosc_init();
    if (_vn_test_pkt_format(reffile, pktfile))
        return 0;
    else return -1;
}

/*
 * UProxy - A small proxy program for UDP based protocols
 *
 * Copyright (C) 2000  Alessandro Staltari
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#ifdef _WIN32
#include "shr_getopt.h"
#else
#include <getopt.h>
#endif

#include <assert.h>

#ifdef WIN32
#include <sys/timeb.h>
#include <windows.h>
#include <winsock.h>
#else
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#endif

#include <time.h>
#include <stdio.h>

#ifdef WIN32
typedef unsigned long long uint64_t;
#else
#define SOCKET      int
#define closesocket close
#define ioctlsocket ioctl
#endif

#define VERSION "0.90"

#define ARGNUM 3

SOCKET ProxySocket;
struct sockaddr_in ProxySAddr, ClientSAddr, TargetSAddr;
struct sockaddr AnySAddr={ AF_INET , { INADDR_ANY } };
struct ClientList
{
    SOCKET sock;
    struct sockaddr_in saddr;
    time_t last;
    struct ClientList *next;
};
struct ClientList *Clients=NULL;

/* Winsock initialization code stolen from netcat :-) */
#ifdef WIN32

/* res_init
   winsock needs to be initialized. Might as well do it as the res_init
   call for Win32 */

void res_init()
{
    WORD wVersionRequested; 
    WSADATA wsaData; 
    int err; 
    wVersionRequested = MAKEWORD(1, 1); 
    
    err = WSAStartup(wVersionRequested, &wsaData); 
 
    if (err != 0) 
        /* Tell the user that we couldn't find a useable */ 
        /* winsock.dll.     */ 
        return; 
    
    /* Confirm that the Windows Sockets DLL supports 1.1.*/ 
    /* Note that if the DLL supports versions greater */ 
    /* than 1.1 in addition to 1.1, it will still return */ 
    /* 1.1 in wVersion since that is the version we */ 
    /* requested. */ 
    
    if ( LOBYTE( wsaData.wVersion ) != 1 || 
         HIBYTE( wsaData.wVersion ) != 1 ) { 
        /* Tell the user that we couldn't find a useable */ 
        /* winsock.dll. */ 
        WSACleanup(); 
        return; 
    }    
}

#endif

void usage()
{
    printf("uproxy [-l loss] [-d delay] [-s msec]\n");
    printf("       LocalPort ServerAddress ServerPort\n");
    printf("-l loss  : percent of packets to be dropped\n");
    printf("-d delay : hold packet for up to delay ms before sending\n");
    printf("-s msec  : sleeps for msec each loop (limits # of pkts processed)\n");
}

typedef struct _send_buf_t {
    struct _send_buf_t* next;
    struct _send_buf_t* prev;
    uint64_t sendms;
    SOCKET sockfd;
    char* buf;
    size_t buflen;
    struct sockaddr_in addr;    
} send_buf_t;

typedef struct _send_queue_t {
    send_buf_t* first;
    send_buf_t* last;
    size_t size;
} send_queue_t;

send_queue_t send_queue;

uint64_t get_timestamp()
{
#ifdef WIN32
    /* Windows implementation */
    struct _timeb t; 
    _ftime(&t);
    return ((uint64_t) t.time)*1000 + t.millitm;
#else
    // Linux implementation
    struct timeval tv;
    if (gettimeofday(&tv, NULL) == 0) {
        uint64_t time;
        time = ((uint64_t) tv.tv_sec)*1000 + tv.tv_usec/1000;
        return time;
    }
    return 0;
#endif
}

send_buf_t* dequeue_send()
{
    send_buf_t* send_buf = send_queue.first;
    if (send_buf) {
        send_queue.first = send_buf->next;
        if (send_queue.first == NULL) {
            send_queue.last = NULL;
        }
        else {
            send_queue.first->prev = NULL;
        }
        send_buf->next = NULL;
        send_queue.size--;
    }
    return send_buf;
}

void delete_send(send_buf_t* send_buf)
{
    if (send_buf) {
        free(send_buf->buf);
        free(send_buf);
    }
}

void enqueue_send(SOCKET sockfd, const char* buf, size_t buflen, 
      const struct sockaddr_in* addr, int delay)
{
    send_buf_t* send_buf;
    assert(buf);
    assert(addr);

    send_buf = malloc(sizeof(send_buf_t));
    send_buf->sendms = get_timestamp() + delay;
    send_buf->sockfd = sockfd;
    send_buf->buf = malloc(buflen);
    memcpy(send_buf->buf, buf, buflen);
    send_buf->buflen = buflen;
    memcpy(&(send_buf->addr), addr, sizeof(send_buf->addr));

    /* Put buffer at end of send queue */
    if (send_queue.last == NULL) {
        assert(send_queue.first == NULL);
        send_queue.first = send_queue.last = send_buf;
        send_buf->prev = NULL;
    }
    else {
        assert(send_queue.first);
        assert(send_queue.last->next == NULL);
        send_queue.last->next = send_buf;
        send_buf->prev = send_queue.last;
        send_queue.last = send_buf;
    }
    send_queue.size++;
    send_buf->next = NULL;
}

void send_buffers()
{
    while ((send_queue.first != NULL) && 
           (send_queue.first->sendms < get_timestamp()))
    {
        send_buf_t* send_buf = dequeue_send();;
        sendto(send_buf->sockfd, send_buf->buf, (int) send_buf->buflen,
               0, (const struct sockaddr *) &(send_buf->addr),
               sizeof(struct sockaddr_in));
        delete_send(send_buf);
    }
}

#define MAX_BUF_LEN 65536

int main(int argc, char *argv[])
{
    char recbuf[MAX_BUF_LEN];
    int recbuflen;
    struct sockaddr_in fromaddr;
    int fromaddrlen;
    
    int bytes_in=0, bytes_out=0, old_bytes_in=0, old_bytes_out=0;
    int bytes_in_dropped=0, bytes_out_dropped=0;
    int old_bytes_in_dropped=0, old_bytes_out_dropped=0;
    int pkts_in=0, pkts_out=0, old_pkts_in=0, old_pkts_out=0;
    int pkts_in_dropped=0, pkts_out_dropped=0;
    int old_pkts_in_dropped=0, old_pkts_out_dropped=0;
    time_t last_stat;
    unsigned long nonzero=1;

    int rv, send;
    int loss = 0;
    int delay = 0;
    int smsec = 0;
    
    printf("UProxy %s - Copyright (C) 2000 Alessandro Staltari\n", VERSION);
    printf("\nDistributed under the terms of the GNU General Public License 2.0\n\n");
    while ((rv = getopt(argc, argv, "l:d:s:")) != -1) {
        switch (rv) {
        case 'l':
            loss = atoi(optarg);
            break;
        case 'd':
            delay = atoi(optarg);
            break;
        case 's':
            smsec = atoi(optarg);
            break;
        default:
            usage();
            exit(EXIT_FAILURE);
        }
    }

    if (argc!=optind+ARGNUM)
    {
        usage();
        exit(EXIT_FAILURE);
    }
#ifdef WIN32
    res_init();
    SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS );
#endif
    
    last_stat=time(NULL);
    ProxySocket=socket(AF_INET, SOCK_DGRAM, 0);
    ioctlsocket ( ProxySocket, FIONBIO, &nonzero);
    memset(&ProxySAddr,0,sizeof(struct sockaddr_in));
    ProxySAddr.sin_family=AF_INET;
    ProxySAddr.sin_port=htons((unsigned short) atoi(argv[optind]));
    ProxySAddr.sin_addr.s_addr=INADDR_ANY;
    bind(ProxySocket, (const struct sockaddr *) &ProxySAddr, sizeof(struct sockaddr_in));
    
    TargetSAddr.sin_family=AF_INET;
    TargetSAddr.sin_port=htons((unsigned short) atoi(argv[optind+2]));
    TargetSAddr.sin_addr.s_addr=inet_addr(argv[optind+1]);


    memset(recbuf, 0, sizeof(recbuf));
    memset(&send_queue, 0, sizeof(send_queue));
    srand(time(NULL));
    while (1)
    {
        fd_set r_fdset;
        struct timeval tv;
        struct ClientList *client=Clients;
        int count=0;
        SOCKET maxfd;
        time_t current_time;
        
        send_buffers();
        
        if (smsec) {
#ifdef WIN32
            Sleep(smsec);
#else
            usleep(smsec*1000);
#endif
        }

        tv.tv_sec=1;
        tv.tv_usec=0;
        FD_ZERO(&r_fdset);
        FD_SET(ProxySocket, &r_fdset);
        maxfd = ProxySocket;
        if (Clients!=NULL)
        {
            struct ClientList *c=Clients;
            
            while (c!=NULL)
            {
                FD_SET(c->sock, &r_fdset);
                if (c->sock > maxfd) 
                    maxfd = c->sock;
                c=c->next;
                count++;
            }
        }
        select((int) (maxfd+1), &r_fdset, NULL, NULL, &tv);
        current_time=time(NULL);        
        if ((current_time-last_stat)>=10)
        {
            printf("in %4d,%6d (d %4d,%6d) "
                   "out %4d,%6d (d %4d,%6d) queued %4d\n", 
                   pkts_in-old_pkts_in, bytes_in-old_bytes_in, 
                   pkts_in_dropped-old_pkts_in_dropped,
                   bytes_in_dropped-old_bytes_in_dropped,                   
                   pkts_out-old_pkts_out, bytes_out-old_bytes_out,
                   pkts_out_dropped-old_pkts_out_dropped,
                   bytes_out_dropped-old_bytes_out_dropped,
                   send_queue.size);
            fflush(stdout);
            old_bytes_out=bytes_out;
            old_bytes_in=bytes_in;
            old_bytes_out_dropped=bytes_out_dropped;
            old_bytes_in_dropped=bytes_in_dropped;
            old_pkts_out=pkts_out;
            old_pkts_in=pkts_in;
            old_pkts_out_dropped=pkts_out_dropped;
            old_pkts_in_dropped=pkts_in_dropped;
            last_stat=current_time;
        }
        if (FD_ISSET(ProxySocket, &r_fdset)) {
            fromaddrlen = sizeof(fromaddr);
            if ((recbuflen=recvfrom(ProxySocket, recbuf, MAX_BUF_LEN, 0, 
                                    (struct sockaddr *) &fromaddr, 
                                    &fromaddrlen))>=0)
            {
                struct ClientList *c=Clients;
                
                while (c!=NULL)
                {
                    if ((c->saddr.sin_addr.s_addr!=fromaddr.sin_addr.s_addr)
                        || (c->saddr.sin_port!=fromaddr.sin_port))
                        c=c->next;
                    else break;
                }
                if (c==NULL)
                {
                    c=malloc(sizeof(struct ClientList));
                    c->next=Clients;
                    Clients=c;
                    memcpy(&(c->saddr), &fromaddr, sizeof(struct sockaddr_in));
                    c->sock=socket(AF_INET, SOCK_DGRAM, 0);
                    ioctlsocket (c->sock, FIONBIO, &nonzero);
                    bind(c->sock, (const struct sockaddr *) &AnySAddr, sizeof(struct sockaddr_in));
/*                    connect(c->sock, (const struct sockaddr *) &TargetSAddr, sizeof(struct sockaddr_in)); */
                    printf("%s:%d Connected.  Socket %d\n", inet_ntoa(c->saddr.sin_addr), 
                           ntohs(c->saddr.sin_port), c->sock); 

                }
/*                printf("socket %d: <- %s:%d\n", ProxySocket, 
                  inet_ntoa(fromaddr.sin_addr), ntohs(fromaddr.sin_port));  */
                send = (rand() % 100) >= loss;
                if (send) {
                    if (delay) {
                        enqueue_send(c->sock, recbuf, recbuflen, &TargetSAddr,
                                     delay);
                    }
                    else {
                        sendto(c->sock, recbuf, recbuflen, 0, 
                               (const struct sockaddr *) &TargetSAddr,
                               sizeof(struct sockaddr_in));
                    }
                }
                else {
                    bytes_out_dropped+=recbuflen;
                    pkts_out_dropped++;
                }
                c->last=current_time;
                bytes_out+=recbuflen;
                pkts_out++;
            }
        }
        client=Clients;
        while(client!=NULL)
        {
            if (FD_ISSET(client->sock, &r_fdset)) {
                fromaddrlen = sizeof(fromaddr);
                if ((recbuflen=recvfrom(client->sock, recbuf, MAX_BUF_LEN, 0, 
                                        (struct sockaddr *) &fromaddr, 
                                        &fromaddrlen))>=0)
                {
/*                    printf("socket %d: <- %s:%d\n", client->sock, 
                      inet_ntoa(fromaddr.sin_addr), ntohs(fromaddr.sin_port));  */
                    send = (rand() % 100) >= loss;
                    if (send) {
                        if ((fromaddr.sin_addr.s_addr == TargetSAddr.sin_addr.s_addr) &&
                            (fromaddr.sin_port == TargetSAddr.sin_port)) {
                            if (delay) {
                                enqueue_send(ProxySocket, recbuf, recbuflen, 
                                             &(client->saddr), delay);
                            }
                            else {
                                sendto(ProxySocket, recbuf, recbuflen, 0, 
                                       (const struct sockaddr *) &(client->saddr),
                                       sizeof(struct sockaddr_in));
                            }
                        }
                        else {
                            struct ClientList *c=Clients;
                
                            while (c!=NULL)
                            {
                                if ((c->saddr.sin_addr.s_addr!=fromaddr.sin_addr.s_addr)
                                    || (c->saddr.sin_port!=fromaddr.sin_port))
                                    c=c->next;
                                else break;
                            }
                            if (c != NULL) {
                                if (delay) {
                                    enqueue_send(c->sock, recbuf, recbuflen, 
                                                 &(client->saddr), delay);
                                }
                                else {
                                    sendto(c->sock, recbuf, recbuflen, 0, 
                                           (const struct sockaddr *) &(client->saddr),
                                           sizeof(struct sockaddr_in));
                                }
                            }
                            else {
                                printf("%s:%d Unknown sender", inet_ntoa(fromaddr.sin_addr), 
                                       ntohs(fromaddr.sin_port));
                            }
                        }
                    }
                    else {
                        bytes_in_dropped+=recbuflen;
                        pkts_in_dropped++;
                    }
                    bytes_in+=recbuflen;
                    pkts_in++;
                }
                else if ((current_time-client->last)>30)
                {
                    struct ClientList *c=Clients;
                    
                    closesocket(client->sock);
                    printf("%s:%d Disconnected.\n", inet_ntoa(client->saddr.sin_addr), 
                      ntohs(client->saddr.sin_port)); 
                    if (client!=Clients)
                    {
                        while (c->next!=client)
                            c=c->next;
                        c->next=c->next->next;
                        free(client);
                        client=c;
                    }
                    else
                    {
                        Clients=client->next;
                        free(client);
                        client=Clients;
                        continue;
                    }
                }
            }
            client=client->next;
        }
    }
}

#!/bin/bash

# Tests unreliable transmission of messages
#
# Runs 2 hosts with VN setup from configuration file
# Hosts take messages from input file and sends lossy messages to each other
# The messages each host received is compared against the messages the other
# host sent.

testdir=`dirname $0`

execdir=${1:-.}
res=${2:-$execdir/res/vntest2}
testfile=${3:-$0}
propfile="$testdir/vntest1.prop"
vntest="$execdir/vntest"

#valgrind="valgrind --tool=memcheck"

if [ ! -e $vntest ] ; then
    echo "$vntest not found"
    exit 1
fi

if [ ! -e $testfile ] ; then
    echo "Cannot open test input $testfile"
    exit 1
fi

if [ ! -e $propfile ] ; then
    echo "Cannot open test properties $propfile"
    exit 1
fi

if [ ! -e $res ] ; then
    mkdir -p $res
fi

((net=0xc1900000))

$valgrind $vntest -n $net -h 123 -c $propfile -m peer1 -t $testfile -s $res/peer1.s -r $res/peer1.r > $res/peer1.log 2>&1 &

$valgrind $vntest -n $net -h 143 -c $propfile -m peer2 -t $testfile -s $res/peer2.s -r $res/peer2.r > $res/peer2.log 2>&1 &

wait

$execdir/vnreadpkts -f 1 $res/peer1.r.d $res/peer1.r.d.time0
$execdir/vnreadpkts -f 1 $res/peer1.s.d $res/peer1.s.d.time0
$execdir/vnreadpkts -f 1 $res/peer2.r.d $res/peer2.r.d.time0
$execdir/vnreadpkts -f 1 $res/peer2.s.d $res/peer2.s.d.time0

if ! cmp $res/peer1.r.d.time0 $res/peer2.s.d.time0; then
    echo "peer1.r.d and peer2.s.d does not compare ... test FAILS"
    exit 1
fi

if [ ! -s $res/peer1.r.d.time0 ]  ; then
    echo "peer1.r.d and peer2.s.d has no content ... test FAILS"
    exit 1
fi

if ! cmp $res/peer1.s.d.time0 $res/peer2.r.d.time0; then
    echo "peer1.s.d and peer2.r.d does not compare ... test FAILS"
    exit 1
fi

if [ ! -s $res/peer1.s.d.time0 ]  ; then
    echo "peer1.s.d and peer2.r.d has no content ... test FAILS"
    exit 1
fi


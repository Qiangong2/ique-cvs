//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnlocal.h"
#include "vntest.h"

/* Test events */

/* Get events */
_VN_event_t* _vnt_get_next_event(_VN_event_t* cur_event, int* n,
                                 uint8_t* buf, size_t bufsize, int timeout)
{
    _VN_event_t* event;
    assert(buf);
    assert(n);
    if (*n > 0) {
        assert(cur_event);
        event = cur_event->next;
        (*n)--;
    }
    else {
        *n = _VN_get_events(buf, (int) bufsize, timeout);
        if (*n > 0) {
            event = (_VN_event_t*) buf;
            (*n)--;
        }
        else {
            event = NULL;
        }
    }
    if (event) {
        if (_VN_TRACE_ON(TRACE_FINE, _VN_SG_TEST)) {
            _vn_print_event(stdout, event);
        }
    }

    return event;
}

int _vnt_drain_events(uint8_t* buf, size_t bufsize, int timeout)
{
    int n;
    int total = 0;
    assert(buf);
    do {
        n = _VN_get_events(buf, (int) bufsize, timeout);
        if (n > 0) {
            _VN_event_t* event = (_VN_event_t*) buf;            
            int i;
            for (i = 0; i < n; i++) {
                /* Clear messages for events */
                switch (event->tag) {
                case _VN_EVT_NEW_MESSAGE: 
                    _VN_recv(
                        ((_VN_new_message_event*) event)->msg_handle, NULL, 0);
                    break;
                case _VN_EVT_ERR:
                    _VN_get_event_msg(
                        ((_VN_err_event*) event)->msg_handle, NULL, 0);
                    break;
                case _VN_EVT_CONNECTION_REQUEST:
                    _VN_get_event_msg(
                        ((_VN_connection_request_event*) event)->msg_handle, 
                        NULL, 0);
                    break;
                case _VN_EVT_NEW_NET_REQUEST:
                    _VN_get_event_msg(
                        ((_VN_new_net_request_event*) event)->msg_handle, 
                        NULL, 0);
                    break;
                }
            }
            total += n;
        }
    } while (n > 0);

    if (total > 0) {
        _VN_TRACE(TRACE_FINE, _VN_SG_TEST,
                  "_vnt_drain_events: %d events ignored\n", total);
    }

    return total;
}

/* Checks for basic event types */
bool _vnt_check_event(int lineno, _VN_event_t* event,
                      _VN_tag_t tag, _VN_callid_t call_id)
{
    assert(event);
    /* Check to see if this is what I expected */
    if (event->tag != tag) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: event tag is %d, %d expected\n",
                  lineno, event->tag, tag);
        return false;
    }

    if (event->call_id != call_id) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d, event call id is %d, %d expected\n", 
                  lineno, event->call_id, call_id);
        return false;
    }

    return true;
}

bool _vnt_check_event_msg(int lineno, void* handle, uint16_t reported_len,
                          const void* expected_msg, uint16_t expected_len)
{
    char msg[_VN_MAX_MSG_LEN];
    int rv;

    assert(reported_len < _VN_MAX_MSG_LEN);
    rv = _VN_get_event_msg(handle, msg, sizeof(msg));

    if (expected_msg) {
        if (rv < 0) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                      "%d: Unable to retrieve message: %d\n", rv);
            return false;
        }

        if (reported_len != expected_len || rv != expected_len) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                      "%d: msglen is %u, rv is %u, expected %u\n",
                      lineno, reported_len, rv, expected_len);
            return false;
        }

        if (memcmp(expected_msg, msg, expected_len) != 0) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                      "%d: message content differs\n", lineno);
            return false;
        }
    }

    return true;
}
                          
bool _vnt_check_new_message_event(int lineno, _VN_event_t* event,
                                  _VN_callid_t call_id,
                                  _VN_net_t net_id, _VN_host_t from, _VN_host_t to,
                                  _VN_port_t port, 
                                  uint8_t* opthdr, uint8_t opthdr_size)
{
    _VN_new_message_event* msg_event = 
        (_VN_new_message_event*) event;
            
    if (!_vnt_check_event(lineno, event, 
                          _VN_EVT_NEW_MESSAGE, call_id)) {
        return false;
    }

    if (net_id && msg_event->netid != net_id) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: net id is 0x%08x, 0x%08x expected\n", 
                  lineno, msg_event->netid, net_id);
        return false;                    
    }

    if ((from != _VN_HOST_INVALID) && (msg_event->fromhost != from)) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: from host id is %u, %u expected\n", 
                  lineno, msg_event->fromhost, from);
        return false;                    
    }

    if ((to != _VN_HOST_INVALID) && (msg_event->tohost != from)) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: to host id is %u, %u expected\n", 
                  lineno, msg_event->tohost, to);
        return false;                    
    }

    if (port && (msg_event->port != port)) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: port is %u, %u expected\n", 
                  lineno, msg_event->port, port);
        return false;                    
    }

    if (opthdr) {
        if (msg_event->opthdr_size != opthdr_size) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                      "%d: opthdr_size is %u, %u expected\n", 
                      lineno, msg_event->opthdr_size, opthdr_size);
            return false;
        }

        if (memcmp(msg_event->opthdr, opthdr, opthdr_size) != 0) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                      "%d: opthdr differs\n", lineno);
            return false;
        }
    }

    return true;
}

bool _vnt_check_new_network_event(int lineno, _VN_event_t* event,
                                  _VN_callid_t call_id, 
                                  uint32_t netmask, bool isLocal)
{
    _VN_net_t net_id;
    _VN_host_t host_id, maxhost;

    _VN_new_network_event* net_event = 
        (_VN_new_network_event*) event;
            
    if (!_vnt_check_event(lineno, event, 
                          _VN_EVT_NEW_NETWORK, call_id)) {
        return false;
    }
            
    /* Check net id */
    net_id = _VN_addr2net(net_event->myaddr);
    if (isLocal) {
        if (!_VN_NETID_IS_LOCAL(net_id)) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                      "%d: net id is 0x%08x, local net expected\n", 
                      lineno, net_id);
            return false;            
        }
    }
    else {
        if (!_VN_NETID_IS_GLOBAL(net_id)) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                      "%d: net id is 0x%08x, global net expected\n", 
                      lineno, net_id);
            return false;            
        }
    }
            
    if ((net_id & ~netmask) != 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: net id is 0x%08x, netmask is 0x%08x\n", 
                  lineno, net_id, netmask);
        return false;            
    }
    
    /* Check host id */
    host_id = _VN_addr2host(net_event->myaddr);
    if ((host_id & netmask) != 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: host id is 0x%08x, netmask is 0x%08x\n", 
                  lineno, host_id, netmask);
        return false;            
    }
    
    maxhost = _vn_get_maxhost(net_event->myaddr);
    if (host_id > maxhost) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: host id is 0x%08x, netmask is 0x%08x, "
                  "maxhost is %u\n", 
                  lineno, host_id, netmask, maxhost);
        return false;            
    }

    return true;
}

bool _vnt_check_new_connection_event(int lineno, _VN_event_t* event,
                                     _VN_callid_t call_id, 
                                     uint32_t netmask)
{
    _VN_net_t net_id;
    _VN_host_t host_id, maxhost;

    _VN_new_network_event* net_event = 
        (_VN_new_network_event*) event;
            
    if (!_vnt_check_event(lineno, event, 
                          _VN_EVT_NEW_CONNECTION, call_id)) {
        return false;
    }
            
    /* Check net id */
    net_id = _VN_addr2net(net_event->myaddr);
    if ((net_id & ~netmask) != 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: net id is 0x%08x, netmask is 0x%08x\n", 
                  lineno, net_id, netmask);
        return false;            
    }
    
    /* Check host id */
    host_id = _VN_addr2host(net_event->myaddr);
    if ((host_id & netmask) != 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: host id is 0x%08x, netmask is 0x%08x\n", 
                  lineno, host_id, netmask);
        return false;            
    }
    
    maxhost = _vn_get_maxhost(net_event->myaddr);
    if (host_id > maxhost) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: host id is 0x%08x, netmask is 0x%08x, maxhost is %u\n", 
                  lineno, host_id, netmask, maxhost);
        return false;            
    }

    return true;
}

bool _vnt_check_net_config_event(int lineno, _VN_event_t* event,
                                 _VN_callid_t call_id, _VN_net_t netid, 
                                 uint16_t n_joined, uint16_t n_left,
                                 _VN_host_t peers[], uint8_t reason)
{
    int i;
    _VN_net_config_event* net_event = 
        (_VN_net_config_event*) event;

    /* Check to see if this is what I expected */
    if (!_vnt_check_event(lineno, event, 
                          _VN_EVT_NET_CONFIG, call_id)) {
        return false;
    }

    if (net_event->netid != netid) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: net id = 0x%08x, 0x%08x expected\n",
                  lineno, net_event->netid, netid);
        return false;
    }

    if ((net_event->n_left != n_left) || (net_event->n_joined != n_joined)) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: joined=%u, left=%u, joined=%u, left=%u expected\n", 
                  lineno, net_event->n_joined, net_event->n_left,
                  n_joined, n_left );
        return false;
    }

    if (peers) {
        for (i = 0; i < n_left + n_joined; i++) {
            if (net_event->peers[i] != peers[i]) {
                _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                          "%d: peer %d is %u, %u expected\n", 
                          lineno, i, net_event->peers[i], peers[i]);
                return false;
            }
        }
    }

    if (reason) {
        if (net_event->reason != reason) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                      "%d: reason is %u, %u expected\n", 
                      lineno, net_event->reason, reason);
            return false;
        }
    }

    return true;
}

bool _vnt_check_net_disconnected_event(int lineno, _VN_event_t* event,
                                       _VN_callid_t call_id, _VN_net_t netid)
{
    _VN_net_disconnected_event* net_event = 
        (_VN_net_disconnected_event*) event;

    /* Check to see if this is what I expected */
    if (!_vnt_check_event(lineno, event, 
                          _VN_EVT_NET_DISCONNECTED, call_id)) {
        return false;
    }

    if (net_event->netid != netid) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: net id = 0x%08x, 0x%08x expected\n",
                  lineno, net_event->netid, netid);
        return false;
    }

    return true;
}

bool _vnt_check_connection_accepted_event(int lineno, _VN_event_t* event,
                                          _VN_callid_t call_id, _VN_addr_t owner)
{
    _VN_host_t host_id, maxhost;
    uint32_t netmask;

    _VN_connection_accepted_event* net_event = 
        (_VN_connection_accepted_event*) event;

    /* Check to see if this is what I expected */
    if (!_vnt_check_event(lineno, event, 
                          _VN_EVT_CONNECTION_ACCEPTED, call_id)) {
        return false;
    }

    /* Check host id */
    netmask = _vn_get_netmask(net_event->myaddr);
    host_id = _VN_addr2host(net_event->myaddr);
    if ((host_id & netmask) != 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: host id is 0x%08x, netmask is 0x%08x\n", 
                  lineno, host_id, netmask);
        return false;            
    }
    
    maxhost = _vn_get_maxhost(net_event->myaddr);
    if (host_id > maxhost) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: host id is 0x%08x, netmask is 0x%08x, maxhost is %u\n", 
                  lineno, host_id, netmask, maxhost);
        return false;            
    }

    assert(_VN_addr2host(net_event->myaddr) != net_event->owner);

    if (owner != _VN_ADDR_INVALID) {
        _VN_addr_t owner_addr = _VN_make_addr(_VN_addr2net(net_event->myaddr),
                                              net_event->owner);
        if (owner_addr != owner) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                      "%d: Owner address is 0x%08x, 0x%08x expected\n", 
                      lineno, owner_addr, owner);
            return false;
        }
    }
    return true;
}

bool _vnt_check_connection_request_event(int lineno, _VN_event_t* event,
                                         _VN_callid_t call_id,
                                         uint64_t request_id, _VN_addr_t owner,
                                         const void* msg, uint16_t msglen)
{
    _VN_connection_request_event* net_event = 
        (_VN_connection_request_event*) event;

    /* Check to see if this is what I expected */
    if (!_vnt_check_event(lineno, event, 
                          _VN_EVT_CONNECTION_REQUEST, call_id)) {
        return false;
    }

    /* Check request_id and owner */
    if (request_id && (request_id != net_event->request_id)) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: request id is 0x%016x, expected 0x%016x\n",
                  lineno, net_event->request_id, request_id);
        return false;
    }

    if ((owner != _VN_ADDR_INVALID) && (owner != net_event->addr)) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: owner is 0x%08x, expected 0x%08x\n",
                  lineno, net_event->addr, owner);
        return false;
    }

    /* Check message */
    return _vnt_check_event_msg(lineno,
                                net_event->msg_handle, net_event->msglen,
                                msg, msglen);
}

bool _vnt_check_new_net_request_event(int lineno, _VN_event_t* event,
                                      _VN_callid_t call_id,
                                      uint64_t request_id, uint32_t netmask,
                                      _VN_addr_t server_addr, _VN_host_t from_host,
                                      const void* msg, uint16_t msglen)
{
    _VN_new_net_request_event* net_event = 
        (_VN_new_net_request_event*) event;

    /* Check to see if this is what I expected */
    if (!_vnt_check_event(lineno, event, 
                          _VN_EVT_NEW_NET_REQUEST, call_id)) {
        return false;
    }

    /* Check request_id and addresses */
    if (request_id && (request_id != net_event->request_id)) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: request id is 0x%016x, expected 0x%016x\n",
                  lineno, net_event->request_id, request_id);
        return false;
    }

    if (netmask && (netmask != net_event->netmask)) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: netmask is 0x%08x, expected 0x%08x\n",
                  lineno, net_event->netmask, netmask);
        return false;
    }

    if ((server_addr != _VN_ADDR_INVALID) && (server_addr != net_event->addr)) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: server is 0x%08x, expected 0x%08x\n",
                  lineno, net_event->addr, server_addr);
        return false;
    }

    if ((from_host != _VN_HOST_INVALID) && (from_host != net_event->from_host)) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: from_host is %u, expected %u\n",
                  lineno, net_event->from_host, from_host);
        return false;
    }

    /* Check message */
    return _vnt_check_event_msg(lineno,
                                net_event->msg_handle, net_event->msglen,
                                msg, msglen);
}

bool _vnt_check_device_update_event(int lineno, _VN_event_t* event,
                                    _VN_callid_t call_id, 
                                    _VN_guid_t guid, uint8_t reason,
                                    uint16_t n_added, uint16_t n_removed,
                                    uint32_t services[])
{
    int i;
    _VN_device_update_event* dev_event = 
        (_VN_device_update_event*) event;

    /* Check to see if this is what I expected */
    if (!_vnt_check_event(lineno, event, 
                          _VN_EVT_DEVICE_UPDATE, call_id)) {
        return false;
    }

    if (!_vn_guid_match(&dev_event->guid, &guid)) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: guid = 0x%08x-%08x, 0x%08x-%08x expected\n",
                  lineno, 
                  dev_event->guid.device_type, dev_event->guid.chip_id,
                  guid.device_type, guid.chip_id);
        return false;
    }

    if ((dev_event->nservices_add != n_added) || 
        (dev_event->nservices_rem != n_removed)) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: added=%u, removed=%u, added=%u, removed=%u expected\n", 
                  lineno, dev_event->nservices_add, dev_event->nservices_rem,
                  n_added, n_removed );
        return false;
    }

    if (services) {
        for (i = 0; i < n_added + n_removed; i++) {
            if (dev_event->services[i] != services[i]) {
                _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                          "%d: service %d is %u, %u expected\n", 
                          lineno, i, dev_event->services[i], services[i]);
                return false;
            }
        }
    }

    if (reason) {
        if (dev_event->reason != reason) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                      "%d: reason is %u, %u expected\n", 
                      lineno, dev_event->reason, reason);
            return false;
        }
    }

    return true;
}
                                    

bool _vnt_check_err_event(int lineno, _VN_event_t* event, 
                          _VN_callid_t call_id, int rv,
                          const void* msg, uint16_t msglen)
{
    _VN_err_event* err_event = (_VN_err_event*) event;

    /* Check to see if this is what I expected */
    if (!_vnt_check_event(lineno, event, 
                          _VN_EVT_ERR, call_id)) {
        return false;
    }

    if (err_event->errcode != rv) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: Event err is %d, %d expected\n", 
                  lineno, err_event->errcode, rv);
        return false;
    }

    /* Check message */
    return _vnt_check_event_msg(lineno, 
                                err_event->msg_handle, err_event->msglen,
                                msg, msglen);
}
 
bool _vnt_wait_err_event(int lineno, int call_id, int rv, int timeout)
{
    int n;
    uint8_t evt_buf[1024];

    n = _VN_get_events(evt_buf, (int) sizeof(evt_buf), timeout);
    if (n <= 0) {
        /* Oops, didn't get event */
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: _VN_get_events returned %d\n", lineno, n);
        return false;
    }
    else { 
        return _vnt_check_err_event(lineno, (_VN_event_t*) evt_buf,
                                    call_id, rv, NULL, 0);
    }
}

/* Check return codes */
bool _vnt_check_return_code(int lineno, int exp, int rv)
{
    if (exp == _VN_RV_CALLID) {
        if (rv <= 0) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                      "%d: Got RETURN %d, callid expected\n",
                      lineno, rv);
            return false;
        }
    }
    else {
        if (rv != exp) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                      "%d: Got RETURN %d, %d expected\n", 
                      lineno, rv, exp);
            return false;
        }
    }

    return true;
}

/* Check for messages containing return codes */
bool _vnt_check_msg_return_code(int lineno, _VN_event_t* event,
                                _VN_addr_t from, _VN_port_t port,
                                int exp, int* res)
{
    _VN_host_t from_host = _VN_addr2host(from);
    _VN_net_t net_id = _VN_addr2net(from);
    _VN_new_message_event* msg_event;
    int bytes;
    char msg[_VN_MAX_MSG_LEN];
    memset(msg, 0, sizeof(msg));

    if (res) { *res = 0; };   
    if (!_vnt_check_new_message_event(__LINE__, event,
                                      0, net_id, from_host, _VN_HOST_INVALID, 
                                      port, NULL, 0)) {
        return false;
    }
    
    msg_event = (_VN_new_message_event*) event;
    bytes = _VN_recv(msg_event->msg_handle, msg, sizeof(msg));
    if (bytes > 0) {
        int rv;
        int n = sscanf(msg, "RETURN %d", &rv);
        if (_VN_TRACE_ON(TRACE_FINE, _VN_SG_TEST)) {
            printf("Got message with %d bytes\n", bytes);
            _vn_dbg_dump_buf(stdout, msg, bytes);
        }
        if (n == 1) {
            if (res) { *res = rv; };
            return _vnt_check_return_code(lineno, exp, rv);
        }
        else {
            _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                      "%d: Did not get RETURN message\n", lineno);
            return false;
        }
    }
    else if (bytes == 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: Got empty message, RETURN message expected\n",
                  lineno);
        return false;
    }
    else {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: Got _VN_recv error %d, RETURN message expected\n",
                  bytes, lineno);
        return false;
    }
    
    return true;
}

/* Check for messages containing embedded event */
bool _vnt_check_msg_event(int lineno, _VN_event_t* event, _VN_addr_t from,
                          _VN_port_t port, _VN_guid_t guid, 
                          uint8_t* resbuf, size_t resbuf_size, _VN_guid_t* pGuid)
{
    _VN_host_t from_host = _VN_addr2host(from);
    _VN_net_t net_id = _VN_addr2net(from);
    _VN_new_message_event* msg_event;
    int bytes;
    char msg[_VN_MAX_MSG_LEN];
    memset(msg, 0, sizeof(msg));

    if (resbuf) { memset(resbuf, 0, resbuf_size); }
    if (pGuid) { 
        _vn_guid_set_invalid(pGuid);
    }

    if (!_vnt_check_new_message_event(__LINE__, event,
                                      0, net_id, from_host, _VN_HOST_INVALID, 
                                      port, NULL, 0)) {
        return false;
    }
    
    msg_event = (_VN_new_message_event*) event;
    bytes = _VN_recv(msg_event->msg_handle, msg, sizeof(msg));
    if (bytes > 0) {
        _VN_guid_t from_guid;
        int n = sscanf(msg, "EVENT FROM 0x%x %u", 
                       &from_guid.device_type, &from_guid.chip_id);
        if (_VN_TRACE_ON(TRACE_FINE, _VN_SG_TEST)) {
            printf("Got message with %d bytes\n", bytes);
            _vn_dbg_dump_buf(stdout, msg, bytes);
        }
        if (n == 2) {
            int textbytes = (int) strlen(msg) + 1;
            _VN_event_t* new_event = (_VN_event_t*) (msg + textbytes);
            if (resbuf) { 
                assert(bytes >= textbytes + (int) sizeof(_VN_event_t));
                assert(bytes = textbytes + new_event->size);
                assert(resbuf_size >= new_event->size);
                memcpy(resbuf, new_event, new_event->size); 
                if (_VN_TRACE_ON(TRACE_FINE, _VN_SG_TEST)) {
                    _vn_print_event(stdout, new_event);
                }
            }
            if (pGuid) { *pGuid = from_guid; }
            if (!_vn_guid_is_invalid(&guid) && 
                !_vn_guid_match(&from_guid, &guid)) {
                _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                          "%d: Got EVENT from 0x%08x %u, 0x%08x %u expected\n",
                          lineno, from_guid.device_type, from_guid.chip_id,
                          guid.device_type, guid.chip_id);
                return false;
            }
        }
        else {
            _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                      "%d: Did not get expected EVENT message format\n", lineno);
            return false;
        }
    }
    else if (bytes == 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: Got empty message, EVENT message expected\n", lineno);
        return false;
    }
    else {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: Got _VN_recv error %d, EVENT message expected\n",
                  bytes, lineno);
        return false;
    }
    
    return true;
}

/* Checks for several events */

bool _vnt_check_remote_call_return_code(_VN_guid_t guid, 
                                        _VN_addr_t from_addr, _VN_port_t port,
                                        int timeout, int exp, int* res)
{
    uint8_t evt_buf[1024];
    int n = 0, lineno;
    _VN_event_t* event = NULL;
    bool okay = true;;

    /* Check command returned okay */
    event = _vnt_get_next_event(event, &n, evt_buf, sizeof(evt_buf), timeout);
    if (event == NULL) { lineno = __LINE__; goto fail; }

    if (!_vnt_check_msg_return_code(__LINE__, event, from_addr, port,
                                    exp, res)) {
        lineno = __LINE__; goto fail;
    }

    /* Wait for events to drain */
    _vnt_drain_events(evt_buf, sizeof(evt_buf), timeout);
    event = NULL; n = 0;
    goto done;

fail:
    okay = false;
    _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
              "_vnt_check_remote_call_return_code: Failed in %s, line %d\n", 
              __FILE__, lineno);
done:
    return okay;
}    

bool _vnt_check_pending_call_failed(_VN_guid_t guid, 
                                    _VN_addr_t from_addr, _VN_port_t port,
                                    int timeout, int err_code)
                                   
{
    uint8_t evt_buf[1024], evt_buf2[1024];
    int n = 0, lineno;
    _VN_event_t* event = NULL;
    _VN_callid_t callid;
    bool okay = true;;

    /* Check command returned call id */
    event = _vnt_get_next_event(event, &n, evt_buf, sizeof(evt_buf), timeout);
    if (event == NULL) { lineno = __LINE__; goto fail; }

    if (!_vnt_check_msg_return_code(__LINE__, event, from_addr, port,
                                    _VN_RV_CALLID, &callid)) {
        lineno = __LINE__; goto fail;
    }

    /* Check peer got _VN_EVT_ERR */
    event = _vnt_get_next_event(event, &n, evt_buf, sizeof(evt_buf), timeout);
    if (event == NULL) { lineno = __LINE__; goto fail; }

    if (!_vnt_check_msg_event(__LINE__, event, from_addr, port,
                              guid, evt_buf2, sizeof(evt_buf2), NULL)) {
        lineno = __LINE__; goto fail;
    }

    if (!_vnt_check_err_event(__LINE__, (_VN_event_t*) evt_buf2, 
                              callid, err_code, NULL, 0)) {
        lineno = __LINE__; goto fail;
    }
    
    /* Wait for events to drain */
    _vnt_drain_events(evt_buf, sizeof(evt_buf), timeout);
    event = NULL; n = 0;
    goto done;

fail:
    okay = false;
    _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
              "_vnt_check_pending_call_failed: Failed in %s, line %d\n",
              __FILE__, lineno);
done:
    return okay;
}    

bool _vnt_check_create_succeeded(_VN_guid_t guid, _VN_addr_t from_addr,
                                 _VN_port_t port, int timeout, 
                                 uint32_t netmask, bool isLocal,
                                 _VN_addr_t* test_addr, _VN_host_t* test_host)
{
    char cmdbuf[1024];
    uint8_t evt_buf[1024], evt_buf2[1024];
    int n = 0, lineno, i, rv;
    _VN_event_t* event = NULL;
    _VN_callid_t callid;
    bool okay = true;
    _VN_addr_t new_addr;
    _VN_host_t new_host;
 
    if (isLocal) {
        /* Local */

        /* Check command returned call id  */
        event = _vnt_get_next_event(event, &n, evt_buf, sizeof(evt_buf), timeout);
        if (event == NULL) { lineno = __LINE__; goto fail; }
        if (!_vnt_check_msg_return_code(__LINE__, event, from_addr, port,
                                        _VN_RV_CALLID, &callid)) {
            lineno = __LINE__; goto fail;
        }
        
    }
    else {
        /* Global */
        uint64_t request_id = 0;
        _VN_addr_t server_addr;
        
        /* Check command returned call id */ 
        /* Check server got _VN_EVT_NEW_NET_REQUEST */
        for (i = 1; i <= 2; i++) {
            event = _vnt_get_next_event(event, &n, evt_buf,
                                        sizeof(evt_buf), timeout);
            
            if (event == NULL) { lineno = __LINE__; goto fail; }

            if (event->tag == _VN_EVT_NEW_MESSAGE) {           
                /* Check command returned call id */ 
                if (!_vnt_check_event(__LINE__, event, 
                                      _VN_EVT_NEW_MESSAGE, 0)) {
                    lineno = __LINE__; goto fail;
                }
                
                if (!_vnt_check_msg_return_code(__LINE__, event, from_addr, port,
                                                _VN_RV_CALLID, &callid)) {
                    lineno = __LINE__; goto fail;
                }
            }
            else if (event->tag == _VN_EVT_NEW_NET_REQUEST) {    
                /* Save event to check against later */
                memcpy(evt_buf2, event, event->size);
            }
            else {
                lineno = __LINE__; goto fail;
            }
        }
        
        /* Check server got _VN_EVT_NEW_NET_REQUEST event */
        server_addr = _vn_get_default_localaddr(_VN_addr2net(from_addr));
        request_id = _VN_REQID(from_addr, callid);
        if (!_vnt_check_new_net_request_event(__LINE__, 
                                              (_VN_event_t*) evt_buf2,
                                              _VN_MSG_ID_INVALID,
                                              request_id, netmask, server_addr,
                                              _VN_addr2host(from_addr),
                                              NULL, 0)) 
        {
            lineno = __LINE__; goto fail;
        }

        /* Have server accept net */
        sprintf(cmdbuf, "acceptnew %llu %u", request_id, netmask);
        rv = _vnt_proc_command(cmdbuf);
        if (rv < 0) { lineno = __LINE__; goto fail; }
    }

    /* Check peer guid got _VN_EVT_NEW_NETWORK */
    event = _vnt_get_next_event(event, &n, evt_buf, sizeof(evt_buf), timeout);
    if (event == NULL) { lineno = __LINE__; goto fail; }
    
    if (!_vnt_check_msg_event(__LINE__, event, from_addr, port,
                              guid, evt_buf2, sizeof(evt_buf2), NULL)) {
        lineno = __LINE__; goto fail;
    }
    
    if (!_vnt_check_new_network_event(__LINE__, (_VN_event_t*) evt_buf2, 
                                      callid, netmask, isLocal)) {
        lineno = __LINE__; goto fail;
    }
    
    new_addr = ((_VN_new_network_event* ) evt_buf2)->myaddr;
    new_host = _VN_addr2host(new_addr);
    
    if (test_addr) {
        *test_addr = new_addr;
    }
    if (test_host) {
        *test_host = new_host;
    }

    /* Wait for extra events to drain */
    _vnt_drain_events(evt_buf, sizeof(evt_buf), timeout);
    event = NULL; n = 0;
    
    goto done;

fail:
    okay = false;
    _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
              "_vnt_check_create_succeeded: Failed in %s, line %d\n",
              __FILE__, lineno);
done:
    return okay;
}


bool _vnt_check_connect_rejected(_VN_guid_t guid, _VN_addr_t from_addr,
                                 _VN_port_t port, int timeout, 
                                 _VN_callid_t listen_call,
                                 _VN_guid_t owner_guid, 
                                 _VN_addr_t from_owner_addr,
                                 _VN_addr_t owner_addr,
                                 char* msg)
{
    char cmdbuf[1024];
    uint8_t evt_buf[1024], evt_buf2[1024];
    int i, rv, n = 0, lineno;
    _VN_event_t* event = NULL;
    _VN_callid_t callid;
    bool okay = true;
    bool got_command_resp = false;
    uint64_t request_id;
    
    /* Check command returned call id */ 
    /* Check owner got _VN_EVT_CONNECTION_REQUEST event */
    for (i = 1; i <= 2; i++) {
        _VN_guid_t from_guid;
        _VN_addr_t from_vn_addr;
        _VN_new_message_event* msg_event;

        event = _vnt_get_next_event(event, &n, evt_buf, 
                                    sizeof(evt_buf), timeout);
        if (event == NULL) { lineno = __LINE__; goto fail; }
        
        if (!_vnt_check_event(__LINE__, event, 
                              _VN_EVT_NEW_MESSAGE, 0)) {
            lineno = __LINE__; goto fail;
        }
        
        msg_event = (_VN_new_message_event*) event;
        from_vn_addr = _VN_make_addr(msg_event->netid, msg_event->fromhost);
        _VN_get_guid(from_vn_addr, &from_guid);

        if (_vn_guid_match(&from_guid, &guid) && (!got_command_resp)) {
            /* Check command returned call id */ 
            if (!_vnt_check_msg_return_code(__LINE__, event, from_addr, port,
                                            _VN_RV_CALLID, &callid)) {
                lineno = __LINE__; goto fail;
            }

            got_command_resp = true;
        }
        else {
            /* Check owner got _VN_EVT_CONNECTION_REQUEST event */
            if (!_vnt_check_msg_event(__LINE__, event, from_owner_addr, port,
                                      owner_guid, evt_buf2, sizeof(evt_buf2),
                                      NULL)) {
                lineno = __LINE__; goto fail;
            }
        }
    }

    /* Check owner's _VN_EVT_CONNECTION_REQUEST event has correct form */
    request_id = _VN_HS_REQID(guid, callid);
    if (!_vnt_check_connection_request_event(__LINE__, 
                                             (_VN_event_t*) evt_buf2,
                                             listen_call,
                                             request_id, owner_addr,
                                             NULL, 0)) 
    {
        lineno = __LINE__; goto fail;
    }
            
    /* Have owner reject net */
    sprintf(cmdbuf, "tell guid%u %u command accept %u %u 0 %s",
            owner_guid.chip_id, port, guid.chip_id, callid, msg);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }

    /* Check owner got command returned okay */
    /* Check peer got _VN_EVT_ERR */
    got_command_resp = false;
    for (i = 1; i <= 2; i++) {
        _VN_guid_t from_guid;
        _VN_addr_t from_vn_addr;
        _VN_new_message_event* msg_event;

        event = _vnt_get_next_event(event, &n, evt_buf, 
                                    sizeof(evt_buf), timeout);
        if (event == NULL) { lineno = __LINE__; goto fail; }
        
        if (!_vnt_check_event(__LINE__, event, 
                              _VN_EVT_NEW_MESSAGE, 0)) {
            lineno = __LINE__; goto fail;
        }
        
        msg_event = (_VN_new_message_event*) event;
        from_vn_addr = _VN_make_addr(msg_event->netid, msg_event->fromhost);
        _VN_get_guid(from_vn_addr, &from_guid);

        if (_vn_guid_match(&from_guid, &owner_guid) && !got_command_resp) {
            /* Check owner got command returned okay */
            if (!_vnt_check_msg_return_code(__LINE__, event, from_owner_addr,
                                            port, _VN_ERR_OK, NULL)) {
                lineno = __LINE__; goto fail;
            }
            
            got_command_resp = true;
        }
        else {
            /* Check peer got _VN_EVT_ERR */
            if (!_vnt_check_msg_event(__LINE__, event, from_addr, port,
                                      guid, evt_buf2, sizeof(evt_buf2), NULL)) 
            {
                lineno = __LINE__; goto fail;
            }

            if (!_vnt_check_err_event(__LINE__, (_VN_event_t*) evt_buf2, 
                                      callid, _VN_ERR_REJECTED, NULL, 0)) {
                lineno = __LINE__; goto fail;
            }
        }
    }

    /* Wait for events to drain */
    _vnt_drain_events(evt_buf, sizeof(evt_buf), timeout);
    event = NULL; n = 0;
    goto done;

fail:
    okay = false;
    _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
              "_vnt_check_connect_rejected: Failed in %s, line %d\n",
              __FILE__, lineno);
done:
    return okay;
}

bool _vnt_check_connect_succeeded(_VN_guid_t guid, _VN_addr_t from_addr,
                                  _VN_port_t port, int timeout, 
                                  _VN_net_t net, _VN_callid_t listen_call,
                                  _VN_guid_t owner_guid, 
                                  _VN_addr_t from_owner_addr,
                                  _VN_addr_t owner_addr,
                                  int nlocal, int npeers, _VN_host_t peers[],
                                  _VN_addr_t* test_addr, _VN_host_t* test_host)
{
    char cmdbuf[1024];
    uint8_t evt_buf[1024], evt_buf2[1024];
    int rv, i, n = 0, lineno;
    _VN_event_t* event = NULL;
    _VN_callid_t callid;
    bool okay = true;
    _VN_host_t* hostids;
    _VN_addr_t new_addr = _VN_ADDR_INVALID;
    _VN_host_t new_host = _VN_HOST_INVALID;
    int nevt_local = 0;
    uint8_t reason;
    uint64_t request_id;
    bool got_accepted = false;
    bool got_command_resp = false;

    /* Check command returned call id */ 
    /* Check owner got _VN_EVT_CONNECTION_REQUEST event */
    for (i = 1; i <= 2; i++) {
        _VN_guid_t from_guid;
        _VN_addr_t from_vn_addr;
        _VN_new_message_event* msg_event;

        event = _vnt_get_next_event(event, &n, evt_buf,
                                    sizeof(evt_buf), timeout);

        if (event == NULL) { lineno = __LINE__; goto fail; }
        
        if (!_vnt_check_event(__LINE__, event, 
                              _VN_EVT_NEW_MESSAGE, 0)) {
            lineno = __LINE__; goto fail;
        }
        
        msg_event = (_VN_new_message_event*) event;
        from_vn_addr = _VN_make_addr(msg_event->netid, msg_event->fromhost);
        _VN_get_guid(from_vn_addr, &from_guid);
        
        if (_vn_guid_match(&from_guid, &guid) && (!got_command_resp)) {

            if (!_vnt_check_msg_return_code(__LINE__, event, from_addr, port,
                                            _VN_RV_CALLID, &callid)) {
                lineno = __LINE__; goto fail;
            }

            got_command_resp = true;
        }
        else {    
            /* Check owner got _VN_EVT_CONNECTION_REQUEST event */
            if (!_vnt_check_msg_event(__LINE__, event, from_owner_addr, port,
                                      owner_guid, evt_buf2, sizeof(evt_buf2),
                                      NULL)) {
                lineno = __LINE__; goto fail;
            }
        }
    }

    /* Check owner's _VN_EVT_CONNECTION_REQUEST event has correct form */
    request_id = _VN_HS_REQID(guid, callid);
    if (!_vnt_check_connection_request_event(__LINE__,
                                             (_VN_event_t*) evt_buf2,
                                             listen_call,
                                             request_id, owner_addr,
                                             NULL, 0)) 
    {
        lineno = __LINE__; goto fail;
    }
    /* Have owner accept net */
    sprintf(cmdbuf, "tell guid%u %u command accept %u %u",
            owner_guid.chip_id, port, guid.chip_id, callid);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }

    /* Check owner got command returned okay */
    /* Check we got 1 _VN_EVT_CONNECTION_ACCEPTED EVENT and
                  npeers+1 _VN_EVT_NET_CONFIG messages */
    got_command_resp = false;
    for (i = 1; i <= npeers+3; i++) {
        _VN_guid_t invalid_guid;
        _VN_guid_t from_guid;
        int njoined;
        _VN_callid_t callid2;
        _VN_addr_t from_vn_addr;
        _VN_new_message_event* msg_event;

        _vn_guid_set_invalid(&invalid_guid);
        event = _vnt_get_next_event(event, &n, evt_buf, sizeof(evt_buf),
                                    timeout);

        if (event == NULL) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                      "_vnt_check_connect_succeeded: Failed getting %d/%d events\n",
                      i, npeers+3);
            lineno = __LINE__; goto fail; 
        }

        if (!_vnt_check_event(__LINE__, event, 
                              _VN_EVT_NEW_MESSAGE, 0)) {
            lineno = __LINE__; goto fail;
        }
        
        msg_event = (_VN_new_message_event*) event;
        from_vn_addr = _VN_make_addr(msg_event->netid, msg_event->fromhost);
        _VN_get_guid(from_vn_addr, &from_guid);

        if (_vn_guid_match(&from_guid, &owner_guid) && !got_command_resp) {
            /* Check owner got command returned okay */
            if (!_vnt_check_msg_return_code(__LINE__, event, from_owner_addr,
                                            port, _VN_ERR_OK, NULL)) {
                lineno = __LINE__; goto fail;
            }
            got_command_resp = true;
            continue;
        }
        
        /* Don't care who the message is from */
        if (!_vnt_check_msg_event(__LINE__, event, _VN_ADDR_INVALID, port,
                                  invalid_guid, evt_buf2, sizeof(evt_buf2),
                                  &from_guid)) {
            lineno = __LINE__; goto fail;
        }

        if (_vn_guid_match(&from_guid, &guid)) {
            if (nevt_local < nlocal) {
                /* Existing local hosts */
                njoined = 1;
                callid2 = 0;
                if (new_host == _VN_HOST_INVALID) {
                    /* Don't know who the new host is yet */
                    hostids = NULL;
                }
                else {
                    hostids = &new_host;
                }
                reason = _VN_EVT_R_HOST_JOINED;
            }
            else if (!got_accepted) {
                /* Check we got _VN_EVT_CONNECTION_ACCEPTED EVENT */
                if (!_vnt_check_connection_accepted_event(
                        __LINE__, (_VN_event_t*) evt_buf2, 
                        callid, owner_addr)) {
                    lineno = __LINE__; goto fail;
                }
                
                new_addr = 
                    ((_VN_connection_accepted_event* ) evt_buf2)->myaddr;

                new_host = _VN_addr2host(new_addr);
                if (test_addr) {
                    *test_addr = new_addr;
                }
                if (test_host) {
                    *test_host = new_host;
                }
                got_accepted = true;
                continue;
            }
            else {
                /* New host */
                /* For now, all peers (including new peer + owner) */
                njoined = npeers + 1; 
                callid2 = callid;
                hostids = peers;
                reason = _VN_EVT_R_HOST_UPDATE;
            }
            nevt_local++;
        }
        else {
            njoined = 1;
            callid2 = 0;
            if (new_host == _VN_HOST_INVALID) {
                /* Don't know who the new host is yet */
                hostids = NULL;
            }
            else {
                hostids = &new_host;
            }
            reason = _VN_EVT_R_HOST_JOINED;
        }
        
        if (!_vnt_check_net_config_event(__LINE__, (_VN_event_t*) evt_buf2, 
                                         callid2, net, njoined, 0, 
                                         hostids, reason)) {
            lineno = __LINE__; goto fail;
        }
    }
    
    /* Wait for events to drain */
    _vnt_drain_events(evt_buf, sizeof(evt_buf), timeout);
    event = NULL; n = 0;
    goto done;

fail:
    okay = false;
    _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
              "_vnt_check_connect_succeeded: Failed in %s, line %d\n",
              __FILE__, lineno);
done:
    return okay;
}

bool _vnt_check_connect_default_rejected(_VN_guid_t guid, _VN_addr_t from_addr,
                                         _VN_port_t port, int timeout, 
                                         _VN_callid_t listen_call,
                                         _VN_guid_t owner_guid, 
                                         _VN_addr_t from_owner_addr,
                                         char* msg)
{
    char cmdbuf[1024];
    uint8_t evt_buf[1024], evt_buf2[1024];
    int i, rv, n = 0, lineno;
    _VN_event_t* event = NULL;
    _VN_callid_t callid;
    bool okay = true;
    bool got_command_resp = false;
    bool got_net_config = false;
    bool got_new_conn = false;
    bool got_error = false;
    uint64_t request_id;
    _VN_addr_t owner_addr = _VN_ADDR_INVALID;
    _VN_host_t owner_host = _VN_HOST_INVALID;
    _VN_net_t net = 0;
                
    /* Check command returned call id */ 
    /* Check owner got _VN_EVT_CONNECTION_REQUEST event */
    for (i = 1; i <= 3; i++) {
        _VN_guid_t from_guid;
        _VN_addr_t from_vn_addr;
        _VN_new_message_event* msg_event;

        event = _vnt_get_next_event(event, &n, evt_buf, 
                                    sizeof(evt_buf), timeout);
        if (event == NULL) { lineno = __LINE__; goto fail; }
        
        if (!_vnt_check_event(__LINE__, event, 
                              _VN_EVT_NEW_MESSAGE, 0)) {
            lineno = __LINE__; goto fail;
        }
        
        msg_event = (_VN_new_message_event*) event;
        from_vn_addr = _VN_make_addr(msg_event->netid, msg_event->fromhost);
        _VN_get_guid(from_vn_addr, &from_guid);

        if (_vn_guid_match(&from_guid, &guid) && (!got_command_resp)) {
            /* Check command returned call id */ 
            if (!_vnt_check_msg_return_code(__LINE__, event, from_addr, port,
                                            _VN_RV_CALLID, &callid)) {
                lineno = __LINE__; goto fail;
            }

            got_command_resp = true;
        }
        else if (_vn_guid_match(&from_guid, &owner_guid) && !got_new_conn) {
            if (!_vnt_check_msg_event(__LINE__, event, from_owner_addr, port,
                                      owner_guid, evt_buf2, sizeof(evt_buf2),
                                      NULL)) {
                lineno = __LINE__; goto fail;
            }

            /* Check owner guid got _VN_EVT_NEW_CONNECTION event */
            if (!_vnt_check_new_connection_event(__LINE__,
                                                 (_VN_event_t*) evt_buf2, 
                                                 listen_call, _VN_NET_MASK1)) {
                lineno = __LINE__; goto fail;
            }
            
            owner_addr = ((_VN_new_connection_event* ) evt_buf2)->myaddr;
            owner_host = _VN_addr2host(owner_addr);
            net = _VN_addr2net(owner_addr);

            got_new_conn = true;
        }
        else {
            /* Check owner got _VN_EVT_CONNECTION_REQUEST event */
            if (!_vnt_check_msg_event(__LINE__, event, from_owner_addr, port,
                                      owner_guid, evt_buf2, sizeof(evt_buf2),
                                      NULL)) {
                lineno = __LINE__; goto fail;
            }

            request_id = _VN_HS_REQID(guid, callid);
            if (!_vnt_check_connection_request_event(__LINE__, 
                                                     (_VN_event_t*) evt_buf2,
                                                     listen_call,
                                                     request_id, owner_addr,
                                                     NULL, 0)) 
            {
                lineno = __LINE__; goto fail;
            }
        }
    }

    /* Have owner reject net */
    sprintf(cmdbuf, "tell guid%u %u command accept %u %u 0 %s",
            owner_guid.chip_id, port, guid.chip_id, callid, msg);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }

    /* Check owner got command returned okay */
    /* Check peer got _VN_EVT_ERR */
    /* For default net, check owner got 
       _VN_EVT_NET_CONFIG and _VN_EVT_NET_DISCONNECTED */
    got_command_resp = false;
    got_net_config = false;
    for (i = 1; i <= 4; i++) {
        _VN_guid_t from_guid;
        _VN_addr_t from_vn_addr;
        _VN_new_message_event* msg_event;

        event = _vnt_get_next_event(event, &n, evt_buf, 
                                    sizeof(evt_buf), timeout);
        if (event == NULL) { lineno = __LINE__; goto fail; }
        
        if (!_vnt_check_event(__LINE__, event, 
                              _VN_EVT_NEW_MESSAGE, 0)) {
            lineno = __LINE__; goto fail;
        }
        
        msg_event = (_VN_new_message_event*) event;
        from_vn_addr = _VN_make_addr(msg_event->netid, msg_event->fromhost);
        _VN_get_guid(from_vn_addr, &from_guid);

        if (_vn_guid_match(&from_guid, &owner_guid) && !got_command_resp) {
            /* Check owner got command returned okay */
            if (!_vnt_check_msg_return_code(__LINE__, event, from_owner_addr,
                                            port, _VN_ERR_OK, NULL)) {
                lineno = __LINE__; goto fail;
            }
            
            got_command_resp = true;
        }
        else if (_vn_guid_match(&from_guid, &guid) && !got_error) {
            /* Check peer got _VN_EVT_ERR */
            if (!_vnt_check_msg_event(__LINE__, event, from_addr, port,
                                      guid, evt_buf2, sizeof(evt_buf2), NULL)) 
            {
                lineno = __LINE__; goto fail;
            }

            if (!_vnt_check_err_event(__LINE__, (_VN_event_t*) evt_buf2, 
                                      callid, _VN_ERR_REJECTED, NULL, 0)) {
                lineno = __LINE__; goto fail;
            }

            got_error = true;
        }
        else {
            if (!_vnt_check_msg_event(__LINE__, event, from_owner_addr, 
                                      port, owner_guid,
                                      evt_buf2, sizeof(evt_buf2), NULL)) 
            {
                lineno = __LINE__; goto fail;
            }

            /* Should get _VN_EVT_NET_CONFIG and _VN_EVT_NET_DISCONNECTED
               from owner */
            if (!got_net_config) {
                /* Check _VN_EVT_NET_CONFIG */
                _VN_host_t* hosts;
            
                if (owner_host != _VN_HOST_INVALID) {
                    hosts = &owner_host;
                }
                else {
                    hosts = NULL;
                }
                                
                if (!_vnt_check_net_config_event(__LINE__, 
                                                 (_VN_event_t*) evt_buf2, 0,
                                                 net, 0, 1 /* left */, hosts,
                                                 _VN_EVT_R_NET_DISCONNECTED)) {
                    lineno = __LINE__; goto fail;
                }
                
                got_net_config = true;
            }
            else {
                /* Check _VN_EVT_NET_DISCONNECTED */
                if (!_vnt_check_net_disconnected_event(__LINE__, 
                                                       (_VN_event_t*) evt_buf2,
                                                       0 /* callid */, net)) {
                    lineno = __LINE__; goto fail;
                }
            }
        }
    }

    /* Wait for events to drain */
    _vnt_drain_events(evt_buf, sizeof(evt_buf), timeout);
    event = NULL; n = 0;
    goto done;

fail:
    okay = false;
    _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
              "_vnt_check_connect_rejected: Failed in %s, line %d\n",
              __FILE__, lineno);
done:
    return okay;
}

bool _vnt_check_connect_default_succeeded(
    _VN_guid_t guid, _VN_addr_t from_addr,
    _VN_port_t port, int timeout, _VN_callid_t listen_call,
    _VN_guid_t owner_guid, _VN_addr_t from_owner_addr,
    _VN_addr_t* owner_addr, _VN_host_t* owner_host,
    _VN_addr_t* peer_addr, _VN_host_t* peer_host)
{
    char cmdbuf[1024];
    uint8_t evt_buf[1024], evt_buf2[1024];
    int rv, i, n = 0, lineno, njoined;
    _VN_event_t* event = NULL;
    _VN_callid_t callid;
    bool okay = true;
    _VN_addr_t new_addr[2];
    _VN_host_t new_owner[2], new_peer[2];
    _VN_net_t new_net[2];
    bool got_command_resp = false;
    bool got_new_conn = false;
    bool got_conn_accepted = false;
    uint64_t request_id;

    /* Check command returned call id */ 
    /* Check owner got _VN_EVT_CONNECTION_REQUEST event */
    /* Check owner got _VN_EVT_NEW_CONNECTION event */
    for (i = 1; i <= 3; i++) {
        _VN_guid_t from_guid;
        _VN_addr_t from_vn_addr;
        _VN_new_message_event* msg_event;

        event = _vnt_get_next_event(event, &n, evt_buf, sizeof(evt_buf), 
                                    timeout);
        if (event == NULL) { lineno = __LINE__; goto fail; }
        
        if (!_vnt_check_event(__LINE__, event, 
                              _VN_EVT_NEW_MESSAGE, 0)) {
            lineno = __LINE__; goto fail;
        }
        
        msg_event = (_VN_new_message_event*) event;
        from_vn_addr = _VN_make_addr(msg_event->netid, msg_event->fromhost);
        _VN_get_guid(from_vn_addr, &from_guid);

        if (_vn_guid_match(&from_guid, &guid) && !got_command_resp) {
            /* Check command returned call id */ 
            if (!_vnt_check_msg_return_code(__LINE__, event, from_addr, port,
                                            _VN_RV_CALLID, &callid)) {
                lineno = __LINE__; goto fail;
            }
            got_command_resp = true;
        }
        else if (_vn_guid_match(&from_guid, &owner_guid) && !got_new_conn) {
            if (!_vnt_check_msg_event(__LINE__, event, from_owner_addr, port,
                                      owner_guid, evt_buf2, sizeof(evt_buf2),
                                      NULL)) {
                lineno = __LINE__; goto fail;
            }

            /* Check owner guid got _VN_EVT_NEW_CONNECTION event */
            if (!_vnt_check_new_connection_event(__LINE__,
                                                 (_VN_event_t*) evt_buf2, 
                                                 listen_call, _VN_NET_MASK1)) {
                lineno = __LINE__; goto fail;
            }
            
            new_addr[1] = ((_VN_new_connection_event* ) evt_buf2)->myaddr;
            new_peer[1] = ((_VN_new_connection_event* ) evt_buf2)->peer_id;
            new_owner[1] = _VN_addr2host(new_addr[1]);
            new_net[1] = _VN_addr2net(new_addr[1]);

            if (owner_addr) {
                *owner_addr = new_addr[1];
            }
            if (owner_host) {
                *owner_host = new_owner[1];
            }

            got_new_conn = true;
        }
        else {
            /* Check owner got _VN_EVT_CONNECTION_REQUEST event */
            if (!_vnt_check_msg_event(__LINE__, event, from_owner_addr, port,
                                      owner_guid, evt_buf2, sizeof(evt_buf2),
                                      NULL)) 
            {
                lineno = __LINE__; goto fail;
            }

            request_id = _VN_HS_REQID(guid, callid);
            if (!_vnt_check_connection_request_event(__LINE__,
                                                     (_VN_event_t*) evt_buf2,
                                                     listen_call, request_id,
                                                     _VN_ADDR_INVALID, 
                                                     NULL, 0)) 
            {
                lineno = __LINE__; goto fail;
            }
        }
    }

    /* Have owner accept net */
    sprintf(cmdbuf, "tell guid%u %u command accept %u %u",
            owner_guid.chip_id, port, guid.chip_id, callid);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }

    /* Check owner got command returned okay */
    /* Check peer guid got _VN_EVT_CONNECTION_ACCEPTED event */
    /* Check peer guid got _VN_EVT_NET_CONFIG event */
    got_command_resp = false;
    for (i = 0; i < 3; i++) {
        _VN_guid_t from_guid;
        _VN_addr_t from_vn_addr;
        _VN_new_message_event* msg_event;

        event = _vnt_get_next_event(event, &n, evt_buf, sizeof(evt_buf),
                                    timeout);

        if (event == NULL) { lineno = __LINE__; goto fail; }
        
        if (!_vnt_check_event(__LINE__, event, 
                              _VN_EVT_NEW_MESSAGE, 0)) {
            lineno = __LINE__; goto fail;
        }
        
        msg_event = (_VN_new_message_event*) event;
        from_vn_addr = _VN_make_addr(msg_event->netid, msg_event->fromhost);
        _VN_get_guid(from_vn_addr, &from_guid);

        if (_vn_guid_match(&from_guid, &owner_guid) && !got_command_resp) {
            if (!_vnt_check_msg_return_code(__LINE__, event, from_owner_addr,
                                            port, _VN_ERR_OK, NULL)) {
                lineno = __LINE__; goto fail;
            }

            got_command_resp = true;
            continue;
        }
        else if (_vn_guid_match(&from_guid, &guid) && !got_conn_accepted) {
            /* Check peer guid got _VN_EVT_CONNECTION_ACCEPTED event */
            if (!_vnt_check_msg_event(__LINE__, event, from_addr, port,
                                      guid, evt_buf2, sizeof(evt_buf2), NULL)) 
            {
                lineno = __LINE__; goto fail;
            }

            if (!_vnt_check_connection_accepted_event(
                    __LINE__, (_VN_event_t*) evt_buf2, 
                    callid, _VN_ADDR_INVALID)) {
                lineno = __LINE__; goto fail;
            }
            
            new_addr[0] = ((_VN_connection_accepted_event* ) evt_buf2)->myaddr;
            new_peer[0] = _VN_addr2host(new_addr[0]);
            new_owner[0] = ((_VN_connection_accepted_event* ) evt_buf2)->owner;
            new_net[0] = _VN_addr2net(new_addr[0]);
            
            if (peer_addr) {
                *peer_addr = new_addr[0];
            }
            if (peer_host) {
                *peer_host = new_peer[0];
            }

            got_conn_accepted = true;
        }
        else if (_vn_guid_match(&from_guid, &guid)) {
            /* Check peer guid got _VN_EVT_NET_CONFIG event */
            if (!_vnt_check_msg_event(__LINE__, event, from_addr, port,
                                      guid, evt_buf2, sizeof(evt_buf2), NULL)) 
            {
                lineno = __LINE__; goto fail;
            }
            
            /* New host */
            njoined = 2; /* For now, all peers (including new peer + owner) */
            
            if (!_vnt_check_net_config_event(__LINE__, (_VN_event_t*) evt_buf2, 
                                             callid, new_net[0], njoined, 0, 
                                             NULL, _VN_EVT_R_HOST_UPDATE)) {
                lineno = __LINE__; goto fail;
            }
        }
        else {
            /* Unexpected message */
            _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                      "Unexpected message from guid %u", guid);
            lineno = __LINE__; goto fail;
        }

        if (got_new_conn && got_conn_accepted) {
            /* Check net, owner and peer events agree */
            if ((new_net[0] != new_net[1]) ||
                (new_owner[0] != new_owner[1]) || (new_peer[0] != new_peer[1])) {
                _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                          "Disagreement of events:  _VN_new_connection_event "
                          "(net 0x08%x, owner %u, peer %u) "
                          " and _VN_connection_accepted event "
                          "(net 0x%08x, owner %u, peer %u).",
                          new_net[0], new_net[1], new_owner[1], new_peer[1],
                          new_owner[1], new_peer[0]);
                lineno = __LINE__; goto fail;
            }
        }
    }
            
    /* Wait for events to drain */
    _vnt_drain_events(evt_buf, sizeof(evt_buf), timeout);
    event = NULL; n = 0;
    goto done;

fail:
    okay = false;
    _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
              "_vnt_check_connect_default_succeeded: Failed in %s, line %d\n",
              __FILE__, lineno);
done:
    return okay;
}

#!/bin/bash

# Tests basic VN functionality and message sending
#
# Runs VN server with 4 hosts with VNs setup from configuration file
# Takes messages from input file and randomly sends to different hosts
# on different nets with mix of reliable, lossy, encrypted, unencryted.

testdir=`dirname $0`

execdir=${1:-.}
res=${2:-$execdir/res/vntest1}
testfile=${3:-$0}
propfile=${4:-"$testdir/vntest1.prop"}
vntest="$execdir/vntest"

#valgrind="valgrind --tool=memcheck"

echo "Testfile: $testfile"
echo "Properties: $propfile"
echo "Results Directory: $res"

if [ ! -e $vntest ] ; then
    echo "$vntest not found"
    exit 1
fi

if [ ! -e $testfile ] ; then
    echo "Cannot open test input $testfile"
    exit 1
fi

if [ ! -e $propfile ] ; then
    echo "Cannot open test properties $propfile"
    exit 1
fi

if [ ! -e $res ] ; then
    mkdir -p $res
fi

$valgrind $vntest -z -c $propfile -m server -t $testfile -s $res/server.s -r $res/server.r > $res/server.log 2>&1 &
$valgrind $vntest -z -c $propfile -m peer1 -t $testfile -s $res/peer1.s -r $res/peer1.r > $res/peer1.log 2>&1 &
$valgrind $vntest -z -c $propfile -m peer2 -t $testfile -s $res/peer2.s -r $res/peer2.r  > $res/peer2.log 2>&1 &
$valgrind $vntest -z -c $propfile -m peer3 -t $testfile -s $res/peer3.s -r $res/peer3.r > $res/peer3.log 2>&1 &
$valgrind $vntest -z -c $propfile -m peer4 -t $testfile -s $res/peer4.s -r $res/peer4.r > $res/peer4.log 2>&1 &

wait

grep "Sent to" $res/*.log > $res/sent
grep "Recv from" $res/*.log > $res/recv

if [ ! -s $res/sent ]  ; then
    echo "sent has no content ... test FAILS"
    exit 1
fi

if [ ! -s $res/recv ]  ; then
    echo "recv has no content ... test FAILS"
    exit 1
fi

$testdir/vnmatchsr.pl $res/sent $res/recv



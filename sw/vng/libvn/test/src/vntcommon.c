//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnlocal.h"
#include "vntest.h"

uint32_t _vnt_event_count[_VN_EVT_MAX+1];
_VN_callid_t _vnt_test_net_all_call = 0;
int _vnt_send_return = 0;
int _vnt_forward_events = 0;
int _vnt_teststart = 0;
int _vnt_testdone = 0;
int _vnt_pings = 0;
int _vnt_pongs = 0;
int _vnt_init_done = 0;
bool _vnt_verbose = true;
static _vn_mutex_t _vnt_pp_mutex; 
static _vn_cond_t  _vnt_pp_cond;

_vn_wbuf_t* _vn_conv_msg_buffer(const _vn_buf_t *pkt)
{
    if (pkt) {
        _vn_wbuf_t* new_buf = _vn_new_vn_buffer(pkt->buf, pkt->max_len);
        if (new_buf) {
            new_buf->len = pkt->len;
        }
        return new_buf;
    }
    else return NULL;
}

void _vnt_init()
{
    if (!_vnt_init_done) {
        _vn_mutex_init(&_vnt_pp_mutex);
        _vn_cond_init(&_vnt_pp_cond);
        _vnt_init_done = 1;
    }
}


void _vnt_send_message(_VN_net_t net_id, _VN_host_t from,  _VN_host_t to, 
                       _VN_port_t port, _VN_tag_t tag, 
                       const void* msg, size_t len, int attr, bool keep_stats)
{
    int rv;

    rv = _VN_send(net_id, from, to, port, msg, (_VN_msg_len_t) len, 
                  (const void*) &tag, sizeof(tag), attr);
    if (rv < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "VN_send to net 0x%08x, from %u to %u, "
                  "port %u failed due to %d\n",
                  net_id, from, to, port, rv);
    }                       
    else {
        _vn_dbg_save_buf(_VN_DBG_SEND_DATA, msg, len);
    }

#ifndef _SC
    if (keep_stats) {
        _vnt_port_stats_t* stats;
        if (net_id == _VN_NET_DEFAULT) {
            _VN_net_t default_net = _vnt_get_default_netid();
            stats = _vnt_get_sent_stats(default_net, to, port);
        }
        else {
            stats = _vnt_get_sent_stats(net_id, to, port);
        }
        
        if (rv < 0) {
            if (stats) stats->dropped++;
        }
        else {
            if (stats) stats->pkts++;
            if (stats) stats->bytes+=(uint32_t) len;
            if (attr & _VN_MSG_RELIABLE) 
                stats->reliable++;
            else stats->lossy++;            
        }
    }
#endif

}

void _vnt_proc_msg_event(_VN_new_message_event* msg_event, 
                         bool verbose, bool keep_stats)
{
    int cmdlen = (int) strlen("COMMAND");
    int msgsize;
    char* msg;
    _vnt_init();
    if (msg_event && msg_event->msg_handle) {
        int bytes;
    
        msgsize = msg_event->size + 1;
        msg = _vn_malloc(msgsize);    
        if (msg == NULL) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                      "Error allocating memory for _vnt_proc_msg_event\n");
            return;
        }

        memset(msg, 0, msgsize);
        bytes = _VN_recv(msg_event->msg_handle, msg, msgsize);

#ifndef _SC
        if (keep_stats) {
            _vnt_port_stats_t* stats;
            int stag = 0;
            
            stats = _vnt_get_recv_stats(msg_event->netid, 
                                        msg_event->fromhost, 
                                        msg_event->port);
            if (stats) stats->pkts++;
            if (stats) stats->bytes+=msg_event->size;
            /* For test purposes: tag = sent attr */
            if (msg_event->opthdr && msg_event->opthdr_size == 2) {
                stag = (*(uint16_t*) msg_event->opthdr);
            }
            if (stag & _VN_MSG_RELIABLE) 
                stats->reliable++;
            else stats->lossy++;
        }
#endif
        
        if (bytes > 0) {
            if (verbose) {
                printf("Got message with %d bytes\n", bytes);
                _vn_dbg_dump_buf(stdout, msg, bytes);
            }
#ifndef _SC
            _vn_dbg_save_buf(_VN_DBG_RECV_DATA, msg, bytes);
#endif
            if ((bytes == 8) && (strncmp(msg, "TESTDONE", 8) == 0)) {
                _vnt_testdone++;
            }
            else if ((bytes == 9) && (strncmp(msg, "TESTSTART", 9) == 0)) {
                _vnt_teststart++;
            }
            else if (strncasecmp(msg, "COMMAND", cmdlen) == 0) {
                char buf[20];
                int rv;
                rv = _vnt_proc_command(msg + cmdlen);
                
                if (_vnt_send_return) {
                    /* Send back message with return code */  
                    sprintf(buf, "RETURN %d", rv);
                    _VN_send(msg_event->netid, msg_event->tohost,
                             msg_event->fromhost, _VN_PORT_TEST,
                             buf, (_VN_msg_len_t) strlen(buf),
                             NULL, 0, _VN_MSG_RELIABLE);
                }
            }
            else if (strncasecmp(msg, "SEND RETURN ON", 14) == 0) {
                printf("Send return on\n");
                _vnt_send_return = 1;
            }
            else if (strncasecmp(msg, "SEND RETURN OFF", 15) == 0) {
                printf("Send return off\n");
                _vnt_send_return = 0;
            }
            else if (strncasecmp(msg, "FORWARD EVENTS ON", 17) == 0) {
                printf("Forward events on\n");
                _vnt_forward_events = 1;
            }
            else if (strncasecmp(msg, "FORWARD EVENTS OFF", 18) == 0) {
                printf("Forward events off\n");
                _vnt_forward_events = 0;
            }
            else if (strncasecmp(msg, "PING", 4) == 0) {
                msg[1] = 'O';
                _VN_send(msg_event->netid, msg_event->tohost,
                         msg_event->fromhost, _VN_PORT_TEST,
                         msg, (_VN_msg_len_t) strlen(msg),
                         NULL, 0, _VN_MSG_RELIABLE);
                _vn_mutex_lock(&_vnt_pp_mutex);
                _vnt_pings++;
                _vn_cond_signal(&_vnt_pp_cond);
                _vn_mutex_unlock(&_vnt_pp_mutex);
            }
            else if (strncasecmp(msg, "PONG", 4) == 0) {
                _vn_mutex_lock(&_vnt_pp_mutex);
                _vnt_pongs++;
                _vn_cond_signal(&_vnt_pp_cond);
                _vn_mutex_unlock(&_vnt_pp_mutex);
            }
        }
        _vn_free(msg);
    }
}

void _vnt_proc_event(_VN_event_t* event, bool verbose, bool keep_stats)
{
    if (verbose) {
        _vn_print_event(stdout, event);
    }
    
    if (event->tag <= _VN_EVT_MAX) {
        _vnt_event_count[event->tag]++;
    }
    else {
        _vnt_event_count[0]++;
    }

    if (_vnt_forward_events) {
        /* Forward all events (except new message event to server */
        if ((event->tag != _VN_EVT_NEW_MESSAGE) && 
            (event->tag != _VN_EVT_MSG_ERR)) {
            _VN_guid_t myguid;
            char buf[1024];
            /* Forward event to server */ 
            myguid = _vn_get_myguid();
            sprintf(buf, "EVENT FROM 0x%08x %u",
                    myguid.device_type, myguid.chip_id);
            memcpy(buf + strlen(buf) + 1, event, event->size);
            _VN_send(_VN_NET_DEFAULT,            
                     _vn_get_default_localhost(_VN_NET_DEFAULT),
                     _VN_HOST_OTHERS, _VN_PORT_TEST,
                     buf, 
                     (_VN_msg_len_t) (strlen(buf) + 1 + event->size),
                     NULL, 0, _VN_MSG_RELIABLE);
        }        
    }

    switch (event->tag) {
    case _VN_EVT_CONNECTION_REQUEST:
        /* Automatically accept all connect requests */
        if (_vnt_accept_connect)
        {
            _VN_connection_request_event* net_event =
                (_VN_connection_request_event*) event;
            _VN_accept(net_event->request_id, true, NULL, 0);
        }
        break;
    case _VN_EVT_NEW_NET_REQUEST:
        /* Automatically accept all new net requests */
        if (_vnt_accept_net)
        {
            _VN_new_net_request_event* net_event =
                (_VN_new_net_request_event*) event;
            _VN_accept_net(net_event->request_id, net_event->netmask, true, NULL, 0);
        }
        break;
     case _VN_EVT_NEW_MESSAGE:
        _vnt_proc_msg_event((_VN_new_message_event*) event, verbose, keep_stats);
        break;         
    case _VN_EVT_NEW_NETWORK: 
        {
            _VN_new_network_event* net_event = 
                (_VN_new_network_event*) event;
            _VN_config_port(net_event->myaddr,
                            _VN_PORT_TEST, _VN_PT_ACTIVE);
            if (event->call_id == _vnt_test_net_all_call) {
                _vnt_test_net_all = _VN_addr2net(net_event->myaddr);
                _VN_listen_net(_vnt_test_net_all, true);
            }
        }
        break;
    case _VN_EVT_NEW_CONNECTION:
        {
            _VN_new_connection_event* net_event = 
                (_VN_new_connection_event*) event;
            
            _VN_config_port(net_event->myaddr,
                            _VN_PORT_TEST, _VN_PT_ACTIVE);
            if (_vn_isserver() && _vnt_test_net_all_call) {
                char buf[64];
                _VN_guid_t myguid;
                
                myguid = _vn_get_myguid();

                /* Tell peer what is the test net */
                sprintf(buf, "COMMAND set TESTNET %u",
                        _vnt_test_net_all);

                _vnt_send_message(_VN_addr2net(net_event->myaddr),
                                  _VN_addr2host(net_event->myaddr),
                                  net_event->peer_id, _VN_PORT_TEST,
                                  _VN_MSG_RELIABLE,
                                  buf, strlen(buf),
                                  _VN_MSG_RELIABLE, keep_stats);
                
                /* Tell peer to join test net of all peers */
                sprintf(buf, "COMMAND gconnect 0x%08x %u %u",
                        myguid.device_type, myguid.chip_id,
                        _vnt_test_net_all);
                _vnt_send_message(_VN_addr2net(net_event->myaddr),
                                  _VN_addr2host(net_event->myaddr),
                                  net_event->peer_id, _VN_PORT_TEST,
                                  _VN_MSG_RELIABLE,
                                  buf, strlen(buf),
                                  _VN_MSG_RELIABLE, keep_stats);
            }
        }
        break;
    case _VN_EVT_CONNECTION_ACCEPTED: 
        {
            _VN_connection_accepted_event* net_event = 
                (_VN_connection_accepted_event*) event;
            _VN_config_port(net_event->myaddr,
                            _VN_PORT_TEST, _VN_PT_ACTIVE);
        }
        break;
    }    
}

int _vn_init_client(const char* vn_server, _vn_inport_t server_port,
                    _vn_inport_t client_port)
{
    int rv;

    rv = _vn_init_common(NULL, client_port, client_port);
    if (rv < 0) {
        return rv;
    }

    if (_vn_get_default_net_id() == _VN_NET_DEFAULT) {
        /* Don't know our net id - do handshake to set up default VN */
        _VN_TRACE(TRACE_FINE, _VN_SG_TEST,
                  "Default net id not set - connect to %s:%u\n",
                  vn_server, server_port);

        rv = _VN_sconnect(vn_server, server_port, NULL, 0);

        if (rv >= 0) {
            /* Wait for event indicating net created or failed */
            char evt_buf[1024];
            
            memset(evt_buf, 0, sizeof(evt_buf));
            rv = _vn_get_events(evt_buf, sizeof(evt_buf), 1, _VN_TIMEOUT_NONE);
            if (rv >= 0) {
                _VN_event_t *event = (_VN_event_t*) evt_buf;
                _vnt_proc_event(event, true, false);
                if (event->tag == _VN_EVT_ERR) {
                    rv = ((_VN_err_event*) event)->errcode;
                }
                else {
                    rv = _VN_ERR_OK;
                }
            }
        }

    }
    else {
        _vn_key_t key;
        _VN_host_t other_id, my_id;
        _VN_net_t net_id;
        _VN_guid_t server_guid = { _VN_DEVICE_TYPE_UNKNOWN, SERVER_PEER };

        _vn_inaddr_t server_addr;
        server_addr = _vn_netif_getaddr(vn_server);
        if (server_addr == _VN_INADDR_INVALID) {
            return _VN_ERR_SERVER;
        }

        _VN_TRACE(TRACE_FINE, _VN_SG_TEST, "Using preset default net id\n");
        /* For testing, use hardcoded values or values set by other means */
#if _VN_USE_IOSC
        key.Ak = IOSC_FS_ENC_HANDLE;
        key.Hk = IOSC_FS_MAC_HANDLE;
#else
        memset(key, 0, sizeof(key));
#endif
        net_id = _vn_get_default_net_id();
        other_id = _VN_HOST_SERVER;
        my_id = _VN_HOST_CLIENT;

        rv = _vn_add_default_net(net_id, false /* i'm not master */,
                                 my_id, server_guid, other_id, 0,
                                 0, 0, server_addr, server_port, key, 0);
    }

    return rv;
}

/* Test registration of services */
bool _vn_test_get_guids(int exp, int lineno, uint8_t mask,
                        _VN_guid_t *exp_guids,
                        _VN_guid_t *guids, int nguids)
{
    int rv;
    rv = _VN_get_device_count(mask);
    if (rv != exp) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: _VN_get_device_count(%u) FAILED: "
                  "expected %d, got %d\n", lineno, mask, exp, rv);
        return false;
    }

    rv = _VN_get_guids(mask, guids, nguids);
    if (rv != exp) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: _VN_get_guids(%u) FAILED: "
                  "expected %d, got %d\n", lineno, mask, exp, rv);
        return false;
    }
    if (rv > 0) {
        int i, n;
        assert(exp_guids);
        n = MIN(rv, nguids);
        for (i = 0; i < n; i++) {
            if (!_vn_guid_match(&exp_guids[i], &guids[i])) {
                _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                          "%d: __VN_get_guids(%u) FAILED: "
                          " got guid 0x%08x %u, expected 0x%08x %u "
                          "(%d/%d guids compared) \n",
                          lineno, mask,
                          guids[i].device_type, guids[i].chip_id,
                          exp_guids[i].device_type, exp_guids[i].chip_id,
                          i, n);
                return false;
            }
        }
    }
    return true;
}

bool _vn_test_get_service_ids(int exp, int lineno, _VN_guid_t guid,
                              uint32_t *exp_services,
                              uint32_t *services, int nservices)
{
    int rv;
    rv = _VN_get_service_count(guid);
    if (rv != exp) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: _VN_get_service_count(%u) FAILED: "
                  "expected %d, got %d\n", lineno, guid, exp, rv);
        return false;
    }

    rv = _VN_get_service_ids(guid, services, nservices);
    if (rv != exp) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: _VN_get_service_ids(%u) FAILED: "
                  "expected %d, got %d\n", lineno, guid, exp, rv);
        return false;
    }
    if (rv > 0) {
        int n;
        assert(exp_services);
        n = MIN(rv, nservices);
        if (memcmp(exp_services, services, n*sizeof(uint32_t)) != 0) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                      "%d: __VN_get_service_ids(%u) FAILED: "
                      " service id mismatch (%d services compared) \n",
                      lineno, guid, n);
            return false;
        }
    }
    return true;
}

bool _vn_test_register_service(int exp, int lineno,
                               uint32_t service_id, bool public,
                               const void* buf, uint16_t buflen)
{
    int rv;
    rv = _VN_register_service(service_id, public, buf, buflen);
    if (rv != exp) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: _VN_register_service(%u) FAILED: "
                  "expected %d, got %d\n", lineno, service_id, exp, rv);
        return false;
    }

    if ((rv >= 0) && (buflen > 0) && buf) {
        uint8_t *tmpbuf = _vn_malloc(buflen);
        bool okay = true;

        if (tmpbuf == NULL) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                      "%d: _VN_register_service(%u) FAILED: "
                      "error allocating memory\n", lineno, service_id);
            return false;
        }
        rv = _VN_get_service(_vn_get_myguid(), service_id, tmpbuf, buflen);
        if (rv == buflen) {
            if (memcmp(tmpbuf, buf, buflen) != 0) {
                _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                          "%d: _VN_get_service(%u) FAILED: "
                          "registered and returned msg differs\n",
                          lineno, service_id);
                okay = false;
            }
        }
        else {
            _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                      "%d: _VN_get_service(%u) FAILED: "
                      "expected %d, got %d\n", lineno, service_id, buflen, rv);
            okay = false;
        }
        _vn_free(tmpbuf);
        return okay;
    }
    return true;
}

bool _vn_test_unregister_service(int exp, int lineno,
                                 uint32_t service_id)
{
    int rv;
    rv = _VN_unregister_service(service_id);
    if (rv != exp) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "%d: _VN_unregister_service(%u) FAILED: "
                  "expected %d, got %d\n", lineno, service_id, exp, rv);
        return false;
    }
    return true;
}

/* Routines for testing various commands.  
   Events are directly checked (instead of extracted from a message event) */

bool _vnt_test_command(char* cmd, int exp, int* res)
{
    int rv;
    int lineno;
    bool okay = true;

    rv = _vnt_proc_command(cmd);
    if (res) *res = rv;

    if (!_vnt_check_return_code(__LINE__, exp, rv)) {
        lineno = __LINE__; goto fail;
    }

    goto done;

fail:
    okay = false;
    _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
              "_vnt_test_command: Failed in %s, line %d\n",
              __FILE__, lineno);
done:
    return okay;
}

bool _vnt_test_create_command_succeeded(int timeout, 
                                        uint32_t netmask, bool isLocal,
                                        _VN_addr_t* test_addr, 
                                        _VN_host_t* test_host)
{
    char cmdbuf[1024];
    uint8_t evt_buf[1024];
    int n = 0, lineno;
    _VN_event_t* event = NULL;
    _VN_callid_t callid;
    bool okay = true;
    _VN_addr_t new_addr;
    _VN_host_t new_host;

    /* Check command returned call id  */ 
    sprintf(cmdbuf, "create 0x%08x %d", netmask, isLocal);
    if (!_vnt_test_command(cmdbuf, _VN_RV_CALLID, &callid)) {
        lineno = __LINE__; goto fail;
    }

    /* Check we got _VN_EVT_NEW_NETWORK */
    event = _vnt_get_next_event(event, &n, evt_buf, sizeof(evt_buf), timeout);
    if (event == NULL) { lineno = __LINE__; goto fail; }
        
    if (!_vnt_check_new_network_event(__LINE__, event, 
                                      callid, netmask, isLocal)) {
        lineno = __LINE__; goto fail;
    }
    
    new_addr = ((_VN_new_network_event* ) event)->myaddr;
    new_host = _VN_addr2host(new_addr);
    
    if (test_addr) {
        *test_addr = new_addr;
    }
    if (test_host) {
        *test_host = new_host;
    }

    /* Wait for extra events to drain */
    _vnt_drain_events(evt_buf, sizeof(evt_buf), timeout);
    event = NULL; n = 0;
    
    goto done;

fail:
    okay = false;
    _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
              "_vnt_test_create_command_succeeded: Failed in %s, line %d\n",
              __FILE__, lineno);
done:
    return okay;
}

bool _vnt_test_connect_command_succeeded(_VN_guid_t owner_guid,
                                         _VN_guid_t joiner_guid,
                                         _VN_net_t net, 
                                         _VN_callid_t listen_call,
                                         _VN_addr_t owner_addr,
                                         int nlocal, 
                                         int npeers, _VN_host_t peers[],
                                         _VN_addr_t* test_addr, 
                                         _VN_host_t* test_host,
                                         int timeout)
{
    char cmdbuf[1024];
    uint8_t evt_buf[1024];
    int rv, i, n = 0, lineno, nevents;
    _VN_event_t* event = NULL;
    _VN_callid_t callid;
    bool okay = true;
    _VN_host_t* hostids;
    _VN_addr_t new_addr = _VN_ADDR_INVALID;
    _VN_host_t new_host = _VN_HOST_INVALID;
    int nevt_local = 0;
    uint8_t reason;
    uint64_t request_id;
    _VN_guid_t myguid = _vn_get_myguid();
    bool got_accepted = false;
    bool isJoiner = _vn_guid_match(&myguid, &joiner_guid);
    bool isOwner = _vn_guid_match(&myguid, &owner_guid);

    if (isJoiner) {
        /* Connect and Check command returned call id */ 
        sprintf(cmdbuf, "gconnect 0x%08x %u 0x%08x", 
                owner_guid.device_type, owner_guid.chip_id, net);
        if (!_vnt_test_command(cmdbuf, _VN_RV_CALLID, &callid)) {
            lineno = __LINE__; goto fail;
        }
    }
     
    if (isOwner) {
        /* Check owner got _VN_EVT_CONNECTION_REQUEST event */
        event = _vnt_get_next_event(event, &n, evt_buf,
                                    sizeof(evt_buf), timeout);
        
        if (event == NULL) { lineno = __LINE__; goto fail; }
        
        /* Check owner got _VN_EVT_CONNECTION_REQUEST event */
        if (!_vnt_check_connection_request_event(__LINE__, event,
                                                 listen_call,
                                                 0, owner_addr,
                                                 NULL, 0)) 
        {
            lineno = __LINE__; goto fail;
        }

        /* Have owner accept net */
        request_id = ((_VN_connection_request_event*) event)->request_id;
        if (_VN_HS_REQID_GET_CHIP_ID(request_id) != joiner_guid.chip_id) {
            lineno = __LINE__; goto fail; 
        }

        sprintf(cmdbuf, "accept %u %u", 
                joiner_guid.chip_id, 
                _VN_HS_REQID_GET_SESSION_ID(request_id));
        rv = _vnt_proc_command(cmdbuf);
        if (rv < 0) { lineno = __LINE__; goto fail; }
    }
    
    /* Check we get nlocal _VN_EVT_NET_CONFIG messages */
    nevents = nlocal;

    /* Check joiner got 1 _VN_EVT_CONNECTION_ACCEPTED EVENT and
                        1 _VN_EVT_NET_CONFIG messages */
    if (isJoiner) nevents+=2;
    
    for (i = 1; i <= nevents; i++) {
        int njoined;
        _VN_callid_t callid2;

        event = _vnt_get_next_event(event, &n, evt_buf, sizeof(evt_buf),
                                    timeout);

        if (event == NULL) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                      "_vnt_check_connect_succeeded: Failed getting %d/%d events\n",
                      i, nevents);
            lineno = __LINE__; goto fail; 
        }

        if (nevt_local < nlocal) {
            /* Existing local hosts */
            njoined = 1;
            callid2 = 0;
            if (new_host == _VN_HOST_INVALID) {
                /* Don't know who the new host is yet */
                hostids = NULL;
            }
            else {
                hostids = &new_host;
            }
            reason = _VN_EVT_R_HOST_JOINED;
        }
        else if (isJoiner) {
            if (!got_accepted) {
                /* Check we got _VN_EVT_CONNECTION_ACCEPTED EVENT */
                if (!_vnt_check_connection_accepted_event(
                        __LINE__, event, callid, owner_addr)) {
                    lineno = __LINE__; goto fail;
                }
                
                new_addr = 
                    ((_VN_connection_accepted_event* ) event)->myaddr;

                new_host = _VN_addr2host(new_addr);
                if (test_addr) {
                    *test_addr = new_addr;
                }
                if (test_host) {
                    *test_host = new_host;
                }
                got_accepted = true;
                continue;
            }
            else {
                /* New host */
                /* For now, all peers (including new peer + owner) */
                njoined = npeers + 1; 
                callid2 = callid;
                hostids = peers;
                reason = _VN_EVT_R_HOST_UPDATE;
            }
            nevt_local++;
        }
        else {
            /* Unexpected event */
            lineno = __LINE__; goto fail; 
        }
        
        if (!_vnt_check_net_config_event(__LINE__, event, 
                                         callid2, net, njoined, 0, 
                                         hostids, reason)) {
            lineno = __LINE__; goto fail;
        }
    }
    
    /* Wait for events to drain */
    _vnt_drain_events(evt_buf, sizeof(evt_buf), timeout);
    event = NULL; n = 0;
    goto done;

fail:
    okay = false;
    _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
              "_vnt_test_connect_command_succeeded: Failed in %s, line %d\n",
              __FILE__, lineno);
done:
    return okay;
}

#define _VNT_MIN_BUF_SIZE 20
#define _VNT_MAX_BUF_SIZE _VN_MAX_MSG_LEN

int _vnt_test_ping(int count, int bufsize, int nothers)
{
    int rv, n, failed = 0, sent = 0;
    char *buf;
    char val[_VNT_MIN_BUF_SIZE];
    _VN_time_t starttime, endtime;
    int delta;
    float bps = 0, pps = 0;
    bool verbose = _vnt_verbose;
    
    if ((bufsize < _VNT_MIN_BUF_SIZE) || (bufsize > _VNT_MAX_BUF_SIZE)) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "Invalid buffer size of %d, must be between %d and %d\n",
                  bufsize, _VNT_MIN_BUF_SIZE, _VNT_MAX_BUF_SIZE);
        return _VN_ERR_INVALID;
    }

    buf = _vn_malloc(bufsize);
    if (buf == NULL) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "Cannot allocate memory for buffer of %d bytes\n", bufsize);
        return _VN_ERR_NOMEM;
    }

    memset(buf, 'A', bufsize);
    buf[bufsize-1] = '\0';
    
    _vnt_init();

    _vn_mutex_lock(&_vnt_pp_mutex);
    _vnt_pings = 0;
    _vnt_pongs = 0;
    _vn_mutex_unlock(&_vnt_pp_mutex);

    _vnt_verbose = false;
    starttime = _VN_get_timestamp();
    for (n = 0; n < count; n++) {
        sprintf(val, "PING %d ", n);
        memcpy(buf, val, strlen(val));
        rv = _VN_send(_vnt_test_net_all, 
                      _vn_get_default_localhost(_vnt_test_net_all),
                      _VN_HOST_OTHERS, 
                      _VN_PORT_TEST, buf, (_VN_msg_len_t) bufsize, 
                      NULL, 0,
                      _VN_MSG_RELIABLE | _VN_MSG_ENCRYPTED);
        if (rv < 0) {
            failed++;
        } else {
            sent++;
        }

        _vn_mutex_lock(&_vnt_pp_mutex);
        while (_vnt_pongs < nothers*sent) {
            _vn_cond_timedwait(&_vnt_pp_cond, &_vnt_pp_mutex, 1000);
        }
        _vn_mutex_unlock(&_vnt_pp_mutex);
    }
    endtime = _VN_get_timestamp();

    delta = endtime - starttime;
    
    if (delta != 0) {
        pps = 1000.0*sent;
        pps = pps/delta;
        bps = pps*bufsize;
    }
    _VN_TRACE(TRACE_FINE, _VN_SG_TEST,
              "Sent %d packets of size %d in %d ms "
              "(%.2f pkts/sec, %.2f bytes/sec)\n",
              sent, bufsize, delta, pps, bps);


    _vnt_verbose = verbose;

    _vn_free(buf);
 
    return sent;
}


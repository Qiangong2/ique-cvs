SRC_BASE := $(shell while [ ! -e Makefile.setup ]; do cd .. ; done; pwd)
include $(SRC_BASE)/Makefile.setup
sinclude $(SRC_BASE)/libvn/localdefs

CSLDIR = $(ROOT)/usr/lib

ifeq ($(NOTRACE),1)
  CFLAGS += -D_VN_NO_TRACE
endif

ifeq ($(DEBUG),1)
  CFG = DBG
  CFLAGS += -g -DDEBUG
else
  CFG = REL
  CFLAGS += -DNDEBUG
endif

ifeq ($(TARGET_HOST),SC)
  VNRPC=DEVICE
  CSL_LIBDIR=$(CSLDIR)/sc
endif

ifeq ($(TARGET_HOST),LINUX)
  CSL_LIBDIR=$(CSLDIR)/x86
  ifndef VNRPC
    VNRPC=NONE
  endif
endif

ifeq ($(TARGET_HOST),WIN32)
  CSL_LIBDIR=$(CSLDIR)/$(CFG)/SC_SDK
  ifndef VNRPC
    VNRPC=DEVICE
  endif
endif

ifeq ($(VNRPC), NONE)
  SUBDIRS = uproxy
endif

TESTLIB = $(OBJ_DIR)/libvntest.a
SHRLIBDIR  = $(SRC_BASE)/libshr/$(CFG)/$(TARGET_HOST)
SHRLIB     = $(SHRLIBDIR)/libshr.a

CFLAGS += -D_$(TARGET_HOST)
CFLAGS += -I../../include -I../../src -I../.. -I$(SRC_BASE)/libshr/include

TESTS = vntestcore vntestpkt  \
        vntest1 vntest2 vntest3 vntest4 vntest5 vntest6 vntest7 vntest8 \
        vntest10 vntest11

UPNPTESTS = vntestupnp1 vntestupnp2 vntestupnp3 vntestupnp4

SK_ELF = $(ROOT)/usr/bin/sc/sk.elf
AUD_ELF = $(ROOT)/usr/bin/sc/aud.elf
USB_ELF = $(ROOT)/usr/bin/sc/usb.elf
FS_ELF = $(ROOT)/usr/bin/sc/fs.elf
ES_ELF = $(ROOT)/usr/bin/sc/es.elf
DEVMON_ELF = $(ROOT)/usr/bin/sc/ncdevmon.elf

CFILES = vncmd.c vntevents.c vntcommon.c

ifneq ($(TARGET_HOST),SC)
CFILES += vnconfig.c vnteststats.c
endif

HEADERS = vntest.h

OBJS = $(CFILES:%.c=$(OBJ_DIR)/%.o)

DIR.SC = SC

ifeq ($(VNRPC), DEVICE)
  DIR.LINUX = LINUX_RPC
  DIR.WIN32 = SC_SDK
  CFLAGS += -D_VN_RPC_DEVICE
else
  ifeq ($(UPNP),1)	
    DIR.LINUX = LINUX_UPNP
    DIR.WIN32 = WIN32_UPNP
  else
    DIR.LINUX = LINUX
    DIR.WIN32 = WIN32
  endif
endif

DIR = $(DIR.$(TARGET_HOST))

OBJ_DIR = ../$(CFG)/$(DIR)

EXECCFILES = vnclient

ifneq ($(TARGET_HOST),SC)
  EXECCFILES += vnserver vntest vntestcore vnreadpkts vntestpkt
endif

TARGETS.LINUX = $(EXECCFILES:%=../$(CFG)/$(DIR.LINUX)/%)
TARGETS.WIN32 = phony_target.WIN32
TARGETS.CLEAN.WIN32 = ../$(CFG)/$(DIR.WIN32)/libvntest/*.obj \
  $(addprefix  ../$(CFG)/$(DIR.WIN32)/*.,obj dll lib exp pdb ilk exe) \
  $(addprefix  ../$(CFG)/$(DIR.WIN32)/,$(EXECCFILES:%=%/*.obj))
TARGETS.SC = $(EXECCFILES:%=../$(CFG)/$(DIR.SC)/%.test.sc.mrg)
TARGETS.SCE = $(EXECCFILES:%=../$(CFG)/$(DIR.SC)/%.test.sce.mrg)
TARGETS.CLEAN.SC = ../$(CFG)/$(DIR.SC)/*.elf

ifneq ($(VNRPC), PROXY)
  TARGETS = $(TARGETS.$(TARGET_HOST)) $(TARGETS.$(TARGET_HOST)E)
  TARGETS.CLEAN = $(TARGETS.CLEAN.$(TARGET_HOST))
endif

TESTS.LINUX := $(TESTS)
TESTS.SC = $(EXECCFILES:%=%.test.$(DIR.SC))
TESTS = $(TESTS.$(TARGET_HOST))

ifeq ($(TARGET_HOST),SC)
  TH_LIB   = -lios -lioslibc -lc $(GLDLIBS)
  LDFLAGS += -Ttext $(MK_VN_START) -T $(SDRAM_LINK) -L$(ROOT)/usr/lib/sc
  LDFLAGS += $(GLDOPTS)
else
  TH_LIB = -lpthread
endif

LOADLIBES = -L$(OBJ_DIR) -L../$(OBJ_DIR) -lvn -lvntest \
            -L$(SHRLIBDIR) -lshr $(TH_LIB)

ifeq ($(TARGET_HOST),SC)
  LOADLIBES += -liosc -lisfs
else
  LOADLIBES += -L$(CSL_LIBDIR) -lioscrypto -lbsl -lcsl -lnvm
endif

ifneq ($(VNRPC), DEVICE)
  ifeq ($(UPNP),1)
    TESTS += $(UPNPTESTS)
    UPNP_DIR = $(SRC_BASE)/libupnp
    ifeq ($(DEBUG),1)
      UPNP_LIB_DIR = $(UPNP_DIR)/upnp/bin/debug
    else
      UPNP_LIB_DIR = $(UPNP_DIR)/upnp/bin/
    endif
    CFLAGS += -DUPNP
    LOADLIBES += -L$(UPNP_LIB_DIR) -lupnp -lixml -lthreadutil
  endif
endif


.PHONY: sc_sdk

default all:  $(TARGETS)
	@for file in $(SUBDIRS); do \
	  make -C $$file $@; \
	done

.PHONY: $(TESTS)

check: $(TARGETS) $(TESTS)

vntestcore:
	./vnruntest.sh vntestcore $(OBJ_DIR)/vntestcore

vntestpkt:
	./vnruntest.sh vntestpkt $(OBJ_DIR)/vntestpkt

vntest1:
	./vnruntest.sh vntest1.testRandomSend ./vntest1.sh $(OBJ_DIR)

vntest2:
	./vnruntest.sh vntest2.testLossySend2Peers ./vntest2.sh $(OBJ_DIR)

vntest3:
	./vnruntest.sh vntest3.testReliableSend2ProxiedPeers ./vntest3.sh $(OBJ_DIR)

vntest4:
	./vnruntest.sh vntest4.testHandshakeDuplicate ./vntest4.sh $(OBJ_DIR)

vntest5:
	./vnruntest.sh vntest5.testClassIGlobalVN ./vnsctest.sh vntest5 $(OBJ_DIR)

vntest6:
	./vnruntest.sh vntest6.testMultipleLocalHosts ./vnsctest.sh vntest6 $(OBJ_DIR)

vntest7:
	./vnruntest.sh vntest7.testDefaultNetLocalHost ./vnsctest.sh vntest7 $(OBJ_DIR)

vntest8:
	./vnruntest.sh vntest8.testInactivityTimeout ./vnsctest.sh vntest8 $(OBJ_DIR)

vntest9:
	./vnruntest.sh vntest9.testDuplicateNetId ./vnsctest.sh vntest9 $(OBJ_DIR)

vntest10:
	./vnruntest.sh vntest10.testNetIdGeneration ./vnsctest.sh vntest10 $(OBJ_DIR)

vntest11:
	./vnruntest.sh vntest11.testAllLocalHosts ./vnsctest.sh vntest11 $(OBJ_DIR)

vntestupnp1:
	./vnruntest.sh vntestupnp1.testSimpleHostDiscovery ./vncctest.sh vntestupnp1 $(OBJ_DIR)

vntestupnp2:
	./vnruntest.sh vntestupnp2.testSimpleHostQuery ./vncctest.sh vntestupnp2 $(OBJ_DIR)

vntestupnp3:
	./vnruntest.sh vntestupnp3.testSimpleHostDiscQuery ./vncctest.sh vntestupnp3 $(OBJ_DIR)

vntestupnp4:
	./vnruntest.sh vntestupnp4.testClearQueryList ./vncctest.sh vntestupnp4 $(OBJ_DIR)

install: 

install_libs: 

config config_libs:

$(OBJS): $(HEADERS)

$(OBJ_DIR)/%.sce.o: %.c
	@-mkdir -p $(OBJ_DIR)
	$(CC) $(CFLAGS) -D_SCE -c $< -o $@

$(OBJ_DIR)/%.o: %.c
	@-mkdir -p $(OBJ_DIR)
	$(CC) $(CFLAGS) -c $< -o $@


$(TESTLIB): $(OBJS)
	ar crs $@ $+

.PHONY: phony_target.WIN32

phony_target.WIN32:
	$(VCC) devenv ../../libvn.sln /build $(CFG)_$(DIR)

$(TARGETS.LINUX):  ../$(OBJ_DIR)/libvn.a $(TESTLIB) $(SHRLIB)

$(TARGETS.SC): ../$(CFG)/SC/%.test.sc.mrg: ../$(CFG)/SC/%.sc.elf
	$(MERGE)  -o $@ -s $(MK_SHARED_START) -z $(MK_SHARED_SIZE) -k $(SK_ELF) -u $(USB_ELF) -f $(FS_ELF) -e $(ES_ELF) -a $(AUD_ELF) -d $(DEVMON_ELF) -v $< 

$(TARGETS.SC:%.test.sc.mrg=%.sc.elf): ../$(CFG)/SC/%.sc.elf: ../$(CFG)/SC/%.o \
              $(CRT0) ../$(OBJ_DIR)/libvn.a $(TESTLIB) $(SHRLIB)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $< $(CRT0) $(LOADLIBES) -lvio_usb

$(TARGETS.SCE): ../$(CFG)/SC/%.test.sce.mrg: ../$(CFG)/SC/%.sce.elf
	$(MERGE)  -o $@  -s $(MK_SHARED_START) -v $< -k $(SK_ELF) -z $(MK_SHARED_SIZE)

$(TARGETS.SCE:%.test.sce.mrg=%.sce.elf): ../$(CFG)/SC/%.sce.elf: ../$(CFG)/SC/%.sce.o \
              $(CRT0) ../$(OBJ_DIR)/libvn.a $(TESTLIB) $(SHRLIB)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $< $(CRT0) $(LOADLIBES) -lvio

.PHONY: runtest $(TESTS)

runtest: $(TESTS)

TEST.$(DIR) = $(@:%.test.$(DIR)=%)

$(TESTS.WIN32) $(TESTS.SC_SDK):
	PATH="$(VNG_LIBDIR)" $(OBJ_DIR)/$(TEST.$(DIR)).exe $(TESTOPTS.$(TEST.$(DIR)))

#$(TESTS.LINUX): 
#	$(OBJ_DIR)/$(TEST.LINUX)  $(TESTOPTS.$(TEST.LINUX))

$(TESTS.SC): $(TARGETS.SCE)
	$(ROOT)/usr/bin/x86/sce -t 0x1ff -f -o $(OBJ_DIR)/$(TEST.SC).test.sce.mrg -u :20010 -a '-g 1'

include $(SRC_BASE)/Makefile.rules

clean:
	rm -f $(OBJ_DIR)/*.o $(TARGETS) $(TARGET) $(TARGETS.CLEAN) $(TESTLIB)
	rm -f $(OBJ_DIR)/.*.d
	@for file in $(SUBDIRS); do \
	  make -C $$file $@; \
	done



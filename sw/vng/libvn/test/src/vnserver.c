//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnlocal.h"
#include "vntest.h"

int recv_thread_keep_running = 1;
_vn_thread_t recv_thread;

void recv_events_loop()
{
    uint8_t events[65536];
    _VN_event_t* event;
    while (recv_thread_keep_running) {
        /* Check if I got any events */
        int n = _VN_get_events(events, sizeof(events), 1000);
        if (n == _VN_ERR_CANCELED) {
             _VN_TRACE(TRACE_FINE, _VN_SG_TEST,
                       "_VN_get_events canceled\n", n);
        }
        event = (_VN_event_t*) events;
        while (n > 0) {
            _vnt_proc_event(event, _vnt_verbose, false);
            event = event->next;
            n--;
        }
    }
}

void recv_events_start()
{
    _vn_thread_create(&recv_thread, NULL, (void*) &recv_events_loop, NULL);
}

void recv_events_stop()
{
    recv_thread_keep_running = 0;
    _VN_cancel_get_events();
    _vn_thread_join(recv_thread, NULL);
}

bool waitClientsJoin(const char* testname, uint32_t nguids)
{
    int loop = 0;
    int maxloops = nguids*60;
    /* Wait for clients to join - give each client 1 minute to join */
    _VN_TRACE(TRACE_INFO, _VN_SG_TEST,
              "%s: Waiting for %d clients to join\n", testname, nguids);
    recv_events_start();
    while (_vnt_event_count[_VN_EVT_NEW_CONNECTION] < nguids) {
        _vn_thread_sleep(1000);
        loop++;
        if (loop >= maxloops) {
            _VN_TRACE(TRACE_INFO, _VN_SG_TEST,
                      "%s: Only %d/%d clients joined after %d loops, aborting\n",
                      testname, _vnt_event_count[_VN_EVT_NEW_CONNECTION], nguids,
                      loop);
            return false;
        }
    }
    _vn_thread_sleep(3000); /* Wait for events to drain */
    recv_events_stop();
    return true;
}

bool tellPeersTestStart(uint32_t nguids, int port)
{
    char cmdbuf[1024];
    int rv, lineno;
    _VN_guid_t guid = _VN_GUID_INVALID;
    bool okay = true;

    /* Have peers forward events to the server so we can check them */
    for (guid.chip_id = 1; guid.chip_id <= nguids; guid.chip_id++) {
        sprintf(cmdbuf, "tell guid%u %u command set accept 0", 
                guid.chip_id, port);
        rv = _vnt_proc_command(cmdbuf);
        if (rv < 0) { lineno = __LINE__; goto fail; }

        sprintf(cmdbuf, "tell guid%u %u send return on", 
                guid.chip_id, port);
        rv = _vnt_proc_command(cmdbuf);
        if (rv < 0) { lineno = __LINE__; goto fail; }

        sprintf(cmdbuf, "tell guid%u %u forward events on",
                guid.chip_id, port);
        rv = _vnt_proc_command(cmdbuf);
        if (rv < 0) { lineno = __LINE__; goto fail; }
    }
    goto done;

fail:
    okay = false;
    _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
              "testPeersTestStart: Failed in %s, line %d\n", __FILE__, lineno);

done:
    return okay;
}

void tellPeersTestDone(uint32_t nguids, int port)
{
    char cmdbuf[1024];
    int rv;
    _VN_guid_t guid = _VN_GUID_INVALID;
    for (guid.chip_id = 1; guid.chip_id <= nguids; guid.chip_id++) {
        sprintf(cmdbuf, "tell guid%u %u command dump", guid.chip_id, port);
        rv = _vnt_proc_command(cmdbuf);
        sprintf(cmdbuf, "tell guid%u %u command exit 1", guid.chip_id, port);
        rv = _vnt_proc_command(cmdbuf);
    }

    /* Wait for events to drain */
    _vnt_drain_events(cmdbuf, sizeof(cmdbuf), 4000);
}

bool testNetIdGeneration()
{
    /* Test Net ID Generation */
    /* (generation of local net ids also tested in vntestcore) */
        
    /* TODO: Check global net ids are properly released (not implemented) */
             
    int i, rv;
    _VN_port_t port = _VN_PORT_TEST;
    _VN_net_t net;
    uint32_t netmask = _VN_NET_MASK1;
    bool isLocal = true, okay = true;
    char cmdbuf[1024];
    char* testname = "testNetIdGeneration";
    _VN_addr_t default_addrs[6], test_addrs[6];
    _VN_host_t test_hosts[6];
    int timeout = 200, lineno;
    _VN_guid_t guid = _VN_GUID_INVALID;
    _VN_guid_t owner_guid = { _VN_DEVICE_TYPE_UNKNOWN, 1 };
    uint32_t nguids = 2;
    _VN_callid_t listen_call[6];

    /* Wait for 2 clients to join */
    if (!waitClientsJoin(testname, nguids)) {
        lineno = __LINE__; goto fail;
    }

    _VN_TRACE(TRACE_INFO, _VN_SG_TEST, "%s: Starting test\n", testname);
    fflush(stdout);

    /* Have peers forward events to the server so we can check them */
    if (!tellPeersTestStart(nguids, port)) {
        lineno = __LINE__; goto fail; 
    }

    for (guid.chip_id = 1; guid.chip_id <= nguids; guid.chip_id++) {
        default_addrs[guid.chip_id] = _vn_get_default_client_addr(guid);
    }

    /*
     * Test local id generation when a host has net IDs that are created
     * by another host, and itself.
     * Client 1 listens on default net
     * Client 2 connects to client one on default net
     * Client 2 creates new nets 
     */

    /***********************************************************/
    /* Tell peer1 to listen on the default net */
    net = _VN_NET_DEFAULT;
    guid.chip_id = 1;
    sprintf(cmdbuf, "tell guid%u %u command listen 0x%08x",
            guid.chip_id, port, net);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }
    
    /* Check command returned call id (also drains events) */
    if (!_vnt_check_remote_call_return_code(guid,
                                            default_addrs[guid.chip_id],
                                            port, timeout, _VN_RV_CALLID, 
                                            &listen_call[guid.chip_id])) {
        lineno = __LINE__; goto fail;
    }

    /***********************************************************/
    /* Tell peer2 to connect to peer1 on net (should succeed) */
    guid.chip_id = 2;
    sprintf(cmdbuf, "tell guid%u %u command gconnect 0x%08x %u 0x%08x",
            guid.chip_id, port, owner_guid.device_type,
            owner_guid.chip_id, net);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }
        
    /* Check connect succeeded and peers notified (also drains events) */
    if (!_vnt_check_connect_default_succeeded(
            guid, default_addrs[guid.chip_id], 
            port, timeout, listen_call[owner_guid.chip_id],
            owner_guid, default_addrs[owner_guid.chip_id],
            &test_addrs[owner_guid.chip_id], 
            &test_hosts[owner_guid.chip_id], /* owner */
            &test_addrs[guid.chip_id], &test_hosts[guid.chip_id])) {
        lineno = __LINE__; goto fail;
    }

    /***********************************************************/
    /* Tell peer2 to create a new net 255 times */
    guid.chip_id = 2;
    sprintf(cmdbuf, "tell guid%u %u command create 0x%08x %d",
            guid.chip_id, port, netmask, isLocal);
    for (i = 0; i < 255; i++) {
        rv = _vnt_proc_command(cmdbuf);
        if (rv < 0) { lineno = __LINE__; goto fail; }
        
        /* Check new net created successfully (also drains events) */
        if (!_vnt_check_create_succeeded(guid, default_addrs[guid.chip_id],
                                         port, timeout,
                                         netmask, isLocal,
                                         &test_addrs[guid.chip_id], 
                                         &test_hosts[guid.chip_id])) {
            lineno = __LINE__; goto fail;
        }
    }

    /* Check overflow and no more available net id handled correctly,
       by trying to generate more than 256 net ids. */

    /* Create 257th net (should fail) */
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }

     /* Check command returned _VN_ERR_NETID (also drains events) */
    if (!_vnt_check_remote_call_return_code(guid,
                                            default_addrs[guid.chip_id], 
                                            port, timeout, 
                                            _VN_ERR_NETID, NULL)) {
        lineno = __LINE__; goto fail;
    }

    /* Create class II, class III, and class IV net (should succeed) */
    netmask = _VN_NET_MASK2;
    sprintf(cmdbuf, "tell guid%u %u command create 0x%08x %d",
            guid.chip_id, port, netmask, isLocal);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }
    
    /* Check new net created successfully (also drains events) */
    if (!_vnt_check_create_succeeded(guid, default_addrs[guid.chip_id],
                                     port, timeout,
                                     netmask, isLocal,
                                     &test_addrs[guid.chip_id],
                                     &test_hosts[guid.chip_id])) {
        lineno = __LINE__; goto fail;
    }

    netmask = _VN_NET_MASK3;
    sprintf(cmdbuf, "tell guid%u %u command create 0x%08x %d",
            guid.chip_id, port, netmask, isLocal);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }
    
    /* Check new net created successfully (also drains events) */
    if (!_vnt_check_create_succeeded(guid, default_addrs[guid.chip_id],
                                     port, timeout,
                                     netmask, isLocal,
                                     &test_addrs[guid.chip_id],
                                     &test_hosts[guid.chip_id])) {
        lineno = __LINE__; goto fail;
    }

    netmask = _VN_NET_MASK4;
    sprintf(cmdbuf, "tell guid%u %u command create 0x%08x %d",
            guid.chip_id, port, netmask, isLocal);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }
    
    /* Check new net created successfully (also drains events) */
    if (!_vnt_check_create_succeeded(guid, default_addrs[guid.chip_id],
                                     port, timeout,
                                     netmask, isLocal,
                                     &test_addrs[guid.chip_id],
                                     &test_hosts[guid.chip_id])) {
        lineno = __LINE__; goto fail;
    }
    
    goto done;

fail:
    okay = false;
    _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
              "%s: Failed in %s, line %d\n", testname, __FILE__, lineno);

done:
    /* Have peers exit */
    tellPeersTestDone(nguids, port);

    _VN_TRACE(TRACE_INFO, _VN_SG_TEST,
              "%s: Test %s\n", testname, okay? "PASS": "FAILED");
    fflush(stdout);
    return okay;
}

bool testDuplicateNetId()
{
    /* Tests attempting to connect to net with duplicate net id.
     *
     * Server will wait for 3 clients to join.
     * Client 1 and 2 will both create a new local net 
     *   (they should have the same net id)
     * Client 2 will attempt to connect to Client 1's local net 
     *   (should fail)
     * Client 3 will attempt to connect to both Client 1 and 2's local net 
     *   (one will succeed, the other fail)
     * Client 3 will then disconnect from Client 1's local net and
     *   connect to Client 2's local net (should succeed)
     *
     * Tests the following functions and conditions:
     * _VN_get_events:  Event types checked include 
     *                  _VN_new_network_event, _VN_net_config_event
     *                  _VN_connection_accepted_event, 
     *                  _VN_net_disconnected_event,
     *                  _VN_new_message_event
     * _VN_new_net:     For class I local VN
     * _VN_listen_net:  Setting listening of non-default net to on
     * _VN_connect:     Joining a net where 
     *                  - net has same id as an existing net
     *                  - switching from one net with the same id to another net
     * _VN_leave_net:   Non-owner leaving a net
     */

    int i, rv;
    _VN_port_t port = _VN_PORT_TEST;
    _VN_net_t net1, net2;
    uint32_t netmask = _VN_NET_MASK1;
    bool isLocal = true, okay = true;
    char cmdbuf[1024];
    char* testname = "testDuplicateNetId";
    _VN_addr_t default_addrs[6], test_addrs[6];
    _VN_host_t test_hosts[6];
    uint8_t evt_buf[1024], evt_buf2[1024];
    int n = 0, timeout = 500, lineno;
    _VN_event_t* event = NULL;
    _VN_callid_t callid;
    _VN_guid_t guid = _VN_GUID_INVALID;
    _VN_guid_t owner_guid = { _VN_DEVICE_TYPE_UNKNOWN, 1 };
    _VN_guid_t invalid_guid = _VN_GUID_INVALID;
    uint32_t nguids = 3;
    _VN_callid_t listen_call[6];

    /* Wait for 3 clients to join */
    if (!waitClientsJoin(testname, nguids)) {
        lineno = __LINE__; goto fail;
    }

    _VN_TRACE(TRACE_INFO, _VN_SG_TEST, "%s: Starting test\n", testname);
    fflush(stdout);

    /* Have peers forward events to the server so we can check them */
    if (!tellPeersTestStart(nguids, port)) {
        lineno = __LINE__; goto fail; 
    }

    for (guid.chip_id = 1; guid.chip_id <= nguids; guid.chip_id++) {
        default_addrs[guid.chip_id] = _vn_get_default_client_addr(guid);
    }

    /***********************************************************/
    /* Tell peer1 and peer2 to create a class I net (up to 4 peers) */
    for (guid.chip_id = 1; guid.chip_id <=2; guid.chip_id++) {
        sprintf(cmdbuf, "tell guid%u %u command create 0x%08x %d",
                guid.chip_id, port, netmask, isLocal);
        rv = _vnt_proc_command(cmdbuf);
        if (rv < 0) { lineno = __LINE__; goto fail; }
        
        /* Check new net created successfully (also drains events) */
        if (!_vnt_check_create_succeeded(guid, default_addrs[guid.chip_id],
                                         port, timeout, netmask, isLocal,
                                         &test_addrs[guid.chip_id],
                                         &test_hosts[guid.chip_id])) {
            lineno = __LINE__; goto fail;
        }
    }

    /* Check that the 2 netids are the same (should be unless we move to random net ids) */
    net1 = _VN_addr2net(test_addrs[1]);
    net2 = _VN_addr2net(test_addrs[2]);
    if (net1 != net2) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST, "Net ids expected to be same, got "
                  "0x%08x and 0x%08x\n", net1, net2);
        lineno = __LINE__; goto fail;        
    }

    /***********************************************************/
    /* Tell peer1 and peer2 to listen on newly created nets */
    for (guid.chip_id = 1; guid.chip_id <=2; guid.chip_id++) {
        sprintf(cmdbuf, "tell guid%u %u command listen 0x%08x",
                guid.chip_id, port, net1);
        rv = _vnt_proc_command(cmdbuf);
        if (rv < 0) { lineno = __LINE__; goto fail; }

        /* Check command returned call id (also drains events) */
        if (!_vnt_check_remote_call_return_code(guid,
                                                default_addrs[guid.chip_id],
                                                port, timeout, _VN_RV_CALLID, 
                                                &listen_call[guid.chip_id])) {
            lineno = __LINE__; goto fail;
        }
    }

    /***********************************************************/
    /* Tell peer 2 to connect to peer1 on net (should fail) */
    owner_guid.chip_id = 1;
    guid.chip_id = 2;
    sprintf(cmdbuf, "tell guid%u %u command gconnect 0x%08x %u 0x%08x",
            guid.chip_id, port, owner_guid.device_type,
            owner_guid.chip_id, net1);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }

    /* Check call failed with _VN_ERR_NETID (also drains events) */
    if (!_vnt_check_pending_call_failed(guid, default_addrs[guid.chip_id],
                                        port, timeout, _VN_ERR_NETID))
    {  lineno = __LINE__; goto fail; }

    /***********************************************************/
    /* Tell peer 3 to connect to peer1 on net (should succeed) */
    guid.chip_id = 3;
    sprintf(cmdbuf, "tell guid%u %u command gconnect 0x%08x %u 0x%08x",
            guid.chip_id, port, owner_guid.device_type,
            owner_guid.chip_id, net1);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }
    
    /* Check connect succeeded and peers notified (also drains events) */
    if (!_vnt_check_connect_succeeded(guid, default_addrs[guid.chip_id],
                                      port, timeout, net1,
                                      listen_call[owner_guid.chip_id],
                                      owner_guid,
                                      default_addrs[owner_guid.chip_id],
                                      test_addrs[owner_guid.chip_id],
                                      0, 1, NULL /*&test_hosts[1]*/,
                                      &test_addrs[guid.chip_id],
                                      &test_hosts[guid.chip_id])) {
        lineno = __LINE__; goto fail;
    }

    /***********************************************************/
    /* Tell peer 3 to connect to peer2 on net (should fail) */
    owner_guid.chip_id = 2;
    guid.chip_id = 3;
    sprintf(cmdbuf, "tell guid%u %u command gconnect 0x%08x %u 0x%08x",
            guid.chip_id, port, owner_guid.device_type, 
            owner_guid.chip_id, net2);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }
    
    /* Check call failed with _VN_ERR_NETID (also drains events) */
    if (!_vnt_check_pending_call_failed(guid, default_addrs[guid.chip_id],
                                        port, timeout, _VN_ERR_NETID))
    {  lineno = __LINE__; goto fail; }

    /***********************************************************/
    /* Tell peer3 to leave net (everyone should get notified peer3 left) */
    guid.chip_id = 3;
    sprintf(cmdbuf, "tell guid%u %u command leave 0x%08x",
            guid.chip_id, port, net1);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }

    event = _vnt_get_next_event(event, &n, evt_buf, sizeof(evt_buf), timeout);
    if (event == NULL) { lineno = __LINE__; goto fail; }

    if (!_vnt_check_msg_return_code(__LINE__, event,
                                    default_addrs[guid.chip_id], port,
                                    _VN_ERR_OK, &callid)) {
        lineno = __LINE__; goto fail;
    }

    /* Check peer 3 got _VN_EVT_NET_DISCONNECTED */
    /* Check other peers got _VN_EVT_NET_CONFIG */    
    for (i = 1; i <= 2; i++) {
        _VN_guid_t from_guid;

        event = _vnt_get_next_event(event, &n, evt_buf, sizeof(evt_buf), timeout);
        if (event == NULL) { lineno = __LINE__; goto fail; }

        /* Don't care who the message is from */
        if (!_vnt_check_msg_event(__LINE__, event, _VN_ADDR_INVALID, port,
                                  invalid_guid, evt_buf2, sizeof(evt_buf2),
                                  &from_guid)) {
            lineno = __LINE__; goto fail;
        }
        
        if (_vn_guid_match(&from_guid, &guid)) {
            /* From peer 3 - expect _VN_EVT_NET_DISCONNECTED */
            if (!_vnt_check_net_disconnected_event(__LINE__, (_VN_event_t*) evt_buf2,
                                                   0 /* callid */, net1)) {
                lineno = __LINE__; goto fail;
            }
        }
        else {
            /* From another peer - expect _VN_EVT_NET_CONFIG */
            _VN_host_t host[1];
            host[0] = test_hosts[guid.chip_id];
            if (!_vnt_check_net_config_event(__LINE__, (_VN_event_t*) evt_buf2, 
                                             0 /* callid */, net1, 0, 1 /* left */, 
                                             host, _VN_EVT_R_HOST_LEFT)) {
                lineno = __LINE__; goto fail;
            }
        }
        
    }

    /* Wait for events to drain */
    _vnt_drain_events(evt_buf, sizeof(evt_buf), timeout);
    event = NULL; n = 0;

    /***********************************************************/
    /* Tell peer 3 to connect to peer2 on net (should succeed now) */
    owner_guid.chip_id = 2;
    guid.chip_id = 3;
    sprintf(cmdbuf, "tell guid%u %u command gconnect 0x%08x %u 0x%08x",
            guid.chip_id, port, owner_guid.device_type,
            owner_guid.chip_id, net2);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }
    
    /* Check connect succeeded and peers notified (also drains events) */
    if (!_vnt_check_connect_succeeded(guid, default_addrs[guid.chip_id],
                                      port, timeout, net2,
                                      listen_call[owner_guid.chip_id],
                                      owner_guid, 
                                      default_addrs[owner_guid.chip_id],
                                      test_addrs[owner_guid.chip_id],
                                      0, 1, NULL /*&test_hosts[1]*/,
                                      &test_addrs[guid.chip_id],
                                      &test_hosts[guid.chip_id])) {
        lineno = __LINE__; goto fail;
    }

    goto done;

fail:
    okay = false;
    _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
              "%s: Failed in %s, line %d\n", testname, __FILE__, lineno);

done:
    /* Have peers exit */
    tellPeersTestDone(nguids, port);

    _VN_TRACE(TRACE_INFO, _VN_SG_TEST,
              "%s: Test %s\n", testname, okay? "PASS": "FAILED");
    fflush(stdout);
    return okay;
}

bool testInactivityTimeout()
{
    /* Tests inactivity timeout from owner to host.
     * Have one client connect to the server
     * and then have the client disconnect without leaving any nets.
     * The server should timeout the client after 1 minute of inactivity 
     */

    _VN_port_t port = _VN_PORT_TEST;
    bool okay = true;
    char cmdbuf[1024];
    char* testname = "testInactivityTimeout";
    _VN_addr_t default_addrs[6];
    _VN_net_t default_net;
    _VN_host_t default_client_host, default_server_host;
    uint8_t evt_buf[1024];
    int rv, n = 0, lineno, timeout = 1000;
    _VN_event_t* event = NULL;
    _VN_guid_t guid = _VN_GUID_INVALID;
    uint32_t nguids = 1;

    _vn_inactivity_set_timeout(60*1000); /* Set timeout to 60 seconds */

    /* Wait for 1 client to join */
    if (!waitClientsJoin(testname, nguids)) {
        lineno = __LINE__; goto fail;
    }

    _VN_TRACE(TRACE_INFO, _VN_SG_TEST, "%s: Starting test\n", testname);
    fflush(stdout);

    /* Have peers forward events to the server so we can check them */
    for (guid.chip_id = 1; guid.chip_id <= nguids; guid.chip_id++) {
        sprintf(cmdbuf, "tell guid%u %u forward events on",
                guid.chip_id, port);
        rv = _vnt_proc_command(cmdbuf);
        if (rv < 0) { lineno = __LINE__; goto fail; }

        default_addrs[guid.chip_id] = _vn_get_default_client_addr(guid);
    }    

    guid.chip_id = 1;
    default_net = _VN_addr2net(default_addrs[guid.chip_id]);
    default_client_host = _VN_addr2host(default_addrs[guid.chip_id]);
    default_server_host = _vn_get_default_localhost(default_net);

    /***********************************************************/
    /* Tell peer1 to exit without leaving all the nets first */
    sprintf(cmdbuf, "tell guid%u %u command exit 0", guid.chip_id, port);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }

    timeout = _vn_inactivity_get_timeout();

    /***********************************************************/
    /* Wait for timeout to get events */
    event = _vnt_get_next_event(event, &n, evt_buf, sizeof(evt_buf), 
                                timeout + 1000);
    if (event == NULL) { lineno = __LINE__; goto fail; }
    
    /* After timeout, should have events for leaving default net, test net */
    
    /* First event would be peer timed out on default net */
    if (!_vnt_check_net_config_event(__LINE__, event, 0, 
                                     default_net, 0, 1 /* left */,
                                     &default_client_host, _VN_EVT_R_HOST_TIMEOUT)) {
        lineno = __LINE__; goto fail;
    }

    /* Events should all be ready by now */
    timeout = 100;

    /* Next 2 events would be server getting rid of default net for peer */
    /* Server leaving default net */
    event = _vnt_get_next_event(event, &n, evt_buf, sizeof(evt_buf), 
                                timeout);
    if (event == NULL) { lineno = __LINE__; goto fail; }
    
    if (!_vnt_check_net_config_event(__LINE__, event, 0,
                                     default_net, 0, 1 /* left */,
                                     &default_server_host, _VN_EVT_R_NET_DISCONNECTED)) {
        lineno = __LINE__; goto fail;
    }

    /* Server deleting default net */
    event = _vnt_get_next_event(event, &n, evt_buf, sizeof(evt_buf), 
                                timeout);
    if (event == NULL) { lineno = __LINE__; goto fail; }

    if (!_vnt_check_net_disconnected_event(__LINE__, event,
                                           0 /* callid */, default_net)) {
        lineno = __LINE__; goto fail;
    }

    /* Last event will be client leaving test net */
    event = _vnt_get_next_event(event, &n, evt_buf, sizeof(evt_buf), 
                                timeout);
    if (event == NULL) { lineno = __LINE__; goto fail; }

    if (!_vnt_check_net_config_event(__LINE__, event, 0, 
                                     _vnt_test_net_all, 0, 1 /* left */,
                                     NULL, _VN_EVT_R_HOST_TIMEOUT)) {
        lineno = __LINE__; goto fail;
    }

    goto done;

fail:
    okay = false;
    _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
              "%s: Failed in %s, line %d\n", testname, __FILE__, lineno);

done:
    _VN_TRACE(TRACE_INFO, _VN_SG_TEST, 
              "%s: Test %s\n", testname, okay? "PASS": "FAILED");
    fflush(stdout);
    return okay;
}

bool testDefaultNetLocalHost()
{
    /* Test localhost connecting to the default net
     * Tests the following functions and conditions:
     * _VN_get_events:  Event types checked include 
     *                  _VN_new_connection_event, _VN_net_config_event
     *                  _VN_connection_accepted_event, 
     *                  _VN_net_disconnected_event,
     *                  _VN_new_message_event
     * _VN_listen_net:  Setting listening of default net to on, and then off
     * _VN_connect:     Joining default net where owner is another localhost
     *                  Attempting to connect to localhost that is not 
     *                     not listening on the default net
     * _VN_leave_net:   Non-owner leaving the default net where
     *                  - owner is another localhost
     */

    int i, rv, evtcnt[6];
    _VN_port_t port = _VN_PORT_TEST;
    _VN_net_t net;
    bool okay = true;
    char cmdbuf[1024];
    char* testname = "testDefaultNetLocalHost";
    _VN_addr_t default_addrs[6], test_addrs[6];
    _VN_host_t test_hosts[6];
    uint8_t evt_buf[1024], evt_buf2[1024];
    int n = 0, timeout = 500, lineno;
    _VN_event_t* event = NULL;
    _VN_callid_t callid;
    _VN_guid_t guid = _VN_GUID_INVALID;
    _VN_guid_t owner_guid =  { _VN_DEVICE_TYPE_UNKNOWN, 1 };
    uint32_t nguids = 1;
    _VN_callid_t listen_call[6];

    /* Wait for 1 client to join */
    if (!waitClientsJoin(testname, nguids)) {
        lineno = __LINE__; goto fail;
    }

    _VN_TRACE(TRACE_INFO, _VN_SG_TEST, "%s: Starting test\n", testname);
    fflush(stdout);

    /* Have peers forward events to the server so we can check them */
    if (!tellPeersTestStart(nguids, port)) {
        lineno = __LINE__; goto fail; 
    }

    for (guid.chip_id = 1; guid.chip_id <= nguids; guid.chip_id++) {
        default_addrs[guid.chip_id] = _vn_get_default_client_addr(guid);
    }

    net = _VN_NET_DEFAULT;

    /***********************************************************/
    /* Tell peer1 to listen on default net */
    guid.chip_id = 1;
    sprintf(cmdbuf, "tell guid%u %u command listen 0x%08x",
            guid.chip_id, port, net);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }

    /* Check command returned call id (also drains events) */
    if (!_vnt_check_remote_call_return_code(guid,
                                            default_addrs[guid.chip_id],
                                            port, timeout, _VN_RV_CALLID,
                                            &listen_call[guid.chip_id])) {
        lineno = __LINE__; goto fail;
    }

    /***********************************************************/
    /* Tell peer1 to try to connect to peer1 on newly created net
       and have peer1 reject the connection */
    guid.chip_id = 1;
    sprintf(cmdbuf, "tell guid%u %u command gconnect 0x%08x %u 0x%08x", 
            guid.chip_id, port, owner_guid.device_type, 
            owner_guid.chip_id, net);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }

    /* Check connection rejected */
    if (!_vnt_check_connect_default_rejected(guid,
                                             default_addrs[guid.chip_id],
                                             port, timeout, 
                                             listen_call[owner_guid.chip_id],
                                             owner_guid, 
                                             default_addrs[owner_guid.chip_id],
                                             "REJECT LOCAL DEF CONNECTION TEST"))
    {  lineno = __LINE__; goto fail; }    

    /***********************************************************/
    /* Tell peer 1 to connect to peer1 on net (should succeed) */
    guid.chip_id = 1;
    sprintf(cmdbuf, "tell guid%u %u command gconnect 0x%08x %u 0x%08x",
            guid.chip_id, port, owner_guid.device_type,
            owner_guid.chip_id, net);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }
    
    /* Check connect succeeded and peers notified (also drains events) */
    if (!_vnt_check_connect_default_succeeded(
            guid, default_addrs[guid.chip_id], 
            port, timeout, listen_call[owner_guid.chip_id],
            owner_guid, default_addrs[owner_guid.chip_id],
            &test_addrs[guid.chip_id],
            &test_hosts[guid.chip_id], /* owner */
            &test_addrs[guid.chip_id+nguids],
            &test_hosts[guid.chip_id+nguids])) {
        lineno = __LINE__; goto fail;
    }

    net = _VN_addr2net(test_addrs[guid.chip_id]);

    /***********************************************************/
    /* Tell peer1 non-owner host to leave net (owner should disconnect too) */
    guid.chip_id = 1;
    sprintf(cmdbuf, "tell guid%u %u command leave 0x%08x %u",
            guid.chip_id, port, net, test_hosts[guid.chip_id+nguids]);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }

    /* Check command returned callid */
    event = _vnt_get_next_event(event, &n, evt_buf, sizeof(evt_buf), timeout);
    if (event == NULL) { lineno = __LINE__; goto fail; }

    if (!_vnt_check_msg_return_code(__LINE__, event,
                                    default_addrs[guid.chip_id], port,
                                    0, &callid)) {
        lineno = __LINE__; goto fail;
    }

    /* Check peer 1 got 2 _VN_EVT_NET_CONFIG, 1 _VN_EVT_NET_DISCONNECTED */
    memset(evtcnt, 0, sizeof(evtcnt));
    for (i = 1; i <= 3; i++) {
        _VN_guid_t from_guid;
        _VN_host_t host[1];

        event = _vnt_get_next_event(event, &n, evt_buf, sizeof(evt_buf), timeout);
        if (event == NULL) { lineno = __LINE__; goto fail; }

        if (!_vnt_check_msg_event(__LINE__, event, 
                                  default_addrs[guid.chip_id], port,
                                  guid, evt_buf2, sizeof(evt_buf2), &from_guid)) {
            lineno = __LINE__; goto fail;
        }

        evtcnt[from_guid.chip_id]++;
        /* From peer 1 - expect _VN_EVT_NET_CONFIG for leaving host
           then _VN_EVT_NET_CONFIG for localhost (owner)
           then _VNT_NET_DISCONNECTED */
        if (evtcnt[from_guid.chip_id] == 1) {                
            host[0] = test_hosts[guid.chip_id+nguids];
            if (!_vnt_check_net_config_event(__LINE__, (_VN_event_t*) evt_buf2, 
                                             0, net, 0, 1 /* left */, 
                                             host, _VN_EVT_R_HOST_LEFT)) {
                lineno = __LINE__; goto fail;
            }
        }
        else if (evtcnt[from_guid.chip_id] == 2) {
            host[0] = test_hosts[guid.chip_id];
            if (!_vnt_check_net_config_event(__LINE__, (_VN_event_t*) evt_buf2, 
                                             0, net, 0, 1 /* left */, 
                                             host, _VN_EVT_R_NET_DISCONNECTED)) {
                lineno = __LINE__; goto fail;
            }
        }
        else {
            if (!_vnt_check_net_disconnected_event(__LINE__, 
                                                   (_VN_event_t*) evt_buf2,
                                                   0 /* callid */, net)) {
                lineno = __LINE__; goto fail;
            }
        }
    }

    /***********************************************************/
    /* Tell peer1 to stop listening on default net */
    net = _VN_NET_DEFAULT;
    guid.chip_id = 1;
    sprintf(cmdbuf, "tell guid%u %u command listen 0x%08x 0", 
            guid.chip_id, port, net);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }

    /* Check command returned call id (also drains events) */
    if (!_vnt_check_remote_call_return_code(guid, default_addrs[guid.chip_id],
                                            port, timeout, _VN_RV_CALLID,
                                            &listen_call[guid.chip_id])) {
        lineno = __LINE__; goto fail;
    }

    /***********************************************************/
    /* Tell peer 1 to connect to peer1 on net (should fail) */
    guid.chip_id = 1;
    sprintf(cmdbuf, "tell guid%u %u command gconnect 0x%08x %u 0x%08x",
            guid.chip_id, port, owner_guid.device_type,
            owner_guid.chip_id, net);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }

    /* Check call failed with _VN_ERR_INACTIVE (also drains events) */
    if (!_vnt_check_pending_call_failed(guid, default_addrs[guid.chip_id],
                                        port, timeout, _VN_ERR_INACTIVE))
    {  lineno = __LINE__; goto fail; }

    goto done;

fail:
    okay = false;
    _VN_TRACE(TRACE_INFO, _VN_SG_TEST,
              "%s: Failed in %s, line %d\n", testname, __FILE__, lineno);

done:
    /* Have peers exit */
    tellPeersTestDone(nguids, port);

    _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
              "%s: Test %s\n", testname, okay? "PASS": "FAILED");
    fflush(stdout);
    return okay;
}

bool testAllLocalHosts()
{
    /* Test multiple local hosts on the same VN (Class 3 VN)
     * Tests the following functions and conditions:
     * _VN_get_events:  Event types checked include 
     *                  _VN_new_network_event, _VN_net_config_event
     *                  _VN_connection_accepted_event, 
     *                  _VN_net_disconnected_event,
     *                  _VN_new_message_event
     * _VN_new_net:     For class III local VN
     * _VN_listen_net:  Setting listening of non-default net to on
     * _VN_connect:     Joining a net until it is full
     * _VN_leave_net:   Leaving the net until only the owner is left
     */

    int rv;
    _VN_port_t port = _VN_PORT_TEST;
    _VN_net_t net;
    bool isLocal = true, okay = true;
    char cmdbuf[1024];
    char* testname = "testAllLocalHosts";
    _VN_addr_t default_addrs[2], test_addrs[_VN_HOST_MAX3+1];
    _VN_host_t test_hosts[_VN_HOST_MAX3+2];
    uint8_t evt_buf[1024], evt_buf2[1024];
    int n = 0, timeout = 700, lineno;
    _VN_event_t* event = NULL;
    _VN_callid_t callid;
    _VN_guid_t guid = _VN_GUID_INVALID;
    _VN_guid_t owner_guid = { _VN_DEVICE_TYPE_UNKNOWN, 1 };
    _VN_guid_t invalid_guid = _VN_GUID_INVALID;
    uint32_t i, j, nguids = 1;
    uint8_t reason;
    _VN_callid_t listen_call[2];
    uint32_t netmask = _VN_NET_MASK3;
    int maxhosts = _VN_HOST_MAX3+1;

    /* Wait for 1 clients to join */
    if (!waitClientsJoin(testname, nguids)) {
        lineno = __LINE__; goto fail;
    }

    _VN_TRACE(TRACE_INFO, _VN_SG_TEST, "%s: Starting test\n", testname);
    fflush(stdout);

    /* Have peers forward events to the server so we can check them */
    if (!tellPeersTestStart(nguids, port)) {
        lineno = __LINE__; goto fail; 
    }

    for (guid.chip_id = 1; guid.chip_id <= nguids; guid.chip_id++) {
        default_addrs[guid.chip_id] = _vn_get_default_client_addr(guid);
    }

    /***********************************************************/
    /* Tell peer1 to create a class N net */
    guid.chip_id = 1;
    sprintf(cmdbuf, "tell guid%u %u command create 0x%08x %d",
            guid.chip_id, port, netmask, isLocal);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }
        
    /* Check new net created successfully (also drains events) */
    if (!_vnt_check_create_succeeded(guid, default_addrs[guid.chip_id],
                                     port, timeout, netmask, isLocal,
                                     &test_addrs[guid.chip_id],
                                     &test_hosts[guid.chip_id])) {
        lineno = __LINE__; goto fail;
    }

    net = _VN_addr2net(test_addrs[guid.chip_id]);

    /***********************************************************/
    /* Tell peer1 to listen on net */
    guid.chip_id = 1;
    sprintf(cmdbuf, "tell guid%u %u command listen 0x%08x",
            guid.chip_id, port, net);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }

    /* Check command returned call id (also drains events) */
    if (!_vnt_check_remote_call_return_code(guid, default_addrs[guid.chip_id],
                                            port, timeout, _VN_RV_CALLID,
                                            &listen_call[guid.chip_id])) {
        lineno = __LINE__; goto fail;
    }

    /***********************************************************/
    /* Tell peer 1 to connect to peer1 on net (should succeed) */
    guid.chip_id = 1;
    for (i = 0; i < maxhosts-1; i++) {
        sprintf(cmdbuf, "tell guid%u %u command gconnect 0x%08x %u 0x%08x",
                guid.chip_id, port, owner_guid.device_type,
                owner_guid.chip_id, net);
        rv = _vnt_proc_command(cmdbuf);
        if (rv < 0) { lineno = __LINE__; goto fail; }
        
        /* Check connect succeeded and peers notified (also drains events) */
        if (!_vnt_check_connect_succeeded(guid, default_addrs[guid.chip_id],
                                          port, timeout, net,
                                          listen_call[owner_guid.chip_id],
                                          owner_guid,
                                          default_addrs[owner_guid.chip_id],
                                          test_addrs[owner_guid.chip_id],
                                          i+1, i+1, NULL /*&test_hosts[1]*/,
                                          &test_addrs[guid.chip_id+i+1],
                                          &test_hosts[guid.chip_id+i+1])) {
            lineno = __LINE__; goto fail;
        }
    }

    /***********************************************************/
    /* Tell peer 1 to connect to peer1 on net again (should fail - net full) */
    sprintf(cmdbuf, "tell guid%u %u command gconnect 0x%08x %u 0x%08x",
            guid.chip_id, port, owner_guid.device_type,
            owner_guid.chip_id, net);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }
    
    /* Check call failed with _VN_ERR_FULL (also drains events) */
    if (!_vnt_check_pending_call_failed(guid, default_addrs[guid.chip_id],
                                        port, timeout, _VN_ERR_FULL)) {
        lineno = __LINE__; goto fail;
    }

    /*********************************************************************/
    /* Tell hosts on peer 1 to disconnect from net (should succeed) */
    
    for (i = 0; i < maxhosts-1; i++) {
        _VN_host_t leaving_host = test_hosts[guid.chip_id+i+1];
        sprintf(cmdbuf, "tell guid%u %u command leave 0x%08x %u",
                guid.chip_id, port, net, leaving_host);
        rv = _vnt_proc_command(cmdbuf);
        if (rv < 0) { lineno = __LINE__; goto fail; }
        
        /* Check command returned 0 */
        event = _vnt_get_next_event(event, &n, evt_buf, sizeof(evt_buf), timeout);
        if (event == NULL) { lineno = __LINE__; goto fail; }
        
        if (!_vnt_check_msg_return_code(__LINE__, event,
                                        default_addrs[guid.chip_id],
                                        port, 0, &callid)) {
            lineno = __LINE__; goto fail;
        }

        /* Check other peers got _VN_EVT_NET_CONFIG */    
        /* NOTE: There is no _VN_EVT_NET_DISCONNECTED event for peer since 
           there is still one more host on the net */
        for (j = 1; j <= maxhosts - (i+1); j++) {
            _VN_guid_t from_guid;
            
            event = _vnt_get_next_event(event, &n, evt_buf, sizeof(evt_buf), timeout);
            if (event == NULL) { lineno = __LINE__; goto fail; }
            
            /* Don't care who the message is from */
            if (!_vnt_check_msg_event(__LINE__, event, _VN_ADDR_INVALID, port,
                                      invalid_guid, evt_buf2, sizeof(evt_buf2),
                                      &from_guid)) {
                lineno = __LINE__; goto fail;
            }
        
            /* Expect _VN_EVT_NET_CONFIG */
            reason = _VN_EVT_R_HOST_LEFT;
            if (!_vnt_check_net_config_event(__LINE__, (_VN_event_t*) evt_buf2, 
                                             0 /* callid */, net, 0, 1 /* left */, 
                                             &leaving_host, reason)) {
                lineno = __LINE__; goto fail;
            }
        }
    }

    /* Wait for events to drain */
    _vnt_drain_events(evt_buf, sizeof(evt_buf), timeout);
    event = NULL; n = 0;

    goto done;

fail:
    okay = false;
    _VN_TRACE(TRACE_INFO, _VN_SG_TEST,
              "%s: Failed in %s, line %d\n", testname, __FILE__, lineno);

done:
    /* Have peers exit */
    tellPeersTestDone(nguids, port);

    _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
              "%s: Test %s\n", testname, okay? "PASS": "FAILED");
    fflush(stdout);
    return okay;
}

bool testMultipleLocalHosts()
{
    /* Test multiple local hosts on the same VN 
     * Tests the following functions and conditions:
     * _VN_get_events:  Event types checked include 
     *                  _VN_new_network_event, _VN_net_config_event
     *                  _VN_connection_accepted_event, 
     *                  _VN_net_disconnected_event,
     *                  _VN_new_message_event
     * _VN_new_net:     For class I local VN
     * _VN_listen_net:  Setting listening of non-default net to on
     * _VN_connect:     Joining a net where 
     *                  - owner is another localhost
     *                  - there are 2 localhosts on the same remote device
     *                  - there is another localhost (nonowner) in the net
     *                  - after another localhost has left 
     *                  - net is full
     * _VN_leave_net:   Non-owner leaving a net where
     *                  - owner is another localhost
     *                  - there is another localhost that is not the owner
     *                  Owner leaving a net where
     *                  - there is another localhost
     *                  - there are 2 localhosts on the same remote device
     */

    int rv, evtcnt[6];
    _VN_port_t port = _VN_PORT_TEST;
    _VN_net_t net;
    uint32_t netmask = _VN_NET_MASK1;
    bool isLocal = true, okay = true;
    char cmdbuf[1024];
    char* testname = "testMultipleLocalHosts";
    _VN_addr_t default_addrs[6], test_addrs[6];
    _VN_host_t test_hosts[6];
    uint8_t evt_buf[1024], evt_buf2[1024];
    int n = 0, timeout = 500, lineno;
    _VN_event_t* event = NULL;
    _VN_callid_t callid;
    _VN_guid_t guid = _VN_GUID_INVALID;
    _VN_guid_t owner_guid = { _VN_DEVICE_TYPE_UNKNOWN, 1 };
    _VN_guid_t invalid_guid = _VN_GUID_INVALID;
    uint32_t i, nguids = 2;
    uint8_t reason;
    _VN_callid_t listen_call[6];

    /* Wait for 2 clients to join */
    if (!waitClientsJoin(testname, nguids)) {
        lineno = __LINE__; goto fail;
    }

    _VN_TRACE(TRACE_INFO, _VN_SG_TEST, "%s: Starting test\n", testname);
    fflush(stdout);

    /* Have peers forward events to the server so we can check them */
    if (!tellPeersTestStart(nguids, port)) {
        lineno = __LINE__; goto fail; 
    }

    for (guid.chip_id = 1; guid.chip_id <= nguids; guid.chip_id++) {
        default_addrs[guid.chip_id] = _vn_get_default_client_addr(guid);
    }

    /***********************************************************/
    /* Tell peer1 to create a class I net (up to 4 peers) */
    guid.chip_id = 1;
    sprintf(cmdbuf, "tell guid%u %u command create 0x%08x %d",
            guid.chip_id, port, netmask, isLocal);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }
        
    /* Check new net created successfully (also drains events) */
    if (!_vnt_check_create_succeeded(guid, default_addrs[guid.chip_id],
                                     port, timeout, netmask, isLocal,
                                     &test_addrs[guid.chip_id],
                                     &test_hosts[guid.chip_id])) {
        lineno = __LINE__; goto fail;
    }

    net = _VN_addr2net(test_addrs[guid.chip_id]);

    /***********************************************************/
    /* Tell peer1 to listen on net */
    guid.chip_id = 1;
    sprintf(cmdbuf, "tell guid%u %u command listen 0x%08x",
            guid.chip_id, port, net);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }

    /* Check command returned call id (also drains events) */
    if (!_vnt_check_remote_call_return_code(guid, default_addrs[guid.chip_id],
                                            port, timeout, _VN_RV_CALLID,
                                            &listen_call[guid.chip_id])) {
        lineno = __LINE__; goto fail;
    }

    /***********************************************************/
    /* Tell peer1 to try to connect to peer1 on newly created net
       and have peer1 reject the connection */
    guid.chip_id = 1;
    sprintf(cmdbuf, "tell guid%u %u command gconnect 0x%08x %u 0x%08x", 
            guid.chip_id, port, owner_guid.device_type,
            owner_guid.chip_id, net);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }

    /* Check connection rejected */
    if (!_vnt_check_connect_rejected(guid, default_addrs[guid.chip_id],
                                     port, timeout,
                                     listen_call[owner_guid.chip_id],
                                     owner_guid,
                                     default_addrs[owner_guid.chip_id],
                                     test_addrs[owner_guid.chip_id],
                                     "REJECT LOCAL CONNECTION TEST"))
    {  lineno = __LINE__; goto fail; }    

    /***********************************************************/
    /* Tell peer 1 to connect to peer1 on net (should succeed) */
    guid.chip_id = 1;
    sprintf(cmdbuf, "tell guid%u %u command gconnect 0x%08x %u 0x%08x",
            guid.chip_id , port, owner_guid.device_type,
            owner_guid.chip_id , net);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }
    
    /* Check connect succeeded and peers notified (also drains events) */
    if (!_vnt_check_connect_succeeded(guid, default_addrs[guid.chip_id],
                                      port, timeout, net, 
                                      listen_call[owner_guid.chip_id],
                                      owner_guid,
                                      default_addrs[owner_guid.chip_id],
                                      test_addrs[owner_guid.chip_id],
                                      1, 1, NULL /*&test_hosts[1]*/,
                                      &test_addrs[guid.chip_id+nguids],
                                      &test_hosts[guid.chip_id+nguids])) {
        lineno = __LINE__; goto fail;
    }

    /***********************************************************/
    /* Tell peer2 to try to connect to peer1 on net
       and have peer1 reject the connection */
    guid.chip_id = 2;
    sprintf(cmdbuf, "tell guid%u %u command gconnect 0x%08x %u 0x%08x", 
            guid.chip_id, port, owner_guid.device_type,
            owner_guid.chip_id, net);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }

    /* Check connection rejected */
    if (!_vnt_check_connect_rejected(guid, default_addrs[guid.chip_id],
                                     port, timeout, 
                                     listen_call[owner_guid.chip_id],
                                     owner_guid,
                                     default_addrs[owner_guid.chip_id],
                                     test_addrs[owner_guid.chip_id],
                                     "REJECT LOCAL CONNECTION TEST"))
    {  lineno = __LINE__; goto fail; }    

    /***********************************************************/
    /* Tell peer 2 to connect to peer1 on net twice (should succeed) */
    guid.chip_id = 2;
    for (i = 0; i < 2; i++) {
        sprintf(cmdbuf, "tell guid%u %u command gconnect 0x%08x %u 0x%08x",
                guid.chip_id, port, owner_guid.device_type,
                owner_guid.chip_id, net);
        rv = _vnt_proc_command(cmdbuf);
        if (rv < 0) { lineno = __LINE__; goto fail; }
        
        /* Check connect succeeded and peers notified (also drains events) */
        if (!_vnt_check_connect_succeeded(guid, default_addrs[guid.chip_id],
                                          port, timeout,
                                          net, listen_call[owner_guid.chip_id],
                                          owner_guid,
                                          default_addrs[owner_guid.chip_id],
                                          test_addrs[owner_guid.chip_id],
                                          i, 2 + i, NULL /*&test_hosts[1]*/,
                                          &test_addrs[guid.chip_id+nguids*i], 
                                          &test_hosts[guid.chip_id+nguids*i])) {
            lineno = __LINE__; goto fail;
        }
    }

    /***********************************************************/
    /* Tell peer 1,2 to connect to peer1 on net again (should fail - net full) */
    for (guid.chip_id = 1; guid.chip_id <= 2; guid.chip_id++) {
        sprintf(cmdbuf, "tell guid%u %u command gconnect 0x%08x %u 0x%08x",
                guid.chip_id, port, owner_guid.device_type,
                owner_guid.chip_id, net);
        rv = _vnt_proc_command(cmdbuf);
        if (rv < 0) { lineno = __LINE__; goto fail; }
        
        /* Check call failed with _VN_ERR_FULL (also drains events) */
        if (!_vnt_check_pending_call_failed(guid, default_addrs[guid.chip_id],
                                            port, timeout, _VN_ERR_FULL)) {
            lineno = __LINE__; goto fail;
        }
    }

    /*********************************************************************/
    /* Tell one host on peer 1,2 to disconnect from net (should succeed) */
    
    for (guid.chip_id = 1; guid.chip_id <= 2; guid.chip_id++) {
        _VN_host_t leaving_host = test_hosts[guid.chip_id+nguids];
        sprintf(cmdbuf, "tell guid%u %u command leave 0x%08x %u",
                guid.chip_id, port, net, leaving_host);
        rv = _vnt_proc_command(cmdbuf);
        if (rv < 0) { lineno = __LINE__; goto fail; }
        
        /* Check command returned 0 */
        event = _vnt_get_next_event(event, &n, evt_buf, sizeof(evt_buf), timeout);
        if (event == NULL) { lineno = __LINE__; goto fail; }
        
        if (!_vnt_check_msg_return_code(__LINE__, event,
                                        default_addrs[guid.chip_id],
                                        port, 0, &callid)) {
            lineno = __LINE__; goto fail;
        }

        /* Check other peers got _VN_EVT_NET_CONFIG */    
        /* NOTE: There is no _VN_EVT_NET_DISCONNECTED event for peer since 
           there is still one more host on the net */
        for (i = 1; i <= 4 - guid.chip_id; i++) {
            _VN_guid_t from_guid;
            
            event = _vnt_get_next_event(event, &n, evt_buf, sizeof(evt_buf), timeout);
            if (event == NULL) { lineno = __LINE__; goto fail; }
            
            /* Don't care who the message is from */
            if (!_vnt_check_msg_event(__LINE__, event, _VN_ADDR_INVALID, port,
                                      invalid_guid, evt_buf2, sizeof(evt_buf2),
                                      &from_guid)) {
                lineno = __LINE__; goto fail;
            }
        
            /* Expect _VN_EVT_NET_CONFIG */
            reason = _VN_EVT_R_HOST_LEFT;
            if (!_vnt_check_net_config_event(__LINE__, (_VN_event_t*) evt_buf2, 
                                             0 /* callid */, net, 0, 1 /* left */, 
                                             &leaving_host, reason)) {
                lineno = __LINE__; goto fail;
            }
        }
    }

    /* Wait for events to drain */
    _vnt_drain_events(evt_buf, sizeof(evt_buf), timeout);
    event = NULL; n = 0;

    /***********************************************************/
    /* Tell peer1 to try to connect to peer1 on net
       and have peer1 reject the connection */
    guid.chip_id = 1;
    sprintf(cmdbuf, "tell guid%u %u command gconnect 0x%08x %u 0x%08x", 
            guid.chip_id, port, owner_guid.device_type,
            owner_guid.chip_id, net);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }

    /* Check connection rejected */
    if (!_vnt_check_connect_rejected(guid, default_addrs[guid.chip_id],
                                     port, timeout,
                                     listen_call[owner_guid.chip_id],
                                     owner_guid,
                                     default_addrs[owner_guid.chip_id],
                                     test_addrs[owner_guid.chip_id],
                                     "REJECT LOCAL CONNECTION TEST"))
    {  lineno = __LINE__; goto fail; }

    /***********************************************************/
    /* Tell peer 1,2 to connect to peer1 on net again (should succeed) */
    for (guid.chip_id = 1; guid.chip_id <= 2; guid.chip_id++) {
        sprintf(cmdbuf, "tell guid%u %u command gconnect 0x%08x %u 0x%08x",
                guid.chip_id, port, owner_guid.device_type,
                owner_guid.chip_id, net);
        rv = _vnt_proc_command(cmdbuf);
        if (rv < 0) { lineno = __LINE__; goto fail; }
        
        /* Check connect succeeded and peers notified (also drains events) */
        if (!_vnt_check_connect_succeeded(guid, default_addrs[guid.chip_id],
                                          port, timeout, net,
                                          listen_call[owner_guid.chip_id],
                                          owner_guid,
                                          default_addrs[owner_guid.chip_id],
                                          test_addrs[owner_guid.chip_id],
                                          1, guid.chip_id + 1, 
                                          NULL /*&test_hosts[1]*/,
                                          &test_addrs[guid.chip_id+nguids], 
                                          &test_hosts[guid.chip_id+nguids])) {
            lineno = __LINE__; goto fail;
        }
    }


    /***********************************************************/
    /* Tell peer1 owner host to leave net (everyone should get kicked out) */
    guid.chip_id = 1;
    sprintf(cmdbuf, "tell guid%u %u command leave 0x%08x %u",
            guid.chip_id, port, net, test_hosts[guid.chip_id]);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }

    /* Check command returned callid */
    event = _vnt_get_next_event(event, &n, evt_buf, sizeof(evt_buf), timeout);
    if (event == NULL) { lineno = __LINE__; goto fail; }

    if (!_vnt_check_msg_return_code(__LINE__, event,
                                    default_addrs[guid.chip_id],
                                    port, 0, &callid)) {
        lineno = __LINE__; goto fail;
    }

    /* Check peer 1 got _VN_EVT_NET_CONFIG and then _VN_EVT_NET_DISCONNECTED */
    /* Check other peers got _VN_EVT_NET_CONFIG and _VN_EVT_NET_DISCONNECTED */
    /* Seven messages expected, 3 for peer1, 3 for peer2 */
    memset(evtcnt, 0, sizeof(evtcnt));
    for (i = 1; i <= 6; i++) {
        _VN_guid_t from_guid;
        _VN_host_t host[1];

        event = _vnt_get_next_event(event, &n, evt_buf, sizeof(evt_buf), timeout);
        if (event == NULL) { lineno = __LINE__; goto fail; }

        /* Don't care who the message is from */
        if (!_vnt_check_msg_event(__LINE__, event, _VN_ADDR_INVALID, port,
                                  invalid_guid, evt_buf2, sizeof(evt_buf2),
                                  &from_guid)) {
            lineno = __LINE__; goto fail;
        }

        evtcnt[from_guid.chip_id]++;
        /* From peer 1,2 - expect _VN_EVT_NET_CONFIG for owner
           then _VN_EVT_NET_CONFIG for localhost
           then _VNT_NET_DISCONNECTED */
        if (evtcnt[from_guid.chip_id] == 1) {                
            host[0] = test_hosts[guid.chip_id];
            reason = _VN_EVT_R_HOST_LEFT;
            if (!_vnt_check_net_config_event(__LINE__, (_VN_event_t*) evt_buf2, 
                                             0, net, 0, 1 /* left */, 
                                             host, reason)) {
                lineno = __LINE__; goto fail;
            }
        }
        else if (evtcnt[from_guid.chip_id] == 2) {
            int nleft;
            nleft = _vn_guid_match(&from_guid, &owner_guid)? 1:2;
            host[0] = test_hosts[from_guid.chip_id];
            reason = _VN_EVT_R_NET_DISCONNECTED;
            if (!_vnt_check_net_config_event(__LINE__, (_VN_event_t*) evt_buf2, 
                                             0, net, 0, nleft, 
                                             NULL/*hosts*/, reason)) {
                lineno = __LINE__; goto fail;
            }
        }
        else {
            if (!_vnt_check_net_disconnected_event(__LINE__, 
                                                   (_VN_event_t*) evt_buf2,
                                                   0 /* callid */, net)) {
                lineno = __LINE__; goto fail;
            }
        }
    }

    goto done;

fail:
    okay = false;
    _VN_TRACE(TRACE_INFO, _VN_SG_TEST,
              "%s: Failed in %s, line %d\n", testname, __FILE__, lineno);

done:
    /* Have peers exit */
    tellPeersTestDone(nguids, port);

    _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
              "%s: Test %s\n", testname, okay? "PASS": "FAILED");
    fflush(stdout);
    return okay;
}

bool testClassIGlobalVN()
{
    int rv, evtcnt[6];
    _VN_port_t port = _VN_PORT_TEST;
    _VN_net_t net;
    uint32_t netmask = _VN_NET_MASK1;
    bool isLocal = false, okay = true;
    char cmdbuf[1024];
    char* testname = "testClassIGlobalVN";
    _VN_addr_t default_addrs[6], test_addrs[6];
    _VN_host_t test_hosts[6];
    uint8_t evt_buf[1024], evt_buf2[1024];
    int n = 0, timeout = 500, lineno;
    _VN_event_t* event = NULL;
    _VN_callid_t callid;
    _VN_guid_t guid = _VN_GUID_INVALID;
    _VN_guid_t owner_guid = { _VN_DEVICE_TYPE_UNKNOWN, 1 };
    _VN_guid_t invalid_guid = _VN_GUID_INVALID;
    uint32_t i, nguids = 5;
    uint8_t reason;
    _VN_callid_t listen_call[6];

    /* Test creation of a class I global VN that 5 peers will attempt 
     * to join and leave.   Tests the following functions and conditions:
     * _VN_get_events:  Event types checked include 
     *                  _VN_new_network_event, _VN_net_config_event
     *                  _VN_connection_accepted_event, _VN_net_disconnected_event,
     *                  _VN_new_message_event
     * _VN_new_net:     For class I global VN
     * _VN_listen_net:  Setting listening of non-default net to on
     *                  Setting listening of non-default net to off
     * _VN_connect:     Successful connect to non-default net
     *                  Joining a net that was not set to listen
     *                  Joining a net that is full
     *                  Joining a net after a peer has left
     * _VN_leave_net:   Non-owner host leaving the net
     *                  Owner host leaving the net
     *                  Non-owner hort forcing another host to leave (FAIL)
     *                  Owner host forcing another host to lave
     *
     */

    /* Wait for 5 clients to join */
    if (!waitClientsJoin(testname, nguids)) {
        lineno = __LINE__; goto fail;
    }

    _VN_TRACE(TRACE_INFO, _VN_SG_TEST, "%s: Starting test\n", testname);
    fflush(stdout);

    /* Have peers forward events to the server so we can check them */
    if (!tellPeersTestStart(nguids, port)) {
        lineno = __LINE__; goto fail; 
    }

    for (guid.chip_id = 1; guid.chip_id <= nguids; guid.chip_id++) {
        default_addrs[guid.chip_id] = _vn_get_default_client_addr(guid);
    }

    /* Wait for events to drain */
    _vnt_drain_events(evt_buf, sizeof(evt_buf), timeout);
    event = NULL; n = 0;

    /***********************************************************/
    /* Tell peer1 to create a class I net (up to 4 peers) */
    guid.chip_id = 1;
    sprintf(cmdbuf, "tell guid%u %u command create 0x%08x %d",
            guid.chip_id, port, netmask, isLocal);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }

    /* Check new net created successfully (also drains events) */
    if (!_vnt_check_create_succeeded(guid, default_addrs[guid.chip_id],
                                     port, timeout, netmask, isLocal,
                                     &test_addrs[guid.chip_id],
                                     &test_hosts[guid.chip_id])) {
        lineno = __LINE__; goto fail;
    }

    net = _VN_addr2net(test_addrs[guid.chip_id]);

    /***********************************************************/
    /* Tell peer2 to try to connect to unknown peer on net (should fail) */ 
    guid.chip_id = 2;
    sprintf(cmdbuf, "tell guid%u %u command gconnect 0x%08x %u 0x%08x", 
            guid.chip_id, port, 0, 16, net);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }

    /* Check call failed with _VN_ERR_NOTFOUND (also drains events) */
    if (!_vnt_check_pending_call_failed(guid, default_addrs[guid.chip_id],
                                        port, timeout, _VN_ERR_NOTFOUND))
    {  lineno = __LINE__; goto fail; }

    /***********************************************************/
    /* Tell peer2 to try to connect to peer1 on newly created net
       (will fail since peer1 is not listening on it yet) */
    guid.chip_id = 2;
    sprintf(cmdbuf, "tell guid%u %u command gconnect 0x%08x %u 0x%08x", 
            guid.chip_id, port, owner_guid.device_type,
            owner_guid.chip_id, net);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }

    /* Check call failed with _VN_ERR_INACTIVE (also drains events) */
    if (!_vnt_check_pending_call_failed(guid, default_addrs[guid.chip_id],
                                        port, timeout, _VN_ERR_INACTIVE))
    {  lineno = __LINE__; goto fail; }

    /***********************************************************/
    /* Tell peer1 to listen on net */
    guid.chip_id = 1;
    sprintf(cmdbuf, "tell guid%u %u command listen 0x%08x",
            guid.chip_id, port, net);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }

    /* Check command returned call id (also drains events) */
    if (!_vnt_check_remote_call_return_code(guid, default_addrs[guid.chip_id],
                                            port, timeout, _VN_RV_CALLID,
                                            &listen_call[guid.chip_id])) {
        lineno = __LINE__; goto fail;
    }

    /***********************************************************/
    /* Tell peer2 to try to connect to peer1 on newly created net
       and have peer1 reject the connection */
    guid.chip_id = 2;
    sprintf(cmdbuf, "tell guid%u %u command gconnect 0x%08x %u 0x%08x", 
            guid.chip_id, port, owner_guid.device_type,
            owner_guid.chip_id, net);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }

    /* Check connection rejected */
    if (!_vnt_check_connect_rejected(guid, default_addrs[guid.chip_id],
                                     port, timeout,
                                     listen_call[owner_guid.chip_id],
                                     owner_guid,
                                     default_addrs[owner_guid.chip_id],
                                     test_addrs[owner_guid.chip_id],
                                     "REJECT CONNECTION TEST"))
    {  lineno = __LINE__; goto fail; }

    /***********************************************************/
    /* Tell peers 2-4 to connect to peer1 on net (should succeed now) */
    for (guid.chip_id = 2; guid.chip_id <= 4; guid.chip_id++) {
        sprintf(cmdbuf, "tell guid%u %u command gconnect 0x%08x %u 0x%08x",
                guid.chip_id, port, owner_guid.device_type,
                owner_guid.chip_id, net);
        rv = _vnt_proc_command(cmdbuf);
        if (rv < 0) { lineno = __LINE__; goto fail; }

        /* Check connect succeeded and peers notified (also drains events) */
        if (!_vnt_check_connect_succeeded(guid, default_addrs[guid.chip_id],
                                          port, timeout, net,
                                          listen_call[owner_guid.chip_id],
                                          owner_guid,
                                          default_addrs[owner_guid.chip_id],
                                          test_addrs[owner_guid.chip_id],
                                          0, guid.chip_id-1, &test_hosts[1],
                                          &test_addrs[guid.chip_id],
                                          &test_hosts[guid.chip_id])) 
        {
            lineno = __LINE__; goto fail;
        }
    }

    /***********************************************************/
    /* Tell peer 5 to connect to peer1 on net (should fail since net full) */
    guid.chip_id = 5;
    sprintf(cmdbuf, "tell guid%u %u command gconnect 0x%08x %u 0x%08x",
            guid.chip_id, port, owner_guid.device_type,
            owner_guid.chip_id, net);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }

    /* Check call failed with _VN_ERR_FULL (also drains events) */
    if (!_vnt_check_pending_call_failed(guid, default_addrs[guid.chip_id],
                                        port, timeout, _VN_ERR_FULL))
    {  lineno = __LINE__; goto fail; }

    /***********************************************************/
    /* Tell peer3 to leave net (everyone should get notified peer3 left) */
    guid.chip_id = 3;
    sprintf(cmdbuf, "tell guid%u %u command leave 0x%08x",
            guid.chip_id, port, net);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }

    /* Check command returned 0 */
    event = _vnt_get_next_event(event, &n, evt_buf, sizeof(evt_buf), timeout);
    if (event == NULL) { lineno = __LINE__; goto fail; }

    if (!_vnt_check_msg_return_code(__LINE__, event,
                                    default_addrs[guid.chip_id],
                                    port, 0, &callid)) {
        lineno = __LINE__; goto fail;
    }

    /* Check peer 3 got _VN_EVT_NET_DISCONNECTED */
    /* Check other peers got _VN_EVT_NET_CONFIG */    
    for (i = 1; i <= 4; i++) {
        _VN_guid_t from_guid;

        event = _vnt_get_next_event(event, &n, evt_buf, sizeof(evt_buf), timeout);
        if (event == NULL) { lineno = __LINE__; goto fail; }

        /* Don't care who the message is from */
        if (!_vnt_check_msg_event(__LINE__, event, _VN_ADDR_INVALID, port,
                                  invalid_guid, evt_buf2, sizeof(evt_buf2),
                                  &from_guid)) {
            lineno = __LINE__; goto fail;
        }
        
        if (_vn_guid_match(&from_guid, &guid)) {
            /* From peer 3 - expect _VN_EVT_NET_DISCONNECTED */
            if (!_vnt_check_net_disconnected_event(__LINE__, (_VN_event_t*) evt_buf2,
                                                   0 /* callid */, net)) {
                lineno = __LINE__; goto fail;
            }
        }
        else {
            /* From another peer - expect _VN_EVT_NET_CONFIG */
            _VN_host_t host[1];
            host[0] = test_hosts[guid.chip_id];
            reason = _VN_EVT_R_HOST_LEFT;
            if (!_vnt_check_net_config_event(__LINE__, (_VN_event_t*) evt_buf2, 
                                             0 /* callid */, net, 0, 1 /* left */, 
                                             host, reason)) {
                lineno = __LINE__; goto fail;
            }
        }
        
    }

    /* Wait for events to drain */
    _vnt_drain_events(evt_buf, sizeof(evt_buf), timeout);
    event = NULL; n = 0;

    /***********************************************************/
    /* Tell peer 5 to connect to peer1 on net (should succeed) */
    guid.chip_id = 5;
    sprintf(cmdbuf, "tell guid%u %u command gconnect 0x%08x %u 0x%08x",
            guid.chip_id, port, owner_guid.device_type,
            owner_guid.chip_id, net);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }

    if (!_vnt_check_connect_succeeded(guid, default_addrs[guid.chip_id],
                                      port, timeout, net,
                                      listen_call[owner_guid.chip_id],
                                      owner_guid,
                                      default_addrs[owner_guid.chip_id],
                                      test_addrs[owner_guid.chip_id],
                                      0, 3, NULL,
                                      &test_addrs[guid.chip_id],
                                      &test_hosts[guid.chip_id])) {
        lineno = __LINE__; goto fail;
    }

    /***********************************************************/
    /* Tell peer 1 to force peer 2 to leave net (peer2 should get kicked out) */
    guid.chip_id = 2;
    sprintf(cmdbuf, "tell guid%u %u command leave 0x%08x %d ",
            owner_guid.chip_id, port, net, test_hosts[guid.chip_id]);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }

    /* Check command returned callid */
    event = _vnt_get_next_event(event, &n, evt_buf, sizeof(evt_buf), timeout);
    if (event == NULL) { lineno = __LINE__; goto fail; }

    if (!_vnt_check_msg_return_code(__LINE__, event,
                                    default_addrs[owner_guid.chip_id], port,
                                    _VN_RV_CALLID, &callid)) {
        lineno = __LINE__; goto fail;
    }

    /* Check peer 2 got _VN_EVT_NET_CONFIG and then _VN_EVT_NET_DISCONNECTED */
    /* Check other peers got _VN_EVT_NET_CONFIG */
    memset(evtcnt, 0, sizeof(evtcnt));
    for (i = 1; i <= nguids; i++) {
        _VN_guid_t from_guid;

        event = _vnt_get_next_event(event, &n, evt_buf, 
                                    sizeof(evt_buf), timeout);
        if (event == NULL) { lineno = __LINE__; goto fail; }

        /* Don't care who the message is from */
        if (!_vnt_check_msg_event(__LINE__, event, _VN_ADDR_INVALID, port,
                                  invalid_guid, evt_buf2, sizeof(evt_buf2),
                                  &from_guid)) {
            lineno = __LINE__; goto fail;
        }
        
        evtcnt[from_guid.chip_id]++;

        if (_vn_guid_match(&from_guid, &guid) && 
            (evtcnt[from_guid.chip_id] == 2)) {
            /* 2nd event from peer 2, expect _VN_EVT_NET_DISCONNECTED */
            if (!_vnt_check_net_disconnected_event(__LINE__, 
                                                   (_VN_event_t*) evt_buf2,
                                                   0 /* callid */, net)) {
                lineno = __LINE__; goto fail;
            }
        }
        else {
            /* From another peer - expect _VN_EVT_NET_CONFIG */
            _VN_host_t host[1];
            _VN_callid_t callid2 = _vn_guid_match(&guid, &owner_guid)? callid: 0;
            host[0] = test_hosts[guid.chip_id];
            reason = _vn_guid_match(&from_guid, &guid)? 
                _VN_EVT_R_HOST_EVICTED: _VN_EVT_R_HOST_LEFT;

            if (!_vnt_check_net_config_event(__LINE__, (_VN_event_t*) evt_buf2, 
                                             callid2, net, 0, 1 /* left */, 
                                             host, reason)) {
                lineno = __LINE__; goto fail;
            }
        }
        
    }

    /* Wait for events to drain */
    _vnt_drain_events(evt_buf, sizeof(evt_buf), timeout);
    event = NULL; n = 0;

    /***********************************************************/ 
    /* Tell peer 5 to force peer 4 to leave net (should fail since 5 not owner) */
    guid.chip_id = 5;
    sprintf(cmdbuf, "tell guid%u %u command leave 0x%08x %u ",
            guid.chip_id, port, net, test_hosts[4]);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }

    /* Check command returned _VN_ERR_HOSTID (also drains events) */
    if (!_vnt_check_remote_call_return_code(guid, default_addrs[guid.chip_id],
                                            port, timeout, _VN_ERR_HOSTID,
                                            NULL)) {
        lineno = __LINE__; goto fail;
    }

    /***********************************************************/
    /* Tell peer1 to stop listening on net */
    guid.chip_id = 1;
    sprintf(cmdbuf, "tell guid%u %u command listen 0x%08x 0",
            guid.chip_id, port, net);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }

    /* Check command returned call id (also drains events) */
    if (!_vnt_check_remote_call_return_code(guid, default_addrs[guid.chip_id],
                                            port, timeout, _VN_RV_CALLID,
                                            &listen_call[guid.chip_id])) {
        lineno = __LINE__; goto fail;
    }

    /***********************************************************/
    /* Tell peer 3 to connect to peer1 on net 
       (should fail, peer1 no longer listening) */
    guid.chip_id = 3;
    sprintf(cmdbuf, "tell guid%u %u command gconnect 0x%08x %u 0x%08x",
            guid.chip_id, port, owner_guid.device_type,
            owner_guid.chip_id, net);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }

    /* Check call failed with _VN_ERR_INACTIVE (also drains events) */
    if (!_vnt_check_pending_call_failed(guid, default_addrs[guid.chip_id],
                                        port, timeout, _VN_ERR_INACTIVE))
    {  lineno = __LINE__; goto fail; }

    /***********************************************************/
    /* Tell peer1 to leave net (everyone should get kicked out) */
    guid.chip_id = 1;
    sprintf(cmdbuf, "tell guid%u %u command leave 0x%08x",
            guid.chip_id, port, net);
    rv = _vnt_proc_command(cmdbuf);
    if (rv < 0) { lineno = __LINE__; goto fail; }

    /* Check command returned callid */
    event = _vnt_get_next_event(event, &n, evt_buf, sizeof(evt_buf), timeout);
    if (event == NULL) { lineno = __LINE__; goto fail; }

    if (!_vnt_check_msg_return_code(__LINE__, event,
                                    default_addrs[guid.chip_id],
                                    port, 0, &callid)) {
        lineno = __LINE__; goto fail;
    }

    /* Check peer 1 got _VN_EVT_NET_DISCONNECTED */
    /* Check other peers got _VN_EVT_NET_CONFIG and _VN_EVT_NET_DISCONNECTED */
    /* Seven messages expected, 1 for owner, 3 each for the other 2 peers */
    memset(evtcnt, 0, sizeof(evtcnt));
    for (i = 1; i <= 7; i++) {
        _VN_guid_t from_guid;

        event = _vnt_get_next_event(event, &n, evt_buf, sizeof(evt_buf), timeout);
        if (event == NULL) { lineno = __LINE__; goto fail; }

        /* Don't care who the message is from */
        if (!_vnt_check_msg_event(__LINE__, event, _VN_ADDR_INVALID, port,
                                  invalid_guid, evt_buf2, sizeof(evt_buf2),
                                  &from_guid)) {
            lineno = __LINE__; goto fail;
        }

        evtcnt[from_guid.chip_id]++;
        if (_vn_guid_match(&from_guid, &guid)) {
            /* From peer 1 - expect _VN_EVT_NET_DISCONNECTED */
            if (!_vnt_check_net_disconnected_event(__LINE__, (_VN_event_t*) evt_buf2,
                                                   0 /* callid */, net)) {
                lineno = __LINE__; goto fail;
            }
        }
        else {
            /* From another peer - expect _VN_EVT_NET_CONFIG for owner
                                     then _VN_EVT_NET_CONFIG for localhost
                                     then _VNT_NET_DISCONNECTED */
            _VN_host_t host[1];
            if (evtcnt[from_guid.chip_id] == 1) {                
                host[0] = test_hosts[guid.chip_id];
                reason = _VN_EVT_R_HOST_LEFT;
                if (!_vnt_check_net_config_event(__LINE__, (_VN_event_t*) evt_buf2, 
                                                 0, net, 0, 1 /* left */, 
                                                 host, reason)) {
                    lineno = __LINE__; goto fail;
                }
            }
            else if (evtcnt[from_guid.chip_id] == 2) {
                host[0] = test_hosts[from_guid.chip_id];
                reason = _VN_EVT_R_NET_DISCONNECTED;
                if (!_vnt_check_net_config_event(__LINE__, (_VN_event_t*) evt_buf2, 
                                                 0, net, 0, 1 /* left */, 
                                                 host, reason)) {
                    lineno = __LINE__; goto fail;
                }
            }
            else {
                if (!_vnt_check_net_disconnected_event(__LINE__, 
                                                       (_VN_event_t*) evt_buf2,
                                                       0 /* callid */, net)) {
                    lineno = __LINE__; goto fail;
                }
            }
        }
        
    }

    /* Wait for events to drain */
    _vnt_drain_events(evt_buf, sizeof(evt_buf), timeout);
    event = NULL; n = 0;
        
    goto done;

fail:
    okay = false;
    _VN_TRACE(TRACE_INFO, _VN_SG_TEST,
              "%s: Failed in %s, line %d\n", testname, __FILE__, lineno);

done:
    /* Have peers exit */
    tellPeersTestDone(nguids, port);

    _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
              "%s: Test %s\n", testname, okay? "PASS": "FAILED");
    fflush(stdout);
    return okay;
}    

void usage()
{
    printf("Usage: vnserver [-g guid] [-i] [-l level] [-h server] [port]\n");
#ifdef _VN_USB_SOCKETS
    printf("                [-u usbport]\n");
    printf("-u usbport TCP port to use for USB connections (default %u)\n",
           _VN_USB_PORT_PROXY);
    
#endif
    printf("-l level   Set trace level\n");
    printf("-i         Interactive mode with commands\n");
    printf("-a flag    To create test net of all peers or not (default: 1)\n");
    printf("-g guid    Use guid for my guid\n");
    printf("-r logfile Uses logfile for binary log of received packets\n");
    printf("-s logfile Uses logfile for binary log of sent packets\n");
    printf("-h server  Hostname or IP address to bind to\n");
}

int main(int argc, char** argv)
{
    _vn_inport_t server_port = _VN_SERVER_TEST_PORT;
    char line[MAXLINE];
    int rv;
    _VN_chip_id_t chip_id = _VN_CHIP_ID_INVALID;
    bool interactive = false;
    bool create_testnet = true;
    int test = 0;
    const char* recv_logfile = "/tmp/vnserver.recv";
    const char* sent_logfile = "/tmp/vnserver.sent";
    const char* server = NULL;
    int trace_level = TRACE_FINE;
    
    memset(_vnt_event_count, 0, sizeof(_vnt_event_count));

#ifdef _VN_USB_SOCKETS
    while ((rv = getopt(argc, argv, "a:g:ir:s:t:h:l:u:")) != -1) {
#else
    while ((rv = getopt(argc, argv, "a:g:ir:s:t:h:l:")) != -1) {
#endif
        switch (rv) {
        case 'l':
            trace_level = atoi(optarg);
            break;
        case 'a':
            create_testnet = atoi(optarg);
            break;
        case 'i':
            interactive = true;
            break;
        case 'g':
            chip_id = atoi(optarg);
            break;
        case 'r':
            recv_logfile = optarg;
            break;
        case 's':
            sent_logfile = optarg;
            break;
        case 't':
            test = atoi(optarg);
            break;
        case 'h':
            server = optarg;
            break;
#ifdef _VN_USB_SOCKETS
        case 'u':
            _vn_netif_set_usb_proxy_port(atoi(optarg));
            break;
#endif                       
        default:
            usage();
            exit(1);
        }
    }

    _vn_set_trace_level(trace_level);

    if (chip_id != _VN_CHIP_ID_INVALID) {
        _vn_set_my_chip_id(chip_id);
    }

    if (optind < argc) {
        server_port = atoi(argv[optind]);
    }

    if ((rv =  _vn_init_server(server, server_port)) < 0) {
        _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                  "_vn_init_server at port %d failed with %d\n",
                  server_port, rv);
        return 1;
    }

    _vn_dbg_setup_logs(sent_logfile, recv_logfile);

    if (create_testnet) {
        /* Create test net of all peers */
        _vnt_test_net_all_call = _VN_new_net(_VN_NET_MASK4, true, 0, NULL, 0);
        if (_vnt_test_net_all_call < 0) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                      "Cannot create test net, rv = %d\n",
                      _vnt_test_net_all_call);
            return 1;
        }
    }

    if (test) {
        bool okay;
        switch (test) {
        case 1:
            okay = testClassIGlobalVN();
            break;
        case 2:
            okay = testMultipleLocalHosts();
            break;
        case 3:
            okay = testDefaultNetLocalHost();
            break;
        case 4:
            okay = testInactivityTimeout();
            break;
        case 5:
            okay = testDuplicateNetId();
            break;
        case 6:
            okay = testNetIdGeneration();
            break;
        case 7:
            okay = testAllLocalHosts();
            break;
        default:
            _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                      "Unknown test number %d\n", test);
            okay = false;
        }
        rv = okay? 0: 1;
    }
    else {
        recv_events_start();
        
        if (interactive) {
            _VN_PRINT("> ");
        }
        while (!_vnt_exit && (fgets(line, MAXLINE, stdin) != NULL)) {
            if (interactive) {
                rv = _vnt_proc_command(line);
                _VN_PRINT("> ");
            }
            else {
                /* Send this */
                rv = _VN_send(_vnt_test_net_all,
                              _vn_get_default_localhost(_vnt_test_net_all),
                              _VN_HOST_ANY,
                              _VN_PORT_TEST, line, 
                              (_VN_msg_len_t) strlen(line), NULL, 0,
                              _VN_MSG_RELIABLE | _VN_MSG_ENCRYPTED);
                if (rv < 0) {
                    _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                              "VN_send failed due to %d\n", rv);
                }
            }
        }
 
        _vn_leave_all();

        recv_events_stop();
        rv = 0;
    }

    _vn_cleanup();
    _vn_dbg_close_logs();
    return rv;
}

#!/bin/bash

# Tests reliable transmission of messages
#
# Runs 2 hosts with VN setup from configuration file with a UDP proxy 
# configured with 40% packet drop rate in the middle..
# 
# Hosts take messages from input file and sends reliable messages to each other
# The messages each host received is compared against the messages the other
# host sent.

testdir=`dirname $0`

execdir=${1:-.}
res=${2:-$execdir/res/vntest3}
testfile=${3:-$0}
vntest="$execdir/vntest"
uproxydir=${execdir%_*}
uproxy="$uproxydir/uproxy"
uproxy_opts="-l 40"

#valgrind="valgrind --tool=memcheck"

if [ ! -e $vntest ] ; then
    echo "$vntest not found"
    exit 1
fi

if [ ! -e $uproxy ] ; then
    echo "uproxy $uproxy not found"
    exit 1
fi

if [ ! -e $testfile ] ; then
    echo "Cannot open test input $testfile"
    exit 1
fi

if [ ! -e $res ] ; then
    mkdir -p $res
fi

$uproxy $uproxy_opts 10034 127.0.0.1 10024 > $res/uproxy1.log 2>&1 &
u1=$!
$uproxy $uproxy_opts 10025 127.0.0.1 10035 > $res/uproxy2.log 2>&1 &
u2=$!

((net=0xc1900000))

$valgrind $vntest -n $net -h 123 -c $testdir/vntest3a.prop -m peer1 -a 3 -t $testfile -s $res/peer1.s -r $res/peer1.r > $res/peer1.log 2>&1 &
w1=$! 

$valgrind $vntest -n $net -h 143 -c $testdir/vntest3b.prop -m peer2 -a 3 -t $testfile -s $res/peer2.s -r $res/peer2.r > $res/peer2.log 2>&1 &
w2=$!

wait $w1 $w2
kill $u1 $u2
wait

$execdir/vnreadpkts -f 1 $res/peer1.r.d $res/peer1.r.d.time0
$execdir/vnreadpkts -f 1 $res/peer1.s.d $res/peer1.s.d.time0
$execdir/vnreadpkts -f 1 $res/peer2.r.d $res/peer2.r.d.time0
$execdir/vnreadpkts -f 1 $res/peer2.s.d $res/peer2.s.d.time0

if ! cmp $res/peer1.r.d.time0 $res/peer2.s.d.time0; then
    echo "peer1.r.d and peer2.s.d does not compare ... test FAILS"
    exit 1
fi

if [ ! -s $res/peer1.r.d.time0 ]  ; then
    echo "peer1.r.d and peer2.s.d has no content ... test FAILS"
    exit 1
fi

if ! cmp $res/peer1.s.d.time0 $res/peer2.r.d.time0; then
    echo "peer1.s.d and peer2.r.d does not compare ... test FAILS"
    exit 1
fi

if [ ! -s $res/peer1.s.d.time0 ]  ; then
    echo "peer1.s.d and peer2.r.d has no content ... test FAILS"
    exit 1
fi

#!/bin/bash

# Tests basic handshake with the server 
#
# Runs VN server with 5 hosts connecting to it (4 good, 1 duplicate)
# Each host that successsfully connects to the server will 
# join a net of all peers, and will send messages (from input file)
# to all other hosts on the network.  Each host will disconnect from
# the network and their default net when done.

testdir=`dirname $0`

execdir=${1:-.}
res=${2:-$execdir/res/vntest4}
testfile=${3:-$0}
vntest="$execdir/vntest"

#valgrind="valgrind --tool=memcheck"

echo "Testfile: $testfile"
echo "Results Directory: $res"

if [ ! -e $vntest ] ; then
    echo "$vntest not found"
    exit 1
fi

if [ ! -e $testfile ] ; then
    echo "Cannot open test input $testfile"
    exit 1
fi

if [ ! -e $res ] ; then
    mkdir -p $res
fi

# Runs test with without preconfigured nets
# Tests basic handshake with the server (4 good, 1 dup)

$valgrind $vntest -m server -d 100 -e 20000 -t $testfile -s $res/server.s -r $res/server.r > $res/server.log -w 4 2>&1 &
$valgrind $vntest -g 1 -m peer1 -d 100 -e 20000 -t $testfile -s $res/peer1.s -r $res/peer1.r > $res/peer1.log 2>&1 &
$valgrind $vntest -g 2 -m peer2 -d 100 -e 20000 -t $testfile -s $res/peer2.s -r $res/peer2.r  > $res/peer2.log 2>&1 &
$valgrind $vntest -g 3 -m peer3 -d 100 -e 20000 -t $testfile -s $res/peer3.s -r $res/peer3.r > $res/peer3.log 2>&1 &
$valgrind $vntest -g 4 -m peer4 -d 100 -e 20000 -t $testfile -s $res/peer4.s -r $res/peer4.r > $res/peer4.log 2>&1 &
$valgrind $vntest -g 1 -m peer1 -d 100 -e 20000 -t $testfile -s $res/peer1dup.s -r $res/peer1dup.r > $res/peer1dup.log 2>&1 &

wait

# TODO: Check test results
#grep "Sent to" $res/*.log > $res/sent
#grep "Recv from" $res/*.log > $res/recv
#$testdir/vnmatchsr.pl $res/sent $res/recv

# Make sure only 0 VN_init  failed 
# (peer1 and peer1dup will both succeed since session id is different)
egrep -e "_vn_init_client to localhost:[0-9]+ failed" $res/*.log | {

lines=0
fail=0
while read myline; do
    (( lines += 1 ))
    if ! echo $myline | grep -q peer1 ; then
        echo "FAILED: $myline unexpected"
        fail=1
    fi
done 

if [[ $lines -ne 0 ]] ; then
    echo "FAILED: VN_init failed $lines times, should have failed 0 times"
    fail=1
fi

exit $fail

}

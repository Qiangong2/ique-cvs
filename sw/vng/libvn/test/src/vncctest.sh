#!/bin/bash

function run_vncc_test {
    # Tests VN network management API with n clients

    memcheck=$1
    testnum=$2
    npeers=$3
    testdir=$4
    execdir=$5
    res=$6

    echo "run_vnclient_test: $*"

    vnclient="$execdir/vnclient"

    if [ $memcheck -eq 1 ] ; then
        valgrind="valgrind --tool=memcheck --num-callers=20 --leak-check=full"
    else 
        valgrind=""
    fi
        
    if [ ! -e $vnclient ] ; then
        echo "$vnclient not found"
        exit 1
    fi
    
    if [ ! -e $res ] ; then
        mkdir -p $res
    fi

    n=1
    while [ $n -le $npeers ] ; do
        $valgrind $vnclient -t $testnum -i 0 -g $n -s $res/peer$n.s -r $res/peer$n.r > $res/peer$n.log 2>&1 &
        sleep 1
        procs[$n]="$!"
        (( n++ ))
    done

    fail=0
    n=1;
    while [ $n -le $npeers ] ; do
        if [ $fail -eq 1 ] ; then
            # Already failed, just kill process
            kill -9 ${procs[$n]}
        fi
        wait ${procs[$n]}
        if [[ $? -ne 0 ]] ; then
            echo "Peer $n failed"
            fail=1
        fi
        (( n++ ))
    done 
    
    if [[ $fail -ne 0 ]] ; then
        echo "FAILED"
    fi
    
    return $fail
}

testdir=`dirname $0`

testname=$1
execdir=${2:-.}
resdir=${3:-$execdir/res/$testname}

case "$testname" in
"vntestupnp1")
    # Runs 2 VN clients (Discover, no Query)
    # (see testSimpleHostDiscovery in vnclient.c)
    testnum=1
    npeers=2
    ;;

"vntestupnp2")
    # Runs 2 VN clients (No Discover, Query)
    # (see testSimpleHostDiscovery in vnclient.c)
    testnum=2
    npeers=2
    ;;

"vntestupnp3")
    # Runs 2 VN clients (Discover, Query)
    # (see testSimpleHostDiscovery in vnclient.c)
    testnum=3
    npeers=2
    ;;

"vntestupnp4")
    # Runs 2 VN clients (No Discover, Query, Clear Query List)
    # (see testSimpleHostDiscovery in vnclient.c)
    testnum=4
    npeers=2
    ;;

*)
    echo "Unknown test $testname"
    exit 1
    ;;
esac

memcheck=0
sleep 5
run_vncc_test $memcheck $testnum $npeers $testdir $execdir $resdir


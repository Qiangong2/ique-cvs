//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnlocal.h"
#include "vnhs.h"
#include "vntest.h"

int _vnt_accept_connect = 1;
int _vnt_accept_net = 1;
int _vnt_exit = 0;
_VN_net_t _vnt_test_net_all = _VN_NET_TEST_ALL;

/**
 * Tags for valid commands issued at the command prompt 
 */
typedef enum {
    CMD_HELP = 0, 
    CMD_DUMP,
    CMD_NET_RESET,          /* Resets the VN (_VN_reset) */
    CMD_NET_CREATE,         /* Creates a new net (_VN_new_net) */
    CMD_NET_LISTEN,         /* Activates a net to listen for connections (_VN_listen_net) */
    CMD_NET_ACCEPT,         /* Accepts/Rejects a join net request (_VN_accept) */
    CMD_NET_ACCEPT_NEW,     /* Accepts/Rejects a new net request (_VN_accept_net) */
    CMD_NET_ADDR_CONNECT,   /* Connect using Hostname/Port/Net */
    CMD_NET_GUID_CONNECT,   /* Connect using GUID/Net (_VN_connect) */
    CMD_NET_SERVER_CONNECT, /* Connect to server (_VN_sconnect) */
    CMD_NET_LEAVE,          /* Leaves a net (_VN_leave_net) */
    CMD_NET_SET_DAEMON,     /* Mark a net as a daemon net (_VN_set_daemon_net) */
    CMD_CANCEL_EVENTS,      /* Cancels _VN_get_events (_VN_cancel_get_events) */
    CMD_PORT_CONFIG,        /* Configures a VN port (_VN_config_port) */
    CMD_PORT_PRIORITY,      /* Sets a VN port's priority (_VN_set_priority) */
    CMD_HOST_DISCOVERY,     /* Starts/Stops host discovery (_VN_discover_hosts) */
    CMD_HOST_QUERY,         /* Queries a host for services (_VN_query_host) */
    CMD_HOST_QUERY_CLEAR,   /* Clears the list of hosts to be queried (_VN_clear_query_list) */
    CMD_SERVICE_REGISTER,   /* Registers a service for host discovery (_VN_register_service) */
    CMD_SERVICE_UNREGISTER, /* Unregisters a service (_VN_unregister_service) */
    CMD_CLK_CALIBRATE,      /* Clock calibration (_VN_calibrate_clock) */
    CMD_MSG_SEND,           /* Sends a message (_VN_send) */
    CMD_MSG_GUID_SEND,      /* Sends a message by guid (_VN_send) */
    CMD_WHOAMI,             /* Provide information about this client */
    CMD_GET_TIME,           /* Returns the current time */
    CMD_GET_STATUS,         /* Returns the status */
    CMD_TEST_PING,          /* Test ping */
    CMD_SET,
    CMD_EXIT
} _vnt_cmd_t;

/**
 * Data structure for parsing VN commands from the command line 
 */
typedef struct __vnt_cmd_info_t {
    char *name;                   /* command name */
    _vnt_cmd_t cmdnum;            /* command number */
    int numargs_min;              /* the minimum number of arguments */
    int numargs_max;              /* the maximum number of arguments */
    char *args;                   /* the args */
    /* function for processing the command */
    int (*cmdfunc) (struct __vnt_cmd_info_t* cmd, void* data);
    char *desc;
    char *detail;
} _vnt_cmd_info_t;

int _vnt_cmd_notsupported(_vnt_cmd_info_t* cmd, void* data);
int _vnt_cmd_help(_vnt_cmd_info_t* cmd, void* data);
int _vnt_cmd_dump(_vnt_cmd_info_t* cmd, void* data);
int _vnt_cmd_reset(_vnt_cmd_info_t* cmd, void* data);
int _vnt_cmd_new_net(_vnt_cmd_info_t* cmd, void* data);
int _vnt_cmd_accept(_vnt_cmd_info_t* cmd, void* data);
int _vnt_cmd_accept_new(_vnt_cmd_info_t* cmd, void* data);
int _vnt_cmd_listen_net(_vnt_cmd_info_t* cmd, void* data);
int _vnt_cmd_addr_connect_net(_vnt_cmd_info_t* cmd, void* data);
int _vnt_cmd_guid_connect_net(_vnt_cmd_info_t* cmd, void* data);
int _vnt_cmd_server_connect_net(_vnt_cmd_info_t* cmd, void* data);
int _vnt_cmd_leave_net(_vnt_cmd_info_t* cmd, void* data);
int _vnt_cmd_set_daemon_net(_vnt_cmd_info_t* cmd, void* data);
int _vnt_cmd_config_port(_vnt_cmd_info_t* cmd, void* data);
int _vnt_cmd_set_priority(_vnt_cmd_info_t* cmd, void* data);
int _vnt_cmd_send_msg(_vnt_cmd_info_t* cmd, void* data);
int _vnt_cmd_guid_send_msg(_vnt_cmd_info_t* cmd, void* data);
int _vnt_cmd_set(_vnt_cmd_info_t* cmd, void* data);
int _vnt_cmd_whoami(_vnt_cmd_info_t* cmd, void* data);
int _vnt_cmd_get_time(_vnt_cmd_info_t* cmd, void* data);
int _vnt_cmd_cancel_get_events(_vnt_cmd_info_t* cmd, void* data);
int _vnt_cmd_discover_hosts(_vnt_cmd_info_t* cmd, void* data);
int _vnt_cmd_query_host(_vnt_cmd_info_t* cmd, void* data);
int _vnt_cmd_clear_query_list(_vnt_cmd_info_t* cmd, void* data);
int _vnt_cmd_service_register(_vnt_cmd_info_t* cmd, void* data);
int _vnt_cmd_service_unregister(_vnt_cmd_info_t* cmd, void* data);
int _vnt_cmd_clk_calibrate(_vnt_cmd_info_t* cmd, void* data);
int _vnt_cmd_get_status(_vnt_cmd_info_t* cmd, void* data);
int _vnt_cmd_test_ping(_vnt_cmd_info_t* cmd, void* data);
int _vnt_cmd_exit(_vnt_cmd_info_t* cmd, void* data);

/**
 * Mappings between command text names, command tag,
 * and required command arguments for command line
 * commands 
 */
static _vnt_cmd_info_t _vnt_cmdlist[] = {
    { "Help", CMD_HELP, 0, 1, "<Command>", &_vnt_cmd_help, 
      "Displays help", NULL },

    { "Dump", CMD_DUMP, 0, 1, "<Data> <Value>", &_vnt_cmd_dump, 
      "Dumps VN data structures", 
      "Parameters\n"
      "<Data> = Data structures to dump\n"
      "         stats = VN statistics\n"
      "         nets = VN nets\n"
      "         net = Specific VN net\n"
      "         host = Specific VN host\n"
      "         devices = Devices\n"
      "         device = Specific device\n"
#ifdef UPNP
      "         upnp = UPNP VN devices\n"
      "         nats = NAT devices\n"
#endif
      "         hs = Handshake tables\n"
      "<Value> = VN net id, address, or device guid\n" },

    { "Reset", CMD_NET_RESET, 0, 1, "<Flag>", &_vnt_cmd_reset, 
      "Resets VN",
      "Parameters\n"
      "<Flag> = Reset all (1) or only non-daemon nets (0). (default = 1)\n"},

    { "Create", CMD_NET_CREATE, 0, 3,  "<Netmask> <IsLocal> <Message>",
      &_vnt_cmd_new_net,
      "Create a new net",
      "Parameters\n"
      "<Netmask> = Netmask indicating what class of VN to create\n"
      "            0xfffffffc = Class I net with up to 4 hosts\n"
      "            0xfffffff0 = Class II net with up to 16 hosts\n"
      "            0xffffffc0 = Class III net with up to 256 hosts\n"
      "            0xffff0000 = Class IV net with up to 65536 hosts\n"
      "<IsLocal> = Whether the new net should have a local (1) or global (0) ID\n"
      "<Message> = Optional message to send\n" },

    { "Listen", CMD_NET_LISTEN, 0, 2, "<Net> <Flag>",
      &_vnt_cmd_listen_net,
      "Allows or disallows new game devices to connect to the specified VN",
      "Parameters\n"
      "<Net> = Net ID of VN (default = _VN_NET_DEFAULT)\n"
      "<Flag> = Indicates to allow connections (1) or not (0). (default = 1)\n" },

    { "Accept", CMD_NET_ACCEPT, 2, 4,
      "<Device ID> <Session ID> <Flag> <Message>",
      &_vnt_cmd_accept,
      "Accepts or rejects connecting game devices",
      "Parameters\n"
      "<Device ID> = Device ID of connecting device\n"
      "<Session ID> = Session ID of handshake\n"
      "<Flag> = Indicates to accept (1) or reject (0). (default = 1)\n"
      "<Message> = Optional message to send (i.e. error message)\n" },

    { "AcceptNew", CMD_NET_ACCEPT_NEW, 2, 4, "<RequestID> <Netmask> <Flag> <Message>",
      &_vnt_cmd_accept_new,
      "Accepts or rejects new net request",
      "Parameters\n"
      "<RequestID> = Request ID\n"
      "<Netmask> = Netmask\n"
      "<Flag> = Indicates to accept (1) or reject (0). (default = 1)\n"
      "<Message> = Optional message to send (i.e. error message)\n" },

    { "Connect", CMD_NET_ADDR_CONNECT, 0, 4, "<Host> <Port> <Net> <Message>", 
      &_vnt_cmd_addr_connect_net, 
      "Initiates a connection to a VN based on IP address and port",
      "Parameters:\n"
      "<Host> = Hostname or IP address (default = \"localhost\")\n"
      "<Port> = UDP port (default = _VN_SERVER_TEST_PORT)\n"
      "<Net>  = Net ID of VN (default = _VN_NET_DEFAULT)\n"
      "<Message> = Optional message to send\n" },

    { "GConnect", CMD_NET_GUID_CONNECT, 1, 3,
      "<Device Type> <Device ID> <Net> <Message>", 
      &_vnt_cmd_guid_connect_net,
      "Initiates a connection to a VN based on GUID (must be connected to server first)",
      "Parameters:\n"
      "<Device Type> = Device type\n"
      "<Device ID> = Device ID\n"
      "<Net>  = Net ID of VN (default = _VN_NET_DEFAULT)\n"
      "<Message> = Optional message to send\n" },

    { "SConnect", CMD_NET_SERVER_CONNECT, 0, 3, "<Host> <Port> <Message>",
      &_vnt_cmd_server_connect_net,
      "Initiates a connection to a server",
      "Parameters:\n"
      "<Host> = Hostname or IP address of server (default = \"localhost\")\n"
      "<Port> = UDP Port of server (default = VN_SERVER_TEST_PORT)\n" 
      "<Message> = Optional message to send\n" },

    { "Leave", CMD_NET_LEAVE, 0, 2, "<Net> <Host>", 
      &_vnt_cmd_leave_net,
      "Remove the specifed host from the VN",
      "Parameters:\n"
      "<Net>  = Net ID of VN (default = _VN_NET_DEFAULT_\n"
      "<Host> = Host ID of leaving host (default = first localhost)\n" },

    { "Cancel", CMD_CANCEL_EVENTS, 0, 0, "", 
      &_vnt_cmd_cancel_get_events,
      "Cancels any ongoing _VN_get_events", NULL },

    { "SetDaemon", CMD_NET_SET_DAEMON, 0, 1, "<Net>", 
      &_vnt_cmd_set_daemon_net, 
      "Marks the specified net as a daemon net",
      "Parameters\n"
      "<Net> = Net ID of VN to mark\n" },

    { "ConfigPort", CMD_PORT_CONFIG, 3, 4, "<Net> <Host> <Port> <State>",
      &_vnt_cmd_config_port, 
      "Activates/deactivates specified port to receive incoming messages",
      "Parameters\n"
      "<Net> = Net ID of VN\n"
      "<Host> = Host ID of localhost\n"
      "<Port> = VN Port\n"
      "<State> = To activate or deactivate port\n"
      "          1 (active), 0 (inactive), 2 (default, to show current state)\n" },

    { "SetPriority", CMD_PORT_PRIORITY, 4, 4, "<Net> <Host> <Port> <Priority>",
      &_vnt_cmd_set_priority, 
      "Sets the priority for the specified host and port",
      "Parameters\n"
      "<Net>  = Net ID of VN\n"
      "<Host> = Host ID of destination host\n"
      "<Port> = VN Port\n"
      "<Priority> = New Priority (0 = low to 7 = high)\n" },

    { "DiscoverHosts", CMD_HOST_DISCOVERY, 0, 1, "<flag>",
      &_vnt_cmd_discover_hosts,
      "Starts or stops host discovery ",
      "Parameters\n"
      "<flag> = To start or stop host discovery\n"
      "         1 (default, start host discover), 0 (stop host discovery)\n" },

    { "QueryHost", CMD_HOST_QUERY, 0, 2, "<Host> <Port>", 
      &_vnt_cmd_query_host, 
      "Queries a host for services based on hostname and port",
      "Parameters:\n"
      "<Host> = Hostname or IP address (default = \"localhost\")\n"
      "<Port> = UDP port (default = _VN_SERVER_TEST_PORT)\n" },

    { "ClearQueryHosts", CMD_HOST_QUERY_CLEAR, 0, 0, "", 
      &_vnt_cmd_clear_query_list, 
      "Clears the list of hosts to be queried\n", NULL },

    { "RegisterService", CMD_SERVICE_REGISTER, 1, 3, "<Service ID> <public> <Msg>",
      &_vnt_cmd_service_register,
      "Registers a service for host discovery",
      "Parameters\n"
      "<Service ID> = Unique ID of the service\n"
      "<Public> = If the service should be broadcast to other peers (default 1)\n"
      "<Msg> = Optional message describing the service\n" },

    { "UnregisterService", CMD_SERVICE_REGISTER, 1, 1, "<Service ID>",
      &_vnt_cmd_service_unregister,
      "Unregisters a service",
      "Parameters\n"
      "<Service ID> = Unique ID of the service\n" },

    { "CalibrateClock", CMD_CLK_CALIBRATE, 2, 2, "<Net> <Host>", 
      &_vnt_cmd_clk_calibrate,
      "Performs clock calibration",
      "Parameters\n"
      "<Net>  = Net ID of VN\n"
      "<Host> = Host ID to do clock calibration with\n" },

    { "Send", CMD_MSG_SEND, 5, 5, "<Net> <From> <To> <Port> <Message>",
      &_vnt_cmd_send_msg,
      "Sends message from one host to another on the specified network",
      "Parameters:\n"
      "<Net>  = Net ID of VN\n"
      "<From> = Host ID of localhost to send the message from\n"
      "<To>   = Host ID of destination host\n"
      "<Port> = VN port to use\n"
      "<Message> = Message to send\n" },

    { "Tell", CMD_MSG_GUID_SEND, 3, 3, "<Device> <Port> <Message>",
      &_vnt_cmd_guid_send_msg,
      "Sends message to the specifed device on the default network",
      "Parameters:\n"
      "<Device> = Device to send to\n"
      "           \"Guid<guid>\" - Sends to device with the specifed guid\n"
      "           \"Server\" - Sends to server\n"
      "<Port> = VN port to use\n"
      "<Message> = Message to send\n" },

    { "Set", CMD_SET, 2, 2, "<Variable> <Value>", 
      &_vnt_cmd_set, 
      "Sets a variable to the specifed value",
      "Parameters:\n"
      "<Variable> = Variable to set\n"
      "<Value>    = New Value (Shows old value if not specified)\n"
      "Variables:\n"
      "   Accept     - Automatically accept join net requests\n"
      "   Encryption - Use encryption for messages (1 = on, 0 = off, default = off)\n"
      "   Reliable   - Use reliable mode for messages (1 = on, 0 = off, default = off)\n"
      "   Timeout    - Set Timeout\n"
      "   Trace      - Trace Level ( 0 = off, 6 = finest)\n" },

    { "WhoAmI", CMD_WHOAMI, 0, 0, "", 
      &_vnt_cmd_whoami,
      "Displays my GUID and port information", NULL },

    { "Time", CMD_GET_TIME, 0, 0, "", 
      &_vnt_cmd_get_time,
      "Displays the current time", NULL },

    { "Status", CMD_GET_STATUS, 0, 0, "", 
      &_vnt_cmd_get_status,
      "Displays the VN status", NULL },

    { "TestPing", CMD_TEST_PING, 0, 3, 
      "<Count> <Buffer Size> <Others>", 
      &_vnt_cmd_test_ping,
      "Test ping", 
      "Parameters:\n"
      "<Count>         = Number of pings to send (default = 1)\n"
      "<Buffer Size>   = Size of ping buffer (default = 128)\n"
      "<Others>        = Number of other hosts in test VN (default = 1}\n" },

    { "Quit", CMD_EXIT, 0, 0, "<Leave net?>", 
      &_vnt_cmd_exit, 
      "Exits this program", 
      "Parameters:\n"
      "<Leave nets?> = Whether to leave all nets nicely when exiting\n" },

    { "Exit", CMD_EXIT, 0, 0, "<Leave net?>", 
      &_vnt_cmd_exit, 
      "Exits this program", 
      "Parameters:\n"
      "<Leave nets?> = Whether to leave all nets nicely when exiting\n" }
};

int _vnt_cmd_notsupported(_vnt_cmd_info_t* cmd, void* data)
{
    assert(cmd);
    _VN_PRINT("Command '%s' not supported\n", cmd->name);
    return _VN_ERR_NOSUPPORT;
}

int _vnt_send_attr = 0;

int _vnt_cmd_help(_vnt_cmd_info_t* cmd, void* data)
{
    int i, n = 0;
    char cmdstr[20];
    if (data != NULL) {
        n = sscanf(data, "%s", cmdstr);
    }
    if (n >= 1) {
        bool found = false;
        for (i = 0; i < sizeof(_vnt_cmdlist)/sizeof(_vnt_cmdlist[0]); i++) {
            if (strcasecmp(_vnt_cmdlist[i].name, cmdstr) == 0) {
                _VN_PRINT("%s %s:\n    %s\n", _vnt_cmdlist[i].name, _vnt_cmdlist[i].args,
                          _vnt_cmdlist[i].desc);
                if (_vnt_cmdlist[i].detail) {
                    _VN_PRINT("%s", _vnt_cmdlist[i].detail);
                }
                found = true;
                break;
            }
        }
        if (!found) {
            _VN_PRINT("Unknown command '%s'.  Try 'Help'\n", cmdstr);
        }
    }
    else {
        for (i = 0; i < sizeof(_vnt_cmdlist)/sizeof(_vnt_cmdlist[0]); i++) {
            _VN_PRINT("%s %s:\n    %s\n", _vnt_cmdlist[i].name, _vnt_cmdlist[i].args,
                      _vnt_cmdlist[i].desc);
        }
    }
    return _VN_ERR_OK;
}

int _vnt_cmd_dump(_vnt_cmd_info_t* cmd, void* data)
{
    int n = 0;
    char datastr[20];
    int handle = 0;
    int handle2 = 0;
    if (data != NULL) {
        n = sscanf(data, "%s 0x%x 0x%x", datastr, &handle, &handle2);
        if (n < 3) {
            n = sscanf(data, "%s 0x%x %d", datastr, &handle, &handle2);
        }
        if (n < 2) {
            n = sscanf(data, "%s %d", datastr, &handle);
        }
    }
    if (n >= 1) {
        if (strcasecmp(datastr, "stats") == 0) {
            _vn_print_stats(stdout);
        }
        else if (strcasecmp(datastr, "nets") == 0) {
            _vn_dump_net_tables(stdout, handle);
        }
        else if (strcasecmp(datastr, "net") == 0) {
            _vn_net_info_t* net_info;
            net_info = _vn_lookup_net_info(handle);
            if (net_info) {
                _vn_print_net_info(stdout, net_info);
            }
            else {
                _VN_PRINT("Unknown net 0x%08x\n", handle);
            }
        }
        else if (strcasecmp(datastr, "host") == 0) {
            _vn_net_info_t* net_info;
            _VN_net_t net_id = _VN_addr2net(handle);
            _VN_host_t host_id = _VN_addr2host(handle);
            net_info = _vn_lookup_net_info(net_id);
            if (net_info) {
                _vn_host_info_t* host_info;
                host_info = _vn_lookup_host_info(net_info, host_id);
                if (host_info) {
                    _vn_print_host_info(stdout, host_info);
                }
                else {
                    _VN_PRINT("Unknown host 0x%08x\n", handle);
                }
            }
            else {
                _VN_PRINT("Unknown net 0x%08x\n", net_id);
            }
        }
        else if (strcasecmp(datastr, "devices") == 0) {
            _vn_dump_devices(stdout);
        }
        else if (strcasecmp(datastr, "device") == 0) {
            if (n > 2) {
                _vn_device_info_t* device;
                _VN_guid_t guid;
                guid.device_type = handle;
                guid.chip_id = handle2;
                device = _vn_lookup_device(guid);
                if (device) {
                    _vn_print_device(stdout, device);
                }
                else {
                    _VN_PRINT("Unknown device 0x%08x-%08x (%u)\n", 
                              guid.device_type, 
                              guid.chip_id, guid.chip_id);
                }
            }
            else {
                _VN_PRINT("Please specify both the device type and id\n");
            }
        }
#ifdef UPNP
        else if (strcasecmp(datastr, "upnp") == 0) {
            _vn_upnp_dump_devices(stdout);
        }
        else if (strcasecmp(datastr, "nats") == 0) {
            _vn_upnp_dump_nats(stdout);
        }
#endif
        else if (strcasecmp(datastr, "hs") == 0) {
            _vn_dump_hs_tables(stdout);
        }
        else {
            _VN_PRINT("Unknown data structures '%s'.  Try 'Help Dump'\n", datastr);
        }
    }
    else {
        _vn_dump_state(stdout, handle);
    }
    return _VN_ERR_OK;
}

int _vnt_cmd_reset(_vnt_cmd_info_t* cmd, void* data)
{
    int flag = true;
    int n;
    assert(cmd);
    if (data == NULL) return _VN_ERR_FAIL;
    n = sscanf(data, "%d", &flag);
    _VN_reset(flag);
    return _VN_ERR_OK;
}

int _vnt_cmd_new_net(_vnt_cmd_info_t* cmd, void* data)
{
    int netmask = 0xf, is_local = false;
    int n, rv, parsed = 0;
    _VN_addr_t vn_server;   
    char* msg = NULL;
    uint16_t msglen = 0;

    assert(cmd);
    if (data == NULL) return _VN_ERR_FAIL;
    n = sscanf(data, "0x%x %d %n", &netmask, &is_local, &parsed);
    if (n < 1) {
        n = sscanf(data, "%u %d %n", &netmask, &is_local, &parsed);
    }
    if (parsed) {
        msg = (uint8_t*) data + parsed;
        msglen = (uint16_t) strlen(msg);
    }
    vn_server = _vn_get_default_server();
    rv = _VN_new_net(netmask, is_local, vn_server, msg, msglen);
    return rv;
}

int _vnt_cmd_accept(_vnt_cmd_info_t* cmd, void* data)
{
    _VN_guid_t guid;
    uint32_t session_id;
    int rv, n, parsed = 0, flag = 1;
    char* msg = NULL;
    uint16_t msglen = 0;

    assert(cmd);
    if (data == NULL) return _VN_ERR_FAIL;
    guid.device_type = _VN_DEVICE_TYPE_UNKNOWN;
    n = sscanf(data, "%u %u %d %n",
               &guid.chip_id, &session_id, &flag, &parsed);
    if (n >= 2) {
        if (parsed) {
            msg = (uint8_t*) data + parsed;
            msglen = (uint16_t) strlen(msg);
        }
        rv = _VN_accept(_VN_HS_REQID(guid, session_id),
                        flag, msg, msglen);
    }
    else {
        rv = _VN_ERR_FAIL;
    }
    return rv;
}

int _vnt_cmd_accept_new(_vnt_cmd_info_t* cmd, void* data)
{
    uint64_t request_id;
    uint32_t netmask;
    int rv, n, parsed = 0, flag = 1;
    char* msg = NULL;
    uint16_t msglen = 0;

    assert(cmd);
    if (data == NULL) return _VN_ERR_FAIL;
    n = sscanf(data, "%llu 0x%x %d %n", &request_id, &netmask, &flag, &parsed);
    if (n < 2) {
        n = sscanf(data, "%llu %u %d %n", 
                   &request_id, &netmask, &flag, &parsed);
    }
    if (n >= 2) {
        if (parsed) {
            msg = (uint8_t*) data + parsed;
            msglen = (uint16_t) strlen(msg);
        }
        printf("Accept net %llu netmask 0x%08x, flag %d\n",
                request_id, netmask, flag);
        rv = _VN_accept_net(request_id, netmask, flag, msg, msglen);
    }
    else {
        rv = _VN_ERR_FAIL;
    }
    return rv;
}

int _vnt_cmd_listen_net(_vnt_cmd_info_t* cmd, void* data)
{
    _VN_net_t net_id = _VN_NET_DEFAULT;
    int listen = true;
    int n;
    assert(cmd);
    if (data == NULL) return _VN_ERR_FAIL;
    n = sscanf(data, "0x%x %d", &net_id, &listen);
    if (n < 1) {
        n = sscanf(data, "%u %d", &net_id, &listen);
    }
    return _VN_listen_net(net_id, listen);
}

int _vnt_cmd_leave_net(_vnt_cmd_info_t* cmd, void* data)
{
    _VN_net_t net_id = _VN_NET_DEFAULT;
    _VN_addr_t addr;
    uint32_t host_id = _VN_HOST_INVALID;
    int n, rv;

    assert(cmd);
    if (data == NULL) return _VN_ERR_FAIL;
    n = sscanf(data, "0x%x %u", &net_id, &host_id);
    if (n < 1) {
        n = sscanf(data, "%u %u", &net_id, &host_id);
    }
    if (host_id == _VN_HOST_INVALID) {
        addr = _vn_get_default_localaddr(net_id);
    }
    else {
        addr = _VN_make_addr(net_id, host_id);
    }
    rv = _VN_leave_net(addr, _vn_get_default_server());
    return rv;
}

int _vnt_cmd_set_daemon_net(_vnt_cmd_info_t* cmd, void* data)
{
    _VN_net_t net_id = _VN_NET_DEFAULT;
    int n, rv;

    assert(cmd);
    if (data == NULL) return _VN_ERR_FAIL;
    n = sscanf(data, "0x%x", &net_id);
    if (n < 1) {
        n = sscanf(data, "%u", &net_id);
    }
    rv = _VN_set_daemon_net(net_id);
    return rv;
}

int _vnt_cmd_addr_connect_net(_vnt_cmd_info_t* cmd, void* data)
{
    _VN_net_t net_id = _VN_NET_DEFAULT;
    char hostname[40];
    uint32_t port = _VN_SERVER_TEST_PORT;
    int rv, call_id, n, parsed = 0;
    _vn_inaddr_t ipaddr;
    char* msg = NULL;
    uint16_t msglen = 0;
    _VN_guid_t guid;

    assert(cmd);
    if (data == NULL) return _VN_ERR_FAIL;
    n = sscanf(data, "%s %u 0x%x %n", hostname, &port, &net_id, &parsed);
    if (n < 3) {
        n = sscanf(data, "%s %u %u %n", hostname, &port, &net_id, &parsed);
    }
    if (n < 1) {
        sprintf(hostname, "localhost");
    }

    ipaddr = _vn_netif_getaddr(hostname);
    if (ipaddr == _VN_INADDR_INVALID) {
        return _VN_ERR_SERVER;
    }

    call_id = _vn_new_call_id();
    if (parsed) {
        msg = (uint8_t*) data + parsed;
        msglen = (uint16_t) strlen(msg);
    }
    _vn_guid_set_invalid(&guid);
    rv = _vn_hs_connect(guid, true, call_id,
                        ipaddr, port, net_id, msg, msglen);
    if (rv >= 0) {
        rv = call_id;
    }
    return rv;
}

int _vnt_cmd_guid_connect_net(_vnt_cmd_info_t* cmd, void* data)
{
    _VN_net_t net_id = _VN_NET_DEFAULT;
    _VN_guid_t guid;
    int rv, n, parsed = 0;
    char* msg = NULL;
    uint16_t msglen = 0;

    assert(cmd);
    if (data == NULL) return _VN_ERR_FAIL;
    n = sscanf(data, "0x%x %u 0x%x %n", 
               &guid.device_type, &guid.chip_id, &net_id, &parsed);
    if (n < 3) {
        n = sscanf(data, "0x%x %u %u %n", 
                   &guid.device_type, &guid.chip_id, &net_id, &parsed);
    }
    if (n >= 2) {
        if (parsed) {
            msg = (uint8_t*) data + parsed;
            msglen = (uint16_t) strlen(msg);
        }
        rv = _VN_connect(guid, net_id, _vn_get_default_server(), msg, msglen);
    }
    else {
        rv = _VN_ERR_FAIL;
    }
    return rv;
}

int _vnt_cmd_server_connect_net(_vnt_cmd_info_t* cmd, void* data)
{
    char hostname[40];
    int rv, n, parsed = 0;
    uint32_t port = _VN_SERVER_TEST_PORT;
    char* msg = NULL;
    uint16_t msglen = 0;

    assert(cmd);
    if (data == NULL) return _VN_ERR_FAIL;
    n = sscanf(data, "%s %u %n", hostname, &port, &parsed);
    if (n < 1) {
        sprintf(hostname, "localhost");
    }
    if (parsed) {
        msg = (uint8_t*) data + parsed;
        msglen = (uint16_t) strlen(msg);
    }
    rv = _VN_sconnect(hostname, port, msg, msglen);
    return rv;
}

int _vnt_cmd_config_port(_vnt_cmd_info_t* cmd, void* data)
{
    _VN_net_t net_id;
    _VN_addr_t addr;
    uint32_t port, state = _VN_PT_UNCHANGED, host_id = _VN_HOST_INVALID;
    int rv, n;
    assert(cmd);
    if (data == NULL) return _VN_ERR_FAIL;
    n = sscanf(data, "0x%x %u %u %u", &net_id , &host_id, &port, &state);
    if (n < 1) {
        n = sscanf(data, "%u %u %u %u", &net_id , &host_id, &port, &state);
    }
    if (n >= 3) {
        if (host_id == _VN_HOST_INVALID) {
            addr = _vn_get_default_localaddr(net_id);
        }
        else {
            addr = _VN_make_addr(net_id, host_id);
        }
        rv = _VN_config_port(addr, port, state);
    }
    else {
        rv = _VN_ERR_FAIL;
    }
    return rv;
}

int _vnt_cmd_set_priority(_vnt_cmd_info_t* cmd, void* data)
{
    _VN_net_t net_id;
    _VN_addr_t addr;
    uint32_t host_id = _VN_HOST_INVALID, port, priority;
    int rv, n;
    assert(cmd);
    if (data == NULL) return _VN_ERR_FAIL;
    n = sscanf(data, "0x%x %u %u %u", &net_id, &host_id, &port, &priority);
    if (n < 1) {
        n = sscanf(data, "%u %u %u %u", &net_id, &host_id, &port, &priority);
    }
    if (n >= 3) {
        if (host_id == _VN_HOST_INVALID) {
            addr = _vn_get_default_localaddr(net_id);
        }
        else {
            addr = _VN_make_addr(net_id, host_id);
        }
        rv = _VN_set_priority(addr, port, priority);
    }
    else {
        rv = _VN_ERR_FAIL;
    }
    return rv;
}

int _vnt_cmd_discover_hosts(_vnt_cmd_info_t* cmd, void* data)
{
    int flag = true;
    int n;
    assert(cmd);
    if (data == NULL) return _VN_ERR_FAIL;
    n = sscanf(data, "%d", &flag);
    return _VN_discover_hosts(flag);
}

int _vnt_cmd_query_host(_vnt_cmd_info_t* cmd, void* data)
{
    _vn_inaddr_t ipaddr;
    char hostname[40];
    uint32_t port = _VN_SERVER_TEST_PORT;
    int rv, n;

    assert(cmd);
    if (data == NULL) return _VN_ERR_FAIL;
    n = sscanf(data, "%s %u", hostname, &port);
    if (n < 1) {
        sprintf(hostname, "localhost");
    }

    ipaddr = _vn_netif_getaddr(hostname);
    if (ipaddr == _VN_INADDR_INVALID) {
        return _VN_ERR_SERVER;
    }

    rv = _VN_query_host(hostname, port);
    return rv;
}

int _vnt_cmd_clear_query_list(_vnt_cmd_info_t* cmd, void* data)
{
    return _VN_clear_query_list();
}

int _vnt_cmd_service_register(_vnt_cmd_info_t* cmd, void* data)
{
    uint32_t service_id, public = 1;
    int rv, n, parsed = 0;

    assert(cmd);
    if (data == NULL) return _VN_ERR_FAIL;
    n = sscanf(data, "%u %u %n", &service_id, &public, &parsed);

    if (n >= 1) {
        char* msg = NULL;
        _VN_msg_len_t msglen = 0;
        if (parsed) {
            msg = (uint8_t*) data + parsed;
            msglen = (_VN_msg_len_t) strlen(msg);
        }
        rv = _VN_register_service(service_id, public, msg, msglen);

        if (rv < 0) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                      "_VN_register_service %u failed due to %d\n",
                      service_id, rv);
        }
    }
    else {
        rv = _VN_ERR_FAIL;
    }
    return rv;
}

int _vnt_cmd_service_unregister(_vnt_cmd_info_t* cmd, void* data)
{
    uint32_t service_id;
    int rv, n;

    assert(cmd);
    if (data == NULL) return _VN_ERR_FAIL;
    n = sscanf(data, "%u", &service_id);

    if (n >= 1) {
        rv = _VN_unregister_service(service_id);

        if (rv < 0) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                      "_VN_unregister_service %u failed due to %d\n",
                      service_id, rv);
        }
    }
    else {
        rv = _VN_ERR_FAIL;
    }
    return rv;
}

int _vnt_cmd_send_msg(_vnt_cmd_info_t* cmd, void* data)
{
    _VN_net_t net_id;
    uint32_t port, from, to;
    int rv, n, parsed = 0;

    assert(cmd);
    if (data == NULL) return _VN_ERR_FAIL;
    n = sscanf(data, "0x%x %u %u %u %n", &net_id, &from, &to, &port, &parsed);
    if (n < 1) {
        n = sscanf(data, "%u %u %u %u %n", &net_id, &from, &to, &port, &parsed);
    }
    if (n >= 4) {
        char* msg = NULL;
        _VN_msg_len_t msglen = 0;
        if (parsed) {
            msg = (uint8_t*) data + parsed;
            msglen = (_VN_msg_len_t) strlen(msg);
        }
        rv = _VN_send(net_id, from, to, port, msg, msglen,
                      NULL, 0, _vnt_send_attr);
        if (rv < 0) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                      "VN_send failed due to %d\n", rv);
        }
    }
    else {
        rv = _VN_ERR_FAIL;
    }
    return rv;
}

int _vnt_cmd_guid_send_msg(_vnt_cmd_info_t* cmd, void* data)
{
    _VN_addr_t addr = _VN_ADDR_INVALID;
    _VN_guid_t guid = _VN_GUID_INVALID;
    _VN_net_t net_id;
    uint32_t port, host_id;
    int rv = _VN_ERR_OK, n, parsed = 0;

    assert(cmd);
    if (data == NULL) return _VN_ERR_FAIL;
    n = sscanf(data, "guid%u %u %n", &guid.chip_id, &port, &parsed);
    if (n < 1) {
        n = sscanf(data, "server %u %n", &port, &parsed);
        if (n >= 1) {
            addr = _vn_get_default_server();
            if (addr == _VN_ADDR_INVALID) {
                _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                          "Cannot find addr for server\n");
                rv = _VN_ERR_NETID;
            }
        }
        else {
            rv = _VN_ERR_FAIL;
        }
    }
    else if (n >= 2) {
        addr = _vn_get_default_client_addr(guid);       
        if (addr == _VN_ADDR_INVALID) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                      "Cannot find default addr for guid %u\n",
                      guid);
            rv = _VN_ERR_NETID;
        }
    }
    else {
        rv = _VN_ERR_FAIL;
    }

    if (rv >= 0) {
        char* msg = NULL;
        _VN_msg_len_t msglen = 0;
        if (parsed) {
            msg = (uint8_t*) data + parsed;
            msglen = (_VN_msg_len_t) strlen(msg);
        }
        net_id = _VN_addr2net(addr);
        host_id = _VN_addr2host(addr);
        rv = _VN_send(net_id, _vn_get_default_localhost(net_id), host_id,
                      port, msg, msglen, NULL, 0, _vnt_send_attr);
        if (rv < 0) {
            _VN_TRACE(TRACE_ERROR, _VN_SG_TEST,
                      "VN_send failed due to %d\n", rv);
        }
    }

    return rv;
}

int _vnt_cmd_set(_vnt_cmd_info_t* cmd, void* data)
{
    char arg1[100];
    uint32_t arg2, arg3;
    int n;
    assert(cmd);
    if (data == NULL) return _VN_ERR_FAIL;
    n = sscanf(data, "%s %u %u", arg1, &arg2, &arg3);
    if (n < 1) return _VN_ERR_FAIL;
    if (strcasecmp(arg1, "Encryption") == 0) {
        if (n >= 2) {
            if (arg2) 
                _vnt_send_attr |=  _VN_MSG_ENCRYPTED;
            else _vnt_send_attr &= ~_VN_MSG_ENCRYPTED;
        }
        _VN_PRINT("Encryption = %d\n",
                 (_vnt_send_attr & _VN_MSG_ENCRYPTED)? 1:0);
    }
    else if (strcasecmp(arg1, "Reliable") == 0) {
        if (n >= 2) {
            if (arg2) 
                _vnt_send_attr |=  _VN_MSG_RELIABLE;
            else _vnt_send_attr &= ~_VN_MSG_RELIABLE;
        }
        _VN_PRINT("Reliable = %d\n", 
                 (_vnt_send_attr & _VN_MSG_RELIABLE)? 1:0);
    }
    else if (strcasecmp(arg1, "Trace") == 0) {
        if (n >= 3) {
            _vn_set_sg_trace_level(_TRACE_VN, arg3, arg2);
        }
        else if (n >= 2) {
            _vn_set_trace_level(arg2);
        }
        _VN_PRINT("Trace level = %d (%s)\n", _vn_trace_level, 
                 _vn_tracelevel(_vn_trace_level));
    }
    else if (strcasecmp(arg1, "Testnet") == 0) {
        if (n >= 2) {
            _vnt_test_net_all = arg2;
        }
        _VN_PRINT("TESTNET = 0x%08x\n", _vnt_test_net_all);
    }
    else if (strcasecmp(arg1, "Accept") == 0) {
        if (n >= 2) {
            _vnt_accept_connect = arg2;
            _vnt_accept_net = arg2;
        }
        _VN_PRINT("Accept Connect = %d\n", _vnt_accept_connect);
        _VN_PRINT("Accept Net = %d\n", _vnt_accept_net);
    }
    else if (strcasecmp(arg1, "Timeout") == 0) {
        if (n >= 2) {
            /* TODO: Separately set timeouts */
            _vn_hs_set_timeout(arg2);
            _vn_inactivity_set_timeout(arg2);
            _vn_retx_set_timeout(arg2);
            _vn_recv_set_timeout(arg2);
#ifdef _VN_RPC_DEVICE
            _vn_netif_rpc_set_timeouts(arg2, arg2);
#else
            _vn_firewall_set_timeout(arg2);
#endif
        }
        _VN_PRINT("Handshake Timeout = %d\n",  _vn_hs_get_timeout());
        _VN_PRINT("Inactivity Timeout = %d\n",  _vn_inactivity_get_timeout());
        _VN_PRINT("Retransmission Timeout = %d\n",  _vn_retx_get_timeout());
        _VN_PRINT("Recv Timeout = %d\n",  _vn_recv_get_timeout());
#ifdef _VN_RPC_DEVICE
        _VN_PRINT("RPC Timeout = %d, %d\n",  
                  _vn_netif_rpc_get_min_timeout(),
                  _vn_netif_rpc_get_max_timeout());
#else
        _VN_PRINT("Firewall Timeout = %d\n",  _vn_firewall_get_timeout());
#endif
    }
    else {
        _VN_PRINT("Unknown variable '%s'\n", arg1);
        return _VN_ERR_FAIL;
    }
    return _VN_ERR_OK;
}

int _vnt_cmd_get_time(_vnt_cmd_info_t* cmd, void* data)
{
#ifdef _SC
    _VN_PRINT("Time %llu\n", _vn_get_timestamp());
#else
    char buf[100];
    time_t t = time(NULL);
    char* timebuf = ctime_r(&t, buf); 

#ifdef _WIN32
    _VN_PRINT("Time %I64u : %s", _vn_get_timestamp(), timebuf);
#else
    _VN_PRINT("Time %llu : %s", _vn_get_timestamp(), timebuf);
#endif

#endif
    _VN_PRINT("Current timer slot %u\n", _vn_timer_get_current());

    return _VN_ERR_OK;
}

int _vnt_cmd_get_status(_vnt_cmd_info_t* cmd, void* data)
{
    uint32_t status;
    status = _VN_get_status();
    _VN_PRINT("VN status 0x%0x\n", status);
    _VN_PRINT("  Initialized? %d\n", (status & _VN_ST_INITIALIZED)? 1:0);
    _VN_PRINT("  USB? %d\n", (status & _VN_ST_USB_CONNECTED)? 1:0);
    _VN_PRINT("  Proxy? %d\n", (status & _VN_ST_PROXY_CONNECTED)? 1:0);
    _VN_PRINT("  AdHoc? %d\n", (status & _VN_ST_ADHOC_SUPPORTED)? 1:0);
    _VN_PRINT("  Gateway? %d\n", (status & _VN_ST_GATEWAY_DEFINED)? 1:0);
    return _VN_ERR_OK;
}

int _vnt_cmd_whoami(_vnt_cmd_info_t* cmd, void* data)
{
    _VN_guid_t guid = _vn_get_myguid();
    char localhostname[255];
    char localaddr[_VN_INET_ADDRSTRLEN];
    _vn_inaddr_t ipaddr;
    _vn_inport_t port;
    ipaddr = _vn_netif_getlocalhost(_vn_netif_get_instance(),
                                    localhostname, sizeof(localhostname));
    port = _vn_netif_getlocalport(_vn_netif_get_instance());
    _vn_inet_ntop(&(ipaddr), localaddr, sizeof(localaddr));

    _VN_PRINT("My guid 0x%08x-%08x (%u)\n", guid.device_type, 
              guid.chip_id, guid.chip_id);

    _VN_PRINT("VN listener at %s(%s):%u\n", localhostname, localaddr, port);

#ifdef UPNP
    _VN_PRINT("UPNP listener at %s:%u\n",
              _vn_upnp_get_server_host(), _vn_upnp_get_server_port());
#endif

    return _VN_ERR_OK;
}


int _vnt_cmd_cancel_get_events(_vnt_cmd_info_t* cmd, void* data)
{
    _VN_cancel_get_events();
    return _VN_ERR_OK;
}

int _vnt_cmd_clk_calibrate(_vnt_cmd_info_t* cmd, void* data)
{
    _VN_net_t net_id;
    uint32_t host_id;
    int n, rv;

    assert(cmd);
    if (data == NULL) return _VN_ERR_FAIL;
    n = sscanf(data, "0x%x %u", &net_id, &host_id);
    if (n < 1) {
        n = sscanf(data, "%u %u", &net_id, &host_id);
    }
    if (n >= 2) {
        rv = _VN_calibrate_clock(_VN_make_addr(net_id, host_id));
    }
    else {
        rv = _VN_ERR_FAIL;
    }
    return rv;
}

int _vnt_cmd_test_ping(_vnt_cmd_info_t* cmd, void* data)
{
    int count = 1, nothers = 1, bufsize = 128, n, rv;
    if (data == NULL) return _VN_ERR_FAIL;
    n = sscanf(data, "%u %u %u", &count, &bufsize, &nothers);
    rv = _vnt_test_ping(count, bufsize, nothers);
    return rv;
}

int _vnt_cmd_exit(_vnt_cmd_info_t* cmd, void* data)
{
    int leave_nets = 1;
    assert(cmd);
    if (data != NULL) {
        sscanf( data, "%d", &leave_nets);
    }
    if (leave_nets) {
        _vn_leave_all();
    }
    _vnt_exit = 1;
    return _VN_ERR_OK;
}

int _vnt_proc_command(char *cmdline)
{
    char cmdstr[100];
    char* cmdargs = NULL;
    int validargs;
    int numofcmds = sizeof( _vnt_cmdlist ) / sizeof( _vnt_cmd_info_t );
    int rv, parsed = 0, i;
    _vnt_cmd_info_t* cmd = NULL;
    memset(cmdstr, 0, sizeof(cmdstr));
    validargs = sscanf( cmdline, "%s %n", cmdstr, &parsed);

    /* Ignore blank lines and comments */
    if (validargs < 1) return _VN_ERR_FAIL;
    if (cmdstr[0] == '#') return _VN_ERR_FAIL;

    for( i = 0; i < numofcmds; i++ ) {
        if( strcasecmp( cmdstr, _vnt_cmdlist[i].name ) == 0 ) {
            cmd = &(_vnt_cmdlist[i]);
            break;
        }
    }

    if (cmd == NULL) {
        _VN_PRINT("Unknown command '%s', see 'Help'\n", cmdstr);
        return _VN_ERR_FAIL;
    }

    if (parsed) {
        cmdargs = cmdline + parsed;
    }

    if (cmd->cmdfunc) {
        rv = (cmd->cmdfunc)(cmd, cmdargs);
        _VN_PRINT("%s returned with %d\n", cmd->name, rv);
    }
    else {
        rv = _vnt_cmd_notsupported(cmd, cmdargs);
    }

    return rv;
}
    

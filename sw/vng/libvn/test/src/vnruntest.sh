#!/bin/bash

testname=$1
shift
command="$*"
module="VN"

starttime=${SECONDS}

$command
rv=$?

endtime=${SECONDS}
((duration=endtime-starttime))

if [[ $rv -ne 0 ]] ; then
    status="FAILED"
else
    status="PASSED"
fi


echo "*** $module $testname TEST $status"
echo "=== $module $testname TIME Elapsed $duration"

exit $fail

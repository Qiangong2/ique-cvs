//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#ifndef __VN_TEST_H__
#define __VN_TEST_H__

#include "shr_getopt.h"

#ifdef _SC
extern int atoi(const char *__nptr);
extern int sprintf(char *, const char *, ...);
extern int sscanf(const char *, const char *, ...);
extern int strcasecmp(const char *, const char *);
extern int strncasecmp(const char *, const char *, size_t);

extern char* gets(char *s);
#endif

/* Test server port */
#define _VN_SERVER_TEST_PORT   20000

/* Test net of all peers */
#define _VN_NET_TEST_ALL       0x01

/* Test port */
#define _VN_PORT_TEST          0xff

#define MAXLINE     1024
#define MAXNAMELEN    80
#define MAXPEERS     256
#define MAXNETS       16
#define MAXPORTS      16
#define MAXHEADERS     8

#define MAXSTATS    1024

#define _VN_SG_TEST     _TRACE_TEST, 0

typedef struct {
    _VN_net_t  net_id;
    _VN_host_t host_id;
    _VN_port_t port;
    uint32_t pkts;
    uint32_t bytes;
    uint32_t reliable;
    uint32_t lossy;
    uint32_t dropped;
} _vnt_port_stats_t;

extern bool _vnt_verbose;
extern _vnt_port_stats_t* _vnt_all_sent_stats[MAXSTATS];
extern _vnt_port_stats_t* _vnt_all_recv_stats[MAXSTATS];

void _vnt_init_stats();
void _vnt_clear_stats();
_vnt_port_stats_t* _vnt_get_recv_stats(_VN_net_t net_id, _VN_host_t host_id,
                                       _VN_port_t port);
_vnt_port_stats_t* _vnt_get_sent_stats(_VN_net_t net_id, _VN_host_t host_id,
                                       _VN_port_t port);
void _vnt_dump_stats(FILE* fp);

typedef struct {
    bool     valid;
    uint32_t ip;
    uint16_t port;
    int64_t  clock_delta;
    _VN_net_t default_net;
    int      nets[MAXNETS];
    int      nnets;
} _vnt_peer_info_t;

typedef struct {
    _VN_host_t hostid;
    int peer;
    uint8_t active_ports[MAXPEERS];
    uint8_t nports;
} _vnt_net_peer_info_t;

typedef struct {
    _VN_net_t net_id;
    _VN_host_t myhost;
    _VN_host_t master;
    int npeers;
    _vnt_net_peer_info_t* mypeer;
    _vnt_net_peer_info_t peers[MAXPEERS];
    _vn_key_t key;
} _vnt_net_info_t;

#define SERVER_PEER 0

extern _vnt_net_info_t _vnt_all_nets[MAXNETS];
extern _vnt_peer_info_t _vnt_all_peers[MAXPEERS];
extern int _vnt_nnets;
extern int _vnt_npeers;
extern int _vnt_mypeerno;

extern _VN_net_t _vnt_test_net_all;
extern _VN_callid_t _vnt_test_net_all_call;

extern int _vnt_exit;
extern int _vnt_accept_connect;
extern int _vnt_accept_net;

extern uint32_t _vnt_event_count[_VN_EVT_MAX+1];
extern int _vnt_send_return;
extern int _vnt_forward_events;
extern int _vnt_teststart;
extern int _vnt_testdone;

int _vn_init_client(const char* vn_server, _vn_inport_t server_port, 
                    _vn_inport_t client_port);

int _vn_config(FILE* fp, const char* me);
_VN_net_t _vnt_get_default_netid();

int _vnt_proc_command(char *cmdline);

void _vnt_send_message(_VN_net_t net_id, _VN_host_t from,  _VN_host_t to, 
                       _VN_port_t port, _VN_tag_t tag, 
                       const void* msg, size_t len, int attr, bool keep_stats);
void _vnt_proc_event(_VN_event_t* event, bool verbose, bool keep_stats);
_vn_wbuf_t* _vn_conv_msg_buffer(const _vn_buf_t *pkt);

/* Functions for checking events */
#define _VN_RV_CALLID 1

bool _vnt_check_return_code(int lineno, int exp, int rv);
bool _vnt_check_event(int lineno, _VN_event_t* event,
                      _VN_tag_t tag, _VN_callid_t call_id);
bool _vnt_check_new_message_event(int lineno, _VN_event_t* event,
                                  _VN_callid_t call_id,
                                  _VN_net_t net_id, 
                                  _VN_host_t from, _VN_host_t to,
                                  _VN_port_t port, 
                                  uint8_t* opthdr, uint8_t opthdr_size);
bool _vnt_check_new_network_event(int lineno, _VN_event_t* event,
                                  _VN_callid_t call_id, 
                                  uint32_t netmask, bool isLocal);
bool _vnt_check_new_connection_event(int lineno, _VN_event_t* event,
                                     _VN_callid_t call_id, 
                                     uint32_t netmask);
bool _vnt_check_net_config_event(int lineno, _VN_event_t* event,
                                 _VN_callid_t callid, _VN_net_t netid, 
                                 uint16_t n_joined, uint16_t n_left,
                                 _VN_host_t peers[], uint8_t reason);
bool _vnt_check_net_disconnected_event(int lineno, _VN_event_t* event,
                                       _VN_callid_t call_id, _VN_net_t netid);
bool _vnt_check_new_net_request_event(int lineno, _VN_event_t* event,
                                      _VN_callid_t call_id,
                                      uint64_t request_id, uint32_t netmask,
                                      _VN_addr_t server_addr, _VN_host_t from_host,
                                      const void* msg, uint16_t msglen);
bool _vnt_check_connection_request_event(int lineno, _VN_event_t* event,
                                         _VN_callid_t call_id,
                                         uint64_t request_id, _VN_addr_t owner,
                                         const void* msg, uint16_t msglen);
bool _vnt_check_connection_accepted_event(int lineno, _VN_event_t* event,
                                          _VN_callid_t callid, _VN_addr_t owner);
bool _vnt_check_device_update_event(int lineno, _VN_event_t* event,
                                    _VN_callid_t call_id, 
                                    _VN_guid_t guid, uint8_t reason,
                                    uint16_t n_added, uint16_t n_removed,
                                    uint32_t services[]);

bool _vnt_check_err_event(int lineno, _VN_event_t* event, 
                          _VN_callid_t call_id, int rv,
                          const void* msg, uint16_t msglen);

bool _vnt_wait_err_event(int lineno, int call_id, int rv, int timeout);

_VN_event_t* _vnt_get_next_event(_VN_event_t* cur_event, int* n,
                                 uint8_t* buf, size_t bufsize, int timeout);
int  _vnt_drain_events(uint8_t* buf, size_t bufsize, int timeout);

bool _vnt_check_msg_return_code(int lineno, _VN_event_t* event,
                                _VN_addr_t from, _VN_port_t port,
                                int exp, int* res);
bool _vnt_check_msg_event(int lineno, _VN_event_t* event, _VN_addr_t from,
                          _VN_port_t port, _VN_guid_t guid, 
                          uint8_t* resbuf, size_t resbuf_size, 
                          _VN_guid_t* pGuid);

bool _vnt_check_remote_call_return_code(_VN_guid_t guid, 
                                        _VN_addr_t from_addr, _VN_port_t port,
                                        int timeout, int exp, int* res);

bool _vnt_check_pending_call_failed(_VN_guid_t guid, 
                                    _VN_addr_t from_addr, _VN_port_t port,
                                    int timeout, int err_code);

bool _vnt_check_connect_rejected(_VN_guid_t guid, _VN_addr_t from_addr,
                                 _VN_port_t port, int timeout, 
                                 _VN_callid_t listen_call,
                                 _VN_guid_t owner_guid, 
                                 _VN_addr_t from_owner_addr,
                                 _VN_addr_t owner_addr,
                                 char* msg);

bool _vnt_check_connect_succeeded(_VN_guid_t guid, _VN_addr_t from_addr,
                                  _VN_port_t port, int timeout, 
                                  _VN_net_t net,
                                  _VN_callid_t listen_call,
                                  _VN_guid_t owner_guid,
                                  _VN_addr_t from_owner_addr,
                                  _VN_addr_t owner_addr,
                                  int nlocal, int npeers, _VN_host_t peers[],
                                  _VN_addr_t* test_addr, _VN_host_t* test_host);
bool _vnt_check_connect_default_rejected(_VN_guid_t guid, _VN_addr_t from_addr,
                                         _VN_port_t port, int timeout, 
                                         _VN_callid_t listen_call,
                                         _VN_guid_t owner_guid, 
                                         _VN_addr_t from_owner_addr,
                                         char* msg);

bool _vnt_check_connect_default_succeeded(
    _VN_guid_t guid, _VN_addr_t from_addr,
    _VN_port_t port, int timeout, 
    _VN_callid_t listen_call,
    _VN_guid_t owner_guid, _VN_addr_t from_owner_addr,
    _VN_addr_t* owner_addr, _VN_host_t* owner_host,
    _VN_addr_t* peer_addr, _VN_host_t* peer_host);

bool _vnt_check_create_succeeded(_VN_guid_t guid, _VN_addr_t from_addr,
                                 _VN_port_t port, int timeout, 
                                 uint32_t netmask, bool isLocal,
                                 _VN_addr_t* test_addr, _VN_host_t* test_host);

bool _vn_test_get_guids(int exp, int lineno, uint8_t mask,
                        _VN_guid_t *exp_guids,
                        _VN_guid_t *guids, int nguids);
bool _vn_test_get_service_ids(int exp, int lineno, _VN_guid_t guid,
                              uint32_t *exp_services,
                              uint32_t *services, int nservices);
bool _vn_test_register_service(int exp, int lineno,
                               uint32_t service_id, bool public,
                               const void* buf, uint16_t buflen);
bool _vn_test_unregister_service(int exp, int lineno,
                                 uint32_t service_id);

bool _vnt_test_command(char* cmd, int exp, int* res);
bool _vnt_test_create_command_succeeded(int timeout, 
                                        uint32_t netmask, bool isLocal,
                                        _VN_addr_t* test_addr, 
                                        _VN_host_t* test_host);
bool _vnt_test_connect_command_succeeded(_VN_guid_t owner_guid,
                                         _VN_guid_t joiner_guid,
                                         _VN_net_t net, 
                                         _VN_callid_t listen_call,
                                         _VN_addr_t owner_addr,
                                         int nlocal, 
                                         int npeers, _VN_host_t peers[],
                                         _VN_addr_t* test_addr, 
                                         _VN_host_t* test_host,
                                         int timeout);
int _vnt_test_ping(int count, int bufsize, int nothers);

#endif /* __VN_TEST_H__ */
 

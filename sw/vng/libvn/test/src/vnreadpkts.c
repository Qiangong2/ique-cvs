//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnlocal.h"
#include "vntest.h"

#ifdef _VN_NO_TRACE
#define _vn_dbg_read_pkt_fp(fp, t)   0
#else
extern _vn_buf_t* _vn_dbg_read_pkt_fp(FILE* fp, _VN_time_t* t);
#endif

void usage()
{
    printf("Usage: vnreadpkts [-f format] [-m filter] [-t] infile outfile\n");
    printf("-t         Display timestamp\n");
    printf("-f format  What format to output the packet\n");
    printf("           format = 1 Orginal binary format\n");
    printf("                  = 2 Ascii dump of packet (default)\n");
    printf("                  = 3 Parsed dump of packet\n");
    printf("-m filter  Only outputs packets matching the filter condition\n");
}


#define _VN_MATCH_NET       0x01
#define _VN_MATCH_FROM      0x02
#define _VN_MATCH_TO        0x04
#define _VN_MATCH_PORT      0x08
#define _VN_MATCH_OPT       0x10

typedef struct {
    uint32_t  match_mask;
    uint32_t  neg_match_mask; /* Not used */
    _VN_net_t  net;
    _VN_host_t from;
    _VN_host_t to;
    _VN_port_t port;
    uint8_t    opthdr_size;
    uint8_t    opthdr[_VN_MAX_HDR_LEN];
} _vn_filter_cond_t;

#define _VN_MAX_MATCHES 16
static _vn_filter_cond_t _vn_filters[_VN_MAX_MATCHES];
static int _vn_nfilters = 0;

bool _vn_match_pkt(_vn_buf_t* pkt, _vn_filter_cond_t* pCond)
{
    int rv;
    _vn_msg_t msg;
    assert(pkt);
    assert(pCond);
    if (pCond->match_mask == 0) return true; /* No condition specified */
    rv = _vn_parse_msg(&msg, pkt->buf, pkt->len);
    if (pCond->match_mask & _VN_MATCH_NET) {
        if (msg.net_id != pCond->net) 
            return false;
    }

    if (pCond->match_mask & _VN_MATCH_FROM) {
        if (msg.from != pCond->from) 
            return false;
    }

    if (pCond->match_mask & _VN_MATCH_TO) {
        if (msg.to != pCond->to) 
            return false;
    }

    if (pCond->match_mask & _VN_MATCH_PORT) {
        if (msg.port != pCond->port) 
            return false;
    }

    if (pCond->match_mask & _VN_MATCH_OPT) {
        if ((msg.opthdr_size != pCond->opthdr_size) ||
            (memcmp(msg.opthdr, pCond->opthdr, pCond->opthdr_size) != 0)) {
            return false;
        }
    }

    return true;
}

int main(int argc, char** argv)
{
    const char* input_file = NULL;
    const char* output_file = NULL;
    _vn_buf_t* pkt;
    FILE* ifp, *ofp;
    int rv, format = 2, val;
    char* pch;
    _vn_filter_cond_t* pCond;
    _VN_time_t t;
    bool keep_timestamp = false;
    memset(_vn_filters, 0, sizeof(_vn_filters));
    while ((rv = getopt(argc, argv, "f:m:tT")) != -1) {
        switch (rv) {
        case 't':
            keep_timestamp = true;
            break;
        case 'f':
            format = atoi(optarg);
            if ((format < 1) || (format > 3)) {
                fprintf(stderr, "format has to be from 1 to 3\n");
                exit(1);
            }
            break;
        case 'm':
            if (_vn_nfilters >= _VN_MAX_MATCHES) {
                fprintf(stderr, "Too many match conditions specified, only "
                        "%d allowed\n", _VN_MAX_MATCHES);
                exit(1);
            }
            pCond = &_vn_filters[_vn_nfilters];
            pch = strtok(optarg, ",");
            while (pch != NULL)
            {
                if (sscanf(pch, " net = %d", &val) == 1) {
                    if (pCond->match_mask & _VN_MATCH_NET) {
                        fprintf(stderr, "net already specified for match\n");
                        exit(1);
                    }
                    pCond->match_mask |= _VN_MATCH_NET;
                    pCond->net = val;
                }
                else if (sscanf(pch, " from = %d", &val) == 1) {
                    if (pCond->match_mask & _VN_MATCH_FROM) {
                        fprintf(stderr, "from already specified for match\n");
                        exit(1);
                    }
                    pCond->match_mask |= _VN_MATCH_FROM;
                    pCond->from = val;
                }
                else if (sscanf(pch, " to = %d", &val) == 1) {
                    if (pCond->match_mask & _VN_MATCH_TO) {
                        fprintf(stderr, "to already specified for match\n");
                        exit(1);
                    }
                    pCond->match_mask |= _VN_MATCH_TO;
                    pCond->to = val;
                }
                else if (sscanf(pch, " port = %d", &val) == 1) {
                    if (pCond->match_mask & _VN_MATCH_PORT) {
                        fprintf(stderr, "port already specified for match\n");
                        exit(1);
                    }
                    pCond->match_mask |= _VN_MATCH_PORT;
                    pCond->port = val;
                }
                /* TODO: have opt be arbitrary hex string of 16 bytes */
                else if (sscanf(pch, " opt = %d", &val) == 1) {
                    if (pCond->match_mask & _VN_MATCH_OPT) {
                        fprintf(stderr, "opt already specified for match\n");
                        exit(1);
                    }
                    pCond->match_mask |= _VN_MATCH_OPT;
                    pCond->opthdr_size = 2;
                    (* ((int16_t*) pCond->opthdr)) = val;
                } 
                else {
                    fprintf(stderr, "Invalid match condition %s\n", pch);
                    exit(1);
                }
                pch = strtok(NULL, ",");
            }
            _vn_nfilters++;
            break;
        default:
            usage();
            exit(1);
        }
    }
            
    if (optind < argc) {
        input_file = argv[optind];
        if (optind + 1 < argc) {
            output_file = argv[optind+1];
        }
    }
    else {
        usage();
        exit(1);
    }
 
    ifp = fopen(input_file, "rb"); 
    if (ifp == NULL) {
        fprintf(stderr, "Unable to open input file %s\n", input_file);
        exit(1);
    }

    if (output_file) {
        ofp = fopen(output_file, "wb");
        if (ofp == NULL) {
            fprintf(stderr, "Unable to open output file %s\n", output_file);
            exit(1);
        }
    }
    else { ofp = stdout; }
    
    while ( (pkt = _vn_dbg_read_pkt_fp(ifp, &t)) ) {
        bool match = true;
        int i;
        for (i = 0; match && (i < _vn_nfilters); i++) {
            match = _vn_match_pkt(pkt, &_vn_filters[i]);
        }
        if (match) {
            switch (format) {
            case 1:
                if (!keep_timestamp) t = 0; /* Disregard timestamp */
                _vn_dbg_write_timed_buf(ofp, t, pkt->buf, pkt->len);
                break;
            case 2:
                if (keep_timestamp) fprintf(ofp, "%llu: ", t);
                _vn_dbg_dump_buf(ofp, pkt->buf, pkt->len);
                fprintf(ofp, "\n");
                break;
            case 3:
                if (keep_timestamp) fprintf(ofp, "%llu: ", t);
                _vn_dbg_print_pkt(ofp, pkt->buf, pkt->len);
                break;
            }
        }
        _vn_free_msg_buffer(pkt);        
    }

    fclose(ifp);
    if (output_file) fclose(ofp);
    return 0;
}
    

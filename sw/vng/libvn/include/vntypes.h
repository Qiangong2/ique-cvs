//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#ifndef __VN_TYPES_H__
#define __VN_TYPES_H__

#ifdef __cplusplus
extern "C" {
#endif

#ifdef _LINUX
#include <stdint.h>
#else
typedef signed char             int8_t;
typedef short int               int16_t;
typedef int                     int32_t;
typedef long long int           int64_t;

typedef unsigned char           uint8_t;
typedef unsigned short int      uint16_t;
typedef unsigned int            uint32_t;
typedef unsigned long long int  uint64_t;
#endif

#ifndef __cplusplus
typedef unsigned char bool;
typedef unsigned char uchar_t;

#define false 0
#define true  1
#endif

/* Type definitions */
typedef uint32_t _VN_net_t;    

/* Net constants */
#define _VN_NET_DEFAULT         0
#define _VN_NET_ANY             0x4

/* For each net, the upper 2 bit indicates the net class */
#define _VN_NET_MASK1           0xfffffffc
#define _VN_NET_MASK2           0xfffffff0
#define _VN_NET_MASK3           0xffffffc0
#define _VN_NET_MASK4           0xffff0000

#define _VN_NET_MAX_RESERVED      0x03fc

/* Class 1 net: 2^2 (4) hosts, 2^28 (256M) nets, 256 local */
#define _VN_NET_MIN_LOCAL1        0x0400
#define _VN_NET_MAX_LOCAL1        0x07fc
#define _VN_NET_MIN_GLOBAL1       0x0800
#define _VN_NET_MAX_GLOBAL1       0x3ffffffc

/* Class 2 net: 2^4 (16) hosts, 2^26 (64M) nets, 128 local */
#define _VN_NET_MIN_LOCAL2        0x40000000
#define _VN_NET_MAX_LOCAL2        0x400007f0
#define _VN_NET_MIN_GLOBAL2       0x40000800
#define _VN_NET_MAX_GLOBAL2       0x7ffffff0

/* Class 3 net: 2^6 (64) hosts, 2^24 (16M) nets, 64 local */
#define _VN_NET_MIN_LOCAL3        0x80000000
#define _VN_NET_MAX_LOCAL3        0x80000fc0
#define _VN_NET_MIN_GLOBAL3       0x80001000
#define _VN_NET_MAX_GLOBAL3       0xbfffffc0

/* Class 4 net: 2^16 - 16 (65520) hosts, 2^14 (16K) nets, 32 local */
#define _VN_NET_MIN_LOCAL4        0xc0000000
#define _VN_NET_MAX_LOCAL4        0xc01f0000
#define _VN_NET_MIN_GLOBAL4       0xc0200000
#define _VN_NET_MAX_GLOBAL4       0xffff0000

/* Network Macros */
#define _VN_NETID_IS_RESERVED(net_id)     \
  ((net_id) < _VN_NET_MAX_RESERVED)

#define _VN_NETID_IS_LOCAL(net_id)        \
 ((((net_id) >= _VN_NET_MIN_LOCAL1) && ((net_id) <= _VN_NET_MAX_LOCAL1)) ||  \
  (((net_id) >= _VN_NET_MIN_LOCAL2) && ((net_id) <= _VN_NET_MAX_LOCAL2)) ||  \
  (((net_id) >= _VN_NET_MIN_LOCAL3) && ((net_id) <= _VN_NET_MAX_LOCAL3)) ||  \
  (((net_id) >= _VN_NET_MIN_LOCAL4) && ((net_id) <= _VN_NET_MAX_LOCAL4)))

#define _VN_NETID_IS_GLOBAL(net_id)       \
 ((((net_id) >= _VN_NET_MIN_GLOBAL1) && ((net_id) <= _VN_NET_MAX_GLOBAL1)) || \
  (((net_id) >= _VN_NET_MIN_GLOBAL2) && ((net_id) <= _VN_NET_MAX_GLOBAL2)) || \
  (((net_id) >= _VN_NET_MIN_GLOBAL3) && ((net_id) <= _VN_NET_MAX_GLOBAL3)) || \
  (((net_id) >= _VN_NET_MIN_GLOBAL4) && ((net_id) <= _VN_NET_MAX_GLOBAL4)))

typedef uint16_t _VN_host_t;
#define _VN_HOST_MAX1           0x0003
#define _VN_HOST_MAX2           0x000f
#define _VN_HOST_MAX3           0x003f
#define _VN_HOST_MAX4           0xffef
#define _VN_HOST_MAX            _VN_HOST_MAX4

#define _VN_HOST_ANY            0xfffc
#define _VN_HOST_OTHERS         0xfffd
#define _VN_HOST_SELF           0xfffe
#define _VN_HOST_INVALID        0xffff

#define _VN_HOST_SERVER         0x0000
#define _VN_HOST_CLIENT         0x0001

typedef uint32_t _VN_addr_t;
#define _VN_ADDR_INVALID        (-1)

typedef uint8_t _VN_port_t;

#define _VN_PORT_NET_CTRL       0
#define _VN_PORT_MAX_RESERVED   0xf

/* Port Macros */
/* System port - 0x00..0x0f */
#define _VN_PORT_IS_SYSTEM(port)       (port < _VN_PORT_MAX_RESERVED)


#define _VN_PT_INACTIVE         0        /* port states */
#define _VN_PT_ACTIVE           1
#define _VN_PT_UNCHANGED        2

typedef uint64_t _VN_time_t;
#define _VN_TIMESTAMP_INVALID   (-1)
#define _VN_TIMEOUT_NONE        (-1)    /* wait indefinitely with no timeout */

typedef uint16_t _VN_msg_len_t;
#define _VN_MAX_MSG_LEN         16000   /* max payload data size */
#define _VN_MAX_HDR_LEN         16

#define _VN_MSG_RELIABLE        0x1     /* specify reliable transmission */
#define _VN_MSG_ENCRYPTED       0x2     /* specify encryption */

typedef uint16_t _VN_tag_t;
typedef int32_t  _VN_callid_t;

/* Device definitions */
typedef uint32_t _VN_device_type_t;
typedef uint32_t _VN_chip_id_t;

typedef struct
{
    _VN_device_type_t  device_type;  /* Device Type */
    _VN_chip_id_t      chip_id;      /* Device Chip ID */
} _VN_guid_t;

#define _VN_DEVICE_TYPE_UNKNOWN   (-1)
#define _VN_CHIP_ID_INVALID     (-1)

#define _VN_GUID_INVALID        { _VN_DEVICE_TYPE_UNKNOWN, _VN_CHIP_ID_INVALID }

/* Real device types */
#define _VN_DEVICE_BB            0x00     /* No VN support */
#define _VN_DEVICE_RV            0x01     /* No VN support, maybe future */
#define _VN_DEVICE_NC            0x02

/* Virtual device types */
#define _VN_DEVICE_VS            0x80     /* VN Server */
#define _VN_DEVICE_PC            0x81     /* PC clients */

/* Mask for looking up devices */
#define _VN_DEVICE_PROP_LOCAL       0x01  /* device is this local device */
#define _VN_DEVICE_PROP_LAN         0x02  /* device is on the same LAN */
#define _VN_DEVICE_PROP_QUERIED     0x04  /* device discovered on ad-hoc net
                                             using query */
#define _VN_DEVICE_PROP_ADHOC       0x08  /* device discovered on ad-hoc net
                                             using host discovery or query */
#define _VN_DEVICE_PROP_SAME_PROXY  0x10  /* device shares the same proxy as
                                             the local device */

/* Host Status */

#define _VN_HOST_ST_UNKNOWN      0   
#define _VN_HOST_ST_REACHABLE    1
#define _VN_HOST_ST_UNREACHABLE  2 

typedef struct {
    uint32_t conn_state;  /* Connection state */
                          /* Possible values are:
                           * _VN_HOST_ST_REACHABLE 
                           * - Have received messages from host
                           * _VN_HOST_ST_UNREACHABLE 
                           * - Attempt to send reliable message to host 
                           *   has timed out
                           * _VN_HOST_ST_UNKNOWN 
                           * - Connection state is unknown. 
                           */

    uint32_t rtt;         /* Round trip time to this host in ms 
                           * _VN_TIMESTAMP_INVALID is used for an unknown rtt
                           */

} _VN_host_status;

/* VN status */
#define _VN_ST_INITIALIZED                    0x01
#define _VN_ST_USB_CONNECTED                  0x02
#define _VN_ST_PROXY_CONNECTED                0x04
#define _VN_ST_ADHOC_SUPPORTED                0x08
#define _VN_ST_GATEWAY_DEFINED                0x10
#define _VN_ST_PROXY_RUNNING                  0x20

/* Define minimum and maximum VN timeouts for use in _VN_config_t */
#define _VN_MAX_CONFIG_TIMEOUT          10*60*1000
#define _VN_MIN_CONFIG_TIMEOUT                1000

/* VN configuration parameters (to be passed in to _VN_init */
typedef struct {
    /* UDP port range for VN to bind to */
    /* Use port of 0 to use the default port */
    uint16_t vn_port_min;
    uint16_t vn_port_max;

#ifdef UPNP
    /* If UPNP is supported, starting TCP and UDP port to use for UPNP */
    /* Ignored if UPNP is not supported */
    uint16_t upnp_port;
#endif

    /* Timeouts (in ms) to set, use a timeout of 0 to use the default values */
    /* Timeouts need to be between 1000 ms and 10 minutes */
    uint32_t connect_timeout; /* Timeout for handshake (connecting to a VN) */
    uint32_t peer_timeout;    /* Timeout before a inactive peer is 
                                 disconnected by the owner */
    uint32_t retx_timeout;    /* Timeout after which the retransmission
                                 of a reliable message is aborted */
} _VN_config_t;

#ifdef  __cplusplus
}
#endif
 
#endif /* __VN_TYPES_H__ */

//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#ifndef __VN_H__
#define __VN_H__

#include "vntypes.h"
#include "vnevents.h"

#ifdef __cplusplus
extern "C" {
#endif

#define _VN_VERSION             0
#define _VN_SERVER_PORT         16978
#define _VN_CLIENT_PORT         0

#define _VN_PRIORITY_HIGHEST    7
#define _VN_PRIORITY_LOWEST     0
#define _VN_PRIORITY_DEFAULT    _VN_PRIORITY_LOWEST

/* Error codes */
#define _VN_ERR_OK             0      /* No error, everything good */
#define _VN_ERR_FAIL          -1      /* Generic error */
#define _VN_ERR_NOMEM         -2      /* No more memory */
#define _VN_ERR_NOTFOUND      -3
#define _VN_ERR_NETID         -4      /* Bad net id */
#define _VN_ERR_HOSTID        -5      /* Bad host id */
#define _VN_ERR_TIME          -6      /* Invalid timestamp */
#define _VN_ERR_LENGTH        -7   
#define _VN_ERR_INVALID       -8
#define _VN_ERR_PORT          -9
#define _VN_ERR_PORT_STATE    -10
#define _VN_ERR_TOO_SHORT     -11
#define _VN_ERR_BAD_VERSION   -12
#define _VN_ERR_NOSUPPORT     -13
#define _VN_ERR_OUTOFSEQ      -14
#define _VN_ERR_SERVER        -15     /* Unknown server */
#define _VN_ERR_TIMEOUT       -16     /* Operation has timed out */
#define _VN_ERR_PENDING       -17     /* Operation is still pending */
#define _VN_ERR_CANCELED      -18     /* Operation was canceled */
#define _VN_ERR_HANDLE        -19
#define _VN_ERR_DUPPKT        -20
#define _VN_ERR_DUPENTRY      -21  
#define _VN_ERR_PRIORITY      -22     /* Invalid priority for VN port */
#define _VN_ERR_AUTH          -23     /* Authentication error */
#define _VN_ERR_FULL          -24
#define _VN_ERR_EMPTYPKT      -25
#define _VN_ERR_INACTIVE      -26
#define _VN_ERR_GSEQ          -27
#define _VN_ERR_SOCKET        -28     /* Socket error */
#define _VN_ERR_NOSPACE       -29
#define _VN_ERR_NOT_OWNER     -30     /* Only VN owner allowed to do this */
#define _VN_ERR_CONFIG        -31
#define _VN_ERR_BLOCKED       -32
#define _VN_ERR_REJECTED      -33
#define _VN_ERR_CLOSED        -34
#define _VN_ERR_UNREACHABLE   -35
#define _VN_ERR_USB           -36     /* USB error */
#define _VN_ERR_SEC           -37     /* IOSC error */
#define _VN_ERR_PROXY         -38     /* Proxy error */
#define _VN_ERR_NOTREADY      -39     /* USB is not ready */
#define _VN_ERR_FS            -40     /* File system error */
#define _VN_ERR_RPC_NOTFAST   -41     /* RPC not processed because not marked as fast */

/* New API */

/* Events */
int  _VN_get_events(void* evt_buf, int evt_size, int timeout);
void _VN_cancel_get_events();
int  _VN_get_event_msg(const void* handle, void* msg, uint16_t len);

/* Virtual Network Functions */
bool _VN_is_net_host_valid(_VN_net_t net_id, _VN_host_t host_id);
_VN_addr_t _VN_make_addr(_VN_net_t net_id, _VN_host_t host_id);
_VN_host_t _VN_addr2host(_VN_addr_t addr);
_VN_net_t  _VN_addr2net(_VN_addr_t addr);

/* Virtual Network Status */
uint32_t  _VN_get_status();

/* Virtual Network Initialization */
int       _VN_init(const _VN_config_t* config);
void      _VN_reset(bool reset_all);
int       _VN_set_daemon_net(_VN_net_t net_id);
int       _VN_sconnect(const char* vn_server, uint16_t udp_port,
                       const void* msg, uint16_t msglen);

/* Virtual Network Managment */
int       _VN_new_net(uint32_t netmask, bool is_local, _VN_addr_t vn_server,
                      const void* msg, uint16_t msglen);
int       _VN_accept_net(uint64_t request_id, uint32_t netmask, bool flag,
                         const void* msg, uint16_t msglen);
int       _VN_listen_net(_VN_net_t net_id, bool flag);
int       _VN_connect(_VN_guid_t guid, _VN_net_t net_id, _VN_addr_t vn_server,
                      const void* msg, uint16_t msglen);
int       _VN_leave_net(_VN_net_t net_id, _VN_addr_t vn_server);
int       _VN_accept(uint64_t request_id, bool flag, 
                     const void* msg, uint16_t msglen);
int       _VN_lookup_connection_request(uint64_t request_id, _VN_net_t *pNet);

/* Host Managment */
int        _VN_get_local_hostids(_VN_net_t netid, _VN_host_t* hosts,
                                 uint8_t hosts_size);
_VN_host_t _VN_get_owner_host_id(_VN_net_t net_id);
int        _VN_get_vn_size(_VN_net_t net_id);
int        _VN_get_vn_hostids(_VN_net_t net_id, 
                              _VN_host_t* peers, int peers_size);
int        _VN_get_host_status(_VN_addr_t addr, _VN_host_status* status);
int        _VN_get_host_ip(_VN_addr_t addr, uint32_t* ip);

/* Host Discovery */
int _VN_discover_hosts(bool flag);
int _VN_query_host(const char* hostname, uint16_t port);
int _VN_clear_query_list();

int _VN_register_service(uint32_t service_id, bool public,
                         const void* msg, uint16_t msglen);
int _VN_unregister_service(uint32_t service_id);

int _VN_get_device_count(uint8_t mask);
int _VN_get_guids(uint8_t mask, _VN_guid_t* devices, uint16_t ndevices);

int _VN_get_service_count(_VN_guid_t guid);
int _VN_get_service_ids(_VN_guid_t guid,
                        uint32_t* services, uint16_t nservices);
int _VN_get_service(_VN_guid_t guid, uint32_t service,
                    void* msg, uint16_t msglen);


/* Virtual Network Operation */
int       _VN_config_port(_VN_addr_t addr, _VN_port_t port, int port_state);
int       _VN_set_priority(_VN_addr_t dest, _VN_port_t port, int priority);
int       _VN_send(_VN_net_t net_id, _VN_host_t from_host, _VN_host_t to_host,
                   _VN_port_t port, const void* msg, _VN_msg_len_t len, 
                   const void* opthdr, uint8_t hdr_len,
                   uint32_t attr);
int       _VN_recv(const void* handle, void* msg, _VN_msg_len_t len);

/* Clock Calibration */
_VN_time_t _VN_get_timestamp();
_VN_time_t _VN_conv_timestamp(_VN_addr_t addr,
                              _VN_time_t remote_timestamp);
int _VN_calibrate_clock(_VN_addr_t addr);

/* Device information */
int _VN_get_guid(_VN_addr_t addr, _VN_guid_t* guid);

#ifdef  __cplusplus
}
#endif

#endif /* __VN_H__ */
 

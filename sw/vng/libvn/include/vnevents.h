//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#ifndef __VN_EVENTS_H__
#define __VN_EVENTS_H__

#include "vntypes.h"

#ifdef __cplusplus
extern "C" {
#endif

#define _VN_EVT_NEW_NETWORK                   1
#define _VN_EVT_NEW_CONNECTION                2
#define _VN_EVT_NET_CONFIG                    3
#define _VN_EVT_NET_DISCONNECTED              4
#define _VN_EVT_CONNECTION_REQUEST            5
#define _VN_EVT_CONNECTION_ACCEPTED           6
#define _VN_EVT_NEW_NET_REQUEST               7
#define _VN_EVT_NEW_MESSAGE                   8
#define _VN_EVT_MSG_ERR                       9
#define _VN_EVT_ERR                           10
#define _VN_EVT_DEVICE_UPDATE                 11
#define _VN_EVT_MAX                           11

/* Net config event reason codes */
/* Reasons for getting net config event with joining hosts */
#define _VN_EVT_R_HOST_UPDATE                 1    /* Update of host status */
#define _VN_EVT_R_HOST_JOINED                 2    /* New host joined */
/* Reasons for getting net config event with leaving hosts */
#define _VN_EVT_R_HOST_TIMEOUT                3    /* Host timed out */
#define _VN_EVT_R_HOST_EVICTED                4    /* Host was booted by owner */
#define _VN_EVT_R_HOST_LEFT                   5    /* Host left of his own free will */
#define _VN_EVT_R_NET_DISCONNECTED            6    /* Host leaving because net is disconnected */

/* Reasons for getting device update event */
#define _VN_EVT_R_DEVICE_NEW                 10    /* New device discovered */
#define _VN_EVT_R_DEVICE_UPDATE              11    /* Update to services */
#define _VN_EVT_R_DEVICE_TIMEOUT             12    /* Device timed out */
#define _VN_EVT_R_DEVICE_DISCONNECTED        13    /* Device disconnected */

/* Event Definition */
typedef struct __VN_event_t {
    struct __VN_event_t* next;    /* for linking up events */
    _VN_tag_t             tag;    /* specify the type of this event */
    uint16_t             size;    /* specify the size of this event */
    _VN_callid_t         call_id; /* unique ID assigned to the function call 
                                     that triggers this event, or 0 if there
                                     is no corresponding function call. */

    /* specific fields of each event */
} _VN_event_t;

/* Generic error event */
typedef struct {
    _VN_event_t event;
    int errcode;              /* error code */
    void*       msg_handle;   /* Handle to error message */
    uint16_t    msglen;       /* Length of msg in bytes */
} _VN_err_event;

/* Specifies a new virtual network has been created with the game device being
   it's owner (as result of calling _VN_new_network). */
typedef struct {
    _VN_event_t event;
    _VN_addr_t  myaddr;      /* VN address that encodes both the
                                network ID and host ID. */
} _VN_new_network_event;

/* Specifies the establishment of a new virtual network connection */
typedef struct {
    _VN_event_t event;
    _VN_addr_t  myaddr;      /* VN address that encodes both the
                                network ID and host ID. */
    _VN_host_t  peer_id;     /* host ID of the peer. */
} _VN_new_connection_event;

/* A peer host has joined or left a virtual network */
typedef struct {
    _VN_event_t event;
    _VN_net_t  netid;       /* Virtual network ID */
    _VN_host_t hostid;      /* Host ID of the peer for which 
                               this event is for  
                               (can be _VN_HOST_INVALID if the event
                                is not for a specific host) */
    uint8_t    reason;      /* Reason code describing why this 
                               net config event was generated */
    uint16_t   n_joined;    /* Number of newly joined peers */
    uint16_t   n_left;      /* Number of leaving peers */    
    _VN_host_t peers[];     /* Array of host IDs of newly joined peers,
                               followed by those who just left */    
} _VN_net_config_event;

/* A connection request has been accepted */
typedef struct {
    _VN_event_t event;
    _VN_addr_t  myaddr;
    _VN_host_t  owner;
    uint16_t    msglen;       /* Length of msg in bytes */
    void*       msg_handle;   /* Handle to message buffer 
                                 passed by _VN_accept */
} _VN_connection_accepted_event;

/* The game device is no longer a member of this virtual network */
typedef struct
{
    _VN_event_t event;
    _VN_net_t   netid;    
} _VN_net_disconnected_event;

/* Specifies that a game device is attempting to join this virtual network */
typedef struct
{
    _VN_event_t event;
    _VN_addr_t  addr;         /* Address of VN owner */
    _VN_guid_t  guid;         /* Guid of the joiner */
    uint64_t    request_id;   /* Request ID */
    void*       msg_handle;   /* Handle to message buffer 
                                 passed by _VN_connect */

    uint16_t    msglen;       /* Length of msg in bytes */
} _VN_connection_request_event;

/* Specifies that a game device is requesting to create a new global net */
typedef struct
{
    _VN_event_t event;
    uint64_t    request_id;   /* Request ID */
    uint32_t    netmask;      /* Requested net netmask */
    _VN_addr_t  addr;         /* Address of VN server (recipient of request) */
    _VN_host_t  from_host;    /* Host ID of device requesting new net */
    uint16_t    msglen;       /* Length of msg in bytes */
    void*       msg_handle;   /* Handle to message buffer 
                                 passed by _VN_new_net */
} _VN_new_net_request_event;

/* Specifies the arrival of a new VN packet */
typedef struct 
{
    _VN_event_t event;
    _VN_net_t   netid;        /* virtual network */
    _VN_host_t  fromhost;     /* source host ID. */
    _VN_host_t  tohost;       /* destination host ID. */
    _VN_port_t  port;         /* destination host port. */
    uint8_t     pseq;         /* Per port sequence number 
                                 (different for reliable/lossy packets) */
    uint16_t    size;         /* # of bytes of payload data. */
    _VN_time_t  recv_time;    /* time this packet arrived. */
    void*       msg_handle;   /* a handle to the payload data. */
    uint8_t     attr;         /* attribute indicating if message
                                 was reliable or encrypted */
    uint8_t     opthdr_size;  /* size of optional headers. */
    char        opthdr[];     /* optional headers */
} _VN_new_message_event;

/* Message that was previously scheduled for delivery is not sent correctly */
typedef struct {
    _VN_event_t event;
    int errcode;             /* error code */
    uint32_t   attr;         /* attr argument passed down from _VN_send */
    _VN_net_t netid;         /* source and destination information
                                of the failed message. */
    _VN_host_t fromhost;
    _VN_host_t tohost;
    _VN_port_t port;
    uint8_t    opthdr_size;  /* size of optional headers. */
    char       opthdr[];     /* optional headers */
} _VN_msg_err_event;

/* Specifies a change in the services offered by a device */
typedef struct
{
    _VN_event_t   event;
    _VN_guid_t    guid;            /* Device ID */
    uint8_t       reason;          /* Reason code describing why this
                                      event was generated */
    uint8_t       flags;           /* Flags indicating if device is
                                      LOCAL, LAN, QUERIED, ADHOC, SAME_PROXY */
    uint16_t      nservices_add;   /* Number of services added */
    uint16_t      nservices_rem;   /* Number of services removed */
    uint32_t      services[];      /* List of service IDs, new services
                                      followed by removed services */
} _VN_device_update_event;

#ifdef  __cplusplus
}
#endif
 
#endif /* __VN_EVENTS_H__ */
 

/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */
#include "sc/sc.h"
#include "sc/ios.h"
#include "sc/libc.h"

#include "rpc_dispatch.h"

#define STACK_SIZE 32*1024
const u8 initStack[STACK_SIZE];
const u32 initStackSize = sizeof(initStack);
const u32 initPriority = 10;

/* temp. hack to hard code the entry point of the "Game_NC" */
const u32 entry_point = 0x9000000;

/* a dummy main program for linking together all the NC libraries and give
   it a process and thread.  It also starts the Game_NC.  This could later
   be replaced by the Viewer_NC.
*/
int
main(void)
{
    /* temp. hack to load Game_NC into SDRAM at "entry_point" */
    SK_LaunchGame(entry_point);

    /* pass control (and the jump table conversion function) to the entry
       point of Game_NC */
    (*(void (*)(void *))entry_point)(jtab_get_addr);

    exit(0);
    return 0;
}

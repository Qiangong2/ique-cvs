/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */

#ifndef __VNP_H__
#define __VNP_H__  1

#include <sc/ios.h>

#include "vng.h"
#include "vng_p.h"

#if !defined(_GBA) && !defined(_SC)
#pragma pack(push, 4)
#endif

/*   16 bit read or read/write args are stored in sdram as 32 bit values
 *   to avoid unreliability of 16 bit writes from gba.  Write only args
 *   (e.g. the return value or returned callerTag) the are left as 16 bit
 *    since they are not affected by the problem.
 */
typedef uint32_t VNMember_32;
typedef uint32_t VNGPort_32;
typedef uint32_t VNServiceTag_32;
typedef uint32_t VNCallerTag_32;
typedef uint32_t VNAttr_32;

/****************************/

typedef struct {
    VNG          *vng;
    VNGInitParm  *parm;
    VNGInitParm   realParm;

} VNPA_r__VNG_Init;            /* Read only args */


typedef struct {

} VNPA_rw_VNG_Init;            /* Read/write arg buffer */


typedef struct {
    VNGErrCode    rv;          /* Must be first */

} VNPA__w_VNG_Init;            /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vng;          /* Non-optional reads */

} VNPRv_VNG_Init;  /* Read vector */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VNG_Init;  /* Write vector */


typedef struct {
    VNPRv_VNG_Init  read;
    VNPWv_VNG_Init  write;

} VNPV_r__VNG_Init;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VNG_Init  a_r;
    VNPV_r__VNG_Init  v_r;
    VNPA_rw_VNG_Init  a_rw;
    VNPA__w_VNG_Init  a__w;

} VNPIBuf_VNG_Init;  /* Ioctlv args, buffered args, rv, vector */


/****************************/

typedef struct {
    VNG     *vng;

} VNPA_r__VNG_Fini;  /* Read only args */


typedef struct {

} VNPA_rw_VNG_Fini;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode    rv;          /* Must be first */

} VNPA__w_VNG_Fini;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vng;          /* Non-optional reads */

} VNPRv_VNG_Fini;  /* Read vector */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VNG_Fini;  /* Write vector */


typedef struct {
    VNPRv_VNG_Fini  read;
    VNPWv_VNG_Fini  write;

} VNPV_r__VNG_Fini;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VNG_Fini  a_r;
    VNPV_r__VNG_Fini  v_r;
    VNPA_rw_VNG_Fini  a_rw;
    VNPA__w_VNG_Fini  a__w;

} VNPIBuf_VNG_Fini;


/****************************/

typedef struct {
    VNG        *vng;
    char        serverName [VNG_SERVER_NAME_BUF_SIZE];
    VNGPort_32  serverPort;
    VNGTimeout  timeout;


} VNPA_r__VNG_Register;  /* Read only args */


typedef struct {

} VNPA_rw_VNG_Register;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode    rv;          /* Must be first */
    char          loginName [VNG_LOGIN_BUF_SIZE];
    char          passwd [VNG_PASSWD_BUF_SIZE];

} VNPA__w_VNG_Register;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vng;          /* Non-optional reads */

} VNPRv_VNG_Register;  /* Read vector */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VNG_Register;  /* Write vector */


typedef struct {
    VNPRv_VNG_Register  read;
    VNPWv_VNG_Register  write;

} VNPV_r__VNG_Register;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VNG_Register  a_r;
    VNPV_r__VNG_Register  v_r;
    VNPA_rw_VNG_Register  a_rw;
    VNPA__w_VNG_Register  a__w;

} VNPIBuf_VNG_Register;


/****************************/

typedef struct {
    VNG        *vng;
    char        serverName [VNG_SERVER_NAME_BUF_SIZE];
    VNGPort_32  serverPort;
    char        loginName [VNG_LOGIN_BUF_SIZE];
    char        passwd [VNG_PASSWD_BUF_SIZE];
    int32_t     loginType;
    VNGTimeout  timeout;


} VNPA_r__VNG_Login;  /* Read only args */


typedef struct {

} VNPA_rw_VNG_Login;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode    rv;          /* Must be first */

} VNPA__w_VNG_Login;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vng;          /* Non-optional reads */

} VNPRv_VNG_Login;  /* Read vector */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VNG_Login;  /* Write vector */


typedef struct {
    VNPRv_VNG_Login  read;
    VNPWv_VNG_Login  write;

} VNPV_r__VNG_Login;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VNG_Login  a_r;
    VNPV_r__VNG_Login  v_r;
    VNPA_rw_VNG_Login  a_rw;
    VNPA__w_VNG_Login  a__w;

} VNPIBuf_VNG_Login;


/****************************/

typedef struct {
    VNG        *vng;
    VNGTimeout  timeout;

} VNPA_r__VNG_Logout;  /* Read only args */


typedef struct {

} VNPA_rw_VNG_Logout;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode    rv;          /* Must be first */

} VNPA__w_VNG_Logout;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vng;          /* Non-optional reads */

} VNPRv_VNG_Logout;  /* Read vector */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VNG_Logout;  /* Write vector */


typedef struct {
    VNPRv_VNG_Logout  read;
    VNPWv_VNG_Logout  write;

} VNPV_r__VNG_Logout;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VNG_Logout  a_r;
    VNPV_r__VNG_Logout  v_r;
    VNPA_rw_VNG_Logout  a_rw;
    VNPA__w_VNG_Logout  a__w;

} VNPIBuf_VNG_Logout;


/****************************/

typedef struct {
    VNG     *vng;

} VNPA_r__VNG_GetLoginType;  /* Read only args */


typedef struct {

} VNPA_rw_VNG_GetLoginType;  /* Read/write arg buffer */


typedef struct {
    int32_t    rv;             /* Must be first */

} VNPA__w_VNG_GetLoginType;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vng;          /* Non-optional reads */

} VNPRv_VNG_GetLoginType;  /* Read vector */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VNG_GetLoginType;  /* Write vector */


typedef struct {
    VNPRv_VNG_GetLoginType  read;
    VNPWv_VNG_GetLoginType  write;

} VNPV_r__VNG_GetLoginType;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VNG_GetLoginType  a_r;
    VNPV_r__VNG_GetLoginType  v_r;
    VNPA_rw_VNG_GetLoginType  a_rw;
    VNPA__w_VNG_GetLoginType  a__w;

} VNPIBuf_VNG_GetLoginType;


/****************************/

typedef struct {
    VNG         *vng;
    VNGUserId    uid;
    VNGTimeout   timeout;

} VNPA_r__VNG_GetUserInfo;  /* Read only args */


typedef struct {

} VNPA_rw_VNG_GetUserInfo;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode    rv;          /* Must be first */
    VNGUserInfo   uinfo;

} VNPA__w_VNG_GetUserInfo;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vng;          /* Non-optional reads */

} VNPRv_VNG_GetUserInfo;  /* Read vector */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VNG_GetUserInfo;  /* Write vector */


typedef struct {
    VNPRv_VNG_GetUserInfo  read;
    VNPWv_VNG_GetUserInfo  write;

} VNPV_r__VNG_GetUserInfo;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VNG_GetUserInfo  a_r;
    VNPV_r__VNG_GetUserInfo  v_r;
    VNPA_rw_VNG_GetUserInfo  a_rw;
    VNPA__w_VNG_GetUserInfo  a__w;

} VNPIBuf_VNG_GetUserInfo;


/****************************/

typedef struct {
    VNG       *vng;
    VN        *vn;
    VNClass    vnClass;
    VNDomain   domain;
    VNPolicies policies;

} VNPA_r__VNG_NewVN;  /* Read only args */


typedef struct {

} VNPA_rw_VNG_NewVN;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode    rv;          /* Must be first */

} VNPA__w_VNG_NewVN;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vng;          /* Non-optional reads */

} VNPRv_VNG_NewVN;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
    IOSIoVector  vn;           /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VNG_NewVN;  /* Write vector */


typedef struct {
    VNPRv_VNG_NewVN  read;
    VNPWv_VNG_NewVN  write;

} VNPV_r__VNG_NewVN;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VNG_NewVN  a_r;
    VNPV_r__VNG_NewVN  v_r;
    VNPA_rw_VNG_NewVN  a_rw;
    VNPA__w_VNG_NewVN  a__w;

} VNPIBuf_VNG_NewVN;


/****************************/

typedef struct {
    VNG       *vng;
    VN        *vn;

} VNPA_r__VNG_DeleteVN;  /* Read only args */


typedef struct {

} VNPA_rw_VNG_DeleteVN;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode    rv;          /* Must be first */

} VNPA__w_VNG_DeleteVN;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vng;          /* Non-optional reads */
    IOSIoVector  vn;

} VNPRv_VNG_DeleteVN;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VNG_DeleteVN;  /* Write vector */


typedef struct {
    VNPRv_VNG_DeleteVN  read;
    VNPWv_VNG_DeleteVN  write;

} VNPV_r__VNG_DeleteVN;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VNG_DeleteVN  a_r;
    VNPV_r__VNG_DeleteVN  v_r;
    VNPA_rw_VNG_DeleteVN  a_rw;
    VNPA__w_VNG_DeleteVN  a__w;

} VNPIBuf_VNG_DeleteVN;


/****************************/

typedef struct {
    VNG         *vng;
    VNId         vnId;
    const char  *msg;
    VN          *vn;
    char        *denyReason;
    size_t       denyReasonLen;
    VNGTimeout   timeout;

} VNPA_r__VNG_JoinVN;  /* Read only args */


typedef struct {

} VNPA_rw_VNG_JoinVN;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode    rv;          /* Must be first */

} VNPA__w_VNG_JoinVN;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  msg;          /* Optional read */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vng;          /* Non-optional reads */

} VNPRv_VNG_JoinVN;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
    IOSIoVector  vn;           /* Non-optional writes */
    IOSIoVector  denyReason;   /* Optional writes last */

} VNPWv_VNG_JoinVN;  /* Write vector */


typedef struct {
    VNPRv_VNG_JoinVN  read;
    VNPWv_VNG_JoinVN  write;

} VNPV_r__VNG_JoinVN;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VNG_JoinVN  a_r;
    VNPV_r__VNG_JoinVN  v_r;
    VNPA_rw_VNG_JoinVN  a_rw;
    VNPA__w_VNG_JoinVN  a__w;

} VNPIBuf_VNG_JoinVN;


/****************************/

typedef struct {
    VNG         *vng;
    VNId         vnId;
    VN         **vn;
    uint64_t    *requestId;
    VNGUserInfo *userInfo;
    char        *joinReason;
    size_t       joinReasonLen;
    VNGTimeout   timeout;

} VNPA_r__VNG_GetJoinRequest;  /* Read only args */


typedef struct {

} VNPA_rw_VNG_GetJoinRequest;  /* Read/write arg buffer */

typedef struct {
    VNGErrCode     rv;         /* Must be first */
    VN            *vn;
    uint64_t       requestId;
    VNGUserInfo    userInfo;

} VNPA__w_VNG_GetJoinRequest;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vng;          /* Non-optional reads */

} VNPRv_VNG_GetJoinRequest;  /* Read vector */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
    IOSIoVector  joinReason;   /* Optional writes last */

} VNPWv_VNG_GetJoinRequest;  /* Write vector */


typedef struct {
    VNPRv_VNG_GetJoinRequest  read;
    VNPWv_VNG_GetJoinRequest  write;

} VNPV_r__VNG_GetJoinRequest;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VNG_GetJoinRequest  a_r;
    VNPV_r__VNG_GetJoinRequest  v_r;
    VNPA_rw_VNG_GetJoinRequest  a_rw;
    VNPA__w_VNG_GetJoinRequest  a__w;

} VNPIBuf_VNG_GetJoinRequest;   /* Ioctlv args, buffered args, rv, vector */


/****************************/

typedef struct {
    VNG        *vng;
    uint64_t    requestId;

} VNPA_r__VNG_AcceptJoinRequest;  /* Read only args */


typedef struct {

} VNPA_rw_VNG_AcceptJoinRequest;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode    rv;          /* Must be first */

} VNPA__w_VNG_AcceptJoinRequest;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vng;          /* Non-optional reads */

} VNPRv_VNG_AcceptJoinRequest;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VNG_AcceptJoinRequest;  /* Write vector */


typedef struct {
    VNPRv_VNG_AcceptJoinRequest  read;
    VNPWv_VNG_AcceptJoinRequest  write;

} VNPV_r__VNG_AcceptJoinRequest;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VNG_AcceptJoinRequest  a_r;
    VNPV_r__VNG_AcceptJoinRequest  v_r;
    VNPA_rw_VNG_AcceptJoinRequest  a_rw;
    VNPA__w_VNG_AcceptJoinRequest  a__w;

} VNPIBuf_VNG_AcceptJoinRequest;


/****************************/

typedef struct {
    VNG        *vng;
    uint64_t    requestId;   
    const char *reason;

} VNPA_r__VNG_RejectJoinRequest;  /* Read only args */


typedef struct {

} VNPA_rw_VNG_RejectJoinRequest;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode    rv;          /* Must be first */

} VNPA__w_VNG_RejectJoinRequest;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  reason;       /* Optional read */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vng;          /* Non-optional reads */

} VNPRv_VNG_RejectJoinRequest;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VNG_RejectJoinRequest;  /* Write vector */


typedef struct {
    VNPRv_VNG_RejectJoinRequest  read;
    VNPWv_VNG_RejectJoinRequest  write;

} VNPV_r__VNG_RejectJoinRequest;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VNG_RejectJoinRequest  a_r;
    VNPV_r__VNG_RejectJoinRequest  v_r;
    VNPA_rw_VNG_RejectJoinRequest  a_rw;
    VNPA__w_VNG_RejectJoinRequest  a__w;

} VNPIBuf_VNG_RejectJoinRequest;


/****************************/

typedef struct {
    VNG *vng;
    VN  *vn;

} VNPA_r__VNG_LeaveVN;  /* Read only args */


typedef struct {

} VNPA_rw_VNG_LeaveVN;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode    rv;          /* Must be first */

} VNPA__w_VNG_LeaveVN;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vng;          /* Non-optional reads */
    IOSIoVector  vn;

} VNPRv_VNG_LeaveVN;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VNG_LeaveVN;  /* Write vector */


typedef struct {
    VNPRv_VNG_LeaveVN  read;
    VNPWv_VNG_LeaveVN  write;

} VNPV_r__VNG_LeaveVN;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VNG_LeaveVN  a_r;
    VNPV_r__VNG_LeaveVN  v_r;
    VNPA_rw_VNG_LeaveVN  a_rw;
    VNPA__w_VNG_LeaveVN  a__w;

} VNPIBuf_VNG_LeaveVN;


/****************************/

typedef struct {
    VNG         *vng;
    VN          *vn;
    VNMember_32  memb;

} VNPA_r__VNG_RemoveVNMember;  /* Read only args */


typedef struct {

} VNPA_rw_VNG_RemoveVNMember;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode    rv;          /* Must be first */

} VNPA__w_VNG_RemoveVNMember;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vng;          /* Non-optional reads */
    IOSIoVector  vn;

} VNPRv_VNG_RemoveVNMember;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VNG_RemoveVNMember;  /* Write vector */


typedef struct {
    VNPRv_VNG_RemoveVNMember  read;
    VNPWv_VNG_RemoveVNMember  write;

} VNPV_r__VNG_RemoveVNMember;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VNG_RemoveVNMember  a_r;
    VNPV_r__VNG_RemoveVNMember  v_r;
    VNPA_rw_VNG_RemoveVNMember  a_rw;
    VNPA__w_VNG_RemoveVNMember  a__w;

} VNPIBuf_VNG_RemoveVNMember;


/****************************/

typedef struct {
    VN *vn;

} VNPA_r__VN_NumMembers;  /* Read only args */


typedef struct {

} VNPA_rw_VN_NumMembers;  /* Read/write arg buffer */


typedef struct {
    uint32_t                 rv;       /* Must be first */

} VNPA__w_VN_NumMembers;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vn;           /* Non-optional reads */

} VNPRv_VN_NumMembers;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VN_NumMembers;  /* Write vector */


typedef struct {
    VNPRv_VN_NumMembers  read;
    VNPWv_VN_NumMembers  write;

} VNPV_r__VN_NumMembers;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VN_NumMembers  a_r;
    VNPV_r__VN_NumMembers  v_r;
    VNPA_rw_VN_NumMembers  a_rw;
    VNPA__w_VN_NumMembers  a__w;

} VNPIBuf_VN_NumMembers;


/****************************/

typedef struct {
    VN       *vn;
    VNMember *members;
    uint32_t  nMembers;

} VNPA_r__VN_GetMembers;  /* Read only args */


typedef struct {

} VNPA_rw_VN_GetMembers;  /* Read/write arg buffer */


typedef struct {
    uint32_t     rv;          /* Must be first */

} VNPA__w_VN_GetMembers;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vn;           /* Non-optional reads */

} VNPRv_VN_GetMembers;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
    IOSIoVector  members;      /* Optional writes last */

} VNPWv_VN_GetMembers;  /* Write vector */


typedef struct {
    VNPRv_VN_GetMembers  read;
    VNPWv_VN_GetMembers  write;

} VNPV_r__VN_GetMembers;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VN_GetMembers  a_r;
    VNPV_r__VN_GetMembers  v_r;
    VNPA_rw_VN_GetMembers  a_rw;
    VNPA__w_VN_GetMembers  a__w;

} VNPIBuf_VN_GetMembers;


/****************************/

typedef struct {
    VN *vn;
    VNMember_32 memb;

} VNPA_r__VN_MemberState;  /* Read only args */


typedef struct {

} VNPA_rw_VN_MemberState;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode    rv;          /* Must be first */

} VNPA__w_VN_MemberState;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vn;           /* Non-optional reads */

} VNPRv_VN_MemberState;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VN_MemberState;  /* Write vector */


typedef struct {
    VNPRv_VN_MemberState  read;
    VNPWv_VN_MemberState  write;

} VNPV_r__VN_MemberState;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VN_MemberState  a_r;
    VNPV_r__VN_MemberState  v_r;
    VNPA_rw_VN_MemberState  a_rw;
    VNPA__w_VN_MemberState  a__w;

} VNPIBuf_VN_MemberState;


/****************************/

typedef struct {
    VN *vn;
    VNMember_32 memb;
    VNGTimeout timeout;

} VNPA_r__VN_MemberUserId;  /* Read only args */


typedef struct {

} VNPA_rw_VN_MemberUserId;  /* Read/write arg buffer */


typedef struct {
    VNGUserId    rv;          /* Must be first */

} VNPA__w_VN_MemberUserId;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vn;           /* Non-optional reads */

} VNPRv_VN_MemberUserId;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VN_MemberUserId;  /* Write vector */


typedef struct {
    VNPRv_VN_MemberUserId  read;
    VNPWv_VN_MemberUserId  write;

} VNPV_r__VN_MemberUserId;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VN_MemberUserId  a_r;
    VNPV_r__VN_MemberUserId  v_r;
    VNPA_rw_VN_MemberUserId  a_rw;
    VNPA__w_VN_MemberUserId  a__w;

} VNPIBuf_VN_MemberUserId;


/****************************/

typedef struct {
    VN *vn;
    VNMember_32 memb;

} VNPA_r__VN_MemberLatency;  /* Read only args */


typedef struct {

} VNPA_rw_VN_MemberLatency;  /* Read/write arg buffer */


typedef struct {
    VNGMillisec  rv;          /* Must be first */

} VNPA__w_VN_MemberLatency;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vn;           /* Non-optional reads */

} VNPRv_VN_MemberLatency;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VN_MemberLatency;  /* Write vector */


typedef struct {
    VNPRv_VN_MemberLatency  read;
    VNPWv_VN_MemberLatency  write;

} VNPV_r__VN_MemberLatency;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VN_MemberLatency  a_r;
    VNPV_r__VN_MemberLatency  v_r;
    VNPA_rw_VN_MemberLatency  a_rw;
    VNPA__w_VN_MemberLatency  a__w;

} VNPIBuf_VN_MemberLatency;


/****************************/

typedef struct {
    VN *vn;
    VNMember_32 memb;

} VNPA_r__VN_MemberDeviceType;  /* Read only args */


typedef struct {

} VNPA_rw_VN_MemberDeviceType;  /* Read/write arg buffer */


typedef struct {
    VNGDeviceType rv;          /* Must be first */

} VNPA__w_VN_MemberDeviceType;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vn;           /* Non-optional reads */

} VNPRv_VN_MemberDeviceType;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VN_MemberDeviceType;  /* Write vector */


typedef struct {
    VNPRv_VN_MemberDeviceType  read;
    VNPWv_VN_MemberDeviceType  write;

} VNPV_r__VN_MemberDeviceType;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VN_MemberDeviceType  a_r;
    VNPV_r__VN_MemberDeviceType  v_r;
    VNPA_rw_VN_MemberDeviceType  a_rw;
    VNPA__w_VN_MemberDeviceType  a__w;

} VNPIBuf_VN_MemberDeviceType;


/****************************/

typedef struct {
    VN      *vn;
    VNId    *vnId;

} VNPA_r__VN_GetVNId;  /* Read only args */


typedef struct {

} VNPA_rw_VN_GetVNId;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode    rv;          /* Must be first */
    VNId          vnId;

} VNPA__w_VN_GetVNId;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vn;           /* Non-optional reads */

} VNPRv_VN_GetVNId;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VN_GetVNId;  /* Write vector */


typedef struct {
    VNPRv_VN_GetVNId  read;
    VNPWv_VN_GetVNId  write;

} VNPV_r__VN_GetVNId;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VN_GetVNId  a_r;
    VNPV_r__VN_GetVNId  v_r;
    VNPA_rw_VN_GetVNId  a_rw;
    VNPA__w_VN_GetVNId  a__w;

} VNPIBuf_VN_GetVNId;


/****************************/

typedef struct {
    VN                *vn;
    VNServiceTypeSet   selectFrom;
    VNServiceTypeSet  *ready;
    VNMember_32        memb;
    VNGTimeout         timeout;

} VNPA_r__VN_Select;  /* Read only args */


typedef struct {

} VNPA_rw_VN_Select;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode        rv;      /* Must be first */
    VNServiceTypeSet  ready;

} VNPA__w_VN_Select;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vn;           /* Non-optional reads */

} VNPRv_VN_Select;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VN_Select;  /* Write vector */


typedef struct {
    VNPRv_VN_Select  read;
    VNPWv_VN_Select  write;

} VNPV_r__VN_Select;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VN_Select  a_r;
    VNPV_r__VN_Select  v_r;
    VNPA_rw_VN_Select  a_rw;
    VNPA__w_VN_Select  a__w;

} VNPIBuf_VN_Select;


/****************************/

typedef struct {
    VN              *vn;
    VNMember_32      memb; 
    VNServiceTag_32  serviceTag;
    const void      *msg;
    size_t           msglen;
    VNAttr_32        attr;
    VNGTimeout       timeout;

} VNPA_r__VN_SendMsg;  /* Read only args */


typedef struct {

} VNPA_rw_VN_SendMsg;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode    rv;          /* Must be first */

} VNPA__w_VN_SendMsg;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  msg;          /* Optional read */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vn;           /* Non-optional reads */

} VNPRv_VN_SendMsg;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VN_SendMsg;  /* Write vector */


typedef struct {
    VNPRv_VN_SendMsg  read;
    VNPWv_VN_SendMsg  write;

} VNPV_r__VN_SendMsg;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VN_SendMsg  a_r;
    VNPV_r__VN_SendMsg  v_r;
    VNPA_rw_VN_SendMsg  a_rw;
    VNPA__w_VN_SendMsg  a__w;

} VNPIBuf_VN_SendMsg;


/****************************/

typedef struct {
    VN              *vn; 
    VNMember_32      memb;  
    VNServiceTag_32  serviceTag;
    void            *msg;
    VNMsgHdr        *hdr;
    VNGTimeout       timeout;

} VNPA_r__VN_RecvMsg;  /* Read only args */


typedef struct {
    size_t        msglen;

} VNPA_rw_VN_RecvMsg;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode    rv;          /* Must be first */
    VNMsgHdr      hdr;         /* Optional must be last */

} VNPA__w_VN_RecvMsg;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vn;           /* Non-optional reads */

} VNPRv_VN_RecvMsg;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
    IOSIoVector  msg;          /* Optional writes last */

} VNPWv_VN_RecvMsg;  /* Write vector */


typedef struct {
    VNPRv_VN_RecvMsg  read;
    VNPWv_VN_RecvMsg  write;

} VNPV_r__VN_RecvMsg;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VN_RecvMsg  a_r;
    VNPV_r__VN_RecvMsg  v_r;
    VNPA_rw_VN_RecvMsg  a_rw;
    VNPA__w_VN_RecvMsg  a__w;

} VNPIBuf_VN_RecvMsg;


/****************************/

typedef struct {
    VN              *vn; 
    VNMember_32      memb;
    VNServiceTag_32  serviceTag;
    const void      *msg;
    size_t           msglen;
    int32_t          optData;
    VNGTimeout       timeout;

} VNPA_r__VN_SendReq;  /* Read only args */


typedef struct {

} VNPA_rw_VN_SendReq;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode   rv;          /* Must be first */
    VNCallerTag  callerTag;   /* Not optional */

} VNPA__w_VN_SendReq;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  msg;          /* Optional read */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vn;           /* Non-optional reads */

} VNPRv_VN_SendReq;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VN_SendReq;  /* Write vector */


typedef struct {
    VNPRv_VN_SendReq  read;
    VNPWv_VN_SendReq  write;

} VNPV_r__VN_SendReq;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VN_SendReq  a_r;
    VNPV_r__VN_SendReq  v_r;
    VNPA_rw_VN_SendReq  a_rw;
    VNPA__w_VN_SendReq  a__w;

} VNPIBuf_VN_SendReq;


/****************************/

typedef struct {
    VN              *vn;
    VNMember_32      memb;  
    VNServiceTag_32  serviceTag;
    void            *msg;
    VNMsgHdr        *hdr;
    VNGTimeout       timeout;

} VNPA_r__VN_RecvReq;  /* Read only args */


typedef struct {
    size_t        msglen;

} VNPA_rw_VN_RecvReq;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode    rv;          /* Must be first */
    VNMsgHdr      hdr;         /* Optional must be last */

} VNPA__w_VN_RecvReq;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vn;           /* Non-optional reads */

} VNPRv_VN_RecvReq;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
    IOSIoVector  msg;          /* Optional writes last */

} VNPWv_VN_RecvReq;  /* Write vector */


typedef struct {
    VNPRv_VN_RecvReq  read;
    VNPWv_VN_RecvReq  write;

} VNPV_r__VN_RecvReq;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VN_RecvReq  a_r;
    VNPV_r__VN_RecvReq  v_r;
    VNPA_rw_VN_RecvReq  a_rw;
    VNPA__w_VN_RecvReq  a__w;

} VNPIBuf_VN_RecvReq;


/****************************/

typedef struct {
    VN              *vn; 
    VNMember_32      memb;
    VNServiceTag_32  serviceTag;
    VNCallerTag_32   callerTag;
    const void      *msg;
    size_t           msglen;
    int32_t          optData;
    VNGTimeout       timeout;

} VNPA_r__VN_SendResp;  /* Read only args */


typedef struct {

} VNPA_rw_VN_SendResp;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode    rv;          /* Must be first */

} VNPA__w_VN_SendResp;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  msg;          /* Optional read */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vn;           /* Non-optional reads */

} VNPRv_VN_SendResp;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VN_SendResp;  /* Write vector */


typedef struct {
    VNPRv_VN_SendResp  read;
    VNPWv_VN_SendResp  write;

} VNPV_r__VN_SendResp;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VN_SendResp  a_r;
    VNPV_r__VN_SendResp  v_r;
    VNPA_rw_VN_SendResp  a_rw;
    VNPA__w_VN_SendResp  a__w;

} VNPIBuf_VN_SendResp;


/****************************/

typedef struct {
    VN              *vn;
    VNMember_32      memb;  
    VNServiceTag_32  serviceTag;
    VNCallerTag_32   callerTag;  
    void            *msg;
    VNMsgHdr        *hdr;
    VNGTimeout       timeout;

} VNPA_r__VN_RecvResp;  /* Read only args */


typedef struct {
    size_t        msglen;

} VNPA_rw_VN_RecvResp;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode    rv;          /* Must be first */
    VNMsgHdr      hdr;         /* Optional must be last */

} VNPA__w_VN_RecvResp;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vn;           /* Non-optional reads */

} VNPRv_VN_RecvResp;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
    IOSIoVector  msg;          /* Optional writes last */

} VNPWv_VN_RecvResp;  /* Write vector */


typedef struct {
    VNPRv_VN_RecvResp  read;
    VNPWv_VN_RecvResp  write;

} VNPV_r__VN_RecvResp;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VN_RecvResp  a_r;
    VNPV_r__VN_RecvResp  v_r;
    VNPA_rw_VN_RecvResp  a_rw;
    VNPA__w_VN_RecvResp  a__w;

} VNPIBuf_VN_RecvResp;


/****************************/

typedef struct {
    VN               *vn;
    VNServiceTag_32   procId;
    VNRemoteProcedure proc;

} VNPA_r__VN_RegisterRPCService;  /* Read only args */


typedef struct {

} VNPA_rw_VN_RegisterRPCService;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode    rv;          /* Must be first */

} VNPA__w_VN_RegisterRPCService;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vn;           /* Non-optional reads */

} VNPRv_VN_RegisterRPCService;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VN_RegisterRPCService;  /* Write vector */


typedef struct {
    VNPRv_VN_RegisterRPCService  read;
    VNPWv_VN_RegisterRPCService  write;

} VNPV_r__VN_RegisterRPCService;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VN_RegisterRPCService  a_r;
    VNPV_r__VN_RegisterRPCService  v_r;
    VNPA_rw_VN_RegisterRPCService  a_rw;
    VNPA__w_VN_RegisterRPCService  a__w;

} VNPIBuf_VN_RegisterRPCService;


/****************************/

typedef struct {
    VN               *vn;
    VNServiceTag_32   procId;

} VNPA_r__VN_UnregisterRPCService;  /* Read only args */


typedef struct {

} VNPA_rw_VN_UnregisterRPCService;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode    rv;          /* Must be first */

} VNPA__w_VN_UnregisterRPCService;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vn;           /* Non-optional reads */

} VNPRv_VN_UnregisterRPCService;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VN_UnregisterRPCService;  /* Write vector */


typedef struct {
    VNPRv_VN_UnregisterRPCService  read;
    VNPWv_VN_UnregisterRPCService  write;

} VNPV_r__VN_UnregisterRPCService;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VN_UnregisterRPCService  a_r;
    VNPV_r__VN_UnregisterRPCService  v_r;
    VNPA_rw_VN_UnregisterRPCService  a_rw;
    VNPA__w_VN_UnregisterRPCService  a__w;

} VNPIBuf_VN_UnregisterRPCService;


/****************************/

typedef struct {

    VN              *vn; 
    VNMember_32      memb;
    VNServiceTag_32  serviceTag;
    const void      *args;
    size_t           argsLen;
    void            *ret;
    VNGTimeout       timeout;

} VNPA_r__VN_SendRPC;  /* Read only args */


typedef struct {
    size_t        retLen;
    int32_t       optData;

} VNPA_rw_VN_SendRPC;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode    rv;          /* Must be first */

} VNPA__w_VN_SendRPC;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  args;         /* Optional read */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vn;           /* Non-optional reads */

} VNPRv_VN_SendRPC;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
    IOSIoVector  ret;          /* Optional writes last */

}VNPWv_VN_SendRPC;  /* Write vector */


typedef struct {
    VNPRv_VN_SendRPC  read;
    VNPWv_VN_SendRPC  write;

} VNPV_r__VN_SendRPC;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VN_SendRPC  a_r;
    VNPV_r__VN_SendRPC  v_r;
    VNPA_rw_VN_SendRPC  a_rw;
    VNPA__w_VN_SendRPC  a__w;

} VNPIBuf_VN_SendRPC;


/****************************/

typedef struct {
    VNG               *vng; 
    void              *args;
    VNGTimeout         timeout;

} VNPA_r__VNG_RecvRPC;  /* Read only args */


typedef struct {
    size_t            argsLen;

} VNPA_rw_VNG_RecvRPC;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode    rv;          /* Must be first */
    VNRemoteProcedure proc;    /* Not Optional */
    VN               *vn;      /* Not Optional */
    VNMsgHdr          hdr;     /* Not Optional */
                               /* Optional must be last */

} VNPA__w_VNG_RecvRPC;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vng;          /* Non-optional reads */

} VNPRv_VNG_RecvRPC;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
    IOSIoVector  args;         /* Optional writes last */
  
} VNPWv_VNG_RecvRPC;  /* Write vector */


typedef struct {
    VNPRv_VNG_RecvRPC  read;
    VNPWv_VNG_RecvRPC  write;

} VNPV_r__VNG_RecvRPC;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VNG_RecvRPC  a_r;
    VNPV_r__VNG_RecvRPC  v_r;
    VNPA_rw_VNG_RecvRPC  a_rw;
    VNPA__w_VNG_RecvRPC  a__w;

} VNPIBuf_VNG_RecvRPC;


/****************************/

typedef struct {
    VNG        *vng;
    VNGTimeout  timeout;

} VNPA_r__VNG_GetEvent;  /* Read only args */


typedef struct {

} VNPA_rw_VNG_GetEvent;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode    rv;          /* Must be first */
    VNGEvent   event;

} VNPA__w_VNG_GetEvent;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vng;          /* Non-optional reads */

} VNPRv_VNG_GetEvent;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VNG_GetEvent;  /* Write vector */


typedef struct {
    VNPRv_VNG_GetEvent  read;
    VNPWv_VNG_GetEvent  write;

} VNPV_r__VNG_GetEvent;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VNG_GetEvent  a_r;
    VNPV_r__VNG_GetEvent  v_r;
    VNPA_rw_VNG_GetEvent  a_rw;
    VNPA__w_VNG_GetEvent  a__w;

} VNPIBuf_VNG_GetEvent;


/****************************/

typedef struct {
    VNG          *vng;
    VNGUserId    *uid;
    uint32_t      nUsers;
    const char   *message;
    size_t        messageLen;

} VNPA_r__VNG_SendNotification;  /* Read only args */


typedef struct {

} VNPA_rw_VNG_SendNotification;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode    rv;          /* Must be first */

} VNPA__w_VNG_SendNotification;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  message;      /* Optional read */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vng;          /* Non-optional reads */
    IOSIoVector  uid;

} VNPRv_VNG_SendNotification;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VNG_SendNotification;  /* Write vector */


typedef struct {
    VNPRv_VNG_SendNotification  read;
    VNPWv_VNG_SendNotification  write;

} VNPV_r__VNG_SendNotification;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VNG_SendNotification  a_r;
    VNPV_r__VNG_SendNotification  v_r;
    VNPA_rw_VNG_SendNotification  a_rw;
    VNPA__w_VNG_SendNotification  a__w;

} VNPIBuf_VNG_SendNotification;


/****************************/

typedef struct {
    VNG          *vng;
    char         *message;
    VNGTimeout    timeout;

} VNPA_r__VNG_RecvNotification;  /* Read only args */


typedef struct {
    size_t        messageLen;

} VNPA_rw_VNG_RecvNotification;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode    rv;          /* Must be first */
    VNGUserInfo   userInfo;

} VNPA__w_VNG_RecvNotification;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vng;          /* Non-optional reads */

} VNPRv_VNG_RecvNotification;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
    IOSIoVector  message;    /* Optional writes last */

} VNPWv_VNG_RecvNotification;  /* Write vector */


typedef struct {
    VNPRv_VNG_RecvNotification  read;
    VNPWv_VNG_RecvNotification  write;

} VNPV_r__VNG_RecvNotification;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VNG_RecvNotification  a_r;
    VNPV_r__VNG_RecvNotification  v_r;
    VNPA_rw_VNG_RecvNotification  a_rw;
    VNPA__w_VNG_RecvNotification  a__w;

} VNPIBuf_VNG_RecvNotification;


/****************************/

typedef struct {
    VNG          *vng;
    VNGGameInfo   info;
    char         *comments;
    VNGTimeout    timeout;

} VNPA_r__VNG_RegisterGame;  /* Read only args */


typedef struct {

} VNPA_rw_VNG_RegisterGame;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode    rv;          /* Must be first */

} VNPA__w_VNG_RegisterGame;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  comments;     /* Optional read */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vng;          /* Non-optional reads */

} VNPRv_VNG_RegisterGame;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VNG_RegisterGame;  /* Write vector */


typedef struct {
    VNPRv_VNG_RegisterGame  read;
    VNPWv_VNG_RegisterGame  write;

} VNPV_r__VNG_RegisterGame;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VNG_RegisterGame  a_r;
    VNPV_r__VNG_RegisterGame  v_r;
    VNPA_rw_VNG_RegisterGame  a_rw;
    VNPA__w_VNG_RegisterGame  a__w;

} VNPIBuf_VNG_RegisterGame;


/****************************/

typedef struct {
    VNG          *vng;
    VNId          vnId;
    VNGTimeout    timeout;

} VNPA_r__VNG_UnregisterGame;  /* Read only args */


typedef struct {

} VNPA_rw_VNG_UnregisterGame;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode    rv;          /* Must be first */

} VNPA__w_VNG_UnregisterGame;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vng;          /* Non-optional reads */

} VNPRv_VNG_UnregisterGame;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VNG_UnregisterGame;  /* Write vector */


typedef struct {
    VNPRv_VNG_UnregisterGame  read;
    VNPWv_VNG_UnregisterGame  write;

} VNPV_r__VNG_UnregisterGame;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VNG_UnregisterGame  a_r;
    VNPV_r__VNG_UnregisterGame  v_r;
    VNPA_rw_VNG_UnregisterGame  a_rw;
    VNPA__w_VNG_UnregisterGame  a__w;

} VNPIBuf_VNG_UnregisterGame;


/****************************/

#if 0

typedef struct {
    VNG            *vng; 
    VNGTimeout      timeout;

} VNPA_r__VNG_ServeRPC;  /* Read only args */


typedef struct {

} VNPA_rw_VNG_ServeRPC;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode    rv;          /* Must be first */

} VNPA__w_VNG_ServeRPC;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vng;          /* Non-optional reads */

} VNPRv_VNG_ServeRPC;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VNG_ServeRPC;  /* Write vector */


typedef struct {
    VNPRv_VNG_ServeRPC  read;
    VNPWv_VNG_ServeRPC  write;

} VNPV_r__VNG_ServeRPC;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VNG_ServeRPC  a_r;
    VNPV_r__VNG_ServeRPC  v_r;
    VNPA_rw_VNG_ServeRPC  a_rw;
    VNPA__w_VNG_ServeRPC  a__w;

} VNPIBuf_VNG_ServeRPC;

#endif

/****************************/

/* VNG_GetBuddyList
 * VNG_InviteUser
 * VNG_AcceptUser
 * VNG_RejectUser
 * VNG_BlockUser
 * VNG_UnblockUser
 * VNG_RemoveUser
 * VNG_UpdateStatus
 * VNG_GetBuddyStatus
 * VNG_EnableTracking
 * VNG_DisableTracking
 */

/****************************/

typedef struct {
    VNG          *vng;
    VNId          vnId;
    uint32_t      gameStatus;
    uint32_t      numPlayers;
    VNGTimeout    timeout;

} VNPA_r__VNG_UpdateGameStatus;  /* Read only args */


typedef struct {

} VNPA_rw_VNG_UpdateGameStatus;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode    rv;          /* Must be first */

} VNPA__w_VNG_UpdateGameStatus;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vng;          /* Non-optional reads */

} VNPRv_VNG_UpdateGameStatus;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VNG_UpdateGameStatus;  /* Write vector */


typedef struct {
    VNPRv_VNG_UpdateGameStatus  read;
    VNPWv_VNG_UpdateGameStatus  write;

} VNPV_r__VNG_UpdateGameStatus;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VNG_UpdateGameStatus  a_r;
    VNPV_r__VNG_UpdateGameStatus  v_r;
    VNPA_rw_VNG_UpdateGameStatus  a_rw;
    VNPA__w_VNG_UpdateGameStatus  a__w;

} VNPIBuf_VNG_UpdateGameStatus;


/****************************/

typedef struct {
    VNG            *vng;
    VNGGameStatus  *gameStatus;
    uint32_t        nGameStatus;
    VNGTimeout      timeout;

} VNPA_r__VNG_GetGameStatus;  /* Read only args */


typedef struct {

} VNPA_rw_VNG_GetGameStatus;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode    rv;          /* Must be first */

} VNPA__w_VNG_GetGameStatus;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  gameStatus;   /* Optional read */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vng;          /* Non-optional reads */

} VNPRv_VNG_GetGameStatus;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
    IOSIoVector  gameStatus;   /* Optional writes last */

} VNPWv_VNG_GetGameStatus;  /* Write vector */


typedef struct {
    VNPRv_VNG_GetGameStatus  read;
    VNPWv_VNG_GetGameStatus  write;

} VNPV_r__VNG_GetGameStatus;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VNG_GetGameStatus  a_r;
    VNPV_r__VNG_GetGameStatus  v_r;
    VNPA_rw_VNG_GetGameStatus  a_rw;
    VNPA__w_VNG_GetGameStatus  a__w;

} VNPIBuf_VNG_GetGameStatus;


/****************************/

typedef struct {
    VNG          *vng;
    VNId          vnId;
    char         *comments;
    size_t       *commentSize;
    VNGTimeout    timeout;

} VNPA_r__VNG_GetGameComments;  /* Read only args */


typedef struct {
    size_t        commentSize;

} VNPA_rw_VNG_GetGameComments;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode    rv;          /* Must be first */

} VNPA__w_VNG_GetGameComments;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vng;          /* Non-optional reads */

} VNPRv_VNG_GetGameComments;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
    IOSIoVector  comments;     /* Optional writes last */

} VNPWv_VNG_GetGameComments;  /* Write vector */


typedef struct {
    VNPRv_VNG_GetGameComments  read;
    VNPWv_VNG_GetGameComments  write;

} VNPV_r__VNG_GetGameComments;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VNG_GetGameComments  a_r;
    VNPV_r__VNG_GetGameComments  v_r;
    VNPA_rw_VNG_GetGameComments  a_rw;
    VNPA__w_VNG_GetGameComments  a__w;

} VNPIBuf_VNG_GetGameComments;


/****************************/

typedef struct {
    VNG                *vng;
    VNGSearchCriteria  *searchCriteria;
    VNGSearchCriteria   searchCriteriaBuf;
    VNGGameStatus      *gameStatus;
    uint32_t            skipN;
    VNGTimeout          timeout;

} VNPA_r__VNG_SearchGames;  /* Read only args */


typedef struct {
    uint32_t            nGameStatus;

} VNPA_rw_VNG_SearchGames;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode    rv;          /* Must be first */

} VNPA__w_VNG_SearchGames;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vng;          /* Non-optional reads */

} VNPRv_VNG_SearchGames;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
    IOSIoVector  gameStatus;   /* Optional writes last */

} VNPWv_VNG_SearchGames;  /* Write vector */


typedef struct {
    VNPRv_VNG_SearchGames  read;
    VNPWv_VNG_SearchGames  write;

} VNPV_r__VNG_SearchGames;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VNG_SearchGames  a_r;
    VNPV_r__VNG_SearchGames  v_r;
    VNPA_rw_VNG_SearchGames  a_rw;
    VNPA__w_VNG_SearchGames  a__w;

} VNPIBuf_VNG_SearchGames;


/****************************/

typedef struct {
    VNG          *vng;
    const char   *serverName;
    VNGPort_32    serverPort;

} VNPA_r__VNG_AddServerToAdhocDomain;  /* Read only args */


typedef struct {

} VNPA_rw_VNG_AddServerToAdhocDomain;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode    rv;          /* Must be first */

} VNPA__w_VNG_AddServerToAdhocDomain;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vng;          /* Non-optional reads */
    IOSIoVector  serverName;

} VNPRv_VNG_AddServerToAdhocDomain;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VNG_AddServerToAdhocDomain;  /* Write vector */


typedef struct {
    VNPRv_VNG_AddServerToAdhocDomain  read;
    VNPWv_VNG_AddServerToAdhocDomain  write;

} VNPV_r__VNG_AddServerToAdhocDomain;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VNG_AddServerToAdhocDomain  a_r;
    VNPV_r__VNG_AddServerToAdhocDomain  v_r;
    VNPA_rw_VNG_AddServerToAdhocDomain  a_rw;
    VNPA__w_VNG_AddServerToAdhocDomain  a__w;

} VNPIBuf_VNG_AddServerToAdhocDomain;


/****************************/

typedef struct {
    VNG          *vng;

} VNPA_r__VNG_ResetAdhocDomain;  /* Read only args */


typedef struct {

} VNPA_rw_VNG_ResetAdhocDomain;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode    rv;          /* Must be first */

} VNPA__w_VNG_ResetAdhocDomain;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vng;          /* Non-optional reads */

} VNPRv_VNG_ResetAdhocDomain;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VNG_ResetAdhocDomain;  /* Write vector */


typedef struct {
    VNPRv_VNG_ResetAdhocDomain  read;
    VNPWv_VNG_ResetAdhocDomain  write;

} VNPV_r__VNG_ResetAdhocDomain;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VNG_ResetAdhocDomain  a_r;
    VNPV_r__VNG_ResetAdhocDomain  v_r;
    VNPA_rw_VNG_ResetAdhocDomain  a_rw;
    VNPA__w_VNG_ResetAdhocDomain  a__w;

} VNPIBuf_VNG_ResetAdhocDomain;


/****************************/


/* VNG_SubmitScore  
 * VNG_SubmitScoreObject 
 * VNG_GetGameScore 
 * VNG_GetGameScoreObject
 * VNG_DeleteGameScore
 * VNG_GetRankedScores
 * VNG_GetScoresItems
 * VNG_StoreMessage 
 * VNG_GetMessageList
 * VNG_RetrieveMessage
 * VNG_DeleteMessage 
 */


/****************************/

typedef struct {
    VNG     *vng;

} VNPA_r__VNG_ConnectStatus;  /* Read only args */


typedef struct {

} VNPA_rw_VNG_ConnectStatus;  /* Read/write arg buffer */


typedef struct {
    uint32_t      rv;          /* Must be first */

} VNPA__w_VNG_ConnectStatus;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vng;          /* Non-optional reads */

} VNPRv_VNG_ConnectStatus;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VNG_ConnectStatus;  /* Write vector */


typedef struct {
    VNPRv_VNG_ConnectStatus  read;
    VNPWv_VNG_ConnectStatus  write;

} VNPV_r__VNG_ConnectStatus;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VNG_ConnectStatus  a_r;
    VNPV_r__VNG_ConnectStatus  v_r;
    VNPA_rw_VNG_ConnectStatus  a_rw;
    VNPA__w_VNG_ConnectStatus  a__w;

} VNPIBuf_VNG_ConnectStatus;


/****************************/

typedef struct {
    VNG     *vng;

} VNPA_r__VNG_MyUserId;  /* Read only args */


typedef struct {

} VNPA_rw_VNG_MyUserId;  /* Read/write arg buffer */


typedef struct {
    VNGUserId     rv;          /* Must be first */

} VNPA__w_VNG_MyUserId;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vng;          /* Non-optional reads */

} VNPRv_VNG_MyUserId;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VNG_MyUserId;  /* Write vector */


typedef struct {
    VNPRv_VNG_MyUserId  read;
    VNPWv_VNG_MyUserId  write;

} VNPV_r__VNG_MyUserId;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VNG_MyUserId  a_r;
    VNPV_r__VNG_MyUserId  v_r;
    VNPA_rw_VNG_MyUserId  a_rw;
    VNPA__w_VNG_MyUserId  a__w;

} VNPIBuf_VNG_MyUserId;


/****************************/

typedef struct {
    VN     *vn;

} VNPA_r__VN_Self;  /* Read only args */


typedef struct {

} VNPA_rw_VN_Self;  /* Read/write arg buffer */


typedef struct {
    VNMember   rv;             /* Must be first */

} VNPA__w_VN_Self;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vn;           /* Non-optional reads */

} VNPRv_VN_Self;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VN_Self;  /* Write vector */


typedef struct {
    VNPRv_VN_Self  read;
    VNPWv_VN_Self  write;

} VNPV_r__VN_Self;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VN_Self  a_r;
    VNPV_r__VN_Self  v_r;
    VNPA_rw_VN_Self  a_rw;
    VNPA__w_VN_Self  a__w;

} VNPIBuf_VN_Self;


/****************************/

typedef struct {
    VN     *vn;

} VNPA_r__VN_Owner;  /* Read only args */


typedef struct {

} VNPA_rw_VN_Owner;  /* Read/write arg buffer */


typedef struct {
    VNMember   rv;             /* Must be first */

} VNPA__w_VN_Owner;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vn;           /* Non-optional reads */

} VNPRv_VN_Owner;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VN_Owner;  /* Write vector */


typedef struct {
    VNPRv_VN_Owner  read;
    VNPWv_VN_Owner  write;

} VNPV_r__VN_Owner;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VN_Owner  a_r;
    VNPV_r__VN_Owner  v_r;
    VNPA_rw_VN_Owner  a_rw;
    VNPA__w_VN_Owner  a__w;

} VNPIBuf_VN_Owner;


/****************************/

typedef struct {
    VN     *vn;

} VNPA_r__VN_State;  /* Read only args */


typedef struct {

} VNPA_rw_VN_State;  /* Read/write arg buffer */


typedef struct {
    VNState       rv;          /* Must be first */

} VNPA__w_VN_State;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vn;           /* Non-optional reads */

} VNPRv_VN_State;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VN_State;  /* Write vector */


typedef struct {
    VNPRv_VN_State  read;
    VNPWv_VN_State  write;

} VNPV_r__VN_State;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VN_State  a_r;
    VNPV_r__VN_State  v_r;
    VNPA_rw_VN_State  a_rw;
    VNPA__w_VN_State  a__w;

} VNPIBuf_VN_State;


/****************************/

typedef struct {
    VNG       *vng;
    VN        *vns;

} VNPA_r__VNG_GetVNs;  /* Read only args */


typedef struct {
    uint32_t   nVNs;

} VNPA_rw_VNG_GetVNs;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode    rv;          /* Must be first */

} VNPA__w_VNG_GetVNs;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vng;          /* Non-optional reads */

} VNPRv_VNG_GetVNs;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
    IOSIoVector  vns;          /* Optional writes last */

} VNPWv_VNG_GetVNs;  /* Write vector */


typedef struct {
    VNPRv_VNG_GetVNs  read;
    VNPWv_VNG_GetVNs  write;

} VNPV_r__VNG_GetVNs;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VNG_GetVNs  a_r;
    VNPV_r__VNG_GetVNs  v_r;
    VNPA_rw_VNG_GetVNs  a_rw;
    VNPA__w_VNG_GetVNs  a__w;

} VNPIBuf_VNG_GetVNs;


/****************************/

typedef struct {
    VNG        *vng;
    VNGTraceOpt trop;

} VNPA_r__VNG_SetTraceOpt;  /* Read only args */


typedef struct {

} VNPA_rw_VNG_SetTraceOpt;  /* Read/write arg buffer */


typedef struct {
    VNGErrCode    rv;          /* Must be first */

} VNPA__w_VNG_SetTraceOpt;  /* Write only arg buffer */


typedef struct {               /* Optional reads first */
    IOSIoVector  buf;          /* Points to VNPIBuf_<api> */
    IOSIoVector  vng;          /* Non-optional reads */

} VNPRv_VNG_SetTraceOpt;  /* Read vector  */


typedef struct {
    IOSIoVector  buf;          /* Points to VNPIBuf_<api>.a_rw */
                               /* Non-optional writes */
                               /* Optional writes last */

} VNPWv_VNG_SetTraceOpt;  /* Write vector */


typedef struct {
    VNPRv_VNG_SetTraceOpt  read;
    VNPWv_VNG_SetTraceOpt  write;

} VNPV_r__VNG_SetTraceOpt;  /* Ioctlv reads/writes vector */


typedef struct {
    VNPA_r__VNG_SetTraceOpt  a_r;
    VNPV_r__VNG_SetTraceOpt  v_r;
    VNPA_rw_VNG_SetTraceOpt  a_rw;
    VNPA__w_VNG_SetTraceOpt  a__w;

} VNPIBuf_VNG_SetTraceOpt;


/****************************/

#if !defined(_GBA) && !defined(_SC)
#pragma pack(pop)
#endif

#endif /* __VNP_H__ */

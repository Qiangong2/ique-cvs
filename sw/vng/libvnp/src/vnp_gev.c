//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//


#include "vnp_i.h"






//  _vng_gev_find_waiter ()
//
//  A vn event has been received that requires a game event to be issued.
//  We are checking if a thread is waiting for a game event.
//
//  gev is the new external VNGEvent
//
//  Game event waiters and pending game events are stored in singly linked fifo lists.
//  The lists are __vnp.gev_rcvd and __vnp.gev_waiters
//
//     check the gev waiters list for a thread waiting for a game event.
//     if found
//         remove head waiter from gev waiters list
//         remove waiter from timeout list
//         copy VNGEvent info to gev waiter
//         signal gev waiter _VNGEvent.ev.syncObj
//     else
//         insert a _VNGGameEvent into __vnp.gevs_rcvd with a timeout
//     return _VNG_OK or error code but not VNGERR_NOT_FOUND
//
VNGErrCode
_vng_gev_find_waiter (VNG       *vng,
                      VNGEvent   gev)
{
    VNGErrCode  ec = VNG_OK;
   _VNGGameEvent *waiter;

    waiter = (_VNGGameEvent*)
        _vng_evt_s_list_remove_head (vng, &__vnp.gev_waiters, true);

    if (waiter) {
        waiter->gev = gev;
        _vnp_sem_post (waiter->ev.syncObj);
    }
    else {
            int           vnEventSize = sizeof (_VNGGameEvent);
            VNGTimeout    evt_expiration = _VNG_EVT_GEV_EXPIRATION;
           _VNGGameEvent *evt;

            if (!(evt=(_VNGGameEvent*)_vng_vnEvent_new (vng, vnEventSize)))
                return VNGERR_NOMEM;

            _vng_vnEvent_init ((_VNGEvent*) evt, vnEventSize, NULL);

            evt->ev.timeout.time = _vng_to2time (vng, evt_expiration);

            evt->gev = gev;
            evt->vn_ev = &evt->buf.vn_ev;
            evt->vn_ev->tag = _VNG_EVT_GEV;
            evt->vn_ev->size = sizeof evt->buf;

            // _vng_evt_s_list_insert_end registers the timeout
            // A queued event with no waiter that times out will be deleted
            _vng_evt_s_list_insert_end (vng, (_VNGVnEvent*) evt, &__vnp.gevs_rcvd);
    }

    return ec; 
}


//
//  _vng_evt_gev_find_rcvd()
//
//  A VNG_GetEvent() has been called and we are
//  checking if a game event is already waiting
//
//  waiter is the waiter of the VNG_GetEvent().
//
//  Gev waiters and rcvd gevs are stored in singly linked fifo lists.
//  The lists are __vnp.gevs_rcvd and __vnp.gev_waiters
//
//    if found
//       remove head _VNGGameEvent from __vnp.gevs_rcvd
//       remove _VNGGameEvent from timeout queue
//       copy _VNGGameEvent->gev to waiter
//       free _VNGGameEvent
//       return VNG_OK
//    else
//      return VNGERR_NOT_FOUND
//
//    or return appropriate VNGErrCode
//

VNGErrCode
_vng_evt_gev_find_rcvd   (VNG            *vng,
                         _VNGGameEvent   *waiter)
{
    VNGErrCode  ec = VNGERR_NOT_FOUND;
   _VNGGameEvent *rcvd;

    rcvd = (_VNGGameEvent*)
                _vng_evt_s_list_remove_head (vng, &__vnp.gevs_rcvd, true);

    if (rcvd) {
        waiter->gev = rcvd->gev;
        _vng_vnEvent_free (vng, (_VNGEvent*) rcvd);
         ec = VNG_OK;
    }

    return ec;
}


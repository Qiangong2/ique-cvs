//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//


#include "vnp_i.h"



// __vnp.mutex must be owned on entry for all functions in this file
//



// _vng_vn2vng_time converts 64 bit vn timestamp to VNGTime

VNGTime _vng_vn2vng_time (VNG *vng, _VN_time_t vn_timestamp)
{
    return (VNGTime) (vn_timestamp - __vnp.base_vn_time);
}



// _vng_vng2vn_time converts VNGTime to 64 bit vn timestamp

_VN_time_t  _vng_vng2vn_time (VNG *vng, VNGTime vng_time)
{
    return vng_time + __vnp.base_vn_time;
}



// _vng_get_time returns the current VNGTime

VNGTime _vng_get_time (VNG *vng)
{
    return _vng_vn2vng_time (vng, _VN_get_timestamp());
}



// _vng_to2time converts a timeout value to a VNGTimeOfDay

VNGTimeOfDay _vng_to2time (VNG *vng, VNGTimeout timeout) {
    if (timeout == VNG_NOWAIT)
        return VNG_NOWAIT;
    else
        return _VN_get_timestamp() + timeout;
}





// _vng_next_to_time()
//
//      if no timeout registerd
//          returns 0
//      else
//          returns VNGTimeOfDay for timeout that should occur next

VNGTimeOfDay  _vng_next_to_time (VNG *vng)
{
    if (!__vnp.timeouts.first)
        return 0;
    else
        return __vnp.timeouts.first->ev.timeout.time;
}



// _vng_next_to()
//
//      if no timeout registerd
//          returns INT32_MAX
//      else
//          returns msec till timeout that should occur next
//
//  A short max timeout is not needed as _VN_cancel_get_events
//  is used to release the evt dispatch thread form _VN_get_events
//  so it can recalculate the next timeout.

VNGTimeout  _vng_next_to (VNG *vng)
{
    VNGTimeout   timeout = 0;

    VNGTimeOfDay to_time = _vng_next_to_time (vng);

    VNGTimeOfDay now  = _VN_get_timestamp();

    if (!to_time) {
        timeout = INT32_MAX;
    }
    else if (to_time > now) {
        timeout = (VNGTimeout)(to_time - now);
    }

    return timeout;
}



VNGErrCode  _vng_register_timeout (VNG *vng, _VNGEvent *vnEvent)
{
    VNGErrCode    ec = VNG_OK;
    _VNGEvent    *prev;
    _VNGEvent    *next;
    VNGTimeOfDay  to_time = vnEvent->ev.timeout.time;

    /* insertion sort into doubly linked list */

    if (__vnp.timeouts.last && __vnp.timeouts.last->ev.timeout.time <= to_time) {
        // append to end of list
        vnEvent->ev.timeout.next = NULL;
        vnEvent->ev.timeout.prev = __vnp.timeouts.last;
        __vnp.timeouts.last->ev.timeout.next = vnEvent;
        __vnp.timeouts.last = vnEvent;
        return ec;
    }

    prev = (_VNGEvent*) &__vnp.timeouts.first;

    while ((next=prev->ev.timeout.next) && to_time >= next->ev.timeout.time) {
        prev = prev->ev.timeout.next;
    }

    // insert after prev

    vnEvent->ev.timeout.next = next;
    vnEvent->ev.timeout.prev = prev;

    if (next)
        next->ev.timeout.prev = vnEvent;
    else
        __vnp.timeouts.last = vnEvent;

    prev->ev.timeout.next = vnEvent;

    if (__vnp.timeouts.first == vnEvent && !__vnp.dispatch_evts)
        _VN_cancel_get_events ();

    return ec;
}


VNGErrCode  _vng_check_timeouts (VNG *vng)
{
    VNGErrCode    ec    = VNG_OK;
    VNGTimeOfDay  now   = _VN_get_timestamp();
   _VNGEvent *first = (_VNGEvent *) &__vnp.timeouts.first;
   _VNGEvent *prev;
   _VNGEvent *next;
   _VNGEvent *toe;  // timed out event

    /* remove timeout events in list until event timeout is in future  */

    /* for each timed out event:
    *     remove from timeout list
    *     set err code to timeout
    *     if (has sync object)
    *         signal sync object
    *     else
    *         free the event (no one accepted it within expiration timeout)
    */
    for (prev = first;
         prev && (toe=prev->ev.timeout.next) && now >= toe->ev.timeout.time;
         prev = next) {

        // remove from timeout list

        next = toe->ev.timeout.next;
        prev->ev.timeout.next = next;

        if (next) {
            next->ev.timeout.prev = prev;
        }
        else {
            assert (__vnp.timeouts.last == toe);
            if (prev == first)
                __vnp.timeouts.last = NULL;
            else
                __vnp.timeouts.last = prev;
        }

        toe->ev.timeout.next = toe->ev.timeout.prev = NULL;

        toe->ev.ec = VNGERR_TIMEOUT;
        _vng_vnEvents_remove (vng, NULL, toe, false);

        if (toe->ev.syncObj)
            _vnp_sem_post (toe->ev.syncObj);
        else {
            ++__vnp.timeouts.num_expired;
            _vng_vnEvent_free (vng, toe);
        }
    }

    return ec;
}


VNGErrCode  _vng_remove_timeout (VNG *vng, _VNGEvent *vnEvent)
{
    VNGErrCode   ec = VNG_OK;
    _VNGEvent *first = (_VNGEvent *) &__vnp.timeouts.first;
    _VNGEvent *next  = vnEvent->ev.timeout.next;
    _VNGEvent *prev  = vnEvent->ev.timeout.prev;

    /* remove from doubly linked list */

    assert (vnEvent != first);

    if (vnEvent == first || prev == NULL)
        return VNGERR_INVALID_ARG;

    prev->ev.timeout.next = next;

    if (next) {
        next->ev.timeout.prev = prev;
    }
    else {
        assert (__vnp.timeouts.last == vnEvent);
        if (prev == first)
            __vnp.timeouts.last = NULL;
        else
            __vnp.timeouts.last = prev;
    }

    vnEvent->ev.timeout.next = vnEvent->ev.timeout.prev = NULL;

    return ec;
}



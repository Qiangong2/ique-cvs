//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//


#include "vnp_i.h"



// _vng_evt_conn_request
//
//  A vn connection request event has been received.
//
//  If the vn has policy VN_AUTO_ACCEPT_JOIN,
//
//     _VN_accept will be called
//      VNG_OK will be returned
//
//  Otherwise:
//
//  Check to see if a thread is waiting for a join requrest.
//
//  If not, a gev event will be created and the join request will be queued.
//
//  Join request waiters are kept in the singly linked fifo list
//      __vnp.jr.waiters.
//
//  Waiters for join requests for a specific vn indicate
//  the vn in waiter->vn_ev->addr.
//
//  Waiters for join requests to any VN indicate that by
//  setting wiater->vn_ev->addr to 0.
//
//     if can't determine vn
//          reject and return VNGERR_INVALID_VN
//
//     if vn has auto accept policy
//          accept and return VNG_OK
//
//     Check the jr waiters list for a waiter for ev->addr.
//
//     if not found,
//       check the jr waiters list for a waiter for
//       a join request to any vn.
//
//     if a waiter was found
//         remove head waiter from jr waiters list
//         remove waiter from timeout list
//         copy event info to jr waiter
//         signal jr waiter _VNGEvent.ev.syncObj
//         return _VNG_OK
//     else
//         insert a _VNGGameEvent into __vnp.gevs_rcvd with a timeout
//         return VNGERR_NOT_FOUND so the conn req ev will be put
//                in the __vnp.jr.received list by _vng_dispatch_evt().
//
VNGErrCode
_vng_evt_conn_request (VNG                         *vng,
                const _VN_connection_request_event *ev)
{
    VNGErrCode  ec;
   _VNGJoinRequestEvent *waiter;
   _VNGNetNode          *vn;
    uint32_t    policies;

    if (!(vn = _vng_addr2vn (ev->addr))) {
        if (ev->msglen) {
            _VN_get_event_msg (ev->msg_handle, NULL, 0); // discard msg
        }
       _VN_accept (ev->request_id, false, NULL, 0);
        return VNGERR_INVALID_VN;
    }

    if ((vn->policies & VN_AUTO_ACCEPT_JOIN)) {
       _VN_get_event_msg (ev->msg_handle, NULL, 0); // discard msg
        policies = htoel(vn->policies);
       _VN_accept (ev->request_id, true, &policies, sizeof policies);
        return VNG_OK;
    }

    waiter = (_VNGJoinRequestEvent*)
        _vng_evt_s_list_remove_by_ca_addr (vng, ev->addr,
                                           &__vnp.jr.waiters, true);
    if (!waiter) {
        // check for thread waiting for any vn
        waiter = (_VNGJoinRequestEvent*)
            _vng_evt_s_list_remove_by_ca_addr (vng, 0,
                                               &__vnp.jr.waiters, true);
    }

    if (waiter) {
        waiter->buf.vn_ev = *ev;
        waiter->vn_ev->event.next = NULL;
       _vnp_sem_post (waiter->ev.syncObj);
        ec = VNG_OK;
    }
    else {
        VNGEvent   gev;
       _VN_guid_t  guid;
        gev.eid = VNG_EVENT_JOIN_REQUEST;
        gev.evt.joinRequest.vnId.netId = _VN_addr2net(ev->addr);
        if (0 > _VN_get_guid(ev->addr, &guid)) {
            gev.evt.joinRequest.vnId.deviceId = _VN_CHIP_ID_INVALID;
        } else {
            gev.evt.joinRequest.vnId.deviceId = guidToDeviceId(guid);
        }

        if (gev.evt.joinRequest.vnId.deviceId == _VN_CHIP_ID_INVALID) {
            ec = VNGERR_INVALID_VN;
           _VN_get_event_msg (ev->msg_handle, NULL, 0); // discard msg
        }
        else if (!(ec = _vng_gev_find_waiter (vng, gev))) {
            ec = VNGERR_NOT_FOUND; // so jr will be queued
        }
    }

    return ec; 
}


//
//  _vng_evt_jr_find_rcvd()
//
//  A VNG_GetJoinRequest() has been called and we are
//  checking if a connection request is already waiting
//  before inserting the waiter in the jr waiters list.
//
//  waiter represents a thread that called VNG_GetJoinRequest().
//
//  Join requests are kept in the __vnp.jr.received list
//
//  If waiter is looking for a join request for any vn,
//      waiter->addr will be 0
//
//  If waiter is looking for a join request for only a specific vn,
//      addr will be the addr of the vn owner.
//
//  Check __vnp.jr.received for an existing join request
//
//    if existing join request found
//       remove head _VNGJoinRequestEvent from vn->jr.received
//       remove _VNGJoinRequestEvent from timeout queue
//       copy _VNGJoinRequestEvent to waiter
//       free _VNGJoinRequestEvent
//       return VNG_OK
//    else
//      return VNGERR_NOT_FOUND
//
//    or return appropriate VNGErrCode
//

VNGErrCode
_vng_evt_jr_find_rcvd (VNG                 *vng,
                      _VNGJoinRequestEvent *waiter)
{
    VNGErrCode           ec;
   _VNGJoinRequestEvent *rcvd;

    if (!waiter->vn_ev->addr) {  // if waiting for any join request
        rcvd = (_VNGJoinRequestEvent*)
            _vng_evt_s_list_remove_head (vng, &__vnp.jr.received, true);
    }
    else {
        rcvd = (_VNGJoinRequestEvent*)
            _vng_evt_s_list_remove_by_ca_addr (vng,
                                               waiter->vn_ev->addr,
                                               &__vnp.jr.received, true);
    }

    if (rcvd) {
        waiter->buf = rcvd->buf;
        _vng_vnEvent_free (vng, (_VNGEvent*) rcvd);
        ec = VNG_OK;
    }
    else {
        ec = VNGERR_NOT_FOUND;
    }

    return ec;
}


//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//


#include "vnp_i.h"
#include "vnp_ec.h"



// _vng_xec
// 
// Make sure err code is valid before returning
// from a public API.
//
VNGErrCode _vng_xec (VNGErrCode ec)
{
    if (ec && (ec < 0 || ec > VNGERR_MAX)) {
        assert (ec && (ec < 0 || ec > VNGERR_MAX));
        trace (ERR, API, "Invalid error code on API return: %d\n", ec);
        return VNGERR_UNKNOWN;
    }
    return ec;
}



VNGErrCode _vng_vn2vng_err (int vn_rv)
{
    VNGErrCode ec;

    if (vn_rv >= 0)
        return VNG_OK;

    //### DLE: TODO: Convert other VN error codes
    //               Think about which ones can be generic
    //               and which must be interpreted in context.

    switch (vn_rv) {
        case _VN_ERR_NOMEM:
            ec = VNGERR_NOMEM;
            break;

        case _VN_ERR_FULL:
        case _VN_ERR_NOSPACE:
            ec = VNGERR_BUF_FULL;
            break;

        case _VN_ERR_HOSTID:
            ec = VNGERR_INVALID_MEMB;
            break;

        case _VN_ERR_NOSUPPORT:
            ec = VNGERR_NOT_SUPPORTED;
            break;

        case _VN_ERR_SERVER:     // if see this here, don't know why
        case _VN_ERR_TIMEOUT:    // not the same as VNGERR_TIMEOUT
        case _VN_ERR_INVALID:
        default:
            trace (INFO, MISC, "VNGERR_UNKNOWN, VN error code: %d\n", vn_rv);
            ec = VNGERR_UNKNOWN; // most vn errs can only be interpreted in context
            break;
    }
    return ec;
}


// _VN_EVT_ERR to VNGErrCode
//
VNGErrCode _vng_evt2vng_err (int errcode)
{
    VNGErrCode ec;

    if (errcode >= 0)
        return VNG_OK;

    //### DLE: TODO: convert other VN error codes

    switch (errcode) {

        case _VN_ERR_REJECTED:
            ec = VNGERR_CONN_REFUSED;
            break;

        case _VN_ERR_SERVER:
        case _VN_ERR_TIMEOUT:    // not the same as VNGERR_TIMEOUT
            ec = VNGERR_UNREACHABLE;
            trace (INFO, EVD, "VNGERR_UNREACHABLE, VN error code: %d\n", errcode);
            break;

        default:
            ec = _vng_vn2vng_err (errcode);
            break;
    }
    return ec;
}


static _VNG_LOCALE  current_locale = _VNG_DEFAULT_LOCALE;


VNGErrCode  _vng_err_msg (VNGErrCode  errcode,
                          char       *msg,
                          size_t      msgLen)
{
    const char *res = NULL;
    int msgs_len = (sizeof _vng_err_codes)/(sizeof _vng_err_codes[0]);
    int i;

    const char  **msgs = _vng_msgs_by_locale [current_locale];

    if (!msg && msgLen)
        return VNGERR_INVALID_LEN;

    if (errcode > 0  && errcode < VNGERR_MAX) {

        for (i = 0;  i < msgs_len;  ++i) {
            if (_vng_err_codes[i] == errcode) {
                res = msgs[i];
                break;
            }
        }
    }

    if (res == NULL) {
        if (msgLen) {
            *msg = '\0';
        }
        return VNGERR_NOT_FOUND;
    }

    if (msgLen) {
        strncpy (msg, res, msgLen);
        if (msgLen == strnlen(res, msgLen)) {
            msg[msgLen-1] = '\0';
            return VNGERR_INVALID_LEN;
        }
    }

    return VNG_OK;
}




/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#include <sc/sc.h>
#include <sc/ios.h>
#include <ioslibc.h>
#include "vnp_i.h"
#include "vnp.h"
#include "vnp_rm_jt.h"



/*  This is an NC only file
 *
 *  The VNG public APIs that are implemented in the VN process
 *  are referred to as the VN process (i.e VNP) public APIs or
 *  for brevity, the VNP APIs
 *
 *  All VNP public APIs are accessed through the VNP Resource Manager
 *  via the kernel call IOS_Ioctlv(fd, cmd, in, insize, out, outsize).
 *
 *  See more comments on the implementation of the interface to
 *  VNP APIs via the VNP RM in the libvnp wrapper file vnp_rm_api.c
 * 
 */


#define VNP_NUM_RMQ_THREADS          4
#define VNP_RMQ_THREAD_STACK_SIZE   (16*1024)

const u8  _initStack [VNP_RMQ_THREAD_STACK_SIZE];
const u32 _initStackSize = sizeof(_initStack);
const u32 _initPriority = _VNG_API_THREAD_PRIORITY + 1;

/* additional stacks for other than initial thread */
static const u8  __addStacks [VNP_NUM_RMQ_THREADS - 1] [VNP_RMQ_THREAD_STACK_SIZE];


typedef struct {
    u32          ti;  /* index in __rmqThread */
    IOSThreadId  tid;
   _VNPSemaphore sem;

} VNPRMQThread;

static VNPRMQThread __rmqThread [VNP_NUM_RMQ_THREADS];
static u32          __numRMQThreads;
static volatile u32 __busyMask;

IOSMessageQueueId  __vnpRMQId = IOS_ERROR_NOEXISTS;

static IOSThreadId
createRMQThread (IOSMessageQueueId mq);



static IOSError
vnpOpen(IOSResourceRequest *req)
{
    IOSError          rv = IOS_ERROR_OK;
    IOSResourceOpen  *args = &req->args.open;
   _VNPSession       *s;

    trace (FINE, RM, "VNP RM in vnpOpen: '%s', u=%d, g=%d\n", args->path, args->uid, args->gid);

    if (strncmp(args->path, VNP_DEV, sizeof(VNP_DEV)-1)) {
        rv = IOS_ERROR_NOEXISTS;
        goto end;
    }

    s = _vnp_alloc_session (args->uid, args->gid, args->flags);

    if (!s) {
        rv = IOS_ERROR_MAX;
        goto end;
    }
    rv = (IOSError) s;

end:
    return rv;
}


static IOSError
vnpClose(IOSResourceRequest *req)
{
    IOSError rv = IOS_ERROR_OK;
   _VNPSession *s = (_VNPSession *) req->handle;

    trace (FINE, RM, "VNP RM in vnpClose\n");

    if (_vnp_release_session (s)) {
        rv = IOS_ERROR_INVALID;
    }

    return rv;
}




static IOSError
vnpIoctlv(IOSResourceRequest *req, IOSMessageQueueId mq)
{
    IOSError           rv = IOS_ERROR_OK;
   _VNPSession        *s = (_VNPSession *) req->handle;
    IOSResourceIoctlv *ioctlv = (IOSResourceIoctlv *) &(req->args.ioctlv);
    VNPRmApiWrapper    funcWrapper;

    trace (FINEST, RM, "VNP RM in vnpIoctlv\n");

    if (!s->inUse ||
            ((ioctlv->readCount || ioctlv->writeCount) &&
                 !ioctlv->vector)) {

        rv = IOS_ERROR_INVALID;
        goto end;
    }

    funcWrapper = _vnp_rm_getApiWrapper (ioctlv->cmd);
    trace (FINER, RM, "VNP RM vnpIoctlv Just before call API %d\n", ioctlv->cmd);
    rv = funcWrapper (ioctlv, s);
    trace (FINER, RM, "VNP RM vnpIoctlv API %d returned %d\n", ioctlv->cmd, rv);

end:
    return rv;
}



static VNPRMQThread*
vnpGetRMQThread ()
{
    VNPRMQThread *th = NULL;
    IOSThreadId   tid = IOS_GetThreadId ();
    u32           i;

    for (i = 0; i < __numRMQThreads; ++i) {
        if (__rmqThread[i].tid == tid) {
            th = &__rmqThread[i];
            break;
        }
    }

    return th;
}


static IOSError
waitForRMCmds (IOSMessageQueueId mq)
{
    IOSError       rv;
    IOSMessage     msg;
    VNPRMQThread  *th = vnpGetRMQThread();

    if (!th) {
        rv = IOS_ERROR_UNKNOWN;
        goto end;
    }

    while (1) {

        trace (FINEST, RM, "VNP RMQ thread %d waiting for commands. "
                           "busyMask 0x%08x\n", th->ti, __busyMask);

        __busyMask &= ~(1 << th->ti); /* __busyMask is for debug, so no lock */

        if ((rv = IOS_ReceiveMessage (mq, &msg, IOS_MESSAGE_BLOCK)) != IOS_ERROR_OK) {
            trace (ERR, RM, "VNP RM IOS_ReceiveMessage retruned %d\n", rv);
            goto end;
        }

        if (!(th = vnpGetRMQThread())) {
            rv = IOS_ERROR_UNKNOWN;
            goto end;
        }

        __busyMask |= (1 << th->ti);

        if (msg == VNP_RMQ_PING_CMD) {
            trace (FINEST, RM,
                    "RMQ Thead %d busyMask 0x%08x\n", th->ti, __busyMask);
            continue;
        } else  {
            trace (FINEST, RM,
                    "VNP RMQ Thread %d received request 0x%08x.  "
                    "busyMask 0x%08x\n", th->ti, msg, __busyMask);
        }

        IOSResourceRequest *req = (IOSResourceRequest*) msg;
        switch (req->cmd) {
            case IOS_OPEN:
                rv = vnpOpen (req);
                break;
            case IOS_CLOSE:
                rv = vnpClose (req);
                break;
            case IOS_IOCTLV:
                rv = vnpIoctlv (req, mq);
                break;
            default:
                trace (ERR, RM, "VNP RM cmd %u not supported\n", req->cmd);
                rv = IOS_ERROR_INVALID;
                break;
        }
        trace (FINEST, RM, "VNP RM Returning %d\n", rv);
        IOS_ResourceReply(req, rv);
    }


end:
    return rv;
}



_VNPSemaphore* vnpGetRMQThreadSem ()
{
    /* Must have __vnp_mutex locked on entry */

    u32            i;
    IOSThreadId    tid  = IOS_GetThreadId ();
   _VNPSemaphore  *sem = NULL;
   _SHRError       twRv;

    for (i = 0; i < __numRMQThreads; ++i) {
        if (__rmqThread[i].tid == tid) {
            break;
        }
    }
    if (i < __numRMQThreads) {
        sem = &__rmqThread[i].sem;
        if (!sem) {
            trace (ERR, RM,
                   "vnpGetRMQThreadSem: _rmqThread[%d].sem is NULL\n", i);
        } else {
            do {
                twRv = _SHR_sem_trywait(sem);
            } while (twRv == _SHR_ERR_OK);

            if (twRv != _SHR_ERR_EAGAIN) {
                sem = NULL;
            }
        }
    }

    return sem;
}


static IOSError
vnpInitRMQThreadSem (VNPRMQThread *th)
{
    /*   Need __vnp_mutex locked except during
     *   thread creation/initialization by RM main.
     */

    IOSError rv;

    if (0 > (rv = _vnp_sem_init (&th->sem))) {
        trace (ERR, RM, "createRMQThread : _vnp_sem_init returned %d\n", rv);
        if (rv == _SHR_ERR_MAX) {
            rv = IOS_ERROR_MAX;
        } else {
            rv = IOS_ERROR_UNKNOWN;
        }
    }

    return rv;
}



static void 
rmqThreadEntryProc (u32 arg)
{    
    waitForRMCmds ((IOSMessageQueueId) arg);
}



/* createRMQThread() is only called from RM main
 * to create the RMQ threads.  Since the initial
 * thread priority is greater than the priority
 * of the created thread, no lock is needed.
 *
 * __numRMQThreads must be at least 1 on entry
*/
static IOSThreadId
createRMQThread (IOSMessageQueueId mq)
{
    IOSError       rv;
    IOSThreadId    tid;
    VNPRMQThread  *th;

    if (__numRMQThreads == VNP_NUM_RMQ_THREADS) {
        rv = IOS_ERROR_MAX;
        goto end;
    }

    tid = IOS_CreateThread ((IOSEntryProc) rmqThreadEntryProc,
                            (void*)mq,
                            (void*)__addStacks[__numRMQThreads],
                            VNP_RMQ_THREAD_STACK_SIZE,
                            _VNG_API_THREAD_PRIORITY,
                            IOS_THREAD_CREATE_DETACHED); 

    if (tid < 0) {
        trace (ERR, RM, "VNP RM IOS_CreateThread returns %d\n", tid);
        rv = tid;
        goto end;
    }

    if (0 > (rv = IOS_StartThread(tid))) {
        trace (ERR, RM, "VNP RM IOS_StartThread returns %d\n", rv);
        goto end;
    }

    th = &__rmqThread[__numRMQThreads];
    th->ti   = __numRMQThreads;
    th->tid  = tid;

    if ((rv = vnpInitRMQThreadSem (th))) {
        goto end;
    }

    __numRMQThreads++;
    __busyMask |= (1 << th->ti);

    rv = tid;

end:
    return rv;
}





int main(void)
{
    IOSError           rv;
    IOSMessageQueueId  mq;
    IOSMessage         msgArray[8];
    IOSThreadId        tid;
    u32                i;

    trace (INFO, RM, "VNP RM starting\n");

    if ((_vnp_init_global_mutex())) {
        rv = IOS_ERROR_UNKNOWN;
        goto end;
    }

    if ((mq = IOS_CreateMessageQueue (msgArray,
                                      sizeof(msgArray)/sizeof(msgArray[0])
                                      )) < 0) {
        rv = mq;
        trace (ERR, RM, "VNP RM IOS_CreateMessageQueue for resource manager "
                        "returned %d\n", rv);
        goto end;
    }

    if ((rv = IOS_RegisterResourceManager (VNP_DEV, mq)) != IOS_ERROR_OK) {
        trace (ERR, RM, "VNP RM IOS_RegisterResourceManager returned %d\n", rv);
        goto end;
    }

    __vnpRMQId = mq;

    __rmqThread[0].ti     = 0;
    __rmqThread[0].tid    = IOS_GetThreadId ();

    if ((rv = vnpInitRMQThreadSem (&__rmqThread[0]))) {
        goto end;
    }
    __numRMQThreads = 1;
    __busyMask = 0x00000001;

    for (i = 1;  i < VNP_NUM_RMQ_THREADS;  ++i) {
        tid = createRMQThread (mq);
        if (tid < 0) {
            rv = tid;
            goto end;
        }
    }

    /* Initial thread is started with higher priority
     * than RMQ threads started above.  After other RMQ threads
     * have been started, lower priority of this thread and
     * use it as an RMQ thread.
     */
    if (0>(rv = IOS_SetThreadPriority(__rmqThread[0].tid, _VNG_API_THREAD_PRIORITY))) {
        trace (ERR, RM, "VNP RM IOS_SetThreadPriority returned %d\n", rv);
        goto end;
    }

    rv = waitForRMCmds (mq);

end:
    /* should never get here */
    trace (ERR, RM, "VNP RM exiting with error %d\n", rv);

    return rv;
}

/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#include "vnp_rm_jt.h"
#include "vnp_i.h"
#include "vnp.h"


/* This is an NC only file
 *
 * The VNG public APIs that are implemented in the VN process
 * are referred to as the VN process (i.e VNP) public APIs or
 * for brevity, the VNP APIs
 *
 * All VNP public APIs are accessed through the VNP Resource Manager
 * via the kernel call IOS_Ioctlv(fd, cmd, in, insize, out, outsize).
 *
 * See more comments on the implementation of the interface to
 * VNP APIs via the VNP RM in the libvnp wrapper file vnp_rm_api.c
 * 
 */



#define WRAP_FUNC(func_name, func_call) \
static IOSError \
F_##func_name (IOSResourceIoctlv *ioctlv, _VNPSession *s) \
{ \
    VNPIBuf_##func_name  *b; \
    VNPA_r__##func_name  *a; \
    u32 bufIndex; \
    u32 maxReads = sizeof(VNPRv_##func_name)/sizeof(IOSIoVector); \
    u32 maxIBufIndex = offsetof(VNPRv_##func_name, buf)/sizeof(IOSIoVector); \
\
    if ( maxReads < ioctlv->readCount || \
        (maxReads - ioctlv->readCount) > maxIBufIndex ) { \
\
         return IOS_ERROR_INVALID; \
    } \
\
    bufIndex = maxIBufIndex - (maxReads -ioctlv->readCount); \
\
    if (ioctlv->vector[bufIndex].length != \
                  (sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw)) { \
        return IOS_ERROR_INVALID; \
    } \
\
    b = (VNPIBuf_##func_name*) ioctlv->vector[bufIndex].base; \
    a = &b->a_r; \
\
    b->a__w.rv = func_call; \
\
    return IOS_ERROR_OK; \
}


/*
 *  A few examples of expanded WRAP_FUNC() macros
 *
 *  WRAP_FUNC (VNG_Init,
 *            _vnp_init (a->vng, s, a->parm, NULL, NULL, 0))
 *  
 *  Expands to:
 *
 *  static IOSError
 *  F_VNG_Init (IOSResourceIoctlv *ioctlv, _VNPSession *s)
 *  {
 *      VNPIBuf_VNG_Init  *b;
 *      VNPA_r__VNG_Init  *a;
 *      u32 bufIndex;
 *      u32 maxReads = sizeof(VNPRv_VNG_Init)/sizeof(IOSIoVector);
 *      u32 maxIBufIndex = offsetof(VNPRv_VNG_Init, buf)/sizeof(IOSIoVector);
 *
 *      if ( maxReads < ioctlv->readCount ||
 *          (maxReads - ioctlv->readCount) > maxIBufIndex ) {
 *
 *           return IOS_ERROR_INVALID;
 *      }
 *
 *      bufIndex = maxIBufIndex - (maxReads -ioctlv->readCount);
 *
 *      if (ioctlv->vector[bufIndex].length !=
 *                    (sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw)) {
 *          return IOS_ERROR_INVALID;
 *      }
 *
 *      b = (VNPIBuf_VNG_Init*) ioctlv->vector[bufIndex].base;
 *      a = &b->a_r;
 *
 *      b->a__w.rv = _vnp_init (a->vng, s, a->parm, NULL, NULL, 0);
 *
 *      return IOS_ERROR_OK;
 *  }
 *  
 *  
 *  WRAP_FUNC (VN_SendRPC,
 *             VN_SendRPC (a->vn, a->memb, a->serviceTag,
 *                         a->args, a->argsLen,
 *                         a->ret, &b->a_rw.retLen,
 *                         &b->a_rw.optData, a->timeout))
 *
 *  Expands to:
 *
 *  static IOSError
 *  F_VN_SendRPC (IOSResourceIoctlv *ioctlv, _VNPSession *s)
 *  {
 *      VNPIBuf_VN_SendRPC  *b;
 *      VNPA_r__VN_SendRPC  *a;
 *      u32 bufIndex;
 *      u33 maxReads = sizeof(VNPRv_VN_SendRPC)/sizeof(IOSIoVector);
 *      u32 maxIBufIndex = offsetof(VNPRv_VN_SendRPC, buf)/sizeof(IOSIoVector);
 * 
 *      if ( maxReads < ioctlv->readCount ||
 *          (maxReads - ioctlv->readCount) > maxIBufIndex ) {
 * 
 *           return IOS_ERROR_INVALID;
 *      }
 * 
 *      bufIndex = maxIBufIndex - (maxReads -ioctlv->readCount);
 * 
 *      if (ioctlv->vector[bufIndex].length !=
 *                    (sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw)) {
 *          return IOS_ERROR_INVALID;
 *      }
 * 
 *      b = (VNPIBuf_VN_SendRPC*) ioctlv->vector[bufIndex].base;
 *      a = &b->a_r;
 * 
 *      b->a__w.rv = VN_SendRPC (a->vn, a->memb, a->serviceTag,
 *                               a->args, a->argsLen,
 *                               a->ret, &b->a_rw.retLen,
 *                               &b->a_rw.optData, a->timeout))
 * 
 *      return IOS_ERROR_OK;
 *
 *  }
 *
 *
 */


WRAP_FUNC (VNG_Init,
          _vnp_init (a->vng, s, a->parm, NULL, NULL, 0))

WRAP_FUNC (VNG_Fini,
           VNG_Fini (a->vng))

WRAP_FUNC (VNG_Login,
           VNG_Login (a->vng, a->serverName, a->serverPort,
                      a->loginName, a->passwd, a->timeout))

WRAP_FUNC (VNG_Logout,
           VNG_Logout (a->vng, a->timeout))

WRAP_FUNC (VNG_GetLoginType,
           VNG_GetLoginType (a->vng))

WRAP_FUNC (VNG_GetUserInfo,
           VNG_GetUserInfo (a->vng, a->uid, &b->a__w.uinfo, a->timeout))

WRAP_FUNC (VNG_NewVN,
           VNG_NewVN (a->vng, a->vn,
                      a->vnClass, a->domain, a->policies))

WRAP_FUNC (VNG_DeleteVN,
           VNG_DeleteVN (a->vng, a->vn))

WRAP_FUNC (VNG_JoinVN,
           VNG_JoinVN (a->vng, a->vnId,
                       a->msg, a->vn, a->denyReason,
                       a->denyReasonLen, a->timeout))

WRAP_FUNC (VNG_GetJoinRequest,
           VNG_GetJoinRequest (a->vng, a->vnId, a->vn,
                               a->requestId, a->userInfo,
                               a->joinReason, a->joinReasonLen,
                               a->timeout))

WRAP_FUNC (VNG_AcceptJoinRequest,
           VNG_AcceptJoinRequest (a->vng, a->requestId))

WRAP_FUNC (VNG_RejectJoinRequest,
           VNG_RejectJoinRequest (a->vng, a->requestId, a->reason))

WRAP_FUNC (VNG_LeaveVN,
           VNG_LeaveVN (a->vng, a->vn))

WRAP_FUNC (VN_NumMembers,
           VN_NumMembers (a->vn))

WRAP_FUNC (VN_GetMembers,
           VN_GetMembers (a->vn, a->members, a->nMembers))

WRAP_FUNC (VN_MemberState,
           VN_MemberState (a->vn, a->memb))

WRAP_FUNC (VN_MemberUserId,
           VN_MemberUserId (a->vn, a->memb, a->timeout))

WRAP_FUNC (VN_MemberLatency,
           VN_MemberLatency (a->vn, a->memb))

WRAP_FUNC (VN_MemberDeviceType,
           VN_MemberDeviceType (a->vn, a->memb))

WRAP_FUNC (VN_GetVNId,
           VN_GetVNId (a->vn, a->vnId))

WRAP_FUNC (VN_Select,
           VN_Select (a->vn, a->selectFrom, a->ready, a->memb, a->timeout))

WRAP_FUNC (VN_SendMsg,
           VN_SendMsg (a->vn, a->memb, a->serviceTag,
                       a->msg, a->msglen, a->attr, a->timeout))

WRAP_FUNC (VN_RecvMsg,
           VN_RecvMsg (a->vn, a->memb, a->serviceTag,
                       a->msg, &b->a_rw.msglen, a->hdr, a->timeout))

WRAP_FUNC (VN_SendReq,
           VN_SendReq (a->vn, a->memb, a->serviceTag, &b->a__w.callerTag,
                       a->msg, a->msglen, a->optData, a->timeout))

WRAP_FUNC (VN_RecvReq,
           VN_RecvReq (a->vn, a->memb, a->serviceTag,
                       a->msg, &b->a_rw.msglen, a->hdr, a->timeout))

WRAP_FUNC (VN_SendResp,
           VN_SendResp (a->vn, a->memb, a->serviceTag, a->callerTag,
                        a->msg, a->msglen, a->optData, a->timeout))

WRAP_FUNC (VN_RecvResp,
           VN_RecvResp (a->vn, a->memb, a->serviceTag, a->callerTag,
                        a->msg, &b->a_rw.msglen, a->hdr, a->timeout))

WRAP_FUNC (VN_RegisterRPCService,
           VN_RegisterRPCService (a->vn, a->procId, a->proc))

WRAP_FUNC (VN_UnregisterRPCService,
           VN_UnregisterRPCService (a->vn, a->procId))

WRAP_FUNC (VN_SendRPC,
           VN_SendRPC (a->vn, a->memb, a->serviceTag,
                       a->args, a->argsLen,
                       a->ret, &b->a_rw.retLen,
                       &b->a_rw.optData, a->timeout))

WRAP_FUNC (VNG_RecvRPC,
           VNG_RecvRPC (a->vng,
                        &b->a__w.proc, &b->a__w.vn, &b->a__w.hdr,
                        a->args, &b->a_rw.argsLen, a->timeout))

WRAP_FUNC (VNG_GetEvent,
           VNG_GetEvent (a->vng, &b->a__w.event, a->timeout))

WRAP_FUNC (VNG_SendNotification,
           VNG_SendNotification (a->vng,
                       a->uid, a->nUsers, a->message, a->messageLen))

WRAP_FUNC (VNG_RecvNotification,
           VNG_RecvNotification (a->vng, &b->a__w.userInfo,
                              a->message, &b->a_rw.messageLen, a->timeout))

WRAP_FUNC (VNG_RegisterGame,
           VNG_RegisterGame (a->vng, &a->info, a->comments, a->timeout))

WRAP_FUNC (VNG_UnregisterGame,
           VNG_UnregisterGame (a->vng, a->vnId, a->timeout))

WRAP_FUNC (VNG_UpdateGameStatus,
           VNG_UpdateGameStatus (a->vng, a->vnId, a->gameStatus,
                                 a->numPlayers, a->timeout))

WRAP_FUNC (VNG_GetGameStatus,
           VNG_GetGameStatus (a->vng, a->gameStatus, a->nGameStatus,
                              a->timeout))

WRAP_FUNC (VNG_GetGameComments,
           VNG_GetGameComments (a->vng, a->vnId,
                                a->comments, a->commentSize,
                                a->timeout))

WRAP_FUNC (VNG_SearchGames,
           VNG_SearchGames (a->vng, a->searchCriteria,
                            a->gameStatus, &b->a_rw.nGameStatus, a->skipN,
                            a->timeout))

WRAP_FUNC (VNG_AddServerToAdhocDomain,
           VNG_AddServerToAdhocDomain (a->vng, a->serverName, a->serverPort))

WRAP_FUNC (VNG_ResetAdhocDomain,
           VNG_ResetAdhocDomain (a->vng))

WRAP_FUNC (VNG_ConnectStatus,
           VNG_ConnectStatus (a->vng))

WRAP_FUNC (VNG_MyUserId,
           VNG_MyUserId (a->vng))

WRAP_FUNC (VN_Self,
           VN_Self (a->vn))

WRAP_FUNC (VN_Owner,
           VN_Owner (a->vn))

WRAP_FUNC (VN_State,
           VN_State (a->vn))

WRAP_FUNC (VNG_GetVNs,
           VNG_GetVNs (a->vng, a->vns, &b->a_rw.nVNs))

WRAP_FUNC (VNG_SetTraceOpt,
           VNG_SetTraceOpt (a->vng, a->trop))

WRAP_FUNC (VNG_Register,
           VNG_Register (a->vng, a->serverName, a->serverPort,
                         b->a__w.loginName, b->a__w.passwd,
                         a->timeout))

WRAP_FUNC (VNG_RemoveVNMember,
           VNG_RemoveVNMember (a->vng, a->vn, a->memb))





static
VNPRmApiWrapper __funcWrapper [(VNP_RM_FUNC_ID_MAX + 1) - VNP_RM_FUNC_ID_MIN] = {

/*  Wrapper Func                       Func ID */
/*  __________________________         _______ */
    F_VNG_Init,                     /*  1001   */
    F_VNG_Fini,                     /*  1002   */
    F_VNG_Login,                    /*  1003   */
    F_VNG_Logout,                   /*  1004   */
    F_VNG_GetLoginType,             /*  1005   */
    F_VNG_GetUserInfo,              /*  1006   */
    F_VNG_NewVN,                    /*  1007   */
    F_VNG_DeleteVN,                 /*  1008   */
    F_VNG_JoinVN,                   /*  1009   */
    F_VNG_GetJoinRequest,           /*  1010   */
    F_VNG_AcceptJoinRequest,        /*  1011   */
    F_VNG_RejectJoinRequest,        /*  1012   */
    F_VNG_LeaveVN,                  /*  1013   */
    F_VN_NumMembers,                /*  1014   */
    F_VN_GetMembers,                /*  1015   */
    F_VN_MemberState,               /*  1016   */
    F_VN_MemberUserId,              /*  1017   */
    F_VN_MemberLatency,             /*  1018   */
    F_VN_MemberDeviceType,          /*  1019   */
    F_VN_GetVNId,                   /*  1020   */
    F_VN_Select,                    /*  1021   */
    F_VN_SendMsg,                   /*  1022   */
    F_VN_RecvMsg,                   /*  1023   */
    F_VN_SendReq,                   /*  1024   */
    F_VN_RecvReq,                   /*  1025   */
    F_VN_SendResp,                  /*  1026   */
    F_VN_RecvResp,                  /*  1027   */
    F_VN_RegisterRPCService,        /*  1028   */
    F_VN_UnregisterRPCService,      /*  1029   */
    F_VN_SendRPC,                   /*  1030   */
    F_VNG_RecvRPC,                  /*  1031   */
    F_VNG_GetEvent,                 /*  1032   */
    F_VNG_SendNotification,         /*  1033   */
    F_VNG_RecvNotification,         /*  1034   */
    F_VNG_RegisterGame,             /*  1035   */
    F_VNG_UnregisterGame,           /*  1036   */
    F_VNG_UpdateGameStatus,         /*  1037   */
    F_VNG_GetGameStatus,            /*  1038   */
    F_VNG_GetGameComments,          /*  1039   */
    F_VNG_SearchGames,              /*  1040   */
    F_VNG_AddServerToAdhocDomain,   /*  1041   */
    F_VNG_ResetAdhocDomain,         /*  1042   */
    F_VNG_ConnectStatus,            /*  1043   */
    F_VNG_MyUserId,                 /*  1044   */
    F_VN_Self,                      /*  1045   */
    F_VN_Owner,                     /*  1046   */
    F_VN_State,                     /*  1047   */
    F_VNG_GetVNs,                   /*  1048   */
    F_VNG_SetTraceOpt,              /*  1049   */
    F_VNG_Register,                 /*  1050   */
    F_VNG_RemoveVNMember,           /*  1051   */
};


VNPRmApiWrapper  _vnp_rm_getApiWrapper (u32 func_id)
{
    VNPRmApiWrapper rv = NULL;

    if (func_id >= VNP_RM_FUNC_ID_MIN && func_id <= VNP_RM_FUNC_ID_MAX) {
        rv = __funcWrapper [func_id - VNP_RM_FUNC_ID_MIN ];
    }

    return rv;
}





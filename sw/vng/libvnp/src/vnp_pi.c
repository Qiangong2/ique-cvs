//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//


#include "vnp_i.h"



// _vng_evt_msg_find_notify_waiter
//
//  A msg with _VNG_NOTIFY received.
//
//  Check to see if a thread is waiting for a player invite.
//
//  If not, a gev event will be created and the player invite will be queued.
//
//  Player invite waiters are kept in the singly linked fifo list
//      __vnp.pi.waiters.
//
//  Waiters for player invites do not specify any filter criteria and will
//  receive any player invite.
//
//     Check the pi waiters list for any waiter.
//
//     if a waiter was found
//         remove head waiter from pi waiters list
//         remove waiter from timeout list
//         copy event info to pi waiter
//         signal pi waiter _VNGEvent.ev.syncObj
//         return VNG_OK
//     else
//         insert the player invite msg event in __vnp.pi.received.
//         make a VNGEvent with eid = VNG_EVENT_RECV_NOTIFY 
//         if there is a gev waiter
//              pass the VNGEvent to the waiter
//         else
//              put the VNGEvent in the _VNGGameEvent queue with timeout
//         return _VNG_OK so the msg won't be queued by _vng_dispatch_evt.
//
VNGErrCode
_vng_evt_msg_find_notify_waiter (VNG                   *vng,
                                 const _VN_new_message_event  *ev)
{
   _VNGNotifyWaiter *waiter;
    VNGEvent              gev;

    trace (FINE, EVD,
           "Player Invite msg rcvd  "
           "netid 0x%08x  from %u  to %u  port %u\n",
           ev->netid, ev->fromhost, ev->tohost, ev->port);

    waiter = (_VNGNotifyWaiter*)
                _vng_evt_s_list_remove_head (vng, &__vnp.pi.waiters, true);

    if (waiter) {
        assert (ev->event.size <=
                        sizeof *waiter - offsetof(_VNGNotifyWaiter,buf));
        memcpy (waiter->vn_ev, ev,
                MIN(ev->event.size,
                    sizeof *waiter - offsetof(_VNGNotifyWaiter,buf)));
        waiter->vn_ev->event.next = NULL;
       _vnp_sem_post (waiter->ev.syncObj);
    }
    else {
        int  vnEventSize = sizeof (_VNGNotifyRcvd) + ev->event.size;
        VNGTimeout  evt_expiration = _VNG_EVT_GEV_EXPIRATION;
       _VNGNotifyRcvd  *rcvd;

        if (!(rcvd=(_VNGNotifyRcvd*)
                        _vng_vnEvent_new (vng, vnEventSize)))
            return VNGERR_NOMEM;

        _vng_vnEvent_init ((_VNGEvent*) rcvd, vnEventSize, &ev->event);

        rcvd->ev.timeout.time = _vng_to2time (vng, evt_expiration);

        rcvd->vn_ev->event.tag = _VNG_EVT_NOTIFY;

        // insert the player invite msg event in __vnp.pi.received 
        // and register an expiration timeout
        _vng_evt_s_list_insert_end (vng, (_VNGVnEvent*) rcvd, &__vnp.pi.received);

        // pass a VNGEvent to a waiter or queue it with expiration timeout
        gev.eid = VNG_EVENT_RECV_NOTIFY;
        _vng_gev_find_waiter (vng, gev);
    }

    return VNG_OK; 
}


//
//  _vng_evt_pi_find_rcvd()
//
//  A VNG_GetInvitation() has been called and we are
//  checking if a player invite is already waiting
//  before inserting the waiter in the pi waiters list.
//
//  waiter represents a thread that called VNG_GetInvitation().
//
//  Player invites are kept in the __vnp.pi.received list
//
//  Check __vnp.pi.received for an existing player invite
//
//    if existing player invite found
//       remove head _VNGNotifyRcvd from vn->pi.received
//       remove _VNGNotifyRcvd from timeout queue
//       copy _VNGNotifyRcvd to waiter
//       free _VNGNotifyRcvd
//       return VNG_OK
//    else
//      return VNGERR_NOT_FOUND
//
//    or return appropriate VNGErrCode
//

VNGErrCode
_vng_evt_notify_find_rcvd (VNG                   *vng,
                      _VNGNotifyWaiter *waiter)
{
    VNGErrCode           ec;
   _VNGNotifyRcvd *rcvd;

    rcvd = (_VNGNotifyRcvd*)
                _vng_evt_s_list_remove_head (vng, &__vnp.pi.received, true);

    if (rcvd) {
        assert (rcvd->vn_ev->event.size <=
                        sizeof *waiter - offsetof(_VNGNotifyWaiter,buf));
        memcpy (waiter->vn_ev, rcvd->vn_ev,
                MIN(rcvd->vn_ev->event.size,
                    sizeof *waiter - offsetof(_VNGNotifyWaiter,buf)));
        _vng_vnEvent_free (vng, (_VNGEvent*) rcvd);
        ec = VNG_OK;
        if (!__vnp.pi.received.head) {
            while (_vng_evt_s_list_remove_by_eid (vng, VNG_EVENT_RECV_NOTIFY,
                                                  &__vnp.gevs_rcvd, true)) {
                ;
            }
        } 
    }
    else {
        ec = VNGERR_NOT_FOUND;
    }

    return ec;
}


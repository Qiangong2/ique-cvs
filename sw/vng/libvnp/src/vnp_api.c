//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//


#include "vnp_i.h"

#ifdef _SC
#include "fs.h"   /* read /sys/id.sys */
#include "es.h"
#include "iosc.h"
IOSCEccSignedCert deviceCert ES_SHA_ALIGN __attribute__ ((section (".data")));
#endif

_VNPSession __vnp_sessions[_VNP_MAX_SESSIONS];

_VNP  __vnp;

_VNPMutex  __vnp_mutex;

static uint8_t __vnpHeap [_VNG_HEAP_SIZE];



//-----------------------------------------------------------
//                  Initialization
//-----------------------------------------------------------


VNGErrCode _vnp_init_global_mutex()
{
    static bool __vnp_mutex_initialized;

    VNGErrCode ec = VNG_OK;

    if (!__vnp_mutex_initialized) {
        if (_vnp_mutex_init(&__vnp_mutex)) {
            ec = VNGERR_RM_API;
        } else {
            __vnp.mutex = &__vnp_mutex;
            __vnp_mutex_initialized = true;
        }
    }

    return ec;
}


#ifndef _SC


// Not called by RM.
//
LIBVNG_API VNGErrCode VNG_Init (VNG  *vng,
                                const VNGInitParm *parm)
{
    VNGErrCode    ec;
   _VNPSession*   session;


   if (!(ec = _vnp_alloc_session_without_rm (vng, &session))) {
        ec = _vnp_init (vng, session, parm, NULL, NULL, 0);
   }

    return ec;
}


// Not called by RM.
//
LIBVNG_API VNGErrCode VNG_InitServer (VNG            *vng,
                                      const char     *server,
                                      VNGPort         serverPort,
                                const VNGServerParm  *optParm)
{
    VNGErrCode            ec = VNG_OK;
   _VNPSession*           session;
    VNGServerParm         def;
    const VNGServerParm  *svr;

    if (optParm) {
        svr = optParm;
    }
    else {
        svr = &def;
        def.auto_accept_new_vn  = VNG_SVR_AUTO_ACCEPT_NEW_VN;
        def.num_vn_hash_buckets = VNG_SVR_NUM_VN_HASH_BUCKETS;
        def.vn_evt_buf_size     = VNG_SVR_EVT_BUF_SIZE;
    }

   if (!(ec = _vnp_alloc_session_without_rm (vng, &session))) {
        ec = _vnp_init (vng, session, NULL, svr, server, serverPort);
   }

    return ec;
}


LIBVNG_API VNGErrCode VNG_CloneVN (VNG *vng,
                                   VN  *vn_api,       /* w */
                             const VN  *vnToClone)    /* r */
{
    VNGErrCode  ec;

    if (!vng) {
        ec = VNGERR_INVALID_VNG;
        goto end;
    }

    vn_api->id = vnToClone->id;
    vn_api->addr = vnToClone->addr;
    vn_api->vng_id = vng->id;
  
    ec = VNG_OK;

end:
    return ec;
}

#endif


VNGErrCode _vnp_init (VNG            *vng,
                     _VNPSession     *session,
               const  VNGInitParm    *parm,
               const  VNGServerParm  *svr,
                      const char     *server,
                      VNGPort         serverPort)
{
    VNGErrCode             ec = VNG_OK;
    int                    rv;
    static _VNPSemaphore   exit_sem;
    static _VNPThread      evt_dispatch_thread;
   _VNPThreadAttr         *attr = NULL;
    char                  *vn_init_msg;
    _VN_config_t           vn_config;
    _VN_config_t          *vn_init_parm = NULL;
    #ifdef _SC
        static uint8_t   __evdStack [_VNG_EVD_THREAD_STACK_SIZE];
       _VNPThreadAttr      sc_th_attr;
    #endif

    if (_vnp_mutex_lock (&__vnp_mutex)) {
        return VNGERR_UNKNOWN; // Fatal error. Should never occur.
    }

    session->vng = vng;
    session->vng_id = vng->id;

    if (__vnp.initial_session) {
        // _vnp already initialized
        goto unlock;
    }

    // Init __vnp members

    memset (&__vnp, 0, sizeof __vnp);

    __vnp.initial_session = session;
    __vnp.mutex = &__vnp_mutex;

    if (!svr) {
        __vnp.heap = _SHR_heap_init (0, __vnpHeap, sizeof __vnpHeap);
        if (!__vnp.heap) {
            trace (ERR, API, "VNG_Init() failed to init heap\n");
            ec = VNGERR_NOMEM;
            goto unlock;
        }
    }

    __vnp.exit_sem = &exit_sem;
    __vnp.evt_dispatch_thread = &evt_dispatch_thread;

    if (_vnp_sem_init (__vnp.exit_sem)) {
        _vnp_mutex_destroy (__vnp.mutex);
        ec = VNGERR_INVALID_VNG;
        goto unlock;
    }

    if (svr) {
        if (0 > (rv = _vn_init_server(server, serverPort))) {
            trace (ERR, API, "_vn_init_server returned %d\n", rv);
            ec = _vng_vn2vng_err(rv);
            goto end;
        }
        vn_init_msg = "initialized VN as server";
        __vnp.server.auto_accept_new_vn = svr->auto_accept_new_vn;
        __vnp.vn.hash.num_buckets       = svr->num_vn_hash_buckets;
        __vnp.evt_buf_size              = svr->vn_evt_buf_size;
    }
    else {
        __vnp.evt_buf_size  = _VNG_EVT_BUF_SIZE;
        __vnp.num_evts_allocated_msg_limit = _VNG_DEF_MAX_ALLOC_MSG_LIMIT;
        if (parm) {
            memset (&vn_config, 0, sizeof vn_config);
            vn_config.vn_port_min = parm->vnPortMin;
            vn_config.vn_port_max = parm->vnPortMax;
            vn_init_parm = &vn_config;
        }
        if (0 > (rv = _VN_init(vn_init_parm))) {
            trace (ERR, API, "_VN_init returned %d\n", rv);
            ec = _vng_vn2vng_err(rv);
            goto end;
        }
        vn_init_msg = "initialized VN as client";
    }

    __vnp.base_vn_time  = _VN_get_timestamp();

    trace (INFO, API, "_VNG_Init %s\n", vn_init_msg);

    __vnp.evt_buf = _vnp_malloc (__vnp.evt_buf_size);
    if (!__vnp.evt_buf) {
        trace (ERR, API, "Failed to allocate VN event buffer.  size %d\n",
                          __vnp.evt_buf_size);
        ec = VNGERR_NOMEM;
        goto end;
    }

    _vng_vn_init_exited (vng, &__vnp.vn.server_vn_api, &__vnp.vn.server);

    // Create VNG thread

    #ifdef _SC
        attr             = &sc_th_attr;
        attr->stack      = __evdStack;
        attr->stackSize  = sizeof __evdStack;
        attr->priority   = _VNG_EVD_THREAD_PRIORITY;
        attr->start      =  true;
        attr->attributes = IOS_THREAD_CREATE_JOINABLE;
    #endif

    __vnp.state = _VNG_OK;
    rv = _vnp_thread_create(__vnp.evt_dispatch_thread, attr,
                 (_VNPThreadFunc)_vng_dispatch_evts, vng);
    if (rv != 0) {
        trace (ERR, API, "Failed to create VNG event dispatch thread: %d\n", rv);
        ec = VNGERR_UNKNOWN;
    }

end:

    if (ec) {
        __vnp.state = _VNG_FINI;
        __vnp.initial_session = NULL;
       _vnp_sem_destroy (__vnp.exit_sem);
       if (__vnp.evt_buf) {
            _vng_free (__vnp, __vnp.evt_buf);
            __vnp.evt_buf = NULL;
       }
    }
    else if (!svr && (rv = _VN_discover_hosts (true))) {
        trace (INFO, API, "_VNG_INIT  _VN_discover_hosts  Failed  %d\n", rv);
    } else {
        trace (INFO, API, "_VNG_INIT completed successfully\n");
    }

unlock:
    if (ec) {
        trace (INFO, API, "_VNG_INIT Failed  %d\n", ec);
    }
    _vnp_mutex_unlock (&__vnp_mutex);
    return ec;
}




LIBVNG_API VNGErrCode VNG_Fini (VNG *vng)
{
    VNGErrCode    ec;
    VNGErrCode    evd_rv;
   _VNGNetNode   *vn;
   _VNGNetNode  **prev;
   _VNPSession   *s;
    unsigned      i;

    trace (INFO, API, "VNG_Fini started\n");

    if ((ec = _vng_lock (vng))) {
        return ec;
    }

    if (!(s = _vnp_vngId2Session(vng->id))) {
        trace (ERR, API, "VNG_Fini() vng->id invalid: %d\n", vng->id);
        ec = VNGERR_INVALID_VNG;
        goto unlock;
    }

    if (__vnp.initial_session != s) {
        if (__vnp.vn.server.vng_id == vng->id) {
          __vnp.loginType = VNG_NOT_LOGIN; // Prevent logout RPC since leaving the vn
            VNG_Logout (vng, VNG_NOWAIT);
        }
        for (i = 0;  i < __vnp.vn.hash.num_buckets;  ++i) {
            vn   = __vnp.vn.hash.table[i];
            prev = (_VNGNetNode**) &__vnp.vn.hash.table[i];
            while (vn) {
                if (vn->vng_id == vng->id) {
                   _vng_leave_vn (vng, vn);
                    vn = *prev;
                } else {
                    prev = &vn->next;
                    vn   =  vn->next;
                }
            }
        }

        vn = __vnp.vn.any;
        prev = &__vnp.vn.any;
        while (vn) {
            if (vn->vng_id == vng->id) {
                _vng_free_vn (vng, vn);
                vn = *prev;
            }
        }

        vn   = __vnp.vn.left_auto_created;
        prev = &__vnp.vn.left_auto_created;
        while (vn) {
            if (vn->vng_id == vng->id) {
                *prev = vn->next;       // remove from list
                vn->next = NULL;
                _vng_free (vng, vn);    // free _VNGNetNode and VN
                _vng_free (vng, vn->vn_api);
                vn = *prev;
            } else {
                prev = &vn->next;
                vn   =  vn->next;
            }
        }
        goto end;
    }

    __vnp.loginType = VNG_NOT_LOGIN; // Prevent logout RPC since leaving the vn
    VNG_Logout (vng, VNG_NOWAIT);

    __vnp.state = _VNG_EXITING;

    trace (INFO, API, "\n"
            "VNG_Fini:\n"
            "   num_waiters: %u\n"
            "   num_evts_rcvd: %u\n"
            "   num_evts_unrecognized: %u\n"
            "   num_evts_queued: %u\n"
            "   num_evts_dispatched: %u\n"
            "   num_evts_allocated_now: %u\n"
            "   num_evts_allocated_max: %u\n"
            "   num_evts_allocated_msg_limit: %u\n"
            "   num_evts_dropped_msg_limit: %u\n"
            "   num_evts_dropped_buf_full: %u\n"
            "   num_evts_dropped_nomem: %u\n"
            "   num_evts_dispatch_err: %u\n",
              __vnp.num_waiters,
              __vnp.num_evts_rcvd,
              __vnp.num_evts_unrecognized,
              __vnp.num_evts_queued,
              __vnp.num_evts_dispatched,
              __vnp.num_evts_allocated_now,
              __vnp.num_evts_allocated_max,
              __vnp.num_evts_allocated_msg_limit,
              __vnp.num_evts_dropped_msg_limit,
              __vnp.num_evts_dropped_buf_full,
              __vnp.num_evts_dropped_nomem,
              __vnp.num_evts_dispatch_err);

    for (i = 0;  i < __vnp.vn.hash.num_buckets;  ++i) {
        while ((vn = __vnp.vn.hash.table[i])) {
            _vng_leave_vn (vng, vn);
        }
    }

    while ((vn = __vnp.vn.any)) {
        _vng_free_vn (vng, vn);
    }

    if (__vnp.vn.hash.table) {
       _vng_free (vng, __vnp.vn.hash.table);
        __vnp.vn.hash.table = NULL;
    }

    _vng_evt_s_list_remove_all  (vng, &__vnp.rpc_waiters, &__vnp.rpcs_rcvd);
    _vng_evt_s_list_remove_all  (vng, &__vnp.gev_waiters, &__vnp.gevs_rcvd);
    _vng_evt_s_lists_remove_all (vng, &__vnp.new_net);
    _vng_evt_s_lists_remove_all (vng, &__vnp.conn_accepted);
    _vng_evt_s_lists_remove_all (vng, &__vnp.msg_err);
    _vng_evt_s_lists_remove_all (vng, &__vnp.vn_err);
    _vng_evt_s_lists_remove_all (vng, &__vnp.jr);
    _vng_evt_s_lists_remove_all (vng, &__vnp.pi);
    _vng_evt_s_lists_remove_all (vng, &__vnp.nr);

    _VN_cancel_get_events ();

    vn   =  __vnp.vn.left_auto_created;
    prev = &__vnp.vn.left_auto_created;
    while (vn) {
        *prev = vn->next;     // remove from list
        vn->next = NULL;
        _vng_free (vng, vn);  // free _VNGNetNode and VN
        _vng_free (vng, vn->vn_api);
        vn   =  *prev;
    }

    _vnp_mutex_unlock (__vnp.mutex);
    _vnp_thread_join (*__vnp.evt_dispatch_thread, (_VNPThreadRT*)&evd_rv, 10000);
    trace (INFO, API, "VNG_Fini vng dispatch thread returned %d\n", evd_rv);
    _vnp_mutex_lock (__vnp.mutex);

    _vng_exit_sync(vng);
    _vnp_sem_destroy (__vnp.exit_sem);

    __vnp.state = _VNG_FINI;
    __vnp.initial_session = NULL;

    if(__vnp.heap && _SHR_heap_destroy (__vnp.heap)) {
        trace (ERR, API, "VNG_Fini() failed to destroy heap\n");
    }

    // _VN_discover_hosts (false);
    // _VN_reset(0,0);

    memset (&__vnp, 0, sizeof __vnp);

end:
    if ((ec = _vnp_release_session (s))) {
        trace (ERR, API, "VNG_Fini() Failed to release session\n");
    }

unlock:
    _vnp_mutex_unlock (&__vnp_mutex);
    _vnp_assert_unlocked (&__vnp_mutex);

    trace (INFO, API, "VNG_Fini completed %d\n", ec);
    return ec;
}



LIBVNG_API uint32_t VNG_ConnectStatus (VNG *vng)
{
    uint32_t     status = VNG_ST_RM_OK;

    if (!(_vng_check_state(vng))) {
        status |= _VN_get_status();
        if (!(status & _VN_ST_INITIALIZED)) {
            status = VNG_ST_RM_OK;
        }
        else {
            status |= VNG_ST_VNG_INITIALIZED;
            if (__vnp.loginType == VNG_USER_LOGIN) {
                status |= VNG_ST_USER_LOGIN;
            }
        }
    }

    trace (FINE, API, "VNG_ConnectStatus 0x%x\n", status);
    return status;
}




//-----------------------------------------------------------
//----- Registration, server connection, and logon
//-----------------------------------------------------------


LIBVNG_API  VNGErrCode  VNG_Register(VNG        *vng,
                                     const char *serverName,    /* r */
                                     VNGPort     serverPort,
                                     char       *loginName,     /* w */
                                     char       *passwd,        /* w */
                                     VNGTimeout  timeout)
{
    VNGErrCode   ec;
    VN           server;

    if ((ec = VNG_InitInfraServerVN (vng, &server))) {
        return ec;
    }

    if ((ec = _vng_lock (vng)))
        return ec;

    if (!serverName) {
        ec = VNGERR_INVALID_ARG;
    }
    else if (!loginName || !passwd) {
        ec = VNGERR_INVALID_LOGIN;
    }

    loginName [0] = '\0';
    passwd [0]    = '\0';

    if (ec) {
        _vnp_mutex_unlock (__vnp.mutex);
        return ec;
    }

    ec = _vng_sconnect (vng, serverName, serverPort,
                        NULL, 0, NULL, 0, timeout);

    if (!ec) {
        _VNGRegisterArg arg;
        _VNGRegisterRet ret;
        size_t         retLen = sizeof ret;
        int32_t        optData = 0;
        VNGErrCode     ec2;

        _vnp_mutex_unlock (__vnp.mutex);

        // clear arg to 0.
        memset(&arg, 0, sizeof(arg));

        memcpy(arg.serialNumber, "sn-unknown", sizeof("sn-unknown"));
#ifdef _SC
        // access SN file on NC /sys/id.sys
        {
            IOSFd fsFd;
            char *idBuf;
            int len = 0;
            char *sn_begin = NULL;
            char *sn_end = NULL;

            idBuf = IOS_AllocAligned(0, IDSYS_BUFSIZE+1, 16);
            if (idBuf == NULL) {
                printf("Cannot alloc shared memory \n");
                return VNGERR_NOMEM; 
            }
	    memset(idBuf,0,sizeof(idBuf));
            if ((fsFd = ISFS_Open(IDSYS_FILE, ISFS_READ_ACCESS)) >= 0) {
                len = ISFS_Read(fsFd, (void*)idBuf, IDSYS_BUFSIZE);
                ISFS_Close(fsFd);
                if (len > 0) {
                    char *p = idBuf;// skip device id
                    while (p < &idBuf[IDSYS_BUFSIZE] && *p != '\n' && *p != '\0') p++;
                    if (*p == '\n') {
                        p++;
                        sn_begin = p;
                        while (p < &idBuf[IDSYS_BUFSIZE] && *p != '\n' && *p != '\0') p++;
                        sn_end = p;
                    }
                }
                if (sn_begin != NULL &&
                    sn_end != NULL &&
                    sn_end - sn_begin < MAX_DEVICE_SN_SIZE) {
		    // arg.serialNumber is cleared when arg is memset to 0
                    memcpy(arg.serialNumber, sn_begin, sn_end - sn_begin);
                }
            }
	    IOS_Free(0, idBuf);
        }
#endif

#ifdef _SC
        arg.deviceCertSize = sizeof(deviceCert);
        IOSC_GetDeviceCertificate (&deviceCert);
        memcpy(arg.deviceCert, deviceCert, sizeof(deviceCert));
#else
        arg.deviceCertSize = 384;
        memcpy(arg.deviceCert, "device-ecc-cert",  sizeof("device-ecc-cert"));
#endif

        ec = VN_SendRPC (&server, VN_MEMBER_OWNER,
                        _VNG_REGISTER,
                        &arg, sizeof(arg),
                        &ret, &retLen,
                        &optData,
                        timeout);

        if (!ec) {
            if (optData != VNG_OK)
                ec = optData;
            else if (retLen < sizeof ret)
                ec = VNGERR_UNKNOWN;
            else {
                strncpy (loginName, ret.login, VNG_MAX_LOGIN_LEN);
                loginName [VNG_MAX_LOGIN_LEN] = '\0';
                strncpy (passwd, ret.password, VNG_MAX_PASSWD_LEN);
                passwd [VNG_MAX_PASSWD_LEN] = '\0';
            }
        }

        if ((ec2 = _vng_lock (vng)))
            return ec2;

        __vnp.loginType = VNG_NOT_LOGIN;
        __vnp.userId    = VNG_INVALID_USER_ID;

        if (__vnp.vn.server.state == VN_OK)
            ec2 = _vng_leave_vn (vng, &__vnp.vn.server);
        else
            _vng_vn_init_exited (vng, &__vnp.vn.server_vn_api, &__vnp.vn.server);

        if (ec2) {
            trace (ERR, API, "VNG_Register _vng_leave_vn returned %d\n", ec2);
        }
    }
    _vnp_mutex_unlock (__vnp.mutex);

    trace (INFO, API, "_VNG_Register completed %d  "
                       "name %s, serverName %s  serverPort %u\n",
                       ec, loginName, serverName, serverPort);
    return _vng_xec(ec);
}


LIBVNG_API VNGErrCode VNG_Login (VNG         *vng,
                                 const char  *serverName,
                                 VNGPort      serverPort,
                                 const char  *loginName,
                                 const char  *passwd,
                                 VNGTimeout   timeout)
{
    VNGErrCode   ec;
    VNGTimeout  _timeout = timeout;
   _VN_time_t    endTime = 0;
   _VN_time_t    now;
    VN           server;

    uint32_t      loginType = VNG_USER_LOGIN;

    if (__vnp.loginType != VNG_NOT_LOGIN) {
        if ((ec = VNG_Logout (vng, timeout))) {
            return ec;
        }
    }

    if ((ec = VNG_InitInfraServerVN (vng, &server))) {
        return ec;
    }

    if (timeout != VNG_NOWAIT)
        endTime = _VN_get_timestamp() + _timeout;

    if ((ec = _vng_lock (vng)))
        return ec;

    if (__vnp.loginType != VNG_NOT_LOGIN) {
        ec = VNGERR_UNKNOWN;
        _vnp_mutex_unlock (__vnp.mutex);
        return ec;
    }


    if (!serverName) {
        ec = VNGERR_INVALID_ARG;
    }
    else if (!loginName ||
                strnlen(loginName, VNG_LOGIN_BUF_SIZE) == VNG_LOGIN_BUF_SIZE) {
        ec = VNGERR_INVALID_LOGIN;
    }
    else if (!passwd ||
                strnlen(passwd, VNG_PASSWD_BUF_SIZE) == VNG_PASSWD_BUF_SIZE) {
        ec = VNGERR_INVALID_LOGIN;
    }

    if (ec) {
        _vnp_mutex_unlock (__vnp.mutex);
        return ec;
    }

    ec = _vng_sconnect (vng, serverName, serverPort,
                        NULL, 0, NULL, 0, timeout);

    if (!ec) {
       _vng_login_arg  la;
       _vng_login_ret  user;
        size_t         retLen = sizeof user;
        int32_t        optData = 0;

        strncpy(la.user, loginName, VNG_MAX_LOGIN_LEN);
        la.user[VNG_MAX_LOGIN_LEN] = '\0';
        strncpy(la.passwd, passwd, VNG_MAX_PASSWD_LEN);
        la.passwd[VNG_MAX_PASSWD_LEN] = '\0';

        _vnp_mutex_unlock (__vnp.mutex);

        if (timeout != VNG_NOWAIT) {
            now = _VN_get_timestamp();
            if (now >= endTime)
                ec = VNGERR_TIMEOUT;
            else
                _timeout = (VNGTimeout) (endTime - now);
        }
        if (!ec) {
            ec = VN_SendRPC (&server, VN_MEMBER_OWNER,
                            _VNG_LOGIN,
                            &la, sizeof la,
                            &user, &retLen,
                            &optData,
                            _timeout);
        }

        if (!ec) {
            if (optData != VNG_OK)
                ec = optData;
            else if (retLen < sizeof user)
                ec = VNGERR_UNKNOWN;
            else if ((ec = _vng_lock (vng)))
                    return ec;
            else {
                __vnp.loginType = loginType;
                __vnp.userId = user.uInfo.uid;
		// TODO: cache userInfo 
                _vnp_mutex_unlock (__vnp.mutex);
            }
        }

        if (ec) {
            // timeout doesn't matter because VNG_Logout won't do
            // rpc since __vnp.loginType == VNG_NOT_LOGIN
            VNG_Logout (vng, timeout);
        }
    }
    else {
        _vnp_mutex_unlock (__vnp.mutex);
    }

    trace (INFO, API, "_VNG_Login completed %d  "
                       "name %s, serverName %s  serverPort %u\n",
                       ec, loginName, serverName, serverPort);
    return _vng_xec(ec);
}

LIBVNG_API VNGErrCode VNG_Logout (VNG         *vng,
                                  VNGTimeout   timeout)
{
    VNGErrCode  ec = VNG_OK;
    VN          server;

    trace (INFO, API, "VNG_Logout starting\n");

    if ((ec = VNG_InitInfraServerVN (vng, &server))) {
        return ec;
    }

    if ((ec = _vng_check_state (vng)))
        return ec;

    if (__vnp.loginType != VNG_NOT_LOGIN) {
        // Don't really care if this is successful
        ec = VN_SendRPC (&server, VN_MEMBER_OWNER,
                        _VNG_LOGOUT,
                        NULL, 0,
                        NULL, NULL,
                        NULL,
                        timeout);

    }

    if ((ec = _vng_lock (vng)))
        return ec;

    __vnp.loginType = VNG_NOT_LOGIN;
    __vnp.userId    = VNG_INVALID_USER_ID;

    if (__vnp.vn.server.state == VN_OK)
        ec = _vng_leave_vn (vng, &__vnp.vn.server);
    else
        _vng_vn_init_exited (vng, &__vnp.vn.server_vn_api, &__vnp.vn.server);

    trace (INFO, API, "VNG_Logout complete\n");

    _vnp_mutex_unlock (__vnp.mutex);
    return ec;
}


LIBVNG_API int32_t VNG_GetLoginType (VNG *vng)
{
    if (_vng_check_state (vng))
        return VNG_NOT_LOGIN;
    else
        return __vnp.loginType;
}


LIBVNG_API VNGUserId  VNG_MyUserId (VNG *vng)
{
    if (_vng_check_state (vng))
        return VNG_INVALID_USER_ID;
    else
        return __vnp.userId; // VNG_INVALID_USER_ID if not logged in 
}


LIBVNG_API VNGErrCode VNG_GetUserInfo (VNG         *vng,
                                       VNGUserId    uid,    // in
                                       VNGUserInfo *uinfo,  // out
                                       VNGTimeout   timeout)
{
    VNGErrCode     ec     = VNG_OK;
    size_t         retLen = sizeof *uinfo;
    int32_t        optData = 0;
    bool           locked = true;
    VN             server;

    if (!uinfo) {
        return VNGERR_INVALID_ARG;
    }

    if ((ec = VNG_InitInfraServerVN (vng, &server))) {
        return ec;
    }

    if ((ec = _vng_lock (vng)))
        return ec;

    if (__vnp.loginType == VNG_NOT_LOGIN) {
        ec = VNGERR_NOT_LOGGED_IN;
    }
    else {
        _vnp_mutex_unlock (__vnp.mutex);
        locked = false;

        ec = VN_SendRPC (&server, VN_MEMBER_OWNER,
                        _VNG_GET_USER_INFO,
                         &uid, sizeof uid,
                         uinfo, &retLen,
                         &optData,
                         timeout);
        if (!ec) {
            if (optData != VNG_OK)
                ec = optData == VNGERR_UNKNOWN ? VNGERR_NOT_FOUND : optData;
            else if (retLen < sizeof *uinfo)
                ec = VNGERR_UNKNOWN;
        }
    }

    if (ec) {
        uinfo->uid = VNG_INVALID_USER_ID;
        uinfo->login[0] = '\0';
        uinfo->nickname[0] = '\0';
        uinfo->description[0] = '\0';
        uinfo->attrCount = 0;
    }

    if (locked)
        _vnp_mutex_unlock (__vnp.mutex);

    return ec;
}


LIBVNG_API VNGErrCode VN_Listen (VN *vn_api, bool listen)
{
    VNGErrCode ec;
    int rv;
   _VN_net_t    netid;
   _VNGNetNode *vn;
   
    if ((ec = _vng_lock_vn (vn_api, &vn))) {
        return ec;
    }

    if (vn->owner != vn->self) {
        ec = VNGERR_INVALID_VN;
    }
    else {
        if (vn->netid == _VN_NET_ANY)
            netid =  _VN_NET_DEFAULT;
        else
            netid = vn->netid;
        if (0>(rv=_VN_listen_net(netid, listen))) {
            if (rv == _VN_ERR_NETID || rv == _VN_ERR_NOT_OWNER)
                ec = VNGERR_INVALID_VN;
            else
                ec = VNGERR_UNKNOWN;
        }
        else if (netid == _VN_NET_DEFAULT) {
            __vnp.vn.auto_create_policies = vn->policies;
            __vnp.vn.auto_create_policies &= ~VN_ANY;
        }
    }

    _vnp_mutex_unlock (__vnp.mutex);
    return ec;
}




//-----------------------------------------------------------
//                  Trace options
//-----------------------------------------------------------


LIBVNG_API VNGErrCode  VNG_SetTraceOpt (VNG         *vng,
                                        VNGTraceOpt  trop)
{
#ifdef _SHR_NOTRACE
    return VNGERR_NOT_SUPPORTED;
#else

    VNGErrCode ec = VNG_OK;
    bool      isNC;

    #ifdef _SC
        isNC = true;
    #else
        isNC = false;
    #endif

    if (trop.mask & VNG_TROP_GLOBAL_ENABLE) {
        _SHR_set_trace_enable (trop.global.enable);
    }
        
    if (trop.mask & VNG_TROP_GLOBAL_LEVEL) {
        if (trop.global.level < 0 || trop.global.level > TRACE_MAX_LEVEL) {
            ec = VNGERR_INVALID_ARG;
            goto end;
        }
        _SHR_set_trace_level (trop.global.level);
    }
        
    if (trop.mask & VNG_TROP_GLOBAL_PREFIX) {
        _SHR_set_trace_prefix (trop.global.prefix);
    }

    if (trop.mask & (VNG_TROP_GRP_ENABLE |
                     VNG_TROP_GRP_LEVEL  |
                     VNG_TROP_SUBGRP_ENABLE_MASK |
                     VNG_TROP_SUBGRP_DISABLE_MASK |
                     VNG_TROP_SUBGRP_LEVEL)) {
        if (trop.group.id < 0 || trop.group.id > _TRACE_MAX_GRP) {
            ec = VNGERR_INVALID_ARG;
            goto end;
        }
         if (trop.mask & VNG_TROP_SUBGRP_LEVEL) {
            if (trop.group.sub.bit < 0 || trop.group.level > _TRACE_MAX_SUBGRP) {
                ec = VNGERR_INVALID_ARG;
                goto end;
            }
        }
    }
        
    if (trop.mask & VNG_TROP_GRP_ENABLE) {
        _SHR_set_grp_trace_enable (trop.group.id, trop.group.enable);
    }
        
    if (trop.mask & VNG_TROP_GRP_LEVEL) {
        if (trop.group.level < 0 || trop.group.level > TRACE_MAX_LEVEL) {
            ec = VNGERR_INVALID_ARG;
            goto end;
        }
        _SHR_set_grp_trace_level (trop.group.id, trop.group.level);
    }
        
    if (trop.mask & VNG_TROP_SUBGRP_ENABLE_MASK) {
        _SHR_set_grp_trace_enable_mask (trop.group.id, trop.group.subGrpEnableMask);
    }
        
    if (trop.mask & VNG_TROP_SUBGRP_DISABLE_MASK) {
        _SHR_set_grp_trace_disable_mask (trop.group.id, trop.group.subGrpDisableMask);
    }

    if (trop.mask & VNG_TROP_SUBGRP_LEVEL) {
        if (trop.group.sub.level < 0 || trop.group.sub.level > TRACE_MAX_LEVEL) {
            ec = VNGERR_INVALID_ARG;
            goto end;
        }
        _SHR_set_sg_trace_level (trop.group.id, trop.group.sub.bit, trop.group.sub.level);
    }
        
end:
    return ec;

#endif // _SHR_NOTRACE
}




//-----------------------------------------------------------
//                  VN Creation
//-----------------------------------------------------------



LIBVNG_API VNGErrCode VNG_NewVN (VNG       *vng,
                                 VN        *vn_api,
                                 VNClass    vnClass,
                                 VNDomain   domain,
                                 VNPolicies policies)
{
    VNGErrCode       ec;
    int              rv;
    bool             infra =  domain == VN_DOMAIN_INFRA;
   _VNGNewNetEvent   waiter;
    uint32_t         netmask;
   _VN_addr_t        vn_server;
   _VN_guid_t        guid = _VN_GUID_INVALID;

    if (!vn_api)
        return VNGERR_INVALID_VN;

    vn_api->id = _VNG_INVALID_VNGVNID;

    if (vnClass == VN_CLASS_4)
        return VNGERR_NOT_SUPPORTED;

    if (vnClass < VN_CLASS_MIN  || vnClass > VN_CLASS_MAX   ||
        domain  < VN_DOMAIN_MIN || domain  > VN_DOMAIN_MAX  ||
        (policies & ~VN_POLICIES_ALL)) {

        return VNGERR_INVALID_ARG;
    }

    if ((ec = _vng_lock (vng)))
        return ec;

    if ((ec = _vng_vn_init_exited (vng, vn_api, NULL))) {
        _vnp_mutex_unlock (__vnp.mutex);
        return ec;
    }

    netmask = _vng_get_class_netmask (vnClass);

    if (policies & VN_ANY) {
        ec = _vng_vn_init (vng, vn_api, NULL, _VN_NET_ANY,
                           VN_MEMBER_ANY, VN_MEMBER_ANY,
                           domain, policies, 0);
        _vnp_mutex_unlock (__vnp.mutex);
        return ec;
    }

    _vng_vnEvent_init ((_VNGEvent*) &waiter, sizeof waiter, NULL);

    waiter.vn_ev = &waiter.buf.vn_ev;
    waiter.vn_ev->event.tag  = _VN_EVT_NEW_NETWORK;

    if (infra)
        vn_server = _VN_make_addr (__vnp.vn.server.netid, __vnp.vn.server.owner);
    else
        vn_server = _VN_ADDR_INVALID;

    if (infra && __vnp.loginType == VNG_NOT_LOGIN) {
        ec = VNGERR_NOT_LOGGED_IN;
    }
    else if (0>(rv=_VN_new_net (netmask, !infra, vn_server, NULL, 0))) {
        ec = _vng_vn2vng_err (rv);
    }
    else {
        waiter.vn_ev->event.call_id = rv;
        ec = _vng_vnEvent_wait (vng, NULL, (_VNGEvent*) &waiter,
                                 _VNG_DEF_NEW_NET_TIMEOUT, false);
        // The event data contains the new net id and my peer id.
        // The new net request/response needs to be matched by call_id.
        //
        // So there is no point checking for a rcvd response event until
        // we get the call_id from the _VN_new_net() and events
        // are returned from _VN_get_events().  Events returned by
        // _VN_get_events() won't be processed __vnp.mutex is released.
        // So the "checkFirst" arg to _vng_vnEvent_wait is false.
    }

    if (!ec) {
        _VN_net_t  netid = _VN_addr2net(waiter.vn_ev->myaddr);
        _VN_host_t self  = _VN_addr2host(waiter.vn_ev->myaddr);
        ec = _vng_vn_init (vng, vn_api, NULL, netid, self, self,
                                    domain, policies, 0);

        if (0 > _VN_get_guid(waiter.vn_ev->myaddr, &guid)) {
            guid.chip_id = _VN_CHIP_ID_INVALID;
            guid.device_type = _VN_DEVICE_TYPE_UNKNOWN;
        }

        trace (INFO, API,
            "VNG_NewVN  %p  deviceId 0x%08x.0x%08x  netid 0x%08x  owner %u\n",
            vn_api, guid.device_type, guid.chip_id, netid, self);
    }
    else if (waiter.msglen && waiter.msg_handle) {
        _VN_get_event_msg (waiter.msg_handle, NULL, 0); // discard msg from _VN_err_event
    }


   _vnp_mutex_unlock (__vnp.mutex);
    return ec;
}


LIBVNG_API VNGErrCode VNG_DeleteVN (VNG *vng, VN *vn_api)
{
    VNGErrCode    ec = VNG_OK;
    bool          isAutoCreated = false;
   _VNGNetNode   *vn;
   _VNGNetNode   *left;
   _VNGNetNode  **prev;

    if (!vn_api)
        return  VNGERR_INVALID_ARG;

    if ((ec = _vng_lock (vng)))
        return ec;

    vn = _vng_addrId2vn (vn_api->addr, vn_api->id);

    if (((ec = _vng_check_vn_state (vn)) == VNG_OK)) {
        if (vn->owner != vn->self)
            ec = VNGERR_VN_NOT_HOST;
        else {
            if (vn->flags & _VNG_VN_AUTO_CREATED) {
                isAutoCreated = true;
            }
            ec = _vng_leave_vn (vn->vng, vn);
        }
    }

    if (!vn || (isAutoCreated && !ec)) {
        // check for left auto created vn waiting for free
        left =  __vnp.vn.left_auto_created;
        prev = &__vnp.vn.left_auto_created;
        while (left) {
            if (left->id == vn_api->id) {     // if unique id matches
                if (left->vn_api == vn_api) { // if vn_api is addr allocated
                    trace (INFO, API, "Free auto created vn_api->id %u\n", vn_api->id);
                    *prev = left->next;       // remove from list
                    left->next = NULL;
                    _vng_free (vng, left);    // free _VNGNetNode and VN
                    _vng_free (vng, vn_api);
                    ec = VNG_OK;
                } else {
                    trace (INFO, API, "Cloned auto created vn_api->id %u "
                                      "passed to VNG_Delete\n", vn_api->id);
                }
                break;
            }
            prev = &left->next;
            left =  left->next;
        }
    }

    _vnp_mutex_unlock (__vnp.mutex);

    return ec;
}



// Won't clone server vn.
//
// Can pass NULL vn_api to just check if vn can be cloned.
//
static
VNGErrCode  _vng_clone_VN_from_VNGNetNode (VNG         *vng,
                                           VN          *vn_api,
                                          _VNGNetNode  *vn)
{
    VNGErrCode  ec = VNG_OK;
   _VN_addr_t   addr;

    if (vn == &__vnp.vn.server) {
        ec = VNGERR_INVALID_VN;
        goto end;
    }
    
    if ((ec = _vng_check_vn_state (vn))) {
        goto end;
    }

    if ((addr = _VN_make_addr(vn->netid, vn->self)) == _VN_ADDR_INVALID) {
        ec = VNGERR_INVALID_VN;
        goto end;
    }

    if (vn_api) {
        vn_api->id = vn->id;
        vn_api->addr = addr;
        vn_api->vng_id = vng->id;
    }

end:
    return ec;
}



LIBVNG_API  VNGErrCode VNG_GetVNs (VNG       *vng,
                                   VN        *vns,
                                   uint32_t  *nVNs)
{
    VNGErrCode  ec;

    uint32_t      i;
   _VNGNetNode   *vn;
    VN           *vn_api;
    uint32_t      maxVNs;
    uint32_t      nFound = 0;

    if (!nVNs) {
        return VNGERR_INVALID_ARG;
    }

    maxVNs = *nVNs;
    *nVNs = 0;

    if (maxVNs && !vns) {
        return VNGERR_INVALID_LEN;
    }

    if ((ec = _vng_lock (vng)))
        return ec;

    for (i = 0;  i < __vnp.vn.hash.num_buckets;  ++i) {
        for (vn = __vnp.vn.hash.table[i];  vn;  vn = vn->next) {

            if (nFound < maxVNs) {
                vn_api = &vns[nFound];
            } else {
                vn_api = NULL;
            }

            ec = _vng_clone_VN_from_VNGNetNode (vng, vn_api, vn);

            if (ec == VNG_OK) {
                ++nFound;
            }
        }
    }

    for (vn = __vnp.vn.any;  vn;  vn = vn->next) {

        if (nFound < maxVNs) {
            vn_api = &vns[nFound];
        } else {
            vn_api = NULL;
        }

        ec = _vng_clone_VN_from_VNGNetNode (vng, vn_api, vn);

        if (ec == VNG_OK) {
            ++nFound;
        }
    }

    *nVNs = nFound;

    if (nFound > maxVNs) {
        ec = VNGERR_OVERFLOW;
    } else {
        ec = VNG_OK;
    }

    _vnp_mutex_unlock (__vnp.mutex);

    return ec;
}



//-----------------------------------------------------------
//       Server APIs for accept/reject  VNG_NewVN requests
//-----------------------------------------------------------


LIBVNG_API VNGErrCode VNG_GetNewVNRequest (VNG         *vng,
                                           VN         **vn_api,       // out
                                           VNMember    *memb,         // out
                                           uint64_t    *requestId,    // out
                                           VNClass     *vnClass,      // out
                                           VNGTimeout   timeout)
{
    VNGErrCode             ec;
   _VNGNewNetRequestEvent  waiter;
   _VNGNetNode            *vn_r = NULL;
    VNGTimeout            _timeout = timeout;
   _VN_time_t              endTime = 0;
   _VN_time_t              now;



    if (!requestId || !vnClass)
        return VNGERR_INVALID_ARG;

    *requestId = 0;

    if (vn_api) {
        *vn_api = NULL;
    }

    if ((ec = _vng_lock (vng)))
        return ec;

    _vng_vnEvent_init ((_VNGEvent*) &waiter, sizeof waiter, NULL);

    waiter.vn_ev = &waiter.buf.vn_ev;
    waiter.vn_ev->event.tag  = _VN_EVT_NEW_NET_REQUEST;

    // check and wait for new net request

    if (timeout != VNG_NOWAIT)
        endTime = _VN_get_timestamp() + _timeout;

    do {

        ec = _vng_vnEvent_wait (vng, NULL, (_VNGEvent*) &waiter, _timeout, true);

        //  VNG_NewVn never passes a message in _VN_new_net_request_event,
        //  but if there was one it was already discarded by _vng_evt_new_net_request.

        if (  ec == VNG_OK
              &&  !(vn_r = _vng_addr2vn (waiter.vn_ev->addr))) {

            // The net on which the request was received is no longer valid
            // Reject and wait for another request

            _VN_accept_net (waiter.vn_ev->request_id,
                            waiter.vn_ev->netmask,
                            false,
                            NULL, 0);

            if (timeout != VNG_NOWAIT) {
                now = _VN_get_timestamp();
                if (now >= endTime)
                    ec = VNGERR_TIMEOUT;
                else
                   _timeout = (VNGTimeout) (endTime - now);
            }
        }
    } while ( ec == VNG_OK && !vn_r);

    if (ec == VNG_OK) {
        *requestId = waiter.vn_ev->request_id;
        *vnClass   = _vng_get_netmask_class (waiter.vn_ev->netmask);

        if (vn_api)
            *vn_api = vn_r->vn_api;

        if (memb)
            *memb = waiter.vn_ev->from_host;
    }

   _vnp_mutex_unlock (__vnp.mutex);

    return _vng_xec(ec);
}


// Responding to a new net request
//
LIBVNG_API VNGErrCode VNG_RespondNewVNRequest (VNG        *vng, 
                                               uint64_t    requestId,
                                               VNClass     vnClass,
                                               bool        accept)
{
    if (vnClass < VN_CLASS_MIN || vnClass > VN_CLASS_MAX)
        return  VNGERR_INVALID_ARG;

    if ((0>_VN_accept_net (requestId,
                          _vng_get_class_netmask (vnClass),
                           accept,
                           NULL, 0))) {
        return VNGERR_INVALID_ARG;  // requestId couldn't be resolved
    
    }
    return VNG_OK;
}





//-----------------------------------------------------------
//                  Joining and leaving a VN
//-----------------------------------------------------------


LIBVNG_API VNGErrCode VNG_JoinVN (VNG         *vng,        // in
                                  VNId         vnId,       // in
                                  const char  *msg,        // in
                                  VN          *vn_api,     // out
                                  char        *denyReason, // out
                                  size_t       denyReasonLen, // in
                                  VNGTimeout   timeout)
{
    VNGErrCode ec;

   _VNGJoinRequest      jr;
    size_t              jr_len;
    size_t              msg_len;

    if (!vn_api) {
        return VNGERR_INVALID_ARG;
    }

    if (denyReasonLen) {
        if (denyReason)
            *denyReason = '\0';
        else
            return VNGERR_INVALID_LEN;

        if (denyReasonLen > VN_MAX_MSG_LEN)
            denyReasonLen = VN_MAX_MSG_LEN;
    }

    if (msg)
        strncpy(jr.message, msg, sizeof jr.message);
    else
        jr.message[0] = '\0';

    msg_len = strnlen (jr.message, sizeof jr.message);
    if (msg_len == sizeof jr.message) {
        return VNGERR_INVALID_LEN;
    }

    if ((ec = _vng_lock (vng)))
        return ec;

    if ((ec = _vng_vn_init_exited (vng, vn_api, NULL))) {
        _vnp_mutex_unlock (__vnp.mutex);
        return ec;
    }

    jr.vnId = vnId;
    jr.userInfo.uid = __vnp.userId;  // uid is VNG_INVALID_USER_ID if not logged in
    // DLE FIX: only send userId because we don't cache userInfo anymore
    jr.userInfo.login[0] = '\0';
    jr.userInfo.nickname[0] = '\0';
    jr.userInfo.description[0] = '\0';
    jr.userInfo.attrCount = 0;
    jr_len = offsetof(_VNGJoinRequest, message)
            + msg_len + 1;


    ec = _vng_connect (vng, vnId,
                       &jr, (_VN_msg_len_t) jr_len,
                       denyReason, (_VN_msg_len_t) denyReasonLen,
                       vn_api,  timeout);

    if (ec == VNGERR_CONN_REFUSED) {
        ec = VNGERR_VN_JOIN_DENIED;
    }

   _vnp_mutex_unlock (__vnp.mutex);
    return ec;
}


LIBVNG_API VNGErrCode VNG_GetJoinRequest (VNG         *vng,
                                          VNId         vnId,      // in
                                          VN         **vn_api,    // out
                                          uint64_t    *requestId,
                                          VNGUserInfo *userInfo,
                                          char        *joinReason,
                                          size_t       joinReasonLen,
                                          VNGTimeout   timeout)
{
    VNGErrCode           ec;
   _VNGJoinRequestEvent  waiter;
    bool                 any_jr = false;

   _VNGNetNode     *vn_r = NULL;
    VNGTimeout     _timeout = timeout;
   _VN_time_t       endTime = 0;
   _VN_time_t       now;

   _VNGJoinRequest  jr;
   _VN_msg_len_t    msglen;
    int             recv_len;
    int             min_jr_len = offsetof(_VNGJoinRequest, message);
    int             reasonLen;
    size_t          cstrLen;
    size_t          cpyLen;


    if (!requestId)
        return VNGERR_INVALID_ARG;

    *requestId = 0;

    if (vn_api) {
        *vn_api = NULL;
    }

    if (userInfo) {
        userInfo->login[0] = '\0';
        userInfo->nickname[0] = '\0';
        userInfo->uid = 0;
        userInfo->description[0] = '\0';
        userInfo->attrCount = 0;
    }

    if (joinReasonLen && !joinReason)
        return VNGERR_INVALID_LEN;

    if (vnId.deviceId == 0 && vnId.netId == 0) {
        any_jr = true;
    }

    if (!any_jr && (vnId.deviceId  == _VN_CHIP_ID_INVALID ||
                    vnId.netId == _VNG_NET_INVALID ||
                   _VN_NETID_IS_RESERVED (vnId.netId))) {

        return VNGERR_INVALID_VN;
    }

    if ((ec = _vng_lock (vng)))
        return ec;

    _vng_vnEvent_init ((_VNGEvent*) &waiter, sizeof waiter, NULL);

    waiter.vn_ev = &waiter.buf.vn_ev;
    waiter.vn_ev->event.tag  = _VN_EVT_CONNECTION_REQUEST;

    if (any_jr) {
        waiter.vn_ev->addr = 0;
    }
    else {
        if (!(vn_r = _vng_netid2ownervn (vnId.netId))) {
            _vnp_mutex_unlock (__vnp.mutex);
            return VNGERR_INVALID_VN;
        }
        waiter.vn_ev->addr = _VN_make_addr(vnId.netId, vn_r->owner);
    }

    // check and wait for join request

    if (timeout != VNG_NOWAIT)
        endTime = _VN_get_timestamp() + _timeout;

    do {

        ec = _vng_vnEvent_wait (vng, NULL, (_VNGEvent*) &waiter, _timeout, true);

        if (  ec == VNG_OK
              &&  !(vn_r = _vng_addr2vn (waiter.vn_ev->addr))) {

            // The vn for which the request was received is no longer available
            // Discard event msg and retject the request.
            // If waiting for specific vn,
            //     return VNGERR_INVALID_VN
            // else
            //     wait for another request

            if (waiter.vn_ev->msglen) {
                _VN_get_event_msg (waiter.vn_ev->msg_handle, NULL, 0); // discard msg
            }
            _VN_accept (waiter.vn_ev->request_id, false, NULL, 0); // reject

            if (!any_jr)
                ec = VNGERR_INVALID_VN;
            else if (timeout != VNG_NOWAIT) {
                now = _VN_get_timestamp();
                if (now >= endTime)
                    ec = VNGERR_TIMEOUT;
                else
                _timeout = (VNGTimeout) (endTime - now);
            }
        }
    } while ( ec == VNG_OK && !vn_r);

    if (ec == VNG_OK) {
        if (waiter.vn_ev->msglen < min_jr_len || !waiter.vn_ev->msg_handle) {
            ec = VNGERR_UNKNOWN;
        }
        else {      
            if (vn_api)
                *vn_api = vn_r->vn_api;

            msglen = waiter.vn_ev->msglen;
            if (msglen > sizeof jr) {
                msglen = sizeof jr;
            }

            recv_len = _VN_get_event_msg (waiter.vn_ev->msg_handle, &jr, msglen);
            if (userInfo) { *userInfo = jr.userInfo; }
            *requestId = waiter.vn_ev->request_id;
            if (joinReasonLen) {
                if (recv_len <= min_jr_len) {
                    *joinReason = '\0';
                }
                else {
                    reasonLen = recv_len - min_jr_len;

                    if (reasonLen >= sizeof jr.message) {
                        ec = VNGERR_OVERFLOW;
                        reasonLen = sizeof jr.message - 1;
                    }
                    jr.message [reasonLen] = '\0';

                    cstrLen = strnlen (jr.message, joinReasonLen);
                    if (cstrLen == joinReasonLen) {
                        ec = VNGERR_OVERFLOW;
                        jr.message [joinReasonLen-1] = '\0';
                        cpyLen = joinReasonLen;
                    }
                    else {
                        cpyLen = cstrLen + 1;
                    }
                    memcpy (joinReason, jr.message, cpyLen);
                }
            }
        }
    }

   _vnp_mutex_unlock (__vnp.mutex);

    return _vng_xec(ec);
}


LIBVNG_API VNGErrCode VNG_AcceptJoinRequest (VNG        *vng,
                                             uint64_t    requestId)
{
     VNGErrCode  ec = VNG_OK;
    _VNGNetNode *vn;
    _VN_net_t    netid;
     uint32_t    policies;

    if (0>_VN_lookup_connection_request (requestId, &netid))
        return VNGERR_NOT_FOUND;

    if ((ec = _vng_lock (vng)))
        return ec;

    if (!(vn = _vng_netid2ownervn (netid))) {
        ec = VNGERR_NOT_FOUND;
        goto end;
    }

    policies = htoel(vn->policies);

    _VN_accept (requestId, true, &policies, sizeof policies);

end:
   _vnp_mutex_unlock (__vnp.mutex);

   return ec;
}


LIBVNG_API VNGErrCode VNG_RejectJoinRequest (VNG        *vng,
                                             uint64_t    requestId,      
                                             const char *reason)
{
    VNGErrCode  ec = VNG_OK;
    int  len;

    if (!reason)
        len= 0;
    else {
        len = (int) strnlen (reason, VNG_MAX_JOIN_DNY_MSG_LEN + 1);
        if (len > VNG_MAX_JOIN_DNY_MSG_LEN)
            ec = VNGERR_INVALID_LEN; // _vng_connect will terminate with null
        else
            len += 1;
    }

   _VN_accept (requestId, false, reason, len);
    return ec;
}



LIBVNG_API VNGErrCode VNG_LeaveVN (VNG *vng, VN *vn_api)
{
    VNGErrCode  ec = VNG_OK;
   _VNGNetNode *vn;

   if (!vn_api)  {
       return VNGERR_INVALID_ARG;
   }

    if ((ec = _vng_lock_vn (vn_api, &vn)))
        return ec;

    ec = _vng_leave_vn (vn->vng, vn);

    _vnp_mutex_unlock (__vnp.mutex);
    return ec;
}


LIBVNG_API  VNGErrCode  VNG_RemoveVNMember (VNG         *vng,
                                            VN          *vn_api,
                                            VNMember     memb)
{
    VNGErrCode  ec = VNG_OK;
   _VNGNetNode *vn;

   if (!vn_api)  {
       return VNGERR_INVALID_ARG;
   }

    if ((ec = _vng_lock_vn (vn_api, &vn)))
        return ec;

    if (vn->self != vn->owner) {
        ec = VNGERR_VN_NOT_HOST;
    } else if (memb == vn->self) {
        ec = VNGERR_INVALID_MEMB;
    } 
    else {
        ec = _vng_remove_vn_member (vn->vng, vn, memb);
    }

    _vnp_mutex_unlock (__vnp.mutex);
    return ec;
}




//-----------------------------------------------------------
//                  Invitation to Join a Virtual Network
//-----------------------------------------------------------


LIBVNG_API VNGErrCode VNG_SendNotification(VNG          *vng,
                                           VNGUserId    *uid,
                                           uint32_t      nUsers,
                                           const char   *message,
				           size_t        messageLen)
{
    VNGErrCode    ec;
    uint32_t      msgLen;
    char         *buf;
    uint32_t      piOff;
    
   _VNGSendNotificationArg  *msg; 
   _VNGNotifyMsg *pi;

    if (!nUsers)
        return VNG_OK;

    if (messageLen >= VNG_MAX_NOTIFY_MSG_BUF_SIZE || messageLen < 0)
        return VNGERR_INVALID_LEN;

    piOff = sizeof(_VNGSendNotificationArg) + ((nUsers-1) * sizeof(VNGUserId));

    msgLen =  piOff  + offsetof(_VNGNotifyMsg, message) + messageLen;

    if (!uid || message == NULL || msgLen > VN_MAX_MSG_LEN)
        return VNGERR_INVALID_LEN;
            
    if ((ec = _vng_lock (vng)))
        return ec;

    // Must be logged in because only the server can associate a uid
    // with a server vn for transmitting the invite to the invitee.
    // Also, only the server can communicate with the invitee over the
    // server vn.  A different mechanism would need to be implemented
    // to do this on an adhoc network.
    // 
    if (__vnp.loginType == VNG_NOT_LOGIN) {
        _vnp_mutex_unlock (__vnp.mutex);
        ec = VNGERR_NOT_LOGGED_IN;
        goto end;
    }

    buf = _vng_malloc (vng, VN_MAX_MSG_LEN);
    if (!buf) {
        ec = VNGERR_NOMEM;
        goto end;
    }

    msg = (_VNGSendNotificationArg*) buf;
    pi = (_VNGNotifyMsg*) &buf[piOff];

    msg->count = nUsers;
    memcpy (msg->userId, uid, nUsers * sizeof(VNGUserId));
    pi->userInfo.uid = __vnp.userId;  // uid is VNG_INVALID_USER_ID if not logged in
    // Userinfo will be filled in by server
    pi->userInfo.login[0] = '\0';
    pi->userInfo.nickname[0] = '\0';
    pi->userInfo.description[0] = '\0';
    pi->userInfo.attrCount = 0;
 
    memcpy (pi->message, message, messageLen); 

    ec = _vng_send_msg (&__vnp.vn.server_vn_api,
                       _VNG_MSG_LOCKED_FLAG,
                        VN_SERVICE_TYPE_RELIABLE_MSG,
                        __vnp.vn.server.owner,
                       _VNG_NOTIFY,
                       _VNG_CALLER_TAG_NOT_APPLICABLE,
                        msg,
                        msgLen,
                        VN_DEFAULT_ATTR,
                        0,
                        VNG_NOWAIT);

   _vng_free (vng, buf);

end:
   _vnp_mutex_unlock (__vnp.mutex);

   return ec;
}


LIBVNG_API VNGErrCode VNG_RecvNotification(VNG          *vng,
                                           VNGUserInfo  *userInfo,
                                           char         *message,
                                           size_t       *messageLen,
                                           VNGTimeout    timeout)
{
    VNGErrCode            ec;
   _VNGNotifyWaiter       waiter;

   _VNGNotifyMsg    pi;
   _VN_msg_len_t          maxLen   = sizeof pi;
    int                   minLen   = sizeof pi - sizeof pi.message;
    int                   msgLen;
    int                   rcvdLen;
        
    if (!messageLen || !message || !userInfo)
        return VNGERR_INVALID_LEN;

    if ((ec = _vng_lock (vng)))
        return ec;

    _vng_vnEvent_init ((_VNGEvent*) &waiter, sizeof waiter, NULL);

    waiter.vn_ev = &waiter.buf.vn_ev;
    waiter.vn_ev->event.tag  = _VNG_EVT_NOTIFY;

    // check and wait for player invite

    ec = _vng_vnEvent_wait (vng, NULL, (_VNGEvent*) &waiter, timeout, true);

    if (!ec) {
        if (waiter.vn_ev->size < minLen ||
              !waiter.vn_ev->msg_handle ||
               minLen > (rcvdLen = _VN_recv (waiter.vn_ev->msg_handle, &pi,
                                             maxLen))) {
            ec = VNGERR_UNKNOWN;
        }
        else if (rcvdLen) {
            if (userInfo)
                *userInfo = pi.userInfo;

            msgLen = rcvdLen - minLen;
            if ((size_t)msgLen > *messageLen) 
                ec = VNGERR_OVERFLOW;
            else {
                memcpy (message, pi.message, msgLen);
		*messageLen = msgLen;
	    }
        }
    }

   _vnp_mutex_unlock (__vnp.mutex);

    return _vng_xec(ec);
}




//-----------------------------------------------------------
//                  VN: Accessing VN inforamtion
//-----------------------------------------------------------


LIBVNG_API VNMember  VN_Self (VN *vn_api)
{
    VNMember    memb;
   _VNGNetNode *vn;

    if (_vng_lock_vn (vn_api, &vn)) {
        memb = VN_MEMBER_INVALID;
    } else {
        memb = vn->self;
        _vnp_mutex_unlock (__vnp.mutex);
    }

    return memb;
}



LIBVNG_API VNMember  VN_Owner (VN *vn_api)
{
    VNMember    memb;
   _VNGNetNode *vn;

    if (_vng_lock_vn (vn_api, &vn)) {
        memb = VN_MEMBER_INVALID;
    } else {
        memb = vn->owner;
        _vnp_mutex_unlock (__vnp.mutex);
    }

    return memb;
}



LIBVNG_API VNState  VN_State (VN *vn_api)
{
    VNState     state;
   _VNGNetNode *vn;

    if (_vng_lock_vn (vn_api, &vn)) {
        state = VN_EXITED;
    } else {
        state = vn->state;
        _vnp_mutex_unlock (__vnp.mutex);
    }

    return state;
}



LIBVNG_API uint32_t  VN_NumMembers (VN *vn_api)
{
    VNGErrCode ec = VNG_OK;
    int rv;
    int nMemb;
   _VNGNetNode *vn;
   
    // No way to return error code

    if ((ec = _vng_lock_vn (vn_api, &vn)))
        return 0;

    if (vn->state != VN_OK) {
        nMemb = 0;
        ec = VNGERR_INVALID_VN;
    }
    else if (0>(rv=_VN_get_vn_size (vn->netid))) {
        nMemb = 0;
        if (rv == _VN_ERR_NETID)
            ec = VNGERR_INVALID_VN;
        else
            ec = VNGERR_UNKNOWN;
    }
    else {
        nMemb = rv;
    }

    _vnp_mutex_unlock (__vnp.mutex);
    return nMemb;
}


LIBVNG_API uint32_t  VN_GetMembers (VN       *vn_api,
                                    VNMember *members,
                                    uint32_t  nMembers)
{
    // VN_GetMembers has no way to indicate errors
    //  other than return 0 members

    VNGErrCode ec;
    int rv;
    uint32_t  nMemb = 0;
   _VNGNetNode *vn;
   
    if (!nMembers) {
        nMemb = VN_NumMembers (vn_api);
        goto end;
    }

    if (!members) {
        ec = VNGERR_INVALID_ARG; // no way to indicate error
        goto end;
    }

    if ((ec = _vng_lock_vn (vn_api, &vn))) {
        goto end;
    }

    if (0>(rv = _VN_get_vn_hostids (vn->netid, members,
                                              nMembers))) {
        nMemb = 0;
        if (rv == _VN_ERR_NETID)
            ec = VNGERR_INVALID_VN;
        else if (rv == _VN_ERR_INVALID)
            ec = VNGERR_INVALID_ARG;
        else
            ec = VNGERR_UNKNOWN;
    }
    else {
        nMemb = rv;
        ec = VNG_OK;
    }

    _vnp_mutex_unlock (__vnp.mutex);

end:
    return nMemb;
}


LIBVNG_API VNGErrCode  VN_MemberState (VN *vn_api, VNMember memb)
{
   _VN_addr_t    addr;
    int          rv;
    VNGErrCode   ec;
   _VNGNetNode *vn;
   
    if ((ec = _vng_lock_vn (vn_api, &vn)))
        return ec;

    if (vn->netid == _VN_NET_ANY) {
        if (memb != VN_MEMBER_ANY) {
            ec = VNGERR_INVALID_MEMB; 
        }
    }
    else if ((addr = _VN_make_addr(vn->netid, memb)) == _VN_ADDR_INVALID)
        ec = VNGERR_INVALID_MEMB;
    else if ((0>(rv = _VN_get_host_status(addr, NULL))))
        ec = VNGERR_NOT_FOUND;

    _vnp_mutex_unlock (__vnp.mutex);

    return ec;
}


LIBVNG_API VNGUserId  VN_MemberUserId (VN         *vn_api,
                                       VNMember    memb,
                                       VNGTimeout  timeout)
{
    // Get memb useid directly from the member
    // It will be VNG_INVALID_USER_ID if the member is not
    // logged in to the infrastructure.

    // Since any memb user id request will return the same
    // value, exchange messages rather than use rpc.

    VNGErrCode     ec;
    VNGUserId      uid;
    uint8_t        buf[sizeof(uint32_t)];
    size_t         rcvdLen = sizeof buf;
    VNMsgHdr       hdr;
    VNGTimeout    _timeout = timeout;
   _VN_time_t      endTime = 0;
   _VN_time_t      now;

    if (timeout != VNG_NOWAIT)
        endTime = _VN_get_timestamp() + _timeout;

    assert (sizeof uid == sizeof buf); // catch variable size change

    if ((ec = _vng_send_msg (vn_api,
                            _VNG_2_VNG_PORT_FLAG,
                             VN_SERVICE_TYPE_RELIABLE_MSG,
                             memb,
                            _VNG_ST_GET_USERID,
                            _VNG_CALLER_TAG_NOT_APPLICABLE,
                             NULL,  // no msg
                             0,     // msg len 0
                             VN_DEFAULT_ATTR,
                             0,
                             timeout))) {

        return VNG_INVALID_USER_ID;
    }

    if (timeout != VNG_NOWAIT) {
        now = _VN_get_timestamp();
        if (now >= endTime)
            _timeout = VNG_NOWAIT;
        else
            _timeout = (VNGTimeout) (endTime - now);
    }

    if ((ec = _vng_recv_msg (vn_api,
                            _VNG_2_VNG_PORT_FLAG,
                             VN_SERVICE_TYPE_RELIABLE_MSG,
                             memb,
                            _VNG_ST_USERID,
                            _VNG_CALLER_TAG_NOT_APPLICABLE,
                             buf,
                             &rcvdLen,
                             &hdr,
                            _timeout))) {
        return VNG_INVALID_USER_ID;
    }

    if (rcvdLen != sizeof uid)
        uid = VNG_INVALID_USER_ID;
    else
        VNP_DecodeUint32 (buf, &uid);

    return uid;
}




LIBVNG_API VNGMillisec  VN_MemberLatency (VN *vn_api, VNMember memb)
{
    VNGMillisec  rtt;
   _VNGNetNode  *vn;
   
    if (_vng_lock_vn (vn_api, &vn)) {
        rtt = VN_MEMB_LATENCY_NA;
    }
    else {
        rtt = _vng_memb_latency (vn->netid, memb);
       _vnp_mutex_unlock (__vnp.mutex);
    }

    return rtt;
}


// returns VNG_INVALID_DEV_TYPE for a vn_any
//
LIBVNG_API VNGDeviceType  VN_MemberDeviceType (VN *vn_api, VNMember memb)
{
   _VN_addr_t     addr;
   _VNGNetNode   *vn;
   _VN_guid_t     guid = _VN_GUID_INVALID;
    VNGDeviceType vndt = VNG_INVALID_DEV_TYPE;

    if (!_vng_lock_vn (vn_api, &vn)) {
        if (   (addr = _VN_make_addr(vn->netid, memb)) != _VN_ADDR_INVALID
            && (0 <= _VN_get_guid(addr, &guid))) {
            
            vndt = guid.device_type;
        }
       _vnp_mutex_unlock (__vnp.mutex);
    }

    return vndt;
}

   
// sets *deviceId to -1 (_VN_DEVICE_TYPE_UNKNOWN, _VN_CHIP_ID_INVALID)
// for a vn_any or other invalid case
//
LIBVNG_API VNGErrCode  VN_GetMemberDeviceId (VN *vn_api, VNMember memb, uint64_t *deviceId)
{
    VNGErrCode    ec;
   _VN_addr_t     addr;
   _VNGNetNode   *vn;
   _VN_guid_t     guid = _VN_GUID_INVALID;

    if (!deviceId)
        return VNGERR_INVALID_ARG;

    *deviceId = guidToDeviceId (guid);;

    if ((ec = _vng_lock_vn (vn_api, &vn)))
        return ec;

    if ((addr = _VN_make_addr(vn->netid, memb)) == _VN_ADDR_INVALID) {
        ec = VNGERR_INVALID_VN;
    }
    else {
        if (0 > _VN_get_guid(addr, &guid)) {
            ec = VNGERR_INVALID_VN;
        } else {
            *deviceId = guidToDeviceId (guid);
        }
    }
    _vnp_mutex_unlock (__vnp.mutex);

    return ec;
}

   
// returns VNGERR_INVALID_VN for a vn_any
//
LIBVNG_API VNGErrCode VN_GetVNId (VN *vn_api, VNId *vnId)
{
    VNGErrCode  ec;
   _VN_addr_t   addr;
   _VNGNetNode *vn;
   _VN_guid_t   guid = _VN_GUID_INVALID;

    if (!vnId)
        return VNGERR_INVALID_ARG;

    vnId->deviceId = guidToDeviceId (guid);;
    vnId->netId    = _VNG_NET_INVALID;
   
    if ((ec = _vng_lock_vn (vn_api, &vn)))
        return ec;

    if ((addr = _VN_make_addr(vn->netid, vn->owner)) == _VN_ADDR_INVALID) {
        ec = VNGERR_INVALID_VN;
    }
    else {
        if (0 > _VN_get_guid(addr, &guid)) {
            ec = VNGERR_INVALID_VN;
        } else {
            vnId->deviceId = guidToDeviceId (guid);
            vnId->netId    = vn->netid;
        }
    }

    _vnp_mutex_unlock (__vnp.mutex);
    return ec;
}




// returns VNGERR_INVALID_VN for a vn_any
//
LIBVNG_API VNGErrCode VN_MemberIPAddr (VN         *vn_api, 
                                       VNMember    memb,
                                       uint32_t   *ipAddr)
{
    VNGErrCode  ec = VNG_OK;
    int         rv;
   _VN_addr_t   addr;
   _VNGNetNode *vn;
   
    if ((ec = _vng_lock_vn (vn_api, &vn)))
        return ec;

    if ((addr = _VN_make_addr(vn->netid, memb)) == _VN_ADDR_INVALID)  
        ec = VNGERR_INVALID_VN;
    else if ((rv = _VN_get_host_ip (addr, ipAddr)))
        ec = _vng_vn2vng_err (rv);
    else if (*ipAddr == 0)
        ec = VNGERR_NOT_FOUND;

    _vnp_mutex_unlock (__vnp.mutex);
    return ec;
}






//-----------------------------------------------------------
//                  VN: peer to peer communication
//-----------------------------------------------------------

LIBVNG_API VNGErrCode VN_Select (VN               *vn_api,
                                 VNServiceTypeSet  selectFrom,
                                 VNServiceTypeSet *ready,
                                 VNMember          memb,
                                 VNGTimeout        timeout)
{
    VNGErrCode       ec;
    VNG             *vng;
   _VNGNewMsgWaiter  waiter;
   _VNGNetNode      *vn;
   
    if ((ec = _vng_lock_vn (vn_api, &vn))) {
        return ec;
    }

    vng = vn->vng;

    if ((selectFrom != VN_SERVICE_TYPE_ANY
                    && (selectFrom & ~VN_SERVICE_TYPE_ANY_MASK))
            || !ready) {
        ec = VNGERR_INVALID_ARG;
    }
    else if (memb > _VNG_VN_MEMBER_MAX
                && !(   memb == VN_MEMBER_SELF
                     || memb == VN_MEMBER_OWNER
                     || memb == VN_MEMBER_OTHERS
                     || memb == VN_MEMBER_ANY)) {
        ec = VNGERR_INVALID_MEMB;
    }
    else if (vn->netid == _VN_NET_ANY  &&  memb != VN_MEMBER_ANY) {
        ec = VNGERR_INVALID_ARG;
    }

    if (ec) {
        _vnp_mutex_unlock (__vnp.mutex);
        return ec;
    }

    *ready = 0;

    _vng_vnEvent_init ((_VNGEvent*) &waiter, sizeof waiter, NULL);

    waiter.vn_ev = &waiter.buf.vn_ev;
    waiter.vn_ev->event.tag  = _VN_EVT_NEW_MESSAGE;

    waiter.vn_ev->netid = vn->netid;
    if (vn->netid == _VN_NET_ANY)
        waiter.vn_ev->tohost = VN_MEMBER_ANY;
    else
        waiter.vn_ev->tohost = vn->self;

    // Only actual node id, VN_MEMBER_OTHERS, or VN_MEMBER_ANY
    // are allowed in fromhost when finding  or waiting for msgs.
    if (memb == VN_MEMBER_OWNER)
        waiter.vn_ev->fromhost = vn->owner;
    else if (memb == VN_MEMBER_SELF)
        waiter.vn_ev->fromhost = vn->self;
    else
        waiter.vn_ev->fromhost = memb;

    //Don't care about port or serviceTag, only services

    VNP_EncodeUint16 (waiter.vn_ev->opthdr, VN_SERVICE_TAG_ANY);

    if (selectFrom == VN_SERVICE_TYPE_ANY)
        waiter.serviceTypeSet = VN_SERVICE_TYPE_ANY_MASK;
    else
        waiter.serviceTypeSet = selectFrom;

    waiter.byServiceType = true;

    // Ready service channels are returned in waiter
    // check and wait for matching _VN_EVT_NEW_MESSAGEs

    ec = _vng_vnEvent_wait (vng, vn, (_VNGEvent*) &waiter,
                                                  timeout, true);
    if (!ec) {
        *ready = waiter.ready;
    }

   _vnp_mutex_unlock (__vnp.mutex);

    return _vng_xec(ec);
}




LIBVNG_API VNGErrCode VN_SendMsg (VN            *vn_api,
                                  VNMember       memb,
                                  VNServiceTag   serviceTag,
                                  const void    *msg,
                                  size_t         msglen,
                                  VNAttr         msg_attr,
                                  VNGTimeout     timeout)
{
    VNServiceType svcType;

    if (msg_attr & VN_ATTR_UNRELIABLE)
        svcType = VN_SERVICE_TYPE_UNRELIABLE_MSG;
    else
        svcType = VN_SERVICE_TYPE_RELIABLE_MSG;

    return _vng_send_msg (vn_api,
                         _VNG_MSG_DEF_FLAGS,
                          svcType,
                          memb,
                          serviceTag,
                         _VNG_CALLER_TAG_NOT_APPLICABLE,
                          msg,
                          msglen,
                          msg_attr,
                          0,
                          timeout);
}







// See comments and VN_RecvMsg pseudo code in vng_vnev.c

LIBVNG_API VNGErrCode VN_RecvMsg (VN           *vn_api,
                                  VNMember      memb,
                                  VNServiceTag  serviceTag,
                                  void         *msg,
                                  size_t       *msglen,
                                  VNMsgHdr     *hdr,
                                  VNGTimeout    timeout)
{
    // we don't distinguish between reliable and unreliable
    // messages on receive.

    return  _vng_recv_msg (vn_api,
                          _VNG_MSG_DEF_FLAGS,
                           VN_SERVICE_TYPE_RELIABLE_MSG,
                           memb,
                           serviceTag,
                          _VNG_CALLER_TAG_NOT_APPLICABLE,
                           msg,
                           msglen,
                           hdr,
                           timeout);
}




//-----------------------------------------------------------
//                  VN SERVICE REQUEST
//-----------------------------------------------------------


LIBVNG_API VNGErrCode VN_SendReq (VN           *vn_api,
                                  VNMember      memb,
                                  VNServiceTag  serviceTag,  // function id
                                  VNCallerTag  *callerTag,
                                  const void   *msg,
                                  size_t        msglen,
                                  int32_t       optData,
                                  VNGTimeout    timeout)
{
    return _vng_send_msg (vn_api,
                         _VNG_MSG_DEF_FLAGS,
                          VN_SERVICE_TYPE_REQUEST,
                          memb,
                          serviceTag,
                          callerTag,
                          msg,
                          msglen,
                          VN_DEFAULT_ATTR,
                          optData,
                          timeout);
}




LIBVNG_API VNGErrCode VN_RecvReq (VN           *vn_api,
                                  VNMember      memb,
                                  VNServiceTag  serviceTag,
                                  void         *msg,
                                  size_t       *msglen,
                                  VNMsgHdr     *hdr,
                                  VNGTimeout    timeout)
{
    return  _vng_recv_msg (vn_api,
                          _VNG_MSG_DEF_FLAGS,
                           VN_SERVICE_TYPE_REQUEST,
                           memb,
                           serviceTag,
                          _VNG_CALLER_TAG_ANY,
                           msg,
                           msglen,
                           hdr,
                           timeout);
}





//-----------------------------------------------------------
//                  VN SERVICE RESPONSE
//-----------------------------------------------------------



// It is ok for msg to be NULL if and only if msglen is 0
//
LIBVNG_API VNGErrCode VN_SendResp (VN           *vn_api,
                                   VNMember      memb,
                                   VNServiceTag  serviceTag,
                                   VNCallerTag   callerTag,
                                   const void   *msg,
                                   size_t        msglen,
                                   int32_t       optData,
                                   VNGTimeout    timeout)
{
    return _vng_send_msg (vn_api,
                         _VNG_MSG_DEF_FLAGS,
                          VN_SERVICE_TYPE_RESPONSE,
                          memb,
                          serviceTag,
                         &callerTag,
                          msg,
                          msglen,
                          VN_DEFAULT_ATTR,
                          optData,
                          timeout);
}




// It is ok for msg to be NULL if and only if
// msglen is NULL or *msglen is 0
//
// If msg is not NULL and msglen is NULL,
// it will be treated as zero msg length
//
// hdr can be NULL
//
LIBVNG_API VNGErrCode VN_RecvResp (VN           *vn_api,
                                   VNMember      memb,
                                   VNServiceTag  serviceTag,
                                   VNCallerTag   callerTag,
                                   void         *msg,
                                   size_t       *msglen,
                                   VNMsgHdr     *hdr,
                                   VNGTimeout    timeout)
{
    return  _vng_recv_msg (vn_api,
                          _VNG_MSG_DEF_FLAGS,
                           VN_SERVICE_TYPE_RESPONSE,
                           memb,
                           serviceTag,
                           callerTag,
                           msg,
                           msglen,
                           hdr,
                           timeout);
}




//-----------------------------------------------------------
//                  VN RPC
//-----------------------------------------------------------


// VN_RegisterRPCService
//
// Check if RPC is already in RPC list
// if not, put in RPC list
//

LIBVNG_API VNGErrCode VN_RegisterRPCService (VN               *vn_api,
                                             VNServiceTag      procId,
                                             VNRemoteProcedure proc)
{
    VNGErrCode  ec = VNG_OK;
   _VNGRpc     *rpc;
   _VNGNetNode *vn;

   if (procId == VN_SERVICE_TAG_ANY) {
        return VNGERR_INVALID_ARG;
   }

    if ((ec = _vng_lock_vn (vn_api, &vn))) {
        return ec;
    }

    rpc = _vng_malloc (vn->vng, sizeof *rpc);

    if (!rpc) {
        ec = VNGERR_NOMEM;
        goto end;
    }

    // See _VNGRpc comments in vnp_i.h

    rpc->next = NULL;
    rpc->procId = procId;
    rpc->proc = proc;

    _vng_rpc_map_insert (vn, rpc);

end:
    _vnp_mutex_unlock (__vnp.mutex);
    return ec;
}



LIBVNG_API VNGErrCode VN_UnregisterRPCService (VN               *vn_api,
                                               VNServiceTag      procId)
{
    VNGErrCode  ec = VNG_OK;
   _VNGRpc    *rpc;
   _VNGNetNode *vn;
   
    if ((ec = _vng_lock_vn (vn_api, &vn))) {
        return ec;
    }

    if (procId == VN_SERVICE_TAG_ANY) {
        _vng_rpc_map_remove_all (vn->vng, vn);
    }
    else {
        rpc = _vng_rpc_map_remove (vn, procId);

        while (_vng_evt_s_list_remove_by_procId (vn->vng, procId,
                                                &__vnp.rpcs_rcvd, true))
            {}

        if (rpc)
            _vng_free (vn->vng, rpc);
        else
            ec = VNGERR_NOT_FOUND;
    }

    _vnp_mutex_unlock (__vnp.mutex);
    return ec;
}


// It is ok for args to be NULL if and only if
// argsLen is 0
//
// It is ok for ret to be NULL if and only if
// retLen is NULL or *retLen is 0
//
// If ret is not NULL and retLen is NULL,
// it will be treated as zero ret length
//
// optData can be NULL
//
LIBVNG_API VNGErrCode VN_SendRPC (VN           *vn_api,
                                  VNMember      memb,
                                  VNServiceTag  serviceTag,
                                  const void   *args,
                                  size_t        argsLen,
                                  void         *ret,
                                  size_t       *retLen,
                                  int32_t      *optData,
                                  VNGTimeout    timeout)
{
    VNGErrCode     ec;
    VNMsgHdr       resp_hdr;
    VNCallerTag    callerTag;
    VNGTimeout    _timeout = timeout;
   _VN_time_t      endTime = 0;
   _VN_time_t      now;

    trace (FINER, API, "VN_SendRPC vn %p  memb %d  serviceTag %d  "
        "args %p   argsLen %d  ret %p  retLen %p  optData %p  timeout %d\n",
        vn_api, memb, serviceTag, args, argsLen, ret, retLen, optData, timeout);

    if (timeout != VNG_NOWAIT)
        endTime = _VN_get_timestamp() + _timeout;

    ec = VN_SendReq (vn_api, memb, serviceTag, &callerTag,
                     args, argsLen,
                     optData ? *optData : 0, timeout);

    if (!ec) {
        if (timeout != VNG_NOWAIT) {
            now = _VN_get_timestamp();
            if (now >= endTime)
                _timeout = VNG_NOWAIT;
            else
                _timeout = (VNGTimeout) (endTime - now);
        }
        ec = VN_RecvResp (vn_api, memb, serviceTag, callerTag,
                              ret, retLen, &resp_hdr, _timeout);
        if (ec == VNGERR_NOWAIT && timeout != VNG_NOWAIT)
            ec = VNGERR_TIMEOUT;
    }

    if (ec) {
        if (retLen)
            *retLen = 0;
    }
    else if (optData)
        *optData = resp_hdr.optData;

    return ec;
}





// VNG_RecvRPC
//
// This API is not advertised in public VNG client interface.
//
// It is provided for use in implementing VNG APIs on a
// game device when core VNG is implemented on secure cartridge.
//
// See comments on API in vng_p.h
//
LIBVNG_API VNGErrCode  VNG_RecvRPC (VNG               *vng,
                                    VNRemoteProcedure *proc,     // out
                                    VN               **vn_api,   // out
                                    VNMsgHdr          *hdr,      // out
                                    void              *args,     // out
                                    size_t            *argsLen,  // in/out
                                    VNGTimeout         timeout)
{
    if (!args && (argsLen && *argsLen))
         return VNGERR_INVALID_LEN;

    return _vng_recv_rpc (vng,
                          proc,
                          vn_api,
                          hdr,
                          &args,
                          argsLen,
                          timeout);
}





//-----------------------------------------------------------
//                  VNG Game Events
//-----------------------------------------------------------


LIBVNG_API VNGErrCode VNG_GetEvent (VNG        *vng,
                                    VNGEvent   *event,
                                    VNGTimeout  timeout)
{
    VNGErrCode      ec;
   _VNGGameEvent    waiter;

    if (!event)
        return VNGERR_INVALID_ARG;

    if ((ec = _vng_lock (vng)))
        return ec;

    _vng_vnEvent_init ((_VNGEvent*) &waiter, sizeof waiter, NULL);

    waiter.vn_ev = &waiter.buf.vn_ev;
    waiter.vn_ev->tag  = _VNG_EVT_GEV;

    // check and wait for game event

    ec = _vng_vnEvent_wait (vng, NULL, (_VNGEvent*) &waiter, timeout, true);

    if (!ec) {
        *event = waiter.gev;
    }

   _vnp_mutex_unlock (__vnp.mutex);

    return _vng_xec(ec);
}



//-----------------------------------------------------------
//                  Game Registration
//-----------------------------------------------------------


LIBVNG_API VNGErrCode VNG_RegisterGame (VNG          *vng,
                                        VNGGameInfo  *info,
                                        char         *comments,
                                        VNGTimeout    timeout)
{
    VNGErrCode   ec;
    bool         is_infra;
    bool         locked = false;
    VN           server;

    if ((ec = VNG_InitInfraServerVN (vng, &server))) {
        return ec;
    }

    if (!info)
        return VNGERR_INVALID_ARG;

    if (comments && strnlen(comments, VNG_GAME_COMMENTS_LEN) == VNG_GAME_COMMENTS_LEN)
        return VNGERR_INVALID_LEN;

    if ((ec = _vng_is_vnid_infra (info->vnId, &is_infra))) {
        return VNGERR_INVALID_VN;
    }

    if (is_infra) {
       _vng_register_game_arg a;
        int32_t  optData = 0;

        if ((ec = _vng_check_login (vng))) {
            goto end;
        }

        a.info = *info;
        if (comments) { 
            strncpy(a.comments, comments, VNG_GAME_COMMENTS_LEN);
        } else {
            a.comments[0] = 0;
        }

        if (!(ec = VN_SendRPC (&server, VN_MEMBER_OWNER,
                              _VNG_REGISTER_GAME,
                               &a, sizeof a,
                               NULL, NULL,
                               &optData,
                               timeout))) {
            ec = optData;
        }
    }
    else {  // adhoc or local

        int           rv;
       _VN_net_t      netid;
       _VNGAdhocGame  g;
        uint8_t       b[sizeof g];
        uint16_t      len;
       _VNGNetNode   *vn;
       _VN_guid_t     guid;

        if ((ec = _vng_lock (vng)))
            goto end;

        locked = true;

        netid = info->vnId.netId;
        guid.chip_id = deviceIdToChipId (info->vnId.deviceId);
        guid.device_type = deviceIdToDeviceType (info->vnId.deviceId);

        if (!(vn = _vng_netid2ownervn (netid))) {
            ec = VNGERR_INVALID_VN;
            goto end;
        }

        if (0>(rv = _VN_get_service (guid, netid, b, sizeof b))
                  || (0>_vng_unpack_adhoc_game (&g, b, rv))) {
            g.status = 0;
            g.numPlayers = 0;
        }

        g.info = *info;
        g.info.owner = __vnp.userId;

        if (comments) { 
            strncpy(g.comments, comments, VNG_GAME_COMMENTS_LEN);
        } else {
            g.comments[0] = 0;
        }

        len = _vng_pack_adhoc_game (&g, b);

        if (0>(rv = _VN_register_service (netid,
                                          vn->domain != VN_DOMAIN_LOCAL,
                                          b, len))) {
            ec = _vng_vn2vng_err(rv);
        }
    }

end:
    if (locked)
        _vnp_mutex_unlock (__vnp.mutex);

    return ec;
}


LIBVNG_API VNGErrCode VNG_UnregisterGame (VNG            *vng,
                                          VNId            vnId,
                                          VNGTimeout      timeout)
{
    VNGErrCode   ec;
    _vng_unregister_game_arg a;
    int32_t      optData = 0;
    bool         is_infra;
    VN           server;

    if ((ec = VNG_InitInfraServerVN (vng, &server))) {
        return ec;
    }

    a.vnId = vnId;

    if ((ec = _vng_is_vnid_infra (vnId, &is_infra))) {
        ec = VNGERR_INVALID_ARG;
        goto end;
    }

    if (is_infra) {

        if ((ec = _vng_check_login (vng))) {
            goto end;
        }

        if (!(ec = VN_SendRPC (&server, VN_MEMBER_OWNER,
                              _VNG_UNREGISTER_GAME,
                               &a, sizeof a,
                               NULL, NULL,
                               &optData,
                               timeout)))
        {
            ec = optData;
        }
    }
    else {
        int  rv;
        if (0>(rv = _VN_unregister_service(vnId.netId))) {
            ec = _vng_vn2vng_err(rv);
        }
    }

end:
    return ec;
}


//-----------------------------------------------------------
//--------------------- Details of the Games
//-----------------------------------------------------------


LIBVNG_API VNGErrCode VNG_UpdateGameStatus (VNG            *vng,
                                            VNId            vnId,
                                            uint32_t        gameStatus,
                                            uint32_t        numPlayers,
                                            VNGTimeout      timeout)
{
    VNGErrCode   ec;
    bool         is_infra;
    bool         locked = false;
    VN           server;

    if ((ec = VNG_InitInfraServerVN (vng, &server))) {
        return ec;
    }

    if ((ec = _vng_is_vnid_infra (vnId, &is_infra))) {
        return VNGERR_INVALID_VN;
    }

    if (is_infra) {
       _vng_update_game_status_arg a;
        int32_t  optData = 0;

        if ((ec = _vng_check_login (vng))) {
            goto end;
        }

        a.vnId = vnId;
        a.gameStatus = gameStatus;
        a.numPlayers = numPlayers;

        if (!(ec = VN_SendRPC (&server, VN_MEMBER_OWNER,
                              _VNG_UPDATE_GAME_STATUS,
                               &a, sizeof a,
                               NULL, NULL,
                               &optData,
                               timeout))) {
            ec = optData;
        }
    }
    else {  // adhoc or local

        int            rv;
       _VN_net_t       netid;
       _VNGAdhocGame   g;
        uint8_t        b[sizeof g];
        uint16_t       len;
       _VNGNetNode    *vn;
       _VN_guid_t      guid;

        if ((ec = _vng_lock (vng)))
            goto end;

        locked = true;

        netid = vnId.netId;
        guid.chip_id = deviceIdToChipId (vnId.deviceId);
        guid.device_type = deviceIdToDeviceType (vnId.deviceId);

        if (!(vn = _vng_netid2ownervn (netid))) {
            ec = VNGERR_INVALID_VN;
            goto end;
        }

        if (0>(rv = _VN_get_service (guid, netid, b, sizeof b))
                  || (0>_vng_unpack_adhoc_game (&g, b, rv))) {
            ec = VNGERR_NOT_FOUND;
            goto end;
        }

        g.status = gameStatus;
        g.numPlayers = numPlayers;

        len = _vng_pack_adhoc_game (&g, b);

        if (0>(rv = _VN_register_service (netid,
                                          vn->domain != VN_DOMAIN_LOCAL,
                                          b, len))) {
            ec = _vng_vn2vng_err(rv);
        }
    }

end:
    if (locked)
        _vnp_mutex_unlock (__vnp.mutex);

    return ec;
}


LIBVNG_API VNGErrCode VNG_GetGameStatus (VNG            *vng,
                                         VNGGameStatus  *gameStatus,
                                         uint32_t        nGameStatus, 
                                         VNGTimeout      timeout)
{
    VNGErrCode    ec = VNG_OK;
    bool          is_infra;
    bool          some_infra = false;
    bool          some_adhoc = false;  // adhoc or local
    int32_t       optData = 0;
    uint32_t      i;
    VNId          vnId;
    size_t        retLen = nGameStatus * sizeof(VNGGameStatus);
    VN            server;

   _vng_get_game_status_arg a;

    if ((ec = VNG_InitInfraServerVN (vng, &server))) {
        return ec;
    }

    if (nGameStatus && (!gameStatus || nGameStatus > MAX_GAME_STATUS))
        return VNGERR_INVALID_LEN;

    for (i = 0; i < nGameStatus; i++) {
        a.vnId[i] = vnId = gameStatus[i].gameInfo.vnId;
        gameStatus->gameInfo.gameId = 0;
        if ((ec = _vng_is_vnid_infra (vnId, &is_infra))) {
            return VNGERR_INVALID_VN;
        }

        if (is_infra)
            some_infra = true;
        else
            some_adhoc = true;
    }

    if ((ec = _vng_check_state (vng))) {
        goto end;
    }

    if (some_infra && !some_adhoc
            && __vnp.loginType == VNG_NOT_LOGIN) {
        ec = VNGERR_NOT_LOGGED_IN;
        goto end;
    }

    if (some_infra && __vnp.loginType != VNG_NOT_LOGIN) {
        a.count = nGameStatus;

        if (!(ec = VN_SendRPC (&server, VN_MEMBER_OWNER,
                              _VNG_GET_GAME_STATUS,
                               &a, sizeof a,
                               gameStatus, &retLen,
                               &optData,
                               timeout))) {
            ec = optData;
        }
    }

    if (ec || !some_adhoc)
        goto end;

    for (i = 0; i < nGameStatus; i++) {
        int           rv;
       _VNGAdhocGame  g;
        uint8_t       b[sizeof g];
       _VN_guid_t     guid;

        if (gameStatus->gameInfo.gameId)
            continue;

        vnId = gameStatus[i].gameInfo.vnId;

        if ((ec = _vng_is_vnid_infra (vnId, &is_infra))) {
            return VNGERR_INVALID_VN;
        }

        if (is_infra)
            continue;

        guid.chip_id = deviceIdToChipId (vnId.deviceId);
        guid.device_type = deviceIdToDeviceType (vnId.deviceId);

        if (0>(rv = _VN_get_service (guid, vnId.netId, b, sizeof b))
                  || (0>_vng_unpack_adhoc_game (&g, b, rv))) {
            continue; // couldn't find or unpack so leave gameId == 0
        }

        gameStatus[i].gameInfo =   g.info;
        gameStatus[i].gameStatus = g.status;
        gameStatus[i].numPlayers = g.numPlayers;
    }

end:
    return ec;
}


LIBVNG_API VNGErrCode VNG_GetGameComments (VNG        *vng,
                                           VNId        vnId,
                                           char       *comments,
                                           size_t     *commentSize, 
                                           VNGTimeout  timeout)
{
    VNGErrCode   ec;
    bool         is_infra;
    size_t       len = 0;
    int32_t      optData = 0;
    VN           server;

   _vng_get_game_comments_arg a;

    if ((ec = VNG_InitInfraServerVN (vng, &server))) {
        return ec;
    }

    if (!commentSize || !*commentSize)
        return VNGERR_INVALID_LEN;

    if (!comments)
        return VNGERR_INVALID_ARG;

    if ((ec = _vng_is_vnid_infra (vnId, &is_infra))) {
        return VNGERR_INVALID_VN;
    }

    if (is_infra) {

        a.vnId = vnId;

        if ((ec = _vng_check_login (vng))) {
            goto end;
        }

        len = *commentSize;
        if (!(ec = VN_SendRPC (&server, VN_MEMBER_OWNER,
                              _VNG_GET_GAME_COMMENTS,
                               &a, sizeof a,
                               comments, &len,
                               &optData,
                               timeout))) {
            ec = optData;
        }
    }
    else {
        int           rv;
       _VNGAdhocGame  g;
        uint8_t       b[sizeof g];
       _VN_guid_t      guid;

        guid.chip_id = deviceIdToChipId (vnId.deviceId);
        guid.device_type = deviceIdToDeviceType (vnId.deviceId);

        if (0>(rv=_VN_get_service (guid, vnId.netId, b, sizeof b))
                  || (0>_vng_unpack_adhoc_game (&g, b, rv))) {
            ec = VNGERR_NOT_FOUND;
        }
        else {
            strncpy (comments, g.comments, *commentSize);
            len = strnlen(comments, *commentSize);
            if (len == *commentSize) {
                ec = VNGERR_OVERFLOW;
                comments[--len] = 0;
            }
        }
    }

end:
    *commentSize = len;
    return ec;
}


//-----------------------------------------------------------
//                  Matchmaking
//-----------------------------------------------------------


LIBVNG_API VNGErrCode VNG_SearchGames (VNG                 *vng,
                                       VNGSearchCriteria   *searchCriteria,
                                       VNGGameStatus       *gameStatus,
                                       uint32_t            *nGameStatus,  // in & out
                                       uint32_t             skipN,
                                       VNGTimeout           timeout)
{
    VNGErrCode  ec = VNG_OK;
    VN          server;

    size_t      retLen;
    int32_t     optData = 0;
    uint32_t    maxGs;        // max num game status that can be returned
    uint32_t    nlaGs   = 0;  // num local and adhoc game status returned
    uint32_t    niGs    = 0;  // num infra game status returned
    uint32_t    skipNla = 0;  // num local and adhoc skipped

   _vng_match_game_arg a;

    if ((ec = VNG_InitInfraServerVN (vng, &server))) {
        return ec;
    }

    if (!nGameStatus || !*nGameStatus) {
        return VNGERR_INVALID_ARG;
    }

    if (!gameStatus) {
        ec = VNGERR_INVALID_ARG;
        goto end;
    }

    maxGs = *nGameStatus;

    if (searchCriteria == VNG_LIST_ADHOC_GAMES
            ||  (searchCriteria->domain & VNG_SEARCH_ADHOC)) {

        skipNla = skipN;
        nlaGs = *nGameStatus;
        if ((ec =  _vng_get_adhoc_games (vng, searchCriteria,
                                         gameStatus, &nlaGs, &skipNla))) {
            goto end;
        }
    }

    if (searchCriteria == VNG_LIST_ADHOC_GAMES
            ||  !(searchCriteria->domain & VNG_SEARCH_INFRA_MASK)
                    || nlaGs == maxGs)
        goto end;

    if (!searchCriteria->gameId) {
        trace (WARN, API,
            "VNG_SearchGames gameId must be non-zero for infra search.  "
            "gameId %u  maxLatency %d  cmpKeyword %u  "
                "nGameStatus %u  skipN %u\n",
                 searchCriteria->gameId, searchCriteria->maxLatency,
                 searchCriteria->cmpKeyword, *nGameStatus, skipN);
        ec = VNGERR_INVALID_ARG;
        goto end;
    }

    trace (FINER, API,
           "VNG_SearchGames gameId %u  maxLatency %d  cmpKeyword %u  "
           "nGameStatus %u  skipN %u\n",
           searchCriteria->gameId, searchCriteria->maxLatency,
           searchCriteria->cmpKeyword, *nGameStatus, skipN);

    a.skipN = skipN - skipNla;
    a.count = maxGs - nlaGs;
    a.searchCriteria = *searchCriteria;

    if ((ec = _vng_check_login (vng))) {
        goto end;
    }

    retLen = a.count * sizeof(VNGGameStatus);

    if (!(ec = VN_SendRPC (&server, VN_MEMBER_OWNER,
                          _VNG_MATCH_GAME,
                           &a, sizeof a,
                           gameStatus + nlaGs, &retLen,
                           &optData,
                           timeout)))
    {
        if (optData != VNG_OK) {
            ec = optData;
        } else {
            niGs = (uint32_t) retLen / sizeof(VNGGameStatus);
        }
    }

end:
    *nGameStatus = nlaGs + niGs;
    return ec;
}



//-----------------------------------------------------------
//                  Adhoc Domain
//-----------------------------------------------------------


LIBVNG_API VNGErrCode VNG_AddServerToAdhocDomain (VNG         *vng,
                                                  const char  *serverName,
                                                  VNGPort      serverPort)
{
    VNGErrCode ec = VNG_OK;
    int        rv;

    if (!serverName || !*serverName)
        return VNGERR_INVALID_ARG;

    if (0>(rv = _VN_query_host (serverName, serverPort))) {
        if (rv == _VN_ERR_NOSUPPORT)
            ec = VNGERR_NOT_SUPPORTED;
        else
            ec = VNGERR_UNKNOWN;
    }

    return ec;
}


LIBVNG_API VNGErrCode VNG_ResetAdhocDomain (VNG *vng)
{
    _VN_clear_query_list();
    return VNG_OK;
}




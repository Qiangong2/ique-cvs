/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */


#include "vng.h"
#include "vng_p.h"


/*
 *  See important comments on using VNG_GBAlloc()
 *  and VNG_GBFree() in vng_p.h
 *
 */


#if defined (_SC)

#include "sc/ios.h"

#define VNG_NC_ALLOC_HEAP_SIZE  (64*1024)

static uint8_t    __heap [VNG_NC_ALLOC_HEAP_SIZE] __attribute__((aligned(16)));
static int32_t    __heapInit = false;
static IOSHeapId  __heapId;

void*  VNG_GBAlloc(size_t size)
{
    void *rv;

    if (!__heapInit) {
        __heapId = IOS_CreateHeap (__heap, sizeof __heap);
        if (__heapId < 0) {
            rv = 0;
            goto end;
        }
        __heapInit = true;
    }
    rv = IOS_Alloc(__heapId, size);

end:
    return rv;
}


int VNG_GBFree (void *ptr)
{
    return IOS_Free (__heapId, ptr);
}


#elif defined (_GBA)

#define VNG_GBA_ALLOC_HEAP_SIZE  (32*1024)

static uint8_t  __heap [VNG_GBA_ALLOC_HEAP_SIZE] __attribute__((aligned(16))) = {1};
static uint8_t* __heapPtr = __heap;

void*  VNG_GBAlloc(size_t size)
{
    void *rv;

    rv = __heapPtr;
    __heapPtr += ((size + 3) & (~3));

    return rv;
}


int VNG_GBFree (void *ptr)
{
    /* any VNG_GBFree on the GBA will free the entire VNG_GBAlloc() heap */
    __heapPtr = __heap;
    return 0;
}

#else

#include <stdlib.h>

void* VNG_GBAlloc(size_t size)
{
    return malloc(size);
}

int VNG_GBFree (void *ptr)
{
    free (ptr);
    return 0;
}

#endif

//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//


#include "vnp_i.h"



// All functions in this file assume __vnp.mutex is already owned
// unless comments in the function indicate otherwise or it is obvious.



// This should only be called by _vng_vnEvent_wait()
// Must own __vnp.mutex on entry
VNGErrCode   _vng_check_exit(VNG *vng)
{
    if (__vnp.state == _VNG_OK)
        return VNG_OK;

    if (__vnp.state == _VNG_EXITING) {
        if (__vnp.num_waiters == 0) {
            _vnp_sem_post (__vnp.exit_sem);
        }
    }

    return VNGERR_FINI;
}


// This should only be called by VNG_Fini()
// Must own __vnp.mutex on entry
VNGErrCode   _vng_exit_sync(VNG *vng)
{
    if (__vnp.num_waiters) {
        _vnp_mutex_unlock (__vnp.mutex);
        _vnp_sem_wait (__vnp.exit_sem);
        _vnp_mutex_lock (__vnp.mutex);
        // at this point there are no waiters
        // and no more will be allowed to wait
    }
    return VNG_OK;
}


VNGErrCode _vng_check_vn_state (_VNGNetNode *vn)
{
    VNGErrCode  ec;

    if (__vnp.state != _VNG_OK) {
        ec = VNGERR_FINI;
    }
    else if (   vn
             && vn->state == VN_OK
             && vn->vng_id >= 0
             && vn->vn_api
             && vn->vng) {

        ec = VNG_OK;
    }
    else if (vn == &__vnp.vn.server) {
        ec = VNGERR_NOT_LOGGED_IN;
    } else{
        ec = VNGERR_INVALID_VN;
    }

    return  ec;
}


// OK whether or not__vng.mutex is locked
VNGErrCode _vng_check_state (VNG *vng)
{
    if (vng && __vnp.state == _VNG_OK)
        return VNG_OK;

    if (!vng)
        return VNGERR_INVALID_VNG;

    return  VNGERR_FINI;
}



// Since using _SHR_MUTEX_RECURSIVE for __vnp.mutex,
// _vnp_mutex_lock(), _vng_lock(), _vng_lock_vnn, _vng_lock_vn
// are OK whether or not __vnp.mutex is owned.
//
// However, all code was originally created assuming _SHR_MUTEX_NON_RECURSIVE.
//
// If successful, must have matching _vnp_mutex_unlock(__vnp.mutex)
//
VNGErrCode _vng_lock (VNG *vng)
{
    VNGErrCode ec;

    if ((ec = _vng_check_state (vng)))
        return ec;
   
    if (_vnp_mutex_lock (__vnp.mutex)) {
        // Fatal error. Should never occur.
        assert (!VNGERR_INVALID_VNG);
        return VNGERR_INVALID_VNG;
    }

    if ((ec = _vng_check_state (vng))) {
         _vnp_mutex_unlock (__vnp.mutex);
        return ec;
    }

    return VNG_OK;
}


// See comments for _vng_lock()
VNGErrCode _vng_lock_vnn (_VNGNetNode *vn)
{
    VNGErrCode ec;

    if (__vnp.state != _VNG_OK) {
        ec = VNGERR_FINI;
    }
    else if (!vn) {
        ec = VNGERR_INVALID_VN;
    }
    else if (_vnp_mutex_lock (__vnp.mutex)) {
        // Fatal error. Should never occur.
        assert (!VNGERR_INVALID_VNG);
        ec = VNGERR_INVALID_VNG;
    }
    else if ((ec = _vng_check_vn_state (vn))) {
         _vnp_mutex_unlock (__vnp.mutex);
    }

    return ec;
}


// See comments for _vng_lock()
VNGErrCode _vng_lock_vn (VN *vn_api, _VNGNetNode **vn)
{
    VNGErrCode ec;

    if (__vnp.state != _VNG_OK) {
        ec = VNGERR_FINI;
    }
    else if (!vn_api) {
        ec = VNGERR_INVALID_VN;
    }
    else if (_vnp_mutex_lock (__vnp.mutex)) {
            // Fatal error. Should never occur.
            assert (!VNGERR_INVALID_VNG);
            ec = VNGERR_INVALID_VNG;
    }
    else {

        *vn = _vng_addrId2vn (vn_api->addr, vn_api->id);

        if ((ec = _vng_check_vn_state (*vn))) {
            _vnp_mutex_unlock (__vnp.mutex);
        }
    }

    return ec;
}


// An RM open / close corresponds to a VNG session
// opened and closed via VNG_Init() and VNG_Fini().
//
// Therefore, an RM open connection descriptor
// identifies a VNG session.
//
// The state common to all VNG sessions is kept in
// the global _VNP data structure __vnp.
//
// Ok to call when not locked
//
_VNPSession*  _vnp_alloc_session (IOSUid   uid,
                                  IOSGid   gid,
                                  u32      flags)
{
    int          i;
   _VNPSession*  s = NULL;

    if (_vnp_mutex_lock (&__vnp_mutex)) {
        goto end; // Fatal error. Should never occur.
    }

    for (i = 0; i < _VNP_MAX_SESSIONS; ++i) {

        if (!__vnp_sessions[i].inUse) {

            s = &__vnp_sessions[i];

            s->inUse  = 1;
            s->uid    = uid;
            s->gid    = gid;
            s->flags  = flags;
            s->vng    = NULL;
            s->vng_id = _VNG_INVALID_VNGID;
            s->index  = i;

            #ifdef _SC
                trace (INFO, RM,
            #else
                trace (INFO, API,
            #endif
                "_vnp_alloc_session: new VNG session index %d\n", i);

            break;
        }
    }

    _vnp_mutex_unlock (&__vnp_mutex);

end:
    return s;
}


// Ok to call when not locked
//
VNGErrCode _vnp_alloc_session_without_rm (VNG         *vng,
                                         _VNPSession **session)
{
    VNGErrCode    ec;
   _VNPSession*   s = NULL;

    // in case this is the first time
    if ((ec = _vnp_init_global_mutex())) {
        goto end;
    }

    // Pass 0 for uid, gid, flags when not using RM
    s = _vnp_alloc_session (0, 0, 0);

    if (!s) {
        ec = VNGERR_MAX_REACHED;
        vng->id = _VNG_INVALID_VNGID;  // Since not from RM
        goto end;
    }

    vng->id = s->index;

end:
    *session = s;
    return ec;
}



// Ok to call when not locked
//
VNGErrCode _vnp_release_session (_VNPSession *s)
{
    VNGErrCode  rv = VNGERR_NOT_FOUND;
    int         i;

    if (_vnp_mutex_lock (&__vnp_mutex)) {
        rv = VNGERR_UNKNOWN;
        goto end; // Fatal error. Should never occur.
    }

    for (i = 0; i < _VNP_MAX_SESSIONS; ++i) {

        if (s == &__vnp_sessions[i]) {
            rv = VNG_OK;
            s->inUse = 0;
            s->vng = NULL;
            s->vng_id = _VNG_INVALID_VNGID;
            #ifdef _SC
                trace (INFO, RM,
            #else
                trace (INFO, API,
            #endif
                "_vnp_release_session: session index %d\n", i);

            break;
        }
    }

    _vnp_mutex_unlock (&__vnp_mutex);

end:
    return rv;
}



int32_t _vnp_get_num_sessions ()
{
    int  rv = 0;
    int  i;

    for (i = 0; i < _VNP_MAX_SESSIONS; ++i) {
        if (__vnp_sessions[i].inUse) {
            ++rv;
        }
    }

    #ifdef _SC
        trace (FINE, RM,
    #else
        trace (FINE, API,
    #endif
        "_vnp_get_num_sessions: session index %d\n", i);

    return rv;
}



_VNPSession*  _vnp_vngId2Session (_VNGId vng_id)
{
    int          i;
   _VNPSession*  s = NULL;

    for (i = 0; i < _VNP_MAX_SESSIONS; ++i) {
        if (__vnp_sessions[i].inUse &&
            __vnp_sessions[i].vng_id == vng_id) {

            s = &__vnp_sessions[i];
            break;
        }
    }

    return s;
}




// _vng_xec
// 
// Make sure err code is valid before returning
// from a public API.
//
// Ok to call when not locked
//
VNGErrCode _vng_xec (VNGErrCode ec)
{
    if (ec && (ec < 0 || ec > VNGERR_MAX)) {
        assert (ec && (ec < 0 || ec > VNGERR_MAX));
        trace (ERR, API, "Invalid error code on API return: %d\n", ec);
        return VNGERR_UNKNOWN;
    }
    return ec;
}




// Ok to call when not locked
//
VNGErrCode _vng_vn2vng_err (int vn_rv)
{
    VNGErrCode ec;

    if (vn_rv >= 0)
        return VNG_OK;

    //### DLE: TODO: Convert other VN error codes
    //               Think about which ones can be generic
    //               and which must be interpreted in context.

    switch (vn_rv) {
        case _VN_ERR_NOMEM:
            ec = VNGERR_NOMEM;
            break;

        case _VN_ERR_FULL:
        case _VN_ERR_NOSPACE:
            ec = VNGERR_BUF_FULL;
            break;

        case _VN_ERR_HOSTID:
            ec = VNGERR_INVALID_MEMB;
            break;

        case _VN_ERR_NOSUPPORT:
            ec = VNGERR_NOT_SUPPORTED;
            break;

        case _VN_ERR_SERVER:     // if see this here, don't know why
        case _VN_ERR_TIMEOUT:    // not the same as VNGERR_TIMEOUT
        case _VN_ERR_INVALID:
        default:
            trace (INFO, MISC, "VNGERR_UNKNOWN, VN error code: %d\n", vn_rv);
            ec = VNGERR_UNKNOWN; // most vn errs can only be interpreted in context
            break;
    }
    return ec;
}


// _VN_EVT_ERR to VNGErrCode
//
// Ok to call when not locked
//
VNGErrCode _vng_evt2vng_err (int errcode)
{
    VNGErrCode ec;

    if (errcode >= 0)
        return VNG_OK;

    //### DLE: TODO: convert other VN error codes

    switch (errcode) {

        case _VN_ERR_REJECTED:
            ec = VNGERR_CONN_REFUSED;
            break;

        case _VN_ERR_SERVER:
        case _VN_ERR_TIMEOUT:    // not the same as VNGERR_TIMEOUT
            ec = VNGERR_UNREACHABLE;
            trace (INFO, EVD, "VNGERR_UNREACHABLE, VN error code: %d\n", errcode);
            break;

        default:
            ec = _vng_vn2vng_err (errcode);
            break;
    }
    return ec;
}



VNGErrCode _vng_shr2vngErr (_SHRError shr_err)
{
    VNGErrCode ec;

    if (shr_err >= 0) {
        ec = VNG_OK;
    }
    else switch (shr_err) {
        case _SHR_ERR_ACCESS:
            ec = VNGERR_ACCESS;
            break;
        case _SHR_ERR_MAX:
            ec = VNGERR_MAX_REACHED;
            break;
        default:
            ec = VNGERR_UNKNOWN;
            break;
    }

    trace (INFO, MISC, "shr2vngErr shr err: %d  vng err %d\n", shr_err, ec);

    return ec;
}



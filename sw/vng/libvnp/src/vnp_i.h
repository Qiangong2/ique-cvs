//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//


#ifndef __VNP_I_H__
#define __VNP_I_H__  1

#include "vn.h"
#include "vnp_plat.h"
#include "vng.h"
#include "vng_p.h"
#include "vng_server.h"
#include "server_rpc.h"
#include "shr_trace.h"

#ifdef _SC
#include <sc/priority.h>
#else
#define IOS_PRIORITY_VNG     9
#include <stdio.h>
#endif



// See pack comments in vng.h
//
#if !defined(_GBA) && !defined(_SC)
#pragma pack(push, 4)
#endif



// Trace sub-group definitions
//
//     0  - default miscellaneous group
//     1  - API thread
//     2  - event dispatch loop
//     3  - peer to peer messages/reqs/resp/rpc

#define SG_MISC  0
#define SG_API   1
#define SG_EVD   2
#define SG_P2P   3
#define SG_RM    4
#define SG_VNEV  5

#define MISC     _TRACE_VNG, SG_MISC
#define API      _TRACE_VNG, SG_API
#define EVD      _TRACE_VNG, SG_EVD
#define P2P      _TRACE_VNG, SG_P2P
#define RM       _TRACE_VNG, SG_RM
#define VNEV     _TRACE_VNG, SG_VNEV



#ifdef __cplusplus
extern "C" {
#endif


#define VNP_DEV "/dev/vn"

#define IDSYS_FILE "/sys/id.sys"
    
#define IDSYS_BUFSIZE 256

// All timeouts in milliseconds

#define _VNG_SEND_SLEEP_TIME                 4

#define _VNG_DEF_NEW_NET_TIMEOUT            (30*1000)
#define _VNG_EVT_DEF_EXPIRATION             (300*1000)
#define _VNG_RMQ_PING_PERIOD                (5*60*1000)
#define  VNP_RMQ_PING_CMD                    0xFACEF00D


#define _VNG_EVT_NEW_NET_EXPIRATION         _VNG_EVT_DEF_EXPIRATION
#define _VNG_EVT_NEW_CONNECTION_EXPIRATION  _VNG_EVT_DEF_EXPIRATION
#define _VNG_EVT_NET_CONFIG_EXPIRATION      _VNG_EVT_DEF_EXPIRATION
#define _VNG_EVT_DISCONNECTED_EXPIRATION    _VNG_EVT_DEF_EXPIRATION
#define _VNG_EVT_CONN_REQUEST_EXPIRATION    _VNG_EVT_DEF_EXPIRATION
#define _VNG_EVT_CONN_ACCEPTED_EXPIRATION   _VNG_EVT_DEF_EXPIRATION
#define _VNG_EVT_NEW_NET_REQUEST_EXPIRATION _VNG_EVT_DEF_EXPIRATION
#define _VNG_EVT_NEW_MSG_EXPIRATION         _VNG_EVT_DEF_EXPIRATION
#define _VNG_EVT_MSG_ERR_EXPIRATION         _VNG_EVT_DEF_EXPIRATION
#define _VNG_EVT_ERR_EXPIRATION             _VNG_EVT_DEF_EXPIRATION
#define _VNG_EVT_DEVICE_UPDATE_EXPIRATION   _VNG_EVT_DEF_EXPIRATION
#define _VNG_EVT_RPC_EXPIRATION             _VNG_EVT_DEF_EXPIRATION

// External game event expiration timeout
#define _VNG_EVT_GEV_EXPIRATION             _VNG_EVT_DEF_EXPIRATION
#define _VNG_EVT_GJR_EXPIRATION             _VNG_EVT_DEF_EXPIRATION

#define _VNG_EVT_UNKNOWN              65536
#define _VNG_EVT_GEV                  65535
#define _VNG_EVT_NOTIFY               65534
#define _VNG_EVT_TIMER                65533
#define _VNG_EVT_RPC                  65532

// vng to vng message serviceTags
#define _VNG_ST_GET_USERID    1
#define _VNG_ST_USERID        2


#define  VN_POLICIES_ALL   (VN_ABORT_ON_ANY_EXIT   | \
                            VN_ABORT_ON_OWNER_EXIT | \
                            VN_AUTO_ACCEPT_JOIN    | \
                            VN_MSG_ENCRYPT         | \
                            VN_ANY)

#define _VNG_EVT_BUF_SIZE          (8*1024)
#define _VNG_EVD_THREAD_STACK_SIZE (20*1024)
#define _VNG_API_THREAD_PRIORITY   (IOS_PRIORITY_VNG)
#define _VNG_EVD_THREAD_PRIORITY   _VNG_API_THREAD_PRIORITY
#define _VNG_MIN_HEAP_SIZE   (_VNG_EVT_BUF_SIZE + 4*1024)
#define _VNG_HEAP_SIZE             (64*1024)

#define _VNG_MAX_ADHOC_DEV_IDS          256
#define _VNG_MAX_ADHOC_GAMES_PER_DEV    256

#define _VNG_DEF_MAX_ALLOC_MSG_LIMIT    (60*5)


#define _VNG_NET_INVALID     (-1)


// Definition of flag bits in __VNGNetNode->flags
#define _VNG_VN_AUTO_CREATED    1

#define VN_DOMAIN_UNKNOWN   ((VNDomain)0)
#define VN_DOMAIN_MIN       VN_DOMAIN_LOCAL
#define VN_DOMAIN_MAX       VN_DOMAIN_INFRA

#define VN_INVALID_CLASS    ((VNClass)-1)
#define VN_CLASS_MIN        VN_CLASS_1
#define VN_CLASS_MAX        VN_CLASS_4


// for backward compatibility,
// port definitions must not change after release

// Sevices that use a single "optional" header do
// not need a flag byte in the opthdr since the
// opthdr len is enough to indicate whether the
// single possible optional header is present.
// If present, the single optional header info
// will immediately follow the required headers
// for that service.
//
// The 3 lsb of service ports can be used to indicate
// presence of additonal optional headers or other info.
//
// Bit 0 is used to indicate that the sevice is
// vng to vng.  A msg using a port with the lsb set
// is a private msg from vng to vng.
//
// Bits 1 and 2 are currently not used and are always 0.
//
// One of the optional headers could contain a flag
// byte indicating presence of additional optional headers.
// However, the small size of the optional headers field
// makes the need for a flag byte both unlikely and
// undesirable.
//
// This scheme depends on VNServiceType < 30
//
// _VNG_SERVICE_PORT_BASE depends on _VN_PORT_MAX_RESERVED
// remaining below 0x10
//
#define _VNG_SERVICE_PORT_BASE        (0x10)

// port = ((service-1) << 3)  + _VNG_SERVICE_PORT_BASE
// svc  = ((port - _VNG_SERVICE_PORT_BASE) >> 3) + 1
// mask = (1<<svc)

#define _VNG_UNRELIABLE_MSG_PORT  0x10
#define _VNG_RELIABLE_MSG_PORT    0x18
#define _VNG_REQUSET_PORT         0x20
#define _VNG_RESPONSE_PORT        0x28

#define _VNG_SERVICE_PORT_RSVD_MASK  0x07

#define _VNG_OPTHDR_SVC_TAG_OFFSET    0
#define _VNG_OPTHDR_CALLER_TAG_OFFSET (sizeof(VNServiceTag))
#define _VNG_OPTHDR_REQ_RESP_MIN_LEN  (_VNG_OPTHDR_CALLER_TAG_OFFSET \
                                            + sizeof(VNCallerTag))
#define _VNG_OPTHDR_MSG_MIN_LEN       (sizeof(VNServiceTag))


#define _VNG_CALLER_TAG_ANY      0  // when caller tag not applicable
#define _VNG_CALLER_TAG_NOT_APPLICABLE  _VNG_CALLER_TAG_ANY
#define _VNG_CALLER_TAG_MIN      1
#define _VNG_CALLER_TAG_MAX      65534

// Flags passed to _vng_send_msg and _vng_recv_msg
#define _VNG_MSG_DEF_FLAGS             0
#define _VNG_MSG_LOCKED_FLAG      0x1000
#define _VNG_2_VNG_PORT_FLAG      0x0001  // or'd with port


#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

#define _VNG_32_BIT_MASK       0xFFFFFFFF
#define _VNG_32_BIT_MASK_ULL   0xFFFFFFFFULL


#define _VNP_MAX_SESSIONS 7

typedef struct {
    u8       inUse;
    IOSUid   uid;
    IOSGid   gid;
    u32      flags;
    VNG     *vng;
   _VNGId    vng_id;
    u32      index;

} _VNPSession;


typedef struct __VNGRpc          _VNGRpc;
typedef struct __VNGEvent        _VNGEvent;
typedef struct __VNGVnEvent      _VNGVnEvent;
typedef struct __VNGNewMsgRcvd   _VNGNewMsgRcvd;
typedef struct __VNGNewMsgWaiter _VNGNewMsgWaiter;


typedef struct {
    _VNGVnEvent *head;
    _VNGVnEvent *tail;
} _VNGVnEvtList;

typedef struct {
    _VNGVnEvtList waiters;
    _VNGVnEvtList received;
} _VNGVnEvtLists;

typedef struct {
    void    **table;
    uint32_t  num_buckets;
    uint32_t  max_load;
} _VNGHash;

typedef struct {
    _VNGNewMsgRcvd  *head;
    _VNGNewMsgRcvd  *tail;
} _VNGNewMsgRcvdList;


typedef enum {
   _VNG_FINI    = 0,
   _VNG_OK      = 1,
   _VNG_EXITING = 2

} _VNG_STATE;


typedef uint32_t _VNGNet;


typedef struct __VNGNetNode  _VNGNetNode;
typedef struct __VNP         _VNP;


struct __VNGNetNode {
   _VNGNetNode      *next;     // link for vn hash map
   _VNGNewMsgWaiter *waiters;
   _VNGNewMsgRcvd   *received;
   _VNGNewMsgWaiter *waiters_vng;
   _VNGNewMsgRcvd   *received_vng;

   struct {
     _VNGHash  hash;
   }                 rpc;      // registered RPCs

   _VNGVnId          id;       // unique per join/create, 0 is an invalid id
   _VNGId            vng_id;   // 0 is a valid vng_id
    VN              *vn_api;
    VNG             *vng;
    VNMember         self;     // my vn member id
    VNMember         owner;    // owner vn member id
   _VNGNet           netid;    // network id

    int32_t          state;    // VNState
    int32_t          domain;   // VNDomain
    VNPolicies       policies;

    uint32_t         flags;

};



struct __VNP {

   _VNPSession     *initial_session;  // from initial VNG_Init()
   _VNPMutex       *mutex;
   _VNPSemaphore   *exit_sem;
   _VNPHeap        *heap;
    uint64_t        base_vn_time;
    uint32_t        evt_buf_size;  // size of buf passed to _VN_get_events()
    uint8_t        *evt_buf;       // buf passed to _VN_get_events()

   _VNPThread      *evt_dispatch_thread;
    int32_t         dispatch_evts; // bool
    int32_t         dropping_msg_evts; // bool

    int32_t         state;         // _VNG_STATE

    uint32_t        num_waiters;
    uint32_t        num_evts_rcvd;
    uint32_t        num_evts_unrecognized;
    uint32_t        num_evts_queued;
    uint32_t        num_evts_dispatched;
    uint32_t        num_evts_allocated_now;
    uint32_t        num_evts_allocated_max;
    uint32_t        num_evts_allocated_msg_limit;
    uint32_t        num_evts_dropped_msg_limit;  // because of msg_limit
    uint32_t        num_evts_dropped_buf_full;
    uint32_t        num_evts_dropped_nomem;
    uint32_t        num_evts_dispatch_err;
    uint32_t        last_caller_tag;  // actual callerTag is uint16_t
    uint32_t        last_vn_id;

    int32_t         loginType;
    VNGUserId       userId;        // logged in user

    struct {
      _VNGEvent  *first;
      _VNGEvent  *last;
       int        num_expired;
    }  timeouts;

    _VNGVnEvtLists  new_net;
    _VNGVnEvtLists  conn_accepted;
    _VNGVnEvtLists  msg_err;
    _VNGVnEvtLists  vn_err;

    struct {
       _VNGNetNode   server;
        VN           server_vn_api;
       _VNGNetNode  *any;
       _VNGHash      hash;
       _VNGNetNode  *left_auto_created;
        VNPolicies   auto_create_policies;
    }  vn;

    struct {
      struct {
        _VNGNewMsgRcvdList  by_time;
        _VNGNewMsgRcvdList  msgs;
        _VNGNewMsgRcvdList  reqs;
        _VNGNewMsgRcvdList  resps;
      } msg_evts;
    }  all_vn;

    struct {
        int32_t  auto_accept_new_vn;  // bool
    } server;

    _VNGVnEvtList  rpcs_rcvd;
    _VNGVnEvtList  rpc_waiters;

    _VNGVnEvtList  gevs_rcvd;
    _VNGVnEvtList  gev_waiters;

    _VNGVnEvtLists jr;
    _VNGVnEvtLists pi;
    _VNGVnEvtLists nr;

};






typedef struct {
   VNGGameInfo   info;
   uint32_t      status;
   uint32_t      numPlayers;
   char          comments[VNG_GAME_COMMENTS_LEN];
} _VNGAdhocGame;



/*  in _VNGvnEventHead:
*
*   - size is size of _VNGEvent + any following bytes for
*     vn_ev and any event type specific vng parameters.
*
*   - If syncObj is non-NULL it is a pointer to an initialized
*     synchronization object.  Events on a waiter queue have
*     a non-NULL syncObj.  Events on a recvied event queue do not.
*
*     Note: syncObj should only be set if it has been initialized.
*     If *syncObj is closed, set syncObj to NULL and vice-versa.
*     _vng_vnEvent_free will close the syncObj if it is non-NULL.
*/

typedef struct {
    struct {
        _VNGEvent    *next;
        _VNGEvent    *prev;
        VNGTimeOfDay  time;
    } timeout;

   _VNPSemaphore   *syncObj; // NULL if doesn't wait
    uint16_t        ec;
    uint16_t        size;

} _VNGVnEventHead;



struct __VNGEvent {
    _VNGVnEventHead  ev;
    _VN_event_t     *vn_ev;

};


struct __VNGVnEvent {
    _VNGVnEventHead  ev;
    _VN_event_t     *vn_ev;
    _VNGVnEvent     *next;
};


typedef struct {
   _VNGVnEventHead   ev;
   _VN_event_t      *vn_ev; // used to allow common code
   _VNGVnEvent      *next;

    union {
      _VN_event_t  vn_ev;
    } buf;

} _VNGTimerEvent;


typedef struct {
   _VNGVnEventHead   ev;
   _VN_event_t      *vn_ev; // used to allow common code
   _VNGVnEvent      *next;
    VNGEvent         gev;   // this is VNGEvent, not _VNGEvent

    union {
      _VN_event_t  vn_ev;
    } buf;

} _VNGGameEvent;


typedef struct {
   _VNGVnEventHead         ev;
   _VN_new_message_event  *vn_ev;
   _VNGVnEvent            *next;

} _VNGNotifyRcvd;


typedef struct {
   _VNGVnEventHead         ev;
   _VN_new_message_event  *vn_ev;
   _VNGVnEvent            *next;

    union {
       _VN_new_message_event   vn_ev;
        uint8_t all [ sizeof(_VN_new_message_event) + _VN_MAX_HDR_LEN ];
    } buf;

} _VNGNotifyWaiter;


typedef struct {
   _VNGVnEventHead          ev;
   _VN_new_network_event   *vn_ev;
   _VNGVnEvent             *next;
    void*                   msg_handle; // non-null if _VN_EVT_ERR
    uint16_t                msglen;

    union {
      _VN_new_network_event  vn_ev;
    } buf;

} _VNGNewNetEvent;


typedef struct {
   _VNGVnEventHead           ev;
   _VN_new_connection_event *vn_ev;
   _VNGVnEvent              *next;

    union {
      _VN_new_connection_event  vn_ev;
    } buf;

} _VNGNewConnectionEvent;


typedef struct {
   _VNGVnEventHead         ev;
   _VN_net_config_event   *vn_ev;
   _VNGVnEvent            *next;

} _VNGNetConfigRcvd;


typedef struct {
   _VNGVnEventHead         ev;
   _VN_net_config_event   *vn_ev;
   _VNGVnEvent            *next;

    union {
      _VN_net_config_event  vn_ev;
    } buf;

} _VNGNetConfigWaiter;


typedef struct {
   _VNGVnEventHead              ev;
   _VN_net_disconnected_event  *vn_ev;
   _VNGVnEvent                 *next;

    union {
      _VN_net_disconnected_event  vn_ev;
    } buf;

} _VNGDisconnectedEvent;


typedef struct {
   _VNGVnEventHead                ev;
   _VN_connection_request_event  *vn_ev;
   _VNGVnEvent                   *next;

    union {
      _VN_connection_request_event  vn_ev;
    } buf;

} _VNGJoinRequestEvent;

typedef struct {    // The message struct sent with a join request
    VNId        vnId;
    VNGUserInfo userInfo;
    char        message[VNG_JOIN_REQ_MSG_BUF_SIZE];  // max size is not
                                                    // actual size of msg
} _VNGJoinRequest;


typedef struct {
   _VNGVnEventHead                 ev;
   _VN_connection_accepted_event  *vn_ev;
   _VNGVnEvent                    *next;
    void*                          msg_handle; // non-null if _VN_EVT_ERR
    uint16_t                       msglen;

    union {
      _VN_connection_accepted_event  vn_ev;
    } buf;

} _VNGConnectionAcceptedEvent;


typedef struct {
   _VNGVnEventHead             ev;
   _VN_new_net_request_event  *vn_ev;
   _VNGVnEvent                *next;

    union {
      _VN_new_net_request_event  vn_ev;
    } buf;

}_VNGNewNetRequestEvent;


typedef struct {
   _VNGVnEventHead          ev;
   _VN_msg_err_event       *vn_ev;

} _VNGMsgErrRcvd;


typedef struct {
   _VNGVnEventHead          ev;
   _VN_msg_err_event       *vn_ev;

    union {
       _VN_msg_err_event   vn_ev;
        uint8_t all [ sizeof(_VN_msg_err_event) + _VN_MAX_HDR_LEN ];
    } buf;

} _VNGMsgErrWaiter;


typedef struct {
   _VNGVnEventHead   ev;
   _VN_err_event    *vn_ev;
   _VNGVnEvent      *next;

    union {
      _VN_err_event  vn_ev;
    } buf;

} _VNGErrEvent;


typedef struct {
   _VNGVnEventHead           ev;
   _VN_device_update_event  *vn_ev;
   _VNGVnEvent              *next;

} _VNGDeviceUpdateRcvd;


typedef struct {
   _VNGVnEventHead           ev;
   _VN_device_update_event  *vn_ev;
   _VNGVnEvent              *next;

    union {
      _VN_device_update_event  vn_ev;
    } buf;

} _VNGDeviceUpdateWaiter;


typedef struct __VNGNewMsg       _VNGNewMsg;

typedef struct {
    _VNGNewMsg   *next;

} _VNGNewMsgLink_1;


typedef struct {
    _VNGNewMsgRcvd   *next;
    _VNGNewMsgRcvd   *prev;

} _VNGNewMsgRcvdLink_2;


typedef struct {
    _VNGNewMsgRcvd   *next;
    _VNGNewMsgRcvd   *prev;
    _VNGNewMsgRcvd   *last;    // self if self is first and last
                               // last if self if first and not last
                               // null if self is neither first nor last
} _VNGNewMsgRcvdLink_3;


// last_same_xxxx ==
//   last  with same xxxx if self if first
//   first with same xxxx if self is last
//   self if self is first and last with same xxxx
//   null if self is neither first nor last with same xxxx


struct __VNGNewMsg {
   _VNGVnEventHead         ev;
   _VN_new_message_event  *vn_ev;

   _VNGNewMsg             *next;
   _VNGNewMsg             *prev;

   _VNGNewMsg             *last_same_svcType; // comment above
   _VNGNewMsg             *last_same_svcTag;
   _VNGNewMsg             *last_same_sender;

};





struct __VNGNewMsgRcvd {
   _VNGVnEventHead         ev;
   _VN_new_message_event  *vn_ev;

   _VNGNewMsgRcvd         *next;
   _VNGNewMsgRcvd         *prev;

   _VNGNewMsgRcvd         *last_same_svcType; // comment above
   _VNGNewMsgRcvd         *last_same_svcTag;
   _VNGNewMsgRcvd         *last_same_sender;

    // The following links are accross all vns

   _VNGNewMsgRcvdLink_2   byTime;
#if 0  // Could optimize {msgs|reqs|resps} lists to one bySvcType list
   _VNGNewMsgRcvdLink_2   bySvcTypeSet;
   _VNGNewMsgRcvd         *last_same_bySvcTypeSet;
#elif 0     // Not currently implemented
   _VNGNewMsgRcvdLink_2   msgs;
   _VNGNewMsgRcvdLink_2   reqs;
   _VNGNewMsgRcvdLink_2   resps;
#endif

};


struct __VNGNewMsgWaiter {
   _VNGVnEventHead         ev;
   _VN_new_message_event  *vn_ev;

   _VNGNewMsgWaiter       *next;
   _VNGNewMsgWaiter       *prev;

   _VNGNewMsgWaiter       *last_same_svcType; // comment above
   _VNGNewMsgWaiter       *last_same_svcTag;
   _VNGNewMsgWaiter       *last_same_sender;

   _VNGNetNode             *vn;    // see note 1. below
    VNServiceTypeSet        serviceTypeSet;
    VNServiceTypeSet        ready;  // only used for output if byServiceType
    bool                    byServiceType;

    union {
       _VN_new_message_event   vn_ev;
        uint8_t all [ sizeof(_VN_new_message_event) + _VN_MAX_HDR_LEN ];
    } buf;

};

// Note 1.
// We have a problem with waiters because they can have
// wild cards for tohost and netid.
// If we are removing the waiter because we found a match,
// we know the vn is the one we were searching.
// But if the waiter times out, we need to find the vn
// whose waiter list contains this waiter. 
// We can handle a wild card netid becuase the only
// one is vn_any.  But if tohost is a wild card we can't hash to the
// _VNGNetNode.  So, I added a pointer to the vn in the waiter info.
// Thats ok, because if the vn is left, all waiters are removed
// and he can't time out.



typedef enum {
    _VNG_ML_NONE = 0,
    _VNG_ML_VN = 1,
    _VNG_ML_TO_NODE = 2,
    _VNG_ML_SVC_TYPE = 3,
    _VNG_ML_SVC_TAG = 4,
    _VNG_ML_SENDER = 5,
    _VNG_ML_CALLER_TAG = 6

} _VNG_MATCH_LEVEL;


// Discussion of use of _VNGRpc, _VNGRpcRcvd, and _VNGRpcWaiter
//
// _VNGRpcRcvd objects are usually referred to as rcvd.
// _VNGRpcWaiter objects are usually refeered to as waiter.
// _VNGRpc objects represent a registered RPC and are referred to as rpc.
//
// Registered RPCs are recorded in _VNGRpc objects kept in a hash map
// in the associated _VNGNetNode with the procId as the key to retrieve
// a pointer to the _VNGRpc object.
//
// If an RPC has been registered for a VN and a request is received while
// there is no VNG_ServeRPC() waiting, a _VNGRpcRcvd object is appended to
// a fifo queue of RPC requests waiting to be served.
//
// The pending RPC queue (rpcs waiting to be severed) is __vnp.rpcs_rcvd.
//
// If VNG_ServeRPC() is called while there is a pending RPC, the head RPC
// is removed from __vnp.rpcs_rcvd and the RPC is served immediately.
//
// If VNG_ServeRPC() is called with a timeout when there are no pending RPCs
// a _VNGRpcWaiter object is appended to a fifo queue (__vnp.rpc_waiters) of
// threads waiting to serve an RPC.  The _VNGRpcWaiter is a local variable
// of the VNG_ServeRPC() function.  The thread waits on a sync object in
// the _VNGRpcWaiter.
//
// If a request for a registered RPC is received while a
// VNG_ServeRPC() is waiting, the request info is recorded in the
// _VNGRpcWaiter object and the _VNGRpcWaiter sync object is signaled.
//
// Since the request did not need to be added to the pending RPCs queue
// the _VNGRpcRcvd was not created.
//
// If the associated VN is deleted or the RPC is unregistered, I can
// remove any associted _VNGRpcRcvd objects from the pending RPCs queue
// before releasing the vng mutex.
//
// No more requests will be queued for that RPC because the RPC will
// no longer be registered.
//
// If the VN was deleted it will no longer be in the vng vn hash map
// so no events will be dispatched for that VN.
//
// When a request for a registered RPC is received, recorded in a waiting
// _VNGRpcWaiter, and the _VNGRpcWaiter sync object is signaled; there is
// a window where other threads can run between the time the vng dispatch
// thread releases the vng mutex and the VNG_ServeRPC() re-acquires the mutex.
//
// The VN can be deleted or the RPC can be unregistered during that window,
// The VNG_ServeRPC() needs to verify that the VN is still valid and that
// the RPC is still registered after re-aquiring the vng mutex.
//
// I do not want to access via the vn pointer before verifying that it
// is still valid since the VN object could have deleted or reused.
//
// The rpc vn can be different from the rcvd req vn because the
// rpc could be registered with vn_any policy.  The mapping to
// the rcvd vn and the rpc vn and the rpc need to be returned
// in waiter such that the rcvd vn, rpc vn, and registered rpc
// can be verified to be still valid when the waiter thread
// re-aquires the vng mutex.
//
// To accomplish this the netid, hostid, vn_id, and procid are
// recorded in _VNGRpcRcvd and _VNGRpcWaiter.
//
// The vn->id is unique to each local vn join or creation so
// the vn can be positively id'd as the same one.
//
// The hash to a vn is based on netid/nodeid but can be verified
// to represent the exact vn join/create by checking the vn id.
//
// A short cut would be to just keep netid/nodeid if it were guaranteed
// that the  same netid/hostid pair will never again be applied to a
// new or joined VN on that host prior to reset of the VNG.  I
// don't feel comfortable assuming that.
//
// Once the vn pointer is obtained, the rpc is retrieved from the vn's
// registered rpcs hash map which uses the procId as the retrieval key.
//
// If it has been unregistered it will not be in the map.


typedef struct __VNGRpcRcvd   _VNGRpcRcvd;
typedef struct __VNGRpcWaiter _VNGRpcWaiter;

struct __VNGRpc {
   _VNGRpc           *next;
    VNServiceTag      procId;
    VNRemoteProcedure proc;

};


struct __VNGRpcRcvd {
   _VNGVnEventHead         ev;
   _VN_new_message_event  *vn_ev;
   _VNGVnEvent            *next;

   _VNGVnId       rcvd_vn_id;   // vn_ev netid/tohost completes mapping
   _VN_net_t      rpc_vn_netid; // mapping to vn where rpc was registered
   _VN_host_t     rpc_vn_self;  //   :   (can be a vn_any)
   _VNGVnId       rpc_vn_id;    //   :
    VNServiceTag  procId;       // for finding rpc in rpc_vn

};

struct __VNGRpcWaiter {
   _VNGVnEventHead         ev;
   _VN_new_message_event  *vn_ev;
   _VNGVnEvent            *next;

   _VNGVnId       rcvd_vn_id;   // vn_ev netid/tohost completes mapping
   _VN_net_t      rpc_vn_netid; // mapping to vn where rpc was registered
   _VN_host_t     rpc_vn_self;  //   :   (can be a vn_any)
   _VNGVnId       rpc_vn_id;    //   :
    VNServiceTag  procId;       // for finding rpc in rpc_vn

    union {
       _VN_new_message_event   vn_ev;
        uint8_t all [ sizeof(_VN_new_message_event) + _VN_MAX_HDR_LEN ];
    } buf;

};









// vnEvent functions

_VNGEvent* _vng_vnEvent_new  (VNG *vng, int size);

VNGErrCode _vng_vnEvent_free (VNG *vng, _VNGEvent* event);

VNGErrCode _vng_vnEvent_init (_VNGEvent           *vnEvent,
                               uint16_t            size,
                               const _VN_event_t  *vn_ev);


VNGErrCode _vng_vnEvents_insert (VNG        *vng,
                                _VNGNetNode *vn,
                                _VNGEvent   *vnEvent);

VNGErrCode _vng_vnEvents_remove (VNG        *vng,
                                _VNGNetNode *vn,
                                _VNGEvent   *vnEvent,
                                 bool        remove_to);


VNGErrCode  _vng_vnEvent_wait   (VNG        *vng,
                                _VNGNetNode *vn,
                                _VNGEvent   *waiter,
                                 VNGTimeout  timeout,
                                 bool        checkFirst);


VNGErrCode  _vng_evt_find_rcvd   (VNG         *vng,
                                 _VNGNetNode  *vn,
                                 _VNGEvent    *waiter);

VNGErrCode  _vng_evt_find_waiter (VNG         *vng,
                           const _VN_event_t  *vn_ev,  // in
                                 _VNGNetNode **vn);    // out

VNGErrCode
_vng_evt_s_list_insert_end (VNG           *vng,
                           _VNGVnEvent    *vnEvent,
                           _VNGVnEvtList  *list);

_VNGVnEvent*
_vng_evt_s_list_remove_head (VNG           *vng,
                            _VNGVnEvtList  *list,
                             bool           remove_to);


_VNGVnEvent*
_vng_evt_s_list_remove      (VNG           *vng,
                            _VNGVnEvent    *vnEvent,
                            _VNGVnEvtList  *list,
                             bool           remove_to);

VNGErrCode
_vng_evt_s_lists_remove_all (VNG            *vng,
                            _VNGVnEvtLists  *lists);

VNGErrCode
_vng_evt_s_list_remove_all  (VNG            *vng,
                            _VNGVnEvtList   *waiters,
                            _VNGVnEvtList   *received);

VNGErrCode
_vng_evt_s_list_remove_waiters (VNG          *vng,
                               _VNGVnEvtList *waiters);

VNGErrCode
_vng_evt_s_list_remove_rcvd   (VNG           *vng,
                              _VNGVnEvtList  *received);




// Currently there doesn't seem to be a need to order by call_id
// So _vng_evt_s_list_insert_by_call_id isn't impliemented.
#if 0
VNGErrCode
_vng_evt_s_list_insert_by_call_id (VNG           *vng,
                                  _VNGVnEvent    *vnEvent,
                                  _VNGVnEvtList  *evt_list);
#endif

_VNGVnEvent*
_vng_evt_s_list_remove_by_call_id (VNG           *vng,
                                  _VN_callid_t    call_id,
                                  _VNGVnEvtList  *evt_list,
                                   bool           remove_to);


VNGErrCode
_vng_evt_s_list_find_rcvd_by_call_id   (VNG          *vng,
                                       _VNGVnEvent   *vnEvent,
                                       _VNGVnEvtList *evt_list);

VNGErrCode
_vng_evt_s_list_find_waiter_by_call_id (VNG          *vng,
                                 const _VN_event_t   *vn_ev,
                                       _VNGVnEvtList *evt_list);

_VNGVnEvent*
_vng_evt_s_list_remove_by_ca_addr (VNG           *vng,
                                  _VN_addr_t      addr,
                                  _VNGVnEvtList  *list,
                                   bool           remove_to);

_VNGRpcRcvd*
_vng_evt_s_list_remove_by_procId (VNG           *vng,
                                  VNServiceTag   procId,
                                 _VNGVnEvtList  *list,
                                  bool           remove_to);

_VNGRpcRcvd*
_vng_evt_s_list_remove_by_vn_id (VNG           *vng,
                                _VNGVnId        vn_id,
                                _VNGVnEvtList  *list,
                                 bool           remove_to);

_VNGGameEvent*
_vng_evt_s_list_remove_by_eid (VNG           *vng,
                               VNGEventID     eid,
                              _VNGVnEvtList  *list,
                               bool           remove_to);


// _VN_EVT_NEW_MESSAGE routines

VNGErrCode
_vng_evt_msg_rcvd_insert   (VNG                   *vng,
                           _VNGNetNode            *vn,
                           _VNGNewMsgRcvd         *new_rcvd);

VNGErrCode
_vng_evt_msg_waiter_insert (VNG                 *vng,
                           _VNGNetNode          *vn,
                           _VNGNewMsgWaiter     *new_waiter);

VNGErrCode
_vng_evt_msg_remove        (VNG         *vng,
                           _VNGNetNode  *vn,
                           _VNGEvent    *vnEvent,
                            bool         remove_to);

VNGErrCode
_vng_evt_msg_rcvd_remove   (VNG                   *vng,
                           _VNGNewMsgRcvd        **list,
                           _VNGNewMsgRcvd         *rcvd,
                            bool                   remove_to);

VNGErrCode
_vng_evt_msg_waiter_remove (VNG                 *vng,
                           _VNGNewMsgWaiter    **list,
                           _VNGNewMsgWaiter     *waiter,
                            bool                 remove_to);

VNGErrCode
_vng_evt_msg_find_rcvd     (VNG                   *vng,
                           _VNGNetNode            *vn,
                           _VNGNewMsgWaiter       *waiter);

VNGErrCode
_vng_evt_msg_find_rcvd_by_time
                           (VNG                   *vng,
                           _VNGNetNode            *vn,
                           _VNGNewMsgWaiter       *waiter,
                           _VNGNewMsgRcvd        **found);

VNGErrCode
_vng_evt_msg_find_rcvd_by_svcType
                           (VNG                   *vng,
                           _VNGNetNode            *vn,
                           _VNGNewMsgWaiter       *waiter,
                           _VNGNewMsgRcvd        **found);

VNGErrCode
_vng_evt_msg_find_waiter (VNG                   *vng,
                         _VNGNetNode            *vn,
                   const _VN_new_message_event  *rcvd,
                          VNServiceType          rcvd_svcType);

VNGErrCode
_vng_evt_req_find_rcvd   (VNG                   *vng,
                         _VNGNetNode            *vn,
                         _VNGNewMsgWaiter       *waiter);

VNGErrCode
_vng_evt_req_find_waiter (VNG                   *vng,
                         _VNGNetNode            *vn,
                   const _VN_new_message_event  *rcvd);

VNGErrCode
_vng_evt_resp_find_rcvd   (VNG                  *vng,
                          _VNGNetNode           *vn,
                          _VNGNewMsgWaiter      *waiter);

VNGErrCode
_vng_evt_resp_find_waiter (VNG                  *vng,
                          _VNGNetNode           *vn,
                    const _VN_new_message_event *rcvd);

VNGErrCode
_vng_evt_req_find_rpc     (VNG                   *vng,
                          _VNGNetNode            *vn_rcvd,
                    const _VN_new_message_event  *rcvd);

VNGErrCode
_vng_evt_rpc_find_waiter  (VNG                   *vng,
                          _VNGNetNode            *vn_rcvd,
                    const _VN_new_message_event  *rcvd,
                          _VNGNetNode            *vn_rpc,
                           VNServiceTag           procId);

VNGErrCode
_vng_evt_rpc_find_rcvd   (VNG                    *vng,
                         _VNGRpcWaiter           *waiter);

uint32_t   _vng_rpc_hash       (_VNGNetNode *vn, VNServiceTag procId);
VNGErrCode _vng_rpc_map_insert (_VNGNetNode *vn, _VNGRpc *rpc);
_VNGRpc*   _vng_rpc_map_remove (_VNGNetNode *vn, VNServiceTag procId);
_VNGRpc*   _vng_procId2rpc     (_VNGNetNode *vn, VNServiceTag procId);

VNGErrCode _vng_rpc_map_remove_all (VNG *vng, _VNGNetNode *vn);


VNGErrCode _vng_recv_rpc (VNG               *vng,
                          VNRemoteProcedure *proc,     // out
                          VN                **vn_api,  // out
                          VNMsgHdr          *hdr,      // out
                          void             **args,     // in/out
                          size_t            *args_len, // in/out
                          VNGTimeout         timeout);
                          
// player invite                          
VNGErrCode
_vng_evt_notify_find_rcvd (VNG                   *vng,
                           _VNGNotifyWaiter *waiter);

VNGErrCode
_vng_evt_msg_find_notify_waiter (VNG                   *vng,
                                 const _VN_new_message_event  *ev);


// find rcvd by service type (currently not used)
VNGErrCode
_vng_evt_bst_find_rcvd (VNG               *vng,
                       _VNGNetNode        *vn,
                       _VNGNewMsgWaiter   *waiter);


VNGErrCode _vng_set_msg_hdr (VNG                   *vng,
                            _VN_new_message_event  *ev,    // in
                            _VNGNetNode            *to_vn, // in
                             VNMsgHdr              *hdr);  // out

void _vng_last_same_insert ( _VNGNewMsg  *new_entry,
                             _VNGNewMsg  *first_same,
                             _VNGNewMsg  *last_same,
                              int         last_same_index);

void _vng_last_same_remove ( _VNGNewMsg  *re,
                              bool        waiter);

// other ev related routines

VNGErrCode
_vng_evt_net_config (VNG                       *vng,
                    const _VN_net_config_event *vn_ev);

VNGErrCode
_vng_evt_net_disconnected (VNG                       *vng,
                    const _VN_net_disconnected_event *vn_ev);


// Connection request (new/join net request) routines

VNGErrCode
_vng_evt_new_net_request (VNG                       *vng,
                   const _VN_new_net_request_event  *ev);

VNGErrCode
_vng_evt_nr_find_rcvd (VNG                 *vng,
                      _VNGNewNetRequestEvent *waiter);
VNGErrCode
_vng_evt_conn_request (VNG                         *vng,
                const _VN_connection_request_event *ev);

VNGErrCode
_vng_evt_jr_find_rcvd (VNG                 *vng,
                      _VNGJoinRequestEvent *waiter);

VNGErrCode
_vng_evt_jr_remove (VNG          *vng,
                    _VNGNetNode  *vn,
                    _VNGEvent    *vnEvent,
                     bool         remove_to);

VNGErrCode
_vng_evt_msg_bs (VNG                   *vng,
          const _VN_new_message_event  *ev);

VNGErrCode
_vng_send_memb_uid (VNG                   *vng,
                   _VNGNetNode            *vn,
             const _VN_new_message_event  *rcvd);




// VNGEvent related functions (i.e. game events)

VNGErrCode
_vng_evt_gev_find_rcvd   (VNG            *vng,
                         _VNGGameEvent   *waiter);

VNGErrCode
_vng_gev_find_waiter (VNG       *vng,
                      VNGEvent   gev);

VNGErrCode
_vng_evt_msg_new_stored_msg (VNG                   *vng,
                      const _VN_new_message_event  *ev);


// vn event dispatcher

_VNPThreadRT  _VNPThreadCC  _vng_dispatch_evts ( VNG *vng );

VNGErrCode _vng_evt_buf_process (VNG *vng);
VNGErrCode _vng_dispatch_evt    (VNG         *vng,
                                _VN_event_t  *vn_ev,
                                 int          vnEventSize,
                                 VNGTimeout   evt_expiration);

VNCallerTag _vng_new_callerTag (VNG *vng);

const char* _vng_evt_name (uint16_t tag);


// vn event timeout queue

VNGTime      _vng_vn2vng_time (VNG *vng, _VN_time_t vn_timestamp);
_VN_time_t   _vng_vng2vn_time (VNG *vng, VNGTime vng_time);
VNGTime      _vng_get_time (VNG *vng);
VNGTimeOfDay _vng_to2time (VNG *vng, VNGTimeout timeout);
VNGTimeOfDay _vng_next_to_time (VNG *vng);
VNGTimeout   _vng_next_to (VNG *vng);
VNGErrCode   _vng_check_timeouts (VNG *vng);
VNGErrCode   _vng_register_timeout (VNG *vng, _VNGEvent *vnEvent);
VNGErrCode   _vng_remove_timeout (VNG *vng, _VNGEvent *vnEvent);


// misc net
uint32_t    _vng_net_node_hash (_VN_net_t netid, _VN_host_t node);
VNGErrCode  _vng_vn_map_insert (VNG *vng, _VNGNetNode* vn);
VNGErrCode  _vng_vn_map_remove (VNG *vng, _VNGNetNode* vn);

_VNGNetNode*  _vng_addr2vn       (_VN_addr_t addr);
_VNGNetNode*  _vng_netNode2vn    (_VN_net_t netid, _VN_host_t self);
_VNGNetNode*  _vng_netid2ownervn (_VN_net_t netid);
_VNGNetNode*  _vng_netNodeId2vn  (_VN_net_t netid, _VN_host_t self,
                                                   _VNGVnId vn_id);
_VNGNetNode*  _vng_addrId2vn (_VN_addr_t addr, _VNGVnId vn_id);

VNGErrCode  _vng_config_ports (VNG *vng, _VN_addr_t addr, int port_state);

VNGErrCode  _vng_leave_vn (VNG *vng, _VNGNetNode *vn);
VNGErrCode  _vng_free_vn (VNG *vng, _VNGNetNode *vn);
VNGErrCode  _vng_remove_vn_member (VNG *vng, _VNGNetNode *vn, VNMember memb);

VNGErrCode  _vng_sconnect (VNG           *vng,           // in
                           const char    *serverName,    // in
                           VNGPort        serverPort,    // in
                           const void    *msg,           // in   
                           _VN_msg_len_t  msglen,        // in
                           char          *denyReason,    // ptr in/content out 
                          _VN_msg_len_t   denyReasonLen, // in
                           VNGTimeout     timeout);

VNGErrCode  _vng_connect (VNG           *vng,           // in
                          VNId           vnId,          // in
                          const void    *msg,           // in
                         _VN_msg_len_t   msglen,        // in
                          char          *denyReason,    // ptr in/content out 
                         _VN_msg_len_t   denyReasonLen, // in
                          VN            *vn_api,        // out
                          VNGTimeout    timeout);


VNGErrCode  _vng_evt_new_connection (VNG                     *vng,
                                    _VN_new_connection_event *ev);

uint32_t _vng_get_class_netmask (VNClass vnClass);
VNClass  _vng_get_netmask_class (uint32_t netmask);
VNClass  _vng_get_vn_class (_VN_net_t net_id);

VNGErrCode  _vng_is_vnid_infra (VNId vnid, bool *is_infra);

bool _vng_is_net_host_valid (_VN_net_t net_id, _VN_host_t host_id);

VNGErrCode  _vng_vn_init_exited (VNG *vng, VN *vn_api, _VNGNetNode *vn);

VNGErrCode  _vng_vn_init (VNG        *vng,
                          VN         *vn_api,
                         _VNGNetNode *vnn,
                         _VN_net_t    netid,
                         _VN_host_t   self,
                         _VN_host_t   owner,
                          VNDomain    domain,
                          VNPolicies  policies,
                          uint32_t    flags);


_VN_port_t        _vng_svc2port (VNServiceType svc);
VNServiceType     _vng_port2Svc (_VN_port_t port);
VNServiceTypeSet  _vng_port2SvcSet (_VN_port_t port);
VNServiceTypeSet  _vng_svc2SvcSet (VNServiceType svc);
VNServiceTypeSet  _vng_svc2SvcMask (VNServiceType svc);


VNGErrCode _vng_send_msg (VN            *vn_api,
                          uint32_t       flags,
                          VNServiceType  svcType,
                          VNMember       memb,
                          VNServiceTag   serviceTag,
                          VNCallerTag   *callerTag, // out for req, in for resp
                          const void    *msg,
                          size_t         msglen,
                          VNAttr         msg_attr,
                          int32_t        optData,
                          VNGTimeout     timeout);

VNGErrCode _vng_recv_msg (VN            *vn_api,
                          uint32_t        flags,
                          VNServiceType   svcType,
                          VNMember        memb,
                          VNServiceTag    serviceTag,
                          VNCallerTag     callerTag,
                          void           *msg,
                          size_t         *msglen,
                          VNMsgHdr       *hdr,
                          VNGTimeout      timeout);

VNGErrCode _vng_vn_evt_msg_err (VNG               *vng,
                         const _VN_msg_err_event  *me);


// error related

VNGErrCode _vng_xec (VNGErrCode ec);
VNGErrCode _vng_vn2vng_err (int vn_rv);
VNGErrCode _vng_evt2vng_err (int errcode);  // _VN_EVT_ERR to VNGErrCode
VNGErrCode _vng_shr2vngErr (_SHRError shr_err);
VNGErrCode _vng_err_msg (VNGErrCode  errcode,
                         char       *msg,
                         size_t      msgLen);



// initialization

VNGErrCode _vnp_init_global_mutex();

VNGErrCode _vnp_init (VNG            *vng,
                     _VNPSession     *session,
               const  VNGInitParm    *parm,
               const  VNGServerParm  *svr,
                      const char     *server,
                      VNGPort         serverPort);

_VNPSession* _vnp_alloc_session (IOSUid   uid,
                                 IOSGid   gid,
                                 u32      flags);

VNGErrCode _vnp_release_session (_VNPSession *s);

VNGErrCode _vnp_alloc_session_without_rm (VNG         *vng,
                                         _VNPSession **session);

_VNPSession*  _vnp_vngId2Session (_VNGId vng_id);

int _vnp_get_num_sessions ();

int _vn_init_server(const char* vn_server, uint16_t port); // vn func



// locking and checking

VNGErrCode  _vng_check_exit(VNG *vng);
VNGErrCode  _vng_exit_sync(VNG *vng);
VNGErrCode  _vng_check_state (VNG *vng);
VNGErrCode  _vng_check_vn_state (_VNGNetNode *vn);
VNGErrCode  _vng_lock (VNG *vng);
VNGErrCode  _vng_lock_vn (VN *vn_api, _VNGNetNode **vn);
VNGErrCode  _vng_lock_vnn (_VNGNetNode *vn);
VNGErrCode  _vng_check_login (VNG *vng);






// other misc

VNGErrCode
_vng_get_adhoc_games (VNG                *vng,
                      VNGSearchCriteria  *searchCriteria,
                      VNGGameStatus      *gameStatus,
                      uint32_t           *nGameStatus,  // in & out
                      uint32_t           *skipN);       // in & out

bool  _vng_apply_pred (uint32_t    cmp,   // VNGCompare
                       int32_t     gameAttr,
                       int32_t     cmpValue);

bool  _vng_cmp_keyword (uint32_t   cmp,   // VNGCompare
                        char       gameKeyword[VNG_GAME_KEYWORD_LEN],
                        char       seaKeyword[VNG_GAME_KEYWORD_LEN]);

VNGMillisec  _vng_memb_latency (_VN_net_t netid, VNMember memb);

_VN_time_t  _vng_msg_ts_2vn_time ( uint32_t   msg_ts,
                                  _VN_time_t  recv_time,
                                  _VN_net_t   netid,
                                  _VN_host_t  from);


int _vng_pack_adhoc_game (const _VNGAdhocGame *g, uint8_t b[sizeof *g]);
int _vng_unpack_adhoc_game (_VNGAdhocGame *g, const uint8_t *b, uint32_t len_b);


typedef IOSError (*VNPRmApiWrapper) (IOSResourceIoctlv *ioctlv, _VNPSession *s);

VNPRmApiWrapper  _vnp_rm_getApiWrapper (u32 func_id);


static inline uint64_t guidToDeviceId (_VN_guid_t guid)
{
    return (((uint64_t)guid.device_type) << 32) + guid.chip_id;
}

static inline uint32_t deviceIdToChipId (uint64_t deviceId)
{
    return (uint32_t) deviceId;
}

static inline int32_t deviceIdToDeviceType (uint64_t deviceId)
{
    return (int32_t) (deviceId >> 32);
}


// Pack data in little endian format.

static inline size_t VNP_EncodeUint16(uint8_t *buffer, uint16_t v)
{
    // *((uint16_t*)buffer) = v; doesn't work on NC can't write to odd addr
    buffer[0] = (uint8_t)(v & UINT32_C(0xff));        // Least significant
    buffer[1] = (uint8_t)((v >> 8) & UINT32_C(0xff)); // Most significant
    return sizeof v;
}

static inline size_t VNP_EncodeUint32(uint8_t *buffer, uint32_t v)
{
    // *((uint32_t*)buffer) = v;
    buffer[0] = (uint8_t)(v & UINT32_C(0xff));        // Least significant
    buffer[1] = (uint8_t)((v >> 8) & UINT32_C(0xff));
    buffer[2] = (uint8_t)((v >> 16) & UINT32_C(0xff));
    buffer[3] = (uint8_t)((v >> 24) & UINT32_C(0xff));// Most significant
    return sizeof v;
}

static inline size_t VNP_EncodeUint64(uint8_t *buffer, uint64_t v)
{
    // *((uint64_t*)buffer) = v;
    buffer[0] = (uint8_t)(v & UINT64_C(0xff));        // Least significant
    buffer[1] = (uint8_t)((v >> 8) & UINT64_C(0xff));
    buffer[2] = (uint8_t)((v >> 16) & UINT64_C(0xff));
    buffer[3] = (uint8_t)((v >> 24) & UINT64_C(0xff));
    buffer[4] = (uint8_t)((v >> 32) & UINT64_C(0xff));
    buffer[5] = (uint8_t)((v >> 40) & UINT64_C(0xff));
    buffer[6] = (uint8_t)((v >> 48) & UINT64_C(0xff));
    buffer[7] = (uint8_t)((v >> 56) & UINT64_C(0xff));// Most significant
    return sizeof v;
}

static inline size_t VNP_EncodeInt16(uint8_t *buffer, int16_t i)
{
    return VNP_EncodeUint16(buffer, (uint16_t)i);
}
static inline size_t VNP_EncodeInt32(uint8_t *buffer, int32_t i)
{
    return VNP_EncodeUint32(buffer, (uint32_t)i);
}
static inline size_t VNP_EncodeInt64(uint8_t *buffer, int64_t i)
{
    return VNP_EncodeUint64(buffer, (uint64_t)i);
}

static inline size_t VNP_DecodeUint16(const uint8_t *enc, uint16_t *i)
{
    // *i = *((uint16_t*)enc);
    *i = (uint16_t)(((uint32_t)enc[1] << 8) + (uint32_t)enc[0]);
    return sizeof *i;
}

static inline size_t VNP_DecodeUint32(const uint8_t *enc, uint32_t *i)
{
    // *i = *((uint32_t*)enc);
    *i = (((uint32_t)enc[3] << 24) +
          ((uint32_t)enc[2] << 16) +
          ((uint32_t)enc[1] << 8)  +
           (uint32_t)enc[0]);
    return sizeof *i;
}

static inline size_t VNP_DecodeUint64(const uint8_t *enc, uint64_t *i)
{
    // *i = *((uint64_t*)enc);
    *i = (((uint64_t)enc[7] << 56) +
          ((uint64_t)enc[6] << 48) +
          ((uint64_t)enc[5] << 40) +
          ((uint64_t)enc[4] << 32) +
          ((uint64_t)enc[3] << 24) +
          ((uint64_t)enc[2] << 16) +
          ((uint64_t)enc[1] << 8)  +
           (uint64_t)enc[0]);
    return sizeof *i;
}

static inline size_t VNP_DecodeInt16(const uint8_t *enc, int16_t *i)
{
    return VNP_DecodeUint16(enc, (uint16_t*)i);
}
static inline size_t VNP_DecodeInt32(const uint8_t *enc, int32_t *i)
{
    return VNP_DecodeUint32(enc, (uint32_t*)i);
}
static inline size_t VNP_DecodeInt64(const uint8_t *enc, int64_t *i)
{
    return VNP_DecodeUint64(enc, (uint64_t*)i);
}

#define htoel(x) (x)
#define etohl(x) (x)


#ifdef _SC
    _VNPSemaphore* vnpGetRMQThreadSem();
    extern  IOSMessageQueueId  __vnpRMQId;
#else
    #define vnpGetRMQThreadSem()   NULL
    #define __vnpRMQId             IOS_ERROR_NOEXISTS
#endif


extern _VNP  __vnp;

extern _VNPSession __vnp_sessions[_VNP_MAX_SESSIONS];

extern _VNPMutex  __vnp_mutex;

#ifdef  __cplusplus
}
#endif


#if !defined(_GBA) && !defined(_SC)
#pragma pack(pop)
#endif


#endif // __VNP_I_H__

//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//


#include "vnp_i.h"



#define _VNG_OPTHDR_TS_BIT     0x01
#define _VNG_OPTHDR_OD_BIT     0x02




// for _vng_send_msg:
//
// req and resp require non-null callerTag
// resp requires *callerTag in valid  range
// callerTag is generated for req, sent in opt hdr and returned in *callerTag
// callerTag is ignored for msgs
//
// if optData is 0,
//     don't send in opthdr and put 0 in VNMsgHdr
// else
//     send optData in opthdr and put it in VNMsgHdr
//
// opt hdr info:
//
// The serviceTag is always first for all service types.
//
// Request and Response always have a callerTag following serviceTag. If
// there is exactly 4 bytes following callerTag, it is optData. If no
// opthdr bytes follow callerTag, optData is 0. If other than 4 or 0 bytes
// follow callerTag, the byte after callerTag is a flag byte indicating
// what follows in the opthdr.
//
// For msgs; if there is exactly 4 bytes following service tag, it is a
// timestamp. If there is not exactly 4 bytes following service tag,
// the first byte following the service tag is a flag byte indicating
// what follows in the opthdr.
//
// The flag byte is never required with the current client APIs but
// is defined to allow additional data to be passed that is backward
// compatible with the current scheme. For example, using this scheme,
// a send timestamp could be added to requests and additonal optData
// could be added to msgs without breaking the scheme. A vng that only
// knows about the current flag byte definitions would be able to receive
// the currently defined parameters, but would ignore additional flag bits
// and additional opthdr content. 
//
// flags bit 0 == 1 means timestamp follows flag byte.
// flags bit 1 == 1 means optData is present next
//
// So, if there is a timestamp, it always prececdes optData.
// If there is no optData in the opt hdr, optData is 0.
//
// Possible future expansion:
// flags bit 1 == 1 means optData[0] is present next
// flags bit 2 == 1 means optData[1] is present next
// etc.
//


VNGErrCode _vng_send_msg (VN            *vn_api,
                          uint32_t       flags,
                          VNServiceType  svcType,
                          VNMember       memb,
                          VNServiceTag   serviceTag,
                          VNCallerTag   *callerTag,  //out for req, in for resp
                          const void    *msg,
                          size_t         msglen,
                          VNAttr         msg_attr,
                          int32_t        optData,
                          VNGTimeout     timeout)
{
    VNGErrCode      ec = VNG_OK;
    int             rv;
    VNG            *vng;
   _VNGTimerEvent   waiter;
   _VN_port_t       port;
    uint32_t        vn_attr;
    uint8_t         hdr[_VN_MAX_HDR_LEN];
    uint8_t         hdr_len;
    bool            locked_on_entry = false;
    VNGTimeout     _timeout = timeout;
   _VN_time_t       endTime = 0;
   _VN_time_t       now;
    uint8_t         hf = 0;  // optHdr flags byte
   _VNGNetNode     *vn;

    if (timeout != VNG_NOWAIT)
        endTime = _VN_get_timestamp() + _timeout;

    if (flags & _VNG_MSG_LOCKED_FLAG) {
        locked_on_entry = true;
        vn = _vng_addrId2vn (vn_api->addr, vn_api->id);
        if ((ec = _vng_check_vn_state (vn))) {
            return ec;;
        }
    }

    if (!locked_on_entry && (ec = _vng_lock_vn (vn_api, &vn)))
        return ec;

    vng = vn->vng;

    _vnp_assert_locked (__vnp.mutex);

    if (vn->netid == _VN_NET_ANY) {
        ec = VNGERR_INVALID_VN;
    }
    else if (memb > _VNG_VN_MEMBER_MAX 
               && !(   memb == VN_MEMBER_SELF
                    || memb == VN_MEMBER_OWNER
                    || (svcType <  VN_SERVICE_TYPE_REQUEST
                           && (   memb == VN_MEMBER_OTHERS
                               || memb == VN_MEMBER_ANY)))) {
        ec = VNGERR_INVALID_MEMB;
    }
    else if (msglen > VN_MAX_MSG_LEN || (!msg && msglen)) {
        ec = VNGERR_INVALID_LEN;
    }
    else if ((svcType == VN_SERVICE_TYPE_REQUEST
                || svcType == VN_SERVICE_TYPE_RESPONSE)
                    &&  !callerTag) {
        ec = VNGERR_INVALID_ARG;
    }
    else if (svcType == VN_SERVICE_TYPE_RESPONSE
                 &&  (   *callerTag  < _VNG_CALLER_TAG_MIN
                      || *callerTag  > _VNG_CALLER_TAG_MAX)) {
        ec = VNGERR_INVALID_ARG;
    }
    else if ((msg_attr & VN_ATTR_NOENCRYPT)
                && (msg_attr & VN_ATTR_ENCRYPT)) {
        ec = VNGERR_INVALID_ARG;
    }


    if (ec) {
        if (!locked_on_entry)
            _vnp_mutex_unlock (__vnp.mutex);
        return ec;
    }

   _vng_vnEvent_init ((_VNGEvent*) &waiter, sizeof waiter, NULL);

    waiter.vn_ev = &waiter.buf.vn_ev;
    waiter.vn_ev->tag  = _VNG_EVT_TIMER;


    if (svcType == VN_SERVICE_TYPE_REQUEST) {
        // callerTag is generated and returned to caller
        *callerTag = _vng_new_callerTag (vng);
    }

    // see optHdr comments above

    hdr_len  = (uint8_t) VNP_EncodeUint16 (hdr, serviceTag);

    if (svcType == VN_SERVICE_TYPE_REQUEST
            || svcType == VN_SERVICE_TYPE_RESPONSE) {

        hdr_len += (uint8_t) VNP_EncodeUint16 (&hdr[hdr_len], *callerTag);

        if (msg_attr & VN_ATTR_TIMESTAMP) {
            hf |= _VNG_OPTHDR_TS_BIT;
            if (optData) {
                hf |= _VNG_OPTHDR_OD_BIT;
            }
        } else if (optData) {
            hdr_len += (uint8_t) VNP_EncodeInt32  (&hdr[hdr_len], optData);
        }
    }
    else {
        // reliable or unreliable msg
        if (optData) {
            hf |= _VNG_OPTHDR_OD_BIT;
            if (msg_attr & VN_ATTR_TIMESTAMP) {
                hf |= _VNG_OPTHDR_TS_BIT;
            }
        } else if (msg_attr & VN_ATTR_TIMESTAMP) {
            hdr_len += (uint8_t) VNP_EncodeUint32 (&hdr[hdr_len], 
                                                (uint32_t) _VN_get_timestamp());
        }
    }

    if (hf) {
        hdr[hdr_len++] = hf;
        if (hf & _VNG_OPTHDR_TS_BIT) {
            hdr_len += (uint8_t) VNP_EncodeUint32 (&hdr[hdr_len], 
                                                (uint32_t) _VN_get_timestamp());
        }
        if (hf & _VNG_OPTHDR_OD_BIT) {
            hdr_len += (uint8_t) VNP_EncodeInt32  (&hdr[hdr_len], optData);
        }
    }

    // See port comments in vnp_i.h  
    port = _vng_svc2port (svcType);
    port |= flags & _VNG_2_VNG_PORT_FLAG;


    if (msg_attr & VN_ATTR_UNRELIABLE) {
        vn_attr = 0;
    } else {
        vn_attr = _VN_MSG_RELIABLE;
    }

    if (((vn->policies & VN_MSG_ENCRYPT) && !(msg_attr & VN_ATTR_NOENCRYPT))
            || (msg_attr & VN_ATTR_ENCRYPT)) {
        vn_attr |= _VN_MSG_ENCRYPTED;
    }

    // _VN_send will return errors for invalid VN, memb, etc
    // _VN_send understands VN_MEMBER_SELF, VN_MEMBER_OTHERS,
    //  VN_MEMBER_ALL, and VN_MEMBER_ANY
    //  Resolve self before calling _VN_send for the case
    //  of multiple net members on the same device.

    if (memb == VN_MEMBER_OWNER)
        memb = vn->owner;
    else if (memb == VN_MEMBER_SELF)
        memb = vn->self;

    while (true) {
        rv = _VN_send (vn->netid, vn->self, memb, port,
                       msg, (_VN_msg_len_t) msglen, hdr, hdr_len, vn_attr);

        if (rv >= 0 || rv != _VN_ERR_FULL || timeout == VNG_NOWAIT) {
            ec = _vng_vn2vng_err (rv);
            break;
        }
        now = _VN_get_timestamp();
        if (now >= endTime) {
            ec = VNGERR_BUF_FULL;
            break;
        }
        _timeout = (VNGTimeout) (endTime - now);
        if (_timeout > _VNG_SEND_SLEEP_TIME) {
            _timeout = _VNG_SEND_SLEEP_TIME;
        }
        if ((ec = _vng_vnEvent_wait (vng, vn,
                                       (_VNGEvent*) &waiter, _timeout, false))
                   != VNGERR_TIMEOUT)
            break;
    }

    if (!locked_on_entry)
        _vnp_mutex_unlock (__vnp.mutex);

    trace (FINE, P2P, "send msg vn %p  netid 0x%08x  owner %u  "
                      "from %u  to %u  "
                      "msglen %u, port %u  svcTag %u  VNAttr 0x%x  "
                      "timeout %d  optData %d  callerTag %u\n",
                     vn, vn->netid, vn->owner, vn->self, memb,
                     msglen, port, serviceTag, msg_attr,
                     timeout, optData, callerTag ? *callerTag : 0);

    return ec;
}




VNGErrCode _vng_recv_msg (VN              *vn_api, 
                          uint32_t         flags,
                          VNServiceType    svcType,
                          VNMember         memb,
                          VNServiceTag     serviceTag,
                          VNCallerTag      callerTag,
                          void            *msg,
                          size_t          *msglen,
                          VNMsgHdr        *hdr,
                          VNGTimeout       timeout)
{
    VNGErrCode        ec = VNG_OK;
    VNG              *vng;
   _VNGNewMsgWaiter   waiter;
    uint8_t           opthdr_len;
    bool              locked_on_entry = false;
   _VNGNetNode       *vn;

    if (flags & _VNG_MSG_LOCKED_FLAG) {
        locked_on_entry = true;
        vn = _vng_addrId2vn (vn_api->addr, vn_api->id);
        if ((ec = _vng_check_vn_state (vn))) {
            return ec;;
        }
    }

    if (!locked_on_entry && (ec = _vng_lock_vn (vn_api, &vn)))
        return ec;

    vng = vn->vng;

    if (svcType == VN_SERVICE_TYPE_RESPONSE
            &&  vn->netid == _VN_NET_ANY) {
        ec = VNGERR_INVALID_VN;
    }
    else if (memb > _VNG_VN_MEMBER_MAX 
               && !(   memb == VN_MEMBER_SELF
                    || memb == VN_MEMBER_OWNER
                    || (svcType != VN_SERVICE_TYPE_RESPONSE
                           && (   memb == VN_MEMBER_OTHERS
                               || memb == VN_MEMBER_ANY)))) {
        ec = VNGERR_INVALID_MEMB;
    }
    else if (vn->netid == _VN_NET_ANY  &&  memb != VN_MEMBER_ANY) {
        ec = VNGERR_INVALID_MEMB;
    }
    else if (!msg && msglen && *msglen)
        ec = VNGERR_INVALID_LEN;
    else if (svcType == VN_SERVICE_TYPE_RESPONSE
               && (   serviceTag == VN_SERVICE_TAG_ANY
                   || callerTag  < _VNG_CALLER_TAG_MIN
                   || callerTag  > _VNG_CALLER_TAG_MAX)) {
        // any serviceTag is OK including VN_SERVICE_TAG_ANY
        // except when service type resp
        ec = VNGERR_INVALID_ARG;
    }

    if (ec) {
        if (!locked_on_entry)
            _vnp_mutex_unlock (__vnp.mutex);
        return ec;
    }

   _vng_vnEvent_init ((_VNGEvent*) &waiter, sizeof waiter, NULL);

    waiter.vn_ev = &waiter.buf.vn_ev;
    waiter.vn_ev->event.tag  = _VN_EVT_NEW_MESSAGE;

    waiter.vn_ev->netid = vn->netid;

    if (vn->netid == _VN_NET_ANY)
        waiter.vn_ev->tohost = VN_MEMBER_ANY;
    else
        waiter.vn_ev->tohost = vn->self;

    // Only actual node id is allowed in fromhost for resp,
    // but VN_MEMBER_OTHERS or VN_MEMBER_ANY are allowed
    // when finding  or waiting for messages or reqs.
    if (memb == VN_MEMBER_OWNER)
        waiter.vn_ev->fromhost = vn->owner;
    else if (memb == VN_MEMBER_SELF)
        waiter.vn_ev->fromhost = vn->self;
    else
        waiter.vn_ev->fromhost = memb;

    // serviceTag applies to messages and requests
    // callerTag  applies only to reponses
    // Since this is waiter info, timestamp is NA
    // hdr_len not really needed but set for completeness

    opthdr_len  = (uint8_t) VNP_EncodeUint16 (waiter.vn_ev->opthdr,
                                              serviceTag);
    opthdr_len += (uint8_t) VNP_EncodeUint16 (&waiter.vn_ev->opthdr[opthdr_len],
                                              callerTag);
    waiter.vn_ev->opthdr_size = opthdr_len;

    // We only care about the lsb of the waiter port to distinquish vng2vng 
    waiter.vn_ev->port = flags & _VNG_2_VNG_PORT_FLAG;

    // We don't distinguish between reliable
    // and unreliable messages on receive.

    // We do care about service type set
    waiter.serviceTypeSet = _vng_svc2SvcSet (svcType);

    // check and wait for _VN_EVT_NEW_MESSAGE or error 

    ec = _vng_vnEvent_wait (vng, vn, (_VNGEvent*)  &waiter, timeout, true);

    if (!ec) {
       _VN_msg_len_t msgLen;
        int          len;

        if (!msglen) {
            msgLen = 0;
        }
        else if (*msglen > UINT16_MAX) {
            msgLen = UINT16_MAX;
        }
        else {
            msgLen = (_VN_msg_len_t) *msglen;
        }

        len = _VN_recv (waiter.vn_ev->msg_handle, msg, msgLen);
        if (len < 0) {
            ec = VNGERR_UNKNOWN;
        }
        else {
           _VN_net_t     netid = waiter.vn_ev->netid;
           _VN_host_t    tohost = waiter.vn_ev->tohost;
           _VNGNetNode  *to_vn =_vng_netNode2vn (netid, tohost);
           _VN_new_message_event   *ev = waiter.vn_ev;

           if (!to_vn)
                ec = VNGERR_INVALID_VN;
           else if (!hdr ||
                    !(ec = _vng_set_msg_hdr (vng, ev, to_vn, hdr))) {
                if (msglen)
                    *msglen = len;
                if (hdr) {
                    trace (FINE, P2P,
                           "recv msg vn %p  netid 0x%08x  owner %u  "
                           "from %u  to %u  "
                           "msglen %u, port %u  svcTag %u  VNAttr 0x%x  "
                           "send time %d  optData %d  callerTag %u\n",
                           hdr->vn, netid, to_vn->owner,
                           hdr->sender, hdr->receiver,
                           hdr->payloadSize, ev->port,
                           hdr->serviceTag, hdr->attr, hdr->sendTimestamp,
                           hdr->optData, hdr->callerTag);
                } else {
                    trace (FINE, P2P,
                           "recv msg vn %p  netid 0x%08x  owner %u  "
                           "from %u  to %u  "
                           "msglen %u, port %u\n",
                           to_vn->vn_api, netid, to_vn->owner,
                           ev->fromhost, tohost,
                           ev->size, ev->port);
                }
            }
        }
    }

    if (ec && msglen) {
        *msglen = 0;
    }

    if (!locked_on_entry)
        _vnp_mutex_unlock (__vnp.mutex);

    if (ec) {
        trace (FINE, P2P, "recv msg failure: %d  vn %p  netid 0x%08x  owner %u  "
                          "from %u  to %u  "
                          "port %u  svcTag %u  callerTag %u  timeout %d\n",
                          ec, vn, vn->netid, vn->owner,
                          waiter.vn_ev->fromhost, waiter.vn_ev->tohost,
                          waiter.vn_ev->port, serviceTag, callerTag, timeout);
    }

    return _vng_xec(ec);
}





// _vng_msg_ts_2vn_time converts a 32 bit msg timestamp to 64 bit vn time
//
_VN_time_t  _vng_msg_ts_2vn_time ( uint32_t   msg_ts,
                                  _VN_time_t  recv_time,
                                  _VN_net_t   netid,
                                  _VN_host_t  from)
{
   _VN_time_t base, ts, mask;
    uint32_t  rt_32, ts_32;

    mask    =  _VNG_32_BIT_MASK_ULL;
    base    =  recv_time & ~mask; 
    ts      = _VN_conv_timestamp (_VN_make_addr(netid, from), base + msg_ts);
    ts_32   =  (uint32_t) (ts  & mask);
    rt_32   =  (uint32_t) (recv_time & mask);
    ts      =  base + ts_32;

    if (ts_32 > rt_32) {
        if ((ts_32 - rt_32) > (_VNG_32_BIT_MASK >> 1)) {
            ts -= (1LL << 32);
        }
    }
    else {
        if ((rt_32 - ts_32) > (_VNG_32_BIT_MASK >> 1)) {
            ts += (1LL << 32);
        }
    }
    return ts;
}


// See comments on opt hdr content at start of file
//
VNGErrCode _vng_set_msg_hdr (VNG                    *vng,
                            _VN_new_message_event   *ev,     // in
                            _VNGNetNode             *to_vn,  // in
                             VNMsgHdr               *hdr)    // out
{
    VNGErrCode  ec = VNG_OK;

    bool has_callerTag = false;
    bool has_timestamp = false;
    bool has_optData   = false;
    bool has_flags     = false;

    size_t opthdr_pos = 0;

    memset (hdr, 0, sizeof *hdr);

    if (!to_vn)
        return VNGERR_INVALID_VN;

    hdr->vn = to_vn->vn_api;

    hdr->sender = ev->fromhost;  // always actual node id
    hdr->receiver = ev->tohost;

    hdr->serviceType = _vng_port2Svc (ev->port);

    if (ev->opthdr_size > _VNG_OPTHDR_SVC_TAG_OFFSET) {
        opthdr_pos = VNP_DecodeUint16 (&ev->opthdr[0], &hdr->serviceTag);
    } else {
        trace (ERR, P2P, "opthdr has no serviceTag\n");
        ec = VNGERR_UNKNOWN;
    }

    switch (hdr->serviceType) {
        case VN_SERVICE_TYPE_UNRELIABLE_MSG:
        case VN_SERVICE_TYPE_RELIABLE_MSG:
            if (ev->opthdr_size - _VNG_OPTHDR_MSG_MIN_LEN == sizeof(uint32_t))
                has_timestamp = true;
            else if (ev->opthdr_size > _VNG_OPTHDR_MSG_MIN_LEN)
                has_flags = true;
            break;

        case VN_SERVICE_TYPE_REQUEST:
        case VN_SERVICE_TYPE_RESPONSE:
            has_callerTag = true;
            if (ev->opthdr_size == _VNG_OPTHDR_REQ_RESP_MIN_LEN)
                 break;

            if (ev->opthdr_size - _VNG_OPTHDR_REQ_RESP_MIN_LEN == sizeof(uint32_t))
                has_optData = true;
            else if (ev->opthdr_size > _VNG_OPTHDR_REQ_RESP_MIN_LEN)
                has_flags = true;
            else {
                trace (ERR, P2P, "opthdr has no callerTag\n");
                has_callerTag = false;
                ec = VNGERR_UNKNOWN;
            }
            break;

        default:
            trace (ERR, P2P, "opthdr has unknown port %u, serviceType %u\n",
                              ev->port, hdr->serviceType);
            ec = VNGERR_UNKNOWN;
            break;
    }

    if (has_callerTag)
        opthdr_pos += VNP_DecodeUint16 (&ev->opthdr[opthdr_pos], &hdr->callerTag);

    if (has_flags) {
        uint8_t hf = ev->opthdr[opthdr_pos++];
        if (hf & _VNG_OPTHDR_TS_BIT)
            has_timestamp = true;
        if (hf & _VNG_OPTHDR_OD_BIT)
            has_optData = true;
    }

    hdr->attr = 0;

    if (has_timestamp) {
        uint32_t ts_32;
        hdr->attr |= VN_ATTR_TIMESTAMP;
        opthdr_pos += VNP_DecodeUint32 (&ev->opthdr[opthdr_pos], &ts_32);
        hdr->sendTimestamp = _vng_vn2vng_time (vng, _vng_msg_ts_2vn_time (
                                                         ts_32,
                                                         ev->recv_time,
                                                         ev->netid,
                                                         ev->fromhost));
    }

    if (has_optData)
        opthdr_pos += VNP_DecodeInt32 (&ev->opthdr[opthdr_pos], &hdr->optData);

    if (ev->opthdr_size > opthdr_pos)
        trace (INFO, P2P, "Opt hdr has more bytes then expected.  % > %u\n",
                          ev->opthdr_size, (uint32_t) opthdr_pos);

    if (hdr->serviceType == VN_SERVICE_TYPE_UNRELIABLE_MSG)
        hdr->attr |= VN_ATTR_UNRELIABLE;

    if (ev->attr & _VN_MSG_ENCRYPTED)
        hdr->attr |= VN_ATTR_ENCRYPT;
    else
        hdr->attr |= VN_ATTR_NOENCRYPT;
    

    hdr->payloadSize = ev->size;

    return ec;
}








// _vng_evt_msg_bs
//
//  A msg with _VNG_PLAYER_STATUS was received.
//
//  Extract the VNGUserId from the msg
//
//  make a buddyStatus VNGEvent
//
//      eid = VNG_EVENT_BUDDY_STATUS
//      uid = VNGUserId from the msg
//
//  if there is a gev waiter
//       pass the VNGEvent to the waiter
//  else
//       put the VNGEvent in the _VNGGameEvent queue with timeout
//  return _VNG_OK so _vng_dispatch_evt won't try to queue the msg.
//
VNGErrCode
_vng_evt_msg_bs (VNG                   *vng,
          const _VN_new_message_event  *ev)
{
    int            rv = 0;
    VNGEvent       gev;
    VNGUserId      uid;
   _VN_msg_len_t   uidLen   = sizeof uid;
 

    if (ev->size < uidLen ||
          !ev->msg_handle ||
             uidLen > (rv=_VN_recv (ev->msg_handle, &uid, uidLen))) {
        trace (ERR, EVD,
            "Update Buddy Status msg:  "
            "size %u  handle %p  _VN_recv %d  "
            "netid 0x%08x  from %u  to %u  port %u\n",
            ev->size, ev->msg_handle, rv,
            ev->netid, ev->fromhost, ev->tohost, ev->port);

        return VNGERR_UNKNOWN;
    }

    trace (FINE, EVD,
           "Update Buddy Status msg rcvd for uid 0x%08x  "
           "netid 0x%08x  from %u  to %u  port %u\n",
           uid,
           ev->netid, ev->fromhost, ev->tohost, ev->port);

    gev.eid = VNG_EVENT_BUDDY_STATUS;
    gev.evt.buddyStatus.uid = uid;

    _vng_gev_find_waiter (vng, gev);

    return VNG_OK; 
}






// _vng_send_memb_uid
//
//  A vng to vng msg with _VNG_ST_GET_USERID was received.
//
//  Send the vng VNGUserId to the requesting vn member
//
//  return _VNG_OK so _vng_dispatch_evt won't try to queue the msg.
//
VNGErrCode
_vng_send_memb_uid (VNG                   *vng,
                   _VNGNetNode            *vn,
             const _VN_new_message_event  *rcvd)
{
    VNGErrCode  ec;
    uint8_t     buf[sizeof(uint64_t)];

    VNP_EncodeUint64 (buf, __vnp.userId);

    if ((ec = _vng_send_msg (vn->vn_api,
                            _VNG_2_VNG_PORT_FLAG | _VNG_MSG_LOCKED_FLAG,
                             VN_SERVICE_TYPE_RELIABLE_MSG,
                             rcvd->fromhost,
                            _VNG_ST_USERID,
                            _VNG_CALLER_TAG_NOT_APPLICABLE,
                             buf,
                             sizeof buf,
                             VN_DEFAULT_ATTR,
                             0,
                             VNG_NOWAIT))) {

    }

    return VNG_OK;
}






VNGErrCode  _vng_vn_evt_msg_err (VNG               *vng,
                          const _VN_msg_err_event  *me)
{
    union {
        uint32_t  u32;
        uint16_t  u16;

    } svcTag = {0};

    if (me->opthdr_size > _VNG_OPTHDR_SVC_TAG_OFFSET)
        VNP_DecodeUint16 (&me->opthdr[_VNG_OPTHDR_SVC_TAG_OFFSET], &svcTag.u16);
    else
        svcTag.u32 = 0x10000; // not a valid svcTag becuase > 65535

    trace (WARN, EVD, "_vng_evt_find_waiter  _VN_EVT_MSG_ERR  "
                      "VN error code %d  call id %d  netid 0x%08x  "
                      "from %u  to %u  port %u  svcTag %u  VNAttr 0x%x\n",
                    me->errcode, me->event.call_id, me->netid,
                    me->fromhost, me->tohost, me->port,
                    svcTag.u32, me->attr);
    // Since currently we never check __vnp.msg_err,
    // VNG_OK is returned to discard the err msg
    // Return VNGERR_NOT_FOUND to add the err msg to __vnp.msg_err
    return VNG_OK;
}




// _vng_evt_msg_new_stored_msg
//
//  A msg with _VNG_NEW_STORED_MSG was received.
//
//  make a buddyStatus VNGEvent
//
//      eid = VNG_EVENT_NEW_STORED_MSG
//
//  if there is a gev waiter
//       pass the VNGEvent to the waiter
//  else
//       put the VNGEvent in the _VNGGameEvent queue with timeout
//  return _VNG_OK so _vng_dispatch_evt won't try to queue the msg.
//
VNGErrCode
_vng_evt_msg_new_stored_msg (VNG                   *vng,
                      const _VN_new_message_event  *ev)
{
    VNGEvent       gev;
 

    if (ev->msg_handle && ev->size) {
        _VN_recv (ev->msg_handle, NULL, 0); // discard
    }

    trace (FINE, EVD,
           "_VNG_NEW_STORED_MSG received  "
           "netid 0x%08x  from %u  to %u  port %u\n",
           ev->netid, ev->fromhost, ev->tohost, ev->port);

    memset (&gev, 0, sizeof gev);
    gev.eid = VNG_EVENT_NEW_STORED_MSG;

    _vng_gev_find_waiter (vng, gev);

    return VNG_OK; 
}


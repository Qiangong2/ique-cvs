#define _SHR_DONT_DEFINE_BOOL
#include "shr_plat.h"
#include "vnp.h"
#include "vnp_rm_jt.h"



/* The VNG public APIs that are implemented in the VN process
 * are referred to as the VN process (i.e VNP) public APIs or
 * for brevity, the VNP APIs
 *
 * All VNP public APIs are accessed through the VNP Resource Manager
 * via the kernel call IOS_Ioctlv(IOSfd, cmd, in, insize, out, outsize).
 *
 * Since IOS_Ioctlv() requires an IOSfd, IOS_Open must be called before
 * any VNP APIs can be accessed.  Since the IOSfd is unique per process,
 * each process that calls VNP APIs must call IOS_Open() and pass the
 * returned IOSfd in subsequent IOS_Ioctlv() calls.  The IOSfd will be
 * obtained when VNG_Init() is called.
 *
 * Therefore, VNG_Init() must be called by each process that calls VNG APIs.
 *
 * A VNG_Init()---VNG_Fini() is a VNG session.  IOS_Open is done
 * in VNG_Init() and IOS_Close() is done in VNG_Fini().
 *
 * Therefore, an RM open connection descriptor identifies a VNG session.
 *
 * The state unique to a VNG session is kept in a data structure
 * identified by the descriptor returned by IOS_Open.
 *
 * The state common to all VNG sessions is kept in a global
 * data structure in the VN process address space.
 *
 * The VNP API wrapper for VNG_Init(VNG *vng) will obtain an IOSfd via 
 * a call to IOS_Open(). The IOSfd will be used in an IOS_Ioctlv() call
 * requesting VNG_Init() and will be returned in vng->id for use in
 * subsequent calls to VNG APIs.
 *
 * Since wrappers are provided in the SC version of libvnp.a for accessing
 * the VNP public APIs via IOS_Ioctlv(), only the wrapper functions need be
 * aware of the IOSfd.
 *
 * Only the RM and the VNG_Init() wrapper needs to be aware of the VNP RM pathname
 * used in calls to IOS_Open() for opening the VNP RM.
 *
 * There is only one VNP instance in the VN process that is used to
 * maintain all VNs used by all NC processes.   There is nothing in
 * the VNG data structures passed in VNG APIs other than the IOSfd.
 *
 * Since the IOSfds are only needed by the VNP wrappers, we could
 * remove the VNG pointer argument from VNG APIs and use a static
 * variable in the libvnp wrappers.
 *
 * For compatibility with existing VNG APIs and client code; and for
 * potential future use, the VNG pointer argument passed to VNG APIs
 * will be retained.  It can be thought of as representing an instance
 * of VNG_Init(). There is currently no significance to multiple instances
 * of VNG_Init() except that a different IOSfd is obtained.  
 *
 * The only reason to use multiple VNG data structures within a process
 * would be if there were something unique about the different VNG sessions.
 * A possible unique aspect would be if we allowed multiple
 * logins to the infrastructure server from the same device.  Each
 * VNG_Init could represent a session associated with a login.
 *
 * We don't allow multiple logins and the trend seems to be to make
 * the single login per device an automatic feature taken care of
 * by the viewer or the VNP and not done by a game.
 *
 * Another possible use for multiple VNGs would be to associate a set
 * of VNs with a VNG so they could be operated on as a set.  For example
 * they could be removed as a set by doing a VNG_Fini(vng).
 *
 * The VN pointer passed in some VNG APIs is required to identify
 * the Virtual Net and member to which the operation applies. The VN data
 * structure contains only the IOSFd for use by VNP API wrapper functions and
 * info needed by the VNP to find an internal data structure representing
 * the Virtual Net node and verify that it is still valid.
 *
 * The VN process VNG_Init() will not re-initialize the VNP if it is
 * already initialized.  It will return VNG_OK.
 *
 * To re-initialize VNP, VNG_Fini() must first be called by the
 * same process that originally called VNG_Init(). On the NetC, that
 * will normally be the viewer.  VNG_Init() can then be called again
 * to re-initialize VNP.
 *
 * Since VNG_Fini will result in loss of all knowledge of previously
 * opened VNs and VNG login, it should not be called unless terminating
 * the interface is actually desired.  That is not desirable in actual
 * NC operation except possibly during a controlled shutdown.
 *
 * When switching between games, the game specific VNs should be deleted.
 * Perhaps we need a VNG_DeleteProcessVNs(bool other).  It could delete all VNs
 * created by the current process or delete all VNs created by other Processes.
 * We could also have a VNG_DeleteVNs(vng) which would delete all VNs created
 * using vng.  This is not required if the mechanism for switching between
 * games is to reset the NC.
 *
 * If VNG_Fini is called by other than the process that first called VNG_Init(),
 * an error will be returned. NOTE: Perhaps we should keep track of what
 * process created each VN and if the process that calls VNG_Fini is
 * not the process that first called VNG_Init, delete all VNs created
 * by the calling process.
 *
 */



#define VNP_DEV "/dev/vn"

#define CHECK_VNG(vng) \
    if (!vng) { \
        rv = VNGERR_INVALID_VNG; \
        goto end; \
    }

#define CHECK_VNG_RV(vng, err_val) \
    if (!vng) { \
        rv = err_val; \
        goto end; \
    }

#define CHECK_VN(vn) \
    if (!vn) { \
        rv = VNGERR_INVALID_VN; \
        goto end; \
    }

#define CHECK_VN_RV(vn, err_val) \
    if (!vn) { \
        rv = err_val; \
        goto end; \
    }

typedef union  {
    u32                                 dummy;
    VNPIBuf_VNG_Init                    b_VNG_Init;
    VNPIBuf_VNG_Fini                    b_VNG_Fini;
    VNPIBuf_VNG_Login                   b_VNG_Login;
    VNPIBuf_VNG_Logout                  b_VNG_Logout;
    VNPIBuf_VNG_GetLoginType            b_VNG_GetLoginType;
    VNPIBuf_VNG_GetUserInfo             b_VNG_GetUserInfo;
    VNPIBuf_VNG_NewVN                   b_VNG_NewVN;
    VNPIBuf_VNG_DeleteVN                b_VNG_DeleteVN;
    VNPIBuf_VNG_JoinVN                  b_VNG_JoinVN;
    VNPIBuf_VNG_GetJoinRequest          b_VNG_GetJoinRequest;
    VNPIBuf_VNG_AcceptJoinRequest       b_VNG_AcceptJoinRequest;
    VNPIBuf_VNG_RejectJoinRequest       b_VNG_RejectJoinRequest;
    VNPIBuf_VNG_LeaveVN                 b_VNG_LeaveVN;
    VNPIBuf_VN_NumMembers               b_VN_NumMembers;
    VNPIBuf_VN_GetMembers               b_VN_GetMembers;
    VNPIBuf_VN_MemberState              b_VN_MemberState;
    VNPIBuf_VN_MemberUserId             b_VN_MemberUserId;
    VNPIBuf_VN_MemberLatency            b_VN_MemberLatency;
    VNPIBuf_VN_MemberDeviceType         b_VN_MemberDeviceType;
    VNPIBuf_VN_GetVNId                  b_VN_GetVNId;
    VNPIBuf_VN_Select                   b_VN_Select;
    VNPIBuf_VN_SendMsg                  b_VN_SendMsg;
    VNPIBuf_VN_RecvMsg                  b_VN_RecvMsg;
    VNPIBuf_VN_SendReq                  b_VN_SendReq;
    VNPIBuf_VN_RecvReq                  b_VN_RecvReq;
    VNPIBuf_VN_SendResp                 b_VN_SendResp;
    VNPIBuf_VN_RecvResp                 b_VN_RecvResp;
    VNPIBuf_VN_RegisterRPCService       b_VN_RegisterRPCService;
    VNPIBuf_VN_UnregisterRPCService     b_VN_UnregisterRPCService;
    VNPIBuf_VN_SendRPC                  b_VN_SendRPC;
    VNPIBuf_VNG_RecvRPC                 b_VNG_RecvRPC;
    VNPIBuf_VNG_GetEvent                b_VNG_GetEvent;
    VNPIBuf_VNG_SendNotification        b_VNG_SendNotification;
    VNPIBuf_VNG_RecvNotification           b_VNG_RecvNotification;
    VNPIBuf_VNG_RegisterGame            b_VNG_RegisterGame;
    VNPIBuf_VNG_UnregisterGame          b_VNG_UnregisterGame;
    VNPIBuf_VNG_UpdateGameStatus        b_VNG_UpdateGameStatus;
    VNPIBuf_VNG_GetGameStatus           b_VNG_GetGameStatus;
    VNPIBuf_VNG_GetGameComments         b_VNG_GetGameComments;
    VNPIBuf_VNG_SearchGames             b_VNG_SearchGames;
    VNPIBuf_VNG_AddServerToAdhocDomain  b_VNG_AddServerToAdhocDomain;
    VNPIBuf_VNG_ResetAdhocDomain        b_VNG_ResetAdhocDomain;
    VNPIBuf_VNG_ConnectStatus           b_VNG_ConnectStatus;
    VNPIBuf_VNG_MyUserId                b_VNG_MyUserId;
    VNPIBuf_VN_Self                     b_VN_Self;
    VNPIBuf_VN_Owner                    b_VN_Owner;
    VNPIBuf_VN_State                    b_VN_State;
    VNPIBuf_VNG_GetVNs                  b_VNG_GetVNs;
    VNPIBuf_VNG_SetTraceOpt             b_VNG_SetTraceOpt;
    VNPIBuf_VNG_Register                b_VNG_Register;
    VNPIBuf_VNG_RemoveVNMember          b_VNG_RemoveVNMember;

} VNPIoctlvBuf;


#ifndef _GBA
    #ifdef _WIN32
        #define VNP_IOCTLV_BUF _alloca(sizeof(VNPIoctlvBuf))
    #else
        #include <alloca.h>
        #define VNP_IOCTLV_BUF alloca(sizeof(VNPIoctlvBuf))
    #endif
#else
    static VNPIoctlvBuf __ioctlvBuf = {1};
    #define VNP_IOCTLV_BUF (void*)(&__ioctlvBuf)
#endif







static VNGErrCode rm2vngErr (IOSError ios_err)
{
    VNGErrCode ec;

    printf ("rm2vngErr: %d\n", ios_err);

    switch (ios_err) {
        case IOS_ERROR_ACCESS:
            ec = VNGERR_ACCESS;
            break;
        case IOS_ERROR_MAX:
            ec = VNGERR_MAX_REACHED;
            break;
        default:
            ec = VNGERR_RM_API;
            break;
    }

    return ec;
}


/* In the VNP RM APIs, vng is always required to be readable for vnp.
 * The VN process does not write to it.
 *
 * Only the VNG_Init and VNG_Fini wrappers executed in the
 * calling process execution environment write to vng.
 *
 * For VNP RM APIs, only VNG_NewVN(), VNG_JoinVN(), and VNG_CloneVN()
 * write to VN data structures allocated by NC App or GBA processes.
 * All other VNP RM APIs with VN pointer arguments will read
 * from the VN but not write.
 *
 */


LIBVNG_API VNGErrCode VNG_CloneVN (VNG *vng,
                                   VN  *vn,           /* w */
                             const VN  *vnToClone)    /* r */
{
    VNGErrCode  rv;

    CHECK_VNG(vng);

    vn->id = vnToClone->id;
    vn->addr = vnToClone->addr;
    vn->vng_id = vng->id;
  
    rv = VNG_OK;

end:
    return rv;
}


LIBVNG_API VNGErrCode VNG_Init (VNG          *vng,      /* p:r  l:rw */
                          const VNGInitParm  *parm)     /* r */
{
    IOSError    iose;
    VNGErrCode   rv;
    VNPIBuf_VNG_Init *b = VNP_IOCTLV_BUF;
    VNPA_r__VNG_Init *a = &b->a_r;
    VNPV_r__VNG_Init *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VNG_Init)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VNG_Init)/sizeof(IOSIoVector));
    
    CHECK_VNG(vng);

    if (0 > (vng->id = IOS_Open(VNP_DEV, 0))) {
        rv = rm2vngErr(vng->id);
        goto end;
    }

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vng.base    = (u8*) vng;
    v->read.vng.length  = sizeof *vng;

    a->vng = vng;
    if (parm) {
        a->parm = &a->realParm;
        memcpy (&a->realParm, parm, sizeof a->realParm);
    } else {
        a->parm = NULL;
    }
    
    iose = IOS_Ioctlv (vng->id, VNP_FI_VNG_INIT, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else {
        rv = b->a__w.rv;
    }

end:
    return rv;
}


LIBVNG_API VNGErrCode VNG_Fini (VNG *vng)       /* p:r  l:rw */
{
    IOSError     iose;
    VNGErrCode   rv;
    VNPIBuf_VNG_Fini *b = VNP_IOCTLV_BUF;
    VNPA_r__VNG_Fini *a = &b->a_r;
    VNPV_r__VNG_Fini *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VNG_Fini)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VNG_Fini)/sizeof(IOSIoVector));
    IOSFd fd;
    
    CHECK_VNG(vng);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vng.base    = (u8*) vng;
    v->read.vng.length  = sizeof *vng;

    a->vng = vng;
    
    fd = vng->id;
    iose = IOS_Ioctlv (vng->id, VNP_FI_VNG_FINI, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else if (b->a__w.rv == VNG_OK) {
        vng->id = IOS_ERROR_INVALID;
        if (0 > (iose = IOS_Close(fd))) {
            rv = rm2vngErr(iose);
        } else {
            rv = VNG_OK;
        }
    } else {
        rv = b->a__w.rv;
    }

end:
    return rv;
}


LIBVNG_API  VNGErrCode  VNG_Register(VNG        *vng,
                                     const char *serverName,    /* r */
                                     VNGPort     serverPort,
                                     char       *loginName,     /* w */
                                     char       *passwd,        /* w */
                                     VNGTimeout  timeout)
{
    IOSError     iose;
    VNGErrCode   rv;
    VNPIBuf_VNG_Register *b = VNP_IOCTLV_BUF;
    VNPA_r__VNG_Register *a = &b->a_r;
    VNPV_r__VNG_Register *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VNG_Register)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VNG_Register)/sizeof(IOSIoVector));

    u32 serverNameBufSize;
    u32 loginNameSize;
    u32 passwdSize;
    char nullChar = '\0';
    
    CHECK_VNG(vng);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vng.base    = (u8*) vng;
    v->read.vng.length  = sizeof *vng;

    if (!serverName || !loginName || !passwd) {
        rv = VNGERR_INVALID_ARG;
        goto end;
    }
        
    serverNameBufSize = 1 + strnlen(serverName, VNG_SERVER_NAME_BUF_SIZE);

    if (serverNameBufSize > VNG_SERVER_NAME_BUF_SIZE) {
        rv = VNGERR_INVALID_LEN;
        goto end;
    }

    a->vng = vng;
    memcpy (a->serverName, serverName, serverNameBufSize);
    a->serverPort = serverPort;
    a->timeout = timeout;
    // values returned in b->a__w.loginName and b->a__w.passwd

    iose = IOS_Ioctlv (vng->id, VNP_FI_VNG_REGISTER, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else if (!(rv = b->a__w.rv)) {
        loginNameSize  = 1 + strnlen(b->a__w.loginName,  VNG_LOGIN_BUF_SIZE);
        passwdSize     = 1 + strnlen(b->a__w.passwd,     VNG_PASSWD_BUF_SIZE);
        if (loginNameSize > VNG_LOGIN_BUF_SIZE) {
            memcpy(&b->a__w.loginName [VNG_LOGIN_BUF_SIZE - 1], &nullChar, sizeof nullChar);
            loginNameSize  = VNG_LOGIN_BUF_SIZE;
            printf ("ERROR: VNG_Register returned loginName strnlen %d %s\n",
                                                  loginNameSize,
                                                  b->a__w.loginName);
            rv = VNGERR_OVERFLOW;
        }
        if (passwdSize > VNG_PASSWD_BUF_SIZE) {
            memcpy(&b->a__w.passwd [VNG_PASSWD_BUF_SIZE - 1], &nullChar, sizeof nullChar);
            passwdSize = VNG_PASSWD_BUF_SIZE;
            printf ("ERROR: VNG_Register returned passwd strnlen %d %s\n",
                                                  passwdSize,
                                                  b->a__w.passwd);
            rv = VNGERR_OVERFLOW;
        }
        memcpy (loginName, b->a__w.loginName, loginNameSize);
        memcpy (passwd, b->a__w.passwd, passwdSize);
    }

end:
    return rv;
}


LIBVNG_API VNGErrCode VNG_Login (VNG        *vng,
                                 const char *serverName,    /* r */
                                 VNGPort     serverPort,
                                 const char *loginName,     /* r */
                                 const char *passwd,        /* r */
                                 VNGTimeout  timeout)
{
    IOSError     iose;
    VNGErrCode   rv;
    VNPIBuf_VNG_Login *b = VNP_IOCTLV_BUF;
    VNPA_r__VNG_Login *a = &b->a_r;
    VNPV_r__VNG_Login *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VNG_Login)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VNG_Login)/sizeof(IOSIoVector));

    u32 serverNameBufSize;
    u32 loginNameBufSize;
    u32 passwdBufSize;
    
    CHECK_VNG(vng);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vng.base    = (u8*) vng;
    v->read.vng.length  = sizeof *vng;

    if (!serverName || !loginName || !passwd) {
        rv = VNGERR_INVALID_ARG;
        goto end;
    }
        
    serverNameBufSize = 1 + strnlen(serverName, VNG_SERVER_NAME_BUF_SIZE);
    loginNameBufSize  = 1 + strnlen(loginName,  VNG_LOGIN_BUF_SIZE);
    passwdBufSize     = 1 + strnlen(passwd,     VNG_PASSWD_BUF_SIZE);

    if (    serverNameBufSize > VNG_SERVER_NAME_BUF_SIZE
        ||  loginNameBufSize  > VNG_LOGIN_BUF_SIZE
        ||  passwdBufSize     > VNG_PASSWD_BUF_SIZE) {

        rv = VNGERR_INVALID_LEN;
        goto end;
    }


    a->vng = vng;
    memcpy (a->serverName, serverName, serverNameBufSize);
    a->serverPort = serverPort;
    memcpy (a->loginName, loginName, loginNameBufSize);
    memcpy (a->passwd, passwd, passwdBufSize);
    a->timeout = timeout;

    iose = IOS_Ioctlv (vng->id, VNP_FI_VNG_LOGIN, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else {
        rv = b->a__w.rv;
    }

end:
    return rv;
}


LIBVNG_API VNGErrCode VNG_Logout (VNG        *vng, 
                                  VNGTimeout  timeout)
{
    IOSError     iose;
    VNGErrCode   rv;
    VNPIBuf_VNG_Logout *b = VNP_IOCTLV_BUF;
    VNPA_r__VNG_Logout *a = &b->a_r;
    VNPV_r__VNG_Logout *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VNG_Logout)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VNG_Logout)/sizeof(IOSIoVector));

    CHECK_VNG(vng);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vng.base    = (u8*) vng;
    v->read.vng.length  = sizeof *vng;

    a->vng = vng;
    a->timeout = timeout;

    iose = IOS_Ioctlv (vng->id, VNP_FI_VNG_LOGOUT, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else {
        rv = b->a__w.rv;
    }

end:
    return rv;
}


LIBVNG_API int32_t VNG_GetLoginType (VNG *vng)
{
    IOSError     iose;
    int32_t      rv;
    VNPIBuf_VNG_GetLoginType *b = VNP_IOCTLV_BUF;
    VNPA_r__VNG_GetLoginType *a = &b->a_r;
    VNPV_r__VNG_GetLoginType *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VNG_GetLoginType)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VNG_GetLoginType)/sizeof(IOSIoVector));

    CHECK_VNG_RV(vng, VNG_NOT_LOGIN);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vng.base    = (u8*) vng;
    v->read.vng.length  = sizeof *vng;

    a->vng = vng;
    
    iose = IOS_Ioctlv (vng->id, VNP_FI_VNG_GETLOGINTYPE, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = VNG_NOT_LOGIN;
    } else {
        rv = b->a__w.rv;
    }

end:
    return rv;
}


LIBVNG_API VNGErrCode VNG_GetUserInfo (VNG         *vng,
                                       VNGUserId    uid,
                                       VNGUserInfo *uinfo,      /* w */ 
                                       VNGTimeout   timeout)
{
    IOSError     iose;
    VNGErrCode   rv;
    VNPIBuf_VNG_GetUserInfo *b = VNP_IOCTLV_BUF;
    VNPA_r__VNG_GetUserInfo *a = &b->a_r;
    VNPV_r__VNG_GetUserInfo *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VNG_GetUserInfo)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VNG_GetUserInfo)/sizeof(IOSIoVector));

    CHECK_VNG(vng);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vng.base    = (u8*) vng;
    v->read.vng.length  = sizeof *vng;

    if (!uinfo) {
        rv = VNGERR_INVALID_ARG;
        goto end;
    }

    a->vng = vng;
    a->uid = uid;
    /* pass &b->a__w.uinfo */
    a->timeout = timeout;

    iose = IOS_Ioctlv (vng->id, VNP_FI_VNG_GETUSERINFO, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else {
        rv = b->a__w.rv;
        *uinfo = b->a__w.uinfo;
    }

end:
    return rv;
}

LIBVNG_API VNGErrCode VNG_NewVN (VNG       *vng,
                                 VN        *vn,         /* w */
                                 VNClass    vnClass,
                                 VNDomain   domain,
                                 VNPolicies policies)
{
    IOSError     iose;
    VNGErrCode   rv;
    VNPIBuf_VNG_NewVN *b = VNP_IOCTLV_BUF;
    VNPA_r__VNG_NewVN *a = &b->a_r;
    VNPV_r__VNG_NewVN *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VNG_NewVN)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VNG_NewVN)/sizeof(IOSIoVector));

    CHECK_VNG(vng);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vng.base    = (u8*) vng;
    v->read.vng.length  = sizeof *vng;

    if (!vn) {
        rv = VNGERR_INVALID_ARG;
        goto end;
    }

    v->write.vn.base   = (u8*) vn;
    v->write.vn.length = sizeof *vn;

    a->vng = vng;
    a->vn = vn;
    a->vnClass = vnClass;
    a->domain = domain;
    a->policies = policies;

    iose = IOS_Ioctlv (vng->id, VNP_FI_VNG_NEWVN, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else {
        rv = b->a__w.rv;
    }

end:
    return rv;
}


LIBVNG_API VNGErrCode VNG_DeleteVN (VNG *vng,
                                    VN  *vn)    /* r */
{
    IOSError     iose;
    VNGErrCode   rv;
    VNPIBuf_VNG_DeleteVN *b = VNP_IOCTLV_BUF;
    VNPA_r__VNG_DeleteVN *a = &b->a_r;
    VNPV_r__VNG_DeleteVN *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VNG_DeleteVN)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VNG_DeleteVN)/sizeof(IOSIoVector));

    CHECK_VNG(vng);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vng.base    = (u8*) vng;
    v->read.vng.length  = sizeof *vng;

    if (!vn) {
        rv = VNGERR_INVALID_ARG;
        goto end;
    }

    v->read.vn.base   = (u8*) vn;
    v->read.vn.length = sizeof *vn;

    a->vng = vng;
    a->vn  = vn;

    iose = IOS_Ioctlv (vng->id, VNP_FI_VNG_DELETEVN, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else {
        rv = b->a__w.rv;
    }

end:
    return rv;
}



LIBVNG_API VNGErrCode VNG_JoinVN (VNG         *vng,
                                  VNId         vnId,
                                  const char  *msg,             /* r or NULL */
                                  VN          *vn,              /* w */
                                  char        *denyReason,      /* w or NULL */
                                  size_t       denyReasonLen,
                                  VNGTimeout   timeout)
{
    IOSError     iose;
    VNGErrCode   rv;
    VNPIBuf_VNG_JoinVN *b = VNP_IOCTLV_BUF;
    VNPA_r__VNG_JoinVN *a = &b->a_r;
    VNPV_r__VNG_JoinVN *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VNG_JoinVN)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VNG_JoinVN)/sizeof(IOSIoVector));

    u32 msgBufSize;
    char nullChar = '\0';
    
    CHECK_VNG(vng);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vng.base    = (u8*) vng;
    v->read.vng.length  = sizeof *vng;

    if (denyReasonLen && denyReason) {
        memcpy (denyReason, &nullChar, sizeof nullChar);
        v->write.denyReason.base   = (u8*) denyReason;
        v->write.denyReason.length = denyReasonLen;
    }
    else {
        /* ok if caller doesn't care about deny reason */
        --writeCount;
    }

    if (!vn) {
        rv = VNGERR_INVALID_ARG;
        goto end;
    }

    v->write.vn.base   = (u8*) vn;
    v->write.vn.length = sizeof *vn;

    if (msg) {
        msgBufSize = 1 + strnlen(msg, VNG_MAX_JOIN_REQ_MSG_LEN);
        if (msgBufSize > VNG_MAX_JOIN_REQ_MSG_LEN) {
            rv = VNGERR_INVALID_LEN;
            goto end;
        }
        v->read.msg.base   = (u8*) msg;
        v->read.msg.length = msgBufSize;
    }
    else {
        /* ok if caller doesn't want to provide a msg */
        ++vector;
        --readCount;
    }

    a->vng = vng;
    a->vnId = vnId;
    a->msg = msg;
    a->vn = vn;
    a->denyReason = denyReason;
    a->denyReasonLen = denyReasonLen;
    a->timeout = timeout;

    iose = IOS_Ioctlv (vng->id, VNP_FI_VNG_JOINVN, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else {
        rv = b->a__w.rv;
    }

end:
    return rv;
}


LIBVNG_API VNGErrCode VNG_GetJoinRequest (VNG         *vng,
                                          VNId         vnId,
                                          VN         **vn,          /* w or null */ 
                                          uint64_t    *requestId,   /* w */
                                          VNGUserInfo *userInfo,    /* w or null*/
                                          char        *joinReason,  /* w or null*/
                                          size_t       joinReasonLen,
                                          VNGTimeout   timeout)
{
    IOSError     iose;
    VNGErrCode   rv;
    VNPIBuf_VNG_GetJoinRequest *b = VNP_IOCTLV_BUF;
    VNPA_r__VNG_GetJoinRequest *a = &b->a_r;
    VNPV_r__VNG_GetJoinRequest *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VNG_GetJoinRequest)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VNG_GetJoinRequest)/sizeof(IOSIoVector));

    CHECK_VNG(vng);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vng.base    = (u8*) vng;
    v->read.vng.length = sizeof *vng;

    if (joinReasonLen && joinReason) {
        v->write.joinReason.base   = (u8*) joinReason;
        v->write.joinReason.length = joinReasonLen;
    } else {
        --writeCount;
    }

    /* For demonstration, let the func return an error if requestId is null.
     * This demonstrates how to centralize error checking in vnp_api.c rather
     * than repeat it here.  Could remove a->requestId and pass
     * &b->a__w.requestId if check that requestId is non-null and return
     * an error if it is.  But that means the error checking and decision
     * about what err code to return are done in both places.  Some other
     * rm api's duplicate the error checking.  For example see VNG_RecvRPC.
     */
    a->vng = vng;
    a->vnId = vnId;
    a->vn        = (vn)        ? &b->a__w.vn        : 0;
    a->requestId = (requestId) ? &b->a__w.requestId : 0;
    a->userInfo  =  (userInfo) ? &b->a__w.userInfo  : 0;
    a->joinReason = joinReason;
    a->joinReasonLen = joinReasonLen;
    a->timeout = timeout;

    iose = IOS_Ioctlv (vng->id, VNP_FI_VNG_GETJOINREQUEST, readCount, writeCount, vector);
    
    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else {
        rv = b->a__w.rv;
        if (vn)        *vn = *a->vn;
        if (requestId) *requestId = *a->requestId;
        if (userInfo)  *userInfo = *a->userInfo;
    }

end:
    return rv;
}


LIBVNG_API VNGErrCode VNG_AcceptJoinRequest (VNG        *vng, 
                                             uint64_t    requestId)
{
    IOSError     iose;
    VNGErrCode   rv;
    VNPIBuf_VNG_AcceptJoinRequest *b = VNP_IOCTLV_BUF;
    VNPA_r__VNG_AcceptJoinRequest *a = &b->a_r;
    VNPV_r__VNG_AcceptJoinRequest *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VNG_AcceptJoinRequest)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VNG_AcceptJoinRequest)/sizeof(IOSIoVector));

    CHECK_VNG(vng);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vng.base    = (u8*) vng;
    v->read.vng.length  = sizeof *vng;

    a->vng = vng;
    a->requestId = requestId;

    iose = IOS_Ioctlv (vng->id, VNP_FI_VNG_ACCEPTJOINREQUEST, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else {
        rv = b->a__w.rv;
    }

end:
    return rv;
}


LIBVNG_API VNGErrCode VNG_RejectJoinRequest (VNG        *vng, 
                                             uint64_t    requestId,      
                                             const char *reason)        /* r or null */
{
    IOSError     iose;
    VNGErrCode   rv;
    VNPIBuf_VNG_RejectJoinRequest *b = VNP_IOCTLV_BUF;
    VNPA_r__VNG_RejectJoinRequest *a = &b->a_r;
    VNPV_r__VNG_RejectJoinRequest *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VNG_RejectJoinRequest)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VNG_RejectJoinRequest)/sizeof(IOSIoVector));

    u32 reasonBufSize;
    
    CHECK_VNG(vng);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vng.base    = (u8*) vng;
    v->read.vng.length  = sizeof *vng;

    if (reason) {
        reasonBufSize = strnlen(reason, VN_MAX_MSG_LEN);
        v->read.reason.base   = (u8*) reason;
        v->read.reason.length = reasonBufSize;
    }
    else {
        /* ok if caller doesn't want to provide a reason */
        ++vector;
        --readCount;
    }

    a->vng = vng;
    a->requestId = requestId;
    a->reason = reason;

    iose = IOS_Ioctlv (vng->id, VNP_FI_VNG_REJECTJOINREQUEST, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else {
        rv = b->a__w.rv;
    }

end:
    return rv;
}


LIBVNG_API VNGErrCode VNG_LeaveVN (VNG *vng,
                                   VN  *vn)     /* r */
{
    IOSError     iose;
    VNGErrCode   rv;
    VNPIBuf_VNG_LeaveVN *b = VNP_IOCTLV_BUF;
    VNPA_r__VNG_LeaveVN *a = &b->a_r;
    VNPV_r__VNG_LeaveVN *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VNG_LeaveVN)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VNG_LeaveVN)/sizeof(IOSIoVector));

    CHECK_VNG(vng);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vng.base    = (u8*) vng;
    v->read.vng.length  = sizeof *vng;

    if (!vn) {
        rv = VNGERR_INVALID_ARG;
        goto end;
    }

    v->read.vn.base   = (u8*) vn;
    v->read.vn.length = sizeof *vn;

    a->vng = vng;
    a->vn  = vn;

    iose = IOS_Ioctlv (vng->id, VNP_FI_VNG_LEAVEVN, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else {
        rv = b->a__w.rv;
    }

end:
    return rv;
}


LIBVNG_API VNGErrCode VNG_RemoveVNMember (VNG      *vng,
                                          VN       *vn,     /* r */
                                          VNMember  memb)
{
    IOSError     iose;
    VNGErrCode   rv;
    VNPIBuf_VNG_RemoveVNMember *b = VNP_IOCTLV_BUF;
    VNPA_r__VNG_RemoveVNMember *a = &b->a_r;
    VNPV_r__VNG_RemoveVNMember *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VNG_RemoveVNMember)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VNG_RemoveVNMember)/sizeof(IOSIoVector));

    CHECK_VNG(vng);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vng.base    = (u8*) vng;
    v->read.vng.length  = sizeof *vng;

    if (!vn) {
        rv = VNGERR_INVALID_ARG;
        goto end;
    }

    v->read.vn.base   = (u8*) vn;
    v->read.vn.length = sizeof *vn;

    a->vng = vng;
    a->vn  = vn;
    a->memb = memb;

    iose = IOS_Ioctlv (vng->id, VNP_FI_VNG_REMOVEVNMEMBER, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else {
        rv = b->a__w.rv;
    }

end:
    return rv;
}


LIBVNG_API uint32_t VN_NumMembers (VN *vn)
{
    IOSError     iose;
    uint32_t     rv;
    VNPIBuf_VN_NumMembers *b = VNP_IOCTLV_BUF;
    VNPA_r__VN_NumMembers *a = &b->a_r;
    VNPV_r__VN_NumMembers *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VN_NumMembers)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VN_NumMembers)/sizeof(IOSIoVector));

    CHECK_VN_RV(vn, 0);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vn.base    = (u8*) vn;
    v->read.vn.length  = sizeof *vn;

    a->vn = vn;

    iose = IOS_Ioctlv (vn->vng_id, VNP_FI_VN_NUMMEMBERS, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = 0;
    } else {
        rv = b->a__w.rv;
    }

end:
    return rv;
}


LIBVNG_API uint32_t VN_GetMembers (VN       *vn,
                                   VNMember *members,   /* w */
                                   uint32_t  nMembers)
{
    IOSError     iose;
    uint32_t     rv;
    VNPIBuf_VN_GetMembers *b = VNP_IOCTLV_BUF;
    VNPA_r__VN_GetMembers *a = &b->a_r;
    VNPV_r__VN_GetMembers *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VN_GetMembers)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VN_GetMembers)/sizeof(IOSIoVector));

    CHECK_VN_RV(vn, 0);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vn.base    = (u8*) vn;
    v->read.vn.length  = sizeof *vn;

    if (members && nMembers) {
        v->write.members.base   = (u8*) members;
        v->write.members.length = nMembers * sizeof *members;
    } else {
        --writeCount;
    }

    a->vn = vn;
    a->members = members;
    a->nMembers = nMembers;

    iose = IOS_Ioctlv (vn->vng_id, VNP_FI_VN_GETMEMBERS, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = 0;
    } else {
        rv = b->a__w.rv;
    }

end:
    return rv;
}


LIBVNG_API VNGErrCode VN_MemberState (VN *vn, VNMember memb)
{
    IOSError     iose;
    VNGErrCode   rv;
    VNPIBuf_VN_MemberState *b = VNP_IOCTLV_BUF;
    VNPA_r__VN_MemberState *a = &b->a_r;
    VNPV_r__VN_MemberState *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VN_MemberState)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VN_MemberState)/sizeof(IOSIoVector));

    CHECK_VN(vn);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vn.base    = (u8*) vn;
    v->read.vn.length  = sizeof *vn;

    a->vn = vn;
    a->memb = memb;

    iose = IOS_Ioctlv (vn->vng_id, VNP_FI_VN_MEMBERSTATE, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else {
        rv = b->a__w.rv;
    }

end:
    return rv;
}


LIBVNG_API VNGUserId VN_MemberUserId (VN        *vn,
                                      VNMember   memb,
                                      VNGTimeout timeout)
{
    IOSError     iose;
    VNGUserId    rv;
    VNPIBuf_VN_MemberUserId *b = VNP_IOCTLV_BUF;
    VNPA_r__VN_MemberUserId *a = &b->a_r;
    VNPV_r__VN_MemberUserId *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VN_MemberUserId)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VN_MemberUserId)/sizeof(IOSIoVector));

    CHECK_VN_RV(vn, VNG_INVALID_USER_ID);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vn.base    = (u8*) vn;
    v->read.vn.length  = sizeof *vn;

    a->vn = vn;
    a->memb = memb;
    a->timeout = timeout;

    iose = IOS_Ioctlv (vn->vng_id, VNP_FI_VN_MEMBERUSERID, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = VNG_INVALID_USER_ID;
    } else {
        rv = b->a__w.rv;
    }

end:
    return rv;
}


LIBVNG_API VNGMillisec  VN_MemberLatency (VN *vn, VNMember memb)  
{
    IOSError     iose;
    VNGMillisec      rv;
    VNPIBuf_VN_MemberLatency *b = VNP_IOCTLV_BUF;
    VNPA_r__VN_MemberLatency *a = &b->a_r;
    VNPV_r__VN_MemberLatency *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VN_MemberLatency)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VN_MemberLatency)/sizeof(IOSIoVector));

    CHECK_VN_RV(vn, VN_MEMB_LATENCY_NA);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vn.base    = (u8*) vn;
    v->read.vn.length  = sizeof *vn;

    a->vn = vn;
    a->memb = memb;

    iose = IOS_Ioctlv (vn->vng_id, VNP_FI_VN_MEMBERLATENCY, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = VN_MEMB_LATENCY_NA;
    } else {
        rv = b->a__w.rv;
    }

end:
    return rv;
}


LIBVNG_API VNGDeviceType VN_MemberDeviceType (VN *vn, VNMember memb)
{
    IOSError     iose;
    VNGDeviceType    rv;
    VNPIBuf_VN_MemberDeviceType *b = VNP_IOCTLV_BUF;
    VNPA_r__VN_MemberDeviceType *a = &b->a_r;
    VNPV_r__VN_MemberDeviceType *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VN_MemberDeviceType)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VN_MemberDeviceType)/sizeof(IOSIoVector));

    CHECK_VN_RV(vn, VNG_INVALID_DEV_TYPE);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vn.base    = (u8*) vn;
    v->read.vn.length  = sizeof *vn;

    a->vn = vn;
    a->memb = memb;

    iose = IOS_Ioctlv (vn->vng_id, VNP_FI_VN_MEMBERDEVICETYPE, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = VNG_INVALID_DEV_TYPE;
    } else {
        rv = b->a__w.rv;
    }

end:
    return rv;
}


LIBVNG_API VNGErrCode VN_GetVNId (VN   *vn,
                                  VNId *vnId)       /* w */
{
    IOSError     iose;
    VNGErrCode   rv;
    VNPIBuf_VN_GetVNId *b = VNP_IOCTLV_BUF;
    VNPA_r__VN_GetVNId *a = &b->a_r;
    VNPV_r__VN_GetVNId *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VN_GetVNId)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VN_GetVNId)/sizeof(IOSIoVector));

    CHECK_VN(vn);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vn.base    = (u8*) vn;
    v->read.vn.length  = sizeof *vn;

    a->vn = vn;
    a->vnId = (vnId) ? &b->a__w.vnId : 0;

    iose = IOS_Ioctlv (vn->vng_id, VNP_FI_VN_GETVNID, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else {
        rv = b->a__w.rv;
        if (vnId) *vnId = b->a__w.vnId;
    }

end:
    return rv;
}


LIBVNG_API VNGErrCode VN_Select (VN               *vn,
                                 VNServiceTypeSet  selectFrom,
                                 VNServiceTypeSet *ready,       /* w */
                                 VNMember          memb,
                                 VNGTimeout        timeout)
{
    IOSError     iose;
    VNGErrCode   rv;
    VNPIBuf_VN_Select *b = VNP_IOCTLV_BUF;
    VNPA_r__VN_Select *a = &b->a_r;
    VNPV_r__VN_Select *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VN_Select)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VN_Select)/sizeof(IOSIoVector));

    CHECK_VN(vn);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vn.base    = (u8*) vn;
    v->read.vn.length  = sizeof *vn;

    a->vn = vn;
    a->selectFrom = selectFrom;
    a->ready = (ready) ? &b->a__w.ready : 0;
    a->memb = memb;
    a->timeout = timeout;

    iose = IOS_Ioctlv (vn->vng_id, VNP_FI_VN_SELECT, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else {
        rv = b->a__w.rv;
        if (ready)  *ready = b->a__w.ready;
    }

end:
    return rv;
}

LIBVNG_API VNGErrCode VN_SendMsg (VN            *vn,
                                  VNMember       memb, 
                                  VNServiceTag   serviceTag,
                                  const void    *msg,           /* r or null */
                                  size_t         msglen,
                                  VNAttr         attr,
                                  VNGTimeout     timeout)
{
    IOSError     iose;
    VNGErrCode   rv;
    VNPIBuf_VN_SendMsg *b = VNP_IOCTLV_BUF;
    VNPA_r__VN_SendMsg *a = &b->a_r;
    VNPV_r__VN_SendMsg *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VN_SendMsg)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VN_SendMsg)/sizeof(IOSIoVector));

    CHECK_VN(vn);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vn.base    = (u8*) vn;
    v->read.vn.length  = sizeof *vn;

    if (msglen) {
        if (msglen > VN_MAX_MSG_LEN || (!msg && msglen)) {
            rv = VNGERR_INVALID_LEN;
            goto end;
        }
        v->read.msg.base   = (u8*) msg;
        v->read.msg.length = msglen;
    }
    else {
        ++vector;
        --readCount;
    }

    a->vn = vn;
    a->memb = memb;
    a->serviceTag = serviceTag;
    a->msg = msg;
    a->msglen = msglen;
    a->attr = attr;
    a->timeout = timeout;

    iose = IOS_Ioctlv (vn->vng_id, VNP_FI_VN_SENDMSG, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else {
        rv = b->a__w.rv;
    }

end:
    return rv;
}

LIBVNG_API VNGErrCode VN_RecvMsg (VN           *vn, 
                                  VNMember      memb,  
                                  VNServiceTag  serviceTag,
                                  void         *msg,        /*  w or null */
                                  size_t       *msglen,     /* rw or null */
                                  VNMsgHdr     *hdr,        /*  w or null */
                                  VNGTimeout    timeout)
{
    IOSError     iose;
    VNGErrCode   rv;
    VNPIBuf_VN_RecvMsg *b = VNP_IOCTLV_BUF;
    VNPA_r__VN_RecvMsg *a = &b->a_r;
    VNPV_r__VN_RecvMsg *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VN_RecvMsg)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VN_RecvMsg)/sizeof(IOSIoVector));

    CHECK_VN(vn);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vn.base    = (u8*) vn;
    v->read.vn.length  = sizeof *vn;

    if (msglen && *msglen && msg) {
        v->write.msg.base   = (u8*) msg;
        v->write.msg.length = *msglen;
    } else {
        --writeCount;
    }

    if (!hdr) {
        /* a->hdr pts to b->a__w.hdr or NULL
         * so we avoid copy if hdr is NULL.
         * We even avoid permission check and
         * cache flush by the following line.
         */
        v->write.buf.length -= sizeof *hdr;
    }

    a->vn = vn;
    a->memb = memb;
    a->serviceTag = serviceTag;
    a->msg = msg;
    b->a_rw.msglen = (msglen) ? *msglen : 0;
    a->hdr = (hdr) ? &b->a__w.hdr : 0;
    a->timeout = timeout;

    iose = IOS_Ioctlv (vn->vng_id, VNP_FI_VN_RECVMSG, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else {
        rv = b->a__w.rv;
        if (msglen) *msglen = b->a_rw.msglen;
        if (hdr) *hdr = b->a__w.hdr;
    }

end:
    return rv;
}


LIBVNG_API VNGErrCode VN_SendReq (VN           *vn, 
                                  VNMember      memb,
                                  VNServiceTag  serviceTag,
                                  VNCallerTag  *callerTag,   /* w */
                                  const void   *msg,         /* r or null */
                                  size_t        msglen,
                                  int32_t       optData,
                                  VNGTimeout    timeout)
{
    IOSError     iose;
    VNGErrCode   rv;
    VNPIBuf_VN_SendReq *b = VNP_IOCTLV_BUF;
    VNPA_r__VN_SendReq *a = &b->a_r;
    VNPV_r__VN_SendReq *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VN_SendReq)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VN_SendReq)/sizeof(IOSIoVector));

    CHECK_VN(vn);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vn.base    = (u8*) vn;
    v->read.vn.length  = sizeof *vn;

    if (msglen) {
        if (msglen > VN_MAX_MSG_LEN || !msg) {
            rv = VNGERR_INVALID_LEN;
            goto end;
        }
        v->read.msg.base   = (u8*) msg;
        v->read.msg.length = msglen;
    }
    else {
        ++vector;
        --readCount;
    }

    if (!callerTag) {
        rv = VNGERR_INVALID_ARG;
        goto end;
    }

    a->vn = vn;
    a->memb = memb;
    a->serviceTag = serviceTag;
    /* pass &b->a__w.callerTag; */
    a->msg = msg;
    a->msglen = msglen;
    a->optData = optData;
    a->timeout = timeout;

    iose = IOS_Ioctlv (vn->vng_id, VNP_FI_VN_SENDREQ, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else {
        rv = b->a__w.rv;
        *callerTag = b->a__w.callerTag;
    }

end:
    return rv;
}

LIBVNG_API VNGErrCode VN_RecvReq (VN           *vn,
                                  VNMember      memb,  
                                  VNServiceTag  serviceTag,
                                  void         *msg,          /* w  or null */
                                  size_t       *msglen,       /* rw or null */
                                  VNMsgHdr     *hdr,          /* w  or null */
                                  VNGTimeout    timeout)
{
    IOSError     iose;
    VNGErrCode   rv;
    VNPIBuf_VN_RecvReq *b = VNP_IOCTLV_BUF;
    VNPA_r__VN_RecvReq *a = &b->a_r;
    VNPV_r__VN_RecvReq *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VN_RecvReq)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VN_RecvReq)/sizeof(IOSIoVector));

    CHECK_VN(vn);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vn.base    = (u8*) vn;
    v->read.vn.length  = sizeof *vn;

    if (msglen && *msglen && msg) {
        v->write.msg.base   = (u8*) msg;
        v->write.msg.length = *msglen;
    } else {
        --writeCount;
    }

    if (!hdr) {
        /* a->hdr pts to b->a__w.hdr or NULL
         * so we avoid copy if hdr is NULL.
         * We even avoid permission check and
         * cache flush by the following line.
         */
        v->write.buf.length -= sizeof *hdr;
    }

    a->vn = vn;
    a->memb = memb;
    a->serviceTag = serviceTag;
    a->msg = msg;
    b->a_rw.msglen = (msglen) ? *msglen : 0;
    a->hdr = (hdr) ? &b->a__w.hdr : 0;
    a->timeout = timeout;

    iose = IOS_Ioctlv (vn->vng_id, VNP_FI_VN_RECVREQ, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else {
        rv = b->a__w.rv;
        if (msglen) *msglen = b->a_rw.msglen;
        if (hdr) *hdr = b->a__w.hdr;
    }

end:
    return rv;
}

LIBVNG_API VNGErrCode VN_SendResp (VN           *vn, 
                                   VNMember      memb,
                                   VNServiceTag  serviceTag,
                                   VNCallerTag   callerTag,
                                   const void   *msg,        /* r or null */
                                   size_t        msglen,
                                   int32_t       optData,
                                   VNGTimeout    timeout)
{
    IOSError     iose;
    VNGErrCode   rv;
    VNPIBuf_VN_SendResp *b = VNP_IOCTLV_BUF;
    VNPA_r__VN_SendResp *a = &b->a_r;
    VNPV_r__VN_SendResp *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VN_SendResp)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VN_SendResp)/sizeof(IOSIoVector));

    CHECK_VN(vn);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vn.base    = (u8*) vn;
    v->read.vn.length  = sizeof *vn;

    if (msglen) {
        if (msglen > VN_MAX_MSG_LEN || !msg) {
            rv = VNGERR_INVALID_LEN;
            goto end;
        }
        v->read.msg.base   = (u8*) msg;
        v->read.msg.length = msglen;
    }
    else {
        ++vector;
        --readCount;
    }


    if (!callerTag) {
        rv = VNGERR_INVALID_ARG;
        goto end;
    }

    a->vn = vn;
    a->memb = memb;
    a->serviceTag = serviceTag;
    a->callerTag = callerTag;
    a->msg = msg;
    a->msglen = msglen;
    a->optData = optData;
    a->timeout = timeout;

    iose = IOS_Ioctlv (vn->vng_id, VNP_FI_VN_SENDRESP, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else {
        rv = b->a__w.rv;
    }

end:
    return rv;
}

LIBVNG_API VNGErrCode VN_RecvResp (VN           *vn,
                                   VNMember      memb,  
                                   VNServiceTag  serviceTag,
                                   VNCallerTag   callerTag,  
                                   void         *msg,           /* w  or null */
                                   size_t       *msglen,        /* rw or null */
                                   VNMsgHdr     *hdr,           /* w  or null */
                                   VNGTimeout    timeout)
{
    IOSError     iose;
    VNGErrCode   rv;
    VNPIBuf_VN_RecvResp *b = VNP_IOCTLV_BUF;
    VNPA_r__VN_RecvResp *a = &b->a_r;
    VNPV_r__VN_RecvResp *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VN_RecvResp)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VN_RecvResp)/sizeof(IOSIoVector));

    CHECK_VN(vn);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vn.base    = (u8*) vn;
    v->read.vn.length  = sizeof *vn;

    if (msglen && *msglen && msg) {
            v->write.msg.base   = (u8*) msg;
            v->write.msg.length = *msglen;
    } else {
        --writeCount;
    }

    if (!hdr) {
        /* a->hdr pts to b->a__w.hdr or NULL
         * so we avoid copy if hdr is NULL.
         * We even avoid permission check and
         * cache flush by the following line.
         */
        v->write.buf.length -= sizeof *hdr;
    }

    a->vn = vn;
    a->memb = memb;
    a->serviceTag = serviceTag;
    a->callerTag = callerTag;
    a->msg = msg;
    b->a_rw.msglen = (msglen) ? *msglen : 0;
    a->hdr = (hdr) ? &b->a__w.hdr : 0;
    a->timeout = timeout;

    iose = IOS_Ioctlv (vn->vng_id, VNP_FI_VN_RECVRESP, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else {
        rv = b->a__w.rv;
        if (msglen) *msglen = b->a_rw.msglen;
        if (hdr) *hdr = b->a__w.hdr;
    }

end:
    return rv;
}


LIBVNG_API VNGErrCode VN_RegisterRPCService (VN               *vn,
                                             VNServiceTag      procId,
                                             VNRemoteProcedure proc)
{
    IOSError     iose;
    VNGErrCode   rv;
    VNPIBuf_VN_RegisterRPCService *b = VNP_IOCTLV_BUF;
    VNPA_r__VN_RegisterRPCService *a = &b->a_r;
    VNPV_r__VN_RegisterRPCService *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VN_RegisterRPCService)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VN_RegisterRPCService)/sizeof(IOSIoVector));

    CHECK_VN(vn);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vn.base    = (u8*) vn;
    v->read.vn.length  = sizeof *vn;

    a->vn = vn;
    a->procId = procId;
    a->proc = proc;

    iose = IOS_Ioctlv (vn->vng_id, VNP_FI_VN_REGISTERRPCSERVICE, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else {
        rv = b->a__w.rv;
    }

end:
    return rv;
}

LIBVNG_API VNGErrCode VN_UnregisterRPCService (VN               *vn,
                                               VNServiceTag      procId)
{
    IOSError     iose;
    VNGErrCode   rv;
    VNPIBuf_VN_UnregisterRPCService *b = VNP_IOCTLV_BUF;
    VNPA_r__VN_UnregisterRPCService *a = &b->a_r;
    VNPV_r__VN_UnregisterRPCService *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VN_UnregisterRPCService)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VN_UnregisterRPCService)/sizeof(IOSIoVector));

    CHECK_VN(vn);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vn.base    = (u8*) vn;
    v->read.vn.length  = sizeof *vn;

    a->vn = vn;
    a->procId = procId;

    iose = IOS_Ioctlv (vn->vng_id, VNP_FI_VN_UNREGISTERRPCSERVICE, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else {
        rv = b->a__w.rv;
    }

end:
    return rv;
}


LIBVNG_API VNGErrCode VN_SendRPC (VN           *vn,         /* r */
                                  VNMember      memb,
                                  VNServiceTag  serviceTag,
                                  const void   *args,       /* r or null */
                                  size_t        argsLen,
                                  void         *ret,        /*  w or null */
                                  size_t       *retLen,     /* rw or null */
                                  int32_t      *optData,    /* rw or null */
                                  VNGTimeout    timeout)
{
    IOSError     iose;
    VNGErrCode   rv;
    VNPIBuf_VN_SendRPC *b = VNP_IOCTLV_BUF;
    VNPA_r__VN_SendRPC *a = &b->a_r;
    VNPV_r__VN_SendRPC *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VN_SendRPC)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VN_SendRPC)/sizeof(IOSIoVector));

    CHECK_VN(vn);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vn.base    = (u8*) vn;
    v->read.vn.length  = sizeof *vn;

    if (argsLen) {
        if (argsLen > VN_MAX_MSG_LEN || !args) {
            rv = VNGERR_INVALID_LEN;
            goto end;
        }
        v->read.args.base   = (u8*) args;
        v->read.args.length = argsLen;
    }
    else {
        ++vector;
        --readCount;
    }

    if (retLen && *retLen && ret) {
        v->write.ret.base    = (u8*) ret;
        v->write.ret.length  = *retLen;
    }
    else {
        --writeCount;
    }

    a->vn = vn;
    a->memb = memb;
    a->serviceTag = serviceTag;
    a->args = args;
    a->argsLen = argsLen;
    a->ret = ret;
    b->a_rw.retLen = (retLen) ? *retLen : 0;
    b->a_rw.optData = (optData) ? *optData : 0;
    a->timeout = timeout;

    iose = IOS_Ioctlv(vn->vng_id, VNP_FI_VN_SENDRPC, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else {
        rv = b->a__w.rv;
        if (retLen) *retLen = b->a_rw.retLen;
        if (optData) *optData = b->a_rw.optData;
    }

end:
    return rv;
}


LIBVNG_API VNGErrCode VNG_RecvRPC (VNG               *vng, 
                                   VNRemoteProcedure *proc,     /*  w  */
                                   VN               **vn,       /*  w  */
                                   VNMsgHdr          *hdr,      /*  w  */
                                   void              *args,     /*  w or null */
                                   size_t            *argsLen,  /* rw */
                                   VNGTimeout         timeout)
{
    IOSError     iose;
    VNGErrCode   rv;
    VNPIBuf_VNG_RecvRPC *b = VNP_IOCTLV_BUF;
    VNPA_r__VNG_RecvRPC *a = &b->a_r;
    VNPV_r__VNG_RecvRPC *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VNG_RecvRPC)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VNG_RecvRPC)/sizeof(IOSIoVector));

    CHECK_VNG(vng);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vng.base    = (u8*) vng;
    v->read.vng.length  = sizeof *vng;


    if (!vn || !proc || !hdr) {
        rv = VNGERR_INVALID_ARG;
        goto end;
    }

    if (argsLen && *argsLen && args) {
        v->write.args.base   = (u8*) args;
        v->write.args.length  = *argsLen;
    }else {
        --writeCount;
    }

    a->vng = vng;
    /* pass &b->a__w.proc */
    /* pass &b->a__w.vn   */
    /* pass &b->a__w.hdr  */
    a->args = args;
    b->a_rw.argsLen = (argsLen) ? *argsLen : 0;
    a->timeout = timeout;

    iose = IOS_Ioctlv (vng->id, VNP_FI_VNG_RECVRPC, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else {
        rv = b->a__w.rv;
       *proc = b->a__w.proc;
       *vn   = b->a__w.vn;
       *hdr  = b->a__w.hdr;
        if (argsLen) *argsLen = b->a_rw.argsLen;
    }

end:
    return rv;
}

LIBVNG_API VNGErrCode VNG_GetEvent (VNG        *vng,
                                    VNGEvent   *event,      /* w */
                                    VNGTimeout  timeout)
{                      
    IOSError     iose; 
    VNGErrCode   rv;   
    VNPIBuf_VNG_GetEvent *b = VNP_IOCTLV_BUF;
    VNPA_r__VNG_GetEvent *a = &b->a_r;
    VNPV_r__VNG_GetEvent *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VNG_GetEvent)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VNG_GetEvent)/sizeof(IOSIoVector));

    CHECK_VNG(vng);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vng.base    = (u8*) vng;
    v->read.vng.length  = sizeof *vng;

    if (!event) {
        rv = VNGERR_INVALID_ARG;
        goto end;
    }

    a->vng = vng;
    /* pass &b->a__w.event */
    a->timeout = timeout;

    iose = IOS_Ioctlv (vng->id, VNP_FI_VNG_GETEVENT, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else {
        rv = b->a__w.rv;
        *event = b->a__w.event;
    }

end:
    return rv;
}


LIBVNG_API VNGErrCode VNG_SendNotification(VNG          *vng,
                                  	   VNGUserId    *uid,        /* r */
                                  	   uint32_t      nUsers,
					   const char   *message,    /* r */
					   size_t        messageLen)
{
    IOSError     iose;
    VNGErrCode   rv;
    VNPIBuf_VNG_SendNotification *b = VNP_IOCTLV_BUF;
    VNPA_r__VNG_SendNotification *a = &b->a_r;
    VNPV_r__VNG_SendNotification *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VNG_SendNotification)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VNG_SendNotification)/sizeof(IOSIoVector));

    CHECK_VNG(vng);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vng.base    = (u8*) vng;
    v->read.vng.length  = sizeof *vng;

    if (!nUsers) {
        rv = VNG_OK;
        goto end;
    }

    if (!uid) {
        rv = VNGERR_INVALID_LEN;  /* becaise nUsers != 0 */
        goto end;
    }

    v->read.uid.base   = (u8*) uid;
    v->read.uid.length = nUsers * sizeof *uid;

    if (message) {
        if ( messageLen > VNG_MAX_NOTIFY_MSG_BUF_SIZE) {
            rv = VNGERR_INVALID_LEN;
            goto end;
        }
        v->read.message.base   = (u8*) message;
        v->read.message.length = messageLen;
    }
    else {
        /* ok if caller doesn't want to provide a inviteMsg */
        ++vector;
        --readCount;
    }

    a->vng = vng;
    a->uid = uid;
    a->nUsers = nUsers;
    a->message = message;
    a->messageLen = messageLen;

    iose = IOS_Ioctlv (vng->id, VNP_FI_VNG_SENDNOTIFICATION, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else {
        rv = b->a__w.rv;
    }

end:
    return rv;
}

LIBVNG_API VNGErrCode VNG_RecvNotification (VNG          *vng,
                                            VNGUserInfo  *userInfo,        /* w or null */
                                            char         *message,       /* w or null */
                                            size_t       *messageLen,
                                            VNGTimeout    timeout)
{
    IOSError     iose;
    VNGErrCode   rv;
    VNPIBuf_VNG_RecvNotification *b = VNP_IOCTLV_BUF;
    VNPA_r__VNG_RecvNotification *a = &b->a_r;
    VNPV_r__VNG_RecvNotification *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VNG_RecvNotification)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VNG_RecvNotification)/sizeof(IOSIoVector));

    CHECK_VNG(vng);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vng.base    = (u8*) vng;
    v->read.vng.length  = sizeof *vng;

    if (*messageLen && message) {
        v->write.message.base   = (u8*) message;
        v->write.message.length = *messageLen;
    }
    else {
        --writeCount;
    }

    a->vng = vng;
    /* pass &b->a__w.userInfo */
    a->message = message;
    a->timeout = timeout;
    b->a_rw.messageLen = messageLen ? *messageLen : 0;

    iose = IOS_Ioctlv (vng->id, VNP_FI_VNG_RECVNOTIFICATION, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else {
        rv = b->a__w.rv;
        if (userInfo) *userInfo = b->a__w.userInfo;
        if (messageLen) *messageLen = b->a_rw.messageLen;
    }

end:
    return rv;
}

LIBVNG_API VNGErrCode VNG_RegisterGame (VNG          *vng,
                                        VNGGameInfo  *info,     /* r */
                                        char         *comments, /* r */
                                        VNGTimeout    timeout)
{
    IOSError     iose;
    VNGErrCode   rv;
    VNPIBuf_VNG_RegisterGame *b = VNP_IOCTLV_BUF;
    VNPA_r__VNG_RegisterGame *a = &b->a_r;
    VNPV_r__VNG_RegisterGame *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VNG_RegisterGame)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VNG_RegisterGame)/sizeof(IOSIoVector));

    u32 commentsBufSize;
    
    CHECK_VNG(vng);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vng.base    = (u8*) vng;
    v->read.vng.length  = sizeof *vng;

    if (!info) {
        rv = VNGERR_INVALID_ARG;
        goto end;
    }

    if (comments) {
        commentsBufSize = 1 + strnlen(comments, VNG_GAME_COMMENTS_LEN);
        if (    commentsBufSize > VNG_GAME_COMMENTS_LEN) {
            rv = VNGERR_INVALID_LEN;
            goto end;
        }
        v->read.comments.base   = (u8*) comments;
        v->read.comments.length = commentsBufSize;
    }
    else {
        /* ok if caller doesn't want to provide a comments */
        ++vector;
        --readCount;
    }

    a->vng = vng;
    a->info = *info;
    a->comments = comments;
    a->timeout = timeout;


    iose = IOS_Ioctlv (vng->id, VNP_FI_VNG_REGISTERGAME, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else {
        rv = b->a__w.rv;
    }

end:
    return rv;
}

LIBVNG_API VNGErrCode VNG_UnregisterGame (VNG            *vng,
                                          VNId            vnId,
                                          VNGTimeout      timeout)
{
    IOSError     iose;
    VNGErrCode   rv;
    VNPIBuf_VNG_UnregisterGame *b = VNP_IOCTLV_BUF;
    VNPA_r__VNG_UnregisterGame *a = &b->a_r;
    VNPV_r__VNG_UnregisterGame *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VNG_UnregisterGame)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VNG_UnregisterGame)/sizeof(IOSIoVector));

    CHECK_VNG(vng);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vng.base    = (u8*) vng;
    v->read.vng.length  = sizeof *vng;

    a->vng = vng;
    a->vnId = vnId;
    a->timeout = timeout;

    iose = IOS_Ioctlv (vng->id, VNP_FI_VNG_UNREGISTERGAME, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else {
        rv = b->a__w.rv;
    }

end:
    return rv;
}


LIBVNG_API VNGErrCode VNG_UpdateGameStatus (VNG            *vng,
                                            VNId            vnId,
                                            uint32_t        gameStatus,
                                            uint32_t        numPlayers,
                                            VNGTimeout      timeout)
{
    IOSError     iose;
    VNGErrCode   rv;
    VNPIBuf_VNG_UpdateGameStatus *b = VNP_IOCTLV_BUF;
    VNPA_r__VNG_UpdateGameStatus *a = &b->a_r;
    VNPV_r__VNG_UpdateGameStatus *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VNG_UpdateGameStatus)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VNG_UpdateGameStatus)/sizeof(IOSIoVector));

    CHECK_VNG(vng);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vng.base    = (u8*) vng;
    v->read.vng.length  = sizeof *vng;

    a->vng = vng;
    a->vnId = vnId;
    a->gameStatus = gameStatus;
    a->numPlayers = numPlayers;
    a->timeout = timeout;

    iose = IOS_Ioctlv (vng->id, VNP_FI_VNG_UPDATEGAMESTATUS, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else {
        rv = b->a__w.rv;
    }

end:
    return rv;
}


LIBVNG_API VNGErrCode VNG_GetGameStatus (VNG            *vng,
                                         VNGGameStatus  *gameStatus,        /* rw */
                                         uint32_t        nGameStatus, 
                                         VNGTimeout      timeout)
{
    IOSError     iose;
    VNGErrCode   rv;
    VNPIBuf_VNG_GetGameStatus *b = VNP_IOCTLV_BUF;
    VNPA_r__VNG_GetGameStatus *a = &b->a_r;
    VNPV_r__VNG_GetGameStatus *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VNG_GetGameStatus)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VNG_GetGameStatus)/sizeof(IOSIoVector));

    CHECK_VNG(vng);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vng.base    = (u8*) vng;
    v->read.vng.length  = sizeof *vng;

    if (nGameStatus && gameStatus) {
        v->read.gameStatus.base   = (u8*) gameStatus;
        v->read.gameStatus.length = nGameStatus * sizeof *gameStatus;
        v->write.gameStatus.base   = (u8*) gameStatus;
        v->write.gameStatus.length = nGameStatus * sizeof *gameStatus;
    }
    else {
        ++vector;
        --readCount;
        --writeCount;
    }

    a->vng = vng;
    a->gameStatus = gameStatus;
    a->nGameStatus = nGameStatus;
    a->timeout = timeout;

    iose = IOS_Ioctlv (vng->id, VNP_FI_VNG_GETGAMESTATUS, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else {
        rv = b->a__w.rv;
    }

end:
    return rv;
}


LIBVNG_API VNGErrCode VNG_GetGameComments (VNG            *vng,
                                           VNId            vnId,
                                           char           *comments,        /* r  or null */
                                           size_t         *commentSize,     /* rw or null */
                                           VNGTimeout      timeout)
{
    IOSError     iose;
    VNGErrCode   rv;
    VNPIBuf_VNG_GetGameComments *b = VNP_IOCTLV_BUF;
    VNPA_r__VNG_GetGameComments *a = &b->a_r;
    VNPV_r__VNG_GetGameComments *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VNG_GetGameComments)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VNG_GetGameComments)/sizeof(IOSIoVector));

    CHECK_VNG(vng);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vng.base    = (u8*) vng;
    v->read.vng.length  = sizeof *vng;

    if (commentSize && *commentSize && comments) {
        v->write.comments.base   = (u8*) comments;
        v->write.comments.length  = *commentSize;
    } else {
        --writeCount;
    }

    a->vng = vng;
    a->vnId = vnId;
    a->comments = comments;
    a->commentSize = (commentSize) ? &b->a_rw.commentSize : 0;
    a->timeout = timeout;

    iose = IOS_Ioctlv (vng->id, VNP_FI_VNG_GETGAMECOMMENTS, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else {
        rv = b->a__w.rv;
        if (commentSize) *commentSize = b->a_rw.commentSize;
    }

end:
    return rv;
}


LIBVNG_API VNGErrCode VNG_SearchGames (VNG                 *vng,
                                       VNGSearchCriteria   *searchCriteria,  /* r or NULL */
                                       VNGGameStatus       *gameStatus,      /* w  */
                                       uint32_t            *nGameStatus,     /* rw */
                                       uint32_t             skipN,
                                       VNGTimeout           timeout)
{
    IOSError     iose;
    VNGErrCode   rv;
    VNPIBuf_VNG_SearchGames *b = VNP_IOCTLV_BUF;
    VNPA_r__VNG_SearchGames *a = &b->a_r;
    VNPV_r__VNG_SearchGames *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VNG_SearchGames)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VNG_SearchGames)/sizeof(IOSIoVector));

    CHECK_VNG(vng);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vng.base    = (u8*) vng;
    v->read.vng.length  = sizeof *vng;

    if (!nGameStatus || !*nGameStatus) {
        rv = VNGERR_INVALID_ARG;
        goto end;
    }

    if (!gameStatus) {
        *nGameStatus = 0;
        rv = VNGERR_INVALID_ARG;
        goto end;
    }

    v->write.gameStatus.base    = (u8*) gameStatus;
    v->write.gameStatus.length  = *nGameStatus * sizeof *gameStatus;

    a->vng = vng;
    if (searchCriteria) {
        a->searchCriteria = &a->searchCriteriaBuf;
        a->searchCriteriaBuf = *searchCriteria;
    } else {
        a->searchCriteria = 0;
    }
    a->gameStatus = gameStatus;
    b->a_rw.nGameStatus = *nGameStatus;
    a->skipN = skipN;
    a->timeout = timeout;

    iose = IOS_Ioctlv (vng->id, VNP_FI_VNG_SEARCHGAMES, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else {
        rv = b->a__w.rv;
        *nGameStatus = b->a_rw.nGameStatus;
    }

end:
    return rv;
}


LIBVNG_API VNGErrCode VNG_AddServerToAdhocDomain (VNG        *vng,
                                                  const char *serverName,   /* r */
                                                  VNGPort     serverPort)
{
    IOSError     iose;
    VNGErrCode   rv;
    VNPIBuf_VNG_AddServerToAdhocDomain *b = VNP_IOCTLV_BUF;
    VNPA_r__VNG_AddServerToAdhocDomain *a = &b->a_r;
    VNPV_r__VNG_AddServerToAdhocDomain *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VNG_AddServerToAdhocDomain)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VNG_AddServerToAdhocDomain)/sizeof(IOSIoVector));

    u32 serverNameBufSize;
    
    CHECK_VNG(vng);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vng.base    = (u8*) vng;
    v->read.vng.length  = sizeof *vng;

    if (!serverName) {
        rv = VNGERR_INVALID_ARG;
        goto end;
    }
        
    serverNameBufSize = 1 + strnlen(serverName, VNG_SERVER_NAME_BUF_SIZE);

    if (serverNameBufSize > VNG_SERVER_NAME_BUF_SIZE) {
        rv = VNGERR_INVALID_LEN;
        goto end;
    }

    v->read.serverName.base   = (u8*) serverName;
    v->read.serverName.length = serverNameBufSize;

    a->vng = vng;
    a->serverName = serverName;
    a->serverPort = serverPort;

    iose = IOS_Ioctlv (vng->id, VNP_FI_VNG_ADDSERVERTOADHOCDOMAIN, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else {
        rv = b->a__w.rv;
    }

end:
    return rv;
}


LIBVNG_API VNGErrCode VNG_ResetAdhocDomain (VNG *vng)
{
    IOSError     iose;
    VNGErrCode   rv;
    VNPIBuf_VNG_ResetAdhocDomain *b = VNP_IOCTLV_BUF;
    VNPA_r__VNG_ResetAdhocDomain *a = &b->a_r;
    VNPV_r__VNG_ResetAdhocDomain *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VNG_ResetAdhocDomain)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VNG_ResetAdhocDomain)/sizeof(IOSIoVector));

    CHECK_VNG(vng);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vng.base    = (u8*) vng;
    v->read.vng.length  = sizeof *vng;

    a->vng = vng;

    iose = IOS_Ioctlv (vng->id, VNP_FI_VNG_RESETADHOCDOMAIN, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else {
        rv = b->a__w.rv;
    }

end:
    return rv;
}


LIBVNG_API uint32_t VNG_ConnectStatus (VNG *vng)
{
    IOSError     iose;
    uint32_t     rv;
    VNPIBuf_VNG_ConnectStatus *b = VNP_IOCTLV_BUF;
    VNPA_r__VNG_ConnectStatus *a = &b->a_r;
    VNPV_r__VNG_ConnectStatus *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VNG_ConnectStatus)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VNG_ConnectStatus)/sizeof(IOSIoVector));

    CHECK_VNG_RV(vng, 0);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vng.base    = (u8*) vng;
    v->read.vng.length  = sizeof *vng;

    a->vng = vng;

    iose = IOS_Ioctlv (vng->id, VNP_FI_VNG_CONNECTSTATUS, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv =VNG_ST_RM_OK;
    } else {
        rv = b->a__w.rv;
    }

end:
    return rv;
}


LIBVNG_API VNGUserId VNG_MyUserId (VNG *vng)
{
    IOSError         iose;
    VNGUserId        rv;
    VNPIBuf_VNG_MyUserId *b = VNP_IOCTLV_BUF;
    VNPA_r__VNG_MyUserId *a = &b->a_r;
    VNPV_r__VNG_MyUserId *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VNG_MyUserId)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VNG_MyUserId)/sizeof(IOSIoVector));

    CHECK_VNG_RV(vng, VNG_INVALID_USER_ID);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vng.base    = (u8*) vng;
    v->read.vng.length  = sizeof *vng;

    a->vng = vng;
    
    iose = IOS_Ioctlv (vng->id, VNP_FI_VNG_MYUSERID, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = VNG_INVALID_USER_ID;
    } else {
        rv = b->a__w.rv;
    }

end:
    return rv;
}

LIBVNG_API VNMember VN_Self (VN *vn)
{
    IOSError     iose;
    VNMember     rv;
    VNPIBuf_VN_Self *b = VNP_IOCTLV_BUF;
    VNPA_r__VN_Self *a = &b->a_r;
    VNPV_r__VN_Self *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VN_Self)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VN_Self)/sizeof(IOSIoVector));

    CHECK_VN_RV(vn, VN_MEMBER_INVALID);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vn.base    = (u8*) vn;
    v->read.vn.length  = sizeof *vn;

    a->vn = vn;

    iose = IOS_Ioctlv (vn->vng_id, VNP_FI_VN_SELF, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = VN_MEMBER_INVALID;
    } else {
        rv = b->a__w.rv;
    }

end:
    return rv;
}


LIBVNG_API VNMember VN_Owner (VN *vn)
{
    IOSError      iose;
    VNMember      rv;
    VNPIBuf_VN_Owner *b = VNP_IOCTLV_BUF;
    VNPA_r__VN_Owner *a = &b->a_r;
    VNPV_r__VN_Owner *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VN_Owner)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VN_Owner)/sizeof(IOSIoVector));

    CHECK_VN_RV(vn, VN_MEMBER_INVALID);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vn.base    = (u8*) vn;
    v->read.vn.length  = sizeof *vn;

    a->vn = vn;

    iose = IOS_Ioctlv (vn->vng_id, VNP_FI_VN_OWNER, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = VN_MEMBER_INVALID;
    } else {
        rv = b->a__w.rv;
    }

end:
    return rv;
}


LIBVNG_API VNState VN_State (VN *vn)
{
    IOSError      iose;
    VNState       rv;
    VNPIBuf_VN_State *b = VNP_IOCTLV_BUF;
    VNPA_r__VN_State *a = &b->a_r;
    VNPV_r__VN_State *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VN_State)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VN_State)/sizeof(IOSIoVector));

    CHECK_VN_RV(vn, VN_EXITED);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vn.base    = (u8*) vn;
    v->read.vn.length  = sizeof *vn;

    a->vn = vn;

    iose = IOS_Ioctlv (vn->vng_id, VNP_FI_VN_STATE, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = VN_EXITED;
    } else {
        rv = b->a__w.rv;
    }

end:
    return rv;
}



LIBVNG_API  VNGErrCode VNG_GetVNs (VNG       *vng,
                                   VN        *vns,  /* w or null */
                                   uint32_t  *nVNs) /* rw */
{
    IOSError     iose;
    VNGErrCode   rv;
    VNPIBuf_VNG_GetVNs *b = VNP_IOCTLV_BUF;
    VNPA_r__VNG_GetVNs *a = &b->a_r;
    VNPV_r__VNG_GetVNs *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VNG_GetVNs)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VNG_GetVNs)/sizeof(IOSIoVector));

    CHECK_VNG(vng);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vng.base    = (u8*) vng;
    v->read.vng.length  = sizeof *vng;

    if (!nVNs) {
        rv = VNGERR_INVALID_ARG;
        goto end;
    }

    if (*nVNs && vns) {
        v->write.vns.base   = (u8*) vns;
        v->write.vns.length = *nVNs * sizeof *vns;
    } else {
        --writeCount;
    }

    a->vng = vng;
    a->vns  = vns;
    b->a_rw.nVNs = *nVNs;

    iose = IOS_Ioctlv (vng->id, VNP_FI_VNG_GETVNS, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else {
        rv = b->a__w.rv;
        *nVNs = b->a_rw.nVNs;
    }

end:
    return rv;
}



LIBVNG_API VNGErrCode VNG_SetTraceOpt (VNG          *vng, 
                                       VNGTraceOpt   trop)
{
    IOSError     iose;
    VNGErrCode   rv;
    VNPIBuf_VNG_SetTraceOpt *b = VNP_IOCTLV_BUF;
    VNPA_r__VNG_SetTraceOpt *a = &b->a_r;
    VNPV_r__VNG_SetTraceOpt *v = &b->v_r;
    IOSIoVector *vector = (IOSIoVector*) v;
    u32 readCount  = (sizeof(VNPRv_VNG_SetTraceOpt)/sizeof(IOSIoVector));
    u32 writeCount = (sizeof(VNPWv_VNG_SetTraceOpt)/sizeof(IOSIoVector));

    CHECK_VNG(vng);

    v->read.buf.base   = (u8*) b;
    v->read.buf.length = sizeof b->a_r + sizeof b->v_r + sizeof b->a_rw;

    v->write.buf.base   = (u8*) &b->a_rw;
    v->write.buf.length = sizeof b->a_rw + sizeof b->a__w;

    v->read.vng.base    = (u8*) vng;
    v->read.vng.length  = sizeof *vng;

    a->vng = vng;
    a->trop = trop;

    iose = IOS_Ioctlv (vng->id, VNP_FI_VNG_SETTRACEOPT, readCount, writeCount, vector);

    if (iose != IOS_ERROR_OK) {
        rv = rm2vngErr(iose);
    } else {
        rv = b->a__w.rv;
    }

end:
    return rv;
}



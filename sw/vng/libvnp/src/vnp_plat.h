/*
*                Copyright (C) 2005, BroadOn Communications Corp.
* 
*   These coded instructions, statements, and computer programs contain
*   unpublished  proprietary information of BroadOn Communications Corp.,
*   and  are protected by Federal copyright law. They may not be disclosed
*   to  third  parties or copied or duplicated in any form, in whole or in
*   part, without the prior written consent of BroadOn Communications Corp.
* 
*/


#ifndef __VNP_PLAT_H__
#define __VNP_PLAT_H__  1


#include "vng_types.h"
#include "shr_plat.h"
#include "shr_th.h"
#include "shr_mem.h"


#ifdef _WIN32
    #if _MSC_VER > 1000    
        #pragma once
    #endif
#endif

#ifdef __cplusplus
extern "C" {
#endif


typedef _SHRHeap _VNPHeap;


#define _vnp_assert_locked      _SHR_assert_locked
#define _vnp_assert_unlocked    _SHR_assert_unlocked


/* Mem allocation */
#ifdef _LINUX
    #define _vnp_malloc(size)        malloc(size)
    #define _vnp_free(ptr)           free(ptr)
#else
    #define _vnp_malloc(size)        _SHR_heap_alloc (__vnp.heap, size)
    #define _vnp_free(ptr)           _SHR_heap_free  (__vnp.heap, ptr)
#endif

#define _vng_malloc(vng, size)  _vnp_malloc(size)
#define _vng_free(vng, ptr)     _vnp_free(ptr)

typedef _SHRSemaphore           _VNPSemaphore;
#define _vnp_sem_init(s)        _SHR_sem_init(s,0,1,0)
#define _vnp_sem_wait           _SHR_sem_wait
#define _vnp_sem_trywait        _SHR_sem_trywait
#define _vnp_sem_post           _SHR_sem_post
#define _vnp_sem_destroy        _SHR_sem_destroy


typedef _SHRMutex               _VNPMutex;
#define _vnp_mutex_init(m)      _SHR_mutex_init(m,_SHR_MUTEX_RECURSIVE,0,0)
#define _vnp_mutex_lock         _SHR_mutex_lock
#define _vnp_mutex_unlock       _SHR_mutex_unlock
#define _vnp_mutex_trylock      _SHR_mutex_trylock
#define _vnp_mutex_destroy      _SHR_mutex_destroy


typedef _SHRThread     _VNPThread;    /* Handle ret'd by _vnp_thread_create  */
typedef _SHRThreadId   _VNPThreadId;  /* Thread id ret'd by _vnp_thread_self */
typedef _SHRThreadRT   _VNPThreadRT;  /* Thread return type                  */
#define _VNPThreadCC   _SHRThreadCC   /* Thread calling convention           */
#define _VNPThreadAttr _SHRThreadAttr /* Thread attr used by pthreads */

#define _vnp_thread_create _SHR_thread_create
#define _vnp_thread_join   _SHR_thread_join
#define _vnp_thread_exit   _SHR_thread_exit
#define _vnp_thread_sleep  _SHR_thread_sleep

typedef _VNPThreadRT (_VNPThreadCC *_VNPThreadFunc) (void *arg);


#ifdef  __cplusplus
}
#endif
 


#endif  /* __VNP_PLAT_H__ */


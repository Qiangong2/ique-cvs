/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */

#ifndef __VNP_RM_JT_H__
#define __VNP_RM_JT_H__

/*  This file contains definitions for
 *  accessing VNP RM APIs based on a func id passed from the VNP RM API wraper
 *  funcs to the VNP RM via calls to IOS_Ioctlv().
 *
 *  The Func ids used by VNP RM api wrappers are defined below.
 *
 *  The values correspond to the VNG part of the libapi NC FUNC IDS.
 *  However, they do not need to be the same.  The VNP RM and its
 *  API wrapper do not depend on the libapi values. But it is nicer if
 *  they track each other.
 *  
 *  Specifically, APIs can be added here before they are added to the libapi.
 *
 *  Valid range: 1000-1199
 *
 */

#define VNP_RM_FUNC_ID_MIN                  1001

#define VNP_FI_VNG_INIT                     1001
#define VNP_FI_VNG_FINI                     1002
#define VNP_FI_VNG_LOGIN                    1003
#define VNP_FI_VNG_LOGOUT                   1004
#define VNP_FI_VNG_GETLOGINTYPE             1005
#define VNP_FI_VNG_GETUSERINFO              1006
#define VNP_FI_VNG_NEWVN                    1007
#define VNP_FI_VNG_DELETEVN                 1008
#define VNP_FI_VNG_JOINVN                   1009
#define VNP_FI_VNG_GETJOINREQUEST           1010
#define VNP_FI_VNG_ACCEPTJOINREQUEST        1011
#define VNP_FI_VNG_REJECTJOINREQUEST        1012
#define VNP_FI_VNG_LEAVEVN                  1013
#define VNP_FI_VN_NUMMEMBERS                1014
#define VNP_FI_VN_GETMEMBERS                1015
#define VNP_FI_VN_MEMBERSTATE               1016
#define VNP_FI_VN_MEMBERUSERID              1017
#define VNP_FI_VN_MEMBERLATENCY             1018
#define VNP_FI_VN_MEMBERDEVICETYPE          1019
#define VNP_FI_VN_GETVNID                   1020
#define VNP_FI_VN_SELECT                    1021
#define VNP_FI_VN_SENDMSG                   1022
#define VNP_FI_VN_RECVMSG                   1023
#define VNP_FI_VN_SENDREQ                   1024
#define VNP_FI_VN_RECVREQ                   1025
#define VNP_FI_VN_SENDRESP                  1026
#define VNP_FI_VN_RECVRESP                  1027
#define VNP_FI_VN_REGISTERRPCSERVICE        1028
#define VNP_FI_VN_UNREGISTERRPCSERVICE      1029
#define VNP_FI_VN_SENDRPC                   1030
#define VNP_FI_VNG_RECVRPC                  1031
#define VNP_FI_VNG_GETEVENT                 1032
#define VNP_FI_VNG_SENDNOTIFICATION         1033
#define VNP_FI_VNG_RECVNOTIFICATION 	    1034
#define VNP_FI_VNG_REGISTERGAME             1035
#define VNP_FI_VNG_UNREGISTERGAME           1036
#define VNP_FI_VNG_UPDATEGAMESTATUS         1037
#define VNP_FI_VNG_GETGAMESTATUS            1038
#define VNP_FI_VNG_GETGAMECOMMENTS          1039
#define VNP_FI_VNG_SEARCHGAMES              1040
#define VNP_FI_VNG_ADDSERVERTOADHOCDOMAIN   1041
#define VNP_FI_VNG_RESETADHOCDOMAIN         1042
#define VNP_FI_VNG_CONNECTSTATUS            1043
#define VNP_FI_VNG_MYUSERID                 1044
#define VNP_FI_VN_SELF                      1045
#define VNP_FI_VN_OWNER                     1046
#define VNP_FI_VN_STATE                     1047
#define VNP_FI_VNG_GETVNS                   1048
#define VNP_FI_VNG_SETTRACEOPT              1049
#define VNP_FI_VNG_REGISTER                 1050
#define VNP_FI_VNG_REMOVEVNMEMBER           1051

#define VNP_RM_FUNC_ID_MAX                  1051




#endif /*  __VNP_RM_JT_H__ */





//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//


#include "vnp_i.h"


#ifdef _LINUX
#define _VNG_DEF_VN_HASH_NUM_BUCKETS   1019
#else
#define _VNG_DEF_VN_HASH_NUM_BUCKETS   131
#endif


// TODO  Improve hash map, make size dependent on expected entries
//       Maybe have a function that can be called after VNG_Init
//       that specifies expected num VN connections for device.
//       Games vs. server could be big difference.
//
uint32_t _vng_net_node_hash (_VN_net_t netid, _VN_host_t node)
{
    return  (_VN_make_addr (netid, node) + 3) % __vnp.vn.hash.num_buckets;
}


VNGErrCode _vng_vn_map_insert (VNG *vng, _VNGNetNode *vn)
{
    uint32_t    hv;  // hash value (i.e. bucket index)
    uint32_t    load;
   _VNGNetNode *other;

    if (!__vnp.vn.hash.num_buckets)
        __vnp.vn.hash.num_buckets = _VNG_DEF_VN_HASH_NUM_BUCKETS;

    if (!__vnp.vn.hash.table) {
        size_t len = __vnp.vn.hash.num_buckets * sizeof vn;
        __vnp.vn.hash.table = _vng_malloc (vng, len);

        if (!__vnp.vn.hash.table)
            return VNGERR_NOMEM;
        memset (__vnp.vn.hash.table, 0, len);
        __vnp.vn.hash.max_load = 1;
    }

    hv = _vng_net_node_hash (vn->netid, vn->self);
    other = __vnp.vn.hash.table[hv];

    if (!other) {
        __vnp.vn.hash.table [hv] = vn;
        vn->next = NULL;
    }
    else {
        for (load = 1;  other != vn;  other = other->next, ++load) {            
            if (!other->next) {
                other->next = vn;
                vn->next = NULL;
                ++load;
                if (__vnp.vn.hash.max_load < load)
                    __vnp.vn.hash.max_load = load;
                break;
            }
        }
    }
    return VNG_OK;
}



VNGErrCode _vng_vn_map_remove (VNG *vng, _VNGNetNode *vn)
{
    uint32_t hv;  // hash value (i.e. bucket index)

   _VNGNetNode  **bucket;
   _VNGNetNode   *prev;

    if (vn->netid == _VN_NET_ANY) {
        bucket = (_VNGNetNode**) &__vnp.vn.any;
    }
    else if (__vnp.vn.hash.table)  {
        hv = _vng_net_node_hash (vn->netid, vn->self);
        bucket = (_VNGNetNode**) &__vnp.vn.hash.table[hv];
    }
    else {
        return VNG_OK;
    }

    if (*bucket) {
        if (*bucket == vn) {
            *bucket = vn->next;
            vn->next = NULL;
        }
        else for (prev = *bucket;  prev->next;  prev = prev->next) {
            if (prev->next == vn) {
                prev->next = vn->next;
                vn->next = NULL;
                break;
            }
        }
    }

    return VNG_OK;
}


_VNGNetNode*  _vng_netNode2vn (_VN_net_t netid, _VN_host_t self)
{
    return _vng_netNodeId2vn (netid, self, 0);
}

// Returns NULL for a _VN_NET_ANY because a vn_any 
// addr is _VN_ADDR_INVALID. For a vn_any use _vng_netNodeId2vn.
//
_VNGNetNode* _vng_addr2vn (_VN_addr_t addr)
{
    return _vng_netNode2vn (_VN_addr2net(addr), _VN_addr2host(addr));
}

_VNGNetNode*  _vng_netid2ownervn (_VN_net_t  netid)
{
   _VNGNetNode*  vn;
   _VN_host_t owner;

    owner = _VN_get_owner_host_id (netid);
    if (owner == _VN_HOST_INVALID
            || !(vn = _vng_netNode2vn (netid, owner))) {
        return  NULL;
    }
    return vn;
}



// The vn_id is optional.  Pass 0 (i.e. _VNG_INVALID_VNGVNID) when not needed.
// Non-zero vn_id is needed if netid is _VN_NET_ANY.
//
// If vn_id is _VNG_SERVER_VN_ID, netid/self are ignored and a pointer
// to the static vng infrastructure server _VNGNetNode is returned.
//
// vn_id is used for RPC case to guarantee the vn wasn't left and
// rejoined for the smae device getting the same netid/self.
// See _VNGRpc comments in vnp_i.h.
//
_VNGNetNode*  _vng_netNodeId2vn (_VN_net_t netid, _VN_host_t self, _VNGVnId vn_id)
{
    uint32_t    hv;  // hash value (i.e. bucket index)
   _VNGNetNode *rv = NULL;
   _VNGNetNode *vn;

    if (vn_id == _VNG_SERVER_VN_ID) {
        vn = &__vnp.vn.server;
    }
    else if (netid == _VN_NET_ANY) {
        if (vn_id)
            vn = __vnp.vn.any;
        else
            vn = NULL;
    }
    else if (__vnp.vn.hash.table)  {
        hv = _vng_net_node_hash (netid, self);
        vn = __vnp.vn.hash.table [hv];
    }
    else {
        vn = NULL;
    }

    for (;  vn;  vn = vn->next) {
        if (vn->netid == netid && vn->self == self
                            && (!vn_id || vn->id == vn_id)) {
            rv = vn;
            break;
        }
    }

    return rv;
}



// The vn_id is not optional.  0 is not a valid vn_id.
// If vn_id is _VNG_SERVER_VN_ID, addr is ignored and a pointer
// to the static vng infrastructure server _VNGNetNode is returned.
//
// For a vn_any, addr is _VN_ADDR_INVALID.  So if the addr passed
// to _vng_addrId2vn() is _VN_ADDR_INVALID, the list of vn_any's
// will be checked for a match of the vn_id.
//
// vn_id is used for external API calls to guarantee the vn wasn't
// left and rejoined for the smae device getting the same netid/self.
// See _VNGRpc comments in vnp_i.h.
//
_VNGNetNode*  _vng_addrId2vn (_VN_addr_t addr, _VNGVnId vn_id)
{
    if (!vn_id) {
        return NULL;
    } else if (vn_id == _VNG_SERVER_VN_ID) {
        return &__vnp.vn.server;
    } else if (addr == _VN_ADDR_INVALID) {  // see if vn_id is a vn_any
        return _vng_netNodeId2vn (_VN_NET_ANY, VN_MEMBER_ANY, vn_id);
    } else {
        return _vng_netNodeId2vn (_VN_addr2net(addr), _VN_addr2host(addr), vn_id);
    }
}



// _vng_port2Svc returns service type associated with port

VNServiceType _vng_port2Svc (_VN_port_t port)
{
    return ((port - _VNG_SERVICE_PORT_BASE) >> 3) + 1;
}


// _vng_svc2port reuturns port for service type

_VN_port_t    _vng_svc2port (VNServiceType svc)
{
    return  ((svc-1) << 3)  + _VNG_SERVICE_PORT_BASE;
}


// Since we never distinguish reliable vs. unreliable msessgaes
// for received msgs, _vng_svc2SvcSet returns svc set with
// both bits set for either reliable or unreliable svc.
// To get the specific mask use _vng_svc2SvcMask
VNServiceTypeSet  _vng_svc2SvcSet (VNServiceType svc)
{
    static const VNServiceTypeSet svcSet [] = {

        VN_SERVICE_TYPE_ANY_MASK,               // VN_SERVICE_TYPE_ANY

          VN_SERVICE_TYPE_UNRELIABLE_MSG_MASK   // VN_SERVICE_TYPE_UNRELIABLE_MSG
        | VN_SERVICE_TYPE_RELIABLE_MSG_MASK,

          VN_SERVICE_TYPE_UNRELIABLE_MSG_MASK   // VN_SERVICE_TYPE_RELIABLE_MSG
        | VN_SERVICE_TYPE_RELIABLE_MSG_MASK,

        VN_SERVICE_TYPE_REQUEST_MASK,           // VN_SERVICE_TYPE_REQUEST

        VN_SERVICE_TYPE_RESPONSE_MASK           // VN_SERVICE_TYPE_RESPONSE
    };

    if (svc > VN_SERVICE_TYPE_MAX)
        return 0;
    else
        return svcSet [svc];
}


// Since we never distinguish reliable vs. unreliable msessgaes
// for received msgs, _vng_port2SvcSet returns svc set with
// both bits set for either reliable or unreliable port.
// To get the specific mask use _vng_svc2SvcMask
VNServiceTypeSet  _vng_port2SvcSet (_VN_port_t port)
{
    return _vng_svc2SvcSet (_vng_port2Svc (port));
}



VNServiceTypeSet  _vng_svc2SvcMask (VNServiceType svc)
{
    static const VNServiceTypeSet svcSet [] = {

        0xFFFFFFFF,                             // VN_SERVICE_TYPE_ANY

        VN_SERVICE_TYPE_UNRELIABLE_MSG_MASK,

        VN_SERVICE_TYPE_RELIABLE_MSG_MASK,

        VN_SERVICE_TYPE_REQUEST_MASK,           // VN_SERVICE_TYPE_REQUEST

        VN_SERVICE_TYPE_RESPONSE_MASK           // VN_SERVICE_TYPE_RESPONSE
    };

    if (svc > VN_SERVICE_TYPE_MAX)
        return 0;
    else
        return svcSet [svc];
}

   





VNGErrCode _vng_config_ports (VNG *vng, _VN_addr_t addr, int port_state)
{
    if (0 > _VN_config_port (addr, _VNG_UNRELIABLE_MSG_PORT, port_state) ||
        0 > _VN_config_port (addr, _VNG_RELIABLE_MSG_PORT,   port_state) ||
        0 > _VN_config_port (addr, _VNG_RELIABLE_MSG_PORT |
                                   _VNG_2_VNG_PORT_FLAG,     port_state) ||
        0 > _VN_config_port (addr, _VNG_REQUSET_PORT,        port_state) || 
        0 > _VN_config_port (addr, _VNG_RESPONSE_PORT,       port_state)) {

        trace (ERR, API, "_vng_config_ports falied addr 0x%08x  state %d\n",
                         addr, port_state);
        return VNGERR_UNKNOWN;
    }
    return VNG_OK;
}



VNGErrCode _vng_leave_vn (VNG *vng, _VNGNetNode *vn) {
    return _vng_remove_vn_member (vng, vn, vn->self);
}


VNGErrCode _vng_remove_vn_member (VNG *vng, _VNGNetNode *vn, VNMember memb)
{
    VNGErrCode  ec = VNG_OK;
    VNGErrCode  cec = VNG_OK;
   _VN_addr_t   addr;
   _VN_addr_t   vn_server;

    trace (INFO, API, "leave vn %p  memb %u  netid 0x%08x  owner %u  self %u  flags 0x%08x\n",
            vn, memb, vn->netid, vn->owner, vn->self, vn->flags);

    if (vn->netid != _VN_NET_ANY) {
        addr      = _VN_make_addr (vn->netid, memb);
        vn_server = _VN_ADDR_INVALID;

        if (_VN_NETID_IS_GLOBAL(vn->netid) && __vnp.vn.server.state == VN_OK) {
            vn_server = _VN_make_addr (__vnp.vn.server.netid,
                                       __vnp.vn.server.owner);
        }
        if (0>_VN_leave_net (addr, vn_server))
            ec = VNGERR_UNKNOWN;
    }

    if (memb == vn->self) {
        cec = _vng_free_vn (vng, vn);
    }

    return ec ? ec : cec;
}



VNGErrCode _vng_free_vn (VNG *vng, _VNGNetNode *vn)
{
    VNGErrCode ec = VNG_OK;

   _VNGNewMsgRcvd   *rcvd;
   _VNGNewMsgWaiter *waiter;
   _VNGVnId          id = vn->id;

    // Remove pending msgs, registered rpcs, etc.

    trace (INFO, MISC, "Remove vn->id %u  vn_api %p  netid 0x%08x  "
            "owner %u  self %u  domain %u  policies 0x%08x  flags 0x%08x\n",
            id, vn->vn_api, vn->netid, vn->owner, vn->self,
            vn->domain, vn->policies, vn->flags);

    while ((rcvd = vn->received)) {
        _vng_evt_msg_rcvd_remove (vng, &vn->received, rcvd, true);
        _vng_vnEvent_free (vng, (_VNGEvent *) rcvd);
    }

    while ((waiter = vn->waiters)) {
        waiter->ev.ec = VNGERR_VN_ENDED;
        _vng_evt_msg_waiter_remove (vng, &vn->waiters, waiter, true);
        _vnp_sem_post (waiter->ev.syncObj);
    }

    while ((rcvd = vn->received_vng)) {
        _vng_evt_msg_rcvd_remove (vng, &vn->received_vng, rcvd, true);
        _vng_vnEvent_free (vng, (_VNGEvent *) rcvd);
    }

    while ((waiter = vn->waiters_vng)) {
        waiter->ev.ec = VNGERR_VN_ENDED;
        _vng_evt_msg_waiter_remove (vng, &vn->waiters_vng, waiter, true);
        _vnp_sem_post (waiter->ev.syncObj);
    }

    _vng_rpc_map_remove_all (vng, vn);

    // There may be game events that reference the vn.  We will allow
    // them to be delivered.  The vn will indicate that it is exited.

    _vng_vn_map_remove (vng, vn);

    // Never need to write to vn_api after VNG_NewVN or VNG_JoinVN
    // The id is unique, so any access will not find the _VNGNetNode
    // in the hash map and VNGERR_INVALID_VN will be returned.
    // So don't need to do the following.
    // _vng_vn_init_exited (vng, vn->vn_api, NULL);

    if (vn->flags & _VNG_VN_AUTO_CREATED) {
        // Remember exited auto created vn so can find it on VNG_DeleteVN
        if (__vnp.vn.left_auto_created) {
            vn->next = __vnp.vn.left_auto_created;
        } else {
            vn->next = NULL;
        }
        __vnp.vn.left_auto_created = vn;
        vn->state = VN_EXITED;
    }
    else if (vn == &__vnp.vn.server) {
        _vng_vn_init_exited (vng, &__vnp.vn.server_vn_api, &__vnp.vn.server);
    }
    else {
        _vng_free (vng, vn);
    }

    trace (INFO, MISC, "Remove vn->id %u completed\n", id);

    return ec;
}





VNGErrCode  _vng_sconnect (VNG           *vng,           // in
                           const char    *serverName,    // in
                           VNGPort        serverPort,    // in
                           const void    *msg,           // in   
                           _VN_msg_len_t  msglen,        // in
                           char          *denyReason,    // ptr in/content out 
                          _VN_msg_len_t   denyReasonLen, // in
                           VNGTimeout     timeout)
{
    VNGErrCode       ec = VNGERR_UNKNOWN;
    int              rv;

    _VNGConnectionAcceptedEvent  waiter;

    _vng_vnEvent_init ((_VNGEvent*) &waiter, sizeof waiter, NULL);

    waiter.vn_ev = &waiter.buf.vn_ev;
    waiter.vn_ev->event.tag  = _VN_EVT_CONNECTION_ACCEPTED;

    if (0>(rv=_VN_sconnect (serverName, serverPort, msg, msglen))) {
        if (rv == _VN_ERR_SERVER || rv == _VN_ERR_NOTFOUND)
            ec = VNGERR_UNREACHABLE;
        else
            ec = _vng_vn2vng_err (rv);
    }
    else {
        waiter.vn_ev->event.call_id = rv;
        trace (FINER, API, "attempt server connect  serverName %s  port %u  call id 0x%08x\n",
               serverName, serverPort, rv);
        ec = _vng_vnEvent_wait (vng, NULL, (_VNGEvent*) &waiter, timeout, false);
        // The event data contains the new net id, my peer id, and owners id.
        // The sconnect request/response needs to be matched by call_id.
        //
        // So there is no point checking for a rcvd response event until
        // we get the call_id from the _VN_sconnect() and events
        // are returned from _VN_get_events().  Events returned by
        // _VN_get_events() won't be processed untill __vnp.mutex is released.
        // So the "checkFirst" arg to _vng_vnEvent_wait is false.
    }

    if (!ec) {
       _VN_host_t  self     = _VN_addr2host (waiter.vn_ev->myaddr);
       _VN_host_t  owner    =  waiter.vn_ev->owner;
       _VN_net_t   netid    = _VN_addr2net  (waiter.vn_ev->myaddr);
        VNDomain   domain   = _VN_NETID_IS_GLOBAL(netid) ? VN_DOMAIN_INFRA
                                                         : VN_DOMAIN_UNKNOWN;
        VNPolicies policies;

        int  recv_len = _VN_get_event_msg (waiter.vn_ev->msg_handle,
                                           &policies, sizeof policies);
        if (recv_len < (int)(sizeof policies)) {
            policies = VN_ABORT_ON_OWNER_EXIT;
        } else {
            policies = etohl(policies);
        }

        ec = _vng_vn_init (vng, &__vnp.vn.server_vn_api, &__vnp.vn.server,
                                netid, self, owner,
                                domain, policies, 0);
        trace (INFO, API, "server connect success vn %p  netid 0x%08x  owner %u  self %u\n",
               &__vnp.vn.server, netid, owner, self);
    }
    else if (waiter.msglen && waiter.msg_handle) {
        char         *msg;
       _VN_msg_len_t  msglen;
        int           recv_len;

        if (ec == VNGERR_CONN_REFUSED) {
            msg = denyReason;
            msglen = denyReasonLen;
        }
        else {
            msg = NULL;
            msglen = 0;
        }
        recv_len = _VN_get_event_msg (waiter.msg_handle, msg, msglen);
        if (denyReason && denyReasonLen) {
            if (recv_len < denyReasonLen)
                denyReason[recv_len] = '\0';
            else
                denyReason[denyReasonLen - 1] = '\0';
        }
    }

    if (ec)
        trace (INFO, API, "server connect failed: %d  serverName %s  port %u\n",
               ec, serverName, serverPort);

    return ec;
}






VNGErrCode  _vng_connect (VNG           *vng,           // in
                          VNId           vnId,          // in
                          const void    *msg,           // in
                         _VN_msg_len_t   msglen,        // in
                          char          *denyReason,    // ptr in/content out 
                         _VN_msg_len_t   denyReasonLen, // in
                          VN            *vn_api,        // out
                          VNGTimeout     timeout)
{
    VNGErrCode   ec = VNGERR_UNKNOWN;
    int          rv;
    bool         infra;
   _VN_addr_t    vn_server;
   _VN_guid_t    guid;

   _VNGConnectionAcceptedEvent  waiter;

    if ((ec = _vng_is_vnid_infra (vnId, &infra)))
        return ec;

    if (!infra)
        vn_server = _VN_ADDR_INVALID;
    else if (__vnp.vn.server.state == VN_OK)
        vn_server = _VN_make_addr (__vnp.vn.server.netid, __vnp.vn.server.owner);
    else
        return VNGERR_NOT_LOGGED_IN;

    _vng_vnEvent_init ((_VNGEvent*) &waiter, sizeof waiter, NULL);

    waiter.vn_ev = &waiter.buf.vn_ev;
    waiter.vn_ev->event.tag  = _VN_EVT_CONNECTION_ACCEPTED;

    guid.chip_id = deviceIdToChipId (vnId.deviceId);
    guid.device_type = deviceIdToDeviceType (vnId.deviceId);

    if (0>(rv=_VN_connect(guid, vnId.netId, vn_server, msg, msglen))) {
        if (rv == _VN_ERR_SERVER || rv == _VN_ERR_NOTFOUND)
            ec = VNGERR_UNREACHABLE;
        else
            ec = _vng_vn2vng_err (rv);
    }
    else {
        waiter.vn_ev->event.call_id = rv;
        trace (FINER, API, "attempt connect  deviceId 0x%016llx  netId 0x%08x  call id 0x%08x\n",
                       vnId.deviceId, vnId.netId, rv);

        ec = _vng_vnEvent_wait (vng, NULL, (_VNGEvent*) &waiter, timeout, false);
        // The event data contains the new net id, my peer id, and owners id.
        // The connect request/response needs to be matched by call_id.
        //
        // So there is no point checking for a rcvd response event until
        // we get the call_id from the _VN_connect() and events
        // are returned from _VN_get_events().  Events returned by
        // _VN_get_events() won't be processed until __vnp.mutex is released.
        // So the "checkFirst" arg to _vng_vnEvent_wait is false.
    }

    if (!ec) {
       _VN_guid_t  guid     = _VN_GUID_INVALID;
       _VN_host_t  self     = _VN_addr2host (waiter.vn_ev->myaddr);
       _VN_host_t  owner    =  waiter.vn_ev->owner;
       _VN_net_t   netid    = _VN_addr2net  (waiter.vn_ev->myaddr);
        VNDomain   domain   = _VN_NETID_IS_GLOBAL(netid) ? VN_DOMAIN_INFRA
                                                         : VN_DOMAIN_UNKNOWN;
        VNPolicies policies;

        int  recv_len = _VN_get_event_msg (waiter.vn_ev->msg_handle,
                                           &policies, sizeof policies);
        if (recv_len < (int)(sizeof policies)) {
            policies = VN_ABORT_ON_OWNER_EXIT;
        } else {
            policies = etohl(policies);
        }

        ec = _vng_vn_init (vng, vn_api, NULL,
                                netid, self, owner,
                                domain, policies, 0);

        if (0 > _VN_get_guid(waiter.vn_ev->myaddr, &guid)) {
            guid.chip_id = _VN_CHIP_ID_INVALID;
            guid.device_type = _VN_DEVICE_TYPE_UNKNOWN;
        }

        trace (INFO, API,
            "deviceId 0x%08x.0x%08x  joined vn %p  netid 0x%08x  owner %u  self %u\n",
            guid.device_type, guid.chip_id, vn_api, netid, owner, self);
    }
    else if (waiter.msglen && waiter.msg_handle) {
        char         *msg;
       _VN_msg_len_t  msglen;
        int           recv_len;

        if (ec == VNGERR_CONN_REFUSED) {
            msg = denyReason;
            msglen = denyReasonLen;
        }
        else {
            msg = NULL;
            msglen = 0;
        }
        recv_len = _VN_get_event_msg (waiter.msg_handle, msg, msglen);
        if (denyReason && denyReasonLen) {
            if (recv_len < denyReasonLen)
                denyReason[recv_len] = '\0';
            else
                denyReason[denyReasonLen - 1] = '\0';
            // can't return VNGERR_OVERFLOW when denyReasonLen isn't big
            // enough, because already returning VNGERR_CONN_REFUSED.
        }
    }

    if (ec)
        trace (INFO, API, "connect failed %d   deviceId 0x%016llx  netid 0x%08x\n",
               ec, vnId.deviceId, vnId.netId);

    return ec;
}





VNGErrCode  _vng_evt_new_connection (VNG                     *vng,
                                    _VN_new_connection_event *ev)
{
    VNGErrCode ec;
   _VN_host_t  self     = _VN_addr2host (ev->myaddr);
   _VN_net_t   netid    = _VN_addr2net (ev->myaddr);
    VNDomain   domain   = _VN_NETID_IS_GLOBAL(netid) ? VN_DOMAIN_INFRA
                                                     : VN_DOMAIN_UNKNOWN;
    VNPolicies policies = __vnp.vn.auto_create_policies;

    VN  *vn_api = _vng_malloc (vng, sizeof *vn_api);

    if (!vn_api)
        return VNGERR_NOMEM;

    // Currently VN always creates a class 1 network
    // for _VN_EVT_NEW_CONNECTION
    //
    // vng is the vng passed to _vng_dispatch_evts().  That will be the vng of
    // the first _VNG_Init().
    if ((ec = _vng_vn_init (vng, vn_api, NULL,
                            netid, self, self,
                            domain, policies,
                           _VNG_VN_AUTO_CREATED))) {

         _vng_free (vng, vn_api);
    }

    // never returns VNGERR_NOT_FOUND
    return ec;
}




static uint32_t _vng_netmask_map[] = 
{ _VN_NET_MASK1, _VN_NET_MASK2, _VN_NET_MASK3, _VN_NET_MASK4 };


uint32_t _vng_get_class_netmask (VNClass vnClass)
{
    int n = (sizeof _vng_netmask_map/sizeof _vng_netmask_map[0]);
    assert (vnClass >= 0 && vnClass < n);
    if (vnClass < 0 || vnClass >= n)
        return _vng_netmask_map[VN_DEFAULT_CLASS]; // should never happen

    return _vng_netmask_map[vnClass];
}


VNClass  _vng_get_netmask_class (uint32_t netmask)
{
    int i = (sizeof _vng_netmask_map/sizeof _vng_netmask_map[0]);
    while (--i > 0) {
        if (netmask == _vng_netmask_map[i])
            break;
    }
    // -1 is VN_INVALID_CLASS
    assert (i >= 0);

    return i;
}


VNClass  _vng_get_vn_class (_VN_net_t net_id)
{
    return net_id >> 30;
}



VNGErrCode  _vng_is_vnid_infra (VNId     vnid,
                                bool    *is_infra)
{
    *is_infra = _VN_NETID_IS_GLOBAL (vnid.netId);
    return _VN_NETID_IS_RESERVED (vnid.netId)  ?  VNGERR_UNKNOWN : VNG_OK;
}



bool _vng_is_net_host_valid (_VN_net_t net_id, _VN_host_t host_id)
{
    return      _VN_is_net_host_valid (net_id, host_id)
            && !_VN_NETID_IS_RESERVED (net_id);

}

// vn->vn_api will not be automatically initialized.
// Pass it in the vn_api argument to initialize vn->vn_api
// Only provide vn arg if vn is an initialized _VNGNetNode
// No identity is preserved, so don't call if want to preserve id or flags
//
VNGErrCode  _vng_vn_init_exited (VNG *vng, VN *vn_api, _VNGNetNode *vn)
{
    VNGErrCode  ec = VNG_OK;
   _VNGNetNode *vn_mapped;

    if (vn_api) {
        if (    vn_api->id
            && (vn_mapped = _vng_addrId2vn (vn_api->addr, vn_api->id))
            &&  vn_mapped->vn_api == vn_api) {

            // can not init exited if in vn hash map or __vnp.vn.any
            ec = VNGERR_INVALID_VN;
            goto end;
        }
        vn_api->id = 0;   // _VNG_INVALID_VNGVNID
        vn_api->vng_id = vng->id;
        vn_api->addr = _VN_ADDR_INVALID;
    }


    if (vn) {
        if (   vn->state == VN_OK
            && (vn->netid == _VN_NET_ANY ||
               _vng_is_net_host_valid (vn->netid, vn->self))
            && vn->id
            && vn == _vng_netNodeId2vn (vn->netid,
                                        vn->self,
                                        vn->id)) {
            // can not init exited if in vn hash map or __vnp.vn.any
            ec = VNGERR_INVALID_VN;
            goto end;
        }

        memset(vn, 0, sizeof *vn);
        vn->self  = _VN_HOST_INVALID;
        vn->owner = _VN_HOST_INVALID;
        vn->netid = _VNG_NET_INVALID;
        vn->state =  VN_EXITED;
        vn->vng   =  vng;
    }

end:
    return ec;
}


VNGErrCode  _vng_vn_init (VNG        *vng,
                          VN         *vn_api,
                         _VNGNetNode *vnn,
                         _VN_net_t    netid,
                         _VN_host_t   self,
                         _VN_host_t   owner,
                          VNDomain    domain,
                          VNPolicies  policies,
                          uint32_t    flags)
{
    VNGErrCode      ec;
    int             rv;
   _VNGNetNode     *any;
   _VNGNetNode     *vn = NULL;
   _VNGNetAddr      addr;

    if (netid == _VN_NET_ANY) {
        addr = _VN_ADDR_INVALID;
    } else {
        addr = _VN_make_addr(netid, self);
    }

    if (!vn_api) {
        ec = VNGERR_INVALID_ARG;
        goto end;
    }

    vn = vnn ? vnn : _vng_malloc(vng, sizeof *vn);

    if (!vn) {
        _vng_vn_init_exited (vng, vn_api, NULL);
        ec = VNGERR_NOMEM;
        goto end;
    }

    memset(vn, 0, sizeof *vn);
    vn->id       =  ++__vnp.last_vn_id;
    vn->vng_id   =  vng->id;
    vn->vn_api   =  vn_api;
    vn->vng      =  vng;
    vn->self     =  self;
    vn->owner    =  owner;
    vn->netid    =  netid;
    vn->state    =  VN_OK;
    vn->domain   =  domain;
    vn->policies =  policies;
    vn->flags    =  flags;

    vn->vn_api->addr = addr;
    vn->vn_api->id = vn->id;
    vn->vn_api->vng_id = vn->vng_id;

    if (policies & VN_ANY) {
        if (!__vnp.vn.any)
            __vnp.vn.any = vn;
        else {
            for (any = __vnp.vn.any;  any->next;  any = any->next)
                {;}
            any->next = vn;
        }
        ec = VNG_OK;
        goto end;
    }

    _vng_vn_map_insert (vng, vn);

    // configure ports to receive

    ec = _vng_config_ports (vng, addr, _VN_PT_ACTIVE);

    // Allow peers to request to join VN

    if (!ec && !(vn->flags & _VNG_VN_AUTO_CREATED) && vn->owner == vn->self) {
        if (0>(rv=_VN_listen_net(vn->netid, true))) {
            // Only possible err is Not Owner, but we just created
            // the vn as owner.  So an error should never happen.
            if (rv == _VN_ERR_NOT_OWNER)
                ec = VNGERR_VN_NOT_HOST;
            else
                ec = VNGERR_UNKNOWN;
        }
    }

    if (ec != VNG_OK) {
        _vng_free_vn (vng, vn);
    }

end:
    if (ec != VNG_OK) {
        trace (INFO, MISC,
             "_vng_vn_init failed %d  vn_api %p  vn_api->id %u  netid 0x%08x"
             "  owner %u  self %u  domain %u  policies 0x%08x  flags 0x%08x\n",
                ec, vn_api, (vn_api ? vn_api->id:0), netid, owner, self, domain, policies, flags);
    } else {
        trace (INFO, MISC,
            "_vng_vn_init success  vn_api %p  vn_api->id %u  netid 0x%08x  owner %u  self %u"
                "  domain %u  policies 0x%08x  flags 0x%08x\n",
                vn_api, vn_api->id, netid, owner, self, domain, policies, flags);
    }

    return ec;
}



VNGErrCode
_vng_evt_net_disconnected (VNG                       *vng,
                    const _VN_net_disconnected_event *rcvd)
{
    // We don't currently have a use for this event as it is redundant
    // to a _VN_net_config_event for each net peer.
    //
    // Since the cleanup of the disconnected net is already done
    // when processing the _VN_net_config_event, we will ignore
    // this event.

    return VNG_OK;
}



VNGErrCode
_vng_evt_net_config (VNG                       *vng,
                    const _VN_net_config_event *ev)
{
    VNGErrCode ec  = VNG_OK;
    VNGErrCode fec;

    int  i, n, rv;
    int  n_lochosts;
    int  n_peers = ev->n_joined + ev->n_left;

   _VNGNetNode  *vn = NULL;

   _VN_host_t  lochosts [UINT8_MAX];
   _VN_host_t  h4 = ev->hostid; // host the event is for,
                                // or _VN_HOST_INVALID if for all
    uint8_t    buflen = (sizeof lochosts)/(sizeof lochosts[0]);
    VNGEvent   gev;


    // For each host that left,
    //     if vn owner is local, not exited, not host that left
    //        and vn is VN_ABORT_ON_ANY_EXIT
    //          leave the owner vn
    //     if find vn for netid/host,
    //          remove vn and generate event
    //
    // for each loc host of net that isn't exited,
    //     for each left or joined peer
    //          generate event
    //
    // Note: If _vng_gev_find_waiter doesn't find a waiter,
    //          it copies gev to a new _VNGGameEvent
    //          and puts it in __vnp.gev_rcvd

    if (n_peers == 0)
        return VNG_OK;

    for (i = ev->n_joined; i < n_peers;  ++i) {
        if ((vn = _vng_netid2ownervn (ev->netid))
                        && vn->state != VN_EXITED
                        && vn->owner != ev->peers[i]
                        && (vn->policies & VN_ABORT_ON_ANY_EXIT)) {
            // _vng_netid2ownervn returns null if vn already
            // removed vn->owner and added owner to ev->peers.
            // Don't care if _vng_leave_vn returns error.
            trace (INFO, EVD, "leaving vn %p  netid 0x%08x  owner %u  self %u  "
                              "due to VN_ABORT_ON_ANY_EXIT  peer %u\n",
                    vn->vn_api, vn->netid, vn->owner, vn->self, ev->peers[i]);

            _vng_leave_vn (vn->vng, vn);
        }
        if ((vn = _vng_netNode2vn (ev->netid, ev->peers[i]))) {

            if (vn == &__vnp.vn.server) {
                gev.eid = VNG_EVENT_LOGOUT;  // exit from server vn is equiv to logout
                __vnp.loginType = VNG_NOT_LOGIN;
                __vnp.userId    = VNG_INVALID_USER_ID;
            }
            else {
                gev.eid = VNG_EVENT_PEER_STATUS;
                gev.evt.peerStatus.vn   = vn->vn_api;
                gev.evt.peerStatus.memb = vn->self;
            }

           _vng_free_vn (vng, vn);
            fec = _vng_gev_find_waiter (vng, gev);
            if (fec && !ec)
                ec = fec;
        }
    }

    if (0>=(rv=_VN_get_local_hostids (ev->netid, lochosts, buflen)))
        n_lochosts = 0;
    else if (rv > buflen)
        n_lochosts = buflen;
    else
        n_lochosts = rv;

    for (n = 0;   n < n_lochosts;  ++n ) {
        if (h4 != lochosts[n] && h4 != _VN_HOST_INVALID) {
            continue;
        }
        if ((vn = _vng_netNode2vn (ev->netid, lochosts[n]))
                        && vn != &__vnp.vn.server) {

            for (i=0; i < n_peers; ++i) {

                gev.eid = VNG_EVENT_PEER_STATUS;
                gev.evt.peerStatus.vn   = vn->vn_api;
                gev.evt.peerStatus.memb = ev->peers[i];

                fec = _vng_gev_find_waiter (vng, gev);
                if (fec && !ec)
                    ec = fec;
            }
       }
    }

    return ec;
}


VNGErrCode _vng_check_login (VNG *vng)
{
    VNGErrCode ec;

    if (!(ec = _vng_check_state (vng))) {
        if (__vnp.loginType == VNG_NOT_LOGIN) {
            ec = VNGERR_NOT_LOGGED_IN;
        }
    }

    return ec;
}



///////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2000-2003 Intel Corporation 
// All rights reserved. 
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions are met: 
//
// * Redistributions of source code must retain the above copyright notice, 
// this list of conditions and the following disclaimer. 
// * Redistributions in binary form must reproduce the above copyright notice, 
// this list of conditions and the following disclaimer in the documentation 
// and/or other materials provided with the distribution. 
// * Neither name of Intel Corporation nor the names of its contributors 
// may be used to endorse or promote products derived from this software 
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL INTEL OR 
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY 
// OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
///////////////////////////////////////////////////////////////////////////

#ifndef UPNP_NAT_CTRLPT_H
#define UPNP_NAT_CTRLPT_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>

#include "ithread.h"
#ifndef _WIN32
#include <unistd.h>
#endif
#include <stdarg.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>

#include "upnp.h"
#include "upnptools.h"
#include "sample_util.h"

#define NAT_SERVICE_SERVCOUNT	        3
#define NAT_SERVICE_WANIPCONN	        0
#define NAT_SERVICE_WANCOMMONIFCONFIG	1
#define NAT_SERVICE_L3FORWARDING        2

#define NAT_WANIPCONN_VARCOUNT		3
#define NAT_WANIPCONN_CONNTYPE		0
#define NAT_WANIPCONN_EXIPADDR          1
#define NAT_WANIPCONN_NUMPORTMAPS       2

#define NAT_WANCOMMONIFCONFIG_VARCOUNT	0

#define NAT_L3FORWARDING_VARCOUNT       0

#define NAT_MAX_VAL_LEN			5

#define NAT_SUCCESS				0
#define NAT_ERROR				(-1)
#define NAT_WARNING				1

/* This should be the maximum VARCOUNT from above */
#define NAT_MAXVARS				NAT_WANIPCONN_VARCOUNT

extern char NatDeviceType[];
extern char *NatServiceType[];
extern char *NatServiceName[];
extern char *NatVarName[NAT_SERVICE_SERVCOUNT][NAT_MAXVARS];
extern char NatVarCount[];

struct nat_service {
    char ServiceId[NAME_SIZE];
    char ServiceType[NAME_SIZE];
    char *VariableStrVal[NAT_MAXVARS];
    char EventURL[NAME_SIZE];
    char ControlURL[NAME_SIZE];
    char SID[NAME_SIZE];
};

extern struct NatDeviceNode *GlobalDeviceList;

struct NatDevice {
    char UDN[250];
    char DescDocURL[250];
    char FriendlyName[250];
    char PresURL[250];
    int  AdvrTimeOut;
    struct nat_service NatService[NAT_SERVICE_SERVCOUNT];
};

struct NatDeviceNode {
    struct NatDevice device;
    struct NatDeviceNode *next;
};

extern ithread_mutex_t DeviceListMutex;

extern UpnpClient_Handle ctrlpt_handle;

void	NatCtrlPointPrintHelp( void );
int		NatCtrlPointDeleteNode(struct NatDeviceNode*);
int		NatCtrlPointRemoveDevice(char*);
int		NatCtrlPointRemoveAll( void );
int		NatCtrlPointRefresh( void );


int		NatCtrlPointSendAction(int, int, char *, char **, char **, int);
int		NatCtrlPointSendActionNumericArg(int devnum, int service, char *actionName, char *paramName, int paramValue);
int		NatCtrlPointSendGetConnectionType(int devnum);
int		NatCtrlPointSendSetConnectionType(int, char*);
int             NatCtrlPointSendAddTcpPortMapping(int, int, char*, int);
int             NatCtrlPointSendAddUdpPortMapping(int, int, char*, int);
int             NatCtrlPointSendDeleteUdpPortMapping(int, int);
int             NatCtrlPointSendDeleteTcpPortMapping(int, int);

int		NatCtrlPointGetVar(int, int, char*);
int		NatCtrlPointGetConnectionType(int devnum);

int		NatCtrlPointGetDevice(int, struct NatDeviceNode **);
int		NatCtrlPointPrintList( void );
int		NatCtrlPointPrintDevice(int);
void	NatCtrlPointAddDevice (IXML_Document *, struct Upnp_Discovery *, char *, int); 
void    NatCtrlPointHandleGetVar(char *,char *,DOMString);
void	NatStateUpdate(char*,int, IXML_Document * , char **);
void	NatCtrlPointHandleEvent(Upnp_SID, int, IXML_Document *); 
void	NatCtrlPointHandleSubscribeUpdate(char *, Upnp_SID, int); 
int		NatCtrlPointCallbackEventHandler(Upnp_EventType, void *, void *);
void	NatCtrlPointVerifyTimeouts(int);
void	NatCtrlPointPrintCommands( void );
void*	NatCtrlPointCommandLoop( void* );
int		NatCtrlPointStart( print_string printFunctionPtr, state_update updateFunctionPtr );
int		NatCtrlPointStop( void );
int		NatCtrlPointProcessCommand( char *cmdline );

#ifdef __cplusplus
};
#endif

#endif //UPNP_NAT_CTRLPT_H

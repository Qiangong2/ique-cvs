///////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2000-2003 Intel Corporation 
// All rights reserved. 
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions are met: 
//
// * Redistributions of source code must retain the above copyright notice, 
// this list of conditions and the following disclaimer. 
// * Redistributions in binary form must reproduce the above copyright notice, 
// this list of conditions and the following disclaimer in the documentation 
// and/or other materials provided with the distribution. 
// * Neither name of Intel Corporation nor the names of its contributors 
// may be used to endorse or promote products derived from this software 
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL INTEL OR 
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY 
// OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
///////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include "sample_util.h"
#include "upnp_nat_ctrlpt.h"
#include <string.h>

/*
   Tags for valid commands issued at the command prompt 
 */
enum cmdloop_tvcmds {
    PRTHELP = 0, PRTFULLHELP, GETCONN, SETCONN, 
    ADD_TCP_MAP, ADD_UDP_MAP, DEL_TCP_MAP, DEL_UDP_MAP,
    WANIPCONN_ACTION, WANIPCONN_GENATAR,
    WANCONF_ACTION, WANCONF_GENATAR,
    PRTDEV, LSTDEV, REFRESH, EXITCMD
};

/*
   Data structure for parsing commands from the command line 
 */
struct cmdloop_commands {
    char *str;                  // the string 
    int cmdnum;                 // the command
    int numargs;                // the number of arguments
    char *args;                 // the args
} cmdloop_commands;

/*
   Mappings between command text names, command tag,
   and required command arguments for command line
   commands 
 */
static struct cmdloop_commands cmdloop_cmdlist[] = {
    {"Help", PRTHELP, 1, ""},
    {"HelpFull", PRTFULLHELP, 1, ""},
    {"ListDev", LSTDEV, 1, ""},
    {"Refresh", REFRESH, 1, ""},
    {"PrintDev", PRTDEV, 2, "<devnum>"},
    {"GetConn", GETCONN, 2, "<devnum>"},
    {"SetConn", SETCONN, 3, "<devnum> <type (string)>"},
    {"AddTcpPortMapping", ADD_TCP_MAP, 5, 
     "<devnum> <ext port (int)> <client addr (string)> <client port (int)>"},
    {"AddUdpPortMapping", ADD_UDP_MAP, 5, 
     "<devnum> <ext port (int)> <client addr (string)> <client port (int)>"},
    {"DelTcpPortMapping", DEL_TCP_MAP, 3, "<devnum> <ext port (int)>"},
    {"DelUdpPortMapping", DEL_UDP_MAP, 3, "<devnum> <ext port (int)>"},
    {"WanIPAction", WANIPCONN_ACTION, 2, "<devnum> <action (string)>"},
    {"WanIPGetVar", WANIPCONN_GENATAR, 2, "<devnum> <varname (string)>"},
    {"WanConfAction", WANCONF_ACTION, 2, "<devnum> <action (string)>"},
    {"WanConfGetVar", WANCONF_GENATAR, 2, "<devnum> <varname (string)>"},
    {"Exit", EXITCMD, 1, ""}
};

void
linux_print( const char *string )
{
    puts( string );
}

/********************************************************************************
 * NatCtrlPointPrintHelp
 *
 * Description: 
 *       Print help info for this application.
 ********************************************************************************/
void
NatCtrlPointPrintShortHelp( void )
{
    int i, ncmds;
    SampleUtil_Print( "Commands:" );
    ncmds = sizeof(cmdloop_cmdlist)/sizeof(cmdloop_cmdlist[0]);
    for (i = 0; i < ncmds; i++) {
        struct cmdloop_commands *cmd;
        cmd = &cmdloop_cmdlist[i];
        SampleUtil_Print(" %s: %s", cmd->str, cmd->args);
    }
}

void
NatCtrlPointPrintLongHelp( void )
{
    SampleUtil_Print( "" );
    SampleUtil_Print( "******************************" );
    SampleUtil_Print( "* NAT Control Point Help Info *" );
    SampleUtil_Print( "******************************" );
    SampleUtil_Print( "" );
    SampleUtil_Print
        ( "This sample control point application automatically searches" );
    SampleUtil_Print
        ( "for and subscribes to the services of television device emulator" );
    SampleUtil_Print
        ( "devices, described in the tvdevicedesc.xml description document." );
    SampleUtil_Print( "" );
    SampleUtil_Print( "Commands:" );
    SampleUtil_Print( "  Help" );
    SampleUtil_Print( "       Print this help info." );
    SampleUtil_Print( "  ListDev" );
    SampleUtil_Print
        ( "       Print the current list of NAT Device Emulators that this" );
    SampleUtil_Print
        ( "         control point is aware of.  Each device is preceded by a" );
    SampleUtil_Print
        ( "         device number which corresponds to the devnum argument of" );
    SampleUtil_Print( "         commands listed below." );
    SampleUtil_Print( "  Refresh" );
    SampleUtil_Print
        ( "       Delete all of the devices from the device list and issue new" );
    SampleUtil_Print
        ( "         search request to rebuild the list from scratch." );
    SampleUtil_Print( "  PrintDev       <devnum>" );
    SampleUtil_Print
        ( "       Print the state table for the device <devnum>." );
    SampleUtil_Print
        ( "         e.g., 'PrintDev 1' prints the state table for the first" );
    SampleUtil_Print( "         device in the device list." );
    SampleUtil_Print( "  GetConn        <devnum>" );
    SampleUtil_Print
        ( "       Sends the GetConnectionType action to the WanIPConnection\n"
          "       Service of device <devnum>." );

    SampleUtil_Print( "  SetConn        <devnum> <type (string)>" );
    SampleUtil_Print
        ( "       Sends the SetConnectionType action to the WanIPConnection\n"
          "       Service of device <devnum>." );

    SampleUtil_Print( "  WanIPAction     <devnum> <action>" );
    SampleUtil_Print
        ( "       Sends an action request specified by the string <action>" );
    SampleUtil_Print
        ( "         to the WanIPConnection Service of device <devnum>.  This command" );
    SampleUtil_Print
        ( "         only works for actions that have no arguments." );
    SampleUtil_Print
        ( "         (e.g., \"WanIPAction 1 IncreaseChannel\")" );

    SampleUtil_Print( "  WanIPGetVar     <devnum> <varname>" );
    SampleUtil_Print
        ( "       Requests the value of a variable specified by the string <varname>" );
    SampleUtil_Print
        ( "         from the WanIPConnection Service of device <devnum>." );
    SampleUtil_Print( "         (e.g., \"WanIPGetVar 1 Volume\")" );

    SampleUtil_Print( "  Exit" );
    SampleUtil_Print( "       Exits the control point application." );
}

/********************************************************************************
 * NatCtrlPointPrintCommands
 *
 * Description: 
 *       Print the list of valid command line commands to the user
 *
 * Parameters:
 *   None
 *
 ********************************************************************************/
void
NatCtrlPointPrintCommands(  )
{
    int i;
    int numofcmds = sizeof( cmdloop_cmdlist ) / sizeof( cmdloop_commands );

    SampleUtil_Print( "Valid Commands:" );
    for( i = 0; i < numofcmds; i++ ) {
        SampleUtil_Print( "  %-14s %s", cmdloop_cmdlist[i].str,
                          cmdloop_cmdlist[i].args );
    }
    SampleUtil_Print( "" );
}

/********************************************************************************
 * NatCtrlPointCommandLoop
 *
 * Description: 
 *       Function that receives commands from the user at the command prompt
 *       during the lifetime of the control point, and calls the appropriate
 *       functions for those commands.
 *
 * Parameters:
 *    None
 *
 ********************************************************************************/
void *
NatCtrlPointCommandLoop( void *args )
{
    char cmdline[100];

    while( 1 ) {
        SampleUtil_Print( ">> " );
        fgets( cmdline, 100, stdin );
        NatCtrlPointProcessCommand( cmdline );
    }
}

int
NatCtrlPointProcessCommand( char *cmdline )
{
    char cmd[100];
    char strarg[100];
    int arg_val_err = -99999;
    int arg1 = arg_val_err;
    int arg2 = arg_val_err;
    int arg3 = arg_val_err;
    int cmdnum = -1;
    int numofcmds = sizeof( cmdloop_cmdlist ) / sizeof( cmdloop_commands );
    int cmdfound = 0;
    int i,
      rc;
    int invalidargs = 0;
    int validargs;    

    validargs = sscanf( cmdline, "%s %d %d %s %d",
                        cmd, &arg1, &arg2, strarg, &arg3);

    for( i = 0; i < numofcmds; i++ ) {
        if( strcasecmp( cmd, cmdloop_cmdlist[i].str ) == 0 ) {
            cmdnum = cmdloop_cmdlist[i].cmdnum;
            cmdfound++;
            if( validargs != cmdloop_cmdlist[i].numargs )
                invalidargs++;
            break;
        }
    }

    if( !cmdfound ) {
        SampleUtil_Print( "Command not found; try 'Help'" );
        return NAT_SUCCESS;
    }

    if( invalidargs ) {
        SampleUtil_Print( "Invalid arguments; try 'Help'" );
        return NAT_SUCCESS;
    }

    switch ( cmdnum ) {
        case PRTHELP:
            NatCtrlPointPrintShortHelp(  );
            break;

        case PRTFULLHELP:
            NatCtrlPointPrintLongHelp(  );
            break;

        case GETCONN:
            SampleUtil_Print("connection: %d", arg1 );
            NatCtrlPointSendGetConnectionType( arg1 );
            break;

        case SETCONN:
            /*
               re-parse commandline since second arg is string 
             */
            validargs = sscanf( cmdline, "%s %d %s", cmd, &arg1, strarg );
            if( 3 == validargs )
                NatCtrlPointSendSetConnectionType( arg1, strarg );
            else
                invalidargs++;
            break;

        case ADD_TCP_MAP:
            NatCtrlPointSendAddTcpPortMapping(arg1, arg2, strarg, arg3);
            break;

        case ADD_UDP_MAP:
            NatCtrlPointSendAddUdpPortMapping(arg1, arg2, strarg, arg3);
            break;

        case DEL_TCP_MAP:
            NatCtrlPointSendDeleteTcpPortMapping(arg1, arg2);
            break;

        case DEL_UDP_MAP:
            NatCtrlPointSendDeleteUdpPortMapping(arg1, arg2);
            break;

        case WANIPCONN_ACTION:
            /*
               re-parse commandline since second arg is string 
             */
            validargs = sscanf( cmdline, "%s %d %s", cmd, &arg1, strarg );
            if( 3 == validargs )
                NatCtrlPointSendAction( NAT_SERVICE_WANIPCONN, arg1, strarg,
                                       NULL, NULL, 0 );
            else
                invalidargs++;
            break;

        case WANIPCONN_GENATAR:
            /*
               re-parse commandline since second arg is string 
             */
            validargs = sscanf( cmdline, "%s %d %s", cmd, &arg1, strarg );
            if( 3 == validargs )
                NatCtrlPointGetVar( NAT_SERVICE_WANIPCONN, arg1, strarg );
            else
                invalidargs++;
            break;

        case WANCONF_ACTION:
            /*
               re-parse commandline since second arg is string 
             */
            validargs = sscanf( cmdline, "%s %d %s", cmd, &arg1, strarg );
            if( 3 == validargs )
                NatCtrlPointSendAction( NAT_SERVICE_WANCOMMONIFCONFIG, arg1, strarg,
                                       NULL, NULL, 0 );
            else
                invalidargs++;
            break;

        case WANCONF_GENATAR:
            /*
               re-parse commandline since second arg is string 
             */
            validargs = sscanf( cmdline, "%s %d %s", cmd, &arg1, strarg );
            if( 3 == validargs )
                NatCtrlPointGetVar( NAT_SERVICE_WANCOMMONIFCONFIG, arg1, strarg );
            else
                invalidargs++;
            break;

        case PRTDEV:
            NatCtrlPointPrintDevice( arg1 );
            break;

        case LSTDEV:
            NatCtrlPointPrintList(  );
            break;

        case REFRESH:
            NatCtrlPointRefresh(  );
            break;

        case EXITCMD:
            rc = NatCtrlPointStop(  );
            exit( rc );
            break;

        default:
            SampleUtil_Print( "Command not implemented; see 'Help'" );
            break;
    }

    if( invalidargs )
        SampleUtil_Print( "Invalid args in command; see 'Help'" );

    return NAT_SUCCESS;
}

int
main( int argc,
      char **argv )
{
    int rc;
    ithread_t cmdloop_thread;
    int sig;
    sigset_t sigs_to_catch;
    int code;

    rc = NatCtrlPointStart( linux_print, NULL );
    if( rc != NAT_SUCCESS ) {
        SampleUtil_Print( "Error starting UPnP NAT Control Point" );
        exit( rc );
    }
    // start a command loop thread
    code =
        ithread_create( &cmdloop_thread, NULL, NatCtrlPointCommandLoop,
                        NULL );

    /*
       Catch Ctrl-C and properly shutdown 
     */
    sigemptyset( &sigs_to_catch );
    sigaddset( &sigs_to_catch, SIGINT );
    sigwait( &sigs_to_catch, &sig );

    SampleUtil_Print( "Shutting down on signal %d...", sig );
    rc = NatCtrlPointStop(  );
    exit( rc );
}

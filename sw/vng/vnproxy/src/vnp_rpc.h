//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#ifndef __VNP_RPC_H__
#define __VNP_RPC_H__

#include "vnlocal.h"

#ifdef __cplusplus
extern "C" {
#endif

extern uint32_t _vn_proxy_rpc_min_timeout;
extern uint32_t _vn_proxy_rpc_max_timeout;

int _vn_proxy_register_rpcs(_vn_usb_rpc_service_t service);

/* RPC functions calls to the SC */
int _vn_proxy_get_device_info(_vn_netif_t *netif, 
                              _VN_guid_t *guid);
int _vn_proxy_dispatch_pkt(_vn_netif_t* netif, _vn_buf_t* pkt,
                           _vn_inaddr_t addr, _vn_inport_t port);
int _vn_proxy_enqueue_keep_alive(_vn_netif_t* netif, _VN_addr_t vnaddr);
int _vn_proxy_device_sync(_vn_netif_t* netif);
int _vn_proxy_device_set_trace_level(_vn_netif_t* netif,
                                     int group, int subgroup, int level);

/* Registered RPC Functions on the VN Proxy */
int _vn_proxy_rpc_netif_getaddr(_vn_usb_t *usb, 
                                const void *args, size_t argslen,
                                void *ret, size_t *retlen);
int _vn_proxy_rpc_netif_getlocalhostname(_vn_usb_t *usb,
                                         const void *args, size_t argslen,
                                         void *ret, size_t *retlen);
int _vn_proxy_rpc_netif_getlocalport(_vn_usb_t *usb,
                                     const void *args, size_t argslen,
                                     void *ret, size_t *retlen);
int _vn_proxy_rpc_netif_getlocalip(_vn_usb_t *usb,
                                   const void *args, size_t argslen,
                                   void *ret, size_t *retlen);
int _vn_proxy_rpc_netif_getlocalips(_vn_usb_t *usb,
                                    const void *args, size_t argslen,
                                    void *ret, size_t *retlen);
int _vn_proxy_rpc_netif_getstatus(_vn_usb_t *usb,
                                  const void *args, size_t argslen,
                                  void *ret, size_t *retlen);
int _vn_proxy_rpc_send_vn_pkt(_vn_usb_t *usb,
                              const void *args, size_t argslen,
                              void *ret, size_t *retlen);
int _vn_proxy_rpc_send_udp_pkt(_vn_usb_t *usb,
                               const void *args, size_t argslen,
                               void *ret, size_t *retlen);
int _vn_proxy_rpc_add_net(_vn_usb_t *usb,
                          const void *args, size_t argslen,
                          void *ret, size_t *retlen);
int _vn_proxy_rpc_del_net(_vn_usb_t *usb,
                          const void *args, size_t argslen,
                          void *ret, size_t *retlen);
int _vn_proxy_rpc_add_host(_vn_usb_t *usb,
                           const void *args, size_t argslen,
                           void *ret, size_t *retlen);
int _vn_proxy_rpc_del_host(_vn_usb_t *usb,
                           const void *args, size_t argslen,
                           void *ret, size_t *retlen);
int _vn_proxy_rpc_set_host(_vn_usb_t *usb,
                           const void *args, size_t argslen,
                           void *ret, size_t *retlen);
int _vn_proxy_rpc_get_host(_vn_usb_t *usb,
                           const void *args, size_t argslen,
                           void *ret, size_t *retlen);
int _vn_proxy_rpc_firewall_open(_vn_usb_t *usb,
                                const void *args, size_t argslen,
                                void *ret, size_t *retlen);
int _vn_proxy_rpc_netif_reset(_vn_usb_t *usb,
                              const void *args, size_t argslen,
                              void *ret, size_t *retlen);
int _vn_proxy_rpc_netif_init(_vn_usb_t *usb,
                              const void *args, size_t argslen,
                              void *ret, size_t *retlen);
int _vn_proxy_rpc_query_host(_vn_usb_t *usb,
                             const void *args, size_t argslen,
                             void *ret, size_t *retlen);
int _vn_proxy_rpc_clear_query_list(_vn_usb_t *usb,
                                   const void *args, size_t argslen,
                                   void *ret, size_t *retlen);
int _vn_proxy_rpc_discover_hosts(_vn_usb_t *usb,
                                 const void *args, size_t argslen,
                                 void *ret, size_t *retlen);

#ifdef  __cplusplus
}
#endif

#endif /* __VNP_RPC_H__ */

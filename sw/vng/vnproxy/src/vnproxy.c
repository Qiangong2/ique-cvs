//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnproxy.h"
#include "vnmux.h"

#include "shr_getopt.h"

#define MAXLINE     1024

_vn_proxy_client_t _vnp_clients[_VNP_CLIENTS_MAX];
_vn_mutex_t _vnp_mutex;

uint32_t _vn_proxy_timeout = 1000; /* 1 second timeout */

#define VERSION_MAJOR 0
#define VERSION_MINOR 6

/**
 * vnproxy sits on the PC and acts as an intermediary between the Internet 
 * and the VN library on the SC.  The SC is connected to the PC using a USB
 * cable.  vnproxy will communicate with the SC via RPC over USB.
 */

void usage(const char* name)
{
    printf("Usage: %s %d.%d (%s %s)\n", 
           name, VERSION_MAJOR, VERSION_MINOR,  __DATE__, __TIME__);
    printf("       [-i] [-r] [-p] [-l tracelevel] [-d tracelevel]\n");

#ifdef _VN_USB_SOCKETS
    printf("       [-u usbport]\n");
#endif

    printf("-i            Interactive mode\n");
    printf("-r            Poll for real USB (deprecated, default is to poll for USB)\n");
    printf("-p            Handle printf\n");
    printf("-d tracelevel Device trace level\n");
    printf("-l tracelevel Trace level to use\n");
    printf("              ERROR  = %d\n", TRACE_ERROR);
    printf("              WARN   = %d\n", TRACE_WARN);
    printf("              INFO   = %d\n", TRACE_INFO);
    printf("              FINE   = %d (default)\n", TRACE_FINE);
    printf("              FINER  = %d\n", TRACE_FINER);
    printf("              FINEST = %d\n", TRACE_FINEST);

#ifdef _VN_USB_SOCKETS
    printf("-u usbport    TCP port to use for USB connections (default %u)\n",
           _VN_USB_PORT_PROXY);
#endif
}

/* Locks vnproxy specific data structures */
int _vn_proxy_lock()
{
    /* TODO: Remove use of _vnp_mutex and just use _vn_net_lock? */
    return _vn_mutex_lock(&_vnp_mutex);
}

/* Unlocks vnproxy specific data structures */
int _vn_proxy_unlock()
{
    /* TODO: Remove use of _vnp_mutex and just use _vn_net_unlock? */
    return _vn_mutex_unlock(&_vnp_mutex);
}

_vn_proxy_client_t* _vn_proxy_get_client(_vn_usb_handle_t handle)
{
    if ((handle >= 0) && (handle < _VNP_CLIENTS_MAX)) {
        return &_vnp_clients[handle];
    }
    else return NULL;
}

_VN_guid_t _vn_proxy_get_client_guid(_vn_usb_handle_t handle)
{
    _vn_proxy_client_t* client;
    _VN_guid_t guid;

    _vn_proxy_lock();        
    client = _vn_proxy_get_client(handle);
    if (_VNP_CLIENT_VALID(client)) {
        guid = client->guid;
    } else {
        _vn_guid_set_invalid(&guid);
    }
    _vn_proxy_unlock();
    return guid;
}

int _vn_proxy_get_client_guids(_VN_guid_t *guids, int nguids)
{
    int i, n = 0;
    _VN_guid_t guid;
    
    for (i = 0; i < _VNP_CLIENTS_MAX; i++) {
        guid = _vn_proxy_get_client_guid(i);
        if (guids && (n < nguids)) {
            guids[n] = guid;
        }

        if (!_vn_guid_is_invalid(&guid)) {
            n++;
        }
    }
    return n;
}

int _vn_proxy_lookup_client_index(_VN_guid_t guid)
{
    int i;
    _vn_proxy_client_t *client;
   
    for (i = 0; i < _VNP_CLIENTS_MAX; i++) {
        _vn_proxy_lock();
        client = _vn_proxy_get_client(i);

        if (_VNP_CLIENT_VALID(client)) {
            if (_vn_guid_eq(&client->guid, &guid)) {
                _vn_proxy_unlock();
                return i;
            }
        }
        _vn_proxy_unlock();
    }
    return _VN_ERR_NOTFOUND;
}

/* Returns true if client is interested 
   in devices associated with the upnp info */
bool _vn_proxy_client_interested(_vn_proxy_client_t* client,
                                 int upnp_device_flags,
                                 _VN_guid_t guid)
{
    if (_VNP_CLIENT_VALID(client) && 
        !_vn_guid_eq(&client->guid, &guid)) {
#ifdef UPNP
        if (_VNP_CLIENT_DISCOVER(client)) {
            /* Client is doing discovering, interested in all LAN devices */
            bool res = (_VN_UPNP_VNDEVICE_FLAGS(upnp_device_flags) & 
                        _VN_DEVICE_PROP_LAN);
            _VN_TRACE(TRACE_FINEST, _VNP_SG_MAIN,
                      "client %d, flags 0x%08x, guid %d, interested? %d\n",
                      client->usb_handle, upnp_device_flags, guid, res);
            return res;
        }
        else {
            /* Client not doing discovering, check if host explicitly 
               queried for this device */
            bool res = ((_VN_UPNP_VNDEVICE_FLAGS(upnp_device_flags) & 
                         _VN_DEVICE_PROP_QUERIED) &&
                        (_VN_UPNP_CLIENT_TO_FLAG(client->usb_handle+1) & 
                         upnp_device_flags));
            _VN_TRACE(TRACE_FINEST, _VNP_SG_MAIN,
                      "client %d, flags 0x%08x, guid %d, interested? %d\n",
                      client->usb_handle, upnp_device_flags, guid, res);
            return res;
        }
#else
        return _VNP_CLIENT_DISCOVER(client);
#endif
    }
    else {
        return false;
    }
}

/* Tell client about existing devices we know about */
int _vn_proxy_update_discovered_devices(_vn_usb_handle_t handle)
{
    _vn_ht_iter_t iter;
    _vn_device_info_t* device;

    /* Lock devices data structure so we can iterate through */
    _vn_net_lock();

    _VN_TRACE(TRACE_FINEST, _VNP_SG_MAIN,
              "Update device table for client %d\n",
              handle);

    _vn_ht_iterator_init(&iter, _vn_get_device_table());
    
    while ((device = (_vn_device_info_t*) (_vn_ht_iterator_next(&iter)))) {
        if (device->flags & _VN_DEVICE_PROP_LAN) {
            _VN_TRACE(TRACE_FINEST, _VNP_SG_MAIN, 
                      "Update device %u\n", device->guid);
            _vn_rpc_call_add_device(handle,
                                    _VN_USB_RPC_SERVICE,
                                    _VN_USB_RPC_NOBLOCK_TIMEOUT,
                                    device);
        }
        else {
            _VN_TRACE(TRACE_FINEST, _VNP_SG_MAIN,
                      "Client %d not interested in device %u\n",
                      handle, device->guid);
        }
    }
    _vn_net_unlock();

    return _VN_ERR_OK;
}

/* Start/stop host discovery */
int _vn_proxy_discover_hosts(_vn_usb_handle_t handle, int flag)
{
    _vn_proxy_client_t *client;
    int rv = _VN_ERR_OK;
    bool update_devices = false;

    _VN_TRACE(TRACE_FINER, _VNP_SG_MAIN, "Discover hosts %d for client %d\n",
              flag, handle);

    _vn_proxy_lock();
    client = _vn_proxy_get_client(handle);
    if (_VNP_CLIENT_VALID(client)) {
        if (flag) {
            /* Start host discovery */
            if (!_VNP_CLIENT_DISCOVER(client)) {
                client->flags |= _VNP_CLIENT_FLAG_DISCOVER;
                update_devices = true;
            }
        }
        else {
            client->flags &= ~_VNP_CLIENT_FLAG_DISCOVER;
        }
    }
    else {
        rv = _VN_ERR_INVALID;
    }
    _vn_proxy_unlock();

    if (update_devices) {
        _vn_proxy_update_discovered_devices(handle);
    }

    return rv;
}

/* Starts/stops advertisements for client device */
int _vn_proxy_advertise_client(_vn_usb_handle_t handle, int flag)
{
    _vn_proxy_client_t *client;
    int rv = _VN_ERR_OK;
    bool notify_device = false;

    _vn_proxy_lock();
    client = _vn_proxy_get_client(handle);
    if (_VNP_CLIENT_VALID(client)) {
        if (flag) {
            /* Start advertising client */
            if (!_VNP_CLIENT_ADVERTISE(client)) {
                client->flags |= _VNP_CLIENT_FLAG_ADVERTISE;
                notify_device = true;
            }
        }
        else {
            if (_VNP_CLIENT_ADVERTISE(client)) {
                client->flags &= ~_VNP_CLIENT_FLAG_ADVERTISE;
                notify_device = true;                
            }
        }
    }
    else {
        rv = _VN_ERR_INVALID;
    }
    _vn_proxy_unlock();

    if (notify_device) {
        /* TODO: Tell other clients? */
        if (flag) {
#ifdef UPNP
            _vn_upnp_notify_device_added(client->guid); 
#endif
        }
        else {
#ifdef UPNP
            _vn_upnp_notify_device_removed(client->guid);
#endif
        }
    }

    return rv;
}

/* A new peer has been discovered, synchronize device table with clients */
int _vn_proxy_device_discovered_sync(int upnp_device_flags,
                                     _vn_device_info_t* device)
{
    int i, n = 0;
    _vn_proxy_client_t *client;
    _vn_usb_handle_t usb_handle;
    bool interested;
   
    assert(device);
    for (i = 0; i < _VNP_CLIENTS_MAX; i++) {
        _vn_proxy_lock();
        client = _vn_proxy_get_client(i);
        interested = _vn_proxy_client_interested(client, upnp_device_flags,
                                                 device->guid);
        usb_handle = client->usb_handle;
        _vn_proxy_unlock();

        /* Only tell client about new device if they are interested */
        if (interested) {
            _vn_rpc_call_add_device(usb_handle,
                                    _VN_USB_RPC_SERVICE,
                                    _vn_proxy_rpc_min_timeout,
                                    device);
            n++;
        }
    }
    return n;
}

/* The expiration time of peer has changed, synchronize device table with clients */
int _vn_proxy_device_timeout_changed_sync(int upnp_device_flags,
                                          _vn_device_info_t* device)
{
    int i, n = 0;
    _vn_proxy_client_t *client;
    _vn_usb_handle_t usb_handle;
    bool interested;
   
    assert(device);
    for (i = 0; i < _VNP_CLIENTS_MAX; i++) {
        _vn_proxy_lock();
        client = _vn_proxy_get_client(i);
        interested = _vn_proxy_client_interested(client, upnp_device_flags,
                                                 device->guid);
        usb_handle = client->usb_handle;
        _vn_proxy_unlock();

        /* Only tell client about new device if they are interested */
        if (interested) {
            _vn_rpc_call_set_device_timeout(usb_handle,
                                            _VN_USB_RPC_SERVICE,
                                            _vn_proxy_rpc_min_timeout,
                                            device->guid,
                                            _vn_timer_get_remaining(
                                                device->timer));
            n++;
        }
    }
    return n;
}

/* A peer has been removed, synchronize device table with clients */
int _vn_proxy_device_removed_sync(_VN_guid_t guid, uint8_t reason)
{
    int i, n = 0;
    _vn_proxy_client_t *client;
    _vn_usb_handle_t usb_handle;
    bool interested;

    for (i = 0; i < _VNP_CLIENTS_MAX; i++) {
        _vn_proxy_lock();
        client = _vn_proxy_get_client(i);
        interested = _VNP_CLIENT_VALID(client) && 
            !_vn_guid_eq(&client->guid, &guid);
        usb_handle = client->usb_handle;
        _vn_proxy_unlock();

        if (interested) {
            _vn_rpc_call_remove_device(usb_handle,
                                       _VN_USB_RPC_SERVICE,
                                       _vn_proxy_rpc_min_timeout,
                                       guid, reason);

            n++;
        }
    }
    return n;
}

/* A new service has been registered, tell clients about it */
int _vn_proxy_service_added_sync(int upnp_device_flags,
                                 _VN_guid_t guid, uint32_t service_id)
{
    int i, n = 0;
    _vn_proxy_client_t *client;
    _vn_service_t *service;
    _vn_usb_handle_t usb_handle;
    bool interested;
   
    service = _vn_lookup_service(guid, service_id);
    if (service == NULL) {
        return _VN_ERR_NOTFOUND;
    }

    for (i = 0; i < _VNP_CLIENTS_MAX; i++) {
        _vn_proxy_lock();
        client = _vn_proxy_get_client(i);
        interested = _vn_proxy_client_interested(client,
                                                 upnp_device_flags, guid);
        usb_handle = client->usb_handle;
        _vn_proxy_unlock();        

        /* Only tell client about new service if they are interested */
        if (interested) {
            _vn_rpc_call_add_service(usb_handle,
                                     _VN_USB_RPC_SERVICE,
                                     _vn_proxy_rpc_min_timeout,
                                     guid, service);

            n++;
        }
    }
    return n;
}

/* A service has been unregistered, tell clients about it */
int _vn_proxy_service_removed_sync(int upnp_device_flags,
                                   _VN_guid_t guid, uint32_t service_id)
{
    int i, n = 0;
    _vn_proxy_client_t *client;
    _vn_usb_handle_t usb_handle;
    bool interested;

    for (i = 0; i < _VNP_CLIENTS_MAX; i++) {
        _vn_proxy_lock();
        client = _vn_proxy_get_client(i);
        interested = _VNP_CLIENT_VALID(client) && 
            !_vn_guid_eq(&client->guid, &guid);
        usb_handle = client->usb_handle;
        _vn_proxy_unlock();

        if (interested) {
            _vn_rpc_call_remove_service(usb_handle,
                                        _VN_USB_RPC_SERVICE,
                                        _vn_proxy_rpc_min_timeout,
                                        guid, service_id);

            n++;
        }
    }
    return n;
}

/* New services has been discovered, synchronize device table with clients */
/* The given buffer is to be sent to the device as is, it is assumed
   to be formatted correctly */
int _vn_proxy_services_added_sync_raw(int upnp_device_flags,
                                      _VN_guid_t guid,
                                      uint8_t* buf, size_t buflen)
{
    int i, n = 0;
    _vn_proxy_client_t *client;
    _vn_usb_handle_t usb_handle;
    bool interested;
   
    for (i = 0; i < _VNP_CLIENTS_MAX; i++) {
        _vn_proxy_lock();
        client = _vn_proxy_get_client(i);
        interested = _vn_proxy_client_interested(client,
                                                 upnp_device_flags, guid);
        usb_handle = client->usb_handle;
        _vn_proxy_unlock();        

        /* Only tell client about new services if they are interested */
        if (interested) {
            _vn_usb_call_rpc(usb_handle, _VN_USB_RPC_SERVICE,
                             _VN_USB_RPC_COMMON_ADD_SERVICES,
                             buf, buflen, NULL, NULL,
                             _vn_proxy_rpc_min_timeout);

            n++;
        }
    }
    return n;
}

/* Services has been removed, synchronize device table with clients */
/* The given buffer is to be sent to the device as is, it is assumed
   to be formatted correctly */
int _vn_proxy_services_removed_sync_raw(int upnp_device_flags,
                                        _VN_guid_t guid,
                                        uint8_t* buf, size_t buflen)
{
    int i, n = 0;
    _vn_proxy_client_t *client;
    _vn_usb_handle_t usb_handle;
    bool interested;
   
    for (i = 0; i < _VNP_CLIENTS_MAX; i++) {
        _vn_proxy_lock();
        client = _vn_proxy_get_client(i);
        interested = _vn_proxy_client_interested(client,
                                                 upnp_device_flags, guid);
        usb_handle = client->usb_handle;
        _vn_proxy_unlock();        

        /* Only tell client about removed services if they are interested */
        if (interested) {
            _vn_usb_call_rpc(usb_handle, _VN_USB_RPC_SERVICE,
                             _VN_USB_RPC_COMMON_REMOVE_SERVICES,
                             buf, buflen, NULL, NULL,
                             _vn_proxy_rpc_min_timeout);

            n++;
        }
    }
    return n;
}

/* Initializes the client to a known state */
void _vn_proxy_client_init(_vn_proxy_client_t *client)
{
    if (client) {
        client->usb_handle = _VN_USB_HANDLE_INVALID;
        client->state = _VN_STATE_INIT;
        _vn_guid_set_invalid(&client->guid);
        client->flags = 0;
    }
}

/* This function is assumed to be called with the vnp mutex locked */
int _vn_proxy_client_cleanup(_vn_proxy_client_t *client)
{
    assert(client);
    assert(client->state != _VN_STATE_RUNNING);

    if (client->usb_handle != _VN_USB_HANDLE_INVALID) {
        if (!_vn_guid_is_invalid(&client->guid)) {
            /* Clear device table for client */
            _vn_devices_clear_client(client->netif_handle, true);
            
            /* Clear query list for client */
            _vn_netif_clear_query_list(
                _vn_netif_get_instance(client->netif_handle));
        }

        /* Other cleanup */
        _vn_usb_rpc_stop(client->usb_handle);
        _vn_usb_destroy(client->usb_handle);
        _vn_netif_destroy(client->netif_handle);
        _vn_proxy_client_init(client);        
    }

    return _VN_ERR_OK;
}

int _vn_proxy_client_detached(_vn_usb_t *usb)
{
    _vn_proxy_client_t *client;

    assert(usb);
    _VN_TRACE(TRACE_FINE, _VNP_SG_MAIN,
              "USB client %u detached\n", usb->handle);

    /* Stops client dispatcher thread  */

    _vn_proxy_lock();
    client = _vn_proxy_get_client(usb->handle);
    if (client) {
        if (client->state == _VN_STATE_RUNNING) {
            client->state = _VN_STATE_STOPPING;
        }

        _vn_proxy_unlock();
        _vn_thread_join(client->disp_thread, NULL);
        _vn_proxy_lock();

        client->state = _VN_STATE_STOP;
    }
    _vn_proxy_unlock();

    return _VN_ERR_OK;
}

void _vnp_print_client(FILE *fp, int handle)
{
    _vn_proxy_client_t *client;
    assert(fp);
    _vn_net_lock();
    _vn_proxy_lock();
    client = _vn_proxy_get_client(handle);
    if (client) {
        fprintf(fp, "Client %d (USB %d, netif %d), device type 0x%08x, id %u, "
                "state %d\n", 
                handle, client->usb_handle, client->netif_handle, 
                client->guid.device_type, client->guid.chip_id,
                client->state);
        _vn_dump_netif(fp, client->netif_handle);
        _vn_mux_print(fp, handle);
        /* _vn_dump_usb(fp, client->usb_handle); */
    }
    else {
        fprintf(fp, "Unknown client %d\n", handle);
    }
    _vn_proxy_unlock();
    _vn_net_unlock();
}

void _vnp_print_clients(FILE *fp)
{
    /* Show what clients are connected */
    int i, nclients = 0;
    _vn_net_lock();
    _vn_proxy_lock();
    for (i = 0; i < _VNP_CLIENTS_MAX; i++) {
        _vn_proxy_client_t *client = _vn_proxy_get_client(i);
        if (_VNP_CLIENT_VALID(client)) {
            char addr[_VN_INET_ADDRSTRLEN];
            _vn_netif_t *netif;
            _vn_usb_t *usb;

            fprintf(fp, "Client %d (0x%08x %u) <-> ", 
                    i, client->guid.device_type, client->guid.chip_id);
            netif = _vn_netif_get_instance(client->netif_handle);
            if (netif) {
                _vn_inet_ntop(&(netif->localip), addr, sizeof(addr));
                fprintf(fp, "VN %s:%d, ", addr, netif->localport);
            }
            else {
                fprintf(fp, "VN null, ");
            }

            usb = _vn_usb_get(client->usb_handle);

            if (usb) {
                switch (usb->mode) {
#ifdef _VN_USB_SOCKETS                
                case _VN_USB_MODE_SOCKETS:
                    _vn_inet_ntop(&(usb->usb.usocket.device_addr), 
                                  addr, sizeof(addr));
                    fprintf(fp, "USB %s:%d\n", addr, 
                            usb->usb.usocket.device_port);
                    break;
#endif
                default:
                    fprintf(fp, "USB unknown\n");
                }
            }
            else {
                fprintf(fp, "USB null\n");
            }

            nclients++;
        }
    }
    fprintf(fp, "%d of %d clients connected\n", nclients, _VNP_CLIENTS_MAX);
    _vn_proxy_unlock();
    _vn_net_unlock();
}

void _vn_proxy_dispatcher_run(_vn_proxy_client_t* client)
{
    int rv;
    _vn_netif_t* netif;
    _vn_buf_t* pkt;
    _vn_inaddr_t addr;
    _vn_inport_t port, vnport = _VN_INPORT_INVALID;

    _vn_usb_handle_t usb_handle;    
    int              netif_handle;
    _VN_guid_t       guid;

    assert(client);

    _vn_proxy_lock();
    usb_handle = client->usb_handle;
    netif_handle = client->netif_handle;
    _vn_proxy_unlock();
    
    /* Start RPC server */
    _vn_usb_rpc_register_callback(usb_handle,
                                  _VN_USB_ST_CLOSED,
                                  _vn_proxy_client_detached);
    rv = _vn_usb_rpc_start(usb_handle, _vn_proxy_timeout);
    if (rv < 0) {
        goto reject;
    }

    /* Get memory */
    pkt = _vn_get_msg_buffer(_VN_MSG_BUF_SIZE);

    if (pkt == NULL) {
        _VN_TRACE(TRACE_ERROR, _VNP_SG_MAIN,
                  "Unable to start VNProxy Dispatcher %u "
                  "(out of memory}\n", usb_handle);
reject:
        /* Rely on listener loop to clean up client */
        _vn_proxy_lock();
        client->state = _VN_STATE_STOP;
        _vn_proxy_unlock();
        return;
    }

    _VN_TRACE(TRACE_FINE, _VNP_SG_MAIN,
              "Waiting for VN client %u to connect\n", usb_handle);

    _vn_net_lock();
    netif = _vn_netif_get_instance(netif_handle);
    while (netif == NULL || _vn_guid_is_invalid(&netif->guid)) {
        _vn_net_unlock();

        _VN_TRACE(TRACE_FINER, _VNP_SG_MAIN,
                  "Waiting for VN client %u\n", usb_handle);

        _vn_thread_sleep(1000);

        if (client->state != _VN_STATE_RUNNING) {
            _VN_TRACE(TRACE_FINE, _VNP_SG_MAIN,
                      "Abandoning wait for VN client %u\n", usb_handle);
            return;
        }
        
        _vn_net_lock();
        netif = _vn_netif_get_instance(netif_handle);
    }

    if (netif) {
        guid = netif->guid;
    }
    _vn_net_unlock();


    _VN_TRACE(TRACE_FINE, _VNP_SG_MAIN,
              "Starting VNProxy Dispatcher %u\n", 
              usb_handle);

    _vn_proxy_lock();
    
    client->guid = guid;

    _vn_proxy_unlock();

    /* Add device to list, notify subscribed devices */
    _vn_net_lock();
    
    vnport = _vn_netif_getlocalport(netif);

#ifdef UPNP
    /* Uses default IP as client IP */
    _vn_upnp_nat_add_port_mappings(NULL, "UDP", vnport, vnport);
#endif

    _vn_proxy_device_sync(netif);

    _vn_net_unlock();

    /* Enter loop listening for packets */
    _vn_proxy_lock();
    
    /* TODO: Check if client still valid? */    
    while (client->state == _VN_STATE_RUNNING) {
        _vn_proxy_unlock();
        rv = _vn_recv_wait(netif->sockfd, 
                           pkt->buf, pkt->max_len, _vn_proxy_timeout,
                           &addr, &port);
                
        if (rv >= 0) {
            pkt->len = (_VN_msg_len_t) rv;
            /* _vn_dbg_print_pkt(stdout, pkt->buf, pkt->len); */
                    
            /* Got packet - send to dispatcher */
            rv = _vn_proxy_dispatch_pkt(netif, pkt, addr, port);

        }
        else if (rv != _VN_ERR_TIMEOUT) {
            /* TODO: handle error */
            _VN_TRACE(TRACE_WARN, _VNP_SG_MAIN,
                      "Error %d getting packet\n", rv);
        }

        _vn_proxy_lock();
   }
    _vn_proxy_unlock();

    if (pkt) {
        _vn_free_msg_buffer(pkt);
    }

    /* Remove device from list, notify subscribed devices */
    _vn_net_lock();
    _vn_disconnect_device(guid);
#ifdef UPNP
    _vn_upnp_notify_device_removed(guid);
    _vn_upnp_nat_remove_port_mappings("UDP", vnport);
#endif
    _vn_net_unlock();

    _VN_TRACE(TRACE_FINE, _VNP_SG_MAIN,
              "Stopping VNProxy Dispatcher %u\n",
              usb_handle);
}

#ifdef _VN_USB_SOCKETS
/* Emulates USB using TCP sockets, listens for incoming TCP connections */
void _vn_proxy_rpc_tcp_listen(_vn_proxy_config_t *config)
{
    int i;
    _vn_inaddr_t ipaddr;
    _vn_inport_t port;
    _vn_socket_t usb_socket, conn_socket;
    _vn_proxy_client_t *client;
    int nclients = 0; /* Number of connected clients */

    assert(config);

    usb_socket = _vn_socket_tcp(_VN_INADDR_INVALID,
                                config->proxy_usb_port);

    if (usb_socket == _VN_SOCKET_INVALID) {
        _VN_TRACE(TRACE_ERROR, _VNP_SG_MAIN, "Unable to create USB socket\n");
        _vnp_exit = 1;
        return;
    }

    _vn_listen(usb_socket);

    _VN_TRACE(TRACE_FINE, _VNP_SG_MAIN,
              "Listening for simulated USB connections on TCP port %u\n", 
              config->proxy_usb_port);

    /* Initialize USB RPC */
    while (!_vnp_exit) {
        conn_socket = _vn_accept(usb_socket, _vn_proxy_timeout, 
                                 &ipaddr, &port);

        if (conn_socket != _VN_SOCKET_INVALID) {
            char addr[_VN_INET_ADDRSTRLEN];
            _vn_usb_handle_t usb_handle;
            _vn_usb_socket_t usb_config;

            _vn_inet_ntop(&(ipaddr), addr, sizeof(addr));

            if (nclients < _VNP_CLIENTS_MAX) {
                /* Accept connection */
                _VN_TRACE(TRACE_FINE, _VNP_SG_MAIN,
                          "Accept USB connection from %s:%u\n",
                          addr, port);

                usb_config.sockfd = conn_socket;
                usb_config.device_addr = ipaddr;
                usb_config.device_port = port;
                usb_config.proxy_addr = config->proxy_usb_addr;
                usb_config.proxy_port = config->proxy_usb_port;
                usb_handle = _vn_usb_create(_VN_USB_MODE_SOCKETS,
                                            &usb_config);

                if (usb_handle < 0) {
                    goto reject;
                }

                _vn_proxy_lock();
                client = _vn_proxy_get_client(usb_handle);
                
                if (client) {
                    assert(client->usb_handle == _VN_USB_HANDLE_INVALID);
                    client->usb_handle = usb_handle;
                    client->netif_handle = usb_handle;
                    client->state = _VN_STATE_RUNNING;
                    _vn_thread_create(&client->disp_thread, NULL, 
                                      (void*) &_vn_proxy_dispatcher_run, 
                                      client);
                }
                nclients++;
                _vn_proxy_unlock();
            }
            else {
reject:
                /* Reject connection */
                _VN_TRACE(TRACE_FINE, _VNP_SG_MAIN, 
                          "Reject USB connection from %s:%u\n",
                          addr, port);
                _vn_close_socket(conn_socket);
            }

        }

        /* Clean up any clients that went away */
        _vn_net_lock();
        _vn_proxy_lock();
        for (i = 0; i < _VNP_CLIENTS_MAX; i++) {
            client = &_vnp_clients[i];
            if (client->usb_handle != _VN_USB_HANDLE_INVALID &&
                client->state == _VN_STATE_STOP) {
                _vn_proxy_client_cleanup(client);
                nclients--;
            }
        }
        _vn_proxy_unlock();
        _vn_net_unlock();
    }

    /* Clean up all clients */
    _vn_net_lock();
    _vn_proxy_lock();
    for (i = 0; i < _VNP_CLIENTS_MAX; i++) {
        client = &_vnp_clients[i];
        if (client->usb_handle != _VN_USB_HANDLE_INVALID) {

            /* Disable callback */
            _vn_usb_rpc_register_callback(client->usb_handle,
                                          _VN_USB_ST_CLOSED,
                                          NULL);

            if (client->state == _VN_STATE_RUNNING) {
                client->state = _VN_STATE_STOPPING;
            }
            
            _vn_proxy_unlock();
            _vn_net_unlock();

            _vn_thread_join(client->disp_thread, NULL);

            _vn_net_lock();
            _vn_proxy_lock();

            client->state = _VN_STATE_STOP;

            _vn_proxy_client_cleanup(client);
            nclients--;
        }
    }
    _vn_proxy_unlock();
    _vn_net_unlock();            

    _vn_close_socket(usb_socket);
}
#endif

/* Polls for real USB connections */
int _vn_real_usb_connected = 0;

void _vn_proxy_rpc_listen(_vn_proxy_config_t *config)
{
    /* TODO: Change from polling model to event model */
    while (!_vnp_exit) {
        if (_vn_real_usb_connected < (_VN_USB_MAX/2)) {
            _vn_mux_new_usb(config);            
        }
        _vn_thread_sleep(5000);
    }
}

int _vn_proxy_init()
{
    int rv, i;

    rv = _vn_sys_init();
    if (rv < 0) { return rv; }

    rv = _vn_mutex_init(&_vnp_mutex);
    if (rv < 0) {
        _vn_sys_cleanup();
        return rv;
    }

    rv = _vn_net_lock_init();
    if (rv < 0) {
        goto err0;
    }

    /* Init my guid after system initialized */
    _vn_init_myguid();

    /* Make sure timer thread starts before other threads,
     * and before populating net tables
     */
    if ((rv = _vn_timer_start()) != 0) {
        goto err1;
    }

    /* Initialize USB RPC */
    rv = _vn_usb_init_rpc();
    if (rv < 0) { goto err2; }

    /* Register RPC functions */
    rv = _vn_proxy_register_rpcs(_VN_USB_RPC_SERVICE);
    if (rv < 0) { goto err3; }

    /* Initialize table of devices */
    rv = _vn_init_device_table();
    if (rv < 0) { goto err3; }

    /* Initialize VN network interface layer */
    rv = _vn_netif_init();
    if (rv < 0) { goto err4; }

    /* Initialize client table */
    for (i = 0; i < _VNP_CLIENTS_MAX; i++) {
        _vn_proxy_client_init(&_vnp_clients[i]);
    }

    /* Initialize the firewall */
    rv = _vn_firewall_init();
    if (rv < 0) { goto err5; }

    _VN_TRACE(TRACE_FINER, _VNP_SG_MAIN,
              "VNProxy Initialization successful\n");

    return _VN_ERR_OK;

  err5:
    _vn_netif_cleanup();
  err4:
    _vn_destroy_device_table();
  err3:
    _vn_usb_shutdown_rpc();
  err2:
    _vn_timer_stop();
    _vn_timer_cleanup();
  err1:
    _vn_net_lock_destroy();
  err0:
    _vn_mutex_destroy(&_vnp_mutex);
    _vn_sys_cleanup();
    _VN_TRACE(TRACE_ERROR, _VNP_SG_MAIN, 
              "VNProxy Initialization failed: %d\n", rv);
    return rv;
}

int _vn_proxy_shutdown()
{
    _vn_timer_stop(); 
    _vn_firewall_shutdown();
    _vn_netif_cleanup();
    _vn_destroy_device_table();
    _vn_usb_shutdown_rpc();
    _vn_timer_cleanup();
    _vn_mutex_destroy(&_vnp_mutex);
    _vn_net_lock_destroy();
    _vn_sys_cleanup();

    return _VN_ERR_OK;
}

_vn_proxy_config_t _vn_proxy_config;

int main(int argc, char* argv[])
{
    const char* progname;
    char line[MAXLINE];
    _vn_thread_t proxy_rpc_thread;
    _vn_thread_t proxy_tcp_rpc_thread;
    bool interactive = false;
    bool poll_usb;
    int trace_level = TRACE_FINE;
    int rv;

    _vn_proxy_config.device_trace_level = TRACE_DEF;
    _vn_proxy_config.vn_addr = _VN_INADDR_INVALID;
    _vn_proxy_config.vn_port = _VN_CLIENT_PORT;
#ifdef _VN_USB_SOCKETS
    _vn_proxy_config.device_usb_addr = htonl( INADDR_LOOPBACK );
    _vn_proxy_config.device_usb_port = _VN_USB_PORT_DEVICE;
    _vn_proxy_config.proxy_usb_addr = _VN_INADDR_INVALID;
    _vn_proxy_config.proxy_usb_port = _VN_USB_PORT_PROXY;
#endif
    _vn_proxy_config.flags = 0;
    /* Make polling for usb default */
    _vn_proxy_config.flags |= _VNP_CONFIG_FLAG_POLL_USB;

    progname = argv[0];
        
#ifdef _VN_USB_SOCKETS
    while ((rv = getopt(argc, argv, "u:d:l:irp")) != -1) {
#else
    while ((rv = getopt(argc, argv, "d:l:irp")) != -1) {
#endif
        switch (rv) {
        case 'i':
            interactive = true;
            break;
        case 'l':
            trace_level = atoi(optarg);
            break;
        case 'd':
            _vn_proxy_config.device_trace_level = atoi(optarg);
            break;
        case 'r':
            _VN_PRINT("-r is deprecated, USB is enabled by default\n");
            _vn_proxy_config.flags |= _VNP_CONFIG_FLAG_POLL_USB;
            break;
        case 'p':
            _vn_proxy_config.flags |= _VNP_CONFIG_FLAG_HANDLE_PRINT;
            break;
#ifdef _VN_USB_SOCKETS
        case 'u':
            _vn_proxy_config.proxy_usb_port = atoi(optarg);
            break;
#endif
        default:
            usage(progname);
            exit(1);
        }
    }

    _SHR_set_trace_prefix("PRXY: ");
    _vn_set_trace_level(trace_level);

    poll_usb = _vn_proxy_config.flags & _VNP_CONFIG_FLAG_POLL_USB;

    _vn_proxy_init();
    if (poll_usb) {
        _vn_mux_start(&_vn_proxy_config);
    }

#ifdef _VN_USB_SOCKETS
    _vn_thread_create(&proxy_tcp_rpc_thread, NULL, 
                      (void*) &_vn_proxy_rpc_tcp_listen, &_vn_proxy_config);
#else
    _VN_TRACE(TRACE_ERROR, _VNP_SG_MAIN, 
              "USB emulation support through sockets only!!\n");
    _vnp_exit = 1; 
#endif

    if (poll_usb) {
        _vn_thread_create(&proxy_rpc_thread, NULL, 
                          (void*) &_vn_proxy_rpc_listen, &_vn_proxy_config);
    }

    if (interactive) {
        while (!_vnp_exit) {
            _VN_PRINT("> ");
            if (fgets(line, MAXLINE, stdin) != NULL) {
                _vnp_proc_command(line);
            }
            else {
                _vnp_exit = 1;
            }
        }
    }
    else {
        while (!_vnp_exit) {
            _vn_thread_sleep(5000);
        }
    }

    _vn_thread_join(proxy_tcp_rpc_thread, NULL);
    if (poll_usb) {
        _vn_thread_join(proxy_rpc_thread, NULL);
    }

    if (poll_usb) {
        _vn_mux_shutdown();
    }
    _vn_proxy_shutdown();

    return 0;
}


//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#ifndef __VN_PROXY_H__
#define __VN_PROXY_H__

#include "vnp_rpc.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Constants */

#define _VNP_CLIENTS_MAX         _VN_PROXY_MAX_DEVICES

#define _VNP_SG_MAIN     _TRACE_APP, 0
#define _VNP_SG_RPC      _TRACE_APP, 1
#define _VNP_SG_MUX      _TRACE_APP, 2

/* Macros */
#define _VNP_CLIENT_VALID(client)    \
   (client && client->usb_handle != _VN_USB_HANDLE_INVALID)

/* Data Structures */

#define _VNP_CONFIG_FLAG_POLL_USB            1
#define _VNP_CONFIG_FLAG_HANDLE_PRINT        2

typedef struct {
    _vn_inaddr_t vn_addr;
    _vn_inport_t vn_port;
#ifdef _VN_USB_SOCKETS
    _vn_inaddr_t device_usb_addr;
    _vn_inaddr_t proxy_usb_addr;
    _vn_inport_t device_usb_port;
    _vn_inport_t proxy_usb_port;
#endif
    uint32_t     flags;
    int          device_trace_level;
} _vn_proxy_config_t;

extern _vn_proxy_config_t _vn_proxy_config;

#define _VNP_CLIENT_FLAG_ADVERTISE        0x01
#define _VNP_CLIENT_FLAG_DISCOVER         0x02

#define _VNP_CLIENT_ADVERTISE(client)             \
    (client->flags & _VNP_CLIENT_FLAG_ADVERTISE)

#define _VNP_CLIENT_DISCOVER(client)              \
    (client->flags & _VNP_CLIENT_FLAG_DISCOVER)

typedef struct {
    _VN_guid_t       guid;

    _vn_usb_handle_t usb_handle;    
    _vn_thread_t     disp_thread;   
    int              netif_handle;

    uint8_t          state;
    uint8_t          flags;
} _vn_proxy_client_t;

/* Variables */

extern _vn_usb_handle_t _vn_proxy_usb_handle;
extern int _vnp_exit;

/* Functions Prototypes */
int _vnp_proc_command(char *cmdline);

void _vnp_print_client(FILE *fp, int handle);
void _vnp_print_clients(FILE *fp);

#ifdef  __cplusplus
}
#endif

#endif /* __VN_PROXY_H__ */

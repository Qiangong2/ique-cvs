//
//               Copyright (C) 2006, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#ifndef __VN_MUX_H__
#define __VN_MUX_H__

#include "vnproxy.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef uint16_t _vn_mux_proto_t;

typedef struct {
    _vn_usb_handle_t   usb_handle;
    _vn_mux_proto_t    mux_proto;
} _vn_mux_header_t;

#define _VN_MUX_FLAG_SYNC_WRITE            0x01
#define _VN_MUX_FLAG_FRAMING               0x02

typedef struct {
    _vn_usb_handle_t usb_handle;
    _vn_mux_proto_t  mux_proto;
    _vn_socket_t listen_sockfd;
    _vn_inport_t listen_port;
    _vn_socket_t sockfd;
    _vn_socket_t connfd;
    _vn_inaddr_t ipaddr;
    _vn_inport_t port;

    /* Number of write buffers available on the NC, 
       cannot do write when this goes to 0 */
    int          flags;
    int          wcount;
} _vn_mux_client_t;

typedef struct {
    _vn_usb_handle_t usb_handle;
    _vn_thread_t send_thread;
    _vn_thread_t recv_thread;
    uint8_t      state;
    uint8_t      nclients;
    _vn_cond_t client_attached;
} _vn_mux_usb_t;

void _vn_mux_new_usb(_vn_proxy_config_t* config);
int _vn_mux_start(_vn_proxy_config_t* config);
int _vn_mux_shutdown();

int _vn_mux_print(FILE* fp, _vn_usb_handle_t handle);

void _vn_usb_mux_send_dispatcher(_vn_mux_usb_t* mux_usb);
void _vn_usb_mux_recv_dispatcher(_vn_mux_usb_t* mux_usb);

#ifdef  __cplusplus
}
#endif

#endif /* __VN_MUX_H__ */

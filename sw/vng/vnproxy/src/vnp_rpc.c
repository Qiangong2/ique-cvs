//
//               Copyright (C) 2005, BroadOn Communications Corp.
//
//  These coded instructions, statements, and computer programs contain
//  unpublished  proprietary information of BroadOn Communications Corp.,
//  and  are protected by Federal copyright law. They may not be disclosed
//  to  third  parties or copied or duplicated in any form, in whole or in
//  part, without the prior written consent of BroadOn Communications Corp.
//
//

#include "vnp_rpc.h"
#include "vnproxy.h"

/* Stubs for RPC function calls to the SC */

/* TODO: Figure out which device it is for and dispatch it */
/* For now assume, there is only one device */ 

uint32_t _vn_proxy_rpc_min_timeout = _VN_USB_RPC_MIN_TIMEOUT;
uint32_t _vn_proxy_rpc_max_timeout = _VN_USB_RPC_MAX_TIMEOUT;

/* DO RPC call to get the device type and device id */
int _vn_proxy_get_device_info(_vn_netif_t *netif, 
                              _VN_guid_t *guid)
{
    _vn_usb_rpc_device_info_t device_info;
    _vn_usb_handle_t usb_handle;
    _vn_usb_rpc_service_t service = _VN_USB_RPC_SERVICE;
    size_t retlen = sizeof(device_info);
    int rv;
 
    assert(netif);

    usb_handle = netif->handle;
   
    rv = _vn_usb_call_rpc(usb_handle, service,
                          _VN_USB_RPC_DEVICE_GET_INFO,
                          NULL, 0, &device_info, &retlen,
                          _vn_proxy_rpc_max_timeout);

    if (rv >= 0) {
        assert(retlen >= sizeof(device_info));
        if (guid) {
            guid->device_type = ntohl(device_info.guid.device_type);
            guid->chip_id = ntohl(device_info.guid.chip_id);
        }
    }
        
    return rv;
}

/* Sends received packet to dispatcher for processing */
int _vn_proxy_dispatch_pkt(_vn_netif_t* netif, _vn_buf_t* pkt,
                           _vn_inaddr_t addr, _vn_inport_t port)
{
    int rv;
    _vn_usb_rpc_udp_pkt_t *rpc_pkt;
    _vn_usb_handle_t usb_handle;
    _vn_usb_rpc_service_t service = _VN_USB_RPC_SERVICE;

    size_t rpc_pkt_len = sizeof(_vn_usb_rpc_udp_pkt_t) + pkt->len;

    assert(netif);
    usb_handle = netif->handle;

    assert(pkt);    rpc_pkt = _vn_malloc(rpc_pkt_len);
    if (rpc_pkt == NULL) {
        return _VN_ERR_NOMEM;
    }
    
    rpc_pkt->ipaddr = addr;
    rpc_pkt->port = htons(port);
    rpc_pkt->msglen = htons(pkt->len);
    memcpy(rpc_pkt->msg, pkt->buf, pkt->len);

    rv = _vn_usb_call_rpc(usb_handle, service,
                          _VN_USB_RPC_DEVICE_PROC_UDP_PKT,
                          rpc_pkt, rpc_pkt_len,
                          NULL, NULL, 0/*_vn_proxy_rpc_min_timeout*/);

    _vn_free(rpc_pkt);

    return rv;
}

/* Have Queue manager enqueue a keep alive packet */
int _vn_proxy_enqueue_keep_alive(_vn_netif_t* netif, _VN_addr_t vnaddr)
{
    int rv;
    _vn_usb_handle_t usb_handle;
    _vn_usb_rpc_service_t service = _VN_USB_RPC_SERVICE;

    assert(netif);
    usb_handle = netif->handle;

    rv = _vn_usb_call_rpc(usb_handle, service, 
                          _VN_USB_RPC_DEVICE_SEND_KEEP_ALIVE,
                          &vnaddr, sizeof(vnaddr),
                          NULL, NULL, _vn_proxy_rpc_min_timeout);
    return rv;
}

/* Have device synchronize data structures */
int _vn_proxy_device_sync(_vn_netif_t* netif)
{
    int rv;
    _vn_usb_handle_t usb_handle;
    _vn_usb_rpc_service_t service = _VN_USB_RPC_SERVICE;

    assert(netif);
    usb_handle = netif->handle;

    rv = _vn_usb_call_rpc(usb_handle, service, 
                          _VN_USB_RPC_DEVICE_SYNC,
                          NULL, 0, NULL, NULL, _vn_proxy_rpc_min_timeout);
    return rv;    
}
 
/* Have device set trace level */
int _vn_proxy_device_set_trace_level(_vn_netif_t* netif,
                                     int group, int subgroup, int level)
{
    int rv;
    _vn_usb_rpc_set_trace_req_t req_arg;
    _vn_usb_handle_t usb_handle;
    _vn_usb_rpc_service_t service = _VN_USB_RPC_SERVICE;

    assert(netif);
    usb_handle = netif->handle;

    req_arg.group = htonl(group);
    req_arg.subgroup = htonl(subgroup);
    req_arg.level = htonl(level);

    rv = _vn_usb_call_rpc(usb_handle, service, 
                          _VN_USB_RPC_DEVICE_SET_TRACE,
                          &req_arg, sizeof(req_arg),
                          NULL, NULL, _vn_proxy_rpc_min_timeout);

    return rv;
}
 
/* Registered RPC functions on the VN Proxy */

int _vn_proxy_register_rpcs(_vn_usb_rpc_service_t service)
{
    _vn_register_common_rpcs(service);

    /* Add RPC functions for managing VN net connection structures */
    _vn_usb_register_rpc(service, _VN_USB_RPC_PROXY_ADD_NET, 0,
                         _vn_proxy_rpc_add_net);
    _vn_usb_register_rpc(service, _VN_USB_RPC_PROXY_DEL_NET, 0,
                         _vn_proxy_rpc_del_net);
    _vn_usb_register_rpc(service, _VN_USB_RPC_PROXY_ADD_HOST, 0,
                         _vn_proxy_rpc_add_host);
    _vn_usb_register_rpc(service, _VN_USB_RPC_PROXY_DEL_HOST, 0,
                         _vn_proxy_rpc_del_host);
    _vn_usb_register_rpc(service, _VN_USB_RPC_PROXY_SET_HOST, 0,
                         _vn_proxy_rpc_set_host);
    _vn_usb_register_rpc(service, _VN_USB_RPC_PROXY_GET_HOST, 0,
                         _vn_proxy_rpc_get_host);

    /* Add RPC functions for sending packets */
    _vn_usb_register_rpc(service, _VN_USB_RPC_PROXY_SEND_VN_PKT, 0,
                         _vn_proxy_rpc_send_vn_pkt);
    _vn_usb_register_rpc(service, _VN_USB_RPC_PROXY_SEND_UDP_PKT, 0,
                         _vn_proxy_rpc_send_udp_pkt);

    /* Add RPC functions for determining IP/Port information */
    _vn_usb_register_rpc(service, _VN_USB_RPC_PROXY_NETIF_GETADDR, 0,
                         _vn_proxy_rpc_netif_getaddr);
    _vn_usb_register_rpc(service, _VN_USB_RPC_PROXY_NETIF_GETLOCALHOSTNAME, 0,
                         _vn_proxy_rpc_netif_getlocalhostname);
    _vn_usb_register_rpc(service, _VN_USB_RPC_PROXY_NETIF_GETLOCALPORT, 0,
                         _vn_proxy_rpc_netif_getlocalport);
    _vn_usb_register_rpc(service, _VN_USB_RPC_PROXY_NETIF_GETLOCALIP, 0,
                         _vn_proxy_rpc_netif_getlocalip);
    _vn_usb_register_rpc(service, _VN_USB_RPC_PROXY_NETIF_GETLOCALIPS, 0,
                         _vn_proxy_rpc_netif_getlocalips);

    /* Add RPC functions for VN status */
    _vn_usb_register_rpc(service, _VN_USB_RPC_PROXY_NETIF_GETSTATUS, 0,
                         _vn_proxy_rpc_netif_getstatus);

    /* Add RPC functions for managing the firewall */
    _vn_usb_register_rpc(service, _VN_USB_RPC_PROXY_FIREWALL_OPEN, 0,
                         _vn_proxy_rpc_firewall_open);

    /* Add RPC functions for resetting (clearing) the netif table */
    _vn_usb_register_rpc(service, _VN_USB_RPC_PROXY_NETIF_RESET, 0,
                         _vn_proxy_rpc_netif_reset);

    /* Add RPC functions for initializing the netif */
    _vn_usb_register_rpc(service, _VN_USB_RPC_PROXY_NETIF_INIT, 0,
                         _vn_proxy_rpc_netif_init);

    /* Add RPC functions for host discovery */
    _vn_usb_register_rpc(service, _VN_USB_RPC_PROXY_QUERY_HOST, 0,
                         _vn_proxy_rpc_query_host);

    _vn_usb_register_rpc(service, _VN_USB_RPC_PROXY_CLEAR_QUERY_LIST, 0,
                         _vn_proxy_rpc_clear_query_list);

    _vn_usb_register_rpc(service, _VN_USB_RPC_PROXY_DISCOVER_HOSTS, 0,
                         _vn_proxy_rpc_discover_hosts);

    return _VN_ERR_OK;
}

/**
 * RPC to send a packet to a VN address
 */
int _vn_proxy_rpc_send_vn_pkt(_vn_usb_t *usb,
                              const void *args, size_t argslen,
                              void *ret, size_t *retlen)
{
    _vn_netif_t *netif;
    _vn_usb_rpc_vn_pkt_t *rpc_pkt;
    uint16_t msglen, len;
    _VN_addr_t vnaddr;
    int rv;

    assert(usb);

    if (retlen) { *retlen = 0; }

    if (!args || (argslen < sizeof(_vn_usb_rpc_vn_pkt_t))) {
        return _VN_ERR_INVALID;
    }

    rpc_pkt = (_vn_usb_rpc_vn_pkt_t*) args;
    vnaddr = ntohl(rpc_pkt->vnaddr);
    msglen = ntohs(rpc_pkt->msglen);
    
    len = (uint16_t) argslen - sizeof(_vn_usb_rpc_vn_pkt_t);
    if (len < msglen) {
        return _VN_ERR_TOO_SHORT;
    }

    _vn_net_lock();
    netif = _vn_netif_get_instance(usb->handle);
    if (netif) {
        rv = _vn_netif_send_vn_pkt(netif,
                                   _VN_addr2net(vnaddr),
                                   _VN_addr2host(vnaddr),
                                   rpc_pkt->msg, msglen);
    } else {
        rv = _VN_ERR_PROXY;
    }
    _vn_net_unlock();

    return rv;    
}

/**
 * RPC to send a packet to a IP/UDP Port
 */
int _vn_proxy_rpc_send_udp_pkt(_vn_usb_t *usb,
                               const void *args, size_t argslen,
                               void *ret, size_t *retlen)
{
    _vn_netif_t *netif;
    _vn_usb_rpc_udp_pkt_t *rpc_pkt;
    uint16_t msglen, len;
    _vn_inaddr_t ipaddr;
    _vn_inport_t port;
    int rv;

    assert(usb);

    if (retlen) { *retlen = 0; }

    if (!args || (argslen < sizeof(_vn_usb_rpc_udp_pkt_t))) {
        return _VN_ERR_INVALID;
    }

    rpc_pkt = (_vn_usb_rpc_udp_pkt_t*) args;
    ipaddr = rpc_pkt->ipaddr;
    port = ntohs(rpc_pkt->port);
    msglen = ntohs(rpc_pkt->msglen);
    
    len = (uint16_t) argslen - sizeof(_vn_usb_rpc_udp_pkt_t);
    if (len < msglen) {
        return _VN_ERR_TOO_SHORT;
    }

    _vn_net_lock();
    netif = _vn_netif_get_instance(usb->handle);    
    if (netif) {
        rv = _vn_netif_send_udp_pkt(netif, ipaddr, port, rpc_pkt->msg, msglen);
    } else {
        rv = _VN_ERR_PROXY;
    }
    _vn_net_unlock();

    return rv;
}

/**
 * RPC to add a new net
 */
int _vn_proxy_rpc_add_net(_vn_usb_t *usb,
                          const void *args, size_t argslen,
                          void *ret, size_t *retlen)
{
    _vn_netif_t *netif;
    _vn_usb_rpc_add_net_arg_t *add_net_arg;
    _VN_net_t net_id;
    int rv;

    assert(usb);

    if (retlen) { *retlen = 0; }

    if (!args || (argslen < sizeof(_vn_usb_rpc_add_net_arg_t))) {
        return _VN_ERR_INVALID;
    }

    add_net_arg = (_vn_usb_rpc_add_net_arg_t*) args;
    net_id = ntohl(add_net_arg->net_id);

    _vn_net_lock();
    netif = _vn_netif_get_instance(usb->handle);
    if (netif) {
        rv = _vn_netif_add_net(netif, net_id);
    } else {
        rv = _VN_ERR_PROXY;
    }
    _vn_net_unlock();

    return rv;
}

/**
 * RPC to delete a net
 */
int _vn_proxy_rpc_del_net(_vn_usb_t *usb,
                          const void *args, size_t argslen,
                          void *ret, size_t *retlen)
{
    _vn_netif_t *netif;
    _vn_usb_rpc_del_net_arg_t *del_net_arg;
    _VN_net_t net_id;

    int rv;

    assert(usb);

    if (retlen) { *retlen = 0; }

    if (!args || (argslen < sizeof(_vn_usb_rpc_del_net_arg_t))) {
        return _VN_ERR_INVALID;
    }

    del_net_arg = (_vn_usb_rpc_del_net_arg_t*) args;
    net_id = ntohl(del_net_arg->net);

    _vn_net_lock();
    netif = _vn_netif_get_instance(usb->handle);
    if (netif) {
        rv = _vn_netif_delete_net(netif, net_id);
    } else {
        rv = _VN_ERR_PROXY;
    }
    _vn_net_unlock();

    return rv;
}

/**
 * RPC to add a host
 */
int _vn_proxy_rpc_add_host(_vn_usb_t *usb,
                           const void *args, size_t argslen,
                           void *ret, size_t *retlen)
{
    _vn_netif_t *netif;
    _vn_usb_rpc_add_host_arg_t *add_host_arg;
    _VN_addr_t vnaddr;
    _VN_guid_t guid;
    _vn_inaddr_t ipaddr;
    _vn_inport_t port;
    bool keep_alive;
    int rv;

    assert(usb);

    if (retlen) { *retlen = 0; }

    if (!args || (argslen < sizeof(_vn_usb_rpc_add_host_arg_t))) {
        return _VN_ERR_INVALID;
    }

    add_host_arg = (_vn_usb_rpc_add_host_arg_t*) args;
    vnaddr = ntohl(add_host_arg->vnaddr);
    guid.device_type = ntohl(add_host_arg->guid.device_type);
    guid.chip_id = ntohl(add_host_arg->guid.chip_id);
    ipaddr = add_host_arg->ipaddr;
    port = ntohs(add_host_arg->port);
    keep_alive = add_host_arg->keep_alive;

    _vn_net_lock();
    netif = _vn_netif_get_instance(usb->handle);
    if (netif) {
        rv = _vn_netif_add_host(netif, _VN_addr2net(vnaddr),
                                _VN_addr2host(vnaddr), guid,
                                ipaddr, port, keep_alive);
    } else {
        rv = _VN_ERR_PROXY;
    }
    _vn_net_unlock();

    return rv;
}

/**
 * RPC to delete a host
 */
int _vn_proxy_rpc_del_host(_vn_usb_t *usb,
                           const void *args, size_t argslen,
                           void *ret, size_t *retlen)
{
    _vn_netif_t *netif;
    _vn_usb_rpc_del_host_arg_t *del_host_arg;
    _VN_addr_t vnaddr;
    int rv;

    assert(usb);

    if (retlen) { *retlen = 0; }

    if (!args || (argslen < sizeof(_vn_usb_rpc_del_host_arg_t))) {
        return _VN_ERR_INVALID;
    }

    del_host_arg = (_vn_usb_rpc_del_host_arg_t*) args;
    vnaddr = ntohl(del_host_arg->vnaddr);

    _vn_net_lock();
    netif = _vn_netif_get_instance(usb->handle);
    if (netif) {
        rv = _vn_netif_delete_host(netif, _VN_addr2net(vnaddr),
                                   _VN_addr2host(vnaddr));
    } else {
        rv = _VN_ERR_PROXY;
    }
    _vn_net_unlock();

    return rv;
}

/**
 * RPC to set host mapping
 */
int _vn_proxy_rpc_set_host(_vn_usb_t *usb,
                           const void *args, size_t argslen,
                           void *ret, size_t *retlen)
{
    _vn_netif_t *netif;
    _vn_usb_rpc_set_host_arg_t *set_host_arg;
    _VN_addr_t vnaddr;
    _vn_inaddr_t ipaddr;
    _vn_inport_t port;
    int rv;

    assert(usb);

    if (retlen) { *retlen = 0; }

    if (!args || (argslen < sizeof(_vn_usb_rpc_set_host_arg_t))) {
        return _VN_ERR_INVALID;
    }

    set_host_arg = (_vn_usb_rpc_set_host_arg_t*) args;
    vnaddr = ntohl(set_host_arg->vnaddr);
    ipaddr = set_host_arg->ipaddr;
    port = ntohs(set_host_arg->port);

    _vn_net_lock();
    netif = _vn_netif_get_instance(usb->handle);
    if (netif) {
        rv = _vn_netif_set_host_mapping(netif, _VN_addr2net(vnaddr),
                                        _VN_addr2host(vnaddr),
                                        ipaddr, port);
    } else {
        rv = _VN_ERR_PROXY;
    }
    _vn_net_unlock();

    return rv;
}

/**
 * RPC to get host mapping
 */
int _vn_proxy_rpc_get_host(_vn_usb_t *usb,
                           const void *args, size_t argslen,
                           void *ret, size_t *retlen)
{
    _vn_netif_t *netif;
    _VN_addr_t vnaddr;
    _vn_inaddr_t ipaddr;
    _vn_inport_t port;
    int rv;

    assert(usb);

    if ((ret == NULL) || (retlen == NULL) || 
        (*retlen < sizeof(_vn_usb_rpc_ipport_t))) {
        if (retlen) { *retlen = 0; }
        return _VN_ERR_INVALID;
    }

    if (!args || (argslen < sizeof(_VN_addr_t))) {
        *retlen = 0;
        return _VN_ERR_INVALID;
    }


    vnaddr = ntohl(*((_VN_addr_t*) args));

    _vn_net_lock();
    netif = _vn_netif_get_instance(usb->handle);
    if (netif) {
        rv = _vn_netif_get_host_mapping(netif, _VN_addr2net(vnaddr),
                                        _VN_addr2host(vnaddr),
                                        &ipaddr, &port);
    } else {
        rv = _VN_ERR_PROXY;
    }

    _vn_net_unlock();

    if (rv >= 0) {
        _vn_usb_rpc_ipport_t *ipport = (_vn_usb_rpc_ipport_t*) ret;
        *retlen = sizeof(_vn_usb_rpc_ipport_t);
        ipport->ipaddr = ipaddr;
        ipport->port = htons(port);
        memset(ipport->reserved, 0, sizeof(ipport->reserved));
    }
    else {
        *retlen = 0;
    }

    return rv;
}

/**
 * RPC to get the IP address from hostname
 */
int _vn_proxy_rpc_netif_getaddr(_vn_usb_t *usb,
                                const void *args, size_t argslen,
                                void *ret, size_t *retlen)
{
    _vn_inaddr_t *pAddr;
    char* hostname;

    assert(usb);

    if ((ret == NULL) || (retlen == NULL) || (*retlen < sizeof(_vn_inaddr_t))) {
        if (retlen) { *retlen = 0; }
        return _VN_ERR_INVALID;
    }

    pAddr = (_vn_inaddr_t*) ret;
    *retlen = sizeof(_vn_inaddr_t);

    if (args && argslen > 0) {
        /* make sure string is null terminated */
        hostname = (char*) args;
        hostname[argslen-1] = '\0';
        *pAddr = _vn_netif_getaddr(hostname);
        return _VN_ERR_OK;
    }
    else {
        *pAddr = _VN_INADDR_INVALID;
        return _VN_ERR_INVALID;
    }
}

/**
 * RPC to get the hostname of the local machine
 */
int _vn_proxy_rpc_netif_getlocalhostname(_vn_usb_t *usb,
                                         const void *args, size_t argslen,
                                         void *ret, size_t *retlen)
{
    assert(usb);

    if (retlen == NULL) {
        return _VN_ERR_INVALID;
    }
    else if (ret == NULL) {
        *retlen = 0;
        return _VN_ERR_INVALID;
    }
    else {
        int rv = _vn_getlocalhostname(ret, *retlen);
        if (rv >= 0) {
            *retlen = strlen(ret) + 1; /* Include null term char */
        }
        else {
            *retlen = 0;
        }
        return rv;
    }
}

/**
 * RPC to get the local UDP port used for VN
 */
int _vn_proxy_rpc_netif_getlocalport(_vn_usb_t *usb,
                                     const void *args, size_t argslen,
                                     void *ret, size_t *retlen)
{
    _vn_netif_t *netif; 
    _vn_inport_t *pPort;
    int rv;

    assert(usb);

    if ((ret == NULL) || (retlen == NULL) || (*retlen < sizeof(_vn_inport_t))) {
        if (retlen) { *retlen = 0; }
        return _VN_ERR_INVALID;
    }

    pPort = (_vn_inport_t*) ret;
    *retlen = sizeof(_vn_inport_t);

    _vn_net_lock();
    netif = _vn_netif_get_instance(usb->handle);
    if (netif) {
        *pPort = htons(_vn_netif_getlocalport(netif));
        rv = _VN_ERR_OK;
    } else {
        rv = _VN_ERR_PROXY;
    }
    _vn_net_unlock();

    return rv;
}

/**
 * RPC to get the local IP address used for VN
 */
int _vn_proxy_rpc_netif_getlocalip(_vn_usb_t *usb,
                                   const void *args, size_t argslen,
                                   void *ret, size_t *retlen)
{
    _vn_netif_t *netif;
    _vn_inaddr_t *pAddr;
    int rv;

    assert(usb);

    if ((ret == NULL) || (retlen == NULL) || (*retlen < sizeof(_vn_inaddr_t))) {
        if (retlen) { *retlen = 0; }
        return _VN_ERR_INVALID;
    }

    pAddr = (_vn_inaddr_t*) ret;
    *retlen = sizeof(_vn_inaddr_t);

    _vn_net_lock();
    netif = _vn_netif_get_instance(usb->handle);
    if (netif) {
        *pAddr = _vn_netif_getlocalip(netif);
        rv = _VN_ERR_OK;
    } else {
        rv = _VN_ERR_PROXY;
    }
    _vn_net_unlock();

    return rv;
}


/**
 * RPC to get the list of all local IP address
 */
int _vn_proxy_rpc_netif_getlocalips(_vn_usb_t *usb,
                                    const void *args, size_t argslen,
                                    void *ret, size_t *retlen)
{
    _vn_netif_t *netif;
    _vn_inaddr_t *ipaddrs = NULL;
    size_t len;
    int rv;

    assert(usb);

    if ((ret == NULL) || (retlen == NULL)) {
        if (retlen) { *retlen = 0; }
        return _VN_ERR_INVALID;
    }

    _vn_net_lock();
    netif = _vn_netif_get_instance(usb->handle);
    if (netif) {
        rv = _vn_netif_getlocalips(netif, &ipaddrs);
    } else {
        rv = _VN_ERR_PROXY;
    }
    _vn_net_unlock();

    if (rv > 0) {
        len = rv*sizeof(_vn_inaddr_t);
        if (*retlen >= len) {
            memcpy(ret, ipaddrs, len);
            *retlen = len;
        }
        else {
            *retlen = 0;
            rv = _VN_ERR_TOO_SHORT;
        }
    }
    else {
        *retlen = 0;
    }

    if (ipaddrs) {
        _vn_free(ipaddrs);
    }

    return rv;
}

/**
 * RPC to get the VN status
 */
int _vn_proxy_rpc_netif_getstatus(_vn_usb_t *usb,
                                  const void *args, size_t argslen,
                                  void *ret, size_t *retlen)
{
    _vn_netif_t *netif;
    uint32_t *pStatus;
    int rv;

    assert(usb);

    if ((ret == NULL) || (retlen == NULL) || (*retlen < sizeof(uint32_t))) {
        if (retlen) { *retlen = 0; }
        return _VN_ERR_INVALID;
    }

    pStatus = (uint32_t*) ret;
    *retlen = sizeof(uint32_t);

    _vn_net_lock();
    netif = _vn_netif_get_instance(usb->handle);
    if (netif) {
        *pStatus = _vn_netif_get_status(netif);
        rv = _VN_ERR_OK;
    } else {
        rv = _VN_ERR_PROXY;
    }
    _vn_net_unlock();

    return rv;
}

/**
 * RPC to open the firewall to the specified IP/port
 */
int _vn_proxy_rpc_firewall_open(_vn_usb_t *usb,
                                const void *args, size_t argslen,
                                void *ret, size_t *retlen)
{
    _vn_netif_t *netif;
    _vn_usb_rpc_ipport_t *ipport;
    _vn_inaddr_t ipaddr;
    _vn_inport_t port;
    int rv;

    assert(usb);

    if (retlen) { *retlen = 0; }

    if (!args || (argslen < sizeof(_vn_usb_rpc_ipport_t))) {
        return _VN_ERR_INVALID;
    }

    ipport = (_vn_usb_rpc_ipport_t*) args;
    ipaddr = ipport->ipaddr;
    port = ntohs(ipport->port);

    _vn_net_lock();
    netif = _vn_netif_get_instance(usb->handle);
    if (netif) {
        _vn_firewall_open(netif, ipaddr, port);
        rv = _VN_ERR_OK;
    } else {
        rv = _VN_ERR_PROXY;
    }
    _vn_net_unlock();

    return rv;
}

/**
 * RPC to reset (clear) the netif tables
 */
int _vn_proxy_rpc_netif_reset(_vn_usb_t *usb,
                              const void *args, size_t argslen,
                              void *ret, size_t *retlen)
{
    _vn_netif_t *netif;
    int rv;

    assert(usb);

    if (retlen) (*retlen = 0);

    _vn_net_lock();
    netif = _vn_netif_get_instance(usb->handle);
    if (netif) {
        _vn_netif_clear_instance(netif);
        rv = _VN_ERR_OK;
    } else {
        rv = _VN_ERR_PROXY;
    }
    _vn_net_unlock();

    return rv;
}

/**
 * RPC to initialize the netif tables (need to be done before netif can be used)
 */
int _vn_proxy_rpc_netif_init(_vn_usb_t *usb,
                             const void *args, size_t argslen,
                             void *ret, size_t *retlen)
{
    int rv;
    _vn_netif_t *netif; 
    _vn_usb_rpc_netif_init_req_arg_t *req_arg;
    _vn_usb_rpc_netif_init_resp_arg_t *resp_arg;
    _vn_inport_t min_port, max_port;
    _VN_chip_id_t chip_id;
    _VN_device_type_t device_type;

    assert(usb);

    if ((ret == NULL) || (retlen == NULL) || 
        (*retlen < sizeof(_vn_usb_rpc_netif_init_resp_arg_t))) {
        if (retlen) { *retlen = 0; }
        return _VN_ERR_INVALID;
    }

    *retlen = 0;

    if (!args || (argslen < sizeof(_vn_usb_rpc_netif_init_req_arg_t))) {
        return _VN_ERR_INVALID;
    }

    _vn_net_lock();
    netif = _vn_netif_get_instance(usb->handle);
    _vn_net_unlock();

    req_arg = (_vn_usb_rpc_netif_init_req_arg_t*) args;
    resp_arg = (_vn_usb_rpc_netif_init_resp_arg_t*) ret;
    resp_arg->trace_level = htonl(_vn_proxy_config.device_trace_level);

    *retlen = sizeof(_vn_usb_rpc_netif_init_resp_arg_t);

    chip_id = ntohl(req_arg->device_info.guid.chip_id);
    device_type = ntohl(req_arg->device_info.guid.device_type);

    if (netif != NULL) {
        if (netif->guid.chip_id == chip_id && 
            netif->guid.device_type == device_type) {
            resp_arg->ipport.ipaddr = _vn_netif_getlocalip(netif);
            resp_arg->ipport.port = _vn_netif_getlocalport(netif);
            resp_arg->ipport.port = htons(resp_arg->ipport.port);
            return _VN_ERR_OK;
        } else {
            resp_arg->ipport.ipaddr = _VN_INADDR_INVALID;
            resp_arg->ipport.port = htons(_VN_INPORT_INVALID);
            return _VN_ERR_DUPENTRY;
        }
    }

    min_port = ntohs(req_arg->ipport.min_port);
    max_port = ntohs(req_arg->ipport.max_port);
        
    rv = _vn_netif_create_at(usb->handle, 
                             req_arg->ipport.ipaddr,
                             min_port,
                             max_port);

    _vn_net_lock();
    netif = _vn_netif_get_instance(usb->handle);
    if (netif) {
        netif->guid.chip_id = chip_id;
        netif->guid.device_type = device_type;
        resp_arg->ipport.ipaddr = _vn_netif_getlocalip(netif);
        resp_arg->ipport.port = _vn_netif_getlocalport(netif);
        resp_arg->ipport.port = htons(resp_arg->ipport.port);
    } else {
        resp_arg->ipport.ipaddr = _VN_INADDR_INVALID;
        resp_arg->ipport.port = htons(_VN_INPORT_INVALID);
    }

    _vn_net_unlock();

    return rv;
}

/**
 * RPC to query host
 */
int _vn_proxy_rpc_query_host(_vn_usb_t *usb,
                             const void *args, size_t argslen,
                             void *ret, size_t *retlen)
{
    _vn_usb_rpc_query_host_arg_t *query_host_arg;
    int rv;
    uint16_t len;
    _vn_netif_t* netif;
    
    assert(usb);

    if (retlen) (*retlen = 0);

    if (!args || (argslen < sizeof(_vn_usb_rpc_query_host_arg_t))) {
        return _VN_ERR_INVALID;
    }

    query_host_arg = (_vn_usb_rpc_query_host_arg_t*) args;
    len = ntohs(query_host_arg->hostname_len);
    if (argslen < sizeof(_vn_usb_rpc_query_host_arg_t) + len) {
        return _VN_ERR_INVALID;
    }

    /* Make sure hostname null terminated */
    query_host_arg->hostname[len-1] = '\0';

    _vn_net_lock();
    netif = _vn_netif_get_instance(usb->handle);
    if (netif) {
        rv = _vn_netif_query_host(netif,
                                  query_host_arg->hostname,
                                  ntohs(query_host_arg->port));
    } else {
        rv = _VN_ERR_PROXY;
    }
    _vn_net_unlock();

    return rv;
}

/**
 * RPC to clear query list
 */
int _vn_proxy_rpc_clear_query_list(_vn_usb_t *usb,
                                   const void *args, size_t argslen,
                                   void *ret, size_t *retlen)
{
    int rv;
    _vn_netif_t* netif;
    
    assert(usb);

    if (retlen) (*retlen = 0);

    _vn_net_lock();
    /* Clear query list for this client */
    netif = _vn_netif_get_instance(usb->handle);
    if (netif) {
        rv = _vn_netif_clear_query_list(netif);
    } else {
        rv = _VN_ERR_PROXY;
    }
    _vn_net_unlock();

    return rv;
}

/**
 * RPC to start/stop host discovery
 */
int _vn_proxy_rpc_discover_hosts(_vn_usb_t *usb,
                                 const void *args, size_t argslen,
                                 void *ret, size_t *retlen)
{
    _vn_usb_rpc_discover_hosts_arg_t *disc_hosts_arg;
    int rv;
    
    assert(usb);

    if (retlen) (*retlen = 0);

    if (!args || (argslen < sizeof(_vn_usb_rpc_discover_hosts_arg_t))) {
        return _VN_ERR_INVALID;
    }

    disc_hosts_arg = (_vn_usb_rpc_discover_hosts_arg_t*) args;

    rv = _vn_proxy_discover_hosts(usb->handle, disc_hosts_arg->flag);

    return rv;
}


#!/bin/sh

# pkinst - 
#   the tool to install a broadon package

trap cleanup INT


userdo()
{
    su -l $userid -c "$*"
}


cleanup()
{
    if [ -e "$tmpdir" ]; then
	rm -fr $tmpdir
    fi
}


fatal()
{
    echo "$progname: $*"
    cleanup
    exit 1
}

get_dsc()
{
    grep "$1=" $2 | sed -e 's/module.*=[ 	]*//' -e 's:[ 	]*$::'
}

init()
{
    usage="Usage: pkinst [-v] <pkg-file>"

    if [ $# -ne 1 ] && [ $# -ne 2 ]; then
	fatal $usage
    fi
    
    progname=$0
    if [ "$1" = "-v" ]; then
	verbose=1
	pkgfile=$2
    else
	pkgfile=$1
    fi

    if [ ${pkgfile#/} = $pkgfile ]; then
	pkgfile=`pwd`/$pkgfile
    fi

    if ! [ -e "$pkgfile" ]; then
	fatal "package $pkgfile not found"
    fi

    tmpdir=/opt/tmp/pkinst.$$
    if [ ! -x /usr/tmp ] || [ ! -w /usr/tmp ]; then
	echo "Can't write to /usr/tmp.  Switch to /opt/tmp!"
	mkdir -p /opt/tmp
	tmpdir=/opt/tmp/pkinst.$$
    fi

    mkdir -p $tmpdir
    (cd $tmpdir; tar xf $pkgfile)
    (cd $tmpdir; tar zxf control.tgz ./release.dsc)

    if [ ! -e $tmpdir/release.dsc ] ; then
	fatal "can't find release.dsc"
    fi

    pkgid=`get_dsc module.pkgid $tmpdir/release.dsc`
    userid=`get_dsc module.userid $tmpdir/release.dsc`
    uid=`get_dsc module.uid $tmpdir/release.dsc`
    gid=`get_dsc module.gid $tmpdir/release.dsc`
    mod_name=`get_dsc module.name $tmpdir/release.dsc`
    release=`get_dsc module.release $tmpdir/release.dsc`
    revision=`get_dsc module.revision $tmpdir/release.dsc`
    major_version=`get_dsc module.major_version $tmpdir/release.dsc`
    sorder=`get_dsc module.order $tmpdir/release.dsc`
    

    if [ -z "$pkgfile" ] || [ -z "$uid" ] || [ -z "$userid" ] || [ -z "$pkgid" ] ; then
	fatal "invalid control/release.dsc"
    fi

    if [ -z $gid ]; then
	gid=$uid
    fi

    if [ -z $mod_name ]; then
	mod_name="Unknown"
    fi
    
    if [ -z $sorder ]; then
	sorder="90";
    fi
    korder="$((100-$sorder))"

    prefix=/opt/broadon
    mgmtdir=$prefix/mgmt
    datadir=$prefix/data
    pkgdir=$prefix/pkgs

    mgmt=$mgmtdir/$pkgid
    data=$datadir/$pkgid
    pkg=$pkgdir/$pkgid

    oldmgmt=$mgmtdir/$pkgid.old
    olddata=$datadir/$pkgid.old
    oldpkg=$pkgdir/$pkgid.old

    if [ -n "$verbose" ]; then
	echo -e "Package ID:\t$pkgid"
	echo -e "User ID:\t$userid"
	echo -e "Uid:\t\t$uid"
	echo -e "Gid:\t\t$gid"
	echo -e "Major_version:\t$major_version"
	echo -e "Revision:\t$revision"
	echo -e "Release:\t$release"
	echo -e "Order:\t$order"
	echo -e "MgmtDir:\t$mgmtdir"
	echo -e "DataDir:\t$datadir"
	echo -e "PkgDir: \t$pkgdir"
    fi

# system init (needed only once)
    mkdir -p $mgmtdir
    mkdir -p $datadir
    mkdir -p $pkgdir
}


adduser()
{
    if ! [ -e /opt/broadon/mgmt/$userid.passwd ]; then
	mkdir -p /opt/broadon/mgmt
	echo "$userid:x:$uid:$gid:$mod_name:/opt/broadon/data/$userid:/bin/sh" > /opt/broadon/mgmt/$userid.passwd
	sync
    fi
    if [ -z "`grep \"\<$userid\>\" /etc/passwd`" ]; then
	cat /opt/broadon/mgmt/$userid.passwd >> /etc/passwd
	mkdir -p /opt/broadon/data/$userid
	chown $uid.$gid /opt/broadon/data/$userid
	if [ -z "`grep :$gid: /etc/group`" ]; then
	    echo "$userid:x:$gid:" >> /etc/group
	fi
    fi
}


expand()
{
    if ! [ -e $tmpdir/control.tgz ]; then
	fatal "missing control.tgz file"
    fi
    if ! [ -e $tmpdir/cmd.tgz ]; then
	fatal "missing cmd.tgz file"
    fi
    if ! [ -e $tmpdir/package.tgz ]; then
	fatal "missing package.tgz file"
    fi
	
    if [ -e $mgmt ] && [ -e $pkg ]; then
	rm -fr $oldmgmt
	rm -fr $oldpkg
	mv -f $mgmt $oldmgmt
	mv -f $pkg $oldpkg
    fi

    mkdir -p $mgmt
    mkdir -p $mgmt/files
    mkdir -p $mgmt/control
    mkdir -p $mgmt/cmd
    chown -R $userid $mgmt

    mv -f $tmpdir/* $mgmt/files
    rm -fr $tmpdir
    chmod -R a+r $mgmt/files
    sync

    userdo tar --no-same-owner -zxf $mgmt/files/control.tgz -C $mgmt/control
    sync

    userdo tar --no-same-owner -zxf $mgmt/files/cmd.tgz -C $mgmt/cmd
    sync
}


preinst()
{
    sync
    test -e $mgmt/control/preinst && \
	$mgmt/control/preinst --userid $userid --mgmt $mgmt --oldmgmt $oldmgmt --pkg $pkg --oldpkg $oldpkg --data $data
    sync
}


install()
{
    mkdir -p $pkg
    mkdir -p $data
    chown $userid $pkg $data
    userdo tar --no-same-owner -zxf $mgmt/files/package.tgz -C $pkg
}


postinst()
{
    sync
    test -e $mgmt/control/postinst && \
	$mgmt/control/postinst --userid $userid --mgmt $mgmt --oldmgmt $oldmgmt --pkg $pkg --oldpkg $oldpkg --data $data
    sync

    # setup /opt/init.d scripts
    if [ -e $mgmt/cmd/init ]; then
	mkdir -p /opt/init.d
	rm -fr /opt/init.d/[SK][0-9][0-9]$pkgid
	ln -sf $mgmt/cmd/init /opt/init.d/S$sorder$pkgid
	ln -sf $mgmt/cmd/init /opt/init.d/K$korder$pkgid
    fi
}


fini()
{
    sync
    if ! [ -x $mgmt/cmd/getVersion ]; then
	fatal "getVersion not exists or not installed properly"
    fi

    rm -fr $oldmgmt
    rm -fr $oldpkg

    cleanup

    if [ -e "/tmp/pkinst_reboot" ]; then
	rm -fr "/tmp/pkinst_reboot"
	reboot
	sleep 300
    fi
}

init $*
adduser
expand
preinst
install
postinst
fini

 



#!/bin/sh

PKG=/opt/broadon/pkgs/ems
DATA=/opt/broadon/data/svcdrv
PROP=BBserver.properties
NOARPCTL=/opt/broadon/pkgs/netutil/bin/noarpctl

act_ems=`/sbin/printconf sys.act.ems`
act_ems=${act_ems// /@@}
act_lb=`/sbin/printconf sys.act.lb`
act_lb=${act_lb// /@@}

#
# The activation variable is of the form:
# act_value;key1=value1;key2=value2 ...
#
parse_attributes() {
    # remove the leading part up to the first ';'
    config=${act_ems#*;}
    if [ "$act_ems" != "$config" ]; then
	# we do have attribute to handle

	oldIFS=$IFS
	IFS=';'
	# for each token of the form x=y, replace the '=' by a space and then
	# set the config variable to it.
	for i in $config; do
	    IFS=$oldIFS
	    kv=(`echo ${i/=/' '}`)
	    /sbin/setconf ${kv[0]} "${kv[1]//@@/' '}"
	    IFS=';'
	done
	IFS=$oldIFS
    fi
}

configure() {
    cp $PKG/conf/ems.conf $DATA/conf/httpd
    cp $PKG/conf/ems.xml $DATA/webapps
    mkdir -p $DATA/webapps/ems
    cp $PKG/conf/XMLRequest.properties $DATA/webapps/ems
    cp $PKG/webapps/ems.war $DATA/webapps

    db_url=`/sbin/printconf ems.db.url`
    db_user=`/sbin/printconf ems.db.user`
    db_password=`/sbin/printconf ems.db.password`
    hsm_server=`/sbin/printconf ems.hsm.url`
    if [ -z "$db_url" ] || [ -z "$db_user" ] || [ -z "$db_password" ] || \
       [ -z "$hsm_server" ]; then
       echo "Missing config. variables"
       exit 1
    fi
    sed -e "s/@@DB_URL@@/$db_url/"		\
        -e "s/@@DB_USER@@/$db_user/"		\
	-e "s/@@DB_PASSWORD@@/$db_password/"	\
	-e "s/@@HSM_SERVER@@/$hsm_server/"	\
	    $PKG/conf/$PROP.tmpl > $DATA/webapps/ems/$PROP
    if [ $? != 0 ]; then
	exit 1
    fi
}

#
# Set the IP address used by this package.
#
set_ip() {
    IP=`host ems | sed -e 's/^.* //'`
    IPd=`echo ${IP} | sed -e 's/^.*\.//'`
    addrinfo=(`ifconfig eth0 | grep inet | sed -e 's/inet //'`)
    myaddr=${addrinfo[0]/*:/}
    bcast=${addrinfo[1]/*:/}
    netmask=${addrinfo[2]/*:/}

    # turn on "noarp" if I'm served by load-balancer
    if [ "$lb" = "p" ] || [ "$lb" = "s" ]; then
        ${NOARPCTL} add ${IP} ${myaddr}
    fi
    
    ifconfig eth0:${IPd} ${IP} netmask ${netmask} broadcast ${bcast}
}

#
# Clear the IP address used by this package.
#
unset_ip() {
    IP=`host ems | sed -e 's/^.* //'`
    IPd=`echo ${IP} | sed -e 's/^.*\.//'`
    ifconfig eth0:${IPd} down

    if [ "$lb" = "p" ] || [ "$lb" = "s" ]; then
        ${NOARPCTL} del ${IP}
    fi
}

start() {
    if [ "${act_ems%%;*}" != "1" ]; then
	exit 0
    fi
    # set up IP alias only if load balancer is not running
    if [ "${act_lb%%;*}" != "1" ] || ( [ "$lb" != "p" ] && [ "$lb" != "s" ] ); then
        set_ip
    fi
    configure
}

stop() {
    if [ "${act_lb%%;*}" != "1" ] || ( [ "$lb" != "p" ] && [ "$lb" != "s" ] ); then
	unset_ip
    fi
}


parse_attributes
lb=`/sbin/printconf ems.lb`


case "$1" in
    start)
	start
	;;
    stop)
	stop
	;;
    *)
	echo $"Usage: $0 {start|stop}"
esac

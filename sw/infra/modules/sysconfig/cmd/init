#!/bin/sh

PKG=/opt/broadon/pkgs/sysconfig
NOARPCTL=/opt/broadon/pkgs/netutil/bin/noarpctl

sys_net_dev=`/sbin/printconf sys.net.dev`
act_sysconfig=`/sbin/printconf sys.act.sysconfig`
act_sysconfig=${act_sysconfig// /@@}
act_lb=`/sbin/printconf sys.act.lb`
act_lb=${act_lb// /@@}

#
# The activation variable is of the form:
# act_value;key1=value1;key2=value2 ...
#
parse_attributes() {
    # remove the leading part up to the first ';'
    config=${act_sysconfig#*;}
    if [ "$act_sysconfig" != "$config" ]; then
	# we do have attribute to handle

	oldIFS=$IFS
	IFS=';'
	# for each token of the form x=y, replace the '=' by a space and then
	# set the config variable to it.
	for i in $config; do
	    IFS=$oldIFS
	    kv=(`echo ${i/=/' '}`)
	    /sbin/setconf ${kv[0]} "${kv[1]//@@/' '}"
	    IFS=';'
	done
	IFS=$oldIFS
    fi
}

#
# Set the IP address used by this package.
#
set_ip() {
    IP=`host dns | sed -e 's/^.* //'`
    IPd=`echo ${IP} | sed -e 's/^.*\.//'`
    addrinfo=(`ifconfig $sys_net_dev | grep inet | sed -e 's/inet //'`)
    myaddr=${addrinfo[0]/*:/}
    bcast=${addrinfo[1]/*:/}
    netmask=${addrinfo[2]/*:/}
    
    if [ "$myaddr" = "$IP" ]; then
	return 0
    fi

    # turn on "noarp" if I'm served by load-balancer
    if [ "$dns_lb" = "p" ] || [ "$dns_lb" = "s" ]; then
        ${NOARPCTL} add ${IP} ${myaddr}
    fi
    
    ifconfig $sys_net_dev:${IPd} ${IP} netmask ${netmask} broadcast ${bcast}
}

#
# Clear the IP address used by this package.
#
unset_ip() {
    IP=`host dns  | sed -e 's/^.* //'`
    IPd=`echo ${IP} | sed -e 's/^.*\.//'`
    myaddr=`ifconfig $sys_net_dev | grep "inet addr:" | sed -e 's/ *Bcast.*$//' | sed -e 's/^.*://'`

    if [ "$myaddr" = "$IP" ]; then
	return 0
    fi

    ifconfig $sys_net_dev:${IPd} down

    if [ "$dns_lb" = "p" ] || [ "$dns_lb" = "b" ]; then
        ${NOARPCTL} del ${IP}
    fi
}

start() {
    if [ "${act_sysconfig%%;*}" != "1" ]; then
	exit 0
    fi

    act_dns=`/sbin/printconf sysconfig.act.dns`
    act_dns=${act_dns// /@@}
    if [ "${act_dns}" != "1" ]; then
	exit 0
    fi
    
    # set up IP alias only if load balancer is not running
    if [ "${act_lb%%;*}" != "1" ] || ( [ "$dns_lb" != "p" ] && [ "$dns_lb" != "s" ] ); then
        set_ip
    fi

    # Tell named to listen to any newly created IP aliases.
    /etc/init.d/named restart
}

stop() {
    if [ "${act_lb%%;*}" != "1" ] || ( [ "$dns_lb" != "p" ] && [ "$dns_lb" != "s" ] ); then
	unset_ip
    fi
}


parse_attributes
dns_lb=`/sbin/printconf sysconfig.dns.lb`
    
case "$1" in
    start)
	start
	;;
    stop)
	stop
	;;
    restart)
	stop
	start
	;;
    *)
	echo $"Usage: $0 {start|stop|restart}"
esac

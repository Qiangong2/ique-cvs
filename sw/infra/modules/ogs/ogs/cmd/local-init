#!/bin/sh

PKG=/opt/broadon/pkgs/ogs
DATA=/opt/broadon/data/svcdrv
PROP=BBserver.properties
NOARPCTL=/opt/broadon/pkgs/netutil/bin/noarpctl

sys_net_dev=`/sbin/printconf sys.net.dev`
act_ogs=`/sbin/printconf sys.act.ogs`
act_ogs=${act_ogs// /@@}
act_lb=`/sbin/printconf sys.act.lb`
act_lb=${act_lb// /@@}

#
# The activation variable is of the form:
# act_value;key1=value1;key2=value2 ...
#
parse_attributes() {
    # remove the leading part up to the first ';'
    config=${act_ogs#*;}
    if [ "$act_ogs" != "$config" ]; then
	# we do have attribute to handle

	oldIFS=$IFS
	IFS=';'
	# for each token of the form x=y, replace the '=' by a space and then
	# set the config variable to it.
	for i in $config; do
	    IFS=$oldIFS
	    kv=(`echo ${i/=/' '}`)
	    /sbin/setconf ${kv[0]} "${kv[1]//@@/' '}"
	    IFS=';'
	done
	IFS=$oldIFS
    fi
}

configure() {
    cp $PKG/conf/ogs.conf $DATA/conf/httpd
    cp $PKG/conf/ogs.xml $DATA/webapps
    mkdir -p $DATA/webapps/ogs
    cp $PKG/webapps/ogs.war $DATA/webapps

    db_url=`/sbin/printconf ogs.db.url`
    db_user=`/sbin/printconf ogs.db.user`
    db_password=`/sbin/printconf ogs.db.password`
    if [ -z "$db_url" ] || [ -z "$db_user" ] || [ -z "$db_password" ]; then
       echo "Missing config. variables"
       exit 1
    fi
    sed -e "s/@@DB_URL@@/$db_url/"		\
        -e "s/@@DB_USER@@/$db_user/"		\
	-e "s/@@DB_PASSWORD@@/$db_password/"	\
	    $PKG/conf/$PROP.tmpl > $DATA/webapps/ogs/$PROP
    if [ $? != 0 ]; then
	exit 1
    fi
}

#
# Set the IP address used by this package.
#
set_ip() {
    IP=`host ogs | sed -e 's/^.* //'`
    IPd=`echo ${IP} | sed -e 's/^.*\.//'`
    addrinfo=(`ifconfig $sys_net_dev | grep inet | sed -e 's/inet //'`)
    myaddr=${addrinfo[0]/*:/}
    bcast=${addrinfo[1]/*:/}
    netmask=${addrinfo[2]/*:/}

    # turn on "noarp" if I'm served by load-balancer
    if [ "$lb" = "p" ] || [ "$lb" = "s" ]; then
        ${NOARPCTL} add ${IP} ${myaddr}
    fi
    
    ifconfig $sys_net_dev:${IPd} ${IP} netmask ${netmask} broadcast ${bcast}
}

#
# Clear the IP address used by this package.
#
unset_ip() {
    IP=`host ogs | sed -e 's/^.* //'`
    IPd=`echo ${IP} | sed -e 's/^.*\.//'`
    ifconfig $sys_net_dev:${IPd} down

    if [ "$lb" = "p" ] || [ "$lb" = "s" ]; then
        ${NOARPCTL} del ${IP}
    fi
}

start() {
    if [ "${act_ogs%%;*}" != "1" ]; then
	exit 0
    fi
    # set up IP logs only if load balancer is not running
    if [ "${act_lb%%;*}" != "1" ] || ( [ "$lb" != "p" ] && [ "$lb" != "s" ] ); then
        set_ip
    fi
    configure
}

stop() {
    if [ "${act_lb%%;*}" != "1" ] || ( [ "$lb" != "p" ] && [ "$lb" != "s" ] ); then
	unset_ip
    fi
}

parse_attributes
lb=`/sbin/printconf ogs.lb`

case "$1" in
    start)
	start
	;;
    stop)
	stop
	;;
    *)
	echo $"Usage: $0 {start|stop}"
esac

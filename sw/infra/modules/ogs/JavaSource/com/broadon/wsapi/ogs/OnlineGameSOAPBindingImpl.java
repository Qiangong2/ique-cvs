/**
 * OnlineGameSOAPBindingImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ogs;

import com.broadon.ogs.*;

public class OnlineGameSOAPBindingImpl extends OnlineGameServiceImpl
  implements com.broadon.wsapi.ogs.OnlineGamePortType{
    public com.broadon.wsapi.ogs.ListBuddiesResponseType listBuddies(com.broadon.wsapi.ogs.ListBuddiesRequestType listBuddiesRequest) throws java.rmi.RemoteException {
        return ListBuddies.listBuddies(this, listBuddiesRequest);
    }

    public com.broadon.wsapi.ogs.ChangeBuddyStateResponseType changeBuddyState(com.broadon.wsapi.ogs.ChangeBuddyStateRequestType changeBuddyStateRequest) throws java.rmi.RemoteException {
        return ChangeBuddyState.changeBuddyState(this, changeBuddyStateRequest);
    }

    public com.broadon.wsapi.ogs.InviteBuddyResponseType inviteBuddy(com.broadon.wsapi.ogs.InviteBuddyRequestType inviteBuddyRequest) throws java.rmi.RemoteException {
        return InviteBuddy.inviteBuddy(this, inviteBuddyRequest);
    }

    public com.broadon.wsapi.ogs.DeleteGameScoreResponseType deleteGameScore(com.broadon.wsapi.ogs.DeleteGameScoreRequestType deleteGameScoreRequest) throws java.rmi.RemoteException {
        return DeleteGameScore.deleteGameScore(this, deleteGameScoreRequest);
    }

    public com.broadon.wsapi.ogs.DeleteMessagesResponseType deleteMessages(com.broadon.wsapi.ogs.DeleteMessagesRequestType deleteMessagesRequest) throws java.rmi.RemoteException {
        return DeleteMessages.deleteMessages(this, deleteMessagesRequest);
    }
    
    public com.broadon.wsapi.ogs.GetGameScoreKeysResponseType getGameScoreKeys(com.broadon.wsapi.ogs.GetGameScoreKeysRequestType getGameScoreKeysRequest) throws java.rmi.RemoteException {
        return GetGameScoreKeys.getGameScoreKeys(this, getGameScoreKeysRequest);
    }

    public com.broadon.wsapi.ogs.GetGameScoreObjectResponseType getGameScoreObject(com.broadon.wsapi.ogs.GetGameScoreObjectRequestType getGameScoreObjectRequest) throws java.rmi.RemoteException {
        return GetGameScoreObject.getGameScoreObject(this, getGameScoreObjectRequest);
    }

    public com.broadon.wsapi.ogs.GetGameScoreResponseType getGameScore(com.broadon.wsapi.ogs.GetGameScoreRequestType getGameScoreRequest) throws java.rmi.RemoteException {
        return GetGameScore.getGameScore(this, getGameScoreRequest);
    }

    public com.broadon.wsapi.ogs.GetMessageListResponseType getMessageList(com.broadon.wsapi.ogs.GetMessageListRequestType getMessageListRequest) throws java.rmi.RemoteException {
        return GetMessageList.getMessageList(this, getMessageListRequest);
    }

    public com.broadon.wsapi.ogs.GetRankedGameScoreResponseType getRankedGameScore(com.broadon.wsapi.ogs.GetRankedGameScoreRequestType getRankedGameScoreRequest) throws java.rmi.RemoteException {
        return GetRankedGameScore.getRankedGameScore(this, getRankedGameScoreRequest);
    }

    public com.broadon.wsapi.ogs.QueryGameSessionsResponseType queryGameSessions(com.broadon.wsapi.ogs.QueryGameSessionsRequestType queryGameSessionsRequest) throws java.rmi.RemoteException {
        return QueryGameSessions.queryGameSessions(this, queryGameSessionsRequest);
    }

    public com.broadon.wsapi.ogs.RegisterGameSessionResponseType registerGameSession(com.broadon.wsapi.ogs.RegisterGameSessionRequestType registerGameSessionRequest) throws java.rmi.RemoteException {
        return RegisterGameSession.registerGameSession(this, registerGameSessionRequest);
    }

    public com.broadon.wsapi.ogs.RetrieveMessageResponseType retrieveMessage(com.broadon.wsapi.ogs.RetrieveMessageRequestType retrieveMessageRequest) throws java.rmi.RemoteException {
        return RetrieveMessage.retrieveMessage(this, retrieveMessageRequest);
    }

    public com.broadon.wsapi.ogs.SearchGameSessionsResponseType searchGameSessions(com.broadon.wsapi.ogs.SearchGameSessionsRequestType searchGameSessionsRequest) throws java.rmi.RemoteException {
        return SearchGameSessions.searchGameSessions(this, searchGameSessionsRequest);
    }

    public com.broadon.wsapi.ogs.GetNetIdRangeResponseType getNetIdRange(com.broadon.wsapi.ogs.GetNetIdRangeRequestType getNetIdRangeRequest) throws java.rmi.RemoteException {
        return GetNetIdRange.getNetIdRange(this, getNetIdRangeRequest);
    }
    
    public com.broadon.wsapi.ogs.StoreMessageResponseType storeMessage(com.broadon.wsapi.ogs.StoreMessageRequestType storeMessageRequest) throws java.rmi.RemoteException {
        return StoreMessage.storeMessage(this, storeMessageRequest);
    }

    public com.broadon.wsapi.ogs.SubmitGameScoresResponseType submitGameScores(com.broadon.wsapi.ogs.SubmitGameScoresRequestType submitGameScoresRequest) throws java.rmi.RemoteException {
        return SubmitGameScores.submitGameScores(this, submitGameScoresRequest);
    }

    public com.broadon.wsapi.ogs.SubmitGameScoreObjectResponseType submitGameScoreObject(com.broadon.wsapi.ogs.SubmitGameScoreObjectRequestType submitGameScoreObjectRequest) throws java.rmi.RemoteException {
        return SubmitGameScoreObject.submitGameScoreObject(this, submitGameScoreObjectRequest);
    }

    public com.broadon.wsapi.ogs.UnregisterGameSessionResponseType unregisterGameSession(com.broadon.wsapi.ogs.UnregisterGameSessionRequestType unregisterGameSessionRequest) throws java.rmi.RemoteException {
        return UnregisterGameSession.unregisterGameSession(this, unregisterGameSessionRequest);
    }

    public com.broadon.wsapi.ogs.ExtendGameSessionResponseType extendGameSession(com.broadon.wsapi.ogs.ExtendGameSessionRequestType extendGameSessionRequest) throws java.rmi.RemoteException {
        return ExtendGameSession.extendGameSession(this, extendGameSessionRequest);
    }
    
    public com.broadon.wsapi.ogs.RegisterResponseType register(com.broadon.wsapi.ogs.RegisterRequestType registerRequest) throws java.rmi.RemoteException {
        return Register.register(this, registerRequest);
    }

    public com.broadon.wsapi.ogs.LoginResponseType login(com.broadon.wsapi.ogs.LoginRequestType loginRequest) throws java.rmi.RemoteException {
        return Login.login(this, loginRequest);
    }

    public com.broadon.wsapi.ogs.SetPasswordResponseType setPassword(com.broadon.wsapi.ogs.SetPasswordRequestType setPasswordRequest) throws java.rmi.RemoteException {
        return SetPassword.setPassword(this, setPasswordRequest);
    }

    public com.broadon.wsapi.ogs.UpdateUserInfoResponseType updateUserInfo(com.broadon.wsapi.ogs.UpdateUserInfoRequestType updateUserInfoRequest) throws java.rmi.RemoteException {
        return UpdateUserInfo.updateUserInfo(this, updateUserInfoRequest);
    }

    public com.broadon.wsapi.ogs.GetUserInfoResponseType getUserInfo(com.broadon.wsapi.ogs.GetUserInfoRequestType getUserInfoRequest) throws java.rmi.RemoteException {
        return GetUserInfo.getUserInfo(this, getUserInfoRequest);
    }
    
    public com.broadon.wsapi.ogs.GetAccountIdResponseType getAccountId(com.broadon.wsapi.ogs.GetAccountIdRequestType getAccountIdRequest) throws java.rmi.RemoteException {
        return GetAccountId.getAccountId(this, getAccountIdRequest);
    }
}

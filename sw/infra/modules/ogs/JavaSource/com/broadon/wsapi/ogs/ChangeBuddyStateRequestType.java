/**
 * ChangeBuddyStateRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ogs;

public class ChangeBuddyStateRequestType  extends com.broadon.wsapi.ogs.AbstractRequestType  implements java.io.Serializable {
    private int accountId;
    private int buddyAccountId;
    private com.broadon.wsapi.ogs.ChangeBuddyStateActionType action;

    public ChangeBuddyStateRequestType() {
    }

    public ChangeBuddyStateRequestType(
           int accountId,
           int buddyAccountId,
           com.broadon.wsapi.ogs.ChangeBuddyStateActionType action) {
           this.accountId = accountId;
           this.buddyAccountId = buddyAccountId;
           this.action = action;
    }


    /**
     * Gets the accountId value for this ChangeBuddyStateRequestType.
     * 
     * @return accountId
     */
    public int getAccountId() {
        return accountId;
    }


    /**
     * Sets the accountId value for this ChangeBuddyStateRequestType.
     * 
     * @param accountId
     */
    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }


    /**
     * Gets the buddyAccountId value for this ChangeBuddyStateRequestType.
     * 
     * @return buddyAccountId
     */
    public int getBuddyAccountId() {
        return buddyAccountId;
    }


    /**
     * Sets the buddyAccountId value for this ChangeBuddyStateRequestType.
     * 
     * @param buddyAccountId
     */
    public void setBuddyAccountId(int buddyAccountId) {
        this.buddyAccountId = buddyAccountId;
    }


    /**
     * Gets the action value for this ChangeBuddyStateRequestType.
     * 
     * @return action
     */
    public com.broadon.wsapi.ogs.ChangeBuddyStateActionType getAction() {
        return action;
    }


    /**
     * Sets the action value for this ChangeBuddyStateRequestType.
     * 
     * @param action
     */
    public void setAction(com.broadon.wsapi.ogs.ChangeBuddyStateActionType action) {
        this.action = action;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ChangeBuddyStateRequestType)) return false;
        ChangeBuddyStateRequestType other = (ChangeBuddyStateRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.accountId == other.getAccountId() &&
            this.buddyAccountId == other.getBuddyAccountId() &&
            ((this.action==null && other.getAction()==null) || 
             (this.action!=null &&
              this.action.equals(other.getAction())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += getAccountId();
        _hashCode += getBuddyAccountId();
        if (getAction() != null) {
            _hashCode += getAction().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ChangeBuddyStateRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ChangeBuddyStateRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "AccountId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "AccountIdType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("buddyAccountId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "BuddyAccountId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "AccountIdType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("action");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "Action"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ChangeBuddyStateActionType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

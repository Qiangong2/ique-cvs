/**
 * CmpOperatorType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ogs;

public class CmpOperatorType implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected CmpOperatorType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _EQ = "EQ";
    public static final java.lang.String _NEQ = "NEQ";
    public static final java.lang.String _LT = "LT";
    public static final java.lang.String _GT = "GT";
    public static final java.lang.String _LE = "LE";
    public static final java.lang.String _GE = "GE";
    public static final CmpOperatorType EQ = new CmpOperatorType(_EQ);
    public static final CmpOperatorType NEQ = new CmpOperatorType(_NEQ);
    public static final CmpOperatorType LT = new CmpOperatorType(_LT);
    public static final CmpOperatorType GT = new CmpOperatorType(_GT);
    public static final CmpOperatorType LE = new CmpOperatorType(_LE);
    public static final CmpOperatorType GE = new CmpOperatorType(_GE);
    public java.lang.String getValue() { return _value_;}
    public static CmpOperatorType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        CmpOperatorType enumeration = (CmpOperatorType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static CmpOperatorType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CmpOperatorType.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "CmpOperatorType"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}

/**
 * GetRankedGameScoreResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ogs;

public class GetRankedGameScoreResponseType  extends com.broadon.wsapi.ogs.AbstractResponseType  implements java.io.Serializable {
    private com.broadon.wsapi.ogs.RankedScoreResultType[] rankedScoreResult;

    public GetRankedGameScoreResponseType() {
    }

    public GetRankedGameScoreResponseType(
           com.broadon.wsapi.ogs.RankedScoreResultType[] rankedScoreResult) {
           this.rankedScoreResult = rankedScoreResult;
    }


    /**
     * Gets the rankedScoreResult value for this GetRankedGameScoreResponseType.
     * 
     * @return rankedScoreResult
     */
    public com.broadon.wsapi.ogs.RankedScoreResultType[] getRankedScoreResult() {
        return rankedScoreResult;
    }


    /**
     * Sets the rankedScoreResult value for this GetRankedGameScoreResponseType.
     * 
     * @param rankedScoreResult
     */
    public void setRankedScoreResult(com.broadon.wsapi.ogs.RankedScoreResultType[] rankedScoreResult) {
        this.rankedScoreResult = rankedScoreResult;
    }

    public com.broadon.wsapi.ogs.RankedScoreResultType getRankedScoreResult(int i) {
        return this.rankedScoreResult[i];
    }

    public void setRankedScoreResult(int i, com.broadon.wsapi.ogs.RankedScoreResultType _value) {
        this.rankedScoreResult[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetRankedGameScoreResponseType)) return false;
        GetRankedGameScoreResponseType other = (GetRankedGameScoreResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.rankedScoreResult==null && other.getRankedScoreResult()==null) || 
             (this.rankedScoreResult!=null &&
              java.util.Arrays.equals(this.rankedScoreResult, other.getRankedScoreResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getRankedScoreResult() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRankedScoreResult());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRankedScoreResult(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetRankedGameScoreResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetRankedGameScoreResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rankedScoreResult");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "RankedScoreResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "RankedScoreResultType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

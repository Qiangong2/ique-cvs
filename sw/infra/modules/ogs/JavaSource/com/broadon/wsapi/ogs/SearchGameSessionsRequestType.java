/**
 * SearchGameSessionsRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ogs;

public class SearchGameSessionsRequestType  extends com.broadon.wsapi.ogs.AbstractRequestType  implements java.io.Serializable {
    private java.lang.Integer gameId;
    private java.lang.Integer maxLatency;
    private java.lang.String keyword;
    private com.broadon.wsapi.ogs.AttrPredicateType[] attrPredicate;
    private int skipResults;
    private int maxResults;

    public SearchGameSessionsRequestType() {
    }

    public SearchGameSessionsRequestType(
           java.lang.Integer gameId,
           java.lang.Integer maxLatency,
           java.lang.String keyword,
           com.broadon.wsapi.ogs.AttrPredicateType[] attrPredicate,
           int skipResults,
           int maxResults) {
           this.gameId = gameId;
           this.maxLatency = maxLatency;
           this.keyword = keyword;
           this.attrPredicate = attrPredicate;
           this.skipResults = skipResults;
           this.maxResults = maxResults;
    }


    /**
     * Gets the gameId value for this SearchGameSessionsRequestType.
     * 
     * @return gameId
     */
    public java.lang.Integer getGameId() {
        return gameId;
    }


    /**
     * Sets the gameId value for this SearchGameSessionsRequestType.
     * 
     * @param gameId
     */
    public void setGameId(java.lang.Integer gameId) {
        this.gameId = gameId;
    }


    /**
     * Gets the maxLatency value for this SearchGameSessionsRequestType.
     * 
     * @return maxLatency
     */
    public java.lang.Integer getMaxLatency() {
        return maxLatency;
    }


    /**
     * Sets the maxLatency value for this SearchGameSessionsRequestType.
     * 
     * @param maxLatency
     */
    public void setMaxLatency(java.lang.Integer maxLatency) {
        this.maxLatency = maxLatency;
    }


    /**
     * Gets the keyword value for this SearchGameSessionsRequestType.
     * 
     * @return keyword
     */
    public java.lang.String getKeyword() {
        return keyword;
    }


    /**
     * Sets the keyword value for this SearchGameSessionsRequestType.
     * 
     * @param keyword
     */
    public void setKeyword(java.lang.String keyword) {
        this.keyword = keyword;
    }


    /**
     * Gets the attrPredicate value for this SearchGameSessionsRequestType.
     * 
     * @return attrPredicate
     */
    public com.broadon.wsapi.ogs.AttrPredicateType[] getAttrPredicate() {
        return attrPredicate;
    }


    /**
     * Sets the attrPredicate value for this SearchGameSessionsRequestType.
     * 
     * @param attrPredicate
     */
    public void setAttrPredicate(com.broadon.wsapi.ogs.AttrPredicateType[] attrPredicate) {
        this.attrPredicate = attrPredicate;
    }

    public com.broadon.wsapi.ogs.AttrPredicateType getAttrPredicate(int i) {
        return this.attrPredicate[i];
    }

    public void setAttrPredicate(int i, com.broadon.wsapi.ogs.AttrPredicateType _value) {
        this.attrPredicate[i] = _value;
    }


    /**
     * Gets the skipResults value for this SearchGameSessionsRequestType.
     * 
     * @return skipResults
     */
    public int getSkipResults() {
        return skipResults;
    }


    /**
     * Sets the skipResults value for this SearchGameSessionsRequestType.
     * 
     * @param skipResults
     */
    public void setSkipResults(int skipResults) {
        this.skipResults = skipResults;
    }


    /**
     * Gets the maxResults value for this SearchGameSessionsRequestType.
     * 
     * @return maxResults
     */
    public int getMaxResults() {
        return maxResults;
    }


    /**
     * Sets the maxResults value for this SearchGameSessionsRequestType.
     * 
     * @param maxResults
     */
    public void setMaxResults(int maxResults) {
        this.maxResults = maxResults;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SearchGameSessionsRequestType)) return false;
        SearchGameSessionsRequestType other = (SearchGameSessionsRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.gameId==null && other.getGameId()==null) || 
             (this.gameId!=null &&
              this.gameId.equals(other.getGameId()))) &&
            ((this.maxLatency==null && other.getMaxLatency()==null) || 
             (this.maxLatency!=null &&
              this.maxLatency.equals(other.getMaxLatency()))) &&
            ((this.keyword==null && other.getKeyword()==null) || 
             (this.keyword!=null &&
              this.keyword.equals(other.getKeyword()))) &&
            ((this.attrPredicate==null && other.getAttrPredicate()==null) || 
             (this.attrPredicate!=null &&
              java.util.Arrays.equals(this.attrPredicate, other.getAttrPredicate()))) &&
            this.skipResults == other.getSkipResults() &&
            this.maxResults == other.getMaxResults();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getGameId() != null) {
            _hashCode += getGameId().hashCode();
        }
        if (getMaxLatency() != null) {
            _hashCode += getMaxLatency().hashCode();
        }
        if (getKeyword() != null) {
            _hashCode += getKeyword().hashCode();
        }
        if (getAttrPredicate() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAttrPredicate());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAttrPredicate(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        _hashCode += getSkipResults();
        _hashCode += getMaxResults();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SearchGameSessionsRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SearchGameSessionsRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gameId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GameId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maxLatency");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "MaxLatency"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("keyword");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "Keyword"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attrPredicate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "AttrPredicate"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "AttrPredicateType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("skipResults");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SkipResults"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maxResults");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "MaxResults"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

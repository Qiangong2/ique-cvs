/**
 * DeleteMessagesRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ogs;

public class DeleteMessagesRequestType  extends com.broadon.wsapi.ogs.AbstractRequestType  implements java.io.Serializable {
    private int accountId;
    private int[] msgId;

    public DeleteMessagesRequestType() {
    }

    public DeleteMessagesRequestType(
           int accountId,
           int[] msgId) {
           this.accountId = accountId;
           this.msgId = msgId;
    }


    /**
     * Gets the accountId value for this DeleteMessagesRequestType.
     * 
     * @return accountId
     */
    public int getAccountId() {
        return accountId;
    }


    /**
     * Sets the accountId value for this DeleteMessagesRequestType.
     * 
     * @param accountId
     */
    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }


    /**
     * Gets the msgId value for this DeleteMessagesRequestType.
     * 
     * @return msgId
     */
    public int[] getMsgId() {
        return msgId;
    }


    /**
     * Sets the msgId value for this DeleteMessagesRequestType.
     * 
     * @param msgId
     */
    public void setMsgId(int[] msgId) {
        this.msgId = msgId;
    }

    public int getMsgId(int i) {
        return this.msgId[i];
    }

    public void setMsgId(int i, int _value) {
        this.msgId[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DeleteMessagesRequestType)) return false;
        DeleteMessagesRequestType other = (DeleteMessagesRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.accountId == other.getAccountId() &&
            ((this.msgId==null && other.getMsgId()==null) || 
             (this.msgId!=null &&
              java.util.Arrays.equals(this.msgId, other.getMsgId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += getAccountId();
        if (getMsgId() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getMsgId());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getMsgId(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DeleteMessagesRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "DeleteMessagesRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "AccountId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "AccountIdType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("msgId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "MsgId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

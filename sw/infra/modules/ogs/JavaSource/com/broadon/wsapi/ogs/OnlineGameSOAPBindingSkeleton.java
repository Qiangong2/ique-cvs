/**
 * OnlineGameSOAPBindingSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ogs;

import javax.xml.rpc.ServiceException;
import javax.xml.rpc.server.ServiceLifecycle;

public class OnlineGameSOAPBindingSkeleton implements ServiceLifecycle, 
    com.broadon.wsapi.ogs.OnlineGamePortType, org.apache.axis.wsdl.Skeleton {
    private com.broadon.wsapi.ogs.OnlineGamePortType impl;
    private static java.util.Map _myOperations = new java.util.Hashtable();
    private static java.util.Collection _myOperationsList = new java.util.ArrayList();

    /**
    * Returns List of OperationDesc objects with this name
    */
    public static java.util.List getOperationDescByName(java.lang.String methodName) {
        return (java.util.List)_myOperations.get(methodName);
    }

    /**
    * Returns Collection of OperationDescs
    */
    public static java.util.Collection getOperationDescs() {
        return _myOperationsList;
    }

    static {
        org.apache.axis.description.OperationDesc _oper;
        org.apache.axis.description.FaultDesc _fault;
        org.apache.axis.description.ParameterDesc [] _params;
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ListBuddies"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ListBuddiesRequestType"), com.broadon.wsapi.ogs.ListBuddiesRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("listBuddies", _params, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ListBuddiesResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ListBuddiesResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "ListBuddies"));
        _oper.setSoapAction("urn:ogs.wsapi.broadon.com/ListBuddies");
        _myOperationsList.add(_oper);
        if (_myOperations.get("listBuddies") == null) {
            _myOperations.put("listBuddies", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("listBuddies")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ChangeBuddyState"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ChangeBuddyStateRequestType"), com.broadon.wsapi.ogs.ChangeBuddyStateRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("changeBuddyState", _params, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ChangeBuddyStateResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ChangeBuddyStateResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "ChangeBuddyState"));
        _oper.setSoapAction("urn:ogs.wsapi.broadon.com/ChangeBuddyState");
        _myOperationsList.add(_oper);
        if (_myOperations.get("changeBuddyState") == null) {
            _myOperations.put("changeBuddyState", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("changeBuddyState")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "InviteBuddy"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "InviteBuddyRequestType"), com.broadon.wsapi.ogs.InviteBuddyRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("inviteBuddy", _params, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "InviteBuddyResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "InviteBuddyResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "InviteBuddy"));
        _oper.setSoapAction("urn:ogs.wsapi.broadon.com/InviteBuddy");
        _myOperationsList.add(_oper);
        if (_myOperations.get("inviteBuddy") == null) {
            _myOperations.put("inviteBuddy", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("inviteBuddy")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "DeleteGameScore"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "DeleteGameScoreRequestType"), com.broadon.wsapi.ogs.DeleteGameScoreRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("deleteGameScore", _params, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "DeleteGameScoreResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "DeleteGameScoreResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "DeleteGameScore"));
        _oper.setSoapAction("urn:ogs.wsapi.broadon.com/DeleteGameScore");
        _myOperationsList.add(_oper);
        if (_myOperations.get("deleteGameScore") == null) {
            _myOperations.put("deleteGameScore", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("deleteGameScore")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "DeleteMessages"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "DeleteMessagesRequestType"), com.broadon.wsapi.ogs.DeleteMessagesRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("deleteMessages", _params, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "DeleteMessagesResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "DeleteMessagesResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "DeleteMessages"));
        _oper.setSoapAction("urn:ogs.wsapi.broadon.com/DeleteMessages");
        _myOperationsList.add(_oper);
        if (_myOperations.get("deleteMessages") == null) {
            _myOperations.put("deleteMessages", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("deleteMessages")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetGameScoreKeys"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetGameScoreKeysRequestType"), com.broadon.wsapi.ogs.GetGameScoreKeysRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getGameScoreKeys", _params, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetGameScoreKeysResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetGameScoreKeysResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "GetGameScoreKeys"));
        _oper.setSoapAction("urn:ogs.wsapi.broadon.com/GetGameScoreKeys");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getGameScoreKeys") == null) {
            _myOperations.put("getGameScoreKeys", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getGameScoreKeys")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetGameScoreObject"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetGameScoreObjectRequestType"), com.broadon.wsapi.ogs.GetGameScoreObjectRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getGameScoreObject", _params, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetGameScoreObjectResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetGameScoreObjectResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "GetGameScoreObject"));
        _oper.setSoapAction("urn:ogs.wsapi.broadon.com/GetGameScoreObject");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getGameScoreObject") == null) {
            _myOperations.put("getGameScoreObject", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getGameScoreObject")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetGameScore"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetGameScoreRequestType"), com.broadon.wsapi.ogs.GetGameScoreRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getGameScore", _params, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetGameScoreResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetGameScoreResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "GetGameScore"));
        _oper.setSoapAction("urn:ogs.wsapi.broadon.com/GetGameScore");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getGameScore") == null) {
            _myOperations.put("getGameScore", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getGameScore")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetMessageList"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetMessageListRequestType"), com.broadon.wsapi.ogs.GetMessageListRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getMessageList", _params, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetMessageListResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetMessageListResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "GetMessageList"));
        _oper.setSoapAction("urn:ogs.wsapi.broadon.com/GetMessageList");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getMessageList") == null) {
            _myOperations.put("getMessageList", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getMessageList")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetRankedGameScore"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetRankedGameScoreRequestType"), com.broadon.wsapi.ogs.GetRankedGameScoreRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getRankedGameScore", _params, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetRankedGameScoreResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetRankedGameScoreResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "GetRankedGameScore"));
        _oper.setSoapAction("urn:ogs.wsapi.broadon.com/GetRankedGameScore");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getRankedGameScore") == null) {
            _myOperations.put("getRankedGameScore", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getRankedGameScore")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "QueryGameSessions"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "QueryGameSessionsRequestType"), com.broadon.wsapi.ogs.QueryGameSessionsRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("queryGameSessions", _params, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "QueryGameSessionsResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "QueryGameSessionsResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "QueryGameSessions"));
        _oper.setSoapAction("urn:ogs.wsapi.broadon.com/QueryGameSessions");
        _myOperationsList.add(_oper);
        if (_myOperations.get("queryGameSessions") == null) {
            _myOperations.put("queryGameSessions", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("queryGameSessions")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "RegisterGameSession"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "RegisterGameSessionRequestType"), com.broadon.wsapi.ogs.RegisterGameSessionRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("registerGameSession", _params, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "RegisterGameSessionResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "RegisterGameSessionResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "RegisterGameSession"));
        _oper.setSoapAction("urn:ogs.wsapi.broadon.com/RegisterGameSession");
        _myOperationsList.add(_oper);
        if (_myOperations.get("registerGameSession") == null) {
            _myOperations.put("registerGameSession", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("registerGameSession")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "RetrieveMessage"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "RetrieveMessageRequestType"), com.broadon.wsapi.ogs.RetrieveMessageRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("retrieveMessage", _params, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "RetrieveMessageResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "RetrieveMessageResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "RetrieveMessage"));
        _oper.setSoapAction("urn:ogs.wsapi.broadon.com/RetrieveMessage");
        _myOperationsList.add(_oper);
        if (_myOperations.get("retrieveMessage") == null) {
            _myOperations.put("retrieveMessage", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("retrieveMessage")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SearchGameSessions"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SearchGameSessionsRequestType"), com.broadon.wsapi.ogs.SearchGameSessionsRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("searchGameSessions", _params, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SearchGameSessionsResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SearchGameSessionsResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "SearchGameSessions"));
        _oper.setSoapAction("urn:ogs.wsapi.broadon.com/SearchGameSessions");
        _myOperationsList.add(_oper);
        if (_myOperations.get("searchGameSessions") == null) {
            _myOperations.put("searchGameSessions", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("searchGameSessions")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "StoreMessage"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "StoreMessageRequestType"), com.broadon.wsapi.ogs.StoreMessageRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("storeMessage", _params, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "StoreMessageResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "StoreMessageResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "StoreMessage"));
        _oper.setSoapAction("urn:ogs.wsapi.broadon.com/StoreMessage");
        _myOperationsList.add(_oper);
        if (_myOperations.get("storeMessage") == null) {
            _myOperations.put("storeMessage", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("storeMessage")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SubmitGameScores"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SubmitGameScoresRequestType"), com.broadon.wsapi.ogs.SubmitGameScoresRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("submitGameScores", _params, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SubmitGameScoresResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SubmitGameScoresResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "SubmitGameScores"));
        _oper.setSoapAction("urn:ogs.wsapi.broadon.com/SubmitGameScores");
        _myOperationsList.add(_oper);
        if (_myOperations.get("submitGameScores") == null) {
            _myOperations.put("submitGameScores", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("submitGameScores")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SubmitGameScoreObject"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SubmitGameScoreObjectRequestType"), com.broadon.wsapi.ogs.SubmitGameScoreObjectRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("submitGameScoreObject", _params, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SubmitGameScoreObjectResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SubmitGameScoreObjectResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "SubmitGameScoreObject"));
        _oper.setSoapAction("urn:ogs.wsapi.broadon.com/SubmitGameScoreObject");
        _myOperationsList.add(_oper);
        if (_myOperations.get("submitGameScoreObject") == null) {
            _myOperations.put("submitGameScoreObject", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("submitGameScoreObject")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ExtendGameSession"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ExtendGameSessionRequestType"), com.broadon.wsapi.ogs.ExtendGameSessionRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("extendGameSession", _params, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ExtendGameSessionResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ExtendGameSessionResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "ExtendGameSession"));
        _oper.setSoapAction("urn:ogs.wsapi.broadon.com/ExtendGameSession");
        _myOperationsList.add(_oper);
        if (_myOperations.get("extendGameSession") == null) {
            _myOperations.put("extendGameSession", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("extendGameSession")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "UnregisterGameSession"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "UnregisterGameSessionRequestType"), com.broadon.wsapi.ogs.UnregisterGameSessionRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("unregisterGameSession", _params, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "UnregisterGameSessionResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "UnregisterGameSessionResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "UnregisterGameSession"));
        _oper.setSoapAction("urn:ogs.wsapi.broadon.com/UnregisterGameSession");
        _myOperationsList.add(_oper);
        if (_myOperations.get("unregisterGameSession") == null) {
            _myOperations.put("unregisterGameSession", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("unregisterGameSession")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "Register"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "RegisterRequestType"), com.broadon.wsapi.ogs.RegisterRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("register", _params, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "RegisterResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "RegisterResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "Register"));
        _oper.setSoapAction("urn:ogs.wsapi.broadon.com/Register");
        _myOperationsList.add(_oper);
        if (_myOperations.get("register") == null) {
            _myOperations.put("register", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("register")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "Login"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "LoginRequestType"), com.broadon.wsapi.ogs.LoginRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("login", _params, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "LoginResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "LoginResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "Login"));
        _oper.setSoapAction("urn:ogs.wsapi.broadon.com/Login");
        _myOperationsList.add(_oper);
        if (_myOperations.get("login") == null) {
            _myOperations.put("login", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("login")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SetPassword"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SetPasswordRequestType"), com.broadon.wsapi.ogs.SetPasswordRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("setPassword", _params, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SetPasswordResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SetPasswordResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "SetPassword"));
        _oper.setSoapAction("urn:ogs.wsapi.broadon.com/SetPassword");
        _myOperationsList.add(_oper);
        if (_myOperations.get("setPassword") == null) {
            _myOperations.put("setPassword", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("setPassword")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "UpdateUserInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "UpdateUserInfoRequestType"), com.broadon.wsapi.ogs.UpdateUserInfoRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("updateUserInfo", _params, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "UpdateUserInfoResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "UpdateUserInfoResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "UpdateUserInfo"));
        _oper.setSoapAction("urn:ogs.wsapi.broadon.com/UpdateUserInfo");
        _myOperationsList.add(_oper);
        if (_myOperations.get("updateUserInfo") == null) {
            _myOperations.put("updateUserInfo", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("updateUserInfo")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetUserInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetUserInfoRequestType"), com.broadon.wsapi.ogs.GetUserInfoRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getUserInfo", _params, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetUserInfoResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetUserInfoResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "GetUserInfo"));
        _oper.setSoapAction("urn:ogs.wsapi.broadon.com/GetUserInfo");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getUserInfo") == null) {
            _myOperations.put("getUserInfo", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getUserInfo")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetNetIdRange"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetNetIdRangeRequestType"), com.broadon.wsapi.ogs.GetNetIdRangeRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getNetIdRange", _params, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetNetIdRangeResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetNetIdRangeResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "GetNetIdRange"));
        _oper.setSoapAction("urn:ogs.wsapi.broadon.com/GetNetIdRange");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getNetIdRange") == null) {
            _myOperations.put("getNetIdRange", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getNetIdRange")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetAccountId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetAccountIdRequestType"), com.broadon.wsapi.ogs.GetAccountIdRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getAccountId", _params, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetAccountIdResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetAccountIdResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "GetAccountId"));
        _oper.setSoapAction("urn:ogs.wsapi.broadon.com/GetAccountId");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getAccountId") == null) {
            _myOperations.put("getAccountId", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getAccountId")).add(_oper);
    }

    public OnlineGameSOAPBindingSkeleton() {
        this.impl = new com.broadon.wsapi.ogs.OnlineGameSOAPBindingImpl();
    }

    public OnlineGameSOAPBindingSkeleton(com.broadon.wsapi.ogs.OnlineGamePortType impl) {
        this.impl = impl;
    }
    public com.broadon.wsapi.ogs.ListBuddiesResponseType listBuddies(com.broadon.wsapi.ogs.ListBuddiesRequestType listBuddiesRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ogs.ListBuddiesResponseType ret = impl.listBuddies(listBuddiesRequest);
        return ret;
    }

    public com.broadon.wsapi.ogs.ChangeBuddyStateResponseType changeBuddyState(com.broadon.wsapi.ogs.ChangeBuddyStateRequestType changeBuddyStateRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ogs.ChangeBuddyStateResponseType ret = impl.changeBuddyState(changeBuddyStateRequest);
        return ret;
    }

    public com.broadon.wsapi.ogs.InviteBuddyResponseType inviteBuddy(com.broadon.wsapi.ogs.InviteBuddyRequestType inviteBuddyRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ogs.InviteBuddyResponseType ret = impl.inviteBuddy(inviteBuddyRequest);
        return ret;
    }

    public com.broadon.wsapi.ogs.DeleteGameScoreResponseType deleteGameScore(com.broadon.wsapi.ogs.DeleteGameScoreRequestType deleteGameScoreRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ogs.DeleteGameScoreResponseType ret = impl.deleteGameScore(deleteGameScoreRequest);
        return ret;
    }

    public com.broadon.wsapi.ogs.DeleteMessagesResponseType deleteMessages(com.broadon.wsapi.ogs.DeleteMessagesRequestType deleteMessagesRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ogs.DeleteMessagesResponseType ret = impl.deleteMessages(deleteMessagesRequest);
        return ret;
    }

    public com.broadon.wsapi.ogs.GetGameScoreKeysResponseType getGameScoreKeys(com.broadon.wsapi.ogs.GetGameScoreKeysRequestType getGameScoreKeysRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ogs.GetGameScoreKeysResponseType ret = impl.getGameScoreKeys(getGameScoreKeysRequest);
        return ret;
    }

    public com.broadon.wsapi.ogs.GetGameScoreObjectResponseType getGameScoreObject(com.broadon.wsapi.ogs.GetGameScoreObjectRequestType getGameScoreObjectRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ogs.GetGameScoreObjectResponseType ret = impl.getGameScoreObject(getGameScoreObjectRequest);
        return ret;
    }

    public com.broadon.wsapi.ogs.GetGameScoreResponseType getGameScore(com.broadon.wsapi.ogs.GetGameScoreRequestType getGameScoreRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ogs.GetGameScoreResponseType ret = impl.getGameScore(getGameScoreRequest);
        return ret;
    }

    public com.broadon.wsapi.ogs.GetMessageListResponseType getMessageList(com.broadon.wsapi.ogs.GetMessageListRequestType getMessageListRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ogs.GetMessageListResponseType ret = impl.getMessageList(getMessageListRequest);
        return ret;
    }

    public com.broadon.wsapi.ogs.GetRankedGameScoreResponseType getRankedGameScore(com.broadon.wsapi.ogs.GetRankedGameScoreRequestType getRankedGameScoreRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ogs.GetRankedGameScoreResponseType ret = impl.getRankedGameScore(getRankedGameScoreRequest);
        return ret;
    }

    public com.broadon.wsapi.ogs.QueryGameSessionsResponseType queryGameSessions(com.broadon.wsapi.ogs.QueryGameSessionsRequestType queryGameSessionsRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ogs.QueryGameSessionsResponseType ret = impl.queryGameSessions(queryGameSessionsRequest);
        return ret;
    }

    public com.broadon.wsapi.ogs.RegisterGameSessionResponseType registerGameSession(com.broadon.wsapi.ogs.RegisterGameSessionRequestType registerGameSessionRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ogs.RegisterGameSessionResponseType ret = impl.registerGameSession(registerGameSessionRequest);
        return ret;
    }

    public com.broadon.wsapi.ogs.RetrieveMessageResponseType retrieveMessage(com.broadon.wsapi.ogs.RetrieveMessageRequestType retrieveMessageRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ogs.RetrieveMessageResponseType ret = impl.retrieveMessage(retrieveMessageRequest);
        return ret;
    }

    public com.broadon.wsapi.ogs.SearchGameSessionsResponseType searchGameSessions(com.broadon.wsapi.ogs.SearchGameSessionsRequestType searchGameSessionsRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ogs.SearchGameSessionsResponseType ret = impl.searchGameSessions(searchGameSessionsRequest);
        return ret;
    }

    public com.broadon.wsapi.ogs.StoreMessageResponseType storeMessage(com.broadon.wsapi.ogs.StoreMessageRequestType storeMessageRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ogs.StoreMessageResponseType ret = impl.storeMessage(storeMessageRequest);
        return ret;
    }

    public com.broadon.wsapi.ogs.SubmitGameScoresResponseType submitGameScores(com.broadon.wsapi.ogs.SubmitGameScoresRequestType submitGameScoresRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ogs.SubmitGameScoresResponseType ret = impl.submitGameScores(submitGameScoresRequest);
        return ret;
    }

    public com.broadon.wsapi.ogs.SubmitGameScoreObjectResponseType submitGameScoreObject(com.broadon.wsapi.ogs.SubmitGameScoreObjectRequestType submitGameScoreObjectRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ogs.SubmitGameScoreObjectResponseType ret = impl.submitGameScoreObject(submitGameScoreObjectRequest);
        return ret;
    }

    public com.broadon.wsapi.ogs.ExtendGameSessionResponseType extendGameSession(com.broadon.wsapi.ogs.ExtendGameSessionRequestType extendGameSessionRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ogs.ExtendGameSessionResponseType ret = impl.extendGameSession(extendGameSessionRequest);
        return ret;
    }

    public com.broadon.wsapi.ogs.UnregisterGameSessionResponseType unregisterGameSession(com.broadon.wsapi.ogs.UnregisterGameSessionRequestType unregisterGameSessionRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ogs.UnregisterGameSessionResponseType ret = impl.unregisterGameSession(unregisterGameSessionRequest);
        return ret;
    }

    public com.broadon.wsapi.ogs.RegisterResponseType register(com.broadon.wsapi.ogs.RegisterRequestType registerRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ogs.RegisterResponseType ret = impl.register(registerRequest);
        return ret;
    }

    public com.broadon.wsapi.ogs.LoginResponseType login(com.broadon.wsapi.ogs.LoginRequestType loginRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ogs.LoginResponseType ret = impl.login(loginRequest);
        return ret;
    }

    public com.broadon.wsapi.ogs.SetPasswordResponseType setPassword(com.broadon.wsapi.ogs.SetPasswordRequestType setPasswordRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ogs.SetPasswordResponseType ret = impl.setPassword(setPasswordRequest);
        return ret;
    }

    public com.broadon.wsapi.ogs.UpdateUserInfoResponseType updateUserInfo(com.broadon.wsapi.ogs.UpdateUserInfoRequestType updateUserInfoRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ogs.UpdateUserInfoResponseType ret = impl.updateUserInfo(updateUserInfoRequest);
        return ret;
    }

    public com.broadon.wsapi.ogs.GetUserInfoResponseType getUserInfo(com.broadon.wsapi.ogs.GetUserInfoRequestType getUserInfoRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ogs.GetUserInfoResponseType ret = impl.getUserInfo(getUserInfoRequest);
        return ret;
    }

    public com.broadon.wsapi.ogs.GetNetIdRangeResponseType getNetIdRange(com.broadon.wsapi.ogs.GetNetIdRangeRequestType getNetIdRangeRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ogs.GetNetIdRangeResponseType ret = impl.getNetIdRange(getNetIdRangeRequest);
        return ret;
    }

    public com.broadon.wsapi.ogs.GetAccountIdResponseType getAccountId(com.broadon.wsapi.ogs.GetAccountIdRequestType getAccountIdRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ogs.GetAccountIdResponseType ret = impl.getAccountId(getAccountIdRequest);
        return ret;
    }

    public void init(Object context) throws ServiceException    
    {   
        if (impl instanceof ServiceLifecycle) {  
            ((ServiceLifecycle) impl).init(context);  
        }  
    }  
    
    public void destroy()  
    {   
        if (impl instanceof ServiceLifecycle) {  
            ((ServiceLifecycle) impl).destroy();  
        }  
    }

}

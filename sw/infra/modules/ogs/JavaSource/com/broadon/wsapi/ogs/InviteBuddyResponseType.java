/**
 * InviteBuddyResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ogs;

public class InviteBuddyResponseType  extends com.broadon.wsapi.ogs.AbstractResponseType  implements java.io.Serializable {
    private java.lang.Integer buddyAccountId;
    private com.broadon.wsapi.ogs.RelationType relation;

    public InviteBuddyResponseType() {
    }

    public InviteBuddyResponseType(
           java.lang.Integer buddyAccountId,
           com.broadon.wsapi.ogs.RelationType relation) {
           this.buddyAccountId = buddyAccountId;
           this.relation = relation;
    }


    /**
     * Gets the buddyAccountId value for this InviteBuddyResponseType.
     * 
     * @return buddyAccountId
     */
    public java.lang.Integer getBuddyAccountId() {
        return buddyAccountId;
    }


    /**
     * Sets the buddyAccountId value for this InviteBuddyResponseType.
     * 
     * @param buddyAccountId
     */
    public void setBuddyAccountId(java.lang.Integer buddyAccountId) {
        this.buddyAccountId = buddyAccountId;
    }


    /**
     * Gets the relation value for this InviteBuddyResponseType.
     * 
     * @return relation
     */
    public com.broadon.wsapi.ogs.RelationType getRelation() {
        return relation;
    }


    /**
     * Sets the relation value for this InviteBuddyResponseType.
     * 
     * @param relation
     */
    public void setRelation(com.broadon.wsapi.ogs.RelationType relation) {
        this.relation = relation;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof InviteBuddyResponseType)) return false;
        InviteBuddyResponseType other = (InviteBuddyResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.buddyAccountId==null && other.getBuddyAccountId()==null) || 
             (this.buddyAccountId!=null &&
              this.buddyAccountId.equals(other.getBuddyAccountId()))) &&
            ((this.relation==null && other.getRelation()==null) || 
             (this.relation!=null &&
              this.relation.equals(other.getRelation())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getBuddyAccountId() != null) {
            _hashCode += getBuddyAccountId().hashCode();
        }
        if (getRelation() != null) {
            _hashCode += getRelation().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(InviteBuddyResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "InviteBuddyResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("buddyAccountId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "BuddyAccountId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "AccountIdType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("relation");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "Relation"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "RelationType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

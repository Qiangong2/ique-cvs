/**
 * ScoreItemResultType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ogs;

public class ScoreItemResultType  implements java.io.Serializable {
    private com.broadon.wsapi.ogs.ScoreType score;
    private com.broadon.wsapi.ogs.ScoreItemType[] scoreItem;

    public ScoreItemResultType() {
    }

    public ScoreItemResultType(
           com.broadon.wsapi.ogs.ScoreType score,
           com.broadon.wsapi.ogs.ScoreItemType[] scoreItem) {
           this.score = score;
           this.scoreItem = scoreItem;
    }


    /**
     * Gets the score value for this ScoreItemResultType.
     * 
     * @return score
     */
    public com.broadon.wsapi.ogs.ScoreType getScore() {
        return score;
    }


    /**
     * Sets the score value for this ScoreItemResultType.
     * 
     * @param score
     */
    public void setScore(com.broadon.wsapi.ogs.ScoreType score) {
        this.score = score;
    }


    /**
     * Gets the scoreItem value for this ScoreItemResultType.
     * 
     * @return scoreItem
     */
    public com.broadon.wsapi.ogs.ScoreItemType[] getScoreItem() {
        return scoreItem;
    }


    /**
     * Sets the scoreItem value for this ScoreItemResultType.
     * 
     * @param scoreItem
     */
    public void setScoreItem(com.broadon.wsapi.ogs.ScoreItemType[] scoreItem) {
        this.scoreItem = scoreItem;
    }

    public com.broadon.wsapi.ogs.ScoreItemType getScoreItem(int i) {
        return this.scoreItem[i];
    }

    public void setScoreItem(int i, com.broadon.wsapi.ogs.ScoreItemType _value) {
        this.scoreItem[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ScoreItemResultType)) return false;
        ScoreItemResultType other = (ScoreItemResultType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.score==null && other.getScore()==null) || 
             (this.score!=null &&
              this.score.equals(other.getScore()))) &&
            ((this.scoreItem==null && other.getScoreItem()==null) || 
             (this.scoreItem!=null &&
              java.util.Arrays.equals(this.scoreItem, other.getScoreItem())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getScore() != null) {
            _hashCode += getScore().hashCode();
        }
        if (getScoreItem() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getScoreItem());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getScoreItem(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ScoreItemResultType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ScoreItemResultType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("score");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "Score"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ScoreType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scoreItem");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ScoreItem"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ScoreItemType"));
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

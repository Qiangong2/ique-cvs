/**
 * ExtendGameSessionRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ogs;

public class ExtendGameSessionRequestType  extends com.broadon.wsapi.ogs.AbstractRequestType  implements java.io.Serializable {
    private int netId;
    private long extendTime;

    public ExtendGameSessionRequestType() {
    }

    public ExtendGameSessionRequestType(
           int netId,
           long extendTime) {
           this.netId = netId;
           this.extendTime = extendTime;
    }


    /**
     * Gets the netId value for this ExtendGameSessionRequestType.
     * 
     * @return netId
     */
    public int getNetId() {
        return netId;
    }


    /**
     * Sets the netId value for this ExtendGameSessionRequestType.
     * 
     * @param netId
     */
    public void setNetId(int netId) {
        this.netId = netId;
    }


    /**
     * Gets the extendTime value for this ExtendGameSessionRequestType.
     * 
     * @return extendTime
     */
    public long getExtendTime() {
        return extendTime;
    }


    /**
     * Sets the extendTime value for this ExtendGameSessionRequestType.
     * 
     * @param extendTime
     */
    public void setExtendTime(long extendTime) {
        this.extendTime = extendTime;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ExtendGameSessionRequestType)) return false;
        ExtendGameSessionRequestType other = (ExtendGameSessionRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.netId == other.getNetId() &&
            this.extendTime == other.getExtendTime();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += getNetId();
        _hashCode += new Long(getExtendTime()).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ExtendGameSessionRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ExtendGameSessionRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("netId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "NetId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("extendTime");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ExtendTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "TimeStampType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

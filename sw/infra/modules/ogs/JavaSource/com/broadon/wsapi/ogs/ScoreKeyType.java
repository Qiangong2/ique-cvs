/**
 * ScoreKeyType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ogs;

public class ScoreKeyType  implements java.io.Serializable {
    private int accountId;
    private int gameId;
    private long titleId;
    private int slotId;
    private int scoreId;

    public ScoreKeyType() {
    }

    public ScoreKeyType(
           int accountId,
           int gameId,
           long titleId,
           int slotId,
           int scoreId) {
           this.accountId = accountId;
           this.gameId = gameId;
           this.titleId = titleId;
           this.slotId = slotId;
           this.scoreId = scoreId;
    }


    /**
     * Gets the accountId value for this ScoreKeyType.
     * 
     * @return accountId
     */
    public int getAccountId() {
        return accountId;
    }


    /**
     * Sets the accountId value for this ScoreKeyType.
     * 
     * @param accountId
     */
    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }


    /**
     * Gets the gameId value for this ScoreKeyType.
     * 
     * @return gameId
     */
    public int getGameId() {
        return gameId;
    }


    /**
     * Sets the gameId value for this ScoreKeyType.
     * 
     * @param gameId
     */
    public void setGameId(int gameId) {
        this.gameId = gameId;
    }


    /**
     * Gets the titleId value for this ScoreKeyType.
     * 
     * @return titleId
     */
    public long getTitleId() {
        return titleId;
    }


    /**
     * Sets the titleId value for this ScoreKeyType.
     * 
     * @param titleId
     */
    public void setTitleId(long titleId) {
        this.titleId = titleId;
    }


    /**
     * Gets the slotId value for this ScoreKeyType.
     * 
     * @return slotId
     */
    public int getSlotId() {
        return slotId;
    }


    /**
     * Sets the slotId value for this ScoreKeyType.
     * 
     * @param slotId
     */
    public void setSlotId(int slotId) {
        this.slotId = slotId;
    }


    /**
     * Gets the scoreId value for this ScoreKeyType.
     * 
     * @return scoreId
     */
    public int getScoreId() {
        return scoreId;
    }


    /**
     * Sets the scoreId value for this ScoreKeyType.
     * 
     * @param scoreId
     */
    public void setScoreId(int scoreId) {
        this.scoreId = scoreId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ScoreKeyType)) return false;
        ScoreKeyType other = (ScoreKeyType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.accountId == other.getAccountId() &&
            this.gameId == other.getGameId() &&
            this.titleId == other.getTitleId() &&
            this.slotId == other.getSlotId() &&
            this.scoreId == other.getScoreId();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getAccountId();
        _hashCode += getGameId();
        _hashCode += new Long(getTitleId()).hashCode();
        _hashCode += getSlotId();
        _hashCode += getScoreId();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ScoreKeyType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ScoreKeyType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "AccountId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "AccountIdType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gameId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GameId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titleId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "TitleId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("slotId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SlotId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scoreId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ScoreId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

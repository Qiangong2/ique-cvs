/**
 * MessageHeaderType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ogs;

public class MessageHeaderType  implements java.io.Serializable {
    private long sendTime;
    private int msgId;
    private java.lang.String subject;
    private java.lang.String sender;
    private java.lang.String recipient;
    private int status;
    private int mediaFormat;
    private int mediaSize;
    private java.lang.Integer replyMsgId;

    public MessageHeaderType() {
    }

    public MessageHeaderType(
           long sendTime,
           int msgId,
           java.lang.String subject,
           java.lang.String sender,
           java.lang.String recipient,
           int status,
           int mediaFormat,
           int mediaSize,
           java.lang.Integer replyMsgId) {
           this.sendTime = sendTime;
           this.msgId = msgId;
           this.subject = subject;
           this.sender = sender;
           this.recipient = recipient;
           this.status = status;
           this.mediaFormat = mediaFormat;
           this.mediaSize = mediaSize;
           this.replyMsgId = replyMsgId;
    }


    /**
     * Gets the sendTime value for this MessageHeaderType.
     * 
     * @return sendTime
     */
    public long getSendTime() {
        return sendTime;
    }


    /**
     * Sets the sendTime value for this MessageHeaderType.
     * 
     * @param sendTime
     */
    public void setSendTime(long sendTime) {
        this.sendTime = sendTime;
    }


    /**
     * Gets the msgId value for this MessageHeaderType.
     * 
     * @return msgId
     */
    public int getMsgId() {
        return msgId;
    }


    /**
     * Sets the msgId value for this MessageHeaderType.
     * 
     * @param msgId
     */
    public void setMsgId(int msgId) {
        this.msgId = msgId;
    }


    /**
     * Gets the subject value for this MessageHeaderType.
     * 
     * @return subject
     */
    public java.lang.String getSubject() {
        return subject;
    }


    /**
     * Sets the subject value for this MessageHeaderType.
     * 
     * @param subject
     */
    public void setSubject(java.lang.String subject) {
        this.subject = subject;
    }


    /**
     * Gets the sender value for this MessageHeaderType.
     * 
     * @return sender
     */
    public java.lang.String getSender() {
        return sender;
    }


    /**
     * Sets the sender value for this MessageHeaderType.
     * 
     * @param sender
     */
    public void setSender(java.lang.String sender) {
        this.sender = sender;
    }


    /**
     * Gets the recipient value for this MessageHeaderType.
     * 
     * @return recipient
     */
    public java.lang.String getRecipient() {
        return recipient;
    }


    /**
     * Sets the recipient value for this MessageHeaderType.
     * 
     * @param recipient
     */
    public void setRecipient(java.lang.String recipient) {
        this.recipient = recipient;
    }


    /**
     * Gets the status value for this MessageHeaderType.
     * 
     * @return status
     */
    public int getStatus() {
        return status;
    }


    /**
     * Sets the status value for this MessageHeaderType.
     * 
     * @param status
     */
    public void setStatus(int status) {
        this.status = status;
    }


    /**
     * Gets the mediaFormat value for this MessageHeaderType.
     * 
     * @return mediaFormat
     */
    public int getMediaFormat() {
        return mediaFormat;
    }


    /**
     * Sets the mediaFormat value for this MessageHeaderType.
     * 
     * @param mediaFormat
     */
    public void setMediaFormat(int mediaFormat) {
        this.mediaFormat = mediaFormat;
    }


    /**
     * Gets the mediaSize value for this MessageHeaderType.
     * 
     * @return mediaSize
     */
    public int getMediaSize() {
        return mediaSize;
    }


    /**
     * Sets the mediaSize value for this MessageHeaderType.
     * 
     * @param mediaSize
     */
    public void setMediaSize(int mediaSize) {
        this.mediaSize = mediaSize;
    }


    /**
     * Gets the replyMsgId value for this MessageHeaderType.
     * 
     * @return replyMsgId
     */
    public java.lang.Integer getReplyMsgId() {
        return replyMsgId;
    }


    /**
     * Sets the replyMsgId value for this MessageHeaderType.
     * 
     * @param replyMsgId
     */
    public void setReplyMsgId(java.lang.Integer replyMsgId) {
        this.replyMsgId = replyMsgId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof MessageHeaderType)) return false;
        MessageHeaderType other = (MessageHeaderType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.sendTime == other.getSendTime() &&
            this.msgId == other.getMsgId() &&
            ((this.subject==null && other.getSubject()==null) || 
             (this.subject!=null &&
              this.subject.equals(other.getSubject()))) &&
            ((this.sender==null && other.getSender()==null) || 
             (this.sender!=null &&
              this.sender.equals(other.getSender()))) &&
            ((this.recipient==null && other.getRecipient()==null) || 
             (this.recipient!=null &&
              this.recipient.equals(other.getRecipient()))) &&
            this.status == other.getStatus() &&
            this.mediaFormat == other.getMediaFormat() &&
            this.mediaSize == other.getMediaSize() &&
            ((this.replyMsgId==null && other.getReplyMsgId()==null) || 
             (this.replyMsgId!=null &&
              this.replyMsgId.equals(other.getReplyMsgId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += new Long(getSendTime()).hashCode();
        _hashCode += getMsgId();
        if (getSubject() != null) {
            _hashCode += getSubject().hashCode();
        }
        if (getSender() != null) {
            _hashCode += getSender().hashCode();
        }
        if (getRecipient() != null) {
            _hashCode += getRecipient().hashCode();
        }
        _hashCode += getStatus();
        _hashCode += getMediaFormat();
        _hashCode += getMediaSize();
        if (getReplyMsgId() != null) {
            _hashCode += getReplyMsgId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(MessageHeaderType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "MessageHeaderType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sendTime");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SendTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("msgId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "MsgId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subject");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "Subject"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sender");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "Sender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recipient");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "Recipient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "Status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mediaFormat");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "MediaFormat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mediaSize");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "MediaSize"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("replyMsgId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ReplyMsgId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

/**
 * StoreMessageResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ogs;

public class StoreMessageResponseType  extends com.broadon.wsapi.ogs.AbstractResponseType  implements java.io.Serializable {
    private java.lang.Integer msgId;
    private int[] deliveryResult;

    public StoreMessageResponseType() {
    }

    public StoreMessageResponseType(
           java.lang.Integer msgId,
           int[] deliveryResult) {
           this.msgId = msgId;
           this.deliveryResult = deliveryResult;
    }


    /**
     * Gets the msgId value for this StoreMessageResponseType.
     * 
     * @return msgId
     */
    public java.lang.Integer getMsgId() {
        return msgId;
    }


    /**
     * Sets the msgId value for this StoreMessageResponseType.
     * 
     * @param msgId
     */
    public void setMsgId(java.lang.Integer msgId) {
        this.msgId = msgId;
    }


    /**
     * Gets the deliveryResult value for this StoreMessageResponseType.
     * 
     * @return deliveryResult
     */
    public int[] getDeliveryResult() {
        return deliveryResult;
    }


    /**
     * Sets the deliveryResult value for this StoreMessageResponseType.
     * 
     * @param deliveryResult
     */
    public void setDeliveryResult(int[] deliveryResult) {
        this.deliveryResult = deliveryResult;
    }

    public int getDeliveryResult(int i) {
        return this.deliveryResult[i];
    }

    public void setDeliveryResult(int i, int _value) {
        this.deliveryResult[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof StoreMessageResponseType)) return false;
        StoreMessageResponseType other = (StoreMessageResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.msgId==null && other.getMsgId()==null) || 
             (this.msgId!=null &&
              this.msgId.equals(other.getMsgId()))) &&
            ((this.deliveryResult==null && other.getDeliveryResult()==null) || 
             (this.deliveryResult!=null &&
              java.util.Arrays.equals(this.deliveryResult, other.getDeliveryResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getMsgId() != null) {
            _hashCode += getMsgId().hashCode();
        }
        if (getDeliveryResult() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDeliveryResult());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDeliveryResult(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(StoreMessageResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "StoreMessageResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("msgId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "MsgId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deliveryResult");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "DeliveryResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

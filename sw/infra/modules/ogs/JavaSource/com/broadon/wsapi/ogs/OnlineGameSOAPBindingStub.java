/**
 * OnlineGameSOAPBindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ogs;

public class OnlineGameSOAPBindingStub extends org.apache.axis.client.Stub implements com.broadon.wsapi.ogs.OnlineGamePortType {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[26];
        _initOperationDesc1();
        _initOperationDesc2();
        _initOperationDesc3();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ListBuddies");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ListBuddies"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ListBuddiesRequestType"), com.broadon.wsapi.ogs.ListBuddiesRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ListBuddiesResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ogs.ListBuddiesResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ListBuddiesResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ChangeBuddyState");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ChangeBuddyState"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ChangeBuddyStateRequestType"), com.broadon.wsapi.ogs.ChangeBuddyStateRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ChangeBuddyStateResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ogs.ChangeBuddyStateResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ChangeBuddyStateResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("InviteBuddy");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "InviteBuddy"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "InviteBuddyRequestType"), com.broadon.wsapi.ogs.InviteBuddyRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "InviteBuddyResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ogs.InviteBuddyResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "InviteBuddyResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("DeleteGameScore");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "DeleteGameScore"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "DeleteGameScoreRequestType"), com.broadon.wsapi.ogs.DeleteGameScoreRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "DeleteGameScoreResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ogs.DeleteGameScoreResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "DeleteGameScoreResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("DeleteMessages");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "DeleteMessages"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "DeleteMessagesRequestType"), com.broadon.wsapi.ogs.DeleteMessagesRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "DeleteMessagesResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ogs.DeleteMessagesResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "DeleteMessagesResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetGameScoreKeys");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetGameScoreKeys"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetGameScoreKeysRequestType"), com.broadon.wsapi.ogs.GetGameScoreKeysRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetGameScoreKeysResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ogs.GetGameScoreKeysResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetGameScoreKeysResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetGameScoreObject");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetGameScoreObject"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetGameScoreObjectRequestType"), com.broadon.wsapi.ogs.GetGameScoreObjectRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetGameScoreObjectResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ogs.GetGameScoreObjectResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetGameScoreObjectResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetGameScore");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetGameScore"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetGameScoreRequestType"), com.broadon.wsapi.ogs.GetGameScoreRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetGameScoreResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ogs.GetGameScoreResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetGameScoreResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetMessageList");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetMessageList"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetMessageListRequestType"), com.broadon.wsapi.ogs.GetMessageListRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetMessageListResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ogs.GetMessageListResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetMessageListResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetRankedGameScore");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetRankedGameScore"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetRankedGameScoreRequestType"), com.broadon.wsapi.ogs.GetRankedGameScoreRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetRankedGameScoreResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ogs.GetRankedGameScoreResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetRankedGameScoreResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("QueryGameSessions");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "QueryGameSessions"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "QueryGameSessionsRequestType"), com.broadon.wsapi.ogs.QueryGameSessionsRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "QueryGameSessionsResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ogs.QueryGameSessionsResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "QueryGameSessionsResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("RegisterGameSession");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "RegisterGameSession"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "RegisterGameSessionRequestType"), com.broadon.wsapi.ogs.RegisterGameSessionRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "RegisterGameSessionResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ogs.RegisterGameSessionResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "RegisterGameSessionResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("RetrieveMessage");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "RetrieveMessage"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "RetrieveMessageRequestType"), com.broadon.wsapi.ogs.RetrieveMessageRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "RetrieveMessageResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ogs.RetrieveMessageResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "RetrieveMessageResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[12] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("SearchGameSessions");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SearchGameSessions"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SearchGameSessionsRequestType"), com.broadon.wsapi.ogs.SearchGameSessionsRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SearchGameSessionsResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ogs.SearchGameSessionsResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SearchGameSessionsResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[13] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("StoreMessage");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "StoreMessage"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "StoreMessageRequestType"), com.broadon.wsapi.ogs.StoreMessageRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "StoreMessageResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ogs.StoreMessageResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "StoreMessageResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[14] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("SubmitGameScores");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SubmitGameScores"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SubmitGameScoresRequestType"), com.broadon.wsapi.ogs.SubmitGameScoresRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SubmitGameScoresResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ogs.SubmitGameScoresResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SubmitGameScoresResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[15] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("SubmitGameScoreObject");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SubmitGameScoreObject"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SubmitGameScoreObjectRequestType"), com.broadon.wsapi.ogs.SubmitGameScoreObjectRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SubmitGameScoreObjectResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ogs.SubmitGameScoreObjectResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SubmitGameScoreObjectResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[16] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ExtendGameSession");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ExtendGameSession"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ExtendGameSessionRequestType"), com.broadon.wsapi.ogs.ExtendGameSessionRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ExtendGameSessionResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ogs.ExtendGameSessionResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ExtendGameSessionResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[17] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UnregisterGameSession");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "UnregisterGameSession"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "UnregisterGameSessionRequestType"), com.broadon.wsapi.ogs.UnregisterGameSessionRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "UnregisterGameSessionResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ogs.UnregisterGameSessionResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "UnregisterGameSessionResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[18] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Register");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "Register"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "RegisterRequestType"), com.broadon.wsapi.ogs.RegisterRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "RegisterResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ogs.RegisterResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "RegisterResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[19] = oper;

    }

    private static void _initOperationDesc3(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Login");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "Login"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "LoginRequestType"), com.broadon.wsapi.ogs.LoginRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "LoginResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ogs.LoginResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "LoginResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[20] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("SetPassword");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SetPassword"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SetPasswordRequestType"), com.broadon.wsapi.ogs.SetPasswordRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SetPasswordResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ogs.SetPasswordResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SetPasswordResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[21] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateUserInfo");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "UpdateUserInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "UpdateUserInfoRequestType"), com.broadon.wsapi.ogs.UpdateUserInfoRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "UpdateUserInfoResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ogs.UpdateUserInfoResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "UpdateUserInfoResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[22] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetUserInfo");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetUserInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetUserInfoRequestType"), com.broadon.wsapi.ogs.GetUserInfoRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetUserInfoResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ogs.GetUserInfoResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetUserInfoResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[23] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetNetIdRange");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetNetIdRange"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetNetIdRangeRequestType"), com.broadon.wsapi.ogs.GetNetIdRangeRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetNetIdRangeResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ogs.GetNetIdRangeResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetNetIdRangeResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[24] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetAccountId");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetAccountId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetAccountIdRequestType"), com.broadon.wsapi.ogs.GetAccountIdRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetAccountIdResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ogs.GetAccountIdResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetAccountIdResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[25] = oper;

    }

    public OnlineGameSOAPBindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public OnlineGameSOAPBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public OnlineGameSOAPBindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "AbstractRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.AbstractRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "AbstractResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.AbstractResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "AccountIdType");
            cachedSerQNames.add(qName);
            cls = int.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(simplesf);
            cachedDeserFactories.add(simpledf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "AttrPredicateType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.AttrPredicateType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "BuddyInfoType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.BuddyInfoType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ChangeBuddyStateActionType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.ChangeBuddyStateActionType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ChangeBuddyStateRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.ChangeBuddyStateRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ChangeBuddyStateResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.ChangeBuddyStateResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "CmpOperatorType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.CmpOperatorType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "DeleteGameScoreRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.DeleteGameScoreRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "DeleteGameScoreResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.DeleteGameScoreResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "DeleteMessagesRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.DeleteMessagesRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "DeleteMessagesResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.DeleteMessagesResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ExtendGameSessionRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.ExtendGameSessionRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ExtendGameSessionResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.ExtendGameSessionResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GameSessionType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.GameSessionType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetAccountIdRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.GetAccountIdRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetAccountIdResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.GetAccountIdResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetGameScoreKeysRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.GetGameScoreKeysRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetGameScoreKeysResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.GetGameScoreKeysResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetGameScoreObjectRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.GetGameScoreObjectRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetGameScoreObjectResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.GetGameScoreObjectResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetGameScoreRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.GetGameScoreRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetGameScoreResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.GetGameScoreResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetMessageListRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.GetMessageListRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetMessageListResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.GetMessageListResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetNetIdRangeRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.GetNetIdRangeRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetNetIdRangeResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.GetNetIdRangeResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetRankedGameScoreRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.GetRankedGameScoreRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetRankedGameScoreResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.GetRankedGameScoreResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetUserInfoRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.GetUserInfoRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetUserInfoResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.GetUserInfoResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "InviteBuddyRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.InviteBuddyRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "InviteBuddyResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.InviteBuddyResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ListBuddiesRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.ListBuddiesRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ListBuddiesResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.ListBuddiesResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "LoginRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.LoginRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "LoginResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.LoginResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "MessageHeaderType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.MessageHeaderType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "QueryGameSessionsRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.QueryGameSessionsRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "QueryGameSessionsResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.QueryGameSessionsResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "RankedScoreResultType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.RankedScoreResultType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "RegisterGameSessionRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.RegisterGameSessionRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "RegisterGameSessionResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.RegisterGameSessionResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "RegisterRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.RegisterRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "RegisterResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.RegisterResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "RelationType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.RelationType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "RetrieveMessageRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.RetrieveMessageRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "RetrieveMessageResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.RetrieveMessageResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ScoreItemResultType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.ScoreItemResultType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ScoreItemType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.ScoreItemType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ScoreKeyType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.ScoreKeyType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ScoreType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.ScoreType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SearchGameSessionsRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.SearchGameSessionsRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SearchGameSessionsResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.SearchGameSessionsResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SetPasswordRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.SetPasswordRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SetPasswordResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.SetPasswordResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "StoreMessageRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.StoreMessageRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "StoreMessageResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.StoreMessageResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SubmitGameScoreObjectRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.SubmitGameScoreObjectRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SubmitGameScoreObjectResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.SubmitGameScoreObjectResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SubmitGameScoresRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.SubmitGameScoresRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SubmitGameScoresResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.SubmitGameScoresResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "TimeStampType");
            cachedSerQNames.add(qName);
            cls = long.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(simplesf);
            cachedDeserFactories.add(simpledf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "UnregisterGameSessionRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.UnregisterGameSessionRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "UnregisterGameSessionResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.UnregisterGameSessionResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "UpdateUserInfoRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.UpdateUserInfoRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "UpdateUserInfoResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.UpdateUserInfoResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "UserAttributeType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.UserAttributeType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "UserInfoType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ogs.UserInfoType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.broadon.wsapi.ogs.ListBuddiesResponseType listBuddies(com.broadon.wsapi.ogs.ListBuddiesRequestType listBuddiesRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ogs.wsapi.broadon.com/ListBuddies");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "ListBuddies"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {listBuddiesRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ogs.ListBuddiesResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ogs.ListBuddiesResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ogs.ListBuddiesResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ogs.ChangeBuddyStateResponseType changeBuddyState(com.broadon.wsapi.ogs.ChangeBuddyStateRequestType changeBuddyStateRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ogs.wsapi.broadon.com/ChangeBuddyState");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "ChangeBuddyState"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {changeBuddyStateRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ogs.ChangeBuddyStateResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ogs.ChangeBuddyStateResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ogs.ChangeBuddyStateResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ogs.InviteBuddyResponseType inviteBuddy(com.broadon.wsapi.ogs.InviteBuddyRequestType inviteBuddyRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ogs.wsapi.broadon.com/InviteBuddy");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "InviteBuddy"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {inviteBuddyRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ogs.InviteBuddyResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ogs.InviteBuddyResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ogs.InviteBuddyResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ogs.DeleteGameScoreResponseType deleteGameScore(com.broadon.wsapi.ogs.DeleteGameScoreRequestType deleteGameScoreRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ogs.wsapi.broadon.com/DeleteGameScore");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "DeleteGameScore"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {deleteGameScoreRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ogs.DeleteGameScoreResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ogs.DeleteGameScoreResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ogs.DeleteGameScoreResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ogs.DeleteMessagesResponseType deleteMessages(com.broadon.wsapi.ogs.DeleteMessagesRequestType deleteMessagesRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ogs.wsapi.broadon.com/DeleteMessages");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "DeleteMessages"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {deleteMessagesRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ogs.DeleteMessagesResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ogs.DeleteMessagesResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ogs.DeleteMessagesResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ogs.GetGameScoreKeysResponseType getGameScoreKeys(com.broadon.wsapi.ogs.GetGameScoreKeysRequestType getGameScoreKeysRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ogs.wsapi.broadon.com/GetGameScoreKeys");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetGameScoreKeys"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getGameScoreKeysRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ogs.GetGameScoreKeysResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ogs.GetGameScoreKeysResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ogs.GetGameScoreKeysResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ogs.GetGameScoreObjectResponseType getGameScoreObject(com.broadon.wsapi.ogs.GetGameScoreObjectRequestType getGameScoreObjectRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ogs.wsapi.broadon.com/GetGameScoreObject");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetGameScoreObject"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getGameScoreObjectRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ogs.GetGameScoreObjectResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ogs.GetGameScoreObjectResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ogs.GetGameScoreObjectResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ogs.GetGameScoreResponseType getGameScore(com.broadon.wsapi.ogs.GetGameScoreRequestType getGameScoreRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ogs.wsapi.broadon.com/GetGameScore");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetGameScore"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getGameScoreRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ogs.GetGameScoreResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ogs.GetGameScoreResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ogs.GetGameScoreResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ogs.GetMessageListResponseType getMessageList(com.broadon.wsapi.ogs.GetMessageListRequestType getMessageListRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ogs.wsapi.broadon.com/GetMessageList");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetMessageList"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getMessageListRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ogs.GetMessageListResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ogs.GetMessageListResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ogs.GetMessageListResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ogs.GetRankedGameScoreResponseType getRankedGameScore(com.broadon.wsapi.ogs.GetRankedGameScoreRequestType getRankedGameScoreRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ogs.wsapi.broadon.com/GetRankedGameScore");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetRankedGameScore"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getRankedGameScoreRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ogs.GetRankedGameScoreResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ogs.GetRankedGameScoreResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ogs.GetRankedGameScoreResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ogs.QueryGameSessionsResponseType queryGameSessions(com.broadon.wsapi.ogs.QueryGameSessionsRequestType queryGameSessionsRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ogs.wsapi.broadon.com/QueryGameSessions");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "QueryGameSessions"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {queryGameSessionsRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ogs.QueryGameSessionsResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ogs.QueryGameSessionsResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ogs.QueryGameSessionsResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ogs.RegisterGameSessionResponseType registerGameSession(com.broadon.wsapi.ogs.RegisterGameSessionRequestType registerGameSessionRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ogs.wsapi.broadon.com/RegisterGameSession");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "RegisterGameSession"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {registerGameSessionRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ogs.RegisterGameSessionResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ogs.RegisterGameSessionResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ogs.RegisterGameSessionResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ogs.RetrieveMessageResponseType retrieveMessage(com.broadon.wsapi.ogs.RetrieveMessageRequestType retrieveMessageRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ogs.wsapi.broadon.com/RetrieveMessage");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "RetrieveMessage"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {retrieveMessageRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ogs.RetrieveMessageResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ogs.RetrieveMessageResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ogs.RetrieveMessageResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ogs.SearchGameSessionsResponseType searchGameSessions(com.broadon.wsapi.ogs.SearchGameSessionsRequestType searchGameSessionsRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[13]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ogs.wsapi.broadon.com/SearchGameSessions");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "SearchGameSessions"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {searchGameSessionsRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ogs.SearchGameSessionsResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ogs.SearchGameSessionsResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ogs.SearchGameSessionsResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ogs.StoreMessageResponseType storeMessage(com.broadon.wsapi.ogs.StoreMessageRequestType storeMessageRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[14]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ogs.wsapi.broadon.com/StoreMessage");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "StoreMessage"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {storeMessageRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ogs.StoreMessageResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ogs.StoreMessageResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ogs.StoreMessageResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ogs.SubmitGameScoresResponseType submitGameScores(com.broadon.wsapi.ogs.SubmitGameScoresRequestType submitGameScoresRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[15]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ogs.wsapi.broadon.com/SubmitGameScores");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "SubmitGameScores"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {submitGameScoresRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ogs.SubmitGameScoresResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ogs.SubmitGameScoresResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ogs.SubmitGameScoresResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ogs.SubmitGameScoreObjectResponseType submitGameScoreObject(com.broadon.wsapi.ogs.SubmitGameScoreObjectRequestType submitGameScoreObjectRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[16]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ogs.wsapi.broadon.com/SubmitGameScoreObject");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "SubmitGameScoreObject"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {submitGameScoreObjectRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ogs.SubmitGameScoreObjectResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ogs.SubmitGameScoreObjectResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ogs.SubmitGameScoreObjectResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ogs.ExtendGameSessionResponseType extendGameSession(com.broadon.wsapi.ogs.ExtendGameSessionRequestType extendGameSessionRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[17]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ogs.wsapi.broadon.com/ExtendGameSession");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "ExtendGameSession"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {extendGameSessionRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ogs.ExtendGameSessionResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ogs.ExtendGameSessionResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ogs.ExtendGameSessionResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ogs.UnregisterGameSessionResponseType unregisterGameSession(com.broadon.wsapi.ogs.UnregisterGameSessionRequestType unregisterGameSessionRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[18]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ogs.wsapi.broadon.com/UnregisterGameSession");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "UnregisterGameSession"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {unregisterGameSessionRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ogs.UnregisterGameSessionResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ogs.UnregisterGameSessionResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ogs.UnregisterGameSessionResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ogs.RegisterResponseType register(com.broadon.wsapi.ogs.RegisterRequestType registerRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[19]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ogs.wsapi.broadon.com/Register");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "Register"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {registerRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ogs.RegisterResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ogs.RegisterResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ogs.RegisterResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ogs.LoginResponseType login(com.broadon.wsapi.ogs.LoginRequestType loginRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[20]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ogs.wsapi.broadon.com/Login");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "Login"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {loginRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ogs.LoginResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ogs.LoginResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ogs.LoginResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ogs.SetPasswordResponseType setPassword(com.broadon.wsapi.ogs.SetPasswordRequestType setPasswordRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[21]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ogs.wsapi.broadon.com/SetPassword");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "SetPassword"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {setPasswordRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ogs.SetPasswordResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ogs.SetPasswordResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ogs.SetPasswordResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ogs.UpdateUserInfoResponseType updateUserInfo(com.broadon.wsapi.ogs.UpdateUserInfoRequestType updateUserInfoRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[22]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ogs.wsapi.broadon.com/UpdateUserInfo");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "UpdateUserInfo"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {updateUserInfoRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ogs.UpdateUserInfoResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ogs.UpdateUserInfoResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ogs.UpdateUserInfoResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ogs.GetUserInfoResponseType getUserInfo(com.broadon.wsapi.ogs.GetUserInfoRequestType getUserInfoRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[23]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ogs.wsapi.broadon.com/GetUserInfo");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetUserInfo"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getUserInfoRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ogs.GetUserInfoResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ogs.GetUserInfoResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ogs.GetUserInfoResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ogs.GetNetIdRangeResponseType getNetIdRange(com.broadon.wsapi.ogs.GetNetIdRangeRequestType getNetIdRangeRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[24]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ogs.wsapi.broadon.com/GetNetIdRange");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetNetIdRange"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getNetIdRangeRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ogs.GetNetIdRangeResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ogs.GetNetIdRangeResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ogs.GetNetIdRangeResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ogs.GetAccountIdResponseType getAccountId(com.broadon.wsapi.ogs.GetAccountIdRequestType getAccountIdRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[25]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ogs.wsapi.broadon.com/GetAccountId");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetAccountId"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getAccountIdRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ogs.GetAccountIdResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ogs.GetAccountIdResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ogs.GetAccountIdResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}

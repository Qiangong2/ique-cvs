/**
 * AttrPredicateType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ogs;

public class AttrPredicateType  implements java.io.Serializable {
    private int attrId;
    private long attrValue;
    private com.broadon.wsapi.ogs.CmpOperatorType cmpOperator;

    public AttrPredicateType() {
    }

    public AttrPredicateType(
           int attrId,
           long attrValue,
           com.broadon.wsapi.ogs.CmpOperatorType cmpOperator) {
           this.attrId = attrId;
           this.attrValue = attrValue;
           this.cmpOperator = cmpOperator;
    }


    /**
     * Gets the attrId value for this AttrPredicateType.
     * 
     * @return attrId
     */
    public int getAttrId() {
        return attrId;
    }


    /**
     * Sets the attrId value for this AttrPredicateType.
     * 
     * @param attrId
     */
    public void setAttrId(int attrId) {
        this.attrId = attrId;
    }


    /**
     * Gets the attrValue value for this AttrPredicateType.
     * 
     * @return attrValue
     */
    public long getAttrValue() {
        return attrValue;
    }


    /**
     * Sets the attrValue value for this AttrPredicateType.
     * 
     * @param attrValue
     */
    public void setAttrValue(long attrValue) {
        this.attrValue = attrValue;
    }


    /**
     * Gets the cmpOperator value for this AttrPredicateType.
     * 
     * @return cmpOperator
     */
    public com.broadon.wsapi.ogs.CmpOperatorType getCmpOperator() {
        return cmpOperator;
    }


    /**
     * Sets the cmpOperator value for this AttrPredicateType.
     * 
     * @param cmpOperator
     */
    public void setCmpOperator(com.broadon.wsapi.ogs.CmpOperatorType cmpOperator) {
        this.cmpOperator = cmpOperator;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AttrPredicateType)) return false;
        AttrPredicateType other = (AttrPredicateType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.attrId == other.getAttrId() &&
            this.attrValue == other.getAttrValue() &&
            ((this.cmpOperator==null && other.getCmpOperator()==null) || 
             (this.cmpOperator!=null &&
              this.cmpOperator.equals(other.getCmpOperator())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getAttrId();
        _hashCode += new Long(getAttrValue()).hashCode();
        if (getCmpOperator() != null) {
            _hashCode += getCmpOperator().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AttrPredicateType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "AttrPredicateType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attrId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "AttrId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attrValue");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "AttrValue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cmpOperator");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "CmpOperator"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "CmpOperatorType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

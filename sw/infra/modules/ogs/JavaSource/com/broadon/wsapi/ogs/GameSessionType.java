/**
 * GameSessionType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ogs;

public class GameSessionType  implements java.io.Serializable {
    private int accountId;
    private long deviceId;
    private int netId;
    private int gameId;
    private long titleId;
    private int accessControl;
    private int totalSlots;
    private int friendSlots;
    private java.lang.Long endDate;
    private int netZone;
    private java.lang.Integer maxLatency;
    private int[] GA;
    private java.lang.String keyword;
    private java.lang.String comments;
    private java.lang.Long createDate;
    private java.lang.Long lastUpdated;

    public GameSessionType() {
    }

    public GameSessionType(
           int accountId,
           long deviceId,
           int netId,
           int gameId,
           long titleId,
           int accessControl,
           int totalSlots,
           int friendSlots,
           java.lang.Long endDate,
           int netZone,
           java.lang.Integer maxLatency,
           int[] GA,
           java.lang.String keyword,
           java.lang.String comments,
           java.lang.Long createDate,
           java.lang.Long lastUpdated) {
           this.accountId = accountId;
           this.deviceId = deviceId;
           this.netId = netId;
           this.gameId = gameId;
           this.titleId = titleId;
           this.accessControl = accessControl;
           this.totalSlots = totalSlots;
           this.friendSlots = friendSlots;
           this.endDate = endDate;
           this.netZone = netZone;
           this.maxLatency = maxLatency;
           this.GA = GA;
           this.keyword = keyword;
           this.comments = comments;
           this.createDate = createDate;
           this.lastUpdated = lastUpdated;
    }


    /**
     * Gets the accountId value for this GameSessionType.
     * 
     * @return accountId
     */
    public int getAccountId() {
        return accountId;
    }


    /**
     * Sets the accountId value for this GameSessionType.
     * 
     * @param accountId
     */
    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }


    /**
     * Gets the deviceId value for this GameSessionType.
     * 
     * @return deviceId
     */
    public long getDeviceId() {
        return deviceId;
    }


    /**
     * Sets the deviceId value for this GameSessionType.
     * 
     * @param deviceId
     */
    public void setDeviceId(long deviceId) {
        this.deviceId = deviceId;
    }


    /**
     * Gets the netId value for this GameSessionType.
     * 
     * @return netId
     */
    public int getNetId() {
        return netId;
    }


    /**
     * Sets the netId value for this GameSessionType.
     * 
     * @param netId
     */
    public void setNetId(int netId) {
        this.netId = netId;
    }


    /**
     * Gets the gameId value for this GameSessionType.
     * 
     * @return gameId
     */
    public int getGameId() {
        return gameId;
    }


    /**
     * Sets the gameId value for this GameSessionType.
     * 
     * @param gameId
     */
    public void setGameId(int gameId) {
        this.gameId = gameId;
    }


    /**
     * Gets the titleId value for this GameSessionType.
     * 
     * @return titleId
     */
    public long getTitleId() {
        return titleId;
    }


    /**
     * Sets the titleId value for this GameSessionType.
     * 
     * @param titleId
     */
    public void setTitleId(long titleId) {
        this.titleId = titleId;
    }


    /**
     * Gets the accessControl value for this GameSessionType.
     * 
     * @return accessControl
     */
    public int getAccessControl() {
        return accessControl;
    }


    /**
     * Sets the accessControl value for this GameSessionType.
     * 
     * @param accessControl
     */
    public void setAccessControl(int accessControl) {
        this.accessControl = accessControl;
    }


    /**
     * Gets the totalSlots value for this GameSessionType.
     * 
     * @return totalSlots
     */
    public int getTotalSlots() {
        return totalSlots;
    }


    /**
     * Sets the totalSlots value for this GameSessionType.
     * 
     * @param totalSlots
     */
    public void setTotalSlots(int totalSlots) {
        this.totalSlots = totalSlots;
    }


    /**
     * Gets the friendSlots value for this GameSessionType.
     * 
     * @return friendSlots
     */
    public int getFriendSlots() {
        return friendSlots;
    }


    /**
     * Sets the friendSlots value for this GameSessionType.
     * 
     * @param friendSlots
     */
    public void setFriendSlots(int friendSlots) {
        this.friendSlots = friendSlots;
    }


    /**
     * Gets the endDate value for this GameSessionType.
     * 
     * @return endDate
     */
    public java.lang.Long getEndDate() {
        return endDate;
    }


    /**
     * Sets the endDate value for this GameSessionType.
     * 
     * @param endDate
     */
    public void setEndDate(java.lang.Long endDate) {
        this.endDate = endDate;
    }


    /**
     * Gets the netZone value for this GameSessionType.
     * 
     * @return netZone
     */
    public int getNetZone() {
        return netZone;
    }


    /**
     * Sets the netZone value for this GameSessionType.
     * 
     * @param netZone
     */
    public void setNetZone(int netZone) {
        this.netZone = netZone;
    }


    /**
     * Gets the maxLatency value for this GameSessionType.
     * 
     * @return maxLatency
     */
    public java.lang.Integer getMaxLatency() {
        return maxLatency;
    }


    /**
     * Sets the maxLatency value for this GameSessionType.
     * 
     * @param maxLatency
     */
    public void setMaxLatency(java.lang.Integer maxLatency) {
        this.maxLatency = maxLatency;
    }


    /**
     * Gets the GA value for this GameSessionType.
     * 
     * @return GA
     */
    public int[] getGA() {
        return GA;
    }


    /**
     * Sets the GA value for this GameSessionType.
     * 
     * @param GA
     */
    public void setGA(int[] GA) {
        this.GA = GA;
    }

    public int getGA(int i) {
        return this.GA[i];
    }

    public void setGA(int i, int _value) {
        this.GA[i] = _value;
    }


    /**
     * Gets the keyword value for this GameSessionType.
     * 
     * @return keyword
     */
    public java.lang.String getKeyword() {
        return keyword;
    }


    /**
     * Sets the keyword value for this GameSessionType.
     * 
     * @param keyword
     */
    public void setKeyword(java.lang.String keyword) {
        this.keyword = keyword;
    }


    /**
     * Gets the comments value for this GameSessionType.
     * 
     * @return comments
     */
    public java.lang.String getComments() {
        return comments;
    }


    /**
     * Sets the comments value for this GameSessionType.
     * 
     * @param comments
     */
    public void setComments(java.lang.String comments) {
        this.comments = comments;
    }


    /**
     * Gets the createDate value for this GameSessionType.
     * 
     * @return createDate
     */
    public java.lang.Long getCreateDate() {
        return createDate;
    }


    /**
     * Sets the createDate value for this GameSessionType.
     * 
     * @param createDate
     */
    public void setCreateDate(java.lang.Long createDate) {
        this.createDate = createDate;
    }


    /**
     * Gets the lastUpdated value for this GameSessionType.
     * 
     * @return lastUpdated
     */
    public java.lang.Long getLastUpdated() {
        return lastUpdated;
    }


    /**
     * Sets the lastUpdated value for this GameSessionType.
     * 
     * @param lastUpdated
     */
    public void setLastUpdated(java.lang.Long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GameSessionType)) return false;
        GameSessionType other = (GameSessionType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.accountId == other.getAccountId() &&
            this.deviceId == other.getDeviceId() &&
            this.netId == other.getNetId() &&
            this.gameId == other.getGameId() &&
            this.titleId == other.getTitleId() &&
            this.accessControl == other.getAccessControl() &&
            this.totalSlots == other.getTotalSlots() &&
            this.friendSlots == other.getFriendSlots() &&
            ((this.endDate==null && other.getEndDate()==null) || 
             (this.endDate!=null &&
              this.endDate.equals(other.getEndDate()))) &&
            this.netZone == other.getNetZone() &&
            ((this.maxLatency==null && other.getMaxLatency()==null) || 
             (this.maxLatency!=null &&
              this.maxLatency.equals(other.getMaxLatency()))) &&
            ((this.GA==null && other.getGA()==null) || 
             (this.GA!=null &&
              java.util.Arrays.equals(this.GA, other.getGA()))) &&
            ((this.keyword==null && other.getKeyword()==null) || 
             (this.keyword!=null &&
              this.keyword.equals(other.getKeyword()))) &&
            ((this.comments==null && other.getComments()==null) || 
             (this.comments!=null &&
              this.comments.equals(other.getComments()))) &&
            ((this.createDate==null && other.getCreateDate()==null) || 
             (this.createDate!=null &&
              this.createDate.equals(other.getCreateDate()))) &&
            ((this.lastUpdated==null && other.getLastUpdated()==null) || 
             (this.lastUpdated!=null &&
              this.lastUpdated.equals(other.getLastUpdated())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getAccountId();
        _hashCode += new Long(getDeviceId()).hashCode();
        _hashCode += getNetId();
        _hashCode += getGameId();
        _hashCode += new Long(getTitleId()).hashCode();
        _hashCode += getAccessControl();
        _hashCode += getTotalSlots();
        _hashCode += getFriendSlots();
        if (getEndDate() != null) {
            _hashCode += getEndDate().hashCode();
        }
        _hashCode += getNetZone();
        if (getMaxLatency() != null) {
            _hashCode += getMaxLatency().hashCode();
        }
        if (getGA() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGA());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGA(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getKeyword() != null) {
            _hashCode += getKeyword().hashCode();
        }
        if (getComments() != null) {
            _hashCode += getComments().hashCode();
        }
        if (getCreateDate() != null) {
            _hashCode += getCreateDate().hashCode();
        }
        if (getLastUpdated() != null) {
            _hashCode += getLastUpdated().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GameSessionType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GameSessionType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "AccountId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "AccountIdType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deviceId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "DeviceId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("netId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "NetId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gameId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GameId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titleId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "TitleId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accessControl");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "AccessControl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalSlots");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "TotalSlots"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("friendSlots");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "FriendSlots"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "EndDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("netZone");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "NetZone"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maxLatency");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "MaxLatency"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("GA");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("keyword");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "Keyword"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comments");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "Comments"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "CreateDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastUpdated");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "LastUpdated"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

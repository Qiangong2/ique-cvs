/**
 * NetUpdateSOAPBindingSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.nus;

import javax.xml.rpc.ServiceException;
import javax.xml.rpc.server.ServiceLifecycle;


public class NetUpdateSOAPBindingSkeleton implements ServiceLifecycle, com.broadon.wsapi.nus.NetUpdatePortType, org.apache.axis.wsdl.Skeleton {
    private com.broadon.wsapi.nus.NetUpdatePortType impl;
    private static java.util.Map _myOperations = new java.util.Hashtable();
    private static java.util.Collection _myOperationsList = new java.util.ArrayList();

    /**
    * Returns List of OperationDesc objects with this name
    */
    public static java.util.List getOperationDescByName(java.lang.String methodName) {
        return (java.util.List)_myOperations.get(methodName);
    }

    /**
    * Returns Collection of OperationDescs
    */
    public static java.util.Collection getOperationDescs() {
        return _myOperationsList;
    }

    static {
        org.apache.axis.description.OperationDesc _oper;
        org.apache.axis.description.FaultDesc _fault;
        org.apache.axis.description.ParameterDesc [] _params;
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:nus.wsapi.broadon.com", "GetSystemUpdateRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:nus.wsapi.broadon.com", "GetSystemUpdateRequestType"), com.broadon.wsapi.nus.GetSystemUpdateRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getSystemUpdate", _params, new javax.xml.namespace.QName("urn:nus.wsapi.broadon.com", "GetSystemUpdateResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:nus.wsapi.broadon.com", "GetSystemUpdateResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "GetSystemUpdate"));
        _oper.setSoapAction("urn:nus.wsapi.broadon.com/GetSystemUpdate");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getSystemUpdate") == null) {
            _myOperations.put("getSystemUpdate", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getSystemUpdate")).add(_oper);
    }

    public NetUpdateSOAPBindingSkeleton() {
        this.impl = new com.broadon.wsapi.nus.NetUpdateSOAPBindingImpl();
    }

    public NetUpdateSOAPBindingSkeleton(com.broadon.wsapi.nus.NetUpdatePortType impl) {
        this.impl = impl;
    }
    public com.broadon.wsapi.nus.GetSystemUpdateResponseType getSystemUpdate(com.broadon.wsapi.nus.GetSystemUpdateRequestType getSystemUpdateRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.nus.GetSystemUpdateResponseType ret = impl.getSystemUpdate(getSystemUpdateRequest);
        return ret;
    }
    
    public void init(Object context) throws ServiceException
    {
        if (impl instanceof ServiceLifecycle) {
            ((ServiceLifecycle) impl).init(context);
        }
    }

    public void destroy()
    {
        if (impl instanceof ServiceLifecycle) {
            ((ServiceLifecycle) impl).destroy();
        }
    }
}

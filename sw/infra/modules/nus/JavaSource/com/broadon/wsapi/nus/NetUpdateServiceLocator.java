/**
 * NetUpdateServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.nus;

public class NetUpdateServiceLocator extends org.apache.axis.client.Service implements com.broadon.wsapi.nus.NetUpdateService {

    public NetUpdateServiceLocator() {
    }


    public NetUpdateServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public NetUpdateServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for NetUpdateSOAP
    private java.lang.String NetUpdateSOAP_address = "http://nus:16981/nus/services/NetUpdateSOAP";

    public java.lang.String getNetUpdateSOAPAddress() {
        return NetUpdateSOAP_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String NetUpdateSOAPWSDDServiceName = "NetUpdateSOAP";

    public java.lang.String getNetUpdateSOAPWSDDServiceName() {
        return NetUpdateSOAPWSDDServiceName;
    }

    public void setNetUpdateSOAPWSDDServiceName(java.lang.String name) {
        NetUpdateSOAPWSDDServiceName = name;
    }

    public com.broadon.wsapi.nus.NetUpdatePortType getNetUpdateSOAP() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(NetUpdateSOAP_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getNetUpdateSOAP(endpoint);
    }

    public com.broadon.wsapi.nus.NetUpdatePortType getNetUpdateSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.broadon.wsapi.nus.NetUpdateSOAPBindingStub _stub = new com.broadon.wsapi.nus.NetUpdateSOAPBindingStub(portAddress, this);
            _stub.setPortName(getNetUpdateSOAPWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setNetUpdateSOAPEndpointAddress(java.lang.String address) {
        NetUpdateSOAP_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.broadon.wsapi.nus.NetUpdatePortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.broadon.wsapi.nus.NetUpdateSOAPBindingStub _stub = new com.broadon.wsapi.nus.NetUpdateSOAPBindingStub(new java.net.URL(NetUpdateSOAP_address), this);
                _stub.setPortName(getNetUpdateSOAPWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("NetUpdateSOAP".equals(inputPortName)) {
            return getNetUpdateSOAP();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("urn:nus.wsapi.broadon.com", "NetUpdateService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("urn:nus.wsapi.broadon.com", "NetUpdateSOAP"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("NetUpdateSOAP".equals(portName)) {
            setNetUpdateSOAPEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}

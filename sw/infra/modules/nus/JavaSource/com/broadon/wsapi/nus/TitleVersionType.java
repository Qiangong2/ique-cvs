/**
 * TitleVersionType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.nus;


/**
 * Basic information about a title that is used for title update.
 */
public class TitleVersionType  implements java.io.Serializable {
    private java.lang.String titleId;
    private int version;
    /** Optional. Approximate size the title will take on the device's
 * file system (sum of the content sizes, each
 *             rounded up to the block size) */
    private java.lang.Long fsSize;

    public TitleVersionType() {
    }

    public TitleVersionType(
           java.lang.String titleId,
           int version,
           java.lang.Long fsSize) {
           this.titleId = titleId;
           this.version = version;
           this.fsSize = fsSize;
    }


    /**
     * Gets the titleId value for this TitleVersionType.
     * 
     * @return titleId
     */
    public java.lang.String getTitleId() {
        return titleId;
    }


    /**
     * Sets the titleId value for this TitleVersionType.
     * 
     * @param titleId
     */
    public void setTitleId(java.lang.String titleId) {
        this.titleId = titleId;
    }


    /**
     * Gets the version value for this TitleVersionType.
     * 
     * @return version
     */
    public int getVersion() {
        return version;
    }


    /**
     * Sets the version value for this TitleVersionType.
     * 
     * @param version
     */
    public void setVersion(int version) {
        this.version = version;
    }


    /**
     * Gets the fsSize value for this TitleVersionType.
     * 
     * @return fsSize Optional. Approximate size the title will take on the device's
 * file system (sum of the content sizes, each
 *             rounded up to the block size)
     */
    public java.lang.Long getFsSize() {
        return fsSize;
    }


    /**
     * Sets the fsSize value for this TitleVersionType.
     * 
     * @param fsSize Optional. Approximate size the title will take on the device's
 * file system (sum of the content sizes, each
 *             rounded up to the block size)
     */
    public void setFsSize(java.lang.Long fsSize) {
        this.fsSize = fsSize;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitleVersionType)) return false;
        TitleVersionType other = (TitleVersionType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.titleId==null && other.getTitleId()==null) || 
             (this.titleId!=null &&
              this.titleId.equals(other.getTitleId()))) &&
            this.version == other.getVersion() &&
            ((this.fsSize==null && other.getFsSize()==null) || 
             (this.fsSize!=null &&
              this.fsSize.equals(other.getFsSize())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTitleId() != null) {
            _hashCode += getTitleId().hashCode();
        }
        _hashCode += getVersion();
        if (getFsSize() != null) {
            _hashCode += getFsSize().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitleVersionType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:nus.wsapi.broadon.com", "TitleVersionType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titleId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:nus.wsapi.broadon.com", "TitleId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:nus.wsapi.broadon.com", "TitleIdType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("version");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:nus.wsapi.broadon.com", "Version"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fsSize");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:nus.wsapi.broadon.com", "FsSize"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

package com.broadon.test.cds;

import java.io.StringWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;

import com.broadon.bean.Bean;
import com.broadon.bean.XMLFormatWriter;
import com.broadon.cds.sync.ContentSync;
import com.broadon.db.DBAccessFactory;
import com.broadon.test.NameMap;
import com.broadon.test.OracleDS;
import com.broadon.util.Timer;

/**
 * The <c>MTTest</c> class provides mutli-thread testing of the content
 * sync functionalities, provided by the <c>ContentSync</c> class.
 *
 * @version	$Revision: 1.5 $
 */
public class MTTest
{
    private static MTTest	mtTest = new MTTest(4);

    /**
     * Uses multiple threads to test the concurrent aspect of content
     * sync functionality.
     *
     * @param	args
     *			args[0]		the number of time to loop [100]
     *			args[1]		the number of thread to use [4]
     */
    public static final void main(String[] args)
	throws Throwable
    {
	int	loops = 100;

	if (args.length > 0)
	{
	    loops = Integer.parseInt(args[0]);
	    if (args.length > 1)
	    {
		mtTest.threads = Integer.parseInt(args[1]);
	    }
	}
	System.err.println("loop[" + loops + "] n[" + mtTest.threads + "]");
	/*
	 * Connect to database.
	 */
	DataSource	ds = OracleDS.createDataSource();
	/*
	 * Create the threads.
	 */
	Map		nameMap = NameMap.getNameMap();
	Date		from = new Date(0);
	Date		to = new Date(new Date().getTime() + 86400000);
	Timer		timer = new Timer();
	Reader[]	readers = new Reader[mtTest.threads];

	for (int n = 0; n < mtTest.threads; n++)
	{
	    readers[n] = mtTest.new Reader(ds, nameMap, from, to, loops);
	}
	/*
	 * Start the thread, i.e., start the concurrent testing.
	 */
	timer.start();
	for (int n = 0; n < mtTest.threads; n++)
	{
	    readers[n].start();
	}
	/*
	 * Wait for them to finish.
	 */
	for (int n = 0; n < mtTest.threads; n++)
	{
	    readers[n].join();
	}
	timer.stop();
	/*
	 * Show results.
	 */
	for (int n = 0; n < mtTest.threads; n++)
	{
	    System.out.println("Reader " + (n + 1) + ": " + readers[n]);
	}
	System.out.println("Running " +
			   mtTest.threads +
			   " each loops " +
			   loops +
			   " times take: " +
			   timer);
    }

    private int		threads;

    /**
     * Constructs a new MTTest instance.
     *
     * @param	threads			the number of threads to use
     */
    public MTTest(int threads)
    {
	this.threads = threads;
    }

    /**
     * The <c>Reader</c> class tests the content sync functionalities.
     */
    private class Reader
	extends Thread
    {
	private Timer		timer;
	private StringWriter	out;
	private ContentSync	contentSync;
	private Date		from;
	private Date		to;
	private int		loops;

	/**
	 * Constructs a new Reader instance.
	 *
	 * @param	nameMap		the name mapping table
	 * @param	from		the starting time interval
	 * @param	to		the ending time interval
	 * @param	loops		the number of times to loop
	 */
	private Reader(DataSource dataSource,
		       Map nameMap,
		       Date from,
		       Date to,
		       int loops)
	{
	    try
	    {
		/*
		 * Initialize.
		 */
		this.timer = new Timer();
		this.out = new StringWriter();
		this.contentSync = new ContentSync();
		contentSync.setDataSource(dataSource);
		contentSync.setNameMap(nameMap);
		this.from = from;
		this.to = to;
		this.loops = loops;
	    }
	    catch (Throwable t)
	    {
		t.printStackTrace();
	    }
	}

	/**
	 * Cleans up.
	 */
	protected void finalize()
	    throws Throwable
	{
	    super.finalize();
	    if (out != null)
	    {
		out.close();
		out = null;
	    }
	}

	/**
	 * The main method of this thread.
	 */
	public void run()
	{
	    timer.start();
	    try
	    {
		CSSTest.loop(contentSync, from, to, out, loops);
	    }
	    catch (Throwable t)
	    {
		System.err.println("Thread[" + this + "]");
		t.printStackTrace();
	    }
	    finally
	    {
		timer.stop();
	    }
	}

	/**
	 * Returns the string representation of this Reader.
	 *
	 * @return	the string representation.
	 */
	public String toString()
	{
	    return "Finsih[" + getName() + "] in " + timer.getTime() + "ms\n" +
		   out.toString();
	}
    }
}

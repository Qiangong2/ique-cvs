package com.broadon.test.cds;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;

import com.broadon.cds.download.ContentDownload;
import com.broadon.test.NameMap;
import com.broadon.test.OracleDS;
import com.broadon.util.Timer;

/**
 * The <c>MTTest</c> class provides mutli-thread testing of the content
 * download functionalities, provided by the <c>ContentDownload</c> class.
 *
 * @version	$Revision: 1.3 $
 */
public class MTDTest
{
    private static MTDTest	mtdTest = new MTDTest(4);

    /**
     * Uses multiple threads to test the concurrent aspect of content
     * sync functionality.
     *
     * @param	args
     *			args[0]		the content ID
     *			args[1]		the number of time to loop [100]
     *			args[2]		the number of thread to use [4]
     */
    public static final void main(String[] args)
	throws Throwable
    {
	int	loops = 100;
	long	contentID = Long.parseLong(args[0]);
	String	file = "test-contents";

	if (args.length > 1)
	{
	    loops = Integer.parseInt(args[1]);
	    if (args.length > 2)
	    {
		mtdTest.threads = Integer.parseInt(args[2]);
		if (args.length > 3)
		    file = args[3];
	    }
	}
	System.err.print("loop[" + loops + "] n[" + mtdTest.threads + "]");
	System.err.println(" content[" + contentID + "]");
	/*
	 * Connect to Oracle.
	 */
	String		url = "jdbc:oracle:thin:@lab1-db2:1521:bbdb2";
	DataSource	ds = OracleDS.createDataSource(url, "xs", "xs");
	/*
	 * Create the threads.
	 */
	Map		nameMap = NameMap.getNameMap();
	Timer		timer = new Timer();
	Reader[]	readers = new Reader[mtdTest.threads];

	for (int n = 0; n < mtdTest.threads; n++)
	{
	    readers[n] = mtdTest.new Reader(ds,
					    nameMap,
					    contentID,
					    file + "-" + n,
					    loops);
	}
	/*
	 * Start the thread, i.e., start the concurrent testing.
	 */
	timer.start();
	for (int n = 0; n < mtdTest.threads; n++)
	{
	    readers[n].start();
	}
	/*
	 * Wait for them to finish.
	 */
	for (int n = 0; n < mtdTest.threads; n++)
	{
	    readers[n].join();
	}
	timer.stop();
	/*
	 * Show results.
	 */
	for (int n = 0; n < mtdTest.threads; n++)
	{
	    System.err.println("Reader " + (n + 1) + ": " + readers[n]);
	}
	System.err.println("Running " +
			   mtdTest.threads +
			   " each loops " +
			   loops +
			   " times take: " +
			   timer);
    }

    private int		threads;

    /**
     * Constructs a new MTDTest instance.
     *
     * @param	threads			the number of threads to use
     */
    public MTDTest(int threads)
    {
	this.threads = threads;
    }

    /**
     * The <c>Reader</c> class tests the content sync functionalities.
     */
    private class Reader
	extends Thread
    {
	private Timer		timer;
	private OutputStream	out;
	private ContentDownload	contentDownload;
	private long		contentID;
	private int		loops;

	/**
	 * Constructs a new Reader instance.
	 *
	 * @param	nameMap		the name mapping table
	 * @param	contentID	the content ID
	 * @param	loops		the number of times to loop
	 */
	private Reader(DataSource dataSource,
		       Map nameMap,
		       long contentID,
		       String file,
		       int loops)
	{
	    try
	    {
		/*
		 * Initialize.
		 */
		this.timer = new Timer();
		this.contentDownload = new ContentDownload("cache",
							   16384,
							   true);
		contentDownload.setDataSource(dataSource);
		contentDownload.setNameMap(nameMap);
		this.contentID = contentID;
		this.out = new FileOutputStream(file);
		this.loops = loops;
	    }
	    catch (Throwable t)
	    {
		t.printStackTrace();
	    }
	}

	/**
	 * Cleans up.
	 */
	protected void finalize()
	    throws Throwable
	{
	    super.finalize();
	    if (out != null)
	    {
		out.close();
		out = null;
	    }
	}

	/**
	 * The main method of this thread.
	 */
	public void run()
	{
	    timer.start();
	    try
	    {
		CDSTest.loop(contentDownload, contentID, out, false, loops);
	    }
	    catch (Throwable t)
	    {
		System.err.println("Thread[" + this + "]");
		t.printStackTrace();
	    }
	    finally
	    {
		timer.stop();
	    }
	}

	/**
	 * Returns the string representation of this Reader.
	 *
	 * @return	the string representation.
	 */
	public String toString()
	{
	    return "Finsih[" + getName() + "] in " + timer.getTime() + "ms\n";
	}
    }
}

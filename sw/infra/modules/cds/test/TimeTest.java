package com.broadon.test.cds;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;

import oracle.jdbc.pool.OracleConnectionCacheImpl;

import com.broadon.bean.Bean;
import com.broadon.bean.XMLFormatWriter;
import com.broadon.db.DBAccessFactory;
import com.broadon.db.DBException;
import com.broadon.db.Time;
import com.broadon.db.TimeFactory;
import com.broadon.test.NameMap;

/**
 * The <code>TimeTest</code> class tests the Time class.
 * The data are retrieved from the database.
 *
 * @version	$Revision: 1.1 $
 */
public class TimeTest
{
    /**
     * Tests the PBean instances.
     */
    public static final void main(String[] args)
	throws Throwable
    {
	/*
	 * Connect to Oracle.
	 */
	OracleConnectionCacheImpl	ds;

	ds = new OracleConnectionCacheImpl();
	ds.setDriverType("thin");
	ds.setServerName("lab1-db2");
	ds.setNetworkProtocol("tcp");
	ds.setDatabaseName("bbdb2");
	ds.setPortNumber(1521);
	ds.setUser("xs");
	ds.setPassword("xs");
	/*
	 * Start testing.
	 */
	Class		factoryClass = TimeFactory.class;
	String		factoryName = factoryClass.getName();
	Method		singleton = factoryClass.getMethod(
						"getInstance",
						new Class[] { String.class });
	TimeFactory	factory;

	factory = (TimeFactory)singleton.invoke(
						null,
						new String[] { factoryName });
	System.out.println(factory);

	Map		nameMap = NameMap.getNameMap();
	String		xml;
	Time		time;

	factory.beginTransaction(ds, false, false);
	try
	{
	    time = factory.getTime(nameMap);
	    System.out.println("Database time = " + time.getSysdate());
	}
	finally
	{
	    factory.commitTransaction();
	}
    }
}

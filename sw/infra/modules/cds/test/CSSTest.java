package com.broadon.test.cds;

import java.io.PrintWriter;
import java.io.Writer;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;

import com.broadon.bean.Bean;
import com.broadon.bean.XMLFormatWriter;
import com.broadon.cds.sync.ContentSync;
import com.broadon.test.NameMap;
import com.broadon.test.OracleDS;
import com.broadon.util.Timer;

/**
 * The <c>CDSTest</c> class tests the content sync functionalities,
 * provided by the <c>ContentSync</c> class.
 *
 * @version	$Revision: 1.11 $
 */
public class CSSTest
{
    /**
     * Tests the content download functionalities.
     *
     * @param	args
     *			args[0]		the number of time to loop [10]
     *			args[1]		the database url
     */
    public static final void main(String[] args)
	throws Throwable
    {
	int	loops = 10;
	String	url = "jdbc:oracle:thin:@lab1-db2:1521:bbdb2";

	if (args.length > 0)
	{
	    loops = Integer.parseInt(args[0]);
	    if (args.length > 1)
		url = args[1];
	}
	/*
	 * Connect to Oracle.
	 */
	DataSource		ds = OracleDS.createDataSource(url,
							       "xs",
							       "xs");
	/*
	 * Start testing.
	 */
	Map			nameMap = NameMap.getNameMap();
	ContentSync		contentSync = new ContentSync();
	Date			from = new Date(0);
	Date			to = new Date(new Date().getTime() + 86400000);

	contentSync.setDataSource(ds);
	contentSync.setNameMap(nameMap);

	PrintWriter		out = new PrintWriter(System.out);
	Timer			timer = new Timer();

	timer.start();
	loop(contentSync, from, to, out, loops);
	timer.stop();
	System.err.println("Running " + loops + " times take: " + timer);
	out.close();
    }

    /**
     * Performs the task repeatedly. The number of times is specified
     * by the value of loops.
     *
     * @throws	Throwable
     */
    public static void loop(ContentSync contentSync,
			    Date from,
			    Date to,
			    PrintWriter out,
			    int loops)
	throws Throwable
    {
	Map			nameMap = contentSync.getNameMap();

	for (int m = 0; m < loops; m++)
	{
	    contentSync.beginTransaction();
	    try
	    {
		Bean[]		beans;
		XMLFormatWriter	formatter;
		String		rootTag;
		int		size;

		/*
		 * Get database time, in seconds since epoch.
		 */
		Date		currentTime = contentSync.getCurrentTime();

		out.print("  <db_systime>");
		out.print(currentTime.getTime() / 1000);
		out.println("</db_systime>");
		/*
		 * Content info, without the content.
		 */
		beans = contentSync.getContentInfo(from, to, null);
		if (beans != null)
		{
		    if ((m + 1) == loops)
		    {
			size = beans.length;
			for (int n = 0; n < size; n++)
			{
			    formatter = new XMLFormatWriter(beans[n]);
			    formatter.setNameMap(nameMap);
			    rootTag = contentSync.getStringID(
						ContentSync.CONTENT_INFO);
			    formatter.format(out, 1, rootTag);
			}
		    }
		}
		/*
		 * Titles.
		 */
		beans = contentSync.getTitles(from, to, null);
		if (beans != null)
		{
		    if ((m + 1) == loops)
		    {
			size = beans.length;
			for (int n = 0; n < size; n++)
			{
			    formatter = new XMLFormatWriter(beans[n]);
			    formatter.setNameMap(nameMap);
			    rootTag = contentSync.getStringID(
						ContentSync.TITLE);
			    formatter.format(out, 1, rootTag);
			}
		    }
		}
		/*
		 * Title content information.
		 */
		beans = contentSync.getTitleContents(from, to, null);
		if (beans != null)
		{
		    if ((m + 1) == loops)
		    {
			size = beans.length;
			for (int n = 0; n < size; n++)
			{
			    formatter = new XMLFormatWriter(beans[n]);
			    formatter.setNameMap(nameMap);
			    rootTag = contentSync.getStringID(
						ContentSync.TITLE_CONTENT);
			    formatter.format(out, 1, rootTag);
			}
		    }
		}
		/*
		 * Title reviews.
		 */
		beans = contentSync.getTitleReviews(from, to, null);
		if (beans != null)
		{
		    if ((m + 1) == loops)
		    {
			size = beans.length;
			for (int n = 0; n < size; n++)
			{
			    formatter = new XMLFormatWriter(beans[n]);
			    formatter.setNameMap(nameMap);
			    rootTag = contentSync.getStringID(
						ContentSync.TITLE_REVIEW);
			    formatter.format(out, 1, rootTag);
			}
		    }
		}
		/*
		 * Title regions.
		 */
		beans = contentSync.getTitleRegions(from, to, null);
		if (beans != null)
		{
		    if ((m + 1) == loops)
		    {
			size = beans.length;
			for (int n = 0; n < size; n++)
			{
			    formatter = new XMLFormatWriter(beans[n]);
			    formatter.setNameMap(nameMap);
			    rootTag = contentSync.getStringID(
						ContentSync.TITLE_REGION);
			    formatter.format(out, 1, rootTag);
			}
		    }
		}
		/*
		 * ECard types.
		 */
		beans = contentSync.getECardTypes(from, to, null);
		if (beans != null)
		{
		    if ((m + 1) == loops)
		    {
			size = beans.length;
			for (int n = 0; n < size; n++)
			{
			    formatter = new XMLFormatWriter(beans[n]);
			    formatter.setNameMap(nameMap);
			    rootTag = contentSync.getStringID(
						ContentSync.ECARD_TYPE);
			    formatter.format(out, 1, rootTag);
			}
		    }
		}
		/*
		 * ETicket CRL.
		 */
		beans = contentSync.getETicketCRL(from, to, null);
		if (beans != null)
		{
		    if ((m + 1) == loops)
		    {
			size = beans.length;
			for (int n = 0; n < size; n++)
			{
			    formatter = new XMLFormatWriter(beans[n]);
			    formatter.setNameMap(nameMap);
			    rootTag = contentSync.getStringID(
						ContentSync.ETICKET_CRL);
			    formatter.format(out, 1, rootTag);
			}
		    }
		}
		/*
		 * BB hardware releases.
		 */
		beans = contentSync.getBBHwReleases(from, to, null);
		if (beans != null)
		{
		    if ((m + 1) == loops)
		    {
			size = beans.length;
			for (int n = 0; n < size; n++)
			{
			    formatter = new XMLFormatWriter(beans[n]);
			    formatter.setNameMap(nameMap);
			    rootTag = contentSync.getStringID(
						ContentSync.BB_HW_RELEASE);
			    formatter.format(out, 1, rootTag);
			}
		    }
		}
		/*
		 * BB content types.
		 */
		beans = contentSync.getBBContentTypes(from, to, null);
		if (beans != null)
		{
		    if ((m + 1) == loops)
		    {
			size = beans.length;
			for (int n = 0; n < size; n++)
			{
			    formatter = new XMLFormatWriter(beans[n]);
			    formatter.setNameMap(nameMap);
			    rootTag = contentSync.getStringID(
						ContentSync.BB_CONTENT_TYPE);
			    formatter.format(out, 1, rootTag);
			}
		    }
		}
		/*
		 * Record counts.
		 */
		beans = contentSync.getRecordCounts(to);
		if (beans != null)
		{
		    if ((m + 1) == loops)
		    {
			size = beans.length;
			for (int n = 0; n < size; n++)
			{
			    formatter = new XMLFormatWriter(beans[n]);
			    formatter.setNameMap(nameMap);
			    formatter.format(out, 1, "record_count");
			}
		    }
		}
	    }
	    finally
	    {
		contentSync.commitTransaction();
	    }
	}
    }
}

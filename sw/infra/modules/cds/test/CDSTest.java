package com.broadon.test.cds;

import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import oracle.jdbc.pool.OracleConnectionCacheImpl;

import com.broadon.bean.Bean;
import com.broadon.bean.XMLFormatter;
import com.broadon.cds.download.ContentDownload;
import com.broadon.test.NameMap;
import com.broadon.util.Timer;

/**
 * The <c>CDSTest</c> class tests the content download functionalities,
 * provided by the <c>ContentDownload</c> class.
 *
 * @version	$Revision: 1.8 $
 */
public class CDSTest
{
    /**
     * Tests the content download functionalities.
     *
     * @param	args
     *			args[0]		the content identifier [2]
     *			args[1]		true => encode; false => as is [true]
     *			args[2]		the number of times to loop [1]
     */
    public static final void main(String[] args)
	throws Throwable
    {
	int	loops = 1;
	long	contentID = 2;
	boolean	encode = true;

	if (args.length > 0)
	{
	    contentID = Long.parseLong(args[0]);
	    if (args.length > 1)
	    {
		encode = Boolean.valueOf(args[1]).booleanValue();
		if (args.length > 2)
		{
		    loops = Integer.parseInt(args[2]);
		}
	    }
	}
	/*
	 * Connect to Oracle.
	 */
	OracleConnectionCacheImpl	ds;

	ds = new OracleConnectionCacheImpl();
	ds.setDriverType("thin");
	ds.setServerName("db1.beta-int.broadon.com");
	ds.setNetworkProtocol("tcp");
	ds.setDatabaseName("ibudb");
	ds.setPortNumber(1521);
	ds.setUser("xs");
	ds.setPassword("xs");
	/*
	 * Start testing.
	 */
	Map			nameMap = NameMap.getNameMap();
	String			path = "cache";
	ContentDownload		contentDownload = new ContentDownload(path,
								      16384,
								      true);
	Timer			timer = new Timer();

	contentDownload.setDataSource(ds);
	contentDownload.setNameMap(nameMap);

	System.err.println("Finding contentID[" + contentID + "]" +
			   " encode[" + encode + "]" +
			   " loops[" + loops + "]");

	timer.start();
	loop(contentDownload, contentID, System.out, encode, loops);
	timer.stop();
	System.err.println("Running " + loops + " times take: " + timer);
    }

    /**
     * Performs the task repeatedly. The number of times is specified
     * by the value of loops.
     *
     * @throws	Throwable
     */
    public static void loop(ContentDownload contentDownload,
			    long contentID,
			    OutputStream out,
			    boolean encode,
			    int loops)
	throws Throwable
    {
	for (int n = 0; n < loops; n++)
	{
	    contentDownload.getContent(contentID, 0, out, encode);
	}
    }
}

INFRA_BASE := $(shell while [ ! -e Makefile.setup ]; do cd .. ; done; pwd)
include $(INFRA_BASE)/Makefile.setup

LIB_SUBDIRS =		\
	exception	\
	util		\
	xml		\
	bean		\
	sql		\
	db		\
	servlet		\
	status		\
	filter		\
	security	\
	hsm		\
	hsmserver	\
	auditlog	\
	online		\
	jni		\
	syslog

PACKAGES = $(patsubst %,com.broadon.%,$(LIB_SUBDIRS))
LIB_PATHS = $(patsubst %,$(MODULE_JAVASRC)/com/broadon/%,$(LIB_SUBDIRS))
JAVADOC_DIR = $(MODULE_DOC)/javalib

NFJAVA_LIB = $(IMPORT_LIB)/nfjava.jar
KMJAVA_LIB = $(IMPORT_LIB)/kmjava.jar
XML_PARSER_LIB = $(IMPORT_LIB)/xmlparserv2.jar
XSU_LIB = $(IMPORT_LIB)/xsu12.jar
LOG4J_LIB = $(IMPORT_LIB)/log4j-1.2.8.jar
XDB_LIB = $(IMPORT_LIB)/xdb.jar
MAIL_LIB = $(IMPORT_LIB)/mail.jar
ACTIVATION_LIB = $(IMPORT_LIB)/activation.jar
CLASSPATH := $(CLASSPATH):$(NFJAVA_LIB):$(KMJAVA_LIB):$(XML_PARSER_LIB):$(XSU_LIB):$(LOG4J_LIB):$(XDB_LIB):$(MAIL_LIB):$(ACTIVATION_LIB)

include javalib/VERSION

default config_libs libs all config:
ifneq ("$(LIB_SUBDIRS)", "")
	mkdir -p classes
	@for file in $(LIB_PATHS); do \
	    make -C $$file INFRA_BASE=$(INFRA_BASE) install_libs; \
	done
endif

install_libs: default
	jar -cf $(MODULE_LIB)/common.jar -C classes com

install image: 
ifneq ("$(LIB_SUBDIRS)", "")
	@echo "Packaging javalib"
	rm -fr $(BUILD_DIR)
	mkdir -p $(BUILD_DIR)/control $(BUILD_DIR)/cmd $(BUILD_DIR)/package
	(cd $(BUILD_DIR)/package; mkdir -p jar lib)
	(cd javalib/cmd; cp getVersion init $(BUILD_DIR)/cmd)
	(cd javalib/control; cp postinst preinst $(BUILD_DIR)/control)
	cp $(MODULE_LIB)/common.jar $(BUILD_DIR)/package/jar
	cp $(MODULE_JAVASRC)/com/broadon/jni/BBcrypto.so $(BUILD_DIR)/package/lib
	$(RELGEN) javalib/release.dsc-tmpl > $(BUILD_DIR)/control/release.dsc
	cp $(BUILD_DIR)/control/release.dsc $(BUILD_DIR)/release.dsc
	$(PKGEN) --indir $(BUILD_DIR) --outfile $(BUILD_DIR)/$(MODULE_NAME).pkg
	$(RELGEN) javalib/sw_rel.xml-tmpl > $(BUILD_DIR)/sw_rel.xml
endif

javadoc:
ifneq ("$(LIB_SUBDIRS)", "")
	mkdir -p $(JAVADOC_DIR)
	javadoc -author -version -windowtitle "Common libraries" \
		-classpath $(CLASSPATH) -d $(JAVADOC_DIR)		\
		-link http://java.sun.com/j2se/1.4.2/docs/api		\
		-link http://jakarta.apache.org/tomcat/tomcat-4.1-doc/servletapi \
		-sourcepath $(MODULE_JAVASRC) $(PACKAGES)
endif

clean:
	@for file in $(SUBDIRS) $(LIB_PATHS); do \
	    make -C $$file INFRA_BASE=$(INFRA_BASE) $@; \
	done
	rm -fr $(JAVADOC_DIR) classes

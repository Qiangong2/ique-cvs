#!/bin/bash

######
# This script creates a seperate directory for each CA file uploaded.
# Untar the ca tar file to extract the three xml files
# Verifies all required files exists.
# DONT CHANGE EXIT VALUES - since they are used to report the results back
######

ROOTDIR=$1
WORKDIR=$1/$2
TARFILE=$2.$3
CERTXML=certificates.xml
CHAINXML=certificate_chains.xml
CRLXML=current_crls.xml

rm -rf $WORKDIR
mkdir -p $WORKDIR

mv -f $ROOTDIR/$TARFILE $WORKDIR
cd $WORKDIR

tar xf $TARFILE
if [ $? -ne 0 ]
then
	rm -rf $WORKDIR
	exit 1
fi

if [ ! -e $CERTXML ]
then
	rm -rf $WORKDIR
	exit 2
fi

if [ ! -f $CHAINXML ]
then
	rm -rf $WORKDIR
	exit 2
fi

if [ ! -f $CRLXML ]
then
	rm -rf $WORKDIR
	exit 2
fi

exit 0

#!/bin/bash

######
# This script creates a seperate directory for each Content file uploaded.
# Untar the content tar file to extract the five xml files and the object file
# Verifies all required files exists.
# DONT CHANGE EXIT VALUES - since they are used to report the results back
######

ROOTDIR=$1
WORKDIR=$1/$2
TARFILE=$2.$3
SIG=sig
CERT=cert
CERTID=cert_id
CONTENTTAR=contentPackage.tar
OBJECTXML=CONTENT_OBJECTS.xml
TITLEXML=CONTENT_TITLES.xml
REVIEWXML=CONTENT_TITLE_REVIEWS.xml

rm -rf $WORKDIR
mkdir -p $WORKDIR

mv -f $ROOTDIR/$TARFILE $WORKDIR
cd $WORKDIR

tar xf $TARFILE
if [ $? -ne 0 ]
then
	rm -rf $WORKDIR
	exit 1
fi

if [ ! -e $CERTID ]
then
	rm -rf $WORKDIR
	exit 2
fi

if [ ! -e $CERT ]
then
	rm -rf $WORKDIR
	exit 2
fi

if [ ! -e $SIG ]
then
	rm -rf $WORKDIR
	exit 2
fi

if [ -e $CONTENTTAR ]
then
        tar xf $CONTENTTAR
        if [ $? -ne 0 ]
        then
                rm -rf $WORKDIR
                exit 2
        fi
else
        rm -rf $WORKDIR
        exit 2
fi

if [ ! -e $OBJECTXML ]
then
	rm -rf $WORKDIR
	exit 2
fi

if [ ! -e $TITLEXML ]
then
	rm -rf $WORKDIR
	exit 2
fi

if [ ! -e $REVIEWXML ]
then
	rm -rf $WORKDIR
	exit 2
fi

exit 0

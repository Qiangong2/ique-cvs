#!/bin/bash

######
# This script creates a seperate directory for each RMA file uploaded.
# MOves the file to the unique directory created for each upload
# DONT CHANGE EXIT VALUES - since they are used to report the results back
######

ROOTDIR=$1
WORKDIR=$1/$2
RMAFILE=$3

rm -rf $WORKDIR
mkdir -p $WORKDIR

mv -f $ROOTDIR/$RMAFILE $WORKDIR

exit 0

#!/bin/bash

######
# This script creates a seperate directory for each NEC report file uploaded.
# Untar the report file to extract the signature and the xml tar file.
# Untar the xml tar file to extract the two xml files
# Verifies all required files exists.
# DONT CHANGE EXIT VALUES - since they are used to report the results back
######

ROOTDIR=$1
WORKDIR=$1/$2
TARFILE=$2.$3
EXTENSION=$3
XMLTAR=xmlexport.tar
XMLSIG=xmlexport.sig
XMLDIR=xmlexport
LOTXML=lots.xml
CHIPSXML=lot_chips.xml

rm -rf $WORKDIR
mkdir -p $WORKDIR

mv -f $ROOTDIR/$TARFILE $WORKDIR
cd $WORKDIR

if [ "$EXTENSION" = "tar" ]
then
	tar xf $TARFILE
elif [ "$EXTENSION" = "bz2" ]
then
	tar jxf $TARFILE
else
	exit 2
fi


if [ $? -ne 0 ]
then
	rm -rf $WORKDIR
	exit 1
fi

if [ ! -e $XMLSIG ]
then
	rm -rf $WORKDIR
	exit 2
fi

if [ -e $XMLTAR ]
then
	tar xf $XMLTAR
	if [ $? -ne 0 ]
	then
		rm -rf $WORKDIR
		exit 2
	fi
else
	rm -rf $WORKDIR
	exit 2
fi

if [ -d $WORKDIR/$XMLDIR ]
then
	cd $WORKDIR/$XMLDIR
else
	rm -rf $WORKDIR
	exit 2
fi

if [ ! -e $LOTXML ]
then
	rm -rf $WORKDIR
	exit 2
fi

if [ ! -f $CHIPSXML ]
then
	rm -rf $WORKDIR
	exit 2
fi

exit 0

<%
   Object role = session.getAttribute("BCC_ROLE");
   String ROLE_ADMIN = "10";
   String ROLE_NEC = "20";
   String ROLE_CA = "50";
   String ROLE_CONTENT = "60";
   String ROLE_RMA = "70";

   String mode=request.getParameter("display");
   if (mode==null) {
     mode="blank";
   }

   String index=request.getParameter("page");
   if (index==null) {
     index="home";
   }
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">
   <%if (index.equals("home")) {%>
     <TITLE><%=session.getAttribute("HOME_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
      function onClickSubmit(formObj) {
          if (isNull(formObj.newrole.value) || isBlank(formObj.newrole.value))
          { alert(<%="\"" + session.getAttribute("ALERT_CHANGE_ROLE") + "\""%>);
            formObj.newrole.focus();
            return; }
        formObj.submit();
     }
     </SCRIPT>

   <%} else if (index.equals("dlrList")) {%>
     <TITLE><%=session.getAttribute("DLR_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.dlrsort.value = sort;
        }
     </SCRIPT>       
    <%} else if (index.equals("userEdit")) {%>
     <TITLE><%= session.getAttribute("OPERATION_USER_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
      function onClickSubmit(formObj) {
          formObj.fullname.value=Trim(formObj.fullname.value);
          if (isNull(formObj.fullname.value) || isBlank(formObj.fullname.value))
          { alert(<%="\"" + session.getAttribute("ALERT_FULLNAME") + "\""%>);
            formObj.fullname.focus();
            return; }

          formObj.email.value=Trim(formObj.email.value);
          if (isNull(formObj.email.value) || isBlank(formObj.email.value))
          { alert(<%="\"" + session.getAttribute("ALERT_EMAIL_ADDRESS") + "\""%>);
            formObj.email.focus();
            return;
          } else {
            var splitted = formObj.email.value.match("^(.+)@(.+)$");
            if (splitted==null)
            {
               var charpos = formObj.email.value.search("[^A-Za-z0-9]");
               if (formObj.email.value.length > 0 && charpos >= 0)
               {
                   alert(<%="\"" + session.getAttribute("ALERT_CHAR_EMAIL_ADDRESS") + "\""%>);
                   formObj.email.focus();
                   return;
               }
            } else {
               if (splitted[1]!=null )
               {
                  var regexp_user=/^\"?[\w-_\.]*\"?$/;
                  if (splitted[1].match(regexp_user)==null)
                  {
                     alert(<%="\"" + session.getAttribute("ALERT_CHAR_EMAIL_ADDRESS") + "\""%>);
                     formObj.email.focus();
                     return;
                  }
               }
               if (splitted[2]!=null)
               {
                  var regexp_domain=/^[\w-\.]*\.[A-Za-z]{2,4}$/;
                  if (splitted[2].match(regexp_domain)==null)
                  {
                     var regexp_ip=/^\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\]$/;
                     if (splitted[2].match(regexp_ip)==null)
                     {
                        alert(<%="\"" + session.getAttribute("ALERT_CHAR_EMAIL_ADDRESS") + "\""%>);
                        formObj.email.focus();
                        return;
                     }
                  }
               }
            }
          }

          if (isNull(formObj.role.value) || isBlank(formObj.role.value))
          { alert(<%="\"" + session.getAttribute("ALERT_ROLE_LEVEL") + "\""%>);
            formObj.role.focus();
            return; }
          if (formObj.confpasswd.value != formObj.passwd.value)
          { alert(<%="\"" + session.getAttribute("ALERT_PASSWORD_MISMATCH") + "\""%>);
            formObj.passwd.focus();
            return; }
          if (formObj.email.value!=formObj.last_email.value)
          { if (!(confirm(<%="\"" + session.getAttribute("ALERT_REVOKE_SMARTCARD_EMAIL") + "\""%>))) {
            formObj.email.focus();
            return; }
          }
        formObj.submit();
     }
     </SCRIPT>

     <%} else if (index.equals("certList")) {%>
     <TITLE><%= session.getAttribute("CERT_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.certsort.value = sort;
        }
     </SCRIPT>

     <%} else if (index.equals("certEdit")) {%>
     <TITLE><%= session.getAttribute("CERT_TEXT_DESC")%></TITLE>

     <%} else if (index.equals("chainList")) {%>
     <TITLE><%= session.getAttribute("CHAIN_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.chainsort.value = sort;
        }
     </SCRIPT>

   <%} else if (index.equals("titleList")) {%>
     <TITLE><%= session.getAttribute("TITLE_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.tlsort.value = sort;
        }
     </SCRIPT>

   <%} else if (index.equals("titleEdit")) {%>
     <TITLE><%= session.getAttribute("TITLE_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
      function onClickSubmit(formObj) {
         formObj.title_name.value=Trim(formObj.title_name.value);
         if (isNull(formObj.title_name.value) || isBlank(formObj.title_name.value))
         { alert(<%="\"" + session.getAttribute("ALERT_TITLE_NAME") + "\""%>);
           formObj.title_name.focus();
           return; }
         formObj.category.value=Trim(formObj.category.value);
         if (isNull(formObj.category.value) || isBlank(formObj.category.value))
         { alert(<%="\"" + session.getAttribute("ALERT_CATEGORY") + "\""%>);
           formObj.category.focus();
           return; }
         formObj.publisher.value=Trim(formObj.publisher.value);
         if (isNull(formObj.publisher.value) || isBlank(formObj.publisher.value))
         { alert(<%="\"" + session.getAttribute("ALERT_PUBLISHER") + "\""%>);
           formObj.publisher.focus();
           return; }
         formObj.developer.value=Trim(formObj.developer.value);
         if (isNull(formObj.developer.value) || isBlank(formObj.developer.value))
         { alert(<%="\"" + session.getAttribute("ALERT_DEVELOPER") + "\""%>);
           formObj.developer.focus();
           return; }
         formObj.desc.value=Trim(formObj.desc.value);
         formObj.submit();
     }
     </SCRIPT>

   <%} else if (index.equals("contentList")) {%>
     <TITLE><%= session.getAttribute("CONTENT_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.consort.value = sort;
        }
     </SCRIPT>

   <%} else if (index.equals("contentDetail")) {%>
     <TITLE><%= session.getAttribute("CONTENT_TEXT_DESC")%></TITLE>

   <%} else if (index.equals("cotList")) {%>
     <TITLE><%= session.getAttribute("CONTENT_TYPE_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.ctsort.value = sort;
        }
     </SCRIPT>

   <%} else if (index.equals("cotAdd")) {%>
     <TITLE><%= session.getAttribute("CONTENT_TYPE_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
     function onClickSubmit(formObj) {
        formObj.cot.value=Trim(formObj.cot.value);
        if (isNull(formObj.cot.value) || isBlank(formObj.cot.value))
        { alert(<%="\"" + session.getAttribute("ALERT_CONTENT_TYPE") + "\""%>);
          formObj.cot.focus();
          return; }
        formObj.desc.value=Trim(formObj.desc.value);
        formObj.submit();
     }
     </SCRIPT>
   
   <%} else if (index.equals("cotEdit")) {%>
     <TITLE><%= session.getAttribute("CONTENT_TYPE_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
     function onClickSubmit(formObj) {
        formObj.desc.value=Trim(formObj.desc.value);
        formObj.submit();
     }
     </SCRIPT>
   
   <%} else if (index.equals("geList")) {%>
     <TITLE><%= session.getAttribute("GLOBAL_ETICKETS_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.gesort.value = sort;
        }
        function onClickGetLatest(formObj, content, bu, tid) {
            var msg = <%="\"" + session.getAttribute("TEXT_CONFIRM_GET_LATEST") + "\""%>;
            if (confirm(msg) ) {
        	formObj.type.value="ge.latest";
        	formObj.action.value="update";
	        formObj.cid.value=content;
        	formObj.buid.value=bu;
        	formObj.tid.value=tid;
        	formObj.submit();
            }
        }

        function onClickRevoke(formObj, content, bu) {
            var msg = <%="\"" + session.getAttribute("TEXT_CONFIRM_REVOKE") + "\""%>;
            if (confirm(msg) ) {
        	formObj.type.value="ge.revoke";
        	formObj.action.value="update";
	        formObj.cid.value=content;
        	formObj.buid.value=bu;
        	formObj.submit();
            }
        }
     </SCRIPT>

   <%} else if (index.equals("geDetail")) {%>
     <TITLE><%= session.getAttribute("GLOBAL_ETICKETS_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
     function onClickSubmit(formObj) {
        if (isNull(formObj.buid.value) || isBlank(formObj.buid.value))
        { alert(<%="\"" + session.getAttribute("ALERT_BU") + "\""%>);
          formObj.buid.focus();
          return; }
       <%if (mode.equals("add")) {%>
        if (isNull(formObj.title.value) || isBlank(formObj.title.value))
        { alert(<%="\"" + session.getAttribute("ALERT_TITLE") + "\""%>);
          formObj.title.focus();
          return; }
       <%}%>
        formObj.submit();
     }
     </SCRIPT>
   
   <%} else if (index.equals("lotList")) {%>
     <TITLE><%=session.getAttribute("LOTS_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.lsort.value = sort;
        }
     </SCRIPT>       
   <%} else if (index.equals("necUpload")) {%>
     <TITLE><%=session.getAttribute("NEC_UPLOAD_TEXT_DESC")%></TITLE>
	<script language = "Javascript">
	function ValidateForm(theForm){
                theForm['file1'].value=Trim(theForm['file1'].value);
		if (!(!isNull(theForm['file1'].value) && !isBlank(theForm['file1'].value))) {
	   	    alert("No Files to Upload !");
		    return;
		}
	        theForm.submit();
	}
        </script>
   <%} else if (index.equals("rmaUpload")) {%>
     <TITLE><%=session.getAttribute("RMA_UPLOAD_TEXT_DESC")%></TITLE>
	<script language = "Javascript">
	function ValidateForm(theForm){
                theForm['file1'].value=Trim(theForm['file1'].value);
		if (!(!isNull(theForm['file1'].value) && !isBlank(theForm['file1'].value))) {
	   	    alert("No Files to Upload !");
		    return;
		}
	        theForm.submit();
	}
        </script>
   <%} else if (index.equals("caUpload")) {%>
     <TITLE><%=session.getAttribute("CA_UPLOAD_TEXT_DESC")%></TITLE>
	<script language = "Javascript">
	function ValidateForm(theForm){
                theForm['file1'].value=Trim(theForm['file1'].value);
                if (!(!isNull(theForm['file1'].value) && !isBlank(theForm['file1'].value))) {
	   	    alert("No Files to Upload !");
		    return;
		}
	        theForm.submit();
	}
        </script>
  <%} else if (index.equals("contentUpload")) {%>
     <TITLE><%=session.getAttribute("CONTENT_UPLOAD_TEXT_DESC")%></TITLE>
	<script language = "Javascript">
	function ValidateForm(theForm){
                theForm['file1'].value=Trim(theForm['file1'].value);
                if (!(!isNull(theForm['file1'].value) && !isBlank(theForm['file1'].value))) {
	   	    alert("No Files to Upload !");
		    return;
		}
	        theForm.submit();
	}
        </script>
  <%}%>
        
  <%if (role!=null && (role.toString().equals(ROLE_NEC) || role.toString().equals(ROLE_CA) || 
                       role.toString().equals(ROLE_CONTENT) || role.toString().equals(ROLE_RMA))) {%>
    <SCRIPT LANGUAGE="javascript" src="/js/functions.js"></SCRIPT>
    <SCRIPT LANGUAGE="JavaScript">
    function getHelp(pageIndex) {

        var helpUrl = pageIndex;                 
        if (pageIndex == "home") {		// Welcome Page
            helpUrl = "help.html";
        }
        else if (pageIndex == "necUpload") {	// NEC uploads
            helpUrl = "chipUpload.html";
        }
        else if (pageIndex == "dlrList") {	// Uploads List
            helpUrl = "chipReview.html#uploads";
        }
        else if (pageIndex == "lotList") {	// Lots List
            helpUrl = "chipReview.html#lots";
        }
    
        var url = "/help/"+helpUrl;
        var helpWin = window.open("", "_bbhelp","status=no,toolbar=no,scrollbars=yes,resizable=yes,menubar=no,copyhistory=no,directories=no,location=no,width=640,height=600");
        helpWin.focus();  // Put help window on top
        helpWin.location = url;  // Load the URL
    }
   </SCRIPT>
  <%}%>

   <LINK rel="stylesheet" type="text/css" href="/css/rms.css" title="Style"/>
</HEAD>

<!-- <body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> -->
<!-- <BODY bgcolor="white" link="#003366" vlink="#003366" alink="#003366" text="black"> -->
<BODY bgcolor="white" text="black">

<TABLE border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="white">
  <!-- 1st Row: Logo Section -->
  <TR width="100%"> 
    <TD colspan="2"> 
      <TABLE border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="white">
        <TR height="39"> 
          <TD width="1%" valign="top" bgcolor="white"><IMG border="0" src="/images/logo_whiteonblue.gif" width="176" height="39"> 
          </TD>
          <TD width="98%" valign="middle" align="right" nowrap style="background-repeat: no-repeat" background="/images/title_background.gif"> 
	  <%if (role!=null && role.toString().equals(ROLE_NEC)) {%>
            <FONT class="titleFont">Chip Upload Server&nbsp;&nbsp;</FONT></TD>
          <%} else if (role!=null && role.toString().equals(ROLE_CA)){%>
            <FONT class="titleFont">CA Upload Server&nbsp;&nbsp;</FONT></TD>
          <%} else if (role!=null && role.toString().equals(ROLE_CONTENT)){%>
            <FONT class="titleFont">Content Upload Server&nbsp;&nbsp;</FONT></TD>
          <%} else if (role!=null && role.toString().equals(ROLE_RMA)){%>
            <FONT class="titleFont">RMA Data Upload&nbsp;&nbsp;</FONT></TD>
          <%}%>
          <TD width="1%"><IMG border="0" width="1" src="/images/spacer.gif" height="39"></TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
  <!-- 2nd Row: White Line -->
  <TR width="100%" bgcolor=white><TD colspan="2"><IMG border=0 width=1 height=1 src="/images/spacer.gif"></TD></TR>
  <!-- 3rd Row: Menu Navigation Bar -->
  <TR width="100%">
    <TD >
      <!-- Top-Level Menu Bar: Menu 1 is selected -->
      <TABLE border=0 cellpadding=0 cellspacing=0 width=100% bgcolor="#efefef">
        <TR>
         <%if (index.equals("necUpload") || index.equals("caUpload") || index.equals("contentUpload") || index.equals("rmaUpload")) {%>
          <TD><IMG src="/images/menu.off.start.gif"></TD>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
	  <%if (role!=null && role.toString().equals(ROLE_NEC)) {%>
              <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_NEC_UPLOAD")%></FONT>&nbsp;&nbsp;</TD>
          <%} else if (role!=null && role.toString().equals(ROLE_CA)){%>
              <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_CA_UPLOAD")%></FONT>&nbsp;&nbsp;</TD>
          <%} else if (role!=null && role.toString().equals(ROLE_CONTENT)){%>
              <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_CONTENT_UPLOAD")%></FONT>&nbsp;&nbsp;</TD>
          <%} else if (role!=null && role.toString().equals(ROLE_RMA)){%>
              <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_RMA_UPLOAD")%></FONT>&nbsp;&nbsp;</TD>
          <%}%>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
         <%} else {%>
	  <%if (role!=null && role.toString().equals(ROLE_NEC)) {%>
              <TD nowrap background="/images/menu.off.bg.gif">&nbsp;&nbsp;<A class="mainMenuHeader" HREF="necUpload"><FONT class="mainMenuText"><%=session.getAttribute("MENU_NEC_UPLOAD")%></FONT></A>&nbsp;</TD>
          <%} else if (role!=null && role.toString().equals(ROLE_CA)){%>
              <TD nowrap background="/images/menu.off.bg.gif">&nbsp;&nbsp;<A class="mainMenuHeader" HREF="caUpload"><FONT class="mainMenuText"><%=session.getAttribute("MENU_CA_UPLOAD")%></FONT></A>&nbsp;</TD>
          <%} else if (role!=null && role.toString().equals(ROLE_CONTENT)){%>
              <TD nowrap background="/images/menu.off.bg.gif">&nbsp;&nbsp;<A class="mainMenuHeader" HREF="contentUpload"><FONT class="mainMenuText"><%=session.getAttribute("MENU_CONTENT_UPLOAD")%></FONT></A>&nbsp;</TD>
          <%} else if (role!=null && role.toString().equals(ROLE_RMA)){%>
              <TD nowrap background="/images/menu.off.bg.gif">&nbsp;&nbsp;<A class="mainMenuHeader" HREF="rmaUpload"><FONT class="mainMenuText"><%=session.getAttribute("MENU_RMA_UPLOAD")%></FONT></A>&nbsp;</TD>
          <%}%>
         <%}%>

         <%if (index.equals("certList") || index.equals("certEdit") || index.equals("chainList")) {%>
	  <%if (role!=null && role.toString().equals(ROLE_CA)) {%>
           <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
           <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_CC_BROWSER")%></FONT>&nbsp;&nbsp;</TD>
           <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
          <%}%>
         <%} else {%>
	  <%if (role!=null && role.toString().equals(ROLE_CA)) {%>
	      <%if (!(index.equals("caUpload"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
              <TD nowrap background="/images/menu.off.bg.gif">&nbsp;&nbsp;<A class="mainMenuHeader" HREF="serv?type=cert&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_CC_BROWSER")%></FONT></A>&nbsp;</TD>
          <%}%>
         <%}%>

         <%if (index.equals("cotList") || index.equals("cotAdd") || index.equals("cotEdit") ||
               index.equals("titleList") || index.equals("titleEdit") ||
               index.equals("geList") || index.equals("geDetail") ||
               index.equals("contentList") || index.equals("contentDetail")) {%>
	  <%if (role!=null && role.toString().equals(ROLE_CONTENT)) {%>
           <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
           <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_CDS")%></FONT>&nbsp;&nbsp;</TD>
           <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
          <%}%>
         <%} else {%>
	  <%if (role!=null && role.toString().equals(ROLE_CONTENT)) {%>
	      <%if (!(index.equals("contentUpload"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
              <TD nowrap background="/images/menu.off.bg.gif">&nbsp;&nbsp;<A class="mainMenuHeader" HREF="serv?type=content&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_CDS")%></FONT></A>&nbsp;</TD>
          <%}%>
         <%}%>

         <%if (index.equals("dlrList") || index.equals("lotList")) {%>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
	  <%if (role!=null && role.toString().equals(ROLE_NEC)) {%>
              <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_NEC_LIST")%></FONT>&nbsp;&nbsp;</TD>
          <%} else if (role!=null && role.toString().equals(ROLE_CA)){%>
              <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_CA_LIST")%></FONT>&nbsp;&nbsp;</TD>
          <%} else if (role!=null && role.toString().equals(ROLE_CONTENT)){%>
              <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_CONTENT_LIST")%></FONT>&nbsp;&nbsp;</TD>
          <%} else if (role!=null && role.toString().equals(ROLE_RMA)){%>
              <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_RMA_LIST")%></FONT>&nbsp;&nbsp;</TD>
          <%}%>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
         <%} else {%>
	    <%if (role!=null && role.toString().equals(ROLE_NEC)) {%>
	      <%if (!(index.equals("necUpload"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
              <TD nowrap background="/images/menu.off.bg.gif">&nbsp;&nbsp;<A class="mainMenuHeader" HREF="serv?type=dlr&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_NEC_LIST")%></FONT></A>&nbsp;</TD>
            <%} else if (role!=null && role.toString().equals(ROLE_CA)){%>
	      <%if (!(index.equals("certList") || index.equals("certEdit") || index.equals("chainList"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
              <TD nowrap background="/images/menu.off.bg.gif">&nbsp;&nbsp;<A class="mainMenuHeader" HREF="serv?type=dlr&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_CA_LIST")%></FONT></A>&nbsp;</TD>
            <%} else if (role!=null && role.toString().equals(ROLE_CONTENT)){%>
              <%if (!(index.equals("cotList") || index.equals("cotAdd") || index.equals("cotEdit") ||
                      index.equals("titleList") || index.equals("titleEdit") || index.equals("geList") || index.equals("geDetail") ||
                      index.equals("contentList") || index.equals("contentDetail"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
              <TD nowrap background="/images/menu.off.bg.gif">&nbsp;&nbsp;<A class="mainMenuHeader" HREF="serv?type=dlr&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_CONTENT_LIST")%></FONT></A>&nbsp;</TD>
	    <%} else if (role!=null && role.toString().equals(ROLE_RMA)) {%>
	      <%if (!(index.equals("rmaUpload"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
              <TD nowrap background="/images/menu.off.bg.gif">&nbsp;&nbsp;<A class="mainMenuHeader" HREF="serv?type=dlr&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_RMA_LIST")%></FONT></A>&nbsp;</TD>
            <%}%>
         <%}%>

         <%if (index.equals("userEdit")) {%>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_USER_ACCOUNT")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
         <%} else {%>
           <%if (!(index.equals("dlrList") || index.equals("lotList"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
              <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=user&action=edit"><FONT class="mainMenuText"><%=session.getAttribute("MENU_USER_ACCOUNT")%></FONT></A>&nbsp;</TD>
         <%}%>

	  <%if (role!=null && (role.toString().equals(ROLE_NEC) || role.toString().equals(ROLE_CA) ||
                role.toString().equals(ROLE_RMA) || role.toString().equals(ROLE_CONTENT))) {%>
           <%if (!(index.equals("userEdit"))) {%>
 	    <TD><IMG src="/images/menu.off.off.separator.gif"></TD>
           <%}%>
           <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="javascript:getHelp('<%=index%>')"><FONT class="mainMenuText"><%=session.getAttribute("MENU_HELP")%></FONT></A>&nbsp;</TD>
           <TD bgcolor="#336699" nowrap width="100%" align=right background="/images/menu.off.bg.gif">&nbsp;</TD>
          <%}%>
        </TR>
      </TABLE>
     <!-- End of Menu Bar -->
    </TD>
    <TD nowrap align="right" background="/images/menu.off.bg.gif">
      <font class="userText"><%=session.getAttribute("BCC_USER")%>: </font>
      <a class="logoutMenuHeader" href="login?laction=logout" id="logout" target="_top">
      <font class="logoutText"><%=session.getAttribute("TEXT_LOGOUT")%></font></a>&nbsp;&nbsp;
    </TD>
  </TR>
    <!-- Small Gray Bar -->
  <TR width="100%" bgcolor="#efefef">
    <TD colspan="2"><IMG border="0" width="1" height="5" src="/images/spacer.gif"></TD>
  </TR>
</TABLE>

<div style="width:100%;background-color=#efefef">
<TABLE width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#efefef">
  <TR width="100%" height="15">
<%if (role!=null && role.toString().equals(ROLE_NEC)) {%>
 <%if (index.equals("dlrList") || index.equals("lotList")) {%>
  <!-- Start of Sub-Menu Bar -->
  <%if (index.equals("dlrList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_NEC_UPLOADS_LIST")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=dlr&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_NEC_UPLOADS_LIST")%></FONT></A></TD>
  <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|&nbsp;&nbsp;</TD>
  <%if (index.equals("lotList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_NEC_LOTS_LIST")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=lot&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_NEC_LOTS_LIST")%></FONT></A></TD>
  <%}%>
  <!-- End of 1st Sub-Menu Bar for Lots List -->
 <%}%>
<%}%>

<%if (role!=null && role.toString().equals(ROLE_CA)) {%>
 <%if (index.equals("certList") || index.equals("certEdit") || index.equals("chainList")) {%>
  <!-- Start of Sub-Menu Bar -->
  <%if (index.equals("certList") || index.equals("certEdit")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_CERTIFICATES")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=cert&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_CERTIFICATES")%></FONT></A></TD>
  <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("chainList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_CHAINS")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=chain&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_CHAINS")%></FONT></A></TD>
  <%}%>
  <!-- End of 1st Sub-Menu Bar for CC Browser -->
 <%}%>
<%}%>

<%if (role!=null && role.toString().equals(ROLE_CONTENT)) {%>
  <%if (index.equals("cotList") || index.equals("cotAdd") || index.equals("cotEdit") ||
        index.equals("titleList") || index.equals("titleEdit") ||
        index.equals("geList") || index.equals("geDetail") ||
        index.equals("contentList") || index.equals("contentDetail")) {%>
    <%if (index.equals("contentList") || index.equals("contentDetail")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_CONTENT")%></FONT></TD>
    <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=content&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_CONTENT")%></FONT></A></TD>  
    <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
    <%if (index.equals("titleList") || index.equals("titleEdit")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_TITLE")%></FONT></TD>
    <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=title&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_TITLE")%></FONT></A></TD>
    <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
    <%if (index.equals("cotList") || index.equals("cotAdd") || index.equals("cotEdit")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_CONTENT_TYPE")%></FONT></TD>
    <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=cot&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_CONTENT_TYPE")%></FONT></TD>
    <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
    <%if (index.equals("geList") || index.equals("geDetail")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_GLOBAL_ETICKETS")%></FONT></TD>
    <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ge&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_GLOBAL_ETICKETS")%></FONT></A></TD>
    <%}%>
  <!-- End of 1st Sub-Menu Bar for Content Browser -->
 <%}%>
<%}%>

      <TD nowrap width="100%" align="right"><A class="subMenuHeader" href="serv?type=home&action=edit" id="currole"><FONT class="subMenuText"><%=session.getAttribute("BCC_ROLE_NAME")%></FONT></A>&nbsp;&nbsp;</TD>
  </TR>
</TABLE>
</div>

<!-- Bar under menu -->
<TABLE width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#efefef">
  <TR width="100%">
    <TD><IMG border="0" width="1" height="5" src="/images/spacer.gif"></TD>
  </TR>
  <TR width="100%" bgcolor="#999999">
    <TD><IMG border="0" width="1" height="1" src="/images/spacer.gif"></TD>
  </TR>
</TABLE>
<TABLE width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
  <TR width="100%">
    <TD><IMG border="0" width="1" height="5" src="/images/spacer.gif"></TD>
  </TR>
</TABLE>
<!-- End of bar -->

  <!-- Start of 2nd Sub-Menu Bar -->
<center>
<%if (index.equals("titleEdit")) {%>
  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=title&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_TLLIST")%></FONT></A></TD>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_TLUPDATE")%></FONT></TD>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for Content Title -->

<%} else if (index.equals("contentDetail")) {%>
  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=content&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_CONLIST")%></FONT></A></TD>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_CONDETAIL")%></FONT></TD>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for Content Object -->

<%} else if (index.equals("cotList") || index.equals("cotAdd") || index.equals("cotEdit")) {%>
  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">
  <%if (index.equals("cotList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_CTLIST")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=cot&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_CTLIST")%></FONT></A></TD>
  <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("cotAdd")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_CTADD")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=cot&action=add"><FONT class="subMenuText"><%=session.getAttribute("MENU_CTADD")%></FONT></A></TD>
  <%}%>
  <%if (index.equals("cotEdit")){%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_CTUPDATE")%></FONT></TD>
  <%}%>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for Content Object Type -->

<%} else if (index.equals("geList") || index.equals("geDetail")) {%>
  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">
  <%if (index.equals("geList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_GE_LIST")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ge&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_GE_LIST")%></FONT></A></TD>
  <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("geDetail")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_GE_ADD")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ge.bu&action=add"><FONT class="subMenuText"><%=session.getAttribute("MENU_GE_ADD")%></FONT></A></TD>
  <%}%>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for Global ETickets  -->

<%} else if (index.equals("certEdit")) {%>
  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=cert&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_CERT_LIST")%></FONT></A></TD>      
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_CERT_DETAIL")%></FONT></TD>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for Certificates -->

<%}%>
</center>
<P>


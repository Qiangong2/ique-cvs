<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.ms.common.QueryBean" %>
<jsp:useBean class="com.broadon.ms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();
  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  String[] params = qb.getParamArray();

  String model = params[0];
  String sku = params[1];
  String start = params[2];
  String end = params[3];
  String buid = params[4];
  String buName = params[5];

  String titleStr = htmlResults[0];
%>

<jsp:include page="bcc-header.jsp" flush="true">
    <jsp:param name="page" value="bundletitleAdd"/>   
</jsp:include>

<%if (err!=null && err!="") {%><center><font class="errorText"><%=err%></font></center><p><%}%>

<form name="theForm" id="theForm" action="serv" method="POST" align="center" onSubmit="return false;">
    <input type="hidden" name="type" value="bundletitle"></input>
    <input type="hidden" name="action" value="insert"></input>
    <input type="hidden" name="model" value="<%=model%>"></input>
    <input type="hidden" name="buid" value="<%=buid%>"></input>
    <input type="hidden" name="start" value="<%=start%>"></input>
    <input type="hidden" name="end1" value="<%=end%>"></input>
    <input type="hidden" name="sku" value="<%=sku%>"></input>
 
<TABLE cellSpacing=0 cellPadding=1 width=50% align=center bgColor="#336699" border=0>
  <TR> 
    <TD> 
      <!-- BB Bundles Titles Add Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699> 
          <TD width="100%"> 
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              <TR> 
                  <TD width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2"><%=session.getAttribute("TEXT_ADD_BUNDLES_TITLE")%></FONT></TD>
              </TR>
             </TABLE>
          </TD>
        </TR>
        <TR> 
          <TD bgColor=#efefef>
            <TABLE bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <TR>
                <TD class="tableheader" align=center width=50%><%=session.getAttribute("COL_PARAMETER")%></TD>
                <TD class="tableheader" align=center width=50% colspan=2><%=session.getAttribute("COL_VALUE")%></TD>
              </TR>
                <tr><td class="formLabel2" nowrap="true"><%=session.getAttribute("COL_BU_NAME")%>:</td><td class="formField"></td>
                  <td class="formField" nowrap="true">
                    <%=buName%>
                  </td>
                </tr>
                <tr><td class="formLabel2" nowrap="true"><%=session.getAttribute("COL_BB_MODEL")%>:</td><td class="formField"></td>
		  <td class="formField" nowrap="true">
                    <%=model%>
                  </td>
                </tr>
                <tr><td class="formLabel2" nowrap="true"><%=session.getAttribute("COL_TITLE")%>:</td><td class="formField"></td>
                  <td class="formField" nowrap="true">
                     <select name="title"><option value=""></option><%=titleStr%></select>
                     <font color="red">*</font>
                  </td>
                </tr>
                <tr>
                  <td class="formLabel2" nowrap="true"><%=session.getAttribute("COL_RTYPE")%>:</td>
                  <td class="formField"></td>
                  <td class="formField" nowrap="true">
                     <select name="rtype"><option value=""></option>
                        <option value="PR"><%=session.getAttribute("RTYPE_OPTION_PR")%></option>
                        <option value="LR"><%=session.getAttribute("RTYPE_OPTION_LR")%></option>
                        <option value="TR"><%=session.getAttribute("RTYPE_OPTION_TR")%></option>
                        <option value="XR"><%=session.getAttribute("RTYPE_OPTION_XR")%></option>
                     </select>
                     <font color="red">*</font>
                  </td>
                </tr>
                <tr>
                  <td nowrap="true" class="formLabel2"><%=session.getAttribute("COL_LIMITS")%>:</td>
                  <td class="formField"></td>
                  <td nowrap="true" class="formField"><input type="text" name="limits" size="19" value=""></td>
                </tr>
                <tr>
                  <td nowrap="true" class="formLabel2"><%=session.getAttribute("COL_START_DATE")%>:</td><td class="formField"></td>
		  <td class="formField" nowrap="true">
                    <%=start%>
                  </td>
                </tr>
                <tr>
                  <td nowrap="true" class="formLabel2"><%=session.getAttribute("COL_END_DATE")%>:</td><td class="formField"></td>
		  <td class="formField" nowrap="true">
                    <%=end%>
                  </td>
                </tr>
                <tr><td class="formLabel2" nowrap="true"><%=session.getAttribute("COL_SKU")%>:</td><td class="formField"></td>
		  <td class="formField" nowrap="true">
                    <%=sku%>
                  </td>
                </tr>
              <TR><TD class="formLabel2"><IMG border=0 height=1 src="/images/spacer.gif"></TD><TD class="formField" colspan=2><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
              <TR><TD colspan="3" bgcolor="white">
                <CENTER><FONT color="red" size="-1">* Required</FONT></CENTER>
              </TD></TR>
              <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
              <TR>
                <TD colspan="3" bgcolor="white">
                <CENTER>
                <INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_SUBMIT")%>" OnClick="onClickSubmit(theForm);">
                <INPUT class="sbutton" type="reset" value="<%=session.getAttribute("BUTTON_RESET")%>">
                <INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_CANCEL")%>" OnClick="history.go(-1)">
                </CENTER>
                </TD>
              </TR>
              <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
</FORM name="theForm">


<jsp:include page="footer.jsp" flush="true"/>


<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.ms.common.QueryBean" %>
<jsp:useBean class="com.broadon.ms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  Object role = session.getAttribute("BCC_ROLE");
  String ROLE_ADMIN = "10";
  String ROLE_CONTENT = "60";

  String err = request.getAttribute("ERROR").toString();
  String success = request.getAttribute("SUCCESS").toString();

  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();

  String conStr = htmlResults[0];
  String reviewStr = htmlResults[1];
  String titleStr = htmlResults[2];

  String[] contentHeader = {"COL_NAME", "COL_ID", "COL_PUB_DATE", "COL_VERSION", "COL_TYPE", 
                            "COL_MIN_UPGRADE_VERSION", "COL_UPGRADE_CONSENT", "COL_LAST_UPDATED"};
  String[] reviewHeader = {"COL_REV_DATE", "COL_REV_BY", "COL_REV_TITLE", "COL_RATINGS"};

%>

<%if (role!=null && (role.toString().equals(ROLE_ADMIN))) {%>
  <jsp:include page="bcc-header.jsp" flush="true">
    <jsp:param name="page" value="titleDetail"/>   
  </jsp:include>
<%} else if (role!=null && (role.toString().equals(ROLE_CONTENT))) {%>
  <jsp:include page="bcc-upload-header.jsp" flush="true">
    <jsp:param name="page" value="titleEdit"/>   
  </jsp:include>
<%}%>

<%if (err!=null && err!="") {%>
  <center><font class="errorText"><%=err%></font></center><p>
<%}%>

<form name="theForm" id="theForm" action="serv" method="POST" align="center" onSubmit="return false;">
    <input type="hidden" name="type" value="title"></input>
    <input type="hidden" name="action" value="update"></input>

<TABLE cellSpacing=0 cellPadding=1 width=50% align=center bgColor="#336699" border=0>
  <TR>
    <TD>
      <!-- Title Detail Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699>
          <TD width="100%">
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              <TR>
                <TD width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2"><%=session.getAttribute("TEXT_TITLE_DETAIL")%></FONT></TD>
              </TR>
             </TABLE>
          </TD>
        </TR>
        <TR>
          <TD bgColor=#efefef>
            <TABLE bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <TR>
                <TD class="tableheader" align=center width=50%><%=session.getAttribute("COL_PARAMETER")%></TD>
                <TD class="tableheader" align=center width=50% colspan=2><%=session.getAttribute("COL_VALUE")%></TD>
              </TR>
              <%=titleStr%>
              <TR><TD class="formLabel2"><IMG border=0 height=1 src="/images/spacer.gif"></TD><TD class="formField" colspan=2><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>

              <%if (role!=null && (role.toString().equals(ROLE_CONTENT))) {%>
              <TR><TD colspan="3" bgcolor="white">
               <CENTER><FONT color="red" size="-1"><%=session.getAttribute("TEXT_REQUIRED")%></FONT></CENTER>
              </TD></TR>
              <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
              <TR>
                <TD colspan="3" bgcolor="white">
                <CENTER>
                <INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_SUBMIT")%>" OnClick="onClickSubmit(theForm);">
                <INPUT class="sbutton" type="reset" value="<%=session.getAttribute("BUTTON_RESET")%>">
                <INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_CANCEL")%>" OnClick="history.go(-1)">
                </CENTER>
                </TD>
              </TR>
              <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
              <%}%>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
</FORM name="theForm">

<p>
<!-- List Contents -->
<TABLE cellSpacing=0 cellPadding=1 width=100% align=center bgColor="#336699" border=0>
  <TR>
    <TD>
      <!-- Regions Detail Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699>
          <TD width="100%">
            <TABLE width="100%" align="center" cellspacing="1" cellpadding="4" bgcolor="#336699">
              <TR><TD width=100% class="tblSubHdrLabel2">
                <%=session.getAttribute("TEXT_CONTENTS_LIST")%>
              </TD></TR>
            </TABLE>
          </TD>
        </TR>
        <TR>
          <TD bgColor=#efefef>
<table border="0" width="100%" align="center" cellspacing="1" cellpadding="4">
<%if (conStr!=null && conStr.indexOf("<tr")>=0) {%>
<tr>
<td class="tableheader" nowrap="true"><%=session.getAttribute("COL_NO")%></td>
  <%for (int i=0; i<contentHeader.length; i++) {%>
    <td class="tableheader" nowrap="true">
      <%=session.getAttribute(contentHeader[i])%>
    </td>
  <%}%>
</tr>
<%=conStr%>
<%} else {%>
 <TR width="100%">
    <TD>
 <!-- No Content Found -->
      <P>
      <CENTER>
       <%=session.getAttribute("TEXT_NO_CONTENT")%>
      </CENTER>
    </TD>
  </TR>
<%}%>
</table>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>

<p>
<!-- List Reviews -->
<TABLE cellSpacing=0 cellPadding=1 width=100% align=center bgColor="#336699" border=0>
  <TR>
    <TD>
      <!-- Regions Detail Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699>
          <TD width="100%">
            <TABLE width="100%" align="center" cellspacing="1" cellpadding="4" bgcolor="#336699">
              <TR><TD width=100% class="tblSubHdrLabel2"> 
                <%=session.getAttribute("TEXT_REVIEWS_LIST")%>
               </TD></TR>
            </TABLE>
          </TD>
        </TR>
        <TR>
          <TD bgColor=#efefef>
<table border="0" width="100%" align="center" cellspacing="1" cellpadding="4">
<%if (reviewStr!=null && reviewStr.indexOf("<tr")>=0) {%>
<tr>
<td class="tableheader" nowrap="true"><%=session.getAttribute("COL_NO")%></td>
  <%for (int i=0; i<reviewHeader.length; i++) {%>
    <td class="tableheader" nowrap="true">
      <%=session.getAttribute(reviewHeader[i])%>
    </td>
  <%}%>
</tr>
<%=reviewStr%>
<%} else {%>
 <TR width="100%">
    <TD>
 <!-- No Review Found -->
      <P>
      <CENTER>
       <%=session.getAttribute("TEXT_NO_REVIEW")%>
      </CENTER>
    </TD>
  </TR>
<%}%>
</table>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>

<%if (role!=null && (role.toString().equals(ROLE_ADMIN))) {%>
<P>
<CENTER>
  <INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_CANCEL")%>" OnClick="history.go(-1)">
</CENTER>
<%}%>

<jsp:include page="footer.jsp" flush="true"/>


<%
   String index=request.getParameter("page");
   if (index==null) {
     index="home";
   }

   String mode=request.getParameter("display");
   if (mode==null) {
     mode="blank";
   }
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">

   <%if (index.equals("home")) {%>
     <TITLE><%= session.getAttribute("HOME_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
      function onClickSubmit(formObj) {
          if (isNull(formObj.newrole.value) || isBlank(formObj.newrole.value))
          { alert(<%="\"" + session.getAttribute("ALERT_CHANGE_ROLE") + "\""%>);
            formObj.newrole.focus();
            return; }
        formObj.submit();
     }
     </SCRIPT>

   <%} else if (index.equals("businessUnitList")) {%>
     <TITLE><%= session.getAttribute("BUSINESS_UNIT_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.busort.value = sort;
        }
     </SCRIPT>       

   <%} else if (index.equals("businessUnitAdd")) {%>
     <TITLE><%= session.getAttribute("BUSINESS_UNIT_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
     function onClickSubmit(formObj) {
        formObj.id.value=Trim(formObj.id.value);
        if (isNull(formObj.id.value) || isBlank(formObj.id.value))
        { alert(<%="\"" + session.getAttribute("ALERT_BU_ID") + "\""%>); 
          formObj.id.focus();	
	  return; }
        else {
            if (!isInteger(formObj.id.value))
            { alert(<%="\"" + session.getAttribute("ALERT_BAD_BU_ID") + "\""%>);
              formObj.id.focus();
              return;
            }
        }
       formObj.business_name.value=Trim(formObj.business_name.value);
       if (isNull(formObj.business_name.value) || isBlank(formObj.business_name.value))
       { alert(<%="\"" + session.getAttribute("ALERT_BUSINESS_NAME") + "\""%>); 
         formObj.business_name.focus();	
         return; }
       formObj.contact.value=Trim(formObj.contact.value);
       formObj.submit();
     }
     </SCRIPT>

   <%} else if (index.equals("businessUnitEdit")) {%>
     <TITLE><%= session.getAttribute("BUSINESS_UNIT_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
     function onClickSubmit(formObj) {
       formObj.business_name.value=Trim(formObj.business_name.value);
       if (isNull(formObj.business_name.value) || isBlank(formObj.business_name.value))
       { alert(<%="\"" + session.getAttribute("ALERT_BUSINESS_NAME") + "\""%>); 
         formObj.business_name.focus();	
         return; }
       formObj.contact.value=Trim(formObj.contact.value);
       formObj.submit();
     }
     </SCRIPT>

     <%} else if (index.equals("modelList")) {%>
     <TITLE><%= session.getAttribute("BB_MODEL_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.bbmsort.value = sort;
        }
     </SCRIPT>       

     <%} else if (index.equals("modelAdd")) {%>
     <TITLE><%= session.getAttribute("BB_MODEL_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
     function onClickSubmit(formObj) {
        formObj.model.value=Trim(formObj.model.value);
        if (isNull(formObj.model.value) || isBlank(formObj.model.value))
        { alert(<%="\"" + session.getAttribute("ALERT_BB_MODEL") + "\""%>); 
          formObj.model.focus();	
          return; }
        formObj.desc.value=Trim(formObj.desc.value);
        formObj.submit();
     }
     </SCRIPT>

     <%} else if (index.equals("modelEdit")) {%>
     <TITLE><%= session.getAttribute("BB_MODEL_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
     function onClickSubmit(formObj) {
        formObj.desc.value=Trim(formObj.desc.value);
        formObj.submit();
     }
     </SCRIPT>

     <%} else if (index.equals("playerDetail")) {%>
     <TITLE><%= session.getAttribute("BB_PLAYER_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
     function onClickSearch(formObj) {
        formObj.id.value=Trim(formObj.id.value);
        if (isNull(formObj.id.value) || isBlank(formObj.id.value))
        { alert(<%="\"" + session.getAttribute("ALERT_BB_ID") + "\""%>);
          formObj.id.focus();
          return;
        } else {
          if (!isInteger(formObj.id.value))
          { alert(<%="\"" + session.getAttribute("ALERT_BAD_BB_ID") + "\""%>);
            formObj.id.focus();
            return;
          }
        }
        formObj.submit();
     }

     function onClickSubmit(formObj, id) {
        formObj.id.value=id;
        if (isNull(formObj.model.value) || isBlank(formObj.model.value))
        { alert(<%="\"" + session.getAttribute("ALERT_BB_MODEL") + "\""%>); 
          formObj.model.focus();	
          return; }
        formObj.action.value="update";
        formObj.submit();
     }
     </SCRIPT>

     <%} else if (index.equals("titleList")) {%>
     <TITLE><%= session.getAttribute("TITLE_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.tlsort.value = sort;
        }
     </SCRIPT>       

     <%} else if (index.equals("titleDetail")) {%>
     <TITLE><%= session.getAttribute("TITLE_TEXT_DESC")%></TITLE>

     <%} else if (index.equals("contentList")) {%>
     <TITLE><%= session.getAttribute("CONTENT_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.consort.value = sort;
        }
     </SCRIPT>       

     <%} else if (index.equals("contentDetail")) {%>
     <TITLE><%= session.getAttribute("CONTENT_TEXT_DESC")%></TITLE>

     <%} else if (index.equals("mcotList")) {%>
     <TITLE><%= session.getAttribute("BB_CONTENT_TYPE_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.bbctsort.value = sort;
        }
     </SCRIPT>       

     <%} else if (index.equals("mcotAdd")) {%>
     <TITLE><%= session.getAttribute("BB_CONTENT_TYPE_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
      function onClickSubmit(formObj) {
         if (isNull(formObj.model.value) || isBlank(formObj.model.value))
         { alert(<%="\"" + session.getAttribute("ALERT_BB_MODEL") + "\""%>); 
           formObj.model.focus();	
           return; }
         if (isNull(formObj.cot.value) || isBlank(formObj.cot.value))
         { alert(<%="\"" + session.getAttribute("ALERT_CONTENT_TYPE") + "\""%>); 
           formObj.cot.focus();	
           return; }
        formObj.desc.value=Trim(formObj.desc.value);
        formObj.submit();
     }
     </SCRIPT>

     <%} else if (index.equals("mcotEdit")) {%>
     <TITLE><%= session.getAttribute("BB_CONTENT_TYPE_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
      function onClickSubmit(formObj) {
        formObj.desc.value=Trim(formObj.desc.value);
        formObj.submit();
     }
     </SCRIPT>

     <%} else if (index.equals("bundleList")) {%>
     <TITLE><%= session.getAttribute("BUNDLE_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.bsort.value = sort;
        }
     </SCRIPT>       

     <%} else if (index.equals("bundleAdd")) {%>
     <TITLE><%= session.getAttribute("BUNDLE_TEXT_DESC")%></TITLE>
     <LINK REL="stylesheet" type="text/css" href="/css/popcalendar.css" />
     <script language="javascript" src="/js/popcalendar.js"></script>
     <SCRIPT LANGUAGE="JavaScript">
      function onClickSubmit(formObj) {
          formObj.limits.value=Trim(formObj.limits.value);
          formObj.sku.value=Trim(formObj.sku.value);

          if (isNull(formObj.buid.value) || isBlank(formObj.buid.value))
          { alert(<%="\"" + session.getAttribute("ALERT_BU_NAME") + "\""%>); 
            formObj.buid.focus();	
            return; }
          if (isNull(formObj.model.value) || isBlank(formObj.model.value))
          { alert(<%="\"" + session.getAttribute("ALERT_BB_MODEL") + "\""%>); 
	    formObj.model.focus();	
	    return; }
          if (isNull(formObj.title.value) || isBlank(formObj.title.value))
          { alert(<%="\"" + session.getAttribute("ALERT_TITLE") + "\""%>); 
            formObj.title.focus();	
            return; }
          if (isNull(formObj.rtype.value) || isBlank(formObj.rtype.value))
          { alert(<%="\"" + session.getAttribute("ALERT_RTYPE") + "\""%>); 
            formObj.rtype.focus();	
            return; }
          if (!isNull(formObj.rtype.value) && (formObj.rtype.value=="PR" || formObj.rtype.value=="XR"))
          { if (!isNull(formObj.limits.value) && !isBlank(formObj.limits.value))
            { alert(<%="\"" + session.getAttribute("ALERT_LIMITS_PR") + "\""%>); 
              formObj.limits.focus();	
              return; }
          }
          if (!isNull(formObj.rtype.value) && formObj.rtype.value=="LR")
          { if (isNull(formObj.limits.value) || isBlank(formObj.limits.value))
            { alert(<%="\"" + session.getAttribute("ALERT_LIMITS_LR") + "\""%>); 
              formObj.limits.focus();	
              return; }
            else {
               if (!isInteger(formObj.limits.value))
               {
                   alert(<%="\"" + session.getAttribute("ALERT_LIMITS_LR") + "\""%>); 
                   formObj.limits.focus();	
                   return; 
               }
            }
          }
          if (!isNull(formObj.rtype.value) && formObj.rtype.value=="TR")
          { if (isNull(formObj.limits.value) || isBlank(formObj.limits.value))
            { alert(<%="\"" + session.getAttribute("ALERT_LIMITS_TR") + "\""%>); 
              formObj.limits.focus();	
              return; }
            else {
               if (!isInteger(formObj.limits.value))
               {
                   alert(<%="\"" + session.getAttribute("ALERT_LIMITS_TR") + "\""%>); 
                   formObj.limits.focus();	
                   return; 
               }
            }
          }
          if (isNull(formObj.start1.value) || isBlank(formObj.start1.value))
          { alert(<%="\"" + session.getAttribute("ALERT_START_DATE") + "\""%>); 
            formObj.start1.focus();	
            return;
          } else {
              if (!isNull(formObj.end1.value) && !isBlank(formObj.end1.value)) {
                if(compareDates(formObj.start1.value, formObj.end1.value) < 0) {
                  alert(<%="\"" + session.getAttribute("ALERT_DATES") + "\""%>);
                  return;
                }
              }
          }
          if (isNull(formObj.sku.value) || isBlank(formObj.sku.value))
          { alert(<%="\"" + session.getAttribute("ALERT_SKU") + "\""%>); 
            formObj.sku.focus();	
            return; }
        formObj.submit();
     }
     </SCRIPT>

     <%} else if (index.equals("bundleEdit")) {%>
     <TITLE><%= session.getAttribute("BUNDLE_TEXT_DESC")%></TITLE>
     <LINK REL="stylesheet" type="text/css" href="/css/popcalendar.css" />
     <script language="javascript" src="/js/popcalendar.js"></script>
     <SCRIPT LANGUAGE="JavaScript">
      function onClickSubmit(formObj, todo) {
       formObj.action.value=todo;
       if (todo!="add")
       {
          formObj.type.value="bundle";

          if (!isNull(formObj.end1.value) && !isBlank(formObj.end1.value))
          { if(compareDates(formObj.start.value, formObj.end1.value) < 0) {
               alert(<%="\"" + session.getAttribute("ALERT_DATES") + "\""%>);
               return;
            }
          } 
       } else {
          formObj.type.value="bundletitle";
       }
       formObj.submit();
     }
     </SCRIPT>

     <%} else if (index.equals("bundletitleEdit")) {%>
     <TITLE><%= session.getAttribute("BUNDLE_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
      function onClickSubmit(formObj) {
          formObj.limits.value=Trim(formObj.limits.value);
          if (isNull(formObj.rtype.value) || isBlank(formObj.rtype.value))
          { alert(<%="\"" + session.getAttribute("ALERT_RTYPE") + "\""%>); 
            formObj.rtype.focus();	
            return; }
          if (!isNull(formObj.rtype.value) && (formObj.rtype.value=="PR" || formObj.rtype.value=="XR"))
          { if (!isNull(formObj.limits.value) && !isBlank(formObj.limits.value))
            { alert(<%="\"" + session.getAttribute("ALERT_LIMITS_PR") + "\""%>); 
              formObj.limits.focus();	
              return; }
          }
          if (!isNull(formObj.rtype.value) && formObj.rtype.value=="LR")
          { if (isNull(formObj.limits.value) || isBlank(formObj.limits.value))
            { alert(<%="\"" + session.getAttribute("ALERT_LIMITS_LR") + "\""%>); 
              formObj.limits.focus();	
              return; }
            else {
               if (!isInteger(formObj.limits.value))
               {
                   alert(<%="\"" + session.getAttribute("ALERT_LIMITS_LR") + "\""%>); 
                   formObj.limits.focus();	
                   return; 
               }
            }
          }
          if (!isNull(formObj.rtype.value) && formObj.rtype.value=="TR")
          { if (isNull(formObj.limits.value) || isBlank(formObj.limits.value))
            { alert(<%="\"" + session.getAttribute("ALERT_LIMITS_TR") + "\""%>); 
              formObj.limits.focus();	
              return; }
            else {
               if (!isInteger(formObj.limits.value))
               {
                   alert(<%="\"" + session.getAttribute("ALERT_LIMITS_TR") + "\""%>); 
                   formObj.limits.focus();	
                   return; 
               }
            }
          }
        formObj.submit();
     }
     </SCRIPT>

     <%} else if (index.equals("bundletitleAdd")) {%>
     <TITLE><%= session.getAttribute("BUNDLE_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
      function onClickSubmit(formObj) {
        formObj.limits.value=Trim(formObj.limits.value);
          if (isNull(formObj.title.value) || isBlank(formObj.title.value))
          { alert(<%="\"" + session.getAttribute("ALERT_TITLE") + "\""%>); 
            formObj.title.focus();	
            return; }
          if (isNull(formObj.rtype.value) || isBlank(formObj.rtype.value))
          { alert(<%="\"" + session.getAttribute("ALERT_RTYPE") + "\""%>); 
            formObj.rtype.focus();	
            return; }
          if (!isNull(formObj.rtype.value) && (formObj.rtype.value=="PR" || formObj.rtype.value=="XR"))
          { if (!isNull(formObj.limits.value) && !isBlank(formObj.limits.value))
            { alert(<%="\"" + session.getAttribute("ALERT_LIMITS_PR") + "\""%>); 
              formObj.limits.focus();	
              return; }
          }
          if (!isNull(formObj.rtype.value) && formObj.rtype.value=="LR")
          { if (isNull(formObj.limits.value) || isBlank(formObj.limits.value))
            { alert(<%="\"" + session.getAttribute("ALERT_LIMITS_LR") + "\""%>); 
              formObj.limits.focus();	
              return; }
            else {
               if (!isInteger(formObj.limits.value))
               {
                   alert(<%="\"" + session.getAttribute("ALERT_LIMITS_LR") + "\""%>); 
                   formObj.limits.focus();	
                   return; 
               }
            }
          }
          if (!isNull(formObj.rtype.value) && formObj.rtype.value=="TR")
          { if (isNull(formObj.limits.value) || isBlank(formObj.limits.value))
            { alert(<%="\"" + session.getAttribute("ALERT_LIMITS_TR") + "\""%>); 
              formObj.limits.focus();	
              return; }
            else {
               if (!isInteger(formObj.limits.value))
               {
                   alert(<%="\"" + session.getAttribute("ALERT_LIMITS_TR") + "\""%>); 
                   formObj.limits.focus();	
                   return; 
               }
            }
          }
        formObj.submit();
     }
     </SCRIPT>

     <%} else if (index.equals("certList")) {%>
     <TITLE><%= session.getAttribute("CERT_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.certsort.value = sort;
        }
     </SCRIPT>       

     <%} else if (index.equals("certEdit")) {%>
     <TITLE><%= session.getAttribute("CERT_TEXT_DESC")%></TITLE>

     <%} else if (index.equals("chainList")) {%>
     <TITLE><%= session.getAttribute("CHAIN_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.chainsort.value = sort;
        }
     </SCRIPT>       

     <%} else if (index.equals("userList")) {%>
     <TITLE><%= session.getAttribute("OPERATION_USER_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.usort.value = sort;
        }
     </SCRIPT>    

     <%} else if (index.equals("userPassword")) {%>
     <TITLE><%= session.getAttribute("OPERATION_USER_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function onClickOK(formObj) {
           formObj.submit();
      }
     </SCRIPT>       
   
     <%} else if (index.equals("userAdd")) {%>
     <TITLE><%= session.getAttribute("OPERATION_USER_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
      function onClickSubmit(formObj) {
         formObj.fullname.value=Trim(formObj.fullname.value);
          if (isNull(formObj.fullname.value) || isBlank(formObj.fullname.value))
          { alert(<%="\"" + session.getAttribute("ALERT_FULLNAME") + "\""%>);
            formObj.fullname.focus();
            return; }

          formObj.email.value=Trim(formObj.email.value);
          if (isNull(formObj.email.value) || isBlank(formObj.email.value))
          { alert(<%="\"" + session.getAttribute("ALERT_EMAIL_ADDRESS") + "\""%>);
            formObj.email.focus();
            return;
          } else {
            var splitted = formObj.email.value.match("^(.+)@(.+)$");
            if (splitted==null)
            {
               var charpos = formObj.email.value.search("[^A-Za-z0-9]");
               if (formObj.email.value.length > 0 && charpos >= 0)
               {
                   alert(<%="\"" + session.getAttribute("ALERT_CHAR_EMAIL_ADDRESS") + "\""%>);
                   formObj.email.focus();
                   return;
               }
            } else {
               if (splitted[1]!=null )
               {
                  var regexp_user=/^\"?[\w-_\.]*\"?$/;
                  if (splitted[1].match(regexp_user)==null)
                  {
                     alert(<%="\"" + session.getAttribute("ALERT_CHAR_EMAIL_ADDRESS") + "\""%>);
                     formObj.email.focus();
                     return;
                  }
               }
               if (splitted[2]!=null)
               {
                  var regexp_domain=/^[\w-\.]*\.[A-Za-z]{2,4}$/;
                  if (splitted[2].match(regexp_domain)==null)
                  {
                     var regexp_ip=/^\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\]$/;
                     if (splitted[2].match(regexp_ip)==null)
                     {
                        alert(<%="\"" + session.getAttribute("ALERT_CHAR_EMAIL_ADDRESS") + "\""%>);
                        formObj.email.focus();
                        return;
                     }
                  }
               }
            }
          }

          if (isNull(formObj.status.value) || isBlank(formObj.status.value))
          { alert(<%="\"" + session.getAttribute("ALERT_STATUS") + "\""%>);
            formObj.status.focus();
            return; }

          if (isNull(formObj.role.value) || isBlank(formObj.role.value))
          { alert(<%="\"" + session.getAttribute("ALERT_ROLE_LEVEL") + "\""%>);
            formObj.role.focus();
            return; }

         formObj.passwd.value = Math.floor(Math.random() * 100000000);
         formObj.submit();
     }
     </SCRIPT>

     <%} else if (index.equals("userEdit")) {%>
     <TITLE><%= session.getAttribute("OPERATION_USER_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
      function showRoles(opid, buid) {
           window.location.href = "serv?type=userrole&action=edit&opid=" + opid + "&buid=" + buid;
           window.location.href.reload();
      }
      function onClickSubmit(formObj) {
         formObj.fullname.value=Trim(formObj.fullname.value);
          if (isNull(formObj.fullname.value) || isBlank(formObj.fullname.value))
          { alert(<%="\"" + session.getAttribute("ALERT_FULLNAME") + "\""%>);
            formObj.fullname.focus();
            return; }

          formObj.email.value=Trim(formObj.email.value);
          if (isNull(formObj.email.value) || isBlank(formObj.email.value))
          { alert(<%="\"" + session.getAttribute("ALERT_EMAIL_ADDRESS") + "\""%>);
            formObj.email.focus();
            return;
          } else {
            var splitted = formObj.email.value.match("^(.+)@(.+)$");
            if (splitted==null)
            {
               var charpos = formObj.email.value.search("[^A-Za-z0-9]");
               if (formObj.email.value.length > 0 && charpos >= 0)
               {
                   alert(<%="\"" + session.getAttribute("ALERT_CHAR_EMAIL_ADDRESS") + "\""%>);
                   formObj.email.focus();
                   return;
               }
            } else {
               if (splitted[1]!=null )
               {
                  var regexp_user=/^\"?[\w-_\.]*\"?$/;
                  if (splitted[1].match(regexp_user)==null)
                  {
                     alert(<%="\"" + session.getAttribute("ALERT_CHAR_EMAIL_ADDRESS") + "\""%>);
                     formObj.email.focus();
                     return;
                  }
               }
               if (splitted[2]!=null)
               {
                  var regexp_domain=/^[\w-\.]*\.[A-Za-z]{2,4}$/;
                  if (splitted[2].match(regexp_domain)==null)
                  {
                     var regexp_ip=/^\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\]$/;
                     if (splitted[2].match(regexp_ip)==null)
                     {
                        alert(<%="\"" + session.getAttribute("ALERT_CHAR_EMAIL_ADDRESS") + "\""%>);
                        formObj.email.focus();
                        return;
                     }
                  }
               }
            }
          }

          if (isNull(formObj.status.value) || isBlank(formObj.status.value))
          { alert(<%="\"" + session.getAttribute("ALERT_STATUS") + "\""%>);
            formObj.status.focus();
            return; }

          if (isNull(formObj.role.value) || isBlank(formObj.role.value))
          { alert(<%="\"" + session.getAttribute("ALERT_ROLE_LEVEL") + "\""%>);
            formObj.role.focus();
            return; }

          if (formObj.confpasswd.value != formObj.passwd.value)
          { alert(<%="\"" + session.getAttribute("ALERT_PASSWORD_MISMATCH") + "\""%>);
            formObj.passwd.focus();
            return; }
          if (formObj.email.value!=formObj.last_email.value)
          { if (!(confirm(<%="\"" + session.getAttribute("ALERT_REVOKE_SMARTCARD_EMAIL") + "\""%>))) {
            formObj.email.focus();
            return; }
          }
          if (formObj.status.value=="I")
          { if (!(confirm(<%="\"" + session.getAttribute("ALERT_REVOKE_SMARTCARD_STATUS") + "\""%>))) {
            formObj.status.focus();
            return; }
          }
         formObj.submit();
     }
     </SCRIPT>

   <%} else if (index.equals("dlrList")) {%>
     <TITLE><%=session.getAttribute("DLR_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.dlrsort.value = sort;
        }
     </SCRIPT>

   <%} else if (index.equals("hwrelList")) {%>
     <TITLE><%=session.getAttribute("BBHR_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.bbhrsort.value = sort;
        }
     </SCRIPT>

   <%} else if (index.equals("ectList")) {%>
     <TITLE><%=session.getAttribute("ECARD_TYPE_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.ectsort.value = sort;
        }
     </SCRIPT>

   <%} else if (index.equals("ecbList")) {%>
     <TITLE><%=session.getAttribute("ECARD_BATCHES_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.ecbsort.value = sort;
        }
     </SCRIPT>

    <%} else if (index.equals("ecDetail")) {%>
     <TITLE><%= session.getAttribute("ECARDS_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function onClickSubmit(formObj) {
          formObj.id.value=Trim(formObj.id.value);
          if (isNull(formObj.id.value) || isBlank(formObj.id.value))
          { alert(<%="\"" + session.getAttribute("ALERT_ECARD_ID") + "\""%>);
            formObj.id.focus();
            return;
          } else {
            if (formObj.id.value.length != 10 || !isInteger(formObj.id.value))
            { alert(<%="\"" + session.getAttribute("ALERT_BAD_ECARD_ID") + "\""%>);
              formObj.id.focus();
              return;
            }
          }
          formObj.submit();
        }
     </SCRIPT>
   <%}%>
        
    <SCRIPT LANGUAGE="javascript" src="/js/functions.js"></SCRIPT>
    <SCRIPT LANGUAGE="JavaScript">
    function getHelp(pageIndex) {

        var helpUrl = pageIndex;                 
        if (pageIndex == "businessUnit") {						// Business Units
            helpUrl = "businessUnits";
        } else if (pageIndex == "bbModel") {						// BB Models
            helpUrl = "bbModel";
        } else if (pageIndex == "contentType") {					// Content Object Type
            helpUrl = "contentType";
        } else if (pageIndex == "bbContentType") {					// BB-Content Object Type Mapping
            helpUrl = "bbContentType";
        } else if (pageIndex == "upload") {					// BB-Content Object Type Mapping
            helpUrl = "upload";
        }
    
        var url = "/help/"+helpUrl;
        var helpWin = window.open("", "_bbhelp","status=no,toolbar=no,scrollbars=yes,resizable=yes,menubar=no,copyhistory=no,directories=no,location=no,width=640,height=600");
        helpWin.focus();  // Put help window on top
        helpWin.location = url;  // Load the URL
    }

   </SCRIPT>

   <LINK rel="stylesheet" type="text/css" href="/css/rms.css" title="Style"/>
</HEAD>

<!-- <body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> -->
<!-- <BODY bgcolor="white" link="#003366" vlink="#003366" alink="#003366" text="black"> -->
<BODY bgcolor="white" text="black">

<TABLE border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="white">
  <!-- 1st Row: Logo Section -->
  <TR width="100%"> 
    <TD colspan="2"> 
      <TABLE border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="white">
        <TR height="39"> 
          <TD width="1%" valign="top" bgcolor="white"><IMG border="0" src="/images/logo_whiteonblue.gif" width="176" height="39"> 
          </TD>
          <TD width="98%" valign="middle" align="right" nowrap style="background-repeat: no-repeat" background="/images/title_background.gif"> 
            <FONT class="titleFont">BCC Management System&nbsp;&nbsp;</FONT></TD>
          <TD width="1%"><IMG border="0" width="1" src="/images/spacer.gif" height="39"></TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
  <!-- 2nd Row: White Line -->
  <TR width="100%" bgcolor=white><TD colspan="2"><IMG border=0 width=1 height=1 src="/images/spacer.gif"></TD></TR>
  <!-- 3rd Row: Menu Navigation Bar -->
  <TR width="100%">
    <TD >
      <!-- Top-Level Menu Bar -->
      <TABLE border=0 cellpadding=0 cellspacing=0 width=100% bgcolor="#efefef">
        <TR>
<%if (index.equals("userList") || index.equals("userAdd") || index.equals("userEdit") || index.equals("userPassword")) {%>
          <TD><IMG src="/images/menu.off.start.gif"></TD>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_OPERATION_USER")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
<%} else {%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;&nbsp;<A class="mainMenuHeader" HREF="serv?type=user&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_OPERATION_USER")%></FONT></A>&nbsp;</TD> 
<%}%>
<%if (index.equals("businessUnitList") || index.equals("businessUnitAdd") || index.equals("businessUnitEdit")) {%>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_BUSINESS_UNIT")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
<%} else {%>
          <%if (!(index.equals("userList") || index.equals("userAdd") || index.equals("userEdit") || index.equals("userPassword"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=businessUnit&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_BUSINESS_UNIT")%></FONT></A>&nbsp;</TD>
<%}%>
<%if (index.equals("modelList") || index.equals("modelAdd") || index.equals("modelEdit") || 
      index.equals("mcotList") || index.equals("mcotAdd") || index.equals("mcotEdit") ||
      index.equals("bundleList") || index.equals("bundleEdit") || index.equals("bundleAdd") || 
      index.equals("bundletitleEdit") || index.equals("bundletitleAdd") ||
      index.equals("hwrelList") || index.equals("playerDetail")) {%>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_BBHW")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
<%} else {%>
          <%if (!(index.equals("businessUnitList") || index.equals("businessUnitAdd") || index.equals("businessUnitEdit"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=model&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_BBHW")%></FONT></A>&nbsp;</TD>
<%}%>
<%if (index.equals("titleList") || index.equals("titleDetail") || 
      index.equals("contentList") || index.equals("contentDetail")) {%>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_CDS")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
<%} else {%>
          <%if (!(index.equals("modelList") || index.equals("modelAdd") || index.equals("modelEdit") || 
                  index.equals("mcotList") || index.equals("mcotAdd") || index.equals("mcotEdit") || 
                  index.equals("bundleList") || index.equals("bundleEdit") || index.equals("bundleAdd") || 
                  index.equals("bundletitleEdit") || index.equals("bundletitleAdd") ||
                  index.equals("hwrelList") || index.equals("playerDetail"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=content&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_CDS")%></FONT></A>&nbsp;</TD> 
<%}%>
<%if (index.equals("ectList") || index.equals("ecbList") ||
      index.equals("ecbEdit") || index.equals("ecDetail")){%>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_ECARDS")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
<%} else {%>
          <%if (!(index.equals("titleList") || index.equals("titleDetail") ||
		  index.equals("contentList") || index.equals("contentDetail"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=ec&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_ECARDS")%></FONT></A>&nbsp;</TD> 
<%}%>
<%if (index.equals("certList") || index.equals("certEdit") || index.equals("chainList")){%>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_CC_BROWSER")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
<%} else {%>
          <%if (!(index.equals("ectList") || index.equals("ecbList") || 
		  index.equals("ecbEdit") || index.equals("ecDetail"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=cert&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_CC_BROWSER")%></FONT></A>&nbsp;</TD> 
<%}%>
<%if (index.equals("dlrList")){%>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_UPLOADS")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
<%} else {%>
          <%if (!(index.equals("certList") || index.equals("certEdit") || index.equals("chainList"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=dlr&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_UPLOADS")%></FONT></A>&nbsp;</TD> 
<%}%>
          <%if (!(index.equals("dlrList"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="javascript:getHelp('<%=index%>')"><FONT class="mainMenuText"><%=session.getAttribute("MENU_HELP")%></FONT></A>&nbsp;</TD>
          <TD bgcolor="#336699" nowrap width="100%" align=right background="/images/menu.off.bg.gif">&nbsp;</TD>
        </TR>
      </TABLE>
     <!-- End of Menu Bar -->
    </TD>
    <TD nowrap align="right" background="/images/menu.off.bg.gif">
      <font class="userText"><%=session.getAttribute("BCC_USER")%>: </font>
      <a class="logoutMenuHeader" href="login?laction=logout" id="logout" target="_top">
      <font class="logoutText"><%=session.getAttribute("TEXT_LOGOUT")%></font></a>&nbsp;&nbsp;
    </TD>
  </TR>
    <!-- Small Gray Bar -->
  <TR width="100%" bgcolor="#efefef">
    <TD colspan="2"><IMG border="0" width="1" height="5" src="/images/spacer.gif"></TD>
  </TR>
</TABLE>

  <!-- Start of 1st Sub-Menu Bar -->

<TABLE width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#efefef">
  <TR width="100%" height="15">
<%if (index.equals("modelList") || index.equals("modelAdd") || index.equals("modelEdit") ||       
      index.equals("mcotList") || index.equals("mcotAdd") || index.equals("mcotEdit") || 
      index.equals("bundleList") || index.equals("bundleEdit") || index.equals("bundleAdd") || 
      index.equals("bundletitleEdit") || index.equals("bundletitleAdd") ||
      index.equals("hwrelList") || index.equals("playerDetail")) {%>
  <%if (index.equals("modelList") || index.equals("modelAdd") || index.equals("modelEdit")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_BB_MODEL")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=model&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_BB_MODEL")%></FONT></A></TD>
  <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("mcotList") || index.equals("mcotAdd") || index.equals("mcotEdit")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_BB_CONTENT_TYPE")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=mcot&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_BB_CONTENT_TYPE")%></FONT></A></TD>
  <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("bundleList") || index.equals("bundleEdit") || index.equals("bundleAdd") ||
        index.equals("bundletitleEdit") || index.equals("bundletitleAdd")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_BB_BUNDLE")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=bundle&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_BB_BUNDLE")%></FONT></A></TD>
  <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("hwrelList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_BBHR")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=hwrel&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_BBHR")%></FONT></A></TD>
  <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("playerDetail")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_BB_PLAYER")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=player&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_BB_PLAYER")%></FONT></A></TD>
  <%}%>
  <!-- End of 1st Sub-Menu Bar for BB Hardware-->

<%} else if (index.equals("titleList") || index.equals("titleDetail") ||
	     index.equals("contentList") || index.equals("contentDetail")) {%>
  <%if (index.equals("contentList") || index.equals("contentDetail")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_CONTENT")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=content&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_CONTENT")%></FONT></A></TD>
  <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("titleList") || index.equals("titleDetail")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_TITLE")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=title&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_TITLE")%></FONT></A></TD>
  <%}%>
  <!-- End of 1st Sub-Menu Bar for Content Browser -->

<%} else if (index.equals("ectList") || index.equals("ecbList") ||
	     index.equals("ecbEdit") || index.equals("ecDetail")) {%>
  <%if (index.equals("ecDetail")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_ECARDS")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ec&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_ECARDS")%></FONT></A></TD>
  <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("ecbList") || index.equals("ecbEdit")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_ECARD_BATCHES")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ecb&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_ECARD_BATCHES")%></FONT></A></TD>
  <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("ectList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_ECARD_TYPE")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ect&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_ECARD_TYPE")%></FONT></A></TD>
  <%}%>
  <!-- End of 1st Sub-Menu Bar for ECards -->

<%} else if (index.equals("certList") || index.equals("certEdit") || index.equals("chainList")) {%>
  <%if (index.equals("certList") || index.equals("certEdit")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_CERTIFICATES")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=cert&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_CERTIFICATES")%></FONT></A></TD>
  <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("chainList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_CHAINS")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=chain&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_CHAINS")%></FONT></A></TD>
  <%}%>
  <!-- End of 1st Sub-Menu Bar for CC Browser -->

<%}%>

   <TD nowrap width="100%" align="right"><A class="subMenuHeader" href="serv?type=home&action=edit" id="currole"><FONT class="subMenuText"><%=session.getAttribute("BCC_ROLE_NAME")%></FONT></A>&nbsp;&nbsp;</TD>
  </TR>
</TABLE>

<!-- Bar under menu -->
<TABLE width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#efefef">
  <TR width="100%">
    <TD><IMG border="0" width="1" height="5" src="/images/spacer.gif"></TD>
  </TR>
  <TR width="100%" bgcolor="#999999">
    <TD><IMG border="0" width="1" height="1" src="/images/spacer.gif"></TD>
  </TR>
</TABLE>
<TABLE width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
  <TR width="100%">
    <TD><IMG border="0" width="1" height="5" src="/images/spacer.gif"></TD>
  </TR>
</TABLE>
<!-- End of bar -->

  <!-- Start of 2nd Sub-Menu Bar -->
<center>
<%if (index.equals("userList") || index.equals("userAdd") || index.equals("userEdit")) {%>
  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">
  <%if (index.equals("userList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_OPUSERLIST")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=user&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_OPUSERLIST")%></FONT></A></TD>
  <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("userAdd")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_OPUSERADD")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=user&action=add"><FONT class="subMenuText"><%=session.getAttribute("MENU_OPUSERADD")%></FONT></A></TD>
  <%}%>
  <%if (index.equals("userEdit")){%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_OPUSERUPDATE")%></FONT></TD>
  <%}%>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for Users -->

<%} else if (index.equals("businessUnitList") || index.equals("businessUnitAdd") || index.equals("businessUnitEdit")) {%>
  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">
  <%if (index.equals("businessUnitList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_BULIST")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=businessUnit&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_BULIST")%></FONT></A></TD>
  <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("businessUnitAdd")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_BUADD")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=businessUnit&action=add"><FONT class="subMenuText"><%=session.getAttribute("MENU_BUADD")%></FONT></A></TD>
  <%}%>
  <%if (index.equals("businessUnitEdit")){%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_BUUPDATE")%></FONT></TD>
  <%}%>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for Business Units -->

<%} else if (index.equals("modelList") || index.equals("modelAdd") || index.equals("modelEdit")) {%>
  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">
  <%if (index.equals("modelList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_BBMLIST")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=model&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_BBMLIST")%></FONT></A></TD>
  <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("modelAdd")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_BBMADD")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=model&action=add"><FONT class="subMenuText"><%=session.getAttribute("MENU_BBMADD")%></FONT></A></TD>
  <%}%>
  <%if (index.equals("modelEdit")){%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_BBMUPDATE")%></FONT></TD>
  <%}%>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for BB Models -->

<%} else if (index.equals("mcotList") || index.equals("mcotAdd") || index.equals("mcotEdit")) {%>
  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">
  <%if (index.equals("mcotList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_BBCTLIST")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=mcot&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_BBCTLIST")%></FONT></A></TD>
  <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("mcotAdd")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_BBCTADD")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=mcot&action=add"><FONT class="subMenuText"><%=session.getAttribute("MENU_BBCTADD")%></FONT></A></TD>
  <%}%>
  <%if (index.equals("mcotEdit")){%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_BBCTUPDATE")%></FONT></TD>
  <%}%>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for BB-Content Object Type -->

<%} else if (index.equals("bundleList") || index.equals("bundleEdit") || index.equals("bundleAdd") ||
             index.equals("bundletitleEdit") || index.equals("bundletitleAdd")) {%>
  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">
  <%if (index.equals("bundleList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_BUNDLES_LIST")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=bundle&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_BUNDLES_LIST")%></FONT></A></TD>
  <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("bundleAdd")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_BUNDLES_ADD")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=bundle&action=add"><FONT class="subMenuText"><%=session.getAttribute("MENU_BUNDLES_ADD")%></FONT></A></TD>
  <%}%>
  <%if (index.equals("bundleEdit")){%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_BUNDLES_DETAIL")%></FONT></TD>
  <%}%>
  <%if (index.equals("bundletitleEdit") || index.equals("bundletitleAdd")){%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_BUNDLES_UPDATE")%></FONT></TD>
  <%}%>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for BB Bundles -->

<%} else if (index.equals("certEdit")) {%>
  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=cert&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_CERT_LIST")%></FONT></A></TD>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_CERT_DETAIL")%></FONT></TD>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for Certificates -->

<%} else if (index.equals("ecbList") || index.equals("ecbEdit")) {%>
  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">
  <%if (index.equals("ecbList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_ECARD_BATCHES_LIST")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ecb&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_ECARD_BATCHES_LIST")%></FONT></A></TD>
  <%}%>
  <%if (index.equals("ecbEdit")){%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_ECARD_BATCHES_DETAIL")%></FONT></TD>
  <%}%>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for ECard Batches -->

<%} else if (index.equals("titleDetail")) {%>
  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=title&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_TLLIST")%></FONT></A></TD>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_TLDETAIL")%></FONT></TD>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for Content Title -->

<%} else if (index.equals("contentDetail")) {%>
  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=content&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_CONLIST")%></FONT></A></TD>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_CONDETAIL")%></FONT></TD>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for Content Object -->

<%}%>
</center>
<p>

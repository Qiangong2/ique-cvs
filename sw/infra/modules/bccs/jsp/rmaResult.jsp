<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.online.UploadBean" %>
<jsp:useBean class="com.broadon.online.UploadBean"
             id="upbean"
             scope="request"/>

<%
  UploadBean rma = (UploadBean)request.getAttribute("upbean");
  String resultStr = rma.getUploadHTML();
%>

<jsp:include page="bcc-upload-header.jsp" flush="true">
    <jsp:param name="page" value="rmaUpload"/>   
</jsp:include>

<P>
<CENTER>

<TABLE cellSpacing=0 cellPadding=1 width=50% bgColor=#336699 border=0>
 <TR bgColor=#336699>
   <TD width=100%>
     <TABLE cellSpacing=0 cellPadding=4 width=100% border=0>
       <TR>
	<TD width=100% bgColor=#336699><FONT color=white><b>Upload Result</b></FONT></TD>
       </TR>
     </TABLE>
   </TD>
  </TR>
<%if (resultStr!=null) {%>
  <TR>
   <TD bgColor=#efefef>
     <TABLE bgColor=#efefef cellSpacing=0 cellPadding=2 width=100% border=0>
       <TR>
	<TD bgcolor=#D8E8F5 color=#333333 align=left width=50%><b>File</b></TD>
        <TD bgcolor=#D8E8F5 color=#333333 align=left width=25%><b>Size</b></TD>
        <TD bgcolor=#D8E8F5 color=#333333 align=left width=25%><b>Result</b></TD>
       </TR>
       <%=resultStr%>
     </TABLE>
   </TD>
 </TR>
<%} else {%>
  <TR>
   <TD bgColor=#efefef><b>No Data Processed</b></TD>
  </TR>
<%}%>
  <TR>
    <TD bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD>
  </TR>
</TABLE>
<P>
<INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_OK")%>" OnClick="self.location.href='rmaUpload'">
</CENTER>
<P>
<jsp:include page="footer.jsp" flush="true"/>


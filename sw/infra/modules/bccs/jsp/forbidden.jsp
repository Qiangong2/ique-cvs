<%@ page contentType="text/html; charset=utf-8" %>
<%
  Object role = session.getAttribute("BCC_ROLE");
  String ROLE_ADMIN = "10";
  String ROLE_ECARD = "40";
%>

<%if (role!=null && (role.toString().equals(ROLE_ADMIN))) {%>
  <jsp:include page="bcc-header.jsp" flush="true">
    <jsp:param name="page" value="home"/>
  </jsp:include>
<%} else if (role!=null && (role.toString().equals(ROLE_ECARD))) {%>
  <jsp:include page="bcc-ecard-header.jsp" flush="true">
    <jsp:param name="page" value="home"/>
  </jsp:include>
<%} else if (role!=null) {%>
  <jsp:include page="bcc-upload-header.jsp" flush="true">
    <jsp:param name="page" value="home"/>
  </jsp:include>
<%}%>

<!-- List Title -->
<P>
<CENTER>
<B><%=session.getAttribute("BCC_FULLNAME")%></B>
<BR>
<FONT class="errorText">
<%=session.getAttribute("TEXT_FORBIDDEN")%>
</FONT>
<BR><BR>
<%=session.getAttribute("TEXT_MESSAGE")%> 
</CENTER>
<P>
<jsp:include page="footer.jsp" flush="true"/>


<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.ms.common.QueryBean" %>
<jsp:useBean class="com.broadon.ms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  Object role = session.getAttribute("BCC_ROLE");
  String ROLE_ADMIN = "10";
  String ROLE_ECARD = "40";

  String err = request.getAttribute("ERROR").toString();
  String success = request.getAttribute("SUCCESS").toString();
  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  String[] params = qb.getParamArray();

  String userStr = "";
  String roleStr = "";
  String email = "";

  if (role.toString().equals(ROLE_ADMIN))
  {
      userStr = htmlResults[0];
      roleStr = htmlResults[2];
      email = params[0];
  } else {
      userStr = htmlResults[1];
      roleStr = htmlResults[3];
      email = params[1];
  }

  String opid = params[2];
%>

<%if (role!=null && (role.toString().equals(ROLE_ADMIN))) {%>
  <jsp:include page="bcc-header.jsp" flush="true">
    <jsp:param name="page" value="userEdit"/>   
  </jsp:include>
<%} else if (role!=null && (role.toString().equals(ROLE_ECARD))) {%>
  <jsp:include page="bcc-ecard-header.jsp" flush="true">
    <jsp:param name="page" value="userEdit"/>   
  </jsp:include>
<%} else if (role!=null) {%>
  <jsp:include page="bcc-upload-header.jsp" flush="true">
    <jsp:param name="page" value="userEdit"/>   
  </jsp:include>
<%}%>

<form name="theForm" id="theForm" action="serv" method="POST" align="center" onSubmit="return false;">
    <input type="hidden" name="type" value="user"></input>
    <input type="hidden" name="action" value="update"></input>
    <input type="hidden" name="last_email" value="<%=email%>"></input>

<%if (err!=null && err!="") {%>
  <center><font class="errorText"><%=err%></font></center><p>
<%} else if (success!=null && success!="") {%>
  <center><font class="successText"><%=success%></font></center><p>
<%}%>

<TABLE cellSpacing=0 cellPadding=1 width=50% align=center bgColor="#336699" border=0>
  <TR> 
    <TD> 
      <!-- OperationUser Detail Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699> 
          <TD width="100%"> 
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              <TR>
                <TD width="50%" bgColor=#336699><FONT class="tblSubHdrLabel2"><%=session.getAttribute("TEXT_OPERATION_USER_DETAIL")%></FONT></TD>
                <%if (role!=null && role.toString().equals(ROLE_ADMIN)) {%>
                 <TD width="50%" bgColor=#336699 align="right">
                      <INPUT class="sbutton" type="button" value="<%=session.getAttribute("TEXT_MANAGE_ROLES")%>" OnClick="showRoles(<%=opid%>, <%=session.getAttribute("BU_ID")%>);">
                 </TD>
                <%}%>
              </TR>
             </TABLE>
          </TD>
        </TR>
        <TR> 
          <TD bgColor=#efefef>
            <TABLE bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <TR>
                <TD class="tableheader" align=center width=50%><%=session.getAttribute("COL_PARAMETER")%></TD>
                <TD class="tableheader" align=center width=50% colspan=2><%=session.getAttribute("COL_VALUE")%></TD>
              </TR>
                <%=userStr%>
	      <tr><td class="formLabel2" nowrap="true"><%=session.getAttribute("TEXT_DEFAULT_ROLE")%></td><td class="formField"></td>
                  <td class="formField" nowrap="true">
                     <select name="role"><option value=""></option><%=roleStr%></select>
                     <font color="red">*</font>
                  </td>
              </tr>
              <TR><TD class="formLabel2"><IMG border=0 height=1 src="/images/spacer.gif"></TD><TD class="formField" colspan=2><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
              <TR><TD colspan="3" bgcolor="white">
                <CENTER><FONT color="red" size="-1"><%=session.getAttribute("TEXT_REQUIRED")%></FONT></CENTER>
              </TD></TR>
              <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
              <TR>
                <TD colspan="3" bgcolor="white">
                <CENTER>
                <INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_SUBMIT")%>" OnClick="onClickSubmit(theForm);">
                <INPUT class="sbutton" type="reset" value="<%=session.getAttribute("BUTTON_RESET")%>">
                <INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_CANCEL")%>" OnClick="history.go(-1)">
                </CENTER>
                </TD>
              </TR>
              <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
</FORM name="theForm">

<jsp:include page="footer.jsp" flush="true"/>

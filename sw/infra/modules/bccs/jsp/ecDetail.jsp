<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.ms.common.QueryBean" %>
<jsp:useBean class="com.broadon.ms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  Object role = session.getAttribute("BCC_ROLE");
  String ROLE_ADMIN = "10";
  String ROLE_ECARD = "40";

  String err = request.getAttribute("ERROR").toString();
  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  String[] params = qb.getParamArray();

  String ecStr = "";
  String ecBatch = "";
  String id = "";

  if (htmlResults!=null) {
      ecStr = htmlResults[0];
      ecBatch = htmlResults[1];
      if (ecStr != null)
          ecStr = ecStr.trim();
  }

  if (params!=null) {
      id = params[0];
      if (id == null)
          id = "";
  }
 
%>

<%if (role!=null && (role.toString().equals(ROLE_ADMIN))) {%>
  <jsp:include page="bcc-header.jsp" flush="true">
    <jsp:param name="page" value="ecDetail"/>
  </jsp:include>
<%} else if (role!=null && (role.toString().equals(ROLE_ECARD))) {%>
  <jsp:include page="bcc-ecard-header.jsp" flush="true">
    <jsp:param name="page" value="ecDetail"/>
  </jsp:include>
<%}%>

<%if (err!=null && err!="") {%><center><font class="errorText"><%=err%></font></center><p><%}%>

<form name="theForm" id="theForm" action="serv" method="POST" align="center" onSubmit="return false;">
    <input type="hidden" name="type" value="ec"></input>
    <input type="hidden" name="action" value="edit"></input>

<CENTER>
 <TABLE border=0>
  <TR>
   <TD><B><%=session.getAttribute("TEXT_ECARD_ID")%></B></TD>
   <TD><INPUT type="text" name="id" size="16" value="<%=id%>"></TD>
   <TD><INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_QUERY")%>" OnClick="onClickSubmit(theForm);"></TD>
  </TR>
 </TABLE>
</CENTER>
<%if (ecStr!=null && !ecStr.equals("")) {%>
<BR>
<P>
<TABLE cellSpacing=0 cellPadding=1 width=50% align=center bgColor="#336699" border=0>
  <TR> 
    <TD> 
      <!-- ECard Detail Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699> 
          <TD width="100%"> 
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              <TR> 
                <TD width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2"><%=session.getAttribute("TEXT_ECARDS_DETAIL")%></FONT></TD>
              </TR>
             </TABLE>
          </TD>
        </TR>
        <TR> 
          <TD bgColor=#efefef>
            <TABLE bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
	      <TR><TD class="formLabel2" colspan=3><IMG border=0 height=2 src="/images/spacer.gif"></TD></TR>
              <TR><TD class="formLabel4" width=100% colspan=3><%=id%></TD></TR>
              <TR><TD class="formLabel2" colspan=3><IMG border=0 height=2 src="/images/spacer.gif"></TD></TR>
              <TR>
                <TD class="tableheader" align=center width=50%><%=session.getAttribute("COL_PARAMETER")%></TD>
                <TD class="tableheader" align=center width=50% colspan=2><%=session.getAttribute("COL_VALUE")%></TD>
              </TR>
              <%=ecBatch%>
              <%=ecStr%>
              <TR><TD class="formLabel2" width=50%><IMG border=0 height=1 src="/images/spacer.gif"></TD><TD class="formField" width=50% colspan=2><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
              <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
<%} else if (id!=null && !id.equals("")) {%>
<BR><BR>
<P>
<CENTER>
   <%=session.getAttribute("TEXT_RESULT")%>:&nbsp;<%=session.getAttribute("TEXT_EMPTY")%>
</CENTER>
<%}%>

</FORM name="theForm">

<jsp:include page="footer.jsp" flush="true"/>

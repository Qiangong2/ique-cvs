<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.ms.common.QueryBean" %>
<jsp:useBean class="com.broadon.ms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();
  String success = request.getAttribute("SUCCESS").toString();

  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  String[] params = qb.getParamArray();
  int[] counts = qb.getCountArray();

  String opid = params[0];
  String buid = params[1];
  String userName = params[2];
  String userLogin = params[3];

  String curStr = htmlResults[0];
  String newStr = htmlResults[1];

  int curCount = counts[0];
  int newCount = counts[1];

  String reqStr = "&nbsp;&nbsp;<font color=\"red\">*</font>";

  if (newStr!=null) newStr = newStr.trim();
  if (curStr!=null) curStr = curStr.trim();
%>

<jsp:include page="bcc-header.jsp" flush="true">
    <jsp:param name="page" value="userEdit"/>
</jsp:include>

<%if (err!=null && err!="") {%>
   <center><font class="errorText"><%=err%></font></center><p>
<%} else if (success!=null && success!="") {%>
   <center><font class="successText"><%=success%></font></center><p>
<%}%>

<form name="theForm" id="theForm" action="serv" method="POST" align="center" onSubmit="return false;">
    <input type="hidden" name="type" value="userrole"></input>
    <input type="hidden" name="action" value="update"></input>
    <input type="hidden" name="opid" value="<%=opid%>"></input>
    <input type="hidden" name="buid" value="<%=buid%>"></input>
    <input type="hidden" name="curnum" value="<%=curCount%>"></input>
    <input type="hidden" name="newnum" value="<%=newCount%>"></input>

<!-- Content Name -->
<TABLE cellSpacing=0 cellPadding=1 width=45% align=center bgcolor="white" border=0>
  <TR> 
    <TD><b><%=session.getAttribute("COL_NAME")%>:&nbsp;</b><%=userName%></TD>
  </TR>
  <TR>
    <TD><b><%=session.getAttribute("COL_LOGIN")%>:&nbsp;</b><%=userLogin%></TD>
  </TR>
</TABLE>
<br>
<TABLE cellSpacing=0 cellPadding=1 width=45% align=center bgcolor="#336699" border=0>
  <TR> 
    <TD> 
      <!-- OperationUserRoles Detail Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" border=0>
        <TR bgColor=#336699> 
          <TD width="100%"> 
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              <TR>
                <TD bgColor=#336699><FONT class="tblSubHdrLabel2"><%=session.getAttribute("TEXT_USER_ROLES_DETAIL")%></FONT></TD>
              </TR>
             </TABLE>
          </TD>
        </TR>
        <TR> 
          <TD>
            <TABLE cellSpacing=1 cellPadding=2 width="100%" border=0 bgcolor="white">
                <%if ((curStr != null && !curStr.equals("")) || (newStr != null && !newStr.equals(""))) {%>
                <TR>
                  <TD class="tableheader" align=left width=15%><%=session.getAttribute("COL_ADD")%><%=reqStr%></TD>
                  <TD class="tableheader" align=left width=50%><%=session.getAttribute("COL_ROLE")%></TD>
                  <TD class="tableheader" align=left width=20%><%=session.getAttribute("COL_ACTIVE")%><%=reqStr%></TD>
                  <TD class="tableheader" width=15%></TD>
                </TR>
                <%=curStr%>
                <%=newStr%>
                <TR><TD colspan="4" bgcolor="white"><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
                <TR><TD colspan="4" bgcolor="white">
                  <CENTER><FONT color="red" size="-1">* Required</FONT></CENTER>
                </TD></TR>
                <TR><TD colspan="5" bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
                <TR>
                  <TD colspan="5" bgcolor="white">
                  <CENTER>
                   <INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_SUBMIT")%>" OnClick="submit();">
                   <INPUT class="sbutton" type="reset" value="<%=session.getAttribute("BUTTON_RESET")%>">
                <%} else {%>
                    <TR><TD colspan="5" bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
                    <TR>
                    <TD align=center colspan=5 bgcolor="white">
                        <%=session.getAttribute("TEXT_NO_ROLES")%>
                    </TD>
                    </TR>
                    <TR><TD colspan="5" bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
                    <TR>
                     <TD colspan="5" bgcolor="white">
                     <CENTER>
               <%}%>
                <INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_CANCEL")%>" OnClick="history.go(-1)">
                </CENTER>
                </TD>
               </TR>
               <TR><TD colspan="5" bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
</FORM name="theForm">

<jsp:include page="footer.jsp" flush="true"/>


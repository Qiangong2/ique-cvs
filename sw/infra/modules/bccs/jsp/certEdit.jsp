<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.ms.common.QueryBean" %>
<jsp:useBean class="com.broadon.ms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  Object role = session.getAttribute("BCC_ROLE");
  String ROLE_ADMIN = "10";
  String ROLE_CA = "50";

  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();

  String certStr = htmlResults[0];
  String certParentStr = htmlResults[1];
%>

<%if (role!=null && (role.toString().equals(ROLE_ADMIN))) {%>
  <jsp:include page="bcc-header.jsp" flush="true">
    <jsp:param name="page" value="certEdit"/>   
  </jsp:include>
<%} else if (role!=null && (role.toString().equals(ROLE_CA))) {%>
  <jsp:include page="bcc-upload-header.jsp" flush="true">
    <jsp:param name="page" value="certEdit"/>   
  </jsp:include>
<%}%>

<TABLE cellSpacing=0 cellPadding=1 width=50% align=center bgColor="#336699" border=0>
  <TR> 
    <TD> 
      <!-- Certificate Detail Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699> 
          <TD width="100%"> 
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              <TR> 
                <TD width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2"><%=session.getAttribute("TEXT_CERT_DETAIL")%></FONT></TD>
              </TR>
             </TABLE>
          </TD>
        </TR>
        <TR> 
          <TD bgColor=#efefef>
            <TABLE bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <TR>
                <TD class="tableheader" align=center width=50%><%=session.getAttribute("COL_PARAMETER")%></TD>
                <TD class="tableheader" align=center width=50% colspan=2><%=session.getAttribute("COL_VALUE")%></TD>
              </TR>
              <%=certStr%>
              <%=certParentStr%>
              <TR><TD class="formLabel2"><IMG border=0 height=1 src="/images/spacer.gif"></TD><TD class="formField" colspan=2><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
              <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
              <TR>
                <TD colspan="3" bgcolor="white" align="left">
                <INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_CANCEL")%>" OnClick="history.go(-1)">
                </TD>
              </TR>
              <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>

<jsp:include page="footer.jsp" flush="true"/>


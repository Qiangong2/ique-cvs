<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.ms.common.QueryBean" %>
<jsp:useBean class="com.broadon.ms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();
  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  String[] params = qb.getParamArray();

  String titleStr = htmlResults[0];

  String buid = params[0];
  String buStr = params[1];

  int flag = 0;

  if (titleStr!=null && !titleStr.equals("")) {
      titleStr.trim();
      if (titleStr.indexOf("option") < 0)
          flag = 1;
  } else
      flag = 1;
%> 
%>

<jsp:include page="bcc-upload-header.jsp" flush="true">
    <jsp:param name="page" value="geDetail"/>   
    <jsp:param name="display" value="add"/>   
</jsp:include>

<%if (err!=null && err!="") {%><center><font class="errorText"><%=err%></font></center><p><%}%>

<form name="theForm" id="theForm" action="serv" method="POST" onSubmit="return false;">
<input type="hidden" name="type" value="ge"></input>
<input type="hidden" name="action" value="insert"></input>
<input type="hidden" name="buid" value=<%=buid%>></input>

<TABLE cellSpacing=0 cellPadding=1 width=50% align=center bgColor="#336699" border=0>
  <TR> 
    <TD> 
      <!-- Global ETickets Detail Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699> 
          <TD width="100%"> 
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              <TR> 
                <TD width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2"><%=session.getAttribute("TEXT_ADD_GLOBAL_ETICKETS")%></FONT></TD>
              </TR>
             </TABLE>
          </TD>
        </TR>
        <TR> 
          <TD bgColor=#efefef>
            <TABLE bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <TR>
                <TD class="tableheader" align=center width=50%><%=session.getAttribute("COL_PARAMETER")%></TD>
                <TD class="tableheader" align=center width=50% colspan=2><%=session.getAttribute("COL_VALUE")%></TD>
              </TR>
              <tr><td class="formLabel2" nowrap="true"><%=session.getAttribute("COL_BU")%>:</td><td class="formField"></td>
                  <td class="formField" nowrap="true"><%=buStr%></td>
              </tr>
              <tr><td class="formLabel2" nowrap="true"><%=session.getAttribute("COL_TITLE")%>:</td><td class="formField"></td>
                  <td class="formField" nowrap="true">
                  <%if (flag!=1) {%>
                        <select name="title"><option value=""></option><%=titleStr%></select>
                        <font color="red">*</font>
                  <%} else {%>
		      <%=session.getAttribute("TEXT_NO_TITLE")%>
		  <%}%>
                  </td>
              </tr>
              <TR><TD class="formLabel2"><IMG border=0 height=1 src="/images/spacer.gif"></TD><TD class="formField" colspan=2><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
              <%if (flag!=1) {%>
                <TR><TD colspan="3" bgcolor="white">
                  <CENTER><FONT color="red" size="-1">* Required</FONT></CENTER>
                </TD></TR>
              <%}%>
              <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
              <TR>
                <TD colspan="3" bgcolor="white">
                <CENTER>
                <%if (flag!=1) {%>
                  <INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_SUBMIT")%>" OnClick="onClickSubmit(theForm);">
                  <INPUT class="sbutton" type="reset" value="<%=session.getAttribute("BUTTON_RESET")%>">
   	        <%}%>
                <INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_CANCEL")%>" OnClick="history.go(-1)">
                </CENTER>
                </TD>
              </TR>
              <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
</FORM name="theForm">

<jsp:include page="footer.jsp" flush="true"/>


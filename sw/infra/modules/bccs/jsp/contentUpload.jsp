<%@ page contentType="text/html; charset=utf-8" %>
<jsp:include page="bcc-upload-header.jsp" flush="true">
    <jsp:param name="page" value="contentUpload"/>   
</jsp:include>

<FORM name="theForm" enctype="multipart/form-data" action="contentUpload" method="POST" onSubmit="return false;">
<INPUT type="hidden" name="client" value="CONTENT"></INPUT>

<!-- List Title -->
<P>
<CENTER>
<TABLE id="uForm" cellSpacing=0 cellPadding=1 width=35% bgColor=#336699 border=0>
 <TR bgColor=#336699>
     <TD width=100%>
	<TABLE cellSpacing=0 cellPadding=4 width=100% border=0>
	  <TR>
		<TD width=100% class="tblSubHdrLabel2"><%=session.getAttribute("TEXT_CONTENT_UPLOAD")%></TD>
  	  </TR>
        </TABLE>
      </TD>
 </TR>
 <TR>
     <TD bgColor=#efefef>
        <TABLE bgColor=#efefef cellSpacing=0 cellPadding=2 width=100% border=0>
          <TR>
		<TD bgcolor=#D8E8F5 color=#333333 align=right width=40% nowrap="true"><b>Package file:</b></TD>
                <TD width=60%><INPUT type="file" size="30" name="file1" onKeyDown="return false;"></TD>
          </TR>
          <TR>
		<TD colspan=2 align=center>
			<INPUT type="button" name="uploadservlet" value="Upload" onClick="ValidateForm(theForm);"> 
                        <INPUT type="reset" value="Clear">
		</TD>
          </TR>
        </TABLE>
     </TD>
  </TR>
</TABLE>
</CENTER>
</FORM>
<p>
<jsp:include page="footer.jsp" flush="true"/>

<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.ms.common.QueryBean" %>
<jsp:useBean class="com.broadon.ms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String resultStr = qb.getResult();
%>

<jsp:include page="bcc-upload-header.jsp" flush="true">
    <jsp:param name="page" value="contentUpload"/>   
</jsp:include>

<P>
<CENTER>

<TABLE cellSpacing=0 cellPadding=1 width=75% bgColor=#336699 border=1>
 <TR bgColor=#336699>
   <TD width=100%>
     <TABLE cellSpacing=0 cellPadding=4 width=100% border=0>
       <TR>
	<TD bgcolor=#336699 width=80%><FONT color=white><b>File</b></FONT></TD>
        <TD bgcolor=#336699 width=20%><FONT color=white><b>Size</b></FONT></TD>
       </TR>
     </TABLE>
   </TD>
  </TR>
<%if (resultStr!=null) {%>
  <TR>
   <TD bgColor=#efefef>
     <TABLE bgColor=#efefef cellSpacing=0 cellPadding=2 width=100% border=0>
       <%=resultStr%>
     </TABLE>
   </TD>
 </TR>
<%} else {%>
  <TR>
   <TD bgColor=#efefef align="center"><b>No Data Processed</b></TD>
  </TR>
<%}%>
</TABLE>
<P>
<INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_OK")%>" OnClick="self.location.href='contentUpload'">
</CENTER>
<P>
<jsp:include page="footer.jsp" flush="true"/>


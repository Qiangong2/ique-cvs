<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.ms.common.QueryBean" %>
<jsp:useBean class="com.broadon.ms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  Object role = session.getAttribute("BCC_ROLE");
  String ROLE_ADMIN = "10";
  String ROLE_CONTENT = "60";

  String err = request.getAttribute("ERROR").toString();
  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  String[] params = qb.getParamArray();
  int[] counts = qb.getCountArray();

  String titleStr = htmlResults[0];
  String conName = params[0];
  String conID = params[1];

  int mCount = counts[0];
  String metadata = "No";
  if (mCount > 0)
      metadata = "Yes";

  String[] titleHeader = {"COL_TITLE", "COL_TITLE_ID", "COL_TITLE_TYPE", "COL_CATEGORY", "COL_PUBLISHER", "COL_DEVELOPER", "COL_LAST_UPDATED"};
%>

<%if (role!=null && (role.toString().equals(ROLE_ADMIN))) {%>
  <jsp:include page="bcc-header.jsp" flush="true">
    <jsp:param name="page" value="contentDetail"/>   
  </jsp:include>
<%} else if (role!=null && (role.toString().equals(ROLE_CONTENT))) {%>
  <jsp:include page="bcc-upload-header.jsp" flush="true">
    <jsp:param name="page" value="contentDetail"/>   
  </jsp:include>
<%}%>

<%if (err!=null && err!="") {%><center><font class="errorText"><%=err%></font></center><p><%}%>

<!-- Content Name -->
<b><%=session.getAttribute("COL_NAME")%>:&nbsp;</b><%=conName%><br>
<b><%=session.getAttribute("COL_ID")%>:&nbsp;</b><%=conID%><br>
<b><%=session.getAttribute("COL_HAS_METADATA")%>:&nbsp;</b><%=metadata%>

<p>
<!-- List Contents -->
<TABLE cellSpacing=0 cellPadding=1 width=100% align=center bgColor="#336699" border=0>
  <TR>
    <TD>
      <!-- Regions Detail Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699>
          <TD width="100%">
            <TABLE width="100%" align="center" cellspacing="1" cellpadding="4" bgcolor="#336699">
              <TR><TD width=100% class="tblSubHdrLabel2">
                <%=session.getAttribute("TEXT_TITLES_LIST")%>
	      </TD></TR>
            </TABLE>
          </TD>
        </TR>
        <TR>
          <TD bgColor=#efefef>
<table border="0" width="100%" align="center" cellspacing="1" cellpadding="4">
<%if (titleStr!=null && titleStr.indexOf("<tr")>=0) {%>
<tr>
<td class="tableheader" nowrap="true"><%=session.getAttribute("COL_NO")%></td>
  <%for (int i=0; i<titleHeader.length; i++) {%>
    <td class="tableheader" nowrap="true">
      <%=session.getAttribute(titleHeader[i])%>
    </td>
  <%}%>
</tr>
<%=titleStr%>
<%} else {%>
 <TR width="100%">
    <TD>
 <!-- No Title Found -->
      <P>
      <CENTER>
       <%=session.getAttribute("TEXT_NO_TITLE")%>
      </CENTER>
    </TD>
  </TR>
<%}%>
</table>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>

<P>
<CENTER>
  <INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_CANCEL")%>" OnClick="history.go(-1)">
</CENTER>

<jsp:include page="footer.jsp" flush="true"/>


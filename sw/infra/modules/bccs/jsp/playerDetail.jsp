<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.ms.common.QueryBean" %>
<jsp:useBean class="com.broadon.ms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();
  String success = request.getAttribute("SUCCESS").toString();
  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  String[] params = qb.getParamArray();

  String playerStr = "";
  String modelStr = "";
  String id = "";
  String bu = "";
  int buid = -1;

  if (params!=null) {
      id = params[0];
      bu = params[1];
      if (id == null)
          id = "";
  }

  if (htmlResults!=null) {
      if (bu!=null && !bu.equals(""))
          buid = Integer.parseInt(bu);

      if (buid >= 0 && buid <= 100) {
          playerStr = htmlResults[0];
          modelStr = htmlResults[2];
      } else if (buid > 100 && buid <= 200) {
          playerStr = htmlResults[1];
          modelStr = htmlResults[3];
      }

      if (playerStr != null)
          playerStr = playerStr.trim();
      if (modelStr != null)
          modelStr = modelStr.trim();
  }
%>

<jsp:include page="bcc-header.jsp" flush="true">
    <jsp:param name="page" value="playerDetail"/>   
</jsp:include>

<form name="theForm" id="theForm" action="serv" method="POST" onSubmit="return false;">
<input type="hidden" name="type" value="player"></input>
<input type="hidden" name="action" value="edit"></input>
<input type="hidden" name="buid" value="<%=buid%>"></input>

<CENTER>
 <TABLE border=0>
  <TR>
   <TD><B><%=session.getAttribute("TEXT_BB_ID")%></B></TD>
   <TD><INPUT type="text" name="id" size="16" value="<%=id%>"></TD>
   <TD> <INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_BB_PLAYER_DETAILS")%>" OnClick="onClickSearch(theForm);"></TD>
  </TR>
 </TABLE>

<%if (err!=null && err!="") {%>
  <center><font class="errorText"><%=err%></font></center><p>
<%} else if (success!=null && success!="") {%>
  <center><font class="successText"><%=success%></font></center><p>
<%}%>

<%if (playerStr!=null && !playerStr.equals("")) {%>
<BR>

<P>
<TABLE cellSpacing=0 cellPadding=1 width=50% align=center bgColor="#336699" border=0>
  <TR> 
    <TD> 
      <!-- Player Detail Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699> 
          <TD width="100%"> 
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              <TR> 
                <TD width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2"><%=session.getAttribute("TEXT_BB_PLAYERS_DETAIL")%></FONT></TD>
              </TR>
             </TABLE>
          </TD>
        </TR>

        <TR> 
          <TD bgColor=#efefef>
            <TABLE bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
	      <TR><TD class="formLabel2" colspan=3><IMG border=0 height=2 src="/images/spacer.gif"></TD></TR>
              <TR><TD class="formLabel4" width=100% colspan=3><%=id%></TD></TR>
              <TR><TD class="formLabel2" colspan=3><IMG border=0 height=2 src="/images/spacer.gif"></TD></TR>
              <TR>
                <TD class="tableheader" align=center width=50%><%=session.getAttribute("COL_PARAMETER")%></TD>
                <TD class="tableheader" align=center width=50% colspan=2><%=session.getAttribute("COL_VALUE")%></TD>
              </TR>
              <TR><TD class="formLabel2" nowrap="true">Model:</TD><TD class="formField"></TD>
                 <TD class="formField" nowrap="true">
                    <select name="model"><option value=""></option><%=modelStr%></select><font color="red">*</font>
                 </TD>
              </TR>
              <%=playerStr%>
              <TR><TD class="formLabel2" width=50%><IMG border=0 height=1 src="/images/spacer.gif"></TD><TD class="formField" width=50% colspan=2><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
              <TR><TD colspan="3" bgcolor="white">
                <CENTER><FONT color="red" size="-1">* Required</FONT></CENTER>
              </TD></TR>
              <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
              <TR>
                <TD colspan="3" bgcolor="white">
                <CENTER>
                <INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_SUBMIT")%>" OnClick="onClickSubmit(theForm, '<%=id%>');">
                <INPUT class="sbutton" type="reset" value="<%=session.getAttribute("BUTTON_RESET")%>">
                <INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_CANCEL")%>" OnClick="history.go(-1)">
                </CENTER>
                </TD>
              </TR>
              <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
<%} else if (id!=null && !id.equals("")) {%>
<BR><BR>
<P>
<CENTER>
   <%=session.getAttribute("TEXT_RESULT")%>:&nbsp;<%=session.getAttribute("TEXT_EMPTY")%>
</CENTER>
<%}%>

</CENTER>

</FORM name="theForm">

<jsp:include page="footer.jsp" flush="true"/>


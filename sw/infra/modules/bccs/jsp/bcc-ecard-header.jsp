<%
  Object role = session.getAttribute("BCC_ROLE");
   String ROLE_ADMIN = "10";
   String ROLE_NEC = "20";
   String ROLE_CA = "50";
   String ROLE_CONTENT = "60";

   String index=request.getParameter("page");
   if (index==null) {
     index="home";
   }
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">

   <%if (index.equals("home")) {%>
     <TITLE><%= session.getAttribute("HOME_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
      function onClickSubmit(formObj) {
          if (isNull(formObj.newrole.value) || isBlank(formObj.newrole.value))
          { alert(<%="\"" + session.getAttribute("ALERT_CHANGE_ROLE") + "\""%>);
            formObj.newrole.focus();
            return; }
        formObj.submit();
     }
     </SCRIPT>

   <%} else if (index.equals("ectList")) {%>
     <TITLE><%=session.getAttribute("ECARD_TYPE_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.ectsort.value = sort;
        }
     </SCRIPT>

   <%} else if (index.equals("ecbList")) {%>
     <TITLE><%=session.getAttribute("ECARD_BATCHES_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.ecbsort.value = sort;
        }
     </SCRIPT>

    <%} else if (index.equals("ecDetail")) {%>
     <TITLE><%= session.getAttribute("ECARDS_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function onClickSubmit(formObj) {
          formObj.id.value=Trim(formObj.id.value);
          if (isNull(formObj.id.value) || isBlank(formObj.id.value))
          { alert(<%="\"" + session.getAttribute("ALERT_ECARD_ID") + "\""%>);
            formObj.id.focus();
            return;
          } else {
            if (formObj.id.value.length != 10 || !isInteger(formObj.id.value))
            { alert(<%="\"" + session.getAttribute("ALERT_BAD_ECARD_ID") + "\""%>);
              formObj.id.focus();
              return;
            }
          }
          formObj.submit();
        }
     </SCRIPT>

   <%} else if (index.equals("userEdit")) {%>
     <TITLE><%= session.getAttribute("OPERATION_USER_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
      function onClickSubmit(formObj) {
         formObj.fullname.value=Trim(formObj.fullname.value);
          if (isNull(formObj.fullname.value) || isBlank(formObj.fullname.value))
          { alert(<%="\"" + session.getAttribute("ALERT_FULLNAME") + "\""%>);
            formObj.fullname.focus();
            return; }

          formObj.email.value=Trim(formObj.email.value);
          if (isNull(formObj.email.value) || isBlank(formObj.email.value))
          { alert(<%="\"" + session.getAttribute("ALERT_EMAIL_ADDRESS") + "\""%>);
            formObj.email.focus();
            return;
          } else {
            var splitted = formObj.email.value.match("^(.+)@(.+)$");
            if (splitted==null)
            {
               var charpos = formObj.email.value.search("[^A-Za-z0-9]");
               if (formObj.email.value.length > 0 && charpos >= 0)
               {
                   alert(<%="\"" + session.getAttribute("ALERT_CHAR_EMAIL_ADDRESS") + "\""%>);
                   formObj.email.focus();
                   return;
               }
            } else {
               if (splitted[1]!=null )
               {
                  var regexp_user=/^\"?[\w-_\.]*\"?$/;
                  if (splitted[1].match(regexp_user)==null)
                  {
                    alert(<%="\"" + session.getAttribute("ALERT_CHAR_EMAIL_ADDRESS") + "\""%>);
                     formObj.email.focus();
                     return;
                  }
               }
               if (splitted[2]!=null)
               {
                  var regexp_domain=/^[\w-\.]*\.[A-Za-z]{2,4}$/;
                  if (splitted[2].match(regexp_domain)==null)
                  {
                     var regexp_ip=/^\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\]$/;
                     if (splitted[2].match(regexp_ip)==null)
                     {
                        alert(<%="\"" + session.getAttribute("ALERT_CHAR_EMAIL_ADDRESS") + "\""%>);
                        formObj.email.focus();
                        return;
                     }
                  }
               }
            }
          }

          if (isNull(formObj.role.value) || isBlank(formObj.role.value))
          { alert(<%="\"" + session.getAttribute("ALERT_ROLE_LEVEL") + "\""%>);
            formObj.role.focus();
            return; }
          if (formObj.confpasswd.value != formObj.passwd.value)
          { alert(<%="\"" + session.getAttribute("ALERT_PASSWORD_MISMATCH") + "\""%>);
            formObj.passwd.focus();
            return; }
          if (formObj.email.value!=formObj.last_email.value)
          { if (!(confirm(<%="\"" + session.getAttribute("ALERT_REVOKE_SMARTCARD_EMAIL") + "\""%>))) {
            formObj.email.focus();
            return; }
          }
        formObj.submit();
     }
     </SCRIPT>
   <%}%>
        
    <SCRIPT LANGUAGE="javascript" src="/js/functions.js"></SCRIPT>
    <SCRIPT LANGUAGE="JavaScript">
    function getHelp(pageIndex) {

        var helpUrl = pageIndex;                 
        if (pageIndex == "businessUnit") {						// Business Units
            helpUrl = "businessUnits";
        } else if (pageIndex == "bbModel") {						// BB Models
            helpUrl = "bbModel";
        } else if (pageIndex == "contentType") {					// Content Object Type
            helpUrl = "contentType";
        } else if (pageIndex == "bbContentType") {					// BB-Content Object Type Mapping
            helpUrl = "bbContentType";
        } else if (pageIndex == "upload") {					// BB-Content Object Type Mapping
            helpUrl = "upload";
        }
    
        var url = "/help/"+helpUrl;
        var helpWin = window.open("", "_bbhelp","status=no,toolbar=no,scrollbars=yes,resizable=yes,menubar=no,copyhistory=no,directories=no,location=no,width=640,height=600");
        helpWin.focus();  // Put help window on top
        helpWin.location = url;  // Load the URL
    }

   </SCRIPT>

   <LINK rel="stylesheet" type="text/css" href="/css/rms.css" title="Style"/>
</HEAD>

<!-- <body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> -->
<!-- <BODY bgcolor="white" link="#003366" vlink="#003366" alink="#003366" text="black"> -->
<BODY bgcolor="white" text="black">

<TABLE border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="white">
  <!-- 1st Row: Logo Section -->
  <TR width="100%"> 
    <TD colspan="2"> 
      <TABLE border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="white">
        <TR height="39"> 
          <TD width="1%" valign="top" bgcolor="white"><IMG border="0" src="/images/logo_whiteonblue.gif" width="176" height="39"> 
          </TD>
          <TD width="98%" valign="middle" align="right" nowrap style="background-repeat: no-repeat" background="/images/title_background.gif"> 
            <FONT class="titleFont">BCC Management System&nbsp;&nbsp;</FONT></TD>
          <TD width="1%"><IMG border="0" width="1" src="/images/spacer.gif" height="39"></TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
  <!-- 2nd Row: White Line -->
  <TR width="100%" bgcolor=white><TD colspan="2"><IMG border=0 width=1 height=1 src="/images/spacer.gif"></TD></TR>
  <!-- 3rd Row: Menu Navigation Bar -->
  <TR width="100%">
    <TD >
      <!-- Top-Level Menu Bar -->
      <TABLE border=0 cellpadding=0 cellspacing=0 width=100% bgcolor="#efefef">
        <TR>
<%if (index.equals("ectList") || index.equals("ecbList") ||
      index.equals("ecbEdit") || index.equals("ecDetail")){%>
          <TD><IMG src="/images/menu.off.start.gif"></TD>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_ECARDS")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
<%} else {%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;&nbsp;<A class="mainMenuHeader" HREF="serv?type=ec&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_ECARDS")%></FONT></A>&nbsp;</TD> 
<%}%>
<%if (index.equals("userEdit")){%>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_USER_ACCOUNT")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
<%} else {%>
          <%if (!(index.equals("ectList") || index.equals("ecbList") || 
		  index.equals("ecbEdit") || index.equals("ecDetail"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;&nbsp;<A class="mainMenuHeader" HREF="serv?type=user&action=edit"><FONT class="mainMenuText"><%=session.getAttribute("MENU_USER_ACCOUNT")%></FONT></A>&nbsp;</TD>
<%}%>
          <%if (!(index.equals("userEdit"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="javascript:getHelp('<%=index%>')"><FONT class="mainMenuText"><%=session.getAttribute("MENU_HELP")%></FONT></A>&nbsp;</TD>
          <TD bgcolor="#336699" nowrap width="100%" align=right background="/images/menu.off.bg.gif">&nbsp;</TD>
        </TR>
      </TABLE>
     <!-- End of Menu Bar -->
    </TD>
    <TD nowrap align="right" background="/images/menu.off.bg.gif">
      <font class="userText"><%=session.getAttribute("BCC_USER")%>: </font>
      <a class="logoutMenuHeader" href="login?laction=logout" id="logout" target="_top">
      <font class="logoutText"><%=session.getAttribute("TEXT_LOGOUT")%></font></a>&nbsp;&nbsp;
    </TD>
  </TR>
    <!-- Small Gray Bar -->
  <TR width="100%" bgcolor="#efefef">
    <TD colspan="2"><IMG border="0" width="1" height="5" src="/images/spacer.gif"></TD>
  </TR>
</TABLE>

  <!-- Start of 1st Sub-Menu Bar -->

<TABLE width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#efefef">
  <TR width="100%" height="15">
<%if (index.equals("ectList") || index.equals("ecbList") ||
      index.equals("ecbEdit") || index.equals("ecDetail")) {%>
  <%if (index.equals("ecDetail")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_ECARDS")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ec&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_ECARDS")%></FONT></A></TD>
  <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("ecbList") || index.equals("ecbEdit")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_ECARD_BATCHES")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ecb&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_ECARD_BATCHES")%></FONT></A></TD>
  <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("ectList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_ECARD_TYPE")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ect&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_ECARD_TYPE")%></FONT></A></TD>
  <%}%>
  <!-- End of 1st Sub-Menu Bar for ECards -->

<%}%>
      <TD nowrap width="100%" align="right"><A class="subMenuHeader" href="serv?type=home&action=edit" id="currole"><FONT class="subMenuText"><%=session.getAttribute("BCC_ROLE_NAME")%></FONT></A>&nbsp;&nbsp;</TD>
  </TR>
</TABLE>

<!-- Bar under menu -->
<TABLE width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#efefef">
  <TR width="100%">
    <TD><IMG border="0" width="1" height="5" src="/images/spacer.gif"></TD>
  </TR>
  <TR width="100%" bgcolor="#999999">
    <TD><IMG border="0" width="1" height="1" src="/images/spacer.gif"></TD>
  </TR>
</TABLE>
<TABLE width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
  <TR width="100%">
    <TD><IMG border="0" width="1" height="5" src="/images/spacer.gif"></TD>
  </TR>
</TABLE>
<!-- End of bar -->

  <!-- Start of 2nd Sub-Menu Bar -->
<center>
<%if (index.equals("ecbList") || index.equals("ecbEdit")) {%>
  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">
  <%if (index.equals("ecbList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_ECARD_BATCHES_LIST")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ecb&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_ECARD_BATCHES_LIST")%></FONT></A></TD>
  <%}%>
  <%if (index.equals("ecbEdit")){%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_ECARD_BATCHES_DETAIL")%></FONT></TD>
  <%}%>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for ECard Batches -->

<%}%>
</center>
<p>

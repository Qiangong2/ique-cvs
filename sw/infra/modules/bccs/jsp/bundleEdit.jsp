<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.ms.common.QueryBean" %>
<jsp:useBean class="com.broadon.ms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();
  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  String[] params = qb.getParamArray();
  int[] counts = qb.getCountArray();

  String model = params[0];
  String sku = params[1];
  String start = params[2];
  String end = params[3];
  String buid = params[4];
  String buName = params[5];

  String titleStr = htmlResults[0];

  int iTotalCount = counts[0];
    
  String[] aHeader = {"COL_TITLE", "COL_TITLE_ID", "COL_RTYPE", "COL_LIMITS"};
%>

<jsp:include page="bcc-header.jsp" flush="true">
    <jsp:param name="page" value="bundleEdit"/>   
</jsp:include>

<%if (err!=null && err!="") {%><center><font class="errorText"><%=err%></font></center><p><%}%>

<form name="theForm" id="theForm" action="serv" method="POST" align="center" onSubmit="return false;">
    <input type="hidden" name="type" value=""></input>
    <input type="hidden" name="action" value=""></input>
    <input type="hidden" name="titleCount" value="<%=iTotalCount%>"></input>
    <input type="hidden" name="model" value="<%=model%>"></input>
    <input type="hidden" name="buid" value="<%=buid%>"></input>
    <input type="hidden" name="start" value="<%=start%>"></input>
    <input type="hidden" name="sku" value="<%=sku%>"></input>
 
<p>
<TABLE cellSpacing=0 cellPadding=1 width=30% align=center bgcolor="white" border=0>
  <TR>
    <TD nowrap="true" align="right"><b><%=session.getAttribute("COL_BU_NAME")%>:&nbsp;</b></TD>
    <TD><%=buName%></TD>
    <TD></TD>
  </TR>
  <TR> 
    <TD nowrap="true" align="right"><b><%=session.getAttribute("COL_BB_MODEL")%>:&nbsp;</b></TD>
    <TD><%=model%></TD>
    <TD></TD>
  </TR>
  <TR> 
    <TD nowrap="true" align="right"><b><%=session.getAttribute("COL_SKU")%>:&nbsp;</b></TD>
    <TD><%=sku%></TD>
    <TD></TD>
  </TR>
  <TR>
    <TD nowrap="true" align="right"><b><%=session.getAttribute("COL_START_DATE")%>:&nbsp;</b></TD>
    <TD><%=start%></TD>
    <TD></TD>
  </TR>
  <TR>
    <TD nowrap="true" align="right"><b><%=session.getAttribute("COL_END_DATE")%>:&nbsp;</b></TD>
    <TD><INPUT type="text" name="end1" size="19" value="<%=end%>" onfocus="this.blur();popUpCalendar(this, theForm.end1, 'yyyy.mm.dd', 'end')"></INPUT>
        <img src="/images/date.gif" onClick="popUpCalendar(this, theForm.end1, 'yyyy.mm.dd', 'end')">
    </TD>
    <TD><INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_SUBMIT")%>" OnClick="onClickSubmit(theForm, 'update');"></TD>
  </TR>
</TABLE>
<br>
<TABLE cellSpacing=0 cellPadding=1 width=60% align=center bgColor="#336699" border=0>
  <TR>
    <TD>
      <!-- List Title -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699>
          <TD width="100%">
            <TABLE width="100%" align="center" cellspacing="1" cellpadding="4" bgcolor="#336699">
              <TR>
                <TD width="50%" class="tblSubHdrLabel2"><%=session.getAttribute("TEXT_BUNDLES_TITLE_LIST")%></TD>
                <TD width="50%" bgColor=#336699 align="right"><INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_ADD_BUNDLES_TITLE")%>" OnClick="onClickSubmit(theForm, 'add');"></TD>
              </TR>
            </TABLE>
          </TD>
        </TR>
        <TR>
          <TD bgColor=#efefef>
	       <TABLE border="0" width="100%" align="center" cellspacing="1" cellpadding="4">
<%if (iTotalCount>0) {%>
<tr>
<td class="tableheader" nowrap="true"><%=session.getAttribute("COL_NO")%></td>
  <%for (int i=0; i<aHeader.length; i++) {%>
    <td class="tableheader" nowrap="true">
      <%=session.getAttribute(aHeader[i])%>
    </td>
  <%}%>
</tr>
<%=titleStr%>
<%} else {%>
 <TR width="100%">
    <TD>
 <!-- No Result Found -->
      <P>
      <CENTER>
      <%=session.getAttribute("TEXT_RESULT")%>:&nbsp;<%=session.getAttribute("TEXT_EMPTY")%>
      </CENTER>
    </TD>
  </TR>
<%}%>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>

</form name="theForm">

<jsp:include page="footer.jsp" flush="true"/>


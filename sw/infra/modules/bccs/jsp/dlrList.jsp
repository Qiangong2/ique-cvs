<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.ms.common.QueryBean" %>
<jsp:useBean class="com.broadon.ms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  Object role = session.getAttribute("BCC_ROLE");
  String ROLE_ADMIN = "10";

  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  int[] counts = qb.getCountArray();

  String dlrStr = "";
  int iTotalCount = 0;

  if (role.toString().equals(ROLE_ADMIN)) {
      dlrStr = htmlResults[1];
      iTotalCount = counts[1];
  } else {
      dlrStr = htmlResults[0];
      iTotalCount = counts[0];
  }

  String sort = qb.getSort();
  sort = sort.toLowerCase();

  String[] aHeader = {"COL_REQ_DATE", "COL_JOB_OWNER", "COL_JOB_NAME", "COL_VALI_DATE", "COL_PROC_DATE", "COL_STATUS"};
  String[] aSort = {"request_date", "email_address", "job_name", "validate_date", "process_date", "process_status"};
%>

<%if (role!=null && (role.toString().equals(ROLE_ADMIN))) {%>
  <jsp:include page="bcc-header.jsp" flush="true">
    <jsp:param name="page" value="dlrList"/>   
  </jsp:include>
<%} else if (role!=null) {%>
  <jsp:include page="bcc-upload-header.jsp" flush="true">
    <jsp:param name="page" value="dlrList"/>   
  </jsp:include>
<%}%>

<form name="theForm" id="theForm" action="serv" method="POST" align=center>
<input type="hidden" name="type" value="dlr"></input>
<input type="hidden" name="action" value="list"></input>
<input type="hidden" name="p" value="1"></input>
<input type="hidden" name="dlrsort" value=""></input>

<p>
<TABLE cellSpacing=0 cellPadding=1 width=100% align=center bgColor="#336699" border=0>
  <TR>
    <TD>
      <!-- List Title -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699>
          <TD width="100%">
            <TABLE width="100%" align="center" cellspacing="1" cellpadding="4" bgcolor="#336699">
              <TR><TD width=100% class="tblSubHdrLabel2">
                <%=session.getAttribute("TEXT_DLR_LIST")%>
	      </TD></TR>
            </TABLE>
          </TD>
        </TR>
        <TR>
          <TD bgColor=#efefef>
	       <TABLE border="0" width="100%" align="center" cellspacing="1" cellpadding="4">
<%if (iTotalCount > 0) {%>
<tr>
<td class="tableheader" nowrap="true"><%=session.getAttribute("COL_NO")%></td>
  <%for (int i=0; i<aHeader.length; i++) {%>
    <td class="tableheader" nowrap="true">
    <%if (sort.equals(aSort[i])) {%>
      <a href="serv?type=dlr&action=list&dlrsort=<%=aSort[i]%>_d"><%=session.getAttribute(aHeader[i])%></a> <img src="/images/up.gif">
    <%} else if (sort.equals(aSort[i]+"_d")) {%>
      <a href="serv?type=dlr&action=list&dlrsort=<%=aSort[i]%>"><%=session.getAttribute(aHeader[i])%></a> <img src="/images/down.gif">
    <%} else {%>
      <a href="serv?type=dlr&action=list&dlrsort=<%=aSort[i]%>"><%=session.getAttribute(aHeader[i])%></a>
    <%}%>
    </td>
  <%}%>
</tr>
<%=dlrStr%>
<%} else {%>
<TR width="100%">
    <TD>
 <!-- No Result Found -->
      <P>
      <CENTER>
      <%=session.getAttribute("TEXT_RESULT")%>:&nbsp;<%=session.getAttribute("TEXT_EMPTY")%>
      </CENTER>
    </TD>
  </TR>
<%}%>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>

<p>
<!--BEGIN NAVIGATION TABLE -->
<%
    int iPageSize = qb.getPageSize();
    int iPageNo = qb.getPageNo();
    int iPageCount = 0;
    if (iTotalCount>iPageSize) {
      iPageCount = iTotalCount / iPageSize;
      if (iPageCount * iPageSize < iTotalCount) iPageCount ++;
    }
%>

<%if (iTotalCount>iPageSize) {%>
    <TABLE BORDER=0 width=100% cellspacing=0 cellpadding=4> 
    <TR>
    <TD><%=session.getAttribute("LBL_TOTAL_RECORDS")%> <%= iTotalCount%>&nbsp;&nbsp;
    <%if (iPageNo>1) { %>
       <input class="sbutton" type="submit" value="<%=session.getAttribute("BUTTON_FIRST")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_FIRST")%>" onClick="postpage(this.form, 1, '<%=sort%>');"></input>
       <input class="sbutton" type="submit" value="<%=session.getAttribute("BUTTON_PREV")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_PREV")%>" onClick="postpage(this.form, <%=iPageNo-1%>, '<%=sort%>');"></input>
    <% }%>
    <%if (iPageNo<iPageCount) { %>
       <input class="sbutton" type="submit" value="<%=session.getAttribute("BUTTON_NEXT")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_NEXT")%>" onClick="postpage(this.form,<%=iPageNo+1%>, '<%=sort%>');"></input>
       <input class="sbutton" type="submit" value="<%=session.getAttribute("BUTTON_LAST")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_LAST")%>" onClick="postpage(this.form,<%=iPageCount%>, '<%=sort%>');"></input>
    <% }%>
    <%String pStr = null;
      for (int i=1; i<=iPageCount; i++) {
        if (i!=iPageNo)
          pStr=pStr+"<option value="+i+">Page&nbsp;"+i+"</option>";
        else
          pStr=pStr+"<option selected value="+i+">Page&nbsp;"+i+"</option>";
      }
      if (pStr!=null) { %>
        &nbsp;<select class="smallField" name="gopnum"><%=pStr%></select>
        <input class="sbutton" type="submit" value="<%=session.getAttribute("BUTTON_GO")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_GO")%>" 
        onClick="postpage(this.form,this.form.gopnum.options[this.form.gopnum.selectedIndex].value, '<%=sort%>');"></input>
    <%}%>
    </TD>
    </TR>
    </TABLE>
<%}%>
<!--END NAVIGATION TABLE -->

</form name="theForm">

<jsp:include page="footer.jsp" flush="true"/>


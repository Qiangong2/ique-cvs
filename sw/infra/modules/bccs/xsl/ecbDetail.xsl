<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
<tr>
  <td class="formLabel2" nowrap="true">Start Game Ticket ID:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true"><xsl:value-of select="START_ECARD_ID"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">End Game Ticket ID:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true"><xsl:value-of select="END_ECARD_ID"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">Cert ID:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true"><xsl:value-of select="CERT_ID"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">Game Ticket Type:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
     <xsl:value-of disable-output-escaping="yes" select="ECARD_TYPE"/>
     <xsl:choose>
         <xsl:when test="DESCRIPTION!=''">
             <xsl:text> (</xsl:text>
             <xsl:value-of disable-output-escaping="yes" select="DESCRIPTION"/>
             <xsl:text>)</xsl:text>
         </xsl:when>
     </xsl:choose>
  </td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">Business Unit:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
     <xsl:value-of select="BU_ID"/>
     <xsl:choose>
         <xsl:when test="BUSINESS_NAME!=''">
             <xsl:text> (</xsl:text>
             <xsl:value-of disable-output-escaping="yes" select="BUSINESS_NAME"/>
             <xsl:text>)</xsl:text>
         </xsl:when>
     </xsl:choose>
  </td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">Create Date:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true"><xsl:value-of select="CREATE_DATE"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">Print Date:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true"><xsl:value-of select="PRINT_DATE"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">Ship Date:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true"><xsl:value-of select="SHIP_DATE"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">Upload Date:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true"><xsl:value-of select="UPLOAD_DATE"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">Expire Date:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true"><xsl:value-of select="EXPIRE_DATE"/></td>
</tr>

</xsl:template>

</xsl:stylesheet>

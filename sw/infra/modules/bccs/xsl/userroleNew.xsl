<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
<xsl:variable name="counter"><xsl:value-of select="NO"/></xsl:variable>
<tr>
  <td class="formLabel5" nowrap="true">
    <input>
      <xsl:attribute name="type">checkbox</xsl:attribute>
      <xsl:attribute name="name"><xsl:value-of select="concat('nrole', $counter)"/></xsl:attribute>
      <xsl:attribute name="value"><xsl:value-of select="ROLE_LEVEL"/></xsl:attribute>
    </input>
  </td>
  <td class="formLabel5" nowrap="true">
      <xsl:value-of disable-output-escaping="yes" select="DESCRIPTION"/>
  </td>
  <td class="formLabel5" nowrap="true">
        <select>
          <xsl:attribute name="name"><xsl:value-of select="concat('nactive', $counter)"/></xsl:attribute>
          <option>
              <xsl:attribute name="selected">true</xsl:attribute>
              <xsl:attribute name="value">1</xsl:attribute>
              <xsl:text>Yes</xsl:text>
          </option>
          <option>
              <xsl:attribute name="value">0</xsl:attribute>
              <xsl:text>No</xsl:text>
          </option>
        </select>
  </td>
  <td class="formLabel5" nowrap="true">
        <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
  </td>
</tr>
</xsl:template>

</xsl:stylesheet>

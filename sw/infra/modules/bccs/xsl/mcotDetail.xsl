<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">

<tr><td class="formLabel2" nowrap="true">Description:</td><td class="formField"></td>
<td class="formField" nowrap="true">
<textarea>
<xsl:attribute name="cols">30</xsl:attribute>
<xsl:attribute name="rows">6</xsl:attribute>
<xsl:attribute name="name">desc</xsl:attribute>
<xsl:value-of disable-output-escaping="yes" select="DESCRIPTION"/>
</textarea>

</td></tr> 

<xsl:choose>
<xsl:when test="BB_MODEL!=''">                         
    <tr><td class="formLabel2" nowrap="true">Last Updated:</td><td class="formField"></td>
    <td class="formField" nowrap="true">
	<xsl:value-of select="LAST_UPDATED"/>
    </td></tr> 
</xsl:when>
</xsl:choose>

</xsl:template>

</xsl:stylesheet>

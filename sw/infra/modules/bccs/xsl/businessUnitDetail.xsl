<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">

<tr><td class="formLabel2" nowrap="true">Business Unit ID:</td><td class="formField"></td>
<td class="formField" nowrap="true">
<xsl:choose>
<xsl:when test="BU_ID!=''">                         
    <input>
        <xsl:attribute name="type">hidden</xsl:attribute>
	<xsl:attribute name="name">id</xsl:attribute>
	<xsl:attribute name="value">
  	    <xsl:value-of select="BU_ID"/>
        </xsl:attribute>
    </input>
	<xsl:value-of select="BU_ID"/>
</xsl:when>
<xsl:otherwise>
    <input>
        <xsl:attribute name="type">text</xsl:attribute>
	<xsl:attribute name="size">30</xsl:attribute>
	<xsl:attribute name="name">id</xsl:attribute>
	<xsl:attribute name="value"></xsl:attribute>
    </input>
    <font color="red">*</font>
</xsl:otherwise>
</xsl:choose>
</td></tr> 

<tr><td class="formLabel2" nowrap="true">Business Name:</td><td class="formField"></td>
<td class="formField" nowrap="true">
<input>
<xsl:attribute name="type">text</xsl:attribute>
<xsl:attribute name="size">30</xsl:attribute>
<xsl:attribute name="maxlength">128</xsl:attribute>
<xsl:attribute name="name">business_name</xsl:attribute>
<xsl:attribute name="value"><xsl:value-of disable-output-escaping="yes" select="BUSINESS_NAME"/></xsl:attribute>
</input>
<font color="red">*</font>
</td></tr> 

<tr><td class="formLabel2" nowrap="true">Contact Info:</td><td class="formField"></td>
<td class="formField" nowrap="true">
<textarea>
<xsl:attribute name="cols">30</xsl:attribute>
<xsl:attribute name="rows">6</xsl:attribute>
<xsl:attribute name="name">contact</xsl:attribute>
<xsl:value-of disable-output-escaping="yes" select="CONTACT_INFO"/>
</textarea>
</td></tr>

<xsl:choose>
<xsl:when test="BU_ID!=''">                         
    <tr><td class="formLabel2" nowrap="true">Create Date:</td><td class="formField"></td>
    <td class="formField" nowrap="true">
	<xsl:value-of select="CREATE_DATE"/>
    </td></tr> 
</xsl:when>
</xsl:choose>

</xsl:template>

</xsl:stylesheet>

<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW[position() mod 2 = 1]">
   <tr class="oddrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=bundle&amp;action=edit&amp;model=</xsl:text>
              <xsl:value-of select="BB_MODEL"/>
	      <xsl:text>&amp;buid=</xsl:text>
              <xsl:value-of select="BU_ID"/>
	      <xsl:text>&amp;sku=&quot;</xsl:text>
              <xsl:value-of select="SKU"/>
	      <xsl:text>&quot;&amp;start=&quot;</xsl:text>
              <xsl:value-of select="START_DATE"/>
	      <xsl:text>&quot;&amp;end1=&quot;</xsl:text>
              <xsl:value-of select="END_DATE"/>
	      <xsl:text>&quot;</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="BU_ID"/><xsl:text>-</xsl:text><xsl:value-of disable-output-escaping="yes" select="BB_MODEL"/><xsl:text>-</xsl:text><xsl:value-of select="SDATE"/>
          </A>    
        </td>
	<td class="smallText" nowrap="true"><xsl:value-of select="SKU"/></td>	
	<td class="smallText" nowrap="true"><xsl:value-of select="START_DATE"/></td>	
	<td class="smallText" nowrap="true"><xsl:value-of select="END_DATE"/></td>	
        <td class="smallText2"><xsl:value-of select="TCOUNT"/></td>
   </tr>
</xsl:template>

<xsl:template match="ROW">
   <tr class="evenrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=bundle&amp;action=edit&amp;model=</xsl:text>
              <xsl:value-of select="BB_MODEL"/>
	      <xsl:text>&amp;buid=</xsl:text>
              <xsl:value-of select="BU_ID"/>
	      <xsl:text>&amp;sku=&quot;</xsl:text>
              <xsl:value-of select="SKU"/>
	      <xsl:text>&quot;&amp;start=&quot;</xsl:text>
              <xsl:value-of select="START_DATE"/>
	      <xsl:text>&quot;&amp;end1=&quot;</xsl:text>
              <xsl:value-of select="END_DATE"/>
	      <xsl:text>&quot;</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="BU_ID"/><xsl:text>-</xsl:text><xsl:value-of disable-output-escaping="yes" select="BB_MODEL"/><xsl:text>-</xsl:text><xsl:value-of select="SDATE"/>
          </A>    
        </td>
	<td class="smallText" nowrap="true"><xsl:value-of select="SKU"/></td>	
	<td class="smallText" nowrap="true"><xsl:value-of select="START_DATE"/></td>	
	<td class="smallText" nowrap="true"><xsl:value-of select="END_DATE"/></td>	
        <td class="smallText2"><xsl:value-of select="TCOUNT"/></td>
   </tr>
</xsl:template>

</xsl:stylesheet>

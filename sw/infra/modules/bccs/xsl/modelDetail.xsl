<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">

<tr><td class="formLabel2" nowrap="true">Model:</td><td class="formField"></td>
<td class="formField" nowrap="true">
<xsl:choose>
<xsl:when test="BB_MODEL!=''">                         
    <input>
        <xsl:attribute name="type">hidden</xsl:attribute>
	<xsl:attribute name="name">model</xsl:attribute>
	<xsl:attribute name="value"><xsl:value-of disable-output-escaping="yes" select="BB_MODEL"/></xsl:attribute>
    </input>
    <xsl:value-of disable-output-escaping="yes" select="BB_MODEL"/>
</xsl:when>
<xsl:otherwise>
    <input>
        <xsl:attribute name="type">text</xsl:attribute>
	<xsl:attribute name="size">30</xsl:attribute>
	<xsl:attribute name="name">model</xsl:attribute>
	<xsl:attribute name="value"></xsl:attribute>
    </input>
    <font color="red">*</font>
</xsl:otherwise>
</xsl:choose>
</td></tr> 

<tr><td class="formLabel2" nowrap="true">Description:</td><td class="formField"></td>
<td class="formField" nowrap="true">
<textarea>
<xsl:attribute name="cols">30</xsl:attribute>
<xsl:attribute name="rows">6</xsl:attribute>
<xsl:attribute name="name">desc</xsl:attribute>
<xsl:value-of disable-output-escaping="yes" select="DESCRIPTION"/>
</textarea>
</td></tr> 

<xsl:choose>
<xsl:when test="BB_MODEL!=''">                         
    <tr><td class="formLabel2" nowrap="true">Create Date:</td><td class="formField"></td>
    <td class="formField" nowrap="true">
	<xsl:value-of select="CREATE_DATE"/>
    </td></tr> 
</xsl:when>
</xsl:choose>

</xsl:template>

</xsl:stylesheet>

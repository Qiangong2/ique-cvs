<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
<tr>
  <td class="formLabel2" nowrap="true">Game Ticket Type:</td>
  <td class="formField"></td>
  <td class="formField">
     <xsl:value-of disable-output-escaping="yes" select="ECARD_TYPE"/>
     <xsl:choose>
         <xsl:when test="DESCRIPTION!=''">
             <xsl:text> (</xsl:text>
             <xsl:value-of disable-output-escaping="yes" select="DESCRIPTION"/>
             <xsl:text>)</xsl:text>
         </xsl:when>
     </xsl:choose>
  </td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">Business Unit:</td>
  <td class="formField"></td>
  <td class="formField">
    <A class="listText">
     <xsl:attribute name="href">
     <xsl:text>serv?type=businessUnit&amp;action=edit&amp;id=</xsl:text>
     <xsl:value-of select="BU_ID"/>
     </xsl:attribute>
     <xsl:value-of select="BU_ID"/>
     <xsl:choose>
         <xsl:when test="BUSINESS_NAME!=''">
             <xsl:text> (</xsl:text>
             <xsl:value-of disable-output-escaping="yes" select="BUSINESS_NAME"/>
             <xsl:text>)</xsl:text>
         </xsl:when>
     </xsl:choose>
    </A>
  </td>
</tr>

</xsl:template>

</xsl:stylesheet>

<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW[position() mod 2 = 1]">
   <tr class="oddrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="REQUEST_DATE"/></td>
        <td class="smallText" nowrap="true"><xsl:value-of select="EMAIL_ADDRESS"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="JOB_NAME"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="VALIDATE_DATE"/></td>        
	<td class="smallText" nowrap="true"><xsl:value-of select="PROCESS_DATE"/></td>        
	<td class="smallText" nowrap="true">
        <xsl:choose>
            <xsl:when test="ROLE_LEVEL='70'">
               <xsl:choose>
                   <xsl:when test="PROCESS_STATUS='Uploaded'">
                       <xsl:value-of select="PROCESS_STATUS"/>
                   </xsl:when>
                   <xsl:otherwise>
                       <A class="listText">
                         <xsl:attribute name="href">
                         <xsl:text>/logs/</xsl:text>
                         <xsl:value-of select="substring-after(PROCESS_XML, 'bccs/')"/>
                         </xsl:attribute>
                         <xsl:attribute name="target">
                         <xsl:text>_blank</xsl:text>
                         </xsl:attribute>
                         <xsl:value-of select="PROCESS_STATUS"/>
                      </A>
                   </xsl:otherwise>
               </xsl:choose>
           </xsl:when>
           <xsl:otherwise>
               <xsl:choose>
                   <xsl:when test="PROCESS_STATUS!='Failed'">
                       <xsl:value-of select="PROCESS_STATUS"/>
                   </xsl:when>
                   <xsl:otherwise>
                       <A class="listText">
                        <xsl:attribute name="href">
                        <xsl:text>JavaScript: alert('</xsl:text>
                        <xsl:value-of select="PROCESS_XML"/>
                        <xsl:text>')</xsl:text>
                        </xsl:attribute>
                        <xsl:value-of select="PROCESS_STATUS"/>
                      </A>
                   </xsl:otherwise>
               </xsl:choose>
           </xsl:otherwise>
       </xsl:choose>
       </td>        
   </tr>
</xsl:template>

<xsl:template match="ROW">
   <tr class="evenrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="REQUEST_DATE"/></td>
        <td class="smallText" nowrap="true"><xsl:value-of select="EMAIL_ADDRESS"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="JOB_NAME"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="VALIDATE_DATE"/></td>        
	<td class="smallText" nowrap="true"><xsl:value-of select="PROCESS_DATE"/></td>        
	<td class="smallText" nowrap="true">
        <xsl:choose>
            <xsl:when test="ROLE_LEVEL='70'">
               <xsl:choose>
                   <xsl:when test="PROCESS_STATUS='Uploaded'">
                       <xsl:value-of select="PROCESS_STATUS"/>
                   </xsl:when>
                   <xsl:otherwise>
                       <A class="listText">
                         <xsl:attribute name="href">
                         <xsl:text>/logs/</xsl:text>
                         <xsl:value-of select="substring-after(PROCESS_XML, 'bccs/')"/>
                         </xsl:attribute>
                         <xsl:attribute name="target">
                         <xsl:text>_blank</xsl:text>
                         </xsl:attribute>
                         <xsl:value-of select="PROCESS_STATUS"/>
                      </A>
                   </xsl:otherwise>
               </xsl:choose>
           </xsl:when>
           <xsl:otherwise>
               <xsl:choose>
                   <xsl:when test="PROCESS_STATUS!='Failed'">
                       <xsl:value-of select="PROCESS_STATUS"/>
                   </xsl:when>
                   <xsl:otherwise>
                       <A class="listText">
                        <xsl:attribute name="href">
                        <xsl:text>JavaScript: alert('</xsl:text>
                        <xsl:value-of select="PROCESS_XML"/>
                        <xsl:text>')</xsl:text>
                        </xsl:attribute>
                        <xsl:value-of select="PROCESS_STATUS"/>
                      </A>
                   </xsl:otherwise>
               </xsl:choose>
           </xsl:otherwise>
       </xsl:choose>
       </td>        
   </tr>
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
	extension-element-prefixes="xalan">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW[position() mod 2 = 1]">
   <tr class="oddrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=cert&amp;action=edit&amp;id=</xsl:text>
              <xsl:value-of select="CERT_ID"/>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="CERT_ID"/>
          </A>    
        </td>
	<td class="smallText" nowrap="true"><xsl:value-of select="CN"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="OU"/></td>
	<td class="smallText2"><xsl:value-of select="SERIAL_NO"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="REVOKE_DATE"/></td>
   </tr>
</xsl:template>

<xsl:template match="ROW">
   <tr class="evenrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=cert&amp;action=edit&amp;id=</xsl:text>
              <xsl:value-of select="CERT_ID"/>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="CERT_ID"/>
          </A>    
        </td>
	<td class="smallText" nowrap="true"><xsl:value-of select="CN"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="OU"/></td>
	<td class="smallText2"><xsl:value-of select="SERIAL_NO"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="REVOKE_DATE"/></td>
  </tr>
</xsl:template>

</xsl:stylesheet>

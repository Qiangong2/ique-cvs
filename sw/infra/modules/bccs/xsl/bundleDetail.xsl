<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW[position() mod 2 = 1]">
<xsl:variable name="counter"><xsl:value-of select="NO"/></xsl:variable>
   <tr class="oddrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=bundletitle&amp;action=edit&amp;model=</xsl:text>
              <xsl:value-of select="BB_MODEL"/>
              <xsl:text>&amp;buid=</xsl:text>
              <xsl:value-of select="BU_ID"/>
              <xsl:text>&amp;title=</xsl:text>
              <xsl:value-of select="TITLE_ID"/>
              <xsl:text>&amp;sku=&quot;</xsl:text>
              <xsl:value-of select="SKU"/>
              <xsl:text>&quot;&amp;start=&quot;</xsl:text>
              <xsl:value-of select="START_DATE"/>
              <xsl:text>&quot;</xsl:text>
              <xsl:text>&amp;end1=&quot;</xsl:text>
              <xsl:value-of select="END_DATE"/>
              <xsl:text>&quot;</xsl:text>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="TITLE"/>
          </A>    
        </td>
	<td class="smallText2"><xsl:value-of select="TITLE_ID"/>
          <input>
             <xsl:attribute name="type">hidden</xsl:attribute>
             <xsl:attribute name="name"><xsl:value-of select="concat('title', $counter)"/></xsl:attribute>
             <xsl:attribute name="value"><xsl:value-of select="TITLE_ID"/></xsl:attribute>
          </input>
        </td>	
	<td class="smallText" nowrap="true">
           <xsl:choose>
               <xsl:when test="RTYPE='PR'">
                 <xsl:text>Permanent Right</xsl:text>
               </xsl:when>
               <xsl:when test="RTYPE='LR'">
                 <xsl:text>Limited Play Right</xsl:text>
               </xsl:when>
               <xsl:when test="RTYPE='TR'">
                 <xsl:text>Limited Time Right</xsl:text>
               </xsl:when>
               <xsl:when test="RTYPE='XR'">
                 <xsl:text>Extended Limited Right</xsl:text>
               </xsl:when>
               <xsl:otherwise>
                 <xsl:text>Permanent Right</xsl:text>
               </xsl:otherwise>
          </xsl:choose>
        </td>
	<td class="smallText2"><xsl:value-of select="LIMITS"/></td>	
   </tr>
</xsl:template>

<xsl:template match="ROW">
<xsl:variable name="counter"><xsl:value-of select="NO"/></xsl:variable>
   <tr class="evenrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=bundletitle&amp;action=edit&amp;model=</xsl:text>
              <xsl:value-of select="BB_MODEL"/>
              <xsl:text>&amp;buid=</xsl:text>
              <xsl:value-of select="BU_ID"/>
              <xsl:text>&amp;title=</xsl:text>
              <xsl:value-of select="TITLE_ID"/>
              <xsl:text>&amp;sku=&quot;</xsl:text>
              <xsl:value-of select="SKU"/>
              <xsl:text>&quot;&amp;start=&quot;</xsl:text>
              <xsl:value-of select="START_DATE"/>
              <xsl:text>&quot;</xsl:text>
              <xsl:text>&amp;end1=&quot;</xsl:text>
              <xsl:value-of select="END_DATE"/>
              <xsl:text>&quot;</xsl:text>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="TITLE"/>
          </A>    
        </td>
	<td class="smallText2"><xsl:value-of select="TITLE_ID"/>
          <input>
             <xsl:attribute name="type">hidden</xsl:attribute>
             <xsl:attribute name="name"><xsl:value-of select="concat('title', $counter)"/></xsl:attribute>
             <xsl:attribute name="value"><xsl:value-of select="TITLE_ID"/></xsl:attribute>
          </input>
        </td>	
	<td class="smallText" nowrap="true">
           <xsl:choose>
               <xsl:when test="RTYPE='PR'">
                 <xsl:text>Permanent Right</xsl:text>
               </xsl:when>
               <xsl:when test="RTYPE='LR'">
                 <xsl:text>Limited Play Right</xsl:text>
               </xsl:when>
               <xsl:when test="RTYPE='TR'">
                 <xsl:text>Limited Time Right</xsl:text>
               </xsl:when>
               <xsl:when test="RTYPE='XR'">
                 <xsl:text>Extended Limited Right</xsl:text>
               </xsl:when>
               <xsl:otherwise>
                 <xsl:text>Permanent Right</xsl:text>
               </xsl:otherwise>
          </xsl:choose>
        </td>
	<td class="smallText2"><xsl:value-of select="LIMITS"/></td>	
   </tr>
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">

<tr>
  <td class="formLabel2" nowrap="true">Certificate ID:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true"><xsl:value-of select="CERT_ID"/></td>
</tr> 
<tr>
  <td class="formLabel2" nowrap="true">Common Name:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true"><xsl:value-of select="CN"/></td>
</tr> 
<tr>
  <td class="formLabel2" nowrap="true">Organizational Unit:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true"><xsl:value-of select="OU"/></td>
</tr> 
<tr>
  <td class="formLabel2" nowrap="true">Serial No:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true"><xsl:value-of select="SERIAL_NO"/></td>
</tr> 
<tr>
  <td class="formLabel2" nowrap="true">Revoke Date:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true"><xsl:value-of select="REVOKE_DATE"/></td>
</tr> 
<tr>
  <td class="formLabel2">
    <img>
        <xsl:attribute name="src">/images/spacer.gif</xsl:attribute>
        <xsl:attribute name="height">1</xsl:attribute>
        <xsl:attribute name="border">0</xsl:attribute>
      </img>
  </td>
  <td colspan="2" bgcolor="white">
    <img>
        <xsl:attribute name="src">/images/spacer.gif</xsl:attribute>
        <xsl:attribute name="width">1</xsl:attribute>
        <xsl:attribute name="height">1</xsl:attribute>
        <xsl:attribute name="border">0</xsl:attribute>
      </img>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">Private Key:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="PRIVATE_KEY"/></td>
</tr> 
<tr>
  <td class="formLabel2">
    <img>
        <xsl:attribute name="src">/images/spacer.gif</xsl:attribute>
        <xsl:attribute name="height">1</xsl:attribute>
        <xsl:attribute name="border">0</xsl:attribute>
      </img>
  </td>
  <td colspan="2" bgcolor="white">
    <img>
        <xsl:attribute name="src">/images/spacer.gif</xsl:attribute>
        <xsl:attribute name="width">1</xsl:attribute>
        <xsl:attribute name="height">1</xsl:attribute>
        <xsl:attribute name="border">0</xsl:attribute>
      </img>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">Public Key:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="PUBLIC_KEY"/></td>
</tr> 
<tr>
  <td class="formLabel2">
    <img>
        <xsl:attribute name="src">/images/spacer.gif</xsl:attribute>
        <xsl:attribute name="height">1</xsl:attribute>
        <xsl:attribute name="border">0</xsl:attribute>
      </img>
  </td>
  <td colspan="2" bgcolor="white">
    <img>
        <xsl:attribute name="src">/images/spacer.gif</xsl:attribute>
        <xsl:attribute name="width">1</xsl:attribute>
        <xsl:attribute name="height">1</xsl:attribute>
        <xsl:attribute name="border">0</xsl:attribute>
      </img>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">Certificate:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="CERTIFICATE"/></td>
</tr> 

</xsl:template>

</xsl:stylesheet>

<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
<xsl:variable name="counter"><xsl:value-of select="NO"/></xsl:variable>
<tr>
  <td class="formLabel5" nowrap="true">
    <input>
      <xsl:attribute name="type">checkbox</xsl:attribute>
      <xsl:attribute name="name"><xsl:value-of select="concat('dummy', $counter)"/></xsl:attribute>
      <xsl:attribute name="value"><xsl:value-of select="ROLE_LEVEL"/></xsl:attribute>
      <xsl:attribute name="checked">true</xsl:attribute>
      <xsl:attribute name="disabled">true</xsl:attribute>
    </input>
  </td>
  <td class="formLabel5" nowrap="true">
      <xsl:value-of disable-output-escaping="yes" select="DESCRIPTION"/>
  </td>
  <td class="formLabel5" nowrap="true">
  <input>
     <xsl:attribute name="type">hidden</xsl:attribute>
     <xsl:attribute name="name"><xsl:value-of select="concat('crole', $counter)"/></xsl:attribute>
     <xsl:attribute name="value"><xsl:value-of select="ROLE_LEVEL"/></xsl:attribute>
  </input>
  <xsl:choose>
    <xsl:when test="ROLE_LEVEL != DEFAULT_ROLE">
        <select>
          <xsl:attribute name="name"><xsl:value-of select="concat('cactive', $counter)"/></xsl:attribute>
          <xsl:choose>
              <xsl:when test="IS_ACTIVE != 1">
               <option>
                 <xsl:attribute name="value">1</xsl:attribute>
                 <xsl:text>Yes</xsl:text>
               </option>
               <option>
                 <xsl:attribute name="selected">true</xsl:attribute>
                 <xsl:attribute name="value">0</xsl:attribute>
                 <xsl:text>No</xsl:text>
               </option>
              </xsl:when>
              <xsl:otherwise>
               <option>
                 <xsl:attribute name="selected">true</xsl:attribute>
                 <xsl:attribute name="value">1</xsl:attribute>
                 <xsl:text>Yes</xsl:text>
               </option>
               <option>
                 <xsl:attribute name="value">0</xsl:attribute>
                 <xsl:text>No</xsl:text>
               </option>
              </xsl:otherwise>
            </xsl:choose>
        </select>
    </xsl:when>
    <xsl:otherwise>
       <input>
           <xsl:attribute name="type">hidden</xsl:attribute>
           <xsl:attribute name="name"><xsl:value-of select="concat('cactive', $counter)"/></xsl:attribute>
           <xsl:choose>
               <xsl:when test="IS_ACTIVE != 1">
                   <xsl:attribute name="value">0</xsl:attribute>
                  <xsl:text>No</xsl:text>
               </xsl:when>
               <xsl:otherwise>
                 <xsl:attribute name="value">1</xsl:attribute>
                 <xsl:text>Yes</xsl:text>
              </xsl:otherwise>
           </xsl:choose>
        </input>
    </xsl:otherwise>
  </xsl:choose>
  </td>
  <td class="formLabel5" nowrap="true">
    <xsl:choose>
        <xsl:when test="ROLE_LEVEL != DEFAULT_ROLE">
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
        </xsl:when>
        <xsl:otherwise>
            <xsl:text>Default</xsl:text>
        </xsl:otherwise>
    </xsl:choose>
  </td>
</tr>
</xsl:template>

</xsl:stylesheet>

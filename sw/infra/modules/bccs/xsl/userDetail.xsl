<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:param name="id">-1</xsl:param>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">

<xsl:choose>
    <xsl:when test="OPERATOR_ID!=''">    
        <input>
           <xsl:attribute name="type">hidden</xsl:attribute>
           <xsl:attribute name="name">opid</xsl:attribute>
           <xsl:attribute name="value"><xsl:value-of select="OPERATOR_ID"/></xsl:attribute>
        </input>
    </xsl:when>
</xsl:choose>

<tr><td class="formLabel2" nowrap="true">Full Name:</td><td class="formField"></td>
<td class="formField" nowrap="true">
<input>
<xsl:attribute name="type">text</xsl:attribute>
<xsl:attribute name="size">30</xsl:attribute>
<xsl:attribute name="name">fullname</xsl:attribute>
<xsl:attribute name="value"><xsl:value-of disable-output-escaping="yes" select="FULLNAME"/></xsl:attribute>
</input>
<font color="red">*</font>
</td></tr> 

<tr><td class="formLabel2" nowrap="true">Login:</td><td class="formField"></td>
<td class="formField" nowrap="true">
<input>
<xsl:attribute name="type">text</xsl:attribute>
<xsl:attribute name="size">30</xsl:attribute>
<xsl:attribute name="name">email</xsl:attribute>
<xsl:attribute name="value"><xsl:value-of disable-output-escaping="yes" select="EMAIL_ADDRESS"/></xsl:attribute>
</input>
<font color="red">*</font>
</td></tr> 

<xsl:choose>
    <xsl:when test="OPERATOR_ID!=''">    
        <tr><td class="formLabel2" nowrap="true">Password:</td><td class="formField"></td>
        <td class="formField" nowrap="true">
        <input>
           <xsl:attribute name="type">password</xsl:attribute>
           <xsl:attribute name="size">30</xsl:attribute>
           <xsl:attribute name="name">passwd</xsl:attribute>
           <xsl:attribute name="value"><xsl:value-of disable-output-escaping="yes" select="PASSWD"/></xsl:attribute>
        </input>
        </td></tr>
    </xsl:when>
</xsl:choose>

<xsl:choose>
    <xsl:when test="OPERATOR_ID!=''">                         
        <tr><td class="formLabel2" nowrap="true">Confirm Password:</td><td class="formField"></td>
        <td class="formField" nowrap="true">
        <input>
           <xsl:attribute name="type">password</xsl:attribute>
           <xsl:attribute name="size">30</xsl:attribute>
           <xsl:attribute name="name">confpasswd</xsl:attribute>
           <xsl:attribute name="value"><xsl:value-of disable-output-escaping="yes" select="PASSWD"/></xsl:attribute>
        </input>
        </td></tr> 
    </xsl:when>
</xsl:choose>

<tr><td class="formLabel2" nowrap="true">Status:</td><td class="formField"></td>
<td class="formField" nowrap="true">
<xsl:choose>
    <xsl:when test="$id!=10">
       <xsl:choose>
           <xsl:when test="STATUS!='A'">
                 <xsl:text>Inactive</xsl:text>
           </xsl:when>
           <xsl:otherwise>
                 <xsl:text>Active</xsl:text>
           </xsl:otherwise>
       </xsl:choose>
        <input>
           <xsl:attribute name="type">hidden</xsl:attribute>
           <xsl:attribute name="name">status</xsl:attribute>
           <xsl:attribute name="value"><xsl:value-of select="STATUS"/></xsl:attribute>
        </input>
   </xsl:when>
   <xsl:otherwise>
        <select>
          <xsl:attribute name="name">status</xsl:attribute>                     
          <xsl:choose>
              <xsl:when test="STATUS!='A'">
	       <option>
                 <xsl:attribute name="value">A</xsl:attribute>
                 <xsl:text>Active</xsl:text>
               </option>                      
               <option>
                 <xsl:attribute name="selected">true</xsl:attribute>
                 <xsl:attribute name="value">I</xsl:attribute>
                 <xsl:text>Inactive</xsl:text>
               </option>
               </xsl:when>
               <xsl:otherwise>
               <option>
	         <xsl:attribute name="selected">true</xsl:attribute>
                 <xsl:attribute name="value">A</xsl:attribute>
                 <xsl:text>Active</xsl:text>
               </option>                      
               <option>        
                 <xsl:attribute name="value">I</xsl:attribute>
                 <xsl:text>Inactive</xsl:text>
               </option>
               </xsl:otherwise>
            </xsl:choose>
        </select>
        <font color="red">*</font>
   </xsl:otherwise>
</xsl:choose>
</td></tr>

</xsl:template>

</xsl:stylesheet>

<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
	extension-element-prefixes="xalan">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW[position() mod 2 = 1]">
   <tr class="oddrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=title&amp;action=edit&amp;titleid=</xsl:text>
              <xsl:value-of select="TITLE_ID"/>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="TITLE"/>
          </A>    
        </td>
	<td class="smallText2"><xsl:value-of select="TITLE_ID"/></td>
	<td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=content&amp;action=edit&amp;contentid=</xsl:text>
              <xsl:value-of select="CONTENT_ID"/>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="CONTENT_OBJECT_NAME"/>
          </A>    
        </td>
	<td class="smallText2"><xsl:value-of select="CONTENT_ID"/></td>
	<td class="smallText2"><xsl:value-of select="CONTENT_OBJECT_VERSION"/></td>
	<td class="smallText"><xsl:value-of disable-output-escaping="yes" select="BUSINESS_NAME"/></td>
	<td class="smallText2"><xsl:value-of select="TID"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="CREATE_DATE"/></td>
        <xsl:choose>
            <xsl:when test="IS_CONTENT_OBJECT_LATEST='Y'">
  	        <td class="successText"><xsl:text>Latest</xsl:text></td>
            </xsl:when>
            <xsl:otherwise>
                <td>
                   <input>
                      <xsl:attribute name="class">sbutton</xsl:attribute>
                      <xsl:attribute name="type">button</xsl:attribute>
                      <xsl:attribute name="name">latest</xsl:attribute>
                      <xsl:attribute name="value">Get Latest</xsl:attribute>
                      <xsl:attribute name="OnClick">
                          <xsl:text>onClickGetLatest(theForm, '</xsl:text>
                          <xsl:value-of select="CONTENT_ID"/>
                          <xsl:text>', '</xsl:text>
                          <xsl:value-of select="BU_ID"/>
                          <xsl:text>', '</xsl:text>
                          <xsl:value-of select="TID"/>
                          <xsl:text>');</xsl:text>
                      </xsl:attribute>
                   </input>
               </td>
            </xsl:otherwise>
        </xsl:choose>  
        <td>
           <input>
              <xsl:attribute name="class">sbutton</xsl:attribute>
              <xsl:attribute name="type">button</xsl:attribute>
              <xsl:attribute name="name">revoke</xsl:attribute>
              <xsl:attribute name="value">Revoke</xsl:attribute>
              <xsl:attribute name="OnClick">
                  <xsl:text>onClickRevoke(theForm, '</xsl:text>
                  <xsl:value-of select="CONTENT_ID"/>
                  <xsl:text>', '</xsl:text>
                  <xsl:value-of select="BU_ID"/>
                  <xsl:text>');</xsl:text>
              </xsl:attribute>
           </input>
        </td>
   </tr>
</xsl:template>

<xsl:template match="ROW">
   <tr class="evenrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=title&amp;action=edit&amp;titleid=</xsl:text>
              <xsl:value-of select="TITLE_ID"/>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="TITLE"/>
          </A>    
        </td>
	<td class="smallText2"><xsl:value-of select="TITLE_ID"/></td>
	<td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=content&amp;action=edit&amp;contentid=</xsl:text>
              <xsl:value-of select="CONTENT_ID"/>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="CONTENT_OBJECT_NAME"/>
          </A>    
        </td>
	<td class="smallText2"><xsl:value-of select="CONTENT_ID"/></td>
	<td class="smallText2"><xsl:value-of select="CONTENT_OBJECT_VERSION"/></td>
	<td class="smallText"><xsl:value-of disable-output-escaping="yes" select="BUSINESS_NAME"/></td>
	<td class="smallText2"><xsl:value-of select="TID"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="CREATE_DATE"/></td>
        <xsl:choose>
            <xsl:when test="IS_CONTENT_OBJECT_LATEST='Y'">
  	        <td class="successText"><xsl:text>Latest</xsl:text></td>
            </xsl:when>
            <xsl:otherwise>
                <td>
                   <input>
                      <xsl:attribute name="class">sbutton</xsl:attribute>
                      <xsl:attribute name="type">button</xsl:attribute>
                      <xsl:attribute name="name">latest</xsl:attribute>
                      <xsl:attribute name="value">Get Latest</xsl:attribute>
                      <xsl:attribute name="OnClick">
                          <xsl:text>onClickGetLatest(theForm, '</xsl:text>
                          <xsl:value-of select="CONTENT_ID"/>
                          <xsl:text>', '</xsl:text>
                          <xsl:value-of select="BU_ID"/>
                          <xsl:text>', '</xsl:text>
                          <xsl:value-of select="TID"/>
                          <xsl:text>');</xsl:text>
                      </xsl:attribute>
                   </input>
               </td>
            </xsl:otherwise>
        </xsl:choose>  
        <td>
           <input>
              <xsl:attribute name="class">sbutton</xsl:attribute>
              <xsl:attribute name="type">button</xsl:attribute>
              <xsl:attribute name="name">revoke</xsl:attribute>
              <xsl:attribute name="value">Revoke</xsl:attribute>
              <xsl:attribute name="OnClick">
                  <xsl:text>onClickRevoke(theForm, '</xsl:text>
                  <xsl:value-of select="CONTENT_ID"/>
                  <xsl:text>', '</xsl:text>
                  <xsl:value-of select="BU_ID"/>
                  <xsl:text>');</xsl:text>
              </xsl:attribute>
           </input>
        </td>
  </tr>
</xsl:template>

</xsl:stylesheet>

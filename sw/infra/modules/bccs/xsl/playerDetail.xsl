<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
<tr>
  <td class="formLabel2" nowrap="true">HW Revision:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="BB_HWREV"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">Business Unit:</td>
  <td class="formField"></td>
  <td class="formField">
    <A class="listText">
      <xsl:attribute name="href">
          <xsl:text>serv?type=businessUnit&amp;action=edit&amp;id=</xsl:text>
          <xsl:value-of select="BU_ID"/>
      </xsl:attribute>
      <xsl:value-of select="BU_ID"/>
      <xsl:text> (</xsl:text>
      <xsl:value-of disable-output-escaping="yes" select="BUSINESS_NAME"/>
      <xsl:text>)</xsl:text>
    </A>
  </td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">Bundle Start Date:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="BUNDLE_START_DATE"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">Manufacture Date:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="MANUFACTURE_DATE"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">Serial Number:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="SN"/></td>
</tr>

</xsl:template>

</xsl:stylesheet>

<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">

<tr>
  <td class="formLabel2" nowrap="true">Parent Certificate ID:</td>
  <td class="formField"></td>
  <td class="smallText" nowrap="true">
    <A class="listText">
        <xsl:attribute name="href">
           <xsl:text>serv?type=cert&amp;action=edit&amp;id=</xsl:text>
           <xsl:value-of select="CA_CERT_ID"/>
        </xsl:attribute>
        <xsl:value-of disable-output-escaping="yes" select="CA_CERT_ID"/>
   </A>
  </td>
</tr> 

</xsl:template>

</xsl:stylesheet>

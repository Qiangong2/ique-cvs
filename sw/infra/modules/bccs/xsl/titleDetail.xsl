<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:param name="id">-1</xsl:param>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">

<xsl:choose>
    <xsl:when test="$id!=10">

<tr>
    <td class="formLabel2" nowrap="true">Title ID:</td>
    <td class="formField"></td>
    <td class="formField" nowrap="true">
    <input>
        <xsl:attribute name="type">hidden</xsl:attribute>
        <xsl:attribute name="name">title_id</xsl:attribute>
        <xsl:attribute name="value"><xsl:value-of select="TITLE_ID"/></xsl:attribute>
    </input>
    <xsl:value-of select="TITLE_ID"/>
    </td>
</tr> 

<tr>
    <td class="formLabel2" nowrap="true">Title:</td>
    <td class="formField"></td>
    <td class="formField" nowrap="true">
    <input>
        <xsl:attribute name="type">text</xsl:attribute>
        <xsl:attribute name="size">30</xsl:attribute>
        <xsl:attribute name="name">title_name</xsl:attribute>
        <xsl:attribute name="value"><xsl:value-of disable-output-escaping="yes" select="TITLE"/></xsl:attribute>
    </input>
    <font color="red">*</font>
    </td>
</tr> 

<tr>
    <td class="formLabel2" nowrap="true">Title Type:</td>
    <td class="formField"></td>
    <td class="formField" nowrap="true">
    <select>
        <xsl:attribute name="name">title_type</xsl:attribute>
        <xsl:choose>
            <xsl:when test="TITLE_TYPE='manual'">
                <option>
                    <xsl:attribute name="selected">true</xsl:attribute>
                    <xsl:attribute name="value">manual</xsl:attribute>
                    <xsl:text>manual</xsl:text>
                </option>
                <option>
                    <xsl:attribute name="value">push</xsl:attribute>
                    <xsl:text>push</xsl:text>
                </option>
                <option>
                    <xsl:attribute name="value">screensaver</xsl:attribute>
                    <xsl:text>screensaver</xsl:text>
                </option>
                <option>
                    <xsl:attribute name="value">visible</xsl:attribute>
                    <xsl:text>visible</xsl:text>
                </option>
            </xsl:when>
            <xsl:when test="TITLE_TYPE='push'">
                <option>
                    <xsl:attribute name="value">manual</xsl:attribute>
                    <xsl:text>manual</xsl:text>
                </option>
                <option>
                    <xsl:attribute name="selected">true</xsl:attribute>
                    <xsl:attribute name="value">push</xsl:attribute>
                    <xsl:text>push</xsl:text>
                </option>
                <option>
                    <xsl:attribute name="value">screensaver</xsl:attribute>
                    <xsl:text>screensaver</xsl:text>
                </option>
                <option>
                    <xsl:attribute name="value">visible</xsl:attribute>
                    <xsl:text>visible</xsl:text>
                </option>
            </xsl:when>
            <xsl:when test="TITLE_TYPE='screensaver'">
                <option>
                    <xsl:attribute name="value">manual</xsl:attribute>
                    <xsl:text>manual</xsl:text>
                </option>
                <option>
                    <xsl:attribute name="value">push</xsl:attribute>
                    <xsl:text>push</xsl:text>
                </option>
                <option>
                    <xsl:attribute name="selected">true</xsl:attribute>
                    <xsl:attribute name="value">screensaver</xsl:attribute>
                    <xsl:text>screensaver</xsl:text>
                </option>
                <option>
                    <xsl:attribute name="value">visible</xsl:attribute>
                    <xsl:text>visible</xsl:text>
                </option>
            </xsl:when>
            <xsl:when test="TITLE_TYPE='visible'">
                <option>
                    <xsl:attribute name="value">manual</xsl:attribute>
                    <xsl:text>manual</xsl:text>
                </option>
                <option>
                    <xsl:attribute name="value">push</xsl:attribute>
                    <xsl:text>push</xsl:text>
                </option>
                <option>
                    <xsl:attribute name="value">screensaver</xsl:attribute>
                    <xsl:text>screensaver</xsl:text>
                </option>
                <option>
                    <xsl:attribute name="selected">true</xsl:attribute>
                    <xsl:attribute name="value">visible</xsl:attribute>
                    <xsl:text>visible</xsl:text>
                </option>
             </xsl:when>
        </xsl:choose>
     </select>
    <font color="red">*</font>
    </td>
</tr> 

<tr>
    <td class="formLabel2" nowrap="true">Category:</td>
    <td class="formField"></td>
    <td class="formField" nowrap="true">
    <input>
        <xsl:attribute name="type">text</xsl:attribute>
        <xsl:attribute name="size">30</xsl:attribute>
        <xsl:attribute name="name">category</xsl:attribute>
        <xsl:attribute name="value"><xsl:value-of disable-output-escaping="yes" select="CATEGORY"/></xsl:attribute>
    </input>
    <font color="red">*</font>
    </td>
</tr> 

<tr>
    <td class="formLabel2" nowrap="true">Publisher:</td>
    <td class="formField"></td>
    <td class="formField" nowrap="true">
    <input>
        <xsl:attribute name="type">text</xsl:attribute>
        <xsl:attribute name="size">30</xsl:attribute>
        <xsl:attribute name="name">publisher</xsl:attribute>
        <xsl:attribute name="value"><xsl:value-of disable-output-escaping="yes" select="PUBLISHER"/></xsl:attribute>
    </input>
    <font color="red">*</font>
    </td>
</tr> 

<tr>
    <td class="formLabel2" nowrap="true">Developer:</td>
    <td class="formField"></td>
    <td class="formField" nowrap="true">
    <input>
        <xsl:attribute name="type">text</xsl:attribute>
        <xsl:attribute name="size">30</xsl:attribute>
        <xsl:attribute name="name">developer</xsl:attribute>
        <xsl:attribute name="value"><xsl:value-of disable-output-escaping="yes" select="DEVELOPER"/></xsl:attribute>
    </input>
    <font color="red">*</font>
    </td>
</tr> 

<tr>
    <td class="formLabel2" nowrap="true">Description:</td>
    <td class="formField"></td>
    <td class="formField" nowrap="true">
    <textarea>
        <xsl:attribute name="cols">50</xsl:attribute>
        <xsl:attribute name="rows">9</xsl:attribute>
        <xsl:attribute name="name">desc</xsl:attribute>
        <xsl:value-of disable-output-escaping="yes" select="DESCRIPTION"/>
    </textarea>
    </td>
</tr>

</xsl:when>
<xsl:otherwise>

<tr>
    <td class="formLabel2" nowrap="true">Title ID:</td>
    <td class="formField"></td>
    <td class="formField" nowrap="true"><xsl:value-of select="TITLE_ID"/></td>
</tr> 

<tr>
    <td class="formLabel2" nowrap="true">Title:</td>
    <td class="formField"></td>
    <td class="formField" nowrap="true"><xsl:value-of disable-output-escaping="yes" select="TITLE"/></td>
</tr> 

<tr>
    <td class="formLabel2" nowrap="true">Title Type:</td>
    <td class="formField"></td>
    <td class="formField" nowrap="true"><xsl:value-of disable-output-escaping="yes" select="TITLE_TYPE"/></td>
</tr> 

<tr>
    <td class="formLabel2" nowrap="true">Category:</td>
    <td class="formField"></td>
    <td class="formField" nowrap="true"><xsl:value-of disable-output-escaping="yes" select="CATEGORY"/></td>
</tr> 

<tr>
    <td class="formLabel2" nowrap="true">Publisher:</td>
    <td class="formField"></td>
    <td class="formField" nowrap="true"><xsl:value-of disable-output-escaping="yes" select="PUBLISHER"/></td>
</tr> 

<tr>
    <td class="formLabel2" nowrap="true">Developer:</td>
    <td class="formField"></td>
    <td class="formField" nowrap="true"><xsl:value-of disable-output-escaping="yes" select="DEVELOPER"/></td>
</tr> 

<tr>
    <td class="formLabel2" nowrap="true">Description:</td>
    <td class="formField"></td>
    <td class="formField">
      <xsl:call-template name="replace-newline">
        <xsl:with-param name="stringIn"><xsl:value-of disable-output-escaping="yes" select="DESCRIPTION"/></xsl:with-param>
      </xsl:call-template>
    </td>
</tr>

</xsl:otherwise>
</xsl:choose>

<xsl:choose>
<xsl:when test="TITLE_ID!=''">                         
    <tr><td class="formLabel2" nowrap="true">Last Updated:</td><td class="formField"></td>
    <td class="formField" nowrap="true">
	<xsl:value-of select="LAST_UPDATED"/>
    </td></tr> 
</xsl:when>
</xsl:choose>

</xsl:template>

<xsl:template name="replace-newline">
   <xsl:param name="stringIn"/>
   <xsl:choose>
   <xsl:when test="contains($stringIn, '&#xa;')">
      <xsl:value-of select="substring-before($stringIn, '&#xa;')" disable-output-escaping="yes"/>
      <br/>
      <xsl:call-template name="replace-newline">
          <xsl:with-param name="stringIn" select="substring-after($stringIn,'&#xa;')"/>
      </xsl:call-template>
   </xsl:when>
   <xsl:otherwise>
        <xsl:value-of select="$stringIn" disable-output-escaping="yes"/>
   </xsl:otherwise>
   </xsl:choose>
</xsl:template>

</xsl:stylesheet>

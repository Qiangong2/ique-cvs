<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">

<tr>
 <td class="formLabel2" nowrap="true">Business Unit:</td><td class="formField"></td>
 <td class="formField" nowrap="true"><xsl:value-of disable-output-escaping="yes" select="BUSINESS_NAME"/></td>
</tr> 

<tr>
 <td class="formLabel2" nowrap="true">Model:</td><td class="formField"></td>
 <td class="formField" nowrap="true"><xsl:value-of disable-output-escaping="yes" select="BB_MODEL"/></td>
</tr> 

<tr>
 <td class="formLabel2" nowrap="true">Title:</td><td class="formField"></td>
 <td class="formField" nowrap="true"><xsl:value-of disable-output-escaping="yes" select="TITLE"/></td>
</tr> 

<tr>
 <td class="formLabel2" nowrap="true">Rights:</td><td class="formField"></td>
 <td class="formField" nowrap="true">
       <select>
          <xsl:attribute name="name">rtype</xsl:attribute>
          <xsl:choose>

              <xsl:when test="RTYPE='PR'">
               <option>
                 <xsl:attribute name="selected">true</xsl:attribute>
                 <xsl:attribute name="value">PR</xsl:attribute>
                 <xsl:text>Permanent Right</xsl:text>
               </option>
               <option>
                 <xsl:attribute name="value">LR</xsl:attribute>
                 <xsl:text>Limited Play Right</xsl:text>
               </option>
               <option>
                 <xsl:attribute name="value">TR</xsl:attribute>
                 <xsl:text>Limited Time Right</xsl:text>
               </option>
               <option>
                 <xsl:attribute name="value">XR</xsl:attribute>
                 <xsl:text>Extended Limited Right</xsl:text>
               </option>
              </xsl:when>

              <xsl:when test="RTYPE='LR'">
               <option>
                 <xsl:attribute name="value">PR</xsl:attribute>
                 <xsl:text>Permanent Right</xsl:text>
               </option>
               <option>
                 <xsl:attribute name="selected">true</xsl:attribute>
                 <xsl:attribute name="value">LR</xsl:attribute>
                 <xsl:text>Limited Play Right</xsl:text>
               </option>
               <option>
                 <xsl:attribute name="value">TR</xsl:attribute>
                 <xsl:text>Limited Time Right</xsl:text>
               </option>
               <option>
                 <xsl:attribute name="value">XR</xsl:attribute>
                 <xsl:text>Extended Limited Right</xsl:text>
               </option>
              </xsl:when>

              <xsl:when test="RTYPE='TR'">
               <option>
                 <xsl:attribute name="value">PR</xsl:attribute>
                 <xsl:text>Permanent Right</xsl:text>
               </option>
               <option>
                 <xsl:attribute name="value">LR</xsl:attribute>
                 <xsl:text>Limited Play Right</xsl:text>
               </option>
               <option>
                 <xsl:attribute name="selected">true</xsl:attribute>
                 <xsl:attribute name="value">TR</xsl:attribute>
                 <xsl:text>Limited Time Right</xsl:text>
               </option>
               <option>
                 <xsl:attribute name="value">XR</xsl:attribute>
                 <xsl:text>Extended Limited Right</xsl:text>
               </option>
              </xsl:when>

              <xsl:when test="RTYPE='XR'">
               <option>
                 <xsl:attribute name="value">PR</xsl:attribute>
                 <xsl:text>Permanent Right</xsl:text>
               </option>
               <option>
                 <xsl:attribute name="value">LR</xsl:attribute>
                 <xsl:text>Limited Play Right</xsl:text>
               </option>
               <option>
                 <xsl:attribute name="value">TR</xsl:attribute>
                 <xsl:text>Limited Time Right</xsl:text>
               </option>
               <option>
                 <xsl:attribute name="selected">true</xsl:attribute>
                 <xsl:attribute name="value">XR</xsl:attribute>
                 <xsl:text>Extended Limited Right</xsl:text>
               </option>
              </xsl:when>

              <xsl:otherwise>
               <option>
                 <xsl:attribute name="selected">true</xsl:attribute>
                 <xsl:attribute name="value">PR</xsl:attribute>
                 <xsl:text>Permanent Right</xsl:text>
               </option>
               <option>
                 <xsl:attribute name="value">LR</xsl:attribute>
                 <xsl:text>Limited Play Right</xsl:text>
               </option>
               <option>
                 <xsl:attribute name="value">TR</xsl:attribute>
                 <xsl:text>Limited Time Right</xsl:text>
               </option>
               <option>
                 <xsl:attribute name="value">XR</xsl:attribute>
                 <xsl:text>Extended Limited Right</xsl:text>
               </option>
              </xsl:otherwise>
            </xsl:choose>
        </select>
    <font color="red">*</font>
 </td>
</tr> 

<tr>
 <td class="formLabel2" nowrap="true">Limits:</td><td class="formField"></td>
 <td class="formField" nowrap="true">
 <input>
    <xsl:attribute name="type">text</xsl:attribute>
    <xsl:attribute name="size">30</xsl:attribute>
    <xsl:attribute name="name">limits</xsl:attribute>
    <xsl:attribute name="value"><xsl:value-of select="LIMITS"/></xsl:attribute>
 </input>
 </td>
</tr> 

<tr>
 <td class="formLabel2" nowrap="true">Start Date:</td><td class="formField"></td>
 <td class="formField" nowrap="true"><xsl:value-of select="START_DATE"/></td>
</tr> 

<tr>
 <td class="formLabel2" nowrap="true">End Date:</td><td class="formField"></td>
 <td class="formField" nowrap="true"><xsl:value-of select="END_DATE"/></td>
</tr> 

<tr>
 <td class="formLabel2" nowrap="true">SKU:</td><td class="formField"></td>
 <td class="formField" nowrap="true"><xsl:value-of select="SKU"/></td>
</tr> 

</xsl:template>

</xsl:stylesheet>

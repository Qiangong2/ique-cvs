//-------------------------------------------------------------------
// Trim functions
//   Returns string with whitespace trimmed
//-------------------------------------------------------------------

function LTrim(str) 
{
    if (str==null)
    {
        return str;
    }
    
    for (var i=0; str.charAt(i)==" " || str.charAt(i)=="\n" || str.charAt(i)=="\t"; i++);
    
    return str.substring(i,str.length);
}

function RTrim(str) 
{
    if (str==null)
    {
        return str;
    }
    
    for (var i=str.length-1; str.charAt(i)==" " || str.charAt(i)=="\n" || str.charAt(i)=="\t"; i--);
    
    return str.substring(0,i+1);
}

function Trim(str)
{
    return LTrim(RTrim(str));
}

//-------------------------------------------------------------------
// isNull(value)
//   Returns true if value is null
//-------------------------------------------------------------------
function isNull(val)
{
    return (val==null);
}

//-------------------------------------------------------------------
// isBlank(value)
//   Returns true if value only contains spaces
//-------------------------------------------------------------------
function isBlank(val)
{
    if (val==null)
    {
        return true;
    }

    for (var i=0;i<val.length;i++) 
    {
	if ((val.charAt(i)!=' ')&&(val.charAt(i)!="\t")&&(val.charAt(i)!="\n")&&(val.charAt(i)!="\r"))
        {
            return false;
        }
    }
    
    return true;
}

//-------------------------------------------------------------------
// isInteger(value)
//   Returns true if value contains all digits
//-------------------------------------------------------------------
function isInteger(val)
{
    if (isBlank(val))
    {
        return false;
    }
	
    for(var i=0;i<val.length;i++)
    {
	if(!isDigit(val.charAt(i)))
        {
            return false;
        }
    }

    return true;
}

//-------------------------------------------------------------------
// isNumeric(value)
//   Returns true if value contains a positive float value
//-------------------------------------------------------------------
function isNumeric(val)
{
    return(parseFloat(val,10)==(val*1));
}


//-------------------------------------------------------------------
// isDigit(value)
//   Returns true if value is a 1-character digit
//-------------------------------------------------------------------
function isDigit(num) 
{
    if (num.length>1)
    {
        return false;
    }

    var string="1234567890";

    if (string.indexOf(num)!=-1)
    {
        return true;
    }
	
    return false;
}

function compareDates (value1, value2)    
{   
    var date1, date2;   
    var month1, month2;   
    var year1, year2;   
    
    year1 = value1.substring (0, value1.indexOf("."));   
    month1 = value1.substring (value1.indexOf(".")+1, value1.lastIndexOf("."));   
    date1 = value1.substring (value1.lastIndexOf(".")+1, value1.indexOf(" "));   
    
    year2 = value2.substring (0, value2.indexOf ("."));   
    month2 = value2.substring (value2.indexOf (".")+1, value2.lastIndexOf ("."));   
    date2 = value2.substring (value2.lastIndexOf (".")+1, value2.indexOf(" "));   
    
    if (year2 > year1) return 1;   
    else if (year2 < year1) return -1;   
    else if (month2 > month1) return 1;   
    else if (month2 < month1) return -1;   
    else if (date2 > date1) return 1;   
    else if (date2 < date1) return -1;   
    else return 0;   
} 

function element_exists(element_name) {
    if (document.getElementsByName(element_name).length > 0) 
    {
        return true;
    } else {
        return false;
    }
}


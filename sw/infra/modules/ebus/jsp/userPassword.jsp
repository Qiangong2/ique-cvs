<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.ms.common.QueryBean" %>
<jsp:useBean class="com.broadon.ms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] params = qb.getParamArray();

  String pswdStr = params[0];
  String nameStr = params[1];
  String loginStr = params[2];
%>

<jsp:include page="ebu-header.jsp" flush="true">
    <jsp:param name="page" value="userPassword"/>   
</jsp:include>

<P>
<CENTER>
   <%=session.getAttribute("TEXT_ACCOUNT_FOR")%>&nbsp;
   <FONT class="errorText"><%=nameStr%></FONT>&nbsp;
   <%=session.getAttribute("TEXT_USER_ADDED")%></FONT>
</CENTER>

<P>
<CENTER><%=session.getAttribute("TEXT_NOTE")%></CENTER>

<form name="theForm" id="theForm" action="serv" method="POST" align=center>
<input type="hidden" name="type" value="user"></input>
<input type="hidden" name="action" value="list"></input>

<P>
<TABLE cellSpacing=0 cellPadding=1 width=20% align=center bgColor="#336699" border=0>
  <TR> 
    <TD> 
      <!-- OperationUser Password Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>        
        <TR> 
          <TD bgColor=#efefef>
            <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
              <TR><TD><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
	        <TR>
                  <TD>
		        <CENTER>
                      <B><%=session.getAttribute("COL_EMAIL_ADDRESS")%></B>:&nbsp;<FONT class="successText"><%=loginStr%></FONT>
                    </CENTER>
                  </TD>
              </TR>
              <TR>
                  <TD>
		        <CENTER>
                      <B><%=session.getAttribute("COL_PASSWORD")%></B>:&nbsp;<FONT class="successText"><%=pswdStr%></FONT>
                    </CENTER>
                  </TD>
              </TR>
              <TR><TD><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
              </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>

<P>
<CENTER>
   <INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_OK")%>" OnClick="onClickOK(theForm);">
</CENTER>
               
</FORM name="theForm">


<jsp:include page="footer.jsp" flush="true"/>

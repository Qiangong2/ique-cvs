<%@ page contentType="text/html; charset=utf-8" %>
<%  
    String err = request.getAttribute("ERROR").toString();
    String url = request.getParameter("url");
    if (url==null) url="";
%>

<HTML>
<HEAD>
    <TITLE><%= session.getAttribute("TEXT_LOGIN_TITLE") %></TITLE>
    <SCRIPT LANGUAGE="JavaScript">
    function onClickSubmit(formObj) {
        if (formObj.email.value==null || formObj.email.value=="")
        { alert(<%="\"" + session.getAttribute("ALERT_EMAIL") + "\""%>); return; }
        if (formObj.pwd.value==null || formObj.pwd.value=="")
        { alert(<%="\"" + session.getAttribute("ALERT_PWD") + "\""%>); return; }
        formObj.laction.value = "login";
    }
    </SCRIPT>
   <LINK rel="stylesheet" type="text/css" href="/css/rms.css" title="Style"/>
</HEAD>    
<BODY bgcolor="white" text="black">

<TABLE border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="white">
    <!-- 1st Row: Logo Section -->
    <TR width="100%"> 
        <TD> 
            <TABLE border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="white">
                <TR height="39"> 
                    <TD width="1%" valign="top" bgcolor="white"><IMG border="0" src="/images/logo_whiteonblue.gif" width="176" height="39"> 
                    </TD>
                    <TD width="98%" valign="middle" align="right" nowrap style="background-repeat: no-repeat" background="/images/title_background.gif"> 
                        <FONT class="titleFont">EMS Management System&nbsp;&nbsp;</FONT></TD>
                    <TD width="1%"><IMG border="0" width="1" src="/images/spacer.gif" height="39"></TD>
                </TR>
            </TABLE>
        </TD>
    </TR>
    <!-- 2nd Row: White Line -->
    <TR width="100%" bgcolor=white><TD><IMG border=0 width=1 height=1 src="/images/spacer.gif"></TD></TR>
</TABLE>
<p>
<%if (err!=null && err!="") {%><center><font class="errorText"><%=err%></font></center><p><%}%>

<form name="theForm" action="login" method="POST">
  <input type="hidden" name="laction" value="">
  <input type="hidden" name="url" value="<%=url%>">
  <input type="hidden" name="up" value="1">
<TABLE cellSpacing=0 cellPadding=1 width="500" align="center" bgColor="#336699" border=0>
<TR> 
<TD> 
<!-- Login Table -->
<TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
    <TR bgColor=#336699> 
    <TD width="100%"> 
        <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
            <TR> 
                <TD width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2"><%=session.getAttribute("TEXT_LOGIN")%></FONT></TD>
            </TR>
        </TABLE>
    </TD>
    </TR>
    <TR> 
        <TD bgColor=#efefef>
            <TABLE bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
                <TR><TD class="formLabel2" nowrap="true" width="40%"><%=session.getAttribute("TEXT_EMAIL")%></TD><TD class="formField"></TD>
                    <TD class="formField" nowrap="true" width="60%"><input type="text" name="email" size="30" maxlength="64" value=""></TD>
                </TR>
                <TR><TD class="formLabel2" nowrap="true"><%=session.getAttribute("TEXT_PWD")%></TD><TD class="formField"></TD>
                    <TD class="formField" nowrap="true"><input type="password" name="pwd" size="30" maxlength="24" value=""></TD>
                </TR>
                <TR><TD class="formLabel2" nowrap="true"><%=session.getAttribute("TEXT_LOCALE")%></TD><TD class="formField"></TD>
                    <TD><select class="smallField3" name="current_locale">
                        <option selected value="">English
                        </select>
                    </TD>
                </TR>
                <TR><TD class="formLabel2"><IMG border=0 height=1 src="/images/spacer.gif"></TD><TD class="formField" colspan=2><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
                <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
                <TR>
                    <TD colspan="3" bgcolor="white">
                        <CENTER>
                            <INPUT class="sbutton2" type="submit" value="<%=session.getAttribute("BUTTON_SUBMIT")%>" OnClick="onClickSubmit(theForm);">
                        </CENTER>
                    </TD>
                </TR>
                <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
            </TABLE>
        </TD>
    </TR>
</TABLE>
</TD>
</TR>
</TABLE>
</FORM>
<P>

<SCRIPT LANGUAGE="JavaScript">
<!--
    document.theForm.email.focus();    
-->
</SCRIPT>
<jsp:include page="footer.jsp" flush="true"/>



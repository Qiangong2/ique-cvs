<%
   Object role = session.getAttribute("BCC_ROLE");
   String ROLE_EBU_ADMIN = "110";

   String index=request.getParameter("page");
   if (index==null) {
     index="home";
   }

   String mode=request.getParameter("display");
   if (mode==null) {
     mode="blank";
   }

   String rcflag=request.getParameter("rcflag");
   if (rcflag==null) {
     rcflag="1";
   }
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">

   <%if (index.equals("userList")) {%>
     <TITLE><%= session.getAttribute("OPERATION_USER_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.usort.value = sort;
        }
     </SCRIPT>    

   <%} else if (index.equals("userPassword")) {%>
     <TITLE><%= session.getAttribute("OPERATION_USER_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function onClickOK(formObj) {
           formObj.submit();
      }
     </SCRIPT>       

   <%} else if (index.equals("userAdd")) {%>
     <TITLE><%= session.getAttribute("OPERATION_USER_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
          function onClickSubmit(formObj) {  
            formObj.fullname.value=Trim(formObj.fullname.value);
            if (isNull(formObj.fullname.value) || isBlank(formObj.fullname.value))
            { alert(<%="\"" + session.getAttribute("ALERT_FULLNAME") + "\""%>);
              formObj.fullname.focus();
              return; }

            formObj.email.value=Trim(formObj.email.value);
            if (isNull(formObj.email.value) || isBlank(formObj.email.value))
            { alert(<%="\"" + session.getAttribute("ALERT_EMAIL_ADDRESS") + "\""%>);
              formObj.email.focus();
              return;
            } else {
              var splitted = formObj.email.value.match("^(.+)@(.+)$");
              if (splitted==null)
              {
                var charpos = formObj.email.value.search("[^A-Za-z0-9]");
                if (formObj.email.value.length > 0 && charpos >= 0)
                {
                  alert(<%="\"" + session.getAttribute("ALERT_CHAR_EMAIL_ADDRESS") + "\""%>);
                  formObj.email.focus();
                  return;
                }
              } else {
                if (splitted[1]!=null )
                {
                  var regexp_user=/^\"?[\w-_\.]*\"?$/;
                  if (splitted[1].match(regexp_user)==null)
                  {
                     alert(<%="\"" + session.getAttribute("ALERT_CHAR_EMAIL_ADDRESS") + "\""%>);
                     formObj.email.focus();
                     return;
                  }
                }
                if (splitted[2]!=null)
                {
                  var regexp_domain=/^[\w-\.]*\.[A-Za-z]{2,4}$/;
                  if (splitted[2].match(regexp_domain)==null)
                  {
                    var regexp_ip=/^\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\]$/;
                    if (splitted[2].match(regexp_ip)==null)
                    {
                       alert(<%="\"" + session.getAttribute("ALERT_CHAR_EMAIL_ADDRESS") + "\""%>);
                       formObj.email.focus();
                       return;
                    }
                  }
                }
              }
            }

            if (formObj.status.value==null || formObj.status.value=="")
            { alert(<%="\"" + session.getAttribute("ALERT_STATUS") + "\""%>);
              formObj.status.focus();
              return; }
          
            formObj.storeid.value=Trim(formObj.storeid.value);
            if (!isNull(formObj.storeid.value) && !isBlank(formObj.storeid.value))
            { if (!isInteger(formObj.storeid.value)) {
              alert(<%="\"" + session.getAttribute("ALERT_STORE_ID") + "\""%>);
              formObj.storeid.focus();
              return; }
            }

            if (isNull(formObj.role.value) || isBlank(formObj.role.value))
            { alert(<%="\"" + session.getAttribute("ALERT_ROLE_LEVEL") + "\""%>);
              formObj.role.focus();
              return; }

	    formObj.passwd.value = Math.floor(Math.random() * 100000000);
            formObj.submit();
          }
     </SCRIPT>

   <%} else if (index.equals("userEdit")) {%>
     <TITLE><%= session.getAttribute("OPERATION_USER_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
          function showRoles(opid, buid) {
            window.location.href = "serv?type=userrole&action=edit&opid=" + opid + "&buid=" + buid;
            window.location.href.reload();
          }

          function onClickSubmit(formObj) {  
            formObj.fullname.value=Trim(formObj.fullname.value);
            if (isNull(formObj.fullname.value) || isBlank(formObj.fullname.value))
            { alert(<%="\"" + session.getAttribute("ALERT_FULLNAME") + "\""%>);
              formObj.fullname.focus();
              return; }

            formObj.email.value=Trim(formObj.email.value);
            if (isNull(formObj.email.value) || isBlank(formObj.email.value))
            { alert(<%="\"" + session.getAttribute("ALERT_EMAIL_ADDRESS") + "\""%>);
              formObj.email.focus();
              return;
            } else {
              var splitted = formObj.email.value.match("^(.+)@(.+)$");
              if (splitted==null)
              {
                var charpos = formObj.email.value.search("[^A-Za-z0-9]");
                if (formObj.email.value.length > 0 && charpos >= 0)
                {
                  alert(<%="\"" + session.getAttribute("ALERT_CHAR_EMAIL_ADDRESS") + "\""%>);
                  formObj.email.focus();
                  return;
                }
              } else {
                if (splitted[1]!=null )
                {
                  var regexp_user=/^\"?[\w-_\.]*\"?$/;
                  if (splitted[1].match(regexp_user)==null)
                  {
                     alert(<%="\"" + session.getAttribute("ALERT_CHAR_EMAIL_ADDRESS") + "\""%>);
                     formObj.email.focus();
                     return;
                  }
                }
                if (splitted[2]!=null)
                {
                  var regexp_domain=/^[\w-\.]*\.[A-Za-z]{2,4}$/;
                  if (splitted[2].match(regexp_domain)==null)
                  {
                    var regexp_ip=/^\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\]$/;
                    if (splitted[2].match(regexp_ip)==null)
                    {
                       alert(<%="\"" + session.getAttribute("ALERT_CHAR_EMAIL_ADDRESS") + "\""%>);
                       formObj.email.focus();
                       return;
                    }
                  }
                }
              }
            }

            if (formObj.status.value==null || formObj.status.value=="")
            { alert(<%="\"" + session.getAttribute("ALERT_STATUS") + "\""%>);
              formObj.status.focus();
              return; }
          
            formObj.storeid.value=Trim(formObj.storeid.value);
            if (!isNull(formObj.storeid.value) && !isBlank(formObj.storeid.value))
            { if (!isInteger(formObj.storeid.value)) {
              alert(<%="\"" + session.getAttribute("ALERT_STORE_ID") + "\""%>);
              formObj.storeid.focus();
              return; }
            }

            if (isNull(formObj.role.value) || isBlank(formObj.role.value))
            { alert(<%="\"" + session.getAttribute("ALERT_ROLE_LEVEL") + "\""%>);
              formObj.role.focus();
              return; }

            if (formObj.confpasswd.value != formObj.passwd.value)
            { alert(<%="\"" + session.getAttribute("ALERT_PASSWORD_MISMATCH") + "\""%>);
              formObj.passwd.focus();
              return; }

            if (formObj.email.value!=formObj.last_email.value)
            { if (!(confirm(<%="\"" + session.getAttribute("ALERT_REVOKE_SMARTCARD_EMAIL") + "\""%>))) {
              formObj.email.focus();
              return; }
            }

            if (formObj.status.value=="I")
            { if (!(confirm(<%="\"" + session.getAttribute("ALERT_REVOKE_SMARTCARD_STATUS") + "\""%>))) {
              formObj.status.focus();
              return; }
            }

            formObj.submit();
          }
     </SCRIPT>

   <%} else if (index.equals("businessUnitList")) {%>
     <TITLE><%= session.getAttribute("BUSINESS_UNIT_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.busort.value = sort;
        }
     </SCRIPT>

   <%} else if (index.equals("modelList")) {%>
     <TITLE><%= session.getAttribute("BB_MODEL_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.bbmsort.value = sort;
        }
     </SCRIPT>

   <%} else if (index.equals("mcotList")) {%>
     <TITLE><%= session.getAttribute("BB_CONTENT_TYPE_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.bbctsort.value = sort;
        }
     </SCRIPT>

   <%} else if (index.equals("playerDetail")) {%>
     <TITLE><%= session.getAttribute("PLAYER_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
     function onClickSubmit(formObj) {
        formObj.id.value=Trim(formObj.id.value);
        if (isNull(formObj.id.value) || isBlank(formObj.id.value))
        { alert(<%="\"" + session.getAttribute("ALERT_BB_ID") + "\""%>);
           formObj.id.focus();
           return; 
        } else {
            if (!isInteger(formObj.id.value))
            { alert(<%="\"" + session.getAttribute("ALERT_BAD_BB_ID") + "\""%>);
              formObj.id.focus();
              return;
            }
        }
        formObj.submit();
     }
     </SCRIPT>

   <%} else if (index.equals("playerMftrList"))  {%>
     <TITLE><%= session.getAttribute("PLAYER_MFTR_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.mlsort.value = sort;
        }
     </SCRIPT>

   <%} else if (index.equals("playerMftrDetailList")) {%>
     <TITLE><%= session.getAttribute("PLAYER_MFTR_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, bbmodel, mftrdate, sort) {
            formObj.p.value=newpage;
            formObj.model.value = bbmodel;
            formObj.mdate.value = mftrdate;
            formObj.mdsort.value = sort;
        }
     </SCRIPT>

   <%} else if (index.equals("contentTypesList")) {%>
     <TITLE><%= session.getAttribute("CONTENT_TYPE_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.ctsort.value = sort;
        }
     </SCRIPT>

   <%} else if (index.equals("bundleList")) {%>
     <TITLE><%= session.getAttribute("BB_BUNDLE_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.bbbsort.value = sort;
        }
     </SCRIPT>

   <%} else if (index.equals("bundleEdit")) {%>
     <TITLE><%= session.getAttribute("BB_BUNDLE_TEXT_DESC") %></TITLE>

   <%} else if (index.equals("hwrelList")) {%>
     <TITLE><%=session.getAttribute("BBHR_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.bbhrsort.value = sort;
        }
     </SCRIPT>

   <%} else if (index.equals("lotList")) {%>
     <TITLE><%=session.getAttribute("LOTS_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.lsort.value = sort;
        }
     </SCRIPT>     
     
   <%} else if (index.equals("chipList")) {%>
     <TITLE><%=session.getAttribute("LOTS_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, lotnum, sort) {
            formObj.p.value=newpage;
            formObj.lot.value = lotnum;
            formObj.lcsort.value = sort;
        }
     </SCRIPT>     
     
   <%} else if (index.equals("regionalCenterList")) {%>
     <TITLE><%= session.getAttribute("REGIONAL_CENTERS_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
     function postpage (formObj, newpage, sort) {
        formObj.p.value=newpage;
        formObj.rcsort.value = sort;
     }
     </SCRIPT>
	        
   <%} else if (index.equals("regionalCenterAdd") || index.equals("regionalCenterEdit")) {%>
     <TITLE><%= session.getAttribute("REGIONAL_CENTERS_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
     function onClickSubmit(formObj) {
        formObj.center_loc.value=Trim(formObj.center_loc.value);
        if (isNull(formObj.center_loc.value) || isBlank(formObj.center_loc.value))
        { alert(<%="\"" + session.getAttribute("ALERT_CENTER_LOC") + "\""%>);
          formObj.center_loc.focus();
          return; }

        formObj.center_name.value=Trim(formObj.center_name.value);
        if (isNull(formObj.center_name.value) || isBlank(formObj.center_name.value))
        { alert(<%="\"" + session.getAttribute("ALERT_NAME") + "\""%>);
          formObj.center_name.focus();
          return; }

        formObj.hsm_config_id.value=Trim(formObj.hsm_config_id.value);
        if (!isNull(formObj.hsm_config_id.value) && !isBlank(formObj.hsm_config_id.value) &&
            !isInteger(formObj.hsm_config_id.value))
        { alert(<%="\"" + session.getAttribute("ALERT_HSM_CONFIG_ID") + "\""%>);
          formObj.hsm_config_id.focus();
          return; }

        for (var i=0, j=formObj.elements.length; i<j; i++)
        {   var myName = formObj.elements[i].name;
            if (myName.indexOf('addr') > -1)
            {  formObj.elements[i].value=Trim(formObj.elements[i].value);
               if (isNull(formObj.elements[i].value) || isBlank(formObj.elements[i].value))
               { alert(<%="\"" + session.getAttribute("ALERT_MISSING") + "\""%>);
                 formObj.elements[i].focus();
                 return;
               } else if ((formObj.elements[i].value.indexOf(':') > -1) ||
                          (formObj.elements[i].value.indexOf('/') > -1) ||
                          (formObj.elements[i].value.indexOf(' ') > -1))
               {
                 alert(<%="\"" + session.getAttribute("ALERT_ILLEGAL_LINE1") + "\""%> +"\n" + <%="\"" + session.getAttribute("ALERT_ILLEGAL_LINE2") + "\""%>);
                 formObj.elements[i].focus();
                 return;
               }
            }
        }

        for (var x=0, y=formObj.elements.length; x<y; x++)
        {   var elName = formObj.elements[x].name;
            if (elName.indexOf('addr') > -1)
            {  formObj.elements[x].value=Trim(formObj.elements[x].value);
               var index = elName.charAt(4);
               var prefix = "";
               var suffix = "";

               if (formObj['name'+index].value == "CDDS") {
                   prefix = <%="\"" + session.getAttribute("PREFIX_CDDS") + "\""%>;
                   suffix = <%="\"" + session.getAttribute("SUFFIX_CDDS") + "\""%>;
               } else if (formObj['name'+index].value == "CDS") {
                   prefix = <%="\"" + session.getAttribute("PREFIX_CDS") + "\""%>;
                   suffix = <%="\"" + session.getAttribute("SUFFIX_CDS") + "\""%>;
               } else if (formObj['name'+index].value == "IS") {
                   prefix = <%="\"" + session.getAttribute("PREFIX_IS") + "\""%>;
                   suffix = <%="\"" + session.getAttribute("SUFFIX_IS") + "\""%>;
               } else if (formObj['name'+index].value == "OPS") {
                   prefix = <%="\"" + session.getAttribute("PREFIX_OPS") + "\""%>;
                   suffix = <%="\"" + session.getAttribute("SUFFIX_OPS") + "\""%>;
               } else if (formObj['name'+index].value == "XS") {
                   prefix = <%="\"" + session.getAttribute("PREFIX_XS") + "\""%>;
                   suffix = <%="\"" + session.getAttribute("SUFFIX_XS") + "\""%>;
               }

               if (prefix == null)
                   prefix = "";
               if (suffix == null)
                   suffix = "";

               formObj.elements[x].value=prefix+formObj.elements[x].value+suffix;
            }
        }

        formObj.submit();
     }
     </SCRIPT>

   <%} else if (index.equals("regionList")) {%>
     <TITLE><%= session.getAttribute("REGIONS_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.rgsort.value = sort;
        }
     </SCRIPT>   
	     
   <%} else if (index.equals("regionAdd") || index.equals("regionEdit")) {%>
     <TITLE><%= session.getAttribute("REGIONS_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
     function onClickSubmit(formObj) {
        formObj.region_name.value=Trim(formObj.region_name.value);
        if (isNull(formObj.region_name.value) || isBlank(formObj.region_name.value))
        { alert(<%="\"" + session.getAttribute("ALERT_REGION_NAME") + "\""%>);
          formObj.region_name.focus();
          return; }
        formObj.submit();
     }
     </SCRIPT>

   <%} else if (index.equals("retailerList")) {%>
     <TITLE><%= session.getAttribute("RETAILERS_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.rlsort.value = sort;
        }
     </SCRIPT>
	        
   <%} else if (index.equals("retailerAdd") || index.equals("retailerEdit")) {%>
     <TITLE><%= session.getAttribute("RETAILERS_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
     function onClickSubmit(formObj) {
        formObj.retailer_name.value=Trim(formObj.retailer_name.value);
        if (isNull(formObj.retailer_name.value) || isBlank(formObj.retailer_name.value))
        { alert(<%="\"" + session.getAttribute("ALERT_RETAILER_NAME") + "\""%>);
          formObj.retailer_name.focus();
          return; }
        if (isNull(formObj.code.value) || isBlank(formObj.code.value))
        { alert(<%="\"" + session.getAttribute("ALERT_RETAILER_TYPE") + "\""%>);
          formObj.code.focus();
          return; }
        formObj.submit();
     }
     </SCRIPT>

   <%} else if (index.equals("storeList")) {%>
     <TITLE><%= session.getAttribute("STORES_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.stsort.value = sort;
        }
     </SCRIPT>   
	     
   <%} else if (index.equals("storeAdd") || index.equals("storeEdit")) {%>
     <TITLE><%= session.getAttribute("STORES_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
     function onClickSubmit(formObj) {
        formObj.addr.value=Trim(formObj.addr.value);
        if (isNull(formObj.addr.value) || isBlank(formObj.addr.value))
        { alert(<%="\"" + session.getAttribute("ALERT_ADDRESS") + "\""%>);
          formObj.addr.focus();
          return; }
        formObj.tz.value=Trim(formObj.tz.value);
        if (isNull(formObj.tz.value) || isBlank(formObj.tz.value))
        { alert(<%="\"" + session.getAttribute("ALERT_TIMEZONE") + "\""%>);
          formObj.tz.focus();
          return; }
        if (isNull(formObj.rcid.value) || isBlank(formObj.rcid.value))
        { alert(<%="\"" + session.getAttribute("ALERT_REGIONAL_CENTER") + "\""%>);
          formObj.rcid.focus();
          return; }
        if (isNull(formObj.rgid.value) || isBlank(formObj.rgid.value))
        { alert(<%="\"" + session.getAttribute("ALERT_REGION") + "\""%>);
          formObj.rgid.focus();
          return; }
        if (isNull(formObj.rlid.value) || isBlank(formObj.rlid.value))
        { alert(<%="\"" + session.getAttribute("ALERT_RETAILER") + "\""%>);
          formObj.rlid.focus();
          return; }
        formObj.submit();
     }
     </SCRIPT>

   <%} else if (index.equals("titleList")) {%>
     <TITLE><%= session.getAttribute("TITLE_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.tlsort.value = sort;
        }
     </SCRIPT> 
	       
   <%} else if (index.equals("titleDetail")) {%>
     <TITLE><%= session.getAttribute("TITLE_TEXT_DESC")%></TITLE>

     <%} else if (index.equals("contentList")) {%>
     <TITLE><%= session.getAttribute("CONTENT_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.consort.value = sort;
        }
     </SCRIPT> 
	       
   <%} else if (index.equals("contentDetail")) {%>
     <TITLE><%= session.getAttribute("CONTENT_TEXT_DESC")%></TITLE>

   <%} else if (index.equals("home")) {%>
     <TITLE><%= session.getAttribute("HOME_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
      function onClickSubmit(formObj) {
          if (isNull(formObj.newrole.value) || isBlank(formObj.newrole.value))
          { alert(<%="\"" + session.getAttribute("ALERT_CHANGE_ROLE") + "\""%>);
            formObj.newrole.focus();
            return; }
          formObj.submit();
      }
     </SCRIPT>

   <%}%>

    <SCRIPT LANGUAGE="javascript" src="/js/functions.js"></SCRIPT>
    <SCRIPT LANGUAGE="JavaScript">
    function getHelp(pageIndex) {

        var helpUrl = pageIndex;                 
        if (pageIndex == "regions") {			// Regions
            helpUrl = "regions";
        } else if (pageIndex == "ctr") {		// Content Title Regions
            helpUrl = "ctr";
        } else if (pageIndex == "retailers") {		// Retailers
            helpUrl = "retailers";
        } else if (pageIndex == "stores") {		// Stores
            helpUrl = "stores";
        }
    
        var url = "/broadon/RMS/help/"+helpUrl;
        var helpWin = window.open("", "_bbhelp","status=no,toolbar=no,scrollbars=yes,resizable=yes,menubar=no,copyhistory=no,directories=no,location=no,width=640,height=600");
        helpWin.focus();  // Put help window on top
        helpWin.location = url;  // Load the URL
    }

   </SCRIPT>


   <LINK rel="stylesheet" type="text/css" href="/css/rms.css" title="Style"/>
</HEAD>

<!-- <body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> -->
<!-- <BODY bgcolor="white" link="#003366" vlink="#003366" alink="#003366" text="black"> -->
<BODY bgcolor="white" text="black">

<TABLE border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="white">
  <!-- 1st Row: Logo Section -->
  <TR width="100%"> 
    <TD colspan="2"> 
      <TABLE border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="white">
        <TR height="39"> 
          <TD width="1%" valign="top" bgcolor="white"><IMG border="0" src="/images/logo_whiteonblue.gif" width="176" height="39"> 
          </TD>
          <TD width="98%" valign="middle" align="right" nowrap style="background-repeat: no-repeat" background="/images/title_background.gif"> 
            <FONT class="titleFont">EMS Management System&nbsp;&nbsp;</FONT></TD>
          <TD width="1%"><IMG border="0" width="1" src="/images/spacer.gif" height="39"></TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
  <!-- 2nd Row: White Line -->
  <TR width="100%" bgcolor=white><TD colspan="2"><IMG border=0 width=1 height=1 src="/images/spacer.gif"></TD></TR>
  <!-- 3rd Row: Menu Navigation Bar -->
  <TR width="100%">
    <TD >
      <!-- Top-Level Menu Bar: Menu 1 is selected -->

<%if (role!=null && role.toString().equals(ROLE_EBU_ADMIN)) {%>

      <TABLE border=0 cellpadding=0 cellspacing=0 width=100% bgcolor="#efefef">
        <TR>

  <%if (index.equals("userList") || index.equals("userAdd") || index.equals("userEdit") || index.equals("userPassword")) {%>
          <TD><IMG src="/images/menu.off.start.gif"></TD>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_OPERATION_USERS")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
  <%} else {%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;&nbsp;<A class="mainMenuHeader" HREF="serv?type=user&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_OPERATION_USERS")%></FONT></A>&nbsp;</TD>
  <%}%>

  <%if (index.equals("businessUnitList")) {%>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_BUSINESS_UNIT")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
  <%} else {%>
          <%if (!(index.equals("userList") || index.equals("userAdd") || index.equals("userEdit") || index.equals("userPassword"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=businessUnit&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_BUSINESS_UNIT")%></FONT></A>&nbsp;</TD>
  <%}%>

  <%if (index.equals("modelList") || index.equals("mcotList") || index.equals("bundleList") || index.equals("bundleEdit") ||
        index.equals("hwrelList") || index.equals("playerDetail") || index.equals("playerMftrList") || index.equals("playerMftrDetailList") ||
        index.equals("lotList") || index.equals("chipList")) {%>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_BBHW")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
  <%} else {%>
          <%if (!(index.equals("businessUnitList"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=model&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_BBHW")%></FONT></A>&nbsp;</TD>
  <%}%>

  <%if (index.equals("cotList") || 
        index.equals("titleList") || index.equals("titleDetail") ||
        index.equals("contentList") || index.equals("contentDetail")) {%>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_CDS")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
  <%} else {%>
          <%if (!(index.equals("modelList") || index.equals("mcotList") || index.equals("bundleList") || index.equals("bundleEdit") ||
                  index.equals("hwrelList") || index.equals("playerDetail") || index.equals("playerMftrList") || 
                  index.equals("playerMftrDetailList") || index.equals("lotList") || index.equals("chipList"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=content&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_CDS")%></FONT></A>&nbsp;</TD>
  <%}%>

  <%if (index.equals("regionalCenterList") || index.equals("regionalCenterAdd") || index.equals("regionalCenterEdit")) {%>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_REGIONAL_CENTERS")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
  <%} else {%>
          <%if (!(index.equals("cotList") || 
                  index.equals("titleList") || index.equals("titleDetail") ||
                  index.equals("contentList") ||index.equals("contentDetail"))) {%>
          <TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=regionalCenter&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_REGIONAL_CENTERS")%></FONT></A>&nbsp;</TD> 
  <%}%>

  <%if (index.equals("regionList") || index.equals("regionAdd") || index.equals("regionEdit")) {%>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_REGIONS")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
  <%} else {%>
          <%if (!(index.equals("regionalCenterList") || index.equals("regionalCenterAdd") || index.equals("regionalCenterEdit"))) {%>
          <TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;&nbsp;<A class="mainMenuHeader" HREF="serv?type=region&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_REGIONS")%></FONT></A>&nbsp;</TD>
  <%}%>

  <%if (index.equals("retailerList") || index.equals("retailerAdd") || index.equals("retailerEdit") ||
        index.equals("storeList") || index.equals("storeAdd") || index.equals("storeEdit")) {%>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_CHANNELS")%> </FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
  <%} else {%>
          <%if (!(index.equals("regionList") || index.equals("regionAdd") || index.equals("regionEdit"))) {%>
          <TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=retailer&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_CHANNELS")%></FONT></A>&nbsp;</TD> 
  <%}%>

  <%if (!(index.equals("retailerList") || index.equals("retailerAdd") || index.equals("retailerEdit") || 
          index.equals("storeList") || index.equals("storeAdd") || index.equals("storeEdit"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="javascript:getHelp('<%=index%>')"><FONT class="mainMenuText"><%=session.getAttribute("MENU_HELP")%></FONT></A>&nbsp;</TD>
          <TD bgcolor="#336699" nowrap width="100%" align=right background="/images/menu.off.bg.gif">&nbsp;</TD>
        </TR>
      </TABLE>

<%}%>

     <!-- End of Menu Bar -->
    </TD>
    <TD nowrap align="right" background="/images/menu.off.bg.gif">
      <font class="userText"><%=session.getAttribute("BCC_USER")%>: </font>
      <a class="logoutMenuHeader" href="login?laction=logout" id="logout" target="_top">
      <font class="logoutText"><%=session.getAttribute("TEXT_LOGOUT")%></font></a>&nbsp;&nbsp;
    </TD>
  </TR>
    <!-- Small Gray Bar -->
  <TR width="100%" bgcolor="#efefef">
    <TD colspan="2"><IMG border="0" width="1" height="5" src="/images/spacer.gif"></TD>
  </TR>
</TABLE>

  <!-- Start of 1st Sub-Menu Bar -->

<TABLE width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#efefef">
  <TR width="100%" height="15">

<%if (role!=null && role.toString().equals(ROLE_EBU_ADMIN)) {%>
 <%if (index.equals("modelList") || index.equals("mcotList") || index.equals("bundleList") || index.equals("bundleEdit") ||
      index.equals("hwrelList") || index.equals("playerDetail") || index.equals("playerMftrList") || index.equals("playerMftrDetailList") ||
      index.equals("lotList") || index.equals("chipList")) {%>
  <%if (index.equals("modelList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_BB_MODEL")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=model&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_BB_MODEL")%></FONT></A></TD>
  <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("mcotList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_BB_CONTENT_TYPE")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=mcot&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_BB_CONTENT_TYPE")%></FONT></A></TD>
  <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("bundleList") || index.equals("bundleEdit")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_BB_BUNDLE")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=bundle&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_BB_BUNDLE")%></FONT></A></TD>
  <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("hwrelList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_BBHR")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=hwrel&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_BBHR")%></FONT></A></TD>
  <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("playerDetail") || index.equals("playerMftrList") || index.equals("playerMftrDetailList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_PLAYERS")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=player&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_PLAYERS")%></FONT></A></TD>
  <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("lotList") || index.equals("chipList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_LOTS")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=lot&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_LOTS")%></FONT></A></TD>
  <%}%>
  <!-- End of 1st Sub-Menu Bar for BB Hardware-->

 <%} else if (index.equals("cotList") ||
             index.equals("titleList") || index.equals("titleDetail") ||
             index.equals("contentList") || index.equals("contentDetail")) {%>
  <%if (index.equals("contentList") || index.equals("contentDetail")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_CONTENT")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=content&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_CONTENT")%></FONT></A></TD>
  <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("titleList") || index.equals("titleDetail")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_TITLE")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=title&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_TITLE")%></FONT></A></TD>
  <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("cotList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_CONTENT_TYPE")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=cot&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_CONTENT_TYPE")%></FONT></A></TD>
  <%}%>
  <!-- End of 1st Sub-Menu Bar for Content Browser -->

 <%} else if (index.equals("retailerList") || index.equals("retailerAdd") || index.equals("retailerEdit") ||
       index.equals("storeList") || index.equals("storeAdd") || index.equals("storeEdit")) {%>
  <%if (index.equals("retailerList") || index.equals("retailerAdd") || index.equals("retailerEdit")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_RETAILERS")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=retailer&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_RETAILERS")%></FONT></A></TD>
  <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("storeList") || index.equals("storeAdd") || index.equals("storeEdit")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_STORES")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=store&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_STORES")%></FONT></A></TD>
  <%}%>
  <!-- End of 1st Sub-Menu Bar for Channels -->
 
 <%}%>
<%}%>

      <TD nowrap width="100%" align="right"><A class="subMenuHeader" href="serv?type=home&action=edit" id="currole"><FONT class="subMenuText"><%=session.getAttribute("BCC_ROLE_NAME")%></FONT></A>&nbsp;&nbsp;</TD>
  </TR>
</TABLE>

<!-- Bar under menu -->
<TABLE width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#efefef">
  <TR width="100%">
    <TD><IMG border="0" width="1" height="5" src="/images/spacer.gif"></TD>
  </TR>
  <TR width="100%" bgcolor="#999999">
    <TD><IMG border="0" width="1" height="1" src="/images/spacer.gif"></TD>
  </TR>
</TABLE>
<TABLE width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
  <TR width="100%">
    <TD><IMG border="0" width="1" height="5" src="/images/spacer.gif"></TD>
  </TR>
</TABLE>
<!-- End of bar -->

  <!-- Start of 2nd Sub-Menu Bar -->
<center>
<%if (role!=null && role.toString().equals(ROLE_EBU_ADMIN)) {%>

 <%if (index.equals("userList") || index.equals("userAdd") || index.equals("userEdit")) {%>

  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">

  <%if (index.equals("userList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_OPUSERLIST")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=user&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_OPUSERLIST")%></FONT></A></TD>
  <%}%>

      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("userAdd")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_OPUSERADD")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=user&action=add"><FONT class="subMenuText"><%=session.getAttribute("MENU_OPUSERADD")%></FONT></A></TD>
  <%}%>

  <%if (index.equals("userEdit")){%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_OPUSERUPDATE")%></FONT></TD>
  <%}%>

    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for Users -->

 <%} else if (index.equals("regionalCenterList") || index.equals("regionalCenterAdd") || index.equals("regionalCenterEdit")) {%>

  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">

  <%if (index.equals("regionalCenterList")) {%>
    <%if (!rcflag.equals("0")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_RCLIST")%></FONT></TD>
    <%}%>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=regionalCenter&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_RCLIST")%></FONT></A></TD>
  <%}%>

  <%if (index.equals("regionalCenterAdd") && !rcflag.equals("0")) {%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_RCADD")%></FONT></TD>
  <%} else if (!rcflag.equals("0")) {%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=regionalCenter&action=add"><FONT class="subMenuText"><%=session.getAttribute("MENU_RCADD")%></FONT></A></TD>
  <%}%>

  <%if (index.equals("regionalCenterEdit")){%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_RCUPDATE")%></FONT></TD>
  <%}%>

    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for Regional Centers -->

 <%} else if (index.equals("titleDetail")) {%>

  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=title&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_TLLIST")%></FONT></A></TD>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_TLDETAIL")%></FONT></TD>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for Content Titles -->

 <%} else if (index.equals("retailerList") || index.equals("retailerAdd") || index.equals("retailerEdit")) {%>

  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">
  <%if (index.equals("retailerList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_RLLIST")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=retailer&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_RLLIST")%> </FONT></A></TD>
  <%}%>

      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("retailerAdd")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_RLADD")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=retailer&action=add"><FONT class="subMenuText"><%=session.getAttribute("MENU_RLADD")%> </FONT></A></TD>
  <%}%>

  <%if (index.equals("retailerEdit")) {%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_RLUPDATE")%></FONT></TD>
  <%}%>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of Sub-Menu Bar for Distributors -->

 <%} else if (index.equals("storeList") || index.equals("storeAdd") || index.equals("storeEdit")) {%>

  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">
  <%if (index.equals("storeList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_STLIST")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=store&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_STLIST")%> </FONT></A></TD>
  <%}%>

      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("storeAdd")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_STADD")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=store&action=add"><FONT class="subMenuText"><%=session.getAttribute("MENU_STADD")%> </FONT></A></TD>
  <%}%>

  <%if (index.equals("storeEdit")) {%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_STUPDATE")%></FONT></TD>
  <%}%>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of Sub-Menu Bar for Stores -->

 <%} else if (index.equals("contentDetail")) {%>

  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=content&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_CONLIST")%></FONT></A></TD>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_CONDETAIL")%></FONT></TD>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for Content Object -->

 <%} else if (index.equals("regionList") || index.equals("regionAdd") || index.equals("regionEdit")) {%>

  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">

  <%if (index.equals("regionList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_RGLIST")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=region&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_RGLIST")%></FONT></A></TD>
  <%}%>

      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("regionAdd")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_RGADD")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=region&action=add"><FONT class="subMenuText"><%=session.getAttribute("MENU_RGADD")%></FONT></A></TD>
  <%}%>

  <%if (index.equals("regionEdit")){%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_RGUPDATE")%></FONT></TD>
  <%}%>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for Regions -->

 <%} else if (index.equals("playerDetail") || index.equals("playerMftrList") || index.equals("playerMftrDetailList")) {%>

  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">

  <%if (index.equals("playerDetail")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_PLAYERS_SEARCH")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=player&action=list"><FONT  class="subMenuText"><%=session.getAttribute("MENU_PLAYERS_SEARCH")%></FONT></A></TD>
  <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("playerMftrList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_PLAYERS_MFTR_LIST")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=player.mftr&action=list"><FONT  class="subMenuText"><%=session.getAttribute("MENU_PLAYERS_MFTR_LIST")%></FONT></A></TD>
  <%}%>

  <%if (index.equals("playerMftrDetailList")){%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_PLAYERS_MFTR_DETAILS")%></FONT></TD>
  <%}%>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for Players -->

 <%} else if (index.equals("lotList") || index.equals("chipList")) {%>

  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">

  <%if (index.equals("lotList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_LOTS_LIST")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=lot&action=list"><FONT  class="subMenuText"><%=session.getAttribute("MENU_LOTS_LIST")%></FONT></A></TD>
  <%}%>

  <%if (index.equals("chipList")){%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_LOTS_DETAILS")%></FONT></TD>
  <%}%>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for Lots -->

 <%} else if (index.equals("bundleList") || index.equals("bundleEdit")) {%>

  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">

  <%if (index.equals("bundleList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_BBBLIST")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=bundle&action=list"><FONT  class="subMenuText"><%=session.getAttribute("MENU_BBBLIST")%></FONT></A></TD>
  <%}%>

  <%if (index.equals("bundleEdit")){%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_BBBDETAIL")%></FONT></TD>
  <%}%>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for Lots -->

 <%}%>

<%}%>

</center>
<p>

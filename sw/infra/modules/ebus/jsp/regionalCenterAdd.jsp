<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.ms.common.QueryBean" %>
<jsp:useBean class="com.broadon.ms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();
  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();

  String regionalCenterStr = htmlResults[0];
  int serverCount = Integer.parseInt(session.getAttribute("REGIONAL_SERVER_COUNT").toString());
  String rcflag = "1";
%>

<jsp:include page="ebu-header.jsp" flush="true">
    <jsp:param name="page" value="regionalCenterAdd"/>
    <jsp:param name="rcflag" value="<%=rcflag%>"/>
</jsp:include>

<%if (err!=null && err!="") {%><center><font class="errorText"><%=err%></font></center><p><%}%>

<form name="theForm" id="theForm" action="serv" method="POST" onSubmit="return false;">
<input type="hidden" name="type" value="regionalCenter"></input>
<input type="hidden" name="action" value="insert"></input>
<input type="hidden" name="servernum" value="<%=serverCount%>"></input>

<TABLE cellSpacing=0 cellPadding=1 width=60% align=center bgColor="#336699" border=0>
  <TR> 
    <TD> 
      <!-- Regional Center Detail Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699> 
          <TD width="100%"> 
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              <TR> 
                <TD width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2"><%=session.getAttribute("TEXT_ADD_REGIONAL_CENTER")%></FONT></TD>
              </TR>
             </TABLE>
          </TD>
        </TR>
        <TR> 
          <TD bgColor=#efefef>
            <TABLE bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <TR>
                <TD class="tableheader" align=center width=50%><%=session.getAttribute("COL_PARAMETER")%></TD>
                <TD class="tableheader" align=center width=50% colspan=2><%=session.getAttribute("COL_VALUE")%></TD>
              </TR>
              <%=regionalCenterStr%>
              <% String serverStr = "";
                 for (int i=1; i<=serverCount; i++) {
                     serverStr += "<TR><TD class=\"formLabel2\" nowrap=\"true\">"+session.getAttribute("REGIONAL_SERVER_"+i)+" Server:</TD>";
                     serverStr += "<TD class=\"formField\"></TD>";
                     serverStr += "<TD class=\"formField\" nowrap=\"true\">";
                     serverStr += "<input type=\"hidden\" name=\"name"+i+"\" value=\""+session.getAttribute("REGIONAL_SERVER_"+i)+"\"></input>";
                     serverStr += "<input type=\"text\" name=\"addr"+i+"\" size=\"30\" value=\"\"></input>";
                     serverStr += "<font color=\"red\">*</font></TD></TR>";
                 }
              %>
              <%=serverStr%>
              <TR><TD class="formLabel2"><IMG border=0 height=1 src="/images/spacer.gif"></TD><TD class="formField" colspan=2><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
              <TR><TD colspan="3" bgcolor="white">
                <CENTER><FONT color="red" size="-1">* Required</FONT></CENTER>
              </TD></TR>
              <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
              <TR>
                <TD colspan="3" bgcolor="white">
                <CENTER>
                <INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_SUBMIT")%>" OnClick="onClickSubmit(theForm);">
                <INPUT class="sbutton" type="reset" value="<%=session.getAttribute("BUTTON_RESET")%>">
                <INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_CANCEL")%>" OnClick="history.go(-1)">
                </CENTER>
                </TD>
              </TR>
              <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
</FORM name="theForm">

<jsp:include page="footer.jsp" flush="true"/>


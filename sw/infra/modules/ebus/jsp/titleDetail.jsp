<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.ms.common.QueryBean" %>
<jsp:useBean class="com.broadon.ms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();
  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  String[] params = qb.getParamArray();

  String conStr = htmlResults[0];
  String reviewStr = htmlResults[1];

  String titleName = params[0];

  String[] contentHeader = {"COL_NAME", "COL_ID", "COL_PUB_DATE", "COL_VERSION", "COL_TYPE", 
                            "COL_MIN_UPGRADE_VERSION", "COL_UPGRADE_CONSENT", "COL_LAST_UPDATED"};
  String[] reviewHeader = {"COL_REV_DATE", "COL_REV_BY", "COL_REV_TITLE", "COL_RATINGS"};
%>

<jsp:include page="ebu-header.jsp" flush="true">
  <jsp:param name="page" value="titleDetail"/>   
</jsp:include>

<%if (err!=null && err!="") {%><center><font class="errorText"><%=err%></font></center><p><%}%>

<!-- Title Name -->
<b>Title:&nbsp;</b><%=titleName%>

<p>
<!-- List Contents -->
<TABLE cellSpacing=0 cellPadding=1 width=100% align=center bgColor="#336699" border=0>
  <TR>
    <TD>
      <!-- Regions Detail Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699>
          <TD width="100%">
            <TABLE width="100%" align="center" cellspacing="1" cellpadding="4" bgcolor="#336699">
              <TR><TD width=100% class="tblSubHdrLabel2">
                <%=session.getAttribute("TEXT_CONTENTS_LIST")%>
              </TD></TR>
            </TABLE>
          </TD>
        </TR>
        <TR>
          <TD bgColor=#efefef>
<table border="0" width="100%" align="center" cellspacing="1" cellpadding="4">
<%if (conStr!=null && conStr.indexOf("<tr")>=0) {%>
<tr>
<td class="tableheader" nowrap="true"><%=session.getAttribute("COL_NO")%></td>
  <%for (int i=0; i<contentHeader.length; i++) {%>
    <td class="tableheader" nowrap="true">
      <%=session.getAttribute(contentHeader[i])%>
    </td>
  <%}%>
</tr>
<%=conStr%>
<%} else {%>
 <TR width="100%">
    <TD>
 <!-- No Content Found -->
      <P>
      <CENTER>
       <%=session.getAttribute("TEXT_NO_CONTENT")%>
      </CENTER>
    </TD>
  </TR>
<%}%>
</table>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>

<p>
<!-- List Reviews -->
<TABLE cellSpacing=0 cellPadding=1 width=100% align=center bgColor="#336699" border=0>
  <TR>
    <TD>
      <!-- Regions Detail Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699>
          <TD width="100%">
            <TABLE width="100%" align="center" cellspacing="1" cellpadding="4" bgcolor="#336699">
              <TR><TD width=100% class="tblSubHdrLabel2"> 
                <%=session.getAttribute("TEXT_REVIEWS_LIST")%>
               </TD></TR>
            </TABLE>
          </TD>
        </TR>
        <TR>
          <TD bgColor=#efefef>
<table border="0" width="100%" align="center" cellspacing="1" cellpadding="4">
<%if (reviewStr!=null && reviewStr.indexOf("<tr")>=0) {%>
<tr>
<td class="tableheader" nowrap="true"><%=session.getAttribute("COL_NO")%></td>
  <%for (int i=0; i<reviewHeader.length; i++) {%>
    <td class="tableheader" nowrap="true">
      <%=session.getAttribute(reviewHeader[i])%>
    </td>
  <%}%>
</tr>
<%=reviewStr%>
<%} else {%>
 <TR width="100%">
    <TD>
 <!-- No Review Found -->
      <P>
      <CENTER>
       <%=session.getAttribute("TEXT_NO_REVIEW")%>
      </CENTER>
    </TD>
  </TR>
<%}%>
</table>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
<P>
<CENTER>
  <INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_CANCEL")%>" OnClick="history.go(-1)">
</CENTER>

<jsp:include page="footer.jsp" flush="true"/>


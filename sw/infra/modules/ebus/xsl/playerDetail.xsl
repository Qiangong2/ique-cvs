<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
<tr>
  <td class="formLabel2" nowrap="true">Model:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of disable-output-escaping="yes" select="BB_MODEL"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">HW Revision:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="BB_HWREV"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">Manufacture Date:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="MANUFACTURE_DATE"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">Serial Number:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="SN"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">Business Unit:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of disable-output-escaping="yes" select="BUSINESS_NAME"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">Bundle Start Date:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="BUNDLE_START_DATE"/></td>
</tr>

</xsl:template>

</xsl:stylesheet>

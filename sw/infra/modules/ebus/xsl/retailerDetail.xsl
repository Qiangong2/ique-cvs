<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">

<xsl:choose>
<xsl:when test="RETAILER_ID!=''">                         
    <tr><td class="formLabel2" nowrap="true">Retailer ID:</td><td class="formField"></td>
    <td class="formField" nowrap="true">
	<input>
		<xsl:attribute name="type">hidden</xsl:attribute>
		<xsl:attribute name="name">id</xsl:attribute>
		<xsl:attribute name="value"><xsl:value-of select="RETAILER_ID"/></xsl:attribute>
	</input>
	<xsl:value-of select="RETAILER_ID"/>
    </td></tr> 
</xsl:when>
</xsl:choose>

<tr><td class="formLabel2" nowrap="true">Retailer Name:</td><td class="formField"></td>
<td class="formField" nowrap="true">
<input>
	<xsl:attribute name="type">text</xsl:attribute>
	<xsl:attribute name="size">30</xsl:attribute>
	<xsl:attribute name="maxlength">64</xsl:attribute>
	<xsl:attribute name="name">retailer_name</xsl:attribute>
	<xsl:attribute name="value"><xsl:value-of disable-output-escaping="yes" select="RETAILER_NAME"/></xsl:attribute>
</input>
<font color="red">*</font>
</td></tr> 

<xsl:choose>
<xsl:when test="RETAILER_ID!=''">                         
    <tr><td class="formLabel2" nowrap="true">Stores:</td><td class="formField"></td>
    <td class="formField">
          <xsl:choose>
            <xsl:when test="TOTAL_STORES>0">
              <A class="listText">
                <xsl:attribute name="href">
                  <xsl:text>serv?type=store.retailer&amp;action=list&amp;rlid=</xsl:text>
                  <xsl:value-of select="RETAILER_ID"/>
                </xsl:attribute>
                <xsl:value-of select="TOTAL_STORES"/>
              </A>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="TOTAL_STORES"/>
            </xsl:otherwise>
          </xsl:choose>
    </td></tr> 
</xsl:when>
</xsl:choose>

<xsl:choose>
<xsl:when test="RETAILER_ID!=''">                         
    <tr><td class="formLabel2" nowrap="true">Create Date:</td><td class="formField"></td>
    <td class="formField" nowrap="true">
	<xsl:value-of select="CREATE_DATE"/>
    </td></tr> 
</xsl:when>
</xsl:choose>

<xsl:choose>
<xsl:when test="RETAILER_ID!=''">                         
    <tr><td class="formLabel2" nowrap="true">Revoke Date:</td><td class="formField"></td>
    <td class="formField" nowrap="true">
	<xsl:value-of select="REVOKE_DATE"/>
    </td></tr> 
</xsl:when>
</xsl:choose>

<xsl:choose>
<xsl:when test="RETAILER_ID!=''">                         
    <tr><td class="formLabel2" nowrap="true">Suspend Date:</td><td class="formField"></td>
    <td class="formField" nowrap="true">
	<xsl:value-of select="SUSPEND_DATE"/>
    </td></tr> 
</xsl:when>
</xsl:choose>

</xsl:template>

</xsl:stylesheet>

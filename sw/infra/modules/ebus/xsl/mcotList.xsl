<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW[position() mod 2 = 1]">
   <tr class="oddrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText" nowrap="true"><xsl:value-of disable-output-escaping="yes" select="BB_MODEL"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of disable-output-escaping="yes" select="CONTENT_OBJECT_TYPE"/></td>
        <td class="smallText"><xsl:value-of disable-output-escaping="yes" select="DESCRIPTION"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="LAST_UPDATED"/></td>	
   </tr>
</xsl:template>

<xsl:template match="ROW">
   <tr class="evenrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText" nowrap="true"><xsl:value-of disable-output-escaping="yes" select="BB_MODEL"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of disable-output-escaping="yes" select="CONTENT_OBJECT_TYPE"/></td>
        <td class="smallText"><xsl:value-of disable-output-escaping="yes" select="DESCRIPTION"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="LAST_UPDATED"/></td>  
   </tr>
</xsl:template>

</xsl:stylesheet>

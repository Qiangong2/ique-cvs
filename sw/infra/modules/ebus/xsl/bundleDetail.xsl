<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW[position() mod 2 = 1]">
   <tr class="oddrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=title&amp;action=edit&amp;titleid=</xsl:text>
              <xsl:value-of select="TITLE_ID"/>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="TITLE"/>
          </A>    
        </td>
	<td class="smallText2"><xsl:value-of select="TITLE_ID"/></td>	
	<td class="smallText" nowrap="true">
           <xsl:choose>
               <xsl:when test="RTYPE='PR'">
                 <xsl:text>Permanent Right</xsl:text>
               </xsl:when>
               <xsl:when test="RTYPE='LR'">
                 <xsl:text>Limited Play Right</xsl:text>
               </xsl:when>
               <xsl:when test="RTYPE='TR'">
                 <xsl:text>Limited Time Right</xsl:text>
               </xsl:when>
               <xsl:when test="RTYPE='XR'">
                 <xsl:text>Extended Limited Right</xsl:text>
               </xsl:when>
               <xsl:otherwise>
                 <xsl:text>Permanent Right</xsl:text>
               </xsl:otherwise>
          </xsl:choose>
        </td>
	<td class="smallText2"><xsl:value-of select="LIMITS"/></td>	
   </tr>
</xsl:template>

<xsl:template match="ROW">
   <tr class="evenrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=title&amp;action=edit&amp;titleid=</xsl:text>
              <xsl:value-of select="TITLE_ID"/>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="TITLE"/>
          </A>    
        </td>
	<td class="smallText2"><xsl:value-of select="TITLE_ID"/></td>	
	<td class="smallText" nowrap="true">
           <xsl:choose>
               <xsl:when test="RTYPE='PR'">
                 <xsl:text>Permanent Right</xsl:text>
               </xsl:when>
               <xsl:when test="RTYPE='LR'">
                 <xsl:text>Limited Play Right</xsl:text>
               </xsl:when>
               <xsl:when test="RTYPE='TR'">
                 <xsl:text>Limited Time Right</xsl:text>
               </xsl:when>
               <xsl:when test="RTYPE='XR'">
                 <xsl:text>Extended Limited Right</xsl:text>
               </xsl:when>
               <xsl:otherwise>
                 <xsl:text>Permanent Right</xsl:text>
               </xsl:otherwise>
          </xsl:choose>
        </td>
	<td class="smallText2"><xsl:value-of select="LIMITS"/></td>	
   </tr>
</xsl:template>

</xsl:stylesheet>

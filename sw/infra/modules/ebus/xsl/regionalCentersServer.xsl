<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
<xsl:variable name="counter"><xsl:value-of select="NO"/></xsl:variable>
<tr>
  <td class="formLabel2" nowrap="true">
     <input>
        <xsl:attribute name="type">hidden</xsl:attribute>
        <xsl:attribute name="name"><xsl:value-of select="concat('name', $counter)"/></xsl:attribute>
        <xsl:attribute name="value"><xsl:value-of select="SERVER_TYPE"/></xsl:attribute>
     </input>
     <xsl:value-of select="SERVER_TYPE"/>
     <xsl:text> Server:</xsl:text>
  </td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
     <input>
        <xsl:attribute name="type">text</xsl:attribute>
        <xsl:attribute name="size">30</xsl:attribute>
        <xsl:attribute name="name"><xsl:value-of select="concat('addr', $counter)"/></xsl:attribute>
        <xsl:attribute name="value">
            <xsl:choose>
		<xsl:when test="contains(SERVER_ADDR, 'http://')">
		    <xsl:variable name="first"><xsl:value-of select="substring-after(SERVER_ADDR, 'http://')"/></xsl:variable>
                    <xsl:choose>
			<xsl:when test="contains($first, ':')">
				<xsl:value-of select="substring-before($first, ':')"/>
                        </xsl:when>
			<xsl:when test="contains($first, '/')">
				<xsl:value-of select="substring-before($first, '/')"/>
                        </xsl:when>
                        <xsl:otherwise>
				<xsl:value-of select="$first"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
		<xsl:when test="contains(SERVER_ADDR, 'https://')">
		    <xsl:variable name="second"><xsl:value-of select="substring-after(SERVER_ADDR, 'https://')"/></xsl:variable>
                    <xsl:choose>
			<xsl:when test="contains($second, ':')">
				<xsl:value-of select="substring-before($second, ':')"/>
                        </xsl:when>
			<xsl:when test="contains($second, '/')">
				<xsl:value-of select="substring-before($second, '/')"/>
                        </xsl:when>
                        <xsl:otherwise>
				<xsl:value-of select="$second"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
		<xsl:when test="contains(SERVER_ADDR, ':')">
  		    <xsl:value-of select="substring-before(SERVER_ADDR, ':')"/>
                </xsl:when>
		<xsl:when test="contains(SERVER_ADDR, '/')">
  		    <xsl:value-of select="substring-before(SERVER_ADDR, '/')"/>
                </xsl:when>
                <xsl:otherwise>
  	            <xsl:value-of select="SERVER_ADDR"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:attribute>   
     </input>
     <font color="red">*</font>
  </td>
</tr> 

</xsl:template>

</xsl:stylesheet>

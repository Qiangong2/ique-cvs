<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
 
      <option>
      <xsl:attribute name="value"><xsl:value-of select="TITLE_ID"/></xsl:attribute>
      <xsl:value-of disable-output-escaping="yes" select="TITLE"/><xsl:text> (</xsl:text><xsl:value-of select="TITLE_ID"/><xsl:text>)</xsl:text>
      </option>
 
</xsl:template>

</xsl:stylesheet>

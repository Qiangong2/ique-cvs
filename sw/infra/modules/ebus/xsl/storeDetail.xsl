<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">

<xsl:choose>
<xsl:when test="STORE_ID!=''">
    <tr><td class="formLabel2" nowrap="true">Store ID:</td><td class="formField"></td>
    <td class="formField" nowrap="true">
	<input>
	    <xsl:attribute name="type">hidden</xsl:attribute>
	    <xsl:attribute name="name">id</xsl:attribute>
	    <xsl:attribute name="value"><xsl:value-of select="STORE_ID"/></xsl:attribute>
	</input>
        <xsl:value-of select="STORE_ID"/>
    </td></tr>
</xsl:when>
</xsl:choose>

<tr><td class="formLabel2" nowrap="true">Address:</td><td class="formField"></td>
<td class="formField" nowrap="true">
<input>
    <xsl:attribute name="type">text</xsl:attribute>
    <xsl:attribute name="size">30</xsl:attribute>
    <xsl:attribute name="maxlength">128</xsl:attribute>
    <xsl:attribute name="name">addr</xsl:attribute>
    <xsl:attribute name="value"><xsl:value-of disable-output-escaping="yes" select="ADDRESS"/></xsl:attribute>
</input>
<font color="red">*</font>
</td></tr> 

<tr><td class="formLabel2" nowrap="true">Timezone:</td><td class="formField"></td>
<td class="formField" nowrap="true">
<input>
    <xsl:attribute name="type">text</xsl:attribute>
    <xsl:attribute name="size">30</xsl:attribute>
    <xsl:attribute name="maxlength">64</xsl:attribute>
    <xsl:attribute name="name">tz</xsl:attribute>
    <xsl:attribute name="value"><xsl:value-of select="TIMEZONE"/></xsl:attribute>
</input>
<font color="red">*</font>
</td></tr> 

<xsl:choose>
<xsl:when test="STORE_ID!=''">                         
    <tr><td class="formLabel2" nowrap="true">Create Date:</td><td class="formField"></td>
    <td class="formField" nowrap="true">
	<xsl:value-of select="CREATE_DATE"/>
    </td></tr> 
</xsl:when>
</xsl:choose>

<xsl:choose>
<xsl:when test="STORE_ID!=''">                         
    <tr><td class="formLabel2" nowrap="true">Revoke Date:</td><td class="formField"></td>
    <td class="formField" nowrap="true">
	<xsl:value-of select="REVOKE_DATE"/>
    </td></tr> 
</xsl:when>
</xsl:choose>

<xsl:choose>
<xsl:when test="STORE_ID!=''">                         
    <tr><td class="formLabel2" nowrap="true">Suspend Date:</td><td class="formField"></td>
    <td class="formField" nowrap="true">
	<xsl:value-of select="SUSPEND_DATE"/>
    </td></tr> 
</xsl:when>
</xsl:choose>

</xsl:template>

</xsl:stylesheet>

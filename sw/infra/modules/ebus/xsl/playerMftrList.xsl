<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW[position() mod 2 = 1]">
   <tr class="oddrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText" nowrap="true"><xsl:value-of select="BB_MODEL"/></td>
        <td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=player.mftr.detail&amp;action=list&amp;model=</xsl:text>
              <xsl:value-of select="BB_MODEL"/>
              <xsl:text>&amp;mdate=</xsl:text>
              <xsl:value-of select="substring-before(MDATE, ' ')"/>
            </xsl:attribute>
            <xsl:value-of select="substring-before(MDATE, ' ')"/>
          </A>    
        </td>
        <td class="smallText2"><xsl:value-of select="QUANTITY"/></td>
   </tr>
</xsl:template>

<xsl:template match="ROW">
   <tr class="evenrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText" nowrap="true"><xsl:value-of select="BB_MODEL"/></td>
        <td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=player.mftr.detail&amp;action=list&amp;model=</xsl:text>
              <xsl:value-of select="BB_MODEL"/>
              <xsl:text>&amp;mdate=</xsl:text>
              <xsl:value-of select="substring-before(MDATE, ' ')"/>
            </xsl:attribute>
            <xsl:value-of select="substring-before(MDATE, ' ')"/>
          </A>    
        </td>
        <td class="smallText2"><xsl:value-of select="QUANTITY"/></td>
   </tr>
</xsl:template>

</xsl:stylesheet>

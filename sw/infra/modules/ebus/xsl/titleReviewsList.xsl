<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
	extension-element-prefixes="xalan">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW[position() mod 2 = 1]">
   <tr class="oddrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="REVIEW_DATE"/></td>
	<td class="smallText"><xsl:value-of disable-output-escaping="yes" select="REVIEWED_BY"/></td>
	<td class="smallText"><xsl:value-of disable-output-escaping="yes" select="REVIEW_TITLE"/></td>
	<td class="smallText2"><xsl:value-of select="RATINGS"/></td>
   </tr>
</xsl:template>

<xsl:template match="ROW">
   <tr class="evenrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="REVIEW_DATE"/></td>
	<td class="smallText"><xsl:value-of disable-output-escaping="yes" select="REVIEWED_BY"/></td>
	<td class="smallText"><xsl:value-of disable-output-escaping="yes" select="REVIEW_TITLE"/></td>
	<td class="smallText2"><xsl:value-of select="RATINGS"/></td>
  </tr>
</xsl:template>

</xsl:stylesheet>

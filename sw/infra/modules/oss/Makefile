INFRA_BASE := $(shell while [ ! -e Makefile.setup ]; do cd .. ; done; pwd)
include $(INFRA_BASE)/Makefile.setup

SERVICE_BASE = $(INFRA_BASE)/modules/oss

JAVA_BASE = $(MODULE_JAVASRC)

LOCAL_SUBDIR = \
	$(JAVA_BASE)/com/broadon/oss

LINKED_SUBDIR = 

DEFAULT_LOCALE = en_US
LOCALES = en_US ja_JP common
SUBDIR = $(LINKED_SUBDIR) $(LOCAL_SUBDIR)

TARGET = $(CLASSFILES) $(OBJFILES) 

PACKAGE_BASE = oss
PACKAGE = com.broadon.$(PACKAGE_BASE)
PACKAGE_PATH = com/broadon/$(PACKAGE_BASE)

PACKAGES = $(PACKAGE) \
	com.broadon.oss

CLASSDIR = $(SERVICE_BASE)/WEB-INF/classes
LIBDIR = $(SERVICE_BASE)/WEB-INF/lib

CLASSPATH := $(CLASSPATH):$(CLASSDIR):$(AXIS_CLASSPATH)

#
# Java Section
#

JFILES = 
CLASSFILES = $(patsubst %.java,$(CLASSDIR)/%.class,$(JFILES))
LIBFILES = $(patsubst %,$(LIBDIR)/%,wsclient.jar)

MODULE_LIB_FILES = $(patsubst %,$(MODULE_LIB)/%,cas.jar)

default all: makedir subdirs $(TARGET)

makedir:
	@mkdir -p $(CLASSDIR)

subdirs:
	@for file in $(SUBDIR); do \
	    make -C $$file $(MAKECMDGOALS); \
	done

include oss/VERSION

HTDOCS_DIR = $(BUILD_DIR)/package/htdocs
.PNONY: $(HTDOCS_DIR)
$(HTDOCS_DIR): build_dir
	@for locale in $(LOCALES); do \
		mkdir -p $@/$$locale; \
		rsync -aC WebContent/common/images \
	              WebContent/common/js \
                  WebContent/common/css \
                  WebContent/common/html \
                  WebContent/common/titles \
	              $@/$$locale; \
		rsync -aC WebContent/$$locale/images \
				  WebContent/$$locale/js \
                  WebContent/$$locale/css \
                  WebContent/$$locale/html \
                  WebContent/$$locale/titles \
	              $@/$$locale; \
	done

install: default build_dir $(HTDOCS_DIR) $(BUILD_DIR)/package/webapps/oss.war
	(cd conf; cp BBserver.properties.tmpl 	\
		     oss.conf oss.xml log4j.properties $(BUILD_DIR)/package/conf)
	(cd oss/control; cp postinst preinst $(BUILD_DIR)/control)
	(cd oss/cmd; cp getVersion local-getHealth local-init $(BUILD_DIR)/cmd)
	$(RELGEN) $(MODULE_SRC)/scripts/getStat > $(BUILD_DIR)/cmd/getStat
	chmod a+x $(BUILD_DIR)/cmd/getStat
	$(RELGEN) oss/release.dsc-tmpl > $(BUILD_DIR)/control/release.dsc
	cp $(BUILD_DIR)/control/release.dsc $(BUILD_DIR)/release.dsc
	$(PKGEN) --indir $(BUILD_DIR) --outfile $(BUILD_DIR)/$(MODULE_NAME).pkg
	$(RELGEN) oss/sw_rel.xml-tmpl > $(BUILD_DIR)/sw_rel.xml

build_dir:
	rm -fr $(BUILD_DIR)
	mkdir -p $(BUILD_DIR)/package $(BUILD_DIR)/cmd $(BUILD_DIR)/control
	(cd $(BUILD_DIR)/package; mkdir -p conf webapps htdocs)

PROPDIR_UTF8 = resources
PROPDIR = WEB-INF/classes/resources

PROPFILES_UTF8 = $(shell ls $(PROPDIR_UTF8)/*.properties)
PROPFILES = $(patsubst $(PROPDIR_UTF8)/%.properties,$(PROPDIR)/%.properties,$(PROPFILES_UTF8))
DEFAULT_PROPFILES_UTF8 = $(shell ls $(PROPDIR_UTF8)/*_$(DEFAULT_LOCALE).properties)
DEFAULT_PROPFILES = $(patsubst $(PROPDIR_UTF8)/%_$(DEFAULT_LOCALE).properties,$(PROPDIR)/%,$(DEFAULT_PROPFILES_UTF8))

prop_dir:
	rm -fr $(PROPDIR)
	mkdir -p $(PROPDIR)

$(PROPDIR)/%.properties: $(PROPDIR_UTF8)/%.properties
	native2ascii -encoding UTF8 $< $@

resources: prop_dir $(PROPFILES)
	@for file in $(DEFAULT_PROPFILES); do \
		echo cp $$file\_$(DEFAULT_LOCALE).properties $$file.properties ; \
		cp --reply no $$file\_$(DEFAULT_LOCALE).properties $$file.properties ; \
	done

$(BUILD_DIR)/package/webapps/oss.war: $(CLASSFILES) resources WEB-INF/web.xml \
  $(MODULE_LIB)/cas.jar $(MODULE_LIB)/creditcard.jar \
  $(IMPORT_LIB)/jstl.jar $(IMPORT_LIB)/standard.jar $(IMPORT_TLD)/c.tld  $(IMPORT_TLD)/fmt.tld
	mkdir -p webapp/WEB-INF/lib webapp/WEB-INF/tld
	@for locale in $(LOCALES); do \
		rsync -aC WebContent/common/jsp \
	              WebContent/common/txt \
                  webapp/$$locale; \
		rsync -aC WebContent/$$locale/jsp \
	              WebContent/$$locale/txt \
	              webapp/$$locale; \
	    echo $$locale copied; \
	done
	rsync -a WebContent/WEB-INF/BBserver.properties webapp/WEB-INF
	cp $(MODULE_LIB)/cas.jar webapp/WEB-INF/lib
	cp $(MODULE_LIB)/creditcard.jar webapp/WEB-INF/lib
	cp $(MODULE_LIB)/wsclient.jar webapp/WEB-INF/lib
	cp $(IMPORT_LIB)/jstl.jar webapp/WEB-INF/lib
	cp $(IMPORT_LIB)/standard.jar webapp/WEB-INF/lib
	cp $(IMPORT_TLD)/c.tld webapp/WEB-INF/tld
	cp $(IMPORT_TLD)/fmt.tld webapp/WEB-INF/tld
	cp $(IMPORT_LIB)/jaxb-api.jar webapp/WEB-INF/lib
	cp $(IMPORT_LIB)/jaxb-impl.jar webapp/WEB-INF/lib
	cp $(IMPORT_LIB)/jaxb-libs.jar webapp/WEB-INF/lib
	cp $(IMPORT_LIB)/jaxb-xjc.jar webapp/WEB-INF/lib
	cp $(IMPORT_LIB)/namespace.jar webapp/WEB-INF/lib
	cp $(IMPORT_LIB)/jax-qname.jar webapp/WEB-INF/lib
	cp $(IMPORT_LIB)/relaxngDatatype.jar webapp/WEB-INF/lib
	cp -f WEB-INF/web.xml webapp/WEB-INF
	cp -rf WEB-INF/classes webapp/WEB-INF
	jar -cf $@ -C webapp WEB-INF
	@for locale in $(LOCALES); do \
		jar -uf $@ -C webapp $$locale; \
	done
	# rm -fr webapp

.SUFFIXES: .class .java

$(CLASSFILES): $(JFILES)
	$(JCFLAGS) $^ -d $(CLASSDIR)

JAVADOC_DIR = doc

clobber clean: subdirs
	/bin/rm -fr *.class *.o .deps $(TARGET) $(CLASSDIR) $(JAVADOC_DIR) \
		javadoc $(BUILD_DIR) WEB-INF/lib webapp

javadoc: 
	mkdir -p $(JAVADOC_DIR)
	javadoc -author -version -windowtitle "$(PACKAGE)"	\
	-classpath $(CLASSPATH) -d $(JAVADOC_DIR)		\
	-link http://java.sun.com/j2se/1.4.2/docs/api/		\
	-link http://jakarta.apache.org/tomcat/tomcat-4.1-doc/servletapi \
	-link http://ws.apache.org/axis/java/apiDocs		\
	-sourcepath $(JAVA_BASE):JavaSource $(PACKAGES) 

config_libs:

TLD_DIR=WebContent/WEB-INF/tld
DEPLOY_DIR=WebContent/WEB-INF/lib
CLASSES_DIR=WebContent/WEB-INF/classes

.PHONY: eclipse_dev
eclipse_dev: resources
#	rm -fr $(DEPLOY_DIR)
	mkdir -p $(DEPLOY_DIR) $(CLASSES_DIR) $(TLD_DIR)
	cp -rp $(PROPDIR) $(CLASSES_DIR)
	cp $(MODULE_LIB)/wsclient.jar $(DEPLOY_DIR)
	cp $(MODULE_LIB)/creditcard.jar $(DEPLOY_DIR)
	cp $(IMPORT_LIB)/axis-ant.jar $(DEPLOY_DIR)
	cp $(IMPORT_LIB)/axis-schema.jar $(DEPLOY_DIR)
	cp $(IMPORT_LIB)/axis.jar $(DEPLOY_DIR)
	cp $(IMPORT_LIB)/commons-discovery-0.2.jar $(DEPLOY_DIR)
	cp $(IMPORT_LIB)/commons-logging-1.0.4.jar $(DEPLOY_DIR)
	cp $(IMPORT_LIB)/jaxrpc.jar $(DEPLOY_DIR)
	cp $(IMPORT_LIB)/log4j-1.2.8.jar $(DEPLOY_DIR)
	cp $(IMPORT_LIB)/saaj.jar $(DEPLOY_DIR)
	cp $(IMPORT_LIB)/wsdl4j-1.5.1.jar $(DEPLOY_DIR)
	cp $(MODULE_LIB)/common.jar $(DEPLOY_DIR)
	cp $(IMPORT_LIB)/jstl.jar $(DEPLOY_DIR)
	cp $(IMPORT_LIB)/standard.jar $(DEPLOY_DIR)
	cp $(IMPORT_TLD)/c.tld $(TLD_DIR)
	cp $(IMPORT_TLD)/fmt.tld $(TLD_DIR)
	cp $(IMPORT_LIB)/jaxb-api.jar $(DEPLOY_DIR)
	cp $(IMPORT_LIB)/jaxb-impl.jar $(DEPLOY_DIR)
	cp $(IMPORT_LIB)/jaxb-libs.jar $(DEPLOY_DIR)
	cp $(IMPORT_LIB)/jaxb-xjc.jar $(DEPLOY_DIR)
	cp $(IMPORT_LIB)/namespace.jar $(DEPLOY_DIR)
	cp $(IMPORT_LIB)/jax-qname.jar $(DEPLOY_DIR)
	cp $(IMPORT_LIB)/relaxngDatatype.jar $(DEPLOY_DIR)
	
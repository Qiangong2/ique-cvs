/**
 * ETicketSOAPBindingImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ets;

public class ETicketSOAPBindingImpl extends com.broadon.ets.ETicketServiceImpl 
	implements com.broadon.wsapi.ets.ETicketPortType 
{
	
    public com.broadon.wsapi.ets.GetTicketsResponseType getTickets(com.broadon.wsapi.ets.GetTicketsRequestType getTicketsRequest) throws java.rmi.RemoteException {
        return super.getTickets(getTicketsRequest);
    }

    public java.lang.String getHealth() throws java.rmi.RemoteException {
        return super.getHealth();
    }

}

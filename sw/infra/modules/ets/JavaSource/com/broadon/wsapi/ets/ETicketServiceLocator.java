/**
 * ETicketServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ets;

public class ETicketServiceLocator extends org.apache.axis.client.Service implements com.broadon.wsapi.ets.ETicketService {

    public ETicketServiceLocator() {
    }


    public ETicketServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ETicketServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ETicketSOAP
    private java.lang.String ETicketSOAP_address = "http://ets:17100/ets/services/ETicketSOAP";

    public java.lang.String getETicketSOAPAddress() {
        return ETicketSOAP_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ETicketSOAPWSDDServiceName = "ETicketSOAP";

    public java.lang.String getETicketSOAPWSDDServiceName() {
        return ETicketSOAPWSDDServiceName;
    }

    public void setETicketSOAPWSDDServiceName(java.lang.String name) {
        ETicketSOAPWSDDServiceName = name;
    }

    public com.broadon.wsapi.ets.ETicketPortType getETicketSOAP() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ETicketSOAP_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getETicketSOAP(endpoint);
    }

    public com.broadon.wsapi.ets.ETicketPortType getETicketSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.broadon.wsapi.ets.ETicketSOAPBindingStub _stub = new com.broadon.wsapi.ets.ETicketSOAPBindingStub(portAddress, this);
            _stub.setPortName(getETicketSOAPWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setETicketSOAPEndpointAddress(java.lang.String address) {
        ETicketSOAP_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.broadon.wsapi.ets.ETicketPortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.broadon.wsapi.ets.ETicketSOAPBindingStub _stub = new com.broadon.wsapi.ets.ETicketSOAPBindingStub(new java.net.URL(ETicketSOAP_address), this);
                _stub.setPortName(getETicketSOAPWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ETicketSOAP".equals(inputPortName)) {
            return getETicketSOAP();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("urn:ets.wsapi.broadon.com", "ETicketService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("urn:ets.wsapi.broadon.com", "ETicketSOAP"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ETicketSOAP".equals(portName)) {
            setETicketSOAPEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}

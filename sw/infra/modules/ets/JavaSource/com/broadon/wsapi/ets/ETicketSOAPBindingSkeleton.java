/**
 * ETicketSOAPBindingSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ets;

import javax.xml.rpc.ServiceException;
import javax.xml.rpc.server.ServiceLifecycle;

public class ETicketSOAPBindingSkeleton 
	implements ServiceLifecycle, com.broadon.wsapi.ets.ETicketPortType, org.apache.axis.wsdl.Skeleton 
{
    private com.broadon.wsapi.ets.ETicketPortType impl;
    private static java.util.Map _myOperations = new java.util.Hashtable();
    private static java.util.Collection _myOperationsList = new java.util.ArrayList();

    /**
    * Returns List of OperationDesc objects with this name
    */
    public static java.util.List getOperationDescByName(java.lang.String methodName) {
        return (java.util.List)_myOperations.get(methodName);
    }

    /**
    * Returns Collection of OperationDescs
    */
    public static java.util.Collection getOperationDescs() {
        return _myOperationsList;
    }

    static {
        org.apache.axis.description.OperationDesc _oper;
        org.apache.axis.description.ParameterDesc [] _params;
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ets.wsapi.broadon.com", "GetTicketsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ets.wsapi.broadon.com", "GetTicketsRequestType"), com.broadon.wsapi.ets.GetTicketsRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getTickets", _params, new javax.xml.namespace.QName("urn:ets.wsapi.broadon.com", "GetTicketsResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ets.wsapi.broadon.com", "GetTicketsResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "GetTickets"));
        _oper.setSoapAction("urn:ets.wsapi.broadon.com/GetTickets");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getTickets") == null) {
            _myOperations.put("getTickets", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getTickets")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
        };
        _oper = new org.apache.axis.description.OperationDesc("getHealth", _params, new javax.xml.namespace.QName("", "GetHealthResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "GetHealth"));
        _oper.setSoapAction("urn:ets.wsapi.broadon.com/GetHealth");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getHealth") == null) {
            _myOperations.put("getHealth", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getHealth")).add(_oper);
    }

    public ETicketSOAPBindingSkeleton() {
        this.impl = new com.broadon.wsapi.ets.ETicketSOAPBindingImpl();
    }

    public ETicketSOAPBindingSkeleton(com.broadon.wsapi.ets.ETicketPortType impl) {
        this.impl = impl;
    }
    public com.broadon.wsapi.ets.GetTicketsResponseType getTickets(com.broadon.wsapi.ets.GetTicketsRequestType getTicketsRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ets.GetTicketsResponseType ret = impl.getTickets(getTicketsRequest);
        return ret;
    }

    public java.lang.String getHealth() throws java.rmi.RemoteException
    {
        java.lang.String ret = impl.getHealth();
        return ret;
    }

    public void init(Object context) throws ServiceException  
    { 
        if (impl instanceof ServiceLifecycle) {
            ((ServiceLifecycle) impl).init(context);
        }
    }
 
    public void destroy()
    { 
        if (impl instanceof ServiceLifecycle) {
            ((ServiceLifecycle) impl).destroy();
        }
    }
}

/**
 * GetTicketsResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ets;

public class GetTicketsResponseType  extends com.broadon.wsapi.ets.AbstractTransactionType  implements java.io.Serializable {
    private int errorCode;
    private java.lang.String errorMessage;
    /** In case of error, specify the
 * 										"first" title ID for
 * 										which a ticket cannot be
 * 										generated. */
    private java.lang.String errorTitleId;
    private com.broadon.wsapi.ets.ETicketPackageType ETicketPackage;
    /** Optional comment string for debugging and documentation purpose. */
    private java.lang.String comment;

    public GetTicketsResponseType() {
    }

    public GetTicketsResponseType(
           int errorCode,
           java.lang.String errorMessage,
           java.lang.String errorTitleId,
           com.broadon.wsapi.ets.ETicketPackageType ETicketPackage,
           java.lang.String comment) {
           this.errorCode = errorCode;
           this.errorMessage = errorMessage;
           this.errorTitleId = errorTitleId;
           this.ETicketPackage = ETicketPackage;
           this.comment = comment;
    }


    /**
     * Gets the errorCode value for this GetTicketsResponseType.
     * 
     * @return errorCode
     */
    public int getErrorCode() {
        return errorCode;
    }


    /**
     * Sets the errorCode value for this GetTicketsResponseType.
     * 
     * @param errorCode
     */
    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }


    /**
     * Gets the errorMessage value for this GetTicketsResponseType.
     * 
     * @return errorMessage
     */
    public java.lang.String getErrorMessage() {
        return errorMessage;
    }


    /**
     * Sets the errorMessage value for this GetTicketsResponseType.
     * 
     * @param errorMessage
     */
    public void setErrorMessage(java.lang.String errorMessage) {
        this.errorMessage = errorMessage;
    }


    /**
     * Gets the errorTitleId value for this GetTicketsResponseType.
     * 
     * @return errorTitleId In case of error, specify the
 * 										"first" title ID for
 * 										which a ticket cannot be
 * 										generated.
     */
    public java.lang.String getErrorTitleId() {
        return errorTitleId;
    }


    /**
     * Sets the errorTitleId value for this GetTicketsResponseType.
     * 
     * @param errorTitleId In case of error, specify the
 * 										"first" title ID for
 * 										which a ticket cannot be
 * 										generated.
     */
    public void setErrorTitleId(java.lang.String errorTitleId) {
        this.errorTitleId = errorTitleId;
    }


    /**
     * Gets the ETicketPackage value for this GetTicketsResponseType.
     * 
     * @return ETicketPackage
     */
    public com.broadon.wsapi.ets.ETicketPackageType getETicketPackage() {
        return ETicketPackage;
    }


    /**
     * Sets the ETicketPackage value for this GetTicketsResponseType.
     * 
     * @param ETicketPackage
     */
    public void setETicketPackage(com.broadon.wsapi.ets.ETicketPackageType ETicketPackage) {
        this.ETicketPackage = ETicketPackage;
    }


    /**
     * Gets the comment value for this GetTicketsResponseType.
     * 
     * @return comment Optional comment string for debugging and documentation purpose.
     */
    public java.lang.String getComment() {
        return comment;
    }


    /**
     * Sets the comment value for this GetTicketsResponseType.
     * 
     * @param comment Optional comment string for debugging and documentation purpose.
     */
    public void setComment(java.lang.String comment) {
        this.comment = comment;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetTicketsResponseType)) return false;
        GetTicketsResponseType other = (GetTicketsResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.errorCode == other.getErrorCode() &&
            ((this.errorMessage==null && other.getErrorMessage()==null) || 
             (this.errorMessage!=null &&
              this.errorMessage.equals(other.getErrorMessage()))) &&
            ((this.errorTitleId==null && other.getErrorTitleId()==null) || 
             (this.errorTitleId!=null &&
              this.errorTitleId.equals(other.getErrorTitleId()))) &&
            ((this.ETicketPackage==null && other.getETicketPackage()==null) || 
             (this.ETicketPackage!=null &&
              this.ETicketPackage.equals(other.getETicketPackage()))) &&
            ((this.comment==null && other.getComment()==null) || 
             (this.comment!=null &&
              this.comment.equals(other.getComment())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += getErrorCode();
        if (getErrorMessage() != null) {
            _hashCode += getErrorMessage().hashCode();
        }
        if (getErrorTitleId() != null) {
            _hashCode += getErrorTitleId().hashCode();
        }
        if (getETicketPackage() != null) {
            _hashCode += getETicketPackage().hashCode();
        }
        if (getComment() != null) {
            _hashCode += getComment().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetTicketsResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ets.wsapi.broadon.com", "GetTicketsResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorCode");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ets.wsapi.broadon.com", "ErrorCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorMessage");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ets.wsapi.broadon.com", "ErrorMessage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorTitleId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ets.wsapi.broadon.com", "ErrorTitleId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ets.wsapi.broadon.com", "TitleIdType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ETicketPackage");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ets.wsapi.broadon.com", "ETicketPackage"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ets.wsapi.broadon.com", "ETicketPackageType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comment");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ets.wsapi.broadon.com", "Comment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

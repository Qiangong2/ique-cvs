/**
 * PrepaidCardGenerationPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.pcis;

public interface PrepaidCardGenerationPortType extends java.rmi.Remote {
    public com.broadon.wsapi.pcis.PrepaidCardGenerationResponseType prepaidCardGeneration(com.broadon.wsapi.pcis.PrepaidCardGenerationRequestType prepaidCardGenerationRequest) throws java.rmi.RemoteException;
}

/**
 * PrepaidCardGenerationRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.pcis;

public class PrepaidCardGenerationRequestType  extends com.broadon.wsapi.pcis.AbstractRequestType  implements java.io.Serializable {
    private int operatorId;
    private java.lang.String password;
    private int quantity;
    private int ECardType;
    private java.lang.String referenceId1;
    private java.lang.String referenceId2;
    private java.lang.String referenceId3;

    public PrepaidCardGenerationRequestType() {
    }

    public PrepaidCardGenerationRequestType(
           int operatorId,
           java.lang.String password,
           int quantity,
           int ECardType,
           java.lang.String referenceId1,
           java.lang.String referenceId2,
           java.lang.String referenceId3) {
           this.operatorId = operatorId;
           this.password = password;
           this.quantity = quantity;
           this.ECardType = ECardType;
           this.referenceId1 = referenceId1;
           this.referenceId2 = referenceId2;
           this.referenceId3 = referenceId3;
    }


    /**
     * Gets the operatorId value for this PrepaidCardGenerationRequestType.
     * 
     * @return operatorId
     */
    public int getOperatorId() {
        return operatorId;
    }


    /**
     * Sets the operatorId value for this PrepaidCardGenerationRequestType.
     * 
     * @param operatorId
     */
    public void setOperatorId(int operatorId) {
        this.operatorId = operatorId;
    }


    /**
     * Gets the password value for this PrepaidCardGenerationRequestType.
     * 
     * @return password
     */
    public java.lang.String getPassword() {
        return password;
    }


    /**
     * Sets the password value for this PrepaidCardGenerationRequestType.
     * 
     * @param password
     */
    public void setPassword(java.lang.String password) {
        this.password = password;
    }


    /**
     * Gets the quantity value for this PrepaidCardGenerationRequestType.
     * 
     * @return quantity
     */
    public int getQuantity() {
        return quantity;
    }


    /**
     * Sets the quantity value for this PrepaidCardGenerationRequestType.
     * 
     * @param quantity
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }


    /**
     * Gets the ECardType value for this PrepaidCardGenerationRequestType.
     * 
     * @return ECardType
     */
    public int getECardType() {
        return ECardType;
    }


    /**
     * Sets the ECardType value for this PrepaidCardGenerationRequestType.
     * 
     * @param ECardType
     */
    public void setECardType(int ECardType) {
        this.ECardType = ECardType;
    }


    /**
     * Gets the referenceId1 value for this PrepaidCardGenerationRequestType.
     * 
     * @return referenceId1
     */
    public java.lang.String getReferenceId1() {
        return referenceId1;
    }


    /**
     * Sets the referenceId1 value for this PrepaidCardGenerationRequestType.
     * 
     * @param referenceId1
     */
    public void setReferenceId1(java.lang.String referenceId1) {
        this.referenceId1 = referenceId1;
    }


    /**
     * Gets the referenceId2 value for this PrepaidCardGenerationRequestType.
     * 
     * @return referenceId2
     */
    public java.lang.String getReferenceId2() {
        return referenceId2;
    }


    /**
     * Sets the referenceId2 value for this PrepaidCardGenerationRequestType.
     * 
     * @param referenceId2
     */
    public void setReferenceId2(java.lang.String referenceId2) {
        this.referenceId2 = referenceId2;
    }


    /**
     * Gets the referenceId3 value for this PrepaidCardGenerationRequestType.
     * 
     * @return referenceId3
     */
    public java.lang.String getReferenceId3() {
        return referenceId3;
    }


    /**
     * Sets the referenceId3 value for this PrepaidCardGenerationRequestType.
     * 
     * @param referenceId3
     */
    public void setReferenceId3(java.lang.String referenceId3) {
        this.referenceId3 = referenceId3;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PrepaidCardGenerationRequestType)) return false;
        PrepaidCardGenerationRequestType other = (PrepaidCardGenerationRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.operatorId == other.getOperatorId() &&
            ((this.password==null && other.getPassword()==null) || 
             (this.password!=null &&
              this.password.equals(other.getPassword()))) &&
            this.quantity == other.getQuantity() &&
            this.ECardType == other.getECardType() &&
            ((this.referenceId1==null && other.getReferenceId1()==null) || 
             (this.referenceId1!=null &&
              this.referenceId1.equals(other.getReferenceId1()))) &&
            ((this.referenceId2==null && other.getReferenceId2()==null) || 
             (this.referenceId2!=null &&
              this.referenceId2.equals(other.getReferenceId2()))) &&
            ((this.referenceId3==null && other.getReferenceId3()==null) || 
             (this.referenceId3!=null &&
              this.referenceId3.equals(other.getReferenceId3())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += getOperatorId();
        if (getPassword() != null) {
            _hashCode += getPassword().hashCode();
        }
        _hashCode += getQuantity();
        _hashCode += getECardType();
        if (getReferenceId1() != null) {
            _hashCode += getReferenceId1().hashCode();
        }
        if (getReferenceId2() != null) {
            _hashCode += getReferenceId2().hashCode();
        }
        if (getReferenceId3() != null) {
            _hashCode += getReferenceId3().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PrepaidCardGenerationRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:pcis.wsapi.broadon.com", "PrepaidCardGenerationRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("operatorId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pcis.wsapi.broadon.com", "OperatorId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("password");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pcis.wsapi.broadon.com", "Password"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quantity");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pcis.wsapi.broadon.com", "Quantity"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pcis.wsapi.broadon.com", "QuantityType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ECardType");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pcis.wsapi.broadon.com", "ECardType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("referenceId1");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pcis.wsapi.broadon.com", "ReferenceId1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("referenceId2");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pcis.wsapi.broadon.com", "ReferenceId2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("referenceId3");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pcis.wsapi.broadon.com", "ReferenceId3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

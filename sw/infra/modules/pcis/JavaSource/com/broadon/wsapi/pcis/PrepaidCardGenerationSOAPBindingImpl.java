/**
 * PrepaidCardGenerationSOAPBindingImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.pcis;

import com.broadon.pcis.PrepaidCardGenerationServiceImpl;

public class PrepaidCardGenerationSOAPBindingImpl 
        extends PrepaidCardGenerationServiceImpl
        implements com.broadon.wsapi.pcis.PrepaidCardGenerationPortType{
    public com.broadon.wsapi.pcis.PrepaidCardGenerationResponseType prepaidCardGeneration(com.broadon.wsapi.pcis.PrepaidCardGenerationRequestType prepaidCardGenerationRequest) throws java.rmi.RemoteException {
        return super.prepaidCardGeneration(prepaidCardGenerationRequest);
    }

}

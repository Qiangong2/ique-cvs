/**
 * PrepaidCardGenerationResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.pcis;

public class PrepaidCardGenerationResponseType  extends com.broadon.wsapi.pcis.AbstractResponseType  implements java.io.Serializable {
    /** each entry is a prepaid card print string. No entry if error occurs. */
    private java.lang.String[] prepaidCardString;

    public PrepaidCardGenerationResponseType() {
    }

    public PrepaidCardGenerationResponseType(
           java.lang.String[] prepaidCardString) {
           this.prepaidCardString = prepaidCardString;
    }


    /**
     * Gets the prepaidCardString value for this PrepaidCardGenerationResponseType.
     * 
     * @return prepaidCardString each entry is a prepaid card print string. No entry if error occurs.
     */
    public java.lang.String[] getPrepaidCardString() {
        return prepaidCardString;
    }


    /**
     * Sets the prepaidCardString value for this PrepaidCardGenerationResponseType.
     * 
     * @param prepaidCardString each entry is a prepaid card print string. No entry if error occurs.
     */
    public void setPrepaidCardString(java.lang.String[] prepaidCardString) {
        this.prepaidCardString = prepaidCardString;
    }

    public java.lang.String getPrepaidCardString(int i) {
        return this.prepaidCardString[i];
    }

    public void setPrepaidCardString(int i, java.lang.String _value) {
        this.prepaidCardString[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PrepaidCardGenerationResponseType)) return false;
        PrepaidCardGenerationResponseType other = (PrepaidCardGenerationResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.prepaidCardString==null && other.getPrepaidCardString()==null) || 
             (this.prepaidCardString!=null &&
              java.util.Arrays.equals(this.prepaidCardString, other.getPrepaidCardString())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getPrepaidCardString() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPrepaidCardString());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPrepaidCardString(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PrepaidCardGenerationResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:pcis.wsapi.broadon.com", "PrepaidCardGenerationResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prepaidCardString");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pcis.wsapi.broadon.com", "PrepaidCardString"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

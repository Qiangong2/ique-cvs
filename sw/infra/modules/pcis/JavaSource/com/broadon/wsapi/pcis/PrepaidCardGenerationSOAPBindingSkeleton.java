/**
 * PrepaidCardGenerationSOAPBindingSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.pcis;

import javax.xml.rpc.ServiceException;
import javax.xml.rpc.server.ServiceLifecycle;

public class PrepaidCardGenerationSOAPBindingSkeleton implements ServiceLifecycle,
        com.broadon.wsapi.pcis.PrepaidCardGenerationPortType, org.apache.axis.wsdl.Skeleton {
    private com.broadon.wsapi.pcis.PrepaidCardGenerationPortType impl;
    private static java.util.Map _myOperations = new java.util.Hashtable();
    private static java.util.Collection _myOperationsList = new java.util.ArrayList();

    /**
    * Returns List of OperationDesc objects with this name
    */
    public static java.util.List getOperationDescByName(java.lang.String methodName) {
        return (java.util.List)_myOperations.get(methodName);
    }

    /**
    * Returns Collection of OperationDescs
    */
    public static java.util.Collection getOperationDescs() {
        return _myOperationsList;
    }

    static {
        org.apache.axis.description.OperationDesc _oper;
        org.apache.axis.description.FaultDesc _fault;
        org.apache.axis.description.ParameterDesc [] _params;
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pcis.wsapi.broadon.com", "PrepaidCardGeneration"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pcis.wsapi.broadon.com", "PrepaidCardGenerationRequestType"), com.broadon.wsapi.pcis.PrepaidCardGenerationRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("prepaidCardGeneration", _params, new javax.xml.namespace.QName("urn:pcis.wsapi.broadon.com", "PrepaidCardGenerationResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:pcis.wsapi.broadon.com", "PrepaidCardGenerationResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "PrepaidCardGeneration"));
        _oper.setSoapAction("urn:wsapi.pcis.broadon.com/PrepaidCardGeneration");
        _myOperationsList.add(_oper);
        if (_myOperations.get("prepaidCardGeneration") == null) {
            _myOperations.put("prepaidCardGeneration", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("prepaidCardGeneration")).add(_oper);
    }

    public PrepaidCardGenerationSOAPBindingSkeleton() {
        this.impl = new com.broadon.wsapi.pcis.PrepaidCardGenerationSOAPBindingImpl();
    }

    public PrepaidCardGenerationSOAPBindingSkeleton(com.broadon.wsapi.pcis.PrepaidCardGenerationPortType impl) {
        this.impl = impl;
    }
    public com.broadon.wsapi.pcis.PrepaidCardGenerationResponseType prepaidCardGeneration(com.broadon.wsapi.pcis.PrepaidCardGenerationRequestType prepaidCardGenerationRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.pcis.PrepaidCardGenerationResponseType ret = impl.prepaidCardGeneration(prepaidCardGenerationRequest);
        return ret;
    }
    public void init(Object context) throws ServiceException    
    {   
        if (impl instanceof ServiceLifecycle) {  
            ((ServiceLifecycle) impl).init(context);  
        }  
    }  
    public void destroy()  
    {   
        if (impl instanceof ServiceLifecycle) {  
            ((ServiceLifecycle) impl).destroy();  
        }  
    }
}

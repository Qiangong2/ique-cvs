/**
 * AccountElementTagsType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ias;

public class AccountElementTagsType  implements java.io.Serializable {
    private java.lang.Boolean loginName;
    private java.lang.Boolean parentAccountId;
    private java.lang.Boolean nickName;
    private java.lang.Boolean emailAddress;
    private java.lang.Boolean fullName;
    private java.lang.Boolean birthDate;

    public AccountElementTagsType() {
    }

    public AccountElementTagsType(
           java.lang.Boolean loginName,
           java.lang.Boolean parentAccountId,
           java.lang.Boolean nickName,
           java.lang.Boolean emailAddress,
           java.lang.Boolean fullName,
           java.lang.Boolean birthDate) {
           this.loginName = loginName;
           this.parentAccountId = parentAccountId;
           this.nickName = nickName;
           this.emailAddress = emailAddress;
           this.fullName = fullName;
           this.birthDate = birthDate;
    }


    /**
     * Gets the loginName value for this AccountElementTagsType.
     * 
     * @return loginName
     */
    public java.lang.Boolean getLoginName() {
        return loginName;
    }


    /**
     * Sets the loginName value for this AccountElementTagsType.
     * 
     * @param loginName
     */
    public void setLoginName(java.lang.Boolean loginName) {
        this.loginName = loginName;
    }


    /**
     * Gets the parentAccountId value for this AccountElementTagsType.
     * 
     * @return parentAccountId
     */
    public java.lang.Boolean getParentAccountId() {
        return parentAccountId;
    }


    /**
     * Sets the parentAccountId value for this AccountElementTagsType.
     * 
     * @param parentAccountId
     */
    public void setParentAccountId(java.lang.Boolean parentAccountId) {
        this.parentAccountId = parentAccountId;
    }


    /**
     * Gets the nickName value for this AccountElementTagsType.
     * 
     * @return nickName
     */
    public java.lang.Boolean getNickName() {
        return nickName;
    }


    /**
     * Sets the nickName value for this AccountElementTagsType.
     * 
     * @param nickName
     */
    public void setNickName(java.lang.Boolean nickName) {
        this.nickName = nickName;
    }


    /**
     * Gets the emailAddress value for this AccountElementTagsType.
     * 
     * @return emailAddress
     */
    public java.lang.Boolean getEmailAddress() {
        return emailAddress;
    }


    /**
     * Sets the emailAddress value for this AccountElementTagsType.
     * 
     * @param emailAddress
     */
    public void setEmailAddress(java.lang.Boolean emailAddress) {
        this.emailAddress = emailAddress;
    }


    /**
     * Gets the fullName value for this AccountElementTagsType.
     * 
     * @return fullName
     */
    public java.lang.Boolean getFullName() {
        return fullName;
    }


    /**
     * Sets the fullName value for this AccountElementTagsType.
     * 
     * @param fullName
     */
    public void setFullName(java.lang.Boolean fullName) {
        this.fullName = fullName;
    }


    /**
     * Gets the birthDate value for this AccountElementTagsType.
     * 
     * @return birthDate
     */
    public java.lang.Boolean getBirthDate() {
        return birthDate;
    }


    /**
     * Sets the birthDate value for this AccountElementTagsType.
     * 
     * @param birthDate
     */
    public void setBirthDate(java.lang.Boolean birthDate) {
        this.birthDate = birthDate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AccountElementTagsType)) return false;
        AccountElementTagsType other = (AccountElementTagsType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.loginName==null && other.getLoginName()==null) || 
             (this.loginName!=null &&
              this.loginName.equals(other.getLoginName()))) &&
            ((this.parentAccountId==null && other.getParentAccountId()==null) || 
             (this.parentAccountId!=null &&
              this.parentAccountId.equals(other.getParentAccountId()))) &&
            ((this.nickName==null && other.getNickName()==null) || 
             (this.nickName!=null &&
              this.nickName.equals(other.getNickName()))) &&
            ((this.emailAddress==null && other.getEmailAddress()==null) || 
             (this.emailAddress!=null &&
              this.emailAddress.equals(other.getEmailAddress()))) &&
            ((this.fullName==null && other.getFullName()==null) || 
             (this.fullName!=null &&
              this.fullName.equals(other.getFullName()))) &&
            ((this.birthDate==null && other.getBirthDate()==null) || 
             (this.birthDate!=null &&
              this.birthDate.equals(other.getBirthDate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLoginName() != null) {
            _hashCode += getLoginName().hashCode();
        }
        if (getParentAccountId() != null) {
            _hashCode += getParentAccountId().hashCode();
        }
        if (getNickName() != null) {
            _hashCode += getNickName().hashCode();
        }
        if (getEmailAddress() != null) {
            _hashCode += getEmailAddress().hashCode();
        }
        if (getFullName() != null) {
            _hashCode += getFullName().hashCode();
        }
        if (getBirthDate() != null) {
            _hashCode += getBirthDate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AccountElementTagsType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AccountElementTagsType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loginName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "LoginName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parentAccountId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ParentAccountId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nickName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "NickName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("emailAddress");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "EmailAddress"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fullName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "FullName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("birthDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "BirthDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

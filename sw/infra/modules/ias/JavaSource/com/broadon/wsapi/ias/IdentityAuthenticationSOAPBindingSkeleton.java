/**
 * IdentityAuthenticationSOAPBindingSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ias;

import javax.xml.rpc.ServiceException;  
import javax.xml.rpc.server.ServiceLifecycle;  
 
public class IdentityAuthenticationSOAPBindingSkeleton implements ServiceLifecycle,  
    com.broadon.wsapi.ias.IdentityAuthenticationPortType, org.apache.axis.wsdl.Skeleton  
{
    private com.broadon.wsapi.ias.IdentityAuthenticationPortType impl;
    private static java.util.Map _myOperations = new java.util.Hashtable();
    private static java.util.Collection _myOperationsList = new java.util.ArrayList();

    /**
    * Returns List of OperationDesc objects with this name
    */
    public static java.util.List getOperationDescByName(java.lang.String methodName) {
        return (java.util.List)_myOperations.get(methodName);
    }

    /**
    * Returns Collection of OperationDescs
    */
    public static java.util.Collection getOperationDescs() {
        return _myOperationsList;
    }

    static {
        org.apache.axis.description.OperationDesc _oper;
        org.apache.axis.description.FaultDesc _fault;
        org.apache.axis.description.ParameterDesc [] _params;
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AuthenticateAccount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AuthenticateAccountRequestType"), com.broadon.wsapi.ias.AuthenticateAccountRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("authenticateAccount", _params, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AuthenticateAccountResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AuthenticateAccountResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "AuthenticateAccount"));
        _oper.setSoapAction("urn:ias.wsapi.broadon.com/AuthenticateAccount");
        _myOperationsList.add(_oper);
        if (_myOperations.get("authenticateAccount") == null) {
            _myOperations.put("authenticateAccount", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("authenticateAccount")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AuthenticateDevice"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AuthenticateDeviceRequestType"), com.broadon.wsapi.ias.AuthenticateDeviceRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("authenticateDevice", _params, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AuthenticateDeviceResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AuthenticateDeviceResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "AuthenticateDevice"));
        _oper.setSoapAction("urn:ias.wsapi.broadon.com/AuthenticateDevice");
        _myOperationsList.add(_oper);
        if (_myOperations.get("authenticateDevice") == null) {
            _myOperations.put("authenticateDevice", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("authenticateDevice")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ExportAccount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ExportAccountRequestType"), com.broadon.wsapi.ias.ExportAccountRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("exportAccount", _params, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ExportAccountResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ExportAccountResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "ExportAccount"));
        _oper.setSoapAction("urn:ias.wsapi.broadon.com/ExportAccount");
        _myOperationsList.add(_oper);
        if (_myOperations.get("exportAccount") == null) {
            _myOperations.put("exportAccount", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("exportAccount")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetAccountAttributes"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetAccountAttributesRequestType"), com.broadon.wsapi.ias.GetAccountAttributesRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getAccountAttributes", _params, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetAccountAttributesResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetAccountAttributesResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "GetAccountAttributes"));
        _oper.setSoapAction("urn:ias.wsapi.broadon.com/GetAccountAttributes");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getAccountAttributes") == null) {
            _myOperations.put("getAccountAttributes", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getAccountAttributes")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetAccountInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetAccountInfoRequestType"), com.broadon.wsapi.ias.GetAccountInfoRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getAccountInfo", _params, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetAccountInfoResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetAccountInfoResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "GetAccountInfo"));
        _oper.setSoapAction("urn:ias.wsapi.broadon.com/GetAccountInfo");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getAccountInfo") == null) {
            _myOperations.put("getAccountInfo", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getAccountInfo")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetDeviceAccounts"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetDeviceAccountsRequestType"), com.broadon.wsapi.ias.GetDeviceAccountsRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getDeviceAccounts", _params, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetDeviceAccountsResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetDeviceAccountsResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "GetDeviceAccounts"));
        _oper.setSoapAction("urn:ias.wsapi.broadon.com/GetDeviceAccounts");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getDeviceAccounts") == null) {
            _myOperations.put("getDeviceAccounts", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getDeviceAccounts")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetDeviceInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetDeviceInfoRequestType"), com.broadon.wsapi.ias.GetDeviceInfoRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getDeviceInfo", _params, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetDeviceInfoResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetDeviceInfoResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "GetDeviceInfo"));
        _oper.setSoapAction("urn:ias.wsapi.broadon.com/GetDeviceInfo");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getDeviceInfo") == null) {
            _myOperations.put("getDeviceInfo", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getDeviceInfo")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetDeviceSubscriptions"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetDeviceSubscriptionsRequestType"), com.broadon.wsapi.ias.GetDeviceSubscriptionsRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getDeviceSubscriptions", _params, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetDeviceSubscriptionsResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetDeviceSubscriptionsResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "GetDeviceSubscriptions"));
        _oper.setSoapAction("urn:ias.wsapi.broadon.com/GetDeviceSubscriptions");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getDeviceSubscriptions") == null) {
            _myOperations.put("getDeviceSubscriptions", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getDeviceSubscriptions")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "LinkDeviceAccount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "LinkDeviceAccountRequestType"), com.broadon.wsapi.ias.LinkDeviceAccountRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("linkDeviceAccount", _params, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "LinkDeviceAccountResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "LinkDeviceAccountResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "LinkDeviceAccount"));
        _oper.setSoapAction("urn:ias.wsapi.broadon.com/LinkDeviceAccount");
        _myOperationsList.add(_oper);
        if (_myOperations.get("linkDeviceAccount") == null) {
            _myOperations.put("linkDeviceAccount", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("linkDeviceAccount")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RegisterAccount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RegisterAccountRequestType"), com.broadon.wsapi.ias.RegisterAccountRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("registerAccount", _params, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RegisterAccountResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RegisterAccountResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "RegisterAccount"));
        _oper.setSoapAction("urn:ias.wsapi.broadon.com/RegisterAccount");
        _myOperationsList.add(_oper);
        if (_myOperations.get("registerAccount") == null) {
            _myOperations.put("registerAccount", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("registerAccount")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RegisterDevice"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RegisterDeviceRequestType"), com.broadon.wsapi.ias.RegisterDeviceRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("registerDevice", _params, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RegisterDeviceResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RegisterDeviceResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "RegisterDevice"));
        _oper.setSoapAction("urn:ias.wsapi.broadon.com/RegisterDevice");
        _myOperationsList.add(_oper);
        if (_myOperations.get("registerDevice") == null) {
            _myOperations.put("registerDevice", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("registerDevice")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RemoveDeviceAccount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RemoveDeviceAccountRequestType"), com.broadon.wsapi.ias.RemoveDeviceAccountRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("removeDeviceAccount", _params, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RemoveDeviceAccountResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RemoveDeviceAccountResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "RemoveDeviceAccount"));
        _oper.setSoapAction("urn:ias.wsapi.broadon.com/RemoveDeviceAccount");
        _myOperationsList.add(_oper);
        if (_myOperations.get("removeDeviceAccount") == null) {
            _myOperations.put("removeDeviceAccount", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("removeDeviceAccount")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RenewAccountToken"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RenewAccountTokenRequestType"), com.broadon.wsapi.ias.RenewAccountTokenRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("renewAccountToken", _params, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RenewAccountTokenResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RenewAccountTokenResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "RenewAccountToken"));
        _oper.setSoapAction("urn:ias.wsapi.broadon.com/RenewAccountToken");
        _myOperationsList.add(_oper);
        if (_myOperations.get("renewAccountToken") == null) {
            _myOperations.put("renewAccountToken", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("renewAccountToken")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RenewDeviceToken"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RenewDeviceTokenRequestType"), com.broadon.wsapi.ias.RenewDeviceTokenRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("renewDeviceToken", _params, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RenewDeviceTokenResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RenewDeviceTokenResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "RenewDeviceToken"));
        _oper.setSoapAction("urn:ias.wsapi.broadon.com/RenewDeviceToken");
        _myOperationsList.add(_oper);
        if (_myOperations.get("renewDeviceToken") == null) {
            _myOperations.put("renewDeviceToken", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("renewDeviceToken")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ResetAccountPassword"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ResetAccountPasswordRequestType"), com.broadon.wsapi.ias.ResetAccountPasswordRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("resetAccountPassword", _params, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ResetAccountPasswordResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ResetAccountPasswordResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "ResetAccountPassword"));
        _oper.setSoapAction("urn:ias.wsapi.broadon.com/ResetAccountPassword");
        _myOperationsList.add(_oper);
        if (_myOperations.get("resetAccountPassword") == null) {
            _myOperations.put("resetAccountPassword", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("resetAccountPassword")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateAccount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateAccountRequestType"), com.broadon.wsapi.ias.UpdateAccountRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("updateAccount", _params, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateAccountResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateAccountResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "UpdateAccount"));
        _oper.setSoapAction("urn:ias.wsapi.broadon.com/UpdateAccount");
        _myOperationsList.add(_oper);
        if (_myOperations.get("updateAccount") == null) {
            _myOperations.put("updateAccount", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("updateAccount")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateAccountAttributes"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateAccountAttributesRequestType"), com.broadon.wsapi.ias.UpdateAccountAttributesRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("updateAccountAttributes", _params, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateAccountAttributesResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateAccountAttributesResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "UpdateAccountAttributes"));
        _oper.setSoapAction("urn:ias.wsapi.broadon.com/UpdateAccountAttributes");
        _myOperationsList.add(_oper);
        if (_myOperations.get("updateAccountAttributes") == null) {
            _myOperations.put("updateAccountAttributes", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("updateAccountAttributes")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateAccountPassword"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateAccountPasswordRequestType"), com.broadon.wsapi.ias.UpdateAccountPasswordRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("updateAccountPassword", _params, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateAccountPasswordResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateAccountPasswordResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "UpdateAccountPassword"));
        _oper.setSoapAction("urn:ias.wsapi.broadon.com/UpdateAccountPassword");
        _myOperationsList.add(_oper);
        if (_myOperations.get("updateAccountPassword") == null) {
            _myOperations.put("updateAccountPassword", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("updateAccountPassword")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateDeviceSubscription"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateDeviceSubscriptionRequestType"), com.broadon.wsapi.ias.UpdateDeviceSubscriptionRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("updateDeviceSubscription", _params, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateDeviceSubscriptionResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateDeviceSubscriptionResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "UpdateDeviceSubscription"));
        _oper.setSoapAction("urn:ias.wsapi.broadon.com/UpdateDeviceSubscription");
        _myOperationsList.add(_oper);
        if (_myOperations.get("updateDeviceSubscription") == null) {
            _myOperations.put("updateDeviceSubscription", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("updateDeviceSubscription")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ValidateAccountToken"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ValidateAccountTokenRequestType"), com.broadon.wsapi.ias.ValidateAccountTokenRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("validateAccountToken", _params, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ValidateAccountTokenResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ValidateAccountTokenResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "ValidateAccountToken"));
        _oper.setSoapAction("urn:ias.wsapi.broadon.com/ValidateAccountToken");
        _myOperationsList.add(_oper);
        if (_myOperations.get("validateAccountToken") == null) {
            _myOperations.put("validateAccountToken", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("validateAccountToken")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ValidateDeviceToken"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ValidateDeviceTokenRequestType"), com.broadon.wsapi.ias.ValidateDeviceTokenRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("validateDeviceToken", _params, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ValidateDeviceTokenResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ValidateDeviceTokenResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "ValidateDeviceToken"));
        _oper.setSoapAction("urn:ias.wsapi.broadon.com/ValidateDeviceToken");
        _myOperationsList.add(_oper);
        if (_myOperations.get("validateDeviceToken") == null) {
            _myOperations.put("validateDeviceToken", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("validateDeviceToken")).add(_oper);
    }

    public IdentityAuthenticationSOAPBindingSkeleton() {
        this.impl = new com.broadon.wsapi.ias.IdentityAuthenticationSOAPBindingImpl();
    }

    public IdentityAuthenticationSOAPBindingSkeleton(com.broadon.wsapi.ias.IdentityAuthenticationPortType impl) {
        this.impl = impl;
    }
    public com.broadon.wsapi.ias.AuthenticateAccountResponseType authenticateAccount(com.broadon.wsapi.ias.AuthenticateAccountRequestType authenticateAccountRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ias.AuthenticateAccountResponseType ret = impl.authenticateAccount(authenticateAccountRequest);
        return ret;
    }

    public com.broadon.wsapi.ias.AuthenticateDeviceResponseType authenticateDevice(com.broadon.wsapi.ias.AuthenticateDeviceRequestType authenticateDeviceRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ias.AuthenticateDeviceResponseType ret = impl.authenticateDevice(authenticateDeviceRequest);
        return ret;
    }

    public com.broadon.wsapi.ias.ExportAccountResponseType exportAccount(com.broadon.wsapi.ias.ExportAccountRequestType exportAccountRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ias.ExportAccountResponseType ret = impl.exportAccount(exportAccountRequest);
        return ret;
    }

    public com.broadon.wsapi.ias.GetAccountAttributesResponseType getAccountAttributes(com.broadon.wsapi.ias.GetAccountAttributesRequestType getAccountAttributesRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ias.GetAccountAttributesResponseType ret = impl.getAccountAttributes(getAccountAttributesRequest);
        return ret;
    }

    public com.broadon.wsapi.ias.GetAccountInfoResponseType getAccountInfo(com.broadon.wsapi.ias.GetAccountInfoRequestType getAccountInfoRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ias.GetAccountInfoResponseType ret = impl.getAccountInfo(getAccountInfoRequest);
        return ret;
    }

    public com.broadon.wsapi.ias.GetDeviceAccountsResponseType getDeviceAccounts(com.broadon.wsapi.ias.GetDeviceAccountsRequestType getDeviceAccountsRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ias.GetDeviceAccountsResponseType ret = impl.getDeviceAccounts(getDeviceAccountsRequest);
        return ret;
    }

    public com.broadon.wsapi.ias.GetDeviceInfoResponseType getDeviceInfo(com.broadon.wsapi.ias.GetDeviceInfoRequestType getDeviceInfoRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ias.GetDeviceInfoResponseType ret = impl.getDeviceInfo(getDeviceInfoRequest);
        return ret;
    }

    public com.broadon.wsapi.ias.GetDeviceSubscriptionsResponseType getDeviceSubscriptions(com.broadon.wsapi.ias.GetDeviceSubscriptionsRequestType getDeviceSubscriptionsRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ias.GetDeviceSubscriptionsResponseType ret = impl.getDeviceSubscriptions(getDeviceSubscriptionsRequest);
        return ret;
    }

    public com.broadon.wsapi.ias.LinkDeviceAccountResponseType linkDeviceAccount(com.broadon.wsapi.ias.LinkDeviceAccountRequestType linkDeviceAccountRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ias.LinkDeviceAccountResponseType ret = impl.linkDeviceAccount(linkDeviceAccountRequest);
        return ret;
    }

    public com.broadon.wsapi.ias.RegisterAccountResponseType registerAccount(com.broadon.wsapi.ias.RegisterAccountRequestType registerAccountRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ias.RegisterAccountResponseType ret = impl.registerAccount(registerAccountRequest);
        return ret;
    }

    public com.broadon.wsapi.ias.RegisterDeviceResponseType registerDevice(com.broadon.wsapi.ias.RegisterDeviceRequestType registerDeviceRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ias.RegisterDeviceResponseType ret = impl.registerDevice(registerDeviceRequest);
        return ret;
    }

    public com.broadon.wsapi.ias.RemoveDeviceAccountResponseType removeDeviceAccount(com.broadon.wsapi.ias.RemoveDeviceAccountRequestType removeDeviceAccountRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ias.RemoveDeviceAccountResponseType ret = impl.removeDeviceAccount(removeDeviceAccountRequest);
        return ret;
    }

    public com.broadon.wsapi.ias.RenewAccountTokenResponseType renewAccountToken(com.broadon.wsapi.ias.RenewAccountTokenRequestType renewAccountTokenRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ias.RenewAccountTokenResponseType ret = impl.renewAccountToken(renewAccountTokenRequest);
        return ret;
    }

    public com.broadon.wsapi.ias.RenewDeviceTokenResponseType renewDeviceToken(com.broadon.wsapi.ias.RenewDeviceTokenRequestType renewDeviceTokenRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ias.RenewDeviceTokenResponseType ret = impl.renewDeviceToken(renewDeviceTokenRequest);
        return ret;
    }

    public com.broadon.wsapi.ias.ResetAccountPasswordResponseType resetAccountPassword(com.broadon.wsapi.ias.ResetAccountPasswordRequestType resetAccountPasswordRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ias.ResetAccountPasswordResponseType ret = impl.resetAccountPassword(resetAccountPasswordRequest);
        return ret;
    }

    public com.broadon.wsapi.ias.UpdateAccountResponseType updateAccount(com.broadon.wsapi.ias.UpdateAccountRequestType updateAccountRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ias.UpdateAccountResponseType ret = impl.updateAccount(updateAccountRequest);
        return ret;
    }

    public com.broadon.wsapi.ias.UpdateAccountAttributesResponseType updateAccountAttributes(com.broadon.wsapi.ias.UpdateAccountAttributesRequestType updateAccountAttributesRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ias.UpdateAccountAttributesResponseType ret = impl.updateAccountAttributes(updateAccountAttributesRequest);
        return ret;
    }

    public com.broadon.wsapi.ias.UpdateAccountPasswordResponseType updateAccountPassword(com.broadon.wsapi.ias.UpdateAccountPasswordRequestType updateAccountPasswordRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ias.UpdateAccountPasswordResponseType ret = impl.updateAccountPassword(updateAccountPasswordRequest);
        return ret;
    }

    public com.broadon.wsapi.ias.UpdateDeviceSubscriptionResponseType updateDeviceSubscription(com.broadon.wsapi.ias.UpdateDeviceSubscriptionRequestType updateDeviceSubscriptionRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ias.UpdateDeviceSubscriptionResponseType ret = impl.updateDeviceSubscription(updateDeviceSubscriptionRequest);
        return ret;
    }

    public com.broadon.wsapi.ias.ValidateAccountTokenResponseType validateAccountToken(com.broadon.wsapi.ias.ValidateAccountTokenRequestType validateAccountTokenRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ias.ValidateAccountTokenResponseType ret = impl.validateAccountToken(validateAccountTokenRequest);
        return ret;
    }

    public com.broadon.wsapi.ias.ValidateDeviceTokenResponseType validateDeviceToken(com.broadon.wsapi.ias.ValidateDeviceTokenRequestType validateDeviceTokenRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.ias.ValidateDeviceTokenResponseType ret = impl.validateDeviceToken(validateDeviceTokenRequest);
        return ret;
    }
    
    public void init(Object context) throws ServiceException    
    {   
        if (impl instanceof ServiceLifecycle) {  
            ((ServiceLifecycle) impl).init(context);  
        }  
    }  
   
    public void destroy()  
    {   
        if (impl instanceof ServiceLifecycle) {  
            ((ServiceLifecycle) impl).destroy();  
        }  
    }
}

/**
 * DeviceAccountType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ias;

public class DeviceAccountType  implements java.io.Serializable {
    private int accountId;
    private long registerDate;
    private java.lang.Long removeDate;

    public DeviceAccountType() {
    }

    public DeviceAccountType(
           int accountId,
           long registerDate,
           java.lang.Long removeDate) {
           this.accountId = accountId;
           this.registerDate = registerDate;
           this.removeDate = removeDate;
    }


    /**
     * Gets the accountId value for this DeviceAccountType.
     * 
     * @return accountId
     */
    public int getAccountId() {
        return accountId;
    }


    /**
     * Sets the accountId value for this DeviceAccountType.
     * 
     * @param accountId
     */
    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }


    /**
     * Gets the registerDate value for this DeviceAccountType.
     * 
     * @return registerDate
     */
    public long getRegisterDate() {
        return registerDate;
    }


    /**
     * Sets the registerDate value for this DeviceAccountType.
     * 
     * @param registerDate
     */
    public void setRegisterDate(long registerDate) {
        this.registerDate = registerDate;
    }


    /**
     * Gets the removeDate value for this DeviceAccountType.
     * 
     * @return removeDate
     */
    public java.lang.Long getRemoveDate() {
        return removeDate;
    }


    /**
     * Sets the removeDate value for this DeviceAccountType.
     * 
     * @param removeDate
     */
    public void setRemoveDate(java.lang.Long removeDate) {
        this.removeDate = removeDate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DeviceAccountType)) return false;
        DeviceAccountType other = (DeviceAccountType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.accountId == other.getAccountId() &&
            this.registerDate == other.getRegisterDate() &&
            ((this.removeDate==null && other.getRemoveDate()==null) || 
             (this.removeDate!=null &&
              this.removeDate.equals(other.getRemoveDate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getAccountId();
        _hashCode += new Long(getRegisterDate()).hashCode();
        if (getRemoveDate() != null) {
            _hashCode += getRemoveDate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DeviceAccountType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "DeviceAccountType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AccountId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AccountIdType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("registerDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RegisterDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "TimeStampType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("removeDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RemoveDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "TimeStampType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

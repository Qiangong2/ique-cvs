/**
 * RegisterDeviceRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ias;

public class RegisterDeviceRequestType  extends com.broadon.wsapi.ias.AbstractDeviceRequestType  implements java.io.Serializable {
    private byte[] deviceCert;
    private java.lang.String serialNumber;
    private byte[] configCode;

    public RegisterDeviceRequestType() {
    }

    public RegisterDeviceRequestType(
           byte[] deviceCert,
           java.lang.String serialNumber,
           byte[] configCode) {
           this.deviceCert = deviceCert;
           this.serialNumber = serialNumber;
           this.configCode = configCode;
    }


    /**
     * Gets the deviceCert value for this RegisterDeviceRequestType.
     * 
     * @return deviceCert
     */
    public byte[] getDeviceCert() {
        return deviceCert;
    }


    /**
     * Sets the deviceCert value for this RegisterDeviceRequestType.
     * 
     * @param deviceCert
     */
    public void setDeviceCert(byte[] deviceCert) {
        this.deviceCert = deviceCert;
    }


    /**
     * Gets the serialNumber value for this RegisterDeviceRequestType.
     * 
     * @return serialNumber
     */
    public java.lang.String getSerialNumber() {
        return serialNumber;
    }


    /**
     * Sets the serialNumber value for this RegisterDeviceRequestType.
     * 
     * @param serialNumber
     */
    public void setSerialNumber(java.lang.String serialNumber) {
        this.serialNumber = serialNumber;
    }


    /**
     * Gets the configCode value for this RegisterDeviceRequestType.
     * 
     * @return configCode
     */
    public byte[] getConfigCode() {
        return configCode;
    }


    /**
     * Sets the configCode value for this RegisterDeviceRequestType.
     * 
     * @param configCode
     */
    public void setConfigCode(byte[] configCode) {
        this.configCode = configCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RegisterDeviceRequestType)) return false;
        RegisterDeviceRequestType other = (RegisterDeviceRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.deviceCert==null && other.getDeviceCert()==null) || 
             (this.deviceCert!=null &&
              java.util.Arrays.equals(this.deviceCert, other.getDeviceCert()))) &&
            ((this.serialNumber==null && other.getSerialNumber()==null) || 
             (this.serialNumber!=null &&
              this.serialNumber.equals(other.getSerialNumber()))) &&
            ((this.configCode==null && other.getConfigCode()==null) || 
             (this.configCode!=null &&
              java.util.Arrays.equals(this.configCode, other.getConfigCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getDeviceCert() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDeviceCert());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDeviceCert(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSerialNumber() != null) {
            _hashCode += getSerialNumber().hashCode();
        }
        if (getConfigCode() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getConfigCode());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getConfigCode(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RegisterDeviceRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RegisterDeviceRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deviceCert");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "DeviceCert"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serialNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "SerialNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("configCode");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ConfigCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

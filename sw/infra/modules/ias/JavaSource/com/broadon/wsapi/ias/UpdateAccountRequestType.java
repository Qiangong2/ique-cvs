/**
 * UpdateAccountRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ias;

public class UpdateAccountRequestType  extends com.broadon.wsapi.ias.AbstractAccountRequestType  implements java.io.Serializable {
    private com.broadon.wsapi.ias.AccountInfoType accountInfo;
    private com.broadon.wsapi.ias.AccountElementTagsType unsetElements;

    public UpdateAccountRequestType() {
    }

    public UpdateAccountRequestType(
           com.broadon.wsapi.ias.AccountInfoType accountInfo,
           com.broadon.wsapi.ias.AccountElementTagsType unsetElements) {
           this.accountInfo = accountInfo;
           this.unsetElements = unsetElements;
    }


    /**
     * Gets the accountInfo value for this UpdateAccountRequestType.
     * 
     * @return accountInfo
     */
    public com.broadon.wsapi.ias.AccountInfoType getAccountInfo() {
        return accountInfo;
    }


    /**
     * Sets the accountInfo value for this UpdateAccountRequestType.
     * 
     * @param accountInfo
     */
    public void setAccountInfo(com.broadon.wsapi.ias.AccountInfoType accountInfo) {
        this.accountInfo = accountInfo;
    }


    /**
     * Gets the unsetElements value for this UpdateAccountRequestType.
     * 
     * @return unsetElements
     */
    public com.broadon.wsapi.ias.AccountElementTagsType getUnsetElements() {
        return unsetElements;
    }


    /**
     * Sets the unsetElements value for this UpdateAccountRequestType.
     * 
     * @param unsetElements
     */
    public void setUnsetElements(com.broadon.wsapi.ias.AccountElementTagsType unsetElements) {
        this.unsetElements = unsetElements;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpdateAccountRequestType)) return false;
        UpdateAccountRequestType other = (UpdateAccountRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.accountInfo==null && other.getAccountInfo()==null) || 
             (this.accountInfo!=null &&
              this.accountInfo.equals(other.getAccountInfo()))) &&
            ((this.unsetElements==null && other.getUnsetElements()==null) || 
             (this.unsetElements!=null &&
              this.unsetElements.equals(other.getUnsetElements())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAccountInfo() != null) {
            _hashCode += getAccountInfo().hashCode();
        }
        if (getUnsetElements() != null) {
            _hashCode += getUnsetElements().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UpdateAccountRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateAccountRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AccountInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AccountInfoType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("unsetElements");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UnsetElements"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AccountElementTagsType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

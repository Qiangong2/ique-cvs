/**
 * GetDeviceAccountsResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ias;

public class GetDeviceAccountsResponseType  extends com.broadon.wsapi.ias.AbstractDeviceResponseType  implements java.io.Serializable {
    private com.broadon.wsapi.ias.DeviceAccountType[] deviceAccounts;

    public GetDeviceAccountsResponseType() {
    }

    public GetDeviceAccountsResponseType(
           com.broadon.wsapi.ias.DeviceAccountType[] deviceAccounts) {
           this.deviceAccounts = deviceAccounts;
    }


    /**
     * Gets the deviceAccounts value for this GetDeviceAccountsResponseType.
     * 
     * @return deviceAccounts
     */
    public com.broadon.wsapi.ias.DeviceAccountType[] getDeviceAccounts() {
        return deviceAccounts;
    }


    /**
     * Sets the deviceAccounts value for this GetDeviceAccountsResponseType.
     * 
     * @param deviceAccounts
     */
    public void setDeviceAccounts(com.broadon.wsapi.ias.DeviceAccountType[] deviceAccounts) {
        this.deviceAccounts = deviceAccounts;
    }

    public com.broadon.wsapi.ias.DeviceAccountType getDeviceAccounts(int i) {
        return this.deviceAccounts[i];
    }

    public void setDeviceAccounts(int i, com.broadon.wsapi.ias.DeviceAccountType _value) {
        this.deviceAccounts[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetDeviceAccountsResponseType)) return false;
        GetDeviceAccountsResponseType other = (GetDeviceAccountsResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.deviceAccounts==null && other.getDeviceAccounts()==null) || 
             (this.deviceAccounts!=null &&
              java.util.Arrays.equals(this.deviceAccounts, other.getDeviceAccounts())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getDeviceAccounts() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDeviceAccounts());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDeviceAccounts(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetDeviceAccountsResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetDeviceAccountsResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deviceAccounts");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "DeviceAccounts"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "DeviceAccountType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

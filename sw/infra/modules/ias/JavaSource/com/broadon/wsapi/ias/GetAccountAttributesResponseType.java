/**
 * GetAccountAttributesResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ias;

public class GetAccountAttributesResponseType  extends com.broadon.wsapi.ias.AbstractAccountResponseType  implements java.io.Serializable {
    private com.broadon.wsapi.ias.AttributeType[] accountAttributes;

    public GetAccountAttributesResponseType() {
    }

    public GetAccountAttributesResponseType(
           com.broadon.wsapi.ias.AttributeType[] accountAttributes) {
           this.accountAttributes = accountAttributes;
    }


    /**
     * Gets the accountAttributes value for this GetAccountAttributesResponseType.
     * 
     * @return accountAttributes
     */
    public com.broadon.wsapi.ias.AttributeType[] getAccountAttributes() {
        return accountAttributes;
    }


    /**
     * Sets the accountAttributes value for this GetAccountAttributesResponseType.
     * 
     * @param accountAttributes
     */
    public void setAccountAttributes(com.broadon.wsapi.ias.AttributeType[] accountAttributes) {
        this.accountAttributes = accountAttributes;
    }

    public com.broadon.wsapi.ias.AttributeType getAccountAttributes(int i) {
        return this.accountAttributes[i];
    }

    public void setAccountAttributes(int i, com.broadon.wsapi.ias.AttributeType _value) {
        this.accountAttributes[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetAccountAttributesResponseType)) return false;
        GetAccountAttributesResponseType other = (GetAccountAttributesResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.accountAttributes==null && other.getAccountAttributes()==null) || 
             (this.accountAttributes!=null &&
              java.util.Arrays.equals(this.accountAttributes, other.getAccountAttributes())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAccountAttributes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAccountAttributes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAccountAttributes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetAccountAttributesResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetAccountAttributesResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountAttributes");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AccountAttributes"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AttributeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

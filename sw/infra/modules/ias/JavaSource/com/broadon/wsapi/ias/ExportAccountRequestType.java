/**
 * ExportAccountRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ias;

public class ExportAccountRequestType  extends com.broadon.wsapi.ias.AbstractRequestType  implements java.io.Serializable {
    private com.broadon.wsapi.ias.ExternalAccountType[] externalAccountInfo;

    public ExportAccountRequestType() {
    }

    public ExportAccountRequestType(
           com.broadon.wsapi.ias.ExternalAccountType[] externalAccountInfo) {
           this.externalAccountInfo = externalAccountInfo;
    }


    /**
     * Gets the externalAccountInfo value for this ExportAccountRequestType.
     * 
     * @return externalAccountInfo
     */
    public com.broadon.wsapi.ias.ExternalAccountType[] getExternalAccountInfo() {
        return externalAccountInfo;
    }


    /**
     * Sets the externalAccountInfo value for this ExportAccountRequestType.
     * 
     * @param externalAccountInfo
     */
    public void setExternalAccountInfo(com.broadon.wsapi.ias.ExternalAccountType[] externalAccountInfo) {
        this.externalAccountInfo = externalAccountInfo;
    }

    public com.broadon.wsapi.ias.ExternalAccountType getExternalAccountInfo(int i) {
        return this.externalAccountInfo[i];
    }

    public void setExternalAccountInfo(int i, com.broadon.wsapi.ias.ExternalAccountType _value) {
        this.externalAccountInfo[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ExportAccountRequestType)) return false;
        ExportAccountRequestType other = (ExportAccountRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.externalAccountInfo==null && other.getExternalAccountInfo()==null) || 
             (this.externalAccountInfo!=null &&
              java.util.Arrays.equals(this.externalAccountInfo, other.getExternalAccountInfo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getExternalAccountInfo() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getExternalAccountInfo());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getExternalAccountInfo(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ExportAccountRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ExportAccountRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("externalAccountInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ExternalAccountInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ExternalAccountType"));
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

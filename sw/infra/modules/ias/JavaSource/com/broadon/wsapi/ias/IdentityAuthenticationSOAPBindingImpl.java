/**
 * IdentityAuthenticationSOAPBindingImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ias;

import com.broadon.ias.*;

public class IdentityAuthenticationSOAPBindingImpl 
    extends IdentityAuthenticationServiceImpl 
    implements com.broadon.wsapi.ias.IdentityAuthenticationPortType {

    public com.broadon.wsapi.ias.AuthenticateAccountResponseType authenticateAccount(com.broadon.wsapi.ias.AuthenticateAccountRequestType authenticateAccountRequest) throws java.rmi.RemoteException {
        return AuthenticateAccount.authenticateAccount(this, authenticateAccountRequest);
    }

    public com.broadon.wsapi.ias.AuthenticateDeviceResponseType authenticateDevice(com.broadon.wsapi.ias.AuthenticateDeviceRequestType authenticateDeviceRequest) throws java.rmi.RemoteException {
        return AuthenticateDevice.authenticateDevice(this, authenticateDeviceRequest);
    }

    public com.broadon.wsapi.ias.ExportAccountResponseType exportAccount(com.broadon.wsapi.ias.ExportAccountRequestType exportAccountRequest) throws java.rmi.RemoteException {
        return ExportAccount.exportAccount(this, exportAccountRequest);
    }
    
    public com.broadon.wsapi.ias.GetAccountAttributesResponseType getAccountAttributes(com.broadon.wsapi.ias.GetAccountAttributesRequestType getAccountAttributesRequest) throws java.rmi.RemoteException {
        return GetAccountAttributes.getAccountAttributes(this, getAccountAttributesRequest);
    }

    public com.broadon.wsapi.ias.GetAccountInfoResponseType getAccountInfo(com.broadon.wsapi.ias.GetAccountInfoRequestType getAccountInfoRequest) throws java.rmi.RemoteException {
        return GetAccountInfo.getAccountInfo(this, getAccountInfoRequest);
    }

    public com.broadon.wsapi.ias.GetDeviceAccountsResponseType getDeviceAccounts(com.broadon.wsapi.ias.GetDeviceAccountsRequestType getDeviceAccountsRequest) throws java.rmi.RemoteException {
        return GetDeviceAccounts.getDeviceAccounts(this, getDeviceAccountsRequest);
    }

    public com.broadon.wsapi.ias.GetDeviceInfoResponseType getDeviceInfo(com.broadon.wsapi.ias.GetDeviceInfoRequestType getDeviceInfoRequest) throws java.rmi.RemoteException {
        return GetDeviceInfo.getDeviceInfo(this, getDeviceInfoRequest);
    }

    public com.broadon.wsapi.ias.GetDeviceSubscriptionsResponseType getDeviceSubscriptions(com.broadon.wsapi.ias.GetDeviceSubscriptionsRequestType getDeviceSubscriptionsRequest) throws java.rmi.RemoteException {
        return GetDeviceSubscriptions.getDeviceSubscriptions(this, getDeviceSubscriptionsRequest);
    }

    public com.broadon.wsapi.ias.LinkDeviceAccountResponseType linkDeviceAccount(com.broadon.wsapi.ias.LinkDeviceAccountRequestType linkDeviceAccountRequest) throws java.rmi.RemoteException {
        return LinkDeviceAccount.linkDeviceAccount(this, linkDeviceAccountRequest);
    }

    public com.broadon.wsapi.ias.RegisterAccountResponseType registerAccount(com.broadon.wsapi.ias.RegisterAccountRequestType registerAccountRequest) throws java.rmi.RemoteException {
        return RegisterAccount.registerAccount(this, registerAccountRequest);
    }

    public com.broadon.wsapi.ias.RegisterDeviceResponseType registerDevice(com.broadon.wsapi.ias.RegisterDeviceRequestType registerDeviceRequest) throws java.rmi.RemoteException {
        return RegisterDevice.registerDevice(this, registerDeviceRequest);
    }

    public com.broadon.wsapi.ias.RemoveDeviceAccountResponseType removeDeviceAccount(com.broadon.wsapi.ias.RemoveDeviceAccountRequestType removeDeviceAccountRequest) throws java.rmi.RemoteException {
        return RemoveDeviceAccount.removeDeviceAccount(this, removeDeviceAccountRequest);
    }

    public com.broadon.wsapi.ias.RenewAccountTokenResponseType renewAccountToken(com.broadon.wsapi.ias.RenewAccountTokenRequestType renewAccountTokenRequest) throws java.rmi.RemoteException {
        return RenewAccountToken.renewAccountToken(this, renewAccountTokenRequest);
    }

    public com.broadon.wsapi.ias.RenewDeviceTokenResponseType renewDeviceToken(com.broadon.wsapi.ias.RenewDeviceTokenRequestType renewDeviceTokenRequest) throws java.rmi.RemoteException {
        return RenewDeviceToken.renewDeviceToken(this, renewDeviceTokenRequest);
    }

    public com.broadon.wsapi.ias.ResetAccountPasswordResponseType resetAccountPassword(com.broadon.wsapi.ias.ResetAccountPasswordRequestType resetAccountPasswordRequest) throws java.rmi.RemoteException {
        return ResetAccountPassword.resetAccountPassword(this, resetAccountPasswordRequest);
    }

    public com.broadon.wsapi.ias.UpdateAccountResponseType updateAccount(com.broadon.wsapi.ias.UpdateAccountRequestType updateAccountRequest) throws java.rmi.RemoteException {
        return UpdateAccount.updateAccount(this, updateAccountRequest);
    }

    public com.broadon.wsapi.ias.UpdateAccountAttributesResponseType updateAccountAttributes(com.broadon.wsapi.ias.UpdateAccountAttributesRequestType updateAccountAttributesRequest) throws java.rmi.RemoteException {
        return UpdateAccountAttributes.updateAccountAttributes(this, updateAccountAttributesRequest);
    }

    public com.broadon.wsapi.ias.UpdateAccountPasswordResponseType updateAccountPassword(com.broadon.wsapi.ias.UpdateAccountPasswordRequestType updateAccountPasswordRequest) throws java.rmi.RemoteException {
        return UpdateAccountPassword.updateAccountPassword(this, updateAccountPasswordRequest);
    }

    public com.broadon.wsapi.ias.UpdateDeviceSubscriptionResponseType updateDeviceSubscription(com.broadon.wsapi.ias.UpdateDeviceSubscriptionRequestType updateDeviceSubscriptionRequest) throws java.rmi.RemoteException {
        return UpdateDeviceSubscription.updateDeviceSubscription(this, updateDeviceSubscriptionRequest);
    }

    public com.broadon.wsapi.ias.ValidateAccountTokenResponseType validateAccountToken(com.broadon.wsapi.ias.ValidateAccountTokenRequestType validateAccountTokenRequest) throws java.rmi.RemoteException {
        return ValidateAccountToken.validateAccountToken(this, validateAccountTokenRequest);
    }

    public com.broadon.wsapi.ias.ValidateDeviceTokenResponseType validateDeviceToken(com.broadon.wsapi.ias.ValidateDeviceTokenRequestType validateDeviceTokenRequest) throws java.rmi.RemoteException {
        return ValidateDeviceToken.validateDeviceToken(this, validateDeviceTokenRequest);
    }

}

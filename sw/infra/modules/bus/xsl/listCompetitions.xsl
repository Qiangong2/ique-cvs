<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:param name="id">-1</xsl:param>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">

    <xsl:choose>
    <xsl:when test="COMPETITION_ID=$id">                         
        <option>
        <xsl:attribute name="selected">true</xsl:attribute>
        <xsl:attribute name="value"><xsl:value-of select="COMPETITION_ID"/></xsl:attribute>
        <xsl:value-of disable-output-escaping="yes" select="COMPETITION_NAME"/>
        <xsl:text> (</xsl:text>
        <xsl:value-of select="COMPETITION_ID"/>
        <xsl:text>)</xsl:text>
        </option>
    </xsl:when>
    <xsl:otherwise>
        <option>
        <xsl:attribute name="value"><xsl:value-of select="COMPETITION_ID"/></xsl:attribute>
        <xsl:value-of disable-output-escaping="yes" select="COMPETITION_NAME"/>
        <xsl:text> (</xsl:text>
        <xsl:value-of select="COMPETITION_ID"/>
        <xsl:text>)</xsl:text>
        </option>
    </xsl:otherwise>
    </xsl:choose>

</xsl:template>

</xsl:stylesheet>

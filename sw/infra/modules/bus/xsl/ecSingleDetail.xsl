<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
<tr>
  <td class="formLabel2" nowrap="true">Game Ticket Type:</td>
  <td class="formField"></td>
  <td class="formField">
     <xsl:value-of select="ECARD_TYPE"/>
     <xsl:choose>
         <xsl:when test="DESCRIPTION!=''">
             <xsl:text> (</xsl:text>
             <xsl:value-of disable-output-escaping="yes" select="DESCRIPTION"/>
             <xsl:text>)</xsl:text>
         </xsl:when>
     </xsl:choose>
  </td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">EUnits Redeemed:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="EUNITS_REDEEMED"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">Usable:</td>
  <td class="formField"></td>
  <td class="formField">
  <xsl:choose>
      <xsl:when test="IS_USABLE='1'"> 
          <xsl:text>Yes</xsl:text>
      </xsl:when>
      <xsl:otherwise>
          <xsl:text>No</xsl:text>
      </xsl:otherwise>
  </xsl:choose>
  </td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">Activate Date:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="ACTIVATE_DATE"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">Last Redeem Date:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="LAST_REDEEM_DATE"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">Revoke Date:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="REVOKE_DATE"/></td>
</tr>

</xsl:template>

</xsl:stylesheet>

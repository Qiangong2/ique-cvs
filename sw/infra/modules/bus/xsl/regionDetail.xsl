<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:param name="id">-1</xsl:param>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">

<xsl:choose>
<xsl:when test="REGION_ID!=''">                         
    <tr><td class="formLabel2" nowrap="true">Region ID:</td><td class="formField"></td>
    <td class="formField" nowrap="true">
        <input>
                <xsl:attribute name="type">hidden</xsl:attribute>
                <xsl:attribute name="name">id</xsl:attribute>
                <xsl:attribute name="value"><xsl:value-of select="REGION_ID"/></xsl:attribute>
        </input>
        <input>
		<xsl:attribute name="type">hidden</xsl:attribute>
		<xsl:attribute name="name">buid</xsl:attribute>
		<xsl:attribute name="value"><xsl:value-of select="BU_ID"/></xsl:attribute>
        </input>
	<xsl:value-of select="REGION_ID"/>
    </td></tr> 
</xsl:when>
</xsl:choose>

<xsl:choose>
    <xsl:when test="$id!=120">
        <xsl:choose>
        <xsl:when test="REGION_ID!=''">
            <tr><td class="formLabel2" nowrap="true">Region Name:</td><td class="formField"></td>
            <td class="formField" nowrap="true">
                <xsl:value-of disable-output-escaping="yes" select="REGION_NAME"/>
            </td></tr>
        </xsl:when>
        </xsl:choose>
    </xsl:when>
    <xsl:otherwise>
        <tr><td class="formLabel2" nowrap="true">Region Name:</td><td class="formField"></td>
        <td class="formField" nowrap="true">
        <input>
                <xsl:attribute name="type">text</xsl:attribute>
                <xsl:attribute name="size">30</xsl:attribute>
                <xsl:attribute name="maxlength">64</xsl:attribute>
                <xsl:attribute name="name">region_name</xsl:attribute>
                <xsl:attribute name="value"><xsl:value-of disable-output-escaping="yes" select="REGION_NAME"/></xsl:attribute>
        </input>
        <font color="red">*</font>
        </td></tr>
    </xsl:otherwise>
</xsl:choose>

<xsl:choose>
<xsl:when test="REGION_ID!=''">                         
    <tr><td class="formLabel2" nowrap="true">Create Date:</td><td class="formField"></td>
    <td class="formField" nowrap="true">
	<xsl:value-of select="CREATE_DATE"/>
    </td></tr> 
</xsl:when>
</xsl:choose>

<xsl:choose>
<xsl:when test="REGION_ID!=''">                         
    <tr><td class="formLabel2" nowrap="true">Revoke Date:</td><td class="formField"></td>
    <td class="formField" nowrap="true">
	<xsl:value-of select="REVOKE_DATE"/>
    </td></tr> 
</xsl:when>
</xsl:choose>

<xsl:choose>
<xsl:when test="REGION_ID!=''">                         
    <tr><td class="formLabel2" nowrap="true">Suspend Date:</td><td class="formField"></td>
    <td class="formField" nowrap="true">
	<xsl:value-of select="SUSPEND_DATE"/>
    </td></tr> 
</xsl:when>
</xsl:choose>

</xsl:template>

</xsl:stylesheet>

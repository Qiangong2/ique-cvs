<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW[position() mod 2 = 1]">
   <tr class="oddrow">
        <td class="smallText" nowrap="true">
         <A class="listText">
          <xsl:attribute name="href">
           <xsl:text>serv?type=ctr&amp;action=edit&amp;titleid=</xsl:text>
           <xsl:value-of select="TITLE_ID"/>
          </xsl:attribute>
          <xsl:value-of disable-output-escaping="yes" select="TITLE"/>
         </A>
        </td>
        <td class="smallText" nowrap="true">
          <xsl:value-of select="START_DATE"/>
        </td>
        <td class="smallText" nowrap="true">
          <xsl:value-of select="END_DATE"/>
        </td>
   </tr>
</xsl:template>

<xsl:template match="ROW">
   <tr class="evenrow">
        <td class="smallText" nowrap="true">
         <A class="listText">
          <xsl:attribute name="href">
           <xsl:text>serv?type=ctr&amp;action=edit&amp;titleid=</xsl:text>
           <xsl:value-of select="TITLE_ID"/>
          </xsl:attribute>
          <xsl:value-of disable-output-escaping="yes" select="TITLE"/>
         </A>
        </td>
        <td class="smallText" nowrap="true">
          <xsl:value-of select="START_DATE"/>
        </td>
        <td class="smallText" nowrap="true">
          <xsl:value-of select="END_DATE"/>
        </td>
   </tr>
</xsl:template>

</xsl:stylesheet>

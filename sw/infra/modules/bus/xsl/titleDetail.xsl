<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">

<tr>
    <td class="formLabel2" nowrap="true">Title ID:</td>
    <td class="formField"></td>
    <td class="formField" nowrap="true"><xsl:value-of select="TITLE_ID"/></td>
</tr> 

<tr>
    <td class="formLabel2" nowrap="true">Title:</td>
    <td class="formField"></td>
    <td class="formField" nowrap="true"><xsl:value-of disable-output-escaping="yes" select="TITLE"/></td>
</tr> 

<tr>
    <td class="formLabel2" nowrap="true">Title Type:</td>
    <td class="formField"></td>
    <td class="formField" nowrap="true"><xsl:value-of disable-output-escaping="yes" select="TITLE_TYPE"/></td>
</tr> 

<tr>
    <td class="formLabel2" nowrap="true">Category:</td>
    <td class="formField"></td>
    <td class="formField" nowrap="true"><xsl:value-of disable-output-escaping="yes" select="CATEGORY"/></td>
</tr> 

<tr>
    <td class="formLabel2" nowrap="true">Publisher:</td>
    <td class="formField"></td>
    <td class="formField" nowrap="true"><xsl:value-of disable-output-escaping="yes" select="PUBLISHER"/></td>
</tr> 

<tr>
    <td class="formLabel2" nowrap="true">Developer:</td>
    <td class="formField"></td>
    <td class="formField" nowrap="true"><xsl:value-of disable-output-escaping="yes" select="DEVELOPER"/></td>
</tr> 

<tr>
    <td class="formLabel2" nowrap="true">Description:</td>
    <td class="formField"></td>
    <td class="formField">
      <xsl:call-template name="replace-newline">
        <xsl:with-param name="stringIn"><xsl:value-of disable-output-escaping="yes" select="DESCRIPTION"/></xsl:with-param>
      </xsl:call-template>
    </td>
</tr>

<tr>
    <td class="formLabel2" nowrap="true">Last Updated:</td>
    <td class="formField"></td>
    <td class="formField" nowrap="true">
	<xsl:value-of select="LAST_UPDATED"/>
    </td>
</tr> 

</xsl:template>

<xsl:template name="replace-newline">
   <xsl:param name="stringIn"/>
   <xsl:choose>
   <xsl:when test="contains($stringIn, '&#xa;')">
      <xsl:value-of select="substring-before($stringIn, '&#xa;')" disable-output-escaping="yes"/>
      <br/>
      <xsl:call-template name="replace-newline">
          <xsl:with-param name="stringIn" select="substring-after($stringIn,'&#xa;')"/>
      </xsl:call-template>
   </xsl:when>
   <xsl:otherwise>
        <xsl:value-of select="$stringIn" disable-output-escaping="yes"/>
   </xsl:otherwise>
   </xsl:choose>
</xsl:template>

</xsl:stylesheet>

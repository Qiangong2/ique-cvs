<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW[position() mod 2 = 1]">
<xsl:choose>
  <xsl:when test="CATEGORY_CODE">
   <tr class="oddrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=compcat&amp;action=edit&amp;comp_id=</xsl:text>
              <xsl:value-of select="COMPETITION_ID"/>
              <xsl:text>&amp;cat_code=&quot;</xsl:text>
              <xsl:value-of select="CATEGORY_CODE"/>
              <xsl:text>&quot;</xsl:text>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="CATEGORY_CODE"/>
          </A>    
        </td>
        <td class="smallText" nowrap="true"><xsl:value-of select="CATEGORY_NAME"/></td>
        <td class="smallText" nowrap="true"><xsl:value-of select="CITY_CODE"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="START_DATE"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="END_DATE"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="LAST_UPDATED"/></td>
   </tr>
  </xsl:when>
</xsl:choose>
</xsl:template>

<xsl:template match="ROW">
<xsl:choose>
  <xsl:when test="CATEGORY_CODE">
   <tr class="evenrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=compcat&amp;action=edit&amp;comp_id=</xsl:text>
              <xsl:value-of select="COMPETITION_ID"/>
              <xsl:text>&amp;cat_code=&quot;</xsl:text>
              <xsl:value-of select="CATEGORY_CODE"/>
              <xsl:text>&quot;</xsl:text>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="CATEGORY_CODE"/>
          </A>    
        </td>
        <td class="smallText" nowrap="true"><xsl:value-of select="CATEGORY_NAME"/></td>
        <td class="smallText" nowrap="true"><xsl:value-of select="CITY_CODE"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="START_DATE"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="END_DATE"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="LAST_UPDATED"/></td>
   </tr>
  </xsl:when>
</xsl:choose>
</xsl:template>

</xsl:stylesheet>

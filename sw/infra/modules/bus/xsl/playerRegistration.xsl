<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
<tr>
  <td class="formLabel2">Name:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of disable-output-escaping="yes" select="NAME"/></td>
</tr>
<tr>
  <td class="formLabel2">Code:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="EMAIL_ADDRESS"/></td>
</tr>
<tr>
  <td class="formLabel2">ADDRESS:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="ADDRESS"/></td>
</tr>
<tr>
  <td class="formLabel2">BIRTHDATE:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="BIRTHDATE"/></td>
</tr>
<tr>
  <td class="formLabel2">TELEPHONE:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="TELEPHONE"/></td>
</tr>
<tr>
  <td class="formLabel2">GENDER:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="GENDER"/></td>
</tr>
<tr>
  <td class="formLabel2">PRODUCT SN:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="EXT_SN"/></td>
</tr>
<tr>
  <td class="formLabel2">BB_ID:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="BB_ID"/></td>
</tr>
<tr>
  <td class="formLabel2">STORE_ADDRESS:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="STORE_ADDRESS"/></td>
</tr>

</xsl:template>

</xsl:stylesheet>

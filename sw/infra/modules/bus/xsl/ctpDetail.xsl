<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">

<xsl:choose>
<xsl:when test="PRICING_ID!=''">                         
    <tr><td class="formLabel2" nowrap="true">Pricing ID:</td><td class="formField"></td>
    <td class="formField" nowrap="true">
        <input>
            <xsl:attribute name="type">hidden</xsl:attribute>
            <xsl:attribute name="name">pricing_id</xsl:attribute>
            <xsl:attribute name="value"><xsl:value-of select="PRICING_ID"/></xsl:attribute>
        </input>
	<xsl:value-of select="PRICING_ID"/>
    </td></tr> 
</xsl:when>
</xsl:choose>

<xsl:choose>
<xsl:when test="PRICING_ID!=''">                         
  <tr>
    <td class="formLabel2" nowrap="true">Pricing Name:</td><td class="formField"></td>
    <td class="formField" nowrap="true">
    <input>
        <xsl:attribute name="type">text</xsl:attribute>
        <xsl:attribute name="size">30</xsl:attribute>
        <xsl:attribute name="maxlength">128</xsl:attribute>
        <xsl:attribute name="name">pricing_name</xsl:attribute>
        <xsl:attribute name="value"><xsl:value-of disable-output-escaping="yes" select="PRICING_NAME"/></xsl:attribute>
    </input>
    <font color="red">*</font>
    </td>
  </tr>
</xsl:when>
</xsl:choose>

<tr>
    <td class="formLabel2" nowrap="true">Pricing Type:</td><td class="formField"></td>
    <td class="formField" nowrap="true">
    <xsl:choose>
    <xsl:when test="PRICING_ID!=''">                         
      <xsl:value-of disable-output-escaping="yes" select="PRICING_TYPE"/>
    </xsl:when>
    <xsl:otherwise>
     <select>
      <xsl:attribute name="name">pricing_type</xsl:attribute>
      <option>
        <xsl:attribute name="value"></xsl:attribute>
        <xsl:text></xsl:text>
      </option>
      <option>
        <xsl:attribute name="value">BONUS</xsl:attribute>
        <xsl:text>BONUS</xsl:text>
      </option>
      <option>
        <xsl:attribute name="value">PERMANENT</xsl:attribute>
        <xsl:text>PERMANENT</xsl:text>
      </option>
      <option>
         <xsl:attribute name="value">RENTAL</xsl:attribute>
          <xsl:text>RENTAL</xsl:text>
      </option>
      <option>
         <xsl:attribute name="value">TRIAL</xsl:attribute>
          <xsl:text>TRIAL</xsl:text>
      </option>
     </select>
     <font color="red">*</font>
    </xsl:otherwise>
    </xsl:choose>
    </td>
</tr>

<tr>
    <td class="formLabel2" nowrap="true">Mask:</td><td class="formField"></td>
    <td class="formField" nowrap="true">
    <xsl:choose>
    <xsl:when test="PRICING_ID!=''">                         
	<xsl:value-of disable-output-escaping="yes" select="MASK"/>
    </xsl:when>
    <xsl:otherwise>
      <input>
        <xsl:attribute name="disabled">true</xsl:attribute>
        <xsl:attribute name="type">text</xsl:attribute>
        <xsl:attribute name="size">30</xsl:attribute>
        <xsl:attribute name="maxlength">64</xsl:attribute>
        <xsl:attribute name="name">mask</xsl:attribute>
        <xsl:attribute name="value"></xsl:attribute>
      </input>
    </xsl:otherwise>
    </xsl:choose>
    </td>
</tr>

<tr>
    <td class="formLabel2" nowrap="true">Price:</td><td class="formField"></td>
    <td class="formField" nowrap="true">
    <xsl:choose>
    <xsl:when test="PRICING_ID!=''">                         
        <xsl:value-of select="PRICE"/>
    </xsl:when>
    <xsl:otherwise>
      <input>
        <xsl:attribute name="type">text</xsl:attribute>
        <xsl:attribute name="size">30</xsl:attribute>
        <xsl:attribute name="name">price</xsl:attribute>
        <xsl:attribute name="value"></xsl:attribute>
      </input>
      <font color="red">*</font>
    </xsl:otherwise>
    </xsl:choose>
    </td>
</tr>

<tr>
    <td class="formLabel2" nowrap="true">Rights:</td><td class="formField"></td>
    <td class="formField" nowrap="true">
    <xsl:choose>
    <xsl:when test="PRICING_ID!=''">                         
      <xsl:choose>
      <xsl:when test="RTYPE='PR'">                         
        <xsl:text>Permanent</xsl:text>
      </xsl:when>
      <xsl:when test="RTYPE='LR'">                         
        <xsl:text>Limited Play</xsl:text>
      </xsl:when>
      <xsl:when test="RTYPE='TR'">                         
        <xsl:text>Limited Time</xsl:text>
      </xsl:when>
      </xsl:choose>
    </xsl:when>
    <xsl:otherwise>
     <select>
      <xsl:attribute name="name">rtype</xsl:attribute>
      <option>
        <xsl:attribute name="value"></xsl:attribute>
        <xsl:text></xsl:text>
      </option>
      <option>
        <xsl:attribute name="value">LR</xsl:attribute>
        <xsl:text>Limited Play</xsl:text>
      </option>
      <option>
        <xsl:attribute name="value">TR</xsl:attribute>
        <xsl:text>Limited Time</xsl:text>
      </option>
      <option>
         <xsl:attribute name="value">PR</xsl:attribute>
          <xsl:text>Permanent</xsl:text>
      </option>
     </select>
     <font color="red">*</font>
    </xsl:otherwise>
    </xsl:choose>
    </td>
</tr>

<tr>
    <td class="formLabel2" nowrap="true">Limits:</td><td class="formField"></td>
    <td class="formField" nowrap="true">
    <xsl:choose>
    <xsl:when test="PRICING_ID!=''">                         
      <xsl:value-of select="LIMITS"/>
    </xsl:when>
    <xsl:otherwise>
      <input>
        <xsl:attribute name="type">hidden</xsl:attribute>
        <xsl:attribute name="name">status</xsl:attribute>
        <xsl:attribute name="value">A</xsl:attribute>
      </input>
      <input>
        <xsl:attribute name="type">text</xsl:attribute>
        <xsl:attribute name="size">30</xsl:attribute>
        <xsl:attribute name="name">limits</xsl:attribute>
        <xsl:attribute name="value"></xsl:attribute>
      </input>
    </xsl:otherwise>
    </xsl:choose>
    </td>
</tr>

<xsl:choose>
<xsl:when test="REGION_ID!=''">                         
    <tr>
        <td class="formLabel2" nowrap="true">Last Updated:</td><td class="formField"></td>
        <td class="formField" nowrap="true">
	    <xsl:value-of select="LAST_UPDATED"/>
        </td>
    </tr> 
</xsl:when>
</xsl:choose>

</xsl:template>

</xsl:stylesheet>

<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
<tr>
  <td class="formLabel2" nowrap="true">Game Ticket Type:</td>
  <td class="formField"></td>
  <td class="formField">
     <xsl:value-of select="ECARD_TYPE"/>
     <xsl:choose>
         <xsl:when test="DESCRIPTION!=''">
             <xsl:text> (</xsl:text>
             <xsl:value-of disable-output-escaping="yes" select="DESCRIPTION"/>
             <xsl:text>)</xsl:text>
         </xsl:when>
     </xsl:choose>
  </td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">Usable:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="USABLE"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">Not Usable:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="NOT_USABLE"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">Activated:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="ACTIVATED"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">Redeemed:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="REDEEMED"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">Revoked:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="REVOKED"/></td>
</tr>

</xsl:template>

</xsl:stylesheet>

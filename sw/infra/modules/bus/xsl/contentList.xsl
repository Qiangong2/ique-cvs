<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
	extension-element-prefixes="xalan">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW[position() mod 2 = 1]">
   <tr class="oddrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=content&amp;action=edit&amp;contentid=</xsl:text>
              <xsl:value-of disable-output-escaping="yes" select="CONTENT_ID"/>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="CONTENT_OBJECT_NAME"/>
          </A>    
        </td>
	<td class="smallText2"><xsl:value-of select="CONTENT_ID"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="PUBLISH_DATE"/></td>
	<td class="smallText2"><xsl:value-of disable-output-escaping="yes" select="CONTENT_OBJECT_VERSION"/></td>
	<td class="smallText2"><xsl:value-of select="CONTENT_SIZE"/></td>
	<td class="smallText"><xsl:value-of disable-output-escaping="yes" select="CONTENT_OBJECT_TYPE"/></td>
	<td class="smallText" nowrap="true">
            <xsl:choose>
                <xsl:when test="MIN_UPGRADE_VERSION!=''">
                    <xsl:value-of select="MIN_UPGRADE_VERSION"/>
                </xsl:when>
                <xsl:otherwise>
   		    <xsl:text>N/A</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </td>
	<td class="smallText" nowrap="true">
            <xsl:choose>
                <xsl:when test="UPGRADE_CONSENT='0'">
   		    <xsl:text>No</xsl:text>
                </xsl:when>
                <xsl:when test="UPGRADE_CONSENT='1'">
   		    <xsl:text>Yes</xsl:text>
                </xsl:when>
                <xsl:otherwise>
   		    <xsl:text>N/A</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </td>
	<td class="smallText" nowrap="true"><xsl:value-of select="LAST_UPDATED"/></td>
   </tr>
</xsl:template>

<xsl:template match="ROW">
   <tr class="evenrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=content&amp;action=edit&amp;contentid=</xsl:text>
              <xsl:value-of disable-output-escaping="yes" select="CONTENT_ID"/>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="CONTENT_OBJECT_NAME"/>
          </A>    
        </td>
	<td class="smallText2"><xsl:value-of select="CONTENT_ID"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="PUBLISH_DATE"/></td>
	<td class="smallText2"><xsl:value-of disable-output-escaping="yes" select="CONTENT_OBJECT_VERSION"/></td>
	<td class="smallText2"><xsl:value-of select="CONTENT_SIZE"/></td>
	<td class="smallText"><xsl:value-of disable-output-escaping="yes" select="CONTENT_OBJECT_TYPE"/></td>
	<td class="smallText" nowrap="true">
            <xsl:choose>
                <xsl:when test="MIN_UPGRADE_VERSION!=''">
                    <xsl:value-of select="MIN_UPGRADE_VERSION"/>
                </xsl:when>
                <xsl:otherwise>
   		    <xsl:text>N/A</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </td>
	<td class="smallText" nowrap="true">
            <xsl:choose>
                <xsl:when test="UPGRADE_CONSENT='0'">
   		    <xsl:text>No</xsl:text>
                </xsl:when>
                <xsl:when test="UPGRADE_CONSENT='1'">
   		    <xsl:text>Yes</xsl:text>
                </xsl:when>
                <xsl:otherwise>
   		    <xsl:text>N/A</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </td>
	<td class="smallText" nowrap="true"><xsl:value-of select="LAST_UPDATED"/></td>
  </tr>
</xsl:template>

</xsl:stylesheet>

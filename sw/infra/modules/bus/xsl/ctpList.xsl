<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW[position() mod 2 = 1]">
   <tr class="oddrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=ctp&amp;action=edit&amp;pricing_id=</xsl:text>
              <xsl:value-of select="PRICING_ID"/>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="PRICING_NAME"/>
          </A>    
        </td>
        <td class="smallText2"><xsl:value-of select="PRICING_ID"/></td>
        <td class="smallText"><xsl:value-of disable-output-escaping="yes" select="PRICING_TYPE"/></td>
        <td class="smallText" nowrap="true"><xsl:value-of select="MASK"/></td>
	<td class="smallText2"><xsl:value-of select="PRICE"/></td>
	<td class="smallText" nowrap="true">
          <xsl:choose>
            <xsl:when test="RTYPE='PR'">
              <xsl:text>Permanent</xsl:text>
            </xsl:when>
            <xsl:when test="RTYPE='LR'">
              <xsl:text>Limited Play</xsl:text>
            </xsl:when>
            <xsl:when test="RTYPE='TR'">
              <xsl:text>Limited Time</xsl:text>
            </xsl:when>
          </xsl:choose>
        </td>
	<td class="smallText" nowrap="true">
          <xsl:choose>
            <xsl:when test="RTYPE='PR'">
              <xsl:text>-</xsl:text>
            </xsl:when>
            <xsl:when test="RTYPE='LR'">
              <xsl:value-of select="LIMITS"/>
              <xsl:text> Plays</xsl:text>
            </xsl:when>
            <xsl:when test="RTYPE='TR'">
              <xsl:value-of select="LIMITS"/>
              <xsl:text> Minutes</xsl:text>
            </xsl:when>
          </xsl:choose>
        </td>        
	<td class="smallText" nowrap="true"><xsl:value-of select="LAST_UPDATED"/></td>        
   </tr>
</xsl:template>

<xsl:template match="ROW">
   <tr class="evenrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=ctp&amp;action=edit&amp;pricing_id=</xsl:text>
              <xsl:value-of select="PRICING_ID"/>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="PRICING_NAME"/>
          </A>    
        </td>
        <td class="smallText2"><xsl:value-of select="PRICING_ID"/></td>
        <td class="smallText"><xsl:value-of disable-output-escaping="yes" select="PRICING_TYPE"/></td>
        <td class="smallText" nowrap="true"><xsl:value-of select="MASK"/></td>
	<td class="smallText2"><xsl:value-of select="PRICE"/></td>
	<td class="smallText" nowrap="true">
          <xsl:choose>
            <xsl:when test="RTYPE='PR'">
              <xsl:text>Permanent</xsl:text>
            </xsl:when>
            <xsl:when test="RTYPE='LR'">
              <xsl:text>Limited Play</xsl:text>
            </xsl:when>
            <xsl:when test="RTYPE='TR'">
              <xsl:text>Limited Time</xsl:text>
            </xsl:when>
          </xsl:choose>
        </td>
	<td class="smallText" nowrap="true">
          <xsl:choose>
            <xsl:when test="RTYPE='PR'">
              <xsl:text>-</xsl:text>
            </xsl:when>
            <xsl:when test="RTYPE='LR'">
              <xsl:value-of select="LIMITS"/>
              <xsl:text> Plays</xsl:text>
            </xsl:when>
            <xsl:when test="RTYPE='TR'">
              <xsl:value-of select="LIMITS"/>
              <xsl:text> Minutes</xsl:text>
            </xsl:when>
          </xsl:choose>
        </td>        
	<td class="smallText" nowrap="true"><xsl:value-of select="LAST_UPDATED"/></td>        
   </tr>
</xsl:template>

</xsl:stylesheet>

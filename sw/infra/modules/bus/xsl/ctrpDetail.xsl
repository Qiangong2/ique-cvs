<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:param name="id">-1</xsl:param>
<xsl:param name="popt">-1</xsl:param>
<xsl:param name="npopt">-1</xsl:param>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
<xsl:variable name="counter" select="NO+1"/>
<xsl:variable name="type_prev">
        <xsl:value-of select="preceding-sibling::ROW[1]/PRICING_TYPE"/>
</xsl:variable>

<xsl:choose>
  <xsl:when test="PRICING_TYPE!=$type_prev">
    <tr>
      <td colspan="8" bgcolor="white">
        <img border="0" height="1" width="1" src="/images/spacer.gif"/>
      </td>
    </tr>
  </xsl:when>
</xsl:choose>

<tr>
 <td class="formLabel5" nowrap="true" width="2%">
  <xsl:choose>
    <xsl:when test="$id!=130 and END_FLAG!='0'">
      <input>
         <xsl:attribute name="type">hidden</xsl:attribute>
         <xsl:attribute name="name"><xsl:value-of select="concat('active', $counter)"/></xsl:attribute>
         <xsl:attribute name="value">1</xsl:attribute>
      </input>
      <input>
     	 <xsl:attribute name="type">checkbox</xsl:attribute>
	 <xsl:attribute name="name"><xsl:value-of select="concat('region', $counter)"/></xsl:attribute>
	 <xsl:attribute name="value"><xsl:value-of select="REGION_ID"/></xsl:attribute>
         <xsl:attribute name="onClick">toggleCheckBox(theForm,'0',this.checked)</xsl:attribute>
      </input>
    </xsl:when>
    <xsl:when test="$id!=130">
      <input>
         <xsl:attribute name="type">hidden</xsl:attribute>
         <xsl:attribute name="name"><xsl:value-of select="concat('active', $counter)"/></xsl:attribute>
         <xsl:attribute name="value">0</xsl:attribute>
      </input>
      <input>
         <xsl:attribute name="type">checkbox</xsl:attribute>
         <xsl:attribute name="name"><xsl:value-of select="concat('region', $counter)"/></xsl:attribute>
         <xsl:attribute name="value"><xsl:value-of select="REGION_ID"/></xsl:attribute>
         <xsl:attribute name="disabled">true</xsl:attribute>
      </input>
    </xsl:when>
  </xsl:choose>
 </td>

 <td class="formField5" nowrap="true" width="15%">
  <xsl:choose>
    <xsl:when test="$id!=130">
      <xsl:choose>
        <xsl:when test="START_FLAG!='0'">
          <input>
            <xsl:attribute name="type">hidden</xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="concat('last_start', $counter)"/></xsl:attribute>
            <xsl:attribute name="value"><xsl:value-of select="PURCHASE_START_DATE"/></xsl:attribute>
          </input>
          <input>
            <xsl:attribute name="type">text</xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="concat('start', $counter)"/></xsl:attribute>
            <xsl:attribute name="size">19</xsl:attribute>
            <xsl:attribute name="value"><xsl:value-of select="PURCHASE_START_DATE"/></xsl:attribute>
            <xsl:attribute name="onfocus"><xsl:value-of select="concat('this.blur();popUpCalendar(theForm.start', $counter, ', ', 'theForm.start', $counter, ', ', &quot;'&quot;, 'yyyy.mm.dd', &quot;'&quot;, ', ', &quot;'&quot;, 'start', &quot;'&quot;, ')')"/></xsl:attribute>
          </input>
          <xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;</xsl:text>
          <img>
            <xsl:attribute name="src">/images/date.gif</xsl:attribute>
            <xsl:attribute name="onClick"><xsl:value-of select="concat('popUpCalendar(theForm.start', $counter, ', ', 'theForm.start', $counter, ', ', &quot;'&quot;, 'yyyy.mm.dd', &quot;'&quot;, ', ', &quot;'&quot;, 'start', &quot;'&quot;, ')')"/></xsl:attribute>
          </img>
        </xsl:when>
        <xsl:otherwise>
          <input>
            <xsl:attribute name="type">hidden</xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="concat('last_start', $counter)"/></xsl:attribute>
            <xsl:attribute name="value"><xsl:value-of select="PURCHASE_START_DATE"/></xsl:attribute>
          </input>
          <input>
            <xsl:attribute name="type">hidden</xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="concat('start', $counter)"/></xsl:attribute>
            <xsl:attribute name="value"><xsl:value-of select="PURCHASE_START_DATE"/></xsl:attribute>
          </input>
          <xsl:value-of select="PURCHASE_START_DATE"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="PURCHASE_START_DATE"/>
    </xsl:otherwise>
  </xsl:choose>
 </td>

 <td class="formField5" nowrap="true" width="15%">
  <xsl:choose>
    <xsl:when test="$id!=130">
      <xsl:choose>
        <xsl:when test="not(PURCHASE_END_DATE) or END_FLAG!='0'">
          <input>
            <xsl:attribute name="type">hidden</xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="concat('last_end', $counter)"/></xsl:attribute>
            <xsl:attribute name="value"><xsl:value-of select="PURCHASE_END_DATE"/></xsl:attribute>
          </input>
          <xsl:choose>
            <xsl:when test="RTYPE='PR'">
              <input>
                <xsl:attribute name="type">text</xsl:attribute>
                <xsl:attribute name="name"><xsl:value-of select="concat('end', $counter)"/></xsl:attribute>
                <xsl:attribute name="size">19</xsl:attribute>
                <xsl:attribute name="value"><xsl:value-of select="PURCHASE_END_DATE"/></xsl:attribute>
                <xsl:attribute name="onfocus"><xsl:value-of select="concat('this.blur();popUpCalendar(theForm.end', $counter, ', ', 'theForm.end', $counter, ', ', &quot;'&quot;, 'yyyy.mm.dd', &quot;'&quot;, ', ', &quot;'&quot;, 'end', &quot;'&quot;, ')')"/></xsl:attribute>
              </input>
              <xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;</xsl:text>
              <img>
                <xsl:attribute name="src">/images/date.gif</xsl:attribute>
                <xsl:attribute name="onClick"><xsl:value-of select="concat('popUpCalendar(theForm.end', $counter, ', ', 'theForm.end', $counter, ', ', &quot;'&quot;, 'yyyy.mm.dd', &quot;'&quot;, ', ', &quot;'&quot;, 'end', &quot;'&quot;, ')')"/></xsl:attribute>
              </img>
            </xsl:when>
            <xsl:otherwise>
              <input>
                <xsl:attribute name="type">hidden</xsl:attribute>
                <xsl:attribute name="name"><xsl:value-of select="concat('end', $counter)"/></xsl:attribute>
                <xsl:attribute name="value"><xsl:value-of select="PURCHASE_END_DATE"/></xsl:attribute>
              </input>
              <xsl:value-of select="PURCHASE_END_DATE"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:otherwise>
          <input>
            <xsl:attribute name="type">hidden</xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="concat('last_end', $counter)"/></xsl:attribute>
            <xsl:attribute name="value"><xsl:value-of select="PURCHASE_END_DATE"/></xsl:attribute>
          </input>
          <input>
            <xsl:attribute name="type">hidden</xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="concat('end', $counter)"/></xsl:attribute>
            <xsl:attribute name="value"><xsl:value-of select="PURCHASE_END_DATE"/></xsl:attribute>
          </input>
          <xsl:value-of select="PURCHASE_END_DATE"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="PURCHASE_END_DATE"/>
    </xsl:otherwise>
  </xsl:choose>
 </td>

 <td class="formField5" nowrap="true" width="30%">
  <xsl:choose>
    <xsl:when test="$id!=130">
      <xsl:choose>
        <xsl:when test="START_FLAG!='0'">
          <input>
            <xsl:attribute name="type">hidden</xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="concat('last_pid', $counter)"/></xsl:attribute>
            <xsl:attribute name="value"><xsl:value-of select="PRICING_ID"/></xsl:attribute>
          </input>
          <input>
            <xsl:attribute name="type">hidden</xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="concat('pid', $counter)"/></xsl:attribute>
            <xsl:attribute name="value"></xsl:attribute>
         </input>
         <input>
            <xsl:attribute name="type">hidden</xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="concat('ptype', $counter)"/></xsl:attribute>
            <xsl:attribute name="value"></xsl:attribute>
         </input>
         <input>
            <xsl:attribute name="type">hidden</xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="concat('rtype', $counter)"/></xsl:attribute>
            <xsl:attribute name="value"></xsl:attribute>
         </input>
         <xsl:choose>
           <xsl:when test="RTYPE!='PR'">
             <select>
               <xsl:attribute name="name"><xsl:value-of select="concat('pr', $counter)"/></xsl:attribute>
               <xsl:call-template name="get-pricing-options">
                 <xsl:with-param name="current_pr">
                   <xsl:value-of select="PRICING_ID"/><xsl:text>|</xsl:text><xsl:value-of select="PRICING_TYPE"/><xsl:text>|</xsl:text><xsl:value-of select="RTYPE"/>
                 </xsl:with-param>
                 <xsl:with-param name="current_name">
                   <xsl:value-of select="PRICING_NAME"/>
                 </xsl:with-param>
                 <xsl:with-param name="current_html">
                   <xsl:value-of select="$npopt"/>
                 </xsl:with-param>
               </xsl:call-template>
             </select>
           </xsl:when>
           <xsl:otherwise>
             <select>
               <xsl:attribute name="name"><xsl:value-of select="concat('pr', $counter)"/></xsl:attribute>
               <xsl:call-template name="get-pricing-options">
                 <xsl:with-param name="current_pr">
                   <xsl:value-of select="PRICING_ID"/><xsl:text>|</xsl:text><xsl:value-of select="PRICING_TYPE"/><xsl:text>|</xsl:text><xsl:value-of select="RTYPE"/>
                 </xsl:with-param>
                 <xsl:with-param name="current_name">
                   <xsl:value-of select="PRICING_NAME"/>
                 </xsl:with-param>
                 <xsl:with-param name="current_html">
                   <xsl:value-of select="$popt"/>
                 </xsl:with-param>
               </xsl:call-template>
             </select>
           </xsl:otherwise>
        </xsl:choose>
        </xsl:when>
        <xsl:otherwise>
          <input>
            <xsl:attribute name="type">hidden</xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="concat('last_pid', $counter)"/></xsl:attribute>
            <xsl:attribute name="value"><xsl:value-of select="PRICING_ID"/></xsl:attribute>
          </input>
          <input>
            <xsl:attribute name="type">hidden</xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="concat('pid', $counter)"/></xsl:attribute>
            <xsl:attribute name="value"></xsl:attribute>
         </input>
         <input>
            <xsl:attribute name="type">hidden</xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="concat('ptype', $counter)"/></xsl:attribute>
            <xsl:attribute name="value"></xsl:attribute>
         </input>
         <input>
            <xsl:attribute name="type">hidden</xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="concat('rtype', $counter)"/></xsl:attribute>
            <xsl:attribute name="value"></xsl:attribute>
         </input>
         <input>
            <xsl:attribute name="type">hidden</xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="concat('pr', $counter)"/></xsl:attribute>
            <xsl:attribute name="value"><xsl:value-of select="PRICING_ID"/><xsl:text>|</xsl:text><xsl:value-of select="PRICING_TYPE"/><xsl:text>|</xsl:text><xsl:value-of select="RTYPE"/></xsl:attribute>
         </input>
         <xsl:value-of select="PRICING_NAME"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="PRICING_NAME"/>
    </xsl:otherwise>
  </xsl:choose>
 </td>

 <td class="formField5" nowrap="true" width="7%">
   <xsl:value-of select="PRICE"/>
 </td>

 <td class="formField5" nowrap="true" width="8%">
   <xsl:choose>
     <xsl:when test="RTYPE='LR'">
       <xsl:text>Limited Play</xsl:text>
     </xsl:when>
     <xsl:when test="RTYPE='TR'">
        <xsl:text>Limited Time</xsl:text>
     </xsl:when>
     <xsl:when test="RTYPE='PR'">
        <xsl:text>Permanent</xsl:text>
     </xsl:when>
     <xsl:otherwise>
        <xsl:text>Unknown</xsl:text>
     </xsl:otherwise>
   </xsl:choose>
 </td>

 <td class="formField5" nowrap="true" width="8%">
   <xsl:choose>
     <xsl:when test="RTYPE='LR'">
       <xsl:value-of select="LIMITS"/>
       <xsl:text> Plays</xsl:text>
     </xsl:when>
     <xsl:when test="RTYPE='TR'">
       <xsl:value-of select="LIMITS"/>
        <xsl:text> Minutes</xsl:text>
     </xsl:when>
   </xsl:choose>
 </td>

 <td class="formField5" nowrap="true" width="15%">
   <xsl:value-of select="LAST_UPDATED"/>
 </td>

</tr>

</xsl:template>

<xsl:template name="get-pricing-options">
   <xsl:param name="current_pr"/>
   <xsl:param name="current_name"/>
   <xsl:param name="current_html"/>
   <xsl:variable name="quote">"</xsl:variable>

   <xsl:choose>
   <xsl:when test="contains($current_html, concat('value=',$quote,$current_pr,$quote))">
      <xsl:value-of disable-output-escaping="yes" select="substring-before($current_html, concat('value=',$quote,$current_pr,$quote))"/>
      <xsl:text>value="</xsl:text>
      <xsl:value-of select="$current_pr"/> 
      <xsl:text>" selected="true"</xsl:text>
      <xsl:value-of disable-output-escaping="yes" select="substring-after($current_html, concat('value=',$quote,$current_pr,$quote))"/>
   </xsl:when>
   <xsl:otherwise>
      <xsl:text disable-output-escaping="yes">&lt;option value="</xsl:text>
      <xsl:value-of select="$current_pr"/> 
      <xsl:text disable-output-escaping="yes">" selected="true"&gt;</xsl:text>
      <xsl:value-of select="$current_name"/> 
      <xsl:text disable-output-escaping="yes">&lt;/option&gt;</xsl:text>
      <xsl:value-of disable-output-escaping="yes" select="$current_html"/>
   </xsl:otherwise>
   </xsl:choose>
</xsl:template>

</xsl:stylesheet>

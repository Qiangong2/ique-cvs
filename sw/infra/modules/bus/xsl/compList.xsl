<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW[position() mod 2 = 1]">
   <tr class="oddrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=comp&amp;action=edit&amp;comp_id=</xsl:text>
              <xsl:value-of select="COMPETITION_ID"/>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="COMPETITION_NAME"/>
          </A>    
        </td>
        <td class="smallText2"><xsl:value-of select="COMPETITION_ID"/></td>
	<td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=title&amp;action=edit&amp;titleid=</xsl:text>
              <xsl:value-of select="TITLE_ID"/>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="TITLE"/>
          </A>    
        </td>
	<td class="smallText2">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=compcat&amp;action=list&amp;comp_id=</xsl:text>
              <xsl:value-of select="COMPETITION_ID"/>
            </xsl:attribute>
            <xsl:value-of select="TOTAL_CATEGORIES"/>
          </A>    
        </td>
	<td class="smallText" nowrap="true"><xsl:value-of select="START_DATE"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="END_DATE"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="LAST_UPDATED"/></td>
   </tr>
</xsl:template>

<xsl:template match="ROW">
   <tr class="evenrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=comp&amp;action=edit&amp;comp_id=</xsl:text>
              <xsl:value-of select="COMPETITION_ID"/>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="COMPETITION_NAME"/>
          </A>    
        </td>
        <td class="smallText2"><xsl:value-of select="COMPETITION_ID"/></td>
	<td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=title&amp;action=edit&amp;id=</xsl:text>
              <xsl:value-of select="TITLE_ID"/>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="TITLE"/>
          </A>    
        </td>
	<td class="smallText2">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=compcat&amp;action=list&amp;comp_id=</xsl:text>
              <xsl:value-of select="COMPETITION_ID"/>
            </xsl:attribute>
            <xsl:value-of select="TOTAL_CATEGORIES"/>
          </A>    
        </td>
	<td class="smallText" nowrap="true"><xsl:value-of select="START_DATE"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="END_DATE"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="LAST_UPDATED"/></td>
   </tr>
</xsl:template>

</xsl:stylesheet>

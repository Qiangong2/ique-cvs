<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
	extension-element-prefixes="xalan">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW[position() mod 2 = 1]">
   <tr class="oddrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=title&amp;action=edit&amp;titleid=</xsl:text>
              <xsl:value-of disable-output-escaping="yes" select="TITLE_ID"/>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="TITLE"/>
          </A>    
        </td>
	<td class="smallText2"><xsl:value-of select="TITLE_ID"/></td>
	<td class="smallText"><xsl:value-of disable-output-escaping="yes" select="TITLE_TYPE"/></td>
	<td class="smallText"><xsl:value-of disable-output-escaping="yes" select="CATEGORY"/></td>
	<td class="smallText"><xsl:value-of disable-output-escaping="yes" select="PUBLISHER"/></td>
	<td class="smallText"><xsl:value-of disable-output-escaping="yes" select="DEVELOPER"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="LAST_UPDATED"/></td>
   </tr>
</xsl:template>

<xsl:template match="ROW">
   <tr class="evenrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=title&amp;action=edit&amp;titleid=</xsl:text>
              <xsl:value-of disable-output-escaping="yes" select="TITLE_ID"/>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="TITLE"/>
          </A>    
        </td>
	<td class="smallText2"><xsl:value-of select="TITLE_ID"/></td>
	<td class="smallText"><xsl:value-of disable-output-escaping="yes" select="TITLE_TYPE"/></td>
	<td class="smallText"><xsl:value-of disable-output-escaping="yes" select="CATEGORY"/></td>
	<td class="smallText"><xsl:value-of disable-output-escaping="yes" select="PUBLISHER"/></td>
	<td class="smallText"><xsl:value-of disable-output-escaping="yes" select="DEVELOPER"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="LAST_UPDATED"/></td>
  </tr>
</xsl:template>

</xsl:stylesheet>

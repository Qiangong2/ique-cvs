<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
<tr>
  <td class="formLabel2" nowrap="true">Model:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of disable-output-escaping="yes" select="BB_MODEL"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">Activate Date:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="ACTIVATE_DATE"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">Deactivate Date:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="DEACTIVATE_DATE"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">Revoke Date:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="REVOKE_DATE"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">Replaced Player ID:</td>
  <td class="formField"></td>
  <td class="formField">
    <A class="listText">
      <xsl:attribute name="href">
          <xsl:text>serv?type=player.detail&amp;action=list&amp;id=</xsl:text>
          <xsl:value-of select="REPLACED_BB_ID"/>
      </xsl:attribute>
      <xsl:value-of select="REPLACED_BB_ID"/>
    </A>
  </td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">ETickets Sync Stamp:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="LAST_ETDATE"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">Manufacture Date:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="MANUFACTURE_DATE"/></td>
</tr>
<tr>
  <td class="formLabel2" nowrap="true">Serial Number:</td>
  <td class="formField"></td>
  <td class="formField"><xsl:value-of select="SN"/></td>
</tr>

</xsl:template>

</xsl:stylesheet>

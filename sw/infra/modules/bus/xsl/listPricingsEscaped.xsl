<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
  <xsl:choose>
    <xsl:when test="STATUS='A'">
      <xsl:text disable-output-escaping="yes">&amp;lt;option value="</xsl:text>
      <xsl:value-of select="PRICING_ID"/>
      <xsl:text>|</xsl:text>
      <xsl:value-of select="PRICING_TYPE"/>
      <xsl:text>|</xsl:text>
      <xsl:value-of select="RTYPE"/>
      <xsl:text disable-output-escaping="yes">"&amp;gt;</xsl:text>
      <xsl:value-of disable-output-escaping="yes" select="PRICING_NAME"/>
      <xsl:text disable-output-escaping="yes">&amp;lt;/option&amp;gt;</xsl:text>
    </xsl:when>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:param name="id">-1</xsl:param>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">

<tr><td class="formLabel2" nowrap="true">Category Code:</td><td class="formField"></td>
<td class="formField" nowrap="true">
<xsl:choose>
    <xsl:when test="$id!=120">
        <xsl:value-of disable-output-escaping="yes" select="CATEGORY_CODE"/>
    </xsl:when>
    <xsl:otherwise>
        <xsl:choose>
        <xsl:when test="COMPETITION_ID!=''">
            <input>
               <xsl:attribute name="type">hidden</xsl:attribute>
               <xsl:attribute name="name">cat_code</xsl:attribute>
               <xsl:attribute name="value"><xsl:value-of select="CATEGORY_CODE"/></xsl:attribute>
            </input>
            <xsl:value-of disable-output-escaping="yes" select="CATEGORY_CODE"/>
        </xsl:when>
        <xsl:otherwise>
            <input>
               <xsl:attribute name="type">text</xsl:attribute>
               <xsl:attribute name="size">30</xsl:attribute>
               <xsl:attribute name="name">cat_code</xsl:attribute>
               <xsl:attribute name="value"></xsl:attribute>
            </input>
            <font color="red">*</font>
        </xsl:otherwise>
        </xsl:choose>
    </xsl:otherwise>
</xsl:choose>
</td></tr>

<tr><td class="formLabel2" nowrap="true">Category Name:</td><td class="formField"></td>
<td class="formField" nowrap="true">
<xsl:choose>
    <xsl:when test="$id!=120">
        <xsl:value-of disable-output-escaping="yes" select="CATEGORY_NAME"/>
    </xsl:when>
    <xsl:otherwise>
        <input>
            <xsl:attribute name="type">text</xsl:attribute>
            <xsl:attribute name="size">30</xsl:attribute>
            <xsl:attribute name="name">cat_name</xsl:attribute>
            <xsl:attribute name="value"><xsl:value-of disable-output-escaping="yes" select="CATEGORY_NAME"/></xsl:attribute>
        </input>
        <font color="red">*</font>
    </xsl:otherwise>
</xsl:choose>
</td></tr>

<tr><td class="formLabel2" nowrap="true">Start Date:</td><td class="formField"></td>
<td class="formField" nowrap="true">
<xsl:choose>
    <xsl:when test="$id!=120">
        <xsl:value-of disable-output-escaping="yes" select="START_DATE"/>
    </xsl:when>
    <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="not(START_DATE) or START_FLAG!='0'">
            <input>
              <xsl:attribute name="type">text</xsl:attribute>
              <xsl:attribute name="name">start</xsl:attribute>
              <xsl:attribute name="size">19</xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="START_DATE"/></xsl:attribute>
              <xsl:attribute name="onfocus"><xsl:value-of select="concat('this.blur();popUpCalendar(this, theForm.start, ', &quot;'&quot;, 'yyyy.mm.dd', &quot;'&quot;, ', ', &quot;'&quot;, 'start', &quot;'&quot;, ')')"/></xsl:attribute>
            </input>
            <xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;</xsl:text>
            <img>
              <xsl:attribute name="src">/images/date.gif</xsl:attribute>
              <xsl:attribute name="onClick"><xsl:value-of select="concat('popUpCalendar(this, theForm.start, ', &quot;'&quot;, 'yyyy.mm.dd',&quot;'&quot;, ', ', &quot;'&quot;, 'start', &quot;'&quot;, ')')"/></xsl:attribute>
            </img>
            <font color="red"> *</font>
          </xsl:when>
          <xsl:otherwise>
            <input>
              <xsl:attribute name="type">hidden</xsl:attribute>
              <xsl:attribute name="name">start</xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="START_DATE"/></xsl:attribute>
            </input>
   	    <xsl:value-of select="START_DATE"/>
          </xsl:otherwise>
        </xsl:choose>
    </xsl:otherwise>
</xsl:choose>
</td></tr>

<tr><td class="formLabel2" nowrap="true">End Date:</td><td class="formField"></td>
<td class="formField" nowrap="true">
<xsl:choose>
    <xsl:when test="$id!=120">
         <xsl:value-of disable-output-escaping="yes" select="END_DATE"/>
    </xsl:when>
    <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="not(END_DATE) or END_FLAG!='0'">
            <input>
              <xsl:attribute name="type">text</xsl:attribute>
              <xsl:attribute name="name">end</xsl:attribute>
              <xsl:attribute name="size">19</xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="END_DATE"/></xsl:attribute>
              <xsl:attribute name="onfocus"><xsl:value-of select="concat('this.blur();popUpCalendar(this, theForm.end, ', &quot;'&quot;, 'yyyy.mm.dd', &quot;'&quot;, ', ', &quot;'&quot;, 'end', &quot;'&quot;, ')')"/></xsl:attribute>
            </input>
            <xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;</xsl:text>
            <img>
              <xsl:attribute name="src">/images/date.gif</xsl:attribute>
              <xsl:attribute name="onClick"><xsl:value-of select="concat('popUpCalendar(this, theForm.end, ', &quot;'&quot;, 'yyyy.mm.dd',&quot;'&quot;, ', ', &quot;'&quot;, 'end', &quot;'&quot;, ')')"/></xsl:attribute>
            </img>
          </xsl:when>
          <xsl:otherwise>
            <input>
              <xsl:attribute name="type">hidden</xsl:attribute>
              <xsl:attribute name="name">end</xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="END_DATE"/></xsl:attribute>
            </input>
   	    <xsl:value-of select="END_DATE"/>
          </xsl:otherwise>
        </xsl:choose>
    </xsl:otherwise>
</xsl:choose>
</td></tr>

<xsl:choose>
<xsl:when test="COMPETITION_ID!=''">                         
    <tr><td class="formLabel2" nowrap="true">Last Updated:</td><td class="formField"></td>
    <td class="formField" nowrap="true">
	<xsl:value-of select="LAST_UPDATED"/>
    </td></tr> 
</xsl:when>
</xsl:choose>

<xsl:choose>
  <xsl:when test="$id!=120">
     <tr><td class="formLabel2" nowrap="true">City Code:</td><td class="formField"></td>
         <td class="formField" nowrap="true">
	   <xsl:value-of disable-output-escaping="yes" select="CITY_CODE"/>
         </td>
     </tr> 
  </xsl:when>
</xsl:choose>

</xsl:template>

</xsl:stylesheet>

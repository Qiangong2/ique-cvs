<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW[position() mod 2 = 1]">
<xsl:variable name="title_prev">
        <xsl:value-of select="preceding-sibling::ROW[1]/TITLE_ID"/>
</xsl:variable>
<xsl:variable name="title_next">
        <xsl:value-of select="following-sibling::ROW/TITLE_ID"/>
</xsl:variable>

 <tr class="oddrow">
  <td class="smallText" nowrap="true">
    <xsl:choose>
      <xsl:when test="TITLE_ID!=$title_prev">
         <A class="listText">
          <xsl:attribute name="href">
           <xsl:text>serv?type=ctr&amp;action=edit&amp;titleid=</xsl:text>
           <xsl:value-of select="TITLE_ID"/>
          </xsl:attribute>
          <xsl:value-of disable-output-escaping="yes" select="TITLE"/>
         </A>
      </xsl:when>
    </xsl:choose>
  </td>
  <td class="smallText" nowrap="true">
    <xsl:choose>
      <xsl:when test="TITLE_ID!=$title_prev">
        <xsl:value-of select="TRANS_DATE"/>
      </xsl:when>
    </xsl:choose>
  </td>
  <td class="smallText2" nowrap="true">
    <xsl:choose>
      <xsl:when test="TITLE_ID!=$title_prev">
        <xsl:value-of select="PRICE"/>
      </xsl:when>
    </xsl:choose>
  </td>
  <td class="smallText2" nowrap="true">
    <xsl:choose>
      <xsl:when test="TITLE_ID!=$title_prev">
        <xsl:value-of select="PAID"/>
      </xsl:when>
    </xsl:choose>
  </td>
  <td class="smallText2" nowrap="true">
    <xsl:choose>
      <xsl:when test="TITLE_ID!=$title_prev">
         <A class="listText">
          <xsl:attribute name="href">
           <xsl:text>serv?type=store&amp;action=edit&amp;id=</xsl:text>
           <xsl:value-of select="STORE_ID"/>
           <xsl:text>&amp;rcid=</xsl:text>
           <xsl:value-of select="REGIONAL_CENTER_ID"/>
           <xsl:text>&amp;rgid=</xsl:text>
           <xsl:value-of select="REGION_ID"/>
           <xsl:text>&amp;rlid=</xsl:text>
           <xsl:value-of select="RETAILER_ID"/>
          </xsl:attribute>
          <xsl:value-of select="STORE_ID"/>
         </A>
      </xsl:when>
    </xsl:choose>
  </td>
  <td class="smallText2" nowrap="true">
    <xsl:choose>
      <xsl:when test="TITLE_ID!=$title_prev">
        <xsl:value-of select="ADDRESS"/>
      </xsl:when>
    </xsl:choose>
  </td>
  <td class="smallText" nowrap="true">
         <A class="listText">
          <xsl:attribute name="href">
           <xsl:text>serv?type=ec&amp;action=list&amp;id=</xsl:text>
           <xsl:value-of select="ECARD_ID"/>
           <xsl:text>&amp;ecaction=query</xsl:text>
          </xsl:attribute>
          <xsl:value-of select="ECARD_ID"/>
         </A>
  </td>
  <td class="smallText2" nowrap="true">
    <xsl:value-of select="EUNITS"/>
  </td>
 </tr>

</xsl:template>

<xsl:template match="ROW">
<xsl:variable name="title_prev">
        <xsl:value-of select="preceding-sibling::ROW[1]/TITLE_ID"/>
</xsl:variable>
<xsl:variable name="title_next">
        <xsl:value-of select="following-sibling::ROW/TITLE_ID"/>
</xsl:variable>

 <tr class="evenrow">
  <td class="smallText" nowrap="true">
    <xsl:choose>
      <xsl:when test="TITLE_ID!=$title_prev">
         <A class="listText">
          <xsl:attribute name="href">
           <xsl:text>serv?type=ctr&amp;action=edit&amp;titleid=</xsl:text>
           <xsl:value-of select="TITLE_ID"/>
          </xsl:attribute>
          <xsl:value-of disable-output-escaping="yes" select="TITLE"/>
         </A>
      </xsl:when>
    </xsl:choose>
  </td>
  <td class="smallText" nowrap="true">
    <xsl:choose>
      <xsl:when test="TITLE_ID!=$title_prev">
        <xsl:value-of select="TRANS_DATE"/>
      </xsl:when>
    </xsl:choose>
  </td>
  <td class="smallText2" nowrap="true">
    <xsl:choose>
      <xsl:when test="TITLE_ID!=$title_prev">
        <xsl:value-of select="PRICE"/>
      </xsl:when>
    </xsl:choose>
  </td>
  <td class="smallText2" nowrap="true">
    <xsl:choose>
      <xsl:when test="TITLE_ID!=$title_prev">
        <xsl:value-of select="PAID"/>
      </xsl:when>
    </xsl:choose>
  </td>
  <td class="smallText2" nowrap="true">
    <xsl:choose>
      <xsl:when test="TITLE_ID!=$title_prev">
         <A class="listText">
          <xsl:attribute name="href">
           <xsl:text>serv?type=store&amp;action=edit&amp;id=</xsl:text>
           <xsl:value-of select="STORE_ID"/>
           <xsl:text>&amp;rcid=</xsl:text>
           <xsl:value-of select="REGIONAL_CENTER_ID"/>
           <xsl:text>&amp;rgid=</xsl:text>
           <xsl:value-of select="REGION_ID"/>
           <xsl:text>&amp;rlid=</xsl:text>
           <xsl:value-of select="RETAILER_ID"/>
          </xsl:attribute>
          <xsl:value-of select="STORE_ID"/>
         </A>
      </xsl:when>
    </xsl:choose>
  </td>
  <td class="smallText2" nowrap="true">
    <xsl:choose>
      <xsl:when test="TITLE_ID!=$title_prev">
        <xsl:value-of select="ADDRESS"/>
      </xsl:when>
    </xsl:choose>
  </td>
  <td class="smallText" nowrap="true">
         <A class="listText">
          <xsl:attribute name="href">
           <xsl:text>serv?type=ec&amp;action=list&amp;id=</xsl:text>
           <xsl:value-of select="ECARD_ID"/>
           <xsl:text>&amp;ecaction=query</xsl:text>
          </xsl:attribute>
          <xsl:value-of select="ECARD_ID"/>
         </A>
  </td>
  <td class="smallText2" nowrap="true">
    <xsl:value-of select="EUNITS"/>
  </td>
 </tr>

</xsl:template>

</xsl:stylesheet>


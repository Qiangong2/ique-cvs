<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">
<tr>
  <td class="smallText2"><xsl:value-of select="TITLE"/></td>
  <td class="smallText2"><xsl:value-of select="BB_ID"/></td>
  <td class="smallText2"><xsl:value-of select="RETAILER_NAME"/></td>
  <td class="smallText2"><xsl:value-of select="REGION_NAME"/></td>
</tr>

</xsl:template>

</xsl:stylesheet>

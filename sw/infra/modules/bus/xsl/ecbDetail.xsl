<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:param name="id">-1</xsl:param>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW">

<tr>
  <td class="formLabel2" nowrap="true">Start Game Ticket ID:</td>
  <td class="formField">
     <input>
       	<xsl:attribute name="type">hidden</xsl:attribute>
        <xsl:attribute name="name">start</xsl:attribute>
       	<xsl:attribute name="value"><xsl:value-of select="START_ECARD_ID"/></xsl:attribute>
     </input>
  </td>
  <td class="formField" nowrap="true"><xsl:value-of select="START_ECARD_ID"/></td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">End Game Ticket ID:</td>
  <td class="formField">
     <input>
       	<xsl:attribute name="type">hidden</xsl:attribute>
        <xsl:attribute name="name">end</xsl:attribute>
       	<xsl:attribute name="value"><xsl:value-of select="END_ECARD_ID"/></xsl:attribute>
     </input>
  </td>
  <td class="formField" nowrap="true"><xsl:value-of select="END_ECARD_ID"/></td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">Game Ticket Type:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true">
     <xsl:value-of select="ECARD_TYPE"/>
     <xsl:choose>
         <xsl:when test="DESCRIPTION!=''">
             <xsl:text> (</xsl:text>
             <xsl:value-of disable-output-escaping="yes" select="DESCRIPTION"/>
             <xsl:text>)</xsl:text>
         </xsl:when>
     </xsl:choose>
  </td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">Create Date:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true"><xsl:value-of select="CREATE_DATE"/></td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">Print Date:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true"><xsl:value-of select="PRINT_DATE"/></td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">Ship Date:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true"><xsl:value-of select="SHIP_DATE"/></td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">Upload Date:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true"><xsl:value-of select="UPLOAD_DATE"/></td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">Expire Date:</td>
  <td class="formField"></td>
  <td class="formField" nowrap="true"><xsl:value-of select="EXPIRE_DATE"/></td>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">Activate Date:</td>
  <td class="formField"></td>
  <xsl:choose>
    <xsl:when test="$id!=130">
     <xsl:choose>
      <xsl:when test="ADATE!='' and ADATE&lt;=CDATE">
          <td class="formField" nowrap="true">
             <xsl:value-of select="ACTIVATE_DATE"/>
             <input>
                <xsl:attribute name="type">hidden</xsl:attribute>
                <xsl:attribute name="name">activate</xsl:attribute>
                <xsl:attribute name="value"><xsl:value-of select="ACTIVATE_DATE"/></xsl:attribute>
             </input>
         </td>
      </xsl:when>
      <xsl:otherwise>
          <td class="formField" nowrap="true">
	   <input>
	    <xsl:attribute name="type">hidden</xsl:attribute>
	    <xsl:attribute name="name">current</xsl:attribute>
	    <xsl:attribute name="value"><xsl:value-of select="CURRENT_DATE"/></xsl:attribute>
	   </input>
           <input>
            <xsl:attribute name="type">text</xsl:attribute>
            <xsl:attribute name="name">activate</xsl:attribute>
            <xsl:attribute name="size">19</xsl:attribute>
            <xsl:attribute name="value"><xsl:value-of select="ACTIVATE_DATE"/></xsl:attribute>
            <xsl:attribute name="onfocus"><xsl:value-of select="concat('this.blur();popUpCalendar(this, theForm.activate, ', &quot;'&quot;, 'yyyy.mm.dd', &quot;'&quot;, ', ', &quot;'&quot;, 'start', &quot;'&quot;, ')')"/></xsl:attribute>
           </input>
           <xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;</xsl:text>
           <img>
             <xsl:attribute name="src">/images/date.gif</xsl:attribute>
             <xsl:attribute name="onClick"><xsl:value-of select="concat('popUpCalendar(this, theForm.activate, ', &quot;'&quot;, 'yyyy.mm.dd', &quot;'&quot;, ', ', &quot;'&quot;, 'start', &quot;'&quot;, ')')"/></xsl:attribute>
           </img>
           <font color="red"> *</font>
          </td>
      </xsl:otherwise>
     </xsl:choose>
    </xsl:when>
    <xsl:otherwise>
        <td class="formField" nowrap="true"><xsl:value-of select="ACTIVATE_DATE"/></td>
    </xsl:otherwise>
  </xsl:choose>
</tr>

<tr>
  <td class="formLabel2" nowrap="true">Revoke Date:</td>
  <td class="formField"></td>
  <xsl:choose>
    <xsl:when test="$id!=130">
     <xsl:choose>
      <xsl:when test="RDATE!='' and RDATE&lt;=CDATE">
         <td class="formField" nowrap="true">
             <xsl:value-of select="REVOKE_DATE"/>
             <input>
                <xsl:attribute name="type">hidden</xsl:attribute>
                <xsl:attribute name="name">revoke</xsl:attribute>
                <xsl:attribute name="value"><xsl:value-of select="REVOKE_DATE"/></xsl:attribute>
             </input>
         </td>
      </xsl:when>
      <xsl:otherwise>
          <td class="formField" nowrap="true">
           <input>
            <xsl:attribute name="type">text</xsl:attribute>
            <xsl:attribute name="name">revoke</xsl:attribute>
            <xsl:attribute name="size">19</xsl:attribute>
            <xsl:attribute name="value"><xsl:value-of select="REVOKE_DATE"/></xsl:attribute>
            <xsl:attribute name="onfocus"><xsl:value-of select="concat('this.blur();popUpCalendar(this, theForm.revoke, ', &quot;'&quot;, 'yyyy.mm.dd', &quot;'&quot;, ', ', &quot;'&quot;, 'start', &quot;'&quot;, ')')"/></xsl:attribute>
           </input>
           <xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;</xsl:text>
           <img>
             <xsl:attribute name="src">/images/date.gif</xsl:attribute>
             <xsl:attribute name="onClick"><xsl:value-of select="concat('popUpCalendar(this, theForm.revoke, ', &quot;'&quot;, 'yyyy.mm.dd', &quot;'&quot;, ', ', &quot;'&quot;, 'start', &quot;'&quot;, ')')"/></xsl:attribute>
           </img>
          </td>
      </xsl:otherwise>
     </xsl:choose>
    </xsl:when>
    <xsl:otherwise>
        <td class="formField" nowrap="true"><xsl:value-of select="REVOKE_DATE"/></td>
    </xsl:otherwise>
  </xsl:choose>
</tr>

</xsl:template>

</xsl:stylesheet>

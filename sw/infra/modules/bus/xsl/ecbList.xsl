<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW[position() mod 2 = 1]">
   <tr class="oddrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=ecb&amp;action=edit&amp;start=</xsl:text>
              <xsl:value-of select="START_ECARD_ID"/>
              <xsl:text>&amp;end=</xsl:text>
              <xsl:value-of select="END_ECARD_ID"/>
            </xsl:attribute>
            <xsl:value-of select="START_ECARD_ID"/>
            <xsl:text> - </xsl:text>
            <xsl:value-of select="END_ECARD_ID"/>
          </A>
        </td>
        <td class="smallText"><xsl:value-of select="END_ECARD_ID - START_ECARD_ID + 1"/></td>
        <td class="smallText">
            <xsl:value-of select="ECARD_TYPE"/>
            <xsl:choose>
                <xsl:when test="DESCRIPTION!=''">
                    <xsl:text> (</xsl:text>
                    <xsl:value-of disable-output-escaping="yes" select="DESCRIPTION"/>
                    <xsl:text>)</xsl:text>
                </xsl:when>
            </xsl:choose>
        </td>
        <td class="smallText" nowrap="true"><xsl:value-of select="CREATE_DATE"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="ACTIVATE_DATE"/></td>        
	<td class="smallText" nowrap="true"><xsl:value-of select="REVOKE_DATE"/></td>        
   </tr>
</xsl:template>

<xsl:template match="ROW">
   <tr class="evenrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=ecb&amp;action=edit&amp;start=</xsl:text>
              <xsl:value-of select="START_ECARD_ID"/>
              <xsl:text>&amp;end=</xsl:text>
              <xsl:value-of select="END_ECARD_ID"/>
            </xsl:attribute>
            <xsl:value-of select="START_ECARD_ID"/>
            <xsl:text> - </xsl:text>
            <xsl:value-of select="END_ECARD_ID"/>
          </A>
        </td>
        <td class="smallText"><xsl:value-of select="END_ECARD_ID - START_ECARD_ID + 1"/></td>
        <td class="smallText">
            <xsl:value-of select="ECARD_TYPE"/>
            <xsl:choose>
                <xsl:when test="DESCRIPTION!=''">
                    <xsl:text> (</xsl:text>
                    <xsl:value-of disable-output-escaping="yes" select="DESCRIPTION"/>
                    <xsl:text>)</xsl:text>
                </xsl:when>
            </xsl:choose>
        </td>
        <td class="smallText" nowrap="true"><xsl:value-of select="CREATE_DATE"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="ACTIVATE_DATE"/></td>        
	<td class="smallText" nowrap="true"><xsl:value-of select="REVOKE_DATE"/></td>        
   </tr>
</xsl:template>

</xsl:stylesheet>

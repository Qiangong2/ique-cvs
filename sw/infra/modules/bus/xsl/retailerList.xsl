<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

<xsl:template match="/ROWSET">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ROW[position() mod 2 = 1]">
   <tr class="oddrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=retailer&amp;action=edit&amp;id=</xsl:text>
              <xsl:value-of select="RETAILER_ID"/>
              <xsl:text>&amp;code=</xsl:text>
              <xsl:value-of select="CATEGORY_CODE"/>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="RETAILER_NAME"/>
          </A>    
        </td>
	<td class="smallText2"><xsl:value-of select="RETAILER_ID"/></td>
        <td class="smallText" nowrap="true"><xsl:value-of select="CATEGORY_NAME"/></td>
        <td class="smallText2"><xsl:value-of select="CATEGORY_CODE"/></td>
        <td class="smallText2">
          <xsl:choose>
            <xsl:when test="TOTAL_STORES>0">
              <A class="listText">
                <xsl:attribute name="href">
                  <xsl:text>serv?type=store.retailer&amp;action=list&amp;rlid=</xsl:text>
                  <xsl:value-of select="RETAILER_ID"/>
                </xsl:attribute>
                <xsl:value-of select="TOTAL_STORES"/>
              </A>    
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="TOTAL_STORES"/>
            </xsl:otherwise>
          </xsl:choose>
        </td>
        <td class="smallText" nowrap="true"><xsl:value-of select="CREATE_DATE"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="REVOKE_DATE"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="SUSPEND_DATE"/></td>    
   </tr>
</xsl:template>

<xsl:template match="ROW">
   <tr class="evenrow">
        <td class="smallText2"><xsl:value-of select="NO"/></td>
        <td class="smallText" nowrap="true">
          <A class="listText">
            <xsl:attribute name="href">
              <xsl:text>serv?type=retailer&amp;action=edit&amp;id=</xsl:text>
              <xsl:value-of select="RETAILER_ID"/>
              <xsl:text>&amp;code=</xsl:text>
              <xsl:value-of select="CATEGORY_CODE"/>
            </xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="RETAILER_NAME"/>
          </A>    
        </td>
	<td class="smallText2"><xsl:value-of select="RETAILER_ID"/></td>
        <td class="smallText" nowrap="true"><xsl:value-of select="CATEGORY_NAME"/></td>
        <td class="smallText2"><xsl:value-of select="CATEGORY_CODE"/></td>
        <td class="smallText2">
          <xsl:choose>
            <xsl:when test="TOTAL_STORES>0">
              <A class="listText">
                <xsl:attribute name="href">
                  <xsl:text>serv?type=store.retailer&amp;action=list&amp;rlid=</xsl:text>
                  <xsl:value-of select="RETAILER_ID"/>
                </xsl:attribute>
                <xsl:value-of select="TOTAL_STORES"/>
              </A>    
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="TOTAL_STORES"/>
            </xsl:otherwise>
          </xsl:choose>
        </td>
        <td class="smallText" nowrap="true"><xsl:value-of select="CREATE_DATE"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="REVOKE_DATE"/></td>
	<td class="smallText" nowrap="true"><xsl:value-of select="SUSPEND_DATE"/></td>    
  </tr>
</xsl:template>

</xsl:stylesheet>

<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.ms.common.QueryBean" %>
<jsp:useBean class="com.broadon.ms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  Object role = session.getAttribute("BCC_ROLE");
  String ROLE_BU_ECARD = "180";
  String ROLE_BU_CS = "130";
  String ROLE_BU_SSUP = "200";

  String err = request.getAttribute("ERROR").toString();
  String success = request.getAttribute("SUCCESS").toString();
  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  String[] params = qb.getParamArray();

  String ecStr = "";
  String tiStr = "";
  String id = "";
  String ecid = "";
  

  if (htmlResults!= null) {
      ecStr = htmlResults[0];
	  tiStr = htmlResults[1];
      if (ecStr != null)
          ecStr = ecStr.trim();
	  if (tiStr != null)
	      tiStr = tiStr.trim();
  }
  
  if (params!=null) {
      id = params[0];
      if (id == null)
          id = "";
  }

  if (id!=null && !id.equals("")) {
      ecid = id;
  }
%>

<jsp:include page="bu-header.jsp" flush="true">
    <jsp:param name="page" value="ecInfo"/>
</jsp:include>

<%if (err!=null && err!="") {%>
  <center><font class="errorText"><%=err%></font></center><p>
<%} else if (success!=null && success!="") {%>
  <center><font class="successText"><%=success%></font></center><p>
<%}%>

<form name="theForm" id="theForm" action="serv" method="POST" onSubmit="return false;">
<input type="hidden" name="type" value=""></input>
<input type="hidden" name="action" value=""></input>
<input type="hidden" name="time" value=""></input>

<TABLE cellSpacing=0 cellPadding=1 width=30% align=center bgColor="#336699" border=0>
  <TR>
    <TD>
      <!-- Game Ticket Search Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699>
          <TD width="100%">
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              <TR>
                <TD width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2"><%=session.getAttribute("TEXT_ECARD_SEARCH")%></FONT></TD>
              </TR>
             </TABLE>
          </TD>
        </TR>
        <TR>
          <TD bgColor=#efefef>
	    <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
	      <TR><TD><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
	      <TR>
	          <TD align="right"><B><%=session.getAttribute("TEXT_ECARD_ID")%></B></TD>
                  <TD align="left"><INPUT type="text" name="id" size="16" value="<%=id%>"><font color="red">*</font></TD>
	      </TR>
	      <TR><TD><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
	    </TABLE>
	    <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
	      <TR>
	         <TD bgcolor="white">
  	            <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
  	              <TR><TD bgcolor="white">
	                  <CENTER><FONT color="red" size="-1">* Required</FONT></CENTER>
	                  </TD>
	              </TR>
	              <TR><TD bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
          	      <TR>
	                <TD bgcolor="white">
	                  <CENTER>
	                  <INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_QUERY")%>" OnClick="onClickSubmit(theForm);">
	                  <INPUT class="sbutton" type="reset" value="<%=session.getAttribute("BUTTON_RESET")%>">
	                  <INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_CANCEL")%>" OnClick="history.go(-1)">
	                  </CENTER>
        	       </TD>
		     </TR>
	           </TABLE>
	         </TD>
	      </TR>
	      <TR><TD bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
	    </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>

<%if (ecStr!=null && !ecStr.equals("")) {%>
<BR>
<P>
<TABLE cellSpacing=0 cellPadding=1 width=95% align=center bgColor="#336699" border=0>
  <TR> 
    <TD> 
      <!-- ECard Detail Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699> 
          <TD width="100%"> 
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              <TR> 
                <TD width="70%" align="left" bgColor=#336699><FONT class="tblSubHdrLabel2"><%=session.getAttribute("TEXT_ECARD")%><%=ecid%></FONT></TD>                
              </TR>
             </TABLE>
          </TD>
        </TR>

        <TR> 
          <TD bgColor=#efefef>
            <TABLE bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
	      <TR>
                <TD class="formLabel4" width=100% colspan=5><%=session.getAttribute("TEXT_ECINFO_PURCHASE")%></TD>
              </TR>
              <TR>
                <TD class="tableheader" align=center><%=session.getAttribute("COL_TITLE")%></TD>
                <TD class="tableheader" align=center><%=session.getAttribute("COL_STORE_NAME")%></TD>
                <TD class="tableheader" align=center><%=session.getAttribute("COL_TRANS_DATE")%></TD>
	        <TD class="tableheader" align=center><%=session.getAttribute("COL_EUNITS_REDEEMED")%></TD>
                <TD class="tableheader" align=center><%=session.getAttribute("COL_EXT_SN")%></TD>
              </TR>
              <%=tiStr%>
            </TABLE>
          </TD>
        </TR>

	<TR>
          <TD bgColor=#efefef>
            <TABLE bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
	      <TR>
                <TD class="formLabel4" width=100% colspan=3><%=session.getAttribute("TEXT_ECINFO_ECARD")%></TD>
              </TR>
              <TR>
                <TD class="tableheader" align=center width=50%><%=session.getAttribute("COL_PARAMETER")%></TD>
                <TD class="tableheader" align=center width=50% colspan=2><%=session.getAttribute("COL_VALUE")%></TD>
              </TR>
              <%=ecStr%>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
<%} else if (id!=null && !id.equals("")) {%>
<BR><BR>
<P>
<CENTER>
   <%=session.getAttribute("TEXT_RESULT")%>:&nbsp;<%=session.getAttribute("TEXT_EMPTY")%>
</CENTER>
<%}%>
</FORM name="theForm">
<jsp:include page="footer.jsp" flush="true"/>


<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.ms.common.QueryBean" %>
<jsp:useBean class="com.broadon.ms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  Object role = session.getAttribute("BCC_ROLE");
  String ROLE_BU_MKTG = "120";

  String err = request.getAttribute("ERROR").toString();
  String success = request.getAttribute("SUCCESS").toString();

  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  String[] params = qb.getParamArray();
  int[] counts = qb.getCountArray();

  String regionStr = htmlResults[0];
  String pricingStr = htmlResults[1];
  String ctrpStr = htmlResults[4];

  String titleid = params[0];
  String titleName = params[1];
  String regionid = params[2];
  String regionName = params[3];

  int ctrpCount = counts[0];

  if (regionid!=null && !regionid.equals(""))
      ctrpCount = ctrpCount+1;

  if (regionStr!=null) regionStr = regionStr.trim();
  if (ctrpStr!=null) ctrpStr = ctrpStr.trim();
  if (pricingStr!=null) pricingStr = pricingStr.trim();

  String reqStr = "<font color=\"red\">*</font>";
%>

<jsp:include page="bu-header.jsp" flush="true">
   <jsp:param name="page" value="ctrpEdit"/>
</jsp:include>

<%if (err!=null && err!="") {%>
  <center><font class="errorText"><%=err%></font></center><p>
<%} else if (success!=null && success!="") {%>
  <center><font class="successText"><%=success%></font></center><p>
<%}%>

<form name="theForm" id="theForm" action="serv" method="POST" onSubmit="return false;">
<input type="hidden" name="type" value="ctrp"></input>
<input type="hidden" name="action" value=""></input>
<input type="hidden" name="records" value="<%=ctrpCount%>"></input>
<input type="hidden" name="tid" value="<%=titleid%>"></input>

<br>
<P>
<TABLE cellSpacing=0 cellPadding=1 width=30% align=center bgColor="#336699" border=0>
  <TR> 
    <TD> 
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR>
          <TD bgColor=#efefef>
            <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
              <TR><TD><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
              <TR>
                  <TD align="right"><B><%=session.getAttribute("COL_CTRP_TITLE")%>:</B></TD>
                  <TD align="left"><%=titleName%></TD>
              </TR>
              <TR>
                  <TD align="right"><B><%=session.getAttribute("TEXT_CTRP_SELECT_REGION")%></B></TD>
                  <TD align="left">
                    <select name="rgid" onChange="showDetails(theForm)">
                    <option value=""></option>
                    <%=regionStr%>
                    </select>
                  </TD>   
              </TR>
              <TR><TD><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>

<%if (regionid!=null && !regionid.equals("")) {%>
<BR>
<P>
<TABLE cellSpacing=0 cellPadding=1 width=90% align=center bgColor="#336699" border=0>
  <TR> 
    <TD> 
      <!-- Content Title Region Pricings Detail Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699> 
          <TD width="100%"> 
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              <TR>
                <TD width="50%" align="left" bgColor=#336699><FONT class="tblSubHdrLabel2"><%=session.getAttribute("TEXT_CTRP_TITLE_DETAIL")%>&nbsp;<%=titleName%></FONT></TD>
                <TD width="50%" align="right" bgColor=#336699><FONT class="tblSubHdrLabel2"><%=session.getAttribute("TEXT_CTRP_REGION_DETAIL")%>:&nbsp;<%=regionName%></FONT></TD>
              </TR>
             </TABLE>
          </TD>
        </TR>
        <TR> 
          <TD bgColor=#efefef>
            <TABLE bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
                <TR>
                  <TD class="tableheader" align=left width="2%"></TD>
                  <TD class="tableheader" nowrap="true" align=left width="15%"><%=session.getAttribute("COL_CTRP_START_DATE")%>
                   <%if (role!=null && role.toString().equals(ROLE_BU_MKTG)) {%>
                     <%=reqStr%>
                   <%}%>
                  </TD>
                  <TD class="tableheader" nowrap="true" align=left width="15%"><%=session.getAttribute("COL_CTRP_END_DATE")%></TD>
                  <TD class="tableheader" nowrap="true" align=left width="30%"><%=session.getAttribute("COL_CTRP_PRICING")%>
                   <%if (role!=null && role.toString().equals(ROLE_BU_MKTG)) {%>
                     <%=reqStr%>
                   <%}%>
                  </TD>
                  <TD class="tableheader" nowrap="true" align=left width="7%"><%=session.getAttribute("COL_CTRP_PRICE")%></TD>
                  <TD class="tableheader" nowrap="true" align=left width="8%"><%=session.getAttribute("COL_CTRP_RTYPE")%></TD>
                  <TD class="tableheader" nowrap="true" align=left width="8%"><%=session.getAttribute("COL_CTRP_LIMITS")%></TD>
                  <TD class="tableheader" nowrap="true" align=left width="15%"><%=session.getAttribute("COL_CTRP_LAST_UPDATED")%></TD>
                </TR>
                <%if (ctrpStr != null && !ctrpStr.equals("")) {%>
                  <%=ctrpStr%>
                <%} else {%>
                 <TR><TD colspan=8 bgcolor=#D8E8F5><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
                 <TR><TD align=center colspan=8 bgcolor=#D8E8F5><%=session.getAttribute("TEXT_NO_CTRP")%></TD></TR>
                 <TR><TD colspan=8 bgcolor=#D8E8F5><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
                <%}%>
                
                <%if (role!=null && role.toString().equals(ROLE_BU_MKTG)) {%>
		 <!-- Add Record -->
                <TR><TD colspan="8" bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
                <TR><TD align=center colspan=8 bgcolor="white"><b><%=session.getAttribute("TEXT_ADD_NEW_CTRP")%></b></TD></TR>
                <TR><TD colspan="8" bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
		<TR class="evenrow">
                  <td nowrap="true" width="2%"><input type="checkbox" name="region1" value="<%=regionid%>" onClick="toggleCheckBox(theForm,'1',this.checked)"></input></td>
                  <td nowrap="true" width="15%">
                     <input type="hidden" name="last_start1" value=""></input>
                     <input type="hidden" name="last_end1" value=""></input>
                     <input type="hidden" name="last_pid1" value=""></input>
                     <input type="hidden" name="pid1" value=""></input>
                     <input type="hidden" name="ptype1" value=""></input>
                     <input type="hidden" name="rtype1" value=""></input>
                     <input type="text" name="start1" size="19" value="" onfocus="this.blur();popUpCalendar(theForm.start1, theForm.start1, 'yyyy.mm.dd', 'start')"></input>&nbsp;&nbsp;
                       <img src="/images/date.gif" onClick="popUpCalendar(theForm.start1, theForm.start1, 'yyyy.mm.dd', 'start')">
                  </td>
                  <td nowrap="true" width="15%">
                     <input type="text" name="end1" size="19" value="" onfocus="this.blur();onClickNonPermanentEndDate(theForm)">&nbsp;&nbsp;
                       <img src="/images/date.gif" onClick="onClickNonPermanentEndDate(theForm)">
                  </td>
                  <td nowrap="true" width="30%">
                     <select name="pr1" onChange="setNonPermanentEndDate(theForm)">
                       <option value=""></option>
                       <%=pricingStr%>
                     </select>
                  </td>
                  <td nowrap="true" colspan=4><a href="serv?type=ctp&action=add"><%=session.getAttribute("TEXT_ADD_NEW_CTP")%></a></td>
                </TR>
                <TR><TD colspan="8" bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
                <TR><TD colspan="8" bgcolor="white"><CENTER><FONT color="red" size="-1">* Required</FONT></CENTER></TD></TR>
                <%}%>
                <TR><TD colspan="8" bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
                <TR>
                  <TD colspan="8" bgcolor="white">
                  <CENTER>
                  <%if (role!=null && role.toString().equals(ROLE_BU_MKTG)) {%>
                   <INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_SUBMIT")%>" OnClick="onClickSubmit(theForm);">
                   <INPUT class="sbutton" type="reset" value="<%=session.getAttribute("BUTTON_RESET")%>">
                  <%}%>
                <INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_CANCEL")%>" OnClick="history.go(-1)">
                </CENTER>
                </TD>
               </TR>
               <TR><TD colspan="8" bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
<%}%>
</FORM name="theForm">

<jsp:include page="footer.jsp" flush="true"/>


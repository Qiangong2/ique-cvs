<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.ms.common.QueryBean" %>

<jsp:useBean class="com.broadon.ms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();
  String success = request.getAttribute("SUCCESS").toString();
  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  int[] counts = qb.getCountArray();
  int iTotalCount = counts[0];

  String sort = qb.getSort();
  sort = sort.toLowerCase();
  String statusStr = "";
  
  String[] aHeader = {"COL_CDS_HR_ID", "COL_CDS_ADDRESS", "COL_CDS_TO_DOWNLOAD", "COL_CDS_DOWNLOADED", "COL_LAST_UPDATED"};
  String[] aSort = {"hr_id", "address", "total_to_download", "total_downloaded", "last_updated"};

  if (htmlResults!=null) {
      statusStr = htmlResults[0];
      if (statusStr != null)
          statusStr = statusStr.trim();
  }

%>

<jsp:include page="bu-header.jsp" flush="true">
    <jsp:param name="page" value="cdsList"/>
</jsp:include>

<form name="theForm" id="theForm" action="serv" method="POST" align=center>
<input type="hidden" name="type" value="cds.status"></input>
<input type="hidden" name="action" value="list"></input>
<input type="hidden" name="p" value="1"></input>
<input type="hidden" name="cdssort" value=""></input>

<%if (statusStr!=null && !statusStr.equals("")) {%>
<BR>
<P>
<TABLE cellSpacing=0 cellPadding=1 width=95% align=center bgColor="#336699" border=0>
  <TR> 
    <TD> 
      <!-- CDS Status Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699> 
          <TD width="100%"> 
            <TABLE cellSpacing=1 cellPadding=4 width="100%" bgColor=#336699>
              <TR>
                <TD width="100%" bgColor=#336699 align="left"><FONT class="tblSubHdrLabel2"><%=session.getAttribute("TEXT_CDS_STATUS")%>&nbsp;</FONT></TD>
              </TR>
             </TABLE>
          </TD>
        </TR>
       <TR>
          <TD bgColor=#efefef>
            <TABLE cellSpacing="1" cellPadding="4" width="100%" border="0" align="center">
              <%if (iTotalCount>0) {%>
	      <tr>
		<td class="tableheader" nowrap="true"><%=session.getAttribute("COL_NO")%></td>
		  <%for (int i=0; i<aHeader.length; i++) {%>
                   <td class="tableheader" nowrap="true">
	            <%if (sort.equals(aSort[i])) {%>
                       <a href="serv?type=cds.status&action=list&cdssort=<%=aSort[i]%>_d"><%=session.getAttribute(aHeader[i])%></a> <img src="/images/up.gif">
                    <%} else if (sort.equals(aSort[i]+"_d")) {%>
                       <a href="serv?type=cds.status&action=list&cdssort=<%=aSort[i]%>"><%=session.getAttribute(aHeader[i])%></a> <img src="/images/down.gif">
                    <%} else {%>
                       <a href="serv?type=cds.status&action=list&cdssort=<%=aSort[i]%>"><%=session.getAttribute(aHeader[i])%></a>
                   <%}%>
                  </td>
                 <%}%>
             </tr>
	     <%=statusStr%>
	     <%} else {%>
	     <TR width="100%">
	       <TD>
		 <!-- No Result Found -->
		 <P>
	         <CENTER>	
	         <%=session.getAttribute("TEXT_RESULT")%>:&nbsp;<%=session.getAttribute("TEXT_EMPTY")%>
		 </CENTER>
	       </TD>
	     </TR>
	     <%}%>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
<!--BEGIN NAVIGATION TABLE -->
<%
    int iPageSize = qb.getPageSize();
    int iPageNo = qb.getPageNo();
    int iPageCount = 0;
    if (iTotalCount>iPageSize) {
      iPageCount = iTotalCount / iPageSize;
      if (iPageCount * iPageSize < iTotalCount) iPageCount ++;
    }
%>

<%if (iTotalCount>iPageSize) {%>
    <TABLE BORDER=0 width=95% cellspacing=0 cellpadding=4> 
    <TR>
    <TD><%=session.getAttribute("LBL_TOTAL_RECORDS")%> <%= iTotalCount%>&nbsp;&nbsp;
    <%if (iPageNo>1) { %>
       <input class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_FIRST")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_FIRST")%>" onClick="postpage(this.form, 1, '<%=sort%>');"></input>
       <input class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_PREV")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_PREV")%>" onClick="postpage(this.form, <%=iPageNo-1%>, '<%=sort%>');"></input>
    <% }%>
    <%if (iPageNo<iPageCount) { %>
       <input class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_NEXT")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_NEXT")%>" onClick="postpage(this.form,<%=iPageNo+1%>, '<%=sort%>');"></input>
       <input class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_LAST")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_LAST")%>" onClick="postpage(this.form,<%=iPageCount%>, '<%=sort%>');"></input>
    <% }%>
    <%String ppStr = null;
      for (int i=1; i<=iPageCount; i++) {
        if (i!=iPageNo)
          ppStr=ppStr+"<option value="+i+">Page&nbsp;"+i+"</option>";
        else
          ppStr=ppStr+"<option selected value="+i+">Page&nbsp;"+i+"</option>";
      }
      if (ppStr!=null) { %>
        &nbsp;<select class="smallField" name="gopnum" onchange="postpage(this.form,this.form.gopnum.options[this.form.gopnum.selectedIndex].value, '<%=sort%>');"><%=ppStr%></select>
        <input class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_GO")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_GO")%>" 
        onClick="postpage(this.form,this.form.gopnum.options[this.form.gopnum.selectedIndex].value, '<%=sort%>');"></input>
    <%}%>
    </TD>
    </TR>
    </TABLE>
<%}%>
<!--END NAVIGATION TABLE -->
<%}%>

</FORM name="theForm">

<jsp:include page="footer.jsp" flush="true"/>

<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.ms.common.QueryBean" %>
<jsp:useBean class="com.broadon.ms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();
  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  String[] params = qb.getParamArray();

  String catStr = htmlResults[0];
  String cityStr = htmlResults[1];

  String compID = params[0];
  String compName = params[1];
  String compStartDate = params[2];
  String compEndDate = params[3];

  if (compEndDate == null)
     compEndDate = "";

%>

<jsp:include page="bu-header.jsp" flush="true">
   <jsp:param name="page" value="compcatAdd"/>
</jsp:include>

<%if (err!=null && err!="") {%><center><font class="errorText"><%=err%></font></center><p><%}%>

<form name="theForm" id="theForm" action="serv" method="POST" onSubmit="return false;">
<input type="hidden" name="type" value="compcat"></input>
<input type="hidden" name="action" value="insert"></input>
<input type="hidden" name="comp_id" value="<%=compID%>"></input>

<TABLE width=50% align=center border=0>
  <TR><TD align=left><b><%=session.getAttribute("COL_COMPETITION_ID")%>:&nbsp;</b><%=compID%></TD></TR>
  <TR><TD align=left><b><%=session.getAttribute("COL_COMPETITION_NAME")%>:&nbsp;</b><A class="listText" href="serv?type=comp&action=edit&comp_id=<%=compID%>"><%=compName%></A></TD></TR>
  <TR><TD align=left><b><%=session.getAttribute("COL_COMPETITION_START_DATE")%>:&nbsp;</b><%=compStartDate%></TD></TR>
  <TR><TD align=left><b><%=session.getAttribute("COL_COMPETITION_END_DATE")%>:&nbsp;</b><%=compEndDate%></TD></TR>
</TABLE>

<p>
<TABLE cellSpacing=0 cellPadding=1 width=50% align=center bgColor="#336699" border=0>
  <TR> 
    <TD> 
      <!-- Competitions Detail Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699> 
          <TD width="100%"> 
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              <TR> 
                <TD width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2"><%=session.getAttribute("TEXT_ADD_COMPETING_CATEGORY")%></FONT></TD>
              </TR>
             </TABLE>
          </TD>
        </TR>
        <TR> 
          <TD bgColor=#efefef>
            <TABLE bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <TR>
                <TD class="tableheader" align=center width=50%><%=session.getAttribute("COL_PARAMETER")%></TD>
                <TD class="tableheader" align=center width=50% colspan=2><%=session.getAttribute("COL_VALUE")%></TD>
              </TR>
              <%=catStr%>
              <tr><td class="formLabel2" nowrap="true"><%=session.getAttribute("COL_COMPETITION_CITY_CODE")%>:</td><td class="formField"></td>
                  <td class="formField" nowrap="true">
                     <select name="city_code"><option value=''></option><%=cityStr%></select>
                  </td>
              </tr>
              <TR><TD class="formLabel2"><IMG border=0 height=1 src="/images/spacer.gif"></TD><TD class="formField" colspan=2><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
              <TR><TD colspan="3" bgcolor="white">
                   <CENTER><FONT color="red" size="-1">* Required</FONT></CENTER>
              </TD></TR>
              <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
              <TR>
                <TD colspan="3" bgcolor="white">
                <CENTER>
                   <INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_SUBMIT")%>" OnClick="onClickSubmit(theForm);">
                   <INPUT class="sbutton" type="reset" value="<%=session.getAttribute("BUTTON_RESET")%>">
                   <INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_CANCEL")%>" OnClick="history.go(-1)">
                </CENTER>
                </TD>
              </TR>
              <TR><TD colspan="3" bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
</FORM name="theForm">

<jsp:include page="footer.jsp" flush="true"/>


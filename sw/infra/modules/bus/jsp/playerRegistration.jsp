<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.ms.common.QueryBean" %>
<jsp:useBean class="com.broadon.ms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();
  String success = request.getAttribute("SUCCESS").toString();
  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  String[] params = qb.getParamArray();

  String playerStr = "";
  String sn = "";

  if (htmlResults!=null) {
      playerStr = htmlResults[0];
      if (playerStr != null)
          playerStr = playerStr.trim();
  }

  if (params!=null) {
      sn = params[0];
      if (sn == null)
          sn = "";
  }
%>

<jsp:include page="bu-header.jsp" flush="true">
    <jsp:param name="page" value="playerDetail"/>
</jsp:include>

<form name="theForm" id="theForm" action="serv" method="POST" onSubmit="return false;">
<input type="hidden" name="type" value=""></input>
<input type="hidden" name="action" value="list"></input>

<TABLE cellSpacing=0 cellPadding=1 width=60% align=center bgColor="#336699" border=0>
  <TR>
    <TD>
      <!-- Player Search Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699>
          <TD width="100%">
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              <TR>
                <TD width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2"><%=session.getAttribute("TEXT_PLAYER_SEARCH")%></FONT></TD>
              </TR>
             </TABLE>
          </TD>
        </TR>
        <TR>
          <TD bgColor=#efefef>
            <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
              <TR><TD><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
              <TR>
                  <TD align="right" width="50%"><B><%=session.getAttribute("TEXT_PRODUCT_SN")%></B></TD>
                  <TD align="left" width="50%"><INPUT type="text" name="sn" size="16" value="<%=sn%>"><INPUT type="hidden" name="id"><font color="red">*</font></TD>
              </TR>
              <TR><TD><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
            </TABLE>
            <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
              <TR>
                 <TD bgcolor="white">
                   <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
                     <TR><TD bgcolor="white">
                         <CENTER><FONT color="red" size="-1">* Required</FONT></CENTER>
                         </TD>
                     </TR>
                     <TR><TD bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
                     <TR>
                       <TD bgcolor="white">
                         <CENTER>
                         <jsp:include page="playerButton.jsp" flush="true"/>
                         </CENTER>
                       </TD>
                     </TR>
                   </TABLE>
                 </TD>
              </TR>
              <TR><TD bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>

<BR>
<P>
<%if (playerStr!=null && !playerStr.equals("")) {%>
<TABLE cellSpacing=0 cellPadding=1 width=60% align=center bgColor="#336699" border=0>
  <TR> 
    <TD> 
      <!-- Player Detail Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699> 
          <TD width="100%"> 
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              <TR> 
                <TD width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2"><%=session.getAttribute("TEXT_PLAYER_REGISTRATION")%>&nbsp;&nbsp;<%=sn%></FONT></TD>
              </TR>
             </TABLE>
          </TD>
        </TR>

        <TR> 
          <TD bgColor=#efefef>
            <TABLE bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <TR>
                <TD class="tableheader" align=center width=30%><%=session.getAttribute("COL_PARAMETER")%></TD>
                <TD class="tableheader" align=center width=70% colspan=2><%=session.getAttribute("COL_VALUE")%></TD>
              </TR>
              <%=playerStr%>
              <TR><TD class="formLabel2" width=30%><IMG border=0 height=1 src="/images/spacer.gif"></TD><TD class="formField" width=70% colspan=2><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
<%} else if (sn!=null && !sn.equals("")) {%>
<BR>
<CENTER>
   <%=session.getAttribute("TEXT_RESULT")%>:&nbsp;<%=session.getAttribute("TEXT_EMPTY")%>
</CENTER>
<%}%>
</FORM name="theForm">

<jsp:include page="footer.jsp" flush="true"/>

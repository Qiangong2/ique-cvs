<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.ms.common.QueryBean" %>

<jsp:useBean class="com.broadon.ms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  String[] params = qb.getParamArray();
  int[] counts = qb.getCountArray();

  String aStr = "";
  String timeline = "";
  String sn = "";
  int iTotalCount = 0;
  String sort = "";

  if (htmlResults!=null) {
      aStr = htmlResults[0];
      if (aStr != null) {
          aStr = aStr.trim();
          iTotalCount = counts[0];
          sort = qb.getSort();
          sort = sort.toLowerCase();
      }
  }

  if (params!=null) {
      sn = params[0];
      if (sn == null)
          sn = "";
      timeline = params[1];
      if (timeline == null)
          timeline = "";
  }

  System.out.println("Obtained All data...");
  String[] aHeader = {"COL_REQUEST_DATE", "COL_ACTION_CODE", "COL_STORE_ID", "COL_REQUEST_STATUS", "COL_REQUEST_LOG"};
  String[] aSort = {"request_date", "action_code", "store_id", "request_status", "request_log"};

  String[] sName = {"SELECT_ALL_ACTIVITY", "SELECT_AFTER_ACTIVITY", "SELECT_BEFORE_ACTIVITY"};
  String[] sValue = {"player.activity.all", "player.activity.after", "player.activity.before"};
%>

<jsp:include page="bu-header.jsp" flush="true">
    <jsp:param name="page" value="playerDetail"/>
</jsp:include>

<form name="theForm" id="theForm" action="serv" method="POST" align=center>
<input type="hidden" name="type" value=""></input>
<input type="hidden" name="action" value="list"></input>
<input type="hidden" name="p" value="1"></input>
<input type="hidden" name="pasort" value=""></input>

<TABLE cellSpacing=0 cellPadding=1 width=60% align=center bgColor="#336699" border=0>
  <TR>
    <TD>
      <!-- Player Search Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699>
          <TD width="100%">
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              <TR>
                <TD width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2"><%=session.getAttribute("TEXT_PLAYER_SEARCH")%></FONT></TD>
              </TR>
             </TABLE>
          </TD>
        </TR>
        <TR>
          <TD bgColor=#efefef>
            <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
              <TR><TD><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
              <TR>
                  <TD align="right" width="50%"><B><%=session.getAttribute("TEXT_PRODUCT_SN")%></B></TD>
                  <TD align="left" width="50%"><INPUT type="text" name="sn" size="16" value="<%=sn%>"><INPUT type="hidden" name="id"><font color="red">*</font></TD>
              </TR>
              <TR><TD><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
            </TABLE>
            <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
              <TR>
                 <TD bgcolor="white">
                   <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
                     <TR><TD bgcolor="white">
                         <CENTER><FONT color="red" size="-1">* Required</FONT></CENTER>
                         </TD>
                     </TR>
                     <TR><TD bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
                     <TR>
                       <TD bgcolor="white">
                         <CENTER>
                         <jsp:include page="playerButton.jsp" flush="true"/>
                         </CENTER>
                       </TD>
                     </TR>
                   </TABLE>
                 </TD>
              </TR>
              <TR><TD bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>


<BR>
<P>
<%if (aStr!=null && !aStr.equals("")) {%>
<TABLE cellSpacing=0 cellPadding=1 width=80% align=center bgColor="#336699" border=0>
  <TR> 
    <TD> 
      <!-- Player Activity Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699> 
          <TD width="100%"> 
            <TABLE cellSpacing=1 cellPadding=4 width="100%" bgColor=#336699>
              <TR> 
                <TD width="50%" bgColor=#336699 align="left"><FONT class="tblSubHdrLabel2"><%=session.getAttribute("TEXT_PLAYERS_ACTIVITY")%>&nbsp;&nbsp;<%=sn%></FONT></TD>
                <TD width="50%" bgColor=#336699 align="right">
                  <select name="timeline" onChange="onClickSubmit(theForm, this.options[this.selectedIndex].value)">
                   <%for (int i=0; i<sName.length; i++) {%>
                     <%if (timeline!=null && timeline.equals(sValue[i])) {%>
                        <option value=<%=sValue[i]%> selected><%=session.getAttribute(sName[i])%></option>
                     <%} else {%>
                        <option value=<%=sValue[i]%>><%=session.getAttribute(sName[i])%></option>
                     <%}%>
                   <%}%>
                  </select>
                </TD>   
              </TR>
             </TABLE>
          </TD>
        </TR>

        <TR> 
          <TD bgColor=#efefef>
            <TABLE cellSpacing="1" cellPadding="4" width="100%" border="0" align="center">
             <%if (iTotalCount>0) {%>
              <tr>
               <td class="tableheader" nowrap="true"><%=session.getAttribute("COL_NO")%></td>
               <%for (int i=0; i<aHeader.length; i++) {%>
                 <td class="tableheader" nowrap="true">
                 <%if (sort.equals(aSort[i])) {%>
                   <a href="serv?type=<%=timeline%>&action=list&sn=<%=sn%>&pasort=<%=aSort[i]%>_d"><%=session.getAttribute(aHeader[i])%></a> <img src="/images/up.gif">
                 <%} else if (sort.equals(aSort[i]+"_d")) {%>
                   <a href="serv?type=<%=timeline%>&action=list&sn=<%=sn%>&pasort=<%=aSort[i]%>"><%=session.getAttribute(aHeader[i])%></a> <img src="/images/down.gif">
                 <%} else {%>
                   <a href="serv?type=<%=timeline%>&action=list&sn=<%=sn%>&pasort=<%=aSort[i]%>"><%=session.getAttribute(aHeader[i])%></a>
                 <%}%>
                 </td>
               <%}%>
              </tr>
              <%=aStr%>
              <TR><TD class="formLabel2" colspan=6><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
            <%} else {%>
              <TR width="100%">
               <TD>
                <!-- No Result Found -->
                <P>
                <CENTER>
                  <%=session.getAttribute("TEXT_RESULT")%>:&nbsp;<%=session.getAttribute("TEXT_EMPTY")%>
                </CENTER>
              </TD>
             </TR>
            <%}%>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>

<p>
<!--BEGIN NAVIGATION TABLE -->
<%
        System.out.println("Obtaining Page info...");
    int iPageSize = qb.getPageSize();
    int iPageNo = qb.getPageNo();
    int iPageCount = 0;
    if (iTotalCount>iPageSize) {
      iPageCount = iTotalCount / iPageSize;
      if (iPageCount * iPageSize < iTotalCount) iPageCount ++;
    }
%>

<%if (iTotalCount>iPageSize) {%>
    <TABLE BORDER=0 align=center width=80% cellspacing=0 cellpadding=4> 
    <TR>
    <TD><%=session.getAttribute("LBL_TOTAL_RECORDS")%> <%= iTotalCount%>&nbsp;&nbsp;
    <%if (iPageNo>1) { %>
       <input class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_FIRST")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_FIRST")%>" onClick="postpage(this.form, 1, '<%=sort%>', '<%=timeline%>', '<%=sn%>');"></input>
       <input class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_PREV")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_PREV")%>" onClick="postpage(this.form, <%=iPageNo-1%>, '<%=sort%>', '<%=timeline%>', '<%=sn%>');"></input>
    <% }%>
    <%if (iPageNo<iPageCount) { %>
       <input class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_NEXT")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_NEXT")%>" onClick="postpage(this.form,<%=iPageNo+1%>, '<%=sort%>', '<%=timeline%>', '<%=sn%>');"></input>
       <input class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_LAST")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_LAST")%>" onClick="postpage(this.form,<%=iPageCount%>, '<%=sort%>', '<%=timeline%>', '<%=sn%>');"></input>
    <% }%>
    <%String pgStr = null;
      for (int i=1; i<=iPageCount; i++) {
        System.out.println("Creating Page..."+Integer.toString(i));
        if (i!=iPageNo)
          pgStr=pgStr+"<option value="+i+">Page&nbsp;"+i+"</option>";
        else
          pgStr=pgStr+"<option selected value="+i+">Page&nbsp;"+i+"</option>";
      }
      if (pgStr!=null) { %>
        &nbsp;<select class="smallField" name="gopnum"><%=pgStr%></select>
        <input class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_GO")%>" border="0" title="<%=session.getAttribute("ALT_BUTTON_GO")%>" 
        onClick="postpage(this.form,this.form.gopnum.options[this.form.gopnum.selectedIndex].value, '<%=sort%>', '<%=timeline%>', '<%=sn%>');"></input>
    <%}%>
    </TD>
    </TR>
    </TABLE>
<%}%>
<!--END NAVIGATION TABLE -->

<%} else if (sn!=null && !sn.equals("")) {%>
<BR>
<CENTER>
   <%=session.getAttribute("TEXT_RESULT")%>:&nbsp;<%=session.getAttribute("TEXT_EMPTY")%>
</CENTER>
<%}%>

</FORM name="theForm">

<jsp:include page="footer.jsp" flush="true"/>

<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.ms.common.QueryBean" %>
<jsp:useBean class="com.broadon.ms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  Object role = session.getAttribute("BCC_ROLE");
  String ROLE_BU_ECARD = "180";
  String ROLE_BU_CS = "130";

  String err = request.getAttribute("ERROR").toString();
  String success = request.getAttribute("SUCCESS").toString();
  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  String[] params = qb.getParamArray();

  String ecStr = "";
  String ecBatch = "";
  String id = "";
  String rStr = "";
  String ecid = "";

  int range = 1;
  int to_activate = 0;
  int to_revoke = 0;

  if (htmlResults!=null) {
      ecStr = htmlResults[0];
      ecBatch = htmlResults[1];
      if (ecStr != null)
          ecStr = ecStr.trim();
      if (ecBatch != null)
          ecBatch = ecBatch.trim();
  }

  if (params!=null) {
      id = params[0];
      rStr = params[1];
      if (id == null)
          id = "";
      if (rStr != null && !rStr.equals(""))
          range = Integer.parseInt(rStr);
      to_activate = range - Integer.parseInt(params[2]);
      to_revoke = range - Integer.parseInt(params[3]);
  }

  if (id!=null && !id.equals("")) {
      int i1 = Integer.parseInt(id);
      int i2 = range;
 
      if (i2==1)
       ecid = id;
      else
       ecid = String.valueOf(i1-(i1%i2)) + " - " + String.valueOf((i1+(i2-(i1%i2)))-1);
  }
%>

<jsp:include page="bu-header.jsp" flush="true">
    <jsp:param name="page" value="ecDetail"/>
</jsp:include>

<%if (err!=null && err!="") {%>
  <center><font class="errorText"><%=err%></font></center><p>
<%} else if (success!=null && success!="") {%>
  <center><font class="successText"><%=success%></font></center><p>
<%}%>

<form name="theForm" id="theForm" action="serv" method="POST" onSubmit="return false;">
<input type="hidden" name="type" value=""></input>
<input type="hidden" name="action" value=""></input>
<input type="hidden" name="time" value=""></input>
<input type="hidden" name="to_revoke" value="<%=to_revoke%>"></input>
<input type="hidden" name="to_activate" value="<%=to_activate%>"></input>

<TABLE cellSpacing=0 cellPadding=1 width=30% align=center bgColor="#336699" border=0>
  <TR>
    <TD>
      <!-- Game Ticket Search Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699>
          <TD width="100%">
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              <TR>
                <TD width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2"><%=session.getAttribute("TEXT_ECARD_SEARCH")%></FONT></TD>
              </TR>
             </TABLE>
          </TD>
        </TR>
        <TR>
          <TD bgColor=#efefef>
	    <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
	      <TR><TD><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
	      <TR>
	          <TD align="right"><B><%=session.getAttribute("TEXT_ECARD_ID")%></B></TD>
                  <TD align="left"><INPUT type="text" name="id" size="16" value="<%=id%>"><font color="red">*</font></TD>
	      </TR>
	      <TR>
	          <TD align="right"><B><%=session.getAttribute("TEXT_ECARD_RANGE")%></B></TD>
                  <TD align="left">
  	             <select name="range">
                       <%if (range==1) {%>
	               <option value="1" selected><%=session.getAttribute("TEXT_ECARD_SINGLE")%></option>
                       <%} else {%>
	               <option value="1"><%=session.getAttribute("TEXT_ECARD_SINGLE")%></option>
                       <%}%>
                       <%if (range==100) {%>
	               <option value="100" selected><%=session.getAttribute("TEXT_ECARD_SET")%></option>
                       <%} else {%>
	               <option value="100"><%=session.getAttribute("TEXT_ECARD_SET")%></option>
                       <%}%>
                       <%if (range==200) {%>
 	               <option value="200" selected><%=session.getAttribute("TEXT_ECARD_PACK")%></option>
                       <%} else {%>
 	               <option value="200"><%=session.getAttribute("TEXT_ECARD_PACK")%></option>
                       <%}%>
                       <%if (range==1000) {%>
	               <option value="1000" selected><%=session.getAttribute("TEXT_ECARD_LOT")%></option>
                       <%} else {%>
	               <option value="1000"><%=session.getAttribute("TEXT_ECARD_LOT")%></option>
                       <%}%>
	             </select>
	          </TD>
	      </TR>
	      <TR><TD><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
	    </TABLE>
	    <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
	      <TR>
	         <TD bgcolor="white">
  	            <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
  	              <TR><TD bgcolor="white">
	                  <CENTER><FONT color="red" size="-1">* Required</FONT></CENTER>
	                  </TD>
	              </TR>
	              <TR><TD bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
          	      <TR>
	                <TD bgcolor="white">
	                  <CENTER>
	                  <INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_QUERY")%>" OnClick="onClickSubmit(theForm);">
	                  <INPUT class="sbutton" type="reset" value="<%=session.getAttribute("BUTTON_RESET")%>">
	                  <INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_CANCEL")%>" OnClick="history.go(-1)">
	                  </CENTER>
        	       </TD>
		     </TR>
	           </TABLE>
	         </TD>
	      </TR>
	      <TR><TD bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
	    </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>

<%if (ecStr!=null && !ecStr.equals("")) {%>
<BR>
<P>
<TABLE cellSpacing=0 cellPadding=1 width=50% align=center bgColor="#336699" border=0>
  <TR> 
    <TD> 
      <!-- ECard Detail Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699> 
          <TD width="100%"> 
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              <TR> 
                <TD width="70%" align="left" bgColor=#336699><FONT class="tblSubHdrLabel2"><%=ecid%></FONT></TD>
                <%if (role!=null && (role.toString().equals(ROLE_BU_ECARD))) {%>
                  <% if (to_activate == 0) {%>
                    <TD width="15%" align="right" bgColor=#336699><FONT class="tblSubHdrLabel2"><%=session.getAttribute("TEXT_ECARDS_ACTIVATED")%></FONT></TD>
                  <%} else {%>
                    <TD width="15%" align="right" bgColor=#336699><INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_ACTIVATE")%>" OnClick="onClickActivate(theForm, '<%=ecid%>');"></TD>
                  <%}%>
                  <% if (to_revoke == 0) {%>
                    <TD width="15%" align="right" bgColor=#336699><FONT class="tblSubHdrLabel2"><%=session.getAttribute("TEXT_ECARDS_REVOKED")%></FONT></TD>
                  <%} else {%>
                    <TD width="15%" align="right" bgColor=#336699><INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_REVOKE")%>" OnClick="onClickRevoke(theForm, '<%=ecid%>');"></TD>
                  <%}%>
               <%}%>
              </TR>
             </TABLE>
          </TD>
        </TR>

        <TR> 
          <TD bgColor=#efefef>
            <TABLE bgColor=#efefef cellSpacing=0 cellPadding=2 width="100%" border=0>
              <TR>
                <TD class="tableheader" align=center width=50%><%=session.getAttribute("COL_PARAMETER")%></TD>
                <TD class="tableheader" align=center width=50% colspan=2><%=session.getAttribute("COL_VALUE")%></TD>
              </TR>
              <%=ecBatch%>
              <%=ecStr%>
              <TR><TD class="formLabel2" width=50%><IMG border=0 height=1 src="/images/spacer.gif"></TD><TD class="formField" width=50% colspan=2><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
<%} else if (id!=null && !id.equals("")) {%>
<BR><BR>
<P>
<CENTER>
   <%=session.getAttribute("TEXT_RESULT")%>:&nbsp;<%=session.getAttribute("TEXT_EMPTY")%>
</CENTER>
<%}%>

</FORM name="theForm">


<jsp:include page="footer.jsp" flush="true"/>


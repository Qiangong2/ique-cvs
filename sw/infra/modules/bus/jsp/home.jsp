<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.broadon.ms.common.QueryBean" %>
<jsp:useBean class="com.broadon.ms.common.QueryBean"
             id="qbean"
             scope="request"/>

<%
  String err = request.getAttribute("ERROR").toString();
  QueryBean qb = (QueryBean)request.getAttribute("qbean");
  String[] htmlResults = qb.getHTMLResultArray();
  String[] params = qb.getParamArray();
  int flag = 0;

  String roleStr = htmlResults[0];
  session.setAttribute("BCC_ROLE_NAME", params[0]);

  Object numRoles = session.getAttribute("BCC_ROLE_NUM");
  if (numRoles!=null)
     flag = Integer.parseInt(numRoles.toString());
%>

<jsp:include page="bu-header.jsp" flush="true">
    <jsp:param name="page" value="home"/>   
</jsp:include>

<P>
<CENTER>
  <FONT class="successText"><%=session.getAttribute("TEXT_WELCOME")%></FONT><BR>
  <B><%=session.getAttribute("BCC_FULLNAME")%></B><BR><BR>
  <%=session.getAttribute("TEXT_MESSAGE")%> 
</CENTER>
<P>

<%if (err!=null && err!="") {%><center><font class="errorText"><%=err%></font></center><p><%}%>

<%if (flag>1) {%>
<form name="theForm" id="theForm" action="serv" method="POST" align="center" onSubmit="return false;">
    <input type="hidden" name="type" value="home"></input>
    <input type="hidden" name="action" value="update"></input>

<TABLE cellSpacing=0 cellPadding=1 width=30% align=center bgColor="#336699" border=0>
  <TR> 
    <TD> 
      <!-- User Roles Table -->
      <TABLE cellSpacing=0 cellPadding=1 width="100%" bgColor=#336699 border=0>
        <TR bgColor=#336699> 
          <TD width="100%"> 
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              <TR>
                <TD width="100%" bgColor=#336699><FONT class="tblSubHdrLabel2"><%=session.getAttribute("TEXT_CHANGE_OPERATION_USER_ROLE")%></FONT></TD>
              </TR>
             </TABLE>
          </TD>
        </TR>
        <TR> 
          <TD bgColor=#efefef>
            <TABLE bgColor=white cellSpacing=0 cellPadding=2 width="100%" border=0>
              <TR><TD><IMG border=0 height=1 src="/images/spacer.gif"></TD></TR>
	      <TR>
                  <TD>
		    <CENTER>
                     <select name="newrole"></option><%=roleStr%></select>
                    </CENTER>
                  </TD>
              </TR>
              <TR><TD bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
              <TR>
                <TD bgcolor="white">
                <CENTER>
                <INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_SUBMIT")%>" OnClick="onClickSubmit(theForm);">
                <INPUT class="sbutton" type="reset" value="<%=session.getAttribute("BUTTON_RESET")%>">
                <INPUT class="sbutton" type="button" value="<%=session.getAttribute("BUTTON_CANCEL")%>" OnClick="history.go(-1)">
                </CENTER>
                </TD>
              </TR>
              <TR><TD bgcolor="white"><IMG border=0 height=1 width=1 src="/images/spacer.gif"></TD></TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
</FORM name="theForm">
<%}%>

<jsp:include page="footer.jsp" flush="true"/>

<%
   Object role = session.getAttribute("BCC_ROLE");
   String ROLE_BU_ADMIN = "110";
   String ROLE_BU_MKTG = "120";
   String ROLE_BU_CS = "130";
   String ROLE_BU_CSUP = "131";
   String ROLE_BU_ECARD = "180";
   String ROLE_BU_SSUP = "200";

   String index=request.getParameter("page");
   if (index==null) {
     index="home";
   }

   String mode=request.getParameter("display");
   if (mode==null) {
     mode="blank";
   }

   String rcflag=request.getParameter("rcflag");
   if (rcflag==null) {
     rcflag="1";
   }
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
   <META http-equiv="Content-Type" content="text/html; charset=utf-8">
   <META http-equiv="Expires" content="-1">
   <META http-equiv="Pragma" content="no-cache">

   <%if (index.equals("userList")) {%>
     <TITLE><%= session.getAttribute("OPERATION_USER_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.usort.value = sort;
        }
     </SCRIPT>    

   <%} else if (index.equals("userPassword")) {%>
     <TITLE><%= session.getAttribute("OPERATION_USER_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function onClickOK(formObj) {
           formObj.submit();
      }
     </SCRIPT>       

   <%} else if (index.equals("userAdd")) {%>
     <TITLE><%= session.getAttribute("OPERATION_USER_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
      function onClickSubmit(formObj) {
          formObj.fullname.value=Trim(formObj.fullname.value);
          if (isNull(formObj.fullname.value) || isBlank(formObj.fullname.value))
          { alert(<%="\"" + session.getAttribute("ALERT_FULLNAME") + "\""%>);
            formObj.fullname.focus();
            return; }

          formObj.email.value=Trim(formObj.email.value);
          if (isNull(formObj.email.value) || isBlank(formObj.email.value))
          { alert(<%="\"" + session.getAttribute("ALERT_EMAIL_ADDRESS") + "\""%>);
            formObj.email.focus();
            return; 
          } else {
            var splitted = formObj.email.value.match("^(.+)@(.+)$");
            if (splitted==null) 
            {
               var charpos = formObj.email.value.search("[^A-Za-z0-9]");
               if (formObj.email.value.length > 0 && charpos >= 0)
               {
                   alert(<%="\"" + session.getAttribute("ALERT_CHAR_EMAIL_ADDRESS") + "\""%>); 
                   formObj.email.focus();	
                   return; 
               }
            } else {
               if (splitted[1]!=null )
               {
                  var regexp_user=/^\"?[\w-_\.]*\"?$/;
                  if (splitted[1].match(regexp_user)==null) 
                  {
                     alert(<%="\"" + session.getAttribute("ALERT_CHAR_EMAIL_ADDRESS") + "\""%>); 
                     formObj.email.focus();	
                     return; 
                  }
               }
               if (splitted[2]!=null)
               {
                  var regexp_domain=/^[\w-\.]*\.[A-Za-z]{2,4}$/;
                  if (splitted[2].match(regexp_domain)==null)
                  {
                     var regexp_ip=/^\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\]$/;
                     if (splitted[2].match(regexp_ip)==null) 
                     {
                        alert(<%="\"" + session.getAttribute("ALERT_CHAR_EMAIL_ADDRESS") + "\""%>); 
                        formObj.email.focus();	
                        return; 
                     }
                  }
               }
            }
          }

          if (isNull(formObj.status.value) || isBlank(formObj.status.value))
          { alert(<%="\"" + session.getAttribute("ALERT_STATUS") + "\""%>);
            formObj.status.focus();
            return; }

          formObj.storeid.value=Trim(formObj.storeid.value);
          if (!isNull(formObj.storeid.value) && !isBlank(formObj.storeid.value))
          { if (!isInteger(formObj.storeid.value)) {
                alert(<%="\"" + session.getAttribute("ALERT_STORE_ID") + "\""%>);
                formObj.storeid.focus();
                return; }
          }

          if (isNull(formObj.role.value) || isBlank(formObj.role.value))
          { alert(<%="\"" + session.getAttribute("ALERT_ROLE_LEVEL") + "\""%>);
            formObj.role.focus();
            return; }

        formObj.passwd.value = Math.floor(Math.random() * 100000000);
        formObj.submit();
     }
     </SCRIPT>

   <%} else if (index.equals("userEdit")) {%>
     <TITLE><%= session.getAttribute("OPERATION_USER_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">

     <%if (role!=null && role.toString().equals(ROLE_BU_ADMIN)) {%>
      function showRoles(opid, buid) {
           window.location.href = "serv?type=userrole&action=edit&opid=" + opid + "&buid=" + buid;
           window.location.href.reload();
      }
     <%}%>

      function onClickSubmit(formObj) {
          formObj.fullname.value=Trim(formObj.fullname.value);
          if (isNull(formObj.fullname.value) || isBlank(formObj.fullname.value))
          { alert(<%="\"" + session.getAttribute("ALERT_FULLNAME") + "\""%>);
            formObj.fullname.focus();
            return; }

          formObj.email.value=Trim(formObj.email.value);
          if (isNull(formObj.email.value) || isBlank(formObj.email.value))
          { alert(<%="\"" + session.getAttribute("ALERT_EMAIL_ADDRESS") + "\""%>);
            formObj.email.focus();
            return; 
          } else {
            var splitted = formObj.email.value.match("^(.+)@(.+)$");
            if (splitted==null) 
            {
               var charpos = formObj.email.value.search("[^A-Za-z0-9]");
               if (formObj.email.value.length > 0 && charpos >= 0)
               {
                   alert(<%="\"" + session.getAttribute("ALERT_CHAR_EMAIL_ADDRESS") + "\""%>); 
                   formObj.email.focus();	
                   return; 
               }
            } else {
               if (splitted[1]!=null )
               {
                  var regexp_user=/^\"?[\w-_\.]*\"?$/;
                  if (splitted[1].match(regexp_user)==null) 
                  {
                     alert(<%="\"" + session.getAttribute("ALERT_CHAR_EMAIL_ADDRESS") + "\""%>); 
                     formObj.email.focus();	
                     return; 
                  }
               }
               if (splitted[2]!=null)
               {
                  var regexp_domain=/^[\w-\.]*\.[A-Za-z]{2,4}$/;
                  if (splitted[2].match(regexp_domain)==null)
                  {
                     var regexp_ip=/^\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\]$/;
                     if (splitted[2].match(regexp_ip)==null) 
                     {
                        alert(<%="\"" + session.getAttribute("ALERT_CHAR_EMAIL_ADDRESS") + "\""%>); 
                        formObj.email.focus();	
                        return; 
                     }
                  }
               }
            }
          }

        <%if (role!=null && role.toString().equals(ROLE_BU_ADMIN)) {%>
          if (isNull(formObj.status.value) || isBlank(formObj.status.value))
          { alert(<%="\"" + session.getAttribute("ALERT_STATUS") + "\""%>);
            formObj.status.focus();
            return; }

          formObj.storeid.value=Trim(formObj.storeid.value);
          if (!isNull(formObj.storeid.value) && !isBlank(formObj.storeid.value))
          { if (!isInteger(formObj.storeid.value)) {
                alert(<%="\"" + session.getAttribute("ALERT_STORE_ID") + "\""%>);
                formObj.storeid.focus();
                return; }
          }
        <%}%>

          if (isNull(formObj.role.value) || isBlank(formObj.role.value))
          { alert(<%="\"" + session.getAttribute("ALERT_ROLE_LEVEL") + "\""%>);
            formObj.role.focus();
            return; }

          if (formObj.confpasswd.value != formObj.passwd.value)
          { alert(<%="\"" + session.getAttribute("ALERT_PASSWORD_MISMATCH") + "\""%>); 
            formObj.passwd.focus();	
            return; }
          if (formObj.email.value!=formObj.last_email.value)
          { if (!(confirm(<%="\"" + session.getAttribute("ALERT_REVOKE_SMARTCARD_EMAIL") + "\""%>))) {
            formObj.email.focus();	
            return; }
          }
        <%if (role!=null && role.toString().equals(ROLE_BU_ADMIN)) {%>
          if (formObj.status.value=="I")
          { if (!(confirm(<%="\"" + session.getAttribute("ALERT_REVOKE_SMARTCARD_STATUS") + "\""%>))) {
            formObj.status.focus();	
            return; }
          }
        <%}%>
        formObj.submit();
     }
     </SCRIPT>

   <%} else if (index.equals("regionalCenterList")) {%>
     <TITLE><%= session.getAttribute("REGIONAL_CENTERS_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
     function postpage (formObj, newpage, sort) {
        formObj.p.value=newpage;
        formObj.rcsort.value = sort;
     }
     </SCRIPT>
	        
   <%} else if (index.equals("regionalCenterAdd") || index.equals("regionalCenterEdit")) {%>
     <TITLE><%= session.getAttribute("REGIONAL_CENTERS_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
     function onClickSubmit(formObj) {
        formObj.center_loc.value=Trim(formObj.center_loc.value);
	if (isNull(formObj.center_loc.value) || isBlank(formObj.center_loc.value))
        { alert(<%="\"" + session.getAttribute("ALERT_CENTER_LOC") + "\""%>); 
          formObj.center_loc.focus();	
          return; }

        formObj.center_name.value=Trim(formObj.center_name.value);
        if (isNull(formObj.center_name.value) || isBlank(formObj.center_name.value))
        { alert(<%="\"" + session.getAttribute("ALERT_CENTER_NAME") + "\""%>); 
          formObj.center_name.focus();	
          return; }

        formObj.hsm_config_id.value=Trim(formObj.hsm_config_id.value);
        if (!isNull(formObj.hsm_config_id.value) && !isBlank(formObj.hsm_config_id.value) &&
            !isInteger(formObj.hsm_config_id.value))
        { alert(<%="\"" + session.getAttribute("ALERT_HSM_CONFIG_ID") + "\""%>); 
          formObj.hsm_config_id.focus();	
          return; }

        for (var i=0, j=formObj.elements.length; i<j; i++) 
        {   var myName = formObj.elements[i].name;
            if (myName.indexOf('addr') > -1) 
            {  formObj.elements[i].value=Trim(formObj.elements[i].value);
               if (isNull(formObj.elements[i].value) || isBlank(formObj.elements[i].value))
               { alert(<%="\"" + session.getAttribute("ALERT_MISSING") + "\""%>); 
                 formObj.elements[i].focus();	
                 return; 
               } else if ((formObj.elements[i].value.indexOf(':') > -1) ||
                          (formObj.elements[i].value.indexOf('/') > -1) ||
                          (formObj.elements[i].value.indexOf(' ') > -1))
               { 
                 alert(<%="\"" + session.getAttribute("ALERT_ILLEGAL_LINE1") + "\""%> +"\n" + <%="\"" + session.getAttribute("ALERT_ILLEGAL_LINE2") + "\""%>); 
                 formObj.elements[i].focus();	
                 return; 
               } 
            }
        }

        for (var x=0, y=formObj.elements.length; x<y; x++) 
        {   var elName = formObj.elements[x].name;
            if (elName.indexOf('addr') > -1) 
            {  formObj.elements[x].value=Trim(formObj.elements[x].value);
               var index = elName.charAt(4);
               var prefix = "";
               var suffix = "";

               if (formObj['name'+index].value == "CDDS") {
                   prefix = <%="\"" + session.getAttribute("PREFIX_CDDS") + "\""%>;
                   suffix = <%="\"" + session.getAttribute("SUFFIX_CDDS") + "\""%>;
               } else if (formObj['name'+index].value == "CDS") {
                   prefix = <%="\"" + session.getAttribute("PREFIX_CDS") + "\""%>;
                   suffix = <%="\"" + session.getAttribute("SUFFIX_CDS") + "\""%>;
               } else if (formObj['name'+index].value == "IS") {
                   prefix = <%="\"" + session.getAttribute("PREFIX_IS") + "\""%>;
                   suffix = <%="\"" + session.getAttribute("SUFFIX_IS") + "\""%>;
               } else if (formObj['name'+index].value == "OPS") {
                   prefix = <%="\"" + session.getAttribute("PREFIX_OPS") + "\""%>;
                   suffix = <%="\"" + session.getAttribute("SUFFIX_OPS") + "\""%>;
               } else if (formObj['name'+index].value == "XS") {
                   prefix = <%="\"" + session.getAttribute("PREFIX_XS") + "\""%>;
                   suffix = <%="\"" + session.getAttribute("SUFFIX_XS") + "\""%>;
               }

               if (prefix == null)
                   prefix = "";
               if (suffix == null)
                   suffix = "";

               formObj.elements[x].value=prefix+formObj.elements[x].value+suffix;
            }
        }

        formObj.submit();
     }
     </SCRIPT>

   <%} else if (index.equals("regionList")) {%>
     <TITLE><%= session.getAttribute("REGIONS_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.rgsort.value = sort;
        }
     </SCRIPT>   
	     
   <%} else if (index.equals("regionAdd") || index.equals("regionEdit")) {%>
     <TITLE><%= session.getAttribute("REGIONS_TEXT_DESC") %></TITLE>

    <%if (role!=null && role.toString().equals(ROLE_BU_MKTG)) {%>
     <SCRIPT LANGUAGE="JavaScript">
     function onClickSubmit(formObj) {
        formObj.region_name.value=Trim(formObj.region_name.value);
        if (isNull(formObj.region_name.value) || isBlank(formObj.region_name.value))
        { alert(<%="\"" + session.getAttribute("ALERT_REGION_NAME") + "\""%>); 
          formObj.region_name.focus();	
          return; }
        formObj.submit();
     }
     </SCRIPT>
    <%}%>

   <%} else if (index.equals("cdsList")) {%>
     <TITLE><%= session.getAttribute("CDS_STATUS_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
     function postpage (formObj, newpage, sort, id) {
        formObj.p.value = newpage;
        formObj.cdssort.value = sort;
        formObj.submit();
     }
     </SCRIPT>    

   <%} else if (index.equals("retailerList")) {%>
     <TITLE><%= session.getAttribute("RETAILERS_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.rlsort.value = sort;
        }
     </SCRIPT>
	        
   <%} else if (index.equals("retailerAdd") || index.equals("retailerEdit")) {%>
     <TITLE><%= session.getAttribute("RETAILERS_TEXT_DESC") %></TITLE>

    <%if (role!=null && role.toString().equals(ROLE_BU_CS)) {%>
     <SCRIPT LANGUAGE="JavaScript">
     function onClickSubmit(formObj) {
        formObj.retailer_name.value=Trim(formObj.retailer_name.value);
        if (isNull(formObj.retailer_name.value) || isBlank(formObj.retailer_name.value))
        { alert(<%="\"" + session.getAttribute("ALERT_RETAILER_NAME") + "\""%>); 
          formObj.retailer_name.focus();	
          return; }
        if (isNull(formObj.code.value) || isBlank(formObj.code.value))
        { alert(<%="\"" + session.getAttribute("ALERT_RETAILER_TYPE") + "\""%>); 
          formObj.code.focus();	
          return; }
        formObj.submit();
     }
     </SCRIPT>
    <%}%>

   <%} else if (index.equals("storeList")) {%>
     <TITLE><%= session.getAttribute("STORES_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.stsort.value = sort;
        }
     </SCRIPT>   
	     
   <%} else if (index.equals("storeAdd") || index.equals("storeEdit")) {%>
     <TITLE><%= session.getAttribute("STORES_TEXT_DESC") %></TITLE>

    <%if (role!=null && role.toString().equals(ROLE_BU_CS)) {%>
     <SCRIPT LANGUAGE="JavaScript">
     function onClickSubmit(formObj) {
        formObj.addr.value=Trim(formObj.addr.value);
        if (isNull(formObj.addr.value) || isBlank(formObj.addr.value))
        { alert(<%="\"" + session.getAttribute("ALERT_ADDRESS") + "\""%>); 
          formObj.addr.focus();	
          return; }
        formObj.tz.value=Trim(formObj.tz.value);
        if (isNull(formObj.tz.value) || isBlank(formObj.tz.value))
        { alert(<%="\"" + session.getAttribute("ALERT_TIMEZONE") + "\""%>); 
          formObj.tz.focus();	
          return; }
        if (isNull(formObj.rcid.value) || isBlank(formObj.rcid.value))
        { alert(<%="\"" + session.getAttribute("ALERT_REGIONAL_CENTER") + "\""%>); 
          formObj.rcid.focus();	
          return; }
        if (isNull(formObj.rgid.value) || isBlank(formObj.rgid.value))
        { alert(<%="\"" + session.getAttribute("ALERT_REGION") + "\""%>); 
          formObj.rgid.focus();	
          return; }
        if (isNull(formObj.rlid.value) || isBlank(formObj.rlid.value))
        { alert(<%="\"" + session.getAttribute("ALERT_RETAILER") + "\""%>); 
          formObj.rlid.focus();	
          return; }
        formObj.submit();
     }
     </SCRIPT>
    <%}%>

   <%} else if (index.equals("ctpList")) {%>
     <TITLE><%= session.getAttribute("CTP_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.ctpsort.value = sort;
        }
     </SCRIPT>

   <%} else if (index.equals("ctpAdd")) {%>
     <TITLE><%= session.getAttribute("CTP_TEXT_DESC") %></TITLE>

     <SCRIPT LANGUAGE="JavaScript">
     function onClickSubmit(formObj) {
        if (isNull(formObj.pricing_type.value) || isBlank(formObj.pricing_type.value))
        { alert(<%="\"" + session.getAttribute("ALERT_CTP_PRICING_TYPE") + "\""%>); 
          formObj.pricing_type.focus();	
          return; }

        formObj.price.value=Trim(formObj.price.value);
        if (isNull(formObj.price.value) || isBlank(formObj.price.value) ||
            (!isNull(formObj.price.value) && !isInteger(formObj.price.value)))
        { alert(<%="\"" + session.getAttribute("ALERT_CTP_PRICE") + "\""%>); 
          formObj.price.focus();	
          return; }

        if (isNull(formObj.rtype.value) || isBlank(formObj.rtype.value))
        { alert(<%="\"" + session.getAttribute("ALERT_CTP_RTYPE") + "\""%>); 
          formObj.rtype.focus();	
          return; 
        } else {
          if (formObj.pricing_type.value=='PERMANENT' && formObj.rtype.value!='PR')
          { alert(<%="\"" + session.getAttribute("ALERT_CTP_PERMANENT_RTYPE") + "\""%>); 
            formObj.rtype.focus();	
            return; 
          }
          if (formObj.pricing_type.value!='PERMANENT' && formObj.rtype.value=='PR')
          { alert(<%="\"" + session.getAttribute("ALERT_CTP_LIMITED_RTYPE") + "\""%>); 
            formObj.rtype.focus();	
            return; 
          }
        }

        if (!isNull(formObj.rtype.value) && formObj.rtype.value=="PR")
        {
          formObj.limits.value=Trim(formObj.limits.value);
          if (!isNull(formObj.limits.value) && !isBlank(formObj.limits.value) &&
               (!isInteger(formObj.limits.value) || formObj.limits.value!='0'))
          { alert(<%="\"" + session.getAttribute("ALERT_CTP_LIMITS_PR") + "\""%>);
            formObj.limits.focus();
            return;
          } else {
            formObj.limits.value='0';
          }
        }

        if (!isNull(formObj.rtype.value) && formObj.rtype.value=="LR")
        {
          formObj.limits.value=Trim(formObj.limits.value);
          if (isNull(formObj.limits.value) || isBlank(formObj.limits.value) ||
              (!isNull(formObj.limits.value) && !isInteger(formObj.limits.value)))
          { alert(<%="\"" + session.getAttribute("ALERT_CTP_LIMITS_LR") + "\""%>);
            formObj.limits.focus();
            return;
          }
        }

        if (!isNull(formObj.rtype.value) && formObj.rtype.value=="TR")
        {
          formObj.limits.value=Trim(formObj.limits.value);
          if (isNull(formObj.limits.value) || isBlank(formObj.limits.value) ||
              (!isNull(formObj.limits.value) && !isInteger(formObj.limits.value)))
          { alert(<%="\"" + session.getAttribute("ALERT_CTP_LIMITS_TR") + "\""%>);
            formObj.limits.focus();
            return;
          }
        }

        setPricingName(formObj);        
        formObj.submit();
     }

     function setPricingName(formObj) {
        pt = formObj.pricing_type.value;
        rt = formObj.rtype.value;
        pr = formObj.price.value;
        lt = formObj.limits.value;
        pn = pt + ' - ';

        if (pt=='BONUS' || pt=='TRIAL') {
          pn += lt;
         
          if (rt == 'LR')
            pn += ' Plays';
          else if (rt =='TR')
            pn += ' Minutes';
        }

        if (pt=='PERMANENT') {
          if (pr == '0')
            pn += 'Free';
          else
            pn += pr + ' RMB';
        }

        if (pt=='RENTAL') {
          pn += lt;
         
          if (rt == 'LR')
            pn += ' Plays';
          else if (rt =='TR')
            pn += ' Minutes';

          pn += ' for ';

          if (pr == '0')
            pn += 'Free';
          else
            pn += pr + ' RMB';
        }

        formObj.pricing_name.value=pn;
        return;
     }
     </SCRIPT>

   <%} else if (index.equals("ctpEdit")) {%>
     <TITLE><%= session.getAttribute("CTP_TEXT_DESC") %></TITLE>

     <SCRIPT LANGUAGE="JavaScript">
     function onClickSubmit(formObj) {
        formObj.pricing_name.value=Trim(formObj.pricing_name.value);
        if (isNull(formObj.pricing_name.value) || isBlank(formObj.pricing_name.value))
        { alert(<%="\"" + session.getAttribute("ALERT_CTP_PRICING_NAME") + "\""%>); 
          formObj.pricing_name.focus();	
          return; }

        formObj.submit();
     }
     </SCRIPT>

   <%} else if (index.equals("ctrpList")) {%>
     <TITLE><%= session.getAttribute("CTRP_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.ctrpsort.value = sort;
        }
     </SCRIPT>

   <%} else if (index.equals("ctrpEdit")) {%>
     <TITLE><%= session.getAttribute("CTRP_TEXT_DESC")%></TITLE>
 
     <LINK REL="stylesheet" type="text/css" href="/css/popcalendar.css" />
     <script language="javascript" src="/js/popcalendar.js"></script>
     <SCRIPT LANGUAGE="JavaScript">

     function showDetails(formObj) 
     {
       formObj.rgid.value = Trim(formObj.rgid.value);

       if (isNull(formObj.rgid.value) || isBlank(formObj.rgid.value)) {
         alert(<%="\"" + session.getAttribute("ALERT_CTRP_REGION") + "\""%>);
         formObj.rgid.focus();
         return;
       }

       formObj.action.value = "edit";
       formObj.submit();
     }

     <%if (role!=null && role.toString().equals(ROLE_BU_MKTG)) {%>

     function toggleCheckBox(formObj, flag, enabled) 
     {
       var isChecked = false;
       if (flag == 1)
       {
         for (var i=2; i<=(formObj.records.value-0); i++)
         {
           if (formObj['active'+ i].value != '0') 
           {
              formObj['region'+ i].checked = false;
              formObj['region'+ i].disabled = enabled;
           }
         }
       } else {
         formObj.region1.checked = false;

         for (var i=2; i<=(formObj.records.value-0); i++)
         {
           if (formObj['active'+ i].value != '0' && formObj['region'+ i].checked)
           {
              isChecked = true;
              break;
           }
         }

         if (isChecked)
           formObj.region1.disabled = true;
         else
           formObj.region1.disabled = false;
       }
     }

     function onChangePermanentDates(formObj, index)
     {
       for (var i=2; i<=(formObj.records.value-0); i++)
       {
         if (formObj['ptype'+ i].value != 'PERMANENT')
         {
           if (formObj['start'+ i].value == formObj['last_start'+ index].value &&
               formObj['end'+ i].value == formObj['last_end'+ index].value &&
               formObj['start'+ i].value == formObj['last_start'+ i].value &&
               formObj['end'+ i].value == formObj['last_end'+ i].value)
           {
             formObj['start'+ i].value = formObj['start'+ index].value;
             formObj['end'+ i].value = formObj['end'+ index].value;
             formObj['region'+ i].checked = true;
           }
         }
       }
     }

     function onClickNonPermanentEndDate(formObj)
     {
       var pr = formObj.pr1.value;

       if (isNull(pr) || isBlank(pr) || pr.indexOf('PERMANENT')>=0)
       {
         popUpCalendar(formObj.end1, formObj.end1, 'yyyy.mm.dd', 'end');
       }
     }

     function setNonPermanentEndDate(formObj)
     {
       var pr = formObj.pr1.value;

       if (pr.indexOf('PERMANENT')<0)
         formObj.end1.value = "";
     }

     function checkEnd(formObj, next, e)
     {
       if (next == 0)
         return true;
       else if (!element_exists('start'+ next))
         return true;
       else if (isNull(formObj['end'+ e].value) || isBlank(formObj['end'+ e].value))
         return false;
       else if (compareDates(formObj['start'+ next].value, formObj['end'+ e].value) < 0)
         return true;
       else
         return false;
     }

     function checkStart(formObj, prev, s)
     {
       if (prev == 0 || prev == 1)
         return true;
       else if (isNull(formObj['end'+prev].value) || isBlank(formObj['end'+prev].value))
         return false;
       else if (compareDates(formObj['end'+prev].value, formObj['start'+s].value) > 0)
         return true;
       else
         return false;
     }

     function checkOverlap(formObj, index)
     {
       var end = 0;
       var flag = 0;
       
       for (var i=1; i<=(formObj.records.value-0); i++)
       {
         if (i != index)
         {
           if (formObj['ptype'+ i].value == formObj['ptype'+ index].value)
           {
             var j = i + 1;
             end = 0;
             flag = 0;

             if (compareDates(formObj['start'+ i].value, formObj['start'+ index].value) < 0) 
             {
               end = i;
               flag = 1;
             }
             else if (compareDates(formObj['start'+ i].value, formObj['start'+ index].value) == 0) flag = -1;
             else 
             {
               if (isNull(formObj['end'+ i].value) || isBlank(formObj['end'+ i].value)) flag = -1;
               else if (compareDates(formObj['end'+ i].value, formObj['start'+ index].value) > 0) 
               {
                 if (element_exists('start'+ j) && formObj['ptype'+ j].value == formObj['ptype'+ index].value)
                 {
                   if (compareDates(formObj['start'+ j].value, formObj['start'+ index].value) < 0)
                   {
                     end = j;
                     flag = 1;
                   }
                 }
               } 
               else flag = -1; 
             
             }

             if (flag != 0)
               break;
           }
         }
       }

       if (flag >= 0 && checkEnd(formObj, end, index))
         return false;
       else
         return true;
     } 
              
     function checkMatchingPermanentRecord(formObj, index)
     {
       var flag = 0;

       for (var i=formObj.records.value-0; i>1; i--)
       {
         if (i != index && 
             formObj['ptype'+ i].value == 'PERMANENT' && 
             formObj['start'+ i].value == formObj['start'+ index].value)
         {
           formObj['region'+ i].disabled = false;
           formObj['region'+ i].checked = true;
           formObj['end'+ index].value = formObj['end'+ i].value;
           flag = 1;
           break;
         }
       }

       if (flag == 0) 
       {
         formObj['end'+ index].value = formObj['last_end'+ index].value;
         return false;

       } else if (flag == 1) {
         for (var i=formObj.records.value-0; i>1; i--)
         {
           if (i != index && 
               formObj['ptype'+ i].value != 'PERMANENT' && 
               formObj['start'+ i].value == formObj['start'+ index].value)
           {
             formObj['region'+ i].disabled = false;
             formObj['region'+ i].checked = true;
           }
         }
         return true;
       }
     }

     function checkMatchingNonPermanentRights(formObj, index)
     {
       var flag = 1;

       for (var i=formObj.records.value-0; i>1; i--)
       {
           if (i != index && 
               formObj['ptype'+ i].value != 'PERMANENT' && 
               formObj['start'+ i].value == formObj['start'+ index].value &&
               formObj['rtype'+ i].value != formObj['rtype'+ index].value)
           {
             flag = 0;
             break;
           }
       }

       if (flag == 0) 
         return false;
       else
         return true;
     }

     function onClickSubmit(formObj)
     {
       var selected = false;
       for (var k=formObj.records.value-0; k>0; k--)
       {
         if (k != 1)
         {
           var t1 = formObj['pr'+ k].value;
           formObj['pid'+ k].value = t1.substring(0,t1.indexOf("|"))
           var t2 = t1.substring(t1.indexOf("|")+1);
           formObj['ptype'+ k].value = t2.substring(0,t2.indexOf("|"));
           formObj['rtype'+ k].value = t2.substring(t2.indexOf("|")+1);
         }

         if (formObj['region'+ k].checked)
           selected = true;
       }

       if(!selected)
       {
         alert(<%="\"" + session.getAttribute("ALERT_CTRP_NOT_SELECTED") + "\""%>);
         return;
       }

       for (var j=formObj.records.value-0; j>1; j--)
       {
         if (!formObj['region'+ j].checked)
         {
           sdate = '0000.00.00';
           if (!isNull(formObj['start'+ j].value) && !isBlank(formObj['start'+ j].value))
             sdate = formObj['start'+ j].value;
           if (compareDates(sdate, formObj['last_start'+ j].value) != 0)
             formObj['start'+ j].value = formObj['last_start'+ j].value;

           edate = '0000.00.00';
           if (!isNull(formObj['end'+ j].value) && !isBlank(formObj['end'+ j].value))
             edate = formObj['end'+ j].value;
           ledate = '0000.00.00';
           if (!isNull(formObj['last_end'+ j].value) && !isBlank(formObj['last_end'+ j].value))
             ledate = formObj['last_end'+ j].value;
           if (compareDates(edate, ledate) != 0)
             formObj['end'+ j].value = formObj['last_end'+ j].value;
         }
       }

       for (var i=formObj.records.value-0; i>0; i--)
       {
         if (formObj['region'+ i].checked)
         {
           if (i != 1 && formObj['ptype'+ i].value != 'PERMANENT')
             formObj['end'+ i].value = "";

           if (isNull(formObj['start'+ i].value) || isBlank(formObj['start'+ i].value)) 
           {
             alert(<%="\"" + session.getAttribute("ALERT_CTRP_MISSING_START_DATE") + "\""%>);
             formObj['start'+ i].focus();
             return;
           } else {
             if (!isNull(formObj['end'+ i].value) && !isBlank(formObj['end'+ i].value)) 
             {
               if (compareDates(formObj['start' + i].value, formObj['end' + i].value) < 0) 
               {
                 alert(<%="\"" + session.getAttribute("ALERT_CTRP_COMPARE_DATES") + "\""%>);
                 return;
               } 
             }
           }

           if (i == 1) 
           {
             if (isNull(formObj.pr1.value) || isBlank(formObj.pr1.value)) 
             {
               alert(<%="\"" + session.getAttribute("ALERT_CTRP_MISSING_PRICE") + "\""%>);
               formObj.pr1.focus();
               return;
             }

             var t1 = formObj.pr1.value;
             formObj.pid1.value = t1.substring(0,t1.indexOf("|"))
             var t2 = t1.substring(t1.indexOf("|")+1);
             formObj.ptype1.value = t2.substring(0,t2.indexOf("|"));
             formObj.rtype1.value = t2.substring(t2.indexOf("|")+1);
             formObj.last_start1.value=formObj.start1.value;

             if (formObj.ptype1.value != 'PERMANENT')
             {
               if (!checkMatchingPermanentRecord(formObj, i))
               {
                 alert(<%="\"" + session.getAttribute("ALERT_CTRP_NOT_MATCHING_PERMANENT") + "\""%>);
                 return;
               } 

               if (!checkMatchingNonPermanentRights(formObj, i))
               {
                 alert(<%="\"" + session.getAttribute("ALERT_CTRP_NOT_MATCHING_NON_PERMANENT_RIGHTS") + "\""%>);
                 return;
               } 
             }

             if (checkOverlap(formObj, i)) 
             {
               alert(<%="\"" + session.getAttribute("ALERT_CTRP_NEW_OVERLAP_DATES") + "\""%>);
               return;
             }
 
           } else {

             if (formObj['ptype'+ i].value != 'PERMANENT')
             {
               if (!checkMatchingPermanentRecord(formObj, i))
               {
                 alert(<%="\"" + session.getAttribute("ALERT_CTRP_NOT_MATCHING_PERMANENT") + "\""%>);
                 return;
               } 

               if (!checkMatchingNonPermanentRights(formObj, i))
               {
                 alert(<%="\"" + session.getAttribute("ALERT_CTRP_NOT_MATCHING_NON_PERMANENT_RIGHTS") + "\""%>);
                 return;
               } 
             }

             if (checkOverlap(formObj, i)) 
             {
               alert(<%="\"" + session.getAttribute("ALERT_CTRP_OVERLAP_DATES") + "\""%>);
               return;
             } 

             if (formObj['ptype'+ i].value == 'PERMANENT')
             {
               if (formObj['start'+ i].value != formObj['last_start'+ i].value || 
                   formObj['end'+ i].value != formObj['last_end'+ i].value)
                 onChangePermanentDates(formObj, i);
             }
           }
         }
       }

       formObj.action.value = "update";
       formObj.submit();
     }
    <%}%>
     </SCRIPT>

   <%} else if (index.equals("compList")) {%>
     <TITLE><%= session.getAttribute("COMPETITIONS_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.compsort.value = sort;
        }
     </SCRIPT>   
	     
   <%} else if (index.equals("compAdd") || index.equals("compEdit")) {%>
     <TITLE><%= session.getAttribute("COMPETITIONS_TEXT_DESC") %></TITLE>

    <%if (role!=null && role.toString().equals(ROLE_BU_MKTG)) {%>
     <LINK REL="stylesheet" type="text/css" href="/css/popcalendar.css" />
     <script language="javascript" src="/js/popcalendar.js"></script>
     <SCRIPT LANGUAGE="JavaScript">
     function onClickSubmit(formObj) {
        formObj.title.value=Trim(formObj.title.value);
        if (isNull(formObj.title.value) || isBlank(formObj.title.value))
        { alert(<%="\"" + session.getAttribute("ALERT_COMPETITION_TITLE") + "\""%>); 
          formObj.title.focus();	
          return; }
        formObj.comp_name.value=Trim(formObj.comp_name.value);
        if (isNull(formObj.comp_name.value) || isBlank(formObj.comp_name.value))
        { alert(<%="\"" + session.getAttribute("ALERT_COMPETITION_NAME") + "\""%>); 
          formObj.comp_name.focus();	
          return; }
        if (isNull(formObj.start.value) || isBlank(formObj.start.value)) 
        { alert(<%="\"" + session.getAttribute("ALERT_COMPETITION_START_DATE") + "\""%>);
          formObj.start.focus();
          return;
        } else {
          if (!isNull(formObj.end.value) && !isBlank(formObj.end.value)) {
             if (compareDates(formObj.start.value, formObj.end.value) < 0) {
               alert(<%="\"" + session.getAttribute("ALERT_COMPETITION_COMPARE_DATES") + "\""%>);
               return;
             }
          }
       }
       formObj.submit();
     }
     </SCRIPT>
    <%}%>

   <%} else if (index.equals("compcatList")) {%>
     <TITLE><%= session.getAttribute("COMPETING_CATEGORIES_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
      <%if (role!=null && role.toString().equals(ROLE_BU_MKTG)) {%>
        function showAddForm(compID) {
           window.location.href = "serv?type=compcat&action=add&comp_id=" + compID;
           window.location.href.reload();
        }
      <%}%>

        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.compcatsort.value = sort;
        }
     </SCRIPT>   
	     
   <%} else if (index.equals("compcatAdd") || index.equals("compcatEdit")) {%>
     <TITLE><%= session.getAttribute("COMPETING_CATEGORIES_TEXT_DESC")%></TITLE>

    <%if (role!=null && role.toString().equals(ROLE_BU_MKTG)) {%>
     <LINK REL="stylesheet" type="text/css" href="/css/popcalendar.css" />
     <script language="javascript" src="/js/popcalendar.js"></script>
     <SCRIPT LANGUAGE="JavaScript">
     function onClickSubmit(formObj) {
        formObj.cat_code.value=Trim(formObj.cat_code.value);
        if (isNull(formObj.cat_code.value) || isBlank(formObj.cat_code.value))
        { alert(<%="\"" + session.getAttribute("ALERT_COMPETING_CATEGORY_CODE") + "\""%>); 
          formObj.cat_code.focus();	
          return; 
        } else { formObj.cat_code.value = formObj.cat_code.value.toUpperCase(); }
        formObj.cat_name.value=Trim(formObj.cat_name.value);
        if (isNull(formObj.cat_name.value) || isBlank(formObj.cat_name.value))
        { alert(<%="\"" + session.getAttribute("ALERT_COMPETING_CATEGORY_NAME") + "\""%>); 
          formObj.cat_name.focus();	
          return; }
        if (isNull(formObj.start.value) || isBlank(formObj.start.value)) 
        { alert(<%="\"" + session.getAttribute("ALERT_COMPETING_START_DATE") + "\""%>);
          formObj.start.focus();
          return;
        } else {
          if (!isNull(formObj.end.value) && !isBlank(formObj.end.value)) {
             if (compareDates(formObj.start.value, formObj.end.value) < 0) {
               alert(<%="\"" + session.getAttribute("ALERT_COMPETING_COMPARE_DATES") + "\""%>);
               return;
             }
          }
       }
       formObj.submit();
     }
     </SCRIPT>
    <%}%>

   <%} else if (index.equals("ectList")) {%>
     <TITLE><%=session.getAttribute("ECARD_TYPE_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.ectsort.value = sort;
        }
     </SCRIPT>

   <%} else if (index.equals("ecbList")) {%>
     <TITLE><%=session.getAttribute("ECARD_BATCHES_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.ecbsort.value = sort;
        }
     </SCRIPT>

   <%} else if (index.equals("ecbEdit")) {%>
     <TITLE><%= session.getAttribute("ECARD_BATCHES_TEXT_DESC")%></TITLE>

    <%if (role!=null && role.toString().equals(ROLE_BU_ECARD)) {%>
     <LINK REL="stylesheet" type="text/css" href="/css/popcalendar.css" />
     <script language="javascript" src="/js/popcalendar.js"></script>
     <SCRIPT LANGUAGE="JavaScript">
     function onClickSubmit(formObj) {
       if (!isNull(formObj.activate.value) && !isBlank(formObj.activate.value))
       {   if (element_exists('current') && compareDates(formObj.current.value, formObj.activate.value) < 0)
           { alert(<%="\"" + session.getAttribute("ALERT_ACTIVATE_DATE") + "\""%>);
             formObj.activate.focus();
             return;
           }

           if (!isNull(formObj.revoke.value) && !isBlank(formObj.revoke.value))
           {   if (compareDates(formObj.activate.value, formObj.revoke.value) < 0)
               {  alert(<%="\"" + session.getAttribute("ALERT_REVOKE_DATE") + "\""%>);
                  formObj.revoke.focus();
                  return;
               }
           } 
       } else
       {  alert(<%="\"" + session.getAttribute("ALERT_NO_ACTIVATE_DATE") + "\""%>);
          formObj.activate.focus();
          return;
       } 

       formObj.submit();     
     }
     </SCRIPT>
    <%}%>

   <%} else if (index.equals("ecDetail")) {%>
     <TITLE><%= session.getAttribute("ECARDS_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function onClickSubmit(formObj) {
          formObj.id.value=Trim(formObj.id.value);
          if (isNull(formObj.id.value) || isBlank(formObj.id.value))
          { alert(<%="\"" + session.getAttribute("ALERT_ECARD_ID") + "\""%>);
            formObj.id.focus();
            return;
          } else {
            if (formObj.id.value.length != 10 || !isInteger(formObj.id.value))
            { alert(<%="\"" + session.getAttribute("ALERT_BAD_ECARD_ID") + "\""%>);
              formObj.id.focus();
              return;
            }
          }

          var curdate = new Date();
          formObj.time.value = curdate.getTime();

          if (formObj.range.value != "1")
          {  formObj.type.value = "ec.multiple"; }
          else
          {  formObj.type.value = "ec.single"; }

          formObj.action.value = "edit";

          formObj.submit();     
        }

      <%if (role!=null && role.toString().equals(ROLE_BU_ECARD)) {%>
        function onClickRevoke(formObj, ecid) {
          if (!(confirm(<%="\"" + session.getAttribute("ALERT_REVOKE_ECARDS") + "\""%> + " " + ecid + " ?"))) {
            return; }

          var curdate = new Date();
          formObj.time.value = curdate.getTime();

          if (formObj.range.value != "1")
          {  formObj.type.value = "ec.multiple.revoke"; }
          else
          {  formObj.type.value = "ec.single.revoke"; }

          formObj.action.value="update";
   
          formObj.submit();     
        }

        function onClickActivate(formObj, ecid) {
          if (!(confirm(<%="\"" + session.getAttribute("ALERT_ACTIVATE_ECARDS") + "\""%> + " " + ecid + " ?"))) {
            return; }

          var curdate = new Date();
          formObj.time.value = curdate.getTime();

          if (formObj.range.value != "1")
          {  formObj.type.value = "ec.multiple.activate"; }
          else
          {  formObj.type.value = "ec.single.activate"; }

          formObj.action.value="update";

          formObj.submit();     
        }
     <%}%>
     </SCRIPT>
	 
	 <%} else if (index.equals("ecInfo")) {%>
     <TITLE><%= session.getAttribute("ECARD_INFO_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function onClickSubmit(formObj) {
          formObj.id.value=Trim(formObj.id.value);
          if (isNull(formObj.id.value) || isBlank(formObj.id.value))
          { alert(<%="\"" + session.getAttribute("ALERT_ECARD_ID") + "\""%>);
            formObj.id.focus();
            return;
          } else {
            if (formObj.id.value.length != 10 || !isInteger(formObj.id.value))
            { alert(<%="\"" + session.getAttribute("ALERT_BAD_ECARD_ID") + "\""%>);
              formObj.id.focus();
              return;
            }
          }
          var curdate = new Date();
          formObj.time.value = curdate.getTime();
          formObj.type.value = "eci.single"; 
          formObj.action.value = "edit";
          formObj.submit();     
        }
     </SCRIPT>

   <%} else if (index.equals("playerDetail")) {%>
     <TITLE><%= session.getAttribute("PLAYER_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function onClickSubmit(formObj, type) {
          formObj.sn.value=Trim(formObj.sn.value);
          if (isNull(formObj.sn.value) || isBlank(formObj.sn.value))
          { alert(<%="\"" + session.getAttribute("ALERT_BB_SN") + "\""%>);
            formObj.sn.focus();
            return false;
          }
          if (isInteger(formObj.sn.value))
          {
            formObj.id.value=formObj.sn.value
          }else{
            formObj.id.value="0"
          }
          formObj.type.value=type;
          formObj.submit();     
        }
        function postpage (formObj, newpage, sort, type, sn)
        {
            formObj.p.value = newpage;
            formObj.pasort.value = sort;
            formObj.type.value = type;
            formObj.sn.value = sn;
            if (isInteger(formObj.sn.value))
            {
              formObj.id.value=formObj.sn.value
            }else{
              formObj.id.value="0"
            }
            formObj.submit();     
        }
     </SCRIPT>

   <%} else if (index.equals("titleList")) {%>
     <TITLE><%= session.getAttribute("TITLE_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.tlsort.value = sort;
        }
     </SCRIPT> 
	       
   <%} else if (index.equals("titleDetail")) {%>
     <TITLE><%= session.getAttribute("TITLE_TEXT_DESC")%></TITLE>

     <%} else if (index.equals("contentList")) {%>
     <TITLE><%= session.getAttribute("CONTENT_TEXT_DESC") %></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
        function postpage (formObj, newpage, sort) {
            formObj.p.value=newpage;
            formObj.consort.value = sort;
        }
     </SCRIPT> 
	       
   <%} else if (index.equals("contentDetail")) {%>
     <TITLE><%= session.getAttribute("CONTENT_TEXT_DESC")%></TITLE>

   <%} else if (index.equals("home")) {%>
     <TITLE><%= session.getAttribute("HOME_TEXT_DESC")%></TITLE>
     <SCRIPT LANGUAGE="JavaScript">
      function onClickSubmit(formObj) {
          if (isNull(formObj.newrole.value) || isBlank(formObj.newrole.value))
          { alert(<%="\"" + session.getAttribute("ALERT_CHANGE_ROLE") + "\""%>);
            formObj.newrole.focus();
            return; }
        formObj.submit();
     }
     </SCRIPT>

   <%}%>
 
    <SCRIPT LANGUAGE="javascript" src="/js/functions.js"></SCRIPT>
    <SCRIPT LANGUAGE="JavaScript">
    function getHelp(pageIndex) {

        var helpUrl = pageIndex;                 
        if (pageIndex == "regions") {			// Regions
            helpUrl = "regions";
        } else if (pageIndex == "ctr") {		// Content Title Regions
            helpUrl = "ctr";
        } else if (pageIndex == "retailers") {		// Retailers
            helpUrl = "retailers";
        } else if (pageIndex == "stores") {		// Stores
            helpUrl = "stores";
        }
    
        var url = "/broadon/RMS/help/"+helpUrl;
        var helpWin = window.open("", "_bbhelp","status=no,toolbar=no,scrollbars=yes,resizable=yes,menubar=no,copyhistory=no,directories=no,location=no,width=640,height=600");
        helpWin.focus();  // Put help window on top
        helpWin.location = url;  // Load the URL
    }

   </SCRIPT>

   <LINK rel="stylesheet" type="text/css" href="/css/rms.css" title="Style"/>
</HEAD>

<!-- <body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> -->
<!-- <BODY bgcolor="white" link="#003366" vlink="#003366" alink="#003366" text="black"> -->
<BODY bgcolor="white" text="black">

<TABLE border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="white">
  <!-- 1st Row: Logo Section -->
  <TR width="100%"> 
    <TD colspan="2"> 
      <TABLE border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="white">
        <TR height="39"> 
          <TD width="1%" valign="top" bgcolor="white"><IMG border="0" src="/images/logo_whiteonblue.gif" width="176" height="39"> 
          </TD>
          <TD width="98%" valign="middle" align="right" nowrap style="background-repeat: no-repeat" background="/images/title_background.gif"> 
            <FONT class="titleFont">Account Management System&nbsp;&nbsp;</FONT></TD>
          <TD width="1%"><IMG border="0" width="1" src="/images/spacer.gif" height="39"></TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
  <!-- 2nd Row: White Line -->
  <TR width="100%" bgcolor=white><TD colspan="2"><IMG border=0 width=1 height=1 src="/images/spacer.gif"></TD></TR>
  <!-- 3rd Row: Menu Navigation Bar -->
  <TR width="100%">
    <TD >
      <!-- Top-Level Menu Bar: Menu 1 is selected -->

<%if (role!=null && role.toString().equals(ROLE_BU_ADMIN)) {%>

      <TABLE border=0 cellpadding=0 cellspacing=0 width=100% bgcolor="#efefef">
        <TR>

  <%if (index.equals("userList") || index.equals("userAdd") || index.equals("userEdit") || index.equals("userPassword")) {%>
          <TD><IMG src="/images/menu.off.start.gif"></TD>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_OPERATION_USERS")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
  <%} else {%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;&nbsp;<A class="mainMenuHeader" HREF="serv?type=user&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_OPERATION_USERS")%></FONT></A>&nbsp;</TD>
  <%}%>

  <%if (index.equals("regionalCenterList") || index.equals("regionalCenterAdd") || index.equals("regionalCenterEdit")) {%>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_REGIONAL_CENTERS")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
  <%} else {%>
          <%if (!(index.equals("userList") || index.equals("userAdd") || index.equals("userEdit") || index.equals("userPassword"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=regionalCenter&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_REGIONAL_CENTERS")%></FONT></A>&nbsp;</TD> 
  <%}%>

  <%if (index.equals("retailerList") || index.equals("retailerEdit") ||
        index.equals("storeList") || index.equals("storeEdit")) {%>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_CHANNELS")%> </FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
  <%} else {%>
          <%if (!(index.equals("regionalCenterList") || index.equals("regionalCenterAdd") || index.equals("regionalCenterEdit"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=retailer&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_CHANNELS")%></FONT></A>&nbsp;</TD> 
  <%}%>

  <%if (index.equals("titleList") || index.equals("titleDetail") || 
        index.equals("contentList") || index.equals("contentDetail")) {%>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_CDS")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
  <%} else {%>
          <%if (!(index.equals("retailerList") || index.equals("retailerEdit") || index.equals("storeList") || index.equals("storeEdit"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=content&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_CDS")%></FONT></A>&nbsp;</TD>
  <%}%>

  <%if (index.equals("cdsList")) {%>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_CDS_STATUS")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
  <%} else {%>
           <%if (!(index.equals("titleList") || index.equals("titleDetail") || index.equals("contentList") || index.equals("contentDetail"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=cds.status&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_CDS_STATUS")%></FONT></A>&nbsp;</TD>
  <%}%>

  <%if (!(index.equals("cdsList"))) {%>
          <TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="javascript:getHelp('<%=index%>')"><FONT class="mainMenuText"><%=session.getAttribute("MENU_HELP")%></FONT></A>&nbsp;</TD>
          <TD bgcolor="#336699" nowrap width="100%" algin=right background="/images/menu.off.bg.gif">&nbsp;</TD>
        </TR>
      </TABLE>

<%} else if (role!=null && role.toString().equals(ROLE_BU_MKTG)) {%>

      <TABLE border=0 cellpadding=0 cellspacing=0 width=100% bgcolor="#efefef">
        <TR>
  <%if (index.equals("regionList") || index.equals("regionAdd") || index.equals("regionEdit")) {%>
          <TD><IMG src="/images/menu.off.start.gif"></TD>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_REGIONS")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
  <%} else {%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;&nbsp;<A class="mainMenuHeader" HREF="serv?type=region&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_REGIONS")%></FONT></A>&nbsp;</TD>
  <%}%>

  <%if (index.equals("retailerList") || index.equals("retailerEdit") ||
        index.equals("storeList") || index.equals("storeEdit")) {%>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_CHANNELS")%> </FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
  <%} else {%>
          <%if (!(index.equals("regionList") || index.equals("regionAdd") || index.equals("regionEdit"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=retailer&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_CHANNELS")%></FONT></A>&nbsp;</TD> 
  <%}%>

  <%if (index.equals("titleList") || index.equals("titleDetail") || 
        index.equals("contentList") || index.equals("contentDetail")) {%>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_CDS")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
  <%} else {%>
          <%if (!(index.equals("retailerList") || index.equals("retailerEdit") || index.equals("storeList") || index.equals("storeEdit"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=content&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_CDS")%></FONT></A>&nbsp;</TD>
  <%}%>

  <%if (index.equals("ctpList") || index.equals("ctpAdd") || index.equals("ctpEdit") ||
        index.equals("ctrpList") || index.equals("ctrpEdit")) {%>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_PRICING")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
  <%} else {%>
          <%if (!(index.equals("titleList") || index.equals("titleDetail") || 
                  index.equals("contentList") || index.equals("contentDetail"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=ctrp&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_PRICING")%></FONT></A>&nbsp;</TD>
  <%}%>

  <%if (index.equals("compList") || index.equals("compAdd") || index.equals("compEdit") || 
        index.equals("compcatList") || index.equals("compcatAdd") || index.equals("compcatEdit")) {%>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_COMP")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
  <%} else {%>
          <%if (!(index.equals("ctpList") || index.equals("ctpAdd") || index.equals("ctpEdit") ||
                  index.equals("ctrpList") || index.equals("ctrpEdit"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=comp&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_COMP")%></FONT></A>&nbsp;</TD>
  <%}%>

  <%if (index.equals("userEdit")) {%>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_USER_ACCOUNT")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
  <%} else {%>
          <%if (!(index.equals("compList") || index.equals("compAdd") || index.equals("compEdit") ||
                  index.equals("compcatList") || index.equals("compcatAdd") || index.equals("compcatEdit"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=user&action=edit"><FONT class="mainMenuText"><%=session.getAttribute("MENU_USER_ACCOUNT")%></FONT></A>&nbsp;</TD>
  <%}%>

  <%if (!(index.equals("userEdit"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="javascript:getHelp('<%=index%>')"><FONT class="mainMenuText"><%=session.getAttribute("MENU_HELP")%></FONT></A>&nbsp;</TD>
          <TD bgcolor="#336699" nowrap width="100%" algin=right background="/images/menu.off.bg.gif">&nbsp;</TD>
        </TR>
      </TABLE>

<%} else if (role!=null && role.toString().equals(ROLE_BU_CS)) {%>

      <TABLE border=0 cellpadding=0 cellspacing=0 width=100% bgcolor="#efefef">
        <TR>

  <%if (index.equals("regionList") || index.equals("regionEdit")) {%>
          <TD><IMG src="/images/menu.off.start.gif"></TD>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_REGIONS")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
  <%} else {%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;&nbsp;<A class="mainMenuHeader" HREF="serv?type=region&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_REGIONS")%></FONT></A>&nbsp;</TD>
  <%}%>

  <%if (index.equals("retailerList") || index.equals("retailerAdd") || index.equals("retailerEdit") ||
        index.equals("storeList") || index.equals("storeAdd") || index.equals("storeEdit")) {%>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_CHANNELS")%> </FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
  <%} else {%>
          <%if (!(index.equals("regionList") || index.equals("regionEdit"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=retailer&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_CHANNELS")%></FONT></A>&nbsp;</TD> 
  <%}%>

  <%if (index.equals("titleList") || index.equals("titleDetail") || 
        index.equals("contentList") || index.equals("contentDetail")) {%>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_CDS")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
  <%} else {%>
          <%if (!(index.equals("retailerList") || index.equals("retailerAdd") || index.equals("retailerEdit") || 
                  index.equals("storeList") || index.equals("storeAdd") || index.equals("storeEdit"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=content&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_CDS")%></FONT></A>&nbsp;</TD>
  <%}%>

  <%if (index.equals("ctrpList") || index.equals("ctrpEdit")) {%>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_PRICING")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
  <%} else {%>
          <%if (!(index.equals("titleList") || index.equals("titleDetail") || 
                  index.equals("contentList") || index.equals("contentDetail"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=ctrp&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_PRICING")%></FONT></A>&nbsp;</TD>
  <%}%>

  <%if (index.equals("compList") || index.equals("compAdd") || index.equals("compEdit") || 
        index.equals("compcatList") || index.equals("compcatAdd") || index.equals("compcatEdit")) {%>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_COMP")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
  <%} else {%>
          <%if (!(index.equals("ctrpList") || index.equals("ctrpEdit"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=comp&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_COMP")%></FONT></A>&nbsp;</TD>
  <%}%>

  <%if (index.equals("ectList") || index.equals("ecbList") ||
        index.equals("ecbEdit") || index.equals("ecDetail") || index.equals("ecInfo")) {%>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_ECARDS")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
  <%} else {%>
          <%if (!(index.equals("compList") || index.equals("compAdd") || index.equals("compEdit") ||
                  index.equals("compcatList") || index.equals("compcatAdd") || index.equals("compcatEdit"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=ec&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_ECARDS")%></FONT></A>&nbsp;</TD>
  <%}%>

  <%if (index.equals("playerDetail")) {%>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_PLAYERS")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
  <%} else {%>
          <%if (!(index.equals("ectList") || index.equals("ecbList") || 
                  index.equals("ecbEdit") || index.equals("ecDetail") || index.equals("ecInfo"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=player&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_PLAYERS")%></FONT></A>&nbsp;</TD>
  <%}%>

  <%if (index.equals("userEdit")) {%>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_USER_ACCOUNT")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
  <%} else {%>
          <%if (!(index.equals("playerDetail"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=user&action=edit"><FONT class="mainMenuText"><%=session.getAttribute("MENU_USER_ACCOUNT")%></FONT></A>&nbsp;</TD>
  <%}%>

  <%if (!(index.equals("userEdit"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="javascript:getHelp('<%=index%>')"><FONT class="mainMenuText"><%=session.getAttribute("MENU_HELP")%></FONT></A>&nbsp;</TD>
          <TD bgcolor="#336699" nowrap width="100%" align=right background="/images/menu.off.bg.gif">&nbsp;</TD>
        </TR>
      </TABLE>

<%} else if (role!=null && role.toString().equals(ROLE_BU_CSUP)) {%>

      <TABLE border=0 cellpadding=0 cellspacing=0 width=100% bgcolor="#efefef">
        <TR>

  <%if (index.equals("ectList") || index.equals("ecbList") ||
        index.equals("ecbEdit") || index.equals("ecDetail") || index.equals("ecInfo")) {%>
          <TD><IMG src="/images/menu.off.start.gif"></TD>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_ECARDS")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
  <%} else {%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=eci&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_ECARDS")%></FONT></A>&nbsp;</TD>
  <%}%>

  <%if (index.equals("playerDetail")) {%>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_PLAYERS")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
  <%} else {%>
          <%if (!(index.equals("ectList") || index.equals("ecbList") || 
                  index.equals("ecbEdit") || index.equals("ecDetail") || index.equals("ecInfo"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=player&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_PLAYERS")%></FONT></A>&nbsp;</TD>
  <%}%>

  <%if (index.equals("userEdit")) {%>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_USER_ACCOUNT")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
  <%} else {%>
          <%if (!(index.equals("playerDetail"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=user&action=edit"><FONT class="mainMenuText"><%=session.getAttribute("MENU_USER_ACCOUNT")%></FONT></A>&nbsp;</TD>
  <%}%>

  <%if (!(index.equals("userEdit"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="javascript:getHelp('<%=index%>')"><FONT class="mainMenuText"><%=session.getAttribute("MENU_HELP")%></FONT></A>&nbsp;</TD>
          <TD bgcolor="#336699" nowrap width="100%" align=right background="/images/menu.off.bg.gif">&nbsp;</TD>
        </TR>
      </TABLE>

<%} else if (role!=null && role.toString().equals(ROLE_BU_ECARD)) {%>

      <TABLE border=0 cellpadding=0 cellspacing=0 width=100% bgcolor="#efefef">
        <TR>

  <%if (index.equals("ectList") || index.equals("ecbList") ||
        index.equals("ecbEdit") || index.equals("ecDetail") || index.equals("ecInfo")){%>
          <TD><IMG src="/images/menu.off.start.gif"></TD>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_ECARDS")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
  <%} else {%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=ec&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_ECARDS")%></FONT></A>&nbsp;</TD>
  <%}%>

  <%if (index.equals("userEdit")) {%>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_USER_ACCOUNT")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
  <%} else {%>
          <%if (!(index.equals("ectList") || index.equals("ecbList") || 
                  index.equals("ecbEdit") || index.equals("ecDetail"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=user&action=edit"><FONT class="mainMenuText"><%=session.getAttribute("MENU_USER_ACCOUNT")%></FONT></A>&nbsp;</TD>
  <%}%>

  <%if (!(index.equals("userEdit"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="javascript:getHelp('<%=index%>')"><FONT class="mainMenuText"><%=session.getAttribute("MENU_HELP")%></FONT></A>&nbsp;</TD>
          <TD bgcolor="#336699" nowrap width="100%" align=right background="/images/menu.off.bg.gif">&nbsp;</TD>
        </TR>
      </TABLE>

<%} else if (role!=null && role.toString().equals(ROLE_BU_SSUP)) {%>

      <TABLE border=0 cellpadding=0 cellspacing=0 width=100% bgcolor="#efefef">
        <TR>

  <%if (index.equals("ecInfo")){%>
          <TD><IMG src="/images/menu.off.start.gif"></TD>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_ECARDS")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
  <%} else {%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=ec&action=list"><FONT class="mainMenuText"><%=session.getAttribute("MENU_ECARDS")%></FONT></A>&nbsp;</TD>
  <%}%>

  <%if (index.equals("userEdit")) {%>
          <TD><IMG src="/images/menu.off.on.separator.gif"></TD>
          <TD nowrap background="/images/menu.on.bg.gif">&nbsp;<FONT class="mainMenuSelected"><%=session.getAttribute("MENU_USER_ACCOUNT")%></FONT>&nbsp;&nbsp;</TD>
          <TD><IMG src="/images/menu.on.off.separator.gif"></TD>
  <%} else {%>
          <%if (!(index.equals("ectList") || index.equals("ecbList") || 
                  index.equals("ecbEdit") || index.equals("ecDetail"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="serv?type=user&action=edit"><FONT class="mainMenuText"><%=session.getAttribute("MENU_USER_ACCOUNT")%></FONT></A>&nbsp;</TD>
  <%}%>

  <%if (!(index.equals("userEdit"))) {%><TD><IMG src="/images/menu.off.off.separator.gif"></TD><%}%>
          <TD nowrap background="/images/menu.off.bg.gif">&nbsp;<A class="mainMenuHeader" HREF="javascript:getHelp('<%=index%>')"><FONT class="mainMenuText"><%=session.getAttribute("MENU_HELP")%></FONT></A>&nbsp;</TD>
          <TD bgcolor="#336699" nowrap width="100%" align=right background="/images/menu.off.bg.gif">&nbsp;</TD>
        </TR>
      </TABLE>

<%}%>


     <!-- End of Menu Bar -->
    </TD>
    <TD nowrap align="right" background="/images/menu.off.bg.gif">
      <font class="userText"><%=session.getAttribute("BCC_USER")%>: </font>
      <a class="logoutMenuHeader" href="login?laction=logout" id="logout" target="_top">
      <font class="logoutText"><%=session.getAttribute("TEXT_LOGOUT")%></font></a>&nbsp;&nbsp;
    </TD>
  </TR>
    <!-- Small Gray Bar -->
  <TR width="100%" bgcolor="#efefef">
    <TD colspan="2"><IMG border="0" width="1" height="5" src="/images/spacer.gif"></TD>
  </TR>
</TABLE>

  <!-- Start of 1st Sub-Menu Bar -->

 <TABLE width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#efefef">
   <TR width="100%" height="15">
<%if (role!=null && role.toString().equals(ROLE_BU_MKTG)) {%>
<%if (index.equals("ctpList") || index.equals("ctpAdd") || index.equals("ctpEdit") || 
      index.equals("ctrpList") || index.equals("ctrpEdit")) {%>
  <%if (index.equals("ctrpList") || index.equals("ctrpEdit")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_CTRP")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ctrp&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_CTRP")%></FONT></A></TD>
  <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("ctpList") || index.equals("ctpAdd") || index.equals("ctpEdit")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_CTP")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ctp&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_CTP")%></FONT></A></TD>
  <%}%>
  <!-- End of 1st Sub-Menu Bar for Pricing -->
<%}%>
<%}%>

<%if (index.equals("titleList") || index.equals("titleDetail") ||
      index.equals("contentList") || index.equals("contentDetail")) {%>
  <%if (index.equals("contentList") || index.equals("contentDetail")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_CONTENT")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=content&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_CONTENT")%></FONT></A></TD>
  <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("titleList") || index.equals("titleDetail")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_TITLE")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=title&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_TITLE")%></FONT></A></TD>
  <%}%>
  <!-- End of 1st Sub-Menu Bar for Content -->

<%}%>

<%if (index.equals("retailerList") || index.equals("retailerAdd") || index.equals("retailerEdit") ||
       index.equals("storeList") || index.equals("storeAdd") || index.equals("storeEdit")) {%>
  <%if (index.equals("retailerList") || index.equals("retailerAdd") || index.equals("retailerEdit")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_RETAILERS")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=retailer&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_RETAILERS")%></FONT></A></TD>
  <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("storeList") || index.equals("storeAdd") || index.equals("storeEdit")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_STORES")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=store&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_STORES")%></FONT></A></TD>
  <%}%>
  <!-- End of 1st Sub-Menu Bar for Channels -->
<%}%>

<%if (index.equals("ectList") || index.equals("ecbList") ||
        index.equals("ecbEdit") || index.equals("ecDetail") || index.equals("ecInfo")) {%>
  <%if (index.equals("ecDetail")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_ECARDS")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ec&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_ECARDS")%></FONT></A></TD>
  <%}%>
  <%if (role!=null && (role.toString().equals(ROLE_BU_ECARD) || role.toString().equals(ROLE_BU_CS))) {%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
    <%if (index.equals("ecbList") || index.equals("ecbEdit")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_ECARD_BATCHES")%></FONT></TD>
    <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ecb&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_ECARD_BATCHES")%></FONT></A></TD>
    <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
    <%if (index.equals("ectList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_ECARD_TYPE")%></FONT></TD>
    <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ect&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_ECARD_TYPE")%></FONT></A></TD>
    <%}%>
  <%}%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("ecInfo")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_ECARD_INFO")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=eci&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_ECARD_INFO")%></FONT></A></TD>
  <%}%>  
  <!-- End of 1st Sub-Menu Bar for ECards -->
<%}%>

      <TD nowrap width="100%" align="right"><A class="subMenuHeader" href="serv?type=home&action=edit" id="currole"><FONT class="subMenuText"><%=session.getAttribute("BCC_ROLE_NAME")%></FONT></A>&nbsp;&nbsp;</TD>
    </TR>
  </TABLE>


<!-- Bar under menu -->
<TABLE width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#efefef">
  <TR width="100%">
    <TD><IMG border="0" width="1" height="5" src="/images/spacer.gif"></TD>
  </TR>
  <TR width="100%" bgcolor="#999999">
    <TD><IMG border="0" width="1" height="1" src="/images/spacer.gif"></TD>
  </TR>
</TABLE>
<TABLE width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
  <TR width="100%">
    <TD><IMG border="0" width="1" height="5" src="/images/spacer.gif"></TD>
  </TR>
</TABLE>
<!-- End of bar -->

  <!-- Start of 2nd Sub-Menu Bar -->
<center>
<%if (role!=null && role.toString().equals(ROLE_BU_ADMIN)) {%>

 <%if (index.equals("userList") || index.equals("userAdd") || index.equals("userEdit")) {%>

  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">

  <%if (index.equals("userList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_OPUSERLIST")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=user&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_OPUSERLIST")%></FONT></A></TD>
  <%}%>

      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("userAdd")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_OPUSERADD")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=user&action=add"><FONT class="subMenuText"><%=session.getAttribute("MENU_OPUSERADD")%></FONT></A></TD>
  <%}%>

  <%if (index.equals("userEdit")){%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_OPUSERUPDATE")%></FONT></TD>
  <%}%>

    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for Users -->

 <%} else if (index.equals("regionalCenterList") || index.equals("regionalCenterAdd") || index.equals("regionalCenterEdit")) {%>

  <!-- Start of Sub-Menu Bar -->
  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">

  <%if (index.equals("regionalCenterList")) {%>
     <%if (!rcflag.equals("0")) {%>
       <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_RCLIST")%></FONT></TD>
     <%}%>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=regionalCenter&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_RCLIST")%></FONT></A></TD>
  <%}%>

  <%if (index.equals("regionalCenterAdd") && !rcflag.equals("0")) {%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_RCADD")%></FONT></TD>
  <%} else if (!rcflag.equals("0")) {%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=regionalCenter&action=add"><FONT class="subMenuText"><%=session.getAttribute("MENU_RCADD")%></FONT></A></TD>
  <%}%>

  <%if (index.equals("regionalCenterEdit")){%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_RCUPDATE")%></FONT></TD>
  <%}%>

    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for Regional Centers -->

 <%} else if (index.equals("retailerEdit")) {%>

  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=retailer&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_RLLIST")%> </FONT></A></TD>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_RLDETAILS")%></FONT></TD>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of Sub-Menu Bar for Retailers -->

 <%} else if (index.equals("storeEdit")) {%>

  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=store&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_STLIST")%> </FONT></A></TD>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_STDETAILS")%></FONT></TD>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of Sub-Menu Bar for Stores -->

 <%}%>

<%} else if (role!=null && role.toString().equals(ROLE_BU_MKTG)) {%>

 <%if (index.equals("regionList") || index.equals("regionAdd") || index.equals("regionEdit")) {%>
  <!-- Start of Sub-Menu Bar -->
  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">

  <%if (index.equals("regionList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_RGLIST")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=region&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_RGLIST")%></FONT></A></TD>
  <%}%>

      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("regionAdd")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_RGADD")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=region&action=add"><FONT class="subMenuText"><%=session.getAttribute("MENU_RGADD")%></FONT></A></TD>
  <%}%>

  <%if (index.equals("regionEdit")){%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_RGUPDATE")%></FONT></TD>
  <%}%>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for Regions -->

 <%} else if (index.equals("retailerEdit")) {%>

  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=retailer&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_RLLIST")%> </FONT></A></TD>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_RLDETAILS")%></FONT></TD>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of Sub-Menu Bar for Retailers -->

 <%} else if (index.equals("storeEdit")) {%>

  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=store&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_STLIST")%> </FONT></A></TD>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_STDETAILS")%></FONT></TD>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of Sub-Menu Bar for Stores -->

 <%} else if (index.equals("ctpList") || index.equals("ctpAdd") || index.equals("ctpEdit")) {%>
  <!-- Start of Sub-Menu Bar -->
  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">

  <%if (index.equals("ctpList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_CTPLIST")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ctp&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_CTPLIST")%></FONT></A></TD>
  <%}%>

      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("ctpAdd")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_CTPADD")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ctp&action=add"><FONT class="subMenuText"><%=session.getAttribute("MENU_CTPADD")%></FONT></A></TD>
  <%}%>

  <%if (index.equals("ctpEdit")){%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_CTPUPDATE")%></FONT></TD>
  <%}%>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for Pricing -->

 <%} else if (index.equals("ctrpList") || index.equals("ctrpEdit")) {%>

  <!-- Start of Sub-Menu Bar -->
  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">

  <%if (index.equals("ctrpList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_CTRPLIST")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ctrp&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_CTRPLIST")%></FONT></A></TD>
  <%}%>

  <%if (index.equals("ctrpEdit")){%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_CTRPUPDATE")%></FONT></TD>
  <%}%>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for Content-Title-Region-Pricings  -->

 <%} else if (index.equals("compList") || index.equals("compAdd") || index.equals("compEdit") ||
              index.equals("compcatList") || index.equals("compcatAdd") || index.equals("compcatEdit")) {%>

  <!-- Start of Sub-Menu Bar -->
  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">

  <%if (index.equals("compList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_COMPLIST")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=comp&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_COMPLIST")%></FONT></A></TD>
  <%}%>

      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("compAdd")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_COMPADD")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=comp&action=add"><FONT class="subMenuText"><%=session.getAttribute("MENU_COMPADD")%></FONT></A></TD>
  <%}%>

  <%if (index.equals("compEdit") ||index.equals("compcatList") || index.equals("compcatAdd") || index.equals("compcatEdit")) {%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_COMPUPDATE")%></FONT></TD>
  <%}%>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for Competitions -->

 <%}%>

<%} else if (role!=null && role.toString().equals(ROLE_BU_CS)) {%>

 <%if (index.equals("regionEdit")){%>

  <!-- Start of Sub-Menu Bar -->
  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=region&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_RGLIST")%></FONT></A></TD>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_RGDETAILS")%></FONT></TD>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for Regions -->

 <%} else if (index.equals("ctrpEdit")){%>

  <!-- Start of Sub-Menu Bar -->
  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ctrp&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_CTRPLIST")%></FONT></A></TD>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_CTRPDETAILS")%></FONT></TD>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for Content-Title-Region-Pricings -->
 
 <%} else if (index.equals("compEdit") || index.equals("compcatList") || index.equals("compcatEdit")) {%>

  <!-- Start of Sub-Menu Bar -->
  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=comp&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_COMPLIST")%></FONT></A></TD>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_COMPDETAILS")%></FONT></TD>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for Competitions -->
 
 <%} else if (index.equals("retailerList") || index.equals("retailerAdd") || index.equals("retailerEdit")) {%>

  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">
  <%if (index.equals("retailerList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_RLLIST")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=retailer&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_RLLIST")%> </FONT></A></TD>
  <%}%>

      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("retailerAdd")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_RLADD")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=retailer&action=add"><FONT class="subMenuText"><%=session.getAttribute("MENU_RLADD")%> </FONT></A></TD>
  <%}%>

  <%if (index.equals("retailerEdit")) {%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_RLUPDATE")%></FONT></TD>
  <%}%>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of Sub-Menu Bar -->

 <%} else if (index.equals("storeList") || index.equals("storeAdd") || index.equals("storeEdit")) {%>

  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">
  <%if (index.equals("storeList")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_STLIST")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=store&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_STLIST")%> </FONT></A></TD>
  <%}%>

      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
  <%if (index.equals("storeAdd")) {%>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_STADD")%></FONT></TD>
  <%} else {%>
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=store&action=add"><FONT class="subMenuText"><%=session.getAttribute("MENU_STADD")%> </FONT></A></TD>
  <%}%>

  <%if (index.equals("storeEdit")) {%>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_STUPDATE")%></FONT></TD>
  <%}%>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of Sub-Menu Bar -->

 <%}%>

<%}%>

 <%if (index.equals("titleDetail")) {%>

  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=title&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_TLLIST")%></FONT></A></TD>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_TLDETAIL")%></FONT></TD>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for Content Title -->

 <%} else if (index.equals("contentDetail")) {%>

  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=content&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_CONLIST")%></FONT></A></TD>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_CONDETAIL")%></FONT></TD>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for Content Object -->

 <%} else if ( index.equals("ecbEdit")) {%>
  <TABLE width="20%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
    <TR width="100%" height="15">
      <TD nowrap>&nbsp;&nbsp;<A class="subMenuHeader" HREF="serv?type=ecb&action=list"><FONT class="subMenuText"><%=session.getAttribute("MENU_ECARD_BATCHES_LIST")%></FONT></A></TD>
      <TD height="15" class="subMenuBar">&nbsp;&nbsp;|</TD>
      <TD nowrap>&nbsp;&nbsp;<FONT class="subMenuSelected"><%=session.getAttribute("MENU_ECARD_BATCHES_DETAIL")%></FONT></TD>
    <TD width="100%">&nbsp;</TD>
    </TR>
  </TABLE>
  <!-- End of 2nd Sub-Menu Bar for ECard Batches -->

 <%}%>

</center>
<p>


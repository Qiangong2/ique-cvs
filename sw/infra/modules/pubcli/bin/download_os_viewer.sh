#!/bin/sh 

OSID="0000000200000001"
D_OSID="0000000200000003"
VIEWERID="0000000200000002"
OS_FILENAME="os.mrg"
D_OS_FILENAME="d_os.mrg"
VIEWER_FILENAME="viewer.mrg"

if [ "$1" == "wii" ]; then
  OSID="0000000100000003"
  VIEWERID="0000000100000002"
  OS_FILENAME="os.img"
  VIEWER_FILENAME="viewerD.elf"
fi

if [ -z "$ROOT" ]; then
  echo "download_os_viewer.sh:  ROOT not defined"
  exit 1
fi

if [ -z "$CCS_HOST" ]; then
  echo "download_os_viewer.sh:  CCS_HOST not defined"
  exit 1
fi

#  Download latest NC OS and Viewer COMMON TICKET, TMD, and MRG

TMDSIZE=520
TIKSIZE=676



# Downoad OS TMD
wget -O os.tmd_certs "http://$CCS_HOST:16983/ccs/download/$OSID/tmd"
dd if=os.tmd_certs of=os.tmd bs=1 count=$TMDSIZE
dd if=os.tmd_certs of=os.cpcerts bs=1 skip=$TMDSIZE

# Download OS CETK
wget -O os.cetk "http://$CCS_HOST:16983/ccs/download/$OSID/cetk"
dd if=os.cetk of=os.tik bs=1 count=$TIKSIZE
dd if=os.cetk of=os.etcerts bs=1 skip=$TIKSIZE

# Download OS MRG
OSCID=$($ROOT/usr/bin/x86/tmd --IN os.tmd -p | grep cid | sed -e 's/.*0x//')
wget -O  $OS_FILENAME "http://$CCS_HOST:16983/ccs/download/$OSID/$OSCID"

if [ -n "$D_OSID" ]; then
    wget -O d_os.tmd_certs "http://$CCS_HOST:16983/ccs/download/$D_OSID/tmd"
    dd if=d_os.tmd_certs of=d_os.tmd bs=1 count=$TMDSIZE
    dd if=d_os.tmd_certs of=d_os.cpcerts bs=1 skip=$TMDSIZE

    # Download OS CETK
    wget -O d_os.cetk "http://$CCS_HOST:16983/ccs/download/$D_OSID/cetk"
    dd if=d_os.cetk of=d_os.tik bs=1 count=$TIKSIZE
    dd if=d_os.cetk of=d_os.etcerts bs=1 skip=$TIKSIZE

    # Download OS MRG
    D_OSCID=$($ROOT/usr/bin/x86/tmd --IN os.tmd -p | grep cid | sed -e 's/.*0x//')
    wget -O $D_OS_FILENAME "http://$CCS_HOST:16983/ccs/download/$D_OSID/$D_OSCID"
fi


# Downoad VIEWER TMD
wget -O viewer.tmd_certs "http://$CCS_HOST:16983/ccs/download/$VIEWERID/tmd"
dd if=viewer.tmd_certs of=viewer.tmd bs=1 count=$TMDSIZE
dd if=viewer.tmd_certs of=viewer.cpcerts bs=1 skip=$TMDSIZE

# Download VIEWER CETK
wget -O viewer.cetk "http://$CCS_HOST:16983/ccs/download/$VIEWERID/cetk"
dd if=viewer.cetk of=viewer.tik bs=1 count=$TIKSIZE
dd if=viewer.cetk of=viewer.etcerts bs=1 skip=$TIKSIZE


# Download VIEWER MRG
VIEWERCID=$($ROOT/usr/bin/x86/tmd --IN viewer.tmd -p | grep cid | sed -e 's/.*0x//')
wget -O $VIEWER_FILENAME "http://$CCS_HOST:16983/ccs/download/$VIEWERID/$VIEWERCID"




#!/bin/sh

# ETS_HOST should be defined as an env variable
#
# The first parameter is the title directory.
#
# This script loads the 'properties' file in the title directory.
#
# The properties file must define:
#   TITLE_NAME
#   TITLE_TYPE
#   ACTION_TYPE      
#   ACCESS_RIGHTS

TITLE_DIR=$1
if [ ! -d "$TITLE_DIR" ]; then
  echo "Usage: publish.sh <title directory>"
  exit 1;
fi

if [ -z "$ETS_HOST" ]; then
  echo "publish.sh:  ETS_HOST not defined"
  exit 1
fi

cd $TITLE_DIR
source properties

if [ -z "$TITLE_DIR" ]; then
  echo "publish.sh:  TITLE_DIR not defined"
  exit 1
fi
if [ -z "$TITLE_TYPE" ]; then
  echo "publish.sh:  TITLE_TYPE not defined"
  exit 1
fi

title_id=$(basename $TITLE_DIR)

/opt/broadon/pkgs/cls/bin/getTicket -h $ETS_HOST -t $TITLE_TYPE -e $title_id.tik -c $title_id.etcerts $(basename $TITLE_DIR)


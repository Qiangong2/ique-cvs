#!/bin/sh

if [ -z "$1" ]; then
  echo "Usage: upload_met.sh <title_dir>"
  exit 1
fi

title_dir=$1
title_id=$(basename $title_dir)
met_file=$(echo $title_dir/*.met)

if [ ! -e "$met_file" ]; then
  echo "upload_met.sh: couldn't find met file"
  exit 1
fi

echo "Uploading $met_file as MET for title $title_id"
/opt/broadon/pkgs/cas/bin/upload.sh $CAS_DB $title_id $met_file MET

#!/bin/sh

if [ -z "$1" ]; then
  echo "Usage: upload_cetk.sh <title_dir>"
  exit 1
fi

title_dir=$1
title_id=$(basename $title_dir)
tik_file=$(echo $title_dir/$title_id.tik)
certs_file=$(echo $title_dir/$title_id.etcerts)

if [ ! -e "$tik_file" ]; then
  echo "upload_met.sh: couldn't find $title_dir.tik file"
  exit 1
fi
if [ ! -e "$certs_file" ]; then
  echo "upload_met.sh: couldn't find $title_dir.etcerts file"
  exit 1
fi
cat $tik_file $certs_file > $title_dir/$title_id.cetk

echo "Uploading $tik_file as COMMON_TICKET for title $title_id"
/opt/broadon/pkgs/cas/bin/upload.sh $CAS_DB $title_id $title_dir/$title_id.cetk CETK

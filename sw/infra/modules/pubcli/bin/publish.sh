#!/bin/sh

# CPS_HOST should be defined as an env variable
#
# The first parameter is the title directory.
#
# This script loads the 'properties' file in the title directory.
#
# The properties file must define:
#   TITLE_NAME
#   TITLE_TYPE
#   ACTION_TYPE      
#   ACCESS_RIGHTS
#   OS_TITLE_ID
#
# The properties file may optionally define:
#   ALLOW_COMMON_TICKET  (1 = set allow common ticket to true)

TITLE_DIR=$1
if [ ! -d "$TITLE_DIR" ]; then
  echo "Usage: publish.sh <title directory>"
  exit 1;
fi

if [ -z "$CPS_HOST" ]; then
  echo "publish.sh:  CPS_HOST not defined"
  exit 1
fi

cd $TITLE_DIR
source properties
act=""

if [ -z "$TITLE_NAME" ]; then
  echo "publish.sh:  TITLE_NAME not defined"
  exit 1
fi
if [ -z "$TITLE_DIR" ]; then
  echo "publish.sh:  TITLE_DIR not defined"
  exit 1
fi
if [ -z "$ACTION_TYPE" ]; then
  echo "publish.sh:  ACTION_TYPE not defined"
  exit 1
fi
if [ -z "$TITLE_TYPE" ]; then
  echo "publish.sh:  TITLE_TYPE not defined"
  exit 1
fi
if [ -z "$ACCESS_RIGHTS" ]; then
  echo "publish.sh:  ACCESS_RIGHTS not defined"
  exit 1
fi
if [ -z "$OS_TITLE_ID" ]; then
  echo "publish.sh:  OS_TITLE_ID not defined"
  exit 1
fi
if [ "$ALLOW_COMMON_TICKET" == "1" ]; then
  act="-c" 
fi

wslib=/opt/broadon/pkgs/wslib/jar
pkg=/opt/broadon/pkgs/pubcli
lib=$pkg/lib
java=/opt/broadon/pkgs/jre/bin/java
maxmem=-Xmx512m

CLASSPATH=$lib/pubcli.jar:$lib/common.jar:$lib/wsclient.jar:$wslib/activation.jar:$wslib/axis_ant.jar:$wslib/axis-schema.jar:$wslib/axis.jar:$wslib/commons-discovery-0.2.jar:$wslib/commons-logging-1.0.4.jar:$wslib/jaxrpc.jar:$wslib/mail.jar:$wslib/saaj.jar:$wslib/wsdl4j-1.5.1.jar:$wslib/log4j-1.2.8.jar
export LANG=en_US.UTF-8

$java $maxmem -classpath $CLASSPATH com.broadon.pubcli.Publish -t "$TITLE_NAME" -d $(pwd) -a $ACTION_TYPE -s $CPS_HOST -y $TITLE_TYPE -r $ACCESS_RIGHTS -o $OS_TITLE_ID $act

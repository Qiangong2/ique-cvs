/**
 * PointsConfirmationSoapBindingImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package net.wiivc.services;

public class PointsConfirmationSoapBindingImpl implements net.wiivc.services.PointsConfirmation{
    public net.wiivc.services.PointsAppResults[] applyPoints(java.lang.String[] transIdList) throws java.rmi.RemoteException {
        PointsAppResults[] results = new PointsAppResults[transIdList.length];
        for (int i=0; i<transIdList.length; i++) {
            results[i] = new PointsAppResults();
            results[i].setTransId(transIdList[i]);
            results[i].setAppliedResult("SUCCESS");
        }
        return results;
    }
}

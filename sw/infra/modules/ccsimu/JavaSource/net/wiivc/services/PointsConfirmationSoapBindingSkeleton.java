/**
 * PointsConfirmationSoapBindingSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package net.wiivc.services;

import javax.xml.rpc.ServiceException;
import javax.xml.rpc.server.ServiceLifecycle;

public class PointsConfirmationSoapBindingSkeleton implements ServiceLifecycle,
        net.wiivc.services.PointsConfirmation, org.apache.axis.wsdl.Skeleton {
    private net.wiivc.services.PointsConfirmation impl;
    private static java.util.Map _myOperations = new java.util.Hashtable();
    private static java.util.Collection _myOperationsList = new java.util.ArrayList();

    /**
    * Returns List of OperationDesc objects with this name
    */
    public static java.util.List getOperationDescByName(java.lang.String methodName) {
        return (java.util.List)_myOperations.get(methodName);
    }

    /**
    * Returns Collection of OperationDescs
    */
    public static java.util.Collection getOperationDescs() {
        return _myOperationsList;
    }

    static {
        org.apache.axis.description.OperationDesc _oper;
        org.apache.axis.description.FaultDesc _fault;
        org.apache.axis.description.ParameterDesc [] _params;
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://services.wiivc.net", "TransIdList"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String[].class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("applyPoints", _params, new javax.xml.namespace.QName("http://services.wiivc.net", "applyPointsReturn"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://services.wiivc.net", "PointsAppResults"));
        _oper.setElementQName(new javax.xml.namespace.QName("http://services.wiivc.net", "applyPoints"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("applyPoints") == null) {
            _myOperations.put("applyPoints", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("applyPoints")).add(_oper);
    }

    public PointsConfirmationSoapBindingSkeleton() {
        this.impl = new net.wiivc.services.PointsConfirmationSoapBindingImpl();
    }

    public PointsConfirmationSoapBindingSkeleton(net.wiivc.services.PointsConfirmation impl) {
        this.impl = impl;
    }
    public net.wiivc.services.PointsAppResults[] applyPoints(java.lang.String[] transIdList) throws java.rmi.RemoteException
    {
        net.wiivc.services.PointsAppResults[] ret = impl.applyPoints(transIdList);
        return ret;
    }
    public void init(Object context) throws ServiceException    
    {   
        if (impl instanceof ServiceLifecycle) {  
            ((ServiceLifecycle) impl).init(context);  
        }  
    }  
    
    public void destroy()  
    {   
        if (impl instanceof ServiceLifecycle) {  
            ((ServiceLifecycle) impl).destroy();  
        }  
    }
}

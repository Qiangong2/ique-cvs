/**
 * VCRefund.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package net.wiivc.services;

public interface VCRefund extends java.rmi.Remote {
    public java.lang.String doRefund(net.wiivc.services.RefundTxn thisRefund) throws java.rmi.RemoteException;
}

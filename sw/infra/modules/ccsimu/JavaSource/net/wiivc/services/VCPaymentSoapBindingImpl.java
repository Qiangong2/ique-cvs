/**
 * VCPaymentSoapBindingImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package net.wiivc.services;

import java.math.BigDecimal;
import java.util.Date;

public class VCPaymentSoapBindingImpl implements net.wiivc.services.VCPayment {
    public net.wiivc.services.AuthTxn sendATxn(net.wiivc.services.AuthTxn thisTxn) throws java.rmi.RemoteException {
        String functionCode = thisTxn.getFunctionCode();
        try {
          if (functionCode == null)
              throw new IllegalArgumentException("missing functionCode param");
          if (functionCode.equalsIgnoreCase("CALC_PAYMENT")) {
            // Check credit card number present
            String creditCardStr = thisTxn.getCardNum();
            if (creditCardStr == null || creditCardStr.trim().length() == 0) {
                thisTxn.setVcMessage("Missing card number");
                thisTxn.setVcMsgCode(new String[] {"cardNumber"});
                throw new IllegalArgumentException("missing card number");                
            }
            // total tax and total paid
            String totalAmountStr = thisTxn.getTotalAmount();
            if (totalAmountStr == null) {
                thisTxn.setVcMessage("Missing total amount");
                thisTxn.setVcMsgCode(new String[] {"totalAmount"});
                throw new IllegalArgumentException("missing totalAmount param");
            }
            BigDecimal totalAmount = new BigDecimal(totalAmountStr);
            String country = thisTxn.getCountry();
            BigDecimal totalTax = BigDecimal.valueOf(0);
            if (country == null) {
                thisTxn.setVcMessage("Missing country");
                thisTxn.setVcMsgCode(new String[] {"country"});
                throw new IllegalArgumentException("missing country param");
            }
            if (country.equalsIgnoreCase("JP"))
                totalTax = totalAmount.multiply(BigDecimal.valueOf(10, 2));    // 10% tax
            else if (country.equalsIgnoreCase("US"))
                totalTax = totalAmount.multiply(BigDecimal.valueOf(8, 2));    // 8% tax
            else {
                thisTxn.setVcMessage("Unknown country");
                thisTxn.setVcMsgCode(new String[] {"country"});
                throw new IllegalArgumentException("unknown country");
            }
            thisTxn.setTotalTax(totalTax.toString());
            BigDecimal totalPaid = totalAmount.add(totalTax);
            thisTxn.setTotalPaid(totalPaid.toString());
            // invoice modifiers
            InvoiceModifier[] ims = new InvoiceModifier[1];
            InvoiceModifier im = new InvoiceModifier();
            im.setAmount(totalTax.toString());
            if (country.equalsIgnoreCase("JP"))
                im.setDesc("TAX@10%");
            else
                im.setDesc("TAX@8%");
            im.setLineItemType("T");
            ims[0] = im;
            thisTxn.setInvMod(ims);
            // set msgs
            thisTxn.setVcMessage("Press SUBMIT to accept transaction and process your credit card");
            String[] msgCodes = new String[] {"TRANSACTION"};
            thisTxn.setVcMsgCode(msgCodes);
            thisTxn.setResult("SUCCESS");
          } else if (functionCode.equalsIgnoreCase("AUTH_PAYMENT")) {
            thisTxn.setApproveDate(new Date().toString());
            thisTxn.setVcMessage("Your credit card has been approved. Thank you for your order.");
            String[] msgCodes = new String[] {"TRANSACTION"};
            thisTxn.setVcMsgCode(msgCodes);
            thisTxn.setResult("APPROVE");
          } else {
            thisTxn.setVcMessage("Unknown function code");
            thisTxn.setVcMsgCode(new String[] {"functionCode"});
            throw new IllegalArgumentException("unknown function code");
          }
          try {
            Thread.sleep(2000L);    // artificially delay 2 seconds
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        } catch (Exception e) {
            e.printStackTrace();
            thisTxn.setResult("ERROR: "+e.toString());
        }
        return thisTxn;
    }

}

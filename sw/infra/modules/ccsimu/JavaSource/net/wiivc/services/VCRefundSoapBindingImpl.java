/**
 * VCRefundSoapBindingImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package net.wiivc.services;

public class VCRefundSoapBindingImpl implements net.wiivc.services.VCRefund{
    public java.lang.String doRefund(net.wiivc.services.RefundTxn thisRefund) throws java.rmi.RemoteException {
        String transId = thisRefund.getTransId();
        return transId + " has been refunded";
    }

}

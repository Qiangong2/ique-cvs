/**
 * VCPaymentSoapBindingSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package net.wiivc.services;

import javax.xml.rpc.ServiceException;
import javax.xml.rpc.server.ServiceLifecycle;

public class VCPaymentSoapBindingSkeleton implements ServiceLifecycle,
        net.wiivc.services.VCPayment, org.apache.axis.wsdl.Skeleton {
    private net.wiivc.services.VCPayment impl;
    private static java.util.Map _myOperations = new java.util.Hashtable();
    private static java.util.Collection _myOperationsList = new java.util.ArrayList();

    /**
    * Returns List of OperationDesc objects with this name
    */
    public static java.util.List getOperationDescByName(java.lang.String methodName) {
        return (java.util.List)_myOperations.get(methodName);
    }

    /**
    * Returns Collection of OperationDescs
    */
    public static java.util.Collection getOperationDescs() {
        return _myOperationsList;
    }

    static {
        org.apache.axis.description.OperationDesc _oper;
        org.apache.axis.description.FaultDesc _fault;
        org.apache.axis.description.ParameterDesc [] _params;
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://services.wiivc.net", "thisTxn"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://services.wiivc.net", "AuthTxn"), net.wiivc.services.AuthTxn.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("sendATxn", _params, new javax.xml.namespace.QName("http://services.wiivc.net", "sendATxnReturn"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://services.wiivc.net", "AuthTxn"));
        _oper.setElementQName(new javax.xml.namespace.QName("http://services.wiivc.net", "sendATxn"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("sendATxn") == null) {
            _myOperations.put("sendATxn", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("sendATxn")).add(_oper);
    }

    public VCPaymentSoapBindingSkeleton() {
        this.impl = new net.wiivc.services.VCPaymentSoapBindingImpl();
    }

    public VCPaymentSoapBindingSkeleton(net.wiivc.services.VCPayment impl) {
        this.impl = impl;
    }
    public net.wiivc.services.AuthTxn sendATxn(net.wiivc.services.AuthTxn thisTxn) throws java.rmi.RemoteException
    {
        net.wiivc.services.AuthTxn ret = impl.sendATxn(thisTxn);
        return ret;
    }
    public void init(Object context) throws ServiceException    
    {   
        if (impl instanceof ServiceLifecycle) {  
            ((ServiceLifecycle) impl).init(context);  
        }  
    }  
    
    public void destroy()  
    {   
        if (impl instanceof ServiceLifecycle) {  
            ((ServiceLifecycle) impl).destroy();  
        }  
    }
}

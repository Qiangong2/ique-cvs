package com.broadon.ecard.test;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;

import com.broadon.bean.Bean;
import com.broadon.bean.XMLFormatWriter;
import com.broadon.db.DBAccessFactory;
import com.broadon.db.DBException;
import com.broadon.db.NameMapManager;
import com.broadon.db.OracleDataSource;
import com.broadon.ecard.db.ECardID;
import com.broadon.ecard.db.ECard;
import com.broadon.ecard.db.ECardFactory;
import com.broadon.test.OracleDS;
import com.broadon.util.Timer;

/**
 * The <code>ECardTest</code> class tests the subclasses of
 * ECard class.
 * The data are retrieved from the database.
 *
 * @version	$Revision: 1.1 $
 */
public class ECardTest
{
    /**
     * Tests the PBean instances.
     *
     * @param	args
     *			args[0]		the eCard id
     */
    public static final void main(String[] args)
	throws Throwable
    {
	long		eCardID = 1262304000;

	if (args.length > 0)
	{
	    eCardID = Long.parseLong(args[0]);
	}
	System.out.println("ECardID[" + eCardID + "]");
	/*
	 * Connect to database.
	 */
	DataSource	ds;

	ds = OracleDS.createDataSource();
	/*
	 * Start testing.
	 */
	String		className = "ECard";
	String		factoryName = "com.broadon.ecard.db." +
				      className +
				      "Factory";
	Class		factoryClass = Class.forName(factoryName);
	Method		singleton = factoryClass.getMethod(
						"getInstance",
						new Class[] { String.class });
	ECardFactory	factory;

	factory = (ECardFactory)singleton.invoke(
						null,
						new String[] { factoryName });
	System.out.println(factory);

	Timer		timer = new Timer();
	NameMapManager	nameMapManager = new NameMapManager("BBname.map");
	Map		nameMap = nameMapManager.getNameMap();
	Bean		bean;

	timer.start();
	factory.beginTransaction(ds, false, false);
	try
	{
	    timer.start();
	    bean = factory.getECard(eCardID, nameMap);
	    timer.stop();
	    if (bean == null)
	    {
		System.err.println("No data");
		return;
	    }
	    System.out.println("Query takes " + timer);

	    Class		beanClass = bean.getClass();
	    PrintWriter		out = new PrintWriter(System.out);

	    System.out.println("XML");
	    new XMLFormatWriter(bean).format(out, 1);
	}
	finally
	{
	    factory.commitTransaction();
	}
    }
}

#!/bin/sh

export BROADON_HOME=/opt/broadon
export BROADON_PKG_HOME=${BROADON_HOME}/pkgs
export JAVA_HOME=${BROADON_PKG_HOME}/jre

export ECARD_DATA=${BROADON_HOME}/data/eCard
export ECARD_HOME=${BROADON_PKG_HOME}/eCard
export WSLIB_HOME=${BROADON_PKG_HOME}/wslib
export JAVALIB_HOME=${BROADON_PKG_HOME}/javalib
export CLASSPATH=${CLASSPATH}:${ECARD_HOME}/lib/eCard.jar
export CLASSPATH=${CLASSPATH}:${WSLIB_HOME}/jar/log4j-1.2.8.jar
export CLASSPATH=${CLASSPATH}:${JAVALIB_HOME}/jar/common.jar
export PATH=${JAVA_HOME}/bin:${PATH}

debug=`/sbin/printconf eCard.debug`
java $debug com.broadon.ecard.ECardExporter -p ${ECARD_HOME}/conf/eCard.properties $*

#!/bin/sh

#DEBUG="-Xdebug -Xnoagent -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5053"

java=/opt/broadon/pkgs/jre/bin/java
java_opts="-Xmx512m $DEBUG"
base=/opt/broadon/pkgs/cls
lib=$base/jar
javalib=/opt/broadon/pkgs/javalib/jar
wslib=/opt/broadon/pkgs/wslib/jar


CLASSPATH=$lib/cls.jar:$javalib/common.jar:$lib/wsclient.jar:$wslib/bcprov-jdk14-131.jar:$wslib/axis.jar:$wslib/jaxrpc.jar:$wslib/kmjava.jar:$wslib/nfjava.jar:$wslib/jutils.jar:$wslib/activation.jar:$wslib/mail.jar:$wslib/axis-ant.jar:$wslib/axis-schema.jar:$wslib/axis.jar:$wslib/commons-discovery-0.2.jar:$wslib/commons-logging-1.0.4.jar:$wslib/jaxrpc.jar:$wslib/saaj.jar:$wslib/wsdl4j-1.5.1.jar:$wslib/log4j-1.2.8.jar

export LANG=en_US.UTF-8

exec $java $java_opts -classpath $CLASSPATH -DCLS_BASE=$base com.broadon.cls.Upload $*

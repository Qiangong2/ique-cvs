#!/bin/sh

java=/opt/broadon/pkgs/jre/bin/java
java_opts="-Xmx512m $DEBUG"
base=/opt/broadon/pkgs/cls
lib=$base/jar
javalib=/opt/broadon/pkgs/javalib/jar
wslib=/opt/broadon/pkgs/wslib/jar


CLASSPATH=$lib/cls.jar:$javalib/common.jar:$lib/wsclient.jar:$wslib/bcprov-jdk14-131.jar:$wslib/axis.jar:$wslib/jaxrpc.jar:$wslib/kmjava.jar:$wslib/nfjava.jar:$wslib/jutils.jar

export LANG=en_US.UTF-8

exec $java  -classpath $CLASSPATH com.broadon.cls.ReadCLS $*

#!/bin/sh

#DEBUG="-Xdebug -Xnoagent -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5053"

TITLE_DIR=$1
if [ ! -d "$TITLE_DIR" ]; then
    echo "Usage: genLic <title_dir>"
    exit 1;
fi

java=/opt/broadon/pkgs/jre/bin/java
java_opts="-Xmx512m $DEBUG"
base=/opt/broadon/pkgs/cls
lib=$base/jar
javalib=/opt/broadon/pkgs/javalib/jar
wslib=/opt/broadon/pkgs/wslib/jar


CLASSPATH=$lib/cls.jar:$javalib/common.jar:$lib/wsclient.jar:$wslib/bcprov-jdk14-131.jar:$wslib/axis.jar:$wslib/jaxrpc.jar:$wslib/kmjava.jar:$wslib/nfjava.jar:$wslib/jutils.jar

export LANG=en_US.UTF-8

exec $java $java_opts -classpath $CLASSPATH -DCLS_BASE=$base com.broadon.cls.Driver $*

/**
 * UpdateAccountAttributesRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ias;

public class UpdateAccountAttributesRequestType  extends com.broadon.wsapi.ias.AbstractAccountRequestType  implements java.io.Serializable {
    private com.broadon.wsapi.ias.AttributeType[] accountAttributes;
    private int[] unsetAccountAttributes;

    public UpdateAccountAttributesRequestType() {
    }

    public UpdateAccountAttributesRequestType(
           com.broadon.wsapi.ias.AttributeType[] accountAttributes,
           int[] unsetAccountAttributes) {
           this.accountAttributes = accountAttributes;
           this.unsetAccountAttributes = unsetAccountAttributes;
    }


    /**
     * Gets the accountAttributes value for this UpdateAccountAttributesRequestType.
     * 
     * @return accountAttributes
     */
    public com.broadon.wsapi.ias.AttributeType[] getAccountAttributes() {
        return accountAttributes;
    }


    /**
     * Sets the accountAttributes value for this UpdateAccountAttributesRequestType.
     * 
     * @param accountAttributes
     */
    public void setAccountAttributes(com.broadon.wsapi.ias.AttributeType[] accountAttributes) {
        this.accountAttributes = accountAttributes;
    }

    public com.broadon.wsapi.ias.AttributeType getAccountAttributes(int i) {
        return this.accountAttributes[i];
    }

    public void setAccountAttributes(int i, com.broadon.wsapi.ias.AttributeType _value) {
        this.accountAttributes[i] = _value;
    }


    /**
     * Gets the unsetAccountAttributes value for this UpdateAccountAttributesRequestType.
     * 
     * @return unsetAccountAttributes
     */
    public int[] getUnsetAccountAttributes() {
        return unsetAccountAttributes;
    }


    /**
     * Sets the unsetAccountAttributes value for this UpdateAccountAttributesRequestType.
     * 
     * @param unsetAccountAttributes
     */
    public void setUnsetAccountAttributes(int[] unsetAccountAttributes) {
        this.unsetAccountAttributes = unsetAccountAttributes;
    }

    public int getUnsetAccountAttributes(int i) {
        return this.unsetAccountAttributes[i];
    }

    public void setUnsetAccountAttributes(int i, int _value) {
        this.unsetAccountAttributes[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpdateAccountAttributesRequestType)) return false;
        UpdateAccountAttributesRequestType other = (UpdateAccountAttributesRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.accountAttributes==null && other.getAccountAttributes()==null) || 
             (this.accountAttributes!=null &&
              java.util.Arrays.equals(this.accountAttributes, other.getAccountAttributes()))) &&
            ((this.unsetAccountAttributes==null && other.getUnsetAccountAttributes()==null) || 
             (this.unsetAccountAttributes!=null &&
              java.util.Arrays.equals(this.unsetAccountAttributes, other.getUnsetAccountAttributes())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAccountAttributes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAccountAttributes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAccountAttributes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getUnsetAccountAttributes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getUnsetAccountAttributes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getUnsetAccountAttributes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UpdateAccountAttributesRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateAccountAttributesRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountAttributes");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AccountAttributes"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AttributeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("unsetAccountAttributes");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UnsetAccountAttributes"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AttributeIdType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

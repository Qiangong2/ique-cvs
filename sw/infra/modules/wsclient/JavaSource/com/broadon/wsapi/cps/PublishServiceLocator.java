/**
 * PublishServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.cps;

public class PublishServiceLocator extends org.apache.axis.client.Service implements com.broadon.wsapi.cps.PublishService {

    public PublishServiceLocator() {
    }


    public PublishServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public PublishServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for PublishSOAP
    private java.lang.String PublishSOAP_address = "http://cps:17102/cps/services/PublishSOAP";

    public java.lang.String getPublishSOAPAddress() {
        return PublishSOAP_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String PublishSOAPWSDDServiceName = "PublishSOAP";

    public java.lang.String getPublishSOAPWSDDServiceName() {
        return PublishSOAPWSDDServiceName;
    }

    public void setPublishSOAPWSDDServiceName(java.lang.String name) {
        PublishSOAPWSDDServiceName = name;
    }

    public com.broadon.wsapi.cps.PublishPortType getPublishSOAP() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(PublishSOAP_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getPublishSOAP(endpoint);
    }

    public com.broadon.wsapi.cps.PublishPortType getPublishSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.broadon.wsapi.cps.PublishSOAPBindingStub _stub = new com.broadon.wsapi.cps.PublishSOAPBindingStub(portAddress, this);
            _stub.setPortName(getPublishSOAPWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setPublishSOAPEndpointAddress(java.lang.String address) {
        PublishSOAP_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.broadon.wsapi.cps.PublishPortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.broadon.wsapi.cps.PublishSOAPBindingStub _stub = new com.broadon.wsapi.cps.PublishSOAPBindingStub(new java.net.URL(PublishSOAP_address), this);
                _stub.setPortName(getPublishSOAPWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("PublishSOAP".equals(inputPortName)) {
            return getPublishSOAP();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "PublishService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "PublishSOAP"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("PublishSOAP".equals(portName)) {
            setPublishSOAPEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}

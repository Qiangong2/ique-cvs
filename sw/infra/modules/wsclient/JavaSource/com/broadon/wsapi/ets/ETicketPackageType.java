/**
 * ETicketPackageType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ets;

public class ETicketPackageType  implements java.io.Serializable {
    private byte[][] ETicket;
    private byte[][] cert;

    public ETicketPackageType() {
    }

    public ETicketPackageType(
           byte[][] ETicket,
           byte[][] cert) {
           this.ETicket = ETicket;
           this.cert = cert;
    }


    /**
     * Gets the ETicket value for this ETicketPackageType.
     * 
     * @return ETicket
     */
    public byte[][] getETicket() {
        return ETicket;
    }


    /**
     * Sets the ETicket value for this ETicketPackageType.
     * 
     * @param ETicket
     */
    public void setETicket(byte[][] ETicket) {
        this.ETicket = ETicket;
    }

    public byte[] getETicket(int i) {
        return this.ETicket[i];
    }

    public void setETicket(int i, byte[] _value) {
        this.ETicket[i] = _value;
    }


    /**
     * Gets the cert value for this ETicketPackageType.
     * 
     * @return cert
     */
    public byte[][] getCert() {
        return cert;
    }


    /**
     * Sets the cert value for this ETicketPackageType.
     * 
     * @param cert
     */
    public void setCert(byte[][] cert) {
        this.cert = cert;
    }

    public byte[] getCert(int i) {
        return this.cert[i];
    }

    public void setCert(int i, byte[] _value) {
        this.cert[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ETicketPackageType)) return false;
        ETicketPackageType other = (ETicketPackageType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ETicket==null && other.getETicket()==null) || 
             (this.ETicket!=null &&
              java.util.Arrays.equals(this.ETicket, other.getETicket()))) &&
            ((this.cert==null && other.getCert()==null) || 
             (this.cert!=null &&
              java.util.Arrays.equals(this.cert, other.getCert())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getETicket() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getETicket());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getETicket(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCert() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCert());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCert(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ETicketPackageType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ets.wsapi.broadon.com", "ETicketPackageType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ETicket");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ets.wsapi.broadon.com", "ETicket"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cert");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ets.wsapi.broadon.com", "Cert"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

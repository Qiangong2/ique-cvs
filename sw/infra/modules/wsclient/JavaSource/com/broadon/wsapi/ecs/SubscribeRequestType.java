/**
 * SubscribeRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ecs;

public class SubscribeRequestType  extends com.broadon.wsapi.ecs.TicketedPurchaseRequestType  implements java.io.Serializable {
    private java.lang.String channelId;
    private com.broadon.wsapi.ecs.TimeDurationType subscriptionLength;

    public SubscribeRequestType() {
    }

    public SubscribeRequestType(
           java.lang.String channelId,
           com.broadon.wsapi.ecs.TimeDurationType subscriptionLength) {
           this.channelId = channelId;
           this.subscriptionLength = subscriptionLength;
    }


    /**
     * Gets the channelId value for this SubscribeRequestType.
     * 
     * @return channelId
     */
    public java.lang.String getChannelId() {
        return channelId;
    }


    /**
     * Sets the channelId value for this SubscribeRequestType.
     * 
     * @param channelId
     */
    public void setChannelId(java.lang.String channelId) {
        this.channelId = channelId;
    }


    /**
     * Gets the subscriptionLength value for this SubscribeRequestType.
     * 
     * @return subscriptionLength
     */
    public com.broadon.wsapi.ecs.TimeDurationType getSubscriptionLength() {
        return subscriptionLength;
    }


    /**
     * Sets the subscriptionLength value for this SubscribeRequestType.
     * 
     * @param subscriptionLength
     */
    public void setSubscriptionLength(com.broadon.wsapi.ecs.TimeDurationType subscriptionLength) {
        this.subscriptionLength = subscriptionLength;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SubscribeRequestType)) return false;
        SubscribeRequestType other = (SubscribeRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.channelId==null && other.getChannelId()==null) || 
             (this.channelId!=null &&
              this.channelId.equals(other.getChannelId()))) &&
            ((this.subscriptionLength==null && other.getSubscriptionLength()==null) || 
             (this.subscriptionLength!=null &&
              this.subscriptionLength.equals(other.getSubscriptionLength())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getChannelId() != null) {
            _hashCode += getChannelId().hashCode();
        }
        if (getSubscriptionLength() != null) {
            _hashCode += getSubscriptionLength().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SubscribeRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "SubscribeRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("channelId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ChannelId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ChannelIdType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subscriptionLength");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "SubscriptionLength"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TimeDurationType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

/**
 * DeviceSubscriptionType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ias;

public class DeviceSubscriptionType  implements java.io.Serializable {
    private java.lang.String subscriptionType;
    private long startDate;
    private long expirationDate;
    private java.lang.Long lastUpdated;
    private java.lang.String referenceId;

    public DeviceSubscriptionType() {
    }

    public DeviceSubscriptionType(
           java.lang.String subscriptionType,
           long startDate,
           long expirationDate,
           java.lang.Long lastUpdated,
           java.lang.String referenceId) {
           this.subscriptionType = subscriptionType;
           this.startDate = startDate;
           this.expirationDate = expirationDate;
           this.lastUpdated = lastUpdated;
           this.referenceId = referenceId;
    }


    /**
     * Gets the subscriptionType value for this DeviceSubscriptionType.
     * 
     * @return subscriptionType
     */
    public java.lang.String getSubscriptionType() {
        return subscriptionType;
    }


    /**
     * Sets the subscriptionType value for this DeviceSubscriptionType.
     * 
     * @param subscriptionType
     */
    public void setSubscriptionType(java.lang.String subscriptionType) {
        this.subscriptionType = subscriptionType;
    }


    /**
     * Gets the startDate value for this DeviceSubscriptionType.
     * 
     * @return startDate
     */
    public long getStartDate() {
        return startDate;
    }


    /**
     * Sets the startDate value for this DeviceSubscriptionType.
     * 
     * @param startDate
     */
    public void setStartDate(long startDate) {
        this.startDate = startDate;
    }


    /**
     * Gets the expirationDate value for this DeviceSubscriptionType.
     * 
     * @return expirationDate
     */
    public long getExpirationDate() {
        return expirationDate;
    }


    /**
     * Sets the expirationDate value for this DeviceSubscriptionType.
     * 
     * @param expirationDate
     */
    public void setExpirationDate(long expirationDate) {
        this.expirationDate = expirationDate;
    }


    /**
     * Gets the lastUpdated value for this DeviceSubscriptionType.
     * 
     * @return lastUpdated
     */
    public java.lang.Long getLastUpdated() {
        return lastUpdated;
    }


    /**
     * Sets the lastUpdated value for this DeviceSubscriptionType.
     * 
     * @param lastUpdated
     */
    public void setLastUpdated(java.lang.Long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }


    /**
     * Gets the referenceId value for this DeviceSubscriptionType.
     * 
     * @return referenceId
     */
    public java.lang.String getReferenceId() {
        return referenceId;
    }


    /**
     * Sets the referenceId value for this DeviceSubscriptionType.
     * 
     * @param referenceId
     */
    public void setReferenceId(java.lang.String referenceId) {
        this.referenceId = referenceId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DeviceSubscriptionType)) return false;
        DeviceSubscriptionType other = (DeviceSubscriptionType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.subscriptionType==null && other.getSubscriptionType()==null) || 
             (this.subscriptionType!=null &&
              this.subscriptionType.equals(other.getSubscriptionType()))) &&
            this.startDate == other.getStartDate() &&
            this.expirationDate == other.getExpirationDate() &&
            ((this.lastUpdated==null && other.getLastUpdated()==null) || 
             (this.lastUpdated!=null &&
              this.lastUpdated.equals(other.getLastUpdated()))) &&
            ((this.referenceId==null && other.getReferenceId()==null) || 
             (this.referenceId!=null &&
              this.referenceId.equals(other.getReferenceId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSubscriptionType() != null) {
            _hashCode += getSubscriptionType().hashCode();
        }
        _hashCode += new Long(getStartDate()).hashCode();
        _hashCode += new Long(getExpirationDate()).hashCode();
        if (getLastUpdated() != null) {
            _hashCode += getLastUpdated().hashCode();
        }
        if (getReferenceId() != null) {
            _hashCode += getReferenceId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DeviceSubscriptionType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "DeviceSubscriptionType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subscriptionType");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "SubscriptionType"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "SubscriptionType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("startDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "StartDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "TimeStampType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expirationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ExpirationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "TimeStampType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastUpdated");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "LastUpdated"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "TimeStampType"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("referenceId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ReferenceId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

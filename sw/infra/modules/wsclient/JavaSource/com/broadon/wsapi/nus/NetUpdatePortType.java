/**
 * NetUpdatePortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.nus;

public interface NetUpdatePortType extends java.rmi.Remote {
    public com.broadon.wsapi.nus.GetSystemUpdateResponseType getSystemUpdate(com.broadon.wsapi.nus.GetSystemUpdateRequestType getSystemUpdateRequest) throws java.rmi.RemoteException;
}

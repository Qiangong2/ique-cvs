/**
 * SubmitGameScoresResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ogs;

public class SubmitGameScoresResponseType  extends com.broadon.wsapi.ogs.AbstractResponseType  implements java.io.Serializable {
    private com.broadon.wsapi.ogs.ScoreItemType[] scoreItem;

    public SubmitGameScoresResponseType() {
    }

    public SubmitGameScoresResponseType(
           com.broadon.wsapi.ogs.ScoreItemType[] scoreItem) {
           this.scoreItem = scoreItem;
    }


    /**
     * Gets the scoreItem value for this SubmitGameScoresResponseType.
     * 
     * @return scoreItem
     */
    public com.broadon.wsapi.ogs.ScoreItemType[] getScoreItem() {
        return scoreItem;
    }


    /**
     * Sets the scoreItem value for this SubmitGameScoresResponseType.
     * 
     * @param scoreItem
     */
    public void setScoreItem(com.broadon.wsapi.ogs.ScoreItemType[] scoreItem) {
        this.scoreItem = scoreItem;
    }

    public com.broadon.wsapi.ogs.ScoreItemType getScoreItem(int i) {
        return this.scoreItem[i];
    }

    public void setScoreItem(int i, com.broadon.wsapi.ogs.ScoreItemType _value) {
        this.scoreItem[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SubmitGameScoresResponseType)) return false;
        SubmitGameScoresResponseType other = (SubmitGameScoresResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.scoreItem==null && other.getScoreItem()==null) || 
             (this.scoreItem!=null &&
              java.util.Arrays.equals(this.scoreItem, other.getScoreItem())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getScoreItem() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getScoreItem());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getScoreItem(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SubmitGameScoresResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SubmitGameScoresResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scoreItem");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ScoreItem"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ScoreItemType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

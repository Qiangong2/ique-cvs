/**
 * PublishResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.cps;

public class PublishResponseType  implements java.io.Serializable {
    private int errorCode;
    private java.lang.String errorMessage;
    /** Optional comment string for debugging and documentation purpose. */
    private java.lang.String comment;
    private byte[] titleMetaData;

    public PublishResponseType() {
    }

    public PublishResponseType(
           int errorCode,
           java.lang.String errorMessage,
           java.lang.String comment,
           byte[] titleMetaData) {
           this.errorCode = errorCode;
           this.errorMessage = errorMessage;
           this.comment = comment;
           this.titleMetaData = titleMetaData;
    }


    /**
     * Gets the errorCode value for this PublishResponseType.
     * 
     * @return errorCode
     */
    public int getErrorCode() {
        return errorCode;
    }


    /**
     * Sets the errorCode value for this PublishResponseType.
     * 
     * @param errorCode
     */
    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }


    /**
     * Gets the errorMessage value for this PublishResponseType.
     * 
     * @return errorMessage
     */
    public java.lang.String getErrorMessage() {
        return errorMessage;
    }


    /**
     * Sets the errorMessage value for this PublishResponseType.
     * 
     * @param errorMessage
     */
    public void setErrorMessage(java.lang.String errorMessage) {
        this.errorMessage = errorMessage;
    }


    /**
     * Gets the comment value for this PublishResponseType.
     * 
     * @return comment Optional comment string for debugging and documentation purpose.
     */
    public java.lang.String getComment() {
        return comment;
    }


    /**
     * Sets the comment value for this PublishResponseType.
     * 
     * @param comment Optional comment string for debugging and documentation purpose.
     */
    public void setComment(java.lang.String comment) {
        this.comment = comment;
    }

    /**
     * Gets the titleMetaData value for this PublishResponseType.
     * 
     * @return titleMetaData
     */
    public byte[] getTitleMetaData() {
        return titleMetaData;
    }


    /**
     * Sets the titleMetaData value for this PublishResponseType.
     * 
     * @param titleMetaData
     */
    public void setTitleMetaData(byte[] titleMetaData) {
        this.titleMetaData = titleMetaData;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PublishResponseType)) return false;
        PublishResponseType other = (PublishResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.errorCode == other.getErrorCode() &&
            ((this.errorMessage==null && other.getErrorMessage()==null) || 
             (this.errorMessage!=null &&
              this.errorMessage.equals(other.getErrorMessage()))) &&
            ((this.comment==null && other.getComment()==null) || 
             (this.comment!=null &&
              this.comment.equals(other.getComment()))) &&
            ((this.titleMetaData==null && other.getTitleMetaData()==null) || 
             (this.titleMetaData!=null &&
              java.util.Arrays.equals(this.titleMetaData, other.getTitleMetaData())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getErrorCode();
        if (getErrorMessage() != null) {
            _hashCode += getErrorMessage().hashCode();
        }
        if (getComment() != null) {
            _hashCode += getComment().hashCode();
        }
        if (getTitleMetaData() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTitleMetaData());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTitleMetaData(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PublishResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "PublishResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorCode");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "ErrorCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorMessage");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "ErrorMessage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comment");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "Comment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titleMetaData");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "TitleMetaData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

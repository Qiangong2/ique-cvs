package com.broadon.wsapi.nus;

public class NetUpdatePortTypeProxy implements com.broadon.wsapi.nus.NetUpdatePortType {
  private String _endpoint = null;
  private com.broadon.wsapi.nus.NetUpdatePortType netUpdatePortType = null;
  
  public NetUpdatePortTypeProxy() {
    _initNetUpdatePortTypeProxy();
  }
  
  private void _initNetUpdatePortTypeProxy() {
    try {
      netUpdatePortType = (new com.broadon.wsapi.nus.NetUpdateServiceLocator()).getNetUpdateSOAP();
      if (netUpdatePortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)netUpdatePortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)netUpdatePortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (netUpdatePortType != null)
      ((javax.xml.rpc.Stub)netUpdatePortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.broadon.wsapi.nus.NetUpdatePortType getNetUpdatePortType() {
    if (netUpdatePortType == null)
      _initNetUpdatePortTypeProxy();
    return netUpdatePortType;
  }
  
  public com.broadon.wsapi.nus.GetSystemUpdateResponseType getSystemUpdate(com.broadon.wsapi.nus.GetSystemUpdateRequestType getSystemUpdateRequest) throws java.rmi.RemoteException{
    if (netUpdatePortType == null)
      _initNetUpdatePortTypeProxy();
    return netUpdatePortType.getSystemUpdate(getSystemUpdateRequest);
  }
  
  
}
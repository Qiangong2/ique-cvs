/**
 * ActionRecordType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.pas;

public class ActionRecordType  implements java.io.Serializable {
    private java.lang.String actionID;
    private long actionDate;
    private com.broadon.wsapi.pas.ActionKindType actionType;
    private java.lang.String actionAmount;

    public ActionRecordType() {
    }

    public ActionRecordType(
           java.lang.String actionID,
           long actionDate,
           com.broadon.wsapi.pas.ActionKindType actionType,
           java.lang.String actionAmount) {
           this.actionID = actionID;
           this.actionDate = actionDate;
           this.actionType = actionType;
           this.actionAmount = actionAmount;
    }


    /**
     * Gets the actionID value for this ActionRecordType.
     * 
     * @return actionID
     */
    public java.lang.String getActionID() {
        return actionID;
    }


    /**
     * Sets the actionID value for this ActionRecordType.
     * 
     * @param actionID
     */
    public void setActionID(java.lang.String actionID) {
        this.actionID = actionID;
    }


    /**
     * Gets the actionDate value for this ActionRecordType.
     * 
     * @return actionDate
     */
    public long getActionDate() {
        return actionDate;
    }


    /**
     * Sets the actionDate value for this ActionRecordType.
     * 
     * @param actionDate
     */
    public void setActionDate(long actionDate) {
        this.actionDate = actionDate;
    }


    /**
     * Gets the actionType value for this ActionRecordType.
     * 
     * @return actionType
     */
    public com.broadon.wsapi.pas.ActionKindType getActionType() {
        return actionType;
    }


    /**
     * Sets the actionType value for this ActionRecordType.
     * 
     * @param actionType
     */
    public void setActionType(com.broadon.wsapi.pas.ActionKindType actionType) {
        this.actionType = actionType;
    }


    /**
     * Gets the actionAmount value for this ActionRecordType.
     * 
     * @return actionAmount
     */
    public java.lang.String getActionAmount() {
        return actionAmount;
    }


    /**
     * Sets the actionAmount value for this ActionRecordType.
     * 
     * @param actionAmount
     */
    public void setActionAmount(java.lang.String actionAmount) {
        this.actionAmount = actionAmount;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ActionRecordType)) return false;
        ActionRecordType other = (ActionRecordType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.actionID==null && other.getActionID()==null) || 
             (this.actionID!=null &&
              this.actionID.equals(other.getActionID()))) &&
            this.actionDate == other.getActionDate() &&
            ((this.actionType==null && other.getActionType()==null) || 
             (this.actionType!=null &&
              this.actionType.equals(other.getActionType()))) &&
            ((this.actionAmount==null && other.getActionAmount()==null) || 
             (this.actionAmount!=null &&
              this.actionAmount.equals(other.getActionAmount())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getActionID() != null) {
            _hashCode += getActionID().hashCode();
        }
        _hashCode += new Long(getActionDate()).hashCode();
        if (getActionType() != null) {
            _hashCode += getActionType().hashCode();
        }
        if (getActionAmount() != null) {
            _hashCode += getActionAmount().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ActionRecordType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ActionRecordType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("actionID");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ActionID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("actionDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ActionDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "TimeStampType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("actionType");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ActionType"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ActionKindType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("actionAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ActionAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

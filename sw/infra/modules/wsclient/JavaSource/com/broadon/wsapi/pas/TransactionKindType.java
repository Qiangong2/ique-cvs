/**
 * TransactionKindType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.pas;

public class TransactionKindType implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected TransactionKindType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _Authorize = "Authorize";
    public static final java.lang.String _Purchase = "Purchase";
    public static final java.lang.String _Deposit = "Deposit";
    public static final java.lang.String _Transfer = "Transfer";
    public static final java.lang.String _Deposit_to = "Deposit_to";
    public static final java.lang.String _Trans_out = "Trans_out";
    public static final TransactionKindType Authorize = new TransactionKindType(_Authorize);
    public static final TransactionKindType Purchase = new TransactionKindType(_Purchase);
    public static final TransactionKindType Deposit = new TransactionKindType(_Deposit);
    public static final TransactionKindType Transfer = new TransactionKindType(_Transfer);
    public static final TransactionKindType Deposit_to = new TransactionKindType(_Deposit_to);
    public static final TransactionKindType Trans_out = new TransactionKindType(_Trans_out);
    public java.lang.String getValue() { return _value_;}
    public static TransactionKindType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        TransactionKindType enumeration = (TransactionKindType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static TransactionKindType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TransactionKindType.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "TransactionKindType"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}

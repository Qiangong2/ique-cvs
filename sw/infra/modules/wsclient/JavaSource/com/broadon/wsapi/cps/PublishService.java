/**
 * PublishService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.cps;

public interface PublishService extends javax.xml.rpc.Service {
    public java.lang.String getPublishSOAPAddress();

    public com.broadon.wsapi.cps.PublishPortType getPublishSOAP() throws javax.xml.rpc.ServiceException;

    public com.broadon.wsapi.cps.PublishPortType getPublishSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}

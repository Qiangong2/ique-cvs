/**
 * AuthorizeECardPaymentResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.pas;

public class AuthorizeECardPaymentResponseType  extends com.broadon.wsapi.pas.AbstractResponseType  implements java.io.Serializable {
    /** optional field, has a value iff no error (ErrorCode=0). */
    private com.broadon.wsapi.pas.PasTokenType authorizationToken;
    /** optional field, has a value iff no error (ErrorCode=0). */
    private com.broadon.wsapi.pas.AuthorizationStateType authorizationState;

    public AuthorizeECardPaymentResponseType() {
    }

    public AuthorizeECardPaymentResponseType(
           com.broadon.wsapi.pas.PasTokenType authorizationToken,
           com.broadon.wsapi.pas.AuthorizationStateType authorizationState) {
           this.authorizationToken = authorizationToken;
           this.authorizationState = authorizationState;
    }


    /**
     * Gets the authorizationToken value for this AuthorizeECardPaymentResponseType.
     * 
     * @return authorizationToken optional field, has a value iff no error (ErrorCode=0).
     */
    public com.broadon.wsapi.pas.PasTokenType getAuthorizationToken() {
        return authorizationToken;
    }


    /**
     * Sets the authorizationToken value for this AuthorizeECardPaymentResponseType.
     * 
     * @param authorizationToken optional field, has a value iff no error (ErrorCode=0).
     */
    public void setAuthorizationToken(com.broadon.wsapi.pas.PasTokenType authorizationToken) {
        this.authorizationToken = authorizationToken;
    }


    /**
     * Gets the authorizationState value for this AuthorizeECardPaymentResponseType.
     * 
     * @return authorizationState optional field, has a value iff no error (ErrorCode=0).
     */
    public com.broadon.wsapi.pas.AuthorizationStateType getAuthorizationState() {
        return authorizationState;
    }


    /**
     * Sets the authorizationState value for this AuthorizeECardPaymentResponseType.
     * 
     * @param authorizationState optional field, has a value iff no error (ErrorCode=0).
     */
    public void setAuthorizationState(com.broadon.wsapi.pas.AuthorizationStateType authorizationState) {
        this.authorizationState = authorizationState;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AuthorizeECardPaymentResponseType)) return false;
        AuthorizeECardPaymentResponseType other = (AuthorizeECardPaymentResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.authorizationToken==null && other.getAuthorizationToken()==null) || 
             (this.authorizationToken!=null &&
              this.authorizationToken.equals(other.getAuthorizationToken()))) &&
            ((this.authorizationState==null && other.getAuthorizationState()==null) || 
             (this.authorizationState!=null &&
              this.authorizationState.equals(other.getAuthorizationState())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAuthorizationToken() != null) {
            _hashCode += getAuthorizationToken().hashCode();
        }
        if (getAuthorizationState() != null) {
            _hashCode += getAuthorizationState().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AuthorizeECardPaymentResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizeECardPaymentResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authorizationToken");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizationToken"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "PasTokenType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authorizationState");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizationState"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizationStateType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

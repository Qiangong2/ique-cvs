package com.broadon.wsapi.ets;

public class ETicketPortTypeProxy implements com.broadon.wsapi.ets.ETicketPortType {
  private String _endpoint = null;
  private com.broadon.wsapi.ets.ETicketPortType eTicketPortType = null;
  
  public ETicketPortTypeProxy() {
    _initETicketPortTypeProxy();
  }
  
  private void _initETicketPortTypeProxy() {
    try {
      eTicketPortType = (new com.broadon.wsapi.ets.ETicketServiceLocator()).getETicketSOAP();
      if (eTicketPortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)eTicketPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)eTicketPortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (eTicketPortType != null)
      ((javax.xml.rpc.Stub)eTicketPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.broadon.wsapi.ets.ETicketPortType getETicketPortType() {
    if (eTicketPortType == null)
      _initETicketPortTypeProxy();
    return eTicketPortType;
  }
  
  public com.broadon.wsapi.ets.GetTicketsResponseType getTickets(com.broadon.wsapi.ets.GetTicketsRequestType getTicketsRequest) throws java.rmi.RemoteException{
    if (eTicketPortType == null)
      _initETicketPortTypeProxy();
    return eTicketPortType.getTickets(getTicketsRequest);
  }
  
  public java.lang.String getHealth() throws java.rmi.RemoteException{
    if (eTicketPortType == null)
      _initETicketPortTypeProxy();
    return eTicketPortType.getHealth();
  }
  
  
}

/**
 * GetMetaResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ecs;

public class GetMetaResponseType  extends com.broadon.wsapi.ecs.AbstractResponseType  implements java.io.Serializable {
    /** URL used as a prefix for retrieving contents. */
    private org.apache.axis.types.URI contentPrefixURL;
    /** List of special titles and their versions for the device.
 * 										It is up to the eCommerce client to map the titleId to a
 * 										specific title. */
    private com.broadon.wsapi.ecs.TitleVersionType[] titleVersion;
    /** List of contents to be preloaded. */
    private com.broadon.wsapi.ecs.ContentInfoType[] preloadContents;
    /** Optional. Timestamp of latest title update. */
    private java.lang.Long titleUpdateTime;
    /** List of titles and their versions that are owned by
 * 										the device and has been updated. */
    private com.broadon.wsapi.ecs.TitleVersionType[] updatedTitles;

    public GetMetaResponseType() {
    }

    public GetMetaResponseType(
           org.apache.axis.types.URI contentPrefixURL,
           com.broadon.wsapi.ecs.TitleVersionType[] titleVersion,
           com.broadon.wsapi.ecs.ContentInfoType[] preloadContents,
           java.lang.Long titleUpdateTime,
           com.broadon.wsapi.ecs.TitleVersionType[] updatedTitles) {
           this.contentPrefixURL = contentPrefixURL;
           this.titleVersion = titleVersion;
           this.preloadContents = preloadContents;
           this.titleUpdateTime = titleUpdateTime;
           this.updatedTitles = updatedTitles;
    }


    /**
     * Gets the contentPrefixURL value for this GetMetaResponseType.
     * 
     * @return contentPrefixURL URL used as a prefix for retrieving contents.
     */
    public org.apache.axis.types.URI getContentPrefixURL() {
        return contentPrefixURL;
    }


    /**
     * Sets the contentPrefixURL value for this GetMetaResponseType.
     * 
     * @param contentPrefixURL URL used as a prefix for retrieving contents.
     */
    public void setContentPrefixURL(org.apache.axis.types.URI contentPrefixURL) {
        this.contentPrefixURL = contentPrefixURL;
    }


    /**
     * Gets the titleVersion value for this GetMetaResponseType.
     * 
     * @return titleVersion List of special titles and their versions for the device.
 * 										It is up to the eCommerce client to map the titleId to a
 * 										specific title.
     */
    public com.broadon.wsapi.ecs.TitleVersionType[] getTitleVersion() {
        return titleVersion;
    }


    /**
     * Sets the titleVersion value for this GetMetaResponseType.
     * 
     * @param titleVersion List of special titles and their versions for the device.
 * 										It is up to the eCommerce client to map the titleId to a
 * 										specific title.
     */
    public void setTitleVersion(com.broadon.wsapi.ecs.TitleVersionType[] titleVersion) {
        this.titleVersion = titleVersion;
    }

    public com.broadon.wsapi.ecs.TitleVersionType getTitleVersion(int i) {
        return this.titleVersion[i];
    }

    public void setTitleVersion(int i, com.broadon.wsapi.ecs.TitleVersionType _value) {
        this.titleVersion[i] = _value;
    }


    /**
     * Gets the preloadContents value for this GetMetaResponseType.
     * 
     * @return preloadContents List of contents to be preloaded.
     */
    public com.broadon.wsapi.ecs.ContentInfoType[] getPreloadContents() {
        return preloadContents;
    }


    /**
     * Sets the preloadContents value for this GetMetaResponseType.
     * 
     * @param preloadContents List of contents to be preloaded.
     */
    public void setPreloadContents(com.broadon.wsapi.ecs.ContentInfoType[] preloadContents) {
        this.preloadContents = preloadContents;
    }

    public com.broadon.wsapi.ecs.ContentInfoType getPreloadContents(int i) {
        return this.preloadContents[i];
    }

    public void setPreloadContents(int i, com.broadon.wsapi.ecs.ContentInfoType _value) {
        this.preloadContents[i] = _value;
    }


    /**
     * Gets the titleUpdateTime value for this GetMetaResponseType.
     * 
     * @return titleUpdateTime Optional. Timestamp of latest title update.
     */
    public java.lang.Long getTitleUpdateTime() {
        return titleUpdateTime;
    }


    /**
     * Sets the titleUpdateTime value for this GetMetaResponseType.
     * 
     * @param titleUpdateTime Optional. Timestamp of latest title update.
     */
    public void setTitleUpdateTime(java.lang.Long titleUpdateTime) {
        this.titleUpdateTime = titleUpdateTime;
    }


    /**
     * Gets the updatedTitles value for this GetMetaResponseType.
     * 
     * @return updatedTitles List of titles and their versions that are owned by
 * 										the device and has been updated.
     */
    public com.broadon.wsapi.ecs.TitleVersionType[] getUpdatedTitles() {
        return updatedTitles;
    }


    /**
     * Sets the updatedTitles value for this GetMetaResponseType.
     * 
     * @param updatedTitles List of titles and their versions that are owned by
 * 										the device and has been updated.
     */
    public void setUpdatedTitles(com.broadon.wsapi.ecs.TitleVersionType[] updatedTitles) {
        this.updatedTitles = updatedTitles;
    }

    public com.broadon.wsapi.ecs.TitleVersionType getUpdatedTitles(int i) {
        return this.updatedTitles[i];
    }

    public void setUpdatedTitles(int i, com.broadon.wsapi.ecs.TitleVersionType _value) {
        this.updatedTitles[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetMetaResponseType)) return false;
        GetMetaResponseType other = (GetMetaResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.contentPrefixURL==null && other.getContentPrefixURL()==null) || 
             (this.contentPrefixURL!=null &&
              this.contentPrefixURL.equals(other.getContentPrefixURL()))) &&
            ((this.titleVersion==null && other.getTitleVersion()==null) || 
             (this.titleVersion!=null &&
              java.util.Arrays.equals(this.titleVersion, other.getTitleVersion()))) &&
            ((this.preloadContents==null && other.getPreloadContents()==null) || 
             (this.preloadContents!=null &&
              java.util.Arrays.equals(this.preloadContents, other.getPreloadContents()))) &&
            ((this.titleUpdateTime==null && other.getTitleUpdateTime()==null) || 
             (this.titleUpdateTime!=null &&
              this.titleUpdateTime.equals(other.getTitleUpdateTime()))) &&
            ((this.updatedTitles==null && other.getUpdatedTitles()==null) || 
             (this.updatedTitles!=null &&
              java.util.Arrays.equals(this.updatedTitles, other.getUpdatedTitles())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getContentPrefixURL() != null) {
            _hashCode += getContentPrefixURL().hashCode();
        }
        if (getTitleVersion() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTitleVersion());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTitleVersion(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPreloadContents() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPreloadContents());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPreloadContents(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTitleUpdateTime() != null) {
            _hashCode += getTitleUpdateTime().hashCode();
        }
        if (getUpdatedTitles() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getUpdatedTitles());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getUpdatedTitles(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetMetaResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetMetaResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contentPrefixURL");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ContentPrefixURL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "anyURI"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titleVersion");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TitleVersion"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TitleVersionType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("preloadContents");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PreloadContents"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ContentInfoType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titleUpdateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TitleUpdateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TimeStampType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updatedTitles");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "UpdatedTitles"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TitleVersionType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

/**
 * PaymentAuthorizationPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.pas;

public interface PaymentAuthorizationPortType extends java.rmi.Remote {
    public com.broadon.wsapi.pas.AuthorizeAccountPaymentResponseType authorizeAccountPayment(com.broadon.wsapi.pas.AuthorizeAccountPaymentRequestType authorizeAccountPaymentRequest) throws java.rmi.RemoteException;
    public com.broadon.wsapi.pas.AuthorizeECardPaymentResponseType authorizeECardPayment(com.broadon.wsapi.pas.AuthorizeECardPaymentRequestType authorizeECardPaymentRequest) throws java.rmi.RemoteException;
    public com.broadon.wsapi.pas.AuthorizeCreditCardPaymentResponseType authorizeCreditCardPayment(com.broadon.wsapi.pas.AuthorizeCreditCardPaymentRequestType authorizeCreditCardPaymentRequest) throws java.rmi.RemoteException;
    public com.broadon.wsapi.pas.CapturePaymentResponseType capturePayment(com.broadon.wsapi.pas.CapturePaymentRequestType capturePaymentRequest) throws java.rmi.RemoteException;
    public com.broadon.wsapi.pas.RefundPaymentResponseType refundPayment(com.broadon.wsapi.pas.RefundPaymentRequestType refundPaymentRequest) throws java.rmi.RemoteException;
    public com.broadon.wsapi.pas.VoidAuthorizationResponseType voidAuthorization(com.broadon.wsapi.pas.VoidAuthorizationRequestType voidAuthorizationRequest) throws java.rmi.RemoteException;
    public com.broadon.wsapi.pas.GetAuthorizationHistoryResponseType getAuthorizationHistory(com.broadon.wsapi.pas.GetAuthorizationHistoryRequestType getAuthorizationHistoryRequest) throws java.rmi.RemoteException;
    public com.broadon.wsapi.pas.GetAccountHistoryResponseType getBalanceAccountHistory(com.broadon.wsapi.pas.GetAccountHistoryRequestType getAccountHistoryRequest) throws java.rmi.RemoteException;
    public com.broadon.wsapi.pas.GetECardHistoryResponseType getECardAccountHistory(com.broadon.wsapi.pas.GetECardHistoryRequestType getECardHistoryRequest) throws java.rmi.RemoteException;
    public com.broadon.wsapi.pas.GetCreditCardHistoryResponseType getCreditCardAccountHistory(com.broadon.wsapi.pas.GetCreditCardHistoryRequestType getCreditCardHistoryRequest) throws java.rmi.RemoteException;
    public com.broadon.wsapi.pas.CaptureAccountToAccountResponseType captureAccountToAccount(com.broadon.wsapi.pas.CaptureAccountToAccountRequestType captureAccountToAccountRequest) throws java.rmi.RemoteException;
    public com.broadon.wsapi.pas.CaptureCreditCardToAccountResponseType captureCreditCardToAccount(com.broadon.wsapi.pas.CaptureCreditCardToAccountRequestType captureCreditCardToAccountRequest) throws java.rmi.RemoteException;
    public com.broadon.wsapi.pas.CaptureECardToAccountResponseType captureECardToAccount(com.broadon.wsapi.pas.CaptureECardToAccountRequestType captureECardToAccountRequest) throws java.rmi.RemoteException;
    public com.broadon.wsapi.pas.CheckAccountBalanceResponseType checkAccountBalance(com.broadon.wsapi.pas.CheckAccountBalanceRequestType checkAccountBalanceRequest) throws java.rmi.RemoteException;
    public com.broadon.wsapi.pas.CheckECardBalanceResponseType checkECardBalance(com.broadon.wsapi.pas.CheckECardBalanceRequestType checkECardBalanceRequest) throws java.rmi.RemoteException;
    public com.broadon.wsapi.pas.CreateAccountResponseType createAccount(com.broadon.wsapi.pas.CreateAccountRequestType createAccountRequest) throws java.rmi.RemoteException;
    public com.broadon.wsapi.pas.DisableAccountResponseType disableAccount(com.broadon.wsapi.pas.DisableAccountRequestType disableAccountRequest) throws java.rmi.RemoteException;
    public com.broadon.wsapi.pas.ResetAccountPINResponseType resetAccountPIN(com.broadon.wsapi.pas.ResetAccountPINRequestType resetAccountPINRequest) throws java.rmi.RemoteException;
    public com.broadon.wsapi.pas.GetECardTypeResponseType getECardType(com.broadon.wsapi.pas.GetECardTypeRequestType getECardTypeRequest) throws java.rmi.RemoteException;
}

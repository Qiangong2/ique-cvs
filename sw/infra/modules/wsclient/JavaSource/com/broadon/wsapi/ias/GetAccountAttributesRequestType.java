/**
 * GetAccountAttributesRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ias;

public class GetAccountAttributesRequestType  extends com.broadon.wsapi.ias.AbstractAccountRequestType  implements java.io.Serializable {
    private int[] attributeId;

    public GetAccountAttributesRequestType() {
    }

    public GetAccountAttributesRequestType(
           int[] attributeId) {
           this.attributeId = attributeId;
    }


    /**
     * Gets the attributeId value for this GetAccountAttributesRequestType.
     * 
     * @return attributeId
     */
    public int[] getAttributeId() {
        return attributeId;
    }


    /**
     * Sets the attributeId value for this GetAccountAttributesRequestType.
     * 
     * @param attributeId
     */
    public void setAttributeId(int[] attributeId) {
        this.attributeId = attributeId;
    }

    public int getAttributeId(int i) {
        return this.attributeId[i];
    }

    public void setAttributeId(int i, int _value) {
        this.attributeId[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetAccountAttributesRequestType)) return false;
        GetAccountAttributesRequestType other = (GetAccountAttributesRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.attributeId==null && other.getAttributeId()==null) || 
             (this.attributeId!=null &&
              java.util.Arrays.equals(this.attributeId, other.getAttributeId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAttributeId() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAttributeId());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAttributeId(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetAccountAttributesRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetAccountAttributesRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attributeId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AttributeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AttributeIdType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

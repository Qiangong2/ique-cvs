/**
 * SyncETicketsRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ecs;

public class SyncETicketsRequestType  extends com.broadon.wsapi.ecs.AbstractRequestType  implements java.io.Serializable {
    /** Optional.  The timestamp of the last syncETickets.  
 * 										ETickets that have beeen updated since that time should
 * be returned. */
    private java.lang.Long lastSyncTime;
    /** Certificate containing the public key of the device */
    private byte[] deviceCert;

    public SyncETicketsRequestType() {
    }

    public SyncETicketsRequestType(
           java.lang.Long lastSyncTime,
           byte[] deviceCert) {
           this.lastSyncTime = lastSyncTime;
           this.deviceCert = deviceCert;
    }


    /**
     * Gets the lastSyncTime value for this SyncETicketsRequestType.
     * 
     * @return lastSyncTime Optional.  The timestamp of the last syncETickets.  
 * 										ETickets that have beeen updated since that time should
 * be returned.
     */
    public java.lang.Long getLastSyncTime() {
        return lastSyncTime;
    }


    /**
     * Sets the lastSyncTime value for this SyncETicketsRequestType.
     * 
     * @param lastSyncTime Optional.  The timestamp of the last syncETickets.  
 * 										ETickets that have beeen updated since that time should
 * be returned.
     */
    public void setLastSyncTime(java.lang.Long lastSyncTime) {
        this.lastSyncTime = lastSyncTime;
    }


    /**
     * Gets the deviceCert value for this SyncETicketsRequestType.
     * 
     * @return deviceCert Certificate containing the public key of the device
     */
    public byte[] getDeviceCert() {
        return deviceCert;
    }


    /**
     * Sets the deviceCert value for this SyncETicketsRequestType.
     * 
     * @param deviceCert Certificate containing the public key of the device
     */
    public void setDeviceCert(byte[] deviceCert) {
        this.deviceCert = deviceCert;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SyncETicketsRequestType)) return false;
        SyncETicketsRequestType other = (SyncETicketsRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.lastSyncTime==null && other.getLastSyncTime()==null) || 
             (this.lastSyncTime!=null &&
              this.lastSyncTime.equals(other.getLastSyncTime()))) &&
            ((this.deviceCert==null && other.getDeviceCert()==null) || 
             (this.deviceCert!=null &&
              java.util.Arrays.equals(this.deviceCert, other.getDeviceCert())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getLastSyncTime() != null) {
            _hashCode += getLastSyncTime().hashCode();
        }
        if (getDeviceCert() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDeviceCert());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDeviceCert(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SyncETicketsRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "SyncETicketsRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastSyncTime");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "LastSyncTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TimeStampType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deviceCert");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "DeviceCert"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

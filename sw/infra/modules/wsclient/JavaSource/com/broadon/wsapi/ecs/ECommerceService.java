/**
 * ECommerceService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ecs;

public interface ECommerceService extends javax.xml.rpc.Service {
    public java.lang.String getECommerceSOAPAddress();

    public com.broadon.wsapi.ecs.ECommercePortType getECommerceSOAP() throws javax.xml.rpc.ServiceException;

    public com.broadon.wsapi.ecs.ECommercePortType getECommerceSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}

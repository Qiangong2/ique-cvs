/**
 * GetTicketsRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ets;

public class GetTicketsRequestType  extends com.broadon.wsapi.ets.AbstractTransactionType  implements java.io.Serializable {
    private byte[] deviceCertificate;
    private com.broadon.wsapi.ets.TicketsRequestType[] ticketsRequest;
    /** Optional comment string for debugging and documentation. */
    private java.lang.String comment;
    /** Identify the client that makes this request.  Usually, this is
 * the certificate ID of the requesting ECS server. */
    private java.lang.String requesterId;

    public GetTicketsRequestType() {
    }

    public GetTicketsRequestType(
           byte[] deviceCertificate,
           com.broadon.wsapi.ets.TicketsRequestType[] ticketsRequest,
           java.lang.String comment,
           java.lang.String requesterId) {
           this.deviceCertificate = deviceCertificate;
           this.ticketsRequest = ticketsRequest;
           this.comment = comment;
           this.requesterId = requesterId;
    }


    /**
     * Gets the deviceCertificate value for this GetTicketsRequestType.
     * 
     * @return deviceCertificate
     */
    public byte[] getDeviceCertificate() {
        return deviceCertificate;
    }


    /**
     * Sets the deviceCertificate value for this GetTicketsRequestType.
     * 
     * @param deviceCertificate
     */
    public void setDeviceCertificate(byte[] deviceCertificate) {
        this.deviceCertificate = deviceCertificate;
    }


    /**
     * Gets the ticketsRequest value for this GetTicketsRequestType.
     * 
     * @return ticketsRequest
     */
    public com.broadon.wsapi.ets.TicketsRequestType[] getTicketsRequest() {
        return ticketsRequest;
    }


    /**
     * Sets the ticketsRequest value for this GetTicketsRequestType.
     * 
     * @param ticketsRequest
     */
    public void setTicketsRequest(com.broadon.wsapi.ets.TicketsRequestType[] ticketsRequest) {
        this.ticketsRequest = ticketsRequest;
    }

    public com.broadon.wsapi.ets.TicketsRequestType getTicketsRequest(int i) {
        return this.ticketsRequest[i];
    }

    public void setTicketsRequest(int i, com.broadon.wsapi.ets.TicketsRequestType _value) {
        this.ticketsRequest[i] = _value;
    }


    /**
     * Gets the comment value for this GetTicketsRequestType.
     * 
     * @return comment Optional comment string for debugging and documentation.
     */
    public java.lang.String getComment() {
        return comment;
    }


    /**
     * Sets the comment value for this GetTicketsRequestType.
     * 
     * @param comment Optional comment string for debugging and documentation.
     */
    public void setComment(java.lang.String comment) {
        this.comment = comment;
    }


    /**
     * Gets the requesterId value for this GetTicketsRequestType.
     * 
     * @return requesterId Identify the client that makes this request.  Usually, this is
 * the certificate ID of the requesting ECS server.
     */
    public java.lang.String getRequesterId() {
        return requesterId;
    }


    /**
     * Sets the requesterId value for this GetTicketsRequestType.
     * 
     * @param requesterId Identify the client that makes this request.  Usually, this is
 * the certificate ID of the requesting ECS server.
     */
    public void setRequesterId(java.lang.String requesterId) {
        this.requesterId = requesterId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetTicketsRequestType)) return false;
        GetTicketsRequestType other = (GetTicketsRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.deviceCertificate==null && other.getDeviceCertificate()==null) || 
             (this.deviceCertificate!=null &&
              java.util.Arrays.equals(this.deviceCertificate, other.getDeviceCertificate()))) &&
            ((this.ticketsRequest==null && other.getTicketsRequest()==null) || 
             (this.ticketsRequest!=null &&
              java.util.Arrays.equals(this.ticketsRequest, other.getTicketsRequest()))) &&
            ((this.comment==null && other.getComment()==null) || 
             (this.comment!=null &&
              this.comment.equals(other.getComment()))) &&
            ((this.requesterId==null && other.getRequesterId()==null) || 
             (this.requesterId!=null &&
              this.requesterId.equals(other.getRequesterId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getDeviceCertificate() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDeviceCertificate());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDeviceCertificate(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTicketsRequest() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTicketsRequest());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTicketsRequest(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getComment() != null) {
            _hashCode += getComment().hashCode();
        }
        if (getRequesterId() != null) {
            _hashCode += getRequesterId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetTicketsRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ets.wsapi.broadon.com", "GetTicketsRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deviceCertificate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ets.wsapi.broadon.com", "DeviceCertificate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ticketsRequest");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ets.wsapi.broadon.com", "TicketsRequest"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ets.wsapi.broadon.com", "TicketsRequestType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comment");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ets.wsapi.broadon.com", "Comment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("requesterId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ets.wsapi.broadon.com", "RequesterId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

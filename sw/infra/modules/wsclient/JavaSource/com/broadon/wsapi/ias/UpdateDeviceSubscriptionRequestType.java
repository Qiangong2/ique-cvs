/**
 * UpdateDeviceSubscriptionRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ias;

public class UpdateDeviceSubscriptionRequestType  extends com.broadon.wsapi.ias.AbstractDeviceRequestType  implements java.io.Serializable {
    private com.broadon.wsapi.ias.DeviceSubscriptionType deviceSubscription;

    public UpdateDeviceSubscriptionRequestType() {
    }

    public UpdateDeviceSubscriptionRequestType(
           com.broadon.wsapi.ias.DeviceSubscriptionType deviceSubscription) {
           this.deviceSubscription = deviceSubscription;
    }


    /**
     * Gets the deviceSubscription value for this UpdateDeviceSubscriptionRequestType.
     * 
     * @return deviceSubscription
     */
    public com.broadon.wsapi.ias.DeviceSubscriptionType getDeviceSubscription() {
        return deviceSubscription;
    }


    /**
     * Sets the deviceSubscription value for this UpdateDeviceSubscriptionRequestType.
     * 
     * @param deviceSubscription
     */
    public void setDeviceSubscription(com.broadon.wsapi.ias.DeviceSubscriptionType deviceSubscription) {
        this.deviceSubscription = deviceSubscription;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpdateDeviceSubscriptionRequestType)) return false;
        UpdateDeviceSubscriptionRequestType other = (UpdateDeviceSubscriptionRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.deviceSubscription==null && other.getDeviceSubscription()==null) || 
             (this.deviceSubscription!=null &&
              this.deviceSubscription.equals(other.getDeviceSubscription())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getDeviceSubscription() != null) {
            _hashCode += getDeviceSubscription().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UpdateDeviceSubscriptionRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateDeviceSubscriptionRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deviceSubscription");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "DeviceSubscription"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "DeviceSubscriptionType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

/**
 * AuthorizationRecordType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.pas;


/**
 * The AuthorizationRecordType is used in reporting the history of
 * a particular payment authorization. 
 * 				Each record is a pair of action taken and a authorization state
 * BEFORE the action is taken.
 */
public class AuthorizationRecordType  implements java.io.Serializable {
    private com.broadon.wsapi.pas.ActionRecordType action;
    private com.broadon.wsapi.pas.AuthorizationStateType state;

    public AuthorizationRecordType() {
    }

    public AuthorizationRecordType(
           com.broadon.wsapi.pas.ActionRecordType action,
           com.broadon.wsapi.pas.AuthorizationStateType state) {
           this.action = action;
           this.state = state;
    }


    /**
     * Gets the action value for this AuthorizationRecordType.
     * 
     * @return action
     */
    public com.broadon.wsapi.pas.ActionRecordType getAction() {
        return action;
    }


    /**
     * Sets the action value for this AuthorizationRecordType.
     * 
     * @param action
     */
    public void setAction(com.broadon.wsapi.pas.ActionRecordType action) {
        this.action = action;
    }


    /**
     * Gets the state value for this AuthorizationRecordType.
     * 
     * @return state
     */
    public com.broadon.wsapi.pas.AuthorizationStateType getState() {
        return state;
    }


    /**
     * Sets the state value for this AuthorizationRecordType.
     * 
     * @param state
     */
    public void setState(com.broadon.wsapi.pas.AuthorizationStateType state) {
        this.state = state;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AuthorizationRecordType)) return false;
        AuthorizationRecordType other = (AuthorizationRecordType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.action==null && other.getAction()==null) || 
             (this.action!=null &&
              this.action.equals(other.getAction()))) &&
            ((this.state==null && other.getState()==null) || 
             (this.state!=null &&
              this.state.equals(other.getState())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAction() != null) {
            _hashCode += getAction().hashCode();
        }
        if (getState() != null) {
            _hashCode += getState().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AuthorizationRecordType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizationRecordType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("action");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "Action"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ActionRecordType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("state");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "State"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizationStateType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

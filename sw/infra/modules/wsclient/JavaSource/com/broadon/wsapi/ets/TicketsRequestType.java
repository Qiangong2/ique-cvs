/**
 * TicketsRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ets;

public class TicketsRequestType  implements java.io.Serializable {
    private java.lang.String titleId;
    private long ticketId;
    /** Content ID of the TMD corresponding to the Title. */
    private java.lang.String contentId;
    /** The bit mask corresponding to the
 * 								content IDs authorized by this tickets.
 * 								Variable length, but only the first 512
 * 								bits would be used. */
    private byte[] contentMask;
    private com.broadon.wsapi.ets.TicketAttributeType[] limits;
    private java.lang.String titleCategory;
    private java.lang.Boolean commonTicket;

    public TicketsRequestType() {
    }

    public TicketsRequestType(
           java.lang.String titleId,
           long ticketId,
           java.lang.String contentId,
           byte[] contentMask,
           com.broadon.wsapi.ets.TicketAttributeType[] limits,
           java.lang.String titleCategory,
           java.lang.Boolean commonTicket) {
           this.titleId = titleId;
           this.ticketId = ticketId;
           this.contentId = contentId;
           this.contentMask = contentMask;
           this.limits = limits;
           this.titleCategory = titleCategory;
           this.commonTicket = commonTicket;
    }


    /**
     * Gets the titleId value for this TicketsRequestType.
     * 
     * @return titleId
     */
    public java.lang.String getTitleId() {
        return titleId;
    }


    /**
     * Sets the titleId value for this TicketsRequestType.
     * 
     * @param titleId
     */
    public void setTitleId(java.lang.String titleId) {
        this.titleId = titleId;
    }


    /**
     * Gets the ticketId value for this TicketsRequestType.
     * 
     * @return ticketId
     */
    public long getTicketId() {
        return ticketId;
    }


    /**
     * Sets the ticketId value for this TicketsRequestType.
     * 
     * @param ticketId
     */
    public void setTicketId(long ticketId) {
        this.ticketId = ticketId;
    }


    /**
     * Gets the contentId value for this TicketsRequestType.
     * 
     * @return contentId Content ID of the TMD corresponding to the Title.
     */
    public java.lang.String getContentId() {
        return contentId;
    }


    /**
     * Sets the contentId value for this TicketsRequestType.
     * 
     * @param contentId Content ID of the TMD corresponding to the Title.
     */
    public void setContentId(java.lang.String contentId) {
        this.contentId = contentId;
    }


    /**
     * Gets the contentMask value for this TicketsRequestType.
     * 
     * @return contentMask The bit mask corresponding to the
 * 								content IDs authorized by this tickets.
 * 								Variable length, but only the first 512
 * 								bits would be used.
     */
    public byte[] getContentMask() {
        return contentMask;
    }


    /**
     * Sets the contentMask value for this TicketsRequestType.
     * 
     * @param contentMask The bit mask corresponding to the
 * 								content IDs authorized by this tickets.
 * 								Variable length, but only the first 512
 * 								bits would be used.
     */
    public void setContentMask(byte[] contentMask) {
        this.contentMask = contentMask;
    }


    /**
     * Gets the limits value for this TicketsRequestType.
     * 
     * @return limits
     */
    public com.broadon.wsapi.ets.TicketAttributeType[] getLimits() {
        return limits;
    }


    /**
     * Sets the limits value for this TicketsRequestType.
     * 
     * @param limits
     */
    public void setLimits(com.broadon.wsapi.ets.TicketAttributeType[] limits) {
        this.limits = limits;
    }

    public com.broadon.wsapi.ets.TicketAttributeType getLimits(int i) {
        return this.limits[i];
    }

    public void setLimits(int i, com.broadon.wsapi.ets.TicketAttributeType _value) {
        this.limits[i] = _value;
    }


    /**
     * Gets the titleCategory value for this TicketsRequestType.
     * 
     * @return titleCategory
     */
    public java.lang.String getTitleCategory() {
        return titleCategory;
    }


    /**
     * Sets the titleCategory value for this TicketsRequestType.
     * 
     * @param titleCategory
     */
    public void setTitleCategory(java.lang.String titleCategory) {
        this.titleCategory = titleCategory;
    }


    /**
     * Gets the commonTicket value for this TicketsRequestType.
     * 
     * @return commonTicket
     */
    public java.lang.Boolean getCommonTicket() {
        return commonTicket;
    }


    /**
     * Sets the commonTicket value for this TicketsRequestType.
     * 
     * @param commonTicket
     */
    public void setCommonTicket(java.lang.Boolean commonTicket) {
        this.commonTicket = commonTicket;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TicketsRequestType)) return false;
        TicketsRequestType other = (TicketsRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.titleId==null && other.getTitleId()==null) || 
             (this.titleId!=null &&
              this.titleId.equals(other.getTitleId()))) &&
            this.ticketId == other.getTicketId() &&
            ((this.contentId==null && other.getContentId()==null) || 
             (this.contentId!=null &&
              this.contentId.equals(other.getContentId()))) &&
            ((this.contentMask==null && other.getContentMask()==null) || 
             (this.contentMask!=null &&
              java.util.Arrays.equals(this.contentMask, other.getContentMask()))) &&
            ((this.limits==null && other.getLimits()==null) || 
             (this.limits!=null &&
              java.util.Arrays.equals(this.limits, other.getLimits()))) &&
            ((this.titleCategory==null && other.getTitleCategory()==null) || 
             (this.titleCategory!=null &&
              this.titleCategory.equals(other.getTitleCategory()))) &&
            ((this.commonTicket==null && other.getCommonTicket()==null) || 
             (this.commonTicket!=null &&
              this.commonTicket.equals(other.getCommonTicket())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTitleId() != null) {
            _hashCode += getTitleId().hashCode();
        }
        _hashCode += new Long(getTicketId()).hashCode();
        if (getContentId() != null) {
            _hashCode += getContentId().hashCode();
        }
        if (getContentMask() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getContentMask());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getContentMask(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getLimits() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getLimits());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getLimits(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTitleCategory() != null) {
            _hashCode += getTitleCategory().hashCode();
        }
        if (getCommonTicket() != null) {
            _hashCode += getCommonTicket().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TicketsRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ets.wsapi.broadon.com", "TicketsRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titleId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ets.wsapi.broadon.com", "TitleId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ets.wsapi.broadon.com", "TitleIdType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ticketId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ets.wsapi.broadon.com", "TicketId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ets.wsapi.broadon.com", "TicketIdType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contentId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ets.wsapi.broadon.com", "ContentId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ets.wsapi.broadon.com", "ContentIdType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contentMask");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ets.wsapi.broadon.com", "ContentMask"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("limits");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ets.wsapi.broadon.com", "Limits"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ets.wsapi.broadon.com", "TicketAttributeType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titleCategory");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ets.wsapi.broadon.com", "TitleCategory"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ets.wsapi.broadon.com", "TitleCategoryType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commonTicket");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ets.wsapi.broadon.com", "CommonTicket"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

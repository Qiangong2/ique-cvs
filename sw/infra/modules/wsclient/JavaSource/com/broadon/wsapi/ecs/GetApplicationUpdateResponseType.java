/**
 * GetApplicationUpdateResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ecs;

public class GetApplicationUpdateResponseType  extends com.broadon.wsapi.ecs.AbstractResponseType  implements java.io.Serializable {
    /** URL used as a prefix for retrieving contents. */
    private org.apache.axis.types.URI contentPrefixURL;
    /** URL used as a prefix for retrieving uncached contents such as TMD. */
    private org.apache.axis.types.URI uncachedContentPrefixURL;
    /** Optional. Timestamp of latest title update. */
    private java.lang.Long titleUpdateTime;
    /** List of titles and their versions that are owned by
 * 										the device and has been updated. */
    private com.broadon.wsapi.ecs.TitleVersionType[] updatedTitles;

    public GetApplicationUpdateResponseType() {
    }

    public GetApplicationUpdateResponseType(
           org.apache.axis.types.URI contentPrefixURL,
           org.apache.axis.types.URI uncachedContentPrefixURL,
           java.lang.Long titleUpdateTime,
           com.broadon.wsapi.ecs.TitleVersionType[] updatedTitles) {
           this.contentPrefixURL = contentPrefixURL;
           this.uncachedContentPrefixURL = uncachedContentPrefixURL;
           this.titleUpdateTime = titleUpdateTime;
           this.updatedTitles = updatedTitles;
    }


    /**
     * Gets the contentPrefixURL value for this GetApplicationUpdateResponseType.
     * 
     * @return contentPrefixURL URL used as a prefix for retrieving contents.
     */
    public org.apache.axis.types.URI getContentPrefixURL() {
        return contentPrefixURL;
    }


    /**
     * Sets the contentPrefixURL value for this GetApplicationUpdateResponseType.
     * 
     * @param contentPrefixURL URL used as a prefix for retrieving contents.
     */
    public void setContentPrefixURL(org.apache.axis.types.URI contentPrefixURL) {
        this.contentPrefixURL = contentPrefixURL;
    }


    /**
     * Gets the uncachedContentPrefixURL value for this GetApplicationUpdateResponseType.
     * 
     * @return uncachedContentPrefixURL URL used as a prefix for retrieving uncached contents such as TMD.
     */
    public org.apache.axis.types.URI getUncachedContentPrefixURL() {
        return uncachedContentPrefixURL;
    }


    /**
     * Sets the uncachedContentPrefixURL value for this GetApplicationUpdateResponseType.
     * 
     * @param uncachedContentPrefixURL URL used as a prefix for retrieving uncached contents such as TMD.
     */
    public void setUncachedContentPrefixURL(org.apache.axis.types.URI uncachedContentPrefixURL) {
        this.uncachedContentPrefixURL = uncachedContentPrefixURL;
    }


    /**
     * Gets the titleUpdateTime value for this GetApplicationUpdateResponseType.
     * 
     * @return titleUpdateTime Optional. Timestamp of latest title update.
     */
    public java.lang.Long getTitleUpdateTime() {
        return titleUpdateTime;
    }


    /**
     * Sets the titleUpdateTime value for this GetApplicationUpdateResponseType.
     * 
     * @param titleUpdateTime Optional. Timestamp of latest title update.
     */
    public void setTitleUpdateTime(java.lang.Long titleUpdateTime) {
        this.titleUpdateTime = titleUpdateTime;
    }


    /**
     * Gets the updatedTitles value for this GetApplicationUpdateResponseType.
     * 
     * @return updatedTitles List of titles and their versions that are owned by
 * 										the device and has been updated.
     */
    public com.broadon.wsapi.ecs.TitleVersionType[] getUpdatedTitles() {
        return updatedTitles;
    }


    /**
     * Sets the updatedTitles value for this GetApplicationUpdateResponseType.
     * 
     * @param updatedTitles List of titles and their versions that are owned by
 * 										the device and has been updated.
     */
    public void setUpdatedTitles(com.broadon.wsapi.ecs.TitleVersionType[] updatedTitles) {
        this.updatedTitles = updatedTitles;
    }

    public com.broadon.wsapi.ecs.TitleVersionType getUpdatedTitles(int i) {
        return this.updatedTitles[i];
    }

    public void setUpdatedTitles(int i, com.broadon.wsapi.ecs.TitleVersionType _value) {
        this.updatedTitles[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetApplicationUpdateResponseType)) return false;
        GetApplicationUpdateResponseType other = (GetApplicationUpdateResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.contentPrefixURL==null && other.getContentPrefixURL()==null) || 
             (this.contentPrefixURL!=null &&
              this.contentPrefixURL.equals(other.getContentPrefixURL()))) &&
            ((this.uncachedContentPrefixURL==null && other.getUncachedContentPrefixURL()==null) || 
             (this.uncachedContentPrefixURL!=null &&
              this.uncachedContentPrefixURL.equals(other.getUncachedContentPrefixURL()))) &&
            ((this.titleUpdateTime==null && other.getTitleUpdateTime()==null) || 
             (this.titleUpdateTime!=null &&
              this.titleUpdateTime.equals(other.getTitleUpdateTime()))) &&
            ((this.updatedTitles==null && other.getUpdatedTitles()==null) || 
             (this.updatedTitles!=null &&
              java.util.Arrays.equals(this.updatedTitles, other.getUpdatedTitles())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getContentPrefixURL() != null) {
            _hashCode += getContentPrefixURL().hashCode();
        }
        if (getUncachedContentPrefixURL() != null) {
            _hashCode += getUncachedContentPrefixURL().hashCode();
        }
        if (getTitleUpdateTime() != null) {
            _hashCode += getTitleUpdateTime().hashCode();
        }
        if (getUpdatedTitles() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getUpdatedTitles());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getUpdatedTitles(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetApplicationUpdateResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetApplicationUpdateResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contentPrefixURL");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ContentPrefixURL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "anyURI"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uncachedContentPrefixURL");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "UncachedContentPrefixURL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "anyURI"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titleUpdateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TitleUpdateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TimeStampType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updatedTitles");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "UpdatedTitles"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TitleVersionType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

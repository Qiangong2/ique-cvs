/**
 * IdentityAuthenticationService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ias;

public interface IdentityAuthenticationService extends javax.xml.rpc.Service {
    public java.lang.String getIdentityAuthenticationSOAPAddress();

    public com.broadon.wsapi.ias.IdentityAuthenticationPortType getIdentityAuthenticationSOAP() throws javax.xml.rpc.ServiceException;

    public com.broadon.wsapi.ias.IdentityAuthenticationPortType getIdentityAuthenticationSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}

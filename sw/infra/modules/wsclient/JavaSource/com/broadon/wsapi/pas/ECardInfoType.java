/**
 * ECardInfoType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.pas;

public class ECardInfoType  implements java.io.Serializable {
    private com.broadon.wsapi.pas.ECardKindType ECardKind;
    private java.lang.Long activatedTime;
    private java.lang.Long revokedTime;
    private java.lang.Long lastUsedTime;
    private com.broadon.wsapi.pas.BalanceType balance;

    public ECardInfoType() {
    }

    public ECardInfoType(
           com.broadon.wsapi.pas.ECardKindType ECardKind,
           java.lang.Long activatedTime,
           java.lang.Long revokedTime,
           java.lang.Long lastUsedTime,
           com.broadon.wsapi.pas.BalanceType balance) {
           this.ECardKind = ECardKind;
           this.activatedTime = activatedTime;
           this.revokedTime = revokedTime;
           this.lastUsedTime = lastUsedTime;
           this.balance = balance;
    }


    /**
     * Gets the ECardKind value for this ECardInfoType.
     * 
     * @return ECardKind
     */
    public com.broadon.wsapi.pas.ECardKindType getECardKind() {
        return ECardKind;
    }


    /**
     * Sets the ECardKind value for this ECardInfoType.
     * 
     * @param ECardKind
     */
    public void setECardKind(com.broadon.wsapi.pas.ECardKindType ECardKind) {
        this.ECardKind = ECardKind;
    }


    /**
     * Gets the activatedTime value for this ECardInfoType.
     * 
     * @return activatedTime
     */
    public java.lang.Long getActivatedTime() {
        return activatedTime;
    }


    /**
     * Sets the activatedTime value for this ECardInfoType.
     * 
     * @param activatedTime
     */
    public void setActivatedTime(java.lang.Long activatedTime) {
        this.activatedTime = activatedTime;
    }


    /**
     * Gets the revokedTime value for this ECardInfoType.
     * 
     * @return revokedTime
     */
    public java.lang.Long getRevokedTime() {
        return revokedTime;
    }


    /**
     * Sets the revokedTime value for this ECardInfoType.
     * 
     * @param revokedTime
     */
    public void setRevokedTime(java.lang.Long revokedTime) {
        this.revokedTime = revokedTime;
    }


    /**
     * Gets the lastUsedTime value for this ECardInfoType.
     * 
     * @return lastUsedTime
     */
    public java.lang.Long getLastUsedTime() {
        return lastUsedTime;
    }


    /**
     * Sets the lastUsedTime value for this ECardInfoType.
     * 
     * @param lastUsedTime
     */
    public void setLastUsedTime(java.lang.Long lastUsedTime) {
        this.lastUsedTime = lastUsedTime;
    }


    /**
     * Gets the balance value for this ECardInfoType.
     * 
     * @return balance
     */
    public com.broadon.wsapi.pas.BalanceType getBalance() {
        return balance;
    }


    /**
     * Sets the balance value for this ECardInfoType.
     * 
     * @param balance
     */
    public void setBalance(com.broadon.wsapi.pas.BalanceType balance) {
        this.balance = balance;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ECardInfoType)) return false;
        ECardInfoType other = (ECardInfoType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ECardKind==null && other.getECardKind()==null) || 
             (this.ECardKind!=null &&
              this.ECardKind.equals(other.getECardKind()))) &&
            ((this.activatedTime==null && other.getActivatedTime()==null) || 
             (this.activatedTime!=null &&
              this.activatedTime.equals(other.getActivatedTime()))) &&
            ((this.revokedTime==null && other.getRevokedTime()==null) || 
             (this.revokedTime!=null &&
              this.revokedTime.equals(other.getRevokedTime()))) &&
            ((this.lastUsedTime==null && other.getLastUsedTime()==null) || 
             (this.lastUsedTime!=null &&
              this.lastUsedTime.equals(other.getLastUsedTime()))) &&
            ((this.balance==null && other.getBalance()==null) || 
             (this.balance!=null &&
              this.balance.equals(other.getBalance())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getECardKind() != null) {
            _hashCode += getECardKind().hashCode();
        }
        if (getActivatedTime() != null) {
            _hashCode += getActivatedTime().hashCode();
        }
        if (getRevokedTime() != null) {
            _hashCode += getRevokedTime().hashCode();
        }
        if (getLastUsedTime() != null) {
            _hashCode += getLastUsedTime().hashCode();
        }
        if (getBalance() != null) {
            _hashCode += getBalance().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ECardInfoType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ECardInfoType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ECardKind");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ECardKind"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ECardKindType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("activatedTime");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ActivatedTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "TimeStampType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("revokedTime");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "RevokedTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "TimeStampType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastUsedTime");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "LastUsedTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "TimeStampType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("balance");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "Balance"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "BalanceType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

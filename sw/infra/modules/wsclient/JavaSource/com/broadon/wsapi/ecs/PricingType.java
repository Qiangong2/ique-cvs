/**
 * PricingType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ecs;

public class PricingType  implements java.io.Serializable {
    private int itemId;
    private com.broadon.wsapi.ecs.LimitType[] limits;
    private com.broadon.wsapi.ecs.PriceType price;
    /** Whether the price is for a Subscription, Bonus, 
 * 						Rental, Trial, or Permanent purchase */
    private com.broadon.wsapi.ecs.PricingCategoryType pricingCategory;

    public PricingType() {
    }

    public PricingType(
           int itemId,
           com.broadon.wsapi.ecs.LimitType[] limits,
           com.broadon.wsapi.ecs.PriceType price,
           com.broadon.wsapi.ecs.PricingCategoryType pricingCategory) {
           this.itemId = itemId;
           this.limits = limits;
           this.price = price;
           this.pricingCategory = pricingCategory;
    }


    /**
     * Gets the itemId value for this PricingType.
     * 
     * @return itemId
     */
    public int getItemId() {
        return itemId;
    }


    /**
     * Sets the itemId value for this PricingType.
     * 
     * @param itemId
     */
    public void setItemId(int itemId) {
        this.itemId = itemId;
    }


    /**
     * Gets the limits value for this PricingType.
     * 
     * @return limits
     */
    public com.broadon.wsapi.ecs.LimitType[] getLimits() {
        return limits;
    }


    /**
     * Sets the limits value for this PricingType.
     * 
     * @param limits
     */
    public void setLimits(com.broadon.wsapi.ecs.LimitType[] limits) {
        this.limits = limits;
    }

    public com.broadon.wsapi.ecs.LimitType getLimits(int i) {
        return this.limits[i];
    }

    public void setLimits(int i, com.broadon.wsapi.ecs.LimitType _value) {
        this.limits[i] = _value;
    }


    /**
     * Gets the price value for this PricingType.
     * 
     * @return price
     */
    public com.broadon.wsapi.ecs.PriceType getPrice() {
        return price;
    }


    /**
     * Sets the price value for this PricingType.
     * 
     * @param price
     */
    public void setPrice(com.broadon.wsapi.ecs.PriceType price) {
        this.price = price;
    }


    /**
     * Gets the pricingCategory value for this PricingType.
     * 
     * @return pricingCategory Whether the price is for a Subscription, Bonus, 
 * 						Rental, Trial, or Permanent purchase
     */
    public com.broadon.wsapi.ecs.PricingCategoryType getPricingCategory() {
        return pricingCategory;
    }


    /**
     * Sets the pricingCategory value for this PricingType.
     * 
     * @param pricingCategory Whether the price is for a Subscription, Bonus, 
 * 						Rental, Trial, or Permanent purchase
     */
    public void setPricingCategory(com.broadon.wsapi.ecs.PricingCategoryType pricingCategory) {
        this.pricingCategory = pricingCategory;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PricingType)) return false;
        PricingType other = (PricingType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.itemId == other.getItemId() &&
            ((this.limits==null && other.getLimits()==null) || 
             (this.limits!=null &&
              java.util.Arrays.equals(this.limits, other.getLimits()))) &&
            ((this.price==null && other.getPrice()==null) || 
             (this.price!=null &&
              this.price.equals(other.getPrice()))) &&
            ((this.pricingCategory==null && other.getPricingCategory()==null) || 
             (this.pricingCategory!=null &&
              this.pricingCategory.equals(other.getPricingCategory())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getItemId();
        if (getLimits() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getLimits());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getLimits(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPrice() != null) {
            _hashCode += getPrice().hashCode();
        }
        if (getPricingCategory() != null) {
            _hashCode += getPricingCategory().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PricingType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PricingType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ItemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ItemIdType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("limits");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "Limits"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "LimitType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("price");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "Price"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PriceType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pricingCategory");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PricingCategory"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PricingCategoryType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

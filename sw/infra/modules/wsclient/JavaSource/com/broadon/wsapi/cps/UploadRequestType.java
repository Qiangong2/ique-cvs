/**
 * UploadRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.cps;

public class UploadRequestType  implements java.io.Serializable {
    
    private static final long serialVersionUID = 2L;
    
    private final int uploadRequestVersion = 2;
    private com.broadon.wsapi.cps.ActionType action;
    private byte[] titleMetaData;
    private java.lang.String titleName;
    private java.lang.String titleType;
    private byte[] encryptedTitleKey;
    private byte[] encryptedChannelKey;
    private java.lang.Boolean allowCommonTicket;
    private long publishDate;
    private com.broadon.wsapi.cps.ContentLinkType[] contentLink;
    private java.lang.String productCode;
    /** Smart Card ID that authorized generation of the TMD. */
    private java.lang.String authorization;
    private byte[] commonETicket;
    private byte[] metaData;

    public UploadRequestType() {
    }

    public UploadRequestType(
           com.broadon.wsapi.cps.ActionType action,
           byte[] titleMetaData,
           java.lang.String titleName,
           java.lang.String titleType,
           byte[] encryptedTitleKey,
           byte[] encryptedChannelKey,
           java.lang.Boolean allowCommonTicket,
           long publishDate,
           com.broadon.wsapi.cps.ContentLinkType[] contentLink,
           java.lang.String productCode,
           java.lang.String authorization,
           byte[] commonETicket,
           byte[] metaData) {
           this.action = action;
           this.titleMetaData = titleMetaData;
           this.titleName = titleName;
           this.titleType = titleType;
           this.encryptedTitleKey = encryptedTitleKey;
           this.encryptedChannelKey = encryptedChannelKey;
           this.allowCommonTicket = allowCommonTicket;
           this.publishDate = publishDate;
           this.contentLink = contentLink;
           this.productCode = productCode;
           this.authorization = authorization;
           this.commonETicket = commonETicket;
           this.metaData = metaData;
    }


    /**
     * Gets the uploadRequestVersion value for this UploadRequestType.
     * 
     * @return uploadRequestVersion
     */
    public int getUploadRequestVersion() {
        return uploadRequestVersion;
    }


    /**
     * Sets the uploadRequestVersion value for this UploadRequestType.
     * 
     * @param uploadRequestVersion
     */
    public void setUploadRequestVersion(int uploadRequestVersion) {
    }


    /**
     * Gets the action value for this UploadRequestType.
     * 
     * @return action
     */
    public com.broadon.wsapi.cps.ActionType getAction() {
        return action;
    }


    /**
     * Sets the action value for this UploadRequestType.
     * 
     * @param action
     */
    public void setAction(com.broadon.wsapi.cps.ActionType action) {
        this.action = action;
    }


    /**
     * Gets the titleMetaData value for this UploadRequestType.
     * 
     * @return titleMetaData
     */
    public byte[] getTitleMetaData() {
        return titleMetaData;
    }


    /**
     * Sets the titleMetaData value for this UploadRequestType.
     * 
     * @param titleMetaData
     */
    public void setTitleMetaData(byte[] titleMetaData) {
        this.titleMetaData = titleMetaData;
    }


    /**
     * Gets the titleName value for this UploadRequestType.
     * 
     * @return titleName
     */
    public java.lang.String getTitleName() {
        return titleName;
    }


    /**
     * Sets the titleName value for this UploadRequestType.
     * 
     * @param titleName
     */
    public void setTitleName(java.lang.String titleName) {
        this.titleName = titleName;
    }


    /**
     * Gets the titleType value for this UploadRequestType.
     * 
     * @return titleType
     */
    public java.lang.String getTitleType() {
        return titleType;
    }


    /**
     * Sets the titleType value for this UploadRequestType.
     * 
     * @param titleType
     */
    public void setTitleType(java.lang.String titleType) {
        this.titleType = titleType;
    }


    /**
     * Gets the encryptedTitleKey value for this UploadRequestType.
     * 
     * @return encryptedTitleKey
     */
    public byte[] getEncryptedTitleKey() {
        return encryptedTitleKey;
    }


    /**
     * Sets the encryptedTitleKey value for this UploadRequestType.
     * 
     * @param encryptedTitleKey
     */
    public void setEncryptedTitleKey(byte[] encryptedTitleKey) {
        this.encryptedTitleKey = encryptedTitleKey;
    }


    /**
     * Gets the encryptedChannelKey value for this UploadRequestType.
     * 
     * @return encryptedChannelKey
     */
    public byte[] getEncryptedChannelKey() {
        return encryptedChannelKey;
    }


    /**
     * Sets the encryptedChannelKey value for this UploadRequestType.
     * 
     * @param encryptedChannelKey
     */
    public void setEncryptedChannelKey(byte[] encryptedChannelKey) {
        this.encryptedChannelKey = encryptedChannelKey;
    }


    /**
     * Gets the allowCommonTicket value for this UploadRequestType.
     * 
     * @return allowCommonTicket
     */
    public java.lang.Boolean getAllowCommonTicket() {
        return allowCommonTicket;
    }


    /**
     * Sets the allowCommonTicket value for this UploadRequestType.
     * 
     * @param allowCommonTicket
     */
    public void setAllowCommonTicket(java.lang.Boolean allowCommonTicket) {
        this.allowCommonTicket = allowCommonTicket;
    }


    /**
     * Gets the publishDate value for this UploadRequestType.
     * 
     * @return publishDate
     */
    public long getPublishDate() {
        return publishDate;
    }


    /**
     * Sets the publishDate value for this UploadRequestType.
     * 
     * @param publishDate
     */
    public void setPublishDate(long publishDate) {
        this.publishDate = publishDate;
    }


    /**
     * Gets the contentLink value for this UploadRequestType.
     * 
     * @return contentLink
     */
    public com.broadon.wsapi.cps.ContentLinkType[] getContentLink() {
        return contentLink;
    }


    /**
     * Sets the contentLink value for this UploadRequestType.
     * 
     * @param contentLink
     */
    public void setContentLink(com.broadon.wsapi.cps.ContentLinkType[] contentLink) {
        this.contentLink = contentLink;
    }

    public com.broadon.wsapi.cps.ContentLinkType getContentLink(int i) {
        return this.contentLink[i];
    }

    public void setContentLink(int i, com.broadon.wsapi.cps.ContentLinkType _value) {
        this.contentLink[i] = _value;
    }


    /**
     * Gets the productCode value for this UploadRequestType.
     * 
     * @return productCode
     */
    public java.lang.String getProductCode() {
        return productCode;
    }


    /**
     * Sets the productCode value for this UploadRequestType.
     * 
     * @param productCode
     */
    public void setProductCode(java.lang.String productCode) {
        this.productCode = productCode;
    }


    /**
     * Gets the authorization value for this UploadRequestType.
     * 
     * @return authorization Smart Card ID that authorized generation of the TMD.
     */
    public java.lang.String getAuthorization() {
        return authorization;
    }


    /**
     * Sets the authorization value for this UploadRequestType.
     * 
     * @param authorization Smart Card ID that authorized generation of the TMD.
     */
    public void setAuthorization(java.lang.String authorization) {
        this.authorization = authorization;
    }


    /**
     * Gets the commonETicket value for this UploadRequestType.
     * 
     * @return commonETicket
     */
    public byte[] getCommonETicket() {
        return commonETicket;
    }


    /**
     * Sets the commonETicket value for this UploadRequestType.
     * 
     * @param commonETicket
     */
    public void setCommonETicket(byte[] commonETicket) {
        this.commonETicket = commonETicket;
    }


    /**
     * Gets the metaData value for this UploadRequestType.
     * 
     * @return metaData
     */
    public byte[] getMetaData() {
        return metaData;
    }


    /**
     * Sets the metaData value for this UploadRequestType.
     * 
     * @param metaData
     */
    public void setMetaData(byte[] metaData) {
        this.metaData = metaData;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UploadRequestType)) return false;
        UploadRequestType other = (UploadRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.uploadRequestVersion == other.getUploadRequestVersion() &&
            ((this.action==null && other.getAction()==null) || 
             (this.action!=null &&
              this.action.equals(other.getAction()))) &&
            ((this.titleMetaData==null && other.getTitleMetaData()==null) || 
             (this.titleMetaData!=null &&
              java.util.Arrays.equals(this.titleMetaData, other.getTitleMetaData()))) &&
            ((this.titleName==null && other.getTitleName()==null) || 
             (this.titleName!=null &&
              this.titleName.equals(other.getTitleName()))) &&
            ((this.titleType==null && other.getTitleType()==null) || 
             (this.titleType!=null &&
              this.titleType.equals(other.getTitleType()))) &&
            ((this.encryptedTitleKey==null && other.getEncryptedTitleKey()==null) || 
             (this.encryptedTitleKey!=null &&
              java.util.Arrays.equals(this.encryptedTitleKey, other.getEncryptedTitleKey()))) &&
            ((this.encryptedChannelKey==null && other.getEncryptedChannelKey()==null) || 
             (this.encryptedChannelKey!=null &&
              java.util.Arrays.equals(this.encryptedChannelKey, other.getEncryptedChannelKey()))) &&
            ((this.allowCommonTicket==null && other.getAllowCommonTicket()==null) || 
             (this.allowCommonTicket!=null &&
              this.allowCommonTicket.equals(other.getAllowCommonTicket()))) &&
            this.publishDate == other.getPublishDate() &&
            ((this.contentLink==null && other.getContentLink()==null) || 
             (this.contentLink!=null &&
              java.util.Arrays.equals(this.contentLink, other.getContentLink()))) &&
            ((this.productCode==null && other.getProductCode()==null) || 
             (this.productCode!=null &&
              this.productCode.equals(other.getProductCode()))) &&
            ((this.authorization==null && other.getAuthorization()==null) || 
             (this.authorization!=null &&
              this.authorization.equals(other.getAuthorization()))) &&
            ((this.commonETicket==null && other.getCommonETicket()==null) || 
             (this.commonETicket!=null &&
              java.util.Arrays.equals(this.commonETicket, other.getCommonETicket()))) &&
            ((this.metaData==null && other.getMetaData()==null) || 
             (this.metaData!=null &&
              java.util.Arrays.equals(this.metaData, other.getMetaData())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getUploadRequestVersion();
        if (getAction() != null) {
            _hashCode += getAction().hashCode();
        }
        if (getTitleMetaData() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTitleMetaData());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTitleMetaData(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTitleName() != null) {
            _hashCode += getTitleName().hashCode();
        }
        if (getTitleType() != null) {
            _hashCode += getTitleType().hashCode();
        }
        if (getEncryptedTitleKey() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getEncryptedTitleKey());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getEncryptedTitleKey(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getEncryptedChannelKey() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getEncryptedChannelKey());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getEncryptedChannelKey(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getAllowCommonTicket() != null) {
            _hashCode += getAllowCommonTicket().hashCode();
        }
        _hashCode += new Long(getPublishDate()).hashCode();
        if (getContentLink() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getContentLink());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getContentLink(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getProductCode() != null) {
            _hashCode += getProductCode().hashCode();
        }
        if (getAuthorization() != null) {
            _hashCode += getAuthorization().hashCode();
        }
        if (getCommonETicket() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCommonETicket());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCommonETicket(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getMetaData() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getMetaData());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getMetaData(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UploadRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "UploadRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uploadRequestVersion");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "UploadRequestVersion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("action");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "Action"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "ActionType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titleMetaData");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "TitleMetaData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titleName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "TitleName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titleType");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "TitleType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("encryptedTitleKey");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "EncryptedTitleKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("encryptedChannelKey");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "EncryptedChannelKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("allowCommonTicket");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "AllowCommonTicket"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("publishDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "PublishDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contentLink");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "ContentLink"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "ContentLinkType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productCode");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "ProductCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authorization");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "Authorization"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commonETicket");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "CommonETicket"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("metaData");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:cps.wsapi.broadon.com", "MetaData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

/**
 * UpdateStatusRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ecs;

public class UpdateStatusRequestType  extends com.broadon.wsapi.ecs.AbstractRequestType  implements java.io.Serializable {
    /** List of consumption counts indicating how long each title
 * 										was played. */
    private com.broadon.wsapi.ecs.ConsumptionType[] consumptions;

    public UpdateStatusRequestType() {
    }

    public UpdateStatusRequestType(
           com.broadon.wsapi.ecs.ConsumptionType[] consumptions) {
           this.consumptions = consumptions;
    }


    /**
     * Gets the consumptions value for this UpdateStatusRequestType.
     * 
     * @return consumptions List of consumption counts indicating how long each title
 * 										was played.
     */
    public com.broadon.wsapi.ecs.ConsumptionType[] getConsumptions() {
        return consumptions;
    }


    /**
     * Sets the consumptions value for this UpdateStatusRequestType.
     * 
     * @param consumptions List of consumption counts indicating how long each title
 * 										was played.
     */
    public void setConsumptions(com.broadon.wsapi.ecs.ConsumptionType[] consumptions) {
        this.consumptions = consumptions;
    }

    public com.broadon.wsapi.ecs.ConsumptionType getConsumptions(int i) {
        return this.consumptions[i];
    }

    public void setConsumptions(int i, com.broadon.wsapi.ecs.ConsumptionType _value) {
        this.consumptions[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpdateStatusRequestType)) return false;
        UpdateStatusRequestType other = (UpdateStatusRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.consumptions==null && other.getConsumptions()==null) || 
             (this.consumptions!=null &&
              java.util.Arrays.equals(this.consumptions, other.getConsumptions())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getConsumptions() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getConsumptions());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getConsumptions(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UpdateStatusRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "UpdateStatusRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("consumptions");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "Consumptions"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ConsumptionType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

/**
 * DeviceInfoType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ias;

public class DeviceInfoType  implements java.io.Serializable {
    private java.lang.String publicKey;
    private java.lang.String serialNumber;
    private byte[] configCode;
    private int deviceTypeId;
    private long registerDate;

    public DeviceInfoType() {
    }

    public DeviceInfoType(
           java.lang.String publicKey,
           java.lang.String serialNumber,
           byte[] configCode,
           int deviceTypeId,
           long registerDate) {
           this.publicKey = publicKey;
           this.serialNumber = serialNumber;
           this.configCode = configCode;
           this.deviceTypeId = deviceTypeId;
           this.registerDate = registerDate;
    }


    /**
     * Gets the publicKey value for this DeviceInfoType.
     * 
     * @return publicKey
     */
    public java.lang.String getPublicKey() {
        return publicKey;
    }


    /**
     * Sets the publicKey value for this DeviceInfoType.
     * 
     * @param publicKey
     */
    public void setPublicKey(java.lang.String publicKey) {
        this.publicKey = publicKey;
    }


    /**
     * Gets the serialNumber value for this DeviceInfoType.
     * 
     * @return serialNumber
     */
    public java.lang.String getSerialNumber() {
        return serialNumber;
    }


    /**
     * Sets the serialNumber value for this DeviceInfoType.
     * 
     * @param serialNumber
     */
    public void setSerialNumber(java.lang.String serialNumber) {
        this.serialNumber = serialNumber;
    }


    /**
     * Gets the configCode value for this DeviceInfoType.
     * 
     * @return configCode
     */
    public byte[] getConfigCode() {
        return configCode;
    }


    /**
     * Sets the configCode value for this DeviceInfoType.
     * 
     * @param configCode
     */
    public void setConfigCode(byte[] configCode) {
        this.configCode = configCode;
    }


    /**
     * Gets the deviceTypeId value for this DeviceInfoType.
     * 
     * @return deviceTypeId
     */
    public int getDeviceTypeId() {
        return deviceTypeId;
    }


    /**
     * Sets the deviceTypeId value for this DeviceInfoType.
     * 
     * @param deviceTypeId
     */
    public void setDeviceTypeId(int deviceTypeId) {
        this.deviceTypeId = deviceTypeId;
    }


    /**
     * Gets the registerDate value for this DeviceInfoType.
     * 
     * @return registerDate
     */
    public long getRegisterDate() {
        return registerDate;
    }


    /**
     * Sets the registerDate value for this DeviceInfoType.
     * 
     * @param registerDate
     */
    public void setRegisterDate(long registerDate) {
        this.registerDate = registerDate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DeviceInfoType)) return false;
        DeviceInfoType other = (DeviceInfoType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.publicKey==null && other.getPublicKey()==null) || 
             (this.publicKey!=null &&
              this.publicKey.equals(other.getPublicKey()))) &&
            ((this.serialNumber==null && other.getSerialNumber()==null) || 
             (this.serialNumber!=null &&
              this.serialNumber.equals(other.getSerialNumber()))) &&
            ((this.configCode==null && other.getConfigCode()==null) || 
             (this.configCode!=null &&
              java.util.Arrays.equals(this.configCode, other.getConfigCode()))) &&
            this.deviceTypeId == other.getDeviceTypeId() &&
            this.registerDate == other.getRegisterDate();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPublicKey() != null) {
            _hashCode += getPublicKey().hashCode();
        }
        if (getSerialNumber() != null) {
            _hashCode += getSerialNumber().hashCode();
        }
        if (getConfigCode() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getConfigCode());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getConfigCode(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        _hashCode += getDeviceTypeId();
        _hashCode += new Long(getRegisterDate()).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DeviceInfoType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "DeviceInfoType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("publicKey");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "PublicKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serialNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "SerialNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("configCode");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ConfigCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deviceTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "DeviceTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("registerDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RegisterDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "TimeStampType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

/**
 * PaymentType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ecs;

public class PaymentType  implements java.io.Serializable {
    private com.broadon.wsapi.ecs.PaymentMethodType paymentMethod;
    private com.broadon.wsapi.ecs.ECardPaymentType ECardPayment;
    private com.broadon.wsapi.ecs.AccountPaymentType accountPayment;
    private com.broadon.wsapi.ecs.CreditCardPaymentType creditCardPayment;

    public PaymentType() {
    }

    public PaymentType(
           com.broadon.wsapi.ecs.PaymentMethodType paymentMethod,
           com.broadon.wsapi.ecs.ECardPaymentType ECardPayment,
           com.broadon.wsapi.ecs.AccountPaymentType accountPayment,
           com.broadon.wsapi.ecs.CreditCardPaymentType creditCardPayment) {
           this.paymentMethod = paymentMethod;
           this.ECardPayment = ECardPayment;
           this.accountPayment = accountPayment;
           this.creditCardPayment = creditCardPayment;
    }


    /**
     * Gets the paymentMethod value for this PaymentType.
     * 
     * @return paymentMethod
     */
    public com.broadon.wsapi.ecs.PaymentMethodType getPaymentMethod() {
        return paymentMethod;
    }


    /**
     * Sets the paymentMethod value for this PaymentType.
     * 
     * @param paymentMethod
     */
    public void setPaymentMethod(com.broadon.wsapi.ecs.PaymentMethodType paymentMethod) {
        this.paymentMethod = paymentMethod;
    }


    /**
     * Gets the ECardPayment value for this PaymentType.
     * 
     * @return ECardPayment
     */
    public com.broadon.wsapi.ecs.ECardPaymentType getECardPayment() {
        return ECardPayment;
    }


    /**
     * Sets the ECardPayment value for this PaymentType.
     * 
     * @param ECardPayment
     */
    public void setECardPayment(com.broadon.wsapi.ecs.ECardPaymentType ECardPayment) {
        this.ECardPayment = ECardPayment;
    }


    /**
     * Gets the accountPayment value for this PaymentType.
     * 
     * @return accountPayment
     */
    public com.broadon.wsapi.ecs.AccountPaymentType getAccountPayment() {
        return accountPayment;
    }


    /**
     * Sets the accountPayment value for this PaymentType.
     * 
     * @param accountPayment
     */
    public void setAccountPayment(com.broadon.wsapi.ecs.AccountPaymentType accountPayment) {
        this.accountPayment = accountPayment;
    }


    /**
     * Gets the creditCardPayment value for this PaymentType.
     * 
     * @return creditCardPayment
     */
    public com.broadon.wsapi.ecs.CreditCardPaymentType getCreditCardPayment() {
        return creditCardPayment;
    }


    /**
     * Sets the creditCardPayment value for this PaymentType.
     * 
     * @param creditCardPayment
     */
    public void setCreditCardPayment(com.broadon.wsapi.ecs.CreditCardPaymentType creditCardPayment) {
        this.creditCardPayment = creditCardPayment;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PaymentType)) return false;
        PaymentType other = (PaymentType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.paymentMethod==null && other.getPaymentMethod()==null) || 
             (this.paymentMethod!=null &&
              this.paymentMethod.equals(other.getPaymentMethod()))) &&
            ((this.ECardPayment==null && other.getECardPayment()==null) || 
             (this.ECardPayment!=null &&
              this.ECardPayment.equals(other.getECardPayment()))) &&
            ((this.accountPayment==null && other.getAccountPayment()==null) || 
             (this.accountPayment!=null &&
              this.accountPayment.equals(other.getAccountPayment()))) &&
            ((this.creditCardPayment==null && other.getCreditCardPayment()==null) || 
             (this.creditCardPayment!=null &&
              this.creditCardPayment.equals(other.getCreditCardPayment())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPaymentMethod() != null) {
            _hashCode += getPaymentMethod().hashCode();
        }
        if (getECardPayment() != null) {
            _hashCode += getECardPayment().hashCode();
        }
        if (getAccountPayment() != null) {
            _hashCode += getAccountPayment().hashCode();
        }
        if (getCreditCardPayment() != null) {
            _hashCode += getCreditCardPayment().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PaymentType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PaymentType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentMethod");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PaymentMethod"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PaymentMethodType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ECardPayment");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ECardPayment"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ECardPaymentType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountPayment");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "AccountPayment"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "AccountPaymentType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditCardPayment");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "CreditCardPayment"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "CreditCardPaymentType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

/**
 * ECommerceSOAPBindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ecs;

public class ECommerceSOAPBindingStub extends org.apache.axis.client.Stub implements com.broadon.wsapi.ecs.ECommercePortType {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[19];
        _initOperationDesc1();
        _initOperationDesc2();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ListTitles");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListTitlesRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListTitlesRequestType"), com.broadon.wsapi.ecs.ListTitlesRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListTitlesResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ecs.ListTitlesResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListTitlesResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetTitleDetails");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetTitleDetailsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetTitleDetailsRequestType"), com.broadon.wsapi.ecs.GetTitleDetailsRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetTitleDetailsResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ecs.GetTitleDetailsResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetTitleDetailsResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetMeta");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetMetaRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetMetaRequestType"), com.broadon.wsapi.ecs.GetMetaRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetMetaResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ecs.GetMetaResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetMetaResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("PurchaseTitle");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PurchaseTitleRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PurchaseTitleRequestType"), com.broadon.wsapi.ecs.PurchaseTitleRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PurchaseTitleResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ecs.PurchaseTitleResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PurchaseTitleResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("SyncETickets");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "SyncETicketsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "SyncETicketsRequestType"), com.broadon.wsapi.ecs.SyncETicketsRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "SyncETicketsResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ecs.SyncETicketsResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "SyncETicketsResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Subscribe");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "SubscribeRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "SubscribeRequestType"), com.broadon.wsapi.ecs.SubscribeRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "SubscribeResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ecs.SubscribeResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "SubscribeResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateStatus");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "UpdateStatusRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "UpdateStatusRequestType"), com.broadon.wsapi.ecs.UpdateStatusRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "UpdateStatusResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ecs.UpdateStatusResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "UpdateStatusResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ListSubscriptionPricings");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListSubscriptionPricingsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListSubscriptionPricingsRequestType"), com.broadon.wsapi.ecs.ListSubscriptionPricingsRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListSubscriptionPricingsResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ecs.ListSubscriptionPricingsResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListSubscriptionPricingsResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("RedeemECard");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "RedeemECardRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "RedeemECardRequestType"), com.broadon.wsapi.ecs.RedeemECardRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "RedeemECardResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ecs.RedeemECardResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "RedeemECardResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("PurchasePoints");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PurchasePointsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PurchasePointsRequestType"), com.broadon.wsapi.ecs.PurchasePointsRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PurchasePointsResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ecs.PurchasePointsResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PurchasePointsResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CheckAccountBalance");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "CheckAccountBalanceRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "CheckAccountBalanceRequestType"), com.broadon.wsapi.ecs.CheckAccountBalanceRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "CheckAccountBalanceResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ecs.CheckAccountBalanceResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "CheckAccountBalanceResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("TransferETickets");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TransferETicketsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TransferETicketsRequestType"), com.broadon.wsapi.ecs.TransferETicketsRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TransferETicketsResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ecs.TransferETicketsResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TransferETicketsResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ListTransactions");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListTransactionsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListTransactionsRequestType"), com.broadon.wsapi.ecs.ListTransactionsRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListTransactionsResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ecs.ListTransactionsResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListTransactionsResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[12] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ListETickets");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListETicketsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListETicketsRequestType"), com.broadon.wsapi.ecs.ListETicketsRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListETicketsResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ecs.ListETicketsResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListETicketsResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[13] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("DeleteETickets");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "DeleteETicketsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "DeleteETicketsRequestType"), com.broadon.wsapi.ecs.DeleteETicketsRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "DeleteETicketsResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ecs.DeleteETicketsResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "DeleteETicketsResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[14] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("TransferPoints");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TransferPointsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TransferPointsRequestType"), com.broadon.wsapi.ecs.TransferPointsRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TransferPointsResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ecs.TransferPointsResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TransferPointsResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[15] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetSystemUpdate");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetSystemUpdateRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetSystemUpdateRequestType"), com.broadon.wsapi.ecs.GetSystemUpdateRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetSystemUpdateResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ecs.GetSystemUpdateResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetSystemUpdateResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[16] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetApplicationUpdate");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetApplicationUpdateRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetApplicationUpdateRequestType"), com.broadon.wsapi.ecs.GetApplicationUpdateRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetApplicationUpdateResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ecs.GetApplicationUpdateResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetApplicationUpdateResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[17] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CheckECardBalance");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "CheckECardBalanceRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "CheckECardBalanceRequestType"), com.broadon.wsapi.ecs.CheckECardBalanceRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "CheckECardBalanceResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ecs.CheckECardBalanceResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "CheckECardBalanceResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[18] = oper;

    }

    public ECommerceSOAPBindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public ECommerceSOAPBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public ECommerceSOAPBindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "AbstractPurchaseRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.AbstractPurchaseRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "AbstractPurchaseResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.AbstractPurchaseResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "AbstractRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.AbstractRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "AbstractResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.AbstractResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "AccountPaymentType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.AccountPaymentType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ChannelIdType");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(simplesf);
            cachedDeserFactories.add(simpledf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "CheckAccountBalanceRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.CheckAccountBalanceRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "CheckAccountBalanceResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.CheckAccountBalanceResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "CheckECardBalanceRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.CheckECardBalanceRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "CheckECardBalanceResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.CheckECardBalanceResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ConsumptionType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.ConsumptionType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ContentIdType");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(simplesf);
            cachedDeserFactories.add(simpledf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ContentInfoType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.ContentInfoType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "CreditCardPaymentType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.CreditCardPaymentType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "CurrencyType");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(simplesf);
            cachedDeserFactories.add(simpledf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "DebitCreditType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.DebitCreditType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "DeleteETicketsRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.DeleteETicketsRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "DeleteETicketsResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.DeleteETicketsResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "DeviceIdType");
            cachedSerQNames.add(qName);
            cls = long.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(simplesf);
            cachedDeserFactories.add(simpledf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "DeviceType");
            cachedSerQNames.add(qName);
            cls = int.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(simplesf);
            cachedDeserFactories.add(simpledf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ECardInfoType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.ECardInfoType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ECardKindType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.ECardKindType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ECardPaymentType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.ECardPaymentType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ETicketType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.ETicketType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetApplicationUpdateRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.GetApplicationUpdateRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetApplicationUpdateResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.GetApplicationUpdateResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetMetaRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.GetMetaRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetMetaResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.GetMetaResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetSystemUpdateRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.GetSystemUpdateRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetSystemUpdateResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.GetSystemUpdateResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetTitleDetailsRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.GetTitleDetailsRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "GetTitleDetailsResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.GetTitleDetailsResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ItemIdType");
            cachedSerQNames.add(qName);
            cls = int.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(simplesf);
            cachedDeserFactories.add(simpledf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "LimitKindType");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(simplesf);
            cachedDeserFactories.add(simpledf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "LimitType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.LimitType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListETicketsRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.ListETicketsRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListETicketsResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.ListETicketsResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListSubscriptionPricingsRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.ListSubscriptionPricingsRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListSubscriptionPricingsResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.ListSubscriptionPricingsResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListTitlesRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.ListTitlesRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListTitlesResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.ListTitlesResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListTransactionsRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.ListTransactionsRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ListTransactionsResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.ListTransactionsResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "MoneyType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.MoneyType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PaymentMethodType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.PaymentMethodType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PaymentType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.PaymentType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PriceType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.PriceType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PricingCategoryType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.PricingCategoryType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PricingKindType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.PricingKindType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PricingType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.PricingType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PurchasePointsRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.PurchasePointsRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PurchasePointsResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.PurchasePointsResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PurchaseTitleRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.PurchaseTitleRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PurchaseTitleResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.PurchaseTitleResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "RatingType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.RatingType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "RedeemECardRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.RedeemECardRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "RedeemECardResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.RedeemECardResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "RedeemKindType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.RedeemKindType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "SubscribeRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.SubscribeRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "SubscribeResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.SubscribeResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "SubscriptionPricingType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.SubscriptionPricingType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "SyncETicketsRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.SyncETicketsRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "SyncETicketsResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.SyncETicketsResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TicketedPurchaseRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.TicketedPurchaseRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TicketedPurchaseResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.TicketedPurchaseResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TicketIdType");
            cachedSerQNames.add(qName);
            cls = long.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(simplesf);
            cachedDeserFactories.add(simpledf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TimeDurationType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.TimeDurationType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TimeStampType");
            cachedSerQNames.add(qName);
            cls = long.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(simplesf);
            cachedDeserFactories.add(simpledf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TimeUnitType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.TimeUnitType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TitleIdType");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(simplesf);
            cachedDeserFactories.add(simpledf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TitleInfoType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.TitleInfoType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TitleKindType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.TitleKindType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TitleType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.TitleType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TitleVersionType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.TitleVersionType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TransactionType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.TransactionType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TransferETicketsRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.TransferETicketsRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TransferETicketsResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.TransferETicketsResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TransferPointsRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.TransferPointsRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TransferPointsResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.TransferPointsResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "UpdateStatusRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.UpdateStatusRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "UpdateStatusResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ecs.UpdateStatusResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.broadon.wsapi.ecs.ListTitlesResponseType listTitles(com.broadon.wsapi.ecs.ListTitlesRequestType listTitlesRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ecs.wsapi.broadon.com/ListTitles");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "ListTitles"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {listTitlesRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ecs.ListTitlesResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ecs.ListTitlesResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ecs.ListTitlesResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ecs.GetTitleDetailsResponseType getTitleDetails(com.broadon.wsapi.ecs.GetTitleDetailsRequestType getTitleDetailsRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ecs.wsapi.broadon.com/GetTitleDetails");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetTitleDetails"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getTitleDetailsRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ecs.GetTitleDetailsResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ecs.GetTitleDetailsResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ecs.GetTitleDetailsResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ecs.GetMetaResponseType getMeta(com.broadon.wsapi.ecs.GetMetaRequestType getMetaRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ecs.wsapi.broadon.com/GetMeta");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetMeta"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getMetaRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ecs.GetMetaResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ecs.GetMetaResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ecs.GetMetaResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ecs.PurchaseTitleResponseType purchaseTitle(com.broadon.wsapi.ecs.PurchaseTitleRequestType purchaseTitleRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ecs.wsapi.broadon.com/PurchaseTitle");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "PurchaseTitle"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {purchaseTitleRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ecs.PurchaseTitleResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ecs.PurchaseTitleResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ecs.PurchaseTitleResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ecs.SyncETicketsResponseType syncETickets(com.broadon.wsapi.ecs.SyncETicketsRequestType syncETicketsRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ecs.wsapi.broadon.com/SyncETickets");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "SyncETickets"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {syncETicketsRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ecs.SyncETicketsResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ecs.SyncETicketsResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ecs.SyncETicketsResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ecs.SubscribeResponseType subscribe(com.broadon.wsapi.ecs.SubscribeRequestType subscribeRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ecs.wsapi.broadon.com/Subscribe");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "Subscribe"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {subscribeRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ecs.SubscribeResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ecs.SubscribeResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ecs.SubscribeResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ecs.UpdateStatusResponseType updateStatus(com.broadon.wsapi.ecs.UpdateStatusRequestType updateStatusRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ecs.wsapi.broadon.com/UpdateStatus");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "UpdateStatus"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {updateStatusRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ecs.UpdateStatusResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ecs.UpdateStatusResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ecs.UpdateStatusResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ecs.ListSubscriptionPricingsResponseType listSubscriptionPricings(com.broadon.wsapi.ecs.ListSubscriptionPricingsRequestType listSubscriptionPricingsRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ecs.wsapi.broadon.com/ListSubscriptionPricings");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "ListSubscriptionPricings"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {listSubscriptionPricingsRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ecs.ListSubscriptionPricingsResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ecs.ListSubscriptionPricingsResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ecs.ListSubscriptionPricingsResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ecs.RedeemECardResponseType redeemECard(com.broadon.wsapi.ecs.RedeemECardRequestType redeemECardRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ecs.wsapi.broadon.com/RedeemECard");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "RedeemECard"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {redeemECardRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ecs.RedeemECardResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ecs.RedeemECardResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ecs.RedeemECardResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ecs.PurchasePointsResponseType purchasePoints(com.broadon.wsapi.ecs.PurchasePointsRequestType purchasePointsRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ecs.wsapi.broadon.com/PurchasePoints");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "PurchasePoints"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {purchasePointsRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ecs.PurchasePointsResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ecs.PurchasePointsResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ecs.PurchasePointsResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ecs.CheckAccountBalanceResponseType checkAccountBalance(com.broadon.wsapi.ecs.CheckAccountBalanceRequestType checkAccountBalanceRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ecs.wsapi.broadon.com/CheckAccountBalance");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "CheckAccountBalance"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {checkAccountBalanceRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ecs.CheckAccountBalanceResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ecs.CheckAccountBalanceResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ecs.CheckAccountBalanceResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ecs.TransferETicketsResponseType transferETickets(com.broadon.wsapi.ecs.TransferETicketsRequestType transferETicketsRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ecs.wsapi.broadon.com/TransferETickets");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "TransferETickets"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {transferETicketsRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ecs.TransferETicketsResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ecs.TransferETicketsResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ecs.TransferETicketsResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ecs.ListTransactionsResponseType listTransactions(com.broadon.wsapi.ecs.ListTransactionsRequestType listTransactionsRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ecs.wsapi.broadon.com/ListTransactions");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "ListTransactions"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {listTransactionsRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ecs.ListTransactionsResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ecs.ListTransactionsResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ecs.ListTransactionsResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ecs.ListETicketsResponseType listETickets(com.broadon.wsapi.ecs.ListETicketsRequestType listETicketsRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[13]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ecs.wsapi.broadon.com/ListETickets");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "ListETickets"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {listETicketsRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ecs.ListETicketsResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ecs.ListETicketsResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ecs.ListETicketsResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ecs.DeleteETicketsResponseType deleteETickets(com.broadon.wsapi.ecs.DeleteETicketsRequestType deleteETicketsRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[14]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ecs.wsapi.broadon.com/DeleteETickets");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "DeleteETickets"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {deleteETicketsRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ecs.DeleteETicketsResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ecs.DeleteETicketsResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ecs.DeleteETicketsResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ecs.TransferPointsResponseType transferPoints(com.broadon.wsapi.ecs.TransferPointsRequestType transferPointsRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[15]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ecs.wsapi.broadon.com/TransferPoints");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "TransferPoints"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {transferPointsRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ecs.TransferPointsResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ecs.TransferPointsResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ecs.TransferPointsResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ecs.GetSystemUpdateResponseType getSystemUpdate(com.broadon.wsapi.ecs.GetSystemUpdateRequestType getSystemUpdateRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[16]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ecs.wsapi.broadon.com/GetSystemUpdate");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetSystemUpdate"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getSystemUpdateRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ecs.GetSystemUpdateResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ecs.GetSystemUpdateResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ecs.GetSystemUpdateResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ecs.GetApplicationUpdateResponseType getApplicationUpdate(com.broadon.wsapi.ecs.GetApplicationUpdateRequestType getApplicationUpdateRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[17]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ecs.wsapi.broadon.com/GetApplicationUpdate");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetApplicationUpdate"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getApplicationUpdateRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ecs.GetApplicationUpdateResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ecs.GetApplicationUpdateResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ecs.GetApplicationUpdateResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ecs.CheckECardBalanceResponseType checkECardBalance(com.broadon.wsapi.ecs.CheckECardBalanceRequestType checkECardBalanceRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[18]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ecs.wsapi.broadon.com/CheckECardBalance");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "CheckECardBalance"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {checkECardBalanceRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ecs.CheckECardBalanceResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ecs.CheckECardBalanceResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ecs.CheckECardBalanceResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}

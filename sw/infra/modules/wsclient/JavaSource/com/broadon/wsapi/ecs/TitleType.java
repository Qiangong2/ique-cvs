/**
 * TitleType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ecs;


/**
 * Basic information about a title that is returned in ListTitles.
 */
public class TitleType  implements java.io.Serializable {
    private java.lang.String titleId;
    private java.lang.String titleName;
    /** Actual title size (sum of the content sizes) */
    private long titleSize;
    /** Approximate size the title will take on the device's file system
 * 
 * 						(sum of the content sizes, each rounded up to the block size) */
    private long fsSize;
    private java.lang.String category;
    private java.lang.String platform;

    public TitleType() {
    }

    public TitleType(
           java.lang.String titleId,
           java.lang.String titleName,
           long titleSize,
           long fsSize,
           java.lang.String category,
           java.lang.String platform) {
           this.titleId = titleId;
           this.titleName = titleName;
           this.titleSize = titleSize;
           this.fsSize = fsSize;
           this.category = category;
           this.platform = platform;
    }


    /**
     * Gets the titleId value for this TitleType.
     * 
     * @return titleId
     */
    public java.lang.String getTitleId() {
        return titleId;
    }


    /**
     * Sets the titleId value for this TitleType.
     * 
     * @param titleId
     */
    public void setTitleId(java.lang.String titleId) {
        this.titleId = titleId;
    }


    /**
     * Gets the titleName value for this TitleType.
     * 
     * @return titleName
     */
    public java.lang.String getTitleName() {
        return titleName;
    }


    /**
     * Sets the titleName value for this TitleType.
     * 
     * @param titleName
     */
    public void setTitleName(java.lang.String titleName) {
        this.titleName = titleName;
    }


    /**
     * Gets the titleSize value for this TitleType.
     * 
     * @return titleSize Actual title size (sum of the content sizes)
     */
    public long getTitleSize() {
        return titleSize;
    }


    /**
     * Sets the titleSize value for this TitleType.
     * 
     * @param titleSize Actual title size (sum of the content sizes)
     */
    public void setTitleSize(long titleSize) {
        this.titleSize = titleSize;
    }


    /**
     * Gets the fsSize value for this TitleType.
     * 
     * @return fsSize Approximate size the title will take on the device's file system
 * 
 * 						(sum of the content sizes, each rounded up to the block size)
     */
    public long getFsSize() {
        return fsSize;
    }


    /**
     * Sets the fsSize value for this TitleType.
     * 
     * @param fsSize Approximate size the title will take on the device's file system
 * 
 * 						(sum of the content sizes, each rounded up to the block size)
     */
    public void setFsSize(long fsSize) {
        this.fsSize = fsSize;
    }


    /**
     * Gets the category value for this TitleType.
     * 
     * @return category
     */
    public java.lang.String getCategory() {
        return category;
    }


    /**
     * Sets the category value for this TitleType.
     * 
     * @param category
     */
    public void setCategory(java.lang.String category) {
        this.category = category;
    }


    /**
     * Gets the platform value for this TitleType.
     * 
     * @return platform
     */
    public java.lang.String getPlatform() {
        return platform;
    }


    /**
     * Sets the platform value for this TitleType.
     * 
     * @param platform
     */
    public void setPlatform(java.lang.String platform) {
        this.platform = platform;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitleType)) return false;
        TitleType other = (TitleType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.titleId==null && other.getTitleId()==null) || 
             (this.titleId!=null &&
              this.titleId.equals(other.getTitleId()))) &&
            ((this.titleName==null && other.getTitleName()==null) || 
             (this.titleName!=null &&
              this.titleName.equals(other.getTitleName()))) &&
            this.titleSize == other.getTitleSize() &&
            this.fsSize == other.getFsSize() &&
            ((this.category==null && other.getCategory()==null) || 
             (this.category!=null &&
              this.category.equals(other.getCategory()))) &&
            ((this.platform==null && other.getPlatform()==null) || 
             (this.platform!=null &&
              this.platform.equals(other.getPlatform())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTitleId() != null) {
            _hashCode += getTitleId().hashCode();
        }
        if (getTitleName() != null) {
            _hashCode += getTitleName().hashCode();
        }
        _hashCode += new Long(getTitleSize()).hashCode();
        _hashCode += new Long(getFsSize()).hashCode();
        if (getCategory() != null) {
            _hashCode += getCategory().hashCode();
        }
        if (getPlatform() != null) {
            _hashCode += getPlatform().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitleType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TitleType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titleId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TitleId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TitleIdType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titleName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TitleName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titleSize");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TitleSize"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fsSize");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "FsSize"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("category");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "Category"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("platform");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "Platform"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

/**
 * CheckAccountBalanceResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.pas;

public class CheckAccountBalanceResponseType  extends com.broadon.wsapi.pas.AbstractResponseType  implements java.io.Serializable {
    private com.broadon.wsapi.pas.BalanceAccountInfoType balanceAccountInfo;

    public CheckAccountBalanceResponseType() {
    }

    public CheckAccountBalanceResponseType(
           com.broadon.wsapi.pas.BalanceAccountInfoType balanceAccountInfo) {
           this.balanceAccountInfo = balanceAccountInfo;
    }


    /**
     * Gets the balanceAccountInfo value for this CheckAccountBalanceResponseType.
     * 
     * @return balanceAccountInfo
     */
    public com.broadon.wsapi.pas.BalanceAccountInfoType getBalanceAccountInfo() {
        return balanceAccountInfo;
    }


    /**
     * Sets the balanceAccountInfo value for this CheckAccountBalanceResponseType.
     * 
     * @param balanceAccountInfo
     */
    public void setBalanceAccountInfo(com.broadon.wsapi.pas.BalanceAccountInfoType balanceAccountInfo) {
        this.balanceAccountInfo = balanceAccountInfo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CheckAccountBalanceResponseType)) return false;
        CheckAccountBalanceResponseType other = (CheckAccountBalanceResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.balanceAccountInfo==null && other.getBalanceAccountInfo()==null) || 
             (this.balanceAccountInfo!=null &&
              this.balanceAccountInfo.equals(other.getBalanceAccountInfo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getBalanceAccountInfo() != null) {
            _hashCode += getBalanceAccountInfo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CheckAccountBalanceResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CheckAccountBalanceResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("balanceAccountInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "BalanceAccountInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "BalanceAccountInfoType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

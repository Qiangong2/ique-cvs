/**
 * CaptureAccountToAccountResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.pas;

public class CaptureAccountToAccountResponseType  extends com.broadon.wsapi.pas.CapturePaymentResponseType  implements CapturePaymentToAccountResponseIfc, java.io.Serializable {
    private com.broadon.wsapi.pas.MoneyType depositedAmount;
    private com.broadon.wsapi.pas.BalanceType balance;

    public CaptureAccountToAccountResponseType() {
    }

    public CaptureAccountToAccountResponseType(
           com.broadon.wsapi.pas.MoneyType depositedAmount,
           com.broadon.wsapi.pas.BalanceType balance) {
           this.depositedAmount = depositedAmount;
           this.balance = balance;
    }


    /**
     * Gets the depositedAmount value for this CaptureAccountToAccountResponseType.
     * 
     * @return depositedAmount
     */
    public com.broadon.wsapi.pas.MoneyType getDepositedAmount() {
        return depositedAmount;
    }


    /**
     * Sets the depositedAmount value for this CaptureAccountToAccountResponseType.
     * 
     * @param depositedAmount
     */
    public void setDepositedAmount(com.broadon.wsapi.pas.MoneyType depositedAmount) {
        this.depositedAmount = depositedAmount;
    }


    /**
     * Gets the balance value for this CaptureAccountToAccountResponseType.
     * 
     * @return balance
     */
    public com.broadon.wsapi.pas.BalanceType getBalance() {
        return balance;
    }


    /**
     * Sets the balance value for this CaptureAccountToAccountResponseType.
     * 
     * @param balance
     */
    public void setBalance(com.broadon.wsapi.pas.BalanceType balance) {
        this.balance = balance;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CaptureAccountToAccountResponseType)) return false;
        CaptureAccountToAccountResponseType other = (CaptureAccountToAccountResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.depositedAmount==null && other.getDepositedAmount()==null) || 
             (this.depositedAmount!=null &&
              this.depositedAmount.equals(other.getDepositedAmount()))) &&
            ((this.balance==null && other.getBalance()==null) || 
             (this.balance!=null &&
              this.balance.equals(other.getBalance())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getDepositedAmount() != null) {
            _hashCode += getDepositedAmount().hashCode();
        }
        if (getBalance() != null) {
            _hashCode += getBalance().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CaptureAccountToAccountResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CaptureAccountToAccountResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("depositedAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "DepositedAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "MoneyType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("balance");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "Balance"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "BalanceType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

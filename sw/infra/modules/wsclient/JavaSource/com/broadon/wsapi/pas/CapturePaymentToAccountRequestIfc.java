package com.broadon.wsapi.pas;

public interface CapturePaymentToAccountRequestIfc extends CapturePaymentRequestIfc{

    /**
     * Gets the toBalanceAccount value for this CaptureCreditCardToAccountRequestType.
     * 
     * @return toBalanceAccount
     */
    public com.broadon.wsapi.pas.BalanceAccountType getToBalanceAccount();

    /**
     * Sets the toBalanceAccount value for this CaptureCreditCardToAccountRequestType.
     * 
     * @param toBalanceAccount
     */
    public void setToBalanceAccount(
            com.broadon.wsapi.pas.BalanceAccountType toBalanceAccount);

    /**
     * Gets the depositAmount value for this CaptureCreditCardToAccountRequestType.
     * 
     * @return depositAmount
     */
    //public com.broadon.wsapi.pas.MoneyType getDepositAmount();

    /**
     * Sets the depositAmount value for this CaptureCreditCardToAccountRequestType.
     * 
     * @param depositAmount
     */
    //public void setDepositAmount(com.broadon.wsapi.pas.MoneyType depositAmount);

    public boolean equals(java.lang.Object obj);

    public int hashCode();

}
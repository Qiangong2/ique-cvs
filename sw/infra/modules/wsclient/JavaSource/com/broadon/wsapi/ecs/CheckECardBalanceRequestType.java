/**
 * CheckECardBalanceRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ecs;

public class CheckECardBalanceRequestType  extends com.broadon.wsapi.ecs.AbstractRequestType  implements java.io.Serializable {
    private com.broadon.wsapi.ecs.ECardPaymentType ECard;

    public CheckECardBalanceRequestType() {
    }

    public CheckECardBalanceRequestType(
           com.broadon.wsapi.ecs.ECardPaymentType ECard) {
           this.ECard = ECard;
    }


    /**
     * Gets the ECard value for this CheckECardBalanceRequestType.
     * 
     * @return ECard
     */
    public com.broadon.wsapi.ecs.ECardPaymentType getECard() {
        return ECard;
    }


    /**
     * Sets the ECard value for this CheckECardBalanceRequestType.
     * 
     * @param ECard
     */
    public void setECard(com.broadon.wsapi.ecs.ECardPaymentType ECard) {
        this.ECard = ECard;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CheckECardBalanceRequestType)) return false;
        CheckECardBalanceRequestType other = (CheckECardBalanceRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.ECard==null && other.getECard()==null) || 
             (this.ECard!=null &&
              this.ECard.equals(other.getECard())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getECard() != null) {
            _hashCode += getECard().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CheckECardBalanceRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "CheckECardBalanceRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ECard");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ECard"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ECardPaymentType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

package com.broadon.wsapi.pas;

public interface AbstractResponseIfc {

    /* (non-Javadoc)
     * @see com.broadon.wsapi.pas.xyz#getVersion()
     */
    public java.lang.String getVersion();

    /* (non-Javadoc)
     * @see com.broadon.wsapi.pas.xyz#setVersion(java.lang.String)
     */
    public void setVersion(java.lang.String version);

    /* (non-Javadoc)
     * @see com.broadon.wsapi.pas.xyz#getMessageID()
     */
    public java.lang.String getMessageID();

    /* (non-Javadoc)
     * @see com.broadon.wsapi.pas.xyz#setMessageID(java.lang.String)
     */
    public void setMessageID(java.lang.String messageID);

    /* (non-Javadoc)
     * @see com.broadon.wsapi.pas.xyz#getTimeStamp()
     */
    public long getTimeStamp();

    /* (non-Javadoc)
     * @see com.broadon.wsapi.pas.xyz#setTimeStamp(long)
     */
    public void setTimeStamp(long timeStamp);

    /* (non-Javadoc)
     * @see com.broadon.wsapi.pas.xyz#getErrorCode()
     */
    public int getErrorCode();

    /* (non-Javadoc)
     * @see com.broadon.wsapi.pas.xyz#setErrorCode(int)
     */
    public void setErrorCode(int errorCode);

    /* (non-Javadoc)
     * @see com.broadon.wsapi.pas.xyz#getErrorMessage()
     */
    public java.lang.String getErrorMessage();

    /* (non-Javadoc)
     * @see com.broadon.wsapi.pas.xyz#setErrorMessage(java.lang.String)
     */
    public void setErrorMessage(java.lang.String errorMessage);

    /* (non-Javadoc)
     * @see com.broadon.wsapi.pas.xyz#getProperties()
     */
    public com.broadon.wsapi.pas.PropertyType[] getProperties();

    /* (non-Javadoc)
     * @see com.broadon.wsapi.pas.xyz#setProperties(com.broadon.wsapi.pas.PropertyType[])
     */
    public void setProperties(com.broadon.wsapi.pas.PropertyType[] properties);

    /* (non-Javadoc)
     * @see com.broadon.wsapi.pas.xyz#getProperties(int)
     */
    public com.broadon.wsapi.pas.PropertyType getProperties(int i);

    /* (non-Javadoc)
     * @see com.broadon.wsapi.pas.xyz#setProperties(int, com.broadon.wsapi.pas.PropertyType)
     */
    public void setProperties(int i, com.broadon.wsapi.pas.PropertyType _value);

    /* (non-Javadoc)
     * @see com.broadon.wsapi.pas.xyz#equals(java.lang.Object)
     */
    public boolean equals(java.lang.Object obj);

    /* (non-Javadoc)
     * @see com.broadon.wsapi.pas.xyz#hashCode()
     */
    public int hashCode();

}
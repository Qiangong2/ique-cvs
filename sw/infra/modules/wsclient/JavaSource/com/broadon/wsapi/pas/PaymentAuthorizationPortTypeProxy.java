package com.broadon.wsapi.pas;

public class PaymentAuthorizationPortTypeProxy implements com.broadon.wsapi.pas.PaymentAuthorizationPortType {
  private String _endpoint = null;
  private com.broadon.wsapi.pas.PaymentAuthorizationPortType paymentAuthorizationPortType = null;
  
  public PaymentAuthorizationPortTypeProxy() {
    _initPaymentAuthorizationPortTypeProxy();
  }
  
  private void _initPaymentAuthorizationPortTypeProxy() {
    try {
      paymentAuthorizationPortType = (new com.broadon.wsapi.pas.PaymentAuthorizationServiceLocator()).getPaymentAuthorizationSOAP();
      if (paymentAuthorizationPortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)paymentAuthorizationPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)paymentAuthorizationPortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (paymentAuthorizationPortType != null)
      ((javax.xml.rpc.Stub)paymentAuthorizationPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.broadon.wsapi.pas.PaymentAuthorizationPortType getPaymentAuthorizationPortType() {
    if (paymentAuthorizationPortType == null)
      _initPaymentAuthorizationPortTypeProxy();
    return paymentAuthorizationPortType;
  }
  
  public com.broadon.wsapi.pas.AuthorizeAccountPaymentResponseType authorizeAccountPayment(com.broadon.wsapi.pas.AuthorizeAccountPaymentRequestType authorizeAccountPaymentRequest) throws java.rmi.RemoteException{
    if (paymentAuthorizationPortType == null)
      _initPaymentAuthorizationPortTypeProxy();
    return paymentAuthorizationPortType.authorizeAccountPayment(authorizeAccountPaymentRequest);
  }
  
  public com.broadon.wsapi.pas.AuthorizeECardPaymentResponseType authorizeECardPayment(com.broadon.wsapi.pas.AuthorizeECardPaymentRequestType authorizeECardPaymentRequest) throws java.rmi.RemoteException{
    if (paymentAuthorizationPortType == null)
      _initPaymentAuthorizationPortTypeProxy();
    return paymentAuthorizationPortType.authorizeECardPayment(authorizeECardPaymentRequest);
  }
  
  public com.broadon.wsapi.pas.AuthorizeCreditCardPaymentResponseType authorizeCreditCardPayment(com.broadon.wsapi.pas.AuthorizeCreditCardPaymentRequestType authorizeCreditCardPaymentRequest) throws java.rmi.RemoteException{
    if (paymentAuthorizationPortType == null)
      _initPaymentAuthorizationPortTypeProxy();
    return paymentAuthorizationPortType.authorizeCreditCardPayment(authorizeCreditCardPaymentRequest);
  }
  
  public com.broadon.wsapi.pas.CapturePaymentResponseType capturePayment(com.broadon.wsapi.pas.CapturePaymentRequestType capturePaymentRequest) throws java.rmi.RemoteException{
    if (paymentAuthorizationPortType == null)
      _initPaymentAuthorizationPortTypeProxy();
    return paymentAuthorizationPortType.capturePayment(capturePaymentRequest);
  }
  
  public com.broadon.wsapi.pas.RefundPaymentResponseType refundPayment(com.broadon.wsapi.pas.RefundPaymentRequestType refundPaymentRequest) throws java.rmi.RemoteException{
    if (paymentAuthorizationPortType == null)
      _initPaymentAuthorizationPortTypeProxy();
    return paymentAuthorizationPortType.refundPayment(refundPaymentRequest);
  }
  
  public com.broadon.wsapi.pas.VoidAuthorizationResponseType voidAuthorization(com.broadon.wsapi.pas.VoidAuthorizationRequestType voidAuthorizationRequest) throws java.rmi.RemoteException{
    if (paymentAuthorizationPortType == null)
      _initPaymentAuthorizationPortTypeProxy();
    return paymentAuthorizationPortType.voidAuthorization(voidAuthorizationRequest);
  }
  
  public com.broadon.wsapi.pas.GetAuthorizationHistoryResponseType getAuthorizationHistory(com.broadon.wsapi.pas.GetAuthorizationHistoryRequestType getAuthorizationHistoryRequest) throws java.rmi.RemoteException{
    if (paymentAuthorizationPortType == null)
      _initPaymentAuthorizationPortTypeProxy();
    return paymentAuthorizationPortType.getAuthorizationHistory(getAuthorizationHistoryRequest);
  }
  
  public com.broadon.wsapi.pas.GetAccountHistoryResponseType getBalanceAccountHistory(com.broadon.wsapi.pas.GetAccountHistoryRequestType getAccountHistoryRequest) throws java.rmi.RemoteException{
    if (paymentAuthorizationPortType == null)
      _initPaymentAuthorizationPortTypeProxy();
    return paymentAuthorizationPortType.getBalanceAccountHistory(getAccountHistoryRequest);
  }
  
  public com.broadon.wsapi.pas.GetECardHistoryResponseType getECardAccountHistory(com.broadon.wsapi.pas.GetECardHistoryRequestType getECardHistoryRequest) throws java.rmi.RemoteException{
    if (paymentAuthorizationPortType == null)
      _initPaymentAuthorizationPortTypeProxy();
    return paymentAuthorizationPortType.getECardAccountHistory(getECardHistoryRequest);
  }
  
  public com.broadon.wsapi.pas.GetCreditCardHistoryResponseType getCreditCardAccountHistory(com.broadon.wsapi.pas.GetCreditCardHistoryRequestType getCreditCardHistoryRequest) throws java.rmi.RemoteException{
    if (paymentAuthorizationPortType == null)
      _initPaymentAuthorizationPortTypeProxy();
    return paymentAuthorizationPortType.getCreditCardAccountHistory(getCreditCardHistoryRequest);
  }
  
  public com.broadon.wsapi.pas.CaptureAccountToAccountResponseType captureAccountToAccount(com.broadon.wsapi.pas.CaptureAccountToAccountRequestType captureAccountToAccountRequest) throws java.rmi.RemoteException{
    if (paymentAuthorizationPortType == null)
      _initPaymentAuthorizationPortTypeProxy();
    return paymentAuthorizationPortType.captureAccountToAccount(captureAccountToAccountRequest);
  }
  
  public com.broadon.wsapi.pas.CaptureCreditCardToAccountResponseType captureCreditCardToAccount(com.broadon.wsapi.pas.CaptureCreditCardToAccountRequestType captureCreditCardToAccountRequest) throws java.rmi.RemoteException{
    if (paymentAuthorizationPortType == null)
      _initPaymentAuthorizationPortTypeProxy();
    return paymentAuthorizationPortType.captureCreditCardToAccount(captureCreditCardToAccountRequest);
  }
  
  public com.broadon.wsapi.pas.CaptureECardToAccountResponseType captureECardToAccount(com.broadon.wsapi.pas.CaptureECardToAccountRequestType captureECardToAccountRequest) throws java.rmi.RemoteException{
    if (paymentAuthorizationPortType == null)
      _initPaymentAuthorizationPortTypeProxy();
    return paymentAuthorizationPortType.captureECardToAccount(captureECardToAccountRequest);
  }
  
  public com.broadon.wsapi.pas.CheckAccountBalanceResponseType checkAccountBalance(com.broadon.wsapi.pas.CheckAccountBalanceRequestType checkAccountBalanceRequest) throws java.rmi.RemoteException{
    if (paymentAuthorizationPortType == null)
      _initPaymentAuthorizationPortTypeProxy();
    return paymentAuthorizationPortType.checkAccountBalance(checkAccountBalanceRequest);
  }
  
  public com.broadon.wsapi.pas.CheckECardBalanceResponseType checkECardBalance(com.broadon.wsapi.pas.CheckECardBalanceRequestType checkECardBalanceRequest) throws java.rmi.RemoteException{
    if (paymentAuthorizationPortType == null)
      _initPaymentAuthorizationPortTypeProxy();
    return paymentAuthorizationPortType.checkECardBalance(checkECardBalanceRequest);
  }
  
  public com.broadon.wsapi.pas.CreateAccountResponseType createAccount(com.broadon.wsapi.pas.CreateAccountRequestType createAccountRequest) throws java.rmi.RemoteException{
    if (paymentAuthorizationPortType == null)
      _initPaymentAuthorizationPortTypeProxy();
    return paymentAuthorizationPortType.createAccount(createAccountRequest);
  }
  
  public com.broadon.wsapi.pas.DisableAccountResponseType disableAccount(com.broadon.wsapi.pas.DisableAccountRequestType disableAccountRequest) throws java.rmi.RemoteException{
    if (paymentAuthorizationPortType == null)
      _initPaymentAuthorizationPortTypeProxy();
    return paymentAuthorizationPortType.disableAccount(disableAccountRequest);
  }
  
  public com.broadon.wsapi.pas.ResetAccountPINResponseType resetAccountPIN(com.broadon.wsapi.pas.ResetAccountPINRequestType resetAccountPINRequest) throws java.rmi.RemoteException{
    if (paymentAuthorizationPortType == null)
      _initPaymentAuthorizationPortTypeProxy();
    return paymentAuthorizationPortType.resetAccountPIN(resetAccountPINRequest);
  }
  
  public com.broadon.wsapi.pas.GetECardTypeResponseType getECardType(com.broadon.wsapi.pas.GetECardTypeRequestType getECardTypeRequest) throws java.rmi.RemoteException{
    if (paymentAuthorizationPortType == null)
      _initPaymentAuthorizationPortTypeProxy();
    return paymentAuthorizationPortType.getECardType(getECardTypeRequest);
  }
  
}
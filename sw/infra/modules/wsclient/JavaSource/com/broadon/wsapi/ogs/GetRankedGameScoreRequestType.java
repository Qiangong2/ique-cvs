/**
 * GetRankedGameScoreRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ogs;

public class GetRankedGameScoreRequestType  extends com.broadon.wsapi.ogs.AbstractRequestType  implements java.io.Serializable {
    private int gameId;
    private int scoreId;
    private int itemId;
    private java.lang.Integer accountId;
    private int rankBegin;
    private int maxResults;

    public GetRankedGameScoreRequestType() {
    }

    public GetRankedGameScoreRequestType(
           int gameId,
           int scoreId,
           int itemId,
           java.lang.Integer accountId,
           int rankBegin,
           int maxResults) {
           this.gameId = gameId;
           this.scoreId = scoreId;
           this.itemId = itemId;
           this.accountId = accountId;
           this.rankBegin = rankBegin;
           this.maxResults = maxResults;
    }


    /**
     * Gets the gameId value for this GetRankedGameScoreRequestType.
     * 
     * @return gameId
     */
    public int getGameId() {
        return gameId;
    }


    /**
     * Sets the gameId value for this GetRankedGameScoreRequestType.
     * 
     * @param gameId
     */
    public void setGameId(int gameId) {
        this.gameId = gameId;
    }


    /**
     * Gets the scoreId value for this GetRankedGameScoreRequestType.
     * 
     * @return scoreId
     */
    public int getScoreId() {
        return scoreId;
    }


    /**
     * Sets the scoreId value for this GetRankedGameScoreRequestType.
     * 
     * @param scoreId
     */
    public void setScoreId(int scoreId) {
        this.scoreId = scoreId;
    }


    /**
     * Gets the itemId value for this GetRankedGameScoreRequestType.
     * 
     * @return itemId
     */
    public int getItemId() {
        return itemId;
    }


    /**
     * Sets the itemId value for this GetRankedGameScoreRequestType.
     * 
     * @param itemId
     */
    public void setItemId(int itemId) {
        this.itemId = itemId;
    }


    /**
     * Gets the accountId value for this GetRankedGameScoreRequestType.
     * 
     * @return accountId
     */
    public java.lang.Integer getAccountId() {
        return accountId;
    }


    /**
     * Sets the accountId value for this GetRankedGameScoreRequestType.
     * 
     * @param accountId
     */
    public void setAccountId(java.lang.Integer accountId) {
        this.accountId = accountId;
    }


    /**
     * Gets the rankBegin value for this GetRankedGameScoreRequestType.
     * 
     * @return rankBegin
     */
    public int getRankBegin() {
        return rankBegin;
    }


    /**
     * Sets the rankBegin value for this GetRankedGameScoreRequestType.
     * 
     * @param rankBegin
     */
    public void setRankBegin(int rankBegin) {
        this.rankBegin = rankBegin;
    }


    /**
     * Gets the maxResults value for this GetRankedGameScoreRequestType.
     * 
     * @return maxResults
     */
    public int getMaxResults() {
        return maxResults;
    }


    /**
     * Sets the maxResults value for this GetRankedGameScoreRequestType.
     * 
     * @param maxResults
     */
    public void setMaxResults(int maxResults) {
        this.maxResults = maxResults;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetRankedGameScoreRequestType)) return false;
        GetRankedGameScoreRequestType other = (GetRankedGameScoreRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.gameId == other.getGameId() &&
            this.scoreId == other.getScoreId() &&
            this.itemId == other.getItemId() &&
            ((this.accountId==null && other.getAccountId()==null) || 
             (this.accountId!=null &&
              this.accountId.equals(other.getAccountId()))) &&
            this.rankBegin == other.getRankBegin() &&
            this.maxResults == other.getMaxResults();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += getGameId();
        _hashCode += getScoreId();
        _hashCode += getItemId();
        if (getAccountId() != null) {
            _hashCode += getAccountId().hashCode();
        }
        _hashCode += getRankBegin();
        _hashCode += getMaxResults();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetRankedGameScoreRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetRankedGameScoreRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gameId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GameId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scoreId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ScoreId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ItemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "AccountId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "AccountIdType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rankBegin");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "RankBegin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maxResults");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "MaxResults"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

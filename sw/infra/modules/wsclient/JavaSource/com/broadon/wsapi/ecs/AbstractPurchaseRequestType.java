/**
 * AbstractPurchaseRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ecs;

public class AbstractPurchaseRequestType  extends com.broadon.wsapi.ecs.AbstractRequestType  implements java.io.Serializable {
    /** Item to be purchased */
    private int itemId;
    /** Expected price */
    private com.broadon.wsapi.ecs.PriceType price;
    /** Optional amount to discount (in same currency as price) */
    private java.lang.String discount;
    /** Optional amount to tax (in same currency as price) */
    private java.lang.String taxes;
    /** Payment information */
    private com.broadon.wsapi.ecs.PaymentType payment;
    /** Optional string with additional information about the purchase.
 * 								For purchase with credit cards, the NOA tax response should
 * go here. */
    private java.lang.String purchaseInfo;
    /** Optional account for which this transaction will be associated. */
    private com.broadon.wsapi.ecs.AccountPaymentType account;

    public AbstractPurchaseRequestType() {
    }

    public AbstractPurchaseRequestType(
           int itemId,
           com.broadon.wsapi.ecs.PriceType price,
           java.lang.String discount,
           java.lang.String taxes,
           com.broadon.wsapi.ecs.PaymentType payment,
           java.lang.String purchaseInfo,
           com.broadon.wsapi.ecs.AccountPaymentType account) {
           this.itemId = itemId;
           this.price = price;
           this.discount = discount;
           this.taxes = taxes;
           this.payment = payment;
           this.purchaseInfo = purchaseInfo;
           this.account = account;
    }


    /**
     * Gets the itemId value for this AbstractPurchaseRequestType.
     * 
     * @return itemId Item to be purchased
     */
    public int getItemId() {
        return itemId;
    }


    /**
     * Sets the itemId value for this AbstractPurchaseRequestType.
     * 
     * @param itemId Item to be purchased
     */
    public void setItemId(int itemId) {
        this.itemId = itemId;
    }


    /**
     * Gets the price value for this AbstractPurchaseRequestType.
     * 
     * @return price Expected price
     */
    public com.broadon.wsapi.ecs.PriceType getPrice() {
        return price;
    }


    /**
     * Sets the price value for this AbstractPurchaseRequestType.
     * 
     * @param price Expected price
     */
    public void setPrice(com.broadon.wsapi.ecs.PriceType price) {
        this.price = price;
    }


    /**
     * Gets the discount value for this AbstractPurchaseRequestType.
     * 
     * @return discount Optional amount to discount (in same currency as price)
     */
    public java.lang.String getDiscount() {
        return discount;
    }


    /**
     * Sets the discount value for this AbstractPurchaseRequestType.
     * 
     * @param discount Optional amount to discount (in same currency as price)
     */
    public void setDiscount(java.lang.String discount) {
        this.discount = discount;
    }


    /**
     * Gets the taxes value for this AbstractPurchaseRequestType.
     * 
     * @return taxes Optional amount to tax (in same currency as price)
     */
    public java.lang.String getTaxes() {
        return taxes;
    }


    /**
     * Sets the taxes value for this AbstractPurchaseRequestType.
     * 
     * @param taxes Optional amount to tax (in same currency as price)
     */
    public void setTaxes(java.lang.String taxes) {
        this.taxes = taxes;
    }


    /**
     * Gets the payment value for this AbstractPurchaseRequestType.
     * 
     * @return payment Payment information
     */
    public com.broadon.wsapi.ecs.PaymentType getPayment() {
        return payment;
    }


    /**
     * Sets the payment value for this AbstractPurchaseRequestType.
     * 
     * @param payment Payment information
     */
    public void setPayment(com.broadon.wsapi.ecs.PaymentType payment) {
        this.payment = payment;
    }


    /**
     * Gets the purchaseInfo value for this AbstractPurchaseRequestType.
     * 
     * @return purchaseInfo Optional string with additional information about the purchase.
 * 								For purchase with credit cards, the NOA tax response should
 * go here.
     */
    public java.lang.String getPurchaseInfo() {
        return purchaseInfo;
    }


    /**
     * Sets the purchaseInfo value for this AbstractPurchaseRequestType.
     * 
     * @param purchaseInfo Optional string with additional information about the purchase.
 * 								For purchase with credit cards, the NOA tax response should
 * go here.
     */
    public void setPurchaseInfo(java.lang.String purchaseInfo) {
        this.purchaseInfo = purchaseInfo;
    }


    /**
     * Gets the account value for this AbstractPurchaseRequestType.
     * 
     * @return account Optional account for which this transaction will be associated.
     */
    public com.broadon.wsapi.ecs.AccountPaymentType getAccount() {
        return account;
    }


    /**
     * Sets the account value for this AbstractPurchaseRequestType.
     * 
     * @param account Optional account for which this transaction will be associated.
     */
    public void setAccount(com.broadon.wsapi.ecs.AccountPaymentType account) {
        this.account = account;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AbstractPurchaseRequestType)) return false;
        AbstractPurchaseRequestType other = (AbstractPurchaseRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.itemId == other.getItemId() &&
            ((this.price==null && other.getPrice()==null) || 
             (this.price!=null &&
              this.price.equals(other.getPrice()))) &&
            ((this.discount==null && other.getDiscount()==null) || 
             (this.discount!=null &&
              this.discount.equals(other.getDiscount()))) &&
            ((this.taxes==null && other.getTaxes()==null) || 
             (this.taxes!=null &&
              this.taxes.equals(other.getTaxes()))) &&
            ((this.payment==null && other.getPayment()==null) || 
             (this.payment!=null &&
              this.payment.equals(other.getPayment()))) &&
            ((this.purchaseInfo==null && other.getPurchaseInfo()==null) || 
             (this.purchaseInfo!=null &&
              this.purchaseInfo.equals(other.getPurchaseInfo()))) &&
            ((this.account==null && other.getAccount()==null) || 
             (this.account!=null &&
              this.account.equals(other.getAccount())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += getItemId();
        if (getPrice() != null) {
            _hashCode += getPrice().hashCode();
        }
        if (getDiscount() != null) {
            _hashCode += getDiscount().hashCode();
        }
        if (getTaxes() != null) {
            _hashCode += getTaxes().hashCode();
        }
        if (getPayment() != null) {
            _hashCode += getPayment().hashCode();
        }
        if (getPurchaseInfo() != null) {
            _hashCode += getPurchaseInfo().hashCode();
        }
        if (getAccount() != null) {
            _hashCode += getAccount().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AbstractPurchaseRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "AbstractPurchaseRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ItemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ItemIdType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("price");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "Price"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PriceType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("discount");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "Discount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxes");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "Taxes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("payment");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "Payment"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PaymentType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("purchaseInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PurchaseInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("account");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "Account"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "AccountPaymentType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

/**
 * GetGameScoreKeysRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ogs;

public class GetGameScoreKeysRequestType  extends com.broadon.wsapi.ogs.AbstractRequestType  implements java.io.Serializable {
    private java.lang.Integer gameId;
    private java.lang.Integer scoreId;
    private int accountId;

    public GetGameScoreKeysRequestType() {
    }

    public GetGameScoreKeysRequestType(
           java.lang.Integer gameId,
           java.lang.Integer scoreId,
           int accountId) {
           this.gameId = gameId;
           this.scoreId = scoreId;
           this.accountId = accountId;
    }


    /**
     * Gets the gameId value for this GetGameScoreKeysRequestType.
     * 
     * @return gameId
     */
    public java.lang.Integer getGameId() {
        return gameId;
    }


    /**
     * Sets the gameId value for this GetGameScoreKeysRequestType.
     * 
     * @param gameId
     */
    public void setGameId(java.lang.Integer gameId) {
        this.gameId = gameId;
    }


    /**
     * Gets the scoreId value for this GetGameScoreKeysRequestType.
     * 
     * @return scoreId
     */
    public java.lang.Integer getScoreId() {
        return scoreId;
    }


    /**
     * Sets the scoreId value for this GetGameScoreKeysRequestType.
     * 
     * @param scoreId
     */
    public void setScoreId(java.lang.Integer scoreId) {
        this.scoreId = scoreId;
    }


    /**
     * Gets the accountId value for this GetGameScoreKeysRequestType.
     * 
     * @return accountId
     */
    public int getAccountId() {
        return accountId;
    }


    /**
     * Sets the accountId value for this GetGameScoreKeysRequestType.
     * 
     * @param accountId
     */
    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetGameScoreKeysRequestType)) return false;
        GetGameScoreKeysRequestType other = (GetGameScoreKeysRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.gameId==null && other.getGameId()==null) || 
             (this.gameId!=null &&
              this.gameId.equals(other.getGameId()))) &&
            ((this.scoreId==null && other.getScoreId()==null) || 
             (this.scoreId!=null &&
              this.scoreId.equals(other.getScoreId()))) &&
            this.accountId == other.getAccountId();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getGameId() != null) {
            _hashCode += getGameId().hashCode();
        }
        if (getScoreId() != null) {
            _hashCode += getScoreId().hashCode();
        }
        _hashCode += getAccountId();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetGameScoreKeysRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetGameScoreKeysRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gameId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GameId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scoreId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ScoreId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "AccountId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "AccountIdType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

/**
 * IdentityAuthenticationSOAPBindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ias;

public class IdentityAuthenticationSOAPBindingStub extends org.apache.axis.client.Stub implements com.broadon.wsapi.ias.IdentityAuthenticationPortType {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[21];
        _initOperationDesc1();
        _initOperationDesc2();
        _initOperationDesc3();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AuthenticateAccount");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AuthenticateAccount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AuthenticateAccountRequestType"), com.broadon.wsapi.ias.AuthenticateAccountRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AuthenticateAccountResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ias.AuthenticateAccountResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AuthenticateAccountResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AuthenticateDevice");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AuthenticateDevice"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AuthenticateDeviceRequestType"), com.broadon.wsapi.ias.AuthenticateDeviceRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AuthenticateDeviceResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ias.AuthenticateDeviceResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AuthenticateDeviceResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ExportAccount");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ExportAccount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ExportAccountRequestType"), com.broadon.wsapi.ias.ExportAccountRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ExportAccountResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ias.ExportAccountResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ExportAccountResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetAccountAttributes");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetAccountAttributes"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetAccountAttributesRequestType"), com.broadon.wsapi.ias.GetAccountAttributesRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetAccountAttributesResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ias.GetAccountAttributesResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetAccountAttributesResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetAccountInfo");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetAccountInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetAccountInfoRequestType"), com.broadon.wsapi.ias.GetAccountInfoRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetAccountInfoResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ias.GetAccountInfoResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetAccountInfoResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetDeviceAccounts");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetDeviceAccounts"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetDeviceAccountsRequestType"), com.broadon.wsapi.ias.GetDeviceAccountsRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetDeviceAccountsResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ias.GetDeviceAccountsResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetDeviceAccountsResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetDeviceInfo");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetDeviceInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetDeviceInfoRequestType"), com.broadon.wsapi.ias.GetDeviceInfoRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetDeviceInfoResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ias.GetDeviceInfoResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetDeviceInfoResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetDeviceSubscriptions");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetDeviceSubscriptions"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetDeviceSubscriptionsRequestType"), com.broadon.wsapi.ias.GetDeviceSubscriptionsRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetDeviceSubscriptionsResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ias.GetDeviceSubscriptionsResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetDeviceSubscriptionsResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("LinkDeviceAccount");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "LinkDeviceAccount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "LinkDeviceAccountRequestType"), com.broadon.wsapi.ias.LinkDeviceAccountRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "LinkDeviceAccountResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ias.LinkDeviceAccountResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "LinkDeviceAccountResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("RegisterAccount");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RegisterAccount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RegisterAccountRequestType"), com.broadon.wsapi.ias.RegisterAccountRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RegisterAccountResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ias.RegisterAccountResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RegisterAccountResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("RegisterDevice");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RegisterDevice"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RegisterDeviceRequestType"), com.broadon.wsapi.ias.RegisterDeviceRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RegisterDeviceResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ias.RegisterDeviceResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RegisterDeviceResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("RemoveDeviceAccount");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RemoveDeviceAccount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RemoveDeviceAccountRequestType"), com.broadon.wsapi.ias.RemoveDeviceAccountRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RemoveDeviceAccountResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ias.RemoveDeviceAccountResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RemoveDeviceAccountResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("RenewAccountToken");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RenewAccountToken"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RenewAccountTokenRequestType"), com.broadon.wsapi.ias.RenewAccountTokenRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RenewAccountTokenResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ias.RenewAccountTokenResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RenewAccountTokenResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[12] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("RenewDeviceToken");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RenewDeviceToken"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RenewDeviceTokenRequestType"), com.broadon.wsapi.ias.RenewDeviceTokenRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RenewDeviceTokenResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ias.RenewDeviceTokenResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RenewDeviceTokenResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[13] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ResetAccountPassword");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ResetAccountPassword"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ResetAccountPasswordRequestType"), com.broadon.wsapi.ias.ResetAccountPasswordRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ResetAccountPasswordResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ias.ResetAccountPasswordResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ResetAccountPasswordResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[14] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateAccount");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateAccount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateAccountRequestType"), com.broadon.wsapi.ias.UpdateAccountRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateAccountResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ias.UpdateAccountResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateAccountResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[15] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateAccountAttributes");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateAccountAttributes"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateAccountAttributesRequestType"), com.broadon.wsapi.ias.UpdateAccountAttributesRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateAccountAttributesResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ias.UpdateAccountAttributesResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateAccountAttributesResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[16] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateAccountPassword");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateAccountPassword"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateAccountPasswordRequestType"), com.broadon.wsapi.ias.UpdateAccountPasswordRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateAccountPasswordResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ias.UpdateAccountPasswordResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateAccountPasswordResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[17] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateDeviceSubscription");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateDeviceSubscription"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateDeviceSubscriptionRequestType"), com.broadon.wsapi.ias.UpdateDeviceSubscriptionRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateDeviceSubscriptionResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ias.UpdateDeviceSubscriptionResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateDeviceSubscriptionResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[18] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ValidateAccountToken");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ValidateAccountToken"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ValidateAccountTokenRequestType"), com.broadon.wsapi.ias.ValidateAccountTokenRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ValidateAccountTokenResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ias.ValidateAccountTokenResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ValidateAccountTokenResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[19] = oper;

    }

    private static void _initOperationDesc3(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ValidateDeviceToken");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ValidateDeviceToken"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ValidateDeviceTokenRequestType"), com.broadon.wsapi.ias.ValidateDeviceTokenRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ValidateDeviceTokenResponseType"));
        oper.setReturnClass(com.broadon.wsapi.ias.ValidateDeviceTokenResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ValidateDeviceTokenResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[20] = oper;

    }

    public IdentityAuthenticationSOAPBindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public IdentityAuthenticationSOAPBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public IdentityAuthenticationSOAPBindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AbstractAccountRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.AbstractAccountRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AbstractAccountResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.AbstractAccountResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AbstractDeviceRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.AbstractDeviceRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AbstractDeviceResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.AbstractDeviceResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AbstractRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.AbstractRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AbstractResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.AbstractResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AccountElementTagsType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.AccountElementTagsType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AccountIdType");
            cachedSerQNames.add(qName);
            cls = int.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(simplesf);
            cachedDeserFactories.add(simpledf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AccountInfoType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.AccountInfoType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AttributeIdType");
            cachedSerQNames.add(qName);
            cls = int.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(simplesf);
            cachedDeserFactories.add(simpledf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AttributeType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.AttributeType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AuthenticateAccountRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.AuthenticateAccountRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AuthenticateAccountResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.AuthenticateAccountResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AuthenticateDeviceRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.AuthenticateDeviceRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "AuthenticateDeviceResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.AuthenticateDeviceResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "DeviceAccountType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.DeviceAccountType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "DeviceIdType");
            cachedSerQNames.add(qName);
            cls = long.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(simplesf);
            cachedDeserFactories.add(simpledf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "DeviceInfoType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.DeviceInfoType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "DeviceSubscriptionType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.DeviceSubscriptionType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ExportAccountRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.ExportAccountRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ExportAccountResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.ExportAccountResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ExternalAccountType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.ExternalAccountType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetAccountAttributesRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.GetAccountAttributesRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetAccountAttributesResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.GetAccountAttributesResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetAccountInfoRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.GetAccountInfoRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetAccountInfoResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.GetAccountInfoResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetDeviceAccountsRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.GetDeviceAccountsRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetDeviceAccountsResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.GetDeviceAccountsResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetDeviceInfoRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.GetDeviceInfoRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetDeviceInfoResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.GetDeviceInfoResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetDeviceSubscriptionsRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.GetDeviceSubscriptionsRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetDeviceSubscriptionsResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.GetDeviceSubscriptionsResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "LinkDeviceAccountRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.LinkDeviceAccountRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "LinkDeviceAccountResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.LinkDeviceAccountResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "LocaleType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.LocaleType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "LoginNameType");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(simplesf);
            cachedDeserFactories.add(simpledf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "PasswordType");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(simplesf);
            cachedDeserFactories.add(simpledf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RegisterAccountRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.RegisterAccountRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RegisterAccountResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.RegisterAccountResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RegisterDeviceRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.RegisterDeviceRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RegisterDeviceResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.RegisterDeviceResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RemoveDeviceAccountRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.RemoveDeviceAccountRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RemoveDeviceAccountResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.RemoveDeviceAccountResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RenewAccountTokenRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.RenewAccountTokenRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RenewAccountTokenResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.RenewAccountTokenResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RenewDeviceTokenRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.RenewDeviceTokenRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "RenewDeviceTokenResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.RenewDeviceTokenResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ResetAccountPasswordRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.ResetAccountPasswordRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ResetAccountPasswordResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.ResetAccountPasswordResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "SubscriptionType");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(simplesf);
            cachedDeserFactories.add(simpledf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "TimeStampType");
            cachedSerQNames.add(qName);
            cls = long.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(simplesf);
            cachedDeserFactories.add(simpledf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "TokenType");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(simplesf);
            cachedDeserFactories.add(simpledf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateAccountAttributesRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.UpdateAccountAttributesRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateAccountAttributesResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.UpdateAccountAttributesResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateAccountPasswordRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.UpdateAccountPasswordRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateAccountPasswordResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.UpdateAccountPasswordResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateAccountRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.UpdateAccountRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateAccountResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.UpdateAccountResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateDeviceSubscriptionRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.UpdateDeviceSubscriptionRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "UpdateDeviceSubscriptionResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.UpdateDeviceSubscriptionResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ValidateAccountTokenRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.ValidateAccountTokenRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ValidateAccountTokenResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.ValidateAccountTokenResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ValidateDeviceTokenRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.ValidateDeviceTokenRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ValidateDeviceTokenResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.ias.ValidateDeviceTokenResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.broadon.wsapi.ias.AuthenticateAccountResponseType authenticateAccount(com.broadon.wsapi.ias.AuthenticateAccountRequestType authenticateAccountRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ias.wsapi.broadon.com/AuthenticateAccount");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "AuthenticateAccount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {authenticateAccountRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ias.AuthenticateAccountResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ias.AuthenticateAccountResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ias.AuthenticateAccountResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ias.AuthenticateDeviceResponseType authenticateDevice(com.broadon.wsapi.ias.AuthenticateDeviceRequestType authenticateDeviceRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ias.wsapi.broadon.com/AuthenticateDevice");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "AuthenticateDevice"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {authenticateDeviceRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ias.AuthenticateDeviceResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ias.AuthenticateDeviceResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ias.AuthenticateDeviceResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ias.ExportAccountResponseType exportAccount(com.broadon.wsapi.ias.ExportAccountRequestType exportAccountRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ias.wsapi.broadon.com/ExportAccount");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "ExportAccount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {exportAccountRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ias.ExportAccountResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ias.ExportAccountResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ias.ExportAccountResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ias.GetAccountAttributesResponseType getAccountAttributes(com.broadon.wsapi.ias.GetAccountAttributesRequestType getAccountAttributesRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ias.wsapi.broadon.com/GetAccountAttributes");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetAccountAttributes"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getAccountAttributesRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ias.GetAccountAttributesResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ias.GetAccountAttributesResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ias.GetAccountAttributesResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ias.GetAccountInfoResponseType getAccountInfo(com.broadon.wsapi.ias.GetAccountInfoRequestType getAccountInfoRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ias.wsapi.broadon.com/GetAccountInfo");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetAccountInfo"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getAccountInfoRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ias.GetAccountInfoResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ias.GetAccountInfoResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ias.GetAccountInfoResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ias.GetDeviceAccountsResponseType getDeviceAccounts(com.broadon.wsapi.ias.GetDeviceAccountsRequestType getDeviceAccountsRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ias.wsapi.broadon.com/GetDeviceAccounts");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetDeviceAccounts"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getDeviceAccountsRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ias.GetDeviceAccountsResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ias.GetDeviceAccountsResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ias.GetDeviceAccountsResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ias.GetDeviceInfoResponseType getDeviceInfo(com.broadon.wsapi.ias.GetDeviceInfoRequestType getDeviceInfoRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ias.wsapi.broadon.com/GetDeviceInfo");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetDeviceInfo"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getDeviceInfoRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ias.GetDeviceInfoResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ias.GetDeviceInfoResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ias.GetDeviceInfoResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ias.GetDeviceSubscriptionsResponseType getDeviceSubscriptions(com.broadon.wsapi.ias.GetDeviceSubscriptionsRequestType getDeviceSubscriptionsRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ias.wsapi.broadon.com/GetDeviceSubscriptions");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetDeviceSubscriptions"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getDeviceSubscriptionsRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ias.GetDeviceSubscriptionsResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ias.GetDeviceSubscriptionsResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ias.GetDeviceSubscriptionsResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ias.LinkDeviceAccountResponseType linkDeviceAccount(com.broadon.wsapi.ias.LinkDeviceAccountRequestType linkDeviceAccountRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ias.wsapi.broadon.com/LinkDeviceAccount");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "LinkDeviceAccount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {linkDeviceAccountRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ias.LinkDeviceAccountResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ias.LinkDeviceAccountResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ias.LinkDeviceAccountResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ias.RegisterAccountResponseType registerAccount(com.broadon.wsapi.ias.RegisterAccountRequestType registerAccountRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ias.wsapi.broadon.com/RegisterAccount");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "RegisterAccount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {registerAccountRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ias.RegisterAccountResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ias.RegisterAccountResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ias.RegisterAccountResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ias.RegisterDeviceResponseType registerDevice(com.broadon.wsapi.ias.RegisterDeviceRequestType registerDeviceRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ias.wsapi.broadon.com/RegisterDevice");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "RegisterDevice"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {registerDeviceRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ias.RegisterDeviceResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ias.RegisterDeviceResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ias.RegisterDeviceResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ias.RemoveDeviceAccountResponseType removeDeviceAccount(com.broadon.wsapi.ias.RemoveDeviceAccountRequestType removeDeviceAccountRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ias.wsapi.broadon.com/RemoveDeviceAccount");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "RemoveDeviceAccount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {removeDeviceAccountRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ias.RemoveDeviceAccountResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ias.RemoveDeviceAccountResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ias.RemoveDeviceAccountResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ias.RenewAccountTokenResponseType renewAccountToken(com.broadon.wsapi.ias.RenewAccountTokenRequestType renewAccountTokenRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ias.wsapi.broadon.com/RenewAccountToken");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "RenewAccountToken"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {renewAccountTokenRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ias.RenewAccountTokenResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ias.RenewAccountTokenResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ias.RenewAccountTokenResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ias.RenewDeviceTokenResponseType renewDeviceToken(com.broadon.wsapi.ias.RenewDeviceTokenRequestType renewDeviceTokenRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[13]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ias.wsapi.broadon.com/RenewDeviceToken");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "RenewDeviceToken"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {renewDeviceTokenRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ias.RenewDeviceTokenResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ias.RenewDeviceTokenResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ias.RenewDeviceTokenResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ias.ResetAccountPasswordResponseType resetAccountPassword(com.broadon.wsapi.ias.ResetAccountPasswordRequestType resetAccountPasswordRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[14]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ias.wsapi.broadon.com/ResetAccountPassword");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "ResetAccountPassword"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {resetAccountPasswordRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ias.ResetAccountPasswordResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ias.ResetAccountPasswordResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ias.ResetAccountPasswordResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ias.UpdateAccountResponseType updateAccount(com.broadon.wsapi.ias.UpdateAccountRequestType updateAccountRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[15]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ias.wsapi.broadon.com/UpdateAccount");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "UpdateAccount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {updateAccountRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ias.UpdateAccountResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ias.UpdateAccountResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ias.UpdateAccountResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ias.UpdateAccountAttributesResponseType updateAccountAttributes(com.broadon.wsapi.ias.UpdateAccountAttributesRequestType updateAccountAttributesRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[16]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ias.wsapi.broadon.com/UpdateAccountAttributes");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "UpdateAccountAttributes"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {updateAccountAttributesRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ias.UpdateAccountAttributesResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ias.UpdateAccountAttributesResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ias.UpdateAccountAttributesResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ias.UpdateAccountPasswordResponseType updateAccountPassword(com.broadon.wsapi.ias.UpdateAccountPasswordRequestType updateAccountPasswordRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[17]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ias.wsapi.broadon.com/UpdateAccountPassword");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "UpdateAccountPassword"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {updateAccountPasswordRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ias.UpdateAccountPasswordResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ias.UpdateAccountPasswordResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ias.UpdateAccountPasswordResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ias.UpdateDeviceSubscriptionResponseType updateDeviceSubscription(com.broadon.wsapi.ias.UpdateDeviceSubscriptionRequestType updateDeviceSubscriptionRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[18]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ias.wsapi.broadon.com/UpdateDeviceSubscription");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "UpdateDeviceSubscription"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {updateDeviceSubscriptionRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ias.UpdateDeviceSubscriptionResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ias.UpdateDeviceSubscriptionResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ias.UpdateDeviceSubscriptionResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ias.ValidateAccountTokenResponseType validateAccountToken(com.broadon.wsapi.ias.ValidateAccountTokenRequestType validateAccountTokenRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[19]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ias.wsapi.broadon.com/ValidateAccountToken");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "ValidateAccountToken"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {validateAccountTokenRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ias.ValidateAccountTokenResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ias.ValidateAccountTokenResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ias.ValidateAccountTokenResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.ias.ValidateDeviceTokenResponseType validateDeviceToken(com.broadon.wsapi.ias.ValidateDeviceTokenRequestType validateDeviceTokenRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[20]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:ias.wsapi.broadon.com/ValidateDeviceToken");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "ValidateDeviceToken"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {validateDeviceTokenRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.ias.ValidateDeviceTokenResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.ias.ValidateDeviceTokenResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.ias.ValidateDeviceTokenResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}

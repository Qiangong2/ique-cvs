/**
 * CheckECardBalanceResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.pas;

public class CheckECardBalanceResponseType  extends com.broadon.wsapi.pas.AbstractResponseType
        implements java.io.Serializable {
    private com.broadon.wsapi.pas.ECardInfoType ECardInfo;

    public CheckECardBalanceResponseType() {
    }

    public CheckECardBalanceResponseType(
           com.broadon.wsapi.pas.ECardInfoType ECardInfo) {
           this.ECardInfo = ECardInfo;
    }


    /**
     * Gets the ECardInfo value for this CheckECardBalanceResponseType.
     * 
     * @return ECardInfo
     */
    public com.broadon.wsapi.pas.ECardInfoType getECardInfo() {
        return ECardInfo;
    }


    /**
     * Sets the ECardInfo value for this CheckECardBalanceResponseType.
     * 
     * @param ECardInfo
     */
    public void setECardInfo(com.broadon.wsapi.pas.ECardInfoType ECardInfo) {
        this.ECardInfo = ECardInfo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CheckECardBalanceResponseType)) return false;
        CheckECardBalanceResponseType other = (CheckECardBalanceResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.ECardInfo==null && other.getECardInfo()==null) || 
             (this.ECardInfo!=null &&
              this.ECardInfo.equals(other.getECardInfo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getECardInfo() != null) {
            _hashCode += getECardInfo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CheckECardBalanceResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CheckECardBalanceResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ECardInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ECardInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ECardInfoType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

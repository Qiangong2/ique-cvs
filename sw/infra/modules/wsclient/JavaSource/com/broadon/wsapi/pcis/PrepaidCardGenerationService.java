/**
 * PrepaidCardGenerationService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.pcis;

public interface PrepaidCardGenerationService extends javax.xml.rpc.Service {
    public java.lang.String getPrepaidCardGenerationSOAPAddress();

    public com.broadon.wsapi.pcis.PrepaidCardGenerationPortType getPrepaidCardGenerationSOAP() throws javax.xml.rpc.ServiceException;

    public com.broadon.wsapi.pcis.PrepaidCardGenerationPortType getPrepaidCardGenerationSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}

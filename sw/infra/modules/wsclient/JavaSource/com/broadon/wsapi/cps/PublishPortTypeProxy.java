package com.broadon.wsapi.cps;

public class PublishPortTypeProxy implements com.broadon.wsapi.cps.PublishPortType {
  private String _endpoint = null;
  private com.broadon.wsapi.cps.PublishPortType publishPortType = null;
  
  public PublishPortTypeProxy() {
    _initPublishPortTypeProxy();
  }
  
  private void _initPublishPortTypeProxy() {
    try {
      publishPortType = (new com.broadon.wsapi.cps.PublishServiceLocator()).getPublishSOAP();
      if (publishPortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)publishPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)publishPortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (publishPortType != null)
      ((javax.xml.rpc.Stub)publishPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.broadon.wsapi.cps.PublishPortType getPublishPortType() {
    if (publishPortType == null)
      _initPublishPortTypeProxy();
    return publishPortType;
  }
  
  public com.broadon.wsapi.cps.PublishResponseType publish(com.broadon.wsapi.cps.PublishRequestType publishRequest, javax.activation.DataHandler[] attachedContent) throws java.rmi.RemoteException{
    if (publishPortType == null)
      _initPublishPortTypeProxy();
    return publishPortType.publish(publishRequest, attachedContent);
  }
  
  public com.broadon.wsapi.cps.UploadResponseType upload(com.broadon.wsapi.cps.UploadRequestType uploadRequest, javax.activation.DataHandler[] attachedContent) throws java.rmi.RemoteException{
    if (publishPortType == null)
      _initPublishPortTypeProxy();
    return publishPortType.upload(uploadRequest, attachedContent);
  }
  
  
}

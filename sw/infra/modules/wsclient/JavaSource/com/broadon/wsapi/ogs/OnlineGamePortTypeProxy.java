package com.broadon.wsapi.ogs;

public class OnlineGamePortTypeProxy implements com.broadon.wsapi.ogs.OnlineGamePortType {
  private String _endpoint = null;
  private com.broadon.wsapi.ogs.OnlineGamePortType onlineGamePortType = null;
  
  public OnlineGamePortTypeProxy() {
    _initOnlineGamePortTypeProxy();
  }
  
  private void _initOnlineGamePortTypeProxy() {
    try {
      onlineGamePortType = (new com.broadon.wsapi.ogs.OnlineGameServiceLocator()).getOnlineGameSOAP();
      if (onlineGamePortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)onlineGamePortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)onlineGamePortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (onlineGamePortType != null)
      ((javax.xml.rpc.Stub)onlineGamePortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.broadon.wsapi.ogs.OnlineGamePortType getOnlineGamePortType() {
    if (onlineGamePortType == null)
      _initOnlineGamePortTypeProxy();
    return onlineGamePortType;
  }
  
  public com.broadon.wsapi.ogs.ListBuddiesResponseType listBuddies(com.broadon.wsapi.ogs.ListBuddiesRequestType listBuddiesRequest) throws java.rmi.RemoteException{
    if (onlineGamePortType == null)
      _initOnlineGamePortTypeProxy();
    return onlineGamePortType.listBuddies(listBuddiesRequest);
  }
  
  public com.broadon.wsapi.ogs.ChangeBuddyStateResponseType changeBuddyState(com.broadon.wsapi.ogs.ChangeBuddyStateRequestType changeBuddyStateRequest) throws java.rmi.RemoteException{
    if (onlineGamePortType == null)
      _initOnlineGamePortTypeProxy();
    return onlineGamePortType.changeBuddyState(changeBuddyStateRequest);
  }
  
  public com.broadon.wsapi.ogs.InviteBuddyResponseType inviteBuddy(com.broadon.wsapi.ogs.InviteBuddyRequestType inviteBuddyRequest) throws java.rmi.RemoteException{
    if (onlineGamePortType == null)
      _initOnlineGamePortTypeProxy();
    return onlineGamePortType.inviteBuddy(inviteBuddyRequest);
  }
  
  public com.broadon.wsapi.ogs.DeleteGameScoreResponseType deleteGameScore(com.broadon.wsapi.ogs.DeleteGameScoreRequestType deleteGameScoreRequest) throws java.rmi.RemoteException{
    if (onlineGamePortType == null)
      _initOnlineGamePortTypeProxy();
    return onlineGamePortType.deleteGameScore(deleteGameScoreRequest);
  }
  
  public com.broadon.wsapi.ogs.DeleteMessagesResponseType deleteMessages(com.broadon.wsapi.ogs.DeleteMessagesRequestType deleteMessagesRequest) throws java.rmi.RemoteException{
    if (onlineGamePortType == null)
      _initOnlineGamePortTypeProxy();
    return onlineGamePortType.deleteMessages(deleteMessagesRequest);
  }
  
  public com.broadon.wsapi.ogs.GetGameScoreKeysResponseType getGameScoreKeys(com.broadon.wsapi.ogs.GetGameScoreKeysRequestType getGameScoreKeysRequest) throws java.rmi.RemoteException{
    if (onlineGamePortType == null)
      _initOnlineGamePortTypeProxy();
    return onlineGamePortType.getGameScoreKeys(getGameScoreKeysRequest);
  }
  
  public com.broadon.wsapi.ogs.GetGameScoreObjectResponseType getGameScoreObject(com.broadon.wsapi.ogs.GetGameScoreObjectRequestType getGameScoreObjectRequest) throws java.rmi.RemoteException{
    if (onlineGamePortType == null)
      _initOnlineGamePortTypeProxy();
    return onlineGamePortType.getGameScoreObject(getGameScoreObjectRequest);
  }
  
  public com.broadon.wsapi.ogs.GetGameScoreResponseType getGameScore(com.broadon.wsapi.ogs.GetGameScoreRequestType getGameScoreRequest) throws java.rmi.RemoteException{
    if (onlineGamePortType == null)
      _initOnlineGamePortTypeProxy();
    return onlineGamePortType.getGameScore(getGameScoreRequest);
  }
  
  public com.broadon.wsapi.ogs.GetMessageListResponseType getMessageList(com.broadon.wsapi.ogs.GetMessageListRequestType getMessageListRequest) throws java.rmi.RemoteException{
    if (onlineGamePortType == null)
      _initOnlineGamePortTypeProxy();
    return onlineGamePortType.getMessageList(getMessageListRequest);
  }
  
  public com.broadon.wsapi.ogs.GetRankedGameScoreResponseType getRankedGameScore(com.broadon.wsapi.ogs.GetRankedGameScoreRequestType getRankedGameScoreRequest) throws java.rmi.RemoteException{
    if (onlineGamePortType == null)
      _initOnlineGamePortTypeProxy();
    return onlineGamePortType.getRankedGameScore(getRankedGameScoreRequest);
  }
  
  public com.broadon.wsapi.ogs.QueryGameSessionsResponseType queryGameSessions(com.broadon.wsapi.ogs.QueryGameSessionsRequestType queryGameSessionsRequest) throws java.rmi.RemoteException{
    if (onlineGamePortType == null)
      _initOnlineGamePortTypeProxy();
    return onlineGamePortType.queryGameSessions(queryGameSessionsRequest);
  }
  
  public com.broadon.wsapi.ogs.RegisterGameSessionResponseType registerGameSession(com.broadon.wsapi.ogs.RegisterGameSessionRequestType registerGameSessionRequest) throws java.rmi.RemoteException{
    if (onlineGamePortType == null)
      _initOnlineGamePortTypeProxy();
    return onlineGamePortType.registerGameSession(registerGameSessionRequest);
  }
  
  public com.broadon.wsapi.ogs.RetrieveMessageResponseType retrieveMessage(com.broadon.wsapi.ogs.RetrieveMessageRequestType retrieveMessageRequest) throws java.rmi.RemoteException{
    if (onlineGamePortType == null)
      _initOnlineGamePortTypeProxy();
    return onlineGamePortType.retrieveMessage(retrieveMessageRequest);
  }
  
  public com.broadon.wsapi.ogs.SearchGameSessionsResponseType searchGameSessions(com.broadon.wsapi.ogs.SearchGameSessionsRequestType searchGameSessionsRequest) throws java.rmi.RemoteException{
    if (onlineGamePortType == null)
      _initOnlineGamePortTypeProxy();
    return onlineGamePortType.searchGameSessions(searchGameSessionsRequest);
  }
  
  public com.broadon.wsapi.ogs.StoreMessageResponseType storeMessage(com.broadon.wsapi.ogs.StoreMessageRequestType storeMessageRequest) throws java.rmi.RemoteException{
    if (onlineGamePortType == null)
      _initOnlineGamePortTypeProxy();
    return onlineGamePortType.storeMessage(storeMessageRequest);
  }
  
  public com.broadon.wsapi.ogs.SubmitGameScoresResponseType submitGameScores(com.broadon.wsapi.ogs.SubmitGameScoresRequestType submitGameScoresRequest) throws java.rmi.RemoteException{
    if (onlineGamePortType == null)
      _initOnlineGamePortTypeProxy();
    return onlineGamePortType.submitGameScores(submitGameScoresRequest);
  }
  
  public com.broadon.wsapi.ogs.SubmitGameScoreObjectResponseType submitGameScoreObject(com.broadon.wsapi.ogs.SubmitGameScoreObjectRequestType submitGameScoreObjectRequest) throws java.rmi.RemoteException{
    if (onlineGamePortType == null)
      _initOnlineGamePortTypeProxy();
    return onlineGamePortType.submitGameScoreObject(submitGameScoreObjectRequest);
  }
  
  public com.broadon.wsapi.ogs.ExtendGameSessionResponseType extendGameSession(com.broadon.wsapi.ogs.ExtendGameSessionRequestType extendGameSessionRequest) throws java.rmi.RemoteException{
    if (onlineGamePortType == null)
      _initOnlineGamePortTypeProxy();
    return onlineGamePortType.extendGameSession(extendGameSessionRequest);
  }
  
  public com.broadon.wsapi.ogs.UnregisterGameSessionResponseType unregisterGameSession(com.broadon.wsapi.ogs.UnregisterGameSessionRequestType unregisterGameSessionRequest) throws java.rmi.RemoteException{
    if (onlineGamePortType == null)
      _initOnlineGamePortTypeProxy();
    return onlineGamePortType.unregisterGameSession(unregisterGameSessionRequest);
  }
  
  public com.broadon.wsapi.ogs.RegisterResponseType register(com.broadon.wsapi.ogs.RegisterRequestType registerRequest) throws java.rmi.RemoteException{
    if (onlineGamePortType == null)
      _initOnlineGamePortTypeProxy();
    return onlineGamePortType.register(registerRequest);
  }
  
  public com.broadon.wsapi.ogs.LoginResponseType login(com.broadon.wsapi.ogs.LoginRequestType loginRequest) throws java.rmi.RemoteException{
    if (onlineGamePortType == null)
      _initOnlineGamePortTypeProxy();
    return onlineGamePortType.login(loginRequest);
  }
  
  public com.broadon.wsapi.ogs.SetPasswordResponseType setPassword(com.broadon.wsapi.ogs.SetPasswordRequestType setPasswordRequest) throws java.rmi.RemoteException{
    if (onlineGamePortType == null)
      _initOnlineGamePortTypeProxy();
    return onlineGamePortType.setPassword(setPasswordRequest);
  }
  
  public com.broadon.wsapi.ogs.UpdateUserInfoResponseType updateUserInfo(com.broadon.wsapi.ogs.UpdateUserInfoRequestType updateUserInfoRequest) throws java.rmi.RemoteException{
    if (onlineGamePortType == null)
      _initOnlineGamePortTypeProxy();
    return onlineGamePortType.updateUserInfo(updateUserInfoRequest);
  }
  
  public com.broadon.wsapi.ogs.GetUserInfoResponseType getUserInfo(com.broadon.wsapi.ogs.GetUserInfoRequestType getUserInfoRequest) throws java.rmi.RemoteException{
    if (onlineGamePortType == null)
      _initOnlineGamePortTypeProxy();
    return onlineGamePortType.getUserInfo(getUserInfoRequest);
  }
  
  public com.broadon.wsapi.ogs.GetNetIdRangeResponseType getNetIdRange(com.broadon.wsapi.ogs.GetNetIdRangeRequestType getNetIdRangeRequest) throws java.rmi.RemoteException{
    if (onlineGamePortType == null)
      _initOnlineGamePortTypeProxy();
    return onlineGamePortType.getNetIdRange(getNetIdRangeRequest);
  }
  
  public com.broadon.wsapi.ogs.GetAccountIdResponseType getAccountId(com.broadon.wsapi.ogs.GetAccountIdRequestType getAccountIdRequest) throws java.rmi.RemoteException{
    if (onlineGamePortType == null)
      _initOnlineGamePortTypeProxy();
    return onlineGamePortType.getAccountId(getAccountIdRequest);
  }
  
  
}
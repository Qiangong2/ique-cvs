/**
 * IdentityAuthenticationServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ias;

public class IdentityAuthenticationServiceLocator extends org.apache.axis.client.Service implements com.broadon.wsapi.ias.IdentityAuthenticationService {

    public IdentityAuthenticationServiceLocator() {
    }


    public IdentityAuthenticationServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public IdentityAuthenticationServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for IdentityAuthenticationSOAP
    private java.lang.String IdentityAuthenticationSOAP_address = "http://ias:17101/ias/services/IdentityAuthenticationSOAP";

    public java.lang.String getIdentityAuthenticationSOAPAddress() {
        return IdentityAuthenticationSOAP_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String IdentityAuthenticationSOAPWSDDServiceName = "IdentityAuthenticationSOAP";

    public java.lang.String getIdentityAuthenticationSOAPWSDDServiceName() {
        return IdentityAuthenticationSOAPWSDDServiceName;
    }

    public void setIdentityAuthenticationSOAPWSDDServiceName(java.lang.String name) {
        IdentityAuthenticationSOAPWSDDServiceName = name;
    }

    public com.broadon.wsapi.ias.IdentityAuthenticationPortType getIdentityAuthenticationSOAP() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(IdentityAuthenticationSOAP_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getIdentityAuthenticationSOAP(endpoint);
    }

    public com.broadon.wsapi.ias.IdentityAuthenticationPortType getIdentityAuthenticationSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.broadon.wsapi.ias.IdentityAuthenticationSOAPBindingStub _stub = new com.broadon.wsapi.ias.IdentityAuthenticationSOAPBindingStub(portAddress, this);
            _stub.setPortName(getIdentityAuthenticationSOAPWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setIdentityAuthenticationSOAPEndpointAddress(java.lang.String address) {
        IdentityAuthenticationSOAP_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.broadon.wsapi.ias.IdentityAuthenticationPortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.broadon.wsapi.ias.IdentityAuthenticationSOAPBindingStub _stub = new com.broadon.wsapi.ias.IdentityAuthenticationSOAPBindingStub(new java.net.URL(IdentityAuthenticationSOAP_address), this);
                _stub.setPortName(getIdentityAuthenticationSOAPWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("IdentityAuthenticationSOAP".equals(inputPortName)) {
            return getIdentityAuthenticationSOAP();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "IdentityAuthenticationService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "IdentityAuthenticationSOAP"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("IdentityAuthenticationSOAP".equals(portName)) {
            setIdentityAuthenticationSOAPEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}

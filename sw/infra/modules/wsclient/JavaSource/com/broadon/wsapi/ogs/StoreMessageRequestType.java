/**
 * StoreMessageRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ogs;

public class StoreMessageRequestType  extends com.broadon.wsapi.ogs.AbstractRequestType  implements java.io.Serializable {
    private int accountId;
    private java.lang.String[] recipient;
    private java.lang.String subject;
    private int mediaFormat;
    private byte[] mediaObject;
    private java.lang.Integer replyMsgId;

    public StoreMessageRequestType() {
    }

    public StoreMessageRequestType(
           int accountId,
           java.lang.String[] recipient,
           java.lang.String subject,
           int mediaFormat,
           byte[] mediaObject,
           java.lang.Integer replyMsgId) {
           this.accountId = accountId;
           this.recipient = recipient;
           this.subject = subject;
           this.mediaFormat = mediaFormat;
           this.mediaObject = mediaObject;
           this.replyMsgId = replyMsgId;
    }


    /**
     * Gets the accountId value for this StoreMessageRequestType.
     * 
     * @return accountId
     */
    public int getAccountId() {
        return accountId;
    }


    /**
     * Sets the accountId value for this StoreMessageRequestType.
     * 
     * @param accountId
     */
    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }


    /**
     * Gets the recipient value for this StoreMessageRequestType.
     * 
     * @return recipient
     */
    public java.lang.String[] getRecipient() {
        return recipient;
    }


    /**
     * Sets the recipient value for this StoreMessageRequestType.
     * 
     * @param recipient
     */
    public void setRecipient(java.lang.String[] recipient) {
        this.recipient = recipient;
    }

    public java.lang.String getRecipient(int i) {
        return this.recipient[i];
    }

    public void setRecipient(int i, java.lang.String _value) {
        this.recipient[i] = _value;
    }


    /**
     * Gets the subject value for this StoreMessageRequestType.
     * 
     * @return subject
     */
    public java.lang.String getSubject() {
        return subject;
    }


    /**
     * Sets the subject value for this StoreMessageRequestType.
     * 
     * @param subject
     */
    public void setSubject(java.lang.String subject) {
        this.subject = subject;
    }


    /**
     * Gets the mediaFormat value for this StoreMessageRequestType.
     * 
     * @return mediaFormat
     */
    public int getMediaFormat() {
        return mediaFormat;
    }


    /**
     * Sets the mediaFormat value for this StoreMessageRequestType.
     * 
     * @param mediaFormat
     */
    public void setMediaFormat(int mediaFormat) {
        this.mediaFormat = mediaFormat;
    }


    /**
     * Gets the mediaObject value for this StoreMessageRequestType.
     * 
     * @return mediaObject
     */
    public byte[] getMediaObject() {
        return mediaObject;
    }


    /**
     * Sets the mediaObject value for this StoreMessageRequestType.
     * 
     * @param mediaObject
     */
    public void setMediaObject(byte[] mediaObject) {
        this.mediaObject = mediaObject;
    }


    /**
     * Gets the replyMsgId value for this StoreMessageRequestType.
     * 
     * @return replyMsgId
     */
    public java.lang.Integer getReplyMsgId() {
        return replyMsgId;
    }


    /**
     * Sets the replyMsgId value for this StoreMessageRequestType.
     * 
     * @param replyMsgId
     */
    public void setReplyMsgId(java.lang.Integer replyMsgId) {
        this.replyMsgId = replyMsgId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof StoreMessageRequestType)) return false;
        StoreMessageRequestType other = (StoreMessageRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.accountId == other.getAccountId() &&
            ((this.recipient==null && other.getRecipient()==null) || 
             (this.recipient!=null &&
              java.util.Arrays.equals(this.recipient, other.getRecipient()))) &&
            ((this.subject==null && other.getSubject()==null) || 
             (this.subject!=null &&
              this.subject.equals(other.getSubject()))) &&
            this.mediaFormat == other.getMediaFormat() &&
            ((this.mediaObject==null && other.getMediaObject()==null) || 
             (this.mediaObject!=null &&
              java.util.Arrays.equals(this.mediaObject, other.getMediaObject()))) &&
            ((this.replyMsgId==null && other.getReplyMsgId()==null) || 
             (this.replyMsgId!=null &&
              this.replyMsgId.equals(other.getReplyMsgId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += getAccountId();
        if (getRecipient() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRecipient());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRecipient(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSubject() != null) {
            _hashCode += getSubject().hashCode();
        }
        _hashCode += getMediaFormat();
        if (getMediaObject() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getMediaObject());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getMediaObject(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getReplyMsgId() != null) {
            _hashCode += getReplyMsgId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(StoreMessageRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "StoreMessageRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "AccountId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "AccountIdType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recipient");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "Recipient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subject");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "Subject"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mediaFormat");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "MediaFormat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mediaObject");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "MediaObject"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("replyMsgId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ReplyMsgId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

package com.broadon.wsapi.ecs;

public class ECommercePortTypeProxy implements com.broadon.wsapi.ecs.ECommercePortType {
  private String _endpoint = null;
  private com.broadon.wsapi.ecs.ECommercePortType eCommercePortType = null;
  
  public ECommercePortTypeProxy() {
    _initECommercePortTypeProxy();
  }
  
  private void _initECommercePortTypeProxy() {
    try {
      eCommercePortType = (new com.broadon.wsapi.ecs.ECommerceServiceLocator()).getECommerceSOAP();
      if (eCommercePortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)eCommercePortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)eCommercePortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (eCommercePortType != null)
      ((javax.xml.rpc.Stub)eCommercePortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.broadon.wsapi.ecs.ECommercePortType getECommercePortType() {
    if (eCommercePortType == null)
      _initECommercePortTypeProxy();
    return eCommercePortType;
  }
  
  public com.broadon.wsapi.ecs.ListTitlesResponseType listTitles(com.broadon.wsapi.ecs.ListTitlesRequestType listTitlesRequest) throws java.rmi.RemoteException{
    if (eCommercePortType == null)
      _initECommercePortTypeProxy();
    return eCommercePortType.listTitles(listTitlesRequest);
  }
  
  public com.broadon.wsapi.ecs.GetTitleDetailsResponseType getTitleDetails(com.broadon.wsapi.ecs.GetTitleDetailsRequestType getTitleDetailsRequest) throws java.rmi.RemoteException{
    if (eCommercePortType == null)
      _initECommercePortTypeProxy();
    return eCommercePortType.getTitleDetails(getTitleDetailsRequest);
  }
  
  public com.broadon.wsapi.ecs.GetMetaResponseType getMeta(com.broadon.wsapi.ecs.GetMetaRequestType getMetaRequest) throws java.rmi.RemoteException{
    if (eCommercePortType == null)
      _initECommercePortTypeProxy();
    return eCommercePortType.getMeta(getMetaRequest);
  }
  
  public com.broadon.wsapi.ecs.PurchaseTitleResponseType purchaseTitle(com.broadon.wsapi.ecs.PurchaseTitleRequestType purchaseTitleRequest) throws java.rmi.RemoteException{
    if (eCommercePortType == null)
      _initECommercePortTypeProxy();
    return eCommercePortType.purchaseTitle(purchaseTitleRequest);
  }
  
  public com.broadon.wsapi.ecs.SyncETicketsResponseType syncETickets(com.broadon.wsapi.ecs.SyncETicketsRequestType syncETicketsRequest) throws java.rmi.RemoteException{
    if (eCommercePortType == null)
      _initECommercePortTypeProxy();
    return eCommercePortType.syncETickets(syncETicketsRequest);
  }
  
  public com.broadon.wsapi.ecs.SubscribeResponseType subscribe(com.broadon.wsapi.ecs.SubscribeRequestType subscribeRequest) throws java.rmi.RemoteException{
    if (eCommercePortType == null)
      _initECommercePortTypeProxy();
    return eCommercePortType.subscribe(subscribeRequest);
  }
  
  public com.broadon.wsapi.ecs.UpdateStatusResponseType updateStatus(com.broadon.wsapi.ecs.UpdateStatusRequestType updateStatusRequest) throws java.rmi.RemoteException{
    if (eCommercePortType == null)
      _initECommercePortTypeProxy();
    return eCommercePortType.updateStatus(updateStatusRequest);
  }
  
  public com.broadon.wsapi.ecs.ListSubscriptionPricingsResponseType listSubscriptionPricings(com.broadon.wsapi.ecs.ListSubscriptionPricingsRequestType listSubscriptionPricingsRequest) throws java.rmi.RemoteException{
    if (eCommercePortType == null)
      _initECommercePortTypeProxy();
    return eCommercePortType.listSubscriptionPricings(listSubscriptionPricingsRequest);
  }
  
  public com.broadon.wsapi.ecs.RedeemECardResponseType redeemECard(com.broadon.wsapi.ecs.RedeemECardRequestType redeemECardRequest) throws java.rmi.RemoteException{
    if (eCommercePortType == null)
      _initECommercePortTypeProxy();
    return eCommercePortType.redeemECard(redeemECardRequest);
  }
  
  public com.broadon.wsapi.ecs.PurchasePointsResponseType purchasePoints(com.broadon.wsapi.ecs.PurchasePointsRequestType purchasePointsRequest) throws java.rmi.RemoteException{
    if (eCommercePortType == null)
      _initECommercePortTypeProxy();
    return eCommercePortType.purchasePoints(purchasePointsRequest);
  }
  
  public com.broadon.wsapi.ecs.CheckAccountBalanceResponseType checkAccountBalance(com.broadon.wsapi.ecs.CheckAccountBalanceRequestType checkAccountBalanceRequest) throws java.rmi.RemoteException{
    if (eCommercePortType == null)
      _initECommercePortTypeProxy();
    return eCommercePortType.checkAccountBalance(checkAccountBalanceRequest);
  }
  
  public com.broadon.wsapi.ecs.TransferETicketsResponseType transferETickets(com.broadon.wsapi.ecs.TransferETicketsRequestType transferETicketsRequest) throws java.rmi.RemoteException{
    if (eCommercePortType == null)
      _initECommercePortTypeProxy();
    return eCommercePortType.transferETickets(transferETicketsRequest);
  }
  
  public com.broadon.wsapi.ecs.ListTransactionsResponseType listTransactions(com.broadon.wsapi.ecs.ListTransactionsRequestType listTransactionsRequest) throws java.rmi.RemoteException{
    if (eCommercePortType == null)
      _initECommercePortTypeProxy();
    return eCommercePortType.listTransactions(listTransactionsRequest);
  }
  
  public com.broadon.wsapi.ecs.ListETicketsResponseType listETickets(com.broadon.wsapi.ecs.ListETicketsRequestType listETicketsRequest) throws java.rmi.RemoteException{
    if (eCommercePortType == null)
      _initECommercePortTypeProxy();
    return eCommercePortType.listETickets(listETicketsRequest);
  }
  
  public com.broadon.wsapi.ecs.DeleteETicketsResponseType deleteETickets(com.broadon.wsapi.ecs.DeleteETicketsRequestType deleteETicketsRequest) throws java.rmi.RemoteException{
    if (eCommercePortType == null)
      _initECommercePortTypeProxy();
    return eCommercePortType.deleteETickets(deleteETicketsRequest);
  }
  
  public com.broadon.wsapi.ecs.TransferPointsResponseType transferPoints(com.broadon.wsapi.ecs.TransferPointsRequestType transferPointsRequest) throws java.rmi.RemoteException{
    if (eCommercePortType == null)
      _initECommercePortTypeProxy();
    return eCommercePortType.transferPoints(transferPointsRequest);
  }
  
  public com.broadon.wsapi.ecs.GetSystemUpdateResponseType getSystemUpdate(com.broadon.wsapi.ecs.GetSystemUpdateRequestType getSystemUpdateRequest) throws java.rmi.RemoteException{
    if (eCommercePortType == null)
      _initECommercePortTypeProxy();
    return eCommercePortType.getSystemUpdate(getSystemUpdateRequest);
  }
  
  public com.broadon.wsapi.ecs.GetApplicationUpdateResponseType getApplicationUpdate(com.broadon.wsapi.ecs.GetApplicationUpdateRequestType getApplicationUpdateRequest) throws java.rmi.RemoteException{
    if (eCommercePortType == null)
      _initECommercePortTypeProxy();
    return eCommercePortType.getApplicationUpdate(getApplicationUpdateRequest);
  }
  
  public com.broadon.wsapi.ecs.CheckECardBalanceResponseType checkECardBalance(com.broadon.wsapi.ecs.CheckECardBalanceRequestType checkECardBalanceRequest) throws java.rmi.RemoteException{
    if (eCommercePortType == null)
      _initECommercePortTypeProxy();
    return eCommercePortType.checkECardBalance(checkECardBalanceRequest);
  }
  
  
}
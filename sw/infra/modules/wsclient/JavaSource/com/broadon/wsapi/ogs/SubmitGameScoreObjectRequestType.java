/**
 * SubmitGameScoreObjectRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ogs;

public class SubmitGameScoreObjectRequestType  extends com.broadon.wsapi.ogs.AbstractRequestType  implements java.io.Serializable {
    private com.broadon.wsapi.ogs.ScoreKeyType scoreKey;
    private byte[] object;

    public SubmitGameScoreObjectRequestType() {
    }

    public SubmitGameScoreObjectRequestType(
           com.broadon.wsapi.ogs.ScoreKeyType scoreKey,
           byte[] object) {
           this.scoreKey = scoreKey;
           this.object = object;
    }


    /**
     * Gets the scoreKey value for this SubmitGameScoreObjectRequestType.
     * 
     * @return scoreKey
     */
    public com.broadon.wsapi.ogs.ScoreKeyType getScoreKey() {
        return scoreKey;
    }


    /**
     * Sets the scoreKey value for this SubmitGameScoreObjectRequestType.
     * 
     * @param scoreKey
     */
    public void setScoreKey(com.broadon.wsapi.ogs.ScoreKeyType scoreKey) {
        this.scoreKey = scoreKey;
    }


    /**
     * Gets the object value for this SubmitGameScoreObjectRequestType.
     * 
     * @return object
     */
    public byte[] getObject() {
        return object;
    }


    /**
     * Sets the object value for this SubmitGameScoreObjectRequestType.
     * 
     * @param object
     */
    public void setObject(byte[] object) {
        this.object = object;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SubmitGameScoreObjectRequestType)) return false;
        SubmitGameScoreObjectRequestType other = (SubmitGameScoreObjectRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.scoreKey==null && other.getScoreKey()==null) || 
             (this.scoreKey!=null &&
              this.scoreKey.equals(other.getScoreKey()))) &&
            ((this.object==null && other.getObject()==null) || 
             (this.object!=null &&
              java.util.Arrays.equals(this.object, other.getObject())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getScoreKey() != null) {
            _hashCode += getScoreKey().hashCode();
        }
        if (getObject() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getObject());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getObject(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SubmitGameScoreObjectRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "SubmitGameScoreObjectRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scoreKey");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ScoreKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "ScoreKeyType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("object");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "Object"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

/**
 * CreditCardKindType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ecs;

public class CreditCardKindType implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected CreditCardKindType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _MasterCard = "MasterCard";
    public static final java.lang.String _Visa = "Visa";
    public static final java.lang.String _Discover = "Discover";
    public static final java.lang.String _Amex = "Amex";
    public static final java.lang.String _DinersClub = "DinersClub";
    public static final java.lang.String _JCB = "JCB";
    public static final CreditCardKindType MasterCard = new CreditCardKindType(_MasterCard);
    public static final CreditCardKindType Visa = new CreditCardKindType(_Visa);
    public static final CreditCardKindType Discover = new CreditCardKindType(_Discover);
    public static final CreditCardKindType Amex = new CreditCardKindType(_Amex);
    public static final CreditCardKindType DinersClub = new CreditCardKindType(_DinersClub);
    public static final CreditCardKindType JCB = new CreditCardKindType(_JCB);
    public java.lang.String getValue() { return _value_;}
    public static CreditCardKindType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        CreditCardKindType enumeration = (CreditCardKindType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static CreditCardKindType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CreditCardKindType.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "CreditCardKindType"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}

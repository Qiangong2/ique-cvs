/**
 * GetSystemUpdateRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.nus;

public class GetSystemUpdateRequestType  extends com.broadon.wsapi.nus.AbstractRequestType  implements java.io.Serializable {
    /** List of current system titles and their versions on the device. */
    private com.broadon.wsapi.nus.TitleVersionType[] titleVersion;
    /** Optional. User defined attribute. One bit can be used to determine
 * if this is a forced update (i.e.
 *                     triggered by user) or not. */
    private java.lang.Integer attribute;
    /** Optional. */
    private java.lang.String auditData;

    public GetSystemUpdateRequestType() {
    }

    public GetSystemUpdateRequestType(
           com.broadon.wsapi.nus.TitleVersionType[] titleVersion,
           java.lang.Integer attribute,
           java.lang.String auditData) {
           this.titleVersion = titleVersion;
           this.attribute = attribute;
           this.auditData = auditData;
    }


    /**
     * Gets the titleVersion value for this GetSystemUpdateRequestType.
     * 
     * @return titleVersion List of current system titles and their versions on the device.
     */
    public com.broadon.wsapi.nus.TitleVersionType[] getTitleVersion() {
        return titleVersion;
    }


    /**
     * Sets the titleVersion value for this GetSystemUpdateRequestType.
     * 
     * @param titleVersion List of current system titles and their versions on the device.
     */
    public void setTitleVersion(com.broadon.wsapi.nus.TitleVersionType[] titleVersion) {
        this.titleVersion = titleVersion;
    }

    public com.broadon.wsapi.nus.TitleVersionType getTitleVersion(int i) {
        return this.titleVersion[i];
    }

    public void setTitleVersion(int i, com.broadon.wsapi.nus.TitleVersionType _value) {
        this.titleVersion[i] = _value;
    }


    /**
     * Gets the attribute value for this GetSystemUpdateRequestType.
     * 
     * @return attribute Optional. User defined attribute. One bit can be used to determine
 * if this is a forced update (i.e.
 *                     triggered by user) or not.
     */
    public java.lang.Integer getAttribute() {
        return attribute;
    }


    /**
     * Sets the attribute value for this GetSystemUpdateRequestType.
     * 
     * @param attribute Optional. User defined attribute. One bit can be used to determine
 * if this is a forced update (i.e.
 *                     triggered by user) or not.
     */
    public void setAttribute(java.lang.Integer attribute) {
        this.attribute = attribute;
    }


    /**
     * Gets the auditData value for this GetSystemUpdateRequestType.
     * 
     * @return auditData Optional.
     */
    public java.lang.String getAuditData() {
        return auditData;
    }


    /**
     * Sets the auditData value for this GetSystemUpdateRequestType.
     * 
     * @param auditData Optional.
     */
    public void setAuditData(java.lang.String auditData) {
        this.auditData = auditData;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetSystemUpdateRequestType)) return false;
        GetSystemUpdateRequestType other = (GetSystemUpdateRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.titleVersion==null && other.getTitleVersion()==null) || 
             (this.titleVersion!=null &&
              java.util.Arrays.equals(this.titleVersion, other.getTitleVersion()))) &&
            ((this.attribute==null && other.getAttribute()==null) || 
             (this.attribute!=null &&
              this.attribute.equals(other.getAttribute()))) &&
            ((this.auditData==null && other.getAuditData()==null) || 
             (this.auditData!=null &&
              this.auditData.equals(other.getAuditData())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getTitleVersion() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTitleVersion());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTitleVersion(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getAttribute() != null) {
            _hashCode += getAttribute().hashCode();
        }
        if (getAuditData() != null) {
            _hashCode += getAuditData().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetSystemUpdateRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:nus.wsapi.broadon.com", "GetSystemUpdateRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titleVersion");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:nus.wsapi.broadon.com", "TitleVersion"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:nus.wsapi.broadon.com", "TitleVersionType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attribute");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:nus.wsapi.broadon.com", "Attribute"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("auditData");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:nus.wsapi.broadon.com", "AuditData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

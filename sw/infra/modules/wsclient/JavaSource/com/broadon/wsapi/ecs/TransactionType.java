/**
 * TransactionType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ecs;


/**
 * ECS transaction information
 */
public class TransactionType  implements java.io.Serializable {
    private long transactionId;
    private long date;
    private java.lang.String type;
    private java.lang.String accountId;
    private long deviceId;
    private java.lang.String status;
    private long statusDate;
    private java.lang.String totalAmount;
    private java.lang.String totalTaxes;
    private java.lang.String totalPaid;
    private java.lang.String currency;
    private com.broadon.wsapi.ecs.DebitCreditType debitCredit;
    private java.lang.String balance;
    private long origTransId;
    private long operatorId;
    private long countryId;
    private int itemId;
    private java.lang.String productCode;
    private com.broadon.wsapi.ecs.TitleType title;
    private com.broadon.wsapi.ecs.PricingType titlePricing;
    private com.broadon.wsapi.ecs.RatingType rating;
    private java.lang.String qty;
    private java.lang.String totalDiscount;
    private java.lang.String paymentType;
    private java.lang.String paymentMethodId;
    private java.lang.String pasAuthId;
    private java.lang.String paymentEcardType;

    public TransactionType() {
    }

    public TransactionType(
           long transactionId,
           long date,
           java.lang.String type,
           java.lang.String accountId,
           long deviceId,
           java.lang.String status,
           long statusDate,
           java.lang.String totalAmount,
           java.lang.String totalTaxes,
           java.lang.String totalPaid,
           java.lang.String currency,
           com.broadon.wsapi.ecs.DebitCreditType debitCredit,
           java.lang.String balance,
           long origTransId,
           long operatorId,
           long countryId,
           int itemId,
           java.lang.String productCode,
           com.broadon.wsapi.ecs.TitleType title,
           com.broadon.wsapi.ecs.PricingType titlePricing,
           com.broadon.wsapi.ecs.RatingType rating,
           java.lang.String qty,
           java.lang.String totalDiscount,
           java.lang.String paymentType,
           java.lang.String paymentMethodId,
           java.lang.String pasAuthId,
           java.lang.String paymentEcardType) {
           this.transactionId = transactionId;
           this.date = date;
           this.type = type;
           this.accountId = accountId;
           this.deviceId = deviceId;
           this.status = status;
           this.statusDate = statusDate;
           this.totalAmount = totalAmount;
           this.totalTaxes = totalTaxes;
           this.totalPaid = totalPaid;
           this.currency = currency;
           this.debitCredit = debitCredit;
           this.balance = balance;
           this.origTransId = origTransId;
           this.operatorId = operatorId;
           this.countryId = countryId;
           this.itemId = itemId;
           this.productCode = productCode;
           this.title = title;
           this.titlePricing = titlePricing;
           this.rating = rating;
           this.qty = qty;
           this.totalDiscount = totalDiscount;
           this.paymentType = paymentType;
           this.paymentMethodId = paymentMethodId;
           this.pasAuthId = pasAuthId;
           this.paymentEcardType = paymentEcardType;
    }


    /**
     * Gets the transactionId value for this TransactionType.
     * 
     * @return transactionId
     */
    public long getTransactionId() {
        return transactionId;
    }


    /**
     * Sets the transactionId value for this TransactionType.
     * 
     * @param transactionId
     */
    public void setTransactionId(long transactionId) {
        this.transactionId = transactionId;
    }


    /**
     * Gets the date value for this TransactionType.
     * 
     * @return date
     */
    public long getDate() {
        return date;
    }


    /**
     * Sets the date value for this TransactionType.
     * 
     * @param date
     */
    public void setDate(long date) {
        this.date = date;
    }


    /**
     * Gets the type value for this TransactionType.
     * 
     * @return type
     */
    public java.lang.String getType() {
        return type;
    }


    /**
     * Sets the type value for this TransactionType.
     * 
     * @param type
     */
    public void setType(java.lang.String type) {
        this.type = type;
    }


    /**
     * Gets the accountId value for this TransactionType.
     * 
     * @return accountId
     */
    public java.lang.String getAccountId() {
        return accountId;
    }


    /**
     * Sets the accountId value for this TransactionType.
     * 
     * @param accountId
     */
    public void setAccountId(java.lang.String accountId) {
        this.accountId = accountId;
    }


    /**
     * Gets the deviceId value for this TransactionType.
     * 
     * @return deviceId
     */
    public long getDeviceId() {
        return deviceId;
    }


    /**
     * Sets the deviceId value for this TransactionType.
     * 
     * @param deviceId
     */
    public void setDeviceId(long deviceId) {
        this.deviceId = deviceId;
    }


    /**
     * Gets the status value for this TransactionType.
     * 
     * @return status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this TransactionType.
     * 
     * @param status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }


    /**
     * Gets the statusDate value for this TransactionType.
     * 
     * @return statusDate
     */
    public long getStatusDate() {
        return statusDate;
    }


    /**
     * Sets the statusDate value for this TransactionType.
     * 
     * @param statusDate
     */
    public void setStatusDate(long statusDate) {
        this.statusDate = statusDate;
    }


    /**
     * Gets the totalAmount value for this TransactionType.
     * 
     * @return totalAmount
     */
    public java.lang.String getTotalAmount() {
        return totalAmount;
    }


    /**
     * Sets the totalAmount value for this TransactionType.
     * 
     * @param totalAmount
     */
    public void setTotalAmount(java.lang.String totalAmount) {
        this.totalAmount = totalAmount;
    }


    /**
     * Gets the totalTaxes value for this TransactionType.
     * 
     * @return totalTaxes
     */
    public java.lang.String getTotalTaxes() {
        return totalTaxes;
    }


    /**
     * Sets the totalTaxes value for this TransactionType.
     * 
     * @param totalTaxes
     */
    public void setTotalTaxes(java.lang.String totalTaxes) {
        this.totalTaxes = totalTaxes;
    }


    /**
     * Gets the totalPaid value for this TransactionType.
     * 
     * @return totalPaid
     */
    public java.lang.String getTotalPaid() {
        return totalPaid;
    }


    /**
     * Sets the totalPaid value for this TransactionType.
     * 
     * @param totalPaid
     */
    public void setTotalPaid(java.lang.String totalPaid) {
        this.totalPaid = totalPaid;
    }


    /**
     * Gets the currency value for this TransactionType.
     * 
     * @return currency
     */
    public java.lang.String getCurrency() {
        return currency;
    }


    /**
     * Sets the currency value for this TransactionType.
     * 
     * @param currency
     */
    public void setCurrency(java.lang.String currency) {
        this.currency = currency;
    }


    /**
     * Gets the debitCredit value for this TransactionType.
     * 
     * @return debitCredit
     */
    public com.broadon.wsapi.ecs.DebitCreditType getDebitCredit() {
        return debitCredit;
    }


    /**
     * Sets the debitCredit value for this TransactionType.
     * 
     * @param debitCredit
     */
    public void setDebitCredit(com.broadon.wsapi.ecs.DebitCreditType debitCredit) {
        this.debitCredit = debitCredit;
    }


    /**
     * Gets the balance value for this TransactionType.
     * 
     * @return balance
     */
    public java.lang.String getBalance() {
        return balance;
    }


    /**
     * Sets the balance value for this TransactionType.
     * 
     * @param balance
     */
    public void setBalance(java.lang.String balance) {
        this.balance = balance;
    }


    /**
     * Gets the origTransId value for this TransactionType.
     * 
     * @return origTransId
     */
    public long getOrigTransId() {
        return origTransId;
    }


    /**
     * Sets the origTransId value for this TransactionType.
     * 
     * @param origTransId
     */
    public void setOrigTransId(long origTransId) {
        this.origTransId = origTransId;
    }


    /**
     * Gets the operatorId value for this TransactionType.
     * 
     * @return operatorId
     */
    public long getOperatorId() {
        return operatorId;
    }


    /**
     * Sets the operatorId value for this TransactionType.
     * 
     * @param operatorId
     */
    public void setOperatorId(long operatorId) {
        this.operatorId = operatorId;
    }


    /**
     * Gets the countryId value for this TransactionType.
     * 
     * @return countryId
     */
    public long getCountryId() {
        return countryId;
    }


    /**
     * Sets the countryId value for this TransactionType.
     * 
     * @param countryId
     */
    public void setCountryId(long countryId) {
        this.countryId = countryId;
    }


    /**
     * Gets the itemId value for this TransactionType.
     * 
     * @return itemId
     */
    public int getItemId() {
        return itemId;
    }


    /**
     * Sets the itemId value for this TransactionType.
     * 
     * @param itemId
     */
    public void setItemId(int itemId) {
        this.itemId = itemId;
    }


    /**
     * Gets the productCode value for this TransactionType.
     * 
     * @return productCode
     */
    public java.lang.String getProductCode() {
        return productCode;
    }


    /**
     * Sets the productCode value for this TransactionType.
     * 
     * @param productCode
     */
    public void setProductCode(java.lang.String productCode) {
        this.productCode = productCode;
    }


    /**
     * Gets the title value for this TransactionType.
     * 
     * @return title
     */
    public com.broadon.wsapi.ecs.TitleType getTitle() {
        return title;
    }


    /**
     * Sets the title value for this TransactionType.
     * 
     * @param title
     */
    public void setTitle(com.broadon.wsapi.ecs.TitleType title) {
        this.title = title;
    }


    /**
     * Gets the titlePricing value for this TransactionType.
     * 
     * @return titlePricing
     */
    public com.broadon.wsapi.ecs.PricingType getTitlePricing() {
        return titlePricing;
    }


    /**
     * Sets the titlePricing value for this TransactionType.
     * 
     * @param titlePricing
     */
    public void setTitlePricing(com.broadon.wsapi.ecs.PricingType titlePricing) {
        this.titlePricing = titlePricing;
    }


    /**
     * Gets the rating value for this TransactionType.
     * 
     * @return rating
     */
    public com.broadon.wsapi.ecs.RatingType getRating() {
        return rating;
    }


    /**
     * Sets the rating value for this TransactionType.
     * 
     * @param rating
     */
    public void setRating(com.broadon.wsapi.ecs.RatingType rating) {
        this.rating = rating;
    }


    /**
     * Gets the qty value for this TransactionType.
     * 
     * @return qty
     */
    public java.lang.String getQty() {
        return qty;
    }


    /**
     * Sets the qty value for this TransactionType.
     * 
     * @param qty
     */
    public void setQty(java.lang.String qty) {
        this.qty = qty;
    }


    /**
     * Gets the totalDiscount value for this TransactionType.
     * 
     * @return totalDiscount
     */
    public java.lang.String getTotalDiscount() {
        return totalDiscount;
    }


    /**
     * Sets the totalDiscount value for this TransactionType.
     * 
     * @param totalDiscount
     */
    public void setTotalDiscount(java.lang.String totalDiscount) {
        this.totalDiscount = totalDiscount;
    }


    /**
     * Gets the paymentType value for this TransactionType.
     * 
     * @return paymentType
     */
    public java.lang.String getPaymentType() {
        return paymentType;
    }


    /**
     * Sets the paymentType value for this TransactionType.
     * 
     * @param paymentType
     */
    public void setPaymentType(java.lang.String paymentType) {
        this.paymentType = paymentType;
    }


    /**
     * Gets the paymentMethodId value for this TransactionType.
     * 
     * @return paymentMethodId
     */
    public java.lang.String getPaymentMethodId() {
        return paymentMethodId;
    }


    /**
     * Sets the paymentMethodId value for this TransactionType.
     * 
     * @param paymentMethodId
     */
    public void setPaymentMethodId(java.lang.String paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }


    /**
     * Gets the pasAuthId value for this TransactionType.
     * 
     * @return pasAuthId
     */
    public java.lang.String getPasAuthId() {
        return pasAuthId;
    }


    /**
     * Sets the pasAuthId value for this TransactionType.
     * 
     * @param pasAuthId
     */
    public void setPasAuthId(java.lang.String pasAuthId) {
        this.pasAuthId = pasAuthId;
    }


    /**
     * Gets the paymentEcardType value for this TransactionType.
     * 
     * @return paymentEcardType
     */
    public java.lang.String getPaymentEcardType() {
        return paymentEcardType;
    }


    /**
     * Sets the paymentEcardType value for this TransactionType.
     * 
     * @param paymentEcardType
     */
    public void setPaymentEcardType(java.lang.String paymentEcardType) {
        this.paymentEcardType = paymentEcardType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TransactionType)) return false;
        TransactionType other = (TransactionType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.transactionId == other.getTransactionId() &&
            this.date == other.getDate() &&
            ((this.type==null && other.getType()==null) || 
             (this.type!=null &&
              this.type.equals(other.getType()))) &&
            ((this.accountId==null && other.getAccountId()==null) || 
             (this.accountId!=null &&
              this.accountId.equals(other.getAccountId()))) &&
            this.deviceId == other.getDeviceId() &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            this.statusDate == other.getStatusDate() &&
            ((this.totalAmount==null && other.getTotalAmount()==null) || 
             (this.totalAmount!=null &&
              this.totalAmount.equals(other.getTotalAmount()))) &&
            ((this.totalTaxes==null && other.getTotalTaxes()==null) || 
             (this.totalTaxes!=null &&
              this.totalTaxes.equals(other.getTotalTaxes()))) &&
            ((this.totalPaid==null && other.getTotalPaid()==null) || 
             (this.totalPaid!=null &&
              this.totalPaid.equals(other.getTotalPaid()))) &&
            ((this.currency==null && other.getCurrency()==null) || 
             (this.currency!=null &&
              this.currency.equals(other.getCurrency()))) &&
            ((this.debitCredit==null && other.getDebitCredit()==null) || 
             (this.debitCredit!=null &&
              this.debitCredit.equals(other.getDebitCredit()))) &&
            ((this.balance==null && other.getBalance()==null) || 
             (this.balance!=null &&
              this.balance.equals(other.getBalance()))) &&
            this.origTransId == other.getOrigTransId() &&
            this.operatorId == other.getOperatorId() &&
            this.countryId == other.getCountryId() &&
            this.itemId == other.getItemId() &&
            ((this.productCode==null && other.getProductCode()==null) || 
             (this.productCode!=null &&
              this.productCode.equals(other.getProductCode()))) &&
            ((this.title==null && other.getTitle()==null) || 
             (this.title!=null &&
              this.title.equals(other.getTitle()))) &&
            ((this.titlePricing==null && other.getTitlePricing()==null) || 
             (this.titlePricing!=null &&
              this.titlePricing.equals(other.getTitlePricing()))) &&
            ((this.rating==null && other.getRating()==null) || 
             (this.rating!=null &&
              this.rating.equals(other.getRating()))) &&
            ((this.qty==null && other.getQty()==null) || 
             (this.qty!=null &&
              this.qty.equals(other.getQty()))) &&
            ((this.totalDiscount==null && other.getTotalDiscount()==null) || 
             (this.totalDiscount!=null &&
              this.totalDiscount.equals(other.getTotalDiscount()))) &&
            ((this.paymentType==null && other.getPaymentType()==null) || 
             (this.paymentType!=null &&
              this.paymentType.equals(other.getPaymentType()))) &&
            ((this.paymentMethodId==null && other.getPaymentMethodId()==null) || 
             (this.paymentMethodId!=null &&
              this.paymentMethodId.equals(other.getPaymentMethodId()))) &&
            ((this.pasAuthId==null && other.getPasAuthId()==null) || 
             (this.pasAuthId!=null &&
              this.pasAuthId.equals(other.getPasAuthId()))) &&
            ((this.paymentEcardType==null && other.getPaymentEcardType()==null) || 
             (this.paymentEcardType!=null &&
              this.paymentEcardType.equals(other.getPaymentEcardType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += new Long(getTransactionId()).hashCode();
        _hashCode += new Long(getDate()).hashCode();
        if (getType() != null) {
            _hashCode += getType().hashCode();
        }
        if (getAccountId() != null) {
            _hashCode += getAccountId().hashCode();
        }
        _hashCode += new Long(getDeviceId()).hashCode();
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        _hashCode += new Long(getStatusDate()).hashCode();
        if (getTotalAmount() != null) {
            _hashCode += getTotalAmount().hashCode();
        }
        if (getTotalTaxes() != null) {
            _hashCode += getTotalTaxes().hashCode();
        }
        if (getTotalPaid() != null) {
            _hashCode += getTotalPaid().hashCode();
        }
        if (getCurrency() != null) {
            _hashCode += getCurrency().hashCode();
        }
        if (getDebitCredit() != null) {
            _hashCode += getDebitCredit().hashCode();
        }
        if (getBalance() != null) {
            _hashCode += getBalance().hashCode();
        }
        _hashCode += new Long(getOrigTransId()).hashCode();
        _hashCode += new Long(getOperatorId()).hashCode();
        _hashCode += new Long(getCountryId()).hashCode();
        _hashCode += getItemId();
        if (getProductCode() != null) {
            _hashCode += getProductCode().hashCode();
        }
        if (getTitle() != null) {
            _hashCode += getTitle().hashCode();
        }
        if (getTitlePricing() != null) {
            _hashCode += getTitlePricing().hashCode();
        }
        if (getRating() != null) {
            _hashCode += getRating().hashCode();
        }
        if (getQty() != null) {
            _hashCode += getQty().hashCode();
        }
        if (getTotalDiscount() != null) {
            _hashCode += getTotalDiscount().hashCode();
        }
        if (getPaymentType() != null) {
            _hashCode += getPaymentType().hashCode();
        }
        if (getPaymentMethodId() != null) {
            _hashCode += getPaymentMethodId().hashCode();
        }
        if (getPasAuthId() != null) {
            _hashCode += getPasAuthId().hashCode();
        }
        if (getPaymentEcardType() != null) {
            _hashCode += getPaymentEcardType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TransactionType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TransactionType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TransactionId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("date");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "Date"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TimeStampType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("type");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "Type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "AccountId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deviceId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "DeviceId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "DeviceIdType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "Status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statusDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "StatusDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TimeStampType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TotalAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalTaxes");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TotalTaxes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalPaid");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TotalPaid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currency");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "Currency"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("debitCredit");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "DebitCredit"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "DebitCreditType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("balance");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "Balance"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("origTransId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "OrigTransId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("operatorId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "OperatorId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("countryId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "CountryId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ItemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ItemIdType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productCode");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ProductCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("title");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "Title"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TitleType"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titlePricing");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TitlePricing"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PricingType"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rating");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "Rating"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "RatingType"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("qty");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "Qty"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalDiscount");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TotalDiscount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentType");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PaymentType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentMethodId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PaymentMethodId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pasAuthId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PasAuthId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentEcardType");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PaymentEcardType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

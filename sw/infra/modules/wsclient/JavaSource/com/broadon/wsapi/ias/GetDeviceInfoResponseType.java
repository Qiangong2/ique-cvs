/**
 * GetDeviceInfoResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ias;

public class GetDeviceInfoResponseType  extends com.broadon.wsapi.ias.AbstractDeviceResponseType  implements java.io.Serializable {
    private com.broadon.wsapi.ias.DeviceInfoType deviceInfo;
    private com.broadon.wsapi.ias.DeviceSubscriptionType[] deviceSubscriptions;

    public GetDeviceInfoResponseType() {
    }

    public GetDeviceInfoResponseType(
           com.broadon.wsapi.ias.DeviceInfoType deviceInfo,
           com.broadon.wsapi.ias.DeviceSubscriptionType[] deviceSubscriptions) {
           this.deviceInfo = deviceInfo;
           this.deviceSubscriptions = deviceSubscriptions;
    }


    /**
     * Gets the deviceInfo value for this GetDeviceInfoResponseType.
     * 
     * @return deviceInfo
     */
    public com.broadon.wsapi.ias.DeviceInfoType getDeviceInfo() {
        return deviceInfo;
    }


    /**
     * Sets the deviceInfo value for this GetDeviceInfoResponseType.
     * 
     * @param deviceInfo
     */
    public void setDeviceInfo(com.broadon.wsapi.ias.DeviceInfoType deviceInfo) {
        this.deviceInfo = deviceInfo;
    }


    /**
     * Gets the deviceSubscriptions value for this GetDeviceInfoResponseType.
     * 
     * @return deviceSubscriptions
     */
    public com.broadon.wsapi.ias.DeviceSubscriptionType[] getDeviceSubscriptions() {
        return deviceSubscriptions;
    }


    /**
     * Sets the deviceSubscriptions value for this GetDeviceInfoResponseType.
     * 
     * @param deviceSubscriptions
     */
    public void setDeviceSubscriptions(com.broadon.wsapi.ias.DeviceSubscriptionType[] deviceSubscriptions) {
        this.deviceSubscriptions = deviceSubscriptions;
    }

    public com.broadon.wsapi.ias.DeviceSubscriptionType getDeviceSubscriptions(int i) {
        return this.deviceSubscriptions[i];
    }

    public void setDeviceSubscriptions(int i, com.broadon.wsapi.ias.DeviceSubscriptionType _value) {
        this.deviceSubscriptions[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetDeviceInfoResponseType)) return false;
        GetDeviceInfoResponseType other = (GetDeviceInfoResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.deviceInfo==null && other.getDeviceInfo()==null) || 
             (this.deviceInfo!=null &&
              this.deviceInfo.equals(other.getDeviceInfo()))) &&
            ((this.deviceSubscriptions==null && other.getDeviceSubscriptions()==null) || 
             (this.deviceSubscriptions!=null &&
              java.util.Arrays.equals(this.deviceSubscriptions, other.getDeviceSubscriptions())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getDeviceInfo() != null) {
            _hashCode += getDeviceInfo().hashCode();
        }
        if (getDeviceSubscriptions() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDeviceSubscriptions());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDeviceSubscriptions(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetDeviceInfoResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "GetDeviceInfoResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deviceInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "DeviceInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "DeviceInfoType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deviceSubscriptions");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "DeviceSubscriptions"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "DeviceSubscriptionType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

package com.broadon.wsapi.pcis;

public class PrepaidCardGenerationPortTypeProxy implements com.broadon.wsapi.pcis.PrepaidCardGenerationPortType {
  private String _endpoint = null;
  private com.broadon.wsapi.pcis.PrepaidCardGenerationPortType prepaidCardGenerationPortType = null;
  
  public PrepaidCardGenerationPortTypeProxy() {
    _initPrepaidCardGenerationPortTypeProxy();
  }
  
  private void _initPrepaidCardGenerationPortTypeProxy() {
    try {
      prepaidCardGenerationPortType = (new com.broadon.wsapi.pcis.PrepaidCardGenerationServiceLocator()).getPrepaidCardGenerationSOAP();
      if (prepaidCardGenerationPortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)prepaidCardGenerationPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)prepaidCardGenerationPortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (prepaidCardGenerationPortType != null)
      ((javax.xml.rpc.Stub)prepaidCardGenerationPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.broadon.wsapi.pcis.PrepaidCardGenerationPortType getPrepaidCardGenerationPortType() {
    if (prepaidCardGenerationPortType == null)
      _initPrepaidCardGenerationPortTypeProxy();
    return prepaidCardGenerationPortType;
  }
  
  public com.broadon.wsapi.pcis.PrepaidCardGenerationResponseType prepaidCardGeneration(com.broadon.wsapi.pcis.PrepaidCardGenerationRequestType prepaidCardGenerationRequest) throws java.rmi.RemoteException{
    if (prepaidCardGenerationPortType == null)
      _initPrepaidCardGenerationPortTypeProxy();
    return prepaidCardGenerationPortType.prepaidCardGeneration(prepaidCardGenerationRequest);
  }
  
  
}
/**
 * AuthorizeCreditCardPaymentRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.pas;

public class AuthorizeCreditCardPaymentRequestType  extends com.broadon.wsapi.pas.AbstractRequestType  implements java.io.Serializable {
    private java.lang.String referenceID;
    private com.broadon.wsapi.pas.MoneyType amount;
    private java.lang.String creditCardNumber;
    private java.lang.String NOACreditCardReq;

    public AuthorizeCreditCardPaymentRequestType() {
    }

    public AuthorizeCreditCardPaymentRequestType(
           java.lang.String referenceID,
           com.broadon.wsapi.pas.MoneyType amount,
           java.lang.String creditCardNumber,
           java.lang.String NOACreditCardReq) {
           this.referenceID = referenceID;
           this.amount = amount;
           this.creditCardNumber = creditCardNumber;
           this.NOACreditCardReq = NOACreditCardReq;
    }


    /**
     * Gets the referenceID value for this AuthorizeCreditCardPaymentRequestType.
     * 
     * @return referenceID
     */
    public java.lang.String getReferenceID() {
        return referenceID;
    }


    /**
     * Sets the referenceID value for this AuthorizeCreditCardPaymentRequestType.
     * 
     * @param referenceID
     */
    public void setReferenceID(java.lang.String referenceID) {
        this.referenceID = referenceID;
    }


    /**
     * Gets the amount value for this AuthorizeCreditCardPaymentRequestType.
     * 
     * @return amount
     */
    public com.broadon.wsapi.pas.MoneyType getAmount() {
        return amount;
    }


    /**
     * Sets the amount value for this AuthorizeCreditCardPaymentRequestType.
     * 
     * @param amount
     */
    public void setAmount(com.broadon.wsapi.pas.MoneyType amount) {
        this.amount = amount;
    }


    /**
     * Gets the creditCardNumber value for this AuthorizeCreditCardPaymentRequestType.
     * 
     * @return creditCardNumber
     */
    public java.lang.String getCreditCardNumber() {
        return creditCardNumber;
    }


    /**
     * Sets the creditCardNumber value for this AuthorizeCreditCardPaymentRequestType.
     * 
     * @param creditCardNumber
     */
    public void setCreditCardNumber(java.lang.String creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }


    /**
     * Gets the NOACreditCardReq value for this AuthorizeCreditCardPaymentRequestType.
     * 
     * @return NOACreditCardReq
     */
    public java.lang.String getNOACreditCardReq() {
        return NOACreditCardReq;
    }


    /**
     * Sets the NOACreditCardReq value for this AuthorizeCreditCardPaymentRequestType.
     * 
     * @param NOACreditCardReq
     */
    public void setNOACreditCardReq(java.lang.String NOACreditCardReq) {
        this.NOACreditCardReq = NOACreditCardReq;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AuthorizeCreditCardPaymentRequestType)) return false;
        AuthorizeCreditCardPaymentRequestType other = (AuthorizeCreditCardPaymentRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.referenceID==null && other.getReferenceID()==null) || 
             (this.referenceID!=null &&
              this.referenceID.equals(other.getReferenceID()))) &&
            ((this.amount==null && other.getAmount()==null) || 
             (this.amount!=null &&
              this.amount.equals(other.getAmount()))) &&
            ((this.creditCardNumber==null && other.getCreditCardNumber()==null) || 
             (this.creditCardNumber!=null &&
              this.creditCardNumber.equals(other.getCreditCardNumber()))) &&
            ((this.NOACreditCardReq==null && other.getNOACreditCardReq()==null) || 
             (this.NOACreditCardReq!=null &&
              this.NOACreditCardReq.equals(other.getNOACreditCardReq())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getReferenceID() != null) {
            _hashCode += getReferenceID().hashCode();
        }
        if (getAmount() != null) {
            _hashCode += getAmount().hashCode();
        }
        if (getCreditCardNumber() != null) {
            _hashCode += getCreditCardNumber().hashCode();
        }
        if (getNOACreditCardReq() != null) {
            _hashCode += getNOACreditCardReq().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AuthorizeCreditCardPaymentRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizeCreditCardPaymentRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("referenceID");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ReferenceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amount");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "Amount"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "MoneyType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditCardNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CreditCardNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NOACreditCardReq");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "NOACreditCardReq"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

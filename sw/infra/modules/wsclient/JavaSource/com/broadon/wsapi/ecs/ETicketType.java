/**
 * ETicketType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ecs;


/**
 * ETicket record containing information about the title owned and
 * licensing rights for the title
 */
public class ETicketType  implements java.io.Serializable {
    private long ticketId;
    private java.lang.String titleId;
    private com.broadon.wsapi.ecs.PricingCategoryType licenseType;
    private com.broadon.wsapi.ecs.LimitType[] limits;
    private long createDate;
    private long revokeDate;

    public ETicketType() {
    }

    public ETicketType(
           long ticketId,
           java.lang.String titleId,
           com.broadon.wsapi.ecs.PricingCategoryType licenseType,
           com.broadon.wsapi.ecs.LimitType[] limits,
           long createDate,
           long revokeDate) {
           this.ticketId = ticketId;
           this.titleId = titleId;
           this.licenseType = licenseType;
           this.limits = limits;
           this.createDate = createDate;
           this.revokeDate = revokeDate;
    }


    /**
     * Gets the ticketId value for this ETicketType.
     * 
     * @return ticketId
     */
    public long getTicketId() {
        return ticketId;
    }


    /**
     * Sets the ticketId value for this ETicketType.
     * 
     * @param ticketId
     */
    public void setTicketId(long ticketId) {
        this.ticketId = ticketId;
    }


    /**
     * Gets the titleId value for this ETicketType.
     * 
     * @return titleId
     */
    public java.lang.String getTitleId() {
        return titleId;
    }


    /**
     * Sets the titleId value for this ETicketType.
     * 
     * @param titleId
     */
    public void setTitleId(java.lang.String titleId) {
        this.titleId = titleId;
    }


    /**
     * Gets the licenseType value for this ETicketType.
     * 
     * @return licenseType
     */
    public com.broadon.wsapi.ecs.PricingCategoryType getLicenseType() {
        return licenseType;
    }


    /**
     * Sets the licenseType value for this ETicketType.
     * 
     * @param licenseType
     */
    public void setLicenseType(com.broadon.wsapi.ecs.PricingCategoryType licenseType) {
        this.licenseType = licenseType;
    }


    /**
     * Gets the limits value for this ETicketType.
     * 
     * @return limits
     */
    public com.broadon.wsapi.ecs.LimitType[] getLimits() {
        return limits;
    }


    /**
     * Sets the limits value for this ETicketType.
     * 
     * @param limits
     */
    public void setLimits(com.broadon.wsapi.ecs.LimitType[] limits) {
        this.limits = limits;
    }

    public com.broadon.wsapi.ecs.LimitType getLimits(int i) {
        return this.limits[i];
    }

    public void setLimits(int i, com.broadon.wsapi.ecs.LimitType _value) {
        this.limits[i] = _value;
    }


    /**
     * Gets the createDate value for this ETicketType.
     * 
     * @return createDate
     */
    public long getCreateDate() {
        return createDate;
    }


    /**
     * Sets the createDate value for this ETicketType.
     * 
     * @param createDate
     */
    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }


    /**
     * Gets the revokeDate value for this ETicketType.
     * 
     * @return revokeDate
     */
    public long getRevokeDate() {
        return revokeDate;
    }


    /**
     * Sets the revokeDate value for this ETicketType.
     * 
     * @param revokeDate
     */
    public void setRevokeDate(long revokeDate) {
        this.revokeDate = revokeDate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ETicketType)) return false;
        ETicketType other = (ETicketType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.ticketId == other.getTicketId() &&
            ((this.titleId==null && other.getTitleId()==null) || 
             (this.titleId!=null &&
              this.titleId.equals(other.getTitleId()))) &&
            ((this.licenseType==null && other.getLicenseType()==null) || 
             (this.licenseType!=null &&
              this.licenseType.equals(other.getLicenseType()))) &&
            ((this.limits==null && other.getLimits()==null) || 
             (this.limits!=null &&
              java.util.Arrays.equals(this.limits, other.getLimits()))) &&
            this.createDate == other.getCreateDate() &&
            this.revokeDate == other.getRevokeDate();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += new Long(getTicketId()).hashCode();
        if (getTitleId() != null) {
            _hashCode += getTitleId().hashCode();
        }
        if (getLicenseType() != null) {
            _hashCode += getLicenseType().hashCode();
        }
        if (getLimits() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getLimits());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getLimits(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        _hashCode += new Long(getCreateDate()).hashCode();
        _hashCode += new Long(getRevokeDate()).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ETicketType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ETicketType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ticketId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TicketId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TicketIdType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titleId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TitleId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TitleIdType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("licenseType");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "LicenseType"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PricingCategoryType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("limits");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "Limits"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "LimitType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "CreateDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TimeStampType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("revokeDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "RevokeDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TimeStampType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

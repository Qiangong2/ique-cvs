/**
 * SubscriptionPricingType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ecs;

public class SubscriptionPricingType  implements java.io.Serializable {
    private int itemId;
    private java.lang.String channelId;
    private java.lang.String channelName;
    private java.lang.String channelDescription;
    /** Length of the subscription, can be in months or days. */
    private com.broadon.wsapi.ecs.TimeDurationType subscriptionLength;
    /** Price of the subscription */
    private com.broadon.wsapi.ecs.PriceType price;
    /** Optional field indicating the max number of
 * 						checkouts allowed for this subscription. */
    private java.lang.Integer maxCheckouts;

    public SubscriptionPricingType() {
    }

    public SubscriptionPricingType(
           int itemId,
           java.lang.String channelId,
           java.lang.String channelName,
           java.lang.String channelDescription,
           com.broadon.wsapi.ecs.TimeDurationType subscriptionLength,
           com.broadon.wsapi.ecs.PriceType price,
           java.lang.Integer maxCheckouts) {
           this.itemId = itemId;
           this.channelId = channelId;
           this.channelName = channelName;
           this.channelDescription = channelDescription;
           this.subscriptionLength = subscriptionLength;
           this.price = price;
           this.maxCheckouts = maxCheckouts;
    }


    /**
     * Gets the itemId value for this SubscriptionPricingType.
     * 
     * @return itemId
     */
    public int getItemId() {
        return itemId;
    }


    /**
     * Sets the itemId value for this SubscriptionPricingType.
     * 
     * @param itemId
     */
    public void setItemId(int itemId) {
        this.itemId = itemId;
    }


    /**
     * Gets the channelId value for this SubscriptionPricingType.
     * 
     * @return channelId
     */
    public java.lang.String getChannelId() {
        return channelId;
    }


    /**
     * Sets the channelId value for this SubscriptionPricingType.
     * 
     * @param channelId
     */
    public void setChannelId(java.lang.String channelId) {
        this.channelId = channelId;
    }


    /**
     * Gets the channelName value for this SubscriptionPricingType.
     * 
     * @return channelName
     */
    public java.lang.String getChannelName() {
        return channelName;
    }


    /**
     * Sets the channelName value for this SubscriptionPricingType.
     * 
     * @param channelName
     */
    public void setChannelName(java.lang.String channelName) {
        this.channelName = channelName;
    }


    /**
     * Gets the channelDescription value for this SubscriptionPricingType.
     * 
     * @return channelDescription
     */
    public java.lang.String getChannelDescription() {
        return channelDescription;
    }


    /**
     * Sets the channelDescription value for this SubscriptionPricingType.
     * 
     * @param channelDescription
     */
    public void setChannelDescription(java.lang.String channelDescription) {
        this.channelDescription = channelDescription;
    }


    /**
     * Gets the subscriptionLength value for this SubscriptionPricingType.
     * 
     * @return subscriptionLength Length of the subscription, can be in months or days.
     */
    public com.broadon.wsapi.ecs.TimeDurationType getSubscriptionLength() {
        return subscriptionLength;
    }


    /**
     * Sets the subscriptionLength value for this SubscriptionPricingType.
     * 
     * @param subscriptionLength Length of the subscription, can be in months or days.
     */
    public void setSubscriptionLength(com.broadon.wsapi.ecs.TimeDurationType subscriptionLength) {
        this.subscriptionLength = subscriptionLength;
    }


    /**
     * Gets the price value for this SubscriptionPricingType.
     * 
     * @return price Price of the subscription
     */
    public com.broadon.wsapi.ecs.PriceType getPrice() {
        return price;
    }


    /**
     * Sets the price value for this SubscriptionPricingType.
     * 
     * @param price Price of the subscription
     */
    public void setPrice(com.broadon.wsapi.ecs.PriceType price) {
        this.price = price;
    }


    /**
     * Gets the maxCheckouts value for this SubscriptionPricingType.
     * 
     * @return maxCheckouts Optional field indicating the max number of
 * 						checkouts allowed for this subscription.
     */
    public java.lang.Integer getMaxCheckouts() {
        return maxCheckouts;
    }


    /**
     * Sets the maxCheckouts value for this SubscriptionPricingType.
     * 
     * @param maxCheckouts Optional field indicating the max number of
 * 						checkouts allowed for this subscription.
     */
    public void setMaxCheckouts(java.lang.Integer maxCheckouts) {
        this.maxCheckouts = maxCheckouts;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SubscriptionPricingType)) return false;
        SubscriptionPricingType other = (SubscriptionPricingType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.itemId == other.getItemId() &&
            ((this.channelId==null && other.getChannelId()==null) || 
             (this.channelId!=null &&
              this.channelId.equals(other.getChannelId()))) &&
            ((this.channelName==null && other.getChannelName()==null) || 
             (this.channelName!=null &&
              this.channelName.equals(other.getChannelName()))) &&
            ((this.channelDescription==null && other.getChannelDescription()==null) || 
             (this.channelDescription!=null &&
              this.channelDescription.equals(other.getChannelDescription()))) &&
            ((this.subscriptionLength==null && other.getSubscriptionLength()==null) || 
             (this.subscriptionLength!=null &&
              this.subscriptionLength.equals(other.getSubscriptionLength()))) &&
            ((this.price==null && other.getPrice()==null) || 
             (this.price!=null &&
              this.price.equals(other.getPrice()))) &&
            ((this.maxCheckouts==null && other.getMaxCheckouts()==null) || 
             (this.maxCheckouts!=null &&
              this.maxCheckouts.equals(other.getMaxCheckouts())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getItemId();
        if (getChannelId() != null) {
            _hashCode += getChannelId().hashCode();
        }
        if (getChannelName() != null) {
            _hashCode += getChannelName().hashCode();
        }
        if (getChannelDescription() != null) {
            _hashCode += getChannelDescription().hashCode();
        }
        if (getSubscriptionLength() != null) {
            _hashCode += getSubscriptionLength().hashCode();
        }
        if (getPrice() != null) {
            _hashCode += getPrice().hashCode();
        }
        if (getMaxCheckouts() != null) {
            _hashCode += getMaxCheckouts().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SubscriptionPricingType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "SubscriptionPricingType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ItemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ItemIdType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("channelId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ChannelId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ChannelIdType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("channelName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ChannelName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("channelDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ChannelDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subscriptionLength");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "SubscriptionLength"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "TimeDurationType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("price");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "Price"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "PriceType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maxCheckouts");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "MaxCheckouts"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

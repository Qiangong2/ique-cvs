package com.broadon.wsapi.ias;

public class IdentityAuthenticationPortTypeProxy implements com.broadon.wsapi.ias.IdentityAuthenticationPortType {
  private String _endpoint = null;
  private com.broadon.wsapi.ias.IdentityAuthenticationPortType identityAuthenticationPortType = null;
  
  public IdentityAuthenticationPortTypeProxy() {
    _initIdentityAuthenticationPortTypeProxy();
  }
  
  private void _initIdentityAuthenticationPortTypeProxy() {
    try {
      identityAuthenticationPortType = (new com.broadon.wsapi.ias.IdentityAuthenticationServiceLocator()).getIdentityAuthenticationSOAP();
      if (identityAuthenticationPortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)identityAuthenticationPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)identityAuthenticationPortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (identityAuthenticationPortType != null)
      ((javax.xml.rpc.Stub)identityAuthenticationPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.broadon.wsapi.ias.IdentityAuthenticationPortType getIdentityAuthenticationPortType() {
    if (identityAuthenticationPortType == null)
      _initIdentityAuthenticationPortTypeProxy();
    return identityAuthenticationPortType;
  }
  
  public com.broadon.wsapi.ias.AuthenticateAccountResponseType authenticateAccount(com.broadon.wsapi.ias.AuthenticateAccountRequestType authenticateAccountRequest) throws java.rmi.RemoteException{
    if (identityAuthenticationPortType == null)
      _initIdentityAuthenticationPortTypeProxy();
    return identityAuthenticationPortType.authenticateAccount(authenticateAccountRequest);
  }
  
  public com.broadon.wsapi.ias.AuthenticateDeviceResponseType authenticateDevice(com.broadon.wsapi.ias.AuthenticateDeviceRequestType authenticateDeviceRequest) throws java.rmi.RemoteException{
    if (identityAuthenticationPortType == null)
      _initIdentityAuthenticationPortTypeProxy();
    return identityAuthenticationPortType.authenticateDevice(authenticateDeviceRequest);
  }
  
  public com.broadon.wsapi.ias.ExportAccountResponseType exportAccount(com.broadon.wsapi.ias.ExportAccountRequestType exportAccountRequest) throws java.rmi.RemoteException{
    if (identityAuthenticationPortType == null)
      _initIdentityAuthenticationPortTypeProxy();
    return identityAuthenticationPortType.exportAccount(exportAccountRequest);
  }
  
  public com.broadon.wsapi.ias.GetAccountAttributesResponseType getAccountAttributes(com.broadon.wsapi.ias.GetAccountAttributesRequestType getAccountAttributesRequest) throws java.rmi.RemoteException{
    if (identityAuthenticationPortType == null)
      _initIdentityAuthenticationPortTypeProxy();
    return identityAuthenticationPortType.getAccountAttributes(getAccountAttributesRequest);
  }
  
  public com.broadon.wsapi.ias.GetAccountInfoResponseType getAccountInfo(com.broadon.wsapi.ias.GetAccountInfoRequestType getAccountInfoRequest) throws java.rmi.RemoteException{
    if (identityAuthenticationPortType == null)
      _initIdentityAuthenticationPortTypeProxy();
    return identityAuthenticationPortType.getAccountInfo(getAccountInfoRequest);
  }
  
  public com.broadon.wsapi.ias.GetDeviceAccountsResponseType getDeviceAccounts(com.broadon.wsapi.ias.GetDeviceAccountsRequestType getDeviceAccountsRequest) throws java.rmi.RemoteException{
    if (identityAuthenticationPortType == null)
      _initIdentityAuthenticationPortTypeProxy();
    return identityAuthenticationPortType.getDeviceAccounts(getDeviceAccountsRequest);
  }
  
  public com.broadon.wsapi.ias.GetDeviceInfoResponseType getDeviceInfo(com.broadon.wsapi.ias.GetDeviceInfoRequestType getDeviceInfoRequest) throws java.rmi.RemoteException{
    if (identityAuthenticationPortType == null)
      _initIdentityAuthenticationPortTypeProxy();
    return identityAuthenticationPortType.getDeviceInfo(getDeviceInfoRequest);
  }
  
  public com.broadon.wsapi.ias.GetDeviceSubscriptionsResponseType getDeviceSubscriptions(com.broadon.wsapi.ias.GetDeviceSubscriptionsRequestType getDeviceSubscriptionsRequest) throws java.rmi.RemoteException{
    if (identityAuthenticationPortType == null)
      _initIdentityAuthenticationPortTypeProxy();
    return identityAuthenticationPortType.getDeviceSubscriptions(getDeviceSubscriptionsRequest);
  }
  
  public com.broadon.wsapi.ias.LinkDeviceAccountResponseType linkDeviceAccount(com.broadon.wsapi.ias.LinkDeviceAccountRequestType linkDeviceAccountRequest) throws java.rmi.RemoteException{
    if (identityAuthenticationPortType == null)
      _initIdentityAuthenticationPortTypeProxy();
    return identityAuthenticationPortType.linkDeviceAccount(linkDeviceAccountRequest);
  }
  
  public com.broadon.wsapi.ias.RegisterAccountResponseType registerAccount(com.broadon.wsapi.ias.RegisterAccountRequestType registerAccountRequest) throws java.rmi.RemoteException{
    if (identityAuthenticationPortType == null)
      _initIdentityAuthenticationPortTypeProxy();
    return identityAuthenticationPortType.registerAccount(registerAccountRequest);
  }
  
  public com.broadon.wsapi.ias.RegisterDeviceResponseType registerDevice(com.broadon.wsapi.ias.RegisterDeviceRequestType registerDeviceRequest) throws java.rmi.RemoteException{
    if (identityAuthenticationPortType == null)
      _initIdentityAuthenticationPortTypeProxy();
    return identityAuthenticationPortType.registerDevice(registerDeviceRequest);
  }
  
  public com.broadon.wsapi.ias.RemoveDeviceAccountResponseType removeDeviceAccount(com.broadon.wsapi.ias.RemoveDeviceAccountRequestType removeDeviceAccountRequest) throws java.rmi.RemoteException{
    if (identityAuthenticationPortType == null)
      _initIdentityAuthenticationPortTypeProxy();
    return identityAuthenticationPortType.removeDeviceAccount(removeDeviceAccountRequest);
  }
  
  public com.broadon.wsapi.ias.RenewAccountTokenResponseType renewAccountToken(com.broadon.wsapi.ias.RenewAccountTokenRequestType renewAccountTokenRequest) throws java.rmi.RemoteException{
    if (identityAuthenticationPortType == null)
      _initIdentityAuthenticationPortTypeProxy();
    return identityAuthenticationPortType.renewAccountToken(renewAccountTokenRequest);
  }
  
  public com.broadon.wsapi.ias.RenewDeviceTokenResponseType renewDeviceToken(com.broadon.wsapi.ias.RenewDeviceTokenRequestType renewDeviceTokenRequest) throws java.rmi.RemoteException{
    if (identityAuthenticationPortType == null)
      _initIdentityAuthenticationPortTypeProxy();
    return identityAuthenticationPortType.renewDeviceToken(renewDeviceTokenRequest);
  }
  
  public com.broadon.wsapi.ias.ResetAccountPasswordResponseType resetAccountPassword(com.broadon.wsapi.ias.ResetAccountPasswordRequestType resetAccountPasswordRequest) throws java.rmi.RemoteException{
    if (identityAuthenticationPortType == null)
      _initIdentityAuthenticationPortTypeProxy();
    return identityAuthenticationPortType.resetAccountPassword(resetAccountPasswordRequest);
  }
  
  public com.broadon.wsapi.ias.UpdateAccountResponseType updateAccount(com.broadon.wsapi.ias.UpdateAccountRequestType updateAccountRequest) throws java.rmi.RemoteException{
    if (identityAuthenticationPortType == null)
      _initIdentityAuthenticationPortTypeProxy();
    return identityAuthenticationPortType.updateAccount(updateAccountRequest);
  }
  
  public com.broadon.wsapi.ias.UpdateAccountAttributesResponseType updateAccountAttributes(com.broadon.wsapi.ias.UpdateAccountAttributesRequestType updateAccountAttributesRequest) throws java.rmi.RemoteException{
    if (identityAuthenticationPortType == null)
      _initIdentityAuthenticationPortTypeProxy();
    return identityAuthenticationPortType.updateAccountAttributes(updateAccountAttributesRequest);
  }
  
  public com.broadon.wsapi.ias.UpdateAccountPasswordResponseType updateAccountPassword(com.broadon.wsapi.ias.UpdateAccountPasswordRequestType updateAccountPasswordRequest) throws java.rmi.RemoteException{
    if (identityAuthenticationPortType == null)
      _initIdentityAuthenticationPortTypeProxy();
    return identityAuthenticationPortType.updateAccountPassword(updateAccountPasswordRequest);
  }
  
  public com.broadon.wsapi.ias.UpdateDeviceSubscriptionResponseType updateDeviceSubscription(com.broadon.wsapi.ias.UpdateDeviceSubscriptionRequestType updateDeviceSubscriptionRequest) throws java.rmi.RemoteException{
    if (identityAuthenticationPortType == null)
      _initIdentityAuthenticationPortTypeProxy();
    return identityAuthenticationPortType.updateDeviceSubscription(updateDeviceSubscriptionRequest);
  }
  
  public com.broadon.wsapi.ias.ValidateAccountTokenResponseType validateAccountToken(com.broadon.wsapi.ias.ValidateAccountTokenRequestType validateAccountTokenRequest) throws java.rmi.RemoteException{
    if (identityAuthenticationPortType == null)
      _initIdentityAuthenticationPortTypeProxy();
    return identityAuthenticationPortType.validateAccountToken(validateAccountTokenRequest);
  }
  
  public com.broadon.wsapi.ias.ValidateDeviceTokenResponseType validateDeviceToken(com.broadon.wsapi.ias.ValidateDeviceTokenRequestType validateDeviceTokenRequest) throws java.rmi.RemoteException{
    if (identityAuthenticationPortType == null)
      _initIdentityAuthenticationPortTypeProxy();
    return identityAuthenticationPortType.validateDeviceToken(validateDeviceTokenRequest);
  }
  
  
}
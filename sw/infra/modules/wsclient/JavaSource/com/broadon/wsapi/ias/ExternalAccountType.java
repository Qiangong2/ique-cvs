/**
 * ExternalAccountType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ias;

public class ExternalAccountType  implements java.io.Serializable {
    private java.lang.String externalAccountId;
    private java.lang.String externalEntity;
    private java.lang.String externalData;

    public ExternalAccountType() {
    }

    public ExternalAccountType(
           java.lang.String externalAccountId,
           java.lang.String externalEntity,
           java.lang.String externalData) {
           this.externalAccountId = externalAccountId;
           this.externalEntity = externalEntity;
           this.externalData = externalData;
    }


    /**
     * Gets the externalAccountId value for this ExternalAccountType.
     * 
     * @return externalAccountId
     */
    public java.lang.String getExternalAccountId() {
        return externalAccountId;
    }


    /**
     * Sets the externalAccountId value for this ExternalAccountType.
     * 
     * @param externalAccountId
     */
    public void setExternalAccountId(java.lang.String externalAccountId) {
        this.externalAccountId = externalAccountId;
    }


    /**
     * Gets the externalEntity value for this ExternalAccountType.
     * 
     * @return externalEntity
     */
    public java.lang.String getExternalEntity() {
        return externalEntity;
    }


    /**
     * Sets the externalEntity value for this ExternalAccountType.
     * 
     * @param externalEntity
     */
    public void setExternalEntity(java.lang.String externalEntity) {
        this.externalEntity = externalEntity;
    }


    /**
     * Gets the externalData value for this ExternalAccountType.
     * 
     * @return externalData
     */
    public java.lang.String getExternalData() {
        return externalData;
    }


    /**
     * Sets the externalData value for this ExternalAccountType.
     * 
     * @param externalData
     */
    public void setExternalData(java.lang.String externalData) {
        this.externalData = externalData;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ExternalAccountType)) return false;
        ExternalAccountType other = (ExternalAccountType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.externalAccountId==null && other.getExternalAccountId()==null) || 
             (this.externalAccountId!=null &&
              this.externalAccountId.equals(other.getExternalAccountId()))) &&
            ((this.externalEntity==null && other.getExternalEntity()==null) || 
             (this.externalEntity!=null &&
              this.externalEntity.equals(other.getExternalEntity()))) &&
            ((this.externalData==null && other.getExternalData()==null) || 
             (this.externalData!=null &&
              this.externalData.equals(other.getExternalData())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getExternalAccountId() != null) {
            _hashCode += getExternalAccountId().hashCode();
        }
        if (getExternalEntity() != null) {
            _hashCode += getExternalEntity().hashCode();
        }
        if (getExternalData() != null) {
            _hashCode += getExternalData().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ExternalAccountType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ExternalAccountType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("externalAccountId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ExternalAccountId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("externalEntity");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ExternalEntity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("externalData");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ias.wsapi.broadon.com", "ExternalData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

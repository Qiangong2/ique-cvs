/**
 * OnlineGameServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ogs;

public class OnlineGameServiceLocator extends org.apache.axis.client.Service implements com.broadon.wsapi.ogs.OnlineGameService {

    public OnlineGameServiceLocator() {
    }


    public OnlineGameServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public OnlineGameServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for OnlineGameSOAP
    private java.lang.String OnlineGameSOAP_address = "http://ogs:17104/ogs/services/OnlineGameSOAP";

    public java.lang.String getOnlineGameSOAPAddress() {
        return OnlineGameSOAP_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String OnlineGameSOAPWSDDServiceName = "OnlineGameSOAP";

    public java.lang.String getOnlineGameSOAPWSDDServiceName() {
        return OnlineGameSOAPWSDDServiceName;
    }

    public void setOnlineGameSOAPWSDDServiceName(java.lang.String name) {
        OnlineGameSOAPWSDDServiceName = name;
    }

    public com.broadon.wsapi.ogs.OnlineGamePortType getOnlineGameSOAP() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(OnlineGameSOAP_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getOnlineGameSOAP(endpoint);
    }

    public com.broadon.wsapi.ogs.OnlineGamePortType getOnlineGameSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.broadon.wsapi.ogs.OnlineGameSOAPBindingStub _stub = new com.broadon.wsapi.ogs.OnlineGameSOAPBindingStub(portAddress, this);
            _stub.setPortName(getOnlineGameSOAPWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setOnlineGameSOAPEndpointAddress(java.lang.String address) {
        OnlineGameSOAP_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.broadon.wsapi.ogs.OnlineGamePortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.broadon.wsapi.ogs.OnlineGameSOAPBindingStub _stub = new com.broadon.wsapi.ogs.OnlineGameSOAPBindingStub(new java.net.URL(OnlineGameSOAP_address), this);
                _stub.setPortName(getOnlineGameSOAPWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("OnlineGameSOAP".equals(inputPortName)) {
            return getOnlineGameSOAP();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "OnlineGameService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "OnlineGameSOAP"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("OnlineGameSOAP".equals(portName)) {
            setOnlineGameSOAPEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}

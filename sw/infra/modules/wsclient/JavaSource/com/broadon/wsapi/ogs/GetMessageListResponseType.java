/**
 * GetMessageListResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ogs;

public class GetMessageListResponseType  extends com.broadon.wsapi.ogs.AbstractResponseType  implements java.io.Serializable {
    private com.broadon.wsapi.ogs.MessageHeaderType[] messageHeader;

    public GetMessageListResponseType() {
    }

    public GetMessageListResponseType(
           com.broadon.wsapi.ogs.MessageHeaderType[] messageHeader) {
           this.messageHeader = messageHeader;
    }


    /**
     * Gets the messageHeader value for this GetMessageListResponseType.
     * 
     * @return messageHeader
     */
    public com.broadon.wsapi.ogs.MessageHeaderType[] getMessageHeader() {
        return messageHeader;
    }


    /**
     * Sets the messageHeader value for this GetMessageListResponseType.
     * 
     * @param messageHeader
     */
    public void setMessageHeader(com.broadon.wsapi.ogs.MessageHeaderType[] messageHeader) {
        this.messageHeader = messageHeader;
    }

    public com.broadon.wsapi.ogs.MessageHeaderType getMessageHeader(int i) {
        return this.messageHeader[i];
    }

    public void setMessageHeader(int i, com.broadon.wsapi.ogs.MessageHeaderType _value) {
        this.messageHeader[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetMessageListResponseType)) return false;
        GetMessageListResponseType other = (GetMessageListResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.messageHeader==null && other.getMessageHeader()==null) || 
             (this.messageHeader!=null &&
              java.util.Arrays.equals(this.messageHeader, other.getMessageHeader())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getMessageHeader() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getMessageHeader());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getMessageHeader(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetMessageListResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetMessageListResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("messageHeader");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "MessageHeader"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "MessageHeaderType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

/**
 * ECardRecordType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.pas;


/**
 * ECardType is 6 digits.
 * 				ECardNumber is a 10 digit sequence number that uniquely identify
 * an ECard, regardless the type.
 * 				HashNumber is a 32 Hex-Decimal string from the 10 digit readom
 * number of the ECard.
 * 				Country code is optional and follows ISO 3166 standard.
 */
public class ECardRecordType  implements java.io.Serializable {
    private java.lang.String ECardType;
    private java.lang.String ECardNumber;
    private java.lang.String hashNumber;
    private java.lang.String countryCode;

    public ECardRecordType() {
    }

    public ECardRecordType(
           java.lang.String ECardType,
           java.lang.String ECardNumber,
           java.lang.String hashNumber,
           java.lang.String countryCode) {
           this.ECardType = ECardType;
           this.ECardNumber = ECardNumber;
           this.hashNumber = hashNumber;
           this.countryCode = countryCode;
    }


    /**
     * Gets the ECardType value for this ECardRecordType.
     * 
     * @return ECardType
     */
    public java.lang.String getECardType() {
        return ECardType;
    }


    /**
     * Sets the ECardType value for this ECardRecordType.
     * 
     * @param ECardType
     */
    public void setECardType(java.lang.String ECardType) {
        this.ECardType = ECardType;
    }


    /**
     * Gets the ECardNumber value for this ECardRecordType.
     * 
     * @return ECardNumber
     */
    public java.lang.String getECardNumber() {
        return ECardNumber;
    }


    /**
     * Sets the ECardNumber value for this ECardRecordType.
     * 
     * @param ECardNumber
     */
    public void setECardNumber(java.lang.String ECardNumber) {
        this.ECardNumber = ECardNumber;
    }


    /**
     * Gets the hashNumber value for this ECardRecordType.
     * 
     * @return hashNumber
     */
    public java.lang.String getHashNumber() {
        return hashNumber;
    }


    /**
     * Sets the hashNumber value for this ECardRecordType.
     * 
     * @param hashNumber
     */
    public void setHashNumber(java.lang.String hashNumber) {
        this.hashNumber = hashNumber;
    }


    /**
     * Gets the countryCode value for this ECardRecordType.
     * 
     * @return countryCode
     */
    public java.lang.String getCountryCode() {
        return countryCode;
    }


    /**
     * Sets the countryCode value for this ECardRecordType.
     * 
     * @param countryCode
     */
    public void setCountryCode(java.lang.String countryCode) {
        this.countryCode = countryCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ECardRecordType)) return false;
        ECardRecordType other = (ECardRecordType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ECardType==null && other.getECardType()==null) || 
             (this.ECardType!=null &&
              this.ECardType.equals(other.getECardType()))) &&
            ((this.ECardNumber==null && other.getECardNumber()==null) || 
             (this.ECardNumber!=null &&
              this.ECardNumber.equals(other.getECardNumber()))) &&
            ((this.hashNumber==null && other.getHashNumber()==null) || 
             (this.hashNumber!=null &&
              this.hashNumber.equals(other.getHashNumber()))) &&
            ((this.countryCode==null && other.getCountryCode()==null) || 
             (this.countryCode!=null &&
              this.countryCode.equals(other.getCountryCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getECardType() != null) {
            _hashCode += getECardType().hashCode();
        }
        if (getECardNumber() != null) {
            _hashCode += getECardNumber().hashCode();
        }
        if (getHashNumber() != null) {
            _hashCode += getHashNumber().hashCode();
        }
        if (getCountryCode() != null) {
            _hashCode += getCountryCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ECardRecordType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ECardRecordType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ECardType");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ECardType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ECardNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ECardNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hashNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "HashNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("countryCode");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CountryCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

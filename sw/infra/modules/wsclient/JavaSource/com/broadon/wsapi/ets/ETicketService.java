/**
 * ETicketService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ets;

public interface ETicketService extends javax.xml.rpc.Service {
    public java.lang.String getETicketSOAPAddress();

    public com.broadon.wsapi.ets.ETicketPortType getETicketSOAP() throws javax.xml.rpc.ServiceException;

    public com.broadon.wsapi.ets.ETicketPortType getETicketSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}

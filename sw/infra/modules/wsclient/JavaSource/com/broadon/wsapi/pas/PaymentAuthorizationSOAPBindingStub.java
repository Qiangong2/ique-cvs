/**
 * PaymentAuthorizationSOAPBindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.pas;

public class PaymentAuthorizationSOAPBindingStub extends org.apache.axis.client.Stub implements com.broadon.wsapi.pas.PaymentAuthorizationPortType {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[19];
        _initOperationDesc1();
        _initOperationDesc2();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AuthorizeAccountPayment");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizeAccountPayment"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizeAccountPaymentRequestType"), com.broadon.wsapi.pas.AuthorizeAccountPaymentRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizeAccountPaymentResponseType"));
        oper.setReturnClass(com.broadon.wsapi.pas.AuthorizeAccountPaymentResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizeAccountPaymentResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AuthorizeECardPayment");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizeECardPayment"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizeECardPaymentRequestType"), com.broadon.wsapi.pas.AuthorizeECardPaymentRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizeECardPaymentResponseType"));
        oper.setReturnClass(com.broadon.wsapi.pas.AuthorizeECardPaymentResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizeECardPaymentResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AuthorizeCreditCardPayment");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizeCreditCardPayment"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizeCreditCardPaymentRequestType"), com.broadon.wsapi.pas.AuthorizeCreditCardPaymentRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizeCreditCardPaymentResponseType"));
        oper.setReturnClass(com.broadon.wsapi.pas.AuthorizeCreditCardPaymentResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizeCreditCardPaymentResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CapturePayment");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CapturePayment"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CapturePaymentRequestType"), com.broadon.wsapi.pas.CapturePaymentRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CapturePaymentResponseType"));
        oper.setReturnClass(com.broadon.wsapi.pas.CapturePaymentResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CapturePaymentResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("RefundPayment");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "RefundPayment"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "RefundPaymentRequestType"), com.broadon.wsapi.pas.RefundPaymentRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "RefundPaymentResponseType"));
        oper.setReturnClass(com.broadon.wsapi.pas.RefundPaymentResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "RefundPaymentResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("VoidAuthorization");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "VoidAuthorization"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "VoidAuthorizationRequestType"), com.broadon.wsapi.pas.VoidAuthorizationRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "VoidAuthorizationResponseType"));
        oper.setReturnClass(com.broadon.wsapi.pas.VoidAuthorizationResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "VoidAuthorizationResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetAuthorizationHistory");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetAuthorizationHistory"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetAuthorizationHistoryRequestType"), com.broadon.wsapi.pas.GetAuthorizationHistoryRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetAuthorizationHistoryResponseType"));
        oper.setReturnClass(com.broadon.wsapi.pas.GetAuthorizationHistoryResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetAuthorizationHistoryResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetBalanceAccountHistory");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetAccountHistory"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetAccountHistoryRequestType"), com.broadon.wsapi.pas.GetAccountHistoryRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetAccountHistoryResponseType"));
        oper.setReturnClass(com.broadon.wsapi.pas.GetAccountHistoryResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetAccountHistoryResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetECardAccountHistory");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetECardHistory"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetECardHistoryRequestType"), com.broadon.wsapi.pas.GetECardHistoryRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetECardHistoryResponseType"));
        oper.setReturnClass(com.broadon.wsapi.pas.GetECardHistoryResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetECardHistoryResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetCreditCardAccountHistory");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetCreditCardHistory"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetCreditCardHistoryRequestType"), com.broadon.wsapi.pas.GetCreditCardHistoryRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetCreditCardHistoryResponseType"));
        oper.setReturnClass(com.broadon.wsapi.pas.GetCreditCardHistoryResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetCreditCardHistoryResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CaptureAccountToAccount");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CaptureAccountToAccount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CaptureAccountToAccountRequestType"), com.broadon.wsapi.pas.CaptureAccountToAccountRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CaptureAccountToAccountResponseType"));
        oper.setReturnClass(com.broadon.wsapi.pas.CaptureAccountToAccountResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CaptureAccountToAccountResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CaptureCreditCardToAccount");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CaptureCreditCardToAccount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CaptureCreditCardToAccountRequestType"), com.broadon.wsapi.pas.CaptureCreditCardToAccountRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CaptureCreditCardToAccountResponseType"));
        oper.setReturnClass(com.broadon.wsapi.pas.CaptureCreditCardToAccountResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CaptureCreditCardToAccountResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CaptureECardToAccount");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CaptureECardToAccount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CaptureECardToAccountRequestType"), com.broadon.wsapi.pas.CaptureECardToAccountRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CaptureECardToAccountResponseType"));
        oper.setReturnClass(com.broadon.wsapi.pas.CaptureECardToAccountResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CaptureECardToAccountResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[12] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CheckAccountBalance");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CheckAccountBalance"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CheckAccountBalanceRequestType"), com.broadon.wsapi.pas.CheckAccountBalanceRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CheckAccountBalanceResponseType"));
        oper.setReturnClass(com.broadon.wsapi.pas.CheckAccountBalanceResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CheckAccountBalanceResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[13] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CheckECardBalance");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CheckECardBalance"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CheckECardBalanceRequestType"), com.broadon.wsapi.pas.CheckECardBalanceRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CheckECardBalanceResponseType"));
        oper.setReturnClass(com.broadon.wsapi.pas.CheckECardBalanceResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CheckECardBalanceResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[14] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CreateAccount");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CreateAccount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CreateAccountRequestType"), com.broadon.wsapi.pas.CreateAccountRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CreateAccountResponseType"));
        oper.setReturnClass(com.broadon.wsapi.pas.CreateAccountResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CreateAccountResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[15] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("DisableAccount");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "DisableAccount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "DisableAccountRequestType"), com.broadon.wsapi.pas.DisableAccountRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "DisableAccountResponseType"));
        oper.setReturnClass(com.broadon.wsapi.pas.DisableAccountResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "DisableAccountResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[16] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ResetAccountPIN");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ResetAccountPIN"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ResetAccountPINRequestType"), com.broadon.wsapi.pas.ResetAccountPINRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ResetAccountPINResponseType"));
        oper.setReturnClass(com.broadon.wsapi.pas.ResetAccountPINResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ResetAccountPINResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[17] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetECardType");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetECardType"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetECardTypeRequestType"), com.broadon.wsapi.pas.GetECardTypeRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetECardTypeResponseType"));
        oper.setReturnClass(com.broadon.wsapi.pas.GetECardTypeResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetECardTypeResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[18] = oper;

    }

    public PaymentAuthorizationSOAPBindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public PaymentAuthorizationSOAPBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public PaymentAuthorizationSOAPBindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AbstractRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.AbstractRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AbstractResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.AbstractResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ActionKindType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.ActionKindType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ActionRecordType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.ActionRecordType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizationRecordType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.AuthorizationRecordType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizationStateType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.AuthorizationStateType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizationStatusType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.AuthorizationStatusType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizeAccountPaymentRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.AuthorizeAccountPaymentRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizeAccountPaymentResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.AuthorizeAccountPaymentResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizeCreditCardPaymentRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.AuthorizeCreditCardPaymentRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizeCreditCardPaymentResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.AuthorizeCreditCardPaymentResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizeECardPaymentRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.AuthorizeECardPaymentRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizeECardPaymentResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.AuthorizeECardPaymentResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "BalanceAccountInfoType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.BalanceAccountInfoType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "BalanceAccountType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.BalanceAccountType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "BalanceType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.BalanceType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CaptureAccountToAccountRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.CaptureAccountToAccountRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CaptureAccountToAccountResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.CaptureAccountToAccountResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CaptureCreditCardToAccountRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.CaptureCreditCardToAccountRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CaptureCreditCardToAccountResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.CaptureCreditCardToAccountResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CaptureECardToAccountRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.CaptureECardToAccountRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CaptureECardToAccountResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.CaptureECardToAccountResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CapturePaymentRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.CapturePaymentRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CapturePaymentResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.CapturePaymentResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CheckAccountBalanceRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.CheckAccountBalanceRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CheckAccountBalanceResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.CheckAccountBalanceResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CheckECardBalanceRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.CheckECardBalanceRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CheckECardBalanceResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.CheckECardBalanceResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CreateAccountRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.CreateAccountRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CreateAccountResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.CreateAccountResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "DisableAccountRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.DisableAccountRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "DisableAccountResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.DisableAccountResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ECardInfoType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.ECardInfoType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ECardKindType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.ECardKindType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ECardRecordType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.ECardRecordType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ECardTypeCountriesType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.ECardTypeCountriesType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetAccountHistoryRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.GetAccountHistoryRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetAccountHistoryResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.GetAccountHistoryResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetAuthorizationHistoryRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.GetAuthorizationHistoryRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetAuthorizationHistoryResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.GetAuthorizationHistoryResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetCreditCardHistoryRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.GetCreditCardHistoryRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetCreditCardHistoryResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.GetCreditCardHistoryResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetECardHistoryRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.GetECardHistoryRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetECardHistoryResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.GetECardHistoryResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetECardTypeRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.GetECardTypeRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetECardTypeResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.GetECardTypeResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "MoneyType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.MoneyType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "PasTokenType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.PasTokenType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "PaymentMethodType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.PaymentMethodType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "PropertyType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.PropertyType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "RefundPaymentRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.RefundPaymentRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "RefundPaymentResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.RefundPaymentResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ResetAccountPINRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.ResetAccountPINRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ResetAccountPINResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.ResetAccountPINResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "TimeStampType");
            cachedSerQNames.add(qName);
            cls = long.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(simplesf);
            cachedDeserFactories.add(simpledf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "TransactionRecordType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.TransactionRecordType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "VoidAuthorizationRequestType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.VoidAuthorizationRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "VoidAuthorizationResponseType");
            cachedSerQNames.add(qName);
            cls = com.broadon.wsapi.pas.VoidAuthorizationResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.broadon.wsapi.pas.AuthorizeAccountPaymentResponseType authorizeAccountPayment(com.broadon.wsapi.pas.AuthorizeAccountPaymentRequestType authorizeAccountPaymentRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:wsapi.pas.broadon.com/AuthorizeAccountPayment");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "AuthorizeAccountPayment"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {authorizeAccountPaymentRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.pas.AuthorizeAccountPaymentResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.pas.AuthorizeAccountPaymentResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.pas.AuthorizeAccountPaymentResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.pas.AuthorizeECardPaymentResponseType authorizeECardPayment(com.broadon.wsapi.pas.AuthorizeECardPaymentRequestType authorizeECardPaymentRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:wsapi.pas.broadon.com/AuthorizeECardPayment");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "AuthorizeECardPayment"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {authorizeECardPaymentRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.pas.AuthorizeECardPaymentResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.pas.AuthorizeECardPaymentResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.pas.AuthorizeECardPaymentResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.pas.AuthorizeCreditCardPaymentResponseType authorizeCreditCardPayment(com.broadon.wsapi.pas.AuthorizeCreditCardPaymentRequestType authorizeCreditCardPaymentRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:wsapi.pas.broadon.com/AuthorizeCreditCardPayment");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "AuthorizeCreditCardPayment"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {authorizeCreditCardPaymentRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.pas.AuthorizeCreditCardPaymentResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.pas.AuthorizeCreditCardPaymentResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.pas.AuthorizeCreditCardPaymentResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.pas.CapturePaymentResponseType capturePayment(com.broadon.wsapi.pas.CapturePaymentRequestType capturePaymentRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:wsapi.pas.broadon.com/CapturePayment");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "CapturePayment"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {capturePaymentRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.pas.CapturePaymentResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.pas.CapturePaymentResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.pas.CapturePaymentResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.pas.RefundPaymentResponseType refundPayment(com.broadon.wsapi.pas.RefundPaymentRequestType refundPaymentRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:wsapi.pas.broadon.com/RefundPayment");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "RefundPayment"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {refundPaymentRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.pas.RefundPaymentResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.pas.RefundPaymentResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.pas.RefundPaymentResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.pas.VoidAuthorizationResponseType voidAuthorization(com.broadon.wsapi.pas.VoidAuthorizationRequestType voidAuthorizationRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:wsapi.pas.broadon.com/VoidAuthorization");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "VoidAuthorization"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {voidAuthorizationRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.pas.VoidAuthorizationResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.pas.VoidAuthorizationResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.pas.VoidAuthorizationResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.pas.GetAuthorizationHistoryResponseType getAuthorizationHistory(com.broadon.wsapi.pas.GetAuthorizationHistoryRequestType getAuthorizationHistoryRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:wsapi.pas.broadon.com/GetAuthorizationHistory");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetAuthorizationHistory"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getAuthorizationHistoryRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.pas.GetAuthorizationHistoryResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.pas.GetAuthorizationHistoryResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.pas.GetAuthorizationHistoryResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.pas.GetAccountHistoryResponseType getBalanceAccountHistory(com.broadon.wsapi.pas.GetAccountHistoryRequestType getAccountHistoryRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:wsapi.pas.broadon.com/GetAccountHistory");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetBalanceAccountHistory"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getAccountHistoryRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.pas.GetAccountHistoryResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.pas.GetAccountHistoryResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.pas.GetAccountHistoryResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.pas.GetECardHistoryResponseType getECardAccountHistory(com.broadon.wsapi.pas.GetECardHistoryRequestType getECardHistoryRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:wsapi.pas.broadon.com/GetECardHistory");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetECardAccountHistory"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getECardHistoryRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.pas.GetECardHistoryResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.pas.GetECardHistoryResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.pas.GetECardHistoryResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.pas.GetCreditCardHistoryResponseType getCreditCardAccountHistory(com.broadon.wsapi.pas.GetCreditCardHistoryRequestType getCreditCardHistoryRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:wsapi.pas.broadon.com/GetCreditCardHistory");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetCreditCardAccountHistory"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getCreditCardHistoryRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.pas.GetCreditCardHistoryResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.pas.GetCreditCardHistoryResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.pas.GetCreditCardHistoryResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.pas.CaptureAccountToAccountResponseType captureAccountToAccount(com.broadon.wsapi.pas.CaptureAccountToAccountRequestType captureAccountToAccountRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:wsapi.pas.broadon.com/CaptureAccountToAccount");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "CaptureAccountToAccount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {captureAccountToAccountRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.pas.CaptureAccountToAccountResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.pas.CaptureAccountToAccountResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.pas.CaptureAccountToAccountResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.pas.CaptureCreditCardToAccountResponseType captureCreditCardToAccount(com.broadon.wsapi.pas.CaptureCreditCardToAccountRequestType captureCreditCardToAccountRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:wsapi.pas.broadon.com/CaptureCreditCardToAccount");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "CaptureCreditCardToAccount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {captureCreditCardToAccountRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.pas.CaptureCreditCardToAccountResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.pas.CaptureCreditCardToAccountResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.pas.CaptureCreditCardToAccountResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.pas.CaptureECardToAccountResponseType captureECardToAccount(com.broadon.wsapi.pas.CaptureECardToAccountRequestType captureECardToAccountRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:wsapi.pas.broadon.com/CaptureECardToAccount");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "CaptureECardToAccount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {captureECardToAccountRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.pas.CaptureECardToAccountResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.pas.CaptureECardToAccountResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.pas.CaptureECardToAccountResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.pas.CheckAccountBalanceResponseType checkAccountBalance(com.broadon.wsapi.pas.CheckAccountBalanceRequestType checkAccountBalanceRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[13]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:wsapi.pas.broadon.com/CheckAccountBalance");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "CheckAccountBalance"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {checkAccountBalanceRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.pas.CheckAccountBalanceResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.pas.CheckAccountBalanceResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.pas.CheckAccountBalanceResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.pas.CheckECardBalanceResponseType checkECardBalance(com.broadon.wsapi.pas.CheckECardBalanceRequestType checkECardBalanceRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[14]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:wsapi.pas.broadon.com/CheckECardBalance");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "CheckECardBalance"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {checkECardBalanceRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.pas.CheckECardBalanceResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.pas.CheckECardBalanceResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.pas.CheckECardBalanceResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.pas.CreateAccountResponseType createAccount(com.broadon.wsapi.pas.CreateAccountRequestType createAccountRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[15]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:wsapi.pas.broadon.com/CreateAccount");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "CreateAccount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {createAccountRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.pas.CreateAccountResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.pas.CreateAccountResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.pas.CreateAccountResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.pas.DisableAccountResponseType disableAccount(com.broadon.wsapi.pas.DisableAccountRequestType disableAccountRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[16]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:wsapi.pas.broadon.com/DisableAccount");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "DisableAccount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {disableAccountRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.pas.DisableAccountResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.pas.DisableAccountResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.pas.DisableAccountResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.pas.ResetAccountPINResponseType resetAccountPIN(com.broadon.wsapi.pas.ResetAccountPINRequestType resetAccountPINRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[17]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:wsapi.pas.broadon.com/ResetAccountPIN");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "ResetAccountPIN"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {resetAccountPINRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.pas.ResetAccountPINResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.pas.ResetAccountPINResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.pas.ResetAccountPINResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.broadon.wsapi.pas.GetECardTypeResponseType getECardType(com.broadon.wsapi.pas.GetECardTypeRequestType getECardTypeRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[18]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:wsapi.pas.broadon.com/GetECardType");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetECardType"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getECardTypeRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.broadon.wsapi.pas.GetECardTypeResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.broadon.wsapi.pas.GetECardTypeResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.broadon.wsapi.pas.GetECardTypeResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}

package com.broadon.wsapi.pas;

public interface AbstractRequestIfc {

    /**
     * Gets the version value for this AbstractRequestType.
     * 
     * @return version
     */
    public java.lang.String getVersion();

    /**
     * Sets the version value for this AbstractRequestType.
     * 
     * @param version
     */
    public void setVersion(java.lang.String version);

    /**
     * Gets the messageID value for this AbstractRequestType.
     * 
     * @return messageID
     */
    public java.lang.String getMessageID();

    /**
     * Sets the messageID value for this AbstractRequestType.
     * 
     * @param messageID
     */
    public void setMessageID(java.lang.String messageID);

    /**
     * Gets the properties value for this AbstractRequestType.
     * 
     * @return properties Optional field for generic data input from client.
     */
    public com.broadon.wsapi.pas.PropertyType[] getProperties();

    /**
     * Sets the properties value for this AbstractRequestType.
     * 
     * @param properties Optional field for generic data input from client.
     */
    public void setProperties(com.broadon.wsapi.pas.PropertyType[] properties);

    public com.broadon.wsapi.pas.PropertyType getProperties(int i);

    public void setProperties(int i, com.broadon.wsapi.pas.PropertyType _value);

    public boolean equals(java.lang.Object obj);

    public int hashCode();

}
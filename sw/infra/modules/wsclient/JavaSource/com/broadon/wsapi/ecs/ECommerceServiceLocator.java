/**
 * ECommerceServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ecs;

public class ECommerceServiceLocator extends org.apache.axis.client.Service implements com.broadon.wsapi.ecs.ECommerceService {

    public ECommerceServiceLocator() {
    }


    public ECommerceServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ECommerceServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ECommerceSOAP
    private java.lang.String ECommerceSOAP_address = "https://ecs:16981/ecs/services/ECommerceSOAP";

    public java.lang.String getECommerceSOAPAddress() {
        return ECommerceSOAP_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ECommerceSOAPWSDDServiceName = "ECommerceSOAP";

    public java.lang.String getECommerceSOAPWSDDServiceName() {
        return ECommerceSOAPWSDDServiceName;
    }

    public void setECommerceSOAPWSDDServiceName(java.lang.String name) {
        ECommerceSOAPWSDDServiceName = name;
    }

    public com.broadon.wsapi.ecs.ECommercePortType getECommerceSOAP() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ECommerceSOAP_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getECommerceSOAP(endpoint);
    }

    public com.broadon.wsapi.ecs.ECommercePortType getECommerceSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.broadon.wsapi.ecs.ECommerceSOAPBindingStub _stub = new com.broadon.wsapi.ecs.ECommerceSOAPBindingStub(portAddress, this);
            _stub.setPortName(getECommerceSOAPWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setECommerceSOAPEndpointAddress(java.lang.String address) {
        ECommerceSOAP_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.broadon.wsapi.ecs.ECommercePortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.broadon.wsapi.ecs.ECommerceSOAPBindingStub _stub = new com.broadon.wsapi.ecs.ECommerceSOAPBindingStub(new java.net.URL(ECommerceSOAP_address), this);
                _stub.setPortName(getECommerceSOAPWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ECommerceSOAP".equals(inputPortName)) {
            return getECommerceSOAP();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ECommerceService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("urn:ecs.wsapi.broadon.com", "ECommerceSOAP"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ECommerceSOAP".equals(portName)) {
            setECommerceSOAPEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}

/**
 * BalanceAccountInfoType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.pas;

public class BalanceAccountInfoType  implements java.io.Serializable {
    private java.lang.String accountType;
    private java.lang.Long activatedTime;
    private java.lang.Long deactivateTime;
    private java.lang.Long revokedTime;
    private java.lang.Long lastUpdatedTime;
    private com.broadon.wsapi.pas.BalanceType balance;

    public BalanceAccountInfoType() {
    }

    public BalanceAccountInfoType(
           java.lang.String accountType,
           java.lang.Long activatedTime,
           java.lang.Long deactivateTime,
           java.lang.Long revokedTime,
           java.lang.Long lastUpdatedTime,
           com.broadon.wsapi.pas.BalanceType balance) {
           this.accountType = accountType;
           this.activatedTime = activatedTime;
           this.deactivateTime = deactivateTime;
           this.revokedTime = revokedTime;
           this.lastUpdatedTime = lastUpdatedTime;
           this.balance = balance;
    }


    /**
     * Gets the accountType value for this BalanceAccountInfoType.
     * 
     * @return accountType
     */
    public java.lang.String getAccountType() {
        return accountType;
    }


    /**
     * Sets the accountType value for this BalanceAccountInfoType.
     * 
     * @param accountType
     */
    public void setAccountType(java.lang.String accountType) {
        this.accountType = accountType;
    }


    /**
     * Gets the activatedTime value for this BalanceAccountInfoType.
     * 
     * @return activatedTime
     */
    public java.lang.Long getActivatedTime() {
        return activatedTime;
    }


    /**
     * Sets the activatedTime value for this BalanceAccountInfoType.
     * 
     * @param activatedTime
     */
    public void setActivatedTime(java.lang.Long activatedTime) {
        this.activatedTime = activatedTime;
    }


    /**
     * Gets the deactivateTime value for this BalanceAccountInfoType.
     * 
     * @return deactivateTime
     */
    public java.lang.Long getDeactivateTime() {
        return deactivateTime;
    }


    /**
     * Sets the deactivateTime value for this BalanceAccountInfoType.
     * 
     * @param deactivateTime
     */
    public void setDeactivateTime(java.lang.Long deactivateTime) {
        this.deactivateTime = deactivateTime;
    }


    /**
     * Gets the revokedTime value for this BalanceAccountInfoType.
     * 
     * @return revokedTime
     */
    public java.lang.Long getRevokedTime() {
        return revokedTime;
    }


    /**
     * Sets the revokedTime value for this BalanceAccountInfoType.
     * 
     * @param revokedTime
     */
    public void setRevokedTime(java.lang.Long revokedTime) {
        this.revokedTime = revokedTime;
    }


    /**
     * Gets the lastUpdatedTime value for this BalanceAccountInfoType.
     * 
     * @return lastUpdatedTime
     */
    public java.lang.Long getLastUpdatedTime() {
        return lastUpdatedTime;
    }


    /**
     * Sets the lastUpdatedTime value for this BalanceAccountInfoType.
     * 
     * @param lastUpdatedTime
     */
    public void setLastUpdatedTime(java.lang.Long lastUpdatedTime) {
        this.lastUpdatedTime = lastUpdatedTime;
    }


    /**
     * Gets the balance value for this BalanceAccountInfoType.
     * 
     * @return balance
     */
    public com.broadon.wsapi.pas.BalanceType getBalance() {
        return balance;
    }


    /**
     * Sets the balance value for this BalanceAccountInfoType.
     * 
     * @param balance
     */
    public void setBalance(com.broadon.wsapi.pas.BalanceType balance) {
        this.balance = balance;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BalanceAccountInfoType)) return false;
        BalanceAccountInfoType other = (BalanceAccountInfoType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.accountType==null && other.getAccountType()==null) || 
             (this.accountType!=null &&
              this.accountType.equals(other.getAccountType()))) &&
            ((this.activatedTime==null && other.getActivatedTime()==null) || 
             (this.activatedTime!=null &&
              this.activatedTime.equals(other.getActivatedTime()))) &&
            ((this.deactivateTime==null && other.getDeactivateTime()==null) || 
             (this.deactivateTime!=null &&
              this.deactivateTime.equals(other.getDeactivateTime()))) &&
            ((this.revokedTime==null && other.getRevokedTime()==null) || 
             (this.revokedTime!=null &&
              this.revokedTime.equals(other.getRevokedTime()))) &&
            ((this.lastUpdatedTime==null && other.getLastUpdatedTime()==null) || 
             (this.lastUpdatedTime!=null &&
              this.lastUpdatedTime.equals(other.getLastUpdatedTime()))) &&
            ((this.balance==null && other.getBalance()==null) || 
             (this.balance!=null &&
              this.balance.equals(other.getBalance())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAccountType() != null) {
            _hashCode += getAccountType().hashCode();
        }
        if (getActivatedTime() != null) {
            _hashCode += getActivatedTime().hashCode();
        }
        if (getDeactivateTime() != null) {
            _hashCode += getDeactivateTime().hashCode();
        }
        if (getRevokedTime() != null) {
            _hashCode += getRevokedTime().hashCode();
        }
        if (getLastUpdatedTime() != null) {
            _hashCode += getLastUpdatedTime().hashCode();
        }
        if (getBalance() != null) {
            _hashCode += getBalance().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BalanceAccountInfoType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "BalanceAccountInfoType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountType");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AccountType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("activatedTime");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ActivatedTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "TimeStampType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deactivateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "DeactivateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "TimeStampType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("revokedTime");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "RevokedTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "TimeStampType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastUpdatedTime");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "LastUpdatedTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "TimeStampType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("balance");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "Balance"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "BalanceType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

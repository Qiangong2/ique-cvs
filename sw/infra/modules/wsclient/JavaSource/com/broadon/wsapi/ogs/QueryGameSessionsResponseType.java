/**
 * QueryGameSessionsResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ogs;

public class QueryGameSessionsResponseType  extends com.broadon.wsapi.ogs.AbstractResponseType  implements java.io.Serializable {
    private com.broadon.wsapi.ogs.GameSessionType[] gameSession;

    public QueryGameSessionsResponseType() {
    }

    public QueryGameSessionsResponseType(
           com.broadon.wsapi.ogs.GameSessionType[] gameSession) {
           this.gameSession = gameSession;
    }


    /**
     * Gets the gameSession value for this QueryGameSessionsResponseType.
     * 
     * @return gameSession
     */
    public com.broadon.wsapi.ogs.GameSessionType[] getGameSession() {
        return gameSession;
    }


    /**
     * Sets the gameSession value for this QueryGameSessionsResponseType.
     * 
     * @param gameSession
     */
    public void setGameSession(com.broadon.wsapi.ogs.GameSessionType[] gameSession) {
        this.gameSession = gameSession;
    }

    public com.broadon.wsapi.ogs.GameSessionType getGameSession(int i) {
        return this.gameSession[i];
    }

    public void setGameSession(int i, com.broadon.wsapi.ogs.GameSessionType _value) {
        this.gameSession[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof QueryGameSessionsResponseType)) return false;
        QueryGameSessionsResponseType other = (QueryGameSessionsResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.gameSession==null && other.getGameSession()==null) || 
             (this.gameSession!=null &&
              java.util.Arrays.equals(this.gameSession, other.getGameSession())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getGameSession() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGameSession());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGameSession(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(QueryGameSessionsResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "QueryGameSessionsResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gameSession");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GameSession"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GameSessionType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

/**
 * GetNetIdRangeResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.ogs;

public class GetNetIdRangeResponseType  extends com.broadon.wsapi.ogs.AbstractResponseType  implements java.io.Serializable {
    private int beginNetId;
    private int endNetId;

    public GetNetIdRangeResponseType() {
    }

    public GetNetIdRangeResponseType(
           int beginNetId,
           int endNetId) {
           this.beginNetId = beginNetId;
           this.endNetId = endNetId;
    }


    /**
     * Gets the beginNetId value for this GetNetIdRangeResponseType.
     * 
     * @return beginNetId
     */
    public int getBeginNetId() {
        return beginNetId;
    }


    /**
     * Sets the beginNetId value for this GetNetIdRangeResponseType.
     * 
     * @param beginNetId
     */
    public void setBeginNetId(int beginNetId) {
        this.beginNetId = beginNetId;
    }


    /**
     * Gets the endNetId value for this GetNetIdRangeResponseType.
     * 
     * @return endNetId
     */
    public int getEndNetId() {
        return endNetId;
    }


    /**
     * Sets the endNetId value for this GetNetIdRangeResponseType.
     * 
     * @param endNetId
     */
    public void setEndNetId(int endNetId) {
        this.endNetId = endNetId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetNetIdRangeResponseType)) return false;
        GetNetIdRangeResponseType other = (GetNetIdRangeResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.beginNetId == other.getBeginNetId() &&
            this.endNetId == other.getEndNetId();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += getBeginNetId();
        _hashCode += getEndNetId();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetNetIdRangeResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "GetNetIdRangeResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("beginNetId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "BeginNetId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endNetId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:ogs.wsapi.broadon.com", "EndNetId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

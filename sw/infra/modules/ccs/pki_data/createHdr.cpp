// create ccs cache header

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <netinet/in.h>
#include <openssl/md5.h>


// copied from libah/comm/comm.cpp!!!
//  - funny way !!!

static int64_t
htonll(unsigned char* p)
{
    if (htonl(1) == 1) {
        // no swapping needed
        return *((int64_t*) p);
    } else {
        int64_t result = 0;
        for (int i = 0; i < 8; ++i) {
            result = (result << 8) | p[i];
        }
        return result;
    }
}


int main(int argc, char *argv[])
{
   char *infile = argv[1];
   char outfile[256];
   char inbuf[16*1024+1];
   char outbuf[48];  // 48 byte header

   if (argc != 2) exit(-1);
   printf("Create ccs cache header for %s\n", infile); 
   snprintf(outfile, sizeof(outfile), "%s.hdr", infile); 

      // check magic number
    static unsigned char magic[8] = { 0x1c, 0x2c, 0x3c, 0x4c, 0x5c, 0x6c, 0x7c, 0x8c };

    memset(outbuf, 0, sizeof(outbuf)); 
    memcpy(outbuf, magic, sizeof(magic));

    int chunksize = 1024*1024; // 1M 
    chunksize = htonl(chunksize); 
    memcpy(outbuf+12, &chunksize, sizeof(chunksize));

    int fd = open(infile, O_RDONLY);
    if (fd < 0) 
        printf("can't open %s\n", infile);
    int size = read(fd, inbuf, sizeof(inbuf));
    if (size < 0 || size == sizeof(inbuf)) {
        printf("buffer not large enough\n");
        exit(-1);
    }
    close(fd);

    int64_t sz = size;
    sz = htonll((unsigned char*)&sz);
    memcpy(outbuf+16, &sz, sizeof(sz));

    if (MD5((unsigned char *)inbuf, size, (unsigned char *)outbuf+32) == NULL)
        printf("md5 error\n");

    fd = open(outfile, O_WRONLY|O_CREAT, 0755);
    if (fd < 0)
        printf("can't open %s\n", outfile);
    size = write(fd, outbuf, sizeof(outbuf));
    if (size != sizeof(outbuf))
         printf("failed to write %s\n", outfile);
    close(fd);   
}

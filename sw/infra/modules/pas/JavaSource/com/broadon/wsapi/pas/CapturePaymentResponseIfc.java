package com.broadon.wsapi.pas;

public interface CapturePaymentResponseIfc extends AbstractResponseIfc{

    /* (non-Javadoc)
     * @see com.broadon.wsapi.pas.CapturePaymentToAccountResponse#getAuthorizationState()
     */
    public com.broadon.wsapi.pas.AuthorizationStateType getAuthorizationState();

    /* (non-Javadoc)
     * @see com.broadon.wsapi.pas.CapturePaymentToAccountResponse#setAuthorizationState(com.broadon.wsapi.pas.AuthorizationStateType)
     */
    public void setAuthorizationState(
            com.broadon.wsapi.pas.AuthorizationStateType authorizationState);

    /* (non-Javadoc)
     * @see com.broadon.wsapi.pas.CapturePaymentToAccountResponse#equals(java.lang.Object)
     */
    public boolean equals(java.lang.Object obj);

    /* (non-Javadoc)
     * @see com.broadon.wsapi.pas.CapturePaymentToAccountResponse#hashCode()
     */
    public int hashCode();

}
/**
 * AuthorizationStateType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.pas;

public class AuthorizationStateType  implements java.io.Serializable {
    private com.broadon.wsapi.pas.AuthorizationStatusType status;
    private long authorizationTime;
    private long expirationTime;
    private com.broadon.wsapi.pas.MoneyType authorizedAmount;
    private com.broadon.wsapi.pas.MoneyType capturedAmount;
    private com.broadon.wsapi.pas.MoneyType refundedAmount;
    private boolean isRejected;

    public AuthorizationStateType() {
    }

    public AuthorizationStateType(
           com.broadon.wsapi.pas.AuthorizationStatusType status,
           long authorizationTime,
           long expirationTime,
           com.broadon.wsapi.pas.MoneyType authorizedAmount,
           com.broadon.wsapi.pas.MoneyType capturedAmount,
           com.broadon.wsapi.pas.MoneyType refundedAmount,
           boolean isRejected) {
           this.status = status;
           this.authorizationTime = authorizationTime;
           this.expirationTime = expirationTime;
           this.authorizedAmount = authorizedAmount;
           this.capturedAmount = capturedAmount;
           this.refundedAmount = refundedAmount;
           this.isRejected = isRejected;
    }


    /**
     * Gets the status value for this AuthorizationStateType.
     * 
     * @return status
     */
    public com.broadon.wsapi.pas.AuthorizationStatusType getStatus() {
        return status;
    }


    /**
     * Sets the status value for this AuthorizationStateType.
     * 
     * @param status
     */
    public void setStatus(com.broadon.wsapi.pas.AuthorizationStatusType status) {
        this.status = status;
    }


    /**
     * Gets the authorizationTime value for this AuthorizationStateType.
     * 
     * @return authorizationTime
     */
    public long getAuthorizationTime() {
        return authorizationTime;
    }


    /**
     * Sets the authorizationTime value for this AuthorizationStateType.
     * 
     * @param authorizationTime
     */
    public void setAuthorizationTime(long authorizationTime) {
        this.authorizationTime = authorizationTime;
    }


    /**
     * Gets the expirationTime value for this AuthorizationStateType.
     * 
     * @return expirationTime
     */
    public long getExpirationTime() {
        return expirationTime;
    }


    /**
     * Sets the expirationTime value for this AuthorizationStateType.
     * 
     * @param expirationTime
     */
    public void setExpirationTime(long expirationTime) {
        this.expirationTime = expirationTime;
    }


    /**
     * Gets the authorizedAmount value for this AuthorizationStateType.
     * 
     * @return authorizedAmount
     */
    public com.broadon.wsapi.pas.MoneyType getAuthorizedAmount() {
        return authorizedAmount;
    }


    /**
     * Sets the authorizedAmount value for this AuthorizationStateType.
     * 
     * @param authorizedAmount
     */
    public void setAuthorizedAmount(com.broadon.wsapi.pas.MoneyType authorizedAmount) {
        this.authorizedAmount = authorizedAmount;
    }


    /**
     * Gets the capturedAmount value for this AuthorizationStateType.
     * 
     * @return capturedAmount
     */
    public com.broadon.wsapi.pas.MoneyType getCapturedAmount() {
        return capturedAmount;
    }


    /**
     * Sets the capturedAmount value for this AuthorizationStateType.
     * 
     * @param capturedAmount
     */
    public void setCapturedAmount(com.broadon.wsapi.pas.MoneyType capturedAmount) {
        this.capturedAmount = capturedAmount;
    }


    /**
     * Gets the refundedAmount value for this AuthorizationStateType.
     * 
     * @return refundedAmount
     */
    public com.broadon.wsapi.pas.MoneyType getRefundedAmount() {
        return refundedAmount;
    }


    /**
     * Sets the refundedAmount value for this AuthorizationStateType.
     * 
     * @param refundedAmount
     */
    public void setRefundedAmount(com.broadon.wsapi.pas.MoneyType refundedAmount) {
        this.refundedAmount = refundedAmount;
    }


    /**
     * Gets the isRejected value for this AuthorizationStateType.
     * 
     * @return isRejected
     */
    public boolean isIsRejected() {
        return isRejected;
    }


    /**
     * Sets the isRejected value for this AuthorizationStateType.
     * 
     * @param isRejected
     */
    public void setIsRejected(boolean isRejected) {
        this.isRejected = isRejected;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AuthorizationStateType)) return false;
        AuthorizationStateType other = (AuthorizationStateType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            this.authorizationTime == other.getAuthorizationTime() &&
            this.expirationTime == other.getExpirationTime() &&
            ((this.authorizedAmount==null && other.getAuthorizedAmount()==null) || 
             (this.authorizedAmount!=null &&
              this.authorizedAmount.equals(other.getAuthorizedAmount()))) &&
            ((this.capturedAmount==null && other.getCapturedAmount()==null) || 
             (this.capturedAmount!=null &&
              this.capturedAmount.equals(other.getCapturedAmount()))) &&
            ((this.refundedAmount==null && other.getRefundedAmount()==null) || 
             (this.refundedAmount!=null &&
              this.refundedAmount.equals(other.getRefundedAmount()))) &&
            this.isRejected == other.isIsRejected();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        _hashCode += new Long(getAuthorizationTime()).hashCode();
        _hashCode += new Long(getExpirationTime()).hashCode();
        if (getAuthorizedAmount() != null) {
            _hashCode += getAuthorizedAmount().hashCode();
        }
        if (getCapturedAmount() != null) {
            _hashCode += getCapturedAmount().hashCode();
        }
        if (getRefundedAmount() != null) {
            _hashCode += getRefundedAmount().hashCode();
        }
        _hashCode += (isIsRejected() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AuthorizationStateType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizationStateType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "Status"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizationStatusType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authorizationTime");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizationTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "TimeStampType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expirationTime");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ExpirationTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "TimeStampType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authorizedAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizedAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "MoneyType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("capturedAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CapturedAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "MoneyType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("refundedAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "RefundedAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "MoneyType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isRejected");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "IsRejected"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

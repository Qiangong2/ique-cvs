/**
 * GetAccountHistoryResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.pas;


/**
 * Return the Balance Account activity history.
 * 						The response contains a CurrentBalance and list of TransactionRecordType
 * elements.
 * 						Each TransactionRecordType contains an action and the balance
 * prior the action is taken.
 * 						The balance is sorted in reverse time order.
 */
public class GetAccountHistoryResponseType  extends com.broadon.wsapi.pas.AbstractResponseType  implements AccountHistory, java.io.Serializable {
    private com.broadon.wsapi.pas.BalanceType currentBalance;
    private com.broadon.wsapi.pas.TransactionRecordType[] transactions;

    public GetAccountHistoryResponseType() {
    }

    public GetAccountHistoryResponseType(
           com.broadon.wsapi.pas.BalanceType currentBalance,
           com.broadon.wsapi.pas.TransactionRecordType[] transactions) {
           this.currentBalance = currentBalance;
           this.transactions = transactions;
    }


    /**
     * Gets the currentBalance value for this GetAccountHistoryResponseType.
     * 
     * @return currentBalance
     */
    public com.broadon.wsapi.pas.BalanceType getCurrentBalance() {
        return currentBalance;
    }


    /**
     * Sets the currentBalance value for this GetAccountHistoryResponseType.
     * 
     * @param currentBalance
     */
    public void setCurrentBalance(com.broadon.wsapi.pas.BalanceType currentBalance) {
        this.currentBalance = currentBalance;
    }


    /**
     * Gets the transactions value for this GetAccountHistoryResponseType.
     * 
     * @return transactions
     */
    public com.broadon.wsapi.pas.TransactionRecordType[] getTransactions() {
        return transactions;
    }


    /**
     * Sets the transactions value for this GetAccountHistoryResponseType.
     * 
     * @param transactions
     */
    public void setTransactions(com.broadon.wsapi.pas.TransactionRecordType[] transactions) {
        this.transactions = transactions;
    }

    public com.broadon.wsapi.pas.TransactionRecordType getTransactions(int i) {
        return this.transactions[i];
    }

    public void setTransactions(int i, com.broadon.wsapi.pas.TransactionRecordType _value) {
        this.transactions[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetAccountHistoryResponseType)) return false;
        GetAccountHistoryResponseType other = (GetAccountHistoryResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.currentBalance==null && other.getCurrentBalance()==null) || 
             (this.currentBalance!=null &&
              this.currentBalance.equals(other.getCurrentBalance()))) &&
            ((this.transactions==null && other.getTransactions()==null) || 
             (this.transactions!=null &&
              java.util.Arrays.equals(this.transactions, other.getTransactions())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCurrentBalance() != null) {
            _hashCode += getCurrentBalance().hashCode();
        }
        if (getTransactions() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTransactions());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTransactions(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetAccountHistoryResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetAccountHistoryResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currentBalance");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CurrentBalance"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "BalanceType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactions");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "Transactions"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "TransactionRecordType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

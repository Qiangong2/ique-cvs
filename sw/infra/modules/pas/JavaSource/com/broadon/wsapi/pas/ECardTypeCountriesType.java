/**
 * ECardTypeCountriesType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.pas;

public class ECardTypeCountriesType  implements java.io.Serializable {
    private java.lang.String ECardType;
    private java.lang.String[] countries;

    public ECardTypeCountriesType() {
    }

    public ECardTypeCountriesType(
           java.lang.String ECardType,
           java.lang.String[] countries) {
           this.ECardType = ECardType;
           this.countries = countries;
    }


    /**
     * Gets the ECardType value for this ECardTypeCountriesType.
     * 
     * @return ECardType
     */
    public java.lang.String getECardType() {
        return ECardType;
    }


    /**
     * Sets the ECardType value for this ECardTypeCountriesType.
     * 
     * @param ECardType
     */
    public void setECardType(java.lang.String ECardType) {
        this.ECardType = ECardType;
    }


    /**
     * Gets the countries value for this ECardTypeCountriesType.
     * 
     * @return countries
     */
    public java.lang.String[] getCountries() {
        return countries;
    }


    /**
     * Sets the countries value for this ECardTypeCountriesType.
     * 
     * @param countries
     */
    public void setCountries(java.lang.String[] countries) {
        this.countries = countries;
    }

    public java.lang.String getCountries(int i) {
        return this.countries[i];
    }

    public void setCountries(int i, java.lang.String _value) {
        this.countries[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ECardTypeCountriesType)) return false;
        ECardTypeCountriesType other = (ECardTypeCountriesType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ECardType==null && other.getECardType()==null) || 
             (this.ECardType!=null &&
              this.ECardType.equals(other.getECardType()))) &&
            ((this.countries==null && other.getCountries()==null) || 
             (this.countries!=null &&
              java.util.Arrays.equals(this.countries, other.getCountries())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getECardType() != null) {
            _hashCode += getECardType().hashCode();
        }
        if (getCountries() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCountries());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCountries(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ECardTypeCountriesType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ECardTypeCountriesType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ECardType");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ECardType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("countries");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "Countries"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

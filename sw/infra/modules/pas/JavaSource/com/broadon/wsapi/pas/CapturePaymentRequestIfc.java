package com.broadon.wsapi.pas;

public interface CapturePaymentRequestIfc extends AbstractRequestIfc{

    /* (non-Javadoc)
     * @see com.broadon.wsapi.pas.CapturePaymentToAccount#getAuthorizationToken()
     */
    public com.broadon.wsapi.pas.PasTokenType getAuthorizationToken();

    /**
     * Sets the authorizationToken value for this CapturePaymentRequestType.
     * 
     * @param authorizationToken
     */
    public void setAuthorizationToken(
            com.broadon.wsapi.pas.PasTokenType authorizationToken);

    /* (non-Javadoc)
     * @see com.broadon.wsapi.pas.CapturePaymentToAccount#getAmount()
     */
    public com.broadon.wsapi.pas.MoneyType getAmount();

    /**
     * Sets the amount value for this CapturePaymentRequestType.
     * 
     * @param amount
     */
    public void setAmount(com.broadon.wsapi.pas.MoneyType amount);

    /* (non-Javadoc)
     * @see com.broadon.wsapi.pas.CapturePaymentToAccount#isIsFinal()
     */
    public boolean isIsFinal();

    /**
     * Sets the isFinal value for this CapturePaymentRequestType.
     * 
     * @param isFinal If isFinal is set to true, the authorization will be in
     * 										Complete state and not available for future catpure, even
     * the
     * 										authorized amonut is not fully captured yet.
     */
    public void setIsFinal(boolean isFinal);

    public boolean equals(java.lang.Object obj);

    public int hashCode();

}
/**
 * AuthorizeAccountPaymentRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.pas;

public class AuthorizeAccountPaymentRequestType  extends com.broadon.wsapi.pas.AbstractRequestType  implements java.io.Serializable {
    private com.broadon.wsapi.pas.MoneyType amount;
    private java.lang.String referenceID;
    private com.broadon.wsapi.pas.BalanceAccountType balanceAccount;

    public AuthorizeAccountPaymentRequestType() {
    }

    public AuthorizeAccountPaymentRequestType(
           com.broadon.wsapi.pas.MoneyType amount,
           java.lang.String referenceID,
           com.broadon.wsapi.pas.BalanceAccountType balanceAccount) {
           this.amount = amount;
           this.referenceID = referenceID;
           this.balanceAccount = balanceAccount;
    }


    /**
     * Gets the amount value for this AuthorizeAccountPaymentRequestType.
     * 
     * @return amount
     */
    public com.broadon.wsapi.pas.MoneyType getAmount() {
        return amount;
    }


    /**
     * Sets the amount value for this AuthorizeAccountPaymentRequestType.
     * 
     * @param amount
     */
    public void setAmount(com.broadon.wsapi.pas.MoneyType amount) {
        this.amount = amount;
    }


    /**
     * Gets the referenceID value for this AuthorizeAccountPaymentRequestType.
     * 
     * @return referenceID
     */
    public java.lang.String getReferenceID() {
        return referenceID;
    }


    /**
     * Sets the referenceID value for this AuthorizeAccountPaymentRequestType.
     * 
     * @param referenceID
     */
    public void setReferenceID(java.lang.String referenceID) {
        this.referenceID = referenceID;
    }


    /**
     * Gets the balanceAccount value for this AuthorizeAccountPaymentRequestType.
     * 
     * @return balanceAccount
     */
    public com.broadon.wsapi.pas.BalanceAccountType getBalanceAccount() {
        return balanceAccount;
    }


    /**
     * Sets the balanceAccount value for this AuthorizeAccountPaymentRequestType.
     * 
     * @param balanceAccount
     */
    public void setBalanceAccount(com.broadon.wsapi.pas.BalanceAccountType balanceAccount) {
        this.balanceAccount = balanceAccount;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AuthorizeAccountPaymentRequestType)) return false;
        AuthorizeAccountPaymentRequestType other = (AuthorizeAccountPaymentRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.amount==null && other.getAmount()==null) || 
             (this.amount!=null &&
              this.amount.equals(other.getAmount()))) &&
            ((this.referenceID==null && other.getReferenceID()==null) || 
             (this.referenceID!=null &&
              this.referenceID.equals(other.getReferenceID()))) &&
            ((this.balanceAccount==null && other.getBalanceAccount()==null) || 
             (this.balanceAccount!=null &&
              this.balanceAccount.equals(other.getBalanceAccount())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAmount() != null) {
            _hashCode += getAmount().hashCode();
        }
        if (getReferenceID() != null) {
            _hashCode += getReferenceID().hashCode();
        }
        if (getBalanceAccount() != null) {
            _hashCode += getBalanceAccount().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AuthorizeAccountPaymentRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizeAccountPaymentRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amount");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "Amount"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "MoneyType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("referenceID");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ReferenceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("balanceAccount");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "BalanceAccount"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "BalanceAccountType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

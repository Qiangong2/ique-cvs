/**
 * PaymentAuthorizationSOAPBindingImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.pas;

import java.rmi.RemoteException;

import com.broadon.pas.*;

public class PaymentAuthorizationSOAPBindingImpl 
    extends PaymentAuthorizationServiceImpl 
    implements PaymentAuthorizationPortType, PasConstants {
    
    public AuthorizeAccountPaymentResponseType authorizeAccountPayment(
            AuthorizeAccountPaymentRequestType authorizeAccountPaymentRequest) 
    throws java.rmi.RemoteException {
        return bas.authorizeAccountPayment(this, authorizeAccountPaymentRequest);
    }

    public AuthorizeECardPaymentResponseType authorizeECardPayment(
            AuthorizeECardPaymentRequestType authorizeECardPaymentRequest) 
    throws java.rmi.RemoteException {
        return ecs.authorizeECardPayment(this, authorizeECardPaymentRequest);
    }

    public AuthorizeCreditCardPaymentResponseType authorizeCreditCardPayment(
            AuthorizeCreditCardPaymentRequestType authorizeCreditCardPaymentRequest) 
    throws java.rmi.RemoteException {
        return ccs.authorizeCreditCardPayment(this, authorizeCreditCardPaymentRequest);
    }

    public CapturePaymentResponseType capturePayment(
            CapturePaymentRequestType capturePaymentRequest) throws java.rmi.RemoteException {
        CapturePaymentResponseType response = new CapturePaymentResponseType();
        PasFunction.initResponse(capturePaymentRequest, response);
        if (response.getErrorCode()!= PasConstants.STATUS_OK)
            return response;
        PasTokenType token = capturePaymentRequest.getAuthorizationToken();
        if (token == null) {
            PasFunction.setErrorCode(StatusCode.PAS_MISSING_DATA, PasConstants.NO_AUTHO_TOKEN, response);
        } else {
            PaymentMethodType paymentMethod = token.getPaymentMethod();
            if (paymentMethod != null && paymentMethod == PaymentMethodType.ECARD)
                response = ecs.capturePayment(this, capturePaymentRequest);
            else if (paymentMethod != null && paymentMethod == PaymentMethodType.CCARD)
                response = ccs.capturePayment(this, capturePaymentRequest);
            else if (paymentMethod != null && paymentMethod == PaymentMethodType.ACCOUNT)
                response = bas.capturePayment(this, capturePaymentRequest);
            else { // unknown payment method error
                PasFunction.setErrorCode(StatusCode.PAS_UNKNOWN_PAYMENT_METHOD, 
                        PasConstants.CANNOT_CAPTURE, response);
            }
        }
        return response;
    }

    public RefundPaymentResponseType refundPayment(
            RefundPaymentRequestType refundPaymentRequest) throws java.rmi.RemoteException {
        RefundPaymentResponseType response = new RefundPaymentResponseType();
        PasFunction.initResponse(refundPaymentRequest, response);
        if (response.getErrorCode()!= PasConstants.STATUS_OK)
            return response;
        PasTokenType token = refundPaymentRequest.getAuthorizationToken();
        if (token == null) {
            PasFunction.setErrorCode(StatusCode.PAS_MISSING_DATA, PasConstants.NO_AUTHO_TOKEN, response);
        } else {
            PaymentMethodType paymentMethod = token.getPaymentMethod();
            if (paymentMethod != null && paymentMethod == PaymentMethodType.ECARD)
                response = ecs.refundPayment(this, refundPaymentRequest);
            else if (paymentMethod != null && paymentMethod == PaymentMethodType.CCARD)
                response = ccs.refundPayment(this, refundPaymentRequest);
            else if (paymentMethod != null && paymentMethod == PaymentMethodType.ACCOUNT)
                response = bas.refundPayment(this, refundPaymentRequest);
            else { // unknown payment method error
                PasFunction.setErrorCode(StatusCode.PAS_UNKNOWN_PAYMENT_METHOD, 
                        PasConstants.CANNOT_REFUND, response);
            }
        }
        return response;
    }

    public VoidAuthorizationResponseType voidAuthorization(
            VoidAuthorizationRequestType voidAuthorizationRequest) throws java.rmi.RemoteException {
        VoidAuthorizationResponseType response = new VoidAuthorizationResponseType();
        PasFunction.initResponse(voidAuthorizationRequest, response);
        if (response.getErrorCode()!= PasConstants.STATUS_OK)
            return response;
        PasTokenType token = voidAuthorizationRequest.getAuthorizationToken();
        if (token == null) {
            PasFunction.setErrorCode(StatusCode.PAS_MISSING_DATA, PasConstants.NO_AUTHO_TOKEN, response);
        } else {
            PaymentMethodType paymentMethod = token.getPaymentMethod();
            if (paymentMethod != null && paymentMethod == PaymentMethodType.ECARD)
                response = ecs.voidAuthorization(this, voidAuthorizationRequest);
            else if (paymentMethod != null && paymentMethod == PaymentMethodType.CCARD)
                response = ccs.voidAuthorization(this, voidAuthorizationRequest);
            else if (paymentMethod != null && paymentMethod == PaymentMethodType.ACCOUNT)
                response = bas.voidAuthorization(this, voidAuthorizationRequest);
            else { // unknown payment method error
            PasFunction.setErrorCode(StatusCode.PAS_UNKNOWN_PAYMENT_METHOD, PasConstants.CANNOT_VOID, response);
            }
        }
        return response;
    }

    public GetAccountHistoryResponseType getBalanceAccountHistory(
            GetAccountHistoryRequestType getAccountHistoryRequest) 
    throws java.rmi.RemoteException {
        return bas.getBalanceAccountHistory(this, getAccountHistoryRequest);
    }

    public GetECardHistoryResponseType getECardAccountHistory(
            GetECardHistoryRequestType getECardHistoryRequest) 
    throws java.rmi.RemoteException {
        return ecs.getECardAccountHistory(this, getECardHistoryRequest);
    }

    public GetCreditCardHistoryResponseType getCreditCardAccountHistory(
            GetCreditCardHistoryRequestType getCreditCardHistoryRequest) 
    throws java.rmi.RemoteException {
        return ccs.getCreditCardAccountHistory(this, getCreditCardHistoryRequest);
    }

    public CheckAccountBalanceResponseType checkAccountBalance(CheckAccountBalanceRequestType checkAccountBalanceRequest)
    throws java.rmi.RemoteException {
        return bas.checkAccountBalance(this, checkAccountBalanceRequest);
    }

    public CheckECardBalanceResponseType checkECardBalance(CheckECardBalanceRequestType checkECardBalanceRequest)
    throws java.rmi.RemoteException {
        return ecs.checkECardBalance(this, checkECardBalanceRequest);
    }

    public DisableAccountResponseType disableAccount(DisableAccountRequestType disableAccountRequest) throws RemoteException {
        return bas.disableAccount(this, disableAccountRequest);
    }

    public CreateAccountResponseType createAccount(CreateAccountRequestType createAccountRequest) throws RemoteException {
        return bas.createAccount(this, createAccountRequest);
    }

    public GetECardTypeResponseType getECardType(GetECardTypeRequestType getECardTypeRequest)
    throws java.rmi.RemoteException {
        return ecs.getECardType(this, getECardTypeRequest);
    }

    public ResetAccountPINResponseType resetAccountPIN(ResetAccountPINRequestType resetAccountPINRequest) throws RemoteException {
        return bas.resetAccountPIN(this, resetAccountPINRequest);
    }

}

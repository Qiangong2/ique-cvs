/**
 * GetCreditCardHistoryRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.pas;

public class GetCreditCardHistoryRequestType  extends com.broadon.wsapi.pas.AbstractRequestType  implements java.io.Serializable {
    private java.lang.String creditCardID;
    private java.util.Date begin;
    private java.util.Date end;

    public GetCreditCardHistoryRequestType() {
    }

    public GetCreditCardHistoryRequestType(
           java.lang.String creditCardID,
           java.util.Date begin,
           java.util.Date end) {
           this.creditCardID = creditCardID;
           this.begin = begin;
           this.end = end;
    }


    /**
     * Gets the creditCardID value for this GetCreditCardHistoryRequestType.
     * 
     * @return creditCardID
     */
    public java.lang.String getCreditCardID() {
        return creditCardID;
    }


    /**
     * Sets the creditCardID value for this GetCreditCardHistoryRequestType.
     * 
     * @param creditCardID
     */
    public void setCreditCardID(java.lang.String creditCardID) {
        this.creditCardID = creditCardID;
    }


    /**
     * Gets the begin value for this GetCreditCardHistoryRequestType.
     * 
     * @return begin
     */
    public java.util.Date getBegin() {
        return begin;
    }


    /**
     * Sets the begin value for this GetCreditCardHistoryRequestType.
     * 
     * @param begin
     */
    public void setBegin(java.util.Date begin) {
        this.begin = begin;
    }


    /**
     * Gets the end value for this GetCreditCardHistoryRequestType.
     * 
     * @return end
     */
    public java.util.Date getEnd() {
        return end;
    }


    /**
     * Sets the end value for this GetCreditCardHistoryRequestType.
     * 
     * @param end
     */
    public void setEnd(java.util.Date end) {
        this.end = end;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetCreditCardHistoryRequestType)) return false;
        GetCreditCardHistoryRequestType other = (GetCreditCardHistoryRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.creditCardID==null && other.getCreditCardID()==null) || 
             (this.creditCardID!=null &&
              this.creditCardID.equals(other.getCreditCardID()))) &&
            ((this.begin==null && other.getBegin()==null) || 
             (this.begin!=null &&
              this.begin.equals(other.getBegin()))) &&
            ((this.end==null && other.getEnd()==null) || 
             (this.end!=null &&
              this.end.equals(other.getEnd())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCreditCardID() != null) {
            _hashCode += getCreditCardID().hashCode();
        }
        if (getBegin() != null) {
            _hashCode += getBegin().hashCode();
        }
        if (getEnd() != null) {
            _hashCode += getEnd().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetCreditCardHistoryRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetCreditCardHistoryRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditCardID");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CreditCardID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("begin");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "begin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("end");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "end"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

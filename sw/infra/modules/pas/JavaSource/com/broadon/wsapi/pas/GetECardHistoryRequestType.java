/**
 * GetECardHistoryRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.pas;


/**
 * Get ECard Account History.
 * 						begin and end date are optional. No restriction is imposed if
 * the field 
 * 						(begin or end) is missing.
 */
public class GetECardHistoryRequestType  extends com.broadon.wsapi.pas.AbstractRequestType  implements java.io.Serializable {
    private com.broadon.wsapi.pas.ECardRecordType ECardID;
    private java.util.Date begin;
    private java.util.Date end;

    public GetECardHistoryRequestType() {
    }

    public GetECardHistoryRequestType(
           com.broadon.wsapi.pas.ECardRecordType ECardID,
           java.util.Date begin,
           java.util.Date end) {
           this.ECardID = ECardID;
           this.begin = begin;
           this.end = end;
    }


    /**
     * Gets the ECardID value for this GetECardHistoryRequestType.
     * 
     * @return ECardID
     */
    public com.broadon.wsapi.pas.ECardRecordType getECardID() {
        return ECardID;
    }


    /**
     * Sets the ECardID value for this GetECardHistoryRequestType.
     * 
     * @param ECardID
     */
    public void setECardID(com.broadon.wsapi.pas.ECardRecordType ECardID) {
        this.ECardID = ECardID;
    }


    /**
     * Gets the begin value for this GetECardHistoryRequestType.
     * 
     * @return begin
     */
    public java.util.Date getBegin() {
        return begin;
    }


    /**
     * Sets the begin value for this GetECardHistoryRequestType.
     * 
     * @param begin
     */
    public void setBegin(java.util.Date begin) {
        this.begin = begin;
    }


    /**
     * Gets the end value for this GetECardHistoryRequestType.
     * 
     * @return end
     */
    public java.util.Date getEnd() {
        return end;
    }


    /**
     * Sets the end value for this GetECardHistoryRequestType.
     * 
     * @param end
     */
    public void setEnd(java.util.Date end) {
        this.end = end;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetECardHistoryRequestType)) return false;
        GetECardHistoryRequestType other = (GetECardHistoryRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.ECardID==null && other.getECardID()==null) || 
             (this.ECardID!=null &&
              this.ECardID.equals(other.getECardID()))) &&
            ((this.begin==null && other.getBegin()==null) || 
             (this.begin!=null &&
              this.begin.equals(other.getBegin()))) &&
            ((this.end==null && other.getEnd()==null) || 
             (this.end!=null &&
              this.end.equals(other.getEnd())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getECardID() != null) {
            _hashCode += getECardID().hashCode();
        }
        if (getBegin() != null) {
            _hashCode += getBegin().hashCode();
        }
        if (getEnd() != null) {
            _hashCode += getEnd().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetECardHistoryRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetECardHistoryRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ECardID");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ECardID"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ECardRecordType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("begin");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "begin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("end");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "end"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

/**
 * CapturePaymentRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.pas;

public class CapturePaymentRequestType  extends com.broadon.wsapi.pas.AbstractRequestType  implements CapturePaymentRequestIfc, java.io.Serializable {
    private com.broadon.wsapi.pas.PasTokenType authorizationToken;
    private com.broadon.wsapi.pas.MoneyType amount;
    /** If isFinal is set to true, the authorization will be in
 * 										Complete state and not available for future catpure, even
 * the
 * 										authorized amonut is not fully captured yet. */
    private boolean isFinal;

    public CapturePaymentRequestType() {
    }

    public CapturePaymentRequestType(
           com.broadon.wsapi.pas.PasTokenType authorizationToken,
           com.broadon.wsapi.pas.MoneyType amount,
           boolean isFinal) {
           this.authorizationToken = authorizationToken;
           this.amount = amount;
           this.isFinal = isFinal;
    }


    /**
     * Gets the authorizationToken value for this CapturePaymentRequestType.
     * 
     * @return authorizationToken
     */
    public com.broadon.wsapi.pas.PasTokenType getAuthorizationToken() {
        return authorizationToken;
    }


    /**
     * Sets the authorizationToken value for this CapturePaymentRequestType.
     * 
     * @param authorizationToken
     */
    public void setAuthorizationToken(com.broadon.wsapi.pas.PasTokenType authorizationToken) {
        this.authorizationToken = authorizationToken;
    }


    /**
     * Gets the amount value for this CapturePaymentRequestType.
     * 
     * @return amount
     */
    public com.broadon.wsapi.pas.MoneyType getAmount() {
        return amount;
    }


    /**
     * Sets the amount value for this CapturePaymentRequestType.
     * 
     * @param amount
     */
    public void setAmount(com.broadon.wsapi.pas.MoneyType amount) {
        this.amount = amount;
    }


    /**
     * Gets the isFinal value for this CapturePaymentRequestType.
     * 
     * @return isFinal If isFinal is set to true, the authorization will be in
 * 										Complete state and not available for future catpure, even
 * the
 * 										authorized amonut is not fully captured yet.
     */
    public boolean isIsFinal() {
        return isFinal;
    }


    /**
     * Sets the isFinal value for this CapturePaymentRequestType.
     * 
     * @param isFinal If isFinal is set to true, the authorization will be in
 * 										Complete state and not available for future catpure, even
 * the
 * 										authorized amonut is not fully captured yet.
     */
    public void setIsFinal(boolean isFinal) {
        this.isFinal = isFinal;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CapturePaymentRequestType)) return false;
        CapturePaymentRequestType other = (CapturePaymentRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.authorizationToken==null && other.getAuthorizationToken()==null) || 
             (this.authorizationToken!=null &&
              this.authorizationToken.equals(other.getAuthorizationToken()))) &&
            ((this.amount==null && other.getAmount()==null) || 
             (this.amount!=null &&
              this.amount.equals(other.getAmount()))) &&
            this.isFinal == other.isIsFinal();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAuthorizationToken() != null) {
            _hashCode += getAuthorizationToken().hashCode();
        }
        if (getAmount() != null) {
            _hashCode += getAmount().hashCode();
        }
        _hashCode += (isIsFinal() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CapturePaymentRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CapturePaymentRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authorizationToken");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizationToken"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "PasTokenType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amount");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "Amount"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "MoneyType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isFinal");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "IsFinal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

/**
 * PaymentAuthorizationServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.pas;

public class PaymentAuthorizationServiceLocator extends org.apache.axis.client.Service implements com.broadon.wsapi.pas.PaymentAuthorizationService {

    public PaymentAuthorizationServiceLocator() {
    }


    public PaymentAuthorizationServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public PaymentAuthorizationServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for PaymentAuthorizationSOAP
    private java.lang.String PaymentAuthorizationSOAP_address = "http://pas:17103/pas/services/PaymentAuthorizationSOAP";

    public java.lang.String getPaymentAuthorizationSOAPAddress() {
        return PaymentAuthorizationSOAP_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String PaymentAuthorizationSOAPWSDDServiceName = "PaymentAuthorizationSOAP";

    public java.lang.String getPaymentAuthorizationSOAPWSDDServiceName() {
        return PaymentAuthorizationSOAPWSDDServiceName;
    }

    public void setPaymentAuthorizationSOAPWSDDServiceName(java.lang.String name) {
        PaymentAuthorizationSOAPWSDDServiceName = name;
    }

    public com.broadon.wsapi.pas.PaymentAuthorizationPortType getPaymentAuthorizationSOAP() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(PaymentAuthorizationSOAP_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getPaymentAuthorizationSOAP(endpoint);
    }

    public com.broadon.wsapi.pas.PaymentAuthorizationPortType getPaymentAuthorizationSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.broadon.wsapi.pas.PaymentAuthorizationSOAPBindingStub _stub = new com.broadon.wsapi.pas.PaymentAuthorizationSOAPBindingStub(portAddress, this);
            _stub.setPortName(getPaymentAuthorizationSOAPWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setPaymentAuthorizationSOAPEndpointAddress(java.lang.String address) {
        PaymentAuthorizationSOAP_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.broadon.wsapi.pas.PaymentAuthorizationPortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.broadon.wsapi.pas.PaymentAuthorizationSOAPBindingStub _stub = new com.broadon.wsapi.pas.PaymentAuthorizationSOAPBindingStub(new java.net.URL(PaymentAuthorizationSOAP_address), this);
                _stub.setPortName(getPaymentAuthorizationSOAPWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("PaymentAuthorizationSOAP".equals(inputPortName)) {
            return getPaymentAuthorizationSOAP();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "PaymentAuthorizationService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "PaymentAuthorizationSOAP"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("PaymentAuthorizationSOAP".equals(portName)) {
            setPaymentAuthorizationSOAPEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}

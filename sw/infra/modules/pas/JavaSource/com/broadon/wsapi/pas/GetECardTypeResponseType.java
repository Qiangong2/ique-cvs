/**
 * GetECardTypeResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.pas;

public class GetECardTypeResponseType  extends com.broadon.wsapi.pas.AbstractResponseType  implements java.io.Serializable {
    private com.broadon.wsapi.pas.ECardTypeCountriesType ECardType;

    public GetECardTypeResponseType() {
    }

    public GetECardTypeResponseType(
           com.broadon.wsapi.pas.ECardTypeCountriesType ECardType) {
           this.ECardType = ECardType;
    }


    /**
     * Gets the ECardType value for this GetECardTypeResponseType.
     * 
     * @return ECardType
     */
    public com.broadon.wsapi.pas.ECardTypeCountriesType getECardType() {
        return ECardType;
    }


    /**
     * Sets the ECardType value for this GetECardTypeResponseType.
     * 
     * @param ECardType
     */
    public void setECardType(com.broadon.wsapi.pas.ECardTypeCountriesType ECardType) {
        this.ECardType = ECardType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetECardTypeResponseType)) return false;
        GetECardTypeResponseType other = (GetECardTypeResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.ECardType==null && other.getECardType()==null) || 
             (this.ECardType!=null &&
              this.ECardType.equals(other.getECardType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getECardType() != null) {
            _hashCode += getECardType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetECardTypeResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetECardTypeResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ECardType");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ECardType"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ECardTypeCountriesType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

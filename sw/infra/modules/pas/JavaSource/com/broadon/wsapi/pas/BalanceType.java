/**
 * BalanceType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.pas;


/**
 * Reserved is the sum of all authorized payment that not yet captured.
 * 				Available = Balance - Reserved.
 */
public class BalanceType  implements java.io.Serializable {
    private com.broadon.wsapi.pas.MoneyType balance;
    private com.broadon.wsapi.pas.MoneyType reserved;
    private com.broadon.wsapi.pas.MoneyType available;

    public BalanceType() {
    }

    public BalanceType(
           com.broadon.wsapi.pas.MoneyType balance,
           com.broadon.wsapi.pas.MoneyType reserved,
           com.broadon.wsapi.pas.MoneyType available) {
           this.balance = balance;
           this.reserved = reserved;
           this.available = available;
    }


    /**
     * Gets the balance value for this BalanceType.
     * 
     * @return balance
     */
    public com.broadon.wsapi.pas.MoneyType getBalance() {
        return balance;
    }


    /**
     * Sets the balance value for this BalanceType.
     * 
     * @param balance
     */
    public void setBalance(com.broadon.wsapi.pas.MoneyType balance) {
        this.balance = balance;
    }


    /**
     * Gets the reserved value for this BalanceType.
     * 
     * @return reserved
     */
    public com.broadon.wsapi.pas.MoneyType getReserved() {
        return reserved;
    }


    /**
     * Sets the reserved value for this BalanceType.
     * 
     * @param reserved
     */
    public void setReserved(com.broadon.wsapi.pas.MoneyType reserved) {
        this.reserved = reserved;
    }


    /**
     * Gets the available value for this BalanceType.
     * 
     * @return available
     */
    public com.broadon.wsapi.pas.MoneyType getAvailable() {
        return available;
    }


    /**
     * Sets the available value for this BalanceType.
     * 
     * @param available
     */
    public void setAvailable(com.broadon.wsapi.pas.MoneyType available) {
        this.available = available;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BalanceType)) return false;
        BalanceType other = (BalanceType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.balance==null && other.getBalance()==null) || 
             (this.balance!=null &&
              this.balance.equals(other.getBalance()))) &&
            ((this.reserved==null && other.getReserved()==null) || 
             (this.reserved!=null &&
              this.reserved.equals(other.getReserved()))) &&
            ((this.available==null && other.getAvailable()==null) || 
             (this.available!=null &&
              this.available.equals(other.getAvailable())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBalance() != null) {
            _hashCode += getBalance().hashCode();
        }
        if (getReserved() != null) {
            _hashCode += getReserved().hashCode();
        }
        if (getAvailable() != null) {
            _hashCode += getAvailable().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BalanceType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "BalanceType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("balance");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "Balance"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "MoneyType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reserved");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "Reserved"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "MoneyType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("available");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "Available"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "MoneyType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

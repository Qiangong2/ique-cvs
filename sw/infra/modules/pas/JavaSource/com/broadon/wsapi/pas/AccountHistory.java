package com.broadon.wsapi.pas;

public interface AccountHistory {
    /**
     * Sets the transactions value for this GetAccountHistoryResponseType.
     * 
     * @param transactions
     */
    public void setTransactions(com.broadon.wsapi.pas.TransactionRecordType[] transactions);
}

/**
 * CaptureCreditCardToAccountRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.pas;


/**
 * Deposit a Credit Card authorization to a balance account.
 * 						Caller must provide both DepositAmount and CaptureAmount,
 * 						currencyType does not need to match.
 */
public class CaptureCreditCardToAccountRequestType  extends com.broadon.wsapi.pas.CapturePaymentRequestType  implements CapturePaymentToAccountRequestIfc, java.io.Serializable {
    private com.broadon.wsapi.pas.BalanceAccountType toBalanceAccount;
    private com.broadon.wsapi.pas.MoneyType depositAmount;

    public CaptureCreditCardToAccountRequestType() {
    }

    public CaptureCreditCardToAccountRequestType(
           com.broadon.wsapi.pas.BalanceAccountType toBalanceAccount,
           com.broadon.wsapi.pas.MoneyType depositAmount) {
           this.toBalanceAccount = toBalanceAccount;
           this.depositAmount = depositAmount;
    }


    /**
     * Gets the toBalanceAccount value for this CaptureCreditCardToAccountRequestType.
     * 
     * @return toBalanceAccount
     */
    public com.broadon.wsapi.pas.BalanceAccountType getToBalanceAccount() {
        return toBalanceAccount;
    }


    /**
     * Sets the toBalanceAccount value for this CaptureCreditCardToAccountRequestType.
     * 
     * @param toBalanceAccount
     */
    public void setToBalanceAccount(com.broadon.wsapi.pas.BalanceAccountType toBalanceAccount) {
        this.toBalanceAccount = toBalanceAccount;
    }


    /**
     * Gets the depositAmount value for this CaptureCreditCardToAccountRequestType.
     * 
     * @return depositAmount
     */
    public com.broadon.wsapi.pas.MoneyType getDepositAmount() {
        return depositAmount;
    }


    /**
     * Sets the depositAmount value for this CaptureCreditCardToAccountRequestType.
     * 
     * @param depositAmount
     */
    public void setDepositAmount(com.broadon.wsapi.pas.MoneyType depositAmount) {
        this.depositAmount = depositAmount;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CaptureCreditCardToAccountRequestType)) return false;
        CaptureCreditCardToAccountRequestType other = (CaptureCreditCardToAccountRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.toBalanceAccount==null && other.getToBalanceAccount()==null) || 
             (this.toBalanceAccount!=null &&
              this.toBalanceAccount.equals(other.getToBalanceAccount()))) &&
            ((this.depositAmount==null && other.getDepositAmount()==null) || 
             (this.depositAmount!=null &&
              this.depositAmount.equals(other.getDepositAmount())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getToBalanceAccount() != null) {
            _hashCode += getToBalanceAccount().hashCode();
        }
        if (getDepositAmount() != null) {
            _hashCode += getDepositAmount().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CaptureCreditCardToAccountRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CaptureCreditCardToAccountRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("toBalanceAccount");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "toBalanceAccount"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "BalanceAccountType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("depositAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "DepositAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "MoneyType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

/**
 * AuthorizeCreditCardPaymentResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.pas;

public class AuthorizeCreditCardPaymentResponseType  extends com.broadon.wsapi.pas.AbstractResponseType  implements java.io.Serializable {
    /** optional field, has a value iff no error (ErrorCode=0). */
    private com.broadon.wsapi.pas.PasTokenType authorizationToken;
    /** optional field, has a value iff no error (ErrorCode=0). */
    private com.broadon.wsapi.pas.AuthorizationStateType authorizationState;
    private java.lang.String NOACreditCardResp;

    public AuthorizeCreditCardPaymentResponseType() {
    }

    public AuthorizeCreditCardPaymentResponseType(
           com.broadon.wsapi.pas.PasTokenType authorizationToken,
           com.broadon.wsapi.pas.AuthorizationStateType authorizationState,
           java.lang.String NOACreditCardResp) {
           this.authorizationToken = authorizationToken;
           this.authorizationState = authorizationState;
           this.NOACreditCardResp = NOACreditCardResp;
    }


    /**
     * Gets the authorizationToken value for this AuthorizeCreditCardPaymentResponseType.
     * 
     * @return authorizationToken optional field, has a value iff no error (ErrorCode=0).
     */
    public com.broadon.wsapi.pas.PasTokenType getAuthorizationToken() {
        return authorizationToken;
    }


    /**
     * Sets the authorizationToken value for this AuthorizeCreditCardPaymentResponseType.
     * 
     * @param authorizationToken optional field, has a value iff no error (ErrorCode=0).
     */
    public void setAuthorizationToken(com.broadon.wsapi.pas.PasTokenType authorizationToken) {
        this.authorizationToken = authorizationToken;
    }


    /**
     * Gets the authorizationState value for this AuthorizeCreditCardPaymentResponseType.
     * 
     * @return authorizationState optional field, has a value iff no error (ErrorCode=0).
     */
    public com.broadon.wsapi.pas.AuthorizationStateType getAuthorizationState() {
        return authorizationState;
    }


    /**
     * Sets the authorizationState value for this AuthorizeCreditCardPaymentResponseType.
     * 
     * @param authorizationState optional field, has a value iff no error (ErrorCode=0).
     */
    public void setAuthorizationState(com.broadon.wsapi.pas.AuthorizationStateType authorizationState) {
        this.authorizationState = authorizationState;
    }


    /**
     * Gets the NOACreditCardResp value for this AuthorizeCreditCardPaymentResponseType.
     * 
     * @return NOACreditCardResp
     */
    public java.lang.String getNOACreditCardResp() {
        return NOACreditCardResp;
    }


    /**
     * Sets the NOACreditCardResp value for this AuthorizeCreditCardPaymentResponseType.
     * 
     * @param NOACreditCardResp
     */
    public void setNOACreditCardResp(java.lang.String NOACreditCardResp) {
        this.NOACreditCardResp = NOACreditCardResp;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AuthorizeCreditCardPaymentResponseType)) return false;
        AuthorizeCreditCardPaymentResponseType other = (AuthorizeCreditCardPaymentResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.authorizationToken==null && other.getAuthorizationToken()==null) || 
             (this.authorizationToken!=null &&
              this.authorizationToken.equals(other.getAuthorizationToken()))) &&
            ((this.authorizationState==null && other.getAuthorizationState()==null) || 
             (this.authorizationState!=null &&
              this.authorizationState.equals(other.getAuthorizationState()))) &&
            ((this.NOACreditCardResp==null && other.getNOACreditCardResp()==null) || 
             (this.NOACreditCardResp!=null &&
              this.NOACreditCardResp.equals(other.getNOACreditCardResp())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAuthorizationToken() != null) {
            _hashCode += getAuthorizationToken().hashCode();
        }
        if (getAuthorizationState() != null) {
            _hashCode += getAuthorizationState().hashCode();
        }
        if (getNOACreditCardResp() != null) {
            _hashCode += getNOACreditCardResp().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AuthorizeCreditCardPaymentResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizeCreditCardPaymentResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authorizationToken");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizationToken"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "PasTokenType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authorizationState");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizationState"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizationStateType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NOACreditCardResp");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "NOACreditCardResp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

/**
 * PaymentAuthorizationSOAPBindingSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.pas;

import javax.xml.rpc.ServiceException;
import javax.xml.rpc.server.ServiceLifecycle;

public class PaymentAuthorizationSOAPBindingSkeleton implements ServiceLifecycle,
        com.broadon.wsapi.pas.PaymentAuthorizationPortType, org.apache.axis.wsdl.Skeleton {
    private com.broadon.wsapi.pas.PaymentAuthorizationPortType impl;
    private static java.util.Map _myOperations = new java.util.Hashtable();
    private static java.util.Collection _myOperationsList = new java.util.ArrayList();

    /**
    * Returns List of OperationDesc objects with this name
    */
    public static java.util.List getOperationDescByName(java.lang.String methodName) {
        return (java.util.List)_myOperations.get(methodName);
    }

    /**
    * Returns Collection of OperationDescs
    */
    public static java.util.Collection getOperationDescs() {
        return _myOperationsList;
    }

    static {
        org.apache.axis.description.OperationDesc _oper;
        org.apache.axis.description.FaultDesc _fault;
        org.apache.axis.description.ParameterDesc [] _params;
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizeAccountPayment"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizeAccountPaymentRequestType"), com.broadon.wsapi.pas.AuthorizeAccountPaymentRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("authorizeAccountPayment", _params, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizeAccountPaymentResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizeAccountPaymentResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "AuthorizeAccountPayment"));
        _oper.setSoapAction("urn:wsapi.pas.broadon.com/AuthorizeAccountPayment");
        _myOperationsList.add(_oper);
        if (_myOperations.get("authorizeAccountPayment") == null) {
            _myOperations.put("authorizeAccountPayment", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("authorizeAccountPayment")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizeECardPayment"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizeECardPaymentRequestType"), com.broadon.wsapi.pas.AuthorizeECardPaymentRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("authorizeECardPayment", _params, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizeECardPaymentResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizeECardPaymentResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "AuthorizeECardPayment"));
        _oper.setSoapAction("urn:wsapi.pas.broadon.com/AuthorizeECardPayment");
        _myOperationsList.add(_oper);
        if (_myOperations.get("authorizeECardPayment") == null) {
            _myOperations.put("authorizeECardPayment", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("authorizeECardPayment")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizeCreditCardPayment"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizeCreditCardPaymentRequestType"), com.broadon.wsapi.pas.AuthorizeCreditCardPaymentRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("authorizeCreditCardPayment", _params, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizeCreditCardPaymentResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "AuthorizeCreditCardPaymentResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "AuthorizeCreditCardPayment"));
        _oper.setSoapAction("urn:wsapi.pas.broadon.com/AuthorizeCreditCardPayment");
        _myOperationsList.add(_oper);
        if (_myOperations.get("authorizeCreditCardPayment") == null) {
            _myOperations.put("authorizeCreditCardPayment", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("authorizeCreditCardPayment")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CapturePayment"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CapturePaymentRequestType"), com.broadon.wsapi.pas.CapturePaymentRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("capturePayment", _params, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CapturePaymentResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CapturePaymentResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "CapturePayment"));
        _oper.setSoapAction("urn:wsapi.pas.broadon.com/CapturePayment");
        _myOperationsList.add(_oper);
        if (_myOperations.get("capturePayment") == null) {
            _myOperations.put("capturePayment", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("capturePayment")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "RefundPayment"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "RefundPaymentRequestType"), com.broadon.wsapi.pas.RefundPaymentRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("refundPayment", _params, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "RefundPaymentResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "RefundPaymentResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "RefundPayment"));
        _oper.setSoapAction("urn:wsapi.pas.broadon.com/RefundPayment");
        _myOperationsList.add(_oper);
        if (_myOperations.get("refundPayment") == null) {
            _myOperations.put("refundPayment", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("refundPayment")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "VoidAuthorization"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "VoidAuthorizationRequestType"), com.broadon.wsapi.pas.VoidAuthorizationRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("voidAuthorization", _params, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "VoidAuthorizationResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "VoidAuthorizationResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "VoidAuthorization"));
        _oper.setSoapAction("urn:wsapi.pas.broadon.com/VoidAuthorization");
        _myOperationsList.add(_oper);
        if (_myOperations.get("voidAuthorization") == null) {
            _myOperations.put("voidAuthorization", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("voidAuthorization")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetAuthorizationHistory"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetAuthorizationHistoryRequestType"), com.broadon.wsapi.pas.GetAuthorizationHistoryRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getAuthorizationHistory", _params, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetAuthorizationHistoryResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetAuthorizationHistoryResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "GetAuthorizationHistory"));
        _oper.setSoapAction("urn:wsapi.pas.broadon.com/GetAuthorizationHistory");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getAuthorizationHistory") == null) {
            _myOperations.put("getAuthorizationHistory", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getAuthorizationHistory")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetAccountHistory"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetAccountHistoryRequestType"), com.broadon.wsapi.pas.GetAccountHistoryRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getBalanceAccountHistory", _params, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetAccountHistoryResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetAccountHistoryResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "GetBalanceAccountHistory"));
        _oper.setSoapAction("urn:wsapi.pas.broadon.com/GetAccountHistory");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getBalanceAccountHistory") == null) {
            _myOperations.put("getBalanceAccountHistory", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getBalanceAccountHistory")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetECardHistory"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetECardHistoryRequestType"), com.broadon.wsapi.pas.GetECardHistoryRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getECardAccountHistory", _params, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetECardHistoryResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetECardHistoryResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "GetECardAccountHistory"));
        _oper.setSoapAction("urn:wsapi.pas.broadon.com/GetECardHistory");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getECardAccountHistory") == null) {
            _myOperations.put("getECardAccountHistory", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getECardAccountHistory")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetCreditCardHistory"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetCreditCardHistoryRequestType"), com.broadon.wsapi.pas.GetCreditCardHistoryRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getCreditCardAccountHistory", _params, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetCreditCardHistoryResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetCreditCardHistoryResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "GetCreditCardAccountHistory"));
        _oper.setSoapAction("urn:wsapi.pas.broadon.com/GetCreditCardHistory");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getCreditCardAccountHistory") == null) {
            _myOperations.put("getCreditCardAccountHistory", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getCreditCardAccountHistory")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CaptureAccountToAccount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CaptureAccountToAccountRequestType"), com.broadon.wsapi.pas.CaptureAccountToAccountRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("captureAccountToAccount", _params, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CaptureAccountToAccountResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CaptureAccountToAccountResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "CaptureAccountToAccount"));
        _oper.setSoapAction("urn:wsapi.pas.broadon.com/CaptureAccountToAccount");
        _myOperationsList.add(_oper);
        if (_myOperations.get("captureAccountToAccount") == null) {
            _myOperations.put("captureAccountToAccount", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("captureAccountToAccount")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CaptureCreditCardToAccount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CaptureCreditCardToAccountRequestType"), com.broadon.wsapi.pas.CaptureCreditCardToAccountRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("captureCreditCardToAccount", _params, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CaptureCreditCardToAccountResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CaptureCreditCardToAccountResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "CaptureCreditCardToAccount"));
        _oper.setSoapAction("urn:wsapi.pas.broadon.com/CaptureCreditCardToAccount");
        _myOperationsList.add(_oper);
        if (_myOperations.get("captureCreditCardToAccount") == null) {
            _myOperations.put("captureCreditCardToAccount", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("captureCreditCardToAccount")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CaptureECardToAccount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CaptureECardToAccountRequestType"), com.broadon.wsapi.pas.CaptureECardToAccountRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("captureECardToAccount", _params, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CaptureECardToAccountResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CaptureECardToAccountResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "CaptureECardToAccount"));
        _oper.setSoapAction("urn:wsapi.pas.broadon.com/CaptureECardToAccount");
        _myOperationsList.add(_oper);
        if (_myOperations.get("captureECardToAccount") == null) {
            _myOperations.put("captureECardToAccount", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("captureECardToAccount")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CheckAccountBalance"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CheckAccountBalanceRequestType"), com.broadon.wsapi.pas.CheckAccountBalanceRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("checkAccountBalance", _params, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CheckAccountBalanceResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CheckAccountBalanceResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "CheckAccountBalance"));
        _oper.setSoapAction("urn:wsapi.pas.broadon.com/CheckAccountBalance");
        _myOperationsList.add(_oper);
        if (_myOperations.get("checkAccountBalance") == null) {
            _myOperations.put("checkAccountBalance", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("checkAccountBalance")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CheckECardBalance"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CheckECardBalanceRequestType"), com.broadon.wsapi.pas.CheckECardBalanceRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("checkECardBalance", _params, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CheckECardBalanceResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CheckECardBalanceResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "CheckECardBalance"));
        _oper.setSoapAction("urn:wsapi.pas.broadon.com/CheckECardBalance");
        _myOperationsList.add(_oper);
        if (_myOperations.get("checkECardBalance") == null) {
            _myOperations.put("checkECardBalance", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("checkECardBalance")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CreateAccount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CreateAccountRequestType"), com.broadon.wsapi.pas.CreateAccountRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("createAccount", _params, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CreateAccountResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "CreateAccountResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "CreateAccount"));
        _oper.setSoapAction("urn:wsapi.pas.broadon.com/CreateAccount");
        _myOperationsList.add(_oper);
        if (_myOperations.get("createAccount") == null) {
            _myOperations.put("createAccount", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("createAccount")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "DisableAccount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "DisableAccountRequestType"), com.broadon.wsapi.pas.DisableAccountRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("disableAccount", _params, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "DisableAccountResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "DisableAccountResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "DisableAccount"));
        _oper.setSoapAction("urn:wsapi.pas.broadon.com/DisableAccount");
        _myOperationsList.add(_oper);
        if (_myOperations.get("disableAccount") == null) {
            _myOperations.put("disableAccount", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("disableAccount")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ResetAccountPIN"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ResetAccountPINRequestType"), com.broadon.wsapi.pas.ResetAccountPINRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("resetAccountPIN", _params, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ResetAccountPINResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "ResetAccountPINResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "ResetAccountPIN"));
        _oper.setSoapAction("urn:wsapi.pas.broadon.com/ResetAccountPIN");
        _myOperationsList.add(_oper);
        if (_myOperations.get("resetAccountPIN") == null) {
            _myOperations.put("resetAccountPIN", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("resetAccountPIN")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetECardType"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetECardTypeRequestType"), com.broadon.wsapi.pas.GetECardTypeRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getECardType", _params, new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetECardTypeResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "GetECardTypeResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "GetECardType"));
        _oper.setSoapAction("urn:wsapi.pas.broadon.com/GetECardType");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getECardType") == null) {
            _myOperations.put("getECardType", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getECardType")).add(_oper);
    }

    public PaymentAuthorizationSOAPBindingSkeleton() {
        this.impl = new com.broadon.wsapi.pas.PaymentAuthorizationSOAPBindingImpl();
    }

    public PaymentAuthorizationSOAPBindingSkeleton(com.broadon.wsapi.pas.PaymentAuthorizationPortType impl) {
        this.impl = impl;
    }
    public com.broadon.wsapi.pas.AuthorizeAccountPaymentResponseType authorizeAccountPayment(com.broadon.wsapi.pas.AuthorizeAccountPaymentRequestType authorizeAccountPaymentRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.pas.AuthorizeAccountPaymentResponseType ret = impl.authorizeAccountPayment(authorizeAccountPaymentRequest);
        return ret;
    }

    public com.broadon.wsapi.pas.AuthorizeECardPaymentResponseType authorizeECardPayment(com.broadon.wsapi.pas.AuthorizeECardPaymentRequestType authorizeECardPaymentRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.pas.AuthorizeECardPaymentResponseType ret = impl.authorizeECardPayment(authorizeECardPaymentRequest);
        return ret;
    }

    public com.broadon.wsapi.pas.AuthorizeCreditCardPaymentResponseType authorizeCreditCardPayment(com.broadon.wsapi.pas.AuthorizeCreditCardPaymentRequestType authorizeCreditCardPaymentRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.pas.AuthorizeCreditCardPaymentResponseType ret = impl.authorizeCreditCardPayment(authorizeCreditCardPaymentRequest);
        return ret;
    }

    public com.broadon.wsapi.pas.CapturePaymentResponseType capturePayment(com.broadon.wsapi.pas.CapturePaymentRequestType capturePaymentRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.pas.CapturePaymentResponseType ret = impl.capturePayment(capturePaymentRequest);
        return ret;
    }

    public com.broadon.wsapi.pas.RefundPaymentResponseType refundPayment(com.broadon.wsapi.pas.RefundPaymentRequestType refundPaymentRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.pas.RefundPaymentResponseType ret = impl.refundPayment(refundPaymentRequest);
        return ret;
    }

    public com.broadon.wsapi.pas.VoidAuthorizationResponseType voidAuthorization(com.broadon.wsapi.pas.VoidAuthorizationRequestType voidAuthorizationRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.pas.VoidAuthorizationResponseType ret = impl.voidAuthorization(voidAuthorizationRequest);
        return ret;
    }

    public com.broadon.wsapi.pas.GetAuthorizationHistoryResponseType getAuthorizationHistory(com.broadon.wsapi.pas.GetAuthorizationHistoryRequestType getAuthorizationHistoryRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.pas.GetAuthorizationHistoryResponseType ret = impl.getAuthorizationHistory(getAuthorizationHistoryRequest);
        return ret;
    }

    public com.broadon.wsapi.pas.GetAccountHistoryResponseType getBalanceAccountHistory(com.broadon.wsapi.pas.GetAccountHistoryRequestType getAccountHistoryRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.pas.GetAccountHistoryResponseType ret = impl.getBalanceAccountHistory(getAccountHistoryRequest);
        return ret;
    }

    public com.broadon.wsapi.pas.GetECardHistoryResponseType getECardAccountHistory(com.broadon.wsapi.pas.GetECardHistoryRequestType getECardHistoryRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.pas.GetECardHistoryResponseType ret = impl.getECardAccountHistory(getECardHistoryRequest);
        return ret;
    }

    public com.broadon.wsapi.pas.GetCreditCardHistoryResponseType getCreditCardAccountHistory(com.broadon.wsapi.pas.GetCreditCardHistoryRequestType getCreditCardHistoryRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.pas.GetCreditCardHistoryResponseType ret = impl.getCreditCardAccountHistory(getCreditCardHistoryRequest);
        return ret;
    }

    public com.broadon.wsapi.pas.CaptureAccountToAccountResponseType captureAccountToAccount(com.broadon.wsapi.pas.CaptureAccountToAccountRequestType captureAccountToAccountRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.pas.CaptureAccountToAccountResponseType ret = impl.captureAccountToAccount(captureAccountToAccountRequest);
        return ret;
    }

    public com.broadon.wsapi.pas.CaptureCreditCardToAccountResponseType captureCreditCardToAccount(com.broadon.wsapi.pas.CaptureCreditCardToAccountRequestType captureCreditCardToAccountRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.pas.CaptureCreditCardToAccountResponseType ret = impl.captureCreditCardToAccount(captureCreditCardToAccountRequest);
        return ret;
    }

    public com.broadon.wsapi.pas.CaptureECardToAccountResponseType captureECardToAccount(com.broadon.wsapi.pas.CaptureECardToAccountRequestType captureECardToAccountRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.pas.CaptureECardToAccountResponseType ret = impl.captureECardToAccount(captureECardToAccountRequest);
        return ret;
    }

    public com.broadon.wsapi.pas.CheckAccountBalanceResponseType checkAccountBalance(com.broadon.wsapi.pas.CheckAccountBalanceRequestType checkAccountBalanceRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.pas.CheckAccountBalanceResponseType ret = impl.checkAccountBalance(checkAccountBalanceRequest);
        return ret;
    }

    public com.broadon.wsapi.pas.CheckECardBalanceResponseType checkECardBalance(com.broadon.wsapi.pas.CheckECardBalanceRequestType checkECardBalanceRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.pas.CheckECardBalanceResponseType ret = impl.checkECardBalance(checkECardBalanceRequest);
        return ret;
    }

    public com.broadon.wsapi.pas.CreateAccountResponseType createAccount(com.broadon.wsapi.pas.CreateAccountRequestType createAccountRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.pas.CreateAccountResponseType ret = impl.createAccount(createAccountRequest);
        return ret;
    }

    public com.broadon.wsapi.pas.DisableAccountResponseType disableAccount(com.broadon.wsapi.pas.DisableAccountRequestType disableAccountRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.pas.DisableAccountResponseType ret = impl.disableAccount(disableAccountRequest);
        return ret;
    }

    public com.broadon.wsapi.pas.ResetAccountPINResponseType resetAccountPIN(com.broadon.wsapi.pas.ResetAccountPINRequestType resetAccountPINRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.pas.ResetAccountPINResponseType ret = impl.resetAccountPIN(resetAccountPINRequest);
        return ret;
    }

    public com.broadon.wsapi.pas.GetECardTypeResponseType getECardType(com.broadon.wsapi.pas.GetECardTypeRequestType getECardTypeRequest) throws java.rmi.RemoteException
    {
        com.broadon.wsapi.pas.GetECardTypeResponseType ret = impl.getECardType(getECardTypeRequest);
        return ret;
    }
    public void init(Object context) throws ServiceException    
    {   
        if (impl instanceof ServiceLifecycle) {  
            ((ServiceLifecycle) impl).init(context);  
        }  
    }  
    
    public void destroy()  
    {   
        if (impl instanceof ServiceLifecycle) {  
            ((ServiceLifecycle) impl).destroy();  
        }  
    }

}

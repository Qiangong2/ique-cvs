package com.broadon.wsapi.pas;

public interface CapturePaymentToAccountResponseIfc extends CapturePaymentResponseIfc{

    /**
     * Gets the depositedAmount value for this CaptureCreditCardToAccountResponseType.
     * 
     * @return depositedAmount
     */
    public com.broadon.wsapi.pas.MoneyType getDepositedAmount();

    /**
     * Sets the depositedAmount value for this CaptureCreditCardToAccountResponseType.
     * 
     * @param depositedAmount
     */
    public void setDepositedAmount(
            com.broadon.wsapi.pas.MoneyType depositedAmount);

    /**
     * Gets the balance value for this CaptureCreditCardToAccountResponseType.
     * 
     * @return balance
     */
    public com.broadon.wsapi.pas.BalanceType getBalance();

    /**
     * Sets the balance value for this CaptureCreditCardToAccountResponseType.
     * 
     * @param balance
     */
    public void setBalance(com.broadon.wsapi.pas.BalanceType balance);

    public boolean equals(java.lang.Object obj);

    public int hashCode();

}
/**
 * PasTokenType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.broadon.wsapi.pas;

public class PasTokenType  implements java.io.Serializable {
    private byte[] token;
    private com.broadon.wsapi.pas.PaymentMethodType paymentMethod;

    public PasTokenType() {
    }

    public PasTokenType(
           byte[] token,
           com.broadon.wsapi.pas.PaymentMethodType paymentMethod) {
           this.token = token;
           this.paymentMethod = paymentMethod;
    }


    /**
     * Gets the token value for this PasTokenType.
     * 
     * @return token
     */
    public byte[] getToken() {
        return token;
    }


    /**
     * Sets the token value for this PasTokenType.
     * 
     * @param token
     */
    public void setToken(byte[] token) {
        this.token = token;
    }


    /**
     * Gets the paymentMethod value for this PasTokenType.
     * 
     * @return paymentMethod
     */
    public com.broadon.wsapi.pas.PaymentMethodType getPaymentMethod() {
        return paymentMethod;
    }


    /**
     * Sets the paymentMethod value for this PasTokenType.
     * 
     * @param paymentMethod
     */
    public void setPaymentMethod(com.broadon.wsapi.pas.PaymentMethodType paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PasTokenType)) return false;
        PasTokenType other = (PasTokenType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.token==null && other.getToken()==null) || 
             (this.token!=null &&
              java.util.Arrays.equals(this.token, other.getToken()))) &&
            ((this.paymentMethod==null && other.getPaymentMethod()==null) || 
             (this.paymentMethod!=null &&
              this.paymentMethod.equals(other.getPaymentMethod())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getToken() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getToken());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getToken(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPaymentMethod() != null) {
            _hashCode += getPaymentMethod().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PasTokenType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "PasTokenType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("token");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "Token"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentMethod");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "PaymentMethod"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:pas.wsapi.broadon.com", "PaymentMethodType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

<?xml version="1.0" encoding="UTF-8"?>
<schema xmlns="http://www.w3.org/2001/XMLSchema" xmlns:tns="urn:pas.wsapi.broadon.com" xmlns:xsd="http://www.w3.org/2001/XMLSchema" attributeFormDefault="unqualified" elementFormDefault="qualified" targetNamespace="urn:pas.wsapi.broadon.com">

	<xsd:complexType name="AbstractRequestType">
		<xsd:sequence>
			<xsd:element name="Version" type="xsd:string"/>
			<xsd:element name="MessageID" type="xsd:string"/>
			<xsd:element maxOccurs="unbounded" minOccurs="0" name="Properties" type="tns:PropertyType">
				<annotation>
					<documentation>
						Optional field for generic data input from client.
					</documentation>
				</annotation>
			</xsd:element>
		</xsd:sequence>
	</xsd:complexType>

	<xsd:complexType name="AbstractResponseType">
		<xsd:sequence>
			<xsd:element name="Version" type="xsd:string"/>
			<xsd:element name="MessageID" type="xsd:string"/>
			<xsd:element name="TimeStamp" type="tns:TimeStampType"/>
			<xsd:element name="ErrorCode" type="xsd:int"/>
			<xsd:element maxOccurs="1" minOccurs="0" name="ErrorMessage" type="xsd:string"/>
			<xsd:element maxOccurs="unbounded" minOccurs="0" name="Properties" type="tns:PropertyType">
				<annotation>
					<documentation>
						Optional field for generic data output to client.
					</documentation>
				</annotation>
			</xsd:element>
		</xsd:sequence>
	</xsd:complexType>
	
	<xsd:complexType name="PropertyType">
		<annotation>
			<documentation>
				This is a generic data store of (name, value) pairs. It's put into the abstract request/response
				for minot changes that does not have to change the existing API.
			</documentation>
			</annotation>
		<xsd:sequence>
			<xsd:element name="Name" type="xsd:string"/>
			<xsd:element name="Value" type="xsd:string"/>
		</xsd:sequence>
	</xsd:complexType>

	<xsd:simpleType name="TimeStampType">
		<xsd:restriction base="xsd:long"/>
	</xsd:simpleType>
	
	<xsd:complexType name="PasTokenType">
		<xsd:sequence>
			<xsd:element name="Token" type="xsd:base64Binary"/>
			<xsd:element name="PaymentMethod" type="tns:PaymentMethodType"/>
		</xsd:sequence>
	</xsd:complexType>
	
	<xsd:complexType name="MoneyType">
		<xsd:sequence>
			<xsd:element name="currencyType" type="xsd:string"/>
			<xsd:element name="amount" type="xsd:string"/>
		</xsd:sequence>
	</xsd:complexType>

	<xsd:complexType name="BalanceType">
		<annotation>
			<documentation>
				Reserved is the sum of all authorized payment that not yet captured.
				Available = Balance - Reserved.
			</documentation>
		</annotation>
		<xsd:sequence>
			<xsd:element name="Balance" type="tns:MoneyType"/>
			<xsd:element name="Reserved" type="tns:MoneyType"/>
			<xsd:element name="Available" type="tns:MoneyType"/>
		</xsd:sequence>
	</xsd:complexType>
	
	<xsd:simpleType name="AuthorizationStatusType">
		<xsd:restriction base="xsd:string">
			<xsd:enumeration value="Pending"/>
			<xsd:enumeration value="Expired"/>
			<xsd:enumeration value="Void"/>
			<xsd:enumeration value="Completed"/>
		</xsd:restriction>
	</xsd:simpleType>
			
	<xsd:complexType name="AuthorizationStateType">
		<xsd:sequence>
			<xsd:element name="Status" type="tns:AuthorizationStatusType"/>
			<xsd:element name="AuthorizationTime" type="tns:TimeStampType"/>
			<xsd:element name="ExpirationTime" type="tns:TimeStampType"/>
			<xsd:element name="AuthorizedAmount" type="tns:MoneyType"/>
			<xsd:element name="CapturedAmount" type="tns:MoneyType"/>
			<xsd:element name="RefundedAmount" type="tns:MoneyType"/>
			<xsd:element name="IsRejected" type="xsd:boolean"/>
		</xsd:sequence>
	</xsd:complexType>
	
	<xsd:simpleType name="PaymentMethodType">
		<xsd:restriction base="xsd:string">
			<xsd:enumeration value="ECARD"/>
			<xsd:enumeration value="ACCOUNT"/>
			<xsd:enumeration value="CCARD"/>
		</xsd:restriction>
	</xsd:simpleType>
	
	<xsd:complexType name="BalanceAccountType">
		<xsd:sequence>
			<xsd:element name="PIN" type="xsd:string"/>
			<xsd:element name="AccountNumber" type="xsd:string"/>
		</xsd:sequence>
	</xsd:complexType>
	
	<xsd:complexType name="BalanceAccountInfoType">
		<xsd:sequence>
			<xsd:element name="AccountType" type="xsd:string"/>
			<xsd:element minOccurs="0" name="ActivatedTime" type="tns:TimeStampType"/>
			<xsd:element minOccurs="0" name="DeactivateTime" type="tns:TimeStampType"/>
			<xsd:element minOccurs="0" name="RevokedTime" type="tns:TimeStampType"/>
			<xsd:element minOccurs="0" name="LastUpdatedTime" type="tns:TimeStampType"/>
			<xsd:element name="Balance" type="tns:BalanceType"/>
		</xsd:sequence>
	</xsd:complexType>

	<xsd:complexType name="ECardRecordType">
		<annotation>
			<documentation>
				ECardType is 6 digits.
				ECardNumber is a 10 digit sequence number that uniquely identify an ECard, regardless the type.
				HashNumber is a 32 Hex-Decimal string from the 10 digit readom number of the ECard.
				Country code is optional and follows ISO 3166 standard.
			</documentation>
		</annotation>
		<xsd:sequence>
			<xsd:element name="ECardType" type="xsd:string"/>
			<xsd:element name="ECardNumber" type="xsd:string"/>
			<xsd:element name="HashNumber" type="xsd:string"/>
			<xsd:element minOccurs="0" name="CountryCode" type="xsd:string"/>
		</xsd:sequence>
	</xsd:complexType>
	
	<xsd:simpleType name="ECardKindType">
		<xsd:restriction base="xsd:string">
			<xsd:enumeration value="UseOnce"/>
			<xsd:enumeration value="UseMany"/>
			<xsd:enumeration value="Refillable"/>
		</xsd:restriction>
	</xsd:simpleType>

	<xsd:complexType name="ECardInfoType">
		<xsd:sequence>
			<xsd:element name="ECardKind" type="tns:ECardKindType"/>
			<xsd:element minOccurs="0" name="ActivatedTime" type="tns:TimeStampType"/>
			<xsd:element minOccurs="0" name="RevokedTime" type="tns:TimeStampType"/>
			<xsd:element minOccurs="0" name="LastUsedTime" type="tns:TimeStampType"/>
			<xsd:element name="Balance" type="tns:BalanceType"/>
		</xsd:sequence>
	</xsd:complexType>
	
	<xsd:complexType name="ECardTypeCountriesType">
		<xsd:sequence>
			<xsd:element name="ECardType" type="xsd:string"/>
			<xsd:element maxOccurs="unbounded" minOccurs="0" name="Countries" type="xsd:string"/>
		</xsd:sequence>
	</xsd:complexType>
	
	<xsd:complexType name="TransactionRecordType">
		<annotation>
			<documentation>
				The TransactionRecordType is used in reporting account history of a particular account. 
				Each record is a pair of account balance and the action taken. 
				The balance shown is BEFORE the action is taken.
			</documentation>
		</annotation> 
		<xsd:sequence>
			<xsd:element name="Action" type="tns:ActionRecordType"/>
			<xsd:element name="Balance" type="xsd:string"/>
		</xsd:sequence>
	</xsd:complexType>
	
	<xsd:simpleType name="TransactionKindType">
		<annotation>
			<documentation>
				A transaction can be a payment authorization or an account action:
				An authorization is initially created with type "Authorize" and later changed
				to either "Purcase" or "Deposit", depending on it is used to purchase goods or
				deposit to another account -- there two usages cannot be mixed. When used as a
				payment authorization it can associates with multiple TransactionActions, each with
				a type indicating the action performed on this authorization.
				An account action is either "Deposit_to" (associated with exactly one
				TransactionAction of type "Deposit") or "Trans_out" (associated with
				exactly one TransactionAction of type  "Capture"). 
				"Transfer" is defined but currently not used.
			</documentation>
		</annotation> 
		<xsd:restriction base="xsd:string">
			<xsd:enumeration value="Authorize"/>
			<xsd:enumeration value="Purchase"/>
			<xsd:enumeration value="Deposit"/>
			<xsd:enumeration value="Transfer"/>
			<xsd:enumeration value="Deposit_to"/>
			<xsd:enumeration value="Trans_out"/>
		</xsd:restriction>
	</xsd:simpleType>
	<xsd:complexType name="DummyType">
		<xsd:sequence>
			<xsd:element name="dummy" type="tns:TransactionKindType"/>
			</xsd:sequence>
	</xsd:complexType>
	<xsd:complexType name="ActionRecordType">
		<xsd:sequence>
			<xsd:element name="ActionID" type="xsd:string"/>
			<xsd:element name="ActionDate" type="tns:TimeStampType"/>
			<xsd:element name="ActionType" type="tns:ActionKindType"/>
			<xsd:element name="ActionAmount" type="xsd:string"/>
		</xsd:sequence>
	</xsd:complexType>
	
	<xsd:simpleType name="ActionKindType">
		<annotation>
			<documentation>
				An ActionKindType, when used in a transaction action associated with 
				a payment authorization transaction, indicating an action (service) performed 
				using the authorization.
				When used in a transaction action associated with an account, is either 
				"Capture" (withdraw) or "Deposit" (deposit)
			</documentation>
		</annotation> 
		<xsd:restriction base="xsd:string">
			<xsd:enumeration value="Capture"/>
			<xsd:enumeration value="Void"/>
			<xsd:enumeration value="Refund"/>
			<xsd:enumeration value="LastCap"/>
			<xsd:enumeration value="Deposit"/>
		</xsd:restriction>
	</xsd:simpleType>
	
	<xsd:complexType name="AuthorizationRecordType">
		<annotation>
			<documentation>
				The AuthorizationRecordType is used in reporting the history of a particular payment authorization. 
				Each record is a pair of action taken and a authorization state BEFORE the action is taken.
			</documentation>
		</annotation>
		<xsd:sequence>
			<xsd:element minOccurs="0" name="Action" type="tns:ActionRecordType"/>
			<xsd:element name="State" type="tns:AuthorizationStateType"/>
		</xsd:sequence>
	</xsd:complexType>
</schema>

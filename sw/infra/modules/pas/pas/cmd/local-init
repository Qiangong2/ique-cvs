#!/bin/sh

PKG=/opt/broadon/pkgs/pas
DATA=/opt/broadon/data/svcdrv
PROP=BBserver.properties
NOARPCTL=/opt/broadon/pkgs/netutil/bin/noarpctl

sys_net_dev=`/sbin/printconf sys.net.dev`
act_pas=`/sbin/printconf sys.act.pas`
act_pas=${act_pas// /@@}
act_lb=`/sbin/printconf sys.act.lb`
act_lb=${act_lb// /@@}

#
# The activation variable is of the form:
# act_value;key1=value1;key2=value2 ...
#
parse_attributes() {
    # remove the leading part up to the first ';'
    config=${act_pas#*;}
    if [ "$act_pas" != "$config" ]; then
	# we do have attribute to handle

	oldIFS=$IFS
	IFS=';'
	# for each token of the form x=y, replace the '=' by a space and then
	# set the config variable to it.
	for i in $config; do
	    IFS=$oldIFS
	    kv=(`echo ${i/=/' '}`)
	    /sbin/setconf ${kv[0]} "${kv[1]//@@/' '}"
	    IFS=';'
	done
	IFS=$oldIFS
    fi
}

configure() {
    cp $PKG/conf/pas.conf $DATA/conf/httpd
    cp $PKG/conf/pas.xml $DATA/webapps
    mkdir -p $DATA/webapps/pas
    cp $PKG/conf/XMLRequest.properties $DATA/webapps/pas
    cp $PKG/webapps/pas.war $DATA/webapps

    db_url=`/sbin/printconf pas.db.url`
    db_user=`/sbin/printconf pas.db.user`
    db_password=`/sbin/printconf pas.db.password`
    if [ -z "$db_url" ] || [ -z "$db_user" ] || [ -z "$db_password" ]; then
       echo "Missing config. variables"
       exit 1
    fi
    sed -e "s/@@DB_URL@@/$db_url/"		\
        -e "s/@@DB_USER@@/$db_user/"		\
	-e "s/@@DB_PASSWORD@@/$db_password/"	\
	    $PKG/conf/$PROP.tmpl > $DATA/webapps/pas/$PROP
    if [ $? != 0 ]; then
	exit 1
    fi
}

#
# Set the IP address used by this package.
#
set_ip() {
    IP=`host pas | sed -e 's/^.* //'`
    IPd=`echo ${IP} | sed -e 's/^.*\.//'`
    addrinfo=(`ifconfig $sys_net_dev | grep inet | sed -e 's/inet //'`)
    myaddr=${addrinfo[0]/*:/}
    bcast=${addrinfo[1]/*:/}
    netmask=${addrinfo[2]/*:/}

    # turn on "noarp" if I'm served by load-balancer
    if [ "$lb" = "p" ] || [ "$lb" = "s" ]; then
        ${NOARPCTL} add ${IP} ${myaddr}
    fi
    
    ifconfig $sys_net_dev:${IPd} ${IP} netmask ${netmask} broadcast ${bcast}
}

#
# Clear the IP address used by this package.
#
unset_ip() {
    IP=`host pas | sed -e 's/^.* //'`
    IPd=`echo ${IP} | sed -e 's/^.*\.//'`
    ifconfig $sys_net_dev:${IPd} down

    if [ "$lb" = "p" ] || [ "$lb" = "s" ]; then
        ${NOARPCTL} del ${IP}
    fi
}

start() {
    if [ "${act_pas%%;*}" != "1" ]; then
	exit 0
    fi
    # set up IP alpas only if load balancer is not running
    if [ "${act_lb%%;*}" != "1" ] || ( [ "$lb" != "p" ] && [ "$lb" != "s" ] ); then
        set_ip
    fi
    configure
}

stop() {
    if [ "${act_lb%%;*}" != "1" ] || ( [ "$lb" != "p" ] && [ "$lb" != "s" ] ); then
	unset_ip
    fi
}

parse_attributes
lb=`/sbin/printconf pas.lb`

case "$1" in
    start)
	start
	;;
    stop)
	stop
	;;
    *)
	echo $"Usage: $0 {start|stop}"
esac

import java.sql.*;
import java.security.GeneralSecurityException;

import com.broadon.util.Database;
import com.broadon.security.X509;

public class FixHash
{
    static final String getCert =
	"SELECT OWNER_ID, CERTIFICATE FROM SMARTCARDS A, OPERATION_USERS B " +
	"WHERE A.REVOKE_DATE IS NULL AND A.OWNER_ID=B.OPERATOR_ID AND" +
	"      B.OTHER_INFO IS NULL";

    static final String updateUser =
	"UPDATE OPERATION_USERS SET OTHER_INFO=? WHERE OPERATOR_ID=?";

    static public void main(String[] args) {
	try {
	    Database db = new Database();
            Connection conn = db.getConnection();
	    conn.setAutoCommit(false);

	    PreparedStatement ps1 = conn.prepareStatement(getCert);
	    PreparedStatement ps2 = conn.prepareStatement(updateUser);
	    X509 x509 = new X509();

	    try {
		ResultSet rs = ps1.executeQuery();
		while (rs.next()) {
		    long id = rs.getLong(1);
		    String cert = rs.getString(2);
		    try {
			String hash =
			    x509.genUniqueID(x509.readEncodedX509(cert));
			ps2.setString(1, hash);
			ps2.setLong(2, id);
			System.out.println("Adding " + hash + " to operator " + id);
			ps2.addBatch();
		    } catch (GeneralSecurityException e) {
			System.err.println("Ignoring smartcard cert for operator " + id + ": " + e.getMessage());
		    }
		}
		ps1.close();

		ps2.executeBatch();
		conn.commit();
		ps2.close();
	    } finally {
		conn.rollback();
		conn.close();
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    System.exit(1);
	}
    }
}

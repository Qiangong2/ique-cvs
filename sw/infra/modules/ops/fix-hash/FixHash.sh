#!/bin/sh

JAVA_HOME=/opt/broadon/pkgs/jre
CLASSPATH=/opt/broadon/pkgs/javalib/jar/common.jar:.
CONFIG=DB.properties

exec $JAVA_HOME/bin/java -classpath $CLASSPATH -DDB.properties=$CONFIG \
    FixHash $*

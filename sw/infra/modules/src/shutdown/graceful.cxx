// provide graceful shutdown of all BB servers.

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>

#define SERVER_TERMINATING 901		// shutdown status code
#define SERVER_TERMINATED 902
#define POLL_INTERVAL 5
#define DEFAULT_TIMEOUT 180		// 3 mins time out

#define DEFAULT_HTTP_PORT 16900

static unsigned http_port = DEFAULT_HTTP_PORT;
static sockaddr_in local_addr;

static bool
init_local_addr(unsigned short port)
{
    local_addr.sin_family = AF_INET;
    local_addr.sin_port = htons(port);
    memset(&local_addr.sin_zero, 0, sizeof(local_addr.sin_zero));
    return (inet_aton("127.0.0.1", &local_addr.sin_addr) != 0);
}


// open and establish a TCP connection -- auto close
struct TCP_SOCKET {
    int sock;

    TCP_SOCKET(const sockaddr_in* addr) {
	sock = -1;
	int fd = socket (PF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (fd == -1)
	    return;

	int on = 1;
	if (setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, &on, sizeof(on)) != 0) {
	    close(fd);
	    return;
	}

	if (connect (fd, (const sockaddr*) addr, sizeof(sockaddr)) != 0) {
	    close(fd);
	    return;
	}
	sock = fd;
    }

    ~TCP_SOCKET() {
	if (sock >= 0)
	    close(sock);
    }
};
    

class TIMEOUT
{
 private:
    pid_t child_pid;

    void create_timer(int limit) {
	child_pid = fork();
	switch (child_pid) {
	case 0:				// child
	    sleep(limit);
#if DEBUG
	    fprintf(stderr, "killing %d\n", getppid());
#endif    
	    kill(getppid(), SIGTERM);	// kill parent and then exit
	    exit(0);
	    break;
	case -1:
	    exit(1);
	    break;
	default:			// parent
#if DEBUG
	    fprintf(stderr, "parent pid = %d, child pid = %d\n", getpid(), child_pid);
#endif
	    break;
	}
    }

 public:
    
    TIMEOUT(int limit) {
	create_timer(limit);
    }

    ~TIMEOUT() {
	if (child_pid != 0)
	    kill(child_pid, SIGTERM);
    }

    void renew(int limit) {
	kill(child_pid, SIGTERM);
	create_timer(limit);
    }
}; // TIMEOUT


// simple HTTP get request.
// read the entire response into "response" and returns the number of bytes
// read.  Also extract the response code into "response_code".
// return -1 if error
static int
http_get(const char* uri, int& response_code, char* response, int resp_size)
{
    TCP_SOCKET tcp(&local_addr);
    int sock = tcp.sock;
    if (sock < 0)
	return -1;

    char buffer[1024];
    int len = snprintf(buffer, sizeof(buffer), "GET %s HTTP/1.0\r\n\r\n", uri);
    if (send(sock, buffer, len, 0) != len) {
	return -1;
    }

    // send EOF to server
    shutdown(sock, SHUT_WR);
    len = recv(sock, response, resp_size, 0);
    if (len < 0)
	return -1;
    else if (len >= resp_size)
	len = resp_size;		// silently truncate the excess output

    if (strncmp("HTTP", response, 4) != 0)
	return -1;
    const char* p = index(response, ' ');
    if (p == NULL)
	return -1;
    else {
	response_code = strtol(p, NULL, 10);
	return len;
    }
    
} // http_get


static int
count_busy_process(const char* status_str)
{
    int count = 0;
    for (const char* p = status_str; *p != 0 && *p != '\n'; ++p) {
	switch (*p) {
	case 'K':
	case '.':
	case '_':
	    break;
	default:
	    ++count;
	}
    }
#if DEBUG
    fprintf(stderr, "count = %d from %s\n", count, status_str);
#endif
    return count;
} // count_busy_process


#define STATUS_STR "Scoreboard: "

// parse the apache server status output and returns the number of busy child
// processes
static int
server_proc_count(const char* response)
{
    const char* p = strstr(response, STATUS_STR);
    if (p == NULL)
	return -1;
    p += strlen(STATUS_STR);
    return count_busy_process(p);
} // server_proc_count


struct STATUS_MESSAGE
{
    STATUS_MESSAGE() {
	printf("Waiting for existing connections to terminate ...");
	fflush(stdout);
    }

    ~STATUS_MESSAGE() {
	printf(" done\n");
    }
};

int
main(int argc, char* argv[])
{
    if (argc < 2) {
	fprintf(stderr, "Usage: %s server1 server2 ...\n", argv[0]);
	return 1;
    }

    // first timeout to safeguard the case where the local server is stuck
    // or the load balancer never contacts the server.
    TIMEOUT timeout(DEFAULT_TIMEOUT);			

    init_local_addr(http_port);
    char buf[2048];
    int response_size;
    int status;

    STATUS_MESSAGE msg;

    // tell each server to sent negative response to load-balancer's poll
    // message, making it stop directing traffic to them.
    while (true) {
	bool done = true;
	for (int i = 1; i < argc; ++i) {
	    if (argv[i] == NULL)
		continue;
	    char uri[1024];
	    snprintf(uri, sizeof(uri), "/%s/shutdown", argv[i]);
	    response_size = http_get(uri, status, buf, sizeof(buf));
	    if (response_size >= 0 && status == SERVER_TERMINATING) {
		done = false;
#if DEBUG
		fprintf(stderr, "Shutdown %s returns %d\n", argv[i],
			response_size >= 0 ? status : response_size);
#endif
	    } else {
		// 2 cases here, either the server has been successfully
		// shut down, or there is error connecting to it.  In
		// either case, we stop polling this server.
#if DEBUG
		if (response_size >= 0)
		    fprintf(stderr, "%s shutdown completed\n", argv[i]);
		else
		    fprintf(stderr, "Cannot connect to %s\n", argv[i]);
#endif
		argv[i] = NULL;
	    }
	}
	if (! done)
	    sleep(POLL_INTERVAL);
	else
	    break;
    }


    // second timeout is the limit a transaction could take, this is
    // derived from the max. time a client would wait for a transaction to
    // complete. 
    timeout.renew(DEFAULT_TIMEOUT);	

    // wait for existing transactions to terminate

    while (true) {
	response_size = http_get("/server-status?auto", status, buf, sizeof(buf));
	if (response_size < 0)		// serious error, just quit
	    return 1;
	if (response_size == sizeof(buf))
	    --response_size;
	buf[response_size] = 0;		// null terminate for easier parsing

	int count = server_proc_count(buf);
	if (count < 0)
	    return 1;
	else if (count > 1)
	    sleep(POLL_INTERVAL);
	else
	    break;
    }
    return 0;
} // main

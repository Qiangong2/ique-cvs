/*
 * (C) 2006, BroadOn Communications Corp, Inc.,
 * $Id: BMSOPS.sql,v 1.3 2006/08/24 23:27:09 jchang Exp $
 */

CREATE OR REPLACE PACKAGE BODY bmsops
AS
/*
 * (C) 2006, BroadOn Communications Corp, Inc.,
 * $Id: BMSOPS.sql,v 1.3 2006/08/24 23:27:09 jchang Exp $
 */
   CURSOR c1 (pPrice NUMBER, pCurrency VARCHAR2) IS
   SELECT price_id FROM bms.cas_price_tags
   WHERE price=pPrice AND currency=pCurrency;

   PROCEDURE syncCasTitle (ptitleid IN INTEGER, pdate IN DATE) IS
   BEGIN
     UPDATE CDS.CONTENT_TITLES SET approve_date=pdate WHERE TITLE_ID=ptitleid;
     DELETE FROM CAS.CAS_ITEM_PRICINGS WHERE TITLE_ID=ptitleId;
     DELETE FROM CAS.CONTENT_TITLES WHERE TITLE_ID=ptitleId;
     DELETE FROM CAS.CONTENTS WHERE content_id>=ptitleId*POWER(2,32) AND content_id<(ptitleId+1)*POWER(2,32);
     DELETE FROM CAS.CONTENT_TITLE_OBJECTS WHERE TITLE_ID=ptitleId;
     DELETE FROM CAS.CONTENT_TITLE_LOCALES WHERE TITLE_ID=ptitleId;
     DELETE FROM CAS.CONTENT_TITLE_COUNTRIES WHERE TITLE_ID=ptitleId;
     INSERT INTO CAS.CONTENT_TITLES SELECT * FROM CDS.CONTENT_TITLES WHERE TITLE_ID=ptitleid; 
     INSERT INTO CAS.CONTENTS SELECT * FROM CDS.CONTENTS WHERE content_id>=ptitleId*POWER(2,32) AND content_id<(ptitleId+1)*POWER(2,32);
     INSERT INTO CAS.CONTENT_TITLE_OBJECTS SELECT * FROM CDS.CONTENT_TITLE_OBJECTS WHERE TITLE_ID=ptitleid; 
     INSERT INTO CAS.CONTENT_TITLE_LOCALES SELECT * FROM CDS.CONTENT_TITLE_LOCALES WHERE TITLE_ID=ptitleid;
     INSERT INTO CAS.CONTENT_TITLE_COUNTRIES SELECT * FROM CDS.CONTENT_TITLE_COUNTRIES WHERE TITLE_ID=ptitleid; 
     INSERT INTO CAS.CAS_ITEM_PRICINGS SELECT * FROM BMS.CAS_ITEM_PRICINGS WHERE TITLE_ID=ptitleid;
   END;

   PROCEDURE createSeedData IS
   BEGIN
      MERGE INTO SDS.ECARD_TYPES a USING (SELECT 1 ecard_type FROM DUAL) b
      ON (a.ecard_type=b.ecard_type)
      WHEN NOT MATCHED THEN INSERT (ECARD_TYPE, DEFAULT_BALANCE, CURRENCY, IS_USEDONCE, IS_PREPAID, ALLOW_REFILL, LAST_UPDATED, DESCRIPTION, IS_TITLEONLY)
        VALUES (1, 1, 'GUNIT', 1, 1, 0, '24-MAY-06', 'Virtual Console Game Ticket', 1);
      MERGE INTO CDS.CONTENT_CACHE a USING (SELECT 'TESTCASETMDCACHE' content_checksum FROM DUAL) b
      ON (a.content_checksum=b.content_checksum)
      WHEN NOT MATCHED THEN INSERT (CONTENT_CHECKSUM, CONTENT_OBJECT_TYPE, CONTENT_SIZE, CONTENT_OBJECT_NAME, CONTENT_OBJECT_VERSION, CREATE_DATE)
        VALUES ('TESTCASETMDCACHE','TMD',520,'Test Title TMD',0,TRUNC(SYSDATE));
      MERGE INTO CDS.CONTENT_CACHE a USING (SELECT 'TESTCASEGAMECACHE' content_checksum FROM DUAL) b
      ON (a.content_checksum=b.content_checksum)
      WHEN NOT MATCHED THEN INSERT (CONTENT_CHECKSUM, CONTENT_OBJECT_TYPE, CONTENT_SIZE, CONTENT_OBJECT_NAME, CONTENT_OBJECT_VERSION, CREATE_DATE)
        VALUES ('TESTCASEGAMECACHE','GAME',1048576,'Test Title Game',0,TRUNC(SYSDATE));
      MERGE INTO CDS.CONTENT_CACHE a USING (SELECT 'ETKM Place Holder' content_checksum FROM DUAL) b
      ON (a.content_checksum=b.content_checksum)
      WHEN NOT MATCHED THEN INSERT (CONTENT_CHECKSUM, CONTENT_OBJECT_TYPE, CONTENT_SIZE, CONTENT_OBJECT_NAME, CONTENT_OBJECT_VERSION, CREATE_DATE)
        VALUES ('ETKM Place Holder','ETKM',0,'ETKM_Place_Holder',0,TRUNC(SYSDATE));
      MERGE INTO COUNTRIES a USING (SELECT 1 country_id FROM DUAL) b ON (a.country_id=b.country_id)
      WHEN NOT MATCHED THEN INSERT (COUNTRY_ID, COUNTRY, REGION, DEFAULT_LOCALE, DEFAULT_CURRENCY, PRICING_COUNTRY_ID, RATING_SYSTEM, SECOND_SYSTEM, DEFAULT_TIMEZONE)
        VALUES (1,'JPT','TEST','ja_JP','JPY',1,'CERO',NULL,'Asia/Tokyo');
      MERGE INTO COUNTRIES a USING (SELECT 2 country_id FROM DUAL) b ON (a.country_id=b.country_id)
      WHEN NOT MATCHED THEN INSERT (COUNTRY_ID, COUNTRY, REGION, DEFAULT_LOCALE, DEFAULT_CURRENCY, PRICING_COUNTRY_ID, RATING_SYSTEM, SECOND_SYSTEM, DEFAULT_TIMEZONE)
        VALUES (2,'UST','TEST','en_US','USD',2,'ESRB',NULL,'America/Los_Angeles');
      MERGE INTO COUNTRY_LOCALES a USING (SELECT 1 country_id, 'ja' lang FROM DUAL) b ON (a.country_id=b.country_id AND a.language_code=b.lang)
      WHEN NOT MATCHED THEN INSERT VALUES (1,'ja','ja_JP');
      MERGE INTO COUNTRY_LOCALES a USING (SELECT 1 country_id, 'en' lang FROM DUAL) b ON (a.country_id=b.country_id AND a.language_code=b.lang)
      WHEN NOT MATCHED THEN INSERT VALUES (1,'en','en_US');
      MERGE INTO COUNTRY_LOCALES a USING (SELECT 2 country_id, 'sp' lang FROM DUAL) b ON (a.country_id=b.country_id AND a.language_code=b.lang)
      WHEN NOT MATCHED THEN INSERT VALUES (2,'sp','sp_SP');
      MERGE INTO COUNTRY_LOCALES a USING (SELECT 2 country_id, 'en' lang FROM DUAL) b ON (a.country_id=b.country_id AND a.language_code=b.lang)
      WHEN NOT MATCHED THEN INSERT VALUES (2,'en','en_US');
   END;

   PROCEDURE removePerfData IS
   BEGIN
     DELETE FROM COUNTRY_LOCALES WHERE COUNTRY_ID IN (1,2);
     DELETE FROM COUNTRIES WHERE COUNTRY_ID IN (1,2);
     INSERT INTO INCOMING (tablename) SELECT DISTINCT pas_auth_id FROM ECS.ECS_TRANSACTIONS WHERE ITEM_ID BETWEEN 10000 AND 29999;
     DELETE FROM ECS.ECS_TRANSACTIONS WHERE ITEM_ID BETWEEN 10000 AND 29999;
     DELETE FROM PAS.PAS_TRANSACTION_ACTIONS WHERE PAS_TRANS_ID IN (SELECT tablename FROM incoming );
     DELETE FROM PAS.PAS_TRANSACTIONS WHERE PAS_TRANS_ID IN (SELECT tablename FROM incoming );
	 DELETE FROM PAS.PAS_BANK_ACCOUNTS WHERE ACCOUNT_ID>=888000000 and ACCOUNT_ID<=889000000;
     DELETE FROM ecs.etickets WHERE SUBSTR(bccutil.dec2hex(content_id,24),1,12) = '000100000009';
     DELETE FROM BMS.CAS_ITEM_PRICINGS WHERE ITEM_ID BETWEEN 10000 AND 29999;
     DELETE FROM CDS.CONTENT_TITLE_LOCALES WHERE SUBSTR(bccutil.dec2hex(title_id,16),1,12) = '000100000009';
     DELETE FROM CDS.CONTENT_TITLE_COUNTRIES WHERE SUBSTR(bccutil.dec2hex(title_id,16),1,12) = '000100000009';
     DELETE FROM CDS.CONTENT_TITLE_OBJECTS WHERE SUBSTR(bccutil.dec2hex(title_id,16),1,12) = '000100000009';
     DELETE FROM ets.content_eticket_metadata WHERE SUBSTR(bccutil.dec2hex(content_id,24),1,12) = '000100000009';
     DELETE FROM CDS.CONTENTS WHERE SUBSTR(bccutil.dec2hex(content_id,24),1,12) = '000100000009';
     DELETE FROM CDS.CONTENT_TITLES WHERE SUBSTR(bccutil.dec2hex(title_id,16),1,12) = '000100000009';
     DELETE FROM CAS.CAS_ITEM_PRICINGS WHERE ITEM_ID BETWEEN 10000 AND 29999;
     DELETE FROM CAS.CONTENT_TITLE_LOCALES WHERE SUBSTR(bccutil.dec2hex(title_id,16),1,12) = '000100000009';
     DELETE FROM CAS.CONTENT_TITLE_COUNTRIES WHERE SUBSTR(bccutil.dec2hex(title_id,16),1,12) = '000100000009';
     DELETE FROM CAS.CONTENT_TITLE_OBJECTS WHERE SUBSTR(bccutil.dec2hex(title_id,16),1,12) = '000100000009';
     DELETE FROM CAS.CONTENTS WHERE SUBSTR(bccutil.dec2hex(content_id,24),1,12) = '000100000009';
     DELETE FROM CAS.CONTENT_TITLES WHERE SUBSTR(bccutil.dec2hex(title_id,16),1,12) = '000100000009';
   END;
   
   PROCEDURE removeTestData IS
   BEGIN
     DELETE FROM COUNTRY_LOCALES WHERE COUNTRY_ID IN (1,2);
     DELETE FROM COUNTRIES WHERE COUNTRY_ID IN (1,2);
     DELETE FROM BMS.CAS_ITEM_PRICINGS WHERE SUBSTR(bccutil.dec2hex(title_id,16),1,13) = '0001000000081';
     DELETE FROM CDS.CONTENT_TITLE_LOCALES WHERE SUBSTR(bccutil.dec2hex(title_id,16),1,13) = '0001000000081';
     DELETE FROM CDS.CONTENT_TITLE_COUNTRIES WHERE SUBSTR(bccutil.dec2hex(title_id,16),1,13) = '0001000000081';
     DELETE FROM CAS.CAS_ITEM_PRICINGS WHERE SUBSTR(bccutil.dec2hex(title_id,16),1,13) = '0001000000081';
     DELETE FROM CAS.CONTENT_TITLE_LOCALES WHERE SUBSTR(bccutil.dec2hex(title_id,16),1,13) = '0001000000081';
     DELETE FROM CAS.CONTENT_TITLE_COUNTRIES WHERE SUBSTR(bccutil.dec2hex(title_id,16),1,13) = '0001000000081';
   END;
      
   PROCEDURE createPerfData IS
     idx INT :=1;
     ptitleid INT;
     petkmid INT;
     ptmdid INT;
     pcontentid INT;
     ptitle VARCHAR2(64);
     pdate DATE;
     ptime DATE;
     tstart INT:=1;
     tend INT:=1000;
     tPriceId INT;
     pPriceId INT;
     pPrice NUMBER:=3000;
     tPrice NUMBER:=2000;
     pCurrency VARCHAR2(10):='POINTS';
     BEGIN
     removePerfData;
     createSeedData;
	 FOR idx IN 0..1000000-1 LOOP
	    insert into pas_bank_accounts values(888000000+idx, 'VCPOINTS', TRUNC(SYSDATE)-30, null, null, 1000000000, 0, 'POINTS', TRUNC(SYSDATE)-30, 30000);
	 END LOOP;	
 
     OPEN c1(tPrice,pCurrency);
     FETCH c1 INTO tPriceId;
     CLOSE c1;
     IF tPriceId IS NULL THEN
       SELECT MAX(price_id)+1 INTO tPriceId FROM bms.cas_price_tags;
       INSERT INTO bms.cas_price_tags VALUES (tPriceId,tPrice,pCurrency);
     END IF;
     OPEN c1(pPrice,pCurrency);
     FETCH c1 INTO pPriceId;
     CLOSE c1;
     IF pPriceId IS NULL THEN
       SELECT MAX(price_id)+1 INTO pPriceId FROM bms.cas_price_tags;
       INSERT INTO bms.cas_price_tags VALUES (pPriceId,pPrice,pCurrency);
     END IF;
     pdate:=TRUNC(SYSDATE);
     FOR idx IN tstart..tend LOOP
     ptitle:='000100000009'||bccutil.dec2hex(idx,4);
     ptitleid:=bccutil.hex2dec(ptitle);
     petkmid:=bccutil.hex2dec(ptitle||'FFFFFFFF');
     ptmdid:=bccutil.hex2dec(ptitle||'00000001');
     pcontentid:=bccutil.hex2dec(ptitle||'00000002');
     ptime:=sys_extract_utc(SYSDATE);
     -- cleanup
     --content_id>=titleId*POWER(2,32) AND content_id<(titleId+1)*POWER(2,32)
     -- BITAND(FLOOR(CONTENT_ID/POWER(2,32)),POWER(2,96)-1) and ;

     INSERT INTO CDS.CONTENT_TITLES (TITLE_ID, TITLE, TITLE_TYPE, CATEGORY, PUBLISHER, DEVELOPER,
      DESCRIPTION, PUBLISH_DATE, RELEASE_DATE,
      PLATFORM, CHAIN_ID, DEFAULT_LOCALE, PRODUCT_CODE) VALUES (
      ptitleid,'Test Title Name '||ptitle,'GAME',
      DECODE(MOD(idx,8),0,'ACTION',1,'RPG',2,'FIGHTING',3,'STRATEGY',4,'RACING',5,'SPORTS',6,'ADVENTURE','OTHERS'),
      DECODE(MOD(idx,8),0,'NINTENDO',1,'NINTENDO',2,'EA',3,'SEGA',4,'KONAMI',5,'CAPCOM',6,'BROADON','IQUE'),
      DECODE(MOD(idx,8),0,'NINTENDO',1,'NINTENDO',2,'EA',3,'SEGA',4,'KONAMI',5,'CAPCOM',6,'BROADON','IQUE'),
      'Description for Test Title '||ptitle,pdate,TO_CHAR(pdate,'YYYYMMDD'),
      DECODE(MOD(idx,8),0,'N64',1,'SNES',2,'N64',3,'SNES',4,'N64',5,'N64',6,'NES','MEGA-DRIVE'),
      3,'en_US','PERF-'||bccutil.dec2hex(idx,4)); 
     INSERT INTO CDS.CONTENTS (content_id,publish_date,content_checksum) VALUES (petkmid,pdate,'ETKM Place Holder');
     INSERT INTO CDS.CONTENTS (content_id,publish_date,content_checksum) VALUES (ptmdid,pdate,'TESTCASETMDCACHE');
     INSERT INTO CDS.CONTENTS (content_id,publish_date,content_checksum) VALUES (pcontentid,pdate,'TESTCASEGAMECACHE');
     INSERT INTO CDS.CONTENT_TITLE_OBJECTS (TITLE_ID, CONTENT_ID, CREATE_DATE) VALUES (ptitleid,petkmid,ptime);
     INSERT INTO CDS.CONTENT_TITLE_OBJECTS (TITLE_ID, CONTENT_ID, CREATE_DATE) VALUES (ptitleid,ptmdid,ptime);
     INSERT INTO CDS.CONTENT_TITLE_OBJECTS (TITLE_ID, CONTENT_ID, CREATE_DATE) VALUES (ptitleid,pcontentid,ptime);
     INSERT INTO ets.content_eticket_metadata (CONTENT_ID, CHAIN_ID, LAST_UPDATED, ETICKET_METADATA, ATTRIBITS)
     VALUES (petkmid,3,SYSDATE,utl_raw.cast_to_raw('DUMMYETKM0123456'),0);
     INSERT INTO CDS.CONTENT_TITLE_LOCALES (TITLE_ID, LOCALE, TITLE, DESCRIPTION, ACTIVATE_DATE)
     VALUES (ptitleid,'en_US','Test Title Name '||ptitle,'Description for Test Title '||ptitle,ptime);
     INSERT INTO CDS.CONTENT_TITLE_COUNTRIES (TITLE_ID, COUNTRY_ID, RATING_SYSTEM, RATING_VALUE, RATING_AGE, RECOMMEND_DATE, NEW_SINCE, POPULARITY, ACTIVATE_DATE)
     VALUES (ptitleid,1,'CERO','A',0,pdate,pdate,DECODE(MOD(idx,3),0,10,1,9,8),ptime);
     INSERT INTO CDS.CONTENT_TITLE_COUNTRIES (TITLE_ID, COUNTRY_ID, RATING_SYSTEM, RATING_VALUE, RATING_AGE, RECOMMEND_DATE, NEW_SINCE, POPULARITY, ACTIVATE_DATE)
     VALUES (ptitleid,2,'ESRB','E',6,pdate,pdate,DECODE(MOD(idx,3),0,10,1,9,8),ptime);
     INSERT INTO BMS.CAS_ITEM_PRICINGS (ITEM_ID, TITLE_ID, DEVICE_TYPE_ID, COUNTRY_ID, PRICE_ID, PURCHASE_START_DATE,RTYPE, LICENSE_TYPE)
     VALUES (10000+idx,ptitleid,1,1,tPriceId,pdate,'PR','PERMANENT');
     INSERT INTO BMS.CAS_ITEM_PRICINGS (ITEM_ID, TITLE_ID, DEVICE_TYPE_ID, COUNTRY_ID, PRICE_ID, PURCHASE_START_DATE,RTYPE, LICENSE_TYPE)
     VALUES (20000+idx,ptitleid,1,2,pPriceId,pdate,'PR','PERMANENT');
	 syncCasTitle(ptitleid,pdate);
     END LOOP;
   END;
   
   PROCEDURE createTestData IS
     idx INT :=1;
     ptitleid INT;
     ptitle VARCHAR2(64);
     pdate DATE;
     ptime DATE;
     tstart INT:=0;
     tend INT:=9;
     tPriceId INT;
     pPriceId INT;
     pPrice NUMBER:=1;
     tPrice NUMBER:=0;
     pCurrency VARCHAR2(10):='POINTS';
   BEGIN
     removeTestData;
     createSeedData;
     OPEN c1(tPrice,pCurrency);
     FETCH c1 INTO tPriceId;
     CLOSE c1;
     IF tPriceId IS NULL THEN
       SELECT MAX(price_id)+1 INTO tPriceId FROM bms.cas_price_tags;
       INSERT INTO bms.cas_price_tags VALUES (tPriceId,tPrice,pCurrency);
     END IF;
     OPEN c1(pPrice,pCurrency);
     FETCH c1 INTO pPriceId;
     CLOSE c1;
     IF pPriceId IS NULL THEN
       SELECT MAX(price_id)+1 INTO pPriceId FROM bms.cas_price_tags;
       INSERT INTO bms.cas_price_tags VALUES (pPriceId,pPrice,pCurrency);
     END IF;
     pdate:=TRUNC(SYSDATE);
     FOR idx IN tstart..tend LOOP
     ptitle:='0001000000081'||bccutil.dec2hex(idx,3);
     ptitleid:=bccutil.hex2dec(ptitle);
     ptime:=sys_extract_utc(SYSDATE);
     UPDATE CDS.CONTENT_TITLES SET
     CATEGORY=DECODE(MOD(idx,8),0,'ACTION',1,'RPG',2,'FIGHTING',3,'STRATEGY',4,'RACING',5,'SPORTS',6,'ADVENTURE','OTHERS'),
     publisher=DECODE(MOD(idx,8),0,'NINTENDO',1,'NINTENDO',2,'EA',3,'SEGA',4,'KONAMI',5,'CAPCOM',6,'BROADON','IQUE'),
     developer=DECODE(MOD(idx,8),0,'NINTENDO',1,'NINTENDO',2,'EA',3,'SEGA',4,'KONAMI',5,'CAPCOM',6,'BROADON','IQUE'),
     description='Description for Test Title '||ptitle,
     publish_date=pdate,release_date=TO_CHAR(pdate,'YYYYMMDD'),
     platform=DECODE(MOD(idx,8),0,'N64',1,'SNES',2,'N64',3,'SNES',4,'N64',5,'N64',6,'NES','MEGA-DRIVE'),
     default_locale='en_US'
      WHERE TITLE_ID=ptitleid; INSERT INTO CDS.CONTENT_TITLE_LOCALES (TITLE_ID, LOCALE, TITLE, DESCRIPTION, ACTIVATE_DATE)
     VALUES (ptitleid,'en_US','Test Title Name '||ptitle,'Description for Test Title '||ptitle,ptime);
     INSERT INTO CDS.CONTENT_TITLE_COUNTRIES (TITLE_ID, COUNTRY_ID, RATING_SYSTEM, RATING_VALUE, RATING_AGE, RECOMMEND_DATE, NEW_SINCE, POPULARITY, ACTIVATE_DATE)
     VALUES (ptitleid,2,'ESRB','E',6,pdate,pdate,DECODE(MOD(idx,3),0,10,1,9,8),ptime);
     INSERT INTO BMS.CAS_ITEM_PRICINGS (ITEM_ID, TITLE_ID, DEVICE_TYPE_ID, COUNTRY_ID, PRICE_ID, PURCHASE_START_DATE,RTYPE, LICENSE_TYPE)
     VALUES (32000+idx*10,ptitleid,1,2,pPriceId,pdate,'PR','PERMANENT');
     INSERT INTO BMS.CAS_ITEM_PRICINGS (ITEM_ID, TITLE_ID, DEVICE_TYPE_ID, COUNTRY_ID, PRICE_ID, PURCHASE_START_DATE,RTYPE, LICENSE_TYPE, LIMITS)
     VALUES (32000+idx*10+1,ptitleid,1,2,tPriceId,pdate,'TR','TRIAL',600);
	 syncCasTitle(ptitleid,pdate);
     END LOOP;
  END;

END bmsops;
/


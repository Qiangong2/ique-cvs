/*
 * (C) 2006, BroadOn Communications Corp, Inc.,
 * $Id: BMSSCHEMA.sql,v 1.15 2006/08/24 22:00:42 jchang Exp $
 */
ALTER SESSION SET CURRENT_SCHEMA = SYSTEM
/
CREATE TABLESPACE SHARED DATAFILE  SIZE 1M AUTOEXTEND ON NEXT 10M MAXSIZE UNLIMITED
LOGGING
ONLINE
PERMANENT
EXTENT MANAGEMENT LOCAL AUTOALLOCATE
BLOCKSIZE 8K
SEGMENT SPACE MANAGEMENT AUTO
/
CREATE TABLESPACE OPLOGD DATAFILE  SIZE 1M AUTOEXTEND ON NEXT 10M MAXSIZE UNLIMITED
LOGGING
ONLINE
PERMANENT
EXTENT MANAGEMENT LOCAL AUTOALLOCATE
BLOCKSIZE 8K
SEGMENT SPACE MANAGEMENT AUTO
/
CREATE USER bms IDENTIFIED BY &&BMS_PASSWORD DEFAULT TABLESPACE SHARED TEMPORARY TABLESPACE TEMP PROFILE DEFAULT ACCOUNT UNLOCK ;
  GRANT CREATE SNAPSHOT TO bms;
  GRANT ALTER SESSION TO bms;
  GRANT CREATE SESSION TO bms;
  GRANT CREATE ANY DIRECTORY TO bms;
  REVOKE UNLIMITED TABLESPACE FROM bms;
ALTER USER bms DEFAULT TABLESPACE SHARED QUOTA UNLIMITED ON OPLOGD QUOTA UNLIMITED ON SHARED;
ALTER USER bms IDENTIFIED BY &&BMS_PASSWORD;
GRANT ALTER ANY SNAPSHOT TO bms;

CALL dbms_java.grant_permission(
      'BMS',
      'SYS:oracle.aurora.security.JServerPermission',
      'Memory.Call',
      '');

ALTER SESSION SET CURRENT_SCHEMA = SDS
/
GRANT SELECT,UPDATE,DELETE,INSERT,REFERENCES ON countries TO BMS WITH GRANT OPTION;
GRANT SELECT,UPDATE,DELETE,INSERT,REFERENCES ON country_locales TO BMS WITH GRANT OPTION;
GRANT SELECT,UPDATE,DELETE,INSERT,REFERENCES ON ecard_types TO BMS WITH GRANT OPTION;
GRANT SELECT,UPDATE,DELETE,INSERT,REFERENCES ON ecard_countries TO BMS WITH GRANT OPTION;
GRANT SELECT,UPDATE,DELETE,INSERT,REFERENCES ON device_types TO BMS WITH GRANT OPTION;
GRANT SELECT,UPDATE,DELETE,INSERT,REFERENCES ON device_title_types TO BMS WITH GRANT OPTION;
GRANT SELECT,UPDATE,DELETE,INSERT,REFERENCES ON attribute_lookups TO BMS WITH GRANT OPTION;

ALTER SESSION SET CURRENT_SCHEMA = KMS
/
GRANT SELECT,UPDATE,DELETE,INSERT,REFERENCES ON certificates TO BMS WITH GRANT OPTION;
GRANT SELECT,UPDATE,DELETE,INSERT,REFERENCES ON certificate_chains TO BMS WITH GRANT OPTION;
GRANT SELECT,REFERENCES ON eticket_crl TO BMS WITH GRANT OPTION;
GRANT SELECT,UPDATE,DELETE,INSERT,REFERENCES ON current_crls TO BMS WITH GRANT OPTION;
GRANT SELECT,UPDATE,DELETE,INSERT,REFERENCES ON CLIENT_HSM_CERTS TO BMS WITH GRANT OPTION;
GRANT SELECT,UPDATE,DELETE,INSERT,REFERENCES ON MODULE_ACCESS_RIGHTS TO BMS WITH GRANT OPTION;

ALTER SESSION SET CURRENT_SCHEMA = IAS
/
GRANT SELECT,REFERENCES,INSERT,UPDATE,DELETE ON IAS_DEVICES TO BMS WITH GRANT OPTION;
GRANT SELECT,REFERENCES,INSERT,UPDATE,DELETE ON IAS_DEVICE_TRANSFERS TO BMS WITH GRANT OPTION;
GRANT SELECT,REFERENCES,INSERT,UPDATE,DELETE ON IAS_ACCOUNTS TO BMS WITH GRANT OPTION;
GRANT SELECT,REFERENCES,INSERT,UPDATE,DELETE ON IAS_ACCOUNT_DEVICES TO BMS WITH GRANT OPTION;
GRANT SELECT,REFERENCES,INSERT,UPDATE,DELETE ON IAS_ACCOUNT_NOTES TO BMS WITH GRANT OPTION;
GRANT SELECT,REFERENCES,INSERT,UPDATE,DELETE ON IAS_ACCOUNT_ATTRIBUTES TO BMS WITH GRANT OPTION;
GRANT SELECT,REFERENCES,INSERT,UPDATE,DELETE ON IAS_ATTRIBUTES TO BMS WITH GRANT OPTION;
GRANT SELECT,REFERENCES,INSERT,UPDATE,DELETE ON IAS_ATTRIBUTE_PROFILES TO BMS WITH GRANT OPTION;
GRANT SELECT,REFERENCES,INSERT,UPDATE,DELETE ON IAS_EXTERNAL_ACCOUNTS TO BMS WITH GRANT OPTION;
GRANT SELECT,REFERENCES,INSERT,UPDATE,DELETE ON IAS_DEVICE_SUBSCRIPTIONS TO BMS WITH GRANT OPTION;
GRANT SELECT,REFERENCES,INSERT,UPDATE,DELETE ON IAS_AUTHENTICATION_TOKENS TO BMS WITH GRANT OPTION;

ALTER SESSION SET CURRENT_SCHEMA = ETS
/
GRANT SELECT,REFERENCES,INSERT,UPDATE,DELETE ON ets.content_eticket_metadata TO BMS WITH GRANT OPTION;

ALTER SESSION SET CURRENT_SCHEMA = CDS
/
GRANT SELECT,REFERENCES,INSERT,UPDATE,DELETE ON CONTENTS TO BMS WITH GRANT OPTION;
GRANT SELECT,REFERENCES,INSERT,UPDATE,DELETE ON CONTENT_CACHE TO BMS WITH GRANT OPTION;
GRANT SELECT,REFERENCES,INSERT,UPDATE,DELETE ON CONTENT_TITLES TO BMS WITH GRANT OPTION;
GRANT SELECT,REFERENCES,INSERT,UPDATE,DELETE ON CONTENT_TITLE_OBJECTS TO BMS WITH GRANT OPTION;
GRANT SELECT,REFERENCES,INSERT,UPDATE,DELETE ON CONTENT_TITLE_REVIEWS TO BMS WITH GRANT OPTION;
GRANT SELECT,REFERENCES,INSERT,UPDATE,DELETE ON CONTENT_TITLE_LOCALES TO BMS WITH GRANT OPTION;
GRANT SELECT,REFERENCES,INSERT,UPDATE,DELETE ON CONTENT_DOWNLOAD_LOGS TO BMS WITH GRANT OPTION;
GRANT SELECT,REFERENCES ON CONTENT_TITLE_COUNTRY_LOCALES TO BMS WITH GRANT OPTION;
GRANT SELECT,REFERENCES ON CONTENT_TITLE_COUNTRY_RELEASES TO BMS WITH GRANT OPTION;
GRANT SELECT,REFERENCES,INSERT,UPDATE,DELETE ON CONTENT_TITLE_COUNTRIES TO BMS WITH GRANT OPTION;
GRANT SELECT,REFERENCES,INSERT,UPDATE,DELETE ON content_title_last_version TO BMS WITH GRANT OPTION;
GRANT SELECT,REFERENCES,INSERT,UPDATE,DELETE ON latest_content_title_objects TO BMS WITH GRANT OPTION;
GRANT SELECT,REFERENCES,INSERT,UPDATE,DELETE ON CONTENT_OBJECT_TYPES TO BMS WITH GRANT OPTION;
GRANT SELECT,REFERENCES ON CONTENT_OBJECTS_V TO BMS WITH GRANT OPTION;
GRANT SELECT,REFERENCES ON content_title_catalog TO bms WITH GRANT OPTION;

ALTER SESSION SET CURRENT_SCHEMA = ECS
/
GRANT SELECT,REFERENCES,INSERT,UPDATE,DELETE ON ETICKETS TO BMS WITH GRANT OPTION;
GRANT SELECT,REFERENCES,INSERT,UPDATE,DELETE ON ETICKET_TRANSFERS TO BMS WITH GRANT OPTION;
GRANT SELECT,REFERENCES,INSERT,UPDATE,DELETE ON ECS_TRANSACTIONS TO BMS WITH GRANT OPTION;
GRANT SELECT,REFERENCES,INSERT,UPDATE,DELETE ON ECS_TRANSACTION_NOTES TO BMS WITH GRANT OPTION;
GRANT SELECT,REFERENCES,INSERT,UPDATE,DELETE ON ECS_DEVICE_TITLE_CHECKOUTS TO BMS WITH GRANT OPTION;
GRANT SELECT,REFERENCES ON ECS_TRANSACTION_DETAILS TO BMS WITH GRANT OPTION;

ALTER SESSION SET CURRENT_SCHEMA = PAS;
GRANT SELECT,REFERENCES,INSERT,UPDATE,DELETE,ON COMMIT REFRESH,FLASHBACK,QUERY REWRITE ON ECARD_BATCHES TO BMS WITH GRANT OPTION;
GRANT SELECT,REFERENCES,INSERT,UPDATE,DELETE,ON COMMIT REFRESH,FLASHBACK,QUERY REWRITE ON ECARDS TO BMS WITH GRANT OPTION;
GRANT SELECT,REFERENCES,INSERT,UPDATE,DELETE ON PAS_TRANSACTIONS TO BMS WITH GRANT OPTION;
GRANT SELECT,REFERENCES,INSERT,UPDATE,DELETE ON PAS_BANK_ACCOUNTS TO BMS WITH GRANT OPTION;
GRANT SELECT,REFERENCES,INSERT,UPDATE,DELETE ON PAS_TRANSACTION_ACTIONS TO BMS WITH GRANT OPTION;
GRANT SELECT,REFERENCES ON ECARD_BATCH_DETAILS TO BMS WITH GRANT OPTION;
GRANT SELECT,REFERENCES ON ECARD_DETAILS TO BMS WITH GRANT OPTION;

ALTER SESSION SET CURRENT_SCHEMA = BMS
/
CREATE SYNONYM CONTENTS FOR cds.CONTENTS;
CREATE SYNONYM CONTENT_CACHE FOR cds.CONTENT_CACHE;
CREATE SYNONYM CONTENT_OBJECTS FOR cds.CONTENT_OBJECTS;
CREATE SYNONYM CONTENT_TITLES FOR cds.CONTENT_TITLES;
CREATE SYNONYM CONTENT_TITLE_OBJECTS FOR cds.CONTENT_TITLE_OBJECTS;
CREATE SYNONYM CONTENT_TITLE_REVIEWS FOR cds.CONTENT_TITLE_REVIEWS;
CREATE SYNONYM CONTENT_TITLE_COUNTRIES FOR cds.CONTENT_TITLE_COUNTRIES;
CREATE SYNONYM CONTENT_DOWNLOAD_LOGS FOR cds.CONTENT_DOWNLOAD_LOGS;
CREATE SYNONYM CONTENT_TITLE_COUNTRY_LOCALES FOR cds.CONTENT_TITLE_COUNTRY_LOCALES;
CREATE SYNONYM CONTENT_TITLE_COUNTRY_RELEASES FOR cds.CONTENT_TITLE_COUNTRY_RELEASES;
CREATE SYNONYM CONTENT_TITLE_LOCALES FOR cds.CONTENT_TITLE_LOCALES;
CREATE SYNONYM CONTENT_TITLE_CATALOG FOR cds.CONTENT_TITLE_CATALOG;
CREATE SYNONYM content_title_last_version FOR cds.content_title_last_version;
CREATE SYNONYM latest_content_title_objects FOR cds.latest_content_title_objects;
CREATE SYNONYM CONTENT_OBJECT_TYPE FOR CDS.CONTENT_OBJECT_TYPES;

CREATE SYNONYM content_eticket_metadata FOR ets.content_eticket_metadata;

CREATE SYNONYM ECS_DEVICE_TITLE_CHECKOUTS FOR ECS.ECS_DEVICE_TITLE_CHECKOUTS;
DROP SYNONYM ECS_ITEM_PRICINGS;
DROP SYNONYM ECS_COUNTRY_ITEM_PRICINGS;
DROP SYNONYM ECS_PRICE_TAGS;
CREATE SYNONYM ECS_TRANSACTIONS FOR ECS.ECS_TRANSACTIONS;
CREATE SYNONYM ECS_TRANSACTION_NOTES FOR ECS.ECS_TRANSACTION_NOTES;
CREATE SYNONYM ECS_TRANSACTION_DETAILS FOR ECS.ECS_TRANSACTION_DETAILS;
CREATE SYNONYM ETICKETS FOR ECS.ETICKETS;
CREATE SYNONYM ETICKET_TRANSFERS FOR ECS.ETICKET_TRANSFERS;

CREATE SYNONYM ECARDS FOR PAS.ECARDS;
CREATE SYNONYM ECARD_BATCHES FOR PAS.ECARD_BATCHES;
CREATE SYNONYM PAS_TRANSACTIONS FOR PAS.PAS_TRANSACTIONS;
CREATE SYNONYM PAS_BANK_ACCOUNTS FOR PAS.PAS_BANK_ACCOUNTS;
CREATE SYNONYM PAS_TRANSACTION_ACTIONS FOR PAS.PAS_TRANSACTION_ACTIONS;
CREATE SYNONYM ECARD_DETAILS FOR PAS.ECARD_DETAILS;
CREATE SYNONYM ECARD_BATCH_DETAILS FOR PAS.ECARD_BATCH_DETAILS;
--CREATE SYNONYM PAS_EXT_ACTIONS FOR PAS.PAS_EXT_ACTIONS;
--CREATE SYNONYM PAS_EXT_AUTHORIZATIONS FOR PAS.PAS_EXT_AUTHORIZATIONS;

CREATE SYNONYM IAS_DEVICES FOR IAS.IAS_DEVICES;
CREATE SYNONYM IAS_DEVICE_TRANSFERS FOR IAS.IAS_DEVICE_TRANSFERS;
CREATE SYNONYM IAS_ACCOUNTS FOR IAS.IAS_ACCOUNTS;
CREATE SYNONYM IAS_ACCOUNT_DEVICES FOR IAS.IAS_ACCOUNT_DEVICES;
CREATE SYNONYM IAS_ACCOUNT_NOTES FOR IAS.IAS_ACCOUNT_NOTES;
--CREATE SYNONYM IAS_ACCOUNT_ATTRIBUTES FOR IAS.IAS_ACCOUNT_ATTRIBUTES;
--CREATE SYNONYM IAS_ATTRIBUTES FOR IAS.IAS_ATTRIBUTES;
--CREATE SYNONYM IAS_ATTRIBUTE_PROFILES FOR IAS.IAS_ATTRIBUTE_PROFILES;
CREATE SYNONYM IAS_EXTERNAL_ACCOUNTS FOR IAS.IAS_EXTERNAL_ACCOUNTS;
--CREATE SYNONYM IAS_DEVICE_SUBSCRIPTIONS FOR IAS.IAS_DEVICE_SUBSCRIPTIONS;
--CREATE SYNONYM IAS_AUTHENTICATION_TOKENS FOR IAS.IAS_AUTHENTICATION_TOKENS;

CREATE TABLE STRING_TRANSLATIONS
(
  INPUT_STRING VARCHAR2(255),
  OUTPUT_STRING VARCHAR2(255),
  OUTPUT_LANG VARCHAR2(10),
  CONSTRAINT STRING_TRANSLATIONS_PK
 PRIMARY KEY (INPUT_STRING)
)
ORGANIZATION INDEX
TABLESPACE SHARED
/

CREATE TABLE DATA_LOAD_REQUESTS
(
  REQUEST_ID      INTEGER                       NOT NULL,
  REQUEST_DATE    DATE                          DEFAULT sys_extract_utc(CURRENT_TIMESTAMP),
  JOB_NAME        VARCHAR2(64 BYTE),
  OPERATOR_ID     INTEGER,
  ROLE_LEVEL      INTEGER,
  VALIDATE_DATE   DATE,
  PROCESS_DATE    DATE,
  PROCESS_STATUS  VARCHAR2(32 BYTE),
  PROCESS_XML     VARCHAR2(4000 BYTE)
)
TABLESPACE SHARED
/
ALTER TABLE DATA_LOAD_REQUESTS ADD (
  CONSTRAINT DATA_LOAD_REQUEST_PK
 PRIMARY KEY (REQUEST_ID)
 USING INDEX TABLESPACE SHARED)
/

CREATE SEQUENCE KEY_ID_SEQ START WITH 10;
/* uncomment to drop ops tables
DROP TABLE OPERATION_USERS CASCADE CONSTRAINTS;
DROP TABLE OPERATION_GROUPS CASCADE CONSTRAINTS;
DROP TABLE OPERATION_ROLE_ACTIVITIES CASCADE CONSTRAINTS;
DROP TABLE OPERATION_ROLES CASCADE CONSTRAINTS;
DROP TABLE OPERATION_ACTIVITIES CASCADE CONSTRAINTS;
DROP TABLE OPERATOR_ACTIVITIES CASCADE CONSTRAINTS;
*/
CREATE CLUSTER OPERATION_GROUP
   (role_id INTEGER)
SIZE 512 INDEX TABLESPACE SHARED CACHE
/
CREATE INDEX OPERATION_GROUP_IDX ON CLUSTER
 OPERATION_GROUP TABLESPACE SHARED;

CREATE TABLE OPERATION_ACTIVITIES
(
  ACTIVITY_ID    INTEGER,
  ACTIVITY_NAME  VARCHAR2(255) NOT NULL,
  REQUEST_TYPE   VARCHAR2(32) NOT NULL,
  HAS_LIST     CHAR(1) DEFAULT 'N' NOT NULL,
  HAS_SEARCH   CHAR(1) DEFAULT 'N' NOT NULL,
  HAS_DISPLAY  CHAR(1) DEFAULT 'N' NOT NULL,
  HAS_UPDATE   CHAR(1) DEFAULT 'N' NOT NULL,
  HAS_CREATE   CHAR(1) DEFAULT 'N' NOT NULL,
  HAS_DELETE   CHAR(1) DEFAULT 'N' NOT NULL,
  HAS_APPROVE  CHAR(1) DEFAULT 'N' NOT NULL,
  LOG_LIST     SMALLINT DEFAULT 0 NOT NULL,
  LOG_SEARCH   SMALLINT DEFAULT 0 NOT NULL,
  LOG_DISPLAY  SMALLINT DEFAULT 0 NOT NULL,
  LOG_UPDATE   SMALLINT DEFAULT 1 NOT NULL,
  LOG_CREATE   SMALLINT DEFAULT 1 NOT NULL,
  LOG_DELETE   SMALLINT DEFAULT 1 NOT NULL,
  LOG_APPROVE   SMALLINT DEFAULT 1 NOT NULL,
  CONSTRAINT OPERATION_ACTIVITIES_PK
 PRIMARY KEY (ACTIVITY_ID)
) ORGANIZATION INDEX
TABLESPACE SHARED
/

CREATE TABLE OPERATION_ROLES
(
  ROLE_ID      INTEGER,
  ROLE_NAME    VARCHAR2(32),
  DESCRIPTION  VARCHAR2(255),
  CREATE_DATE  DATE                           DEFAULT sys_extract_utc(CURRENT_TIMESTAMP),
  IS_ACTIVE    INTEGER, 
  LAST_UPDATED DATE                           DEFAULT sys_extract_utc(CURRENT_TIMESTAMP), 
  CONSTRAINT OPERATION_ROLES_PK PRIMARY KEY (ROLE_ID)
) CLUSTER OPERATION_GROUP (role_id)
/

CREATE TABLE OPERATION_USERS
(
  OPERATOR_ID      INTEGER                        DEFAULT TO_NUMBER(TO_CHAR(CURRENT_TIMESTAMP,'D')||TO_CHAR(CURRENT_TIMESTAMP,'SSSSSFF')) NOT NULL,
  EMAIL_ADDRESS    VARCHAR2(255 BYTE),
  STATUS           CHAR(1 BYTE),
  FULLNAME         VARCHAR2(255 BYTE),
  EMAIL_ALERTS     NUMBER(1),
  PASSWD           VARCHAR2(64 BYTE),
  ROLE_ID          INTEGER,
  LAST_LOGON       DATE,
  STATUS_DATE      DATE                           DEFAULT sys_extract_utc(CURRENT_TIMESTAMP),
  OTHER_INFO       VARCHAR2(255 BYTE),
  LAST_UPDATED     DATE                           DEFAULT sys_extract_utc(CURRENT_TIMESTAMP), 
  GROUP_ID         INTEGER,
  CONSTRAINT OPERATION_USERS_PK PRIMARY KEY (OPERATOR_ID)
) CLUSTER OPERATION_GROUP (ROLE_ID)
/

CREATE TABLE OPERATION_GROUPS
(
  GROUP_ID       INTEGER,
  ROLE_ID        INTEGER,
  GROUP_NAME     VARCHAR2(255),
  IS_GLOBAL      SMALLINT,
  COUNTRY_LIST   INTEGER_LIST,
  OPERATOR_LIST  INTEGER_LIST,
  CONSTRAINT OPERATION_GROUPS_PK PRIMARY KEY (GROUP_ID)
) CLUSTER OPERATION_GROUP (role_id)
NESTED TABLE COUNTRY_LIST STORE AS OPERATOR_GROUPS_CTYLIST
NESTED TABLE OPERATOR_LIST STORE AS OPERATOR_GROUPS_OPSLIST
(TABLESPACE SHARED)
/
ALTER TABLE OPERATION_GROUPS ADD
 CONSTRAINT OPGROUP_ROLE_FK FOREIGN KEY (ROLE_ID) 
 REFERENCES OPERATION_ROLES (ROLE_ID)
  ON DELETE CASCADE
/

CREATE TABLE OPERATION_ROLE_ACTIVITIES
(
  ROLE_ID        INTEGER,
  ACTIVITY_ID    INTEGER, 
  ALLOW_LIST     CHAR(1) DEFAULT 'N' NOT NULL,
  ALLOW_SEARCH   CHAR(1) DEFAULT 'N' NOT NULL,
  ALLOW_DISPLAY  CHAR(1) DEFAULT 'N' NOT NULL,
  ALLOW_UPDATE   CHAR(1) DEFAULT 'N' NOT NULL,
  ALLOW_CREATE   CHAR(1) DEFAULT 'N' NOT NULL,
  ALLOW_DELETE   CHAR(1) DEFAULT 'N' NOT NULL,
  ALLOW_APPROVE  CHAR(1) DEFAULT 'N' NOT NULL,
  CONSTRAINT OPERATION_ROLE_ACTIVITIES_PK PRIMARY KEY (ROLE_ID,ACTIVITY_ID)
) CLUSTER OPERATION_GROUP (ROLE_ID)
/
ALTER TABLE OPERATION_ROLE_ACTIVITIES ADD
 CONSTRAINT OPRACT_ROLE_FK FOREIGN KEY (ROLE_ID) 
 REFERENCES OPERATION_ROLES (ROLE_ID)
  ON DELETE CASCADE
/
ALTER TABLE OPERATION_ROLE_ACTIVITIES ADD
 CONSTRAINT OPRACT_ACTS_FK FOREIGN KEY (ACTIVITY_ID) 
 REFERENCES OPERATION_ACTIVITIES (ACTIVITY_ID)
  ON DELETE CASCADE
/
CREATE TABLE OPERATOR_ACTIVITIES
(
  OPERATOR_ID    INTEGER,
  ACTIVITY_ID    INTEGER, 
  ACTIVITY_ACTION VARCHAR2(32),
  ACTIVITY_DATE DATE,
  GROUP_ID INTEGER,
  GENERATED_ID VARCHAR2(255),
  DEBUG_IMAGE CLOB,
  BEFORE_IMAGE CLOB,
  AFTER_IMAGE  CLOB,
  CONSTRAINT OPERATOR_ACTIVITIES_PK PRIMARY KEY 
  (OPERATOR_ID,ACTIVITY_ID,ACTIVITY_ACTION,ACTIVITY_DATE)
) ORGANIZATION INDEX COMPRESS 3
LOGGING
TABLESPACE OPLOGD
LOB (DEBUG_IMAGE) STORE AS (
  TABLESPACE OPLOGD
  DISABLE STORAGE IN ROW
  CHUNK 8192
  NOCACHE)
LOB (BEFORE_IMAGE) STORE AS (
  TABLESPACE OPLOGD
  DISABLE STORAGE IN ROW
  CHUNK 8192
  NOCACHE)
LOB (AFTER_IMAGE) STORE AS (
  TABLESPACE OPLOGD
  DISABLE STORAGE IN ROW
  CHUNK 8192
  NOCACHE)
/


CREATE OR REPLACE TRIGGER OPS_LAST_UPDATED_TRG
 BEFORE
 INSERT OR UPDATE
 ON OPERATION_USERS
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
/*
 * (C) 2006, BroadOn Communications Corp, Inc.,
 * $Id: BMSSCHEMA.sql,v 1.15 2006/08/24 22:00:42 jchang Exp $
 */
DECLARE
BEGIN
   :NEW.last_updated:=sys_extract_utc(CURRENT_TIMESTAMP);
END;
/
CREATE OR REPLACE TRIGGER OPR_LAST_UPDATED_TRG
 BEFORE
 INSERT OR UPDATE
 ON OPERATION_ROLES
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
/*
 * (C) 2006, BroadOn Communications Corp, Inc.,
 * $Id: BMSSCHEMA.sql,v 1.15 2006/08/24 22:00:42 jchang Exp $
 */
DECLARE
BEGIN
   :NEW.last_updated:=sys_extract_utc(CURRENT_TIMESTAMP);
END;
/

CREATE GLOBAL TEMPORARY TABLE INCOMING ( TABLENAME VARCHAR2(32), PMODE VARCHAR2(10), XMLDOC CLOB ) ON COMMIT DELETE ROWS;


DROP PUBLIC SYNONYM operation_roles;
DROP PUBLIC SYNONYM operation_users;
DROP PUBLIC SYNONYM operation_user_roles;

CREATE TABLE cas_item_pricings (
item_id INTEGER,
title_id INTEGER,
device_type_id SMALLINT,
country_id SMALLINT,
price_id SMALLINT,
purchase_start_date DATE,
purchase_end_date DATE,
rtype VARCHAR2(10),
limits INTEGER,
license_type VARCHAR2(10),
limit_code VARCHAR2(100),
last_updated DATE,
ecard_type INTEGER,
CONSTRAINT CAS_ITEM_PRICINGS_PK PRIMARY KEY (item_id))
ORGANIZATION INDEX TABLESPACE SHARED
/
DROP TRIGGER CAS_LAST_UPDATED_TRG
/

CREATE TABLE cas_price_tags (
price_id SMALLINT,
price NUMBER,
currency VARCHAR2(10),
CONSTRAINT CAS_PRICE_TAGS_PK PRIMARY KEY (price_id))
ORGANIZATION INDEX TABLESPACE SHARED
/

ALTER TABLE cas_item_pricings ADD (
  CONSTRAINT CASIP_CASPTAG_FK 
 FOREIGN KEY (PRICE_ID) REFERENCES CAS_PRICE_TAGS (PRICE_ID))
/
CREATE SYNONYM ECS_price_tags FOR CAS_price_tags;
CREATE SYNONYM ECS_ITEM_PRICINGS FOR CAS_ITEM_PRICINGS;


COMMENT ON TABLE INCOMING IS '/* $Id: BMSSCHEMA.sql,v 1.15 2006/08/24 22:00:42 jchang Exp $*/';
COMMENT ON TABLE STRING_TRANSLATIONS IS '/* $Id: BMSSCHEMA.sql,v 1.15 2006/08/24 22:00:42 jchang Exp $*/';
COMMENT ON TABLE OPERATION_USERS IS '/* $Id: BMSSCHEMA.sql,v 1.15 2006/08/24 22:00:42 jchang Exp $*/';
COMMENT ON TABLE OPERATION_GROUPS IS '/* $Id: BMSSCHEMA.sql,v 1.15 2006/08/24 22:00:42 jchang Exp $*/';
COMMENT ON TABLE OPERATION_GROUP_USERS IS '/* $Id: BMSSCHEMA.sql,v 1.15 2006/08/24 22:00:42 jchang Exp $*/';
COMMENT ON TABLE OPERATION_ROLES IS '/* $Id: BMSSCHEMA.sql,v 1.15 2006/08/24 22:00:42 jchang Exp $*/';
COMMENT ON TABLE OPERATION_ACTIVITIES IS '/* $Id: BMSSCHEMA.sql,v 1.15 2006/08/24 22:00:42 jchang Exp $*/';
COMMENT ON TABLE OPERATOR_ACTIVITIES IS '/* $Id: BMSSCHEMA.sql,v 1.15 2006/08/24 22:00:42 jchang Exp $*/';
COMMENT ON TABLE OPERATION_ROLE_ACTIVITIES IS '/* $Id: BMSSCHEMA.sql,v 1.15 2006/08/24 22:00:42 jchang Exp $*/';
--COMMENT ON TABLE OPERATION_MENUS IS '/* $Id: BMSSCHEMA.sql,v 1.15 2006/08/24 22:00:42 jchang Exp $*/';
COMMENT ON TABLE DATA_LOAD_REQUESTS IS '/* $Id: BMSSCHEMA.sql,v 1.15 2006/08/24 22:00:42 jchang Exp $*/';
COMMENT ON TABLE CAS_ITEM_PRICINGS IS '/* $Id: BMSSCHEMA.sql,v 1.15 2006/08/24 22:00:42 jchang Exp $*/';
COMMENT ON TABLE CAS_PRICE_TAGS IS '/* $Id: BMSSCHEMA.sql,v 1.15 2006/08/24 22:00:42 jchang Exp $*/';

-- seed
ALTER SESSION SET CURRENT_SCHEMA = BMS;
INSERT INTO OPERATION_ROLES (ROLE_ID,ROLE_NAME,DESCRIPTION,CREATE_DATE,IS_ACTIVE)
  VALUES (0, 'BMSSETUP', 'BMS Setup Role',TRUNC(SYSDATE), 1);
INSERT INTO OPERATION_USERS (OPERATOR_ID, EMAIL_ADDRESS, STATUS, FULLNAME, EMAIL_ALERTS, PASSWD, ROLE_ID, STATUS_DATE, GROUP_ID )
  VALUES (0,'admin','A','BMS Setup Account', 1,'admin',0,TRUNC(SYSDATE),0);
INSERT INTO OPERATION_GROUPS (GROUP_ID,ROLE_ID,GROUP_NAME,IS_GLOBAL,OPERATOR_LIST)
  VALUES (0, 0, 'BMS Setup Group', 1, INTEGER_LIST(0)); 

INSERT INTO ECARD_TYPES VALUES (5,500,'POINTS',1,1,0,SYSDATE,'500 Virtual Console Prepaid Points Card',0);
INSERT INTO ECARD_TYPES VALUES (10,1000,'POINTS',1,1,0,SYSDATE,'1000 Virtual Console Prepaid Points Card',0);
INSERT INTO ECARD_TYPES VALUES (20,2000,'POINTS',1,1,0,SYSDATE,'2000 Virtual Console Prepaid Points Card',0);
INSERT INTO ECARD_TYPES VALUES (30,3000,'POINTS',1,1,0,SYSDATE,'3000 Virtual Console Prepaid Points Card',0);
INSERT INTO ECARD_TYPES VALUES (50,5000,'POINTS',1,1,0,SYSDATE,'5000 Virtual Console Prepaid Points Card',0);
INSERT INTO CAS_PRICE_TAGS VALUES ( 0, 0,'POINTS');
INSERT INTO CAS_PRICE_TAGS VALUES ( 11, 1,'POINTS');
INSERT INTO CAS_PRICE_TAGS VALUES ( 12, 1,'GUNIT');
INSERT INTO CAS_PRICE_TAGS VALUES ( 30, 6,'USD');
INSERT INTO CAS_PRICE_TAGS VALUES ( 1, 500,'POINTS');
INSERT INTO CAS_PRICE_TAGS VALUES ( 2, 1000,'POINTS');
INSERT INTO CAS_PRICE_TAGS VALUES ( 3, 2000,'POINTS');
INSERT INTO CAS_PRICE_TAGS VALUES ( 4, 3000,'POINTS');
INSERT INTO CAS_PRICE_TAGS VALUES ( 5, 5000,'POINTS');
INSERT INTO CAS_ITEM_PRICINGS (ITEM_ID, DEVICE_TYPE_ID, PRICE_ID, PURCHASE_START_DATE, LIMITS, LICENSE_TYPE, ECARD_TYPE)
VALUES (500,1,1,TRUNC(SYSDATE),500,'VCPOINTS',5);
INSERT INTO CAS_ITEM_PRICINGS (ITEM_ID, DEVICE_TYPE_ID, PRICE_ID, PURCHASE_START_DATE, LIMITS, LICENSE_TYPE, ECARD_TYPE)
VALUES (1000,1,2,TRUNC(SYSDATE),1000,'VCPOINTS',10);
INSERT INTO CAS_ITEM_PRICINGS (ITEM_ID, DEVICE_TYPE_ID, PRICE_ID, PURCHASE_START_DATE, LIMITS, LICENSE_TYPE, ECARD_TYPE)
VALUES (2000,1,3,TRUNC(SYSDATE),2000,'VCPOINTS',20);
INSERT INTO CAS_ITEM_PRICINGS (ITEM_ID, DEVICE_TYPE_ID, PRICE_ID, PURCHASE_START_DATE, LIMITS, LICENSE_TYPE, ECARD_TYPE)
VALUES (3000,1,4,TRUNC(SYSDATE),3000,'VCPOINTS',30);
INSERT INTO CAS_ITEM_PRICINGS (ITEM_ID, DEVICE_TYPE_ID, PRICE_ID, PURCHASE_START_DATE, LIMITS, LICENSE_TYPE, ECARD_TYPE)
VALUES (5000,1,5,TRUNC(SYSDATE),5000,'VCPOINTS',50);
INSERT INTO CAS_ITEM_PRICINGS (ITEM_ID, DEVICE_TYPE_ID)
VALUES (100000,1);
INSERT INTO CAS_ITEM_PRICINGS (ITEM_ID, DEVICE_TYPE_ID, COUNTRY_ID, PRICE_ID, PURCHASE_START_DATE, LIMITS, LICENSE_TYPE)
VALUES (144,1,2,30,TRUNC(SYSDATE),60000,'VCPOINTS');
ALTER SESSION SET CURRENT_SCHEMA = BMS
/
@@BMSPKG.sql
@@BMSPKB.sql
@@CATALOGVIEWS.sql
--@@BMSAPPROVAL.sql
UPDATE CDS.CONTENT_TITLES SET approve_date=TRUNC(SYSDATE) WHERE approve_date IS NULL;
UPDATE SDS.COUNTRIES SET points_approve_date=TRUNC(SYSDATE) WHERE points_approve_date IS NULL;
INSERT INTO CDS.CONTENT_CACHE (CONTENT_CHECKSUM, CONTENT_OBJECT_TYPE, CONTENT_SIZE, CONTENT_OBJECT_NAME, CONTENT_OBJECT_VERSION, CREATE_DATE)
VALUES ('ETKM Place Holder','ETKM',0,'ETKM_Place_Holder',0,TRUNC(SYSDATE));

ALTER SESSION SET CURRENT_SCHEMA = BMS
/
CREATE OR REPLACE FORCE VIEW BMS_TRANSACTION_DETAILS
AS 
SELECT TRANS_ID, TRANS_DATE, TRANS_TYPE, ACCOUNT_ID, DEVICE_ID, SERIAL_NO, STATUS, CLIENT_IPADDR,
       TOTAL_AMOUNT, TOTAL_TAXES, TOTAL_PAID, CURRENCY, ORIG_TRANS_ID, OPERATOR_ID,
	   t.COUNTRY_ID,t.ITEM_ID, QTY, TOTAL_DISCOUNT, PAYMENT_TYPE, PAYMENT_METHOD_ID, PAS_AUTH_ID,
	   PAYMENT_ECARD_TYPE, STATUS_DATE, PAYMENT_ECARD_DESCRIPTION,
       DEVICE_TYPE, POINTS, NVL(ip.TITLE_ID,t.title_id) TITLE_ID, HEX_TITLE_ID, t.COUNTRY, t.REGION, PRICING_COUNTRY, PRICING_COUNTRY_ID,
	   ip.ITEM_PRICE, ip.ITEM_CURRENCY, PURCHASE_START_DATE, PURCHASE_END_DATE, RTYPE, LIMITS, LICENSE_TYPE,
	   LIMIT_CODE, LAST_UPDATED, ECARD_TYPE, ECARD_DESCRIPTION,
       DECODE(ip.rtype,'TR',limits/60,NULL) minutes,
       DECODE(ip.license_type,'VCPOINTS',limits,NULL) refill_points,
	   DECODE(ip.item_id,NULL,NULL,
	   DECODE(license_type,'VCPOINTS',DECODE(ip.ecard_type,NULL,'POINTS_PRICING_CATALOG','ECARD_POINTS_CATALOG'),
	          'SUBSCRIPT','SUBSCRIPTION_PRICING_CATALOG',
			  DECODE(ip.title_id,NULL,'UNDEFINED',DECODE(ip.ecard_type,NULL,'GAME_PRICING_CATALOG','ECARD_GAME_CATALOG')))) CATALOG_NAME
  FROM CAS_COUNTRY_ITEM_PRICINGS ip,
       ECS_TRANSACTION_DETAILS t
 WHERE ip.item_id(+)=t.item_id
   AND NVL(ip.country_id,t.country_id)=t.country_id
/
COMMENT ON TABLE BMS_TRANSACTION_DETAILS IS '/* $Id: BMSSCHEMA.sql,v 1.15 2006/08/24 22:00:42 jchang Exp $*/'
/
CREATE OR REPLACE FORCE VIEW EXP_TRANSACTION_DETAILS
AS 
SELECT t.trans_id,t.trans_type,t.trans_date,
t.total_amount,t.total_taxes,t.total_paid,t.total_discount,
t.currency,t.account_id,t.device_id,t.serial_no,t.client_ipaddr,t.title_id,ct.product_code,ct.title,
t.country,t.region,
t.payment_type,t.payment_method_id,t.orig_trans_id,
t.item_id,t.license_type,
t.minutes,t.refill_points,t.catalog_name
FROM BMS_TRANSACTION_DETAILS t, content_title_catalog ct
WHERE t.title_id=ct.title_id(+)
/
COMMENT ON TABLE EXP_TRANSACTION_DETAILS IS '/* $Id: BMSSCHEMA.sql,v 1.15 2006/08/24 22:00:42 jchang Exp $*/'
/
/*
ACCOUNT_ID, LOGIN_NAME, PASSWD, PARENT_ID, CREATE_DATE, ACTIVATE_DATE, STATUS, NICK_NAME, FULL_NAME, EMAIL_ADDRESS, BIRTH_DATE, PAS_ACCOUNT_ID, LAST_UPDATED
DEVICE_ID, DEVICE_TYPE_ID, SERIAL_NO, CONFIG_CODE, REGISTER_DATE, PUBLIC_KEY, LAST_UPDATED
NEW_SERIAL_NO, REQUEST_DATE, ACCOUNT_ID, SERIAL_NO, DEVICE_ID, OPERATOR_ID, NEW_DEVICE_ID, PROCESS_DATE
EXT_ACCOUNT_ID, EXT_ENTITY, IAS_ACCOUNT_ID, IAS_SYNC_TIME, LAST_UPDATED, EXT_DATA
ACCOUNT_ID, CREATE_DATE, DEVICE_ID, LAST_UPDATED, OPERATOR_ID, SHORT_TEXT, NOTES
DEVICE_ID, ACCOUNT_ID, LINK_DATE, REMOVE_DATE
ACCOUNT_ID, ACCOUNT_TYPE, ACTIVATE_DATE, DEACTIVATE_DATE, REVOKE_DATE, BALANCE, BALANCE_AUTHORIZED, CURRENCY, LAST_UPDATED, PIN
*/

CREATE OR REPLACE FORCE VIEW VC_ACCOUNTS AS
SELECT ia.ACCOUNT_ID,ia.ACTIVATE_DATE,ia.STATUS,idv.DEVICE_ID,d.DEVICE_TYPE,idv.SERIAL_NO,CONFIG_CODE,idv.REGISTER_DATE,
ixa.EXT_ACCOUNT_ID,
pba.BALANCE,pba.BALANCE_AUTHORIZED,pba.CURRENCY,pba.PIN,
(SELECT COUNT(*) FROM ETICKETS WHERE device_id=idv.device_id AND revoke_date IS NULL) total_etickets,
(SELECT MAX(create_date) FROM IAS_ACCOUNT_NOTES WHERE account_id=ia.account_id AND create_date IS NOT NULL) last_notes_date,
(SELECT MAX(process_date) FROM IAS_DEVICE_TRANSFERS WHERE account_id=ia.account_id AND process_date IS NOT NULL) last_transfer_date,
(SELECT MAX(trans_date) FROM ECS_TRANSACTIONS WHERE account_id=ia.account_id AND status='SUCCESS') last_transaction_date,
DECODE(idf.request_date,NULL,'N','Y') pending_transfer,
idf.request_date transfer_request_date,idf.NEW_SERIAL_NO,idf.serial_no old_serial_no,idf.DEVICE_ID old_device_id,idf.NEW_DEVICE_ID
FROM IAS_ACCOUNTS ia,
     IAS_DEVICES idv,
     IAS_ACCOUNT_DEVICES iad,
     IAS_EXTERNAL_ACCOUNTS ixa,
	 IAS_DEVICE_TRANSFERS idf,
	 PAS_BANK_ACCOUNTS pba,
	 DEVICE_TYPES d
WHERE d.device_type_id=idv.device_type_id
  AND ia.account_id=iad.account_id
  AND idv.device_id=iad.device_id
  AND iad.remove_date IS NULL
  AND ia.account_id=pba.account_id
  AND ia.account_id=ixa.ias_account_id (+)
  AND ia.account_id=idf.account_id (+)
  AND idf.process_date (+) IS NULL
/

BEGIN DBMS_JOB.REMOVE(-998);
END;
/

ALTER SESSION SET CURRENT_SCHEMA = SYSTEM
/


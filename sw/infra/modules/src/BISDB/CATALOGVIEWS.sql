--ALTER SESSION SET CURRENT_SCHEMA=BMS
--ALTER SESSION SET CURRENT_SCHEMA=CAS

CREATE OR REPLACE FORCE VIEW CAS_COUNTRY_ITEM_PRICINGS
AS 
SELECT
       ip.item_id,dt.device_type, 
       DECODE(license_type,'VCPOINTS',limits,NULL) points,ip.title_id,
       bccutil.dec2hex(ip.title_id,16) hex_title_id,
	   c.country,pc.country pricing_country,pc.country_id pricing_country_id, 
       pt.price_id,pt.price item_price,pt.currency item_currency, 
       c.country_id,ip.DEVICE_TYPE_ID,c.region, 
       ip.purchase_start_date,ip.purchase_end_date,ip.rtype,ip.limits,ip.license_type,ip.limit_code, 
       ip.last_updated,ip.ecard_type ecard_type,et.description ecard_description,
	   DECODE(license_type,'VCPOINTS',pc.points_approve_date,NULL) points_approve_date
  FROM CAS_ITEM_PRICINGS ip, 
       CAS_PRICE_TAGS pt, 
       countries c, 
       countries pc, 
       device_types dt,
       ecard_types et
 WHERE ip.price_id=pt.price_id 
   AND ip.country_id=pc.country_id (+)
   AND ip.country_id=c.pricing_country_id (+)
   AND dt.device_type_id=ip.device_type_id
   AND et.ecard_type(+)=ip.ecard_type
/

CREATE OR REPLACE FORCE VIEW ECARD_GAME_CATALOG
AS 
SELECT
ip.ITEM_ID,ip.TITLE_ID,ip.ECARD_TYPE,ip.ecard_description, 
cl.COUNTRY_ID,cl.COUNTRY,cl.REGION,ip.ITEM_PRICE,ip.ITEM_CURRENCY, 
ip.PURCHASE_START_DATE,ip.PURCHASE_END_DATE,ip.limits,ip.rtype,
ip.LICENSE_TYPE,ip.LIMIT_CODE,ip.LAST_UPDATED, 
cl.language_code,cl.locale,cl.default_locale,cl.country_default,cl.TITLE,cl.title_version,cl.title_version_date,cl.HIDDEN_TITLE,cl.CATEGORY,cl.PLATFORM, 
cl.publisher,cl.developer,cl.publish_date,cl.release_date, 
cl.cacheable,cl.preloadable,cl.recommend_date,cl.Popularity,cl.new_since,cl.new_duration,
cl.classic_controller,cl.rvl_controller,cl.need_dvd,cl.min_players,cl.max_players,cl.attribits, 
cl.tmd_content_id,cl.etkm_content_id,cl.tmd_content_checksum,cl.met_content_checksum, 
cl.title_size,cl.approx_size, 
cl.rating_system,cl.rating_value,cl.rating_age,cl.RATING_ATTRIBITS,
cl.second_system,cl.second_value,cl.second_age,cl.SECOND_ATTRIBITS, 
cl.description,cl.short_phrase,cl.product_code,cl.approve_date,cl.stable_version,
cl.title_type,cl.title_kind
FROM CAS_COUNTRY_ITEM_PRICINGS ip,
     CONTENT_TITLE_COUNTRY_RELEASES cl
WHERE cl.TITLE_KIND='GAME' AND ip.ECARD_TYPE IS NOT NULL 
AND ip.title_id=cl.title_id 
--AND l.locale=NVL(cl.locale,ct.default_locale) 
--AND ip.country_id=cl.country_id 
/

CREATE OR REPLACE FORCE VIEW ECARD_POINTS_CATALOG
AS 
SELECT ip.ITEM_ID,ip.ECARD_TYPE,ip.ecard_description , 
--,ip.COUNTRY_ID,ip.COUNTRY,ip.REGION, 
ip.ITEM_PRICE,ip.ITEM_CURRENCY, 
ip.PURCHASE_START_DATE,ip.PURCHASE_END_DATE,ip.LAST_UPDATED,ip.limits refill_points 
FROM CAS_COUNTRY_ITEM_PRICINGS ip
WHERE license_type='VCPOINTS' AND ip.ecard_type IS NOT NULL
/



CREATE OR REPLACE FORCE VIEW GAME_PRICING_CATALOG
AS 
SELECT ip.ITEM_ID,ip.TITLE_ID,ip.device_type,cl.COUNTRY_ID,cl.COUNTRY,cl.REGION,
ip.pricing_country,
ip.pricing_country_id,
ip.ITEM_PRICE,ip.ITEM_CURRENCY, 
ip.PURCHASE_START_DATE,ip.PURCHASE_END_DATE,ip.limits,ip.rtype,
ip.LICENSE_TYPE,ip.LIMIT_CODE,ip.LAST_UPDATED, 
cl.language_code,cl.locale,cl.default_locale,cl.country_default,cl.TITLE,cl.title_version,cl.title_version_date,cl.HIDDEN_TITLE,cl.CATEGORY,cl.PLATFORM, 
cl.publisher,cl.developer,cl.publish_date,cl.release_date, 
cl.cacheable,cl.preloadable,cl.recommend_date,cl.Popularity,cl.new_since,cl.new_duration,
cl.classic_controller,cl.rvl_controller,cl.need_dvd,cl.min_players,cl.max_players,cl.attribits, 
cl.tmd_content_id,cl.etkm_content_id,cl.tmd_content_checksum,cl.met_content_checksum, 
cl.title_size,cl.approx_size, 
cl.rating_system,cl.rating_value,cl.rating_age,cl.RATING_ATTRIBITS,
cl.second_system,cl.second_value,cl.second_age,cl.SECOND_ATTRIBITS, 
cl.description,cl.short_phrase,cl.product_code,cl.approve_date,cl.stable_version,
cl.title_type,cl.title_kind
FROM CAS_COUNTRY_ITEM_PRICINGS ip,
     CONTENT_TITLE_COUNTRY_RELEASES cl
WHERE cl.TITLE_KIND='GAME' AND ip.ECARD_TYPE IS NULL 
AND ip.title_id=cl.title_id 
AND ip.country_id=cl.country_id
/

CREATE OR REPLACE FORCE VIEW POINTS_PRICING_CATALOG
AS 
SELECT ip.ITEM_ID,ip.TITLE_ID,ip.COUNTRY_ID,ip.COUNTRY,ip.REGION, 
ip.pricing_country,ip.pricing_country_id,ip.ITEM_PRICE,ip.ITEM_CURRENCY, 
ip.PURCHASE_START_DATE,ip.PURCHASE_END_DATE,ip.LAST_UPDATED, 
ip.limits refill_points,points_approve_date
FROM CAS_COUNTRY_ITEM_PRICINGS ip
WHERE license_type='VCPOINTS' AND ip.ecard_type IS NULL 
/


CREATE OR REPLACE FORCE VIEW CURRENT_ECARD_GAME_CATALOG
AS 
SELECT * 
FROM ECARD_GAME_CATALOG
WHERE purchase_start_date<=sys_extract_utc(CURRENT_TIMESTAMP) 
AND (purchase_end_date IS NULL OR 
     purchase_end_date>=sys_extract_utc(CURRENT_TIMESTAMP))
/


CREATE OR REPLACE FORCE VIEW CURRENT_ECARD_POINTS_CATALOG
AS 
SELECT * 
FROM ECARD_POINTS_CATALOG
WHERE purchase_start_date<=sys_extract_utc(CURRENT_TIMESTAMP) 
AND (purchase_end_date IS NULL OR 
     purchase_end_date>=sys_extract_utc(CURRENT_TIMESTAMP))
/


CREATE OR REPLACE FORCE VIEW CURRENT_GAME_PRICING_CATALOG
AS 
SELECT * 
FROM GAME_PRICING_CATALOG
WHERE purchase_start_date<=sys_extract_utc(CURRENT_TIMESTAMP) 
AND (purchase_end_date IS NULL OR 
     purchase_end_date>=sys_extract_utc(CURRENT_TIMESTAMP))
/


CREATE OR REPLACE FORCE VIEW CURRENT_POINTS_PRICING_CATALOG
AS 
SELECT * 
FROM POINTS_PRICING_CATALOG
WHERE purchase_start_date<=sys_extract_utc(CURRENT_TIMESTAMP) 
AND (purchase_end_date IS NULL OR 
     purchase_end_date>=sys_extract_utc(CURRENT_TIMESTAMP))
/

COMMENT ON TABLE ECARD_POINTS_CATALOG IS '/* $Id: CATALOGVIEWS.sql,v 1.11 2006/08/25 20:12:29 jchang Exp $*/';
COMMENT ON TABLE ECARD_GAME_CATALOG IS '/* $Id: CATALOGVIEWS.sql,v 1.11 2006/08/25 20:12:29 jchang Exp $*/';
COMMENT ON TABLE GAME_PRICING_CATALOG IS '/* $Id: CATALOGVIEWS.sql,v 1.11 2006/08/25 20:12:29 jchang Exp $*/';
COMMENT ON TABLE POINTS_PRICING_CATALOG IS '/* $Id: CATALOGVIEWS.sql,v 1.11 2006/08/25 20:12:29 jchang Exp $*/';
COMMENT ON TABLE CURRENT_ECARD_POINTS_CATALOG IS '/* $Id: CATALOGVIEWS.sql,v 1.11 2006/08/25 20:12:29 jchang Exp $*/';
COMMENT ON TABLE CURRENT_ECARD_GAME_CATALOG IS '/* $Id: CATALOGVIEWS.sql,v 1.11 2006/08/25 20:12:29 jchang Exp $*/';
COMMENT ON TABLE CURRENT_GAME_PRICING_CATALOG IS '/* $Id: CATALOGVIEWS.sql,v 1.11 2006/08/25 20:12:29 jchang Exp $*/';
COMMENT ON TABLE CURRENT_POINTS_PRICING_CATALOG IS '/* $Id: CATALOGVIEWS.sql,v 1.11 2006/08/25 20:12:29 jchang Exp $*/';

--/* Old views uncomment if needed
DROP VIEW ECS_ITEM_PRICING_DETAILS;
/*
CREATE OR REPLACE FORCE VIEW ECS_ITEM_PRICING_DETAILS
AS 
SELECT ip.item_id,dt.device_type, 
       ct.refill_points points, ct.*,
       DECODE(ct.title_type,'SUBSCRIPT',SUBSTR(ct.hex_title_id,1,8),NULL) channel_code, 
       c.country, 
       pt.price_id,pt.price item_price,pt.currency item_currency, 
       ip.country_id,ip.DEVICE_TYPE_ID,c.region, 
       ip.purchase_start_date,ip.purchase_end_date,ip.rtype,ip.limits,ip.license_type,ip.limit_code, 
       ip.last_updated,ip.ecard_type item_ecard_type,et.description item_ecard_description,
       cr.rating_system,cr.rating_value,cr.rating_age,cr.second_system,cr.second_value,cr.second_age
  FROM ECS_ITEM_PRICINGS ip, 
       CONTENT_TITLE_CATALOG ct, 
       CONTENT_TITLE_COUNTRIES cr, 
       ECS_PRICE_TAGS pt, 
       countries c, 
       device_types dt,
	   ecard_types et
 WHERE ip.title_id=ct.title_id 
   AND ip.price_id=pt.price_id 
   AND ip.country_id=c.country_id (+)
   AND ip.country_id=cr.country_id (+)
   AND ip.title_id=cr.title_id (+) 
   AND dt.device_type_id=ip.device_type_id
   AND et.ecard_type(+)=ip.ecard_type
/
*/

DROP VIEW ECS_SUBSCRIPTION_CATALOG;
CREATE SYNONYM ECS_SUBSCRIPTION_CATALOG FOR SUBSCRIPTION_PRICING_CATALOG;
CREATE OR REPLACE FORCE VIEW SUBSCRIPTION_PRICING_CATALOG
AS 
SELECT ct.title subscription_name,ct.title_id subscription_title_id,
       ip.limits||' Days iQue NetC Subscription' subscription_desc, 
       ip.limits subscription_length,'day' subscription_interval, 
       ip.item_id subscription_item_id,--eip.item_ecard_type ecard_type,
       SUBSTR(ct.hex_title_id,1,8) channel_code, 
       ct.*,ip.item_price,ip.item_currency,
       ip.country_id,ip.country, ip.pricing_country_id,ip.pricing_country,
       ip.price_id, ip.item_price price,ip.item_currency currency, ip.ecard_type,
       ip.DEVICE_TYPE_ID,ip.region, 
       ip.purchase_start_date,ip.purchase_end_date,ip.rtype,ip.limits,ip.license_type,ip.limit_code, 
       ip.last_updated
FROM   CAS_COUNTRY_ITEM_PRICINGS ip,
       CONTENT_TITLE_CATALOG ct
 WHERE ip.title_id=ct.title_id 
--   AND ip.purchase_start_date<=sys_extract_utc(CURRENT_TIMESTAMP) 
   AND ip.country_id=0 
   AND ct.title_kind='SUBSCRIPTION' 
--   AND (ip.purchase_end_date IS NULL OR 
--        ip.purchase_end_date>=sys_extract_utc(CURRENT_TIMESTAMP))
/


DROP VIEW ECS_SUBS_CONTENT_CATALOG;
CREATE SYNONYM ECS_SUBS_CONTENT_CATALOG FOR SUBSCRIPTION_CONTENT_CATALOG;
CREATE OR REPLACE FORCE VIEW SUBSCRIPTION_CONTENT_CATALOG
AS 
SELECT ect.title_id subscription_title_id,ect.subscription_name, 
2 device_type_id,0 COUNTRY_ID, TO_DATE('01012000','MMDDYYYY') PURCHASE_START_DATE, TO_DATE(NULL) PURCHASE_END_DATE, 
ct.*,
cr.rating_system,cr.rating_value,cr.rating_age,cr.second_system,cr.second_value,cr.second_age,
cr.RATING_ATTRIBITS,cr.SECOND_ATTRIBITS
FROM content_title_catalog ct,content_title_countries cr,
	 (SELECT DISTINCT title_id,subscription_name,hex_title_id FROM SUBSCRIPTION_PRICING_CATALOG) ect
WHERE ct.title_id>ect.title_id 
AND cr.country_id(+)=0
AND cr.title_id(+)=ct.title_id
AND ct.title_id<bccutil.hex2dec(SUBSTR(ect.hex_title_id,1,8)||'99999999')
/

COMMENT ON TABLE SUBSCRIPTION_PRICING_CATALOG IS '/* $Id: CATALOGVIEWS.sql,v 1.11 2006/08/25 20:12:29 jchang Exp $*/';
COMMENT ON TABLE SUBSCRIPTION_CONTENT_CATALOG IS '/* $Id: CATALOGVIEWS.sql,v 1.11 2006/08/25 20:12:29 jchang Exp $*/';

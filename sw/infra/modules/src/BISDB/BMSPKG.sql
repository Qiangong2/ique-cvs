/*
 * (C) 2006, BroadOn Communications Corp, Inc.,
 * $Id: BMSPKG.sql,v 1.5 2006/08/10 23:47:00 jchang Exp $
 */

ALTER SESSION SET CURRENT_SCHEMA = BMS
/

CREATE OR REPLACE PACKAGE bbxml AUTHID CURRENT_USER
IS
/*
 * (C) 2006, BroadOn Communications Corp, Inc.,
 * $Id: BMSPKG.sql,v 1.5 2006/08/10 23:47:00 jchang Exp $
 */

/*  XML table DML and Query

Acceptable date format:  yyyy.MM.dd HH:mm:ss

<ROWSET>
   <ROW num="1">
      <REGION_ID>1</REGION_ID>
      <REGION_NAME>SHANGHAI</REGION_NAME>
      <CREATE_DATE>2002.12.10 23:01:55</CREATE_DATE>
   </ROW>
   <ROW num="2">
      <REGION_ID>2</REGION_ID>
      <REGION_NAME>BEIJING</REGION_NAME>
      <CREATE_DATE>2002.12.10 23:01:55</CREATE_DATE>
   </ROW>
   <ROW num="3">
      <REGION_ID>3</REGION_ID>
      <REGION_NAME>HONG KONG</REGION_NAME>
      <CREATE_DATE>2002.12.10 23:01:55</CREATE_DATE>
   </ROW>
</ROWSET>

TableUpdate and TableDelete uses primary key to perform update and delete.
Only those columns in the XML will be updated or inserted.

Returns number of ROWS processed.

Use with JDBC:
    ?=BBXML.TableInsert(?,?)
    ?=BBXML.TableUpdate(?,?)
    ?=BBXML.TableDelete(?,?)
    ?=BBXML.TableInsertUpdate(?,?)
        1st ?=total processed - bigdecimal
        2nd ?=XML- string
        3rd ?=table name - string

    ?=BBXML.GetQueryXML(?,?,?)
        1st ?=XML - string
        2nd ?=SQL statement - string
        3rd ?=total rows, null or -1 for all - bigdecimal (optional)
        4th ?=skip rows, null or -1 for all - bigdecimal (optional)

* java date format must be    yyyy.MM.dd HH:mm:ss

Autonomous Transaction, such as log tables,
 use ATInsert ATUpdate ATDelete ATInsertUpdate



XML query using BBXML: example to get 10 Content changes from last update
select bbxml.getqueryxml('SELECT * FROM CONTENT_TITLES WHERE LAST_UPDATED>=''2001.12.01 00:00:00'' ORDER BY LAST_UPDATED',10) from dual;
select bbxml.getqueryxml('SELECT CONTENT_ID,RELEASE_DATE,VALIDATE_DATE,PUBLISH_DATE,REVOKE_DATE,REPLACED_CONTENT_ID,LAST_UPDATED,CONTENT_OBJECT_TYPE,CONTENT_SIZE,CONTENT_CHECKSUM,CONTENT_OBJECT_NAME,CONTENT_OBJECT_VERSION
FROM CONTENT_OBJECTS WHERE LAST_UPDATED>=''2001.12.01 00:00:00'' ORDER BY LAST_UPDATED',10) from dual;

XML update using BBXML:  update region names from upper to proper names
x:=bbxml.TableUpdate(
'<ROWSET><ROW num="1"><REGION_ID>1</REGION_ID> <REGION_NAME>Shanghai</REGION_NAME></ROW><ROW num="2"><REGION_ID>2</REGION_ID><REGION_NAME>Beijing</REGION_NAME></ROW><ROW num="3"><REGION_ID>3</REGION_ID> <REGION_NAME>Hong Kong</REGION_NAME></ROW></ROWSET>','REGIONS');

XML insert using BBXML:  insert region 4 USA

x:=bbxml.TableInsert(
'<ROWSET><ROW num="1"><REGION_ID>4</REGION_ID><REGION_NAME>USA</REGION_NAME></ROW></ROWSET>','REGIONS');

XML insert or update using BBXML:  insert or update region 4 USA
x:=bbxml.TableInsertUpdate(
'<ROWSET><ROW num="1"><REGION_ID>4</REGION_ID><REGION_NAME>USA</REGION_NAME></ROW></ROWSET>','REGIONS');

XML delete using BBXML:  delete region 4
x:=bbxml.TableDelete(
'<ROWSET><ROW num="1"><REGION_ID>4</REGION_ID><REGION_NAME>USA</REGION_NAME></ROW></ROWSET>','REGIONS');


*/
   FUNCTION tableinsert (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER;

   FUNCTION tableupdate (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER;

   FUNCTION tabledelete (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER;

   FUNCTION tableinsertupdate (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER;

   FUNCTION tablexdml (
      xmldoc      IN   CLOB,
      tablename   IN   VARCHAR2,
      dmltype     IN   VARCHAR2
   )
      RETURN NUMBER;

   FUNCTION getqueryxml3 (
      i_sql      IN   VARCHAR2,
      numrows    IN   NUMBER DEFAULT NULL,
      skiprows   IN   NUMBER DEFAULT NULL
   )
      RETURN CLOB;
   FUNCTION getqueryxml2 (
      i_sql      IN   VARCHAR2,
      numrows    IN   NUMBER DEFAULT NULL,
      skiprows   IN   NUMBER DEFAULT NULL
   )
      RETURN CLOB;
   FUNCTION getqueryxml (
      i_sql      IN   VARCHAR2,
      numrows    IN   NUMBER DEFAULT NULL,
      skiprows   IN   NUMBER DEFAULT NULL
   )
      RETURN CLOB;

   FUNCTION getquerydtd (tablename IN VARCHAR2)
      RETURN CLOB;

   FUNCTION getqueryschema (tablename IN VARCHAR2)
      RETURN CLOB;

   FUNCTION atinsert (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER;

   FUNCTION atupdate (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER;

   FUNCTION atdelete (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER;

   FUNCTION atinsertupdate (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER;

   FUNCTION atinsertlog (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER;

   FUNCTION atupdatelog (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER;

   FUNCTION atdeletelog (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER;

   FUNCTION atinsertupdatelog (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER;

   FUNCTION getcolvalue (xmldoc CLOB, colchk VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION is_xmlrec (
      xmldoc      CLOB,
      tablename   VARCHAR2,
      chkcol      VARCHAR2 DEFAULT NULL
   )
      RETURN NUMBER;

   FUNCTION getnextpkey (tablename IN VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION getnextid (tablename IN VARCHAR2, colname IN VARCHAR2)
      RETURN NUMBER;

   FUNCTION getmaxmemorysize
      RETURN NUMBER
   IS
      LANGUAGE JAVA
      NAME 'oracle.aurora.vm.OracleRuntime.getMaxMemorySize() returns long';

   PROCEDURE setmaxmemorysize (num NUMBER)
   IS
      LANGUAGE JAVA
      NAME 'oracle.aurora.vm.OracleRuntime.setMaxMemorySize(long)';

   FUNCTION ConvertTimeStamp(msec IN NUMBER) RETURN DATE;

   FUNCTION ConvertTimeStamp(utctime IN DATE) RETURN NUMBER;

   PROCEDURE AuthDML(itab IN VARCHAR2);

   FUNCTION getpkcol (tablename IN VARCHAR2) RETURN string_list;

   FUNCTION is_rec (
      tablename   VARCHAR2,
      colname     string_list,
      colval      string_list,
      chkcol      VARCHAR2 DEFAULT NULL,
   chkval      VARCHAR2 DEFAULT NULL
   ) RETURN NUMBER;

   PROCEDURE get_tabname (
     iowner   VARCHAR2,
     iname    VARCHAR2,
     oOwner   OUT VARCHAR2,
     oName    OUT VARCHAR2
   );

END bbxml;
/

CREATE OR REPLACE PACKAGE BCCUTIL AS
/******************************************************************************

   (C) 2006, BroadOn Communications Corp, Inc.,
   $Id: BMSPKG.sql,v 1.5 2006/08/10 23:47:00 jchang Exp $

   NAME:       BCCUTIL
   PURPOSE:

   REVISIONS:
   Ver        DATE        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        3/3/2006     jchang          1. Created this PACKAGE.
   1.2        3/11/2006    jchang          2. Added ConverTimeStamp
   1.2        3/20/2006    jchang          2. Added hex2dec and dec2hex
******************************************************************************/

   FUNCTION ConvertTimeStamp(msec IN NUMBER) RETURN DATE;
   FUNCTION ConvertTimeStamp(utctime IN DATE) RETURN NUMBER;
   FUNCTION dec2hex(i_dec IN NUMBER,i_hexlength NUMBER DEFAULT 12) RETURN VARCHAR2;
   FUNCTION hex2dec(i_hex IN VARCHAR2) RETURN NUMBER;
   FUNCTION TRANSLATE(i_string IN VARCHAR2, i_lang IN VARCHAR2) RETURN VARCHAR2;

END BCCUTIL;
/

CREATE OR REPLACE PACKAGE CASOPS AS
/*
 * (C) 2006, BroadOn Communications Corp, Inc.,
 * $Id: BMSPKG.sql,v 1.5 2006/08/10 23:47:00 jchang Exp $
 */

   PROCEDURE Sync;

END casops;
/


CREATE OR REPLACE PACKAGE BCCUTIL AS
/******************************************************************************

   (C) 2006, BroadOn Communications Corp, Inc.,
   $Id: BMSPKG.sql,v 1.5 2006/08/10 23:47:00 jchang Exp $

   NAME:       BCCUTIL
   PURPOSE:

   REVISIONS:
   Ver        DATE        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        3/3/2006     jchang          1. Created this PACKAGE.
   1.2        3/11/2006    jchang          2. Added ConverTimeStamp
   1.2        3/20/2006    jchang          2. Added hex2dec and dec2hex
******************************************************************************/

   FUNCTION ConvertTimeStamp(msec IN NUMBER) RETURN DATE;
   FUNCTION ConvertTimeStamp(utctime IN DATE) RETURN NUMBER;
   FUNCTION dec2hex(i_dec IN NUMBER,i_hexlength NUMBER DEFAULT 12) RETURN VARCHAR2;
   FUNCTION hex2dec(i_hex IN VARCHAR2) RETURN NUMBER;
   FUNCTION TRANSLATE(i_string IN VARCHAR2, i_lang IN VARCHAR2) RETURN VARCHAR2;

END BCCUTIL;
/


CREATE OR REPLACE PACKAGE dputil
IS
/*
 * (C) 2002, RouteFree, Inc.,
 * $Id: BMSPKG.sql,v 1.5 2006/08/10 23:47:00 jchang Exp $
 */
   PROCEDURE post_xml (i_xmldoc CLOB, i_tablename VARCHAR2, i_pmode VARCHAR2);

   PROCEDURE process_incoming;
END;
/

CREATE OR REPLACE PACKAGE BMSOPS AS
/*
 * (C) 2006, BroadOn Communications Corp, Inc.,
 * $Id: BMSPKG.sql,v 1.5 2006/08/10 23:47:00 jchang Exp $
 */

   PROCEDURE CreatePerfData;
   PROCEDURE RemovePerfData;
   PROCEDURE CreateTestData;
   PROCEDURE RemoveTestData;

END bmsops;
/


DROP PUBLIC SYNONYM BCCUTIL;
CREATE PUBLIC SYNONYM BCCUTIL FOR BCCUTIL;
GRANT EXECUTE ON  BCCUTIL TO PUBLIC;
DROP PUBLIC SYNONYM BBXML;
CREATE PUBLIC SYNONYM BBXML FOR BBXML;
GRANT EXECUTE ON  BBXML TO PUBLIC;

ALTER SESSION SET CURRENT_SCHEMA = BMS
/

/*
 * (C) 2006, BroadOn Communications Corp, Inc.,
 * $Id: EXPSCHEMA.sql,v 1.2 2006/08/24 23:11:35 jchang Exp $
 */
ALTER SESSION SET CURRENT_SCHEMA = SYSTEM
/
CREATE USER EXP IDENTIFIED BY EXP DEFAULT TABLESPACE SHARED TEMPORARY TABLESPACE TEMP PROFILE DEFAULT ACCOUNT UNLOCK ;
  GRANT ALTER SESSION TO EXP;
  GRANT CREATE SESSION TO EXP;
  REVOKE UNLIMITED TABLESPACE FROM EXP;
ALTER USER EXP DEFAULT TABLESPACE SHARED QUOTA UNLIMITED ON SHARED;
ALTER USER EXP IDENTIFIED BY &&EXP_PASSWORD;

ALTER SESSION SET CURRENT_SCHEMA = BMS
/
GRANT SELECT,REFERENCES ON EXP_TRANSACTION_DETAILS TO EXP;
GRANT SELECT,REFERENCES ON GAME_PRICING_CATALOG TO EXP;
GRANT SELECT,REFERENCES ON POINTS_PRICING_CATALOG TO EXP;
GRANT SELECT,REFERENCES ON ECARD_GAME_CATALOG TO EXP;
GRANT SELECT,REFERENCES ON ECARD_POINTS_CATALOG TO EXP;
GRANT SELECT,REFERENCES ON SUBSCRIPTION_PRICING_CATALOG TO EXP;
GRANT SELECT,REFERENCES ON SUBSCRIPTION_CONTENT_CATALOG TO EXP;
GRANT SELECT,REFERENCES ON VC_ACCOUNTS TO EXP;
GRANT SELECT,REFERENCES ON ECARD_DETAILS TO EXP;
GRANT SELECT,REFERENCES ON ECARD_BATCH_DETAILS TO EXP;
GRANT SELECT,REFERENCES ON CAS_COUNTRY_ITEM_PRICINGS TO EXP;
GRANT SELECT,REFERENCES ON CONTENT_TITLE_COUNTRY_RELEASES TO EXP;
ALTER SESSION SET CURRENT_SCHEMA = EXP
/
CREATE SYNONYM TRANSACTION_DETAILS FOR bms.EXP_TRANSACTION_DETAILS;
CREATE SYNONYM GAME_PRICING_CATALOG FOR bms.GAME_PRICING_CATALOG;
CREATE SYNONYM POINTS_PRICING_CATALOG FOR bms.POINTS_PRICING_CATALOG;
CREATE SYNONYM ECARD_GAME_CATALOG FOR bms.ECARD_GAME_CATALOG;
CREATE SYNONYM ECARD_POINTS_CATALOG FOR bms.ECARD_POINTS_CATALOG;
CREATE SYNONYM VC_ACCOUNTS FOR bms.VC_ACCOUNTS;
CREATE SYNONYM ECARD_BATCH_DETAILS FOR bms.ECARD_BATCH_DETAILS;
CREATE SYNONYM ECARD_DETAILS FOR bms.ECARD_DETAILS;

ALTER SESSION SET CURRENT_SCHEMA = SYSTEM
/


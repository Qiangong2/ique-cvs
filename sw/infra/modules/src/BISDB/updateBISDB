#!/bin/bash
function usage
{
echo
echo "BroadOn Infrastructure Schema Script"
echo
echo "Usage: $script_name [-f <parameter file name>] "
echo "    f - parameter file name"
echo "        Examples:  LAB1.profile"
echo "    s - One or All Schema to create/update"
echo "        Examples:  BMS"
echo "        Default:  ALL"
echo
return
}

function proceed_script
{
echo -n "Proceed with BroadOn InfraDB $DB ($SC Schema) Create/Update on $CONNECT_STRING (Y/N)? "
read ANS
if [ -z "$ANS" ]
then ANS=N; export ANS
fi
ANS=$(echo $ANS | tr '[:lower:]' '[:upper:]')
if [ $ANS != "Y" ] ; then
 exit 0
fi
echo "`date`: Start $script_name.  Please Wait..."
}

function check_db
{
if [ ! -z "$CONNECT_STRING" ]; then
  echo "  Checking Database Connect String ... $CONNECT_STRING"
  tnserror $CONNECT_STRING
fi
echo " "
}
function tnserror
{
  echo
  echo -n "Testing Remote DB Connection using SQLPLUS... "
EXPECTED="Connected."
RESULT=$(sqlplus /nolog <<EOF
connect system/$SYSTEMPW@$1
exit
EOF)
  COMPARE=$(echo $RESULT | grep "${EXPECTED}" | wc -l)
  if [ $COMPARE -eq 0 ]; then
    echo "FAILED!"
    echo "Error Message:"
    echo "$RESULT" | grep "ORA"
    echo
    echo "Please make sure the remote DB $1 is up."
    echo
    return 13
  else echo "OK!"; echo
  fi
  return 0
}

function start_sqlscript
{
sqlplus /nolog <<EOF >> $script_name.$DB-$SC.$NOW.log 2>&1
/*
 * (C) 2006, BroadOn Communications, Inc.,
 * $Id: updateBISDB,v 1.4 2006/08/24 23:27:09 jchang Exp $
 */
spool $script_name.$DB-$SC.$NOW.spool
DEF EXP_PASSWORD="$EXPPW"
DEF BMS_PASSWORD="$BMSPW"
DEF ECS_PASSWORD="$ECSPW"
DEF CDS_PASSWORD="$CDSPW"
DEF CAS_PASSWORD="$CASPW"
DEF ETS_PASSWORD="$ETSPW"
DEF PAS_PASSWORD="$PASPW"
DEF IAS_PASSWORD="$IASPW"
DEF SYSTEM_PASSWORD="$SYSTEMPW"
DEF CONNECT_STRING="$CONNECT_STRING"

CONNECT SYSTEM/&&SYSTEM_PASSWORD@&&CONNECT_STRING
CREATE TABLESPACE SHARED DATAFILE SIZE 1M AUTOEXTEND ON NEXT 10M MAXSIZE UNLIMITED
LOGGING ONLINE PERMANENT
EXTENT MANAGEMENT LOCAL AUTOALLOCATE BLOCKSIZE 8K
SEGMENT SPACE MANAGEMENT AUTO FLASHBACK ON;

CREATE USER bms IDENTIFIED BY &&BMS_PASSWORD DEFAULT TABLESPACE SHARED TEMPORARY TABLESPACE TEMP PROFILE DEFAULT ACCOUNT UNLOCK ;
ALTER USER bms IDENTIFIED BY &&BMS_PASSWORD;
CREATE USER ecs IDENTIFIED BY &&ECS_PASSWORD DEFAULT TABLESPACE SHARED TEMPORARY TABLESPACE TEMP PROFILE DEFAULT ACCOUNT UNLOCK ;
CREATE USER pas IDENTIFIED BY &&PAS_PASSWORD DEFAULT TABLESPACE SHARED TEMPORARY TABLESPACE TEMP PROFILE DEFAULT ACCOUNT UNLOCK ;
CREATE USER ias IDENTIFIED BY &&IAS_PASSWORD DEFAULT TABLESPACE SHARED TEMPORARY TABLESPACE TEMP PROFILE DEFAULT ACCOUNT UNLOCK ;
CREATE USER cas IDENTIFIED BY &&CAS_PASSWORD DEFAULT TABLESPACE SHARED TEMPORARY TABLESPACE TEMP PROFILE DEFAULT ACCOUNT UNLOCK ;
CREATE USER cds IDENTIFIED BY &&CDS_PASSWORD DEFAULT TABLESPACE SHARED TEMPORARY TABLESPACE TEMP PROFILE DEFAULT ACCOUNT UNLOCK ;
CREATE USER sds IDENTIFIED BY sds DEFAULT TABLESPACE SHARED TEMPORARY TABLESPACE TEMP PROFILE DEFAULT ACCOUNT UNLOCK ;
CREATE USER kms IDENTIFIED BY kms DEFAULT TABLESPACE SHARED TEMPORARY TABLESPACE TEMP PROFILE DEFAULT ACCOUNT UNLOCK ;
CREATE USER ets IDENTIFIED BY &&ETS_PASSWORD DEFAULT TABLESPACE SHARED TEMPORARY TABLESPACE TEMP PROFILE DEFAULT ACCOUNT UNLOCK ;
GRANT DBA TO bms;
CONNECT SYSTEM/&&SYSTEM_PASSWORD@&&CONNECT_STRING
@@$SCHEMAFILE
CONNECT SYSTEM/&&SYSTEM_PASSWORD@&&CONNECT_STRING
REVOKE DBA FROM bms;
Prompt *** End of SQL Script! ***
EXIT
EOF
}



# ******************************************************************
# Check for valid input
# ******************************************************************
script_name=$(basename $0)
usage=$(usage)
NOW=`date +%Y%m%d%H%M%S`
export PATH=.:./sqlplus:/bin:/sbin:/usr/bin:/usr/local/bin
export LD_LIBRARY_PATH=./sqlplus

while getopts :f:s: opt
do
    case $opt in
        f)  FILE=$OPTARG;;
        s)  SC=$OPTARG;;
       \?)  usage;return 5;;
    esac
done

SC=$(echo $SC | tr '[:lower:]' '[:upper:]')
if [ -z "$SC" ]; then
    SC=ALL
fi
SCHEMAFILE=${SC}SCHEMA.sql

if [ -z "$FILE" ]; then
    echo
    echo "Parameter file required"
    usage
    exit 6
fi

if ! [ -f $SCHEMAFILE ]; then
   echo "Invalid valid schema name ($SC)."
   echo "$usage"
   exit 12
fi

if ! [ -f $FILE ]; then
   echo "Invalid valid parameter file ($FILE)."
   echo "$usage"
   exit 12
fi

. $FILE

check_db
proceed_script
start_sqlscript
exit 0



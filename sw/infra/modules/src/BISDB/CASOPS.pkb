/*
 * (C) 2006, BroadOn Communications Corp, Inc.,
 * $Id: CASOPS.pkb,v 1.1 2006/08/24 22:00:42 jchang Exp $
 */

CREATE OR REPLACE PACKAGE BODY casops
AS
/*
 * (C) 2006, BroadOn Communications Corp, Inc.,
 * $Id: CASOPS.pkb,v 1.1 2006/08/24 22:00:42 jchang Exp $
 */
   PROCEDURE sync
   IS
      tmpvar   INT;
	  tabs VARCHAR2(4000);
   BEGIN
      tabs:='CAS.CONTENT_CACHE,'||
	        'CAS.CONTENTS,'||
			'CAS.CONTENT_TITLES,'||
			'CAS.CONTENT_TITLE_LOCALES,'||
			'CAS.CONTENT_TITLE_COUNTRIES,'||
            'CAS.CONTENT_TITLE_OBJECTS,'||
			'CAS.CAS_PRICE_TAGS,'||
			'CAS.CAS_ITEM_PRICINGS';
      DBMS_SNAPSHOT.REFRESH(tabs);
	  COMMIT;
   END;
END casops;
/

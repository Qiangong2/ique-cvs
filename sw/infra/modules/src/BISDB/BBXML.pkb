/*
 * (C) 2006, BroadOn Communications Corp, Inc.,
 * $Id: BBXML.pkb,v 1.1 2006/08/24 22:00:42 jchang Exp $
 */

CREATE OR REPLACE PACKAGE BODY bbxml
IS
/*
 * (C) 2006, BroadOn Communications Corp, Inc.,
 * $Id: BBXML.pkb,v 1.1 2006/08/24 22:00:42 jchang Exp $
 */
   x   NUMBER;
   EPOCH CONSTANT DATE:=TO_DATE('00:00:00 1/1/1970','HH24:MI:SS MM/DD/YYYY');

   CURSOR ctab (powner VARCHAR2, pname VARCHAR2)
   IS
      SELECT owner table_owner, table_name, NULL db_link,
             'TABLE' object_type
        FROM ALL_TABLES
       WHERE owner = powner AND table_name = pname;

   CURSOR cSyn (powner VARCHAR2, pname VARCHAR2)
   IS
      SELECT table_owner, table_name, db_link, o.object_type
        FROM ALL_SYNONYMS s, ALL_OBJECTS o
       WHERE s.table_owner = o.owner (+)
         AND o.object_name (+) = s.table_name
         AND s.owner = powner
         AND s.synonym_name = pname;

   CURSOR cPkey (tablename VARCHAR2, tableowner VARCHAR2)
   IS
      SELECT b.column_name
        FROM ALL_CONSTRAINTS a, ALL_CONS_COLUMNS b
       WHERE a.constraint_type = 'P'
         AND a.owner = b.owner
         AND a.constraint_name = b.constraint_name
         AND a.owner = tableowner
         AND a.table_name = tablename
       ORDER BY b.position;

   CURSOR c1 (tablename VARCHAR2)
   IS
      SELECT b.column_name
        FROM USER_CONSTRAINTS a, USER_CONS_COLUMNS b, USER_TABLES c
       WHERE a.table_name = c.table_name
         AND a.constraint_type = 'P'
         AND a.constraint_name = b.constraint_name
         AND c.table_name = tablename;

   CURSOR c2 (tablename VARCHAR2)
   IS
      SELECT b.column_name
        FROM ALL_CONSTRAINTS a, ALL_CONS_COLUMNS b, USER_SYNONYMS c
       WHERE a.table_name = c.table_name
         AND a.constraint_type = 'P'
         AND a.owner = c.table_owner
         AND a.owner = b.owner
         AND a.constraint_name = b.constraint_name
         AND c.synonym_name = tablename;

   CURSOR c3 (tablename VARCHAR2)
   IS
      SELECT b.column_name
        FROM ALL_CONSTRAINTS a, ALL_CONS_COLUMNS b, ALL_SYNONYMS c
       WHERE a.table_name = c.table_name
         AND a.constraint_type = 'P'
         AND a.owner = c.table_owner
         AND a.owner = b.owner
         AND a.constraint_name = b.constraint_name
         AND c.synonym_name = tablename
         AND c.owner = 'PUBLIC';

   FUNCTION atinsertlog (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      x := tableinsert (xmldoc, tablename);
      COMMIT;
      RETURN x;
   EXCEPTION WHEN OTHERS THEN NULL;
   END atinsertlog;

   FUNCTION atupdate (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      x := tableupdate (xmldoc, tablename);
      COMMIT;
      RETURN x;
   EXCEPTION WHEN OTHERS THEN NULL;
   END atupdate;

   FUNCTION atdelete (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      x := tabledelete (xmldoc, tablename);
      COMMIT;
      RETURN x;
   END atdelete;

   FUNCTION atinsertupdate (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      x := tabledelete (xmldoc, tablename);
      COMMIT;
      RETURN x;
   END atinsertupdate;

   FUNCTION atinsert (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      x := tableinsert (xmldoc, tablename);
      COMMIT;
      RETURN x;
   END atinsert;

   FUNCTION atupdatelog (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      x := tableupdate (xmldoc, tablename);
      COMMIT;
      RETURN x;
   EXCEPTION WHEN OTHERS THEN NULL;
   END atupdatelog;

   FUNCTION atdeletelog (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      x := tabledelete (xmldoc, tablename);
      COMMIT;
      RETURN x;
   EXCEPTION WHEN OTHERS THEN NULL;
   END atdeletelog;

   FUNCTION atinsertupdatelog (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      x := tableupdate (xmldoc, tablename);
      COMMIT;
      RETURN x;
   EXCEPTION WHEN OTHERS THEN NULL;
   END atinsertupdatelog;

   FUNCTION tableinsert (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER
   IS
   BEGIN
      RETURN tablexdml (xmldoc, tablename, 'INS');
   END tableinsert;

   FUNCTION tableupdate (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER
   IS
   BEGIN
      RETURN tablexdml (xmldoc, tablename, 'UPD');
   END tableupdate;

   FUNCTION tabledelete (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER
   IS
   BEGIN
      RETURN tablexdml (xmldoc, tablename, 'DEL');
   END tabledelete;

   FUNCTION tableinsertupdate (xmldoc IN CLOB, tablename IN VARCHAR2)
      RETURN NUMBER
   IS
      ROWS   NUMBER;
   BEGIN
      ROWS := tableupdate (xmldoc, tablename);
      IF ROWS = 0
      THEN
         ROWS := tableinsert (xmldoc, tablename);
      END IF;
      RETURN ROWS;
/*   EXCEPTION
      WHEN OTHERS
      THEN
         IF SQLCODE = -29532 or SQLCODE=-1
         THEN
            ROWS := tableupdate (xmldoc, tablename);
            RETURN ROWS;
         END IF;

         RAISE;*/
   END tableinsertupdate;

   FUNCTION tablexdml (
      xmldoc      IN   CLOB,
      tablename   IN   VARCHAR2,
      dmltype     IN   VARCHAR2
   )
      RETURN NUMBER
   IS
      updctx       dbms_xmlsave.ctxtype;
      COLS         string_list;
      ROWS         INT:=0;
      i            INT:=0;
      p            xmlparser.parser     := xmlparser.newparser;
      doc          xmldom.domdocument;
      elemlist     xmldom.domnodelist;
      elemlength   NUMBER;
      colname      VARCHAR2 (64);
      doclength    NUMBER := NVL(dbms_lob.getlength(xmldoc),0);
   BEGIN
    IF doclength <= 255 THEN
 IF doclength=0 OR
           NVL(REPLACE(LTRIM(RTRIM(xmldoc)),' '),'<ROWSET/>') IN
           ('<ROWSET/>',
            '<ROWSET></ROWSET>',
            '<ROW/>',
            '<ROW></ROW>',
            '<ROWSET><ROW></ROW></ROWSET>') THEN
          RETURN 0; -- null xml not allowed
        END IF;
      END IF;
      COLS := getpkcol(tablename);

      IF COLS IS NULL THEN
        RETURN -11; -- no primary key or table found
      ELSIF COLS.COUNT > 0
      THEN
         updctx := dbms_xmlsave.newcontext (tablename); -- get the context
         dbms_xmlsave.setdateformat (updctx, 'yyyy.MM.dd HH:mm:ss');
         dbms_xmlsave.clearupdatecolumnlist (updctx); -- clear the update settings..
         dbms_xmlsave.clearkeycolumnlist (updctx);

         FOR i IN 1..COLS.COUNT LOOP
            dbms_xmlsave.setkeycolumn (updctx, COLS(i)); -- set key column
         END LOOP;

         IF dmltype = 'INS'
         THEN
            xmlparser.parseclob (p, xmldoc);
            doc := xmlparser.getdocument (p);
            elemlist := xmldom.getelementsbytagname (doc, '*');
            elemlength := xmldom.getlength (elemlist);

            FOR i IN 0 .. elemlength - 1
            LOOP
               colname :=
                       UPPER (xmldom.getnodename (xmldom.item (elemlist, i)));

               IF colname NOT IN ('ROW', 'ROWSET')
               THEN
                  dbms_xmlsave.setupdatecolumn (updctx, colname);
               END IF;
            END LOOP;

            xmldom.freedocument (doc);
            ROWS := dbms_xmlsave.insertxml (updctx, xmldoc); -- this inserts the document
         ELSIF dmltype = 'UPD'
         THEN
            ROWS := dbms_xmlsave.UPDATEXML (updctx, xmldoc); -- update the table
         ELSIF dmltype = 'DEL'
         THEN
            ROWS := dbms_xmlsave.deletexml (updctx, xmldoc); -- delete from table
         END IF;

         dbms_xmlsave.closecontext (updctx); -- close the context..!
      END IF;

      RETURN ROWS;
   END tablexdml;

   FUNCTION getqueryxml (
      i_sql      IN   VARCHAR2,
      numrows    IN   NUMBER DEFAULT NULL,
      skiprows   IN   NUMBER DEFAULT NULL
   )
      RETURN CLOB
   IS
      ctx   DBMS_XMLQUERY.ctxhandle;
      xml   CLOB;
   BEGIN
      ctx := DBMS_XMLQUERY.newcontext (i_sql);
      DBMS_XMLQUERY.setdateformat (ctx, 'yyyy.MM.dd HH:mm:ss');

      IF numrows IS NOT NULL AND numrows >= 0
      THEN
         DBMS_XMLQUERY.setmaxrows (ctx, numrows);
      END IF;

      IF skiprows IS NOT NULL AND skiprows >= 0
      THEN
         DBMS_XMLQUERY.setskiprows (ctx, skiprows);
      END IF;

      xml := DBMS_XMLQUERY.getxml (ctx);
      DBMS_XMLQUERY.closecontext (ctx);
      RETURN xml;
   END;

   FUNCTION getqueryxml2 (
      i_sql      IN   VARCHAR2,
      numrows    IN   NUMBER DEFAULT NULL,
      skiprows   IN   NUMBER DEFAULT NULL
   )
      RETURN CLOB
   IS
   BEGIN
     RETURN getqueryxml(i_sql,numrows,skiprows);
   END;


   FUNCTION getqueryxml3 (
      i_sql      IN   VARCHAR2,
      numrows    IN   NUMBER DEFAULT NULL,
      skiprows   IN   NUMBER DEFAULT NULL
   )
      RETURN CLOB
   IS
      ctx   DBMS_XMLGEN.ctxhandle;
      xml   CLOB;
   BEGIN
      DBMS_SESSION.set_nls ('nls_date_format', '''YYYY.MM.DD HH24:MI:SS''');
      ctx := DBMS_XMLGEN.newcontext (i_sql);
--      DBMS_XMLQUERY.setdateformat (ctx, 'yyyy.MM.dd HH:mm:ss');
      DBMS_XMLGEN.setConvertSpecialChars(ctx, FALSE);
      IF numrows IS NOT NULL AND numrows >= 0
      THEN
         DBMS_XMLGEN.setmaxrows (ctx, numrows);
      END IF;

      IF skiprows IS NOT NULL AND skiprows >= 0
      THEN
         DBMS_XMLGEN.setskiprows (ctx, skiprows);
      END IF;

      xml := DBMS_XMLGEN.getxml (ctx);
      DBMS_XMLGEN.closecontext (ctx);
      IF xml IS NULL THEN
        RETURN '<ROWSET/>';
      END IF;
      RETURN xml;
   END;


   FUNCTION getquerydtd (tablename IN VARCHAR2)
      RETURN CLOB
   IS
      ctx   DBMS_XMLQUERY.ctxhandle;
      xml   CLOB;
   BEGIN
      ctx :=
         DBMS_XMLQUERY.newcontext ('select * from ' || tablename
                                   || ' where 1=2'
                                  );
      DBMS_XMLQUERY.setrowsettag (ctx, UPPER (tablename));
      DBMS_XMLQUERY.setrowtag (ctx, '');
      DBMS_XMLQUERY.setrowidattrname (ctx, '');
      xml:=DBMS_XMLQUERY.getxml (ctx, 1);
      DBMS_XMLQUERY.closecontext (ctx);
      RETURN xml;
   END;

   FUNCTION getqueryschema (tablename IN VARCHAR2)
      RETURN CLOB
   IS
      ctx   DBMS_XMLQUERY.ctxhandle;
      xml   CLOB;
   BEGIN
      ctx :=
         DBMS_XMLQUERY.newcontext ('select * from ' || tablename
                                   || ' where 1=2'
                                  );
      DBMS_XMLQUERY.setrowsettag (ctx, UPPER (tablename));
      DBMS_XMLQUERY.setrowtag (ctx, UPPER (tablename));
      DBMS_XMLQUERY.setrowidattrname (ctx, '');
      xml:=DBMS_XMLQUERY.getxml (ctx, 2);
      DBMS_XMLQUERY.closecontext (ctx);
      RETURN xml;
   END;

   FUNCTION getcolvalue (xmldoc CLOB, colchk VARCHAR2)
      RETURN VARCHAR2
   IS
--      updctx       dbms_xmlsave.ctxtype;
      p            xmlparser.parser     := xmlparser.newparser;
      doc          xmldom.domdocument;
      elemlist     xmldom.domnodelist;
      n            xmldom.domnode;
      elemlength   NUMBER;
      colname      VARCHAR2 (2000);
   BEGIN
      xmlparser.parseclob (p, xmldoc);
      doc := xmlparser.getdocument (p);
      elemlist := xmldom.getelementsbytagname (doc, '*');
      elemlength := xmldom.getlength (elemlist);

      FOR i IN 0 .. elemlength - 1
      LOOP
         n := xmldom.item (elemlist, i);

--         DBMS_OUTPUT.put_line ('Node Elements: ' || xmldom.getnodename (n));

         IF xmldom.getnodename (n) = colchk
         THEN
            n := xmldom.getfirstchild (n);
            colname := xmldom.getnodevalue (n);
         END IF;
      END LOOP;

      xmldom.freedocument (doc);
      RETURN colname;
   END;

   FUNCTION is_xmlrec (
      xmldoc      CLOB,
      tablename   VARCHAR2,
      chkcol      VARCHAR2 DEFAULT NULL
   )
      RETURN NUMBER
   IS
      tmpvar     INT:=0;
      i          INT:=0;
      colval     string_list;
   colname    string_list;
   chkval     VARCHAR2(255);
   BEGIN
      colname := getpkcol(tablename);
      IF colname IS NULL THEN
     RETURN -11;
      ELSIF colname.COUNT > 0 THEN
       colval:=string_list(NULL);
        FOR i IN 1..colname.COUNT LOOP
         IF i > 1 THEN colval.EXTEND; END IF;
          colval (i) := getcolvalue (xmldoc, colname(i));
          IF colval (i) IS NULL THEN
            tmpvar:=-12; -- key column value cannot be null
          END IF;
        END LOOP;
      END IF;
      IF tmpvar < 0 THEN RETURN tmpvar; END IF;
   IF chkcol IS NOT NULL THEN
     chkval:=getcolvalue (xmldoc,chkcol);
   END IF;
      RETURN is_rec(tablename,colname,colval,chkcol,chkval);
   END;

   FUNCTION getnextpkey (tablename IN VARCHAR2)
      RETURN VARCHAR2
   IS
      pval     VARCHAR2 (2000);
      p_next   NUMBER;
      p_col    VARCHAR2 (32);
      i        INT             := 0;
      r1       c1%ROWTYPE;
   BEGIN
      FOR r1 IN c1 (tablename)
      LOOP
         i := i + 1;
         p_col := r1.column_name;
      END LOOP;

      IF i = 0
      THEN
         FOR r1 IN c2 (tablename)
         LOOP
            i := i + 1;
            p_col := r1.column_name;
         END LOOP;
      END IF;

      IF i = 0
      THEN
         FOR r1 IN c3 (tablename)
         LOOP
            i := i + 1;
            p_col := r1.column_name;
         END LOOP;
      END IF;

      IF i = 1
      THEN
         p_next := getnextid (tablename, p_col);
         pval := 'NONE';
      ELSIF i = 0
      THEN
         p_next := -1;
         pval := 'NO KEY OR NO TABLE FOUND';
      ELSE
         p_next := -2;
         pval := 'TOO MANY KEY COLUMNS FOR TABLE';
      END IF;
      RETURN '<ROWSET><ROW><TABNAME>'||tablename||'</TABNAME>'||
             '<KEYNAME>'||p_col||'</KEYNAME>'||
             '<NEXTKEY>'||p_next||'</NEXTKEY>'||
             '<ERRMSG>'||pval||'</ERRMSG></ROW></ROWSET>';
   END;

   FUNCTION getnextid (tablename IN VARCHAR2, colname IN VARCHAR2)
      RETURN NUMBER
   IS
      p_next   NUMBER;
   BEGIN
      EXECUTE IMMEDIATE    'declare pid number; '
                        || 'cursor cid is '
                        || 'select /*+ index_desc(tt) */ '
                        || colname
                        || ' nextid from '
                        || tablename
                        || ' tt order by '
                        || colname
                        || ' desc; '
                        || 'begin '
                        || 'open cid; '
                        || 'fetch cid into pid; '
                        || 'if cid%notfound then pid:=0; end if; '
                        || 'close cid; :nextid:=pid+1; end;'
         USING          OUT p_next;
      RETURN p_next;
   END;

   FUNCTION ConvertTimeStamp(msec IN NUMBER) RETURN DATE IS
   BEGIN RETURN EPOCH+msec/86400000; END;

   FUNCTION ConvertTimeStamp(utctime IN DATE) RETURN NUMBER IS
   BEGIN RETURN (utctime-EPOCH)*86400000; END;

   PROCEDURE AuthDML(itab IN VARCHAR2) IS
   p_mod VARCHAR2(255);
   p_act VARCHAR2(255);
   pmesg VARCHAR2(255):='data manipulation operation not legal on '||itab;
   BEGIN
      dbms_application_info.read_module(p_mod,p_act);
      IF p_mod<>'BCC' OR p_act<>'DML' THEN
         RAISE_APPLICATION_ERROR(-20009,pmesg);
      END IF;
   END;

   FUNCTION getpkcol (tablename IN VARCHAR2) RETURN string_list
   IS
   p_retval string_list;
   ROWS INT;
   i INT:=1;
   r c1%ROWTYPE;
   pOwner VARCHAR2(32);
   pName VARCHAR2(32);
   BEGIN
      get_tabname(USER,tablename,pOwner,pName);
      OPEN cPkey (pName,pOwner);
      FETCH cPkey INTO r.column_name;
--      CLOSE cPkey;
--	  RETURN string_list(pOwner||'.'||pName);
      IF r.column_name IS NULL THEN
         ROWS := -1;
      ELSE
         ROWS := 1;
      END IF;

      IF ROWS > 0 THEN
         p_retval:=string_list(NULL);
         p_retval(i):=r.column_name;
	     WHILE TRUE LOOP
            FETCH cPkey INTO r.column_name;
			EXIT WHEN cPkey%NOTFOUND;
            i:=i+1;
            p_retval.EXTEND;
            p_retval(i):=r.column_name;
         END LOOP;
      END IF;
      CLOSE cPkey;
      RETURN p_retval;
   END;


   FUNCTION is_rec (
      tablename   VARCHAR2,
      colname     string_list,
      colval      string_list,
      chkcol      VARCHAR2 DEFAULT NULL,
   chkval      VARCHAR2 DEFAULT NULL
   )
      RETURN NUMBER
   IS
   psql       VARCHAR2 (2000)
         :=    'select max('
            || NVL (chkcol, 'sysdate')
            || '),count(*) from '
            || tablename
            || ' where 1=1';
   isrec INT;
   i INT;
   result VARCHAR2(255);
   curval VARCHAR2(255);
   BEGIN
   IF colname IS NULL THEN
     RETURN -2;  -- null col name list
   ELSIF colval IS NULL THEN
     RETURN -3;  -- null col value list
   ELSIF colname.COUNT<>colval.COUNT THEN
     RETURN -4;  -- unmatched col name,val pair
   ELSIF colname.COUNT>7 THEN
     RETURN -5;  -- limit is 8 columns
   ELSE
     FOR i IN 1..colname.COUNT LOOP
       psql:= psql||' and '||colname(i)||'=:'||colname(i);
     END LOOP;
     DBMS_SESSION.set_nls ('nls_date_format', '''YYYY.MM.DD HH24:MI:SS''');
--     DBMS_OUTPUT.put_line ('colname.count=' || colname.count);
  IF colname.COUNT=1 THEN
       EXECUTE IMMEDIATE psql INTO RESULT, isrec
      USING colval(1);
  ELSIF colname.COUNT=2 THEN
       EXECUTE IMMEDIATE psql INTO RESULT, isrec
      USING colval(1),colval(2);
  ELSIF colname.COUNT=3 THEN
       EXECUTE IMMEDIATE psql INTO RESULT, isrec
      USING colval(1),colval(2),colval(3);
  ELSIF colname.COUNT=4 THEN
       EXECUTE IMMEDIATE psql INTO RESULT, isrec
      USING colval(1),colval(2),colval(3),colval(4);
  ELSIF colname.COUNT=5 THEN
       EXECUTE IMMEDIATE psql INTO RESULT, isrec
      USING colval(1),colval(2),colval(3),colval(4),colval(5);
  ELSIF colname.COUNT=6 THEN
       EXECUTE IMMEDIATE psql INTO RESULT, isrec
      USING colval(1),colval(2),colval(3),colval(4),colval(5),colval(6);
  ELSIF colname.COUNT=7 THEN
       EXECUTE IMMEDIATE psql INTO RESULT, isrec
      USING colval(1),colval(2),colval(3),colval(4),colval(5),colval(6),colval(7);
  ELSIF colname.COUNT=8 THEN
       EXECUTE IMMEDIATE psql INTO RESULT, isrec
      USING colval(1),colval(2),colval(3),colval(4),colval(5),colval(6),colval(7),colval(8);
  END IF;
--     DBMS_OUTPUT.put_line ('sql=' || psql || ' USING ');
--     FOR i in 1..colval.count LOOP
--       dbms_output.put(colval(i)||',');
--     END LOOP;
--     DBMS_OUTPUT.put_line ('isrec=' || isrec);
--     DBMS_OUTPUT.put_line ('result=' || RESULT);
     IF chkcol IS NOT NULL THEN
       BEGIN
         curval:=TO_CHAR(TO_DATE(chkval,'YYYY.MM.DD HH24:MI:SS'));
       EXCEPTION
       WHEN OTHERS THEN
         curval:=chkval;
       END;
--       DBMS_OUTPUT.put_line ('curval=' || curval);

       IF RESULT IS NULL AND isrec = 0 THEN
         RETURN 0;
       ELSIF RESULT IS NULL AND isrec > 0 THEN
         RETURN 1;
       ELSIF RESULT = curval THEN
         RETURN 2;
       ELSIF curval > RESULT THEN
         RETURN 1;
       ELSIF curval < RESULT THEN
         RETURN 3;
       ELSE RETURN -1;
       END IF;
     ELSE
       IF isrec > 0 THEN
         RETURN 1;
       ELSE
         RETURN 0;
       END IF;
     END IF;
   END IF;
   END;

   PROCEDURE get_tabname (
     iowner   IN VARCHAR2,
     iname    IN VARCHAR2,
     oOwner   OUT VARCHAR2,
     oName    OUT VARCHAR2
   )
   IS
     r1   cSyn%ROWTYPE;
     j    INT          := 0;
   BEGIN
     OPEN ctab (iowner, iname);
     FETCH ctab INTO r1;
     CLOSE ctab;
     oOwner:=r1.table_owner;
     oName:=r1.table_name;
     IF r1.table_name IS NULL THEN
        OPEN cSyn (iowner, iname);
        FETCH cSyn INTO r1;
        IF cSyn%NOTFOUND
        THEN
           CLOSE cSyn;
           OPEN cSyn ('PUBLIC', iname);
           FETCH cSyn INTO r1;
        END IF;
        CLOSE cSyn;

        oOwner:=r1.table_owner;
	    oName:=r1.table_name;
        WHILE NVL(r1.object_type,'NULL') != 'TABLE'
        LOOP
           OPEN cSyn (oOwner, oName);
           FETCH cSyn INTO r1;
		   j:=j+1;
		   IF cSyn%NOTFOUND THEN
		     CLOSE cSyn;
			 EXIT;
		   END IF;
           CLOSE cSyn;
           oOwner:=r1.table_owner;
           oName:=r1.table_name;
        END LOOP;
     END IF;
   END;

END bbxml;
/


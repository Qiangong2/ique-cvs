/*
 * (C) 2006, BroadOn Communications Corp, Inc.,
 * $Id: DPUTIL.pkb,v 1.1 2006/08/24 22:00:42 jchang Exp $
 */

CREATE OR REPLACE PACKAGE BODY dputil
AS
/*
 * (C) 2006, BroadOn Communications Corp, Inc.,
 * $Id: DPUTIL.pkb,v 1.1 2006/08/24 22:00:42 jchang Exp $
 */
   PROCEDURE post_xml (i_xmldoc CLOB, i_tablename VARCHAR2, i_pmode VARCHAR2)
   IS
   BEGIN
      INSERT INTO incoming
                  (xmldoc, tablename, pmode
                  )
           VALUES (i_xmldoc, i_tablename, i_pmode
                  );
   END;

   PROCEDURE process_incoming
   IS
      CURSOR c1
      IS
         SELECT *
           FROM incoming;

      tmpvar   NUMBER := 0;
   BEGIN
      FOR r1 IN c1
      LOOP
         IF r1.pmode = 'INS'
         THEN
            tmpvar := bbxml.tableinsert (r1.xmldoc, r1.tablename);
         ELSIF r1.pmode = 'UPD'
         THEN
            tmpvar := bbxml.tableupdate (r1.xmldoc, r1.tablename);
         ELSIF r1.pmode = 'DEL'
         THEN
            tmpvar := bbxml.tabledelete (r1.xmldoc, r1.tablename);
         ELSIF r1.pmode = 'INSUPD'
         THEN
            tmpvar := bbxml.tableinsertupdate (r1.xmldoc, r1.tablename);
         END IF;
      END LOOP;
   END process_incoming;
END dputil;
/

--ALTER SESSION SET CURRENT_SCHEMA=CDS
--ALTER SESSION SET CURRENT_SCHEMA=CAS
CREATE SYNONYM CONTENT_OBJECTS FOR CONTENT_OBJECTS_V;

CREATE OR REPLACE FORCE VIEW CONTENT_OBJECTS_V
AS 
SELECT
  c.CONTENT_ID,
  c.CONTENT_NAME,
  c.CONTENT_TYPE,
  c.PUBLISH_DATE,
  c.REVOKE_DATE,
  c.REPLACED_CONTENT_ID,
  c.LAST_UPDATED,
  cc.CONTENT_OBJECT_TYPE,
  cc.CONTENT_SIZE,
  cc.CONTENT_OBJECT_NAME,
  cc.CONTENT_OBJECT_VERSION,
  cc.CONTENT_CHECKSUM,
  cc.CONTENT_OBJECT,
  c.ETICKET_OBJECT,
  c.MIN_UPGRADE_VERSION,
  c.UPGRADE_CONSENT
FROM CONTENTS c, CONTENT_CACHE cc
WHERE c.content_checksum=cc.content_checksum
/

CREATE OR REPLACE FORCE VIEW CONTENT_TITLE_LAST_VERSION
AS 
SELECT cto.title_id,MAX(LPAD(LTRIM(TO_CHAR(cto.title_id,RPAD('X',16,'X'))),16,'0')) hex_title_id,
        cto.create_date title_version_date,
        MAX(DECODE(co.content_object_type,'TMD',co.content_object_version)) title_version,
        MAX(DECODE(co.content_object_type,'TMD',co.content_id)) tmd_content_id,
        MAX(DECODE(co.content_object_type,'ETKM',co.content_id)) etkm_content_id,
        MAX(DECODE(co.content_object_type,'TMD',co.content_checksum)) tmd_content_checksum,
        MAX(met.content_checksum) met_content_checksum,
        SUM(co.content_size) title_size,
        SUM(DECODE(MOD(co.content_size,16384),0,co.content_size,CEIL(co.content_size/16384)*16384)) approx_size
   FROM content_title_objects cto, content_objects co, content_objects met
  WHERE cto.content_id=co.content_id AND co.revoke_date IS NULL
    AND cto.create_date=(SELECT MAX(create_date) FROM content_title_objects WHERE cto.title_id=title_id)
    AND met.content_id(+)=TO_NUMBER(LPAD(LTRIM(TO_CHAR(cto.title_id,RPAD('X',16,'X'))),16,'0')||'FFFFFFFE','XXXXXXXXXXXXXXXXXXXXXXXX')
  GROUP BY cto.title_id,cto.create_date
/

CREATE OR REPLACE FORCE VIEW LATEST_CONTENT_TITLE_OBJECTS
AS 
 SELECT cto.title_id,cto.create_date title_version_date,
        co.content_id,co.content_object_type,co.content_checksum,co.content_size,
		co.content_object_name,co.content_object_version,co.content_name,co.content_type
   FROM content_title_objects cto, content_objects co
  WHERE cto.content_id=co.content_id AND co.revoke_date IS NULL 
    AND create_date=(SELECT MAX(create_date) FROM content_title_objects WHERE cto.title_id=title_id)
/
/*
 Bit 0 - Cacheable
 Bit 1 - Preloadable
 Bit 2 - Classic Controller
 Bit 3 - RVL Controller
 Bit 4-5 - Reserved
 Bit 6 - Need DVD
 Bit 7 - Reserved
 Bit 8-15 - Minimum # of Players
 Bit 16-23 - Maximum # of Players
*/
CREATE OR REPLACE FORCE VIEW CONTENT_TITLE_CATALOG
AS 
SELECT ct.TITLE_ID,cto.hex_title_id, ' ' mask, 
ct.title,ct.hidden_title,ct.title_type,dt.title_kind,cto.title_version,cto.title_version_date,
DECODE(TITLE_KIND,'SUBSCRIPTION',BITAND(FLOOR(ATTRIBITS/POWER(2,0)),POWER(2,24)-1),NULL) max_checkouts, 
--DECODE(TITLE_TYPE,'POINTS',BITAND(FLOOR(ATTRIBITS/POWER(2,0)),POWER(2,24)-1),NULL) refill_points, 
DECODE(TITLE_KIND,'GAME',BITAND(FLOOR(NVL(ATTRIBITS,0)/POWER(2,0)),1)) cacheable, 
DECODE(TITLE_KIND,'GAME',BITAND(FLOOR(NVL(ATTRIBITS,0)/POWER(2,1)),1)) preloadable, 
DECODE(TITLE_KIND,'GAME',BITAND(FLOOR(NVL(ATTRIBITS,0)/POWER(2,2)),1)) classic_controller, 
DECODE(TITLE_KIND,'GAME',BITAND(FLOOR(NVL(ATTRIBITS,0)/POWER(2,3)),1)) rvl_controller, 
DECODE(TITLE_KIND,'GAME',BITAND(FLOOR(NVL(ATTRIBITS,0)/POWER(2,6)),1)) need_dvd,
DECODE(TITLE_KIND,'GAME',BITAND(FLOOR(NVL(ATTRIBITS,0)/POWER(2,8)),255)) min_players,
DECODE(TITLE_KIND,'GAME',BITAND(FLOOR(NVL(ATTRIBITS,0)/POWER(2,16)),255)) max_players,
attribits, 
cto.tmd_content_id,cto.etkm_content_id,cto.tmd_content_checksum,cto.met_content_checksum, 
cto.title_size,cto.approx_size,ct.default_locale, 
ct.CATEGORY,ct.platform,ct.publisher,ct.developer,ct.publish_date,ct.release_date,ct.description,ct.product_code,ct.approve_date,ct.stable_version
--,CAST(MULTISET(SELECT rating_system,rating_value,rating_age 
--                FROM content_title_ratings WHERE title_id=ct.title_id) 
--  AS ECS_content_ratings) rating 
FROM content_titles ct,
     device_title_types dt,
     content_title_last_version cto
WHERE ct.title_id=cto.title_id(+)
  AND dt.title_type (+)=ct.title_type
/
CREATE OR REPLACE FORCE VIEW CONTENT_TITLE_RATINGS AS
SELECT TITLE_ID,COUNTRY_ID,RATING_SYSTEM,RATING_VALUE,RATING_AGE,SECOND_SYSTEM,SECOND_VALUE,SECOND_AGE,
RATING_ATTRIBITS,SECOND_ATTRIBITS
FROM CONTENT_TITLE_COUNTRIES
/
CREATE OR REPLACE FORCE VIEW CONTENT_TITLE_COUNTRY_LOCALES
AS 
SELECT c.country,c.region,cl.language_code, 
       cl.locale supported_locale,c.default_locale country_default,--ct.default_locale title_default, 
	   DECODE((SELECT COUNT(*) FROM content_title_locales 
	            WHERE title_id=ctc.title_id AND locale=cl.locale AND activate_date IS NOT NULL),1,cl.locale, 
	   DECODE((SELECT COUNT(*) FROM content_title_locales 
	            WHERE title_id=ctc.title_id AND locale=c.default_locale AND activate_date IS NOT NULL),1, 
	          c.default_locale,NULL)) locale, 
       ctc.*,c.new_duration
  FROM country_locales cl,countries c,content_title_countries ctc 
 WHERE ctc.country_id=c.country_id 
   AND cl.country_id=c.country_id
/


CREATE OR REPLACE FORCE VIEW CONTENT_TITLE_COUNTRY_RELEASES
AS
SELECT  ct.title_id,ct.title_type,ct.title_kind,cl.COUNTRY_ID,cl.COUNTRY,cl.REGION,
cl.language_code,l.locale,ct.default_locale,cl.country_default,l.TITLE,ct.title_version,ct.title_version_date,l.HIDDEN_TITLE,ct.CATEGORY,ct.PLATFORM, 
ct.publisher,ct.developer,ct.publish_date,ct.release_date, 
ct.cacheable,ct.preloadable,cl.recommend_date,cl.Popularity,cl.new_since,cl.new_duration,
ct.classic_controller,ct.rvl_controller,ct.need_dvd,ct.min_players,ct.max_players,ct.attribits, 
ct.tmd_content_id,ct.etkm_content_id,ct.tmd_content_checksum,ct.met_content_checksum, 
ct.title_size,ct.approx_size,
cl.rating_system,cl.rating_value,cl.rating_age,cl.second_system,cl.second_value,cl.second_age, 
l.description,l.short_phrase,ct.product_code,RATING_ATTRIBITS,SECOND_ATTRIBITS,ct.approve_date,ct.stable_version
FROM CONTENT_TITLE_CATALOG ct, 
     CONTENT_TITLE_COUNTRY_LOCALES cl, 
     CONTENT_TITLE_LOCALES l
WHERE cl.title_id=ct.title_id
AND l.title_id=ct.title_id
AND l.locale=NVL(cl.locale,ct.default_locale) 
/


COMMENT ON TABLE CONTENT_OBJECTS_V IS '/* $Id: CDSVIEWS.sql,v 1.10 2006/08/25 20:12:29 jchang Exp $*/';
COMMENT ON TABLE CONTENT_TITLE_LAST_VERSION IS '/* $Id: CDSVIEWS.sql,v 1.10 2006/08/25 20:12:29 jchang Exp $*/';
COMMENT ON TABLE CONTENT_TITLE_COUNTRY_LOCALES IS '/* $Id: CDSVIEWS.sql,v 1.10 2006/08/25 20:12:29 jchang Exp $*/';
COMMENT ON TABLE CONTENT_TITLE_RATINGS IS '/* $Id: CDSVIEWS.sql,v 1.10 2006/08/25 20:12:29 jchang Exp $*/';
COMMENT ON TABLE LATEST_CONTENT_TITLE_OBJECTS IS '/* $Id: CDSVIEWS.sql,v 1.10 2006/08/25 20:12:29 jchang Exp $*/';
COMMENT ON TABLE CONTENT_TITLE_CATALOG IS '/* $Id: CDSVIEWS.sql,v 1.10 2006/08/25 20:12:29 jchang Exp $*/';

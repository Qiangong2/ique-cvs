/*
 * (C) 2006, BroadOn Communications Corp, Inc.,
 * $Id: BCCUTIL.pkb,v 1.1 2006/08/24 22:00:42 jchang Exp $
 */

CREATE OR REPLACE PACKAGE BODY bccutil
AS
/*
 * (C) 2006, BroadOn Communications Corp, Inc.,
 * $Id: BCCUTIL.pkb,v 1.1 2006/08/24 22:00:42 jchang Exp $
 */
   x                NUMBER;
   epoch   CONSTANT DATE
                    := TO_DATE ('00:00:00 1/1/1970', 'HH24:MI:SS MM/DD/YYYY');
   debugmsg         VARCHAR2 (1000);

   FUNCTION converttimestamp (msec IN NUMBER)
      RETURN DATE
   IS
   BEGIN
      RETURN epoch + msec / 86400000;
   END;

   FUNCTION converttimestamp (utctime IN DATE)
      RETURN NUMBER
   IS
   BEGIN
      RETURN (utctime - epoch) * 86400000;
   END;

   FUNCTION dec2hex (i_dec IN NUMBER, i_hexlength NUMBER DEFAULT 12)
      RETURN VARCHAR2
   IS
   BEGIN
      RETURN LPAD (LTRIM (TO_CHAR (i_dec, RPAD ('X', i_hexlength, 'X'))),
                   i_hexlength,
                   '0'
                  );
   END;

   FUNCTION hex2dec (i_hex IN VARCHAR2)
      RETURN NUMBER
   IS
   BEGIN
      RETURN TO_NUMBER (i_hex, RPAD ('X', LENGTH (i_hex), 'X'));
   END;

   FUNCTION TRANSLATE (i_string IN VARCHAR2, i_lang IN VARCHAR2)
      RETURN VARCHAR2
   IS
      CURSOR c_out
      IS
         SELECT b.output_string
           FROM string_translations a, string_translations b
          WHERE a.output_string = i_string
            AND b.output_lang = i_lang
            AND a.input_string = b.input_string;

      CURSOR c_in
      IS
         SELECT output_string
           FROM string_translations
          WHERE input_string = i_string AND output_lang = i_lang;

      ret   VARCHAR2 (255);
   BEGIN
      OPEN c_in;

      FETCH c_in
       INTO ret;

      CLOSE c_in;

      IF ret IS NULL
      THEN
         OPEN c_out;

         FETCH c_out
          INTO ret;

         CLOSE c_out;
      END IF;

      RETURN ret;
   END;
END bccutil;
/


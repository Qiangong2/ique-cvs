#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <signal.h>

#include <openssl/ssl.h>
#include <ssl_wrapper.h>


#define ROOT_CERT "/flash/root_cert.pem"
#define TARGET_HOST "localhost"
#define REQUEST_TAG "monitor-request"

#define CONTENT "<" REQUEST_TAG "></" REQUEST_TAG ">"
#define RESPONSE "HTTP/1.1 20"		// any response in the 20x range

#define RESPONSE_POSITIVE "OK"
#define RESPONSE_NEGATIVE "ERROR_RESTART"

static void
error(int)
{
    printf(RESPONSE_NEGATIVE "\n");
    exit(1); 
}

int
main (int argc, char* argv[])
{
    if (argc < 3) {
	fprintf (stderr, "Usage: %s port URI\n", argv[0]);
	return 1;
    }

    signal(SIGALRM, error);
    alarm(30);				// timeout in 30 seconds.

    SSL_wrapper* ssl = new_SSL_wrapper(0, 0, 0, 0, ROOT_CERT, 0);
    if (ssl == 0) {
	// perror ("new_SSL_wrapper");
	error (1);
    }

    if (! SSL_wrapper_connect (ssl, TARGET_HOST, atoi(argv[1]))) {
	// perror ("SSL_wrapper_connect");
	error (1);
    }

    int length = strlen(CONTENT);

    char buf[1024];
    snprintf (buf, sizeof(buf), "POST %s HTTP/1.0\r\n"
	      "Content-Length: %d\r\n\r\n%s",
	     argv[2], length, CONTENT);
    int n = strlen (buf);
    if (SSL_wrapper_write (ssl, buf, n) != n) {
	// perror ("write request");
	error (1);
    }

    /* check response */
    length = strlen(RESPONSE);
    int count = 0;
    while ((n = SSL_wrapper_read(ssl, buf + count, sizeof(buf) - count)) > 0) {
	count += n;
	if (count >= length)
	    break;
    }

    if (strncmp(RESPONSE, buf, length) == 0) {
	printf(RESPONSE_POSITIVE "\n");
	SSL_wrapper_disconnect (ssl);
    } else {
#if 0
	write(1, buf, count);
	while ((n = SSL_wrapper_read(ssl, buf, sizeof(buf))) > 0) {
	    write(1, buf, n);
	}
#endif
	error(1);
    }

    return 0;
}

    

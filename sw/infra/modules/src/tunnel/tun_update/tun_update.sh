#!/bin/sh

SLEEPTIME=60
SERVICE_DOMAIN=`printconf sys.rmsd.service_domain`
STATUS_DIR=/var/tun_server

cd $STATUS_DIR

while [ 1 ]; do

  for file in * ; do
  if [ $file != '*' ]; then
    wget -q -O - -T 5 http://rms.$SERVICE_DOMAIN/hr_status/entry?HR_id=HR$file
    rm $file
  fi
  done

  sleep $SLEEPTIME
done

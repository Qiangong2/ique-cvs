/* 
 *   Ethernet tunnel over TCP - server
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>   
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <string.h>
#include <linux/if.h>
#include <linux/if_tun.h>
#include <map>

using namespace std;
using namespace __gnu_cxx;

#ifndef min
  #define min(a,b)    ( (a)<(b) ? (a):(b) )
#endif

#define TUNDEV  "/dev/net/tun"
#define MAX_PKT_SIZE      2048
#define PKT_OVERHEAD      2
char broadcast[6] = { 0xFF,0xFF,0xFF,0xFF,0xFF,0xFF, };

struct sockaddr_in myaddr;
int port = 16975;
char* dev = "tun0";
char* status_dir = NULL;
int verbose = 0;
char sndbuf[MAX_PKT_SIZE+PKT_OVERHEAD];

typedef map< long long, int, less<long long> > maptype;
maptype sktmap;

typedef struct _skt {
    int skt;
    char buf[MAX_PKT_SIZE];
    char *ptr;
    int len;
    int tot;
    int bad;
    struct _skt* next;
} skt_t;
skt_t* sktlist = NULL;

struct eth_hdr {
    unsigned char pad[4]; /* unknown */
    unsigned char dst_addr[6];
    unsigned char src_addr[6];
};
int eth_hdr_size = sizeof(struct eth_hdr);

void error(const char *msg)
{
    if (verbose)
        perror(msg);
}

void fatal(const char *msg)
{
    perror(msg);
    exit(1);
}

int open_tun_dev()
{
    int fd = open(TUNDEV, O_RDWR);
    if (fd < 0) fatal("open /dev/net/tun");

    struct ifreq ifr;
    memset(&ifr, 0, sizeof(ifr));
    
    ifr.ifr_flags = IFF_TAP; 
    strncpy(ifr.ifr_name, dev, IFNAMSIZ);
    
    if (ioctl(fd, TUNSETIFF, (void *) &ifr) < 0)
	fatal("ioctl");

    return fd;
}


int open_listener()
{
    int fd, r, opt;
    fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd < 0) fatal("socket");
    opt = 1;
    setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)); 
    r = bind(fd, (struct sockaddr *)&myaddr, sizeof(myaddr));
    if (r < 0) fatal("bind");
    r = listen(fd, 10);
    if (r < 0) fatal("listen");
    return fd;
}

int accept_skt(int skt)
{
    int s = accept(skt, NULL, NULL);
    if (s < 0)
        fatal("accept");
    if (fcntl(s, F_SETFL, O_NONBLOCK) < 0)
        fatal("fcntl O_NONBLOCK");
    return s;
}

int read_tcp_pkt(skt_t* rs)
{
    if (rs->len <= 0) {
        unsigned short l;
        int n;
        
        if ((n = read(rs->skt, (char*)&l, sizeof(short))) == 0)
            return 0;
        else if (n != sizeof(short))
            return -1;
        
        l = ntohs(l);
        if (!l || l > MAX_PKT_SIZE) {
            l = MAX_PKT_SIZE;
            rs->bad = 1;
        } else {
            rs->bad = 0;
        }
        rs->len = l;
        rs->tot = rs->len;
        rs->ptr = rs->buf;
    }

    register int t=0, w;
    while (rs->len > 0) {
        if( (w = read(rs->skt, rs->ptr, rs->len)) <= 0 ){
            if (rs->bad)
                rs->bad = rs->len = 0;
            return w;
        }
        rs->len -= w; rs->ptr += w; t += w;
    }
    return t;
}

int write_pkt(int skt, const char *msg, size_t len)
{
    int r = write(skt, msg, len);
    if (r < 0) {
	error("write");
    }
    return r;
}

int write_tcp_pkt(int skt, const char *msg, size_t len)
{
    *((unsigned short *)msg) = htons(len);
    len = len + sizeof(short);
    return (write_pkt(skt, msg, len));
}

void send_all_clients(const char *msg, size_t len)
{
    skt_t* sktp = sktlist;

    *((unsigned short *)msg) = htons(len);
    len = len + sizeof(short);

    while (sktp) {
        write_pkt(sktp->skt, msg, len);
        sktp = sktp->next;
    }
}

void add_skt(int skt)
{
    skt_t* s = (skt_t*)calloc(1, sizeof(skt_t));
    s->skt = skt;
    s->len = 0;
    s->next = sktlist;
    sktlist = s;
}

skt_t* del_skt(int skt)
{
    skt_t **prev, *s;
    prev = &sktlist;

    s = sktlist;
    while (s) {
        if (s->skt == skt) {
            *prev = s->next;
            free(s);
            break;
        }
        prev = &s->next;
        s = s->next;
    }

    for (maptype::iterator it = sktmap.begin();
         it != sktmap.end();
         it++) {
        if ((*it).second == skt)
            sktmap.erase(it);
    }

    return *prev;
}

void update_map(unsigned char* mac, int skt)
{
    long long tmp = 0;
    memcpy(&tmp, mac, 6);
    sktmap[ tmp ] = skt;
}

void update_stat(unsigned char* mac)
{
    int fd;
    char buf[1024];

    if (!status_dir) return;

    sprintf(buf, "%s/%02X%02X%02X%02X%02X%02X", status_dir,
            mac[0], mac[1], mac[2],
            mac[3], mac[4], mac[5]);

    if ((fd = open(buf, O_WRONLY|O_TRUNC|O_CREAT, 0666)) < 0) {
	return;
    }

    close(fd);
}

int fetch_map(unsigned char* mac)
{
    long long tmp = 0;
    memcpy(&tmp, mac, 6);
    
    if (sktmap.count(tmp))
        return sktmap[ tmp ];
    else
        return 0;
}

void update_macs(char* buf, unsigned int size, int skt)
{
    struct eth_hdr *h;

    if (size < sizeof(struct eth_hdr)) {
        return;
    }

    h = (struct eth_hdr *)buf;
    
    update_map(h->src_addr, skt);

    update_stat(h->src_addr);
}
    
int main(int argc, char *argv[])
{
    int n;
    int tunfd, acceptfd, opt;
    fd_set fds;

    while( (opt=getopt(argc,argv,"p:d:s:v")) != EOF ){
	switch(opt){
	    case 'p':
		port = atoi(optarg);
		break;
	    case 'd':
		dev = optarg;
                break;
	    case 's':
		status_dir = optarg;
                break;
	    case 'v':
		verbose = 1;
                break;
	    default:
		printf("usage: %s [-p port] [-d dev] [-s status dir] [-v]\n", argv[0]);
	        exit(1);
	}
    }	

    memset(&myaddr, 0, sizeof(myaddr));
    myaddr.sin_family = AF_INET;
    myaddr.sin_addr.s_addr = INADDR_ANY;
    myaddr.sin_port = htons(port);

    tunfd = open_tun_dev();
    acceptfd = open_listener();

    if (!verbose) {
        if (fork())
            exit(0);
    }

    while (1) {
	FD_ZERO(&fds);
        FD_SET(tunfd, &fds);
        FD_SET(acceptfd, &fds);
        int fmax = (tunfd > acceptfd) ? tunfd : acceptfd;
        skt_t* sktp = sktlist;
        int c = 0;
        while (sktp) {
            c++;
            FD_SET(sktp->skt, &fds);
            fmax = (fmax > sktp->skt) ? fmax : sktp->skt;
            sktp = sktp->next;
        }
        if (verbose) {
            printf("<connections: %d> <map size: %d>\n", c, sktmap.size());
        }
        n = select(fmax+1, &fds, NULL, NULL, NULL);
	if (n < 0) 
	    error("select");

        if (FD_ISSET(acceptfd, &fds)) {
            add_skt(accept_skt(acceptfd));
        }

	if (FD_ISSET(tunfd, &fds)) {
	    n = read(tunfd, sndbuf+PKT_OVERHEAD, MAX_PKT_SIZE);
            if (verbose)
                printf("%d bytes read from tunnel\n", n);
	    if (n > eth_hdr_size) {
                struct eth_hdr *h = (struct eth_hdr*)(sndbuf+PKT_OVERHEAD);
                if (!memcmp(h->dst_addr, broadcast, 6))
                    send_all_clients(sndbuf, n);
                else {
                    int tmp;
                    if ((tmp = fetch_map(h->dst_addr)))
                        write_tcp_pkt(tmp, sndbuf, n);
                }
            }
	}

        sktp = sktlist;
        while (sktp) {
            if (FD_ISSET(sktp->skt, &fds)) {
                n = read_tcp_pkt(sktp);
                if (verbose)
                    printf("%d bytes read from tcp\n", n);
                if (n > 0 && sktp->len <= 0) {
                    update_macs(sktp->buf, sktp->tot, sktp->skt);
                    write_pkt(tunfd, sktp->buf, sktp->tot);
                } else if (n == 0) {
                    close(sktp->skt);
                    sktp = del_skt(sktp->skt);
                    continue;
                }
            }
            sktp = sktp->next;
	}
    }
}

#ifndef __HSM_H__
#define __HSM_H__

class HSM
{

private:

    static const unsigned int MAX_RAND_SIZE = 4096;
    static const unsigned int BAD_KEY = (unsigned int) -1;
    
    NFKM_WorldInfo* world;
    NFast_AppHandle app_handle;		/* application handle */
    NFastApp_Connection conn;		/* connection to HSM */
    M_KeyID key;			// private key for signing
    bool cardset_loaded;

    M_Command rand_cmd;			// generate random command
    M_Command sign_cmd;			// signature command

    int transact(M_Command& cmd, M_Reply& reply);

    inline void freereply(M_Reply& reply) {
	NFastApp_Free_Reply(app_handle, NULL, NULL, &reply);
    }

    M_KeyID loadcardset(NFKM_Key* keyinfo);

    M_KeyID findkey(const char* keyname);

    int do_sign(M_Command& cmd, M_CipherText& signature);

public:

    HSM(const char* keyname);

    ~HSM();

    // check if the HSM is operational
    inline bool operational() {
	return (app_handle != NULL) && (conn != NULL) && (key != BAD_KEY);
    }

    inline bool is_cardset_loaded() {
	return cardset_loaded;
    }

    /* generate 'len' random bytes and put them in 'buf'. Returns Status_OK
       only if successful. */
    int get_random_bytes (unsigned char* buf, unsigned int len);


    // sign the specified document of length 'doc_len' bytes and return the
    // signature as a big number.
    const NFast_Bignum* sign(const void* document, unsigned int doc_len);

}; // HSM


#endif /* __HSM_H__ */

#include <string.h>
#include <string>

#include <openssl/ssl.h>

using namespace std;

extern "C" {				
#include "nfkm.h"
}

#include "bignum.h"
#include "hsm.h"

int
HSM::transact(M_Command& cmd, M_Reply& reply)
{
    int rc;

    memset(&reply, 0, sizeof(M_Reply));

    rc = NFastApp_Transact(conn, NULL, &cmd, &reply, NULL);
    if (cmd.cmd == Cmd_Sign)
	memset(&cmd.args.sign.plain, 0, sizeof(M_PlainText));
    NFastApp_Free_Command(app_handle, NULL, NULL, &cmd);

    if (rc == Status_OK) {
	rc = reply.status;
	if (rc == Status_OK)
	    return (cmd.cmd == reply.cmd) ? rc : Status_Failed;
    }

    freereply(reply);
    return rc;
} /* transact */


int
HSM::get_random_bytes(unsigned char* buf, unsigned int len)
{
    int rc;

    while ( len > MAX_RAND_SIZE ) {
	if ((rc = get_random_bytes(buf, MAX_RAND_SIZE)) != Status_OK)
	    return rc;
	buf += MAX_RAND_SIZE;
	len -= MAX_RAND_SIZE;
    }

    M_Reply reply;

    rand_cmd.args.generaterandom.lenbytes = len;

    rc = transact(rand_cmd, reply);
    
    if (rc == Status_OK) {
	if (reply.reply.generaterandom.data.len == len)
	    memcpy(buf,reply.reply.generaterandom.data.ptr,len);
	else
	    rc = Status_Failed;
    }
    
    freereply(reply);

    return rc;

} /* get_random_bytes */


int
HSM::do_sign(M_Command& cmd, M_CipherText& signature)
{
    M_Reply reply;
    
    int rc = transact(cmd, reply);
    if (rc == Status_OK) {
	rc = reply.status;
	if (rc == Status_OK)
	    signature = reply.reply.sign.sig;
    }

    /* Do the same trick so we get to keep any allocated memory in the
      reply */

    memset(&reply.reply.sign.sig, 0, sizeof(M_CipherText));
    freereply(reply);
    return rc;
} // do_sign


const NFast_Bignum*
HSM::sign(const void* document, unsigned int doc_len)
{
    // sign the hash
    M_PlainText& pt = sign_cmd.args.sign.plain;

    // Using the host cpu to compute the hash is much faster than using the
    // HSM, especially when the document size is large.
    SHA1((const unsigned char*) document, doc_len,
         (unsigned char*) &(pt.data.hash.data));

    pt.type = PlainTextType_Hash;

    M_CipherText signature;
    if (do_sign(sign_cmd, signature) != Status_OK)
	return NULL;

    return signature.data.rsappkcs1.m;
} // sign


M_KeyID
HSM::loadcardset(NFKM_Key* keyinfo)
{
    M_KeyID token = 0;
    NFKM_CardSet* cardset = NULL;
    if (NFKM_findcardset(app_handle, &keyinfo->cardset, &cardset, NULL) != Status_OK)
	return token;
    if (cardset == NULL) {
	return token;
    }
    for (int i = 0; i < world->n_existingobjects; ++i) {
	if (memcmp(world->existingobjects[i]->hash.bytes,
		   cardset->hkltu.bytes,
		   sizeof(cardset->hkltu.bytes)) == 0) {
	    return world->existingobjects[i]->id;
	}
    }

    // start loading card set
    NFKM_LoadCSHandle card_set_handle;
    if (NFKM_loadcardset_begin(app_handle, conn, world->modules[0], cardset,
			       &card_set_handle, 0) != Status_OK) {
	return token;
    }


    // read the card
    int cards_left = 1;
    if (NFKM_loadcardset_nextcard(card_set_handle, world->modules[0]->slots[0],
				  NULL, &cards_left) != Status_OK) {
	return token;
    }
    if (cards_left != 0) {
	return token;
    }
	   
    if (NFKM_loadcardset_done(card_set_handle, &token) != Status_OK) {
	return 0;
    }
    return token;
    
} // loadcardset


M_KeyID
HSM::findkey(const char* keyname)
{
    int nkeys;
    M_KeyID id = BAD_KEY;
    NFKM_KeyIdent* keylist;

    if (NFKM_listkeys(app_handle, &nkeys, &keylist, NULL, NULL) != Status_OK)
	return BAD_KEY;

    M_ModuleID module = world->modules[0]->module;

    for (int i = 0; i < nkeys; ++i) {
	NFKM_Key* keyinfo;
	if (NFKM_findkey(app_handle, keylist[i], &keyinfo, NULL) != Status_OK)
	    continue;
	if (strcmp(keyinfo->name, keyname) == 0) {
	    M_KeyID token = 0;
	    if (keyinfo->flags & Key_flags_ProtectionCardSet) {
		token = loadcardset(keyinfo);
		cardset_loaded = (token != 0);
	    }

	    if (NFKM_cmd_loadblob(app_handle, conn, module,
				  &(keyinfo->privblob), token, &id, NULL,
				  NULL) != Status_OK) {
		id = BAD_KEY;
		continue;
	    }
	    break;
	}
    }
    NFKM_freekeyidentlist(app_handle, nkeys, keylist, NULL);
    return id;
} // findkey




HSM::HSM(const char* keyname)
{
    app_handle = NULL;
    conn = NULL;
    key = BAD_KEY;
    cardset_loaded = false;

    if (NFastApp_Init(&app_handle, NULL, NULL, NULL, NULL) != Status_OK) {
	app_handle = NULL;
	return;
    }

    if (setup_bignum_upcalls(app_handle) != Status_OK)
	return;

    // set up signature service
    world = NULL;
    if (NFKM_getinfo(app_handle, &world, NULL) != Status_OK) {
	return;
    }

    if (NFastApp_Connect(app_handle, &conn,
			 NFastApp_ConnectionFlags_ForceClientID, NULL) != Status_OK) {
	conn = NULL;
	return;
    }

    // set up random number generator
    memset(&rand_cmd, 0, sizeof(rand_cmd));
    rand_cmd.cmd = Cmd_GenerateRandom;

    key = findkey(keyname);
    memset(&sign_cmd, 0, sizeof(sign_cmd));
    sign_cmd.cmd = Cmd_Sign;
    sign_cmd.args.sign.key = key;
    sign_cmd.args.sign.mech = Mech_Any;

} // constructor

HSM::~HSM()
{
    if (conn != NULL)
	NFastApp_Disconnect(conn, NULL);
    if (app_handle != NULL) {
	if (world != NULL)
	    NFKM_freeinfo(app_handle, &world, NULL);
	NFastApp_Finish(app_handle, NULL);
    }
} // destructor


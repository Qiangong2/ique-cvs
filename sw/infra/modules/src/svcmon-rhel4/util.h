#ifndef __UTIL_H__
#define __UTIL_H__

#include <string>
#include <netinet/in.h>

#if (__GNUC__ >= 3)
#include <ext/hash_map>
using namespace __gnu_cxx;
#else
#include <hash_map>
#define __gnu_cxx std
#endif

// common template specialization
namespace __gnu_cxx
{
    template <> struct hash<const in_addr> {
	hash<uint32_t> h;
	
	size_t operator()(const in_addr& addr) const {
	    return h(addr.s_addr);
	}
    };
}

namespace std
{
    
    template <> struct equal_to<const in_addr> {
	bool operator()(const in_addr& x, const in_addr& y) const {
	    return (x.s_addr == y.s_addr);
	}
    };
}


// string equality functor
struct eqstr {
    inline bool operator()(const char* s1, const char* s2) const {
	return (*s1 != *s2) ? false : (strcmp(s1, s2) == 0);
    }
};


using namespace std;

// property list, i.e., a hash map of string -> string.
class PROPERTIES
{
private:

    typedef hash_map<const char*, const char*, hash<const char*>, eqstr> MAP;
    MAP conf;

public:

    // load and parse a file specifying the properties in <key> = <value>
    // format.
    bool load(const char* filename);
    
    inline const char* find(const char* key) const {
	MAP::const_iterator iter = conf.find(key);
	return (iter == conf.end()) ? NULL : iter->second;
    }

    inline const char* find(const string& key) const {
	return find(key.c_str());
    }

    void clear();

    ~PROPERTIES() {
	clear();
    }
    

}; // PROPERTIES


// a convenient class to tokenize a string
class TOKENIZER
{
private:
    vector<const char*> tokens;
    char* buf;
    
public:
    
    // tokenize the string "s" using "delim" as delimiters.
    // null token (i.e., consecutive delimiters) are ignored.
    TOKENIZER(const char* s, const char* delim);

    inline int size() const {
	return tokens.size();
    }

    inline const char* operator[](int i) const {
	return tokens[i];
    }

    ~TOKENIZER() {
	if (buf != 0)
	    free(buf);
    }
};

#endif /* __UTIL_H__ */

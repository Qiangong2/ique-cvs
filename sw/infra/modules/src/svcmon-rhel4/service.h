#ifndef __SERVICE_H__
#define __SERVICE_H__

#include <vector>
#if (__GNUC__ >= 3)
#include <ext/hash_map>
using namespace __gnu_cxx;
#else
#include <hash_map>
#endif

#include <errno.h>
#include <arpa/inet.h>

#ifndef __IFCONFIG_H__
#include "ifconfig.h"
#endif

#ifndef __LOAD_BALANCER_H__
#include "load_balancer.h"
#endif

class SERVER;

typedef vector<SERVER*> SERVER_LIST;
typedef vector<uint16_t> PORT_LIST;
typedef vector<u_char> BUFFER;

class SERVICE
{
private:

    IFCONFIG& ifconf;			// net interface configurator

    const LB& lb;			// load balancer instance

    const char* name;			// name of this service

    bool tcp;				// true => TCP; false => UDP
    in_addr vip;			// exported virtual IP address

    PORT_LIST ports;			// ports listened to by this
					// service

    uint16_t monitor_port;		// port used for status monitoring,
					// could be different from those in
					// "ports"

    uint16_t persistency;		// if > 0; set up as persistent service

    char** reload_cmd;			// command to reload this service
					// -- used only if the server is
					// running locally on the same
					// machine and needs to be reloaded
					// upon changing the VIP.

    SERVER_LIST primary;
    SERVER_LIST backup;

    bool use_primary_servers;		// true if at least one of
					// primary servers is in use

public:
    
    enum SVC_RESPONSE { SVC_RESP_GOOD, SVC_RESP_BAD, SVC_RESP_MORE_DATA };


private:

    int change_server(const in_addr& ipaddr, bool activate) const;

protected:

    virtual void make_request(BUFFER& buf) const = 0;

    virtual bool parse_response(const u_char* buf, int buf_len) const = 0;

public:

    SERVICE(IFCONFIG& ifconf, const LB& lb, const char* name, bool tcp,
	    const in_addr& vip, PORT_LIST& pl, uint16_t mport,
	    uint16_t persistency = 0, const char* cmd_string = NULL);

    virtual ~SERVICE() {
	ifconf.if_down(vip);
	free(const_cast<char*>(name));
	if (reload_cmd) {
	    for (int i = 0; reload_cmd[i] != 0; ++i) {
		free(reload_cmd[i]);
	    }
	    free(reload_cmd);
	}
    }

    inline const char* get_name() const {
	return name;
    }

    inline const in_addr get_vip() const {
	return vip;
    }

    inline const uint16_t get_monitor_port() const {
	return monitor_port;
    }

    inline char* const* get_reload_cmd() const {
	return reload_cmd;
    }

    void add_server(SERVER* s, bool is_primary, const in_addr& ipaddr,
		    bool in_use);

    inline int start_server(const in_addr& ipaddr) const {
	return change_server(ipaddr, true);
    }

    inline int stop_server(const in_addr& ipaddr) const {
	return change_server(ipaddr, false);
    }

    void reconfig();

    int send_request(const sockaddr_in& dest) const;

    SVC_RESPONSE recv_response(int sock, BUFFER& in_buf,
			       const sockaddr_in& dest) const;

    void dump_status(FILE* fd) const;
}; // SERVICE

// ======================================================================


class SERVICE_MAP
{
private:
    
    typedef hash_map<const char*, SERVICE*, hash<const char*>, eqstr> MAP;

    MAP service_map;

public:

    inline void add_service(SERVICE* s) {
	service_map[s->get_name()] = s;
    }

    
    void delete_all();

    
    inline SERVICE* find_service(const char* name) const {
	MAP::const_iterator iter = service_map.find(name);
	return (iter == service_map.end()) ? NULL : iter->second;
    }
	

    inline void reconfigure() const {
	for (MAP::const_iterator i = service_map.begin();
	     i != service_map.end();
	     ++i) {

	    SERVICE* service = i->second;
	    service->reconfig();
	}
    }

    void dump_status(const char* filename) const;

}; // SERVICE_MAP

#endif /* __SERVICE_H__ */

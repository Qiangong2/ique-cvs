#include <stdio.h>
#include <string.h>
#include <string>
#include <arpa/inet.h>
#include <netdb.h>
#include <syslog.h>
#include <errno.h>
#include <signal.h>
#include <libgen.h>
#include <sys/file.h>
#include <fcntl.h>
#include <sys/wait.h>

#include "util.h"
#include "ifconfig.h"
#include "net_config.h"
#include "load_balancer.h"
#include "monitor.h"

static volatile bool job_done = false;
static volatile bool restart = false;
static volatile bool check_lb_status = true; // when true, check if heartbeat
					// has activated/deactivated the
					// load balancer

bool stop_monitor()
{
    return (job_done || restart || check_lb_status);
}


struct SERVER_INFO
{
    string name;
    bool is_primary;
    in_addr ipaddr;

    SERVER_INFO(const char* svr, bool primary, in_addr addr):
	name(svr), is_primary(primary), ipaddr(addr)
    {}
};

typedef vector<SERVER_INFO> SERVER_CONFIG;


static void
parse_server_config(FILE* fd, SERVER_CONFIG& server_conf)
{
    char buffer[1024];
    while (fgets(buffer, sizeof(buffer), fd)) {
	TOKENIZER tk(buffer, " ");
	if (tk.size() < 3) {
	    syslog(LOG_ALERT,
		   "Miss configuration: invalid server state \"%s\"",
		   buffer);
	    continue;			
	}
	const char* name = tk[0];
	in_addr ipaddr;
	inet_aton(tk[1], &ipaddr);
	bool primary = (*(tk[2]) == 'p');
	server_conf.push_back(SERVER_INFO(name, primary, ipaddr));
    }
} // parse_server_config


// read from the database the list of activate servers
static void
load_server_config(const PROPERTIES& config, SERVER_CONFIG& server_conf)
{
    string cmd(config.find("get_lb_config_cmd"));
    if (!cmd.c_str()) {
        cmd += config.find("db_command");
        cmd += " \"";
        cmd += config.find("db_url");
        cmd += "\" ";
        cmd += config.find("db_user");
        cmd += " ";
        cmd += config.find("db_password");
    }

    FILE* fd = popen(cmd.c_str(), "r");
    if (fd != NULL) {
	parse_server_config(fd, server_conf);
	int status = pclose(fd);
	if (status != -1 && WIFEXITED(status) != 0 && server_conf.size() > 0)
	    return;
    }

    syslog(LOG_ALERT,
	   "Failed to read server configuration from database or repository - using cached values");

    const char* cache_file = config.find("conf_cache");
    if (cache_file) {
	fd = fopen(cache_file, "r");
	if (fd) {
	    parse_server_config(fd, server_conf);
	    fclose(fd);
	    return;
	}
    }
    syslog(LOG_ALERT,
	   "Failed to read server configuration from local cache");
    exit(1);
} // load_server_config


static void
update_config_cache(PROPERTIES& config, SERVER_CONFIG& server_conf)
{
    const char* filename = config.find("conf_cache");
    if (filename == NULL) {
	syslog(LOG_WARNING, "Cannot update configuration file - missing filename");
	return;
    }
    
    FILE* fd = fopen(filename, "w");
    if (fd == NULL) {
	syslog(LOG_WARNING, "Cannot update configuration file %s - %s",
	       filename, strerror(errno));
	return;
    }
	
    for (SERVER_CONFIG::const_iterator iter = server_conf.begin();
	 iter != server_conf.end();
	 ++iter) {
	fprintf(fd, "%s %s %c\n", iter->name.c_str(), inet_ntoa(iter->ipaddr),
		iter->is_primary ? 'p' : 's');
    }
    fclose(fd);
} // update_config_cache


static void
load_config(const char* config_file, PROPERTIES& config,
	    SERVER_CONFIG& server_conf)
{
    if (restart) {
	syslog(LOG_NOTICE, "Reloading configuration");
	restart = false;
    }
    config.clear();
    if (! config.load(config_file)) {
	syslog(LOG_ALERT, "Failed to load configuration file %s -- %s",
	       config_file, strerror(errno));
	exit(1);
    }

    server_conf.clear();
    load_server_config(config, server_conf);
    update_config_cache(config, server_conf);
} // load_config


static void
write_pid(const char* pidfile)
{
    FILE* fd = fopen(pidfile, "w");
    if (fd == NULL) {
	syslog(LOG_ALERT, "Cannot create pid file - %s", strerror(errno));
	exit(1);
    }
    fprintf(fd, "%d\n", getpid());
    fclose(fd);
} // write_pid

// check if heartbeat is placing me in service
static bool
lb_activated(const char* filename)
{
    static bool status = false;

    if (check_lb_status) {
	check_lb_status = false;
	int fd = open(filename, O_RDONLY);
	if (fd < 0)
	    return false;
	if (flock(fd, LOCK_EX) != 0)
	    return false;
	char ch = 0;
	read(fd, &ch, 1);
	flock(fd, LOCK_UN);
	close(fd);
	status = (ch == '1');
    }

    return status;
} // lb_activated

// ======================================================================

// signal handlers
static void
shutdown(int sig)
{
    signal(sig, SIG_IGN);
    signal(SIGHUP, SIG_IGN);
    signal(SIGUSR1, SIG_IGN);
    job_done = true;
}


static void
restart_monitor(int sig)
{
    restart = true;
    check_lb_status = true;
    signal(sig, restart_monitor);
}


static void
check_status(int sig)
{
    check_lb_status = true;
    signal(sig, check_status);
}

// ======================================================================


static inline in_addr
get_addr(const sockaddr& in)
{
    return ((const sockaddr_in*) &in)->sin_addr;
}


static void
setup_local_servers(const SERVER_CONFIG& server_conf, const MONITOR& mon,
		    IFCONFIG& ifconf)
{
    in_addr local_addr = get_addr(ifconf.get_ipaddr());

    for (SERVER_CONFIG::const_iterator iter = server_conf.begin();
	 iter != server_conf.end();
	 ++iter) {

	in_addr ipaddr = iter->ipaddr;

	if (ipaddr.s_addr != local_addr.s_addr)
	    continue;

	SERVICE* svc = mon.find_service(iter->name.c_str());
	if (svc == NULL) {
	    syslog(LOG_ALERT, "Unrecognized service %s -- check configuration",
		   iter->name.c_str());
	    continue;
	}

	// set up IP alias with noarp
	if (! ifconf.if_up(svc->get_vip(), true)) {
	    syslog(LOG_ALERT, "Failed to set up IP alias %s with noarp",
		   inet_ntoa(svc->get_vip()));
	}

	// if service needs to be reloaded upon any network configuration
	// change ...
	char* const* reload_cmd = svc->get_reload_cmd();
	if (reload_cmd) {
	    if (fork() == 0) {
		execvp(reload_cmd[0], reload_cmd);
	    }
	}
    }
} // setup_local_servers


// check and wait until "heartbeat" tells us that we are activated
static void
wait_for_heartbeat(IFCONFIG& ifconf, const char* status_file,
		   const SERVER_CONFIG& server_conf, const MONITOR& mon)
{	
    if (status_file == NULL) {
	syslog(LOG_ALERT,
	       "Missing heartbeat status filename -- check configuration");
	exit(1);
    }

    if (! lb_activated(status_file)) {

	syslog(LOG_NOTICE, "Getting into hibernation mode");

	// set up IP aliases for local servers
	setup_local_servers(server_conf, mon, ifconf);

	do {
	    sleep(60);			// do not use pause(), in case SIGUSR1
					// arrives just before we block

	    // to get rid of zombie process from the local server reload
	    // process, we just blindly "wait" (non-blocking) here
	    // periodically
	    waitpid(-1, NULL, WNOHANG);

	} while (!(restart || job_done || lb_activated(status_file)));
	
	// unset the IP aliases
	ifconf.clear();
	syslog(LOG_NOTICE, "Waking up");
    }
} // wait_for_heartbeat


// configure the load balancer for each services and real servers, then
// enter the main monitoring loop in "mon.monitor()", which does not return
// until a signal is received.
static void
start_services(IFCONFIG& ifconf, const SERVER_CONFIG& server_conf,
	       const char* lb_host, MONITOR& mon)
{
    in_addr local_addr = get_addr(ifconf.get_ipaddr());
	    
    for (SERVER_CONFIG::const_iterator iter = server_conf.begin();
	 iter != server_conf.end();
	 ++iter) {

	if (ifconf.addr_is_local(iter->ipaddr))
	    mon.add_server(iter->name.c_str(), iter->is_primary, iter->ipaddr,
			   iter->ipaddr.s_addr == local_addr.s_addr);
    }

    if (! mon.add_lb_alias(lb_host)) {
	syslog(LOG_WARNING, "Cannot set up IP alias for load balancer \"%s\"",
	       lb_host == NULL ? "null" : lb_host);
    }

    mon.monitor();
} // start_services


int
main(int argc, char* argv[])
{
    if (argc < 2) {
	syslog(LOG_ALERT, "Missing argument to %s", argv[0]);
	return 1;
    }
    openlog(basename(argv[0]), LOG_CONS, LOG_DAEMON);

    PROPERTIES config;
    SERVER_CONFIG server_conf;

    load_config(argv[1], config, server_conf);

    const char* status_filename = config.find("server_status");

    if (status_filename) {
	unlink(status_filename);
    } else {
	syslog(LOG_WARNING, "Server status file undefined -- check configuration");
    }

    write_pid(config.find("pidfile"));

    signal(SIGHUP, restart_monitor);
    signal(SIGTERM, shutdown);
    signal(SIGINT, shutdown);
    signal(SIGUSR1, check_status);
	
    while (! job_done) {
	
	IFCONFIG ifconf(config.find("net_device"), config.find("send_arp"));
	init_net_config(ifconf.get_bcast_addr(), config);

	LB lb;
	MONITOR mon(config, lb, ifconf);

	wait_for_heartbeat(ifconf, config.find("lb_status"), server_conf, mon);
	
	if (! stop_monitor()) 
	    start_services(ifconf, server_conf, config.find("lb.host"), mon);

	// we do the following so that we can finish re-loading the
	// configuration BEFORE "mon" gets out of scope and destructed.
	// This minimizes the "down time" of the load balancer.  Note that
	// the config is loaded via an external Java program and is very slow.
	if (restart) {
	    load_config(argv[1], config, server_conf);
	}
    }

    syslog(LOG_NOTICE, "Shut down");
    return 100;				// required to stop heartbeat from
					// respawning myself again
}
	

#ifndef __DNS_SERVICE_H__
#define __DNS_SERVICE_H__

#include <arpa/nameser.h>

// Monitor a DNS server
// Use an address lookup request to check the server status.  The hostname
// used is specified in "dname".
class DNS : public SERVICE
{
private:
    u_char msg[NS_PACKETSZ];	// buffer for DNS query
    int msg_len;

public:
    DNS(IFCONFIG& ifconfig, const LB& lb, const char* service,
	const in_addr& ipaddr, PORT_LIST& port, const char* dname,
	const char* reload_cmd);

    void make_request(BUFFER& buf) const {
	buf.insert(buf.end(), msg, msg + msg_len);
    }

    // See RFC 1035, the DNS response header.
    // only check if there is any error, but ignore its content.
    bool parse_response(const u_char* buf, int buf_len) const {
	return ((buf_len >= 4) && ((buf[3] & 0xf) == 0));
    }

}; // DNS

#endif /* __DNS_SERVICE_H__ */

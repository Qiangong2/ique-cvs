#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctime>
#include <cstdio>
#include <cstring>
#include <string>
#include <vector>

#include <openssl/ssl.h>
#include <ssl_wrapper.h>

// #define DBG_TIME 1
#ifdef DBG_TIME
#define TRACK_TIME(tm) \
   gettimeofday(&tm, 0)
#define LOG_TIME(prog, uri, start, conn, req, res, end) \
   LogTime(prog, uri, \
           start.tv_sec + (double)start.tv_usec/1000000,\
           conn.tv_sec + (double)conn.tv_usec/1000000,\
           req.tv_sec + (double)req.tv_usec/1000000,\
           res.tv_sec + (double)res.tv_usec/1000000,\
           end.tv_sec + (double)end.tv_usec/1000000);
#else
#define TRACK_TIME(tm) (0)
#define LOG_TIME(prog, uri, start, conn, req, res, end) (0)
#endif


class EncryptPaths {
private:

    static const char * const DEFAULT_FLASH_PATH;
    std::string m_flash_path;
    std::string m_passwd;
    std::string m_chain;
    std::string m_trusted;
    std::string m_cert;
    std::string m_key;

public:

    EncryptPaths(const char *flash_path): m_flash_path(DEFAULT_FLASH_PATH)
    {
        if (flash_path != NULL) {
            m_flash_path = flash_path;
            if (!m_flash_path.empty() && 
                m_flash_path != "/" && 
                m_flash_path[m_flash_path.length() - 1] != '/') {
                m_flash_path += "/";
            }
        }

        m_chain = m_flash_path + "ca_chain.pem";
        m_trusted = m_flash_path + "root_cert.pem";
        m_cert = m_flash_path + "identity.pem";
        m_key = m_flash_path + "private_key.pem";
        m_passwd = m_flash_path + "passwd";
    }

    const std::string &chain_path() const {return m_chain;}
    const std::string &trusted_path() const {return m_trusted;}
    const std::string &cert_path() const {return m_cert;}
    const std::string &key_path() const {return m_key;}
    const std::string &passwd_path() const {return m_passwd;}

}; //  class EncryptPaths

const char* const EncryptPaths::DEFAULT_FLASH_PATH = "/flash/depot";

static int get_passwd(char *passwd, 
                      int len, 
                      const std::string &passwd_path, 
                      const std::string &privatekey_path)
{
    size_t      passwd_file_sz = 0;
    struct stat passwd_stat;
    struct stat privatekey_stat;

    if (stat(passwd_path.c_str(), &passwd_stat) == 0 &&
        stat(privatekey_path.c_str(), &privatekey_stat) == 0) {
        //
        // Only use the passwd file if the mod time is not earlier than
        // that of the private_key.pem file.
        //
        if (passwd_stat.st_mtime >= privatekey_stat.st_mtime)
            passwd_file_sz = passwd_stat.st_size;
    }
        
    FILE *fp = (passwd_file_sz == 0? NULL : fopen(passwd_path.c_str(), "r"));
    if (fp == NULL) {
        //
        // Try to get the passwd from printconf
        //
        std::string passwd_from_conf = 
            std::string("/sbin/printconf sys.comm.depot.passwd > ") + 
            passwd_path;
    
        if (system(passwd_from_conf.c_str()) != 0 || 
            (fp = fopen(passwd_path.c_str(), "r")) == NULL) {

            fprintf(stderr, "can't open nonempty %s\n", passwd_path.c_str());
            return -1;
        }
    }

    fgets(passwd, len, fp);
    fclose(fp);
    char *p = passwd;
    while (*p != '\0') {
        if (*p == '\n') {
            *p = '\0';
            break;
        }
        p++;
    }
    return 0;
} // get_passwd


static int Write_HTTP_Header(SSL_wrapper *ssl,
                             char        *host, 
                             char        *port, 
                             char        *uri, 
                             size_t       content_length)
{
    // Returns number of chars written
    //
    char buf[1024];
    sprintf (buf, "POST %s HTTP/1.0\r\nHOST: %s:%s\r\n"
	     "Content-Length: %d\r\n\r\n",
	     uri, host, port, content_length);
    int n = strlen (buf);
    if (SSL_wrapper_write (ssl, buf, n) != n) {
        perror ("write headers");
        return 0;
    }
    return n;
}


void LogTime(const char *prog,
             const char *uri,
             double     sec_start, 
             double     sec_conn, 
             double     sec_req, 
             double     sec_res, 
             double     sec_end)
{
    // Get program name
    //
    const char * prog_nm = strrchr(prog, '/');
    if (prog_nm != NULL)
        prog_nm = prog_nm + 1;

    // Write a log of the time spent into a /tmp/prog_nm log file 
    //
    const std::string log_file = std::string("/tmp/") + prog_nm + ".log";
    const char *      log_name = log_file.c_str();
    struct stat       log_stat;
    if (stat(log_name, &log_stat) == 0 && log_stat.st_size > 1000000) {
        remove(log_name);
    }
    FILE *f = fopen(log_name, "a");
    if (f != NULL) {
        const time_t tm = time(0);
        char         now[64];
        strncpy(now, ctime(&tm), 64);
        now[strlen(now)-1] = '\0';
        fprintf(f,
                "%s [%s] %s total_secs=%f setup=%f "
                "connect=%f request=%f response=%f\n",
                now, prog, uri,
                sec_end  - sec_start,
                sec_conn - sec_start,
                sec_req  - sec_conn,
                sec_res  - sec_req,
                sec_end  - sec_res);
        fclose(f);
    }
    else {
        perror (log_name);
        exit(1);
    }
} // LogTime


int
main (int argc, char* argv[])
{
    timeval startTm;
    timeval connectTm;
    timeval requestTm;
    timeval responseTm;
    timeval endTm;

    TRACK_TIME(startTm);

    // Handle the optional arguments and verify that we have the
    // required arguments
    //
    if (argc < 5) {
        fprintf (stderr, 
                 "Usage: %s host port URI depot_flash_dir \n"
                 "       [postfile [timeout [outputfile]]]\n"
                 "\n"
                 "When no postfile is given input is taken from stdin.\n"
                 "Stdin is read until the EOF is reached.  When no\n"
                 "outputfile is given output is written to stdout\n",
                 argv[0]);
        return 1;
    }
    else if (argc > 6) {
        //
        // timeout specified
        //
        alarm(atoi(argv[6]));
    }

    EncryptPaths ep(argv[4]);

    char passwd[256];
    if (get_passwd(passwd, sizeof(passwd),
                   ep.passwd_path(), ep.key_path()) != 0)
        return 1;

    SSL_wrapper* ssl = new_SSL_wrapper(ep.cert_path().c_str(), 
                                       ep.chain_path().c_str(), 
                                       ep.key_path().c_str(), 
                                       passwd, 
                                       ep.trusted_path().c_str(),
                                       0);
    if (ssl == 0) {
        perror ("Failed to create new_SSL_wrapper");
        return 1;
    }

    TRACK_TIME(connectTm);
    if (! SSL_wrapper_connect (ssl, argv[1], atoi(argv[2]))) {
        perror ("Failed to do SSL_wrapper_connect");
        return 1;
    }

    // Send POST request
    //
    int  n;
    char buf[1024];
    TRACK_TIME(requestTm);
    if (argc == 5) {
        //
        // Using STDIN : This is work in progress and not yet functional!!!
        //
        int                       contentLength = 0;
        std::vector<std::string*> contentBuffer;

        // Get bytes and content length
        // TODO: Read input until some condition other than EOF is reached.
        //
        contentBuffer.reserve(128);
        while ((n = read(STDIN_FILENO, buf, sizeof(buf))) > 0) {
            contentBuffer.push_back(new std::string(buf, n));
            contentLength += n;
        }

        // Write header
        //
        if (Write_HTTP_Header(ssl,
                              argv[1], argv[2], argv[3], 
                              contentLength) == 0) {
            return 1;
        }

        // Write content
        //
        for (std::vector<std::string*>::iterator i = contentBuffer.begin();
             i != contentBuffer.end(); 
             ++i) {
            const std::string * s = *i;
            if (SSL_wrapper_write(ssl, 
                                  s->c_str(), s->length()) != s->length()) {
                perror ("write data");
                return 1;
            }
            *i = NULL;
            delete s;
        }
	}
    else {
        //
        // Read content from file
        //
        int inFd = open(argv[5], O_RDONLY);
        if (inFd < 0) {
            perror("open");
            return 1;
        }

        // Get file length
        //
        struct stat stat_buf;
        if (fstat (inFd, &stat_buf) != 0) {
            perror ("fstat");
            return 1;
        }

        // Write header
        //
        if (Write_HTTP_Header(ssl,
                              argv[1], argv[2], argv[3], 
                              stat_buf.st_size) == 0) {
            return 1;
        }

        // Write content
        //
        while ((n = read (inFd, buf, sizeof(buf))) > 0) {
            if (SSL_wrapper_write (ssl, buf, n) != n) {
                perror ("write data");
                return 1;
            }
        }
        
        if (inFd != STDIN_FILENO && close(inFd) != 0) {
            perror("close");
            return 1;
        }
    }

    // Write POST response
    //
    int outFd = STDOUT_FILENO;
    if (argc > 7) {
        //
        // Write output to unique file
        //
        outFd = open(argv[7], 
                     O_CREAT | O_WRONLY | O_TRUNC,
                     S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH);
        if (outFd < 0) {
            perror("create");
            return 1;
        }
    }
    TRACK_TIME(responseTm);
    while ((n = SSL_wrapper_read (ssl, buf, 1024)) > 0) {
        write (outFd, buf, n);
    }
    if (outFd != STDOUT_FILENO && close(outFd) != 0) {
        perror("close");
        return 1;
    }

    SSL_wrapper_disconnect (ssl);
    TRACK_TIME(endTm);
    LOG_TIME(argv[0], argv[3], startTm, connectTm, requestTm, responseTm, endTm);
    return 0;
}


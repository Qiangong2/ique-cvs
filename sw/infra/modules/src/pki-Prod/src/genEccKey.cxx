#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>

#include <x86/esl.h>

class RANDOM
{
 private:
    int fd;

 public:
    RANDOM() {
	fd = open("/dev/urandom", O_RDONLY);
	if (fd < 0) {
	    perror("Can't open /dev/urandom");
	    exit(1);
	}
    }

    ~RANDOM() {
	if (fd >= 0)
	    close(fd);
    }

    void getRandom(u8* buf, int buf_size) {
	if (read(fd, buf, buf_size) != buf_size) {
	    perror("Can't read random bytes");
	    exit(1);
	}
    }
};

static RANDOM rng;

static void
genEccKey(u8* privKey, u8* pubKey)
{
    u8 seed[ES_ECC_RANDN_SIZE];
    rng.getRandom(seed, sizeof(seed));
    if (ES_GenerateEccKeyPair(seed, privKey, pubKey) != ES_ERR_OK) {
	fprintf(stderr, "Error generate ECC key pair\n");
	exit(1);
    }
}


int
main(int argc, char* argv[])
{
    int out_fd = 0;

    if (argc > 1) {
	out_fd = creat(argv[1], S_IRWXU|S_IRGRP|S_IROTH);
	if (out_fd < 0) {
	    perror("Can't create output file");
	    exit(1);
	}
    } else
	out_fd = fileno(stdout);
	
    u8 privKey[ES_ECC_PVT_KEY_SIZE];
    u8 pubKey[ES_ECC_PUB_KEY_SIZE];
    
    genEccKey(privKey, pubKey);

    write(out_fd, privKey, sizeof(privKey));
    write(out_fd, pubKey, sizeof(pubKey));
    close(out_fd);
    return 0;
}

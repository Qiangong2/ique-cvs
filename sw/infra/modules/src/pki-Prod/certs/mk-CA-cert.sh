#!/bin/sh

. $HOME/lib/functions

need_two_dirs $*

export LD_LIBRARY_PATH=/opt/nfast/toolkits/hwcrhk

generate_cert CA root
generate_BCC_cert CA root CA Root


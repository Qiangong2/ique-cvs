#!/bin/sh

. $HOME/lib/functions

need_one_dir $*

hsm_dir=$1

echo "Seeding random number generator..."
dd if=/dev/random of=$hsm_dir/rand bs=1 count=512
openssl genrsa -out $hsm_dir/root-sw-key.pem -rand $hsm_dir/rand 4096
rm -f $hsm_dir/rand

days=`days_till $HOME/conf/root.exp`
openssl req -config $HOME/conf/root.cnf -new -x509 -key $hsm_dir/root-sw-key.pem -out $hsm_dir/root-cert.pem -sha1 -days $days 

rm -f $hsm_dir/root-req.pem

echo "Importing root key into HSM..."
generatekey -i -b embed  protect=token pemreadfile=$hsm_dir/root-sw-key.pem embedsavefile=$hsm_dir/root-key.pem plainname=root



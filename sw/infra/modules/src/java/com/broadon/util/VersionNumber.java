package com.broadon.util;

import java.util.NoSuchElementException;
import java.util.StringTokenizer;

/**
 * The <c>VersionNumber</c> class allows construction through the factory
 * method "valueOf", which will throw a NumberFormatException if a non-null
 * string argument is not a version consisting of either:
 *
 *    three unsigned integers separated by dots (major.middle.minor)
 *
 * or
 *
 *    two unsigned integers separated by dots (major.middle).
 *
 * We support the two-number version for backwards compatibility reasons.
 * A null string will result in a null version number.
 *
 * VersionNumber is intended to allow easy comparisons between version numbers.
 *
 * Construction is mildly expensive, so avoid repeated construction for
 * the same version string.
 *
 * @version	$Revision: 1.1 $
 */
public class VersionNumber implements Comparable 
{
    private static final String VERSION_DELIMITER = ".";
    private String versionString;
    private int [] version;

    private VersionNumber(String asString, int i0, int i1, int i2) 
    {
        versionString = asString;
        version = new int[3];
        version[0] = i0;
        version[1] = i1;
        version[2] = i2;
    }

    // Factory of Version numbers
    //
    public static VersionNumber valueOf(String v) throws NumberFormatException 
    {
        if (v == null)
            return null;

        int             v0;
        int             v1;
        int             v2;
        StringTokenizer t = new StringTokenizer(v, ".");
        try {
            v0 = Integer.parseInt(t.nextToken());
            v1 = Integer.parseInt(t.nextToken());
            try {
                v2 = Integer.parseInt(t.nextToken());
            } catch (NoSuchElementException e) {
                v2 = 0;
            }
        }
        catch (NoSuchElementException e) {
            throw new NumberFormatException("Too few values in version number: " + v);
        }
        catch (NumberFormatException e) {
            throw new NumberFormatException("Cannot parse version number: " + v);
        }
        if (t.hasMoreTokens()) {
            throw new NumberFormatException("Unexpected end of version number: " + v);
        }
        return new VersionNumber(v, v0, v1, v2);
    } // valueOf

    public int major() {return version[0];}
    public int middle() {return version[1];}
    public int minor() {return version[2];}

    public int compareTo(Object otherObject)
    {
        // Where values cannot be compared view (1) as "different" and (0)
        // as equal.
        //
        if (otherObject == null) return 1;
        else if (this == otherObject) return 0;
        else if (getClass() != otherObject.getClass()) return 1;

        // Compare values
        //
        final VersionNumber other = (VersionNumber)otherObject;
        if (major() < other.major()) return -1;
        else if (major() > other.major()) return 1;
        else if (middle() < other.middle()) return -1;
        else if (middle() > other.middle()) return 1;
        else if (minor() < other.minor()) return -1;
        else if (minor() > other.minor()) return 1;
        else return 0;
    } // compareTo

    public String toString() 
    {
        return versionString;
    }
    
    public boolean equals(Object otherObject) 
    {
        return compareTo(otherObject) == 0;
    }

    public static void main(String[] args)
    {
        // Test program
        //
        VersionNumber n1 = VersionNumber.valueOf("1.1.2");
        VersionNumber n2 = VersionNumber.valueOf("1.2.0");
        VersionNumber n3 = VersionNumber.valueOf("1.2.1");
        VersionNumber n4 = VersionNumber.valueOf("2.0.0");
        VersionNumber n5 = VersionNumber.valueOf("1.1.2");
        if (n1.compareTo(n1) != 0) System.out.println("t0 failed");
        if (n1.compareTo(n2) != -1) System.out.println("t1 failed");
        if (n1.compareTo(n3) != -1) System.out.println("t2 failed");
        if (n1.compareTo(n4) != -1) System.out.println("t3 failed");
        if (n1.compareTo(n5) != 0) System.out.println("t5 failed");

        if (n2.compareTo(n1) != 1) System.out.println("t6 failed");
        if (n2.compareTo(n3) != -1) System.out.println("t7 failed");
        if (n2.compareTo(n4) != -1) System.out.println("t8 failed");

        if (n3.compareTo(n1) != 1) System.out.println("t9 failed");
        if (n3.compareTo(n2) != 1) System.out.println("t10 failed");
        if (n3.compareTo(n4) != -1) System.out.println("t11 failed");

        if (n4.compareTo(n1) != 1) System.out.println("t12 failed");
        if (n4.compareTo(n2) != 1) System.out.println("t13 failed");
        if (n4.compareTo(n3) != 1) System.out.println("t14 failed");

        System.out.println(n4);
        System.out.println(n5);
    }
    
} // class VersionNumber

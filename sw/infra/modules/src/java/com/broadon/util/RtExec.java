package com.broadon.util;

import java.io.*;

/** A class for executing external programs from within a java application
 *  in a safe and efficient manner.  All exported methods are static, so
 *  no instance of this class needs to be created:
 *
 *     runCmd: Run the command synchronously, waiting at most
 *             10 seconds after the command is done for the
 *             output and input streams to be completed. see
 *             additional comment on the method.
 *
 *     runCmd_Tokens: Same as runCmd, but returns result as sequence
 *             of tokens instead of a string.
 *
 *  The parameters to run an executable are as follows:
 *
 *     cmd:  The full path to the executable and the executable 
 *           parameters as separate strings in the string array.
 *
 *     in_bytes: The input to the command through the standard input
 *           stream.  No input streaming will occur for the command
 *           if this value is null (i.e. the stdin stream will be 
 *           empty).
 *
 *     env:  The environment variables used during the execution
 *           as an array of strings of the form "<name>=<value>".
 *           The array can be null if the default environment 
 *           variables are to be used.
 *
 *     dir:  The working directory of the subprocess, or null if
 *           the subprocess should inherit the working directory
 *           of the current process.
 *       
 */
public class RtExec
{
    private static class ExecInputProducer extends Thread
    {
        private InputStream      is;
        private OutputStream     os;
        private volatile boolean done;
    
        public ExecInputProducer(InputStream redirect, OutputStream os)
        {
            this.is = redirect;
            this.os = os;
            done = false;
        }

        public void stopStream() 
        {
            done = true;
        }

        public void closeStreams() throws IOException
        {
            os.close();
            is.close();
        }

        public void sleepMsecs(long millis)
        {
            try {
                sleep(millis); 
            } catch (InterruptedException e) 
            {
                /*ignore*/
            }
        }

        public void run()
        {
            try
            {
                byte[] buffer = new byte[1024];   
                int n = 0;
                while (!done && (n = is.read(buffer)) >= 0) {
                    try
                    {
                        if (n == 0)
                            sleepMsecs(1); // Sleep 1 millisec
                        else
                            os.write(buffer, 0, n);
                    } catch (IOException ioe) 
                    {
                        done = true; // Output stream is closed?
                    }
                }
                os.flush();
            } catch (IOException ioe)
            {
                ioe.printStackTrace(); // Failed to read
            }
        }
    } // ExecInputProducer


    private static class ExecOutputConsumer extends Thread
    {
        private InputStream      is;
        private OutputStream     os;
    
        ExecOutputConsumer(InputStream is, OutputStream redirect)
        {
            this.is = is;
            this.os = redirect;
        }

        public void sleepMsecs(long millis)
        {
            try {
                sleep(millis); 
            } catch (InterruptedException e) 
            {
                /*ignore*/
            }
        }

        public void run()
        {
            try
            {
                BufferedInputStream inp = new BufferedInputStream(is);
                byte[] buffer = new byte[1024];   
                int n = 0;
                while ((n = inp.read(buffer)) >= 0) {
                    if (n == 0)
                        sleepMsecs(1); // Sleep 1 millisec
                    else
                        os.write(buffer, 0, n);
                }
                os.flush();
                is.close();
                inp.close();
            } catch (IOException ioe)
            {
                ioe.printStackTrace(); // Failed to read
            }
        }
    }


    private static void throwCmdException(String[]  cmd, 
                                          Throwable e, 
                                          String    msg) 
        throws IOException
    {
        // Upon the rare occation there is an excpetion in running a
        // command, we convert the exception into an IOException
        //
        // First, construct a String for the message
        //
        StringBuffer io_msg = new StringBuffer("[");
        for (int i = 0; i < cmd.length; ++i) {
            if (i > 0)
                io_msg.append(" ");
            io_msg.append(cmd[i]);
        }
        io_msg.append("]");
        io_msg.append(msg);
        io_msg.append(":\n");
        if (e != null)
            io_msg.append(e.getMessage());

        // Then create and throw the exception
        //
        IOException ioe = new IOException(io_msg.toString());
        if (e != null)
            ioe.setStackTrace(e.getStackTrace());
        else
            ioe.fillInStackTrace();
        throw ioe;
    }


    /** Executes a system command, controlling the stdin and capturing
     *  the stderr and stdout streams.
     *
     *  @returns the standard output and standard error output
     *  concatenated as a string.  Never returns null.
     *  @throws IOException containing the standard error output if
     *  there is an error.
     */
    public static String runCmd(String[] cmd, 
                                byte  [] in_bytes, 
                                String[] env, 
                                File     dir)
        throws IOException
    {
        try
        {
            // Execute the command
            //
            Runtime rt = Runtime.getRuntime();
            Process proc = rt.exec(cmd, env, dir);
            
            // Producer for standard input
            //
            ExecInputProducer inp_consumer = null;
            if (in_bytes != null) {
                ByteArrayInputStream inp = 
                    new ByteArrayInputStream(in_bytes);
                BufferedOutputStream outp = 
                    new BufferedOutputStream(proc.getOutputStream());
                inp_consumer = new ExecInputProducer(inp, outp);
            }

            // Consumer for error messages
            //
            ByteArrayOutputStream err = new ByteArrayOutputStream();
            ExecOutputConsumer err_consumer = 
                new ExecOutputConsumer(proc.getErrorStream(), err);            
            
            // Consumer for standard output
            //
            ByteArrayOutputStream outp = new ByteArrayOutputStream();
            ExecOutputConsumer outp_consumer = 
                new ExecOutputConsumer(proc.getInputStream(), outp);  
                
            // Kick off consumer threads
            //
            if (inp_consumer != null) {
                inp_consumer.start();
            }
            err_consumer.start();
            outp_consumer.start();
                            
            // Wait for the 3 or 4 threads (exec, and producer and consumer
            // streams) to end
            //
            final int exitVal = proc.waitFor();
            if (inp_consumer != null) {
                inp_consumer.stopStream(); // In case all input not consumed
                inp_consumer.join(100); // Wait at most 100 msec
                inp_consumer.closeStreams();
            }
            err_consumer.join(10000);  // Wait at most 10 secs
            outp_consumer.join(10000); // Wait at most 10 secs

            // Return result and/or report errors
            //
            String errval = err.toString();
            String result = outp.toString() + errval;
            outp.close();
            err.close();
            if (exitVal != 0) {
                throwCmdException(cmd, null, "Program exited with value " + 
                                  proc.exitValue() + "\n\t" + errval);
            }
            return result;
        }
        catch (SecurityException e) {
            throwCmdException(cmd, e,
                              "Security manager exists and its checkExec " +
                              "method doesn't allow creation of a subprocess");
        }
        catch (NullPointerException e) {
            throwCmdException(cmd, e, "Cannot execute null command");
        }
        catch (IndexOutOfBoundsException e) {
            throwCmdException(cmd, e, "Cannot execute empty command");
        }
        catch (InterruptedException e) {
            throwCmdException(cmd, e, "Command was interrupted");
        }
        return null; // Should never occur!!
    }


    /** Executes a system command like runCmd.  Takes no std input.
     */
    public static String runCmd(String[] cmd, 
                                String[] env, 
                                File     dir)
        throws IOException
    {
        return runCmd(cmd, null, env, dir);
    }


    /** Executes a system command.
     *  @returns the stdout output as a sequence of string tokens with
     *  all whitespace in-between tokens removed.  Never returns null.
     *  @throws IOException if there is an error, and never returns
     *  null.
     */
    public static String[] runCmd_Tokens(String[] cmd, 
                                         byte  [] in_bytes, 
                                         String[] env, 
                                         File     dir)
        throws IOException
    {
        return runCmd(cmd, in_bytes, env, dir).split("\\s");
    }
    
    
    /** Executes a system command like runCmd_Tokens.  Takes no std input.
     */
    public static String[] runCmd_Tokens(String[] cmd, 
                                       String[] env, 
                                       File     dir)
        throws IOException
    {
        return runCmd_Tokens(cmd, null, env, dir);
    }
} // class RtExec

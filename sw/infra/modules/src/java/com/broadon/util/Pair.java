package com.broadon.util;

/**
 * Convenient class to hold a pair of objects.
 */
public class Pair {
    /** First object of the pair. */
    public Object first;

    /** Second object of the pair. */
    public Object second;

    /**
     * Create a pair of objects.
     * @param first First object of the pair.
     * @param second Second object of the pair.
     */
    public Pair(Object first, Object second) {
	this.first = first;
	this.second = second;
    }
}


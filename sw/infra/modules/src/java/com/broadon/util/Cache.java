package com.broadon.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.HashMap;

/**
 * A general class to support caching of objects from an associated backing
 * store. Objects from the backing store are uniquely identified by keys.
 * Referenced objects are retrieved from the backing store and kept in the local
 * cache table. Optionally, the caller could choose to implement the object
 * retrival outside of this class and explicitly put objects into the cache.
 * 
 * The max. size of the cache is static and specified upon creation.  Likewise, the expiration
 * time for each cached objects is also specified at creation time.
 * 
 * When the cache is fill up, a user-implemented cache replacement algorithm is invoked to
 * select a subset of entries to be cleared.
 * 
 * @see com.broadon.util.CacheLFU
 * 
 */
public abstract class Cache
{
    private static final int MAX_ENTRIES = 5000;        // default value
    private static final float LOAD_FACTOR = 0.75f;       // for HashMap resizing
    
    protected HashMap cache;
    /** Max. number of entries in the cache.  */
    protected final int maxEntries;
    /** Total number of reference to the cached objects */
    protected int refCount;
    private final long expiration;
    private Method bs = null;           // BackingStore.retrieve() method, if implemented
    private Object[] bsArgs = null;     // arguments for BackingStore.retrieve()

    
    private void init()
    {
        // leave room in the cache to avoid auto-resizing.
        cache = new HashMap((int) (maxEntries / LOAD_FACTOR + 1), LOAD_FACTOR);
        refCount = 0;
        
        // check if the concrete class implements the "BackingStore" interface
        Class[] c = getClass().getInterfaces();
        String bsClass = BackingStore.class.getName();
        for (int i = 0; i < c.length; i++) {
            if (bsClass.equals(c[i].getName())) {
                try {
                    bs = c[i].getMethod("retrieve", new Class[] { Class.forName("java.lang.Object") });
                    bsArgs = new Object[1];
                    break;
                } catch (Exception e) {}
            }
        }
    }

    /**
     * Construct a cache object.
     * 
     * @param cacheSize
     *            The max. number of objects that could be cached. When the
     *            cache is filled up, a selected subset of objects will be
     *            flushed from the cache. The replacement algorithm is
     *            implemented in the <code>refreshCache</code> method.
     * @see #flushCache
     * @param expiration
     *            Max. time (in milliseconds) any object can remain in the cache
     *            before being flushed. If set to <code>-1</code>, the cache
     *            never expires.
     */
    public Cache(int cacheSize, long expiration) {
        maxEntries = cacheSize;
        this.expiration = expiration;
        init();
    }

    /**
     * Construct a cache object with default cache size. The default size is
     * 5000.
     * 
     * @param expiration
     *            Max. time (in milliseconds) any object can remain in the cache
     *            before being flushed. If set to <code>-1</code>, the cache
     *            never expires.
     */
    public Cache(long expiration) {
        maxEntries = MAX_ENTRIES;
        this.expiration = expiration;
        init();
    }
    
    /**
     * Construct a cache object with default cache size (5000), and default
     * expiration, which is <b>never</b>.
     * 
     */
    public Cache() {
        maxEntries = MAX_ENTRIES;
        expiration = -1;
        init();
    }
    
    
    /**
     * Explicitly put an object into the cache. This method should be used only
     * when the BackingStore interface is not implemented.
     * 
     * @see BackingStore
     * @param key
     *            Key for identifying the cached object.
     * @param value
     *            Value to be cached.
     * @return The object that is just put into the cache.
     */
    protected synchronized Object put(Object key, Object value) {
        CacheObject co = newCacheObject(value, expiration);
        cache.put(key, co);
        ++refCount;
        return co.get();
    }
   
    
    /**
     * Retrieve a cached object. If the BackingStore interface is implemented,
     * and if there is a cache miss, the corresponding value associated with the
     * given key will be transparently fetched from the backing store, which in
     * most cases should be the database. If BackingStore is not implemented,
     * <code>null</code> is returned upon cache misses.
     * 
     * @param key
     *            Key for identifying the cached object
     * @return The cached object, or null if there is a cache miss and
     *         BackingStore interfact is not implemented.
     * @throws BackingStoreException
     *             If BackingStore is implemented but the corresponding object
     *             cannot be found.
     * @see BackingStore
     */
    public synchronized Object get(Object key) throws BackingStoreException {
        CacheObject value = (CacheObject) cache.get(key);
        if (value != null) {
            // check for cache expiration
            if (! (expiration >= 0 && value.expirationTime <= System.currentTimeMillis())) {
                ++refCount;
                return value.get();
            }
        } 
        
        // if the cache is full, clear out the old entries
        if (cache.size() >= maxEntries)
            flushCache();
        
        // retrieve the object from backing store if this method is supported
        if (bs != null) {
            try {
                bsArgs[0] = key;
                return put(key, bs.invoke(this, bsArgs));
            } catch (InvocationTargetException e) {
                BackingStoreException be;
                Throwable cause = e.getCause();
                if (cause == null) {
                    be = new BackingStoreException(e.getMessage());
                    be.setStackTrace(e.getStackTrace());
                    throw be;
                } else if (cause instanceof BackingStoreException) {
                    be = (BackingStoreException) cause;
                    throw be;
                } else {
                    be = new BackingStoreException(cause.getMessage());
                    be.setStackTrace(cause.getStackTrace());
                    throw be;
                }
            } catch (IllegalAccessException e) {
                RuntimeException re = new RuntimeException(e.getMessage());
                re.setStackTrace(e.getStackTrace());
                throw re;
            }
        } else // the caller is filling the cache explicitly
            return null;
    }
    
    /**
     * Select a subset of cache entries to be removed when the cache table is full.
     */
    abstract protected void flushCache();

    /**
     * Create a new cache entry that holds the specified cachable object.
     * @param value Object to be cached.
     * @param expiration Number. of milliseconds this object could remain cached.
     * @return A cache entry.
     */
    abstract protected CacheObject newCacheObject(Object value, long expiration);

}

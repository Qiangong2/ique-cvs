package com.broadon.util;

import java.util.Date;

/**
 * Structure for holding a cached object.
 * @see com.broadon.util.Cache
 */
/**
 * @author ho
 *
 */
public class CacheObject
{
    final Object value;                 // object to be cached

    long expirationTime;                // Java time that this object should be expired.

    /**
     * Create a CacheObject 
     * @param value Object to be cached.
     * @param expiration  Expiration time (in milliseconds since Epoch).
     */
    protected CacheObject(Object value, long expiration) {
        this.value = value;
        if (expiration > 0) {
            expirationTime = System.currentTimeMillis() + expiration;
        } else
            expirationTime = Long.MAX_VALUE;

    }

    
    /**
     * @return The cached object.
     */
    protected Object get()
    {
        return value;
    }

}

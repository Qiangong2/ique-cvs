package com.broadon.util;

import java.util.Properties;
import java.io.*;
import java.sql.*;

import oracle.jdbc.pool.*;

/**
 * Standalone version of database access class -- for used by standalone tools.
 */
public class Database
{
    static final String DBproperty = "DB.properties";
    static final String DB_url = "DB_URL";
    static final String DB_user = "DB_USER";
    static final String DB_password = "DB_PASSWORD";

    OracleDataSource ds;

    /**
     * Create an <code>Database</code> object, with a single
     * (non-pooled) connection.  
     */
    public Database()
	throws IOException, SQLException
    {
	Properties prop = new Properties();
	String dbProp = System.getProperty(DBproperty);
	if (dbProp == null)
	    throw new IOException("DB.properties file undefined");
	prop.load(new FileInputStream(dbProp));
	ds = new OracleDataSource();
	ds.setURL(prop.getProperty(DB_url));
	ds.setUser(prop.getProperty(DB_user));
	ds.setPassword(prop.getProperty(DB_password));
    }


    /**
     * Create a new DB connection.
     */
    public Connection getConnection()
	throws SQLException
    {
	return ds.getConnection();
    }

    /*
    public static void main(String[] args) {
	try {
	    Database db = new Database();
	    Connection conn = db.getConnection();
	    Statement stmt = conn.createStatement();
	    ResultSet rs = stmt.executeQuery("SELECT SYSDATE FROM DUAL");
	    if (rs.next()) {
		System.out.println(rs.getString(1));
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }
    */
}
    
   

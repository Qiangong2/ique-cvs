package com.broadon.util;


/**
 * An interface invoked by the Cache class fetch an object from the backing
 * store to be cached.
 * @see com.broadon.util.Cache
 */
public interface BackingStore
{
    
    /**
     * Retrieve an object from the backing store.
     * @param key Identify the object to be fetched.
     * @return The fetched object.
     * @throws BackingStoreException If the specified key cannot be found.
     */
    public Object retrieve(Object key) throws com.broadon.util.BackingStoreException;
}

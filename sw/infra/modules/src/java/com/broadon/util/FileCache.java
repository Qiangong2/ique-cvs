package com.broadon.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * <c>FileCache</c> is a <c>Cache</c> that stores the objects into
 * files under a specified directory. It is configured in terms of the
 * following parameters:
 * <p>
 *    <c>rootPath</c>:  The directory into which cached content will be
 *               created.  Each <c>FileCache</c> object must have a unique
 *               rootPath that is not shared with any other FileCache and
 *               is not used for any other purposes.  If the <c>rootPath</c>
 *               does not exist a new one will be created.
 * <p>
 *    <c>maxMbytes</c>:  The maximum number of mega-bytes that will be
 *                stored in the cache.  When this is exceeded an LRU
 *                algorithm will kick in to remove old cached content.
 * <p>
 *    <c>maxFiles</c>:  Each object will be stored in one file, and the
 *                maximum number of files that will be stores is therefore
 *                also a limit on the maximum number of objects that will
 *                be stored.
 * <p>
 *    <c>numKeySets</c>: This cache allows each cached value to be 
 *                accessable through separate sets of keys.  Each key
 *                will separately map to the cached file, and for each
 *                set there <em>must</em> be a unique key for <em>each</em>
 *                cached value.  The only requirement on a key is
 *                that it implements <c>hashCode</c> and <c>equals</c>
 *                and has a good hash-function for all keys within the
 *                same set of keys.
 * <p>
 *    <c>clearCache</c>: Determines whether or not the given cache 
 *                directory should be cleared out upon construction
 *                of a new FileCache.  Otherwise the cache will be
 *                reconstructed from files in the cache directory.
 * <p>
 * This cache is useful when objects are retrieved from a slow resource
 * (e.g., database, removable drive, etc.).
 */
public class FileCache
{
    // -------------------------------------------------------------
    // -------------------- Static variables -----------------------
    // -------------------------------------------------------------

    private static final boolean TRACE = true;
    private static final String FILECACHE_PREFIX = "CACHED_";
    private static final String FILECACHE_SUFFIX = ".object";
    private static final String FILETEMP_PREFIX = "TEMP_";
    private static final String FILETEMP_SUFFIX = ".temp";

    private static Map fileCaches = new HashMap(5);


    // -------------------------------------------------------------
    // -------------------- Member variables -----------------------
    // -------------------------------------------------------------

    private final File             cacheDir;        // the the cache directory
    private final long             maxCachedBytes;  // the maximum bytes cached
    private final long             maxCachedFiles;  // the maximum files cached

    // Requiring thread-safe access synchronized on this object
    //
    private long   numCachedBytes;  // the number of bytes cached
    private long   numCachedFiles;  // the number of files cached
    private Map[]  cacheMap;        // the map to the cached files
    private Map    lruMap;          // a special map that maintains LRU order
    private int    hit;             // hit count
    private int    missed;          // missed count


    // -------------------------------------------------------------
    // -------------------- Nested private interfaces ---------------
    // -------------------------------------------------------------

    private static class CachedValue {
        private final KeySet ks;
        private final File   cached;
        public CachedValue(KeySet ks, File cached) 
        {
            this.ks = ks; 
            this.cached = cached;
        }
        public KeySet getKeySet() {return ks;}
        public File   getCached() {return cached;}

    }


    // -------------------------------------------------------------
    // -------------------- Nested public interfaces ---------------
    // -------------------------------------------------------------

    public static interface KeySet
    {
        public int    numSets();    // >= 1
        public Object key(int i); // non-null for 0 <= i < numAliases
    }

    public static interface RestoreCache
    {
        public KeySet apply(File cached); // Returns NULL upon an error
    }

    public static class CachedStream {
        private final KeySet          ks;
        private final FileInputStream cached;
        public CachedStream(CachedValue v) throws FileNotFoundException
        {
            this.ks = v.getKeySet(); 
            this.cached = new FileInputStream(v.getCached());
        }
        public KeySet getKeySet() {return ks;}
        public FileInputStream getCached() {return cached;}
    }

    // -------------------------------------------------------------
    // -------------- Private and protected Methods ----------------
    // -------------------------------------------------------------
    
    private void err(String msg)
    {
        final DateFormat dateFmt = 
            DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.LONG);
        System.err.println(dateFmt.format(new Date()) + " ERROR " + msg);
    }
    private void log(String msg)
    {
        final DateFormat dateFmt = 
            DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.LONG);
        System.out.println(dateFmt.format(new Date()) + " INFO " + msg);
    }


    private FileCache(String       rootPath,
                      long         maxMbytes,
                      long         maxFiles,
                      int          numKeySets,
                      RestoreCache restorer,
                      boolean      clearCache)
        throws IOException
    {
        // Constructs a FileCache instance with the given
        // parameters. If the cache should not be cleared, an attempt
        // will be made to reuse the cache-files stored in the given
        // root path, otherwise the root directory will be emptied.
        //
        cacheDir = new File(rootPath);
        cacheDir.mkdirs();

        // Make sure that the path exists.
        //
        if (!cacheDir.exists() || !cacheDir.isDirectory()) {
            throw new IOException("FileCache failed to create directory " +
                                  cacheDir.getAbsolutePath());
	    }

        // Limits and statistics
        //
        maxCachedBytes = maxMbytes * 1024 * 1024; 
        maxCachedFiles = maxFiles; 
        numCachedBytes = 0;
        numCachedFiles = 0;

        // A separate cache for each set of keys
        //
        cacheMap = new Map[numKeySets];
        for (int i = 0; i < cacheMap.length; ++i) {
             cacheMap[i] = new HashMap(200);
        }

        // A separate cache keyed on key(0) for maintaining
        // the LRU order of cached entries.
        //
        lruMap = new LinkedHashMap(200, 0.75F, true);
        
        // Clear the cache if necessary
        //
        if (clearCache)
            clearCacheDir();
        else
            restoreCacheDir(restorer);
    } // FileCache constructor

    
    private boolean isCached(KeySet ks)
    {
        return cacheMap[0].get(ks.key(0)) != null;
    }


    private File renameToCached(KeySet ks, File temp) throws IOException
    {
        // Renames the given temp-file to a unique cached file
        // for the given content, using key(0) as the suffix.  Both files
        // must be on the same file-system, the assumption being that
        // getNewTempFileDescr() was used to create the temp file.
        //
        final String fname = FILECACHE_PREFIX + ks.key(0) + FILECACHE_SUFFIX;
        final File   dest = new File(cacheDir, fname);
        dest.delete();
        if (!temp.renameTo(dest))
            throw new IOException("Failed to rename " + 
                                  temp.getAbsolutePath() +
                                  " to " + dest.getAbsolutePath());
        else
            return dest;
    }


    private boolean delete(File f)
    {
        if (f.isDirectory()) {
            boolean ok = true;
            File[]  src = f.listFiles();
            for (int i = 0; ok && i < src.length; ++i) {
                ok = delete(src[i]);
            }
        }
        return f.delete();
    }


    private void clearCacheDir() throws IOException
    {
        // Removes all cached files in the cacheDir
        //
        boolean ok = true;
        File[]  src = cacheDir.listFiles();
        for (int i = 0; ok && i < src.length; ++i) {
            ok = delete(src[i]);
        }
        if (!ok) {
            throw new IOException("FileCache failed to remove files in " +
                                  cacheDir.getAbsolutePath());
        }
    }


    private void remove(KeySet ks)
    {
        // We do not usually use this method, as most removals happen 
        // in insert() and are due to the LRU algorithm.  We use this
        // method to clean up inconsistent states (such as missing files).
        //
        CachedValue v = (CachedValue)cacheMap[0].get(ks.key(0));
        lruMap.remove(ks.key(0));
        for (int m = 0; m < cacheMap.length; ++m) {
            cacheMap[m].remove(ks.key(m));
        }
        numCachedBytes -= v.getCached().length();
        numCachedFiles -= 1;
        delete(v.getCached());
    }


    private void insert(KeySet ks, File cached)
    {
        // Precondition: The ks is not already in the cache.
        //
        // Inserts a shared cached value into the cacheMap
        // and into the lru sequence, taking care to keep track
        // of limits and removing lru content if necessary.
        //
        long       newCachedBytes = cached.length() + numCachedBytes;
        long       newCachedFiles = numCachedFiles + 1;
        Iterator   lru = lruMap.values().iterator();
        while (lru.hasNext() &&
               ((newCachedFiles > maxCachedFiles) ||
                (newCachedBytes > maxCachedBytes))) {
            CachedValue v = (CachedValue)lru.next();
            lru.remove();
            for (int m = 0; m < cacheMap.length; ++m) {
                cacheMap[m].remove(v.getKeySet().key(m));
            }
            newCachedBytes -= v.getCached().length();
            --newCachedFiles;
            delete(v.getCached());
        }

        CachedValue kv = new CachedValue(ks, cached);
        for (int m = 0; m < cacheMap.length; ++m) {
            cacheMap[m].put(ks.key(m), kv);
        }
        lruMap.put(ks.key(0), kv);
        numCachedBytes = newCachedBytes;
        numCachedFiles = newCachedFiles;
    } // insert


    private void restoreCacheDir(RestoreCache restorer)
    {
        // Attempt to restore all files in the cacheDir, removing
        // files that have a wrong prefix or cannot be restored, and
        // inserting valid files into the maps.
        //
        File[]  src = cacheDir.listFiles();
        for (int i = 0; i < src.length; ++i) {
            if (src[i].isFile() &&
                src[i].getName().startsWith(FILECACHE_PREFIX)) {
                //
                // Looks like a valid cache file, so try to restore it
                //
                KeySet ks = restorer.apply(src[i]);
                if (ks != null && 
                    ks.numSets() == cacheMap.length &&
                    !isCached(ks)) {
                    insert(ks, src[i]);
                }
                else {
                    delete(src[i]); // Clear out invalid file, ignoring errors
                }
            }
            else {
                delete(src[i]); // Clear out invalid file, ignoring errors
            }
        }
    } // restoreAll



    // -------------------------------------------------------------
    // -------------------- Factory Methods ------------------------
    // -------------------------------------------------------------

    /**
     * Returns the FileCache instance with the path of the root directory
     * for the persistent cache.
     * <p>
     * Use the existing cache for the given path, if one exists, and
     * otherwise create a new one with the given parameters. The
     * clearCache will only happen if a new cache is created.  If the cache
     * should not be cleared, an attempt will be made to reuse the cache
     * stored in the given root path.
     *
     * @param rootPath   The path of the root directory of the persistent cache
     * @param maxMbytes  The maximum cache size in megabytes
     * @param maxFiles   The maximum number of files/objects in the cache
     * @param numKeySets The number of disjoint key-sets to a particular value
     * @param restorer   A function object extracting a key for a cache-file;
     *                   Can be null when clearCache is true.
     * @param clearCache Whether or not to clear any existing cache
     */
    public static FileCache getFileCache(String       rootPath,
                                         long         maxMbytes,
                                         long         maxFiles,
                                         int          numKeySets,
                                         RestoreCache restorer,
                                         boolean      clearCache)
        throws IOException
    {
        FileCache fileCache;
        synchronized(fileCaches) {
            fileCache = (FileCache)fileCaches.get(rootPath);
            if (fileCache == null) {
                fileCache = new FileCache(rootPath, 
                                          maxMbytes, 
                                          maxFiles,
                                          numKeySets,
                                          restorer,
                                          clearCache);
                fileCaches.put(rootPath, fileCache);
            }
        }
        return fileCache;
    } //  getFileCache



    // -------------------------------------------------------------
    // --------------------- Public Methods ------------------------
    // -------------------------------------------------------------


    /** Get the cached file as an input stream, along with the full
     *  KeySet used when the file was cached.  Returns null if no such
     *  entry exists.
     *
     * @param key      The key used in the KeySet at index keyIndex
     * @param keyIndex The index for which the key should be applied
     * @throws IllegalStateException When there is an unexpected internal
     *                               error
     */
    public synchronized CachedStream getStream(Object key, int keyIndex)
    {
        // Get the cached value if it exists
        //
        CachedStream s = null;
        CachedValue v = (CachedValue)cacheMap[keyIndex].get(key);
        if (v != null) {
            //
            // Convert value into an open input stream
            //
            try {
                s = new CachedStream(v);
            } 
            catch (FileNotFoundException e) {
                //
                // Something is wrong with the cached file, so remove this
                // entry altogether.
                //
                remove(v.getKeySet());
                err("Ignoring corrupt file in FileCaching");
            }
            
            // Update LRU map
            //
            if (s != null && v != lruMap.get(v.getKeySet().key(0))) {
                //
                // Should never occur
                //
                throw new IllegalStateException(
                    "Internal error: mismatched lru and cached entries");
            }
        }

        if (s == null) {
            if (TRACE) log("FileCache.getStream " +
                           key + " index=" + 
                           keyIndex + " MISSED");
            ++missed;
        }
        else {
            if (TRACE) log("FileCache.getStream " +
                           key + " index=" + 
                           keyIndex + " FOUND");
            ++hit;
        }
        return s;
    } // getStream


    /** Insert and get a cached file as an input stream, along with the
     *  full KeySet ks as one atomic operation.  This is the only insert
     *  mechanism we support as a separate insert and get method is not
     *  threadsafe and can result in the get failing if LRU kicks in on
     *  an insert by another thread between the insert and get for this
     *  thread.
     *
     *  The client must close the stream when done with it.
     *  This method never returns null.
     *
     * @param key      The key used in the KeySet at index keyIndex
     * @param keyIndex The index for which the key should be applied
     * @throws IllegalStateException When there is an unexpected internal
     *                               error
     * @throws IOException When the temp file cannot be renamed to a
     *                     cached file-name.
     */
    public synchronized CachedStream insertAndGetStream(KeySet ks, File temp)
        throws IOException
    {
       if (isCached(ks)) {
            //
            // Ignore a second attempt to insert the content.
            //
            delete(temp);
        }
        else {
            File cached = renameToCached(ks, temp);
            if (TRACE) log("FileCache.insert:" + ks + 
                           " File:" + cached.getAbsolutePath());
            insert(ks, cached);
        }
        return getStream(ks.key(0), 0);
    } // insertAndGetStream


    /** While not required, we strongly recommend the use of this
     *  method for creating the temp file used in a subsequent call to
     *  insertAndGetStream().  This serves two purposes:
     *
     *    1) Making sure the temp file is on the same disk partition 
     *       as the cached file, and therefore enabling renaming of 
     *       the file.
     *
     *    2) Allowing the content to be written to file without locking
     *       the whole FileCache, thus allowing better thread-utilization
     *       as all synchronized methods in this class should execute very
     *       quickly and do not involve time-consuming IO operations.
     */
    public File getNewTempFile(Object key)
        throws IOException
    {
        // Use the primary key (key(0)) as the unique identifier in the
        // file-name.
        //
        final String fname = FILETEMP_PREFIX + key.toString();
        return File.createTempFile(fname, FILETEMP_SUFFIX, cacheDir);
    }

}

package com.broadon.util;

import java.sql.SQLException;

public class BackingStoreException extends Exception
{

    public BackingStoreException()
    {
        super();
    }

    public BackingStoreException(String message)
    {
        super(message);
    }
    
    
    public BackingStoreException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Rewrap a SQLException as a BackingStoreException and then throw it.
     * @param e The original SQLException
     * @throws BackingStoreException
     */
    public static BackingStoreException rewrap(SQLException e) {
        BackingStoreException bse = new BackingStoreException("SQL Exception", e);
        bse.setStackTrace(e.getStackTrace());
        return bse;
    }
}

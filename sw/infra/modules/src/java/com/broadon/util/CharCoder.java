package com.broadon.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;

/**
 * The <code>CharCoder</code> class is an abstract class whose subclasses
 * encodes a binary array to a String and decodes a String back to a binary
 * array.
 *
 * @version	$Revision: 1.2 $
 */
public abstract class CharCoder
{
    protected PrintWriter	printWriter;

    /**
     * Returns the encoding block size.
     *
     * @return	int
     */
    protected abstract int encodeBlockSize();

    /**
     * Returns the encoding line size.
     *
     * @return	int
     */
    protected abstract int encodeLineSize();

    /**
     * Encodes the prefix of the data.
     *
     * @param	out			the destination
     *
     * @throws	IOException
     */
    protected void encodePrefix(Writer out)
	throws IOException
    {
	printWriter = new PrintWriter(out);
    }

    /**
     * Encodes the suffix of the data.
     *
     * @param	out			the destination
     *
     * @throws	IOException
     */
    protected void encodeSuffix(Writer out)
	throws IOException
    {
    }

    /**
     * Encodes the prefix of each output line.
     *
     * @param	out			the destination
     * @param	size			the size of the current line
     *
     * @throws	IOException
     */
    protected void encodeLinePrefix(Writer out, int size)
	throws IOException
    {
    }

    /**
     * Encodes the suffix of each output line.
     *
     * @param	out			the destination
     *
     * @throws	IOException
     */
    protected void encodeLineSuffix(Writer out)
	throws IOException
    {
	printWriter.println();
    }

    /**
     * Encodes a block of data.
     *
     * @param	out			the destination
     * @param	block			the block of data to be encoded
     * @param	offset			the offset from block
     * @param	size			the size to be encoded
     *
     * @throws	IOException
     */
    protected abstract void encodeBlock(Writer out,
					byte[] block,
					int offset,
					int size)
	throws IOException;

    /**
     * Encodes bytes from the source into text characters to the destination.
     *
     * @param	in			the source
     * @param	out			the destination
     *
     * @throws	IOException
     */
    public void encode(InputStream in, Writer out)
	throws IOException
    {
	BufferedInputStream	bin = new BufferedInputStream(in);
	BufferedWriter		bout = new BufferedWriter(out);
	int			blockSize = encodeBlockSize();
	int			lineSize = encodeLineSize();
	byte[]			buffer = new byte[lineSize];

	encodePrefix(bout);
	while (true)
	{
	    int	size = readBlock(bin, buffer, 0, lineSize);

	    if (size <= 0)
	    {
		break;
	    }
	    encodeLinePrefix(bout, size);
	    for (int n = 0; n < size; n += blockSize)
	    {
		encodeBlock(bout,
			    buffer,
			    n,
			    (blockSize > (size - n)) ? (size - n) : blockSize);
	    }
	    if (size < lineSize)
	    {
		break;
	    }
	    encodeLineSuffix(bout);
	}
	encodeSuffix(bout);
	bout.flush();
    }

    /**
     * Encodes bytes from the source into text characters to the destination.
     *
     * @param	in			the source
     * @param	out			the destination
     *
     * @throws	IOException
     */
    public void encode(InputStream in, OutputStream out)
	throws IOException
    {
	OutputStreamWriter	writer = new OutputStreamWriter(out);

	try
	{
	    encode(in, writer);
	}
	finally
	{
	    writer.close();
	}
    }

    /**
     * Encodes bytes from the source into text characters to the
     * destination.
     *
     * @param	data			the source in byte array
     * @param	out			the destination
     *
     * @throws	IOException
     */
    public void encode(byte[] data, Writer out)
	throws IOException
    {
	encode(new ByteArrayInputStream(data), out);
    }

    /**
     * Returns the encoded result of the given source.
     *
     * @param	data			the source in byte array
     *
     * @return	String
     */
    public String encode(byte[] data)
    {
	StringWriter	out = new StringWriter();

	try
	{
	    encode(data, out);
	    return out.toString();
	}
	catch (IOException ie)
	{
	    ie.printStackTrace();
	    throw new Error(ie.getLocalizedMessage());
	}
	finally
	{
	    try
	    {
		out.close();
	    }
	    catch (IOException ie)
	    {
	    }
	}
    }

    /**
     * Returns the decoding block size.
     *
     * @return	int
     */
    protected abstract int decodeBlockSize();

    /**
     * Returns the decoding line size.
     *
     * @return	int
     */
    protected abstract int decodeLineSize();

    /**
     * Decodes the prefix of the data.
     *
     * @param	in			the source
     * @param	out			the destination
     *
     * @throws	IOException
     */
    protected void decodePrefix(Reader in, OutputStream out)
	throws IOException
    {
    }

    /**
     * Decodes the suffix of the data.
     *
     * @param	in			the source
     * @param	out			the destination
     *
     * @throws	IOException
     */
    protected void decodeSuffix(Reader in, OutputStream out)
	throws IOException
    {
    }

    /**
     * Returns the number of bytes that will be decoded for this line. If
     * this information is not available, returns the maximum number of
     * bytes that could have been encoded on the line.
     *
     * @param	in			the source
     * @param	out			the destination
     *
     * @throws	IOException
     */
    protected int decodeLinePrefix(Reader in, OutputStream out)
	throws IOException
    {
	return decodeLineSize();
    }

    /**
     * Post processes the line.
     *
     * @param	in			the source
     * @param	out			the destination
     *
     * @throws	IOException
     */
    protected void decodeLineSuffix(Reader in, OutputStream out)
	throws IOException
    {
    }

    /**
     * Decodes a block of data.
     *
     * @param	in			the source
     * @param	out			the destination
     * @param	size			the size of this block
     *
     * @throws	IOException
     */
    protected void decodeBlock(Reader in, OutputStream out, int size)
	throws IOException
    {
	throw new EOFException();
    }

    /**
     * Decodes the text characters from the source and write the
     * decoded octets to the destination.
     *
     * @param	in			the source
     * @param	out			the destination
     *
     * @throws	IOException
     */
    public void decode(Reader in, OutputStream out)
	throws IOException
    {
	BufferedReader		bin = new BufferedReader(in);
	BufferedOutputStream	bout = new BufferedOutputStream(out);

	decodePrefix(bin, bout);
	while (true)
	{
	    try
	    {
		int	blockSize = decodeBlockSize();
		int	size = decodeLinePrefix(bin, bout);
		int	n;

		for (n = 0; (n + blockSize) < size; n += blockSize)
		{
		    decodeBlock(bin, bout, blockSize);
		}
		if (n < size)
		{
		    decodeBlock(bin, bout, size - n);
		}
		decodeLineSuffix(bin, bout);
	    }
	    catch (EOFException e)
	    {
		break;
	    }
	}
	decodeSuffix(bin, bout);
	bout.flush();
    }

    /**
     * Decodes the text characters from the source and write the
     * decoded octets to the destination.
     *
     * @param	in			the source
     * @param	out			the destination
     *
     * @throws	IOException
     */
    public void decode(InputStream in, OutputStream out)
	throws IOException
    {
	decode(new InputStreamReader(in), out);
    }

    /**
     * Decodes text characters from the data string, and returns the decoded
     * octets as byte[].
     *
     * @param	data			the source
     *
     * @return	byte[]
     *
     * @throws	IOException
     */
    public byte[] decode(String data)
	throws IOException
    {
	ByteArrayOutputStream	out = new ByteArrayOutputStream();

	try
	{
	    decode(new StringReader(data), out);
	    return out.toByteArray();
	}
	finally
	{
	    try
	    {
		out.close();
	    }
	    catch (IOException ie)
	    {
	    }
	}
    }

    /**
     * Decodes text characters from the input stream, and returns the decoded
     * octets as byte[].
     *
     * @param	in			the source
     *
     * @return	byte[]
     *
     * @throws	IOException
     */
    public byte[] decode(Reader in)
	throws IOException
    {
	ByteArrayOutputStream	out = new ByteArrayOutputStream();

	try
	{
	    decode(in, out);
	    return out.toByteArray();
	}
	finally
	{
	    out.close();
	}
    }

    /**
     * Reads at most a number of bytes into buffer, starting at offset,
     * specified by size.
     *
     * @param	in			the source
     * @param	buffer			the destination
     * @param	offset			the starting location
     * @param	size			the number of bytes to be read
     *
     * @return	int, the number of bytes read
     *
     * @throws	IOException
     */
    protected int readBlock(Reader in, byte[] buffer, int offset, int size)
	throws IOException
    {
	if (buffer == null || size == 0)
	{
	    return 0;
	}
	for (int n = 0; n < size; n++)
	{
	    int	ch = in.read();

	    if (ch == -1)
	    {
		return (n == 0) ? -1 : n;
	    }
	    buffer[n + offset] = (byte)ch;
	}
	return size;
    }

    /**
     * Reads at most a number of bytes into buffer, starting at offset,
     * specified by size.
     *
     * @param	in			the source
     * @param	buffer			the destination
     * @param	offset			the starting location
     * @param	size			the number of bytes to be read
     *
     * @return	int, the number of bytes read
     *
     * @throws	IOException
     */
    protected int readBlock(InputStream in, byte[] buffer, int offset, int size)
	throws IOException
    {
	if (buffer == null || size == 0)
	{
	    return 0;
	}
	for (int n = 0; n < size; n++)
	{
	    int	ch = in.read();

	    if (ch == -1)
	    {
		return (n == 0) ? -1 : n;
	    }
	    buffer[n + offset] = (byte)ch;
	}
	return size;
    }
}

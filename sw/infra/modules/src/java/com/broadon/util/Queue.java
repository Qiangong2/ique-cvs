package com.broadon.util;

import java.util.List;

/**
 * The <code>Queue</code> class provides a thread-safe queueing mechanism.
 */
public class Queue
{
    public final static int	DEFAULT_QUEUE_SIZE = 100;

    protected Object[]		queue;		// the actual queue of objects
    protected int		head;		// head of the queue
    protected int		tail;		// tail of the queue
    private int			fullWaiters;	// wait # to enqueue when full
    private int			emptyWaiters;	// wait # to dequeue when empty
    private boolean		toTerminate;	// true => terminating

    /**
     * Constructs a Queue instance.
     */
    public Queue()
    {
	this(DEFAULT_QUEUE_SIZE);
    }

    /**
     * Constructs a Queue instance.
     *
     * @param	queueSize		the size of this Queue
     */
    public Queue(int queueSize)
    {
        this.queue = new Object[queueSize];
	head = 0;
	tail = -1;
	fullWaiters = 0;
	emptyWaiters = 0;
	start();
    }

    /**
     * Appends the given object to the tail of the queue.
     *
     * @param	object			the object to be appended
     */
    public void enqueue(Object object)
    {
        while (true)
	{
            if (full())
	    {
                waitWhenQueueFull();
	    }
	    /*
	     * The queue object is used for protecting the queuing
	     * operations, and for synchronization by the empty
	     * queue waiters.
	     */
            synchronized (queue)
	    {
		/*
		 * It may still be full by the time it gets here.
		 * In this case, go back to wait.
		 */
                if (!full())
		{
                    /*
                     * Enqueue.
                     */
                    if (++tail == queue.length)
		    {
			/*
			 * Wrap around.
			 */
                        tail = 0;
		    }
                    queue[tail] = object;
                    /*
                     * Wake up one waiting on the empty queue, if any.
                     */
                    notifyEmptyQueueWaiter(false);
                    break;
                }
            }
        }
    }

    /**
     * Appends the given objects to the tail of the queue.
     * This method can just call the single object enqueue method
     * multiple time. It's done this way for performance reason.
     *
     * @param	objects			the objects to be appended
     */
    public void enqueue(List objects)
    {
	this.enqueue(objects.toArray());
    }

    /**
     * Appends the given objects to the tail of the queue.
     * This method can just call the single object enqueue method
     * multiple time. It's done this way for performance reason.
     *
     * @param	objects			the objects to be appended
     */
    public void enqueue(Object[] objects)
    {
        int	size = objects.length;

        for (int n = 0; n < size;)
	{
            if (full())
	    {
                waitWhenQueueFull();
	    }
            synchronized (queue)
	    {
		/*
		 * It may still be full by the time it gets here.
		 * In this case, go back to wait.
		 */
                if (!full())
		{
                    /*
                     * Enqueue.
                     */
                    do
		    {
                        if (++tail == queue.length)
			{
			    /*
			     * Wrap around.
			     */
                            tail = 0;
			}
                        queue[tail] = objects[n++];
			/*
			 * Do as many as possible within this synchronized
			 * block. If the queue is full after this enqueuing,
			 * go back to wait until it's not full again.
			 */
                    } while (!full() && n < size);
                    /*
                     * Wake up those waiting on the empty queue, if any.
                     */
                    notifyEmptyQueueWaiter(true);
                }
            }
        }
    }

    /**
     * Removes an object from the head of the queue.
     *
     * @return	the first object from the queue
     */
    public Object dequeue()
    {
        return this.dequeue(true);
    }

    /**
     * Removes an object from the head of the queue.
     *
     * @param	wait			true  => wait when queue is empty
     *					false => return immediately when empty
     *
     * @return	The first object from the queue.
     */
    public Object dequeue(boolean wait)
    {
        Object		object;

        synchronized (queue)
	{
            if (empty())
	    {
                if (!wait)
		{
		    /*
		     * Caller does not want to wait, return immediately.
		     */
                    return null;
		}
                waitWhenQueueEmpty();
		if (toTerminate)
		{
		    return null;
		}
            }
            /*
             * Dequeue.
             */
            object = queue[head];
            queue[head] = null;
            if (head == tail)
	    {
                /*
                 * Empty the queue.
                 */
                head = 0;
                tail = -1;
            }
            else if (++head == queue.length)
	    {
		/*
		 * Wrap around.
		 */
                head = 0;
	    }
        }
	/*
	 * Wake up those waiting when the queue was full, if any.
	 */
        notifyFullQueueWaiter(false);
        return object;
    }

    /**
     * Removes all available objects from the queue.
     * This method can just call the dequeue method multiple time.
     * It's done this way for performance reason.
     *
     * @param	wait			true  => wait when queue is empty
     *					false => return immediately when empty
     *
     * @return	The available objects from the queue.
     */
    public Object[] dequeueAll(boolean wait)
    {
	Object[]	objects;

        synchronized (queue)
	{
            if (empty())
	    {
                if (!wait)
		{
		    /*
		     * Caller does not want to wait, return immediately.
		     */
                    return null;
		}
                waitWhenQueueEmpty();
		if (toTerminate)
		{
		    return null;
		}
            }
            /*
             * Dequeue.
             */
	    objects = this.dequeueAll(wait, size());
        }
	/*
	 * Wake up those waiting when the queue was full, if any.
	 */
        notifyFullQueueWaiter(true);
        return objects;
    }

    /**
     * Removes at most max available objects from the queue.
     *
     * @param	wait			true  => wait when queue is empty
     *					false => return immediately when empty
     * @param	max			the maximum elements to be returned
     *
     * @return	At most max available objects from the queue.
     */
    public Object[] dequeueAll(boolean wait, int max)
    {
        Object[]	objects;

        synchronized (queue)
	{
            if (empty())
	    {
                if (!wait)
		{
		    /*
		     * Caller does not want to wait, return immediately.
		     */
                    return null;
		}
                waitWhenQueueEmpty();
            }
            /*
             * Dequeue.
             */
            int	size = size();

            if (size > max)
            	size = max;
            objects = new Object[size];
            for (int n = 0; n < objects.length; n++)
	    {
                objects[n] = queue[head];
                queue[head] = null;
                if (head == tail)
		{
                    /*
                     * Empty the queue.
                     */
                    head = 0;
                    tail = -1;
                    break;
                }
		else if (++head == queue.length)
		{
		    /*
		     * Wrap around.
		     */
                    head = 0;
		}
            }
        }
	/*
	 * Wake up those waiting when the queue was full.
	 */
        notifyFullQueueWaiter(true);
        return objects;
    }

    /**
     * Returns the size of the queue.
     * It does not have any synchronized block to protect here because it
     * assumes the caller will protect it when necessary.
     *
     * @return	the number of elements in the queue
     */
    public final int size()
    {
	if (tail < 0)
	{
	    return 0;
	}

	int	size = tail - head + 1;

	if (size <= 0)
	{
	    /*
	     * Handle the wrap around case.
	     */
	    size += queue.length;
	}
	return size;
    }

    /**
     * Returns whether this Queue is empty or not.
     *
     * @return	true => empty queue; false => non-empty queue
     */
    public final boolean isEmpty()
    {
        synchronized (queue)
	{
            return empty();
        }
    }

    /**
     * Starts the queue.
     */
    public final void start()
    {
	this.toTerminate = false;
    }

    /**
     * Wakes up all the empty queue waiters so that it can terminate
     * gracefully.
     */
    public final void terminate()
    {
	this.toTerminate = true;
	synchronized (queue)
	{
	    notifyEmptyQueueWaiter(true);
	}
    }

    /**
     * Returns the information about this Queue in String format.
     */
    public String toString()
    {
        return "Queue[" + size() + ", " + head + ", " + tail + "]";
    }

    /**
     * Returns true if the queue is empty, false otherwise.
     *
     * @return	true if the queue is empty, false otherwise
     */
    protected final boolean empty()
    {
        return tail < 0;
    }

    /**
     * Returns true if the queue is full, false otherwise.
     *
     * @return	true if the queue is full, false otherwise
     */
    protected final boolean full()
    {
	return size() == queue.length;
    }

    /**
     * Waits while the queue is full during enqueuing.
     */
    protected final void waitWhenQueueFull()
    {
        do
	{
	    /*
	     * The this object is used for synchronization by the full
	     * queue waiters.
	     */
            synchronized (this)
	    {
		/*
		 * Make sure the queue is still full before waiting.
		 */
                synchronized (queue)
		{
                    if (!full())
		    {
                    	return;
		    }
                }
                try
		{
                    fullWaiters++;
                    /*
                     * Wait until the queue is not full.
                     */
                    wait();
                    fullWaiters--;
                }
                catch(InterruptedException e)
		{
		    /*
		     * Ignore.
		     */
		}
            }
        } while (full());
    }

    /**
     * Wakes up full queue waiters.
     *
     * @param	all			true  => wake up all waiters
     *					false => wake up one waiter
     */
    protected final void notifyFullQueueWaiter(boolean all)
    {
        if (fullWaiters > 0)
	{
            synchronized (this)
	    {
		if (fullWaiters > 0)
		{
		    if (all)
		    {
			/*
			 * Wake up those waiting on the full queue.
			 */
			notifyAll();
		    }
		    else
		    {
			/*
			 * Wake up one waiting on the full queue.
			 */
			notify();
		    }
		}
            }
        }
    }

    /**
     * Waits while the queue is empty during dequeuing.
     */
    protected final void waitWhenQueueEmpty()
    {
	if (toTerminate)
	{
	    return;
	}

	do
	{
	    try
	    {
		emptyWaiters++;
		/*
		 * Wait until the queue is not empty.
		 */
		queue.wait();
		emptyWaiters--;
	    }
	    catch(InterruptedException e)
	    {
		/*
		 * Ignore.
		 */
	    }
	} while (empty() && !toTerminate);
    }

    /**
     * Wakes up empty queue waiters.
     *
     * @param	all			true  => wake up all waiters
     *					false => wake up one waiter
     */
    protected final void notifyEmptyQueueWaiter(boolean all)
    {
	if (emptyWaiters > 0)
	{
	    if (all)
	    {
		/*
		 * Wake up those waiting on the empty queue.
		 */
		queue.notifyAll();
	    }
	    else
	    {
		/*
		 * Wake up one waiting on the empty queue.
		 */
		queue.notify();
	    }
	}
    }
}

package com.broadon.util;

/**
 * The <code>RSCoder</code> class is converted from a C program written by
 * Simon Rockliff of University of Adelaide: http://www.eccpage.com/rs.c.
 *
 * It provides the mechanism to encode and decode Reed-Solomon codes,
 * which are block-based error correcting codes. They are a subset of
 * BCH codes and are linear block codes.
 * Encoding is in systematic form, decoding via the Berlekamp iterative
 * algorithm.
 *
 * The representation of the elements of GF(2*m) is either in index form
 * where the number is the power of the primitive element alpha, or in
 * polynomial form where the bits represent the coefficients of the
 * polynomial representation of the number. The index form is convenient
 * for multiplication while the polynomial form is good for addition.
 * The two forms can be switched around via lookup tables.
 *
 * This class does not handle erasures at present, but should not be hard
 * to adapt to do this, as it is just an adjustment to the Berlekamp-Massey
 * algorithm. It also does not attempt to decode past the BCH bound -- see
 * Blahut's "Theory and practice of error control codes" for how to do so.
 * There is a later version of this code that performs errors + erasures
 * decoding. Since there is no need for that at the moment, we just base on
 * the original code.
 *
 * @version	$Revision: 1.2 $
 */
public class RSCoder
{
    protected int	m;	// RS code over GF(2**m)
    protected int	t;	// number of correctable errors
    protected int	n;	// n=2**m - 1 (codeword length)
    protected int	k;	// number of data cells
    protected int[]	g;	// generator polynomial of t - error correecting
    protected int[]	alpha;	// alpha**i, where i = 0, 1, ..., n
    protected int[]	index;	// index[alpha**i] = i

    /**
     * Constructs a RSCoder instance for encoding/decoding Reed-Solomon codes.
     * It will compute the following:
     *<pre>
     *	n = 2**m - 1
     *	k = n - 2*t
     *</pre>
     *
     * @param	m			the RS code over GF(2**m)
     * @param	t			the number of correctable errors
     * @param	p			the irreducible polynomial coefficients
     */
    public RSCoder(int m, int t, int[] p)
    {
	this.m = m;
	this.t = t;
	this.n = (1 << m) - 1;
	this.k = n - 2*t;
	generateGF(p);
	generatePolynomial();
    }

    /**
     * Generates GF(2**m) from the irreducible polynomial p(i), where
     * i = [0, m].
     *
     * It also generates the lookup tables, alpha and index, where alpha
     * is from index form to polynomial form lookup, and index is from
     * polynomial form to index form lookup. In other words,
     *<pre>
     *	alpha(i) = alpha**i
     *	index(alpha**i) = i
     *</pre>
     *
     * @param	p			the irreducible polynomial coefficients
     */
    private void generateGF(int[] p)
    {
	int	mask = 1;

	this.alpha = new int[n + 1];
	this.index = new int[n + 1];

	alpha[m] = 0;
	for (int i = 0; i < m; i++)
	{
	    alpha[i] = mask;
	    index[mask] = i;
	    if (p[i] != 0)
	    {
		alpha[m] ^= mask;
	    }
	    mask <<= 1;
	}
	index[alpha[m]] = m;

	mask >>= 1;
	for (int i = m + 1; i < n; i++)
	{
	    int	a = alpha[i - 1];
	    int	ai = (a < mask) ? a << 1 : alpha[m] ^ ((a ^ mask) << 1);

	    alpha[i] = ai;
	    index[ai] = i;
	}
	index[0] = -1;
    }

    /**
     * Generates the polynomial of the t-error correcting, length n
     * Reed-Solomon code from the product of (X + alpha**i), i = [1, 2*t].
     */
    private void generatePolynomial()
    {
	int	twot = n - k;

	this.g = new int[twot + 1];

	g[0] = 2;	// alpha = 2 for GF(2**m)
	g[1] = 1;	// g(x) = (x + alpha), initially
	for (int i = 2; i <= twot; i++)
	{
	    g[i] = 1;
	    for (int j = i - 1; j > 0; j--)
	    {
		g[j] = (g[j] == 0)
			    ? g[j - 1]
			    : g[j - 1] ^ alpha[(index[g[j]] + i) % n];
	    }
	    g[0] = alpha[(index[g[0]] + i) % n];	// never 0
	}
	/*
	 * Convert g[] to index form for quicker encoding
	 */
	for (int i = 0; i <= twot; i++)
	{
	    g[i] = index[g[i]];
	}
    }

    /**
     * Encodes the given data (data[i], i = [0, k-1]) systematically to
     * produce 2*t parity symbols b (b[i], i = [0, 2*t-1]). Both data
     * and b are in polynomial form.
     *<p>
     * Encoding is done by using a feedback shift register with appropriate
     * connections specified by the elements of g.
     *<p>
     * The codeword is c(x) = data(x) * x**(n-k) + b(x).
     *
     * @param	data			the data to be encoded
     *
     * @return	The int array, b, that contains the parity symbols.
     */
    public int[] encode(int[] data)
    {
	int	twot = n - k;
	int[]	b = new int[twot];

	/*
	 * Initialize.
	 */
	for (int i = 0; i < twot; i++)
	{
	    b[i] = 0;
	}
	/*
	 * Encode.
	 */
	for (int i = k - 1; i >= 0; i--)
	{
	    int	feedback = index[data[i] ^ b[twot - 1]];

	    if (feedback == -1)
	    {
		/*
		 * No feedback. Encoder becomes a single-byte shifter.
		 */
		for (int j = twot - 1; j > 0; j--)
		{
		    b[j] = b[j - 1];
		}
		b[0] = 0;
	    }
	    else
	    {
		/*
		 * Feedback term is non-zero.
		 */
		for (int j = twot - 1; j > 0; j--)
		{
		    b[j] = (g[j] == -1)
				? b[j - 1]
				: b[j - 1] ^ alpha[(g[j] + feedback) % n];
		}
		b[0] = alpha[(g[0] + feedback) % n];
	    }
	}
	return b;
    }

    /**
     * Decodes the given codes, that includes both the received data and
     * the parity symbols, in polynomial form back to the original data.
     * If there is no error, the received data and data should be identical.
     * In case of errors, this method tries to make corrections and return
     * the corrected data. It always returns in polynomial form.
     *
     * First compute the 2*t syndromes by substituting alpha**i into
     * codes(x)
     *
     * @param	codes			the received data and parity
     *
     * @return	The corrected data, or null if not correctable.
     */
    public int[] decode(int[] codes)
    {
	boolean	synError = false;
	int	i;

	/*
	 * Convert input to index form.
	 */
	for (i = 0; i < n; i++)
	{
	    codes[i] = index[codes[i]];
	}
	/*
	 * Form the syndromes.
	 */
	int	twot = n - k;
	int[]	s = new int[twot + 1];

	s[0] = 0;
	for (i = 1; i <= twot; i++)
	{
	    s[i] = 0;
	    for (int j = 0; j < n; j++)
	    {
		if (codes[j] != -1)
		{
		    s[i] ^= alpha[(codes[j] + i*j) % n];
		}
	    }
	    if (s[i] != 0)
	    {
		synError = true;
	    }
	    /*
	     * Covert syndrome from polynomial form to index form.
	     */
	    s[i] = index[s[i]];
	}
	if (!synError)
	{
	    /*
	     * No error, convert back to polynomial form.
	     */
	    for (i = 0; i < n; i++)
	    {
		codes[i] = (codes[i] == -1) ? 0 : alpha[codes[i]];
	    }
	}
	else
	{
	    /*
	     * Compute the error location polynomial via the Berlekamp
	     * iterative algorithm.
	     *
	     * Following the terminology of Lin and Costello:
	     *
	     *	d[u]	the 'mu'th discrepancy, where u = 'mu'+1, and 'mu'
	     *		is the step number ranging from -1 to 2*t,
	     *
	     *	l[u]	the degree of the elp at that step, and
	     *
	     *	ul[u]	the difference between the step number and the
	     *		degree of the elp.
	     */
	    int[][]	elp = new int[twot + 2][twot];
	    int[]	d = new int[twot + 2];
	    int[]	l = new int[twot + 2];
	    int[]	ul = new int[twot + 2];
	    int		u;

	    /*
	     * Initialize.
	     */
	    d[0] = 0;			// index form
	    d[1] = s[1];		// index form
	    elp[0][0] = 0;		// index form
	    elp[1][0] = 1;		// polynomial form
	    for (i = 1; i < twot; i++)
	    {
		elp[0][i] = -1;		// index form
		elp[1][i] = 0;		// polynormial form
	    }
	    l[0] = 0;
	    l[1] = 0;
	    ul[0] = -1;
	    ul[1] = 0;
	    u = 0;

	    do
	    {
		u++;
		if (d[u] == -1)
		{
		    l[u + 1] = l[u];
		    for (i = 0; i <= l[u]; i++)
		    {
			elp[u + 1][i] = elp[u][i];
			elp[u][i] = index[elp[u][i]];
		    }
		}
		else
		{
		    /*
		     * Find greatest ul[q] for which d[q] != 0.
		     */
		    int	q = u - 1;

		    while ((d[q] == -1) && (q > 0))
		    {
			q--;
		    }
		    if (q > 0)
		    {
			/*
			 * Found first non-zero d[q]. Now look for the
			 * greatest ul[q].
			 */
			int	j = q;

			do
			{
			    j--;
			    if ((d[j] != -1) && (ul[q] < ul[j]))
			    {
				q = j;
			    }
			} while (j > 0);
		    }
		    /*
		     * Found the qualified q. Store the degree of new
		     * elp polynormial.
		     */
		    l[u + 1] = (l[u] >= l[q] + u - q)
					? l[u]
					: l[q] + u - q;
		    /*
		     * Form new elp.
		     */
		    for (i = 0; i < twot; i++)
		    {
			elp[u + 1][i] = 0;
		    }
		    for (i = 0; i <= l[q]; i++)
		    {
			if (elp[q][i] != -1)
			{
			    try
			    {
				elp[u + 1][i + u - q] =
				    alpha[(d[u] + n - d[q] + elp[q][i]) % n];
			    }
			    catch (ArrayIndexOutOfBoundsException e)
			    {
				/*
				 * Cannot correct error.
				 */
				return null;
			    }
			}
		    }
		    for (i = 0; i <= l[u]; i++)
		    {
			elp[u + 1][i] ^= elp[u][i];
			elp[u][i] = index[elp[u][i]];	// convert to index form
		    }
		}
		ul[u + 1] = u - l[u + 1];
		/*
		 * Form (u+1)th discrepancy.
		 */
		if (u < twot)
		{
		    /*
		     * No discrepancy computed on last iteration.
		     */
		    d[u + 1] = (s[u + 1] == -1) ? 0 : alpha[s[u + 1]];
		    for (i = 1; i <= l[u + 1]; i++)
		    {
			if ((s[u + 1 - i] != -1) && (elp[u + 1][i] != 0))
			{
			    d[u + 1] ^= alpha[(s[u + 1 - i] +
					       index[elp[u + 1][i]]) % n];
			}
		    }
		    d[u + 1] = index[d[u + 1]];		// put to index form
		}
	    } while ((u < twot) && (l[u + 1] <= t));

	    u++;
	    if (l[u] > t)
	    {
		/*
		 * Cannot correct error since elp has degree > t.
		 */
		return null;
	    }
	    /*
	     * Attempt to correct error.
	     *
	     * First, put elp into index form.
	     */
	    for (i = 0; i <= l[u]; i++)
	    {
		elp[u][i] = index[elp[u][i]];
	    }
	    /*
	     * Find roots of the error location polynormial.
	     */
	    int[]	root = new int[t];
	    int[]	loc = new int[t];
	    int[]	reg = new int[t + 1];
	    int[]	z = new int[t + 1];
	    int[]	err = new int[n];
	    int		count = 0;

	    for (i = 0; i <= l[u]; i++)
	    {
		reg[i] = elp[u][i];
	    }
	    for (i = 1; i <= n; i++)
	    {
		int	q = 1;

		for (int j = 1; j <= l[u]; j++)
		{
		    if (reg[j] != -1)
		    {
			reg[j] = (reg[j] + j) % n;
			q ^= alpha[reg[j]];
		    }
		    /*
		     * Store root and error location number indices.
		     */
		    if (q == 0)
		    {
			if (count < t)
			{
			    root[count] = i;
			    loc[count] = n - i;
			}
			count++;
		    }
		}
	    }
	    if (count != l[u])
	    {
		/*
		 * Cannot correct error since number of roots !=
		 * degree of elp.
		 */
		return null;
	    }
	    /*
	     * Form polynomial z[x].
	     */
	    for (i = 1; i <= l[u]; i++)
	    {
		z[i] = (s[i] == -1)
			? (elp[u][i] == -1)
			    ? 0
			    : alpha[elp[u][i]]
			: (elp[u][i] == -1)
			    ? alpha[s[i]]
			    : alpha[s[i]] ^ alpha[elp[u][i]];
		for (int j = 1; j < i; j++)
		{
		    if ((s[j] != -1) && (elp[u][i - j] != -1))
		    {
			z[i] ^= alpha[(elp[u][i - j] + s[j]) % n];
		    }
		}
		z[i] = index[z[i]];		// put into index form
	    }
	    /*
	     * Evaluate errors at locations given by error location
	     * numbers loc[i].
	     */
	    for (i = 0; i < n; i++)
	    {
		err[i] = 0;
		/*
		 * Back to polynomial form.
		 */
		codes[i] = (codes[i] == -1) ? 0 : alpha[codes[i]];
	    }
	    for (i = 0; i < l[u]; i++)
	    {
		/*
		 * Compute numerator of error term first.
		 */
		err[loc[i]] = 1;	// accounts for z[0]
		for (int j = 1; j <= l[u]; j++)
		{
		    if (z[j] != -1)
		    {
			err[loc[i]] ^= alpha[(z[j] + j*root[i]) % n];
		    }
		}
		if (err[loc[i]] != 0)
		{
		    /*
		     * Form denominator of error term.
		     */
		    int	q = 0;

		    err[loc[i]] = index[err[loc[i]]];
		    for (int j = 0; j < l[u]; j++)
		    {
			if (j != i)
			{
			    q += index[1 ^ alpha[(loc[j] + root[i]) % n]];
			}
		    }
		    q %= n;
		    err[loc[i]] = alpha[(err[loc[i]] - q + n) % n];
		    codes[loc[i]] ^= err[loc[i]];
		}
	    }
	}
	/*
	 * Return only the data portion.
	 */
	int[]	data = new int[k];

	for (i = 0; i < k; i++)
	{
	    data[i] = codes[i];
	}
	return data;
    }

    /**
     * Verifies whether the given codes, that includes both the received
     * data and the parity symbols, in polynomial form, is correct or not.
     *
     * @param	codes			the received data and parity
     *
     * @return	true => correct; false => incorrect.
     */
    public boolean verify(int[] codes)
    {
	/*
	 * Convert input to index form.
	 */
	for (int i = 0; i < n; i++)
	{
	    codes[i] = index[codes[i]];
	}
	/*
	 * Verify.
	 */
	int	twot = n - k;

	for (int i = 1; i <= twot; i++)
	{
	    int	s = 0;

	    for (int j = 0; j < n; j++)
	    {
		if (codes[j] != -1)
		{
		    s ^= alpha[(codes[j] + i*j) % n];
		}
	    }
	    if (s != 0)
	    {
		/*
		 * Not the same as the original.
		 */
		return false;
	    }
	}
	/*
	 * Cannot detect any difference, assume the same.
	 */
	return true;
    }
}

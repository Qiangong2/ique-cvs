package com.broadon.util;

import java.io.IOException;
import java.security.SecureRandom;
import java.util.StringTokenizer;

/**
 * The <code>UUID</code> class generates a 128-bit readom number using secureRandom as an UUID.
 *
 * @version $Revision: 1.1 $
 */
public class UUID {

    public static final int LENGTH_IN_BYTES = 16;
    public static final int LENGTH_IN_HEX = 32;
    private static SecureRandom seeder = new SecureRandom();
    
    private byte[] uuid = new byte[LENGTH_IN_BYTES];
    private Base64 base64Encoder = new Base64();
    
    /**
     * Static factory to retrieve a type 4 (pseudo randomly generated) UUID. 
     * The UUID is generated using a cryptographically strong pseudo random number generator. 
     *
     * @return a randomly generated UUID.
     */
    public static UUID randomUUID() {
        byte[] tmp = new byte[LENGTH_IN_BYTES];
        seeder.nextBytes(tmp);
        return new UUID(tmp);
    }
    /**
     * Create a UUID from the string standard representation
     * such as the output from the toString() method.
     * 
     * @param   standard string representation of the UUID
     * 
     * @throws IllegalArgumentException
     */
    public static UUID fromString(String name) {
        StringTokenizer tokens = new StringTokenizer(name, "-");
        StringBuffer sb = new StringBuffer();
        byte[] value = new byte[LENGTH_IN_BYTES];
        /*
        int secondRound = 0;
        while (tokens.hasMoreTokens()) {
            sb.append(tokens.nextToken());
            if (sb.length()==LENGTH_IN_BYTES) {
                byte[] tmp = longToByteArray(Long.parseLong(sb.toString(), 16));
                for (int i=0; i<LENGTH_IN_BYTES/2; i++)
                    value[i+LENGTH_IN_BYTES/2*secondRound]=tmp[i];
                secondRound = 1;
            }
        }
        */
        while (tokens.hasMoreTokens())
            sb.append(tokens.nextToken());
        
        for (int i=0; i<LENGTH_IN_HEX; i++) {
            byte j = (byte) Character.digit(sb.charAt(i), 16);
            if (i==(i/2)*2) {
                value[i/2] = j;
            } else
                value[i/2] = (byte) (value[i/2]*16+j);
        }
        return new UUID(value);
    }
    /**
     * Static factory to constructs an UUID using the specified data. 
     * 
     * @param   byteArray stores the 64 bits of the UUID
     * 
     * @throws IllegalArgumentException
     */
    public static UUID fromByteArray(byte[] byteArray) {
        return new UUID(byteArray);
    }
    /**
     * Static factory to constructs an UUID using the specified data. 
     * 
     * @param   a base64 string representation of this uuid.
     * 
     * @throws IllegalArgumentException
     */
    public static UUID fromBase64(String base64Str) throws IOException {
        if (base64Str == null)
            throw new IllegalArgumentException();
        Base64 base64Encoder = new Base64();
        return UUID.fromByteArray(base64Encoder.decode(base64Str));
    }
    /**
     * Utility method to convert a long to a byte array.
     *
     * @param   value   the long value to be converted
     * @return a byte array
     */
    public static byte[] longToByteArray( long value ) {
        long   temp;
        byte[] rv = new byte[8];
        for (int i = 7; i >=0; i--) {
        //for (int i = 0; i < 8; i++) {
            temp = value & 0xFF;
            if ( temp > 127 )  temp -=256;
                rv[i] = (byte) temp;
             value >>= 8;
        }
        return rv;
    }
    /**
     * Utility method to convert a byte array to a  long value
     *
     * @param   array   the source byte array
     * @param   offset  the start position to convert
     * @param   length  the number of bytes to convert into the long value
     * 
     * @return a long value
     */    
    public static long byteArrayToLong(
            byte[] array, int offset, int length)
    {
        long rv = 0;
        for (int i = 0; i < length; i++ )
        {
            long bv = array[ offset+i ];
            if ( i > 0 & bv < 0 )
                bv +=256;
            rv *= 256;
            rv += bv;
        }
        return rv;
    }
    /**
     * Constructs a new UUID using the specified data. 
     * 
     * @param   mostSigBits the most significant 64 bits of the UUID
     * @param   leastSigBits the least significant 64 bits of the UUID
     */
    public UUID(long mostSigBits, long leastSigBits) {
        super();
        byte[] tmp = longToByteArray(mostSigBits);
        for (int i=0; i<LENGTH_IN_BYTES/2; i++)
            uuid[i] = tmp[i];
        tmp = longToByteArray(leastSigBits);
        for (int i=0, j=LENGTH_IN_BYTES/2; i<LENGTH_IN_BYTES/2; i++, j++)
            uuid[j] = tmp[i];
    }
    private UUID(byte[] byteArray) {
        super();
        if (byteArray == null || byteArray.length < LENGTH_IN_BYTES )
            throw new IllegalArgumentException();
        for (int i=0; i<LENGTH_IN_BYTES; i++)
            uuid[i] = byteArray[i];
    }
    /**
     * Returns a hash code for this UUID. 
     * 
     * @return  a hash code value for this UUID.
     */
    public int hashCode() {
        byte[] tmp = new byte[LENGTH_IN_BYTES/2];
        for (int i=0; i< LENGTH_IN_BYTES/2; i++)
            tmp[i] = uuid[LENGTH_IN_BYTES/2+i];
        return getInt(tmp);
    }
    /**
     * Compares this object to the specified object. 
     * The result is true if and only if the argument is not null, is a UUID object, 
     * has the same variant, and contains the same value, bit for bit, as this UUID. 
     * 
     * @param u The UUID object to compare with.
     * 
     * @return  a boolen indicates if they are equal    
     */
    public boolean equals(Object u) {
        if (u == null)
            return false;
        if (! (u instanceof UUID))
            return false;
        UUID tmp = (UUID) u;
        if (variant() != tmp.variant())
            return false;
        for(int i=0; i<LENGTH_IN_BYTES; i++)
            if (uuid[i] != tmp.uuid[i])
                return false;
        return true;
    }
    /**
     * Return a String object representing this UUID.
     * 
     * @return  string representation of the uuid.
     */
    public String toString() {
        StringBuffer sb = new StringBuffer();
        byte[] tmp = new byte[4];

        for (int i=0; i<4; i++)
            tmp[i] = uuid[i];
        sb.append(hexFormat(getInt(tmp), 8)).append("-");
        tmp[0]='0';
        tmp[1]='0';
        for (int j=0; j<3; j++) {
            for (int i=0; i<2; i++)
                tmp[i+2]=uuid[j*2+4+i];
            sb.append(hexFormat(getInt(tmp), 4).substring(4)).append("-");
        }
        tmp[2]=uuid[10];
        tmp[3]=uuid[11];
        sb.append(hexFormat(getInt(tmp), 4).substring(4));
        for (int i=0; i<4; i++)
            tmp[i] = uuid[12+i];
        sb.append(hexFormat(getInt(tmp), 8));
        return sb.toString();
    }
    /**
     * convert the uuid to a base64 string.
     * 
     * @return a base64 string representation of the 128 bits.
     */
    public String toBase64() {
        return base64Encoder.encode(uuid);
    }
    
    /**
     * Dump the uuid byte array.
     * 
     * @return the 16 bytes array that identifies this uuid.
     */
    public byte[] toByteArray() {
        byte[] ret = new byte[LENGTH_IN_BYTES];
        for (int i=0; i<LENGTH_IN_BYTES; i++)
            ret[i] = uuid[i];
        return ret;
    }
    /**
     * Returns the most significant 64 bits of this UUID's 128 bit value.
     * 
     * @return  a long that contains the most significant 64 bits of this UUID's 128 bit value.
     */
    public long getMostSignificantBits() {
        return byteArrayToLong(uuid, 0, LENGTH_IN_BYTES/2);
    }
    /**
     * Returns the least significant 64 bits of this UUID's 128 bit value.
     * 
     * @return  a long that contains the least significant 64 bits of this UUID's 128 bit value.
     */
    public long getLeastSignificantBits() {
        return byteArrayToLong(uuid, LENGTH_IN_BYTES/2, LENGTH_IN_BYTES/2);
    }
    /**
     * The version number associated with this UUID. 
     * The version number describes how this UUID was generated.
     * We provide only randomly generated UUID that has a value of 4
     * 
     * @return  4
     */
    public int version() {
        return 4;
    }
    /**
     * The variant number associated with this UUID. 
     * The variant number describes the layout of the UUID. 
     * This class uses the Leach-Salz variant that has a value of 2.
     * 
     * @return  2
     */
    public int variant() {
        return 2;
    }
    
    /** <p>
     * A utility method to take a byte array of 4 bytes and produce an int value. 
     * </p>
     * @param abyte0 The byte array containg 4 bytes that represent an IP address.
     * @return An int that is the actual value of the ip address.
     */    
    private int getInt(byte ba[])
    {
        int i = 0;
        int j = 24;
        for(int k = 0; j >= 0; k++)
        {
            int l = ba[k] & 0xff;
            i += l << j;
            j -= 8;
        }
        return i;
    }
    
    /** <p>
     * A utility method to produce a correctly formatted hex string string from an int
     * value and and an int specifying the length the hex string that represents the
     * int value should be.
     * </p>
     * <p>
     * Utilises both the padHex and toHexString methods.
     * </p>
     * @param i The int value that is to be transformed to a hex string.
     * @param j An int specifying the length of the hex string to be returned.
     * @return A string that contains the formatted hex string.
     */    
    private String hexFormat(int i, int j)
    {
        String s = Integer.toHexString(i);
        return padHex(s, j) + s;
    }

    /** <p>
     * A utility method that takes in a string of hex characters and prepends a number
     * characters to the string to make up a string of the required length as defined
     * in the int value passed into the method. This is because the values for say the
     * hashcode on a lower memory machine will only be 4 characters long and so to
     * the correct formatting is produced 0 characters must be prepended to the fornt
     * of the string.
     * <p>
     * @param s The String containing the hex values.
     * @param i The int specifying the length that the string should be.
     * @return A String of the correct length containing the original hex value and a
     * number of pad zeros at the front of the string.
     */    
    private String padHex(String s, int i)
    {
        StringBuffer stringbuffer = new StringBuffer();
        if(s.length() < i)
        {
            for(int j = 0; j < i - s.length(); j++)
                stringbuffer.append("0");

        }
        return stringbuffer.toString();
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
       try {
        // test long and bytes conversion
        long before = 123456789;
        long after = UUID.byteArrayToLong(UUID.longToByteArray(before),0,LENGTH_IN_BYTES/2);
        System.out.println("befone: " + before + ", after: " + after);
        before = -987654321;
        after = UUID.byteArrayToLong(UUID.longToByteArray(before),0,LENGTH_IN_BYTES/2);
        System.out.println("befone: " + before + ", after: " + after);
        
        // test various constructor/factory and string representation methods
        UUID uuid1 = UUID.randomUUID();
        UUID uuid2 = UUID.fromByteArray(uuid1.toByteArray());
        if (uuid1.equals(uuid2))
            System.out.println("uuid1 equals uuid2");
        System.out.println("uuid1 hash= " + uuid1.hashCode() + ", uuid2 hash: " + uuid2.hashCode());
        UUID uuid3 = new UUID(
                uuid1.getMostSignificantBits(), uuid1.getLeastSignificantBits());
        if (uuid1.equals(uuid3))
            System.out.println("uuid1 equals uuid3");
        System.out.println("uuid1 hash= " + uuid1.hashCode() + ", uuid3 hash: " + uuid3.hashCode());
        UUID uuid4 = UUID.randomUUID();
        if (uuid1.equals(uuid4))
            System.out.println("uuid1 equals uuid4");
        System.out.println("uuid1 hash= " + uuid1.hashCode() + ", uuid4 hash: " + uuid4.hashCode());
        UUID uuid5 = UUID.fromBase64(uuid1.toBase64());
        if (uuid1.equals(uuid5))
            System.out.println("uuid1 equals uuid5");
        System.out.println("uuid1 hash= " + uuid1.hashCode() + ", uuid5 hash: " + uuid5.hashCode());
        System.out.println("uuid1 hex: " + uuid1.toString());
        
        UUID uuid6 = UUID.fromString(uuid1.toString());
        if (uuid1.equals(uuid6))
            System.out.println("uuid1 equals uuid6");
        System.out.println("uuid1 hash= " + uuid1.hashCode() + ", uuid6 hash: " + uuid6.hashCode());
 
       } catch (IOException e) {
           e.printStackTrace();
       }
    }
}

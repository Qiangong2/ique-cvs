package com.broadon.util;

/**
 * Simple extension of CacheLFU, where the expiration of each cache entry can be
 * individually adjusted.
 * <p>
 * Concrete extension classes must implement the BackingStore interface, where the object
 * returned by the <code>retrieve</code> method must be a <code>com.broadon.util.Pair</code>
 * object, with the <code>first</code> field being the new expiration time, and the <code>second</code>
 * field being the object that is to be cached.
 * 
 * @see com.broadon.util.CacheLFU
 * @see com.broadon.util.Pair
 */
public class CacheExp extends CacheLFU
{
    class CacheObjWithExplicitExpiration extends CacheObjectWithRefCount
    {
        // reset the expiration date if the explicitly given one is earlier
        protected CacheObjWithExplicitExpiration(Object value, long expiration) {
            super(((Pair) value).second, expiration);

            Long exp = (Long) ((Pair) value).first;
            if (exp != null && expirationTime > exp.longValue())
                expirationTime = exp.longValue();
        }
    }

    public CacheExp(int cacheSize, long expiration) {
        super(cacheSize, expiration);
    }

    public CacheExp(long expiration) {
        super(expiration);
    }

    public CacheExp() {
        super();
    }

    protected CacheObject newCacheObject(Object value, long expiration)
    {
        return new CacheObjWithExplicitExpiration(value, expiration);
    }   
}

package com.broadon.util;

import java.io.Writer;
import java.io.StringWriter;
import java.io.IOException;
import java.io.InputStream;

/**
 * Convert a byte array to hex representation
 */
public class HexString
{
    private static final char[] HEX_TO_CHAR = {
	'0',  '1',  '2',  '3',  '4',  '5',  '6',  '7',
	'8',  '9',  'A',  'B',  'C',  'D',  'E',  'F'
    };


    /**
     * Write the given byte array as hex string.
     * @param out <code>Writer</code> class for output.
     * @param data Data to be written.
     * @param offset Offset from which to start writing bytes.
     * @param len Number of bytes to write.
     * @exception IOException If an I/O error occurs.
     */
    public static void write(Writer out, byte[] data, int offset, int length)
	throws IOException
    {
	char[] buffer = new char[2];
	int last = offset + length;
	
	for (int i = offset; i < last; ++i) {
	    byte b = data[i];
	    buffer[0] = HEX_TO_CHAR[(b >> 4) & 0xf];
	    buffer[1] = HEX_TO_CHAR[b & 0xf];
	    out.write(buffer);
	}
    }


    /**
     * Write the given byte array as hex string.
     * @param out <code>Writer</code> class for output.
     * @param data Data to be written.
     * @exception IOException If an I/O error occurs.
     */
    public static void write(Writer out, byte[] data)
	throws IOException
    {
	write(out, data, 0, data.length);
    }


    /**
     * Copy the input to the output as hex string.
     * @param out <code>Writer</code> class for output.
     * @param in <code>InputStream</code> from which to read data.
     * @exception IOException If an I/O error occurs.
     */
    public static void write(Writer out, InputStream in)
	throws IOException
    {
	byte buffer[] = new byte[4096];
	int n;
	while ((n = in.read(buffer)) > 0) {
	    write(out, buffer, 0, n);
	}
    }


    /**
     * Convert the given byte array to an hex string.
     * @param data Data to be written.
     * @param offset Offset from which to start converting bytes.
     * @param len Number of bytes to convert.
     * @return Hex string representation of the byte array.
     */
    public static String toHexString(byte[] data, int offset, int length)
    {
	StringWriter out = new StringWriter(length * 2);
	try {
	    write(out, data, offset, length);
	} catch (IOException e) {
	    // should never happen
	}
	return out.toString();
    }

    /**
     * Convert the given byte array to an hex string.
     * @param data Data to be written.
     * @return Hex string representation of the byte array. 
     */
    public static String toHexString(byte[] data)
    {
	return toHexString(data, 0, data.length);
    }


    /**
     * Convert the given input stream to an hex string.
     * @param in <code>InputStream</code> from which to read data.
     * @return Hex string representation of the byte array.
     * @exception IOException If an I/O error occurs.
     */
    public static String toHexString(InputStream in)
	throws IOException
    {
	StringWriter out = new StringWriter();
	write(out, in);
	return out.toString();
    }

    public static int toByte(char c) {
	if (c >= '0' && c <= '9')
	    return c - '0';
	else if (c >= 'A' && c <= 'F')
	    return c - 'A' + 10;
	else if (c >= 'a' && c <= 'f')
	    return c - 'a' + 10;
	else
	    return -1;
    }


    public static byte[] fromHexString(String s)
    {
	byte[] data = new byte[s.length() / 2];
	for (int i = 0; i < data.length; ++i) {
	    int n = (toByte(s.charAt(i*2)) << 4) | toByte(s.charAt(i*2+1));
	    if (n > 256 || n < 0)
		throw new NumberFormatException("Not a hex string");
	    else
		data[i] = (byte) n;
	}
	return data;
    }
}

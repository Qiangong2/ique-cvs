package com.broadon.util;

import java.io.IOException;
import java.io.EOFException;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;

/**
 * The <code>Base64</code> class encodes a binary array to a String and
 * decodes a String back to a binary array.
 *
 * @version	$Revision: 1.2 $
 */
public class Base64
    extends CharCoder
{
    private static final byte	EQUAL = '=';

    private static final char[]	enc =
	{
	    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
	    'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
	    'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
	    'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
	    'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
	    'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
	    'w', 'x', 'y', 'z', '0', '1', '2', '3',
	    '4', '5', '6', '7', '8', '9', '+', '/'
	};

    private static final byte[]	dec = new byte[256];

    static
    {
	int	incSize = enc.length;
	int	decSize = dec.length;
	int	n;

	for (n = 0; n < decSize; n++)
	{
	    dec[n] = -1;
	}
	for (n = 0; n < incSize; n++)
	{
	    dec[enc[n]] = (byte)n;
	}
	dec[EQUAL] = 0;
    }

    /**
     * Returns the encoding block size for Base 64 type.
     *
     * @return	int
     */
    protected int encodeBlockSize()
    {
	return 3;
    }

    /**
     * Returns the encoding line size for Base 64 type.
     *
     * @return	int
     */
    protected int encodeLineSize()
    {
	return 54;
    }

    /**
     * Encodes the given binary array to ascii. There will be a 35% overhead
     * as it turns 3 octets into 4 characters by taking every 6 bits and by
     * converting them to one of these 64 characters: [A-Z], [a-z], [0-9],
     * '+', and '/'.
     *
     * @param	out			the destination
     * @param	block			the block of data to be encoded
     * @param	offset			the offset from block
     * @param	size			the size to be encoded
     *
     * @throws	IOException
     */
    public void encodeBlock(Writer out, byte[] block, int offset, int size)
	throws IOException
    {
	byte		ch1;
	byte		ch2;
	byte		ch3;

	switch (size)
	{
	case 1:
	    ch1 = block[offset];
	    out.write(enc[(ch1 >> 2) & 0x3f]);
	    out.write(enc[(ch1 << 4) & 0x30]);
	    out.write(EQUAL);
	    out.write(EQUAL);
	    break;

	case 2:
	    ch1 = block[offset];
	    ch2 = block[offset + 1];
	    out.write(enc[(ch1 >>> 2) & 0x3f]);
	    out.write(enc[((ch1 << 4) & 0x30) | ((ch2 >>> 4) & 0xf)]);
	    out.write(enc[(ch2 << 2) & 0x3c]);
	    out.write(EQUAL);
	    break;

	case 3:
	    ch1 = block[offset];
	    ch2 = block[offset + 1];
	    ch3 = block[offset + 2];
	    out.write(enc[(ch1 >> 2) & 0x3f]);
	    out.write(enc[((ch1 << 4) & 0x30) | ((ch2 >>> 4) & 0xf)]);
	    out.write(enc[((ch2 << 2) & 0x3c) | ((ch3 >>> 6) & 0x3)]);
	    out.write(enc[ch3 & 0x3f]);
	    break;
	}
    }

    /**
     * Returns the decoding block size for Base 64 type.
     *
     * @return	int
     */
    protected int decodeBlockSize()
    {
	return 4;
    }

    /**
     * Returns the decoding line size for Base 64 type.
     *
     * @return	int
     */
    protected int decodeLineSize()
    {
	return 72;
    }

    /**
     * Decodes the given ascii back to the binary array. The is the reverse
     * of what encode() does.
     *
     * @param	in			the source
     * @param	out			the destination
     * @param	size			the size of this block
     *
     * @throws	IOException
     */
    public void decodeBlock(Reader in, OutputStream out, int size)
	throws IOException
    {
	int	n;
	int	ch;
	byte	ch1;
	byte	ch2;
	byte	ch3;
	byte	ch4;

	if (size < 2)
	{
	    throw new EOFException("Base64.decodeBlock: block size too small");
	}
	/*
	 * Skip end of line characters.
	 */
	do
	{
	    ch = in.read();
	    if (ch == -1)
	    {
		throw new EOFException();
	    }
	} while (ch == '\n' || ch == '\r');
	/*
	 * Fill in the buffer for decoding.
	 */
	byte[]	buffer = new byte[decodeBlockSize()];

	buffer[0] = (byte)ch;
	n = readBlock(in, buffer, 1, size - 1);
	if (n == -1)
	{
	    throw new EOFException();
	}
	/*
	 * Skip '=' characters.
	 */
	if (size > 3 && buffer[3] == EQUAL)
	{
	    size = 3;
	}
	if (size > 2 && buffer[2] == EQUAL)
	{
	    size = 2;
	}
	/*
	 * Decode.
	 */
	switch (size)
	{
	case 4:
	    ch1 = dec[buffer[0] & 0xff];
	    ch2 = dec[buffer[1] & 0xff];
	    ch3 = dec[buffer[2] & 0xff];
	    ch4 = dec[buffer[3] & 0xff];
	    out.write((byte)(((ch1 << 2) & 0xfc) | ((ch2 >>> 4) & 0x3)));
	    out.write((byte)(((ch2 << 4) & 0xf0) | ((ch3 >>> 2) & 0xf)));
	    out.write((byte)(((ch3 << 6) & 0xc0) | (ch4 & 0x3f)));
	    break;

	case 3:
	    ch1 = dec[buffer[0] & 0xff];
	    ch2 = dec[buffer[1] & 0xff];
	    ch3 = dec[buffer[2] & 0xff];
	    out.write((byte)(((ch1 << 2) & 0xfc) | ((ch2 >>> 4) & 0x3)));
	    out.write((byte)(((ch2 << 4) & 0xf0) | ((ch3 >>> 2) & 0xf)));
	    break;

	case 2:
	    ch1 = dec[buffer[0] & 0xff];
	    ch2 = dec[buffer[1] & 0xff];
	    out.write((byte)(((ch1 << 2) & 0xfc) | ((ch2 >>> 4) & 0x3)));
	    break;
	}
    }
}

package com.broadon.util;

/**
 * The <code>Timer</code> class provides timer functions.
 *
 * @version	$Revision: 1.1 $
 */
public class Timer
{
    private long	totalTime;
    private long	startTime;
    private long	elapseTime;

    /**
     * Empty constructor.
     */
    public
    Timer()
    {
	reset();
    }

    /**
     * Starts the timer.
     */
    public
    void start()
    {
	startTime = System.currentTimeMillis();
    }

    /**
     * Stops and records the timer.
     */
    public
    void stop()
    {
	elapseTime = System.currentTimeMillis() - startTime;
	totalTime += elapseTime;
    }

    /**
     * Resets the timer.
     */
    public
    void reset()
    {
	totalTime = 0;
	startTime = 0;
	elapseTime = 0;
    }

    /**
     * Gets the time spent between the last start and stop, in milliseconds.
     *
     * @return	the time spent between the last start and stop, in milliseconds
     */
    public
    long getTime()
    {
	return elapseTime;
    }

    /**
     * Gets the total time spent, in milliseconds.
     *
     * @return	the total time spent, in milliseconds
     */
    public
    long getTotalTime()
    {
	return totalTime;
    }

    /**
     * Returns the time and total time in String format.
     */
    public
    String toString()
    {
	return "[" + elapseTime + "ms, " + totalTime + "ms]";
    }
}

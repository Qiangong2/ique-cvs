package com.broadon.util;

import java.util.Iterator;
import java.util.Map;

/**
 * A simple implementation of com.broadon.util.Cache with LFU (Least Frequently
 * Used) replacement algorithm.
 * 
 * @see com.broadon.util.Cache
 */
public class CacheLFU extends Cache
{
    
    /**
     * Extends the CacheObject with a reference count.
     */
    public class CacheObjectWithRefCount extends CacheObject 
    {
        int refCount;

        protected CacheObjectWithRefCount(Object value, long expiration) {
            super(value, expiration);
            refCount = 0;
        }
        
        protected Object get() {
            ++refCount;
            return super.get();
        }
    }
    
    
    public CacheLFU(int cacheSize, long expiration) {
        super(cacheSize, expiration);
    }

    public CacheLFU(long expiration) {
        super(expiration);
    }

    public CacheLFU() {
        super();
    }

    /**
     * Remove all cache entries with reference count less than the average.
     * Reset the ref. count of the remaining entries to avoid hot but "least
     * recently used" entries to stay forever.
     * 
     * @see com.broadon.util.Cache#flushCache()
     */
    protected void flushCache()
    {
        int avg = refCount / cache.size() + 1;
        for (Iterator iter = cache.entrySet().iterator(); iter.hasNext();) {
            CacheObjectWithRefCount cacheEntry = (CacheObjectWithRefCount) 
                ((Map.Entry) iter.next()).getValue();
            if (cacheEntry.refCount < avg)
                iter.remove();
            else
                cacheEntry.refCount = 0;
        }
        refCount = 0;
    }

    protected CacheObject newCacheObject(Object value, long expiration)
    {
       return new CacheObjectWithRefCount(value, expiration);
    }
}

package com.broadon.util;

import java.lang.Comparable;
import java.util.Comparator;
import java.util.ArrayList;

/**
 * The <code>PriorityQueue</code> class provides an unbounded priority
 * queue on top of an ArrayList.  The objects inserted must either
 * implement the Comparable interface, where the priority queue will
 * be ordered with the largest value first, or using a Comparator object
 * for customized ordering potentially different from any Comparable
 * interface.  When using a Comparator object the objects inserted need
 * not implement the Comparable interface.  The ordering imposed by a
 * Comparable interface or a Comparator object must be a strict weak 
 * ordering (see http://www.sgi.com/tech/stl/StrictWeakOrdering.html)
 * and can contain elements that are equal.
 *
 * This class is not threadsafe out of performance concerns.  For a
 * threadsafe version, use a wrapper class that can do size/top/pop as
 * one atomic operation that returns null for an empty queue, possibly
 * using a blocking scheme like that used in Queue (which should be
 * renamed BoundedBuffer).
 *
 * Note that we limit the size to 32 bits, and none of the pushed 
 * elements may be null.
 *
 * For methods not described, the description matches that of a Container.
 *
 */
public class PriorityQueue 
{

    private static final class DefaultComparator implements Comparator
    {
        public int compare(Object o1, Object o2)
        {
            return ((Comparable)o1).compareTo(o2);
        }
    }

    
    private static final int FIRST_IDX = 1; // We use q[0] as a sentinel
 
    private ArrayList  q;
    private Comparator comp;


    private PriorityQueue(ArrayList q, Comparator compare) 
    {
        this.q = q;
        comp = compare;
    }

    /** Constructs an empty list with an initial capacity ten.
     */
    public PriorityQueue() 
    {
        this(10, new DefaultComparator());
    }

    /** Constructs an empty list with the given initial capacity
     */
    public PriorityQueue(int initialCapacity) 
    {
        this(initialCapacity, new DefaultComparator());
    }

    /** Constructs an empty list with an initial capacity ten, and
     *  where element x occurs before y when compare(x, y) > 0
     */
    public PriorityQueue(Comparator compare) 
    {
        this(10, compare);
    }

    /** Constructs an empty list with the given initial capacity, and
     *  where element x occurs before y when compare(x, y) > 0
     */
    public PriorityQueue(int initialCapacity, Comparator compare) 
    {
        q = new ArrayList(initialCapacity+FIRST_IDX);
        q.add(null); // sentinel value  
        comp = compare;
    }

    public Object clone()
    {
        return new PriorityQueue((ArrayList)q.clone(), comp);
    }
    public void    clear()   {q.clear(); q.add(null);}
    public int     size()    {return q.size()-FIRST_IDX;}
    public boolean isEmpty() {return q.size() == FIRST_IDX;}


    public void push(Object o)
    {
        // Add the new value to the back of the queue
        //
        q.add(o);

        // Float the new value up the heap with O(log n) compares.
        //
        int    i = size();
        int    parentIdx = i/2;
        Object parent = q.get(parentIdx);
        while (parent != null && comp.compare(parent, o) < 0) {
            q.set(i, parent); // Move parent down
            i = parentIdx;
            parentIdx = i/2;
            parent = q.get(parentIdx);
        }
        q.set(i, o);
    }

    /** 
     * Has no effect if the queue is empty
     */
    public void pop()
    {
        final int sz = size();
        if (sz == 1) {
            q.remove(FIRST_IDX);
        }
        else if (sz > 1) {
            //
            // Move the last element to the front of the queue
            //
            final Object o = q.remove(sz);
            q.set(FIRST_IDX, o);

            // Push the new first element down the heap to its rightful
            // position with O(log n) compares.
            //
            int i = FIRST_IDX;
            int larger = i;
            do {
                if (larger != i) {
                    q.set(i, q.get(larger)); // Float the larger value up
                    q.set(larger, o);        // Move this value down
                    i = larger;
                }

                final int left = i*2;
                if (left < sz && comp.compare(q.get(left), o) > 0)
                    larger = left;
                else
                    larger = i;

                final int right = i*2 + 1;
                if (right < sz && comp.compare(q.get(right), 
                                               q.get(larger)) > 0)
                    larger = right;

            } while (larger != i);
        }
    }


    /** 
     * @throws IndexOutOfBoundsException if the queue is empty
     */
    public Object top()
    {
        return q.get(FIRST_IDX);
    }
    
} // class PriorityQueue

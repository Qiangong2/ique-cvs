package com.broadon.util;

import java.util.*;

/** A class for tracing time throughout a java program and in the end
 *  printing out a log.  There are two modes of time-tracing, which can
 *  be used in unison through this interface:
 *
 *      endInterval(name): Will set the time spent in the named interval
 *                         to the difference between the current time and
 *                         the last call to the constructor, pushInterval
 *                         or endInterval.
 *      pushInterval(name): Same as endInterval, but the next interval
 *                         will be a nested interval to be ended with the
 *                         next call to endInterval or pushInterval.
 *
 *      incrTime(name, tm): Will add the given time to the named 
 *                         interval, where the interval is assumed to
 *                         be zero if it does not already exist.
 *      pushTime(name): Creates a nested time-trace to be associated with
 *                      the given name in this trace.  If the name does not
 *                      exist, it will be created.
 *
 *      toString(): will log all intervals to a string buffer and return the
 *             resultant string.  This will also log the time between the
 *             construction of the TimeTrace and this call.
 *
 *      clear(): clears the trace of all intervals
 *
 *  Note that endInterval/pushInterval allows for consistent times
 *  with nested time-lines, while incrTime/pushTime allows time freely to
 *  be added to named slots and there will be no guarantees of consistency.
 *
 *  While the two modes can be freely mixed, we do not recommend doing so
 *  as it may become difficult to make sense of the resulting log.
 *
 *  The log can be printed in two modes: 
 *
 *      ADDITIVE: nested times are added to parent times.
 *      INCLUSIVE: nested times are assumed to be parts of parent times.
 *
 *  Nested traces inherit the log mode of the parent.
 *
 */
public class TimeTrace
{
    static private class Value {
        public long      tm;
        public TimeTrace nested;
        public Value(long tm) {this.tm = tm; nested = null;}
        public Value(long tm, TimeTrace tr) {this.tm = tm; nested = tr;}
    }

    private static class LogType {};

    private Vector    traceOrder;
    private Map       traceMap;
    private long      lastTime;
    private TimeTrace nested;
    private LogType   lt;

    private long append(StringBuffer b) 
    {
        long total = 0;
        for (int i = 0; i < traceOrder.size(); ++i) {
            final String nm = (String)traceOrder.get(i);
            final Value v = (Value)traceMap.get(nm);
            if (i > 0) b.append(' ');
            b.append(nm);
            if (v.nested != null) {
                b.append("=[");
                long tm = v.nested.append(b);
                b.append(" total=");
                if (lt == INCLUSIVE)
                    tm = (tm > v.tm? tm : v.tm); // Choose larger time
                else // ADDITIVE
                    tm += v.tm;                  // Add nestd time
                b.append(Long.toString(tm));
                total += tm;
                b.append(']');
            }
            else {
                b.append('=');
                b.append(Long.toString(v.tm));
                total += v.tm;
            }
        }
        return total;
    } // append


    public static final LogType ADDITIVE = new LogType();
    public static final LogType INCLUSIVE = new LogType();

    public TimeTrace(LogType l)
    {
        lastTime = System.currentTimeMillis();
        traceMap = new HashMap(17);
        traceOrder = new Vector(17);
        nested = null;
        lt = l;
    }
    public TimeTrace(long t, LogType l)
    {
        lastTime = t;
        traceMap = new HashMap(17);
        traceOrder = new Vector(17);
        nested = null;
        lt = l;
    }

    /** Will set the time spent in the named interval to the difference
     *  between the current time and the last call to the constructor,
     *  pushInterval or endInterval.
     */
    public void endInterval(String nm)
    {
        final long tm = System.currentTimeMillis();
        while (traceMap.containsKey(nm)) {
            nm += "1"; // creates a unique name
        }
        traceMap.put(nm, new Value(tm - lastTime, nested));
        traceOrder.add(nm);
        lastTime = tm;
        nested = null;
    }

    /** Same as endInterval, but the next interval will be a nested interval
     *  to be ended with the next call to endInterval or pushInterval.
     */
    public TimeTrace pushInterval(String nm)
    {
        final long tm = System.currentTimeMillis();
        while (traceMap.containsKey(nm)) {
            nm += "1"; // creates a unique name
        }
        traceMap.put(nm, new Value(tm - lastTime, nested));
        traceOrder.add(nm);
        lastTime = tm;
        nested = new TimeTrace(tm, lt);
        return nested;
    }

    /** Will add the given time (t) to the named interval, where the 
     * interval is assumed to be zero if it does not already exist.
     */
    public void incrTime(String nm, long t) {
        final Value old = (Value)traceMap.get(nm);
        if (old == null) {
            traceMap.put(nm, new Value(t));
            traceOrder.add(nm);
        }
        else {
            old.tm += t;
        }
    }

    /** Creates a nested time-trace to be associated with the given name
     *  in this trace.  If the name does not exist, it will be created.
     */
    public TimeTrace pushTime(String nm) {
        final long      tm = System.currentTimeMillis();
        final Value     old = (Value)traceMap.get(nm);
        final TimeTrace tr = new TimeTrace(tm, lt);
        if (old == null) {
            traceMap.put(nm, new Value(0, tr));
            traceOrder.add(nm);
        }
        else {
            old.nested = tr;
        }
        return tr;
    }

    public String toString() 
    {
        StringBuffer b = new StringBuffer(1024);
        b.append("TimeTrace(msecs)=[");
        long total = append(b);
        b.append(" total=");
        b.append(Long.toString(total));
        b.append(']');
        return b.toString();
    }

    public void clear()
    {
        for (int i = 0; i < traceOrder.size(); ++i) {
            final String nm = (String)traceOrder.get(i);
            final Value v = (Value)traceMap.get(nm);
            if (v.nested != null) v.nested.clear();
        }
        traceMap.clear();
        traceOrder.clear();
    }
} // class TimeTrace

package com.broadon.util;

import java.math.BigInteger;

/**
 * Format an eCard ID.
 */
public class ECardNumber
{
    static final int typeSize = 6;
    static final int serialSize = 10;
    static final int randomSize = 10;
    static final int md5Size = 32;

    public static void merge(int offset, int size, String s,
			      StringBuffer buf) {
	int len = s.length();
	offset += size - len;
	buf.replace(offset, offset + len, s);
    }

    
    /**
     * Generate an eCard ID.
     * @param type The eCard type.
     * @param serial eCard serial number.
     * @param random The random number part of the eCard ID.
     * @return Encoded 26 digit eCard ID.
     */
    public static String encode(int type, long serial, long random) {
	StringBuffer buf =
	    new StringBuffer("00000000000000000000000000");

	merge(0, typeSize, String.valueOf(type), buf);
	merge(typeSize, serialSize, String.valueOf(serial), buf);
	merge(typeSize + serialSize, randomSize, String.valueOf(random), buf);
	return buf.toString();
    }

    public static String extractType(String eCardID) {
	return eCardID.substring(0, typeSize);
    }

    public static String extractSerial(String eCardID) {
	return eCardID.substring(typeSize, serialSize + typeSize);
    }

    public static String extractRandom(String eCardID) {
	return eCardID.substring(typeSize + serialSize,
				 typeSize + serialSize + randomSize);
    }


    public static byte[] digest(int type, long serial, BigInteger hash)
    {
	StringBuffer buf =
	    new StringBuffer("000000000000000000000000000000000000000000000000");
	merge(0, typeSize, String.valueOf(type), buf);
	merge(typeSize, serialSize, String.valueOf(serial), buf);
	merge(typeSize + serialSize, md5Size, hash.toString(16), buf);
	return buf.toString().getBytes();
    }
}

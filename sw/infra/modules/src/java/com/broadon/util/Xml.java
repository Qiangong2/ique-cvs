package com.broadon.util;

import java.io.ByteArrayOutputStream;

/**
 *  Simple class for escaping XML meta characters
 */ 
public class Xml
{
    static final String lt = "&lt;";
    static final String gt = "&gt;";
    static final String amp = "&amp;";

    static private String escape(String s, int start, int end)
    {
	StringBuffer sbuf = new StringBuffer(s.substring(0, start));
	for (int i = start; i < end; ++i) {
	    char c = s.charAt(i);
	    switch (c) {
	    case '<':
		sbuf.append(lt);
		break;

	    case '>':
		sbuf.append(gt);
		break;
		
	    case '&':
		sbuf.append(amp);
		break;

	    default:
		sbuf.append(c);
		break;
	    }
	}
	return sbuf.toString();
    }


    /**
     * Encode a string by escaping XML meta-characters (i.e., '<' and '&');
     * @param s XML text string
     * @return Encoded string.
     */
    static public String encode(String s)
    {
	int i = 0;
	int len = s.length();
	while (i < len) {
	    switch (s.charAt(i)) {
	    case '<':
	    case '&':
	    case '>':
		return escape(s, i, len);

	    default:
		break;
	    }
	    ++i;
	}
	return s;
    } // encode


    /*
     * Below is the equivalent for byte arrays
     */

    static final byte[] b_lt = { '&', 'l', 't', ';' };
    static final byte[] b_gt = { '&', 'g', 't', ';' };
    static final byte[] b_amp = { '&', 'a', 'm', 'p', ';' };

    static private byte[] escape(byte[] b, int start)
    {
	ByteArrayOutputStream out = new ByteArrayOutputStream();
	out.write(b, 0, start);
	for (int i = start; i < b.length; ++i) {
	    switch (b[i]) {
	    case '<':
		out.write(b_lt, 0, b_lt.length);
		break;

	    case '>':
		out.write(b_gt, 0, b_gt.length);
		break;

	    case '&':
		out.write(b_amp, 0, b_amp.length);
		break;

	    default:
		out.write(b[i]);
		break;
	    }
	}
	return out.toByteArray();
    } // escape


    static public byte[] encode(byte[] b)
    {
	int i = 0;
	while (i < b.length) {
	    switch (b[i]) {
	    case '<':
	    case '&':
	    case '>':
		return escape(b, i);

	    default:
		break;
	    }
	    ++i;
	}
	return b;
    }
}

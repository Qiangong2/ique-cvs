package com.broadon.sql;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.PreparedStatement;
import java.util.Map;

/**
 * The <c>PredicateTerm</c> class implements the predicate term part.
 *
 * This implementation has the following grammar in BNF form:
 *<ul>
 *<li>Predicate ::= PredicateItem | PredicateTerm | "NOT" PredicateTerm
 *<li>PredicateTerm ::= Predicate "AND" Predicate | Predicate "OR" Predicate
 *<li>PredicateItem ::= Name Operator Value
 *<li>Operator ::= "=" | "<>" | "<" | "<=" | ">" | ">="
 *</ul>
 * The intention of this implementation is the make it simple but common
 * enough to cover a majority of the cases.
 *
 * @version	$Revision: 1.4 $
 */
public class PredicateTerm
    implements Predicate
{
    private Predicate		child;		// next level down
    private boolean		not;		// true => NOT

    /**
     * Constructs an empty PredicateTerm instance.
     */
    public PredicateTerm()
    {
	this(null);
    }

    /**
     * Constructs an empty PredicateTerm instance.
     *
     * @param	child			the child Predicate
     */
    public PredicateTerm(Predicate child)
    {
	this.child = child;
	this.not = false;
    }

    /**
     * Adds a Predicate instance to this Predicate that has the AND
     * relationship with it. This operation is not supported by the
     * PredicateTerm.
     *
     * @param	predicate		the Predicate instance that has the AND
     *					relationship with this Predicate
     */
    public Predicate and(Predicate predicate)
    {
	throw new UnsupportedOperationException();
    }

    /**
     * Adds a Predicate instance to this Predicate that has the OR
     * relationship with it. This operation is not supported by the
     * PredicateTerm.
     *
     * @param	predicate		the Predicate instance that has the OR
     *					relationship with this Predicate
     */
    public Predicate or(Predicate predicate)
    {
	throw new UnsupportedOperationException();
    }

    /**
     * Negates this PredicateTerm.
     *
     * @return	The PredicateTerm itself, so that this method can be part of
     *		an expression.
     */
    public Predicate not()
    {
	not = true;
	return this;
    }

    /**
     * Binds this Predicate instance to the given PreparedStatement.
     *
     * @param	statement		the PreparedStatement
     *
     * @throws	IOException
     */
    public void bind(PreparedStatement statement)
	throws IOException
    {
	bind(statement, 0);
    }

    /**
     * Binds this Predicate instance to the given PreparedStatement.
     *
     * @param	statement		the PreparedStatement
     * @param	index			the attribute index
     *
     * @throws	IOException
     */
    public void bind(PreparedStatement statement, int index)
	throws IOException
    {
	if (child != null)
	{
	    child.bind(statement, index);
	}
    }

    /**
     * Generates the where clause component of this PredicateTerm.
     *
     * @param	nameMap			the name mapping table
     *
     * @return	The where clause component.
     *
     * @throws	IOException
     */
    public String generateWherePart(Map nameMap)
	throws IOException
    {
	if (child == null)
	{
	    return "";
	}

	StringWriter	out = new StringWriter();

	try
	{
	    generateWherePart(out, nameMap);
	    out.flush();
	    return out.toString();
	}
	finally
	{
	    out.close();
	}
    }

    /**
     * Generates the where clause component of this PredicateItem.
     *
     * @param	out			the output destination
     * @param	nameMap			the name mapping table
     *
     * @throws	IOException
     */
    public void generateWherePart(Writer out, Map nameMap)
	throws IOException
    {
	if (child != null)
	{
	    if (not)
	    {
		out.write(' ');
		out.write(NOT);
	    }
	    out.write(" (");
	    child.generateWherePart(out, nameMap);
	    out.write(" )");
	}
    }
}

package com.broadon.sql;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.PreparedStatement;
import java.util.Map;

import com.broadon.bean.Bean;
import com.broadon.bean.BeanOutputProcessor;
import com.broadon.bean.TextFormatWriter;

/**
 * The <c>PredicateItem</c> class implements the predicate item part.
 *
 * @see	com.broadon.sql.PredicateTerm
 *
 * @version	$Revision: 1.4 $
 */
public class PredicateItem
    extends Bean
    implements Predicate
{
    private Predicate		next;		// the sibling(s)
    private String		reOp;		// relational operator
    private String		arOp;		// arithmetic operator

    /**
     * Constructs a PredicateItem instance.
     */
    public PredicateItem()
    {
	this("=");
    }

    /**
     * Constructs a PredicateItem instance.
     *
     * @param	arOp			the arithmetic operator for this
     *					PredicateItem
     */
    public PredicateItem(String arOp)
    {
	this.next = null;
	this.reOp = null;
	this.arOp = arOp;
    }

    /**
     * Adds a Predicate instance to this PredicateItem that has the AND
     * relationship with it.
     *
     * @param	predicate		the Predicate instance that has the AND
     *					relationship with this PredicateItem
     *
     * @return	The given Predicate, so that this method can be part of an
     *		expression.
     */
    public Predicate and(Predicate predicate)
    {
	reOp = AND;
	next = predicate;
	return predicate;
    }

    /**
     * Adds a Predicate instance to this PredicateItem that has the OR
     * relationship with it.
     *
     * @param	predicate		the Predicate instance that has the OR
     *					relationship with this PredicateItem
     *
     * @return	The given Predicate, so that this method can be part of an
     *		expression.
     */
    public Predicate or(Predicate predicate)
    {
	reOp = OR;
	next = predicate;
	return predicate;
    }

    /**
     * Negates this Predicate. This operation is not supported by the
     * PredicateItem.
     */
    public Predicate not()
    {
	throw new UnsupportedOperationException();
    }

    /**
     * Binds this Predicate instance to the given PreparedStatement.
     *
     * @param	statement		the PreparedStatement
     *
     * @throws	IOException
     */
    public void bind(PreparedStatement statement)
	throws IOException
    {
	bind(statement, 0);
    }

    /**
     * Binds this Predicate instance to the given PreparedStatement.
     *
     * @param	statement		the PreparedStatement
     * @param	index			the attribute index
     *
     * @throws	IOException
     */
    public void bind(PreparedStatement statement, int index)
	throws IOException
    {
	new Binder(this, statement, index).process();
    }

    /**
     * Generates the where clause component of this PredicateItem.
     *
     * @param	nameMap			the name mapping table
     *
     * @return	The where clause component.
     *
     * @throws	IOException
     */
    public String generateWherePart(Map nameMap)
	throws IOException
    {
	StringWriter	out = new StringWriter();

	try
	{
	    generateWherePart(out, nameMap);
	    out.flush();
	    return out.toString();
	}
	finally
	{
	    out.close();
	}
    }

    /**
     * Generates the where clause component of this PredicateItem.
     *
     * @param	out			the output destination
     * @param	nameMap			the name mapping table
     *
     * @throws	IOException
     */
    public void generateWherePart(Writer out, Map nameMap)
	throws IOException
    {
	WherePart	wherePart = new WherePart(this, nameMap);

	wherePart.format(out, 0);
    }

    /**
     * The <c>Binder</c> class provides mechanism to bind a predicate
     * tree to a given PreparedStatement.
     */
    private class Binder
	extends BeanOutputProcessor
    {
	private PreparedStatement	statement;
	private int			index;

	/**
	 * Constructs a Binder instance.
	 *
	 * @param	predicate	the starting predicate item
	 * @param	statement	the PreparedStatement to be bound
	 */
	private Binder(Predicate predicate, PreparedStatement statement)
	{
	    this(predicate, statement, 0);
	}

	/**
	 * Constructs a Binder instance.
	 *
	 * @param	predicate	the starting predicate item (or term)
	 * @param	statement	the PreparedStatement to be bound
	 * @param	index		the attribute index
	 */
	private Binder(Predicate predicate,
		       PreparedStatement statement,
		       int index)
	{
	    super((Bean)predicate);
	    this.statement = statement;
	    this.index = index;
	}

	/**
	 * Binds the predicate item to the PreparedStatement.
	 *
	 * @param	name		the name of the predicate item
	 * @param	data		the value of the predicate item
	 * @param	dateType	the class type of the data
	 * @param	parameter	the parameter information
	 *
	 * @throws	IOException
	 */
	protected void processAttribute(String name,
					Object data,
					Class dataType,
					Object parameter)
	    throws IOException
	{
	    PBean.bindStatement(statement, ++index, data, dataType);
	    /*
	     * Process the next one, if any.
	     */
	    if (next != null)
	    {
		next.bind(statement, index);
	    }
	}
    }

    /**
     * The <c>WherePart</c> class provides the mechanism to generate the
     * where clause of a SQL statement (without the "where" keyword).
     */
    private class WherePart
	extends TextFormatWriter
    {
	private int	count;

	/**
	 * Constructs a WherePart instance.
	 *
	 * @param	bean		the JavaBeans object to be formatted
	 */
	private WherePart(Bean bean)
	{
	    this(bean, null);
	}

	/**
	 * Constructs a WherePart instance.
	 *
	 * @param	bean		the JavaBeans object to be formatted
	 * @param	nameMap		the name mapping table
	 */
	private WherePart(Bean bean, Map nameMap)
	{
	    super(bean);
	    setNameMap(nameMap);
	    this.count = 0;
	}

	/**
	 * Process the attribute, identified by the given name, with the given
	 * data, into Predicate style.
	 *
	 * @param	name		the name of the attribute
	 * @param	data		the value of the attribute
	 * @param	dateType	the class type of the data
	 * @param	parameter	the parameter information
	 *
	 * @throws	IOException
	 */
	protected final void processAttribute(String name,
					      Object data,
					      Class dataType,
					      Object parameter)
	    throws IOException
	{
	    TextParam	param = (TextParam)parameter;
	    Writer	out = param.out;

	    if (count++ > 0)
	    {
		/*
		 * This is not the first attribute, use the AND operator
		 * to separate them.
		 */
		out.write(' ');
		out.write(AND);
	    }
	    /*
	     * Show name and operator.
	     */
	    out.write(' ');
	    out.write(name);
	    out.write(' ');
	    out.write(arOp);
	    out.write(" ?");
	    /*
	     * Show the next one, if any.
	     */
	    if (next != null)
	    {
		out.write(' ');
		out.write(reOp);
		next.generateWherePart(out, getNameMap());
	    }
	}
    }
}

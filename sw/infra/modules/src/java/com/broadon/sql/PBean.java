package com.broadon.sql;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;

import com.broadon.bean.Bean;
import com.broadon.bean.BeanInputProcessor;
import com.broadon.bean.BeanOutputProcessor;
import com.broadon.bean.TextFormatWriter;
import com.broadon.util.Base64;

/**
 * The <c>PBean</c> class provides the mechanism to bind its instance
 * to a PreparedStatement, and to fill its instance from a ResultSet.
 *
 * @version	$Revision: 1.19 $
 */
public class PBean
    extends Bean
{
    private static final TimeZone	timeZone;
    private static final Base64	    blobEncoder;

    static
    {
        /*
         *  Find out the offset from GMT in milli-seconds.
         */
        timeZone = TimeZone.getDefault();
        blobEncoder = new Base64();
    }

    /**
     * Binds the data to the PreparedStatement at the given column name.
     *
     * @param	statement		the statement to be bound
     * @param	index			the column index
     * @param	data			the value to be assigned
     * @param	dateType		the class type of the data
     *
     * @throws	IOException
     */
    public static void bindStatement(PreparedStatement statement,
				     int index,
				     Object data,
				     Class dataType)
	throws IOException
    {
	Integer	type = (Integer)typeMap.get(dataType);

	try
	{
	    switch ((type == null) ? OTHER_TYPE : type.intValue())
	    {
	    case BOOL_TYPE:
		statement.setBoolean(index, ((Boolean)data).booleanValue());
		break;

	    case BYTE_TYPE:
		statement.setByte(index, ((Byte)data).byteValue());
		break;

	    case CHAR_TYPE:
		statement.setString(
				index,
				new String(
				    new char[]
					{
					    ((Character)data).charValue()
					}));
		break;

	    case DOUBLE_TYPE:
		statement.setDouble(index, ((Double)data).doubleValue());
		break;

	    case FLOAT_TYPE:
		statement.setFloat(index, ((Float)data).floatValue());
		break;

	    case INTEGER_TYPE:
		statement.setInt(index, ((Integer)data).intValue());
		break;

	    case LONG_TYPE:
		statement.setLong(index, ((Long)data).longValue());
		break;

	    case SHORT_TYPE:
		statement.setShort(index, ((Short)data).shortValue());
		break;

	    case DATE_TYPE:
		/*
		 *  Convert the time to GMT.
		 */
		long		time = ((Date)data).getTime();
		long		offset = timeZone.getOffset(time);
		Timestamp	timestamp = new Timestamp(time - offset);

		statement.setTimestamp(index, timestamp);
		break;

	    case BYTE_ARRAY_TYPE:
		statement.setBytes(index, (byte[])data);
		break;

	    case STRING_TYPE:
	    default:
		statement.setString(index, data.toString());
		break;
	    }
	}
	catch (SQLException se)
	{
	    se.printStackTrace();
	    throw new IOException("SQL Exception: " + se.getMessage());
	}
    }

    /**
     * Constructs a PBean instance.
     */
    public PBean()
    {
	super();
    }

    /**
     * Binds this PBean instance to the given PreparedStatement.
     *
     * @param	statement		the PreparedStatement
     *
     * @return	The last index used for this PreparedStatement.
     *
     * @throws	IOException
     */
    public int bind(PreparedStatement statement)
	throws IOException
    {
	return bind(statement, 0);
    }

    /**
     * Binds this PBean instance to the given PreparedStatement.
     *
     * @param	statement		the PreparedStatement
     * @param	index			the attribute index
     *
     * @return	The last index used for this PreparedStatement.
     *
     * @throws	IOException
     */
    public int bind(PreparedStatement statement, int index)
	throws IOException
    {
	Binder	binder = new Binder(this, statement, index);

	binder .process();
	return binder.getIndex();
    }

    /**
     * Fills this PBean with data from the given ResultSet.
     *
     * @param	resultSet		the ResultSet that contains the data
     * @param	nameMap			the name mapping table
     *
     * @throws	IOException
     */
    public void fill(ResultSet resultSet, Map nameMap)
	throws IOException
    {
	new Filler(this, resultSet, nameMap).process();
    }

    /**
     * Generates the set clause component of this PBean.
     *
     * @param	nameMap			the name mapping table
     *
     * @return	The set clause component.
     *
     * @throws	IOException
     */
    public String generateSetPart(Map nameMap)
	throws IOException
    {
	StringWriter	out = new StringWriter();

	try
	{
	    generateSetPart(out, nameMap);
	    return out.toString();
	}
	finally
	{
	    out.close();
	}
    }

    /**
     * Generates the set clause component of this PBean.
     *
     * @param	out			the output destination
     * @param	nameMap			the name mapping table
     *
     * @throws	IOException
     */
    public void generateSetPart(Writer out, Map nameMap)
	throws IOException
    {
	SetPart	setPart = new SetPart(this, nameMap);

	setPart.format(out, 0);
    }

    /**
     * Generates the values clause component of this PBean.
     *
     * @param	nameMap			the name mapping table
     *
     * @return	The values clause component.
     *
     * @throws	IOException
     */
    public String generateValuesPart(Map nameMap)
	throws IOException
    {
	StringWriter	out = new StringWriter();

	try
	{
	    generateValuesPart(out, nameMap);
	    return out.toString();
	}
	finally
	{
	    out.close();
	}
    }

    /**
     * Generates the values clause component of this PBean.
     *
     * @param	out			the output destination
     * @param	nameMap			the name mapping table
     *
     * @throws	IOException
     */
    public void generateValuesPart(Writer out, Map nameMap)
	throws IOException
    {
	ValuesPart	valuesPart = new ValuesPart(this, nameMap);

	out.write(" (");
	valuesPart.format(out, 0);
	out.write(" ) values (");
	valuesPart.resetCounter();
	valuesPart.setPart(false);
	valuesPart.format(out, 0);
	out.write(" )");
    }

    /**
     * The <c>Binder</c> class provides mechanism to traverse the PBean
     * and bind to a PreparedStatement.
     */
    private class Binder
	extends BeanOutputProcessor
    {
	private PreparedStatement	statement;
	private int			index;

	/**
	 * Constructs a Binder instance.
	 *
	 * @param	bean		the PBean instance
	 * @param	statement	the PreparedStatement to be bound
	 */
	private Binder(PBean bean, PreparedStatement statement)
	{
	    this(bean, statement, 0);
	}

	/**
	 * Constructs a Binder instance.
	 *
	 * @param	bean		the PBean instance
	 * @param	statement	the PreparedStatement to be bound
	 * @param	index		the attribute index
	 */
	private Binder(PBean bean, PreparedStatement statement, int index)
	{
	    super(bean);
	    setSkipNull(true);
	    this.statement = statement;
	    this.index = index;
	}

	/**
	 * Returns the last index used for the PreparedStatement.
	 *
	 * @return	The last index used for the PreparedStatement.
	 */
	private int getIndex()
	{
	    return index;
	}

	/**
	 * Binds the bean to the PreparedStatement.
	 *
	 * @param	name		the name of the PBean attribute
	 * @param	data		the value of the PBean attribute
	 * @param	dateType	the class type of the data
	 * @param	parameter	the parameter information
	 *
	 * @throws	IOException
	 */
	protected void processAttribute(String name,
					Object data,
					Class dataType,
					Object parameter)
	    throws IOException
	{
	    bindStatement(statement, ++index, data, dataType);
	}
    }

    /**
     * The <c>Filler</c> class provides the mechanism to fill the data
     * from a ResultSet.
     */
    private class Filler
	extends BeanInputProcessor
    {
	private ResultSet	resultSet;

	/**
	 * Constructs a Filler instance.
	 *
	 * @param	bean		the JavaBeans object to be formatted
	 * @param	resultSet	the ResultSet that contains the data
	 */
	private Filler(Bean bean, ResultSet resultSet)
	{
	    this(bean, resultSet, null);
	}

	/**
	 * Constructs a Filler instance.
	 *
	 * @param	bean		the JavaBeans object to be formatted
	 * @param	resultSet	the ResultSet that contains the data
	 * @param	nameMap		the name mapping table
	 */
	private Filler(Bean bean, ResultSet resultSet, Map nameMap)
	{
	    super(bean, nameMap);
	    this.resultSet = resultSet;
	}

	/**
	 * Fills the bean with data from the ResultSet.
	 *
	 * @param	name		the name of the PBean attribute
	 * @param	dateType	the class type of the data
	 *
	 * @throws	IOException
	 */
	protected Object processAttribute(String name, Class dataType)
	    throws IOException
	{
	    if ("class".equals(name))
	    {
		/*
		 * Skip.
		 */
		return null;
	    }

	    try
	    {
            // Handle timestamps specially, since getObject will use the 
            // default jdbc java mapping, which may not accurately reflect
            // the time as recorded in the DB (e.g. may round to midnight).
            //
            // We force the Date result to be interpreted as a 
            // java.sql.Timestamp.
            //
            if (dataType == java.util.Date.class) {
                Timestamp t = resultSet.getTimestamp(name);
                if (t != null) {
                    long time = t.getTime();
                    long offset = timeZone.getOffset(time);
                    return new Date(time + offset);
                }
            }

		Object	data = resultSet.getObject(name);

		if (data == null)
		{
		    return null;
		}
		if (dataType.isInstance(data))
		{
		    /*
		     * Types match, no need to convert.
		     * Examples: byte[], String.
		     */
            return data;
		}

        final Integer type = (Integer)Bean.typeMap.get(dataType);
        final int ti = (type == null) ? Bean.OTHER_TYPE : type.intValue();
		switch (ti)
		{
		case Bean.DOUBLE_TYPE:
		    return new Double(data.toString());

		case Bean.FLOAT_TYPE:
		    return new Float(data.toString());

		case Bean.INTEGER_TYPE:
		    return new Integer(data.toString());

		case Bean.LONG_TYPE:
		    return new Long(data.toString());

		case Bean.SHORT_TYPE:
		    return new Short(data.toString());

		case Bean.BOOL_TYPE:
		    return new Boolean("1".equals(data.toString()));

		case Bean.BYTE_TYPE:
		case Bean.CHAR_TYPE:
		    return data;

		default:
            // TODO: Perhaps move this into XMLFormatWriter and keep Clob and
            // Blob data as their respective types in the Bean objects for the
            // tables (e.g. Competitions).
            //
            if (data instanceof Clob) 
            {
                /*
                 * Handle Clob data. Copy the whole string value, up 64KB.
                 */
                final Clob clob = (Clob)data;
                return clob.getSubString(1L, (int)Math.min(64*1024, (int)clob.length()));
            }
		    else if (data instanceof Blob)
		    {
			/*
			 * Handle Blob data. Encode it to a Base64 string.
			 */
			Blob		blob = (Blob)data;
			long		size = blob.length();
			int		batchSize = 1728*1024;
			StringWriter	out;

			out = new StringWriter((int)(size * 1.36));
			try
			{
			    /*
			     * Process it a batch at a time to save memory.
			     * Make the batch size multiple of Base64 line
			     * size (54).
			     */
			    for (long n = 1; n <= size; n += batchSize)
			    {
				blobEncoder.encode(blob.getBytes(n, batchSize), out);
			    }
			    return out.toString();
			}
			finally
			{
			    out.close();
			}
		    }
		    /*
		     * This is not one of the supported type, just
		     * return the string representation of this object.
		     */
		    return data.toString();
		}
	    }
	    catch (SQLException se)
	    {
		if (se.getMessage().equals("Invalid column name"))
		{
		    /*
		     * There may be some extra attributes in the JavaBeans
		     * but not in the database. Ignore the exceptions that
		     * report invalid column name for now until the
		     * processing of those extra attributes are skipped.
		     */
		    return null;
		}
		se.printStackTrace();
		throw new IOException("SQL exception: " + se.getMessage());
	    }
	    catch (Throwable t)
	    {
		t.printStackTrace();
		throw new OutOfMemoryError("processAttribute");
	    }
	}
    }

    /**
     * The <c>SetPart</c> class provides the mechanism to generate the
     * set clause of an UPDATE statement (without the "set" keyword).
     */
    private class SetPart
	extends TextFormatWriter
    {
	int		count;

	/**
	 * Constructs a SetPart instance.
	 *
	 * @param	bean		the JavaBeans object to be formatted
	 */
	private SetPart(Bean bean)
	{
	    this(bean, null);
	}

	/**
	 * Constructs a SetPart instance.
	 *
	 * @param	bean		the JavaBeans object to be formatted
	 * @param	nameMap		the name mapping table
	 */
	private SetPart(Bean bean, Map nameMap)
	{
	    super(bean);
	    setNameMap(nameMap);
	    this.count = 0;
	}

	/**
	 * Process the attribute, identified by the given name, with the given
	 * data, into Predicate style.
	 *
	 * @param	name		the name of the attribute
	 * @param	data		the value of the attribute
	 * @param	dateType	the class type of the data
	 * @param	parameter	the parameter information
	 *
	 * @throws	IOException
	 */
	protected final void processAttribute(String name,
					      Object data,
					      Class dataType,
					      Object parameter)
	    throws IOException
	{
	    TextParam	param = (TextParam)parameter;
	    Writer	out = param.out;

	    if (count++ > 0)
	    {
		out.write(',');
	    }
	    /*
	     * Show name and operator.
	     */
	    out.write(' ');
	    out.write(name);
	    out.write(" = ?");
	}
    }

    /**
     * The <c>ValuesPart</c> class provides the mechanism to generate the
     * before and after of the values clause of an INSERT statement
     * (without the "values" keyword).
     */
    private class ValuesPart
	extends TextFormatWriter
    {
	int		count;
	boolean		before;

	/**
	 * Constructs a ValuesPart instance.
	 *
	 * @param	bean		the JavaBeans object to be formatted
	 */
	private ValuesPart(Bean bean)
	{
	    this(bean, null);
	}

	/**
	 * Constructs a ValuesPart instance.
	 *
	 * @param	bean		the JavaBeans object to be formatted
	 * @param	nameMap		the name mapping table
	 */
	private ValuesPart(Bean bean, Map nameMap)
	{
	    super(bean);
	    setNameMap(nameMap);
	    this.count = 0;
	    this.before = true;
	}

	/**
	 * Resets the counter.
	 */
	private void resetCounter()
	{
	    this.count = 0;
	}

	/**
	 * Indicates whether this is the before part or the after part of
	 * the values clause. Without calling this method, the default is
	 * set to be true (i.e. before).
	 *
	 * @param	before		true => before; false => after
	 */
	private void setPart(boolean before)
	{
	    this.before = before;
	}

	/**
	 * Process the attribute, identified by the given name, with the given
	 * data, into Predicate style.
	 *
	 * @param	name		the name of the attribute
	 * @param	data		the value of the attribute
	 * @param	dateType	the class type of the data
	 * @param	parameter	the parameter information
	 *
	 * @throws	IOException
	 */
	protected final void processAttribute(String name,
					      Object data,
					      Class dataType,
					      Object parameter)
	    throws IOException
	{
	    TextParam	param = (TextParam)parameter;
	    Writer	out = param.out;

	    if (count++ > 0)
	    {
		out.write(',');
	    }
	    out.write(' ');
	    if (before)
	    {
		/*
		 * Show name.
		 */
		out.write(name);
	    }
	    else
	    {
		/*
		 * Show '?'.
		 */
		out.write('?');
	    }
	}
    }
}

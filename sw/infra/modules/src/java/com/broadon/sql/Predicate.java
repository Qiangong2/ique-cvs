package com.broadon.sql;

import java.io.IOException;
import java.io.Writer;
import java.sql.PreparedStatement;
import java.util.Map;

/**
 * The <c>Predicate</c> interface defines the interface for predicate
 * classes used for finding records from the database.
 *
 * @version	$Revision: 1.3 $
 */
public interface Predicate
{
    public static final String	AND = "AND";
    public static final String	OR  = "OR";
    public static final String	NOT = "NOT";

    /**
     * Adds a Predicate instance to this Predicate that has the AND
     * relationship with it.
     *
     * @param	predicate		the Predicate instance that has the AND
     *					relationship with this Predicate
     *
     * @return	The given Predicate, so that this method can be part of an
     *		expression.
     */
    public Predicate and(Predicate predicate);

    /**
     * Adds a Predicate instance to this Predicate that has the OR
     * relationship with it.
     *
     * @param	predicate		the Predicate instance that has the OR
     *					relationship with this Predicate
     *
     * @return	The given Predicate, so that this method can be part of an
     *		expression.
     */
    public Predicate or(Predicate predicate);

    /**
     * Negates this Predicate.
     *
     * @return	The Predicate itself, so that this method can be part of
     *		an expression.
     */
    public Predicate not();

    /**
     * Binds the predicate to the given PreparedStatement.
     *
     * @param	statement		the PreparedStatement
     *
     * @throws	IOException
     */
    public void bind(PreparedStatement statement)
	throws IOException;

    /**
     * Binds the predicate to the given PreparedStatement.
     *
     * @param	statement		the PreparedStatement
     * @param	index			the attribute index
     *
     * @throws	IOException
     */
    public void bind(PreparedStatement statement, int index)
	throws IOException;

    /**
     * Generates the where clause component of this Predicate.
     *
     * @param	nameMap			the name mapping table
     *
     * @return	The where clause component.
     *
     * @throws	IOException
     */
    public String generateWherePart(Map nameMap)
	throws IOException;

    /**
     * Generates the where clause component of this PredicateItem.
     *
     * @param	out			the output destination
     * @param	nameMap			the name mapping table
     *
     * @throws	IOException
     */
    public void generateWherePart(Writer out, Map nameMap)
	throws IOException;
}

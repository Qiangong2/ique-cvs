package com.broadon.status;

import java.io.IOException;
import java.io.PrintWriter;

/**
 * Collect statistics of a servlet.
 */
public class Statistics
{
    Counter responseTime;
    Counter requestConcurrency;
    int byteSent;
    long start;

    /**
     * Construct a <code>Statistics</code> object.
     * @param pkgName Name of the package (e.g. xs, cds, etc.).  This
     * would be used to tag the stat. report.
     */
    public Statistics() {
	responseTime = new Counter();
	requestConcurrency = new Counter();
	byteSent = 0;
	start = System.currentTimeMillis();
    }

    /**
     * Insert a data point.
     * @param responseTime Time to finish processing a request (in seconds). 
     * @param requestConcurrency Total number of threads currently running.
     */
    synchronized public void addData(double responseTime,
				     int requestConcurrency, int byteSent) {
	this.responseTime.inc(responseTime);
	this.requestConcurrency.inc(requestConcurrency);
	this.byteSent += byteSent;
    }

    /**
     * Insert a data point indicating an unsuccessful request.
     */
    synchronized public void addError() {
	responseTime.error();
	requestConcurrency.error();
    }


    /**
     * Flush the accumulated statistical data, and then reset all counters.
     * @param out Output stream.
     * @param append Set to <code>true</code> if the output file
     * already exists and this data point is being appended to it.
     * @exception IOException
     */
    synchronized public void report(PrintWriter out, boolean append)
	throws IOException
    {
	long now = System.currentTimeMillis();

	if (append)
	    out.println("#");
	out.println("start_time=" + (start / 1000));
	out.println("interval=" + ((now - start) / 1000));
	responseTime.report("response_time.", out);
	requestConcurrency.report("request_concurrency.", out);
	out.println("byte_sent=" + byteSent);
	reset(now);
    }

    /**
     * Reset all statistic counters.
     * @param start Time indicating the starting point of data collection.
     */
    synchronized public void reset(long start) {
	responseTime.reset();
	requestConcurrency.reset();
	byteSent = 0;
	this.start = start;
    }


    // helper class for holding min., max, avg., etc.
    class Counter {
	int success_count;
	int failure_count;

	double sum;
	double min;
	double max;

	void reset() {
	    success_count = failure_count = 0;
	    sum = 0.0;
	    min = Double.POSITIVE_INFINITY;
	    max = Double.NEGATIVE_INFINITY;
	}

	Counter() {
	    reset();
	}

	void inc(double value) {
	    success_count++;
	    sum += value;
	    if (value < min)
		min = value;
	    if (value > max)
		max = value;
	}

	void error() {
	    failure_count++;
	}

	double getAverage() {
	    return sum / success_count;
	}

	void report(String prefix, PrintWriter out)
	    throws IOException
	{
	    out.println(prefix + "count=" + success_count);
	    out.println(prefix + "error=" + failure_count);
	    out.println(prefix + "min=" + min);
	    out.println(prefix + "max=" + max);
	    out.println(prefix + "avg=" + getAverage());
	}

    }; // Counter
}

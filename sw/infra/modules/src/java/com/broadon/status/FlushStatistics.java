package com.broadon.status;

import java.io.*;
import java.util.TimerTask;

/**
 * Timer task to trigger a flush of the statistics to the output file.
 */
public class FlushStatistics extends TimerTask
{
    String statFile;
    Statistics stat;

    /**
     * @param statFile Path name of the output file.
     * @param stat <code>Statistics</code> object that holds the
     * accumulated data. 
     */
    public FlushStatistics(String statFile, Statistics stat) {
	this.statFile = statFile;
	this.stat = stat;
    }

    public void run() {
	PrintWriter out = null;
	try {
	    File file = new File(statFile);
	    boolean append = file.exists();
	    out = new PrintWriter(new FileOutputStream(file, append), true);
	    stat.report(out, append);
	} catch (IOException e) {
	    e.printStackTrace(System.err);
	} finally {
	    if (out != null)
		out.close();
	}
    }
}

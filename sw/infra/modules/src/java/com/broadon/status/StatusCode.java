package com.broadon.status;

import java.util.HashMap;

/**
 * Top-level class for consolidating all BB Server response code and
 * error messages.  This class contains all status codes that are
 * common to all services.  Service-specifi status code could extend
 * this class.  Note that all fields and methods are static.
 */
public class StatusCode
{
    /** request successful */
    public static final String SC_OK = "000";
    static final String SC_OK_MSG = "OK";

    /** Database inconsistent error. */
    public static final String SC_DB_EXCEPTION = "001";
    static final String SC_DB_EXCEPTION_MSG =
	"Records in the database are not consistent.";

    /** Database connection error. */
    public static final String SC_SQL_EXCEPTION = "002";
    static final String SC_SQL_EXCEPTION_MSG =
	"Database connection error.";

    // mapping of status code to message
    static HashMap map = new HashMap();

    static {
	map.put(SC_OK, SC_OK_MSG);
	map.put(SC_DB_EXCEPTION, SC_DB_EXCEPTION_MSG);
	map.put(SC_SQL_EXCEPTION, SC_SQL_EXCEPTION_MSG);
    }

    /**
     * Return a textual description of a given status code.
     * @param code Statis code.
     * @return Corresponding description, or <code>null</code> if the
     * code is not found.
     */
    public static String getMessage(String code) {
	return (String) map.get(code);
    }

     /**
     * Add a new status code and corresponding description to the code
     * map.  Should be called by all subclasses to add their
     * service-specific status code to the <em>shared</em> code map.
     * @param code Status code string (3-digit number).
     * @param msg Status code description.
     */
    protected static void addStatusCode(String code, String msg) {
	map.put(code, msg);
    }

}

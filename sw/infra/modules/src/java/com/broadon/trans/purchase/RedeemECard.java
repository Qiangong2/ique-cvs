package com.broadon.trans.purchase;

import java.sql.*;

import com.broadon.db.DBException;
import com.broadon.trans.common.AuditLog;
import com.broadon.util.ECardNumber;

/**
 * Database interface for redeeming a list of eCards.
 */
public class RedeemECard {
    static final String sqlRedeem =
	"{ ? = call xsops.redeem_ecard(?, ?, ?, ?) }";

    /**
     * Redeem a list of eCards.  Some of the eCards might not be
     * redeemed if the total value of the rest of the eCards already
     * exceed the sale price.
     * @param conn Database connection.
     * @param bbID Encoded ID of the BB player for the eCards are
     * redeemed.
     * @param hrID Encoded ID of the Depot carrying out this
     * transaction.
     * @param ecards Vector of <em>unique</em> eCards to be redeemed.
     * @param price Cost of the transaction.
     * @return A RedeemECard object.
     * @exception SQLException
     * @exception DBException.
     */
    protected static RedeemECard redeem(Connection conn, long bbID,
					long hrID, long titleID,
					String[] ecards, int price,
					AuditLog auditLog)
	throws SQLException, DBException
    {
	CallableStatement cs = conn.prepareCall(sqlRedeem);
	cs.registerOutParameter(1, Types.INTEGER);

	if (ecards == null || price <= 0)
	    return new RedeemECard(0);
	RedeemECard cards = new RedeemECard(ecards.length);
	ValidateEcard eNumber = new ValidateEcard(conn);

	try {
	    for (int i = 0; i < ecards.length; ++i) {

		ECardRedemptionLog log =
		    new ECardRedemptionLog(bbID, titleID, ecards[i]);
		auditLog.add(log);

		if (! eNumber.isValid(ecards[i], titleID)) {
		    cards.cardStatus[i] = CARD_NOTFOUND;
		    log.setStatus(0, String.valueOf(-CARD_NOTFOUND),
				  "Invalid card number");
		    auditLog.add(log);
		    continue;
		}
	    
		cs.setLong(2, bbID);
		cs.setLong(3, hrID);
		cs.setLong(4, titleID);
		cs.setString(5, ECardNumber.extractSerial(ecards[i]));
		cs.executeUpdate();

		cards.addValue(cs.getInt(1), i, log);
		auditLog.add(log);
	    
		if (cards.totalRedeemed >= price) {
		    for (int j = i + 1; j < ecards.length; ++j)
			cards.cardStatus[j] = CARD_NOTUSED;
		    break;
		}
	    }
	} finally {
	    eNumber.close();
	    cs.close();
	}

	return cards;
    } // RedeemECard

    int totalRedeemed;
    int[] cardStatus;

    /** eCard redemption status:  eCard is good and redeemed. */
    public static final int CARD_GOOD = 0;
    /** eCard redemption status:  eCard is not used. */
    public static final int CARD_NOTUSED = -1;
    /** eCard redemption status:  eCard is not found . */
    public static final int CARD_NOTFOUND = -11;
    /** eCard redemption status:  eCard is revoked. */
    public static final int CARD_REVOKED = -12;
    /** eCard redemption status:  eCard is expired. */
    public static final int CARD_EXPIRED = -13;
    /** eCard redemption status:  eCard is not activated. */
    public static final int CARD_NOT_ACTIVATED = -14;
    /** eCard redemption status:  eCard is already redeemed. */
    public static final int CARD_REDEEMED = -15;
    /** eCard redemption status:  eCard has zero balance. */
    public static final int CARD_NO_VALUE = -16;
    /** eCard redemption status:  eCard type is not supported. */
    public static final int CARD_NOT_SUPPORTED = -19;
    

    private RedeemECard(int nCards) {
	totalRedeemed = 0;
	cardStatus = new int[nCards];
    }


    // Add the given card value to the total.  Check and record error
    // condition.  
    private void addValue(int value, int idx, ECardRedemptionLog log)
	throws DBException
    {
	if (value >= 0) {
	    totalRedeemed += value;
	    cardStatus[idx] = CARD_GOOD;
	    log.setStatus(value, "OK", null);
	} else {
	    String msg = null;
	    
	    switch (value) {
	    case CARD_NOTFOUND:
		msg = "Invalud card number";
		break;
		
	    case CARD_REVOKED:
		msg = "revoked";
		break;
		
	    case CARD_EXPIRED:
		msg = "expired";
		break;
		
	    case CARD_NOT_ACTIVATED:
		msg = "not activated";
		break;
		
	    case CARD_REDEEMED:
		msg = "redeemed";
		break;
		
	    case CARD_NO_VALUE:
		msg = "zero value";
		break;
		
	    case CARD_NOT_SUPPORTED:
		msg = "invalid card type";
		break;
		
	    default:
		throw new DBException("Database record inconsistent");
	    }
	    log.setStatus(0, String.valueOf(-value), msg);
	    cardStatus[idx] = value;
	}
    } // addValue

    /** @return the total redeemed values of all eCards. */
    public int getValue() {
	return totalRedeemed;
    }

    /** @return the array of eCards status.  */
    public int[] getCardStatus() { 
	return cardStatus;
    }
}  

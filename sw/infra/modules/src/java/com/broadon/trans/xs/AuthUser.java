package com.broadon.trans.xs;

import java.sql.Connection;
import java.sql.SQLException;
import java.security.GeneralSecurityException;
import javax.servlet.http.HttpServletRequest;

import com.broadon.filter.authWrapper;

import com.broadon.trans.common.*;

/**
 * Authenticate the given user id and password (or smart card encrypted message)
 */
public final class AuthUser extends TransactionServlet
{
    static final String REQUEST_RESULT_TAG = "operator_auth_result";
    static final String DEPOT_VERSION_KEY = "depot_version";
    static final String USER_ID_KEY = "user_id";
    static final String PASSWORD_KEY = "password";
    static final String ROLE_KEY = "role";
    
    protected TransactionRequest readInputs(XMLResponse res,
                                            HttpServletRequest req)
    {
	AuthUserRequest authReq = new AuthUserRequest();

	res.startDoc(REQUEST_RESULT_TAG);

        res.printElement(DEPOT_VERSION_KEY,
			 req.getParameter(DEPOT_VERSION_KEY));

	authReq.userID = req.getParameter(USER_ID_KEY);
        res.printElement(USER_ID_KEY, authReq.userID);

        authReq.password = req.getParameter(PASSWORD_KEY);
        res.printElement(PASSWORD_KEY, authReq.password);

	return authReq;
    }

    protected void process(Connection conn, authWrapper req,
                           TransactionRequest transReq, XMLResponse res)
        throws SQLException
    {
        AuthUserRequest authReq = (AuthUserRequest) transReq;

	// validate password or smart card
	try {
	    String[] roles = ServiceTech.getRoles(conn, req.getBUID(),
						  authReq.userID,
						  authReq.password, req);
	    for (int i = 0; i < roles.length; ++i)
		res.printElement(ROLE_KEY, roles[i]);
	    res.printExitStatus(StatusCode.SC_OK);
	} catch (GeneralSecurityException e) {
	    res.printExitStatus(StatusCode.XS_UNAUTH_TECH);
	    return;
	}
    }
}

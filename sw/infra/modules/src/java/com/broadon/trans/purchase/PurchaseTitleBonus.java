package com.broadon.trans.purchase;

import java.sql.*;

import com.broadon.jni.BBcrypto;
import com.broadon.db.DBException;
import com.broadon.exception.*;
import com.broadon.trans.common.BBPlayer;
import com.broadon.trans.common.AuditLog;

/**
 * Database interface to handle purchasing of a game title for trial play.
 * See the description of Purchase title for a description of methods redefined
 * in this subclass.
 * @version 
 * @author 
 */
public class PurchaseTitleBonus extends PurchaseTitle
{
    protected void generateTicketProperties(BBPlayer  bb, 
                                            TitleInfo tinfo)
    {
        final short limit = (short)tinfo.getBonusLimits();
        final int   rtype =
            (tinfo.getRtype().equals(TitleContentInfo.RTYPE_TIME_LIMITED)?
             BBcrypto.LR_TIME : BBcrypto.LR_COUNT);

        ticketIDs = new int[getContentIDs().length];
        ticketRTypes = new int[getContentIDs().length];
        ticketLimits = new short[getContentIDs().length];
        int tid = bb.getLimitedTID();
        for (int i = 0; i < ticketIDs.length; ++i) {
            if (++tid > MAX_TEMPORARY_TICKET_ID)
                tid = MIN_TEMPORARY_TICKET_ID;
            ticketIDs[i] = tid;
            ticketLimits[i] = limit;
            ticketRTypes[i] = rtype;
        }
        bb.setLimitedTID(tid);
    }

    /**
     * Construct a <code>PurchaseTitleBonus</code> object.
     * @param bb BB player record.
     * @param titleID Title to be purchased.
     * @param hrID Depot that submits this purchase request.
     * @param storeID ID of the store to which the Depot belongs.
     * @param regionID Logical region where the BB Depot belongs.
     * @param contendIDs One or more content ID that compose this
     * title.  If <code>null</code>, the content ID list will be
     * fetched from the database.
     * @param clientIP The client IP address of the member doing the purchase
     * @param membershipID The identity of the member doing the purchase
     * @param memberID The identity of the member doing the purchase
     */
    public PurchaseTitleBonus(BBPlayer bb,
                              long     hrID, // BB id
                              int      storeID,
                              int      regionID,
                              long     titleID, 
                              long[]   contentIDs,
                              String   clientIP,     // null when undefined
                              long     membershipID, // -1 when undefined
                              long     memberID)     // -1 when undefined
    {
        super(bb, hrID, storeID, regionID, titleID, contentIDs,
              clientIP, membershipID, memberID);
    }


    protected void validateContentInfo(TitleInfo        tinfo,
                                       TitleContentInfo tcf,
                                       long             cID, 
                                       Object           cinf) throws
        InvalidRequestException,
        TitleAlreadyPurchasedException,
        TrialAfterUseException
    {
        super.validateContentInfo(tinfo, tcf, cID, cinf);

        if (tcf.etkExists(cinf)) {
            //
            // Ensure that no permanent purchase of this content is in 
            // effect.
            //
            if (tcf.etkRtype(cinf).equals(TitleContentInfo.RTYPE_UNLIMITED))
                throw new TitleAlreadyPurchasedException("Title ID = " +
                                                         getTitleID() +
                                                         " ContentName = " +
                                                         tcf.coName(cinf));
            //
            // Ensure that the type of limited play (time vs number of plays)
            // does not change.
            //
            else if (!tcf.etkRtype(cinf).equals(tinfo.getRtype()))
                throw new InvalidRequestException("Limitation changed from " +
                                                  tcf.etkRtype(cinf) +
                                                  " to " +
                                                  tinfo.getRtype() + 
                                                  " ContentName = " +
                                                  tcf.coName(cinf));
        }
    }


    protected void validateTitleInfo(TitleInfo tinfo) throws
        InvalidRequestException,
        TitleAlreadyPurchasedException,
        TrialAfterUseException
    {
        super.validateTitleInfo(tinfo);
        
        // Verify that the title rtype allows limited play
        //
        if (!tinfo.allowsLimitedPlay() || tinfo.getBonusLimits() <= 0) {
            throw new 
                InvalidRequestException("Title disallows bonus purchase.");
        }
    }


    protected int getPurchasePrice(TitleInfo tinfo)
    {
        return 0;
    }


    protected String getPurchaseRtype(TitleInfo tinfo)
    {
        return tinfo.getRtype();
    }


    protected void payForPurchase(Connection conn, 
                                  TitleInfo  tinfo,
                                  AuditLog   auditLog) throws
        SQLException, 
        DBException, 
        ECardRedemptionException
    {
        return;
    }


    public String purchaseWith()
    {
        return "SC_BONUS";
    }


    protected void generateEticketRecord(Connection conn,
                                         TitleInfo  tinfo,
                                         int []     ticketIDs)
	throws SQLException, DBException
    {
        ETicketRecord.generate(conn, 
                               getBbID(), 
                               getContentIDs(), 
                               ticketIDs,
                               tinfo.getRtype(),
                               tinfo.getBonusLimits(),
                               false); // Not a test
    }


    public int[] getECardStatus() 
    {
        return new int[0];
    }

}

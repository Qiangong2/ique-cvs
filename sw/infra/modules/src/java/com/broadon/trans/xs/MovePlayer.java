package com.broadon.trans.xs;

import java.sql.*;
import java.security.GeneralSecurityException;
import javax.servlet.http.*;

import com.broadon.filter.*;
import com.broadon.util.Pair;
import com.broadon.exception.InvalidRequestException;

import com.broadon.trans.common.*;
import com.broadon.trans.etickets.*;
import com.broadon.trans.rma.*;


/**
 * Servlet implementation that handles a BB player RMA request.
 */
public final class MovePlayer extends TransactionServlet
{
    static final String REQUEST_RESULT_TAG = "rma_result";
    static final String DEPOT_VERSION_KEY = "depot_version";
    static final String OLD_BB_ID_KEY = "old_bb_id";
    static final String NEW_BB_ID_KEY = "new_bb_id";
    static final String OLD_SERIAL_KEY = "old_serial_no";
    static final String NEW_SERIAL_KEY = "new_serial_no";
    static final String BB_MODEL_KEY = "bb_model";
    static final String USER_ID_KEY = "user_id";
    static final String PASSWORD_KEY = "password";
    static final String TIMESTAMP_KEY = "sync_timestamp";

    protected void process(Connection conn, authWrapper req,
			   TransactionRequest transReq, XMLResponse res)
	throws SQLException
    {
	MovePlayerRequest moveReq = (MovePlayerRequest) transReq;

	// validate password or smart card
	long operatorID;
	try {
	    operatorID = ServiceTech.validate(conn, req.getBUID(),
					      moveReq.userID, moveReq.password,
					      req, ServiceTech.rmaOperator);
	} catch (GeneralSecurityException e) {
	    res.printExitStatus(StatusCode.XS_UNAUTH_TECH);
	    return;
	}

	try {
	    readBBInfo(conn, moveReq);
	    res.printElement(OLD_BB_ID_KEY, String.valueOf(moveReq.oldBBID));
	    res.printElement(NEW_BB_ID_KEY, String.valueOf(moveReq.newBBID));
	    res.printElement(OLD_SERIAL_KEY, moveReq.oldSerial);
	    res.printElement(NEW_SERIAL_KEY, moveReq.newSerial);
	} catch (BadReplacementBBException e) {
	    res.printExitStatus(StatusCode.XS_BAD_RMA_REQUEST_NEW_BB, e);
	    return;
	} catch (BadUserBBException e) {
	    res.printExitStatus(StatusCode.XS_BAD_RMA_REQUEST_OLD_BB, e);
	    return;
	}

	// log the operator id that authorized the RMA
	BBexchangeLog log = new BBexchangeLog(moveReq.newBBID,
					      moveReq.oldBBID,
					      "RMA", operatorID);

	auditLog.add(log);

	try {
	    conn.setAutoCommit(false);
	    BBexchange moveBB = new BBexchange(moveReq.oldBBID,
					       moveReq.newBBID,
					       req.getBUID(),
					       conn);

	    moveBB.performExchange(log.getRequestTime(), regionalCenterID);

	    res.printElement(BB_MODEL_KEY, moveBB.getBBModel());

	    // send sync time and tickets
	    res.printElement(TIMESTAMP_KEY,
			     String.valueOf(log.getRequestTime()));

	    ETickets tickets = new ETickets(conn, hsm, moveReq.newBBID,
					    moveBB.getBBKey(),
					    regionalCenterID,
					    ETickets.MIN_TID, req.getBUID());
	    tickets.generateAll();
	    ETicketsFormatter out = new ETicketsFormatter(tickets, res);
	    out.dumpTickets();
	    out.dumpCerts();

	    conn.commit();
	    res.printExitStatus(StatusCode.SC_OK);

	} catch (BadReplacementBBException e) {
	    res.printExitStatus(StatusCode.XS_BAD_RMA_REQUEST_NEW_BB, e);
	} catch (BadUserBBException e) {
	    res.printExitStatus(StatusCode.XS_BAD_RMA_REQUEST_OLD_BB, e);
	} catch (ETicketCreationException ece) {
	    res.printExitStatus(StatusCode.XS_ETICKET_SYNC, ece);
	    
	} finally {
	    log.setStatus(res.getExitStatusCode(), res.getExitStatusMsg());
	    auditLog.add(log);
	    conn.rollback();
	}

    } // process


    protected TransactionRequest readInputs(XMLResponse res,
					    HttpServletRequest req)
    {
	String s;
	MovePlayerRequest moveReq = new MovePlayerRequest();

	res.startDoc(REQUEST_RESULT_TAG);

	s = req.getParameter(DEPOT_VERSION_KEY);
	res.printElement(DEPOT_VERSION_KEY, s);
	
	s = req.getParameter(OLD_BB_ID_KEY);
	moveReq.oldBBID = (s == null) ? -1 : Long.parseLong(s);

	s = req.getParameter(NEW_BB_ID_KEY);
	moveReq.newBBID = (s == null) ? -1 : Long.parseLong(s);

	moveReq.oldSerial = req.getParameter(OLD_SERIAL_KEY);
	
	moveReq.newSerial = req.getParameter(NEW_SERIAL_KEY);

	s = req.getParameter(HR_ID_KEY);
	// Verify the format of the Depot ID
        if (s.length() != 14 || ! s.startsWith("HR")) {
            throw new NumberFormatException("Invalid Depot ID");
        }
        moveReq.depotID = Long.parseLong(s.substring(2), 16);
	res.printElement(HR_ID_KEY, s);

	moveReq.userID = req.getParameter(USER_ID_KEY);
        res.printElement(USER_ID_KEY, moveReq.userID);

        moveReq.password = req.getParameter(PASSWORD_KEY);
        res.printElement(PASSWORD_KEY, moveReq.password);

	return moveReq;
    } // readInputs


    // ======================================================================

    static final String readBB =
	"SELECT BB_ID, SN FROM BB_PLAYERS WHERE BB_ID = ? OR SN = ?";

    void readBBInfo(Connection conn, MovePlayerRequest req)
	throws SQLException, BadUserBBException, BadReplacementBBException
    {
	PreparedStatement ps = conn.prepareStatement(readBB);

	Pair value;

	try {
	    try {
		value = locateBB(ps, req.oldBBID, req.oldSerial);
		req.oldBBID = ((Long) (value.first)).longValue();
		req.oldSerial = (String) value.second;
	    } catch (InvalidRequestException e) {
		throw new BadUserBBException(e.getMessage());
	    }

	    try {
		value = locateBB(ps, req.newBBID, req.newSerial);
		req.newBBID = ((Long) (value.first)).longValue();
		req.newSerial = (String) value.second;
	    } catch (InvalidRequestException e) {
		throw new BadReplacementBBException(e.getMessage());
	    }
	} finally {
	    ps.close();
	}
    }


    Pair locateBB(PreparedStatement ps, long bbID, String sn)
	throws SQLException, InvalidRequestException
    {
	ps.setLong(1, bbID);
	ps.setString(2, sn);
	ResultSet rs = ps.executeQuery();
	try {
	    if (!rs.next()) {
		String id = (bbID == -1) ? sn : String.valueOf(bbID);
		throw new InvalidRequestException("Cannot locate player " + id);
	    }
	    
	    Pair result = new Pair(new Long(rs.getLong(1)), rs.getString(2));
	    
	    if (rs.next()) {
		// should not have more than a row
		throw new InvalidRequestException("Inconsistent player Id " +
						  bbID + " and serial number " +
						  sn);
	    }

	    return result;
	} finally {
	    rs.close();
	}
    } // locateBB
}

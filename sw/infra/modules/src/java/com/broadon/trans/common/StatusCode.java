package com.broadon.trans.common;

/**
 * Response code specific to the transaction server.
 */
public class StatusCode extends com.broadon.status.StatusCode
{
    /** Purchase request is bad. */
    public static final String XS_BAD_REQUEST = "101";
    static final String XS_BAD_REQUEST_MSG =
	"Inconsistent price or content ID list for specified title";

    /** Malformed XML input data. */
    public static final String XS_MALFORMED_INPUT = "102";
    static final String XS_MALFORMED_INPUT_MSG =
	"Missing or malformed XML input.";

    /** BB Player already owns that title.  Duplicated purchase ignored. */
    public static final String XS_DUPLICATED_PURCHASE = "103";
    static final String XS_DUPLICATED_PURCHASE_MSG =
	"BB Player already owns that title.  Duplicated purchase ignored.";

    /** Ecard values < price */
    public static final String XS_ECARD_ERROR = "104";
    static final String XS_ECARD_ERROR_MSG =
	"Some of the eCards are bad or the total value does not cover the sale price";

    /** Bad BB ID. */
    public static final String XS_BAD_BB_ID = "105";
    static final String XS_BAD_BB_ID_MSG =
	"Invalid BB ID for this business unit.";

    /** eTicket sync. */
    public static final String XS_ETICKET_SYNC = "106";
    static final String XS_ETICKET_SYNC_MSG =
	"Error in retrieving eTickets";

    /** Bad Store ID. */
    public static final String XS_BAD_STORE = "107";
    static final String XS_BAD_STORE_MSG =
	"Invalid or revoked store ID";

    /** Unauthorized Service Technician. */
    public static final String XS_UNAUTH_TECH = "108";
    static final String XS_UNAUTH_TECH_MSG =
	"Service Tech. login failure";

    /** Unable to generate new certificate. */
    public static final String XS_CERT_GEN = "109";
    static final String XS_CERT_GEN_MSG =
	"Error in generating new certificate.";

    /** eTickets already in sync. */
    public static final String XS_ETICKET_IN_SYNC = "110";
    static final String XS_ETICKET_IN_SYNC_MSG =
	"eTickets are already in sync.";

    /** requested email address already taken by other customer. */
    public static final String XS_EMAIL_ADDR_TAKEN = "111";
    static final String XS_EMAIL_ADDR_TAKEN_MSG =
	"email address already taken.";

    /** No registration record for BB ID. */
    public static final String XS_NO_REG_RECORD = "112";
    static final String XS_NO_REG_RECORD_MSG =
	"Customer registration record not found";

    /** Invalid bundle ID . */
    public static final String EMS_BAD_BUNDLE = "113";
    static final String EMS_BAD_BUNDLE_MSG =
	"Invalid bundle ID";

    /** Bad replacement BB for RMA. */
    public static final String XS_BAD_RMA_REQUEST_NEW_BB = "114";
    static final String XS_BAD_RMA_REQUEST_NEW_BB_MSG =
	"RMA request rejected because of bad replacement BB";

    public static final String XS_BAD_RMA_REQUEST_OLD_BB = "115";
    static final String XS_BAD_RMA_REQUEST_OLD_BB_MSG =
	"RMA request rejected because of bad user BB";

    public static final String XS_UPGRADE_TICKET = "116";
    static final String XS_UPGRADE_TICKET_MSG = "Cannot upgrade content";

    public static final String XS_TRIAL_AFTER_USE = "117";
    static final String XS_TRIAL_AFTER_USE_MSG = "Cannot try a game that has already been acquired in a limited or unlimited capacity";

    public static final String XS_CANNOT_COMPETE = "118";
    static final String XS_CANNOT_COMPETE_MSG = "Cannot enter competition due to time or location constraint";

    static {
	addStatusCode(XS_BAD_REQUEST, XS_BAD_REQUEST_MSG);
	addStatusCode(XS_MALFORMED_INPUT, XS_MALFORMED_INPUT_MSG);
	addStatusCode(XS_DUPLICATED_PURCHASE, XS_DUPLICATED_PURCHASE_MSG);
	addStatusCode(XS_ECARD_ERROR, XS_ECARD_ERROR_MSG);
	addStatusCode(XS_BAD_BB_ID, XS_BAD_BB_ID_MSG);
	addStatusCode(XS_ETICKET_SYNC, XS_ETICKET_SYNC_MSG);
	addStatusCode(XS_BAD_STORE, XS_BAD_STORE_MSG);
	addStatusCode(XS_UNAUTH_TECH, XS_UNAUTH_TECH_MSG);
	addStatusCode(XS_CERT_GEN, XS_CERT_GEN_MSG);
	addStatusCode(XS_ETICKET_IN_SYNC, XS_ETICKET_IN_SYNC_MSG);
	addStatusCode(XS_EMAIL_ADDR_TAKEN, XS_EMAIL_ADDR_TAKEN_MSG);
	addStatusCode(XS_NO_REG_RECORD, XS_NO_REG_RECORD_MSG);
	addStatusCode(EMS_BAD_BUNDLE, EMS_BAD_BUNDLE_MSG);
	addStatusCode(XS_BAD_RMA_REQUEST_NEW_BB, XS_BAD_RMA_REQUEST_NEW_BB_MSG);
	addStatusCode(XS_BAD_RMA_REQUEST_OLD_BB, XS_BAD_RMA_REQUEST_OLD_BB_MSG);
	addStatusCode(XS_UPGRADE_TICKET, XS_UPGRADE_TICKET_MSG);
    addStatusCode(XS_TRIAL_AFTER_USE, XS_TRIAL_AFTER_USE_MSG);
    addStatusCode(XS_CANNOT_COMPETE, XS_CANNOT_COMPETE_MSG);
    }
}

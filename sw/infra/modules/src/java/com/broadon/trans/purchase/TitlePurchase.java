package com.broadon.trans.purchase;

import java.sql.*;
import com.broadon.db.DBException;

/**
 * Database interface for generating a <code>TitlePurchase</code>
 * record, which records an approved sale of a particualr title for a
 * particular BB player.
 */
public class TitlePurchase {
    static final String newPurchase =
	"{ ? = call xsops.title_purchase(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) }";

    /**
     * Generate a <code>TitlePurchase</code> record.
     * @param conn Database connection to be used.
     * @param bbID Encoded ID of the BB player.
     * @param hrID Depot that submits this purchase request.
     * @param titleID ID of this title.
     * @param regionID Logical region where this BB Depot belongs.
     * @param storeID ID of the store to which the Depot belongs.
     * @param clientIP The client IP address of the member doing the purchase
     * @param membershipID The identity of the member doing the purchase
     * @param memberID The identity of the member doing the purchase
     * @exception SQLException Database access error.
     * @exception DBException Data inconsistency detected.
     * @exception TitleAlreadyPurchasedException BB player already
     * owns this title.
     */
    protected static void generate(Connection conn, 
                                   long       bbID, 
                                   long       hrID,
                                   long       titleID, 
                                   int        regionID, 
                                   int        storeID, 
                                   String     limitedPlayType,
                                   int        price,
                                   long       operatorID,
                                   String     purchaseWith,
                                   String     clientIP,     // null = undefined
                                   long       membershipID, // -1 = undefined
                                   long       memberID)     // -1 = undefined
	throws SQLException, DBException, TitleAlreadyPurchasedException
    {
	CallableStatement cs = conn.prepareCall(newPurchase);
	cs.registerOutParameter(1, Types.INTEGER);
	cs.setLong(2, bbID);
	cs.setLong(3, hrID);
	cs.setLong(4, titleID);
	cs.setInt(5, regionID);
	cs.setInt(6, storeID);
	cs.setString(7, limitedPlayType);
	cs.setInt(8, price);
    if (operatorID != -1L)
        cs.setLong(9, operatorID);
    else 
        cs.setNull(9, Types.NUMERIC);
	cs.setString(10, purchaseWith);

    if (clientIP != null)
        cs.setString(11, clientIP);
    else 
        cs.setNull(11, Types.VARCHAR);

    if (membershipID != -1L)
        cs.setLong(12, membershipID);
    else 
        cs.setNull(12, Types.NUMERIC);

    if (memberID != -1L)
        cs.setLong(13, memberID);
    else 
        cs.setNull(13, Types.NUMERIC);

	try {
	    cs.executeUpdate();
	    int returnCode = cs.getInt(1);

	    switch (returnCode) {
	    case 1:
		return;

	    case -5:
		throw new TitleAlreadyPurchasedException("Title ID = " + titleID);

	    default:
		throw new DBException("Cannot create purchase record.");
	    }
	} finally {
	    cs.close();
	}
	
    } // generate
					    
}

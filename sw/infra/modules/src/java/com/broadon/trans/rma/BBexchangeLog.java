package com.broadon.trans.rma;

import java.sql.*;

import com.broadon.trans.common.Logger;

/**
 * Logger for RMA:  moving all etickets from one BB to another.
 */
public class BBexchangeLog extends Logger
{
    static final String addLog =
	"INSERT INTO BB_EXCHANGE_LOGS (BB_ID, REQUEST_DATE, EXCHAGE_TYPE, " +
	"  REPLACED_BB_ID, OPERATOR_ID) VALUES (?, " + TIMESTAMP + ", ?, ?, ?)";

    static final String updateLog =
	"UPDATE BB_EXCHANGE_LOGS SET REQUEST_STATUS=?, REQUEST_LOG=? " +
	"    WHERE BB_ID=? AND REQUEST_DATE = " + TIMESTAMP;

    long newBB;
    long oldBB;
    String exchangeType;
    long opID;

    String status;
    String message;

    boolean flushed;

    /**
     * Create an <code>BBexchagneLog</code> object holding the
     * values to be inserted into the audit log.
     */
    public BBexchangeLog(long newBB, long oldBB, String type, long opID)
    {
	super();
	
	this.newBB = newBB;
	this.oldBB = oldBB;
	this.exchangeType = type;
	this.opID = opID;

	flushed = false;
	
	status = message = null;
    }


    synchronized public void flush(Connection conn) throws SQLException
    {
	PreparedStatement ps;
	int i = 1;

	if (flushed) {
	    // already written once, need to update the existing record
	    ps = conn.prepareStatement(updateLog);
	    ps.setString(i++, status);
	    ps.setString(i++, message);
	    ps.setLong(i++, newBB);
	    ps.setLong(i++, timestamp);
	} else {
	    ps = conn.prepareStatement(addLog);
	    ps.setLong(i++, newBB);
	    ps.setLong(i++, timestamp);
	    ps.setString(i++, exchangeType);
	    ps.setLong(i++, oldBB);
	    ps.setLong(i++, opID);
	}

	try {
	    ps.executeUpdate();
	} finally {
	    ps.close();
	}

	flushed = true;
    }

    /**
     * Set the status of BB exchange and possibly an error message. 
     */
    synchronized public void setStatus(String status, String message)
    {
	this.status = status;
	this.message = message;
    }

    public long getRequestTime() {
	return timestamp;
    }
}

package com.broadon.trans.common;

import java.math.BigInteger;
import java.sql.*;

import com.broadon.db.DBException;

/**
 * Database interface to access the BB_PLAYERS record.
 */
public class BBPlayer
{
    static final String getBB =
	"SELECT PUBLIC_KEY, REGIONAL_CENTER_ID, BB_MODEL, " +
	"    BBXML.ConvertTimeStamp(LAST_ETDATE), LAST_LR_TID, LAST_PR_TID, " +
	"    BBXML.ConvertTimeStamp(XSOPS.GLOBAL_ETICKET_TS), " +
	"    BBXML.ConvertTimeStamp(CURRENT_TIMESTAMP) " +
	"FROM BB_PLAYERS WHERE BB_ID = ? AND BU_ID = ? AND " +
	"    DEACTIVATE_DATE IS NULL AND REVOKE_DATE IS NULL";

    static final String updateRecord =
	"UPDATE BB_PLAYERS SET LAST_ETDATE=BBXML.ConvertTimeStamp(?), " +
	"    LAST_LR_TID=?, LAST_PR_TID=? WHERE BB_ID = ?";

    long bbID;
    String model;
    BigInteger bbKey;
    int regional_center;
    long lastSync;
    long globalTicketTimestamp;
    long currentTime;
    int limitedTID;		// ticket ID for limited rights eTicket
    int permanentTID;		// ticket ID for permanent eTicket

    boolean recordChanged = false;
    long newTimestamp;
    int newLimitedTID;
    int newPermanentTID;
    

    /**
     * Construct a <code>BBPlayer</code> object and fill in the fields
     * with data from the database.
     * @param bbID Encoded ID of a BB player.
     * @param buID Business Unit ID of this BB player.
     * @param conn Database connection to be used.
     * @exception SQLException Database access error.
     * @exception DBException BB ID not found.
     */
    public BBPlayer(long bbID, int buID, Connection conn)
	throws SQLException, InvalidBBIDException
    {
	this.bbID = bbID;

	PreparedStatement ps = conn.prepareStatement(getBB);
	ps.setLong(1, bbID);
	ps.setInt(2, buID);
	try {
	    ResultSet rs = ps.executeQuery();
	
	    // TO DO:  check for mismatched regional center

	    if (rs.next()) {
		int i = 0;
		bbKey = new BigInteger(rs.getString(++i), 32);
		regional_center = rs.getInt(++i);
		model = rs.getString(++i);
		lastSync = rs.getLong(++i);
		if (rs.wasNull())
		    lastSync = 0;
		limitedTID = rs.getInt(++i);
		permanentTID = rs.getInt(++i);
		globalTicketTimestamp = rs.getLong(++i);
		currentTime = rs.getLong(++i);

		lastSync = (lastSync > globalTicketTimestamp) ? lastSync :
		    globalTicketTimestamp;
		newTimestamp = lastSync;
		newPermanentTID = permanentTID;
		newLimitedTID = limitedTID;

		rs.close();
	    } else
		throw new InvalidBBIDException("BB ID not found for BU #" + buID);
	} finally {
	    ps.close();
	}
    }


    /** @return The encoded BB ID. */
    public long getBBID() {
	return bbID;
    }

    /** @return The model name of this BB player. */
    public String getBBModel() {
	return model;
    }

    /** @return The public key of this BB player. */
    public BigInteger getBBKey() {
	return bbKey;
    }

    /** @return the regional center ID where this BB player last
	connected to. */
    public int getRegionalCenter() {
	return regional_center;
    }

    /** @return the last eTicket synchronization time in number of
	seconds since Epoch. */
    public long getLastSync() {
	return lastSync / 1000;
    }

    /** @return the current database time in number of seconds since
	Epoch. */
    public long getCurrentTime() {
	return currentTime / 1000;
    }

    /** @return the timestamp when any global ticket was last updated, in
	number of seconds since Epoch. */
    public long getGlobalTicketTimestamp() {
	return globalTicketTimestamp / 1000;
    }

    /** @return the last ticket ID for permanent eTickets. */
    public int getPermanentTID() {
	return permanentTID;
    }

    /** @return the last ticket ID for limited rights eTickets. */
    public int getLimitedTID() {
	return limitedTID;
    }


    /** Set the last sync time to the current timestamp. */
    public void setSyncTime() {
	newTimestamp = currentTime;
	recordChanged = true;
    }

    /** Set the TID for permanent eTicket.
     * @param tid New ticket ID value.
     */
    public void setPermanentTID(int tid) {
	newPermanentTID = tid;
	recordChanged = true;
    }

    /** Set the TID for limited rights eTicket.
     * @param tid New ticket ID value.
     */
    public void setLimitedTID(int tid) {
	newLimitedTID = tid;
	recordChanged = true;
    }

    

    /**
     * Update the last sync time of this BB player to the current
     * database time.
     * @param conn Database connection.
     * @exception SQLException
     */
    public void updateRecord(Connection conn)
	throws SQLException
    {
	PreparedStatement ps = conn.prepareStatement(updateRecord);
	ps.setLong(1, newTimestamp);
	ps.setInt(2, newLimitedTID);
	ps.setInt(3, newPermanentTID);
	ps.setLong(4, bbID);
	try {
	    ps.executeUpdate();
	} finally {
	    ps.close();
	}
    }
}

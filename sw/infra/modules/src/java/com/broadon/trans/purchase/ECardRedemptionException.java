package com.broadon.trans.purchase;

import com.broadon.exception.BroadOnException;

/**
 * The <code>ECardRedemptionException</code> class is thrown when
 * a purchase request is rejected because the total value of eCards
 * does not cover the cost of the title.
 * 
 *
 * @version	$Revision: 1.2 $
 */
public class ECardRedemptionException
    extends BroadOnException
{
    /**
     * Constructs a ECardRedemptionException instance.
     */
    public ECardRedemptionException()
    {
	super();
    }

    /**
     * Constructs a ECardRedemptionException instance.
     *
     * @param	message			the exception message
     */
    public ECardRedemptionException(String message)
    {
	super(message);
    }

    /**
     * Constructs a ECardRedemptionException instance.
     *
     * @param	message			the exception message
     * @param	throwable		the nested exception
     */
    public ECardRedemptionException(String message, Throwable throwable)
    {
	super(message, throwable);
    }
}

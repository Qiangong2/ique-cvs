package com.broadon.trans.upload;

import com.broadon.db.DBException;

/**
 * The <code>CannotCompeteException</code> class is thrown when
 * a competition cannot be entered because the competition has
 * expired, a resubmission cannot enter all categories of the 
 * original submission, or the competition has no categories in
 * the city code of the store where the submission occurs.
 * 
 *
 * @version	$Revision: 1.2 $
 */
public class CannotCompeteException
    extends DBException
{
    /**
     * Constructs a CannotCompeteException instance.
     */
    public CannotCompeteException()
    {
	super();
    }

    /**
     * Constructs a CannotCompeteException instance.
     *
     * @param	message			the exception message
     */
    public CannotCompeteException(String message)
    {
	super(message);
    }

    /**
     * Constructs a CannotCompeteException instance.
     *
     * @param	message			the exception message
     * @param	throwable		the nested exception
     */
    public CannotCompeteException(String message, Throwable throwable)
    {
	super(message, throwable);
    }
}

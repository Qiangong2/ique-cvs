package com.broadon.trans.xs;

import java.sql.*;
import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;

import com.broadon.filter.authWrapper;

import com.broadon.trans.common.*;
import com.broadon.trans.user.Registration;
import com.broadon.trans.user.XmlTags;


public final class UserSync extends TransactionServlet implements XmlTags
{
    protected TransactionRequest readInputs(XMLResponse res,
					    HttpServletRequest req)
    {
	UserSyncRequest userReq = new UserSyncRequest();

	res.startDoc(SYNC_RESULT_TAG);

	res.printElement(DEPOT_VERSION_KEY, req.getParameter(DEPOT_VERSION_KEY));
	res.printElement(HR_ID_KEY, req.getParameter(HR_ID_KEY));

	userReq.bbID = Long.parseLong(req.getParameter(BB_ID_KEY));
	res.printElement(BB_ID_KEY, String.valueOf(userReq.bbID));

	return userReq;
    }


    protected void process(Connection conn, authWrapper req,
			   TransactionRequest transReq, XMLResponse res)
	throws SQLException
    {
	UserSyncRequest userReq = (UserSyncRequest) transReq;

	Registration register = new Registration(conn, userReq.bbID, null, true);

	if (! register.recordExists()) {
	    res.printExitStatus(StatusCode.XS_NO_REG_RECORD);
	    return;
	}
	res.printElement(REPLIED_EMAIL_KEY, register.getEmailAddress() +
			 register.getEmailDomain());
	res.printElement(USER_NAME_KEY, register.getName());
	res.printElement(ADDRESS_KEY, register.getAddress());

	SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
	res.printElement(BIRTHDAY_KEY, fmt.format(register.getBirthdate()));
	
	res.printElement(TELEPHONE_KEY, register.getTelephone());
	res.printElement(GENDER_KEY, register.getGender());
	res.printElement(BB_ID_KEY, String.valueOf(userReq.bbID));
	res.printElement(CREATE_DATE_KEY,
			 fmt.format(register.getCreateDate()));
	res.printElement(OTHER_INFO_KEY, register.getMisc());
	res.printExitStatus(StatusCode.SC_OK);
    }
    
}

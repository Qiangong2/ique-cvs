package com.broadon.trans.xs;

import java.sql.*;
import javax.servlet.http.*;

import com.broadon.exception.InvalidRequestException;
import com.broadon.util.TimeTrace;
import com.broadon.db.DBException;
import com.broadon.filter.*;

import com.broadon.trans.common.*;
import com.broadon.trans.etickets.*;
import com.broadon.trans.purchase.*;


/**
 * Servlet implementation that handles a general purpose purchase request.
 * Note that we allow derivations of this class for special purpose
 * purchase requests, such as operator-assisted purchase requests.
 */
public class Purchase extends TransactionServlet
{
    private static final String REQUEST_RESULT_TAG = "purchase_result";
    private static final String DEPOT_VERSION_KEY = "depot_version";
    private static final String BB_ID_KEY = "bb_id";
    private static final String BB_MODEL_KEY = "bb_model";
    private static final String KIND_OF_PURCHASE_KEY = "kind_of_purchase";
    private static final String TITLE_ID_KEY = "title_id";
    private static final String CONTENT_ID_KEY = "content_id";
    private static final String EUNITS_KEY = "eunits";
    private static final String ECARD_KEY = "ecard";
    private static final String ECARD_STATUS_KEY = "ecard_status";
    private static final String TIMESTAMP_KEY = "sync_timestamp";
    private static final String TID_KEY = "tid";
    private static final String ETICKET_IN_FULL_KEY = "eticket_in_full";
    private static final String FULL_SYNC_KEY = "full_sync";
    private static final String CLIENT_IP_KEY = "client_ip";
    private static final String MEMBERSHIP_ID_KEY = "membership_id";
    private static final String MEMBER_ID_KEY = "member_id";

    // Invalid member and membership identity
    //
    private static final long INVALID_ID = -1L;

    // Purchase kinds supported by this servlet
    //
    public final static String PURCHASE_KIND_UNLIMITED = "UNLIMITED";
    public final static String PURCHASE_KIND_TRIAL = "TRIAL";
    public final static String PURCHASE_KIND_RENTAL = "RENTAL";
    public final static String PURCHASE_KIND_BONUS = "BONUS";
    

    /**
     * Constructs an appropriate <code>PurchaseTitle</code> object.
     * @param bb BB player record.
     * @param hrID Depot that submits this purchase request.
     * @param storeID ID of the store to which the Depot belongs.
     * @param regionID Logical region where the BB Depot belongs.
     * @param kindOfPurchase One of the PURCHASE_XXX constants defined in
     * PurchaseTitle.
     * @param titleID Title to be purchased.
     * @param ecards One or more eCards for purchase payment (null for TRIAL).
     * @param eUnits Cost of this title (if cost is unknown, set this
     * to -1 and the correct value will be fetched from the database)
     * @param contendIDs One or more content ID that compose this
     * title.  If <code>null</code>, the content ID list will be
     * fetched from the database.
     * @param clientIP The client IP address of the member doing the purchase
     * @param membershipID The identity of the member doing the purchase
     * @param memberID The identity of the member doing the purchase
     * @exception InvalidRequestException The kindOfPurchase is not 
     * supported by this servlet.
     */
    protected PurchaseTitle 
    getPurchaseTitle(BBPlayer bb,
                     long     hrID, // BB id
                     int      storeID,
                     int      regionID,
                     String   kindOfPurchase,
                     long     titleID, 
                     String[] ecards,
                     int      eUnits, 
                     long[]   contentIDs,
                     String   clientIP,     // Use null when undefined
                     long     membershipID, // Use -1 when undefined
                     long     memberID)     // Use -1 when undefined
        throws InvalidRequestException
    {
        // This is a customer initiated request for purchase, where the
        // kinds of purchase supported are as listed below:
        //
        
        PurchaseTitle p = null;

        if (kindOfPurchase.equals(PURCHASE_KIND_UNLIMITED)) {
            p = new PurchaseTitleUnlimited(bb, hrID, storeID, regionID, 
                                           titleID, ecards, eUnits,
                                           contentIDs,
                                           clientIP, membershipID, memberID);
        }
        else if (kindOfPurchase.equals(PURCHASE_KIND_TRIAL)) {
            p = new PurchaseTitleTrial(bb, hrID, storeID, regionID, 
                                       titleID, contentIDs,
                                       clientIP, membershipID, memberID);
        }
        else if (kindOfPurchase.equals(PURCHASE_KIND_RENTAL)) {
            p = new PurchaseTitleRental(bb, hrID, storeID, regionID, 
                                        titleID, ecards, eUnits,
                                        contentIDs,
                                        clientIP, membershipID, memberID);
        }
        else {
            throw new InvalidRequestException("Customer is not allowed to" +
                                              " initate " +
                                              kindOfPurchase +
                                              " purchase");
        }
        return p;
    } // getPurchaseTitle


    protected void process(Connection conn, authWrapper req,
                           TransactionRequest transReq, XMLResponse res)
        throws SQLException
    {
        TimeTrace ttrace = getTimeTrace();
        BBPlayer bb = null;
        PurchaseRequest purchaseReq = (PurchaseRequest) transReq;

        ETicketSyncLog syncLog = new ETicketSyncLog(purchaseReq.bbID,
                                                    purchaseReq.depotID);
        PurchaseLog prchLog = new PurchaseLog(req.getRegionID(),
                                              purchaseReq.depotID,
                                              req.getStoreID(),
                                              purchaseReq.kindOfPurchase,
                                              purchaseReq.eUnits,
                                              purchaseReq.operatorID,
                                              purchaseReq.bbID,
                                              purchaseReq.titleID);
        auditLog.add(syncLog);
        auditLog.add(prchLog);

        PurchaseTitle pTitle = null;

        try {
            bb = new BBPlayer(purchaseReq.bbID, req.getBUID(), conn);
	    
            res.printElement(BB_MODEL_KEY, bb.getBBModel());

            if (ttrace!=null) ttrace.endInterval("purchase_setup");
            pTitle = getPurchaseTitle(bb,
                                      req.getDepotID(),
                                      req.getStoreID(),
                                      req.getRegionID(),
                                      purchaseReq.kindOfPurchase,
                                      purchaseReq.titleID,
                                      purchaseReq.ecards, // null for trial
                                      purchaseReq.eUnits, // null for trial
                                      purchaseReq.contentIDs,
                                      purchaseReq.clientIP,
                                      purchaseReq.membershipID,
                                      purchaseReq.memberID);
            if (ttrace!=null) ttrace.endInterval("purchase_title");

            conn.setAutoCommit(false);
	    
            // step 1: generate the purchase record 
            //         and update the purchase type.
            pTitle.process(conn, bb, auditLog, 
                           purchaseReq.operatorID, 
                           req.getBUID());
            prchLog.setPurchaseWith(pTitle.purchaseWith());

	    long[] cid = pTitle.getContentIDs();
	    for (int i = 0; i < cid.length; ++i)
		res.printElement(CONTENT_ID_KEY, String.valueOf(cid[i]));


            sendEcardStatus(res, pTitle.getECardStatus());
            if (ttrace!=null) ttrace.endInterval("purchase_record");

            // step 2: create eTicket(s)
            res.printElement(TIMESTAMP_KEY,
                             Long.toString(bb.getCurrentTime()));
            getTickets(conn, bb, purchaseReq, 
                       pTitle.getTicketIDs(), 
                       pTitle.getTicketRTypes(),
                       pTitle.getTicketLimits(),
		       pTitle.getContentIDs(),
                       res, 
                       req.getBUID());

            conn.commit();

            res.printExitStatus(StatusCode.SC_OK);

        } catch (InvalidBBIDException badBB) {
            // badBB.printStackTrace();
            res.printExitStatus(StatusCode.XS_BAD_BB_ID, badBB);

        } catch (InvalidRequestException ire) {
            // ire.printStackTrace();
            res.printExitStatus(StatusCode.XS_BAD_REQUEST, ire);
	    
        } catch (TitleAlreadyPurchasedException tpe) {
            // tpe.printStackTrace();
            res.printExitStatus(StatusCode.XS_DUPLICATED_PURCHASE, tpe);
	    
        } catch (ECardRedemptionException ere) {
            // ere.printStackTrace();
            sendEcardStatus(res, pTitle.getECardStatus());
            res.printExitStatus(StatusCode.XS_ECARD_ERROR, ere);
	    
        } catch (ETicketCreationException ece) {
            // ece.printStackTrace();
            res.printExitStatus(StatusCode.XS_ETICKET_SYNC, ece);
	    
        } catch (TrialAfterUseException taue) {
            // taue.printStackTrace();
            res.printExitStatus(StatusCode.XS_TRIAL_AFTER_USE, taue);

        } catch (DBException dbe) {
            dbe.printStackTrace();
            res.printExitStatus(StatusCode.SC_DB_EXCEPTION, dbe);

        } finally {
            conn.rollback();
	    
            String exit_code = res.getExitStatusCode();
            String exit_msg = res.getExitStatusMsg();
            syncLog.setStatus(exit_code, exit_msg);
            auditLog.add(syncLog);
            prchLog.setStatus(exit_code, exit_msg);
            auditLog.add(prchLog);
        }

    } // process


    // retrieve all eTickets owned by this BB.
    private void getTickets(Connection      conn, 
                            BBPlayer        bb,
                            PurchaseRequest req,
                            int[]           ticketIDs, 
                            int[]           ticketRTypes, 
                            short[]         ticketLimits,
			    long[]	    contentIDs,
                            XMLResponse     res, 
                            int             buID)
        throws ETicketCreationException
    {
        TimeTrace ttrace = getTimeTrace();
        try {
            ETickets tickets = new ETickets(conn, hsm, bb.getBBID(),
                                            bb.getBBKey(), regionalCenterID,
                                            req.minTID, buID);
            if (bb.getLastSync() == req.lastSync && req.lastSync != 0) {
                res.printElement(FULL_SYNC_KEY, "0");
                tickets.generate(contentIDs, 
                                 ticketIDs, 
                                 ticketRTypes, 
                                 ticketLimits,
                                 req.getEticketInFull);
                if (ttrace!=null) ttrace.endInterval("generateTicket");
            }
            else {
                res.printElement(FULL_SYNC_KEY, "1");
                tickets.generateAll(req.getEticketInFull);
                if (ttrace!=null) ttrace.endInterval("generateAllTickets");
            }
	    
            ETicketsFormatter out = new ETicketsFormatter(tickets, res);
            out.dumpTickets();
            if (ttrace!=null) ttrace.endInterval("formatTickets");
            out.dumpCerts();
            if (ttrace!=null) ttrace.endInterval("formatCerts");
        } catch (SQLException sql) {
            throw new ETicketCreationException(sql.getMessage());
        }

    } // getTickets
    

    private void sendEcardStatus(XMLResponse res, int[] status)
    {
        for (int i = 0; i < status.length; ++i) {
            res.printElement(ECARD_STATUS_KEY, String.valueOf(-status[i]));
        }
    }

    protected TransactionRequest readInputs(XMLResponse res,
                                            HttpServletRequest req)
    {
        String s;
        PurchaseRequest purchaseReq = new PurchaseRequest();

        // No operator involved in default purchasing
        //
        purchaseReq.userID = null;
        purchaseReq.password = null;
        purchaseReq.operatorID = -1;

        res.startDoc(REQUEST_RESULT_TAG);

        s = req.getParameter(DEPOT_VERSION_KEY);
        res.printElement(DEPOT_VERSION_KEY, s);
	
        s = req.getParameter(BB_ID_KEY);
        purchaseReq.bbID = Long.parseLong(s);
        res.printElement(BB_ID_KEY, s);

        s = req.getParameter(HR_ID_KEY);
        // Verify the format of the Depot ID
        if (s.length() != 14 || ! s.startsWith("HR")) {
            throw new NumberFormatException("Invalid Depot ID");
        }
        purchaseReq.depotID = Long.parseLong(s.substring(2), 16);
        res.printElement(HR_ID_KEY, s);

        purchaseReq.kindOfPurchase = req.getParameter(KIND_OF_PURCHASE_KEY);
        if (purchaseReq.kindOfPurchase == null) {
            purchaseReq.kindOfPurchase = PURCHASE_KIND_UNLIMITED;
        }
        else {
            res.printElement(KIND_OF_PURCHASE_KEY, purchaseReq.kindOfPurchase);
        }
	
        s = req.getParameter(TITLE_ID_KEY);
        purchaseReq.titleID = Long.parseLong(s);
        res.printElement(TITLE_ID_KEY, s);
	
        String[] cid = req.getParameterValues(CONTENT_ID_KEY);
        purchaseReq.contentIDs = new long[cid.length];
        for (int i = 0; i < cid.length; ++i) {
            purchaseReq.contentIDs[i] = Long.parseLong(cid[i]);
        }
	
        s = req.getParameter(EUNITS_KEY);
        if (s != null) {
            purchaseReq.eUnits = Integer.parseInt(s);
            res.printElement(EUNITS_KEY, s);
        }
        else {
            purchaseReq.eUnits = 0;
        }

        purchaseReq.ecards = req.getParameterValues(ECARD_KEY);
        if (purchaseReq.ecards != null) {
            for (int i = 0; i < purchaseReq.ecards.length; ++i) {
                res.printElement(ECARD_KEY, purchaseReq.ecards[i]);
            }
        }
	
        s = req.getParameter(TIMESTAMP_KEY);
        purchaseReq.lastSync = Long.parseLong(s);

        s = req.getParameter(TID_KEY);
        purchaseReq.minTID =
            (s == null) ? ETickets.MIN_TID : Integer.parseInt(s);
        res.printElement(TID_KEY, String.valueOf(purchaseReq.minTID));

        s = req.getParameter(ETICKET_IN_FULL_KEY);
        purchaseReq.getEticketInFull = (s != null && s.equals("1"));

        // Client identification 
        //
        purchaseReq.clientIP = req.getParameter(CLIENT_IP_KEY);
        s = req.getParameter(MEMBERSHIP_ID_KEY);
        if (s != null)
            purchaseReq.membershipID = Long.parseLong(s);
        else
            purchaseReq.membershipID = INVALID_ID;
        s = req.getParameter(MEMBER_ID_KEY);
        if (s != null)
            purchaseReq.memberID = Long.parseLong(s);
        else
            purchaseReq.memberID = INVALID_ID;

        return purchaseReq;
    } // readInputs
}

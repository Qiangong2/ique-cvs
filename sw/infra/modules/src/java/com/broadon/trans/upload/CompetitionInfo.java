package com.broadon.trans.upload;

import java.sql.*;
import com.broadon.exception.InvalidRequestException;
import com.broadon.trans.upload.CannotCompeteException;

/**
 * Read all competition info related to a competitionID and verify that it
 * is valid w.r.t. the request parameters.
 */
public class CompetitionInfo 
{
    // Request parameters
    //
    Connection conn;
    long       bbID;
    long       titleID;
    long       contentID;
    int        competitionID;
    int        storeID;
    long       currentTime; // UTC msecs

    // Results from DB requests and validation checks
    //
    private String cityCode = null;
    private String emailAddress = null;
    private long   competitionStartTime = 0; // UTC msecs
    private long   competitionEndTime = 0;   // UTC msecs
    private long   lastSubmissionTime = 0;   // UTC msecs

    private static final String UNIVERSAL_CITY_CODE = "*";
    private static final String TIMESTAMP = CompetitionLog.TimeConvStr();
    private static final String selectCompetitionInfo = 
    "SELECT " +
    " s.city_code, " +
    " cr.email_address, " +
    " BBXML.ConvertTimeStamp(c.start_date) comp_start_date, " +
    " BBXML.ConvertTimeStamp(c.end_date) comp_end_date " +
    "FROM " +
    " stores s, " +
    " competitions c, " +
    " customer_registrations cr, " +
    " content_title_objects cto " +
    "WHERE " +
    " cr.bb_id=? AND " +
    " s.store_id=? AND " +
    " c.competition_id=? AND " +
    " c.title_id=? AND " +
    " cto.content_id=? AND " +
    " cto.title_id=c.title_id";

    private static final String selectLastSubmissionTime =
    "SELECT BBXML.ConvertTimeStamp(submit_date) " +
    "FROM competing_customers " +
    "WHERE email_address=? AND competition_id=?";

    private static final String selectCompetingCategories =
    "SELECT BBXML.ConvertTimeStamp(start_date), " +
    "       BBXML.ConvertTimeStamp(end_date )" +
    "FROM competing_categories " +
    "WHERE competition_id=? AND " +
    "      (city_code IS NULL OR city_code=?) AND " +
    "      (start_date IS NULL OR start_date <= " + TIMESTAMP + ") AND" +
    "      (end_date IS NULL OR end_date >= " + TIMESTAMP + ")";

    // Disable construction other than through readCompetitionInfo
    //
    private CompetitionInfo(Connection conn,
                            long       bbID,
                            long       titleID,
                            long       contentID,
                            int        competitionID,
                            int        storeID,
                            long       currentTime) 
    {
        this.conn = conn;
        this.bbID = bbID;
        this.titleID = titleID;
        this.contentID = contentID;
        this.competitionID = competitionID;
        this.storeID = storeID;
        this.currentTime = currentTime;
        this.lastSubmissionTime = currentTime;
    }


    private void getCompetitionInfo()
        throws SQLException
    {
        PreparedStatement ps = conn.prepareStatement(selectCompetitionInfo);
        ps.setLong(1, bbID);
        ps.setInt(2, storeID);
        ps.setInt(3, competitionID);
        ps.setLong(4, titleID);
        ps.setLong(5, contentID);
        try {
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                cityCode = rs.getString(1);
                if (cityCode == null)
                    cityCode = UNIVERSAL_CITY_CODE;
                emailAddress = rs.getString(2);
                competitionStartTime = rs.getLong(3);
                competitionEndTime = rs.getLong(4);
            }
            rs.close();
        } finally {
            ps.close();
        }
    } // getCompetitionInfo


    private void getLastSubmissionInfo()
        throws SQLException
    {
        // Must be called after getCompetitionInfo
        //
        PreparedStatement ps = conn.prepareStatement(selectLastSubmissionTime);
        ps.setString(1, emailAddress);
        ps.setLong(2, competitionID);
        try {
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                lastSubmissionTime = rs.getLong(1);
            }
            rs.close();
        } finally {
            ps.close();
        }
    } // getLastSubmissionInfo
        

    private void verifyCompetition()
        throws CannotCompeteException, InvalidRequestException, SQLException
    {
        // Gets the relevant information about the store from which the
        // submission is made, the customer making the submission, the 
        // competition entered, and any previous submission to this
        // competition.  Then verifies that it is ok for the
        // customer to enter this competition.
        //
        getCompetitionInfo();
        getLastSubmissionInfo();

        // Did we get valid competition information
        //
        if (cityCode == null ||
            emailAddress == null ||
            competitionStartTime <= 0) {
            throw new InvalidRequestException(
                "BB " + bbID + 
                " cannot enter competition " + competitionID +
                " at store " + storeID +
                ": " + 
                (lastSubmissionTime != currentTime?
                 "Re-submission failed" : "Submission failed"));
        }

        // Is the competition still active?
        //
        if (currentTime < competitionStartTime ||
            (competitionEndTime != 0 && currentTime > competitionEndTime)) {
            throw new CannotCompeteException(
                "Competition (" + competitionID + ") has ended: " +
                (lastSubmissionTime != currentTime?
                 "Re-submission failed" : "Submission failed"));
        }
    } // verifyCompetition


    private void verifyCompetingCategories() 
        throws CannotCompeteException, InvalidRequestException, SQLException
    {
        // Returns true if all the categories selected in a former submission
        // are still valid at the current time, or when there is no former
        // submission.
        //
        // Precondition: verifyCompetition has been called and did not fail.
        //
        PreparedStatement ps = 
            conn.prepareStatement(selectCompetingCategories);
        ps.setLong(1, competitionID);
        ps.setString(2, cityCode);
        ps.setLong(3, lastSubmissionTime);
        ps.setLong(4, lastSubmissionTime);
        try {
            ResultSet rs = ps.executeQuery();
            if (!rs.next()) {
                throw new CannotCompeteException(
                    "No valid competing category for competition (" + 
                    competitionID + "): " +
                    (lastSubmissionTime != currentTime?
                     "Re-submission failed" : "Submission failed"));
            }
            else do {
                final long cstart = rs.getLong(1);
                final long cend = rs.getLong(2);
                if ((cstart != 0 && currentTime < cstart) ||
                    (cend != 0 && currentTime > cend)) {
                    //
                    // This can only occur if an entry for the same
                    // competition is entered more than once, and a category
                    // has expired since the initial submission.
                    //
                    throw new CannotCompeteException(
                        "Resubmission to competition (" + competitionID +
                        "): one or more competing category has expired");
                }
            } while (rs.next());
            rs.close();
        } finally {
            ps.close();
        }
    } // verifyCompetingCategories


    public static CompetitionInfo 
    readCompetitionInfo(Connection conn,
                        long       bbID,
                        long       titleID,
                        long       contentID,
                        int        competitionID,
                        int        storeID,
                        long       currentTime)
        throws CannotCompeteException, InvalidRequestException, SQLException
    {
        CompetitionInfo ci = new CompetitionInfo(conn,
                                                 bbID,
                                                 titleID,
                                                 contentID,
                                                 competitionID,
                                                 storeID,
                                                 currentTime);

        ci.verifyCompetition();
        ci.verifyCompetingCategories();
        return ci;
    } // readCompetitionInfo


    // Accessor functions
    //
    public boolean isUniversalCityCode() 
    {
        return cityCode == UNIVERSAL_CITY_CODE;
    }
    public String getEmailAddress() {return emailAddress;}
    public String getCityCode() {return cityCode;}
    public long   getCurrentTime() {return currentTime;}
    public int    getStoreID() {return storeID;}
}

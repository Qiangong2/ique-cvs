package com.broadon.trans.ems;

import java.sql.*;

import com.broadon.jni.BBcrypto;
import com.broadon.db.DBException;

public class GenTickets
{
    protected long[] contentID;
    protected int[] ticketID;
    protected int[] LRType;	// limited rights type
    protected short[] limit;
    String[] LRTypeString;

    static final String getCID =
	"SELECT RTYPE, LIMITS, CONTENT_ID FROM BB_BUNDLE_DETAILS_V WHERE " +
	"    BB_MODEL=? AND BU_ID=? AND START_DATE=? AND REVOKE_DATE IS NULL" +
	"    AND REPLACED_CONTENT_ID IS NULL AND (END_DATE IS NULL OR " +
	"                                         END_DATE >= SYSDATE)";

    static final String updateBB =
	"UPDATE BB_PLAYERS SET LAST_PR_TID=?, LAST_LR_TID=? WHERE BB_ID=?";

    static final String genTicket =
	"{ ? = call etops.etcreate(?, ?, ?, 'CTB', ?, ?) }";
    
    protected GenTickets(Connection conn, NewBB bb, int tid)
	throws SQLException, DBException
    {
	getTicketInfo(conn, bb, tid);
	if (contentID.length == 0)
	    throw new DBException("Invalid bundle");
	createTickets(conn, bb.bbID);
    }

    
    void getTicketInfo(Connection conn, NewBB bb, int tid)
	throws SQLException
    {
	PreparedStatement ps =
	    conn.prepareStatement(getCID,
				  ResultSet.TYPE_SCROLL_INSENSITIVE,
				  ResultSet.CONCUR_READ_ONLY);
	ps.setString(1, bb.model);
	ps.setInt(2, bb.buID);
	ps.setDate(3, bb.bundleDate);
	try {
	    ResultSet rs = ps.executeQuery();

	    int size;
	    if (rs.last()) {
		size = rs.getRow();
		rs.beforeFirst();
	    } else
		size = 0;

	    contentID = new long[size];
	    ticketID = new int[size];
	    LRType = new int[size];
	    LRTypeString = new String[size];
	    limit = new short[size];

	    int prtid = 0;
	    int lrtid = tid;

	    for (int i = 0; i < size && rs.next(); ++i) {
		contentID[i] = rs.getLong(3);
		LRTypeString[i] = rs.getString(1);
		LRType[i] = BBcrypto.parseRType(LRTypeString[i]);
		if (LRType[i] == BBcrypto.PR) {
		    // permanent rights
		    ticketID[i] = prtid++;
		    limit[i] = 0;
		} else {
		    // limited rights
		    ticketID[i] = lrtid++;
		    limit[i] = rs.getShort(2);
		}
	    }
	    ps.close();

	    // update the TIDs
	    ps = conn.prepareStatement(updateBB);
	    ps.setInt(1, prtid);
	    ps.setInt(2, lrtid);
	    ps.setLong(3, bb.bbID);
	    ps.executeUpdate();
	} finally {
	    ps.close();
	}
    }

    
    void createTickets(Connection conn, long bbID)
	throws SQLException
    {
	CallableStatement cs = conn.prepareCall(genTicket);
	cs.registerOutParameter(1, Types.VARCHAR);
	cs.setLong(2, bbID);
	try {
	    for (int i = 0; i < contentID.length; ++i) {
		int j = 2;
		cs.setLong(++j, contentID[i]);
		cs.setInt(++j, ticketID[i]);
		cs.setString(++j, LRTypeString[i]);
		cs.setShort(++j, limit[i]);

		cs.executeUpdate();
		String status = cs.getString(1);
		if (status == null || !status.startsWith("ETCR_"))
		    throw new SQLException("Cannot create eTicket record.");
	    }
	} finally {
	    cs.close();
	}
    }
}

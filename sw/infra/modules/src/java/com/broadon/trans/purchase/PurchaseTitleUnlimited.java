package com.broadon.trans.purchase;

import java.sql.*;

import com.broadon.jni.BBcrypto;
import com.broadon.db.DBException;
import com.broadon.exception.*;
import com.broadon.trans.common.BBPlayer;
import com.broadon.trans.common.AuditLog;

/**
 * Database interface to handle purchasing of a game title for unlimited play.
 * See the description of Purchase title for a description of methods redefined
 * in this subclass.
 * @version 
 * @author 
 */
public class PurchaseTitleUnlimited extends PurchaseTitle
{

    // input parameters
    
    final String[] ecards;	// eCards used for this purchase
    final int      eUnits;  // price of this purchase (optional)

    // output parameters
    
    int[] ecardStatus;		// eCards redemption status


    protected void generateTicketProperties(BBPlayer  bb, 
                                            TitleInfo tinfo)
    {
        final short limit = 0;
        final int   rtype = BBcrypto.PR;

        ticketIDs = new int[getContentIDs().length];
        ticketRTypes = new int[getContentIDs().length];
        ticketLimits = new short[getContentIDs().length];
        int tid = bb.getPermanentTID();
        for (int i = 0; i < ticketIDs.length; ++i) {
            if (++tid > MAX_PERMANENT_TICKET_ID)
                tid = MIN_PERMANENT_TICKET_ID;
            ticketIDs[i] = tid;
            ticketLimits[i] = limit;
            ticketRTypes[i] = rtype;
        }
        bb.setPermanentTID(tid);
    }


    /**
     * Construct a <code>PurchaseTitleUnlimited</code> object.
     * @param bb BB player record.
     * @param titleID Title to be purchased.
     * @param hrID Depot that submits this purchase request.
     * @param storeID ID of the store to which the Depot belongs.
     * @param regionID Logical region where the BB Depot belongs.
     * @param ecards One or more eCards for purchase payment.
     * @param eUnits Cost of this title (if cost is unknown, set this
     * to -1 and the correct value will be fetched from the database).
     * @param contendIDs One or more content ID that compose this
     * title.
     * @param clientIP The client IP address of the member doing the purchase
     * @param membershipID The identity of the member doing the purchase
     * @param memberID The identity of the member doing the purchase
     */
    public PurchaseTitleUnlimited(BBPlayer bb,
                                  long     hrID, // BB id
                                  int      storeID,
                                  int      regionID,
                                  long     titleID, 
                                  String[] ecards,
                                  int      eUnits, 
                                  long[]   contentIDs,
                                  String   clientIP,     // null when undefined
                                  long     membershipID, // -1 when undefined
                                  long     memberID)     // -1 when undefined
    {
        super(bb, hrID, storeID, regionID, titleID, contentIDs,
              clientIP, membershipID, memberID);
        this.ecards = ecards;
        this.eUnits = eUnits;
        ecardStatus = null;
    }


    protected void validateContentInfo(TitleInfo        tinfo,
                                       TitleContentInfo tcf,
                                       long             cID, 
                                       Object           cinf) throws
        InvalidRequestException,
        TitleAlreadyPurchasedException,
        TrialAfterUseException
    {
        super.validateContentInfo(tinfo, tcf, cID, cinf);

        // Ensure that no other permanent purchase of this content is in 
        // effect.
        //
        if (tcf.etkExists(cinf) &&
            tcf.etkRtype(cinf).equals(TitleContentInfo.RTYPE_UNLIMITED)) {
            throw new TitleAlreadyPurchasedException("Title ID = " + 
                                                     getTitleID() + 
                                                     " ContentName = " +
                                                     tcf.coName(cinf));
        }
    }


    protected void validateTitleInfo(TitleInfo tinfo) throws
        InvalidRequestException,
        TitleAlreadyPurchasedException,
        TrialAfterUseException
    {
        super.validateTitleInfo(tinfo);

        // Verify that the price matches the eUnits submitted
        //
        if (tinfo.getPrice() < 0) {
            throw new InvalidRequestException("Price unavailable for title.");
        }
        else if (eUnits != -1 && eUnits != tinfo.getPrice())
            throw new InvalidRequestException("Inconsistent price");
    }


    protected int getPurchasePrice(TitleInfo tinfo)
    {
        return tinfo.getPrice();
    }

    protected String getPurchaseRtype(TitleInfo tinfo)
    {
        return TitleContentInfo.RTYPE_UNLIMITED;
    }

    protected void payForPurchase(Connection conn, 
                                  TitleInfo  tinfo,
                                  AuditLog   auditLog) throws
        SQLException, 
        DBException, 
        ECardRedemptionException
    {
        RedeemECard rcards = 
            RedeemECard.redeem(conn, getBbID(), getHrID(), getTitleID(),
                               ecards, tinfo.getPrice(), auditLog);
        ecardStatus = rcards.getCardStatus();
        if (rcards.getValue() < tinfo.getPrice()) {
            // not enough money
            throw new ECardRedemptionException();
        }
    }


    public String purchaseWith()
    {
        return "ECARD";
    }

    
    protected void generateEticketRecord(Connection conn, 
                                         TitleInfo  tinfo,
                                         int []     ticketIDs)
	throws SQLException, DBException
    {
        ETicketRecord.generate(conn, 
                               getBbID(), 
                               getContentIDs(), 
                               ticketIDs,
                               TitleContentInfo.RTYPE_UNLIMITED,
                               0,      // No limits for unlimited license
                               false); // Not a test
    }


    public int[] getECardStatus() 
    {
        return (ecardStatus == null) ? new int[0] : ecardStatus;
    }

}

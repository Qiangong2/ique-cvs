package com.broadon.trans.xs;

import java.sql.*;
import javax.servlet.http.*;

import com.broadon.filter.*;
import com.broadon.servlet.ServletConstants;

import com.broadon.trans.common.*;
import com.broadon.trans.etickets.*;
import com.broadon.trans.purchase.ETicketSyncLog;


/**
 * Servlet implementation that handles a eTicket sync request.
 */
public class SyncTickets extends TransactionServlet implements ServletConstants
{
    static final String REQUEST_RESULT_TAG = "eticket_sync_result";
    static final String DEPOT_VERSION_KEY = "depot_version";
    static final String BB_ID_KEY = "bb_id";
    static final String BB_MODEL_KEY = "bb_model";
    static final String TIMESTAMP_KEY = "sync_timestamp";
    static final String TID_KEY = "tid";
    static final String ETICKET_IN_FULL_KEY = "eticket_in_full";
    
    protected void process(Connection conn, authWrapper req,
			   TransactionRequest transReq, XMLResponse res)
	throws SQLException
    {
	ETicketSyncLog log = null;

	try {
	    SyncRequest syncReq = (SyncRequest) transReq;

	    log = new ETicketSyncLog(syncReq.bbID, syncReq.depotID);
	    auditLog.add(log);

	    BBPlayer bb = new BBPlayer(syncReq.bbID, req.getBUID(), conn);
	    long lastSync = bb.getLastSync();

	    res.printElement(TIMESTAMP_KEY, Long.toString(lastSync));
	    
	    if (lastSync == syncReq.lastSync && syncReq.lastSync != 0) {
		// don't send the eTickets if the BB Player is already in sync.
		res.printExitStatus(StatusCode.XS_ETICKET_IN_SYNC);
		return;
	    } 

	    res.printElement(BB_MODEL_KEY, bb.getBBModel());

	    ETickets tickets = new ETickets(conn, hsm, bb.getBBID(),
					    bb.getBBKey(), regionalCenterID,
					    syncReq.minTID, req.getBUID());
	    tickets.generateAll(syncReq.getEticketInFull);
	    conn.close();	// don't hold on the connection, the
				// rest could take a long time if the
				// client is on a slow network link.
	    ETicketsFormatter out = new ETicketsFormatter(tickets, res);
	    out.dumpTickets();
	    out.dumpCerts();
	    res.printExitStatus(StatusCode.SC_OK);
	} catch (InvalidBBIDException badBB) {
	    res.printExitStatus(StatusCode.XS_BAD_BB_ID, badBB);    
	} catch (ETicketCreationException e) {
	    res.printExitStatus(StatusCode.XS_ETICKET_SYNC, e);
	} finally {
	    log.setStatus(res.getExitStatusCode(), res.getExitStatusMsg());
	    auditLog.add(log);
	}
    } // process


    protected TransactionRequest readInputs(XMLResponse res,
					    HttpServletRequest req)
    {
	String s;
	SyncRequest syncReq = new SyncRequest();

	res.startDoc(REQUEST_RESULT_TAG);

	s = req.getParameter(DEPOT_VERSION_KEY);
	res.printElement(DEPOT_VERSION_KEY, s);
	
	s = req.getParameter(BB_ID_KEY);
	syncReq.bbID = Long.parseLong(s);
	res.printElement(BB_ID_KEY, s);

	// Get and verify the format of the Depot ID
	s = req.getParameter(HR_ID_KEY);
    if (s.length() != 14 || ! s.startsWith("HR")) {
        throw new NumberFormatException("Invalid Depot ID");
    }
    syncReq.depotID = Long.parseLong(s.substring(2), 16);
	res.printElement(HR_ID_KEY, s);

	s = req.getParameter(TIMESTAMP_KEY);
	syncReq.lastSync = Long.parseLong(s);
	
	s = req.getParameter(TID_KEY);
	syncReq.minTID = (s == null) ? ETickets.MIN_TID : Integer.parseInt(s);
	res.printElement(TID_KEY, String.valueOf(syncReq.minTID));

	s = req.getParameter(ETICKET_IN_FULL_KEY);
    syncReq.getEticketInFull = (s != null && s.equals("1"));

	return syncReq;
	
    } // readInputs
}

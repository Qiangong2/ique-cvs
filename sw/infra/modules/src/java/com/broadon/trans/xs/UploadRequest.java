package com.broadon.trans.xs;

import com.broadon.trans.common.TransactionRequest;

public class UploadRequest extends TransactionRequest
{
    protected long bbID;

    protected long titleID;

    protected long contentID;

    protected int competitionID;

    /** Raw data submitted from the BB player. */
    protected byte[] data = null;

    /** signature corresponding to data. */
    protected String signature;

}

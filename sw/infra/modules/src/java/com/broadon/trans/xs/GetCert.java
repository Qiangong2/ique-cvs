package com.broadon.trans.xs;

import java.io.*;
import java.security.GeneralSecurityException;
import java.security.cert.*;
import java.security.interfaces.RSAPublicKey;
import java.sql.*;

import javax.servlet.http.*;

import com.broadon.db.DBException;
import com.broadon.filter.authWrapper;
import com.broadon.security.CertInfo;
import com.broadon.security.X509;

import com.broadon.trans.common.*;
import com.broadon.trans.installer.DepotConfigLog;
import com.broadon.trans.installer.Depot;


/**
 * Servlet implementation that handles a Depot certificate request.
 */
public final class GetCert extends TransactionServlet
{
    static final String REQUEST_RESULT_TAG = "cert_issue_result";
    static final String DEPOT_VERSION_KEY = "depot_version";
    static final String USER_ID_KEY = "user_id";
    static final String PASSWORD_KEY = "password";
    static final String STORE_ID_KEY = "store_id";
    static final String CERT_KEY = "x509_cert";
    static final String CHAIN_ID_KEY = "ca_chain";
    static final String PRIVATE_KEY_KEY = "private_key";
    static final String KEY_PASSWD_KEY = "private_key_pw";


    protected TransactionRequest readInputs(XMLResponse res,
					    HttpServletRequest req)
    {
	String s;

	GetCertRequest certReq = new GetCertRequest();

	res.startDoc(REQUEST_RESULT_TAG);

	s = req.getParameter(DEPOT_VERSION_KEY);
	res.printElement(DEPOT_VERSION_KEY, s);

	certReq.userID = req.getParameter(USER_ID_KEY);
	res.printElement(USER_ID_KEY, certReq.userID);

	certReq.password = req.getParameter(PASSWORD_KEY);
	res.printElement(PASSWORD_KEY, certReq.password);

	s = req.getParameter(STORE_ID_KEY);
	certReq.storeID = Integer.parseInt(s);
	res.printElement(STORE_ID_KEY, s);

	s = req.getParameter(HR_ID_KEY);
	// Verify the format of the Depot ID
	if (s.length() != 14 || ! s.startsWith("HR")) {
	    throw new NumberFormatException("Invalid Depot ID");
	}
	certReq.encodedDepotID = s;
	certReq.depotID = Long.parseLong(s.substring(2), 16);
	res.printElement(HR_ID_KEY, s);

	return certReq;
    } // readInputs


    protected void process(Connection conn, authWrapper req,
			   TransactionRequest transReq, XMLResponse res)
	throws SQLException
    {
	GetCertRequest certReq = (GetCertRequest) transReq;

	DepotConfigLog log = new DepotConfigLog(certReq.depotID,
						certReq.storeID);
	auditLog.add(log);

	try {
	    // validate password or smart card
	    try {
		int buID = getBUID(conn, certReq.storeID);
		ServiceTech.validate(conn, buID,
				     certReq.userID, certReq.password,
				     req, ServiceTech.installer);
	    } catch (GeneralSecurityException e) {
		res.printExitStatus(StatusCode.XS_UNAUTH_TECH);
		return;
	    }

	    // generate a new cert.
	    CertInfo cert = hsm.getDepotCert(certReq.encodedDepotID);

	    String pubKey = getPublicKey(cert.getCert());

	    // update the database record
	    conn.setAutoCommit(false);
	    Depot.insert(conn, certReq.depotID, certReq.storeID, pubKey);

	    res.printElement(CERT_KEY, new String(cert.getCert()));
	    res.printElement(PRIVATE_KEY_KEY, new String(cert.getPrivateKey()));
	    res.printElement(KEY_PASSWD_KEY, new String(cert.getPassword()));
	
	    // get CA chain
	    String[] chain = getChain(cert.getChain());
	    res.printElement(CHAIN_ID_KEY, chain[0]);
	    res.printElement(CHAIN_ID_KEY, chain[1]);

	    // no error found, commit.
	    conn.commit();

	    res.printExitStatus(StatusCode.SC_OK);
	} catch (DBException e) {
	    res.printExitStatus(StatusCode.SC_DB_EXCEPTION, e);
	} catch (IOException e) {
	    res.printExitStatus(StatusCode.XS_CERT_GEN, e);
	} catch (CertificateException e) {
	    res.printExitStatus(StatusCode.XS_CERT_GEN, e);
	} finally {
	    conn.rollback();
	    log.setStatus(res.getExitStatusCode(), res.getExitStatusMsg());
	    auditLog.add(log);
	}
    }


    static final String readBUID =
	"SELECT B.BU_ID FROM STORES A, REGIONS B " +
	"WHERE A.SUSPEND_DATE IS NULL AND A.REVOKE_DATE IS NULL AND " +
	"      A.REGION_ID = B.REGION_ID AND A.STORE_ID = ?";

    int getBUID(Connection conn, int storeID)
	throws SQLException, DBException
    {
	PreparedStatement ps = conn.prepareStatement(readBUID);
	ps.setInt(1, storeID);
	try {
	    ResultSet rs = ps.executeQuery();
	    if (rs.next()) {
		return rs.getInt(1);
	    }
	    throw new DBException("Invalid store ID");
	} finally {
	    ps.close();
	}
    }

    String[] getChain(byte[] rawChain)
    {
	String chain = new String(rawChain);
	String[] results = chain.split(X509.X509_FOOTER);
	for (int i = 0; i < results.length; ++i)
	    results[i] += X509.X509_FOOTER;
	return results;
    }


    String getPublicKey(byte[] rawCert)
	throws CertificateException
    {
	CertificateFactory cf = CertificateFactory.getInstance("X.509");
	X509Certificate x509Cert = (X509Certificate)
	    cf.generateCertificate(new ByteArrayInputStream(rawCert));
	RSAPublicKey key = (RSAPublicKey) x509Cert.getPublicKey();
	return key.getModulus().toString(32);
    }
}

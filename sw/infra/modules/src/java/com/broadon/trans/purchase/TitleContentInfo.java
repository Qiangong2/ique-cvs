package com.broadon.trans.purchase;

import java.sql.*;
import java.util.*;
import com.broadon.db.DBException;

/**
 * Read all content info related to a Title, bbID and bb-model for
 * purchasing purpose.
 */
public class TitleContentInfo {

    public static final String RTYPE_UNLIMITED = "PR";
    public static final String RTYPE_TIME_LIMITED = "TR";
    public static final String RTYPE_NPLAY_LIMITED = "LR";
    public static final String RTYPE_XFUNC_LIMITED = "XR";

    private static final String getContentInfo =
    "SELECT" +
    "   cto.content_id,"   +
    "   BBXML.ConvertTimeStamp(cto.revoke_date) cto_revoke_date," +
    "   DECODE (et.content_id, NULL, 'N', 'Y') etk_exist,"   +
    "   et.rtype etk_rtype,"   +
    "   DECODE (et.revoke_date, NULL, 'N', 'Y') etk_revoked, " +
    "   co.content_object_name,"   +
    "   co.content_object_version,"   +
    "   co.content_object_type,"   +
    "   ctops.is_content_object_latest (co.content_id,"   +
    "                                   co.content_object_name,"   +
    "                                   co.content_object_version,"   +
    "                                   cto.title_id,co.revoke_date"   +
    "                                  ) is_content_object_latest " +
    "FROM content_title_objects cto,"   +
    "   content_objects co,"   +
    "   content_eticket_metadata cem,"   +
    "   etickets et "   +
    "WHERE cto.content_id = co.content_id"   +
    "   AND co.content_id = cem.content_id"   +
    "   AND cto.title_id = ?"   +
    "   AND et.bb_id (+) = ?"   +
    "   AND et.content_id (+) = cto.content_id"   +
    "   AND co.content_object_type IN"   +
    "   (SELECT content_object_type " +
    "    FROM xs.bb_content_object_types WHERE bb_model = ?)"   +
    "   AND co.revoke_date is NULL";

    private static class ContentInfo {
        boolean etk_exists;
	boolean etk_revoked;
        boolean co_is_latest;
        long    cto_revoke_date; // UTC time in seconds
        int     co_version;
        String  etk_rtype;
        String  co_name;
        String  co_type;
    }


    private TreeMap contentInfo;
    private long[] existingContentIDs;


    // Disable construction other than through readContentInfo
    //
    private TitleContentInfo(TreeMap contentInfo, long[] existingContentIDs)
    {
        this.contentInfo = contentInfo;
	this.existingContentIDs = existingContentIDs;
    }

    
    /**
     * The factory for <code>TitleContentInfo</code> objects.  Reads
     * information from various DB tables, exposing a collection of
     * values relevant to purchasing of the content_ids for the given
     * title.  Revoked content objects are not included.
     * @param conn Database connection.
     * @param bbID ID of the BB player.
     * @param titleID ID of the title.
     * @param model BB player model.
     * @return A TitleContentInfo object.
     * @exception SQLException
     * @exception DBException
     */
    public static TitleContentInfo readContentInfo(Connection conn, 
                                                   long       bbID,
                                                   long       titleID,
                                                   String     model)
        throws SQLException, DBException
    {
        PreparedStatement ps = conn.prepareStatement(getContentInfo);
        TreeMap           contentMap = new TreeMap();
	ArrayList	  cids = new ArrayList();
        ps.setLong(1, titleID);
        ps.setLong(2, bbID);
        ps.setString(3, model);
        try {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ContentInfo c = new ContentInfo();
                Long        cID  = new Long(rs.getLong(1));
                c.cto_revoke_date= rs.getLong(2)/1000;
                c.etk_exists     = (rs.getString(3).equals("Y")? true : false);
                c.etk_rtype      = (c.etk_exists? 
                                    rs.getString(4) : RTYPE_UNLIMITED);
		c.etk_revoked	 = ("Y".equals(rs.getString(5)) ? true : false);
                c.co_name        = rs.getString(6);
                c.co_version     = rs.getInt(7);
                c.co_type        = rs.getString(8);
                c.co_is_latest   = (rs.getString(9).equals("Y")? true : false);
                contentMap.put(cID, c);
		if (c.etk_exists && ! c.etk_revoked)
		    cids.add(cID);
            }
            rs.close();
        } finally {
            ps.close();
        }

        if (contentMap.isEmpty()) {
            throw new DBException("Missing Content ID for Title=" + titleID);
        }
	long[] contentIDs = null;
	if (cids.size() > 0) {
	    contentIDs = new long[cids.size()];
	    for (int i = 0; i < contentIDs.length; ++i) {
		contentIDs[i] = ((Long) cids.get(i)).longValue();
	    }
	}
        return new TitleContentInfo(contentMap, contentIDs);
    } // readContentInfo

    
    public boolean contains(long contentID)
    {
        return contentInfo.containsKey(new Long(contentID));
    }

    /**
     * Gets the purchasing values associated with a given contentID.
     * Use the content info accessor methods to access a non-null value.
     * @return the information associated with the given contentID or null
     * if no such value exists.  This is the same kind of object returned
     * by <code>getInfo()</code>.
     */
    public Object get(long contentID)
    {
        return contentInfo.get(new Long(contentID));
    }

    /**
     * @return the content IDs of any existing eTickets.
     */
    public long[] getExistingContentIDs() {
	return existingContentIDs;
    }

    // --------------------------------------------------------------
    // Iterating over information about each content object that has
    // not been revoked and belongs to the title/region.
    // --------------------------------------------------------------

    /**
     * Gets an iterator over the contentIDs found for the title.  
     * Use getInfo and getContentId to access the respective info and
     * contentID objects.  Use the info accessor methods on the info object
     * to access info values. For example:
     * <code>
     *     ContentTitleInfo t = ...
     *     Iterator         i = t.iterator();
     *     while (i.hasNext()) {
     *        Object e = i.next();
     *        Object v = t.getInfo(e);
     *        long   contentID = t.getContentID(e);
     *        String rtype = t.getRtype(v);
     *        ....
     *     }
     * </code>
     * @return the iterator over all contentIDs for this title.
     */
    public Iterator iterator()
    {
        return contentInfo.entrySet().iterator();
    }
    public long getContentID(Object iterated)
    {
        final Map.Entry e = (Map.Entry)iterated;
        return ((Long)e.getKey()).longValue();
    }
    public Object getInfo(Object iterated)
    {
        final Map.Entry e = (Map.Entry)iterated;
        return e.getValue();
    }

    // --------------------------------------------------------------
    // Accessor methods for the object returned by get() or getInfo()
    // --------------------------------------------------------------

    /**
     * @return True if an eticket existed for this contentID.
     */
    public boolean etkExists(Object info) {
        return ((ContentInfo)info).etk_exists; }
    /**
     * @return True if an eticket exists but has been revoked for this contentID.
     */
    public boolean etkRevoked(Object info) {
        return ((ContentInfo)info).etk_revoked; }
    /**
     * @return True if this is the latest content version for the title.
     */
    public boolean coIsLatest(Object info) {
        return ((ContentInfo)info).co_is_latest; }
    /**
     * @return The UTC time when this contents grace period ends. Zero when
     * no grace period is defined.
     */
    public long ctoRevokeDate(Object info) {
        return ((ContentInfo)info).cto_revoke_date; }
    /**
     * @return The version of this content object.
     */
    public int coVersion(Object info) {
        return ((ContentInfo)info).co_version; }
    /**
     * @return The eticket licence type as defined by the RTYPE values
     * defined in this class.
     */
    public String etkRtype(Object info) {
        return ((ContentInfo)info).etk_rtype; }
    /**
     * @return The name of this content object (e.g. Star Fox (Chinese))
     */
    public String coName(Object info) {
        return ((ContentInfo)info).co_name; }
    /**
     * @return The type of this content object (e.g. GAME).
     */
    public String coType(Object info) {
        return ((ContentInfo)info).co_type; }
    
}

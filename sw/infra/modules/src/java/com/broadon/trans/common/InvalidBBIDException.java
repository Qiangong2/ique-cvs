package com.broadon.trans.common;

import com.broadon.db.DBException;

/**
 * The <code>InvalidBBIDException</code> class is thrown when
 * an online transaction specifies an invalid or revoked BB ID.
 * 
 *
 * @version	$Revision: 1.2 $
 */
public class InvalidBBIDException extends DBException
{
    /**
     * Constructs a InvalidBBIDException instance.
     */
    public InvalidBBIDException()
    {
	super();
    }

    /**
     * Constructs a InvalidBBIDException instance.
     *
     * @param	message			the exception message
     */
    public InvalidBBIDException(String message)
    {
	super(message);
    }

    /**
     * Constructs a InvalidBBIDException instance.
     *
     * @param	message			the exception message
     * @param	throwable		the nested exception
     */
    public InvalidBBIDException(String message, Throwable throwable)
    {
	super(message, throwable);
    }
}

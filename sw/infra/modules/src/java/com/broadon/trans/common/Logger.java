package com.broadon.trans.common;

import java.util.Date;
import java.sql.SQLException;
import java.sql.Connection;

/**
 * Base class of all Audit Log classes.  A separate
 * thread reads from a queue of objects of its sub-classes and
 * flush the log info to the database.
 */
public abstract class Logger
{
    /** Time stamp shared by all sub-classes */
    protected long timestamp;

    protected static final String TIMESTAMP = "BBXML.ConvertTimeStamp(?)";

    public static final String LOGGER_KEY = "com.broadon.xs.AuditLogger";

    protected Logger() {
	long time = new Date().getTime();
	// round up to nearest second
	timestamp = ((time + 500) / 1000) * 1000;
    }

    /**
     * Write (and immediately commit) the specified values into the
     * corresponding audit log table.  The implementation is
     * responsible for deciding if a new record should be created, or
     * an existing record be updated.
     */
    abstract public void flush(Connection conn) throws SQLException;
}

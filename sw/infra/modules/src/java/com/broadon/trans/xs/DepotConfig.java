package com.broadon.trans.xs;

import java.util.Properties;
import java.util.Enumeration;
import java.sql.*;
import java.security.GeneralSecurityException;
import javax.servlet.http.*;

import com.broadon.filter.*;
import com.broadon.db.ServerName;

import com.broadon.trans.common.*;
import com.broadon.trans.installer.*;


/**
 * Servlet implementation that handles a Depot configuration request.
 */
public final class DepotConfig extends TransactionServlet implements ServerName
{
    static final String REQUEST_RESULT_TAG = "depot_install_result";
    static final String DEPOT_VERSION_KEY = "depot_version";
    static final String USER_ID_KEY = "user_id";
    static final String PASSWORD_KEY = "password";
    static final String STORE_ID_KEY = "store_id";
    static final String STORE_ADDR_KEY = "store_addr";
    static final String REGION_ID_KEY = "region_id";
    static final String BU_ID_KEY = "bu_id";
    static final String CITY_CODE_KEY = "city_code";
    static final String CONTENT_SYNC_URL_KEY = "content_sync_url";
    static final String REMOTE_MGMT_URL_KEY = "remote_mgmt_server";
    static final String TRANSACTION_URL_KEY = "transaction_url";
    static final String INSTALLER_URL_KEY = "installer_url";
    static final String NETWORK_TIME_URL_KEY = "network_time_server";
    static final String OPERATOR_URL_KEY = "operator_id_url";

    static final Properties URL_KEYS;

    static {
	URL_KEYS = new Properties();
	URL_KEYS.setProperty(S_CDS, CONTENT_SYNC_URL_KEY);
	URL_KEYS.setProperty(S_NTP, NETWORK_TIME_URL_KEY);
	URL_KEYS.setProperty(S_RMS, REMOTE_MGMT_URL_KEY);
	URL_KEYS.setProperty(S_XS, TRANSACTION_URL_KEY);
	URL_KEYS.setProperty(S_OPS, OPERATOR_URL_KEY);
	URL_KEYS.setProperty(S_IS, INSTALLER_URL_KEY);
    }
    
    protected TransactionRequest readInputs(XMLResponse res,
					    HttpServletRequest req)
    {
	String s;

	DepotConfigRequest confReq = new DepotConfigRequest();

	res.startDoc(REQUEST_RESULT_TAG);

	s = req.getParameter(DEPOT_VERSION_KEY);
	res.printElement(DEPOT_VERSION_KEY, s);

	confReq.userID = req.getParameter(USER_ID_KEY);
	res.printElement(USER_ID_KEY, confReq.userID);

	confReq.password = req.getParameter(PASSWORD_KEY);
	res.printElement(PASSWORD_KEY, confReq.password);

	s = req.getParameter(STORE_ID_KEY);
	confReq.storeID = Integer.parseInt(s);
	res.printElement(STORE_ID_KEY, s);

	return confReq;
    } // readInputs


    protected void process(Connection conn, authWrapper req,
			   TransactionRequest transReq, XMLResponse res)
	throws SQLException
    {
	DepotConfigRequest confReq = (DepotConfigRequest) transReq;
	try {
	    ConfigDepot config = new ConfigDepot(conn, confReq.storeID);


	    // we need the BU ID to validate the service tech.
	    // validate password or smart card
	    try {
		ServiceTech.validate(conn, config.getBUID(),
				     confReq.userID, confReq.password,
				     req, ServiceTech.installer);
	    } catch (GeneralSecurityException e) {
		res.printExitStatus(StatusCode.XS_UNAUTH_TECH);
		return;
	    }

	    res.printElement(STORE_ADDR_KEY, config.getAddress());
	    res.printElement(REGION_ID_KEY, String.valueOf(config.getRegionID()));
	    res.printElement(BU_ID_KEY, String.valueOf(config.getBUID()));
	    res.printElement(CITY_CODE_KEY, config.getCityCode());
			     

	    Properties servers = config.getServerAddr();
	    for (Enumeration e = servers.propertyNames(); e.hasMoreElements(); ) {

		String name = (String) e.nextElement();
		String value = servers.getProperty(name);
		String key = URL_KEYS.getProperty(name);
		if (value != null && key != null)
		    res.printElement(key, value);
	    }

	    res.printExitStatus(StatusCode.SC_OK);
	} catch (InvalidStoreIDException e) {
	    res.printExitStatus(StatusCode.XS_BAD_STORE, e);
	}
    }


    
}

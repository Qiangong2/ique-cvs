package com.broadon.trans.etickets;

import java.util.Enumeration;

import com.broadon.util.Base64;
import com.broadon.trans.common.XMLResponse;
import com.broadon.trans.etickets.ETickets;

/**
 * Format the list of eTickets and their corresponding CA chain as XML
 * elements.
 */
public class ETicketsFormatter
{
    static final String CHAIN_ID_KEY = "ca_chain";
    static final String ETICKET_KEY = "eticket";

    final ETickets tickets;
    final XMLResponse res;

    /**
     * @param tickets <code>ETickets</code> object holding the list of
     * eTickets and CA chain.
     * @param res XML formatter.
     */
    public ETicketsFormatter(ETickets tickets, XMLResponse res) {
	this.tickets = tickets;
	this.res = res;
    }

    /**
     * Print all the eTickets.
     */
    public void dumpTickets() {
	Base64 base64 = new Base64();
	for (Enumeration e = tickets.getTickets(); e.hasMoreElements(); ) {
	    byte[] rawTickets = (byte[]) e.nextElement();
	    res.printElement(ETICKET_KEY, base64.encode(rawTickets));
	}
    }

    /**
     * Print all the certificates in the CA chain.
     */
    public void dumpCerts() {
	for (Enumeration e = tickets.getCACerts(); e.hasMoreElements(); ) {
	    res.printElement(CHAIN_ID_KEY, (String) e.nextElement());
	}
    }
}

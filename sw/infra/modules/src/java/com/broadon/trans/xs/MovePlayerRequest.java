package com.broadon.trans.xs;

import com.broadon.trans.common.TransactionRequest;

/**
 * Hold all input parameters of a RMA request.
 */
public class MovePlayerRequest extends TransactionRequest
{
    /** Encoded ID of the old BB Player. */
    protected long oldBBID;

    /** Encoded ID of the new BB Player. */
    protected long newBBID;

    /** Serial number of the old BB Player. */
    protected String oldSerial;

    /** Serial number of the new BB Player. */
    protected String newSerial;

    /** ID of the BB Depot. */
    protected long depotID;

    /** time stamp for last eTicket synchronization. */
    protected long lastSync;

    protected String userID;

    protected String password;
}

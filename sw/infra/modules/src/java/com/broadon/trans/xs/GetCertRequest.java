package com.broadon.trans.xs;

import com.broadon.trans.common.TransactionRequest;

/**
 * Hold the data of a "get depot certificate" request.
 */
public class GetCertRequest extends TransactionRequest
{
    /** User ID of the service technician. */
    protected String userID;

    /** Password of the service technician. */
    protected String password;

    /** ID of the store where the request is made. */
    protected int storeID;

    /** Encoded ID of a BB Depot. */
    protected String encodedDepotID;

    /** ID of the BB Depot (internal format). */
    protected long depotID;

    
}

package com.broadon.trans.user;

public interface XmlTags
{
    static final String REGISTRATION_RESULT_TAG = "user_reg_result";
    static final String SYNC_RESULT_TAG = "user_sync_result";
    static final String DEPOT_VERSION_KEY = "depot_version";
    static final String REPLIED_EMAIL_KEY = "email_address";
    static final String USER_NAME_KEY = "name";
    static final String PINYIN_NAME_KEY = "pinyin";
    static final String ADDRESS_KEY = "address";
    static final String BIRTHDAY_KEY = "birthdate";
    static final String TELEPHONE_KEY = "telephone";
    static final String GENDER_KEY = "gender";
    static final String BB_ID_KEY = "bb_id";
    static final String OTHER_INFO_KEY = "misc";
    static final String BB_SIGNATURE_KEY = "bbplayer_signature";
    static final String CREATE_DATE_KEY = "first_reg_date";
}

package com.broadon.trans.etickets;

/**
 * Constants related to eTickets.
 */
public interface ETicketConstants
{
    public static final int MIN_TID = 0x8000;

    public static final int MAX_TID = 0xffff;
}

package com.broadon.trans.purchase;

import java.sql.*;

import com.broadon.trans.common.Logger;

/**
 * Logger for eTicket sync.  Typically, an instance of this class
 * is created upon receiving the sync request and the info is
 * flushed to the audit log.  After the sync process, this
 * object is put into the audit log queue again with the sync
 * status updated.
 */
public class ETicketSyncLog extends Logger
{
    static final String addLog =
	"INSERT INTO ETICKET_SYNC_LOGS (BB_ID, HR_ID, REQUEST_DATE) " +
	"    VALUES (?, ?, " + TIMESTAMP + ")";

    static final String updateLog =
	"UPDATE ETICKET_SYNC_LOGS SET REQUEST_STATUS=?, REQUEST_LOG=? " +
	"    WHERE BB_ID=? AND HR_ID = ? AND REQUEST_DATE = " + TIMESTAMP;

    long bbId;
    long hrId;

    String status;
    String message;

    boolean flushed;

    /**
     * Create an <code>ETicketSyncLog</code> object holding the
     * values to be inserted into the audit log.
     */
    public ETicketSyncLog(long bbId, long depotId)
    {
	super();
	
	this.bbId = bbId;
	this.hrId = depotId;

	flushed = false;
	
	status = message = null;
    }


    synchronized public void flush(Connection conn) throws SQLException
    {
	PreparedStatement ps;
	int i = 1;

	if (flushed) {
	    // already written once, need to update the existing record
	    ps = conn.prepareStatement(updateLog);
	    ps.setString(i++, status);
	    ps.setString(i++, message);
	} else {
	    ps = conn.prepareStatement(addLog);
	}

	ps.setLong(i++, bbId);
	ps.setLong(i++, hrId);
	ps.setLong(i++, timestamp);

	try {
	    ps.executeUpdate();
	} finally {
	    ps.close();
	}

	flushed = true;
    }

    /**
     * Set the status of eTicket sync and possibly an error
     * message. 
     */
    synchronized public void setStatus(String status, String message)
    {
	this.status = status;
	this.message = message;
    }
}

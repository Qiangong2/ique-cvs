package com.broadon.trans.xs;

import java.sql.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.OutputStream;
import oracle.sql.BLOB;
import oracle.jdbc.OracleResultSet;

import com.broadon.filter.authWrapper;
import com.broadon.util.Base64;
import com.broadon.exception.InvalidRequestException;

import com.broadon.trans.common.*;
import com.broadon.trans.upload.CompetitionLog;
import com.broadon.trans.upload.CompetitionInfo;
import com.broadon.trans.upload.CannotCompeteException;

/**
 * General servlet handling submission of data from Depot. 
 */
public final class Upload extends TransactionServlet 
{
    static final String REQUEST_RESULT_TAG = "submit_states_result";
    static final String DEPOT_VERSION_KEY = "depot_version";
    static final String BB_ID_KEY = "bb_id";
    static final String COMPETITION_ID_KEY = "competition_id";
    static final String TITLE_ID_KEY = "title_id";
    static final String CONTENT_ID_KEY = "content_id";
    static final String STATES_KEY = "states";
    static final String SIG_KEY = "signature";

    static final String Timestamp = CompetitionLog.TimeConvStr();

    static final String Submission =
	"INSERT INTO COMPETING_CUSTOMERS (EMAIL_ADDRESS, COMPETITION_ID, " +
	"  BB_ID, SUBMIT_DATE, CONTENT_ID, SIG_GS, RAW_GS, " +
    "  STORE_ID, CITY_CODE) " +
	"  VALUES (?, ?, ?, " + Timestamp + ", ?, ?, empty_blob(), ?, ?)";

    static final String UpdateStates =
	"UPDATE COMPETING_CUSTOMERS SET SUBMIT_DATE = " + Timestamp + 
	"    , STORE_ID = ?, CITY_CODE = ?, SIG_GS = ?, RAW_GS = empty_blob() " +
	"WHERE EMAIL_ADDRESS = ? AND COMPETITION_ID = ?";

    static final String SelectGameState =
    "SELECT RAW_GS FROM COMPETING_CUSTOMERS " + 
    "WHERE EMAIL_ADDRESS = ? AND COMPETITION_ID = ?";


    protected TransactionRequest readInputs(XMLResponse        res,
                                            HttpServletRequest req)
    {
        String        s;
        UploadRequest uploadReq = new UploadRequest();

        res.startDoc(REQUEST_RESULT_TAG);

        res.printElement(DEPOT_VERSION_KEY, req.getParameter(DEPOT_VERSION_KEY));
        res.printElement(HR_ID_KEY, req.getParameter(HR_ID_KEY));

        s = req.getParameter(BB_ID_KEY);
        uploadReq.bbID = Long.parseLong(s);
        res.printElement(BB_ID_KEY, s);

        s = req.getParameter(COMPETITION_ID_KEY);
        if (s == null) {
            //
            // To respond with an error to upload requests from depots older
            // than version 1.2.
            //
            uploadReq.competitionID = CompetitionLog.MISSING_VALUE_INT;
        }
        else {
            uploadReq.competitionID = Integer.parseInt(s);
            res.printElement(COMPETITION_ID_KEY, s);
        }

        s = req.getParameter(TITLE_ID_KEY);
        uploadReq.titleID = Long.parseLong(s);
        res.printElement(TITLE_ID_KEY, s);

        s = req.getParameter(CONTENT_ID_KEY);
        uploadReq.contentID = Long.parseLong(s);
        res.printElement(CONTENT_ID_KEY, s);

        uploadReq.signature = req.getParameter(SIG_KEY);
	
        Base64 base64 = new Base64();
        s = req.getParameter(STATES_KEY);
        if (s == null) {
            uploadReq.data = null;
        }
        else {
            try {
                uploadReq.data = base64.decode(s);
            } catch (IOException e) {}
        }
        return uploadReq;
    } // readInputs


    protected void process(Connection         conn, 
                           authWrapper        req,
                           TransactionRequest transReq, 
                           XMLResponse        res)
	throws SQLException
    {
        UploadRequest uploadReq = (UploadRequest) transReq;

        CompetitionLog log =
            new CompetitionLog(uploadReq.bbID, 
                               req.getDepotID(),
                               uploadReq.competitionID,
                               uploadReq.titleID, 
                               uploadReq.contentID);
        auditLog.add(log);

        try {

            // Note:  A depot will delete the spooled data file only if the
            // response code is either OK or XS_MALFORMED_INPUT.
            //
            if (uploadReq.competitionID == CompetitionLog.MISSING_VALUE_INT) {
                res.printExitStatus(StatusCode.XS_MALFORMED_INPUT,
                                    new Exception("Missing <competition_id>."));
                return;
            }
            else if (uploadReq.data == null) {
                res.printExitStatus(StatusCode.XS_MALFORMED_INPUT,
                                    new Exception("No data for submission."));
                return;
            }

            // verify if the BB ID is valid
            //
            BBPlayer bb = new BBPlayer(uploadReq.bbID, req.getBUID(), conn);

            // validate input and get hold of associated information
            //
            final long      utcMsecTime = bb.getCurrentTime()*1000;
            CompetitionInfo cinfo = 
                CompetitionInfo.readCompetitionInfo(conn,
                                                    uploadReq.bbID,
                                                    uploadReq.titleID,
                                                    uploadReq.contentID,
                                                    uploadReq.competitionID,
                                                    req.getStoreID(),
                                                    utcMsecTime);
            
            // Upload to DB
            //
            conn.setAutoCommit(false);
            doUpload(uploadReq, conn, cinfo);
            conn.commit();

            res.printExitStatus(StatusCode.SC_OK);

        } catch (InvalidBBIDException e) {
            res.printExitStatus(StatusCode.XS_MALFORMED_INPUT, e);
        } catch (InvalidRequestException e) {
            res.printExitStatus(StatusCode.XS_MALFORMED_INPUT, e);
        } catch (CannotCompeteException e) {
            res.printExitStatus(StatusCode.XS_CANNOT_COMPETE, e);
        } catch (SQLException e) {
            e.printStackTrace(System.out);
            res.printExitStatus(StatusCode.SC_SQL_EXCEPTION, e);
        } catch (IOException e) {
            res.printExitStatus(StatusCode.SC_DB_EXCEPTION, e);
        } finally {
            conn.rollback();
            log.setStatus(res.getExitStatusCode(), res.getExitStatusMsg());
            auditLog.add(log);
        }
    }


    private void writeGameState(String     emailAddress,
                                int        competitionID,
                                byte[]     gameState, 
                                Connection conn)
        throws SQLException, IOException
    {
        PreparedStatement ps = conn.prepareStatement(SelectGameState);
        ps.setString(1, emailAddress);
        ps.setInt(2, competitionID);
        try {
            final ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                final BLOB blob = ((OracleResultSet)rs).getBLOB(1);
                OutputStream outstream = blob.setBinaryStream(1L);
                outstream.write(gameState);
                outstream.close();
            }
            else {
                throw new SQLException("Failed to select game-state BLOB");
            }
            rs.close();
        } finally {
            ps.close();
        }
    }


    void doUpload(UploadRequest   req, 
                  Connection      conn, 
                  CompetitionInfo cinfo)
        throws InvalidRequestException, SQLException, IOException
    {
        PreparedStatement ps = conn.prepareStatement(Submission);
        int i = 0;
        ps.setString(++i, cinfo.getEmailAddress());
        ps.setInt(++i, req.competitionID);
        ps.setLong(++i, req.bbID);
        ps.setLong(++i, cinfo.getCurrentTime());
        ps.setLong(++i, req.contentID);
        ps.setString(++i, req.signature);
        ps.setInt(++i, cinfo.getStoreID());
        if (cinfo.isUniversalCityCode())
            ps.setNull(++i, Types.VARCHAR);
        else
            ps.setString(++i, cinfo.getCityCode());

        try {
            ps.executeUpdate();
            
        } catch (SQLException e) {
            if (e.getErrorCode() == 1) {
                // unique constraint violation -- this means that the
                // data has already been submitted
                ps.close();
                ps = conn.prepareStatement(UpdateStates);
                i = 0;
                ps.setLong(++i, cinfo.getCurrentTime());

                ps.setInt(++i, cinfo.getStoreID());
                if (cinfo.isUniversalCityCode())
                    ps.setNull(++i, Types.VARCHAR);
                else
                    ps.setString(++i, cinfo.getCityCode());
                ps.setString(++i, req.signature);
                ps.setString(++i, cinfo.getEmailAddress());
                ps.setInt(++i,  req.competitionID);

                ps.executeUpdate();
            } else {
                throw e;
            }
        } finally {
            ps.close();
        }

        // Update the game-state BLOB for the upload
        //
        writeGameState(cinfo.getEmailAddress(),
                       req.competitionID,
                       req.data, 
                       conn);

    } // doUpload

	
}

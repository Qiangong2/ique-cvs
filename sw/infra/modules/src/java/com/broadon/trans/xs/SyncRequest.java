package com.broadon.trans.xs;

import com.broadon.trans.common.TransactionRequest;

/**
 * Hold all input parameters of a eTicket sync request.
 */
public class SyncRequest extends TransactionRequest
{
    /** Encoded ID of a BB Player. */
    protected long bbID;

    /** ID of the BB Depot. */
    protected long depotID;

    /** time stamp for last eTicket synchronization. */
    protected long lastSync;

    /** minimum ticket ID honored by the player. */
    protected int minTID;

    /** whether or not to send back the full eticket,
     *  or only the variant part
    */
    protected boolean getEticketInFull;
}

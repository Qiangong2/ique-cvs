package com.broadon.trans.xs;

import java.security.GeneralSecurityException;
import java.sql.*;

import javax.servlet.http.HttpServletRequest;

import com.broadon.exception.InvalidRequestException;
import com.broadon.filter.authWrapper;
import com.broadon.trans.common.*;
import com.broadon.trans.purchase.PurchaseTitle;
import com.broadon.trans.purchase.PurchaseTitleBonus;


/**
 * Servlet implementation that handles a purchase request by operator
 * (using a smart-card).  Apart from the kind-of-purchase, the 
 * operator authentication, and the identification of an operatorID,
 * this servlet is just like the Purchase servlet.
 */
public final class PurchaseByOperator extends Purchase
{

    static final String USER_ID_KEY = "user_id";
    static final String PASSWORD_KEY = "password";

    /**
     * Constructs an appropriate <code>PurchaseTitle</code> object.
     * @param bb BB player record.
     * @param hrID Depot that submits this purchase request.
     * @param storeID ID of the store to which the Depot belongs.
     * @param regionID Logical region where the BB Depot belongs.
     * @param kindOfPurchase One of the PURCHASE_XXX constants defined in
     * PurchaseTitle.
     * @param titleID Title to be purchased.
     * @param ecards One or more eCards for purchase payment (null for TRIAL).
     * @param eUnits Cost of this title (if cost is unknown, set this
     * to -1 and the correct value will be fetched from the database)
     * @param contendIDs One or more content ID that compose this
     * title.  If <code>null</code>, the content ID list will be
     * fetched from the database.
     * @param clientIP The client IP address of the member doing the purchase
     * @param membershipID The identity of the member doing the purchase
     * @param memberID The identity of the member doing the purchase
     * @exception InvalidRequestException The kindOfPurchase is not 
     * supported by this servlet.
     */
    protected PurchaseTitle 
    getPurchaseTitle(BBPlayer bb,
                     long     hrID, // BB id
                     int      storeID,
                     int      regionID,
                     String   kindOfPurchase,
                     long     titleID, 
                     String[] ecards,
                     int      eUnits, 
                     long[]   contentIDs,
                     String   clientIP,     // Use null when undefined
                     long     membershipID, // Use -1 when undefined
                     long     memberID)     // Use -1 when undefined
        throws InvalidRequestException
    {
        // This is an operator initiated request for purchase, where the
        // kinds of purchase supported are as listed below:
        //
        
        PurchaseTitle p = null;

        if (kindOfPurchase.equals(PURCHASE_KIND_BONUS)) {
            p = new PurchaseTitleBonus(bb, hrID, storeID, regionID, 
                                       titleID, contentIDs,
                                       clientIP, membershipID, memberID);
        }
        else {
            throw new InvalidRequestException("Operator cannot initate " +
                                              kindOfPurchase +
                                              " purchase");
        }
        return p;
    } // getPurchaseTitle


    protected void process(Connection conn, authWrapper req,
                           TransactionRequest transReq, XMLResponse res)
        throws SQLException
    {
        PurchaseRequest purchaseReq = (PurchaseRequest) transReq;

        // validate password or smartcard
        //
        try {
            purchaseReq.operatorID =
                ServiceTech.validate(conn, req.getBUID(), purchaseReq.userID,
                                     purchaseReq.password, req,
                                     ServiceTech.bonusGameIssuer);
        } catch (GeneralSecurityException e) {
            res.printExitStatus(StatusCode.XS_UNAUTH_TECH);
            return;
        }

        // Now perform regular purchasing, but with the overrided
        // getPurchaseTitle as defined here, with the additional
        // readInputs() added here, and with the operatorID defined 
        // above.
        // 
        super.process(conn, req, transReq, res);
    }


    protected TransactionRequest readInputs(XMLResponse res,
                                            HttpServletRequest req)
    {
        PurchaseRequest purchaseReq = 
            (PurchaseRequest)super.readInputs(res, req);	

        purchaseReq.userID = req.getParameter(USER_ID_KEY);
        res.printElement(USER_ID_KEY, purchaseReq.userID);

        purchaseReq.password = req.getParameter(PASSWORD_KEY);
        res.printElement(PASSWORD_KEY, purchaseReq.password);

        return purchaseReq;
    } // readInputs
}

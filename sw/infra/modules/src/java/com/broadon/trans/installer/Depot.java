package com.broadon.trans.installer;

import java.sql.*;

import com.broadon.db.DBException;


/**
 * Databae interface to insert or update a Depot record.
 */
public class Depot
{
    static final String readDepot =
	"SELECT SUSPEND_DATE, REVOKE_DATE, STORE_ID FROM DEPOTS WHERE HR_ID = ?";

    static final String updateDepot =
	"UPDATE DEPOTS SET CONFIG_DATE = BBXML.ConvertTimeStamp(?), " +
	"    PUBLIC_KEY = ? WHERE HR_ID = ?";

    static final String reusedDepot =
	"UPDATE DEPOTS SET STORE_ID = ?, CREATE_DATE = BBXML.ConvertTimeStamp(?), " +
	"    CONFIG_DATE = BBXML.ConvertTimeStamp(?), SUSPEND_DATE=NULL, " +
	"    REVOKE_DATE = NULL, PUBLIC_KEY = ? WHERE HR_ID = ?";

    static final String insertDepot =
	"INSERT INTO DEPOTS (HR_ID, STORE_ID, CREATE_DATE, CONFIG_DATE, " +
	"    PUBLIC_KEY) VALUES (?, ?, BBXML.ConvertTimeStamp(?), " +
	"    BBXML.ConvertTimeStamp(?), ?)";

    /**
     * Update the store ID and public key of the Depot record for the
     * specified BB Depot (<code>hrID</code>), or insert a new record
     * if such a record does not exist.
     * @param conn Database connection to be used.
     * @param hrID ID of the BB Depot.
     * @param storeID ID of the store where the depot resides.
     * @param publicKey Public key of the x509 certificate that
     * identifies this depot.
     * @exception SQLException Any database access error.
     * @exception DBException Data inconsistency error.
     */
    public static void insert(Connection conn, long hrID, int storeID,
			      String publicKey)
	throws SQLException, DBException
    {
	long now = new java.util.Date().getTime();

	PreparedStatement ps = conn.prepareStatement(readDepot);
	ps.setLong(1, hrID);
	try {
	    ResultSet rs = ps.executeQuery();

	    if (rs.next()) {
		// Record already exists, do update
		if (rs.getInt(3) == storeID) {
		    // updating a depot in the same store, check for revocation
		    if (rs.getDate(1) != null || rs.getDate(2) != null) {
			rs.close();
			ps.close();
			throw new DBException("Depot suspended or revoked");
		    }
		    ps.close();
	    
		    ps = conn.prepareStatement(updateDepot);
		    ps.setLong(1,  now);
		    ps.setString(2, publicKey);
		    ps.setLong(3, hrID);
		} else {
		    // recycling a previously registered Depot to a new store
		    ps.close();

		    ps = conn.prepareStatement(reusedDepot);
		    ps.setInt(1, storeID);
		    ps.setLong(2, now);
		    ps.setLong(3, now);
		    ps.setString(4, publicKey);
		    ps.setLong(5, hrID);
		}
	    } else {
		ps.close();
	    
		// create new record
		ps = conn.prepareStatement(insertDepot);
		ps.setLong(1, hrID);
		ps.setInt(2, storeID);
		ps.setLong(3, now);
		ps.setLong(4, now);
		ps.setString(5, publicKey);
	    }

	    ps.executeUpdate();
	} finally {
	    ps.close();
	}
    }
}

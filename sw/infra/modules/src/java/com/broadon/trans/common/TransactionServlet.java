package com.broadon.trans.common;

import java.util.*;
import java.sql.*;
import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.*;

import com.broadon.filter.*;
import com.broadon.servlet.ServletConstants;
import com.broadon.hsm.HSMClient;
import com.broadon.db.DataSourceScheduler;
import com.broadon.util.TimeTrace;

/**
 * Top-level class providing the basis for all servlet implementation
 * in the Transaction servers.
 * @version $Revision: 1.20 $
 * @author W. Wilson Ho
 */
public abstract class TransactionServlet
    extends HttpServlet implements ServletConstants
{
    /** Tracing configuration in BBserver.properties */
    private static final String CFG_TRACING = "TRACING";
    private static final String DEF_TRACING_ON = "ON";

    /** Servlet Context */
    protected ServletContext ctx;

    /** XML tag for the Depot ID (aka HR ID). */
    protected static final String HR_ID_KEY = "hr_id";

    /** Regional Center ID of this server. */
    protected int regionalCenterID;

    /** Hardware Security Module connector. */
    protected HSMClient hsm;

    /** Audit log hander. */
    protected AuditLog auditLog;

    /** Tracing of elapsed time */
    private   boolean   tracing;
    private   TimeTrace timeTrace;

    protected boolean   isTracing() {return tracing;}
    protected TimeTrace getTimeTrace() {return timeTrace;}
    
    public void init(ServletConfig config) throws ServletException
    {
        ctx = config.getServletContext();
        regionalCenterID = Integer.parseInt((String) ctx.getAttribute(REGIONAL_CENTER_KEY));
        hsm = (HSMClient) ctx.getAttribute(HSM_CONNECTOR_KEY);
        auditLog = (AuditLog) ctx.getAttribute(Logger.LOGGER_KEY);

        final Properties prop = (Properties)ctx.getAttribute(PROPERTY_KEY);
        final String     trace = (String)prop.get(CFG_TRACING);
        tracing = (trace!=null && trace.toUpperCase().equals(DEF_TRACING_ON));
        timeTrace = null;
    }
    

    public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
    {
        final TimeTrace ttrace = (tracing? 
                                  new TimeTrace(TimeTrace.INCLUSIVE) : null);

	res.setStatus(res.SC_ACCEPTED);

	HttpServletRequestWrapper treq = (HttpServletRequestWrapper) req;
	while (! treq.getClass().getName().equals("com.broadon.filter.authWrapper")) {
	    treq = (HttpServletRequestWrapper) treq.getRequest();
	}

	authWrapper wreq = (authWrapper) treq;

	// Check if DepotID in the request matches that in the cert.
	if (wreq.hasClientAuth() &&
	    ! wreq.getEncodedDepotID().equals(req.getParameter(HR_ID_KEY))) {
	    res.sendError(res.SC_UNAUTHORIZED);
	    return;
	}

	res.setContentType("text/plain; charset=utf-8");
	XMLResponse response = new XMLResponse(res);
	TransactionRequest transRequest = null;

	try {
	    transRequest = readInputs(response, req);
	} catch (Exception e) {
	    response.printExitStatus(StatusCode.XS_MALFORMED_INPUT, e);
	    return;
	}

	Connection conn = null;
	
	try {
	    conn = wreq.getConnection(DataSourceScheduler.DB_PRIORITY_FAST);
        if (tracing) timeTrace = ttrace.pushInterval("setup");
	    process(conn, wreq, transRequest, response);
        if (tracing) {
            ttrace.endInterval("process");
            System.out.println("TransactionServlet " + ttrace);
        }
	} catch (SQLException sql) {
	    StringBuffer buf = new StringBuffer();
	    for (SQLException s = sql; s != null; s = s.getNextException()) {
		buf.append(s.toString());
	    }
	    response.printExitStatus(StatusCode.SC_SQL_EXCEPTION,
				     buf.toString());
	    sql.printStackTrace();
	} finally {
	    if (conn != null) {
		try {
		    conn.close();
		} catch (SQLException e) {}
	    }
        if (tracing) {
            ttrace.clear();
        }
	}

	if (StatusCode.SC_OK.equals(response.getExitStatusCode()))
	    res.setStatus(res.SC_OK);
    } // doPost


    /** Read the XML inputs.   Missing parameter might result in null
     * pointer exception or cannot be converted to proper numeric
     * values.  These exceptions are caught by the caller.
     * @param res Generate response in XML format.
     * @param req HTTP request.
     * @return A class embedding all relevant input data already
     * parsed and converted to correct type.
     */
    abstract protected TransactionRequest readInputs(XMLResponse res,
						     HttpServletRequest req);


    /**
     * Implements the <em>body</em> of this servlet.
     * @param conn Database connection to be used.
     * @param req HTTP request wrapper.  Includes the parameters
     * extracted from the client's certificate.
     * @param transRequest A class containing all the parsed request parameters.
     * @param res Generate response in XML format.
     */
    abstract protected void process(Connection conn, authWrapper req,
				    TransactionRequest transRequest,
				    XMLResponse res)
	throws SQLException;

}

package com.broadon.trans.installer;

import java.util.Properties;
import java.sql.*;

/**
 * Retrieve depot configuration parameters.
 */
public class ConfigDepot {

    static final String getStoreInfo =
	"SELECT A.ADDRESS, A.REGION_ID, A.REGIONAL_CENTER_ID, B.BU_ID, " +
	"       A.CITY_CODE " +
	"FROM STORES A, REGIONS B " +
	"WHERE A.SUSPEND_DATE IS NULL AND A.REVOKE_DATE IS NULL AND " +
	"      A.REGION_ID = B.REGION_ID AND A.STORE_ID = ?";

    static final String getServerInfo =
	"SELECT SERVER_TYPE, SERVER_ADDR FROM REGIONAL_SERVERS WHERE " +
	"    REGIONAL_CENTER_ID = ?";

    String address;
    int regionID;
    int buID;
    String cityCode;
    Properties serverAddr;

    /**
     * Read from the database the BB Depot configuration parameter for
     * the given store.
     * @param conn Database connectin to use.
     * @param storeID ID of the store where the Depot resides.
     * @exception SQLException Any database access error.
     * @exception InvalidStoreIDException Store ID invalid or revoked.
     */
    public ConfigDepot(Connection conn, int storeID)
	throws SQLException, InvalidStoreIDException
    {
	int regionalCtr = -1;

	PreparedStatement ps = conn.prepareStatement(getStoreInfo);
	ps.setInt(1, storeID);

	try {
	    ResultSet rs = ps.executeQuery();
	    if (rs.next()) {
		address = rs.getString(1);
		regionID = rs.getInt(2);
		regionalCtr = rs.getInt(3);
		buID = rs.getInt(4);
		cityCode = rs.getString(5);
	    } else
		throw new InvalidStoreIDException("Store ID = " + storeID);
	    rs.close();
	    ps.close();
	
	    ps = conn.prepareStatement(getServerInfo);
	    ps.setInt(1, regionalCtr);
	    rs = ps.executeQuery();
	    serverAddr = new Properties();
	    while (rs.next()) {
		serverAddr.setProperty(rs.getString(1), rs.getString(2));
	    }
	    rs.close();
	} finally {
	    ps.close();
	}
    }

    /** @return Street address of the store. */
    public String getAddress() {
	return address;
    }

    /** @return Region ID of this store. */
    public int getRegionID() {
	return regionID;
    }

    /** @return Property list of all servers. */
    public Properties getServerAddr() {
	return serverAddr;
    }

    /** @return Business Unit of this store. */
    public int getBUID() {
	return buID;
    }

    /** @return City Code (city name) of all servers. */
    public String getCityCode() {
	return cityCode;
    }
}

package com.broadon.trans.purchase;

import com.broadon.db.DBException;

/**
 * The <code>TitleAlreadyPurchaseException</code> class is thrown when
 * a purchase request is rejected because the specified BB Player
 * already owns that title.
 * 
 *
 * @version	$Revision: 1.2 $
 */
public class TrialAfterUseException
    extends DBException
{
    /**
     * Constructs a TrialAfterUseException instance.
     */
    public TrialAfterUseException()
    {
	super();
    }

    /**
     * Constructs a TrialAfterUseException instance.
     *
     * @param	message			the exception message
     */
    public TrialAfterUseException(String message)
    {
	super(message);
    }

    /**
     * Constructs a TrialAfterUseException instance.
     *
     * @param	message			the exception message
     * @param	throwable		the nested exception
     */
    public TrialAfterUseException(String message, Throwable throwable)
    {
	super(message, throwable);
    }
}

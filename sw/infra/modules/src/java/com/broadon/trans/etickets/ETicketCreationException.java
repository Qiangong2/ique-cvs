package com.broadon.trans.etickets;

import com.broadon.db.DBException;

/**
 * The <code>ETicketCreationException</code> class is thrown when
 * an eTicket cannot be issued due to data inconsistency.
 * 
 *
 * @version	$Revision: 1.2 $
 */
public class ETicketCreationException extends DBException
{
    /**
     * Constructs a ETicketCreationException instance.
     */
    public ETicketCreationException()
    {
	super();
    }

    /**
     * Constructs a ETicketCreationException instance.
     *
     * @param	message			the exception message
     */
    public ETicketCreationException(String message)
    {
	super(message);
    }

    /**
     * Constructs a ETicketCreationException instance.
     *
     * @param	message			the exception message
     * @param	throwable		the nested exception
     */
    public ETicketCreationException(String message, Throwable throwable)
    {
	super(message, throwable);
    }
}

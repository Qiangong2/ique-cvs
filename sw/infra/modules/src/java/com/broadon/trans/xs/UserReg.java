package com.broadon.trans.xs;

import java.sql.*;
import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;

import com.broadon.filter.authWrapper;
import com.broadon.db.DBException;

import com.broadon.trans.common.*;
import com.broadon.trans.user.Registration;
import com.broadon.trans.user.XmlTags;

/**
 * Servlet handling customer registration
 */
public final class UserReg extends TransactionServlet implements XmlTags
{
    protected TransactionRequest readInputs(XMLResponse res,
					    HttpServletRequest req)
    {
	UserRegRequest userReq = new UserRegRequest();

	res.startDoc(REGISTRATION_RESULT_TAG);

	res.printElement(DEPOT_VERSION_KEY, req.getParameter(DEPOT_VERSION_KEY));

	res.printElement(HR_ID_KEY, req.getParameter(HR_ID_KEY));

	userReq.pinyinName = req.getParameter(PINYIN_NAME_KEY);
	if (userReq.pinyinName != null)
	    userReq.pinyinName = userReq.pinyinName.toLowerCase();
	res.printElement(PINYIN_NAME_KEY, userReq.pinyinName);

	userReq.name = req.getParameter(USER_NAME_KEY);
	res.printElement(USER_NAME_KEY, userReq.name);

	userReq.address = req.getParameter(ADDRESS_KEY);
	res.printElement(ADDRESS_KEY, userReq.address);

	userReq.setBirthday(req.getParameter(BIRTHDAY_KEY));
	res.printElement(BIRTHDAY_KEY, userReq.formatBirthday());

	userReq.telephone = req.getParameter(TELEPHONE_KEY);
	res.printElement(TELEPHONE_KEY, userReq.telephone);

	userReq.gender = req.getParameter(GENDER_KEY);
	res.printElement(GENDER_KEY, userReq.gender);

	userReq.bbID = Long.parseLong(req.getParameter(BB_ID_KEY));
	res.printElement(BB_ID_KEY, String.valueOf(userReq.bbID));

	userReq.otherInfo = req.getParameter(OTHER_INFO_KEY);
	res.printElement(OTHER_INFO_KEY, userReq.otherInfo);

	userReq.emailAddress = generateEmailAddress(userReq);

	return userReq;
    }

    String generateEmailAddress(UserRegRequest req) {
	if (req.pinyinName == null)
	    return null;
	if (! req.pinyinName.matches("[a-z][a-z.-]*"))
	    return null;
	SimpleDateFormat fmt = new SimpleDateFormat("yyMMdd");
	return req.pinyinName + fmt.format(req.birthday);
    }

    protected void process(Connection conn, authWrapper req,
			   TransactionRequest transReq, XMLResponse res)
	throws SQLException
    {
	UserRegRequest userReq = (UserRegRequest) transReq;

	// to do: perform some authentication

	if (userReq.emailAddress == null) {
	    res.printExitStatus(StatusCode.XS_MALFORMED_INPUT,
				new Exception("Cannot derive email address"));
	    return;
	}

	try {
	    // verify if the BB ID is valid
	    BBPlayer bb = new BBPlayer(userReq.bbID, req.getBUID(), conn);

	    Registration register = new Registration(conn, userReq.bbID,
						     userReq.emailAddress,
						     false);
	    register.setValues(userReq.name,
			       userReq.address,
			       userReq.birthday,
			       userReq.telephone,
			       userReq.gender,
			       req.getStoreID(),
			       userReq.otherInfo);
	    conn.setAutoCommit(false);
	    register.update();
	    conn.commit();
	    res.printElement(CREATE_DATE_KEY,
			     formatDate(register.getCreateDate()));
	    res.printElement(REPLIED_EMAIL_KEY, register.getEmailAddress() +
			     register.getEmailDomain());
	    
	    res.printExitStatus(StatusCode.SC_OK);
	} catch (InvalidBBIDException e) {
	    res.printExitStatus(StatusCode.XS_BAD_BB_ID, e);    
	} catch (DBException e) {
	    res.printExitStatus(StatusCode.SC_DB_EXCEPTION, e);
	} finally {
	    // Even with auto-commit set to false, JDBC still commits
	    // any changes upon closing the connection.  So we have to
	    // explicitly rollback upon any error.
	    conn.rollback();
	}
    }

    
    String formatDate(Date date)
    {
	SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
	return fmt.format(date);
    }
	
}

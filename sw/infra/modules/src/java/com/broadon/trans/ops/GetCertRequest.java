package com.broadon.trans.ops;

import com.broadon.trans.common.TransactionRequest;

/**
 * Hold the data of a "get smart card certificate" request.
 */
public class GetCertRequest extends TransactionRequest
{
    /** User ID of the service technician. */
    protected String userID;

    /** Password of the service technician. */
    protected String password;

    /** ID of the operator. */
    protected String opID;

    /** Serial number of the smart card. */
    protected String serialNo;

    /** Transport Key of the smart card. */
    protected String transportKey;

    /** Unique ID of a smart card. */
    protected String smartcardID;
}

package com.broadon.trans.purchase;

import java.sql.*;

import com.broadon.trans.common.Logger;

/**
 * Logger for eCard redemption.  Typically, an instance of this class
 * is created upon receiving the redemption request and the info is
 * flushed to the audit log.  After the redemption process, this
 * object is put into the audit log queue again with the redemption
 * status updated.
 */
public class ECardRedemptionLog extends Logger
{
    static final String addLog =
	"INSERT INTO ECARD_REDEMPTION_LOGS (BB_ID, TITLE_ID, REQUEST_DATE, " +
	"    ECARD_ID_ENC) VALUES (?, ?, " + TIMESTAMP + ", ?)";

    static final String updateLog =
	"UPDATE ECARD_REDEMPTION_LOGS SET EUNITS_APPLIED=?, " +
	"    REQUEST_STATUS=?, REQUEST_LOG=? WHERE BB_ID=? AND TITLE_ID = ?" +
	"    AND REQUEST_DATE = " + TIMESTAMP + " AND ECARD_ID_ENC = ?";

    long bbId;
    long titleId;
    String ecardId;

    int eunits;
    String status;
    String message;

    boolean flushed;

    /**
     * Create an <code>ECardRdemptionLog</code> object holding the
     * values to be inserted into the audit log.
     */
    public ECardRedemptionLog(long bbId, long titleId, String eCardId)
    {
	super();
	
	this.bbId = bbId;
	this.titleId = titleId;
	this.ecardId = eCardId;

	flushed = false;
	
	eunits = 0;
	status = message = null;
    }


    synchronized public void flush(Connection conn) throws SQLException
    {
	PreparedStatement ps;
	int i = 1;

	if (flushed) {
	    // already written once, need to update the existing record
	    ps = conn.prepareStatement(updateLog);
	    ps.setInt(i++, eunits);
	    ps.setString(i++, status);
	    ps.setString(i++, message);
	} else {
	    ps = conn.prepareStatement(addLog);
	}


	ps.setLong(i++, bbId);
	ps.setLong(i++, titleId);
	ps.setLong(i++, timestamp);
	ps.setString(i++, ecardId);

	try {
	    ps.executeUpdate();
	} finally {
	    ps.close();
	}

	flushed = true;
    }

    /**
     * Set the status of the eCard redemption and possibly an error
     * message. 
     */
    synchronized public void setStatus(int eunits, String status, String message)
    {
	this.eunits = eunits;
	this.status = status;
	this.message = message;
    }
}

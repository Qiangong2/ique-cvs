package com.broadon.trans.xs;

import com.broadon.trans.common.TransactionRequest;

/**
 * Hold the data of a Depot configuration request.
 */
public class DepotConfigRequest extends TransactionRequest
{
    /** User ID of the service technician. */
    protected String userID;

    /** Password of the service technician. */
    protected String password;

    /** ID of the store where the Depot resides. */
    protected int storeID;
}

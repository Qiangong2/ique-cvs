package com.broadon.trans.installer;

import com.broadon.db.DBException;

/**
 * The <code>InvalidStoreIDException</code> class is thrown when
 * a Depot configuration request is rejected because the specified store ID
 * is invalid or revoked.
 * 
 *
 * @version	$Revision: 1.2 $
 */
public class InvalidStoreIDException
    extends DBException
{
    /**
     * Constructs a InvalidStoreIDException instance.
     */
    public InvalidStoreIDException()
    {
	super();
    }

    /**
     * Constructs a InvalidStoreIDException instance.
     *
     * @param	message			the exception message
     */
    public InvalidStoreIDException(String message)
    {
	super(message);
    }

    /**
     * Constructs a InvalidStoreIDException instance.
     *
     * @param	message			the exception message
     * @param	throwable		the nested exception
     */
    public InvalidStoreIDException(String message, Throwable throwable)
    {
	super(message, throwable);
    }
}

package com.broadon.trans.rma;

import java.sql.*;
import java.math.BigInteger;

import com.broadon.jni.BBcrypto;

public class BBexchange
{
    static final String validateOldBB =
	"SELECT BB_MODEL, SN, EXT_SN FROM BB_PLAYERS WHERE BB_ID = ? AND " +
	"   BU_ID = ? AND REVOKE_DATE IS NULL AND DEACTIVATE_DATE IS NULL";

    // locate all the content IDs of the original bundle for NEW.BB_ID
    static final String bundledCID =
	"SELECT A.CONTENT_ID FROM CONTENT_TITLE_OBJECTS A, BB_BUNDLES B, " +
	"    BB_PLAYERS C WHERE C.BB_ID = D.BB_ID AND " +
	"    B.START_DATE = C.BUNDLE_START_DATE AND B.BU_ID = C.BU_ID AND " +
	"    B.BB_MODEL = C.BB_MODEL AND A.TITLE_ID = B.TITLE_ID";

    // find out all the content IDs that are not part of the original ticket
    // bundle
    static final String purchasedContent =
	"SELECT CONTENT_ID FROM ETICKETS D WHERE D.BB_ID = ? AND " +
	"    CONTENT_ID NOT IN (" + bundledCID + ")";

    // check if the old and new BB are of the same model
    // (we don't check the deactivate_date, but revoke_date must not be set
    static final String validateNewBB =  
	"SELECT BB_MODEL, LAST_LR_TID, PUBLIC_KEY, SN, EXT_SN " +
	"FROM BB_PLAYERS WHERE BB_ID = ? AND REVOKE_DATE IS NULL";

    // read all valid content IDs currently owned by the old BB
    static final String readCID =
	"SELECT CONTENT_ID, RTYPE FROM ETICKETS WHERE BB_ID = ? AND " +
	"    REVOKE_DATE IS NULL";


    long oldBB;
    long newBB;
    int buID;
    Connection conn;

    String bbModel;
    String extSN;		// external serial number
    String SN;			// internal serial number
    int lrtid;
    int prtid = 0;
    long[] ownedContent;
    int[] rType;
    boolean isPCBswap;

    BigInteger newBBKey;
	

    public BBexchange(long oldBB, long newBB, int buID, Connection conn)
	throws SQLException, BadUserBBException, BadReplacementBBException
    {
	// TO DO: audit log

	this.oldBB = oldBB;
	this.newBB = newBB;
	this.buID = buID;
	this.conn = conn;

	verifyOldBB();
	readOldBBinfo();

	verifyNewBB();
    }
    

    void verifyOldBB() throws SQLException, BadUserBBException
    {
	PreparedStatement ps = conn.prepareStatement(validateOldBB);
	ps.setLong(1, oldBB);
	ps.setInt(2, buID);
	try {
	    ResultSet rs = ps.executeQuery();
	    if (rs.next()) {
		bbModel = rs.getString(1);
		SN = rs.getString(2);
		extSN = rs.getString(3);
		if (rs.wasNull())
		    extSN = null;
	    } else {
		throw new
		    BadUserBBException("Record for player  " + oldBB +
				       " cannot be located");
	    }
	    rs.close();
	} finally {
	    ps.close();
	}
    } // verifyOldBB
    

    void readOldBBinfo() throws SQLException
    {
	PreparedStatement ps =
	    conn.prepareStatement(readCID, ResultSet.TYPE_SCROLL_INSENSITIVE,
				  ResultSet.CONCUR_READ_ONLY);
	ps.setLong(1, oldBB);
	try {
	    ResultSet rs = ps.executeQuery();
	    if (rs.last()) {
		int size = rs.getRow();
		ownedContent = new long[size];
		rType = new int[size];
		rs.beforeFirst();
		for (int i = 0; i < size && rs.next(); ++i) {
		    ownedContent[i] = rs.getLong(1);
		    rType[i] = BBcrypto.parseRType(rs.getString(2));
		}
	    } else {
		ownedContent = new long[0];
		rType = new int[0];
	    }
	    rs.close();
	} finally {
	    ps.close();
	}
    }


    // checks that:
    // 1.  The old and new BB are of the same model
    // 2.  The new BB does not have any eTickets other than those included
    //     in the original bundle.
    void verifyNewBB()
	throws SQLException, BadReplacementBBException
    {
	PreparedStatement ps = conn.prepareStatement(validateNewBB);
	ps.setLong(1, newBB);
	try {
	    ResultSet rs = ps.executeQuery();
	    if (! rs.next() )
		throw new
		    BadReplacementBBException("Record for player " + newBB +
					      " cannot be located");
	    else if (! bbModel.equals(rs.getString(1)))
		throw new BadReplacementBBException("The two players are of different models");
	    else {
		lrtid = rs.getInt(2);
		newBBKey = new BigInteger(rs.getString(3), 32);
		String sn = rs.getString(4);
		String exsn = rs.getString(5);
		if (rs.wasNull())
		    isPCBswap = false;
		else
		    isPCBswap = exsn.equals(sn);
	    }
	    rs.close();
	} finally {
	    ps.close();
	}
	
	ps = conn.prepareStatement(purchasedContent);
	ps.setLong(1, newBB);
	try {
	    ResultSet rs = ps.executeQuery();
	    if (rs.next())
		throw new
		    BadReplacementBBException("Replacement player " + newBB +
					      " is not factory refurbished.");
	    rs.close();
	} finally {
	    ps.close();
	}
    }

    // ======================================================================


    static final String convertTime = "BBXML.ConvertTimeStamp(?)";

    static final String deleteTickets =
	"DELETE FROM ETICKETS WHERE BB_ID = ? AND CONTENT_ID IN " +
	"    (SELECT CONTENT_ID FROM ETICKETS WHERE BB_ID=?)";

    static final String revokeTickets =
	"UPDATE ETICKETS SET REVOKE_DATE = " + convertTime +
	"    WHERE BB_ID = ?";

    static final String moveBB =
	"{ CALL XSOPS.EXCHANGE_BBP(?, ?, " + convertTime + ") }";

    static final String moveTickets =
	"UPDATE ETICKETS SET BB_ID=?, TID=? WHERE BB_ID=? AND CONTENT_ID = ?";

    static final String moveRemainingTickets =
	"UPDATE ETICKETS SET BB_ID=? WHERE BB_ID=?";

    static final String whichBB = " WHERE BB_ID=?";
    static final String updateExtSN = ", EXT_SN=?";

    static final String updateNewBB =
	"UPDATE BB_PLAYERS SET BU_ID=?, REPLACED_BB_ID=?, " +
	"    REGIONAL_CENTER_ID=?, LAST_LR_TID=?, LAST_PR_TID=?, " +
	"    REVOKE_DATE=NULL, DEACTIVATE_DATE=NULL, " +
	"    LAST_ETDATE=" + convertTime;

    static final String updateOldBB =
	"UPDATE BB_PLAYERS SET DEACTIVATE_DATE = " + convertTime;

    static final String cleanupLog =
	"DELETE BBR.USLOG$_BB_PLAYERS WHERE BB_ID in (?, ?)";
    
    /**
     * @param timestamp Transaction time in milliseconds since Epoch
     */
    public void performExchange(long timestamp, int regionalID)
	throws SQLException
    {
	// delete/revoke all existing eTickets owned by newBB
	PreparedStatement ps = conn.prepareStatement(deleteTickets);
	ps.setLong(1, newBB);
	ps.setLong(2, oldBB);

	try {
	    ps.executeUpdate();
	    ps.close();

	    ps = conn.prepareStatement(revokeTickets);
	    ps.setLong(1, timestamp);
	    ps.setLong(2, newBB);
	    ps.executeUpdate();
	    ps.close();

	    // Clean up all related BB records
	    CallableStatement cs = conn.prepareCall(moveBB);
	    cs.setLong(1, newBB);
	    cs.setLong(2, oldBB);
	    cs.setLong(3, timestamp);
	    cs.executeUpdate();
	    cs.close();

	    // assigned new ticket ID
	    if (ownedContent.length > 0) {
		ps = conn.prepareStatement(moveTickets);
		ps.setLong(1, newBB);
		ps.setLong(3, oldBB);
		for (int i = 0; i < ownedContent.length; ++i) {
		    ps.setLong(4, ownedContent[i]);
		    if (rType[i] == BBcrypto.PR)
			ps.setInt(2, ++prtid);
		    else {
			++lrtid;
			if (lrtid >= 0x10000 || lrtid < 0x8000)
			    lrtid = 0x8000;
			ps.setInt(2, lrtid);
		    }
		    ps.addBatch();
		}
		ps.executeBatch();
		ps.close();
	    }

	    // move any remaining etickets, revoked or not, to the new BB
	    ps = conn.prepareStatement(moveRemainingTickets);
	    ps.setLong(1, newBB);
	    ps.setLong(2, oldBB);
	    ps.executeUpdate();
	    ps.close();


	    // Update new BB
	    ps = conn.prepareStatement(updateNewBB +
				       (isPCBswap ? updateExtSN : "") +
				       whichBB);
	    int i = 0;
	    ps.setInt(++i, buID);
	    ps.setLong(++i, oldBB);
	    ps.setInt(++i, regionalID);
	    ps.setInt(++i, lrtid);
	    ps.setInt(++i, prtid);
	    ps.setLong(++i, timestamp);
	    if (isPCBswap)
		ps.setString(++i, extSN);
	    ps.setLong(++i, newBB);
	    ps.executeUpdate();
	    ps.close();

	    // Update old BB
	    ps = conn.prepareStatement(updateOldBB +
				       (isPCBswap ? updateExtSN : "") +
				       whichBB);
	    i = 0;
	    ps.setLong(++i, timestamp);
	    if (isPCBswap)
		ps.setString(++i, SN);
	    ps.setLong(++i, oldBB);
	    ps.executeUpdate();
	    ps.close();

	    // clean up sync log
	    ps = conn.prepareStatement(cleanupLog);
	    ps.setLong(1, newBB);
	    ps.setLong(2, oldBB);
	    ps.executeUpdate();
	} finally {
	    ps.close();
	}
	
    } // performExchange


    public BigInteger getBBKey() {
	return newBBKey;
    }

    public String getBBModel() {
	return bbModel;
    }
}

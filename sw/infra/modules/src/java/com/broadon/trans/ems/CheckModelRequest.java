package com.broadon.trans.ems;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.text.ParsePosition;

import com.broadon.trans.common.TransactionRequest;

/**
 * Hold the data of a check model request.
 */
public class CheckModelRequest extends TransactionRequest
{
    /** Model number assigned to the new BB player. */
    protected String model;

    /** Business unit to which this BB player belong. */
    protected int buID;

    /** Starting date where the bundling information becomes effective. */
    protected Date bundledDate;

    protected String formatedBundledDate;

    protected void setBundledDate(String date) {
	formatedBundledDate = date;
	SimpleDateFormat fmt = new SimpleDateFormat("yyMMdd");
	bundledDate = new Date(fmt.parse(date, new ParsePosition(0)).getTime());
    }
}

package com.broadon.trans.common;

import javax.sql.DataSource;
import java.sql.*;

import com.broadon.util.Queue;

/**
 * Base class for audit log writing
 */
public class AuditLog extends Thread
{
    Queue requestQueue = null;
    DataSource ds;
    boolean running = false;


    /**
     * Create an <code>AuditLog</code> instance.
     * @param ds DataSource from which a database connection is obtained.
     */
    protected AuditLog(DataSource ds) {
	requestQueue = new Queue();
	requestQueue.start();
	this.ds = ds;
    }


    /**
     * Enter to the queue an event to be logged to the database.
     * @param log <code>Logger</code> object description the info to be logged.
     * @exception SQLException When the queue is shut down due to
     *		database connection error.
     */
    public void add(Logger log) throws SQLException
    {
	if (running)
	    requestQueue.enqueue(log);
	else
	    throw new SQLException("Audit log not running");
    }


    void process() 
    {
	try {
	    Logger log;

	    while ((log = (Logger) requestQueue.dequeue()) != null) {
		Connection conn = ds.getConnection();
		try {
		    conn.setAutoCommit(true);
		    log.flush(conn);
		} finally {
		    try {
			conn.close();
		    } catch (SQLException e) {}
		}
	    }
	} catch (SQLException e) {
	    e.printStackTrace(System.out);
	} 
    }


    /**
     * Thread entry point.  Automatically retry the database
     * connection when it is down.  The underlying
     * <code>DataSource</code> object is assumed to keep re-connection
     * upon failure until a certain threshold is reached, at which
     * point an <code>SQLException</code> is thrown.
     */
    public void run() {
	try {
	    running = true;
	    while (running) {
		process();
	    }
	} finally {
	    running = false;
	}
	System.out.println("Audit Log shutting down");
    } 

    
    protected void shutdown() {
	requestQueue.terminate();
	running = false;
    }

    public boolean isRunning() {
	return running;
    }
}

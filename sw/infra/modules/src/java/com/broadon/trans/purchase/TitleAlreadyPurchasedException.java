package com.broadon.trans.purchase;

import com.broadon.db.DBException;

/**
 * The <code>TitleAlreadyPurchaseException</code> class is thrown when
 * a purchase request is rejected because the specified BB Player
 * already owns that title.
 * 
 *
 * @version	$Revision: 1.2 $
 */
public class TitleAlreadyPurchasedException
    extends DBException
{
    /**
     * Constructs a TitleAlreadyPurchasedException instance.
     */
    public TitleAlreadyPurchasedException()
    {
	super();
    }

    /**
     * Constructs a TitleAlreadyPurchasedException instance.
     *
     * @param	message			the exception message
     */
    public TitleAlreadyPurchasedException(String message)
    {
	super(message);
    }

    /**
     * Constructs a TitleAlreadyPurchasedException instance.
     *
     * @param	message			the exception message
     * @param	throwable		the nested exception
     */
    public TitleAlreadyPurchasedException(String message, Throwable throwable)
    {
	super(message, throwable);
    }
}

package com.broadon.trans.ems;

import com.broadon.trans.common.TransactionRequest;

/**
 * Hold the data of a purchase request.
 */
public class CreatePlayerRequest extends TransactionRequest
{
    /** Encoded ID of the BB player making the purchase. */
    protected long bbID;

    /** EMS depot's ID */
    protected long depotID;

    /** Model number assigned to the new BB player. */
    protected String model;

    /** Hardware revision of the new BB player. */
    protected long hw_rev;

    /** Serial number of this BB player. */
    protected String serialNo;

    /** External serial number of this BB player. */
    protected String extSerialNo;

    /** Business unit to which this BB player belong. */
    protected int buID;

    /** Starting date where the bundling information becomes effective. */
    protected String bundledDate;

    /** Starting ticket id to be used for limited rights tickets */
    protected int tid;
}

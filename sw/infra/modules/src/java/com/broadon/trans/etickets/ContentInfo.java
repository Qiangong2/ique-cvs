package com.broadon.trans.etickets;

import java.sql.*;

/**
 * Retrieve all information of a piese of content needed to compose an eTicket.
 */
public class ContentInfo
{
    static final String getContentInfo =
	"SELECT A.ETICKET_METADATA, A.CHAIN_ID, B.CRL_VERSION, C.CHAIN_ID, " +
	"    C.KEY_NAME, D.DESCRIPTION, E.ETICKET_OBJECT " +
	"FROM CONTENT_ETICKET_METADATA A, CURRENT_CRLS B, " +
	"    REGIONAL_HSM_CERTS C, CERTIFICATE_CHAINS D, CONTENT_OBJECTS E " +
	"WHERE A.CONTENT_ID = ? AND C.REGIONAL_CENTER_ID = ? AND " +
	"    C.CERT_TYPE = E.CONTENT_OBJECT_TYPE AND B.CRL_TYPE = 'XSCRL' " +
	"    AND E.CONTENT_ID = A.CONTENT_ID AND D.CHAIN_ID = C.CHAIN_ID";

    /** Content ID. */
    protected final long contentID;

    /** Content meta-data. */
    protected final byte[] metadataCommon;
    protected final byte[] metadataCustom;

    /** eTicket CRL version. */
    protected final int crlVersion;

    /** CA chain ID of the eTicket issuer. */
    protected final int eTicketChainID;

    /** CA chain ID of the content publisher. */
    protected final int publisherChainID;

    /** ID of the signer in the HSM. */
    protected final String keyName;

    /** Name of eTicket issuer. */
    protected final String issuerName;


    /**
     * Read from the database all relevant information of a pieie of
     * content for the composition of the corresonding eTicket.
     * @param conn Database connection.
     * @param contentID ID of the content.
     * @param regionalCenter Regional center ID:  this is used to
     * identify the signer used for issuing the eTicket.
     * @exception SQLException Any database access error.
     */
    protected ContentInfo(Connection conn, long contentID, int regionalCenter)
	throws SQLException, ETicketCreationException
    {
	this.contentID = contentID;

	PreparedStatement ps = conn.prepareStatement(getContentInfo);
	ps.setLong(1, contentID);
	ps.setInt(2, regionalCenter);
	try {
	    ResultSet rs = ps.executeQuery();

	    if (! rs.next()) {
		rs.close();
		throw new ETicketCreationException("Cannot find info for content " + contentID);
	    }

	    int i = 0;
	    Blob b = rs.getBlob(++i);
	    metadataCustom = (b != null) ? b.getBytes(1, (int) b.length()) : null;
	    publisherChainID = rs.getInt(++i);
	    crlVersion = rs.getInt(++i);
	    eTicketChainID = rs.getInt(++i);
	    keyName = rs.getString(++i);
	    issuerName = rs.getString(++i);
	    b = rs.getBlob(++i);
	    metadataCommon = (b != null) ? b.getBytes(1, (int) b.length()) : null;

	    rs.close();
	} finally {
	    ps.close();
	}
    }
}

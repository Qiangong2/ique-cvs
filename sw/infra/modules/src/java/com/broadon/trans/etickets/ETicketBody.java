package com.broadon.trans.etickets;

import java.math.BigInteger;
import java.sql.*;
import java.io.IOException;

import com.broadon.hsm.HSMClient;
import com.broadon.exception.InvalidRequestException;
import com.broadon.jni.BBcrypto;

/**
 * Generate the actual bits of an eTicket.
 */
public class ETicketBody implements ETicketConstants
{
    long bbID;
    BigInteger bbKey;		// public key of the BB player
    long contentID;
    int ticketID;
    int LimitedRightsType;     
    short limit;

    HSMClient hsm;		// hardware security module

    int regCenter;		// regional center ID

    byte[] body;		// body of the eTicket
    int eTicketChainID;
    int publisherChainID;

    /**
     * Construct an <code>ETicketBody</code> object with initial
     * parameters.
     * @param bbID Encoded ID of the BB player that owns the eTickets.
     * @param bbKey Public key of this BB player.
     * @param contentID Identify the content for which an eTicket
     * should be created.
     * @param ticketID Unique ID assigned to each ticket.
     * @param lrt Limited rights type.
     * @param limit The quantity of limited rights allowed.
     * @param hsm Connector to the Hardware Security Module.
     */
    public ETicketBody(long bbID, BigInteger bbKey, long contentID,
		       int ticketID, int lrt, short limit,
		       HSMClient hsm, int regionalCenterID) {
	setParameters(bbID, bbKey, contentID, ticketID, lrt, limit);
	this.hsm = hsm;
	regCenter = regionalCenterID;
    }


    /**
     * Set up (or reset) the object for generating the next eTicket.
     * @param bbID Encoded ID of the BB player that owns the eTickets.
     * @param bbKey Public key of this BB player.
     * @param contentID Identify the content for which an eTicket
     * should be created.
     * @param ticketID Unique ID of this eTicket.
     * @param lrt Limited rights type.
     * @param limit The quantity of limited rights allowed.
     */
    public void setParameters(long bbID, BigInteger bbKey, long contentID,
			      int ticketID, int lrt, short limit) {
	this.bbID = bbID;
	this.bbKey = bbKey;
	this.contentID = contentID;
	this.ticketID = ticketID;
	this.LimitedRightsType = lrt;
	this.limit = limit;
    }


    /**
     * Prepare the object for generating another eTicket for the same
     * BB player.
     * @param contentID Identify the content for which an eTicket
     * should be created.
     */
    public void setContentID(long contentID) {
	this.contentID = contentID;
    }

    /**
     * Prepare the object for generating another eTicket for the same
     * BB player.
     * @param ticketID Unique ID of this ticket.
     */
    public void setTicketID(int ticketID) {
	this.ticketID = ticketID;
    }

    public void setLimitedRightsType(int type) {
	this.LimitedRightsType = type;
    }

    public void setLimit(short limit) {
	this.limit = limit;
    }


    /**
     * Generate the actual bits of an eTicket for the specified
     * content.
     * @param conn Database connection used for retrieving the
     * relevant content info.
     * @exception SQLException Database access error.
     * @exception ETicketCreationException Cannot generate the eTicket
     * because of missing or inconsistent data.
     */
    void generate(Connection conn, boolean getEticketInFull)
	throws SQLException, ETicketCreationException
    {
	ContentInfo content = new ContentInfo(conn, contentID, regCenter);

	eTicketChainID = content.eTicketChainID;
	publisherChainID = content.publisherChainID;
	
	if (LimitedRightsType != BBcrypto.PR &&
	    (ticketID < MIN_TID || ticketID > MAX_TID))
	    throw new ETicketCreationException("Invalid limited rights ticket id " + ticketID);

	try {
	    body = BBcrypto.genTicket(content.metadataCommon,
                                  content.metadataCustom,
                                  bbID,
                                  content.crlVersion,
                                  bbKey,
                                  content.issuerName,
                                  hsm,
                                  content.keyName,
                                  ticketID,
                                  LimitedRightsType,
                                  limit,
                                  getEticketInFull);
	} catch (IOException e) {
	    throw new ETicketCreationException("Cannot sign eTicket");
	} catch (InvalidRequestException e) {
	    throw new ETicketCreationException(e.getMessage());
	}
    } // generate
    

    /** @return the actual bits of the eTicket just created. */
    public byte[] getBody() {
	return body;
    }

    /** @return The ID of the CA cert chain of the eTicket signing
	certificate */
    public int getETicketChainID() {
	return eTicketChainID;
    }

    /** @return The ID of the CA cert chain of the publisher signing
	certificate */
    public int getPublisherChainID() {
	return publisherChainID;
    }
}

package com.broadon.trans.ops;

import java.sql.*;
import java.util.Date;
import java.security.GeneralSecurityException;

import com.broadon.security.CertInfo;
import com.broadon.security.X509;
import com.broadon.db.DBException;


public final class SmartCard
{
    static final String getPassword =
	"SELECT OPERATOR_ID, PASSWD FROM OPERATION_USERS WHERE LOWER(EMAIL_ADDRESS) = ?";

    static final String updatePassword =
	"UPDATE OPERATION_USERS SET PASSWD = ? WHERE LOWER(EMAIL_ADDRESS) = ?";

    static final String updateCertHash =
	"UPDATE OPERATION_USERS SET OTHER_INFO = ? WHERE LOWER(EMAIL_ADDRESS) = ?";

    static final String getRoles =
	"SELECT B.ROLE_NAME, C.BU_ID FROM OPERATION_USERS A, " +
	"    OPERATION_ROLES B, OPERATION_USER_ROLES C WHERE " +
	"LOWER(A.EMAIL_ADDRESS) = ? AND A.OPERATOR_ID = C.OPERATOR_ID AND " +
	"C.ROLE_LEVEL = B.ROLE_LEVEL ORDER BY C.BU_ID, B.ROLE_NAME";
    
    static final String insertSC =
	"INSERT INTO SMARTCARDS (OWNER_ID, SN, PUK, PIN, SECRET_CODE, " +
	"    CHAIN_ID, SERIAL_NO, CN, PUBLIC_KEY, CERTIFICATE, " +
	"    SERVER_DH_PRIVATE_KEY, SMARTCARD_ID) VALUES " +
	"    (?,?,?,?,?,?,?,?,?,?,?,?)";

    static final String updateSC =
	"UPDATE SMARTCARDS SET OWNER_ID=?, SN=?, PUK=?, PIN=?, " +
	"    SECRET_CODE=?, CHAIN_ID=?, SERIAL_NO=?, CN=?, PUBLIC_KEY=?, " +
	"    CERTIFICATE=?, SERVER_DH_PRIVATE_KEY=?, REVOKE_DATE = NULL, " +
	"    ISSUE_DATE=SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) " +
	"WHERE SMARTCARD_ID = ?";
    
    static final String revokeSC =
	"UPDATE SMARTCARDS SET REVOKE_DATE=SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)" +
	"    WHERE SMARTCARD_ID = ? AND REVOKE_DATE IS NULL";

    static final String revokeOtherSC =
	"UPDATE SMARTCARDS SET REVOKE_DATE=SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)" +
	"    WHERE OWNER_ID = ? AND SMARTCARD_ID != ? AND " +
	"    REVOKE_DATE IS NULL";
    
    static final String checkSC =
	"SELECT 1 FROM SMARTCARDS WHERE SMARTCARD_ID = ?";

    Connection conn;
    String email;
    long opID;			// operator ID
    long smartcardID;
    boolean isNewSmartCard;
    String pin;				
    String certCN;


    protected SmartCard(Connection conn, String email, String smartcardID)
	throws SQLException, DBException
    {
	this.conn = conn;
	this.email = email;

	if (smartcardID != null) {
	    this.smartcardID = Long.parseLong(smartcardID);
	    PreparedStatement ps = conn.prepareStatement(checkSC);
	    ps.setLong(1, this.smartcardID);
	    try {
		ResultSet rs = ps.executeQuery();
		if (rs.next())
		    isNewSmartCard = false;
		else
		    smartcardID = null;
		rs.close();
	    } finally {
		ps.close();
	    }
	}
	if (smartcardID == null) {
	    isNewSmartCard = true;
	    this.smartcardID = (new Date()).getTime();
	}
	
	pin = getOperatorPassword();
	certCN = genCertName();
    }


    String getOperatorPassword() throws SQLException, DBException
    {
	PreparedStatement ps = conn.prepareStatement(getPassword);
	ps.setString(1, email);
	try {
	    ResultSet rs = ps.executeQuery();
	    if (rs.next()) {
		opID = rs.getLong(1);
		String pw = rs.getString(2);
		// check if it is exactly 8 decimal digits 
		if (pw.length() != 8)
		    return null;
		try {
		    int n = Integer.parseInt(pw);
		    if (n < 0 || n > 99999999)
			return null;
		} catch (NumberFormatException e) {
		    return null;
		}
		return pw;
	    } else
		throw new DBException("Invalid Operator ID");
	} finally {
	    ps.close();
	}
    } // getOperatorPassword
    

    protected void updateOperatorPassword(String newPassword)
	throws SQLException
    {
	PreparedStatement ps = conn.prepareStatement(updatePassword);
	ps.setString(1, newPassword);
	ps.setString(2, email);
	try {
	    ps.executeUpdate();
	} finally {
	    ps.close();
	}
    }

    String genCertName() throws SQLException
    {
	String name = email;

	PreparedStatement ps = conn.prepareStatement(getRoles);
	ps.setString(1, email);
	try {
	    ResultSet rs = ps.executeQuery();
	    int lastBUID = -1;
	    while (rs.next()) {
		String role = rs.getString(1);
		if (role == null || ! role.matches("^[a-zA-Z]$"))
		    continue;
		
		int buID = rs.getInt(2);
		if (buID != lastBUID) {
		    name += ":BU=" + buID + "&ROLES=";
		    lastBUID = buID;
		}
		name += role;
	    }
	    rs.close();
	} finally {
	    ps.close();
	}
	return name;
    } // genCertName

    
    void setupStmt(PreparedStatement ps, GetCertRequest req, String puk,
		   CertInfo cert, String publicKey, long serialNo,
		   String privateKey)
	throws SQLException
    {
	int i = 1;
	ps.setLong(i++, opID);
	ps.setString(i++, req.serialNo);
	ps.setString(i++, puk);
	ps.setString(i++, new String(cert.getPassword())); // PIN
	ps.setString(i++, req.transportKey); // secret code
	ps.setString(i++, new String(cert.getChainID()));
	ps.setLong(i++, serialNo);
	ps.setString(i++, certCN);
	ps.setString(i++, publicKey);
	ps.setString(i++, new String(cert.getCert()));
	ps.setString(i++, privateKey);
	ps.setLong(i, smartcardID);
    }
	

    String getCertHash(byte[] cert)
	throws GeneralSecurityException
    {
	X509 x509 = new X509();
	return x509.genUniqueID(x509.readEncodedX509(new String(cert)));
    }

    protected void updateDB(GetCertRequest req, String puk, CertInfo cert,
			    String publicKey, long serialNo, String privateKey)
	throws SQLException, DBException, GeneralSecurityException
    {
	PreparedStatement ps = null;

	try {
	    if (isNewSmartCard) {
		ps = conn.prepareStatement(insertSC);
		setupStmt(ps, req, puk, cert, publicKey, serialNo, privateKey);
		ps.executeUpdate();
	    } else {
		// explicitly revoke the old record, which triggers the DB to
		// back it up.
		ps = conn.prepareStatement(revokeSC);
		ps.setLong(1, smartcardID);
		ps.executeUpdate();
		ps.close();

		ps = conn.prepareStatement(updateSC);
		setupStmt(ps, req, puk, cert, publicKey, serialNo, privateKey);

		if (ps.executeUpdate() == 0)
		    // record does not exist, smart id is bogus
		    throw new DBException("Invalid smart card ID");
	    }
	    // invalidate any other smart card owned by the same
	    // user, just in case.
	    ps.close();
	    ps = conn.prepareStatement(revokeOtherSC);
	    ps.setLong(1, opID);
	    ps.setLong(2, smartcardID);
	    ps.executeUpdate();

	    // Put the MD5 hash of the cert in the "other_info" field
	    // of this operator
	    ps.close();
	    ps = conn.prepareStatement(updateCertHash);
	    ps.setString(1, getCertHash(cert.getCert()));
	    ps.setString(2, email);
	    ps.executeUpdate();
			 
	} finally {
	    if (ps != null)
		ps.close();
	}
    }


    /** @return the (possibly new) smart card ID */
    protected long getSmartCardID() {
	return smartcardID;
    }

    /** @return the PIN of the smart card. */
    protected String getPIN() {
	return pin;
    }

    /** @return the common name of the certificate in the smart card. */
    protected String getCN() {
	return certCN;
    }
}

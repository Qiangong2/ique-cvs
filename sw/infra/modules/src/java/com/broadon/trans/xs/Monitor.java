package com.broadon.trans.xs;

import java.math.BigInteger;
import javax.servlet.http.*;
import java.util.Enumeration;
import java.io.IOException;
import java.sql.*;

import com.broadon.filter.*;
import com.broadon.servlet.ServletConstants;
import com.broadon.servlet.ContextStatus;

import com.broadon.trans.common.*;
import com.broadon.trans.etickets.*;


public class Monitor extends TransactionServlet implements ServletConstants
{
    static final String BBXML =
	"SELECT BBXML.ConvertTimestamp(SYSDATE) FROM DUAL";

    protected void process(Connection conn, authWrapper req,
                           TransactionRequest transReq, XMLResponse res)
	throws SQLException
    {
	ContextStatus status = (ContextStatus) ctx.getAttribute(MONITOR_STATUS_KEY);
	
	try {
	    if (! auditLog.isRunning() ||
		status.getOperationStatus() == status.OS_LB_NOTIFIED) {
		
		res.error(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		status.setHealth(false);
		return;
	    }
	    

	    BigInteger dummyKey = new BigInteger("e9objrup5tg4s4ahe5pue439pqcsg3mvj2gofo8g08igd8600005v0kdtsaied3ab0e9qnoah47rcjnf6na1sv1m6e2dh8r6h8", 32);

	    ETickets tickets = new ETickets(conn, hsm, 0L, dummyKey,
					    regionalCenterID,
					    ETickets.MIN_TID,
					    getBUID(conn, regionalCenterID));
	    tickets.generateAll();
	    Enumeration e = tickets.getTickets();
	    if (! e.hasMoreElements()) {
		res.error(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		status.setHealth(false);
	    } else {
		// test the store procedure (for invalidated cache)
		PreparedStatement ps = conn.prepareStatement(BBXML);
		try {
		    ps.executeQuery();
		} finally {
		    ps.close();
		}
		status.setHealth(true);
	    }
	} catch (Exception e) {
	    status.setHealth(false);
	    e.printStackTrace(System.out);
	    try {
		res.error(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
	    } catch (IOException ioe) {
		throw new IllegalStateException(ioe.getMessage());
	    }
	} 
    }

    protected TransactionRequest readInputs(XMLResponse res,
                                            HttpServletRequest req)
    {
	return null;
    }

    static final String GET_BUID =
	"SELECT DISTINCT A.BU_ID FROM BUSINESS_UNITS A, REGIONS B, STORES C" +
	"    WHERE C.REGIONAL_CENTER_ID=? AND C.REGION_ID = B.REGION_ID AND " +
	"    B.BU_ID = A.BU_ID";
	 

    int getBUID(Connection conn, int regionalCenterID) throws SQLException
    {
	PreparedStatement ps = conn.prepareStatement(GET_BUID);
	ps.setInt(1, regionalCenterID);
	try {
	    ResultSet rs = ps.executeQuery();
	    if (rs.next()) {
		return rs.getInt(1);
	    }
	    return -1;
	} finally {
	    ps.close();
	}
    }

}

package com.broadon.trans.etickets;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;

import com.broadon.hsm.HSMClient;
import com.broadon.jni.BBcrypto;

/**
 * Interface to generate all eTickets owned by the given BB player.
 */
public class ETickets implements ETicketConstants
{
    static final String getTickets =
	"SELECT A.CONTENT_ID, A.TID, A.RTYPE, A.TOTAL_LIMITS FROM " +
	"    ETICKETS A, CONTENT_OBJECTS B WHERE " +
	"    BB_ID = ? AND A.REVOKE_DATE IS NULL AND B.REVOKE_DATE IS NULL " +
	"    AND A.CONTENT_ID = B.CONTENT_ID AND " +
	"    (A.TID < " + MIN_TID + " OR A.TID >= ?)";
    
    static final String getOneCert =
	"SELECT CERTIFICATE FROM CERTIFICATES A WHERE EXISTS " +
	"  (SELECT 1 FROM CERTIFICATE_CHAINS B WHERE " +
	"     A.CERT_ID IN (B.SIGNER_CERT_ID, B.CA_CERT_ID) AND " +
	"     B.CHAIN_ID = ?)";

    static final String getAllCerts_start =
	"SELECT CERTIFICATE FROM CERTIFICATES A WHERE EXISTS " +
	"  (SELECT 1 FROM CERTIFICATE_CHAINS B WHERE " +
	"     A.CERT_ID IN (B.SIGNER_CERT_ID, B.CA_CERT_ID) AND " +
	"     B.CHAIN_ID IN (";

    static final String getAllCerts_end = "))";
    
    static final String getGlobalTickets =
	"SELECT CONTENT_ID, TID FROM GLOBAL_ETICKETS WHERE " +
	"  REVOKE_DATE IS NULL AND BU_ID = ?";

    ResultSet getOneCert(int chainID, Connection conn)
	throws SQLException
    {
	PreparedStatement ps = conn.prepareStatement(getOneCert);
	ps.setInt(1, chainID);
	try {
	    return ps.executeQuery();
	} finally {
	    ps.close();
	}
    } // getOneCert

    
    ResultSet getAllCerts(TreeSet chainIDs, Connection conn)
	throws SQLException
    {
	// is there a way to bind a value list to a prepared
	// statement instead of composing it on the fly?
	    
	StringBuffer sqlStmt = new StringBuffer(getAllCerts_start);
	Iterator i = chainIDs.iterator();
	sqlStmt.append(i.next().toString());
	while (i.hasNext()) {
	    sqlStmt.append(',');
	    sqlStmt.append(i.next());
	}
	sqlStmt.append(getAllCerts_end);
	
	Statement stmt = conn.createStatement();
	return stmt.executeQuery(sqlStmt.toString());
    } // getAllCerts


    void getCert(TreeSet chainIDs, Connection conn)
	throws SQLException
    {
	ResultSet rs;
	
	if (chainIDs.size() == 1) {
	    rs = getOneCert(((Integer) chainIDs.first()).intValue(), conn);
 	} else if (chainIDs.size() == 0) {
	    return;
	} else {
	    rs = getAllCerts(chainIDs, conn);
	}

	while (rs.next()) {
	    addCACert(rs.getString(1));
	}
	rs.close();
    } // getCert


    /**
     * Generate all eTickets owned by the specified BB player.
     * @exception SQLException Database access error.
     * @exception ETicketCreationException Cannot generate the eTicket
     * because of missing or inconsistent data.
     */
    public void generateAll(boolean getEticketInFull)
	throws SQLException, ETicketCreationException
    {
	ETicketBody newTicket =
	    new ETicketBody(bbID, bbKey, -1L, -1, 0, (short) 0, hsm,
			    regionalCenterID);
	
	// for collecting unique set of chain IDs
	TreeSet chainIDs = new TreeSet();

	PreparedStatement ps = conn.prepareStatement(getTickets);
	ps.setLong(1, bbID);
	ps.setInt(2, minTID);
	try {
	    ResultSet rs = ps.executeQuery();


	    while (rs.next()) {
		// generate eTicket
		long cid = rs.getLong(1);
		int tid = rs.getInt(2);
		int rightsType = BBcrypto.parseRType(rs.getString(3));
		short limit = rs.getShort(4);

		newTicket.setContentID(cid);
		newTicket.setTicketID(tid);
		newTicket.setLimitedRightsType(rightsType);
		newTicket.setLimit(limit);
		newTicket.generate(conn, getEticketInFull);
		addTicket(newTicket.getBody());
		chainIDs.add(new Integer(newTicket.getETicketChainID()));
		chainIDs.add(new Integer(newTicket.getPublisherChainID()));
	    }
	    rs.close();
	} finally {
	    ps.close();
	}

	// Now, get all the global etickets.

	ps = conn.prepareStatement(getGlobalTickets);
	ps.setInt(1, buID);
	try {
	    ResultSet rs = ps.executeQuery();

	    while (rs.next()) {
		long cid = rs.getLong(1);
		int tid = rs.getInt(2);
	    
		newTicket.setContentID(cid);
		newTicket.setTicketID(tid);
		newTicket.setLimitedRightsType(BBcrypto.PR);
		newTicket.setLimit((short)0);
		newTicket.generate(conn, getEticketInFull);
		addTicket(newTicket.getBody());
		chainIDs.add(new Integer(newTicket.getETicketChainID()));
		chainIDs.add(new Integer(newTicket.getPublisherChainID()));
	    }
	} finally {
	    ps.close();
	}
	
	// Read all the CA certs
	getCert(chainIDs, conn);
    } // generateAll


    /**
     * Generate invariant body of all eTickets owned by the specified
     * BB player.
     * @exception SQLException Database access error.
     * @exception ETicketCreationException Cannot generate the eTicket
     * because of missing or inconsistent data.
     */
    public void generateAll()
        throws SQLException, ETicketCreationException
    {
        generateAll(false);
    }


    /**
     * Generate an eTicket.
     * @param contentID ID of the content for which an eTicket is needed.
     * @param ticketID ID of the ticket to be issued.
     * @param lrt Limited rights type.
     * @param limit Amount of limited rights granted.
     * @exception SQLException Database access error.
     * @exception ETicketCreationException Cannot generate the eTicket
     * because of missing or inconsistent data.
     */
    public void generate(long[] contentID, 
                         int[] ticketID, 
                         int[] lrt,
                         short[] limit, 
                         boolean getEticketInFull)
	throws SQLException, ETicketCreationException
    {
	ETicketBody newTicket = new ETicketBody(bbID, bbKey, -1L, -1, 0,
						(short) 0, hsm,
						regionalCenterID);

	// for collecting unique set of chain IDs
	TreeSet chainIDs = new TreeSet();

	for (int i = 0; i < contentID.length; ++i) {
	    newTicket.setContentID(contentID[i]);
	    newTicket.setTicketID(ticketID[i]);
	    newTicket.setLimitedRightsType(lrt[i]);
	    newTicket.setLimit(limit[i]);
	    newTicket.generate(conn, getEticketInFull);
	    addTicket(newTicket.getBody());
	    chainIDs.add(new Integer(newTicket.getETicketChainID()));
	    chainIDs.add(new Integer(newTicket.getPublisherChainID()));
	}
	
	getCert(chainIDs, conn);
    } // generate


    /**
     * Generate a (default) permanent eTickets.
     */
    public void generate(long[] contentID, 
                         int[] ticketID, 
                         boolean getEticketInFull)
	throws SQLException, ETicketCreationException
    {
	int size = contentID.length;
	int[] lrt = new int[size];
	short[] limit = new short[size];
	for (int i = 0; i < size; ++i) {
	    lrt[i] = BBcrypto.PR;
	    limit[i] = 0;
	}
	generate(contentID, ticketID, lrt, limit, getEticketInFull);
    }

    
    //----------------------------------------------------------------------

    Vector ticketRecords;
    Vector caCerts;

    Connection conn;
    HSMClient hsm;
    long bbID;
    BigInteger bbKey;
    int regionalCenterID;
    int buID;
    int minTID;

    /**
     * Create an <code>ETickets</code> object for generation of eTickets.
     * @param conn Database connection to be used.
     * @param hsm Handle to the hardware security module for signing etickets
     * @param bbID Encoded ID of the BB player.
     * @param bbKey Public Key of this BB player.
     * @param regionalCenterID Regional center of this eTicket server.
     * @param minTID Minimum TID allowed by BB player.
     * @param buID Business Unit
     */
    public ETickets(Connection conn, HSMClient hsm, long bbID,
		    BigInteger bbKey, int regionalCenterID, int minTID,
		    int buID)
    {
	ticketRecords = new Vector();
	caCerts = new Vector();
	this.conn = conn;
	this.hsm = hsm;
	this.bbID = bbID;
	this.bbKey = bbKey;
	this.regionalCenterID = regionalCenterID;
	this.minTID = minTID;
	this.buID = buID;
    }

    void addTicket(byte[] body) {
	ticketRecords.add(body);
    }


    /** @return Handle for retrieving all the eTickets, each of which
	is a byte array. */
    public Enumeration getTickets() {
	return ticketRecords.elements();
    }

    void addCACert(String cert) {
	caCerts.add(cert);
    }

    /** @return Handle for retrieving all the CA chains, each of which
	is a base64-encoded string. */
    public Enumeration getCACerts() {
	return caCerts.elements();
    }
}

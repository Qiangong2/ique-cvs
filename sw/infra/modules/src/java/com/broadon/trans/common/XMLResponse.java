package com.broadon.trans.common;

import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.io.IOException;

import com.broadon.util.Xml;

/**
 * Format transaction response as XML elements.
 */
public class XMLResponse
{
    static final String STATUS_KEY = "status";
    static final String STATUS_MSG_KEY = "status_msg";

    static {
	try {
	    Class.forName("com.broadon.trans.common.StatusCode");
	} catch (ClassNotFoundException e) {
	}
    }


    HttpServletResponse res;
    PrintWriter out;
    String rootTag;

    String exitStatusCode;
    String exitStatusMsg;

    /**
     * @param out <code>PrintWriter</code> used for the output.
     */
    protected XMLResponse(HttpServletResponse res) throws IOException
    {
	this.res = res;
	this.out = res.getWriter();
	this.rootTag = null;
	exitStatusCode = null;
	exitStatusMsg = null;
    }

    /**
     * Print an XML root tag.
     * @param rootTag Name of the root tag.
     */
    public void startDoc(String rootTag) {
	this.rootTag = rootTag;
	out.println('<' + rootTag + '>');
    }

    /**
     * Print an XML element.
     * @param tag Name of the element tag.
     * @param value Value of the element.
     */
    public void printElement(String tag, String value) {
	out.println("<" + tag + ">" +
		    ((value == null) ? "" : Xml.encode(value)) +
		    "</" + tag + ">");
    }

    /**
     * Print the closing root tag.
     */
    public void endDoc() {
	out.println("</" + rootTag + '>');
	rootTag = null;
    }


    void printExitStatus(String code, String msg) {
	exitStatusCode = code;
	exitStatusMsg = msg;
	printElement(STATUS_KEY, code);
	printElement(STATUS_MSG_KEY, msg);
	endDoc();
    }
	

    /**
     * Print a <code>status</code> element and then the closing root tag.
     * @param code Status code to be used.
     */
    public void printExitStatus(String code) {
	printExitStatus(code, StatusCode.getMessage(code));
    }

    /**
     * Print a <code>status</code> element and then the closing root tag.
     * @param code Status code to be used.
     * @param e Exception that causes an error Status.  The
     * exception's message will be printed as addition status message.
     */
    public void printExitStatus(String code, Exception e) {
	printExitStatus(code,
			StatusCode.getMessage(code) + ": " + e.toString());
    }


    /** @return the exit status code sent. */
    public String getExitStatusCode() {
	return exitStatusCode;
    }

    /** @return the exit status message sent. */
    public String getExitStatusMsg() {
	return exitStatusMsg;
    }

    public void error(int code) throws IOException
    {
	res.sendError(code);
    }
}

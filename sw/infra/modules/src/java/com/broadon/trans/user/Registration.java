package com.broadon.trans.user;

import java.sql.*;
import java.util.Random;

import com.broadon.db.DBException;

/**
 * Read/Write customer registation info from/to the database.
 */
public class Registration
{
    static final String readUser =
	"SELECT EMAIL_ADDRESS, NAME, ADDRESS, BIRTHDATE, TELEPHONE, GENDER, " +
	"    STORE_ID, BBXML.ConvertTimeStamp(CREATE_DATE), MISC FROM " +
	"CUSTOMER_REGISTRATIONS WHERE BB_ID = ?";

    static final String oldUser =
	"SELECT BBXML.ConvertTimeStamp(CREATE_DATE), EMAIL_ADDRESS FROM " +
	"CUSTOMER_REGISTRATIONS WHERE BB_ID = ?";

    static final String deleteUser =
	"DELETE FROM CUSTOMER_REGISTRATIONS WHERE BB_ID = ?";

    static final String updateUser =
	"UPDATE CUSTOMER_REGISTRATIONS SET NAME = ?, ADDRESS = ?, " +
	"    BIRTHDATE = ?, TELEPHONE = ?, GENDER = ?, STORE_ID = ?, " +
	"    MISC = ? WHERE BB_ID = ?";

    static final String insertUser =
	"INSERT INTO CUSTOMER_REGISTRATIONS (EMAIL_ADDRESS, NAME, ADDRESS, " +
	"    BIRTHDATE, TELEPHONE, GENDER, BB_ID, STORE_ID, CREATE_DATE, MISC)" +
	"VALUES (?, ?, ?, ?, ?, ?, ?, ?, BBXML.ConvertTimeStamp(?), ?)";

    static final String getMaxEmail =
	"SELECT MAX(TO_NUMBER(SUBSTR(EMAIL_ADDRESS, ?, 10))) FROM " +
	"    CUSTOMER_REGISTRATIONS WHERE EMAIL_ADDRESS LIKE ?";
	
    
    static Random rand;

    static {
	rand = new Random(new java.util.Date().getTime());
    };

    static final String emailDomain = "@iQue.net";

    String emailAddress;
    String name;
    String address;
    Date birthdate;
    String telephone;
    String gender;
    long bbID;
    int storeID;
    Date createDate;
    String misc;

    Connection conn;
    boolean recordExists;
    

    /**
     * Construct an <code>Registration</code> object.
     * @param conn Database connection to be used.
     * @param bbID ID of the BB player.
     * @param emailAddress Proposed email address.
     * @param readAll Set to <code>true</code> causes the existing
     * record to be read, otherwise, just verify if an record already
     * exists for this BB player.
     * @exception SQLException
     */
    public Registration(Connection conn, long bbID, String emailAddress,
			boolean readAll)
	throws SQLException
    {
	this.bbID = bbID;
	this.emailAddress = emailAddress;
	this.conn = conn;
	recordExists = false;
	createDate = null;

	if (readAll)
	    readAll();
	else
	    checkExistence();
    }


    // read all columns of this record.
    void readAll() throws SQLException
    {
	PreparedStatement ps = conn.prepareStatement(readUser);
	ps.setLong(1, bbID);
	try {
	    ResultSet rs = ps.executeQuery();
	    if (rs.next()) {
		int i = 1;
		emailAddress = rs.getString(i++);
		name = rs.getString(i++);
		address = rs.getString(i++);
		birthdate = rs.getDate(i++);
		telephone = rs.getString(i++);
		gender = rs.getString(i++);
		storeID = rs.getInt(i++);
		createDate = new Date(rs.getLong(i++));
		misc = rs.getString(i);
		recordExists = true;
	    }
	    rs.close();
	} finally {
	    ps.close();
	}
    }


    // only check if an record exists for this BB ID
    void checkExistence() throws SQLException
    {
	PreparedStatement ps = conn.prepareStatement(oldUser);
	ps.setLong(1, bbID);
	try {
	    ResultSet rs = ps.executeQuery();
	    if (rs.next()) {
		if (! emailAddress.equals(rs.getString(2))) {
		    // email address cannot be changed.
		    emailAddress = rs.getString(2);
		}
		recordExists = true;
		createDate = new Date(rs.getLong(1));
	    }
	    rs.close();
	} finally {
	    ps.close();
	}
    }


    /**
     * Record the registration info to be written to the database.
     * @param name Name of customer.
     * @param address Contact address of customer.
     * @param birthdate Birthday of customer.
     * @param telephone Contact telephone number of customer.
     * @param gender Male or female.
     * @param storeID Store ID of the depot carrying out the registration.
     * @param misc Misc. info regarding this customer.
     */
    public void setValues(String name, String address, Date birthdate,
			  String telephone, String gender, int storeID,
			  String misc)
    {
	this.name = name;
	this.address = address;
	this.birthdate = birthdate;
	this.telephone = telephone;
	this.gender = gender;
	this.storeID = storeID;
	this.misc = misc;
    }


    // Find the next available unique email address.
    // form the email address by appending "-1", "-2", etc. to the end.
    String getEmailAddress(String name)
	throws SQLException
    {
	PreparedStatement ps = conn.prepareStatement(getMaxEmail);
	ps.setInt(1, name.length() + 1);
	ps.setString(2, name + "%");
	try {
	    ResultSet rs = ps.executeQuery();

	    int nextIdx;
	    if (rs.next()) {
		nextIdx = rs.getInt(1) + 1;
		if (rs.wasNull())
		    nextIdx = 1;
	    } else
		nextIdx = 1;
	    rs.close();
	    return name + nextIdx;
	} finally {
	    ps.close();
	}
    }


    /**
     * Write the customer information to the database.  If an record
     * already exists for this customer (BB ID), overwrites with the
     * new information.  Otherwise, insert a new record.
     * @exception SQLException Database access error.  If the error
     * code is 1, the requested email address might have already been
     * taken.
     * @exception DBException data consistency error.
     */
    public void update() throws SQLException, DBException
    {
	PreparedStatement ps = null;
	
	if (recordExists) {
	    ps = conn.prepareStatement(updateUser);
	    int i = 1;
	    ps.setString(i++, name);
	    ps.setString(i++, address);
	    ps.setDate(i++, birthdate);
	    ps.setString(i++, telephone);
	    ps.setString(i++, gender);
	    ps.setInt(i++, storeID);
	    ps.setString(i++, misc);
	    ps.setLong(i++, bbID);
	    try {
		if (ps.executeUpdate() != 1)
		    throw new DBException("Cannot update/insert customer record for " + bbID);
	    } finally {
		ps.close();
	    }
	} else {
	    if (emailAddress == null)
		throw new DBException("Cannot form valid email address from pinyin name");

	    long now = new java.util.Date().getTime();
	    createDate = new Date(now);

	    ps = conn.prepareStatement(insertUser);
	    int i = 1;
	    ps.setString(i++, emailAddress);
	    ps.setString(i++, name);
	    ps.setString(i++, address);
	    ps.setDate(i++, birthdate);
	    ps.setString(i++, telephone);
	    ps.setString(i++, gender);
	    ps.setLong(i++, bbID);
	    ps.setInt(i++, storeID);
	    ps.setLong(i++, now);
	    ps.setString(i++, misc);

	    try {
		ps.executeUpdate();
	    } catch (SQLException e) {
		if (e.getErrorCode() == 1 && ! recordExists) {
		    // unique constraint violation -- this means the email
		    // address has already been used.
		    emailAddress = getEmailAddress(emailAddress);
		    ps.setString(1, emailAddress);
		    ps.executeUpdate();
		} else
		    throw e;
	    } finally {
		ps.close();
	    }
	}
    }


    // Access methods

    public String getEmailAddress() {
	return emailAddress;
    }

    public String getEmailDomain() {
	return emailDomain;
    }

    public String getName() {
	return name;
    }

    public String getAddress() {
	return address;
    }

    public Date getBirthdate() {
	return birthdate;
    }

    public String getTelephone() {
	return telephone;
    }

    public String getGender() {
	return gender;
    }

    public int getStoreID() {
	return storeID;
    }

    public Date getCreateDate() {
	return createDate;
    }

    public String getMisc() {
	return misc;
    }

    public boolean recordExists() {
	return recordExists;
    }
}

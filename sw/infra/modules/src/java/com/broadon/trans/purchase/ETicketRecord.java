package com.broadon.trans.purchase;

import java.sql.*;
import com.broadon.db.DBException;

/**
 * Generate an eTicket record in the database.
 */
public class ETicketRecord
{
    static final String createTicket =
	"{ ? = call etops.etcreate(?, ?, ?, ?, ?, ?, ?) }";

    /**
     * Generate an eTicket record in the database.
     * @param conn Database connection to be used.
     * @param bbID Encoded ID of the BB player that owns this eTicket.
     * @param contentID Specify the content ID(s) of the ticket(s) to be created.
     * @param ticketID ID corresponding to each ticket.
     * @exception SQLException Database access error.
     * @exception DBException Data consistency error.
     */
    public static void generate(Connection conn, 
                                   long       bbID,
                                   long[]     contentID, 
                                   int[]      ticketID,
                                   String     lr_type,
                                   int        lr_limits,
                                   boolean    check_only)
	throws SQLException, DBException
    {
	CallableStatement cs = conn.prepareCall(createTicket);
	cs.registerOutParameter(1, Types.VARCHAR);

	try {
	    for (int i = 0; i < contentID.length; ++i) {
            cs.setLong(2, bbID);
            cs.setLong(3, contentID[i]);
            cs.setInt(4, ticketID[i]);
            cs.setString(5, "CTP");
            cs.setString(6, lr_type);
            cs.setInt(7, lr_limits);
            cs.setInt(8, (check_only? 1 : 0));
		
            cs.executeUpdate();
            String status = cs.getString(1);
            if (status == null || !status.startsWith("ETCR_"))
                throw new DBException("Cannot create eTicket record.");
	    }
	} finally {
	    cs.close();
	}
    }
}
    

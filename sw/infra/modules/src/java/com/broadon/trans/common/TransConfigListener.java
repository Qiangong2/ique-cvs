package com.broadon.trans.common;

import java.util.Properties;
import javax.servlet.*;

import com.broadon.servlet.ServletConstants;
import com.broadon.hsm.HSMClient;


public class TransConfigListener
    implements ServletConstants, ServletContextListener
{
    static final String HSM_SERVER_KEY = "HSM_SERVER";
    static final String HSM_PORT_KEY = "HSM_SERVICE_PORT";
    static final String REGIONAL_CTR_KEY = "REGIONAL_CENTER_ID";

    public void contextInitialized(ServletContextEvent sce) {
	ServletContext context = sce.getServletContext();
	Properties prop = (Properties) context.getAttribute(PROPERTY_KEY);
	    
	HSM_setup(context, prop);
	
	RegCtr_setup(context, prop);
    }

    public void contextDestroyed(ServletContextEvent sce) {
	// do nothing, the garbage collector needs to take care of it anyway.
    }

    void HSM_setup (ServletContext context, Properties prop) {
	String server = prop.getProperty(HSM_SERVER_KEY);
	String port = prop.getProperty(HSM_PORT_KEY);
	if (server != null && port != null) {
	    HSMClient hsm = new HSMClient(server, Integer.parseInt(port));
	    context.setAttribute(HSM_CONNECTOR_KEY, hsm);
	}
    }

    void RegCtr_setup (ServletContext context, Properties prop) {
	String center = prop.getProperty(REGIONAL_CTR_KEY);
	if (center != null) {
	    context.setAttribute(REGIONAL_CENTER_KEY, center);
	}
    }
}

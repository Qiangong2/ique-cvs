package com.broadon.trans.ops;

import java.io.*;
import java.sql.*;
import java.math.BigInteger;
import javax.servlet.http.*;
import java.security.KeyPair;
import java.security.GeneralSecurityException;
import java.security.cert.*;
import java.security.interfaces.RSAPublicKey;
import javax.crypto.interfaces.DHPrivateKey;
import javax.crypto.interfaces.DHPublicKey;

import com.broadon.filter.*;
import com.broadon.db.DBException;
import com.broadon.security.CertInfo;
import com.broadon.security.X509;
import com.broadon.security.SecureMessage;

import com.broadon.trans.common.*;


/**
 * Servlet implementation that handles a Smart Card certificate request.
 */
public final class GetCert extends TransactionServlet
{
    static final String REQUEST_RESULT_TAG = "smartcard_issue_result";
    static final String DEPOT_VERSION_KEY = "depot_version";
    static final String USER_ID_KEY = "user_id";
    static final String PASSWORD_KEY = "password";
    static final String OPERATOR_ID_KEY = "operator_id";
    static final String SMARTCARD_ID_KEY = "smartcard_id";
    static final String SMARTCARD_SN_KEY = "serial_no";
    static final String SMARTCARD_TRANSPORT_KEY = "transport_key";
    static final String SMARTCARD_PUK_KEY = "puk";
    static final String DH_PUBLIC_KEY = "server_dh_public_key";
    static final String CERT_KEY = "x509_cert";
    static final String CHAIN_ID_KEY = "ca_chain";
    static final String PRIVATE_KEY_KEY = "private_key";
    static final String KEY_PASSWD_KEY = "private_key_pw";

    protected TransactionRequest readInputs(XMLResponse res,
					    HttpServletRequest req)
    {
	String s;

	GetCertRequest certReq = new GetCertRequest();

	res.startDoc(REQUEST_RESULT_TAG);

	s = req.getParameter(DEPOT_VERSION_KEY);
	res.printElement(DEPOT_VERSION_KEY, s);

	res.printElement(HR_ID_KEY, req.getParameter(HR_ID_KEY));

	certReq.userID = req.getParameter(USER_ID_KEY);
	res.printElement(USER_ID_KEY, certReq.userID);

	certReq.password = req.getParameter(PASSWORD_KEY);
	res.printElement(PASSWORD_KEY, certReq.password);

	certReq.opID = req.getParameter(OPERATOR_ID_KEY);
	res.printElement(OPERATOR_ID_KEY, certReq.opID);

	certReq.serialNo = req.getParameter(SMARTCARD_SN_KEY);
	res.printElement(SMARTCARD_SN_KEY, certReq.serialNo);

	certReq.transportKey = req.getParameter(SMARTCARD_TRANSPORT_KEY);
	res.printElement(SMARTCARD_TRANSPORT_KEY, certReq.transportKey);

	// smart card ID is optional (i.e., could be null)
	certReq.smartcardID = req.getParameter(SMARTCARD_ID_KEY);

	return certReq;
    } // readInputs


    protected void process(Connection conn, authWrapper req,
			   TransactionRequest transReq, XMLResponse res)
	throws SQLException
    {
	GetCertRequest certReq = (GetCertRequest) transReq;

	try {
	    // validate password
	    try {
		ServiceTech.validate(conn, req.getBUID(),
				     certReq.userID, certReq.password,
				     req, ServiceTech.smartCardIssuer);
	    } catch (GeneralSecurityException e) {
		res.printExitStatus(StatusCode.XS_UNAUTH_TECH);
		return;
	    }
	    
	    SmartCard scard = new SmartCard(conn, certReq.opID, certReq.smartcardID);

	    CertInfo cert =
		hsm.getSmartCardCert(scard.getCN(), scard.getPIN());

	    extractCertInfo(cert.getCert());

	    String puk = getPUK();

	    KeyPair dhKeys = (new SecureMessage()).generateKeyPair();

	    // update the database record
	    conn.setAutoCommit(false);

	    scard.updateDB(certReq, puk, cert, publicKey, serialNo,
			   getPrivateKey(dhKeys));

	    String pin = new String(cert.getPassword());
	    if (! pin.equals(scard.getPIN())) {
		// update the operator password
		scard.updateOperatorPassword(pin);
	    }

	    res.printElement(SMARTCARD_ID_KEY,
			     String.valueOf(scard.getSmartCardID()));
	    res.printElement(SMARTCARD_PUK_KEY, puk);
	    res.printElement(DH_PUBLIC_KEY, getPublicKey(dhKeys));
 	    res.printElement(CERT_KEY, new String(cert.getCert())); 
	    res.printElement(PRIVATE_KEY_KEY, new String(cert.getPrivateKey()));
	    res.printElement(KEY_PASSWD_KEY, pin);
	
	    // get CA chain
	    String[] chain = getChain(cert.getChain());
	    res.printElement(CHAIN_ID_KEY, chain[0]);
	    res.printElement(CHAIN_ID_KEY, chain[1]);

	    // no error found, commit.
	    conn.commit();

	    res.printExitStatus(StatusCode.SC_OK);
	} catch (DBException e) {
	    res.printExitStatus(StatusCode.SC_DB_EXCEPTION, e);
	} catch (IOException e) {
	    res.printExitStatus(StatusCode.XS_CERT_GEN, e);
	} catch (CertificateException e) {
	    res.printExitStatus(StatusCode.XS_CERT_GEN, e);
	} catch (GeneralSecurityException e) {
	    res.printExitStatus(StatusCode.XS_CERT_GEN, e);
	} finally {
	    conn.rollback();
	}
    }


    String[] getChain(byte[] rawChain)
    {
	String chain = new String(rawChain);
	String[] results = chain.split(X509.X509_FOOTER);
	for (int i = 0; i < results.length; ++i)
	    results[i] += X509.X509_FOOTER;
	return results;
    }


    String publicKey;
    long serialNo;

    void extractCertInfo(byte[] rawCert) throws CertificateException
    {
	CertificateFactory cf = CertificateFactory.getInstance("X.509");
	X509Certificate x509Cert = (X509Certificate)
	    cf.generateCertificate(new ByteArrayInputStream(rawCert));
	RSAPublicKey key = (RSAPublicKey) x509Cert.getPublicKey();
	publicKey = key.getModulus().toString(32);
	serialNo = x509Cert.getSerialNumber().longValue();
    }

    // generate 8 random decimal digits (with leading zeros)
    String getPUK() throws IOException
    {
	long key = (new BigInteger(1, hsm.getRandomBytes(4))).longValue();
	String keyString = String.valueOf(key % 100000000);
	if (keyString.length() < 8) {
	    StringBuffer b = new StringBuffer("00000000");
            int len = keyString.length();
            b.replace(8 - len, 8, keyString);
            keyString = b.toString();
        }
	return keyString;
    }

    String getPrivateKey(KeyPair kp)
    {
	DHPrivateKey key = (DHPrivateKey) kp.getPrivate();
	return key.getX().toString(32);
    }

    String getPublicKey(KeyPair kp)
    {
	final int keyLength = 256;
	DHPublicKey key = (DHPublicKey) kp.getPublic();
	String s = key.getY().toString(16);

	if (s.length() < keyLength) {
	    // put in leading zeros;
	    StringBuffer buf = new StringBuffer();
	    int count = keyLength - s.length();
	    for (int i = 0; i < count; ++i)
		buf.append('0');
	    buf.append(s);
	    s = buf.toString();
	}
	return s;
    }
	    
}

package com.broadon.trans.xs;

import com.broadon.trans.common.TransactionRequest;

/**
 * Hold the data of a game upgrade request.
 */
public class UpgradeRequest extends TransactionRequest
{
    /** Encoded ID of the BB player making the purchase. */
    protected long bbID;

    /** ID of BB Depot submitting this request. */
    protected long depotID;

    /** old content ID upgraded from. */
    protected long oldContentID;

    /** new content ID upgraded to. */
    protected long newContentID;

    /** whether or not to send back the full eticket,
     *  or only the variant part
    */
    protected boolean getEticketInFull;
}

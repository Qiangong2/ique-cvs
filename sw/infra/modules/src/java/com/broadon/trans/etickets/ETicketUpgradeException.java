package com.broadon.trans.etickets;

import com.broadon.db.DBException;

/**
 * The <code>ETicketUpgradeException</code> class is thrown when
 * an eTicket cannot be upgraded due to data error.
 * 
 *
 * @version	$Revision: 1.2 $
 */
public class ETicketUpgradeException extends DBException
{
    /**
     * Constructs a ETicketUpgradeException instance.
     */
    public ETicketUpgradeException()
    {
	super();
    }

    /**
     * Constructs a ETicketUpgradeException instance.
     *
     * @param	message			the exception message
     */
    public ETicketUpgradeException(String message)
    {
	super(message);
    }

    /**
     * Constructs a ETicketUpgradeException instance.
     *
     * @param	message			the exception message
     * @param	throwable		the nested exception
     */
    public ETicketUpgradeException(String message, Throwable throwable)
    {
	super(message, throwable);
    }
}

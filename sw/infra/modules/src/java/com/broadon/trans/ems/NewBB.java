package com.broadon.trans.ems;

import java.sql.*;
import java.math.BigInteger;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.text.ParsePosition;

import com.broadon.db.DBException;
import com.broadon.security.BBcert;

import com.broadon.trans.common.InvalidBBIDException;

/**
 * Create or update a <code>BB_PLAYERS</code> record in the database.
 */
public class NewBB
{
    static final String TIMESTAMP = "BBXML.ConvertTimeStamp(?)";

    static final String readBB =
	"SELECT PUBLIC_KEY FROM BB_PLAYERS WHERE BB_ID = ?";

    static final String addBB =
	"INSERT INTO BB_PLAYERS (BB_ID, BB_HWREV, BB_MODEL, MANUFACTURE_DATE," +
	"    PUBLIC_KEY, BU_ID, BUNDLE_START_DATE, SN, EXT_SN) VALUES " +
	"    (?, ?, ?, " + TIMESTAMP + ", ?, ?, ?, ?, ?)";

    static final String updateBB =
	"UPDATE BB_PLAYERS SET BB_HWREV=?, BB_MODEL=?, " +
	"  MANUFACTURE_DATE=" + TIMESTAMP + ", BU_ID=?, " +
	"  BUNDLE_START_DATE=?, SN=?, EXT_SN=? WHERE BB_ID=?";

    static final String logSerialNo =
	"INSERT INTO BB_PLAYER_MFGS (BB_ID, MANUFACTURE_DATE, PCB_SN, EXT_SN) " +
	"  VALUES (?, " + TIMESTAMP + ", ?, ?)";

    protected long bbID;
    protected String model;
    protected Date bundleDate;
    protected int buID;
    protected BigInteger publicKey;
    protected long hw_rev;
    protected String serialNo;
    protected String extSerialNo;

    SimpleDateFormat fmt;

    /**
     * Create or update a <code>BB_PLAYERS</code> record in the database.
     * @param conn Database connection to be used.
     * @param req Request sent in by Depot.
     * @exception SQLException Database access error.
     * @exception InvalidBBIDException Invalid BB ID or missing records.
     * @exception DBException data inconsistency or corruption.
     */
    public NewBB(Connection conn, CreatePlayerRequest req)
	throws SQLException, InvalidBBIDException, DBException
    {
	fmt = new SimpleDateFormat("yyMMdd");

	this.bbID = req.bbID;
	this.model = req.model;
	this.bundleDate = format(req.bundledDate);
	this.buID = req.buID;
	this.hw_rev = req.hw_rev;
	this.serialNo = req.serialNo;
	this.extSerialNo = req.extSerialNo;
	create(conn);
    }
	
    void create(Connection conn)
	throws SQLException, InvalidBBIDException, DBException
    {
	long currentTime = new java.util.Date().getTime();

	PreparedStatement ps = conn.prepareStatement(readBB);
	ps.setLong(1, bbID);

	try {
	    ResultSet rs = ps.executeQuery();
	    if (rs.next()) {
		
		publicKey = new BigInteger(rs.getString(1), 32);

		// record alread exists
		ps.close();
		ps = conn.prepareStatement(updateBB);
		int i = 0;
		ps.setLong(++i, hw_rev);
		ps.setString(++i, model);
		ps.setLong(++i, currentTime);
		ps.setInt(++i, buID);
		ps.setDate(++i, bundleDate);
		ps.setString(++i, serialNo);
		ps.setString(++i, extSerialNo);
		ps.setLong(++i, bbID);
	    } else {
		// record not exist, insert new one

		try {
		    publicKey = getPublicKey(conn);
		} catch (IOException e) {
		    throw new DBException("Corrupted BB cert");
		}
	
		ps.close();
		ps = conn.prepareStatement(addBB);
		int i = 0;
		ps.setLong(++i, bbID);
		ps.setLong(++i, hw_rev);
		ps.setString(++i, model);
		ps.setLong(++i, currentTime);
		ps.setString(++i, publicKey.toString(32));
		ps.setInt(++i, buID);
		ps.setDate(++i, bundleDate);
		ps.setString(++i, serialNo);
		ps.setString(++i, extSerialNo);
	    }

	    ps.executeUpdate();

	    // update the serial number mapping
	    ps.close();
	    ps = conn.prepareStatement(logSerialNo);
	    int i = 0;
	    ps.setLong(++i, bbID);
	    ps.setLong(++i, currentTime);
	    ps.setString(++i, serialNo);
	    ps.setString(++i, extSerialNo);
	    ps.executeUpdate();
	} finally {
	    ps.close();
	}
    } // create

    

    // Read the BB cert from the LOT_CHIPS table and extract the public key.

    static final String readCert =
	"SELECT CERT FROM CSR.LOT_CHIPS WHERE BB_ID = ?";

    BigInteger getPublicKey(Connection conn)
	throws IOException, SQLException, InvalidBBIDException
    {
	PreparedStatement ps = conn.prepareStatement(readCert);
	ps.setLong(1, bbID);
	try {
	    ResultSet rs = ps.executeQuery();
	    if (!rs.next())
		throw new InvalidBBIDException("Cannot find BB " + bbID);
	    BBcert cert = new BBcert(rs.getString(1));
	    BigInteger k = cert.getPublicKey();
	    rs.close();
	    return cert.getPublicKey();
	} finally {
	    ps.close();
	}
    }

    // convert date string in form of "yyMMdd" to SQL Date.
    Date format(String date) {
	return ("000000".equals(date) ? null :
		new Date(fmt.parse(date, new ParsePosition(0)).getTime()));
    }

}

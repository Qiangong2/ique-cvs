package com.broadon.trans.purchase;

import java.util.*;
import java.sql.*;

import com.broadon.db.DBException;
import com.broadon.exception.*;
import com.broadon.trans.common.BBPlayer;
import com.broadon.trans.common.AuditLog;

/**
 * Database interface to handle purchasing of a game title.
 * The following methods can be refined in subclasses as appropriate
 * for the kindOfPurchase:
 *
 *     <code>validateContentInfo</code>
 *     <code>validateTitleInfo</code>
 *     <code>payForPurchase</code>
 *     <code>generateTicketInfo</code>
 *     <code>getPurchasePrice</code>
 *     <code>getPurchaseRtype</code>
 *     <code>getEticketRecord</code>
 *     <code>verifyOperator</code>
 *     <code>process</code>
 *     <code>getECardStatus</code>
 *     <code>purchaseWith</code>
 *
 * @version $Revision: 1.20 $
 * @author W. Wilson Ho
 */
public abstract class PurchaseTitle 
{
    // TicketID ranges.  Note that the range 0x7000 - 0x7fff is 
    // used for "pushed down content" (GLOBAL_ETICKETS).
    //
    public static final int MIN_PERMANENT_TICKET_ID = 0;
    public static final int MAX_PERMANENT_TICKET_ID = 0x6fff;
    public static final int MIN_TEMPORARY_TICKET_ID = 0x8000;
    public static final int MAX_TEMPORARY_TICKET_ID = 0xffff;

    // Purchase parameters
    //
    private final long   bbID;		 // encoded BB Player ID
    private final String model;		 // model of this BB Player
    private final long   hrID;       // ID of Depot that submit this request
    private final long   titleID;
    private final int    storeID;
    private final int    regionID;	 // logical region ID
    private final long   currentTime; // Time of purchase in UTC seconds
    private long[] contentIDs;	       // one or more contents that make up this
                                       // title (optional)

    private final String clientIP;     // Helps identify client doing purchase
    private final long   membershipID; // Helps identify client doing purchase
    private final long   memberID;     // Helps identify client doing purchase

    protected int[]      ticketIDs;	   // corresponding ticket IDs.
    protected int[]      ticketRTypes; // corresponding BBcrypto types.
    protected short[]    ticketLimits; // corresponding ticket limits.

    // ----------------------------------------------------------
    // What follows is only to be used or redefined by subclasses
    // ----------------------------------------------------------
    //
    protected final long   getBbID() {return bbID;}
    protected final String getModel() {return model;}
    protected final long   getHrID() {return hrID;}
    protected final long   getTitleID() {return titleID;}
    protected final int    getStoreID() {return storeID;}
    protected final int    getRegionID() {return regionID;}
    public          long[] getContentIDs() {return contentIDs;}
    protected final long   getCurrentTime() {return currentTime;}


    /**
     * Validates the ContentInfo, possibly differently for different types
     * of purchase, in which case this should be redefined in subclasses.
     * @param tcf The information about all content pertinent to a title
     * @param cID The the contentID for this content information
     * @param cinf This content information
     * @exception InvalidRequestException Parameters specified in the
     * purchase request do not match with the title information.
     * @exception TitleAlreadyPurchasedException BB player already
     * owns this title.
     * @exception TrialAfterUseException Attempt to enter a trial after
     * a limited or unlimited use of the title.
     */
    protected void validateContentInfo(TitleInfo        tinfo,
                                       TitleContentInfo tcf,
                                       long             cID, 
                                       Object           cinf) throws
        InvalidRequestException,
        TitleAlreadyPurchasedException,
        TrialAfterUseException
    {
        // We generally do not allow any kind of purchase of content 
        // for which the ticket has been revoked
        //
        if (tcf.etkRevoked(cinf)) {
            throw new InvalidRequestException("Attempt to purchase content with revoked eticket");
        }

        // We do not allow a first purchase outside (after) the
        // grace-period for a revoked content.  We do allow re-purchase of
        // content after the grace-period (e.g. rental of an old version).
        // 
        else if (!tcf.etkExists(cinf) && 
                 tcf.ctoRevokeDate(cinf) > 0 &&
                 tcf.ctoRevokeDate(cinf) < currentTime) {
            throw new InvalidRequestException("Attempt to purchase new content after revokation grace-period");
        }
    }


    // swap the content list subject if the requested content ID list does not
    // match the database content.
    private void swapContentList(long[] oldCID, TitleContentInfo cinfo)
	throws InvalidRequestException
    {
	try {
	    // normal case
	    if (oldCID.length == 1 && contentIDs.length == 1) {
		if (contentIDs[0] == oldCID[0])
		    return;		// don't need to swap
		// refuse to downgrade
		if (cinfo.coVersion(cinfo.get(contentIDs[0])) <
		    cinfo.coVersion(cinfo.get(oldCID[0])))
		    throw new InvalidRequestException("Attempt to purchase content that is an older version than the user already owns");
		// swap the IDs
		contentIDs = oldCID;
		return;
	    }

	    // more then one content in the title.
	    TreeMap map = new TreeMap();
	    for (int i = 0; i < oldCID.length; ++i) {
		Object c = cinfo.get(oldCID[i]);
		map.put(cinfo.coName(c), c);
	    }

	    boolean needToSwap = false;
	    for (int i = 0; i < contentIDs.length; ++i) {
		Object c = cinfo.get(contentIDs[i]);
		int requestedVersion = cinfo.coVersion(c);
		int recordedVersion = cinfo.coVersion(map.get(cinfo.coName(c)));
		if (requestedVersion < recordedVersion)
		    throw new InvalidRequestException("Attempt to purchase content that is an older version than the user already owns");
		else if (requestedVersion > recordedVersion)
		    needToSwap = true;
	    }

	    if (needToSwap)
		contentIDs = oldCID;
	    
	} catch (NullPointerException e) {
	    throw new InvalidRequestException("Invalid content ID");
	}
    }


    /**
     * Validates the TitleInfo, possibly differently for different types
     * of purchase, in which case this should be redefined in subclasses.
     * @param tinfo The title to be purchased
     * @exception InvalidRequestException Parameters specified in the
     * purchase request do not match with the title information.
     * @exception TitleAlreadyPurchasedException BB player already
     * owns this title.
     * @exception TrialAfterUseException Attempt to enter a trial after
     * a limited or unlimited use of the title.
     */
    protected void validateTitleInfo(TitleInfo tinfo) throws
        InvalidRequestException,
        TitleAlreadyPurchasedException,
        TrialAfterUseException
    {
        // Generally we need to ensure that the given contentIDs are a 
        // subset of all the valid content IDs for the title.
        //
        final TitleContentInfo cinfo = tinfo.getContentIDs();

	// If this player already owns valid eTickets for this title,
	// replace the content ID list by those in the database.  This
	// is needed in the case of iqahc which does not guarantee any
	// eTicket upgrade before purchasing, and as a result the
	// content ID's submitted might be different from that in the
	// BB's memory card, and we might end up have 2 different
	// eTickets for the same title.
	long[] cids = cinfo.getExistingContentIDs();
	if (cids != null) {
	    swapContentList(cids, cinfo);
	}
	
        boolean                ok = contentIDs != null;
        for (int i = 0; ok && (i < contentIDs.length); ++i) {
            final Object c = cinfo.get(contentIDs[i]);
            if (c == null)
                ok = false;
            else {
                validateContentInfo(tinfo, cinfo, contentIDs[i], c);
            }
        }
        if (!ok) {
            throw new InvalidRequestException("Invalid content ID list");
        }
    } // validateTitleInfo


    /**
     * Update DB to reflect payment for the content.
     * @param conn Database connection to be used.
     * @param tinfo The title to be purchased
     * @param auditLog For updating logs other than the ETicket and
     * Purchase logs.
     * @exception SQLException Database access error.
     * @exception DBException Database record inconsistent.
     * @exception ECardRedemptionException  One or more of the eCards
     * is invalid, or their total value is less than the sale price of
     * the title.
     */
    protected abstract void payForPurchase(Connection conn, 
                                           TitleInfo  tinfo,
                                           AuditLog   auditLog) throws
        SQLException, 
        DBException, 
        ECardRedemptionException;


    /**
     * Generates an appropriate ticket_id, rtype, and limit for each 
     * contentID, based on the type of purchase.  Never call this 
     * function directly; use getTicketIDs, getTicketRTypes, and 
     * getTicketLimits instead (after this has been done).
     * @param bb BB Player record
     * @param tinfo Title information
    */
    protected abstract void generateTicketProperties(BBPlayer  bb, 
                                                     TitleInfo tinfo);

    /** Returns the price of a purchase based on the title/region information
     *  and the type of purchase.
     * @param tinfo The information about the given title/region.
     */
    protected abstract int getPurchasePrice(TitleInfo tinfo);

    /** Returns the rtype for a purchase based both on the ContentTitleRegions
     *  information and the type of purchase.
     * @param tinfo The information about the given title/region.
     */
    protected abstract String getPurchaseRtype(TitleInfo tinfo);

    /**
     * Generates the ETicket record or updates the existing one to
     * account for the current purchase.  Must be called AFTER
     * <code>validateTitleInfo</code>, since some information relevant
     * to this value may be extracted in that step.
     * @param conn The DB connection.
     * @param tinfo The information about the given title/region.
     * @param conn The generated ticket IDs for the contents being purchased.
     */
    protected abstract void generateEticketRecord(Connection conn,
                                                  TitleInfo  tinfo,
                                                  int []     ticketIDs)
        throws SQLException, DBException;


    // ----------------------------------------------------------
    // Public methods
    // ----------------------------------------------------------

    /**
     * Construct a <code>PurchaseTitle</code> object.  Must be called AFTER
     * <code>validateTitleInfo</code>, since some information relevant
     * to this value may be extracted in that step.
     * @param bb BB player record.
     * @param hrID Depot that submits this purchase request.
     * @param storeID ID of the store to which the Depot belongs.
     * @param regionID Logical region where the BB Depot belongs.
     * @param titleID Title to be purchased.
     * @param contendIDs Zero or more content ID that compose this
     * @param clientIP     Helps dentify the client initiating the purchase
     * @param membershipID Helps dentify the client initiating the purchase
     * @param memberID     Helps dentify the client initiating the purchase
     * title. 
     */
    public PurchaseTitle(BBPlayer bb,
                         long     hrID, // BB id
                         int      storeID,
                         int      regionID,
                         long     titleID, 
                         long[]   contentIDs,
                         String   clientIP,     // Use null when undefined
                         long     membershipID, // Use -1 when undefined
                         long     memberID)     // Use -1 when undefined
    {
        this.bbID = bb.getBBID();
        this.model = bb.getBBModel();
        this.titleID = titleID;
        this.hrID = hrID;
        this.storeID = storeID;
        this.regionID = regionID;
        this.contentIDs = contentIDs;
        this.ticketIDs = null;
        this.ticketRTypes = null;
        this.ticketLimits = null;
        this.currentTime = bb.getCurrentTime();
        this.clientIP = clientIP;
        this.membershipID = membershipID;
        this.memberID = memberID;
    } // constructor


    /**
     * Process and record this purchase request into the database.
     * Normally the logic defined here is common to all subclasses,
     * altough the door has been left open to redefine it if
     * necessary.
     * @param conn Database connection to be used.
     * @param bb BB Player record
     * @param auditLog For updating logs other than the ETicket and
     * Purchase logs.
     * @param operatorID For smart-card purchases (-1 for other purchases)
     * @exception SQLException Database access error.
     * @exception DBException Database record inconsistent.
     * @exception InvalidRequestException Parameters specified in the
     * purchase request does not match with the database information.
     * @exception TitleAlreadyPurchasedException BB player already
     * owns this title.
     * @exception TrialAfterUseException Attempt to enter a trial after
     * a limited or unlimited use of the title.
     * @exception ECardRedemptionException One or more of the eCards
     * is invalid, or their total value is less than the sale price of
     * the title.
     */
    public void process(Connection conn, 
                        BBPlayer   bb, 
                        AuditLog   auditLog, 
                        long       operatorID,
                        long       buID) throws
        SQLException, 
        DBException, 
        InvalidRequestException,
        TitleAlreadyPurchasedException, 
        TrialAfterUseException,
        ECardRedemptionException
    {
        // Read title information from the DB, then verify that the intended
        // purchase is valid.
        //
        TitleInfo tinfo = TitleInfo.read(conn, bbID, titleID, regionID, model);
        validateTitleInfo(tinfo);

        // Generate a CONTENT_TITLE_PURCHASES entry for this purchase and
        // charge the client as appropriate.
        //
        TitlePurchase.generate(conn, bbID, hrID, titleID, regionID, storeID,
                               getPurchaseRtype(tinfo),
                               getPurchasePrice(tinfo),
                               operatorID,
                               purchaseWith(),
                               clientIP,
                               membershipID,
                               memberID);
        payForPurchase(conn, tinfo, auditLog);

        generateTicketProperties(bb, tinfo);
        generateEticketRecord(conn, tinfo, getTicketIDs());
        bb.setSyncTime();
        bb.updateRecord(conn);
    } // process


    /** 
     * Return the eCard redemption status.
     * @return Array of eCard redemption status, corresponding to the
     * list of ecard passed in.  Never returns <code>null</code>, but
     * may return a zero sized array where no ecard is relevant to the
     * kind of purchase.
     */
    public abstract int[] getECardStatus();


    /**
     * Returns ticket IDs corresponding to the contentIDs used in
     * construction
     * @return Array of ticketIDs
     */
    public final int[] getTicketIDs()
    {
        return ticketIDs;
    }

    /**
     * Returns ticket BBcrypto right types corresponding to the contentIDs
     * used in construction
     * @return Array of right types, one per content object
     */
    public final int[] getTicketRTypes()
    {
        return ticketRTypes;
    }

    /**
     * Returns ticket limits (minutes for BB) corresponding to the contentIDs
     * used in construction
     * @return Array of limit values, one per content object
     */
    public final short[] getTicketLimits()
    {
        return ticketLimits;
    }

    /**
     * Generates the purchaseWith string expected by the
     * xsopts.title_purchase SQL callable statement.  Must be called
     * AFTER <code>validateTitleInfo</code> in <code>process()</code>,
     * since some information relevant to this value may be extracted 
     * in that step.
     * @return ECARD, EC_RINIT, EC_RNEXT, SC_BONUS, IC_TRIAL, etc.
     */
    public abstract String purchaseWith();

}

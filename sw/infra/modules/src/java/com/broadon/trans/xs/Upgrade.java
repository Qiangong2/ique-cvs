package com.broadon.trans.xs;

import java.sql.*;
import javax.servlet.http.*;

import com.broadon.filter.*;
import com.broadon.db.DBException;

import com.broadon.trans.common.*;
import com.broadon.trans.etickets.*;
import com.broadon.trans.purchase.ETicketSyncLog;


/**
 * Servlet implementation that handles a game upgrade request.
 */
public final class Upgrade extends TransactionServlet
{
    static final String REQUEST_RESULT_TAG = "upgrade_result";
    static final String DEPOT_VERSION_KEY = "depot_version";
    static final String BB_ID_KEY = "bb_id";
    static final String OLD_CONTENT_ID_KEY = "old_content_id";
    static final String NEW_CONTENT_ID_KEY = "new_content_id";
    static final String TIMESTAMP_KEY = "sync_timestamp";
    static final String ETICKET_IN_FULL_KEY = "eticket_in_full";
    

    protected void process(Connection conn, authWrapper req,
			   TransactionRequest transReq, XMLResponse res)
	throws SQLException
    {
	BBPlayer bb = null;
	UpgradeRequest upgradeReq = (UpgradeRequest) transReq;

	ETicketSyncLog log = new ETicketSyncLog(upgradeReq.bbID,
						upgradeReq.depotID);
	auditLog.add(log);

	try {
	    bb = new BBPlayer(upgradeReq.bbID, req.getBUID(), conn);
	    UpgradeTicket upgrade = new UpgradeTicket(bb, conn);

	    conn.setAutoCommit(false);

	    upgrade.process(upgradeReq.oldContentID, upgradeReq.newContentID);
	    
	    res.printElement(TIMESTAMP_KEY,
			     Long.toString(bb.getCurrentTime()));

	    ETickets tickets = new ETickets(conn, hsm, bb.getBBID(),
					    bb.getBBKey(), regionalCenterID,
					    ETickets.MIN_TID, req.getBUID());
	    tickets.generateAll(upgradeReq.getEticketInFull);
	    ETicketsFormatter out = new ETicketsFormatter(tickets, res);
	    out.dumpTickets();
	    out.dumpCerts();

	    conn.commit();
	    
	    res.printExitStatus(StatusCode.SC_OK);

	} catch (InvalidBBIDException badBB) {
	    res.printExitStatus(StatusCode.XS_BAD_BB_ID, badBB);

	} catch (ETicketCreationException ece) {
	    res.printExitStatus(StatusCode.XS_ETICKET_SYNC, ece);
	    
	} catch (ETicketUpgradeException eue) {
	    res.printExitStatus(StatusCode.XS_UPGRADE_TICKET, eue);
	    
	} catch (DBException dbe) {
	    dbe.printStackTrace(System.out);
	    res.printExitStatus(StatusCode.SC_DB_EXCEPTION, dbe);
	} finally {
	    conn.rollback();
	    
	    log.setStatus(res.getExitStatusCode(), res.getExitStatusMsg());
	    auditLog.add(log);
	}

    } // process


    protected TransactionRequest readInputs(XMLResponse res,
					    HttpServletRequest req)
    {
	String s;
	UpgradeRequest upgradeReq = new UpgradeRequest();

	res.startDoc(REQUEST_RESULT_TAG);

	s = req.getParameter(DEPOT_VERSION_KEY);
	res.printElement(DEPOT_VERSION_KEY, s);
	
	s = req.getParameter(BB_ID_KEY);
	upgradeReq.bbID = Long.parseLong(s);
	res.printElement(BB_ID_KEY, s);

	s = req.getParameter(HR_ID_KEY);
	// Verify the format of the Depot ID
    if (s.length() != 14 || ! s.startsWith("HR")) {
        throw new NumberFormatException("Invalid Depot ID");
    }
    upgradeReq.depotID = Long.parseLong(s.substring(2), 16);
	res.printElement(HR_ID_KEY, s);
	
	s = req.getParameter(OLD_CONTENT_ID_KEY);
	upgradeReq.oldContentID = Long.parseLong(s);
	res.printElement(OLD_CONTENT_ID_KEY, s);

	s = req.getParameter(NEW_CONTENT_ID_KEY);
	upgradeReq.newContentID = Long.parseLong(s);
	res.printElement(NEW_CONTENT_ID_KEY, s);

    s = req.getParameter(ETICKET_IN_FULL_KEY);
    upgradeReq.getEticketInFull = (s != null && s.equals("1"));

	return upgradeReq;
    } // readInputs
}

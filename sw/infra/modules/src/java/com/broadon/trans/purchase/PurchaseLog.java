package com.broadon.trans.purchase;

import java.sql.*;

import com.broadon.trans.common.Logger;

/**
 * Logger for eTicket purchases.
 */
public class PurchaseLog extends Logger
{
    static final String addLogForEcard =
	"INSERT INTO CONTENT_TITLE_PURCHASE_LOGS " +
	"  (REGION_ID, HR_ID, STORE_ID, " +
    "   EUNITS, BB_ID, " +
	"   TITLE_ID, REQUEST_DATE) VALUES " +
	"  (?, ?, ?, ?, ?, ?, " + TIMESTAMP + ")";

    static final String addLogForScard =
	"INSERT INTO CONTENT_TITLE_PURCHASE_LOGS " +
	"  (REGION_ID, HR_ID, STORE_ID, " +
    "   OPERATOR_ID, BB_ID, " +
	"   TITLE_ID, REQUEST_DATE) VALUES " +
	"  (?, ?, ?, ?, ?, ?, " + TIMESTAMP + ")";

    static final String updateLog =
	"UPDATE CONTENT_TITLE_PURCHASE_LOGS SET PURCHASE_WITH=?, " +
    " REQUEST_STATUS=?, REQUEST_LOG=? " +
	" WHERE BB_ID=? AND TITLE_ID=? AND REQUEST_DATE=" +
	TIMESTAMP;

    int regionID;		
    long hrID;			
    int storeID;		
    int eUnits;
    long bbID;			
    long titleID;		
    long operatorID;			
    String kindOfPurchase;			
    String purchaseWith;
    String status;
    String message;

    boolean flushed;

    /**
     * Create an <code>PurchaseLog</code> object holding the
     * values to be inserted into the audit log.
     */
    public PurchaseLog(int regionID, 
                       long hrID, 
                       int storeID, 
                       String kindOfPurchase, 
                       int eUnits,      // Use 0 when operator assisted
                       long operatorID, // Use -1 when not operator assisted
                       long bbID, 
                       long titleID)
    {
	super();

	this.regionID = regionID;
	this.hrID = hrID;
	this.storeID = storeID;
	this.kindOfPurchase = kindOfPurchase;
    this.purchaseWith = null; // set later
	this.eUnits = eUnits;
	this.bbID = bbID;
	this.titleID = titleID;
	this.operatorID = operatorID;
	
	flushed = false;
	
	status = message = null;
    }


    synchronized public void flush(Connection conn) throws SQLException
    {
	PreparedStatement ps;
	int i = 0;

	if (flushed) {
	    // already written once, need to update the existing record
	    ps = conn.prepareStatement(updateLog);
        if (purchaseWith == null)
            ps.setNull(++i, Types.VARCHAR);
        else
            ps.setString(++i, purchaseWith);
	    ps.setString(++i, status);
	    ps.setString(++i, message);
	} else if (operatorID == -1) {
        // Ecard purchase
        //
	    ps = conn.prepareStatement(addLogForEcard);
	    ps.setInt(++i, regionID);
	    ps.setLong(++i, hrID);
	    ps.setInt(++i, storeID);
	    ps.setInt(++i, eUnits);
    } else {
        // Operator assisted purchase (no ecard)
        //
	    ps = conn.prepareStatement(addLogForScard);
	    ps.setInt(++i, regionID);
	    ps.setLong(++i, hrID);
	    ps.setInt(++i, storeID);
	    ps.setLong(++i, operatorID);
	}

	ps.setLong(++i, bbID);
	ps.setLong(++i, titleID);
	ps.setLong(++i, timestamp);

	try {
	    ps.executeUpdate();
	} finally {
	    ps.close();
	}

	flushed = true;
    }


    /**
     * Set the "purchase_with" value for the log in a subsequent update.
     */
    synchronized public void setPurchaseWith(String pw)
    {
        purchaseWith = pw;
    }

    /**
     * Set the status of eTicket sync and possibly an error
     * message. The error message will be truncated to 240 characters,
     * and then prefixed with the purchase-type.
     */
    synchronized public void setStatus(String status, 
                                       String message)
    {
        this.status = status;
        if (kindOfPurchase == null)
            this.message = "";
        else
            this.message = "[" + kindOfPurchase + "]";

        if (message != null)
            this.message += 
                message.substring(0,Math.min(message.length(),240));
    }
}

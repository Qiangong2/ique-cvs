package com.broadon.trans.rma;

import com.broadon.exception.BroadOnException;

/**
 * The <code>BadUserBBException</code> class is thrown when the
 * body of a request contains invalid or inconsistent information.
 *
 * @version	$Revision: 1.2 $
 */
public class BadUserBBException
    extends BroadOnException
{
    /**
     * Constructs a BadUserBBException instance.
     */
    public BadUserBBException()
    {
	super();
    }

    /**
     * Constructs a BadUserBBException instance.
     *
     * @param	message			the exception message
     */
    public BadUserBBException(String message)
    {
	super(message);
    }

    /**
     * Constructs a BadUserBBException instance.
     *
     * @param	message			the exception message
     * @param	throwable		the nested exception
     */
    public BadUserBBException(String message, Throwable throwable)
    {
	super(message, throwable);
    }
}

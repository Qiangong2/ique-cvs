package com.broadon.trans.ems;

import javax.servlet.http.*;
import java.sql.*;
import java.io.IOException;

import com.broadon.filter.authWrapper;
import com.broadon.servlet.ServletConstants;
import com.broadon.servlet.ContextStatus;

import com.broadon.trans.common.*;



public class Monitor extends TransactionServlet implements ServletConstants
{
    static final String DB_TEST = "SELECT SYSDATE FROM DUAL";

    protected void process(Connection conn, authWrapper req,
                           TransactionRequest transReq, XMLResponse res)
    {
	ContextStatus status = (ContextStatus) ctx.getAttribute(MONITOR_STATUS_KEY);
	
	try {
	    if (! auditLog.isRunning() ||
		status.getOperationStatus() == status.OS_LB_NOTIFIED) {
		res.error(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		status.setHealth(false);
		return;
	    }

	    // test the HSM connection by requesting some random bytes
	    byte[] dummy = hsm.getRandomBytesNoCache(4);

	    PreparedStatement ps = conn.prepareStatement(DB_TEST);
	    try {
		ResultSet rs = ps.executeQuery();
		if (!rs.next()) {
		    res.error(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		    status.setHealth(false);
		}
	    } finally {
		ps.close();
	    }
	    status.setHealth(true);
	} catch (Exception e) {
	    status.setHealth(false);
	    e.printStackTrace(System.out);
	    try {
		res.error(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
	    } catch (IOException ioe) {
		throw new IllegalStateException(ioe.getMessage());
	    }
	}
    }

    protected TransactionRequest readInputs(XMLResponse res,
                                            HttpServletRequest req)
    {
	return null;
    }

}

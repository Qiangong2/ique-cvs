package com.broadon.trans.ems;

import java.sql.*;
import javax.servlet.http.HttpServletRequest;

import com.broadon.filter.authWrapper;

import com.broadon.trans.common.*;

/**
 * Verify if the given BU ID, bundle start date and BB model constitude a
 * valid bundle.
 */
public final class CheckModel extends TransactionServlet
{
    static final String REQUEST_RESULT_TAG = "verify_bundle_result";
    static final String DEPOT_VERSION_KEY = "depot_version";
    static final String BB_MODEL_KEY = "bb_model";
    static final String BU_ID_KEY = "bu_id";
    static final String BUNDLED_DATE_KEY = "bundled_date";
    static final String CONTENT_ID_KEY = "content_id";

    
    protected TransactionRequest readInputs(XMLResponse res,
                                            HttpServletRequest req)
    {
	CheckModelRequest modelReq = new CheckModelRequest();

	res.startDoc(REQUEST_RESULT_TAG);

        res.printElement(DEPOT_VERSION_KEY,
			 req.getParameter(DEPOT_VERSION_KEY));

	modelReq.model = req.getParameter(BB_MODEL_KEY);
	res.printElement(BB_MODEL_KEY, modelReq.model);

	modelReq.buID = Integer.parseInt(req.getParameter(BU_ID_KEY));
	res.printElement(BU_ID_KEY, String.valueOf(modelReq.buID));

	modelReq.setBundledDate(req.getParameter(BUNDLED_DATE_KEY));
	res.printElement(BUNDLED_DATE_KEY, modelReq.formatedBundledDate);

	return modelReq;
    }

    protected void process(Connection conn, authWrapper req,
                           TransactionRequest transReq, XMLResponse res)
        throws SQLException
    {
        CheckModelRequest modelReq = (CheckModelRequest) transReq;
	if (isValidModel(conn, modelReq, res))
	    res.printExitStatus(StatusCode.SC_OK);
	else
	    res.printExitStatus(StatusCode.EMS_BAD_BUNDLE);
    }

    
    static final String getBundle =
	"SELECT CONTENT_ID FROM BB_BUNDLE_DETAILS_V WHERE BB_MODEL = ? AND " +
	"    BU_ID = ? AND START_DATE = ? AND (END_DATE IS NULL OR " +
	"                                      END_DATE >= SYSDATE)";

    static final String getModel =
	"SELECT 1 FROM BUSINESS_UNITS, BB_MODELS WHERE BB_MODEL=? AND " +
	"    BU_ID=?";

    boolean isValidModel(Connection conn, CheckModelRequest req,
			 XMLResponse res)
	throws SQLException
    {
	if (! "000000".equals(req.formatedBundledDate)) {
	    PreparedStatement ps = conn.prepareStatement(getBundle);
	    ps.setString(1, req.model);
	    ps.setInt(2, req.buID);
	    ps.setDate(3, req.bundledDate);
	    try {
		ResultSet rs = ps.executeQuery();
		int count = 0;
		while (rs.next()) {
		    long id = rs.getLong(1);
		    res.printElement(CONTENT_ID_KEY, String.valueOf(id));
		    ++count;
		}
		return (count > 0);
	    } finally {
		ps.close();
	    }
	} else {
	    // no bundled tickets, just check the model and bu ID
	    PreparedStatement ps = conn.prepareStatement(getModel);
	    ps.setString(1, req.model);
	    ps.setInt(2, req.buID);
	    try {
		ResultSet rs = ps.executeQuery();
		return rs.next();
	    } finally {
		ps.close();
	    }
	}
    }
	
}

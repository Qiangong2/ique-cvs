package com.broadon.trans.installer;

import java.sql.*;

import com.broadon.trans.common.Logger;

import com.broadon.util.Database;

/**
 * Logger for BB Depot configuration.  Typically, an instance of this class
 * is created upon receiving the config request and the info is
 * flushed to the audit log.  After the sync process, this
 * object is put into the audit log queue again with the config
 * status updated.
 */
public class DepotConfigLog extends Logger
{
    static final String addLog =
	"INSERT INTO DEPOT_CONFIG_LOGS (HR_ID, REQUEST_DATE, STORE_ID) " +
	"    VALUES (?, " + TIMESTAMP + ", ?)";

    static final String updateLog =
	"UPDATE DEPOT_CONFIG_LOGS SET REQUEST_STATUS=?, REQUEST_LOG=? WHERE " +
	"    HR_ID = ? AND REQUEST_DATE = " + TIMESTAMP + " AND STORE_ID = ?";

    long hrId;

    int storeId;
    String status;
    String message;

    boolean flushed;

    /**
     * Create an <code>DeoitConfigLog</code> object holding the
     * values to be inserted into the audit log.
     */
    public DepotConfigLog(long depotId, int storeId)
    {
	super();
	this.hrId = depotId;
	this.storeId = storeId;

	flushed = false;
	
	status = message = null;
    }


    synchronized public void flush(Connection conn) throws SQLException
    {
	PreparedStatement ps;
	int i = 1;

	if (flushed) {
	    // already written once, need to update the existing record
	    ps = conn.prepareStatement(updateLog);
	    ps.setString(i++, status);
	    ps.setString(i++, message);
	} else {
	    ps = conn.prepareStatement(addLog);
	}

	ps.setLong(i++, hrId);
	ps.setLong(i++, timestamp);
	ps.setInt(i++, storeId);

	try {
	    ps.executeUpdate();
	} finally {
	    ps.close();
	}

	flushed = true;
    }

    /**
     * Set the status of the Depot configuration and possibly an error
     * message. 
     */
    synchronized public void setStatus(String status, String message)
    {
	this.status = status;
	this.message = message;
    }

    public static void main(String[] args) {
	try {
	    Database db = new Database();
	    Connection conn = db.getConnection();
	    DepotConfigLog log = new DepotConfigLog(2L, 3);
	    log.flush(conn);
	    log.setStatus("ok", null);
	    log.flush(conn);
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }
}

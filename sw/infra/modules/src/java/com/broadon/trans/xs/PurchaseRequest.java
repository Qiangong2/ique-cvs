package com.broadon.trans.xs;

import com.broadon.trans.common.TransactionRequest;

/**
 * Hold the data of a purchase request.
 */
public class PurchaseRequest extends TransactionRequest
{
    /** Encoded ID of the BB player making the purchase. */
    protected long bbID;

    /** ID of BB Depot submitting this request. */
    protected long depotID;

    /** time stamp for last eTicket synchronization. */
    protected long lastSync;

    /** minimum ticket ID accepted by the player. */
    protected int minTID;

    /** ID of the title to be purchased. */
    protected long titleID;

    /** List of contents that composes this title. */
    protected long[] contentIDs;

    /** Price of this title. */
    protected int eUnits;

    /** Kind of purchase transaction. */
    protected String kindOfPurchase;
    
    /** List of eCards used for purchasing. */
    protected String[] ecards;

    /** User identity for operator initiated purchasing (null if none). */
    protected String userID;

    /** Password for for operator initiated purchasing (null if none). */
    protected String password;

    /** OperatorID for operator initiated purchasing (-1 if none). */
    protected long operatorID;

    /** whether or not to send back the full eticket,
     *  or only the variant part
    */
    protected boolean getEticketInFull;

    /** Identification of the client making the purchase request
     *
     */
    protected String clientIP;
    protected long   membershipID;
    protected long   memberID;
}

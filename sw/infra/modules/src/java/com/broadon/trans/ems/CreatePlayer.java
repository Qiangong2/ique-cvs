package com.broadon.trans.ems;

import java.sql.*;
import javax.servlet.http.*;

import com.broadon.filter.*;
import com.broadon.db.DBException;

import com.broadon.trans.common.*;
import com.broadon.trans.etickets.*;
import com.broadon.trans.purchase.ETicketSyncLog;


/**
 * Servlet implementation that generates the bundled game tickets at the EMS.
 */
public final class CreatePlayer extends TransactionServlet
{
    static final String REQUEST_RESULT_TAG = "new_player_result";
    static final String DEPOT_VERSION_KEY = "depot_version";
    static final String BB_ID_KEY = "bb_id";
    static final String BB_MODEL_KEY = "bb_model";
    static final String BB_HWREV_KEY = "bb_hwrev";
    static final String BB_SERIAL_NO_KEY = "bb_serial_no";
    static final String PCB_SERIAL_NO_KEY = "pcb_serial_no";
    static final String PRODUCT_SERIAL_NO_KEY = "product_serial_no";
    static final String BU_ID_KEY = "bu_id";
    static final String BUNDLED_DATE_KEY = "bundled_date";
    static final String TID_KEY = "tid";
    

    protected void process(Connection conn, authWrapper req,
			   TransactionRequest transReq, XMLResponse res)
	throws SQLException
    {
	CreatePlayerRequest playerReq = (CreatePlayerRequest) transReq;

	ETicketSyncLog log = new ETicketSyncLog(playerReq.bbID,
                                                playerReq.depotID);
        auditLog.add(log);

	if (playerReq.serialNo == null || playerReq.extSerialNo == null) {
	    res.printExitStatus(StatusCode.XS_MALFORMED_INPUT);
	    log.setStatus(res.getExitStatusCode(), "Missing serial number(s)");
	    auditLog.add(log);
	    return;
	}
		

	try {
	    conn.setAutoCommit(false);

	    NewBB bb = new NewBB(conn, playerReq);

	    ETickets tickets = new ETickets(conn, hsm, bb.bbID, bb.publicKey,
					    regionalCenterID, ETickets.MIN_TID,
					    bb.buID);

	    if (bb.bundleDate != null) {
		new GenTickets(conn, bb, playerReq.tid);
		tickets.generateAll();
	    }
	    conn.commit();
	    
	    ETicketsFormatter out = new ETicketsFormatter(tickets, res);
            out.dumpTickets();
            out.dumpCerts();
	    res.printExitStatus(StatusCode.SC_OK);
	    
	} catch (InvalidBBIDException badBB) {
	    res.printExitStatus(StatusCode.XS_BAD_BB_ID, badBB);
	    
	} catch (ETicketCreationException ece) {
	    res.printExitStatus(StatusCode.XS_ETICKET_SYNC, ece);
	    
	} catch (DBException dbe) {
	    res.printExitStatus(StatusCode.SC_DB_EXCEPTION, dbe);

	} finally {
	    conn.rollback();
	    log.setStatus(res.getExitStatusCode(), res.getExitStatusMsg());
            auditLog.add(log);
	}

    } // process


    protected TransactionRequest readInputs(XMLResponse res,
					    HttpServletRequest req)
    {
	CreatePlayerRequest playerReq = new CreatePlayerRequest();

	res.startDoc(REQUEST_RESULT_TAG);

	res.printElement(DEPOT_VERSION_KEY,
			 req.getParameter(DEPOT_VERSION_KEY));

	String s = req.getParameter(HR_ID_KEY);
	res.printElement(HR_ID_KEY, s);
	playerReq.depotID = Long.parseLong(s.substring(2), 16);

	playerReq.bbID = Long.parseLong(req.getParameter(BB_ID_KEY));
	res.printElement(BB_ID_KEY, String.valueOf(playerReq.bbID));

	playerReq.hw_rev = Long.parseLong(req.getParameter(BB_HWREV_KEY));
	res.printElement(BB_HWREV_KEY, String.valueOf(playerReq.hw_rev));

	// new format: depot passed up 2 separate serial numbers
	playerReq.serialNo = req.getParameter(PCB_SERIAL_NO_KEY);
	res.printElement(PCB_SERIAL_NO_KEY, playerReq.serialNo);
	playerReq.extSerialNo = req.getParameter(PRODUCT_SERIAL_NO_KEY);
	res.printElement(PRODUCT_SERIAL_NO_KEY, playerReq.extSerialNo);

	playerReq.buID = Integer.parseInt(req.getParameter(BU_ID_KEY));
	res.printElement(BU_ID_KEY, String.valueOf(playerReq.buID));
	
	playerReq.model = req.getParameter(BB_MODEL_KEY);
	res.printElement(BB_MODEL_KEY, playerReq.model);

	playerReq.bundledDate = req.getParameter(BUNDLED_DATE_KEY);
	res.printElement(BUNDLED_DATE_KEY, playerReq.bundledDate);

	if (req.getParameter(TID_KEY) != null) {
	    playerReq.tid = Integer.parseInt(req.getParameter(TID_KEY));
	} else
	    playerReq.tid = ETickets.MIN_TID;
	res.printElement(TID_KEY, String.valueOf(playerReq.tid));
	    
	return playerReq;
    } // readInputs
}

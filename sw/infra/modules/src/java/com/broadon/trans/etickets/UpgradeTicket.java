package com.broadon.trans.etickets;

import java.sql.*;

import com.broadon.db.DBException;
import com.broadon.trans.common.BBPlayer;


/**
 * Upgrade any given tickets from an old content version to a new one.
 */
public class UpgradeTicket
{
    BBPlayer bb;
    long bbID;
    Connection conn;
    

    /**
     * Create an <code>UpgradeTicket</code> instance.
     * @param bb Describes the specifics of a BB player.
     * @param conn Database connection to be used.
     */
    public UpgradeTicket(BBPlayer bb, Connection conn)
    {
	this.bb = bb;
	this.bbID = bb.getBBID();
	this.conn = conn;
    }


    static final String checkTicket =
	"SELECT 1 FROM ETICKETS WHERE BB_ID=? AND CONTENT_ID=? AND " +
	"    REVOKE_DATE IS NULL";

    static final String checkContent =
	"SELECT COUNT(*) FROM CONTENT_OBJECTS A, CONTENT_OBJECTS B WHERE " +
	"    A.CONTENT_ID=? AND B.CONTENT_ID=? AND A.REVOKE_DATE IS NULL " +
	"    AND B.REVOKE_DATE IS NULL AND " +
	"    A.CONTENT_OBJECT_NAME = B.CONTENT_OBJECT_NAME AND " +
	"    B.MIN_UPGRADE_VERSION <= A.CONTENT_OBJECT_VERSION";

    static final String updateTicket =
	"UPDATE ETICKETS SET CONTENT_ID=? WHERE CONTENT_ID=? AND BB_ID=?";

    /**
     * Upgrade a specific eTicket from an old content to a new one.
     * @param oldCID The original content ID.
     * @param newCID The new content ID to upgrade to.
     * @exception SQLException Any database access error.
     * @exception ETicketUpgradeException Invalid parameters or data
     *			inconsistency that forbids this upgrade.
     * @exception DBException Other data inconsistency error.
     */
    public void process(long oldCID, long newCID)
	throws SQLException, ETicketUpgradeException, DBException
    {
	// check if this BB owns the old content
	PreparedStatement ps = conn.prepareStatement(checkTicket);
	ps.setLong(1, bbID);
	ps.setLong(2, oldCID);
	try {
	    ResultSet rs = ps.executeQuery();
	    if (! rs.next())
		throw new ETicketUpgradeException("Player does not own content " + oldCID);
	    rs.close();
	    ps.close();

	    // check if the old and new content refers to the same content
	    ps = conn.prepareStatement(checkContent);
	    ps.setLong(1, oldCID);
	    ps.setLong(2, newCID);
	    rs = ps.executeQuery();

	    if (! rs.next())
		throw new DBException("Failed to verify content IDs.");
	    if (rs.getInt(1) < 1)
		throw new ETicketUpgradeException("No upgrade path exists from content ID " + oldCID + " to " + newCID);
	    rs.close();
	    ps.close();

	    ps = conn.prepareStatement(updateTicket);
	    ps.setLong(1, newCID);
	    ps.setLong(2, oldCID);
	    ps.setLong(3, bbID);
	    ps.executeUpdate();

	    // update the timestamp because the db state has changed.
	    bb.setSyncTime();
	    bb.updateRecord(conn);
	    rs.close();

	} finally {
	    ps.close();
	}
    }
}

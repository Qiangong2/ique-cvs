package com.broadon.trans.common;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.sql.DataSource;

import com.broadon.servlet.ServletConstants;

public class AuditLogManager
    implements ServletContextListener, ServletConstants
{
    public void contextInitialized(ServletContextEvent event)
    {
	ServletContext context = event.getServletContext();
	DataSource ds = (DataSource) context.getAttribute(DATA_SOURCE_KEY);

	// start the AuditLog thread
	AuditLog logger = new AuditLog(ds);
	logger.start();

	context.setAttribute(Logger.LOGGER_KEY, logger);
    }
    

    public void contextDestroyed(ServletContextEvent event)
    {
	ServletContext context = event.getServletContext();
	
	AuditLog logger = (AuditLog) context.getAttribute(Logger.LOGGER_KEY);
	if (logger != null) {
	    logger.shutdown();
	    try {
		logger.join(15*1000); // wait 15 seconds for it to die
	    } catch (InterruptedException e) {}
	}
    }
}

package com.broadon.trans.ops;

import javax.servlet.http.*;
import java.sql.*;
import java.io.IOException;

import com.broadon.filter.authWrapper;
import com.broadon.servlet.ContextStatus;

import com.broadon.trans.common.*;


public class Monitor extends TransactionServlet
{
    protected void process(Connection conn, authWrapper req,
                           TransactionRequest transReq, XMLResponse res)
	throws SQLException
    {
	ContextStatus status = (ContextStatus) ctx.getAttribute(MONITOR_STATUS_KEY);

	try {
	    if (status.getOperationStatus() == status.OS_LB_NOTIFIED) {
		res.error(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		status.setHealth(false);
		return;
	    }

	    // check hsm
	    byte[] rand = hsm.getRandomBytesNoCache(1);
	    
	    // check database
	    PreparedStatement ps = conn.prepareStatement("SELECT SYSDATE FROM DUAL");
	    try {
		ResultSet rs = ps.executeQuery();
	    } finally {
		ps.close();
	    }

	    status.setHealth(true);
	} catch (Exception e) {
	    status.setHealth(false);
	    try {
		res.error(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
	    } catch (IOException ioe) {
		throw new IllegalStateException(ioe.getMessage());
	    }
	}	    
    }

    protected TransactionRequest readInputs(XMLResponse res,
                                            HttpServletRequest req)
    {
	return null;
    }
}

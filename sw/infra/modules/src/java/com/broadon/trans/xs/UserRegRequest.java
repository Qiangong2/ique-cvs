package com.broadon.trans.xs;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.text.ParsePosition;

import com.broadon.trans.common.TransactionRequest;

public class UserRegRequest extends TransactionRequest
{
    protected String emailAddress;
    
    protected String name;

    protected String pinyinName;

    protected String address;

    protected Date birthday;

    protected String telephone;

    protected String gender;

    protected long bbID;

    protected String otherInfo;

    SimpleDateFormat fmt;

    protected UserRegRequest() {
	super();
	fmt = new SimpleDateFormat("yyyyMMdd");
    }
    
    protected void setBirthday(String s) {
	java.util.Date date = fmt.parse(s, new ParsePosition(0));
	birthday = new Date(date.getTime());
    }

    protected String formatBirthday() {
	return fmt.format(birthday);
    }
}

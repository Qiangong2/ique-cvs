package com.broadon.trans.common;

import java.sql.*;
import java.util.Vector;
import java.security.GeneralSecurityException;

import com.broadon.security.SecureMessageTags;
import com.broadon.filter.authWrapper;

/**
 * Database interface to access the Service Techincian authentication info.
 */
public class ServiceTech
{
    /** Depot installer role */ 
    public static final int installer = 140;

    /** Smart Cart issuer */
    public static final int smartCardIssuer = 160;

    /** RMA operator role */
    public static final int rmaOperator = 170;

    /** Bonus Game Issuer role */
    public static final int bonusGameIssuer = 190;


    static final String validateByEmail =
	"SELECT A.OPERATOR_ID " +
	"FROM OPERATION_USERS A, OPERATION_USER_ROLES B " +
	"WHERE LOWER(A.EMAIL_ADDRESS) = ? AND A.PASSWD = ? AND " +
	"      A.OPERATOR_ID = B.OPERATOR_ID AND B.BU_ID = ? AND " +
	"      B.ROLE_LEVEL = ? AND B.IS_ACTIVE >= 1";

    static final String validateByOpID =
	"SELECT A.OPERATOR_ID " +
	"FROM OPERATION_USERS A, OPERATION_USER_ROLES B " +
	"WHERE A.OPERATOR_ID = ? AND A.OPERATOR_ID = B.OPERATOR_ID AND " +
	"      B.BU_ID = ? AND B.ROLE_LEVEL = ? AND B.IS_ACTIVE >= 1";
	

    private static Long getOpID(authWrapper req)
	throws GeneralSecurityException
    {
	Boolean authenticated = (Boolean)
	    req.getAttribute(SecureMessageTags.REQUEST_AUTH_TAG);
	
	// if the smart card is not used or is not authenticated,
	// we switch to use the user id and password.  Otherwise,
	// we pick up the operator ID directly from the smartcard.
	if (authenticated == null || ! authenticated.equals(Boolean.TRUE))
	    return null;
	
	return (Long) req.getAttribute(SecureMessageTags.REQUESTOR_TAG);
    } // getOpID
    

    /**
     * Check if the given operator has enough privilege to exercise
     * the given role.
     * @param conn Database connection to use.
     * @param buID Business Unit ID
     * @param userID ID of the service technician.
     * @param password Password of the service technician.
     * @param req The servlet request that contains the smartcard
     * authentication information, if any.
     * @param role The role level needed to perform the operation.
     * @return The operator ID if it has enough privilege,
     * <code>null</code> otherwise.
     * @exception SQLException Any database access error.
     */
    public static long validate(Connection conn, int buID,
				String userID, String password,
				authWrapper req, long role)
	throws SQLException, GeneralSecurityException
    {
	Long opID = getOpID(req);

	if (opID == null && userID == null)
	    throw new GeneralSecurityException();
	
	PreparedStatement ps;
	int i = 0;

	if (opID == null) {
	    ps = conn.prepareStatement(validateByEmail);
	    ps.setString(++i, userID.toLowerCase());
	    ps.setString(++i, password);
	    ps.setInt(++i, buID);
	    ps.setLong(++i, role);
	} else {
	    ps = conn.prepareStatement(validateByOpID);
	    ps.setLong(++i, opID.longValue());
	    ps.setInt(++i, buID);
	    ps.setLong(++i, role);
	}

	try {
	    ResultSet rs = ps.executeQuery();
	    if (rs.next()) {
		return (opID == null) ? rs.getLong(1) : opID.longValue();
	    }
	    throw new GeneralSecurityException();
	} finally {
	    ps.close();
	}
    } // isValid


    static final String getRolesByEmail =
	"SELECT C.ROLE_NAME " +
	"FROM OPERATION_USERS A, OPERATION_USER_ROLES B, OPERATION_ROLES C " +
	"WHERE LOWER(A.EMAIL_ADDRESS) = ? AND A.PASSWD = ? AND " +
	"      A.OPERATOR_ID = B.OPERATOR_ID AND B.BU_ID = ? AND " +
	"      B.IS_ACTIVE >= 1 AND B.ROLE_LEVEL = C.ROLE_LEVEL " +
	"ORDER BY C.ROLE_NAME";

    static final String getRolesByOpID =
	"SELECT C.ROLE_NAME " +
	"FROM OPERATION_USERS A, OPERATION_USER_ROLES B, OPERATION_ROLES C " +
	"WHERE A.OPERATOR_ID = ? AND A.OPERATOR_ID = B.OPERATOR_ID AND " +
	"      B.BU_ID = ? AND B.IS_ACTIVE >= 1 AND B.ROLE_LEVEL = C.ROLE_LEVEL " +
	"ORDER BY C.ROLE_NAME";

    /**
     * Retrieve all the roles of a given user.
     * @param conn Database connection to use.
     * @param buID Business Unit ID
     * @param userID ID of the service technician.
     * @param password Password of the service technician.
     * @param req The servlet request that contains the smartcard
     * authentication information, if any.
     * @exception SQLException Any database access error.
     * @return All approves roles.
     */
    public static String[] getRoles(Connection conn, int buID, String userID,
				    String password, authWrapper req)
	throws SQLException, GeneralSecurityException
    {
	Long opID = getOpID(req);

	if (opID == null && userID == null)
	    throw new GeneralSecurityException();

	PreparedStatement ps;
	int i = 0;

	if (opID == null) {
	    ps = conn.prepareStatement(getRolesByEmail);
	    ps.setString(++i, userID.toLowerCase());
	    ps.setString(++i, password);
	    ps.setInt(++i, buID);
	} else {
	    ps = conn.prepareStatement(getRolesByOpID);
	    ps.setLong(++i, opID.longValue());
	    ps.setInt(++i, buID);
	}
	try {
	    ResultSet rs = ps.executeQuery();
	    Vector v = new Vector();
	    while (rs.next()) {
		v.addElement(rs.getString(1));
	    }
	    if (v.size() <= 0)
		throw new GeneralSecurityException();

	    String[] result = new String[v.size()];
	    return (String[]) v.toArray(result);
	} finally {
	    ps.close();
	}
    }
}

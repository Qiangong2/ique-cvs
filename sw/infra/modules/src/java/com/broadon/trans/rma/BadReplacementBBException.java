package com.broadon.trans.rma;

import com.broadon.exception.BroadOnException;

/**
 * The <code>BadReplacementBBException</code> class is thrown when the
 * body of a request contains invalid or inconsistent information.
 *
 * @version	$Revision: 1.2 $
 */
public class BadReplacementBBException
    extends BroadOnException
{
    /**
     * Constructs a BadReplacementBBException instance.
     */
    public BadReplacementBBException()
    {
	super();
    }

    /**
     * Constructs a BadReplacementBBException instance.
     *
     * @param	message			the exception message
     */
    public BadReplacementBBException(String message)
    {
	super(message);
    }

    /**
     * Constructs a BadReplacementBBException instance.
     *
     * @param	message			the exception message
     * @param	throwable		the nested exception
     */
    public BadReplacementBBException(String message, Throwable throwable)
    {
	super(message, throwable);
    }
}

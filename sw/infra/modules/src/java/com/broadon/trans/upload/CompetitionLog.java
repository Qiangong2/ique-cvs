package com.broadon.trans.upload;

import java.sql.*;

import com.broadon.trans.common.Logger;

/**
 * Logger for BB player data upload.  
 */
public class CompetitionLog extends Logger
{
    static public final long MISSING_VALUE_LONG = -1L;
    static public final int MISSING_VALUE_INT = -1;

    static final String addLog =
	"INSERT INTO COMPETING_CUSTOMER_LOGS (TITLE_ID, CONTENT_ID, BB_ID, " +
	"    HR_ID, REQUEST_DATE) " +
	"VALUES (?, ?, ?, ?, " + TIMESTAMP + ")";

    static final String updateLog =
	"UPDATE COMPETING_CUSTOMER_LOGS SET REQUEST_STATUS=?, REQUEST_LOG=? " +
	"    WHERE BB_ID=? AND HR_ID = ? AND REQUEST_DATE = " + TIMESTAMP;

    long bbID;
    long hrID;
    long titleID;
    long contentID;
    int  competitionID;

    String status;
    String message;

    boolean flushed;

    static public String TimeConvStr() {return TIMESTAMP;}

    /**
     * Create an <code>CompetitionLog</code> object holding the
     * values to be inserted into the audit log.
     */
    public CompetitionLog(long bbID, 
                          long depotID, 
                          int  competitionID,
                          long titleId, 
                          long contentID)
    {
	super();
	
	this.bbID = bbID;
	this.hrID = depotID;
	this.titleID = titleId;
	this.contentID = contentID;
	this.competitionID = competitionID;

	flushed = false;
	
	status = message = null;
    }


    synchronized public void flush(Connection conn) throws SQLException
    {
	PreparedStatement ps;
	int i = 0;

	if (flushed) {
	    // already written once, need to update the existing record
	    ps = conn.prepareStatement(updateLog);
	    ps.setString(++i, status);
	    ps.setString(++i, message);
	} else {
	    ps = conn.prepareStatement(addLog);
	    ps.setLong(++i, titleID);
	    ps.setLong(++i, contentID);
	}

	ps.setLong(++i, bbID);
	ps.setLong(++i, hrID);
	ps.setLong(++i, timestamp);

	try {
	    ps.executeUpdate();
	} finally {
	    ps.close();
	}

	flushed = true;
    }

    /**
     * Set the status of eTicket sync and possibly an error
     * message. 
     */
    synchronized public void setStatus(String status, String message)
    {
        this.status = status;
        if (competitionID == MISSING_VALUE_INT)
            this.message = "";
        else
            this.message = "[competition_id=" + competitionID + "]";

        if (message != null)
            this.message += 
                message.substring(0,Math.min(message.length(),240));
    }
}

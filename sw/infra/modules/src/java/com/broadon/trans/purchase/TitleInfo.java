package com.broadon.trans.purchase;

import java.sql.*;

import com.broadon.db.DBException;

/**
 * Read all info related to a Title for purchasing purpose.
 */
public class TitleInfo {

    private static final String getTitleInfo =
	"{ ? = call xsops.title_purchase_info (?,?,?,?,?,?,?,?) }";

    private TitleContentInfo contentInfo;
    private int              price = 0;
    private String           lr_type = TitleContentInfo.RTYPE_UNLIMITED;
    private int              lr_limits = 0;
    private int              rental_init = 0;
    private int              rental_next = 0;
    private int              trial_limits = 0;
    private int              bonus_limits = 0;
    
    private TitleInfo() {} // Force construction through "read".

    private void readTitleInfo(Connection conn, 
                               long       titleID,
                               int        regionID)
        throws SQLException, DBException
    {
        // Precondition: Must have already readContentInfo().
        // Reads the purchase restrictions and price for the title 
        // from the DB.
        //
        
        CallableStatement cs = conn.prepareCall(getTitleInfo);
        cs.registerOutParameter(1, Types.INTEGER);
        cs.setLong(2, titleID);
        cs.setInt(3, regionID);
        cs.registerOutParameter(4, Types.VARCHAR);
        cs.registerOutParameter(5, Types.INTEGER);
        cs.registerOutParameter(6, Types.INTEGER);
        cs.registerOutParameter(7, Types.INTEGER);
        cs.registerOutParameter(8, Types.INTEGER);
        cs.registerOutParameter(9, Types.INTEGER);
        try {
            cs.executeQuery();
            price = cs.getInt(1);
            if (cs.wasNull())
                price = -1;  // No price

            // Get information about limited play
            //
            lr_type = cs.getString(4);
            if (lr_type != null && allowsLimitedPlay()) {
                lr_limits = cs.getInt(5);
                rental_init = cs.getInt(6);
                rental_next = cs.getInt(7);
                trial_limits = cs.getInt(8);
                bonus_limits = cs.getInt(9);
            }
            else {
                // We do not support XR currently
                //
                lr_type = TitleContentInfo.RTYPE_UNLIMITED;
            }
        } finally {
            cs.close();
        }
    } // readTitleInfo


    private void readContentInfo(Connection conn, 
                                 long       bbID,
                                 long       titleID,
                                 String     model)
        throws SQLException, DBException
    {
        contentInfo = 
            TitleContentInfo.readContentInfo(conn, bbID, titleID, model);
    } // readContentIDs


    /**
     * Create a <code>TitleInfo</code> object with data read from the database.
     * @param conn Database connection.
     * @param titleID ID of the title.
     * @param regionID The region to which the title attributes apply.
     * @return A TitleInfo object.
     * @exception SQLException
     * @exception DBException
     */
    public static TitleInfo read(Connection conn, 
                                 long       bbID,
                                 long       titleID,
                                 int        regionID, 
                                 String     model)
	throws SQLException, DBException
    {
        TitleInfo info = new TitleInfo();
        info.readTitleInfo(conn, titleID, regionID);
        info.readContentInfo(conn, bbID, titleID, model);
        return info;
    }
    

    /** Accessor method for title information
     * @return the regional price of the title. 
     */
    public int    getPrice() {return price;}

    
    /** Determines if limited play is allowed based on the rtype
     *  @return true if limited play is allowed.
     */
    public boolean allowsLimitedPlay() {
        return (lr_type.equals(TitleContentInfo.RTYPE_TIME_LIMITED) || 
                lr_type.equals(TitleContentInfo.RTYPE_NPLAY_LIMITED));
    }

    /** Accessor method for the type of limited play allowed for the title
     * @return LR or TR for nplay/time limited play, and PR if no
     * limited play is allowed.
     */
    public String getRtype() {return lr_type;}

    /** Accessor method for the limits on a rental purchase
     * @return seconds or number-of-plays.
     */
    public int    getRentalLimits() {return lr_limits;}

    /** Accessor method for the prive on a rental purchase
     * @return price for first rental of a title.
     */
    public int    getRentalInitPrice() {return rental_init;}

    /** Accessor method for the price on a rental purchase
     * @return price for subsequent rental of a title.
     */
    public int    getRentalNextPrice() {return rental_next;}

    /** Accessor method for the limits on a trial purchase
     * @return seconds or number-of-plays.
     */
    public int    getTrialLimits() {return trial_limits;}

    /** Accessor method for the limits on a bonus purchase
     * @return seconds or number-of-plays.
     */
    public int    getBonusLimits() {return bonus_limits;}

    /** Accessor method for the content related information for the title
     * @return all valid ContentIDs for the given title.
     */
    public TitleContentInfo getContentIDs() {
        return contentInfo;
    }

}

package com.broadon.trans.common;

/** Dummy base class for holding all transaction data that has been
parsed and converted to the correct type.  It is empty because
currently there is no parameter that is common to all requests. */
public class TransactionRequest
{
}

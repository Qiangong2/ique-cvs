package com.broadon.cas;

public class Limit {
    private String limitCode;
    private long limitValue;
    
    public Limit() {}
    
    public long getValue() {
        return limitValue;
    }
    public void setValue(long value) {
        this.limitValue = value;
    }
    public String getCode() {
        return limitCode;
    }
    public void setCode(String code) {
        this.limitCode = code;
    }
}

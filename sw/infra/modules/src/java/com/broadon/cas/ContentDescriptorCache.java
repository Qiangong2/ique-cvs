package com.broadon.cas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.StringTokenizer;

import javax.sql.DataSource;

import com.broadon.db.DataSourceScheduler;
import com.broadon.util.BackingStore;
import com.broadon.util.BackingStoreException;
import com.broadon.util.CacheLFU;

/**
 * The <code>TitleInfoCache</code> class provides a cache
 * for the TitleInfoType using the titleId as a key.
 *
 * @version $Revision: 1.4 $
 */
public class ContentDescriptorCache 
    extends CacheLFU implements BackingStore 
{    
    private DataSource dataSource;

    private static final String SELECT_ATTR = 
        "SELECT ATTR_VALUES FROM ATTRIBUTE_LOOKUPS " +
        "WHERE ATTR_TYPE = 'RATING_ATTRIBITS' AND ATTR_NAME = ? AND LOCALE = ?";

    public ContentDescriptorCache(DataSource dataSource, long timeoutMsec)
    {
        super(timeoutMsec);
        this.dataSource = dataSource;
    }

    public Object retrieve(Object key) throws BackingStoreException
    {
        Key attrKey = (Key) key;
        ContentDescriptor[] contentDescriptors = null;
        
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = BaseBean.getConnection(dataSource, DataSourceScheduler.DB_PRIORITY_FAST);
            ps = conn.prepareStatement(SELECT_ATTR);
            ps.setString(1, attrKey.name);
            ps.setString(2, attrKey.locale);
            rs = ps.executeQuery();
            while (rs.next()) {
                String values = rs.getString(1);
                StringTokenizer st = new StringTokenizer(values, ",");
                int nTokens = st.countTokens();
                contentDescriptors = new ContentDescriptor[nTokens];
                for (int i = 0; i < nTokens; i++) {
                    contentDescriptors[i] = new ContentDescriptor(i, st.nextToken());
                }
            }
        } catch (SQLException e) {
            throw BackingStoreException.rewrap(e);            
        } finally {
            if (rs != null) {
                try { rs.close(); } catch (SQLException e) {}
            }
            if (ps != null) {
                try { ps.close(); } catch (SQLException e) {}
            }
            if (conn != null) {
                try { conn.close(); } catch (SQLException e) {}
            }
        }
        return contentDescriptors;
    }

    /**
     * Key for looking up an cached entry by rating system and locale
     */
    public static class Key {
        public final String name;
        public final String locale;
        
        public Key(String name, String locale)
        {
            this.name = name;
            this.locale = locale;
        }

        public boolean equals(Object obj)
        {
            if (obj instanceof Key) {
                Key other = (Key) obj;
                return (this.name.equals(other.name)) && (this.locale.equals(other.locale));
            } else {
                return false;
            }
        }
        
        public int hashCode()
        {
            return this.name.hashCode() + this.locale.hashCode();
        }
        
        public String toString()
        {
            return "name " + name + ", locale " + locale; 
        }
    }
}

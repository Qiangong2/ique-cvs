package com.broadon.cas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Vector;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class CountryListBean extends BaseBean {
    protected static Log log = LogFactory.getLog(CountryListBean.class);
    private List countries;

    public CountryListBean() {
        countries = new Vector();
    }
    
    private static String COUNTRIES = "SELECT a.country_id, a.country, "
            + "a.default_locale, a.default_currency, b.language_code, b.locale "
            + "FROM countries a, country_locales b "
            + "where a.country_id = b.country_id AND region = ? "
            + "ORDER BY a.country_id";

    public List list() {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        String region = getRegion();
        countries.clear();
        try {
            conn = getConnection();
            StringBuffer queryStr = new StringBuffer(COUNTRIES);
            log.debug(queryStr.toString());
            log.debug("region: " + region);
            stmt = conn.prepareStatement(queryStr.toString());
            stmt.setString(1, region);
            rs = queryAll(stmt);
            Country country = null;
            while (rs.next()) {
                int i = 1;
                int countryId = rs.getInt(i++);
                boolean newCountry = ((country == null) || (country.getCountryId() != countryId));
                if (newCountry) {
                    country = new Country();
                    country.setCountryId(countryId);
                    country.setCode(rs.getString(i++));
                    country.setName(getCountryName(country.getCode(), country.getDefaultLocale()));
                    country.setRegion(region);
                    country.setDefaultLocale(rs.getString(i++));
                    country.setDefaultCurrency(rs.getString(i++));
                    countries.add(country);
                } else {
                    /* Skip fields */
                    i += 3;
                }
                String language = rs.getString(i++);
                String locale = rs.getString(i++);
                country.addLanguage(language, locale);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        } finally {
            closeResource(conn, stmt, rs);
        }
        return countries;
    }

    public List getCountries() {
        return countries;
    }
}

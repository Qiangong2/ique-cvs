package com.broadon.cas;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Types;
import java.util.Properties;
import oracle.jdbc.pool.OracleDataSource;
import oracle.sql.BLOB;

public class Uploader {
    // Database access parameters
    static final String DB_URL_KEY = "DB_URL";
    static final String DB_USER_KEY = "DB_USER";
    static final String DB_PASSWD_KEY = "DB_PASSWORD";
    static final String MET_TYPE = "MET";
    static final String COMMON_TICKET_TYPE = "CETK";
    static final long MET_CONTENT_ID = 0xFFFFFFFEL;
    static final long COMMON_TICKET_CONTENT_ID = 0xFFFFFFFDL;
    private long titleId;
    private String contentFileName = null;
    private File contentFile = null;
    private String contentType;
    private Properties dbProp;
    private OracleDataSource db;
    Connection conn = null;

    Uploader(File dbPropFile, long titleId, File contentFile,
            String contentFileName, String contentType) throws IOException,
            SQLException {
        this.contentFile = contentFile;
        this.titleId = titleId;
        this.contentFileName = contentFileName;
        this.contentType = contentType;
        dbProp = new Properties();
        FileInputStream in = new FileInputStream(dbPropFile);
        dbProp.load(in);
        in.close();
        db = new OracleDataSource();
        // System.out.println("db_url: " + dbProp.getProperty(DB_URL_KEY));
        // System.out.println("user: " + dbProp.getProperty(DB_USER_KEY));
        // System.out.println("password: " + dbProp.getProperty(DB_PASSWD_KEY));
        db.setURL(dbProp.getProperty(DB_URL_KEY));
        db.setUser(dbProp.getProperty(DB_USER_KEY));
        db.setPassword(dbProp.getProperty(DB_PASSWD_KEY));
        conn = db.getConnection();
        conn.setAutoCommit(false);
    }

    /**
     * Frees up database resources.
     */
    private void closeConnection() {
        try {
            if (conn != null) {
                conn.close();
                conn = null;
            }
        } catch (Exception e) {}
    }

    /**
     * Frees up database resources.
     */
    private void closeResource(Statement stmt, ResultSet rs) {
        try {
            if (stmt != null) {
                stmt.close();
            }
            if (rs != null) {
                rs.close();
            }
        } catch (Exception e) {}
    }
    static final String CHECK_TITLE_ID = "SELECT 1 FROM DUAL WHERE EXISTS (SELECT title_id "
            + "FROM CONTENT_TITLES WHERE title_id = ?)";

    private boolean checkTitleId() throws SQLException {
        System.out.println("Check title id");
        PreparedStatement ps = conn.prepareStatement(CHECK_TITLE_ID);
        ResultSet rs = null;
        try {
            ps.setLong(1, titleId);
            rs = ps.executeQuery();
            if (rs.next()) {
                return true;
            }
            return false;
        } finally {
            closeResource(ps, rs);
        }
    }

    // stream and the content into a temporary blob, and compute its checksum as
    // we go
    private BLOB writeTempBlob(ContentInputStream contentBlob)
            throws SQLException {
        System.out.println("Compute checksum");
        BLOB tempBlob = BLOB.createTemporary(conn, true, BLOB.DURATION_SESSION);
        OutputStream out = tempBlob.setBinaryStream(1);
        byte[] buffer = new byte[64 * 1024];
        int n;
        try {
            while ((n = contentBlob.read(buffer)) >= 0) {
                if (n == 0) // ContentInputSream.read() could return 0 before
                    // EOF
                    continue;
                out.write(buffer, 0, n);
            }
        } catch (IOException e) {
            SQLException se = new SQLException(e.getLocalizedMessage());
            se.setStackTrace(e.getStackTrace());
            throw se;
        } finally {
            try {
                out.close();
            } catch (IOException e) {}
        }
        return tempBlob;
    }
    static final String ADD_CONTENT_CACHE = "INSERT INTO CONTENT_CACHE (CONTENT_CHECKSUM, CONTENT_SIZE, CONTENT_OBJECT_TYPE, "
            + "       CONTENT_OBJECT_NAME, CONTENT_OBJECT_VERSION, CREATE_DATE, CONTENT_OBJECT) "
            + "VALUES (?, ?, ?, ?, ?, SYSDATE, ?)";
    static final String DELETE_CONTENT_CACHE1 = "DELETE FROM CONTENTS WHERE CONTENT_CHECKSUM = ?";
    static final String DELETE_CONTENT_CACHE2 = "DELETE FROM CONTENT_CACHE WHERE CONTENT_CHECKSUM = ?";

    private BigDecimal createContentCache(ContentInputStream contentBlob,
            String contentType, String contentName, int version)
            throws SQLException {
        System.out.println("Create content cache");
        PreparedStatement ps = null;
        Savepoint savePt = conn.setSavepoint();
        BLOB blob = writeTempBlob(contentBlob);
        try {
            // delete the previous content_cache row if any
            ps = conn.prepareStatement(DELETE_CONTENT_CACHE1);
            ps.setString(1, contentBlob.getMd5HashString());
            ps.executeUpdate();
            ps = conn.prepareStatement(DELETE_CONTENT_CACHE2);
            ps.setString(1, contentBlob.getMd5HashString());
            ps.executeUpdate();
            // insert the content_cache row
            ps = conn.prepareStatement(ADD_CONTENT_CACHE);
            int i = 1;
            ps.setString(i++, contentBlob.getMd5HashString());
            ps.setLong(i++, blob.length());
            ps.setString(i++, contentType);
            ps.setString(i++, contentName);
            if (version >= 0)
                ps.setInt(i++, version);
            else
                ps.setNull(i++, Types.SMALLINT);
            ps.setBlob(i++, blob);
            ps.executeUpdate();
        } finally {
            closeResource(ps, null);
        }
        return null;
    }
    static final String ADD_CONTENT = "INSERT INTO CONTENTS (CONTENT_ID, PUBLISH_DATE, CONTENT_CHECKSUM) "
            + "VALUES (?, SYSDATE, ?)";
    static final String DELETE_CONTENT = "DELETE FROM CONTENTS WHERE CONTENT_ID = ?";

    private void createContentRecord(BigDecimal contentID, String checkSum,
            int index) throws SQLException {
        System.out.println("Create content record");
        PreparedStatement ps = null;
        int i = 1;
        try {
            // delete the previous content row if any
            ps = conn.prepareStatement(DELETE_CONTENT);
            ps.setBigDecimal(1, contentID);
            ps.executeUpdate();
            // insert the content row
            ps = conn.prepareStatement(ADD_CONTENT);
            ps.setBigDecimal(i++, contentID);
            ps.setString(i++, checkSum);
            ps.executeUpdate();
        } finally {
            closeResource(ps, null);
        }
    } // createContentRecord

    private int createObject(ContentInputStream contentBlob,
            String contentType, String contentName, int version, int index)
            throws SQLException, GeneralSecurityException {
        BigDecimal cid = createContentCache(contentBlob, contentType,
                contentName, version);
        long contentId = 0;
        if (MET_TYPE.equals(contentType)) {
            contentId = MET_CONTENT_ID;
        } else if (COMMON_TICKET_TYPE.equals(contentType)) {
            contentId = COMMON_TICKET_CONTENT_ID;
        }
        if (cid == null)
            cid = genContentID(titleId, contentId);
        // insert the content record
        createContentRecord(cid, contentBlob.getMd5HashString(), index);
        return extractContentID(cid);
    }

    private BigDecimal genContentID(long tid, long contentID) {
        // BigInteger version of ((titleID << 32) | contentID)
        return new BigDecimal(BigInteger.valueOf(tid).shiftLeft(32).or(
                BigInteger.valueOf(contentID)));
    }

    private int extractContentID(BigDecimal cid) {
        // BigInteger version of (cid & 0xffffffff)
        return cid.toBigInteger().and(BigInteger.valueOf(0xFFFFFFFFL))
                .intValue();
    }

    private String genMETName() {
        return genContentID(titleId, MET_CONTENT_ID).toBigInteger()
                .toString(16)
                + ' ' + MET_TYPE;
    }

    private String genCommonTicketName() {
        return genContentID(titleId, COMMON_TICKET_CONTENT_ID).toBigInteger()
                .toString(16)
                + ' ' + COMMON_TICKET_TYPE;
    }

    private int createMET(ContentInputStream contentBlob) throws SQLException,
            GeneralSecurityException {
        return createObject(contentBlob, MET_TYPE, genMETName(), -1, 1);
        //return createObject(contentBlob, MET_TYPE, contentFileName, -1, 1);
    }

    private int createCommonTicket(ContentInputStream contentBlob)
            throws SQLException, GeneralSecurityException {
        return createObject(contentBlob, COMMON_TICKET_TYPE,
                genCommonTicketName(), -1, 1);
        //return createObject(contentBlob, COMMON_TICKET_TYPE, contentFileName,
        //        -1, 1);
    }

    private int createContent() throws SQLException, GeneralSecurityException,
            FileNotFoundException {
        TrivialContentInputStream contentBlob = new TrivialContentInputStream(
                new FileInputStream(contentFile));
        if (MET_TYPE.equalsIgnoreCase(contentType)) {
            return createMET(contentBlob);
        } else if (COMMON_TICKET_TYPE.equalsIgnoreCase(contentType)) {
            return createCommonTicket(contentBlob);
        }
        return -1;
    }

    public void upload() throws SQLException, GeneralSecurityException,
            FileNotFoundException {
        if (!checkTitleId()) {
            System.err.println("Error: title_id " + titleId + " is not valid.");
            return;
        }
        createContent();
        System.out.println("Done.");
    }

    public static void main(String[] args) {
        Uploader uploader = null;
        try {
            if (args.length != 4) {
                System.err.println("Usage: Uploader db_property title_id "
                        + "content_file type");
                return;
            }
            File dbPropFile = new File(args[0]);
            if (!dbPropFile.exists()) {
                System.err.println("Error: db_property " + args[0]
                        + " does not exist.");
                return;
            }
            long titleId = 0;
            try {
                titleId = Long.parseLong(args[1], 16);
            } catch (Exception ex) {
                System.err.println("Error: Wrong format of title_id " + args[1] +
                    ", the title_id should be a hex number.");
                return;
            }
            File contentFile = new File(args[2]);
            if (!contentFile.exists()) {
                System.err.println("Error: content_file " + args[2]
                        + " does not exist.");
                return;
            }
            String contentType = args[3];
            if (!MET_TYPE.equalsIgnoreCase(contentType)
                    && !COMMON_TICKET_TYPE.equalsIgnoreCase(contentType)) {
                System.err.println("Error: content_type " + args[3]
                        + " is not supported.");
                return;
            }
            uploader = new Uploader(dbPropFile, titleId, contentFile, args[2],
                    contentType);
            uploader.upload();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (uploader != null) {
                uploader.closeConnection();
            }
        }
    }
}

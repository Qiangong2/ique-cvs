package com.broadon.cas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ECardPointListBean extends PointListBean {
    protected static Log log = LogFactory.getLog(ECardPointListBean.class);
    String eCardType;

    protected static String ECARD_FILTER = " WHERE ecard_type = ?";

    public ECardPointListBean() {
        super();
    }

    protected String getCatalog()
    {
       return "current_ecard_points_catalog";            
    }
    
    protected String getQueryString()
    {
        StringBuffer queryStr = new StringBuffer(POINTS);
        queryStr.append(" FROM ");
        queryStr.append(getCatalog());
        queryStr.append(ECARD_FILTER);
        queryStr.append(getFilters());
        queryStr.append(getOrders());
        return queryStr.toString();
    }
    
    protected PreparedStatement getPreparedStatement(Connection conn)
        throws SQLException
    {
        String queryStr = getQueryString();
        log.debug(queryStr);
        log.debug("ecard type: " + getECardType());
        PreparedStatement stmt = conn.prepareStatement(queryStr);
        stmt.setString(1, getECardType());
        return stmt;
    }
    
    public String getECardType() {
        return eCardType;
    }

    public void setECardType(String cardType) {
        eCardType = cardType;
    }
    
}

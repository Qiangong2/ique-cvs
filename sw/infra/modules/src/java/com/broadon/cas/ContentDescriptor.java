package com.broadon.cas;

public class ContentDescriptor {
    private String description;
    private int code;
    
    public ContentDescriptor() {}

    public ContentDescriptor(int code, String description) {
        this.code = code;
        this.description = description;
    }
        
    public int getCode() {
        return code;
    }
    
    public void setCode(int code) {
        this.code = code;
    }
    
    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
}

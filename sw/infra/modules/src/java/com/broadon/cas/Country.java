package com.broadon.cas;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Data set wrapper for ResultSet.
 */
public class Country {
    protected static Log log = LogFactory.getLog(Country.class);

    private int countryId;
    private String name;
    private String code;
    private String region;
    private String defaultLocale;
    private String defaultCurrency;
    
    /* Map of supported locales and locales for the country */
    public Map locales;

    public Country() {
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public String getDefaultCurrency() {
        return defaultCurrency;
    }

    public void setDefaultCurrency(String defaultCurrency) {
        this.defaultCurrency = defaultCurrency;
    }

    public String getDefaultLocale() {
        return defaultLocale;
    }

    public void setDefaultLocale(String defaultLocale) {
        this.defaultLocale = defaultLocale;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void addLanguage(String language, String locale)
    {
        if (locales == null) {
            locales = new HashMap();
        }
        locales.put(language, locale);
    }
    
    public Map getLocales()
    {
        return locales; 
    }
    
    public Set getLanguages()
    {
        return locales.keySet();
    }
    
    public int getLanguageCount()
    {
        return locales.keySet().size();
    }
   
}

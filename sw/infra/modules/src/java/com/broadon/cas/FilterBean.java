package com.broadon.cas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.StringTokenizer;
import java.util.Vector;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class FilterBean extends BaseBean {
    protected static Log log = LogFactory.getLog(FilterBean.class);
    private String filterBy;
    private Vector filterList;

    public FilterBean() {
        filterList = new Vector();
    }
    private static String FILTER_QUERY = 
        "SELECT ATTR_VALUES "
        + " FROM ATTRIBUTE_LOOKUPS "
        + " WHERE ATTR_TYPE = 'CONTENT_TITLES' AND LOCALE = 'en_US' "
        + " AND ATTR_NAME = ?";    

    public Vector list() {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        filterList.clear();
        try {
            conn = getConnection();
            String queryStr = FILTER_QUERY;;
            log.debug(queryStr);
            log.debug("filterBy " + filterBy);
            stmt = conn.prepareStatement(queryStr);
            stmt.setString(1, filterBy);
            rs = queryAll(stmt);
            while (rs.next()) {
                String filterValues = rs.getString(1);
                if (filterValues != null) {
                    StringTokenizer st = new StringTokenizer(filterValues, ",");
                    while (st.hasMoreTokens()) {
                        filterList.add(st.nextToken());
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        } finally {
            closeResource(conn, stmt, rs);
        }
        return filterList;
    }

    public Vector getFilterList() {
        return filterList;
    }

    public String getFilterBy() {
        return filterBy;
    }

    public void setFilterBy(String filterBy) {
        this.filterBy = filterBy;
    }
}

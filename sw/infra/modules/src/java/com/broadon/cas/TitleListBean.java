package com.broadon.cas;

import java.util.List;
import java.util.Vector;

public class TitleListBean extends BaseBean {
    protected List titles;
    protected int page = 1;
    protected int totalCount = -1;

    public TitleListBean() {
        titles = new Vector();
    }

    public void refresh() {
        super.refresh();
        titles.clear();
        totalCount = -1;
    }

    public void addFilter(String by, String value) {
        if ("title".equalsIgnoreCase(by)) {
            addFilter(by, value, true); 
        } else {
            addFilter(by, value, false);             
        }
    }

    public List getTitles() {
        return titles;
    }

    public int getTotalCount() {
        return totalCount;
    }
    
    public int getPageCount() {
        int mod = totalCount % getPageSize();        
        return totalCount / getPageSize() + (mod > 0 ? 1: 0);
    }
    
    public int getFirstEntryIndex() {
        return getPageSize() * (page - 1) + 1;
    }
    
    public int getLastEntryIndex() {
        int lastIndex = getPageSize() * (page);
        if (lastIndex > totalCount) {
            lastIndex = totalCount;
        }
        return lastIndex;
    }
    
    public void searchTitle(String title) {
        refresh();
        addFilter("upper(title)", title.toUpperCase(), true);
    }
    
    public void showRecommended() {
        refresh();
        addFilterNull("recommend_date", false);
    }

    public void showNew() {
        refresh();
        addFilterNull("new_since", false);
    }
    
    public void setCurrencyFilter(String currency) {
        refresh();
        addFilter("item_currency", currency);
    }
            
    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

}

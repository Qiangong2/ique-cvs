package com.broadon.cas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class MessagesBean extends BaseBean {
    private static String GET_MESSAGES = 
        "SELECT attr_values, attr_name from attribute_lookups " 
            + "WHERE LOCALE = ? AND ATTR_TYPE = ? AND ATTR_NAME LIKE ?";
    
    private List messages;
    private String messageType;
    
    public MessagesBean()
    {
        messages = new ArrayList();
    }
    
    public List list() {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        String region = getRegion();
        messages.clear();
        try {
            conn = getConnection();
            String queryStr = GET_MESSAGES;
            log.debug(queryStr);
            log.debug("messageType: " + messageType);
            String countryFilter = getCountryCode() + ".%"; 
            stmt = conn.prepareStatement(queryStr);
            stmt.setString(1, getLocale());
            stmt.setString(2, messageType);
            stmt.setString(3, countryFilter);
            rs = queryAll(stmt);
            while (rs.next()) {
                int i = 1;
                String message = rs.getString(i++);
                String name = rs.getString(i++);
                int index = 0;
                index = Integer.parseInt(name.substring(getCountryCode().length() + 1));
                messages.add(index-1, message);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        } finally {
            closeResource(conn, stmt, rs);
        }
        return messages;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public List getMessages() {
        return messages;
    }


}

package com.broadon.cas;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Data set wrapper for ResultSet.
 */
public class TitlePricing {
    protected static Log log = LogFactory.getLog(TitlePricing.class);
    private Title title;
    private Pricing pricing;

    public TitlePricing() {}

    public Pricing getPricing() {
        return pricing;
    }

    public void setPricing(Pricing pricing) {
        this.pricing = pricing;
    }

    public Title getTitle() {
        return title;
    }

    public void setTitle(Title title) {
        this.title = title;
    }

}

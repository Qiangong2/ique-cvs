package com.broadon.cas;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ECardTitleListBean extends GameTitleListPricingsBean {
    protected static Log log = LogFactory.getLog(ECardTitleListBean.class);
    String eCardType;

    public ECardTitleListBean() {
        super();
    }

    protected String getCatalog()
    {
        return "current_ecard_game_catalog";
    }

    public String getECardType() {
        return eCardType;
    }

    public void setECardType(String cardType) {
        eCardType = cardType;
        addFilter(ECARD_TYPE, eCardType);
    }
    
}

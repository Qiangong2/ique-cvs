package com.broadon.cas;

import java.math.BigDecimal;
import java.sql.Date;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Data set wrapper for ResultSet.
 */
public class Point {
    protected static Log log = LogFactory.getLog(Point.class);

    private int itemId;
    private BigDecimal itemPrice;
    private String itemCurrency;
    private int refillPoints;
    private Date purchaseStartDate;
    private Date purchaseEndDate;

    public Point() {
    }

    public String getItemCurrency() {
        return itemCurrency;
    }

    public void setItemCurrency(String itemCurrency) {
        this.itemCurrency = itemCurrency;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public BigDecimal getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(BigDecimal itemPrice) {
        this.itemPrice = itemPrice;
    }

    public Date getPurchaseEndDate() {
        return purchaseEndDate;
    }

    public void setPurchaseEndDate(Date purchaseEndDate) {
        this.purchaseEndDate = purchaseEndDate;
    }

    public Date getPurchaseStartDate() {
        return purchaseStartDate;
    }

    public void setPurchaseStartDate(Date purchaseStartDate) {
        this.purchaseStartDate = purchaseStartDate;
    }

    public int getRefillPoints() {
        return refillPoints;
    }

    public void setRefillPoints(int refillPoints) {
        this.refillPoints = refillPoints;
    }

}

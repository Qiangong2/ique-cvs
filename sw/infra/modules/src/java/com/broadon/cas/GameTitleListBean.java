package com.broadon.cas;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class GameTitleListBean extends TitleListBean {
    protected static Log log = LogFactory.getLog(GameTitleListBean.class);

    public GameTitleListBean() {
        super();
    }

    private static String TITLES = 
        "SELECT unique bccutil.dec2hex(title_id, 16), "
        + "locale, title, short_phrase, description, category, platform, publisher, "
        + "approx_size, recommend_date, new_since, popularity,"
        + "release_date, "
        + "BCCUTIL.ConvertTimeStamp(publish_date), "
        + "title_version, attribits, "
        + "rating_system, rating_value, rating_age, rating_attribits, "
        + "second_system, second_value, second_age, second_attribits, hidden_title "
        + "FROM current_game_pricing_catalog "
        + "WHERE country = ? AND language_code = ?";
    
    private static String COUNT_TITLES =
        "SELECT count(unique bccutil.dec2hex(title_id, 16)) "
        + "FROM current_game_pricing_catalog "
        + "WHERE country = ? AND language_code = ?";

    public List list() {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        titles.clear();
        try {
            conn = getConnection();
            StringBuffer queryStr = new StringBuffer(TITLES);
            queryStr.append(getFilters());
            queryStr.append(getOrders());
            log.debug(queryStr.toString());
            log.debug("country: " + getCountryCode());
            log.debug("language_code: " + getLanguageCode());
            
            if (totalCount == -1) {
                StringBuffer countStrBuf = new StringBuffer(COUNT_TITLES);
                countStrBuf.append(getFilters());
                String countStr = countStrBuf.toString();
                stmt = conn.prepareStatement(countStr);
                stmt.setString(1, getCountryCode());
                stmt.setString(2, getLanguageCode());
                rs = stmt.executeQuery();
                if (rs.next()) {
                    totalCount = rs.getInt(1);
                }
            }
            stmt = conn.prepareStatement(queryStr.toString());
            stmt.setString(1, getCountryCode());
            stmt.setString(2, getLanguageCode());
            rs = queryPage(stmt, page);
            while (rs.next()) {
                Title title = new Title();
                int i = 1;
                title.setTitleId(rs.getString(i++));
                title.setLocale(rs.getString(i++));
                title.setName(rs.getString(i++));
                title.setPhrase(rs.getString(i++));
                title.setDescription(rs.getString(i++));
                title.setGenre(rs.getString(i++));
                title.setPlatform(rs.getString(i++));
                title.setPublisher(rs.getString(i++));
                title.setTitleSize(rs.getInt(i++));
                Date recommendDate = rs.getDate(i++);
                title.setRecommended(!rs.wasNull());
                Date newSince = rs.getDate(i++);
                title.setNew(!rs.wasNull());
                title.setPopularity(rs.getInt(i++));
                title.setReleaseDate(rs.getString(i++));
                long publishDateMs = rs.getLong(i++);
                if (!rs.wasNull()) {
                    title.setPublishDate(new Date(publishDateMs));
                }
                title.setVersion(rs.getInt(i++));
                title.setAttributes(rs.getLong(i++));
                Rating r1 = new Rating();
                r1.setSystem(rs.getString(i++));
                r1.setValue(rs.getString(i++));
                r1.setAge(rs.getInt(i++));
                r1.setAttributes(
                    getContentDescriptors(r1.getSystem(), title.getLocale()),                        
                    rs.getLong(i++));
                title.addRating(r1);
                String secondSystem = rs.getString(i++);
                if (!rs.wasNull()) {
                    Rating r2 = new Rating();
                    r2.setSystem(secondSystem);
                    r2.setValue(rs.getString(i++));
                    r2.setAge(rs.getInt(i++));
                    r2.setAttributes(
                        getContentDescriptors(r2.getSystem(), title.getLocale()),
                        rs.getLong(i++));                    
                    title.addRating(r2);
                }
                titles.add(title);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        } finally {
            closeResource(conn, stmt, rs);
        }
        return titles;
    }

}

package com.broadon.cas;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Vector;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class PointListBean extends BaseBean {
    protected static Log log = LogFactory.getLog(PointListBean.class);
    private List points;

    public PointListBean() {
        points = new Vector();
        addOrder(REFILL_POINTS);
    }
    
    protected static String POINTS = "SELECT item_id, item_price, "
            + "item_currency, refill_points, "
            + "BCCUTIL.ConvertTimeStamp(purchase_start_date), "
            + "BCCUTIL.ConvertTimeStamp(purchase_end_date) ";

    protected static String COUNTRY_FILTER = " WHERE country = ?";

    protected String getCatalog()
    {
       return "current_points_pricing_catalog";            
    }
    
    protected String getQueryString()
    {
        StringBuffer queryStr = new StringBuffer(POINTS);
        queryStr.append(" FROM ");
        queryStr.append(getCatalog());
        queryStr.append(COUNTRY_FILTER);
        queryStr.append(getFilters());
        queryStr.append(getOrders());
        return queryStr.toString();
    }
    
    protected PreparedStatement getPreparedStatement(Connection conn)
        throws SQLException
    {
        String queryStr = getQueryString();
        log.debug(queryStr);
        log.debug("country: " + getCountryCode());
        PreparedStatement stmt = conn.prepareStatement(queryStr);
        stmt.setString(1, getCountryCode());
        return stmt;
    }
    
    public List list() {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        points.clear();
        try {
            conn = getConnection();
            stmt = getPreparedStatement(conn);
            rs = queryAll(stmt);
            while (rs.next()) {
                Point point = new Point();
                int i = 1;
                point.setItemId(rs.getInt(i++));
                point.setItemPrice(rs.getBigDecimal(i++));
                point.setItemCurrency(rs.getString(i++));
                point.setRefillPoints(rs.getInt(i++));
                long purchaseStartMs = rs.getLong(i++);
                if (!rs.wasNull()) {
                    point.setPurchaseStartDate(new Date(purchaseStartMs));
                }
                long purchaseEndMs = rs.getLong(i++);
                if (!rs.wasNull()) {
                    point.setPurchaseEndDate(new Date(purchaseEndMs));
                }
                points.add(point);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        } finally {
            closeResource(conn, stmt, rs);
        }
        return points;
    }

    public List getPoints() {
        return points;
    }
}

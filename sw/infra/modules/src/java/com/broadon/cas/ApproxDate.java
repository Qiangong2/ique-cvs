package com.broadon.cas;

import java.sql.Date;
import java.util.Calendar;

/**
 * The <code>ApproxDate</code> class represents an approximate
 * date that may be missing the month or day.
 *
 * @version $Revision: 1.1 $
 */
public class ApproxDate extends Date {
    private boolean isMissingMonth;
    private boolean isMissingDay;
    
    public ApproxDate(long date, boolean isMissingMonth, boolean isMissingDay) {
        super(date);
        this.isMissingMonth = isMissingMonth;
        this.isMissingDay = isMissingDay;
    }
    
    public ApproxDate(long date) {
        this(date, false, false);
    }
     
    public void setIsMissingMonth(boolean isMissingMonth)
    {
        this.isMissingMonth = isMissingMonth;
    }
    
    public boolean getIsMissingMonth()
    {
        return isMissingMonth;
    }
    
    public void setIsMissingDay(boolean isMissingDay)
    {
        this.isMissingDay = isMissingDay;
    }
    
    public boolean getIsMissingDay()
    {
        return isMissingDay;
    }
    
    public static ApproxDate valueOf(String str, Calendar cal)
    {
        ApproxDate approxDate = null;
        if (str != null) {
        // Figure out if month/day specified
            int year = Integer.parseInt(str.substring(0, 4));
            int month = Integer.parseInt(str.substring(4, 6));
            int day = Integer.parseInt(str.substring(6, 8));
            boolean noMonth = true;
            boolean noDay = true;
            if (month > 0) {
                noMonth = false;
                month--;
            }
            if (day > 0) {
                noDay = false;
            } else {
                day = 1;
            }
            cal.set(year, month, day);
            approxDate = new ApproxDate(cal.getTimeInMillis(), noMonth, noDay);
        } 
        return approxDate;
    }
}

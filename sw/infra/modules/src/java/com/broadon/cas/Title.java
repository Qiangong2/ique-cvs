package com.broadon.cas;

import java.sql.Connection;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Data set wrapper for ResultSet.
 */
public class Title {
    protected static Log log = LogFactory.getLog(Title.class);
    private static TimeZone GMT = TimeZone.getTimeZone("GMT");
    private static long ATTRBITS_MASK_RVL_CTRL = 0x0004L;
    private static long ATTRBITS_MASK_CLASSIC_CTRL = 0x0008L;
    private static long ATTRBITS_MASK_NEEDS_DVD = 0x0040L;
    private static long ATTRBITS_MASK_MIN_PLAYERS = 0x0000ff00L;
    private static long ATTRBITS_MASK_MAX_PLAYERS = 0x00ff0000L;
    
    private String titleId;
    private String name;
    private String phrase;
    private String description;
    private String[] genres;
    private String platform;
    private String publisher;
    private int version;
    private int titleSize;
    private boolean isRecommended;
    private boolean isNew;
    private int popularity;
    private List ratings = new ArrayList();
    private ApproxDate releaseDate;
    private Date publishDate;
    private String locale; /* Actual locale that the description is for */
       
    private boolean supportsRvlCtrl;
    private boolean supportsClassicCtrl;
    private boolean needsDvd;
    private int minPlayers;
    private int maxPlayers;
    
    /* Contents associated with title for images, screenshots, etc */
    private Map contents = new HashMap();

    public Title() {}

    public String getPhrase() {
        return phrase;
    }
    
    public void setPhrase(String phrase) {
        this.phrase = phrase;
    }
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String[] getGenres() {
        return genres;
    }

    public void setGenre(String genre) {
        if (genre != null) {
            genres = genre.split(",");
        } else {
            genres = null;
        }
    }

    public boolean getIsNew() {
        return isNew;
    }

    public void setNew(boolean isNew) {
        this.isNew = isNew;
    }

    public boolean getIsRecommended() {
        return isRecommended;
    }

    public void setRecommended(boolean isRecommended) {
        this.isRecommended = isRecommended;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public int getPopularity() {
        return popularity;
    }

    public void setPopularity(int popularity) {
        this.popularity = popularity;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public List getRatings() {
        return ratings;
    }

    public void addRating(Rating rating) {
        ratings.add(rating);
    }

    public String getName() {
        return name;
    }

    public void setName(String title) {
        this.name = title;
    }

    public String getTitleId() {
        return titleId;
    }

    public void setTitleId(String titleId) {
        this.titleId = titleId;
    }

    public int getTitleSize() {
        return titleSize;
    }

    public void setTitleSize(int titleSize) {
        this.titleSize = titleSize;
    }

    public ApproxDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDateStr)
    {
        this.releaseDate = ApproxDate.valueOf(releaseDateStr, Calendar.getInstance(GMT));
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    public int getMinPlayers() {
        return minPlayers;
    }

    public void setMinPlayers(int minPlayers) {
        this.minPlayers = minPlayers;
    }

    public boolean getNeedsDvd() {
        return needsDvd;
    }

    public void setNeedsDvd(boolean needsDvd) {
        this.needsDvd = needsDvd;
    }

    public boolean getSupportsClassicCtrl() {
        return supportsClassicCtrl;
    }

    public void setSupportsClassicCtrl(boolean supportsClassicCtrl) {
        this.supportsClassicCtrl = supportsClassicCtrl;
    }

    public boolean getSupportsRvlCtrl() {
        return supportsRvlCtrl;
    }

    public void setSupportsRvlCtrl(boolean supportsRvlCtrl) {
        this.supportsRvlCtrl = supportsRvlCtrl;
    }
    
    public void setAttributes(long attribits)
    {
        setSupportsClassicCtrl((attribits & ATTRBITS_MASK_CLASSIC_CTRL) != 0);
        setSupportsRvlCtrl((attribits & ATTRBITS_MASK_RVL_CTRL) != 0);
        setNeedsDvd((attribits & ATTRBITS_MASK_NEEDS_DVD) != 0);
        setMinPlayers( (int) ((attribits & ATTRBITS_MASK_MIN_PLAYERS) >> 8));
        setMaxPlayers( (int) ((attribits & ATTRBITS_MASK_MAX_PLAYERS) >> 16));
    }
    
    public void addContent(String type, String name, String contentId, boolean replace)
    {
        String key = type + "." + name;
        if (replace || !contents.containsKey(key)) {
            contents.put(key, contentId.substring(16));
        }           
    }
    
    public Map getContents()
    {
        return contents;
    }

    public List getContents(String type)
    {
        Set keys = contents.keySet();
        List screenShots = new ArrayList();
        String typePrefix = type + ".";
        for (Iterator iter = keys.iterator(); iter.hasNext(); ) {
            String key = (String) iter.next();
            if (key.startsWith(typePrefix)) {
                screenShots.add(contents.get(key));
            }
        }
        return screenShots;
    }
    
    public List getImages()
    {
        return getContents("IMG");
    }
    
    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

}

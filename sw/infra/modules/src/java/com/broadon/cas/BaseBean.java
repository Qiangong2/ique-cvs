package com.broadon.cas;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Vector;
import javax.sql.DataSource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.broadon.db.DataSourceScheduler;
import com.broadon.db.OracleDataSource.OracleDataSourceProxy;
import com.broadon.exception.BroadOnException;
import com.broadon.servlet.ServletConstants;
import com.broadon.util.BackingStoreException;

/**
 * BaseBean is base class for all CAS beans.
 */
public class BaseBean implements Serializable, ServletConstants {
    protected static Log log = LogFactory.getLog(BaseBean.class);
    private static DataSource db;
    private static ContentDescriptorCache contentDescriptorCache;
    
    public static final String GENRE = "GENRE";
    public static final String AGE = "AGE";

    public static final String RATING_AGE1 = "RATING_AGE";
    public static final String RATING_AGE2 = "SECOND_AGE";
    public static final String CATEGORY = "CATEGORY";
    public static final String PUBLISHER = "PUBLISHER";
    public static final String PLATFORM = "PLATFORM";
    public static final String TITLE = "TITLE";
    public static final String HIDDLE_TITLE = "HIDDEN_TITLE";
    public static final String POPULARITY = "POPULARITY";
    public static final String RELEASE_DATE = "RELEASE_DATE";
    public static final String LICENSE_TYPE = "LICENSE_TYPE";
    public static final String ECARD_TYPE = "ECARD_TYPE";
    public static final String REFILL_POINTS = "REFILL_POINTS";
    
    private static final int PAGE_SIZE = 2;
    private static final int COUNTRY_ID = 200;
    private static final String COUNTRY_CODE = "US";
    private static final String REGION = "US";
    private static final int DEVICE_ID = 0;
    private static final String LANGUAGE_CODE = "en";
    
    private static boolean inited = false;
    private int pageSize = PAGE_SIZE;
    private String region = REGION;
    private int countryId = COUNTRY_ID;
    private String countryCode = COUNTRY_CODE;
    private String locale;
    private String languageCode = LANGUAGE_CODE;

    private Hashtable filters;
    private Vector orders;

    public BaseBean() {
        filters = new Hashtable();
        orders = new Vector();
    }

    public void refresh() {
        filters.clear();
        orders.clear();
    }

    public synchronized static void init(DataSource datasource)
            throws BroadOnException {
        if (inited) {
            return;
        }
        db = datasource;
        /* REVIEW: Set timeout of content descriptor cache to 1 minute (shouldn't change) */
        contentDescriptorCache = new ContentDescriptorCache(db, 60*1000);
        inited = true;
    }

    protected ContentDescriptor[] getContentDescriptors(String name, String locale)
        throws BackingStoreException
    {
        return (ContentDescriptor[]) 
            contentDescriptorCache.get(new ContentDescriptorCache.Key(name, locale));
    }
    
    /**
     * Creates a new read/write database connection from the connection pool.
     * 
     * @returns A new logical connection, which may be !isValid() when a
     *          connection cannot be established due to an error.
     * @throws SQLException
     *             when the connection cannot be created.
     */
    protected Connection getConnection(int priority, boolean readonly) throws SQLException {
        Connection conn = getConnection(db, priority);
        //conn.setAutoCommit(false);
        //conn.setReadOnly(readonly);
        return conn;
    }

    protected Connection getConnection() throws SQLException {
        return getConnection(DataSourceScheduler.DB_PRIORITY_MEDIUM);
    }

    protected Connection getConnection(int priority) throws SQLException
    {
        return getConnection(priority, false);
    }
    
    protected static Connection getConnection(DataSource dataSource, int priority) throws SQLException
    {
        Connection conn = dataSource instanceof OracleDataSourceProxy ?
                ((OracleDataSourceProxy) dataSource).getConnection(priority) :
                    dataSource.getConnection();
        return conn;
    }
    
    /**
     * Frees up database resources.
     */
    protected void closeResource(Connection conn, Statement stmt, ResultSet rs) {
        try {
            if (conn != null && !conn.isClosed()) {
                conn.close();
            }
            conn = null;
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
            if (rs != null) {
                rs.close();
            }
            rs = null;
        } catch (Exception e) {
            log.error(e);
        }
    }

    /**
     * Counts the number of records identified by the given SQL statement.
     * 
     * @param sql
     *            the SQL statement that counts
     * @return sql counting statement.
     */
    protected String getRecordCount(String sql) {
        int indFrom = sql.toUpperCase().indexOf("FROM");
        if (indFrom == -1) {
            return null;
        }
        String qq = "SELECT COUNT(*) " + sql.substring(indFrom);
        return qq;
    }

    /**
     * Queries all records identified by the given SQL statement.
     * 
     * @param stmt
     *            PreparedStatement
     * @return The ResultSet that represent the qualified records, or null if
     *         none found.
     */
    protected ResultSet queryAll(PreparedStatement stmt) throws SQLException {
        return query(stmt, -1, -1);
    }

    /**
     * Queries the records identified by the given SQL statement based on page
     * size and skiprows.
     * 
     * @param stmt
     *            PreparedStatement
     * @param page #
     *            of page, start from 1
     * @return The ResultSet that represent the qualified records, or null if
     *         none found.
     */
    protected ResultSet queryPage(PreparedStatement stmt, int page)
            throws SQLException {
        int pageSize = getPageSize();
        return query(stmt, (page) * pageSize, (page - 1) * pageSize);
    }

    /**
     * Queries the records identified by the given SQL statement based on
     * numrows and skiprows.
     * 
     * @param stmt
     *            PreparedStatement
     * @param numrows #
     *            of rows to include in the result -1 defaults to all
     * @param skiprows #
     *            of rows to skip in the result -1 defaults to none
     * @return The ResultSet that represent the qualified records, or null if
     *         none found.
     */
    protected ResultSet query(PreparedStatement stmt, int numrows, int skiprows)
            throws SQLException {
        ResultSet rs = null;
        if (stmt != null) {
            if (numrows >= 0) {
                stmt.setMaxRows(numrows);
            }
            rs = stmt.executeQuery();
            int i = skiprows;
            while (i-- > 0 && rs.next());
        }
        return rs;
    }

    public void addFilter(String by, String value) {
        addFilter(by, value, false);
    }

    public void addFilter(String by, String value, boolean like) {
        if (by == null) {
            return;
        }
        value = value.trim();
        if (!value.equals("")) {
            StringBuffer v = new StringBuffer();
            if (like) {
                v.append(" LIKE '%").append(value).append("%'");
            } else {
                v.append(" = '").append(value).append("'");
            }
            filters.put(by, v.toString());
        }
    }

    public final static int EQ_FLAG = 0;
    public final static int MORE_FLAG = 1;
    public final static int MORE_EQ_FLAG = 2;
    public final static int LESS_FLAG = -1;
    public final static int LESS_EQ_FLAG = -2;

    public void addFilterNull(String by, boolean isNull) {
        if (by == null) {
            return;
        }
        if (isNull) {
            filters.put(by, " IS NULL ");
        } else {
            filters.put(by, " IS NOT NULL ");
        }
    }
    
    public void addFilter(String by, String[] values, boolean isIn)
    {
        if (by == null) {
            return;
        }
        
        StringBuffer v = new StringBuffer();
        if (isIn) {
            v.append(" IN ( ");
        } else {
            v.append(" NOT IN ( ");
        }
        for (int i = 0; i < values.length; i++) {
            if (i > 0) {
                v.append(", ");
            }
            v.append("'");
            v.append(values[i]);
            v.append("'");
        }
        v.append(" )");        
        filters.put(by, v.toString());
    }
    
    public void addFilter(String by, int value) {
        addFilter(by, value, EQ_FLAG);
    }
    
    public void addFilter(String by, int value, int flag) {
        if (by == null) {
            return;
        }
        StringBuffer v = new StringBuffer();
        switch (flag) {
        case EQ_FLAG:
            v.append(" = ");
            break;
        case MORE_FLAG:
            v.append(" > ");
            break;
        case MORE_EQ_FLAG:
            v.append(" >= ");
            break;
        case LESS_FLAG:
            v.append(" < ");
            break;
        case LESS_EQ_FLAG:
            v.append(" <= ");
            break;
        }
        v.append(value);
        filters.put(by, v.toString());
    }


    protected String getFilters() {
        StringBuffer filterStr = new StringBuffer();
        for (Enumeration e = filters.keys(); e.hasMoreElements();) {
            String by = (String) e.nextElement();
            String value = (String) filters.get(by);
            filterStr.append(" AND ");
            if (by.equalsIgnoreCase(AGE)) {
                filterStr.append("(");
                filterStr.append(RATING_AGE1);
                filterStr.append(" is null OR ");                
                filterStr.append(RATING_AGE1);
                filterStr.append(value);
                filterStr.append(") AND (");
                filterStr.append(RATING_AGE2);
                filterStr.append(" is null OR ");                
                filterStr.append(RATING_AGE2);
                filterStr.append(value);
                filterStr.append(")");
            } else {
                if (by.equalsIgnoreCase(GENRE)) {
                    filterStr.append(CATEGORY);
                } else {
                    filterStr.append(by);
                }
                filterStr.append(value);
            }
        }
        return filterStr.toString();
    }

    public void addOrder(String by) {
        if (by != null && !orders.contains(by)) {
            orders.add(by);
        }
    }

    protected String getOrders() {
        StringBuffer orderStr = new StringBuffer();
        for (Enumeration e = orders.elements(); e.hasMoreElements();) {
            String by = (String) e.nextElement();
            if (orderStr.length() == 0) {
                orderStr.append(" ORDER BY ");
            } else {
                orderStr.append(", ");
            }
            if (by.equalsIgnoreCase(GENRE)) {
                orderStr.append(CATEGORY);
            } else {
                orderStr.append(by);
            }
        }
        return orderStr.toString();
    }

    public int getDeviceId() {
        return DEVICE_ID;
    }

    public String getRegion() {
        return region;
    }

    public void setRegionCode(String region) {
        this.region = region;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countyId) {
        this.countryId = countyId;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName(String countryCode, String defaultLocale)
    {
        try {
            Locale countryLocale = new Locale("", countryCode);
            Locale currentLocale = new Locale(getLanguageCode(), getCountryCode());
            String countryName = countryLocale.getDisplayCountry(currentLocale);
            if (countryName != null && countryName.length() > 0) {
                return countryName;
            } else {
                return null;
            }
        } catch (Throwable ex) {
            log.error("Error getting country name for " + countryCode + " default locale " + defaultLocale +
                    " current locale " + getLanguageCode() + "_" + getCountryCode(), ex);
            return null;
        }
    }
    
}

package com.broadon.cas;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class GameTitleListPricingsBean extends TitleListBean {
    protected static Log log = LogFactory.getLog(GameTitleListPricingsBean.class);

    public GameTitleListPricingsBean() {
        super();
    }

    private static String TITLES = 
        "SELECT item_id, item_price, item_currency, "
        + "BCCUTIL.ConvertTimeStamp(purchase_start_date), "
        + "BCCUTIL.ConvertTimeStamp(purchase_end_date), "
        + "license_type, rtype, limits, "
        + "bccutil.dec2hex(title_id, 16), "
        + "locale, title, short_phrase, description, category, platform, publisher, "
        + "approx_size, recommend_date, new_since, popularity, "
        + "release_date, "
        + "BCCUTIL.ConvertTimeStamp(publish_date), "
        + "title_version, attribits, "
        + "rating_system, rating_value, rating_age, rating_attribits, "        
        + "second_system, second_value, second_age, second_attribits ";
            
    private static String COUNTRY_FILTER =
       " WHERE country = ? AND language_code = ?";

    protected String getCatalog()
    {
        return "current_game_pricing_catalog";
    }
    
    protected String getQueryString()
    {
        StringBuffer queryStr = new StringBuffer(TITLES);
        queryStr.append(" FROM ");
        queryStr.append(getCatalog());
        queryStr.append(COUNTRY_FILTER);
        queryStr.append(getFilters());
        queryStr.append(getOrders());
        return queryStr.toString();
    }
    
    public List list() {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        titles.clear();
        try {
            conn = getConnection();
            String queryStr = getQueryString();
            log.debug(queryStr);
            log.debug("country: " + getCountryCode());
            log.debug("language_code: " + getLanguageCode());
            
            if (totalCount == -1) {
                String countStr = getRecordCount(queryStr);
                stmt = conn.prepareStatement(countStr);
                stmt.setString(1, getCountryCode());
                stmt.setString(2, getLanguageCode());
                rs = stmt.executeQuery();
                if (rs.next()) {
                    totalCount = rs.getInt(1);
                }
            }
            stmt = conn.prepareStatement(queryStr);
            stmt.setString(1, getCountryCode());
            stmt.setString(2, getLanguageCode());
            rs = queryPage(stmt, page);
            while (rs.next()) {
                int i = 1;
                Pricing pricing = new Pricing();
                pricing.setItemId(rs.getInt(i++));
                pricing.setItemPrice(rs.getBigDecimal(i++));
                pricing.setItemCurrency(rs.getString(i++));
                long purchaseStartMs = rs.getLong(i++);
                if (!rs.wasNull()) {
                    pricing.setPurchaseStartDate(new Date(purchaseStartMs));
                }
                long purchaseEndMs = rs.getLong(i++);
                if (!rs.wasNull()) {
                    pricing.setPurchaseEndDate(new Date(purchaseEndMs));
                }
                pricing.setLicenseType(rs.getString(i++));
                String rtype = rs.getString(i++);
                long limits = rs.getLong(i++);
                if (!rs.wasNull()) {
                    Limit limit = new Limit();
                    limit.setCode(rtype);
                    limit.setValue(limits);
                    pricing.addLimit(limit);
                }

                Title title = new Title();
                title.setTitleId(rs.getString(i++));
                title.setLocale(rs.getString(i++));
                title.setName(rs.getString(i++));
                title.setPhrase(rs.getString(i++));
                title.setDescription(rs.getString(i++));
                title.setGenre(rs.getString(i++));
                title.setPlatform(rs.getString(i++));
                title.setPublisher(rs.getString(i++));
                title.setTitleSize(rs.getInt(i++));
                Date recommendDate = rs.getDate(i++);
                title.setRecommended(!rs.wasNull());
                Date newSince = rs.getDate(i++);
                title.setNew(!rs.wasNull());
                title.setPopularity(rs.getInt(i++));
                title.setReleaseDate(rs.getString(i++));
                long publishDateMs = rs.getLong(i++);
                if (!rs.wasNull()) {
                    title.setPublishDate(new Date(publishDateMs));
                }
                title.setVersion(rs.getInt(i++));
                title.setAttributes(rs.getLong(i++));
                Rating r1 = new Rating();
                r1.setSystem(rs.getString(i++));
                r1.setValue(rs.getString(i++));
                r1.setAge(rs.getInt(i++));
                r1.setAttributes(
                    getContentDescriptors(r1.getSystem(), title.getLocale()),
                    rs.getLong(i++));                
                title.addRating(r1);
                String secondSystem = rs.getString(i++);
                if (!rs.wasNull()) {
                    Rating r2 = new Rating();
                    r2.setSystem(secondSystem);
                    r2.setValue(rs.getString(i++));
                    r2.setAge(rs.getInt(i++));
                    r2.setAttributes(
                        getContentDescriptors(r2.getSystem(), title.getLocale()),
                        rs.getLong(i++));
                    title.addRating(r2);
                }
                
                TitlePricing titlePricing = new TitlePricing();
                titlePricing.setPricing(pricing);
                titlePricing.setTitle(title);
                titles.add(titlePricing);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        } finally {
            closeResource(conn, stmt, rs);
        }
        return titles;
    }

}

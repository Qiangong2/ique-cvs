package com.broadon.cas;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class GameTitleListDetailsBean extends TitleListBean {
    protected static Log log = LogFactory.getLog(GameTitleListDetailsBean.class);

    public GameTitleListDetailsBean() {
        super();
    }

    private static String TITLES = 
        "SELECT " 
        + "bccutil.dec2hex(title_id, 16), "
        + " item_id, item_price, item_currency, "
        + "BCCUTIL.ConvertTimeStamp(purchase_start_date), "
        + "BCCUTIL.ConvertTimeStamp(purchase_end_date), "
        + "license_type, rtype, limits, "
        + "locale, title, short_phrase, description, category, platform, publisher, "
        + "approx_size, recommend_date, new_since, popularity, "
        + "release_date, "
        + "BCCUTIL.ConvertTimeStamp(publish_date), "
        + "title_version, attribits, "
        + "rating_system, rating_value, rating_age, rating_attribits, "        
        + "second_system, second_value, second_age, second_attribits " 
        + "FROM current_game_pricing_catalog "
        + "WHERE country = ? AND language_code = ?";

    private static String COUNT_TITLES =
        "SELECT count(unique bccutil.dec2hex(title_id, 16)) "
        + "FROM current_game_pricing_catalog "
        + "WHERE country = ? AND language_code = ?";

    public List list() {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        titles.clear();
        // Make sure title id is used as an order 
        addOrder("title_id");
        try {
            conn = getConnection();
            StringBuffer queryStr = new StringBuffer(TITLES);
            queryStr.append(getFilters());
            queryStr.append(getOrders());
            log.debug(queryStr.toString());
            log.debug("country: " + getCountryCode());
            log.debug("language_code: " + getLanguageCode());
            
            if (totalCount == -1) {
                StringBuffer countStrBuf = new StringBuffer(COUNT_TITLES);
                countStrBuf.append(getFilters());
                String countStr = countStrBuf.toString();
                stmt = conn.prepareStatement(countStr);
                stmt.setString(1, getCountryCode());
                stmt.setString(2, getLanguageCode());
                rs = stmt.executeQuery();
                if (rs.next()) {
                    totalCount = rs.getInt(1);
                }
            }
            stmt = conn.prepareStatement(queryStr.toString());
            stmt.setString(1, getCountryCode());
            stmt.setString(2, getLanguageCode());
            /* Do our own pagination */
            rs = queryAll(stmt);
            int nTitles = 0;
            String prevTitleId = null;
            boolean newTitle = false;
            int pageSize = getPageSize();
            int skipTitles = (page - 1) * pageSize;
            int maxTitles = (page) * pageSize;
            TitleDetailBean titleDetails = null;
            while (rs.next()) {
                int i = 1;
                String titleId = rs.getString(i++);
                newTitle = (prevTitleId == null || !prevTitleId.equals(titleId));
                if (newTitle) {
                    nTitles++;
                    prevTitleId = titleId;
                } 
                /* Decide to include title or not */
                if (page > 0) {
                    if (nTitles <= skipTitles) {
                        continue;                        
                    }
                    if (nTitles > maxTitles) {
                        break;
                    }
                }
                Pricing pricing = new Pricing();
                pricing.setItemId(rs.getInt(i++));
                pricing.setItemPrice(rs.getBigDecimal(i++));
                pricing.setItemCurrency(rs.getString(i++));
                long purchaseStartMs = rs.getLong(i++);
                if (!rs.wasNull()) {
                    pricing.setPurchaseStartDate(new Date(purchaseStartMs));
                }
                long purchaseEndMs = rs.getLong(i++);
                if (!rs.wasNull()) {
                    pricing.setPurchaseEndDate(new Date(purchaseEndMs));
                }
                pricing.setLicenseType(rs.getString(i++));
                String rtype = rs.getString(i++);
                long limits = rs.getLong(i++);
                if (!rs.wasNull()) {
                    Limit limit = new Limit();
                    limit.setCode(rtype);
                    limit.setValue(limits);
                    pricing.addLimit(limit);
                }

                if (newTitle) {
                    Title title = new Title();
                    title.setTitleId(titleId);
                    title.setLocale(rs.getString(i++));
                    title.setName(rs.getString(i++));
                    title.setPhrase(rs.getString(i++));
                    title.setDescription(rs.getString(i++));
                    title.setGenre(rs.getString(i++));
                    title.setPlatform(rs.getString(i++));
                    title.setPublisher(rs.getString(i++));
                    title.setTitleSize(rs.getInt(i++));
                    Date recommendDate = rs.getDate(i++);
                    title.setRecommended(!rs.wasNull());
                    Date newSince = rs.getDate(i++);
                    title.setNew(!rs.wasNull());
                    title.setPopularity(rs.getInt(i++));
                    title.setReleaseDate(rs.getString(i++));
                    long publishDateMs = rs.getLong(i++);
                    if (!rs.wasNull()) {
                        title.setPublishDate(new Date(publishDateMs));
                    }
                    title.setVersion(rs.getInt(i++));
                    title.setAttributes(rs.getLong(i++));
                    Rating r1 = new Rating();
                    r1.setSystem(rs.getString(i++));
                    r1.setValue(rs.getString(i++));
                    r1.setAge(rs.getInt(i++));
                    r1.setAttributes(
                        getContentDescriptors(r1.getSystem(), title.getLocale()),
                        rs.getLong(i++));
                    title.addRating(r1);
                    String secondSystem = rs.getString(i++);
                    if (!rs.wasNull()) {
                        Rating r2 = new Rating();
                        r2.setSystem(secondSystem);
                        r2.setValue(rs.getString(i++));
                        r2.setAge(rs.getInt(i++));
                        r2.setAttributes(
                            getContentDescriptors(r2.getSystem(), title.getLocale()),
                            rs.getLong(i++));
                        title.addRating(r2);
                    }
                    titleDetails = new TitleDetailBean();
                    titleDetails.setTitle(title);
                    titles.add(titleDetails);
                }
                
                titleDetails.getPricings().add(pricing);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        } finally {
            closeResource(conn, stmt, rs);
        }
        
        for (int i = 0; i < titles.size(); i++) {
            TitleDetailBean titleDetails = (TitleDetailBean) titles.get(i);
            Title title = titleDetails.getTitle();
            try {
                TitleContents.getContents(this, title);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("Error getting title contents for " + title.getTitleId() + " in country " + getCountryCode() 
                    + " for language " + getLanguageCode(), e);
                
            }
        }
        return titles;
    }

}

package com.broadon.cas;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class TitleContents {
    private static String TITLE_CONTENTS =
        "SELECT CONTENT_TYPE, CONTENT_NAME, bccutil.dec2hex(CONTENT_ID, 24) from " +
        " CONTENT_OBJECTS WHERE CONTENT_ID >= ? AND CONTENT_ID <= ?";
    
    public static void getContents(BaseBean bean, Title title)
        throws Exception 
    {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            conn = bean.getConnection();
            stmt = conn.prepareStatement(TITLE_CONTENTS);
            BigDecimal minContentId = new BigDecimal(new BigInteger(title.getTitleId() + "FFFD0000", 16));
            BigDecimal maxContentId = new BigDecimal(new BigInteger(title.getTitleId() + "FFFDFFFF", 16));
            
            stmt.setBigDecimal(1, minContentId);
            stmt.setBigDecimal(2, maxContentId);
            rs = bean.queryAll(stmt);
            while (rs.next()) {
                int i = 1;
                String contentType = rs.getString(i++); 
                String contentName = rs.getString(i++); 
                String contentId = rs.getString(i++);
                if (contentName != null && contentType != null) {
                    int sepIndex = contentName.indexOf("_");
                    if (sepIndex >= 0) {
                        String loc = contentName.substring(sepIndex+1);
                        if (title.getLocale() != null && title.getLocale().equalsIgnoreCase(loc)) {
                            title.addContent(contentType, contentName.substring(0, sepIndex), contentId, true);
                        } else if (bean.getLocale() != null && bean.getLocale().equalsIgnoreCase(loc)) {
                            title.addContent(contentType, contentName.substring(0, sepIndex), contentId, false);
                        }
                    } else {
                        title.addContent(contentType, contentName, contentId, false);
                    }
                    
                }
            }
        } finally {
            bean.closeResource(conn, stmt, rs);
        }
        
    }
    
}

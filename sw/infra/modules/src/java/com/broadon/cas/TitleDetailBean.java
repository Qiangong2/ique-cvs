package com.broadon.cas;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class TitleDetailBean extends BaseBean {
    private static String LICENSE_PERMANENT = "PERMANENT"; 
    private static String LICENSE_TRIAL = "TRIAL"; 
    private static String LICENSE_RENTAL = "RENTAL"; 
    
    protected static Log log = LogFactory.getLog(TitleDetailBean.class);
    private String titleId;
    private Title title = null;
    private List pricings;
    
    /* If a particular item is selected (null if no item selected) */
    private Integer itemId;
    private Pricing itemPricing;

    private static String TITLE_DETAIL = 
        "SELECT " 
            + "locale, title, short_phrase, description,"
            + "category, platform, "
            + "publisher, approx_size, recommend_date, "
            + "new_since, popularity, "
            + "release_date, "
            + "BCCUTIL.ConvertTimeStamp(publish_date), "
            + "title_version, attribits, "
            + "rating_system, rating_value, rating_age, rating_attribits, "
            + "second_system, second_value, second_age, second_attribits "
            + "FROM CONTENT_TITLE_COUNTRY_RELEASES "
            + "WHERE country = ? AND title_id = bccutil.hex2dec(?)"
            + "AND language_code = ?";
    
    private static String TITLE_PRICING_DETAIL = 
        "SELECT " 
            + "item_id, item_price, item_currency, "
            + "BCCUTIL.ConvertTimeStamp(purchase_start_date), "
            + "BCCUTIL.ConvertTimeStamp(purchase_end_date), "
            + "license_type, rtype, limits, "
            + "locale, title, short_phrase, description, "
            + "category, platform, "
            + "publisher, approx_size, recommend_date, "
            + "new_since, popularity, "
            + "release_date, "
            + "BCCUTIL.ConvertTimeStamp(publish_date), "
            + "title_version, attribits, "
            + "rating_system, rating_value, rating_age, rating_attribits, "
            + "second_system, second_value, second_age, second_attribits "
            + "FROM current_game_pricing_catalog "
            + "WHERE country = ? AND title_id = bccutil.hex2dec(?)"
            + "AND language_code = ?";

    public TitleDetailBean() 
    {
        pricings = new ArrayList();
    }

    /* Try to lookup title that has been released by does not have a current pricing */
    public Title lookupUnpricedTitle()
    {
        title = null;
        pricings.clear();
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            StringBuffer queryStr = new StringBuffer(TITLE_DETAIL);
            log.debug(queryStr.toString());
            log.debug("country: " + getCountryCode());
            log.debug("language_code: " + getLanguageCode());
            stmt = conn.prepareStatement(queryStr.toString());
            stmt.setString(1, getCountryCode());
            stmt.setString(2, titleId);
            stmt.setString(3, getLanguageCode());
            rs = queryAll(stmt);
            if (rs.next()) {
                int i = 1;
                title = new Title();
                title.setTitleId(titleId);
                title.setLocale(rs.getString(i++));
                title.setName(rs.getString(i++));
                title.setPhrase(rs.getString(i++));
                title.setDescription(rs.getString(i++));
                title.setGenre(rs.getString(i++));
                title.setPlatform(rs.getString(i++));
                title.setPublisher(rs.getString(i++));
                title.setTitleSize(rs.getInt(i++));
                Date recommendDate = rs.getDate(i++);
                title.setRecommended(!rs.wasNull());
                Date newSince = rs.getDate(i++);
                title.setNew(!rs.wasNull());
                title.setPopularity(rs.getInt(i++));
                title.setReleaseDate(rs.getString(i++));
                long publishDateMs = rs.getLong(i++);
                if (!rs.wasNull()) {
                    title.setPublishDate(new Date(publishDateMs));
                }
                title.setVersion(rs.getInt(i++));
                title.setAttributes(rs.getLong(i++));
                Rating r1 = new Rating();
                r1.setSystem(rs.getString(i++));    
                r1.setValue(rs.getString(i++));
                r1.setAge(rs.getInt(i++));
                r1.setAttributes(
                    getContentDescriptors(r1.getSystem(), title.getLocale()),
                    rs.getLong(i++));
                title.addRating(r1);
                String secondSystem = rs.getString(i++);
                if (!rs.wasNull()) {
                    Rating r2 = new Rating();
                    r2.setSystem(secondSystem);
                    r2.setValue(rs.getString(i++));
                    r2.setAge(rs.getInt(i++));  
                    r2.setAttributes(
                        getContentDescriptors(r2.getSystem(), title.getLocale()),                            
                        rs.getLong(i++));
                    title.addRating(r2);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error getting title details for " + titleId + " in country " + getCountryCode() 
                    + " for language " + getLanguageCode(), e);
        } finally {
            closeResource(conn, stmt, rs);
        }
        return title;
    }

    /* Try to get current prices for the title */
    public Title lookupPricedTitle()
    {
        title = null;
        pricings.clear();
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            StringBuffer queryStr = new StringBuffer(TITLE_PRICING_DETAIL);
            log.debug(queryStr.toString());
            log.debug("country: " + getCountryCode());
            log.debug("language_code: " + getLanguageCode());
            stmt = conn.prepareStatement(queryStr.toString());
            stmt.setString(1, getCountryCode());
            stmt.setString(2, titleId);
            stmt.setString(3, getLanguageCode());
            rs = queryAll(stmt);
            while (rs.next()) {
                int i = 1;
                Pricing pricing = new Pricing();
                pricing.setItemId(rs.getInt(i++));
                pricing.setItemPrice(rs.getBigDecimal(i++));
                pricing.setItemCurrency(rs.getString(i++));
                long purchaseStartMs = rs.getLong(i++);
                if (!rs.wasNull()) {
                    pricing.setPurchaseStartDate(new Date(purchaseStartMs));
                }
                long purchaseEndMs = rs.getLong(i++);
                if (!rs.wasNull()) {
                    pricing.setPurchaseEndDate(new Date(purchaseEndMs));
                }
                pricing.setLicenseType(rs.getString(i++));
                String rtype = rs.getString(i++);
                long limits = rs.getLong(i++);
                if (!rs.wasNull()) {
                    Limit limit = new Limit();
                    limit.setCode(rtype);
                    limit.setValue(limits);
                    pricing.addLimit(limit);
                }
                pricings.add(pricing);
                
                if (title == null)
                { 
                    title = new Title();
                    title.setTitleId(titleId);
                    title.setLocale(rs.getString(i++));
                    title.setName(rs.getString(i++));
                    title.setPhrase(rs.getString(i++));
                    title.setDescription(rs.getString(i++));
                    title.setGenre(rs.getString(i++));
                    title.setPlatform(rs.getString(i++));
                    title.setPublisher(rs.getString(i++));
                    title.setTitleSize(rs.getInt(i++));
                    Date recommendDate = rs.getDate(i++);
                    title.setRecommended(!rs.wasNull());
                    Date newSince = rs.getDate(i++);
                    title.setNew(!rs.wasNull());
                    title.setPopularity(rs.getInt(i++));
                    title.setReleaseDate(rs.getString(i++));
                    long publishDateMs = rs.getLong(i++);
                    if (!rs.wasNull()) {
                        title.setPublishDate(new Date(publishDateMs));
                    }
                    title.setVersion(rs.getInt(i++));
                    title.setAttributes(rs.getLong(i++));
                    Rating r1 = new Rating();
                    r1.setSystem(rs.getString(i++));    
                    r1.setValue(rs.getString(i++));
                    r1.setAge(rs.getInt(i++));
                    r1.setAttributes(
                        getContentDescriptors(r1.getSystem(), title.getLocale()),
                        rs.getLong(i++));
                    title.addRating(r1);
                    String secondSystem = rs.getString(i++);
                    if (!rs.wasNull()) {
                        Rating r2 = new Rating();
                        r2.setSystem(secondSystem);
                        r2.setValue(rs.getString(i++));
                        r2.setAge(rs.getInt(i++));  
                        r2.setAttributes(
                            getContentDescriptors(r2.getSystem(), title.getLocale()),
                            rs.getLong(i++));
                        title.addRating(r2);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error getting title details for " + titleId + " in country " + getCountryCode() 
                    + " for language " + getLanguageCode(), e);
        } finally {
            closeResource(conn, stmt, rs);
        }
        return title;
    }
        
    public Title list() {
        // Get details about title - including pricing
        lookupPricedTitle();
        if (title == null) {
            // Can't find any pricings for title - just get details
            lookupUnpricedTitle();
        }
        if (title != null) {
            try {
                TitleContents.getContents(this, title);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("Error getting title contents for " + title.getTitleId() + " in country " + getCountryCode() 
                    + " for language " + getLanguageCode(), e);
                
            }
        }
        itemPricing = getPricing(itemId);
        return title;
    }

    public Title getTitle() {
        return title;
    }
    
    public void setTitle(Title title) {
        this.title = title;
        this.titleId = (title != null)? title.getTitleId(): null;
        this.pricings.clear();
    }
    
    public List getPricings() {
        return pricings;
    }
        
    public String getTitleId() {
        return titleId;
    }

    public void setTitleId(String titleId) {
        this.titleId = titleId;
    }
    
    public Pricing getPricing()
    {
        return itemPricing;
    } 
    
    /* Return pricing that matches the given item id */
    public Pricing getPricing(int itemId) {
        for (int i = 0; i < pricings.size(); i++) {
            Pricing pricing = (Pricing) pricings.get(i);
            if (pricing.getItemId() == itemId) {
                return pricing;
            }
        }
        return null;
    }
    
    /* Return pricing that matches the given item id */
    public Pricing getPricing(Integer itemId) {
        if (itemId == null) {
            return null;
        } else {
            return getPricing(itemId.intValue());
        }
    }
    
    /* Return pricing that matches the given license type */
    public Pricing getPricingForLicense(String licenseType) {
        if (licenseType == null) {
            return null;
        }
        for (int i = 0; i < pricings.size(); i++) {
            Pricing pricing = (Pricing) pricings.get(i);
            if (licenseType.equals(pricing.getLicenseType())) {
                return pricing;
            }
        }
        return null;
    }
    
    public Pricing getTrialLicensePricing()
    {
        return getPricingForLicense(LICENSE_TRIAL);
    }
    
    public Pricing getPermanentLicensePricing()
    {
        return getPricingForLicense(LICENSE_PERMANENT);
    }
    
    public Pricing getRentalLicensePricing()
    {
        return getPricingForLicense(LICENSE_RENTAL);
    }
    
    public Integer getItemId()
    {
        return itemId;
    }
    
    public void setItemId(Integer itemId)
    {
        this.itemId = itemId;
        this.itemPricing = getPricing(itemId);
    }

}

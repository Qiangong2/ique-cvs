package com.broadon.cas;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class Pricing {
    private Date purchaseStartDate;
    private Date purchaseEndDate;
    private int itemId;
    private BigDecimal itemPrice;
    private String itemCurrency;
    private String licenseType;
    private List limits;
    
    public Pricing() {} 

    public Date getPurchaseEndDate() {
        return purchaseEndDate;
    }

    public void setPurchaseEndDate(Date purchaseEndDate) {
        this.purchaseEndDate = purchaseEndDate;
    }

    public Date getPurchaseStartDate() {
        return purchaseStartDate;
    }

    public void setPurchaseStartDate(Date purchaseStartDate) {
        this.purchaseStartDate = purchaseStartDate;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }
    
    public BigDecimal getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(BigDecimal itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getItemCurrency() {
        return itemCurrency;
    }

    public void setItemCurrency(String currency) {
        this.itemCurrency = currency;
    }
    
    public String getLicenseType() {
        return licenseType;
    }

    public void setLicenseType(String licenseType) {
        this.licenseType = licenseType;
    }
    
    public List getLimits() {
        return limits;
    }

    public void setLimits(List limits) {
        this.limits = limits;
    }
    
    public void addLimit(Limit limit)
    {
        if (limits == null) {
            limits = new ArrayList();
        }
        limits.add(limit);
    }
}

package com.broadon.cas;

import java.util.ArrayList;
import java.util.List;

public class Rating {
    private String system;
    private String value;
    private int age;
    private List contentDescs;
    
    public Rating() {
        contentDescs = new ArrayList();
    }
    
    public int getAge() {
        return age;
    }
    
    public void setAge(int age) {
        this.age = age;
    }
    
    public String getSystem() {
        return system;
    }
    
    public void setSystem(String system) {
        this.system = system;
    }
    
    public String getValue() {
        return value;
    }
    
    public void setValue(String value) {
        this.value = value;
    }
    
    public void setAttributes(ContentDescriptor[] cdList, long attribits)
    {
        /* Converts attrbits into rating content descriptors */
        long bit = 0x1L;
        for (int i = 0; i < 64; i++) {
            if ((attribits & bit) != 0) {
                ContentDescriptor cd = 
                    (cdList != null && i < cdList.length)? 
                            cdList[i]: new ContentDescriptor(i, null);
                contentDescs.add(cd);
            }
            bit = bit << 1;
        }
    }
    
    public List getContentDescriptors()
    {
        return contentDescs;
    }
}

package com.broadon.syslog;

import com.broadon.exception.BroadOnException;

/**
 * The <code>SyslogException</code> class is the top level of syslog related
 * exception hierarchy.
 *
 * @version	$Revision: 1.1 $
 */
public class SyslogException
    extends BroadOnException
{
    /**
     * Constructs a SyslogException instance.
     */
    SyslogException()
    {
        super();
    }

    /**
     * Constructs a SyslogException instance.
     *
     * @param	message			the exception message
     */
    SyslogException(String message)
    {
        super(message);
    }

    /**
     * Constructs a SyslogException instance.
     *
     * @param	message			the exception message
     * @param	throwable		the nested exception
     */
    public SyslogException(String message, Throwable throwable)
    {
	super(message, throwable);
    }
}

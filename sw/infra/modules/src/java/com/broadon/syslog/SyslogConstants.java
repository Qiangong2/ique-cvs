package com.broadon.syslog;

import java.text.ParseException;
import java.util.HashMap;

/**
 * The <code>SyslogConstants</code> class converts from the UNIX syslog
 * include file, /usr/include/sys/syslog.h.
 *
 * @version	$Revision: 1.2 $
 */
public class
SyslogConstants
{
    /*
     * Syslog priority level.
     */

    /**
     * System is unusable.
     */
    public static final int	LOG_EMERG	= 0;

    /**
     * Action must be taken immediately.
     */
    public static final int	LOG_ALERT	= 1;

    /**
     * Critical conditions.
     */
    public static final int	LOG_CRIT	= 2;

    /**
     * Error conditions.
     */
    public static final int	LOG_ERR		= 3;

    /**
     * Warning conditions.
     */
    public static final int	LOG_WARNING	= 4;

    /**
     * Normal but significant condition.
     */
    public static final int	LOG_NOTICE	= 5;

    /**
     * Informational.
     */
    public static final int	LOG_INFO	= 6;

    /**
     * Debug-level messages.
     */
    public static final int	LOG_DEBUG	= 7;

    /**
     * '*' in config, all priority levels.
     */
    public static final int	LOG_ALL		= 8;

    /*
     * Syslog facility.
     */

    /**
     * Kernel messages.
     */
    public static final int	LOG_KERN	= 0;

    /**
     * Random user-level messages.
     */
    public static final int	LOG_USER	= 1;

    /**
     * Mail system.
     */
    public static final int	LOG_MAIL	= 2;

    /**
     * System daemons.
     */
    public static final int	LOG_DAEMON	= 3;

    /**
     * Security/Authorization messages.
     */
    public static final int	LOG_AUTH	= 4;

    /**
     * Messages generated internally by syslogd.
     */
    public static final int	LOG_SYSLOG	= 5;

    /**
     * Line printer subsystem.
     */
    public static final int	LOG_LPR		= 6;

    /**
     * Network news subsystem.
     */
    public static final int	LOG_NEWS	= 7;

    /**
     * UUCP subsystem.
     */
    public static final int	LOG_UUCP	= 8;

    /**
     * Clock daemon.
     */
    public static final int	LOG_CRON	= 9;

    /**
     * Security/Authorization messages (private).
     */
    public static final int	LOG_AUTHPRIV	= 10;

    /**
     * FTP daemon.
     */
    public static final int	LOG_FTP		= 11;

    /*
     * Other codes through 15 reserved for system use.
     */

    /**
     * Reserved for local use.
     */
    public static final int	LOG_LOCAL0	= 16;

    /**
     * Reserved for local use.
     */
    public static final int	LOG_LOCAL1	= 17;

    /**
     * Reserved for local use.
     */
    public static final int	LOG_LOCAL2	= 18;

    /**
     * Reserved for local use.
     */
    public static final int	LOG_LOCAL3	= 19;

    /**
     * Reserved for local use.
     */
    public static final int	LOG_LOCAL4	= 20;

    /**
     * Reserved for local use.
     */
    public static final int	LOG_LOCAL5	= 21;

    /**
     * Reserved for local use.
     */
    public static final int	LOG_LOCAL6	= 22;

    /**
     * Reserved for local use.
     */
    public static final int	LOG_LOCAL7	= 23;

    /**
     * Current number of facilities.
     */
    public static final int	LOG_NFACILITIES	= 24;

    /**
     * Mask to extract priority part (internal).
     */
    public static final int	LOG_PRIMASK	= 0x07;

    /**
     * Mask to extract facility part.
     */
    public static final int	LOG_FACMASK	= 0x03f8;

    /**
     * The "no priority" priority.
     */
    public static final int	INTERNAL_NOPRI	= 0x10;

    /**
     * Log the pid with each message.
     */
    public static final int	LOG_PID		= 0x01;

    /**
     * Log on the console if errors in sending.
     */
    public static final int	LOG_CONS	= 0x02;

    /**
     * Delay open until first syslog() (default).
     */
    public static final int	LOG_ODELAY	= 0x04;

    /**
     * Don't delay open.
     */
    public static final int	LOG_NDELAY	= 0x08;

    /**
     * Don't wait for console forks: DEPRECATED.
     */
    public static final int	LOG_NOWAIT	= 0x10;

    /**
     * Log to stderr as well.
     */
    public static final int	LOG_PERROR	= 0x20;

    /**
     * Log the open.
     */
    public static final int	LOG_START	= 0x40;

    /**
     * The port that syslogd listens.
     */
    public static final int	DEFAULT_PORT	= 514;

    private static HashMap	facilities;
    private static HashMap	priorities;

    static
    {
	facilities = new HashMap(30);
	facilities.put("KERN",		new Integer(LOG_KERN));
	facilities.put("KERNEL",	new Integer(LOG_KERN));
	facilities.put("USER",		new Integer(LOG_USER));
	facilities.put("MAIL",		new Integer(LOG_MAIL));
	facilities.put("DAEMON",	new Integer(LOG_DAEMON));
	facilities.put("AUTH",		new Integer(LOG_AUTH));
	facilities.put("SYSLOG",	new Integer(LOG_SYSLOG));
	facilities.put("LPR",		new Integer(LOG_LPR));
	facilities.put("NEWS",		new Integer(LOG_NEWS));
	facilities.put("UUCP",		new Integer(LOG_UUCP));
	facilities.put("CRON",		new Integer(LOG_CRON));
	facilities.put("AUTHPRIV",	new Integer(LOG_AUTHPRIV));
	facilities.put("FTP",		new Integer(LOG_FTP));
	facilities.put("LOCAL0",	new Integer(LOG_LOCAL0));
	facilities.put("LOCAL1",	new Integer(LOG_LOCAL1));
	facilities.put("LOCAL2",	new Integer(LOG_LOCAL2));
	facilities.put("LOCAL3",	new Integer(LOG_LOCAL3));
	facilities.put("LOCAL4",	new Integer(LOG_LOCAL4));
	facilities.put("LOCAL5",	new Integer(LOG_LOCAL5));
	facilities.put("LOCAL6",	new Integer(LOG_LOCAL6));
	facilities.put("LOCAL7",	new Integer(LOG_LOCAL7));

	priorities = new HashMap(30);
	priorities.put("EMERG",		new Integer(LOG_EMERG));
	priorities.put("EMERGENCY",	new Integer(LOG_EMERG));
	priorities.put("LOG_EMERG",	new Integer(LOG_EMERG));
	priorities.put("ALERT",		new Integer(LOG_ALERT));
	priorities.put("LOG_ALERT",	new Integer(LOG_ALERT));
	priorities.put("CRIT",		new Integer(LOG_CRIT));
	priorities.put("CRITICAL",	new Integer(LOG_CRIT));
	priorities.put("LOG_CRIT",	new Integer(LOG_CRIT));
	priorities.put("ERR",		new Integer(LOG_ERR));
	priorities.put("ERROR",		new Integer(LOG_ERR));
	priorities.put("LOG_ERR",	new Integer(LOG_ERR));
	priorities.put("WARNING",	new Integer(LOG_WARNING));
	priorities.put("LOG_WARNING",	new Integer(LOG_WARNING));
	priorities.put("NOTICE",	new Integer(LOG_NOTICE));
	priorities.put("LOG_NOTICE",	new Integer(LOG_NOTICE));
	priorities.put("INFO",		new Integer(LOG_INFO));
	priorities.put("LOG_INFO",	new Integer(LOG_INFO));
	priorities.put("DEBUG",		new Integer(LOG_DEBUG));
	priorities.put("LOG_DEBUG",	new Integer(LOG_DEBUG));
    }

    /**
     * Extracts the facility code from the combined code.
     *
     * @param	code			the combined code
     *
     * @return	The facility code.
     */
    public static int extractFacility(int code)
    {
	return (code & LOG_FACMASK) >> 3;
    }

    /**
     * Extracts the priority level from the combined code.
     *
     * @param	code			the combined code
     *
     * @return	The priority level.
     */
    public static int extractPriority(int code)
    {
	return code & LOG_PRIMASK;
    }

    /**
     * Groups the facility code and the priority level into a combined code.
     *
     * @param	facility		the facility code
     * @param	priority		the priority level
     *
     * @return	The combined code.
     */
    public static int computeCode(int facility, int priority)
    {
	return (facility << 3) | priority;
    }

    /**
     * Gets the facility name of the given facility code.
     *
     * @param	facility		the facility code
     *
     * @return	The facility name.
     */
    public static String getFacilityName(int facility)
    {
	switch (facility)
	{
	case LOG_KERN:
	    return "kernel";

	case LOG_USER:
	    return "user";

	case LOG_MAIL:
	    return "mail";

	case LOG_DAEMON:
	    return "daemon";

	case LOG_AUTH:
	    return "auth";

	case LOG_SYSLOG:
	    return "syslog";

	case LOG_LPR:
	    return "lpr";

	case LOG_NEWS:
	    return "news";

	case LOG_UUCP:
	    return "uucp";

	case LOG_CRON:
	    return "cron";

	case LOG_AUTHPRIV:
	    return "authpriv";

	case LOG_FTP:
	    return "ftp";

	case LOG_LOCAL0:
	    return "local0";

	case LOG_LOCAL1:
	    return "local1";

	case LOG_LOCAL2:
	    return "local2";

	case LOG_LOCAL3:
	    return "local3";

	case LOG_LOCAL4:
	    return "local4";

	case LOG_LOCAL5:
	    return "local5";

	case LOG_LOCAL6:
	    return "local6";

	case LOG_LOCAL7:
	    return "local7";

	default:
	    return "unknown facility='" + facility + "'";
	}
    }

    /**
     * Gets the priority level name of the given priority level.
     *
     * @param	priority		the priority level
     *
     * @return	The priority level name.
     */
    public static String getPriorityName(int priority)
    {
	switch (priority)
	{
	case LOG_EMERG:
	    return "panic";

	case LOG_ALERT:
	    return "alert";

	case LOG_CRIT:
	    return "critical";

	case LOG_ERR:
	    return "error";

	case LOG_WARNING:
	    return "warning";

	case LOG_NOTICE:
	    return "notice";

	case LOG_INFO:
	    return "info";

	case LOG_DEBUG:
	    return "debug";

	default:
	    return "unknown priority level='" + priority + "'";
	}
    }

    /**
     * Gets the priority level from the given priority level name.
     *
     * @param	priority		the priority level name
     *
     * @return	The priority level.
     */
    public static int getPriority(String priority)
	throws ParseException
    {
	Integer	result = (Integer)priorities.get(priority.toUpperCase());

	if (result == null)
	{
	    throw new ParseException("unknown priority '" + priority + "'", 0);
	}

	return result.intValue();
    }

    /**
     * Gets the facility code from the given facility name.
     *
     * @param	facility		the facility name
     *
     * @return	The facility code.
     */
    public static int getFacility(String facility)
	throws ParseException
    {
	Integer	result = (Integer)facilities.get(facility.toUpperCase());

	if (result == null)
	{
	    throw new ParseException("unknown facility '" + facility + "'", 0);
	}

	return result.intValue();
    }
}

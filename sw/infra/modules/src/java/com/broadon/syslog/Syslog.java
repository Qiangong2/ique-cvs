package com.broadon.syslog;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

/**
 * The <code>Syslog</code> class allows messages to be logged to syslog
 * of the local UNIX host.
 * <br>
 * To use Syslog, simply create an instance, and use the log() method to
 * log your message. The SyslogConstants class provides all the expected
 * syslog constants. For example, LOG_ERR is SyslogConstants.LOG_ERR.
 * <br>
 * The syslogd must have the "-r" option turned on for this to work.
 * <br>
 * The original version of this class was written by Tim Endres.
 *
 * @version	$Revision: 1.2 $
 */
public class Syslog
{
    private String		logName;
    private int			port;
    private int			flags;
    private boolean		includeDate;
    private InetAddress		boundAddress;
    private DatagramSocket	socket;
    private SimpleDateFormat	date1Format;
    private SimpleDateFormat	date2Format;

    /**
     * Creates a Syslog object instance, targeted for the UNIX local
     * host on the default syslog port.
     * The only flags recognized are 'LOG_PERROR', which will log the
     * message to Java's 'System.err'.
     *
     * @param	name			the identity (e.g., program name)
     * @param	flags			the option flags
     */
    public Syslog(String name, int flags)
	throws SyslogException
    {
	this.logName = name;
	this.port = SyslogConstants.DEFAULT_PORT;
	this.flags = flags;

	try
	{
	    this.boundAddress = InetAddress.getLocalHost();
	}
	catch (UnknownHostException ex)
	{
	    String	message = "error locating local host '" +
				  "': " + ex.getMessage();

	    throw new SyslogException(message, ex);
	}

	initialize();
    }

    /**
     * Initializes the socket, and optionally, the date format.
     */
    private void initialize()
	throws SyslogException
    {
	try
	{
	    this.socket = new DatagramSocket();
	}
	catch (SocketException ex)
	{
	    String message = "error creating syslog udp socket: " +
			     ex.getMessage();

	    throw new SyslogException(message, ex);
	}
	/*
	 * This determines if the timestamp is added to the message
	 * in this, the client, or if it is left off for the server
	 * to fill in. Adding it in the client is the "standard" way,
	 * but most servers support the "old style" (no timestamp)
	 * messages as well. I leave this choice, since some servers
	 * may not work without it, and because some JDK's may not
	 * support SimpleDateFormat or TimeZone correctly.
	 */
	this.includeDate = true;

	if (this.includeDate)
	{
	    TimeZone	timeZone = TimeZone.getDefault();

	    /*
	     * We need two separate formatters here, since there is
	     * no way to get the single digit date (day of month) to
	     * pad with a space instead of a zero.
	     */
	    this.date1Format =
		new SimpleDateFormat("MMM  d HH:mm:ss ", Locale.US);

	    this.date2Format =
		new SimpleDateFormat("MMM dd HH:mm:ss ", Locale.US);

	    this.date1Format.setTimeZone(timeZone);
	    this.date2Format.setTimeZone(timeZone);
	}
    }

    /**
     * Logs the message to syslog. The facility and priority level are
     * the same as their UNIX counterparts, and the Syslog class provides
     * constants for these fields. The message is what is actually logged.
     *
     * @param	facility		the facility code
     * @param	priority		the priority level
     * @param	message			the message to be logged
     */
    public void log(int facility, int priority, String message)
	throws SyslogException
    {
	log(this.boundAddress, this.port, facility, priority, message);
    }

    /**
     * Logs the message to syslog. The facility and priority level are
     * the same as their UNIX counterparts, and the Syslog class provides
     * constants for these fields. The message is what is actually logged.
     *
     * @param	address			the Inet address
     * @param	port			the port listened by syslogd
     * @param	facility		the facility code
     * @param	priority		the priority level
     * @param	message			the message to be logged
     */
    public void log(InetAddress address,
		    int port,
		    int facility,
		    int priority,
		    String message)
	throws SyslogException
    {
	int		code = SyslogConstants.computeCode(facility, priority);
	int		length;
	int		index;
	byte[]		data;
	byte[]		bytes;
	String		date;
	String		name;

	if (this.logName != null)
	{
	    name = this.logName;
	}
	else
	{
	    name = Thread.currentThread().getName();
	}

	length = 4 + name.length() + message.length() + 1;
	length += (code > 99) ? 3 : ((code > 9) ? 2 : 1);

	if (!this.includeDate)
	{
	    date = null;
	}
	else
	{
	    // See note above on why we have two formats...
	    Calendar	now = Calendar.getInstance();

	    if (now.get(Calendar.DAY_OF_MONTH) < 10)
		date = this.date1Format.format(now.getTime());
	    else
		date = this.date2Format.format(now.getTime());

	    length += date.length();
	}

	data = new byte[length];

	index = 0;
	data[index++] = (byte)'<';

	bytes = String.valueOf(code).getBytes();
	System.arraycopy(bytes, 0, data, index, bytes.length);
	index += bytes.length;

	data[index++] = (byte)'>';

	if (this.includeDate)
	{
	    bytes = date.getBytes();
	    System.arraycopy(bytes, 0, data, index, bytes.length);
	    index += bytes.length;
	}

	bytes = name.getBytes();
	System.arraycopy(bytes, 0, data, index, bytes.length);
	index += bytes.length;

	data[index++] = (byte)':';
	data[index++] = (byte)' ';

	bytes = message.getBytes();
	System.arraycopy(bytes, 0, data, index, bytes.length);
	index += bytes.length;

	data[index] = 0;

	DatagramPacket	packet = new DatagramPacket(data,
						    length,
						    address,
						    port);

	try
	{
	    socket.send(packet);

	    if ((this.flags & SyslogConstants.LOG_PERROR) != 0)
	    {
		name = (this.logName != null)
				? this.logName
				: Thread.currentThread().getName();

		System.err.println(name + ": " + message);
	    }
	}
	catch (IOException ex)
	{
	    String	errMsg = "error sending message: '" +
				 ex.getMessage() + "'";

	    if ((this.flags & SyslogConstants.LOG_PERROR) != 0)
	    {
		System.err.println(errMsg);
		ex.printStackTrace(System.err);
	    }
	    throw new SyslogException(errMsg, ex);
	}
    }
}

package com.broadon.syslog;

/**
 * The <code>UserSyslog</code> class is a specialize class of
 * <code>Syslog</code>, and allows only syslog messages of the
 * User facility.
 *
 * @version	$Revision: 1.1 $
 */
public class UserSyslog
    extends Syslog
{
    /**
     * Creates a UserSyslog instance for logging to the User facility
     * of syslog.
     *
     * @param	name			the identity (e.g., program name)
     */
    public UserSyslog(String name)
	throws SyslogException
    {
	this(name, SyslogConstants.LOG_PERROR);
    }

    /**
     * Creates a UserSyslog instance for logging to the User facility
     * of syslog.
     *
     * @param	name			the identity (e.g., program name)
     * @param	flags			the option flags
     */
    public UserSyslog(String name, int flags)
	throws SyslogException
    {
	super(name, flags);
	/*
	 * Why do we provide this flag?
	 * Because catching exceptions from 'log()' would be
	 * too onerous for the programmer. By using this flag
	 * the programmer can catch any possible exceptions that
	 * might come up during logging, except for the syslog
	 * server going away during execution.
	 */
	if ((flags & SyslogConstants.LOG_START) != 0)
	{
	    super.log(SyslogConstants.LOG_SYSLOG,
		      SyslogConstants.LOG_INFO,
		      "User syslog ready");
	}
    }

    /**
     * Performs a syslog to the currently bound syslog host.
     * Set the default level to ALERT.
     *
     * @param	message			the message to be logged
     */
    public void log(String message)
    {
	this.log(SyslogConstants.LOG_ALERT, message);
    }

    /**
     * Performs a syslog to the currently bound syslog host.
     *
     * @param	priority		the priority level
     * @param	message			the message to be logged
     */
    public void log(int level, String message)
    {
	try
	{
	    super.log(SyslogConstants.LOG_USER, level, message);
	}
	catch (SyslogException ex)
	{
	    /*
	     * Ignore, stack trace already printed from super.log().
	     */
	}
    }

    /**
     * Logs a user message to the syslog file (indirectly by syslogd).
     */
    public static final void main(String[] args)
	throws Throwable
    {
	String	name = args[0];
	String	message = "This is a test.";

	if (args.length > 1)
	    message = args[1];

	UserSyslog	syslog = new UserSyslog(name);

	syslog.log(message);
    }
}

package com.broadon.ccs;

/**
 * The <code>StatusCode</code> class defines the error codes returned
 * by the content download server.
 *
 * @version	$Revision: 1.1 $
 */
public class StatusCode
    extends com.broadon.status.StatusCode
{
    /**
     * Missing some or all of the arguments.
     */
    public static final String		CCS_BAD_ARGUMENT	= "1201";

    /**
     * Invalid depot identifier.
     */
    public static final String		CCS_INVALID_DEPOT	= "1202";

    /**
     * Incomplete request.
     */
    public static final String		CCS_INCOMPLETE		= "1203";

    private static final String		CCS_CODE_BASE	= CCS_BAD_ARGUMENT;
    private static final String[]	CCS_MESSAGES	=
    {
	"Missing some or all of the arguments",				// 1201
	"Invalid depot identifier",					// 1202
	"The request is only partially completed",			// 1203
    };

    static
    {
	/*
	 * Add the CCS status codes and their messages to the global map.
	 */
	int	code = Integer.parseInt(CCS_CODE_BASE);
	int	count = CCS_MESSAGES.length;

	for (int n = 0; n < count; n++, code++)
	{
	    addStatusCode(String.valueOf(code), CCS_MESSAGES[n]);
	}
    }
}

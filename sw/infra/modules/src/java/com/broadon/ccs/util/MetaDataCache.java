package com.broadon.ccs.util;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.broadon.util.BackingStoreException;
import com.broadon.util.CacheLFU;

public class MetaDataCache extends CacheLFU implements com.broadon.util.BackingStore{

    private static DataSource dataSource;
    final static long CacheTimeOut = 1000*5; // set a short 5 seconds timeout
    
    public MetaDataCache(DataSource ds)
    {
        // set cache size=100 and timeout=5 seconds.
        super(100, CacheTimeOut);
        this.dataSource = ds;
    }
    
    public Object retrieve(Object id) throws BackingStoreException
    {
        ContentLongId contentId = (ContentLongId) id;
        Connection conn = null;
        ContentBean metaData= null;
        try {
           metaData = ContentDownloadUtil.getContent(contentId, dataSource);
           if (metaData == null)
               throw new BackingStoreException();
        } catch (NotFoundException e) {
            throw new BackingStoreException("Data Not Found", e);       
        }catch (SQLException e) {
            throw BackingStoreException.rewrap(e);            
        } finally {
            if (conn != null)
                try {
                    conn.close();
                } catch (SQLException e2) {
                    System.out.println("cannot close connection: "+e2.getMessage());
                }
        }
        return metaData;
    }

    /**
     * Returns details for a given eCardType
     */
    public ContentBean getContentBean(ContentLongId id)
        throws BackingStoreException
    {
        return (ContentBean) get(id);
    }
    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub

    }

}

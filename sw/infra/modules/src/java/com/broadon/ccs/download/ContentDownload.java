package com.broadon.ccs.download;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;
import com.broadon.ccs.CCSInfo;

import com.broadon.ccs.util.ContentFactory;
import com.broadon.ccs.util.ContentHeader;
import com.broadon.ccs.util.ContentLongId;
import com.broadon.ccs.util.FileCache;

/**
 * The <c>ContentDownload</c> class provides the necessary support for finding
 * out what downloads are needed for the depot to become synchronized with
 * the operation center. However, it does not support the actually content
 * download, which is the job of the Content Download Server.
 *
 * @version	$Revision: 1.7 $
 */
public class ContentDownload
    extends CCSInfo
{
    private static final long twentyfourhours = 3600*1000*24;
    private static final long fiveMinutes = 5*1000;
    
    /** A class used to set attributes of the response header
     *  and then get the response output stream
     */
    public static class ContentResponse
        implements ContentFactory.ContentResponse
    {
        private HttpServletResponse res;

        public ContentResponse(HttpServletResponse res)
        {
            this.res = res;
        }
        public void setContentLength(long sz)
        {
            res.setContentLength((int)sz);
        }
        public OutputStream getOutputStream() throws IOException
        {
            return res.getOutputStream();
        }
    } // class ContentResponse


    /*
     * Factories used by the ContentDownload instance.
     */
    public static final int		     CONTENT = 0;
    private static ContentFactory[] factories;
    private static String[]		     factoryNames =
        {
			ContentFactory.class.getName()
        };

    static
    {
        try
        {
            /*
             * Get the factory instances.
             */
            int		size = factoryNames.length;

            factories = new ContentFactory[size];
            for (int n = 0; n < size; n++)
            {
                factories[n] = new ContentFactory();
            }
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            System.exit(-1);
        }
    }


    public static FileCache getFileCache(String  cacheRootPath,
                                         long    cacheMaxMbytes,
                                         long    cacheMaxfiles,
                                         boolean cacheReset)
        throws IOException
    {
        // Define the cache
        //
        ContentFactory factory = factories[CONTENT];
        return factory.getFileCache(cacheRootPath,
                                    cacheMaxMbytes,
                                    cacheMaxfiles,
                                    cacheReset);
    }


    private void setResponseHeader(ContentFactory      fact,
                                   HttpServletResponse res,
                                   long                startTime, 
                                   long                offset,
                                   long                chunksz,
                                   long                cacheTime)
    {
        // Set the parts of the response header not dependent on
        // the actual response data.
        //
        res.setContentType("application/octet-stream");
        if (chunksz > 0 &&
            fact.getRecommendedChunksize() == chunksz &&
            (offset % chunksz) == 0) {
            //
            // Only permit web-server caching when the chunksize matches
            // the recommended size and the offset is a multiple of the
            // chunksize.
            //
            res.setDateHeader("Last-Modified", startTime);
            res.setHeader("Cache-Control", "public");
            res.setDateHeader("Expires", 
                              System.currentTimeMillis() + cacheTime);
        }
        else {
            res.setDateHeader("Last-Modified", startTime);
            res.setDateHeader("Expires", -1);
            res.setHeader("Cache-Control", "no-store");
            res.setHeader("Pragma", "no-cache");
        }
    } // setResponseHeader

    /**
     * Retrieves the content, identified by the checksum, and puts it
     * to the provided OutputStream.
     *
     * @param fileCache     the thread-shared file-cache for this servlet
     * @param res           the HTTP servlet response
     * @param startTime     the servlet/webapp start time in msecs
     * @param checksum      the content checksum
     * @param offset        the starting offset
     * @param chunksize		the maximum size of the next chunk returned
     * @param encode        base64 encoding when true, otherwise binary
     * @param prependHeader whether or not to prepend a content header
     * to the chunk
     *
     * @throws Exception - to be defined
     */
    public void getContent(FileCache           fileCache,
                           HttpServletResponse res,
                           long                startTime,
                           ContentLongId       id,
                           long                offset,
                           long                chunksize,
                           boolean             encode,
                           boolean             prependHeader)
	throws Exception
    {
        ContentFactory	contentFactory = (ContentFactory)factories[CONTENT];
        setResponseHeader(contentFactory, res, startTime, offset, chunksize, twentyfourhours);
        contentFactory.getContent(fileCache,
                                  new ContentResponse(res),
                                  id,
                                  ContentHeader.CHECKSUM_KEY,
                                  offset,
                                  chunksize,
                                  encode,
                                  prependHeader,
                                  super.dataSource); 
    }
    
    /**
     * Retrieves the content, identified by the checksum, and puts it
     * to the provided OutputStream.
     *
     * @param res           the HTTP servlet response
     * @param startTime     the servlet/webapp start time in msecs
     * @param titleId       tmd title id
     * @param checksum      tmd checksum
     * @param offset        the starting offset
     * @param chunksize     the maximum size of the next chunk returned
     * @param encode        base64 encoding when true, otherwise binary
     * @param prependHeader whether or not to prepend a content header
     * to the chunk
     *
     * @throws Exception - to be defined
     */
    public void getMetaContent(
                           HttpServletResponse res,
                           long                startTime,
                           ContentLongId       id,
                           boolean             attachCerts,
                           long                offset,
                           long                chunksize,
                           boolean             encode,
                           boolean             prependHeader)
    throws Exception
    {
        ContentFactory  contentFactory = (ContentFactory)factories[CONTENT];
        setResponseHeader(contentFactory, res, startTime, offset, chunksize, fiveMinutes);
        contentFactory.getMetaContent(new ContentResponse(res),
                                  id,
                                  attachCerts,
                                  offset,
                                  chunksize,
                                  encode,
                                  prependHeader,
                                  super.dataSource); 
    }

}

package com.broadon.ccs.download;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.StringTokenizer;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.broadon.ccs.ContentServlet;
import com.broadon.ccs.util.ContentDownloadLog;
import com.broadon.ccs.util.ContentDownloadUtil;
import com.broadon.ccs.util.ContentLongId;
import com.broadon.ccs.util.ContentLongIdCache;
import com.broadon.ccs.util.FileCache;
import com.broadon.ccs.util.MetaDataCache;

/**
 * The <code>ContentDownloadServlet</code> class handles the requests from
 * the depots to download the content, identified by the content identifier.
 *
 * @version	$Revision: 1.23 $
 */
public class ContentDownloadServlet
    extends ContentServlet
{
    private final static String	CACHE_ROOT_PATH_NAME = "cacheRootPath";
    private final static String	CACHE_MAX_MBYTES = "cacheMaxMBytes";
    private final static String	CACHE_MAX_FILES	= "cacheMaxFiles";
    private final static String	CACHE_RESET	= "cacheReset";
    private final static String METADATA_CACHE = "metaDataCache";

    private static final String REQPARAM_ENCODE = "encode";
    private static final String REQPARAM_GETHEADER = "getheader";
    private static final String REQPARAM_OFFSET = "offset";
    private static final String REQPARAM_CHUNKSIZE = "chunksize";
    private static final String REQPARAM_CONTENT_ID1 = "contentId";
    private static final String REQPARAM_CONTENT_ID2 = "content_id";    // keep the original style just in case
    private static final String REQPARAM_TITLE_ID = "titleId";
    private static final String REQPARAM_DEVICE_ID = "deviceId";

    private final static String	 DEF_CACHE_ROOTPATH = "/opt/broadon/data/svcdrv/cache/ccs";
    private final static String  DEF_FILE_LOC = "/opt/broadon/pkgs/ccs/pki_data/ncbeta";
    private final static int	 DEF_CACHE_MAX_FILES = 128;   // # files
    private final static int	 DEF_CACHE_MAX_MBYTES = 1024; // # Mbytes
    private final static boolean DEF_CACHE_RESET	= true;   // clear on start
    private final static boolean DEF_METADATA_CACHE = false;    // no memory cache for metadata

    public static MetaDataCache metaDataCache;
    private static ContentLongIdCache contentLongIdCache;

    private String          cacheRootPath;  // cache root dir
    private long	        cacheMaxFiles;  // cache maximum number of files
    private long            cacheMaxMbytes; // cache storage size
    private boolean         cacheReset;	    // reset cache?startTime);
    private long            startTimeMsecs; // server start time
    private FileCache       fileCache;      // a cache of content objects
    public static boolean         useMetaDataCache;  // metadata uses in memory?

    private long getLongParam(HttpServletRequest  req,
                              HttpServletResponse res,
                              String              param,
                              long                def,
                              long                err)
    {
        String val = req.getParameter(param);
        long   l;
        if (val == null) {
            l = def;
        }
        else {
            try {
                l = Long.parseLong(val);
            }
            catch (NumberFormatException e) {
                l = err;
            }
        }
        return l;
    } // getLongParam


    /**
     * Constructs a ContentDownloadServlet instance.
     */
    public ContentDownloadServlet()
    {
        super();
        this.cacheRootPath = DEF_CACHE_ROOTPATH;
        this.cacheMaxFiles = DEF_CACHE_MAX_FILES;
        this.cacheMaxMbytes = DEF_CACHE_MAX_MBYTES;
        this.cacheReset = DEF_CACHE_RESET;
        startTimeMsecs = System.currentTimeMillis();
    }

    /**
     * Initializes this servlet.
     *
     * @param	servletConfig		the servlet configuration object
     */
    public synchronized void init(ServletConfig servletConfig)
	throws ServletException
    {
        super.init(servletConfig);

        ServletContext	context = servletConfig.getServletContext();
        String		    temp;

        temp = context.getInitParameter(CACHE_ROOT_PATH_NAME);
        if (temp != null)
            this.cacheRootPath = temp;
        temp = context.getInitParameter(CACHE_MAX_FILES);
        if (temp != null)
            this.cacheMaxFiles = Long.parseLong(temp);
        temp = context.getInitParameter(CACHE_MAX_MBYTES);
        if (temp != null)
            this.cacheMaxMbytes = Long.parseLong(temp);
        temp = context.getInitParameter(CACHE_RESET);
        if (temp != null)
            this.cacheReset = temp.equalsIgnoreCase("true");
        temp = context.getInitParameter(METADATA_CACHE);
        if (temp != null)
            this.useMetaDataCache = temp.equalsIgnoreCase("true");
        else
            this.useMetaDataCache = DEF_METADATA_CACHE;
        if (useMetaDataCache && contentLongIdCache == null)
            contentLongIdCache = new ContentLongIdCache(dataSource);
        if (useMetaDataCache && metaDataCache == null)
            metaDataCache = new MetaDataCache(dataSource);
        if (useMetaDataCache)
            System.out.println("meta data will be cached in memory");
        else
            System.out.println("meta data will not be cached in memory");

        cacheRootPath += File.separator + "Content";
        System.out.println(CACHE_ROOT_PATH_NAME + "[" + cacheRootPath + "]");
        System.out.println(CACHE_MAX_FILES + "[" + cacheMaxFiles + "]");
        System.out.println(CACHE_MAX_MBYTES + "[" + cacheMaxMbytes + "]");
        System.out.println(CACHE_RESET + "[" + cacheReset + "]");

        // A filecache shared between all instances of this servlet.
        //
        try {
            fileCache = ContentDownload.getFileCache(cacheRootPath,
                                                     cacheMaxMbytes, 
                                                     cacheMaxFiles,
                                                     cacheReset);
        }
        catch (IOException e) {
            // Failed to initialize the file-cache
            //
            throw new ServletException("Failed to create the file-cache", e);
        }            

        startTimeMsecs = System.currentTimeMillis();
    }

    /**
     * Handles the Get request.
     *
     * @param	req			the HTTP request
     * @param	res			the HTTP response
     *
     * @throws	ServletException
     * @throws	IOException
     */
    public void doGet(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
        /*
         * Initialize.
         */
        ContentDownload	contentDownload = new ContentDownload();
        super.processInit(req, res, contentDownload);

        // Get hold of request parameters
        //
        boolean getheader = req.getParameter(REQPARAM_GETHEADER) != null;
        boolean encode = req.getParameter(REQPARAM_ENCODE) != null;
        long    offset = getLongParam(req, res, REQPARAM_OFFSET, 0, -1);
        long    chunksz = getLongParam(req, res, REQPARAM_CHUNKSIZE, Long.MAX_VALUE, -1);
        
        /* use new syntax to get titleId and contentId 
        String titleId = req.getParameter(REQPARAM_TITLE_ID);
        String contentId = req.getParameter(REQPARAM_CONTENT_ID1);
        if (contentId == null)
            contentId = req.getParameter(REQPARAM_CONTENT_ID2);
        */
        String deviceIdStr = req.getParameter(REQPARAM_DEVICE_ID);
        /* */
        String[] pathElements = new String[4];
        String reqUri = req.getRequestURI().toString();
        // reqUri will be something like "/ccs/download/1234567/tmd";
        StringTokenizer token = new StringTokenizer(reqUri,"/");
        // extra stuff will be ignored
        try {
            for (int i=0; i<4; i++)
                pathElements[i] = token.nextToken();
        } catch (java.util.NoSuchElementException e) {
            ;   // titleId or contentId will be missing
        }
        String titleId = pathElements[2];
        String contentId = pathElements[3];
        /**/
        // Check for errors (the response will already have been set)
        //
        if (titleId == null) {
            res.sendError(res.SC_BAD_REQUEST, 
                    "Invalid or missing " + REQPARAM_TITLE_ID);
            return;
        }
        else if (contentId == null) {
            res.sendError(res.SC_BAD_REQUEST, 
                    "Invalid or missing " + REQPARAM_CONTENT_ID1);
            return;
        }
        else if (offset < 0) {
            res.sendError(res.SC_BAD_REQUEST, 
                          "Invalid or missing " + REQPARAM_OFFSET);
            return;
        }
        else if (chunksz < 0) {
            res.sendError(res.SC_BAD_REQUEST, 
                          "Invalid or missing " + REQPARAM_CHUNKSIZE);
            return;
        }
        // for cpcerts/xscerts/crl, get from default directory
        // and stream out the data, ignore the other parameters 
        // -- this section may be a temp. solution and taken out later
        if (contentId.equalsIgnoreCase(ContentDownloadUtil.CPCERTS) 
                || contentId.equalsIgnoreCase(ContentDownloadUtil.XSCERTS)
                || contentId.equalsIgnoreCase(ContentDownloadUtil.CRL)) {
            OutputStream os = res.getOutputStream();
            InputStream is;
            // if it is cp.certs, output cp.certs.hdr first
            if (getheader) {
                is = new FileInputStream(DEF_FILE_LOC+"/"+contentId+".hdr");
                byte[] buf = new byte[1024];
                int count = 0;
                while ((count = is.read(buf)) >= 0) {
                    os.write(buf, 0, count);
                }
                is.close();
                os.close();
                return;
            }
            is = new FileInputStream(DEF_FILE_LOC+"/"+contentId);
            byte[] buf = new byte[1024];
            int count = 0;
            while ((count = is.read(buf)) >= 0) {
                os.write(buf, 0, count);
            }
            is.close();
            os.close();
            return;
        }
        //      Create the response and stream it back to the requesting client.
        //
        ContentDownloadLog  contentDownloadLog = new ContentDownloadLog();
        try {
        // create ContentLongId object
        ContentLongId id = null;
        if (contentId.startsWith(ContentDownloadUtil.TMD)) {
            if (!useMetaDataCache)
                id = ContentDownloadUtil.getTMDContentLongId(titleId, contentDownload.getCASDataSource());
            else
                id = contentLongIdCache.getContentLongId(titleId);
        }
        else if (contentId.startsWith(ContentDownloadUtil.MET))
            id = new ContentLongId(titleId, ContentDownloadUtil.MET_CONTENT_ID);
        else if (contentId.startsWith(ContentDownloadUtil.COMMON_ETICKET))
            id = new ContentLongId(titleId, ContentDownloadUtil.COMMON_ETICKET_ID);
        else 
            id = new ContentLongId(titleId, contentId);
        System.out.println("contentId= " +  id.getContentId());
        System.out.println("titleId= " +  id.getTitleId());
        // Prepare the audit log information.
        //
        contentDownloadLog.setRequestDate(new Date());
        contentDownloadLog.setRequestLog(serverLog);
        contentDownloadLog.setRequestStatus(ContentDownloadLog.STATUS_OK);
        contentDownloadLog.setDeviceId(ContentDownloadUtil.getBigInteger(deviceIdStr));
        contentDownloadLog.setContentId(id.toBigInteger());
        
        if (contentId.startsWith(ContentDownloadUtil.TMD) ) {
                contentDownload.getMetaContent(
                        res,
                        startTimeMsecs,
                        id,
                        false,       // don't attach certificates -- attchecd already
                        offset,
                        chunksz,
                        encode,
                        getheader);
        } else if (contentId.startsWith(ContentDownloadUtil.MET) 
                    || contentId.startsWith(ContentDownloadUtil.COMMON_ETICKET)) {
                contentDownload.getMetaContent(
                        res,
                        startTimeMsecs,
                        id,
                        false,      // no certificates attached
                        offset,
                        chunksz,
                        encode,
                        getheader);
        } else {
                contentDownload.getContent(
                        fileCache,
                        res,
                        startTimeMsecs,
                        id,
                        offset,
                        chunksz,
                        encode,
                        getheader);
            }
        }
        catch (Throwable t) {
            // Record the failure
            //
            t.printStackTrace();
            res.sendError(res.SC_NOT_FOUND, t.getMessage());
            contentDownloadLog.setRequestStatus(ContentDownloadLog.STATUS_FAILED);
        }
        finally {
            // log to db
            //
            contentDownloadLog.setCompletionDate(new Date());
            if (contentDownloadLog.getContentId()!=null && contentDownloadLog.getDeviceId()!=null)
                // log only when key is not null
                ContentDownloadUtil.log(contentDownloadLog, contentDownload.getDataSource());
        }
    } // doGet
}

package com.broadon.ccs.util;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.broadon.util.BackingStoreException;
import com.broadon.util.CacheLFU;

public class ContentLongIdCache extends CacheLFU implements com.broadon.util.BackingStore{

    private static DataSource dataSource;
    final static long CacheTimeOut = 1000*5; // set a short 5 seconds timeout
    
    public ContentLongIdCache(DataSource ds)
    {
        // set cache size=100 and timeout=5 seconds.
        super(100, CacheTimeOut);
        this.dataSource = ds;
    }
    
    public Object retrieve(Object id) throws BackingStoreException
    {
        String titleId = (String) id;
        Connection conn = null;
        ContentLongId longId= null;
        try {
           longId = ContentDownloadUtil.getTMDContentLongId(titleId, dataSource);
           if (longId == null)
               throw new BackingStoreException();
        } finally {
            if (conn != null)
                try {
                    conn.close();
                } catch (SQLException e2) {
                    System.out.println("cannot close connection: "+e2.getMessage());
                }
        }
        return longId;
    }

    /**
     * Returns details for a given eCardType
     */
    public ContentLongId getContentLongId(String id)
        throws BackingStoreException
    {
        return (ContentLongId) get(id);
    }
    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub

    }

}

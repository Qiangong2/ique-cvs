package com.broadon.ccs.util;

import java.sql.Blob;

public class ContentBean {
    private String  contentChecksum;
    private Blob contentBlob;
    Blob getContentBlob() {
        return contentBlob;
    }
    void setContentBlob(Blob contentBlob) {
        this.contentBlob = contentBlob;
    }
    String getContentChecksum() {
        return contentChecksum;
    }
    void setContentChecksum(String contentChecksum) {
        this.contentChecksum = contentChecksum;
    }
}

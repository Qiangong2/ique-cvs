package com.broadon.ccs.util;

import java.math.BigInteger;

public class MetaDataRecord {

private String titleId;
private BigInteger contentId;       // from database
private String queryContentId;  // from query string, tmd or met or commoneticket
private String checksum;

public String getChecksum() {
    return checksum;
}
void setChecksum(String checksum) {
    this.checksum = checksum;
}
public BigInteger getContentId() {
    return contentId;
}
void setContentId(BigInteger contentId) {
    this.contentId = contentId;
}
public String getQueryContentId() {
    return queryContentId;
}
void setQueryContentId(String queryContentId) {
    this.queryContentId = queryContentId;
}
public String getTitleId() {
    return titleId;
}
void setTitleId(String titleId) {
    this.titleId = titleId;
}

}

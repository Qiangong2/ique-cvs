package com.broadon.ccs.util;

import java.math.BigInteger;
import java.util.Date;

/**
 * The <c>ContentDownloadLog</c> class conforms to the JavaBeans property,
 * and is mainly used for data transformation.
 *<p>
 * It contains information about a content download log identified by the
 * device identifier, content identifier, and the request date.
 *
 * @version	$Revision: 1.2 $
 */
public class ContentDownloadLog
    extends com.broadon.db.AuditLog
{
    private BigInteger  deviceId;
    private BigInteger	contentID;
    private Date		completionDate;

    /**
     * Constructs an empty ContentDownloadLog instance.
     */
    public ContentDownloadLog()
    {
	super();
    }

    /**
     * Getter for DeviceId.
     *
     * @return	The device identifier.
     */
    public BigInteger getDeviceId()
    {
	return deviceId;
    }

    /**
     * Setter for deviceId.
     *
     * @param	deviceId			the device identifier
     */
    public void setDeviceId(BigInteger deviceId)
    {
	this.deviceId = deviceId;
    }

    /**
     * Getter for contentID.
     *
     * @return	The content idenifier whose content has been downloaded.
     */
    public BigInteger getContentId()
    {
	return contentID;
    }

    /**
     * Setter for contentID.
     *
     * @param	contentID		the content idenifier whose content
     *					has been downloaded
     */
    public void setContentId(BigInteger contentID)
    {
	this.contentID = contentID;
    }

    /**
     * Getter for completionDate.
     *
     * @return	The completion date of downloading.
     */
    public Date getCompletionDate()
    {
	return completionDate;
    }

    /**
     * Setter for completionDate.
     *
     * @param	completionDate		the completion date of downloading
     */
    public void setCompletionDate(Date completionDate)
    {
	this.completionDate = completionDate;
    }
}

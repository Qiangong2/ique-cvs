package com.broadon.ccs.util;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Blob;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Date;

import javax.sql.DataSource;

import com.broadon.ccs.download.ContentDownloadServlet;
import com.broadon.ccs.util.DBException;
import com.broadon.util.BackingStoreException;
import com.broadon.util.HexString;

/**
 * The <c>ContentFactory</c> class interfaces with the database to
 * create, update, or query records from the content-objects table.
 * Each record is represented by a Content JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class. Ugly ugly!!
 *
 * @version	$Revision: 1.15 $
 */
public class ContentFactory
{
    private static void err(String msg)
    {
        final DateFormat dateFmt = 
            DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.LONG);
        System.err.println(dateFmt.format(new Date()) + " ERROR " + msg);
    }
    private static void log(String msg)
    {
        final DateFormat dateFmt = 
            DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.LONG);
        System.out.println(dateFmt.format(new Date()) + " INFO " + msg);
    }

    /** Response attributes
     */
    public interface ContentResponse
    {
        public void setContentLength(long sz);
        public OutputStream getOutputStream() throws IOException;
    }
    

    /** Function object to restore ContentHeader from a cached file
     */
   private static class FileCacheRestorer implements FileCache.RestoreCache
    {
        public FileCache.KeySet apply(File cached, String longId) 
        {
            try {
                InputStream inp = new FileInputStream(cached);
                DataInputStream inpd = new DataInputStream(inp);
                return new ContentHeader(inpd, longId);
            }
            catch (IOException e) {
                err(e.getMessage());
                return null;
            }
        }
    }


    /**
     * Constructs a ContentFactory instance.
     */
    public ContentFactory()
    {
        super();
    }


    /**
     * Use the existing cache for the given path, if one exists, and
     * otherwise create a new one with the given parameters.  The
     * clearCache will only happen if a new cache is created.  If the cache
     * should not be cleared, an attempt will be made to reuse the cache
     * stored in the given root path.
     *
     * This is a no-op if the file-cache has already been set for this 
     * object as the shared file-cache can only be set once.
     *
     * @param	rootDir		The root directory of the file cache
     * @param	maxMbytes	Maximum megabytes to be used by the cache
     * @param   maxFiles    The maximum number of files/objects in the cache
     * @param	clearCache	Whether or not to clear any existing cache
     */
    public FileCache getFileCache(String  rootDir, 
                                  long    maxMbytes, 
                                  long    maxFiles,
                                  boolean clearCache)
        throws IOException
    {
        return FileCache.getFileCache(rootDir,
                                      maxMbytes, 
                                      maxFiles,
                                      ContentHeader.NUMBER_OF_KEYSETS,
                                      new FileCacheRestorer(),
                                      clearCache);
    }


    public long getRecommendedChunksize() 
    {
        return ContentHeader.getChunkSize();
    }




    /**
     * Retrieves the content of the given contentID, and puts it to the
     * provided Output destionation.
     *<p>
     * This query is an exception that it does not return any Bean object.
     * Instead, it takes an OutputStream as the output target. This allows
     * the optimization to directly ship the data to the destination, such
     * as a file or the stream as a response back to the HTTP requestor.
     *
     * @param fileCache     cache of filed content
     * @param res           response attributes
     * @param id		    the identifier
     * @param cacheIdIdx    Key-set index for the id in the FileCache
     * @param offset		the starting offset
     * @param chunksize		the maximum bytes of the next chunk returned
     * @param encode        base64 encoding when true, otherwise binary
     * @param prependHeader whether or not to prepend a content header
     * @param dataSource    SQL data source for DB access
     *
     * @throws	DBException
     * @throws	IOException
     */
    public void getContent(FileCache       fileCache,
                            ContentResponse res,
                            ContentLongId   id,
                            int             cacheIdIdx,
                            long            offset,
                            long            chunksize,
                            boolean         encode,
                            boolean         prependHeader,
                            DataSource      dataSource)
        throws DBException, IOException
    {
        if (offset < 0) {
            //
            // Starting offset must be non-negative.
            ///
            throw new 
                IllegalArgumentException("Invalid content download offset[" +
                                         offset +
                                         "]");
        }
        // See if the content is in the file-cache.
        //
        FileCache.CachedStream cs = fileCache.getStream(id.toString(), cacheIdIdx);
        if (cs == null) {
            //
            // Retrieve the content from the database, writing to a temp-file
            // in the caching directory.  Note that even if no caching
            // is to take place, we will store each content object in the
            // file-cache temporarily.
            //
            cs = cacheContent(fileCache, id, cacheIdIdx, dataSource);
        }
        // We are done with the DB/cache access, and will now stream the
        // binary chunk over the output stream, starting at byte ofst.
        //
        ContentHeader   hdr = (ContentHeader)cs.getKeySet();
        FileInputStream inp = cs.getCached();
        final long      ofst = offset + hdr.getHeaderSize();
        long            numBytes = hdr.getContentSize() - offset;

        // Limit the bytes to the chunksize if so requested
        //
        if (chunksize >= 0 && numBytes > chunksize)
            numBytes = chunksize;

        // Set the bytes in the response in the response header before
        // writing the response, then stream the output chunk to the 
        // given OutputStream
        //
        OutputStream out = res.getOutputStream();
        if (prependHeader) {
            res.setContentLength(hdr.getHeaderSize() + numBytes);
            hdr.writeBytes(out);
        } else {
            res.setContentLength(numBytes);
        }
        writeContent(inp, out, ofst, numBytes);
    } // getContent

    public void getMetaContent(
            ContentResponse res,
            ContentLongId   id,
            boolean         attachCerts,
            long            offset,
            long            chunksize,
            boolean         encode,
            boolean         prependHeader,
            DataSource      dataSource)
    throws SQLException, DBException, NoSuchAlgorithmException, IOException, BackingStoreException
    {
        if (offset < 0) {
            //
            // Starting offset must be non-negative.
            ///
            throw new 
            IllegalArgumentException("Invalid content download offset[" +
                         offset +
                         "]");
        }
        // 1. use content long id to get tmd data
        ContentBean content = null;
        if (ContentDownloadServlet.useMetaDataCache) {
            System.out.println("get meta data from in-memory cache");
            content = ContentDownloadServlet.metaDataCache.getContentBean(id);
        }
        else {
            System.out.println("get meta data: no in-memory cache used.");
            content = ContentDownloadUtil.getContent(id, dataSource);
        }
        Blob contentBlob = content.getContentBlob();
        // metadata size is fairly small, just get all in one shot
        int blobLength = (int)contentBlob.length();
        int contentLength = blobLength;
        byte[][] certs = null;
        if (attachCerts) {
            // 2. use titleId to get certs
            certs = ContentDownloadUtil.getCertificates(id.getTitleId(), dataSource);
            // 3. write to outputStream (check encoding)
            for (int i=0; i<certs.length; i++) {
                contentLength = contentLength + certs[i].length;
            }
        }
        ContentHeader hdr;
        byte[] digest;
        // get contents, attach certs if attachCerts is true
        byte[] contentBytes = new byte[contentLength];
//        System.out.println("blobLength= " + blobLength);
//        if (attachCerts) {
//            System.out.println("cert1Length= " + certs[0].length);
//            System.out.println("cert2Length= " + certs[1].length);
//        }
//        System.out.println("contentLength= " + contentLength);
        System.arraycopy(contentBlob.getBytes((long)1, blobLength), 0, contentBytes, 0, blobLength);
        int destPosition = blobLength;
        if (attachCerts) {
            for (int i=0; i<certs.length; i++) {
                System.arraycopy(certs[i], 0, contentBytes, destPosition, certs[i].length);
                destPosition += certs[i].length;
            }
            MessageDigest md = MessageDigest.getInstance("MD5");
            digest = md.digest(contentBytes);
        } else {
            digest = HexString.fromHexString(content.getContentChecksum());
        }
        hdr = new ContentHeader(id.getContentId(), contentLength, digest, id.toString());
        
        //final long      ofst = offset + hdr.getHeaderSize();
        long            numBytes = hdr.getContentSize() - offset;

        // Limit the bytes to the chunksize if so requested
        //
        if (chunksize >= 0 && numBytes > chunksize)
            numBytes = chunksize;
//System.out.println("blobLength= " + blobLength);
//System.out.println("cert1Length= " + certs[0].length);
//System.out.println("cert2Length= " + certs[1].length);
//System.out.println("contentLength= " + contentLength);
        // Set the bytes in the response header before
        // writing the response, then stream the output chunk to the 
        // given OutputStream
        //
        OutputStream out = res.getOutputStream();
        if (prependHeader) {
            res.setContentLength(hdr.getHeaderSize() + numBytes);
            hdr.writeBytes(out);
        } else {
                res.setContentLength(numBytes);
        }
        out.write(contentBytes, (int)offset, (int)numBytes);
        out.flush();
    } // getTMDContent
    
    private void writeContent(FileInputStream inp, 
                              OutputStream    out, 
                              long            ofst,
                              long            numBytes)
        throws IOException
    {
	    byte[]	   buffer = new byte[10*1024];
        final long endOfst = ofst + numBytes;

        try {
            long sz;
            inp.skip(ofst);
            for (long b = ofst; b < endOfst; b += sz) {
                sz = inp.read(buffer);
                if (sz + b > endOfst) {
                    //
                    // We read a little too much, so scale it back
                    //
                    sz = endOfst - b;
                }
                else if (sz <= 0) {
                    throw new IOException("Unexpected end of content file");
                }
                out.write(buffer, 0, (int)sz);
            }
		}
	    finally
	    {
            out.flush();
            inp.close();
	    }
    } // writeContent

    /**
     * Retrieves the content of the given key, writes it to a temporary file,
     * and then inserts it into the file-cache.
     *
     * @param key        the key object
     * @param id         the string representation of the id
     * @param idIdx      maps id to a column name in the table
     * @param dataSource the data base connection pool
     * @param nameMap    the name mapping table
     *
     * @throws	DBException
     * @throws	IOException
     */
    private FileCache.CachedStream cacheContent(FileCache  fileCache,
                                                ContentLongId     id,
                                                int        idIdx,
                                                DataSource dataSource)
        throws DBException, IOException
    {
        // Create a temporary file to hold the content header and the
        // content.
        //
        File tempFile = fileCache.getNewTempFile(id);
        OutputStream tempOut = new FileOutputStream(tempFile);
        ContentHeader contentHeader = ContentDownloadUtil.populateContentFile(id, dataSource, tempOut);

        // We now have the content header and the content in the temporary
        // file and need to move this file into our caching mechanism.
        // Note that we have to do this even when caching is OFF, since
        // we use the cache as a temporary repository during the processing
        // of this request, even when subsequent requests will not utilize
        // the cached value.
        //
        // Note also that we do not delete the tempFile in the event of
        // an exception to help debugging problems.  Such leftover files
        // will be removed when the WebApp restarts, since the FileCache
        // constructor cleans out the cache directory.
        //
        return fileCache.insertAndGetStream(contentHeader, tempFile);
    }
// REMOVED METHODS COMMENTS:
// The follwoing methods are comment out for:
// 1. contentID is changed to 96-bit and represented in java program as BigInteger
// 2. dependency on the db package is removed
// These methods will be added back as-needed with proper modification in the future.
// see db.ContentFactory for the original methods.

}

package com.broadon.ccs.util;

import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.Date;

import javax.sql.DataSource;
import com.broadon.util.Base64;
import com.broadon.util.HexString;

public class ContentDownloadUtil {
    
//    0xFFFFFFFF - eTicket Meta Data
//    0xFFFFFFFE - NetCard-specific metadata. (MET)
//    0xFFFFFFFD - Common eTicket
    public static final String TMD = "tmd";
    public static final String MET = "met";
    public static final String COMMON_ETICKET = "cetk";
    public static final String CPCERTS = "cp.certs";
    public static final String XSCERTS = "xs.certs";
    public static final String CRL = "crl";
    public static final String MET_CONTENT_ID="FFFFFFFE";
    public static final String COMMON_ETICKET_ID="FFFFFFFD";

    // the following two constants are used to truncate the contentId to lower 32 bits
    public static final int HEX=16;
    public static final int INT_LENGTH_IN_HEX=8;
    public static final int LONG_LENGTH_IN_HEX=16;
    
    // catch any Exception and just return 0
    public static BigInteger getBigInteger(String str) {
        BigInteger ret;
        try {
            ret = new BigInteger(str, HEX);
        } catch (Throwable e) {
            ret = new BigInteger("0");
        }
        return ret;
    }
    public static ContentLongId getTMDContentLongId(String titleId, DataSource ds) {
        Connection conn = null;
        Statement  stmt = null;
        ResultSet rs = null;
        BigDecimal tmdContentId = null;;
        ContentLongId id = null;
        String query = "select co.CONTENT_ID, co.CONTENT_OBJECT_VERSION from content_title_objects cto, content_objects co "
            + " where cto.content_id=co.content_id "
            + " and content_object_type='TMD' and TITLE_ID="
            + Long.parseLong(titleId, HEX)
            + " order by co.CONTENT_OBJECT_VERSION desc";
        //System.out.println(query);
        try {
            conn = ds.getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            // get the one with largest version number
            if (rs.next()) {
                tmdContentId = rs.getBigDecimal("CONTENT_ID");
            }
            if (tmdContentId != null) {
                id = new ContentLongId(tmdContentId);
            }
            else {
                throw new NotFoundException();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            if (stmt != null)
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            if (rs != null)
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
        return id;
    }

    public static ContentBean getContent(ContentLongId id, DataSource dataSource)
        throws SQLException, NotFoundException {
        Connection conn = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = "select * from CONTENT_OBJECTS where content_id = " + id.toBigInteger();
        //System.out.println(query);
        ContentBean content = null;
        try {
            conn = dataSource.getConnection();
            statement = conn.prepareStatement(query);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                Blob contentBlob = resultSet.getBlob("content_object");
                if (contentBlob != null) {
                    // Get the content and associated header information
                    //
                    //long  contentId = resultSet.getLong("content_id");
                    //content_id is extended to 96 bits in db. Here only the
                    //old version 32 bits and discards the rest.
                    content = new ContentBean();
                    content.setContentChecksum(resultSet.getString("content_checksum"));
                    content.setContentBlob(resultSet.getBlob("content_object"));
                }
            }
            if (content == null) {
                //
                // No record.
                //
                // An exception needs to be raised here so that an
                // error can be returned to the requester.
                //
                throw new NotFoundException("Content of [" +
                                        id.toBigInteger() +
                                        "] not found");
            }
        } finally {
            if (conn != null)
                conn.close();
            if (statement != null)
                statement.close();
            if (resultSet != null)
                resultSet.close();
        }
        return content;
    }
    
    public static ContentHeader populateContentFile(ContentLongId id, 
            DataSource dataSource, 
            OutputStream os)
        throws NotFoundException, IOException {
        ContentHeader     contentHeader = null;
        ContentBean content = null;
        try {
            // currently id is checksum only, idIdx is not used
            content = ContentDownloadUtil.getContent(id, dataSource);

            // Get the content and associated header information
            //
            //long  contentId = resultSet.getLong("content_id");
            //content_id is extended to 96 bits in db. Here only the
            //old version 32 bits and discards the rest.
            String  contentChecksum = content.getContentChecksum();
            Blob    contentBlob = content.getContentBlob();
            long    contentSize = contentBlob.length();
            long    contentId = id.getContentId();
            // Create the content header and write it as the very
            // first bytes in the temp file
            //
            contentHeader = new ContentHeader(contentId, 
                                              contentSize,
                                              HexString.fromHexString(contentChecksum),
                                              id.toBigInteger().toString(HEX));
            contentHeader.writeBytes(os);

            // Write the content bytes to the temp file in chunks of
            // 100K bytes at a time.
            //
            final int batchSize = 100*1024;
            for (long n = 1; n <= contentSize; n += batchSize) {
                os.write(contentBlob.getBytes(n, batchSize));
            }
        }
        catch (SQLException se) {
            se.printStackTrace();
            throw new IOException("SQL exception: " + se.getMessage());
        }
        finally {
            //
            // Close the temporary file, the statement and the connection
            // after execution.
            //
            try {
                    os.close();
            }
            catch (IOException ie) {
                err(ie.getMessage());
            }
        }
        return contentHeader;
    }
    
    public static byte[][] getCertificates(String titleId, DataSource dataSource)
        throws NotFoundException {
        Connection conn = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        byte[][] certs = new byte[2][];
        String query = "SELECT CERTIFICATE FROM CERTIFICATES A, CERTIFICATE_CHAINS B"  
                + " WHERE EXISTS "
                + "(SELECT 1 FROM CERTIFICATE_CHAINS D "  
                + " WHERE A.CERT_ID IN (D.SIGNER_CERT_ID, D.CA_CERT_ID) "
                + " AND D.CHAIN_ID = B.CHAIN_ID) "
                + " AND B.CHAIN_ID = "
                + "(select chain_id from content_title_last_version a, content_titles b "
                + " where hex_title_id = ? "
                + " and a.title_id = b.title_id)";
        try {
            conn = dataSource.getConnection();
            statement = conn.prepareStatement(query);
            statement.setString(1, titleId);
            resultSet = statement.executeQuery();
            if (resultSet == null || !resultSet.next()) {
                throw new NotFoundException("Certificate of [" +
                    titleId + "] not found");
            }
            String cert0 = resultSet.getString("CERTIFICATE");
            if (!resultSet.next()) {
                throw new NotFoundException("Certificate of [" +
                        titleId + "] not found");
            }
            String cert1 = resultSet.getString("CERTIFICATE");
            StringReader sr0  = new StringReader(cert0);
            StringReader sr1  = new StringReader(cert1);
            Base64 base64 = new Base64();
            certs[0] = base64.decode(sr0);
            certs[1] = base64.decode(sr1);
        } catch (SQLException e) {
            err("SQL error");
            e.printStackTrace();
        } catch (IOException e2) {
            err("IO error");
            e2.printStackTrace();
        } finally {
            if (conn != null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            if (statement != null)
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            if (resultSet != null) 
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
        return certs;
    }
    
    public static void log(ContentDownloadLog contentDownloadLog, DataSource ds) {
        Connection conn = null;
        PreparedStatement  pstmt = null;
        try {
            String insert = "insert into CONTENT_DOWNLOAD_LOGS "
                + "(DEVICE_ID, CONTENT_ID, REQUEST_DATE, COMPLETION_DATE, REQUEST_STATUS, REQUEST_LOG) "
                + " values (?, ?, ?, ?, ?, ?)";
            conn = ds.getConnection();
            pstmt = conn.prepareStatement(insert);
            pstmt.setBigDecimal(1, new BigDecimal(contentDownloadLog.getDeviceId()));
            pstmt.setBigDecimal(2, new BigDecimal(contentDownloadLog.getContentId()));
            pstmt.setTimestamp(3, new Timestamp(contentDownloadLog.getRequestDate().getTime()));
            pstmt.setTimestamp(4, new Timestamp(contentDownloadLog.getCompletionDate().getTime()));
            pstmt.setString(5, contentDownloadLog.getRequestStatus());
            pstmt.setString(6, contentDownloadLog.getRequestLog());
            pstmt.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            if (pstmt != null)
                try {
                    pstmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
    }
    
    private static void err(String msg)
    {
        final DateFormat dateFmt = 
            DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.LONG);
        System.err.println(dateFmt.format(new Date()) + " ERROR " + msg);
    }

}

package com.broadon.ccs.util;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import com.broadon.util.Base64;
//import com.broadon.util.FileCache;
import com.broadon.util.HexString;


/**
 * The <c>ContentHeader</c> class represents information to accompany
 * any downloaded content object, and will contain the the following
 * values:
 *<p>
 *   8 bytes magicNumber: A fixed number 0x1c2c3c4c5c6c7c8cL
 *   4 bytes headerSize:  The size of this header when encoded as a byte array
 *   4 bytes chunkSize:   The recommended chunk bytes to use for downloads
 *   8 bytes contentSize: The size of the content object (excluding header)
 *   8 bytes contentID
 *  16 bytes contentChecksum
 *<p>
 * The encoding to binary form will encode all integral values in
 * netorder (big endian), such that the most significant byte precedes 
 * the least significant byte in the resultant byte array/stream.
 *
 * @version	$Revision: 1.2 $
 */
public class ContentHeader implements FileCache.KeySet
{
    public static final int CONTENT_ID_KEY = 0;
    public static final int CHECKSUM_KEY = 1;
    public static final int NUMBER_OF_KEYSETS = 2;
    
    private static final int  CHECKSUM_SIZE = 16;
    private static final long MAGIC_NUMBER = 0x1c2c3c4c5c6c7c8cL;
    private static final int  CHUNK_SIZE = 1024*1024; // In units of bytes
    private static final int  HEADER_SIZE = 8 + 4 + 4 + 8 + 8 + CHECKSUM_SIZE;

    private long   contentSize;
    private long   contentID;
    private String contentID_key;
    private byte[] checksum;
    private byte[] encoded;
    private String longId;  // 96-bit id in Hex format


    private void encode() throws IOException
    {
        ByteArrayOutputStream out = new ByteArrayOutputStream(HEADER_SIZE);
        DataOutputStream outd = new DataOutputStream(out);
        
        outd.writeLong(MAGIC_NUMBER);
        outd.writeInt(HEADER_SIZE);
        outd.writeInt(CHUNK_SIZE);
        outd.writeLong(contentSize);
        outd.writeLong(contentID);
        outd.write(checksum, 0, checksum.length);
        outd.close();

        encoded = out.toByteArray();
        if (encoded.length != HEADER_SIZE) {
            throw new IOException(
                "Unexpected header size in ContentHeader encoding (" + 
                encoded.length + ")");
        }
    } // encode


    private void decode(DataInputStream inpd) throws IOException
    {
        if (inpd.readLong() != MAGIC_NUMBER)
            throw new IOException("Wrong magic number for ContentHeader");
        if (inpd.readInt() != HEADER_SIZE)
            throw new IOException("Unexpected header size for ContentHeader");
        if (inpd.readInt() != CHUNK_SIZE)
            throw new IOException("Unexpected chunk size for ContentHeader");

        contentSize = inpd.readLong();
        if (contentSize < 0)
            throw new IOException("Unexpected content size for ContentHeader");

        contentID = inpd.readLong();
        if (contentID <= 0)
            throw new IOException("Unexpected content id for ContentHeader");
        
        contentID_key = String.valueOf(contentID);

        checksum = new byte[CHECKSUM_SIZE];
        inpd.readFully(checksum);
        encode();
    } // decode


    public ContentHeader(long   contentID, 
                         long   contentSize,
                         byte[] checksum,
                         String longId) throws IOException
    {
        this.contentID = contentID;
        this.contentSize = contentSize;
        this.checksum = new byte[CHECKSUM_SIZE];
        for (int i=0; i<CHECKSUM_SIZE; i++)
            this.checksum[i] = checksum[i];
        contentID_key = String.valueOf(contentID);
        encode();
        this.longId = longId;
    }

// This constructor, if allowed, should add a longId parameter, or use a setLongId()
// to add a ongId after the header object is constructed!!
    public ContentHeader(DataInputStream inp, String longId) throws IOException
    {
        decode(inp);
        this.longId = longId;
    }


    public String toString()
    {
        StringBuffer b = new StringBuffer(256);
        b.append("[id=");
        b.append(contentID_key);
        b.append(",csum=");
        b.append(HexString.toHexString(checksum));
        b.append(",size=");
        b.append(String.valueOf(contentSize));
        b.append(",longId=");
        b.append(longId);
        b.append("]");
        return b.toString();
    }


    public void writeBytes(OutputStream out) throws IOException
    {
        out.write(encoded);
    }

    public void writeBase64(OutputStream out) throws IOException
    {
        Base64             base64 = new Base64();
        OutputStreamWriter outw = new OutputStreamWriter(out);
        base64.encode(encoded, outw);
    }


    public static int getHeaderSize() {return HEADER_SIZE;}
    public static int getChunkSize() {return CHUNK_SIZE;}
    public long getContentID() {return contentID;}
    public long getContentSize() {return contentSize;}
    public String getChecksum() {return HexString.toHexString(checksum);}
    public String getLongId() {return longId;}

    // Implement the FileCache.KeySet interface
    //
    public int  numSets() {return NUMBER_OF_KEYSETS;}
    public Object key(int i)
    {
        if (i == CONTENT_ID_KEY) return longId;
        if (i == CHECKSUM_KEY) return checksum;
        else return null;
    }

} // class ContentHeader

package com.broadon.ccs.util;

import java.math.BigDecimal;
import java.math.BigInteger;

public class ContentLongId {
    /*
     * This class is used to store the 64-bit titleId and 32-bit contentID in HEX string,
     * which combines to provides the 96-bit contentID.
     * Convenient utility methods are also provided.
     */
    private String titleId;
    private String contentId;
    
    public ContentLongId(String titleId, String contentId) {
        this.titleId = titleId;
        this.contentId = contentId;
    }
    
    public ContentLongId(BigDecimal longId) {
        String titleId = "0000000000000000";
        String contentId="00000000";
        String idStr = longId.toBigInteger().toString(ContentDownloadUtil.HEX);
        int pos = idStr.length() - ContentDownloadUtil.INT_LENGTH_IN_HEX;
        if (pos > 0) {
            this.titleId = titleId.substring(0, ContentDownloadUtil.LONG_LENGTH_IN_HEX-pos)+idStr.substring(0, pos);
            this.contentId = idStr.substring(pos);
        } else {
            this.titleId = titleId;
            this.contentId = contentId.substring(0, -pos)+idStr;
        }
            
    }
    
    public BigInteger toBigInteger() {
        return new BigInteger(titleId+contentId, ContentDownloadUtil.HEX);
    }
    
    public long getContentId() {
        return Long.parseLong(contentId, ContentDownloadUtil.HEX);
    }
    
    public String getTitleId() {
        return titleId;
    }
    
    public String toString() {
        return titleId+contentId;
    }
    
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (! (obj instanceof ContentLongId))
            return false;
        ContentLongId id = (ContentLongId) obj;
        if (this.titleId.equalsIgnoreCase(id.titleId))
            return this.contentId.equalsIgnoreCase(id.contentId);
        return false;
    }
    
    public int hashCode() {
        return titleId.hashCode();
    }
    
}

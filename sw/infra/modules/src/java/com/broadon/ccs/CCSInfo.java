package com.broadon.ccs;

import java.util.Map;
import javax.sql.DataSource;

import com.broadon.util.Queue;

/**
 * The <code>CCSInfo</code> abstract class contains the necessary information
 * for its subclasses to process CCS requests.
 *
 * @version	$Revision: 1.3 $
 */
public abstract class CCSInfo
{
    protected Map		nameMap;
    protected Map		reverseNameMap;
    protected DataSource	dataSource;
    protected DataSource    CASdataSource;
    protected Queue		auditLogQueue;
    protected int		regionalCenterID;

    /**
     * Constructs an empty CCSInfo type instance.
     */
    public CCSInfo()
    {
	this.nameMap = null;
	this.reverseNameMap = null;
	this.dataSource = null;
	this.auditLogQueue = null;
	this.regionalCenterID = -1;
    }

    /**
     * Returns the name map used by this instance.
     *
     * @return	The name map.
     */
    public final Map getNameMap()
    {
	return nameMap;
    }

    /**
     * Sets the name map to be used by this instance.
     *
     * @param	nameMap			the name map
     */
    public final void setNameMap(Map nameMap)
    {
	this.nameMap = nameMap;
    }

    /**
     * Returns the reverse name map used by this instance.
     *
     * @return	The reverse name map.
     */
    public final Map getReverseNameMap()
    {
	return reverseNameMap;
    }

    /**
     * Sets the reverse name map to be used by this instance.
     *
     * @param	reverseNameMap		the reverse name map
     */
    public final void setReverseNameMap(Map reverseNameMap)
    {
	this.reverseNameMap = reverseNameMap;
    }

    /**
     * Returns the data source used by this instance.
     *
     * @return	The data source.
     */
    public final DataSource getDataSource()
    {
	return dataSource;
    }

    /**
     * Sets the data source to be used by this instance.
     *
     * @param	dataSource		the data source
     */
    public final void setCASDataSource(DataSource CASdataSource)
    {
	this.CASdataSource = CASdataSource;
    }
    
    /**
     * Returns the data source used by this instance.
     *
     * @return  The data source.
     */
    public final DataSource getCASDataSource()
    {
    return CASdataSource;
    }

    /**
     * Sets the data source to be used by this instance.
     *
     * @param   dataSource      the data source
     */
    public final void setDataSource(DataSource dataSource)
    {
    this.dataSource = dataSource;
    }

    /**
     * Returns the audit log queue used by this instance.
     *
     * @return	The audit log queue.
     */
    public final Queue getAuditLogQueue()
    {
	return auditLogQueue;
    }

    /**
     * Sets the audit log queue to be used by this instance.
     *
     * @param	auditLogQueue		the audit log queue
     */
    public final void setAuditLogQueue(Queue auditLogQueue)
    {
	this.auditLogQueue = auditLogQueue;
    }

    /**
     * Returns the regional center identifier.
     *
     * @return	The regional center identifier.
     */
    public final int getRegionalCenterID()
    {
	return regionalCenterID;
    }

    /**
     * Sets the regional center identifier.
     *
     * @param	regionalCenterID	the regional center identifier
     */
    public final void setRegionalCenterID(int regionalCenterID)
    {
	this.regionalCenterID = regionalCenterID;
    }
}

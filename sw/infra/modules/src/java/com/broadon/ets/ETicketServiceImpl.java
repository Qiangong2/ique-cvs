package com.broadon.ets;

import java.io.IOException;
import java.math.BigInteger;
import java.rmi.RemoteException;
import java.security.GeneralSecurityException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Properties;
import java.util.TreeSet;

import javax.servlet.ServletContext;
import javax.sql.DataSource;
import javax.xml.rpc.ServiceException;
import javax.xml.rpc.server.ServiceLifecycle;
import javax.xml.rpc.server.ServletEndpointContext;

import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.logging.Log;

import com.broadon.ets.SignerInfoCache.Value;
import com.broadon.exception.BroadOnException;
import com.broadon.see.SecurityModule;
import com.broadon.servlet.ContextStatus;
import com.broadon.servlet.ServletConstants;
import com.broadon.util.BackingStoreException;
import com.broadon.util.Base64;
import com.broadon.util.Pair;
import com.broadon.wsapi.ets.ETicketPackageType;
import com.broadon.wsapi.ets.GetTicketsRequestType;
import com.broadon.wsapi.ets.GetTicketsResponseType;
import com.broadon.wsapi.ets.TicketsRequestType;
import com.ncipher.nfast.NFException;

public class ETicketServiceImpl implements ServiceLifecycle, ServletConstants
{
    private static final String CRL_INFO =
        "SELECT CRL_TYPE, CRL_VERSION FROM CURRENT_CRLS WHERE CRL_TYPE IN ('XSCRL', 'CACRL')";
 
    
    ServletContext servletContext;

    DataSource dataSource;
    
    SecurityModule sm;

    private ContentKeyCache contentKeyCache;
    private SignerInfoCache signerInfoCache;

    private int signerlCrlVersion = 0;
    private int caCrlVersion = 0;

    protected static Log log = LogFactory.getLog(ETicketServiceImpl.class.getName());

    // fetch the CRL versions.  These values never change.
    void initCrlVersion() throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = dataSource.getConnection();
            ps = conn.prepareStatement(CRL_INFO);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String type = rs.getString("CRL_TYPE");
                int version = rs.getInt("CRL_VERSION");
                if ("XSCRL".equals(type)) {
                    signerlCrlVersion = version;
                } else
                    caCrlVersion  = version;
            }
            rs.close();
        } finally {
            try {
                if (ps != null)
                    ps.close();
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {}
        }
    }
    
    
    private void wrapServiceException(String msg, Exception e) throws ServiceException
    {
        log.error(e.getMessage(), e);
        ServiceException se = new ServiceException(msg, e);
        se.setStackTrace(e.getStackTrace());
        throw se;
    }


    public void init(Object context) throws ServiceException
    {
        log.debug("eTicket Service Init");
        ServletEndpointContext soapContext = (ServletEndpointContext) context;
        servletContext = soapContext.getServletContext();

        // Data source already set up by OracleDSListener
        dataSource = (DataSource) servletContext.getAttribute(DATA_SOURCE_KEY);
        
        try {
            sm = SecurityModule.getInstance((Properties) servletContext.getAttribute(PROPERTY_KEY));
            contentKeyCache = new ContentKeyCache(dataSource);
            signerInfoCache = new SignerInfoCache(dataSource);
            initCrlVersion();
            Class.forName("com.broadon.ets.StatusCode");
        } catch (GeneralSecurityException e) {
            wrapServiceException("Security module initialization error", e);
        } catch (NFException e) {
            wrapServiceException("Security module initialization error", e);
        } catch (SQLException e) {
            wrapServiceException("Error reading CRL versions", e);
        } catch (ClassNotFoundException e) {
            wrapServiceException("Cannot load status code table", e);
        }
        
        // load the JNI library
        System.load(System.getProperty("catalina.base") + "/webapps/ets/ETicket.so");
    }

    
    public void destroy()
    {
        log.debug("eTicket Service shut down.");
    }

    protected GetTicketsResponseType getTickets(GetTicketsRequestType req)
        throws java.rmi.RemoteException
    {
        GetTicketsResponseType res = new GetTicketsResponseType(0, null, null, null, null);
        res.setDeviceId(req.getDeviceId());
        res.setVersion(req.getVersion());
        res.setMessageId(req.getMessageId());
        
        TicketsRequestType[] etreq = req.getTicketsRequest();
        if (etreq != null && etreq.length > 0) {
            long deviceID = Long.parseLong(req.getDeviceId()) & 0xFFFFFFFFL;
            
            ETicket tickGenerator = new ETicket(sm, deviceID, req.getDeviceCertificate());
            ETicketPackageType etPkg = new ETicketPackageType();
            etPkg.setETicket(new byte[etreq.length][]);
            TreeSet certChains = new TreeSet();
            
            for (int i = 0; i < etreq.length; i++) {
                SignerInfoCache.Value signerInfo;
                try {
                    signerInfo = (Value) 
                        signerInfoCache.get(new Pair(req.getRequesterId(), 
                                                     etreq[i].getTitleCategory()));
                } catch (BackingStoreException e) {
                    setErrorCode(res, etreq[i].getTitleId(), StatusCode.ETS_NO_PERMISSION, e);
                    return res;
                }
                
                byte[] titleKey;
                boolean personalized = true;
                try {
                    Pair keyInfo = (Pair) contentKeyCache.get(new BigInteger(etreq[i].getContentId(), 16));
                    titleKey = (byte[]) keyInfo.first;
                    ETicketAttributes attr = (ETicketAttributes) keyInfo.second;
                    Boolean requestCommonTicket = etreq[i].getCommonTicket();
                    if (requestCommonTicket != null && requestCommonTicket.booleanValue()) {
                        if (attr.AllowCommonTicket())
                            personalized = false;
                        else {
                            setErrorCode(res, etreq[i].getTitleId(), StatusCode.ETS_NO_PERMISSION,
                                         new BroadOnException("Non-personalized ticket not allowed."));      
                            return res;
                        }
                    }
                } catch (BackingStoreException e) {
                    setErrorCode(res, etreq[i].getTitleId(), StatusCode.ETS_BAD_CONTENT_ID, e);
                    return res;
                } catch (NumberFormatException e) {
                    setErrorCode(res, etreq[i].getTitleId(), StatusCode.ETS_BAD_CONTENT_ID, e);
                    return res;
                }
                
                byte[] tick;
                try {
                    tick = tickGenerator.generateTicket(signerInfo.signerName, 
                                                        signerInfo.caName,
                                                        caCrlVersion, 
                                                        signerlCrlVersion, 
                                                        signerInfo.keyId, 
                                                        etreq[i], 
                                                        null, 
                                                        titleKey,
                                                        personalized);
                } catch (GeneralSecurityException e) {
                    setErrorCode(res, etreq[i].getTitleId(), StatusCode.ETS_BAD_ETICKET, e);
                    return res;
                } catch (NFException e) {
                    setErrorCode(res, etreq[i].getTitleId(), StatusCode.ETS_HSM_ERROR, e);
                    return res;
                } catch (BroadOnException e) {
                    setErrorCode(res, etreq[i].getTitleId(), StatusCode.ETS_NO_PERMISSION, e);
                    return res;
                } 
              
                etPkg.setETicket(i, tick);
                certChains.add(signerInfo.caCert);
                certChains.add(signerInfo.signerCert);
            }
               
            byte[][] chains = new byte[certChains.size()][];
            Base64 base64 = new Base64();
            int i = 0;
            
            try {
                for (Iterator iter = certChains.iterator(); iter.hasNext();) {
                    String element = (String) iter.next();
                    chains[i++] = base64.decode(element);
                }
            } catch (IOException e) {
                setErrorCode(res, null, StatusCode.ETS_CERTIFICATE_ERROR, e);
                return res;
            }
            etPkg.setCert(chains);
            res.setETicketPackage(etPkg);
         }
        
        return res;
    }
    
    
    static final String DB_TEST = "SELECT 1 FROM DUAL";
    static final String QUICK_CHECK_OK = "OK";
    static final String FULL_CHECK_OK = "FULL PASS";
    
    protected String getHealth() throws java.rmi.RemoteException
    {
        ContextStatus status = (ContextStatus) servletContext.getAttribute(MONITOR_STATUS_KEY);
        
        switch (status.nextAction()) {
        
        case ContextStatus.ACT_DO_NOTHING:
            // no need to update health status, cause no work is done.
            return QUICK_CHECK_OK;
            
        case ContextStatus.ACT_CHECK:
            try {
                // check HSM connection
                sm.getRandom(1);
                
                // check DB connection using the most common action
                contentKeyCache.get(new BigInteger("0"));
            } catch (BackingStoreException e) {
                if (e.getCause() instanceof SQLException) {
                    break;
                }
            } catch (Exception e) {
                break;
            }
            
            status.setHealth(true);
            return FULL_CHECK_OK;
            
        case ContextStatus.ACT_ERROR:
        default:    
            break;
        }
        
        status.setHealth(false);
        throw new RemoteException();
    }

    private void setErrorCode(GetTicketsResponseType res, String titleID, 
                              String errCode, Exception e)
    {
        Throwable cause = e.getCause();
        if (cause != null && cause instanceof SQLException) {
            errCode = StatusCode.SC_SQL_EXCEPTION;
            e = (Exception) cause;
        }
        String errMsg = StatusCode.getMessage(errCode);
        log.error(errMsg, e);  
        res.setErrorCode(Integer.parseInt(errCode));
        res.setErrorMessage(errMsg);
        res.setErrorTitleId(titleID);
    }
}

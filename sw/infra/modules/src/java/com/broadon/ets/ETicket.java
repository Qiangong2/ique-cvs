package com.broadon.ets;

import java.security.GeneralSecurityException;
import java.security.InvalidParameterException;

import com.broadon.exception.BroadOnException;
import com.broadon.see.SecurityModule;
import com.broadon.util.Pair;
import com.broadon.wsapi.ets.TicketAttributeType;
import com.broadon.wsapi.ets.TicketsRequestType;
import com.ncipher.nfast.NFException;

public class ETicket
{
    // Native code interface

    private static native void getTicketParams(int[] params);

    private static native boolean genKey(byte[] seed, byte[] privKey, byte[] pubKey);

    private static native byte[] issueTicket(long ticketID, long deviceID,
                                             long titleID, byte[] contentIndexMask, 
                                             String[] limitCode, long[] limitValue, 
                                             long caCrlVersion, long signerCrlVersion,
                                             byte[] customData, byte[] titleKey, 
                                             byte[] serverPubEccKey,
                                             String signerName, String caName);
    
    private static native byte[] issueTicket(byte[] ticketTemplate, long ticketID, long deviceID,
                                             long titleID, long caCrlVersion, long signerCrlVersion,
                                             byte[] titleKey, byte[] serverPubEccKey,
                                             String signerName, String caName);
    
    private static native boolean encryptTicket(byte[] ticket, byte[] serverPrivEccKey,
						byte[] devicePubEccKey);
    
    private static native byte[] getPublicKey(byte[] cert);
 
    
    final long deviceID;
    final byte[] devicePubEccKey;
        
    static byte[] serverPrivKey;
    static byte[] serverPubKey;
    static int serverKeyUseCount;
    static final int MAX_SERVER_KEY_USE = 100;  // use the server key at most 100 times.
    static final int eccKeyRandSize;
    static final int eccPrivKeySize;
    static final int eccPubKeySize;
    
    static final int signatureSize;
    static final int signatureOffset;
    static final int documentOffset;            // offset to the ticket where signature applies
    static final int contentMaskSize;
    static final int limitSize;
    static final int titleKeySize;
    
    static final Object syncJNI;                // used for serialized JNI calls
    
    SecurityModule sm;
    
    static {
        syncJNI = new Object();
                
        int[] params = new int[9];
	getTicketParams(params);
        
	int i = 0;
        eccKeyRandSize = params[i++];
        eccPrivKeySize = params[i++];
        eccPubKeySize = params[i++];
        signatureSize = params[i++];
        signatureOffset = params[i++];
        documentOffset = params[i++];
        contentMaskSize = params[i++];
        limitSize = params[i++];
        titleKeySize = params[i++];
        
        serverPrivKey = null;
        serverPubKey = null;
        serverKeyUseCount = 0;	
    }
    

    Pair getServerKey() throws GeneralSecurityException, NFException {
        synchronized (syncJNI) {
            if (serverKeyUseCount <= 0) {
                byte[] privKey = new byte[eccPrivKeySize];
                byte[] pubKey = new byte[eccPubKeySize];
                if (!genKey(sm.getRandom(eccKeyRandSize), privKey, pubKey))
                    throw new GeneralSecurityException("Failed to generate server keys");
                serverKeyUseCount = MAX_SERVER_KEY_USE;
                serverPrivKey = privKey;
                serverPubKey = pubKey;
            }
            --serverKeyUseCount;
            return new Pair(serverPrivKey, serverPubKey);
        }
    } // getServerKey
    
    
    /**
     * Create an Eticket instance.  Parameters are those common to all tickets for this request.
     * @param sm Security Module used for random number generation and digital signature.
     * @param deviceID Device ID
     * @param deviceCert Certificate of the device.
     */
    public ETicket(SecurityModule sm, long deviceID, byte[] deviceCert)        
    {
        this.sm = sm;
        this.deviceID = deviceID;
        
        if (deviceCert != null) {
            devicePubEccKey = getPublicKey(deviceCert);        
            if (devicePubEccKey == null || devicePubEccKey.length != eccPubKeySize)
                throw new InvalidParameterException("Invalid device key length");
        } else
            devicePubEccKey = null;
    }
    
    
    /**
     * @param signerName Common name of the signer cert.
     * @param caName Common name of the CA cert
     * @param caCrlVersion CA revocation list version
     * @param signerCrlVersion Signer cert revocation list version
     * @param signer Index to the key that is used to sign this ticket.
     * @param req Ticket request.
     * @param customData Reserved private data from publisher.
     * @param titleKey Decryption key for this title.
     * @param personalized True if generate a persoanlized ticket.
     * @return an eTicket
     * @throws GeneralSecurityException  Non-fatal error in generating an ticket
     * @throws NFException Fatal error (e.g., HSM failure) in generating an ticket
     * @throws BroadOnException When device cert. is missing
     */
    public byte[] generateTicket(String signerName, String caName, long caCrlVersion,
                                 long signerCrlVersion, String signer, TicketsRequestType req,
                                 byte[] customData, byte[] titleKey, boolean personalized) 
        throws GeneralSecurityException, NFException, BroadOnException
    {
        if (titleKey == null || titleKey.length != titleKeySize)
            throw new GeneralSecurityException("Invalid title key");
        
        TicketAttributeType[] limits = req.getLimits();
        if ((limits != null && limits.length > limitSize) || 
            req.getContentMask().length > contentMaskSize)
            throw new InvalidParameterException("Invalid request parameters");
        
        String[] limitCode = null;
        long[] limitValue = null;
        
        if (limits != null) {
            limitCode = new String[limits.length];
            limitValue = new long[limits.length];
            for (int i = 0; i < limits.length; i++) {
                limitCode[i] = limits[i].getLimitKind();
                limitValue[i] = limits[i].getLimits();
            }
        }
        
        
        Pair serverKey = getServerKey();
        byte[] ticket;
        
        synchronized (syncJNI) {
            // HACK HACK HACK
            // We have to prefix "Root-" to the caName because of an
            // inconsistency in the
            // client cert naming convention, where the issuer of a cert is
            // *NOT* the subject name of
            // the issuer cert, but instead is a concatination of the subject
            // name of the full chain.
            // To avoid unnecessary traversal of the chain, we just add "Root-"
            // to the caName, assuming that
            // there will never be another layer between the Root and the CA, or
            // between the CA and the singer.
          
            ticket = issueTicket(req.getTicketId(), deviceID, Long.parseLong(req.getTitleId(), 16), 
                                 req.getContentMask(), limitCode, limitValue,
                                 caCrlVersion, signerCrlVersion, customData, titleKey, 
                                 (byte[]) serverKey.second, signerName, "Root-" + caName);
        }
       
        ticket = signTicket(signer, ticket);
               
        if (! personalized)
            return ticket;
        
        if (devicePubEccKey == null)
            throw new BroadOnException("Missing device certificate"); 
           
        synchronized (syncJNI) {
            if (! encryptTicket(ticket, (byte[]) serverKey.first, devicePubEccKey))
                throw new GeneralSecurityException("Error encrypting eTicket");
        }
        return ticket;        
    }
    
    /**
     * Version use only by off-line licensing server to generate common tickets.
     * @param ticketTemplate
     * @param signerName
     * @param caName
     * @param caCrlVersion
     * @param signerCrlVersion
     * @param signer
     * @param ticketId
     * @param titleId
     * @param titleKey
     * @return
     * @throws GeneralSecurityException
     * @throws NFException
     */
    public byte[] generateTicket(byte[] ticketTemplate, String signerName, String caName, 
                                 long caCrlVersion, long signerCrlVersion, String signer, 
                                 long ticketId, long titleId, byte[] titleKey) 
        throws GeneralSecurityException, NFException
    {
        if (titleKey == null || titleKey.length != titleKeySize)
            throw new GeneralSecurityException("Invalid title key");
        
        Pair serverKey = getServerKey();
        byte[] ticket;
        
        synchronized (syncJNI) {
            // HACK HACK HACK
            // We have to prefix "Root-" to the caName because of an
            // inconsistency in the
            // client cert naming convention, where the issuer of a cert is
            // *NOT* the subject name of
            // the issuer cert, but instead is a concatination of the subject
            // name of the full chain.
            // To avoid unnecessary traversal of the chain, we just add "Root-"
            // to the caName, assuming that
            // there will never be another layer between the Root and the CA, or
            // between the CA and the singer.
          
            ticket = issueTicket(ticketTemplate, ticketId, 0, titleId, caCrlVersion, 
                                 signerCrlVersion, titleKey, (byte[]) serverKey.second, 
                                 signerName, "Root-" + caName);
        }
        
        return signTicket(signer, ticket);
    }

    private byte[] signTicket(String signer, byte[] ticket) 
        throws GeneralSecurityException, NFException
    {
        if (ticket == null || ticket.length <= documentOffset)
            throw new GeneralSecurityException("Error generating eTicket");
        
        return sm.signTicket(signer, ticket, signatureOffset, documentOffset, signatureSize);
    }
    
    
/*    
    public static void main(String[] args) {
        try {
            Properties prop = new Properties();
            prop.setProperty(Keywords.USE_SSM_KEY, "true");
            prop.setProperty(Keywords.KEY_FILE_LIST, "server-key.pem");
            SecurityModule sm = SecurityModule.getInstance(prop);
            
            byte[] devicePrivKey = new byte[eccPrivKeySize];
            byte[] devicePubKey = new byte[eccPubKeySize];
            if (! genKey(sm.getRandom(eccKeyRandSize), devicePrivKey, devicePubKey)) {
                System.out.println("Can create ECC keys");
            }
            
            System.out.println("Device Private Key = " + new BigInteger(1, devicePrivKey).toString(16));
            System.out.println("Device Public Key = " + new BigInteger(1, devicePubKey).toString(16));
            
            ETicket et = new ETicket(sm, 1, devicePubKey, "signer", "CA", 2, 3);
            
            byte[] contentMask = new byte[0];
            TicketsRequestType req = new TicketsRequestType(1, 1, 1, contentMask, null, TitleCategoryType.GAME);
            byte[] ticket = et.generateTicket("server", req, null, sm.getRandom(16));
            
            FileOutputStream out = new FileOutputStream("ticket");
            out.write(ticket);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
  */  
}

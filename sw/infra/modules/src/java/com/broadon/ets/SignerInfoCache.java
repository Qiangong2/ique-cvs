package com.broadon.ets;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.broadon.db.DataSourceScheduler;
import com.broadon.db.OracleDataSource.OracleDataSourceProxy;
import com.broadon.util.BackingStore;
import com.broadon.util.BackingStoreException;
import com.broadon.util.CacheLFU;
import com.broadon.util.Pair;

public class SignerInfoCache extends CacheLFU implements BackingStore
{
    private static final String KEY_INFO =
        "SELECT CERTIFICATE, CN, C.KEY_NAME, " +
        "       DECODE(A.CERT_ID, SIGNER_CERT_ID, 'SIGNER', 'CA') AS CERT_TYPE " +
        "FROM CERTIFICATES A, CERTIFICATE_CHAINS B, CLIENT_HSM_CERTS C " +
        "WHERE EXISTS (SELECT 1 FROM CERTIFICATE_CHAINS D " +
        "              WHERE A.CERT_ID IN (D.SIGNER_CERT_ID, D.CA_CERT_ID) AND " +
        "                    D.CHAIN_ID = B.CHAIN_ID) AND " +
        "      B.CHAIN_ID = C.CHAIN_ID AND C.CERT_ID = ? AND C.KEY_TYPE = ?";
    
    private DataSource ds;

    public SignerInfoCache(DataSource ds)
    {
        super(100, -1);
        this.ds = ds;
    }

    public Object retrieve(Object signerKey) throws BackingStoreException
    {
        String certID = (String) ((Pair)signerKey).first;
    
        String titleCategory = (String) ((Pair)signerKey).second;
        
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        String signerName = null;
        String signerCert = null;
        String caName = null;
        String caCert = null;
        String keyId = null;
        
        try {
            conn = getConnection(DataSourceScheduler.DB_PRIORITY_FAST);
            ps = conn.prepareStatement(KEY_INFO);
            ps.setString(1, certID);
            ps.setString(2, titleCategory.toUpperCase());
            rs = ps.executeQuery();
            while (rs.next()) {
                String cert = rs.getString("CERTIFICATE");
                String name = rs.getString("CN");
                String type = rs.getString("CERT_TYPE");
                if ("SIGNER".equals(type)) {
                    signerCert = cert;
                    signerName = name;
                    keyId = rs.getString("KEY_NAME");
                } else {
                    caCert = cert;
                    caName = name;
                } 
            }
        } catch (SQLException e) {
            throw BackingStoreException.rewrap(e);
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (ps != null)
                    ps.close();
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {}
        }
        
        return new Value(signerCert, signerName, caCert, caName, keyId);
    }
    
    private Connection getConnection(int priority) throws SQLException
    {
        return ds instanceof OracleDataSourceProxy ? 
               ((OracleDataSourceProxy) ds).getConnection(priority) :
               ds.getConnection();
    }
    
    public static class Value
    {
        public final String signerName;
        public final String caName;
        public final String signerCert;
        public final String caCert;
        public final String keyId;
        
        public Value(String scert, String sname, String ccert, String cname, String id) 
            throws BackingStoreException {
            
            if (scert == null || sname == null || ccert == null || cname == null || id == null)
                throw new BackingStoreException("Client has no permission to issue eTickets for given device.");
            caCert = ccert;
            caName = cname;
            signerCert = scert;
            signerName = sname;
            this.keyId = id;
        }      
    }
}

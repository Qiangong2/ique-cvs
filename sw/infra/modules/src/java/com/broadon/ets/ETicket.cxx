#include <string.h>
#include <netinet/in.h>

#include "x86/esl.h"
#include "es_int.h"
#include "ETicket.h"

static bool swapContentMask = true;

// test if the libesl library uses the same byte and bit ordering for
// storing the content bit mask as on the Java side.  If so, set the static
// flag "swapContentMask" to indicate that we don't need to swap the bits. 
static void
test_content_mask()
{
    ESCidxMask mask;
    memset(&mask, 0, sizeof(mask));
    ES_SetMaskBits(mask, 0, 1);
    swapContentMask = ! (mask[0] == 0x1);
} // test_content_mask


// pass predefined parameterss from "C" headers to "Java".
JNIEXPORT void JNICALL
Java_com_broadon_ets_ETicket_getTicketParams(JNIEnv* env, jclass,
					     jintArray _params)
{
    jint params[] = {
	ES_ECC_RANDN_SIZE,
	ES_ECC_PVT_KEY_SIZE,
	ES_ECC_PUB_KEY_SIZE,
	ES_RSA2048_PUB_KEY_SIZE,
	ES_SIGNATURE_OFFSET,
	ES_SIGNATURE_OFFSET + ES_RSA2048_PUB_KEY_SIZE + ES_SIG_PAD_SIZE,
	sizeof(ESCidxMask),
	ES_MAX_LIMIT_TYPE,
	ES_AES_KEY_SIZE
    };

    env->SetIntArrayRegion(_params, 0, sizeof(params)/sizeof(jint), params);

    test_content_mask();
} // getTicketParams


JNIEXPORT jboolean JNICALL
Java_com_broadon_ets_ETicket_genKey(JNIEnv* env, jclass, jbyteArray _seed,
				    jbyteArray _privKey, jbyteArray _pubKey)
{
    u8 seed[ES_ECC_RANDN_SIZE];
    u8 privKey[ES_ECC_PVT_KEY_SIZE];
    u8 pubKey[ES_ECC_PUB_KEY_SIZE];

    env->GetByteArrayRegion(_seed, 0, sizeof(seed), reinterpret_cast<jbyte*>(seed));

    if (ES_GenerateEccKeyPair(seed, privKey, pubKey) != ES_ERR_OK)
	return JNI_FALSE;

    env->SetByteArrayRegion(_privKey, 0, sizeof(privKey),
			    reinterpret_cast<jbyte*>(privKey));
    env->SetByteArrayRegion(_pubKey, 0, sizeof(pubKey),
			    reinterpret_cast<jbyte*>(pubKey));
    return JNI_TRUE;
} // genKey


static bool
setContentMask(ESCidxMask& mask, JNIEnv* env, jbyteArray _contentIndexMask)
{
    unsigned int len = env->GetArrayLength(_contentIndexMask);
    if (len > sizeof(ESCidxMask))
	return false;

    memset(&mask, 0, sizeof(mask));
    if (! swapContentMask) {
	env->GetByteArrayRegion(_contentIndexMask, 0, len,
				reinterpret_cast<jbyte*>(mask));
	return true;
    }

    // else, we need to set the bits using libesl's API
    u8 _mask[len];

    env->GetByteArrayRegion(_contentIndexMask, 0, len,
			    reinterpret_cast<jbyte*>(_mask));

    for (unsigned int i = 0; i < len; ++i) {
	switch (_mask[i]) {
	case 0xff:
	    ES_SetMaskBits(mask, i * 8, 8);
	    break;

	case 0:
	    break;

	default:
	    for (int j = 0; j < 8; ++j) {
		if (_mask[i] & (1 << j))
		    ES_SetMaskBits(mask, i * 8 + j, 1);
	    }
	    break;
	}
    }

    return true;
} // setContentMask


static bool
setLimits(ESTicketView& view, JNIEnv* env, jobjectArray _codes, jlongArray _values)
{
    if (_codes == NULL || _values == NULL)
	return true;

    int len = env->GetArrayLength(_codes);
    if (len != env->GetArrayLength(_values))
	return false;

    jlong values[len];

    env->GetLongArrayRegion(_values, 0, len, values);

    bool result = true;
    for (int i = 0; i < len && result; ++i) {
	jstring codes =
	    static_cast<jstring>(env->GetObjectArrayElement(_codes, i));
	const char* codeName = env->GetStringUTFChars(codes, NULL);

	if (codeName && codeName[2] == 0 && (codeName[1] == 'R' ||
					     codeName[1] == 'r')) {
	    switch(codeName[0]) {
	    case 'T':
	    case 't':
		view.limits[i].code = ES_LC_DURATION_TIME;
		break;

	    case 'S':
	    case 's':
		view.limits[i].code = ES_LC_NUM_TITLES;
		break;

	    case 'P':
	    case 'p':
		// no special code for permanent rights
		view.limits[i].code = 0; 
		break;

	    case 'D':
	    case 'd':
		view.limits[i].code = ES_LC_ABSOLUTE_TIME;
		break;

	    default:
		result = false;
		break;
	    }

	    
	    view.limits[i].limit = values[i];
	} else {
	    result = false;
	}
	
	env->ReleaseStringUTFChars(codes, codeName);
	env->DeleteLocalRef(codes);
    }

    return result;
}


static jbyteArray
generateTicket(JNIEnv* env, ESTicketView* tkView, jlong _caCrlVersion,
	       jlong _signerCrlVersion, jbyteArray _titleKey,
	       jbyteArray _serverPubEccKey, jstring _signerName,
	       jstring _caName)
{
    u8 titleKey[ES_AES_KEY_SIZE];
    u8 serverPubEccKey[ES_ECC_PUB_KEY_SIZE];
    char signerName[ES_NAME_SIZE];
    char caName[ES_NAME_SIZE];
    u8 ticket[ES_TICKET_SIZE];

    env->GetByteArrayRegion(_titleKey, 0, sizeof(titleKey),
			    reinterpret_cast<jbyte*>(titleKey));
    env->GetByteArrayRegion(_serverPubEccKey, 0, sizeof(serverPubEccKey),
			    reinterpret_cast<jbyte*>(serverPubEccKey));
    env->GetStringUTFRegion(_signerName, 0, env->GetStringLength(_signerName), signerName);
    env->GetStringUTFRegion(_caName, 0, env->GetStringLength(_caName), caName);


    if (ES_GenerateUnsignedCommonTicket(tkView, _caCrlVersion, _signerCrlVersion,
					titleKey, serverPubEccKey, signerName,
					caName, ticket) != ES_ERR_OK)
	return NULL;

    jbyteArray result = env->NewByteArray(sizeof(ticket));
    if (result) 
	env->SetByteArrayRegion(result, 0, sizeof(ticket),
				reinterpret_cast<jbyte*>(ticket));
    return result;
} 



#define issueTicket Java_com_broadon_ets_ETicket_issueTicket__JJJ_3B_3Ljava_lang_String_2_3JJJ_3B_3B_3BLjava_lang_String_2Ljava_lang_String_2


JNIEXPORT jbyteArray JNICALL
issueTicket(JNIEnv* env, jclass,
	    jlong _ticketID,
	    jlong _deviceID,
	    jlong _titleID,
	    jbyteArray _contentIndexMask,
	    jobjectArray _limitCode,
	    jlongArray _limitValue,
	    jlong _caCrlVersion,
	    jlong _signerCrlVersion,
	    jbyteArray _customData,
	    jbyteArray _titleKey,
	    jbyteArray _serverPubEccKey,
	    jstring _signerName,
	    jstring _caName)
{
    ESTicketView tkView;

    memset(&tkView, 0, sizeof(tkView));

    tkView.ticketId = _ticketID;
    tkView.deviceId = _deviceID;
    tkView.titleId = _titleID;

    if (! setContentMask(tkView.cidxMask, env, _contentIndexMask))
	return NULL;

    if (! setLimits(tkView, env, _limitCode, _limitValue))
	return NULL;

    if (_customData != NULL)
	env->GetByteArrayRegion(_customData, 0, sizeof(tkView.reserved),
				reinterpret_cast<jbyte*>(tkView.reserved));

    return generateTicket(env, &tkView, _caCrlVersion, _signerCrlVersion,
			  _titleKey, _serverPubEccKey, _signerName, _caName);
    
} // issueTicket


#define issueTicketFromTemplate Java_com_broadon_ets_ETicket_issueTicket___3BJJJJJ_3B_3BLjava_lang_String_2Ljava_lang_String_2

JNIEXPORT jbyteArray JNICALL
issueTicketFromTemplate(JNIEnv* env, jclass,
			jbyteArray _ticketTemplate,
			jlong _ticketID,
			jlong _deviceID,
			jlong _titleID,
			jlong _caCrlVersion,
			jlong _signerCrlVersion,
			jbyteArray _titleKey,
			jbyteArray _serverPubEccKey,
			jstring _signerName,
			jstring _caName)
{
    ESTicketView tkView;

    memset(&tkView, 0, sizeof(tkView));

    tkView.ticketId = _ticketID;
    tkView.deviceId = _deviceID;
    tkView.titleId = _titleID;

    ESTicket ticket;
    if (sizeof(ticket) != env->GetArrayLength(_ticketTemplate))
	return NULL;

    env->GetByteArrayRegion(_ticketTemplate, 0, sizeof(ticket),
			    reinterpret_cast<jbyte*>(&ticket));

    if (ticket.version != ES_TICKET_VERSION)
	return NULL;

    memcpy(tkView.cidxMask, ticket.cidxMask, sizeof(ESCidxMask));

    // need to swap the bytes of limits
    for (int i = 0; i < ES_MAX_LIMIT_TYPE; ++i) {
	tkView.limits[i].code = ntohl(ticket.limits[i].code);
	tkView.limits[i].limit = ntohl(ticket.limits[i].limit);
    }
    
    memcpy(tkView.reserved, ticket.reserved, sizeof(tkView.reserved));
    memcpy(tkView.sysAccessMask, ticket.sysAccessMask, sizeof(tkView.sysAccessMask));

    return generateTicket(env, &tkView, _caCrlVersion, _signerCrlVersion,
			  _titleKey, _serverPubEccKey, _signerName, _caName);
}


JNIEXPORT jboolean JNICALL
Java_com_broadon_ets_ETicket_encryptTicket(JNIEnv* env, jclass,
					   jbyteArray _ticket,
					   jbyteArray _serverPrivKey,
					   jbyteArray _devicePubKey)
{
    u8 serverPrivKey[ES_ECC_PVT_KEY_SIZE];
    u8 devicePubKey[ES_ECC_PUB_KEY_SIZE];

    env->GetByteArrayRegion(_serverPrivKey, 0, sizeof(serverPrivKey),
			    reinterpret_cast<jbyte*>(serverPrivKey));
    env->GetByteArrayRegion(_devicePubKey, 0, sizeof(devicePubKey),
			    reinterpret_cast<jbyte*>(devicePubKey));

    u8* ticket = reinterpret_cast<u8*>(env->GetByteArrayElements(_ticket, NULL));

    if (ticket == NULL)
	return JNI_FALSE;

    if (ES_PersonalizeSignedTicket(ticket, serverPrivKey, devicePubKey) != ES_ERR_OK) {
	env->ReleaseByteArrayElements(_ticket,
				      reinterpret_cast<jbyte*>(ticket),
				      JNI_ABORT);
	return JNI_FALSE;
    } else {
	env->ReleaseByteArrayElements(_ticket,
				      reinterpret_cast<jbyte*>(ticket),
				      0);
	return JNI_TRUE;
    }
} // encryptTicket


JNIEXPORT jbyteArray JNICALL
Java_com_broadon_ets_ETicket_getPublicKey(JNIEnv* env, jclass,
					  jbyteArray _deviceCert)
{
    if (_deviceCert == NULL)
	return NULL;
    
    jbyte* deviceCert = env->GetByteArrayElements(_deviceCert, NULL);
    if (deviceCert == NULL)
	return NULL;
    u32 size;
    u8* pubKey = ES_GetCertPubKey(deviceCert, &size);
    env->ReleaseByteArrayElements(_deviceCert, deviceCert, JNI_ABORT);

    if (pubKey == NULL || size == 0)
	return NULL;

    jbyteArray result = env->NewByteArray(size);
    if (result)
	env->SetByteArrayRegion(result, 0, size,
				reinterpret_cast<jbyte*>(pubKey));
    return result;
}

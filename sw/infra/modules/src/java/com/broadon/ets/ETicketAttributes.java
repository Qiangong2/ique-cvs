package com.broadon.ets;

public class ETicketAttributes
{
    static final int ALLOW_COMMON_TICKET = 0x1;
    
    private int value;
    
    public ETicketAttributes(int value) {
        this.value = value;
    }
    
    public int getAtributes() {
        return value;
    }
    
    private void setFlag(int attr, boolean flag)
    {
        if (flag)
            value |= attr;
        else
            value &= ~attr;
    }

    public void setAllowCommonTicket(boolean flag) {
        setFlag(ALLOW_COMMON_TICKET, flag);
    }
    
    public boolean AllowCommonTicket() {
        return (value & ALLOW_COMMON_TICKET) != 0;
    }
}

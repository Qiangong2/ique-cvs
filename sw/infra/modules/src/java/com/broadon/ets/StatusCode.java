package com.broadon.ets;

public class StatusCode extends com.broadon.status.StatusCode
{
    public static final String ETS_NO_PERMISSION = "301";
    static final String ETS_NO_PERMISSION_MSG = 
        "Client has no permission to issue eTickets for given device";
    
    public static final String ETS_BAD_CONTENT_ID = "302";
    static final String ETS_BAD_CONTENT_ID_MSG = "Invalid content ID ";
    
    public static final String ETS_BAD_ETICKET = "303";
    static final String ETS_BAD_ETICKET_MSG = "eTicket generation error";
    
    public static final String ETS_HSM_ERROR = "304";
    static final String ETS_HSM_ERROR_MSG = "Error communicating with HSM";
    
    public static final String ETS_CERTIFICATE_ERROR = "305";
    static final String ETS_CERTIFICATE_ERROR_MSG = "Internal error: cannot locate certificate chain";
    
    static {
        addStatusCode(ETS_NO_PERMISSION, ETS_NO_PERMISSION_MSG);
        addStatusCode(ETS_BAD_CONTENT_ID, ETS_BAD_CONTENT_ID_MSG);
        addStatusCode(ETS_BAD_ETICKET, ETS_BAD_ETICKET_MSG);
        addStatusCode(ETS_HSM_ERROR, ETS_HSM_ERROR_MSG);
        addStatusCode(ETS_CERTIFICATE_ERROR, ETS_CERTIFICATE_ERROR_MSG);
    }

}

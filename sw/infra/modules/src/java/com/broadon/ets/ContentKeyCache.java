package com.broadon.ets;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.broadon.db.DataSourceScheduler;
import com.broadon.db.OracleDataSource.OracleDataSourceProxy;
import com.broadon.util.BackingStore;
import com.broadon.util.BackingStoreException;
import com.broadon.util.CacheLFU;
import com.broadon.util.Pair;

/**
 * Cache the content keys from the database.
 */
public class ContentKeyCache extends CacheLFU implements BackingStore
{
    private static final int MAX_ENTRIES = 5000;

    private static final String SQL_STMT = 
        "SELECT ETICKET_METADATA, ATTRIBITS FROM CONTENT_ETICKET_METADATA WHERE CONTENT_ID = ?";
    
    private DataSource ds;
    
    public ContentKeyCache(DataSource ds) {
        super(MAX_ENTRIES);
        this.ds = ds;
    }

    
    public Object retrieve(Object contentIdKey) throws BackingStoreException
    {
        BigInteger contentId = (BigInteger) contentIdKey;
        
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            conn = getConnection(DataSourceScheduler.DB_PRIORITY_FAST);
            ps = conn.prepareStatement(SQL_STMT);
            
            ps.setBigDecimal(1, new BigDecimal(contentId));
            rs = ps.executeQuery();
            Blob b = null;
            if (!rs.next() || (b = rs.getBlob(1)) == null)
                throw new BackingStoreException("Invalid content ID " + contentId.toString(16));
            int attr = rs.getInt(2);
            if (rs.wasNull())
                attr = 0;
            return new Pair(b.getBytes(1, (int) b.length()), new ETicketAttributes(attr));
        } catch (SQLException e) {
            throw BackingStoreException.rewrap(e);
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (ps != null)
                    ps.close();
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {}
        }
    }

    private Connection getConnection(int priority) throws SQLException
    {
        return ds instanceof OracleDataSourceProxy ? 
               ((OracleDataSourceProxy) ds).getConnection(priority) :
               ds.getConnection();
    }
}

package com.broadon.online;

/**
 * JSP Bean for querying and managing Content Title RoleNames
 * It has following Bean properties:
 * <ul>
 * <li> mOperationUsercount, int <br> search result count.
 * <li> mOperationUserHTML, String <br> search result  in html.
 * </ul>
 * @author	Vaibhav Bhargava
 * @version	$Revision: 1.5 $
 */
public class OperationUserBean
    extends PageBean
{
    public static final String ID = "oubean";

    private String mOperationUserHTML = null;
    private int mOperationUserCount = 0;
    private String mBusinessUnitHTML = null;
    private String mRoleNameHTML = null;

    private String mUserRoleName = "";
    private String mOpID = "";
    private String mEmail = "";
    private String mPswd = "";
    private String mName = "";
    private String mSort = null;

    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    public OperationUserBean(int pageSize)
    {
	super(pageSize);
    }

    // for a specific operator id
    public OperationUserBean(int pageSize, String operator_id, String email, String dummy)
    {
        super(pageSize);
        mOpID = operator_id;
        mEmail = email;
    }

    // for all search
    public OperationUserBean(int pageSize, String page, String sort)
    {
	super(pageSize);
	super.setPageNo(Integer.parseInt(page));
	setSort(sort);
    }

    /**
     * Bean Property: mSort
     */
    public String getSort() { return mSort; }
    public void setSort(String sort) { mSort = sort; }

    /**
     * Bean Property: mOpID
     */
    public String getOperatorID() { return mOpID; }

    /**
     * Bean Property: mEmail
     */
    public String getEmailAddress() { return mEmail; }

    /**
     * Bean Property: mOperationUserHTML
     */
    public String getOperationUserHTML() { return mOperationUserHTML; }
    public void setOperationUserHTML(String str) { mOperationUserHTML = str; }

    /**
     * Bean Property: mOperationUserCount
     */
    public int getOperationUserCount() { return mOperationUserCount; }
    public void setOperationUserCount(int count) { mOperationUserCount = count; }

    /**
     * Bean Property: mRoleNameHTML
     */
    public String getRoleNameHTML() { return mRoleNameHTML; }
    public void setRoleNameHTML(String str) { mRoleNameHTML = str; }

    /**
     * Bean Property: mBusinessUnitHTML
     */
    public String getBusinessUnitHTML() { return mBusinessUnitHTML; }
    public void setBusinessUnitHTML(String str) { mBusinessUnitHTML = str; }

    /**
     * Bean Property: mUserRoleName
     */
    public String getUserRoleName() { return mUserRoleName; }
    public void setUserRoleName(String str) { mUserRoleName = str; }

    /**
     * Bean Property: mName
     */
    public String getFullname() { return mName; }
    public void setFullname(String str) { mName = str; }

    /**
     * Bean Property: mPswd
     */
    public String getPassword() { return mPswd; }
    public void setPassword(String str) { mPswd = str; }

}

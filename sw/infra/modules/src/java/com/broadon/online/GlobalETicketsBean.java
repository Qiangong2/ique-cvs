package com.broadon.online;

/**
 * JSP Bean for querying and managing Content Title Regions
 * It has following Bean properties:
 * <ul>
 * <li> mGECount, String <br> search count  in html.
 * <li> mGEHTML, String <br> search result  in html.
 * </ul>
 * @author	Vaibhav Bhargava
 * @version	$Revision: 1.1 $
 */
public class GlobalETicketsBean
    extends PageBean
{
    public static final String ID = "gebean";

    private String mGlobalETicketsHTML = null;
    private int mGlobalETicketsCount = 0;

    private String mTitleHTML = null;
    private String mBUHTML = null;

    private String mBUID = null;
    private String mSort = null;

    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    public GlobalETicketsBean(int pageSize)
    {
	super(pageSize);
    }

    // for all search
    public GlobalETicketsBean(int pageSize, String page, String sort)
    {
	super(pageSize);
	super.setPageNo(Integer.parseInt(page));
	setSort(sort);
    }

    /**
     * Bean Property: mSort
     */
    public String getSort() { return mSort; }
    public void setSort(String sort) { mSort = sort; }

    /**
     * Bean Property: mGlobalETicketsHTML
     */
    public String getGlobalETicketsHTML() { return mGlobalETicketsHTML; }
    public void setGlobalETicketsHTML(String str) { mGlobalETicketsHTML = str; }

    /**
     * Bean Property: mGlobalETicketsCount
     */
    public int getGlobalETicketsCount() { return mGlobalETicketsCount; }
    public void setGlobalETicketsCount(int count) { mGlobalETicketsCount = count; }

    /**
     * Bean Property: mTitleHTML
     */
    public String getTitleHTML() { return mTitleHTML; }
    public void setTitleHTML(String str) { mTitleHTML = str; }

    /**
     * Bean Property: mBUHTML
     */
    public String getBUHTML() { return mBUHTML; }
    public void setBUHTML(String str) { mBUHTML = str; }

    /**
     * Bean Property: mBUID
     */
    public String getBUID() { return mBUID; }
    public void setBUID(String str) { mBUID = str; }

}

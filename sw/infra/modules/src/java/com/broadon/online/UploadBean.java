package com.broadon.online;

/**
 * JSP Bean for querying and managing uploads. 
 * It has following Bean properties:
 * <ul>
 * <li> mHTML, String <br> upload result in html.
 * </ul>
 * @author Vaibhav Bhargava
 */

public class UploadBean
{
    public static final String ID = "upbean";

    private String mHTML = null;

    public UploadBean(String str) {
	mHTML = str;
    }
    
    /**
     * Bean Property: mHTML
     */
    public String getUploadHTML() { return mHTML; }
}

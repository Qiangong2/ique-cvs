package com.broadon.online;

/**
 * JSP Bean for querying and managing ECardBatches. 
 * It has following Bean properties:
 * <ul>
 * <li> mCount, int <br> search result count.
 * <li> mHTML, String <br> search result  in html.
 * </ul>
 * @author Vaibhav Bhargava
 */

public class ECardBatchesBean
    extends PageBean
{
    public static final String ID = "ecbbean";
    
    private String mHTML = null;
    private int mCount = 0;

    private String mStart = null;
    private String mEnd = null;

    private String mSort = null;
    
    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    public ECardBatchesBean(int pageSize)
    {
        super(pageSize);
    }

    // for a specific id
    public ECardBatchesBean(int pageSize, String start, String end, String dummy)
    {
        super(pageSize);
        mStart = start;
        mEnd = end;
    }

    // for all search
    public ECardBatchesBean(int pageSize, String page, String sort)
    {
        super(pageSize);
        super.setPageNo(Integer.parseInt(page));
        setSort(sort);
    }

    /**
     * Bean Property: mStart
     */
    public String getStartECardID() { return mStart; }

    /**
     * Bean Property: mEnd
     */
    public String getEndECardID() { return mEnd; }

    /**
     * Bean Property: mSort
     */
    public String getSort() { return mSort; }
    public void setSort(String sort) { mSort = sort; }

    /**
     * Bean Property: mHTML
     */
    public String getECardBatchesHTML() { return mHTML; }
    public void setECardBatchesHTML(String str) { mHTML = str; }

    /**
     * Bean Property: mCount
     */
    public int getECardBatchesCount() { return mCount; }
    public void setECardBatchesCount(int count) { mCount = count; }
}

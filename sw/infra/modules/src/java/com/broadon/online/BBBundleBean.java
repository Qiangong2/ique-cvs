package com.broadon.online;

/**
 * JSP Bean for querying and managing BB Bundles. 
 * It has following Bean properties:
 * <ul>
 * <li> mCount, int <br> search result count.
 * <li> mHTML, String <br> search result in html.
 * </ul>
 * @author Vaibhav Bhargava
 */

public class BBBundleBean
    extends PageBean
{
    public static final String ID = "bbbbean";
    
    private String mHTML = null;
    private int mCount = 0;
    private String mBundleTitleHTML = null;
    private int mBundleTitleCount = 0;

    private String mModelHTML = null;
    private String mTitleHTML = null;
    private String mBUHTML = null;

    private String mModel = "";
    private String mBUID = "";
    private String mTitleID = "";
    private String mStart = "";
    private String mEnd = "";
    private String mSKU = "";
    private boolean mDateFlag = true;
    private String mSort = null;
    
    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    public BBBundleBean(int pageSize)
    {
        super(pageSize);
    }

    // for a specific id
    public BBBundleBean(int pageSize, String model, String buid, String start, String end, String sku)
    {
        super(pageSize);
        mModel = model;
        mBUID = buid;
        mStart = start;
        mEnd = end;
        mSKU = sku;
    }

    // for a specific id
    public BBBundleBean(int pageSize, String model, String buid, String tid, String start, String end, String sku)
    {
        super(pageSize);
        mModel = model;
        mBUID = buid;
        mTitleID = tid;
        mStart = start;
        mEnd = end;
        mSKU = sku;
    }

    // for all search
    public BBBundleBean(int pageSize, String page, String sort)
    {
        super(pageSize);
        super.setPageNo(Integer.parseInt(page));
        setSort(sort);
    }

    /**
     * Bean Property: mSort
     */
    public String getSort() { return mSort; }
    public void setSort(String sort) { mSort = sort; }

    /**
     * Bean Property: mModel
     */
    public String getBBModel() { return mModel; }

    /**
     * Bean Property: mBUID
     */
    public String getBUID() { return mBUID; }

    /**
     * Bean Property: mTitleID
     */
    public String getTitleID() { return mTitleID; }

    /**
     * Bean Property: mStart
     */
    public String getStartDate() { return mStart; }

    /**
     * Bean Property: mEnd
     */
    public String getEndDate() { return mEnd; }

    /**
     * Bean Property: mSKU
     */
    public String getSKU() { return mSKU; }

    /**
     * Bean Property: mHTML
     */
    public String getBBBundleHTML() { return mHTML; }
    public void setBBBundleHTML(String str) { mHTML = str; }

    /**
     * Bean Property: mCount
     */
    public int getBBBundleCount() { return mCount; }
    public void setBBBundleCount(int count) { mCount = count; }

    /**
     * Bean Property: mBundleTitleHTML
     */
    public String getBundleTitleHTML() { return mBundleTitleHTML; }
    public void setBundleTitleHTML(String str) { mBundleTitleHTML = str; }

    /**
     * Bean Property: mBundleTitleCount
     */
    public int getBundleTitleCount() { return mBundleTitleCount; }
    public void setBundleTitleCount(int count) { mBundleTitleCount = count; }

    /**
     * Bean Property: mModelHTML
     */
    public String getBBModelHTML() { return mModelHTML; }
    public void setBBModelHTML(String str) { mModelHTML = str; }

    /**
     * Bean Property: mTitleHTML
     */
    public String getTitleHTML() { return mTitleHTML; }
    public void setTitleHTML(String str) { mTitleHTML = str; }

    /**
     * Bean Property: mBUHTML
     */
    public String getBusinessUnitHTML() { return mBUHTML; }
    public void setBusinessUnitHTML(String str) { mBUHTML = str; }

    /**
     * Bean Property: mDateFlag
     */
    public boolean getDateFlag() { return mDateFlag; }
    public void setDateFlag(boolean flag) { mDateFlag = flag; }

}

package com.broadon.online;

/**
 * JSP Bean for querying and managing BBPlayer. 
 * It has following Bean properties:
 * <ul>
 * <li> mCount, int <br> search result count.
 * <li> mHTML, String <br> search result  in html.
 * </ul>
 * @author Vaibhav Bhargava
 */

public class BBPlayerBean
    extends PageBean
{
    public static final String ID = "bbpbean";
    
    private String mModelsHTML = null;
    private String mPurchasedTitlesHTML = null;
    private String mBundledTitlesHTML = null;
    private String mActivityHTML = null;
    private String mEBUPlayerMftrHTML = null;

    private String mID = null;
    private String mSort = null;
    private String mModel = null;
    private String mDate = null;
    private String mHTML = null;
    private String mTimeline = "";

    private int mMftrCount = 0;
    private int mActCount = 0;

    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    public BBPlayerBean(int pageSize)
    {
        super(pageSize);
    }

    // for a specific id
    public BBPlayerBean(int pageSize, String id)
    {
        super(pageSize);
        mID = id;
    }

    // for all search
    public BBPlayerBean(int pageSize, String page, String sort)
    {
        super(pageSize);
        super.setPageNo(Integer.parseInt(page));
        setSort(sort);
    }

    // for all search
    public BBPlayerBean(int pageSize, String page, String sort, String id)
    {
        super(pageSize);
        super.setPageNo(Integer.parseInt(page));
        setSort(sort);
        mID = id;
    }

    // for all search
    public BBPlayerBean(int pageSize, String page, String sort, String model, String mdate)
    {
        super(pageSize);
        super.setPageNo(Integer.parseInt(page));
        setSort(sort);
        mModel = model;
        mDate = mdate;
    }

    /**
     * Bean Property: mSort
     */
    public String getSort() { return mSort; }
    public void setSort(String sort) { mSort = sort; }
    
    /**
     * Bean Property: mID
     */
    public String getBBID() { return mID; }

    /**
     * Bean Property: mModel
     */
    public String getBBModel() { return mModel; }

    /**
     * Bean Property: mMftrDate
     */
    public String getMftrDate() { return mDate; }

    /**
     * Bean Property: mHTML
     */
    public String getBBPlayersHTML() { return mHTML; }
    public void setBBPlayersHTML(String str) { mHTML = str; }

    /**
     * Bean Property: mPurchasedTitlesHTML
     */
    public String getPurchasedTitlesHTML() { return mPurchasedTitlesHTML; }
    public void setPurchasedTitlesHTML(String str) { mPurchasedTitlesHTML = str; }

    /**
     * Bean Property: mBundledTitlesHTML
     */
    public String getBundledTitlesHTML() { return mBundledTitlesHTML; }
    public void setBundledTitlesHTML(String str) { mBundledTitlesHTML = str; }

    /**
     * Bean Property: mActivityHTML
     */
    public String getActivityHTML() { return mActivityHTML; }
    public void setActivityHTML(String str) { mActivityHTML = str; }

    /**
     * Bean Property: mActCount
     */
    public int getPlayerActivityCount() { return mActCount; }
    public void setPlayerActivityCount(int count) { mActCount = count; }

    /**
     * Bean Property: mModelsHTML
     */
    public String getBBModelsHTML() { return mModelsHTML; }
    public void setBBModelsHTML(String str) { mModelsHTML = str; }

    /**
     * Bean Property: mEBUPlayerMftrHTML
     */
    public String getEBUPlayerMftrHTML() { return mEBUPlayerMftrHTML; }
    public void setEBUPlayerMftrHTML(String str) { mEBUPlayerMftrHTML = str; }

    /**
     * Bean Property: mMftrCount
     */
    public int getEBUPlayerMftrCount() { return mMftrCount; }
    public void setEBUPlayerMftrCount(int count) { mMftrCount = count; }

    /**
     * Bean Property: mTimeline
     */
    public String getActivityTimeline() { return mTimeline; }
    public void setActivityTimeline(String str) { mTimeline = str; }

}

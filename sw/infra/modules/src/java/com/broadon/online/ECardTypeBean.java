package com.broadon.online;

/**
 * JSP Bean for querying and managing ECardTypes. 
 * It has following Bean properties:
 * <ul>
 * <li> mCount, int <br> search result count.
 * <li> mHTML, String <br> search result  in html.
 * </ul>
 * @author Vaibhav Bhargava
 */

public class ECardTypeBean
    extends PageBean
{
    public static final String ID = "ectbean";
    
    private String mHTML = null;
    private int mCount = 0;

    private String mSort = null;
    
    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    public ECardTypeBean(int pageSize)
    {
        super(pageSize);
    }

    // for all search
    public ECardTypeBean(int pageSize, String page, String sort)
    {
        super(pageSize);
        super.setPageNo(Integer.parseInt(page));
        setSort(sort);
    }

    /**
     * Bean Property: mSort
     */
    public String getSort() { return mSort; }
    public void setSort(String sort) { mSort = sort; }

    /**
     * Bean Property: mHTML
     */
    public String getECardTypesHTML() { return mHTML; }
    public void setECardTypesHTML(String str) { mHTML = str; }

    /**
     * Bean Property: mCount
     */
    public int getECardTypesCount() { return mCount; }
    public void setECardTypesCount(int count) { mCount = count; }
}

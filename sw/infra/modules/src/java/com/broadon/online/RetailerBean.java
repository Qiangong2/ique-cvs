package com.broadon.online;

/**
 * JSP Bean for querying and managing Retailers. 
 * It has following Bean properties:
 * <ul>
 * <li> mCount, int <br> search result count.
 * <li> mHTML, String <br> search result  in html.
 * </ul>
 * @author Vaibhav Bhargava
 */

public class RetailerBean
    extends PageBean
{
    public static final String ID = "rlbean";
    
    private String mCatHTML = null;

    private String mHTML = null;
    private int mCount = 0;

    private String mID = "";
    private String mSort = null;
    
    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    public RetailerBean(int pageSize)
    {
        super(pageSize);
    }

    // for a specific id
    public RetailerBean(int pageSize, String id)
    {
        super(pageSize);
        mID = id;
    }

    // for all search
    public RetailerBean(int pageSize, String page, String sort)
    {
        super(pageSize);
        super.setPageNo(Integer.parseInt(page));
        setSort(sort);
    }

    /**
     * Bean Property: mSort
     */
    public String getSort() { return mSort; }
    public void setSort(String sort) { mSort = sort; }

    /**
     * Bean Property: mID
     */
    public String getID() { return mID; }

    /**
     * Bean Property: mCatHTML
     */
    public String getRetailerCategoryHTML() { return mCatHTML; }
    public void setRetailerCategoryHTML(String str) { mCatHTML = str; }

    /**
     * Bean Property: mHTML
     */
    public String getRetailerHTML() { return mHTML; }
    public void setRetailerHTML(String str) { mHTML = str; }

    /**
     * Bean Property: mCount
     */
    public int getRetailerCount() { return mCount; }
    public void setRetailerCount(int count) { mCount = count; }
}

package com.broadon.online;

/**
 * JSP Bean for querying and managing Stores. 
 * It has following Bean properties:
 * <ul>
 * <li> mCount, int <br> search result count.
 * <li> mHTML, String <br> search result  in html.
 * </ul>
 * @author Vaibhav Bhargava
 */

public class StoreBean
    extends PageBean
{
    public static final String ID = "stbean";
    
    private String mRcHTML = null;
    private String mRgHTML = null;
    private String mRlHTML = null;

    private String mHTML = null;
    private int mCount = 0;

    private String mRetailerID = null;
    private String mID = "";
    private String mSort = null;
    
    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    public StoreBean(int pageSize)
    {
        super(pageSize);
    }

    // for a specific id
    public StoreBean(int pageSize, String id)
    {
        super(pageSize);
        mID = id;
    }

    // for all search
    public StoreBean(int pageSize, String page, String sort)
    {
        super(pageSize);
        super.setPageNo(Integer.parseInt(page));
        setSort(sort);
    }

    /**
     * Bean Property: mSort
     */
    public String getSort() { return mSort; }
    public void setSort(String sort) { mSort = sort; }

    /**
     * Bean Property: mID
     */
    public String getID() { return mID; }

    /**
     * Bean Property: mRcHTML
     */
    public String getRegionalCenterHTML() { return mRcHTML; }
    public void setRegionalCenterHTML(String str) { mRcHTML = str; }

    /**
     * Bean Property: mRgHTML
     */
    public String getRegionHTML() { return mRgHTML; }
    public void setRegionHTML(String str) { mRgHTML = str; }

    /**
     * Bean Property: mRlHTML
     */
    public String getRetailerHTML() { return mRlHTML; }
    public void setRetailerHTML(String str) { mRlHTML = str; }

    /**
     * Bean Property: mHTML
     */
    public String getStoreHTML() { return mHTML; }
    public void setStoreHTML(String str) { mHTML = str; }

    /**
     * Bean Property: mCount
     */
    public int getStoreCount() { return mCount; }
    public void setStoreCount(int count) { mCount = count; }

    /**
     * Bean Property: mRetailerID
     */
    public String getRetailerID() { return mRetailerID; }
    public void setRetailerID(String str) { mRetailerID = str; }

}

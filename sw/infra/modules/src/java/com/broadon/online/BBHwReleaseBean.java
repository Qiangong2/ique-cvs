package com.broadon.online;

/**
 * JSP Bean for querying and managing BBHwReleases. 
 * It has following Bean properties:
 * <ul>
 * <li> mCount, int <br> search result count.
 * <li> mHTML, String <br> search result  in html.
 * </ul>
 * @author Vaibhav Bhargava
 */

public class BBHwReleaseBean
    extends PageBean
{
    public static final String ID = "bbhrbean";
    
    private String mHTML = null;
    private int mCount = 0;

    private String mSort = null;
    
    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    public BBHwReleaseBean(int pageSize)
    {
        super(pageSize);
    }

    // for all search
    public BBHwReleaseBean(int pageSize, String page, String sort)
    {
        super(pageSize);
        super.setPageNo(Integer.parseInt(page));
        setSort(sort);
    }

    /**
     * Bean Property: mSort
     */
    public String getSort() { return mSort; }
    public void setSort(String sort) { mSort = sort; }

    /**
     * Bean Property: mHTML
     */
    public String getBBHwReleasesHTML() { return mHTML; }
    public void setBBHwReleasesHTML(String str) { mHTML = str; }

    /**
     * Bean Property: mCount
     */
    public int getBBHwReleasesCount() { return mCount; }
    public void setBBHwReleasesCount(int count) { mCount = count; }
}

package com.broadon.online;

/**
 * JSP Bean for querying and managing Regional Centers. 
 * It has following Bean properties:
 * <ul>
 * <li> mCount, int <br> search result count.
 * <li> mHTML, String <br> search result  in html.
 * </ul>
 * @author Vaibhav Bhargava
 */

public class RegionalCenterBean
    extends PageBean
{
    public static final String ID = "rcbean";
    
    private String mHTML = null;
    private int mCount = 0;

    private String mServerHTML = null;
    private int mServerCount = 0;

    private String mID = "";
    private String mSort = null;
    
    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    public RegionalCenterBean(int pageSize)
    {
        super(pageSize);
    }

    // for a specific id
    public RegionalCenterBean(int pageSize, String id)
    {
        super(pageSize);
        mID = id;
    }

    // for all search
    public RegionalCenterBean(int pageSize, String page, String sort)
    {
        super(pageSize);
        super.setPageNo(Integer.parseInt(page));
        setSort(sort);
    }

    /**
     * Bean Property: mSort
     */
    public String getSort() { return mSort; }
    public void setSort(String sort) { mSort = sort; }

    /**
     * Bean Property: mID
     */
    public String getID() { return mID; }

    /**
     * Bean Property: mHTML
     */
    public String getRegionalCenterHTML() { return mHTML; }
    public void setRegionalCenterHTML(String str) { mHTML = str; }

    /**
     * Bean Property: mCount
     */
    public int getRegionalCenterCount() { return mCount; }
    public void setRegionalCenterCount(int count) { mCount = count; }

    /**
     * Bean Property: mServerHTML
     */
    public String getRegionalCenterServerHTML() { return mServerHTML; }
    public void setRegionalCenterServerHTML(String str) { mServerHTML = str; }

    /**
     * Bean Property: mServerCount
     */
    public int getRegionalCenterServerCount() { return mServerCount; }
    public void setRegionalCenterServerCount(int count) { mServerCount = count; }
}

package com.broadon.online;

/**
 * JSP Bean for querying and managing Content Title Regions
 * It has following Bean properties:
 * <ul>
 * <li> mCtrCount, String <br> search count  in html.
 * <li> mCtrHTML, String <br> search result  in html.
 * </ul>
 * @author	Vaibhav Bhargava
 * @version	$Revision: 1.1 $
 */
public class ContentTitleRegionsBean
    extends PageBean
{
    public static final String ID = "ctrbean";

    private String mCtrHTML = null;
    private int mCtrCount = 0;

    private String mTitleHTML = null;
    private int mTitleCount = 0;
    private String mTitleNameHTML = null;

    private String mRegionHTML = null;
    private int mRegionCount = 0;

    private String mTitleID = "";
    private String mSort = null;

    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    public ContentTitleRegionsBean(int pageSize)
    {
	super(pageSize);
    }

    // for a specific title id
    public ContentTitleRegionsBean(int pageSize, String title_id)
    {
	super(pageSize);
        mTitleID = title_id;
    }

    // for all search
    public ContentTitleRegionsBean(int pageSize, String page, String sort)
    {
	super(pageSize);
	super.setPageNo(Integer.parseInt(page));
	setSort(sort);
    }

    /**
     * Bean Property: mSort
     */
    public String getSort() { return mSort; }
    public void setSort(String sort) { mSort = sort; }

    /**
     * Bean Property: mTitleID
     */
    public String getTitleID() { return mTitleID; }

    /**
     * Bean Property: mCtrHTML
     */
    public String getCtrHTML() { return mCtrHTML; }
    public void setCtrHTML(String str) { mCtrHTML = str; }

    /**
     * Bean Property: mCtrCount
     */
    public int getCtrCount() { return mCtrCount; }
    public void setCtrCount(int count) { mCtrCount = count; }

    /**
     * Bean Property: mTitleHTML
     */
    public String getTitleHTML() { return mTitleHTML; }
    public void setTitleHTML(String str) { mTitleHTML = str; }

    /**
     * Bean Property: mTitleCount
     */
    public int getTitleCount() { return mTitleCount; }
    public void setTitleCount(int count) { mTitleCount = count; }

    /**
     * Bean Property: mTitleNameHTML
     */
    public String getTitleNameHTML() { return mTitleNameHTML; }
    public void setTitleNameHTML(String str) { mTitleNameHTML = str; }

    /**
     * Bean Property: mRegionHTML
     */
    public String getRegionHTML() { return mRegionHTML; }
    public void setRegionHTML(String str) { mRegionHTML = str; }

    /**
     * Bean Property: mRegionCount
     */
    public int getRegionCount() { return mRegionCount; }
    public void setRegionCount(int count) { mRegionCount = count; }
}

package com.broadon.online;

/**
 * JSP Bean for querying and managing ECards. 
 * It has following Bean properties:
 * <ul>
 * <li> mCount, int <br> search result count.
 * <li> mHTML, String <br> search result  in html.
 * </ul>
 * @author Vaibhav Bhargava
 */

public class ECardsBean
    extends PageBean
{
    public static final String ID = "ecbean";
    
    private String mHTML = null;
    private String mID = null;
    private int mRange = 1;

    private int mToRevoke = 0;
    private int mToActivate = 0;
    private String mBatchHTML = null;
    
    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    public ECardsBean(int pageSize)
    {
        super(pageSize);
    }

    // for a specific id
    public ECardsBean(int pageSize, String id)
    {
        super(pageSize);
        mID = id;
    }

    // for a specific id and range
    public ECardsBean(int pageSize, String id, int range)
    {
        super(pageSize);
        mID = id;
        mRange = range;
    }

    /**
     * Bean Property: mID
     */
    public String getECardID() { return mID; }

    /**
     * Bean Property: mRange
     */
    public int getRange() { return mRange; }

    /**
     * Bean Property: mHTML
     */
    public String getECardsHTML() { return mHTML; }
    public void setECardsHTML(String str) { mHTML = str; }

    /**
     * Bean Property: mBatchHTML
     */
    public String getECardBatchHTML() { return mBatchHTML; }
    public void setECardBatchHTML(String str) { mBatchHTML = str; }

    /**
     * Bean Property: mToRevoke
     */
    public int getNumToRevoke() { return mToRevoke; }
    public void setNumToRevoke(int num) { mToRevoke = num; }

    /**
     * Bean Property: mToActivate
     */
    public int getNumToActivate() { return mToActivate; }
    public void setNumToActivate(int num) { mToActivate = num; }

}

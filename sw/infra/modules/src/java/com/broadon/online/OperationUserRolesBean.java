package com.broadon.online;

/**
 * JSP Bean for querying and managing Operation User Roles
 * It has following Bean properties:
 * <ul>
 * <li> mCurrentCount, int <br> current roles result count.
 * <li> mCurrentHTML, String <br> current roles result in html.
 * <li> mNewCount, int <br> new roles result count.
 * <li> mNewHTML, String <br> snew roles result in html.
 * </ul>
 * @author	Vaibhav Bhargava
 * @version	$Revision: 1.2 $
 */
public class OperationUserRolesBean
    extends PageBean
{
    public static final String ID = "ourbean";

    private String mCurrentHTML = null;
    private int mCurrentCount = 0;
    private String mNewHTML = null;
    private int mNewCount = 0;
    private String mUserLogin = "";
    private String mUserName = "";

    private String mOpID = "";
    private String mBUID = "";

    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    public OperationUserRolesBean(int pageSize)
    {
	super(pageSize);
    }

    // for a specific operator id & BU ID
    public OperationUserRolesBean(int pageSize, String operator_id, String bu_id)
    {
      super(pageSize);
      mOpID = operator_id;
      mBUID = bu_id;
    }

    /**
     * Bean Property: mBUID
     */
    public String getBUID() { return mBUID; }

    /**
     * Bean Property: mOpID
     */
    public String getOperatorID() { return mOpID; }

    /**
     * Bean Property: mCurrentHTML
     */
    public String getCurrentHTML() { return mCurrentHTML; }
    public void setCurrentHTML(String str) { mCurrentHTML = str; }

    /**
     * Bean Property: mCurrentCount
     */
    public int getCurrentCount() { return mCurrentCount; }
    public void setCurrentCount(int count) { mCurrentCount = count; }

    /**
     * Bean Property: mNewHTML
     */
    public String getNewHTML() { return mNewHTML; }
    public void setNewHTML(String str) { mNewHTML = str; }

    /**
     * Bean Property: mNewCount
     */
    public int getNewCount() { return mNewCount; }
    public void setNewCount(int count) { mNewCount = count; }

    /**
     * Bean Property: mUserName
     */
    public String getUserName() { return mUserName; }
    public void setUserName(String str) { mUserName = str; }

    /**
     * Bean Property: mUserLogin
     */
    public String getUserLogin() { return mUserLogin; }
    public void setUserLogin(String str) { mUserLogin = str; }

}

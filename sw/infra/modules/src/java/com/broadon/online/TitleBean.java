package com.broadon.online;

/**
 * JSP Bean for querying and managing Content Titles
 * It has following Bean properties:
 * <ul>
 * <li> mTitleCount, String <br> search count  in html.
 * <li> mTitleHTML, String <br> search result  in html.
 * </ul>
 * @author	Vaibhav Bhargava
 * @version	$Revision: 1.1 $
 */
public class TitleBean
    extends PageBean
{
    public static final String ID = "tlbean";

    private String mTitleHTML = null;
    private int mTitleCount = 0;
    private String mTitleNameHTML = null;

    private String mReviewHTML = null;
    private int mReviewCount = 0;

    private String mContentHTML = null;
    private int mContentCount = 0;

    private String mTitleID = "";
    private String mSort = null;

    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    public TitleBean(int pageSize)
    {
	super(pageSize);
    }

    // for a specific title id
    public TitleBean(int pageSize, String title_id)
    {
	super(pageSize);
        mTitleID = title_id;
    }

    // for all search
    public TitleBean(int pageSize, String page, String sort)
    {
	super(pageSize);
	super.setPageNo(Integer.parseInt(page));
	setSort(sort);
    }

    /**
     * Bean Property: mSort
     */
    public String getSort() { return mSort; }
    public void setSort(String sort) { mSort = sort; }

    /**
     * Bean Property: mTitleID
     */
    public String getTitleID() { return mTitleID; }

    /**
     * Bean Property: mContentHTML
     */
    public String getContentHTML() { return mContentHTML; }
    public void setContentHTML(String str) { mContentHTML = str; }

    /**
     * Bean Property: mContentCount
     */
    public int getContentCount() { return mContentCount; }
    public void setContentCount(int count) { mContentCount = count; }

    /**
     * Bean Property: mTitleHTML
     */
    public String getTitleHTML() { return mTitleHTML; }
    public void setTitleHTML(String str) { mTitleHTML = str; }

    /**
     * Bean Property: mTitleCount
     */
    public int getTitleCount() { return mTitleCount; }
    public void setTitleCount(int count) { mTitleCount = count; }

    /**
     * Bean Property: mTitleNameHTML
     */
    public String getTitleNameHTML() { return mTitleNameHTML; }
    public void setTitleNameHTML(String str) { mTitleNameHTML = str; }

    /**
     * Bean Property: mReviewHTML
     */
    public String getReviewHTML() { return mReviewHTML; }
    public void setReviewHTML(String str) { mReviewHTML = str; }

    /**
     * Bean Property: mReviewCount
     */
    public int getReviewCount() { return mReviewCount; }
    public void setReviewCount(int count) { mReviewCount = count; }
}

package com.broadon.online;

/**
 * JSP Bean for querying and managing DataLoadRequests. 
 * It has following Bean properties:
 * <ul>
 * <li> mCount, int <br> search result count.
 * <li> mHTML, String <br> search result  in html.
 * </ul>
 * @author Vaibhav Bhargava
 */

public class DataLoadRequestBean
    extends PageBean
{
    public static final String ID = "dlrbean";
    
    private String mHTML = null;
    private int mCount = 0;

    private String mID = "";
    private String mSort = null;
    
    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    public DataLoadRequestBean(int pageSize)
    {
        super(pageSize);
    }

    // for a specific id
    public DataLoadRequestBean(int pageSize, String id)
    {
        super(pageSize);
        mID = id;
    }

    // for all search
    public DataLoadRequestBean(int pageSize, String page, String sort)
    {
        super(pageSize);
        super.setPageNo(Integer.parseInt(page));
        setSort(sort);
    }

    /**
     * Bean Property: mSort
     */
    public String getSort() { return mSort; }
    public void setSort(String sort) { mSort = sort; }

    /**
     * Bean Property: mID
     */
    public String getID() { return mID; }

    /**
     * Bean Property: mHTML
     */
    public String getDataLoadRequestHTML() { return mHTML; }
    public void setDataLoadRequestHTML(String str) { mHTML = str; }

    /**
     * Bean Property: mCount
     */
    public int getDataLoadRequestCount() { return mCount; }
    public void setDataLoadRequestCount(int count) { mCount = count; }
}

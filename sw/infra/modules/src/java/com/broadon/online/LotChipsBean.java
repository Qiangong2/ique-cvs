package com.broadon.online;

/**
 * JSP Bean for querying and managing LotChips. 
 * It has following Bean properties:
 * <ul>
 * <li> mCount, int <br> search result count.
 * <li> mHTML, String <br> search result  in html.
 * </ul>
 * @author Vaibhav Bhargava
 */

public class LotChipsBean
    extends PageBean
{
    public static final String ID = "lcbean";
    
    private String mHTML = null;
    private int mCount = 0;

    private String mID = "";
    private String mSort = null;
    
    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    public LotChipsBean(int pageSize)
    {
        super(pageSize);
    }

    // for a specific id
    public LotChipsBean(int pageSize, String page, String sort, String id)
    {
        super(pageSize);
        super.setPageNo(Integer.parseInt(page));
        setSort(sort);
        mID = id;
    }

    /**
     * Bean Property: mSort
     */
    public String getSort() { return mSort; }
    public void setSort(String sort) { mSort = sort; }

    /**
     * Bean Property: mID
     */
    public String getID() { return mID; }

    /**
     * Bean Property: mHTML
     */
    public String getLotChipsHTML() { return mHTML; }
    public void setLotChipsHTML(String str) { mHTML = str; }

    /**
     * Bean Property: mCount
     */
    public int getLotChipsCount() { return mCount; }
    public void setLotChipsCount(int count) { mCount = count; }
}

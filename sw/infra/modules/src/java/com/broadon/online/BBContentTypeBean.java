package com.broadon.online;

/**
 * JSP Bean for querying and managing BB Content Object Types. 
 * It has following Bean properties:
 * <ul>
 * <li> mCount, int <br> search result count.
 * <li> mHTML, String <br> search result in html.
 * </ul>
 * @author Vaibhav Bhargava
 */

public class BBContentTypeBean
    extends PageBean
{
    public static final String ID = "bbctbean";
    
    private String mHTML = null;
    private int mCount = 0;

    private String mModelHTML = null;
    private String mTypeHTML = null;

    private String mModel = "";
    private String mType = "";
    private String mSort = null;
    
    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    public BBContentTypeBean(int pageSize)
    {
        super(pageSize);
    }

    // for a specific id
    public BBContentTypeBean(int pageSize, String model, String type, String dummy)
    {
        super(pageSize);
        mModel = model;
        mType = type;
    }

    // for all search
    public BBContentTypeBean(int pageSize, String page, String sort)
    {
        super(pageSize);
        super.setPageNo(Integer.parseInt(page));
        setSort(sort);
    }

    /**
     * Bean Property: mSort
     */
    public String getSort() { return mSort; }
    public void setSort(String sort) { mSort = sort; }

    /**
     * Bean Property: mModel
     */
    public String getBBModel() { return mModel; }

    /**
     * Bean Property: mType
     */
    public String getContentType() { return mType; }

    /**
     * Bean Property: mHTML
     */
    public String getBBContentTypeHTML() { return mHTML; }
    public void setBBContentTypeHTML(String str) { mHTML = str; }

    /**
     * Bean Property: mCount
     */
    public int getBBContentTypeCount() { return mCount; }
    public void setBBContentTypeCount(int count) { mCount = count; }

    /**
     * Bean Property: mModelHTML
     */
    public String getBBModelHTML() { return mModelHTML; }
    public void setBBModelHTML(String str) { mModelHTML = str; }

    /**
     * Bean Property: mTypeHTML
     */
    public String getContentTypeHTML() { return mTypeHTML; }
    public void setContentTypeHTML(String str) { mTypeHTML = str; }

}

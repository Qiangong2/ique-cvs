package com.broadon.online;

/**
 * JSP Bean for querying and managing Content Objects
 * It has following Bean properties:
 * <ul>
 * <li> mContentCount, String <br> search count  in html.
 * <li> mContentHTML, String <br> search result  in html.
 * </ul>
 * @author	Vaibhav Bhargava
 * @version	$Revision: 1.1 $
 */
public class ContentBean
    extends PageBean
{
    public static final String ID = "conbean";

    private String mContentHTML = null;
    private int mContentCount = 0;
    private String mContentNameHTML = null;

    private String mTitleHTML = null;
    private int mTitleCount = 0;

    private String mContentID = "";
    private String mSort = null;

    private String mHasMetadata = "";

    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    public ContentBean(int pageSize)
    {
	super(pageSize);
    }

    // for a specific content id
    public ContentBean(int pageSize, String content_id)
    {
	super(pageSize);
        mContentID = content_id;
    }

    // for all search
    public ContentBean(int pageSize, String page, String sort)
    {
	super(pageSize);
	super.setPageNo(Integer.parseInt(page));
	setSort(sort);
    }

    /**
     * Bean Property: mSort
     */
    public String getSort() { return mSort; }
    public void setSort(String sort) { mSort = sort; }

    /**
     * Bean Property: mContentID
     */
    public String getContentID() { return mContentID; }

    /**
     * Bean Property: mContentHTML
     */
    public String getContentHTML() { return mContentHTML; }
    public void setContentHTML(String str) { mContentHTML = str; }

    /**
     * Bean Property: mContentCount
     */
    public int getContentCount() { return mContentCount; }
    public void setContentCount(int count) { mContentCount = count; }

    /**
     * Bean Property: mContentNameHTML
     */
    public String getContentNameHTML() { return mContentNameHTML; }
    public void setContentNameHTML(String str) { mContentNameHTML = str; }

    /**
     * Bean Property: mTitleHTML
     */
    public String getTitleHTML() { return mTitleHTML; }
    public void setTitleHTML(String str) { mTitleHTML = str; }

    /**
     * Bean Property: mTitleCount
     */
    public int getTitleCount() { return mTitleCount; }
    public void setTitleCount(int count) { mTitleCount = count; }

    /**
     * Bean Property: mHasMetadata
     */
    public String getHasMetadata() { return mHasMetadata; }
    public void setHasMetadata(String str) { mHasMetadata = str; }

}

package com.broadon.online;

/**
 * JSP Bean for querying and managing Certificate and Certificate Chains
 * It has following Bean properties:
 * <ul>
 * <li> mCount, String <br> search count  in html.
 * <li> mHTML, String <br> search result  in html.
 * </ul>
 * @author	Vaibhav Bhargava
 * @version	$Revision: 1.1 $
 */
public class CertificateBean
    extends PageBean
{
    public static final String ID = "certbean";

    private String mParentCertHTML = null;

    private String mCertHTML = null;
    private int mCertCount = 0;

    private String mChainHTML = null;
    private int mChainCount = 0;

    private String mID = "";
    private String mSort = null;

    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    public CertificateBean(int pageSize)
    {
	super(pageSize);
    }

    // for a specific id
    public CertificateBean(int pageSize, String id)
    {
	super(pageSize);
        mID = id;
    }

    // for all search
    public CertificateBean(int pageSize, String page, String sort)
    {
	super(pageSize);
	super.setPageNo(Integer.parseInt(page));
	setSort(sort);
    }

    /**
     * Bean Property: mSort
     */
    public String getSort() { return mSort; }
    public void setSort(String sort) { mSort = sort; }

    /**
     * Bean Property: mID
     */
    public String getID() { return mID; }

    /**
     * Bean Property: mParentCertHTML
     */
    public String getParentCertHTML() { return mParentCertHTML; }
    public void setParentCertHTML(String str) { mParentCertHTML = str; }

    /**
     * Bean Property: mCertHTML
     */
    public String getCertHTML() { return mCertHTML; }
    public void setCertHTML(String str) { mCertHTML = str; }

    /**
     * Bean Property: mCertCount
     */
    public int getCertCount() { return mCertCount; }
    public void setCertCount(int count) { mCertCount = count; }

    /**
     * Bean Property: mChainHTML
     */
    public String getChainHTML() { return mChainHTML; }
    public void setChainHTML(String str) { mChainHTML = str; }

    /**
     * Bean Property: mChainCount
     */
    public int getChainCount() { return mChainCount; }
    public void setChainCount(int count) { mChainCount = count; }
}

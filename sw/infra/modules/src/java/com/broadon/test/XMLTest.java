package com.broadon.test;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;

import com.broadon.bean.Bean;
import com.broadon.bean.TextFormatter;
import com.broadon.bean.XMLFormatter;
import com.broadon.db.DBAccessFactory;
import com.broadon.db.DBException;
import com.broadon.db.SyncableFactory;
import com.broadon.xml.XMLMap;
import com.broadon.xml.XMLOutputProcessor;

/**
 * The <c>XMLTest</c> class tests the XMLMap functionalities.
 *
 * @version	$Revision: 1.5 $
 */
public class XMLTest
{
    /**
     * Tests the XMLMap functionalities.
     *
     * @param	args
     *			args[0]		the leaf name of a Bean class,
     *					must be under "com.broadon.db"
     */
    public static final void main(String[] args)
	throws Throwable
    {
	String		className = "ContentInfo";

	if (args.length > 0)
	{
	    className = args[0];
	}
	/*
	 * Connect to database.
	 */
	DataSource	ds;

	ds = OracleDS.createDataSource();
	/*
	 * Start testing.
	 */
	String		factoryName = "com.broadon.db." + className + "Factory";
	Class		factoryClass = Class.forName(factoryName);
	Method		singleton = factoryClass.getMethod(
						"getInstance",
						new Class[] { String.class });
	SyncableFactory	factory = (SyncableFactory)singleton.invoke(
						null,
						new String[] { factoryName });

	System.out.println(factory);

	DBAccessFactory.beginTransaction(ds, false, false);

	try
	{
	    Map		nameMap = NameMap.getNameMap();
	    Date	from = new Date(0);
	    Date	to = new Date(new Date().getTime() + 86400000);
	    Bean[]	beans = factory.getRecordsWithinInterval(from,
								 to,
								 nameMap);

	    if (beans == null)
	    {
		System.out.println("No data");
		return;
	    }

	    int		size = beans.length;
	    Bean	bean = beans[size - 1];

	    for (int n = 0; n < size; n++)
	    {
		/*
		 * Release unused objects.
		 */
		beans[n] = null;
	    }

	    Class		beanClass = bean.getClass();
	    String		tableName = factory.getTableName();
	    XMLFormatter	formatter = new XMLFormatter(bean);

	    formatter.setSkipNull(false);
	    formatter.setNameMap(nameMap);

	    String		xmlContent = formatter.format(1, tableName);

	    System.out.println("XML with table name as root tag");
	    System.out.print(xmlContent);

	    Map		map = new HashMap();
	    XMLMap	xmlMap = new XMLMap();

	    System.out.println(xmlMap.process(xmlContent, map));
	    map = new HashMap();
	    System.out.println(xmlMap.process(xmlContent, map, tableName));

	    xmlContent = "<l1><l2><l3>3</l3></l2></l1>";
	    map = new HashMap();
	    System.out.println(xmlMap.process(xmlContent, map));
	}
	finally
	{
	    try
	    {
		DBAccessFactory.commitTransaction();
	    }
	    catch (DBException de)
	    {
		de.printStackTrace();
	    }
	}
    }
}

package com.broadon.test;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;

import com.broadon.bean.Bean;
import com.broadon.bean.XMLFormatWriter;
import com.broadon.db.DBAccessFactory;
import com.broadon.db.DBException;
import com.broadon.db.DepotID;
import com.broadon.db.Region;
import com.broadon.db.RegionFactory;
import com.broadon.util.Timer;

/**
 * The <code>RegionTest</code> class tests the subclasses of
 * Region class.
 * The data are retrieved from the database.
 *
 * @version	$Revision: 1.1 $
 */
public class RegionTest
{
    /**
     * Tests the PBean instances.
     */
    public static final void main(String[] args)
	throws Throwable
    {
	String		order = null;

	if (args.length > 0)
	{
	    order = args[0];
	}
	/*
	 * Connect to database.
	 */
	DataSource	ds;

	ds = OracleDS.createDataSource();
	/*
	 * Start testing.
	 */
	Class		factoryClass = RegionFactory.class;
	String		factoryName = factoryClass.getName();
	Method		singleton = factoryClass.getMethod(
						"getInstance",
						new Class[] { String.class });
	RegionFactory	factory;

	factory = (RegionFactory)singleton.invoke(
						null,
						new String[] { factoryName });
	System.out.println(factory);

	Map		nameMap = NameMap.getNameMap();
	String		xml;
	int		count;

	factory.beginTransaction(ds, false, false);
	try
	{
	    count = factory.getRegionCount();
	    System.out.println("Region Names");
	    xml = factory.getRegionNames();
	    System.out.println(xml);
	    System.out.println("Regions");
	    xml = factory.getRegions(0, 100, order);
	    System.out.println(xml);
	    System.out.println("There are " + count + " regions.");
	}
	finally
	{
	    factory.commitTransaction();
	}
    }
}

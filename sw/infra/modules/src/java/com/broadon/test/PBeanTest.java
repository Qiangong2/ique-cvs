package com.broadon.test;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;

import com.broadon.bean.Bean;
import com.broadon.bean.TextFormatWriter;
import com.broadon.bean.TextFormatter;
import com.broadon.bean.XMLConverter;
import com.broadon.bean.XMLFormatWriter;
import com.broadon.bean.XMLFormatter;
import com.broadon.db.DBAccessFactory;
import com.broadon.db.DBException;
import com.broadon.db.SyncableFactory;
import com.broadon.db.TitleContent;
import com.broadon.db.TitleContentFactory;
import com.broadon.util.Timer;

/**
 * The <c>PBeanTest</c> class tests the subclasses of PBean class.
 * The data are retrieved from the database.
 *
 * @version	$Revision: 1.6 $
 */
public class PBeanTest
{
    /**
     * Tests the PBean instances.
     *
     * @param	args
     *			args[0]		the number of times to loop
     *			args[1]		the leaf name of the PBean class
     */
    public static final void main(String[] args)
	throws Throwable
    {
	int		loops = 10;
	String		className = "ContentInfo";

	if (args.length > 0)
	{
	    loops = Integer.parseInt(args[0]);
	    if (args.length > 1)
	    {
		className = args[1];
	    }
	}
	/*
	 * Connect to database.
	 */
	DataSource	ds;

	ds = OracleDS.createDataSource();
	/*
	 * Start testing.
	 */
	String		factoryName = "com.broadon.db." + className + "Factory";
	Class		factoryClass = Class.forName(factoryName);
	Method		singleton = factoryClass.getMethod(
						"getInstance",
						new Class[] { String.class });
	SyncableFactory	factory = (SyncableFactory)singleton.invoke(
						null,
						new String[] { factoryName });

	System.out.println(factory);

	factory.beginTransaction(ds, false, true);

	try
	{
	    Timer	timer = new Timer();
	    Map		nameMap = NameMap.getNameMap();
	    Map		reverseNameMap = NameMap.getReverseNameMap();
	    Date	from = new Date(0);
	    Date	to = new Date(new Date().getTime() + 86400000);
	    Bean[]	beans;

	    timer.start();
	    beans = factory.getRecordsWithinInterval(from, to, nameMap);
	    timer.stop();
	    if (beans == null)
	    {
		System.err.println("No data");
		return;
	    }
	    System.out.println("Query takes " + timer);

	    Bean	bean = beans[0];
	    Class	beanClass = bean.getClass();
	    PrintWriter	out = new PrintWriter(System.out);

	    /*
	     * Release unused beans so that they can be garbage collected.
	     */
	    for (int n = 0; n < beans.length; n++)
	    {
		beans[n] = null;
	    }

	    System.out.println("Text");
	    new TextFormatWriter(bean).format(out, 1);

	    System.out.println("XML");
	    new XMLFormatWriter(bean).format(out, 1);

	    XMLFormatWriter	writer = new XMLFormatWriter(bean);
	    StringWriter	xml = new StringWriter();
	    String		xmlContent;

	    writer.setNameMap(nameMap);
	    writer.format(xml, 1, factory.getTableName());
	    writer = null;
	    bean = null;
	    System.out.println("XML with table name as root tag");
	    xmlContent = xml.toString();
	    System.out.print(xmlContent);

	    System.out.println("Constructed from XML document");

	    XMLConverter	converter = new XMLConverter();

	    bean = converter.toBean(xmlContent,
				    beanClass,
				    reverseNameMap,
				    factory.getTableName());
	    new TextFormatWriter(bean).format(out, 1);

	    timer.start();
	    for (int n = 0; n < loops; n++)
	    {
		xml = new StringWriter();
		writer = new XMLFormatWriter(bean);
		writer.setNameMap(nameMap);
		writer.format(xml, 1);
		xmlContent = xml.toString();
		xml.close();
	    }
	    timer.stop();
	    System.out.println("Convert -> XML " +
				loops +
				" times take: " +
				timer);

	    timer.start();
	    for (int n = 0; n < loops; n++)
	    {
		bean = converter.toBean(xmlContent,
					beanClass,
					reverseNameMap,
					className);
	    }
	    timer.stop();
	    System.out.print(new XMLFormatter(bean));
	    System.out.println("Convert <- XML " +
				loops +
				" times take: " +
				timer);
	}
	finally
	{
	    factory.commitTransaction();
	}
    }
}

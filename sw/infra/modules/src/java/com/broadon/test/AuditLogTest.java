package com.broadon.test;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;

import com.broadon.bean.Bean;
import com.broadon.bean.TextFormatWriter;
import com.broadon.bean.TextFormatter;
import com.broadon.bean.XMLConverter;
import com.broadon.bean.XMLFormatWriter;
import com.broadon.bean.XMLFormatter;
import com.broadon.db.AuditLog;
import com.broadon.db.AuditLog;
import com.broadon.db.ContentDownloadLog;
import com.broadon.db.ContentDownloadLogFactory;
import com.broadon.db.ContentID;
import com.broadon.db.ContentSyncLog;
import com.broadon.db.ContentSyncLogFactory;
import com.broadon.db.DBAccessFactory;
import com.broadon.db.DBException;
import com.broadon.db.DepotID;
import com.broadon.db.RequestDate;
import com.broadon.sql.Predicate;
import com.broadon.util.Timer;

/**
 * The <code>AuditLogTest</code> class tests the subclasses of AuditLog class.
 * The data are retrieved from the database.
 *
 * @version	$Revision: 1.2 $
 */
public class AuditLogTest
{
    /**
     * Tests the PBean instances.
     *
     * @param	args
     *			args[0]		the number of times to loop
     *			args[1]		the leaf name of the PBean class
     */
    public static final void main(String[] args)
	throws Throwable
    {
	int		loops = 10;
	String		className = "ContentSyncLog";

	if (args.length > 0)
	{
	    loops = Integer.parseInt(args[0]);
	    if (args.length > 1)
	    {
		className= args[1];
	    }
	}
	/*
	 * Connect to database.
	 */
	DataSource	ds;

	ds = OracleDS.createDataSource();
	/*
	 * Start testing.
	 */
	String		factoryName = "com.broadon.db." + className + "Factory";
	Class		factoryClass = Class.forName(factoryName);
	Method		singleton = factoryClass.getMethod(
						"getInstance",
						new Class[] { String.class });
	DBAccessFactory	factory;

	factory = (DBAccessFactory)singleton.invoke(
						null,
						new String[] { factoryName });
	System.out.println(factory);

	Timer		timer = new Timer();
	Map		nameMap = NameMap.getNameMap();
	Map		reverseNameMap = NameMap.getReverseNameMap();
	long		depotID = 0x0123456789abL;
	Date		requestDate = new Date();
	Bean		bean;
	Predicate	predicate = new DepotID(depotID);

	timer.start();
	factory.beginTransaction(ds, false, false);
	try
	{
	    if (className.compareTo("ContentSyncLog") == 0)
	    {
		/*
		 * Create a content sync log record.
		 */
		ContentSyncLog	contentSyncLog = new ContentSyncLog();

		contentSyncLog.setDepotID(new Long(depotID));
		contentSyncLog.setRequestDate(requestDate);
		contentSyncLog.setCdsTimestamp(new Date(0));
		contentSyncLog.setNewCdsTimestamp(new Date(1400000000));
		contentSyncLog.setRequestLog("This is a test");
		contentSyncLog.setRequestStatus(AuditLog.STATUS_OK);
		factory.create(contentSyncLog, nameMap);
		predicate = predicate.and(new RequestDate(requestDate));
	    }
	    else
	    {
		/*
		 * Create a content sync log record.
		 */
		ContentDownloadLog	contentDownloadLog;

		contentDownloadLog = new ContentDownloadLog();
		contentDownloadLog.setDepotID(new Long(depotID));
		contentDownloadLog.setContentID(new Long(1));
		contentDownloadLog.setRequestDate(requestDate);
		contentDownloadLog.setCompletionDate(new Date());
		contentDownloadLog.setRequestLog("This is a test");
		contentDownloadLog.setRequestStatus(AuditLog.STATUS_OK);
		factory.create(contentDownloadLog, nameMap);
		predicate = predicate.and(new ContentID(1))
				     .and(new RequestDate(requestDate));
	    }
	}
	finally
	{
	    factory.commitTransaction();
	}
	timer.stop();
	System.out.println("Creation takes " + timer);

	factory.beginTransaction(ds, false, false);
	try
	{
	    timer.start();
	    bean = factory.queryUnique(predicate, nameMap);
	    timer.stop();
	    if (bean == null)
	    {
		System.err.println("No data");
		return;
	    }
	    System.out.println("Query takes " + timer);

	    timer.start();
	    factory.delete(predicate, nameMap);
	    timer.stop();
	    System.out.println("Deletion takes " + timer);

	    Class	beanClass = bean.getClass();
	    PrintWriter	out = new PrintWriter(System.out);

	    System.out.println("Text");
	    new TextFormatWriter(bean).format(out, 1);

	    System.out.println("XML");
	    new XMLFormatWriter(bean).format(out, 1);

	    XMLFormatWriter	writer = new XMLFormatWriter(bean);
	    StringWriter	xml = new StringWriter();
	    String		xmlContent;

	    writer.setNameMap(nameMap);
	    writer.format(xml, 1, factory.getTableName());
	    writer = null;
	    bean = null;
	    System.out.println("XML with table name as root tag");
	    xmlContent = xml.toString();
	    System.out.print(xmlContent);

	    System.out.println("Constructed from XML document");

	    XMLConverter	converter = new XMLConverter();

	    bean = converter.toBean(xmlContent,
				    beanClass,
				    reverseNameMap,
				    factory.getTableName());
	    new TextFormatWriter(bean).format(out, 1);

	    timer.start();
	    for (int n = 0; n < loops; n++)
	    {
		xml = new StringWriter();
		writer = new XMLFormatWriter(bean);
		writer.setNameMap(nameMap);
		writer.format(xml, 1);
		xmlContent = xml.toString();
		xml.close();
	    }
	    timer.stop();
	    System.out.println("Convert -> XML " +
				loops +
				" times take: " +
				timer);

	    timer.start();
	    for (int n = 0; n < loops; n++)
	    {
		bean = converter.toBean(xmlContent,
					beanClass,
					reverseNameMap,
					className);
	    }
	    timer.stop();
	    System.out.print(new XMLFormatter(bean));
	    System.out.println("Convert <- XML " +
				loops +
				" times take: " +
				timer);
	}
	finally
	{
	    factory.commitTransaction();
	}
    }
}

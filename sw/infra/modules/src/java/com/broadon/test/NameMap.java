package com.broadon.test;

import java.util.Map;

import com.broadon.db.NameMapManager;

/**
 * The <code>NameMap</code> class uses a fix path for the location of
 * the name map file.
 *
 * @version	$Revision: 1.8 $
 */
public class NameMap
{
    private static NameMapManager	nameMapManager;

    static
    {
	try
	{
	    nameMapManager = new NameMapManager("BBname.map");
	}
	catch (Throwable t)
	{
	    t.printStackTrace();
	    System.exit(-1);
	}
    }

    /**
     * Returns the name mapping table.
     *
     * @returns	The name mapping table.
     */
    public static Map getNameMap()
    {
	return nameMapManager.getNameMap();
    }

    /**
     * Returns the reverse name mapping table.
     *
     * @returns	The reverse name mapping table.
     */
    public static Map getReverseNameMap()
    {
	return nameMapManager.getReverseNameMap();
    }
}

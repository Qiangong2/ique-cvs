package com.broadon.test;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;

import com.broadon.bean.Bean;
import com.broadon.bean.XMLFormatWriter;
import com.broadon.db.DBAccessFactory;
import com.broadon.db.DBException;
import com.broadon.db.BusinessUnit;
import com.broadon.db.BusinessUnitFactory;

/**
 * The <code>BUTest</code> class tests the subclasses of
 * Business Unit class.
 * The data are retrieved from the database.
 *
 * @version	$Revision: 1.1 $
 */
public class BUTest
{
    /**
     * Tests the PBean instances.
     */
    public static final void main(String[] args)
	throws Throwable
    {
	int		id = 1;

	if (args.length > 0)
	{
	    id = Integer.parseInt(args[0]);
	}
	/*
	 * Connect to database.
	 */
	DataSource	ds;

	ds = OracleDS.createDataSource();
	/*
	 * Start testing.
	 */
	Class		factoryClass = BusinessUnitFactory.class;
	String		factoryName = factoryClass.getName();
	Method		singleton = factoryClass.getMethod(
						"getInstance",
						new Class[] { String.class });
	BusinessUnitFactory	factory;

	factory = (BusinessUnitFactory)singleton.invoke(
						null,
						new String[] { factoryName });
	System.out.println(factory);

	Map		nameMap = NameMap.getNameMap();

	factory.beginTransaction(ds, false, false);
	try
	{
	    Bean	bean = factory.getBusinessUnit(id, nameMap);
	    PrintWriter	out = new PrintWriter(System.out);

	    new XMLFormatWriter(bean).format(out, 1);
	}
	finally
	{
	    factory.commitTransaction();
	}
    }
}

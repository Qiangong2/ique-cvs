package com.broadon.test;

import java.io.StringWriter;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.broadon.bean.Bean;
import com.broadon.bean.TextFormatter;
import com.broadon.bean.XMLConverter;
import com.broadon.bean.XMLFormatWriter;
import com.broadon.bean.XMLFormatter;
import com.broadon.db.DBAccessFactory;
import com.broadon.db.DBException;
import com.broadon.db.TitleContent;
import com.broadon.db.TitleContentFactory;
import com.broadon.util.Timer;

/**
 * The <c>Beantest</c> class tests various aspects of the Bean instances.
 *
 * @version	$Revision: 1.4 $
 */
public class BeanTest
{
    /**
     * Tests the Bean instances.
     *
     * @param	args
     *			args[0]		the number of loops for converting
     *					between Bean <-> XML
     */
    public static final void main(String[] args)
	throws Throwable
    {
	int		loops = 10;

	if (args.length > 0)
	{
	    loops = Integer.parseInt(args[0]);
	}

	String		className = "TitleContent";
	String		factoryName = "com.broadon.db." + className + "Factory";
	Class		factoryClass = Class.forName(factoryName);
	Method		singleton = factoryClass.getMethod(
						"getInstance",
						new Class[] { String.class });
	DBAccessFactory	factory = (DBAccessFactory)singleton.invoke(
						null,
						new String[] { factoryName });

	System.out.println(factory);

	Map		nameMap = NameMap.getNameMap();
	Map		reverseNameMap = NameMap.getReverseNameMap();
	TitleContent	titleContent = new TitleContent();
	Bean		bean = titleContent;

	titleContent.setTitleID(new Long(1));
	titleContent.setContentID(new Long(1234567890));
	titleContent.setRevokeDate(new Date());

	Class		beanClass = bean.getClass();

	System.out.println("Text");
	System.out.print(new TextFormatter(bean));

	System.out.println("XML");
	System.out.print(new XMLFormatter(bean));

	XMLFormatter	formatter = new XMLFormatter(bean);
	String		xmlContent;

	formatter.setNameMap(nameMap);
	xmlContent = formatter.format(1, factory.getTableName());
	System.out.println("XML with table name as root tag");
	System.out.print(xmlContent);

	TestBean	testBean = new TestBean(bean);

	System.out.println("XML with Nested beans");

	formatter = new XMLFormatter(testBean);
	formatter.setSkipNull(false);
	formatter.setNameMap(nameMap);
	System.out.print(formatter);

	System.out.println("Constructed from XML document");

	XMLConverter	converter = new XMLConverter();

	bean = converter.toBean(xmlContent,
				beanClass,
				reverseNameMap,
				factory.getTableName());
	System.out.println(new TextFormatter(bean));

	Timer		timer = new Timer();

	timer.start();
	for (int n = 0; n < loops; n++)
	{
	    StringWriter	out = new StringWriter();
	    XMLFormatWriter	writer = new XMLFormatWriter(bean);

	    writer.setNameMap(nameMap);
	    writer.format(out, 1);
	    out.flush();
	    xmlContent = out.toString();
	    out.close();
	}
	timer.stop();
	System.out.println("Bean -> XML " +
			    loops +
			    " times take: " +
			    timer);

	timer.start();
	for (int n = 0; n < loops; n++)
	{
	    bean = converter.toBean(xmlContent,
				    beanClass,
				    reverseNameMap,
				    className);
	}
	timer.stop();
	System.out.print(new XMLFormatter(bean));
	System.out.println("Bean <- XML " +
			    loops +
			    " times take: " +
			    timer);
    }

    /**
     * The <c>TestBean</c> class provides a nested Bean for simple testing.
     */
    private static class TestBean
	extends Bean
    {
	Bean		bean;
	Integer		num;

	private TestBean()
	{
	    this(null);
	}

	private TestBean(Bean bean)
	{
	    this.bean = bean;
	    this.num = new Integer(168);
	}

	private Bean getBean()
	{
	    return bean;
	}

	private void setBean(Bean bean)
	{
	    this.bean = bean;
	}

	private Integer getNum()
	{
	    return num;
	}

	private void setNum(Integer num)
	{
	    this.num = num;
	}
    }
}

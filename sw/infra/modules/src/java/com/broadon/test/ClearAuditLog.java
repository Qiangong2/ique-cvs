package com.broadon.test;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;

import com.broadon.bean.Bean;
import com.broadon.bean.TextFormatWriter;
import com.broadon.bean.TextFormatter;
import com.broadon.bean.XMLConverter;
import com.broadon.bean.XMLFormatWriter;
import com.broadon.bean.XMLFormatter;
import com.broadon.db.AuditLog;
import com.broadon.db.AuditLog;
import com.broadon.db.ContentDownloadLog;
import com.broadon.db.ContentDownloadLogFactory;
import com.broadon.db.ContentID;
import com.broadon.db.ContentSyncLog;
import com.broadon.db.ContentSyncLogFactory;
import com.broadon.db.DBAccessFactory;
import com.broadon.db.DBException;
import com.broadon.db.DepotID;
import com.broadon.db.RequestDate;
import com.broadon.sql.Predicate;
import com.broadon.util.Timer;

/**
 * The <code>ClearAuditLog</code> class tests the subclasses of AuditLog class.
 * The data are retrieved from the database.
 *
 * @version	$Revision: 1.2 $
 */
public class ClearAuditLog
{
    /**
     * Tests the PBean instances.
     *
     * @param	args
     *			args[0]		the leaf name of the PBean class
     *			args[1]		the depot identifier
     */
    public static final void main(String[] args)
	throws Throwable
    {
	String		className = "ContentSyncLog";
	long		depotID = 0x0123456789abL;

	if (args.length > 0)
	{
	    className= args[0];
	    if (args.length > 1)
	    {
		depotID = Long.parseLong(args[1]);
	    }
	}
	/*
	 * Connect to database.
	 */
	DataSource	ds;

	ds = OracleDS.createDataSource();
	/*
	 * Start testing.
	 */
	String		factoryName = "com.broadon.db." + className + "Factory";
	Class		factoryClass = Class.forName(factoryName);
	Method		singleton = factoryClass.getMethod(
						"getInstance",
						new Class[] { String.class });
	DBAccessFactory	factory;

	factory = (DBAccessFactory)singleton.invoke(
						null,
						new String[] { factoryName });

	Timer		timer = new Timer();
	Map		nameMap = NameMap.getNameMap();
	Map		reverseNameMap = NameMap.getReverseNameMap();
	Date		requestDate = new Date();
	Bean		bean;
	Predicate	predicate = new DepotID(depotID);

	System.out.println("Clearing " + className + "." + depotID);
	timer.start();
	factory.beginTransaction(ds, false, false);
	try
	{
	    factory.delete(predicate, nameMap);
	}
	finally
	{
	    factory.commitTransaction();
	}
	timer.stop();
	System.out.println("Deletion takes " + timer);
    }
}

package com.broadon.test;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;

import com.broadon.bean.Bean;
import com.broadon.bean.XMLFormatWriter;
import com.broadon.db.DBAccessFactory;
import com.broadon.db.DBException;
import com.broadon.db.DepotID;
import com.broadon.db.RegionalCenter;
import com.broadon.db.RegionalCenterFactory;
import com.broadon.util.Timer;

/**
 * The <code>RegionalCenterTest</code> class tests the subclasses of
 * RegionalCenter class.
 * The data are retrieved from the database.
 *
 * @version	$Revision: 1.2 $
 */
public class RegionalCenterTest
{
    /**
     * Tests the PBean instances.
     *
     * @param	args
     *			args[0]		the depot id
     */
    public static final void main(String[] args)
	throws Throwable
    {
	long		depotID = 0x0123456789abL;
	String		order = null;

	if (args.length > 0)
	{
	    depotID = Long.parseLong(args[0]);
	    if (args.length > 1)
	    {
		order = args[1];
	    }
	}
	System.out.println("DepotID[" + depotID + "]");
	/*
	 * Connect to database.
	 */
	DataSource	ds;

	ds = OracleDS.createDataSource();
	/*
	 * Start testing.
	 */
	Class		factoryClass = RegionalCenterFactory.class;
	String		factoryName = factoryClass.getName();
	Method		singleton = factoryClass.getMethod(
						"getInstance",
						new Class[] { String.class });
	RegionalCenterFactory	factory;

	factory = (RegionalCenterFactory)singleton.invoke(
						null,
						new String[] { factoryName });
	System.out.println(factory);

	Timer		timer = new Timer();
	Map		nameMap = NameMap.getNameMap();
	Bean		bean;

	factory.beginTransaction(ds, false, false);
	try
	{
	    timer.start();
	    bean = factory.getRegionalCenterOfDepot(depotID, nameMap);
	    timer.stop();
	    if (bean == null)
	    {
		System.err.println("No data");
		return;
	    }
	    System.out.println("Query takes " + timer);

	    Class		beanClass = bean.getClass();
	    PrintWriter		out = new PrintWriter(System.out);

	    System.out.println("XML");
	    new XMLFormatWriter(bean).format(out, 1);

	    System.out.println(factory.getRegionalCenters(0, 100, order));
	    System.out.println("There are " +
				factory.getRegionalCenterCount() +
				" regional centers");
	}
	finally
	{
	    factory.commitTransaction();
	}
    }
}

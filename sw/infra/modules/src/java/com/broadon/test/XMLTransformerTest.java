package com.broadon.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.StringWriter;

import com.broadon.xml.XMLOutputTransformer;

/**
 * The <c>XMLTest</c> class tests the XMLOutputTransformer functionalities.
 *
 * @version	$Revision: 1.1 $
 */
public class XMLTransformerTest
{
    private static String readFile(String fileName)
	throws Throwable
    {
	FileInputStream	in = new FileInputStream(fileName);
	StringWriter	writer = new StringWriter();
	byte[]		buffer = new byte[1024];
	int		n = in.read(buffer);

	while (n > 0)
	{
	    writer.write(new String(buffer, 0, n));
	    n = in.read(buffer);
	}
	return writer.toString();
    }

    /**
     * Tests the XMLOutputTransformer functionalities.
     *
     * @param	args
     *			args[0]		the name of the xml file
     *			args[1]		the name of the xsl file
     */
    public static final void main(String[] args)
	throws Throwable
    {
	if (args.length != 2)
	{
	    System.err.println("Usage: java " +
				XMLTransformerTest.class.getName() +
				" <xml file> <xsl file>");
	    System.exit(-1);
	}

	String			xmlFile = args[0];
	String			xslFile = args[1];
	XMLOutputTransformer	transformer = new XMLOutputTransformer();
	/*
	FileInputStream		xmlStream = new FileInputStream(xmlFile);

	transformer.setStyleSource(new File(xslFile));
	transformer.transform(xmlStream, System.out);
	*/
	String			xml = readFile(xmlFile);
	String			xsl = readFile(xslFile);

	transformer.setStyleSource(xsl);
	System.out.println("Transformed:");
	System.out.println(transformer.transform(xml));
    }
}

package com.broadon.test;

import java.util.List;
import java.util.Vector;

import com.broadon.util.Timer;
import com.broadon.util.Queue;

/**
 * The <code>MTQueueTest</code> tests the Queue functions using threads.
 */
public class MTQueueTest
{
    static MTQueueTest		test = new MTQueueTest();

    /**
     * Uses multiple threads to make sure the Queue is thread-safe.
     *
     * @param	args
     *			args[0]		the number of items to be queued
     *			args[1]		the number of loops to run
     *			args[2]		the number of threads
     */
    public static final void main(String[] args)
	throws Throwable
    {
	int		size = 10000;
	int		loops = 100;
	int		totalThreads = 2;

	if (args.length >= 1)
	{
	    size = Integer.parseInt(args[0]);
	    if (args.length > 1)
		loops = Integer.parseInt(args[1]);
	    if (args.length > 2)
		totalThreads = Integer.parseInt(args[2]);
	}
	/*
	 * Create items.
	 */
	List		data = new Vector(size);

	for (int n = 0; n < size; n++)
	    data.add(new Integer(n));
	/*
	 * Start looping.
	 */
	Queue		queue = new Queue();
	boolean		succeed = true;
	Timer		timer = new Timer();

	System.out.println("MTQueueTest[" + size + ", " + loops +
			   ", " + totalThreads + "]");
	timer.start();
	for (int i = 0; i <= loops; i++)
	{
	    boolean	pg = !((i % 2) > 0);
	    boolean	cg = !((i % 3) > 0);
	    Producer[]	producers = new Producer[totalThreads];
	    Consumer[]	consumers = new Consumer[totalThreads];
	    int		n;

	    /*
	     * Start the child threads.
	     */
	    for (n = 0; n < totalThreads; n++)
	    {
		producers[n] = test.new Producer(queue, size, pg, data);
		consumers[n] = test.new Consumer(queue, size, cg, totalThreads);
	    }
	    if ((i % 2) == 0)
	    {
		for (n = 0; n < totalThreads; n++)
		    producers[n].start();
		for (n = 0; n < totalThreads; n++)
		    consumers[n].start();
	    }
	    else
	    {
		for (n = 0; n < totalThreads; n++)
		    consumers[n].start();
		for (n = 0; n < totalThreads; n++)
		    producers[n].start();
	    }
	    /*
	     * Wait for children to complete.
	     */
	    for (n = 0; n < totalThreads; n++)
		producers[n].join();
	    for (n = 0; n < totalThreads; n++)
		consumers[n].join();
	    /*
	     * Verify.
	     */
	    if (queue.size() != 0)
	    {
		System.err.println("Loop " + i + " leftover = " + queue.size());
		succeed = false;
	    }
	}
	timer.stop();
	System.out.print("MTQueueTest[" + timer.getTotalTime() + "ms] ");
	System.out.println(succeed ? "passed" : "failed");
    }

    /**
     * The <code>Producer</code> class provides the mechanism to act as
     * a producer of the Queue, i.e., putting items into the Queue.
     */
    private class Producer
	extends Thread
    {
	private Queue	queue;
	private int	max;
	private boolean	group;
	private List	data;

	/**
	 * Constructs the Producer object.
	 *
	 * @param	queue		the queue used by this producer
	 * @param	max		the maximum number of objects produced
	 * @param	group		group operation?
	 * @param	data		the data to be enqueued
	 */
	private Producer(Queue queue, int max, boolean group, List data)
	{
	    this.queue = queue;
	    this.max = max;
	    this.group = group;
	    this.data = data;
	}

	/**
	 * Generates objects.
	 */
	public void run()
	{
	    if (group)
		queue.enqueue(data);
	    else
	    {
		for (int n = 0; n < max; n++)
		    queue.enqueue(data.get(n));
	    }
	}
    }

    /**
     * The <code>Consumer</code> class provides the mechanism to act as
     * a consumer of the Queue, i.e., getting items out from the Queue.
     */
    private class Consumer
	extends Thread
    {
	private Queue	queue;
	private int	max;
	private int	totalThreads;
	private boolean	group;

	/**
	 * Constructs the Consumer object.
	 *
	 * @param	queue		the queue used by this unit
	 * @param	max		the maximum number of objects produced
	 * @param	group		group operation?
	 */
	private Consumer(Queue queue, int max, boolean group, int totalThreads)
	{
	    this.queue = queue;
	    this.max = max;
	    this.group = group;
	    this.totalThreads = totalThreads;
	}

	/**
	 * Waits for objects.
	 */
	public void run()
	{
	    if (group)
	    {
		int	n = 0;
		int	currentMax = max;

		while (n < max)
		{
		    Object[]	objects = queue.dequeueAll(true, currentMax);

		    if (totalThreads > 1)
			n += objects.length;
		    else
		    {
			/*
			 * Verify.
			 */
			int		i;

			for (i = 0; i < objects.length; i++, n++)
			{
			    Integer	value = (Integer)objects[i];

			    if (value.intValue() != n)
			    {
				System.err.println("Queue: Out of order @" + n +
						"[" + value.intValue() + "]");
				break;
			    }
			}
			if (i < objects.length)
			    break;
		    }
		    currentMax -= objects.length;
		}
	    }
	    else
	    {
		for (int n = 0; n < max; n++)
		{
		    Integer	value = (Integer)queue.dequeue(true);

		    if (totalThreads == 1 && value.intValue() != n)
		    {
			System.err.println("Queue: Out of order @" + n +
					    "[" + value.intValue() + "]");
			break;
		    }
		}
	    }
	}
    }
}

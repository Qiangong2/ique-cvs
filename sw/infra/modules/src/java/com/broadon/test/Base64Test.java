package com.broadon.test;

import java.io.FileInputStream;

import com.broadon.util.Base64;

/**
 * The <c>Base64Test</c> tests the Base64 encoding and decoding.
 *
 * @version	$Revision: 1.1 $
 */
public class Base64Test
{
    /**
     * Test the Base64 encoding or decoding based on the arguments.
     *
     * @param	args
     *			args[0]		the name of file to be encoded or
     *					decoded
     *			args[1]		decode when present, encode otherwise
     */
    public static final void main(String[] args)
	throws Throwable
    {
	if (args == null)
	{
	    System.err.println(
		"Usage: java com.broadon.test.cds.Base64Test <file> [d]");
	    System.exit(-1);
	}

	String			fileName = args[0];
	boolean			decode = args.length > 1;
	FileInputStream		in = new FileInputStream(fileName);
	Base64			base64 = new Base64();

	if (decode)
	{
	    /*
	     * Decode.
	     */
	    base64.decode(in, System.out);
	}
	else
	{
	    /*
	     * Encode.
	     */
	    base64.encode(in, System.out);
	}
    }
}

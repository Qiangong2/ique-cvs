package com.broadon.test;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;

import com.broadon.bean.Bean;
import com.broadon.bean.XMLFormatWriter;
import com.broadon.db.DBAccessFactory;
import com.broadon.db.DBException;
import com.broadon.db.RegionalCenterID;
import com.broadon.db.RegionalServer;
import com.broadon.db.RegionalServerFactory;
import com.broadon.util.Timer;

/**
 * The <code>RegionalServerTest</code> class tests the subclasses of
 * RegionalServer class.
 * The data are retrieved from the database.
 *
 * @version	$Revision: 1.1 $
 */
public class RegionalServerTest
{
    /**
     * Tests the PBean instances.
     *
     * @param	args
     *			args[0]		the depot id
     */
    public static final void main(String[] args)
	throws Throwable
    {
	int		regionalCenterID = 1;
	String		serverType = null;

	if (args.length > 0)
	{
	    regionalCenterID = Integer.parseInt(args[0]);
	    if (args.length > 1)
	    {
		serverType = args[1];
	    }
	}
	System.out.println("RegionalCenterID[" + regionalCenterID + "]");
	/*
	 * Connect to database.
	 */
	DataSource	ds;

	ds = OracleDS.createDataSource(
			"jdbc:oracle:thin:@db1.beta.broadon.com:1521:TBBU",
			"xs",
			"xs");
	/*
	 * Start testing.
	 */
	Class		factoryClass = RegionalServerFactory.class;
	String		factoryName = factoryClass.getName();
	Method		singleton = factoryClass.getMethod(
						"getInstance",
						new Class[] { String.class });
	RegionalServerFactory	factory;

	factory = (RegionalServerFactory)singleton.invoke(
						null,
						new String[] { factoryName });
	System.out.println(factory);

	Timer		timer = new Timer();
	Map		nameMap = NameMap.getNameMap();
	Bean		bean;

	factory.beginTransaction(ds, false, false);
	try
	{
	    timer.start();
	    if (serverType != null)
	    {
		bean = factory.getRegionalServer(regionalCenterID,
						 serverType,
						 nameMap);
		timer.stop();
		if (bean == null)
		{
		    System.err.println("No data");
		    return;
		}
		System.out.println("Query takes " + timer);

		Class		beanClass = bean.getClass();
		PrintWriter		out = new PrintWriter(System.out);

		System.out.println("XML");
		new XMLFormatWriter(bean).format(out, 1);
	    }
	    else
	    {
		Bean[]	beans = factory.getRegionalServers(regionalCenterID,
							   nameMap);
		timer.stop();
		if (beans == null)
		{
		    System.err.println("No data");
		    return;
		}
		System.out.println("Query takes " + timer);

		int	count = beans.length;

		for (int n = 0; n < count; n++)
		{
		    Class	beanClass = beans[n].getClass();
		    PrintWriter	out = new PrintWriter(System.out);

		    System.out.println("XML");
		    new XMLFormatWriter(beans[n]).format(out, 1);
		}
	    }
	}
	finally
	{
	    factory.commitTransaction();
	}
    }
}

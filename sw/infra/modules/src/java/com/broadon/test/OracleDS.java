package com.broadon.test;

import java.sql.SQLException;
import javax.sql.DataSource;

import com.broadon.db.OracleDataSource;

/**
 * This <code>OracleDS</code> class isolates the Oracle dependencies
 * into a single place for testing.
 */
public class OracleDS
{
    /**
     * Creates the data source.
     */
    public static DataSource createDataSource()
	throws SQLException
    {
	return OracleDataSource.createDataSource(
				"xs",
				"xs",
				"jdbc:oracle:thin:@lab1-db2:1521:bbdb2");
    }

    /**
     * Creates the data source.
     */
    public static DataSource createDataSource(String url,
					      String user,
					      String password)
	throws SQLException
    {
	return OracleDataSource.createDataSource( user, password, url);
    }
}

package com.broadon.test;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;

import com.broadon.bean.Bean;
import com.broadon.bean.XMLFormatWriter;
import com.broadon.db.DBAccessFactory;
import com.broadon.db.DBException;
import com.broadon.db.DepotID;
import com.broadon.db.Title;
import com.broadon.db.TitleFactory;
import com.broadon.util.Timer;

/**
 * The <code>TitleTest</code> class tests the subclasses of
 * Title class.
 * The data are retrieved from the database.
 *
 * @version	$Revision: 1.1 $
 */
public class TitleTest
{
    /**
     * Tests the PBean instances.
     *
     * @param	args
     *			args[0]		the title id
     */
    public static final void main(String[] args)
	throws Throwable
    {
	long		titleID = 1L;

	if (args.length > 0)
	{
	    titleID = Long.parseLong(args[0]);
	}
	System.out.println("titleID[" + titleID + "]");
	/*
	/*
	 * Connect to database.
	 */
	DataSource	ds;

	ds = OracleDS.createDataSource();
	/*
	 * Start testing.
	 */
	Class		factoryClass = TitleFactory.class;
	String		factoryName = factoryClass.getName();
	Method		singleton = factoryClass.getMethod(
						"getInstance",
						new Class[] { String.class });
	TitleFactory	factory;

	factory = (TitleFactory)singleton.invoke(
						null,
						new String[] { factoryName });
	System.out.println(factory);

	Map		nameMap = NameMap.getNameMap();
	String		xml;
	int		count;

	factory.beginTransaction(ds, false, false);
	try
	{
	    xml = factory.getTitle(titleID);
	    System.out.println(xml);
	    xml = factory.getTitleName(titleID);
	    System.out.println(xml);
	    xml = factory.getTitleNames();
	    System.out.println(xml);
	    xml = factory.getTitles(titleID, 0, 100, "cat");
	    System.out.println(xml);
	    xml = factory.getTitles(0, 100, "cat");
	    System.out.println(xml);
	    count = factory.getTitleCount(titleID);
	    System.out.println("There are " + count + " titles for " + titleID);
	    count = factory.getTitleCount();
	    System.out.println("There are " + count + " titles");
	}
	finally
	{
	    factory.commitTransaction();
	}
    }
}

package com.broadon.test;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;

import com.broadon.bean.Bean;
import com.broadon.bean.XMLFormatWriter;
import com.broadon.db.DBAccessFactory;
import com.broadon.db.DBException;
import com.broadon.db.DepotID;
import com.broadon.db.TitleRegion;
import com.broadon.db.TitleRegionFactory;
import com.broadon.util.Timer;

/**
 * The <code>TitleRegionTest</code> class tests the subclasses of
 * Region class.
 * The data are retrieved from the database.
 *
 * @version	$Revision: 1.1 $
 */
public class TitleRegionTest
{
    /**
     * Tests the PBean instances.
     *
     * @param	args
     *			args[0]		the title id
     */
    public static final void main(String[] args)
	throws Throwable
    {
	long		titleID = 1L;

	if (args.length > 0)
	{
	    titleID = Long.parseLong(args[0]);
	}
	System.out.println("titleID[" + titleID + "]");
	/*
	 * Connect to database.
	 */
	DataSource	ds;

	ds = OracleDS.createDataSource();
	/*
	 * Start testing.
	 */
	Class		factoryClass = TitleRegionFactory.class;
	String		factoryName = factoryClass.getName();
	Method		singleton = factoryClass.getMethod(
						"getInstance",
						new Class[] { String.class });
	TitleRegionFactory	factory;

	factory = (TitleRegionFactory)singleton.invoke(
						null,
						new String[] { factoryName });
	System.out.println(factory);

	Timer		timer = new Timer();
	Map		nameMap = NameMap.getNameMap();
	String		xml;

	factory.beginTransaction(ds, false, false);
	try
	{
	    timer.start();
	    xml = factory.getTitleRegions(titleID);
	    timer.stop();
	    if (xml == null)
	    {
		System.err.println("No data");
		return;
	    }
	    System.out.println("Query takes " + timer);
	    System.out.println(xml);
	}
	finally
	{
	    factory.commitTransaction();
	}
    }
}

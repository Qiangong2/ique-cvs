package com.broadon.test;

import com.broadon.util.Timer;
import com.broadon.util.Queue;

/**
 * The <code>QueueTest</code> tests the Queue functionalities.
 */
public class QueueTest
{
    /**
     * Test the queue functionalities.
     *
     * @param	args
     *			args[0]		the queue size
     *			args[1]		the number of loops to run
     */
    public static final void main(String[] args)
    {
	int		size = 1000;
	int		loops = 2000;

	if (args.length >= 1)
	{
	    size = Integer.parseInt(args[0]);
	    if (args.length > 1)
		loops = Integer.parseInt(args[1]);
	}
	System.out.println("Queue size[" + size + "] loops[" + loops + "]");

	Integer[]	data = new Integer[size];
	Queue		queue = new Queue(size);
	boolean		succeed = true;
	Timer		timer = new Timer();

	timer.start();

	for (int i = 0; i < loops; i++)
	{
	    int		n;

	    /*
	     * Initialize and enqueue.
	     */
	    if (i == 0)
	    {
		for (n = 0; n < data.length; n++)
		{
		    data[n] = new Integer(n);
		    queue.enqueue(data[n]);
		}
	    }
	    else if (i == data.length)
	    {
		for (n = 0; n < data.length; n++)
		{
		    data[n] = new Integer(data.length - n);
		    queue.enqueue(data[n]);
		}
	    }
	    else
	    {
		for (n = 0; n < data.length; n++)
		{
		    data[n] = new Integer(
					(int)Math.round(
						Math.random()*100000));
		    queue.enqueue(data[n]);
		}
	    }
	    /*
	     * Verify.
	     */
	    if (queue.size() != data.length)
	    {
		System.out.print("count = " + data.length);
		System.out.println(" size = " + queue.size());
		succeed = false;
	    }
	    /*
	     * Dequeue.
	     */
	    for (n = 0; n < data.length; n++)
	    {
		Integer	datum = (Integer)queue.dequeue();

		if (!datum.equals(data[n]))
		{
		    System.out.println("Wrong order@" + n + "[" + datum +
				       ", " + data[n] + "]");
		    succeed = false;
		}
	    }
	    /*
	     * Verify.
	     */
	    if (queue.size() != 0)
	    {
		System.out.println("leftover = " + queue.size());
		succeed = false;
	    }
	}
	timer.stop();
	System.out.print("QueueTest[" + timer.getTotalTime() + "ms] ");
	System.out.println(succeed ? "passed" : "failed");
    }
}

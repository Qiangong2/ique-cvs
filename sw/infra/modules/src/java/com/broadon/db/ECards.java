package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <c>ECards</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *<p>
 * It contains information about an ECards.
 *
 * @version	$Revision: 1.2 $
 */
public class ECards
    extends PBean
{
    private Integer		ECardID;
    private Integer		ECardTypeID;
    private Integer		EUnitsRedeemed;
    private Integer		businessUnitID;
    private Integer		isUsable;
    private Date		activateDate;
    private Date		lastRedeemDate;
    private Date		revokeDate;

    /**
     * Constructs an empty ECards instance.
     */
    public ECards()
    {
	this.ECardID = null;
	this.ECardTypeID = null;
	this.EUnitsRedeemed = null;
	this.businessUnitID = null;
	this.isUsable = null;
	this.activateDate = null;
	this.lastRedeemDate = null;
	this.revokeDate = null;
    }

    /**
     * Getter for ECardID.
     *
     * @return	The ECard ID identifier.
     */
    public Integer getECardID()
    {
	return ECardID;
    }

    /**
     * Setter for ECardID.
     *
     * @param	ECardID		the ECard ID identifier
     */
    public void setECardID(Integer ECardID)
    {
	this.ECardID = ECardID;
    }

    /**
     * Getter for ECardTypeID.
     *
     * @return	The ECard Type identifier.
     */
    public Integer getECardTypeID()
    {
	return ECardTypeID;
    }

    /**
     * Setter for ECardTypeID.
     *
     * @param	ECardTypeID		the ECard type identifier
     */
    public void setECardTypeID(Integer ECardTypeID)
    {
	this.ECardTypeID = ECardTypeID;
    }

    /**
     * Getter for EUnitsRedeemed.
     *
     * @return	The redeemed monetary value.
     */
    public Integer getEUnitsRedeemed()
    {
	return EUnitsRedeemed;
    }

    /**
     * Setter for EUnitsRedeemed.
     *
     * @param	EUnitsRedeemed	the monetary value redeemed
     */
    public void setEUnitsRedeemed(Integer EUnitsRedeemed)
    {
	this.EUnitsRedeemed = EUnitsRedeemed;
    }

    /**
     * Getter for businessUnitID.
     *
     * @return	The bu identifier.
     */
    public Integer getBusinessUnitID()
    {
	return businessUnitID;
    }

    /**
     * Setter for businessUnitID.
     *
     * @param	businessUnitID		the bu identifier
     */
    public void setBusinessUnitID(Integer businessUnitID)
    {
	this.businessUnitID = businessUnitID;
    }

    /**
     * Getter for isUsable.
     *
     * @return Is this type of ECards be used ?
     */
    public Integer getIsUsable()
    {
	return isUsable;
    }

    /**
     * Setter for isUsable.
     *
     * @param	isUsable		true  => can be used ;
     *					false => cannot be used
     */
    public void setIsUsable(Integer isUsable)
    {
	this.isUsable = isUsable;
    }

    /**
     * Getter fo activateDate.
     *
     * @return	The activate date of this ECard
     */
    public Date getActivateDate()
    {
	return activateDate;
    }

    /**
     * Setter for activateDate.
     *
     * @param	activateDate		the activate date of this
     *					ECard
     */
    public void setActivateDate(Date activateDate)
    {
	this.activateDate = activateDate;
    }

    /**
     * Getter for lastRedeemDate.
     *
     * @return	The last redeem date of this ECard.
     */
    public Date getlastRedeemDate()
    {
	return lastRedeemDate;
    }

    /**
     * Setter for lastRedeemDate.
     *
     * @param	lastRedeemDate		the last redemeed date of this
     *					ECard-type
     */
    public void setLastRedeemDate(Date lastRedeemDate)
    {
	this.lastRedeemDate = lastRedeemDate;
    }

    /**
     * Getter for revokeDate.
     *
     * @return	The revoke date of this ECard
     */
    public Date getRevokeDate()
    {
	return revokeDate;
    }

    /**
     * Setter for revokeDate.
     *
     * @param	revokeDate		the revoke date of this
     *					ECard
     */
    public void setRevokeDate(Date revokeDate)
    {
	this.revokeDate = revokeDate;
    }
}

package com.broadon.db;

public interface ServerName
{
    static final String S_CDDS = "CDDS";
    static final String S_CDS = "CDS";
    static final String S_NTP = "NTP";
    static final String S_RMS = "RMS";
    static final String S_XS = "XS";
    static final String S_OPS = "OPS";
    static final String S_IS = "IS";

    static final String[] serverList = {
	S_CDDS, S_CDS, S_NTP, S_RMS, S_XS, S_OPS, S_IS
    };
}

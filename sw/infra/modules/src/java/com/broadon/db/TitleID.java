package com.broadon.db;

import com.broadon.sql.PredicateItem;

/**
 * The <c>TitleID</c> class conforms to the JavaBeans property,
 * and is a Predicate that finds the Title identifiy by the title
 * identifier.
 *
 * @version	$Revision: 1.1 $
 */
public class TitleID
    extends PredicateItem
{
    private Long	titleID;

    /**
     * Constructs a TitleID type instance.
     *
     * @param	titleID			the title identifier
     */
    public TitleID(long titleID)
    {
	super();
	this.titleID = new Long(titleID);
    }

    /**
     * Getter for titleID.
     *
     * @return	The title identifier.
     */
    public Long getTitleID()
    {
	return titleID;
    }

    /**
     * Setter for titleID.
     *
     * @param	titleID			the title identifier
     */
    public void setTitleID(Long titleID)
    {
	this.titleID = titleID;
    }
}

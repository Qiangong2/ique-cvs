package com.broadon.db;

import com.broadon.sql.PredicateItem;

/**
 * The <c>RegionID</c> class conforms to the JavaBeans property,
 * and is a Predicate that finds various records.
 *
 * @version	$Revision: 1.1 $
 */
public class RegionID
    extends PredicateItem
{
    private Integer	regionID;

    /**
     * Constructs a RegionID instance.
     *
     * @param	regionID		the region identifier
     */
    public RegionID(int regionID)
    {
	super();
	this.regionID = new Integer(regionID);
    }

    /**
     * Getter for regionID.
     *
     * @return	The region identifier.
     */
    public Integer getRegionID()
    {
	return regionID;
    }

    /**
     * Setter for regionID.
     *
     * @param	regionID		the region identifier
     */
    public void setRegionID(Integer regionID)
    {
	this.regionID = regionID;
    }
}

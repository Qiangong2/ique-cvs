package com.broadon.db;

import java.util.Date;

/**
 * The <c>ContentDownloadLog</c> class conforms to the JavaBeans property,
 * and is mainly used for data transformation.
 *<p>
 * It contains information about a content download log identified by the
 * depot identifier, content identifier, and the request date.
 *
 * @version	$Revision: 1.1 $
 */
public class ContentDownloadLog
    extends AuditLog
{
    private Long		depotID;
    private Long		contentID;
    private Date		completionDate;

    /**
     * Constructs an empty ContentDownloadLog instance.
     */
    public ContentDownloadLog()
    {
	super();
    }

    /**
     * Getter for depotID.
     *
     * @return	The depot identifier.
     */
    public Long getDepotID()
    {
	return depotID;
    }

    /**
     * Setter for depotID.
     *
     * @param	depotID			the depot identifier
     */
    public void setDepotID(Long depotID)
    {
	this.depotID = depotID;
    }

    /**
     * Getter for contentID.
     *
     * @return	The content idenifier whose content has been downloaded.
     */
    public Long getContentID()
    {
	return contentID;
    }

    /**
     * Setter for contentID.
     *
     * @param	contentID		the content idenifier whose content
     *					has been downloaded
     */
    public void setContentID(Long contentID)
    {
	this.contentID = contentID;
    }

    /**
     * Getter for completionDate.
     *
     * @return	The completion date of downloading.
     */
    public Date getCompletionDate()
    {
	return completionDate;
    }

    /**
     * Setter for completionDate.
     *
     * @param	completionDate		the completion date of downloading
     */
    public void setCompletionDate(Date completionDate)
    {
	this.completionDate = completionDate;
    }
}

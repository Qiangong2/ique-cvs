package com.broadon.db;

import java.io.IOException;

import com.broadon.bean.Bean;

/**
 * The <c>BBUBBPlayerFactory</c> class interfaces with the database to
 * query records from the bb player table.
 * Each record is represented by a BBUBBPlayer JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.3 $
 */
public class BBUBBPlayerFactory
    extends DBAccessFactory
{
    private static final String TABLE_NAME;
    private static final String SELECT_BBU_BB_PLAYER_LIST;
    private static final String SELECT_BBU_BB_PLAYER_COUNT;

    static
    {
	TABLE_NAME = "BBU_BB_PLAYERS";
        SELECT_BBU_BB_PLAYER_LIST =
            "SELECT a.bb_id, a.bb_hwrev, a.bb_model, a.bundle_start_date, a.bu_id, " +
            "b.business_name, a.public_key, a.manufacture_date, a.sn FROM " + TABLE_NAME +
            " a, BUSINESS_UNITS b WHERE a.bu_id = b.bu_id";
        SELECT_BBU_BB_PLAYER_COUNT = "select count(*) FROM " + TABLE_NAME;
    }


    /**
     * Constructs a BBUBBPlayerFactory instance.
     */
    protected BBUBBPlayerFactory()
    {
	super();
    }

    /**
     * Returns the bb player information identified by the given bbID.
     *
     * @param	bbID			the bb player identifier
     *
     * @return	The XML document that contains information of a
     *		BBUBBPlayer record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getBBUBBPlayer(long bbID)
	throws DBException, IOException
    {
	return query(SELECT_BBU_BB_PLAYER_LIST + " AND a.bb_id = " + bbID);
    }

    /**
     * Returns the bb player information identified by the given bbID.
     *
     * @param	bbID			the bb player identifier
     *
     * @return	The XML document that contains information of a
     *		BBUBBPlayer record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getBBUBBPlayer(String bbID)
	throws DBException, IOException
    {
	return query(SELECT_BBU_BB_PLAYER_LIST + " AND a.bb_id = " + bbID);
    }

    /** Returns the number of bb players available.
     *
     * @return	The integer that contains the number of bb players.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getBBUBBPlayerCount()
	throws DBException, IOException
    {
	return count(SELECT_BBU_BB_PLAYER_COUNT);
    }

    /**
     * Returns the database table name to be used.
     *
     * @return The data table name.
     */
    public String getTableName()
    {
        return TABLE_NAME;
    }

    /**
     * Creates a BBPlayer JavaBeans object.
     *
     * @return The newly created BBPlayer object.
     */
    protected Bean newBean()
    {
        return new BBPlayer();
    }
}

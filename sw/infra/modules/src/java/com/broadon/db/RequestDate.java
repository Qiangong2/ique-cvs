package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PredicateItem;

/**
 * The <c>RequestDate</c> class conforms to the JavaBeans property, and is
 * a Predicate that finds records that have and satisfy the given value of
 * the request-date attribute.
 *
 * @version	$Revision: 1.1 $
 */
public class RequestDate
    extends PredicateItem
{
    private Date	time;

    /**
     * Constructs a RequestDate instance.
     *
     * @param	time			the matching time
     */
    public RequestDate(Date time)
    {
	this(time, "=");
    }

    /**
     * Constructs a RequestDate instance.
     *
     * @param	time			the matching time
     * @param	operator		the operator
     */
    public RequestDate(Date time, String operator)
    {
	super(operator);
	this.time = time;
    }

    /**
     * Getter for requestDate.
     *
     * @return	The time of request.
     */
    public Date getRequestDate()
    {
	return time;
    }

    /**
     * Setter for requestDate.
     *
     * @param	time			the time of request
     */
    public void setRequestDate(Date time)
    {
	this.time = time;
    }
}

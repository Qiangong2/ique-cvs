package com.broadon.db;

import java.io.IOException;
import java.util.Map;

import com.broadon.bean.Bean;
import com.broadon.sql.Predicate;

/**
 * The <c>DataLoadRequestFactory</c> class interfaces with the database to
 * create, update, or query records from the DataLoadRequests table.
 * Each record is represented by a DataLoadRequest JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.2 $
 */
public class DataLoadRequestFactory
    extends SyncableFactory
{
    private static final String TABLE_NAME;
    private static final String	GET_EMAIL_ADDRESS_COND;
    private static final String	SELECT_DATA_LOAD_REQUEST_LIST;
    private static final String	SELECT_DATA_LOAD_REQUEST_COUNT;

    static
    {
	TABLE_NAME		= "DATA_LOAD_REQUESTS";
	GET_EMAIL_ADDRESS_COND	= "op.operator_id = dlr.operator_id";
	SELECT_DATA_LOAD_REQUEST_LIST	=
	    "select dlr.request_id, dlr.request_date, dlr.operator_id, " +
                "op.email_address, dlr.role_level, dlr.job_name, dlr.validate_date, " +
		"dlr.process_date, dlr.process_status, dlr.process_xml from " + TABLE_NAME +
                " dlr, OPERATION_USERS op";
	SELECT_DATA_LOAD_REQUEST_COUNT	= "select count(*) from " + TABLE_NAME;
    }

    /**
     * Constructs a DataLoadRequestFactory instance.
     */
    protected DataLoadRequestFactory()
    {
	super();
    }

    /**
     * Returns the request information identified by the given requestID.
     *
     * @param	requestID		the request identifier
     * @param	nameMap			the name mapping table
     *
     * @return	The DataLoadRequest JavaBeans that contains information of a
     *		DataLoadRequests record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public DataLoadRequest getDataLoadRequest(long requestID, Map nameMap)
	throws DBException, IOException
    {
	DataLoadRequestID		predicate = new DataLoadRequestID(requestID);

	return (DataLoadRequest)queryUnique(predicate, nameMap);
    }

    /**
     * Returns the request information identified by the given requestID.
     *
     * @param	requestID			the request identifier
     *
     * @return	The XML document that contains information of a
     *		DataLoadRequests record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getDataLoadRequest(long requestID)
	throws DBException, IOException
    {
	return query(SELECT_DATA_LOAD_REQUEST_LIST + " where dlr.request_id = " + 
			requestID + " and " + GET_EMAIL_ADDRESS_COND);
    }

    /**
     * Returns the request information identified by the given array of
     * requestIDs.
     *
     * @param	requestIDs		the request identifiers
     * @param	nameMap			the name mapping table
     *
     * @return	The array of DataLoadRequest JavaBeans, each contains
     *		all information of a DataLoadRequests record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public DataLoadRequest[] getDataLoadRequests(long[] requestIDs, Map nameMap)
	throws DBException, IOException
    {
	if (requestIDs == null)
	{
	    return null;
	}

	int		size = requestIDs.length;

	if (size == 0)
	{
	    return null;
	}
	/*
	 * Set up the predicate.
	 */
	DataLoadRequestID		requestID = new DataLoadRequestID(requestIDs[0]);
	Predicate	predicate = requestID;

	for (int n = 1; n < size; n++)
	{
	    requestID = (DataLoadRequestID)requestID.or(new DataLoadRequestID(requestIDs[n]));
	}
	/*
	 * Query.
	 */
	return (DataLoadRequest[])query(predicate, nameMap);
    }

    /**
     * Returns the request information of a given range.
     *
     * @param	requestID		the request identifier
     * @param	from			the starting record number
     * @param	to			the ending record number
     * @param	order			the sorted order
     *
     * @return	The XML document that contains the tilte information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getDataLoadRequests(String roleLevel, int from, int to, String order)
	throws DBException, IOException
    {
	String	sql = "select * from (select rownum no, inner.* from (" +
			SELECT_DATA_LOAD_REQUEST_LIST +
			    " where dlr.role_level = " + roleLevel + " and " +
			    GET_EMAIL_ADDRESS_COND + " order by " + order + ") " +
			"inner where rownum <= " + to + ") " +
			"where no > " + from;

	return query(sql);
    }

    /**
     * Returns the request information of a given range.
     *
     * @param	from			the starting record number
     * @param	to			the ending record number
     * @param	order			the sorted order
     *
     * @return	The XML document that contains the tilte information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getDataLoadRequests(int from, int to, String order)
	throws DBException, IOException
    {
	String	sql = "select * from (select rownum no, inner.* from (" +
			SELECT_DATA_LOAD_REQUEST_LIST + " where " + 
     		        GET_EMAIL_ADDRESS_COND + " order by " + order + ") " +
			"inner where rownum <= " + to + ") " +
			"where no > " + from;
	return query(sql);
    }

    /**
     * Returns the number of requests available.
     *
     * @param	roleLevel	the role level
     *
     * @return	The integer that contains the number of requests.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getDataLoadRequestCount(long roleLevel)
	throws DBException, IOException
    {
	return count(SELECT_DATA_LOAD_REQUEST_COUNT + " where role_level = " + roleLevel);
    }

    /**
     * Returns the number of requests available.
     *
     * @param	roleLevel	the role level
     *
     * @return	The integer that contains the number of requests.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getDataLoadRequestCount(String roleLevel)
	throws DBException, IOException
    {
	return count(SELECT_DATA_LOAD_REQUEST_COUNT + " where role_level = " + roleLevel);
    }

    /**
     * Returns the number of requests available.
     *
     * @return	The integer that contains the number of requests.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getDataLoadRequestCount()
	throws DBException, IOException
    {
	return count(SELECT_DATA_LOAD_REQUEST_COUNT);
    }

    /**
     * Returns the database table name to be used.
     *
     * @return	The data table name.
     */
    public String getTableName()
    {
	return TABLE_NAME;
    }

    /**
     * Creates a DataLoadRequest JavaBeans object.
     *
     * @return	The newly created DataLoadRequest object.
     */
    protected Bean newBean()
    {
	return new DataLoadRequest();
    }
}

package com.broadon.db;

import com.broadon.sql.PredicateItem;

/**
 * The <c>DepotID</c> class conforms to the JavaBeans property,
 * and is a Predicate that finds the Depot identifiy by the depot
 * identifier.
 *
 * @version	$Revision: 1.1 $
 */
public class DepotID
    extends PredicateItem
{
    private Long	depotID;

    /**
     * Constructs a DepotID type instance.
     *
     * @param	depotID			the depot identifier
     */
    public DepotID(long depotID)
    {
	super();
	this.depotID = new Long(depotID);
    }

    /**
     * Getter for depotID.
     *
     * @return	The depot identifier.
     */
    public Long getDepotID()
    {
	return depotID;
    }

    /**
     * Setter for depotID.
     *
     * @param	depotID			the depot identifier
     */
    public void setDepotID(Long depotID)
    {
	this.depotID = depotID;
    }
}

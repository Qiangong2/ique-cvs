package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <c>ContentType</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *<p>
 * It contains information about a content object type.
 *
 * @version	$Revision: 1.2 $
 */
public class ContentType
    extends PBean
{
    private String		contentObjectType;
    private String		description;
    private Date		      createDate;

    /**
     * Constructs an empty ContentType instance.
     */
    public ContentType()
    {
	this.contentObjectType= null;
	this.createDate = null;
    }

    /**
     * Getter for contentObjectType.
     *
     * @return	The content object type.
     */
    public String getContentObjectType()
    {
	return contentObjectType;
    }

    /**
     * Setter for contentObjectType.
     *
     * @param	contentObjectType		the content object type
     */
    public void setContentObjectType(String contentObjectType)
    {
	this.contentObjectType = contentObjectType;
    }

    /**
     * Getter for description.
     *
     * @return	The description.
     */
    public String getDescription()
    {
	return description;
    }

    /**
     * Setter for description.
     *
     * @param	description		the description
     */
    public void setDescription(String description)
    {
	this.description = description;
    }

    /**
     * Getter for createDate.
     *
     * @return	The date of creation.
     */
    public Date getCreateDate()
    {
	return createDate;
    }

    /**
     * Setter for createDate.
     *
     * @param	createDate		the date of creation
     */
    public void setCreateDate(Date createDate)
    {
	this.createDate = createDate;
    }
}

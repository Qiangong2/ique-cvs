package com.broadon.db;

import java.io.IOException;
import java.util.Map;

import com.broadon.bean.Bean;

/**
 * The <code>StoreFactory</code> class interfaces with the database
 * to create, update, or query records from the Stores table.
 * Each record is represented by a Store JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.4 $
 */
public class StoreFactory
    extends DBAccessFactory
{
    private static final String TABLE_NAME;
    private static final String	SELECT_STORE_LIST;
    private static final String	SELECT_STORE_COUNT;

    static
    {
	TABLE_NAME		= "STORES";
	SELECT_STORE_LIST	=
	    "SELECT st.store_id, st.region_id, rg.region_name, " +
            "st.regional_center_id, rc.center_name, st.retailer_id, " +
            "rl.retailer_name, rl.category_code, st.commission_rate, st.account_balance, " +
            "st.create_date, st.revoke_date, st.suspend_date, st.timezone, " + 
            "st.address FROM " + TABLE_NAME + " st, REGIONS rg, RETAILERS rl, " +
            "REGIONAL_CENTERS rc WHERE rg.region_id = st.region_id AND " +
            "rc.regional_center_id = st.regional_center_id AND " +
            "rl.retailer_id = st.retailer_id";

	SELECT_STORE_COUNT	= "select count(store_id) from " + TABLE_NAME;
    }

    /**
     * Constructs a StoreFactory instance.
     */
    protected StoreFactory()
    {
	super();
    }

    /**
     * Returns the store identified by the given store identifier.
     *
     * @param	storeID		the store identifier
     * @param	nameMap			the name mapping table
     *
     * @return	The Store JavaBeans that contains information of a
     *		Storeal-Centers record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public Store getStore(int storeID, Map nameMap)
	throws DBException, IOException
    {
	return (Store)queryUnique(new StoreID(storeID), nameMap);
    }

    /**
     * Returns the store information of a given range.
     *
     * @param	from			the starting record number
     * @param	to			the ending record number
     * @param	order			the sorted order
     *
     * @return	The XML document that contains the store information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getStores(int from, int to, String order)
	throws DBException, IOException
    {
	String	sql = "select * from (select rownum no, inner.* from (" +
			SELECT_STORE_LIST +
			    " order by " + order + ") " +
			"inner where rownum <= " + to + ") " +
			"where no > " + from;

	return query(sql);
    }

   /**
     * Returns the store details with the given id.
     *
     * @param   storeID                 the store identifier
     *
     * @return The XML document describing the information of store
     *          information.
     *
     * @throws  DBException
     * @throws  IOException
     */
    public String getStore(String storeID)
        throws DBException, IOException
    {
        String  sql = SELECT_STORE_LIST +
                        " AND st.store_id = " + storeID;
        return query(sql);
    }

    /**
     * Returns the number of stores available.
     *
     * @param	storeID		the store identifier
     *
     * @return	The integer that contains the number of stores.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getStoreCount(long storeID)
	throws DBException, IOException
    {
	return count(SELECT_STORE_COUNT + " where store_id = " + storeID);
    }

    /**
     * Returns the number of stores available.
     *
     * @param	storeID		the store identifier
     *
     * @return	The integer that contains the number of stores.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getStoreCount(String storeID)
	throws DBException, IOException
    {
	return count(SELECT_STORE_COUNT + " where store_id = " + storeID);
    }

    /**
     * Returns the number of stores available.
     *
     * @return	The integer document that contains the number of stores.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getStoreCount()
	throws DBException, IOException
    {
	return count(SELECT_STORE_COUNT);
    }

    /**
     * Returns the store information of a given range for a retailer.
     *
     * @param	from			the starting record number
     * @param	to			the ending record number
     * @param	order			the sorted order
     *
     * @return	The XML document that contains the store information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getRetailerStores(int from, int to, String order, String retailerID)
	throws DBException, IOException
    {
	String	sql = "select * from (select rownum no, inner.* from (" +
			SELECT_STORE_LIST + " AND st.retailer_id = " + retailerID +
			    " order by " + order + ") " +
			"inner where rownum <= " + to + ") " +
			"where no > " + from;

	return query(sql);
    }

    /**
     * Returns the database table name to be used.
     *
     * @return	The data table name.
     */
    public String getTableName()
    {
	return TABLE_NAME;
    }

    /**
     * Creates a Store JavaBeans object.
     *
     * @return	The newly created Store object.
     */
    protected Bean newBean()
    {
	return new Store();
    }
}

package com.broadon.db;

import com.broadon.sql.PredicateItem;

/**
 * The <c>ContentTypeID</c> class conforms to the JavaBeans property,
 * and is a Predicate that finds various records.
 *
 * @version	$Revision: 1.1 $
 */
public class ContentTypeID
    extends PredicateItem
{
    private String	contentTypeID;

    /**
     * Constructs a ContentTypeID instance.
     *
     * @param	contentTypeID		the content object type identifier
     */
    public ContentTypeID(String contentTypeID)
    {
	super();
	this.contentTypeID = new String(contentTypeID);
    }

    /**
     * Getter for contentTypeID.
     *
     * @return	The content object type identifier.
     */
    public String getContentTypeID()
    {
	return contentTypeID;
    }

    /**
     * Setter for contentTypeID.
     *
     * @param	contentTypeID		the content object type identifier
     */
    public void setContentTypeID(String contentTypeID)
    {
	this.contentTypeID = contentTypeID;
    }
}

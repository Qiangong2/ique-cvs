package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <code>RegionalServer</code> class conforms to the JavaBeans property,
 * and is mainly used for data transformation.
 *<p>
 * It contains information about a regional server, identified by the
 * regional center identifier and server type.
 *
 * @version	$Revision: 1.2 $
 */
public class RegionalServer
    extends PBean
{
    private Integer		regionalCenterID;
    private String		serverType;
    private String		serverAddr;
    private Date		lastUpdated;

    /**
     * Constructs an empty RegionalServer instance.
     */
    public RegionalServer()
    {
    }

    /**
     * Getter for regionalCenterID.
     *
     * @return	The regional center identifier.
     */
    public Integer getRegionalCenterID()
    {
	return regionalCenterID;
    }

    /**
     * Setter for regionalCenterID.
     *
     * @param	regionalCenterID	the regional center identifier
     */
    public void setRegionalCenterID(Integer regionalCenterID)
    {
	this.regionalCenterID = regionalCenterID;
    }

    /**
     * Getter for serverType.
     *
     * @return	The type of this server.
     */
    public String getServerType()
    {
	return serverType;
    }

    /**
     * Setter for serverType.
     *
     * @param	serverType		the type of this server
     */
    public void setServerType(String serverType)
    {
	this.serverType = serverType;
    }

    /**
     * Getter for serverAddr.
     *
     * @return	The URI to access the server.
     */
    public String getServerAddr()
    {
	return serverAddr;
    }

    /**
     * Setter for serverAddr.
     *
     * @param	serverAddr		the URI to access the server
     */
    public void setServerAddr(String serverAddr)
    {
	this.serverAddr = serverAddr;
    }

    /**
     * Getter for lastUpdated.
     *
     * @return	The last updated date of this center.
     */
    public Date getLastUpdated()
    {
	return lastUpdated;
    }

    /**
     * Setter for lastUpdated.
     *
     * @param	lastUpdated		the last updated date of this center
     */
    public void setLastUpdated(Date lastUpdated)
    {
	this.lastUpdated = lastUpdated;
    }
}

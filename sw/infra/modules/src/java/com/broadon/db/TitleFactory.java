package com.broadon.db;

import java.io.IOException;
import java.util.Map;

import com.broadon.bean.Bean;
import com.broadon.sql.Predicate;

/**
 * The <c>TitleFactory</c> class interfaces with the database to
 * create, update, or query records from the Titles table.
 * Each record is represented by a Title JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.12 $
 */
public class TitleFactory
    extends SyncableFactory
{
    private static final String TABLE_NAME;
    private static final String VIEW_NAME;
    private static final String	SELECT_TITLES;
    private static final String	SELECT_TITLE_NAME;
    private static final String	SELECT_TITLE_ADD;
    private static final String	SELECT_TITLE_LIST;
    private static final String	SELECT_PURCHASE_TITLE_LIST;
    private static final String	SELECT_TITLE_COUNT;
    private static final String	SELECT_CONTENT_LIST;
    private static final String	SELECT_GLOBAL_ETICKET_TITLE_LIST;

    static
    {
	TABLE_NAME		= "CONTENT_TITLES";
	VIEW_NAME		= "CONTENT_TITLE_OBJECT_DETAIL_V";
	SELECT_TITLES		= "select title_id, title from " + 
		TABLE_NAME + " order by title";
	SELECT_TITLE_NAME	= "select title, title_id from " + TABLE_NAME;
	SELECT_TITLE_ADD	=
	    "select title_id, title from " + TABLE_NAME +
		" where lower(title_type)='visible' AND title_id not in " +
		    "(select unique a.title_id from " +
			TABLE_NAME + " a, " + "content_title_regions b " +
			"where a.title_id = b.title_id) " +
		    "order by title";
	SELECT_TITLE_LIST	=
	    "select title_id, title, title_type, category, publisher, " +
		"developer, last_updated from " + TABLE_NAME;
	SELECT_PURCHASE_TITLE_LIST	=
	    "select title_id, title, title_type, category, publisher, " +
		"developer, last_updated from " + TABLE_NAME + " WHERE lower(title_type)='visible'";
	SELECT_TITLE_COUNT	= "select count(title_id) from " + TABLE_NAME;
	SELECT_CONTENT_LIST	=
	    "select rownum no, content_id, publish_date, revoke_date, replaced_content_id, " +
            "content_object_type, content_size, content_object_name, " +
            "content_object_version, min_upgrade_version, upgrade_consent, last_updated from " + VIEW_NAME;
	SELECT_GLOBAL_ETICKET_TITLE_LIST = "SELECT ct.title, ct.title_id " + 
            "FROM CONTENT_TITLES ct, content_title_object_detail_v cv " +
            "WHERE lower(ct.title_type) = 'push' " + 
            "AND ct.title_id = cv.title_id " +
            "AND cv.has_eticket_metadata = 'Y' " +
            "AND cv.is_content_object_latest = 'Y' ";
    }

    /**
     * Constructs a TitleFactory instance.
     */
    protected TitleFactory()
    {
	super();
    }

    /**
     * Returns the title information identified by the given titleID.
     *
     * @param	titleID			the title identifier
     * @param	nameMap			the name mapping table
     *
     * @return	The Title JavaBeans that contains information of a
     *		Content-Titles record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public Title getTitle(long titleID, Map nameMap)
	throws DBException, IOException
    {
	TitleID		predicate = new TitleID(titleID);

	return (Title)queryUnique(predicate, nameMap);
    }

    /**
     * Returns the title information identified by the given titleID.
     *
     * @param	titleID			the title identifier
     *
     * @return	The XML document that contains information of a
     *		Content-Titles record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getTitle(long titleID)
	throws DBException, IOException
    {
	return query(SELECT_TITLE_LIST + " where title_id = " + titleID);
    }

    /**
     * Returns the title information identified by the given array of
     * titleIDs.
     *
     * @param	titleIDs		the title identifiers
     * @param	nameMap			the name mapping table
     *
     * @return	The array of Title JavaBeans, each contains
     *		all information of a Content-Titles record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public Title[] getTitles(long[] titleIDs, Map nameMap)
	throws DBException, IOException
    {
	if (titleIDs == null)
	{
	    return null;
	}

	int		size = titleIDs.length;

	if (size == 0)
	{
	    return null;
	}
	/*
	 * Set up the predicate.
	 */
	TitleID		titleID = new TitleID(titleIDs[0]);
	Predicate	predicate = titleID;

	for (int n = 1; n < size; n++)
	{
	    titleID = (TitleID)titleID.or(new TitleID(titleIDs[n]));
	}
	/*
	 * Query.
	 */
	return (Title[])query(predicate, nameMap);
    }

    /**
     * Returns the titles available to generate global etickets for
     * a given bu
     *
     * @param	buid	the business unit
     *
     * @return	The XML document that contains the title information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getTitlesForGlobalETicket(String buid)
	throws DBException, IOException
    {
	String	sql = SELECT_GLOBAL_ETICKET_TITLE_LIST +
                      " AND cv.content_object_name NOT IN " +
 		      "(SELECT cov.content_object_name FROM content_title_object_detail_v cov, global_etickets ge " +
                      "WHERE ge.content_id = cov.content_id AND ct.title_id = cov.title_id " +
                      "AND ge.revoke_date IS NULL AND ge.bu_id = " + buid + ")";

	return query(sql);
    }

    /**
     * Returns the title information of a given range.
     *
     * @param	titleID			the title identifier
     * @param	from			the starting record number
     * @param	to			the ending record number
     * @param	order			the sorted order
     *
     * @return	The XML document that contains the tilte information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getTitles(long titleID, int from, int to, String order)
	throws DBException, IOException
    {
	String	sql = "select * from (select rownum no, inner.* from (" +
			SELECT_TITLE_LIST +
			    " where title_id = " + titleID +
			    " order by " + order + ") " +
			"inner where rownum <= " + to + ") " +
			"where no > " + from;

	return query(sql);
    }

    /**
     * Returns the title information of a given range.
     *
     * @param	from			the starting record number
     * @param	to			the ending record number
     * @param	order			the sorted order
     *
     * @return	The XML document that contains the tilte information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getTitles(int from, int to, String order)
	throws DBException, IOException
    {
	String	sql = "select * from (select rownum no, inner.* from (" +
			SELECT_TITLE_LIST + " order by " + order + ") " +
			"inner where rownum <= " + to + ") " +
			"where no > " + from;

	return query(sql);
    }

    public String getPurchaseTitles(int from, int to, String order)
	throws DBException, IOException
    {
	String	sql = "select * from (select rownum no, inner.* from (" +
			SELECT_PURCHASE_TITLE_LIST + " order by " + order + ") " +
			"inner where rownum <= " + to + ") " +
			"where no > " + from;

	return query(sql);
    }

    /**
     * Returns the title name of the given title identifier.
     *
     * @param	titleID			the title identifier
     *
     * @return	The XML document that contains the title name.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getTitleName(long titleID)
	throws DBException, IOException
    {
	return query(SELECT_TITLE_NAME + " where title_id = " + titleID);
    }

    /**
     * Returns the title name of the given title identifier.
     *
     * @param	titleID			the title identifier
     *
     * @return	The XML document that contains the title name.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getTitleName(String titleID)
	throws DBException, IOException
    {
	return query(SELECT_TITLE_NAME + " where title_id = " + titleID);
    }

    /**
     * Returns the added titles.
     *
     * @return	The XML document that contains the title names.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getTitles()
	throws DBException, IOException
    {
	return query(SELECT_TITLES);
    }

    /**
     * Returns the added titles.
     *
     * @return	The XML document that contains the title names.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getVisibleTitles()
	throws DBException, IOException
    {
	return query(SELECT_PURCHASE_TITLE_LIST);
    }

    /**
     * Returns the added titles of the given title identifier.
     *
     * @return	The XML document that contains the title name.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getTitleNames()
	throws DBException, IOException
    {
	return query(SELECT_TITLE_ADD);
    }

    /**
     * Returns the number of titles available.
     *
     * @param	titleID			the title identifier
     *
     * @return	The integer that contains the number of titles.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getTitleCount(long titleID)
	throws DBException, IOException
    {
	return count(SELECT_TITLE_COUNT + " where title_id = " + titleID);
    }

    /**
     * Returns the number of titles available.
     *
     * @param	titleID			the title identifier
     *
     * @return	The integer that contains the number of titles.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getTitleCount(String titleID)
	throws DBException, IOException
    {
	return count(SELECT_TITLE_COUNT + " where title_id = " + titleID);
    }

    /**
     * Returns the number of titles available.
     *
     * @return	The integer that contains the number of titles.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getTitleCount()
	throws DBException, IOException
    {
	return count(SELECT_TITLE_COUNT);
    }

    /**
     * Returns the title information identified by the given titleID.
     *
     * @param	titleID			the title identifier
     *
     * @return	The XML document that contains information of a
     *		Content-Objects record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getContents(String titleID)
	throws DBException, IOException
    {
	return query(SELECT_CONTENT_LIST + " where title_id = " + titleID +
		" order by publish_date");
    }

    /**
     * Returns the database table name to be used.
     *
     * @return	The data table name.
     */
    public String getTableName()
    {
	return TABLE_NAME;
    }

    /**
     * Creates a Title JavaBeans object.
     *
     * @return	The newly created Title object.
     */
    protected Bean newBean()
    {
	return new Title();
    }
}

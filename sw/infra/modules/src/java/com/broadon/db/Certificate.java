package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <c>Certificate</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *<p>
 * It contains certificate related information from the CC schema.
 *
 * @version	$Revision: 1.3 $
 */
public class Certificate
    extends PBean
{
    private String		certificateID;
    private String		cn;
    private String		org;
    private Integer		serialNo;
    private Date		revokeDate;
    private String		cert;
    private String		privateKey;
    private String		publicKey;

    /**
     * Constructs an empty Certificate instance.
     */
    public Certificate()
    {
    }

    /**
     * Getter for certificateID.
     *
     * @return	The certificateID.
     */
    public String getCertificateID()
    {
	return certificateID;
    }

    /**
     * Setter for certificateID.
     *
     * @param	certificateID		the cert id
     */
    public void setCertificateID(String certificateID)
    {
	this.certificateID = certificateID;
    }

    /**
     * Getter for cn.
     *
     * @return	The cn.
     */
    public String getCN()
    {
	return cn;
    }

    /**
     * Setter for cn.
     *
     * @param	cn		the CN
     */
    public void setCN(String cn)
    {
	this.cn = cn;
    }

    /**
     * Getter for org.
     *
     * @return	The org.
     */
    public String getORG()
    {
	return org;
    }

    /**
     * Setter for org.
     *
     * @param	org		the org
     */
    public void setORG(String org)
    {
	this.org = org;
    }

    /**
     * Getter for serialNo.
     *
     * @return	The serail number identifier.
     */
    public Integer getSerialNo()
    {
	return serialNo;
    }

    /**
     * Setter for serialNo.
     *
     * @param	serialNo		the serial number
     */
    public void setSerialNo(Integer serialNo)
    {
	this.serialNo = serialNo;
    }

    /**
     * Getter for revokeDate.
     *
     * @return	The revoked date of this certificate.
     */
    public Date getRevokeDate()
    {
	return revokeDate;
    }

    /**
     * Setter for revokeDate.
     *
     * @param	revokeDate		the revoked date
     */
    public void setRevokeDate(Date revokeDate)
    {
	this.revokeDate = revokeDate;
    }

    /**
     * Getter for cert.
     *
     * @return	The cert.
     */
    public String getCertificate()
    {
	return cert;
    }

    /**
     * Setter for cert.
     *
     * @param	cert		the Certificate
     */
    public void setCertificate(String cert)
    {
	this.cert = cert;
    }

    /**
     * Getter for privateKey.
     *
     * @return	The privateKey.
     */
    public String getPrivateKey()
    {
	return privateKey;
    }

    /**
     * Setter for privateKey.
     *
     * @param	privateKey		the private key
     */
    public void setPrivateKey(String privateKey)
    {
	this.privateKey = privateKey;
    }

    /**
     * Getter for publicKey.
     *
     * @return	The publicKey.
     */
    public String getPublicKey()
    {
	return publicKey;
    }

    /**
     * Setter for publicKey.
     *
     * @param	publicKey		the public key
     */
    public void setPublicKey(String publicKey)
    {
	this.publicKey = publicKey;
    }
}

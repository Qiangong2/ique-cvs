package com.broadon.db;

import java.io.IOException;

import com.broadon.bean.Bean;

/**
 * The <code>BBContentTypeFactory</code> abstract class interfaces with the
 * database to create, update, or query records from the bb-content-object-types
 * table.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.4 $
 */
public class BBContentTypeFactory
    extends SyncableFactory
{
    private static final String     TABLE_NAME;
    private static final String	SELECT_BB_CONTENT_TYPE_LIST;
    private static final String	SELECT_BB_CONTENT_TYPE_COUNT;

    static
    { 
	TABLE_NAME		= "BB_CONTENT_OBJECT_TYPES";
	SELECT_BB_CONTENT_TYPE_LIST	=
	    "select bb_model, content_object_type, last_updated, description from " + TABLE_NAME;
	SELECT_BB_CONTENT_TYPE_COUNT	= "select count(*) from " + TABLE_NAME;
    }

    /**
     * Constructs a BBContentTypeFactory instance.
     */
    protected BBContentTypeFactory()
    {
	super();
    }

    /**
     * Returns the bb content type information of a given range.
     *
     * @param	from			the starting record number
     * @param	to			the ending record number
     * @param	order			the sorted order
     *
     * @return	The XML document that contains the bb content type information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getBBContentTypes(int from, int to, String order)
	throws DBException, IOException
    {
	String	sql = "select * from (select rownum no, inner.* from (" +
			SELECT_BB_CONTENT_TYPE_LIST +
			    " order by " + order + ") " +
			"inner where rownum <= " + to + ") " +
			"where no > " + from;

	return query(sql);
    }

    /**
     * Returns the bb content type details with the given model and content type.
     *
     * @param   bbModel		the bb model identifier
     * @param   contentType	the content type identifier
     *
     * @return The XML document describing the information of bb content type
     *          information.
     *
     * @throws  DBException
     * @throws  IOException
     */
    public String getBBContentType(String bbModel, String contentType)
        throws DBException, IOException
    {
        String  sql = SELECT_BB_CONTENT_TYPE_LIST +
                        " where bb_model = '" + bbModel + 
				"' and content_object_type = '" + contentType + "'";
        return query(sql);
    }

    /**
     * Returns the number of bb content types available.
     *
     * @return	The integer document that contains the number of bb content types.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getBBContentTypeCount()
	throws DBException, IOException
    {
	return count(SELECT_BB_CONTENT_TYPE_COUNT);
    }

    /**
     * Returns the database table name to be used.
     *
     * @return	The data table name.
     */
    public String getTableName()
    {
	return TABLE_NAME;
    }

    /**
     * Creates a Content JavaBeans object.
     *
     * @return	The newly created Content object.
     */
    protected Bean newBean()
    {
	return new BBContentType();
    }
}

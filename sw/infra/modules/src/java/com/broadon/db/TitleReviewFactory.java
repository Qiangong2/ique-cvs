package com.broadon.db;

import java.io.IOException;
import java.util.Map;

import com.broadon.bean.Bean;
import com.broadon.sql.Predicate;

/**
 * The <c>TitleReviewFactory</c> class interfaces with the database to
 * create, update, or query records from the TitleReviews table.
 * Each record is represented by a TitleReview JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.4 $
 */
public class TitleReviewFactory
    extends SyncableFactory
{
    private static final String TABLE_NAME;
    private static final String	SELECT_TITLE_REVIEW_LIST;
    private static final String	SELECT_TITLE_REVIEW_COUNT;

    static
    {
	TABLE_NAME		= "CONTENT_TITLE_REVIEWS";
	SELECT_TITLE_REVIEW_LIST	=
	    "select rownum no, title_id, reviewed_by, review_date, ratings, review_title, " +
		"review_detail, revoke_date, last_updated from " + TABLE_NAME;
	SELECT_TITLE_REVIEW_COUNT = "select count(*) from " + TABLE_NAME;
    }

    /**
     * Constructs a TitleReviewFactory instance.
     */
    protected TitleReviewFactory()
    {
	super();
    }

    /**
     * Returns the reviews given to the titles identified by the given
     * array of titleIDs.
     *
     * @param	titleIDs		the title identifiers
     * @param	nameMap			the name mapping table
     *
     * @return	The array of TitleReview JavaBeans, each contains
     *		all information of a Content-Title-Reviews record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public TitleReview[] getTitleReviews(long[] titleIDs, Map nameMap)
	throws DBException, IOException
    {
	if (titleIDs == null)
	{
	    return null;
	}

	int		size = titleIDs.length;

	if (size == 0)
	{
	    return null;
	}
	/*
	 * Set up the predicate.
	 */
	TitleID		titleID = new TitleID(titleIDs[0]);
	Predicate	predicate = titleID;

	for (int n = 1; n < size; n++)
	{
	    titleID = (TitleID)titleID.or(new TitleID(titleIDs[n]));
	}
	/*
	 * Query.
	 */
	return (TitleReview[])query(predicate, nameMap);
    }

    /**
     * Returns the reviews information identified by the given titleID.
     *
     * @param	titleID			the title identifier
     *
     * @return	The XML document that contains information of a
     *		Content-Title-Reviews record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getTitleReviews(String titleID)
	throws DBException, IOException
    {
	return query(SELECT_TITLE_REVIEW_LIST + " where title_id = " + titleID +
		" order by review_date");
    }

    /**
     * Returns the review information of a given range.
     *
     * @param	titleID			the title identifier
     * @param	from			the starting record number
     * @param	to			the ending record number
     * @param	order			the sorted order
     *
     * @return	The XML document that contains the title reviews information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getTitleReviews(String titleID, int from, int to, String order)
	throws DBException, IOException
    {
	String	sql = "select * from (select rownum no, inner.* from (" +
			SELECT_TITLE_REVIEW_LIST +
			    " where title_id = " + titleID +
			    " order by " + order + ") " +
			"inner where rownum <= " + to + ") " +
			"where no > " + from;

	return query(sql);
    }

    /**
     * Returns the reviews information of a given range.
     *
     * @param	from			the starting record number
     * @param	to			the ending record number
     * @param	order			the sorted order
     *
     * @return	The XML document that contains the tilte information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getTitleReviews(int from, int to, String order)
	throws DBException, IOException
    {
	String	sql = "select * from (select rownum no, inner.* from (" +
			SELECT_TITLE_REVIEW_LIST + " order by " + order + ") " +
			"inner where rownum <= " + to + ") " +
			"where no > " + from;

	return query(sql);
    }

    /**
     * Returns the added titles of the given title identifier.
     *
     * @return	The XML document that contains the title name.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getTitleReviews()
	throws DBException, IOException
    {
	return query(SELECT_TITLE_REVIEW_LIST);
    }

    /**
     * Returns the number of title reviews available.
     *
     * @param	titleID			the title identifier
     *
     * @return	The integer that contains the number of titles.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getTitleReviewCount(long titleID)
	throws DBException, IOException
    {
	return count(SELECT_TITLE_REVIEW_COUNT + " where title_id = " + titleID);
    }

    /**
     * Returns the number of title reviews available.
     *
     * @param	titleID			the title identifier
     *
     * @return	The integer that contains the number of titles.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getTitleReviewCount(String titleID)
	throws DBException, IOException
    {
	return count(SELECT_TITLE_REVIEW_COUNT + " where title_id = " + titleID);
    }

    /**
     * Returns the number of title reviews available.
     *
     * @return	The integer that contains the number of titles.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getTitleReviewCount()
	throws DBException, IOException
    {
	return count(SELECT_TITLE_REVIEW_COUNT);
    }

    /**
     * Returns the database table name to be used.
     *
     * @return	The data table name.
     */
    public String getTableName()
    {
	return TABLE_NAME;
    }

    /**
     * Creates a TitleReview JavaBeans object.
     *
     * @return	The newly created TitleReview object.
     */
    protected Bean newBean()
    {
	return new TitleReview();
    }
}

package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <c>CompetingCategory</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *<p>
 * It contains information about a category within a game competition.
 *
 * @version	$Revision: 1.2 $
 */
public class CompetingCategory
    extends PBean
{
    private Integer     competitionID;
    private String		categoryCode;
    private String		categoryName;
    private String      cityCode;
    private Date        startDate;
    private Date        endDate;
    private Date		lastUpdated;

    /**
     * Constructs an empty Competition instance.
     */
    public CompetingCategory()
    {
    }

    /**
     * Getter for competitionID.
     *
     * @return	The competition identifier.
     */
    public Integer getCompetitionID()
    {
        return competitionID;
    }

    /**
     * Setter for competitionID.
     *
     * @param	competitionID the competition identifier
     */
    public void setCompetitionID(Integer competitionID)
    {
        this.competitionID = competitionID;
    }

    /**
     * Getter for category code.
     *
     * @return	The code identifier of a competition category
     */
    public String getCategoryCode()
    {
        return categoryCode;
    }

    /**
     * Setter for categoryCode.
     *
     * @param	categoryCode the code identifier of a competition category
     */
    public void setCategoryCode(String categoryCode)
    {
        this.categoryCode = categoryCode;
    }

    /**
     * Getter for categoryName.
     *
     * @return	The name of a competition category.
     */
    public String getCategoryName()
    {
        return categoryName;
    }

    /**
     * Setter for categoryName.
     *
     * @param	categoryName the name of a competition category
     */
    public void setCategoryName(String categoryName)
    {
        this.categoryName = categoryName;
    }

    /**
     * Getter for cityCode.
     *
     * @return	The city in which the competing category occurs.
     */
    public String getCityCode()
    {
        return cityCode;
    }

    /**
     * Setter for cityCode.
     *
     * @param	cityCode the city in which the competing category occurs.
     */
    public void setCityCode(String cityCode)
    {
        this.cityCode = cityCode;
    }

    /**
     * Getter for startDate.
     *
     * @return	The date the competition category starts.
     */
    public Date getStartDate()
    {
	return startDate;
    }

    /**
     * Setter for startDate.
     *
     * @param	startDate The date the competition category starts.
     */
    public void setStartDate(Date startDate)
    {
	this.startDate = startDate;
    }

    /**
     * Getter for endDate.
     *
     * @return	The date the competition category ends.
     */
    public Date getEndDate()
    {
	return endDate;
    }

    /**
     * Setter for endDate.
     *
     * @param	endDate	The date the competition category ends.
     */
    public void setEndDate(Date endDate)
    {
	this.endDate = endDate;
    }

    /**
     * Getter for lastUpdated.
     *
     * @return	The last updated date.
     */
    public Date getLastUpdated()
    {
	return lastUpdated;
    }

    /**
     * Setter for lastUpdated.
     *
     * @param	lastUpdated		the last updated date
     */
    public void setLastUpdated(Date lastUpdated)
    {
	this.lastUpdated = lastUpdated;
    }

} // class CompetingCategory

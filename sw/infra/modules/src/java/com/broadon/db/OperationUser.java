package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <c>OperationUser</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *<p>
 * It contains information about operation users.
 *
 * @version	$Revision: 1.4 $
 */
public class OperationUser
    extends PBean
{
    private Long		operatorID;
    private String		emailAddress;
    private String		status;
    private String		fullname;
    private Integer		emailAlerts;
    private String		passwd;
    private Integer		roleLevel;
    private Date		lastLogon;
    private Date		statusDate;
    private String		otherInfo;
    private Integer		storeID;

    /**
     * Constructs an empty OperationUser instance.
     */
    public OperationUser()
    {
    }

    /**
     * Getter for operatorID.
     *
     * @return	The operator identifier.
     */
    public Long getOperatorID()
    {
	return operatorID;
    }

    /**
     * Setter for operatorID.
     *
     * @param	operatorID		the operator identifier
     */
    public void setOperatorID(Long operatorID)
    {
	this.operatorID = operatorID;
    }

    /**
     * Getter for emailAddress.
     *
     * @return	The email address.
     */
    public String getEmailAddress()
    {
	return emailAddress;
    }

    /**
     * Setter for emailAddress.
     *
     * @param	emailAddress		the email address
     */
    public void setEmailAddress(String emailAddress)
    {
	this.emailAddress = emailAddress;
    }

    /**
     * Getter for status.
     *
     * @return	The status.
     */
    public String getStatus()
    {
	return status;
    }

    /**
     * Setter for status.
     *
     * @param	status		the status of the operator
     */
    public void setStatus(String status)
    {
	this.status = status;
    }

    /**
     * Getter for fullname.
     *
     * @return	The full name.
     */
    public String getFullname()
    {
	return fullname;
    }

    /**
     * Setter for fullname.
     *
     * @param	fullname		the full name of the operator
     */
    public void setFullname(String fullname)
    {
	this.fullname = fullname;
    }

    /**
     * Getter for emailAlerts.
     *
     * @return	The email alerts flag.
     */
    public Integer getEmailAlerts()
    {
	return emailAlerts;
    }

    /**
     * Setter for emailAlerts.
     *
     * @param	emailAlerts		the email alerts flag
     */
    public void setEmailAlerts(Integer emailAlerts)
    {
	this.emailAlerts = emailAlerts;
    }

    /**
     * Getter for passwd.
     *
     * @return	The password.
     */
    public String getPasswd()
    {
	return passwd;
    }

    /**
     * Setter for passwd.
     *
     * @param	passwd		the password of the operator
     */
    public void setPasswd(String passwd)
    {
	this.passwd = passwd;
    }

    /**
     * Getter for roleLevel.
     *
     * @return	The default role level.
     */
    public Integer getRoleLevel()
    {
	return roleLevel;
    }

    /**
     * Setter for roleLevel.
     *
     * @param	roleLevel		the default role level
     */
    public void setRoleLevel(Integer roleLevel)
    {
	this.roleLevel = roleLevel;
    }

    /**
     * Getter for lastLogon.
     *
     * @return	The last logon date of this operator.
     */
    public Date getLastLogon()
    {
	return lastLogon;
    }

    /**
     * Setter for lastLogon.
     *
     * @param	lastLogon		the last logon date
     */
    public void setLastLogon(Date lastLogon)
    {
	this.lastLogon = lastLogon;
    }

    /**
     * Getter for statusDate.
     *
     * @return	The status change date for an operator.
     */
    public Date getStatusDate()
    {
	return statusDate;
    }

    /**
     * Setter for statusDate.
     *
     * @param	statusDate		the status date
     */
    public void setStatusDate(Date statusDate)
    {
	this.statusDate = statusDate;
    }

    /**
     * Getter for other info.
     *
     * @return	The other info.
     */
    public String getOtherInfo()
    {
	return otherInfo;
    }

    /**
     * Setter for other info.
     *
     * @param	otherInfo 	the other info
     */
    public void setOtherInfo(String otherInfo)
    {
	this.otherInfo = otherInfo;
    }

    /**
     * Getter for storeID.
     *
     * @return	The store ID.
     */
    public Integer getStoreID()
    {
	return storeID;
    }

    /**
     * Setter for storeID
     *
     * @param	storeID		the store id
     */
    public void setStoreID(Integer storeID)
    {
	this.storeID = storeID;
    }


}

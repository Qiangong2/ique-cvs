package com.broadon.db;

/**
 * The <code>AlreadyInTransactionException</code> class indicates that
 * this thread is already in a transaction.
 *
 * @version	$Revision: 1.1 $
 */
public class AlreadyInTransactionException
    extends DBException
{
    /**
     * Constructs a AlreadyInTransactionException instance.
     */
    public AlreadyInTransactionException()
    {
	super();
    }

    /**
     * Constructs a AlreadyInTransactionException instance.
     *
     * @param	message			the exception message
     */
    public AlreadyInTransactionException(String message)
    {
	super(message);
    }

    /**
     * Constructs a AlreadyInTransactionException instance.
     *
     * @param	message			the exception message
     * @param	throwable		the nested exception
     */
    public AlreadyInTransactionException(String message, Throwable throwable)
    {
	super(message, throwable);
    }
}

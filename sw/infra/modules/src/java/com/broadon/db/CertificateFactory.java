package com.broadon.db;

import java.io.IOException;
import java.util.Map;

import com.broadon.bean.Bean;

/**
 * The <code>CertificateFactory</code> class interfaces with the database
 * to create, update, or query records from the Certificates table.
 * Each record is represented by a Certificate JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.3 $
 */
public class CertificateFactory
    extends DBAccessFactory
{
    private static final String TABLE_NAME;
    private static final String	SELECT_PARENT_CERTIFICATE;
    private static final String	SELECT_CERTIFICATE_LIST;
    private static final String	SELECT_CERTIFICATE_COUNT;

    static
    {
	TABLE_NAME		= "CERTIFICATES";
	SELECT_PARENT_CERTIFICATE =
	    "select chain_id, signer_cert_id, ca_cert_id, description " +
		"from CERTIFICATE_CHAINS";
	SELECT_CERTIFICATE_LIST	=
	    "select cert_id, cn, ou, serial_no, revoke_date, certificate, " +
		"private_key, public_key from " + TABLE_NAME;
	SELECT_CERTIFICATE_COUNT	= "select count(cert_id) from " + TABLE_NAME;
    }

    /**
     * Constructs a CertificateFactory instance.
     */
    protected CertificateFactory()
    {
	super();
    }

    /**
     * Returns the certificate identified by the given certificate identifier.
     *
     * @param	certID			the certificate identifier
     * @param	nameMap			the name mapping table
     *
     * @return	The Certificate JavaBeans that contains information of a
     *		Certificateal-Centers record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public Certificate getCertificate(String certID, Map nameMap)
	throws DBException, IOException
    {
	return (Certificate)queryUnique(new CertificateID(certID), nameMap);
    }

    /**
     * Returns the certificate information of a given range.
     *
     * @param	from			the starting record number
     * @param	to			the ending record number
     * @param	order			the sorted order
     *
     * @return	The XML document that contains the certificate information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getCertificates(int from, int to, String order)
	throws DBException, IOException
    {
	String	sql = "select * from (select rownum no, inner.* from (" +
			SELECT_CERTIFICATE_LIST +
			    " order by " + order + ") " +
			"inner where rownum <= " + to + ") " +
			"where no > " + from;

	return query(sql);
    }

   /**
     * Returns the certificate details with the given id.
     *
     * @param   certID                 the certificate identifier
     *
     * @return The XML document describing the information of certificate
     *          information.
     *
     * @throws  DBException
     * @throws  IOException
     */
    public String getCertificate(String certID)
        throws DBException, IOException
    {
        String  sql = SELECT_CERTIFICATE_LIST +
                        " where cert_id = '" + certID + "'";
        return query(sql);
    }

    public String getParentCertificate(String certID)
        throws DBException, IOException
    {
        String  sql = SELECT_PARENT_CERTIFICATE +
                        " where signer_cert_id = '" + certID + "'";
        return query(sql);
    }

    /**
     * Returns the number of certificates available.
     *
     * @param	certID		the certificate identifier
     *
     * @return	The integer that contains the number of certificates.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getCertificateCount(String certID)
	throws DBException, IOException
    {
	return count(SELECT_CERTIFICATE_COUNT + " where cert_id = '" + certID + "'");
    }

    /**
     * Returns the number of certificates available.
     *
     * @return	The integer document that contains the number of certificates.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getCertificateCount()
	throws DBException, IOException
    {
	return count(SELECT_CERTIFICATE_COUNT);
    }

    /**
     * Returns the database table name to be used.
     *
     * @return	The data table name.
     */
    public String getTableName()
    {
	return TABLE_NAME;
    }

    /**
     * Creates a Certificate JavaBeans object.
     *
     * @return	The newly created Certificate object.
     */
    protected Bean newBean()
    {
	return new Certificate();
    }
}

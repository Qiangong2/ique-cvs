package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <c>Region</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *<p>
 * It contains regional related information about regions.
 *
 * @version	$Revision: 1.3 $
 */
public class Region
    extends PBean
{
    private Integer		regionID;
    private String		regionName;
    private Date		createDate;
    private Date		revokeDate;
    private Date		suspendDate;
    private Integer		businessUnit;

    /**
     * Constructs an empty Region instance.
     */
    public Region()
    {
    }

    /**
     * Getter for regionID.
     *
     * @return	The region identifier.
     */
    public Integer getRegionID()
    {
	return regionID;
    }

    /**
     * Setter for regionID.
     *
     * @param	regionID		the region identifier
     */
    public void setRegionID(Integer regionID)
    {
	this.regionID = regionID;
    }

    /**
     * Getter for regionName.
     *
     * @return	The region name.
     */
    public String getRegionName()
    {
	return regionName;
    }

    /**
     * Setter for regionName.
     *
     * @param	regionName		the region name
     */
    public void setRegionName(String regionName)
    {
	this.regionName = regionName;
    }

    /**
     * Getter for createDate.
     *
     * @return	The creation date of this region.
     */
    public Date getCreateDate()
    {
	return createDate;
    }

    /**
     * Setter for createDate.
     *
     * @param	createDate		the creation date
     */
    public void setCreateDate(Date createDate)
    {
	this.createDate = createDate;
    }

    /**
     * Getter for revokeDate.
     *
     * @return	The revoked date of this region.
     */
    public Date getRevokeDate()
    {
	return revokeDate;
    }

    /**
     * Setter for revokeDate.
     *
     * @param	revokeDate		the revoked date
     */
    public void setRevokeDate(Date revokeDate)
    {
	this.revokeDate = revokeDate;
    }

    /**
     * Getter for suspendDate.
     *
     * @return	The suspended date of this region.
     */
    public Date getSuspendDate()
    {
	return suspendDate;
    }

    /**
     * Setter for suspendDate.
     *
     * @param	suspendDate		the suspended date
     */
    public void setSuspendDate(Date suspendDate)
    {
	this.suspendDate = suspendDate;
    }

    /**
     * Getter for businessUnit.
     *
     * @return	The business unit this region belongs to.
     */
    public Integer getBusinessUnit()
    {
	return businessUnit;
    }

    /**
     * Setter for businessUnit.
     *
     * @param	businessUnit		the business unit this region belongs to
     */
    public void setBusinessUnit(Integer businessUnit)
    {
	this.businessUnit = businessUnit;
    }
}

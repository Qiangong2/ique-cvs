package com.broadon.db;

import java.io.IOException;
import java.util.Map;

import com.broadon.bean.Bean;
import com.broadon.sql.Predicate;

/**
 * The <c>TitleContentFactory</c> class interfaces with the database to
 * create, update, or query records from the TitleContents table.
 * Each record is represented by a TitleContent JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.2 $
 */
public class TitleContentFactory
    extends SyncableFactory
{
    /**
     * Constructs a TitleContentFactory instance.
     */
    protected TitleContentFactory()
    {
	super();
    }

    /**
     * Returns the information that describe the relationship between the
     * title and the content, identified by the given titleID and contentID.
     *
     * @param	titleID			the title identifier
     * @param	contentID		the content identifier
     * @param	nameMap			the name mapping table
     *
     * @return	The TitleContent JavaBeans that contains information of a
     *		Content-Title-Objects record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public TitleContent getTitleContents(long titleID,
					 long contentID,
					 Map nameMap)
	throws DBException, IOException
    {
	TitleID		predicate = new TitleID(titleID);

	predicate.and(new ContentID(contentID));
	return (TitleContent)queryUnique(predicate, nameMap);
    }

    /**
     * Returns the information that describe the relationship between the
     * title and the content, identified by the given array of contentIDs.
     *
     * @param	contentIDs		the content identifiers
     * @param	nameMap			the name mapping table
     *
     * @return	The array of TitleContent JavaBeans, each contains
     *		all information of a Content-Title-Objects record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public TitleContent[] getTitleContents(long[] contentIDs, Map nameMap)
	throws DBException, IOException
    {
	if (contentIDs == null)
	{
	    return null;
	}

	int		size = contentIDs.length;

	if (size == 0)
	{
	    return null;
	}
	/*
	 * Set up the predicate.
	 */
	ContentID	contentID = new ContentID(contentIDs[0]);
	Predicate	predicate = contentID;

	for (int n = 1; n < size; n++)
	{
	    contentID = (ContentID)contentID.or(new ContentID(contentIDs[n]));
	}
	/*
	 * Query.
	 */
	return (TitleContent[])query(predicate, nameMap);
    }

    /**
     * Returns the database table name to be used.
     *
     * @return	The data table name.
     */
    public String getTableName()
    {
	return "content_title_objects";
    }

    /**
     * Creates a TitleContent JavaBeans object.
     *
     * @return	The newly created TitleContent object.
     */
    protected Bean newBean()
    {
	return new TitleContent();
    }
}

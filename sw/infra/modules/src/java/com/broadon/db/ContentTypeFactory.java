package com.broadon.db;

import java.io.IOException;
import java.util.Map;

import com.broadon.bean.Bean;

/**
 * The <c>ContentTypeFactory</c> class interfaces with the database to
 * create, update, or query records from the content_object_types table.
 * Each record is represented by a ContentType JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.2 $
 */
public class ContentTypeFactory
    extends DBAccessFactory
{
    private static final String     TABLE_NAME;
    private static final String	SELECT_CONTENT_TYPE_NAMES;
    private static final String	SELECT_CONTENT_TYPE_LIST;
    private static final String	SELECT_CONTENT_TYPE_COUNT;

    static
    { 
	TABLE_NAME		= "CONTENT_OBJECT_TYPES";
	SELECT_CONTENT_TYPE_NAMES	=
	    "select rownum no, content_object_type from " + TABLE_NAME +
		" order by content_object_type";
	SELECT_CONTENT_TYPE_LIST	=
	    "select content_object_type, create_date, description from " + TABLE_NAME;
	SELECT_CONTENT_TYPE_COUNT	= "select count(content_object_type) from " + TABLE_NAME;
    }

   /**
     * Constructs a ContentTypeFactory instance.
     */
    protected ContentTypeFactory()
    {
	super();
    }

    /**
     * Returns the content object type information, based on the given contentType.
     *
     * @param	contentType		the content object type identifier
     * @param	nameMap		the name mapping table
     *
     * @return	The ContentType JavaBeans containing all information of
     *		a ContentType record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public ContentType getContentType(String contentType, Map nameMap)
	throws DBException, IOException
    {
	ContentTypeID	predicate = new ContentTypeID(contentType);

	return (ContentType)queryUnique(predicate, nameMap);
    }

   /**
     * Returns the content object type information of a given range.
     *
     * @param	from			the starting record number
     * @param	to			the ending record number
     * @param	order			the sorted order
     *
     * @return	The XML document that contains the content object type information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getContentTypes(int from, int to, String order)
	throws DBException, IOException
    {
	String	sql = "select * from (select rownum no, inner.* from (" +
			SELECT_CONTENT_TYPE_LIST +
			    " order by " + order + ") " +
			"inner where rownum <= " + to + ") " +
			"where no > " + from;

	return query(sql);
    }

    /**
     * Returns the content type.
     *
     * @return	The XML document that contains information of content type.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getContentTypeNames()
	throws DBException, IOException
    {
	return query(SELECT_CONTENT_TYPE_NAMES);
    }

    /**
     * Returns the content object type details with the given type.
     *
     * @param   contentType		the content object type identifier
     *
     * @return The XML document describing the information of content object type
     *          information.
     *
     * @throws  DBException
     * @throws  IOException
     */
    public String getContentType(String contentType)
        throws DBException, IOException
    {
        String  sql = SELECT_CONTENT_TYPE_LIST +
                        " where content_object_type = '" + contentType + "'";
        return query(sql);
    }

    /**
     * Returns the number of content object type available.
     *
     * @param	contentType		the content object type identifier
     *
     * @return	The integer that contains the number of content object type.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getContentTypeCount(String contentType)
	throws DBException, IOException
    {
	return count(SELECT_CONTENT_TYPE_COUNT + " where content_object_type = '" + contentType + "'");
    }

    /**
     * Returns the number of content object types available.
     *
     * @return	The integer document that contains the number of content object types.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getContentTypeCount()
	throws DBException, IOException
    {
	return count(SELECT_CONTENT_TYPE_COUNT);
    }

    /**
     * Returns the database table name to be used.
     *
     * @return	The data table name.
     */
    public String getTableName()
    {
	return TABLE_NAME;
    }

    /**
     * Creates a ContentType JavaBeans object.
     *
     * @return	The newly created ContentType object.
     */
    protected Bean newBean()
    {
	return new ContentType();
    }
}

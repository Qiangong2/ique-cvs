package com.broadon.db;

import com.broadon.sql.PBean;

/**
 * The <c>Chain</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *<p>
 * It contains information about a chain.
 *
 * @version	$Revision: 1.3 $
 */
public class Chain
    extends PBean
{
    private Integer		chainID;
    private String		signerCertID;
    private String		caCertID;
    private String		description;

    /**
     * Constructs an empty Chain instance.
     */
    public Chain()
    {
    }

    /**
     * Getter for chainID.
     *
     * @return	The chain identifier.
     */
    public Integer getChainID()
    {
	return chainID;
    }

    /**
     * Setter for chainID.
     *
     * @param	chainID		the chain identifier
     */
    public void setChainID(Integer chainID)
    {
	this.chainID = chainID;
    }

    /**
     * Getter for signerCertID.
     *
     * @return	The signer certificate ID.
     */
    public String getSignerCertID()
    {
	return signerCertID;
    }

    /**
     * Setter for signerCertID.
     *
     * @param	signerCertID		the signer certificate ID
     */
    public void setSignerCertID(String signerCertID)
    {
	this.signerCertID = signerCertID;
    }

    /**
     * Getter for caCertID.
     *
     * @return	The  ca cert ID.
     */
    public String getCACertID()
    {
	return caCertID;
    }

    /**
     * Setter for caCertID.
     *
     * @param	caCertID		the  ca cert ID
     */
    public void setCACertID(String caCertID)
    {
	this.caCertID = caCertID;
    }

    /**
     * Getter for description
     *
     * @return	The description.
     */
    public String getDescription()
    {
	return description;
    }

    /**
     * Setter for description.
     *
     * @param	description 	The description
     */
    public void setDescription(String description)
    {
	this.description = description;
    }
}

package com.broadon.db;

import java.io.IOException;
import java.util.Map;

import com.broadon.bean.Bean;

/**
 * The <c>BBModelFactory</c> class interfaces with the database to
 * create, update, or query records from the BB_MODELS table.
 * Each record is represented by a BBModel JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.2 $
 */
public class BBModelFactory
    extends DBAccessFactory
{
    private static final String     TABLE_NAME;
    private static final String	SELECT_BB_MODEL_NAMES;
    private static final String	SELECT_BB_MODEL_LIST;
    private static final String	SELECT_BB_MODEL_COUNT;

    static
    { 
	TABLE_NAME		= "BB_MODELS";
	SELECT_BB_MODEL_NAMES	=
	    "select rownum no, bb_model from " + TABLE_NAME +
		" order by bb_model";
	SELECT_BB_MODEL_LIST	=
	    "select bb_model, create_date, description from " + TABLE_NAME;
	SELECT_BB_MODEL_COUNT	= "select count(bb_model) from " + TABLE_NAME;
    }

   /**
     * Constructs a BBModelFactory instance.
     */
    protected BBModelFactory()
    {
	super();
    }

    /**
     * Returns the bb model information, based on the given bbModel.
     *
     * @param	bbModel		the bb model identifier
     * @param	nameMap		the name mapping table
     *
     * @return	The BBModel JavaBeans containing all information of
     *		a BBModel record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public BBModel getBBModel(String bbModel, Map nameMap)
	throws DBException, IOException
    {
	BBModelID	predicate = new BBModelID(bbModel);

	return (BBModel)queryUnique(predicate, nameMap);
    }

   /**
     * Returns the bb model information of a given range.
     *
     * @param	from			the starting record number
     * @param	to			the ending record number
     * @param	order			the sorted order
     *
     * @return	The XML document that contains the bb model information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getBBModels(int from, int to, String order)
	throws DBException, IOException
    {
	String	sql = "select * from (select rownum no, inner.* from (" +
			SELECT_BB_MODEL_LIST +
			    " order by " + order + ") " +
			"inner where rownum <= " + to + ") " +
			"where no > " + from;

	return query(sql);
    }

    /**
     * Returns the bb model names.
     *
     * @return	The XML document that contains information of bb model names.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getBBModelNames()
	throws DBException, IOException
    {
	return query(SELECT_BB_MODEL_NAMES);
    }

    /**
     * Returns the bb model details with the given model.
     *
     * @param   bbModel		the bb models identifier
     *
     * @return The XML document describing the information of bb model
     *          information.
     *
     * @throws  DBException
     * @throws  IOException
     */
    public String getBBModel(String bbModel)
        throws DBException, IOException
    {
        String  sql = SELECT_BB_MODEL_LIST +
                        " where bb_model = '" + bbModel + "'";
        return query(sql);
    }

    /**
     * Returns the number of bb models available.
     *
     * @param	bbModel		the bb model identifier
     *
     * @return	The integer that contains the number of bb models.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getBBModelCount(String bbModel)
	throws DBException, IOException
    {
	return count(SELECT_BB_MODEL_COUNT + " where bb_model = '" + bbModel + "'");
    }

    /**
     * Returns the number of bb models available.
     *
     * @return	The integer document that contains the number of bb models.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getBBModelCount()
	throws DBException, IOException
    {
	return count(SELECT_BB_MODEL_COUNT);
    }

    /**
     * Returns the database table name to be used.
     *
     * @return	The data table name.
     */
    public String getTableName()
    {
	return TABLE_NAME;
    }

    /**
     * Creates a BBModel JavaBeans object.
     *
     * @return	The newly created BBModel object.
     */
    protected Bean newBean()
    {
	return new BBModel();
    }
}

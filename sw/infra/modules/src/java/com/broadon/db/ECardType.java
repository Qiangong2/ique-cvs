package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <c>ECardType</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *<p>
 * It contains information about an eCard type.
 *
 * @version	$Revision: 1.6 $
 */
public class ECardType
    extends PBean
{
    private Integer		ECardTypeID;
    private Integer		EUnits;
    private String		description;
    private Integer		titleID;
    private Integer		isUsedOnce;
    private Integer		isPrepaid;
    private Integer		isRefillAllowed;
    private Date		lastUpdated;

    /**
     * Constructs an empty ECardType instance.
     */
    public ECardType()
    {
	this.ECardTypeID = null;
	this.EUnits = null;
	this.description = null;
	this.titleID = null;
	this.isUsedOnce = null;
	this.isPrepaid = null;
	this.isRefillAllowed = null;
	this.lastUpdated = null;
    }

    /**
     * Getter for ECardTypeID.
     *
     * @return	The ECard type identifier.
     */
    public Integer getECardTypeID()
    {
	return ECardTypeID;
    }

    /**
     * Setter for ECardTypeID.
     *
     * @param	ECardTypeID		the eCard type identifier
     */
    public void setECardTypeID(Integer ECardTypeID)
    {
	this.ECardTypeID = ECardTypeID;
    }

    /**
     * Getter for EUnits.
     *
     * @return	The monetary value.
     */
    public Integer getEUnits()
    {
	return EUnits;
    }

    /**
     * Setter for EUnits.
     *
     * @param	EUnits			the monetary value
     */
    public void setEUnits(Integer EUnits)
    {
	this.EUnits = EUnits;
    }

    /**
     * Getter for description.
     *
     * @return	The description.
     */
    public String getDescription()
    {
	return description;
    }

    /**
     * Setter for description.
     *
     * @param	description		the description
     */
    public void setDescription(String description)
    {
	this.description = description;
    }

    /**
     * Getter for titleID.
     *
     * @return	The title identifier.
     */
    public Integer getTitleID()
    {
	return titleID;
    }

    /**
     * Setter for titleID.
     *
     * @param	titleID			the title identifier
     */
    public void setTitleID(Integer titleID)
    {
	this.titleID = titleID;
    }

    /**
     * Getter for isUsedOnce.
     *
     * @return	Can this type of eCards be used only once?
     */
    public Integer getIsUsedOnce()
    {
	return isUsedOnce;
    }

    /**
     * Setter for isUsedOnce.
     *
     * @param	isUsedOnce		true  => can only be used once;
     *					false => can be used multiple times
     */
    public void setIsUsedOnce(Integer isUsedOnce)
    {
	this.isUsedOnce = isUsedOnce;
    }

    /**
     * Getter for isPrepaid.
     *
     * @return	Is this type of eCards prepaid?
     */
    public Integer getIsPrepaid()
    {
	return isPrepaid;
    }

    /**
     * Setter for isPrepaid.
     *
     * @param	isPrepaid		true  => a prepaid card;
     *					false => a credit card
     */
    public void setIsPrepaid(Integer isPrepaid)
    {
	this.isPrepaid = isPrepaid;
    }

    /**
     * Getter for isRefillAllowed.
     *
     * @return	Is refill allowed on this type of eCards?
     */
    public Integer getIsRefillAllowed()
    {
	return isRefillAllowed;
    }

    /**
     * Setter for isRefillAllowed.
     *
     * @param	isRefillAllowed		true  => refill allowed;
     *					false => refill not allowed
     */
    public void setIsRefillAllowed(Integer isRefillAllowed)
    {
	this.isRefillAllowed = isRefillAllowed;
    }

    /**
     * Getter for lastUpdated.
     *
     * @return	The last updated date of this eCard-type.
     */
    public Date getLastUpdated()
    {
	return lastUpdated;
    }

    /**
     * Setter for lastUpdated.
     *
     * @param	lastUpdated		the last updated date of this
     *					eCard-type
     */
    public void setLastUpdated(Date lastUpdated)
    {
	this.lastUpdated = lastUpdated;
    }
}

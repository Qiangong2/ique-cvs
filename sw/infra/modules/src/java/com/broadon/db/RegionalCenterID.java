package com.broadon.db;

import com.broadon.sql.PredicateItem;

/**
 * The <code>RegionalCenterID</code> class conforms to the JavaBeans property,
 * and is a Predicate that finds various records.
 *
 * @version	$Revision: 1.1 $
 */
public class RegionalCenterID
    extends PredicateItem
{
    private Integer	regionalCenterID;

    /**
     * Constructs a RegionalCenterID instance.
     *
     * @param	regionalCenterID	the regional center identifier
     */
    public RegionalCenterID(int regionalCenterID)
    {
	super();
	this.regionalCenterID = new Integer(regionalCenterID);
    }

    /**
     * Getter for regionalCenterID.
     *
     * @return	The regional center identifier.
     */
    public Integer getRegionalCenterID()
    {
	return regionalCenterID;
    }

    /**
     * Setter for regionalCenterID.
     *
     * @param	regionalCenterID	the regional center identifier
     */
    public void setRegionalCenterID(Integer regionalCenterID)
    {
	this.regionalCenterID = regionalCenterID;
    }
}

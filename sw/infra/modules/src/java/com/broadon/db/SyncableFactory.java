package com.broadon.db;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

import com.broadon.bean.Bean;
import com.broadon.util.VersionNumber;

/**
 * The <c>SyncableFactory</c> class is the parent for those factory classes
 * which associate with tables that have an attribute to indicate the last
 * updated time on each record; therefore, allowing the query to find all
 * records that are updated within an interval of time.
 *
 * @version	$Revision: 1.8 $
 */
public abstract class SyncableFactory
    extends DBAccessFactory
{
    // Name of the DB column commonly used to represent the last update time
    //
    private static final String LAST_UPDATED = "last_updated";


    /**
     * Constructs a SyncableFactory instance.
     */
    protected SyncableFactory()
    {
	super();
    }

    /**
     * Returns the field representing the last time a DB row was updated
     * for this DB table.  Can optionally be redefined for subclasses.
     *
     * @return	name of DB column.
     */
    protected String lastUpdateTimeNm() {return LAST_UPDATED;}

    /**
     * Determines whether or not the records produced by a factory is
     * supported by a particular depot's database schema.  This method
     * can be redefined by subclasses if the default is inappropriate.
     *
     * @param	depotSchemaVersionfromTime the schema version used by the depot
     * @return	true if the depot supports the record types; otherwise false.
     */
    public boolean inDepotSchemaVersion(VersionNumber depotSchemaVersion)
    {
        // By default a depot includes the records produced by a factory.
        //
        return true;
    }

    /**
     * Returns records that were updated between the given time UTC interval,
     * [fromTime, toTime).
     *
     * @param	fromTime		the beginning interval (inclusive)
     * @param	toTime			the ending interval (exclusive)
     * @param	nameMap			the name mapping table
     *
     * @return	The array of JavaBeans objects, each contains all
     *		information of a qualified record.  Returns null if not
     *      supported by the current schema version.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public Bean[] getRecordsWithinInterval(VersionNumber depotSchemaVersion,
                                           Date          fromTime,
                                           Date          toTime,
                                           Map           nameMap)
        throws DBException, IOException
    {
        if (!inDepotSchemaVersion(depotSchemaVersion))
            return null;

        // Define the sql time predicate as [fromTime, toTime>.
        //
        TimeIntervalPredicate predicate = 
            new TimeIntervalPredicate(fromTime, toTime, lastUpdateTimeNm());

        return query(predicate, nameMap);
    }

    /**
     * Returns the number of records upto the given time.
     *
     * @param	time			   the ending time (exclusive)
     * @param	depotSchemaVersion the version of DB schema used on the depot
     *
     * @return	The number of records or -1 if not supported in the
     *          depot schema version requesting the sync.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getRecordCount(Date time, VersionNumber depotSchemaVersion)
	throws DBException, IOException
    {
        if (!inDepotSchemaVersion(depotSchemaVersion))
            return -1;

        // Define the sql time predicate as [0, toTime>.
        //
        TimeIntervalPredicate predicate = 
            new TimeIntervalPredicate(null, time, lastUpdateTimeNm());

        String sql = 
            "select count(*) from " +
            getTableName() + " where" + predicate.generateWherePart(null);

        return count(predicate, sql);
    }
}

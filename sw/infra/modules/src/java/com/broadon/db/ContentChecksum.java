package com.broadon.db;

import com.broadon.sql.PredicateItem;

/**
 * The <code>ContentChecksum</code> class conforms to the JavaBeans property,
 * and is a Predicate that finds various records.
 *
 * @version	$Revision: 1.2 $
 */
public class ContentChecksum
    extends PredicateItem
{
    private String	contentChecksum;

    /**
     * Constructs a ContentChecksum instance.
     *
     * @param	contentChecksum		the content checksum
     */
    public ContentChecksum(String contentChecksum)
    {
	super();
	this.contentChecksum = contentChecksum;
    }

    /**
     * Getter for contentChecksum.
     *
     * @return	The content checksum.
     */
    public String getContentChecksum()
    {
	return contentChecksum;
    }

    /**
     * Setter for contentChecksum.
     *
     * @param	contentChecksum		the content checksum
     */
    public void setContentChecksum(String contentChecksum)
    {
	this.contentChecksum = contentChecksum;
    }
}

package com.broadon.db;

/**
 * The <c>Content</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *<p>
 * It extends <c>ContentBase</c> by including the content itself.
 *
 * @version	$Revision: 1.1 $
 */
public class Content
    extends ContentBase
{
    private String		content;

    /**
     * Constructs an empty Content instance.
     */
    public Content()
    {
	super();
    }

    /**
     * Getter for content.
     *
     * @return	The content itself.
     */
    public String getContent()
    {
	return content;
    }

    /**
     * Setter for content.
     *
     * @param	content			the content itself
     */
    public void setContent(String content)
    {
	this.content = content;
    }
}

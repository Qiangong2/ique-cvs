package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <c>GlobalETickets</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *<p>
 * It contains information about operation users.
 *
 * @version	$Revision: 1.2 $
 */
public class GlobalETickets
    extends PBean
{
    private Long		TID;
    private Long		contentID;
    private Long		businessUnitID;
    private Date		createDate;
    private Date		revokeDate;

    /**
     * Constructs an empty GlobalEtickets instance.
     */
    public GlobalETickets()
    {
    }

    /**
     * Getter for TID.
     *
     * @return	The TID identifier.
     */
    public Long getTID()
    {
	return TID;
    }

    /**
     * Setter for TID.
     *
     * @param	TID		the TID identifier
     */
    public void setTID(Long TID)
    {
	this.TID = TID;
    }

    /**
     * Getter for contentID.
     *
     * @return	The content identifier.
     */
    public Long getContentID()
    {
	return contentID;
    }

    /**
     * Setter for contentID.
     *
     * @param	contentID		the content identifier
     */
    public void setContentID(Long contentID)
    {
	this.contentID = contentID;
    }

    /**
     * Getter for businessUnitID.
     *
     * @return	The business unit identifier.
     */
    public Long getBusinessUnitID()
    {
	return businessUnitID;
    }

    /**
     * Setter for businessUnitID.
     *
     * @param	businessUnitID		the business unit identifier
     */
    public void setBusinessUnitID(Long businessUnitID)
    {
	this.businessUnitID = businessUnitID;
    }

    /**
     * Getter for createDate.
     *
     * @return	The create date for a global eticket.
     */
    public Date getCreateDate()
    {
	return createDate;
    }

    /**
     * Setter for createDate.
     *
     * @param	createDate		the create date
     */
    public void setCreateDate(Date createDate)
    {
	this.createDate = createDate;
    }

    /**
     * Getter for revokeDate.
     *
     * @return	The revoke date for a global eticket.
     */
    public Date getRevokeDate()
    {
	return revokeDate;
    }

    /**
     * Setter for revokeDate.
     *
     * @param	revokeDate		the revoke date
     */
    public void setRevokeDate(Date revokeDate)
    {
	this.revokeDate = revokeDate;
    }
}

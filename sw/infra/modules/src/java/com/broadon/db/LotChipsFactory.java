package com.broadon.db;

import java.io.IOException;
import java.util.Map;

import com.broadon.bean.Bean;

/**
 * The <c>LotChipsFactory</c> class interfaces with the database to
 * create, update, or query records from the LotChips table.
 * Each record is represented by a LotChips JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.3 $
 */
public class LotChipsFactory
    extends SyncableFactory
{
    private static final String TABLE_NAME;
    private static final String	SELECT_LOT_CHIPS_LIST;
    private static final String	SELECT_LOT_CHIPS_COUNT;

    static
    {
	TABLE_NAME		= "LOT_CHIPS";
	SELECT_LOT_CHIPS_LIST	=
	    "select bb_id, lot_number, process_date, cert from " + TABLE_NAME;
	SELECT_LOT_CHIPS_COUNT	= "select count(*) from " + TABLE_NAME;
    }

    /**
     * Constructs a LotChipsFactory instance.
     */
    protected LotChipsFactory()
    {
	super();
    }

    /**
     * Returns the request information identified by the given requestID.
     *
     * @param	bbID			the chip identifier
     * @param	nameMap			the name mapping table
     *
     * @return	The LotChips JavaBeans that contains information of a
     *		LotChips record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public LotChips getLotChips(long bbID, Map nameMap)
	throws DBException, IOException
    {
	LotChipsID		predicate = new LotChipsID(bbID);

	return (LotChips)queryUnique(predicate, nameMap);
    }

    /**
     * Returns the request information identified by the given lotNumber
     *
     * @param	lotNumber			the lot identifier
     *
     * @return	The XML document that contains information of a
     *		LotChipss record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getLotChips(String lotNumber)
	throws DBException, IOException
    {
	return query(SELECT_LOT_CHIPS_LIST + " where lot_number = '" + lotNumber + "'");
    }

    /**
     * Returns the request information of a given range.
     *
     * @param	lotNumber		the lot identifier
     * @param	from			the starting record number
     * @param	to			the ending record number
     * @param	order			the sorted order
     *
     * @return	The XML document that contains the tilte information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getLotChips(int from, int to, String order, String lotNumber)
	throws DBException, IOException
    {
	String	sql = "select * from (select rownum no, inner.* from (" +
			SELECT_LOT_CHIPS_LIST +
			    " where lot_number = '" + lotNumber +
			    "' order by " + order + ") " +
			"inner where rownum <= " + to + ") " +
			"where no > " + from;

	return query(sql);
    }

    /**
     * Returns the request information of a given range.
     *
     * @param	from			the starting record number
     * @param	to			the ending record number
     * @param	order			the sorted order
     *
     * @return	The XML document that contains the tilte information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getLotChips(int from, int to, String order)
	throws DBException, IOException
    {
	String	sql = "select * from (select rownum no, inner.* from (" +
			SELECT_LOT_CHIPS_LIST + " order by " + order + ") " +
			"inner where rownum <= " + to + ") " +
			"where no > " + from;

	return query(sql);
    }

    /**
     * Returns the number of requests available.
     *
     * @param	lotNumber			the lot identifier
     *
     * @return	The integer that contains the number of requests.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getLotChipsCount(String lotNumber)
	throws DBException, IOException
    {
	return count(SELECT_LOT_CHIPS_COUNT + " where lot_number = '" + lotNumber + "'");
    }

    /**
     * Returns the number of requests available.
     *
     * @return	The integer that contains the number of requests.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getLotChipsCount()
	throws DBException, IOException
    {
	return count(SELECT_LOT_CHIPS_COUNT);
    }

    /**
     * Returns the database table name to be used.
     *
     * @return	The data table name.
     */
    public String getTableName()
    {
	return TABLE_NAME;
    }

    /**
     * Creates a LotChips JavaBeans object.
     *
     * @return	The newly created LotChips object.
     */
    protected Bean newBean()
    {
	return new LotChips();
    }
}

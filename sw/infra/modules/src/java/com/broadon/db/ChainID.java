package com.broadon.db;

import com.broadon.sql.PredicateItem;

/**
 * The <c>ChainID</c> class conforms to the JavaBeans property,
 * and is a Predicate that finds various records.
 *
 * @version	$Revision: 1.1 $
 */
public class ChainID
    extends PredicateItem
{
    private Integer	chainID;

    /**
     * Constructs a ChainID instance.
     *
     * @param	chainID		the chain identifier
     */
    public ChainID(int chainID)
    {
	super();
	this.chainID = new Integer(chainID);
    }

    /**
     * Getter for chainID.
     *
     * @return	The chain identifier.
     */
    public Integer getChainID()
    {
	return chainID;
    }

    /**
     * Setter for chainID.
     *
     * @param	chainID		the chain identifier
     */
    public void setChainID(Integer chainID)
    {
	this.chainID = chainID;
    }
}

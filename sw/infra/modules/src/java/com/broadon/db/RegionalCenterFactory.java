package com.broadon.db;

import java.io.IOException;
import java.util.Map;

import com.broadon.bean.Bean;

/**
 * The <code>RegionalCenterFactory</code> class interfaces with the database
 * to create, update, or query records from the RegionalCenters table.
 * Each record is represented by a RegionalCenter JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.7 $
 */
public class RegionalCenterFactory
    extends DBAccessFactory
{
    private static final String TABLE_NAME;
    private static final String	SELECT_FROM_DEPOT;
    private static final String	SELECT_REGIONAL_CENTER_NAMES;
    private static final String	SELECT_REGIONAL_CENTER_LIST;
    private static final String	SELECT_REGIONAL_CENTER_COUNT;

    static
    {
	TABLE_NAME			= "REGIONAL_CENTERS";
	SELECT_FROM_DEPOT		=
	    "select * from " + TABLE_NAME + " where regional_center_id in " +
		"(select regional_center_id from stores, depots where " +
		    "depots.hr_id = ? and " +
		    "depots.suspend_date is null and " +
		    "depots.revoke_date is null and " +
		    "depots.store_id = stores.store_id and " +
		    "stores.suspend_date is null and " +
		    "stores.revoke_date is null)";
        SELECT_REGIONAL_CENTER_NAMES     =
            "select rownum no, regional_center_id, center_name from " +
		TABLE_NAME +
                " order by center_name";
	SELECT_REGIONAL_CENTER_LIST	= "select * from " + TABLE_NAME;
	SELECT_REGIONAL_CENTER_COUNT	=
	    "select count(regional_center_id) from " + TABLE_NAME;
    }

    /**
     * Constructs a RegionalCenterFactory instance.
     */
    protected RegionalCenterFactory()
    {
	super();
    }

    /**
     * Returns the regional center identified by the given regional center
     * identifier.
     *
     * @param	regionalCenterID	the regional center identifier
     * @param	nameMap			the name mapping table
     *
     * @return	The RegionalCenter JavaBeans that contains information of a
     *		Regional-Centers record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public RegionalCenter getRegionalCenter(int regionalCenterID, Map nameMap)
	throws DBException, IOException
    {
	RegionalCenterID	predicate;

	predicate = new RegionalCenterID(regionalCenterID);
	return (RegionalCenter)queryUnique(predicate, nameMap);
    }

    /**
     * Returns the regional center that the depot identified by the given
     * depot identifier belongs to.
     *
     * @param	depotID			the depot identifier
     * @param	nameMap			the name mapping table
     *
     * @return	The RegionalCenter JavaBeans that contains information of a
     *		Regional-Centers record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public RegionalCenter getRegionalCenterOfDepot(long depotID, Map nameMap)
	throws DBException, IOException
    {
	DepotID		predicate = new DepotID(depotID);

	return (RegionalCenter)queryUnique(predicate,
					   SELECT_FROM_DEPOT,
					   nameMap);
    }

    /**
     * Returns the regional center information of a given range.
     *
     * @param	from			the starting record number
     * @param	to			the ending record number
     * @param	order			the sorted order
     *
     * @return	The XML document that contains the tilte information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getRegionalCenters(int from, int to, String order)
	throws DBException, IOException
    {
	String	sql = "select * from (select rownum no, inner.* from (" +
			SELECT_REGIONAL_CENTER_LIST +
			    " order by " + order + ") " +
			"inner where rownum <= " + to + ") " +
			"where no > " + from;

	return query(sql);
    }

   /**
     * Returns the regional center with the given id.
     *
     * @param   regionalCenterID                 the regional center identifier
     *
     * @return The XML document describing the information of regional
     *          title information.
     *
     * @throws  DBException
     * @throws  IOException
     */
    public String getRegionalCenter(String regionalCenterID)
        throws DBException, IOException
    {
        String  sql = SELECT_REGIONAL_CENTER_LIST +
                        " where regional_center_id = " + regionalCenterID;
        return query(sql);
    }

    /**
     * Returns the regional cneter identifier and regional center names.
     *
     * @return The XML document that contains information of Regional Center records.
     *
     * @throws  DBException
     * @throws  IOException
     */
    public String getRegionalCenterNames()
        throws DBException, IOException
    {
        return query(SELECT_REGIONAL_CENTER_NAMES);
    }

    /**
     * Returns the number of regional centers available.
     *
     * @param	regionalCenterID	the regional center identifier
     *
     * @return	The integer that contains the number of regional centers.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getRegionalCenterCount(long regionalCenterID)
	throws DBException, IOException
    {
	return count(SELECT_REGIONAL_CENTER_COUNT +
			" where regional_center_id = " + regionalCenterID);
    }

    /**
     * Returns the number of regional centers available.
     *
     * @return	The integer that contains the number of regional centers.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getRegionalCenterCount()
	throws DBException, IOException
    {
	return count(SELECT_REGIONAL_CENTER_COUNT);
    }

    /**
     * Returns the database table name to be used.
     *
     * @return	The data table name.
     */
    public String getTableName()
    {
	return TABLE_NAME;
    }

    /**
     * Creates a RegionalCenter JavaBeans object.
     *
     * @return	The newly created RegionalCenter object.
     */
    protected Bean newBean()
    {
	return new RegionalCenter();
    }
}

package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <c>BusinessUnit</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *<p>
 * It contains information about a business unit.
 *
 * @version	$Revision: 1.3 $
 */
public class BusinessUnit
    extends PBean
{
    private Integer		businessUnitID;
    private String		businessName;
    private String		contactInfo;
    private Date		createDate;

    /**
     * Constructs an empty BusinessUnit instance.
     */
    public BusinessUnit()
    {
	this.businessUnitID = null;
	this.createDate = null;
    }

    /**
     * Getter for businessUnitID.
     *
     * @return	The business unit identifier.
     */
    public Integer getBusinessUnitID()
    {
	return businessUnitID;
    }

    /**
     * Setter for businessUnitID.
     *
     * @param	businessUnitID		the business unit identifier
     */
    public void setBusinessUnitID(Integer businessUnitID)
    {
	this.businessUnitID = businessUnitID;
    }

    /**
     * Getter for businessName.
     *
     * @return	The business name.
     */
    public String getBusinessName()
    {
	return businessName;
    }

    /**
     * Setter for businessName.
     *
     * @param	businessName		the business name
     */
    public void setBusinessName(String businessName)
    {
	this.businessName = businessName;
    }

    /**
     * Getter for contactInfo.
     *
     * @return	The contact information.
     */
    public String getContactInfo()
    {
	return contactInfo;
    }

    /**
     * Setter for contactInfo.
     *
     * @param	contactInfo		the contact information
     */
    public void setContactInfo(String contactInfo)
    {
	this.contactInfo = contactInfo;
    }

    /**
     * Getter for createDate.
     *
     * @return	The date of creation.
     */
    public Date getCreateDate()
    {
	return createDate;
    }

    /**
     * Setter for createDate.
     *
     * @param	createDate		the date of creation
     */
    public void setCreateDate(Date createDate)
    {
	this.createDate = createDate;
    }
}

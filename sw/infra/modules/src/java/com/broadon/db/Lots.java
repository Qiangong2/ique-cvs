package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <c>Lots</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *<p>
 * It contains information about lots identified by the lotNumber identifier.
 *
 * @version	$Revision: 1.3 $
 */
public class Lots
    extends PBean
{
    private String		lotNumber;
    private Long		lotSize;
    private Date		createDate;
    private Date		lastProcessed;
    private Date		lastReported;
    private String		chipRevision;
    private String		certificateID;
    private String		manufacturer;
    private String		location;
    private Long		numProcessed;

    /**
     * Constructs an empty Lots instance.
     */
    public Lots()
    {
    }

    /**
     * Getter for lotNumber.
     *
     * @return	The lot identifier.
     */
    public String getLotNumber()
    {
	return lotNumber;
    }

    /**
     * Setter for lotNumber.
     *
     * @param	lotNumber			the lot identifier
     */
    public void setLotNumber(String lotNumber)
    {
	this.lotNumber = lotNumber;
    }

    /**
     * Getter for lotSize.
     *
     * @return	The lot size.
     */
    public Long getLotSize()
    {
	return lotSize;
    }

    /**
     * Setter for lotSize.
     *
     * @param	lotSize			the lot size
     */
    public void setLotSize(Long lotSize)
    {
	this.lotSize = lotSize;
    }

    /**
     * Getter for createDate.
     *
     * @return	The createDate.
     */
    public Date getCreateDate()
    {
	return createDate;
    }

    /**
     * Setter for createDate.
     *
     * @param	createDate	the create date
     */
    public void setCreateDate(Date createDate)
    {
	this.createDate = createDate;
    }

    /**
     * Getter for lastProcessed.
     *
     * @return	The last processed date for this lot.
     */
    public Date getLastProcessed()
    {
	return lastProcessed;
    }

    /**
     * Setter for lastProcessed.
     *
     * @param	lastProcessed	the last processed date for this lot
     */
    public void setLastProcessed(Date lastProcessed)
    {
	this.lastProcessed = lastProcessed;
    }

    /**
     * Getter for lastReported.
     *
     * @return	The last reported date for this lot.
     */
    public Date getLastReported()
    {
	return lastReported;
    }

    /**
     * Setter for lastReported.
     *
     * @param	lastReported	the last reported date for this lot
     */
    public void setLastReported(Date lastReported)
    {
	this.lastReported = lastReported;
    }

    /**
     * Getter for chipRevision.
     *
     * @return	The chip revision.
     */
    public String getChipRevision()
    {
	return chipRevision;
    }

    /**
     * Setter for chipRevision.
     *
     * @param	chipRevision		the chip revision
     */
    public void setChipRevision(String chipRevision)
    {
	this.chipRevision = chipRevision;
    }

    /**
     * Getter for certificateID.
     *
     * @return	The certificate ID.
     */
    public String getCertificateID()
    {
	return certificateID;
    }

    /**
     * Setter for certificateID.
     *
     * @param	certificateID		the certificate ID
     */
    public void setCertificateID(String certificateID)
    {
	this.certificateID = certificateID;
    }

    /**
     * Getter for manufacturer.
     *
     * @return	The manufacturer.
     */
    public String getProcessStatus()
    {
	return manufacturer;
    }

    /**
     * Setter for manufacturer.
     *
     * @param	manufacturer		the manufacturer
     */
    public void setProcessStatus(String manufacturer)
    {
	this.manufacturer = manufacturer;
    }

    /**
     * Getter for location.
     *
     * @return	The location of the manufacturer.
     */
    public String getLocation()
    {
	return location;
    }

    /**
     * Setter for location.
     *
     * @param	location		the location of the manufacturer
     */
    public void setLocation(String location)
    {
	this.location = location;
    }

    /**
     * Getter for numProcessed.
     *
     * @return	The number processed.
     */
    public Long getNumProcessed()
    {
	return numProcessed;
    }

    /**
     * Setter for numProcessed.
     *
     * @param	numProcessed	the number processed
     */
    public void setNumProcessed(Long numProcessed)
    {
	this.numProcessed = numProcessed;
    }
}

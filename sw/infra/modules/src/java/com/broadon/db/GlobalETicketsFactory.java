package com.broadon.db;

import java.io.IOException;
import java.util.Map;

import com.broadon.bean.Bean;

/**
 * The <code>GlobalETicketsFactory</code> class interfaces with the database
 * to update or query records from the global_etickets  table.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.2 $
 */
public class GlobalETicketsFactory
    extends DBAccessFactory
{
    private static final String TABLE_NAME;
    private static final String	SELECT_GLOBAL_ETICKETS_LIST;
    private static final String	SELECT_GLOBAL_ETICKETS_COUNT;

    static
    {
	TABLE_NAME		= "GLOBAL_ETICKETS";
	SELECT_GLOBAL_ETICKETS_LIST	=
	    "select ct.TITLE, cto.TITLE_ID, ge.CONTENT_ID, co.CONTENT_OBJECT_NAME, co.CONTENT_OBJECT_VERSION, bu.BUSINESS_NAME, ge.BU_ID, " +
            "ge.TID, ge.CREATE_DATE, ctops.is_content_object_latest(co.CONTENT_ID, co.CONTENT_OBJECT_NAME, co.CONTENT_OBJECT_VERSION, " +
            "cto.TITLE_ID, co.REVOKE_DATE) IS_CONTENT_OBJECT_LATEST from CONTENT_TITLES ct, CONTENT_TITLE_OBJECTS cto, " +
            "GLOBAL_ETICKETS ge, CONTENT_OBJECTS co, BUSINESS_UNITS bu where ge.CONTENT_ID = cto.CONTENT_ID and " +
            "cto.TITLE_ID = ct.TITLE_ID and cto.CONTENT_ID = co.CONTENT_ID and ge.BU_ID = bu.BU_ID and " +
            "lower(ct.TITLE_TYPE)='push' and ge.revoke_date is null";
        SELECT_GLOBAL_ETICKETS_COUNT     = "select count(*) from " + TABLE_NAME + " where revoke_date is null";
    }

    /**
     * Constructs a GlobalETicketsFactory instance.
     */
    protected GlobalETicketsFactory()
    {
	super();
    }

    /**
     * Returns the global etickets information of a given range.
     *
     * @param   from                    the starting record number
     * @param   to                      the ending record number
     * @param   order                   the sorted order
     *
     * @return The XML document that contains the operator information.
     *
     * @throws  DBException
     * @throws  IOException
     */
    public String getGlobalETickets(int from, int to, String order)
        throws DBException, IOException
    {
        String  sql = "select * from (select rownum no, inner.* from (" +
                        SELECT_GLOBAL_ETICKETS_LIST + " order by " + order + ") " +
                        "inner where rownum <= " + to + ") " +
                        "where no > " + from;

        return query(sql);
    }

    /**
     * Returns the holes in tid for a given bu_id.
     *
     * @param   buid    the business unit identifier
     *
     * @return bean array with the available tid's
     *
     * @throws  DBException
     * @throws  IOException
     */
    public synchronized GlobalETickets[] getHolesInTicketID(int buid, Map nameMap)
        throws DBException, IOException
    {
        String  sql = "SELECT tid FROM " + TABLE_NAME + " WHERE bu_id = ? " +
                      "GROUP BY tid HAVING count(*)=sum(decode(revoke_date,NULL,0,1)) " +
                      "ORDER BY tid desc";

        Bean[]                  beans = query(new BusinessUnitID(buid), sql, nameMap);

        if (beans == null)
            return null;

        int                     count = beans.length;
        GlobalETickets[] tid = new GlobalETickets[count];

        for (int n = 0; n < count; n++)
            tid[n] = (GlobalETickets)beans[n];

        return tid;
    }

    /**
     * Returns the min(tid) thats available for use.
     *
     * @return The integer that can be used as tid for the next record to be created
     *
     * @throws  DBException
     * @throws  IOException
     */
    public synchronized int getNextTicketID(String buid)
        throws DBException, IOException
    {
        String sql = "SELECT NVL(MIN(tid), 32768)-1 FROM " + TABLE_NAME + " WHERE bu_id = " + buid;

        return count(sql);
    }

    /**
     * Returns the number of global etickets available.
     *
     * @return The integer that contains the number of global etickets (non-revoked).
     *
     * @throws  DBException
     * @throws  IOException
     */
    public int getGlobalETicketsCount()
        throws DBException, IOException
    {
        return count(SELECT_GLOBAL_ETICKETS_COUNT);
    }

    /**
     * Returns the database table name to be used.
     *
     * @return	The data table name.
     */
    public String getTableName()
    {
	return TABLE_NAME;
    }

    /**
     * Creates a GlobalEtickets JavaBeans object.
     *
     * @return The newly created GlobalEtickets object.
     */
    protected Bean newBean()
    {
        return new GlobalETickets();
    }
}

package com.broadon.db;

import java.io.IOException;
import java.util.Map;

import com.broadon.bean.Bean;

/**
 * The <code>OperationUserFactory</code> class interfaces with the database
 * to create, update, or query records from the Operation_Users table.
 * Each record is represented by a OperationUser JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.7 $
 */
public class OperationUserFactory
    extends DBAccessFactory
{
    private static final String TABLE_NAME;
    private static final String GET_ROLE_NAME_COND;
    private static final String	SELECT_OPERATION_USER_EMAILS;
    private static final String	SELECT_OPERATION_USER_LIST;
    private static final String	SELECT_OPERATION_USER_COUNT;

    static
    {
	TABLE_NAME		= "OPERATION_USERS";
	GET_ROLE_NAME_COND = "orl.role_level = ou.role_level";
	SELECT_OPERATION_USER_EMAILS	=
	    "select rownum no, operator_id, email_address from " + TABLE_NAME +
	    " order by email_address";
	SELECT_OPERATION_USER_LIST	=
	    "select ou.operator_id, ou.email_address, ou.status, ou.fullname, " +
		"ou.email_alerts, ou.passwd, orl.role_name, orl.description, " +
		"ou.role_level, ou.last_logon, ou.status_date, ou.other_info, ou.store_id from " + 
                TABLE_NAME + " ou, OPERATION_ROLES orl";
	SELECT_OPERATION_USER_COUNT	= "select count(*) from " + TABLE_NAME;
    }

    /**
     * Constructs a OperationUserFactory instance.
     */
    protected OperationUserFactory()
    {
	super();
    }

    /**
     * Returns the operator identified by the given operator identifier.
     *
     * @param	operatorID		the operator identifier
     * @param	nameMap			the name mapping table
     *
     * @return	The OperationUser JavaBeans that contains information of a
     *		OperationUser record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public OperationUser getOperationUser(long operatorID, Map nameMap)
	throws DBException, IOException
    {
	return (OperationUser)queryUnique(new OperationUserID(operatorID), nameMap);
    }

    /**
     * Returns the operator identified by the given email address.
     *
     * @param	emailAddress		the email address
     * @param	nameMap			the name mapping table
     *
     * @return	The OperationUser JavaBeans that contains information of a
     *		OperationUser record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public OperationUser getOperationUser(String emailAddress, Map nameMap)
	throws DBException, IOException
    {
	return (OperationUser)queryUnique(new OperationUserEmailAddress(emailAddress), nameMap);
    }

    /**
     * Returns the operator information of a given range.
     *
     * @param	from			the starting record number
     * @param	to			the ending record number
     * @param	order			the sorted order
     *
     * @return	The XML document that contains the operator information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getOperationUsers(int from, int to, String order)
	throws DBException, IOException
    {
	String	sql = "select * from (select rownum no, inner.* from (" +
			SELECT_OPERATION_USER_LIST + " where " +
			GET_ROLE_NAME_COND + " order by " + order + ") " +
			"inner where rownum <= " + to + ") " +
			"where no > " + from;

	return query(sql);
    }

    /**
     * Returns the operator identifier and email addresses.
     *
     * @return	The XML document that contains information of OperationUser records.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getOperationUserEmails()
	throws DBException, IOException
    {
	return query(SELECT_OPERATION_USER_EMAILS);
    }

   /**
     * Returns the operator details with the given id.
     *
     * @param   operatorID                 the operator identifier
     *
     * @return The XML document describing the information of operator
     *          information.
     *
     * @throws  DBException
     * @throws  IOException
     */
    public String getOperationUser(String operatorID)
        throws DBException, IOException
    {
        String  sql = SELECT_OPERATION_USER_LIST + " where " +
			GET_ROLE_NAME_COND + " and ou.operator_id = " + operatorID;
        return query(sql);
    }

   /**
     * Returns the operator details with the given email address.
     *
     * @param   emailAddress                 the email address
     *
     * @return The XML document describing the information of operator
     *          information.
     *
     * @throws  DBException
     * @throws  IOException
     */
    public String getOperationUserByEmail(String emailAddress)
        throws DBException, IOException
    {
        String  sql = SELECT_OPERATION_USER_LIST + " where " +
			          GET_ROLE_NAME_COND + " and ou.email_address = '" + emailAddress + "'";
        return query(sql);
    }

    /**
     * Returns the number of operators available.
     *
     * @param	operatorID		the operator identifier
     *
     * @return	The integer that contains the number of operators.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getOperationUserCount(long operatorID)
	throws DBException, IOException
    {
	return count(SELECT_OPERATION_USER_COUNT + " where operator_id = " + operatorID);
    }

    /**
     * Returns the number of operators available.
     *
     * @param	operatorID		the operator identifier
     *
     * @return	The integer that contains the number of operators.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getOperationUserCount(String operatorID)
	throws DBException, IOException
    {
	return count(SELECT_OPERATION_USER_COUNT + " where operator_id = " + operatorID);
    }

    /**
     * Returns the number of operators available.
     *
     * @param	operatorID		the operator identifier
     *
     * @return	The integer that contains the number of operators.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int checkEmailAddressCount(String email)
	throws DBException, IOException
    {
	return count(SELECT_OPERATION_USER_COUNT + " where email_address = '" + 
			email + "'");
    }

    /**
     * Returns the number of operators available.
     *
     * @return	The integer document that contains the number of operators.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getOperationUserCount()
	throws DBException, IOException
    {
	return count(SELECT_OPERATION_USER_COUNT);
    }

    /**
     * Returns the database table name to be used.
     *
     * @return	The data table name.
     */
    public String getTableName()
    {
	return TABLE_NAME;
    }

    /**
     * Creates a OperationUser JavaBeans object.
     *
     * @return	The newly created OperationUser object.
     */
    protected Bean newBean()
    {
	return new OperationUser();
    }
}

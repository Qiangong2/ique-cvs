package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <c>Retailer</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *<p>
 * It contains information about retailers.
 *
 * @version	$Revision: 1.3 $
 */
public class Retailer
    extends PBean
{
    private Integer		retailerID;
    private String		retailerName;
    private Integer		categoryCode;
    private Date		createDate;
    private Date		revokeDate;
    private Date		suspendDate;

    /**
     * Constructs an empty Retailer instance.
     */
    public Retailer()
    {
    }

    /**
     * Getter for retailerID.
     *
     * @return	The retailer identifier.
     */
    public Integer getRetailerID()
    {
	return retailerID;
    }

    /**
     * Setter for retailerID.
     *
     * @param	retailerID		the retailer identifier
     */
    public void setRetailerID(Integer retailerID)
    {
	this.retailerID = retailerID;
    }

    /**
     * Getter for retailerName.
     *
     * @return	The retailer name.
     */
    public String getRetailerName()
    {
	return retailerName;
    }

    /**
     * Setter for retailerName.
     *
     * @param	retailerName		the retailer name
     */
    public void setRetailerName(String retailerName)
    {
	this.retailerName = retailerName;
    }

    /**
     * Getter for categoryCode.
     *
     * @return	The category code.
     */
    public Integer getCategoryCode()
    {
	return categoryCode;
    }

    /**
     * Setter for categoryCode.
     *
     * @param	categoryCode		the category code
     */
    public void setCategoryCode(Integer categoryCode)
    {
	this.categoryCode = categoryCode;
    }

    /**
     * Getter for createDate.
     *
     * @return	The creation date of this retailer.
     */
    public Date getCreateDate()
    {
	return createDate;
    }

    /**
     * Setter for createDate.
     *
     * @param	createDate		the creation date
     */
    public void setCreateDate(Date createDate)
    {
	this.createDate = createDate;
    }

    /**
     * Getter for revokeDate.
     *
     * @return	The revoked date of this retailer.
     */
    public Date getRevokeDate()
    {
	return revokeDate;
    }

    /**
     * Setter for revokeDate.
     *
     * @param	revokeDate		the revoked date
     */
    public void setRevokeDate(Date revokeDate)
    {
	this.revokeDate = revokeDate;
    }

    /**
     * Getter for suspendDate.
     *
     * @return	The suspended date of this retailer.
     */
    public Date getSuspendDate()
    {
	return suspendDate;
    }

    /**
     * Setter for suspendDate.
     *
     * @param	suspendDate		the suspended date
     */
    public void setSuspendDate(Date suspendDate)
    {
	this.suspendDate = suspendDate;
    }
}

package com.broadon.db;

import com.broadon.sql.PredicateItem;

/**
 * The <c>ServerType</c> class conforms to the JavaBeans property,
 * and is a Predicate that finds the RegionalServer identifiy by the
 * regional center identifier and this server type.
 *
 * @version	$Revision: 1.1 $
 */
public class ServerType
    extends PredicateItem
{
    private String	serverType;

    /**
     * Constructs a ServerType type instance.
     *
     * @param	serverType		the type of the server
     */
    public ServerType(String serverType)
    {
	super();
	this.serverType = serverType;
    }

    /**
     * Getter for serverType.
     *
     * @return	The type of the server.
     */
    public String getServerType()
    {
	return serverType;
    }

    /**
     * Setter for serverType.
     *
     * @param	serverType		the type of the server
     */
    public void setServerType(String serverType)
    {
	this.serverType = serverType;
    }
}

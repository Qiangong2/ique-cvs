package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PredicateItem;

/**
 * The <c>UpdatedAtTime</c> class conforms to the JavaBeans property, and is
 * a Predicate that finds records that were updated at the given time.
 *
 * @version	$Revision: 1.1 $
 */
public class UpdatedAtTime
    extends PredicateItem
{
    private Date	time;

    /**
     * Constructs a UpdatedAtTime instance.
     *
     * @param	time			the matching time
     */
    public UpdatedAtTime(Date time)
    {
	this(time, "=");
    }

    /**
     * Constructs a UpdatedAtTime instance. This constructor is used by the
     * subclasses to override the operator.
     *
     * @param	time			the matching time
     * @param	operator		the operator
     */
    protected UpdatedAtTime(Date time, String operator)
    {
	super(operator);
	this.time = time;
    }

    /**
     * Getter for lastUpdated.
     *
     * @return	The matching time.
     */
    public Date getLastUpdated()
    {
	return time;
    }

    /**
     * Setter for lastUpdated.
     *
     * @param	time			the matching time
     */
    public void setLastUpdated(Date time)
    {
	this.time = time;
    }
}

package com.broadon.db;

import com.broadon.sql.PredicateItem;

/**
 * The <c>OperationUserID</c> class conforms to the JavaBeans property,
 * and is a Predicate that finds various records.
 *
 * @version	$Revision: 1.1 $
 */
public class OperationUserID
    extends PredicateItem
{
    private Long	operatorID;

    /**
     * Constructs a OperationUserID instance.
     *
     * @param	operatorID		the operator identifier
     */
    public OperationUserID(long operatorID)
    {
	super();
	this.operatorID = new Long(operatorID);
    }

    /**
     * Getter for operatorID.
     *
     * @return	The operator identifier.
     */
    public Long getOperatorID()
    {
	return operatorID;
    }

    /**
     * Setter for operatorID.
     *
     * @param	operatorID		the operator identifier
     */
    public void setOperatorID(Long operatorID)
    {
	this.operatorID = operatorID;
    }
}

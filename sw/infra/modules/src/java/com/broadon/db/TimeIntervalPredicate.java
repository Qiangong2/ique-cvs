package com.broadon.db;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.Map;

import com.broadon.sql.Predicate;

/**
 * The <c>TimeIntervalPredicate</c> class is a Predicate that finds 
 * records that were updated within the given time interval [fromTime,toTime>.
 * 
 * When the fromTime is null, all times less than the toTime are accepted.
 * When the toTime is null, all times larger than or equal to the fromTime
 * are accepted.
 *
 * @version	$Revision: 1.2 $
 */
public class TimeIntervalPredicate implements Predicate
{
    private Date      fromTime;
    private Date      toTime;
    private String    dbColNm;
    private Predicate next;
    private String    nextOp;

    /**
     * Constructs a UpdatedFromTime instance.
     *
     * @param fromTime the starting time (inclusively) in UTC msecs
     * @param toTime   the end time (exclusively) in UTC msecs
     * @param dbColNm  the name of the database column to compare against
     */
    public TimeIntervalPredicate(Date   fromTime, 
                                 Date   toTime,
                                 String dbColNm)
    {
        this.fromTime = fromTime;
        this.toTime = toTime;
        this.dbColNm = dbColNm;
        this.next = null;
        this.nextOp = null;
    }

    public Predicate and(Predicate predicate) 
    {
        nextOp = AND;
        next = predicate;
        return predicate;
    }

    public Predicate or(Predicate predicate)
    {
        nextOp = OR;
        next = predicate;
        return predicate;
    }

    public Predicate not()
    {
        throw new UnsupportedOperationException();
    }

    /**
     * Binds the predicate to the given PreparedStatement.
     *
     * @param	statement		the PreparedStatement
     *
     * @throws	IOException
     */
    public void bind(PreparedStatement statement)
        throws IOException
    {
        bind(statement, 0);
    }

    /**
     * Binds the predicate to the given PreparedStatement.
     *
     * @param	statement		the PreparedStatement
     * @param	index			the attribute starting index
     *
     * @throws	IOException
     */
    public void bind(PreparedStatement statement, int index)
        throws IOException
    {
        try {
            if (fromTime != null) {
                statement.setLong(++index, fromTime.getTime());
            }
            if (toTime != null) {
                statement.setLong(++index, toTime.getTime());
            }
        }
        catch (SQLException e) {
            throw new IOException("SQL Exception: " + e.getMessage());
        }
    }

    /**
     * Generates the where clause component of this Predicate.
     *
     * @param	nameMap			the name mapping table
     *
     * @return	The where clause component.
     *
     * @throws	IOException
     */
    public String generateWherePart(Map nameMap)
        throws IOException
    {
        StringWriter out = new StringWriter();
        try {
            generateWherePart(out, nameMap);
            out.flush();
            return out.toString();
        }
        finally {
            out.close();
        }
    }

    /**
     * Generates the where clause component of this PredicateItem.
     *
     * @param	out			the output destination
     * @param	nameMap			the name mapping table
     *
     * @throws	IOException
     */
    public void generateWherePart(Writer out, Map nameMap)
        throws IOException
    {
        out.write(' ');
        if (fromTime != null && toTime != null) {
            out.write(dbColNm);
            out.write(" >= BBXML.ConvertTimeStamp(?) "); // fromTime
            out.write(AND);
            out.write(" BBXML.ConvertTimeStamp(?) > ");  // toTime
            out.write(dbColNm);
        }
        else if (fromTime != null) {
            out.write(dbColNm);
            out.write(" >= BBXML.ConvertTimeStamp(?) "); // fromTime
        }
        else if (toTime != null) {
            out.write(" BBXML.ConvertTimeStamp(?) > ");  // toTime
            out.write(dbColNm);
        }
    }
}

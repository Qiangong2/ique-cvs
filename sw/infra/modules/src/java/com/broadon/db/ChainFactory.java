package com.broadon.db;

import java.io.IOException;
import java.util.Map;

import com.broadon.bean.Bean;

/**
 * The <code>ChainFactory</code> class interfaces with the database
 * to create, update, or query records from the Chains table.
 * Each record is represented by a Chain JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.2 $
 */
public class ChainFactory
    extends DBAccessFactory
{
    private static final String TABLE_NAME;
    private static final String	SELECT_CHAIN_LIST;
    private static final String	SELECT_CHAIN_COUNT;

    static
    {
	TABLE_NAME		= "CERTIFICATE_CHAINS";
	SELECT_CHAIN_LIST	=
	    "select chain_id, signer_cert_id, ca_cert_id, description " +
            "from " + TABLE_NAME;
	SELECT_CHAIN_COUNT	= "select count(*) from " + TABLE_NAME;
    }

    /**
     * Constructs a ChainFactory instance.
     */
    protected ChainFactory()
    {
	super();
    }

    /**
     * Returns the chain identified by the given chain identifier.
     *
     * @param	chainID			the chain identifier
     * @param	nameMap			the name mapping table
     *
     * @return	The Chain JavaBeans that contains information of a
     *		Certificate Chain record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public Chain getChain(int chainID, Map nameMap)
	throws DBException, IOException
    {
	return (Chain)queryUnique(new ChainID(chainID), nameMap);
    }

    /**
     * Returns the chainificate information of a given range.
     *
     * @param	from			the starting record number
     * @param	to			the ending record number
     * @param	order			the sorted order
     *
     * @return	The XML document that contains the chainificate information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getChains(int from, int to, String order)
	throws DBException, IOException
    {
	String	sql = "select * from (select rownum no, inner.* from (" +
			SELECT_CHAIN_LIST +
			    " order by " + order + ") " +
			"inner where rownum <= " + to + ") " +
			"where no > " + from;

	return query(sql);
    }

   /**
     * Returns the chain details with the given id.
     *
     * @param   chainID                 the chain identifier
     *
     * @return The XML document describing the information of chainificate
     *          information.
     *
     * @throws  DBException
     * @throws  IOException
     */
    public String getChain(String chainID)
        throws DBException, IOException
    {
        String  sql = SELECT_CHAIN_LIST +
                        " where chain_id = " + chainID;
        return query(sql);
    }

    /**
     * Returns the number of chains available.
     *
     * @param	chainID		the chain identifier
     *
     * @return	The integer that contains the number of chains.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getChainCount(String chainID)
	throws DBException, IOException
    {
	return count(SELECT_CHAIN_COUNT + " where chain_id = " + chainID);
    }

    /**
     * Returns the number of chains available.
     *
     * @return	The integer document that contains the number of chains.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getChainCount()
	throws DBException, IOException
    {
	return count(SELECT_CHAIN_COUNT);
    }

    /**
     * Returns the database table name to be used.
     *
     * @return	The data table name.
     */
    public String getTableName()
    {
	return TABLE_NAME;
    }

    /**
     * Creates a Chain JavaBeans object.
     *
     * @return	The newly created Chain object.
     */
    protected Bean newBean()
    {
	return new Chain();
    }
}

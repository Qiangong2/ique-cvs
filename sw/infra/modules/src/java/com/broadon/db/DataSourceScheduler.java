package com.broadon.db;

/**
 * Scheduler for the database connection pool.  <p>Requests are
 * divided into multiple classes.  Each class has a number of reserved
 * connections.  When the reserved connections of its own class are
 * used up, a request could compete for the shared connections.  This
 * guaranteed that no single class of requests would use up all
 * physical connections.
 */
public class DataSourceScheduler
{
    // number of shared connections currently available
    int sharedAvailable;

    // number of connections (shared or reserved) available for a
    // particular class of request (specified by the array index).
    int localAvailable[];

    // the max. number of shared connections.
    final int maxShared;

    // wait() timeout value
    final int maxTimeOut;

    /**
     * Create a new <code>DataSourceScheduler</code> object.
     * @param classCount Max. number of classes of requests.
     * @param reservedCount Number of connections reserved for each class.
     * @param totalCount Max. number of physical connections in the pool
     */
    public DataSourceScheduler(int classCount, int reservedCount,
			       int totalCount, int timeOut)
    {
	maxShared = totalCount - (classCount * reservedCount);
	if (maxShared < 0)
	    throw new IllegalArgumentException("Max connection too small");

	sharedAvailable = maxShared;
	
	int localMax = reservedCount + maxShared;
	localAvailable = new int[classCount];
	for (int i = 0; i < classCount; ++i) {
	    localAvailable[i] = localMax;
	}

        maxTimeOut = timeOut;
    }


    /**
     * Request for the right to get a connection from the pool.  The
     * requesting thread would block until either a reserved or shared
     * connection is available.  The reserved connection is always
     * assigned first before the thread is allowed to compete for the
     * shared connections.
     * @param idx Specify the class of request.
     * @exception InterruptedException interrupted while being blocked.
     */
    public synchronized void getTurn(int idx)
	throws InterruptedException
    {
        long startTime = System.currentTimeMillis();
        long timeOut;

	// For each class of request, it uses up its reserved slots
	// first, then compete for the shared slots
	while ((localAvailable[idx] <= maxShared) && (sharedAvailable <= 0)) {
            timeOut = maxTimeOut - (System.currentTimeMillis() - startTime);
            if (timeOut <= 0) {
                throw new InterruptedException("DataSourceScheduler.getTurn() timeout");
            }
            wait(timeOut);
	}
	if (localAvailable[idx]-- <= maxShared)
	    --sharedAvailable;
    }

    /**
     * Release the right to use a database connection for other requests.
     * @param idx Specify the class of request.
     */
    public synchronized void yieldTurn(int idx)
    {
	if (++localAvailable[idx] <= maxShared)
	    ++sharedAvailable;
	notifyAll();
    }


    // default number of classes

    /** Highest priority for obtaining database connection. */
    public static final int DB_PRIORITY_FAST = 0;

    /** Medium priority for obtaining database connection. */
    public static final int DB_PRIORITY_MEDIUM = 1;

    /** Lowest priority for obtaining database connection. */
    public static final int DB_PRIORITY_SLOW = 2;

    /** Default (== Medium) priority for obtaining database connection. */
    public static final int DB_PRIORITY_DEFAULT = DB_PRIORITY_MEDIUM;

    protected static final int DB_PRIORITY_MAX = 3;
}

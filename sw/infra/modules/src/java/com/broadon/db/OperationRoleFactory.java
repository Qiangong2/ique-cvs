package com.broadon.db;

import java.io.IOException;
import java.util.Map;

import com.broadon.bean.Bean;

/**
 * The <code>OperationRoleFactory</code> class interfaces with the database
 * to create, update, or query records from the Operation_Roles table.
 * Each record is represented by a OperationRole JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.2 $
 */
public class OperationRoleFactory
    extends DBAccessFactory
{
    private static final String TABLE_NAME;
    private static final String	SELECT_OPERATION_ROLE_LIST;
    private static final String	SELECT_OPERATION_ROLE_COUNT;

    static
    {
	TABLE_NAME		= "OPERATION_ROLES";
	SELECT_OPERATION_ROLE_LIST	=
	    "select role_level, role_name, description, create_date, is_active " +
		" from " + TABLE_NAME;
	SELECT_OPERATION_ROLE_COUNT	= "select count(*) from " + TABLE_NAME;
    }

    /**
     * Constructs a OperationRoleFactory instance.
     */
    protected OperationRoleFactory()
    {
	super();
    }

    /**
     * Returns the role identified by the given role level.
     *
     * @param	roleLevel		the role level
     * @param	nameMap			the name mapping table
     *
     * @return	The OperationRole JavaBeans that contains information of a
     *		Operation Role record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public OperationRole getOperationRole(int roleLevel, Map nameMap)
	throws DBException, IOException
    {
	return (OperationRole)queryUnique(new OperationRoleLevel(roleLevel), nameMap);
    }

    /**
     * Returns the role information of a given range.
     *
     * @param	from			the starting record number
     * @param	to				the ending record number
     * @param	order			the sorted order
     *
     * @return	The XML document that contains the role information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getOperationRoles(int from, int to, String order)
	throws DBException, IOException
    {
	String	sql = "select * from (select rownum no, inner.* from (" +
			SELECT_OPERATION_ROLE_LIST +
			    " order by " + order + ") " +
			"inner where rownum <= " + to + ") " +
			"where no > " + from;

	return query(sql);
    }

    /**
     * Returns the role information.
     *
     * 
     * @return	The XML document that contains the role information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getOperationRolesNames()
	throws DBException, IOException
    {
	   return query(SELECT_OPERATION_ROLE_LIST);
    }

    /**
     * Returns the role details with the given id.
     *
     * @param   roleLevel                 the role level
     *
     * @return The XML document describing the information of role
     *          information.
     *
     * @throws  DBException
     * @throws  IOException
     */
    public String getOperationRole(String roleLevel)
        throws DBException, IOException
    {
        String  sql = SELECT_OPERATION_ROLE_LIST +
                        " where role_level = " + roleLevel;
        return query(sql);
    }

    /**
     * Returns the number of roles available.
     *
     * @return	The integer document that contains the number of roles.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getOperationRoleCount()
	throws DBException, IOException
    {
	return count(SELECT_OPERATION_ROLE_COUNT);
    }

    /**
     * Returns the database table name to be used.
     *
     * @return	The data table name.
     */
    public String getTableName()
    {
	return TABLE_NAME;
    }

    /**
     * Creates a OperationRole JavaBeans object.
     *
     * @return	The newly created OperationRole object.
     */
    protected Bean newBean()
    {
	return new OperationRole();
    }
}

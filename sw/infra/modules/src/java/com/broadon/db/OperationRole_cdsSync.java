package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <c>OperationRole_cdsSync</c> class conforms to the JavaBeans property,
 * and is used for depot updates by the CDS server, where createDate is 
 * renamed as lastUpdate.
 *<p>
 * It contains information about operation roles.
 *
 * @version	$Revision: 1.1 $
 */
public class OperationRole_cdsSync
    extends PBean
{
    private Integer		roleLevel;
    private String		roleName;
    private String		description;
    private Integer		isActive;
    private Date		lastUpdated;

    /**
     * Constructs an empty OperationRole_cdsSync instance.
     */
    public OperationRole_cdsSync()
    {
    }

    /**
     * Getter for roleLevel.
     *
     * @return	The role identifier.
     */
    public Integer getRoleLevel()
    {
	return roleLevel;
    }

    /**
     * Setter for roleLevel.
     *
     * @param	roleLevel		the role identifier
     */
    public void setRoleLevel(Integer roleLevel)
    {
	this.roleLevel = roleLevel;
    }

    /**
     * Getter for roleName.
     *
     * @return	The role name.
     */
    public String getRoleName()
    {
	return roleName;
    }

    /**
     * Setter for roleName.
     *
     * @param	roleName		the role name
     */
    public void setRoleName(String roleName)
    {
	this.roleName = roleName;
    }

    /**
     * Getter for description.
     *
     * @return	The description of the role.
     */
    public String getDescription()
    {
	return description;
    }

    /**
     * Setter for description.
     *
     * @param	description		the description of the role
     */
    public void setDescription(String description)
    {
	this.description = description;
    }

    /**
     * Getter for isActive.
     *
     * @return	The is active flag.
     */
    public Integer getIsActive()
    {
	return isActive;
    }

    /**
     * Setter for isActive.
     *
     * @param	isActive		the is active flag
     */
    public void setIsActive(Integer isActive)
    {
	this.isActive = isActive;
    }

    /**
     * Getter for lastUpdated.
     *
     * @return	The last updated date.
     */
    public Date getLastUpdated()
    {
	return lastUpdated;
    }

    /**
     * Setter for lastUpdated.
     *
     * @param	lastUpdated		the last updated date
     */
    public void setLastUpdated(Date lastUpdated)
    {
	this.lastUpdated = lastUpdated;
    }
}

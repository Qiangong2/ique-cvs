package com.broadon.db;

import com.broadon.bean.Bean;
import com.broadon.util.VersionNumber;

/**
 * The <c>CompetingCategoryFactory</c> class interfaces with the database to
 * create, update, or query records from the CompetingCategorys table.
 * Each record is represented by a CompetingCategory JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.3 $
 */
public class CompetingCategoryFactory
    extends SyncableFactory
{
    private static final String TABLE_NAME = "COMPETING_CATEGORIES";
    private static final VersionNumber VERSION_1_1_90 = VersionNumber.valueOf("1.1.90");

    /**
     * Constructs a CompetingCategoryFactory instance.
     */
    protected CompetingCategoryFactory()
    {
        super();
    }

    /**
     * The records produced by this factory is only supported in depot
     * schema versions 1.1.90 and later.
     *
     * @param	depotSchemaVersionfromTime the schema version used by the depot
     * @return	true if the depot supports the record types; otherwise false.
     */
    public boolean inDepotSchemaVersion(VersionNumber depotSchemaVersion)
    {
        return (depotSchemaVersion.compareTo(VERSION_1_1_90) >= 0);
    }

    /**
     * Returns the database table name to be used.
     *
     * @return	The data table name.
     */
    public String getTableName()
    {
        return TABLE_NAME;
    }

    /**
     * Creates a CompetingCategory JavaBeans object.
     *
     * @return	The newly created CompetingCategory object.
     */
    protected Bean newBean()
    {
        return new CompetingCategory();
    }
}

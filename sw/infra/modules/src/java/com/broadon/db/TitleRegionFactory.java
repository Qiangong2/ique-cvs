package com.broadon.db;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

import com.broadon.bean.Bean;
import com.broadon.sql.Predicate;
import com.broadon.sql.PredicateTerm;

/**
 * The <c>TitleRegionFactory</c> class interfaces with the database to
 * create, update, or query records from the TitleRegions table.
 * Each record is represented by a TitleRegion JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.9 $
 */
public class TitleRegionFactory
    extends SyncableFactory
{
    private static final String TABLE_NAME;
    private static final String	SELECT_WITH_DEPOT_ID;
    private static final String	SELECT_TITLE_REGION_LIST;

    static
    {
	TABLE_NAME		= "CONTENT_TITLE_REGIONS";
	SELECT_WITH_DEPOT_ID	=
	    "select * from " + TABLE_NAME + " where region_id in " +
		"(select region_id from stores, depots where " +
		    "depots.hr_id = ? and " +
		    "depots.suspend_date is null and " +
		    "depots.revoke_date is null and " +
		    "depots.store_id = stores.store_id and " +
		    "stores.suspend_date is null and " +
		    "stores.revoke_date is null) and";
	SELECT_TITLE_REGION_LIST	=
		"select rownum no, ctr.title_id, ct.title, " +
			"ctr.region_id, r.region_name, r.bu_id, " +
			"ctr.purchase_start_date, ctr.purchase_end_date, " +
			"ctr.eunits, ctr.last_updated " +
		    "from " +
			TABLE_NAME + " ctr, content_titles ct, regions r " +
		    "where " +
			"ctr.title_id = ct.title_id and " +
			"ctr.region_id = r.region_id";
    }

    /**
     * Constructs a TitleRegionFactory instance.
     */
    protected TitleRegionFactory()
    {
	super();
    }

    /**
     * Returns the region information of the titles identified by the given
     * array of titleIDs, and the regionID.
     *
     * @param	titleIDs		the title identifiers
     * @param	regionID		the region identifier
     * @param	nameMap			the name mapping table
     *
     * @return	The array of TitleRegion JavaBeans, each contains
     *		all information of a Content-Title-Regions record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public Bean[] getTitleRegions(long[] titleIDs, int regionID, Map nameMap)
	throws DBException, IOException
    {
	if (titleIDs == null)
	{
	    return null;
	}

	int		size = titleIDs.length;

	if (size == 0)
	{
	    return null;
	}
	/*
	 * Set up the predicate.
	 */
	RegionID	predicate = new RegionID(regionID);
	TitleID		titleID = new TitleID(titleIDs[0]);
	Predicate	titleGroup = new PredicateTerm(titleID);

	for (int n = 1; n < size; n++)
	{
	    titleID = (TitleID)titleID.or(new TitleID(titleIDs[n]));
	}
	predicate.and(titleGroup);
	/*
	 * Query.
	 */
	return (Bean[])query(predicate, nameMap);
    }

    /**
     * Returns all region information of the titles.
     *
     * @param	titleID			the title identifier
     *
     * @return	The XML document describing the information of regional
     *		title information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getTitleRegions(long titleID)
	throws DBException, IOException
    {
	String	sql = SELECT_TITLE_REGION_LIST +
			" and ctr.title_id = " + titleID +
			" order by r.region_name";
	return query(sql);
    }

    /**
     * Returns all region information of the titles.
     *
     * @param	titleID			the title identifier
     *
     * @return	The XML document describing the information of regional
     *		title information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getTitleRegions(String titleID)
	throws DBException, IOException
    {
	String	sql = SELECT_TITLE_REGION_LIST +
			" and ctr.title_id = " + titleID +
			" order by r.region_name";
	return query(sql);
    }

    /**
     * Returns all region information of the titles.
     *
     * @param	titleID			the title identifier
     *
     * @return	The XML document describing the information of regional
     *		title information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getCountTitleRegions(String titleID)
	throws DBException, IOException
    {
	String	sql = "SELECT count(*) FROM (" + SELECT_TITLE_REGION_LIST +
			" and ctr.title_id = " + titleID + ")";
	return count(sql);
    }

    /**
     * Returns the region information of the titles identified by the given
     * depotID, and the array of titleIDs.
     *
     * @param	depotID			the depot identifier
     * @param	fromTime		the beginning interval (inclusive)
     * @param	toTime			the ending interval (exclusive)
     * @param	nameMap			the name mapping table
     *
     * @return	The array of TitleRegion JavaBeans, each contains
     *		all information of a Content-Title-Regions record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public Bean[] getTitleRegions(long depotID,
				  Date fromTime,
				  Date toTime,
				  Map nameMap)
	throws DBException, IOException
    {
	/*
	 * Set up the predicate.
	 */
	DepotID		predicate = new DepotID(depotID);
	UpdatedFromTime	fromTimePredicate = new UpdatedFromTime(fromTime);

	fromTimePredicate.and(new UpdatedToTime(toTime));
	predicate.and(fromTimePredicate);
	/*
	 * Set up the statement.
	 */
	String			selectStatement = SELECT_WITH_DEPOT_ID;

	selectStatement += fromTimePredicate.generateWherePart(nameMap);
	/*
	 * Query.
	 */
	return (Bean[])query(predicate, selectStatement, nameMap);
    }

    /**
     * Returns the database table name to be used.
     *
     * @return	The data table name.
     */
    public String getTableName()
    {
	return TABLE_NAME;
    }

    /**
     * Creates a TitleRegion JavaBeans object.
     *
     * @return	The newly created TitleRegion object.
     */
    protected Bean newBean()
    {
	return new TitleRegion();
    }
}

package com.broadon.db;

import com.broadon.sql.PredicateItem;

/**
 * The <c>DataLoadRequestID</c> class conforms to the JavaBeans property,
 * and is a Predicate that finds the DataLoadRequest identity by the request
 * identifier.
 *
 * @version	$Revision: 1.1 $
 */
public class DataLoadRequestID
    extends PredicateItem
{
    private Long	requestID;

    /**
     * Constructs a DataLoadRequestID type instance.
     *
     * @param	requestID			the request identifier
     */
    public DataLoadRequestID(long requestID)
    {
	super();
	this.requestID = new Long(requestID);
    }

    /**
     * Getter for requestID.
     *
     * @return	The request identifier.
     */
    public Long getDataLoadRequestID()
    {
	return requestID;
    }

    /**
     * Setter for requestID.
     *
     * @param	requestID			the request identifier
     */
    public void setDataLoadRequestID(Long requestID)
    {
	this.requestID = requestID;
    }
}

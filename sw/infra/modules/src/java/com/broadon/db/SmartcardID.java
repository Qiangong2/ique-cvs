package com.broadon.db;

import com.broadon.sql.PredicateItem;

/**
 * The <c>SmartcardID</c> class conforms to the JavaBeans property,
 * and is a Predicate that finds various records.
 *
 * @version	$Revision: 1.1 $
 */
public class SmartcardID
    extends PredicateItem
{
    private Long	smartcardID;

    /**
     * Constructs a SmartcardID instance.
     *
     * @param	smartcardID		the smartcard identifier
     */
    public SmartcardID(long smartcardID)
    {
	super();
	this.smartcardID = new Long(smartcardID);
    }

    /**
     * Getter for smartcardID.
     *
     * @return	The smartcard identifier.
     */
    public Long getSmartcardID()
    {
	return smartcardID;
    }

    /**
     * Setter for smartcardID.
     *
     * @param	smartcardID		the smartcard identifier
     */
    public void setSmartcardID(Long smartcardID)
    {
	this.smartcardID = smartcardID;
    }
}

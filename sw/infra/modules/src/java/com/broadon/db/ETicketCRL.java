package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <c>ETicketCRL</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *<p>
 * It contains information about a CRL list for eTickets.
 *
 * @version	$Revision: 1.1 $
 */
public class ETicketCRL
    extends PBean
{
    private Integer		crlVersion;
    private String		crl;
    private Date		lastUpdated;

    /**
     * Constructs an empty ETicketCRL instance.
     */
    public ETicketCRL()
    {
	this.crlVersion = null;
	this.crl = null;
	this.lastUpdated = null;
    }

    /**
     * Getter for crlVersion.
     *
     * @return	The CRL version.
     */
    public Integer getCRLVersion()
    {
	return crlVersion;
    }

    /**
     * Setter for crlVersion.
     *
     * @param	crlVersion		the CRL version
     */
    public void setCRLVersion(Integer crlVersion)
    {
	this.crlVersion = crlVersion;
    }

    /**
     * Getter for crl.
     *
     * @return	The Certificate Revocation List.
     */
    public String getCRL()
    {
	return crl;
    }

    /**
     * Setter for crl.
     *
     * @param	crl			the Certificate Revocation List
     */
    public void setCRL(String crl)
    {
	this.crl = crl;
    }

    /**
     * Getter for lastUpdated.
     *
     * @return	The last updated date of this eTicket CRL.
     */
    public Date getLastUpdated()
    {
	return lastUpdated;
    }

    /**
     * Setter for lastUpdated.
     *
     * @param	lastUpdated		the last updated date of this
     *					eTicket CRL
     */
    public void setLastUpdated(Date lastUpdated)
    {
	this.lastUpdated = lastUpdated;
    }
}

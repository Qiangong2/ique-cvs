package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <c>Store</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *<p>
 * It contains information about stores.
 *
 * @version	$Revision: 1.2 $
 */
public class Store
    extends PBean
{
    private Integer		storeID;
    private Integer		regionID;
    private Integer		regionalCenterID;
    private Integer		retailerID;
    private Integer		commisionRate;
    private Integer		accountBalance;
    private Date		createDate;
    private Date		revokeDate;
    private Date		suspendDate;
    private String		timezone;
    private String		address;

    /**
     * Constructs an empty store instance.
     */
    public Store()
    {
    }

    /**
     * Getter for storeID.
     *
     * @return	The store identifier.
     */
    public Integer getStoreID()
    {
	return storeID;
    }

    /**
     * Setter for storeID.
     *
     * @param	storeID		the store identifier
     */
    public void setStoreID(Integer storeID)
    {
	this.storeID = storeID;
    }

    /**
     * Getter for regionID.
     *
     * @return	The region identifier.
     */
    public Integer getRegionID()
    {
	return regionID;
    }

    /**
     * Setter for regionID.
     *
     * @param	regionID		the region identifier
     */
    public void setRegionID(Integer regionID)
    {
	this.regionID = regionID;
    }

    /**
     * Getter for regionalCenterID.
     *
     * @return	The regional center identifier.
     */
    public Integer getRegionalCenterID()
    {
	return regionalCenterID;
    }

    /**
     * Setter for regionalCenterID.
     *
     * @param	regionalCenterID		the regional center identifier
     */
    public void setRegionalCenterID(Integer regionalCenterID)
    {
	this.regionalCenterID = regionalCenterID;
    }

    /**
     * Getter for retailerID.
     *
     * @return	The retailer identifier.
     */
    public Integer getRetailerID()
    {
	return retailerID;
    }

    /**
     * Setter for retailerID.
     *
     * @param	retailerID		the retailer identifier
     */
    public void setRetailerID(Integer retailerID)
    {
	this.retailerID = retailerID;
    }

    /**
     * Getter for commisionRate.
     *
     * @return	The commisionRate identifier.
     */
    public Integer getCommisionRate()
    {
	return commisionRate;
    }

    /**
     * Setter for commisionRate.
     *
     * @param	commisionRate		the commisionRate identifier
     */
    public void setCommisionRate(Integer commisionRate)
    {
	this.commisionRate = commisionRate;
    }

    /**
     * Getter for accountBalance.
     *
     * @return	The accountBalance identifier.
     */
    public Integer getAccountBalance()
    {
	return accountBalance;
    }

    /**
     * Setter for accountBalance.
     *
     * @param	accountBalance		the accountBalance identifier
     */
    public void setAccountBalance(Integer accountBalance)
    {
	this.accountBalance = accountBalance;
    }

    /**
     * Getter for createDate.
     *
     * @return	The creation date of this store.
     */
    public Date getCreateDate()
    {
	return createDate;
    }

    /**
     * Setter for createDate.
     *
     * @param	createDate		the creation date
     */
    public void setCreateDate(Date createDate)
    {
	this.createDate = createDate;
    }

    /**
     * Getter for revokeDate.
     *
     * @return	The revoked date of this store.
     */
    public Date getRevokeDate()
    {
	return revokeDate;
    }

    /**
     * Setter for revokeDate.
     *
     * @param	revokeDate		the revoked date
     */
    public void setRevokeDate(Date revokeDate)
    {
	this.revokeDate = revokeDate;
    }

    /**
     * Getter for suspendDate.
     *
     * @return	The suspended date of this store.
     */
    public Date getSuspendDate()
    {
	return suspendDate;
    }

    /**
     * Setter for suspendDate.
     *
     * @param	suspendDate		the suspended date
     */
    public void setSuspendDate(Date suspendDate)
    {
	this.suspendDate = suspendDate;
    }

    /**
     * Getter for timezone.
     *
     * @return	The timezone.
     */
    public String getTimezone()
    {
	return timezone;
    }

    /**
     * Setter for timezone.
     *
     * @param	timezone		the timezone
     */
    public void setTimezone(String timezone)
    {
	this.timezone = timezone;
    }

    /**
     * Getter for address.
     *
     * @return	The store address.
     */
    public String getAddress()
    {
	return address;
    }

    /**
     * Setter for addresss.
     *
     * @param	address		the store address
     */
    public void setAddress(String address)
    {
	this.address = address;
    }

}

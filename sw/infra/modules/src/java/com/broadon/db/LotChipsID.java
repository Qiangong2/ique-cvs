package com.broadon.db;

import com.broadon.sql.PredicateItem;

/**
 * The <c>LotChipsID</c> class conforms to the JavaBeans property,
 * and is a Predicate that finds LotChips identified by the bb
 * identifier.
 *
 * @version	$Revision: 1.1 $
 */
public class LotChipsID
    extends PredicateItem
{
    private Long	bbID;

    /**
     * Constructs a LotChipsID type instance.
     *
     * @param	bbID		the bb identifier
     */
    public LotChipsID(long bbID)
    {
	super();
	this.bbID = new Long(bbID);
    }

    /**
     * Getter for bbID.
     *
     * @return	The bb identifier.
     */
    public Long getLotChipsID()
    {
	return bbID;
    }

    /**
     * Setter for bbID.
     *
     * @param	bbID		the bb identifier
     */
    public void setLotChipsID(Long bbID)
    {
	this.bbID = bbID;
    }
}

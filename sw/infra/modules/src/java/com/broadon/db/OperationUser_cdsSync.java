package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <c>OperationUser_cdsSync</c> class conforms to the JavaBeans property,
 * and is used by the CDS server sync servlet.
 *<p>
 * It contains partial information about operation users 
 * (unlike OperationUsers which contain all information).
 *
 * @version	$Revision: 1.1 $
 */
public class OperationUser_cdsSync
    extends PBean
{
    private Long		operatorID;
    private String		emailAddress;
    private String		status;
    private Date		statusDate;
    private String		otherInfo;
    private Integer		storeID;
    private Date		lastUpdated;

    /**
     * Constructs an empty OperationUser_cdsSync instance.
     */
    public OperationUser_cdsSync()
    {
    }

    /**
     * Getter for operatorID.
     *
     * @return	The operator identifier.
     */
    public Long getOperatorID()
    {
	return operatorID;
    }

    /**
     * Setter for operatorID.
     *
     * @param	operatorID		the operator identifier
     */
    public void setOperatorID(Long operatorID)
    {
	this.operatorID = operatorID;
    }

    /**
     * Getter for emailAddress.
     *
     * @return	The email address.
     */
    public String getEmailAddress()
    {
	return emailAddress;
    }

    /**
     * Setter for emailAddress.
     *
     * @param	emailAddress		the email address
     */
    public void setEmailAddress(String emailAddress)
    {
	this.emailAddress = emailAddress;
    }

    /**
     * Getter for status.
     *
     * @return	The status.
     */
    public String getStatus()
    {
	return status;
    }

    /**
     * Setter for status.
     *
     * @param	status		the status of the operator
     */
    public void setStatus(String status)
    {
	this.status = status;
    }

    /**
     * Getter for statusDate.
     *
     * @return	The status change date for an operator.
     */
    public Date getStatusDate()
    {
	return statusDate;
    }

    /**
     * Setter for statusDate.
     *
     * @param	statusDate		the status date
     */
    public void setStatusDate(Date statusDate)
    {
	this.statusDate = statusDate;
    }

    /**
     * Getter for other info.
     *
     * @return	The other info.
     */
    public String getOtherInfo()
    {
	return otherInfo;
    }

    /**
     * Setter for other info.
     *
     * @param	otherInfo 	the other info
     */
    public void setOtherInfo(String otherInfo)
    {
	this.otherInfo = otherInfo;
    }

    /**
     * Getter for storeID.
     *
     * @return	The store ID.
     */
    public Integer getStoreID()
    {
	return storeID;
    }

    /**
     * Setter for storeID
     *
     * @param	storeID		the store id
     */
    public void setStoreID(Integer storeID)
    {
	this.storeID = storeID;
    }

    /**
     * Getter for lastUpdated.
     *
     * @return	The last updated date.
     */
    public Date getLastUpdated()
    {
	return lastUpdated;
    }

    /**
     * Setter for lastUpdated.
     *
     * @param	lastUpdated		the last updated date
     */
    public void setLastUpdated(Date lastUpdated)
    {
	this.lastUpdated = lastUpdated;
    }

}

package com.broadon.db;

import java.util.Date;

/**
 * The <c>UpdatedFromTime</c> class conforms to the JavaBeans property,
 * and is a Predicate that finds records that were updated after the
 * given time (inclusive).
 *
 * @version	$Revision: 1.2 $
 */
public class UpdatedFromTime
    extends UpdatedAtTime
{
    /**
     * Constructs a UpdatedFromTime instance.
     *
     * @param	time			the starting time
     */
    public UpdatedFromTime(Date time)
    {
	super(time, ">=");
    }

    /**
     * Constructs a UpdatedFromTime instance.
     *
     * @param	time			the starting time, in milli-seconds
     */
    public UpdatedFromTime(long time)
    {
	super(new Date(time), ">=");
    }
}

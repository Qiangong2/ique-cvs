package com.broadon.db;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * The <code>NameMapManager</code> class retrieves the name mapping table from
 * a configuration file.
 *
 * @version	$Revision: 1.1 $
 */
public class NameMapManager
{
    private Map			nameMap;
    private Map			reverseNameMap;

    /**
     * Constructs the NameMapManager instance from the file identified by
     * fileName.
     *
     * @param	fileName		the file containing the name map
     *
     * @throws	FileNotFoundException
     * @throws	IOException
     */
    public NameMapManager(String fileName)
	throws FileNotFoundException, IOException
    {
	FileInputStream		fis = new FileInputStream(fileName);
	BufferedInputStream	bis = new BufferedInputStream(fis);
	Properties		properties = new Properties();
	Object[]		keys;
	int			size;

	properties.load(bis);
	keys = properties.keySet().toArray();
	size = keys.length;
	this.nameMap = new HashMap(size * 3 / 2);
	this.reverseNameMap = new HashMap(size * 3 / 2);
	for (int n = 0; n < size; n++)
	{
	    this.nameMap.put(keys[n], properties.getProperty((String)keys[n]));
	    this.reverseNameMap.put(properties.getProperty((String)keys[n]),
				    keys[n]);
	}
    }

    /*
     * Returns the name mapping table.
     *
     * @return	The name mapping table.
     */
    public Map getNameMap()
    {
	return nameMap;
    }

    /*
     * Returns the reverse name mapping table.
     *
     * @return	The reverse name mapping table.
     */
    public Map getReverseNameMap()
    {
	return reverseNameMap;
    }
}

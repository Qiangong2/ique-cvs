package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <code>ContentBase</code> abstract class conforms to the JavaBeans
 * property, and is mainly used for data transformation. It is the super
 * class of <code>ContentInfo</code> and <code>Content</code>.
 *<p>
 * It contains information about a content identified by the content
 * identifier, but without the content itself.
 *
 * @version	$Revision: 1.9 $
 */
public abstract class ContentBase
    extends PBean
{
    private Long		contentID;
    private Date		publishDate;
    private Date		revokeDate;
    private Long		replacedContentID;
    private String		contentObjectType;
    private Integer		contentSize;
    private String		contentChecksum;
    private String		contentName;
    private Integer		contentVersion;
    private String		eTicket;
    private Date		lastUpdated;
    private Integer             minUpgradeVersion;
    private Integer             upgradeConsent;

    /**
     * Constructs an empty ContentBase instance.
     */
    public ContentBase()
    {
	this.contentID = null;
	this.publishDate = null;
	this.revokeDate = null;
	this.replacedContentID = null;
	this.contentObjectType = null;
	this.contentSize = null;
	this.contentChecksum = null;
	this.contentName = null;
	this.contentVersion = null;
	this.eTicket = null;
	this.lastUpdated = null;
	this.minUpgradeVersion = null;
	this.upgradeConsent = null;
    }

    /**
     * Getter for contentID.
     *
     * @return	The content identifier.
     */
    public Long getContentID()
    {
	return contentID;
    }

    /**
     * Setter for contentID.
     *
     * @param	contentID		the content identifier
     */
    public void setContentID(Long contentID)
    {
	this.contentID = contentID;
    }

    /**
     * Getter for publishDate.
     *
     * @return	The publish date of this content.
     */
    public Date getPublishDate()
    {
	return publishDate;
    }

    /**
     * Setter for publishDate.
     *
     * @param	publishDate		the publish date of this content
     */
    public void setPublishDate(Date publishDate)
    {
	this.publishDate = publishDate;
    }

    /**
     * Getter for revokeDate.
     *
     * @return	The revoke date of this content.
     */
    public Date getRevokeDate()
    {
	return revokeDate;
    }

    /**
     * Setter for revokeDate.
     *
     * @param	revokeDate		the revoke date of this content
     */
    public void setRevokeDate(Date revokeDate)
    {
	this.revokeDate = revokeDate;
    }

    /**
     * Getter for replacedContentID.
     *
     * @return	The replaced content identifier.
     */
    public Long getReplacedContentID()
    {
	return replacedContentID;
    }

    /**
     * Setter for replacedContentID.
     *
     * @param	replacedContentID	the replaced content identifier
     */
    public void setReplacedContentID(Long replacedContentID)
    {
	this.replacedContentID = replacedContentID;
    }

    /**
     * Getter for contentObjectType.
     *
     * @return	The content type of this content.
     */
    public String getContentObjectType()
    {
	return contentObjectType;
    }

    /**
     * Setter for contentObjectType.
     *
     * @param	contentObjectType	the content type of this content
     */
    public void setContentObjectType(String contentObjectType)
    {
	this.contentObjectType = contentObjectType;
    }

    /**
     * Getter for contentSize.
     *
     * @return	The content size.
     */
    public Integer getContentSize()
    {
	return contentSize;
    }

    /**
     * Setter for contentSize.
     *
     * @param	contentSize		the content size
     */
    public void setContentSize(Integer contentSize)
    {
	this.contentSize = contentSize;
    }

    /**
     * Getter for contentChecksum.
     *
     * @return	The content checksum.
     */
    public String getContentChecksum()
    {
	return contentChecksum;
    }

    /**
     * Setter for contentChecksum.
     *
     * @param	contentChecksum		the content checksum
     */
    public void setContentChecksum(String contentChecksum)
    {
	this.contentChecksum = contentChecksum;
    }

    /**
     * Getter for contentName.
     *
     * @return	The content name.
     */
    public String getContentName()
    {
	return contentName;
    }

    /**
     * Setter for contentName.
     *
     * @param	contentName		the content name
     */
    public void setContentName(String contentName)
    {
	this.contentName = contentName;
    }

    /**
     * Getter for contentVersion.
     *
     * @return	The content version.
     */
    public Integer getContentVersion()
    {
	return contentVersion;
    }

    /**
     * Setter for contentVersion.
     *
     * @param	contentVersion		the content version
     */
    public void setContentVersion(Integer contentVersion)
    {
	this.contentVersion = contentVersion;
    }

    /**
     * Getter for eTicket.
     *
     * @return	The eTicket common information.
     */
    public String getETicket()
    {
	return eTicket;
    }

    /**
     * Setter for eTicket.
     *
     * @param	eTicket			the eTicket common information
     */
    public void setETicket(String eTicket)
    {
	this.eTicket = eTicket;
    }

    /**
     * Getter for lastUpdated.
     *
     * @return	The last updated date of this content.
     */
    public Date getLastUpdated()
    {
	return lastUpdated;
    }

    /**
     * Setter for lastUpdated.
     *
     * @param	lastUpdated		the last updated date of this content
     */
    public void setLastUpdated(Date lastUpdated)
    {
	this.lastUpdated = lastUpdated;
    }

    /**
     * Getter for minUpgradeVersion
     *
     * @return	The last updated date of this content.
     */
    public Integer getMinUpgradeVersion()
    {
	return minUpgradeVersion;
    }

    /**
     * Setter for minUpgradeVersion
     *
     * @param	minUpgradeVersion		the last updated date of this content
     */
    public void setMinUpgradeVersion(Integer minUpgradeVersion)
    {
	this.minUpgradeVersion = minUpgradeVersion;
    }

    /**
     * Getter for upgradeConsent
     *
     * @return	The last updated date of this content.
     */
    public Integer getUpgradeConsent()
    {
	return upgradeConsent;
    }

    /**
     * Setter for upgradeConsent
     *
     * @param	upgradeConsent		the last updated date of this content
     */
    public void setUpgradeConsent(Integer upgradeConsent)
    {
	this.upgradeConsent = upgradeConsent;
    }
}

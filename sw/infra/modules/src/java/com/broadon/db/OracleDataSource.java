package com.broadon.db;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleConnectionWrapper;
import oracle.jdbc.pool.OracleConnectionCacheManager;

/**
 * This <code>OracleDataSource</code> class isolates the Oracle dependencies.
 */
public class OracleDataSource
{
    /**
     * Creates the data source with the given user name and password.
     *
     * @param	user			the user name for the data source
     * @param	password		the password for the data source
     * @param	url			the database target
     * @param	cacheName		identify the cache
     */
    public static DataSource createDataSource(String user,
					      String password,
					      String url,
					      String cacheName)
	throws SQLException
    {
	return createDataSource(user, password, url, cacheName, false);
    }

    /**
     * Creates the data source with the given user name and password.
     *
     * @param	user			the user name for the data source
     * @param	password		the password for the data source
     * @param	url			the database target
     * @param	cacheName		identify the cache
     * @param	noScheduler		true to disable DataSourceScheduler
     */
    public static DataSource createDataSource(String user,
					      String password,
					      String url,
					      String cacheName,
					      boolean noScheduler)
	throws SQLException
    {
	int maxConnection = 8;
	try {
	    String maxConn = System.getProperty("svcdrv.db.maxConnection");
	    maxConnection = Integer.parseInt(maxConn);
	} catch (Exception e) {}

	return createDataSource(user, password, url, cacheName, maxConnection, noScheduler);
    }

    /**
     * Creates the data source with the given user name and password.
     *
     * @param	user			the user name for the data source
     * @param	password		the password for the data source
     * @param	url			the database target
     * @param	cacheName		identify the cache     
     * @param	maxConnections		the maximum number of connections
     * @param	noScheduler		true to disable DataSourceScheduler
     */
    public static DataSource createDataSource(String user,
					      String password,
					      String url,
					      String cacheName,
					      int maxConnections,
					      boolean noScheduler)
	throws SQLException
    {
	return new OracleDataSourceProxy(user,
					 password,
					 url,
					 cacheName,
					 maxConnections,
					 noScheduler);
    }

    /**
     * Wraps around the Oracle's data source to handle reconnect.
     */
    public static class OracleDataSourceProxy
	implements DataSource
    {
        private String cacheName = "ConnectionCache_0";

        private String      user;
        private String      password;
        private String      url;
        private int         maxConnections;
        private boolean     noScheduler;
        private DataSource  dataSource;
	private DataSourceScheduler scheduler;
        private static int TIMEOUT = 60000;

	/**
	 * Constructs a new OracleDataSourceProxy instance.
	 *
	 * @param	user		the user name for the data source
	 * @param	password	the password for the data source
	 * @param	url		the database target
	 * @param	cacheName		identify the cache
	 * @param	maxConnections	the maximum number of connections
	 * @param	noScheduler     true to disable DataSourceScheduler
	 */
	public OracleDataSourceProxy(String user,
				     String password,
				     String url,
				     String cacheName,
				     int maxConnections,
				     boolean noScheduler)
	    throws SQLException
	{
	    this.user = user;
	    this.password = password;
	    this.url = url;
	    this.cacheName = cacheName;
	    this.maxConnections = maxConnections;
	    this.noScheduler = noScheduler;
	    this.dataSource = createDataSource(user,
					       password,
					       url,
					       maxConnections);
            if (noScheduler) { 
                this.scheduler = null;
            } else {
                this.scheduler = new DataSourceScheduler(DataSourceScheduler.DB_PRIORITY_MAX,
                                                         1, maxConnections, TIMEOUT);
            }
	}
	
	/**
	 * Creates a new DataSource.
	 *
	 * @param	user		the user name for the data source
	 * @param	password	the password for the data source
	 * @param	url		the database target
	 * @param	maxConnections	the maximum number of connections
	 * @param	noScheduler     true to disable DataSourceScheduler
	 */
	private DataSource createDataSource(String user,
					    String password,
					    String url,
					    int maxConnections)
	    throws SQLException
	{
	    // Define DataSource properties
	    //
	    java.util.Properties prop = new java.util.Properties();
	    prop.setProperty("MinLimit", "1");
	    prop.setProperty("MaxLimit", 
			     Integer.toString(maxConnections));
            prop.setProperty("ValidateConnection", "true");
            prop.setProperty("AbandonedConnectionTimeout", "900");
	     
	    // Create the DataSource
	    //
	    oracle.jdbc.pool.OracleDataSource ds =
		new oracle.jdbc.pool.OracleDataSource();
	    ds.setURL(url);
	    ds.setUser(user);
	    ds.setPassword(password);
	    ds.setConnectionCachingEnabled(true);
	    ds.setConnectionCacheProperties(prop);
	    ds.setConnectionCacheName(cacheName);
	    
	    // Create the connection cache and associate it with the 
	    // connection cache manager
	    //
	    OracleConnectionCacheManager occm = 
		OracleConnectionCacheManager.getConnectionCacheManagerInstance();
	    occm.createCache(cacheName, ds, prop);
	    
	    return ds;
	}

	/**
	 * Recreate the data source and therefore the connection pool
	 * before trying to get a connection again. It also wait a little
	 * while before retrying.
	 *
	 * @param	user		the user name for the connection
	 * @param	password	the password for the connection
	 */
	private Connection getConnectionWithRetry(String user, String password)
	    throws SQLException
	{
	    Connection ret;
	    int[] sleepTime = {1000, 0};
	    
	    while (true) {
		try {
		    ret = dataSource.getConnection(user, password);
                    if (ret == null) {
                        throw new SQLException();
                    }
                    return ret;
		} catch (SQLException se) {
		    sleepTime[1] = sleepTime[0] + sleepTime[1];
		    if (sleepTime[1] > TIMEOUT) {
			// We give up after the timeout exceeded
			return null;
		    }

                    try {
                        Thread.sleep(sleepTime[0]);
                    } catch (InterruptedException ie) {}
		}
	    }
	}

	/*
	 * The followings are the DataSource interface methods.
	 */

	public Connection getConnection()
	    throws SQLException
	{
	    return getConnection(scheduler.DB_PRIORITY_DEFAULT);
	}


	public Connection getConnection(int priority)
	    throws SQLException
	{
            if (scheduler != null) {
                try {
                    scheduler.getTurn(priority);
                } catch (InterruptedException e) {
                    throw new SQLException(e.getMessage());
                }
            }
	    
	    return new ConnectionWrapper((OracleConnection) getConnection(this.user, this.password),
					 scheduler, priority);
	}


	public Connection getConnection(String user, String password)
	    throws SQLException
	{
            Connection ret;
            ret = getConnectionWithRetry(user, password);
            if (ret == null) {
                throw new SQLException("dataSource.getConnection() failed");
            }
            return ret;
	}

	public int getLoginTimeout()
	    throws SQLException
	{
	    return dataSource.getLoginTimeout();
	}

	public void setLoginTimeout(int seconds)
	    throws SQLException
	{
	    dataSource.setLoginTimeout(seconds);
	}

	public PrintWriter getLogWriter()
	    throws SQLException
	{
	    return dataSource.getLogWriter();
	}

	public void setLogWriter(PrintWriter out)
	    throws SQLException
	{
	    dataSource.setLogWriter(out);
	}
    }


    /*
     * Wrap around the Connection object, where the only purpose is to
     * capture the "close" method so that the scheduler is notified.
     * This should have easily been done via a "Listener" or
     * "Callback" class, but none is available (*sigh*!).  (Note:
     * There is a OracleConnectionCacheCallback mechanism, but that
     * does not work on logical connection.  There is also a
     * "CloseCallback" mechanism, but that is not public and thus not
     * available).
     */
    public static class ConnectionWrapper extends OracleConnectionWrapper
    {
	private DataSourceScheduler scheduler;
	private final int priority;
	private boolean closed;

	public ConnectionWrapper(OracleConnection conn,
				 DataSourceScheduler scheduler,
				 int priority)
	{
	    super(conn);
	    this.scheduler = scheduler;
	    this.priority = priority;
	    this.closed = false;
	}

	public void close() throws SQLException {
	    if (! closed) {
                if (scheduler != null) {
                    scheduler.yieldTurn(priority);
                }
		closed = true;
	    }
	    super.close();
	}
    }
}

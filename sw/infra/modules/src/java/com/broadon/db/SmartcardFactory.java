package com.broadon.db;

import java.io.IOException;

import com.broadon.bean.Bean;

/**
 * The <code>SmartcardFactory</code> class interfaces with the database
 * to update or query records from the smartcards table.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.3 $
 */
public class SmartcardFactory
    extends DBAccessFactory
{
    private static final String TABLE_NAME;
    private static final String	SELECT_SMARTCARDS_LIST;

    static
    {
	TABLE_NAME		= "SMARTCARDS";
	SELECT_SMARTCARDS_LIST	=
	    "select smartcard_id, to_char(sys_extract_utc(current_timestamp), 'YYYY.MM.DD HH24:MI:SS') revoke_date from " + TABLE_NAME;
    }

    /**
     * Constructs a SmartcardFactory instance.
     */
    protected SmartcardFactory()
    {
	super();
    }

   /**
     * Returns the smartcard details with the given id.
     *
     * @param   ownerID                 the operator identifier
     *
     * @return The XML document describing the information of smartcard
     *          information.
     *
     * @throws  DBException
     * @throws  IOException
     */
    public String getOwnerSmartcards(String ownerID)
        throws DBException, IOException
    {
        String  sql = SELECT_SMARTCARDS_LIST + " where revoke_date is null and owner_id = " + ownerID;
        return query(sql);
    }

    /**
     * Returns the database table name to be used.
     *
     * @return	The data table name.
     */
    public String getTableName()
    {
	return TABLE_NAME;
    }

    /**
     * Creates a Smartcard JavaBeans object.
     *
     * @return The newly created Smartcard object.
     */
    protected Bean newBean()
    {
        return new Smartcard();
    }

}

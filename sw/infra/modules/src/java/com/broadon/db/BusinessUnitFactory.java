package com.broadon.db;

import java.io.IOException;
import java.util.Map;

import com.broadon.bean.Bean;

/**
 * The <c>BusinessUnitFactory</c> class interfaces with the database to
 * create, update, or query records from the BusinessUnits table.
 * Each record is represented by a BusinessUnit JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.4 $
 */
public class BusinessUnitFactory
    extends DBAccessFactory
{
    private static final String     TABLE_NAME;
    private static final String	SELECT_BU_NAMES;
    private static final String	SELECT_BU_LIST;
    private static final String	SELECT_BU_COUNT;

    static
    { 
	TABLE_NAME		= "BUSINESS_UNITS";
	SELECT_BU_NAMES	=
	    "select rownum no, bu_id, business_name from " + TABLE_NAME +
		" order by business_name";
	SELECT_BU_LIST	=
	    "select bu_id, business_name, create_date, contact_info from " + TABLE_NAME;
	SELECT_BU_COUNT	= "select count(bu_id) from " + TABLE_NAME;
    }

   /**
     * Constructs a BusinessUnitFactory instance.
     */
    protected BusinessUnitFactory()
    {
	super();
    }

    /**
     * Returns the business unit information, based on the given businessUnitID.
     *
     * @param	businessUnitID		the business unit identifier
     * @param	nameMap			the name mapping table
     *
     * @return	The BusinessUnit JavaBeans containing all information of
     *		a BusinessUnit record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public BusinessUnit getBusinessUnit(int businessUnitID, Map nameMap)
	throws DBException, IOException
    {
	BusinessUnitID	predicate = new BusinessUnitID(businessUnitID);

	return (BusinessUnit)queryUnique(predicate, nameMap);
    }

   /**
     * Returns the business unit information of a given range.
     *
     * @param	from			the starting record number
     * @param	to			the ending record number
     * @param	order			the sorted order
     *
     * @return	The XML document that contains the business unit information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getBusinessUnits(int from, int to, String order)
	throws DBException, IOException
    {
	String	sql = "select * from (select rownum no, inner.* from (" +
			SELECT_BU_LIST +
			    " order by " + order + ") " +
			"inner where rownum <= " + to + ") " +
			"where no > " + from;

	return query(sql);
    }

    /**
     * Returns the business unit identifier and business unit names.
     *
     * @return	The XML document that contains information of business unit records.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getBusinessUnitNames()
	throws DBException, IOException
    {
	return query(SELECT_BU_NAMES);
    }

   /**
     * Returns the business unit details with the given id.
     *
     * @param   buID                 the business unit identifier
     *
     * @return The XML document describing the information of business unit
     *          information.
     *
     * @throws  DBException
     * @throws  IOException
     */
    public String getBusinessUnit(String buID)
        throws DBException, IOException
    {
        String  sql = SELECT_BU_LIST +
                        " where bu_id = " + buID;
        return query(sql);
    }

    /**
     * Returns the number of business unit available.
     *
     * @param	buID		the business unit identifier
     *
     * @return	The integer that contains the number of business units.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getBusinessUnitCount(long buID)
	throws DBException, IOException
    {
	return count(SELECT_BU_COUNT + " where bu_id = " + buID);
    }

    /**
     * Returns the number of business units available.
     *
     * @param	buID		the region identifier
     *
     * @return	The integer that contains the number of business units.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getBusinessUnitCount(String buID)
	throws DBException, IOException
    {
	return count(SELECT_BU_COUNT + " where bu_id = " + buID);
    }

    /**
     * Returns the number of business units available.
     *
     * @return	The integer document that contains the number of business units.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getBusinessUnitCount()
	throws DBException, IOException
    {
	return count(SELECT_BU_COUNT);
    }

    /**
     * Returns the database table name to be used.
     *
     * @return	The data table name.
     */
    public String getTableName()
    {
	return TABLE_NAME;
    }

    /**
     * Creates a BusinessUnit JavaBeans object.
     *
     * @return	The newly created BusinessUnit object.
     */
    protected Bean newBean()
    {
	return new BusinessUnit();
    }
}

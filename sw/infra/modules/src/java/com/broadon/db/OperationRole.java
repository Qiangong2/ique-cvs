package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <c>OperationRole</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *<p>
 * It contains information about operation roles.
 *
 * @version	$Revision: 1.2 $
 */
public class OperationRole
    extends PBean
{
    private Integer		roleLevel;
    private String		roleName;
    private String		description;
    private Integer		isActive;
    private Date		createDate;

    /**
     * Constructs an empty OperationRole instance.
     */
    public OperationRole()
    {
    }

    /**
     * Getter for roleLevel.
     *
     * @return	The role identifier.
     */
    public Integer getRoleLevel()
    {
	return roleLevel;
    }

    /**
     * Setter for roleLevel.
     *
     * @param	roleLevel		the role identifier
     */
    public void setRoleLevel(Integer roleLevel)
    {
	this.roleLevel = roleLevel;
    }

    /**
     * Getter for roleName.
     *
     * @return	The role name.
     */
    public String getRoleName()
    {
	return roleName;
    }

    /**
     * Setter for roleName.
     *
     * @param	roleName		the role name
     */
    public void setRoleName(String roleName)
    {
	this.roleName = roleName;
    }

    /**
     * Getter for description.
     *
     * @return	The description of the role.
     */
    public String getDescription()
    {
	return description;
    }

    /**
     * Setter for description.
     *
     * @param	description		the description of the role
     */
    public void setDescription(String description)
    {
	this.description = description;
    }

    /**
     * Getter for isActive.
     *
     * @return	The is active flag.
     */
    public Integer getIsActive()
    {
	return isActive;
    }

    /**
     * Setter for isActive.
     *
     * @param	isActive		the is active flag
     */
    public void setIsActive(Integer isActive)
    {
	this.isActive = isActive;
    }

    /**
     * Getter for createDate.
     *
     * @return	The create date of this role.
     */
    public Date getCreateDate()
    {
	return createDate;
    }

    /**
     * Setter for createDate.
     *
     * @param	createDate		the create date
     */
    public void setCreateDate(Date createDate)
    {
	this.createDate = createDate;
    }
}

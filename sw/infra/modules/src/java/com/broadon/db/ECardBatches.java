package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <c>ECardBatches</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *<p>
 * It contains information about an eCard batch.
 *
 * @version	$Revision: 1.2 $
 */
public class ECardBatches
    extends PBean
{
    private Integer		endECardID;
    private Integer		startECardID;
    private String		certificateID;
    private Date		createDate;
    private Integer		ECardTypeID;
    private Date		printDate;
    private Date		shipDate;
    private Date		uploadDate;
    private Integer		businessUnitID;
    private Date		expireDate;
    private Date		revokeDate;
    private Date		activateDate;

    /**
     * Constructs an empty ECards instance.
     */
    public ECardBatches()
    {
	this.endECardID = null;
	this.startECardID = null;
	this.certificateID = null;
	this.createDate = null;
	this.ECardTypeID = null;
	this.printDate = null;
	this.shipDate = null;
	this.uploadDate = null;
	this.businessUnitID = null;
	this.expireDate = null;
	this.revokeDate = null;
	this.activateDate = null;
    }

    /**
     * Getter for endECardID.
     *
     * @return	The endECard ID identifier.
     */
    public Integer getEndECardID()
    {
	return endECardID;
    }

    /**
     * Setter for endECardID.
     *
     * @param	endECardID		the endECard ID identifier
     */
    public void setEndECardID(Integer endECardID)
    {
	this.endECardID = endECardID;
    }

    /**
     * Getter for startECardID.
     *
     * @return	The startECard ID identifier.
     */
    public Integer getStartECardID()
    {
	return startECardID;
    }

    /**
     * Setter for startECardID.
     *
     * @param	startECardID		the startECard ID identifier
     */
    public void setStartECardID(Integer startECardID)
    {
	this.startECardID = startECardID;
    }

    /**
     * Getter for certificateID.
     *
     * @return	The certificate ID identifier.
     */
    public String getCertificateID()
    {
	return certificateID;
    }

    /**
     * Setter for certificateID.
     *
     * @param	certificateID		the certificate ID identifier
     */
    public void setCertificateID(String certificateID)
    {
	this.certificateID = certificateID;
    }

    /**
     * Getter for createDate.
     *
     * @return	The create date of this eCard batch
     */
    public Date getCreateDate()
    {
	return createDate;
    }

    /**
     * Setter for createDate.
     *
     * @param	createDate	the create date of this ecard batch
     */
    public void setCreateDate(Date createDate)
    {
	this.createDate = createDate;
    }

    /**
     * Getter for ECardTypeID.
     *
     * @return	The eCard Type identifier.
     */
    public Integer getECardTypeID()
    {
	return ECardTypeID;
    }

    /**
     * Setter for ECardTypeID.
     *
     * @param	ECardTypeID		the eCard type identifier
     */
    public void setECardTypeID(Integer ECardTypeID)
    {
	this.ECardTypeID = ECardTypeID;
    }

    /**
     * Getter for printDate.
     *
     * @return	The print date of this eCard batch
     */
    public Date getPrintDate()
    {
	return printDate;
    }

    /**
     * Setter for printDate.
     *
     * @param	printDa		the print date of this ecard batch
     */
    public void setPrintDate(Date printDate)
    {
	this.printDate = printDate;
    }

    /**
     * Getter for shipDate.
     *
     * @return	The ship date of this eCard batch
     */
    public Date getShipDate()
    {
	return shipDate;
    }

    /**
     * Setter for shipDate.
     *
     * @param	shipDate	the ship date of this ecard batch
     */
    public void setShipDate(Date shipDate)
    {
	this.shipDate = shipDate;
    }

    /**
     * Getter for uploadDate.
     *
     * @return	The upload date of this eCard batch
     */
    public Date getUploadDate()
    {
	return uploadDate;
    }

    /**
     * Setter for uploadDate.
     *
     * @param	uploadDate	the upload date of this ecard batch
     */
    public void setUploadDate(Date uploadDate)
    {
	this.uploadDate = uploadDate;
    }

    /**
     * Getter for businessUnitID.
     *
     * @return	The bu identifier.
     */
    public Integer getBusinessUnitID()
    {
	return businessUnitID;
    }

    /**
     * Setter for businessUnitID.
     *
     * @param	businessUnitID		the bu identifier
     */
    public void setBusinessUnitID(Integer businessUnitID)
    {
	this.businessUnitID = businessUnitID;
    }

    /**
     * Getter for activateDate.
     *
     * @return	The activate date of this eCard
     */
    public Date getActivateDate()
    {
	return activateDate;
    }

    /**
     * Setter for activateDate.
     *
     * @param	activateDate		the activate date of this
     *					eCard
     */
    public void setActivateDate(Date activateDate)
    {
	this.activateDate = activateDate;
    }

    /**
     * Getter for expireDate.
     *
     * @return	The expire date of this eCard batch
     */
    public Date getExpireDate()
    {
	return expireDate;
    }

    /**
     * Setter for expireDate.
     *
     * @param	expireDate	the expire date of this ecard batch
     */
    public void setExpireDate(Date expireDate)
    {
	this.expireDate = expireDate;
    }

    /**
     * Getter for revokeDate.
     *
     * @return	The revoke date of this eCard
     */
    public Date getRevokeDate()
    {
	return revokeDate;
    }

    /**
     * Setter for revokeDate.
     *
     * @param	revokeDate		the revoke date of this
     *					eCard
     */
    public void setRevokeDate(Date revokeDate)
    {
	this.revokeDate = revokeDate;
    }
}

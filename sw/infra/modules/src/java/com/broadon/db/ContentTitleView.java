package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <code>ContentTitleView</code> abstract class conforms to the JavaBeans
 * property, and is mainly used for data transformation. It is the super
 * class of <code>ContentInfo</code> and <code>Content</code>.
 *<p>
 * It contains information about a content identified by the content
 * identifier, but without the content itself.
 *
 * @version	$Revision: 1.3 $
 */
public class ContentTitleView
    extends PBean
{
    private Long		contentID;
    private Long		titleID;
    private Date		publishDate;
    private Date		revokeDate;
    private Date		ctoRevokeDate;
    private Date		ctoCreateDate;
    private Long		replacedContentID;
    private String		contentObjectType;
    private Integer		contentSize;
    private String		hasContentObjectRevoked;
    private String		hasETicketMetadata;
    private String		isContentObjectLatest;
    private String		contentName;
    private Integer		contentVersion;
    private Date		lastUpdated;

    /**
     * Constructs an empty ContentTitleView instance.
     */
    public ContentTitleView()
    {
    }

   /**
     * Getter for contentID.
     *
     * @return	The content identifier.
     */
    public Long getContentID()
    {
	return contentID;
    }

    /**
     * Setter for contentID.
     *
     * @param	contentID		the content identifier
     */
    public void setContentID(Long contentID)
    {
	this.contentID = contentID;
    }

    /**
     * Getter for titleID.
     *
     * @return	The title identifier.
     */
    public Long getTitleID()
    {
	return titleID;
    }

    /**
     * Setter for titleID.
     *
     * @param	titleID		the title identifier
     */
    public void setTitleID(Long titleID)
    {
	this.titleID = titleID;
    }

    /**
     * Getter for publishDate.
     *
     * @return	The publish date of this content.
     */
    public Date getPublishDate()
    {
	return publishDate;
    }

    /**
     * Setter for publishDate.
     *
     * @param	publishDate		the publish date of this content
     */
    public void setPublishDate(Date publishDate)
    {
	this.publishDate = publishDate;
    }

    /**
     * Getter for revokeDate.
     *
     * @return	The revoke date of this content.
     */
    public Date getRevokeDate()
    {
	return revokeDate;
    }

    /**
     * Setter for revokeDate.
     *
     * @param	revokeDate		the revoke date of this content
     */
    public void setRevokeDate(Date revokeDate)
    {
	this.revokeDate = revokeDate;
    }

    /**
     * Getter for ctoRevokeDate.
     *
     * @return	The ctoRevoke date
     */
    public Date getCTORevokeDate()
    {
	return ctoRevokeDate;
    }

    /**
     * Setter for ctoRevokeDate.
     *
     * @param	ctoRevokeDate		the ctoRevoke date
     */
    public void setCTORevokeDate(Date ctoRevokeDate)
    {
	this.ctoRevokeDate = ctoRevokeDate;
    }

    /**
     * Getter for ctoCreateDate.
     *
     * @return	The ctoCreate date
     */
    public Date getCTOCreateDate()
    {
	return ctoCreateDate;
    }

    /**
     * Setter for ctoCreateDate.
     *
     * @param	ctoCreateDate		the ctoCreate date
     */
    public void setCTOCreateDate(Date ctoCreateDate)
    {
	this.ctoCreateDate = ctoCreateDate;
    }

    /**
     * Getter for replacedContentID.
     *
     * @return	The replaced content identifier.
     */
    public Long getReplacedContentID()
    {
	return replacedContentID;
    }

    /**
     * Setter for replacedContentID.
     *
     * @param	replacedContentID	the replaced content identifier
     */
    public void setReplacedContentID(Long replacedContentID)
    {
	this.replacedContentID = replacedContentID;
    }

    /**
     * Getter for contentObjectType.
     *
     * @return	The content type of this content.
     */
    public String getContentObjectType()
    {
	return contentObjectType;
    }

    /**
     * Setter for contentObjectType.
     *
     * @param	contentObjectType	the content type of this content
     */
    public void setContentObjectType(String contentObjectType)
    {
	this.contentObjectType = contentObjectType;
    }

    /**
     * Getter for contentSize.
     *
     * @return	The content size.
     */
    public Integer getContentSize()
    {
	return contentSize;
    }

    /**
     * Setter for contentSize.
     *
     * @param	contentSize		the content size
     */
    public void setContentSize(Integer contentSize)
    {
	this.contentSize = contentSize;
    }

    /**
     * Getter for hasETicketMetadata.
     *
     * @return	The hasETicketMetadata flag.
     */
    public String getHasETicketMetadata()
    {
	return hasETicketMetadata;
    }

    /**
     * Setter for hasETicketMetadata.
     *
     * @param hasETicketMetadata	the hasETicketMetadata flag
     */
    public void setHasETicketMetadata(String hasETicketMetadata)
    {
	this.hasETicketMetadata = hasETicketMetadata;
    }

    /**
     * Getter for hasContentObjectRevoked.
     *
     * @return	The hasContentObjectRevoked flag.
     */
    public String getHasContentObjectRevoked()
    {
	return hasContentObjectRevoked;
    }

    /**
     * Setter for hasContentObjectRevoked.
     *
     * @param hasContentObjectRevoked	the hasContentObjectRevoked flag
     */
    public void setHasContentObjectRevoked(String hasContentObjectRevoked)
    {
	this.hasContentObjectRevoked = hasContentObjectRevoked;
    }

    /**
     * Getter for isContentObjectLatest.
     *
     * @return	The isContentObjectLatest flag
     */
    public String getIsContentObjectLatest()
    {
	return isContentObjectLatest;
    }

    /**
     * Setter for isContentObjectLatest.
     *
     * @param isContentObjectLatest	the isContentObjectLatest flag
     */
    public void setIsContentObjectLatest(String isContentObjectLatest)
    {
	this.isContentObjectLatest = isContentObjectLatest;
    }

    /**
     * Getter for contentName.
     *
     * @return	The content name.
     */
    public String getContentName()
    {
	return contentName;
    }

    /**
     * Setter for contentName.
     *
     * @param	contentName		the content name
     */
    public void setContentName(String contentName)
    {
	this.contentName = contentName;
    }

    /**
     * Getter for contentVersion.
     *
     * @return	The content version.
     */
    public Integer getContentVersion()
    {
	return contentVersion;
    }

    /**
     * Setter for contentVersion.
     *
     * @param	contentVersion		the content version
     */
    public void setContentVersion(Integer contentVersion)
    {
	this.contentVersion = contentVersion;
    }

    /**
     * Getter for lastUpdated.
     *
     * @return	The last updated date of this content.
     */
    public Date getLastUpdated()
    {
	return lastUpdated;
    }

    /**
     * Setter for lastUpdated.
     *
     * @param	lastUpdated		the last updated date of this content
     */
    public void setLastUpdated(Date lastUpdated)
    {
	this.lastUpdated = lastUpdated;
    }
}

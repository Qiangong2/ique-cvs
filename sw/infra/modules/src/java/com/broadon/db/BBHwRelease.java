package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <c>BBHwRelease</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *<p>
 * It contains information about a BB Hardware release.
 *
 * @version	$Revision: 1.4 $
 */
public class BBHwRelease
    extends PBean
{
    private Integer		bbHwRevision;
    private String		bbModel;
    private Integer		chipRevision;
    private Integer		secureContentID;
    private Integer		bootContentID;
    private Integer		diagContentID;
    private Date		lastUpdated;
    private String		description;

    /**
     * Constructs an empty BBHwRelease instance.
     */
    public BBHwRelease()
    {
	this.bbHwRevision = null;
	this.bbModel = null;
	this.chipRevision = null;
	this.secureContentID = null;
	this.lastUpdated = null;
	this.description = null;
    }

    /**
     * Getter for bbHwRevision.
     *
     * @return	The BB hardware revision.
     */
    public Integer getBBHwRevision()
    {
	return bbHwRevision;
    }

    /**
     * Setter for bbHwRevision.
     *
     * @param	bbHwRevision		the BB hardware revision
     */
    public void setBBHwRevision(Integer bbHwRevision)
    {
	this.bbHwRevision = bbHwRevision;
    }

    /**
     * Getter for bbModel.
     *
     * @return	The BB model.
     */
    public String getBBModel()
    {
	return bbModel;
    }

    /**
     * Setter for bbModel.
     *
     * @param	bbModel			the BB model
     */
    public void setBBModel(String bbModel)
    {
	this.bbModel = bbModel;
    }

    /**
     * Getter for chipRevision.
     *
     * @return	The chip revision.
     */
    public Integer getChipRevision()
    {
	return chipRevision;
    }

    /**
     * Setter for chipRevision.
     *
     * @param	chipRevision		the chip revision
     */
    public void setChipRevision(Integer chipRevision)
    {
	this.chipRevision = chipRevision;
    }

    /**
     * Getter for secureContentID.
     *
     * @return	The secure content identifier.
     */
    public Integer getSecureContentID()
    {
	return secureContentID;
    }

    /**
     * Setter for secureContentID.
     *
     * @param	secureContentID		the secure content identifier
     */
    public void setSecureContentID(Integer secureContentID)
    {
	this.secureContentID = secureContentID;
    }

    /**
     * Getter for bootContentID.
     *
     * @return	The boot content identifier.
     */
    public Integer getBootContentID()
    {
	return bootContentID;
    }

    /**
     * Setter for bootContentID.
     *
     * @param	bootContentID		the boot content identifier
     */
    public void setBootContentID(Integer bootContentID)
    {
	this.bootContentID = bootContentID;
    }

    /**
     * Getter for diagContentID.
     *
     * @return	The diagnostic content identifier.
     */
    public Integer getDiagContentID()
    {
	return diagContentID;
    }

    /**
     * Setter for diagContentID.
     *
     * @param	diagContentID		the diagnostic content identifier
     */
    public void setDiagContentID(Integer diagContentID)
    {
	this.diagContentID = diagContentID;
    }

    /**
     * Getter for lastUpdated.
     *
     * @return	The last updated date of this BB hardware release.
     */
    public Date getLastUpdated()
    {
	return lastUpdated;
    }

    /**
     * Setter for lastUpdated.
     *
     * @param	lastUpdated		the last updated date of this
     *					BB hardware release
     */
    public void setLastUpdated(Date lastUpdated)
    {
	this.lastUpdated = lastUpdated;
    }

    /**
     * Getter for description.
     *
     * @return	The Description.
     */
    public String getDescription()
    {
	return description;
    }

    /**
     * Setter for description.
     *
     * @param	description		the Description
     */
    public void setDescription(String description)
    {
	this.description = description;
    }

}

package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <c>DataLoadRequest</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *<p>
 * It contains information about upload requests identified by the request identifier.
 *
 * @version	$Revision: 1.1 $
 */
public class DataLoadRequest
    extends PBean
{
    private Long		requestID;
    private Date		requestDate;
    private Long		operatorID;
    private Long		roleLevel;
    private String		jobName;
    private Date		validateDate;
    private Date		processDate;
    private String		processStatus;
    private String		processXml;

    /**
     * Constructs an empty DataLoadRequest instance.
     */
    public DataLoadRequest()
    {
    }

    /**
     * Getter for requestID.
     *
     * @return	The request identifier.
     */
    public Long getRequestID()
    {
	return requestID;
    }

    /**
     * Setter for requestID.
     *
     * @param	requestID			the request identifier
     */
    public void setRequestID(Long requestID)
    {
	this.requestID = requestID;
    }

    /**
     * Getter for requestDate.
     *
     * @return	The requestDate.
     */
    public Date getRequestDate()
    {
	return requestDate;
    }

    /**
     * Setter for requestDate.
     *
     * @param	requestDate	the request date
     */
    public void setRequestDate(Date requestDate)
    {
	this.requestDate = requestDate;
    }

    /**
     * Getter for operatorID.
     *
     * @return	The operator identifier.
     */
    public Long getOperatorID()
    {
	return operatorID;
    }

    /**
     * Setter for operatorID.
     *
     * @param	operatorID	the operator identifier
     */
    public void setOperatorID(Long operatorID)
    {
	this.operatorID = operatorID;
    }

    /**
     * Getter for roleLevel.
     *
     * @return	The role level of this operator.
     */
    public Long getRoleLevel()
    {
	return roleLevel;
    }

    /**
     * Setter for roleLevel.
     *
     * @param	roleLevel	the role level of this user.
     */
    public void setRoleLevel(Long roleLevel)
    {
	this.roleLevel = roleLevel;
    }

    /**
     * Getter for jobName.
     *
     * @return	The Name of this request.
     */
    public String getJobName()
    {
	return jobName;
    }

    /**
     * Setter for jobName.
     *
     * @param	jobName		the name of this request
     */
    public void setJobName(String jobName)
    {
	this.jobName = jobName;
    }

    /**
     * Getter for validateDate.
     *
     * @return	The validate date of this request.
     */
    public Date getValidateDate()
    {
	return validateDate;
    }

    /**
     * Setter for validateDate.
     *
     * @param	validateDate	the validate date of this request
     */
    public void setValidateDate(Date validateDate)
    {
	this.validateDate = validateDate;
    }

    /**
     * Getter for processDate.
     *
     * @return	The process date of this request.
     */
    public Date getProcessDate()
    {
	return processDate;
    }

    /**
     * Setter for processDate.
     *
     * @param	processDate	the process date of this request
     */
    public void setProcessDate(Date processDate)
    {
	this.processDate = processDate;
    }

    /**
     * Getter for processStatus.
     *
     * @return	The process status of this request.
     */
    public String getProcessStatus()
    {
	return processStatus;
    }

    /**
     * Setter for processStatus.
     *
     * @param	processStatus		the process Status of this request
     */
    public void setProcessStatus(String processStatus)
    {
	this.processStatus = processStatus;
    }

    /**
     * Getter for processXml.
     *
     * @return	The process XML of this request.
     */
    public String getProcessXML()
    {
	return processXml;
    }

    /**
     * Setter for processXml.
     *
     * @param	processXml		the process XML of this request
     */
    public void setProcessXML(String processXml)
    {
	this.processXml = processXml;
    }
}

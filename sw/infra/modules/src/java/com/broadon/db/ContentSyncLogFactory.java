package com.broadon.db;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

import com.broadon.bean.Bean;

/**
 * The <c>ContentSyncLogFactory</c> class interfaces with the database to
 * create, update, or query records from the content-sync-logs table.
 * Each record is represented by a ContentSyncLog JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.1 $
 */
public class ContentSyncLogFactory
    extends DBAccessFactory
{
    /**
     * Constructs a ContentSyncLogFactory instance.
     */
    protected ContentSyncLogFactory()
    {
	super();
    }

    /**
     * Returns the ContentSyncLog information, based on the given depotID
     * and requestDate.
     *
     * @param	depotID			the depot identifier
     * @param	requestDate		the date of the request
     * @param	nameMap			the name mapping table
     *
     * @return	The ContentSyncLog JavaBeans containing all information of
     *		a contenty-sync-logs record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public ContentSyncLog getContentSyncLog(long depotID,
					    Date requestDate,
					    Map nameMap)
	throws DBException, IOException
    {
	DepotID		predicate = new DepotID(depotID);

	predicate.and(new RequestDate(requestDate));
	return (ContentSyncLog)queryUnique(predicate, nameMap);
    }

    /**
     * Returns the database table name to be used.
     *
     * @return	The data table name.
     */
    public String getTableName()
    {
	return "content_sync_logs";
    }

    /**
     * Creates a Depot JavaBeans object.
     *
     * @return	The newly created Depot object.
     */
    protected Bean newBean()
    {
	return new ContentSyncLog();
    }
}

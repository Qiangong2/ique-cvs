package com.broadon.db;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Array;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.xml.sql.dml.OracleXMLSave;
import oracle.xml.sql.query.OracleXMLQuery;

import org.apache.log4j.Logger;
import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.broadon.bean.Bean;
import com.broadon.sql.PBean;
import com.broadon.sql.Predicate;

/**
 * The <code>DBAccessFactory</code> abstract class is extended by all factory
 * classes that access the database.
 *<p>
 * The singleton instance of each subclass can be located by calling the
 * getInstance method with the full class name (with package path).
 *<p>
 * It provides the following public methods shared by all factory classes:
 *<ul>
 *<pre>
 *<li>beginTransaction: 	Begins a new transaction.
 *<li>commitTransaction:	Commits the current transaction.
 *<li>rollbackTransaction:	Rolls back the current transaction.
 *<li>create:			Creates a new record.
 *<li>update:			Updates an existing record.
 *<li>delete:			Deletes an existing record.
 *<li>queryUnique:		Queries the unique record.
 *<li>query:			Queries the record(s).
 *</pre>
 *</ul>
 * The subclasses just need to implement a subset of the following methods,
 * some of them have default implementation that cover the most common cases.
 *<p>
 * These methods must be implemented:
 *<ul>
 *<pre>
 *<li>getTableName:  		Returns the associated database table name.
 *<li>newBean:  		Creates a factory corresponding Bean object.
 *</pre>
 *</ul>
 * These methods are for the more sophisticated factory classes:
 *<ul>
 *<pre>
 *<li>getSelectPart:    	Constructs the select part of the query.
 *<li>bind:			Binds the data to the statement.
 *<li>fill:			Moves data from ResultSet to Bean.
 *</pre>
 *</ul>
 * These methods are for the most sophisticated factory classes:
 *<ul>
 *<pre>
 *<li>setUpCreateStatement:	Constructs and prepares the create statement.
 *<li>setUpUpdateStatement:	Constructs and prepares the update statement.
 *<li>setUpDeleteStatement:	Constructs and prepares the delete statement.
 *<li>setUpQueryStatement:	Constructs and prepares the query statement.
 *<li>prepareStatement: 	Prepares and fills in the SQL statement.
 *<li>processQueryResult:	Processes the query result (single record).
 *<li>processQueryResults:	Processes the query results.
 *</pre>
 *</ul>
 * The factory classes may also by pass some of these methods and implement
 * their own.
 *<p>
 * Normally, each factory class is associated with one database table.
 * Querying from more than one table is possible, though not supported
 * by the default implementation in this class.
 *
 * @version	$Revision: 1.27 $
 */
public abstract class DBAccessFactory
{
    private static final String			GET_NEXT_ID_CALL;
    private static final String 		TABLE_PK_CALL; 

    private static Map				factoryMap;
    private static ThreadLocalConnection	tlConnection;

    private static Logger			logger;

    static
    {
        TABLE_PK_CALL = "{? = call BBXML.getpkcol(?)}";
	GET_NEXT_ID_CALL = "{? = call BBXML.GetNextID(?,?)}";

	factoryMap	= new HashMap();
	tlConnection	= new ThreadLocalConnection();
    }

    /**
     * Retuns the singleton instance of this factory.
     *
     * @throws	Exception
     */
    public static DBAccessFactory getInstance(String className)
	throws DBException
    {
	synchronized (factoryMap)
	{
	    /*
	     * Search by full class name.
	     */
	    DBAccessFactory	factory;
            logger = Logger.getLogger(DBAccessFactory.class.getName());

	    factory = (DBAccessFactory)factoryMap.get(className);
	    if (factory == null)
	    {
		/*
		 * The first time.
		 */
		try
		{
		    Class	factoryClass = Class.forName(className);

		    factory = (DBAccessFactory)factoryClass.newInstance();
		}
		catch (ClassNotFoundException cnfe)
		{
		    throw new DBException("Cannot found class " + className,
					  cnfe);
		}
		catch (InstantiationException ie)
		{
		    throw new DBException("Cannot instantiate " + className,
					  ie);
		}
		catch (IllegalAccessException iae)
		{
		    throw new DBException("Cannot access " + className, iae);
		}
		factoryMap.put(className, factory);
	    }
	    return factory;
	}
    }


    /**
     * Begins a transaction.
     *
     * same as <code>beginTransaction(dataSource, autoCommit, readonly,
     * com.broadon.db.DataSourceScheduler.DB_PRIORITY_DEFAULT)</code>
     */
    public static void beginTransaction(DataSource dataSource,
					boolean autoCommit,
					boolean readOnly)
	throws DBException
    {
	beginTransaction(dataSource, autoCommit, readOnly,
			 DataSourceScheduler.DB_PRIORITY_DEFAULT);
    }
    

    /**
     * Begins a transaction.
     *
     * @param	dataSource		the data source
     * @param	autoCommit		true  => 1 statement/transaction
     *					false => multiple statements/transaction
     * @param	readOnly		true  => read only transaction;
     *					false => otherwise
     * @param   priority		Connection pool access priority, as
     *					defined in <code>DataSourceScheduler</code>.
     *
     * @throws	DBException
     */
    public static void beginTransaction(DataSource dataSource,
					boolean autoCommit,
					boolean readOnly,
					int priority)
	throws DBException
    {
	/*
	 * Get a connection, and remember it for this thread.
	 */
	Connection	connection;

	connection = tlConnection.newConnection(dataSource, priority);
	try
	{
	    /*
	     * Set transaction options.
	     */
	    connection.setAutoCommit(autoCommit);
	    if (readOnly)
	    {
		connection.setReadOnly(readOnly);
	    }
	}
	catch (SQLException se)
	{
	    /*
	     * Cleanup.
	     */
	    tlConnection.clearConnection(connection);
	    se.printStackTrace();
	    throw new DBException("Fail to begin transaction", se);
	}
    }

    /**
     * Commits a transaction.
     *
     * @throws	DBException
     */
    public static void commitTransaction()
	throws DBException
    {
	/*
	 * Retrieve the connection for this thread.
	 */
	Connection	connection = (Connection)tlConnection.get();

	try
	{
	    /*
	     * Commit.
	     */
	    connection.commit();

	}
	catch (SQLException se)
	{
	    se.printStackTrace();
	    throw new DBException("Fail to commit", se);
	}
	finally
	{
	    /*
	     * Cleanup.
	     */
	    tlConnection.clearConnection(connection);
	}
    }

    /**
     * Rolls back a transaction.
     *
     * @throws	DBException
     */
    public static void rollbackTransaction()
	throws DBException
    {
	/*
	 * Retrieve the connection for this thread.
	 */
	Connection	connection = (Connection)tlConnection.get();

	try
	{
	    /*
	     * Rollback.
	     */
	    connection.rollback();
	}
	catch (SQLException se)
	{
	    se.printStackTrace();
	    throw new DBException("Fail to rollback", se);
	}
	finally
	{
	    /*
	     * Cleanup.
	     */
	    tlConnection.clearConnection(connection);
	}
    }

    /**
     * Constructs a ContentInfoFactory instance.
     */
    protected DBAccessFactory()
    {
    }

    /**
     * Creates a record in the database based on the given JavaBeans
     * object.
     *
     * @param	bean			the JavaBeans object that contains
     *					the information to be created
     *
     * @throws	DBException
     * @throws	IOException
     */
    public void create(Bean bean)
	throws DBException, IOException
    {
	this.create(bean, null);
    }

    /**
     * Creates a record in the database based on the given JavaBeans
     * object.
     *
     * @param	bean			the JavaBeans object that contains
     *					the information to be created
     * @param	nameMap			the name mapping table
     *
     * @throws	DBException
     * @throws	IOException
     */
    public void create(Bean bean, Map nameMap)
	throws DBException, IOException
    {
	PreparedStatement	statement;

	statement = setupCreateStatement(bean, nameMap);
	try
	{
	    execute(statement);
	}
	finally
	{
	    /*
	     * Close the statment after execution.
	     */
	    closeStatement(statement);
	}
    }

    /**
     * Creates a record in the database based on the given XML Document.
     *
     * @param	xml			the XML document that contains
     *					the information to be created
     *
     * @throws	DBException
     */
    public int create(String xml)
	throws DBException
    {
	return tableXDML(xml, getTableName(), "INS");
    }

    /**
     * Updates a record in the database based on the given JavaBeans
     * object.
     *
     * @param	bean			the JavaBeans object that contains
     *					the information to be updated
     * @param	predicate		the criteria for qualified record
     *					to be updated
     *
     * @throws	DBException
     * @throws	IOException
     */
    public void update(Bean bean, Predicate predicate)
	throws DBException, IOException
    {
	this.update(bean, predicate, null);
    }

    /**
     * Updates a record in the database based on the given JavaBeans
     * object.
     *
     * @param	bean			the JavaBeans object that contains
     *					the information to be updated
     * @param	predicate		the criteria for qualified record
     *					to be updated
     * @param	nameMap			the name mapping table
     *
     * @throws	DBException
     * @throws	IOException
     */
    public void update(Bean bean, Predicate predicate, Map nameMap)
	throws DBException, IOException
    {
	PreparedStatement	statement;

	statement = setupUpdateStatement(bean, predicate, nameMap);
	try
	{
	    execute(statement);
	}
	finally
	{
	    /*
	     * Close the statment after execution.
	     */
	    closeStatement(statement);
	}
    }

    /**
     * Updates a record in the database based on the given SET clause.
     *
     * @param	set			the SET clause (without the SET keyword)
     * @param	predicate		the criteria for qualified record
     *					to be updated
     *
     * @throws	DBException
     * @throws	IOException
     */
    public void update(String set, Predicate predicate)
	throws DBException, IOException
    {
	this.update(set, predicate, null);
    }

    /**
     * Updates a record in the database based on the given SET clause.
     *
     * @param	set			the SET clause (without the SET keyword)
     * @param	predicate		the criteria for qualified record
     *					to be updated
     * @param	nameMap			the name mapping table
     *
     * @throws	DBException
     * @throws	IOException
     */
    public void update(String set, Predicate predicate, Map nameMap)
	throws DBException, IOException
    {
	PreparedStatement	statement;

	statement = setupUpdateStatement(set, predicate, nameMap);
	try
	{
	    execute(statement);
	}
	finally
	{
	    /*
	     * Close the statment after execution.
	     */
	    closeStatement(statement);
	}
    }

    /**
     * Updates a record in the database based on the given XML Document.
     *
     * @param	xml			the XML document that contains
     *					the information to be created
     *
     * @throws	DBException
     */
    public int update(String xml)
	throws DBException
    {
	return tableXDML(xml, getTableName(), "UPD");
    }

    /**
     * Inserts/Updates records in the database based on the given XML Document.
     *
     * @param	xml			the XML document that contains
     *					the information to be created or updated
     *
     * @throws	DBException
     */
    public int insertupdate(String xml)
	throws DBException
    {
	int rows = 0;
	rows = update(xml);
	if (rows == 0)
	    rows = create(xml);
	return rows;
    }

    /**
     * Deletes a record in the database based on the given JavaBeans
     * object.
     *
     * @param	predicate		the criteria for qualified record
     *					to be deleted
     *
     * @throws	DBException
     * @throws	IOException
     */
    public void delete(Predicate predicate)
	throws DBException, IOException
    {
	this.delete(predicate, null);
    }

    /**
     * Deletes a record in the database based on the given JavaBeans
     * object.
     *
     * @param	predicate		the criteria for qualified record
     *					to be deleted
     * @param	nameMap			the name mapping table
     *
     * @throws	DBException
     * @throws	IOException
     */
    public void delete(Predicate predicate, Map nameMap)
	throws DBException, IOException
    {
	PreparedStatement	statement;

	statement = setupDeleteStatement(predicate, nameMap);
	try
	{
	    execute(statement);
	}
	finally
	{
	    /*
	     * Close the statment after execution.
	     */
	    closeStatement(statement);
	}
    }

    /**
     * Deletes a record in the database based on the given XML Document.
     *
     * @param	xml			the XML document that contains
     *					the information to be created
     *
     * @throws	DBException
     */
    public int delete(String xml)
	throws DBException
    {
	return tableXDML(xml, getTableName(), "DEL");
    }

    /**
     * Queries the record identified by the given unique key.
     *
     * @param	predicate		the criteria for qualified record
     *
     * @return	The JavaBeans object that represents the qualified record,
     *		or null if not found.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public Bean queryUnique(Predicate predicate)
	throws DBException, IOException
    {
	return this.queryUnique(predicate, null);
    }

    /**
     * Queries the record identified by the given unique key.
     *
     * @param	predicate		the criteria for qualified record
     * @param	nameMap			the name mapping table
     *
     * @return	The JavaBeans object that represents the qualified record,
     *		or null if not found.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public Bean queryUnique(Predicate predicate, Map nameMap)
	throws DBException, IOException
    {
	PreparedStatement	statement;

	statement = setupQueryStatement(predicate, nameMap);
	try
	{
	    return processQueryResult(executeQuery(statement), nameMap);
	}
	finally
	{
	    /*
	     * Close the statment after execution.
	     *
	     * The corresponding ResultSet will also be
	     * closed internally.
	     */
	    closeStatement(statement);
	}
    }

    /**
     * Queries the record identified by the given unique key.
     *
     * @param	predicate		the criteria for qualified record
     * @param	sql			the custom SQL statement
     * @param	nameMap			the name mapping table
     *
     * @return	The JavaBeans object that represents the qualified record,
     *		or null if not found.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public Bean queryUnique(Predicate predicate, String sql, Map nameMap)
	throws DBException, IOException
    {
	PreparedStatement	statement;

	statement = prepareStatement(predicate, sql);
	try
	{
	    return processQueryResult(executeQuery(statement), nameMap);
	}
	finally
	{
	    /*
	     * Close the statment after execution.
	     *
	     * The corresponding ResultSet will also be
	     * closed internally.
	     */
	    closeStatement(statement);
	}
    }

    /**
     * Queries the records that satisfied the given predicate info.
     *
     * @param	predicate		the criteria for qualified records
     *
     * @return	The array of JavaBeans objects that represent the qualified
     *		records, or null if none found.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public Bean[] query(Predicate predicate)
	throws DBException, IOException
    {
	return this.query(predicate, null);
    }

    /**
     * Queries the records that satisfied the given predicate info.
     *
     * @param	predicate		the criteria for qualified records
     * @param	nameMap			the name mapping table
     *
     * @return	The array of JavaBeans objects that represent the qualified
     *		records, or null if none found.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public Bean[] query(Predicate predicate, Map nameMap)
	throws DBException, IOException
    {
	PreparedStatement	statement;

	statement = setupQueryStatement(predicate, nameMap);
	try
	{
	    return processQueryResults(executeQuery(statement), nameMap);
	}
	finally
	{
	    /*
	     * Close the statment after execution.
	     *
	     * The corresponding ResultSet will also be
	     * closed internally.
	     */
	    closeStatement(statement);
	}
    }

    /**
     * Queries the records identified by the given predicate info.
     *
     * @param	predicate		the criteria for qualified record
     * @param	sql			the custom SQL statement
     * @param	nameMap			the name mapping table
     *
     * @return	The array of JavaBeans objects that represent the qualified
     *		records, or null if none found.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public Bean[] query(Predicate predicate, String sql, Map nameMap)
	throws DBException, IOException
    {
	PreparedStatement	statement;

	statement = prepareStatement(predicate, sql);
	try
	{
	    return processQueryResults(executeQuery(statement), nameMap);
	}
	finally
	{
	    /*
	     * Close the statment after execution.
	     *
	     * The corresponding ResultSet will also be
	     * closed internally.
	     */
	    closeStatement(statement);
	}
    }

    /**
     * Queries the records identified by the given SQL statement.
     * This is a generic query, and the returning format is XML.
     *
     * @param	sql			the SQL statement
     *
     * @return	The XML document that represent the qualified
     *		records, or null if none found.
     *
     * @throws	DBException
     * @throws	IOException
     */
     public String query(String sql) 
        throws DBException
     {
	return query(sql,-1,-1);
     }

     public String query(String sql, int numrows, int skiprows)
	throws DBException
     {
        Connection conn = (Connection)tlConnection.get();
	if (conn instanceof OracleDataSource.ConnectionWrapper)
	    conn = ((OracleDataSource.ConnectionWrapper)conn).unwrap();
        OracleXMLQuery qry = new OracleXMLQuery(conn, sql);

        logger.debug("QUERY SQL = " + sql);

        if (numrows >= 0) 
            qry.setMaxRows(numrows);
	if (skiprows >= 0) 
            qry.setSkipRows(skiprows);
	qry.setDateFormat("yyyy.MM.dd HH:mm:ss");

	String xml = qry.getXMLString();
        qry.close();

        logger.debug("RESULT XML = " + xml);
	return xml;
    }

    /**
     * Gets the next id to be added into the table.
     *
     * @throws	DBException
     */
    public long getNextID(String column)
	throws DBException
    {
	if (column == null)
	{
	    throw new DBException("No Column Name provided");
	}
	
	CallableStatement	call = prepareCall(GET_NEXT_ID_CALL);
	long ret = -1;

	try
	{
	    call.registerOutParameter(1, Types.NUMERIC);
	    call.setString(2, getTableName());
	    call.setString(3, column);
	    call.executeQuery();

            ret = call.getLong(1);
	}
	catch (SQLException se)
	{
	    throw new DBException("SQL exception: " + se.getMessage(), se);
	}
	finally
	{
	    /*
	     * Close the statment after execution.
	     */
	    closeStatement(call);
	}
        return ret;
    }

    /**
     * Counts the number of records identified by the given SQL statement.
     *
     * @param	sql			the SQL statement that counts
     *
     * @return	The count.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int count(String sql)
	throws DBException, IOException
    {
	return count(null, sql);
    }

    /**
     * Counts the number of records identified by the given SQL statement.
     *
     * @param	predicate		the criteria for qualified record
     * @param	sql			the SQL statement that counts
     *
     * @return	The count.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int count(Predicate predicate, String sql)
	throws DBException, IOException
    {
	PreparedStatement	statement = prepareStatement(predicate, sql);

	try
	{
	    ResultSet	resultSet = executeQuery(statement);

	    if (resultSet == null || !resultSet.next())
	    {
		/*
		 * No record.
		 */
		return 0;
	    }
	    return resultSet.getInt(1);
	}
	catch (SQLException se)
	{
	    throw new DBException("SQL exception: " + se.getMessage(), se);
	}
	finally
	{
	    /*
	     * Close the statment after execution.
	     *
	     * The corresponding ResultSet will also be
	     * closed internally.
	     */
	    closeStatement(statement);
	}
    }

    /**
     * Sets up the create statement.
     *<p>
     * The implementation varies from factory to factory, and therefore will
     * be provided by the subclasses.
     *
     * @param	bean			the JavaBeans object that contains
     *					the information to be created
     * @param	nameMap			the name mapping table
     *
     * @return	The prepared statement.
     *
     * @throws	DBException
     * @throws	IOException
     */
    protected PreparedStatement setupCreateStatement(Bean bean, Map nameMap)
	throws DBException, IOException
    {
	if (bean == null)
	{
	    throw new DBException("No data");
	}
	/*
	 * Setup the insert statement.
	 */
	String		sql;

	sql = "insert into " +
	      getTableName() +
	      ((PBean)bean).generateValuesPart(nameMap);
	/*
	 * Prepare the statement.
	 */
	return prepareStatement((PBean)bean, sql);
    }

    /**
     * Sets up the update statement.
     *<p>
     * It provides the default implementation here. The more sophisticated
     * factory subclasses can rewrite it.
     *
     * @param	bean			the JavaBeans object that contains
     *					the information to be updated
     * @param	predicate		the criteria for qualified record
     *					to be updated
     * @param	nameMap			the name mapping table
     *
     * @return	The prepared statement.
     *
     * @throws	DBException
     * @throws	IOException
     */
    protected PreparedStatement setupUpdateStatement(Bean bean,
						     Predicate predicate,
						     Map nameMap)
	throws DBException, IOException
    {
	if (bean == null)
	{
	    throw new DBException("No data");
	}
	if (predicate == null)
	{
	    throw new DBException("Predicate required");
	}
	/*
	 * Setup the update statement.
	 */
	String		sql;

	sql = "update " +
	      getTableName() +
	      " set" +
	      ((PBean)bean).generateSetPart(nameMap) +
	      " where" +
	      predicate.generateWherePart(nameMap);
	/*
	 * Prepare the statement.
	 */
	return prepareStatement((PBean)bean, predicate, sql);
    }

    /**
     * Sets up the update statement.
     *<p>
     * It provides the default implementation here. The more sophisticated
     * factory subclasses can rewrite it.
     *
     * @param	set			the SET clause (without the SET keyword)
     * @param	predicate		the criteria for qualified record
     *					to be updated
     * @param	nameMap			the name mapping table
     *
     * @return	The prepared statement.
     *
     * @throws	DBException
     * @throws	IOException
     */
    protected PreparedStatement setupUpdateStatement(String set,
						     Predicate predicate,
						     Map nameMap)
	throws DBException, IOException
    {
	if (set == null)
	{
	    throw new DBException("No data");
	}
	if (predicate == null)
	{
	    throw new DBException("Predicate required");
	}
	/*
	 * Setup the update statement.
	 */
	String		sql;

	sql = "update " +
	      getTableName() +
	      " set " +
	      set +
	      " where" +
	      predicate.generateWherePart(nameMap);
	/*
	 * Prepare the statement.
	 */
	return prepareStatement(predicate, sql);
    }

    /**
     * Sets up the delete statement.
     *<p>
     * It provides the default implementation here. The more sophisticated
     * factory subclasses can rewrite it.
     *
     * @param	predicate		the criteria for qualified record
     *					to be deleted
     * @param	nameMap			the name mapping table
     *
     * @return	The prepared statement.
     *
     * @throws	DBException
     * @throws	IOException
     */
    protected PreparedStatement setupDeleteStatement(Predicate predicate,
						     Map nameMap)
	throws DBException, IOException
    {
	if (predicate == null)
	{
	    throw new DBException("Predicate required");
	}
	/*
	 * Setup the delete statement.
	 */
	String		sql = "delete from " +
			      getTableName() +
			      " where" +
			      predicate.generateWherePart(nameMap);
	/*
	 * Prepare the statement.
	 */
	return prepareStatement(predicate, sql);
    }

    /**
     * Sets up the query statement.
     *<p>
     * It provides the default implementation here. The more sophisticated
     * factory subclasses can rewrite it.
     *
     * @param	predicate		the criteria for wanted records
     * @param	nameMap			the name mapping table
     *
     * @return	The prepared statement.
     *
     * @throws	DBException
     * @throws	IOException
     */
    protected PreparedStatement setupQueryStatement(Predicate predicate,
						    Map nameMap)
	throws DBException, IOException
    {
	/*
	 * Get the select part of the SQL statement based on the factory class.
	 */
	String		selectPart = getSelectPart();
	String		sql;

	if (predicate == null)
	{
	    sql = selectPart;
	}
	else
	{
	    String	wherePart = predicate.generateWherePart(nameMap);

	    sql = selectPart + " where" + wherePart;
	}
	/*
	 * Prepare the statement.
	 */
	return prepareStatement(predicate, sql);
    }

    /**
     * Prepares the SQL statement.
     *<p>
     * It provides the default implementation here. The more sophisticated
     * factory subclasses can rewrite it.
     *
     * @param	bean			the criteria for wanted records
     * @param	sql			the SQL statement to be prepared
     *
     * @return	The prepared statement.
     *
     * @throws	DBException
     * @throws	IOException
     */
    protected PreparedStatement prepareStatement(PBean bean,
						 String sql)
	throws DBException, IOException
    {
	PreparedStatement	statement = prepareStatement(sql);

	try
	{
	    /*
	     * Fill in the parameters.
	     */
	    bind((PBean)bean, statement);
	    return statement;
	}
	catch (DBException de)
	{
	    closeStatement(statement);
	    throw de;
	}
	catch (IOException ie)
	{
	    closeStatement(statement);
	    throw ie;
	}
	catch (Throwable t)
	{
	    closeStatement(statement);
	    throw new DBException(t.getMessage(), t);
	}
    }

    /**
     * Prepares the SQL statement.
     *<p>
     * It provides the default implementation here. The more sophisticated
     * factory subclasses can rewrite it.
     *
     * @param	bean			the criteria for wanted records
     * @param	predicate		the criteria for wanted records
     * @param	sql			the SQL statement to be prepared
     *
     * @return	The prepared statement.
     *
     * @throws	DBException
     * @throws	IOException
     */
    protected PreparedStatement prepareStatement(PBean bean,
						 Predicate predicate,
						 String sql)
	throws DBException, IOException
    {
	PreparedStatement	statement = prepareStatement(sql);

	try
	{
	    /*
	     * Fill in the parameters.
	     */
	    int			index = bind((PBean)bean, statement);

	    if (predicate != null)
	    {
		predicate.bind(statement, index);
	    }
	    return statement;
	}
	catch (DBException de)
	{
	    closeStatement(statement);
	    throw de;
	}
	catch (IOException ie)
	{
	    closeStatement(statement);
	    throw ie;
	}
	catch (Throwable t)
	{
	    closeStatement(statement);
	    throw new DBException(t.getMessage(), t);
	}
    }

    /**
     * Prepares the SQL statement.
     *<p>
     * It provides the default implementation here. The more sophisticated
     * factory subclasses can rewrite it.
     *
     * @param	predicate		the criteria for wanted records
     * @param	sql			the SQL statement to be prepared
     *
     * @return	The prepared statement.
     *
     * @throws	DBException
     * @throws	IOException
     */
    protected PreparedStatement prepareStatement(Predicate predicate,
						 String sql)
	throws DBException, IOException
    {
	PreparedStatement	statement = prepareStatement(sql);

	if (predicate == null)
	{
	    /*
	     * No binding necessary.
	     */
	    return statement;
	}

	try
	{
	    /*
	     * Fill in the parameters.
	     */
	    bind(predicate, statement);
	    return statement;
	}
	catch (DBException de)
	{
	    closeStatement(statement);
	    throw de;
	}
	catch (IOException ie)
	{
	    closeStatement(statement);
	    throw ie;
	}
	catch (Throwable t)
	{
	    closeStatement(statement);
	    throw new DBException(t.getMessage(), t);
	}
    }

    /**
     * Processes the query result that has only a single qualified record.
     *<p>
     * It provides the default implementation here. The more sophisticated
     * factory subclasses can rewrite it.
     *
     * @param	resultSet		the ResultSet
     * @param	nameMap			the name mapping table
     *
     * @return	The JavaBeans object that contains the data
     *
     * @throws	DBException
     * @throws	IOException
     */
    protected Bean processQueryResult(ResultSet resultSet, Map nameMap)
	throws DBException, IOException
    {
	try
	{
	    if (resultSet == null || !resultSet.next())
	    {
		/*
		 * No record.
		 */
		return null;
	    }

	    Bean	bean = fill(newBean(), resultSet, nameMap);

	    if (resultSet.next())
	    {
		/*
		 * This is not the only qualified record.
		 */
		throw new NotUniqueException(bean.toString());
	    }
	    return bean;
	}
	catch (SQLException se)
	{
	    se.printStackTrace();
	    throw new DBException("Fail to process result", se);
	}
    }

    /**
     * Processes the query results.
     *<p>
     * It provides the default implementation here. The more sophisticated
     * factory subclasses can rewrite it.
     *
     * @param	resultSet		the ResultSet
     * @param	nameMap			the name mapping table
     *
     * @return	The array of JavaBeans objects that contain the data
     *
     * @throws	DBException
     * @throws	IOException
     */
    protected Bean[] processQueryResults(ResultSet resultSet, Map nameMap)
	throws DBException, IOException
    {
	int	size = 0;
	List	list;

	try
	{
	    if (resultSet == null || !resultSet.next())
	    {
		/*
		 * No record.
		 */
		return null;
	    }

	    list = new ArrayList();
	    /*
	     * Process the records and convert them into Bean objects.
	     */
	    do
	    {
		list.add(fill(newBean(), resultSet, nameMap));
		size++;
	    } while (resultSet.next());
	}
	catch (SQLException se)
	{
	    se.printStackTrace();
	    throw new DBException("Fail to process result after " +
				      size +
				      " records",
				  se);
	}
	/*
	 * Return them as an array.
	 */
	Bean[]	beans = new Bean[size];

	for (int n = 0; n < size; n++)
	{
	    beans[n] = (Bean)list.get(n);
	}
	return beans;
    }

    /**
     * Creates a Bean object according to the factory class.
     *<p>
     * The implementation varies from factory to factory, and therefore will
     * be provided by the subclasses.
     *
     * @return	The factory corresponding Bean object.
     */
    protected abstract Bean newBean();

    /**
     * Returns the select part of the query. Each factory should have only
     * one.
     */
    protected String getSelectPart()
    {
	return "select * from " + getTableName();
    }

    /**
     * Returns the database table name to be used.
     *
     * @return	The data table name.
     */
    public abstract String getTableName();

    /**
     * Binds the bean data to the prepared statement.
     *
     * @param	pBean			the bean that contains the data
     * @param	statement		the PreparedStatement
     *
     * @return	The last index used for the PreparedStatement.
     *
     * @throws	DBException
     * @throws	IOException
     */
    protected int bind(PBean pBean, PreparedStatement statement)
	throws DBException, IOException
    {
	return pBean.bind(statement);
    }

    /**
     * Binds the predicate data to the prepared statement.
     *
     * @param	predicate		the predicate that contains the data
     * @param	statement		the PreparedStatement
     *
     * @throws	DBException
     * @throws	IOException
     */
    protected void bind(Predicate predicate, PreparedStatement statement)
	throws DBException, IOException
    {
	predicate.bind(statement);
    }

    /**
     * Fills the given JavaBeans object with the current record of the
     * ResultSet.
     *<p>
     * The default implementation assumes that the given bean objects
     * are subclass of PBean.
     *
     * @param	bean			the target JavaBeans object
     * @param	resultSet		the ResultSet
     * @param	nameMap			the name mapping table
     *
     * @return	The given Bean object; so that this method can become part
     *		of an expression.
     *
     * @throws	DBException
     * @throws	IOException
     */
    protected Bean fill(Bean bean, ResultSet resultSet, Map nameMap)
	throws DBException, IOException
    {
	((PBean)bean).fill(resultSet, nameMap);
	return bean;
    }

    /**
     * Prepares a SQL statement.
     *<p>
     * The purpose of this method is to hide the database connection from
     * the subclasses.
     *
     * @param	sql			the SQL statement, in readable form
     *
     * @return	The prepared statement.
     *
     * @throws	DBException
     */
    protected PreparedStatement prepareStatement(String sql)
	throws DBException
    {
	/*
	 * Get the connection from ThreadLocal.
	 */
	Connection	connection = (Connection)tlConnection.get();

	return prepareStatement(sql, connection);
    }

    /**
     * Prepares a SQL statement.
     *<p>
     * The purpose of this method is to hide the SQLException from the
     * subclasses.
     *
     * @param	sql			the SQL statement, in readable form
     *
     * @return	The prepared statement.
     *
     * @throws	DBException
     */
    protected PreparedStatement prepareStatement(String sql,
						 Connection connection)
	throws DBException
    {
	try
	{
	    return connection.prepareStatement(sql);
	}
	catch (SQLException se)
	{
	    se.printStackTrace();
	    throw new DBException("Cannot prepare statement: " + sql, se);
	}
    }

    /**
     * Prepares a callable SQL statement.
     *<p>
     * The purpose of this method is to hide the database connection from
     * the subclasses.
     *
     * @param	sql			the SQL statement, in readable form
     *
     * @return	The callable statement.
     *
     * @throws	DBException
     */
    protected CallableStatement prepareCall(String sql)
	throws DBException
    {
	/*
	 * Retrieve the connection for this thread.
	 */
	Connection	connection = (Connection)tlConnection.get();

	return prepareCall(sql, connection);
    }

    /**
     * Prepares a callable SQL statement.
     *<p>
     * The purpose of this method is to hide the SQLException from the
     * subclasses.
     *
     * @param	sql			the SQL statement, in readable form
     *
     * @return	The callable statement.
     *
     * @throws	DBException
     */
    protected CallableStatement prepareCall(String sql, Connection connection)
	throws DBException
    {
	try
	{
	    return connection.prepareCall(sql);
	}
	catch (SQLException se)
	{
	    se.printStackTrace();
	    throw new DBException("Cannot prepare call: " + sql, se);
	}
    }

    /**
     * Executes the prepared statement.
     *<p>
     * The purpose of this method is to hide the SQLException from the
     * subclasses.
     *
     * @param	statemtent		the SQL statement to be executed
     *
     * @return	true  => The next result is a ResultSet object.
     *		false => It is an update count or there are no more results.
     *
     * @throws	DBException
     */
    protected boolean execute(PreparedStatement statement)
	throws DBException
    {
	/*
	 * Execute the query.
	 */
	try
	{
	    return statement.execute();
	}
	catch (SQLException se)
	{
	    se.printStackTrace();
	    throw new DBException("Fail to execute statement", se);
	}
    }

    public int tableXDML(String xml, String tablename, String dmltype)
            throws DBException {
        Connection conn = (Connection) tlConnection.get();
        OracleXMLSave sav = new OracleXMLSave(conn, tablename);
        int rows = 0;

        logger.debug("XML = " + xml);
        logger.debug("TABLE = " + tablename);
        logger.debug("DML TYPE = " + dmltype);

        if (isNullXML(xml))
            return 0;

        String[] keyColNames = getPKcol(tablename);

        try {
            if (keyColNames.length > 0) {
                sav.setDateFormat("yyyy.MM.dd HH:mm:ss");
                sav.setKeyColumnList(keyColNames);

                if (dmltype.equals("INS")) {
                    ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());
                    InputStream inStream = bais;
                    InputSource is = new InputSource(inStream);

                    DOMParser parser = new DOMParser();
                    parser.parse(is);

                    Document doc = parser.getDocument();
                    NodeList nlr = doc.getElementsByTagName("ROW");

                    if (nlr.getLength() > 0) {
                        Node nr = nlr.item(0);
                        NodeList nlrc = nr.getChildNodes();
                        String[] updateCol = new String[nlrc.getLength()];
                        Node nrc;

                        for (int i = 0; i < nlrc.getLength(); i++) {
                            nrc = nlrc.item(i);
                            if (!(nrc.getNodeName().equals("ROWSET")) &&
                                !(nrc.getNodeName().equals("ROW"))) {
                                updateCol[i] = nrc.getNodeName();
                            }
                        }
                        sav.setUpdateColumnList(updateCol);
                        rows = sav.insertXML(xml);
                    }
                }
                if (dmltype.equals("UPD"))
                    rows = sav.updateXML(xml);

                if (dmltype.equals("DEL"))
                    rows = sav.deleteXML(xml);
            }
        } catch (IOException ie) {
            ie.printStackTrace();
            throw new DBException("IO exception: " + ie.getMessage(), ie);
        } catch (SAXException se) {
            se.printStackTrace();
            throw new DBException("SAX exception: " + se.getMessage(), se);
        } finally {
            sav.close();
            logger.debug("ROWS AFFECTED = " + Integer.toString(rows));
        }
        return rows;
    }

    public String[] getPKcol(String tablename) throws DBException {
        CallableStatement cs = prepareCall(TABLE_PK_CALL);
        String[] ret = null;

        try {
            cs.registerOutParameter(1, Types.ARRAY, "STRING_LIST");
            cs.setString(2, tablename);
            cs.executeUpdate();

            Array a = cs.getArray(1);

            if (a != null)
                ret = (String[]) a.getArray();
        } catch (SQLException se) {
            se.printStackTrace();
            throw new DBException("SQL exception: " + se.getMessage(), se);
        } finally {
            /*
             * Close the statment after execution.
             */
            closeStatement(cs);
        }
        return ret;
    }   

    private boolean isNullXML(String xml)
    {
	if (xml.length() <= 255) 
        {
	    if ((xml.length() == 0) ||
	       (xml.equals("<ROWSET/>")) ||
	       (xml.equals("<ROWSET></ROWSET>")) ||
	       (xml.equals("<ROW/>")) ||
	       (xml.equals("<ROW></ROW>")) ||
	       (xml.equals("<ROWSET><ROW></ROW></ROWSET>"))) 
	        return true;
        }
        return false;  
    }

    /**
     * Execute one of those predefined callable SQL statement for create,
     * update, and delete.
     *
     * @param	sql			the SQL statement, in readable form
     * @param	xml			the XML document containing the data
     *
     * @throws	DBException
     */
    private int execute(String sql, String xml)
	throws DBException
    {
	if (sql == null)
	{
	    throw new DBException("No SQL statement");
	}
	if (xml == null)
	{
	    throw new DBException("No XML document");
	}
	
	CallableStatement	call = prepareCall(sql);
	int ret = -1;

	try
	{
	    call.registerOutParameter(1, Types.NUMERIC);
	    call.setString(2, xml);
	    call.setString(3, getTableName());
	    call.executeUpdate();

            ret = call.getInt(1);
	}
	catch (SQLException se)
	{
	    throw new DBException("SQL exception: " + se.getMessage(), se);
	}
	finally
	{
	    /*
	     * Close the statment after execution.
	     */
	    closeStatement(call);
	}
        return ret;
    }

    /**
     * Executes the prepared query statement.
     *<p>
     * The purpose of this method is to hide the SQLException from the
     * subclasses.
     *
     * @param	statemtent		the SQL statement to be executed
     *
     * @throws	DBException
     */
    protected ResultSet executeQuery(PreparedStatement statement)
	throws DBException
    {
	/*
	 * Execute the query.
	 */
	try
	{
	    return statement.executeQuery();
	}
	catch (SQLException se)
	{
	    se.printStackTrace();
	    throw new DBException("Fail to execute query", se);
	}
    }

    /**
     * Closes the given statement, but ignore any exception.
     *
     * @param	statement		the statement to be closed
     */
    protected final void closeStatement(Statement statement)
    {
	if (statement != null)
	{
	    try
	    {
		statement.close();
	    }
	    catch (SQLException se)
	    {
		se.printStackTrace();
	    }
	}
    }

    /**
     * The <c>ThreadLocalConnection</c> class manages the database connection
     * on a per-thread basis.
     *<p>
     * When calling the newConnection method, it gets a database connection
     * from the connection pool and memorizes it for the current thread.
     *<p>
     * The corresponding clearConnection method forgets the database
     * connection for the current thread, and returns it to the conneciton
     * pool. The caller is responsible for calling clearConnection when
     * done.
     */
    private static class ThreadLocalConnection
	extends ThreadLocal
    {
	/**
	 * Sets the initial value to this ThreadLocalConnection instance.
	 *
	 * @return	The initial value.
	 */
	public Object initialValue()
	{
	    /*
	     * No initial value.
	     */
	    return null;
	}

	/**
	 * Gets a newly obtained connection.
	 *
	 * @param	dataSource	the data source
	 * @param	priority	connection pool access priority.
	 * @see		DataSourceScheduler
	 *
	 * @return	The newly obtained connection.
	 *
	 * @throws	DBException
	 */
	private Connection newConnection(DataSource dataSource, int priority)
	    throws DBException
	{
	    /*
	     * Get a connection from the connection pool.
	     */
	    Connection	connection = (Connection)get();

	    if (connection != null)
	    {
		throw new AlreadyInTransactionException();
	    }
	    try
	    {
		if (dataSource instanceof OracleDataSource.OracleDataSourceProxy)
		    connection = ((OracleDataSource.OracleDataSourceProxy) dataSource).getConnection(priority);
		else
		    connection = dataSource.getConnection();
	    }
	    catch (SQLException se)
	    {
		se.printStackTrace();
		throw new DBException("Cannot get Connection from pool", se);
	    }
	    /*
	     * Remember it.
	     */
	    set(connection);
	    return connection;
	}

	/**
	 * Clears the connection from ThreadLocal.
	 *
	 * @throws	DBException
	 */
	private void clearConnection()
	    throws DBException
	{
	    clearConnection((Connection)get());
	}

	/**
	 * Clears the connection from ThreadLocal.
	 *
	 * @throws	DBException
	 */
	private void clearConnection(Connection connection)
	    throws DBException
	{
	    if (connection == null)
	    {
		throw new NotInTransactionException();
	    }
	    try
	    {
		/*
		 * Forget it.
		 */
		set(null);
	    }
	    finally
	    {
		/*
		 * Return the connection back to the connection pool.
		 */
		try
		{
		    connection.close();
		}
		catch (SQLException se)
		{
		    /*
		     * Ignore close error, just report it.
		     */
		    se.printStackTrace();
		}
	    }
	}
    }
}


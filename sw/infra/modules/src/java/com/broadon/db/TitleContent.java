package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <c>TitleContent</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *<p>
 * It contains information about title content relationship, identified by
 * the title identifier and the content identifier.
 *
 * @version	$Revision: 1.3 $
 */
public class TitleContent
    extends PBean
{
    private Long		titleID;
    private Long		contentID;
    private Date		createDate;
    private Date		revokeDate;
    private Date		lastUpdated;

    /**
     * Constructs an empty TitleContent instance.
     */
    public TitleContent()
    {
	this.titleID = null;
	this.contentID = null;
	this.createDate = null;
	this.revokeDate = null;
	this.lastUpdated = null;
    }

    /**
     * Getter for titleID.
     *
     * @return	The title identifier.
     */
    public Long getTitleID()
    {
	return titleID;
    }

    /**
     * Setter for titleID.
     *
     * @param	titleID			the title identifier
     */
    public void setTitleID(Long titleID)
    {
	this.titleID = titleID;
    }

    /**
     * Getter for contentID.
     *
     * @return	The content identifier.
     */
    public Long getContentID()
    {
	return contentID;
    }

    /**
     * Setter for contentID.
     *
     * @param	contentID		the content identifier
     */
    public void setContentID(Long contentID)
    {
	this.contentID = contentID;
    }

    /**
     * Getter for createDate.
     *
     * @return	The creation date of this title-content.
     */
    public Date getCreateDate()
    {
	return createDate;
    }

    /**
     * Setter for createDate.
     *
     * @param	createDate		the creation date of this title-content
     */
    public void setCreateDate(Date createDate)
    {
	this.createDate = createDate;
    }

    /**
     * Getter for revoke date.
     *
     * @return	The revoke date of this content from this title.
     */
    public Date getRevokeDate()
    {
	return revokeDate;
    }

    /**
     * Setter for revoke date.
     *
     * @param	revokeDate		the revoke date of this content from
     *					this title
     */
    public void setRevokeDate(Date revokeDate)
    {
	this.revokeDate = revokeDate;
    }

    /**
     * Getter for lastUpdated.
     *
     * @return	The last updated date of this title-content.
     */
    public Date getLastUpdated()
    {
	return lastUpdated;
    }

    /**
     * Setter for lastUpdated.
     *
     * @param	lastUpdated		the last updated date of this
     *					title-content
     */
    public void setLastUpdated(Date lastUpdated)
    {
	this.lastUpdated = lastUpdated;
    }
}

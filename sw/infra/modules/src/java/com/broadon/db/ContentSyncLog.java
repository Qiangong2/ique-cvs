package com.broadon.db;

import java.util.Date;

/**
 * The <c>ContentSyncLog</c> class conforms to the JavaBeans property,
 * and is mainly used for data transformation.
 *<p>
 * It contains information about a content sync log identified by the
 * depot identifier and the request date.
 *
 * @version	$Revision: 1.1 $
 */
public class ContentSyncLog
    extends AuditLog
{
    private Long		depotID;
    private Date		cdsTimestamp;
    private Date		newCdsTimestamp;

    /**
     * Constructs an empty ContentSyncLog instance.
     */
    public ContentSyncLog()
    {
	super();
    }

    /**
     * Getter for depotID.
     *
     * @return	The depot identifier.
     */
    public Long getDepotID()
    {
	return depotID;
    }

    /**
     * Setter for depotID.
     *
     * @param	depotID			the depot identifier
     */
    public void setDepotID(Long depotID)
    {
	this.depotID = depotID;
    }

    /**
     * Getter for cdsTimestamp.
     *
     * @return	The starting timestamp for syncing.
     */
    public Date getCdsTimestamp()
    {
	return cdsTimestamp;
    }

    /**
     * Setter for cdsTimestamp.
     *
     * @param	cdsTimestamp		the starting timestamp for syncing
     */
    public void setCdsTimestamp(Date cdsTimestamp)
    {
	this.cdsTimestamp = cdsTimestamp;
    }

    /**
     * Getter for newCdsTimestamp.
     *
     * @return	The ending timestamp for syncing.
     */
    public Date getNewCdsTimestamp()
    {
	return newCdsTimestamp;
    }

    /**
     * Setter for newCdsTimestamp.
     *
     * @param	newCdsTimestamp		the ending timestamp for syncing
     */
    public void setNewCdsTimestamp(Date newCdsTimestamp)
    {
	this.newCdsTimestamp = newCdsTimestamp;
    }
}

package com.broadon.db;

import java.io.IOException;
import java.util.Map;

import com.broadon.bean.Bean;

/**
 * The <code>RegionFactory</code> class interfaces with the database
 * to create, update, or query records from the Regions table.
 * Each record is represented by a Region JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.5 $
 */
public class RegionFactory
    extends DBAccessFactory
{
    private static final String TABLE_NAME;
    private static final String	SELECT_REGION_NAMES;
    private static final String	SELECT_REGION_LIST;
    private static final String	SELECT_REGION_COUNT;

    static
    {
	TABLE_NAME		= "REGIONS";
	SELECT_REGION_NAMES	=
	    "select rownum no, region_id, region_name, bu_id from " + TABLE_NAME +
		" order by region_name";
	SELECT_REGION_LIST	=
	    "select r.region_id, r.region_name, r.create_date, r.revoke_date, " +
		"r.suspend_date, r.bu_id, bu.business_name from " + TABLE_NAME +
                " r, BUSINESS_UNITS bu WHERE r.bu_id = bu.bu_id";
	SELECT_REGION_COUNT	= "select count(region_id) from " + TABLE_NAME;
    }

    /**
     * Constructs a RegionFactory instance.
     */
    protected RegionFactory()
    {
	super();
    }

    /**
     * Returns the region identified by the given region identifier.
     *
     * @param	regionID		the region identifier
     * @param	nameMap			the name mapping table
     *
     * @return	The Region JavaBeans that contains information of a
     *		Regional-Centers record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public Region getRegion(int regionID, Map nameMap)
	throws DBException, IOException
    {
	return (Region)queryUnique(new RegionID(regionID), nameMap);
    }

    /**
     * Returns the region information of a given range.
     *
     * @param	from			the starting record number
     * @param	to			the ending record number
     * @param	order			the sorted order
     *
     * @return	The XML document that contains the region information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getRegions(int from, int to, String order)
	throws DBException, IOException
    {
	String	sql = "select * from (select rownum no, inner.* from (" +
			SELECT_REGION_LIST +
			    " order by " + order + ") " +
			"inner where rownum <= " + to + ") " +
			"where no > " + from;

	return query(sql);
    }

    /**
     * Returns the region identifier and region names.
     *
     * @return	The XML document that contains information of Region records.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getRegionNames()
	throws DBException, IOException
    {
	return query(SELECT_REGION_NAMES);
    }

   /**
     * Returns the region details with the given id.
     *
     * @param   regionID                 the region identifier
     *
     * @return The XML document describing the information of region
     *          information.
     *
     * @throws  DBException
     * @throws  IOException
     */
    public String getRegion(String regionID)
        throws DBException, IOException
    {
        String  sql = SELECT_REGION_LIST +
                        " and r.region_id = " + regionID;
        return query(sql);
    }

    /**
     * Returns the number of regions available.
     *
     * @param	regionID		the region identifier
     *
     * @return	The integer that contains the number of regions.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getRegionCount(long regionID)
	throws DBException, IOException
    {
	return count(SELECT_REGION_COUNT + " where region_id = " + regionID);
    }

    /**
     * Returns the number of regions available.
     *
     * @param	regionID		the region identifier
     *
     * @return	The integer that contains the number of regions.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getRegionCount(String regionID)
	throws DBException, IOException
    {
	return count(SELECT_REGION_COUNT + " where region_id = " + regionID);
    }

    /**
     * Returns the number of regions available.
     *
     * @return	The integer document that contains the number of regions.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getRegionCount()
	throws DBException, IOException
    {
	return count(SELECT_REGION_COUNT);
    }

    /**
     * Returns the database table name to be used.
     *
     * @return	The data table name.
     */
    public String getTableName()
    {
	return TABLE_NAME;
    }

    /**
     * Creates a Region JavaBeans object.
     *
     * @return	The newly created Region object.
     */
    protected Bean newBean()
    {
	return new Region();
    }
}

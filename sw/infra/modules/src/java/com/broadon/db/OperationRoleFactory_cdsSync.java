package com.broadon.db;

import com.broadon.bean.Bean;
import com.broadon.util.VersionNumber;

/**
 * The <c>OperationRoleFactory_cdsSync</c> class interfaces with the database
 * to create, update, or query records from the operation_roles table.
 * Each record is represented by a OperationRoles_cdsSync JavaBeans instance.
 * This is a special case of the OperationRoleFactory used by the CDS server.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.3 $
 */
public class OperationRoleFactory_cdsSync
    extends SyncableFactory
{
    private static final String TABLE_NAME = "OPERATION_ROLES";
    private static final String CREATE_DATE = "create_date";
    private static final VersionNumber VERSION_1_2_0 = VersionNumber.valueOf("1.2.0");
    private static final String SELECT_PREFIX = "select role_level, role_name, description, is_active, create_date last_updated from ";

    /**
     * Constructs a OperationRoleFactory_cdsSync instance.
     */
    protected OperationRoleFactory_cdsSync()
    {
        super();
    }

    /**
     * Returns the field representing the last time a DB row was updated
     * for this DB table.  Can optionally be redefined for subclasses.
     *
     * @return	name of DB column.
     */
    protected String lastUpdateTimeNm() {return CREATE_DATE;}

    /**
     * The records produced by this factory is only supported in depot
     * schema versions 1.2.0 and later.
     *
     * @param	depotSchemaVersion the schema version used by the depot
     * @return	true if the depot supports the record types; otherwise false.
     */
    public boolean inDepotSchemaVersion(VersionNumber depotSchemaVersion)
    {
        return (depotSchemaVersion.compareTo(VERSION_1_2_0) >= 0);
    }

    /**
     * Returns the database table name to be used.
     *
     * @return	The data table name.
     */
    public String getTableName()
    {
        return TABLE_NAME;
    }

    /**
     * Creates a OperationRole_cdsSync JavaBeans object.
     *
     * @return	The newly created OperationUser_cdsSync object.
     */
    protected Bean newBean()
    {
        return new OperationRole_cdsSync();
    }

    /**
     * Returns the select part of the query.  We need to alias the create_date
     * to last_updated.  Make sure this is always in sync with the members in
     * OperationRole_cdsSync and the corresponding mappings in BBname.map.
     */
    protected String getSelectPart()
    {
        return SELECT_PREFIX + getTableName();
    }

}

package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <c>Smartcard</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *<p>
 * It contains information about operation users.
 *
 * @version	$Revision: 1.2 $
 */
public class Smartcard
    extends PBean
{
    private Long		smartcardID;
    private Long		ownerID;
    private String		ownerType;
    private Date		revokeDate;

    /**
     * Constructs an empty Smartcard instance.
     */
    public Smartcard()
    {
    }

    /**
     * Getter for smartcardID.
     *
     * @return	The smartcard identifier.
     */
    public Long getSmartcardID()
    {
	return smartcardID;
    }

    /**
     * Setter for smartcardID.
     *
     * @param	smartcardID		the smartcard identifier
     */
    public void setSmartcardID(Long smartcardID)
    {
	this.smartcardID = smartcardID;
    }

    /**
     * Getter for ownerID.
     *
     * @return	The owner identifier.
     */
    public Long getOwnerID()
    {
	return ownerID;
    }

    /**
     * Setter for ownerID.
     *
     * @param	ownerID		the owner identifier
     */
    public void setOwnerID(Long ownerID)
    {
	this.ownerID = ownerID;
    }

    /**
     * Getter for ownerType.
     *
     * @return	The owner type.
     */
    public String getOwnerType()
    {
	return ownerType;
    }

    /**
     * Setter for ownerType.
     *
     * @param	ownerType		the owner type
     */
    public void setOwnerType(String ownerType)
    {
	this.ownerType = ownerType;
    }

    /**
     * Getter for revokeDate.
     *
     * @return	The revoke change date for an smartcard.
     */
    public Date getRevokeDate()
    {
	return revokeDate;
    }

    /**
     * Setter for revokeDate.
     *
     * @param	revokeDate		the revoke date
     */
    public void setRevokeDate(Date revokeDate)
    {
	this.revokeDate = revokeDate;
    }
}

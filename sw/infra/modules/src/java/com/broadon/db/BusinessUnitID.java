package com.broadon.db;

import com.broadon.sql.PredicateItem;

/**
 * The <c>BusinessUnitID</c> class conforms to the JavaBeans property,
 * and is a Predicate that finds various records.
 *
 * @version	$Revision: 1.1 $
 */
public class BusinessUnitID
    extends PredicateItem
{
    private Integer	businessUnitID;

    /**
     * Constructs a BusinessUnitID instance.
     *
     * @param	businessUnitID		the business unit identifier
     */
    public BusinessUnitID(int businessUnitID)
    {
	super();
	this.businessUnitID = new Integer(businessUnitID);
    }

    /**
     * Getter for businessUnitID.
     *
     * @return	The business unit identifier.
     */
    public Integer getBusinessUnitID()
    {
	return businessUnitID;
    }

    /**
     * Setter for businessUnitID.
     *
     * @param	businessUnitID		the business unit identifier
     */
    public void setBusinessUnitID(Integer businessUnitID)
    {
	this.businessUnitID = businessUnitID;
    }
}

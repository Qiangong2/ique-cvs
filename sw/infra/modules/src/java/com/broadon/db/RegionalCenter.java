package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <code>RegionalCenter</code> class conforms to the JavaBeans property,
 * and is mainly used for data transformation.
 *<p>
 * It contains information about a regional center, identified by the
 * regional center identifier.
 *
 * @version	$Revision: 1.4 $
 */
public class RegionalCenter
    extends PBean
{
    private Integer		regionalCenterID;
    private String		centerLocation;
    private String		centerName;
    private Integer		hsmConfigID;
    private Integer		isCurrent;
    private Date		lastUpdated;

    /**
     * Constructs an empty RegionalCenter instance.
     */
    public RegionalCenter()
    {
    }

    /**
     * Getter for regionalCenterID.
     *
     * @return	The regional center identifier.
     */
    public Integer getRegionalCenterID()
    {
	return regionalCenterID;
    }

    /**
     * Setter for regionalCenterID.
     *
     * @param	regionalCenterID	the regional center identifier
     */
    public void setRegionalCenterID(Integer regionalCenterID)
    {
	this.regionalCenterID = regionalCenterID;
    }

    /**
     * Getter for centerLocation.
     *
     * @return	The center location.
     */
    public String getCenterLocation()
    {
	return centerLocation;
    }

    /**
     * Setter for centerLocation.
     *
     * @param	centerLocation		the center location
     */
    public void setCenterLocation(String centerLocation)
    {
	this.centerLocation = centerLocation;
    }

    /**
     * Getter for centerName.
     *
     * @return	The name of this regional center.
     */
    public String getCenterName()
    {
	return centerName;
    }

    /**
     * Setter for centerName.
     *
     * @param	centerName		the name of this regional center
     */
    public void setCenterName(String centerName)
    {
	this.centerName = centerName;
    }

    /**
     * Getter for hsmConfigID.
     *
     * @return	The HSM configuration identifier.
     */
    public Integer getHsmConfigID()
    {
	return hsmConfigID;
    }

    /**
     * Setter for hsmConfigID.
     *
     * @param	hsmConfigID		the HSM configuration identifier
     */
    public void setHsmConfigID(Integer hsmConfigID)
    {
	this.hsmConfigID = hsmConfigID;
    }

    /**
     * Getter for isCurrent.
     *
     * @return	1 => current; 0 => not current
     */
    public Integer getIsCurrent()
    {
	return isCurrent;
    }

    /**
     * Setter for isCurrent.
     *
     * @param	isCurrent		1 => current; 0 => not current
     */
    public void setIsCurrent(Integer isCurrent)
    {
	this.isCurrent = isCurrent;
    }

    /**
     * Getter for lastUpdated.
     *
     * @return	The last updated date of this center.
     */
    public Date getLastUpdated()
    {
	return lastUpdated;
    }

    /**
     * Setter for lastUpdated.
     *
     * @param	lastUpdated		the last updated date of this center
     */
    public void setLastUpdated(Date lastUpdated)
    {
	this.lastUpdated = lastUpdated;
    }
}

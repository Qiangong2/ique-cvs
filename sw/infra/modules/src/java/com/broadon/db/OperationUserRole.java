package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <c>OperationUserRole</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *<p>
 * It contains information about operation user roles.
 *
 * @version	$Revision: 1.3 $
 */
public class OperationUserRole
    extends PBean
{
    private Long		operatorID;
    private Integer		roleLevel;
    private Integer		businessUnitID;
    private Integer		isActive;
    private Date		grantDate;
    private Date		lastUpdated;

    /**
     * Constructs an empty OperationUserRole instance.
     */
    public OperationUserRole()
    {
    }

    /**
     * Getter for operatorID.
     *
     * @return	The operator identifier.
     */
    public Long getOperatorID()
    {
	return operatorID;
    }

    /**
     * Setter for operatorID.
     *
     * @param	operatorID		the operator identifier
     */
    public void setOperatorID(Long operatorID)
    {
	this.operatorID = operatorID;
    }

    /**
     * Getter for roleLevel.
     *
     * @return	The default role level.
     */
    public Integer getRoleLevel()
    {
	return roleLevel;
    }

    /**
     * Setter for roleLevel.
     *
     * @param	roleLevel		the default role level
     */
    public void setRoleLevel(Integer roleLevel)
    {
	this.roleLevel = roleLevel;
    }

    /**
     * Getter for businessUnitID.
     *
     * @return	The default role level.
     */
    public Integer getBusinessUnitID()
    {
	return businessUnitID;
    }

    /**
     * Setter for businessUnitID.
     *
     * @param	businessUnitID		the default role level
     */
    public void setBusinessUnitID(Integer businessUnitID)
    {
	this.businessUnitID = businessUnitID;
    }


    /**
     * Getter for isActive.
     *
     * @return	The is active flag.
     */
    public Integer getIsActive()
    {
	return isActive;
    }

    /**
     * Setter for isActive.
     *
     * @param	isActive		the is active flag
     */
    public void setIsActive(Integer isActive)
    {
	this.isActive = isActive;
    }

    /**
     * Getter for grantDate.
     *
     * @return	The grant date of this user role.
     */
    public Date getGrantDate()
    {
	return grantDate;
    }

    /**
     * Setter for grantDate.
     *
     * @param	grantDate		the grant date
     */
    public void setGrantDate(Date grantDate)
    {
	this.grantDate = grantDate;
    }

    /**
     * Getter for lastUpdated.
     *
     * @return	The last updated date.
     */
    public Date getLastUpdated()
    {
	return lastUpdated;
    }

    /**
     * Setter for lastUpdated.
     *
     * @param	lastUpdated		the last updated date
     */
    public void setLastUpdated(Date lastUpdated)
    {
	this.lastUpdated = lastUpdated;
    }
}

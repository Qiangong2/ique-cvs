package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <c>Title</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *<p>
 * It contains information about title the title of a content or collection
 * of contents, identified by the title identifier.
 *
 * @version	$Revision: 1.2 $
 */
public class Title
    extends PBean
{
    private Long		titleID;
    private String		title;
    private String		titleType;
    private String		category;
    private String		publisher;
    private String		developer;
    private String		description;
    private String		features;
    private String		expandedInfo;
    private Date		revokeDate;
    private Date		lastUpdated;

    /**
     * Constructs an empty Title instance.
     */
    public Title()
    {
    }

    /**
     * Getter for titleID.
     *
     * @return	The title identifier.
     */
    public Long getTitleID()
    {
	return titleID;
    }

    /**
     * Setter for titleID.
     *
     * @param	titleID			the title identifier
     */
    public void setTitleID(Long titleID)
    {
	this.titleID = titleID;
    }

    /**
     * Getter for title.
     *
     * @return	The title.
     */
    public String getTitle()
    {
	return title;
    }

    /**
     * Setter for title.
     *
     * @param	title			the title
     */
    public void setTitle(String title)
    {
	this.title = title;
    }

    /**
     * Getter for titleType.
     *
     * @return	The type of this title.
     */
    public String getTitleType()
    {
	return titleType;
    }

    /**
     * Setter for titleType.
     *
     * @param	titleType		the type of this title
     */
    public void setTitleType(String titleType)
    {
	this.titleType = titleType;
    }

    /**
     * Getter for category.
     *
     * @return	The category of this title.
     */
    public String getCategory()
    {
	return category;
    }

    /**
     * Setter for category.
     *
     * @param	category		the category of this title
     */
    public void setCategory(String category)
    {
	this.category = category;
    }

    /**
     * Getter for publisher.
     *
     * @return	The publisher of this title.
     */
    public String getPublisher()
    {
	return publisher;
    }

    /**
     * Setter for publisher.
     *
     * @param	publisher		the publisher of this title
     */
    public void setPublisher(String publisher)
    {
	this.publisher = publisher;
    }

    /**
     * Getter for developer.
     *
     * @return	The developer of this title.
     */
    public String getDeveloper()
    {
	return developer;
    }

    /**
     * Setter for developer.
     *
     * @param	developer		the developer of this title
     */
    public void setDeveloper(String developer)
    {
	this.developer = developer;
    }

    /**
     * Getter for description.
     *
     * @return	The description of this title.
     */
    public String getDescription()
    {
	return description;
    }

    /**
     * Setter for description.
     *
     * @param	description		the description of this title
     */
    public void setDescription(String description)
    {
	this.description = description;
    }

    /**
     * Getter for features.
     *
     * @return	The features of this title.
     */
    public String getFeatures()
    {
	return features;
    }

    /**
     * Setter for features.
     *
     * @param	features		the features of this title
     */
    public void setFeatures(String features)
    {
	this.features = features;
    }

    /**
     * Getter for expandedInfo.
     *
     * @return	The expanded information of this title.
     */
    public String getExpandedInfo()
    {
	return expandedInfo;
    }

    /**
     * Setter for expandedInfo.
     *
     * @param	expandedInfo		the expanded information of this title
     */
    public void setExpandedInfo(String expandedInfo)
    {
	this.expandedInfo = expandedInfo;
    }

    /**
     * Getter for revokeDate.
     *
     * @return	The revoke date of this title.
     */
    public Date getRevokeDate()
    {
	return revokeDate;
    }

    /**
     * Setter for revokeDate.
     *
     * @param	revokeDate		the revoke date of this title
     */
    public void setRevokeDate(Date revokeDate)
    {
	this.revokeDate = revokeDate;
    }

    /**
     * Getter for lastUpdated.
     *
     * @return	The last updated date of this title.
     */
    public Date getLastUpdated()
    {
	return lastUpdated;
    }

    /**
     * Setter for lastUpdated.
     *
     * @param	lastUpdated		the last updated date of this title
     */
    public void setLastUpdated(Date lastUpdated)
    {
	this.lastUpdated = lastUpdated;
    }
}

package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <c>TitleRegion</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *<p>
 * It contains regional title information.
 *
 * @version	$Revision: 1.3 $
 */
public class TitleRegion
    extends PBean
{
    private Long		titleID;
    private Integer		regionID;
    private Date		purchaseStartDate;
    private Date		purchaseEndDate;
    private Integer		eUnits;
    private String		rType;
    private Integer		limits;
    private Integer		initLReUnits;
    private Integer		nextLReUnits;
    private Integer		bonusLimits;
    private Integer		trialLimits;
    private Date		lastUpdated;

    /**
     * Constructs an empty TitleRegion instance.
     */
    public TitleRegion()
    {
    }

    /**
     * Getter for titleID.
     *
     * @return	The title identifier.
     */
    public Long getTitleID()
    {
	return titleID;
    }

    /**
     * Setter for titleID.
     *
     * @param	titleID			the title identifier
     */
    public void setTitleID(Long titleID)
    {
	this.titleID = titleID;
    }

    /**
     * Getter for regionID.
     *
     * @return	The region identifier.
     */
    public Integer getRegionID()
    {
	return regionID;
    }

    /**
     * Setter for regionID.
     *
     * @param	regionID		the region identifier
     */
    public void setRegionID(Integer regionID)
    {
	this.regionID = regionID;
    }

    /**
     * Getter for purchaseStartDate.
     *
     * @return	The first available date for purchasing this title.
     */
    public Date getPurchaseStartDate()
    {
	return purchaseStartDate;
    }

    /**
     * Setter for purchaseStartDate.
     *
     * @param	purchaseStartDate	the first available date for purchasing
     *					this title
     */
    public void setPurchaseStartDate(Date purchaseStartDate)
    {
	this.purchaseStartDate = purchaseStartDate;
    }

    /**
     * Getter for purchaseEndDate.
     *
     * @return	The last available date for purchasing this title.
     */
    public Date getPurchaseEndDate()
    {
	return purchaseEndDate;
    }

    /**
     * Setter for purchaseEndDate.
     *
     * @param	purchaseEndDate		the last available date for purchasing
     *					this title
     */
    public void setPurchaseEndDate(Date purchaseEndDate)
    {
	this.purchaseEndDate = purchaseEndDate;
    }

    /**
     * Getter for eUnits.
     *
     * @return	The eUnits required to purchase this title.
     */
    public Integer getEUnits()
    {
	return eUnits;
    }

    /**
     * Setter for eUnits.
     *
     * @param	eUnits			the eUnits required to purchase this
     *					title
     */
    public void setEUnits(Integer eUnits)
    {
	this.eUnits = eUnits;
    }

    /**
     * Getter for rType.
     *
     * @return	The limited right type of this title.
     */
    public String getRType()
    {
	return rType;
    }

    /**
     * Setter for rType.
     *
     * @param	rType			the limited right type of this title
     */
    public void setRType(String rType)
    {
	this.rType = rType;
    }

    /**
     * Getter for limits.
     *
     * @return	The limited play units of this title.
     */
    public Integer getLimits()
    {
	return limits;
    }

    /**
     * Setter for limits.
     *
     * @param	limits			the limited play units of this title
     */
    public void setLimits(Integer limits)
    {
	this.limits = limits;
    }

    /**
     * Getter for initLReUnits.
     *
     * @return	The initial limited right price to purchase this title.
     */
    public Integer getInitLReUnits()
    {
	return initLReUnits;
    }

    /**
     * Setter for initLReUnits.
     *
     * @param	initLReUnits		the initial limited right price to
     *					purchase this title
     */
    public void setInitLReUnits(Integer initLReUnits)
    {
	this.initLReUnits = initLReUnits;
    }

    /**
     * Getter for nextLReUnits.
     *
     * @return	The additional limited right price to purchase this title.
     */
    public Integer getNextLReUnits()
    {
	return nextLReUnits;
    }

    /**
     * Setter for nextLReUnits.
     *
     * @param	nextLReUnits		the additional limited right price
     *					to purchase this title
     */
    public void setNextLReUnits(Integer nextLReUnits)
    {
	this.nextLReUnits = nextLReUnits;
    }

    /**
     * Getter for bonusLimits.
     *
     * @return	The limited time this title can be used as a bonus.
     */
    public Integer getBonusLimits()
    {
        return bonusLimits;
    }

    /**
     * Setter for bonusLimits.
     *
     * @param	bonusLimits the limited time this title can be used as a bonus.
     */
    public void setBonusLimits(Integer bonusLimits)
    {
        this.bonusLimits = bonusLimits;
    }

    /**
     * Getter for trialLimits.
     *
     * @return	The limited time this title can be used as a trial.
     */
    public Integer getTrialLimits()
    {
        return trialLimits;
    }

    /**
     * Setter for trialLimits.
     *
     * @param	trialLimits the limited time this title can be used as a trial.
     */
    public void setTrialLimits(Integer trialLimits)
    {
        this.trialLimits = trialLimits;
    }

    /**
     * Getter for lastUpdated.
     *
     * @return	The last updated date.
     */
    public Date getLastUpdated()
    {
	return lastUpdated;
    }

    /**
     * Setter for lastUpdated.
     *
     * @param	lastUpdated		the last updated date
     */
    public void setLastUpdated(Date lastUpdated)
    {
	this.lastUpdated = lastUpdated;
    }
}

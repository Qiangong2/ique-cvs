package com.broadon.db;

import java.io.IOException;
import java.util.Map;

import com.broadon.bean.Bean;

/**
 * The <code>RetailerFactory</code> class interfaces with the database
 * to create, update, or query records from the Retailers table.
 * Each record is represented by a Retailer JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.3 $
 */
public class RetailerFactory
    extends DBAccessFactory
{
    private static final String TABLE_NAME;
    private static final String	SELECT_RETAILER_NAMES;
    private static final String	SELECT_RETAILER_LIST;
    private static final String	SELECT_RETAILER_COUNT;
    private static final String	SELECT_RETAILER_CATEGORIES_LIST;

    static
    {
	TABLE_NAME		= "RETAILERS";
	SELECT_RETAILER_CATEGORIES_LIST	=
	    "select category_code, category_name, create_date from RETAILER_CATEGORIES" +
		" order by category_name";
	SELECT_RETAILER_NAMES	=
	    "select rownum no, retailer_id, retailer_name from " + TABLE_NAME +
		" order by retailer_name";
	SELECT_RETAILER_LIST	=
            "select sum(decode(s.store_id, null, 0, 1)) total_stores, rl.retailer_id, rl.retailer_name," +
                " rl.create_date, rl.revoke_date, rl.suspend_date, rc.category_name, rl.category_code" +
                " FROM " + TABLE_NAME + " rl, RETAILER_CATEGORIES rc, STORES s" +
                " WHERE rc.category_code (+) = rl.category_code AND rl.retailer_id = s.retailer_id (+)";
	SELECT_RETAILER_COUNT	= "select count(retailer_id) from " + TABLE_NAME;
    }

    /**
     * Constructs a RetailerFactory instance.
     */
    protected RetailerFactory()
    {
	super();
    }

    /**
     * Returns the retailer identified by the given retailer identifier.
     *
     * @param	retailerID		the retailer identifier
     * @param	nameMap			the name mapping table
     *
     * @return	The Retailer JavaBeans that contains information of a
     *		Retaileral-Centers record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public Retailer getRetailer(int retailerID, Map nameMap)
	throws DBException, IOException
    {
	return (Retailer)queryUnique(new RetailerID(retailerID), nameMap);
    }

    /**
     * Returns the retailer information of a given range.
     *
     * @param	from			the starting record number
     * @param	to			the ending record number
     * @param	order			the sorted order
     *
     * @return	The XML document that contains the retailer information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getRetailers(int from, int to, String order)
	throws DBException, IOException
    {
	String	sql = "select * from (select rownum no, inner.* from (" + SELECT_RETAILER_LIST + 
                        " group by rl.retailer_id, rl.retailer_name, rl.create_date, rl.revoke_date," +
                        " rl.suspend_date, rc.category_name, rl.category_code order by " + order + 
			") inner where rownum <= " + to + ") where no > " + from;

	return query(sql);
    }

    /**
     * Returns the retailer categories list.
     *
     * @return	The XML document that contains information of Retailer Categories records.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getRetailerCategories()
	throws DBException, IOException
    {
	return query(SELECT_RETAILER_CATEGORIES_LIST);
    }

    /**
     * Returns the retailer identifier and retailer names.
     *
     * @return	The XML document that contains information of Retailer records.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getRetailerNames()
	throws DBException, IOException
    {
	return query(SELECT_RETAILER_NAMES);
    }

   /**
     * Returns the retailer details with the given id.
     *
     * @param   retailerID                 the retailer identifier
     *
     * @return The XML document describing the information of retailer
     *          information.
     *
     * @throws  DBException
     * @throws  IOException
     */
    public String getRetailer(String retailerID)
        throws DBException, IOException
    {
        String  sql = SELECT_RETAILER_LIST +
                        " and rl.retailer_id = " + retailerID +
                        " group by rl.retailer_id, rl.retailer_name," +
                        " rl.create_date, rl.revoke_date, rl.suspend_date," +
                        " rc.category_name, rl.category_code";
        return query(sql);
    }

    /**
     * Returns the number of retailers available.
     *
     * @param	retailerID		the retailer identifier
     *
     * @return	The integer that contains the number of retailers.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getRetailerCount(long retailerID)
	throws DBException, IOException
    {
	return count(SELECT_RETAILER_COUNT + " where retailer_id = " + retailerID);
    }

    /**
     * Returns the number of retailers available.
     *
     * @param	retailerID		the retailer identifier
     *
     * @return	The integer that contains the number of retailers.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getRetailerCount(String retailerID)
	throws DBException, IOException
    {
	return count(SELECT_RETAILER_COUNT + " where retailer_id = " + retailerID);
    }

    /**
     * Returns the number of retailers available.
     *
     * @return	The integer document that contains the number of retailers.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getRetailerCount()
	throws DBException, IOException
    {
	return count(SELECT_RETAILER_COUNT);
    }

    /**
     * Returns the database table name to be used.
     *
     * @return	The data table name.
     */
    public String getTableName()
    {
	return TABLE_NAME;
    }

    /**
     * Creates a Retailer JavaBeans object.
     *
     * @return	The newly created Retailer object.
     */
    protected Bean newBean()
    {
	return new Retailer();
    }
}

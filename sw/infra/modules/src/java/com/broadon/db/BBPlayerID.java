package com.broadon.db;

import com.broadon.sql.PredicateItem;

/**
 * The <c>BBPlayerID</c> class conforms to the JavaBeans property,
 * and is a Predicate that finds BBPlayer identified by the bb
 * identifier.
 *
 * @version	$Revision: 1.1 $
 */
public class BBPlayerID
    extends PredicateItem
{
    private Long	bbID;

    /**
     * Constructs a BBPlayerID type instance.
     *
     * @param	bbID		the bb identifier
     */
    public BBPlayerID(long bbID)
    {
	super();
	this.bbID = new Long(bbID);
    }

    /**
     * Getter for bbID.
     *
     * @return	The bb identifier.
     */
    public Long getBBPlayerID()
    {
	return bbID;
    }

    /**
     * Setter for bbID.
     *
     * @param	bbID		the bb identifier
     */
    public void setBBPlayerID(Long bbID)
    {
	this.bbID = bbID;
    }
}

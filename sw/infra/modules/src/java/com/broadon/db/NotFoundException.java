package com.broadon.db;

/**
 * The <code>NotFoundException</code> class indicates that
 * there is no record found for the given predicate.
 *
 * @version	$Revision: 1.1 $
 */
public class NotFoundException
    extends DBException
{
    /**
     * Constructs a NotFoundException instance.
     */
    public NotFoundException()
    {
	super();
    }

    /**
     * Constructs a NotFoundException instance.
     *
     * @param	message			the exception message
     */
    public NotFoundException(String message)
    {
	super(message);
    }

    /**
     * Constructs a NotFoundException instance.
     *
     * @param	message			the exception message
     * @param	throwable		the nested exception
     */
    public NotFoundException(String message, Throwable throwable)
    {
	super(message, throwable);
    }
}

package com.broadon.db;

import java.io.IOException;
import java.util.Map;

import com.broadon.bean.Bean;

/**
 * The <c>TimeFactory</c> class retrieves the database current time.
 * Each record is represented by a Time JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.4 $
 */
public class TimeFactory
    extends DBAccessFactory
{
    /**
     * Constructs a TimeFactory instance.
     */
    protected TimeFactory()
    {
	super();
    }

    /**
     * Returns the current database time in local-time.
     *
     * @param	nameMap			the name mapping table
     *
     * @return	The Time JavaBeans that contains information of the
     *		current database time.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public Time getTime(Map nameMap)
	throws DBException, IOException
    {
	Bean[]	beans = query(null, "select sysdate from dual", nameMap);

	if (beans == null)
	    return null;

	return (Time)beans[0];
    }

    /**
     * Returns the current database time in UTC time.
     *
     * @param	nameMap			the name mapping table
     *
     * @return	The Time JavaBeans that contains information of the
     *		current database time.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public Time getUtcTime(Map nameMap)
	throws DBException, IOException
    {
	Bean[]	beans = query(null, "select sys_extract_utc(current_timestamp) \"sysdate\" from dual", nameMap);

	if (beans == null)
	    return null;

	return (Time)beans[0];
    }

    /**
     * Returns the database table name to be used.
     *
     * @return	The data table name.
     */
    public String getTableName()
    {
	return "dual";
    }

    /**
     * Creates a Title JavaBeans object.
     *
     * @return	The newly created Title object.
     */
    protected Bean newBean()
    {
	return new Time();
    }
}

package com.broadon.db;

import com.broadon.sql.PredicateItem;

/**
 * The <c>BBModelID</c> class conforms to the JavaBeans property,
 * and is a Predicate that finds various records.
 *
 * @version	$Revision: 1.1 $
 */
public class BBModelID
    extends PredicateItem
{
    private String	bbModelID;

    /**
     * Constructs a BBModelID instance.
     *
     * @param	bbModelID		the bb model identifier
     */
    public BBModelID(String bbModelID)
    {
	super();
	this.bbModelID = new String(bbModelID);
    }

    /**
     * Getter for bbModelID.
     *
     * @return	The bb model identifier.
     */
    public String getBBModelID()
    {
	return bbModelID;
    }

    /**
     * Setter for bbModelID.
     *
     * @param	bbModelID		the bb model identifier
     */
    public void setBBModelID(String bbModelID)
    {
	this.bbModelID = bbModelID;
    }
}

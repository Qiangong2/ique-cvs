package com.broadon.db;


/**
 * The <c>ContentInfo</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *<p>
 * It extends the <c>ContentBase</c> class by adding the URI information.
 *
 * @version	$Revision: 1.3 $
 */
public class ContentInfo
    extends ContentBase
{
    private String		uri;

    /**
     * Constructs an empty ContentInfo instance.
     */
    public ContentInfo()
    {
	super();
	this.uri = null;
    }

    /**
     * Getter for URI.
     *
     * @return	The string representation of the URI.
     */
    public String getURI()
    {
	return uri;
    }

    /**
     * Setter for URI.
     *
     * @param	uri			the string representation of the URI
     */
    public void setURI(String uri)
    {
	this.uri = uri;
    }
}

package com.broadon.db;

import com.broadon.sql.PredicateItem;

/**
 * The <c>OperationRoleID</c> class conforms to the JavaBeans property,
 * and is a Predicate that finds various records.
 *
 * @version	$Revision: 1.1 $
 */
public class OperationRoleLevel
    extends PredicateItem
{
    private Integer	roleLevel;

    /**
     * Constructs a OperationRoleLevel instance.
     *
     * @param	roleLevel		the role level
     */
    public OperationRoleLevel(int roleLevel)
    {
	super();
	this.roleLevel = new Integer(roleLevel);
    }

    /**
     * Getter for roleLevel.
     *
     * @return	The role level
     */
    public Integer getRoleLevel()
    {
	return roleLevel;
    }

    /**
     * Setter for roleLevel.
     *
     * @param	roleLevel		the role level
     */
    public void setRoleLevel(Integer roleLevel)
    {
	this.roleLevel = roleLevel;
    }
}
package com.broadon.db;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import com.broadon.util.Base64;
import com.broadon.util.FileCache;
import com.broadon.util.HexString;


/**
 * The <c>ContentHeader</c> class represents information to accompany
 * any downloaded content object, and will contain the the following
 * values:
 *<p>
 *   8 bytes magicNumber: A fixed number 0x1c2c3c4c5c6c7c8cL
 *   4 bytes headerSize:  The size of this header when encoded as a byte array
 *   4 bytes chunkSize:   The recommended chunk bytes to use for downloads
 *   8 bytes contentSize: The size of the content object (excluding header)
 *   8 bytes contentID
 *  16 bytes contentChecksum
 *<p>
 * The encoding to binary form will encode all integral values in
 * netorder (big endian), such that the most significant byte precedes 
 * the least significant byte in the resultant byte array/stream.
 *
 * @version	$Revision: 1.2 $
 */
public class ContentHeader implements FileCache.KeySet
{
    public static final int CONTENT_ID_KEY = 0;
    public static final int CHECKSUM_KEY = 1;
    public static final int NUMBER_OF_KEYSETS = 2;

    private static final long MAGIC_NUMBER = 0x1c2c3c4c5c6c7c8cL;
    private static final int  CHUNK_SIZE = 1024*1024; // In units of bytes
    private static final int  HEADER_SIZE = 8 + 4 + 4 + 8 + 8 + 16;


    private long   contentSize;
    private long   contentID;
    private String contentID_key;
    private String checksum;
    private byte[] encoded;


    private void encode() throws IOException
    {
        ByteArrayOutputStream out = new ByteArrayOutputStream(HEADER_SIZE);
        DataOutputStream outd = new DataOutputStream(out);
        byte[]           binsum = HexString.fromHexString(checksum);
        
        outd.writeLong(MAGIC_NUMBER);
        outd.writeInt(HEADER_SIZE);
        outd.writeInt(CHUNK_SIZE);
        outd.writeLong(contentSize);
        outd.writeLong(contentID);
        outd.write(binsum, 0, binsum.length);
        outd.close();

        encoded = out.toByteArray();
        if (encoded.length != HEADER_SIZE) {
            throw new IOException(
                "Unexpected header size in ContentHeader encoding (" + 
                encoded.length + ")");
        }
    } // encode


    private void decode(DataInputStream inpd) throws IOException
    {
        if (inpd.readLong() != MAGIC_NUMBER)
            throw new IOException("Wrong magic number for ContentHeader");
        if (inpd.readInt() != HEADER_SIZE)
            throw new IOException("Unexpected header size for ContentHeader");
        if (inpd.readInt() != CHUNK_SIZE)
            throw new IOException("Unexpected chunk size for ContentHeader");

        contentSize = inpd.readLong();
        if (contentSize < 0)
            throw new IOException("Unexpected content size for ContentHeader");

        contentID = inpd.readLong();
        if (contentID <= 0)
            throw new IOException("Unexpected content id for ContentHeader");
        
        contentID_key = String.valueOf(contentID);

        byte[] csum = new byte[16];
        inpd.readFully(csum);
        checksum = HexString.toHexString(csum);
        encode();
    } // decode


    public ContentHeader(long   contentID, 
                         long   contentSize,
                         String hexChecksum) throws IOException
    {
        this.contentID = contentID;
        this.contentSize = contentSize;
        this.checksum = hexChecksum;
        contentID_key = String.valueOf(contentID);
        encode();
    }


    public ContentHeader(DataInputStream inp) throws IOException
    {
        decode(inp);
    }


    public String toString()
    {
        StringBuffer b = new StringBuffer(256);
        b.append("[id=");
        b.append(contentID_key);
        b.append(",csum=");
        b.append(checksum);
        b.append(",size=");
        b.append(String.valueOf(contentSize));
        b.append("]");
        return b.toString();
    }


    public void writeBytes(OutputStream out) throws IOException
    {
        out.write(encoded);
    }

    public void writeBase64(OutputStream out) throws IOException
    {
        Base64             base64 = new Base64();
        OutputStreamWriter outw = new OutputStreamWriter(out);
        base64.encode(encoded, outw);
    }


    public static int getHeaderSize() {return HEADER_SIZE;}
    public static int getChunkSize() {return CHUNK_SIZE;}
    public long getContentID() {return contentID;}
    public long getContentSize() {return contentSize;}
    public String getChecksum() {return checksum;}

    // Implement the FileCache.KeySet interface
    //
    public int  numSets() {return NUMBER_OF_KEYSETS;}
    public Object key(int i)
    {
        if (i == CONTENT_ID_KEY) return contentID_key;
        if (i == CHECKSUM_KEY) return checksum;
        else return null;
    }

} // class ContentHeader

package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <c>AuditLog</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *<p>
 * It contains common information about an audit log.
 *
 * @version	$Revision: 1.1 $
 */
public class AuditLog
    extends PBean
{
    public static final String	STATUS_OK	= "OK";
    public static final String	STATUS_FAILED	= "FAILED";

    private Date		requestDate;
    private String		requestLog;
    private String		requestStatus;

    /**
     * Constructs an empty AuditLog instance.
     */
    public AuditLog()
    {
    }

    /**
     * Getter for requestDate.
     *
     * @return	The date of the request.
     */
    public Date getRequestDate()
    {
	return requestDate;
    }

    /**
     * Setter for requestDate.
     *
     * @param	requestDate		the date of the request
     */
    public void setRequestDate(Date requestDate)
    {
	this.requestDate = requestDate;
    }

    /**
     * Getter for requestLog.
     *
     * @return	The log message of this audit log.
     */
    public String getRequestLog()
    {
	return requestLog;
    }

    /**
     * Setter for requestLog.
     *
     * @param	requestLog		the log message of this audit log
     */
    public void setRequestLog(String requestLog)
    {
	this.requestLog = requestLog;
    }

    /**
     * Getter for requestStatus.
     *
     * @return	The status of this audit log.
     */
    public String getRequestStatus()
    {
	return requestStatus;
    }

    /**
     * Setter for requestStatus.
     *
     * @param	requestStatus		the status of this audit log
     */
    public void setRequestStatus(String requestStatus)
    {
	this.requestStatus = requestStatus;
    }
}

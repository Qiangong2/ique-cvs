package com.broadon.db;

import java.io.IOException;

import com.broadon.bean.Bean;

/**
 * The <c>ECardsFactory</c> class interfaces with the database to
 * create, update, or query records from the ECards table.
 * Each record is represented by a ECards JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.8 $
 */
public class ECardsFactory
    extends SyncableFactory
{
    private static final String TABLE_NAME;
    private static final String	SELECT_ECARD_BATCH;
    private static final String	SELECT_BCC_ECARDS_LIST;
    private static final String	SELECT_BU_ECARDS_LIST;
    private static final String	SELECT_ECARDS_TO_REVOKE_LIST;
    private static final String	SELECT_ECARDS_TO_ACTIVATE_LIST;
    private static final String	SELECT_ECARDS_COUNT;

    static
    {
	TABLE_NAME		= "ECARDS";
	SELECT_ECARD_BATCH =
	    "SELECT eb.start_ecard_id, eb.end_ecard_id " +
            "FROM " + TABLE_NAME + " ec, ECARD_BATCHES eb WHERE " +
            "ec.ecard_id >= eb.start_ecard_id AND ec.ecard_id <= eb.end_ecard_id";
	SELECT_BCC_ECARDS_LIST =
	    "SELECT ec.ecard_id, ec.ecard_type, ec.bu_id, et.description, bu.business_name " +
            "FROM " + TABLE_NAME + " ec, ECARD_TYPES et, BUSINESS_UNITS bu WHERE " +
            "ec.ecard_type = et.ecard_type AND ec.bu_id = bu.bu_id";
	SELECT_BU_ECARDS_LIST =
	    "SELECT ec.ecard_id, ec.ecard_type, ec.bu_id, et.description, bu.business_name, " +
            "ec.eunits_redeemed, ec.is_usable, ec.activate_date, ec.last_redeem_date, ec.revoke_date " +
            "FROM " + TABLE_NAME + " ec, ECARD_TYPES et, BUSINESS_UNITS bu WHERE " +
            "ec.ecard_type = et.ecard_type AND ec.bu_id = bu.bu_id";
	SELECT_ECARDS_COUNT = "select count(*) from " + TABLE_NAME;
        SELECT_ECARDS_TO_REVOKE_LIST  =
            "select ecard_id, to_char(sys_extract_utc(current_timestamp), 'YYYY.MM.DD HH24:MI:SS') revoke_date from " + TABLE_NAME;
        SELECT_ECARDS_TO_ACTIVATE_LIST  =
            "select ecard_id, to_char(sys_extract_utc(current_timestamp), 'YYYY.MM.DD HH24:MI:SS') activate_date from " + TABLE_NAME;
    }

    /**
     * Constructs a ECardsFactory instance.
     */
    protected ECardsFactory()
    {
	super();
    }

    /**
     * Returns the ECard of the given id.
     *
     * @param	eCardID			the ecard id
     * @param	db			the db name(bcc or bu)
     *
     * @return	The XML document that contains the ECards information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getECard(String eCardID, String db)
	throws DBException, IOException
    {
	String sql = null;
	if (db != null && db.equals("bu")) {
	    sql = SELECT_BU_ECARDS_LIST + " AND ec.ecard_id = " + eCardID;
        } else {
	    sql = SELECT_BCC_ECARDS_LIST + " AND ec.ecard_id = " + eCardID;
        }
	return query(sql);
    }

    /**
     * Returns the ECard batch of the given id.
     *
     * @param	eCardID			the ecard id
     *
     * @return	The XML document that contains the ECards information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getECardBatch(String eCardID)
	throws DBException, IOException
    {
	String sql = SELECT_ECARD_BATCH + " AND ec.ecard_id = " + eCardID;
	return query(sql);
    }

    /**
     * Returns the ecard summary for a pack/lot with the given id and range
     *
     * @param   ecardID               the ecard identifier
     * @param   range                 the range defined for lot/pack etc.
     *
     * @return The XML document describing the information for the ecards
     *
     * @throws  DBException
     * @throws  IOException
     */
    public String getECardsSummary(String ecardID, String range, String timestamp)
        throws DBException, IOException
    {
        String  sql = 
            "SELECT max(eb.ecard_type) ecard_type, max(et.description) description, " +
            "sum(decode(ec.is_usable,1,1,0)) usable, sum(decode(ec.is_usable,0,1,0)) not_usable, " + 
            "sum(decode(ec.last_redeem_date,null,0,1)) redeemed, " + 
            "sum(decode(greatest(nvl(nvl(ec.activate_date, eb.activate_date), sysdate+(365*10)), " + 
            "bbxml.converttimestamp(" + timestamp + ")),bbxml.converttimestamp(" + timestamp + "),1,0)) activated, " + 
            "sum(decode(nvl(ec.revoke_date,eb.revoke_date),null,0,1)) revoked " +
            "FROM ecards ec, ecard_batches eb, ecard_types et WHERE ec.ecard_id >= (" + ecardID +
            " - (mod(" + ecardID + "," + range + "))) AND ec.ecard_id < (" + ecardID +
            " + (" + range + " - (mod(" + ecardID + "," + range + ")))) AND ec.ecard_id >= eb.start_ecard_id AND " +
            "ec.ecard_id <= eb.end_ecard_id AND ec.ecard_type = et.ecard_type";
        return query(sql);
    }

    /**
     * Returns the ecard_id/s for a pack/lot with the given id.
     *
     * @param   ecardID               the ecard identifier
     * @param   range                 the range defined for lot/pack etc.
     *
     * @return The XML document describing the information for the ecards
     *
     * @throws  DBException
     * @throws  IOException
     */
    public String getECardsToRevoke(String ecardID, String range)
        throws DBException, IOException
    {
        String  sql = SELECT_ECARDS_TO_REVOKE_LIST + " where revoke_date is null and ecard_id >= (" + ecardID +
                      " - (mod(" + ecardID + "," + range + "))) and ecard_id < (" + ecardID +
                      " + (" + range + " - (mod(" + ecardID + "," + range + "))))";
        return query(sql);
    }

    public String getECardsToActivate(String ecardID, String range)
        throws DBException, IOException
    {
        String  sql = SELECT_ECARDS_TO_ACTIVATE_LIST + " where activate_date is null and ecard_id >= (" + ecardID +
                      " - (mod(" + ecardID + "," + range + "))) and ecard_id < (" + ecardID +
                      " + (" + range + " - (mod(" + ecardID + "," + range + "))))";
        return query(sql);
    }

    /**
     * Returns the number of ECards available.
     *
     * @return	The integer that contains the number of ECard s.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getECardsCount()
	throws DBException, IOException
    {
	return count(SELECT_ECARDS_COUNT);
    }

    /**
     * Returns the database table name to be used.
     *
     * @return	The data table name.
     */
    public String getTableName()
    {
	return TABLE_NAME;
    }

    /**
     * Creates a ECards JavaBeans object.
     *
     * @return	The newly created ECards object.
     */
    protected Bean newBean()
    {
	return new ECards();
    }
}

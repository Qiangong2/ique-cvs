package com.broadon.db;

import java.io.IOException;
import java.util.Map;

import com.broadon.bean.Bean;

/**
 * The <c>BBPlayerFactory</c> class interfaces with the database to
 * query records from the bb player table.
 * Each record is represented by a BBPlayer JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.7 $
 */
public class BBPlayerFactory
    extends DBAccessFactory
{
    private static final String TABLE_NAME;
    private static final String SELECT_BB_PLAYER_LIST;
    private static final String SELECT_PURCHASED_TITLES_LIST;
    private static final String SELECT_BUNDLED_TITLES_LIST;
    private static final String SELECT_ACTIVITY_LIST;
    private static final String SELECT_ACTIVITY_COUNT;
    private static final String SELECT_BB_PLAYER_COUNT;

    private static final String SELECT_EBU_BB_PLAYER_LIST;
    private static final String SELECT_EBU_BB_PLAYER_MFTR_LIST;

    static
    {
        TABLE_NAME              = "BB_PLAYERS";
        SELECT_BB_PLAYER_LIST       =
            "select bb_id, bb_hwrev, bb_model, bundle_start_date, bu_id, activate_date, " +
            "deactivate_date, revoke_date, replaced_bb_id, regional_center_id, last_etdate, " +
            "manufacture_date, sn from " + TABLE_NAME;
        SELECT_PURCHASED_TITLES_LIST =
              "SELECT ct.title, ctp.title_id, ctp.trans_date, ctp.eunits price, ctp.eunits_applied paid, d.store_id, " +
              "ecr.ecard_id, ect.eunits, ecr.eunits_redeemed, st.region_id, st.regional_center_id, st.retailer_id FROM depots d, " +
              "content_title_purchases ctp, ecards ec, ecard_redemptions ecr, content_titles ct, ecard_types ect, stores st, " +
              "bb_players bbp WHERE ((d.hr_id = ctp.hr_id (+)) AND (ctp.bb_id = ecr.bb_id (+)) AND (ctp.title_id = ecr.title_id (+)) " +
              "AND (ec.ecard_id (+) = ecr.ecard_id) AND (ctp.title_id = ct.title_id) AND (ec.ecard_type = ect.ecard_type (+)) " +
              "AND (d.store_id = st.store_id) AND (bbp.bb_id = ctp.bb_id) AND (ctp.trans_date >= bbp.manufacture_date)";
        SELECT_BUNDLED_TITLES_LIST =
              "SELECT ct.title, bbb.title_id, bbb.start_date, bbb.end_date " +
              "FROM content_titles ct, bb_bundles bbb, bb_players bbp, bb_models bbm " +
              "WHERE ((bbp.bb_model = bbm.bb_model) AND (bbm.bb_model = bbb.bb_model) " +
              "AND (bbp.bundle_start_date = bbb.start_date) AND (bbp.bu_id = bbb.bu_id) AND (bbb.title_id = ct.title_id)";
        SELECT_BB_PLAYER_COUNT      = "select count(*) from " + TABLE_NAME;
        SELECT_ACTIVITY_LIST = "SELECT pav.request_date, pav.action_code, pav.bb_id, pav.hr_id, pav.store_id, pav.request_status, " +
              "pav.request_log, st.retailer_id, st.region_id, st.regional_center_id FROM bb_player_activity_v pav, stores st, " +
              TABLE_NAME + " bbp WHERE pav.store_id = st.store_id (+) AND pav.bb_id = bbp.bb_id";
        SELECT_ACTIVITY_COUNT = "SELECT count(*) FROM bb_player_activity_v pav, " + TABLE_NAME + " bbp WHERE pav.bb_id = bbp.bb_id";
        SELECT_EBU_BB_PLAYER_LIST       =
            "select bbp.bb_id, bbp.bb_hwrev, bbp.bb_model, bbp.bundle_start_date, bbp.bu_id, bu.business_name,  " +
            "bbp.public_key, bbp.manufacture_date, bbp.sn from business_units bu, " + TABLE_NAME + " bbp " +
            "where bbp.bu_id = bu.bu_id";
        SELECT_EBU_BB_PLAYER_MFTR_LIST       =
            "select bb_model, trunc(manufacture_date) mdate, count(*) quantity from " + TABLE_NAME + " group by " +
            "trunc(manufacture_date), bb_model";
    }


    /**
     * Constructs a BBPlayerFactory instance.
     */
    protected BBPlayerFactory()
    {
	super();
    }

    /**
     * Returns the bb player information identified by the given bbID.
     *
     * @param	bbID			the bb player identifier
     * @param	nameMap			the name mapping table
     *
     * @return	The BBPlayer JavaBeans that contains information of a
     *		BBPlayer record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public BBPlayer getBBPlayer(long bbID, Map nameMap)
	throws DBException, IOException
    {
	BBPlayerID predicate = new BBPlayerID(bbID);

	return (BBPlayer)queryUnique(predicate, nameMap);
    }

    /**
     * Returns the bb player information identified by the given bbID.
     *
     * @param	bbID			the bb player identifier
     *
     * @return	The XML document that contains information of a
     *		BBPlayer record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getBBPlayer(long bbID)
	throws DBException, IOException
    {
	return query(SELECT_BB_PLAYER_LIST + " where bb_id = " + bbID);
    }

    /**
     * Returns the bb player information identified by the given bbID.
     *
     * @param	bbID			the bb player identifier
     *
     * @return	The XML document that contains information of a
     *		BBPlayer record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getBBPlayer(String bbID)
	throws DBException, IOException
    {
	return query(SELECT_BB_PLAYER_LIST + " where bb_id = " + bbID);
    }

    /**
     * Returns the bb player information of a given range.
     *
     * @param	from			the starting record number
     * @param	to			the ending record number
     * @param	order			the sorted order
     *
     * @return	The XML document that contains the bb player information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getBBPlayers(int from, int to, String order)
	throws DBException, IOException
    {
	String	sql = "select * from (select rownum no, inner.* from (" +
			SELECT_BB_PLAYER_LIST + " order by " + order + ") " +
			"inner where rownum <= " + to + ") " +
			"where no > " + from;

	return query(sql);
    }

    /**
     * Returns the purchased title info for the given bb id.
     *
     * @param   bbID                 the bb id
     *
     * @return The XML document that contains the ContentTitlePurchases information.
     *
     * @throws  DBException
     * @throws  IOException
     */
    public String getPurchasedTitles(String bbID)
        throws DBException, IOException
    {
        String sql = "select rownum no, inner.* from (" +
                     SELECT_PURCHASED_TITLES_LIST + " AND (ctp.bb_id = " + bbID + "))) inner";
        return query(sql);
    }

    /**
     * Returns the bundled title info for the given bb id.
     *
     * @param   bbID                 the bb id
     *
     * @return The XML document that contains the ContentTitlePurchases information.
     *
     * @throws  DBException
     * @throws  IOException
     */
    public String getBundledTitles(String bbID)
        throws DBException, IOException
    {
        String sql = "select rownum no, inner.* from (" +
                     SELECT_BUNDLED_TITLES_LIST + " AND (bbp.bb_id = " + bbID + "))) inner";
        return query(sql);
    }

    /**
     * Returns the activity history info for the given bb id.
     *
     * @param   from                    the starting record number
     * @param   to                      the ending record number
     * @param   order                   the order by parameter
     * @param   bbID                    the bb id
     *
     * @return The XML document that contains the bb player activity.
     *
     * @throws  DBException
     * @throws  IOException
     */
    public String getPlayerActivity(int from, int to, String order, String  timeline, String bbID)
        throws DBException, IOException
    {
        String sql = "SELECT * FROM (SELECT rownum no, inner.* from (" +
                     SELECT_ACTIVITY_LIST + " AND pav.bb_id = " + bbID;

        if (timeline!=null && timeline.equals("before"))
            sql += " AND pav.request_date < bbp.manufacture_date";
        else if (timeline!=null && timeline.equals("after"))
            sql += " AND pav.request_date >= bbp.manufacture_date";

        sql += " order by " + order + " ) inner where rownum <= " + to + ") where no > " + from;

        return query(sql);
    }

    /** Returns the number of bb player activity count.
     *
     * @param   bbID                    the bb id
     *
     * @return	The integer that contains the activity count.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getPlayerActivityCount(String timeline, String bbID)
	throws DBException, IOException
    {
        String sql = SELECT_ACTIVITY_COUNT + " AND pav.bb_id = " + bbID;

        if (timeline!=null && timeline.equals("before"))
            sql += " AND pav.request_date < bbp.manufacture_date";
        else if (timeline!=null && timeline.equals("after"))
            sql += " AND pav.request_date >= bbp.manufacture_date";

	return count(sql);
    }

    /** Returns the number of bb players available.
     *
     * @return	The integer that contains the number of bb players.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getBBPlayerCount()
	throws DBException, IOException
    {
	return count(SELECT_BB_PLAYER_COUNT);
    }

    /**
     * Returns the bb player information identified by the given bbID.
     *
     * @param	bbID			the bb player identifier
     *
     * @return	The XML document that contains information of a
     *		BBPlayer record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getEBUPlayer(String bbID)
	throws DBException, IOException
    {
	return query(SELECT_EBU_BB_PLAYER_LIST + " and bbp.bb_id = " + bbID);
    }

    /**
     * Returns the bb player manufacturing information (model, date, quantity).
     *
     * @param   from                    the starting record number
     * @param   to                      the ending record number
     * @param   oder                      the order by parameter
     *
     * @return	The XML document that contains information of all BBPlayer records.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getEBUPlayerMftrList(int from, int to, String order)
        throws DBException, IOException
    {
        String  sql = "select * from (select rownum no, inner.* from (" +
                        SELECT_EBU_BB_PLAYER_MFTR_LIST + " order by " + order +
                            ") " + "inner where rownum <= " + to + ") " +
                        "where no > " + from;

        return query(sql);
    }

    /** Returns the number of (model, date, quantity).
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getEBUPlayerMftrListCount()
	throws DBException, IOException
    {
        String sql = "select count(*) from (" + SELECT_EBU_BB_PLAYER_MFTR_LIST + ")";
	return count(sql);
    }

    /**
     * Returns the bb player information identified by the given bbID.
     *
     * @param   from                    the starting record number
     * @param   to                      the ending record number
     * @param   order                   the order by parameter
     * @param   model                   the bb model
     * @param   mdate                   the manufacture date
     *
     * @return	The XML document that contains information of all BBPlayer records
     *          for this model manufactured on this date.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getEBUPlayerMftrDetails(int from, int to, String order, String model, String mdate)
        throws DBException, IOException
    {
        String  sql = "select * from (select rownum no, inner.* from (" +
                        SELECT_EBU_BB_PLAYER_LIST + " and bbp.manufacture_date >= to_date('" +
                        mdate + "', 'YYYY.MM.DD') and bbp.manufacture_date < to_date('" + 
                        mdate + "', 'YYYY.MM.DD')+1 and bbp.bb_model = '" + model + 
                        "' order by " + order + ") inner where rownum <= " + to +
                        ") where no > " + from;
        return query(sql);
    }

    /** Returns the quantity of (model, date, quantity).
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getEBUPlayerMftrDetailsCount(String model, String mdate)
	throws DBException, IOException
    {
        String sql = "select count(*) from (" + SELECT_EBU_BB_PLAYER_LIST + 
                        " and bbp.manufacture_date >= to_date('" +
                        mdate + "', 'YYYY.MM.DD') and bbp.manufacture_date < to_date('" +
                        mdate + "', 'YYYY.MM.DD')+1 and bbp.bb_model = '" + model + "')";
	return count(sql);
    }

    /**
     * Returns the database table name to be used.
     *
     * @return The data table name.
     */
    public String getTableName()
    {
        return TABLE_NAME;
    }

    /**
     * Creates a BBPlayer JavaBeans object.
     *
     * @return	The newly created BBPlayer object.
     */
    protected Bean newBean()
    {
	return new BBPlayer();
    }
}

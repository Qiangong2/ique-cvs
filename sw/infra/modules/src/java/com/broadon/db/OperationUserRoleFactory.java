package com.broadon.db;

import java.io.IOException;

import com.broadon.bean.Bean;
import com.broadon.util.VersionNumber;

/**
 * The <code>OperationUserRoleFactory</code> class interfaces with the database
 * to create, update, or query records from the Operation_User_Roles table.
 * Each record is represented by a OperationUserRole JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.9 $
 */
public class OperationUserRoleFactory
    extends SyncableFactory
{
    private static final String TABLE_NAME;
    private static final String	GET_ROLE_NAME_COND;
    private static final String	SELECT_OPERATION_USER_ROLE_LIST;
    private static final String	SELECT_OPERATION_USER_ROLE_COUNT;
    private static final String	SELECT_CUR_OPERATION_USER_ROLE_LIST;
    private static final String	SELECT_CUR_OPERATION_USER_ROLE_COUNT;

    private static final VersionNumber VERSION_1_2_0 = VersionNumber.valueOf("1.2.0");


    static
    {
	TABLE_NAME		= "OPERATION_USER_ROLES";
	GET_ROLE_NAME_COND	= "orl.role_level = our.role_level";
	SELECT_OPERATION_USER_ROLE_LIST	=
	    "select our.operator_id, our.role_level, orl.role_name, orl.description, our.bu_id, " +
                "our.grant_date, our.is_active from " + TABLE_NAME + " our, OPERATION_ROLES orl";
	SELECT_OPERATION_USER_ROLE_COUNT = "select count(*) from " + TABLE_NAME;

	SELECT_CUR_OPERATION_USER_ROLE_LIST	=
	    "SELECT rownum no, our.operator_id, our.role_level, our.bu_id, orl.role_name, " +
            "orl.description, our.is_active, ou.role_level default_role FROM " + TABLE_NAME + 
            " our, OPERATION_ROLES orl, OPERATION_USERS ou WHERE " +
            "our.role_level = orl.role_level AND our.operator_id = ou.operator_id";
	SELECT_CUR_OPERATION_USER_ROLE_COUNT	=
	    "SELECT count(*) FROM " + TABLE_NAME + 
            " our, OPERATION_ROLES orl, OPERATION_USERS ou WHERE " +
            "our.role_level = orl.role_level AND our.operator_id = ou.operator_id";
    }

    /**
     * Constructs a OperationUserRoleFactory instance.
     */
    protected OperationUserRoleFactory()
    {
	super();
    }

    /**
     * Returns the operator information of a given range.
     *
     * @param	from			the starting record number
     * @param	to				the ending record number
     * @param	order			the sorted order
     *
     * @return	The XML document that contains the operator information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getOperationUserRoles(int from, int to, String order)
	throws DBException, IOException
    {
	String	sql = "select * from (select rownum no, inner.* from (" +
			SELECT_OPERATION_USER_ROLE_LIST + " where " + 
			GET_ROLE_NAME_COND + " order by " + order + ") " +
			"inner where rownum <= " + to + ") " +
			"where no > " + from;

	return query(sql);
    }

 
   /**
     * Returns the operator details with the given id and role.
     *
     * @param   operatorID                 the operator identifier
     * @param   roleLevel                  the role level
     *
     * @return The XML document describing the information of operator
     *          information.
     *
     * @throws  DBException
     * @throws  IOException
     */
    public String getOperationUserRole(String operatorID, String roleLevel)
        throws DBException, IOException
    {
        String  sql = SELECT_OPERATION_USER_ROLE_LIST +
                        " where " + GET_ROLE_NAME_COND + 
			" and our.operator_id = " + operatorID +
			" and our.role_level = " + roleLevel +
                        " order by orl.description";
        return query(sql);
    }

   /**
     * Returns the operator details with the given id.
     *
     * @param   operatorID                 the operator identifier
     * @param	active (1 = Yes, 0 = No)	only active roles ?
     *
     * @return The XML document describing the information of operator
     *          information.
     *
     * @throws  DBException
     * @throws  IOException
     */
    public String getOperationUserRole(String operatorID, int active)
        throws DBException, IOException
    {
        String  sql = "";
        if (active != 1) 
            sql = SELECT_OPERATION_USER_ROLE_LIST +
                        " where " + GET_ROLE_NAME_COND + 
			" and our.operator_id = " + operatorID +
                        " order by orl.description";
        else
            sql = SELECT_OPERATION_USER_ROLE_LIST +
                        " where " + GET_ROLE_NAME_COND + 
                        " and our.is_active = 1" +
			" and our.operator_id = " + operatorID +
                        " order by orl.description";
        return query(sql);
    }

   /**
     * Returns the operator role details excluding non-gui operators with the given id.
     *
     * @param   operatorID                 the operator identifier
     * @param	active (1 = Yes, 0 = No)	only active roles ?
     *
     * @return The XML document describing the information of operator
     *          information.
     *
     * @throws  DBException
     * @throws  IOException
     */
    public String getOperationUserRole(String operatorID, String ngRoles, int active)
        throws DBException, IOException
    {
        String  sql = "";
        if (active != 1) 
            sql = SELECT_OPERATION_USER_ROLE_LIST +
                        " where " + GET_ROLE_NAME_COND + 
			" and our.operator_id = " + operatorID +
                        " and our.role_level not in (" + ngRoles + 
                        ") order by orl.description";
        else
            sql = SELECT_OPERATION_USER_ROLE_LIST +
                        " where " + GET_ROLE_NAME_COND + 
                        " and our.is_active = 1" +
			" and our.operator_id = " + operatorID +
                        " and our.role_level not in (" + ngRoles + 
                        ") order by orl.description";
        return query(sql);
    }

    /**
     * Returns the number of operators available.
     *
     * @param	operatorID			the operator identifier
     * @param	active (1 = Yes, 0 = No)	only active roles ?
     *
     * @return	The integer that contains the number of operators.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getOperationUserRoleCount(long operatorID, int active)
	throws DBException, IOException
    {
        if (active != 1) 
  	    return count(SELECT_OPERATION_USER_ROLE_COUNT + " where operator_id = " + 
                          operatorID);
        else
  	    return count(SELECT_OPERATION_USER_ROLE_COUNT + " where operator_id = " + 
                          operatorID + " and is_active = 1");
    }

    /**
     * Returns the number of admins available.
     *
     * @param	buid		   business unit
     * @param	role		   role level
     *
     * @return	The integer that contains the number of operators.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getOperationUserRoleCount(String buid, String role)
	throws DBException, IOException
    {
  	    return count(SELECT_OPERATION_USER_ROLE_COUNT + " where bu_id = " + 
                         buid + " and role_level = " + role + 
                         " and is_active = 1");
    }

    /**
     * Returns the number of admins available.
     *
     * @param	buid		   business unit
     * @param	role		   role level
     *
     * @return	The integer that contains the number of operators.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getOperationUserRoleCount(String operatorID, String role, String buid)
	throws DBException, IOException
    {
  	    return count(SELECT_OPERATION_USER_ROLE_COUNT + " where operator_id = " + 
                         operatorID + " and bu_id = " + buid + 
			 " and role_level = " + role + " and is_active = 1");
    }

    /**
     * Returns the number of operators available.
     *
     * @param	operatorID			the operator identifier
     * @param	active (1 = Yes, 0 = No)	only active roles ?
     *
     * @return	The integer that contains the number of operators.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getOperationUserRoleCount(String operatorID, int active)
	throws DBException, IOException
    {
        if (active != 1) 
  	    return count(SELECT_OPERATION_USER_ROLE_COUNT + " where operator_id = " + 
                          operatorID);
        else
  	    return count(SELECT_OPERATION_USER_ROLE_COUNT + " where operator_id = " + 
                          operatorID + " and is_active = 1");
    }

    /**
     * Returns the number of gui operation roles available.
     *
     * @param	operatorID			the operator identifier
     * @param	active (1 = Yes, 0 = No)	only active roles ?
     *
     * @return	The integer that contains the number of operators.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getOperationUserRoleCount(String operatorID, String ngRoles, int active)
	throws DBException, IOException
    {
        if (active != 1) 
  	    return count(SELECT_OPERATION_USER_ROLE_COUNT + " where operator_id = " + 
                          operatorID + " and role_level not in (" + ngRoles + ")");
        else
  	    return count(SELECT_OPERATION_USER_ROLE_COUNT + " where operator_id = " + 
                          operatorID + " and is_active = 1 and role_level not in (" + 
                          ngRoles + ")");
    }

    /**
     * Returns the number of operators available.
     *
     * @return	The integer document that contains the number of operators.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getOperationUserRoleCount()
	throws DBException, IOException
    {
	return count(SELECT_OPERATION_USER_ROLE_COUNT);
    }

   /**
     * Returns the list of current operator roles with the given id and bu id.
     *
     * @param   operatorID             the operator identifier
     * @param   bu id                  the bu id
     *
     * @return The XML document describing the information of operator
     *          information.
     *
     * @throws  DBException
     * @throws  IOException
     */
    public String getCurrentOperationUserRole(String operatorID, String buid)
        throws DBException, IOException
    {
        String  sql = SELECT_CUR_OPERATION_USER_ROLE_LIST +
                        " AND our.operator_id = " + operatorID +
			" AND our.bu_id = " + buid +
                        " ORDER BY orl.description";
        return query(sql);
    }

   /**
     * Returns the number of current operator roles with the given id and bu id.
     *
     * @param   operatorID             the operator identifier
     * @param   bu id                  the bu id
     *
     * @return	The integer document that contains the number of operators.
     *
     * @throws  DBException
     * @throws  IOException
     */
    public int getCurrentOperationUserRoleCount(String operatorID, String buid)
        throws DBException, IOException
    {
        String  sql = SELECT_CUR_OPERATION_USER_ROLE_COUNT +
                        " AND our.operator_id = " + operatorID +
			" AND our.bu_id = " + buid;
        return count(sql);
    }

   /**
     * Returns the list of new operator roles for the given id and bu id.
     *
     * @param   operatorID             the operator identifier
     * @param   bu id                  the bu id
     *
     * @return The XML document describing the information of operator
     *          information.
     *
     * @throws  DBException
     * @throws  IOException
     */
    public String getNewOperationUserRole(String operatorID, String buid)
        throws DBException, IOException
    {
        String  sql = "SELECT rownum no, role_level, role_name, description FROM OPERATION_ROLES " +
                      "WHERE role_level NOT IN (SELECT role_level FROM OPERATION_USER_ROLES " +
                      "WHERE operator_id = " + operatorID + " AND bu_id = " + buid +
                      ") ORDER BY description";
        return query(sql);
    }

   /**
     * Returns the number of new operator roles for the given id and bu id.
     *
     * @param   operatorID             the operator identifier
     * @param   bu id                  the bu id
     *
     * @return	The integer document that contains the number of operators.
     *
     * @throws  DBException
     * @throws  IOException
     */
    public int getNewOperationUserRoleCount(String operatorID, String buid)
        throws DBException, IOException
    {
        String  sql = "SELECT count(*) FROM OPERATION_ROLES " +
                      "WHERE role_level NOT IN (SELECT role_level FROM OPERATION_USER_ROLES " +
                      "WHERE operator_id = " + operatorID + " AND bu_id = " + buid +
                      ")";
        return count(sql);
    }

    /**
     * The records produced by this factory is only supported in depot
     * schema versions 1.2.0 and later.
     *
     * @param	depotSchemaVersion the schema version used by the depot
     * @return	true if the depot supports the record types; otherwise false.
     */
    public boolean inDepotSchemaVersion(VersionNumber depotSchemaVersion)
    {
        return (depotSchemaVersion.compareTo(VERSION_1_2_0) >= 0);
    }

    /**
     * Returns the database table name to be used.
     *
     * @return	The data table name.
     */
    public String getTableName()
    {
	return TABLE_NAME;
    }

    /**
     * Creates a OperationUserRole JavaBeans object.
     *
     * @return	The newly created OperationUserRole object.
     */
    protected Bean newBean()
    {
	return new OperationUserRole();
    }


}

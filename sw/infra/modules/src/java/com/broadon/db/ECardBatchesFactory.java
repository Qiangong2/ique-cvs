package com.broadon.db;

import java.io.IOException;

import com.broadon.bean.Bean;

/**
 * The <c>ECardBatchesFactory</c> class interfaces with the database to
 * create, update, or query records from the ECardBatches table.
 * Each record is represented by a ECardBatches JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.5 $
 */
public class ECardBatchesFactory
    extends SyncableFactory
{
    private static final String TABLE_NAME;
    private static final String	SELECT_BCC_ECARD_BATCHES_LIST;
    private static final String	SELECT_BU_ECARD_BATCHES_LIST;
    private static final String	SELECT_ECARD_BATCHES_COUNT;

    static
    {
	TABLE_NAME		= "ECARD_BATCHES";
	SELECT_BCC_ECARD_BATCHES_LIST =
	    "SELECT eb.end_ecard_id, eb.start_ecard_id, eb.cert_id, " +
	    "eb.create_date, eb.ecard_type, eb.print_date, eb.ship_date, " + 
	    "eb.upload_date, eb.bu_id, eb.expire_date, et.description, bu.business_name " +
            "FROM " + TABLE_NAME + " eb, ECARD_TYPES et, BUSINESS_UNITS bu WHERE " +
            "eb.ecard_type = et.ecard_type AND eb.bu_id = bu.bu_id";
	SELECT_BU_ECARD_BATCHES_LIST =
	    "SELECT eb.end_ecard_id, eb.start_ecard_id, eb.cert_id, " +
	    "eb.create_date, eb.ecard_type, eb.print_date, eb.ship_date, " + 
	    "eb.upload_date, eb.bu_id, eb.expire_date, et.description, bu.business_name, " +
            "to_char(eb.revoke_date, 'YYYYMMDD') rdate, eb.revoke_date, to_char(eb.activate_date, 'YYYYMMDD') adate, " +
            "eb.activate_date, to_char(sysdate, 'YYYYMMDD') cdate, trunc(sysdate) current_date FROM " + TABLE_NAME + 
            " eb, ECARD_TYPES et, BUSINESS_UNITS bu WHERE " +
            "eb.ecard_type = et.ecard_type AND eb.bu_id = bu.bu_id";
	SELECT_ECARD_BATCHES_COUNT = "select count(*) from " + TABLE_NAME;
    }

    /**
     * Constructs a ECardBatchesFactory instance.
     */
    protected ECardBatchesFactory()
    {
	super();
    }

    /**
     * Returns the ECard Batchess.
     *
     * @param	startID			the starting id number
     * @param	endID			the ending id number
     *
     * @return	The XML document that contains the ECard Batches information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getECardBatches(String startID, String endID, String db)
	throws DBException, IOException
    {
	String sql = null;
	if (db != null && db.equals("bu")) {
	    sql =  SELECT_BU_ECARD_BATCHES_LIST + 
		" AND eb.start_ecard_id = " + startID +
		" AND eb.end_ecard_id = " + endID;
        } else {
	    sql =  SELECT_BCC_ECARD_BATCHES_LIST + 
		" AND eb.start_ecard_id = " + startID +
		" AND eb.end_ecard_id = " + endID;
        }
	return query(sql);
    }

    /**
     * Returns the ECard Batchess information of a given range.
     *
     * @param	from			the starting record number
     * @param	to			the ending record number
     * @param	order			the sorted order
     *
     * @return	The XML document that contains the ECard Batches information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getECardBatches(int from, int to, String order, String db)
	throws DBException, IOException
    {
	String sql = null;
	if (db != null && db.equals("bu")) {
	    sql = "select * from (select rownum no, inner.* from (" +
		   SELECT_BU_ECARD_BATCHES_LIST + " ORDER BY " + order + 
                   ") inner where rownum <= " + to + ") " +
		   "where no > " + from;
        } else {
	    sql = "select * from (select rownum no, inner.* from (" +
		  SELECT_BCC_ECARD_BATCHES_LIST + " ORDER BY " + order + 
                  ") inner where rownum <= " + to + ") " +
		  "where no > " + from;
        }
	return query(sql);
    }

    /**
     * Returns the number of ECard Batches available.
     *
     * @return	The integer that contains the number of ECard Batches.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getECardBatchesCount()
	throws DBException, IOException
    {
	return count(SELECT_ECARD_BATCHES_COUNT);
    }

    /**
     * Returns the database table name to be used.
     *
     * @return	The data table name.
     */
    public String getTableName()
    {
	return TABLE_NAME;
    }

    /**
     * Creates a ECardBatches JavaBeans object.
     *
     * @return	The newly created ECardBatches object.
     */
    protected Bean newBean()
    {
	return new ECardBatches();
    }
}

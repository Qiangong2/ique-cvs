package com.broadon.db;

import com.broadon.sql.PredicateItem;

/**
 * The <c>LotsID</c> class conforms to the JavaBeans property,
 * and is a Predicate that finds the Lots identity by the request
 * identifier.
 *
 * @version	$Revision: 1.1 $
 */
public class LotsID
    extends PredicateItem
{
    private String	lotNumber;

    /**
     * Constructs a LotsID type instance.
     *
     * @param	lotNumber			the request identifier
     */
    public LotsID(String lotNumber)
    {
	super();
	this.lotNumber = new String(lotNumber);
    }

    /**
     * Getter for lotNumber.
     *
     * @return	The request identifier.
     */
    public String getLotNumber()
    {
	return lotNumber;
    }

    /**
     * Setter for lotNumber.
     *
     * @param	lotNumber			the lot identifier
     */
    public void setLotNumber(String lotNumber)
    {
	this.lotNumber = lotNumber;
    }
}

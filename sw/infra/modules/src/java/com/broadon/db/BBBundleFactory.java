package com.broadon.db;

import java.io.IOException;

import com.broadon.bean.Bean;

/**
 * The <code>BBBundleFactory</code> abstract class interfaces with the
 * database to create, update, or query records from the bb-bundles
 * table.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.7 $
 */
public class BBBundleFactory
    extends SyncableFactory
{
    private static final String     TABLE_NAME;
    private static final String	SELECT_BB_BUNDLE_LIST;
    private static final String	SELECT_BB_BUNDLE_DETAIL;
    private static final String	SELECT_BB_BUNDLE_COUNT;

    static
    { 
	TABLE_NAME		= "BB_BUNDLES";
	SELECT_BB_BUNDLE_LIST	=
	    "select bb_model, bu_id, to_char(start_date, 'RRMMDD') sdate, " +
            "start_date, end_date, sku, count(*) tcount from " + TABLE_NAME;
	SELECT_BB_BUNDLE_DETAIL	=
	    "select rownum no, bbb.bb_model, ct.title, bbb.title_id, " + 
            "bu.business_name, bbb.bu_id, to_char(bbb.start_date, 'RRMMDD') sdate, " +
            "bbb.start_date, bbb.end_date, bbb.create_date, bbb.rtype, bbb.limits, bbb.sku from " + 
            TABLE_NAME + " bbb, CONTENT_TITLES ct, BUSINESS_UNITS bu where " +
            "ct.title_id = bbb.title_id and bu.bu_id = bbb.bu_id";
	SELECT_BB_BUNDLE_COUNT	= "select count(*) from (select count(*) from " + TABLE_NAME +
            " group by bb_model, bu_id, start_date having count(*) > 0)";
    }

    /**
     * Constructs a BBBundleFactory instance.
     */
    protected BBBundleFactory()
    {
	super();
    }

    /**
     * Returns the bb bundles information of a given range.
     *
     * @param	from			the starting record number
     * @param	to			the ending record number
     * @param	order			the sorted order
     *
     * @return	The XML document that contains the bb content type information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getBBBundles(int from, int to, String order)
	throws DBException, IOException
    {
	String	sql = "select * from (select rownum no, inner.* from (" +
			SELECT_BB_BUNDLE_LIST +
			" group by bb_model, bu_id, to_char(start_date, 'RRMMDD'), start_date, end_date, sku " +
                        "having count(*) > 0 order by " + order + ") " +
			"inner where rownum <= " + to + ") " +
			"where no > " + from;
	return query(sql);
    }

    /**
     * Returns the bb bundles details with the given model, bu_id, title_id, start_date.
     *
     * @param   bbModel		the bb model identifier
     * @param   buID		the bu id identifier
     * @param   titleID		the title id identifier
     * @param   startDate	the start date identifier
     *
     * @return The XML document describing the information of bb bundle
     *          information.
     *
     * @throws  DBException
     * @throws  IOException
     */
    public String getBBBundle(String bbModel, String buID, String titleID, String startDate)
        throws DBException, IOException
    {
        String  sql = SELECT_BB_BUNDLE_DETAIL + " and bbb.bb_model = '" + bbModel + 
		      "' and bbb.bu_id = " + buID + " and bbb.title_id = " + titleID +
                      " and bbb.start_date = '" + startDate + "'";
        return query(sql);
    }

    /**
     * Returns the bb bundles details with the given model, bu_id, start_date.
     *
     * @param   bbModel		the bb model identifier
     * @param   buID		the bu id identifier
     * @param   startDate	the start date identifier
     *
     * @return The XML document describing the information of bb bundle
     *          information.
     *
     * @throws  DBException
     * @throws  IOException
     */
    public String getBBBundle(String bbModel, String buID, String startDate)
        throws DBException, IOException
    {
        String  sql = SELECT_BB_BUNDLE_DETAIL + " and bbb.bb_model = '" + bbModel + 
		      "' and bbb.bu_id = " + buID + 
                      " and bbb.start_date = '" + startDate + "'";
        return query(sql);
    }

    /**
     * Returns the number of titles in the bb bundle with the given model, bu_id and start_date.
     *
     * @return	The integer document that contains the number of titles.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getBBBundleTitlesCount(String bbModel, String buID, String startDate)
	throws DBException, IOException
    {
        String sql = "SELECT count(*) from BB_BUNDLES where bb_model = '" + bbModel +
                     "' and bu_id = " + buID +
                     " and start_date = '" + startDate + "'";
	return count(sql);
    }

    /**
     * Returns the list of available title that can be added to a bundle with the
     * given model, buid, start
     *
     * @param   bbModel		the bb model identifier
     * @param   buID		the bu id identifier
     * @param   startDate	the start date identifier
     *
     * @return The XML document describing the title information
     *
     * @throws  DBException
     * @throws  IOException
     */
    public String getBBBundleAvailableTitles(String bbModel, String buID, String startDate)
        throws DBException, IOException
    {
        String  sql = "SELECT title_id, title FROM CONTENT_TITLES " +
                      "WHERE lower(title_type) = 'visible' AND title_id NOT IN (SELECT title_id FROM " + 
                      TABLE_NAME + " WHERE bb_model = '" + bbModel + "' AND bu_id = " + buID +
                      " AND start_date = '" + startDate + "') ORDER BY title";
        return query(sql);
    }

    /**
     * Returns the number of records for a given bu_id and sku.
     *
     * @return	The integer document that contains the count
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getSKUCountByBU(String buid, String sku)
	throws DBException, IOException
    {
        String sql = "SELECT count(*) FROM " + TABLE_NAME + " WHERE bu_id = " + buid +
                     " AND sku = '" + sku + "'";
	return count(sql);
    }

    /**
     * Returns the number of records for a given bu_id, model and start date
     *
     * @return	The integer document that contains the count
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getPartNumberCount(String buid, String model, String start)
	throws DBException, IOException
    {
        String sql = "SELECT count(*) FROM " + TABLE_NAME + " WHERE bu_id = " + buid +
                     " AND bb_model = '" + model + "' AND start_date = '" + start + "'";
	return count(sql);
    }

    /**
     * Returns the number of bb bundles available.
     *
     * @return	The integer document that contains the number of bb bundles.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getBBBundlesCount()
	throws DBException, IOException
    {
	return count(SELECT_BB_BUNDLE_COUNT);
    }

    /**
     * Returns the database table name to be used.
     *
     * @return	The data table name.
     */
    public String getTableName()
    {
	return TABLE_NAME;
    }

    /**
     * Creates a Content JavaBeans object.
     *
     * @return	The newly created Content object.
     */
    protected Bean newBean()
    {
	return new BBBundle();
    }
}

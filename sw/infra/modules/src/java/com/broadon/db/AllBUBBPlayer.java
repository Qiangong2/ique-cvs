package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <c>AllBUBBPlayer</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *<p>
 * It contains information about operation users.
 *
 * @version	$Revision: 1.2 $
 */
public class AllBUBBPlayer
    extends PBean
{
    private Long		bbID;
    private Integer		BBHwRevision;
    private String		BBModel;
    private String		publicKey;
    private Integer		businessUnitID;
    private Date		bundleStartDate;
    private Date		mftrDate;
    private String		sn;

    /**
     * Constructs an empty AllBUBBPlayer instance.
     */
    public AllBUBBPlayer()
    {
    }

    /**
     * Getter for bbID.
     *
     * @return	The bb identifier.
     */
    public Long getBBID()
    {
	return bbID;
    }

    /**
     * Setter for bbID.
     *
     * @param	bbID		the bb identifier
     */
    public void setBBID(Long bbID)
    {
	this.bbID = bbID;
    }

    /**
     * Getter for BBModel.
     *
     * @return	The bb model.
     */
    public String getBBModel()
    {
	return BBModel;
    }

    /**
     * Setter for BBModel.
     *
     * @param	BBModel		the bb model
     */
    public void setBBModel(String BBModel)
    {
	this.BBModel = BBModel;
    }

    /**
     * Getter for publicKey.
     *
     * @return	The public key.
     */
    public String getPublicKey()
    {
	return publicKey;
    }

    /**
     * Setter for publicKey.
     *
     * @param	publicKey		the public key of the bb
     */
    public void setPublicKey(String publicKey)
    {
	this.publicKey = publicKey;
    }

    /**
     * Getter for BBHwRevision.
     *
     * @return	The bb hw revision flag.
     */
    public Integer getBBHwRevision()
    {
	return BBHwRevision;
    }

    /**
     * Setter for BBHwRevision.
     *
     * @param	BBHwRevision		the bb hw revision flag
     */
    public void setBBHwRevision(Integer BBHwRevision)
    {
	this.BBHwRevision = BBHwRevision;
    }

    /**
     * Getter for sn.
     *
     * @return	The serial number.
     */
    public String getSerialNumber()
    {
	return sn;
    }

    /**
     * Setter for sn.
     *
     * @param	sn		the serial number of the bb
     */
    public void setSerialNumber(String sn)
    {
	this.sn = sn;
    }

    /**
     * Getter for businessUnitID.
     *
     * @return	The business unit.
     */
    public Integer getBUID()
    {
	return businessUnitID;
    }

    /**
     * Setter for businessUnitID.
     *
     * @param	businessUnitID		the business unit
     */
    public void setBUID(Integer businessUnitID)
    {
	this.businessUnitID = businessUnitID;
    }

    /**
     * Getter for bundleStartDate.
     *
     * @return	The bundle start date date of this bb.
     */
    public Date getBundleStartDate()
    {
	return bundleStartDate;
    }

    /**
     * Setter for bundleStartDate.
     *
     * @param	bundleStartDate		the bundle start date date
     */
    public void setBundleStartDate(Date bundleStartDate)
    {
	this.bundleStartDate = bundleStartDate;
    }

    /**
     * Getter for mftrDate.
     *
     * @return	The manufacture date for an bb.
     */
    public Date getManufactureDate()
    {
	return mftrDate;
    }

    /**
     * Setter for mftrDate.
     *
     * @param	mftrDate		the manufacture date
     */
    public void setManufactureDate(Date mftrDate)
    {
	this.mftrDate = mftrDate;
    }
}


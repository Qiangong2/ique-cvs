package com.broadon.db;

import com.broadon.sql.PredicateItem;

/**
 * The <c>CertificateID</c> class conforms to the JavaBeans property,
 * and is a Predicate that finds various records.
 *
 * @version	$Revision: 1.2 $
 */
public class CertificateID
    extends PredicateItem
{
    private String	certificateID;

    /**
     * Constructs a CertificateID instance.
     *
     * @param	certificateID		the certificate identifier
     */
    public CertificateID(String certificateID)
    {
	super();
	this.certificateID = new String(certificateID);
    }

    /**
     * Getter for certificateID.
     *
     * @return	The certificate identifier.
     */
    public String getCertificateID()
    {
	return certificateID;
    }

    /**
     * Setter for certificateID.
     *
     * @param	certificateID		the certificate identifier
     */
    public void setCertificateID(String certificateID)
    {
	this.certificateID = certificateID;
    }
}

package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <c>BBBundle</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *<p>
 * It contains information about a BB content type.
 *
 * @version	$Revision: 1.4 $
 */
public class BBBundle
    extends PBean
{
    private String		bbModel;
    private Long		titleID;
    private Integer		buID;
    private Date		startDate;
    private Date		endDate;
    private Date		createDate;
    private String		rtype;
    private Integer		limits;
    private String		sku;

    /**
     * Constructs an empty BBBundle instance.
     */
    public BBBundle()
    {
	this.bbModel = null;
	this.titleID = null;
	this.buID = null;
	this.startDate = null;
	this.endDate = null;
	this.createDate = null;
        this.rtype = null;
        this.limits = null;
        this.sku = null;
    }

    /**
     * Getter for bbModel.
     *
     * @return	The BB model.
     */
    public String getBBModel()
    {
	return bbModel;
    }

    /**
     * Setter for bbModel.
     *
     * @param	bbModel			the BB model
     */
    public void setBBModel(String bbModel)
    {
	this.bbModel = bbModel;
    }

    /**
     * Getter for titleID.
     *
     * @return	The title ID of this BB Bundle.
     */
    public Long getTitleID()
    {
	return titleID;
    }

    /**
     * Setter for titleID.
     *
     * @param	titleID	 the title ID of this BB Bundle
     */
    public void setTitleID(Long titleID)
    {
	this.titleID = titleID;
    }

    /**
     * Getter for buID.
     *
     * @return	The bu ID of this BB Bundle.
     */
    public Integer getBUID()
    {
	return buID;
    }

    /**
     * Setter for buID.
     *
     * @param	buID	 the bu ID of this BB Bundle
     */
    public void setBUID(Integer buID)
    {
	this.buID = buID;
    }

    /**
     * Getter for startDate.
     *
     * @return	The start date of this BB Bundle.
     */
    public Date getStartDate()
    {
	return startDate;
    }

    /**
     * Setter for startDate.
     *
     * @param	startDate		the start date of this
     *					BB Bundle
     */
    public void setStartDate(Date startDate)
    {
	this.startDate = startDate;
    }

    /**
     * Getter for endDate.
     *
     * @return	The end date of this BB Bundle.
     */
    public Date getEndDate()
    {
	return endDate;
    }

    /**
     * Setter for endDate.
     *
     * @param	endDate		the end date of this
     *				BB Bundle
     */
    public void setEndDate(Date endDate)
    {
	this.endDate = endDate;
    }

    /**
     * Getter for createDate.
     *
     * @return	The create date of this BB Bundle.
     */
    public Date getCreateDate()
    {
	return createDate;
    }

    /**
     * Setter for createDate.
     *
     * @param	createDate		the create date of this
     *					BB Bundle
     */
    public void setCreateDate(Date createDate)
    {
	this.createDate = createDate;
    }

    /**
     * Getter for rtype.
     *
     * @return	The RType
     */
    public String getRType()
    {
	return rtype;
    }

    /**
     * Setter for rtype.
     *
     * @param	rtype			the RType
     */
    public void setRType(String rtype)
    {
	this.rtype = rtype;
    }

    /**
     * Getter for limits.
     *
     * @return	the limits for this rtype
     */
    public Integer getLimits()
    {
	return limits;
    }

    /**
     * Setter for limits.
     *
     * @param	limits	 the limits for this rtype
     */
    public void setLimits(Integer limits)
    {
	this.limits = limits;
    }

    /**
     * Getter for sku.
     *
     * @return	The SKU.
     */
    public String getSKU()
    {
	return sku;
    }

    /**
     * Setter for sku.
     *
     * @param	sku	the SKU
     */
    public void setSKU(String sku)
    {
	this.sku = sku;
    }

}

package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <c>BBContentType</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *<p>
 * It contains information about a BB content type.
 *
 * @version	$Revision: 1.3 $
 */
public class BBContentType
    extends PBean
{
    private String		bbModel;
    private String		contentObjectType;
    private String		description;
    private Date		lastUpdated;

    /**
     * Constructs an empty BBContentType instance.
     */
    public BBContentType()
    {
	this.bbModel = null;
	this.contentObjectType = null;
	this.description = null;
	this.lastUpdated = null;
    }

    /**
     * Getter for bbModel.
     *
     * @return	The BB model.
     */
    public String getBBModel()
    {
	return bbModel;
    }

    /**
     * Setter for bbModel.
     *
     * @param	bbModel			the BB model
     */
    public void setBBModel(String bbModel)
    {
	this.bbModel = bbModel;
    }

    /**
     * Getter for contentObjectType.
     *
     * @return	The content type of this BB content.
     */
    public String getContentObjectType()
    {
	return contentObjectType;
    }

    /**
     * Setter for contentObjectType.
     *
     * @param	contentObjectType	the content type of this BB content
     */
    public void setContentObjectType(String contentObjectType)
    {
	this.contentObjectType = contentObjectType;
    }

    /**
     * Getter for description.
     *
     * @return	The description of this BB content type.
     */
    public String getDescription()
    {
	return description;
    }

    /**
     * Setter for description.
     *
     * @param	description		the description of this BB content type
     */
    public void setDescription(String description)
    {
	this.description = description;
    }

    /**
     * Getter for lastUpdated.
     *
     * @return	The last updated date of this BB content type.
     */
    public Date getLastUpdated()
    {
	return lastUpdated;
    }

    /**
     * Setter for lastUpdated.
     *
     * @param	lastUpdated		the last updated date of this
     *					BB content type
     */
    public void setLastUpdated(Date lastUpdated)
    {
	this.lastUpdated = lastUpdated;
    }
}

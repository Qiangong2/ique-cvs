package com.broadon.db;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Date;
import java.util.Map;
import java.math.BigDecimal;

import javax.sql.DataSource;

import com.broadon.bean.Bean;
import com.broadon.sql.Predicate;
import com.broadon.util.Base64;
import com.broadon.util.FileCache;

/**
 * The <c>ContentFactory</c> class interfaces with the database to
 * create, update, or query records from the content-objects table.
 * Each record is represented by a Content JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.22 $
 */
public class ContentFactory
    extends ContentBaseFactory
{
    private static final String SELECT_CONTENT_NAME =
    "select content_object_name from CONTENT_OBJECTS";

    private static final String SELECT_CONTENT_LIST = 
    "select content_id, publish_date, revoke_date, " +
    "   replaced_content_id, content_object_type, content_size, " +
    "   content_checksum, content_object_name, " +
    "   content_object_version, last_updated, min_upgrade_version, " +
    "   upgrade_consent " +
    "from CONTENT_OBJECTS";

    private static final String SELECT_TITLE_LIST =
    "select rownum no, ct.title_id, ct.title, ct.title_type, " +
	"   ct.category, ct.publisher, ct.developer, ct.last_updated " +
	"from CONTENT_OBJECTS co, " +
    "   CONTENT_TITLES ct, " +
    "   CONTENT_TITLE_OBJECTS cto " +
    "where ct.title_id = cto.title_id and " +
    "  cto.content_id = co.content_id ";

    private static final String SELECT_CONTENT_COUNT =
    "select count(content_id) from CONTENT_OBJECTS";

    private static final String SELECT_ETICKET_METADATA_COUNT =
    "select count(*) from CONTENT_ETICKET_METADATA";

    // the following two constants are used to truncate the contentId to lower 32 bits
    public static final int HEX=16;
    public static final int INT_LENGTH_IN_HEX=8;

    private static void err(String msg)
    {
        final DateFormat dateFmt = 
            DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.LONG);
        System.err.println(dateFmt.format(new Date()) + " ERROR " + msg);
    }
    private static void log(String msg)
    {
        final DateFormat dateFmt = 
            DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.LONG);
        System.out.println(dateFmt.format(new Date()) + " INFO " + msg);
    }

    /** Response attributes
     */
    public interface ContentResponse
    {
        public void setContentLength(long sz);
        public OutputStream getOutputStream() throws IOException;
    }
    

    /** Function object to restore ContentHeader from a cached file
     */
    private static class FileCacheRestorer implements FileCache.RestoreCache
    {
        public FileCache.KeySet apply(File cached) 
        {
            try {
                InputStream inp = new FileInputStream(cached);
                DataInputStream inpd = new DataInputStream(inp);
                return new ContentHeader(inpd);
            }
            catch (IOException e) {
                err(e.getMessage());
                return null;
            }
        }
    }


    /**
     * Constructs a ContentFactory instance.
     */
    protected ContentFactory()
    {
        super();
    }


    /**
     * Use the existing cache for the given path, if one exists, and
     * otherwise create a new one with the given parameters.  The
     * clearCache will only happen if a new cache is created.  If the cache
     * should not be cleared, an attempt will be made to reuse the cache
     * stored in the given root path.
     *
     * This is a no-op if the file-cache has already been set for this 
     * object as the shared file-cache can only be set once.
     *
     * @param	rootDir		The root directory of the file cache
     * @param	maxMbytes	Maximum megabytes to be used by the cache
     * @param   maxFiles    The maximum number of files/objects in the cache
     * @param	clearCache	Whether or not to clear any existing cache
     */
    public FileCache getFileCache(String  rootDir, 
                                  long    maxMbytes, 
                                  long    maxFiles,
                                  boolean clearCache)
        throws IOException
    {
        return FileCache.getFileCache(rootDir,
                                      maxMbytes, 
                                      maxFiles,
                                      ContentHeader.NUMBER_OF_KEYSETS,
                                      new FileCacheRestorer(),
                                      clearCache);
    }


    public long getRecommendedChunksize() 
    {
        return ContentHeader.getChunkSize();
    }


    /**
     * Retrieves the content of the given contentID, and puts it to the
     * provided Output destionation.
     *<p>
     * This query is an exception that it does not return any Bean object.
     * Instead, it takes an OutputStream as the output target. This allows
     * the optimization to directly ship the data to the destination, such
     * as a file or the stream as a response back to the HTTP requestor.
     *
     * @param fileCache     cache of filed content
     * @param res           response attributes
     * @param contentID		the content identifier
     * @param offset		the starting offset
     * @param chunksize		the maximum bytes of the next chunk returned
     * @param out			the output destination
     * @param encode        base64 encoding when true, otherwise binary
     * @param prependHeader whether or not to prepend a content header
     * @param dataSource    SQL data source for DB access
     * @param nameMap		the name mapping table
     *
     * @throws	DBException
     * @throws	IOException
     */
    public void getContent(FileCache       fileCache,
                           ContentResponse res,
                           long            contentID,
                           long            offset,
                           long            chunksize,
                           boolean         encode,
                           boolean         prependHeader,
                           DataSource      dataSource,
                           Map             nameMap)
        throws DBException, IOException
    {
        getContent(fileCache,
                   res,
                   String.valueOf(contentID),
                   ContentHeader.CONTENT_ID_KEY,
                   offset,
                   chunksize,
                   new ContentID(contentID),
                   encode,
                   prependHeader,
                   dataSource,
                   nameMap);
    }

    /**
     * Retrieves the content of the given checksum, and puts it to the
     * provided Output destionation.
     *<p>
     * This query is an exception that it does not return any Bean object.
     * Instead, it takes an OutputStream as the output target. This allows
     * the optimization to directly ship the data to the destination, such
     * as a file or the stream as a response back to the HTTP requestor.
     *
     * @param fileCache     cache of filed content
     * @param res           response attributes
     * @param checksum		the checksum of the content
     * @param offset		the starting offset
     * @param chunksize		the maximum bytes of the next chunk returned
     * @param out			the output destination
     * @param encode        base64 encoding when true, otherwise binary
     * @param prependHeader whether or not to prepend a content header
     * @param dataSource    SQL data source for DB access
     * @param nameMap		the name mapping table
     *
     * @throws	DBException
     * @throws	IOException
     */
    public void getContent(FileCache       fileCache,
                           ContentResponse res,
                           String          checksum,
                           long            offset,
                           long            chunksize,
                           boolean         encode,
                           boolean         prependHeader,
                           DataSource      dataSource,
                           Map             nameMap)
        throws DBException, IOException
    {
        getContent(fileCache,
                   res,
                   checksum,
                   ContentHeader.CHECKSUM_KEY,
                   offset,
                   chunksize,
                   new ContentChecksum(checksum),
                   encode,
                   prependHeader,
                   dataSource,
                   nameMap);
    }

    /**
     * Retrieves the content of the given contentID, and puts it to the
     * provided Output destionation.
     *<p>
     * This query is an exception that it does not return any Bean object.
     * Instead, it takes an OutputStream as the output target. This allows
     * the optimization to directly ship the data to the destination, such
     * as a file or the stream as a response back to the HTTP requestor.
     *
     * @param fileCache     cache of filed content
     * @param res           response attributes
     * @param id		    the identifier
     * @param cacheIdIdx    Key-set index for the id in the FileCache
     * @param offset		the starting offset
     * @param chunksize		the maximum bytes of the next chunk returned
     * @param key			the key object
     * @param encode        base64 encoding when true, otherwise binary
     * @param prependHeader whether or not to prepend a content header
     * @param dataSource    SQL data source for DB access
     * @param nameMap		the name mapping table
     *
     * @throws	DBException
     * @throws	IOException
     */
    private void getContent(FileCache       fileCache,
                            ContentResponse res,
                            String          id,
                            int             cacheIdIdx,
                            long            offset,
                            long            chunksize,
                            Predicate       key,
                            boolean         encode,
                            boolean         prependHeader,
                            DataSource      dataSource,
                            Map             nameMap)
        throws DBException, IOException
    {
        if (offset < 0) {
            //
            // Starting offset must be non-negative.
            ///
            throw new 
                IllegalArgumentException("Invalid content download offset[" +
                                         offset +
                                         "]");
        }

        // See if the content is in the file-cache.
        //
        FileCache.CachedStream cs = fileCache.getStream(id, cacheIdIdx);
        if (cs == null) {
            //
            // Retrieve the content from the database, writing to a temp-file
            // in the caching directory.  Note that even if no caching
            // is to take place, we will store each content object in the
            // file-cache temporarily.
            //
            cs = cacheContent(fileCache, key, id, dataSource, nameMap);
        }
        // We are done with the DB/cache access, and will now stream the
        // binary chunk over the output stream, starting at byte ofst.
        //
        ContentHeader   hdr = (ContentHeader)cs.getKeySet();
        FileInputStream inp = cs.getCached();
        final long      ofst = offset + hdr.getHeaderSize();
        long            numBytes = hdr.getContentSize() - offset;

        // Limit the bytes to the chunksize if so requested
        //
        if (chunksize >= 0 && numBytes > chunksize)
            numBytes = chunksize;

        // Set the bytes in the response in the response header before
        // writing the response, then stream the output chunk to the 
        // given OutputStream
        //
        OutputStream out = res.getOutputStream();
        if (encode) {
            if (prependHeader)
                hdr.writeBase64(out);
            writeContentBase64(inp, out, ofst, numBytes);
        }
        else {
            if (prependHeader) {
                res.setContentLength(hdr.getHeaderSize() + numBytes);
                hdr.writeBytes(out);
            }
            else {
                res.setContentLength(numBytes);
            }
            writeContent(inp, out, ofst, numBytes);
        }
    } // getContent


    private void writeContent(FileInputStream inp, 
                              OutputStream    out, 
                              long            ofst,
                              long            numBytes)
        throws IOException
    {
	    byte[]	   buffer = new byte[10*1024];
        final long endOfst = ofst + numBytes;

        try {
            long sz;
            inp.skip(ofst);
            for (long b = ofst; b < endOfst; b += sz) {
                sz = inp.read(buffer);
                if (sz + b > endOfst) {
                    //
                    // We read a little too much, so scale it back
                    //
                    sz = endOfst - b;
                }
                else if (sz <= 0) {
                    throw new IOException("Unexpected end of content file");
                }
                out.write(buffer, 0, (int)sz);
            }
		}
	    finally
	    {
            out.flush();
            inp.close();
	    }
    } // writeContent


    private void writeContentBase64(FileInputStream inp, 
                                    OutputStream    out, 
                                    long            ofst,
                                    long            numBytes)
        throws IOException
    {
        Base64             base64 = new Base64();
        OutputStreamWriter outw = new OutputStreamWriter(out);
	    byte[]		       buffer = new byte[10*1024];
        final long         endOfst = ofst + numBytes;

        try {
            long sz;
            inp.skip(ofst);
            for (long b = ofst; b < endOfst; b += sz) {
                sz = inp.read(buffer);
                if (sz + b > endOfst) {
                    //
                    // We read a little too much, so scale it back
                    //
                    sz = endOfst - b;
                }
                else if (sz <= 0) {
                    throw new IOException("Unexpected end of content file");
                }
                base64.encodeBlock(outw, buffer, 0, (int)sz);
            }
		}
	    finally
	    {
            outw.flush();
            inp.close();
	    }
    } // writeContentBase64


    /**
     * Retrieves the content of the given key, writes it to a temporary file,
     * and then inserts it into the file-cache.
     *
     * @param key        the key object
     * @param id         the string representation of the id
     * @param dataSource the data base connection pool
     * @param nameMap    the name mapping table
     *
     * @throws	DBException
     * @throws	IOException
     */
    private FileCache.CachedStream cacheContent(FileCache  fileCache,
                                                Predicate  key,
                                                String     id,
                                                DataSource dataSource,
                                                Map        nameMap)
        throws DBException, IOException
    {
        // Create a temporary file to hold the content header and the
        // content.
        //
        File         tempFile = fileCache.getNewTempFile(id);
        OutputStream tempOut = new FileOutputStream(tempFile);

        // Perform the Database transaction and write the entire content
        // to the temporary file.
        //
        beginTransaction(dataSource, false, true,
                         DataSourceScheduler.DB_PRIORITY_SLOW);

        PreparedStatement statement = setupQueryStatement(key, nameMap);
        ContentHeader     contentHeader = null;
        try {
            ResultSet resultSet = executeQuery(statement);
            if (resultSet == null || !resultSet.next()) {
                //
                // No record.
                //
                // An exception needs to be raised here so that an
                // error can be returned to the requester.
                //
                throw new NotFoundException("Content of [" +
                                            id +
                                            "] not found");
            }

            // Get the content and associated header information
            //
            //long	contentId = resultSet.getLong("content_id");
            //content_id is extended to 96 bits in db. Here only the
            //old version 32 bits and discards the rest.
            BigDecimal longId = resultSet.getBigDecimal("content_id");
            String	contentChecksum = resultSet.getString("content_checksum");
            Blob	contentBlob = resultSet.getBlob("content_object");
            long	contentSize = contentBlob.length();
            String longIdStr = longId.toBigInteger().toString(HEX);
            long    contentId;
            if (longIdStr.length()>INT_LENGTH_IN_HEX)
                contentId = Long.parseLong(longIdStr.substring(longIdStr.length()-INT_LENGTH_IN_HEX), HEX);
            else
                contentId = Long.parseLong(longIdStr, HEX);
            // Create the content header and write it as the very
            // first bytes in the temp file
            //
            contentHeader = new ContentHeader(contentId, 
                                              contentSize,
                                              contentChecksum);
            contentHeader.writeBytes(tempOut);

            // Write the content bytes to the temp file in chunks of
            // 100K bytes at a time.
            //
            final int batchSize = 100*1024;
            for (long n = 1; n <= contentSize; n += batchSize) {
                tempOut.write(contentBlob.getBytes(n, batchSize));
            }

            // We should have at most one content object per id, or else the
            // database should be considered corrupt.
            //
            if (resultSet.next()) {
                //
                // This is not the only qualified record.
                //
                // Do not throw exception.  A content can have multiple content Ids. -Raymond
                // throw new NotUniqueException(id);
            }
        }
        catch (SQLException se) {
            se.printStackTrace();
            throw new IOException("SQL exception: " + se.getMessage());
        }
        finally {
            //
            // Close the temporary file, the statement and the connection
            // after execution.
            //
            try {
                statement.close();
                commitTransaction();
                tempOut.close();
            }
            catch (IOException ie) {
                err(ie.getMessage());
            }
            catch (DBException de) {
                err(de.getMessage());
            }
            catch (SQLException se) {
                err(se.getMessage());
            }
        }

        // We now have the content header and the content in the temporary
        // file and need to move this file into our caching mechanism.
        // Note that we have to do this even when caching is OFF, since
        // we use the cache as a temporary repository during the processing
        // of this request, even when subsequent requests will not utilize
        // the cached value.
        //
        // Note also that we do not delete the tempFile in the event of
        // an exception to help debugging problems.  Such leftover files
        // will be removed when the WebApp restarts, since the FileCache
        // constructor cleans out the cache directory.
        //
        return fileCache.insertAndGetStream(contentHeader, tempFile);
    }

    /**
     * Returns the content information identified by the given contentID.
     *
     * @param	contentID		the content identifier
     * @param	nameMap			the name mapping table
     *
     * @return	The Content JavaBeans that contains information of a
     *		Content-Contents record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public Content getContent(long contentID, Map nameMap)
	throws DBException, IOException
    {
	ContentID predicate = new ContentID(contentID);

	return (Content)queryUnique(predicate, nameMap);
    }

    /**
     * Returns the content information identified by the given contentID.
     *
     * @param	contentID			the content identifier
     *
     * @return	The XML document that contains information of a
     *		Content-Contents record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getContent(long contentID)
	throws DBException, IOException
    {
	return query(SELECT_CONTENT_LIST + " where content_id = " + contentID);
    }

    /**
     * Returns the content information identified by the given array of
     * contentIDs.
     *
     * @param	contentIDs		the content identifiers
     * @param	nameMap			the name mapping table
     *
     * @return	The array of Content JavaBeans, each contains
     *		all information of a Content-Contents record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public Content[] getContents(long[] contentIDs, Map nameMap)
	throws DBException, IOException
    {
	if (contentIDs == null)
	{
	    return null;
	}

	int size = contentIDs.length;

	if (size == 0)
	{
	    return null;
	}
	/*
	 * Set up the predicate.
	 */
	ContentID	contentID = new ContentID(contentIDs[0]);
	Predicate	predicate = contentID;

	for (int n = 1; n < size; n++)
	{
	    contentID = (ContentID)contentID.or(new ContentID(contentIDs[n]));
	}
	/*
	 * Query.
	 */
	return (Content[])query(predicate, nameMap);
    }

    /**
     * Returns the content information of a given range.
     *
     * @param	contentID		the content identifier
     * @param	from			the starting record number
     * @param	to			the ending record number
     * @param	order			the sorted order
     *
     * @return	The XML document that contains the tilte information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getContents(long contentID, int from, int to, String order)
	throws DBException, IOException
    {
	String	sql = "select * from (select rownum no, inner.* from (" +
			SELECT_CONTENT_LIST +
			    " where content_id = " + contentID +
			    " order by " + order + ") " +
			"inner where rownum <= " + to + ") " +
			"where no > " + from;

	return query(sql);
    }

    /**
     * Returns the content information of a given range.
     *
     * @param	from			the starting record number
     * @param	to			the ending record number
     * @param	order			the sorted order
     *
     * @return	The XML document that contains the tilte information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getContents(int from, int to, String order)
	throws DBException, IOException
    {
	String	sql = "select * from (select rownum no, inner.* from (" +
			SELECT_CONTENT_LIST + " order by " + order + ") " +
			"inner where rownum <= " + to + ") " +
			"where no > " + from;

	return query(sql);
    }

    /**
     * Returns the content name of the given content identifier.
     *
     * @param	contentID	the content identifier
     *
     * @return	The XML document that contains the content name.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getContentName(long contentID)
	throws DBException, IOException
    {
	return query(SELECT_CONTENT_NAME + " where content_id = " + contentID);
    }

    /**
     * Returns the content name of the given content identifier.
     *
     * @param	contentID			the content identifier
     *
     * @return	The XML document that contains the content name.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getContentName(String contentID)
	throws DBException, IOException
    {
	return query(SELECT_CONTENT_NAME + " where content_id = " + contentID);
    }

    /**
     * Returns the number of contents available.
     *
     * @param	contentID			the content identifier
     *
     * @return	The integer that contains the number of contents.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getContentCount(long contentID)
	throws DBException, IOException
    {
	return count(SELECT_CONTENT_COUNT + " where content_id = " + contentID);
    }


    /**
     * Returns the number of contents available.
     *
     * @param	contentID			the content identifier
     *
     * @return	The integer that contains the number of contents.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getContentCount(String contentID)
	throws DBException, IOException
    {
	return count(SELECT_CONTENT_COUNT + " where content_id = " + contentID);
    }

    /**
     * Returns the number of contents available.
     *
     * @return	The integer that contains the number of contents.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getContentCount()
	throws DBException, IOException
    {
	return count(SELECT_CONTENT_COUNT);
    }

    /**
     * Returns the title information identified by the given contentID.
     *
     * @param   contentID                 the content identifier
     *
     * @return The XML document that contains information of a
     *          Content-Titles record.
     *
     * @throws  DBException
     * @throws  IOException
     */
    public String getTitles(String contentID)
        throws DBException, IOException
    {
        return query(SELECT_TITLE_LIST + " and co.content_id = " + contentID +
                " order by ct.last_updated");
    }

    /**
     * Returns the number of eticket meteadata available.
     *
     * @param	contentID  	the content identifier
     *
     * @return	The integer that contains the number of contents.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getEticketMetadataCount(String contentID)
	throws DBException, IOException
    {
	return count(SELECT_ETICKET_METADATA_COUNT + " where content_id = " +
		contentID);
    }

    /**
     * Creates a Content JavaBeans object.
     *
     * @return	The newly created Content object.
     */
    protected Bean newBean()
    {
	return new Content();
    }

}

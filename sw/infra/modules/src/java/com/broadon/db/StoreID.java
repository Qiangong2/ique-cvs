package com.broadon.db;

import com.broadon.sql.PredicateItem;

/**
 * The <c>StoreID</c> class conforms to the JavaBeans property,
 * and is a Predicate that finds various records.
 *
 * @version	$Revision: 1.1 $
 */
public class StoreID
    extends PredicateItem
{
    private Integer	storeID;

    /**
     * Constructs a StoreID instance.
     *
     * @param	storeID		the Store identifier
     */
    public StoreID(int storeID)
    {
	super();
	this.storeID = new Integer(storeID);
    }

    /**
     * Getter for storeID.
     *
     * @return	The Store identifier.
     */
    public Integer getStoreID()
    {
	return storeID;
    }

    /**
     * Setter for storeID.
     *
     * @param	storeID		the Store identifier
     */
    public void setStoreID(Integer storeID)
    {
	this.storeID = storeID;
    }
}

package com.broadon.db;

import java.io.IOException;
import java.util.Map;

import com.broadon.bean.Bean;

/**
 * The <code>ContentTitleViewFactory</code> class interfaces with the database
 * to update or query records from the global_etickets  table.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.2 $
 */
public class ContentTitleViewFactory
    extends DBAccessFactory
{
    private static final String TABLE_NAME;
    private static final String SELECT_ATTRIBUTES;

    static
    {
	TABLE_NAME		= "CONTENT_TITLE_OBJECT_DETAIL_V";

        SELECT_ATTRIBUTES = "select " +
                                "content_id, title_id, cto_create_date, cto_revoke_date, " +
                                "has_content_object_revoked, has_eticket_metadata,  content_object_type, " +
                                "content_size, replaced_content_id, revoke_date, " +
                                "content_object_name, publish_date, " +
                                "content_object_version, " +
                                "is_content_object_latest, " +
                                "last_updated";
    }

    /**
     * Constructs a ContentTitleViewFactory instance.
     */
    protected ContentTitleViewFactory()
    {
	super();
    }

    public ContentTitleView[] getNewContentForGlobalETicket(long titleID, String buid, Map nameMap)
        throws DBException, IOException
    {
        String sql = SELECT_ATTRIBUTES + " FROM " + TABLE_NAME + " WHERE " +
            "title_id = ? AND has_eticket_metadata = 'Y' AND is_content_object_latest = 'Y' " +
            "AND content_id NOT IN (SELECT content_id FROM GLOBAL_ETICKETS WHERE revoke_date IS NULL " +
            "AND bu_id = " + buid + ")";

        Bean[]  beans = query(new TitleID(titleID), sql, nameMap);

        if (beans == null)
            return null;

        int count = beans.length;
        ContentTitleView[] ctv = new ContentTitleView[count];

        for (int n = 0; n < count; n++)
            ctv[n] = (ContentTitleView)beans[n];
        return ctv;
    }

    public int countNewContentForGlobalETicket(String titleID, String buid)
        throws DBException, IOException
    {
        String sql = "SELECT count(*) FROM " + TABLE_NAME + " WHERE " +
            "title_id = " + titleID + " AND has_eticket_metadata = 'Y' AND is_content_object_latest = 'Y' " +
            "AND content_id NOT IN (SELECT content_id FROM GLOBAL_ETICKETS WHERE revoke_date IS NULL " +
            "AND bu_id = " + buid + ")";

        return count(sql);
    }

    /**
     * Returns the database table name to be used.
     *
     * @return	The data table name.
     */
    public String getTableName()
    {
	return TABLE_NAME;
    }

    /**
     * Creates a GlobalEtickets JavaBeans object.
     *
     * @return The newly created GlobalEtickets object.
     */
    protected Bean newBean()
    {
        return new ContentTitleView();
    }
}

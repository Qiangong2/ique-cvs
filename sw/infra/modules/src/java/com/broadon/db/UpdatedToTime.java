package com.broadon.db;

import java.util.Date;

/**
 * The <c>UpdatedToTime</c> class conforms to the JavaBeans property,
 * and is a Predicate that finds records that were updated before the
 * given time (exclusive).
 *
 * @version	$Revision: 1.2 $
 */
public class UpdatedToTime
    extends UpdatedAtTime
{
    /**
     * Constructs a UpdatedToTime instance.
     *
     * @param	time			the ending time
     */
    public UpdatedToTime(Date time)
    {
	super(time, "<");
    }

    /**
     * Constructs a UpdatedToTime instance.
     *
     * @param	time			the ending time, in milli-seconds
     */
    public UpdatedToTime(long time)
    {
	super(new Date(time), "<");
    }
}

package com.broadon.db;

import com.broadon.sql.PredicateItem;

/**
 * The <c>OperationUserEmailAddress</c> class conforms to the JavaBeans property,
 * and is a Predicate that finds various records.
 *
 * @version	$Revision: 1.1 $
 */
public class OperationUserEmailAddress
    extends PredicateItem
{
    private String	emailAddress;

    /**
     * Constructs a OperationUserEmailAddress instance.
     *
     * @param	emailAddress		the email address
     */
    public OperationUserEmailAddress(String emailAddress)
    {
	super();
	this.emailAddress = new String(emailAddress);
    }

    /**
     * Getter for email address.
     *
     * @return	The email address.
     */
    public String getEmailAddress()
    {
	return emailAddress;
    }

    /**
     * Setter for emailAddress.
     *
     * @param	emailAddress		the email address
     */
    public void setEmailAddress(String emailAddress)
    {
	this.emailAddress = emailAddress;
    }
}

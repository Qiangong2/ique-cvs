package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <c>Competition</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *<p>
 * It contains information about a game competion and ties into 
 * potentially several different competing categories.
 *
 * @version	$Revision: 1.2 $
 */
public class Competition
    extends PBean
{
    private Long		titleID;
    private Integer     competitionID;
    private Date		startDate;
    private Date		endDate;
    private String		competitionName;
    private String      competitionDesc;
    private String      functionName;
    private Date		lastUpdated;

    /**
     * Constructs an empty Competition instance.
     */
    public Competition()
    {
    }

    /**
     * Getter for titleID.
     *
     * @return	The title identifier.
     */
    public Long getTitleID()
    {
        return titleID;
    }

    /**
     * Setter for titleID.
     *
     * @param	titleID			the title identifier
     */
    public void setTitleID(Long titleID)
    {
        this.titleID = titleID;
    }

    /**
     * Getter for competitionID.
     *
     * @return	The competition identifier.
     */
    public Integer getCompetitionID()
    {
        return competitionID;
    }

    /**
     * Setter for competitionID.
     *
     * @param	competitionID the competition identifier
     */
    public void setCompetitionID(Integer competitionID)
    {
        this.competitionID = competitionID;
    }

    /**
     * Getter for startDate.
     *
     * @return	The start data of the competition.
     */
    public Date getStartDate()
    {
        return startDate;
    }

    /**
     * Setter for startDate.
     *
     * @param	startDate the start data of the competition
     */
    public void setStartDate(Date startDate)
    {
        this.startDate = startDate;
    }

    /**
     * Getter for endDate.
     *
     * @return	The last available date of the competition.
     */
    public Date getEndDate()
    {
        return endDate;
    }

    /**
     * Setter for endDate.
     *
     * @param	endDate the last available date of the competition
     */
    public void setEndDate(Date endDate)
    {
        this.endDate = endDate;
    }

    /**
     * Getter for competitionName.
     *
     * @return	The name of the competition.
     */
    public String getCompetitionName()
    {
        return competitionName;
    }

    /**
     * Setter for competitionName.
     *
     * @param	competitionName the name of the competition
     */
    public void setCompetitionName(String competitionName)
    {
        this.competitionName = competitionName;
    }

    /**
     * Getter for competitionDesc.
     *
     * @return	The description of the competition.
     */
    public String getCompetitionDesc()
    {
        return competitionDesc;
    }

    /**
     * Setter for competitionDesc.
     *
     * @param	competitionDesc the description of the competition
     */
    public void setCompetitionDesc(String competitionDesc)
    {
        this.competitionDesc = competitionDesc;
    }

    /**
     * Getter for functionName.
     *
     * @return	The function used to compute scores in a competition.
     */
    public String getFunctionName()
    {
        return functionName;
    }

    /**
     * Setter for functionName.
     *
     * @param	functionName the function used to compute competition scores
     */
    public void setFunctionName(String functionName)
    {
        this.functionName = functionName;
    }

    /**
     * Getter for lastUpdated.
     *
     * @return	The last updated date.
     */
    public Date getLastUpdated()
    {
	return lastUpdated;
    }

    /**
     * Setter for lastUpdated.
     *
     * @param	lastUpdated		the last updated date
     */
    public void setLastUpdated(Date lastUpdated)
    {
	this.lastUpdated = lastUpdated;
    }

} // class Competition

package com.broadon.db;

/**
 * The <code>NotInTransactionException</code> class indicates that
 * this thread is not yet in a transaction.
 *
 * @version	$Revision: 1.1 $
 */
public class NotInTransactionException
    extends DBException
{
    /**
     * Constructs a NotInTransactionException instance.
     */
    public NotInTransactionException()
    {
	super();
    }

    /**
     * Constructs a NotInTransactionException instance.
     *
     * @param	message			the exception message
     */
    public NotInTransactionException(String message)
    {
	super(message);
    }

    /**
     * Constructs a NotInTransactionException instance.
     *
     * @param	message			the exception message
     * @param	throwable		the nested exception
     */
    public NotInTransactionException(String message, Throwable throwable)
    {
	super(message, throwable);
    }
}

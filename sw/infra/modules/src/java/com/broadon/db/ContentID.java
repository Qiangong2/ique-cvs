package com.broadon.db;

import com.broadon.sql.PredicateItem;

/**
 * The <c>ContentID</c> class conforms to the JavaBeans property,
 * and is a Predicate that finds ContentInfo identifiy by the content
 * identifier.
 *
 * @version	$Revision: 1.1 $
 */
public class ContentID
    extends PredicateItem
{
    private Long	contentID;

    /**
     * Constructs a ContentID type instance.
     *
     * @param	contentID		the content identifier
     */
    public ContentID(long contentID)
    {
	super();
	this.contentID = new Long(contentID);
    }

    /**
     * Getter for contentID.
     *
     * @return	The content identifier.
     */
    public Long getContentID()
    {
	return contentID;
    }

    /**
     * Setter for contentID.
     *
     * @param	contentID		the content identifier
     */
    public void setContentID(Long contentID)
    {
	this.contentID = contentID;
    }
}

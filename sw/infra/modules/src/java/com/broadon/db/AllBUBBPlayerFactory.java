package com.broadon.db;

import java.io.IOException;

import com.broadon.bean.Bean;

/**
 * The <c>AllBUBBPlayerFactory</c> class interfaces with the database to
 * query records from the bb player table.
 * Each record is represented by a AllBUBBPlayer JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.3 $
 */
public class AllBUBBPlayerFactory
    extends DBAccessFactory
{
    private static final String TABLE_NAME;
    private static final String SELECT_ALL_BU_BB_PLAYER_LIST;
    private static final String SELECT_ALL_BU_BB_PLAYER_COUNT;

    static
    {
	TABLE_NAME = "ALL_BU_BB_PLAYERS_V";
        SELECT_ALL_BU_BB_PLAYER_LIST =
            "SELECT a.bb_id, a.bb_hwrev, a.bb_model, a.bundle_start_date, a.bu_id, " +
            "b.business_name, a.public_key, a.manufacture_date, a.sn FROM " + TABLE_NAME +
            " a, BUSINESS_UNITS b WHERE a.bu_id = b.bu_id";
        SELECT_ALL_BU_BB_PLAYER_COUNT = "select count(*) FROM " + TABLE_NAME;
    }

    /**
     * Constructs a AllBUBBPlayerFactory instance.
     */
    protected AllBUBBPlayerFactory()
    {
	super();
    }

   /**
    * Returns the bb player information identified by the given bbID.
    *
    * @param   bbID                    the bb player identifier
    *
    * @return The XML document that contains information of a
    *          IBUBBPlayer record.
    *
    * @throws  DBException
    * @throws  IOException
    */
   public String getAllBUBBPlayer(long bbID)
       throws DBException, IOException
   {
       return query(SELECT_ALL_BU_BB_PLAYER_LIST + " AND a.bb_id = " + bbID);
   }

   /**
    * Returns the bb player information identified by the given bbID.
    *
    * @param   bbID                    the bb player identifier
    *
    * @return The XML document that contains information of a
    *          IBUBBPlayer record.
    *
    * @throws  DBException
    * @throws  IOException
    */
   public String getAllBUBBPlayer(String bbID)
       throws DBException, IOException
   {
       return query(SELECT_ALL_BU_BB_PLAYER_LIST + " AND a.bb_id = " + bbID);
   }

   /** Returns the number of bb players available.
    *
    * @return The integer that contains the number of bb players.
    *
    * @throws  DBException
    * @throws  IOException
    */
   public int getAllBUBBPlayerCount()
       throws DBException, IOException
   {
       return count(SELECT_ALL_BU_BB_PLAYER_COUNT);
   }

    /**
     * Returns the database table name to be used.
     *
     * @return The data table name.
     */
    public String getTableName()
    {
        return TABLE_NAME;
    }

    /**
     * Creates a AllBUBBPlayer JavaBeans object.
     *
     * @return	The newly created AllBUBBPlayer object.
     */
    protected Bean newBean()
    {
	return new AllBUBBPlayer();
    }
}

package com.broadon.db;

import com.broadon.exception.BroadOnException;

/**
 * The <code>DBException</code> class is the top level of database related
 * exception hierarchy.
 *
 * @version	$Revision: 1.1 $
 */
public class DBException
    extends BroadOnException
{
    /**
     * Constructs a DBException instance.
     */
    public DBException()
    {
	super();
    }

    /**
     * Constructs a DBException instance.
     *
     * @param	message			the exception message
     */
    public DBException(String message)
    {
	super(message);
    }

    /**
     * Constructs a DBException instance.
     *
     * @param	message			the exception message
     * @param	throwable		the nested exception
     */
    public DBException(String message, Throwable throwable)
    {
	super(message, throwable);
    }
}

package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <c>BBModel</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *<p>
 * It contains information about a bb models.
 *
 * @version	$Revision: 1.2 $
 */
public class BBModel
    extends PBean
{
    private String		bbModel;
    private String		description;
    private Date		      createDate;

    /**
     * Constructs an empty BBModel instance.
     */
    public BBModel()
    {
	this.bbModel = null;
	this.createDate = null;
    }

    /**
     * Getter for bbModel.
     *
     * @return	The bb model.
     */
    public String getBBModel()
    {
	return bbModel;
    }

    /**
     * Setter for bbModel.
     *
     * @param	bbModel		the bb model
     */
    public void setBBModel(String bbModel)
    {
	this.bbModel= bbModel;
    }

    /**
     * Getter for description.
     *
     * @return	The description.
     */
    public String getDescription()
    {
	return description;
    }

    /**
     * Setter for description.
     *
     * @param	description		the description
     */
    public void setDescription(String description)
    {
	this.description = description;
    }

    /**
     * Getter for createDate.
     *
     * @return	The date of creation.
     */
    public Date getCreateDate()
    {
	return createDate;
    }

    /**
     * Setter for createDate.
     *
     * @param	createDate		the date of creation
     */
    public void setCreateDate(Date createDate)
    {
	this.createDate = createDate;
    }
}

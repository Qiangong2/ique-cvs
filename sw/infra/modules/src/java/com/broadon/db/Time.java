package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <c>Time</c> class conforms to the JavaBeans property, and
 * is mainly used for date/time manipulation.
 *
 * @version	$Revision: 1.1 $
 */
public class Time
    extends PBean
{
    private Date		sysdate;

    /**
     * Constructs an empty Time instance.
     */
    public Time()
    {
	this.sysdate = null;
    }

    /**
     * Getter for sysdate.
     *
     * @return	The sysdate.
     */
    public Date getSysdate()
    {
	return sysdate;
    }

    /**
     * Setter for sysdate.
     *
     * @param	sysdate			the sysdate
     */
    public void setSysdate(Date sysdate)
    {
	this.sysdate = sysdate;
    }
}

package com.broadon.db;

import java.io.IOException;
import java.util.Map;

import com.broadon.bean.Bean;

/**
 * The <code>RegionalServerFactory</code> class interfaces with the database
 * to create, update, or query records from the RegionalServers table.
 * Each record is represented by a RegionalServer JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.3 $
 */
public class RegionalServerFactory
    extends DBAccessFactory
{
    private static final String TABLE_NAME;
    private static final String SELECT_REGIONAL_SERVERS_LIST;
    private static final String SELECT_REGIONAL_SERVERS_COUNT;

    static
    {
	TABLE_NAME			= "REGIONAL_SERVERS";
        SELECT_REGIONAL_SERVERS_LIST     = "select * from " + TABLE_NAME;
        SELECT_REGIONAL_SERVERS_COUNT     = "select count(*) from " + TABLE_NAME;
    }

    /**
     * Constructs a RegionalServerFactory instance.
     */
    protected RegionalServerFactory()
    {
	super();
    }

    /**
     * Returns the regional center identified by the given regional center
     * identifier and server type.
     *
     * @param	regionalCenterID	the regional center identifier
     * @param	serverType		the server type
     * @param	nameMap			the name mapping table
     *
     * @return	The RegionalServer JavaBeans that contains information of a
     *		Regional-Servers record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public RegionalServer getRegionalServer(int regionalCenterID,
					    String serverType,
					    Map nameMap)
	throws DBException, IOException
    {
	RegionalCenterID	predicate;

	predicate = new RegionalCenterID(regionalCenterID);
	predicate.and(new ServerType(serverType));
	return (RegionalServer)queryUnique(predicate, nameMap);
    }

    /**
     * Returns the regional server identified by the given regional center
     * identifier.
     *
     * @param	regionalCenterID	the regional center identifier
     * @param	nameMap			the name mapping table
     *
     * @return	The RegionalServer JavaBeans that contain information of a
     *		Regional-Servers record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public RegionalServer[] getRegionalServers(int regionalCenterID,
					       Map nameMap)
	throws DBException, IOException
    {
	RegionalCenterID	predicate;

	predicate = new RegionalCenterID(regionalCenterID);

	Bean[]			beans = query(predicate, nameMap);

	if (beans == null)
	    return null;

	int			count = beans.length;
	RegionalServer[]	regionalServers = new RegionalServer[count];

	for (int n = 0; n < count; n++)
	    regionalServers[n] = (RegionalServer)beans[n];
	return regionalServers;
    }

    /**
     * Returns the regional center with the given id.
     *
     * @param   regionalCenterID                 the regional center identifier
     *
     * @return The XML document describing the information of regional
     *          server information.
     *
     * @throws  DBException
     * @throws  IOException
     */
     public String getRegionalServers(String regionalCenterID)
         throws DBException, IOException
     {
         String  sql = SELECT_REGIONAL_SERVERS_LIST +
                        " where regional_center_id = " + regionalCenterID;
         return query(sql);
     }

    /**
     * Returns the number of regional centers available.
     *
     * @param   regionalCenterID        the regional center identifier
     *
     * @return The integer that contains the number of regional centers.
     *
     * @throws  DBException
     * @throws  IOException
     */
    public int getRegionalServersCount(String regionalCenterID)
        throws DBException, IOException
    {
        return count(SELECT_REGIONAL_SERVERS_COUNT +
                        " where regional_center_id = " + regionalCenterID);
    }
    
    /**
     * Returns the database table name to be used.
     *
     * @return	The data table name.
     */
    public String getTableName()
    {
	return TABLE_NAME;
    }

    /**
     * Creates a RegionalCenter JavaBeans object.
     *
     * @return	The newly created RegionalCenter object.
     */
    protected Bean newBean()
    {
	return new RegionalServer();
    }
}

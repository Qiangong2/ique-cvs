package com.broadon.db;

import java.io.IOException;
import java.util.Map;

import com.broadon.bean.Bean;

/**
 * The <c>ETicketCRLFactory</c> class interfaces with the database to
 * create, update, or query records from the content-objects table.
 * Each record is represented by a ETicketCRL JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.2 $
 */
public class ETicketCRLFactory
    extends SyncableFactory
{
    /**
     * Constructs a ETicketCRLFactory instance.
     */
    protected ETicketCRLFactory()
    {
	super();
    }

    /**
     * Retrieves the eTicket CRL record. There should at most be one.
     *
     * @param	nameMap			the name mapping table
     *
     * @throws	DBException
     * @throws	IOException
     */
    public ETicketCRL getETicketCRL(Map nameMap)
	throws DBException, IOException
    {
	return (ETicketCRL)queryUnique(null, nameMap);
    }

    /**
     * Returns the database table name to be used.
     *
     * @return	The data table name.
     */
    public String getTableName()
    {
	return "eticket_crl";
    }

    /**
     * Creates a ETicketCRL JavaBeans object.
     *
     * @return	The newly created ETicketCRL object.
     */
    protected Bean newBean()
    {
	return new ETicketCRL();
    }
}

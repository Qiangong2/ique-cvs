package com.broadon.db;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

import com.broadon.bean.Bean;

/**
 * The <c>ContentDownloadLogFactory</c> class interfaces with the database to
 * create, update, or query records from the content-download-logs table.
 * Each record is represented by a ContentDownloadLog JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.1 $
 */
public class ContentDownloadLogFactory
    extends DBAccessFactory
{
    /**
     * Constructs a ContentDownloadLogFactory instance.
     */
    protected ContentDownloadLogFactory()
    {
	super();
    }

    /**
     * Returns the ContentDownloadLog information, based on the given depotID,
     * contentID, and requestDate.
     *
     * @param	depotID			the depot identifier
     * @param	contentID		the content identifier
     * @param	requestDate		the date of the request
     * @param	nameMap			the name mapping table
     *
     * @return	The Depot JavaBeans containing all information of
     *		a Depot record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public ContentDownloadLog getContentDownloadLog(long depotID,
						    long contentID,
						    Date requestDate,
						    Map nameMap)
	throws DBException, IOException
    {
	DepotID		predicate = new DepotID(depotID);

	predicate.and(new ContentID(contentID))
		 .and(new RequestDate(requestDate));
	return (ContentDownloadLog)queryUnique(predicate, nameMap);
    }

    /**
     * Returns the database table name to be used.
     *
     * @return	The data table name.
     */
    public String getTableName()
    {
	return "content_download_logs";
    }

    /**
     * Creates a Depot JavaBeans object.
     *
     * @return	The newly created Depot object.
     */
    protected Bean newBean()
    {
	return new ContentDownloadLog();
    }
}

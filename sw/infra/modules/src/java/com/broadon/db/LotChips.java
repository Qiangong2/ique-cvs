package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <c>LotChips</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *<p>
 * It contains information about lots identified by the lotNumber identifier.
 *
 * @version	$Revision: 1.2 $
 */
public class LotChips
    extends PBean
{
    private Long		bbID;
    private String		lotNumber;
    private Date		processDate;
    private String		cert;

    /**
     * Constructs an empty LotChips instance.
     */
    public LotChips()
    {
    }

    /**
     * Getter for bbID.
     *
     * @return The bb identifier.
     */
    public Long getBBID()
    {
        return bbID;
    }

    /**
     * Setter for bbID.
     *
     * @param   bbID            the bb identifier
     */
    public void setBBID(Long bbID)
    {
        this.bbID = bbID;
    }

    /**
     * Getter for lotNumber.
     *
     * @return	The lot identifier.
     */
    public String getLotNumber()
    {
	return lotNumber;
    }

    /**
     * Setter for lotNumber.
     *
     * @param	lotNumber			the lot identifier
     */
    public void setLotNumber(String lotNumber)
    {
	this.lotNumber = lotNumber;
    }

    /**
     * Getter for processDate.
     *
     * @return	The last processed date for this lot.
     */
    public Date getProcessDate()
    {
	return processDate;
    }

    /**
     * Setter for processDate.
     *
     * @param	processDate	the last processed date for this lot
     */
    public void setProcessDate(Date processDate)
    {
	this.processDate = processDate;
    }

    /**
     * Getter for cert.
     *
     * @return	The certificate.
     */
    public String getCert()
    {
	return cert;
    }

    /**
     * Setter for cert.
     *
     * @param	cert		the certificate
     */
    public void setCert(String cert)
    {
	this.cert = cert;
    }
}

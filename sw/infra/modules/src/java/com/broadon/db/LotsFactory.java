package com.broadon.db;

import java.io.IOException;
import java.util.Map;

import com.broadon.bean.Bean;
import com.broadon.sql.Predicate;

/**
 * The <c>LotsFactory</c> class interfaces with the database to
 * create, update, or query records from the Lotss table.
 * Each record is represented by a Lots JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.3 $
 */
public class LotsFactory
    extends SyncableFactory
{
    private static final String TABLE_NAME;
    private static final String	SELECT_LOTS_LIST;
    private static final String	SELECT_LOTS_COUNT;

    static
    {
	TABLE_NAME		= "LOTS";
	SELECT_LOTS_LIST	=
	    "select lot_number, lot_size, create_date, last_processed, last_reported, " +
		"chip_rev, cert_id, manufacturer, location, num_processed from " + TABLE_NAME;
	SELECT_LOTS_COUNT	= "select count(*) from " + TABLE_NAME;
    }

    /**
     * Constructs a LotsFactory instance.
     */
    protected LotsFactory()
    {
	super();
    }

    /**
     * Returns the request information identified by the given requestID.
     *
     * @param	lotNumber		the lot identifier
     * @param	nameMap			the name mapping table
     *
     * @return	The Lots JavaBeans that contains information of a
     *		Lotss record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public Lots getLots(String lotNumber, Map nameMap)
	throws DBException, IOException
    {
	LotsID		predicate = new LotsID(lotNumber);

	return (Lots)queryUnique(predicate, nameMap);
    }

    /**
     * Returns the request information identified by the given requestID.
     *
     * @param	lotNumber			the lot identifier
     *
     * @return	The XML document that contains information of a
     *		Lotss record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getLots(String lotNumber)
	throws DBException, IOException
    {
	return query(SELECT_LOTS_LIST + " where lot_number = " + lotNumber);
    }

    /**
     * Returns the request information identified by the given array of
     * lotNumbers.
     *
     * @param	lotNumbers		the lot identifiers
     * @param	nameMap			the name mapping table
     *
     * @return	The array of Lots JavaBeans, each contains
     *		all information of a Lots record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public Lots[] getLots(String[] lotNumbers, Map nameMap)
	throws DBException, IOException
    {
	if (lotNumbers == null)
	{
	    return null;
	}

	int		size = lotNumbers.length;

	if (size == 0)
	{
	    return null;
	}
	/*
	 * Set up the predicate.
	 */
	LotsID		lotNumber = new LotsID(lotNumbers[0]);
	Predicate	predicate = lotNumber;

	for (int n = 1; n < size; n++)
	{
	    lotNumber = (LotsID)lotNumber.or(new LotsID(lotNumbers[n]));
	}
	/*
	 * Query.
	 */
	return (Lots[])query(predicate, nameMap);
    }

    /**
     * Returns the request information of a given range.
     *
     * @param	lotNumber		the lot identifier
     * @param	from			the starting record number
     * @param	to			the ending record number
     * @param	order			the sorted order
     *
     * @return	The XML document that contains the tilte information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getLots(String lotNumber, int from, int to, String order)
	throws DBException, IOException
    {
	String	sql = "select * from (select rownum no, inner.* from (" +
			SELECT_LOTS_LIST +
			    " where lot_number = " + lotNumber +
			    " order by " + order + ") " +
			"inner where rownum <= " + to + ") " +
			"where no > " + from;

	return query(sql);
    }

    /**
     * Returns the request information of a given range.
     *
     * @param	from			the starting record number
     * @param	to			the ending record number
     * @param	order			the sorted order
     *
     * @return	The XML document that contains the tilte information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getLots(int from, int to, String order)
	throws DBException, IOException
    {
	String	sql = "select * from (select rownum no, inner.* from (" +
			SELECT_LOTS_LIST + " order by " + order + ") " +
			"inner where rownum <= " + to + ") " +
			"where no > " + from;

	return query(sql);
    }

    /**
     * Returns the number of requests available.
     *
     * @param	lotNumber			the lot identifier
     *
     * @return	The integer that contains the number of requests.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getLotsCount(String lotNumber)
	throws DBException, IOException
    {
	return count(SELECT_LOTS_COUNT + " where lot_number = " + lotNumber);
    }

    /**
     * Returns the number of requests available.
     *
     * @return	The integer that contains the number of requests.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getLotsCount()
	throws DBException, IOException
    {
	return count(SELECT_LOTS_COUNT);
    }

    /**
     * Returns the database table name to be used.
     *
     * @return	The data table name.
     */
    public String getTableName()
    {
	return TABLE_NAME;
    }

    /**
     * Creates a Lots JavaBeans object.
     *
     * @return	The newly created Lots object.
     */
    protected Bean newBean()
    {
	return new Lots();
    }
}

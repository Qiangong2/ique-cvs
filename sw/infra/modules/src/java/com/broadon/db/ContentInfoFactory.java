package com.broadon.db;

import java.io.IOException;
import java.util.Map;

import com.broadon.bean.Bean;

/**
 * The <c>ContentInfoFactory</c> class interfaces with the database to
 * create, update, or query records from the content-objects table.
 * Each record is represented by a ContentInfo JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.9 $
 */
public class ContentInfoFactory
    extends ContentBaseFactory
{
    private static final String	SELECT_ATTRIBUTES;

    static
    {
	SELECT_ATTRIBUTES = "select " +
	    "content_id, publish_date, revoke_date, " +
	    "replaced_content_id, content_object_type, " +
	    "content_size, content_checksum, " +
	    "content_object_name, " +
	    "content_object_version, " +
	    "eticket_object, " +
	    "last_updated, " +
	    "min_upgrade_version, " +
	    "upgrade_consent";
    }

    /**
     * Constructs a ContentInfoFactory instance.
     */
    protected ContentInfoFactory()
    {
	super();
    }

    /**
     * Returns the select part of the query.
     */
    protected String getSelectPart()
    {
	return SELECT_ATTRIBUTES + " from " + getTableName();
    }

    /**
     * Returns the content information identified.
     *
     * @param   contentID               the content identifier
     * @param   nameMap                 the name mapping table
     *
     * @return The Content JavaBeans that contains information of a
     *          Content record.
     *
     * @throws  DBException
     * @throws  IOException
     */
    public ContentInfo getLatestContentForGlobalETicket(long contentID, Map nameMap)
        throws DBException, IOException
    {
        String sql = getSelectPart() + " WHERE content_id = (SELECT content_id FROM " +
            "CONTENT_TITLE_OBJECT_DETAIL_V WHERE title_id = (SELECT title_id FROM " +
            "CONTENT_TITLE_OBJECTS where content_id = ?) AND content_object_name = (SELECT " +
            "content_object_name FROM CONTENT_OBJECTS WHERE content_id = " + contentID + 
            ") AND is_content_object_latest = 'Y' AND has_eticket_metadata = 'Y')"; 

        return (ContentInfo)queryUnique(new ContentID(contentID), sql, nameMap);
    }

    /**
     * Creates a ContentInfo JavaBeans object.
     *
     * @return	The newly created ContentInfo object.
     */
    protected Bean newBean()
    {
	return new ContentInfo();
    }
}

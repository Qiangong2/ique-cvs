package com.broadon.db;

import java.io.IOException;

import com.broadon.bean.Bean;

/**
 * The <code>BBHwReleaseFactory</code> abstract class interfaces with the
 * database to create, update, or query records from the bb-hw-releases
 * table.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.5 $
 */
public class BBHwReleaseFactory
    extends SyncableFactory
{
    private static final String TABLE_NAME;
    private static final String	SELECT_BB_HW_RELEASES_LIST;
    private static final String	SELECT_BB_HW_RELEASES_COUNT;

    static
    {
	TABLE_NAME		= "BB_HW_RELEASES";
	SELECT_BB_HW_RELEASES_LIST =
	    "select bb_hwrev, bb_model, chip_rev, secure_content_id, " +
		"last_updated, description, boot_content_id, diag_content_id from " + TABLE_NAME;
	SELECT_BB_HW_RELEASES_COUNT = "select count(*) from " + TABLE_NAME;
    }

    /**
     * Constructs a BBHwReleaseFactory instance.
     */
    protected BBHwReleaseFactory()
    {
	super();
    }

    /**
     * Returns the BB HW Releases information of a given range.
     *
     * @param	from			the starting record number
     * @param	to			the ending record number
     * @param	order			the sorted order
     *
     * @return	The XML document that contains the BB Hw releases information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getBBHwReleases(int from, int to, String order)
	throws DBException, IOException
    {
	String	sql = "select * from (select rownum no, inner.* from (" +
			SELECT_BB_HW_RELEASES_LIST + " order by " + order + 
                        ") inner where rownum <= " + to + ") " +
			"where no > " + from;
	return query(sql);
    }

    /**
     * Returns the number of BB HW Releases available.
     *
     * @return	The integer that contains the number of BB HW Releases.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getBBHwReleasesCount()
	throws DBException, IOException
    {
	return count(SELECT_BB_HW_RELEASES_COUNT);
    }

    /**
     * Returns the database table name to be used.
     *
     * @return	The data table name.
     */
    public String getTableName()
    {
	return TABLE_NAME;
    }

    /**
     * Creates a Content JavaBeans object.
     *
     * @return	The newly created Content object.
     */
    protected Bean newBean()
    {
	return new BBHwRelease();
    }
}



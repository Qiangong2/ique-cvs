package com.broadon.db;

import java.io.IOException;
import java.util.Map;

import com.broadon.bean.Bean;

/**
 * The <c>DepotFactory</c> class interfaces with the database to
 * create, update, or query records from the Depots table.
 * Each record is represented by a Depot JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.4 $
 */
public class DepotFactory
    extends DBAccessFactory
{
    /**
     * Constructs a DepotFactory instance.
     */
    protected DepotFactory()
    {
	super();
    }

    /**
     * Returns the depot information, based on the given depotID.
     *
     * @param	depotID			the depot identifier
     * @param	nameMap			the name mapping table
     *
     * @return	The Depot JavaBeans containing all information of
     *		a Depot record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public Depot getDepot(long depotID, Map nameMap)
	throws DBException, IOException
    {
	DepotID		predicate = new DepotID(depotID);

	return (Depot)queryUnique(predicate, nameMap);
    }

    /**
     * Returns the database table name to be used.
     *
     * @return	The data table name.
     */
    public String getTableName()
    {
	return "depots";
    }

    /**
     * Creates a Depot JavaBeans object.
     *
     * @return	The newly created Depot object.
     */
    protected Bean newBean()
    {
	return new Depot();
    }
}

package com.broadon.db;

/**
 * The <code>NotUniqueException</code> class indicates that
 * this thread is not yet in a transaction.
 *
 * @version	$Revision: 1.1 $
 */
public class NotUniqueException
    extends DBException
{
    /**
     * Constructs a NotUniqueException instance.
     */
    public NotUniqueException()
    {
	super();
    }

    /**
     * Constructs a NotUniqueException instance.
     *
     * @param	message			the exception message
     */
    public NotUniqueException(String message)
    {
	super(message);
    }

    /**
     * Constructs a NotUniqueException instance.
     *
     * @param	message			the exception message
     * @param	throwable		the nested exception
     */
    public NotUniqueException(String message, Throwable throwable)
    {
	super(message, throwable);
    }
}

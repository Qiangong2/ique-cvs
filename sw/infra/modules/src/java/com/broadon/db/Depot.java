package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <c>Depot</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *<p>
 * It contains information about a depot identified by the depot identifier.
 *
 * @version	$Revision: 1.1 $
 */
public class Depot
    extends PBean
{
    private Long		depotID;
    private Integer		storeID;
    private Date		createDate;
    private Date		suspendDate;
    private Date		revokeDate;
    private Integer		csFlag;
    private String		publicKey;

    /**
     * Constructs an empty Depot instance.
     */
    public Depot()
    {
    }

    /**
     * Getter for depotID.
     *
     * @return	The depot identifier.
     */
    public Long getDepotID()
    {
	return depotID;
    }

    /**
     * Setter for depotID.
     *
     * @param	depotID			the depot identifier
     */
    public void setDepotID(Long depotID)
    {
	this.depotID = depotID;
    }

    /**
     * Getter for storeID.
     *
     * @return	The store identifier.
     */
    public Integer getStoreID()
    {
	return storeID;
    }

    /**
     * Setter for storeID.
     *
     * @param	storeID			the store identifier
     */
    public void setStoreID(Integer storeID)
    {
	this.storeID = storeID;
    }

    /**
     * Getter for createDate.
     *
     * @return	The creation date of this depot.
     */
    public Date getCreateDate()
    {
	return createDate;
    }

    /**
     * Setter for createDate.
     *
     * @param	createDate		the creation date of this depot
     */
    public void setCreateDate(Date createDate)
    {
	this.createDate = createDate;
    }

    /**
     * Getter for suspendDate.
     *
     * @return	The suspended date of this depot.
     */
    public Date getSuspendDate()
    {
	return suspendDate;
    }

    /**
     * Setter for suspendDate.
     *
     * @param	suspendDate		the suspended date of this depot
     */
    public void setSuspendDate(Date suspendDate)
    {
	this.suspendDate = suspendDate;
    }

    /**
     * Getter for revokeDate.
     *
     * @return	The revoke date of this depot.
     */
    public Date getRevokeDate()
    {
	return revokeDate;
    }

    /**
     * Setter for revokeDate.
     *
     * @param	revokeDate		the revoke date of this depot
     */
    public void setRevokeDate(Date revokeDate)
    {
	this.revokeDate = revokeDate;
    }

    /**
     * Getter for csFlag.
     *
     * @return	The status flag of this depot.
     */
    public Integer getCSFlag()
    {
	return csFlag;
    }

    /**
     * Setter for csFlag.
     *
     * @param	csFlag			the status flag of this depot
     */
    public void setCSFlag(Integer csFlag)
    {
	this.csFlag = csFlag;
    }

    /**
     * Getter for publicKey.
     *
     * @return	The public key of this depot.
     */
    public String getPublicKey()
    {
	return publicKey;
    }

    /**
     * Setter for publicKey.
     *
     * @param	publicKey		the public key of this depot
     */
    public void setPublicKey(String publicKey)
    {
	this.publicKey = publicKey;
    }
}

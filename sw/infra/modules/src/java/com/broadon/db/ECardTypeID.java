package com.broadon.db;

import com.broadon.sql.PredicateItem;

/**
 * The <c>ECardTypeID</c> class conforms to the JavaBeans property,
 * and is a Predicate that finds various records.
 *
 * @version	$Revision: 1.1 $
 */
public class ECardTypeID
    extends PredicateItem
{
    private Integer	eCardTypeID;

    /**
     * Constructs a ECardTypeID instance.
     *
     * @param	eCardTypeID		the eCardType identifier
     */
    public ECardTypeID(int eCardTypeID)
    {
	super();
	this.eCardTypeID = new Integer(eCardTypeID);
    }

    /**
     * Getter for eCardTypeID.
     *
     * @return	The eCardType identifier.
     */
    public Integer getECardTypeID()
    {
	return eCardTypeID;
    }

    /**
     * Setter for eCardTypeID.
     *
     * @param	eCardTypeID		the eCardType identifier
     */
    public void setECardTypeID(Integer eCardTypeID)
    {
	this.eCardTypeID = eCardTypeID;
    }
}

package com.broadon.db;

import java.io.IOException;
import java.util.Map;

import com.broadon.bean.Bean;

/**
 * The <c>ECardTypeFactory</c> class interfaces with the database to
 * create, update, or query records from the ECardTypes table.
 * Each record is represented by a ECardType JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.4 $
 */
public class ECardTypeFactory
    extends SyncableFactory
{
    private static final String TABLE_NAME;
    private static final String	SELECT_ECARD_TYPES_LIST;
    private static final String	SELECT_ECARD_TYPES_COUNT;

    static
    {
	TABLE_NAME		= "ECARD_TYPES";
	SELECT_ECARD_TYPES_LIST =
	    "select ecard_type, eunits, is_usedonce, is_prepaid, " +
	    "allow_refill, last_updated, description, title_id " + 
            "from " + TABLE_NAME;  
	SELECT_ECARD_TYPES_COUNT = "select count(*) from " + TABLE_NAME;
    }

    /**
     * Constructs a ECardTypeFactory instance.
     */
    protected ECardTypeFactory()
    {
	super();
    }

    /**
     * Returns the eCard type information, based on the given eCardTypeID.
     *
     * @param	eCardTypeID		the eCard type identifier
     * @param	nameMap			the name mapping table
     *
     * @return	The ECardType JavaBeans containing all information of
     *		a ECardType record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public ECardType getECardType(int eCardTypeID, Map nameMap)
	throws DBException, IOException
    {
	ECardTypeID		predicate = new ECardTypeID(eCardTypeID);

	return (ECardType)queryUnique(predicate, nameMap);
    }

    /**
     * Returns the ECard Types information of a given range.
     *
     * @param	from			the starting record number
     * @param	to			the ending record number
     * @param	order			the sorted order
     *
     * @return	The XML document that contains the ECard Types information.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String getECardTypes(int from, int to, String order)
	throws DBException, IOException
    {
	String	sql = "select * from (select rownum no, inner.* from (" +
			SELECT_ECARD_TYPES_LIST + " order by " + order + 
                        ") inner where rownum <= " + to + ") " +
			"where no > " + from;
	return query(sql);
    }

    /**
     * Returns the number of ECard Types available.
     *
     * @return	The integer that contains the number of ECard Types.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public int getECardTypesCount()
	throws DBException, IOException
    {
	return count(SELECT_ECARD_TYPES_COUNT);
    }

    /**
     * Returns the database table name to be used.
     *
     * @return	The data table name.
     */
    public String getTableName()
    {
	return TABLE_NAME;
    }

    /**
     * Creates a ECardType JavaBeans object.
     *
     * @return	The newly created ECardType object.
     */
    protected Bean newBean()
    {
	return new ECardType();
    }
}

package com.broadon.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <c>TitleReview</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *<p>
 * It contains reviews information on a title, identified by the title
 * identifier, the name of the reviewer, and the review date.
 *
 * @version	$Revision: 1.1 $
 */
public class TitleReview
    extends PBean
{
    private Long		titleID;
    private String		reviewedBy;
    private Date		reviewDate;
    private Float		ratings;
    private String		reviewTitle;
    private String		reviewDetail;
    private Date		revokeDate;
    private Date		lastUpdated;

    /**
     * Constructs an empty TitleReview instance.
     */
    public TitleReview()
    {
    }

    /**
     * Getter for titleID.
     *
     * @return	The title identifier.
     */
    public Long getTitleID()
    {
	return titleID;
    }

    /**
     * Setter for titleID.
     *
     * @param	titleID			the title identifier
     */
    public void setTitleID(Long titleID)
    {
	this.titleID = titleID;
    }

    /**
     * Getter for reviewedBy.
     *
     * @return	The reviewer of this review.
     */
    public String getReviewedBy()
    {
	return reviewedBy;
    }

    /**
     * Setter for reviewedBy.
     *
     * @param	reviewedBy		the reviewer of this review
     */
    public void setReviewedBy(String reviewedBy)
    {
	this.reviewedBy = reviewedBy;
    }

    /**
     * Getter for reviewDate.
     *
     * @return	The date of this review.
     */
    public Date getReviewDate()
    {
	return reviewDate;
    }

    /**
     * Setter for reviewDate.
     *
     * @param	reviewDate		the date of this review
     */
    public void setReviewDate(Date reviewDate)
    {
	this.reviewDate = reviewDate;
    }

    /**
     * Getter for ratings.
     *
     * @return	The ratings given by this review.
     */
    public Float getRatings()
    {
	return ratings;
    }

    /**
     * Setter for ratings.
     *
     * @param	ratings			the ratings given by this review
     */
    public void setRatings(Float ratings)
    {
	this.ratings = ratings;
    }

    /**
     * Getter for reviewTitle.
     *
     * @return	The title of this review.
     */
    public String getReviewTitle()
    {
	return reviewTitle;
    }

    /**
     * Setter for reviewTitle.
     *
     * @param	reviewTitle		the title of this review
     */
    public void setReviewTitle(String reviewTitle)
    {
	this.reviewTitle = reviewTitle;
    }

    /**
     * Getter for reviewDetail.
     *
     * @return	The review detail.
     */
    public String getReviewDetail()
    {
	return reviewDetail;
    }

    /**
     * Setter for reviewDetail.
     *
     * @param	reviewDetail		the review detail
     */
    public void setReviewDetail(String reviewDetail)
    {
	this.reviewDetail = reviewDetail;
    }

    /**
     * Getter for revoke date.
     *
     * @return	The revoke date of this review.
     */
    public Date getRevokeDate()
    {
	return revokeDate;
    }

    /**
     * Setter for revoke date.
     *
     * @param	revokeDate		the revoke date of this review
     */
    public void setRevokeDate(Date revokeDate)
    {
	this.revokeDate = revokeDate;
    }

    /**
     * Getter for lastUpdated.
     *
     * @return	The last updated date of this review.
     */
    public Date getLastUpdated()
    {
	return lastUpdated;
    }

    /**
     * Setter for lastUpdated.
     *
     * @param	lastUpdated		the last updated date of this review
     */
    public void setLastUpdated(Date lastUpdated)
    {
	this.lastUpdated = lastUpdated;
    }
}

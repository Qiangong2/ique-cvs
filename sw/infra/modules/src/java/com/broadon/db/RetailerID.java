package com.broadon.db;

import com.broadon.sql.PredicateItem;

/**
 * The <c>RetailerID</c> class conforms to the JavaBeans property,
 * and is a Predicate that finds various records.
 *
 * @version	$Revision: 1.1 $
 */
public class RetailerID
    extends PredicateItem
{
    private Integer	retailerID;

    /**
     * Constructs a RetailerID instance.
     *
     * @param	retailerID		the retailer identifier
     */
    public RetailerID(int retailerID)
    {
	super();
	this.retailerID = new Integer(retailerID);
    }

    /**
     * Getter for retailerID.
     *
     * @return	The retailer identifier.
     */
    public Integer getRetailerID()
    {
	return retailerID;
    }

    /**
     * Setter for retailerID.
     *
     * @param	retailerID		the retailer identifier
     */
    public void setRetailerID(Integer retailerID)
    {
	this.retailerID = retailerID;
    }
}

package com.broadon.db;


/**
 * The <code>ContentBaseFactory</code> abstract class interfaces with the
 * database to create, update, or query records from the content-objects
 * table.
 * This is the super class of <code>ContentInfoFactory</code> and
 * <code>ContentFactory</code>.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.7 $
 */
public abstract class ContentBaseFactory
    extends SyncableFactory
{
    protected static final String TABLE_NAME = "CONTENT_OBJECTS";

    /**
     * Constructs a ContentBaseFactory instance.
     */
    protected ContentBaseFactory()
    {
	super();
    }

    /**
     * Returns the database table name to be used.
     *
     * @return	The data table name.
     */
    public String getTableName()
    {
        return TABLE_NAME;
    }
}

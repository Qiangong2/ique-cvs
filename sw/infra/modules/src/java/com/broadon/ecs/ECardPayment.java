package com.broadon.ecs;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import com.broadon.wsapi.ecs.ECardPaymentType;
import com.broadon.wsapi.ecs.AbstractRequestType;
import com.broadon.wsapi.ecs.PaymentMethodType;
import com.broadon.wsapi.pas.AuthorizeECardPaymentRequestType;
import com.broadon.wsapi.pas.AuthorizeECardPaymentResponseType;
import com.broadon.wsapi.pas.BalanceType;
import com.broadon.wsapi.pas.CaptureECardToAccountRequestType;
import com.broadon.wsapi.pas.CaptureECardToAccountResponseType;
import com.broadon.wsapi.pas.CheckECardBalanceRequestType;
import com.broadon.wsapi.pas.CheckECardBalanceResponseType;
import com.broadon.wsapi.pas.ECardRecordType;
import com.broadon.wsapi.pas.GetECardTypeRequestType;
import com.broadon.wsapi.pas.GetECardTypeResponseType;
import com.broadon.wsapi.pas.MoneyType;
import com.broadon.wsapi.pas.PaymentAuthorizationPortType;

/**
 * The <code>ECardPayment</code> class handles a payment
 * using a ECard.
 *
 * @version $Revision: 1.17 $
 */
public class ECardPayment extends Payment {
    private static final String GET_ECARD_DEFAULT_BALANCE =
        "SELECT DEFAULT_BALANCE, CURRENCY FROM ECARD_TYPES WHERE ECARD_TYPE=?";
    
    protected static NumberFormat ECARD_TYPE_FORMAT = 
        new DecimalFormat("000000");
    
    com.broadon.wsapi.pas.ECardInfoType pasECardInfo;
    com.broadon.wsapi.ecs.ECardInfoType eCardInfo;
    ECardRecordType eCard;
    int eCardType;
    
    public ECardPayment(AbstractRequestType request,
                        PaymentAuthorizationPortType pas,
                        ECardPaymentType eCard,
                        boolean checkECard)
        throws RemoteException, InvalidPaymentException
    {
        super(pas, null);
        this.eCardInfo = new com.broadon.wsapi.ecs.ECardInfoType();
        this.eCard = new ECardRecordType();
        this.eCard.setECardNumber(eCard.getECardNumber());
        this.eCard.setECardType(eCard.getECardType());
        this.eCard.setHashNumber(eCard.getECardHash());
        this.eCard.setCountryCode(request.getCountryCode());
        try {
            eCardType = Integer.parseInt(eCard.getECardType());
        } catch (Throwable e) {
            throw new InvalidPaymentException("Invalid eCardType");
        }
        if (checkECard) {
            checkECardType(request);
        }
    }
    
    public ECardPayment(AbstractRequestType request,
                        PaymentAuthorizationPortType pas,
                        ECardPaymentType eCard)
        throws RemoteException, InvalidPaymentException
    {
        this(request, pas, eCard, true);
    }
    
    public com.broadon.wsapi.ecs.ECardInfoType getECardInfo()
    {
        return eCardInfo;
    }
        
    public String getPaymentType()
    {
        return PaymentMethodType.ECARD.getValue();
    }
    
    public String getPaymentMethodId()
    {
        return eCard.getECardNumber();
    }
    
    public BalanceType getBalance()
    {        
        return (pasECardInfo != null)? pasECardInfo.getBalance(): null;
    }
    
    public boolean checkBalance(AbstractRequestType request)
        throws RemoteException
    {
        CheckECardBalanceRequestType checkBalanceRequest = 
            new CheckECardBalanceRequestType();
        ECommerceFunction.initPasRequest(request, checkBalanceRequest);
        
        checkBalanceRequest.setECardID(eCard);

        CheckECardBalanceResponseType checkBalanceResponse =
            pas.checkECardBalance(checkBalanceRequest);

        pasECardInfo = checkBalanceResponse.getECardInfo();
        if (pasECardInfo != null) {
            eCardInfo.setActivatedTime(pasECardInfo.getActivatedTime());
            eCardInfo.setRevokedTime(pasECardInfo.getRevokedTime());
            eCardInfo.setLastUsedTime(pasECardInfo.getLastUsedTime());
            eCardInfo.setECardKind(com.broadon.wsapi.ecs.ECardKindType.fromString(
                    pasECardInfo.getECardKind().toString()));
        } else {
            eCardInfo.setActivatedTime(null);
            eCardInfo.setRevokedTime(null);
            eCardInfo.setLastUsedTime(null);
            eCardInfo.setECardKind(null);
        }
        errorCode = checkBalanceResponse.getErrorCode();
        errorMessage = checkBalanceResponse.getErrorMessage();
        return (checkBalanceResponse.getErrorCode() == ECommerceFunction.STATUS_OK);
    }
    
    public boolean authorizePayment(AbstractRequestType request)
        throws RemoteException
    {
        AuthorizeECardPaymentRequestType authPaymentRequest = 
            new AuthorizeECardPaymentRequestType();
        ECommerceFunction.initPasRequest(request, authPaymentRequest);
        
        authPaymentRequest.setAmount(paymentAmount);
        authPaymentRequest.setReferenceID(referenceId);
        authPaymentRequest.setECardID(eCard);

        AuthorizeECardPaymentResponseType authPaymentResponse =
            pas.authorizeECardPayment(authPaymentRequest);

        authToken = authPaymentResponse.getAuthorizationToken(); 

        errorCode = authPaymentResponse.getErrorCode();
        errorMessage = authPaymentResponse.getErrorMessage();
        return (authPaymentResponse.getErrorCode() == ECommerceFunction.STATUS_OK);
    }
    
    public boolean captureToAccount(AbstractRequestType request, AccountPayment toAccount)
        throws RemoteException, InvalidPaymentException
    {
        // Check the deposit amount and payment amount matches
        if (toAccount.depositAmount == null || !toAccount.depositAmount.equals(paymentAmount)) {            
            throw new InvalidPaymentException(
                    "Deposit amount " + toAccount.depositAmount + 
                    " does not match payment amount " + paymentAmount);
        }
        
        CaptureECardToAccountRequestType captureRequest = 
            new CaptureECardToAccountRequestType();
        ECommerceFunction.initPasRequest(request, captureRequest);
        
        captureRequest.setAmount(paymentAmount);
        captureRequest.setAuthorizationToken(getAuthorizationToken());
        captureRequest.setToBalanceAccount(toAccount.account);

        CaptureECardToAccountResponseType captureResponse =
            pas.captureECardToAccount(captureRequest);

        errorCode = captureResponse.getErrorCode();
        errorMessage = captureResponse.getErrorMessage();
        if (errorCode == ECommerceFunction.STATUS_OK) {
            toAccount.balance = captureResponse.getBalance();
            //checkBalance(request);
        }
        return (errorCode == ECommerceFunction.STATUS_OK);
    }

    public void checkECardType(AbstractRequestType request)
        throws RemoteException, InvalidPaymentException
    {
        GetECardTypeRequestType eCardRequest = new GetECardTypeRequestType();
        ECommerceFunction.initPasRequest(request, eCardRequest);
        eCardRequest.setECardNumber(eCard.getECardNumber());

        GetECardTypeResponseType eCardResponse = pas.getECardType(eCardRequest);
        errorCode = eCardResponse.getErrorCode();
        errorMessage = eCardResponse.getErrorMessage();
        if (eCardResponse.getErrorCode() == ECommerceFunction.STATUS_OK) {
            eCardInfo.setECardType(eCardResponse.getECardType().getECardType().trim());
            eCardInfo.setCountries(eCardResponse.getECardType().getCountries());
            int realECardType = Integer.parseInt(eCardInfo.getECardType());
            if (eCardType == 0) {
                eCardType = realECardType;
                eCard.setECardType(ECARD_TYPE_FORMAT.format(eCardType));
            } else if (eCardType != realECardType) {
                throw new InvalidPaymentException(
                    "ECard " + eCard.getECardNumber() + " type " + eCardType +
                    " has actual type of " + realECardType);
            }
            
            /* Check the country */
            String[] countries = eCardResponse.getECardType().getCountries();
            if (countries != null) {
                boolean countryOk = false;
                for (int i = 0; i < countries.length; i++) {
                    if (countries[i] == null || countries[i].equalsIgnoreCase(request.getCountryCode())) {
                        countryOk = true;
                        break;
                    }
                }
                if (!countryOk) {
                    throw new InvalidPaymentException("ECard cannot be used in country " + request.getCountryCode());
                }
            }
        }
    }
    
    /**
     * Lookup the default balance for this eCard
     */
    protected MoneyType lookupDefaultBalance(Connection conn)
        throws SQLException
    {
        return lookupDefaultBalance(conn, eCardType);
    }
    
    /**
     * Lookup the default balance for the eCardType
     */
    protected static MoneyType lookupDefaultBalance(Connection conn, int eCardType)
        throws SQLException
    {
        PreparedStatement stmt = null;
        
        try {
            stmt = conn.prepareStatement(GET_ECARD_DEFAULT_BALANCE);
            stmt.setInt(1, eCardType);
            ResultSet rs = stmt.executeQuery();
            if (rs != null && rs.next()) {
                String balance = rs.getString(1);
                String currency = rs.getString(2);
                return new MoneyType(currency, balance);
            } else {
                return null;
            }
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }
    
    /* No longer used (deprecated in PAS) 
    boolean deposit(AbstractRequestType request, Payment payment)
        throws RemoteException
    {
        DepositPaymentRequestType depositRequest = new DepositPaymentRequestType();
        ECommerceFunction.initPasRequest(request, depositRequest);
        depositRequest.setAuthorizationToken(payment.getAuthorizationToken());
        depositRequest.setToECardID(this.eCard);        
        depositRequest.setAmount(payment.paymentAmount);
        depositRequest.setDepositAmount(this.paymentAmount);

        DepositPaymentResponseType depositResponse =
            pas.depositPayment(depositRequest);

        errorCode = depositResponse.getErrorCode();
        errorMessage = depositResponse.getErrorMessage();
        return (errorCode == ECommerceFunction.STATUS_OK);
    }    
    */
}

package com.broadon.ecs;

import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.logging.Log;

import com.broadon.util.BackingStoreException;
import com.broadon.wsapi.ecs.GetMetaRequestType;
import com.broadon.wsapi.ecs.GetMetaResponseType;
import com.broadon.wsapi.ecs.TitleVersionType;

/**
 * The <code>GetMeta</code> class implements
 * the ECS GetMeta function.
 * 
 * Returns metadata (such as the secure kernel version, CRL version,
 * list of contents to preload, etc) for a given country and device type.
 *
 * @version $Revision: 1.27 $
 */
public class GetMeta extends ECommerceFunction {
    // Uses apache commons logger (follows axis logging)
    protected static Log log = LogFactory.getLog(GetMeta.class.getName());

    public GetMeta(
            ECommerceServiceImpl service, 
            GetMetaRequestType request,
            GetMetaResponseType response)
    {
        super("GetMeta", service, request, response, log);        
    }
    
    public void processImpl()
        throws BackingStoreException
    {
        GetMetaRequestType getMetaRequest = (GetMetaRequestType) request;
        GetMetaResponseType getMetaResponse = (GetMetaResponseType) response;

        int deviceType = getDevice().getDeviceType();
        
        // read cached response
        TitleVersionType[] systemTitles = service.getTitleCatalog().getSystemTitles(getCountry(), deviceType);
        if (systemTitles != null) {
            TitleVersionType[] tmp = new TitleVersionType[systemTitles.length];
            for (int i = 0; i < systemTitles.length; i++) {
                tmp[i] = new TitleVersionType(
                        systemTitles[i].getTitleId(),
                        systemTitles[i].getVersion(),
                        null);
            }
            systemTitles = tmp;
        }
        getMetaResponse.setContentPrefixURL(service.getContentPrefixUrl());
        getMetaResponse.setTitleVersion(systemTitles);
        // Don't provide preload contents anymore (not available in cas schema)
        getMetaResponse.setPreloadContents(null);
    }
    
    /**
     * Returns the meta information for the device
     * @param service
     * @param getMetaRequest
     * @return
     */
    public static GetMetaResponseType getMeta(
            ECommerceServiceImpl service,
            GetMetaRequestType getMetaRequest) 
    {
        GetMetaResponseType response = new GetMetaResponseType();
        GetMeta getMeta = new GetMeta(service, getMetaRequest, response);
        getMeta.process();
        return response;
    }
}

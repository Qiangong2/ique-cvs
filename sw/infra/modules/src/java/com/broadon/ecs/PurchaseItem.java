package com.broadon.ecs;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Locale;

import javax.sql.DataSource;

import org.apache.axis.encoding.Base64;
import org.apache.commons.logging.Log;

import com.broadon.db.DataSourceScheduler;
import com.broadon.exception.BroadOnException;
import com.broadon.util.BackingStoreException;
import com.broadon.wsapi.ecs.AbstractPurchaseResponseType;
import com.broadon.wsapi.ecs.AbstractRequestType;
import com.broadon.wsapi.ecs.AbstractResponseType;
import com.broadon.wsapi.ecs.AbstractPurchaseRequestType;
import com.broadon.wsapi.ecs.AccountPaymentType;
import com.broadon.wsapi.ecs.PaymentMethodType;
import com.broadon.wsapi.ecs.PaymentType;
import com.broadon.wsapi.ecs.PriceType;
import com.broadon.wsapi.pas.MoneyType;

/**
 * The <code>PurchaseItem</code> class handles the
 * purchase of a generic item.
 *
 * @version $Revision: 1.27 $
 */
abstract public class PurchaseItem {
    final protected ECommerceServiceImpl service;
    final protected Log log;
    final protected AbstractRequestType request;
    final protected AbstractResponseType response;
    final protected Device device;
    final protected Country country;
    final protected Locale locale;
    final protected String clientIp;
        
    protected PriceType expectedPrice;
    protected String transactionType;
    protected BigDecimal itemDiscount = PRICE_ZERO;
    protected BigDecimal itemTaxes = PRICE_ZERO;
    protected AccountPaymentType account; /* Account associated with this purchase */
    
    protected boolean paymentRequired = false;
    protected boolean accountRequired = false;
    protected boolean authorizationNeeded = true;
    protected boolean captureNeeded = true;    
    
    final static protected BigDecimal PRICE_ZERO = BigDecimal.valueOf(0);
    
    public PurchaseItem(ECommerceFunction func, String transactionType)
        throws BackingStoreException
    {
        this.service = func.service;
        this.log = func.log;
        this.request = func.request;
        this.response = func.response;
        this.locale = func.locale;
        this.device = func.getDevice();
        this.country = func.getCountry();
        this.transactionType = transactionType;
        this.clientIp = func.getClientIp();
        if (request instanceof AbstractPurchaseRequestType) {
            AbstractPurchaseRequestType purchaseRequest = (AbstractPurchaseRequestType) request;
            expectedPrice = purchaseRequest.getPrice();
            account = purchaseRequest.getAccount();
            if (purchaseRequest.getDiscount() != null) {
                itemDiscount = new BigDecimal(purchaseRequest.getDiscount());
            }
            if (purchaseRequest.getTaxes() != null) {
                itemTaxes = new BigDecimal(purchaseRequest.getTaxes());
            }
        }
    }
    
    protected void setErrorCode(String code, String message)
    {
        ECommerceFunction.setErrorCode(locale, code, message, response);        
    }
    
    /**
     * Get the expected price
     * @return
     */
    protected PriceType getExpectedPrice()
    {
        return expectedPrice;
    }
    
    /**
     * Get the payment method
     * @return
     */
    protected Payment getPayment(AbstractPurchaseRequestType purchaseRequest)
        throws SQLException, RemoteException, InvalidPaymentException
    {
        PaymentType paymentInfo = purchaseRequest.getPayment();
        if (paymentInfo == null) {
            throw new InvalidPaymentException("No payment information");
        }
        PaymentMethodType paymentMethod = paymentInfo.getPaymentMethod();
        if (PaymentMethodType.ECARD.equals(paymentMethod)) {
            if (paymentInfo.getECardPayment() == null) {
                throw new InvalidPaymentException("ECard not specified");
            }
            return new ECardPayment(
                    request,
                    service.getPasSOAP(),
                    paymentInfo.getECardPayment());
        } else if (PaymentMethodType.ACCOUNT.equals(paymentMethod)) {
            if (paymentInfo.getAccountPayment() == null) {
                throw new InvalidPaymentException("Account information not specified");
            }
            return new AccountPayment(
                    service.getPasSOAP(),
                    paymentInfo.getAccountPayment());
        } else if (PaymentMethodType.CCARD.equals(paymentMethod)) {
            if (paymentInfo.getCreditCardPayment() == null) {
                throw new InvalidPaymentException("Credit card information not specified");
            }
            return new CreditCardPayment(
                    service.getPasSOAP(),
                    paymentInfo.getCreditCardPayment(),
                    purchaseRequest.getPurchaseInfo());
        } else {
            throw new InvalidPaymentException("Invalid payment method " + paymentMethod.getValue());
        }
    }
                        
    /**
     * Get the payment method
     * @return
     */
    protected Payment getPayment(MoneyType paymentAmount)
        throws SQLException, RemoteException, InvalidPaymentException
    {
        AbstractPurchaseRequestType purchaseRequest = (AbstractPurchaseRequestType) request;
        Payment payment = getPayment(purchaseRequest);
        payment.setPaymentAmount(paymentAmount);
        return payment;
    }
    /**
     * Get information about the item to be purchased
     * @return
     * @throws BackingStoreException 
     */
    abstract protected PurchaseItemInfo getPurchaseItem()
        throws SQLException, BroadOnException, BackingStoreException;

    /**
     * Validates whether this purchase is okay or not
     */
    protected boolean validatePurchaseItem(PurchaseItemInfo purchaseItem)
        throws SQLException, BroadOnException
    {
        if (purchaseItem == null) {
            if (response.getErrorCode() == ECommerceFunction.STATUS_OK) {
                setErrorCode(StatusCode.ECS_BAD_ITEM_ID, "item not available for purchase");
            }
            return false;
        }

        // Get expected price
        PriceType expectedPrice = getExpectedPrice();

        // Check current pricing  is same as expected
        if (expectedPrice != null) {
            BigDecimal expectedAmount = convertAmount(expectedPrice.getAmount());
            if (!expectedPrice.getCurrency().equals(purchaseItem.currency)
                    || (expectedAmount.compareTo(purchaseItem.getPrice()) != 0)) {
                StringBuffer priceMsg = new StringBuffer();
                priceMsg.append("Actual price is ");
                priceMsg.append(" ");
                priceMsg.append(purchaseItem.getPrice());
                priceMsg.append(purchaseItem.currency);
                priceMsg.append(", expected price is ");
                priceMsg.append(expectedPrice.getAmount());
                priceMsg.append(" ");
                priceMsg.append(expectedPrice.getCurrency());
                setErrorCode(StatusCode.ECS_PRICE_MISMATCH, priceMsg.toString());
                return false;
            }
        }

        return true;
    }
    
    /**
     * The item has been paid for, do work to deliver it to the customer
     * @param request
     * @param response
     * @return
     */
    abstract protected boolean deliverItem(Payment payment, PurchaseItemInfo item)
        throws SQLException, BroadOnException;
    
    protected BigDecimal convertAmount(String amount)
        throws InvalidPriceException
    {
        try {
            return new BigDecimal(amount);
        } catch (NumberFormatException e) {
            throw new InvalidPriceException();
        }
    }
    
    protected boolean checkCountryCode(int itemId, String countryCode) 
    {
        if (countryCode == null || countryCode.equals(request.getCountryCode())) {
            return true;
        } else {
            setErrorCode(
                    StatusCode.ECS_BAD_COUNTRY_CODE,
                    "Item " + itemId + " is not available for " + request.getCountryCode());
            return false;
        }
    }
    
    protected boolean checkDeviceType(int itemId, int deviceType) 
    {
        if (deviceType == device.getDeviceType()) {
            return true;
        } else {
            setErrorCode(
                    StatusCode.ECS_BAD_DEVICE_TYPE,
                    "Item " + itemId + " is for device type " + 
                    deviceType + ", not device type " + device.getDeviceType());
            return false;
        }
    }
    
    protected boolean checkTitleType(int itemId, String titleType, String expectedTitleType) 
    {
        if (expectedTitleType.equals(titleType)) {
            return true;
        } else {
            setErrorCode(
                    StatusCode.ECS_BAD_TITLE_TYPE,
                    "Item " + itemId + " is for " + titleType + " not for " + expectedTitleType);
            return false;
        }
    }
        
    protected boolean checkLicenseType(int itemId, String licenseType, String expectedLicenseType) 
    {
        if (expectedLicenseType.equals(expectedLicenseType)) {
            return true;
        } else {
            setErrorCode(
                    StatusCode.ECS_BAD_LICENSE_TYPE,
                    "Item " + itemId + " is for " + licenseType + " not for " + expectedLicenseType); 
            return false;
        }
    }
        
    protected String getReferenceId(long transId, int seq)
    {
        StringBuffer strBuf = new StringBuffer();
        strBuf.append(service.getName());
        strBuf.append(".");
        strBuf.append(transId);
        strBuf.append(".");
        strBuf.append(seq);
        return strBuf.toString();    
    }

    protected String getLogMessage(long itemId, String state, String referenceId)
    {
        StringBuffer strBuf = new StringBuffer();
        strBuf.append("Error purchasing item ");
        strBuf.append(itemId);
        strBuf.append(" during ");
        strBuf.append(state);
        if (referenceId != null) {
            strBuf.append(", referenceId=");
            strBuf.append(referenceId);
        }
        return strBuf.toString();
    }
    
    protected void checkBalance(Payment payment)
        throws RemoteException
    {
        if (response instanceof AbstractPurchaseResponseType) {
            AbstractPurchaseResponseType purchaseResponse = (AbstractPurchaseResponseType) response;
            if (payment.checkBalance(request)) {
                purchaseResponse.setBalance(payment.getAvailableBalance());
            }
        }
    }
    
    /**
     * Does purchase
     */
    public void doPurchase()
        throws SQLException, BackingStoreException, BroadOnException
    {
        DataSource dataSource = service.getEcsDataSource();
        Connection conn = null;
        Invoice invoice = null;
        
        try {
            if (country == null) {
                if (response.getErrorCode() == ECommerceFunction.STATUS_OK) {
                    setErrorCode(StatusCode.ECS_BAD_COUNTRY_CODE, null);
                }
                return;
            }
            int countryId = country.countryId;
            
            String accountNumber = (account != null)? account.getAccountNumber(): null;
            if (accountRequired && accountNumber == null) {
                setErrorCode(StatusCode.ECS_ACCOUNT_REQUIRED, null);
                return;
            }

            // Get expected price
            PriceType expectedPrice = getExpectedPrice();

            // Look up current pricing
            PurchaseItemInfo purchaseItem = getPurchaseItem();
            if (!validatePurchaseItem(purchaseItem)) {
                return;
            }
            
            // Do Invoicing
            conn = service.getConnection(dataSource, DataSourceScheduler.DB_PRIORITY_MEDIUM);
            conn.setAutoCommit(false);

            int paymentSeq;
            int itemSeq;

            Long titleId = null;
            if (purchaseItem instanceof TicketedItemInfo) {
                titleId = new Long( ((TicketedItemInfo) purchaseItem).titleId);
            }

            invoice = Invoice.newInvoice(conn, accountNumber, countryId,
                    purchaseItem.currency, device.getDeviceId(),
                    request.getSerialNo(), clientIp, transactionType,
                    titleId);
            itemSeq = invoice.addItem(purchaseItem.itemId,
                    purchaseItem.quantity, purchaseItem.getPrice(), 
                    itemDiscount, itemTaxes, true);
            BigDecimal totalPrice = invoice.getTotalAmount();

            if (!paymentRequired &&
                    PRICE_ZERO.compareTo(totalPrice) == 0) {
          
                /* Item is free, no need to collect payment */
                boolean deliverSuccessful = deliverItem(null, purchaseItem);
                if (deliverSuccessful) {
                    invoice.updateTransaction(Invoice.STATUS_SUCCESS);
                } else {
                    invoice.updateTransaction(Invoice.STATUS_FAILED);
                }
                invoice.commit();
                return;
            }
            
            MoneyType paymentAmount = null;
            if (expectedPrice != null) {
                paymentAmount = new MoneyType(expectedPrice.getCurrency(),
                        totalPrice.toString());
            } else {
                paymentAmount = new MoneyType(purchaseItem.currency,
                        totalPrice.toString()); 
            }
            Payment payment = getPayment(paymentAmount);
            invoice.updateTransaction(Invoice.STATUS_OPEN);
            
            Integer eCardType = null;
            if (payment instanceof ECardPayment) {
                eCardType = new Integer( ((ECardPayment) payment).eCardType );
            }
            paymentSeq = invoice.addPayment(
                    payment.getPaymentMethodId(), 
                    payment.getPaymentType(),
                    purchaseItem.getPrice(),
                    Invoice.STATUS_OPEN,
                    eCardType, true);

            long transId = invoice.getTransactionId();
            String referenceId = getReferenceId(transId, paymentSeq);
            payment.setReferenceId(referenceId);

            if (!authorizationNeeded) {
                /* Does not need authorization - only need to deliver item */
                boolean deliverSuccessful = deliverItem(payment, purchaseItem);
                if (deliverSuccessful) {
                    invoice.updateTransaction(Invoice.STATUS_SUCCESS);
                    invoice.updatePayment(paymentSeq, Invoice.STATUS_SUCCESS, null, true);
                    checkBalance(payment);
                } else {
                    invoice.updateTransaction(Invoice.STATUS_FAILED);
                    invoice.updatePayment(paymentSeq, Invoice.STATUS_FAILED, null, true);
                }
                invoice.commit();
                return;
            } 
            
            // Pay for item
            // TODO: commit to DB as needed, handle errors and recover, free connection?
            boolean paymentSuccessful = false;
            boolean deliverSuccessful = false;
            String authToken = null;
            if (payment.authorizePayment(request)) {
                if (payment.authToken != null) {
                    // Store in DB base64 encoding of token
                    authToken = Base64.encode(payment.authToken.getToken());
                }
                
                invoice.updatePayment(paymentSeq, Invoice.STATUS_AUTH,
                        authToken, false);
                invoice.commit();

                // Deliver item
                try {
                    deliverSuccessful = deliverItem(payment, purchaseItem);
                } catch (SQLException e) {               
                    setErrorCode(StatusCode.SC_SQL_EXCEPTION,  e.getLocalizedMessage());
                    log.error(getLogMessage(purchaseItem.itemId, "DELIVERY", referenceId), e);
                } catch (Throwable e) {
                    // Unexpected error, catch and log
                    setErrorCode(StatusCode.ECS_INTERNAL_ERROR, e.getLocalizedMessage());
                    log.error(getLogMessage(purchaseItem.itemId, "DELIVERY", referenceId), e);
                }
                                    
                if (deliverSuccessful) {
                    invoice.updatePayment(paymentSeq, Invoice.STATUS_CAP,
                            authToken, false);
                    invoice.commit();
                    // TODO: If capture payment failed, queue up and try again
                    if (captureNeeded) {
                        if (payment.capturePayment(request)) {
                            paymentSuccessful = true;
                        }
                    } else {
                        paymentSuccessful = true;
                    }
                } else {
                    if (payment.voidAuthorization(request)) {
                        // Payment successfully voided - mark it as being voided
                        invoice.updatePayment(paymentSeq, Invoice.STATUS_VOID,
                                authToken, true);
                        invoice.commit();
                    } else {
                        // TODO: Add to queue of authorizations to void
                    }
                }
            } else {
                invoice.updatePayment(paymentSeq, Invoice.STATUS_FAILED,
                        authToken, true);
                invoice.commit();
            }

            if (paymentSuccessful) {
                invoice.updatePayment(paymentSeq, Invoice.STATUS_SUCCESS,
                        authToken, true);
                invoice.updateTransaction(Invoice.STATUS_SUCCESS);
                checkBalance(payment);
            } else {
                invoice.updateTransaction(Invoice.STATUS_FAILED);

                if (payment.errorCode != ECommerceFunction.STATUS_OK) {
                    if (response.getErrorCode() == ECommerceFunction.STATUS_OK) {
                        response.setErrorCode(payment.errorCode);
                        response.setErrorMessage(payment.errorMessage);
                    }
                }
            }
            invoice.commit();
        } catch (InvalidPaymentException e) {               
            setErrorCode(StatusCode.ECS_INVALID_PAYMENT, e.getMessage());
            log.debug("Invalid Payment", e);
        } catch (InvalidPriceException e) {               
            setErrorCode(StatusCode.ECS_BAD_PRICE, e.getMessage());
            log.debug("Invalid Price", e);
        } catch (RemoteException e) {
            setErrorCode(StatusCode.ECS_PAS_ERROR, e.getMessage());
            log.error("Error processing payment using PAS", e);
        } finally {
            try {
                if (invoice != null) {
                    invoice.close();
                }
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            } catch (SQLException e) {
                log.warn("Error closing connection", e);
            }
        }

    }

}

package com.broadon.ecs;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;

import javax.sql.DataSource;

import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.logging.Log;

import com.broadon.db.DataSourceScheduler;
import com.broadon.wsapi.ecs.*;

/**
 * The <code>UpdateStatus</code> class implements the ECS UpdateStatus function.
 * 
 * Checks in a list of titles and updates their consumption counts.
 * Checks out a list of titles and returns the eTickets for the titles.
 *
 * @version $Revision: 1.10 $
 */
public class UpdateStatus extends ECommerceFunction {
    private static final String ADD_CONSUMPTION =
        "INSERT INTO ECS_DEVICE_TITLE_CHECKOUTS " +
        "    (DEVICE_ID, TITLE_ID, CHECKIN_DATE, CONSUMPTION_COUNTER) " +
        "    VALUES(?, ?, SYS_EXTRACT_UTC(CURRENT_TIMESTAMP), ?)";
    
    private static final String FIND_CONSUMPTION =
        "SELECT CONSUMPTION_COUNTER FROM ECS_DEVICE_TITLE_CHECKOUTS " +
        "    WHERE DEVICE_ID=? AND TITLE_ID=?";

    private static final String UPDATE_CONSUMPTION =
        "UPDATE ECS_DEVICE_TITLE_CHECKOUTS " +
        "    SET CHECKIN_DATE=SYS_EXTRACT_UTC(CURRENT_TIMESTAMP), CONSUMPTION_COUNTER=? " +
        "    WHERE DEVICE_ID=? AND TITLE_ID=? AND CONSUMPTION_COUNTER=?";

    // Uses apache commons logger (follows axis logging)
    protected static Log log = 
        LogFactory.getLog(UpdateStatus.class.getName());

    public UpdateStatus(
            ECommerceServiceImpl service, 
            UpdateStatusRequestType request,
            UpdateStatusResponseType response)
    {
        super("UpdateStatus", service, request, response, log);        
    }
    
    /**
     * Update table with consumption counters
     * @param conn DB connection
     * @param date CheckIn date
     * @param deviceID device ID
     * @param checkins Array of titles and consumption counters
     * @throws SQLException
     * @throws TitleNotCheckedOutException
     */
    private static void updateConsumption(
            Connection conn,
            long deviceId,            
            ConsumptionType[] checkins)
        throws SQLException
    {
        if (checkins == null) return;
        
        PreparedStatement findStmt = null;
        PreparedStatement addStmt = null;
        PreparedStatement updateStmt = null;

        try {
            findStmt = conn.prepareStatement(FIND_CONSUMPTION);
            addStmt = conn.prepareStatement(ADD_CONSUMPTION);
            updateStmt = conn.prepareStatement(UPDATE_CONSUMPTION);
            
            boolean autoCommit = conn.getAutoCommit();
            if (autoCommit) {
                conn.setAutoCommit(false);
            }
            
            for (int i = 0; i < checkins.length; i++) {
                long consumption = checkins[i].getConsumption();
                String title = checkins[i].getTitleId();
                long titleId;
                try {
                    titleId = parseTitleId(title);
                } catch (Throwable e) {
                    throw new IllegalArgumentException("Error parsing title " + title);
                }

                // Find consumption count in DB for title
                findStmt.setLong(1, deviceId);
                findStmt.setLong(2, titleId);
                ResultSet rs = findStmt.executeQuery();
                if (rs != null && rs.next()) {
                    int oldconsumption = rs.getInt(1);
                    rs.close();
                    if (consumption > oldconsumption) {
                        // Update consumption count in DB for title
                        updateStmt.setLong(1, consumption);
                        updateStmt.setLong(2, deviceId);
                        updateStmt.setLong(3, titleId);
                        updateStmt.setLong(4, oldconsumption);
                        updateStmt.execute();
                    } 
                } else {
                    if (rs != null) { rs.close(); };
                    // Add new record for consumption count in DB for title
                    addStmt.setLong(1, deviceId);
                    addStmt.setLong(2, titleId);
                    addStmt.setLong(3, consumption);
                    addStmt.execute();
                }
            }
            if (autoCommit) {
                conn.commit();
                conn.setAutoCommit(true);
            }
        } finally {
            if (addStmt != null) {
                try { addStmt.close(); }
                catch (SQLException e) {}
            }
            if (updateStmt != null) {
                try { updateStmt.close(); }
                catch (SQLException e) {}
            }
            if (findStmt != null) {
                try { findStmt.close(); }
                catch (SQLException e) {}
            }
        }
    }
    
    public void processImpl()
        throws SQLException
    {
        UpdateStatusRequestType updateStatusRequest = (UpdateStatusRequestType) request;
        UpdateStatusResponseType updateStatusResponse = (UpdateStatusResponseType) response;

        DataSource dataSource = service.getEcsDataSource();
        Connection conn = null;

        long deviceId = getDevice().getDeviceId();

        // Update consumption counts
        try {
            conn = service.getConnection(dataSource, DataSourceScheduler.DB_PRIORITY_MEDIUM);           
            conn.setAutoCommit(false);
            ConsumptionType[] checkins = updateStatusRequest.getConsumptions();
            updateConsumption(conn, deviceId, checkins);
            conn.commit();            
        } catch (IllegalArgumentException e) {
            setErrorCode(StatusCode.ECS_BAD_TITLE_ID, e.getLocalizedMessage());            
        } finally {
            try {
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            } catch (SQLException e) {
               log.warn("Error closing connection", e);
            }
        }
    }

    public static UpdateStatusResponseType updateStatus(
            ECommerceServiceImpl service,
            UpdateStatusRequestType updateStatusRequest)
    {
        UpdateStatusResponseType response = new UpdateStatusResponseType();
        UpdateStatus updateStatus = new UpdateStatus(service, updateStatusRequest, response);
        updateStatus.process();
        return response;
    }
}

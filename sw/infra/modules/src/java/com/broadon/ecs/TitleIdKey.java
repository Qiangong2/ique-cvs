package com.broadon.ecs;

/**
 * Key for looking up an cached entry by title id and country id
 * @version $Revision: 1.4 $
 */
public class TitleIdKey {
    public final Country country;
    public final long titleId;
    public final String language;
    
    public TitleIdKey(Country country, String language, long titleId)
    {
        this.titleId = titleId;
        this.country = country;
        if (language == null && country != null) {
            this.language = country.language;
        } else {
            this.language = language;
        }
    }
    
    public boolean equals(Object obj)
    {
        if (obj instanceof TitleIdKey) {
            TitleIdKey other = (TitleIdKey) obj;
            return (this.titleId == other.titleId && 
                   ((this.language == null)? (other.language == null): 
                       (this.language.equals(other.language))) &&
                   ((this.country == null)? (other.country == null): 
                       (this.country.equals(other.country))));
        } else {
            return false;
        }
    }
    
    public int hashCode()
    {
        int hash = (int) titleId;
        if (country != null) {
            hash += country.hashCode();
        }
        return hash;
    }
    
    public String toString()
    {
        return "title " + titleId + ", country " + country + ", language " + language;
    }
}

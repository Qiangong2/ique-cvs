package com.broadon.ecs;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.broadon.db.DataSourceScheduler;
import com.broadon.util.BackingStore;
import com.broadon.util.BackingStoreException;
import com.broadon.util.CacheExp;
import com.broadon.util.Pair;
import com.broadon.wsapi.ets.TicketAttributeType;

/**
 * The <code>PurchaseItemsCache</code> class provides a cache
 * for items that can be purchased based by item id.
 *
 * @version $Revision: 1.2 $
 */
public class PurchaseItemsCache 
    extends CacheExp implements BackingStore 
{    
    private ECommerceServiceImpl service;

    private static final String LOOKUP_ITEMS =
        "SELECT BCCUTIL.ConvertTimeStamp(PURCHASE_END_DATE), COUNTRY, DEVICE_TYPE_ID, " +
        "   ITEM_ID, ITEM_PRICE, ITEM_CURRENCY, LICENSE_TYPE, " +
        "   POINTS, a.TITLE_ID, RTYPE, LIMITS, " +
        "   ETKM_CONTENT_ID, TITLE_TYPE " +
        "   FROM CAS_COUNTRY_ITEM_PRICINGS a, CONTENT_TITLE_CATALOG b" +
        "   WHERE a.TITLE_ID = b.TITLE_ID(+) AND " + ECommerceFunction.FILTER_BY_PURCHASE_DATE; 

    public PurchaseItemsCache(ECommerceServiceImpl service)
    {
        super(service.getContentCacheTimeoutMsecs());
        this.service = service;
    }

    /**
     * Look up list of items corresponding to the given item id and ecard type
     * @param conn DB Connection
     * @param eCardType
     * @return
     * @throws SQLException
     */
    private static Pair getItems(
            Connection conn,
            Integer reqItemId,
            Integer reqECardType)
        throws SQLException
    {
        PreparedStatement stmt = null;
        List items = null;
        Long expirationDate = null;
        
        try {
            String sql = LOOKUP_ITEMS;
            if (reqItemId != null) {
                sql += " AND ITEM_ID=" + reqItemId;
            }
            if (reqECardType != null) {
                sql += " AND ECARD_TYPE=" + reqECardType;
            }
            stmt = conn.prepareStatement(sql);

            ResultSet rs = stmt.executeQuery();
            if (rs != null) {
                long expDate = Long.MAX_VALUE;            
                while (rs.next()) {
                    int i = 1;
                    long endDate = rs.getLong(i++);
                    if (! rs.wasNull()) {
                        if (endDate < expDate) {
                            expDate = endDate;
                        }
                    }                    
                    
                    PurchaseItemInfo item = null;
                    String country = rs.getString(i++);
                    int deviceType = rs.getInt(i++);
                    int itemId = rs.getInt(i++);
                    BigDecimal price = rs.getBigDecimal(i++);
                    String currency = rs.getString(i++);
                    String licenseType = rs.getString(i++);
                    long points = rs.getLong(i++);
                    
                    if (PurchaseItemInfo.VCPOINTS_TYPE.equalsIgnoreCase(licenseType)) {
                        item = new PurchaseItemInfo(itemId, price, currency, 1);                        
                        if (!rs.wasNull()) {
                            item.points = new Long(points);
                        }
                    } else {
                        TicketedItemInfo ticketedItem = new TicketedItemInfo(itemId, price, currency, 1);
                        item = ticketedItem;

                        long titleId = rs.getLong(i++);                    
                        if (rs.wasNull()) {
                            // TODO: Throw error
                        }
                   
                        ticketedItem.titleId = titleId;
                        ticketedItem.rtype = rs.getString(i++);
                        ticketedItem.total_limits = rs.getLong(i++);
                        ticketedItem.licenseType = licenseType;
                        ticketedItem.eTicketContentId = rs.getBigDecimal(i++);
                        ticketedItem.titleCategory = rs.getString(i++);
                        
                        if (TicketedItemInfo.SUBSCRIPTION_TYPE.equalsIgnoreCase(ticketedItem.licenseType)) {
                            /* Subscription - fill in limits later */
                        } else if (!ETicketRecord.LIMIT_TYPE_PR.equals(ticketedItem.rtype)) {
                            ticketedItem.limits = new TicketAttributeType[1];
                            ticketedItem.limits[0] = new TicketAttributeType(ticketedItem.total_limits, ticketedItem.rtype);
                        }
                    }

                    item.country = country;
                    item.deviceType = deviceType;

                    if (items == null) {
                        items = new ArrayList();
                    }
                    items.add(item);                    
                }
                expirationDate = (expDate == Long.MAX_VALUE) ? null : new Long(expDate);                
            }
        } finally {
            if (stmt != null) {
                stmt.close();
            } 
        }
        
        return new Pair(expirationDate, items);
    }

    protected Pair retrieve(Integer reqItemId, Integer reqECardType)
        throws BackingStoreException
    {
        Connection conn = null;
        try {
            conn = service.getConnection(service.getCasDataSource(), DataSourceScheduler.DB_PRIORITY_FAST);
            return getItems(conn, reqItemId, reqECardType);
        } catch (SQLException e) {
            throw BackingStoreException.rewrap(e);            
        } finally {
            if (conn != null) {
                try { conn.close(); } catch (SQLException e) {}
            }
        }
    }
    
    public Object retrieve(Object key) throws BackingStoreException
    {
        return retrieve((Integer) key, null);
    }
}

package com.broadon.ecs;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The <code>ETicketRecord</code> class creates
 * a new record for a eticket in the DB
 *
 * @version $Revision: 1.16 $
 */
public class ETicketRecord {
    public final static String LIMIT_TYPE_PR = "PR";
    public final static String LIMIT_TYPE_SR = "SR";
    public final static String LIMIT_TYPE_DR = "DR";
    public final static String LIMIT_TYPE_TR = "TR";
    
    private final static String NEW_ETICKET =
        "INSERT into ETICKETS " +
        "    (DEVICE_ID, CONTENT_ID, BIRTH_TYPE, RTYPE, TOTAL_LIMITS, TID, MASK, LICENSE_TYPE)" +
        "    VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
    
    private final static String UPDATE_ETICKET =
        "UPDATE ETICKETS SET RTYPE=?, TOTAL_LIMITS=?, TID=?, MASK=?, LICENSE_TYPE=?" +
        "    WHERE DEVICE_ID=? AND CONTENT_ID=?";

    private final static String UPDATE_ETICKET_CHECKED =
        "UPDATE ETICKETS SET RTYPE=?, TOTAL_LIMITS=?, TID=?, MASK=?, LICENSE_TYPE=?" +
        "    WHERE DEVICE_ID=? AND CONTENT_ID=? AND RTYPE=? AND TOTAL_LIMITS=? AND TID=? AND MASK=? AND LICENSE_TYPE=? " +
        "    AND BCCUTIL.ConvertTimeStamp(LAST_UPDATED)=?";

    private final static String LOOKUP_ETICKET =
        "SELECT BCCUTIL.ConvertTimeStamp(LAST_UPDATED), BCCUTIL.ConvertTimeStamp(REVOKE_DATE), BIRTH_TYPE, " +
        "    CONTENT_ID, RTYPE, TOTAL_LIMITS, TID, MASK, LICENSE_TYPE  " +
        "    from ETICKETS " +
        "    WHERE DEVICE_ID=? AND CONTENT_ID=?";
    
    /**
     * Update eticket table with new eticket
     * @param conn DB connection
     * @param string birthType
     * @param deviceId device ID
     * @param items Items for which we got a new ticket or updated an old ticket
     * @throws SQLException
     */
    public static void generate(
            Connection conn,
            String birthType,
            long deviceId,
            TicketedItemInfo[] items)
        throws SQLException
    {
        if (items == null || items.length == 0) return;

        PreparedStatement stmt = null;

        try {
            int cnt = 0;
            stmt = conn.prepareStatement(NEW_ETICKET);
            boolean autoCommit = conn.getAutoCommit();
            if (autoCommit) {
                conn.setAutoCommit(false);
            }
            for (int i = 0; i < items.length; i++) {
                if (items[i].record == null) {
                    // new record
                    int j = 1;                    
                    stmt.setLong(j++, deviceId);
                    stmt.setBigDecimal(j++, items[i].eTicketContentId);
                    stmt.setString(j++, birthType);
                    stmt.setString(j++, items[i].rtype);
                    stmt.setLong(j++, items[i].total_limits);
                    stmt.setLong(j++, items[i].ticketId);
                    stmt.setString(j++, items[i].mask);
                    stmt.setString(j++, items[i].licenseType);
                    stmt.addBatch();
                    cnt++;
                } else {
                    // Update old record
                    items[i].record.update(conn, deviceId, items[i]);
                }
            }
            if (cnt > 0) {
                stmt.executeBatch();
            }
            if (autoCommit) {
                conn.commit();
                conn.setAutoCommit(true);
            }
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    /**
     * Updates existing ETicketRecord in the DB
     * @param conn
     * @throws SQLException
     */
    public static void updateRecord(Connection conn, long deviceId, TicketedItemInfo item)
        throws SQLException
    {
        PreparedStatement stmt = null;

        try {
            stmt = conn.prepareStatement(UPDATE_ETICKET);
            int i = 1;
            stmt.setString(i++, item.rtype);
            stmt.setLong(i++, item.total_limits);
            stmt.setLong(i++, item.ticketId);
            stmt.setString(i++, item.mask);
            stmt.setString(i++, item.licenseType);
            stmt.setLong(i++, deviceId);
            stmt.setBigDecimal(i++, item.eTicketContentId);
            stmt.executeUpdate();
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }
    
    /**
     *  Look up and returns a ETicketRecord
     **/
    public static ETicketRecord lookupRecord(
            Connection conn, long deviceId, long titleId)
        throws SQLException
    {
        PreparedStatement stmt = null;
        
        BigDecimal contentId = TitleCatalog.getContentId(titleId);

        try {
            stmt = conn.prepareStatement(LOOKUP_ETICKET);
            stmt.setLong(1, deviceId);
            stmt.setBigDecimal(2, contentId);
            
            ResultSet rs = stmt.executeQuery();
            if (rs != null) {
                if (rs.next()) {
                    int i = 1;
                    ETicketRecord record = new ETicketRecord();
                    long updated = rs.getLong(i++);
                    record.lastUpdateDate = (rs.wasNull())? null: new Date(updated);
                    long revoke = rs.getLong(i++);
                    record.revokeDate = (rs.wasNull())? null: new Date(revoke);
                    record.deviceId = deviceId;
                    record.titleId = titleId;
                    record.birthType = rs.getString(i++);
                    record.contentId = rs.getBigDecimal(i++);
                    record.rtype = rs.getString(i++);
                    record.limits = rs.getLong(i++);
                    record.tid = rs.getLong(i++);
                    record.mask = rs.getString(i++);
                    record.licenseType = rs.getString(i++);
                    return record;
                }
            }
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
        
        return null;
    }

    /**
     * Updates this ETicketRecord in the DB
     * @param conn
     * @throws SQLException
     */
    public void update(Connection conn)
        throws SQLException
    {
        PreparedStatement stmt = null;

        try {
            stmt = conn.prepareStatement(UPDATE_ETICKET);
            int i = 1;
            stmt.setString(i++, rtype);
            stmt.setLong(i++, limits);
            stmt.setLong(i++, tid);
            stmt.setString(i++, mask);
            stmt.setString(i++, licenseType);
            stmt.setLong(i++, deviceId);
            stmt.setBigDecimal(i++, contentId);
            stmt.executeUpdate();
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    /**
     * Updates existing ETicketRecord in the DB, make sure this is the record being updated
     * @param conn
     * @throws SQLException
     */
    public void update(Connection conn, long deviceId, TicketedItemInfo item)
        throws SQLException
    {
        PreparedStatement stmt = null;

        try {
            stmt = conn.prepareStatement(UPDATE_ETICKET_CHECKED);
            int i = 1;
            stmt.setString(i++, item.rtype);
            stmt.setLong(i++, item.total_limits);
            stmt.setLong(i++, item.ticketId);
            stmt.setString(i++, item.mask);
            stmt.setString(i++, item.licenseType);
            stmt.setLong(i++, deviceId);
            stmt.setBigDecimal(i++, item.eTicketContentId);
            stmt.setString(i++, rtype);
            stmt.setLong(i++, limits);
            stmt.setLong(i++, tid);
            stmt.setString(i++, mask);
            stmt.setString(i++, licenseType);
            stmt.setLong(i++, lastUpdateDate.getTime());
            stmt.executeUpdate();
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }
    
    // Member variables
    long deviceId;
    long titleId;
    BigDecimal contentId;
    Date revokeDate;
    String rtype;
    long limits;
    long tid;
    String mask;
    String licenseType;
    String birthType;
    Date lastUpdateDate;
}

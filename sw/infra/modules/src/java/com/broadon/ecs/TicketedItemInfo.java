package com.broadon.ecs;

import java.math.BigDecimal;

import com.broadon.wsapi.ets.TicketAttributeType;

public class TicketedItemInfo extends PurchaseItemInfo {
    protected long titleId;
    protected String titleCategory;
    protected BigDecimal eTicketContentId;
    protected long ticketId;
    protected String mask;    
    protected String licenseType; 
    protected String rtype;
    protected long total_limits;
    
    TicketAttributeType[] limits = null;
    
    protected ETicketRecord record;
    
    public TicketedItemInfo(int itemId, BigDecimal itemPrice, String currency, int quantity)
    {
        super(itemId, itemPrice, currency, quantity);
    }
    
    public TicketedItemInfo(TicketedItemInfo item)
    {
        super(item);
        this.titleId = item.titleId;
        this.titleCategory = item.titleCategory;
        this.eTicketContentId = item.eTicketContentId;
        this.ticketId = item.ticketId;
        this.mask = item.mask;
        this.licenseType = item.licenseType;
        this.rtype = item.rtype;
        this.total_limits = item.total_limits;
        this.record = item.record;
        if (item.limits != null) {
            this.limits = new TicketAttributeType[item.limits.length];
            for (int i = 0; i < item.limits.length; i++) {
                this.limits[i] = new TicketAttributeType(item.limits[i].getLimits(), item.limits[i].getLimitKind());
            }
        }
    }
    
    public String toString()
    {
        String str = super.toString();
        StringBuffer strBuffer = new StringBuffer();
        strBuffer.append(str);
        strBuffer.append(", title ");
        strBuffer.append(ECommerceFunction.formatTitleId(titleId));
        strBuffer.append(", title type ");
        strBuffer.append(titleCategory);
        strBuffer.append(", etkm content id ");
        strBuffer.append(ECommerceFunction.formatContentId(eTicketContentId));
        strBuffer.append(", mask ");
        strBuffer.append(mask);
        strBuffer.append(", license type ");
        strBuffer.append(licenseType);
        if (limits != null && limits.length > 0) {
            strBuffer.append(", limits ");
            for (int i = 0; i < limits.length; i++) {
                strBuffer.append(limits[i].getLimitKind());
                strBuffer.append("=");
                strBuffer.append(limits[i].getLimits());
                if (i < limits.length - 1) {
                    strBuffer.append(",");
                }
            }
        }
        return strBuffer.toString();
    }
}

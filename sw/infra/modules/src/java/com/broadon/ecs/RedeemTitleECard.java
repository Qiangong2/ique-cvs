package com.broadon.ecs;

import java.rmi.RemoteException;
import java.sql.SQLException;

import com.broadon.util.BackingStoreException;
import com.broadon.wsapi.ecs.RedeemECardRequestType;
import com.broadon.wsapi.pas.MoneyType;

/**
 * The <code>RedeemTitleEcard</code> class redeems
 * a ECard for a Title.
 *
 * @version $Revision: 1.5 $
 */
public class RedeemTitleECard extends PurchaseTitleItem {
    
    ECardPayment eCardPayment;
    TicketedItemInfo ticketedItem;
    
    public RedeemTitleECard(ECommerceFunction func,
            ECardPayment payment,
            TicketedItemInfo item)
        throws BackingStoreException
    {
        super(func, ((RedeemECardRequestType) func.request).getDeviceCert());
        this.eCardPayment = payment;
        this.ticketedItem = item;
        this.transactionType = Invoice.TRANS_REDEEM_GAME;
        paymentRequired = true;
        account = ((RedeemECardRequestType) request).getAccount();
    }

    /**
     * Get the payment method
     * @return
     */
    protected Payment getPayment(MoneyType paymentAmount)
        throws SQLException, RemoteException, InvalidPaymentException
    {
        /* Only accept ECard */
        if (eCardPayment == null) {
            RedeemECardRequestType redeemRequest = (RedeemECardRequestType) request;
            ECardPayment payment = new ECardPayment(
                    request,
                    service.getPasSOAP(), 
                    redeemRequest.getECard());
            payment.setPaymentAmount(paymentAmount);
            return payment;
        } else {
            eCardPayment.setPaymentAmount(paymentAmount);
            return eCardPayment;
        }
    }
                        
     /**
     * Get information on the item to purchase for the
     * requested ecard
     */
    protected TicketedItemInfo getTicketedItem()
    {
        return ticketedItem;
    }

}

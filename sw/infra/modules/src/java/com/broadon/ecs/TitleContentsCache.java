package com.broadon.ecs;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.broadon.db.DataSourceScheduler;
import com.broadon.util.BackingStore;
import com.broadon.util.BackingStoreException;
import com.broadon.util.CacheLFU;
import com.broadon.wsapi.ecs.ContentInfoType;

/**
 * The <code>TitleRatingsCache</code> class provides a cache
 * for an array of title ratings (RatingType[]) using the 
 * titleId and countryId as a key (TitleIdKey).
 *
 * @version $Revision: 1.4 $
 */
public class TitleContentsCache 
    extends CacheLFU implements BackingStore 
{
    private ECommerceServiceImpl service;

    private static final String SELECT_TITLE_CONTENTS = 
        "SELECT CONTENT_ID, CONTENT_SIZE " +
        "FROM LATEST_CONTENT_TITLE_OBJECTS WHERE TITLE_ID=? " +
        "AND CONTENT_OBJECT_TYPE NOT IN ( 'ETKM' )";

    public TitleContentsCache(ECommerceServiceImpl service) {
        super(service.getContentCacheTimeoutMsecs());
        this.service = service;
    }

    public Object retrieve(Object key) throws BackingStoreException
    {
        Long titleIdKey = (Long) key;
        long titleId = titleIdKey.longValue();
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection conn = null;
        try {
            conn = service.getConnection(service.getCasDataSource(), DataSourceScheduler.DB_PRIORITY_FAST);
            ps = conn.prepareStatement(SELECT_TITLE_CONTENTS);
            ps.setLong(1, titleId);
            rs = ps.executeQuery();
            List contentList = new ArrayList();
            while (rs.next()) {
                ContentInfoType content = new ContentInfoType();
                int i = 1;
                BigDecimal contentId = rs.getBigDecimal(i++); 
                content.setContentId(
                        ECommerceFunction.formatContentId(contentId));
                content.setContentSize(rs.getLong(i++));
                contentList.add(content);
            }
            
            if (contentList.size() > 0) {
                ContentInfoType[] contents = new ContentInfoType[contentList.size()];
                return (ContentInfoType[]) contentList.toArray(contents);
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw BackingStoreException.rewrap(e);            
        } finally {
            if (rs != null) {
                try { rs.close(); } catch (SQLException e) {}
            }
            if (ps != null) {
                try { ps.close(); } catch (SQLException e) {}
            }
            if (conn != null) {
                try { conn.close(); } catch (SQLException e) {}
            }
        }
    }
    
}

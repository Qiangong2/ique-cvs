package com.broadon.ecs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.logging.Log;

import com.broadon.wsapi.ecs.DeleteETicketsRequestType;
import com.broadon.wsapi.ecs.DeleteETicketsResponseType;

/**
 * The <code>DeleteETickets</code> class implements
 * the ECS DeleteETickets function.
 * 
 * Delete a list of etickets for a given device.
 * 
 * @version $Revision: 1.3 $
 */
public class DeleteETickets extends ECommerceFunction {
    private static final String DELETE_ETICKETS =
        "UPDATE etickets " +
        "SET    revoke_date = SYSDATE " +
        "WHERE  device_id = ? " +
        "AND    tid = ?";

    // Uses apache commons logger (follows axis logging)
    protected static Log log = LogFactory.getLog(DeleteETickets.class.getName());

    public DeleteETickets(
            ECommerceServiceImpl service, 
            DeleteETicketsRequestType request,
            DeleteETicketsResponseType response)
    {
        super("DeleteETickets", service, request, response, log);        
    }
    
    public void processImpl()
        throws SQLException
    {
        DeleteETicketsRequestType deleteETicketsRequest = (DeleteETicketsRequestType) request;
        DeleteETicketsResponseType deleteETicketsResponse = (DeleteETicketsResponseType) response;

        long[] tids = deleteETicketsRequest.getTickets();
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = service.getEcsDataSource().getConnection();
            // Get title list            
            String sql = DELETE_ETICKETS;
            
            stmt = conn.prepareStatement(sql);
            log.info(sql);
            for (int i=0; i<tids.length; i++) {
                stmt.setLong(1, getDevice().getDeviceId());
                stmt.setLong(2, tids[i]);
                log.info("device_id: " + getDevice().getDeviceId() + ", tid: " + tids[i]);
                stmt.addBatch();
            }
            stmt.executeBatch();
            
            stmt.close();
            stmt = null;
            conn.close();
            conn = null;
        } finally {
            if (stmt != null) {
                try { stmt.close(); } catch (SQLException e) {}
            }
            if (conn != null) {
                try { conn.close(); } catch (SQLException e) {}
            }
        }
    }

    /**
     * Delete ETickets
     */
    public static DeleteETicketsResponseType deleteETickets(
            ECommerceServiceImpl service,
            DeleteETicketsRequestType deleteETicketsRequest) 
    {
        DeleteETicketsResponseType response = new DeleteETicketsResponseType();
        DeleteETickets deleteETicket = new DeleteETickets(service, deleteETicketsRequest, response);
        deleteETicket.process();
        return response;
    }
}

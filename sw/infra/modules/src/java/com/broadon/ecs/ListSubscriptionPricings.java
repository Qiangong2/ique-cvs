package com.broadon.ecs;

import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.logging.Log;

import com.broadon.util.BackingStoreException;
import com.broadon.wsapi.ecs.ListSubscriptionPricingsRequestType;
import com.broadon.wsapi.ecs.ListSubscriptionPricingsResponseType;
import com.broadon.wsapi.ecs.SubscriptionPricingType;

/**
 * The <code>ListSubscriptionPricings</code> class implements
 * the ECS ListSubscriptionPricings function.
 * 
 * Provides a list of subscriptions and their pricings for a given country
 * and device type.
 * 
 * @version $Revision: 1.10 $
 */
public class ListSubscriptionPricings extends ECommerceFunction 
{
    // Uses apache commons logger (follows axis logging)
    protected static Log log = LogFactory.getLog(ListSubscriptionPricings.class.getName());

    public ListSubscriptionPricings(
            ECommerceServiceImpl service, 
            ListSubscriptionPricingsRequestType request,
            ListSubscriptionPricingsResponseType response)
    {
        super("ListSubscriptionPricings", service, request, response, log);        
    }
    
    public void processImpl()
        throws BackingStoreException
    {
        ListSubscriptionPricingsRequestType listSubscriptionPricingsRequest = (ListSubscriptionPricingsRequestType) request;
        ListSubscriptionPricingsResponseType listSubscriptionPricingsResponse = (ListSubscriptionPricingsResponseType) response;

        if (getCountry() == null) {
            setErrorCode(StatusCode.ECS_BAD_COUNTRY_CODE, null);
            return;
        }

        String countryCode = listSubscriptionPricingsRequest.getCountryCode();  

        SubscriptionPricingType[] pricing = (SubscriptionPricingType[]) 
            service.getSubscriptionPricingsCache().get(countryCode);
        listSubscriptionPricingsResponse.setSubscriptionPricings(pricing);
    }

    /**
     * Returns a list of subscription pricings
     */
    public static ListSubscriptionPricingsResponseType listSubscriptionPricings(
            ECommerceServiceImpl service,
            ListSubscriptionPricingsRequestType listSubscriptionPricingsRequest) 
    {
        ListSubscriptionPricingsResponseType response = 
            new ListSubscriptionPricingsResponseType();
        ListSubscriptionPricings checkAccount = new ListSubscriptionPricings(service, listSubscriptionPricingsRequest, response);        
        checkAccount.process();
        return response;
    }
}

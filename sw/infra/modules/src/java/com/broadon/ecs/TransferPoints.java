package com.broadon.ecs;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.logging.Log;
import com.broadon.db.DataSourceScheduler;
import com.broadon.util.BackingStoreException;
import com.broadon.wsapi.ecs.AccountPaymentType;
import com.broadon.wsapi.ecs.MoneyType;
import com.broadon.wsapi.ecs.PaymentMethodType;
import com.broadon.wsapi.ecs.TransferPointsRequestType;
import com.broadon.wsapi.ecs.TransferPointsResponseType;
import com.broadon.wsapi.pas.BalanceType;

/**
 * The <code>TransferPoints</code> class implements the ECS TransferPoints
 * function.
 * 
 * @version $Revision: 1.3 $
 */
public class TransferPoints extends ECommerceFunction {
    // Uses apache commons logger (follows axis logging)
    protected static Log log = LogFactory.getLog(TransferPoints.class.getName());
    private static final String NEW_TRANSACTION = "INSERT INTO ECS_TRANSACTIONS "
            + " (TRANS_ID, TRANS_DATE, TRANS_TYPE, STATUS, TOTAL_AMOUNT, TOTAL_PAID, CURRENCY, "
            + " ACCOUNT_ID, PAYMENT_TYPE, PAYMENT_METHOD_ID, COUNTRY_ID) "
            + " VALUES (?, SYS_EXTRACT_UTC(CURRENT_TIMESTAMP), ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String UPDATE_TRANSACTION = "UPDATE ECS_TRANSACTIONS SET STATUS=?, " +
            " STATUS_DATE=SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) "
            + " WHERE TRANS_ID=?";

    public TransferPoints(ECommerceServiceImpl service,
            TransferPointsRequestType request,
            TransferPointsResponseType response) {
        super("TransferPoints", service, request, response, log);
    }

    private void newTransaction(Connection conn, long transId, String account, 
            BigDecimal points, String currency, String paymentMethod, 
            String paymentMethodId, String transType, int countryId)
            throws SQLException {
        PreparedStatement stmt = null;
        try {
            int i = 1;
            stmt = conn.prepareStatement(NEW_TRANSACTION);
            stmt.setLong(i++, transId);
            stmt.setString(i++, transType);
            stmt.setString(i++, Invoice.STATUS_OPEN);
            stmt.setBigDecimal(i++, points);
            stmt.setBigDecimal(i++, points);
            stmt.setString(i++, currency);
            stmt.setString(i++, account);
            stmt.setString(i++, paymentMethod);
            stmt.setString(i++, paymentMethodId);
            stmt.setInt(i++, countryId);
            stmt.executeUpdate();
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    private void updateTransaction(Connection conn, long transId, String status)
            throws SQLException {
        PreparedStatement stmt = null;
        try {
            int i = 1;
            stmt = conn.prepareStatement(UPDATE_TRANSACTION);
            stmt.setString(i++, status);
            stmt.setLong(i++, transId);
            stmt.executeUpdate();
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    public void processImpl() throws SQLException, BackingStoreException {
        TransferPointsRequestType transferPointsRequest = (TransferPointsRequestType) request;
        TransferPointsResponseType transferPointsResponse = (TransferPointsResponseType) response;
        Connection conn = null;
        DataSource dataSource = service.getEcsDataSource();
        try {
            AccountPaymentType account = transferPointsRequest.getSourceAccount();
            AccountPaymentType targetAccount = transferPointsRequest.getTargetAccount();
            MoneyType points = transferPointsRequest.getPoints();
            conn = service.getConnection(dataSource, DataSourceScheduler.DB_PRIORITY_MEDIUM);
            // Transfer Points
            conn.setAutoCommit(false);
            if (account == null) {
                setErrorCode(StatusCode.ECS_SOURCE_ACCOUNT_MISSING, null);
                return;
            }
            if (targetAccount == null) {
                setErrorCode(StatusCode.ECS_TARGET_ACCOUNT_MISSING, null);
                return;
            }
            if (points == null) {
                setErrorCode(StatusCode.ECS_TRANSFER_POINT_MISSING, null);
                return;
            }
            log.debug("Transfer " + points.getAmount() + " " + points.getCurrency() + " from "
                      + account.getAccountNumber() + " to " + targetAccount.getAccountNumber());
            // Create transactions
            String paymentMethod = PaymentMethodType.ACCOUNT.getValue();
            long fromTransId = Invoice.getNextTransactionId(conn);
            newTransaction(conn, fromTransId, account.getAccountNumber(), 
                           new BigDecimal(points.getAmount()), points.getCurrency(), paymentMethod,
                           targetAccount.getAccountNumber(), Invoice.TRANS_TRANSFER_FROM, getCountry().countryId);
            long toTransId = Invoice.getNextTransactionId(conn);
            newTransaction(conn, toTransId, targetAccount.getAccountNumber(), 
                           new BigDecimal(points.getAmount()), points.getCurrency(), paymentMethod,
                           account.getAccountNumber(), Invoice.TRANS_TRANSFER_TO, getCountry().countryId);
            
            try {
                AccountPayment payment = new AccountPayment(service.getPasSOAP(), account);
                payment.setPaymentAmount(new com.broadon.wsapi.pas.MoneyType(points.getCurrency(), points.getAmount()));
                if (payment.authorizePayment(request)) {
                    AccountPayment toAccount = new AccountPayment(service.getPasSOAP(), targetAccount);
                    toAccount.setDepositAmount(new com.broadon.wsapi.pas.MoneyType(points.getCurrency(), points.getAmount()));
                    if (payment.captureToAccount(request, toAccount)) {
                        updateTransaction(conn, fromTransId, Invoice.STATUS_SUCCESS);
                        updateTransaction(conn, toTransId, Invoice.STATUS_SUCCESS);                        
                    } else {
                        updateTransaction(conn, fromTransId, Invoice.STATUS_FAILED);
                        updateTransaction(conn, toTransId, Invoice.STATUS_FAILED);                        
                        response.setErrorCode(payment.errorCode);
                        response.setErrorMessage(payment.errorMessage);
                    }    
                } else {
                    updateTransaction(conn, fromTransId, Invoice.STATUS_FAILED);
                    updateTransaction(conn, toTransId, Invoice.STATUS_FAILED);                        
                    response.setErrorCode(payment.errorCode);
                    response.setErrorMessage(payment.errorMessage);
                }
            } catch (RemoteException e) {
                setErrorCode(StatusCode.ECS_PAS_ERROR, e.getMessage());
                log.error("Error checking balance using PAS", e);
            } catch (InvalidPaymentException e) {
                setErrorCode(StatusCode.ECS_PAS_ERROR, e.getMessage());
                log.error("Error checking balance using PAS", e);
                e.printStackTrace();
            } 

            conn.commit();
            log.debug("Transferred " + points.getAmount()
                    + " points from account " + account.getAccountNumber()
                    + " to account " + targetAccount.getAccountNumber());
        } finally {
            try {
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            } catch (SQLException e) {
                log.warn("Error closing connection", e);
            }
        }
    }

    public static TransferPointsResponseType transferPoints(
            ECommerceServiceImpl service,
            TransferPointsRequestType transferPointsRequest) {
        TransferPointsResponseType response = new TransferPointsResponseType();
        TransferPoints transferPoints = new TransferPoints(service, transferPointsRequest, response);
        transferPoints.process();
        return response;
    }
}

package com.broadon.ecs;

import java.rmi.RemoteException;

import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.logging.Log;

import com.broadon.wsapi.ecs.*;

/**
 * The <code>CheckECardBalance</code> class implements the ECS CheckECardBalance function.
 * 
 * @version $Revision: 1.1 $
 */
public class CheckECardBalance extends ECommerceFunction {

    // Uses apache commons logger (follows axis logging)
    protected static Log log = 
        LogFactory.getLog(CheckECardBalance.class.getName());

    public CheckECardBalance(
            ECommerceServiceImpl service, 
            CheckECardBalanceRequestType request,
            CheckECardBalanceResponseType response)
    {
        super("CheckECardBalance", service, request, response, log);        
    }
    
    public void processImpl()
    {
        CheckECardBalanceRequestType checkECardRequest = (CheckECardBalanceRequestType) request;
        CheckECardBalanceResponseType checkECardResponse = (CheckECardBalanceResponseType) response;
        ECardPayment eCard = null;
        try {
            eCard = new ECardPayment(
                request,
                service.getPasSOAP(),
                checkECardRequest.getECard(),
                false);
            
            eCard.checkECardType(request);

            if (eCard.checkBalance(request)) {
                MoneyType available = eCard.getAvailableBalance();
                checkECardResponse.setBalance(available);
            } else {
                response.setErrorCode(eCard.errorCode);
                response.setErrorMessage(eCard.errorMessage);
            }           
        } catch (InvalidPaymentException e) {
            setErrorCode(StatusCode.ECS_ECARD_CANNOT_BE_REDEEMED, e.getMessage());
            log.error("Invalid ECard", e);
        } catch (RemoteException e) {
            setErrorCode(StatusCode.ECS_PAS_ERROR, e.getMessage());
            log.error("Error checking balance using PAS", e);
        } finally {
            if (eCard != null) {
                checkECardResponse.setECardInfo(eCard.getECardInfo());
            }
        }
    }

    public static CheckECardBalanceResponseType checkECardBalance(
            ECommerceServiceImpl service,
            CheckECardBalanceRequestType request)
    {
        CheckECardBalanceResponseType response = new CheckECardBalanceResponseType();
        CheckECardBalance checkECard = new CheckECardBalance(service, request, response);
        checkECard.process();
        return response;
    }
}

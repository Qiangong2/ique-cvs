package com.broadon.ecs;

import java.rmi.RemoteException;

import com.broadon.wsapi.ecs.AbstractRequestType;
import com.broadon.wsapi.pas.BalanceType;
import com.broadon.wsapi.pas.CapturePaymentRequestType;
import com.broadon.wsapi.pas.CapturePaymentResponseType;
import com.broadon.wsapi.pas.MoneyType;
import com.broadon.wsapi.pas.PasTokenType;
import com.broadon.wsapi.pas.PaymentAuthorizationPortType;
import com.broadon.wsapi.pas.VoidAuthorizationRequestType;
import com.broadon.wsapi.pas.VoidAuthorizationResponseType;

/**
 * The <code>Payment</code> class interfaces with
 * the PAS to handle a payment.
 *
 * @version $Revision: 1.7 $
 */
abstract public class Payment {
    public static final String POINTS = "POINTS";
    
    final protected PaymentAuthorizationPortType pas;
    
    protected int errorCode;
    protected String errorMessage;
    protected PasTokenType authToken; 
    
    protected MoneyType paymentAmount;
    protected MoneyType depositAmount;
    protected String referenceId;

    protected Payment(PaymentAuthorizationPortType pas, String referenceId, MoneyType paymentAmount, MoneyType depositAmount)
    {
        this.pas = pas;
        this.referenceId = referenceId;
        this.paymentAmount = paymentAmount;
        this.depositAmount = depositAmount;
    }
    
    protected Payment(PaymentAuthorizationPortType pas, String referenceId)
    {
        this(pas, referenceId, null, null);
    }
    
    public PasTokenType getAuthorizationToken() {
        return authToken;
    }
    
    public void setPaymentAmount(MoneyType paymentAmount)
    {
        this.paymentAmount = paymentAmount;
    }
    
    public void setPaymentPoints(long points)
    {
        this.paymentAmount = pointsToMoney(points);
    }
    
    public void setDepositAmount(MoneyType depositAmount)
    {
        this.depositAmount = depositAmount;
    }
    
    public void setDepositPoints(long points)
    {
        this.depositAmount = pointsToMoney(points);
    }
    
    public boolean voidAuthorization(AbstractRequestType request)
        throws RemoteException 
    {
        VoidAuthorizationRequestType voidAuthRequest = 
            new VoidAuthorizationRequestType();
        ECommerceFunction.initPasRequest(request, voidAuthRequest);
        voidAuthRequest.setAuthorizationToken(authToken);
        VoidAuthorizationResponseType voidAuthResponse =
            pas.voidAuthorization(voidAuthRequest);
        errorCode = voidAuthResponse.getErrorCode();
        errorMessage = voidAuthResponse.getErrorMessage();
        return (voidAuthResponse.getErrorCode() == ECommerceFunction.STATUS_OK);
    }
    
    public boolean capturePayment(AbstractRequestType request)
        throws RemoteException
    {
        CapturePaymentRequestType capPaymentRequest = new CapturePaymentRequestType();
        ECommerceFunction.initPasRequest(request, capPaymentRequest);
        capPaymentRequest.setAmount(paymentAmount);
        capPaymentRequest.setAuthorizationToken(authToken);
        CapturePaymentResponseType capPaymentResponse =
            pas.capturePayment(capPaymentRequest);

        errorCode = capPaymentResponse.getErrorCode();
        errorMessage = capPaymentResponse.getErrorMessage();
        return (capPaymentResponse.getErrorCode() == ECommerceFunction.STATUS_OK);
    }

    public void setReferenceId(String refId)
    {
        referenceId = refId;
    }

    public com.broadon.wsapi.ecs.MoneyType getAvailableBalance()
    {
        BalanceType balance = getBalance();
        if (balance != null) {
            com.broadon.wsapi.ecs.MoneyType available = new com.broadon.wsapi.ecs.MoneyType();
            available.setCurrency(balance.getAvailable().getCurrencyType());
            available.setAmount(balance.getAvailable().getAmount());
            return available;
        } else {
            return null;
        }
    }
    
    abstract public String getPaymentType();
    abstract public String getPaymentMethodId();

    abstract public BalanceType getBalance();
    abstract public boolean checkBalance(AbstractRequestType request)
        throws RemoteException;

    abstract public boolean authorizePayment(AbstractRequestType request)
        throws RemoteException;
    
    abstract public boolean captureToAccount(AbstractRequestType request, AccountPayment toAccount)
        throws RemoteException, InvalidPaymentException;
    
    public static MoneyType pointsToMoney(long points)
    {
        com.broadon.wsapi.pas.MoneyType amount = new com.broadon.wsapi.pas.MoneyType();
        amount.setAmount(Long.toString(points));
        amount.setCurrencyType(POINTS);
        return amount;
    }
}

package com.broadon.ecs;

import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.logging.Log;

import com.broadon.util.BackingStoreException;
import com.broadon.wsapi.ecs.ListTitlesRequestType;
import com.broadon.wsapi.ecs.ListTitlesResponseType;
import com.broadon.wsapi.ecs.TitleType;

/**
 * The <code>ListTitles</code> class implements
 * the ECS ListTitles function.
 * 
 * Provides a list of title ids and names for a given country, device type,
 * and purchase type.
 * 
 * @version $Revision: 1.26 $
 */
public class ListTitles extends ECommerceFunction {
    // Uses apache commons logger (follows axis logging)
    protected static Log log = LogFactory.getLog(ListTitles.class.getName());

    public ListTitles(
            ECommerceServiceImpl service, 
            ListTitlesRequestType request,
            ListTitlesResponseType response)
    {
        super("ListTitles", service, request, response, log);        
    }
    
    public void processImpl()
        throws BackingStoreException
    {
        ListTitlesRequestType listTitlesRequest = (ListTitlesRequestType) request;
        ListTitlesResponseType listTitlesResponse = (ListTitlesResponseType) response;

        if (getCountry() == null) {
            setErrorCode(StatusCode.ECS_BAD_COUNTRY_CODE, null);
            return;
        }

        Integer channelId = null;
        try {
            if (listTitlesRequest.getChannelId() != null) {
                channelId = 
                    new Integer(ECommerceFunction.parseChannelId(
                            listTitlesRequest.getChannelId()));
            }
        } catch (Throwable e){
            setErrorCode(
                    StatusCode.ECS_INVALID_CHANNEL_ID, 
                    "Error parsing channel " + listTitlesRequest.getChannelId());
            return;
        }
        
        ListTitlesCache.Key key = 
            new ListTitlesCache.Key(
                    getCountry(),
                    getDevice(),                
                    listTitlesRequest.getTitleKind(), 
                    listTitlesRequest.getPricingKind(),
                    channelId);
            
        listTitlesResponse.setTitles((TitleType[]) service.getListTitleCache().get(key));
    }

    /**
     * Returns a list of titles
     */
    public static ListTitlesResponseType listTitles(
            ECommerceServiceImpl service,
            ListTitlesRequestType listTitlesRequest) 
    {
        ListTitlesResponseType response = new ListTitlesResponseType();
        ListTitles listTitles = new ListTitles(service, listTitlesRequest, response);        
        listTitles.process();
        return response;
    }
}

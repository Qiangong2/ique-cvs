package com.broadon.ecs;

import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.exception.BroadOnException;
import com.broadon.util.BackingStoreException;
import com.broadon.wsapi.ecs.*;

/**
 * The <code>RedeemECard</code> class implements the ECS RedeemECard function.
 * 
 * Redeems a ecard for a title, subscription, or points
 * 
 * @version $Revision: 1.10 $
 */
public class RedeemECard extends ECommerceFunction {
    // Uses apache commons logger (follows axis logging)
    protected static Log log = 
        LogFactory.getLog(RedeemECard.class.getName());
    
    protected ECardPayment eCardPayment;
    protected List items; /* List of items that can be redeemed using this eCard */
    protected long reqTitleId;

    protected RedeemECard(ECommerceServiceImpl service, RedeemECardRequestType request, RedeemECardResponseType response)
    {
        super("RedeemECard", service, request, response, log);
    }    
    
    protected boolean checkCountryCode(String countryCode) 
    {
        if (countryCode == null || countryCode.equals(request.getCountryCode())) {
            return true;
        } else {
            return false;
        }
    }
    
    protected boolean checkDeviceType(int deviceType) 
    {
        if (deviceType == getDevice().getDeviceType()) {
            return true;
        } else {
            return false;
        }
    }
    
    protected boolean checkTitleId(long titleId) 
    {
        if (((RedeemECardRequestType) request).getTitleId() == null || titleId == reqTitleId) {
            return true;
        } else {
            return false;
        }
    }
    
   public PurchaseItemInfo getItem()
   {
       if (items == null || items.size() == 0) {
           setErrorCode(StatusCode.ECS_ECARD_CANNOT_BE_REDEEMED, "No items for eCards");
           return null;
       }
       
       int nRedeemKindMatched = 0;
       int nDeviceMatched = 0;
       int nCountryMatched = 0;
       int nTitleMatched = 0;
       
       RedeemECardRequestType redeemECardRequest = (RedeemECardRequestType) request;

       for (int i = 0; i < items.size(); i++) {
           boolean deviceMatched = false;
           boolean countryMatched = false;
           boolean titleMatched = false;
           boolean redeemKindMatched = false;
               
           RedeemKindType redeemKind = redeemECardRequest.getRedeemKind();
           PurchaseItemInfo item = (PurchaseItemInfo) items.get(i);
           if (checkDeviceType(item.deviceType)) {
               deviceMatched = true;
               nDeviceMatched++;
           }
           
           if (checkCountryCode(item.country)) {
               countryMatched = true;
               nCountryMatched++;
           }
           
           if (item instanceof TicketedItemInfo) {
               TicketedItemInfo ticketedItem = (TicketedItemInfo) item;
               if (TicketedItemInfo.SUBSCRIPTION_TYPE.equalsIgnoreCase(ticketedItem.licenseType)) {
                   if ((redeemKind == null) || RedeemKindType.Subscription.equals(redeemKind)) {
                       redeemKindMatched  = true;                       
                   }                   
               } else {
                   if ((redeemKind == null) || RedeemKindType.Title.equals(redeemKind)) {
                       redeemKindMatched  = true;
                   }                   
               }
               if (checkTitleId(ticketedItem.titleId)) {
                   titleMatched = true;
                   nTitleMatched++;
               }
           } else {
               /* TODO: Not a title item, check that title is not provided */
               if ((redeemKind == null) || RedeemKindType.Points.equals(redeemKind)) {
                   redeemKindMatched  = true;
               }
               if (deviceMatched && countryMatched && redeemKindMatched) {
                   return item;
               }
           }
           
           if (deviceMatched && countryMatched && titleMatched && redeemKindMatched) {
               return item;
           }
           
           if (redeemKindMatched) {
               nRedeemKindMatched++;
           }
       }

       if (nDeviceMatched == 0) {
            setErrorCode(StatusCode.ECS_BAD_DEVICE_TYPE,
                     "Ecard " + eCardPayment.eCardType + " cannot be redeemed for device type " + 
                     getDevice().getDeviceType());
       } else if (nCountryMatched == 0) {
            setErrorCode(StatusCode.ECS_BAD_COUNTRY_CODE,
                    "Ecard " + eCardPayment.eCardType + " cannot be redeemed in country " + request.getCountryCode());
       } else if (nRedeemKindMatched == 0) {
            setErrorCode(StatusCode.ECS_BAD_REDEEM_TYPE,
                    "Ecard " + eCardPayment.eCardType + " cannot be redeemed for " + redeemECardRequest.getRedeemKind());
       } else if ((redeemECardRequest.getTitleId() != null) && (nTitleMatched == 0)) {
            setErrorCode(StatusCode.ECS_BAD_TITLE_ID,
                    "Ecard " + eCardPayment.eCardType + " cannot be redeem for title " + redeemECardRequest.getTitleId()); 
            return null;
       } else {
           setErrorCode(StatusCode.ECS_ECARD_CANNOT_BE_REDEEMED, null);                
       }
       
       return null;
   }
    
    
    public void processImpl()
        throws SQLException, BackingStoreException, BroadOnException
    {
        RedeemECardRequestType redeemECardRequest = (RedeemECardRequestType) request;
        RedeemECardResponseType redeemECardResponse = (RedeemECardResponseType) response;
    
        if (getCountry() == null) {
            setErrorCode(StatusCode.ECS_BAD_COUNTRY_CODE, null);
            return;
        }

        if (redeemECardRequest.getTitleId() != null) {
            try {
                reqTitleId = parseTitleId(redeemECardRequest.getTitleId());
            } catch (Throwable e) {
                setErrorCode(StatusCode.ECS_BAD_TITLE_ID, 
                        "Error parsing title " + redeemECardRequest.getTitleId());
                return;                
            }
        }
        
        try {
            eCardPayment = new ECardPayment(
                                   request,
                                   service.getPasSOAP(), 
                                   redeemECardRequest.getECard());
            if (eCardPayment.errorCode != ECommerceFunction.STATUS_OK) {
                response.setErrorCode(eCardPayment.errorCode);
                response.setErrorMessage(eCardPayment.errorMessage);
                return;
            }
        } catch (InvalidPaymentException e) {               
            setErrorCode(StatusCode.ECS_INVALID_PAYMENT, e.getMessage());
            return;
        } catch (RemoteException e) {               
            setErrorCode(StatusCode.ECS_PAS_ERROR, e.getMessage());
            log.error("PAS error while looking up eCard", e);
            return;
        }
        
        /* Lookup Item */
        items = (List) service.getECardItemsCache().get(new Integer(eCardPayment.eCardType));
        PurchaseItemInfo item = getItem();
        PurchaseItem redeemItem;
        if (item == null) {
            return;
        } else if (item instanceof TicketedItemInfo) {
            TicketedItemInfo ticketedItem = (TicketedItemInfo) item;
            if (TicketedItemInfo.SUBSCRIPTION_TYPE.equalsIgnoreCase(ticketedItem.licenseType)) {
                // ECard is for subscription
                // Copy ticketedItem since limits will be updated
                redeemItem = new RedeemSubscriptionECard(
                    this, eCardPayment, new TicketedItemInfo(ticketedItem));
            } else {
                // ECard is for title
                // Copy ticketedItem since limits will be updated
                redeemItem = new RedeemTitleECard(this, eCardPayment, new TicketedItemInfo(ticketedItem));
            }
        } else {
            // ECard is for points
            redeemItem = new RedeemPointsECard(this, eCardPayment, item); 
        }
            
        try {
            redeemItem.doPurchase();
        } catch (InvalidSubscriptionException e) {
            setErrorCode(StatusCode.ECS_INVALID_SUBSCRIPTION, e.getLocalizedMessage());
            log.debug("RedeemECard error", e);
        }
    }

    /**
     * Service the RedeemECard request
     * @param service
     * @param redeemECardRequest
     * @return
     */
    public static RedeemECardResponseType redeemECard(
            ECommerceServiceImpl service, 
            RedeemECardRequestType redeemECardRequest) 
    {
        RedeemECardResponseType response = new RedeemECardResponseType();
        RedeemECard redeemECard = new RedeemECard(service, redeemECardRequest, response);
        redeemECard.process();
        return response;
    }
}

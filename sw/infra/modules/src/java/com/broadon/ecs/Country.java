package com.broadon.ecs;

/**
 * The <code>Country</code> class contains information
 * about a country.
 *
 * @version $Revision: 1.3 $
 */
public class Country {
    protected final String countryCode;
    protected final int countryId;
    protected final String region;
    protected final String language;
    
    public Country(String region, int countryId, String countryCode, String language)
    {
        this.countryCode = countryCode;
        this.countryId = countryId;
        this.region = region;
        this.language = language;
    }

    public boolean equals(Object obj)
    {
        if (obj instanceof Country) {
            Country other = (Country) obj;
            return (this.countryId == other.countryId) &&
            ((this.countryCode == null)? (other.countryCode == null): 
                (this.countryCode.equals(other.countryCode))) &&
                ((this.region == null)? (other.region == null): 
                    (this.region.equals(other.region)));
        } else {
            return false;
        }
    }
    
    public int hashCode()
    {
        return countryId;            
    }
    
    public String toString()
    {
        return "country " + countryCode + " id " + countryId + " region " + region; 
    }
}

package com.broadon.ecs;

import java.math.BigDecimal;
import java.math.BigInteger;

import com.broadon.util.BackingStoreException;
import com.broadon.wsapi.ecs.ContentInfoType;
import com.broadon.wsapi.ecs.RatingType;
import com.broadon.wsapi.ecs.TitleInfoType;
import com.broadon.wsapi.ecs.TitleVersionType;

/**
 * The <code>TitleCatalog</code> class provides functions for
 * getting information about titles from the database.
 *
 * @version $Revision: 1.13 $
 */
public class TitleCatalog 
{
    private TitleContentsCache titleContentsCache;
    private TitleRatingsCache titleRatingsCache;
    private TitleInfoCache titleInfoCache;
    private SystemTitleCache systemTitleCache;
    private ETicketTitleInfoCache eTicketTitleInfoCache;
    
    
    public TitleCatalog(ECommerceServiceImpl service)
    {
        titleContentsCache = new TitleContentsCache(service);
        titleRatingsCache = new TitleRatingsCache(service);
        titleInfoCache = new TitleInfoCache(service);
        systemTitleCache = new SystemTitleCache(service);
        eTicketTitleInfoCache = new ETicketTitleInfoCache(service);
    }

    /**
     * Look up ETKM content id from titleId
     */
    public static BigDecimal getContentId(long titleId)
    {
        /* TODO: Use CAS to get content id */
        BigInteger contentId = new BigInteger(Long.toHexString(titleId) + "ffffffff", 16);
        return new BigDecimal(contentId);
    }

    /**
     * Returns title related information needed to create a eticket given a eticket content id
     */
    public ETicketTitleInfoCache.ETicketTitleInfo getETicketTitleInfo(BigDecimal etkmContentId)
        throws BackingStoreException
    {
        return (ETicketTitleInfoCache.ETicketTitleInfo) eTicketTitleInfoCache.get(etkmContentId);        
    }
    
    /**
     * Returns title related information needed to create a eticket given a title id
     */
    public ETicketTitleInfoCache.ETicketTitleInfo getETicketTitleInfo(long titleId)
        throws BackingStoreException
    {
        return (ETicketTitleInfoCache.ETicketTitleInfo) eTicketTitleInfoCache.get(new Long(titleId));        
    }
    
    /**
     * Returns ratings for a given title
     */
    public RatingType[] getTitleRatings(TitleIdKey key)
        throws BackingStoreException
    {
        return (RatingType[]) titleRatingsCache.get(key);
    }

    /**
     * Returns details for a given title
     */
    public TitleInfoType getTitleInfo(TitleIdKey key)
        throws BackingStoreException
    {
        return (TitleInfoType) titleInfoCache.get(key);
    }

    /**
     * Returns content list for a given title
     */
    public ContentInfoType[] getContents(long titleId)
        throws BackingStoreException
    {
        return (ContentInfoType[]) titleContentsCache.get(new Long(titleId));
    }
    
    /**
     * Returns list of system titles
     */
    public TitleVersionType[] getSystemTitles(Country country, int deviceType)
        throws BackingStoreException
    {
        return (TitleVersionType[]) systemTitleCache.get(new SystemTitleCache.Key(country, deviceType));
    }
}

package com.broadon.ecs;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Random;

import javax.servlet.ServletContext;
import javax.sql.DataSource;
import javax.xml.rpc.ServiceException;
import javax.xml.rpc.server.ServiceLifecycle;
import javax.xml.rpc.server.ServletEndpointContext;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;
import org.apache.axis.types.URI;
import org.apache.axis.types.URI.MalformedURIException;

import com.broadon.db.OracleDataSource.OracleDataSourceProxy;
import com.broadon.servlet.ServletConstants;
import com.broadon.trans.common.AuditLog;
import com.broadon.trans.common.Logger;

import com.broadon.wsapi.ets.*;
import com.broadon.wsapi.ias.*;
import com.broadon.wsapi.pas.PaymentAuthorizationPortType;
import com.broadon.wsapi.pas.PaymentAuthorizationService;
import com.broadon.wsapi.pas.PaymentAuthorizationServiceLocator;

/**
 * ECommerceServiceImpl provides the implementation for
 * the ECommerce Services.
 * @author angelc
 * * @version $Revision: 1.32 $
 */
public class ECommerceServiceImpl implements ServiceLifecycle, ServletConstants
{   
    /** Servlet Context */
    protected ServletContext servletContext;
    
    /** DataSource for ECS to connect to the DB */
    protected DataSource ecsDataSource;
    
    /** CAS uses main data source */
    private static final String ECS_DATA_SOURCE_KEY = DATA_SOURCE_KEY;
    
    /** DataSource for CAS to connect to the DB */
    protected DataSource casDataSource;
    
    /** CAS uses alt data source 1 */
    private static final String CAS_DATA_SOURCE_KEY = DATA_SOURCE_KEY + "_1";
    
    /** Handle to the ETicket Service */
    protected ETicketPortType eTicketPort;
     
    /** Handle to the IAS Service */
    protected IdentityAuthenticationPortType iasPort;

    /** Handle to the PAS Service */
    protected PaymentAuthorizationPortType pasPort;
    
    /** Audit log hander. */
    protected AuditLog auditLog;

    /** Content Prefix URI */
    protected URI contentPrefixUri;
    protected URI uncachedContentPrefixUri;

    /** ETS, IAS, PAS URLs */
    protected URL etsUrl;
    protected URL iasUrl;
    protected URL pasUrl;

    /** Cache timeout in milliseconds */
    protected long contentCacheTimeoutMsecs;

    // Uses apache commons logger (follows axis logging)
    protected static Log log = 
        LogFactory.getLog(ECommerceServiceImpl.class.getName());
    
    private static final String DEF_CONTENT_CACHE_TIMEOUT_SECS = "30";
    private static final String CFG_CONTENT_CACHE_TIMEOUT_SECS = "CONTENT_CACHE_TIMEOUT_SECS";

    private static final String DEF_CONTENT_PREFIX_URL = "http://ccs:16983/ccs/download";
    private static final String CFG_CONTENT_PREFIX_URL = "CONTENT_PREFIX_URL";
    private static final String CFG_UNCACHED_CONTENT_PREFIX_URL = "UNCACHED_CONTENT_PREFIX_URL";

    private static final String CFG_ETS_URL = "ETS_URL";
    private static final String CFG_IAS_URL = "IAS_URL";
    private static final String CFG_PAS_URL = "PAS_URL";
    
    /* Random number */
    private Random random = new Random();
    
    CountryCache countryCache = null;
    ECardItemsCache eCardItemsCache = null;
    PurchaseItemsCache purchaseItemsCache = null;
    TitleCatalog titleCatalog = null;
    ListTitlesCache listTitleCache = null;
    TitleDetailsCache titleDetailsCache = null;
    SubscriptionPricingsCache subscriptionPricingsCache = null;
    
    public ECommerceServiceImpl()
    {
    	// Force initialization of StatusCode class
    	try {
    	    Class.forName("com.broadon.ecs.StatusCode");
    	} catch  (ClassNotFoundException e) {
    	    log.error("Cannot initialize ECS StatusCode");
    	}
    }
    
   /**
     * Called by the JAX RPC runtime to initialize
     * the service endpoint.  Implements ServiceLifecycle.
     */
    public void init(Object context) throws ServiceException  
    { 
        log.debug("ECommerce Service Init");

        ServletEndpointContext soapContext = (ServletEndpointContext) context;                
        servletContext = soapContext.getServletContext();
        
        // Get server properties (requires ConfigListener)
        
        // Get data source (set by OracleDSListener)
        ecsDataSource = (DataSource)
            servletContext.getAttribute(ECS_DATA_SOURCE_KEY);
        if (ecsDataSource == null) {
            log.error("ECS data source not found");
        }
        
        // Get data source (set by OracleDSListener)
        casDataSource = (DataSource)
            servletContext.getAttribute(CAS_DATA_SOURCE_KEY);
        if (casDataSource == null) {
            log.error("CAS data source not found ... using same data source as ECS");
            casDataSource = ecsDataSource;
        }
        
        // Get audit log (set by AuditLogManager)
        auditLog = (AuditLog) servletContext.getAttribute(Logger.LOGGER_KEY);
        
        // Property file defined in the conf/BBserver.properties.tmpl file.
        // Add properties there as required for the ECS server.
        //
        Properties cfg = (Properties) servletContext.getAttribute(PROPERTY_KEY);

        // Get cache timeout
        contentCacheTimeoutMsecs =
            1000*Long.parseLong(getCfg(cfg, CFG_CONTENT_CACHE_TIMEOUT_SECS,
                                       DEF_CONTENT_CACHE_TIMEOUT_SECS));
        if (contentCacheTimeoutMsecs < 0) {
            log.warn(CFG_CONTENT_CACHE_TIMEOUT_SECS +
                         " is negative, using zero (no caching) instead");
            contentCacheTimeoutMsecs = 0;
        }
        
        countryCache = new CountryCache(this);
        purchaseItemsCache = new PurchaseItemsCache(this);
        eCardItemsCache = new ECardItemsCache(this);
        titleCatalog = new TitleCatalog(this);
        listTitleCache = new ListTitlesCache(this);
        titleDetailsCache = new TitleDetailsCache(this);
        subscriptionPricingsCache = new SubscriptionPricingsCache(this);
        
        // Get Content Prefix URL
        contentPrefixUri = getURICfg(cfg, CFG_CONTENT_PREFIX_URL, DEF_CONTENT_PREFIX_URL);
        uncachedContentPrefixUri = getURICfg(cfg, CFG_UNCACHED_CONTENT_PREFIX_URL, null);
        if (uncachedContentPrefixUri == null) {
            log.info(CFG_UNCACHED_CONTENT_PREFIX_URL + " not specified, using the " + 
                    CFG_CONTENT_PREFIX_URL + " of " + contentPrefixUri);
            uncachedContentPrefixUri = contentPrefixUri;
        }

        // Get ETS, IAS, PAS URLs
        etsUrl = getURLCfg(cfg, CFG_ETS_URL, null);
        iasUrl = getURLCfg(cfg, CFG_IAS_URL, null);
        pasUrl = getURLCfg(cfg, CFG_PAS_URL, null);

        // Instantiate ETS, IAS, PAS services
        // Assumes these object can be used across threads 
        // May need to use a object pool 
  
        ETicketService eTicketService = new ETicketServiceLocator();
        eTicketPort = (etsUrl == null)? eTicketService.getETicketSOAP():
                           eTicketService.getETicketSOAP(etsUrl);

        IdentityAuthenticationService iasService = new IdentityAuthenticationServiceLocator();
        iasPort = (iasUrl == null)? iasService.getIdentityAuthenticationSOAP():
                        iasService.getIdentityAuthenticationSOAP(iasUrl);
        
        PaymentAuthorizationService pasService = new PaymentAuthorizationServiceLocator();
        pasPort = (pasUrl == null)? pasService.getPaymentAuthorizationSOAP():
                        pasService.getPaymentAuthorizationSOAP(pasUrl);
        
        // Seed random number generator
        random.setSeed(System.currentTimeMillis());
    } 

    /**
     * Called by the JAX RPC runtime to destroy resources
     * used by the service endpoint at the end of its lifecycle.
     * Implements ServiceLifecycle.
     */
    public void destroy() 
    {
        log.debug("ECommerce Service Destroy");
    }

    // Helper functions for getting the configuration
    
    private URI getURICfg(Properties cfg, String key, String defValue)
        throws ServiceException
    {
        String uriStr = getCfg(cfg, key, defValue);
        if (uriStr != null) {
            uriStr = uriStr.trim();
            if (uriStr.length() == 0) {
                uriStr = null;
            }
        }
        try {
            URI uri = (uriStr != null)? new URI(uriStr): null;
            return uri;
        }
        catch (MalformedURIException e) {
            throw new ServiceException("Malformed " + key+ ": " + uriStr, e);
        }
    }
    
    private URL getURLCfg(Properties cfg, String key, String defValue)
        throws ServiceException
    {
        String urlStr = getCfg(cfg, key, defValue);
        if (urlStr != null) {
            urlStr = urlStr.trim();
            if (urlStr.length() == 0) {
                urlStr = null;
            }
        }
        try {
            URL url = (urlStr != null)? new URL(urlStr): null;
            return url;
        }
        catch (MalformedURLException e) {
            throw new ServiceException("Malformed " + key+ ": " + urlStr, e);
        }
    }

    private String getCfg(Properties cfg, String key, String defValue)
    {
        String v = (cfg != null)? cfg.getProperty(key): null;
        if (v == null) {
            v = defValue;
            log.warn("BBserver.properties missing " + key + " value" +
                     " ... using default of " + defValue);
        } else {
            log.info("Setting " + key + " to " + v);
        }
        return v;
    }

    /* Accessor functions */
    public String getName()
    {
        return "ECS";
    }
    
    public long getContentCacheTimeoutMsecs()
    {
    	return contentCacheTimeoutMsecs;
    }

    public DataSource getEcsDataSource()
    {
        return ecsDataSource;
    }
    
    public DataSource getCasDataSource()
    {
        return casDataSource;
    }
    
    public Connection getConnection(DataSource dataSource, int priority) throws SQLException
    {
        Connection conn = dataSource instanceof OracleDataSourceProxy ?
                ((OracleDataSourceProxy) dataSource).getConnection(priority) :
                    dataSource.getConnection();
        return conn;
    }
    
    public ETicketPortType getETicketSOAP()
    {
        return eTicketPort; 
    }
    
    public IdentityAuthenticationPortType getIasSOAP()
    {
    	return iasPort;
    }
    
    public PaymentAuthorizationPortType getPasSOAP()
    {
       return pasPort;
    }
    
    public AuditLog getAuditLog()
    {
        return auditLog;
    }
    
    public URI getContentPrefixUrl()
    {
    	return contentPrefixUri;
    }

    public URI getUncachedContentPrefixUrl()
    {
        return uncachedContentPrefixUri;
    }

    public TitleCatalog getTitleCatalog()
    {
        return titleCatalog;
    }

    public ListTitlesCache getListTitleCache()
    {
        return listTitleCache;
    }

    public TitleDetailsCache getTitleDetailsCache()
    {
        return titleDetailsCache;
    }

    public SubscriptionPricingsCache getSubscriptionPricingsCache()
    {
        return subscriptionPricingsCache;
    }

    public CountryCache getCountryCache()
    {
        return countryCache;
    }

    public PurchaseItemsCache getPurchaseItemsCache()
    {
        return purchaseItemsCache;
    }

    public ECardItemsCache getECardItemsCache()
    {
        return eCardItemsCache;
    }

    public long newTicketId(long titleId)
    {
        return (titleId & 0xff00000000000000L) | (random.nextLong() & 0x00ffffffffffffffL);
    }
}


package com.broadon.ecs;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.broadon.db.DataSourceScheduler;
import com.broadon.exception.BroadOnException;
import com.broadon.util.BackingStoreException;
import com.broadon.wsapi.ecs.TicketedPurchaseResponseType;
import com.broadon.wsapi.ets.ETicketPackageType;
import com.broadon.wsapi.ets.TicketsRequestType;

/**
 * The <code>PurchaseTicketedItem</code> class handles the
 * purchase of a ticketed item (which comes with etickets and certs).
 *
 * @version $Revision: 1.24 $
 */
abstract public class PurchaseTicketedItem extends PurchaseItem {
    byte[] deviceCert;
    
    protected PurchaseTicketedItem(ECommerceFunction func,     
            String transactionType,
            byte[] deviceCert)
        throws BackingStoreException
    {
        super(func, transactionType);
        this.deviceCert = deviceCert;
    }

    /**
     * Returns a TicketedItemInfo
     * @throws BackingStoreException 
     */
    abstract protected TicketedItemInfo getTicketedItem()
        throws SQLException, BroadOnException, BackingStoreException;

    /**
     * Calculate new limits
     */
    abstract protected void updateLimits(TicketedItemInfo ticketedItem) 
        throws SQLException, BroadOnException;

    
    /**
     * Get device certificate
     */
    protected byte[] getDeviceCertificate()
    {
        return deviceCert;
    }
    
    /**
     * Cannot be overridden by subclass, returns a ticketed item
     * Subclass should implement getTicketedItem
     */
    public final PurchaseItemInfo getPurchaseItem()
        throws SQLException, BroadOnException, BackingStoreException
    {
        TicketedItemInfo ticketedItem = getTicketedItem();
        return ticketedItem;
    }
    
    /**
     * Validates item to be purchased
     * Looks up 
     */
    protected boolean validatePurchaseItem(PurchaseItemInfo purchaseItem)
        throws SQLException, BroadOnException
    {
        if (!super.validatePurchaseItem(purchaseItem)) {
            return false;
        }
        
        TicketedItemInfo ticketedItem = (TicketedItemInfo) purchaseItem;
        
        if (ticketedItem.eTicketContentId == null) {
            setErrorCode(StatusCode.ECS_NO_ETKM_CONTENT_ID, null);
            return false;
        }
        
        ticketedItem.record = lookupETicket(device.getDeviceId(), ticketedItem.titleId);
        ETicketRecord oldTicket = ticketedItem.record;
        
        // Validate ticket is compatible
        if (oldTicket == null) {
            return true;
        }
        
        // Check if device already owns permanent rights to title
        if (ETicketRecord.LIMIT_TYPE_PR.equals(oldTicket.rtype)) {
            setErrorCode(StatusCode.ECS_TITLE_ALREADY_PURCHASED, null);
            return false;
        }
        
        if ("PERMANENT".equals(ticketedItem.licenseType)) {            
            // Device wants to purchase permanent rights, overrides other rights 
            return true;
        } else if ("TRIAL".equals(ticketedItem.licenseType)) {
            // Device can not try a product if it already used it
            setErrorCode(StatusCode.ECS_TRIAL_AFTER_USE_ERROR, null);
            return false;
        } else {
            // Make sure rights match
            if (ticketedItem.rtype.equals(oldTicket.rtype)) {
                return true;
            } else {
                setErrorCode(StatusCode.ECS_LIMITED_RIGHTS_ERROR,
                        "Cannot change from " + oldTicket.rtype + " to " + 
                        ticketedItem.rtype);
                return false;
            }
        }
    }
    
    /**
     * The item has been paid for, use ETS to get tickets for the title
     * @param request
     * @param response
     * @return
     */
    protected boolean deliverItem(Payment payment, PurchaseItemInfo purchaseItem)
        throws SQLException, BroadOnException
    {
        TicketedItemInfo ticketedItem = (TicketedItemInfo) purchaseItem;
        updateLimits(ticketedItem);        
        ETicketPackageType eTicketPackage = null;
        try {
            eTicketPackage = getETickets(ticketedItem);
        } catch (RemoteException e) {
            setErrorCode(StatusCode.ECS_ETS_ERROR, e.getMessage());
            log.error("Error getting tickets from ETS during purchase", e);
            return false;
        }
        if (eTicketPackage != null) {
            TicketedPurchaseResponseType ticketedResponse = 
                (TicketedPurchaseResponseType) response;
            ticketedResponse.setETickets(eTicketPackage.getETicket());
            ticketedResponse.setCerts(eTicketPackage.getCert());
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Get ETickets for a list of items to be purchased
     */
    protected ETicketPackageType getETickets(Connection conn, TicketedItemInfo[] items)
        throws SQLException, java.rmi.RemoteException
    {
        // Request tickets from ETS
        TicketsRequestType[] tickets = null;
        tickets = new TicketsRequestType[items.length];
        for (int i = 0; i < items.length; i++) {
            byte[] contentMask = ECommerceFunction.getContentMask(items[i].mask);
            tickets[i] = new TicketsRequestType(
                ECommerceFunction.formatTitleId(items[i].titleId),
                items[i].ticketId,
                ECommerceFunction.formatContentId(items[i].eTicketContentId),
                contentMask,
                items[i].limits,                
                items[i].titleCategory,
                null);
        }

        ETicketPackageType eTicketPackage = ECommerceFunction.getETicketPackage(
                service, request, response, tickets, getDeviceCertificate());
        
        //ETicketPackageType eTicketPackage = new ETicketPackageType();
        //eTicketPackage.setETicket(new byte[][] { "fake eticket".getBytes() } );
        
        if (response.getErrorCode() == ECommerceFunction.STATUS_OK) {
            // Update eTicket record
            ETicketRecord.generate(conn,
                    service.getName(),
                    device.getDeviceId(),
                    items);
        }        
        
        return eTicketPackage;
    }
    
    /**
     * Get ETickets for one item
     */
    protected ETicketPackageType getETickets(TicketedItemInfo ticketedItem)
        throws RemoteException, SQLException
    {
        DataSource dataSource = service.getEcsDataSource();
        Connection conn = null;

        log.debug("Getting ticket for " + ticketedItem);
        
        try {
            conn = service.getConnection(dataSource, DataSourceScheduler.DB_PRIORITY_FAST);            
            return getETickets(conn, new TicketedItemInfo[] { ticketedItem } );
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                log.warn("Error closing connection", e);
            }
        }
    }

    protected ETicketRecord lookupETicket(long deviceId, long titleId)
        throws SQLException
    {
        DataSource dataSource = service.getEcsDataSource();
        Connection conn = null;
        try {
            conn = service.getConnection(dataSource, DataSourceScheduler.DB_PRIORITY_FAST);
            return ETicketRecord.lookupRecord(conn, deviceId, titleId); 
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                log.warn("Error closing connection", e);
            }
        }
    }

}

package com.broadon.ecs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.logging.Log;

import com.broadon.db.DBException;
import com.broadon.db.DataSourceScheduler;
import com.broadon.wsapi.ecs.ListETicketsRequestType;
import com.broadon.wsapi.ecs.ListETicketsResponseType;
import com.broadon.wsapi.ecs.ETicketType;
import com.broadon.wsapi.ecs.LimitType;
import com.broadon.wsapi.ecs.PricingCategoryType;

/**
 * The <code>ListETickets</code> class implements
 * the ECS ListETickets function.
 * 
 * Provides a list of etickets for a given device.
 * 
 * @version $Revision: 1.10 $
 */
public class ListETickets extends ECommerceFunction {
    private static final String SELECT_ETICKET_LIST =
        "SELECT tid, content_id, rtype, total_limits, " +
        "mask, license_type, " +
        "SUBSTR(BCCUTIL.Dec2Hex(content_id, 24), 0, 16) hex_title_id, " +
        "BCCUTIL.ConvertTimeStamp(create_date) create_date, " +
        "BCCUTIL.ConvertTimeStamp(revoke_date) revoke_date " +
        "FROM etickets " +
        "WHERE device_id = ? " +
        "ORDER BY tid";

    // Uses apache commons logger (follows axis logging)
    protected static Log log = LogFactory.getLog(ListETickets.class.getName());

    public ListETickets(
            ECommerceServiceImpl service, 
            ListETicketsRequestType request,
            ListETicketsResponseType response)
    {
        super("ListETickets", service, request, response, log);        
    }
    
    /**
     * Returns a list of etickets
     */
    public void processImpl()
        throws SQLException, DBException
    {
        ListETicketsRequestType listETicketsRequest = (ListETicketsRequestType) request;
        ListETicketsResponseType listETicketsResponse = (ListETicketsResponseType) response;

        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = service.getConnection(service.getEcsDataSource(), DataSourceScheduler.DB_PRIORITY_MEDIUM); 
            // Get title list            
            String sql = SELECT_ETICKET_LIST;
            
            stmt = conn.prepareStatement(sql);
            stmt.setLong(1, getDevice().getDeviceId());
            log.info(sql);
            log.info("device_id = " + getDevice().getDeviceId());
            ResultSet rs = stmt.executeQuery();
            List eticketList = new ArrayList();
            long lastTid = 0;
            while (rs.next()) {

                // Don't include duplicates, assumes results sorted by tid
                long tid = rs.getLong("TID");
                if (eticketList.size() == 0 || tid != lastTid) {
                    LimitType[] limits = new LimitType[1];
                    limits[0] = new LimitType(rs.getLong("TOTAL_LIMITS"), rs.getString("RTYPE"));
                    PricingCategoryType licenseType;
                    String ltype = rs.getString("LICENSE_TYPE");
                    if (rs.wasNull()) {
                        licenseType = null;
                    } else {
                        licenseType = getPricingCategoryType(ltype);
                    }
                    ETicketType eticket = new ETicketType(
                            tid,
                            rs.getString("HEX_TITLE_ID"), 
                            licenseType, 
                            limits,
                            rs.getLong("CREATE_DATE"),
                            rs.getLong("REVOKE_DATE"));
                    eticketList.add(eticket);   
                    lastTid = tid;
                    //log.debug("ETicket[tid: " + tid + ", title_id: " + eticket.getTitleId() +
                    //        ", licenseType: " + eticket.getLicenseType() +
                    //        "]");
                }
            }
            
            rs.close();
            rs = null;
            stmt.close();
            stmt = null;
            conn.close();
            conn = null;
            
            if (eticketList.size() > 0) {
                ETicketType[] etickets = new ETicketType[eticketList.size()];
                eticketList.toArray(etickets);
                listETicketsResponse.setTickets(etickets);
            } 
        } finally {
            if (stmt != null) {
                try { stmt.close(); } catch (SQLException e) {}
            }
            if (conn != null) {
                try { conn.close(); } catch (SQLException e) {}
            }
        }
    }
    
    /**
     * List ETickets
     */
    public static ListETicketsResponseType listETickets(
            ECommerceServiceImpl service,
            ListETicketsRequestType ListETicketsRequest) 
    {
        ListETicketsResponseType response = new ListETicketsResponseType();
        ListETickets listETickets = new ListETickets(service, ListETicketsRequest, response);
        listETickets.process();
        return response;
    }
}

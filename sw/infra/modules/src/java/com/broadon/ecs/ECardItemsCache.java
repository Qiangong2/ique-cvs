package com.broadon.ecs;

import com.broadon.util.BackingStore;
import com.broadon.util.BackingStoreException;

/**
 * The <code>ECardItemsCache</code> class provides a cache
 * for items that can be redeemed for a given ECard type.
 *
 * @version $Revision: 1.3 $
 */
public class ECardItemsCache 
    extends PurchaseItemsCache implements BackingStore 
{    

    public ECardItemsCache(ECommerceServiceImpl service)
    {
        super(service);
    }

    public Object retrieve(Object key) throws BackingStoreException
    {
        return retrieve(null, (Integer) key);
    }

}

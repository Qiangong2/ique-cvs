package com.broadon.ecs;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.broadon.db.DataSourceScheduler;
import com.broadon.util.BackingStore;
import com.broadon.util.BackingStoreException;
import com.broadon.util.CacheLFU;

/**
 * The <code>ETicketTitleInfoCache</code> class provides a cache
 * for title related information to generate the eTicket using the contentId or titleId as a key.
 *
 * @version $Revision: 1.2 $
 */
public class ETicketTitleInfoCache 
    extends CacheLFU implements BackingStore 
{    
    private ECommerceServiceImpl service;

    private static final String GET_ETICKET_TITLEINFO_BY_ETKM_CONTENT_ID =
        " SELECT TITLE_ID, TITLE_TYPE, MAX_CHECKOUTS FROM CONTENT_TITLE_CATALOG " +
        "    WHERE ETKM_CONTENT_ID=?";

    private static final String GET_ETICKET_TITLEINFO_BY_TITLE_ID =
        " SELECT ETKM_CONTENT_ID, TITLE_TYPE, MAX_CHECKOUTS FROM CONTENT_TITLE_CATALOG " +
        "    WHERE TITLE_ID=?";

    protected static class ETicketTitleInfo 
    {
        BigDecimal etkmContentId;
        long titleId;
        String titleType;
        
        
        protected ETicketTitleInfo(long titleId, BigDecimal etkmContentId, String titleType)
        {
            this.etkmContentId = etkmContentId; 
            this.titleId = titleId;
            this.titleType = titleType;
        }
    }
    
    protected static class SubscriptionETicketTitleInfo extends ETicketTitleInfo
    {
        Integer maxCheckouts;
        
        protected SubscriptionETicketTitleInfo(long titleId, BigDecimal etkmContentId)
        {
            super(titleId, etkmContentId, TicketedItemInfo.SUBSCRIPTION_TYPE);
        }

        protected SubscriptionETicketTitleInfo(long titleId, BigDecimal etkmContentId, int maxCheckouts)
        {
            this(titleId, etkmContentId);
            this.maxCheckouts = new Integer(maxCheckouts);
        }
    }
    
    /**
     * Look up the title from the CAS content catalog using the eticket content id
     * @param conn DB Connection
     * @param contentId Eticket content id
     * @return
     * @throws SQLException
     */
    private static ETicketTitleInfo getETicketTitle(
            Connection conn,
            BigDecimal contentId)
        throws SQLException
    {
        PreparedStatement stmt = null;
        long titleId = 0;
        String titleType = null;
        
        try {
            stmt = conn.prepareStatement(GET_ETICKET_TITLEINFO_BY_ETKM_CONTENT_ID);
            stmt.setBigDecimal(1, contentId);
        
            ResultSet rs = stmt.executeQuery();
            if (rs != null) {
                if (rs.next()) {
                    int i = 1;
                    titleId = rs.getLong(i++);
                    titleType = rs.getString(i++);
                    if (titleType.equalsIgnoreCase(TicketedItemInfo.SUBSCRIPTION_TYPE)) {
                        int maxCheckouts = rs.getInt(i++);
                        if (rs.wasNull()) {
                            return new SubscriptionETicketTitleInfo(titleId, contentId);                        
                        } else {
                            return new SubscriptionETicketTitleInfo(titleId, contentId, maxCheckouts);
                        }
                    } else {
                        return new ETicketTitleInfo(titleId, contentId, titleType);
                    }
                }
            }
        } finally {
            if (stmt != null) {
                stmt.close();
            } 
        }
        
        return null;
    }

    /**
     * Look up the title from the CAS content catalog using the title id
     * @param conn DB Connection
     * @param titleId
     * @return
     * @throws SQLException
     */
    private static ETicketTitleInfo getETicketTitle(
            Connection conn,
            long titleId)
        throws SQLException
    {
        PreparedStatement stmt = null;
        BigDecimal contentId = null;
        String titleType = null;
        
        try {
            stmt = conn.prepareStatement(GET_ETICKET_TITLEINFO_BY_TITLE_ID);
            stmt.setLong(1, titleId);
        
            ResultSet rs = stmt.executeQuery();
            if (rs != null) {
                if (rs.next()) {
                    int i = 1;
                    contentId = rs.getBigDecimal(i++);
                    titleType = rs.getString(i++);
                    if (titleType.equalsIgnoreCase(TicketedItemInfo.SUBSCRIPTION_TYPE)) {
                        int maxCheckouts = rs.getInt(i++);
                        if (rs.wasNull()) {
                            return new SubscriptionETicketTitleInfo(titleId, contentId);                        
                        } else {
                            return new SubscriptionETicketTitleInfo(titleId, contentId, maxCheckouts);
                        }
                    } else {
                        return new ETicketTitleInfo(titleId, contentId, titleType);
                    }
                }
            }
        } finally {
            if (stmt != null) {
                stmt.close();
            } 
        }
        
        return null;
    }

    public ETicketTitleInfoCache(ECommerceServiceImpl service)
    {
        super(service.getContentCacheTimeoutMsecs());
        this.service = service;
    }

    public Object retrieve(Object key) throws BackingStoreException
    {
        Connection conn = null;
        try {
            if (key instanceof BigDecimal) {
                conn = service.getConnection(service.getCasDataSource(), DataSourceScheduler.DB_PRIORITY_FAST);
                return getETicketTitle(conn, (BigDecimal) key);
            } else if (key instanceof Long) {
                conn = service.getConnection(service.getCasDataSource(), DataSourceScheduler.DB_PRIORITY_FAST);
                return getETicketTitle(conn, ((Long) key).longValue());                
            } else {
                throw new BackingStoreException("Invalid key type");
            }
        } catch (SQLException e) {
            throw BackingStoreException.rewrap(e);            
        } finally {
            if (conn != null) {
                try { conn.close(); } catch (SQLException e) {}
            }
        }
    }

}

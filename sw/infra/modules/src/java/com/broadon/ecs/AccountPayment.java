package com.broadon.ecs;

import java.rmi.RemoteException;

import com.broadon.wsapi.ecs.AccountPaymentType;
import com.broadon.wsapi.ecs.PaymentMethodType;
import com.broadon.wsapi.ecs.AbstractRequestType;
import com.broadon.wsapi.pas.AuthorizeAccountPaymentRequestType;
import com.broadon.wsapi.pas.AuthorizeAccountPaymentResponseType;
import com.broadon.wsapi.pas.BalanceAccountInfoType;
import com.broadon.wsapi.pas.BalanceAccountType;
import com.broadon.wsapi.pas.BalanceType;
import com.broadon.wsapi.pas.CaptureAccountToAccountRequestType;
import com.broadon.wsapi.pas.CaptureAccountToAccountResponseType;
import com.broadon.wsapi.pas.CheckAccountBalanceRequestType;
import com.broadon.wsapi.pas.CheckAccountBalanceResponseType;
import com.broadon.wsapi.pas.PaymentAuthorizationPortType;

/**
 * The <code>AccountPayment</code> class handles a payment
 * using a balance account.
 *
 * @version $Revision: 1.10 $
 */
public class AccountPayment extends Payment {
    BalanceAccountType account;
    BalanceAccountInfoType accountInfo;
    BalanceType balance;
    
    public AccountPayment(
            PaymentAuthorizationPortType pas,
            AccountPaymentType account)
    {
        super(pas, null);
        this.account = new BalanceAccountType(
                account.getPin(),
                account.getAccountNumber());
    }

    public String getPaymentType()
    {
        return PaymentMethodType.ACCOUNT.getValue();
    }
    
    public String getPaymentMethodId()
    {
        return account.getAccountNumber();
    }

    public BalanceType getBalance()
    {
        return balance;
    }
    
        
    public boolean checkBalance(AbstractRequestType request)
        throws RemoteException
    {
        CheckAccountBalanceRequestType checkBalanceRequest = 
            new CheckAccountBalanceRequestType();
        ECommerceFunction.initPasRequest(request, checkBalanceRequest);
        
        checkBalanceRequest.setBalanceAccount(account);

        CheckAccountBalanceResponseType checkBalanceResponse =
            pas.checkAccountBalance(checkBalanceRequest);

        accountInfo = checkBalanceResponse.getBalanceAccountInfo();
        if (accountInfo != null) {
            balance = accountInfo.getBalance();
        }
        errorCode = checkBalanceResponse.getErrorCode();
        errorMessage = checkBalanceResponse.getErrorMessage();
        return (errorCode == ECommerceFunction.STATUS_OK);
    }
    
    public boolean authorizePayment(AbstractRequestType request)
        throws RemoteException
    {
        AuthorizeAccountPaymentRequestType authPaymentRequest = 
            new AuthorizeAccountPaymentRequestType();
        ECommerceFunction.initPasRequest(request, authPaymentRequest);
        
        authPaymentRequest.setAmount(paymentAmount);
        authPaymentRequest.setReferenceID(referenceId);
        authPaymentRequest.setBalanceAccount(account);

        AuthorizeAccountPaymentResponseType authPaymentResponse =
            pas.authorizeAccountPayment(authPaymentRequest);

        authToken = authPaymentResponse.getAuthorizationToken();

        errorCode = authPaymentResponse.getErrorCode();
        errorMessage = authPaymentResponse.getErrorMessage();
        return (errorCode == ECommerceFunction.STATUS_OK);
    }

    public boolean captureToAccount(AbstractRequestType request, AccountPayment toAccount)
        throws RemoteException, InvalidPaymentException
    {
        // Check the deposit amount and payment amount matches
        if (toAccount.depositAmount == null || !toAccount.depositAmount.equals(paymentAmount)) {            
            throw new InvalidPaymentException(
                    "Deposit amount " + toAccount.depositAmount + 
                    " does not match payment amount " + paymentAmount);
        }
        
        CaptureAccountToAccountRequestType captureRequest = 
            new CaptureAccountToAccountRequestType();
        ECommerceFunction.initPasRequest(request, captureRequest);
        
        captureRequest.setAmount(paymentAmount);
        captureRequest.setAuthorizationToken(getAuthorizationToken());
        captureRequest.setToBalanceAccount(toAccount.account);

        CaptureAccountToAccountResponseType captureResponse =
            pas.captureAccountToAccount(captureRequest);

        errorCode = captureResponse.getErrorCode();
        errorMessage = captureResponse.getErrorMessage();
        if (errorCode == ECommerceFunction.STATUS_OK) {
            toAccount.balance = captureResponse.getBalance();
        }
        return (errorCode == ECommerceFunction.STATUS_OK);
    }

    /* No longer used (deprecated in PAS) 
    boolean deposit(AbstractRequestType request, Payment payment)
        throws RemoteException
    {
        DepositPaymentRequestType depositRequest = new DepositPaymentRequestType();
        ECommerceFunction.initPasRequest(request, depositRequest);
        depositRequest.setAuthorizationToken(payment.getAuthorizationToken());
        depositRequest.setToBalanceAccount(this.account);
        depositRequest.setAmount(payment.paymentAmount);
        depositRequest.setDepositAmount(this.paymentAmount);

        DepositPaymentResponseType depositResponse =
            pas.depositPayment(depositRequest);
        
        errorCode = depositResponse.getErrorCode();
        errorMessage = depositResponse.getErrorMessage();
        if (errorCode == ECommerceFunction.STATUS_OK) {
            balance = depositResponse.getBalance();
        }
        return (errorCode == ECommerceFunction.STATUS_OK);
    }    
    
    boolean redeem(AbstractRequestType request, ECardPayment eCard)
        throws RemoteException
    {
        com.broadon.wsapi.pas.RedeemECardRequestType redeemRequest = new com.broadon.wsapi.pas.RedeemECardRequestType();
        ECommerceFunction.initPasRequest(request, redeemRequest);
        redeemRequest.setECardID(eCard.eCard);
        redeemRequest.setToBalanceAccount(this.account);

        com.broadon.wsapi.pas.RedeemECardResponseType redeemResponse =
            pas.redeemECard(redeemRequest);

        errorCode = redeemResponse.getErrorCode();
        errorMessage = redeemResponse.getErrorMessage();
        if (errorCode == ECommerceFunction.STATUS_OK) {
            accountInfo = redeemResponse.getAccountInfo();
            if (accountInfo != null) {
                balance = accountInfo.getBalance();
            }
        }
        return (errorCode == ECommerceFunction.STATUS_OK);
    } 
    */   
}

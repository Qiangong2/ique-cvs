package com.broadon.ecs;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.db.DataSourceScheduler;
import com.broadon.util.BackingStoreException;

import com.broadon.wsapi.ecs.*;
import com.broadon.wsapi.ets.*;

/**
 * The <code>SyncETickets</code> class implements
 * the ECS SyncETickets function.
 * 
 * @version $Revision: 1.46 $
 */
public class SyncETickets extends ECommerceFunction {
    public static byte[] emptyContentMask = new byte[] {};

    private static final String GET_TICKETS =
        "SELECT CONTENT_ID, TID, LICENSE_TYPE, RTYPE, TOTAL_LIMITS, MASK, " +
        "    BCCUTIL.ConvertTimeStamp(LAST_UPDATED) FROM " +
        "    ETICKETS WHERE REVOKE_DATE IS NULL";
    
    private static final String GET_LAST_ETICKET_TIME =
        "SELECT BCCUTIL.ConvertTimeStamp(LAST_ETDATE) FROM ETICKET_SYNC_STAMPS";
        
    // Uses apache commons logger (follows axis logging)
    protected static Log log = 
        LogFactory.getLog(SyncETickets.class.getName());
    
    public SyncETickets(
            ECommerceServiceImpl service, 
            SyncETicketsRequestType request,
            SyncETicketsResponseType response)
    {
        super("SyncETickets", service, request, response, log);        
    }
    
    private static Long getLastETicketTime(Connection conn, long deviceid)
        throws SQLException
    {
        Long syncTime = null;
        String sql = GET_LAST_ETICKET_TIME + " WHERE DEVICE_ID = " + deviceid;
        Statement stmt = null;
        try {
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            if (rs.next()) {
                syncTime = new Long(rs.getLong(1));
            }
            rs.close();
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
        return syncTime;
    }
        
    private static String getTicketsSql(long deviceId, Long lastUpdated)
    {
        StringBuffer strBuffer = new StringBuffer(GET_TICKETS);
        strBuffer.append(" AND DEVICE_ID=");
        strBuffer.append(deviceId);
        if (lastUpdated != null) {
            strBuffer.append(" AND BCCUTIL.ConvertTimeStamp(LAST_UPDATED) > ");
            strBuffer.append(lastUpdated);
        }
        return strBuffer.toString();
    }
    
    private static TicketsRequestType[] getTicketRequests(
            ECommerceServiceImpl service, Connection conn, long deviceId, Long lastUpdated)
        throws SQLException, BackingStoreException
    {
        List ticketRequestList = new ArrayList();

        // Get tickets for the device
        String sql = getTicketsSql(deviceId, lastUpdated);
        
        Statement stmt = null;
        try {
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                int i = 1;
                BigDecimal contentId = rs.getBigDecimal(i++);
                long ticketId = rs.getLong(i++);
                String licenseType = rs.getString(i++);
                String rightsType = rs.getString(i++);
                long limit = rs.getLong(i++);
                String mask = rs.getString(i++);
                long lastUpdatedTime = rs.getLong(i++);

                ETicketTitleInfoCache.ETicketTitleInfo title; 
                TicketAttributeType[] limits = null;

                title = service.getTitleCatalog().getETicketTitleInfo(contentId);
                if (ETicketRecord.LIMIT_TYPE_SR.equals(rightsType)) {
                    ETicketTitleInfoCache.SubscriptionETicketTitleInfo stitle  
                        = (ETicketTitleInfoCache.SubscriptionETicketTitleInfo) title;
                    if (stitle != null) {
                        if (stitle.maxCheckouts != null) {
                            limits = new TicketAttributeType[2];
                            limits[1] = new TicketAttributeType(stitle.maxCheckouts.intValue(), ETicketRecord.LIMIT_TYPE_SR);                
                        } else {
                            limits = new TicketAttributeType[1];
                        }
                        limits[0] = new TicketAttributeType(limit, ETicketRecord.LIMIT_TYPE_DR);  
                    }
                } else {
                    if (title != null && !ETicketRecord.LIMIT_TYPE_PR.equals(rightsType)) {
                        limits = new TicketAttributeType[1]; 
                        limits[0] = new TicketAttributeType(limit, rightsType);                
                    } 
                }

                if (title != null) {
                    byte[] contentMask = ECommerceFunction.getContentMask(mask);
                    
                    TicketsRequestType ticketRequest = new TicketsRequestType(
                        ECommerceFunction.formatTitleId(title.titleId),
                        ticketId,
                        ECommerceFunction.formatContentId(contentId),
                        contentMask,
                        limits,
                        title.titleType,
                        null);
                    ticketRequestList.add(ticketRequest);
                }
            }
            rs.close();
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }

        if (ticketRequestList.size() > 0) {
            TicketsRequestType[] tickets = new TicketsRequestType[ticketRequestList.size()];
            tickets = (TicketsRequestType[]) ticketRequestList.toArray(tickets);
            return tickets;
        } else {
            return null;
        }
    }

    public void processImpl()
        throws BackingStoreException, SQLException
    {
        SyncETicketsRequestType syncETicketsRequest = (SyncETicketsRequestType) request;
        SyncETicketsResponseType syncETicketsResponse = (SyncETicketsResponseType) response;

        Connection conn = null;
        DataSource dataSource = service.getEcsDataSource();

        try {
            long deviceId = getDevice().getDeviceId();
            Long lastSyncTime = syncETicketsRequest.getLastSyncTime();

            conn = service.getConnection(dataSource, DataSourceScheduler.DB_PRIORITY_MEDIUM);

            // Check to see if we need to sync tickets
            Long syncTime = getLastETicketTime(conn, deviceId);
            syncETicketsResponse.setSyncTime(syncTime);
            
            if ((syncTime != null) && ((lastSyncTime == null) || (lastSyncTime.longValue() < syncTime.longValue()))) {
                // Need to sync tickets
                TicketsRequestType[] ticketRequests = getTicketRequests(
                    service,
                    conn, 
                    deviceId,
                    null);

                ETicketPackageType eTicketPackage = getETicketPackage(
                    service, syncETicketsRequest, syncETicketsResponse, 
                    ticketRequests, syncETicketsRequest.getDeviceCert());
                
                if (eTicketPackage != null) {
                    syncETicketsResponse.setCerts(eTicketPackage.getCert());
                    syncETicketsResponse.setETickets(eTicketPackage.getETicket());
                    log.debug("SyncETickets got " + syncETicketsResponse.getCerts().length + " certs, " + 
                        syncETicketsResponse.getETickets().length + " tickets with sync time " + 
                        syncETicketsResponse.getSyncTime() + " for device " + 
                        syncETicketsRequest.getDeviceId() + " last sync " + 
                        syncETicketsRequest.getLastSyncTime());
                }
            } else {
                // No sync needed
                setErrorCode(StatusCode.ECS_ETICKETS_IN_SYNC, null);
                log.debug("SyncETickets - etickets already in sync with sync time " + 
                    syncETicketsResponse.getSyncTime() + " for device " + 
                    syncETicketsRequest.getDeviceId() + " last sync " + 
                    syncETicketsRequest.getLastSyncTime());
            }
        } catch (RemoteException e) {
            setErrorCode(StatusCode.ECS_ETS_ERROR, e.getMessage());
            log.error("SyncETicket error getting tickets from ETS", e);
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
               log.warn("Error closing connection", e);
            }
        }
    }

    public static SyncETicketsResponseType syncETickets(
            ECommerceServiceImpl service,
            SyncETicketsRequestType syncETicketsRequest) 
    {
        SyncETicketsResponseType response = new SyncETicketsResponseType();
        SyncETickets syncETickets = new SyncETickets(service, syncETicketsRequest, response);
        syncETickets.process();
        return response;
    }
}

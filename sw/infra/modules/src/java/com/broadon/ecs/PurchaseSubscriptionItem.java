package com.broadon.ecs;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;

import javax.sql.DataSource;

import com.broadon.db.DataSourceScheduler;
import com.broadon.util.BackingStoreException;
import com.broadon.wsapi.ecs.SubscribeRequestType;
import com.broadon.wsapi.ecs.SubscriptionPricingType;
import com.broadon.wsapi.ecs.TimeDurationType;
import com.broadon.wsapi.ecs.TimeUnitType;
import com.broadon.wsapi.ets.TicketAttributeType;

/**
 * The <code>PurchaseSubscriptionItem</code> class handles the
 * purchase of a subscription.
 *
 * @version $Revision: 1.21 $
 */
public class PurchaseSubscriptionItem extends PurchaseTicketedItem {
    
    protected PurchaseSubscriptionItem(ECommerceFunction func,
            byte[] deviceCert)
        throws BackingStoreException
    {
        super(func, Invoice.TRANS_PURCHASE_SUBSCRIPTION, deviceCert);
    }

    /**
     * Get information on the item to purchase for the
     * requested subscription
     */
    protected TicketedItemInfo getTicketedItem()
        throws InvalidSubscriptionException, BackingStoreException
    {
        SubscribeRequestType subscribeRequest = (SubscribeRequestType) request;
        int itemId = subscribeRequest.getItemId();
        TimeDurationType requestedLength = subscribeRequest.getSubscriptionLength();
        int requestedChannelId = 0;
        PurchaseItemInfo item = null;
        
        if (requestedLength.getLength() <= 0) {
            throw new InvalidSubscriptionException(
                    "Invalid subscription length of '" + requestedLength.getLength() + 
                    " " + requestedLength.getUnit() + 
                    "', subscription length must be a positive integer");
        }

        try {
            requestedChannelId = ECommerceFunction.parseChannelId(
                    subscribeRequest.getChannelId());
        } catch (Throwable e){
            setErrorCode(
                    StatusCode.ECS_INVALID_CHANNEL_ID, 
                    "Error parsing channel " + subscribeRequest.getChannelId());
            return null;
        }

        /* Find corresponding item for this country */
        List items = (List) service.getPurchaseItemsCache().get(new Integer(itemId));        
        boolean found = false;
        if (items != null) {
            for (int i = 0; i < items.size(); i++) {
                item = (PurchaseItemInfo) items.get(i);

                if (!checkDeviceType(itemId, item.deviceType)) {
                    return null;
                }

                if (checkCountryCode(itemId, item.country)) {
                    found = true;
                    break;
                }
            }
        }

        if (!found) {
            return null;
        }
            
        // Validate item
        if (!(item instanceof TicketedItemInfo)) {
            setErrorCode(
                    StatusCode.ECS_BAD_TITLE_TYPE,
                    "Purchase item " + itemId + " is not for a subscription title");
            return null;
        }

        TicketedItemInfo ticketedItem = (TicketedItemInfo) item;
        
        if (!checkTitleType(ticketedItem.itemId, ticketedItem.titleCategory, TicketedItemInfo.SUBSCRIPTION_TYPE)) {
            return null;
        }

        // Fill in limits for ticketedItem
        SubscriptionPricingType subsPricing = service.getSubscriptionPricingsCache().get(
                ticketedItem.country, ticketedItem.itemId);
        if (subsPricing.getMaxCheckouts() != null) {
            ticketedItem.limits = new TicketAttributeType[2];
            ticketedItem.limits[1] = new TicketAttributeType(
                    subsPricing.getMaxCheckouts().intValue(), ETicketRecord.LIMIT_TYPE_SR);
        } else {
            ticketedItem.limits = new TicketAttributeType[1];            
        }
        ticketedItem.limits[0] = new TicketAttributeType(0, ETicketRecord.LIMIT_TYPE_DR);
        
        // TODO: Verify titleId is a valid channel title id (lower 32-bit is not set)
            
        int channelId = ECommerceFunction.getChannelId(ticketedItem.titleId);
        if (channelId != requestedChannelId) {
            setErrorCode(
                   StatusCode.ECS_INVALID_CHANNEL_ID, 
                   "Subscription item " + itemId + " is for channel " +
                   ECommerceFunction.formatChannelId(channelId) + 
                   ", not requested channel " + 
                   subscribeRequest.getChannelId());
            return null;
        }

        int qty;
        if (requestedLength.getUnit().equals(subsPricing.getSubscriptionLength().getUnit())
                && (requestedLength.getLength() % subsPricing.getSubscriptionLength().getLength() == 0)) {
            qty = requestedLength.getLength()/subsPricing.getSubscriptionLength().getLength();            
        } else {
            throw new InvalidSubscriptionException( 
                    "Cannot fulfill subscription for '" + requestedLength.getLength() + 
                    " " + requestedLength.getUnit() + "', item " + itemId +
                    " has a subscription length of '" + subsPricing.getSubscriptionLength().getLength() +
                    " " + subsPricing.getSubscriptionLength().getUnit() + "'");
        }

        // Copy ticketedItem since limits will be updated
        ticketedItem = new TicketedItemInfo(ticketedItem);
        ticketedItem.setQuantity(qty);
        return ticketedItem;
    }

    /**
     * Figure out the time in seconds since the Epoch
     * for when the subscription should expire.
     * @param nMonths
     * @param nDays
     * @return
     */
    protected static long calcExpiration(long startMsecs, int nMonths, int nDays)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(startMsecs);
        cal.add(Calendar.MONTH, nMonths);
        cal.add(Calendar.DATE, nDays);
        return cal.getTimeInMillis();
    }
        
    protected static long calcExpiration(long startMsecs, int length, String unit)
    {
        if (TimeUnitType._day.equals(unit)) {
            return calcExpiration(startMsecs, 0, length);
        } else if (TimeUnitType._month.equals(unit)){
            return calcExpiration(startMsecs, length, 0);
        } else {
            throw new RuntimeException("Invalid subscription length '" +
                    length + " '" + unit + "'"); 
        }
    }

    protected long getExpiration(long startMsecs)
    {
        SubscribeRequestType subscribeRequest = (SubscribeRequestType) request;
        return calcExpiration(startMsecs, subscribeRequest.getSubscriptionLength().getLength(),
                subscribeRequest.getSubscriptionLength().getUnit().toString());
    }

    /**
     * Calculate new expiration time
     * @throws SQLException
     */
    protected void updateLimits(TicketedItemInfo ticketedItem) 
        throws SQLException
    {
        long currentTime;
        long prevExpiration;
        long ticketId;
       
        DataSource dataSource = service.getEcsDataSource();
        Connection conn = null;
        try {
            conn = service.getConnection(dataSource, DataSourceScheduler.DB_PRIORITY_FAST);
            currentTime = ECommerceFunction.getCurrentDbTimestamp(conn);
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                log.warn("Error closing connection", e);
            }
        }

        ETicketRecord oldTicket = ticketedItem.record;
        if (oldTicket == null) {
            ticketId = service.newTicketId(ticketedItem.titleId);
            prevExpiration = currentTime;
        } else {
            // Convert from seconds to milliseconds
            prevExpiration = oldTicket.limits;
            prevExpiration = prevExpiration*1000;
            // If expiration already expired, start from now
            if (prevExpiration < currentTime) {
                prevExpiration = currentTime;
            }
            ticketId = oldTicket.tid;
        }
        
        // Calculate new expiration
        long expiration = getExpiration(prevExpiration);
        
        // Convert from milliseconds back to seconds for limits
        ticketedItem.total_limits = expiration/(1000);
        ticketedItem.limits[0].setLimits(ticketedItem.total_limits);
        ticketedItem.ticketId = ticketId;
    }
    
}

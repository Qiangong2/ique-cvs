package com.broadon.ecs;

import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.logging.Log;

import com.broadon.util.BackingStoreException;
import com.broadon.wsapi.ecs.GetSystemUpdateRequestType;
import com.broadon.wsapi.ecs.GetSystemUpdateResponseType;
import com.broadon.wsapi.ecs.TitleVersionType;

/**
 * The <code>GetSystemUpdate</code> class implements
 * the ECS GetSystemUpdate function.
 * 
 * Returns SystemUpdatedata (such as the secure kernel version, CRL version,
 * list of contents to preload, etc) for a given country and device type.
 *
 * @version $Revision: 1.5 $
 */
public class GetSystemUpdate extends ECommerceFunction {
    // Uses apache commons logger (follows axis logging)
    protected static Log log = LogFactory.getLog(GetSystemUpdate.class.getName());

    public GetSystemUpdate(
            ECommerceServiceImpl service, 
            GetSystemUpdateRequestType request,
            GetSystemUpdateResponseType response)
    {
        super("GetSystemUpdate", service, request, response, log);        
    }
    
    public void processImpl()
        throws BackingStoreException
    {
        GetSystemUpdateRequestType getSystemUpdateRequest = (GetSystemUpdateRequestType) request;
        GetSystemUpdateResponseType getSystemUpdateResponse = (GetSystemUpdateResponseType) response;

        int deviceType = getDevice().getDeviceType();
        
        // read cached response
        TitleVersionType[] systemTitles = service.getTitleCatalog().getSystemTitles(getCountry(), deviceType);
        getSystemUpdateResponse.setContentPrefixURL(service.getContentPrefixUrl());
        getSystemUpdateResponse.setUncachedContentPrefixURL(service.getUncachedContentPrefixUrl());
        getSystemUpdateResponse.setTitleVersion(systemTitles);
        getSystemUpdateResponse.setUploadAuditData(1);
    }
    
    /**
     * Returns the SystemUpdate information for the device
     * @param service
     * @param getSystemUpdateRequest
     * @return
     */
    public static GetSystemUpdateResponseType getSystemUpdate(
            ECommerceServiceImpl service,
            GetSystemUpdateRequestType getSystemUpdateRequest) 
    {
        GetSystemUpdateResponseType response = new GetSystemUpdateResponseType();
        GetSystemUpdate getSystemUpdate = new GetSystemUpdate(service, getSystemUpdateRequest, response);
        getSystemUpdate.process();
        return response;
    }
}

package com.broadon.ecs;

import com.broadon.exception.BroadOnException;

/**
 * The <code>InvalidPriException</code> class is thrown when
 * the price is invalid.  A price is composed of a currency (string)
 * and a amount (decimal represented as a string)
 * @version	$Revision: 1.1 $
 */
public class InvalidPaymentException
    extends BroadOnException
{
    /**
     * Constructs a InvalidPaymentException instance.
     */
    public InvalidPaymentException()
    {
	super();
    }

    /**
     * Constructs a InvalidPaymentException instance.
     *
     * @param	message			the exception message
     */
    public InvalidPaymentException(String message)
    {
	super(message);
    }

    /**
     * Constructs a InvalidPaymentException instance.
     *
     * @param	message			the exception message
     * @param	throwable		the nested exception
     */
    public InvalidPaymentException(String message, Throwable throwable)
    {
	super(message, throwable);
    }
}

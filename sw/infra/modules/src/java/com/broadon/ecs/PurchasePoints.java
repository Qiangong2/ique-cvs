package com.broadon.ecs;

import java.sql.SQLException;

import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.logging.Log;

import com.broadon.exception.BroadOnException;
import com.broadon.util.BackingStoreException;
import com.broadon.wsapi.ecs.*;

/**
 * The <code>PurchasePoints</code> class implements the ECS PurchasePoints function.
 * 
 * @version $Revision: 1.3 $
 */
public class PurchasePoints extends ECommerceFunction {

    // Uses apache commons logger (follows axis logging)
    protected static Log log = 
        LogFactory.getLog(PurchasePoints.class.getName());

    public PurchasePoints(
            ECommerceServiceImpl service, 
            PurchasePointsRequestType request,
            PurchasePointsResponseType response)
    {
        super("PurchasePoints", service, request, response, log);        
    }
    
    public void processImpl()
        throws BackingStoreException, SQLException, BroadOnException
    {
        PurchaseItem purchaseItem = new PurchasePointsItem(this); 
        purchaseItem.doPurchase();
    }

    public static PurchasePointsResponseType purchasePoints(
            ECommerceServiceImpl service,
            PurchasePointsRequestType purchasePointsRequest)
    {
        PurchasePointsResponseType response = new PurchasePointsResponseType();
        PurchasePoints purchasePoints = new PurchasePoints(service, purchasePointsRequest, response);
        purchasePoints.process();
        return response;
    }
}

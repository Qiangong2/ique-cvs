package com.broadon.ecs;

import java.rmi.RemoteException;

import com.broadon.wsapi.ecs.AbstractRequestType;
import com.broadon.wsapi.ecs.CreditCardPaymentType;
import com.broadon.wsapi.ecs.PaymentMethodType;
import com.broadon.wsapi.pas.AuthorizeCreditCardPaymentRequestType;
import com.broadon.wsapi.pas.AuthorizeCreditCardPaymentResponseType;
import com.broadon.wsapi.pas.BalanceType;
import com.broadon.wsapi.pas.CaptureCreditCardToAccountRequestType;
import com.broadon.wsapi.pas.CaptureCreditCardToAccountResponseType;
import com.broadon.wsapi.pas.PaymentAuthorizationPortType;

public class CreditCardPayment extends Payment {

    String creditCardNumber;
    String noaCreditCardReq;
        
    public CreditCardPayment(
            PaymentAuthorizationPortType pas,
            CreditCardPaymentType payment,
            String noaCreditCardReq)
    {
        super(pas, null);
        this.creditCardNumber = payment.getCreditCardNumber();
        this.noaCreditCardReq = noaCreditCardReq;
    }

    public String getPaymentType()
    {
        return PaymentMethodType.CCARD.getValue();
    }

    public String getPaymentMethodId()
    {
        return creditCardNumber;
    }

    public BalanceType getBalance()
    {
        return null;
    }
    
    public boolean checkBalance(AbstractRequestType request) throws RemoteException 
    {
        // TODO: Implement
        return false;
    }

    public boolean authorizePayment(AbstractRequestType request)
            throws RemoteException
    {
        AuthorizeCreditCardPaymentRequestType authPaymentRequest = 
            new AuthorizeCreditCardPaymentRequestType();
        ECommerceFunction.initPasRequest(request, authPaymentRequest);
        
        authPaymentRequest.setAmount(paymentAmount);
        authPaymentRequest.setReferenceID(referenceId);
        authPaymentRequest.setCreditCardNumber(creditCardNumber);
        authPaymentRequest.setNOACreditCardReq(noaCreditCardReq);

        AuthorizeCreditCardPaymentResponseType authPaymentResponse =
            pas.authorizeCreditCardPayment(authPaymentRequest);

        authToken = authPaymentResponse.getAuthorizationToken(); 

        errorCode = authPaymentResponse.getErrorCode();
        errorMessage = authPaymentResponse.getErrorMessage();
        return (authPaymentResponse.getErrorCode() == ECommerceFunction.STATUS_OK);    
    }

    public boolean captureToAccount(AbstractRequestType request, AccountPayment toAccount)
        throws RemoteException
    {
        CaptureCreditCardToAccountRequestType captureRequest = 
            new CaptureCreditCardToAccountRequestType();
        ECommerceFunction.initPasRequest(request, captureRequest);
        
        captureRequest.setAmount(paymentAmount);
        captureRequest.setAuthorizationToken(getAuthorizationToken());
        captureRequest.setToBalanceAccount(toAccount.account);
        captureRequest.setDepositAmount(toAccount.depositAmount);

        CaptureCreditCardToAccountResponseType captureResponse =
            pas.captureCreditCardToAccount(captureRequest);

        errorCode = captureResponse.getErrorCode();
        errorMessage = captureResponse.getErrorMessage();
        if (errorCode == ECommerceFunction.STATUS_OK) {
            toAccount.balance = captureResponse.getBalance();
        }
        return (errorCode == ECommerceFunction.STATUS_OK);
    }

}

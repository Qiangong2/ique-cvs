package com.broadon.ecs;

import java.rmi.RemoteException;
import java.sql.SQLException;

import com.broadon.util.BackingStoreException;
import com.broadon.wsapi.ecs.RedeemECardRequestType;
import com.broadon.wsapi.ecs.RedeemECardResponseType;
import com.broadon.wsapi.pas.BalanceType;
import com.broadon.wsapi.pas.MoneyType;

/**
 * The <code>RedeemTitleEcard</code> class redeems
 * a ECard for a Title.
 *
 * @version $Revision: 1.5 $
 */
public class RedeemPointsECard extends PurchaseItem {
    
    ECardPayment eCardPayment;
    PurchaseItemInfo item;
    
    public RedeemPointsECard(ECommerceFunction func,
            ECardPayment payment,
            PurchaseItemInfo item)
        throws BackingStoreException
    {
        super(func, Invoice.TRANS_REDEEM_POINTS);
        this.eCardPayment = payment;
        this.item = item;
        paymentRequired = true;
        account = ((RedeemECardRequestType) request).getAccount();
        //authorizationNeeded = false;
        captureNeeded = false;
        accountRequired = true;
    }

    /**
     * Get the payment method
     * @return
     */
    protected Payment getPayment(MoneyType paymentAmount)
    {
        eCardPayment.setPaymentAmount(paymentAmount);
        return eCardPayment;
    }
                        
     /**
     * Get information on the item to purchase for the
     * requested ecard
     */
    protected PurchaseItemInfo getPurchaseItem()
    {
        return item;
    }

    /**
     * Use PAS to redeem eCard
     * @param request
     * @param response
     * @return
     */
    protected boolean deliverItem(Payment payment, PurchaseItemInfo purchaseItem)
        throws SQLException
    {
        AccountPayment acct = new AccountPayment(
            service.getPasSOAP(),
            account);
        acct.setDepositPoints(purchaseItem.points.longValue());
        
        try {
            //boolean ok = acct.redeem(request, eCardPayment);
            boolean ok = payment.captureToAccount(request, acct);
            if (ok) {
                BalanceType balance = acct.getBalance();
                if (balance != null) { 
                    RedeemECardResponseType redeemResponse = (RedeemECardResponseType) response;
                    Long totalPoints = Long.valueOf(balance.getAvailable().getAmount());
                    redeemResponse.setTotalPoints(totalPoints);
                    redeemResponse.setRedeemedPoints(purchaseItem.points);
                }
            } else {
                response.setErrorCode(acct.errorCode);
                response.setErrorMessage(acct.errorMessage);
            }
            return ok;
        } catch (InvalidPaymentException e) {
            setErrorCode(StatusCode.ECS_INVALID_PAYMENT, e.getMessage());
            log.debug("Error redeeming points with eCard", e);            
            return false;
        } catch (RemoteException e) {
            setErrorCode(StatusCode.ECS_PAS_ERROR, e.getMessage());
            log.error("Error redeeming points with eCard using PAS", e);            
            return false;
        }
    }    
}

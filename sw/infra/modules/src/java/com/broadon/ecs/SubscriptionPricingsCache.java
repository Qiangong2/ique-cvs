package com.broadon.ecs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.logging.Log;

import com.broadon.db.DataSourceScheduler;
import com.broadon.util.BackingStore;
import com.broadon.util.BackingStoreException;
import com.broadon.util.CacheExp;
import com.broadon.util.Pair;
import com.broadon.wsapi.ecs.PriceType;
import com.broadon.wsapi.ecs.SubscriptionPricingType;
import com.broadon.wsapi.ecs.TimeDurationType;
import com.broadon.wsapi.ecs.TimeUnitType;

public class SubscriptionPricingsCache extends CacheExp implements BackingStore
{
    // NOTE:  When we no longer need to check for purchase_end_date, then we should
    // switch to use a less expensive Cache implmentation (e.g., CacheLFU).
    //
    private static final String SELECT_SUBSCRIPTIONS =
        "SELECT BCCUTIL.ConvertTimeStamp(PURCHASE_END_DATE), subscription_item_id, " +
        "       subscription_name, subscription_desc, subscription_length, subscription_interval, " +
        "       title_id, max_checkouts, item_price, item_currency " +
        "FROM SUBSCRIPTION_PRICING_CATALOG " +
        "WHERE " + ECommerceFunction.FILTER_BY_PURCHASE_DATE;

    private ECommerceServiceImpl service;
    
    //  Uses apache commons logger (follows axis logging)
    private static Log log = LogFactory.getLog(ListTitles.class.getName());


    public SubscriptionPricingsCache(ECommerceServiceImpl service) {
        super(100, service.getContentCacheTimeoutMsecs());
        this.service = service; 
    }

    public SubscriptionPricingType get(String countryCode, int itemId)    
        throws BackingStoreException
    {
        SubscriptionPricingType[] pricings = (SubscriptionPricingType[]) get(countryCode);
        if (pricings != null) {
            for (int i = 0; i < pricings.length; i++) {
                if (pricings[i].getItemId() == itemId) {
                    return pricings[i];
                }
            }
        }
        return null;
    }
    
    public Object retrieve(Object countryCodeKey) throws BackingStoreException
    {
        String countryCode = (String) countryCodeKey;
        
        Connection conn = null;
        PreparedStatement stmt = null;

        String filterString = " AND country = '" + countryCode + "'";
        
        try {
            // Get subscription list            
            String sql = SELECT_SUBSCRIPTIONS + filterString;

            conn = service.getConnection(service.getCasDataSource(), DataSourceScheduler.DB_PRIORITY_FAST);
            stmt = conn.prepareStatement(sql);
            
            ResultSet rs = stmt.executeQuery();
           
            List subscriptionList = new ArrayList();
            long expDate = Long.MAX_VALUE;
            while (rs.next()) {
                int i=1;
                long endDate = rs.getLong(i++);
                if (! rs.wasNull() && endDate < expDate) {
                    expDate = endDate;
                }
                
                SubscriptionPricingType subscription = new SubscriptionPricingType();
                subscription.setItemId(rs.getInt(i++));
                subscription.setChannelName(rs.getString(i++));
                subscription.setChannelDescription(rs.getString(i++));
                TimeDurationType subscriptionLength = null;
                int length = rs.getInt(i++);
                if (!rs.wasNull()) {
                    TimeUnitType unit = TimeUnitType.fromValue(
                            rs.getString(i++));
                    subscriptionLength = new TimeDurationType(
                            length, unit);
                }
                subscription.setSubscriptionLength(subscriptionLength);
                long titleId = rs.getLong(i++);
                subscription.setChannelId(
                        ECommerceFunction.formatChannelId(
                                ECommerceFunction.getChannelId(titleId)));
                int maxCheckouts = rs.getInt(i++);
                if (!rs.wasNull()) {
                    subscription.setMaxCheckouts(new Integer(maxCheckouts));
                }
                subscription.setPrice(new PriceType(
                        rs.getBigDecimal(i++).toString(), rs.getString(i++)));
                subscriptionList.add(subscription);
            }
            rs.close();
            
            // release critical resource ASAP
            stmt.close();
            stmt = null;
            conn.close();
            conn = null;
            
            if (subscriptionList.size() > 0) {
                SubscriptionPricingType[] subscriptions = 
                    new SubscriptionPricingType[subscriptionList.size()];
                subscriptions = (SubscriptionPricingType[]) 
                    subscriptionList.toArray(subscriptions);
                Long expirationDate = (expDate == Long.MAX_VALUE) ? null : new Long(expDate);
                return new Pair(expirationDate, subscriptions);
            } else {
                throw new BackingStoreException("Cannot get subscription pricing info for country " + 
                        countryCode);
            }
        } catch (SQLException e) {               
            log.error("ListSubscriptionPricings error", e);
            throw BackingStoreException.rewrap(e);
        } finally {
            if (stmt != null) {
                try { stmt.close(); } catch (SQLException e) {}
            }
            if (conn != null) {
                try { conn.close(); } catch (SQLException e) {}
            }
        }            
    }

}

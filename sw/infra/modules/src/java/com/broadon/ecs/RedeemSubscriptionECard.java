package com.broadon.ecs;

import java.rmi.RemoteException;
import java.sql.SQLException;

import com.broadon.util.BackingStoreException;
import com.broadon.wsapi.ecs.RedeemECardRequestType;
import com.broadon.wsapi.ecs.SubscriptionPricingType;
import com.broadon.wsapi.ets.TicketAttributeType;
import com.broadon.wsapi.pas.MoneyType;

/**
 * The <code>RedeemSubscriptionEcard</code> class redeems
 * a ECard for Subscription.
 *
 * @version $Revision: 1.19 $
 */
public class RedeemSubscriptionECard extends PurchaseSubscriptionItem {
    
    ECardPayment eCardPayment;
    TicketedItemInfo ticketedItem;
    SubscriptionPricingType subsPricing;    
    
    public RedeemSubscriptionECard(ECommerceFunction func,
            ECardPayment eCardPayment,
            TicketedItemInfo item)
        throws BackingStoreException
    {
        super(func, ((RedeemECardRequestType) func.request).getDeviceCert());
        this.eCardPayment = eCardPayment;
        this.ticketedItem = item;
        this.transactionType = Invoice.TRANS_REDEEM_SUBSCRIPTION;
        paymentRequired = true;
        account = ((RedeemECardRequestType) request).getAccount();
    }

    /**
     * Get the payment method
     * @return
     */
    protected Payment getPayment(MoneyType paymentAmount)
        throws SQLException, RemoteException, InvalidPaymentException
    {
        /* Only accept ECard */
        if (eCardPayment == null) {
            RedeemECardRequestType redeemRequest = (RedeemECardRequestType) request;
            ECardPayment payment = new ECardPayment(
                    request,
                    service.getPasSOAP(), 
                    redeemRequest.getECard());
            payment.setPaymentAmount(paymentAmount);
            return payment;
        } else {
            eCardPayment.setPaymentAmount(paymentAmount);
            return eCardPayment;
        }
    }
                        
    protected long getExpiration(long startMsecs)
    {
        return calcExpiration(startMsecs, 
                subsPricing.getSubscriptionLength().getLength(),
                subsPricing.getSubscriptionLength().getUnit().toString());
    }

     /**
     * Get information on the item to purchase for the
     * requested ecard
     */
    protected TicketedItemInfo getTicketedItem()
        throws InvalidSubscriptionException, BackingStoreException
    {
        if (ticketedItem.limits == null) {
            // Fill in limits for ticketedItem
            subsPricing = service.getSubscriptionPricingsCache().get(
                ticketedItem.country, ticketedItem.itemId);
            if (subsPricing.getMaxCheckouts() != null) {
                ticketedItem.limits = new TicketAttributeType[2];
                ticketedItem.limits[1] = new TicketAttributeType(
                    subsPricing.getMaxCheckouts().intValue(), ETicketRecord.LIMIT_TYPE_SR);
            } else {
                ticketedItem.limits = new TicketAttributeType[1];            
            }
            ticketedItem.limits[0] = new TicketAttributeType(0, ETicketRecord.LIMIT_TYPE_DR);
        }
        return ticketedItem;
    }
}

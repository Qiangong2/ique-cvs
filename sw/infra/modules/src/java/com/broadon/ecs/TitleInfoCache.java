package com.broadon.ecs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.broadon.db.DataSourceScheduler;
import com.broadon.util.BackingStore;
import com.broadon.util.BackingStoreException;
import com.broadon.util.CacheLFU;

import com.broadon.wsapi.ecs.ContentInfoType;
import com.broadon.wsapi.ecs.TitleInfoType;
import com.broadon.wsapi.ecs.TitleKindType;

/**
 * The <code>TitleInfoCache</code> class provides a cache
 * for the TitleInfoType using the titleId as a key.
 *
 * @version $Revision: 1.11 $
 */
public class TitleInfoCache 
    extends CacheLFU implements BackingStore 
{    
    private ECommerceServiceImpl service;

    private static final String SELECT_TITLE_INFO = 
        "SELECT TITLE, TITLE_TYPE, DESCRIPTION, TITLE_SIZE, APPROX_SIZE, " +
        "       PLATFORM, CATEGORY, TITLE_VERSION " +
        "FROM CONTENT_TITLE_COUNTRY_RELEASES " +
        "WHERE TITLE_ID = ? AND COUNTRY_ID = ? AND LANGUAGE_CODE = ?";

    public TitleInfoCache(ECommerceServiceImpl service)
    {
        super(service.getContentCacheTimeoutMsecs());
        this.service = service;
    }

    public Object retrieve(Object titleIdKey) throws BackingStoreException
    {
        TitleIdKey titleKey = (TitleIdKey) titleIdKey;
        
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = service.getConnection(service.getCasDataSource(), DataSourceScheduler.DB_PRIORITY_FAST);
            ps = conn.prepareStatement(SELECT_TITLE_INFO);
            ps.setLong(1, titleKey.titleId);
            ps.setInt(2, titleKey.country.countryId);
            ps.setString(3, titleKey.language);
            rs = ps.executeQuery();
            if (rs.next()) {
                TitleInfoType titleInfo = new TitleInfoType();
                int i = 1;
                String titleName = rs.getString(i++);
                String titleKindStr = rs.getString(i++);
                String titleDesc = rs.getString(i++);
                long titleSize = rs.getLong(i++);
                long approxSize = rs.getLong(i++);
                String platform = rs.getString(i++);
                String category = rs.getString(i++);
                int version = rs.getInt(i++);
                
                // release critical resources ASAP
                rs.close();
                rs = null;
                ps.close();
                ps = null;
                conn.close();
                conn = null;
                
                TitleKindType titleKind = ECommerceFunction.getTitleKindType(titleKindStr);         
                titleInfo.setTitleId(ECommerceFunction.formatTitleId(titleKey.titleId));
                titleInfo.setTitleName(titleName);
                titleInfo.setTitleKind(titleKind);
                titleInfo.setTitleDescription(titleDesc);
                titleInfo.setTitleSize(titleSize);
                titleInfo.setFsSize(approxSize);
                titleInfo.setPlatform(platform);
                titleInfo.setCategory(category);
                titleInfo.setVersion(version);
                
                // Get Content List
                // Set contents to null (deprecated)
                ContentInfoType[] contents = null;
                    //service.getTitleCatalog().getContents(titleKey.titleId);
                titleInfo.setContents(contents);
                return titleInfo;
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw BackingStoreException.rewrap(e);            
        } finally {
            if (rs != null) {
                try { rs.close(); } catch (SQLException e) {}
            }
            if (ps != null) {
                try { ps.close(); } catch (SQLException e) {}
            }
            if (conn != null) {
                try { conn.close(); } catch (SQLException e) {}
            }
        }
    }

}

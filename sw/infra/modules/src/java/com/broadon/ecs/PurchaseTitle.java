package com.broadon.ecs;

import java.sql.SQLException;

import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.logging.Log;

import com.broadon.exception.BroadOnException;
import com.broadon.util.BackingStoreException;
import com.broadon.wsapi.ecs.*;

/**
 * The <code>PurchaseTitle</code> class implements the ECS PurchaseTitle function.
 * 
 * @version $Revision: 1.7 $
 */
public class PurchaseTitle extends ECommerceFunction {

    // Uses apache commons logger (follows axis logging)
    protected static Log log = 
        LogFactory.getLog(PurchaseTitle.class.getName());

    public PurchaseTitle(
            ECommerceServiceImpl service, 
            PurchaseTitleRequestType request,
            PurchaseTitleResponseType response)
    {
        super("PurchaseTitle", service, request, response, log);        
    }
    
    public void processImpl()
        throws BackingStoreException, SQLException, BroadOnException
    {
        PurchaseItem purchaseItem = new PurchaseTitleItem(this); 
        purchaseItem.doPurchase();
    }

    public static PurchaseTitleResponseType purchaseTitle(
            ECommerceServiceImpl service,
            PurchaseTitleRequestType purchaseTitleRequest)
    {
        PurchaseTitleResponseType response = new PurchaseTitleResponseType();
        response.setTitleId(purchaseTitleRequest.getTitleId());
        PurchaseTitle purchaseTitle = new PurchaseTitle(service, purchaseTitleRequest, response);
        purchaseTitle.process();
        return response;
    }

}

package com.broadon.ecs;

/**
 * The <code>Device</code> class contains information
 * about a device.
 *
 * @version $Revision: 1.13 $
 */
public class Device {
    public final static int DEVICE_TYPE_UNKNOWN = -1;
    public final static int DEVICE_TYPE_BB = 0;
    public final static int DEVICE_TYPE_RV = 1;
    public final static int DEVICE_TYPE_NC = 2;
    public final static int[] DEVICE_TYPES = { DEVICE_TYPE_BB, DEVICE_TYPE_RV, DEVICE_TYPE_NC };

    private final int deviceType;
    private final long deviceId;
    private final int virtualDeviceType;
    private final int chipId; 
    
    public Device(long deviceId, Integer virtualDeviceType)
    {
        this.deviceType = (int) (deviceId >> 32);
        this.deviceId = deviceId;
        if (virtualDeviceType == null) {
            this.virtualDeviceType = deviceType;
        } else {
            this.virtualDeviceType = virtualDeviceType.intValue();
        }
        this.chipId = (int) (deviceId & 0xffffffff);
    }

    public String toString()
    {
        if (virtualDeviceType == deviceType) {
            return deviceId + " (" + getDeviceTypeName(deviceType) + chipId + ")";
        } else {
            return deviceId + " (" + getDeviceTypeName(deviceType) + chipId + 
                ", virtual " + getDeviceTypeName(virtualDeviceType) + ")";           
        }
    }
    
    // Accessor functions
    public long getDeviceId()
    {
        return deviceId;
    }
    
    /**
     * @return the virtual device type
     */
    public int getVirtualDeviceType()
    {
        return virtualDeviceType;
    }

    /**
     * @return the real device type
     */
    public int getRealDeviceType()
    {
        return deviceType;
    }

    public int getDeviceType()
    {
        return virtualDeviceType;
    }
    
    /** Static helper methods */
    
    /**
     * Get the string representation for the device type
     * @param deviceType
     * @return
     */
    protected static String getDeviceTypeName(int deviceType)
    {
        switch (deviceType) {
        case DEVICE_TYPE_BB:
            return "BB";
        case DEVICE_TYPE_RV:
            return "RV";
        case DEVICE_TYPE_NC:
            return "NC";
        default:
            return null;
        }
    }
    
}

package com.broadon.ecs;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.logging.Log;

import com.broadon.ecs.ECommerceFunction;
import com.broadon.ecs.ECommerceServiceImpl;
import com.broadon.util.BackingStoreException;
import com.broadon.wsapi.ecs.AbstractRequestType;
import com.broadon.wsapi.ecs.LimitType;
import com.broadon.wsapi.ecs.PurchaseTitleRequestType;
import com.broadon.wsapi.ecs.PurchaseTitleResponseType;
import com.broadon.wsapi.ecs.TicketedPurchaseResponseType;

/**
 * The <code>PurchaseTitleItem</code> class handles the
 * purchase of a title.
 *
 * @version $Revision: 1.18 $
 */
public class PurchaseTitleItem extends PurchaseTicketedItem {

    final PurchaseTitleRequestType purchaseTitleRequest;
    final PurchaseTitleResponseType purchaseTitleResponse;

    public PurchaseTitleItem(ECommerceFunction func)
         throws BackingStoreException
   {
        super(func, Invoice.TRANS_PURCHASE_GAME, 
                ((PurchaseTitleRequestType) func.request).getDeviceCert());
        purchaseTitleRequest = (PurchaseTitleRequestType) request;
        purchaseTitleResponse = (PurchaseTitleResponseType) response;
    }

    protected PurchaseTitleItem(ECommerceFunction func, byte[] deviceCert)
         throws BackingStoreException
   {
        super(func, Invoice.TRANS_PURCHASE_GAME, deviceCert);
        purchaseTitleRequest = null;
        purchaseTitleResponse = null;
    }

    public String formatLimits(LimitType[] limits)
    {
        if (limits == null) {
            return null;
        }
        StringBuffer strbuf = new StringBuffer();
        for (int i = 0; i < limits.length; i++) {
            if (limits[i] != null) {
                if (strbuf.length() > 0) {
                    strbuf.append(",");
                }
                strbuf.append(limits[i].getLimitKind());
                strbuf.append("=");
                strbuf.append(limits[i].getLimits());
            }
        }
        return strbuf.toString();
    } 
    
    /**
     * Get information on the item to purchase for the
     * requested Title
     */
    protected TicketedItemInfo getTicketedItem()
        throws SQLException, BackingStoreException
    {
        long requestedTitleId = 0;
        LimitType[] limits = purchaseTitleRequest.getLimits();
        int itemId = purchaseTitleRequest.getItemId();
        PurchaseItemInfo item = null;

        try {
            requestedTitleId = ECommerceFunction.parseTitleId(
                    purchaseTitleRequest.getTitleId());
        } catch (Throwable e){
            setErrorCode(
                    StatusCode.ECS_BAD_TITLE_ID, 
                    "Error parsing title " + purchaseTitleRequest.getTitleId());
            return null;
        }

        /* Find corresponding item for this country */
        List items = (List) service.getPurchaseItemsCache().get(new Integer(itemId));
        boolean found = false;
        if (items != null) {
            for (int i = 0; i < items.size(); i++) {
                item = (PurchaseItemInfo) items.get(i);

                if (!checkDeviceType(itemId, item.deviceType)) {
                    return null;
                }

                if (checkCountryCode(itemId, item.country)) {
                    found = true;
                    break;
                }
            }
        }

        if (!found) {
            return null;
        }
            
        // Validate item
        if (!(item instanceof TicketedItemInfo)) {
            setErrorCode(
                    StatusCode.ECS_BAD_TITLE_TYPE,
                    "Purchase item " + itemId + " is not for a ticketed title");
        }
        
        TicketedItemInfo ticketedItem = (TicketedItemInfo) item;
        if (ticketedItem.titleId != requestedTitleId) {
            setErrorCode(
                    StatusCode.ECS_BAD_TITLE_ID, 
                    "Purchase item " + itemId + " is for title " +
                    ECommerceFunction.formatTitleId(ticketedItem.titleId) + 
                    ", not requested title " + 
                    purchaseTitleRequest.getTitleId());
            return null;
        }
            
        String reqlimitStr = formatLimits(limits);
        String limitStr = null;
        if (ticketedItem.rtype != null) {
            limitStr = ticketedItem.rtype + "=" + ticketedItem.total_limits;
        }
        if ((reqlimitStr == null && limitStr != null) || 
                !reqlimitStr.equals(limitStr)) {
            setErrorCode(
                    StatusCode.ECS_LIMITED_RIGHTS_ERROR, 
                    "Purchase item " + itemId + " is for limits " + limitStr +
                    ", not requested limits " + reqlimitStr); 
            return null;
        }
        
        // Copy ticketedItem since limits will be updated
        return new TicketedItemInfo(ticketedItem);
    }

    /**
     * Calculate new limits
     */
    protected void updateLimits(TicketedItemInfo ticketedItem) 
    {
        ETicketRecord oldTicket = ticketedItem.record;
        if (oldTicket != null) {
            if (ETicketRecord.LIMIT_TYPE_PR.equals(ticketedItem.rtype)) {
                // Permanent rights - no limits
            } else {
                // Other rights - add limit from old ticket to ticketedItem
                ticketedItem.total_limits += oldTicket.limits;
                ticketedItem.limits[0].setLimits(ticketedItem.total_limits);
            }
            ticketedItem.ticketId = oldTicket.tid;
        } else {
            ticketedItem.ticketId = service.newTicketId(ticketedItem.titleId);
        }
    }
}

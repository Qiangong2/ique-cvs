package com.broadon.ecs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.logging.Log;

import com.broadon.db.DataSourceScheduler;
import com.broadon.util.BackingStore;
import com.broadon.util.BackingStoreException;
import com.broadon.util.CacheExp;
import com.broadon.util.Pair;
import com.broadon.wsapi.ecs.PricingKindType;
import com.broadon.wsapi.ecs.TitleKindType;
import com.broadon.wsapi.ecs.TitleType;

public class ListTitlesCache extends CacheExp implements BackingStore
{
    private ECommerceServiceImpl service;
    
    //  NOTE:  When we no longer need to check for purchase_end_date, then we should
    // switch to use a less expensive Cache implmentation (e.g., CacheLFU).
    //
    private static final String SELECT_SUBS_CONTENT_LIST =
        "SELECT BCCUTIL.ConvertTimeStamp(PURCHASE_END_DATE), " +
        "title_id, title, title_size, approx_size, platform, category " +
        "FROM SUBSCRIPTION_CONTENT_CATALOG " +
        "WHERE " + ECommerceFunction.FILTER_BY_PURCHASE_DATE;

    private static final String SELECT_CONTENT_LIST =
        "SELECT BCCUTIL.ConvertTimeStamp(PURCHASE_END_DATE), " +
        "title_id, title, title_size, approx_size, platform, category " +
        "FROM CURRENT_GAME_PRICING_CATALOG " +
        "WHERE " + ECommerceFunction.FILTER_BY_PURCHASE_DATE;
    
    //  Uses apache commons logger (follows axis logging)
    private static Log log = LogFactory.getLog(ListTitles.class.getName());
    
    public ListTitlesCache(ECommerceServiceImpl service)
    {
        super(100, service.getContentCacheTimeoutMsecs());
        this.service = service;
    }

    public Object retrieve(Object key) throws BackingStoreException
    {
        log.debug("Retrieving titles from DB for " + key);
        
        Key cacheKey = (Key) key;
        
        Connection conn = null;
        PreparedStatement stmt = null;

        StringBuffer filterString = new StringBuffer();
        if (cacheKey.country != null) {
            filterString.append(" AND country_id = " + cacheKey.country.countryId);
        }
        if (cacheKey.deviceType != Device.DEVICE_TYPE_UNKNOWN) {
            //filterString.append(" and device_type_id = " + cacheKey.deviceType);
        }
        if (cacheKey.titleKind != null) {
            /* For now assume no manuals, only games */
            if (cacheKey.titleKind.equals(TitleKindType.Manuals)) {
                return new Pair(null, null);
            }
            /*
             * Use this logic if use title_type to determine manual or game
             */
            /*
            if (cacheKey.deviceType != Device.DEVICE_TYPE_UNKNOWN) {
                filterString.append(" AND title_type = '" + cacheKey.titleType + "'");
            }
            else {
                filterString.append(" AND title_type IN (" + cacheKey.titleType + ")");
            }
            */
        }
        
        if (cacheKey.subscriptionTitleId != null) {
            filterString.append(" AND title_id >= " + cacheKey.subscriptionTitleId +
                    " AND title_id <= " + (cacheKey.subscriptionTitleId.longValue() + 0xFFFFFFFFL));
        }
       
        String orderByString = " ORDER BY title_id";
        
        try {
            conn = service.getConnection(service.getCasDataSource(), DataSourceScheduler.DB_PRIORITY_FAST);
            // Get title list            
            String sql1 = SELECT_CONTENT_LIST + filterString.toString();
            String sql2 = SELECT_SUBS_CONTENT_LIST + filterString.toString();
            String sql;
            if (cacheKey.pricingKind == null) {
                sql = "SELECT * from (" + sql1 + " UNION " + sql2 + ") " + orderByString;
            } else if (PricingKindType.Purchase.equals(cacheKey.pricingKind)) {
                sql = sql1 + orderByString;
            } else if (PricingKindType.Subscription.equals(cacheKey.pricingKind)) {
                sql = sql2 + orderByString;
            } else {
                throw new RuntimeException("Invalid pricingType: " + cacheKey.pricingKind);
            }
            
            stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            List titleList = new ArrayList();
            long expDate = Long.MAX_VALUE;
            long lastTitleId = 0;
            while (rs.next()) {
                int i=1;
                long endDate = rs.getLong(i++);
                if (! rs.wasNull()) {
                    if (endDate < expDate) {
                        expDate = endDate;
                    }
                }

                // Don't include duplicates, assumes results sorted by title id
                long titleId = rs.getLong(i++);
                String titleName = rs.getString(i++);
                long titleSize = rs.getLong(i++);
                long titleFsSize = rs.getLong(i++);
                String platform = rs.getString(i++);
                String category = rs.getString(i++);
                if (titleList.size() == 0 || titleId != lastTitleId) {
                    TitleType title = new TitleType();
                    title.setTitleId(ECommerceFunction.formatTitleId(titleId));
                    title.setTitleName(titleName);
                    title.setTitleSize(titleSize);
                    title.setFsSize(titleFsSize);
                    title.setPlatform(platform);
                    title.setCategory(category);
                    titleList.add(title);   
                    lastTitleId = titleId;
                }
            }
            
            rs.close();
            rs = null;
            stmt.close();
            stmt = null;
            conn.close();
            conn = null;
            
            if (titleList.size() > 0) {
                Long expirationDate = (expDate == Long.MAX_VALUE) ? null : new Long(expDate);
                
                TitleType[] titles = new TitleType[titleList.size()];
                return new Pair(expirationDate, titleList.toArray(titles));
            } else {
                return new Pair(null, null);
            }
        } catch (SQLException e) {
            log.error("ListTitles error", e);
            throw BackingStoreException.rewrap(e);
        } finally {
            if (stmt != null) {
                try { stmt.close(); } catch (SQLException e) {}
            }
            if (conn != null) {
                try { conn.close(); } catch (SQLException e) {}
            }
        }
    }
    
    /**
     * Wrapper class for holding the key for db lookup.
     */
    public static class Key
    {
        public final Country country;
        public final int deviceType;
        public final TitleKindType titleKind;
        public final String titleType;
        public final PricingKindType pricingKind;
        public final Long subscriptionTitleId;
        
        public Key(Country country, Device device, TitleKindType titleKind, 
                PricingKindType pricingKind, Integer channelId)
        {
            this.country = country;
            this.deviceType = device.getDeviceType();
            if (titleKind != null) {
                this.titleKind = titleKind;
                if (deviceType != Device.DEVICE_TYPE_UNKNOWN) {
                    this.titleType = ECommerceFunction.getTitleKindDBString(deviceType, titleKind);
                } else {
                    this.titleType = ECommerceFunction.getTitleKindDBStrings(titleKind);
                }
            } else {
                this.titleKind = null;
                this.titleType = null;
            }
            
            this.pricingKind = pricingKind;
            if (channelId != null) {
                this.subscriptionTitleId = new Long(
                        ECommerceFunction.getSubscriptionTitleId(channelId.intValue()));
            } else {
                this.subscriptionTitleId = null;
            }
        }
        
        public boolean equals(Object obj)
        {
            if (obj instanceof Key) {
                Key other = (Key) obj;
                return (this.deviceType == other.deviceType) &&
                       ((this.country == null)? (other.country == null): 
                            (this.country.equals(other.country))) &&
                       ((this.subscriptionTitleId == null)? (other.subscriptionTitleId == null): 
                            (this.subscriptionTitleId.equals(other.subscriptionTitleId))) &&
                       ((this.titleKind == null)? 
                           (other.titleKind == null): (this.titleKind.equals(other.titleKind))) &&
                       ((this.pricingKind== null)? 
                           (other.pricingKind == null): (this.pricingKind.equals(other.pricingKind)));
            } else {
                return false;
            }
        }
        
        public int hashCode()
        {
            int hash = deviceType;
            if (country != null) {
                hash += country.hashCode();
            }
            if (titleKind != null) {
                hash += titleKind.hashCode();
            }
            if (pricingKind != null) {
                hash += pricingKind.hashCode();
            }
            if (subscriptionTitleId != null) {
                hash += subscriptionTitleId.hashCode();
            }
            return hash;            
        }
        
        public String toString()
        {
            return country + 
                ", device " + Device.getDeviceTypeName(deviceType) +
                ", title kind " + titleKind +
                " and pricing kind " + pricingKind + 
                " and subscriptionTitleId " + subscriptionTitleId;
        }
    }
}

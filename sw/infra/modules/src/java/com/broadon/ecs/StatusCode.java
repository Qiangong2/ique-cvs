package com.broadon.ecs;

/**
 * Response code specific to ecs (use error codes in 600-699 range)
 */
public class StatusCode extends com.broadon.status.StatusCode
{
    /** Bad item ID */
    public static final String ECS_BAD_ITEM_ID = "601";
    static final String ECS_BAD_ITEM_ID_MSG = "Invalid item ID";

    /** Bad title ID */
    public static final String ECS_BAD_TITLE_ID = "602";
    static final String ECS_BAD_TITLE_ID_MSG = "Invalid title ID";

    /** Bad price */
    public static final String ECS_BAD_PRICE = "603";
    static final String ECS_BAD_PRICE_MSG = "Invalid price";

    /** Bad device type */
    public static final String ECS_BAD_DEVICE_TYPE = "604";
    static final String ECS_BAD_DEVICE_TYPE_MSG = "Invalid device type";

    /** Bad version */
    public static final String ECS_BAD_VERSION = "605";
    static final String ECS_BAD_VERSION_MSG = "Invalid version";
    
    /** Bad country ID */
    public static final String ECS_BAD_COUNTRY_CODE = "606";
    static final String ECS_BAD_COUNTRY_CODE_MSG = "Invalid country code";

    /** Bad title type */
    public static final String ECS_BAD_TITLE_TYPE = "607";
    static final String ECS_BAD_TITLE_TYPE_MSG = "Invalid title type";

    /** Bad license type */
    public static final String ECS_BAD_LICENSE_TYPE = "608";
    static final String ECS_BAD_LICENSE_TYPE_MSG = "Invalid license type";

    /** Bad redemption type */
    public static final String ECS_BAD_REDEEM_TYPE = "609";
    static final String ECS_BAD_REDEEM_TYPE_MSG = "Invalid redeem kind";

    /** Bad subscription */
    public static final String ECS_INVALID_SUBSCRIPTION = "612";
    static final String ECS_INVALID_SUBSCRIPTION_MSG =
    "Invalid subscription";

    /** Bad channel ID */
    public static final String ECS_INVALID_CHANNEL_ID = "613";
    static final String ECS_INVALID_CHANNEL_ID_MSG =
    "Invalid channel ID";

    /** Points mismatch */
    public static final String ECS_POINTS_MISMATCH = "614";
    static final String ECS_POINTS_MISMATCH_MSG =
    "Number of points is different from expected number of points";
    
    /** Price mismatch */
    public static final String ECS_PRICE_MISMATCH = "615";
    static final String ECS_PRICE_MISMATCH_MSG =
    "Actual price is different from expected price";
    
    /** Bad payment */
    public static final String ECS_INVALID_PAYMENT = "616";
    static final String ECS_INVALID_PAYMENT_MSG = "Invalid payment";

    /** Ecard cannot be redeem */
    public static final String ECS_ECARD_CANNOT_BE_REDEEMED = "617";
    static final String ECS_ECARD_CANNOT_BE_REDEEMED_MSG = "Ecard cannot be redeemed";
    
    /** Account required */
    public static final String ECS_ACCOUNT_REQUIRED = "618";
    static final String ECS_ACCOUNT_REQUIRED_MSG = "Account information is not provided";

    /** Title is already purchased */
    public static final String ECS_TITLE_ALREADY_PURCHASED = "621";
    static final String ECS_TITLE_ALREADY_PURCHASED_MSG =
    "Title is already purchased";

    /** Already have tickets for title with different limited rights */
    public static final String ECS_LIMITED_RIGHTS_ERROR = "622";
    static final String ECS_LIMITED_RIGHTS_ERROR_MSG =
    "Limited rights error";

    /** Cannot try after use */
    public static final String ECS_TRIAL_AFTER_USE_ERROR = "623";
    static final String ECS_TRIAL_AFTER_USE_ERROR_MSG =
    "Trial not allowed after use";
    
    /** Missing ETicket content id */
    public static final String ECS_NO_ETKM_CONTENT_ID = "624";
    static final String ECS_NO_ETKM_CONTENT_ID_MSG =
    "No corresponding ETicket content id for title";
    
    /** Transfer ETicket errors */
    /** Target serial number not specified */
    public static final String ECS_TARGET_SERIAL_NUMBER_MISSING = "631";
    static final String ECS_TARGET_SERIAL_NUMBER_MISSING_MSG = 
        "Target serial number must be specifed";

    public static final String ECS_TARGET_DEVICE_ID_MISSING = "632";
    static final String ECS_TARGET_DEVICE_ID_MISSING_MSG = 
        "Target device id must be specifed";

    public static final String ECS_DEVICE_IDS_SAME = "633";
    static final String ECS_DEVICE_IDS_SAME_MSG = 
        "Target and source device ids are the same";
    
    public static final String ECS_ETICKETS_IN_SYNC = "641";
    static final String ECS_ETICKETS_IN_SYNC_MSG =
        "Etickets are already in sync";

    /** Transfer Point errors */
    /** Target account not specified */
    public static final String ECS_TARGET_ACCOUNT_MISSING = "651";
    static final String ECS_TARGET_ACCOUNT_MISSING_MSG = 
        "Target account must be specifed";

    public static final String ECS_SOURCE_ACCOUNT_MISSING = "652";
    static final String ECS_SOURCE_ACCOUNT_MISSING_MSG = 
        "Source account must be specifed";

    public static final String ECS_TRANSFER_POINT_MISSING = "653";
    static final String ECS_TRANSFER_POINT_MISSING_MSG = 
        "Points to transfer must be specifed";

    /** ECS Cache error */
    public static final String ECS_CACHE_ERROR = "691";
    static final String ECS_CACHE_ERROR_MSG = "ECS cache error";
    
    /** ETS error */
    public static final String ECS_ETS_ERROR = "697";
    static final String ECS_ETS_ERROR_MSG = "Error getting ETickets";
    
    /** PAS error */
    public static final String ECS_PAS_ERROR = "698";
    static final String ECS_PAS_ERROR_MSG = "Error processing payment";
    
    /** Internal Error. */
    public static final String ECS_INTERNAL_ERROR = "699";
    static final String ECS_INTERNAL_ERROR_MSG = "ECS internal error";
                
    static {
        addStatusCode(ECS_BAD_ITEM_ID, ECS_BAD_ITEM_ID_MSG);
        addStatusCode(ECS_BAD_TITLE_ID, ECS_BAD_TITLE_ID_MSG);
        addStatusCode(ECS_BAD_PRICE, ECS_BAD_PRICE_MSG);
        addStatusCode(ECS_BAD_DEVICE_TYPE, ECS_BAD_DEVICE_TYPE_MSG);
        addStatusCode(ECS_BAD_VERSION, ECS_BAD_VERSION_MSG);
        addStatusCode(ECS_BAD_COUNTRY_CODE, ECS_BAD_COUNTRY_CODE_MSG);
        addStatusCode(ECS_BAD_TITLE_TYPE, ECS_BAD_TITLE_TYPE_MSG);
        addStatusCode(ECS_BAD_LICENSE_TYPE, ECS_BAD_LICENSE_TYPE_MSG);
        addStatusCode(ECS_BAD_REDEEM_TYPE, ECS_BAD_REDEEM_TYPE_MSG);
        addStatusCode(ECS_INVALID_SUBSCRIPTION, ECS_INVALID_SUBSCRIPTION_MSG);
        addStatusCode(ECS_INVALID_CHANNEL_ID, ECS_INVALID_CHANNEL_ID_MSG);
        addStatusCode(ECS_POINTS_MISMATCH, ECS_POINTS_MISMATCH_MSG);
        addStatusCode(ECS_PRICE_MISMATCH, ECS_PRICE_MISMATCH_MSG);
        addStatusCode(ECS_INVALID_PAYMENT, ECS_INVALID_PAYMENT_MSG);
        addStatusCode(ECS_ECARD_CANNOT_BE_REDEEMED, ECS_ECARD_CANNOT_BE_REDEEMED_MSG);
        addStatusCode(ECS_ACCOUNT_REQUIRED, ECS_ACCOUNT_REQUIRED_MSG);
        addStatusCode(ECS_TITLE_ALREADY_PURCHASED, ECS_TITLE_ALREADY_PURCHASED_MSG);
        addStatusCode(ECS_LIMITED_RIGHTS_ERROR, ECS_LIMITED_RIGHTS_ERROR_MSG);
        addStatusCode(ECS_TRIAL_AFTER_USE_ERROR, ECS_TRIAL_AFTER_USE_ERROR_MSG);
        addStatusCode(ECS_NO_ETKM_CONTENT_ID, ECS_NO_ETKM_CONTENT_ID_MSG);
        addStatusCode(ECS_TARGET_SERIAL_NUMBER_MISSING, ECS_TARGET_SERIAL_NUMBER_MISSING_MSG);
        addStatusCode(ECS_TARGET_DEVICE_ID_MISSING, ECS_TARGET_DEVICE_ID_MISSING_MSG);
        addStatusCode(ECS_DEVICE_IDS_SAME, ECS_DEVICE_IDS_SAME_MSG);
        addStatusCode(ECS_ETICKETS_IN_SYNC, ECS_ETICKETS_IN_SYNC_MSG);
        addStatusCode(ECS_CACHE_ERROR, ECS_CACHE_ERROR_MSG);
        addStatusCode(ECS_ETS_ERROR, ECS_ETS_ERROR_MSG);
        addStatusCode(ECS_PAS_ERROR, ECS_PAS_ERROR_MSG);
        addStatusCode(ECS_INTERNAL_ERROR, ECS_INTERNAL_ERROR_MSG);
    }

}

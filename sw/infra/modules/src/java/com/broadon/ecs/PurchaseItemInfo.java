package com.broadon.ecs;

import java.math.BigDecimal;

/**
 * The <code>PurchaseItemInfo</code> class provides information for a
 * item to be purchased using ECS
 * 
 * @version $Revision: 1.8 $
 */
public class PurchaseItemInfo {
    public static String SUBSCRIPTION_TYPE = "SUBSCRIPT";
    public static String VCPOINTS_TYPE = "VCPOINTS";
    public static String PERMANENT_TYPE = "PERMANENT";
    public static String RENTAL_TYPE = "RENTAL";
    public static String TRIAL_TYPE = "TRIAL";
        
    protected String country;  /* Country that the item is for */
    protected int deviceType; /* Device type that the item is for */

    protected final int itemId;
    protected final BigDecimal itemPrice; /* Price per item */
    protected final String currency;

    protected int quantity;    
    protected BigDecimal totalPrice;
    protected Long points;
    
    public PurchaseItemInfo(PurchaseItemInfo item)
    {
        this(item.itemId, item.itemPrice, item.currency, item.quantity);
        this.country = item.country;
        this.deviceType = item.deviceType;
        this.points = item.points;
    }
    
    public PurchaseItemInfo(int itemId, BigDecimal itemPrice, String currency, int quantity)
    {
        this.itemId = itemId;
        this.itemPrice = itemPrice;
        this.currency = currency;
        setQuantity(quantity);
    }

    public BigDecimal getPrice()
    {
        return totalPrice;
    }
    
    public void setQuantity(int quantity)
    {
        this.quantity = quantity;
        this.totalPrice = itemPrice.multiply(BigDecimal.valueOf(quantity));
    }
    
    public String toString()
    {
        StringBuffer strBuffer = new StringBuffer();
        strBuffer.append(" item ");
        strBuffer.append(itemId);
        strBuffer.append(", country ");
        strBuffer.append(country);
        strBuffer.append(", device ");
        strBuffer.append(deviceType);
        strBuffer.append(", price ");
        strBuffer.append(quantity);
        strBuffer.append("x");
        strBuffer.append(itemPrice);
        strBuffer.append("=");
        strBuffer.append(totalPrice);
        strBuffer.append(" ");
        strBuffer.append(currency);
        
        if (points != null) {
            strBuffer.append(", points ");
            strBuffer.append(points);
        }
        return strBuffer.toString();
    }
}

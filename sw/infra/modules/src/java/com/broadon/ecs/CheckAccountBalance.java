package com.broadon.ecs;

import java.rmi.RemoteException;

import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.logging.Log;

import com.broadon.wsapi.ecs.*;

/**
 * The <code>CheckAccountBalance</code> class implements the ECS CheckAccountBalance function.
 * 
 * @version $Revision: 1.5 $
 */
public class CheckAccountBalance extends ECommerceFunction {

    // Uses apache commons logger (follows axis logging)
    protected static Log log = 
        LogFactory.getLog(CheckAccountBalance.class.getName());

    public CheckAccountBalance(
            ECommerceServiceImpl service, 
            CheckAccountBalanceRequestType request,
            CheckAccountBalanceResponseType response)
    {
        super("CheckAccountBalance", service, request, response, log);        
    }
    
    public void processImpl()
    {
        CheckAccountBalanceRequestType checkAccountRequest = (CheckAccountBalanceRequestType) request;
        CheckAccountBalanceResponseType checkAccountResponse = (CheckAccountBalanceResponseType) response;
        
        try {
            AccountPayment account = new AccountPayment(
                service.getPasSOAP(),
                checkAccountRequest.getAccount());

            if (account.checkBalance(request)) {
                MoneyType available = account.getAvailableBalance();
                checkAccountResponse.setBalance(available);
            } else {
                response.setErrorCode(account.errorCode);
                response.setErrorMessage(account.errorMessage);
            }           
        } catch (RemoteException e) {
            setErrorCode(StatusCode.ECS_PAS_ERROR, e.getMessage());
            log.error("Error checking balance using PAS", e);
        }
    }

    public static CheckAccountBalanceResponseType checkAccountBalance(
            ECommerceServiceImpl service,
            CheckAccountBalanceRequestType request)
    {
        CheckAccountBalanceResponseType response = new CheckAccountBalanceResponseType();
        CheckAccountBalance checkAccount = new CheckAccountBalance(service, request, response);
        checkAccount.process();
        return response;
    }
}

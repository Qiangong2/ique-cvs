package com.broadon.ecs;

import java.sql.SQLException;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.exception.BroadOnException;
import com.broadon.util.BackingStoreException;
import com.broadon.wsapi.ecs.*;

/**
 * The <code>Subscribe</code> class implements the ECS Subscribe function.
 * 
 * Purchases a subscription.
 * 
 * @version $Revision: 1.28 $
 */
public class Subscribe extends ECommerceFunction {
    // Uses apache commons logger (follows axis logging)
    protected static Log log = 
        LogFactory.getLog(Subscribe.class.getName());
    
    public Subscribe(
            ECommerceServiceImpl service, 
            SubscribeRequestType request,
            SubscribeResponseType response)
    {
        super("Subscribe", service, request, response, log);        
    }
    
    public void processImpl()
        throws BackingStoreException, SQLException, BroadOnException
    {
        SubscribeRequestType subscribeRequest = (SubscribeRequestType) request;
        SubscribeResponseType subscribeResponse = (SubscribeResponseType) response;

        try {
            // TODO: Use request to update subscription, returning any errors
            //       Use eCard to pay for subscription (substract payment amount, log transaction)
            //       Update play time for device (update tickets expiration)
            PurchaseItem purchaseItem = new PurchaseSubscriptionItem(this, subscribeRequest.getDeviceCert());
            purchaseItem.doPurchase();

        } catch (InvalidSubscriptionException e) {
            setErrorCode(StatusCode.ECS_INVALID_SUBSCRIPTION, e.getLocalizedMessage());
            log.debug("Subscribe error", e);
        }
    }
     
   /**
     * Service the subscribe request
     * @param service
     * @param subscribeRequest
     * @return
     */
    public static SubscribeResponseType subscribe(
            ECommerceServiceImpl service, 
            SubscribeRequestType subscribeRequest) 
    {
        SubscribeResponseType response = new SubscribeResponseType();
        Subscribe subscribe = new Subscribe(service, subscribeRequest, response);
        subscribe.process();
        return response;
    }
}

package com.broadon.ecs;

import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.logging.Log;

import com.broadon.ecs.TitleDetailsCache.CachedTitleDetailsResponse;
import com.broadon.util.BackingStoreException;
import com.broadon.wsapi.ecs.GetTitleDetailsRequestType;
import com.broadon.wsapi.ecs.GetTitleDetailsResponseType;
import com.broadon.wsapi.ecs.TitleInfoType;

/**
 * The <code>GetTitleDetails</code> class implements
 * the ECS GetTitleDetails function.
 *
 * It looks up information about a title, such as the
 * title type, description, list of contents, etc.
 * given the title id and the country id.
 * 
 * @version $Revision: 1.30 $
 */
public class GetTitleDetails extends ECommerceFunction 
{
    // Uses apache commons logger (follows axis logging)
    protected static Log log = LogFactory.getLog(GetTitleDetails.class.getName());

    public GetTitleDetails(
            ECommerceServiceImpl service, 
            GetTitleDetailsRequestType request,
            GetTitleDetailsResponseType response)
    {
        super("GetTitleDetails", service, request, response, log);        
    }
    
    public void processImpl()
        throws BackingStoreException
    {
        GetTitleDetailsRequestType getTitleDetailsRequest = (GetTitleDetailsRequestType) request;
        GetTitleDetailsResponseType getTitleDetailsResponse = (GetTitleDetailsResponseType) response;

        if (getCountry() == null) {
            setErrorCode(StatusCode.ECS_BAD_COUNTRY_CODE, null);
            return;
        }

        long titleId = 0;
        try {
            titleId = parseTitleId(getTitleDetailsRequest.getTitleId());
        } catch (Throwable e) {
            setErrorCode(StatusCode.ECS_BAD_TITLE_ID, 
                    "Error parsing title " + getTitleDetailsRequest.getTitleId());
            return;
        }
        
        CachedTitleDetailsResponse cachedValue = (CachedTitleDetailsResponse)
            service.getTitleDetailsCache().get(
                    new TitleIdKey(getCountry(), request.getLanguage(), titleId));
            
        if (cachedValue != null && cachedValue.titleInfo != null) {
            getTitleDetailsResponse.setTitleInfo(cachedValue.titleInfo);
            getTitleDetailsResponse.setTitlePricings(cachedValue.titlePricings);
            getTitleDetailsResponse.setRatings(cachedValue.ratings);
        } else {
            setErrorCode(StatusCode.ECS_BAD_TITLE_ID, 
                    "Cannot get title details for title " + 
                    getTitleDetailsRequest.getTitleId());
        }
    }

    /**
     * Returns details for a given title
     */
    public static GetTitleDetailsResponseType getTitleDetails(
           ECommerceServiceImpl service, 
           GetTitleDetailsRequestType getTitleDetailsRequest) 
    {
        GetTitleDetailsResponseType response = new GetTitleDetailsResponseType();
        TitleInfoType titleInfo = new TitleInfoType();
        titleInfo.setTitleId(getTitleDetailsRequest.getTitleId());
        response.setTitleInfo(titleInfo);

        GetTitleDetails getTitleDetails = new GetTitleDetails(service, getTitleDetailsRequest, response);        
        getTitleDetails.process();
        return response;
    }
}

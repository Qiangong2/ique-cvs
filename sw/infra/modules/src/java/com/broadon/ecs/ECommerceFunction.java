package com.broadon.ecs;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.Locale;
import java.util.MissingResourceException;

import org.apache.axis.Constants;
import org.apache.axis.MessageContext;
import org.apache.commons.logging.Log;

import com.broadon.db.DBException;
import com.broadon.exception.BroadOnException;
import com.broadon.util.BackingStoreException;
import com.broadon.util.HexString;
import com.broadon.wsapi.ecs.AbstractRequestType;
import com.broadon.wsapi.ecs.AbstractResponseType;
import com.broadon.wsapi.ecs.PricingCategoryType;
import com.broadon.wsapi.ecs.TitleKindType;
import com.broadon.wsapi.ets.AbstractTransactionType;
import com.broadon.wsapi.ets.ETicketPackageType;
import com.broadon.wsapi.ets.ETicketPortType;
import com.broadon.wsapi.ets.GetTicketsRequestType;
import com.broadon.wsapi.ets.GetTicketsResponseType;
import com.broadon.wsapi.ets.TicketsRequestType;
import com.broadon.wsapi.ias.GetDeviceInfoRequestType;
import com.broadon.wsapi.ias.GetDeviceInfoResponseType;
import com.broadon.wsapi.ias.IdentityAuthenticationPortType;

abstract public class ECommerceFunction {
    public static final int STATUS_OK = 0;
    protected static final byte[] DEFAULT_CONTENT_MASK = new byte[32];
    
    protected static String VERSION = "1.0";
    
    protected static String FILTER_BY_PURCHASE_DATE =
        "PURCHASE_START_DATE <= SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) AND " + 
        "(PURCHASE_END_DATE IS NULL OR PURCHASE_END_DATE >= SYS_EXTRACT_UTC(CURRENT_TIMESTAMP))";
    
    protected static String SELECT_CURRENT_TIMESTAMP =
        "SELECT BCCUTIL.ConvertTimeStamp(SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)) FROM DUAL";
    
    static {
        for (int i = 0; i < DEFAULT_CONTENT_MASK.length; i++) {
            DEFAULT_CONTENT_MASK[i] = (byte) 0xFF;
        }
    }
    
    final protected ECommerceServiceImpl service;
    final protected Log log;
    final protected AbstractRequestType request;
    final protected AbstractResponseType response;
    final protected Locale locale;
    final protected String name;
    private Device device;
    private Country country;
    
    public ECommerceFunction(String name, ECommerceServiceImpl service, AbstractRequestType request, AbstractResponseType response, Log log)
    {
        this.request = request;
        this.response = response;
        this.service = service;
        this.locale = Messages.getLocale(request.getLanguage(), request.getCountryCode());
        this.log = log;
        this.name = name;
    }

    public String getClientIp()
    {
        return MessageContext.getCurrentContext().getStrProp(Constants.MC_REMOTE_ADDR); 
    }
    
    public Device getDevice()
    {
        if (device == null) {
            device = getDevice(request, response);
        }
        return device;
    }
        
    public Country getCountry()
        throws BackingStoreException
    {
        if (country == null) {
            country = getCountry(service, request);
            if (country == null) {
                setErrorCode(StatusCode.ECS_BAD_COUNTRY_CODE, null);
            }
            /* TODO: Check region agrees? */
            // if (!country.region.equals(request.getRegionId())) {
            // setErrorCode(StatusCode.ECS_BAD_REGION, null);
            // }
        }
        return country;
    }
    
    protected void setErrorCode(String code, String message)
    {
        setErrorCode(locale, code, message, response);
    }
    
    abstract protected void processImpl()
        throws SQLException, BackingStoreException, BroadOnException;
    
    protected void process()
    {
        try {
            initResponse(locale, request, response);
            if (response.getErrorCode() == STATUS_OK) {
                processImpl();
            }
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION, e.getLocalizedMessage());
            log.error(name + " error", e);
        } catch (BackingStoreException e) {               
            if (e.getCause() instanceof SQLException) {
                setErrorCode(StatusCode.SC_SQL_EXCEPTION, e.getLocalizedMessage());
            } else {
                setErrorCode(StatusCode.ECS_CACHE_ERROR, e.getLocalizedMessage());
            }
            log.error(name + " error", e);
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.ECS_INTERNAL_ERROR, e.getLocalizedMessage());
            log.error(name + " error", e);
        }
    }

    /* Static helper functions */

    /**
     * Returns the current server time in milliseconds
     * @return
     */
    protected static long getServerTimestamp()
    {
        return System.currentTimeMillis();
    }

    /**
     * Returns the current DB time in milliseconds
     */
    protected static long getCurrentDbTimestamp(Connection conn)
        throws SQLException
    {
        Statement stmt = null;
        
        try {
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(SELECT_CURRENT_TIMESTAMP);
            rs.next();
            return rs.getLong(1);
        } finally {
            if (stmt != null) {
                stmt.close();
            } 
        }
    }
    
    /* Prepend a character to make the string the desired length */
    protected static String prependString(String str, char c, int totalLength)
    {
        if (str.length() < totalLength) {
            /* 0 pad str */
            StringBuffer strbuf = new StringBuffer(totalLength);
            for (int i = 0; i < totalLength - str.length(); i++) {
                strbuf.append(c);
            }
            strbuf.append(str);
            str = strbuf.toString();
        }
        return str;
    }
    
    /**
     * Formats a channel id as a hex string
     */
    protected static String formatChannelId(int channelId)
    {
        String str = Integer.toHexString(channelId).toUpperCase();
        str = prependString(str, '0', 8);
        return str;
    }
    
    /**
     * Parses a channel id from a hex string into a integer
     */
    protected static int parseChannelId(String channelId)
    {
        return Integer.parseInt(channelId, 16);
    }
    
    /**
     * Formats a title id as a string
     */
    protected static String formatTitleId(long titleId)
    {
        String str = Long.toHexString(titleId).toUpperCase();
        str = prependString(str, '0', 16);
        return str;
    }
    
    /**
     * Parses a title id from a hex string into a integer
     */
    protected static long parseTitleId(String titleId)
    {
        return Long.parseLong(titleId, 16);
    }
    
    /**
     * Formats content id as a hex string
     * @param contentId
     * @return
     */
    protected static String formatContentId(BigInteger contentId)
    {
        if (contentId != null) {
            String str = contentId.toString(16).toUpperCase();
            str = prependString(str, '0', 24);
            return str;
        } else {
            return null;
        }
    }
    
    protected static String formatContentId(BigDecimal contentId)
    {
        if (contentId != null) {
            return formatContentId(contentId.toBigInteger());
        } else {
            return null;
        }
    }
    
    /**
     * Takes mask represented as hex string from DB
     * and returns a content mask.  The content mask
     * always has 256 bits of 1s and the remaining 256
     * bits comes from the hex string in the DB.
     * @param dbMask
     * @return
     */
    protected static byte[] getContentMask(String dbMask)
    {
        if (dbMask == null) {
            return DEFAULT_CONTENT_MASK;
        }
        
        byte[] moreBytes = HexString.fromHexString(dbMask);
        byte[] contentMask = new byte[DEFAULT_CONTENT_MASK.length + moreBytes.length];
        System.arraycopy(DEFAULT_CONTENT_MASK, 0, contentMask, 0, DEFAULT_CONTENT_MASK.length);
        System.arraycopy(moreBytes, 0, contentMask, DEFAULT_CONTENT_MASK.length, moreBytes.length);
        return contentMask;
   }
    
    /**
     * Initializes fields in a AbstractResponseType given an AbstractRequestType
     * @param request
     * @param response
     */
    protected static void initResponse(Locale locale,
                                       AbstractRequestType request,
                                       AbstractResponseType response)
    {
        response.setDeviceId(request.getDeviceId());
        response.setTimeStamp(getServerTimestamp());
        response.setMessageId(request.getMessageId());
        response.setVersion(request.getVersion());
        if (!VERSION.equals(request.getVersion().trim())) {
            setErrorCode(locale, StatusCode.ECS_BAD_VERSION, null, response);
        }
    }

    /**
     * Initializes fields in a AbstractTransactionType given an AbstractRequestType
     * @param request
     * @param response
     */
    protected static void initTransaction(AbstractRequestType request,
                                          AbstractTransactionType transaction)
    {
        transaction.setDeviceId(Long.toString(request.getDeviceId()));
        transaction.setMessageId(request.getMessageId());
        transaction.setVersion(VERSION);
    }
    
    /**
     * Initializes fields in a PAS AbstractRequestType given an ECS AbstractRequestType
     * @param request
     * @param response
     */
    protected static void initPasRequest(
            com.broadon.wsapi.ecs.AbstractRequestType ecsRequest,
            com.broadon.wsapi.pas.AbstractRequestType pasRequest)
    {
        pasRequest.setMessageID(ecsRequest.getMessageId());
        pasRequest.setVersion(VERSION);
    }
    
    /**
     * Initializes fields in a IAS AbstractDeviceRequest given an ECS AbstractRequestType
     * @param request
     * @param response
     */
    protected static void initDeviceRequest(
    		com.broadon.wsapi.ecs.AbstractRequestType ecsRequest,
    		com.broadon.wsapi.ias.AbstractDeviceRequestType iasRequest)
    {
    	iasRequest.setDeviceId(ecsRequest.getDeviceId());
        // TODO: Remove locale
    	iasRequest.setLocale(com.broadon.wsapi.ias.LocaleType.en_US);
    	iasRequest.setVersion(VERSION);
        iasRequest.setTimeStamp(getServerTimestamp());
    }

    /**
     * Sets the error code in AbstractResponseType
     * @param String code
     * @param String message
     * @param AbstractResponseType response
     */
    protected static void setErrorCode(Locale locale,
                                       String code, 
                                       String message,
                                       AbstractResponseType response)
    {
        response.setErrorCode(Integer.parseInt(code));
        String statusMessage = StatusCode.getMessage(code);
        /*try {
            statusMessage = Messages.getMessage(locale, statusMessage);
        } catch (MissingResourceException e) {
            
        }
        */
        if (message != null) {
            response.setErrorMessage(statusMessage + ": " + message);
        }
        else {
            response.setErrorMessage(statusMessage);
        }
    }  

    /**
     * Returns the device associated with the AbstractRequestType.
     */
    protected static Device getDevice(
    		AbstractRequestType request,
    		AbstractResponseType response)
    {
    	return new Device(
    		request.getDeviceId(),
                request.getVirtualDeviceType());
    }    
    
    /**
     * Returns the country associated with the AbstractRequestType.
     */
    protected static Country getCountry(
            ECommerceServiceImpl service,
            AbstractRequestType request)
        throws BackingStoreException
    {
        return (Country) service.countryCache.get(request.getCountryCode());
    }    
    
    /**
     * Extracts channel id from the title id
     * @return
     */
    protected static int getChannelId(long titleId)
    {
        return (int) (titleId >> 32);
    }
    
    /**
     * @return the subscription title id
     */
    protected static long getSubscriptionTitleId(int channelId)
    {
        return ((long) channelId) << 32;
    }
    
    /**
     * Extracts channel id from the title id
     * @return
     */
    protected static String getChannelId(String titleId)
    {
        return formatChannelId(getChannelId(parseTitleId(titleId)));
    }
    
    /**
     * @return the subscription title id
     */
    protected static String getSubscriptionTitleId(String channelId)
    {
        return formatTitleId(getSubscriptionTitleId(parseChannelId(channelId)));
    }
    
    /**
     * Returns the appropriate TitleKindType given the 
     * database entry for "TITLE_TYPE"
     * @param titleKindStr
     * @return
     */
    protected static TitleKindType getTitleKindType(String titleKindStr)
    {
        TitleKindType titleKind = null;
        if ("visible".equalsIgnoreCase(titleKindStr)
                || "GAME".equalsIgnoreCase(titleKindStr)) {
            titleKind = TitleKindType.Games;
        } else if ("MANUAL".equalsIgnoreCase(titleKindStr)) {
            titleKind = TitleKindType.Manuals;
        }
        return titleKind;
    }

    /**
     * Returns the appropriate PricingCategoryType given the 
     * database entry for "LICENSE_TYPE"
     * @param licenseType
     * @return
     */
    protected static PricingCategoryType getPricingCategoryType(String licenseType)
        throws DBException
    {
        //return PricingCategoryType.fromString(productType);
        
        // Use this logic if the pricing string in the DB is different
        // from the PricingCategoryType names
        PricingCategoryType pricingCategoryType = null; 
        if ("PERMANENT".equalsIgnoreCase(licenseType)) {
            pricingCategoryType = PricingCategoryType.Permanent;
        } else if ("RENTAL".equalsIgnoreCase(licenseType)) {
            pricingCategoryType = PricingCategoryType.Rental;
        } else if ("TRIAL".equalsIgnoreCase(licenseType)) {
            pricingCategoryType = PricingCategoryType.Trial;
        } else if ("BONUS".equalsIgnoreCase(licenseType)) {
            pricingCategoryType = PricingCategoryType.Bonus;
        } else if ("SUBSCRIPT".equalsIgnoreCase(licenseType)) {
            pricingCategoryType = PricingCategoryType.Subscription;
        } else {
            throw new DBException("Cannot map license type '" + licenseType + "'to pricing category");
        }
        return pricingCategoryType;
        
    }
    
    /**
     * Returns the appropriate database entry for "LICENSE_TYPE"
     * given the PricingCategoryType 
     * @param pricing
     * @return
     */
    protected static String getLicenseTypeDBString(PricingCategoryType pricing)
    {
        if (PricingCategoryType.Bonus.equals(pricing)) {
            return "BONUS";
        } else if (PricingCategoryType.Rental.equals(pricing)) {
            return "RENTAL";
        } else if (PricingCategoryType.Permanent.equals(pricing)) {
            return "PERMANENT";
        } else if (PricingCategoryType.Trial.equals(pricing)) {
            return "TRIAL";
        } else if (PricingCategoryType.Subscription.equals(pricing)) {
            return "SUBSCRIPT";
        }
        return null;
    }

    /**
     * Returns the appropriate database entry for "TITLE_TYPE"
     * given the device type and TitleKindType
     * @param deviceType
     * @param titleKind
     * @return
     */
    protected static String getTitleKindDBString(int deviceType, TitleKindType titleKind)
    {
        switch (deviceType) {
        case Device.DEVICE_TYPE_BB:
            if (TitleKindType.Games.equals(titleKind)) {
                return "visible";
            } else if (TitleKindType.Manuals.equals(titleKind)) {
                return "manual";
            }
            return null;
        default:    
            if (TitleKindType.Games.equals(titleKind)) {
                return "GAME";
            } else if (TitleKindType.Manuals.equals(titleKind)) {
                return "MANUAL";
            }
            return null;
        }
    }   

    /**
     * Returns a comma separated string with the appropriate database entries
     * for "TITLE_TYPE" for the given TitleKindType
     * @param titleKind
     * @return
     */
    protected static String getTitleKindDBStrings(TitleKindType titleKind)
    {
        if (TitleKindType.Games.equals(titleKind)) {
            return "'visible', 'GAME'";
        } else if (TitleKindType.Manuals.equals(titleKind)) {
            return "'manual', 'MANUAL'";
        }	
	return null;    	
    }

    /**
     * Returns a comma separated string with the appropriate database entries
     * for "TITLE_TYPE" for the given device type
     * @param deviceType
     * @return
     */
    protected static String getTitleKindDBStrings(int deviceType)
    {
    	switch (deviceType) {
    	case Device.DEVICE_TYPE_BB: 
   			return "'visible', 'manual'";
    	default:
   			return "'GAME', 'MANUAL'";
    	}
    }
  
    /**
     * Calls ETS and returns a ETicketPackage, updates the response with
     * the ETS error code if there is an error
     * @param service
     * @param request
     * @param response
     * @param ticketRequests
     * @param publicKey
     * @return
     * @throws java.rmi.RemoteException
     */
    protected static ETicketPackageType getETicketPackage(
                ECommerceServiceImpl service,
                AbstractRequestType request,
                AbstractResponseType response,
                TicketsRequestType[] ticketRequests,
                byte[] cert)
        throws java.rmi.RemoteException
    {
        if (ticketRequests == null || ticketRequests.length == 0) {
            // Nothing todo
            return null;
        }
        
        ETicketPortType eTicketPort = service.getETicketSOAP();
        GetTicketsRequestType getTicketsRequest = new GetTicketsRequestType();               
        initTransaction(request, getTicketsRequest);
        getTicketsRequest.setDeviceCertificate(cert);
        getTicketsRequest.setTicketsRequest(ticketRequests);
        getTicketsRequest.setRequesterId("LOCAL_ECS");
        GetTicketsResponseType getTicketsResponse = eTicketPort
                .getTickets(getTicketsRequest);
        // Figure out response
        if (getTicketsResponse != null) {
            if (getTicketsResponse.getErrorCode() != STATUS_OK) {
                response.setErrorCode(getTicketsResponse.getErrorCode());
                response.setErrorMessage(getTicketsResponse.getErrorMessage());
            }
            return getTicketsResponse.getETicketPackage();
        } else {
            return null;
        }
    }
    
    /**
     * Get Subscription info from IAS, updates the response with the
     * IAS error code, if there is an error
     * @param service
     * @param request
     * @param response
     * @return
     * @throws java.rmi.RemoteException
     */
    protected static GetDeviceInfoResponseType getDeviceInfoResponse(
    		ECommerceServiceImpl service,
    		AbstractRequestType request,
    		AbstractResponseType response)
    	throws java.rmi.RemoteException
    {
    	IdentityAuthenticationPortType iasPort = service.getIasSOAP();
    	GetDeviceInfoRequestType getInfoRequest = 
    		new GetDeviceInfoRequestType();
    	initDeviceRequest(request, getInfoRequest);
    	//getInfoRequest.setTokenId(request.getDeviceToken());
    	GetDeviceInfoResponseType getInfoResponse = 
    		iasPort.getDeviceInfo(getInfoRequest);
    	response.setErrorCode(getInfoResponse.getErrorCode());
        if (response.getErrorCode() != STATUS_OK) {
            response.setErrorMessage(getInfoResponse.getErrorMessage());
        }
        return getInfoResponse;
    }

}
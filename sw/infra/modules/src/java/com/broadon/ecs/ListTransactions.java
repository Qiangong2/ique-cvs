package com.broadon.ecs;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.logging.Log;
import com.broadon.db.DBException;
import com.broadon.db.DataSourceScheduler;
import com.broadon.wsapi.ecs.AccountPaymentType;
import com.broadon.wsapi.ecs.DebitCreditType;
import com.broadon.wsapi.ecs.LimitType;
import com.broadon.wsapi.ecs.ListTransactionsRequestType;
import com.broadon.wsapi.ecs.ListTransactionsResponseType;
import com.broadon.wsapi.ecs.MoneyType;
import com.broadon.wsapi.ecs.PriceType;
import com.broadon.wsapi.ecs.PricingCategoryType;
import com.broadon.wsapi.ecs.PricingType;
import com.broadon.wsapi.ecs.RatingType;
import com.broadon.wsapi.ecs.TitleType;
import com.broadon.wsapi.ecs.TransactionType;
import com.broadon.wsapi.pas.BalanceType;

/**
 * The <code>ListTransactions</code> class implements
 * the ECS ListTransactions function.
 * 
 * Provides a list of transactions for a given account.
 * 
 * @version $Revision: 1.19 $
 */
public class ListTransactions extends ECommerceFunction {
    private static final String SELECT_TRANSACTION_LIST =
        "SELECT trans_id, BCCUTIL.ConvertTimeStamp(trans_date) trans_date, " +
        "trans_type, account_id, device_id, status, total_amount, total_taxes, " +
        "total_paid, currency, orig_trans_id, operator_id, country_id, item_id, " +
        "qty, total_discount, payment_type, payment_method_id, pas_auth_id, " +
        "payment_ecard_type, BCCUTIL.ConvertTimeStamp(status_date) status_date " +
        "FROM ecs_transactions WHERE account_id = ? ";
    
    private static final String SELECT_TITLE_DETAIL = 
    	"SELECT title_id, title, product_code, title_size, approx_size, " +
    	"platform, category, item_price, item_currency, rtype, " +
        "limits, license_type, rating_system, rating_value " +
    	"FROM game_pricing_catalog WHERE item_id = ? " +
        "ORDER BY decode(locale,default_locale,1,country_default,2,3) ";
    
    // Uses apache commons logger (follows axis logging)
    protected static Log log = LogFactory.getLog(ListTransactions.class.getName());

    public ListTransactions(
            ECommerceServiceImpl service, 
            ListTransactionsRequestType request,
            ListTransactionsResponseType response)
    {
        super("ListTransactions", service, request, response, log);        
    }
    

    public void processImpl()
        throws SQLException, DBException
    {
        ListTransactionsRequestType listTransactionsRequest = (ListTransactionsRequestType) request;
        ListTransactionsResponseType listTransactionsResponse = (ListTransactionsResponseType) response;

        AccountPaymentType acc = listTransactionsRequest.getAccount();
        if (acc == null) {
            setErrorCode(StatusCode.ECS_ACCOUNT_REQUIRED, null);
            return;
        }
        
        MoneyType available = new MoneyType();
        
        try {
            AccountPayment account = new AccountPayment(
                service.getPasSOAP(),
                acc);

            log.debug("Check account balance: account=" + acc.getAccountNumber());
            
            if (account.checkBalance(listTransactionsRequest)) {
                BalanceType balance = account.getBalance();
                available.setCurrency(balance.getAvailable().getCurrencyType());
                available.setAmount(balance.getAvailable().getAmount());
                listTransactionsResponse.setBalance(available);
            } else {
                response.setErrorCode(account.errorCode);
                response.setErrorMessage(account.errorMessage);
                return;
            }           
        } catch (RemoteException e) {
            setErrorCode(StatusCode.ECS_PAS_ERROR, e.getMessage());
            log.error("Error checking balance using PAS", e);
            return;
        } 

        Date begin = listTransactionsRequest.getBegin();
        Date end = listTransactionsRequest.getEnd();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");   // oracle default date format
        StringBuffer timeRange = new StringBuffer("");
        if (begin != null)
            timeRange.append(" and trans_date >= " + formatter.format(begin));
        if (end != null)
            timeRange.append(" and trans_date <= " + formatter.format(end)).append(" ");
        
        Connection conn = null;
        PreparedStatement stmt = null;
        Connection casConn = null;
        PreparedStatement stmt2 = null;
        try {
            conn = service.getConnection(service.getEcsDataSource(), DataSourceScheduler.DB_PRIORITY_MEDIUM); 
            casConn = service.getConnection(service.getCasDataSource(), DataSourceScheduler.DB_PRIORITY_FAST);

            // Get title list            
            String sql = SELECT_TRANSACTION_LIST + timeRange.toString() +
                         "ORDER BY trans_date DESC";
            
            stmt = conn.prepareStatement(sql);
            stmt.setLong(1, Long.parseLong(acc.getAccountNumber()));
            log.info(sql);
            log.info("account_id = " + acc.getAccountNumber());
            ResultSet rs = stmt.executeQuery();
            List transactionList = new ArrayList();
            long lastTransId = 0;
            double balance = Double.parseDouble(available.getAmount());
            stmt2 = casConn.prepareStatement(SELECT_TITLE_DETAIL);
            log.info(SELECT_TITLE_DETAIL);
            while (rs.next()) {

                // Don't include duplicates, assumes results sorted by TRANS_ID
                long transId = rs.getLong("TRANS_ID");
                if (transactionList.size() == 0 || transId != lastTransId) {
                    String transType = rs.getString("TRANS_TYPE");
                    String status = rs.getString("STATUS");
                    int totalPaid = rs.getInt("TOTAL_PAID");
                    String currency = rs.getString("CURRENCY");
                    DebitCreditType debitCredit = null;
                    
                    // calculate balance
                    if (transType.equals(Invoice.TRANS_REDEEM_GAME) ||
                    		transType.equals(Invoice.TRANS_REDEEM_POINTS) ||
                    		transType.equals(Invoice.TRANS_REDEEM_SUBSCRIPTION) ||
                    		transType.equals(Invoice.TRANS_TRANSFER_TO)) {
                        debitCredit = DebitCreditType.Credit;
                    } else {
                        debitCredit = DebitCreditType.Debit;
                    }
                    
                    int balanceInt = (int) balance;
                    int itemId = rs.getInt("ITEM_ID");
                    log.debug("item_id = " + itemId);
                    TitleType title = null;
                    RatingType rating = null;
                    PricingType pricing = null;
                    String productCode = null;
                    if (!rs.wasNull()) {
                        stmt2.setInt(1, itemId);
                        ResultSet rs2 = stmt2.executeQuery();
                        if (rs2.next()) {
                            productCode = rs2.getString("PRODUCT_CODE");
                            title = new TitleType();
                            title.setTitleId(ECommerceFunction.formatTitleId(rs2.getLong("TITLE_ID")));
                            title.setTitleName(rs2.getString("TITLE"));
                            title.setTitleSize(rs2.getLong("TITLE_SIZE"));
                            title.setFsSize(rs2.getLong("APPROX_SIZE"));
                            title.setPlatform(rs2.getString("PLATFORM"));
                            title.setCategory(rs2.getString("CATEGORY"));
                            // Get ratings
                            String ratingSystem = rs2.getString("RATING_SYSTEM");
                            String ratingValue = rs2.getString("RATING_VALUE");
                            if (ratingSystem != null && !ratingSystem.equals("")) {
                                rating = new RatingType(ratingSystem, ratingValue);
                            }
                            // Get pricing
                            String limitKind = rs2.getString("RTYPE");
                            int limit = rs2.getInt("LIMITS");
                            currency = rs2.getString("ITEM_CURRENCY");
                            BigDecimal price = rs2.getBigDecimal("ITEM_PRICE");
                            PriceType productPrice = new PriceType(price.toString(), currency);
                            String productType = rs2.getString("LICENSE_TYPE");
                            PricingCategoryType pricingCategoryType = ECommerceFunction.getPricingCategoryType(productType);
                            
                            LimitType[] limits = null;
                            if (limitKind != null) {
                                limits = new LimitType[] { new LimitType(limit, limitKind) };
                            }
                            pricing = new PricingType(itemId, limits, productPrice, pricingCategoryType);
                        }
                        rs2.close();
                        rs2 = null;
                    }
                    TransactionType transaction = new TransactionType(
                            transId,
                            rs.getLong("TRANS_DATE"),
                            rs.getString("TRANS_TYPE"),
                            rs.getString("ACCOUNT_ID"),
                            rs.getLong("DEVICE_ID"),
                            rs.getString("STATUS"),
                            rs.getLong("STATUS_DATE"),
                            String.valueOf(rs.getDouble("TOTAL_AMOUNT")),
                            String.valueOf(rs.getDouble("TOTAL_TAXES")),
                            String.valueOf(rs.getDouble("TOTAL_PAID")),
                            rs.getString("CURRENCY"),
                            debitCredit,
                            String.valueOf(balanceInt),
                            rs.getLong("ORIG_TRANS_ID"),
                            rs.getLong("OPERATOR_ID"),
                            rs.getLong("COUNTRY_ID"),
                            rs.getInt("ITEM_ID"),
                            productCode, title, pricing, rating,
                            String.valueOf(rs.getInt("QTY")),
                            String.valueOf(rs.getDouble("TOTAL_DISCOUNT")),
                            rs.getString("PAYMENT_TYPE"),
                            rs.getString("PAYMENT_METHOD_ID"),
                            rs.getString("PAS_AUTH_ID"),
                            rs.getString("PAYMENT_ECARD_TYPE"));

                    transactionList.add(transaction);   
                    lastTransId = transId;
                    //log.debug("Transaction[transId: " + transId + ", transType: " + transaction.getType() +
                    //        ", status: "+ status + ", total_paid: "+ transaction.getTotalPaid() +
                    //        ", debitCredit: " + debitCredit.getValue() +                          
                    //        ", balance: "+ balanceInt + ", pamentType: " + transaction.getPaymentType() +
                    //        "]");
                    
                    // calculate previous balance
                    if ("SUCCESS".equals(status)) {
                        if (DebitCreditType._Credit.equals(debitCredit.getValue())) {
                            balance = balance - totalPaid;
                        } else {
                            balance = balance + totalPaid;
                        }
                    }
                }
            }
            
            stmt2.close();
            stmt2 = null;
            casConn.close();
            casConn = null;
            rs.close();
            rs = null;
            stmt.close();
            stmt = null;
            conn.close();
            conn = null;
            
            if (transactionList.size() > 0) {
                TransactionType[] transactions = new TransactionType[transactionList.size()];
                transactionList.toArray(transactions);
                listTransactionsResponse.setTransactions(transactions);
            } 
        } finally {
            if (stmt2 != null) {
                try { stmt2.close(); } catch (SQLException e) {}
            }
            if (casConn != null) {
                try { casConn.close(); } catch (SQLException e) {}
            }
            if (stmt != null) {
                try { stmt.close(); } catch (SQLException e) {}
            }
            if (conn != null) {
                try { conn.close(); } catch (SQLException e) {}
            }
        }
    }

    /**
     * Returns a list of transactions
     */
    public static ListTransactionsResponseType listTransactions(
            ECommerceServiceImpl service,
            ListTransactionsRequestType listTransactionsRequest) 
    {
        ListTransactionsResponseType response = new ListTransactionsResponseType();
        ListTransactions listTransactions = new ListTransactions(service, listTransactionsRequest, response);
        listTransactions.process();
        return response;
    }
}

package com.broadon.ecs;

import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.List;

import com.broadon.ecs.ECommerceFunction;
import com.broadon.exception.BroadOnException;
import com.broadon.util.BackingStoreException;
import com.broadon.wsapi.ecs.PurchasePointsRequestType;
import com.broadon.wsapi.ecs.PurchasePointsResponseType;
import com.broadon.wsapi.pas.BalanceType;

/**
 * The <code>PurchasePointssItem</code> class handles the
 * purchase of points.
 *
 * @version $Revision: 1.13 $
 */
public class PurchasePointsItem extends PurchaseItem {

    final PurchasePointsRequestType purchasePointsRequest;
    final PurchasePointsResponseType purchasePointsResponse;

    public PurchasePointsItem(ECommerceFunction func)
        throws BackingStoreException
    {
        super(func, Invoice.TRANS_PURCHASE_POINTS);
        purchasePointsRequest = (PurchasePointsRequestType) request;
        purchasePointsResponse = (PurchasePointsResponseType) response;
        captureNeeded = false;
        accountRequired = true;
    }

    /**
     * Get information on the item to purchase (i.e. number of points)
     */
    protected PurchaseItemInfo getPurchaseItem()
        throws SQLException, BackingStoreException
    {
        long requestedPoints = purchasePointsRequest.getPoints();
        int itemId = purchasePointsRequest.getItemId();
        PurchaseItemInfo item = null;

        /* Find corresponding item for this country */
        List items = (List) service.getPurchaseItemsCache().get(new Integer(itemId));
        boolean found = false;
        if (items != null) {
            for (int i = 0; i < items.size(); i++) {
                item = (PurchaseItemInfo) items.get(i);
                if (!checkDeviceType(itemId, item.deviceType)) {
                    return null;
                }

                if (checkCountryCode(itemId, item.country)) {
                    found = true;
                    break;
                }
            }
        }

        if (!found) {
            return null;
        }
            
        // Validate item
        if (item.points == null) {
            setErrorCode(
                    StatusCode.ECS_BAD_LICENSE_TYPE, 
                    "Purchase item " + itemId + " is not for points");
            return null;
        }

        if (item.points.longValue() != requestedPoints) {
            setErrorCode(
                    StatusCode.ECS_POINTS_MISMATCH, 
                    "Purchase item " + itemId + " is for " + item.points +
                    " points, not " + requestedPoints + " points");
            return null;
        }
        return item;
    }

    /**
     * The item has been paid for, use PAS to add points to account
     * @param request
     * @param response
     * @return
     */
    protected boolean deliverItem(Payment payment, PurchaseItemInfo purchaseItem)
        throws SQLException, BroadOnException
    {
        AccountPayment acct = new AccountPayment(
            service.getPasSOAP(),
            purchasePointsRequest.getAccount());
        acct.setDepositPoints(purchasePointsRequest.getPoints());
        
        try {
            //boolean ok = acct.deposit(request, payment);
            boolean ok = payment.captureToAccount(request, acct);
            if (ok) {
                BalanceType balance = acct.getBalance();
                if (balance != null) { 
                    long points = Long.parseLong(balance.getAvailable().getAmount());
                    purchasePointsResponse.setTotalPoints(points);
                }
            } else {
                response.setErrorCode(payment.errorCode);
                response.setErrorMessage(payment.errorMessage);
            }
            return ok;
        } catch (RemoteException e) {
            setErrorCode(StatusCode.ECS_PAS_ERROR, e.getMessage());
            log.error("Error depositing points using PAS", e);            
            return false;
        }
    }
    
}

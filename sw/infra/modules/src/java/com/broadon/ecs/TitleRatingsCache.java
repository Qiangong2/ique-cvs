package com.broadon.ecs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.broadon.db.DataSourceScheduler;
import com.broadon.util.BackingStore;
import com.broadon.util.BackingStoreException;
import com.broadon.util.CacheLFU;
import com.broadon.wsapi.ecs.RatingType;

/**
 * The <code>TitleRatingsCache</code> class provides a cache
 * for an array of title ratings (RatingType[]) using the 
 * titleId and countryId as a key (TitleIdKey).
 *
 * @version $Revision: 1.4 $
 */
public class TitleRatingsCache 
    extends CacheLFU implements BackingStore 
{
    private ECommerceServiceImpl service;

    private static final String SELECT_TITLE_RATINGS = 
        "SELECT RATING_SYSTEM, RATING_VALUE " +
        "FROM CONTENT_TITLE_RATINGS WHERE TITLE_ID=? AND COUNTRY_ID=?";

    public TitleRatingsCache(ECommerceServiceImpl service) {
        super(service.getContentCacheTimeoutMsecs());
        this.service = service;
    }

    public Object retrieve(Object key) throws BackingStoreException
    {
        TitleIdKey titleIdKey = (TitleIdKey) key;
        long titleId = titleIdKey.titleId;
        int countryId = titleIdKey.country.countryId; 
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection conn = null;
        try {
            conn = service.getConnection(service.getCasDataSource(), DataSourceScheduler.DB_PRIORITY_FAST);
            ps = conn.prepareStatement(SELECT_TITLE_RATINGS);
            ps.setLong(1, titleId);
            ps.setInt(2, countryId);
            rs = ps.executeQuery();
            List ratingList = new ArrayList();
            while (rs.next()) {
                RatingType rating = new RatingType();
                int i = 1;
                rating.setName(rs.getString(i++));
                rating.setRating(rs.getString(i++));
                ratingList.add(rating);
            }
            
            if (ratingList.size() > 0) {
                RatingType[] ratings = new RatingType[ratingList.size()];
                return (RatingType[]) ratingList.toArray(ratings);
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw BackingStoreException.rewrap(e);            
        } finally {
            if (rs != null) {
                try { rs.close(); } catch (SQLException e) {}
            }
            if (ps != null) {
                try { ps.close(); } catch (SQLException e) {}
            }
            if (conn != null) {
                try { conn.close(); } catch (SQLException e) {}
            }
        }
    }
    
}

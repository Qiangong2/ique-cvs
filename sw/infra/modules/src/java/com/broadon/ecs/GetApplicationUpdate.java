package com.broadon.ecs;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.logging.Log;

import com.broadon.db.DataSourceScheduler;
import com.broadon.wsapi.ecs.GetApplicationUpdateRequestType;
import com.broadon.wsapi.ecs.GetApplicationUpdateResponseType;
import com.broadon.wsapi.ecs.TitleVersionType;

/**
 * The <code>GetApplicationUpdate</code> class implements
 * the ECS GetApplicationUpdate function.
 * 
 * Returns list of application titles that has been updated.
 *
 * @version $Revision: 1.5 $
 */
public class GetApplicationUpdate extends ECommerceFunction {
    // Uses apache commons logger (follows axis logging)
    protected static Log log = LogFactory.getLog(GetApplicationUpdate.class.getName());

    private static final String SELECT_ETICKET_LIST =
        "SELECT content_id FROM etickets WHERE revoke_date is null"; 

    private static final String SELECT_TITLE_LIST =
        "SELECT BCCUTIL.DEC2HEX(TITLE_ID, 16), TITLE_VERSION, APPROX_SIZE " +
        "FROM CONTENT_TITLE_CATALOG WHERE ETKM_CONTENT_ID=?";

    public GetApplicationUpdate(
            ECommerceServiceImpl service, 
            GetApplicationUpdateRequestType request,
            GetApplicationUpdateResponseType response)
    {
        super("GetApplicationUpdate", service, request, response, log);        
    }
    
    private List getETicketList()
        throws SQLException
    {
        Connection conn = null;
        Statement stmt = null;
        List eTicketList = null;
        try {
            conn = service.getConnection(service.getEcsDataSource(), DataSourceScheduler.DB_PRIORITY_MEDIUM); 
            // Get eticket list            
            String sql = SELECT_ETICKET_LIST + " AND DEVICE_ID=" + getDevice().getDeviceId();
            
            stmt = conn.createStatement();            
            ResultSet rs = stmt.executeQuery(sql);
            eTicketList = new ArrayList();
            while (rs.next()) {
                eTicketList.add(rs.getBigDecimal(1));
                
            }            
        } finally {
            if (stmt != null) {
                try { stmt.close(); } catch (SQLException e) {}
            }
            if (conn != null) {
                try { conn.close(); } catch (SQLException e) {}
            }
        }
        return eTicketList;
    }
    
    private TitleVersionType[] getUpdatedTitles(List eTicketList, Long sinceUpdateTimestamp)
        throws SQLException
    {
        if (eTicketList == null) {
            return null;
        }
        
        Connection conn = null;
        PreparedStatement stmt = null;
        List titleList = null;
        
        try {
            conn = service.getConnection(service.getCasDataSource(), DataSourceScheduler.DB_PRIORITY_MEDIUM); 
            // Get title list            
            String sql = SELECT_TITLE_LIST;
            if (sinceUpdateTimestamp != null) {
                 sql += " AND BCCUTIL.ConvertTimeStamp(TITLE_VERSION_DATE) > " + sinceUpdateTimestamp;
            }
            
            stmt = conn.prepareStatement(sql);
            titleList = new ArrayList(eTicketList.size());
            for (int i = 0; i < eTicketList.size(); i++) {
                BigDecimal etkmContentId = (BigDecimal) eTicketList.get(i);
                stmt.setBigDecimal(1, etkmContentId);
                ResultSet rs = stmt.executeQuery();
                if (rs.next()) {
                    TitleVersionType title = new TitleVersionType();
                    title.setTitleId(rs.getString(1));
                    title.setVersion(rs.getInt(2));
                    title.setFsSize(new Long(rs.getLong(3)));
                    titleList.add(title);
                }                
            }
        } finally {
            if (stmt != null) {
                try { stmt.close(); } catch (SQLException e) {}
            }
            if (conn != null) {
                try { conn.close(); } catch (SQLException e) {}
            }
        }
         
        if (titleList != null && titleList.size() > 0) {
            TitleVersionType[] titles = new TitleVersionType[titleList.size()];
            titles = (TitleVersionType[]) titleList.toArray(titles);
            return titles;
        } else {
            return null;
        }
    }
    
    public void processImpl()
        throws SQLException
    {
        GetApplicationUpdateRequestType getApplicationUpdateRequest = (GetApplicationUpdateRequestType) request;
        GetApplicationUpdateResponseType getApplicationUpdateResponse = (GetApplicationUpdateResponseType) response;

        getApplicationUpdateResponse.setContentPrefixURL(service.getContentPrefixUrl());
        getApplicationUpdateResponse.setUncachedContentPrefixURL(service.getUncachedContentPrefixUrl());
        List eTicketList = getETicketList();
        TitleVersionType[] titles = getUpdatedTitles(
                eTicketList, 
                getApplicationUpdateRequest.getLastTitleUpdateTime());
        getApplicationUpdateResponse.setUpdatedTitles(titles);
    }
    
    /**
     * Returns the ApplicationUpdate information for the device
     * @param service
     * @param getApplicationUpdateRequest
     * @return
     */
    public static GetApplicationUpdateResponseType getApplicationUpdate(
            ECommerceServiceImpl service,
            GetApplicationUpdateRequestType getApplicationUpdateRequest) 
    {
        GetApplicationUpdateResponseType response = new GetApplicationUpdateResponseType();
        GetApplicationUpdate getApplicationUpdate = new GetApplicationUpdate(service, getApplicationUpdateRequest, response);
        getApplicationUpdate.process();
        return response;
    }
}

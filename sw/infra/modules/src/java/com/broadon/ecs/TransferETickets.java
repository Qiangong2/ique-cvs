package com.broadon.ecs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.db.DataSourceScheduler;

import com.broadon.wsapi.ecs.*;

/**
 * The <code>TransferETickets</code> class implements
 * the ECS TransferETickets function.
 * 
 * @version $Revision: 1.2 $
 */
public class TransferETickets extends ECommerceFunction {
        
    protected static String COPY_ETICKETS_TO_TRANSFER = 
        "INSERT INTO ETICKET_TRANSFERS " +
        "  (REQUEST_DATE, NEW_SERIAL_NO, CONTENT_ID, DEVICE_ID, " +
        "      CREATE_DATE, BIRTH_TYPE, REVOKE_DATE, RTYPE, TOTAL_LIMITS, TID, MASK, LICENSE_TYPE) " +
        "  SELECT SYS_EXTRACT_UTC(CURRENT_TIMESTAMP), ?, CONTENT_ID, DEVICE_ID, " +
        "      CREATE_DATE, BIRTH_TYPE, REVOKE_DATE, RTYPE, TOTAL_LIMITS, TID, MASK, LICENSE_TYPE" +
        "   FROM ETICKETS WHERE DEVICE_ID=?";

    protected static String REMOVE_ETICKETS =
        "DELETE FROM ETICKETS WHERE DEVICE_ID=?";
    
    protected static String TRANSFER_ETICKETS_TO_DEVICE =
        "INSERT INTO ETICKETS " +
        "  (CONTENT_ID, DEVICE_ID, CREATE_DATE, BIRTH_TYPE, REVOKE_DATE, " +
        "      RTYPE, TOTAL_LIMITS, TID, MASK, LICENSE_TYPE) " +
        "  SELECT CONTENT_ID, ?, CREATE_DATE, BIRTH_TYPE, REVOKE_DATE, " +
        "      RTYPE, TOTAL_LIMITS, TID, MASK, LICENSE_TYPE" +
        "   FROM ETICKET_TRANSFERS WHERE DEVICE_ID=? AND NEW_SERIAL_NO=? AND PROCESS_DATE IS NULL"; 
    
    protected static String MARK_ETICKET_TRANSFER_DONE =
        "UPDATE ETICKET_TRANSFERS SET PROCESS_DATE=SYS_EXTRACT_UTC(CURRENT_TIMESTAMP), NEW_DEVICE_ID=? " +
        "    WHERE DEVICE_ID=? AND NEW_SERIAL_NO=?";
    
    protected static String FILTER_BY_TID = " AND TID=?";
    
    // Uses apache commons logger (follows axis logging)
    protected static Log log = 
        LogFactory.getLog(TransferETickets.class.getName());

    public TransferETickets(
            ECommerceServiceImpl service, 
            TransferETicketsRequestType request,
            TransferETicketsResponseType response)
    {
        super("TransferETickets", service, request, response, log);        
    }
    
    private static int createETicketTransferRecords(Connection conn, String serial, long oldDevice)
        throws SQLException
    {
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(COPY_ETICKETS_TO_TRANSFER);      
            int i = 1;
            ps.setString(i++, serial);
            ps.setLong(i++, oldDevice);
            int count = ps.executeUpdate();
            return count;
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }
    
    private static int createETicketTransferRecords(Connection conn, String serial, long oldDevice,
                                                    long eTickets[])
        throws SQLException
    {
        if (eTickets == null) {
            return createETicketTransferRecords(conn, serial, oldDevice);
        }
        
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(COPY_ETICKETS_TO_TRANSFER + FILTER_BY_TID);
            int count = 0;
            for (int j=0; j < eTickets.length; j++) {
                int i = 1;
                ps.setString(i++, serial);
                ps.setLong(i++, oldDevice);
                ps.setLong(i++, eTickets[j]);
                count += ps.executeUpdate();
            }
            return count;
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }
    
    private static int transferETicketsToDevice(Connection conn, String serial, long oldDevice, long newDevice)
        throws SQLException
    {
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(TRANSFER_ETICKETS_TO_DEVICE);  
            int i = 1;
            ps.setLong(i++, newDevice);
            ps.setLong(i++, oldDevice);            
            ps.setString(i++, serial);
            int count = ps.executeUpdate();
            return count;
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }
    
    private static int transferETicketsToDevice(Connection conn, String serial, long oldDevice, long newDevice,
                                                long[] eTickets)
        throws SQLException
    {
        if (eTickets == null) {
            return transferETicketsToDevice(conn, serial, oldDevice, newDevice);
        }
        
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(TRANSFER_ETICKETS_TO_DEVICE + FILTER_BY_TID);   
            int count = 0;
            for (int j=0; j < eTickets.length; j++) {
                int i = 1;
                ps.setLong(i++, newDevice);
                ps.setLong(i++, oldDevice);            
                ps.setString(i++, serial);
                ps.setLong(i++, eTickets[j]);
                count += ps.executeUpdate();
            }
            return count;
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }
    
    private static int markETicketTransferDone(Connection conn, String serial, long oldDevice, long newDevice)
        throws SQLException
    {
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(MARK_ETICKET_TRANSFER_DONE);   
            int i = 1;
            ps.setLong(i++, newDevice);
            ps.setLong(i++, oldDevice);            
            ps.setString(i++, serial);
            int count = ps.executeUpdate();
            return count;
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }
    
    private static int markETicketTransferDone(Connection conn, String serial, long oldDevice, long newDevice,
                                                long[] eTickets)
        throws SQLException
    {
        if (eTickets == null) {
            return markETicketTransferDone(conn, serial, oldDevice, newDevice);
        }
        
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(MARK_ETICKET_TRANSFER_DONE + FILTER_BY_TID);   
            int count = 0;
            for (int j=0; j < eTickets.length; j++) {
                int i = 1;
                ps.setLong(i++, newDevice);
                ps.setLong(i++, oldDevice);            
                ps.setString(i++, serial);
                ps.setLong(i++, eTickets[j]);
                count = ps.executeUpdate();
            }
            return count;                
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }
    
    private static int removeOldETickets(Connection conn, long oldDevice)
        throws SQLException
    {
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(REMOVE_ETICKETS);   
            int i = 1;
            ps.setLong(i++, oldDevice);
            int count = ps.executeUpdate();
            return count;
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }
    
    private static int removeOldETickets(Connection conn, long oldDevice,
                                         long[] eTickets)
        throws SQLException
    {
        if (eTickets == null) {
            return removeOldETickets(conn, oldDevice);
        }
        
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(REMOVE_ETICKETS + FILTER_BY_TID);   
            int count = 0;
            for (int j=0; j < eTickets.length; j++) {
                int i = 1;
                ps.setLong(i++, oldDevice);
                ps.setLong(i++, eTickets[j]);
                count = ps.executeUpdate();
            }
            return count;
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }
    
    public void processImpl()
        throws SQLException
    {
        TransferETicketsRequestType transferETicketsRequest = (TransferETicketsRequestType) request;
        TransferETicketsResponseType transferETicketsResponse = (TransferETicketsResponseType) response;

        Connection conn = null;
        DataSource dataSource = service.getEcsDataSource();

        try {
            int nTransferred = 0;
            long oldDevice = getDevice().getDeviceId();
            Long newDevice = transferETicketsRequest.getTargetDeviceId();
            String oldSerial = transferETicketsRequest.getSourceSerialNo();
            String newSerial = transferETicketsRequest.getTargetSerialNo();
            long[] eTicketIds = transferETicketsRequest.getTickets(); 
            
            conn = service.getConnection(dataSource, DataSourceScheduler.DB_PRIORITY_MEDIUM);

            // Transfer ETickets
            conn.setAutoCommit(false);

            if (newSerial == null) {
                setErrorCode(StatusCode.ECS_TARGET_SERIAL_NUMBER_MISSING, null);
                return;
            }
            
            if (newSerial.equals(oldSerial)) {
                /* Pending transfer */
                if (newDevice == null) {
                    setErrorCode(StatusCode.ECS_TARGET_DEVICE_ID_MISSING, null);
                    return;
                }
                nTransferred = transferETicketsToDevice(conn, newSerial, oldDevice, newDevice.longValue(), eTicketIds);
                markETicketTransferDone(conn, newSerial, oldDevice, newDevice.longValue(), eTicketIds);
            } else {
                /* New transfer */
                if (newDevice != null && newDevice.longValue() == oldDevice) {
                    // Device ids are the same, nothing to do
                    setErrorCode(StatusCode.ECS_DEVICE_IDS_SAME, null);
                    return;
                }
            
                createETicketTransferRecords(conn, newSerial, oldDevice, eTicketIds);
                nTransferred = removeOldETickets(conn, oldDevice, eTicketIds);
                if (newDevice != null) {
                    nTransferred = transferETicketsToDevice(conn, newSerial, oldDevice, newDevice.longValue(), eTicketIds);
                    markETicketTransferDone(conn, newSerial, oldDevice, newDevice.longValue(), eTicketIds);
                }
            } 
           
            conn.commit();
            transferETicketsResponse.setTicketsTransferred(nTransferred);
            log.debug("Transferred " + nTransferred + " etickets from device " + oldDevice + ", serial " + oldSerial 
                    + " to device " + newDevice + ", serial " + newSerial);
        } finally {
            try {
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            } catch (SQLException e) {
               log.warn("Error closing connection", e);
            }
            
        }
    }

    public static TransferETicketsResponseType transferETickets(
            ECommerceServiceImpl service,
            TransferETicketsRequestType transferETicketsRequest) 
    {
        TransferETicketsResponseType response = new TransferETicketsResponseType();
        TransferETickets transferETickets = new TransferETickets(service, transferETicketsRequest, response);
        transferETickets.process();
        return response;
    }
}

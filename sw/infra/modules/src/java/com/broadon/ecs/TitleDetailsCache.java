package com.broadon.ecs;
 
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.logging.Log;

import com.broadon.db.DBException;
import com.broadon.db.DataSourceScheduler;
import com.broadon.util.BackingStore;
import com.broadon.util.BackingStoreException;
import com.broadon.util.CacheExp;
import com.broadon.util.Pair;
import com.broadon.wsapi.ecs.ContentInfoType;
import com.broadon.wsapi.ecs.LimitType;
import com.broadon.wsapi.ecs.PriceType;
import com.broadon.wsapi.ecs.PricingCategoryType;
import com.broadon.wsapi.ecs.PricingType;
import com.broadon.wsapi.ecs.RatingType;
import com.broadon.wsapi.ecs.TitleInfoType;
import com.broadon.wsapi.ecs.TitleKindType;

public class TitleDetailsCache extends CacheExp implements BackingStore
{
    // NOTE:  When we no longer need to check for purchase_end_date, then we should
    // switch to use a less expensive Cache implmentation (e.g., CacheLFU).
    //
    private static final String SELECT_TITLE_DETAILS = 
        "SELECT BCCUTIL.ConvertTimeStamp(PURCHASE_END_DATE), " +
        "       TITLE_VERSION, " +
        "       ITEM_ID, TITLE, 'GAME' AS TITLE_TYPE, " +
        "       DESCRIPTION, TITLE_SIZE, APPROX_SIZE, TMD_CONTENT_ID, RTYPE, " +
        "       LIMITS, ITEM_PRICE AS PRICE, ITEM_CURRENCY AS CURRENCY, LICENSE_TYPE, CATEGORY, " +
        "       RATING_SYSTEM, RATING_VALUE, SECOND_SYSTEM, SECOND_VALUE " +
        "FROM CURRENT_GAME_PRICING_CATALOG WHERE COUNTRY_ID=? AND TITLE_ID=? AND LANGUAGE_CODE=?";
    
    private static final String SELECT_SUBS_TITLE_DETAILS = 
        "SELECT BCCUTIL.ConvertTimeStamp(PURCHASE_END_DATE), " +
        "       TITLE_VERSION, " +
        "       0 as ITEM_ID, TITLE, TITLE_TYPE, " +
        "       DESCRIPTION, TITLE_SIZE, APPROX_SIZE, TMD_CONTENT_ID, 'SR' as RTYPE, " +
        "       0 as LIMITS, 0 as PRICE, ' ' as CURRENCY, 'SUBSCRIPT' as LICENSE_TYPE, CATEGORY, " +
        "       RATING_SYSTEM, RATING_VALUE, SECOND_SYSTEM, SECOND_VALUE " +
        "FROM SUBSCRIPTION_CONTENT_CATALOG WHERE " +
        "      COUNTRY_ID=? AND TITLE_ID=? AND " +
            ECommerceFunction.FILTER_BY_PURCHASE_DATE;
    
    private ECommerceServiceImpl service;

    //  Uses apache commons logger (follows axis logging)
    private static Log log = LogFactory.getLog(ListTitles.class.getName());

    public TitleDetailsCache(ECommerceServiceImpl service) {
        super(400, service.getContentCacheTimeoutMsecs());
        this.service = service;
    }


    public Object retrieve(Object titleIdKey) throws BackingStoreException
    {
        TitleIdKey titleKey = (TitleIdKey) titleIdKey;
        long titleId = titleKey.titleId;
        int countryId = titleKey.country.countryId;
        String language = titleKey.language;
        
        Connection conn = null;
        PreparedStatement stmt = null;

        try {
            TitleInfoType titleInfo = new TitleInfoType();
            titleInfo.setTitleId(ECommerceFunction.formatTitleId(titleId));
            List pricingList = new ArrayList();
            List ratingList = new ArrayList();
            RatingType[] ratings = null;
            long expDate = Long.MAX_VALUE;
            
            conn = service.getConnection(service.getCasDataSource(), DataSourceScheduler.DB_PRIORITY_FAST);
            // Get Title
            String sql = SELECT_TITLE_DETAILS + " UNION ALL " + SELECT_SUBS_TITLE_DETAILS;
            stmt = conn.prepareStatement(sql);
            int i = 1;
            stmt.setInt(i++, countryId);
            stmt.setLong(i++, titleId);
            stmt.setString(i++, language);
            stmt.setInt(i++, countryId);
            stmt.setLong(i++, titleId);
            
            ResultSet rs = stmt.executeQuery();
           
            while (rs.next()) {
                if (pricingList.size() == 0) {     
                    // NOTE: This get title name and description from first entry, may want to choose language
                    int version = rs.getInt(2);
                    String titleKindStr = rs.getString("TITLE_TYPE");
                    String titleName = rs.getString("TITLE");
                    String titleDesc = rs.getString("DESCRIPTION");
                    String category = rs.getString("CATEGORY");
                    long titleSize = rs.getLong("TITLE_SIZE");
                    long approxSize = rs.getLong("APPROX_SIZE");

                    TitleKindType titleKind = ECommerceFunction.getTitleKindType(titleKindStr);
                    titleInfo.setTitleName(titleName);
                    titleInfo.setTitleKind(titleKind);
                    titleInfo.setTitleDescription(titleDesc);
                    titleInfo.setTitleSize(titleSize);
                    titleInfo.setFsSize(approxSize);
                    titleInfo.setCategory(category);
                    titleInfo.setVersion(version);

                    // Set contents to null (deprecated)
                    ContentInfoType[] contents = null; //service.getTitleCatalog().getContents(titleId);
                    titleInfo.setContents(contents);

                    // Get ratings
                    String ratingSystem = rs.getString("RATING_SYSTEM");
                    String ratingValue = rs.getString("RATING_VALUE");
                    if (ratingSystem != null) {
                        ratingList.add(new RatingType(ratingSystem, ratingValue));
                    }

                    ratingSystem = rs.getString("SECOND_SYSTEM");
                    ratingValue = rs.getString("SECOND_VALUE");
                    if (ratingSystem != null) {
                        ratingList.add(new RatingType(ratingSystem, ratingValue));
                    }
                    ratings = new RatingType[ratingList.size()];
                    ratings = (RatingType[]) ratingList.toArray(ratings);
                }

                long endDate = rs.getLong(1);
                if (! rs.wasNull()) {
                    if (endDate < expDate)
                        expDate = endDate;
                }
                
                // Get pricing
                String limitKind = rs.getString("RTYPE");
                int itemId = rs.getInt("ITEM_ID");
                int limit = rs.getInt("LIMITS");
                String currency = rs.getString("CURRENCY");
                BigDecimal price = rs.getBigDecimal("PRICE");
                PriceType productPrice = new PriceType(price.toString(), currency);
                String productType = rs.getString("LICENSE_TYPE");
                PricingCategoryType pricingCategoryType = ECommerceFunction.getPricingCategoryType(productType);

                LimitType[] limits = null;
                if (limitKind != null) {
                    limits = new LimitType[] { new LimitType(limit, limitKind) };
                }
                pricingList.add(new PricingType(itemId, limits, productPrice,
                                                pricingCategoryType));
            }
            rs.close();
            
            // release resource ASAP, before any possible GC.
            stmt.close();
            stmt = null;
            conn.close();
            conn = null;

            //ratings = service.getTitleCatalog().getTitleRatings(titleKey);
            if (pricingList.size() > 0) {
                PricingType[] pricingTypes = new PricingType[pricingList.size()];
                pricingTypes = (PricingType[]) pricingList.toArray(pricingTypes);
                Long expirationDate = (expDate == Long.MAX_VALUE) ? null : new Long(expDate);
                return new Pair(expirationDate, 
                                new CachedTitleDetailsResponse(titleInfo, pricingTypes, ratings));
            } else {
                titleInfo = service.getTitleCatalog().getTitleInfo(titleKey);
                return new Pair(null, new CachedTitleDetailsResponse(titleInfo, null, ratings));
            }
        } catch (BackingStoreException e) {
            log.error("GetTitleDetails error", e);
            throw e;
        } catch (DBException e) {
            log.error("GetTitleDetails error", e);
            throw new BackingStoreException(e.getMessage(), e);
        } catch (SQLException e) {
            log.error("GetTitleDetails error", e);
            throw BackingStoreException.rewrap(e);
        } finally {
            if (stmt != null) {
                try { stmt.close(); } catch (SQLException e) {}
            }
            if (conn != null) {
                try { conn.close(); } catch (SQLException e) {}
            }
        }
    }
    
    public static class CachedTitleDetailsResponse
    {
        public final TitleInfoType titleInfo;
        public final PricingType[] titlePricings;
        public final RatingType[] ratings;
        
        public CachedTitleDetailsResponse(TitleInfoType info, PricingType[] pricings, RatingType[] ratings) {
            this.titlePricings = pricings;
            this.titleInfo = info;
            this.ratings = ratings;
        }
    }
}

package com.broadon.ecs;

import com.broadon.exception.BroadOnException;

/**
 * The <code>InvalidPriceException</code> class is thrown when
 * the price is invalid.  A price is composed of a currency (string)
 * and a amount (decimal represented as a string)
 * @version	$Revision: 1.1 $
 */
public class InvalidPriceException
    extends BroadOnException
{
    /**
     * Constructs a InvalidPriceException instance.
     */
    public InvalidPriceException()
    {
	super();
    }

    /**
     * Constructs a InvalidPriceException instance.
     *
     * @param	message			the exception message
     */
    public InvalidPriceException(String message)
    {
	super(message);
    }

    /**
     * Constructs a InvalidPriceException instance.
     *
     * @param	message			the exception message
     * @param	throwable		the nested exception
     */
    public InvalidPriceException(String message, Throwable throwable)
    {
	super(message, throwable);
    }
}

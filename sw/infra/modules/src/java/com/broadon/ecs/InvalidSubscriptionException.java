package com.broadon.ecs;

import com.broadon.db.DBException;

/**
 * The <code>InvalidSubscriptionException</code> class is thrown when
 * a subscribe request is rejected because the specified 
 * subscription information is invalid.
 * 
 * @version	$Revision: 1.2 $
 */
public class InvalidSubscriptionException
    extends DBException
{
    /**
     * Constructs a InvalidSubscriptionException instance.
     */
    public InvalidSubscriptionException()
    {
	super();
    }

    /**
     * Constructs a InvalidSubscriptionException instance.
     *
     * @param	message			the exception message
     */
    public InvalidSubscriptionException(String message)
    {
	super(message);
    }

    /**
     * Constructs a InvalidSubscriptionException instance.
     *
     * @param	message			the exception message
     * @param	throwable		the nested exception
     */
    public InvalidSubscriptionException(String message, Throwable throwable)
    {
	super(message, throwable);
    }
}

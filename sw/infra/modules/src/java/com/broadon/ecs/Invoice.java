package com.broadon.ecs;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The <code>Invoice</code> class provides functionality
 * for doing the invocing for a transaction.
 * 
 * @version $Revision: 1.19 $
 */
public class Invoice {
    
    public static String STATUS_OPEN = "OPEN";
    public static String STATUS_AUTH = "AUTH";
    public static String STATUS_CAP = "CAPTURE";
    public static String STATUS_VOID = "VOID";
    public static String STATUS_SUCCESS = "SUCCESS";    
    public static String STATUS_FAILED = "FAIL";    
    
    public static String TRANS_PURCHASE_POINTS = "PURCHPTS";
    public static String TRANS_PURCHASE_GAME = "PURCHGAME";
    public static String TRANS_PURCHASE_SUBSCRIPTION = "PURCHSUBS";
    public static String TRANS_REDEEM_POINTS = "REDEEMPTS";
    public static String TRANS_REDEEM_GAME = "REDEEMGAME";
    public static String TRANS_REDEEM_SUBSCRIPTION = "REDEEMSUBS";
    public static String TRANS_TRANSFER_TO = "XFERTO";
    public static String TRANS_TRANSFER_FROM = "XFERFROM";
    
    private static final String GET_NEXT_TRANSID =
        "select trans_id_seq.nextval from dual";

    private static final String NEW_PAYMENT =
        "INSERT INTO ECS_PAYMENTS " +
        " (TRANS_ID, PAYMENT_SEQ, PAYMENT_TYPE, PAYMENT_AMOUNT, STATUS, STATUS_DATE, PAYMENT_METHOD_ID, PAYMENT_ECARD_TYPE) " +
        " VALUES (?, ?, ?, ?, ?, SYS_EXTRACT_UTC(CURRENT_TIMESTAMP), ?, ?)";

    private static final String UPDATE_PAYMENT =
        "UPDATE ECS_PAYMENTS SET STATUS=?, STATUS_DATE=SYS_EXTRACT_UTC(CURRENT_TIMESTAMP), PAS_AUTH_ID=? " +
        " WHERE TRANS_ID=? AND PAYMENT_SEQ=?";

    private static final String NEW_ITEM =
        "INSERT INTO ECS_TRANSACTION_ITEMS " +
        " (TRANS_ID, ITEM_SEQ, ITEM_ID, QTY, DISCOUNT, TAXES) " +
        " VALUES (?, ?, ?, ?, ?, ?)";

    private static final String NEW_TRANSACTION =
        "INSERT INTO ECS_TRANSACTIONS " +
        " (TRANS_ID, TRANS_DATE, TRANS_TYPE, DEVICE_ID, SERIAL_NO, CLIENT_IPADDR, " +
        " STATUS, TOTAL_AMOUNT, TOTAL_PAID, CURRENCY, COUNTRY_ID, ACCOUNT_ID, TITLE_ID) " +
        " VALUES (?, SYS_EXTRACT_UTC(CURRENT_TIMESTAMP), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    private static final String UPDATE_TRANSACTION =
        "UPDATE ECS_TRANSACTIONS SET STATUS=?, TOTAL_AMOUNT=?, TOTAL_PAID=?" +
        " WHERE TRANS_ID=?";

    private PreparedStatement itemsStmt;
    private PreparedStatement paymentStmt;
    private PreparedStatement updatePaymentStmt;
    
    private final Connection conn;
    private final long transId;
    private final String transType;
    private final long deviceId;
    
    private final String currency;
    private BigDecimal totalAmount;
    private BigDecimal totalPaid;
    private int itemSeq = 0;
    private int paymentSeq = 0;
    private int countryId = 0;
    
    private final String account;
    private final String serialNo;
    private final String clientIp;
    
    private final Long titleId;
       
    private Invoice(Connection conn, String account, int countryId, long transId,
            String currency, long deviceId, String serialNo, String ipaddr, String transType,
            Long titleId)
    {
        this.conn = conn;
        this.account = account;
        this.countryId = countryId;
        this.currency = currency;
        this.deviceId = deviceId;
        this.transId = transId;
        this.transType = transType;
        this.totalAmount = BigDecimal.valueOf(0);
        this.totalPaid = BigDecimal.valueOf(0);
        this.serialNo = serialNo;
        this.clientIp = ipaddr;
        this.titleId = titleId;
    }

    public static Invoice newInvoice(Connection conn, String account, int countryId, 
            String currency, long deviceId, String serialNo, String ipaddr, String transType,
            Long titleId)
        throws SQLException
    {
        long transId = getNextTransactionId(conn);
        Invoice invoice = new Invoice(conn, account, countryId, transId,
                                      currency, deviceId, serialNo, ipaddr, transType, titleId);
        invoice.newTransaction(STATUS_OPEN);
        return invoice;
    }
    
    protected int addPayment(String paymentMethodId, 
            String paymentType, BigDecimal amount, String status, Integer eCardType, boolean isLast)
        throws SQLException
    {
        if (paymentStmt == null) {
            paymentStmt = conn.prepareStatement(NEW_PAYMENT);            
        }
        
        int i = 1;
        
        /* Get next payment sequence */
        paymentSeq++; 

        paymentStmt.setLong(i++, transId);
        paymentStmt.setInt(i++, paymentSeq);
        paymentStmt.setString(i++, paymentType);
        paymentStmt.setBigDecimal(i++, amount);
        paymentStmt.setString(i++, status);
        paymentStmt.setString(i++, paymentMethodId);
        paymentStmt.setObject(i++, eCardType);
        paymentStmt.addBatch();
        
        totalPaid = totalPaid.add(amount);
        if (isLast) {
            addPaymentsDone();
        }
        return paymentSeq;
    }

    protected void addPaymentsDone()
        throws SQLException
    {
        if (paymentStmt != null) {
            paymentStmt.executeBatch();
            paymentStmt.close();
            paymentStmt = null;
        }
    }
    
    protected int updatePayment(int seq, String status, String authId, boolean isLast)  
        throws SQLException
    {
        if (updatePaymentStmt == null) {            
            updatePaymentStmt = conn.prepareStatement(UPDATE_PAYMENT);        
        }
        
        int i = 1;
        
        updatePaymentStmt.setString(i++, status);
        updatePaymentStmt.setString(i++, authId);
        updatePaymentStmt.setLong(i++, transId);
        updatePaymentStmt.setInt(i++, seq);
        updatePaymentStmt.executeUpdate();

        if (isLast) {
            updatePaymentsDone();
        }
        return paymentSeq;
    }
    
    protected void updatePaymentsDone()
        throws SQLException
    {
        if (updatePaymentStmt != null) {
            updatePaymentStmt.close();
            updatePaymentStmt = null;
        }
    }
    
    protected int addItem(int itemId, int qty, BigDecimal amount, BigDecimal discount, BigDecimal taxes, boolean isLast)
        throws SQLException
    {
        if (itemsStmt == null) {
            itemsStmt = conn.prepareStatement(NEW_ITEM);
        }
        
        /* Get next item sequence */
        itemSeq++;

        int i = 1;
        itemsStmt.setLong(i++, transId);
        itemsStmt.setInt(i++, itemSeq);
        itemsStmt.setInt(i++, itemId);
        itemsStmt.setInt(i++, qty);
        itemsStmt.setBigDecimal(i++, discount);
        itemsStmt.setBigDecimal(i++, taxes);
        itemsStmt.addBatch();
        
        totalAmount = totalAmount.add(amount.subtract(discount).add(taxes));
        if (isLast) {
            addItemsDone();
        }
        return itemSeq;
    }
    
    protected void addItemsDone()
        throws SQLException
    {
        if (itemsStmt != null) {
            itemsStmt.executeBatch();
            itemsStmt.close();
            itemsStmt = null;
        }
    }
    
    private void newTransaction(String status)
        throws SQLException
    {
        PreparedStatement stmt = null;
        try {
            int i = 1;
            stmt = conn.prepareStatement(NEW_TRANSACTION);
            stmt.setLong(i++, transId);
            stmt.setString(i++, transType);
            stmt.setLong(i++, deviceId);
            stmt.setString(i++, serialNo);
            stmt.setString(i++, clientIp);
            stmt.setString(i++, status);
            stmt.setBigDecimal(i++, totalAmount);
            stmt.setBigDecimal(i++, totalPaid);
            stmt.setString(i++, currency);
            stmt.setInt(i++, countryId);
            stmt.setString(i++, account);
            stmt.setObject(i++, titleId);
            stmt.executeUpdate();
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    protected void updateTransaction(String status)
        throws SQLException
    {
        PreparedStatement stmt = null;
        try {
            int i = 1;
            stmt = conn.prepareStatement(UPDATE_TRANSACTION);
            stmt.setString(i++, status);
            stmt.setBigDecimal(i++, totalAmount);
            stmt.setBigDecimal(i++, totalPaid);
            stmt.setLong(i++, transId);
            stmt.executeUpdate();
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    /**
     * Get the next transaction ID
     * @param conn
     * @return
     * @throws SQLException
     */
    protected static long getNextTransactionId(Connection conn)
        throws SQLException
    {
        long transId = 0;
        PreparedStatement stmt = null;
        try {
            stmt = conn.prepareStatement(GET_NEXT_TRANSID);
            ResultSet rs = stmt.executeQuery();
        
            if (rs != null) {
                if (rs.next()) {
                    transId = rs.getLong(1);
                }
            }
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
        return transId;
    }

    
    
    /**
     * Commit
     */
    protected void commit()
        throws SQLException
    {
        conn.commit();
    }

    protected void close()
    {
        try {
            if (paymentStmt != null) {
                paymentStmt.close();
            } 
            
        } catch (SQLException e) {
        } finally {
            paymentStmt = null;
        }
        
        try {
            if (itemsStmt != null) {
                itemsStmt.close();
            } 
        } catch (SQLException e) {
        } finally {
            itemsStmt = null;
        }

        try {
            if (updatePaymentStmt != null) {
                updatePaymentStmt.close();
            } 
        } catch (SQLException e) {
        } finally {
            updatePaymentStmt = null;
        }
        
    }
    
    /**
     * Accessor for transaction id
     */
    protected long getTransactionId()
    {
        return transId;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }
}

package com.broadon.pcis;

public class StatusCode extends com.broadon.status.StatusCode{
    //  Missing Input Data
    public static final String PCIS_MISSING_DATA = "1200";
    static final String PCIS_MISSING_DATA_MSG = "PCIS - Null Pointer -- missing data";
    //  BatchSize Too Lagre
    public static final String PCIS_BATCH_SIZE_EXCEEDS_LIMIT = "1201";
    static final String PCIS_BATCH_SIZE_EXCEEDS_LIMIT_MSG = "PCIS - Batch size exceeds limit";
    //  Invalid ECard Type
    public static final String PCIS_INVALID_ECARD_TYPE = "1202";
    static final String PCIS_INVALID_ECARD_TYPE_MSG = "PCIS - Invalid ECARD TYPE";
    //  Cannot Access Certificate
//    public static final String PCIS_NO_CERTIFICATE = "1203";
//    static final String PCIS_NO_CERTIFICATE_MSG = "PCIS - Cannot Access Certificate";
    public static final String PCIS_HSM_ERROR = "1203";
    static final String PCIS_HSM_ERROR_MSG = "PCIS - Cannot Create HSM Object";
    //  Cannot Access Certificate
    public static final String PCIS_EXCEEDS_CERT_QUANTITY = "1204";
    static final String PCIS_EXCEEDS_CERT_QUANTITY_MSG = "PCIS - Exceeds Certificate Quantity Limit";
    //  Cannot Access Certificate
    public static final String PCIS_EXCEEDS_CERT_DURATION = "1205";
    static final String PCIS_EXCEEDS_CERT_DURATION_MSG = "PCIS - Exceeds Certificate Time Limit";
    
    //  Busy, Try Again
    public static final String PCIS_BUSY = "1297";
    static final String PCIS_BUSY_MSG = "PCIS - Busy. Please Try Again.";
    //  Version Mismatch
    public static final String PCIS_VERSION_MISMATCH = "1298";
    static final String PCIS_VERSION_MISMATCH_MSG = "PCIS - Protocol Version Mismatch";
    //
    public static final String PCIS_INTERNAL_ERROR = "1299";
    static final String PCIS_INTERNAL_ERROR_MSG = "PCIS - Internal Error";
    
    static {
        addStatusCode(PCIS_MISSING_DATA, PCIS_MISSING_DATA_MSG);
        addStatusCode(PCIS_BATCH_SIZE_EXCEEDS_LIMIT, PCIS_BATCH_SIZE_EXCEEDS_LIMIT_MSG);
        addStatusCode(PCIS_INVALID_ECARD_TYPE, PCIS_INVALID_ECARD_TYPE_MSG);
        addStatusCode(PCIS_HSM_ERROR, PCIS_HSM_ERROR_MSG);
        addStatusCode(PCIS_EXCEEDS_CERT_QUANTITY, PCIS_EXCEEDS_CERT_QUANTITY_MSG);
        addStatusCode(PCIS_EXCEEDS_CERT_DURATION, PCIS_EXCEEDS_CERT_DURATION_MSG);
        addStatusCode(PCIS_BUSY, PCIS_BUSY_MSG);
        addStatusCode(PCIS_VERSION_MISMATCH, PCIS_VERSION_MISMATCH_MSG);
        addStatusCode(PCIS_INTERNAL_ERROR, PCIS_INTERNAL_ERROR_MSG);
    }
}

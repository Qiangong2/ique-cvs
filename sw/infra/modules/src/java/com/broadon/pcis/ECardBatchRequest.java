package com.broadon.pcis;

public class ECardBatchRequest {
    private long endECardId;
    private long startECardId;
    private int operatorId;
    private int batchSize;
    private int eCardType;
    private String status;
    private String attr01;
    private String attr02;
    private String attr03;
    String getAttr01() {
        return attr01;
    }
    void setAttr01(String attr01) {
        this.attr01 = attr01;
    }
    String getAttr02() {
        return attr02;
    }
    void setAttr02(String attr02) {
        this.attr02 = attr02;
    }
    String getAttr03() {
        return attr03;
    }
    void setAttr03(String attr03) {
        this.attr03 = attr03;
    }
    int getBatchSize() {
        return batchSize;
    }
    void setBatchSize(int batchSize) {
        this.batchSize = batchSize;
    }
    int getECardType() {
        return eCardType;
    }
    void setECardType(int cardType) {
        eCardType = cardType;
    }
    long getEndECardId() {
        return endECardId;
    }
    void setEndECardId(long endECardId) {
        this.endECardId = endECardId;
    }
    int getOperatorId() {
        return operatorId;
    }
    void setOperatorId(int operatorId) {
        this.operatorId = operatorId;
    }
    long getStartECardId() {
        return startECardId;
    }
    void setStartECardId(long startECardId) {
        this.startECardId = startECardId;
    }
    String getStatus() {
        return status;
    }
    void setStatus(String status) {
        this.status = status;
    }
    
    
}

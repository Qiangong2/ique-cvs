package com.broadon.pcis;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;

import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.logging.Log;

import com.broadon.hsmserver.HSM;
import com.broadon.pcis.StatusCode;
import com.broadon.wsapi.pcis.AbstractRequestType;
import com.broadon.wsapi.pcis.AbstractResponseType;
import com.broadon.wsapi.pcis.PropertyType;

public class PcisFunction {
    private static Log log = 
        LogFactory.getLog(PcisFunction.class.getName());
    
    /**
     * Initializes fields in a AbstractResponseType given an AbstractRequestType
     * @param request   the data source of the initialization
     * @param response  the destination to be initialized
     */
    public static void initResponse(AbstractRequestType request,
                                       AbstractResponseType response)
    {
        if (request.getMessageID() == null || request.getVersion() == null) {
            setErrorCode(StatusCode.PCIS_MISSING_DATA, null, response);
            return;
        }
        response.setVersion(request.getVersion());
        response.setMessageID(request.getMessageID());
        response.setTimeStamp(System.currentTimeMillis());
        response.setProperties(new PropertyType[0]);
        if (!PcisConstants.VERSION.equals(request.getVersion().trim())) // needed? always backward compatible
            setErrorCode(StatusCode.PCIS_VERSION_MISMATCH, null, response);
        else
            setErrorCode(StatusCode.SC_OK, null, response);
    }
    
    /**
     * Sets the error code in AbstractResponseType
     * @param code  the error code
     * @param message   the error message
     * @param response  the destination object 
     */
    public static void setErrorCode(String code, 
                                       String message,
                                       AbstractResponseType response)
    {
        response.setErrorCode(Integer.parseInt(code));
        if (message != null)
            response.setErrorMessage(StatusCode.getMessage(code) + message);
        else
            response.setErrorMessage(StatusCode.getMessage(code));
    }
    
    /**
     * Generate an eCard ID.
     * @param serial eCard serial number.
     * @param random The random number part of the eCard ID.
     * @return Encoded 16 digit eCard ID, 8 digit serial number and 8 digit random number.
     */
    public static String encode(long serialNumber, long randomNumber) {

        StringBuffer buf =
            new StringBuffer("0000000000000000");
        merge(0, PcisConstants.SerialNumberSize, String.valueOf(serialNumber), buf);
        merge(PcisConstants.SerialNumberSize, PcisConstants.RandomNumberSize, String.valueOf(randomNumber), buf);
        return buf.toString();
    }
 
    private static void merge(int offset, int size, String s, StringBuffer buf) {
        int len = s.length();
        offset += size - len;
        buf.replace(offset, offset + len, s);
    }
    
    static String verify(Connection conn, ECardBatchBean lastBatch, HSM hsm)
    throws SQLException {
        if (lastBatch == null)
            return StatusCode.SC_OK;    //the first time need not to generate a new key
        String certId = hsm.getCertificateID();
        if (! lastBatch.getCertId().equals(certId))
            return StatusCode.SC_OK;    // a new key has been generated
        KeyUsageInfo keyUsageInfo = PcisDBFunction.getKeyUsageInfo(conn, certId);
        if (keyUsageInfo == null)
            return StatusCode.SC_OK;    // key has not been used before
        if (keyUsageInfo.getTotalECards() > PcisConstants.ECARD_NUMBER_THRESHOLD)
            return StatusCode.PCIS_EXCEEDS_CERT_QUANTITY;   // reached number limit
        long duration = new Date().getTime() - keyUsageInfo.getStartDate().getTime();
        if (duration >= PcisConstants.ECARD_TIME_THRESHOLD)
            return StatusCode.PCIS_EXCEEDS_CERT_DURATION;   // reached time limit
        return StatusCode.SC_OK;    // verification is complete
    }
}

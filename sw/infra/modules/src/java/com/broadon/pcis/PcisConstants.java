package com.broadon.pcis;

public class PcisConstants {
    public static final String VERSION = "1.0";
    public static final int STATUS_OK = 0;
    public static final int maxBatchSize = 10000;
    
    // Changed serial number and random number to 8 digits
    public static final long   MAX_SERIAL_NUMBER       = 99999999L; 
    public static final long   MAX_RANDOM_NUMBER       = 99999999L;
    public static final long   STARTING_SERIAL_NUMBER  = 10000000L;
    
    public static final int SerialNumberSize = 8;
    public static final int RandomNumberSize = 8;
    
    public static final long   ECARD_NUMBER_THRESHOLD  = 10000000L;  // limit to 10M per certificate
    public static final long   ECARD_TIME_THRESHOLD    = 20*365*86400*1000L; // 20 years
}

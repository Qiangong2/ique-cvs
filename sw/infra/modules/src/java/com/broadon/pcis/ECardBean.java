package com.broadon.pcis;

import java.util.Date;
import java.math.BigDecimal;
/**
 * Data store used to insert database ECard table records.
 * 
 */
public class ECardBean {
    private long eCardId;
    private int eCardType;
    private byte[] eCardHash;
    private BigDecimal balance;
    private BigDecimal balanceAuthorized;
    private boolean isUsable;
    private Date lastUsed;
    private Date activateDate;
    private Date revokeDate;
    //private Date expireDate;

    BigDecimal getBalance() {
        return balance;
    }

    void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    BigDecimal getBalanceAuthorized() {
        return balanceAuthorized;
    }

    void setBalanceAuthorized(BigDecimal balanceAuthorized) {
        this.balanceAuthorized = balanceAuthorized;
    }

    long getECardId() {
        return eCardId;
    }

    void setECardId(long cardId) {
        this.eCardId = cardId;
    }

    int getECardType() {
        return eCardType;
    }

    void setECardType(int cardType) {
        this.eCardType = cardType;
    }

    boolean isUsable() {
        return isUsable;
    }
    void setUsable(boolean isUsable) {
        this.isUsable = isUsable;
    }
    Date getLastUsed() {
        return lastUsed;
    }

    void setLastUsed(Date lastUsed) {
        this.lastUsed = lastUsed;
    }

    Date getRevokeDate() {
        return revokeDate;
    }

    void setRevokeDate(Date revokeDate) {
        this.revokeDate = revokeDate;
    }

    byte[] getECardHash() {
        return eCardHash;
    }

    void setECardHash(byte[] cardHash) {
        eCardHash = cardHash;
    }

    Date getActivateDate() {
        return activateDate;
    }

    void setActivateDate(Date activateDate) {
        this.activateDate = activateDate;
    }

}

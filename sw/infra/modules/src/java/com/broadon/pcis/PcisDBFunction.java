package com.broadon.pcis;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.logging.Log;

import com.broadon.db.DBException;
import com.broadon.pcis.ECardTypeBean;
import com.broadon.pcis.ECardBatchBean;

public class PcisDBFunction {
    private static Log log = 
        LogFactory.getLog(PcisDBFunction.class.getName());
    
    static ECardTypeBean getECardType(Connection conn, int eCardType) {
        String query = "select DEFAULT_BALANCE, CURRENCY, IS_USEDONCE,"
            + " IS_PREPAID, ALLOW_REFILL, IS_TITLEONLY"
            + " from \"ECARD_TYPES\""
            + " where ECARD_TYPE=" + eCardType;
            //+ " for update NOWAIT";  read only
        log.debug("SQL: " + query);
        ECardTypeBean eCardTypeBean = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            /* 
                 private int eCardType;
                 private float defaultbalance;
                 private String currency;
                 private boolean isUsedOnce;
                 private boolean isPrepaid;
                 private boolean allowRefill;
                 //private String description;
                 //private Date lastUpdated;
                 private boolean isTitleOnly;
             */
            int tmp;
            while (rs.next()) {
                eCardTypeBean = new ECardTypeBean();
                eCardTypeBean.setECardType(eCardType);
                eCardTypeBean.setDefaultbalance(rs.getBigDecimal("DEFAULT_BALANCE"));
                eCardTypeBean.setCurrency(rs.getString("CURRENCY"));
                tmp = rs.getInt("IS_USEDONCE");
                if (tmp == 0)
                    eCardTypeBean.setUsedOnce(false);
                else
                    eCardTypeBean.setUsedOnce(true);
                tmp = rs.getInt("IS_PREPAID");
                if (tmp == 0)
                    eCardTypeBean.setPrepaid(false);
                else
                    eCardTypeBean.setPrepaid(true);
                tmp = rs.getInt("ALLOW_REFILL");
                if (tmp == 0)
                    eCardTypeBean.setAllowRefill(false);
                else
                    eCardTypeBean.setAllowRefill(true);
                tmp = rs.getInt("IS_TITLEONLY");
                if (tmp == 0)
                    eCardTypeBean.setTitleOnly(false);
                else
                    eCardTypeBean.setTitleOnly(true);
            }
        } catch (SQLException e) {
            eCardTypeBean = null;
            log.error("SQL error: ", e);
        }  catch (Throwable e) {
            // Unexpected error, catch and log
            eCardTypeBean = null;
            log.error("internal error", e);
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                log.error("cannot close stmt or resultSet: ", e);
            }
        }
        return eCardTypeBean;
    }
    
    /**
     * Returns the most recent eCard batch record.
     *
     * @returns The ECardBatch JavaBeans containing all information of
     *      a ECardBatch record.
     *
     * @throws  DBException
     * @throws  IOException
     */ 
    static ECardBatchBean getLastECardBatch(Connection conn, long maxID)
    throws SQLException
    {
        String TABLE_NAME = "ecard_batches";
        String SELECT_LATEST =
            "select END_ECARD_ID, START_ECARD_ID, CERT_ID, "
            + "BCCUTIL.ConvertTimeStamp(CREATE_DATE), ECARD_TYPE, "
            + "BCCUTIL.ConvertTimeStamp(PRINT_DATE), BCCUTIL.ConvertTimeStamp(SHIP_DATE), "
            + "BCCUTIL.ConvertTimeStamp(UPLOAD_DATE), BCCUTIL.ConvertTimeStamp(EXPIRE_DATE), "
            + "BCCUTIL.ConvertTimeStamp(REVOKE_DATE), BCCUTIL.ConvertTimeStamp(ACTIVATE_DATE), "
            + "ATTR01, ATTR02, ATTR03 from " 
            + TABLE_NAME + " where end_ecard_id = "
            + "(select max(end_ecard_id) from " + TABLE_NAME + " where end_ecard_id <= " + maxID + ")"
            + " for update NOWAIT";
        log.debug("SQL: " + SELECT_LATEST);
        ECardBatchBean eCardBatch = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(SELECT_LATEST);
            /* 
                 private long endECardId;
                 private long startECardId;
                 private String certId;
                 private Date createDate;
                 private int eCardType;
                 private Date printDate;
                 private Date shipDate;
                 private Date uploadDate;
                 private Date expireDate;
                 private Date revokeDate;
                 private Date activateDate;
                 private String attr01;
                 private String attr02;
                 private String attr03;
             */
            long timeStamp;
            while (rs.next()) {
                eCardBatch = new ECardBatchBean();
                eCardBatch.setEndECardId(rs.getLong("END_ECARD_ID"));
                eCardBatch.setStartECardId(rs.getLong("START_ECARD_ID"));
                eCardBatch.setCertId(rs.getString("CERT_ID"));
                timeStamp = rs.getLong(4);
                if (timeStamp > 0)
                    eCardBatch.setCreateDate(new Date(timeStamp));
                eCardBatch.setECardType(rs.getInt("ECARD_TYPE"));
                timeStamp = rs.getLong(6);
                if (timeStamp > 0)
                    eCardBatch.setPrintDate(new Date(timeStamp));
                timeStamp = rs.getLong(7);
                if (timeStamp > 0)
                    eCardBatch.setShipDate(new Date(timeStamp));
                timeStamp = rs.getLong(8);
                if (timeStamp > 0)
                    eCardBatch.setUploadDate(new Date(timeStamp));
                timeStamp = rs.getLong(9);
                if (timeStamp > 0)
                    eCardBatch.setExpireDate(new Date(timeStamp));
                timeStamp = rs.getLong(10);
                if (timeStamp > 0)
                    eCardBatch.setRevokeDate(new Date(timeStamp));
                timeStamp = rs.getLong(11);
                if (timeStamp > 0)
                    eCardBatch.setActivateDate(new Date(timeStamp));
                eCardBatch.setAttr01(rs.getString("ATTR01"));
                eCardBatch.setAttr01(rs.getString("ATTR02"));
                eCardBatch.setAttr01(rs.getString("ATTR03"));
            }
        } finally {
                if (stmt != null)
                    stmt.close();
                if (rs != null)
                    rs.close();
        }
        return eCardBatch;
    }
    
    static KeyUsageInfo getKeyUsageInfo(Connection conn, String certId) 
    throws SQLException
    {
        String query =
            "select sum(end_ecard_id - start_ecard_id + 1) totalECards, " +
               "min(create_date) start_date from Ecard_Batches "+
            " where cert_id = '" + certId + "'";
        log.debug("SQL: " + query);
        KeyUsageInfo keyUsageInfo = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                keyUsageInfo = new KeyUsageInfo();
                keyUsageInfo.setStartDate(rs.getDate("start_date"));
                keyUsageInfo.setTotalECards(rs.getLong("totalECards"));
            }
        } finally {
                if (stmt != null)
                    stmt.close();
                if (rs != null)
                    rs.close();
        }
        return keyUsageInfo;
    }
    
    static boolean insertECardBatchRecord(Connection conn, ECardBatchBean eCardBatch) {
        String insert = "insert into ECard_Batches "
            + "(END_ECARD_ID, START_ECARD_ID, CERT_ID, CREATE_DATE, PRINT_DATE, ECARD_TYPE, ATTR01, ATTR02, ATTR03) "
            + "values (?, ?, ?, SYS_EXTRACT_UTC(Current_Timestamp), SYS_EXTRACT_UTC(Current_Timestamp), ?, ?, ?, ?)";
        log.debug("SQL: " + insert);
        boolean ret = true;
        PreparedStatement pstmt = null;
        try {
            pstmt = conn.prepareStatement(insert);
            pstmt.setLong(1, eCardBatch.getEndECardId());
            pstmt.setLong(2, eCardBatch.getStartECardId());
            pstmt.setString(3, eCardBatch.getCertId());
            pstmt.setInt(4, eCardBatch.getECardType());
            pstmt.setString(5, eCardBatch.getAttr01());
            pstmt.setString(6, eCardBatch.getAttr02());
            pstmt.setString(7, eCardBatch.getAttr03());
            pstmt.execute();
        } catch (SQLException e) {
            log.error("SQL error: ", e);
            ret = false;
        } catch (Throwable e) {
            // Unexpected error, catch and log
            ret = false;
            log.error("internal error", e);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException e) {
                log.error("cannot close stmt or resultSet: ", e);
            }
        }
        return ret;
    }
    
    static boolean insertECardBean (Connection conn, ECardBean eCard) {
        String insert = "insert into ECards "
            + "(ECARD_ID, ECARD_TYPE, ECARD_HASH, BALANCE, BALANCE_AUTHORIZED, IS_USABLE) "
            + "values (?, ?, ?, ?, 0, 1)";
        log.debug("SQL: " + insert);
        boolean ret = true;
        PreparedStatement pstmt = null;
        try {
            pstmt = conn.prepareStatement(insert);
            pstmt.setLong(1, eCard.getECardId());
            pstmt.setInt(2, eCard.getECardType());
            pstmt.setBytes(3, eCard.getECardHash());
            pstmt.setBigDecimal(4, eCard.getBalance());
            pstmt.execute();
        } catch (SQLException e) {
            log.error("SQL error: ", e);
            ret = false;
        } catch (Throwable e) {
            // Unexpected error, catch and log
            ret = false;
            log.error("internal error", e);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException e) {
                log.error("cannot close stmt or resultSet: ", e);
            }
        }
        return ret;
    }
    
    static void insertRequestLog(Connection conn, ECardBatchRequest req) {
        String insert = "insert into ECard_Batch_Requests "
            + "( END_ECARD_ID, REQUEST_DATE, START_ECARD_ID, OPERATOR_ID, BATCH_SIZE, "
            + " ECARD_TYPE, STATUS, STATUS_DATE, ATTR01, ATTR02, ATTR03) "
            + " values (?, SYS_EXTRACT_UTC(Current_Timestamp), ?, ?, ?, ?, ?, SYS_EXTRACT_UTC(Current_Timestamp), "
            + " ?, ?, ?)";
        log.debug("SQL: " + insert);
        PreparedStatement pstmt = null;
        try {
            pstmt = conn.prepareStatement(insert);
            pstmt.setLong(1, req.getEndECardId());
            pstmt.setLong(2, req.getStartECardId());
            pstmt.setInt(3, req.getOperatorId());
            pstmt.setInt(4, req.getBatchSize());
            pstmt.setInt(5, req.getECardType());
            pstmt.setString(6, req.getStatus());
            pstmt.setString(7, req.getAttr01());
            pstmt.setString(8, req.getAttr02());
            pstmt.setString(9, req.getAttr03());
            pstmt.execute();
        } catch (SQLException e) {
            log.error("SQL error: ", e);
        } catch (Throwable e) {
            // Unexpected error, catch and log
            log.error("internal error", e);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException e) {
                log.error("cannot close stmt or resultSet: ", e);
            }
        }
        return;
    }
    static void lockProcessLock(Connection conn, String processName) throws SQLException {
        String query = "select * from PAS_PROCESS_LOCKS where PROCESS_NAME = '"
            + processName + "' for update NOWAIT";
        log.debug("SQL: " + query);
        Statement stmt = conn.createStatement();
        stmt.executeQuery(query);
    }
    
    static void updateProcessLock(Connection conn, String processName, String serverName) throws SQLException {
        String update = "update PAS_PROCESS_LOCKS set server_name = '"
        + serverName + "' , end_date = SYS_EXTRACT_UTC(Current_Timestamp)"
        + " where PROCESS_NAME = '" + processName + "'";
        log.debug("SQL: " + update);
        Statement stmt = conn.createStatement();
        stmt.executeUpdate(update);
    }
    
    public static void main(String args[]) {
        System.out.println("SQL tests...");
        try {
            // get Connection
            String url = "jdbc:oracle:thin:@//db1-vip.bcc.lab1.routefree.com:1521/BCCDB";
            //String url = "jdbc:oracle:thin:@//db2-vip.bcc.lab1.routefree.com:1521/BCCDB";
            String login = "pas";
            String passwd = "pas";
            Class.forName("oracle.jdbc.driver.OracleDriver");
            Connection conn = DriverManager.getConnection(url, login, passwd);
            // test KeyUsageInfo
            String certId = "a161394e52afe2f20f42908198c2cb8c";
            KeyUsageInfo keyUsageInfo = getKeyUsageInfo(conn, certId);
            if (keyUsageInfo != null) {
                System.out.println("start date: " + keyUsageInfo.getStartDate());
                System.out.println("total Ecards: " + keyUsageInfo.getTotalECards());
            }
            else
                System.out.println("error");
            ECardTypeBean eCardType = getECardType(conn, 5);
            if (eCardType == null)
                System.out.println("ok, 5 is not a valid ecardtype");
            eCardType = getECardType(conn, 10);
            System.out.println("currency: " + eCardType.getCurrency());
            System.out.println("defaultBalance: " + eCardType.getDefaultbalance());
            System.out.println("lastUpdated: " + eCardType.getLastUpdated());
            System.out.println("allowRefill: " +  eCardType.isAllowRefill());
            // test insertECardBean
            /*
            ECardBean eCard = new ECardBean();
            eCard.setECardId(10040000L);
            eCard.setECardType(101);
            eCard.setECardHash(new byte[100]);
            eCard.setBalance(new BigDecimal("1.23"));
            if (insertECardBean(conn,eCard))
                System.out.println("ok");
            else
                System.out.println("error");
            */
            // test insertECardBatch
            /*
            ECardBatchBean eCardBatch = new ECardBatchBean();
            eCardBatch.setEndECardId(10040000L);
            eCardBatch.setStartECardId(10040000L);
            eCardBatch.setCertId("TEST CASE");
            eCardBatch.setECardType(101);
            eCardBatch.setAttr01(null);
            eCardBatch.setAttr02("TEST");
            eCardBatch.setAttr03("TEST");
            if (insertECardBatchRecord(conn, eCardBatch))
                System.out.println("ok");
            else
                System.out.println("error");
            */
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
}

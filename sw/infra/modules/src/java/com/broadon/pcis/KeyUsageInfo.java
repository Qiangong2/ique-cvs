package com.broadon.pcis;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <c>KeyUsageInfo</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *<p>
 * It contains information about the usage of a key.
 *
 * @version	$Revision: 1.1 $
 */
public class KeyUsageInfo
{
    private long		totalECards;
    private Date		startDate;

    /**
     * Constructs an empty KeyUsageInfo instance.
     */
    public KeyUsageInfo()
    {
	this.totalECards = 0;
	this.startDate = null;
    }

    /**
     * Getter for totalECards.
     *
     * @returns	The total number of eCards generated.
     */
    public long getTotalECards()
    {
	return totalECards;
    }

    /**
     * Setter for totalECards.
     *
     * @param	totalECards		the total number of eCards generated
     */
    public void setTotalECards(long totalECards)
    {
	this.totalECards = totalECards;
    }

    /**
     * Getter for startDate.
     *
     * @returns	The date of start using the associated key.
     */
    public Date getStartDate()
    {
	return startDate;
    }

    /**
     * Setter for startDate.
     *
     * @param	startDate		the date of start using the
     *					associated key
     */
    public void setStartDate(Date startDate)
    {
	this.startDate = startDate;
    }
}

package com.broadon.pcis;
import java.math.BigDecimal;
import java.util.Date;
public class ECardTypeBean {
    private int eCardType;
    private BigDecimal defaultbalance;
    private String currency;
    private boolean isUsedOnce;
    private boolean isPrepaid;
    private boolean allowRefill;
    private String description;
    private Date lastUpdated;
    private boolean isTitleOnly;
    
    public ECardTypeBean() {
        super();
    }

    boolean isAllowRefill() {
        return allowRefill;
    }

    void setAllowRefill(boolean allowRefill) {
        this.allowRefill = allowRefill;
    }

    BigDecimal getDefaultbalance() {
        return defaultbalance;
    }

    void setDefaultbalance(BigDecimal defaultbalance) {
        this.defaultbalance = defaultbalance;
    }

    String getDescription() {
        return description;
    }

    void setDescription(String description) {
        this.description = description;
    }

    int getECardType() {
        return eCardType;
    }

    void setECardType(int cardType) {
        eCardType = cardType;
    }

    boolean isPrepaid() {
        return isPrepaid;
    }

    void setPrepaid(boolean isPrepaid) {
        this.isPrepaid = isPrepaid;
    }

    boolean isTitleOnly() {
        return isTitleOnly;
    }

    void setTitleOnly(boolean isTitleOnly) {
        this.isTitleOnly = isTitleOnly;
    }

    boolean isUsedOnce() {
        return isUsedOnce;
    }

    void setUsedOnce(boolean isUsedOnce) {
        this.isUsedOnce = isUsedOnce;
    }

    Date getLastUpdated() {
        return lastUpdated;
    }

    void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    String getCurrency() {
        return currency;
    }

    void setCurrency(String currency) {
        this.currency = currency;
    }

}

package com.broadon.pcis;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.sql.DataSource;
import javax.xml.rpc.ServiceException;
import javax.xml.rpc.server.ServiceLifecycle;
import javax.xml.rpc.server.ServletEndpointContext;

import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.logging.Log;

import com.broadon.servlet.ServletConstants;
import com.broadon.hsmserver.HSM;
import com.broadon.wsapi.pcis.PrepaidCardGenerationRequestType;
import com.broadon.wsapi.pcis.PrepaidCardGenerationResponseType;

public class PrepaidCardGenerationServiceImpl 
    implements ServiceLifecycle, ServletConstants {
    //  The servlet context
    protected ServletContext servletContext;
    private ServletEndpointContext soapContext;
    // The data source used to get database connection
    protected DataSource dataSource;
    //  The logging module
    private static Log log = 
        LogFactory.getLog(PrepaidCardGenerationServiceImpl.class.getName());
    private static HSM hsm = null;
    private static final String process = "PCIS";
    private static final String server = System.getProperty("HOSTNAME");
    private Properties prop;
    
    public void init(Object context) throws ServiceException {
        log.debug("Prepaid Card Generation Service Init");
        soapContext = (ServletEndpointContext) context; 
        servletContext = soapContext.getServletContext();
        dataSource = (DataSource)servletContext.getAttribute(DATA_SOURCE_KEY);
        // Obtain the properties set for IAS server in the file 
        // defined in conf/BBserver.properties.tmpl
        //
        prop = (Properties) servletContext.getAttribute(PROPERTY_KEY);
        try {
            hsm = new HSM(prop);
        } catch (Exception e) {
            throw new ServiceException("got HSM creation Exception", e);
        }
    }

    /**
     * Called by the JAX RPC runtime to destroy resources
     * used by the service endpoint at the end of its lifecycle.
     * Implements ServiceLifecycle.
     */
    public void destroy() 
    {
        log.debug("Prepaid Card Generation Service Destroy");
    }
    // Get a DB Connection and set autoCommit to false
    public Connection getConnection() throws SQLException
    {
        Connection conn = dataSource.getConnection();
        conn.setAutoCommit(false);
        return conn;
    }
    
    public PrepaidCardGenerationResponseType prepaidCardGeneration(
            PrepaidCardGenerationRequestType prepaidCardGenerationRequest) throws java.rmi.RemoteException {
        PrepaidCardGenerationResponseType response = new PrepaidCardGenerationResponseType();
        PcisFunction.initResponse(prepaidCardGenerationRequest, response);
        if (response.getErrorCode()!= PcisConstants.STATUS_OK)
            return response;
        // get input parameters
        int eCardType = prepaidCardGenerationRequest.getECardType();
        int operatorId = prepaidCardGenerationRequest.getOperatorId();
        String password = prepaidCardGenerationRequest.getPassword();
        int batchSize = prepaidCardGenerationRequest.getQuantity();
        // optional
        String refId1 = prepaidCardGenerationRequest.getReferenceId1();
        String refId2 = prepaidCardGenerationRequest.getReferenceId2();
        String refId3 = prepaidCardGenerationRequest.getReferenceId3();
        ECardBatchBean lastBatch = null;
        Connection conn = null;
        // get last batch
        try { 
            conn = getConnection();
            // lock global record
            PcisDBFunction.lockProcessLock(conn, process);
            lastBatch = PcisDBFunction.getLastECardBatch(conn, PcisConstants.MAX_SERIAL_NUMBER);
        } catch (SQLException dbe) {
            if (conn == null)
                PcisFunction.setErrorCode(StatusCode.PCIS_INTERNAL_ERROR, null, response);
            else
                // if record locked, just return and ket user try again
                PcisFunction.setErrorCode(StatusCode.PCIS_BUSY, null, response);
            log.error(dbe.getMessage());
            return response;
        }
        // if lastBatch is null, first time
        long endECardId;
        long beginECardId;
        if (lastBatch == null) {
            beginECardId = PcisConstants.STARTING_SERIAL_NUMBER;
        } else {
            beginECardId = lastBatch.getEndECardId()+1;
        }
        endECardId = beginECardId+batchSize-1;
        
        ECardBatchRequest reqLog = new ECardBatchRequest();
        reqLog.setEndECardId(endECardId);
        reqLog.setStartECardId(beginECardId);
        reqLog.setOperatorId(operatorId);
        reqLog.setBatchSize(batchSize);
        reqLog.setAttr01(refId1);
        reqLog.setAttr02(refId2);
        reqLog.setAttr03(refId3);
        
        // check batch size limit
        if (batchSize > PcisConstants.maxBatchSize) {
            PcisFunction.setErrorCode(StatusCode.PCIS_BATCH_SIZE_EXCEEDS_LIMIT, null, response);
            return response;
        }
        
        try {
            // check valid ecard type
            ECardTypeBean eCardTypeInfo = PcisDBFunction.getECardType(conn, eCardType);
            if (eCardTypeInfo == null) {
                PcisFunction.setErrorCode(StatusCode.PCIS_INVALID_ECARD_TYPE, null, response);
                return response;
            }
            // get certificate
//            final String CERT_FILE_KEY = "eCard_signer_cert";
//            final String KEY_NAME = "eCard_signer_keyname";
//            String certFileKey = prop.getProperty(CERT_FILE_KEY);
//            String keyName = prop.getProperty(KEY_NAME);
           
            // verify certificate
            if (hsm == null) {
                PcisFunction.setErrorCode(StatusCode.PCIS_HSM_ERROR, null, response);
                log.error("hsm is null!!");
                return response;
            }
            String verifyStatus = PcisFunction.verify(conn, lastBatch, hsm);
            if (!verifyStatus.equals(StatusCode.SC_OK)) {
                PcisFunction.setErrorCode(verifyStatus, null, response);
                return response;
            }
            // generate eCard batch record
            ECardBatchBean eCardBatch = new ECardBatchBean();
            eCardBatch.setEndECardId(endECardId);
            eCardBatch.setStartECardId(beginECardId);
            eCardBatch.setCertId(hsm.getCertificateID());
            eCardBatch.setECardType(eCardType);
            eCardBatch.setAttr01(refId1);
            eCardBatch.setAttr02(refId2);
            eCardBatch.setAttr03(refId3);
            PcisDBFunction.insertECardBatchRecord(conn, eCardBatch);
            //conn.commit();  // release lock on previous select for update
                            // a range of ecardIds have been reserved for this run.
            // generate eCards and ecard records
            ECardBean eCard = new ECardBean();
            long serialNumber = beginECardId;
            String printString[] = new String[batchSize];
            for (int i=0; i<batchSize; i++) {
                ECardSigner eCardSigner;
                eCardSigner = new ECardSigner(eCardType,
                          serialNumber,
                          hsm,
                          PcisConstants.MAX_RANDOM_NUMBER);
//                eCard.setECardID(new Long(serialNumber));
//                eCard.setECardTypeID(eCardTypeIDInt);
//                eCard.setRandomNumber(new Long(eCardSigner.getRandomNumber()));
//                eCard.setECardDigest(eCardSigner.getSignature());
                eCard.setECardId(serialNumber);
                eCard.setECardType(eCardType);
                eCard.setECardHash(eCardSigner.getSignature());
                eCard.setBalance(eCardTypeInfo.getDefaultbalance());
                // Insert into the database.
                PcisDBFunction.insertECardBean(conn, eCard);
                // prepare printString
                printString[i]=PcisFunction.encode(serialNumber, eCardSigner.getRandomNumber());
                // Increase the serial number.
                serialNumber++;
            }
            PcisDBFunction.updateProcessLock(conn, process, server);
            conn.commit();// update return state here before return
            response.setPrepaidCardString(printString);
        } catch (SQLException e) {               
            PcisFunction.setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
                log.debug("prepaidCardGeneration error: ", e);
        } catch (Throwable e) {
                // Unexpected error, catch and log
                PcisFunction.setErrorCode(StatusCode.PCIS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.PCIS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);
                log.error("prepaidCardGeneration error: ", e);
        } finally {
                try {                
                    if (conn != null) {
                        conn.rollback();
                        // eCardRequest populated
                        reqLog.setStatus(new Integer(response.getErrorCode()).toString());
                        PcisDBFunction.insertRequestLog(conn, reqLog);
                        conn.commit();
                        conn.close();
                    }
                } catch (SQLException e) {
                    log.warn("prepaidCardGeneration error while closing connection: ", e);
                }
        }
        return response;
    }
}

package com.broadon.security;

import java.io.IOException;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class SecureMessageRequestWrapper extends HttpServletRequestWrapper
{
    ServletInputStream stream;

    protected SecureMessageRequestWrapper(HttpServletRequest req, byte[] data)
	throws IOException
    {
	super(req);
	stream = new SecureMessageInputStream(data);
    }

    public ServletInputStream getInputStream() {
	return stream;
    }
}

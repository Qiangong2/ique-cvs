package com.broadon.security;

import java.io.IOException;
import java.security.cert.CertificateParsingException;
import java.security.interfaces.RSAPublicKey;

/**
 * Wrapper class to provide uniform interface to existing certificate parsing routines.
 */
public class BCCCertV1_ECC extends BCCCertV1
{
    BBcert cert;
    
    public byte[] getPublicKey()
    {
        return cert.getRawPublicKey();
    }

    public BCCCertV1_ECC(byte[] rawCert, String encodedCert) throws CertificateParsingException 
    {
        super(rawCert, encodedCert);
        sigType = CERTTYPE_ECC;
        try {
            cert = new BBcert(encodedCert);
            issuer = cert.getIssuer();
            subject = Long.toHexString(cert.getBBID());
            expirationTime = 0;
        } catch (IOException e) {
            CertificateParsingException cpe = new CertificateParsingException(e.getMessage());
            cpe.setStackTrace(e.getStackTrace());
            throw cpe;
        }      
    }
    
    public boolean verify(RSAPublicKey rsaKey) {
        return cert.verify(rsaKey);
    }
}

package com.broadon.security;


/**
 * Class to group the private key, password, and certificate into an object.
 */
public class CertInfo
{
    byte[] privateKey;		
    byte[] password;		// password to unlock the private key
    byte[] cert;

    byte[] chain;
    byte[] root;

    byte[] keyStore;
    byte[] trustedStore;

    byte[] chainID;

    /**
     * Create a <code>CertInfo</code> object.
     * @param privateKey Private key of this certificate.
     * @param password Password to unlock the private key.
     * @param cert The X509 certificate.
     * @param chain X509 certificate chain for this cert.
     * @param root Trusted root certs.
     * @param ks Cert and chain in KeyStore format.
     * @param ts Trusted certs in KeyStore format.
     */
    public CertInfo(byte[] privateKey, byte[] password, byte[] cert,
		    byte[] chain, byte[] root, byte[] ks, byte[] ts,
		    byte[] chainID)
    {
	this.privateKey = privateKey;
	this.password = password;
	this.cert = cert;
	this.chain = chain;
	this.root = root;
	this.keyStore = ks;
	this.trustedStore = ts;
	this.chainID = chainID;
    }


    /** @return The private key. */
    public byte[] getPrivateKey() {
	return privateKey;
    }


    /** @return The password to unlock the private key. */
    public byte[] getPassword() {
	return password;
    }


    /** @return The x509 certificate. */
    public byte[] getCert() {
	return cert;
    }

    /** @return X509 certificate chain for this cert. */
    public byte[] getChain() {
	return chain;
    }

    /** @return Trusted root certs. */
    public byte[] getRoot() {
	return root;
    }

    /** @return Cert and chain in KeyStore format. */
    public byte[] getKeyStore() {
	return keyStore;
    }

    /** @return Trusted certs in KeyStore format. */
    public byte[] getTrustedStore() {
	return trustedStore;
    }

    /** @return Chain ID of the signer of the cert. */
    public byte[] getChainID() {
	return chainID;
    }
}

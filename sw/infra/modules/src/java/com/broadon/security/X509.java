package com.broadon.security;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import com.broadon.util.Base64;

/**
 * Manipulate X509 certs.
 */
public class X509
{
    public static final String X509_HEADER = "-----BEGIN CERTIFICATE-----\n";
    public static final String X509_FOOTER = "-----END CERTIFICATE-----\n";

    final CertificateFactory cf;
    final MessageDigest md5;
    final Base64 base64;

    public X509() throws CertificateException, NoSuchAlgorithmException {
	cf = CertificateFactory.getInstance("X.509");
	md5 = MessageDigest.getInstance("MD5");
	base64 = new Base64();
    }


    /** read and parse an X509 cert as a Based64 encoded byte array. */
    public X509Certificate readEncodedX509(InputStream in)
	throws CertificateException
    {
	return (X509Certificate) cf.generateCertificate(in);
    }

    /** Read and parse an X509 cert as a Base64 encoded string */
    public X509Certificate readEncodedX509(String cert)
	throws CertificateException
    {
	return readEncodedX509(new ByteArrayInputStream(cert.getBytes()));
    }

    /** Read and parse an X509 cert (one cert per file). */
    public X509Certificate readX509(String certfile)
	throws IOException, CertificateException
    {
	return readEncodedX509(new FileInputStream(certfile));
    }


    /** Encode an X509 cert in PEM format. */
    public String encode(X509Certificate cert)
	throws CertificateEncodingException
    {
	String t = X509_HEADER + base64.encode(cert.getEncoded());
	if (t.endsWith("\n"))
	    return t + X509_FOOTER;
	else
	    return t + '\n' + X509_FOOTER;
    }
    

    /** Generate an unique ID for a cert.  The
	unique ID is acutally the MD5 checksum. */
    public String genUniqueID(final X509Certificate cert)
	throws CertificateEncodingException
    {
	return new BigInteger(1, md5.digest(cert.getEncoded())).toString(16);
    }

    /*
    public static void main(String[] args)
    {
	try {
	    X509 cert = new X509();
	    X509Certificate c = cert.readX509(args[0]);
	    FileOutputStream out = new FileOutputStream(args[1]);
	    byte[] encoded = cert.encode(c).getBytes();
	    out.write(encoded);
	    System.out.println(cert.genUniqueID(c));
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }
    */
}

package com.broadon.security;

import java.io.IOException;
import java.io.PrintWriter;

import javax.crypto.SecretKey;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

public class SecureMessageResponseWrapper extends HttpServletResponseWrapper
{
    HttpServletResponse res;
    final SecureMessage secMsg;
    final SecretKey key;
    
    ServletOutputStream stream = null;
    PrintWriter writer = null;

    protected SecureMessageResponseWrapper(HttpServletResponse res,
					   SecureMessage msg,
					   SecretKey key)
	throws IOException
    {
	super(res);
	this.res = res;
	secMsg = msg;
	this.key = key;
    }

    public ServletOutputStream getOutputStream()
	throws IOException
    {
	if (writer != null)
	    throw new IllegalStateException("getWriter() has already been called for this response");

	if (stream == null)
	    stream = new SecureMessageOutputStream(secMsg, key,
						   res.getOutputStream());
	return stream;
    }
    
    public PrintWriter getWriter()
	throws IOException
    {
	if (writer != null)
	    return writer;

	if (stream != null)
	     throw new IllegalStateException("getOutputStream() has already been called for this response");

	stream = new SecureMessageOutputStream(secMsg, key,
					       res.getOutputStream());
	writer = new PrintWriter(stream, true);
	return writer;
    }

    public void flushBuffer() throws IOException {
	if (writer != null)
	    writer.flush();
	if (stream != null)
	    stream.flush();
	super.flushBuffer();
    }

    protected void finishResponse() {
	try {
	    if (writer != null)
		writer.close();
	    if (stream != null)
		stream.close();
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }
}

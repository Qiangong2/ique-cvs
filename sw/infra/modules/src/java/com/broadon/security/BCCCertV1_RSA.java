package com.broadon.security;

import java.security.GeneralSecurityException;
import java.security.cert.CertificateParsingException;
import java.security.interfaces.RSAPublicKey;

public class BCCCertV1_RSA extends BCCCertV1
{
    private RSAPublicKey publicKey;
    
    /**
     * @return Returns the pubKey.
     */
    public RSAPublicKey getPublicKey()
    {
        return publicKey;
    }


    public BCCCertV1_RSA(byte[] rawCert, String encodedCert) throws CertificateParsingException {
        super(rawCert, encodedCert);
        sigType = CERTTYPE_RSA;
        RSAcert cert = new RSAcert(encodedCert);
        signature = cert.signature;
        issuer = cert.issuer;
        subject = cert.subject;
        expirationTime = cert.expirationTime;
        serial = cert.serial;
        try {
            publicKey = cert.getPubKey(rawCert);
        } catch (GeneralSecurityException e) {
            CertificateParsingException cpe = new CertificateParsingException(e.getMessage());
            cpe.setStackTrace(e.getStackTrace());
            throw cpe;
        }
    }

    public boolean verify(RSAPublicKey rsaKey)
    {
        throw new UnsupportedOperationException("Method verify not implmented");      
    }

}

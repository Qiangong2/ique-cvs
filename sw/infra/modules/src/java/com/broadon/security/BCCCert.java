package com.broadon.security;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateParsingException;
import java.security.interfaces.RSAPublicKey;
import java.sql.Date;

import com.broadon.util.Base64;

public abstract class BCCCert
{
    /** Signature type. */
    protected int sigType;

    /** signature. */
    protected byte[] signature;
    
    /** Issuer name. */
    protected String issuer;

    /** Subject name. */
    protected String subject;

    /** Expiration time (milliseconds since Epoch) */
    protected long expirationTime;

    /** Base64 encoded cert. */
    protected String encodedCert;
    
    /** unencoded cert. */
    protected byte[] rawCert;

    /** Unique id of cert (MD5 hash of raw cert). */
    protected String uniqueID;     

    /** serial number of cert. */
    protected long serial;


    protected BCCCert(byte[] rawCert, String encodedCert)
    {
        this.rawCert = rawCert;
        this.encodedCert = encodedCert;
        signature = null;
        uniqueID = null;
        serial = 0;
    }
    
    /**
     * @return Returns the encodedCert.
     */
    public String getEncodedCert()
    {
        return encodedCert;
    }

    /**
     * @return Returns the expirationTime.
     */
    public long getExpirationTime()
    {
        return expirationTime;
    }

    /**
     * @return Returns the issuer.
     */
    public String getIssuer()
    {
        return issuer;
    }

    /**
     * @return Returns the rawCert.
     */
    public byte[] getRawCert()
    {
        return rawCert;
    }

    /**
     * @return Returns the serial.
     */
    public long getSerial()
    {
        return serial;
    }

    /**
     * @return Returns the signature.
     */
    public byte[] getSignature()
    {
        return signature;
    }

    /**
     * @return Returns the sigType.
     */
    public int getSigType()
    {
        return sigType;
    }

    /**
     * @return Returns the subject.
     */
    public String getSubject()
    {
        return subject;
    }

    /**
     * @return Returns the uniqueID.
     * @throws CertificateParsingException 
     */
    public String getUniqueID() throws CertificateParsingException
    {
        if (uniqueID == null) {
            try {
                MessageDigest md5 = MessageDigest.getInstance("MD5");
                uniqueID = new BigInteger(1, md5.digest(rawCert)).toString(16);
            } catch (NoSuchAlgorithmException e) {
                CertificateParsingException cpe = new CertificateParsingException(e.getMessage());
                cpe.setStackTrace(e.getStackTrace());
                throw cpe;
            }
        }
        return uniqueID;
    }

    /**
     * Get integer from big endian byte array representation
     */
    protected static long getWord(byte[] buf, int offset, int length) {
        long n = 0L;
        for (int i = 0; i < length; ++i) {
            int b = buf[offset+i];
            n = (n << 8) | (b & 0xff);
        }
        return n;
    }
    
    protected static byte[] decodeCert(String cert) throws CertificateParsingException
    {
        try {
            return new Base64().decode(cert);
        } catch (IOException e) {
            CertificateParsingException cpe = new CertificateParsingException(e.getMessage());
            cpe.setStackTrace(e.getStackTrace());
            throw cpe;
        }
    }
    
    
    public static BCCCert getInstance(byte[] rawCert, String encodedCert) 
        throws CertificateParsingException
    {
        final int CERT_TYPE_SIZE = 4;
        final int BCC_CERT_V2 = 0x10000;
        int certType = (int) getWord(rawCert, 0, CERT_TYPE_SIZE);
        if (certType >= BCC_CERT_V2)
            return BCCCertV2.getInstance(rawCert, encodedCert, certType);
        else
            return BCCCertV1.getInstance(rawCert, encodedCert, certType);
    }
    
    
    /**
     * @param rawCert  Unencoded certificate in binary (array of bytes) format
     * @return An instance of BCCCert
     * @throws CertificateParsingException
     */
    public static BCCCert getInstance(byte[] rawCert) throws CertificateParsingException
    {
        return getInstance(rawCert, new Base64().encode(rawCert));
    }
    
    
    /**
     * @param encodedCert  Certificate encoded in base64 format.
     * @return An instance of BCCCert
     * @throws CertificateParsingException
     */
    public static BCCCert getInstance(String encodedCert) throws CertificateParsingException
    {
        return getInstance(decodeCert(encodedCert), encodedCert);
    }
    
    
    public abstract boolean verify(RSAPublicKey rsaKey);
    
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("Cert ID = ");
        try {
            sb.append(getUniqueID() + '\n');
        } catch (CertificateParsingException e) {
            sb.append("<cannot compute>\n");
        }
        
        sb.append("Serial number = " + serial + '\n');
        sb.append("Signature type = 0x" + Integer.toHexString(sigType) + '\n');
        sb.append("Signature = " + new BigInteger(1, signature).toString(16) + '\n');
        sb.append("Subject = " + subject + '\n');
        sb.append("Issuer = " + issuer + '\n');
        sb.append("Expiration = " + new Date(expirationTime).toString());        
        return sb.toString();
    }
  
    public static void main(String[] args)
    {
        try {
            File file = new File(args[0]);
            byte[] buf = new byte[(int) file.length()];
            FileInputStream in = new FileInputStream(file);
            in.read(buf);
            in.close();
            BCCCertV2_RSA CAcert = (BCCCertV2_RSA) getInstance(buf);
            System.out.println(CAcert);

            file = new File(args[1]);
            buf = new byte[(int) file.length()];
            in = new FileInputStream(file);
            in.read(buf);
            in.close();
            BCCCert MScert = getInstance(buf);
            System.out.println(MScert);
            System.out.println("Verification status: " + MScert.verify(CAcert.getPublicKey()));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
   
}

package com.broadon.security;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.RSAPublicKeySpec;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import javax.crypto.SecretKey;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import com.broadon.servlet.ServletConstants;
import com.broadon.util.Base64;
import com.broadon.util.HexString;
import com.broadon.util.Pair;
import com.broadon.xml.XMLMap;

public class SecureMessageFilter
    implements Filter, SecureMessageTags, ServletConstants
{
    // Thread local XML parser
    private static class InputParser extends ThreadLocal {
        public Object initialValue() {
            try {
                return new XMLMap();
            } catch (IOException e) {
                return null;
            }
        }

        public XMLMap getParser() {
            return (XMLMap) super.get();
        }
    } // InputParser

    static InputParser parser = null;

    // dummy object for synchronization
    static Object sync = new Object();

    DataSource ds = null;
    KeyFactory kf = null;
    Signature verifier = null;
    BigInteger exponent;

    public void init(FilterConfig config) throws ServletException
    {
	ds = (DataSource) config.getServletContext().getAttribute(DATA_SOURCE_KEY);
	try {
	    kf = KeyFactory.getInstance("RSA");
	    verifier = Signature.getInstance("SHA1withRSA");
	} catch (GeneralSecurityException e) {
	    throw new ServletException(e.getMessage());
	}
	exponent = BigInteger.valueOf(0x10001);
	
	synchronized (sync) {
	    if (parser == null)
		parser = new InputParser();
	}
    }

    public void destroy() {
	synchronized (sync) {
	    parser = null;
	}
    }


    byte[] bufferInput(InputStream in)
	throws IOException
    {
	ByteArrayOutputStream out = new ByteArrayOutputStream();
	int ch;
	while ((ch = in.read()) != -1)
	    out.write(ch);
	return out.toByteArray();
    }
    

    static final String smartCardInfo =
	"SELECT PUBLIC_KEY, SERVER_DH_PRIVATE_KEY, OWNER_ID FROM SMARTCARDS " +
	"    WHERE SMARTCARD_ID=? AND REVOKE_DATE IS NULL";

    // read the public key (RSA public key of the smart card) and the
    // server's DH private key (stored in the smart card when it was issued)
    // from the data base.
    Pair getSmartCardInfo(String signerID, HttpServletRequest hreq)
	throws GeneralSecurityException
    {
	long sid;
	try {
	    sid = Long.parseLong(signerID);
	} catch (NumberFormatException e) {
	    throw new GeneralSecurityException(e.getMessage());
	}

	Connection conn = null;
	try {
	    conn = ds.getConnection();
	    PreparedStatement ps = conn.prepareStatement(smartCardInfo);
	    ps.setLong(1, sid);
	    try {
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
		    hreq.setAttribute(REQUESTOR_TAG, new Long(rs.getLong(3)));
		    BigInteger modulus = new BigInteger(rs.getString(1), 32);
		    PublicKey pubKey = kf.generatePublic(new RSAPublicKeySpec(modulus, exponent));
		    BigInteger privateValue = new BigInteger(rs.getString(2), 32);
		    return new Pair(pubKey, privateValue);
		} else
		    throw new GeneralSecurityException("Cannot load public key");
	    } finally {
		ps.close();
	    }
	} catch (SQLException e) {
	    throw new GeneralSecurityException("Unauthorized");
	} finally {
	    try {
		if (conn != null)
		    conn.close();
	    } catch (SQLException e) {
	    }
	}
    } // getSmartCardInfo


    // 3DES decrypt the message, and then verify the signature.
    byte[] decryptMessage(SecureMessage sm, SecretKey skey, PublicKey signer,
			  HashMap message)
	throws GeneralSecurityException
    {
	try {
	    Base64 base64 = new Base64();
	    byte[] cipherText = base64.decode((String)message.get(CRYPTED_PAYLOAD_TAG));
	    byte[] plainText = sm.decrypt(cipherText, skey);
	    int docSize = Integer.parseInt((String)message.get(MESSAGE_SIZE_TAG));
	    
	    byte[] signature = HexString.fromHexString((String) message.get(SIGNATURE_TAG));

	    verifySignature(plainText, docSize, signature, signer);
	    
	    // clear the extra bytes in buffer
	    for (int i = docSize; i < plainText.length; ++i) {
		plainText[i] = ' ';
	    }
	    return plainText;
	} catch (IOException e) {
	    throw new GeneralSecurityException(e.getMessage());
	}
    } // decryptMessage


    void verifySignature(byte[] document, int docSize, byte[] signature,
			 PublicKey key)
	throws GeneralSecurityException
    {
	verifier.initVerify(key);
	verifier.update(document, 0, docSize);
	if (! verifier.verify(signature))
	    throw new SignatureException("Message signature not verified");
    }


    public void doFilter(ServletRequest req, ServletResponse res,
			 FilterChain chain)
	throws IOException, ServletException
    {
	HttpServletRequest hreq = (HttpServletRequest) req;
        HttpServletResponse hres = (HttpServletResponse) res;

	if (! hreq.getMethod().equals("POST")) {
	    hres.sendError(hres.SC_METHOD_NOT_ALLOWED);
            return;
        }

	// buffer the entire input so we could rewind to beginning if needed.
	byte[] content = bufferInput(hreq.getInputStream());
	
	// parse the request content, which is in XML format.  The
        // result is a HashMap of all element tags and their
        // corresponding value(s).
	XMLMap p = parser.getParser();
	HashMap requestBody =
	    (HashMap) p.process(new ByteArrayInputStream(content),
				new HashMap(), SECURE_MESSAGE_TAG);

	if (requestBody.isEmpty()) {
	    // not a secure message, pass along the orignal request
	    chain.doFilter(new SecureMessageRequestWrapper(hreq, content), res);
	    return;
	}

	// found a secure message

	try {
	    Pair pair = getSmartCardInfo((String) requestBody.get(SIGNER_ID_TAG), hreq);
	    
	    // reconstruct the shared secret key
	    SecureMessage sm = new SecureMessage();
	    PrivateKey hostKey = sm.getDHPrivateKey((BigInteger) pair.second);

	    BigInteger peerKey = new BigInteger((String) requestBody.get(PEER_PUBKEY_TAG), 16);
	    SecretKey skey = sm.getSharedKey(peerKey, hostKey);

	    // decrypt the message, and then verify the st signature
	    PublicKey signer = (PublicKey) pair.first;
	    byte[] document = decryptMessage(sm, skey, signer, requestBody);

	    hreq.setAttribute(REQUEST_AUTH_TAG, Boolean.valueOf(true));

	    ServletRequest sreq =
		new SecureMessageRequestWrapper(hreq, document);
	    SecureMessageResponseWrapper sres =
		new SecureMessageResponseWrapper(hres, sm, skey);


	    try {
		chain.doFilter(sreq, sres);
	    } finally {
		sres.finishResponse();
	    }
	} catch (GeneralSecurityException e) {
	    e.printStackTrace();
	    hres.sendError(hres.SC_UNAUTHORIZED);
	}
	    
	return;

    } 
}

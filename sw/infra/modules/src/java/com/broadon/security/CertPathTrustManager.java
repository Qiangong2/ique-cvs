package com.broadon.security;

import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.cert.CertPath;
import java.security.cert.CertPathValidator;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.PKIXParameters;
import java.security.cert.TrustAnchor;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;

import javax.net.ssl.X509TrustManager;

/**
 * Implementation of TrustManager for X509 certificate chains.
 */
public class CertPathTrustManager implements X509TrustManager
{
    CertPathValidator cpv;
    PKIXParameters cpp;
    CertificateFactory cf;
    X509Certificate[] trustedCerts = null;

    /**
     * Create a <code>CertPathTrustManager</code> object.
     * @param trustStore <code>KeyStore</code> containing all the trusted CA's.
     */
    public CertPathTrustManager(KeyStore trustStore)
	throws GeneralSecurityException
    {
	cpv = CertPathValidator.getInstance(CertPathValidator.getDefaultType());
	cpp = new PKIXParameters(trustStore);
	cpp.setRevocationEnabled(false);
	cf = CertificateFactory.getInstance("X.509");

	Set anchors = cpp.getTrustAnchors();
	trustedCerts = new X509Certificate[anchors.size()];
	Iterator iter = anchors.iterator();
	for (int i = 0; i < trustedCerts.length; ++i) {
	    trustedCerts[i] = ((TrustAnchor) iter.next()).getTrustedCert();
	}
    }


    void validate(X509Certificate[] chain, String authType)
	throws CertificateException
    {
	if (! authType.equals("RSA"))
	    throw new CertificateException("authType = " + authType);
	CertPath cp = cf.generateCertPath(Arrays.asList(chain));
	try {
	    cpv.validate(cp, cpp);
	} catch (GeneralSecurityException e) {
	    CertificateException ce = new CertificateException(e.getMessage());
	    ce.setStackTrace(e.getStackTrace());
	    throw ce;
	}
    }

    public void checkClientTrusted(X509Certificate[] chain, String authType)
	throws CertificateException
    {
	validate(chain, authType);
    }

    public void checkServerTrusted(X509Certificate[] chain, String authType)
	throws CertificateException
    {
	validate(chain, authType);
    }

    public X509Certificate[] getAcceptedIssuers() {
	return trustedCerts;
    }
    
}

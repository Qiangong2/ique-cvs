package com.broadon.security;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;

import javax.crypto.SecretKey;
import javax.servlet.ServletOutputStream;

import com.broadon.util.Base64;
import com.broadon.util.Xml;

public class SecureMessageOutputStream extends ServletOutputStream
{
    final SecureMessage secMsg;
    final SecretKey key;

    ByteArrayOutputStream buffer;
    ServletOutputStream out;
    boolean closed = false;
    
    protected SecureMessageOutputStream(SecureMessage msg,
					SecretKey key,
					ServletOutputStream out)
    {
	super();
	secMsg = msg;
	this.key = key;
	this.out = out;
	buffer = new ByteArrayOutputStream();
    }

    public void write(int b) throws IOException {
	buffer.write(b);
    }

    public void write(byte[] b, int off, int len) throws IOException {
	buffer.write(b, off, len);
    } 

    public void close() throws IOException
    {
	if (closed)
	    return;

	// pad the buffer to multiples of 8 bytes
	int size = buffer.size();
	if (size % 8 != 0) {
	    int needed = 8 - (size % 8);
	    for (int i = 0; i < needed; ++i)
		buffer.write(0);
	}

	try {
	    byte[] plainText = buffer.toByteArray();
	    byte[] cipherText = secMsg.encrypt(plainText, key);

	    out.println("<secure_message_resp>");
	    printElement("size", String.valueOf(plainText.length));
	    printElement("crypted_payload", new Base64().encode(cipherText));
	    // printElement("payload", plainText);
	    out.println("</secure_message_resp>");
	    out.close();
	    closed = true;
	} catch (GeneralSecurityException e) {
	    throw new IOException(e.getMessage());
	}
    }

    void printElement(String tag, String value)
	throws IOException
    {
	out.println('<' + tag + '>' + Xml.encode(value) + "</" + tag + '>');
    }

    void printElement(String tag, byte[] value)
	throws IOException
    {
	out.print('<' + tag + '>');
	out.write(Xml.encode(value));
	out.println("</" + tag + '>');
    }
}

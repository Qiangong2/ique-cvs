package com.broadon.security;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.servlet.ServletInputStream;

public class SecureMessageInputStream extends ServletInputStream
{
    ByteArrayInputStream buffer;

    protected SecureMessageInputStream(byte[] data) {
	super();
	buffer = new ByteArrayInputStream(data);
    }

    protected SecureMessageInputStream(String data) {
	super();
	buffer = new ByteArrayInputStream(data.getBytes());
    }

    public int read() throws IOException {
	return buffer.read();
    }
}

package com.broadon.security;

import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.cert.CertificateParsingException;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.RSAPublicKeySpec;

public class BCCCertV2_RSA extends BCCCertV2
{
    static final int PUBLIC_KEY_SIZE = RSA2048_SIG_SIZE;

    static final int EXPONENT_SIZE = 4;   
    
    
    private RSAPublicKey publicKey;
    
    /**
     * @return Returns the pubKey.
     */
    public RSAPublicKey getPublicKey()
    {
        return publicKey;
    }


    protected BCCCertV2_RSA(byte[] rawCert, String encodedCert, int sigType, int pubKeyType) 
        throws CertificateParsingException 
    {
        super(rawCert, encodedCert, sigType, pubKeyType);
        
        if (sigType != SIGTYPE_RSA2048 && sigType != SIGTYPE_RSA4096 && 
            pubKeyType != PUBKEY_RSA2048)
            throw new CertificateParsingException("Invalid signature or public key type"); 
  
        int exponentOffset = publicKeyOffset + PUBLIC_KEY_SIZE;
        
        byte[] pubkey = new byte[PUBLIC_KEY_SIZE];
        byte[] exp = new byte[EXPONENT_SIZE];
        
        System.arraycopy(rawCert, publicKeyOffset, pubkey, 0, PUBLIC_KEY_SIZE);
        System.arraycopy(rawCert, exponentOffset, exp, 0, EXPDATE_SIZE);

        BigInteger modulus = new BigInteger(1, pubkey);
        BigInteger exponent = new BigInteger(1, exp);
        
        try {
            KeyFactory kf = KeyFactory.getInstance("RSA");
            publicKey = (RSAPublicKey) kf.generatePublic(new RSAPublicKeySpec(modulus, exponent));
        } catch (GeneralSecurityException e) {
           CertificateParsingException cpe = new CertificateParsingException(e.getMessage());
           cpe.setStackTrace(e.getStackTrace());
           throw cpe;
        }     
    }

    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        
        sb.append(super.toString());
        sb.append("\nPublic key modulus = ");
        sb.append(publicKey.getModulus().toString(16));
        sb.append("\nPublic key exponent = 0x");
        sb.append(publicKey.getPublicExponent().toString(16));
        return sb.toString();
    }
}

package com.broadon.security;

public interface SecureMessageTags
{
    static final String SECURE_MESSAGE_TAG = "secure_message";

    static final String PEER_PUBKEY_TAG = "X";

    static final String PAYLOAD_TAG = "payload";

    static final String SIGNATURE_TAG = "signature";

    static final String SIGNER_ID_TAG = "signer_id";

    static final String SECURE_MESSAGE_RESPONSE_TAG = "secure_message_resp";

    static final String MESSAGE_SIZE_TAG = "size";

    static final String HOST_PUBKEY_TAG = "Y";

    static final String CRYPTED_PAYLOAD_TAG = "crypted_payload";

    // following are parameter names for the SecureMessage wrappers

    static final String REQUEST_AUTH_TAG =
	"com.broadon.security.requestAuthenticated";

    static final String REQUESTOR_TAG = "com.broadon.security.requestor";
}


package com.broadon.security;

import java.security.GeneralSecurityException;
import java.security.Signature;
import java.security.cert.CertificateParsingException;
import java.security.interfaces.RSAPublicKey;

/**
 * Read and parse a BroadOn certificate (Version 2) for NetC and Revolution
 */
public class BCCCertV2 extends BCCCert
{
    protected static final int SIGTYPE_OFFSET = 0;
    protected static final int SIGTYPE_SIZE = 4;
    
    protected static final int SIGTYPE_RSA4096 = 0x00010000;
    protected static final int SIGTYPE_RSA2048 = 0x00010001;
    protected static final int SIGTYPE_ECC233  = 0x00010002;
    
    protected static final int SIG_OFFSET = SIGTYPE_OFFSET + SIGTYPE_SIZE;
    protected final int sigSize; 
    protected static final int RSA4096_SIG_SIZE = 4096/8;
    protected static final int RSA2048_SIG_SIZE = 2048/8;
    protected static final int ECC233_SIG_SIZE = 512/8;
    
    protected static final int SIG_PADDING_SIZE = 60;
    
    protected final int issuerOffset;
    protected static final int ISSUER_SIZE = 64; 
    
    protected final int certBodyOffset;          // same as issuer_offset
    
    protected final int pubKeyTypeOffset;
    protected static final int PUBKEYTYPE_SIZE = 4;
    protected static final int PUBKEY_RSA4096 = 0;
    protected static final int PUBKEY_RSA2048 = 1;
    protected static final int PUBKEY_ECC233 = 2;
    protected final int pubKeytype;
    
    protected final int subjectOffset;
    protected static final int SUBJECT_SIZE = 64;
    
    static final int SUBJECT_PREFIX_SIZE = 2;
    
    protected final int expDateOffset;
    protected static final int EXPDATE_SIZE = 4;
    
    // The equivalent of Epoch for BroadOn.  This is Jan. 1, 2000 00:00:00 GMT.
    protected static final long BCC_TIME_BASE = 9466848000000L;
    
    protected final int publicKeyOffset;
    static final int RSA4096_KEY_SIZE = RSA4096_SIG_SIZE; 
    static final int RSA2048_KEY_SIZE = RSA2048_SIG_SIZE;
    static final int ECC233_KEY_SIZE = ECC233_SIG_SIZE;
   
    
    public int getPubKeytype()
    {
        return pubKeytype;
    }

    static int parseSigType(int sigType) throws CertificateParsingException {
        switch (sigType) {
        case SIGTYPE_RSA4096:
            return RSA4096_SIG_SIZE;
        case SIGTYPE_RSA2048:
            return RSA2048_SIG_SIZE;
        case SIGTYPE_ECC233:
            return ECC233_SIG_SIZE;
        default:
            throw new CertificateParsingException("Unknown signature type");      
        }
    }
    
    protected BCCCertV2(byte[] rawCert, String encodedCert, int sigType, int pubKeyType)
        throws CertificateParsingException
    {
        super(rawCert, encodedCert);
        
        sigSize = parseSigType(sigType);
        certBodyOffset = issuerOffset = SIG_OFFSET + sigSize + SIG_PADDING_SIZE;
        pubKeyTypeOffset = issuerOffset + ISSUER_SIZE;
        subjectOffset = pubKeyTypeOffset + PUBKEYTYPE_SIZE;
        expDateOffset = subjectOffset + SUBJECT_SIZE;
        publicKeyOffset = expDateOffset + EXPDATE_SIZE;
        
        this.sigType = sigType;
        this.signature = new byte[sigSize]; 
        System.arraycopy(rawCert, SIG_OFFSET, signature, 0, sigSize);
        this.pubKeytype = pubKeyType;
        
        issuer = new String(rawCert, issuerOffset, ISSUER_SIZE).trim();
        subject = new String(rawCert, subjectOffset, SUBJECT_SIZE).trim();
        
        // extract the serial number from the subject string.  The cert actually does
        // not contain a serial number.
        serial = Integer.parseInt(subject.substring(2), 16);
        
        expirationTime = BCC_TIME_BASE + getWord(rawCert, expDateOffset, EXPDATE_SIZE) * 1000;
    }

    
    protected static BCCCertV2 getInstance(byte[] rawCert, String encodedCert, int sigType)
        throws CertificateParsingException 
    {
        int pubKeyOffset = SIG_OFFSET + parseSigType(sigType) + SIG_PADDING_SIZE + ISSUER_SIZE;
        int pubKeyType = (int) getWord(rawCert, pubKeyOffset, PUBKEYTYPE_SIZE);
        
        
        if (pubKeyType == PUBKEY_RSA2048) {
            switch (sigType) {
            case SIGTYPE_RSA4096:
            case SIGTYPE_RSA2048:
                return new BCCCertV2_RSA(rawCert, encodedCert, sigType, pubKeyType);
            }
        } else if (pubKeyType == PUBKEY_ECC233 && sigType == SIGTYPE_RSA2048)
            return new BCCCertV2_ECC(rawCert, encodedCert, sigType, pubKeyType);
            
        throw new CertificateParsingException("Unsupport certificate type");       
    }
    
    /**
     * Verify the signature of this certificate.
     * @param rsaKey RSA public key of the issuer of this cert.
     * @return True if the signature can be verified.
     */
    public boolean verify(RSAPublicKey rsaKey) {
        try {
            Signature sig = Signature.getInstance("SHA1withRSA");
            sig.initVerify(rsaKey);
            sig.update(rawCert, certBodyOffset, rawCert.length - certBodyOffset);
            return sig.verify(rawCert, SIG_OFFSET, sigSize);
        } catch (GeneralSecurityException e) {
            return false;
        }
    }
    
    public String toString() {
        return super.toString() + 
            "\nPublic key type = 0x" + Integer.toHexString(pubKeytype);     
    }
}

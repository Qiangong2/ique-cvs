package com.broadon.security;

import java.math.BigInteger;
import java.security.cert.CertificateParsingException;

public class BCCCertV2_ECC extends BCCCertV2
{
    static final int PUBLIC_KEY_SIZE = ECC233_SIG_SIZE;
    
    // TO DO:  change type to ECCPublicKey when it is supported.
    private byte[] publicKey;
    
    public byte[] getPublicKey()
    {
        return publicKey;
    }

    protected BCCCertV2_ECC(byte[] rawCert, String encodedCert, int sigType, int pubKeyType)
        throws CertificateParsingException
    {
        super(rawCert, encodedCert, sigType, pubKeyType);
       
        if (sigType != SIGTYPE_RSA2048 || pubKeyType != PUBKEY_ECC233)
            throw new CertificateParsingException("Invalid signature or public key type");
        
        publicKey = new byte[PUBLIC_KEY_SIZE];
        System.arraycopy(rawCert, publicKeyOffset, publicKey, 0, PUBLIC_KEY_SIZE);
    }

    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        
        sb.append(super.toString());
        sb.append("\nPublic key = " + new BigInteger(1, publicKey).toString(16));
        return sb.toString();
    }
}

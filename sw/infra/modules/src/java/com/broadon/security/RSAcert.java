package com.broadon.security;

import java.io.IOException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateParsingException;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.RSAPublicKeySpec;

import com.broadon.util.Base64;

/**
 * Read a parse an RSA cert that is in BroadOn format.
 */
public class RSAcert
{
    static final int CERTTYPE_OFFSET = 0;
    static final int CERTTYPE_SIZE = 4;

    static final int CERTTYPE_RSA = 1;

    static final int SIGTYPE_OFFSET = CERTTYPE_OFFSET + CERTTYPE_SIZE;
    static final int SIGTYPE_SIZE = 4;

    static final int SIGTYPE_RSA2048 = 0;
    static final int SIGTYPE_RSA4096 = 1;

    static final int DATE_OFFSET = SIGTYPE_OFFSET + SIGTYPE_SIZE;
    static final int DATE_SIZE = 4;
	
    static final int ISSUER_OFFSET = DATE_OFFSET + DATE_SIZE;
    static final int ISSUER_SIZE = 64;

    static final int SUBJECT_OFFSET = ISSUER_OFFSET + ISSUER_SIZE;
    static final int SUBJECT_SIZE = 64;

    static final int PUBKEY_OFFSET = SUBJECT_OFFSET + SUBJECT_SIZE;
    static final int PUBKEY_SIZE = 256;

    static final int EXPONENT_OFFSET = PUBKEY_OFFSET + PUBKEY_SIZE;
    static final int EXPONENT_SIZE = 4;

    static final int SIG_OFFSET = EXPONENT_OFFSET + EXPONENT_SIZE;
    static final int CA_SIG_SIZE = 512;
    static final int SIG_SIZE = 256;

    static final int PAD_OFFSET = SIG_OFFSET + SIG_SIZE;
    static final int PAD_SIZE = 256;

    static final int CERT_SIZE = SIG_OFFSET + CA_SIG_SIZE;

    /** Certificate type */
    public int certType;

    /** Signature type. */
    public int sigType;

    /** Expiration time (milliseconds since Epoch) */
    public long expirationTime;

    /** Issuer name. */
    public String issuer;

    /** Subject name. */
    public String subject;

    /** RSA Public Key. */
    public RSAPublicKey pubkey;

    /** RSA signature. */
    public byte[] signature;

    /** Base64 encoded cert. */
    public String encodedCert;	

    /** Unique id of cert (MD5 hash of raw cert). */
    public String uniqueID;	

    /** serial number of cert. */
    public long serial;

    long getWord(byte[] buf, int offset, int length) {
	long n = 0L;
	for (int i = 0; i < length; ++i) {
	    int b = buf[offset+i];
	    n = (n << 8) | (b & 0xff);
	}
	return n;
    }

    RSAPublicKey getPubKey(byte[] cert)
	throws GeneralSecurityException
    {
	byte[] pubkey = new byte[PUBKEY_SIZE];
	byte[] exp = new byte[EXPONENT_SIZE];
	
	System.arraycopy(cert, PUBKEY_OFFSET, pubkey, 0, PUBKEY_SIZE);
	System.arraycopy(cert, EXPONENT_OFFSET, exp, 0, EXPONENT_SIZE);

	BigInteger modulus = new BigInteger(1, pubkey);
	BigInteger exponent = new BigInteger(1, exp);
	
	KeyFactory kf = KeyFactory.getInstance("RSA");
	return (RSAPublicKey)
	    kf.generatePublic(new RSAPublicKeySpec(modulus, exponent));
    }
    

    void parseCert(byte[] cert)
	throws CertificateParsingException
    {
	try {
	    MessageDigest md5 = MessageDigest.getInstance("MD5");
	    uniqueID = new BigInteger(1, md5.digest(cert)).toString(16);
	} catch (NoSuchAlgorithmException e) {
	    throw new CertificateParsingException("Error computing unique cert ID");
	}

	if (cert.length != CERT_SIZE)
	    throw new CertificateParsingException("Invalid cert. length");
	
	certType = (int) getWord(cert, CERTTYPE_OFFSET, CERTTYPE_SIZE);
	sigType = (int) getWord(cert, SIGTYPE_OFFSET, SIGTYPE_SIZE);

	if ((certType != CERTTYPE_RSA) ||
	    (sigType != SIGTYPE_RSA2048 && sigType != SIGTYPE_RSA4096))
	    throw new CertificateParsingException("Unknown signature type.");
	expirationTime = getWord(cert, DATE_OFFSET, DATE_SIZE) * 1000;
	issuer = new String(cert, ISSUER_OFFSET, ISSUER_SIZE).trim();
	subject = new String(cert, SUBJECT_OFFSET, SUBJECT_SIZE).trim();
	try {
	    pubkey = getPubKey(cert);
	} catch (GeneralSecurityException e) {
	    throw new CertificateParsingException(e.getMessage());
	}
	if (sigType == SIGTYPE_RSA2048) {
	    signature = new byte[SIG_SIZE];
	    System.arraycopy(cert, SIG_OFFSET + SIG_SIZE, signature, 0,
			     signature.length);
	} else {
	    signature = new byte[CA_SIG_SIZE];
	    System.arraycopy(cert, SIG_OFFSET, signature, 0, CA_SIG_SIZE);
	}

	// parse the Subject string and extract the serial number.
	// subject string is in the form of a prefix followed by 8 hex digits.
	serial = Long.parseLong(subject.substring(subject.length() - 8), 16);
    }


    byte[] decodeCert(String cert)
	throws CertificateParsingException
    {
	try {
	    return new Base64().decode(cert);
	} catch (IOException e) {
	    throw new CertificateParsingException("Not base64 encoded");
	}
    }

    /** Parse the certificate.
     * @param cert certificate in raw format. 
     * @exception CertificateParsingException Invalid certificate format
     */
    public RSAcert(byte[] cert)
	throws CertificateParsingException
    {
	encodedCert = new Base64().encode(cert);
	parseCert(cert);
    }

    /** Parse the certificate.
     * @param cert certificate in base64-encoded format. 
     * @exception CertificateParsingException Invalid certificate format
     */
    public RSAcert(String cert)
	throws CertificateParsingException
    {
	encodedCert = cert;
	parseCert(decodeCert(cert));
    }
}


package com.broadon.security;

import java.io.IOException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.PublicKey;
import java.security.Signature;

import com.broadon.util.Base64;

/**
 * Class to decode and verify a BB certificate.
 */
public class BBcert
{
    static final int CERTTYPE_OFFSET = 0;
    static final int CERTTYPE_SIZE = 4;

    static final int CERTTYPE_ECC = 0;

    static final int SIGTYPE_OFFSET = CERTTYPE_OFFSET + CERTTYPE_SIZE;
    static final int SIGTYPE_SIZE = 4;

    static final int SIGTYPE_RSA2048 = 0;

    static final int DATE_OFFSET = SIGTYPE_OFFSET + SIGTYPE_SIZE;
    static final int DATE_SIZE = 4;
	
    static final int ISSUER_OFFSET = DATE_OFFSET + DATE_SIZE;
    static final int ISSUER_SIZE = 64;

    static final int SUBJECT_OFFSET = ISSUER_OFFSET + ISSUER_SIZE;
    static final int SUBJECT_SIZE = 64;

    static final int PUBKEY_OFFSET = SUBJECT_OFFSET + SUBJECT_SIZE;
    static final int PUBKEY_SIZE = 64;

    static final int SIG_OFFSET = PUBKEY_OFFSET + PUBKEY_SIZE;
    static final int SIG_SIZE = 256;

    static final int PAD_OFFSET = SIG_OFFSET + SIG_SIZE;
    static final int PAD_SIZE = 256;

    static final int CERT_SIZE = PAD_OFFSET + PAD_SIZE;

    static final Base64 base64 = new Base64();

    long bbID;
    byte[] cert;

    /**
     * Construct a <code>BBcert</code> object.
     * @param cert The Base64-encoded certificate 
     * @exception IOException Certificate format invalid.
     */
    public BBcert(String encodedCert) throws IOException {
	cert = base64.decode(encodedCert);

	if (cert.length != CERT_SIZE ||
	    (cert[0] != 0 || cert[1] != 0 || cert[2] != 0 || cert[3] != 0) ||
	    (cert[4] != 0 || cert[5] != 0 || cert[6] != 0 || cert[7] != 0))
	    
	    throw new IOException("Malformed BB certificate");

	// BB id is in the form "BBxxxxxxxx"
	int ofst = SUBJECT_OFFSET + 2; // skip the leading "BB"
	String s = new String(cert, ofst, 8);
	bbID = Long.valueOf(s, 16).longValue();

    } // constructor


    /** @return BB ID. */
    public long getBBID() {
	return bbID;
    }


    public byte[] getRawPublicKey() {
	byte[] key = new byte[PUBKEY_SIZE];
	System.arraycopy(cert, PUBKEY_OFFSET, key, 0, PUBKEY_SIZE);
	return key;
    }

    /** @return BB's ECC public key. */
    public BigInteger getPublicKey() {
	return new BigInteger(1, getRawPublicKey());
    }

    public String getIssuer() {
	String s = new String(cert, ISSUER_OFFSET, ISSUER_SIZE);
	return s.trim();
    }


    /**
     * Verify the signature of this certificate.
     * @param rsaKey RSA public key of the issuer of this cert.
     * @return True if the signature can be verified.
     */
    public boolean verify(PublicKey rsaKey) {
	try {
	    Signature sig = Signature.getInstance("SHA1withRSA");
	    sig.initVerify(rsaKey);
	    sig.update(cert, 0, SIG_OFFSET);
	    return sig.verify(cert, SIG_OFFSET, SIG_SIZE);
	} catch (GeneralSecurityException e) {
	    return false;
	}
    }
}

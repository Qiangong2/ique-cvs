package com.broadon.security;

import java.security.cert.CertificateParsingException;
import java.security.interfaces.RSAPublicKey;

public abstract class BCCCertV1 extends BCCCert
{
    static final int CERTTYPE_ECC = 0;
    static final int CERTTYPE_RSA = 1;


    public BCCCertV1(byte[] rawCert, String encodedCert) {
        super(rawCert, encodedCert);
    }

    protected static BCCCertV1 getInstance(byte[] rawCert, String encodedCert, int certType) throws CertificateParsingException
    {
        switch (certType) {
        case CERTTYPE_ECC:
            return new BCCCertV1_ECC(rawCert, encodedCert);
        case CERTTYPE_RSA:
            return new BCCCertV1_RSA(rawCert, encodedCert);
        default:
            throw new CertificateParsingException("Unrecognized certificate type.");    
        }
    }

    public abstract boolean verify(RSAPublicKey rsaKey);
    
}

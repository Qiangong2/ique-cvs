package com.broadon.security;

import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.KeyAgreement;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.interfaces.DHPublicKey;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.DHParameterSpec;
import javax.crypto.spec.DHPrivateKeySpec;
import javax.crypto.spec.DHPublicKeySpec;
import javax.crypto.spec.IvParameterSpec;

/**
 * Handle secure message passing.
 * Client sends in DH public key and a signed message.
 * Server responds with it's own DH public key and encrypted reply.
 */
public class SecureMessage
{
    static final byte[] prime1024Bytes = {
	(byte)0x97, (byte)0xF6, (byte)0x42, (byte)0x61,
	(byte)0xCA, (byte)0xB5, (byte)0x05, (byte)0xDD,
	(byte)0x28, (byte)0x28, (byte)0xE1, (byte)0x3F,
	(byte)0x1D, (byte)0x68, (byte)0xB6, (byte)0xD3,
	(byte)0xDB, (byte)0xD0, (byte)0xF3, (byte)0x13,
	(byte)0x04, (byte)0x7F, (byte)0x40, (byte)0xE8,
	(byte)0x56, (byte)0xDA, (byte)0x58, (byte)0xCB,
	(byte)0x13, (byte)0xB8, (byte)0xA1, (byte)0xBF,
	(byte)0x2B, (byte)0x78, (byte)0x3A, (byte)0x4C,
	(byte)0x6D, (byte)0x59, (byte)0xD5, (byte)0xF9,
	(byte)0x2A, (byte)0xFC, (byte)0x6C, (byte)0xFF,
	(byte)0x3D, (byte)0x69, (byte)0x3F, (byte)0x78,
	(byte)0xB2, (byte)0x3D, (byte)0x4F, (byte)0x31,
	(byte)0x60, (byte)0xA9, (byte)0x50, (byte)0x2E,
	(byte)0x3E, (byte)0xFA, (byte)0xF7, (byte)0xAB,
	(byte)0x5E, (byte)0x1A, (byte)0xD5, (byte)0xA6,
	(byte)0x5E, (byte)0x55, (byte)0x43, (byte)0x13,
	(byte)0x82, (byte)0x8D, (byte)0xA8, (byte)0x3B,
	(byte)0x9F, (byte)0xF2, (byte)0xD9, (byte)0x41,
	(byte)0xDE, (byte)0xE9, (byte)0x56, (byte)0x89,
	(byte)0xFA, (byte)0xDA, (byte)0xEA, (byte)0x09,
	(byte)0x36, (byte)0xAD, (byte)0xDF, (byte)0x19,
	(byte)0x71, (byte)0xFE, (byte)0x63, (byte)0x5B,
	(byte)0x20, (byte)0xAF, (byte)0x47, (byte)0x03,
	(byte)0x64, (byte)0x60, (byte)0x3C, (byte)0x2D,
	(byte)0xE0, (byte)0x59, (byte)0xF5, (byte)0x4B,
	(byte)0x65, (byte)0x0A, (byte)0xD8, (byte)0xFA,
	(byte)0x0C, (byte)0xF7, (byte)0x01, (byte)0x21,
	(byte)0xC7, (byte)0x47, (byte)0x99, (byte)0xD7,
	(byte)0x58, (byte)0x71, (byte)0x32, (byte)0xBE,
	(byte)0x9B, (byte)0x99, (byte)0x9B, (byte)0xB9,
	(byte)0xB7, (byte)0x87, (byte)0xE8, (byte)0xAB
    };

    static final BigInteger prime1024 = new BigInteger(1, prime1024Bytes);

    static final BigInteger base1024 = BigInteger.valueOf(2);

    static final IvParameterSpec iv;

    static {
	byte[] zeros = new byte[8];
	Arrays.fill(zeros, (byte)0);
	iv = new IvParameterSpec(zeros);
    }

    KeyPairGenerator keyGen;
    KeyAgreement keyAgree;
    MessageDigest md5;
    KeyFactory dhKF;
    SecretKeyFactory desKF;
    Cipher desCipher;

    /**
     * Create an <code>SecureMessage</code> object.
     */
    public SecureMessage()
	throws GeneralSecurityException
    {
	keyGen = KeyPairGenerator.getInstance("DH");
	keyGen.initialize(new DHParameterSpec(prime1024, base1024));

	keyAgree = KeyAgreement.getInstance("DH");
	dhKF = KeyFactory.getInstance("DH");

	md5 = MessageDigest.getInstance("MD5");
	desKF = SecretKeyFactory.getInstance("DESede");
	desCipher = Cipher.getInstance("DESede/CBC/NoPadding");
    }


    public KeyPair generateKeyPair() {
	return keyGen.generateKeyPair();
    }

    /**
     * Given the DH public key from the peer, recover the shared
     * secrect and then convert it to a 3DES key.
     * @param peerPubKey Public key of the peer.
     * @param hostPriKey Private key of the host.
     * @return Triple DES secret key.
     */
    public SecretKey getSharedKey(Key peerPubKey, PrivateKey hostPrivKey)
	throws GeneralSecurityException
    {
	keyAgree.init(hostPrivKey);
	keyAgree.doPhase(peerPubKey, true);

	// convert the 1024-bit shared secret to 16 bytes MD5 hash,
	// and then create the 24 bytes 3DES keys.
	byte[] desKey = new byte[24];
	md5.update(keyAgree.generateSecret());
	md5.digest(desKey, 0, 16);
	System.arraycopy(desKey, 0, desKey, 16, 8);
	return desKF.generateSecret(new DESedeKeySpec(desKey));
    }


    /**
     * Given the DH public key in <code>BigInteger</code> format,
     * recover the shared secrect and then convert it to a 3DES key.
     * @param peerPubKey Public key of the peer.
     * @param hostPriKey Private key of the host.
     * @return Triple DES secret key.
     */
    public SecretKey getSharedKey(BigInteger pubKey, PrivateKey hostPrivKey)
	throws GeneralSecurityException
    {
	DHPublicKeySpec dhKeySpec =
	    new DHPublicKeySpec(pubKey, prime1024, base1024);
	return getSharedKey(dhKF.generatePublic(dhKeySpec), hostPrivKey);
    }


    /**
     * @return the DH public key value (i.e., Y).
     */
    static public BigInteger getDHPublicKeyValue(KeyPair keypair) {
	return ((DHPublicKey) keypair.getPublic()).getY();
    }


    /**
     * @return the DH private Key, given X.
     */
    public PrivateKey getDHPrivateKey(BigInteger keyValue)
	throws GeneralSecurityException
    {
	DHPrivateKeySpec dhKeySpec =
	    new DHPrivateKeySpec(keyValue, prime1024, base1024);
	return dhKF.generatePrivate(dhKeySpec);
    }

    /**
     * 3DES encrypt the given plain text buffer using the specify key.
     */
    public byte[] encrypt(byte[] plaintext, SecretKey key)
	throws GeneralSecurityException
    {
	desCipher.init(Cipher.ENCRYPT_MODE, key, iv);
	return desCipher.doFinal(plaintext);
    }

    /**
     * 3DES descrypt the given cipher text buffer using the specified key.
     */
    public byte[] decrypt(byte[] cipherText, SecretKey key)
	throws GeneralSecurityException
    {
	desCipher.init(Cipher.DECRYPT_MODE, key, iv);
	return desCipher.doFinal(cipherText);
    }
}

package com.broadon.pas;

public class ECardBatchKey {

    private long eCardId;
    private int eCardType;
    
    public ECardBatchKey(long eCardId, int eCardType) {
        super();
        this.eCardId = eCardId;
        this.eCardType = eCardType;
    }

    long getECardId() {
        return eCardId;
    }

    int getECardType() {
        return eCardType;
    }

}

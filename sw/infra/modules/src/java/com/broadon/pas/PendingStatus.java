package com.broadon.pas;
import java.sql.Connection;

import com.broadon.wsapi.pas.AuthorizationStateType;
import com.broadon.wsapi.pas.CapturePaymentRequestIfc;
import com.broadon.wsapi.pas.CapturePaymentResponseIfc;
import com.broadon.wsapi.pas.GetAuthorizationHistoryRequestType;
import com.broadon.wsapi.pas.GetAuthorizationHistoryResponseType;
import com.broadon.wsapi.pas.RefundPaymentRequestType;
import com.broadon.wsapi.pas.RefundPaymentResponseType;
import com.broadon.wsapi.pas.VoidAuthorizationRequestType;
import com.broadon.wsapi.pas.VoidAuthorizationResponseType;
import com.broadon.wsapi.pas.PaymentMethodType;
import com.broadon.wsapi.pas.TransactionKindType;

public class PendingStatus implements BasicServiceInternal {

    private static BasicServiceInternal singleton = new PendingStatus();
    
    public static BasicServiceInternal getInstance() {
        return singleton;
    }
    
    private PendingStatus() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    public boolean capture(
            Connection conn,
            CapturePaymentRequestIfc capturePaymentRequest,
            CapturePaymentResponseIfc capturePaymentResponse,
            PasTransactionBean trans,
            String transType) {
        PaymentMethodType paymentMethod = capturePaymentRequest.getAuthorizationToken().getPaymentMethod();
        return PasFunction.getPaymentServiceImpl(paymentMethod).capture(
                conn, capturePaymentRequest, capturePaymentResponse, trans, transType);
    }

    public boolean refund(
            Connection conn,
            RefundPaymentRequestType refundPaymentRequest,
            RefundPaymentResponseType refundPaymentResponse,
            PasTransactionBean trans) {
        PaymentMethodType paymentMethod = refundPaymentRequest.getAuthorizationToken().getPaymentMethod();
        return PasFunction.getPaymentServiceImpl(paymentMethod).refund(
                conn, refundPaymentRequest, refundPaymentResponse, trans);
    }

    public boolean voidAuth(
            Connection conn,
            VoidAuthorizationRequestType voidAuthorizationRequest,
            VoidAuthorizationResponseType voidAuthorizationResponse,
            PasTransactionBean trans) {
        if (!trans.getTransType().equals(TransactionKindType.Authorize.toString()) &&
                trans.getAmountCaptured().compareTo(trans.getAmountRefunded()) != 0) {
            PasFunction.setErrorCode(StatusCode.PAS_NOT_IN_INITIAL_STATUS, 
                    PasConstants.CANNOT_VOID, voidAuthorizationResponse);
            // auth state
            AuthorizationStateType state = new AuthorizationStateType();
            PasFunction.initAuthStateFromTransBean(trans, state);
            voidAuthorizationResponse.setAuthorizationState(state);
            return false;
        }
        PaymentMethodType paymentMethod = voidAuthorizationRequest.getAuthorizationToken().getPaymentMethod();
        return PasFunction.getPaymentServiceImpl(paymentMethod).voidAuth(
                conn, voidAuthorizationRequest, voidAuthorizationResponse, trans);
    }

    public boolean getHistory(
            Connection conn,
            GetAuthorizationHistoryRequestType getHistoryRequest,
            GetAuthorizationHistoryResponseType getHistoryResponse,
            PasTransactionBean transData) {
        return false;
        // TODO Auto-generated method stub
    }
}

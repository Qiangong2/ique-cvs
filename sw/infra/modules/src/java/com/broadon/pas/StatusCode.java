package com.broadon.pas;

/**
 * PAS Response Code (800-899)
 */
public class StatusCode extends com.broadon.status.StatusCode {

    // Unknown Payment Method
    public static final String PAS_MISSING_DATA = "800";
    static final String PAS_MISSING_DATA_MSG =
        "PAS - Null Pointer -- missing data";
    public static final String PAS_UNKNOWN_PAYMENT_METHOD = "801";
    static final String PAS_UNKNOWN_PAYMENT_METHOD_MSG =
        "PAS - Unknown Payment Method";
    // Unmatched Currency Type
    public static final String PAS_CURRENCY_MISMATCH = "802";
    static final String PAS_CURRENCY_MISMATCH_MSG =
        "PAS - Currency Type Mismatch";
    // Number Format Error
    public static final String PAS_NUMBER_FORMAT_ERROR = "803";
    static final String PAS_NUMBER_FORMAT_ERROR_MSG =
        "PAS - Currency Number String Format Error";

    // Account Problems
    public static final String PAS_NOT_ENOUGH_MONEY = "810";    
    static final String PAS_NOT_ENOUGH_MONEY_MSG =
        "PAS - Not Enough Money or EXCEEDS LIMIT";
    public static final String PAS_ACCOUNT_EXPIRED = "811";    
    static final String PAS_ACCOUNT_EXPIRED_MSG =
        "PAS - Account Expired";
    public static final String PAS_ACCOUNT_REVOKED = "812";    
    static final String PAS_ACCOUNT_REVOKED_MSG =
        "PAS - Account Revoked";
    public static final String PAS_ACCOUNT_NOT_ACTIVATED = "813";    
    static final String PAS_ACCOUNT_NOT_ACTIVATED_MSG =
        "PAS - Account Not Activated";
    public static final String PAS_ACCOUNT_NOT_USABLE = "814";
    static final String PAS_ACCOUNT_NOT_USABLE_MSG = 
        "PAS - Account Not Usable";
    public static final String PAS_ACCOUNT_IS_USED_ONCE = "815";
    static final String PAS_ACCOUNT_IS_USED_ONCE_MSG = 
        "PAS - Account Has Been Used Once (UsedOnce Account)";
    public static final String PAS_ACCOUNT_IS_PREPAID = "816";
    static final String PAS_ACCOUNT_IS_PREPAID_MSG = 
        "PAS - Account Is Prepaid";
    public static final String PAS_ACCOUNT_NOT_REFILLABLE = "817";
    static final String PAS_ACCOUNT_NOT_REFILLABLE_MSG = 
        "PAS - Account Is Not Refillable";
    public static final String PAS_ACCOUNT_IS_TITLE_ONLY = "818";
    static final String PAS_ACCOUNT_IS_TITLE_ONLY_MSG = 
        "PAS - Account Is Title Only";
    public static final String PAS_NO_ACCOUNT_RECORD = "819";
    static final String PAS_NO_ACCOUNT_RECORD_MSG = 
        "PAS - Account Not Found";
    public static final String PAS_PIN_MISMATCH = "820";
    static final String PAS_PIN_MISMATCH_MSG = 
        "PAS - PIN Number Does Not Match";
    // CreditCard problem
    public static final String PAS_CREDITCARD_IF_ERROR = "825";
    static final String PAS_CREDITCARD_IF_ERROR_MSG =
        "PAS - CreditCard Interface Return Error, see Messages for details.";
    public static final String PAS_CREDITCARD_IF_BUSY = "826";
    static final String PAS_CREDITCARD_IF_BUSY_MSG =
        "PAS - CreditCard Processing System Busy, please try later.";
    // ECard problem
    public static final String PAS_INVALID_ECARD = "830";
    static final String PAS_INVALID_ECARD_MSG = 
        "PAS - ECard Verification Failed";
    public static final String PAS_ECARD_COUNTRY_CODE = "831";
    static final String PAS_ECARD_COUNTRY_CODE_MSG =
        "PAS - ECard Country Code Verification Failed";
    
    // Balance Account problem
//    public static final String PAS_SAME_ACCOUNT = "835";
//    static final String PAS_SAME_ACCOUNT_MSG = 
//        "PAS - Cannot Transfer to Same Account";
    
    // Authorization Status
    public static final String PAS_IN_INITIAL_STATUS = "840";
    static final String PAS_IN_INITIAL_STATUS_MSG =
        "PAS - This Authorization is in Initial Pending Status";
    public static final String PAS_IN_PENDING_STATUS = "841";
    static final String PAS_IN_PENDING_STATUS_MSG =
        "PAS - This Authorization is in Pending Status";
    public static final String PAS_IN_COMPLETE_STATUS = "842";
    static final String PAS_IN_COMPLETE_STATUS_MSG =
        "PAS - This Authorization is Completed";
    public static final String PAS_IN_VOID_STATUS = "843";
    static final String PAS_IN_VOID_STATUS_MSG =
        "PAS - This Authorization has already been Voided";
    public static final String PAS_IN_EXPIRED_STATUS = "844";
    static final String PAS_IN_EXPIRED_STATUS_MSG =
        "PAS - This Authorization has Expired";
    public static final String PAS_NOT_IN_INITIAL_STATUS = "845";
    static final String PAS_NOT_IN_INITIAL_STATUS_MSG =
        "PAS - This Authorization is not in Initial Pending Status";
    public static final String PAS_NOT_IN_PENDING_STATUS = "846";
    static final String PAS_NOT_IN_PENDING_STATUS_MSG =
        "PAS - This Authorization is not in Pending Status";
    public static final String PAS_NOT_IN_COMPLETE_STATUS = "847";
    static final String PAS_NOT_IN_COMPLETE_STATUS_MSG =
        "PAS - This Authorization is not Completed";
    public static final String PAS_NOT_IN_VOID_STATUS = "848";
    static final String PAS_NOT_IN_VOID_STATUS_MSG =
        "PAS - This Authorization is not Void";
    public static final String PAS_NOT_IN_EXPIRED_STATUS = "849";
    static final String PAS_NOT_IN_EXPIRED_STATUS_MSG =
        "PAS - This Authorization has not Expired";
    
    public static final String PAS_NO_AUTH_RECORD = "850";
    static final String PAS_NO_AUTH_RECORD_MSG =
        "PAS - Cannot Find Authorization Record";
    public static final String PAS_WRONG_TRANSACTION_TYPE = "851";
    static final String PAS_WRONG_TRANSACTION_TYPE_MSG =
        "PAS - Cannot Mix Purchase with Deposit";
    
    // Void -- (Partially) Captured Authorization Cannot Be Voided.
    public static final String PAS_CANNOT_VOID = "860";
    static final String PAS_CANNOT_VOID_MSG =
        "PAS - This Authorization has been Processed and Cannot be Voided";
    
    
    // Reject -- Rejected By Credit Card Service
    public static final String PAS_REJECTED_BY_EXTERNAL_SERVER = "870";
    static final String PAS_REJECTED_BY_EXTERNAL_SERVER_MSG =
        "PAS - This Authorization is Rejected by external server and can no longer be used";
    
    // Not Implemented Yet
    public static final String PAS_NO_IMPLEMENTATION = "890";
    static final String PAS_NO_IMPLEMENTATION_MSG =
        "PAS - This Feature is Not Implemented Yet";
    // Version Mismatch
    public static final String PAS_VERSION_MISMATCH = "898";
    static final String PAS_VERSION_MISMATCH_MSG = "PAS - Protocol Version Mismatch";
    // Internal Error
    public static final String PAS_INTERNAL_ERROR = "899";
    static final String PAS_INTERNAL_ERROR_MSG = 
        "PAS - Internal Error";

    static {
        //800
        addStatusCode(PAS_MISSING_DATA, PAS_MISSING_DATA_MSG);
        addStatusCode(PAS_UNKNOWN_PAYMENT_METHOD, PAS_UNKNOWN_PAYMENT_METHOD_MSG);
        addStatusCode(PAS_CURRENCY_MISMATCH, PAS_CURRENCY_MISMATCH_MSG);
        addStatusCode(PAS_NUMBER_FORMAT_ERROR, PAS_NUMBER_FORMAT_ERROR_MSG);
        //810
        addStatusCode(PAS_NOT_ENOUGH_MONEY, PAS_NOT_ENOUGH_MONEY_MSG);
        addStatusCode(PAS_ACCOUNT_EXPIRED, PAS_ACCOUNT_EXPIRED_MSG);
        addStatusCode(PAS_ACCOUNT_REVOKED, PAS_ACCOUNT_REVOKED_MSG);
        addStatusCode(PAS_ACCOUNT_NOT_ACTIVATED, PAS_ACCOUNT_NOT_ACTIVATED_MSG);
        addStatusCode(PAS_ACCOUNT_NOT_USABLE, PAS_ACCOUNT_NOT_USABLE_MSG);
        addStatusCode(PAS_ACCOUNT_IS_USED_ONCE, PAS_ACCOUNT_IS_USED_ONCE_MSG);
        addStatusCode(PAS_ACCOUNT_IS_PREPAID, PAS_ACCOUNT_IS_PREPAID_MSG);
        addStatusCode(PAS_ACCOUNT_NOT_REFILLABLE, PAS_ACCOUNT_NOT_REFILLABLE_MSG);
        addStatusCode(PAS_ACCOUNT_IS_TITLE_ONLY, PAS_ACCOUNT_IS_TITLE_ONLY_MSG);
        addStatusCode(PAS_NO_ACCOUNT_RECORD, PAS_NO_ACCOUNT_RECORD_MSG);
        addStatusCode(PAS_PIN_MISMATCH, PAS_PIN_MISMATCH_MSG);
        // 825
        addStatusCode(PAS_CREDITCARD_IF_ERROR, PAS_CREDITCARD_IF_ERROR_MSG);
        addStatusCode(PAS_CREDITCARD_IF_BUSY, PAS_CREDITCARD_IF_BUSY_MSG);
        // 830
        addStatusCode(PAS_INVALID_ECARD, PAS_INVALID_ECARD_MSG);
        addStatusCode(PAS_ECARD_COUNTRY_CODE, PAS_ECARD_COUNTRY_CODE_MSG);
        //addStatusCode(PAS_SAME_ACCOUNT, PAS_SAME_ACCOUNT_MSG);
        //840
        addStatusCode(PAS_IN_INITIAL_STATUS, PAS_IN_INITIAL_STATUS_MSG);
        addStatusCode(PAS_IN_PENDING_STATUS, PAS_IN_PENDING_STATUS_MSG);
        addStatusCode(PAS_IN_COMPLETE_STATUS, PAS_IN_COMPLETE_STATUS_MSG);
        addStatusCode(PAS_IN_VOID_STATUS, PAS_IN_VOID_STATUS_MSG);
        addStatusCode(PAS_IN_EXPIRED_STATUS, PAS_IN_EXPIRED_STATUS_MSG);
        addStatusCode(PAS_NOT_IN_INITIAL_STATUS, PAS_NOT_IN_INITIAL_STATUS_MSG);
        addStatusCode(PAS_NOT_IN_PENDING_STATUS, PAS_NOT_IN_PENDING_STATUS_MSG);
        addStatusCode(PAS_NOT_IN_COMPLETE_STATUS, PAS_NOT_IN_COMPLETE_STATUS_MSG);
        addStatusCode(PAS_NOT_IN_VOID_STATUS, PAS_NOT_IN_VOID_STATUS_MSG);
        addStatusCode(PAS_NOT_IN_EXPIRED_STATUS, PAS_NOT_IN_EXPIRED_STATUS_MSG);
        //850
        addStatusCode(PAS_NO_AUTH_RECORD, PAS_NO_AUTH_RECORD_MSG);
        addStatusCode(PAS_WRONG_TRANSACTION_TYPE, PAS_WRONG_TRANSACTION_TYPE_MSG);
        //860
        addStatusCode(PAS_CANNOT_VOID, PAS_CANNOT_VOID_MSG);
        //870
        addStatusCode(PAS_REJECTED_BY_EXTERNAL_SERVER, PAS_REJECTED_BY_EXTERNAL_SERVER_MSG);
        //890
        addStatusCode(PAS_NO_IMPLEMENTATION, PAS_NO_IMPLEMENTATION_MSG);
        addStatusCode(PAS_VERSION_MISMATCH, PAS_VERSION_MISMATCH_MSG);
        addStatusCode(PAS_INTERNAL_ERROR, PAS_INTERNAL_ERROR_MSG);
    }
}

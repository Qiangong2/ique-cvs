package com.broadon.pas;

import java.sql.Connection;
import java.sql.SQLException;
import java.math.BigDecimal;
import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.logging.Log;
import com.broadon.wsapi.pas.AuthorizationStateType;
import com.broadon.wsapi.pas.CapturePaymentRequestType;
import com.broadon.wsapi.pas.CapturePaymentResponseType;
import com.broadon.wsapi.pas.RefundPaymentRequestType;
import com.broadon.wsapi.pas.RefundPaymentResponseType;
import com.broadon.wsapi.pas.VoidAuthorizationRequestType;
import com.broadon.wsapi.pas.VoidAuthorizationResponseType;
import com.broadon.wsapi.pas.PasTokenType;
import com.broadon.wsapi.pas.TransactionKindType;

public abstract class AuthorizationBasicServiceImpl implements
        AuthorizationBasicService {

    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(AuthorizationBasicServiceImpl.class.getName());
    
    public AuthorizationBasicServiceImpl() {
        super();
    }

    public CapturePaymentResponseType capturePayment(
            PaymentAuthorizationServiceImpl service,
            CapturePaymentRequestType capturePaymentRequest) {
        Connection conn = null;
        CapturePaymentResponseType response = null;
        try {
            response = new CapturePaymentResponseType();
            PasFunction.initResponse(capturePaymentRequest, response);
            if (response.getErrorCode()!= PasConstants.STATUS_OK)
                return response;
            //      get AuthData
            conn = service.getConnection();
            PasTokenType token = capturePaymentRequest.getAuthorizationToken();
            if (token == null) {
                PasFunction.setErrorCode(StatusCode.PAS_MISSING_DATA, PasConstants.CANNOT_CAPTURE, response);
                return response;
            }
            PasTransactionBean trans = PasDBFunction.getTransactionBean(conn, token.getToken());
            if (trans == null) {
                PasFunction.setErrorCode(StatusCode.PAS_NO_AUTH_RECORD, PasConstants.CANNOT_CAPTURE, response);
                return response;
            }
            // check transaction type -- thisinterface is for purchase only
            TransactionKindType transType = TransactionKindType.fromString(trans.getTransType());
            if (transType != TransactionKindType.Purchase
                    && transType != TransactionKindType.Authorize) {
                PasFunction.setErrorCode(StatusCode.PAS_WRONG_TRANSACTION_TYPE, PasConstants.CANNOT_CAPTURE, response);
                return response;
            }
            // check currencyType consistency
            if (! capturePaymentRequest.getAmount().getCurrencyType().equalsIgnoreCase(trans.getCurrencyType())){
                PasFunction.setErrorCode(StatusCode.PAS_CURRENCY_MISMATCH, PasConstants.CANNOT_CAPTURE, response);
                // auth state
                AuthorizationStateType state = new AuthorizationStateType();
                PasFunction.initAuthStateFromTransBean(trans, state);
                response.setAuthorizationState(state);
                return response;
            }
            BigDecimal capture;
            try {
                capture = new BigDecimal(capturePaymentRequest.getAmount().getAmount());
            } catch (NumberFormatException nfe) {
                PasFunction.setErrorCode(StatusCode.PAS_NUMBER_FORMAT_ERROR, PasConstants.MONEY_FORMAT_ERROR, response);
                // auth state
                AuthorizationStateType state = new AuthorizationStateType();
                PasFunction.initAuthStateFromTransBean(trans, state);
                response.setAuthorizationState(state);
                return response;
            }
            // make sure data is consistent.
            // if capture is the amount to capture: capture + amountCaptured <= amountAuthorized.
            // since we interprete capture as "capture up to" to achieve idempotent, 
            // capture is "capture up to": capture <= amountAuthorized
            if ( capture.compareTo(trans.getAmountAuthorized()) > 0) {
                PasFunction.setErrorCode(StatusCode.PAS_NOT_ENOUGH_MONEY, PasConstants.CANNOT_CAPTURE, response);
                // auth state
                AuthorizationStateType state = new AuthorizationStateType();
                PasFunction.initAuthStateFromTransBean(trans, state);
                response.setAuthorizationState(state);
                return response;
            }
            // getStatus and do capture
            if (PasFunction.getStatusObject(trans.getStatus()).capture(conn, capturePaymentRequest, response, trans,
                    TransactionKindType.Purchase.toString()))
                conn.commit();
        } catch (SQLException e) {               
            PasFunction.setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
                log.debug("capturePayment error: ", e);
            
        } catch (Throwable e) {
            // Unexpected error, catch and log
            PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.PAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);
            log.error("capturePayment error: ", e);
        } finally {
            try {                
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            } catch (SQLException e) {
                log.warn("capturePayment error while closing connection: ", e);
            }
        }
        return response;
    }

    public RefundPaymentResponseType refundPayment(
            PaymentAuthorizationServiceImpl service,
            RefundPaymentRequestType refundPaymentRequest) {
        Connection conn = null;
        RefundPaymentResponseType response = null;
        try {
            response = new RefundPaymentResponseType();
            PasFunction.initResponse(refundPaymentRequest, response);
            if (response.getErrorCode()!= PasConstants.STATUS_OK)
                return response;
            //      get AuthData
            conn = service.getConnection();
            PasTokenType token = refundPaymentRequest.getAuthorizationToken();
            if (token == null) {
                PasFunction.setErrorCode(StatusCode.PAS_MISSING_DATA, PasConstants.CANNOT_REFUND, response);
                return response;
            }
            PasTransactionBean trans = PasDBFunction.getTransactionBean(conn, token.getToken());
            if (trans == null) {
                PasFunction.setErrorCode(StatusCode.PAS_NO_AUTH_RECORD, PasConstants.CANNOT_REFUND, response);
                return response;
            }
            // check currencyType consistency
            if (! refundPaymentRequest.getAmount().getCurrencyType().equalsIgnoreCase(trans.getCurrencyType())){
                PasFunction.setErrorCode(StatusCode.PAS_CURRENCY_MISMATCH, PasConstants.CANNOT_REFUND, response);
                // auth state
                AuthorizationStateType state = new AuthorizationStateType();
                PasFunction.initAuthStateFromTransBean(trans, state);
                response.setAuthorizationState(state);
                return response;
            }
            BigDecimal refundRequested;
            try {
                refundRequested = new BigDecimal(refundPaymentRequest.getAmount().getAmount());
            } catch (NumberFormatException nfe) {
                PasFunction.setErrorCode(StatusCode.PAS_NUMBER_FORMAT_ERROR, PasConstants.MONEY_FORMAT_ERROR, response);
                // auth state
                AuthorizationStateType state = new AuthorizationStateType();
                PasFunction.initAuthStateFromTransBean(trans, state);
                response.setAuthorizationState(state);
                return response;
            }
            // make sure data is consistent.
            // if refundRequested is the amount to refund: refundRequested + refundMade <= amountCaptured.
            // since we interprete refundRequested as "refund up to" to achieve idempotent, 
            // refundRequested is "refund up to": refundRequested <= amountCaptured
            //BigDecimal refundMade = trans.getAmountRefunded();
            BigDecimal amountCaptured = trans.getAmountCaptured();
            if (refundRequested.compareTo(amountCaptured) > 0) {
                PasFunction.setErrorCode(StatusCode.PAS_NOT_ENOUGH_MONEY, PasConstants.CANNOT_REFUND, response);
                // auth state
                AuthorizationStateType state = new AuthorizationStateType();
                PasFunction.initAuthStateFromTransBean(trans, state);
                response.setAuthorizationState(state);
                return response;
            }
            // check transaction type -- no refund for deposits
            String transType = trans.getTransType();
            if (TransactionKindType.fromString(transType) == TransactionKindType.Deposit) {
                PasFunction.setErrorCode(StatusCode.PAS_WRONG_TRANSACTION_TYPE, PasConstants.CANNOT_REFUND, response);
                return response;
            }
            // getStatus and do refund
            if (PasFunction.getStatusObject(trans.getStatus()).refund(conn, refundPaymentRequest, response, trans))
                conn.commit();
        } catch (SQLException e) {               
            PasFunction.setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
                log.debug("refundPayment error: ", e);
            
        } catch (Throwable e) {
            // Unexpected error, catch and log
            PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.PAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);
            log.error("refundPayment error: ", e);
            
        } finally {
            try {                
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            } catch (SQLException e) {
                log.warn("refundPayment error while closing connection: ", e);
            }
        }
        return response;
    }

    public VoidAuthorizationResponseType voidAuthorization(
            PaymentAuthorizationServiceImpl service,
            VoidAuthorizationRequestType voidAuthorizationRequest) {
        Connection conn = null;
        VoidAuthorizationResponseType response = null;
        try {
            response = new VoidAuthorizationResponseType();
            PasFunction.initResponse(voidAuthorizationRequest, response);
            if (response.getErrorCode()!= PasConstants.STATUS_OK)
                return response;
            //      get AuthData
            conn = service.getConnection();
            
            PasTokenType token = voidAuthorizationRequest.getAuthorizationToken();
            if (token == null) {
                PasFunction.setErrorCode(StatusCode.PAS_MISSING_DATA, PasConstants.CANNOT_VOID, response);
                return response;
            }
            PasTransactionBean trans = PasDBFunction.getTransactionBean(conn, token.getToken());
            if (trans == null) {
                PasFunction.setErrorCode(StatusCode.PAS_NO_AUTH_RECORD, PasConstants.CANNOT_VOID, response);
                return response;
            }

            // getStatus and do void
            BasicServiceInternal statusObj = PasFunction.getStatusObject(trans.getStatus());
            if (statusObj.voidAuth(conn, voidAuthorizationRequest, response, trans));
                conn.commit();
        } catch (SQLException e) {               
            PasFunction.setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                e.getLocalizedMessage(),
                response);
            log.debug("voidAuthorization error: ", e);
        
        } catch (Throwable e) {
            // Unexpected error, catch and log
            PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR,
                StatusCode.getMessage(StatusCode.PAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                response);
            log.error("voidAuthorization error: ", e);
        
        } finally {
            try {                
                if (conn != null) {
                    conn.rollback();
                    conn.close();
            }
        }
            catch (SQLException e) {
                log.warn("voidAuthorization error while closing connection: ", e);
            }
        }                
        return response;
    }
/*
    public GetAuthorizationHistoryResponseType getAuthorizationHistory(
            PaymentAuthorizationServiceImpl service,
            GetAuthorizationHistoryRequestType getAuthorizationHistoryRequest) {
        Connection conn = null;
        GetAuthorizationHistoryResponseType  response = null;
        try {
            //      get AuthData
            conn = service.getDataSource().getConnection();
            //      get AuthData
            //PasTransactionBean trans = new PasTransactionBean(
            //        getAuthorizationHistoryRequest.getAuthorizationToken().getToken().toString());
            PasTransactionBean trans = null;
            // create response object
            response = new GetAuthorizationHistoryResponseType();
            PasFunction.initResponse(getAuthorizationHistoryRequest, response);
            if (response.getErrorCode()!= PasConstants.STATUS_OK)
                return response;
            // getStatus and do getHistory
            PasFunction.getStatusObject(trans.getStatus()).getHistory(
                    conn, getAuthorizationHistoryRequest, response, trans);
        } catch (SQLException e) {               
            PasFunction.setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                e.getLocalizedMessage(),
                response);
            log.debug("getAuthorizationHistory error: ", e);
        
        } catch (Throwable e) {
            // Unexpected error, catch and log
            PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR,
                StatusCode.getMessage(StatusCode.PAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                response);
            log.error("getAuthorizationHistory error: ", e);
        
        } finally {
            try {                
                if (conn != null) {
                    conn.rollback();
                    conn.close();
            }
        }
            catch (SQLException e) {
                log.warn("getAuthorizationHistory error while closing connection: ", e);
            }
        }                
        return response;
    }
    */
}

package com.broadon.pas;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Timer;
import java.math.BigDecimal;

import javax.servlet.ServletContext;
import javax.sql.DataSource;
import javax.xml.rpc.ServiceException;
import javax.xml.rpc.server.ServiceLifecycle;
import javax.xml.rpc.server.ServletEndpointContext;

import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.logging.Log;

import com.broadon.db.OracleDataSource.OracleDataSourceProxy;
import com.broadon.servlet.ServletConstants;
import com.broadon.wsapi.pas.AuthorizationStatusType;
import com.broadon.wsapi.pas.BalanceAccountType;
import com.broadon.wsapi.pas.BalanceType;
import com.broadon.wsapi.pas.CaptureAccountToAccountRequestType;
import com.broadon.wsapi.pas.CaptureAccountToAccountResponseType;
import com.broadon.wsapi.pas.CaptureCreditCardToAccountRequestType;
import com.broadon.wsapi.pas.CaptureCreditCardToAccountResponseType;
import com.broadon.wsapi.pas.CaptureECardToAccountRequestType;
import com.broadon.wsapi.pas.CaptureECardToAccountResponseType;
import com.broadon.wsapi.pas.CapturePaymentToAccountRequestIfc;
import com.broadon.wsapi.pas.CapturePaymentToAccountResponseIfc;
import com.broadon.wsapi.pas.GetAuthorizationHistoryRequestType;
import com.broadon.wsapi.pas.GetAuthorizationHistoryResponseType;
import com.broadon.wsapi.pas.MoneyType;
import com.broadon.wsapi.pas.PasTokenType;
import com.broadon.wsapi.pas.PaymentMethodType;
import com.broadon.wsapi.pas.TransactionKindType;
import com.broadon.wsapi.pas.ActionKindType;

/**
 * PaymentAuthenticationServiceImpl provides the implementation for
 * the Payment Authentication Services.
 * 
 */
public class PaymentAuthorizationServiceImpl implements
    ServiceLifecycle, ServletConstants {
//  The servlet context
    protected ServletContext servletContext;
    private ServletEndpointContext soapContext;
    // The data source used to get database connection
    protected DataSource dataSource;    

    // The logging module
    private static Log log = 
        LogFactory.getLog(PaymentAuthorizationServiceImpl.class.getName());
    
    protected static CreditCardService ccs;
    protected static ECardService ecs;
    protected static BalanceAccountService bas;
    
    String DefaultECardAuthorizationExpiration = "20";   // 20 minutes
    String DefaultBalanceAccountAuthorizationExpiration = "20";   // 20 minutes
    String DefaultCreditCardAuthorizationExpiration = "20";  // 20 minutes
    
    private int ECardAuthorizationExpirationMinutes;
    private int BalanceAccountAuthorizationExpirationMinutes;
    private int CreditCardAuthorizationExpirationMinutes;
    
    String DefaultCCPaymentURL = "";
    String DefaultCCPointsApplyURL = "";
    String DefaultMaxCreditCardThreadLimit = "300";    //
    String DefaultCreditCardAuthTimeoutMillis = "3000"; // 3 seconds
    String DefaultPointsAppTimeoutMillis = "3000"; // 3 seconds
    String DefaultPointsApplyIntervalMillis = "120000"; // 2 minutes
    
    
    private String CCPaymentURL;
    private String CCPointsApplyURL;
    private int maxCreditCardThreadLimit;
    private long CreditCardAuthTimeoutMillis;
    private long PointsAppTimeoutMillis;
    private long PointsApplyIntervalMillis;
    
    public PaymentAuthorizationServiceImpl() {
        super();
        // Force initialization of StatusCode class
        try {
                Class.forName("com.broadon.pas.StatusCode");
        } catch  (ClassNotFoundException e) {
                log.error("Cannot initialize PAS StatusCode");
        }
    }

    public void init(Object context) throws ServiceException {
        // TODO Auto-generated method stub
        // initial three payment method impls
        ccs = CreditCardServiceImpl.getInstance();
        ecs = ECardServiceImpl.getInstance();
        bas = BalanceAccountServiceImpl.getInstance();
        log.debug("Payment Authorization Service Init");
        
        soapContext = (ServletEndpointContext) context; 
        servletContext = soapContext.getServletContext();
        
        dataSource = (DataSource)servletContext.getAttribute(DATA_SOURCE_KEY);
        PasDBFunction.init(this);
       //auditLog = (AuditLog) servletContext.getAttribute(Logger.LOGGER_KEY);
        
        // Obtain the properties set for IAS server in the file 
        // defined in conf/BBserver.properties.tmpl
        //
        Properties prop = (Properties) servletContext.getAttribute(PROPERTY_KEY);
        
        // Get device token timeout
        ECardAuthorizationExpirationMinutes = Integer.parseInt(getConfig(prop, "ECardAuthorizationExpirationMinutes",
                DefaultECardAuthorizationExpiration));
        CreditCardAuthorizationExpirationMinutes = Integer.parseInt(getConfig(prop, "CreditCardAuthorizationExpirationMinutes",
                DefaultCreditCardAuthorizationExpiration));
        BalanceAccountAuthorizationExpirationMinutes = Integer.parseInt(getConfig(prop, "BalanceAccountAuthorizationExpirationMinutes",
                DefaultBalanceAccountAuthorizationExpiration));
        
        maxCreditCardThreadLimit = Integer.parseInt(getConfig(prop, "MAX_CC_THREAD_LIMIT", DefaultMaxCreditCardThreadLimit));
        CCPaymentURL = getConfig(prop, "CC_PAYMENT_URL", DefaultCCPaymentURL);
        CCPointsApplyURL = getConfig(prop, "CC_POINTSAPPLY_URL", DefaultCCPointsApplyURL);
        
        CreditCardAuthTimeoutMillis = Long.parseLong(getConfig(prop, "AuthTimeoutMillis", DefaultCreditCardAuthTimeoutMillis));
        PointsAppTimeoutMillis = Long.parseLong(getConfig(prop, "PointsAppTimeoutMillis", DefaultPointsAppTimeoutMillis));
        PointsApplyIntervalMillis = Long.parseLong(getConfig(prop, "PointsApplyIntervalMillis", DefaultPointsApplyIntervalMillis));
        // init and schedule timer to run PointsApplied interface
        Timer timer = new Timer(true);
        timer.scheduleAtFixedRate(new PointsApply(this), PointsApplyIntervalMillis, PointsApplyIntervalMillis);
    }

    /**
     * Called by the JAX RPC runtime to destroy resources
     * used by the service endpoint at the end of its lifecycle.
     * Implements ServiceLifecycle.
     */
    public void destroy() 
    {
        log.debug("Payment Authorization Service Destroy");
    }

    // Member access functions
    /*
    public DataSource getDataSource()
    {
        return dataSource;
    }
    */
    
    // Get a DB Connection and set autoCommit to false
    public Connection getConnection() throws SQLException
    {
        Connection conn = dataSource.getConnection();
        conn.setAutoCommit(false);
        return conn;
    }
    
    //  Get a Oracle DB Connection and set autoCommit to false
    public Connection getConnection(int priority) throws SQLException
    {
        Connection conn = dataSource instanceof OracleDataSourceProxy ?
                ((OracleDataSourceProxy) dataSource).getConnection(priority) :
                    dataSource.getConnection();
        conn.setAutoCommit(false);
        return conn;
    }
    
    // common routine shared to deposit an authorization to an account
    // if input param depositAmount is null, it is calculated inside the routine
    // and check for currencyType match.
    // If it is given, use it and do not check for currency match.
    private CapturePaymentToAccountResponseIfc capturePaymentToAccount(
            CapturePaymentToAccountRequestIfc capturePaymentToAccountRequest,
            CapturePaymentToAccountResponseIfc response, MoneyType depositAmount) {
        // 1. check token and get transaction
        PasTokenType token = capturePaymentToAccountRequest.getAuthorizationToken();
        if (token == null) {
            PasFunction.setErrorCode(StatusCode.PAS_MISSING_DATA, PasConstants.NO_AUTHO_TOKEN, response);
            return response;
        }
        Connection conn = null;
        try {   
            conn = getConnection();
            PasTransactionBean trans = PasDBFunction.getTransactionBean(conn, token.getToken());
            if (trans == null) {
                PasFunction.setErrorCode(StatusCode.PAS_NO_AUTH_RECORD, PasConstants.CANNOT_CAPTURE, response);
                return response;
            }
            if (TransactionKindType.fromString(trans.getTransType()) != TransactionKindType.Deposit
                    && TransactionKindType.fromString(trans.getTransType()) != TransactionKindType.Authorize) {
                PasFunction.setErrorCode(StatusCode.PAS_WRONG_TRANSACTION_TYPE, 
                        PasConstants.CANNOT_CAPTURE, response);
                return response;
            }
            // 2. get toBalanceAccount and check currencyType match
            String transCurrencyType = trans.getCurrencyType();
            BalanceAccountType acct = capturePaymentToAccountRequest.getToBalanceAccount();
            AccountBean toAccount = PasFunction.checkBalanceAccount(
                    capturePaymentToAccountRequest, response, acct, true, true, conn);
            if (toAccount == null) // error
                return response;
            if (depositAmount == null) {
                if (! transCurrencyType.equalsIgnoreCase( toAccount.getCurrency())){
                    PasFunction.setErrorCode(StatusCode.PAS_CURRENCY_MISMATCH, 
                            PasConstants.CANNOT_DEPOSIT, response);
                    return null;
                }
            }
            // 3. capture -- withdraw and deposit should create records in transaction/action tables
            // 3.1 before capture, check if exceeds capture limit
            MoneyType captureTotal = capturePaymentToAccountRequest.getAmount();
            if (!PasFunction.checkCaptureAmount(trans, captureTotal )) {
                        PasFunction.setErrorCode(StatusCode.PAS_NOT_ENOUGH_MONEY, 
                                PasConstants.CANNOT_CAPTURE, response);
                        return response;
            } 
            // net capture amount
            BigDecimal captureAmount = new BigDecimal(captureTotal.getAmount());
            BigDecimal netDeposit;
            if (depositAmount != null)
                netDeposit = new BigDecimal(depositAmount.getAmount());
            else
                netDeposit = captureAmount.subtract(trans.getAmountCaptured());
            //???AuthorizationStateType state = new AuthorizationStateType();
            //PasFunction.initAuthStateFromTransBean(trans, state);
            //response.setAuthorizationState(state);
            if (! PasFunction.getStatusObject(trans.getStatus()).capture(conn, 
                    capturePaymentToAccountRequest, response, trans, TransactionKindType.Deposit.toString()))
                        return response;
            // 4. deposit
            BigDecimal newBalance;
            BigDecimal newLocked;
            if (toAccount.getAccountId() != Long.parseLong(trans.getPaymentMethodId())) {
                newBalance = toAccount.getBalance().add(netDeposit);
                newLocked = toAccount.getBalanceAuthorized();
            } else {    // authorization is from the same account
                newBalance = toAccount.getBalance();
                newLocked = toAccount.getBalanceAuthorized().subtract(netDeposit);
            }
            if (! PasDBFunction.updateBalanceAccount(conn, 
                    toAccount.getAccountId(), 
                    newBalance, newLocked)) {
                PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, 
                            PasConstants.CANNOT_DEPOSIT, response);
                return response;
            }
            // 5. create transaction
            String transId = PasDBFunction.createTransactionRecord(response, 
                    trans.getPaymentMethodId(), 
                            PaymentMethodType.ACCOUNT.toString(),
                            TransactionKindType.Deposit_to.toString(),
                            AuthorizationStatusType.Completed.toString(),
                            BalanceAccountAuthorizationExpirationMinutes,
                            conn);
            if (transId == null)
                        return response;
            // 6. create action
            String actionId = PasDBFunction.createActionRecord(conn, 
                            response, transId, ActionKindType.Deposit.toString(), 
                            netDeposit, AuthorizationStatusType.Completed.toString(), null);
            if (actionId == null)
                        return response;
            // 7. commit
            conn.commit();// update return state here before return
                    String currencyType = toAccount.getCurrency();
                    BalanceType balance = new BalanceType(
                            new MoneyType(currencyType, newBalance.toString()),
                            new MoneyType(currencyType, newLocked.toString()), 
                            new MoneyType(currencyType, newBalance.subtract(newLocked).toString()));
                    response.setBalance(balance);
            return response;
        } catch (SQLException e) {               
            PasFunction.setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                e.getLocalizedMessage(),
                response);
            // need to set other fields to null!
            log.debug("capturePayment error: ", e);
        } catch (Throwable e) {
            // Unexpected error, catch and log
            PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR,
                StatusCode.getMessage(StatusCode.PAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                response);
            // need to set other fields to null!
            log.error("capturePayment error: ", e);
        } finally {
            try {                
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            } catch (SQLException e) {
                log.warn("capturePayment error while closing connection: ", e);
            }
        }
        return response;
    }
    public CaptureAccountToAccountResponseType captureAccountToAccount(
            CaptureAccountToAccountRequestType captureAccountToAccountRequest) throws RemoteException {
        CaptureAccountToAccountResponseType response = new CaptureAccountToAccountResponseType();
        PasFunction.initResponse(captureAccountToAccountRequest, response);
        if (response.getErrorCode()!= PasConstants.STATUS_OK)
            return response;
        return (CaptureAccountToAccountResponseType)capturePaymentToAccount(
                captureAccountToAccountRequest, response, null);
    }
    
    public CaptureCreditCardToAccountResponseType captureCreditCardToAccount(
            CaptureCreditCardToAccountRequestType captureCreditCardToAccountRequest) throws RemoteException {
        CaptureCreditCardToAccountResponseType response = new CaptureCreditCardToAccountResponseType();
        PasFunction.initResponse(captureCreditCardToAccountRequest, response);
        if (response.getErrorCode()!= PasConstants.STATUS_OK)
            return response;
        return (CaptureCreditCardToAccountResponseType)capturePaymentToAccount(
                captureCreditCardToAccountRequest, response, captureCreditCardToAccountRequest.getDepositAmount());
    }
    public CaptureECardToAccountResponseType captureECardToAccount(
            CaptureECardToAccountRequestType captureECardToAccountRequest) throws RemoteException {
        CaptureECardToAccountResponseType response = new CaptureECardToAccountResponseType();
        PasFunction.initResponse(captureECardToAccountRequest, response);
        if (response.getErrorCode()!= PasConstants.STATUS_OK)
            return response;
        return (CaptureECardToAccountResponseType)capturePaymentToAccount(
                captureECardToAccountRequest, response, null);
 
    }

    public GetAuthorizationHistoryResponseType getAuthorizationHistory(
            GetAuthorizationHistoryRequestType getAuthorizationHistoryRequest) 
    throws java.rmi.RemoteException {
        // no distinction needed for different payment methods?
        GetAuthorizationHistoryResponseType response = new GetAuthorizationHistoryResponseType();
        PasFunction.initResponse(getAuthorizationHistoryRequest, response);
        if (response.getErrorCode()!= PasConstants.STATUS_OK)
            return response;
        PasTokenType token = getAuthorizationHistoryRequest.getAuthorizationToken();
        if (token == null) {
            PasFunction.setErrorCode(StatusCode.PAS_MISSING_DATA, PasConstants.NO_AUTHO_TOKEN, response);
            return response;
        }
        Connection conn = null;
        try {   
            conn = getConnection();
            if (! PasDBFunction.getAuthorizationHistory(conn, token.getToken(), response))
                PasFunction.setErrorCode(StatusCode.PAS_NO_AUTH_RECORD, 
                        PasConstants.CANNOT_GET_AUTHORIZATION_HISTORY, response);
            // return response; no need to commit, query only
        } catch (SQLException e) {               
            PasFunction.setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                e.getLocalizedMessage(),
                response);
            log.debug("capturePayment error: ", e);
        
        } catch (Throwable e) {
            // Unexpected error, catch and log
            PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR,
                StatusCode.getMessage(StatusCode.PAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                response);
            log.error("capturePayment error: ", e);
        
        } finally {
            try {                
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            } catch (SQLException e) {
                log.warn("capturePayment error while closing connection: ", e);
            }
        }
        return response;
    }

    int getECardAuthorizationExpirationMinutes() {
        return ECardAuthorizationExpirationMinutes;
    }
    int getCreditCardAuthorizationExpirationMinutes() {
        return CreditCardAuthorizationExpirationMinutes;
    }
    int getBalanceAccountAuthorizationExpirationMinutes() {
        return BalanceAccountAuthorizationExpirationMinutes;
    }
    String getCCPaymentURL() {
        return CCPaymentURL;
    }
    String getCCPointsApplyURL() {
        return CCPointsApplyURL;
    }
    int getMaxCreditCardThreadLimit() {
        return maxCreditCardThreadLimit;
    }
    long getCreditCardAuthTimeoutMillis() {
        return CreditCardAuthTimeoutMillis;
    }
    long getPointsAppTimeoutMillis() {
        return PointsAppTimeoutMillis;
    }
    // Private functions
    
    private String getConfig(Properties prop, String key, String defValue)
    {
        String value = (prop != null)? prop.getProperty(key): null;
        if (value == null || value.equals("")) {
            value = defValue;
            log.warn("BBserver.properties missing " + key + " value" +
                     "... using default of " + defValue);
        }
        return value;
    }
}

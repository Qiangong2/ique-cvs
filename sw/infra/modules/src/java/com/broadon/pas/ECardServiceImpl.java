package com.broadon.pas;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.math.BigDecimal;
import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.logging.Log;

import com.broadon.wsapi.pas.AbstractRequestType;
import com.broadon.wsapi.pas.AbstractResponseType;
import com.broadon.wsapi.pas.AuthorizationStateType;
import com.broadon.wsapi.pas.AuthorizeECardPaymentRequestType;
import com.broadon.wsapi.pas.AuthorizeECardPaymentResponseType;
import com.broadon.wsapi.pas.CapturePaymentRequestIfc;
import com.broadon.wsapi.pas.CapturePaymentResponseIfc;
import com.broadon.wsapi.pas.CheckECardBalanceRequestType;
import com.broadon.wsapi.pas.CheckECardBalanceResponseType;
import com.broadon.wsapi.pas.ECardRecordType;
import com.broadon.wsapi.pas.ECardTypeCountriesType;
import com.broadon.wsapi.pas.GetAuthorizationHistoryRequestType;
import com.broadon.wsapi.pas.GetAuthorizationHistoryResponseType;
import com.broadon.wsapi.pas.GetECardHistoryRequestType;
import com.broadon.wsapi.pas.GetECardHistoryResponseType;
import com.broadon.wsapi.pas.GetECardTypeRequestType;
import com.broadon.wsapi.pas.GetECardTypeResponseType;
import com.broadon.wsapi.pas.RefundPaymentRequestType;
import com.broadon.wsapi.pas.RefundPaymentResponseType;
import com.broadon.wsapi.pas.VoidAuthorizationRequestType;
import com.broadon.wsapi.pas.VoidAuthorizationResponseType;
import com.broadon.wsapi.pas.MoneyType;
import com.broadon.wsapi.pas.PasTokenType;
import com.broadon.wsapi.pas.PaymentMethodType;
import com.broadon.wsapi.pas.TransactionKindType;
import com.broadon.wsapi.pas.AuthorizationStatusType;
import com.broadon.wsapi.pas.ECardInfoType;
import com.broadon.wsapi.pas.ActionKindType;
import com.broadon.wsapi.pas.BalanceType;
import com.broadon.util.UUID;

public class ECardServiceImpl extends AuthorizationBasicServiceImpl 
    implements ECardService, BasicServiceInternal{
    
    private static ECardService singleton = new ECardServiceImpl();
    private static BigDecimal bigDecimal0 = new BigDecimal("0");
    
    public static ECardService getInstance() {
        return singleton;
    }
    
    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(ECardServiceImpl.class.getName());
     
    private ECardServiceImpl() {
        super();
    }

    /**
     * Authorize ECard payment request
     */
    public AuthorizeECardPaymentResponseType authorizeECardPayment(
            PaymentAuthorizationServiceImpl service,
            AuthorizeECardPaymentRequestType authorizeECardPaymentRequest) {
        Connection conn = null;
        AuthorizeECardPaymentResponseType response = null;
        try {
            response = new AuthorizeECardPaymentResponseType();
            PasFunction.initResponse(authorizeECardPaymentRequest, response);
            if (response.getErrorCode()!= PasConstants.STATUS_OK)
                return response;
            conn = service.getConnection();
            ECardRecordType eCardRecord = authorizeECardPaymentRequest.getECardID();
            // retrieve and verify eCard, verify everything
            ECardBean eCard = PasFunction.checkECard(authorizeECardPaymentRequest,
                    response, eCardRecord, PasFunction.WITHDRAW, conn);
            if (eCard == null)
                    return response;
            Date today = PasDBFunction.getCurrentDBTime(conn);
            if (today == null) {
                PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, 
                        PasConstants.CANNOT_AUTHORIZE, response);
                return response;
            }
            Date aDate = eCard.getActivateDate();
            if (aDate == null || aDate.after(today)) { // not activated
                PasFunction.setErrorCode(StatusCode.PAS_ACCOUNT_NOT_ACTIVATED, 
                        PasConstants.CANNOT_AUTHORIZE, response);
                return response;
            }
            MoneyType mTypeRequested = authorizeECardPaymentRequest.getAmount();
            if (mTypeRequested == null) {
                PasFunction.setErrorCode(StatusCode.PAS_MISSING_DATA, 
                        PasConstants.CANNOT_AUTHORIZE, response);
                return response;
            }
            String currency = mTypeRequested.getCurrencyType();
            if (currency != null && !currency.equalsIgnoreCase(eCard.getCurrency())) {
                PasFunction.setErrorCode(StatusCode.PAS_CURRENCY_MISMATCH, 
                        PasConstants.CANNOT_AUTHORIZE, response);
                return response;
            }

            BigDecimal balance = eCard.getBalance();
            BigDecimal amountRequested;
            try {
                amountRequested = new BigDecimal(mTypeRequested.getAmount());
            } catch (NumberFormatException nfe) {
                PasFunction.setErrorCode(StatusCode.PAS_NUMBER_FORMAT_ERROR, PasConstants.MONEY_FORMAT_ERROR, response);
                
                return response;
            }
            BigDecimal newBalanceAuthorized = eCard.getBalanceAuthorized().add(amountRequested);
            if (newBalanceAuthorized.compareTo(balance) > 0) {  // not enough balance
                // first try to release locked amounts from expired auth.
                if (PasDBFunction.releaseECardExpiredLockedAmount(conn, eCard)) {
                    balance = eCard.getBalance(); // should remains the same
                    newBalanceAuthorized = eCard.getBalanceAuthorized().add(amountRequested);
                }
                if (newBalanceAuthorized.compareTo(balance) > 0) {  // still not enough balance
                    PasFunction.setErrorCode(StatusCode.PAS_NOT_ENOUGH_MONEY, 
                        PasConstants.CANNOT_AUTHORIZE, response);
                    return response;
                }
            }
            // update eCard info
            if (! PasDBFunction.updateECardAccount(conn, eCard.getECardId(), balance, newBalanceAuthorized)) {
                PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, 
                        PasConstants.CANNOT_AUTHORIZE, response);
                return response;
            }
            // create paymentAuthorization Token
            PasTokenType authToken = new PasTokenType();
            UUID uuid = UUID.randomUUID();
            authToken.setPaymentMethod(PaymentMethodType.ECARD);
            authToken.setToken(uuid.toByteArray());
            //response.setAuthorizationToken(authToken);
            
            MoneyType authorizedAmount = new MoneyType(currency, amountRequested.toString());
            MoneyType capturedAmount = new MoneyType(currency, "0");
            MoneyType refundedAmount = new MoneyType(currency, "0");
            long creationTime = today.getTime();
            long expTime = creationTime+service.getECardAuthorizationExpirationMinutes()*1000*60;
            AuthorizationStateType authState = new AuthorizationStateType(
                    com.broadon.wsapi.pas.AuthorizationStatusType.Pending,
                    creationTime,
                    expTime,
                    authorizedAmount,
                    capturedAmount,
                    refundedAmount,
                    false);
            //response.setAuthorizationState(authState);
            
            // insert transaction to db
            PasTransactionBean transaction = new PasTransactionBean();
            transaction.setTransId(uuid.toBase64());    // use base64 string in db
            transaction.setPaymentMethodId(new Long(eCard.getECardId()).toString());
            transaction.setPaymentMethod(PaymentMethodType.ECARD.toString());
            transaction.setTransType(TransactionKindType.Authorize.toString());
            transaction.setStatus(AuthorizationStatusType.Pending.toString());
            transaction.setAmountAuthorized(amountRequested);
            transaction.setAmountCaptured(bigDecimal0);
            transaction.setAmountRefunded(bigDecimal0);
            transaction.setCurrencyType(currency);
            transaction.setReferenceId(authorizeECardPaymentRequest.getReferenceID());
            if (! PasDBFunction.insertPasTransaction(conn, transaction, service.getECardAuthorizationExpirationMinutes())) {
                PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, 
                        PasConstants.CANNOT_AUTHORIZE, response);
                return response;
            }
            conn.commit();
            response.setAuthorizationToken(authToken);
            response.setAuthorizationState(authState);
        } catch (SQLException e) {               
            PasFunction.setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.debug("authorizeECardPayment error: ", e);
            
        } catch (Throwable e) {
            // Unexpected error, catch and log
            PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.PAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);
            log.error("authorizeECardPayment error: ", e);
            
        } finally {
            try {                
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            }
            catch (SQLException e) {
                log.warn("authorizeECardPayment error while closing connection: ", e);
            }
        }                
        return response;
    }

    public GetECardHistoryResponseType getECardAccountHistory(
            PaymentAuthorizationServiceImpl service,
            GetECardHistoryRequestType getECardHistoryRequest) {
        Connection conn = null;
        GetECardHistoryResponseType response = null;
        try {
            response = new GetECardHistoryResponseType();
            PasFunction.initResponse(getECardHistoryRequest, response);
            if (response.getErrorCode()!= PasConstants.STATUS_OK)
                return response;
            conn = service.getConnection();
            ECardRecordType eCardRecord = getECardHistoryRequest.getECardID();
            // retrieve and verify eCard, validate ecard but ignore other checking
            ECardBean eCard = PasFunction.checkECard(getECardHistoryRequest,
                    response, eCardRecord, PasFunction.VALIDATE, conn);
            if (eCard == null)
                    return response;
            
            BigDecimal balance = eCard.getBalance();
            BigDecimal balanceAuthorized = eCard.getBalanceAuthorized();
            String currencyType = eCard.getCurrency();
            BalanceType currentBalance = new BalanceType(
                    new MoneyType(currencyType, balance.toString()),
                    new MoneyType(currencyType, balanceAuthorized.toString()),
                    new MoneyType(currencyType, balance.subtract(balanceAuthorized).toString()));
            response.setCurrentBalance(currentBalance);
            // add implementing AccountHistory to the generated GetECardHistoryResponseType
            // to remove compilation error of the following line.
            if (! PasDBFunction.getAccountHistory(conn, 
                    String.valueOf(eCard.getECardId()),
                    eCard.getBalance(),
                    PaymentMethodType.ECARD.toString(),
                    getECardHistoryRequest.getBegin(), getECardHistoryRequest.getEnd(), response))
                PasFunction.setErrorCode(StatusCode.PAS_NO_ACCOUNT_RECORD, 
                        PasConstants.CANNOT_GET_ACCOUNT_HISTORY, response);
            // return response; no need to commit, query only
        } catch (SQLException e) {               
            PasFunction.setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                e.getLocalizedMessage(),
                response);
            log.debug("getECardAccountHistory error: ", e);
        
        } catch (Throwable e) {
            // Unexpected error, catch and log
            PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR,
                StatusCode.getMessage(StatusCode.PAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                response);
            log.error("getECardAccountHistory error: ", e);
        
        } finally {
            try {                
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            }
            catch (SQLException e) {
                log.warn("getECardAccountHistory error while closing connection: ", e);
            }
        }                
        return response;
    }

    public CheckECardBalanceResponseType checkECardBalance(
            PaymentAuthorizationServiceImpl service,
            CheckECardBalanceRequestType checkECardBalanceRequest) {
        Connection conn = null;
        CheckECardBalanceResponseType response = null;
        try {
            response = new CheckECardBalanceResponseType();
            PasFunction.initResponse(checkECardBalanceRequest, response);
            if (response.getErrorCode()!= PasConstants.STATUS_OK)
                return response;
            conn = service.getConnection();
            ECardRecordType eCardRecord = checkECardBalanceRequest.getECardID();
            // retrieve and verify eCard, validate ecard but ignore other checking
            ECardBean eCard = PasFunction.checkECard(checkECardBalanceRequest,
                    response, eCardRecord, PasFunction.VALIDATE, conn);
            if (eCard == null)
                    return response;
            
            ECardInfoType eCardInfo = new ECardInfoType();
            PasFunction.initECardInfoFromECardBean(eCard, eCardInfo);
            response.setECardInfo(eCardInfo);
            conn.commit();
        } catch (SQLException e) {               
            PasFunction.setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.debug("checkECardBalance error: ", e);
            
        } catch (Throwable e) {
            // Unexpected error, catch and log
            PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.PAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);
            log.error("checkECardBalance error: ", e);
            
        } finally {
            try {                
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            }
            catch (SQLException e) {
                log.warn("checkECardBalance error while closing connection: ", e);
            }
        }                
        return response;
    }
    public GetECardTypeResponseType getECardType(
            PaymentAuthorizationServiceImpl service,
            GetECardTypeRequestType getECardTypeRequest)
    {
        Connection conn = null;
        GetECardTypeResponseType response = null;
        try {
            response = new GetECardTypeResponseType();
            PasFunction.initResponse(getECardTypeRequest, response);
            if (response.getErrorCode()!= PasConstants.STATUS_OK)
                return response;
            conn = service.getConnection();
            String eCardSeq = getECardTypeRequest.getECardNumber();
            // retrieve eCard, don't use PasFunction.checkECard because the method requires
            // a complete ECardRecord object.
            ECardRecordType eCardRecord = new ECardRecordType();
            eCardRecord.setECardNumber(eCardSeq);
            ECardBean eCard = PasFunction.checkECard(getECardTypeRequest, response,
                    eCardRecord, PasFunction.INFO_ONLY, conn);
            //int eCardId = Integer.parseInt(eCardSeq);
            //ECardBean eCard = PasDBFunction.getECard(conn, eCardId);
            if (eCard == null) {
                PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, 
                        PasConstants.NO_ECARD_RECORD, response);
                return response;
            }
            //String[] countryCodes = PasDBFunction.getCountryCodes(conn, eCardId, eCard.getECardType());
            String[] countryCodes = eCard.getCountryCode();
            if (countryCodes == null) {
                PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, 
                        PasConstants.NO_ECARD_RECORD, response);
                return response;
            }
            ECardTypeCountriesType eCardInfo = new ECardTypeCountriesType();
            eCardInfo.setCountries(countryCodes);
            String retEcardType = PasFunction.getPaddedECardType(new Integer(eCard.getECardType()).toString());
            eCardInfo.setECardType(retEcardType);
            response.setECardType(eCardInfo);
            conn.commit();
        } catch (SQLException e) {               
            PasFunction.setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.debug("checkECardBalance error: ", e);
            
        } catch (Throwable e) {
            // Unexpected error, catch and log
            PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.PAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);
            log.error("checkECardBalance error: ", e);
            
        } finally {
            try {                
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            }
            catch (SQLException e) {
                log.warn("checkECardBalance error while closing connection: ", e);
            }
        }                
        return response;
    }
    
    public boolean capture(Connection conn,
            CapturePaymentRequestIfc capturePaymentRequest, 
            CapturePaymentResponseIfc capturePaymentResponse, 
            PasTransactionBean trans,
            String transType) {
        // need to update transaction and action and eCard table
        ECardBean eCard = PasDBFunction.getECard(conn, Integer.parseInt(trans.getPaymentMethodId()));
        if (eCard == null) {
            PasFunction.setErrorCode(StatusCode.PAS_NO_ACCOUNT_RECORD, 
                    PasConstants.CANNOT_CAPTURE, capturePaymentResponse);
            return false;
        }
        // check for expiration  -- removed to allow ecs to do its work
        /*
        if (trans.getExpirationDate().before(PasDBFunction.getCurrentDBTime(conn))) {
            if (! PasDBFunction.updateTransactionStatus(conn, trans, AuthorizationStatusType.Expired.toString())) {
                PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, 
                        PasConstants.CANNOT_CAPTURE, capturePaymentResponse);
                return false;
            }
            // release uncaptured authorization
            if (! PasDBFunction.updateECardAccount(conn, 
                    eCard.getECardId(), eCard.getBalance(), 
                    eCard.getBalanceAuthorized().subtract(trans.getAmountAuthorized()).add(trans.getAmountCaptured()))) {
                PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, 
                        PasConstants.CANNOT_CAPTURE, capturePaymentResponse);
                return false;
            }
            PasFunction.setErrorCode(StatusCode.PAS_ACCOUNT_EXPIRED, 
                    PasConstants.CANNOT_CAPTURE, capturePaymentResponse);
            return true;    // so the changes will be committed
        }
        */
        BigDecimal newCaptureTotal;
        try {
            newCaptureTotal = new BigDecimal(capturePaymentRequest.getAmount().getAmount());
        } catch (NumberFormatException nfe) {
            PasFunction.setErrorCode(StatusCode.PAS_NUMBER_FORMAT_ERROR, PasConstants.MONEY_FORMAT_ERROR, capturePaymentResponse);
            return false;
        }
        BigDecimal newCaptured = newCaptureTotal.subtract(trans.getAmountCaptured());   // new amount to capture
        BigDecimal newAmountRefunded = trans.getAmountRefunded();    //won't change
        BigDecimal newBalance = eCard.getBalance().subtract(newCaptured);
        // check newBalance > 0??   NO, since we allowed expired auth to capture
        
        // new locked amount
        BigDecimal newBalanceAuthorized = eCard.getBalanceAuthorized().subtract(newCaptured);

        String newStatus = trans.getStatus();
        if (capturePaymentRequest.isIsFinal()) {
            newStatus = AuthorizationStatusType.Completed.toString();
            // unlock unused amount, if any
            newBalanceAuthorized = newBalanceAuthorized.subtract(trans.getAmountAuthorized()).add(newCaptureTotal);
        }
       
        if (newCaptureTotal.compareTo(trans.getAmountAuthorized()) == 0)
            newStatus = AuthorizationStatusType.Completed.toString();
        // eCard table
        if (! PasDBFunction.updateECardAccount(conn, eCard.getECardId(), newBalance, newBalanceAuthorized)) {
            PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, 
                    PasConstants.CANNOT_CAPTURE, capturePaymentResponse);
            return false;
        }
        // transaction table
        if (! PasDBFunction.updateTransactionRecord(conn, 
                trans.getTransId(), transType, newStatus, newCaptureTotal, newAmountRefunded)) {
            PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, 
                    PasConstants.CANNOT_CAPTURE, capturePaymentResponse);
            return false;
        }
        // action table
        String actionKind;
        if (capturePaymentRequest.isIsFinal())
            actionKind = ActionKindType.LastCap.toString();
        else
            actionKind = ActionKindType.Capture.toString();
        String actionId = PasDBFunction.createActionRecord(conn, 
                capturePaymentResponse, trans.getTransId(), actionKind, newCaptured, trans.getStatus(), null);
        if (actionId == null)
            return false;
        // prepare response
        trans.setStatus(newStatus);
        trans.setAmountCaptured(newCaptureTotal);
        AuthorizationStateType authState = new AuthorizationStateType();
        PasFunction.initAuthStateFromTransBean(trans, authState);
        capturePaymentResponse.setAuthorizationState(authState);
        return true;
        
    }

    public boolean refund(Connection conn,
            RefundPaymentRequestType refundPaymentRequest, 
            RefundPaymentResponseType refundPaymentResponse, 
            PasTransactionBean trans) {
//      need to update transaction and action and eCard table
        ECardBean eCard = PasDBFunction.getECard(conn, Integer.parseInt(trans.getPaymentMethodId()));
        if (eCard == null) {
            PasFunction.setErrorCode(StatusCode.PAS_NO_ACCOUNT_RECORD, 
                    PasConstants.CANNOT_REFUND, refundPaymentResponse);
            return false;
        }
        BigDecimal newRefundTotal;
        try {
            newRefundTotal = new BigDecimal(refundPaymentRequest.getAmount().getAmount());
        } catch (NumberFormatException nfe) {
            PasFunction.setErrorCode(StatusCode.PAS_NUMBER_FORMAT_ERROR, PasConstants.MONEY_FORMAT_ERROR, refundPaymentResponse);
            return false;
        }
        BigDecimal newRefund = newRefundTotal.subtract(trans.getAmountRefunded());
        BigDecimal newAmountCaptured = trans.getAmountCaptured();    // won't change
        BigDecimal newBalance = eCard.getBalance().add(newRefund);
        BigDecimal newBalanceAuthorized = eCard.getBalanceAuthorized();  // won't change
        String newStatus = trans.getStatus();   // won't change
        // eCard table
        if (! PasDBFunction.updateECardAccount(conn, eCard.getECardId(), newBalance, newBalanceAuthorized)) {
            PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, 
                    PasConstants.CANNOT_REFUND, refundPaymentResponse);
            return false;
        }
        // transaction table
        if (! PasDBFunction.updateTransactionRecord(conn, 
                trans.getTransId(), trans.getTransType(), newStatus, newAmountCaptured, newRefundTotal)) {
            PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, 
                    PasConstants.CANNOT_REFUND, refundPaymentResponse);
            return false;
        }
        // action table
        String actionId = PasDBFunction.createActionRecord(conn,
                refundPaymentResponse, trans.getTransId(), ActionKindType.Refund.toString(), newRefund, trans.getStatus(), null);
        if (actionId == null)
            return false;
        // prepare response
        trans.setAmountRefunded(newRefundTotal);
        AuthorizationStateType authState = new AuthorizationStateType();
        PasFunction.initAuthStateFromTransBean(trans, authState);
        refundPaymentResponse.setAuthorizationState(authState);
        return true;
    }

    public boolean voidAuth(Connection conn,
            VoidAuthorizationRequestType voidAuthorizationRequest, 
            VoidAuthorizationResponseType voidAuthorizationResponse, 
            PasTransactionBean transData) {
        ECardBean eCard = PasDBFunction.getECard(conn, Integer.parseInt(transData.getPaymentMethodId()));
        if (eCard == null) {
            PasFunction.setErrorCode(StatusCode.PAS_NO_ACCOUNT_RECORD, 
                    PasConstants.CANNOT_VOID, voidAuthorizationResponse);
            return false;
        }

        BigDecimal amountAuthorized = transData.getAmountAuthorized().subtract(transData.getAmountRefunded());
        BigDecimal newBalance = eCard.getBalance();  // won't change
        BigDecimal newLocked = eCard.getBalanceAuthorized().subtract(amountAuthorized);
        if (newLocked.compareTo(bigDecimal0)<0) {
            PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, 
                    PasConstants.CANNOT_VOID, voidAuthorizationResponse);
            return false;
        }
        // update eCard info
        if (! PasDBFunction.updateECardAccount(conn, eCard.getECardId(), newBalance, newLocked)) {
            PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, 
                    PasConstants.CANNOT_VOID, voidAuthorizationResponse);
            return false;
        }
        // update transaction
        if (! PasDBFunction.updateTransactionStatus(conn, transData.getTransId(), AuthorizationStatusType.Void.toString())) {
            PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, 
                    PasConstants.CANNOT_VOID, voidAuthorizationResponse);
            return false;
        }
        transData.setStatus(AuthorizationStatusType.Void.toString());
        AuthorizationStateType authState = new AuthorizationStateType();
        PasFunction.initAuthStateFromTransBean(transData, authState);
        voidAuthorizationResponse.setAuthorizationState(authState);
        return true;
    }

    public boolean getHistory(Connection conn,
            GetAuthorizationHistoryRequestType getAuthorizationHistoryRequest, 
            GetAuthorizationHistoryResponseType getAuthorizationHistoryResponse, 
            PasTransactionBean transData) {
        return false;
        // TODO Auto-generated method stub
        
    }

}

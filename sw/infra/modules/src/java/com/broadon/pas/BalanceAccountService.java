package com.broadon.pas;

public interface BalanceAccountService extends AuthorizationBasicService{
    public com.broadon.wsapi.pas.AuthorizeAccountPaymentResponseType authorizeAccountPayment(
            PaymentAuthorizationServiceImpl service,
            com.broadon.wsapi.pas.AuthorizeAccountPaymentRequestType authorizeAccountPaymentRequest);
    public com.broadon.wsapi.pas.GetAccountHistoryResponseType getBalanceAccountHistory(
            PaymentAuthorizationServiceImpl service,
            com.broadon.wsapi.pas.GetAccountHistoryRequestType getAccountHistoryRequest);
    /*
    public com.broadon.wsapi.pas.TransferBalanceResponseType transferBalance(
            PaymentAuthorizationServiceImpl service,
            com.broadon.wsapi.pas.TransferBalanceRequestType transferBalanceRequest);
            */
    public com.broadon.wsapi.pas.CheckAccountBalanceResponseType checkAccountBalance(
            PaymentAuthorizationServiceImpl service,
            com.broadon.wsapi.pas.CheckAccountBalanceRequestType checkAccountBalanceRequest);
    public com.broadon.wsapi.pas.DisableAccountResponseType disableAccount(
            PaymentAuthorizationServiceImpl service,
            com.broadon.wsapi.pas.DisableAccountRequestType disableAccountRequest);
    public com.broadon.wsapi.pas.ResetAccountPINResponseType resetAccountPIN(
            PaymentAuthorizationServiceImpl service,
            com.broadon.wsapi.pas.ResetAccountPINRequestType resetAccountPINRequest);
    public com.broadon.wsapi.pas.CreateAccountResponseType createAccount(
            PaymentAuthorizationServiceImpl service,
            com.broadon.wsapi.pas.CreateAccountRequestType createAccountRequest);
}

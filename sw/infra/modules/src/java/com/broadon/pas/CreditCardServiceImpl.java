package com.broadon.pas;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;

import net.wiivc.services.AuthTxn;
import net.wiivc.services.CreditCardFunctions;
import net.wiivc.services.JAXBAgent;
import net.wiivc.services.NonBlockingAgent;

import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.logging.Log;

import com.broadon.util.UUID;
import com.broadon.wsapi.pas.ActionKindType;
import com.broadon.wsapi.pas.AuthorizationStateType;
import com.broadon.wsapi.pas.AuthorizationStatusType;
import com.broadon.wsapi.pas.AuthorizeCreditCardPaymentRequestType;
import com.broadon.wsapi.pas.AuthorizeCreditCardPaymentResponseType;
import com.broadon.wsapi.pas.CapturePaymentRequestIfc;
import com.broadon.wsapi.pas.CapturePaymentResponseIfc;
import com.broadon.wsapi.pas.GetAuthorizationHistoryRequestType;
import com.broadon.wsapi.pas.GetAuthorizationHistoryResponseType;
import com.broadon.wsapi.pas.GetCreditCardHistoryRequestType;
import com.broadon.wsapi.pas.GetCreditCardHistoryResponseType;
import com.broadon.wsapi.pas.MoneyType;
import com.broadon.wsapi.pas.PasTokenType;
import com.broadon.wsapi.pas.PaymentMethodType;
import com.broadon.wsapi.pas.RefundPaymentRequestType;
import com.broadon.wsapi.pas.RefundPaymentResponseType;
import com.broadon.wsapi.pas.TransactionKindType;
import com.broadon.wsapi.pas.VoidAuthorizationRequestType;
import com.broadon.wsapi.pas.VoidAuthorizationResponseType;

public class CreditCardServiceImpl extends AuthorizationBasicServiceImpl
        implements CreditCardService, BasicServiceInternal{

    private static BigDecimal bigDecimal0 = new BigDecimal("0");
    private static CreditCardService singleton = new CreditCardServiceImpl();
    private static int creditCardInterfaceThreadCount = 0;
    private static Object syncObj = new Object();
    
    private boolean increaseCCIFThreadCount(PaymentAuthorizationServiceImpl service) {
        synchronized(syncObj) {
        if (service.getMaxCreditCardThreadLimit()<=creditCardInterfaceThreadCount) {
            creditCardInterfaceThreadCount++;
            return true;
        } else
            return false;
        }
    }
    private void decreaseCCIFThreadCount() {
        synchronized(syncObj) {
            creditCardInterfaceThreadCount--;
        }
    }
    
    static CreditCardService getInstance() {
        return singleton;
    }
    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(CreditCardServiceImpl.class.getName());
    private CreditCardServiceImpl() {
        super();
    }
    public AuthorizeCreditCardPaymentResponseType authorizeCreditCardPayment(
            PaymentAuthorizationServiceImpl service,
            AuthorizeCreditCardPaymentRequestType authorizeCreditCardPaymentRequest) {
        Connection conn = null;
        AuthorizeCreditCardPaymentResponseType response = null;
        try {
            response = new AuthorizeCreditCardPaymentResponseType();
            PasFunction.initResponse(authorizeCreditCardPaymentRequest, response);
            if (response.getErrorCode()!= PasConstants.STATUS_OK)
                return response;
            conn = service.getConnection();
            String txnString = authorizeCreditCardPaymentRequest.getNOACreditCardReq();
            JAXBAgent xmlAgent = new JAXBAgent();
            AuthTxn txn = xmlAgent.getAuthTxn(txnString);
            //AuthTxn txn = authorizeCreditCardPaymentRequest.getCreditCardReq();
            // creditcard charging service URL
            String url = service.getCCPaymentURL();
            // max. wait time when calling this URL
            long waitTime = service.getCreditCardAuthTimeoutMillis();
            if (increaseCCIFThreadCount(service)) {
                PasFunction.setErrorCode(StatusCode.PAS_CREDITCARD_IF_BUSY, 
                        PasConstants.CANNOT_AUTHORIZE, response);
                return response;
            }
            CreditCardFunctions agent = new NonBlockingAgent(url, waitTime);
            //pas fills in functionCode and transId before sending the request
            txn.setFunctionCode("AUTH_PAYMENT");    // call for payment authorization
            UUID uuid = UUID.randomUUID();          // construct a pax transaction ID
            String txId = uuid.toBase64();
            txn.setTransId(txId);
            agent.sendATxn(txn);
            AuthTxn retTxn = agent.getATxn();
            decreaseCCIFThreadCount();
            // always return what the interface returns to the caller.
            String retTxnString = xmlAgent.getXMLString(retTxn);
            response.setNOACreditCardResp(retTxnString);
            // process this txn and return
            String retCode = retTxn.getResult();
            if (! retCode.equalsIgnoreCase("APPROVE") && ! retCode.equalsIgnoreCase("SUCCESS")) {
                // the auth request does not succeed.
                PasFunction.setErrorCode(StatusCode.PAS_CREDITCARD_IF_ERROR, 
                        PasConstants.CANNOT_AUTHORIZE, response);
                return response;
            }
            // approved, create pasTransaction and pasToken
            PasTokenType authToken = new PasTokenType();
            authToken.setPaymentMethod(PaymentMethodType.CCARD);
            authToken.setToken(uuid.toByteArray());
                
            String currency = retTxn.getCurrency();
            // NEED TO CONVERT??
            String amountRequested = retTxn.getTotalPaid();
            MoneyType authorizedAmount = new MoneyType(currency, amountRequested);
            MoneyType capturedAmount = new MoneyType(currency, "0");
            MoneyType refundedAmount = new MoneyType(currency, "0");
            long creationTime = PasDBFunction.getCurrentDBTime(conn).getTime();
            long expTime = creationTime+service.getBalanceAccountAuthorizationExpirationMinutes()*1000*60;
            AuthorizationStateType authState = new AuthorizationStateType(
                        com.broadon.wsapi.pas.AuthorizationStatusType.Pending,
                        creationTime,
                        expTime,
                        authorizedAmount,
                        capturedAmount,
                        refundedAmount,
                        false);
            // insert transaction to db
            PasTransactionBean transaction = new PasTransactionBean();
            transaction.setTransId(txId);    // use base64 string in db
            //transaction.setPaymentMethodId(new Long(retTxn.getCardNum()).toString());
            transaction.setPaymentMethodId(retTxn.getCardNum());
            transaction.setPaymentMethod(PaymentMethodType.CCARD.toString());
            transaction.setTransType(TransactionKindType.Authorize.toString());
            transaction.setStatus(AuthorizationStatusType.Pending.toString());
            transaction.setAmountAuthorized(new BigDecimal(amountRequested));
            transaction.setAmountCaptured(bigDecimal0);
            transaction.setAmountRefunded(bigDecimal0);
            transaction.setCurrencyType(currency);
            transaction.setReferenceId(authorizeCreditCardPaymentRequest.getReferenceID());
            if (! PasDBFunction.insertPasTransaction(conn, transaction, service.getCreditCardAuthorizationExpirationMinutes())) {
                PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, 
                            PasConstants.CANNOT_AUTHORIZE, response);
                return response;
            }
            conn.commit();
            response.setAuthorizationToken(authToken);
            response.setAuthorizationState(authState);
        } catch (SQLException e) {               
            PasFunction.setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.debug("authorizeECardPayment error: ", e);
        } catch (Throwable e) {
            // Unexpected error, catch and log
            PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.PAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);
            e.printStackTrace();
            log.error("authorizeECardPayment error: ", e);
        } finally {
            try {                
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            }
            catch (SQLException e) {
                log.warn("authorizeECardPayment error while closing connection: ", e);
            }
        }          
        return response;
    }
/*    public CapturePaymentResponseType capture(
            CapturePaymentRequestType capturePaymentRequest,
            PasTransactionBean transData) {
        CapturePaymentResponseType response = new CapturePaymentResponseType();
        PasFunction.initResponse(capturePaymentRequest, response);
        if (response.getErrorCode()!= PasConstants.STATUS_OK)
                return response;
        PasFunction.setErrorCode(StatusCode.PAS_NO_IMPLEMENTATION, " -- Credit Card capturePayment", response);
        // TODO Auto-generated method stub
        return response;
    }

    public RefundPaymentResponseType refund(
            RefundPaymentRequestType refundPaymentRequest,
            PasTransactionBean transData) {
        RefundPaymentResponseType response = new RefundPaymentResponseType();
        PasFunction.initResponse(refundPaymentRequest, response);
        if (response.getErrorCode()!= PasConstants.STATUS_OK)
                return response;
        PasFunction.setErrorCode(StatusCode.PAS_NO_IMPLEMENTATION, " -- Credit Card refundPayment", response);
        // TODO Auto-generated method stub
        return response;
    }

    public VoidAuthorizationResponseType voidAuth(
            VoidAuthorizationRequestType voidAuthorizationRequest,
            PasTransactionBean transData) {
        // TODO Auto-generated method stub
        return null;
    }

    public GetAuthorizationHistoryResponseType getHistory(
            GetAuthorizationHistoryRequestType getAuthorizationHistoryRequest,
            PasTransactionBean transData) {
        // TODO Auto-generated method stub
        return null;
    }
*/
    public GetCreditCardHistoryResponseType getCreditCardAccountHistory(
            PaymentAuthorizationServiceImpl service,
            GetCreditCardHistoryRequestType getCreditCardHistoryRequest) {
        GetCreditCardHistoryResponseType response = new GetCreditCardHistoryResponseType();
        PasFunction.initResponse(getCreditCardHistoryRequest, response);
        if (response.getErrorCode()!= PasConstants.STATUS_OK)
            return response;
        PasFunction.setErrorCode(StatusCode.PAS_NO_IMPLEMENTATION, PasConstants.GET_CREDITCARD_HISTORY, response);
        return response;
    }

    public boolean capture(Connection conn,
            CapturePaymentRequestIfc capturePaymentRequest,
            CapturePaymentResponseIfc capturePaymentResponse,
            PasTransactionBean trans,
            String transType) {

        // check for expiration -- romoved to allow ecs to do its work during crash recovery
        /*
        if (trans.getExpirationDate().before(PasDBFunction.getCurrentDBTime(conn))) {
            if (! PasDBFunction.updateTransactionStatus(conn, trans, AuthorizationStatusType.Expired.toString())) {
                PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, 
                        PasConstants.CANNOT_CAPTURE, capturePaymentResponse);
                return false;
            }
            PasFunction.setErrorCode(StatusCode.PAS_ACCOUNT_EXPIRED, 
                    PasConstants.CANNOT_CAPTURE, capturePaymentResponse);
            return true;    // so the changes will be committed
        }
        */
        BigDecimal newCaptureTotal;  // new captured total
        try {
            newCaptureTotal = new BigDecimal(capturePaymentRequest.getAmount().getAmount());
        } catch (NumberFormatException nfe) {
            PasFunction.setErrorCode(StatusCode.PAS_NUMBER_FORMAT_ERROR, PasConstants.MONEY_FORMAT_ERROR, capturePaymentResponse);
            return false;
        }
        BigDecimal newCaptured = newCaptureTotal.subtract(trans.getAmountCaptured());  // new amount to capture
        BigDecimal newAmountRefunded = trans.getAmountRefunded();    //won't change
        if (newCaptured.compareTo(trans.getAmountAuthorized()) != 0) {
            PasFunction.setErrorCode(StatusCode.PAS_NOT_IN_COMPLETE_STATUS, null, capturePaymentResponse);
            return false;
        }
        String newStatus = AuthorizationStatusType.Completed.toString();
        // transaction table
        if (! PasDBFunction.updateTransactionRecord(conn, 
                trans.getTransId(), transType, newStatus, newCaptureTotal, newAmountRefunded)) {
            PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, 
                    PasConstants.CANNOT_CAPTURE, capturePaymentResponse);
            return false;
        }
        // action table -- MARK THIS TRANSACTION TO BE CALLED BY POINTS_APPLIED
        String actionKind;
        actionKind = ActionKindType.LastCap.toString();
        if (newCaptured.compareTo(bigDecimal0) != 0) {  
            // create new action record only if this is not a dup request
            String actionId = PasDBFunction.createActionRecord(conn, 
                capturePaymentResponse, trans.getTransId(), actionKind, newCaptured, trans.getStatus(), "Y");
            if (actionId == null)
                return false;
        }
        // prepare response
        trans.setStatus(newStatus);
        trans.setAmountCaptured(newCaptureTotal);
        AuthorizationStateType authState = new AuthorizationStateType();
        PasFunction.initAuthStateFromTransBean(trans, authState);
        capturePaymentResponse.setAuthorizationState(authState);
        return true;
    }

    public boolean refund(Connection conn,
            RefundPaymentRequestType refundPaymentRequest, RefundPaymentResponseType refundPaymentResponse, PasTransactionBean transData) {
        // TODO Auto-generated method stub
        PasFunction.setErrorCode(StatusCode.PAS_NO_IMPLEMENTATION, PasConstants.CANNOT_REFUND, refundPaymentResponse);
        return false;
    }

    public boolean voidAuth(Connection conn,
            VoidAuthorizationRequestType voidAuthorizationRequest, VoidAuthorizationResponseType voidAuthorizationResponse, PasTransactionBean transData) {
        // TODO Auto-generated method stub
        PasFunction.setErrorCode(StatusCode.PAS_NO_IMPLEMENTATION, PasConstants.CANNOT_VOID, voidAuthorizationResponse);
        return false;
    }

    public boolean getHistory(Connection conn,
            GetAuthorizationHistoryRequestType getAuthorizationHistoryRequest, GetAuthorizationHistoryResponseType getAuthorizationHistoryResponse, PasTransactionBean transData) {
        // TODO Auto-generated method stub
        PasFunction.setErrorCode(StatusCode.PAS_NO_IMPLEMENTATION, PasConstants.CANNOT_GET_ACCOUNT_HISTORY, getAuthorizationHistoryResponse);
        return false;
    }

}

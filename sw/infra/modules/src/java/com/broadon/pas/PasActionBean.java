package com.broadon.pas;
import java.util.Date;
import java.math.BigDecimal;
public class PasActionBean {

    private long actionId;
    private String transId;
    private Date actionDate;
    private String actionType;
    //private float actionAmount;
    private BigDecimal actionAmount;
    private String status;
    private String extAuthId;
    private String notify;
    
    public PasActionBean() {
        super();
    }
    
    BigDecimal getActionAmount() {
        return actionAmount;
    }

    void setActionAmount(BigDecimal actionAmount) {
        this.actionAmount = actionAmount;
    }

    Date getActionDate() {
        return actionDate;
    }

    void setActionDate(Date actionDate) {
        this.actionDate = actionDate;
    }

    long getActionId() {
        return actionId;
    }

    void setActionId(long actionId) {
        this.actionId = actionId;
    }

    String getStatus() {
        return status;
    }

    void setStatus(String status) {
        this.status = status;
    }

    String getActionType() {
        return actionType;
    }

    void setActionType(String actionType) {
        this.actionType = actionType;
    }

    String getExtAuthId() {
        return extAuthId;
    }

    void setExtAuthId(String extAuthId) {
        this.extAuthId = extAuthId;
    }

    String getTransId() {
        return transId;
    }

    void setTransId(String transId) {
        this.transId = transId;
    }

    String getNotify() {
        return notify;
    }
    
    void setNotify(String notify) {
        this.notify = notify;
    }
}

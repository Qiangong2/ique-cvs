package com.broadon.pas;

import java.util.Date;
import java.math.BigDecimal;
/**
 * Data store for database ECard table records.
 *
 */
public class ECardBean {
    private int eCardId;
    private int eCardType;
    private BigDecimal balance;
    private BigDecimal balanceAuthorized;
    private boolean isUsable;
    private Date lastUsed;
    //private Date expireDate;
    private Date activateDate;
    private Date revokeDate;
    //private String activateCode;
    private String[] countryCode;
    // the follwoing is from ECardType
    private ECardTypeBean eCardTypeBean;    // cache later?
    //private String currency;
    //private boolean isUsedOnce;
    //private boolean isPrepaid;
    //private boolean allowRefill;
    //private boolean isTitleOnly;
    private ECardBatchBean eCardBatchBean;
    //private Date expireDate;
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub

    }
/*
    String getActivateCode() {
        return activateCode;
    }

    void setActivateCode(String activateCode) {
        this.activateCode = activateCode;
    }
*/
    Date getActivateDate() {
        if (activateDate != null)
            return activateDate;
        else if (eCardBatchBean != null)
            return eCardBatchBean.getActivateDate();
        else 
            return null;
    }

    void setActivateDate(Date activateDate) {
        this.activateDate = activateDate;
    }

    BigDecimal getBalance() {
        return balance;
    }

    void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    BigDecimal getBalanceAuthorized() {
        return balanceAuthorized;
    }

    void setBalanceAuthorized(BigDecimal balanceAuthorized) {
        this.balanceAuthorized = balanceAuthorized;
    }
    
    BigDecimal getBalanceAvailable() {
        return balance.subtract(balanceAuthorized);
    }

    int getECardId() {
        return eCardId;
    }

    void setECardId(int cardId) {
        this.eCardId = cardId;
    }

    int getECardType() {
        return eCardType;
    }

    void setECardType(int cardType) {
        this.eCardType = cardType;
    }

    ECardTypeBean getECardTypeBean() {
        return eCardTypeBean;
    }
    
    void setECardTypeBean(ECardTypeBean eCardTypeBean) {
        this.eCardTypeBean = eCardTypeBean;
    }
    
    boolean isUsable() {
        return isUsable;
    }
    void setUsable(boolean isUsable) {
        this.isUsable = isUsable;
    }
    Date getLastUsed() {
        return lastUsed;
    }

    void setLastUsed(Date lastUsed) {
        this.lastUsed = lastUsed;
    }

    Date getRevokeDate() {
        if (revokeDate != null)
            return revokeDate;
        else if (eCardBatchBean != null)
            return eCardBatchBean.getRevokeDate();
        else
            return null;
    }

    void setRevokeDate(Date revokeDate) {
        this.revokeDate = revokeDate;
    }

    boolean isAllowRefill() {
        return eCardTypeBean.isAllowRefill();
    }
    
    String getCurrency() {
        return eCardTypeBean.getCurrency();
    }
    
    boolean isPrepaid() {
        return eCardTypeBean.isPrepaid();
    }

    boolean isTitleOnly() {
        return eCardTypeBean.isTitleOnly();
    }
    boolean isUsedOnce() {
        return eCardTypeBean.isUsedOnce();
    }
    ECardBatchBean getECardBatchBean() {
        return eCardBatchBean;
    }
    void setECardBatchBean(ECardBatchBean eCardBatchBean) {
        this.eCardBatchBean = eCardBatchBean;
    }
    Date getExpireDate() {
        if (eCardBatchBean != null)
            return eCardBatchBean.getExpireDate();
        else return null;
    }
    String[] getCountryCode() {
        return countryCode;
    }
    void setCountryCode(String[] countryCode) {
        this.countryCode = countryCode;
    }
    
    boolean validCountry(String country) {
        if (country == null)
            for (int i=0; i<countryCode.length; i++)
                if ("world".equalsIgnoreCase(countryCode[i]))
                    return true;
                else
                    return false;
        for (int i=0; i<countryCode.length; i++)
            if (country.equalsIgnoreCase(countryCode[i]))
                return true;
        return false;
    }
}

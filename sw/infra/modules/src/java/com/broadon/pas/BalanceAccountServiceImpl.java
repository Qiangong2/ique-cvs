package com.broadon.pas;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;

import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.logging.Log;

import com.broadon.util.UUID;
import com.broadon.wsapi.pas.ActionKindType;
import com.broadon.wsapi.pas.AuthorizationStateType;
import com.broadon.wsapi.pas.AuthorizationStatusType;
import com.broadon.wsapi.pas.AuthorizeAccountPaymentRequestType;
import com.broadon.wsapi.pas.AuthorizeAccountPaymentResponseType;
import com.broadon.wsapi.pas.BalanceAccountInfoType;
import com.broadon.wsapi.pas.BalanceAccountType;
import com.broadon.wsapi.pas.BalanceType;
import com.broadon.wsapi.pas.CapturePaymentRequestIfc;
import com.broadon.wsapi.pas.CapturePaymentResponseIfc;
import com.broadon.wsapi.pas.CheckAccountBalanceRequestType;
import com.broadon.wsapi.pas.CheckAccountBalanceResponseType;
import com.broadon.wsapi.pas.CreateAccountRequestType;
import com.broadon.wsapi.pas.CreateAccountResponseType;
import com.broadon.wsapi.pas.DisableAccountRequestType;
import com.broadon.wsapi.pas.DisableAccountResponseType;
import com.broadon.wsapi.pas.GetAccountHistoryRequestType;
import com.broadon.wsapi.pas.GetAccountHistoryResponseType;
import com.broadon.wsapi.pas.GetAuthorizationHistoryRequestType;
import com.broadon.wsapi.pas.GetAuthorizationHistoryResponseType;
import com.broadon.wsapi.pas.MoneyType;
import com.broadon.wsapi.pas.PasTokenType;
import com.broadon.wsapi.pas.PaymentMethodType;
import com.broadon.wsapi.pas.RefundPaymentRequestType;
import com.broadon.wsapi.pas.RefundPaymentResponseType;
import com.broadon.wsapi.pas.ResetAccountPINRequestType;
import com.broadon.wsapi.pas.ResetAccountPINResponseType;
import com.broadon.wsapi.pas.TransactionKindType;
import com.broadon.wsapi.pas.VoidAuthorizationRequestType;
import com.broadon.wsapi.pas.VoidAuthorizationResponseType;

public class BalanceAccountServiceImpl extends AuthorizationBasicServiceImpl implements BalanceAccountService, BasicServiceInternal {
    private static BalanceAccountService singleton = new BalanceAccountServiceImpl();
    private static BigDecimal bigDecimal0 = new BigDecimal("0");
    static BalanceAccountService getInstance() {
        return singleton;
    }
    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(BalanceAccountServiceImpl.class.getName());
    
    private BalanceAccountServiceImpl() {
        super();
    }

    public AuthorizeAccountPaymentResponseType authorizeAccountPayment(
            PaymentAuthorizationServiceImpl service,
            AuthorizeAccountPaymentRequestType authorizeAccountPaymentRequest) {
        Connection conn = null;
        AuthorizeAccountPaymentResponseType response = null;
        try {
            response = new AuthorizeAccountPaymentResponseType();
            conn = service.getConnection();
            BalanceAccountType acct = authorizeAccountPaymentRequest.getBalanceAccount();
            // retrieve and verify Balance Account
            AccountBean account = PasFunction.checkBalanceAccount(authorizeAccountPaymentRequest,
                    response, acct, true, true, conn);
            if (account == null)
                    return response;
            Date today = PasDBFunction.getCurrentDBTime(conn);
            if (today == null) {
                PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, 
                        PasConstants.CANNOT_AUTHORIZE, response);
                return response;
            }
            Date aDate = account.getActivateDate();
            if (aDate == null || aDate.after(today)) { // not activated
                PasFunction.setErrorCode(StatusCode.PAS_ACCOUNT_NOT_ACTIVATED, 
                        PasConstants.CANNOT_AUTHORIZE, response);
                return response;
            }
            MoneyType mTypeRequested = authorizeAccountPaymentRequest.getAmount();
            if (mTypeRequested == null) {
                PasFunction.setErrorCode(StatusCode.PAS_MISSING_DATA, 
                        PasConstants.CANNOT_AUTHORIZE, response);
                return response;
            }
            String currency = mTypeRequested.getCurrencyType();
            if (currency != null && !currency.equalsIgnoreCase(account.getCurrency())) {
                PasFunction.setErrorCode(StatusCode.PAS_CURRENCY_MISMATCH, 
                        PasConstants.CANNOT_AUTHORIZE, response);
                return response;
            }
            BigDecimal balance = account.getBalance();
            BigDecimal amountRequested;
            try {
                amountRequested = new BigDecimal(mTypeRequested.getAmount());
            } catch (NumberFormatException nfe) {
                PasFunction.setErrorCode(StatusCode.PAS_NUMBER_FORMAT_ERROR, PasConstants.MONEY_FORMAT_ERROR, response);
                return response;
            }
            BigDecimal newBalanceAuthorized = account.getBalanceAuthorized().add(amountRequested);
            if (newBalanceAuthorized.compareTo(balance) > 0) {  // not enough balance
                // first try to release locked amounts from expired auth.
                if (PasDBFunction.releaseAcctExpiredLockedAmount(conn, account)) {
                    balance = account.getBalance(); // should remains the same
                    newBalanceAuthorized = account.getBalanceAuthorized().add(amountRequested);
                }
                if (newBalanceAuthorized.compareTo(balance) > 0) {  // still not enough balance
                    PasFunction.setErrorCode(StatusCode.PAS_NOT_ENOUGH_MONEY, 
                    PasConstants.CANNOT_AUTHORIZE, response);
                    return response;
                }
            }
            // update account info
            if (! PasDBFunction.updateBalanceAccount(conn, account.getAccountId(), balance, newBalanceAuthorized)) {
                PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, 
                        PasConstants.CANNOT_AUTHORIZE, response);
                return response;
            }
            // create paymentAuthorization Token
            PasTokenType authToken = new PasTokenType();
            UUID uuid = UUID.randomUUID();
            authToken.setPaymentMethod(PaymentMethodType.ACCOUNT);
            authToken.setToken(uuid.toByteArray());
            
            MoneyType authorizedAmount = new MoneyType(currency, amountRequested.toString());
            MoneyType capturedAmount = new MoneyType(currency, "0");
            MoneyType refundedAmount = new MoneyType(currency, "0");
            long creationTime = PasDBFunction.getCurrentDBTime(conn).getTime();
            long expTime = creationTime+service.getBalanceAccountAuthorizationExpirationMinutes()*1000*60;
            AuthorizationStateType authState = new AuthorizationStateType(
                    com.broadon.wsapi.pas.AuthorizationStatusType.Pending,
                    creationTime,
                    expTime,
                    authorizedAmount,
                    capturedAmount,
                    refundedAmount,
                    false);
            // insert transaction to db
            PasTransactionBean transaction = new PasTransactionBean();
            transaction.setTransId(uuid.toBase64());    // use base64 string in db
            transaction.setPaymentMethodId(new Long(account.getAccountId()).toString());
            transaction.setPaymentMethod(PaymentMethodType.ACCOUNT.toString());
            transaction.setTransType(TransactionKindType.Authorize.toString());
            transaction.setStatus(AuthorizationStatusType.Pending.toString());
            transaction.setAmountAuthorized(amountRequested);
            transaction.setAmountCaptured(bigDecimal0);
            transaction.setAmountRefunded(bigDecimal0);
            transaction.setCurrencyType(currency);
            transaction.setReferenceId(authorizeAccountPaymentRequest.getReferenceID());
            if (! PasDBFunction.insertPasTransaction(conn, transaction, service.getBalanceAccountAuthorizationExpirationMinutes())) {
                PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, 
                        PasConstants.CANNOT_AUTHORIZE, response);
                return response;
            }
            conn.commit();
            response.setAuthorizationToken(authToken);
            response.setAuthorizationState(authState);
        } catch (SQLException e) {               
            PasFunction.setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.debug("authorizeAccountPayment error: ", e);
            
        } catch (Throwable e) {
            // Unexpected error, catch and log
            PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.PAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);
            log.error("authorizeAccountPayment error: ", e);
            
        } finally {
            try {                
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            }
            catch (SQLException e) {
                log.warn("authorizeAccountPayment error while closing connection: ", e);
            }
        }                
        return response;
    }

    public GetAccountHistoryResponseType getBalanceAccountHistory(
            PaymentAuthorizationServiceImpl service,
            GetAccountHistoryRequestType getAccountHistoryRequest) {
        Connection conn = null;
        GetAccountHistoryResponseType response = null;
        try {
            response = new GetAccountHistoryResponseType();
            conn = service.getConnection();
            // retrieve and verify bank account
            AccountBean account = PasFunction.checkBalanceAccount(getAccountHistoryRequest,
                    response, getAccountHistoryRequest.getBalanceAccount(), false, true, conn);
            if (account == null)
                    return response;
            
            BigDecimal balance = account.getBalance();
            BigDecimal balanceAuthorized = account.getBalanceAuthorized();
            String currencyType = account.getCurrency();
            BalanceType currentBalance = new BalanceType(
                    new MoneyType(currencyType, balance.toString()),
                    new MoneyType(currencyType, balanceAuthorized.toString()),
                    new MoneyType(currencyType, balance.subtract(balanceAuthorized).toString()));
            response.setCurrentBalance(currentBalance);
            // add implementing AccountHistory to the generated GetAccountHistoryResponseType
            // to remove compilation error of the following line.
            if (! PasDBFunction.getAccountHistory(conn, 
                    String.valueOf(account.getAccountId()),
                    account.getBalance(),
                    PaymentMethodType.ACCOUNT.toString(),
                    getAccountHistoryRequest.getBegin(), getAccountHistoryRequest.getEnd(), response))
                PasFunction.setErrorCode(StatusCode.PAS_NO_ACCOUNT_RECORD, 
                        PasConstants.CANNOT_GET_ACCOUNT_HISTORY, response);
            // return response; no need to commit, query only
        } catch (SQLException e) {               
            PasFunction.setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                e.getLocalizedMessage(),
                response);
            log.debug("getBalanceAccountHistory error: ", e);
        
        } catch (Throwable e) {
            // Unexpected error, catch and log
            PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR,
                StatusCode.getMessage(StatusCode.PAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                response);
            log.error("getBalanceAccountHistory error: ", e);
        
        } finally {
            try {                
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            }
            catch (SQLException e) {
                log.warn("getBalanceAccountHistory error while closing connection: ", e);
            }
        }                
        return response;
    }
/*
    public TransferBalanceResponseType transferBalance(
            PaymentAuthorizationServiceImpl service,
            TransferBalanceRequestType transferBalanceRequest) {
        TransferBalanceResponseType response = new TransferBalanceResponseType();
        PasFunction.initResponse(transferBalanceRequest, response);
        if (response.getErrorCode()!= PasConstants.STATUS_OK)
            return response;
        MoneyType transferAmount = transferBalanceRequest.getTransferAmount();
        if (transferAmount == null) {
            PasFunction.setErrorCode(StatusCode.PAS_MISSING_DATA, PasConstants.CANNOT_TRANSFER, response);
            return response;
        }
        BalanceAccountType fromAccount = transferBalanceRequest.getFromAccount();
        BalanceAccountType toAccount = transferBalanceRequest.getToAccount();
        if (fromAccount == null || toAccount == null) {
            PasFunction.setErrorCode(StatusCode.PAS_MISSING_DATA, PasConstants.CANNOT_TRANSFER, response);
            return response;
        }
        String currencyType = transferAmount.getCurrencyType();
        BigDecimal amount;
        try {
            amount= new BigDecimal(transferAmount.getAmount());
        } catch (NumberFormatException nfe) {
            PasFunction.setErrorCode(StatusCode.PAS_NUMBER_FORMAT_ERROR, PasConstants.MONEY_FORMAT_ERROR, response);
            return response;
        }
        Connection conn = null;
        try {   
            conn = service.getConnection();
            // 1. get fromAccount and toAccount and check currencyType
            //String fromAccountId = fromAccount.getAccountNumber();
            //String toAccountId = toAccount.getAccountNumber();
            AccountBean fromAccountBean = PasFunction.checkBalanceAccount(
                    transferBalanceRequest, response, fromAccount, true, true, conn);
            AccountBean toAccountBean = PasFunction.checkBalanceAccount(
                    transferBalanceRequest, response, toAccount, true, true, conn);
            if (fromAccountBean == null || toAccountBean == null) {
                PasFunction.setErrorCode(StatusCode.PAS_NO_ACCOUNT_RECORD, 
                    PasConstants.CANNOT_TRANSFER, response);
                return response;
            }
            if (!fromAccountBean.getCurrency().equalsIgnoreCase(currencyType)
                    || !toAccountBean.getCurrency().equalsIgnoreCase(currencyType)) {
                PasFunction.setErrorCode(StatusCode.PAS_CURRENCY_MISMATCH, 
                        PasConstants.CANNOT_TRANSFER, response);
                return response;
            }
            if (fromAccountBean.getAccountId() == toAccountBean.getAccountId()) {
                PasFunction.setErrorCode(StatusCode.PAS_SAME_ACCOUNT, 
                    PasConstants.CANNOT_TRANSFER, response);
                return response;
            }
            // 3. update fromAccount
            BigDecimal newBalance;
            BigDecimal newLocked;
            newBalance = fromAccountBean.getBalance().subtract(amount);
            if (newBalance.compareTo(bigDecimal0) < 0) {
                PasFunction.setErrorCode(StatusCode.PAS_NOT_ENOUGH_MONEY, 
                        PasConstants.CANNOT_TRANSFER, response);
                    return response;
            }
            newLocked = fromAccountBean.getBalanceAuthorized(); // no change
            if (! PasDBFunction.updateBalanceAccount(conn, Integer.parseInt(fromAccount.getAccountNumber()), newBalance, newLocked)) {
                    PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, 
                            PasConstants.CANNOT_TRANSFER, response);
                    return response;
                }
            BalanceType returnBalance = new BalanceType(
                    new MoneyType(currencyType, newBalance.toString()), 
                    new MoneyType(currencyType, newLocked.toString()), 
                    new MoneyType(currencyType, newBalance.subtract(newLocked).toString()));
            // 4. update toAccount
            newBalance = toAccountBean.getBalance().add(amount);
            newLocked = toAccountBean.getBalanceAuthorized(); // no change
            if (! PasDBFunction.updateBalanceAccount(conn, Integer.parseInt(toAccount.getAccountNumber()), newBalance, newLocked)) {
                    PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, 
                            PasConstants.CANNOT_TRANSFER, response);
                    return response;
                }
            // 5. create transaction and action for fromAccount
            String transId = PasDBFunction.createTransactionRecord(
                response, 
                fromAccount.getAccountNumber(), 
                PaymentMethodType.BalanAcct.toString(),
                TransactionKindType.Deposit.toString(),
                AuthorizationStatusType.Completed.toString(),
                service.getBalanceAccountAuthorizationExpirationDays(),
                conn);
            if (transId == null)
                return response;
            String actionId = PasDBFunction.createActionRecord(
                    response, transId, ActionKindType.Deposit.toString(), amount, AuthorizationStatusType.Completed.toString(), conn);
            if (actionId == null)
                return response;
            // 6. create transaction and action for toAccount
            transId = PasDBFunction.createTransactionRecord(response, 
                    toAccount.getAccountNumber(), 
                    PaymentMethodType.BalanAcct.toString(),
                    TransactionKindType.Deposit_to.toString(),
                    AuthorizationStatusType.Completed.toString(),
                    service.getBalanceAccountAuthorizationExpirationDays(),
                    conn);
            if (transId == null)
                    return response;
            actionId = PasDBFunction.createActionRecord(
                    response, transId, ActionKindType.Deposit.toString(), amount, AuthorizationStatusType.Completed.toString(), conn);
            if (actionId == null)
                return response;
            // 6. commit
            conn.commit();// update return state here before return
            response.setBalance(returnBalance);
            response.setTransferredAmount(new MoneyType(currencyType, amount.toString()));
            return response;
        } catch (SQLException e) {               
            PasFunction.setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                e.getLocalizedMessage(),
                response);
            log.debug("transferBalance error: ", e);
        
        } catch (Throwable e) {
            // Unexpected error, catch and log
            PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR,
                StatusCode.getMessage(StatusCode.PAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                response);
            log.error("transferBalance error: ", e);
        
        } finally {
            try {                
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            } catch (SQLException e) {
                log.warn("transferBalance error while closing connection: ", e);
            }
        }
        return response;
    }
*/
    public CheckAccountBalanceResponseType checkAccountBalance(
            PaymentAuthorizationServiceImpl service,
            CheckAccountBalanceRequestType checkAccountBalanceRequest) {
        Connection conn = null;
        CheckAccountBalanceResponseType response = null;
        try {
            response = new CheckAccountBalanceResponseType();
            conn = service.getConnection();
            // retrieve and verify account
            AccountBean account = PasFunction.checkBalanceAccount(checkAccountBalanceRequest,
                    response, checkAccountBalanceRequest.getBalanceAccount(), false, true, conn);
            if (account == null)
                    return response;
            BalanceAccountInfoType accountInfo = new BalanceAccountInfoType();
            PasFunction.initBalanceAccountInfoFromAccountBean(account, accountInfo);
            response.setBalanceAccountInfo(accountInfo);
            conn.commit();
        } catch (SQLException e) {               
            PasFunction.setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.debug("checkECardBalance error: ", e);
            
        } catch (Throwable e) {
            // Unexpected error, catch and log
            PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.PAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);
            log.error("checkECardBalance error: ", e);
            
        } finally {
            try {                
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            }
            catch (SQLException e) {
                log.warn("checkECardBalance error while closing connection: ", e);
            }
        }                
        return response;
    }

    public CreateAccountResponseType createAccount(PaymentAuthorizationServiceImpl service, CreateAccountRequestType createAccountRequest) {
        CreateAccountResponseType response = new CreateAccountResponseType();
        PasFunction.initResponse(createAccountRequest, response);
        if (response.getErrorCode()!= PasConstants.STATUS_OK)
            return response;
        Connection conn = null;
        try {   
            conn = service.getConnection();
            BalanceAccountType targetAccount = createAccountRequest.getBalanceAccount();
            AccountBean account = PasFunction.checkBalanceAccount(
                    createAccountRequest, response, targetAccount, false, true, conn);
            if (account == null) {  // create account
                PasFunction.initResponse(createAccountRequest, response);   // reset errorCode
                AccountBean newAccount = new AccountBean();
                newAccount.setAccountId(Integer.parseInt(targetAccount.getAccountNumber()));
                newAccount.setAccountType("VCPOINTS");
                newAccount.setBalance(bigDecimal0);
                newAccount.setBalanceAuthorized(bigDecimal0);
                newAccount.setCurrency("POINTS");
                newAccount.setPin(Integer.parseInt(targetAccount.getPIN()));
                account = PasDBFunction.createBalanceAccount(conn, newAccount);
                if (account == null) {
                    PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, 
                            PasConstants.CANNOT_CREATE_ACCOUNT, response);
                    return response;
                }
            } else {    // enable account
                if (account.getRevokeDate() != null) {
                    account.setRevokeDate(null);
                    if (! PasDBFunction.updateAccountStatus(conn, account, "REVOKE_DATE")) {
                        PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, 
                            PasConstants.CANNOT_ENABLE_ACCOUNT, response);
                    return response;
                    }
                } 
            }
            conn.commit();
            BalanceAccountInfoType accountInfo = new BalanceAccountInfoType();
            PasFunction.initAccountInfo(account, accountInfo);
            response.setAccountInfo(accountInfo);
        } catch (SQLException e) {               
            PasFunction.setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                e.getLocalizedMessage(),
                response);
            log.debug("disableAccount error: ", e);
        } catch (Throwable e) {
            // Unexpected error, catch and log
            PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR,
                StatusCode.getMessage(StatusCode.PAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                response);
            log.error("disableAccount error: ", e);
        } finally {
            try {                
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            }
            catch (SQLException e) {
                log.warn("disableAccount error while closing connection: ", e);
            }
        }
        return response;
    }
    
    public ResetAccountPINResponseType resetAccountPIN(PaymentAuthorizationServiceImpl service, ResetAccountPINRequestType resetAccountPINRequest) {
        ResetAccountPINResponseType response = new ResetAccountPINResponseType();
        PasFunction.initResponse(resetAccountPINRequest, response);
        if (response.getErrorCode()!= PasConstants.STATUS_OK)
            return response;
        Connection conn = null;
        try {   
            conn = service.getConnection();
            BalanceAccountType targetAccount = resetAccountPINRequest.getBalanceAccount();
            AccountBean account = PasFunction.checkBalanceAccount(
                    resetAccountPINRequest, response, targetAccount, false, false, conn);
            if (account == null)
                return response;
            account.setPin(Integer.parseInt(targetAccount.getPIN()));
            if (! PasDBFunction.updateAccountStatus(conn, account, "PIN")) {
                PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, 
                    PasConstants.CANNOT_DISABLE_ACCOUNT, response);
            return response;
            }
            conn.commit();
            BalanceAccountInfoType accountInfo = new BalanceAccountInfoType();
            PasFunction.initAccountInfo(account, accountInfo);
            response.setAccountInfo(accountInfo);
        } catch (SQLException e) {               
            PasFunction.setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                e.getLocalizedMessage(),
                response);
            log.debug("disableAccount error: ", e);
        } catch (Throwable e) {
            // Unexpected error, catch and log
            PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR,
                StatusCode.getMessage(StatusCode.PAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                response);
            log.error("disableAccount error: ", e);
        } finally {
            try {                
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            }
            catch (SQLException e) {
                log.warn("disableAccount error while closing connection: ", e);
            }
        }
        return response;
    }

    public DisableAccountResponseType disableAccount(PaymentAuthorizationServiceImpl service, DisableAccountRequestType disableAccountRequest) {
        DisableAccountResponseType response = new DisableAccountResponseType();
        PasFunction.initResponse(disableAccountRequest, response);
        if (response.getErrorCode()!= PasConstants.STATUS_OK)
            return response;
        Connection conn = null;
        try {   
            conn = service.getConnection();
            BalanceAccountType targetAccount = disableAccountRequest.getBalanceAccount();
            AccountBean account = PasFunction.checkBalanceAccount(
                    disableAccountRequest, response, targetAccount, false, true, conn);
            if (account == null)
                return response;
            if (account.getRevokeDate() == null) {
                account.setRevokeDate(new Date());
                if (! PasDBFunction.updateAccountStatus(conn, account, "REVOKE_DATE")) {
                    PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, 
                        PasConstants.CANNOT_DISABLE_ACCOUNT, response);
                return response;
                }
                conn.commit();
            } 
            BalanceAccountInfoType accountInfo = new BalanceAccountInfoType();
            PasFunction.initAccountInfo(account, accountInfo);
            response.setAccountInfo(accountInfo);
        } catch (SQLException e) {               
            PasFunction.setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                e.getLocalizedMessage(),
                response);
            log.debug("disableAccount error: ", e);
        } catch (Throwable e) {
            // Unexpected error, catch and log
            PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR,
                StatusCode.getMessage(StatusCode.PAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                response);
            log.error("disableAccount error: ", e);
        } finally {
            try {                
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            }
            catch (SQLException e) {
                log.warn("disableAccount error while closing connection: ", e);
            }
        }
        return response;
    }
    
    public  boolean capture(
            Connection conn,
            CapturePaymentRequestIfc capturePaymentRequest, 
            CapturePaymentResponseIfc capturePaymentResponse, 
            PasTransactionBean trans,
            String transType) {
        // need to update transaction and action and bank account table
        AccountBean account = PasDBFunction.getAccount(conn, Integer.parseInt(trans.getPaymentMethodId()));
        if (account == null) {
            PasFunction.setErrorCode(StatusCode.PAS_NO_ACCOUNT_RECORD, 
                    PasConstants.CANNOT_CAPTURE, capturePaymentResponse);
            return false;
        }
        // check for expiration -- romoved to allow ecs to do its work during crash recovery
        /*
        if (trans.getExpirationDate().before(PasDBFunction.getCurrentDBTime(conn))) {
            if (! PasDBFunction.updateTransactionStatus(conn, trans, AuthorizationStatusType.Expired.toString())) {
                PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, 
                        PasConstants.CANNOT_CAPTURE, capturePaymentResponse);
                return false;
            }
            // release uncaptured authorization
            if (! PasDBFunction.updateBalanceAccount(conn, 
                    account.getAccountId(), account.getBalance(), 
                    account.getBalanceAuthorized().subtract(trans.getAmountAuthorized()).add(trans.getAmountCaptured()))) {
                PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, 
                        PasConstants.CANNOT_CAPTURE, capturePaymentResponse);
                return false;
            }
            PasFunction.setErrorCode(StatusCode.PAS_ACCOUNT_EXPIRED, 
                    PasConstants.CANNOT_CAPTURE, capturePaymentResponse);
            return true;    // so the changes will be committed
        }
        */
        BigDecimal newCaptureTotal;  // new captured total
        try {
            newCaptureTotal = new BigDecimal(capturePaymentRequest.getAmount().getAmount());
        } catch (NumberFormatException nfe) {
            PasFunction.setErrorCode(StatusCode.PAS_NUMBER_FORMAT_ERROR, PasConstants.MONEY_FORMAT_ERROR, capturePaymentResponse);
            return false;
        }
        BigDecimal newCaptured = newCaptureTotal.subtract(trans.getAmountCaptured());  // new amount to capture
        BigDecimal newAmountRefunded = trans.getAmountRefunded();    //won't change
        BigDecimal newBalance = account.getBalance().subtract(newCaptured);
        // check newBalance > 0??     NO, since we allowed expired auth to capture
        
        // new locked amount
        BigDecimal newBalanceAuthorized = account.getBalanceAuthorized().subtract(newCaptured);

        String newStatus = trans.getStatus();
        if (capturePaymentRequest.isIsFinal()) {
            newStatus = AuthorizationStatusType.Completed.toString();
            // unlock unused amount, if any
            newBalanceAuthorized = newBalanceAuthorized.subtract(trans.getAmountAuthorized()).add(newCaptureTotal);
        }
       
        if (newCaptureTotal.compareTo(trans.getAmountAuthorized()) == 0)
            newStatus = AuthorizationStatusType.Completed.toString();
        // bank account table
        if (! PasDBFunction.updateBalanceAccount(conn, account.getAccountId(), newBalance, newBalanceAuthorized)) {
            PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, 
                    PasConstants.CANNOT_CAPTURE, capturePaymentResponse);
            return false;
        }
        // transaction table
        if (! PasDBFunction.updateTransactionRecord(conn, 
                trans.getTransId(), transType, newStatus, newCaptureTotal, newAmountRefunded)) {
            PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, 
                    PasConstants.CANNOT_CAPTURE, capturePaymentResponse);
            return false;
        }
        // action table
        String actionKind;
        if (capturePaymentRequest.isIsFinal())
            actionKind = ActionKindType.LastCap.toString();
        else
            actionKind = ActionKindType.Capture.toString();
        String actionId = PasDBFunction.createActionRecord(conn, 
                capturePaymentResponse, trans.getTransId(), actionKind, newCaptured, trans.getStatus(), null);
        if (actionId == null)
            return false;
        // prepare response
        trans.setStatus(newStatus);
        trans.setAmountCaptured(newCaptureTotal);
        AuthorizationStateType authState = new AuthorizationStateType();
        PasFunction.initAuthStateFromTransBean(trans, authState);
        capturePaymentResponse.setAuthorizationState(authState);
        return true;
    }

    public boolean refund(Connection conn,
            RefundPaymentRequestType refundPaymentRequest, 
            RefundPaymentResponseType refundPaymentResponse, 
            PasTransactionBean trans) {
        // need to update transaction and action and bank account table
        AccountBean account = PasDBFunction.getAccount(conn, Integer.parseInt(trans.getPaymentMethodId()));
        if (account == null) {
            PasFunction.setErrorCode(StatusCode.PAS_NO_ACCOUNT_RECORD, 
                    PasConstants.CANNOT_REFUND, refundPaymentResponse);
            return false;
        }
        BigDecimal newRefundTotal;
        try {
            newRefundTotal = new BigDecimal(refundPaymentRequest.getAmount().getAmount());
        } catch (NumberFormatException nfe) {
            PasFunction.setErrorCode(StatusCode.PAS_NUMBER_FORMAT_ERROR, PasConstants.MONEY_FORMAT_ERROR, refundPaymentResponse);
            return false;
        }
        BigDecimal newRefund = newRefundTotal.subtract(trans.getAmountRefunded());
        BigDecimal newAmountCaptured = trans.getAmountCaptured();    // won't change
        BigDecimal newBalance = account.getBalance().add(newRefund);
        BigDecimal newBalanceAuthorized = account.getBalanceAuthorized();  // won't change
        String newStatus = trans.getStatus();   // won't change
        // bank account table
        if (! PasDBFunction.updateBalanceAccount(conn, account.getAccountId(), newBalance, newBalanceAuthorized)) {
            PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, 
                    PasConstants.CANNOT_REFUND, refundPaymentResponse);
            return false;
        }
        // transaction table
        if (! PasDBFunction.updateTransactionRecord(conn, 
                trans.getTransId(), trans.getTransType(), newStatus, newAmountCaptured, newRefundTotal)) {
            PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, 
                    PasConstants.CANNOT_REFUND, refundPaymentResponse);
            return false;
        }
        // action table
        String actionId = PasDBFunction.createActionRecord(conn, 
                refundPaymentResponse, trans.getTransId(), ActionKindType.Refund.toString(), newRefund, trans.getStatus(), null);
        if (actionId == null)
            return false;
        // prepare response
        trans.setAmountRefunded(newRefundTotal);
        AuthorizationStateType authState = new AuthorizationStateType();
        PasFunction.initAuthStateFromTransBean(trans, authState);
        refundPaymentResponse.setAuthorizationState(authState);
        return true;
    }

    public boolean voidAuth(Connection conn,
            VoidAuthorizationRequestType voidAuthorizationRequest, VoidAuthorizationResponseType voidAuthorizationResponse, PasTransactionBean transData) {
        AccountBean account = PasDBFunction.getAccount(conn, Integer.parseInt(transData.getPaymentMethodId()));
        if (account == null) {
            PasFunction.setErrorCode(StatusCode.PAS_NO_ACCOUNT_RECORD, 
                    PasConstants.CANNOT_VOID, voidAuthorizationResponse);
            return false;
        }

        BigDecimal amountAuthorized = transData.getAmountAuthorized().subtract(transData.getAmountRefunded());
        BigDecimal newBalance = account.getBalance();  // won't change
        BigDecimal newLocked = account.getBalanceAuthorized().subtract(amountAuthorized);
        if (newLocked.compareTo(bigDecimal0)<0) {
            PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, 
                    PasConstants.CANNOT_VOID, voidAuthorizationResponse);
            return false;
        }
        // update bank account info
        if (! PasDBFunction.updateBalanceAccount(conn, account.getAccountId(), newBalance, newLocked)) {
            PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, 
                    PasConstants.CANNOT_VOID, voidAuthorizationResponse);
            return false;
        }
        // update transaction
        if (! PasDBFunction.updateTransactionStatus(conn, transData.getTransId(), AuthorizationStatusType.Void.toString())) {
            PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, 
                    PasConstants.CANNOT_VOID, voidAuthorizationResponse);
            return false;
        }
        transData.setStatus(AuthorizationStatusType.Void.toString());
        AuthorizationStateType authState = new AuthorizationStateType();
        PasFunction.initAuthStateFromTransBean(transData, authState);
        voidAuthorizationResponse.setAuthorizationState(authState);
        return true;
    }

    public boolean getHistory(Connection conn,
            GetAuthorizationHistoryRequestType getAuthorizationHistoryRequest, GetAuthorizationHistoryResponseType getAuthorizationHistoryResponse, PasTransactionBean transData) {
        // TODO Auto-generated method stub
        PasFunction.setErrorCode(StatusCode.PAS_NO_IMPLEMENTATION, PasConstants.CANNOT_GET_ACCOUNT_HISTORY, getAuthorizationHistoryResponse);
        return false;
    }

}

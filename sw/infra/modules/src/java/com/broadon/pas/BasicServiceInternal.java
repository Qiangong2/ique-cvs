package com.broadon.pas;
import java.sql.Connection;

import com.broadon.wsapi.pas.CapturePaymentRequestIfc;
import com.broadon.wsapi.pas.CapturePaymentResponseIfc;
import com.broadon.wsapi.pas.RefundPaymentRequestType;
import com.broadon.wsapi.pas.RefundPaymentResponseType;
import com.broadon.wsapi.pas.VoidAuthorizationRequestType;
import com.broadon.wsapi.pas.VoidAuthorizationResponseType;
import com.broadon.wsapi.pas.GetAuthorizationHistoryRequestType;
import com.broadon.wsapi.pas.GetAuthorizationHistoryResponseType;

public interface BasicServiceInternal {
    public boolean capture(
            Connection conn, 
            CapturePaymentRequestIfc capturePaymentRequest,
            CapturePaymentResponseIfc capturePaymentResponse,
            PasTransactionBean transData,
            String transType);
    public boolean refund(
            Connection conn,
            RefundPaymentRequestType refundPaymentRequest,
            RefundPaymentResponseType refundPaymentResponse,
            PasTransactionBean transData);
    public boolean voidAuth(
            Connection conn,
            VoidAuthorizationRequestType voidAuthorizationRequest,
            VoidAuthorizationResponseType voidAuthorizationResponse,
            PasTransactionBean transData);
    public boolean getHistory(
            Connection conn,
            GetAuthorizationHistoryRequestType getAuthorizationHistoryRequest,
            GetAuthorizationHistoryResponseType getAuthorizationHistoryResponse,
            PasTransactionBean transData);
}

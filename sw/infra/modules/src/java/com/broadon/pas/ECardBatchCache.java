package com.broadon.pas;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.logging.Log;

import com.broadon.db.DataSourceScheduler;
import com.broadon.util.BackingStoreException;
import com.broadon.util.CacheLFU;

public class ECardBatchCache extends CacheLFU implements com.broadon.util.BackingStore {

    private static Log log = 
        LogFactory.getLog(ECardBatchCache.class.getName());
    
    private PaymentAuthorizationServiceImpl service;

    public ECardBatchCache(PaymentAuthorizationServiceImpl service)
    {
        super(10, PasConstants.CacheTimeOut);
        this.service = service;
    }
    
    public Object retrieve(Object eCardBatchKey) throws BackingStoreException
    {
        ECardBatchKey key = (ECardBatchKey) eCardBatchKey;
        int eCardType = key.getECardType();
        long eCardId = key.getECardId();
        Connection conn = null;
        ECardBatchBean eCardBatchBean = null;
        try {
           conn = service.getConnection(DataSourceScheduler.DB_PRIORITY_FAST);
           eCardBatchBean = PasDBFunction.getECardBatchBean(conn, eCardId, eCardType);
           if (eCardBatchBean == null)
               throw new BackingStoreException();
        } catch (SQLException e) {
            throw BackingStoreException.rewrap(e);            
        } finally {
            if (conn != null)
                try {
                    conn.close();
                } catch (SQLException e2) {
                    log.error("cannot close connection: ", e2);
                }
        }
        return eCardBatchBean;
    }

    /**
     * Returns details for a given eCardType
     */
    public ECardBatchBean getECardBatchBean(ECardBatchKey eCardBatchKey)
        throws BackingStoreException
    {
        return (ECardBatchBean) get(eCardBatchKey);
    }

}

package com.broadon.pas;

public interface CreditCardService extends AuthorizationBasicService{
    public com.broadon.wsapi.pas.AuthorizeCreditCardPaymentResponseType authorizeCreditCardPayment(
            PaymentAuthorizationServiceImpl service,
            com.broadon.wsapi.pas.AuthorizeCreditCardPaymentRequestType authorizeCreditCardPaymentRequest);
    public com.broadon.wsapi.pas.GetCreditCardHistoryResponseType getCreditCardAccountHistory(
            PaymentAuthorizationServiceImpl service,
            com.broadon.wsapi.pas.GetCreditCardHistoryRequestType getCreditCardHistoryRequest);
}

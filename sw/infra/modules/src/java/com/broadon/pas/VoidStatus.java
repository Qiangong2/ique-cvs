package com.broadon.pas;
import java.sql.Connection;

import com.broadon.wsapi.pas.AuthorizationStateType;
import com.broadon.wsapi.pas.CapturePaymentRequestIfc;
import com.broadon.wsapi.pas.CapturePaymentResponseIfc;
import com.broadon.wsapi.pas.GetAuthorizationHistoryRequestType;
import com.broadon.wsapi.pas.GetAuthorizationHistoryResponseType;
import com.broadon.wsapi.pas.RefundPaymentRequestType;
import com.broadon.wsapi.pas.RefundPaymentResponseType;
import com.broadon.wsapi.pas.VoidAuthorizationRequestType;
import com.broadon.wsapi.pas.VoidAuthorizationResponseType;

public class VoidStatus implements BasicServiceInternal {
private static BasicServiceInternal singleton = new VoidStatus();
    
    public static BasicServiceInternal getInstance() {
        return singleton;
    }
    
    private VoidStatus() {
        super();
        // TODO Auto-generated constructor stub
    }

    public boolean capture(
            Connection conn,
            CapturePaymentRequestIfc capturePaymentRequest,
            CapturePaymentResponseIfc capturePaymentResponse,
            PasTransactionBean transData,
            String transType) {
        PasFunction.setErrorCode(StatusCode.PAS_IN_VOID_STATUS, PasConstants.CANNOT_CAPTURE, capturePaymentResponse);
        // auth state
        AuthorizationStateType state = new AuthorizationStateType();
        PasFunction.initAuthStateFromTransBean(transData, state);
        capturePaymentResponse.setAuthorizationState(state);
        return false;
    }

    public boolean refund(
            Connection conn,
            RefundPaymentRequestType refundPaymentRequest,
            RefundPaymentResponseType refundPaymentResponse,
            PasTransactionBean transData) {
        //PasFunction.initResponse(refundPaymentRequest, refundPaymentResponse);
        PasFunction.setErrorCode(StatusCode.PAS_IN_VOID_STATUS, PasConstants.CANNOT_REFUND, refundPaymentResponse);
        // auth state
        AuthorizationStateType state = new AuthorizationStateType();
        PasFunction.initAuthStateFromTransBean(transData, state);
        refundPaymentResponse.setAuthorizationState(state);
        return false;
    }

    public boolean voidAuth(
            Connection conn,
            VoidAuthorizationRequestType voidAuthorizationRequest,
            VoidAuthorizationResponseType voidAuthorizationResponse,
            PasTransactionBean transData) {
        //PasFunction.initResponse(voidAuthorizationRequest, voidAuthorizationResponse);
        // instead of setting error and return false, return true to make it idempotent
        //PasFunction.setErrorCode(StatusCode.PAS_IN_VOID_STATUS, PasConstants.CANNOT_VOID, voidAuthorizationResponse);
        // auth state
        AuthorizationStateType state = new AuthorizationStateType();
        PasFunction.initAuthStateFromTransBean(transData, state);
        voidAuthorizationResponse.setAuthorizationState(state);
        return true;
    }

    public boolean getHistory(
            Connection conn,
            GetAuthorizationHistoryRequestType getAuthorizationHistoryRequest,
            GetAuthorizationHistoryResponseType getAuthorizationHistoryResponse,
            PasTransactionBean transData) {
        return false;
        // TODO Auto-generated method stub
    }
}

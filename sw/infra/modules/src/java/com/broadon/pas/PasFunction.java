package com.broadon.pas;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.logging.Log;

import com.broadon.wsapi.pas.AbstractRequestIfc;
import com.broadon.wsapi.pas.AbstractRequestType;
import com.broadon.wsapi.pas.AbstractResponseIfc;
import com.broadon.wsapi.pas.AbstractResponseType;
import com.broadon.wsapi.pas.AuthorizationStateType;
import com.broadon.wsapi.pas.AuthorizationStatusType;
import com.broadon.wsapi.pas.BalanceAccountInfoType;
import com.broadon.wsapi.pas.BalanceAccountType;
import com.broadon.wsapi.pas.CaptureAccountToAccountRequestType;
import com.broadon.wsapi.pas.CapturePaymentRequestType;
import com.broadon.wsapi.pas.ECardRecordType;
import com.broadon.wsapi.pas.MoneyType;
import com.broadon.wsapi.pas.BalanceType;
import com.broadon.wsapi.pas.PaymentMethodType;
import com.broadon.wsapi.pas.ECardInfoType;
import com.broadon.wsapi.pas.ECardKindType;
import com.broadon.wsapi.pas.PropertyType;

/**
 * The class conatins the following utilities:
 * 1. generic non bd-access methods.
 * 2. payment-method specific methods, which includes:
 *  2.1 methods specific to ECard
 *  2.2 methods specific to Balance Account
 *  2.3 methods specific to Credit Card
 */

public class PasFunction {
    
    private static Log log = 
        LogFactory.getLog(PasFunction.class.getName());
    private static BigDecimal bigDecimal0 = new BigDecimal("0");
    // Static helper functions
    static Map paymentImplMap = new HashMap();
    static Map statusObjMap = new HashMap();
    
    static {
        BasicServiceInternal ccs = (BasicServiceInternal) CreditCardServiceImpl.getInstance();
        paymentImplMap.put(PaymentMethodType.CCARD, ccs);
        BasicServiceInternal ecs = (BasicServiceInternal) ECardServiceImpl.getInstance();
        paymentImplMap.put(PaymentMethodType.ECARD, ecs);
        BasicServiceInternal bas = (BasicServiceInternal) BalanceAccountServiceImpl.getInstance();
        paymentImplMap.put(PaymentMethodType.ACCOUNT, bas);
        
        BasicServiceInternal voidStatus = VoidStatus.getInstance();
        statusObjMap.put(AuthorizationStatusType.Void.toString(), voidStatus);
        BasicServiceInternal pendingStatus = PendingStatus.getInstance();
        statusObjMap.put(AuthorizationStatusType.Pending.toString(), pendingStatus);
        BasicServiceInternal completeStatus = CompleteStatus.getInstance();
        statusObjMap.put(AuthorizationStatusType.Completed.toString(), completeStatus);
        BasicServiceInternal expiredStatus = ExpiredStatus.getInstance();
        statusObjMap.put(AuthorizationStatusType.Expired.toString(), expiredStatus);
    }
    
    static BasicServiceInternal getPaymentServiceImpl(PaymentMethodType paymentMethod) {
        return (BasicServiceInternal)paymentImplMap.get(paymentMethod);
    }
    
    static BasicServiceInternal getStatusObject(String status) {
        return (BasicServiceInternal) statusObjMap.get(status);
    }
    
    // generic section

    //  creates a capture request for internal use.
    static CapturePaymentRequestType createCaptureRequest(
            CaptureAccountToAccountRequestType captureAccountToAccountRequest,
            PasTransactionBean trans) {
        CapturePaymentRequestType ret = new CapturePaymentRequestType();
        ret.setIsFinal(false);
        ret.setVersion(captureAccountToAccountRequest.getVersion());
        ret.setMessageID(captureAccountToAccountRequest.getMessageID());
        ret.setProperties(captureAccountToAccountRequest.getProperties());
        ret.setAuthorizationToken(captureAccountToAccountRequest.getAuthorizationToken());
        ret.setAmount(captureAccountToAccountRequest.getAmount());
        return ret;
    }
    static boolean checkCaptureAmount(PasTransactionBean trans, MoneyType captureTotal) {
        BigDecimal totalCapture = new BigDecimal(captureTotal.getAmount().toString());
        if (totalCapture.compareTo(trans.getAmountAuthorized()) > 0)
            return false;
        else
            return true;
    }

    /**
     * Initializes fields in a AbstractResponseType given an AbstractRequestType
     * @param request   the data source of the initialization
     * @param response  the destination to be initialized
     */
    public static void initResponse(AbstractRequestIfc request,
                                       AbstractResponseIfc response)
    {
        if (request.getMessageID() == null || request.getVersion() == null) {
            setErrorCode(StatusCode.PAS_MISSING_DATA, null, response);
            return;
        }
        response.setVersion(request.getVersion());
        response.setMessageID(request.getMessageID());
        response.setTimeStamp(System.currentTimeMillis());
        response.setProperties(new PropertyType[0]);
        if (!PasConstants.VERSION.equals(request.getVersion().trim())) // needed? always backward compatible
            setErrorCode(StatusCode.PAS_VERSION_MISMATCH, null, response);
        else
            setErrorCode(StatusCode.SC_OK, null, response);
    }
    
    /**
     * Sets the error code in AbstractResponseType
     * @param code  the error code
     * @param message   the error message
     * @param response  the destination object 
     */
    public static void setErrorCode(String code, 
                                       String message,
                                       AbstractResponseIfc response)
    {
        response.setErrorCode(Integer.parseInt(code));
        if (message != null)
            response.setErrorMessage(StatusCode.getMessage(code) + message);
        else
            response.setErrorMessage(StatusCode.getMessage(code));
    }
    
    /**
     * padding zeros in front of eCardType to make the total length to be six characters
     * @param eCardType the eCardType to be padded.
     * @return the padded string
     */
    public static String getPaddedECardType (String eCardType) {
        StringBuffer sb = new StringBuffer("000000");
        int l = eCardType.length();
        return sb.substring(0, 6-l).toString()+eCardType;
    }
    
    /**
     * Initializes fields in a AuthorizationStateType using a PasTransactionBean
     * @param from  the source
     * @param toAuth    destination
     */
    protected static void initAuthStateFromTransBean(PasTransactionBean fromTrans,
                                        AuthorizationStateType toState)
    {
        toState.setStatus(AuthorizationStatusType.fromValue(fromTrans.getStatus()));
        toState.setAuthorizationTime(fromTrans.getTransDate().getTime());
        toState.setExpirationTime(fromTrans.getExpirationDate().getTime());
        toState.setAuthorizedAmount(new MoneyType(fromTrans.getCurrencyType(), fromTrans.getAmountAuthorized().toString()));
        toState.setCapturedAmount(new MoneyType(fromTrans.getCurrencyType(), fromTrans.getAmountCaptured().toString()));
        toState.setRefundedAmount(new MoneyType(fromTrans.getCurrencyType(), fromTrans.getAmountRefunded().toString()));
        toState.setIsRejected(false);
    }
    
    protected static AuthorizationStateType  dupAuthorizationState(AuthorizationStateType auth) {
        AuthorizationStateType ret = new AuthorizationStateType();
        ret.setStatus(auth.getStatus());
        ret.setAuthorizationTime(auth.getAuthorizationTime());
        ret.setExpirationTime(auth.getExpirationTime());
        ret.setAuthorizedAmount(new MoneyType(
                auth.getAuthorizedAmount().getCurrencyType(),
                auth.getAuthorizedAmount().getAmount()));
        ret.setCapturedAmount(new MoneyType(
                auth.getCapturedAmount().getCurrencyType(),
                auth.getCapturedAmount().getAmount()));
        ret.setRefundedAmount(new MoneyType(
                auth.getRefundedAmount().getCurrencyType(),
                auth.getRefundedAmount().getAmount()));
        ret.setIsRejected(auth.isIsRejected());
        return ret;
    }
    // ECard section
    /**
     * Initializes fields in a AuthorizationStateType using a PasTransactionBean
     * @param fromECard source
     * @param toECardInfo   destination
     */
    protected static void initECardInfoFromECardBean(ECardBean fromECard,
                                        ECardInfoType toECardInfo)
    {
        if (fromECard.isUsedOnce())
            toECardInfo.setECardKind(ECardKindType.UseOnce);
        else
            toECardInfo.setECardKind(ECardKindType.UseMany);

        Date tmp = fromECard.getActivateDate();
        if (tmp != null)
            toECardInfo.setActivatedTime(new Long(tmp.getTime()));
        tmp = fromECard.getRevokeDate();
        if (tmp != null)
            toECardInfo.setRevokedTime(new Long(tmp.getTime()));
        tmp = fromECard.getLastUsed();
        if (tmp != null)
            toECardInfo.setLastUsedTime(new Long(tmp.getTime()));
        
        toECardInfo.setBalance(new BalanceType(
                new MoneyType(fromECard.getCurrency(), fromECard.getBalance().toString()),
                new MoneyType(fromECard.getCurrency(), fromECard.getBalanceAuthorized().toString()),
                new MoneyType(fromECard.getCurrency(), fromECard.getBalance().subtract(fromECard.getBalanceAuthorized()).toString())));
    }

    protected static void initAccountInfo(AccountBean account, BalanceAccountInfoType accountInfo) {
        accountInfo.setAccountType(account.getAccountType());
        Date aDate = account.getActivateDate();
        if (aDate != null)
            accountInfo.setActivatedTime(new Long(aDate.getTime()));
        String currencyType = account.getCurrency();
        MoneyType balance = new MoneyType(currencyType, account.getBalance().toString());
        MoneyType reserved = new MoneyType(currencyType, account.getBalanceAuthorized().toString());
        MoneyType available = new MoneyType(currencyType, 
                account.getBalance().subtract(account.getBalanceAuthorized()).toString());
        BalanceType balanceInfo = new BalanceType(balance, reserved, available);
        accountInfo.setBalance(balanceInfo);
        aDate = account.getDeactivateDate();
        if (aDate != null)
            accountInfo.setDeactivateTime(new Long(aDate.getTime()));
        aDate = account.getRevokeDate();
        if (aDate != null)
            accountInfo.setRevokedTime(new Long(aDate.getTime()));
    }
    
    // retrieve ECard based on eCardRecord on the request and verify it.
    // Usage:
    // for withdraw: checkForWithdraw, checkForDeposit and validation all are  true
    // for deposit: checkForDeposit and validation are true and checkForWithdraw is false
    // for info. only, validation is true and checkForWithdraw, checkForDeposit is false
    public static final int INFO_ONLY = 1;
    public static final int CHECK_FORMAT = 2;
    public static final int VALIDATE = 3;
    public static final int CHECK_USABLE = 4;
    public static final int WITHDRAW = 5;
    public static final int DEPOSIT = 6;
    static ECardBean checkECard(AbstractRequestType request, AbstractResponseType response,
                              ECardRecordType eCardRecord, int checkLevel, Connection conn) throws SQLException
    {
        PasFunction.initResponse(request, response);
        if (response.getErrorCode()!= PasConstants.STATUS_OK)
            return null;
        if (eCardRecord == null) { 
            PasFunction.setErrorCode(StatusCode.PAS_MISSING_DATA, 
                    PasConstants.ECARD_STRING_FORMAT_ERROR, response);
            return null;
        }
        String eCardId = eCardRecord.getECardNumber();
        if (checkLevel >= CHECK_FORMAT) {
            if (eCardId == null || eCardId.length() != PasConstants.serialSize) { 
                PasFunction.setErrorCode(StatusCode.PAS_MISSING_DATA, 
                    PasConstants.ECARD_STRING_FORMAT_ERROR, response);
                return null;
            }
            String eCardType = eCardRecord.getECardType();
            if (eCardType == null || eCardType.length() != PasConstants.typeSize) { 
                PasFunction.setErrorCode(StatusCode.PAS_MISSING_DATA, 
                    PasConstants.ECARD_STRING_FORMAT_ERROR, response);
                return null;
            }
            String eCardHash = eCardRecord.getHashNumber();
            if (eCardHash == null || eCardHash.length() != PasConstants.md5Size) { 
                PasFunction.setErrorCode(StatusCode.PAS_MISSING_DATA, 
                    PasConstants.ECARD_STRING_FORMAT_ERROR, response);
                return null;
            }
        }
        // get ecard account from db
        ECardBean eCard = PasDBFunction.getECard(conn, Integer.parseInt(eCardId));
        if (eCard == null) {
            PasFunction.setErrorCode(StatusCode.PAS_NO_ACCOUNT_RECORD, 
                    PasConstants.NO_ECARD_RECORD, response);
            return null;
        }
        if (checkLevel >= VALIDATE) {
            ValidateECard vec = new ValidateECard(conn);
            if (!vec.isValid(eCardRecord.getECardType()+eCardRecord.getECardNumber()+eCardRecord.getHashNumber())) {
                PasFunction.setErrorCode(StatusCode.PAS_INVALID_ECARD, 
                    eCardRecord.getECardType()+eCardRecord.getECardNumber()+eCardRecord.getHashNumber(), response);
                return null;
            }
        }
        if (checkLevel >= CHECK_USABLE) {
            if (!eCard.isUsable()) {
                PasFunction.setErrorCode(StatusCode.PAS_ACCOUNT_NOT_USABLE, 
                    null, response);
                return null;
            }
            Date today = PasDBFunction.getCurrentDBTime(conn);
            if (today == null) {
                PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, 
                    null, response);
                return null;
            }
            Date aDate = eCard.getExpireDate();
                if (aDate != null && aDate.before(today)) { // expired
                    PasFunction.setErrorCode(StatusCode.PAS_ACCOUNT_EXPIRED, 
                            null, response);
                    return null;
                }
                aDate = eCard.getRevokeDate();
                if (aDate != null && aDate.before(today)) { //revoked
                    PasFunction.setErrorCode(StatusCode.PAS_ACCOUNT_REVOKED, 
                            null, response);
                    return null;
                }
        }
        if (checkLevel == WITHDRAW) {
            if (eCard.isUsedOnce()) {
                Date aDate = eCard.getLastUsed();
                if (aDate != null || eCard.getBalanceAuthorized().compareTo(bigDecimal0) > 0) {
                    PasFunction.setErrorCode(StatusCode.PAS_ACCOUNT_IS_USED_ONCE, 
                            null, response);
                    return null;
                }
            }
            if (!eCard.validCountry(eCardRecord.getCountryCode())) {
                PasFunction.setErrorCode(StatusCode.PAS_ECARD_COUNTRY_CODE, 
                    null, response);
                return null;
            }
        }
        if (checkLevel == DEPOSIT) {
            if (eCard.isUsedOnce()) {
                Date aDate = eCard.getLastUsed();
                if (aDate != null || eCard.getBalanceAuthorized().compareTo(bigDecimal0) > 0) {
                    PasFunction.setErrorCode(StatusCode.PAS_ACCOUNT_IS_USED_ONCE, 
                            null, response);
                    return null;
                }
            }
            if (!eCard.isAllowRefill()) {
                PasFunction.setErrorCode(StatusCode.PAS_ACCOUNT_NOT_REFILLABLE, 
                        null, response);
                return null;
            }
        }
       
        return eCard;
    }
    
    // Balance Account section
    static AccountBean checkBalanceAccount(
            AbstractRequestIfc request, 
            AbstractResponseIfc response, 
            BalanceAccountType acct,
            boolean checkActivated, boolean checkPin, Connection conn) {
        PasFunction.initResponse(request, response);
        if (response.getErrorCode()!= PasConstants.STATUS_OK)
            return null;
        String accountId = acct.getAccountNumber();
        if (accountId == null) { 
            PasFunction.setErrorCode(StatusCode.PAS_MISSING_DATA, 
                    PasConstants.NO_ACCOUNT_ID, response);
            return null;
        }
        // TODO: fix the interface later
        AccountBean account = PasDBFunction.getAccount(conn, Integer.parseInt(accountId));
        if (account == null) {
            PasFunction.setErrorCode(StatusCode.PAS_NO_ACCOUNT_RECORD, 
                    PasConstants.NO_ACCOUNT_RECORD, response);
            return null;
        }
        // check pin
        if (checkPin && account.getPin() != Integer.parseInt(acct.getPIN())) { 
            PasFunction.setErrorCode(StatusCode.PAS_PIN_MISMATCH, 
                    PasConstants.PIN_MISMATCH + "pin1= " + account.getPin() + ", pin2 = " + acct.getPIN(), response);
            return null;
        }
        if (checkActivated) {
            Date today = PasDBFunction.getCurrentDBTime(conn);
            if (today == null) {
                PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, 
                        PasConstants.CANNOT_AUTHORIZE, response);
                return null;
            }
            if (account.getDeactivateDate() != null && account.getDeactivateDate().before(today)) {
                PasFunction.setErrorCode(StatusCode.PAS_ACCOUNT_NOT_ACTIVATED, 
                    PasConstants.ACCT_PROBLEM, response);
                return null;
            }
            if (account.getRevokeDate() != null && account.getRevokeDate().before(today)) {
                PasFunction.setErrorCode(StatusCode.PAS_ACCOUNT_REVOKED, 
                    PasConstants.ACCT_PROBLEM, response);
                return null;
            }
            
        }
        return account;
    }
    
    /**
     * Initializes fields in a BalanceAccountInfoType from an AccountBean
     * @param account   source
     * @param accountInfo   destination
     */
    protected static void initBalanceAccountInfoFromAccountBean(AccountBean account,
                                        BalanceAccountInfoType accountInfo)
    {
        accountInfo.setAccountType(account.getAccountType());
        accountInfo.setActivatedTime(new Long(account.getActivateDate().getTime()));
        if (account.getDeactivateDate()!=null)
            accountInfo.setDeactivateTime(new Long(account.getDeactivateDate().getTime()));
        accountInfo.setLastUpdatedTime(new Long(account.getLastUpdated().getTime()));
        if (account.getRevokeDate()!=null)
            accountInfo.setRevokedTime(new Long(account.getRevokeDate().getTime()));
        accountInfo.setBalance(new BalanceType(
                new MoneyType(account.getCurrency(), account.getBalance().toString()), 
                new MoneyType(account.getCurrency(), account.getBalanceAuthorized().toString()), 
                new MoneyType(account.getCurrency(), account.getBalance().subtract(account.getBalanceAuthorized()).toString())));
    }
    
    public static void main(String args[]) {
        String eCardType = "10";
        System.out.println(getPaddedECardType(eCardType));
        eCardType = "12345";
        System.out.println(getPaddedECardType(eCardType));
    }
}

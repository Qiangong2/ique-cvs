package com.broadon.pas;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Data store for database BalanceAccount table records.
 *
 */
public class AccountBean {
    private int accountId;
    private String accountType;
    private Date activateDate;
    private Date deactivateDate;
    private Date revokeDate;
    private BigDecimal balance;
    private BigDecimal balanceAuthorized;
    private String currency;
    private Date lastUpdated;
    private int pin;
    
    public AccountBean() {
        super();
        // TODO Auto-generated constructor stub
    }

    int getAccountId() {
        return accountId;
    }

    void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    String getAccountType() {
        return accountType;
    }

    void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    Date getActivateDate() {
        return activateDate;
    }

    void setActivateDate(Date activateDate) {
        this.activateDate = activateDate;
    }

    BigDecimal getBalance() {
        return balance;
    }

    void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    BigDecimal getBalanceAuthorized() {
        return balanceAuthorized;
    }

    void setBalanceAuthorized(BigDecimal balanceAuthorized) {
        this.balanceAuthorized = balanceAuthorized;
    }
    
    BigDecimal getBalanceAvailable() {
        return balance.subtract(balanceAuthorized);
    }
    
    String getCurrency() {
        return currency;
    }

    void setCurrency(String currency) {
        this.currency = currency;
    }

    Date getDeactivateDate() {
        return deactivateDate;
    }

    void setDeactivateDate(Date deactivateDate) {
        this.deactivateDate = deactivateDate;
    }

    Date getLastUpdated() {
        return lastUpdated;
    }

    void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    Date getRevokeDate() {
        return revokeDate;
    }

    void setRevokeDate(Date revokeDate) {
        this.revokeDate = revokeDate;
    }

    int getPin() {
        return pin;
    }

    void setPin(int pin) {
        this.pin = pin;
    }

}

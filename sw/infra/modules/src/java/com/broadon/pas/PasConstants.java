package com.broadon.pas;

public interface PasConstants {
    String VERSION = "1.0";
    int STATUS_OK = 0;
    String PENDING_STATUS = "pending";
    String COMPLETE_STATUS = "complete";
    String EXPIRED_STATUS = "expired";
    String VOID_STATUS = "void";
    
    // eCard number size, consistent with that defined in ECardNumber
    int typeSize = 6;
    int serialSize = 10;
    int randomSize = 10;
    int md5Size = 32;
    long CacheTimeOut = 1000*5; // set a short 5 seconds timeout

    // put into resource bundle?
    String CANNOT_AUTHORIZE = " -- cannot authorize";
    String CANNOT_CAPTURE = " -- cannot capture";
    String CANNOT_REFUND = " -- cannot refund";
    String CANNOT_VOID = " -- cannot void";
    String CANNOT_GET_ACCOUNT_HISTORY = " -- cannot get account history";
    String CANNOT_GET_AUTHORIZATION_HISTORY = " -- cannot get authorization history";
    String CANNOT_DEPOSIT = " -- cannot deposit";
    String CANNOT_WITHDRAW = " -- cannot withdraw";
    String CANNOT_GET_ACCOUNT_BALANCE = " -- cannot get account balance";
    String CANNOT_TRANSFER = " -- cannot transfer balance";
    String CANNOT_REDEEM = " -- cannot redeem ecard";
    String CANNOT_DISABLE_ACCOUNT = " -- cannot disable account";
    String CANNOT_ENABLE_ACCOUNT = " -- cannot enable account";
    String CANNOT_CREATE_ACCOUNT = " -- cannot create account";
    
    String NO_AUTHO_TOKEN = " -- no authorization token";
    String MONEY_FORMAT_ERROR = " -- money string format error";
    String ECARD_STRING_FORMAT_ERROR = " -- ecard string should be exact 48(6+10+32) digits";
    String NO_ECARD_RECORD = " -- cannot get databse eCard record";
    String NO_ACCOUNT_ID = " -- no balance account id";
    String NO_ACCOUNT_RECORD = " -- no database account record";
    
    String AUTHORIZE_ACCOUNT_PAYMENT = " -- authorizeAccountPayment";
    String GET_BALANCE_ACCOUNT_HISTORY = " -- getBalanceAccountHistory";
    String DEPOSIT_PAYMENT = " -- depositPayment";
    String TRANSFER_BALANCE = " -- transferBalance";
    String CHECK_ACCOUNT_BALANCE = " -- checkAccountBalance";
    
    String AUTHORIZE_CREDITCARD_PAYMENT = " -- authorizeCreditCardPayment";
    String GET_CREDITCARD_HISTORY = " -- getCreditCardHistory";
    
    String PIN_MISMATCH = " -- PIN does not match";
    String ACCT_PROBLEM = " -- Account Deactivated or Revoked";
}

package com.broadon.pas;

public interface ECardService extends AuthorizationBasicService{
    public com.broadon.wsapi.pas.AuthorizeECardPaymentResponseType authorizeECardPayment(
            PaymentAuthorizationServiceImpl service,
            com.broadon.wsapi.pas.AuthorizeECardPaymentRequestType authorizeECardPaymentRequest);
    public com.broadon.wsapi.pas.GetECardHistoryResponseType getECardAccountHistory(
            PaymentAuthorizationServiceImpl service,
            com.broadon.wsapi.pas.GetECardHistoryRequestType getECardHistoryRequest);
    /*    
    public com.broadon.wsapi.pas.DepositPaymentResponseType depositPayment(
            PaymentAuthorizationServiceImpl service,
            com.broadon.wsapi.pas.DepositPaymentRequestType depositPaymentRequest);
    */
    public com.broadon.wsapi.pas.CheckECardBalanceResponseType checkECardBalance(
            PaymentAuthorizationServiceImpl service,
            com.broadon.wsapi.pas.CheckECardBalanceRequestType checkECardBalanceRequest);
    public com.broadon.wsapi.pas.GetECardTypeResponseType getECardType(
            PaymentAuthorizationServiceImpl service,
            com.broadon.wsapi.pas.GetECardTypeRequestType getECardTypeRequest);
}

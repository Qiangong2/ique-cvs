package com.broadon.pas;

import java.sql.*;
import java.security.*;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.RSAPublicKeySpec;
import java.math.BigInteger;
import com.broadon.util.ECardNumber;

/**
 * Validate an eCard number.
 */
public class ValidateECard
{
    static final String getSignature =
	"SELECT A.ECARD_HASH, C.PUBLIC_KEY " +
	"FROM ECARDS A, ECARD_BATCHES B, CERTIFICATES C, ECARD_TYPES D "+
	"WHERE A.ECARD_ID=? AND A.ECARD_TYPE=? AND " +
	"      (A.ECARD_ID >= B.START_ECARD_ID) AND " +
	"      (A.ECARD_ID <= B.END_ECARD_ID) AND " +
	"      (B.CERT_ID = C.CERT_ID) AND " +
	"      (A.ECARD_TYPE = D.ECARD_TYPE)";

    static Signature sig;
    static KeyFactory kf;
    PreparedStatement ps;

    static {
	try {
	    sig = Signature.getInstance("SHA1withRSA");
	    kf = KeyFactory.getInstance("RSA");
	} catch (NoSuchAlgorithmException e) {
	    e.printStackTrace();
	    sig = null;
	    kf = null;
	}
    } 

    /**
     * Create an <code>ValidateEcard</code> object for validating one
     * or multiple eCards.
     * @param conn Database connection.
     * @exception SQLException
     */
    public ValidateECard(Connection conn)
	throws SQLException
    {
	ps = conn.prepareStatement(getSignature);
    }


    RSAPublicKey getPublicKey(String key)
	throws GeneralSecurityException
    {
	BigInteger modulus = new BigInteger(key, 32);
	BigInteger exponent = new BigInteger("10001", 16);
	return (RSAPublicKey)
	    kf.generatePublic(new RSAPublicKeySpec(modulus, exponent));
    } // getPublicKey
    

    /**
     * Check if the given ecard is valid.
     * @param ecard Ecard number to be verified.
     * @return True iff the ecard number is valid.  Note it does not
     * verify if the ecard has been redeemed, revoked, etc.
     * @exception SQLException DB access error.
     */
    public boolean isValid(String ecard)
	throws SQLException
    {
       
	try {
	    ps.setLong(1, Long.parseLong(ECardNumber.extractSerial(ecard)));
	    ps.setInt(2, Integer.parseInt(ECardNumber.extractType(ecard)));
	    ResultSet rs = ps.executeQuery();
	    if (rs.next()) {
		sig.initVerify(getPublicKey(rs.getString(2)));
		sig.update(ecard.getBytes());
		return sig.verify(rs.getBytes(1));
	    } 
	    return false;
	} catch (GeneralSecurityException e) {
	    return false;
	} catch (StringIndexOutOfBoundsException e) {
	    return false;
    }
	}

    public void close()
	throws SQLException
    {
        if (ps != null)
            ps.close();
    }
}

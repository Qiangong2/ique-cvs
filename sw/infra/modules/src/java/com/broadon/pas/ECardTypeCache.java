package com.broadon.pas;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.logging.Log;

import com.broadon.db.DataSourceScheduler;
import com.broadon.util.BackingStoreException;
import com.broadon.util.CacheLFU;

public class ECardTypeCache extends CacheLFU implements com.broadon.util.BackingStore{
    
    private static Log log = 
        LogFactory.getLog(ECardTypeCache.class.getName());
    
    private PaymentAuthorizationServiceImpl service;

    public ECardTypeCache(PaymentAuthorizationServiceImpl service)
    {
        // set cache size=5 and timeout=5 seconds.
        super(5, PasConstants.CacheTimeOut);
        this.service = service;
    }
    
    public Object retrieve(Object eCardTypeKey) throws BackingStoreException
    {
        int eCardType = ((Integer) eCardTypeKey).intValue();
        Connection conn = null;
        ECardTypeBean eCardTypeBean = null;
        try {
           conn = service.getConnection(DataSourceScheduler.DB_PRIORITY_FAST);
           eCardTypeBean = PasDBFunction.getECardType(conn, eCardType);
           if (eCardTypeBean == null)
               throw new BackingStoreException();
        } catch (SQLException e) {
            throw BackingStoreException.rewrap(e);            
        } finally {
            if (conn != null)
                try {
                    conn.close();
                } catch (SQLException e2) {
                    log.error("cannot close connection: ", e2);
                }
        }
        return eCardTypeBean;
    }

    /**
     * Returns details for a given eCardType
     */
    public ECardTypeBean getECardTypeBean(Integer ECardTypeId)
        throws BackingStoreException
    {
        return (ECardTypeBean) get(ECardTypeId);
    }
    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub

    }

}

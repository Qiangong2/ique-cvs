package com.broadon.pas;
import java.math.BigDecimal;
import java.sql.Connection;

import com.broadon.wsapi.pas.CapturePaymentRequestIfc;
import com.broadon.wsapi.pas.CapturePaymentResponseIfc;
import com.broadon.wsapi.pas.GetAuthorizationHistoryRequestType;
import com.broadon.wsapi.pas.GetAuthorizationHistoryResponseType;
import com.broadon.wsapi.pas.PaymentMethodType;
import com.broadon.wsapi.pas.RefundPaymentRequestType;
import com.broadon.wsapi.pas.RefundPaymentResponseType;
import com.broadon.wsapi.pas.VoidAuthorizationRequestType;
import com.broadon.wsapi.pas.VoidAuthorizationResponseType;
import com.broadon.wsapi.pas.AuthorizationStateType;
public class CompleteStatus implements BasicServiceInternal {
private static BasicServiceInternal singleton = new CompleteStatus();
    
    public static BasicServiceInternal getInstance() {
        return singleton;
    }

    private CompleteStatus() {
        super();
        // TODO Auto-generated constructor stub
    }

    public boolean capture(
            Connection conn,
            CapturePaymentRequestIfc capturePaymentRequest,
            CapturePaymentResponseIfc capturePaymentResponse,
            PasTransactionBean transData,
            String transType) {
        // if captureTotal == authorizedAmount, don't set error to
        // allow idempotence
        if (new BigDecimal(capturePaymentRequest.getAmount().getAmount()).compareTo(transData.getAmountAuthorized()) != 0)
            PasFunction.setErrorCode(StatusCode.PAS_IN_COMPLETE_STATUS, 
                PasConstants.CANNOT_CAPTURE, capturePaymentResponse);
        // auth state
        AuthorizationStateType state = new AuthorizationStateType();
        PasFunction.initAuthStateFromTransBean(transData, state);
        capturePaymentResponse.setAuthorizationState(state);
        return false;
    };

    public boolean refund(
            Connection conn,
            RefundPaymentRequestType refundPaymentRequest,
            RefundPaymentResponseType refundPaymentResponse,
            PasTransactionBean transData) {
        PaymentMethodType paymentMethod = refundPaymentRequest.getAuthorizationToken().getPaymentMethod();
        return PasFunction.getPaymentServiceImpl(paymentMethod).refund(
                conn, refundPaymentRequest, refundPaymentResponse, transData);
    }

    public boolean voidAuth(
            Connection conn,
            VoidAuthorizationRequestType voidAuthorizationRequest,
            VoidAuthorizationResponseType voidAuthorizationResponse,
            PasTransactionBean transData) {
        //PasFunction.initResponse(voidAuthorizationRequest, voidAuthorizationResponse);
        PasFunction.setErrorCode(StatusCode.PAS_IN_COMPLETE_STATUS, 
                PasConstants.CANNOT_VOID, voidAuthorizationResponse);
        // auth state
        AuthorizationStateType state = new AuthorizationStateType();
        PasFunction.initAuthStateFromTransBean(transData, state);
        voidAuthorizationResponse.setAuthorizationState(state);
        return false;
    }

    public boolean getHistory(
            Connection conn,
            GetAuthorizationHistoryRequestType getAuthorizationHistoryRequest,
            GetAuthorizationHistoryResponseType getAuthorizationHistoryResponse,
            PasTransactionBean transData) {
        return false;
        // TODO Auto-generated method stub
    }

}

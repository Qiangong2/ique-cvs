package com.broadon.pas;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.TimerTask;

import net.wiivc.services.CreditCardFunctions;
import net.wiivc.services.NonBlockingAgent;
import net.wiivc.services.PointsAppResults;

import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.logging.Log;

import com.broadon.wsapi.pas.AuthorizeAccountPaymentResponseType;

public class PointsApply extends TimerTask{
    private static Log log = 
        LogFactory.getLog(PointsApply.class.getName());
    private PaymentAuthorizationServiceImpl service;
    private static final String process = "PAS POINTS APPLIED PROCESS";
    private static final String server = System.getProperty("HOSTNAME");
    
    public PointsApply(PaymentAuthorizationServiceImpl service) {
        super();
        this.service = service;
    }

    public void run() {
        Connection conn = null;
        try {
            // get db connection
            conn = service.getConnection();
            // lock global record
            PasDBFunction.lockProcessLock(conn, process);
            // get a list of transactions
            String txIds[] = PasDBFunction.getPointsApplyTx(conn);
            if (txIds != null && txIds.length > 0) {
                // create an agent and send the data 
                String url = service.getCCPointsApplyURL();
                long waitTime = service.getPointsAppTimeoutMillis();
                CreditCardFunctions agent = new NonBlockingAgent(url, waitTime);
                agent.applyPoints(txIds);
                // retrieve results
                PointsAppResults[] results = agent.getApplyPoints();
                // update table
                for (int i=0; i< results.length; i++) {
                    PasDBFunction.markPointsConfirmed(conn, results[i]);
                }
            }
            PasDBFunction.updateProcessLock(conn, process, server);  // mark this run
            // commit
            conn.commit();
        } catch (SQLException e) {
            log.debug("PointsApply error: ", e);
        } catch (Throwable e) {
            e.printStackTrace();
            log.error("PointsApply error: ", e);
        } finally {
            try {                
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            }
            catch (SQLException e) {
                log.warn("PointsApply error while closing connection: ", e);
            }
        }         
    }

}

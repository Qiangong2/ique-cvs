package com.broadon.pas;
import java.util.Date;

public class ECardBatchBean {
    private long endECardId;
    private long startECardId;
    private Date createDate;
    private int eCardType;
    private Date printDate;
    private Date shipDate;
    private Date uploadDate;
    private Date expireDate;
    private Date revokeDate;
    private Date activateDate;
    
    public ECardBatchBean() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub

    }

    Date getActivateDate() {
        return activateDate;
    }

    void setActivateDate(Date activateDate) {
        this.activateDate = activateDate;
    }

    Date getCreateDate() {
        return createDate;
    }

    void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    int getECardType() {
        return eCardType;
    }

    void setECardType(int cardType) {
        eCardType = cardType;
    }

    long getEndECardId() {
        return endECardId;
    }

    void setEndECardId(long endECardId) {
        this.endECardId = endECardId;
    }

    Date getExpireDate() {
        return expireDate;
    }

    void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    Date getPrintDate() {
        return printDate;
    }

    void setPrintDate(Date printDate) {
        this.printDate = printDate;
    }

    Date getRevokeDate() {
        return revokeDate;
    }

    void setRevokeDate(Date revokeDate) {
        this.revokeDate = revokeDate;
    }

    Date getShipDate() {
        return shipDate;
    }

    void setShipDate(Date shipDate) {
        this.shipDate = shipDate;
    }

    long getStartECardId() {
        return startECardId;
    }

    void setStartECardId(long startECardId) {
        this.startECardId = startECardId;
    }

    Date getUploadDate() {
        return uploadDate;
    }

    void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

}

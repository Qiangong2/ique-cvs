package com.broadon.pas;

public interface AuthorizationBasicService {
    public com.broadon.wsapi.pas.CapturePaymentResponseType capturePayment(
            PaymentAuthorizationServiceImpl service,
            com.broadon.wsapi.pas.CapturePaymentRequestType capturePaymentRequest);
    public com.broadon.wsapi.pas.RefundPaymentResponseType refundPayment(
            PaymentAuthorizationServiceImpl service,
            com.broadon.wsapi.pas.RefundPaymentRequestType refundPaymentRequest);
    public com.broadon.wsapi.pas.VoidAuthorizationResponseType voidAuthorization(
            PaymentAuthorizationServiceImpl service,
            com.broadon.wsapi.pas.VoidAuthorizationRequestType voidAuthorizationRequest);
    /*
    public com.broadon.wsapi.pas.DisableAccountResponseType disableAccount(
            PaymentAuthorizationServiceImpl service,
            com.broadon.wsapi.pas.DisableAccountRequestType disableAccountRequest);
    public com.broadon.wsapi.pas.ChangeAccountPINResponseType changeAccountPIN(
            PaymentAuthorizationServiceImpl service,
            com.broadon.wsapi.pas.ChangeAccountPINRequestType changeAccountPINRequest);
    */
}

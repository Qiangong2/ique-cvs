package com.broadon.pas;
import java.util.Date;
import java.math.BigDecimal;

public class PasTransactionBean {

    private String transId;
    private String paymentMethodId;
    private String paymentMethod;
    private String transType;
    private Date transDate;
    private Date expirationDate;
    private String status;
    //private float amountAuthorized;
    //private float amountCaptured;
    //private float amountRefunded;
    private BigDecimal amountAuthorized;
    private BigDecimal amountCaptured;
    private BigDecimal amountRefunded;
    private String currencyType;
    private String referenceId;
    private int actionId;
    private String lastExtAuthId;
    
    String getLastExtAuthId() {
        return lastExtAuthId;
    }

    void setLastExtAuthId(String lastExtAuthId) {
        this.lastExtAuthId = lastExtAuthId;
    }

    public PasTransactionBean() {
        super();
        // TODO Auto-generated constructor stub
        // populate with persistant data
    }

    int getActionId() {
        return actionId;
    }

    void setActionId(int actionId) {
        this.actionId = actionId;
    }

    BigDecimal getAmountAuthorized() {
        return amountAuthorized;
    }

    void setAmountAuthorized(BigDecimal amountAuthorized) {
        this.amountAuthorized = amountAuthorized;
    }

    BigDecimal getAmountCaptured() {
        return amountCaptured;
    }

    void setAmountCaptured(BigDecimal amountCaptured) {
        this.amountCaptured = amountCaptured;
    }

    BigDecimal getAmountRefunded() {
        return amountRefunded;
    }

    void setAmountRefunded(BigDecimal amountRefunded) {
        this.amountRefunded = amountRefunded;
    }
    
    String getCurrencyType() {
        return currencyType;
    }
    
    void setCurrencyType(String currencyType) {
        this.currencyType = currencyType;
    }
    
    Date getExpirationDate() {
        return expirationDate;
    }

    void setExpirationDate(Date expDate) {
        this.expirationDate = expDate;
    }

    String getPaymentMethod() {
        return paymentMethod;
    }

    void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentMethodId() {
        return paymentMethodId;
    }

    void setPaymentMethodId(String paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    String getReferenceId() {
        return referenceId;
    }

    void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    String getStatus() {
        return status;
    }

    void setStatus(String status) {
        this.status = status;
    }

    Date getTransDate() {
        return transDate;
    }

    void setTransDate(Date transDate) {
        this.transDate = transDate;
    }

    String getTransId() {
        return transId;
    }

    void setTransId(String transId) {
        this.transId = transId;
    }

    String getTransType() {
        return transType;
    }

    void setTransType(String transType) {
        this.transType = transType;
    }
    
 

}

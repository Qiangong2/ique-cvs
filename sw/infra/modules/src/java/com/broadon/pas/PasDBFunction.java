package com.broadon.pas;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Enumeration;
import java.util.Properties;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;

import net.wiivc.services.PointsAppResults;

import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.logging.Log;

import com.broadon.util.UUID;
import com.broadon.wsapi.pas.AbstractResponseIfc;
import com.broadon.wsapi.pas.AccountHistory;
import com.broadon.wsapi.pas.AuthorizationRecordType;
import com.broadon.wsapi.pas.GetAuthorizationHistoryResponseType;
import com.broadon.wsapi.pas.AuthorizationStateType;
import com.broadon.wsapi.pas.AuthorizationStatusType;
import com.broadon.wsapi.pas.ActionKindType;
import com.broadon.wsapi.pas.ActionRecordType;
import com.broadon.wsapi.pas.TransactionRecordType;
import com.broadon.wsapi.pas.PaymentMethodType;

public class PasDBFunction {
    
    private static Log log = 
        LogFactory.getLog(PasDBFunction.class.getName());
    private static ECardTypeCache eCardTypeCache;
    private static ECardBatchCache eCardBatchCache;
    
    static void init(PaymentAuthorizationServiceImpl service) {
        eCardTypeCache = new ECardTypeCache(service);
        eCardBatchCache = new ECardBatchCache(service);
    }

    static Date getCurrentDBTime(Connection conn) {
        String query = "SELECT BCCUTIL.ConvertTimeStamp(SYS_EXTRACT_UTC(Current_Timestamp)) from dual";
        Date d = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            if (rs.next())
                d=new Date(rs.getLong(1));
        } catch(SQLException sqlE) {
            log.debug("error code: " + sqlE.getErrorCode());
            log.debug("state: " + sqlE.getSQLState());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                log.error("cannot close stmt or resultSet: ", e);
            }
        }
        return d;
    }
    
    static String[] getCountryCodes(Connection conn, int eCardId, int eCardType) {
        String countQuery = "select count(COUNTRY) from COUNTRIES c, ECARD_COUNTRIES ec"
            + " where c.COUNTRY_ID=ec.COUNTRY_ID and ("
            + eCardId
            + " between START_ECARD_ID and END_ECARD_ID or "
            + " ECARD_TYPE = " + eCardType + ")";
        String query = "select COUNTRY from COUNTRIES c, ECARD_COUNTRIES ec"
            + " where c.COUNTRY_ID=ec.COUNTRY_ID and ("
            + eCardId
            + " between START_ECARD_ID and END_ECARD_ID or "
            + " ECARD_TYPE = " + eCardType + ")";
        int n = 0;
        String[] countryCodes = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.createStatement();
            log.debug("SQL: " + countQuery);
            rs = stmt.executeQuery(countQuery);
            if (rs.next())
                n = rs.getInt(1);
            countryCodes = new String[n];
            log.debug("SQL: " + query);
            rs = stmt.executeQuery(query);
            int i=0;
            while (rs.next())
                countryCodes[i++] = rs.getString("COUNTRY");
        } catch(SQLException sqlE) {
            log.debug("error code: " + sqlE.getErrorCode());
            log.debug("state: " + sqlE.getSQLState());
            countryCodes = null;    // reset to null as error indicator
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                log.error("cannot close stmt or resultSet: ", e);
            }
        }
        return countryCodes;
    }
    static ECardBean getECard(Connection conn, int eCardId) {
        String query = "select ECARD_TYPE, BALANCE,"
            + " BALANCE_AUTHORIZED, IS_USABLE,"
            + " BCCUTIL.ConvertTimeStamp(LAST_USED), BCCUTIL.ConvertTimeStamp(ACTIVATE_DATE), "
            + " BCCUTIL.ConvertTimeStamp(REVOKE_DATE)"
            + " from \"PAS\".\"ECARDS\" where ECARD_ID=" + eCardId
            + " for update NOWAIT";
        log.debug("SQL: " + query);
        ECardBean eCard = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            /* 
             private long eCardId;
             private int eCardType;
             private float balance;
             private float balanceAuthorized; 
             private boolean isUsable;
             private Date lastUsed;
             //private Date expireDate;
             private Date activateDate;
             private Date revokeDate;
             //private String activateCode;
             */
            long timeStamp;
            if (rs.next()) {
                eCard = new ECardBean();
                eCard.setECardId(eCardId);
                eCard.setECardType(rs.getInt("ECARD_TYPE"));
                eCard.setBalance(rs.getBigDecimal("BALANCE"));
                eCard.setBalanceAuthorized(rs.getBigDecimal("BALANCE_AUTHORIZED"));
                int usable = rs.getInt("IS_USABLE");
                if (usable == 0)
                    eCard.setUsable(false);
                else
                    eCard.setUsable(true);
                // what is the column name to use?
                //eCard.setActivateDate(rs.getTimestamp("ACTIVATE_DATE"));
                //eCard.setRevokeDate(rs.getTimestamp("REVOKE_DATE"));
                //eCard.setLastUsed(rs.getTimestamp("LAST_USED"));
                timeStamp = rs.getLong(5);
                if (timeStamp > 0)
                    eCard.setLastUsed(new Date(timeStamp));
                timeStamp = rs.getLong(6);
                if (timeStamp > 0)
                    eCard.setActivateDate(new Date(timeStamp));
                timeStamp = rs.getLong(7);
                if (timeStamp > 0)
                    eCard.setRevokeDate(new Date(timeStamp));
            }
            if (eCard != null) {
                eCard.setCountryCode(getCountryCodes(conn, eCardId, eCard.getECardType()));
                // the associated eCardType and eCardbatch record must exist
                // to make the eCard record a consistent data in the system
                // ECardTypeBean eCardTypeBean = getECardType(conn, eCard.getECardType());
                ECardTypeBean eCardTypeBean = eCardTypeCache.getECardTypeBean(new Integer(eCard.getECardType()));
                if (eCardTypeBean == null)
                    eCard = null;
                else
                    eCard.setECardTypeBean(eCardTypeBean);
                //ECardBatchBean eCardBatchBean = getECardBatchBean(conn, eCard.getECardId(), eCard.getECardType());
                ECardBatchBean eCardBatchBean = eCardBatchCache.getECardBatchBean(
                        new ECardBatchKey(eCard.getECardId(), eCard.getECardType()));
                if (eCardBatchBean == null)
                    eCard = null;
                else
                    eCard.setECardBatchBean(eCardBatchBean);
            }
        } catch (SQLException e) {
            eCard = null;
            log.error("SQL error: ", e);
        } catch (Throwable e) {
            // Unexpected error, catch and log
            eCard = null;
            log.error("internal error", e);
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                log.error("cannot close stmt or resultSet: ", e);
            }
        }
        return eCard;
    }
    
    static ECardTypeBean getECardType(Connection conn, int eCardType) {
        String query = "select DEFAULT_BALANCE, CURRENCY, IS_USEDONCE,"
            + " IS_PREPAID, ALLOW_REFILL, IS_TITLEONLY"
            //+ ", DESCRIPTION, LAST_UPDATED"
            + " from \"ECARD_TYPES\""
            + " where ECARD_TYPE=" + eCardType;
            //+ " for update NOWAIT";  read only
        log.debug("SQL: " + query);
        ECardTypeBean eCardTypeBean = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            eCardTypeBean = new ECardTypeBean();
            eCardTypeBean.setECardType(eCardType);
            /* 
                 private int eCardType;
                 private float defaultbalance;
                 private String currency;
                 private boolean isUsedOnce;
                 private boolean isPrepaid;
                 private boolean allowRefill;
                 //private String description;
                 //private Date lastUpdated;
                 private boolean isTitleOnly;
             */
            int tmp;
            while (rs.next()) {
                eCardTypeBean.setDefaultbalance(rs.getFloat("DEFAULT_BALANCE"));
                eCardTypeBean.setCurrency(rs.getString("CURRENCY"));
                tmp = rs.getInt("IS_USEDONCE");
                if (tmp == 0)
                    eCardTypeBean.setUsedOnce(false);
                else
                    eCardTypeBean.setUsedOnce(true);
                tmp = rs.getInt("IS_PREPAID");
                if (tmp == 0)
                    eCardTypeBean.setPrepaid(false);
                else
                    eCardTypeBean.setPrepaid(true);
                tmp = rs.getInt("ALLOW_REFILL");
                if (tmp == 0)
                    eCardTypeBean.setAllowRefill(false);
                else
                    eCardTypeBean.setAllowRefill(true);
                tmp = rs.getInt("IS_TITLEONLY");
                if (tmp == 0)
                    eCardTypeBean.setTitleOnly(false);
                else
                    eCardTypeBean.setTitleOnly(true);
            }
        } catch (SQLException e) {
            eCardTypeBean = null;
            log.error("SQL error: ", e);
        }  catch (Throwable e) {
            // Unexpected error, catch and log
            eCardTypeBean = null;
            log.error("internal error", e);
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                log.error("cannot close stmt or resultSet: ", e);
            }
        }
        return eCardTypeBean;
    }
    
    static AccountBean getAccount(Connection conn, int accountId) {
        String query = "select ACCOUNT_TYPE, "
            + " BCCUTIL.ConvertTimeStamp(ACTIVATE_DATE), BCCUTIL.ConvertTimeStamp(DEACTIVATE_DATE), "
            + " BCCUTIL.ConvertTimeStamp(REVOKE_DATE), "
            + " BALANCE, BALANCE_AUTHORIZED, CURRENCY, "
            + " BCCUTIL.ConvertTimeStamp(LAST_UPDATED), PIN "
            + " from \"PAS\".\"PAS_BANK_ACCOUNTS\" where ACCOUNT_ID=" + accountId
            + " for update NOWAIT";
        log.debug("SQL: " + query);
        AccountBean account = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            /* 
             private int accountId;
             private String accountType;
             private Date activateDate;
             private Date deactivateDate;
             private Date revokeDate;
             private BigDecimal balance;
             private BigDecimal balanceAuthorized;
             private String currency;
             private Date lastUpdated;
             private int pin;
             */
            long timeStamp;
            if (rs.next()) {
                account = new AccountBean();
                account.setAccountId(accountId);
                account.setAccountType(rs.getString("ACCOUNT_TYPE"));
                timeStamp = rs.getLong(2);
                if (timeStamp > 0)
                    account.setActivateDate(new Date(timeStamp));
                timeStamp = rs.getLong(3);
                if (timeStamp > 0)
                    account.setDeactivateDate(new Date(timeStamp));
                timeStamp = rs.getLong(4);
                if (timeStamp > 0)
                    account.setRevokeDate(new Date(timeStamp));
                account.setBalance(rs.getBigDecimal("BALANCE"));
                account.setBalanceAuthorized(rs.getBigDecimal("BALANCE_AUTHORIZED"));
                account.setCurrency(rs.getString("CURRENCY"));
                timeStamp = rs.getLong(8);
                if (timeStamp > 0)
                    account.setLastUpdated(new Date(timeStamp));
                account.setPin(rs.getInt("PIN"));
            }
        } catch (SQLException e) {
            account = null;
            log.error("SQL error: ", e);
        } catch (Throwable e) {
            // Unexpected error, catch and log
            account = null;
            log.error("internal error", e);
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                log.error("cannot close stmt or resultSet: ", e);
            }
        }
        return account;
    }
    static ECardBatchBean getECardBatchBean(Connection conn, long eCardId, int eCardType) {
        String query = "select END_ECARD_ID, START_ECARD_ID, BCCUTIL.ConvertTimeStamp(CREATE_DATE), "
            + " BCCUTIL.ConvertTimeStamp(PRINT_DATE), BCCUTIL.ConvertTimeStamp(SHIP_DATE), "
            + "BCCUTIL.ConvertTimeStamp(UPLOAD_DATE), BCCUTIL.ConvertTimeStamp(EXPIRE_DATE), "
            + "BCCUTIL.ConvertTimeStamp(REVOKE_DATE), BCCUTIL.ConvertTimeStamp(ACTIVATE_DATE) "
            + "from \"PAS\".\"ECARD_BATCHES\" "
            + "where " + eCardId + " between START_ECARD_ID and END_ECARD_ID and ECARD_TYPE=" + eCardType;
            //+ " for update NOWAIT";  read only
        log.debug("SQL: " + query);
        ECardBatchBean eCardBatch = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            eCardBatch = new ECardBatchBean();
            eCardBatch.setECardType(eCardType);
            /* 
                 private long endECardId;
                 private long startECardId;
                 private Date createDate;
                 private int eCardType;
                 private Date printDate;
                 private Date shipDate;
                 private Date uploadDate;
                 private Date expireDate;
                 private Date revokeDate;
                 private Date activateDate;
             */
            long timeStamp;
            while (rs.next()) {
                eCardBatch.setEndECardId(rs.getLong("END_ECARD_ID"));
                eCardBatch.setStartECardId(rs.getLong("START_ECARD_ID"));
                // what is the column name?
                /*
                eCardBatch.setCreateDate(new Date(rs.getLong("CREATE_DATE")));
                eCardBatch.setPrintDate(new Date(rs.getLong("PRINT_DATE")));
                eCardBatch.setShipDate(new Date(rs.getLong("SHIP_DATE")));
                eCardBatch.setUploadDate(new Date(rs.getLong("UPLOAD_DATE")));
                eCardBatch.setExpireDate(new Date(rs.getLong("EXPIRE_DATE")));
                eCardBatch.setRevokeDate(new Date(rs.getLong("REVOKE_DATE")));
                eCardBatch.setActivateDate(new Date(rs.getLong("ACTIVATE_DATE")));
                */
                timeStamp = rs.getLong(3);
                if (timeStamp > 0)
                    eCardBatch.setCreateDate(new Date(timeStamp));
                timeStamp = rs.getLong(4);
                if (timeStamp > 0)
                    eCardBatch.setPrintDate(new Date(timeStamp));
                timeStamp = rs.getLong(5);
                if (timeStamp > 0)
                    eCardBatch.setShipDate(new Date(timeStamp));
                timeStamp = rs.getLong(6);
                if (timeStamp > 0)
                    eCardBatch.setUploadDate(new Date(timeStamp));
                timeStamp = rs.getLong(7);
                if (timeStamp > 0)
                    eCardBatch.setExpireDate(new Date(timeStamp));
                timeStamp = rs.getLong(8);
                if (timeStamp > 0)
                    eCardBatch.setRevokeDate(new Date(timeStamp));
                timeStamp = rs.getLong(9);
                if (timeStamp > 0)
                    eCardBatch.setActivateDate(new Date(timeStamp));
            }
        } catch (SQLException e) {
            eCardBatch = null;
            log.error("SQL error: ", e);
        }  catch (Throwable e) {
            // Unexpected error, catch and log
            eCardBatch = null;
            log.error("internal error", e);
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                log.error("cannot close stmt or resultSet: ", e);
            }
        }
        return eCardBatch;
    }
    
    static PasTransactionBean getTransactionBean(Connection conn, byte[] id) {
        String transId = UUID.fromByteArray(id).toBase64();
        String query = "select PAYMENT_METHOD_ID, PAYMENT_METHOD, TRANS_TYPE, BCCUTIL.ConvertTimeStamp(TRANS_DATE), "
            + " BCCUTIL.ConvertTimeStamp(EXPIRE_DATE), "
            + " STATUS, AMOUNT_AUTHORIZED, AMOUNT_CAPTURED, AMOUNT_REFUNDED, CURRENCY, REFERENCE_ID, "
            + " PAS_ACTION_ID from \"PAS\".\"PAS_TRANSACTIONS\""
            + "where PAS_TRANS_ID = '" + transId + "'"
            + " for update NOWAIT";
        log.debug("SQL: " + query);
        PasTransactionBean trans = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            pstmt = conn.prepareStatement(query);
            rs = pstmt.executeQuery();
            trans = new PasTransactionBean();
            trans.setTransId(transId);
            /* 
                     private String transId;
                     private String paymentMethodId;
                     private String paymentMethod;
                     private String transType;
                     private Date transDate;
                     private Date expirationDate;
                     private String status;
                     private float amountAuthorized;
                     private float amountCaptured;
                     private float amountRefunded;
                     private String currencyType;
                     private String referenceId;
                     private int actionId;
             */
            long timeStamp;
            while (rs.next()) {
                trans.setPaymentMethodId(rs.getString("PAYMENT_METHOD_ID"));
                trans.setPaymentMethod(rs.getString("PAYMENT_METHOD"));
                trans.setTransType(rs.getString("TRANS_TYPE"));
                // column name?
                //trans.setTransDate(new Date(rs.getLong("TRANS_DATE")));
                timeStamp = rs.getLong(4);
                if (timeStamp > 0)
                    trans.setTransDate(new Date(timeStamp));
                //trans.setExpirationDate(new Date(rs.getLong("EXPIRE_DATE")));
                timeStamp = rs.getLong(5);
                if (timeStamp > 0)
                    trans.setExpirationDate(new Date(timeStamp));
                trans.setStatus(rs.getString("STATUS"));
                trans.setAmountAuthorized(rs.getBigDecimal("AMOUNT_AUTHORIZED"));
                trans.setAmountCaptured(rs.getBigDecimal("AMOUNT_CAPTURED"));
                trans.setAmountRefunded(rs.getBigDecimal("AMOUNT_REFUNDED"));
                trans.setCurrencyType(rs.getString("CURRENCY"));
                trans.setReferenceId(rs.getString("REFERENCE_ID"));
                trans.setActionId(rs.getInt("PAS_ACTION_ID"));
            } 
            } catch (SQLException e) {
                trans = null;
                log.error("SQL error: ", e);
            }  catch (Throwable e) {
                // Unexpected error, catch and log
                trans = null;
                log.error("internal error", e);
            } finally {
                try {
                    if (pstmt != null)
                        pstmt.close();
                    if (rs != null)
                        rs.close();
                } catch (SQLException e) {
                    log.error("cannot close stmt or resultSet: ", e);
                }
            }
            return trans;
    }

    static PasTransactionBean getTransactionBean(Connection conn, String id) {
        String transId = id;
        
        String query = "select PAYMENT_METHOD_ID, PAYMENT_METHOD, TRANS_TYPE, BCCUTIL.ConvertTimeStamp(TRANS_DATE), "
            + " BCCUTIL.ConvertTimeStamp(EXPIRE_DATE), "
            + " STATUS, AMOUNT_AUTHORIZED, AMOUNT_CAPTURED, AMOUNT_REFUNDED, CURRENCY, REFERENCE_ID, "
            + " PAS_ACTION_ID from \"PAS\".\"PAS_TRANSACTIONS\""
            + " where PAS_TRANS_ID = '" + transId + "'"
            + " for update NOWAIT";
        
        log.debug("SQL: " + query);
        PasTransactionBean trans = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            trans = new PasTransactionBean();
            trans.setTransId(transId);
           
            long timeStamp;
            while (rs.next()) {
                trans.setPaymentMethodId(rs.getString("PAYMENT_METHOD_ID"));
                trans.setPaymentMethod(rs.getString("PAYMENT_METHOD"));
                trans.setTransType(rs.getString("TRANS_TYPE"));
                // column name?
                //trans.setTransDate(new Date(rs.getLong("TRANS_DATE")));
                timeStamp = rs.getLong(4);
                if (timeStamp > 0)
                    trans.setTransDate(new Date(timeStamp));
                //trans.setExpirationDate(new Date(rs.getLong("EXPIRE_DATE")));
                timeStamp = rs.getLong(5);
                if (timeStamp > 0)
                    trans.setExpirationDate(new Date(timeStamp));
                trans.setStatus(rs.getString("STATUS"));
                trans.setAmountAuthorized(rs.getBigDecimal("AMOUNT_AUTHORIZED"));
                trans.setAmountCaptured(rs.getBigDecimal("AMOUNT_CAPTURED"));
                trans.setAmountRefunded(rs.getBigDecimal("AMOUNT_REFUNDED"));
                trans.setCurrencyType(rs.getString("CURRENCY"));
                trans.setReferenceId(rs.getString("REFERENCE_ID"));
                trans.setActionId(rs.getInt("PAS_ACTION_ID"));
            } 
            } catch (SQLException e) {
                trans = null;
                log.error("SQL error: ", e);
            }  catch (Throwable e) {
                // Unexpected error, catch and log
                trans = null;
                log.error("internal error", e);
            } finally {
                try {
                    if (stmt != null)
                        stmt.close();
                    if (rs != null)
                        rs.close();
                } catch (SQLException e) {
                    log.error("cannot close stmt or resultSet: ", e);
                }
            }
            return trans;
    }
    
    static boolean releaseAcctExpiredLockedAmount(Connection conn, AccountBean account) {
        String query = "select PAS_TRANS_ID, TRANS_TYPE, BCCUTIL.ConvertTimeStamp(TRANS_DATE), "
            + " BCCUTIL.ConvertTimeStamp(EXPIRE_DATE), "
            + " AMOUNT_AUTHORIZED, AMOUNT_CAPTURED, AMOUNT_REFUNDED, CURRENCY, REFERENCE_ID, "
            + " PAS_ACTION_ID from \"PAS\".\"PAS_TRANSACTIONS\""
            + " where PAYMENT_METHOD = '" + PaymentMethodType.ACCOUNT 
            + "' and PAYMENT_METHOD_ID = '" + account.getAccountId()
            + "' and STATUS = '" + AuthorizationStatusType.Pending + "'"
            + " for update NOWAIT";
        boolean availableIncreased = false;
        log.debug("SQL: " + query);
        Statement stmt = null;
        ResultSet rs = null;
        Date expDate = null;
        Date now = getCurrentDBTime(conn);
        BigDecimal amountAuthorized = null;
        BigDecimal amountCaptured = null;
        String transId = null;
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                expDate = new Date(rs.getLong(4));
                if (expDate.before(now)) {
                    availableIncreased = true;
                    transId = rs.getString("PAS_TRANS_ID");
                    amountAuthorized = rs.getBigDecimal("AMOUNT_AUTHORIZED");
                    amountCaptured = rs.getBigDecimal("AMOUNT_CAPTURED");
                    // adjust balanceAuthorized
                    account.setBalanceAuthorized(account.getBalanceAuthorized().subtract(amountAuthorized).add(amountCaptured));
                    // update authorization status
                    if (! updateTransactionStatus(conn, transId, AuthorizationStatusType.Expired.toString())) {
                        availableIncreased = false;
                        break;  // any update fails, abort 
                    }
                }
            }
            // update balanceAuthorized in db
            if (! updateBalanceAccount(conn, account.getAccountId(), account.getBalance(), account.getBalanceAuthorized())) {
                availableIncreased = false; // any update fails, abort 
            }
                availableIncreased = false;
        } catch (SQLException e) {
            availableIncreased = false;
            log.error("SQL error: ", e);
        }  catch (Throwable e) {
            // Unexpected error, catch and log
            availableIncreased = false;
            log.error("internal error", e);
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                log.error("cannot close stmt or resultSet: ", e);
            }
        }
        return availableIncreased;
    }
    
    static boolean releaseECardExpiredLockedAmount(Connection conn, ECardBean eCard) {
        String query = "select PAS_TRANS_ID, PAYMENT_METHOD_ID, PAYMENT_METHOD, TRANS_TYPE, BCCUTIL.ConvertTimeStamp(TRANS_DATE), "
            + " BCCUTIL.ConvertTimeStamp(EXPIRE_DATE), "
            + " STATUS, AMOUNT_AUTHORIZED, AMOUNT_CAPTURED, AMOUNT_REFUNDED, CURRENCY, REFERENCE_ID, "
            + " PAS_ACTION_ID from \"PAS\".\"PAS_TRANSACTIONS\""
            + " where PAYMENT_METHOD = '" + PaymentMethodType.ECARD
            + "' and PAYMENT_METHOD_ID = '" + eCard.getECardId() 
            + "' and STATUS = '" + AuthorizationStatusType.Pending + "'"
            + " for update NOWAIT";
        boolean availableIncreased = false;
        log.debug("SQL: " + query);
        Statement stmt = null;
        ResultSet rs = null;
        Date expDate = null;
        Date now = getCurrentDBTime(conn);
        BigDecimal amountAuthorized = null;
        BigDecimal amountCaptured = null;
        String transId = null;
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                expDate = new Date(rs.getLong(4));
                if (expDate.before(now)) {
                    availableIncreased = true;
                    transId = rs.getString("PAS_TRANS_ID");
                    amountAuthorized = rs.getBigDecimal("AMOUNT_AUTHORIZED");
                    amountCaptured = rs.getBigDecimal("AMOUNT_CAPTURED");
                    // adjust balanceAuthorized
                    eCard.setBalanceAuthorized(eCard.getBalanceAuthorized().subtract(amountAuthorized).add(amountCaptured));
                    // update authorization status
                    if (! updateTransactionStatus(conn, transId, AuthorizationStatusType.Expired.toString())) {
                        availableIncreased = false;
                        break;  // any update fails, abort 
                    }
                }
            }
            // update balanceAuthorized in db
            if (! updateECardAccount(conn, eCard.getECardId(), eCard.getBalance(), eCard.getBalanceAuthorized())) {
                availableIncreased = false; // any update fails, abort 
            }
                availableIncreased = false;
        } catch (SQLException e) {
            availableIncreased = false;
            log.error("SQL error: ", e);
        }  catch (Throwable e) {
            // Unexpected error, catch and log
            availableIncreased = false;
            log.error("internal error", e);
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                log.error("cannot close stmt or resultSet: ", e);
            }
        }
        return availableIncreased;
    }
    
    /**
     * Get the history of the specified authorization.
     * 
     * @param   conn    the database connection to use
     * @param   id      the authorization id, 16 bytes (128 bits)
     * @param   response    the response contains a list of AuthRecordType, the first element contains 
     *                      the most recent AuthorizationState, followed by a list of actions in descending order 
     *                      and the AuthorizationState before the action.
     * 
     * @return  true if records are retrieved
     *          false if no authorization record is found
     */
    static boolean getAuthorizationHistory(
            Connection conn, byte[] id, 
            GetAuthorizationHistoryResponseType response) {
        boolean ret = true;
        String transId = UUID.fromByteArray(id).toBase64();
        String query = "select PAS_ACTION_ID, BCCUTIL.ConvertTimeStamp(ACTION_DATE), ACTION_TYPE, ACTION_AMOUNT"
            + ", STATUS "
            + " from \"PAS\".\"PAS_TRANSACTION_ACTIONS\""
            + " where PAS_TRANS_ID='" + transId + "' order by ACTION_DATE desc" ;
        String countQuery = "select count(*) from \"PAS\".\"PAS_TRANSACTION_ACTIONS\""
            + " where PAS_TRANS_ID='" + transId + "'";
        // read only, don't need lock
        PasTransactionBean trans = getTransactionBean(conn, id);
        if (trans == null) {
            response.setAuthRecords(new AuthorizationRecordType[0]);
            return false; // no authorization record
        }
        int actionCount = 0;
        AuthorizationRecordType[] authRecords;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            log.debug("SQL: " + countQuery);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(countQuery);
            while (rs.next()) {
                actionCount = rs.getInt(1);
            }
            authRecords = new AuthorizationRecordType[actionCount+1];    // first one will be for
                                                                // the current authorization state
                                                                // with no action associated
            // populate authRecords[0]
            AuthorizationStateType state = new AuthorizationStateType();
            PasFunction.initAuthStateFromTransBean(trans, state);
            AuthorizationRecordType oneAuthRecord = new AuthorizationRecordType();
            oneAuthRecord.setState(state);  // current auth state
            authRecords[0]=oneAuthRecord;

            log.debug("SQL: " + query);
            rs = stmt.executeQuery(query);
            int i=1;
            BigDecimal actionAmount;
            ActionKindType actionKind;
            while (rs.next()) {
                // get one record and populate one authRecord
                authRecords[i]=new AuthorizationRecordType();
                actionKind = ActionKindType.fromString(rs.getString("ACTION_TYPE"));
                actionAmount = rs.getBigDecimal("ACTION_AMOUNT");
                authRecords[i].setAction(new ActionRecordType(
                        rs.getString("PAS_ACTION_ID"),
                        // column name?
                        rs.getLong(2),
                        actionKind,
                        actionAmount.toString()));
                // recreate the state before the action
                authRecords[i].setState(PasFunction.dupAuthorizationState(authRecords[i-1].getState()));
                authRecords[i].getState().setStatus(AuthorizationStatusType.fromString(rs.getString("STATUS")));
                if (actionKind == ActionKindType.Capture || actionKind == ActionKindType.LastCap)
                    authRecords[i].getState().getCapturedAmount().setAmount(
                            new BigDecimal(authRecords[i].getState().getCapturedAmount().getAmount()).subtract(actionAmount).toString());
                if (actionKind == ActionKindType.Refund)
                    authRecords[i].getState().getRefundedAmount().setAmount(
                            new BigDecimal(authRecords[i].getState().getRefundedAmount().getAmount()).subtract(actionAmount).toString());
                i++;
            }
            response.setAuthRecords(authRecords);
        } catch (SQLException e) {
            response.setAuthRecords(new AuthorizationRecordType[0]);
            ret = false;
            log.error("SQL error: ", e);
        }  catch (Throwable e) {
            // Unexpected error, catch and log
            response.setAuthRecords(new AuthorizationRecordType[0]);
            ret = false;
            log.error("internal error", e);
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                log.error("cannot close stmt or resultSet: ", e);
            }
        }
        return ret;
    }

    /**
     * Get the history of the specified account in a specified time period (if provided).
     * 
     * @param   conn    the database connection to use
     * @param   accountId   the account id.
     * @param   paymentType the payment method, a string representation of PaymentMethodType instance.
     * @param   begin   optional, start date (inclusive) used to query the account history. No limit if null.
     * @param   end     optional, end date (inclusive) used to query the account history. No limit if null.
     * @param   response    the response contains the current account balance and a list of TransactionrecordType,
     *                      each entry is a <action, balance> pair. 
     *                      The balance is the balance prior the action was taken, caculated from the balance of
     *                      the previous entry and the action taken.
     * 
     * @return  true if no error happens during the data retirval.
     */
    static boolean getAccountHistory(
            Connection conn, 
            String accountId,
            BigDecimal initBalance,
            String paymentType,
            Date begin, 
            Date end, 
            AccountHistory response) {
        boolean ret = true;
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");   // oracle default date format
        StringBuffer timeRange = new StringBuffer("");
        if (begin != null)
            timeRange.append(" and t1.ACTION_DATE >= " + formatter.format(begin));
        if (end != null)
            timeRange.append(" and t1.ACTION_DATE <= " + formatter.format(end)).append(" ");
        String fromTableAndCondition = 
            " from \"PAS\".\"PAS_TRANSACTION_ACTIONS\" t1, \"PAS\".\"PAS_TRANSACTIONS\" t2"
            + " where t2.PAYMENT_METHOD_ID='" + accountId + "' and "
            + " t2.PAYMENT_METHOD='" + paymentType + "' and "
            + " t2.PAS_TRANS_ID=t1.PAS_TRANS_ID"
            + timeRange.toString();
        String query = "select t1.PAS_ACTION_ID, BCCUTIL.ConvertTimeStamp(t1.ACTION_DATE), t1.ACTION_TYPE, t1.ACTION_AMOUNT"
            + fromTableAndCondition;
        String countQuery = "select count(*) "
            + fromTableAndCondition;
    
        int actionCount = 0;
        TransactionRecordType[] transactionRecords;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            log.debug("SQL: " + countQuery);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(countQuery);
            while (rs.next()) {
                actionCount = rs.getInt(1);
            }
            transactionRecords = new TransactionRecordType[actionCount];
            int i=0;
            BigDecimal prevBalance=initBalance;
            String actionId;
            long actionDate;
            ActionKindType actionKind;
            BigDecimal actionAmount;
            log.debug("SQL: " + query);
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                // get one record and populate one authRecord
                /*
                private java.lang.String actionID;
                private long actionDate;
                private com.broadon.wsapi.pas.ActionKindType actionType;
                private float actionAmount;
                */
                actionId = rs.getString("PAS_ACTION_ID");
//              column name?
                actionDate = rs.getLong(2);
                actionKind = ActionKindType.fromString(rs.getString("ACTION_TYPE"));
                actionAmount = rs.getBigDecimal("ACTION_AMOUNT");
                ActionRecordType oneActionRecord = new ActionRecordType(
                        actionId,
                        actionDate,
                        actionKind,
                        actionAmount.toString());
                TransactionRecordType oneTransactionRecord = new TransactionRecordType();
                oneTransactionRecord.setAction(oneActionRecord);
                if (actionKind == ActionKindType.Capture || actionKind == ActionKindType.LastCap)
                    prevBalance = prevBalance.add(actionAmount);
                if (actionKind == ActionKindType.Refund || actionKind == ActionKindType.Deposit)
                    prevBalance = prevBalance.subtract(actionAmount);
                oneTransactionRecord.setBalance(prevBalance.toString());
                transactionRecords[i]=oneTransactionRecord;
                i++;
            }
            response.setTransactions(transactionRecords);
        } catch (SQLException e) {
            response.setTransactions(new TransactionRecordType[0]);
            ret = false;
            log.error("SQL error: ", e);
        } catch (Throwable e) {
            // Unexpected error, catch and log
            response.setTransactions(new TransactionRecordType[0]);
            ret = false;
            log.error("internal error", e);
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                log.error("cannot close stmt or resultSet: ", e);
            }
        }
        return ret;
    }
            
    static long getNextSeq(Connection conn) {
        String query = "select account_id_seq.nextval from dual";
        long nextSeq = 0;
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                nextSeq = rs.getLong(1);
            }
        } catch (SQLException e) {
            log.error("SQL error: ", e);
        }
        return nextSeq;
    }
    
    static boolean updateECardAccount(Connection conn, int eCardId, BigDecimal balance, BigDecimal locked) {
        String update = "update ECARDS set BALANCE = ?, BALANCE_AUTHORIZED = ?" +
        ", LAST_USED = "
        + " SYS_EXTRACT_UTC(Current_Timestamp) "
        + " where ECARD_ID = ?";
        log.debug("SQL: " + update);
        boolean ret = true;
        PreparedStatement pstmt = null;
        try {
            pstmt = conn.prepareStatement(update);
            pstmt.setBigDecimal(1, balance);
            pstmt.setBigDecimal(2, locked);
            pstmt.setInt(3, eCardId);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            log.error("SQL error: ", e);
            ret = false;
        } catch (Throwable e) {
            // Unexpected error, catch and log
            ret = false;
            log.error("internal error", e);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException e) {
                log.error("cannot close pstmt or resultSet: ", e);
            }
        }
        return ret;
    }
    
    static boolean updateBalanceAccount(Connection conn, int accountId, 
            BigDecimal balance, BigDecimal locked) {
        String update = "update PAS_BANK_ACCOUNTS set BALANCE = ?, BALANCE_AUTHORIZED = ?" +
        ", LAST_UPDATED = "
        + " SYS_EXTRACT_UTC(Current_Timestamp) "
        + " where ACCOUNT_ID = ?";
        log.debug("SQL: " + update);
        boolean ret = true;
        PreparedStatement pstmt = null;
        try {
            pstmt = conn.prepareStatement(update);
            pstmt.setBigDecimal(1, balance);
            pstmt.setBigDecimal(2, locked);
            pstmt.setInt(3, accountId);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            log.error("SQL error: ", e);
            ret = false;
        } catch (Throwable e) {
            // Unexpected error, catch and log
            ret = false;
            log.error("internal error", e);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException e) {
                log.error("cannot close pstmt or resultSet: ", e);
            }
        }
        return ret;
    }
    
    static boolean updateTransactionStatus(
            Connection conn, 
            String transId, 
            String newStatus) {
        String update = "update PAS_TRANSACTIONS set STATUS = '" + newStatus
            + "' where PAS_TRANS_ID = '" + transId + "'";
        log.debug("SQL: " + update);
        boolean ret = true;
        Statement stmt = null;
        try {
            stmt = conn.createStatement();
            stmt.executeUpdate(update);
        } catch (SQLException e) {
            log.error("SQL error: ", e);
            ret = false;
        } catch (Throwable e) {
            // Unexpected error, catch and log
            ret = false;
            log.error("internal error", e);
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException e) {
                log.error("cannot close stmt or resultSet: ", e);
            }
        }
        return ret;
    }
    
    static boolean updateTransactionRecord(
            Connection conn, 
            String transId, 
            String newTransType,
            String newStatus,
            BigDecimal newCaptured,
            BigDecimal newRefunded) {
        String update = "update PAS_TRANSACTIONS set TRANS_TYPE = '" + newTransType 
            + "', STATUS = '" + newStatus
            + "', AMOUNT_CAPTURED = '" + newCaptured.toString() 
            + "', AMOUNT_REFUNDED = '" + newRefunded.toString()
            + "' where PAS_TRANS_ID = '" + transId + "'";
        log.debug("SQL: " + update);
        boolean ret = true;
        Statement stmt = null;
        try {
            stmt = conn.createStatement();
            stmt.executeUpdate(update);
        } catch (SQLException e) {
            log.error("SQL error: ", e);
            ret = false;
        } catch (Throwable e) {
            // Unexpected error, catch and log
            ret = false;
            log.error("internal error", e);
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException e) {
                log.error("cannot close stmt or resultSet: ", e);
            }
        }
        return ret;
    }
    
    static boolean insertPasTransaction(Connection conn, PasTransactionBean trans, int exp) {
        String insert = "insert into \"PAS\".\"PAS_TRANSACTIONS\" "
            + " (PAS_TRANS_ID, PAYMENT_METHOD_ID, PAYMENT_METHOD, TRANS_TYPE, "
            + " TRANS_DATE, EXPIRE_DATE, "
            + " STATUS, AMOUNT_AUTHORIZED, AMOUNT_CAPTURED, AMOUNT_REFUNDED, CURRENCY, REFERENCE_ID) "
            + " values (?, ?, ?, ?, "
            + " SYS_EXTRACT_UTC(Current_Timestamp), "
            + " SYS_EXTRACT_UTC(Current_Timestamp+ INTERVAL '" + exp
            + "' MINUTE), "
            + " ?, ?, ?, ?, ?, ?)";
        log.debug("SQL: " + insert);
        boolean ret = true;
        PreparedStatement pstmt = null;
        try {
            pstmt = conn.prepareStatement(insert);
            pstmt.setString(1, trans.getTransId());
            pstmt.setString(2, trans.getPaymentMethodId());
            pstmt.setString(3, trans.getPaymentMethod());
            pstmt.setString(4, trans.getTransType());
            pstmt.setString(5, trans.getStatus());
            pstmt.setBigDecimal(6, trans.getAmountAuthorized());
            pstmt.setBigDecimal(7, trans.getAmountCaptured());
            pstmt.setBigDecimal(8, trans.getAmountRefunded());
            pstmt.setString(9, trans.getCurrencyType());
            pstmt.setString(10, trans.getReferenceId());
            pstmt.execute();
        } catch (SQLException e) {
            // if (e.getErrorCode() == DUPLICATE_ID(1)) ...
            log.error("SQL error: ", e);
            ret = false;
        } catch (Throwable e) {
            // Unexpected error, catch and log
            ret = false;
            log.error("internal error", e);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException e) {
                log.error("cannot close stmt or resultSet: ", e);
            }
        }
        return ret;
    }
    
    static boolean insertPasAction(Connection conn, PasActionBean action) {
        String insert = "insert into \"PAS\".\"PAS_TRANSACTION_ACTIONS\" "
            + " (PAS_ACTION_ID, PAS_TRANS_ID, ACTION_DATE, "
            + " ACTION_TYPE, ACTION_AMOUNT, STATUS, NOTIFY) "
            + " values (?, ?,"
            + " SYS_EXTRACT_UTC(Current_Timestamp), "
            + " ?, ?, ?, ?)";
        log.debug("SQL: " + insert);
        boolean ret = true;
        PreparedStatement pstmt = null;
        try {
            pstmt = conn.prepareStatement(insert);
            pstmt.setLong(1, action.getActionId());
            pstmt.setString(2, action.getTransId());
            pstmt.setString(3, action.getActionType());
            pstmt.setBigDecimal(4, action.getActionAmount());
            pstmt.setString(5, action.getStatus());
            pstmt.setString(6, action.getNotify());
            pstmt.execute();
        } catch (SQLException e) {
            log.error("SQL error: ", e);
            ret = false;
        } catch (Throwable e) {
            // Unexpected error, catch and log
            ret = false;
            log.error("internal error", e);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException e) {
                log.error("cannot close stmt or resultSet: ", e);
            }
        }
        return ret;
    }
    
    public static void lockProcessLock(Connection conn, String processName) throws SQLException {
        String query = "select * from PAS_PROCESS_LOCKS where PROCESS_NAME = '"
            + processName + "' for update NOWAIT";
        log.debug("SQL: " + query);
        Statement stmt = conn.createStatement();
        stmt.executeQuery(query);
    }
    
    public static void updateProcessLock(Connection conn, String processName, String serverName) throws SQLException {
        String update = "update PAS_PROCESS_LOCKS set server_name = '"
        + serverName + "' , end_date = SYS_EXTRACT_UTC(Current_Timestamp)"
        + " where PROCESS_NAME = '" + processName + "'";
        log.debug("SQL: " + update);
        Statement stmt = conn.createStatement();
        stmt.executeUpdate(update);
    }
    
    public static String[] getPointsApplyTx(Connection conn) {
        String query = "select PAS_TRANS_ID from pas_transaction_actions"
            + " where notify='Y' and action_type='LastCap'";
        String countQuery = "select count(*) from pas_transaction_actions"
            + " where notify='Y' and action_type='LastCap'";
        log.debug("SQL: " + query);
        Statement stmt = null;
        ResultSet rs = null;
        int count=0;
        String[] ret = null;
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(countQuery);
            if (rs.next())
                count = rs.getInt(1);
            ret = new String[count];
            int i=0;
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                ret[i] = rs.getString("PAS_TRANS_ID");
            }
        } catch (SQLException e) {
            log.error("SQL error: ", e);
        } catch (Throwable e) {
            // Unexpected error, catch and log
            log.error("internal error", e);
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException e) {
                log.error("cannot close stmt or resultSet: ", e);
            }
        }
        return ret;
    }
    
    public static void markPointsConfirmed(Connection conn, PointsAppResults paReturn) {
        String update;
        if (paReturn.getAppliedResult().equalsIgnoreCase("SUCCESS")) {
            update = "update pas_transaction_actions"
                + " set notify = null where PAS_TRANS_ID = '"
                + paReturn.getTransId() + "'";
        } else if (paReturn.getAppliedResult().equalsIgnoreCase("ERROR")) {
            update = "update pas_transaction_actions"
                + " set notify = 'E' where PAS_TRANS_ID = '"
                + paReturn.getTransId() + "'";
        } else {
            throw new IllegalArgumentException("invalid return code");
        }
        log.debug("SQL: " + update);
        PreparedStatement pstmt = null;
        int rs = 0;
        try {
            pstmt = conn.prepareStatement(update);
            rs = pstmt.executeUpdate(update);
        } catch (SQLException e) {
            log.error("SQL error: ", e);
        } catch (Throwable e) {
            // Unexpected error, catch and log
            log.error("internal error", e);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
                if (rs == 0)
                    log.error("no pas_action with id: " + paReturn.getTransId());
            } catch (SQLException e) {
                log.error("cannot close stmt or resultSet: ", e);
            }
        }
    }
    
    // utility to creating a transaction record for depositPayment
    public static String createTransactionRecord(
            AbstractResponseIfc response, 
            String paymentMethodId, 
            String paymentMethodType, 
            String transactionKind,
            String authorizationStatus,
            int exp,
            Connection conn) {
        PasTransactionBean trans = new PasTransactionBean();
        UUID uuid = UUID.randomUUID();
        trans.setTransId(uuid.toBase64());    // use base64 string in db
        trans.setPaymentMethodId(paymentMethodId);
        trans.setPaymentMethod(paymentMethodType);
        trans.setTransType(transactionKind);
        trans.setStatus(authorizationStatus);

        if (! PasDBFunction.insertPasTransaction(conn, trans, exp)) {
            PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, null, response);
            return null;
        }
        return trans.getTransId();
    }
    
    // utility for creating a transaction action record for depositPayment
    public static String createActionRecord(
            Connection conn,
            AbstractResponseIfc response, 
            String transId, 
            String actionKind,
            BigDecimal amount, 
            String fromAuthStatus,
            String notify) {
        PasActionBean action = new PasActionBean();
        long actionId = PasDBFunction.getNextSeq(conn);
        action.setActionId(actionId);
        action.setTransId(transId);
        action.setActionType(actionKind);
        action.setActionAmount(amount);
        action.setStatus(fromAuthStatus);
        action.setNotify(notify) ;
        if (! PasDBFunction.insertPasAction(conn, action)) {
            PasFunction.setErrorCode(StatusCode.PAS_INTERNAL_ERROR, null, response);
            return null;
        }
        return new Long(actionId).toString();
    }
    
    // update a specific column in the account atble
    public static boolean updateAccountStatus(Connection conn, AccountBean account, String columnName) {
        String update = "update PAS_BANK_ACCOUNTS set " + columnName + "= ?, "
            + " LAST_UPDATED = SYS_EXTRACT_UTC(Current_Timestamp) where ACCOUNT_ID = "
            + account.getAccountId();
        PreparedStatement pstmt = null;
        String pin = "PIN";
        String revokeDate = "REVOKE_DATE";
        boolean ret = true;
        try {
            pstmt = conn.prepareStatement(update);
            if (columnName.equalsIgnoreCase(revokeDate)) {
                if (account.getRevokeDate() == null)
                    pstmt.setTimestamp(1, null);
                else
                    pstmt.setTimestamp(1, new Timestamp(account.getRevokeDate().getTime()));
            }
            else if (columnName.equalsIgnoreCase(pin))
                pstmt.setInt(1, account.getPin());  
            else
                return false;
            pstmt.executeUpdate();
        } catch (SQLException e) {
            // if (e.getErrorCode() == DUPLICATE_ID(1)) ...
            log.error("SQL error: ", e);
            ret = false;
        } catch (Throwable e) {
            // Unexpected error, catch and log
            log.error("internal error", e);
            ret = false;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException e) {
                log.error("cannot close stmt or resultSet: ", e);
            }
        }
        return ret;
    }
    
    public static AccountBean createBalanceAccount(Connection conn, AccountBean account) {
        String insert = "insert into PAS_BANK_ACCOUNTS"
            + " (ACCOUNT_ID, ACCOUNT_TYPE, ACTIVATE_DATE,"
            + " DEACTIVATE_DATE, REVOKE_DATE, BALANCE,"
            + " BALANCE_AUTHORIZED, CURRENCY, LAST_UPDATED, PIN)"
            + " values(?, ?, SYS_EXTRACT_UTC(Current_Timestamp), ?, ?,"
            + " ?, ?, ?, SYS_EXTRACT_UTC(Current_Timestamp), ?)";
        PreparedStatement pstmt = null;
        AccountBean ret = null;
        try {
            pstmt = conn.prepareStatement(insert);
            pstmt.setInt(1, account.getAccountId());        // ACCOUNT_ID
            pstmt.setString(2, account.getAccountType());   // ACCOUNT_TYPE
            if (account.getActivateDate() == null)          // DEACTIVATE_DATE
                pstmt.setTimestamp(3, null);
            else
                pstmt.setTimestamp(3, new Timestamp(account.getActivateDate().getTime()));
            if (account.getRevokeDate() == null)          // REVOKE_DATE
                pstmt.setTimestamp(4, null);
            else
                pstmt.setTimestamp(4, new Timestamp(account.getRevokeDate().getTime()));
            pstmt.setBigDecimal(5, account.getBalance());    // BALANCE
            pstmt.setBigDecimal(6, account.getBalanceAuthorized()); // BALANCE_AUTHORIZED
            pstmt.setString(7, account.getCurrency());      // CURRENCY
            pstmt.setInt(8, account.getPin());             // PIN
            int inserted = pstmt.executeUpdate();
            if (inserted == 1) {
                Date today = getCurrentDBTime(conn);
                account.setActivateDate(today);
                account.setLastUpdated(today);
                ret = account;
            }
        } catch (SQLException e) {
            // if (e.getErrorCode() == DUPLICATE_ID(1)) ...
            log.error("SQL error: ", e);
        } catch (Throwable e) {
            // Unexpected error, catch and log
            log.error("internal error", e);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException e) {
                log.error("cannot close stmt or resultSet: ", e);
            }
        }
        return ret;
    }
    
    // for stand-alone testing only
    private static ECardBean getECardNoCache(Connection conn, int eCardId) {
        String query = "select ECARD_TYPE, BALANCE,"
            + " BALANCE_AUTHORIZED, IS_USABLE,"
            + " BCCUTIL.ConvertTimeStamp(LAST_USED), BCCUTIL.ConvertTimeStamp(ACTIVATE_DATE), "
            + " BCCUTIL.ConvertTimeStamp(REVOKE_DATE)"
            + " from \"PAS\".\"ECARDS\" where ECARD_ID=" + eCardId
            + " for update NOWAIT";
        log.debug("SQL: " + query);
        ECardBean eCard = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            eCard = new ECardBean();
            eCard.setECardId(eCardId);
            /* 
             private long eCardId;
             private int eCardType;
             private float balance;
             private float balanceAuthorized; 
             private boolean isUsable;
             private Date lastUsed;
             //private Date expireDate;
             private Date activateDate;
             private Date revokeDate;
             //private String activateCode;
             */
            long timeStamp;
            while (rs.next()) {
                eCard.setECardType(rs.getInt("ECARD_TYPE"));
                eCard.setBalance(rs.getBigDecimal("BALANCE"));
                eCard.setBalanceAuthorized(rs.getBigDecimal("BALANCE_AUTHORIZED"));
                int usable = rs.getInt("IS_USABLE");
                if (usable == 0)
                    eCard.setUsable(false);
                else
                    eCard.setUsable(true);
                // what is the column name to use?
                //eCard.setActivateDate(rs.getTimestamp("ACTIVATE_DATE"));
                //eCard.setRevokeDate(rs.getTimestamp("REVOKE_DATE"));
                //eCard.setLastUsed(rs.getTimestamp("LAST_USED"));
                timeStamp = rs.getLong(5);
                if (timeStamp > 0)
                    eCard.setLastUsed(new Date(timeStamp));
                timeStamp = rs.getLong(6);
                if (timeStamp > 0)
                    eCard.setActivateDate(new Date(timeStamp));
                timeStamp = rs.getLong(7);
                if (timeStamp > 0)
                    eCard.setRevokeDate(new Date(timeStamp));
            }
            // the associated eCardType and eCardbatch record must exist
            // to make the eCard record a consistent data in the system
            ECardTypeBean eCardTypeBean = getECardType(conn, eCard.getECardType());
            //ECardTypeBean eCardTypeBean = eCardTypeCache.getECardTypeBean(new Integer(eCard.getECardType()));
            if (eCardTypeBean == null)
                eCard = null;
            else
                eCard.setECardTypeBean(eCardTypeBean);
            ECardBatchBean eCardBatchBean = getECardBatchBean(conn, eCard.getECardId(), eCard.getECardType());
            //ECardBatchBean eCardBatchBean = eCardBatchCache.getECardBatchBean(
            //            new ECardBatchKey(eCard.getECardId(), eCard.getECardType()));
            if (eCardBatchBean == null)
                eCard = null;
            else
                eCard.setECardBatchBean(eCardBatchBean);
        } catch (SQLException e) {
            eCard = null;
            log.error("SQL error: ", e);
        } catch (Throwable e) {
            // Unexpected error, catch and log
            eCard = null;
            log.error("internal error", e);
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                log.error("cannot close stmt or resultSet: ", e);
            }
        }
        return eCard;
    }
    /**
     * @param args
     */
    public static void main(String[] args) {
        log.debug("PasDBFunction...");
        try {
            String url = "jdbc:oracle:thin:@//db1-vip.bcc.lab1.routefree.com:1521/BCCDB";
            //String url = "jdbc:oracle:thin:@//db2-vip.bcc.lab1.routefree.com:1521/BCCDB";
            String login = "pas";
            String passwd = "pas";
            Class.forName("oracle.jdbc.driver.OracleDriver");
            Connection conn = DriverManager.getConnection(url, login, passwd);
            /*
            eCard = getECardNoCache(conn, 1);
            printECard(eCard);
            //eCard = getECardNoCache(conn, 2);
            //printECard(eCard);
            
            PasTransactionBean trans = new PasTransactionBean();
            trans.setTransId(UUID.randomUUID().toBase64());
            trans.setPaymentMethodId("1");
            trans.setPaymentMethod("TEST");
            trans.setTransType("ECard");
            Date today = new Date();
            long creationTime =  today.getTime();
            long expTime = creationTime+2*1000*60*60*24;
            
            trans.setTransDate(new Date(creationTime));
            trans.setExpirationDate(new Date(expTime));
            trans.setStatus("TEST");
            trans.setAmountAuthorized(new BigDecimal(10));
            trans.setAmountCaptured(new BigDecimal(0));
            trans.setAmountRefunded(new BigDecimal(0));
            trans.setCurrencyType("EUNIT");
            trans.setReferenceId("reference");
            PasDBFunction.insertPasTransaction(conn, trans, 2);
            log.debug("nextSeq= " + getNextSeq(conn));
            log.debug(new Date());
            
            String transId= trans.getTransId();

            PasTransactionBean trans2 = getTransactionBean(conn, transId);
            
            printTransaction(trans2);

            conn.commit();
            */
            /*
            String q1 = "select SYS_EXTRACT_UTC(Current_Timestamp) from dual";
            String q2 = "SELECT BCCUTIL.ConvertTimeStamp(Current_Timestamp) from dual";
            String q3 = "SELECT BCCUTIL.ConvertTimeStamp(SYS_EXTRACT_UTC(Current_Timestamp)) from dual";
            Statement s1 = conn.createStatement();
            ResultSet rs1 = s1.executeQuery(q1);
            ResultSet rs2 = s1.executeQuery(q2);
            ResultSet rs3 = s1.executeQuery(q3);
            long t1=0; long t2=0; long t3=0;
            Date d1=null; Date d2=null; Date d3=null;
            if (rs1.next())
                t1=rs1.getLong(1);
            d1 = new Date(t1);
            if (rs2.next())
                t2=rs2.getLong(1);
            d2 = new Date(t2);
            if (rs3.next())
                t3=rs3.getLong(1);
            d3 = new Date(t3);
            System.out.println(d1);
            System.out.println(d2);
            System.out.println(d3);
            */
            String server = "me";
            String process = "PAS POINTS APPLIED PROCESS";
            lockProcessLock(conn, process);
            updateProcessLock(conn, process, server);
            Properties p = System.getProperties();
            Enumeration e = p.keys();
            while(e.hasMoreElements()) {
                String key = (String) e.nextElement();
                System.out.println(key+ " = " + p.getProperty(key));
            }
            System.out.println(System.getProperty("PWD"));
            PointsAppResults paReturn = new PointsAppResults();
            paReturn.setAppliedResult("ERROR");
            paReturn.setTransId("2kyJ8HUsdHE7dTqdF11QAA==");
            markPointsConfirmed(conn, paReturn);
        } catch(SQLException sqlE) {
            sqlE.printStackTrace();
            log.debug("error code: " + sqlE.getErrorCode());
            log.debug("state: " + sqlE.getSQLState());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    static void printECard(ECardBean eCard){
        log.debug(new Boolean(eCard.isAllowRefill()).toString());
        log.debug(new Boolean(eCard.isPrepaid()).toString());
        log.debug(new Boolean(eCard.isTitleOnly()).toString());
        log.debug(new Boolean(eCard.isUsable()).toString());
        log.debug(new Boolean(eCard.isUsedOnce()).toString());
        log.debug(eCard.getActivateDate());
        log.debug(eCard.getBalance().toString());
        log.debug(eCard.getBalanceAuthorized().toString());
        log.debug(eCard.getCurrency());
        log.debug(new Float(eCard.getECardId()).toString());
        log.debug(new Float(eCard.getECardType()).toString());
        log.debug(eCard.getLastUsed());
        log.debug(eCard.getRevokeDate());
        log.debug(eCard.getExpireDate());
    }
    
    static void printTransaction(PasTransactionBean tran) {
        log.debug(tran.getTransId());
        log.debug(tran.getPaymentMethodId());
        log.debug(tran.getCurrencyType());
        log.debug(tran.getExpirationDate());
        log.debug(tran.getTransDate());
    }
}

package com.broadon.exception;

/**
 * The <code>InvalidRequestException</code> class is thrown when the
 * body of a request contains invalid or inconsistent information.
 *
 * @version	$Revision: 1.1 $
 */
public class InvalidRequestException
    extends BroadOnException
{
    /**
     * Constructs a InvalidRequestException instance.
     */
    public InvalidRequestException()
    {
	super();
    }

    /**
     * Constructs a InvalidRequestException instance.
     *
     * @param	message			the exception message
     */
    public InvalidRequestException(String message)
    {
	super(message);
    }

    /**
     * Constructs a InvalidRequestException instance.
     *
     * @param	message			the exception message
     * @param	throwable		the nested exception
     */
    public InvalidRequestException(String message, Throwable throwable)
    {
	super(message, throwable);
    }
}

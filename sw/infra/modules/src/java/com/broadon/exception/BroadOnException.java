package com.broadon.exception;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * The <code>BroadOnException</code> class is the top level of BroadOn
 * exception hierarchy.
 *
 * @version	$Revision: 1.6 $
 */
public class BroadOnException
    extends Exception
{
    /**
     * Constructs a BroadOnException instance.
     */
    public BroadOnException()
    {
        super();
    }

    /**
     * Constructs a BroadOnException instance.
     *
     * @param	message			the exception message
     */
    public BroadOnException(String message)
    {
        super(message);
    }

    /**
     * Constructs a BroadOnException instance, keeping the original
     * stack trace rather than the site where this exception is 
     * constructed, since by the nature of exception handling this 
     * stack-frame is part of the original one.
     *
     * @param	message			the exception message
     * @param	throwable		the nested exception
     */
    public BroadOnException(String message, Throwable throwable)
    {
        super(message + " [" + throwable.getMessage() + "]", throwable);
        setStackTrace(throwable.getStackTrace());
    }


    /**
     * Generates the stack trace as a string given an exception.
     *
     * @return	A string of stack trace.
     */
    public String generateStackTrace()
    {
        StringWriter writer = new StringWriter();

        try
        {
            printStackTrace(new PrintWriter(writer));
            return writer.toString();
        }
        finally
        {
            try
            {
                writer.close();
            }
            catch (IOException ie)
            {
                /*
                 * Ignore error.
                 */
            }
        }
    }
}

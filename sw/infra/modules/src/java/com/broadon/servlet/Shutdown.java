package com.broadon.servlet;

import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.*;


/**
 * Perform a graceful shutdown of the webapps -- by first making sure
 * the load balancer is notified and no new connection is directed to
 * this host.
 */
public class Shutdown extends HttpServlet implements ServletConstants
{
    ServletContext ctx;

    public void init(ServletConfig config) {
	ctx = config.getServletContext();
    }

    public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
    {
	ContextStatus status = (ContextStatus) ctx.getAttribute(MONITOR_STATUS_KEY);
	int opState = status.getOperationStatus();

	if (opState == status.OS_NORMAL) {
	    opState = status.OS_TERMINATING;
	    status.setOperationStatus(opState);
	}

	res.setStatus(900 + opState);
    }
}
			      

package com.broadon.servlet;

import javax.servlet.ServletContext;

import com.broadon.db.AuditLog;
import com.broadon.util.Queue;

/**
 * The <code>AuditLogAgent</code> class manages the audit log Queue.
 *
 * @version	$Revision: 1.3 $
 */
public class AuditLogAgent
    implements ServletConstants
{
    private static final Queue		queue;

    static
    {
	queue = new Queue();
    }

    /**
     * Returns the audit log Queue. The servletContext would be useful
     * if adjusting the queue size is desireable, for example.
     *
     * @param	servletContext		the context of the calling servlet
     *
     * @return	The audit log Queue.
     */
    public static Queue getQueue(ServletContext servletContext)
    {
	/*
	 * Make sure the queue is ready.
	 */
	queue.start();
	return queue;
    }

    /**
     * Files the audit log instance into the Queue for creation.
     *
     * @param	auditLog		the audit log instance
     */
    public static void createLog(AuditLog auditLog)
    {
	queue.enqueue(auditLog);
    }
}

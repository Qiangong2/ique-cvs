package com.broadon.servlet;

/**
 * This <code>ServletConstants</code> interface defines the constants
 * shared by all servlets.
 *
 * @version	$Revision: 1.8 $
 */
public interface ServletConstants
{
    /**
     * The root directory of this servlet server.
     */
    String	SERVLET_BASE		= "catalina.base";

    /**
     * The JNDI search string for the BB server configuration file.
     */
    String	CONFIG_FILE_KEY		= "java:/comp/env/BBconfig";

    /**
     * The key to locate the data source attribute from the servlet context.
     */
    String	DATA_SOURCE_KEY		= "javax.sql.DataSource";

    /**
     * The key to locate the name mapping attribute from the servlet context.
     */
    String	NAME_MAP_KEY		= "com.broadon.NameMap";

    /**
     * The key to locate the BB server configuration attribute from the
     * servlet context.
     */
    String	PROPERTY_KEY		= "com.broadon.Properties";

    /**
     * The key to locate the reverse name mapping attribute from the
     * servlet context.
     */
    String	REVERSE_NAME_MAP_KEY	= "com.broadon.ReverseNameMap";

    /**
     * The key to locate the regional center ID attribute from the
     * servlet context.
     */
    String	REGIONAL_CENTER_KEY	= "com.broadon.RegionalCtr";

    /**
     * The key to locate the HSM connector attribute from the servlet
     * context. 
     */
    String	HSM_CONNECTOR_KEY	= "com.broadon.HSMConnector";

    /**
     * The key for locate the health monitor status.
     */
    String	MONITOR_STATUS_KEY	= "com.broadon.HealthStatus";
    
    /**
     * Identify the rights of the client that sent in a request
     */
    String      CLIENT_CREDENTIAL       = "com.broadon.ClientCredential";
}

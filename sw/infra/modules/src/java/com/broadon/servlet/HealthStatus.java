package com.broadon.servlet;

import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 * Servlet that returns the cached health status of an webapp.
 */
public class HealthStatus extends HttpServlet implements ServletConstants
{
    ServletContext ctx;

    public void init(ServletConfig config) {
	ctx = config.getServletContext();
    }

    public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
    {
	ContextStatus status = (ContextStatus) ctx.getAttribute(MONITOR_STATUS_KEY);
	switch (status.nextAction()) {
        case ContextStatus.ACT_DO_NOTHING:
            res.setStatus(res.SC_OK);
            break;
            
        case ContextStatus.ACT_CHECK:
            RequestDispatcher disp = ctx.getNamedDispatcher("monitor");
            disp.forward(req, res); 
            break;
            
        case ContextStatus.ACT_ERROR:
        default:
            res.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            break;
        }
    }
}

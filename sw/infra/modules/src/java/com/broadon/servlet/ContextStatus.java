package com.broadon.servlet;

import javax.servlet.*;

/**
 * Context attributes for sharing the health monitoring states and
 * shutdown sequence.
 */
public class ContextStatus implements ServletContextListener, ServletConstants
{
    int pollCount;		// number of times the load balancer
				// poll the health status before the
				// last full health status check.
    // number of time we should return the cached status before doing
    // a full health status check.
    static final int POLL_THRESHOLD = 5;

    
    int operationStatus;	// states of the servlet context --
				// see state constants below:

    public static final int OS_NORMAL = 0;	// normal operation
    public static final int OS_TERMINATING = 1;	// should tell load balancer
						// to direct new requests
						// somewhere else
    public static final int OS_LB_NOTIFIED = 2;	// load balancer notified,
						// ready to shutdown.

    
    public static final int ACT_DO_NOTHING = 0; // Do quick return without any more tests.
    public static final int ACT_CHECK = 1;      // perform full self test
    public static final int ACT_ERROR = 2;      // system shutting down, should return error.
    
    public ContextStatus() {
	pollCount = POLL_THRESHOLD;
	operationStatus = OS_NORMAL;
    }

    /**
     * Reset the load-balancer poll counter.  This is called by the
     * health monitor when the webapp's health is confirmed good.
     * @param ok <code>true</code> if the health is good.
     */
    public void setHealth(boolean ok) {
	pollCount = (ok ? 0 : POLL_THRESHOLD);
    }

    /**
     * Return the shutdown operation status.
     */
    public int getOperationStatus() {
	return operationStatus;
    }

    /**
     * Set the shutdown operation status.
     */
    public synchronized void setOperationStatus(int status) {
	if (status > operationStatus)
	    operationStatus = status;
    }
    
    public int nextAction()
    {
        // this routine has been optimized to avoid unnecessary synchronization.
        // This is based on the assumption that the state variable
        // operationStatus is always increasing, and that pollCount is a
        // primitive type so the pre-increment operation is always atomic.
        
        if (operationStatus == OS_NORMAL) {
            if (++pollCount < POLL_THRESHOLD)
                return ACT_DO_NOTHING;
            else
                return ACT_CHECK;
        } else {
            setOperationStatus(OS_LB_NOTIFIED);
            return ACT_ERROR;
        }
    }

    public void contextInitialized(ServletContextEvent event) {
	ServletContext ctx = event.getServletContext();
	ctx.setAttribute(MONITOR_STATUS_KEY, this);
    }

    public void contextDestroyed(ServletContextEvent event) {
	ServletContext ctx = event.getServletContext();
	ctx.removeAttribute(MONITOR_STATUS_KEY);
    }
}

package com.broadon.servlet;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.sql.DataSource;

import com.broadon.db.OracleDataSource;

/**
 * This <code>OracleDSListener</code> class isolates the Oracle dependencies.
 */
public class OracleDSListener
    extends DataSourceListener
{
    private static Map		dataSources;

    static
    {
	/*
	 * The data sources will be shared on a per user basis.
	 */
	dataSources = new HashMap();
    }

    /**
     * Creates the data source with the given user name and password.
     *
     * @param	user			the user name for the data source
     * @param	password		the password for the data source
     * @param	properties		the configuration properties
     */
    protected synchronized DataSource createDataSource(String user,
					  String password,
					  Properties properties)
	throws SQLException
    {
        String dbURL = properties.getProperty(super.DATABASE_URL);
        boolean noScheduler = (properties.getProperty(super.DATA_SOURCE_NO_SCHEDULER) != null);
        return createDataSource(user, password, dbURL, noScheduler);
    }

    /**
     * Creates the data source with the given user name and password.
     *
     * @param   user            the user name for the data source
     * @param   password        the password for the data source
     * @param   dbURL           the database URL for the data source
     * @param   noScheduler     true to disable DataSourceScheduler
     */
    protected synchronized DataSource createDataSource(String user,
                      String password, String dbURL, boolean noScheduler)
    throws SQLException
    {
        DataSource  dataSource;

        String key = user + dbURL;

        dataSource = (DataSource)dataSources.get(key);
        if (dataSource == null) {
            dataSource =
                OracleDataSource.createDataSource(user, password,
                        dbURL, key, noScheduler);
            dataSources.put(key, dataSource);
        }
        return dataSource;
    }
}

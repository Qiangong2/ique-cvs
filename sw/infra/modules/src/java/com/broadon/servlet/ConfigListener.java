package com.broadon.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * This <code>ConfigListener</code> class provides the mechanism to
 * load the configuration properties used by the servlets.
 */
public class ConfigListener
    implements ServletConstants, ServletContextListener
{
    /**
     * Initializes the configuration properties related attributes for
     * the servlet whose context is provided by the given event.
     *
     * @param	event			the event for the starting servlet
     */
    public void contextInitialized(ServletContextEvent event)
    {
	ServletContext	servletContext = event.getServletContext();

	try
	{
	    /*
	     * Load configuration properties.
	     */
	    Context	context = new InitialContext();
	    String	configFile = (String)context.lookup(CONFIG_FILE_KEY);

	    /*
	     * Get the BB server configuration file name.
	     */
	    String	fullPath = System.getProperty(SERVLET_BASE) +
				   File.separator +
				   configFile;
	    File file = new File(fullPath);
	    if (!file.isFile()) {
            // File not found, try under the servlet context path
	    	fullPath = servletContext.getRealPath(configFile);
	    }
	    
	    /*
	     * Read the BB server configuration file.
	     *
	     * Reading the file here allows the servlet to always
	     * have the latest version after reloading.
	     */
	    Properties	properties = new Properties();
	    FileInputStream in = new FileInputStream(fullPath);

	    properties.load(in);
	    in.close();
	    /*
	     * Assign the configuration properties to this servlet.
	     */
	    servletContext.setAttribute(PROPERTY_KEY, properties);
	}
	catch (Throwable t)
	{
	    t.printStackTrace(System.out);
	}
    }

    /**
     * Cleans up the configuration properties related attributes for the
     * servlet whose context is provided by the given event.
     *
     * @param	event			the event for the terminating servlet
     */
    public void contextDestroyed(ServletContextEvent event)
    {
	ServletContext	context = event.getServletContext();

	/*
	 * Cleanup.
	 */
	context.removeAttribute(PROPERTY_KEY);
    }
}

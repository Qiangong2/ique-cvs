package com.broadon.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * This <code>NameMapListener</code> class provides the mechanism to
 * initialize the name mapping table used by the servlets.
 */
public class NameMapListener
    implements ServletConstants, ServletContextListener
{
    private static final String		NAME_MAP_FILE_KEY;

    static
    {
	/*
	 * Initialize constants.
	 */
	NAME_MAP_FILE_KEY = "java:/comp/env/BBnameMap";
    }

    /**
     * Initializes the name mapping table related attributes for
     * the servlet whose context is provided by the given event.
     *
     * @param	event			the event for the starting servlet
     */
    public void contextInitialized(ServletContextEvent event)
    {
	try
	{
	    /*
	     * Load name mapping information.
	     */
	    Context	context = new InitialContext();
	    String	configFile = (String)context.lookup(NAME_MAP_FILE_KEY);
	    /*
	     * Get the name mapping information file name.
	     */
	    String	filePath = System.getProperty(SERVLET_BASE) +
				   File.separator +
				   configFile;
	    /*
	     * Read the name mapping information file.
	     *
	     * Reading the file here allows the servlet to always
	     * have the latest version.
	     */
	    Properties	properties = new Properties();

	    properties.load(new FileInputStream(filePath));
	    /*
	     * Process the name mapping informaton.
	     */
	    Set		keySet = properties.keySet();

	    if (keySet != null)
	    {
		Iterator	iterator = keySet.iterator();
		int		size = keySet.size();
		Map		nameMap = new HashMap(size * 3 / 2);
		Map		rNameMap = new HashMap(size * 3 / 2);

		while (iterator.hasNext())
		{
		    String	name = (String)iterator.next();
		    String	mappedName = (String)properties.get(name);

		    nameMap.put(name, mappedName);
		    rNameMap.put(mappedName, name);
		}
		/*
		 * Assign the name mapping information to this servlet.
		 */
		ServletContext	servletContext = event.getServletContext();

		servletContext.setAttribute(NAME_MAP_KEY, nameMap);
		servletContext.setAttribute(REVERSE_NAME_MAP_KEY, rNameMap);
	    }
	}
	catch (Throwable t)
	{
	    t.printStackTrace(System.out);
	}
    }

    /**
     * Cleans up the name mapping table related attributes for the
     * servlet whose context is provided by the given event.
     *
     * @param	event			the event for the terminating servlet
     */
    public void contextDestroyed(ServletContextEvent event)
    {
	ServletContext	context = event.getServletContext();

	/*
	 * Cleanup.
	 */
	context.removeAttribute(NAME_MAP_KEY);
	context.removeAttribute(REVERSE_NAME_MAP_KEY);
    }
}

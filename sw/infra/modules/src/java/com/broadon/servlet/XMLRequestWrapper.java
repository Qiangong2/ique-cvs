package com.broadon.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import com.broadon.xml.XMLMap;

/**
 * The <c>XMLRequestWrapper</c> class provides the mechanism to convert
 * a HTTP Post request from XML format to map format. It also acts as a
 * wrapper to present the map as if it is part of the parameters from
 * the HttpServletRequest.
 *<p>
 * For example, one can share the code of the Post request and a Get
 * request by doing the following, providing that the Post request
 * comes in XML format:
 *<p>
 *<pre>
 *	public void doPost(HttpServletRequest req, HttpServletResoonse res)
 *	    throws ServletException, IOException
 *	{
 *	    doGet(new XMLRequestWrapper(req), res);
 *	}
 *</pre>
 *
 * @version	$Revision: 1.7 $
 */
public class XMLRequestWrapper
    extends HttpServletRequestWrapper
{
    private HashMap			map;

    /**
     * Constructs a XMLRequestWrapper instance.
     *
     * @param	request			the HttpServletRequest
     */
    public XMLRequestWrapper(HttpServletRequest request)
    {
	super(request);

	try
	{
	    InputStream	inStream = request.getInputStream();
	    XMLMap	xmlMap = new XMLMap();

	    this.map = new HashMap();
	    xmlMap.process(inStream, map);
	}
	catch (IOException ie)
	{
	    ie.printStackTrace();
	    this.map = new HashMap();
	}
    }

    /**
     * Returns the parameter value identified by the given name.
     * It first searches the map obtained from the XML request,
     * before handing the call over to the super class.
     *
     * @param	name			the parameter name
     *
     * @return	The parameter value.
     */
    public String getParameter(String name)
    {
	String	value = (String)map.get(name);

	if (value == null)
	{
	    value = (String)super.getParameter(name);
	}
	return value;
    }

    /**
     * Returns the parameter map.
     */
    public Map getParameterMap()
    {
	HashMap	parentMap = (HashMap)super.getParameterMap();

	if (parentMap == null)
	    return (Map)((HashMap)map).clone();

	HashMap	clone = (HashMap)map.clone();

	clone.putAll(parentMap);
	return clone;
    }
}

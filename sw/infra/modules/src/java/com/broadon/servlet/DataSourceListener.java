package com.broadon.servlet;

import java.sql.SQLException;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.sql.DataSource;

/**
 * This <code>DataSourceListener</code> class provides the mechanism to
 * initialize the data source used by the servlets.
 */
public abstract class DataSourceListener
    implements ServletConstants, ServletContextListener
{
    protected static final String		DATA_SOURCE_NEEDED;
    protected static final String		DATA_SOURCE_USER;
    protected static final String		DATA_SOURCE_PASSWORD;
    protected static final String		DATA_SOURCE_NO_SCHEDULER;
    protected static final String       DATABASE_URL;
    protected static final String       DEFAULT_USER;
    protected static final String       DEFAULT_PASSWORD;
    protected static final String       ALT_DB_COUNT;
    protected static final String       ALT_DATABASE_URL;
    protected static final String       ALT_DB_USER;
    protected static final String       ALT_DB_PASSWORD;

    static
    {
	/*
	 * Initialize constants.
	 */
	DATA_SOURCE_NEEDED	= "data-source-needed";
	DATA_SOURCE_USER	= "data-source-user";
	DATA_SOURCE_PASSWORD= "data-source-password";
        DATA_SOURCE_NO_SCHEDULER = "data-source-no-scheduler";
    DATABASE_URL        = "DB_URL";
    DEFAULT_USER        = "default_servlet_db_user";
    DEFAULT_PASSWORD    = "default_servlet_db_password";
    ALT_DB_COUNT        = "alt_db_count";
    ALT_DATABASE_URL    = "alt_db_url";
    ALT_DB_USER         = "alt_db_user";
    ALT_DB_PASSWORD     = "alt_db_password";
    }

    /**
     * Initializes the data source related attributes for the servlet
     * whose context is provided by the given event.
     *
     * @param	event			the event for the starting servlet
     */
    public void contextInitialized(ServletContextEvent event)
    {
	ServletContext	context = event.getServletContext();
	String		dataSourceNeeded;

	/*
	 * See if data source is needed for this servlet.
	 */
	dataSourceNeeded = context.getInitParameter(DATA_SOURCE_NEEDED);
	if (dataSourceNeeded != null)
	{
	    /*
	     * Get the configuration properties.
	     */
	    Properties	properties;

	    properties = (Properties)context.getAttribute(PROPERTY_KEY);
	    /*
	     * Initialize the data source.
	     * If there is no user name and password specified for this
	     * servlet, it will use the default one.
	     */
	     try
	     {
		Object	dataSource;

		dataSource = initDataSource(
				context.getInitParameter(DATA_SOURCE_USER),
				context.getInitParameter(DATA_SOURCE_PASSWORD),
				properties);
		context.setAttribute(DATA_SOURCE_KEY, dataSource);
        
        /*
         * Create alternative data sources if needed.
         */
        String altDBCountStr = properties.getProperty(ALT_DB_COUNT);
        if (altDBCountStr != null) {
            try {
                int altDBCount = Integer.parseInt(altDBCountStr);
                String altDBURL;
                String altUser;
                String altPassword;
                boolean noScheduler;
                for (int i = 1; i <= altDBCount; i++) {
                    altDBURL = properties.getProperty(ALT_DATABASE_URL + "_" + i);
                    altUser = properties.getProperty(ALT_DB_USER + "_" + i);
                    altPassword = properties.getProperty(ALT_DB_PASSWORD + "_" + i);
                    noScheduler = (properties.getProperty(DATA_SOURCE_NO_SCHEDULER) != null);
                    dataSource = createDataSource(altUser, altPassword, altDBURL, noScheduler);
                    context.setAttribute(DATA_SOURCE_KEY + "_" + i, dataSource);
                }
            } catch (NumberFormatException nex) {
                nex.printStackTrace(System.out);
            }
        }
        
	    }
	    catch (SQLException se)
	    {
		se.printStackTrace(System.out);
	    }
	}
    }

    /**
     * Cleans up the data source related attributes for the servlet
     * whose context is provided by the given event.
     *
     * @param	event			the event for the terminating servlet
     */
    public void contextDestroyed(ServletContextEvent event)
    {
	ServletContext	context = event.getServletContext();

	/*
	 * Cleanup.
	 */
	destroyDataSource(context.getAttribute(DATA_SOURCE_KEY));
	context.removeAttribute(DATA_SOURCE_KEY);
    }

    /**
     * Creates the data source with the given user name and password.
     *<p>
     * This method must be implemented by the subclasses, each uses
     * a different brand of database.
     *
     * @param	user			the user name for the data source
     * @param	password		the password for the data source
     * @param	properties		the configuration properties
     */
    protected abstract DataSource createDataSource(String user,
						   String password,
						   Properties properties)
	throws SQLException;

    /**
     * Creates the data source with the given user name and password.
     *<p>
     * This method must be implemented by the subclasses, each uses
     * a different brand of database.
     *
     * @param   user            the user name for the data source
     * @param   password        the password for the data source
     * @param   dbURL           the database URL for the data source
     */
    protected abstract DataSource createDataSource(String user,
                           String password, String dbURL, boolean noScheduler)
    throws SQLException;

    /**
     * Initializes the data source. If user name and password are null,
     * it uses the default data source; otherwise, it creates a separate
     * one.
     *
     * @param	user			the user name for the data soruce
     * @param	password		the password for the data source
     * @param	properties		the configuration properties
     *
     * @return	The data source used by this servlet.
     */
    private Object initDataSource(String user,
				  String password,
				  Properties properties)
	throws SQLException
    {
	return (user == null) ?
	    createDataSource(properties.getProperty(DEFAULT_USER),
			     properties.getProperty(DEFAULT_PASSWORD),
			     properties) :
	    createDataSource(user, password, properties);
    }

    /**
     * Destroys the data source.
     *
     * @param	dataSource		the data source to be destroyed
     */
    private void destroyDataSource(Object dataSource)
    {
	/*
	 * Nothing to clean up.
	 */
    }
}

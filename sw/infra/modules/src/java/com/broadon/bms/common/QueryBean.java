package com.broadon.bms.common;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Array;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import oracle.xml.sql.dml.OracleXMLSave;
import oracle.xml.sql.query.OracleXMLQuery;

import org.apache.log4j.Logger;
import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * JSP/Data Access Bean for querying and management
 * It has following Bean properties:
 * <ul>
 * <li> mXML, String <br> array of search results in xml.
 * <li> mHTML, String <br> array of search results in html.
 * <li> mParam, String <br> array of required parameters.
 * <li> mCount, int <br> search result count.
 * <li> mSort, int <br> sort parameter.
 * </ul>
 * @author Vaibhav Bhargava
 */

public class QueryBean
    extends PageBean
{
    private static Logger logger = Logger.getLogger(QueryBean.class.getName());
    private static final String	GET_NEXT_ID_CALL = "{? = call BBXML.GetNextID(?,?)}";
    private static final String TABLE_PK_CALL = "{? = call BBXML.getpkcol(?)}";
    private static final String SET_NLS_DATE_FORMAT = "ALTER SESSION SET nls_date_format = 'YYYY.MM.DD HH24:MI:SS'";

    private String[] mXML = null;
    private String[] mHTML = null;
    private String[] mParam = null;
    private int[] mCount = null;
    private String mSort = null;
    private String mResult = null;

    private boolean mSetKey = true;

    public static final String ID = "qbean";
    
    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    public QueryBean()
    {
        super(50);
    }

    public QueryBean(int pageSize)
    {
        super(pageSize);
    }

    // for all search
    public QueryBean(int pageSize, String page)
    {
        super(pageSize);
        super.setPageNo(Integer.parseInt(page));
    }

    /**
     * Bean Property: mXML
     */
    public String[] getXMLResultArray() { return mXML; }
    public void setXMLResultArray(String[] xmlResult) { mXML = xmlResult; }

    /**
     * Bean Property: mHTML
     */
    public String[] getHTMLResultArray() { return mHTML; }
    public void setHTMLResultArray(String[] htmlResult) { mHTML = htmlResult; }

    /**
     * Bean Property: mParam
     */
    public String[] getParamArray() { return mParam; }
    public void setParamArray(String[] param) { mParam = param; }

    /**
     * Bean Property: mCount
     */
    public int[] getCountArray() { return mCount; }
    public void setCountArray(int[] count) { mCount = count; }

    /**
     * Bean Property: mSort
     */
    public String getSort() { return mSort; }
    public void setSort(String sort) { mSort = sort; }

    /**
     * Bean Property: mResult
     */
    public String getResult() { return mResult; }
    public void setResult(String result) { mResult = result; }

    /**
     * Bean Property: mSetKey
     */
    public boolean getSetKey() { return mSetKey; }
    public void setSetKey(boolean setkey) { mSetKey = setkey; }

    /**
     * Queries the records identified by the given SQL statement.
     * This is a generic query, and the returning format is XML.
     *
     * @param	sql			SQL statement
     * @param	conn			DB connection handle
     *
     * @return	The XML document that represent the qualified
     *		records, or null if none found.
     *
     */
     public String query(String sql, Connection conn) 
     {
	return query(sql, -1, -1, conn);
     }

    /**
     * Queries the records identified by the given SQL statement
     * based on numrows and skiprows.
     * 
     * This is a generic query, and the returning format is XML.
     *
     * @param	sql			SQL statement
     * @param   numrows			# of rows to include in the result
     *					-1 defaults to all
     * @param   skiprows			# of rows to skip in the result
     *					-1 defaults to none
     * @param	conn			DB connection handle
     *
     * @return	The XML document that represent the qualified
     *		records, or null if none found.
     *
     */
     public String query(String sql, int numrows, int skiprows, Connection conn)
     {
        OracleXMLQuery qry = new OracleXMLQuery(conn, sql);

        if (numrows >= 0) 
            qry.setMaxRows(numrows);
	if (skiprows >= 0) 
            qry.setSkipRows(skiprows);
	qry.setDateFormat("yyyy.MM.dd HH:mm:ss");

	String xml = qry.getXMLString();
        qry.close();

	return xml;
    }

    /**
     * Inserts a record in the database based on the given XML Document.
     *
     * @param	xml			the XML document that contains 
     *					the information to be inserted
     * @param	tableName		Name of the table in the database
     * @param	conn			DB connection handle
     *
     * @return	# of rows successfully inserted
     *
     * @throws	SQLException
     * @throws	IOException
     * @throws	SAXException
     */
    public int insert(String xml, String tableName, Connection conn)
	throws SQLException, IOException, SAXException
    {
	return tableXDML(xml, tableName, "INS", conn);
    }

    /**
     * Updates a record in the database based on the given XML Document.
     *
     * @param	xml			the XML document that contains
     *					the information to be updated
     * @param	tableName		Name of the table in the database
     * @param	conn			DB connection handle
     *
     * @return	# of rows successfully updated
     *
     * @throws	SQLException
     * @throws	IOException
     * @throws	SAXException
     */
    public int update(String xml, String tableName, Connection conn)
	throws SQLException, IOException, SAXException
    {
	return tableXDML(xml, tableName, "UPD", conn);
    }

    /**
     * Inserts/Updates records in the database based on the given XML Document.
     *
     * @param	xml			the XML document that contains
     *					the information to be inserted or updated
     * @param	tableName		Name of the table in the database
     * @param	conn			DB connection handle
     *
     * @return	# of rows successfully inserted/updated
     *
     * @throws	SQLException
     * @throws	IOException
     * @throws	SAXException
     */
    public int insertupdate(String xml, String tableName, Connection conn)
	throws SQLException, IOException, SAXException
    {
	int rows = 0;
	rows = update(xml, tableName, conn);
	if (rows == 0)
	    rows = insert(xml, tableName, conn);
	return rows;
    }

    /**
     * Deletes a record in the database based on the given XML Document.
     *
     * @param	xml			the XML document that contains
     *					the information to be deleted
     * @param	tableName		Name of the table in the database
     * @param	conn			DB connection handle
     *
     * @return	# of rows successfully deleted
     *
     * @throws	SQLException
     * @throws	IOException
     * @throws	SAXException
     */
    public int delete(String xml, String tableName, Connection conn)
	throws SQLException, IOException, SAXException
    {
	return tableXDML(xml, tableName, "DEL", conn);
    }

    /**
     * Deletes and then Inserts records in the database based on the given XML Document.
     *
     * @param	xml			the XML document that contains
     *					the information to be deleted then inserted
     * @param	tableName		Name of the table in the database
     * @param	conn			DB connection handle
     *
     * @return	# of rows successfully deleted/inserted
     *
     * @throws	SQLException
     * @throws	IOException
     * @throws	SAXException
     */
    public int deleteinsert(String xml, String tableName, Connection conn)
	throws SQLException, IOException, SAXException
    {
	int rows = 0;
	rows = delete(xml, tableName, conn);
        rows = insert(xml, tableName, conn);
	return rows;
    }

    /**
     * Inserts/Updates/Deletes a record in the database based on the given XML Document
     * and the DML Type.
     *
     * @param	xml			the XML document that contains
     *					the information to be deleted
     * @param	tableName		Name of the table in the database
     * @param	dmlType			"INS", "UPD" or "DEL"
     * @param	conn			DB connection handle
     *
     * @return	# of rows successfully modified
     *
     * @throws	SQLException
     * @throws	IOException
     * @throws	SAXException
     */
    private int tableXDML(String xml, String tableName, String dmlType, Connection conn)
	throws SQLException, IOException, SAXException
    {
        OracleXMLSave sav = new OracleXMLSave(conn, tableName);
        int rows=0;
	
        if (isNullXML(xml)) return 0;

        String [] keyColNames = getPKcol(tableName, conn);

        try 
        {
  	    if (keyColNames != null && keyColNames.length > 0) 
            {
	        sav.setDateFormat("yyyy.MM.dd HH:mm:ss");
                
                if (mSetKey)
  	            sav.setKeyColumnList(keyColNames);

 	        if (dmlType.equals("INS")) 
                {
                    ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());
                    InputStream inStream = bais;
                    InputSource is = new InputSource(inStream);

                    DOMParser parser = new DOMParser();
                    parser.parse(is);

                    Document doc = parser.getDocument();
		    NodeList nlr = doc.getElementsByTagName("ROW");

                    if (nlr.getLength() > 0) { 
		        Node nr = nlr.item(0);
                        NodeList nlrc = nr.getChildNodes();
                        String[] updateCol = new String[nlrc.getLength()];
                        Node nrc;

		        for (int i=0; i < nlrc.getLength(); i++) 
                        {
		            nrc = nlrc.item(i);
		            if (!(nrc.getNodeName().equals("ROWSET")) && !(nrc.getNodeName().equals("ROW"))) {
			        updateCol[i] = nrc.getNodeName();
                            }
		        }
		        sav.setUpdateColumnList(updateCol);
		        rows = sav.insertXML(xml);
	            }
                }
	        if (dmlType.equals("UPD"))
		    rows = sav.updateXML(xml);
	
	        if (dmlType.equals("DEL"))
		    rows = sav.deleteXML(xml);
	    }
	} finally {
            mSetKey = true;
	    sav.close();
	}
        return rows;

    }

    /**
     * Obtains the primary key fields for a given table in the database.
     *
     * @param	tableName		Name of the table in the database
     * @param	conn			DB connection handle
     *
     * @return	string array of primary keys
     *
     * @throws	SQLException
     */
    private String[] getPKcol(String tableName, Connection conn)
	throws SQLException
    {
        CallableStatement cs = null;
        String [] ret = null;

        try 
        { 
  	    cs = conn.prepareCall(TABLE_PK_CALL);

  	    cs.registerOutParameter(1, Types.ARRAY, "STRING_LIST");    
  	    cs.setString(2, tableName);
	    cs.executeUpdate();
	
    	    Array a = cs.getArray(1);

     	    if (a != null)
	        ret = (String [])a.getArray();
        } finally {
            if (cs!=null)
                cs.close();
	}
        return ret;
    }   

    /**
     * Verifies if the given xml string contains any valid data.
     *
     * @param	xml			the XML string
     *
     * @return	true = xml string is empty, false = xml string contains valid data
     *
     */
    private boolean isNullXML(String xml)
    {
	if (xml.length() <= 255) 
        {
	    if ((xml.length() == 0) ||
	       (xml.equals("<ROWSET/>")) ||
	       (xml.equals("<ROWSET></ROWSET>")) ||
	       (xml.equals("<ROW/>")) ||
	       (xml.equals("<ROW></ROW>")) ||
	       (xml.equals("<ROWSET><ROW></ROW></ROWSET>"))) 
	        return true;
        }
        return false;  
    }

    /**
     * Gets the next id to be added into the table.
     *
     * @param	column			Name of the column (id field) in the table.
     * @param	tableName		Name of the table in the database
     * @param	conn			DB connection handle
     *
     * @return	next available id
     *
     * @throws	SQLException
     */
    public long getNextID(String column, String tableName, Connection conn)
	throws SQLException
    {
	CallableStatement call = null;
	long ret = -1;

	try
	{
	    call = conn.prepareCall(GET_NEXT_ID_CALL);
	    call.registerOutParameter(1, Types.NUMERIC);
	    call.setString(2, tableName);
	    call.setString(3, column);
	    call.executeQuery();

            ret = call.getLong(1);

	} finally {
	    /*
	     * Close the statement after execution.
	     */
            if (call!=null)
                call.close();
	}
        return ret;
    }

    /**
     * Counts the number of records identified by the given SQL statement.
     *
     * @param	sql			the SQL statement that counts
     * @param	conn			DB connection handle
     *
     * @return  # of records.
     *
     * @throws	SQLException
     * @throws	IOException
     */
    public int count(String sql, Connection conn)
	throws SQLException, IOException
    {
        PreparedStatement statement = null;
        int ret=0;
	try
	{
            setDateFormat(conn);

   	    statement = conn.prepareStatement(sql);

	    ResultSet resultSet = statement.executeQuery();

	    if (resultSet == null || !resultSet.next())
	    {
		/*
		 * No record.
		 */
		ret = 0;
	    } else 
	        ret = resultSet.getInt(1);
	}
	finally
	{
	    /*
	     * Close the statment after execution.
	     *
	     * The corresponding ResultSet will also be
	     * closed internally.
	     */
            if (statement!=null)
                statement.close();
	}
        return ret;
    }
    /**
     * Sets date format to 'yyyy.MM.dd HH:mm:ss'
     *
     * @param	conn			DB connection handle
     *
     * @throws	SQLException
     * @throws	IOException
     */
    public void setDateFormat(Connection conn)
	throws SQLException, IOException
    {
        PreparedStatement statement = null;
        ResultSet rs = null;
	try
	{
   	    statement = conn.prepareStatement(SET_NLS_DATE_FORMAT);
	    rs = statement.executeQuery();
	}
	finally
	{
            if (rs!=null)
                rs.close();
            if (statement!=null)
                statement.close();
	}
    }
}

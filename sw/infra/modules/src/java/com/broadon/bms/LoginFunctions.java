/*
 * (C) 2006, Broadon Communications Corp.,
 * $Id: LoginFunctions.java,v 1.1 2006/08/10 10:33:13 vaibhav Exp $
 */
package com.broadon.bms;

import java.sql.Connection;

import javax.servlet.*;
import javax.servlet.http.*;

import com.broadon.bms.common.AdminServlet;
import com.broadon.bms.common.QueryBean;
import com.broadon.servlet.ServletConstants;

public class LoginFunctions extends AdminServlet implements ServletConstants
{
    public String login(ServletContext context,
            HttpServletRequest request,
            HttpServletResponse response, 
            QueryBean qb, 
            Connection conn,
            String[] param) 
        throws Exception 
    {       
        String result = "3";
        HttpSession session = request.getSession(false);
        if (session == null)
            return result;
           
        String email = request.getParameter("email");
        String pwd = request.getParameter("pwd");
        
        if (email!=null && !email.equals("") &&
                pwd!=null && !pwd.equals("")) {
            
            int ret = 3;            
            ret = HelperDML.processLogin(conn, session, email, pwd, 
                    SES_ROLE, SES_FULLNAME, SES_USER_ID, SES_ROLE_NAME, SES_LAST_LOGON);
            
            if (ret == 0) {
                String locale = getLocale(request);
                if (session.getAttribute(SES_LOCALE)!=null && !session.getAttribute(SES_LOCALE).toString().equals(""))
                    locale = session.getAttribute(SES_LOCALE).toString();
                else if (locale!=null && locale.toLowerCase().startsWith("ja"))
                    locale = "ja_JP";
                else
                    locale = "en_US";
                
                session.setAttribute(SES_USER, email);
                session.setAttribute(SES_LOCALE, locale);
                
                result = null;
                
            } else                
                result = "FAIL" + String.valueOf(ret);            
        
        } else           
            result = "FAIL" + "Invalid or missing values to process login";
        
        return result;
    } 

    public String logout(ServletContext context,
            HttpServletRequest request,
            HttpServletResponse response, 
            QueryBean qb, 
            Connection conn,
            String[] param) 
        throws Exception 
    {
        HttpSession session = request.getSession(false);
        if (session != null) {            
            session.removeAttribute(SES_USER);
            session.removeAttribute(SES_USER_ID);        
            session.removeAttribute(SES_LOCALE);
            session.removeAttribute(SES_LOCATION);
            session.removeAttribute(SES_ROLE);
            session.removeAttribute(SES_ROLE_NUM);
            session.removeAttribute(SES_ROLE_NAME);
            session.removeAttribute(SES_FULLNAME);
        }
        
        return null;
    }

}
package com.broadon.bms;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Properties;
import java.util.TimeZone;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import com.broadon.bms.common.QueryBean;
import com.broadon.servlet.ServletConstants;
import com.broadon.wsapi.ecs.AccountPaymentType;
import com.broadon.wsapi.ecs.DeleteETicketsRequestType;
import com.broadon.wsapi.ecs.DeleteETicketsResponseType;
import com.broadon.wsapi.ecs.ECommercePortType;
import com.broadon.wsapi.ecs.ECommerceService;
import com.broadon.wsapi.ecs.ECommerceServiceLocator;
import com.broadon.wsapi.ecs.ETicketType;
import com.broadon.wsapi.ecs.LimitType;
import com.broadon.wsapi.ecs.ListETicketsRequestType;
import com.broadon.wsapi.ecs.ListETicketsResponseType;
import com.broadon.wsapi.ecs.ListTransactionsRequestType;
import com.broadon.wsapi.ecs.ListTransactionsResponseType;
import com.broadon.wsapi.ecs.MoneyType;
import com.broadon.wsapi.ecs.PricingType;
import com.broadon.wsapi.ecs.RatingType;
import com.broadon.wsapi.ecs.TitleType;
import com.broadon.wsapi.ecs.TransactionType;
import com.broadon.wsapi.ecs.TransferPointsRequestType;
import com.broadon.wsapi.ecs.TransferPointsResponseType;
import com.broadon.wsapi.pas.BalanceAccountType;
import com.broadon.wsapi.pas.PaymentAuthorizationPortType;
import com.broadon.wsapi.pas.PaymentAuthorizationService;
import com.broadon.wsapi.pas.PaymentAuthorizationServiceLocator;
import com.broadon.wsapi.pas.ResetAccountPINRequestType;
import com.broadon.wsapi.pas.ResetAccountPINResponseType;
import com.broadon.wsapi.pcis.PrepaidCardGenerationPortType;
import com.broadon.wsapi.pcis.PrepaidCardGenerationRequestType;
import com.broadon.wsapi.pcis.PrepaidCardGenerationResponseType;
import com.broadon.wsapi.pcis.PrepaidCardGenerationService;
import com.broadon.wsapi.pcis.PrepaidCardGenerationServiceLocator;

public class HelperFunctions implements ServletConstants
{
    private static Logger logger = Logger.getLogger(HelperFunctions.class.getName());
    /** Handle to the various services */
    private static ECommercePortType ecsPort;
    private static PrepaidCardGenerationPortType pcisPort;
    private static PaymentAuthorizationPortType pasPort;
    
    /** Various URL */
    private static URL ecsUrl;
    private static URL pcisUrl;
    private static URL pasUrl;
    
    private static final String SES_PC_GENERATED_DATA = "pc_data";
    
    private static final String CFG_ECS_URL = "ECS_URL";
    private static final String DEF_ECS_URL = "http://ecs:17105/ecs/services/ECommerceSOAP";
    private static final String CFG_PCIS_URL = "PCIS_URL";
    private static final String DEF_PCIS_URL = "http://pcis:17106/pcis/services/PrepaidCardGenerationSOAP";
    private static final String CFG_PAS_URL = "PAS_URL";
    private static final String DEF_PAS_URL = "http://pas:17103/pas/services/PaymentAuthorizationSOAP";
    
    public static String approvePointsChanges(ServletContext context,
            HttpServletRequest request,
            HttpServletResponse response, 
            QueryBean qb, 
            Connection conn,
            String[] param) 
        throws Exception 
    {        
        if (param != null) {
            String countryId = param[0];
            
            if (countryId != null && !countryId.equals("")) {
                try {
                    HelperDML.pushPointsToCAS(conn, countryId);
                    return null;
                } catch (IOException ie) {
                    return "FAIL" + ie.getMessage();
                }
            } 
        }
        
        return "FAIL Country ID not found";            
    }
    
    public static String approveTitleChanges(ServletContext context,
            HttpServletRequest request,
            HttpServletResponse response, 
            QueryBean qb, 
            Connection conn,
            String[] param) 
        throws Exception 
    {        
        if (param != null) {
            String titleId = param[0];
            
            if (titleId != null && !titleId.equals("")) {
                try {
                    HelperDML.pushTitleToCAS(conn, titleId);
                    return null;
                } catch (IOException ie) {
                    return "FAIL" + ie.getMessage();
                }
            } 
        }
        
        return "FAIL Title ID not found";            
    }
    
    public static String getTransactionHistory(ServletContext context,
                      HttpServletRequest request,
                      HttpServletResponse response, 
                      QueryBean qb, 
                      Connection conn,
                      String[] param) 
        throws Exception 
    {
        String xmlStr = null;
        
        if (param[0]!=null && !param[0].equals("")) {
            
            ListTransactionsRequestType ecsRequest = new ListTransactionsRequestType();
            initECSRequest(context, ecsRequest);
            
            AccountPaymentType acctType = new AccountPaymentType();
            acctType.setAccountNumber(param[0]);
            acctType.setPin(HelperDML.getAccountPin(conn, param[0]));
            ecsRequest.setAccount(acctType);
            
            if (param[1]!=null && !param[1].equals("")) {
                Date beginDate = getDate(param[1]);
                if (beginDate != null)
                    ecsRequest.setBegin(beginDate);
            }
            if (param[2]!=null && !param[2].equals("")) {
                Date endDate = getDate(param[2]);
                if (endDate != null)
                    ecsRequest.setEnd(endDate);
            }           
                    
            ListTransactionsResponseType ecsResponse = ecsPort.listTransactions(ecsRequest);
            if (ecsResponse.getErrorCode() != 0)
                throw new IOException("ECS listTransactions error - " + ecsResponse.getErrorMessage());
            else {
                int end_balance = ecsResponse.getBalance() != null ? 
                        Integer.parseInt(ecsResponse.getBalance().getAmount()) : -1;
                TransactionType tType[] = ecsResponse.getTransactions();
                
                if (tType!=null && tType.length > 0) {
                    xmlStr = "<ROWSET>";
                    int num = 0;
                    for (int i = 0; i < tType.length; i++) {
                        if ("SUCCESS".equals(tType[i].getStatus())) {
                            String productCode = tType[i].getProductCode();
                            if (productCode == null) productCode = ""; 
                            String titleName = "";
                            String ratingValue = "";
                            String platform = "";
                            TitleType title = tType[i].getTitle();
                            RatingType rating = tType[i].getRating();
                            PricingType pricing = tType[i].getTitlePricing();
                            if (title != null) {
                                titleName = title.getTitleName();
                                platform = title.getPlatform();
                            }
                            if (rating != null) {
                                ratingValue = rating.getRating();
                            }
                            String productType = "";
                            String limitTime = "";
                            if (pricing != null) {
                                productType = pricing.getPricingCategory().getValue();
                                LimitType limits[] = pricing.getLimits();
                                if (limits != null && limits.length > 0) {
                                    limitTime = String.valueOf(limits[0].getLimits());
                                }
                            }
                            
                            xmlStr += "<ROW num=\"" + (num+1) + "\">" +
                                "<NO>" + (num+1) + "</NO>" + 
                                "<TRANSACTION_ID>" + tType[i].getTransactionId() + "</TRANSACTION_ID>" +
                                "<TRANSACTION_DATE>" + formatDate(tType[i].getDate()) + "</TRANSACTION_DATE>" +
                                "<TRANSACTION_TYPE>" + tType[i].getType() + "</TRANSACTION_TYPE>" +
                                "<PRODUCT_CODE>" + productCode + "</PRODUCT_CODE>" +
                                "<TITLE_NAME>" + titleName + "</TITLE_NAME>" +
                                "<PLATFORM>" + platform + "</PLATFORM>" +
                                "<RATING_VALUE>" + ratingValue + "</RATING_VALUE>" +
                                "<LICENSE_TYPE>" + productType + "</LICENSE_TYPE>" +
                                "<LIMITS>" + limitTime + "</LIMITS>" +
                                "<CREDIT_DEBIT>" + tType[i].getDebitCredit() + "</CREDIT_DEBIT>" +
                                "<TOTAL_AMOUNT>" + tType[i].getTotalAmount() + "</TOTAL_AMOUNT>" +
                                "<BALANCE>" + tType[i].getBalance() + "</BALANCE>" +
                                "<START_BALANCE>" + tType[0].getBalance() + 
                                " " + tType[0].getCurrency() + "</START_BALANCE>" +
                                "<END_BALANCE>" + String.valueOf(end_balance) + 
                                " " + ecsResponse.getBalance().getCurrency() + "</END_BALANCE>";
                            
                            xmlStr += "</ROW>";
                            num++;
                        }
                    }
                    
                    xmlStr += "</ROWSET>";
                }
            }
        }
        
        if (xmlStr == null)
            xmlStr = "<ROWSET></ROWSET>";
        
        System.out.println("getTransactionHistory XML: " + xmlStr);
        return xmlStr;
    }
        
    public static String getETickets(ServletContext context,
                    HttpServletRequest request,
                    HttpServletResponse response,
                    QueryBean qb, 
                    Connection conn,
                    String[] param) 
        throws Exception 
    {
        String xmlStr = null;
        
        if (param[0]!=null && !param[0].equals("")) {
            
            ListETicketsRequestType ecsRequest = new ListETicketsRequestType();
            initECSRequest(context, ecsRequest);
            ecsRequest.setDeviceId(Long.parseLong(param[0]));
                                  
            ListETicketsResponseType ecsResponse = ecsPort.listETickets(ecsRequest);
            if (ecsResponse.getErrorCode() != 0)
                throw new IOException("ECS listETickets error - " + ecsResponse.getErrorMessage());
            else {
                ETicketType eTickets[] = ecsResponse.getTickets();
                if (eTickets != null && eTickets.length > 0) {
                    xmlStr = "<ROWSET>";
                    for (int i = 0; i < eTickets.length; i++) {
                        String titleInfo = HelperDML.getETicketsTitleDetails(conn, param[0], eTickets[i].getTitleId());
                        LimitType limits[] = eTickets[i].getLimits();
                        
                        if (limits != null && limits.length > 0) {
                            for (int j = 0; j < limits.length; j++) {                          
                                xmlStr += "<ROW num=\"" + (i+1) + "\">" +
                                    "<NO>" + (i+1) + "</NO>" + 
                                    titleInfo +
                                    "<HEX_TITLE_ID>" + eTickets[i].getTitleId() + "</HEX_TITLE_ID>" +
                                    "<CREATE_DATE>" + formatDate(eTickets[i].getCreateDate()) + "</CREATE_DATE>" +
                                    "<REVOKE_DATE>" + formatDate(eTickets[i].getRevokeDate()) + "</REVOKE_DATE>" +
                                    "<TICKET_ID>" + eTickets[i].getTicketId() + "</TICKET_ID>" +
                                    "<LICENSE_TYPE>" + eTickets[i].getLicenseType() + "</LICENSE_TYPE>" +
                                    "<LIMITS>" + String.valueOf(limits[j].getLimits()) + "</LIMITS>" +
                                    "<LIMIT_KIND>" + limits[j].getLimitKind() + "</LIMIT_KIND>";
                                xmlStr += "</ROW>";
                            }
                        } else {
                            xmlStr += "<ROW num=\"" + (i+1) + "\">" +
                                "<NO>" + (i+1) + "</NO>" + 
                                titleInfo +
                                "<HEX_TITLE_ID>" + eTickets[i].getTitleId() + "</HEX_TITLE_ID>" +
                                "<CREATE_DATE>" + formatDate(eTickets[i].getCreateDate()) + "</CREATE_DATE>" +
                                "<REVOKE_DATE>" + formatDate(eTickets[i].getRevokeDate()) + "</REVOKE_DATE>" +
                                "<TICKET_ID>" + eTickets[i].getTicketId() + "</TICKET_ID>" +
                                "<LICENSE_TYPE>" + eTickets[i].getLicenseType() + "</LICENSE_TYPE>" +
                                "<LIMITS></LIMITS>" +
                                "<LIMIT_KIND></LIMIT_KIND>";
                            xmlStr += "</ROW>";
                        }
                    }
                    xmlStr += "</ROWSET>";
                }            
            }
        }
        
        if (xmlStr == null)
            xmlStr = "<ROWSET></ROWSET>";
        
        return xmlStr;
    }
    
    public static String deleteETickets(ServletContext context,
                    HttpServletRequest request,
                    HttpServletResponse response, 
                    QueryBean qb, 
                    Connection conn,
                    String[] param) 
        throws Exception 
    {
        String device_id = request.getParameter("did");
        String ticket_id = request.getParameter("tid");
        
        if (device_id!=null && !device_id.equals("") &&
                ticket_id!=null && !ticket_id.equals("")) {
                        
            DeleteETicketsRequestType ecsRequest = new DeleteETicketsRequestType();
            initECSRequest(context, ecsRequest);
            
            ecsRequest.setDeviceId(Long.parseLong(device_id));
            
            long eTickets[] = new long[1];
            eTickets[0] = Long.parseLong(ticket_id);
            ecsRequest.setTickets(eTickets);
                
            DeleteETicketsResponseType ecsResponse = ecsPort.deleteETickets(ecsRequest);
            if (ecsResponse.getErrorCode() != 0)
                throw new IOException("ECS deleteETickets error - " + ecsResponse.getErrorMessage());                   
        } 
        
        return "";       
    }
    
    public static String insertOperationGroup(ServletContext context,
            HttpServletRequest request,
            HttpServletResponse response, 
            QueryBean qb, 
            Connection conn,
            String[] param) 
    throws Exception 
    {
        String group_name = request.getParameter("group_name");
        String role_id = request.getParameter("role_id");
        String is_global = request.getParameter("global");
        String countries = request.getParameter("countries");
        String operators = request.getParameter("users");
        
        if (group_name!=null && !group_name.equals("") &&
                role_id!=null && !role_id.equals("")) {
            
            try {
                if (HelperDML.createOperationGroup(conn, group_name, role_id,
                        is_global, countries, operators))
                    return null;
            } catch (IOException ie) {
                return "FAIL" + ie.getMessage();            
            }                   
        } else
            return "FAIL2";
        
        return "FAIL3";       
    }

    public static String updateOperationGroup(ServletContext context,
            HttpServletRequest request,
            HttpServletResponse response, 
            QueryBean qb, 
            Connection conn,
            String[] param) 
    throws Exception 
    {
        String group_id = request.getParameter("id");
        String group_name = request.getParameter("group_name");
        String is_global = request.getParameter("global");
        String countries = request.getParameter("countries");
        String operators = request.getParameter("users");
        
        if (group_id!=null && !group_id.equals("") &&
                group_name!=null && !group_name.equals("")) {            
            try {
                if (HelperDML.updateOperationGroup(conn, group_id, group_name,
                        is_global, countries, operators))
                    return null;
            } catch (IOException ie) {
                return "FAIL" + ie.getMessage();            
            }                   
        } else
            return "FAIL2";
        
        return "FAIL3";       
    }

    
    public static String transferPoints(ServletContext context,
            HttpServletRequest request,
            HttpServletResponse response,
            QueryBean qb, 
            Connection conn,
            String[] param) 
    throws Exception 
    {
        if (param != null) {
            String accountId = param[0];
            String creditDebit = param[1];
            String point = param[2];
            
            logger.debug(creditDebit + " " + accountId + " " + point + " points");
            if (accountId!=null && !accountId.equals("") &&
                    creditDebit!=null && !creditDebit.equals("") &&
                    point!=null && !point.equals("")) {
                
                TransferPointsRequestType ecsRequest = new TransferPointsRequestType();
                initECSRequest(context, ecsRequest);
                
                AccountPaymentType customerServiceAccount = new AccountPaymentType("888", "888");
                AccountPaymentType account = new AccountPaymentType(accountId, accountId);
                ecsRequest.setPoints(new MoneyType(point, "POINTS"));
                if (creditDebit.equals("CREDIT")) {
                    ecsRequest.setSourceAccount(customerServiceAccount);
                    ecsRequest.setTargetAccount(account);
                } else {
                    ecsRequest.setSourceAccount(account);
                    ecsRequest.setTargetAccount(customerServiceAccount);
                }
                
                TransferPointsResponseType ecsResponse = ecsPort.transferPoints(ecsRequest);
                if (ecsResponse.getErrorCode() != 0)
                    return "FAIL " + (ecsResponse.getErrorMessage() != null ? 
                            ecsResponse.getErrorMessage() : "") + "(" + 
                            String.valueOf(ecsResponse.getErrorCode()) + ")";
                return "";
            } 
        }
        
        return "FAIL Not enough informaction to proceed.";       
    }

    public static String resetAccountPin(ServletContext context,
                HttpServletRequest request,
                HttpServletResponse response, 
                QueryBean qb, 
                Connection conn,
                String[] param) 
        throws Exception 
    {        
        if (param != null) {
            String accountId = param[0];
            
            if (accountId != null && !accountId.equals("")) {
                ResetAccountPINRequestType pasRequest = new  ResetAccountPINRequestType();
                initPASRequest(context, pasRequest);
                
                pasRequest.setBalanceAccount(new BalanceAccountType(accountId, accountId));
                
                ResetAccountPINResponseType pasResponse = pasPort.resetAccountPIN(pasRequest);
                if (pasResponse.getErrorCode() != 0)
                    return "FAIL " + (pasResponse.getErrorMessage() != null ? 
                            pasResponse.getErrorMessage() : "") + "(" + 
                            String.valueOf(pasResponse.getErrorCode()) + ")";
                else
                    return null;
            } 
        }
        
        return "FAIL Account ID not found";            
    }

    public static String generatePrepaidCards(ServletContext context,
            HttpServletRequest request,
            HttpServletResponse response, 
            QueryBean qb, 
            Connection conn,
            String[] param) 
    throws Exception 
    {
        String ecard_type = request.getParameter("id");
        String batch_size = request.getParameter("size");
        
        String attr1 = request.getParameter("attr1");
        String attr2 = request.getParameter("attr2");
        String attr3 = request.getParameter("attr3");
        
        String opid = "12345";
        String pwd = "admin";
        
        if (ecard_type!=null && !ecard_type.equals("") &&
                batch_size!=null && !batch_size.equals("")) {
            
            PrepaidCardGenerationRequestType pcisRequest = new PrepaidCardGenerationRequestType();
            initPCISRequest(context, pcisRequest);
            
            pcisRequest.setOperatorId(Integer.parseInt(opid));
            pcisRequest.setPassword(pwd);
            pcisRequest.setECardType(Integer.parseInt(ecard_type));
            pcisRequest.setQuantity(Integer.parseInt(batch_size));
            
            if (attr1!=null && !attr1.equals(""))
                pcisRequest.setReferenceId1(attr1);
            
            if (attr2!=null && !attr2.equals(""))
                pcisRequest.setReferenceId2(attr2);
            
            if (attr3!=null && !attr3.equals(""))
                pcisRequest.setReferenceId3(attr3);
                    
            PrepaidCardGenerationResponseType pcisResponse = pcisPort.prepaidCardGeneration(pcisRequest);
            if (pcisResponse.getErrorCode() != 0)
                return "FAIL " + (pcisResponse.getErrorMessage() != null ? pcisResponse.getErrorMessage() : "") + "(" + String.valueOf(pcisResponse.getErrorCode()) + ")";
            else {
                String[] pc = pcisResponse.getPrepaidCardString();
                if (pc != null) {
                    String pcStr = "";
                    for (int i=0; i < pc.length; i++)
                        pcStr = pcStr + pc[i] +"\r\n";
                    
                    HttpSession ses = request.getSession(false);
                    ses.setAttribute(SES_PC_GENERATED_DATA, pcStr);
                                                            
                    return "<A HREF=\"download\">" + 
                           pc[0].substring(0,8) + " - " + pc[pc.length-1].substring(0,8) +
                           "</A>";
                    
                } else
                    return "FAIL Card IDs cannot be obtained";
            }
        } 
        
        return "FAIL Required parameters could not be obtained";       
    }
    
    public static String getRatingAttribits(String attr)
    {
        String str;
        if (attr == null || attr.trim().equals("")) {
            str = "0";
        } else {
            str = Integer.toBinaryString(Integer.parseInt(attr));
        }
        for (int i = str.length(); i < 64; i++)
            str = "0" + str;
        return str;        
    }

    public static String getShortDescriptors(String descriptors) {
        if (descriptors == null || descriptors.trim().equals("") || descriptors.length() <= 2) {
            return "";
        }
        descriptors = descriptors.substring(1, descriptors.length()-1);
        logger.debug("short descriptors: " + descriptors);
        if (descriptors.length() > 55) {
            return descriptors.substring(0, 50) + "...";
        }
        return descriptors;
    }

    public static String getRatingDescriptors(String listDescriptors, String rating, String attr)
    {
        logger.debug("rating: " + attr);
        logger.debug("attr: " + attr);
        if (rating == null || rating.trim().equals("")) {
            return "";
        }
        if (attr == null || attr.trim().equals("")) {
            return "";
        }
        int ind = listDescriptors.indexOf(rating + ":");
        if (ind < 0) {
            return "";
        }
        String rd = listDescriptors.substring(ind + rating.length() + 1);
        ind = rd.indexOf("|");
        if (ind >= 0) {
          rd = rd.substring(0, ind);
        }
        int i = 1;
        int ind1 = 0;
        int ind2 = 0;
        StringBuffer resultStr = new StringBuffer(",");
        while (ind2 >= 0) {
          ind2 = rd.indexOf(',', ind1);
          char bit = attr.charAt(attr.length() - i);
          if (bit == '1') {
              if (ind2 == -1) {
                  resultStr.append(rd.substring(ind1)).append(",");
              } else {
                  resultStr.append(rd.substring(ind1, ind2)).append(",");
              }
          }
          ind1 = ind2 + 1;
          i++;
        }
            
        logger.debug("descriptors: " + resultStr.toString());
        return resultStr.toString();        
    }

    public static String convertTimeZone(String dt, String tz)
    {
        String localTime = dt;
        
        if (dt != null && !dt.equals("")) {
            try {
                Calendar utcCal = new GregorianCalendar(TimeZone.getTimeZone("UTC"));                                
                DateFormat uf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
                uf.setTimeZone(TimeZone.getTimeZone("UTC"));
                Date utcDate = uf.parse(dt);
                utcCal.setTime(utcDate);
                
                Calendar tzCal = new GregorianCalendar(TimeZone.getTimeZone(tz));
                tzCal.setTimeInMillis(utcCal.getTimeInMillis());
                Date tzDate = tzCal.getTime();                                
                DateFormat df = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
                df.setTimeZone(TimeZone.getTimeZone(tz));
                localTime = df.format(tzDate);
                
            } catch (ParseException pe) {pe.printStackTrace();} 
        }
        
        return localTime;
    }
    
    public static String getZoneOffset(String tz)
    {
        Calendar tzCal = new GregorianCalendar(TimeZone.getTimeZone(tz));
        return String.valueOf(tzCal.get(Calendar.ZONE_OFFSET)+tzCal.get(Calendar.DST_OFFSET));
    }
        
    private static void initECSRequest(ServletContext context, com.broadon.wsapi.ecs.AbstractRequestType req) 
        throws Exception
    {
        // Property file defined in the conf/BBserver.properties.tmpl file.
        // Add properties there as required for the ECS server.
        //
        Properties cfg = (Properties) context.getAttribute(PROPERTY_KEY);
        
        // Get ECS URL
        ecsUrl = getURLCfg(cfg, CFG_ECS_URL, DEF_ECS_URL);
        
        ECommerceService ecsService = new ECommerceServiceLocator();
        ecsPort = (ecsUrl == null)? ecsService.getECommerceSOAP():
                    ecsService.getECommerceSOAP(ecsUrl);
    
        req.setVersion("1.0");
        req.setRegionId("NOA");
        req.setCountryCode("US");
        req.setMessageId(Long.toString(System.currentTimeMillis()));
    }
   
    private static void initPCISRequest(ServletContext context, com.broadon.wsapi.pcis.AbstractRequestType req) 
        throws Exception
    {
        // Property file defined in the conf/BBserver.properties.tmpl file.
        // Add properties there as required for the PCIS server.
        //
        Properties cfg = (Properties) context.getAttribute(PROPERTY_KEY);
        
        // Get ECS URL
        pcisUrl = getURLCfg(cfg, CFG_PCIS_URL, DEF_PCIS_URL);
        
        
        PrepaidCardGenerationService pcisService = new PrepaidCardGenerationServiceLocator();
        pcisPort = (pcisUrl == null)? pcisService.getPrepaidCardGenerationSOAP():
            pcisService.getPrepaidCardGenerationSOAP(pcisUrl);
        
        req.setVersion("1.0");
        req.setMessageID(Long.toString(System.currentTimeMillis()));
    }
    
    private static void initPASRequest(ServletContext context, com.broadon.wsapi.pas.AbstractRequestType req) 
    throws Exception
    {
        // Property file defined in the conf/BBserver.properties.tmpl file.
        // Add properties there as required for the PCIS server.
        //
        Properties cfg = (Properties) context.getAttribute(PROPERTY_KEY);
        
        // Get PAS URL
        pasUrl = getURLCfg(cfg, CFG_PAS_URL, DEF_PAS_URL);
        
        
        PaymentAuthorizationService pasService = new PaymentAuthorizationServiceLocator();
        pasPort = (pasUrl == null)? pasService.getPaymentAuthorizationSOAP():
            pasService.getPaymentAuthorizationSOAP(pasUrl);
        
        req.setVersion("1.0");
        req.setMessageID(Long.toString(System.currentTimeMillis()));
    }
    
    private static URL getURLCfg(Properties cfg, String key, String defValue)
        throws Exception
    {
        String urlStr = getCfg(cfg, key, defValue);
        try {
            URL url = (urlStr != null)? new URL(urlStr): null;
            return url;
        }
        catch (MalformedURLException e) {
            throw new Exception("Malformed " + key+ ": " + urlStr, e);
        }
    }

    private static String getCfg(Properties cfg, String key, String defValue)
    {
        String v = (cfg != null)? cfg.getProperty(key): null;
        if (v == null)
            v = defValue;
        
        return v;
    }
    
    private static String formatDate(long timeInMilliSecs)
    {
        if (timeInMilliSecs > 0) {
            Calendar calendar = new GregorianCalendar();
            calendar.setTimeInMillis(timeInMilliSecs);
            
            java.util.Date date = calendar.getTime();
            DateFormat utc = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
            utc.setTimeZone(TimeZone.getTimeZone("GMT+00:00"));
            return utc.format(date);
            
        } else
            return "";
    }
    
    private static Date getDate(String dateAsString)
    {
        String strFormat = "yyyy.MM.dd HH:mm:ss";
        DateFormat myDateFormat = new SimpleDateFormat(strFormat);
        Date myDate = null;
        try {
             myDate = myDateFormat.parse(dateAsString);
        } catch (Exception e) {
             System.out.println("Invalid Date Parser Exception ");
             e.printStackTrace();
        }
        return myDate;
    }
    
}

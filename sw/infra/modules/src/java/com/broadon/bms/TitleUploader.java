package com.broadon.bms;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;
import org.apache.log4j.Logger;
import com.broadon.servlet.ServletConstants;
import com.broadon.bms.common.QueryBean;
import com.broadon.wsapi.cps.PublishPortType;
import com.broadon.wsapi.cps.PublishServiceLocator;
import com.broadon.wsapi.cps.UploadRequestType;
import com.broadon.wsapi.cps.UploadResponseType;

public class TitleUploader implements ServletConstants {
    private static Logger logger = Logger.getLogger(TitleUploader.class.getName());
    protected static final String UPLOAD_ZIP_ENTRY = "UploadRequest";
    /** CPS URL */
    private URL cpsUrl;
    private static final String CFG_CPS_URL = "CPS_URL";
    private static final String DEF_CPS_URL = "http://cps:17102/cps/services/PublishSOAP";
    private PublishPortType publisher;

    public TitleUploader() {}

    public String publishTitle(ServletContext context,
            HttpServletRequest request, 
            HttpServletResponse response, 
            QueryBean qb, 
            Connection conn,
            String[] param) 
    throws IOException, ClassNotFoundException,
            MalformedURLException, ServiceException {
        String packageFileName = param[0];
        logger.debug("packageFileName: " + packageFileName);
        if (packageFileName == null || packageFileName.equals("")) {
            logger.error("Not enough information provided to continue");
            return null;
        }
        init(context);
        ZipFile zip = new ZipFile(packageFileName);
        UploadRequestType uploadReq = getUploadRequest(zip);
        logger.debug("Upload Request: name=" + uploadReq.getTitleName() + ", type=" + uploadReq.getTitleType()
                     + ", action=" + uploadReq.getAction());
        DataHandler[] attachments = getAttachments(zip);
        UploadResponseType uploadRes;
        String result = "";
        try {
            uploadRes = publisher.upload(uploadReq, attachments);
            logger.debug("Upload Response: comment=" + uploadRes.getComment() + ", status=" + uploadRes.getErrorCode()
                         + ", error=" + uploadRes.getErrorMessage());
            if (uploadRes.getErrorCode() != 0) {
                logger.error(uploadRes.getErrorMessage());
                result = "FAIL" + uploadRes.getErrorMessage();
            }
        } finally {
            zip.close();
        }
        return result;
    }

    private DataHandler[] getAttachments(ZipFile zip) {
        logger.debug("Fetch attachments");
        ArrayList handlers = new ArrayList(zip.size());
        for (Enumeration e = zip.entries(); e.hasMoreElements();) {
            ZipEntry entry = (ZipEntry) e.nextElement();
            logger.debug(entry.getName());
            if (UPLOAD_ZIP_ENTRY.equals(entry.getName()))
                continue;
            DataHandler hdlr = new DataHandler(new ZipEntryDataSource(zip,
                                                                      entry));
            handlers.add(hdlr);
        }
        return (DataHandler[]) handlers.toArray(new DataHandler[handlers.size()]);
    }

    private UploadRequestType getUploadRequest(ZipFile zip) throws IOException,
            ClassNotFoundException {
        logger.debug("Fetch upload request object");
        ZipEntry entry = zip.getEntry(UPLOAD_ZIP_ENTRY);
        // TODO: check signature
        ObjectInputStream in = new ObjectInputStream(zip.getInputStream(entry));
        try {
            return (UploadRequestType) in.readObject();
        } finally {
            in.close();
        }
    }

    private class ZipEntryDataSource implements DataSource {
        private ZipFile zip;
        private ZipEntry entry;

        public ZipEntryDataSource(ZipFile zip, ZipEntry entry) {
            this.zip = zip;
            this.entry = entry;
        }

        public String getContentType() {
            return "application/octet-stream";
        }

        public InputStream getInputStream() throws IOException {
            return zip.getInputStream(entry);
        }

        public String getName() {
            return entry.getName();
        }

        public OutputStream getOutputStream() throws IOException {
            throw new IOException("getOutputStream not support for "
                    + getClass().getName());
        }
    }

    private void init(ServletContext context) throws MalformedURLException,
            ServiceException {
        // Property file defined in the conf/BBserver.properties.tmpl file.
        // Add properties there as required for the ECS server.
        Properties cfg = (Properties) context.getAttribute(PROPERTY_KEY);
        // Get CPS URL
        cpsUrl = getURLCfg(cfg, CFG_CPS_URL, DEF_CPS_URL);
        logger.debug("init: cpsUrl=" + cpsUrl);
        PublishServiceLocator cpsService = new PublishServiceLocator();
        publisher = cpsService.getPublishSOAP(cpsUrl);
    }

    private URL getURLCfg(Properties cfg, String key, String defValue)
            throws MalformedURLException {
        String urlStr = getCfg(cfg, key, defValue);
        URL url = (urlStr != null) ? new URL(urlStr) : null;
        return url;
    }

    private String getCfg(Properties cfg, String key, String defValue) {
        String v = (cfg != null) ? cfg.getProperty(key) : null;
        if (v == null)
            v = defValue;
        return v;
    }
}

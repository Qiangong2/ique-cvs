package com.broadon.bms;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The <code>DownloadServlet</code> class handles the requests to download
 * values stored in memory HasMap, identified by the key identifier. 
 */
public class DownloadServlet extends HttpServlet
{
    private static final String SES_PC_GENERATED_DATA = "pc_data";
    
    /**
     * Constructs a ContentDownloadServlet instance.
     */
    public DownloadServlet()
    {
        super();        
    }

    /**
     * Initializes this servlet.
     *
     * @param	servletConfig		the servlet configuration object
     */
    public synchronized void init(ServletConfig servletConfig)
	throws ServletException
    {
        super.init(servletConfig);        
    }

    /**
     * Handles the Get request.
     *
     * @param	req			the HTTP request
     * @param	res			the HTTP response
     *
     * @throws	ServletException
     * @throws	IOException
     */
    public void doGet(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
        HttpSession ses = req.getSession(false);
        
        // Obtain the value from the user session
        Object obj = ses.getAttribute(SES_PC_GENERATED_DATA);
        
        if (obj != null) {
            String pcdata = obj.toString();
            String pckey = pcdata.substring(0,8);
            
            // Construct the response
            res.setContentType("application/vnd.ms-excel");
            res.setHeader("Content-Disposition","attachment; filename=\"prepaid-cards-" + 
                    pckey + ".txt\""); 
            
            PrintWriter out = res.getWriter();
            out.print(pcdata);
            
            // Once downloaded, delete from session
            ses.removeAttribute(SES_PC_GENERATED_DATA);
            return;                       
        }
        
        res.sendError(res.SC_BAD_REQUEST, 
                "There is no data available to download");
        return;   
                
    } // doGet
}

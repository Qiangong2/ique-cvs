package com.broadon.bms;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Types;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import oracle.sql.BLOB;
import org.apache.log4j.Logger;

import com.broadon.bms.common.QueryBean;

public class TitleImageUploader {
    private static Logger logger = Logger.getLogger(TitleImageUploader.class.getName());
    
    static final long BEGIN_CONTENT_ID = 0xFFFD0000L;
    private long titleId;
    private String contentType = null;
    private String contentName = null;
    private String contentFile = null;
    Connection conn = null;

    public TitleImageUploader() {}

    public String registerImage(ServletContext context,
            HttpServletRequest request, 
            HttpServletResponse response, 
            QueryBean qb, 
            Connection conn,
            String[] param) 
    throws Exception {
        logger.debug("Register Image");
        String tid = param[0];
        String cid = param[1];
        String imageType = param[2];
        String locale = param[3];
        String imageName = param[4];
        String file = param[5];
        logger.debug("tid: " + tid + ", cid: " + cid + 
                     ", imageType: " + imageType + ", locale: " + locale + 
                     ", imageName: " + imageName + ", file: " + file );
        if (tid == null || tid.equals("") || imageType == null || imageType.equals("") 
                || imageName == null || imageName.equals("")) {
            logger.error("Not enough information provided to continue");
            return null;
        }
        titleId = Long.parseLong(tid);
        contentType = imageType;
        if (locale != null && !locale.equals("")) {
            contentName = imageName + "_" + locale;
        } else {
            contentName = imageName;
        }
        contentFile = file;
        this.conn = conn;
        
        if (!checkUniqueness()) {
            // error code = 1, JSP page will translate it to localized message
            return "FAIL1";
        }
        
        if (cid != null && !cid.equals("")) {
            BigDecimal contentId = new BigDecimal(cid);
            updateContent(contentId);
       } else {
            createImage();
        }
        return "";
    }

    static final String CHECK_UNIQUENESS = "SELECT 1 FROM DUAL " +
            "WHERE EXISTS (SELECT c.content_id FROM contents c, content_titles ct " +
            "WHERE ct.title_id = ?  AND c.content_id BETWEEN " +
            "TO_NUMBER(LPAD(LTRIM(TO_CHAR(ct.title_id,RPAD('X',16,'X'))),16,'0')||'FFFD0000','XXXXXXXXXXXXXXXXXXXXXXXX') " +
            "AND TO_NUMBER(LPAD(LTRIM(TO_CHAR(ct.title_id,RPAD('X',16,'X'))),16,'0')||'FFFDFFFF','XXXXXXXXXXXXXXXXXXXXXXXX') " +
            "AND c.content_type = ? AND c.content_name = ?) ";

    private boolean checkUniqueness() throws SQLException {
        logger.debug("Check uniqueness: " + CHECK_UNIQUENESS + ", titleId: " + titleId +
                     ", contentType: " + contentType + ", contentName: " + contentName);
        PreparedStatement ps = conn.prepareStatement(CHECK_UNIQUENESS);
        ResultSet rs = null;
        try {
            ps.setLong(1, titleId);
            ps.setString(2, contentType);
            ps.setString(3, contentName);
            rs = ps.executeQuery();
            if (rs.next()) {
                return false;
            }
            return true;
        } finally {
            closeResource(ps, rs);
        }
    }
    
    private void createImage()
            throws SQLException, GeneralSecurityException, ServletException,
            FileNotFoundException, IOException {
        logger.debug("Create Image: contentFile=" + contentFile + "");
        TrivialContentInputStream contentBlob = 
            new TrivialContentInputStream(new FileInputStream(contentFile));
        createContentCache(contentBlob);
        BigDecimal contentId = checkContentID();
        if (contentId == null)
            genContentID(titleId, BEGIN_CONTENT_ID);
        // insert the content record
        createContentRecord(contentId, contentBlob.getMd5HashString());
    }

    static final String ADD_CONTENT = "INSERT INTO CONTENTS (CONTENT_ID, PUBLISH_DATE, " +
            "CONTENT_CHECKSUM, CONTENT_NAME, CONTENT_TYPE) " +
            "VALUES (?, SYSDATE, ?, ? , ?)";

    private void createContentRecord(BigDecimal contentId, String checkSum)
            throws SQLException {
        logger.debug("Create content record");
        PreparedStatement ps = null;
        int i = 1;
        try {
            // insert the content row
            logger.debug(ADD_CONTENT + ", contentId: " + contentId + 
                         ", checkSum: " + checkSum);
            ps = conn.prepareStatement(ADD_CONTENT);
            ps.setBigDecimal(i++, contentId);
            ps.setString(i++, checkSum);
            ps.setString(i++, contentName);
            ps.setString(i++, contentType);
            ps.executeUpdate();
        } finally {
            closeResource(ps, null);
        }
    } // createContentRecord
    static final String ADD_CONTENT_CACHE = "INSERT INTO CONTENT_CACHE (CONTENT_CHECKSUM, CONTENT_SIZE, " +
            "CONTENT_OBJECT_VERSION, CREATE_DATE, CONTENT_OBJECT) " +
            "SELECT ?, ?, ?, SYSDATE, ? FROM DUAL " +
            "WHERE NOT EXISTS (SELECT 1 FROM CONTENT_CACHE WHERE CONTENT_CHECKSUM = ?)";
 
    private void createContentCache(ContentInputStream contentBlob)
            throws SQLException {
        logger.debug("Create content cache");
        PreparedStatement ps = null;
        Savepoint savePt = conn.setSavepoint();
        BLOB blob = writeTempBlob(contentBlob);
        String checkSum = contentBlob.getMd5HashString();
        try {
            // insert the content_cache row
            logger.debug(ADD_CONTENT_CACHE + ", checkSum: " + checkSum
                         + ", size: " + blob.length()
                         + ", contentType: " + contentType
                         + ", contentName: " + contentName);
            ps = conn.prepareStatement(ADD_CONTENT_CACHE);
            int i = 1;
            ps.setString(i++, checkSum);
            ps.setLong(i++, blob.length());
            ps.setNull(i++, Types.SMALLINT);
            ps.setBlob(i++, blob);
            ps.setString(i++, checkSum);
            ps.executeUpdate();
        } finally {
            closeResource(ps, null);
        }
    }
    static final String UPDATE_CONTENT = "UPDATE CONTENTS SET CONTENT_TYPE = ?, "
            + "CONTENT_NAME = ? WHERE CONTENT_ID = ?";

    private void updateContent(BigDecimal contentID) throws SQLException {
        logger.debug("Update content cache: " + UPDATE_CONTENT + 
                     ", contentType: " + contentType + 
                     ", contentName: " + contentName +
                     ", contentID: " + contentID);

        PreparedStatement ps = null;
        Savepoint savePt = conn.setSavepoint();
        try {
            // update the content_cache row
            ps = conn.prepareStatement(UPDATE_CONTENT);
            int i = 1;
            ps.setString(i++, contentType);
            ps.setString(i++, contentName);
            ps.setBigDecimal(i++, contentID);
            ps.executeUpdate();
        } finally {
            closeResource(ps, null);
        }
    }
    static final String CHECK_CONTENT_ID = "SELECT max(c.content_id) max_content_id "
            + "FROM contents c, content_titles ct "
            + "WHERE ct.title_id = ?  "
            + "AND c.content_id BETWEEN  "
            + "TO_NUMBER(LPAD(LTRIM(TO_CHAR(ct.title_id,RPAD('X',16,'X'))),16,'0')||'FFFD0000','XXXXXXXXXXXXXXXXXXXXXXXX')  "
            + "AND  "
            + "TO_NUMBER(LPAD(LTRIM(TO_CHAR(ct.title_id,RPAD('X',16,'X'))),16,'0')||'FFFDFFFF','XXXXXXXXXXXXXXXXXXXXXXXX') "
            + "GROUP BY ct.title_id";

    private BigDecimal checkContentID() throws SQLException {
        logger.debug("Check content id: " + CHECK_CONTENT_ID + ", titleId: " + titleId);
        PreparedStatement ps = conn.prepareStatement(CHECK_CONTENT_ID);
        ResultSet rs = null;
        BigDecimal contentId;
        try {
            ps.setLong(1, titleId);
            rs = ps.executeQuery();
            if (rs.next()) {
                contentId = rs.getBigDecimal(1);
                long cid = extractContentID(contentId);
                cid = cid + 1;
                contentId = genContentID(titleId, cid);
                logger.debug("Found existing content id in the range, increase it by 1: " + contentId);
                return contentId;
            } else {
                return genContentID(titleId, this.BEGIN_CONTENT_ID);
            }
        } finally {
            closeResource(ps, rs);
        }
    }

    private BigDecimal genContentID(long tid, long cid) {
        // BigInteger version of ((tid << 32) | cid)
        BigDecimal contentId =new BigDecimal(BigInteger.valueOf(tid).shiftLeft(32).or(
                                                             BigInteger.valueOf(cid)));
        logger.debug("Generated content id: " + contentId);
        return contentId;
    }

    private long extractContentID(BigDecimal contentId) {
        // BigInteger version of (contentId & 0xffffffff)
        return contentId.toBigInteger().and(BigInteger.valueOf(0xFFFFFFFFL)).longValue();
    }

    // stream and the content into a temporary blob, and compute its checksum as
    // we go
    private BLOB writeTempBlob(ContentInputStream contentBlob)
            throws SQLException {
        logger.debug("Compute checksum");
        BLOB tempBlob = BLOB.createTemporary(conn, true, BLOB.DURATION_SESSION);
        OutputStream out = tempBlob.setBinaryStream(1);
        byte[] buffer = new byte[64 * 1024];
        int n;
        try {
            while ((n = contentBlob.read(buffer)) >= 0) {
                if (n == 0) // ContentInputSream.read() could return 0 before
                    // EOF
                    continue;
                out.write(buffer, 0, n);
            }
        } catch (IOException e) {
            SQLException se = new SQLException(e.getLocalizedMessage());
            se.setStackTrace(e.getStackTrace());
            throw se;
        } finally {
            try {
                out.close();
            } catch (IOException e) {}
        }
        return tempBlob;
    }

    /**
     * Frees up database resources.
     */
    private void closeResource(Statement stmt, ResultSet rs) {
        try {
            if (stmt != null) {
                stmt.close();
            }
            if (rs != null) {
                rs.close();
            }
        } catch (Exception e) {}
    }
}

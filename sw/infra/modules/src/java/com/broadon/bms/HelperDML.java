package com.broadon.bms;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import com.broadon.bms.common.QueryBean;

/**
 * Data manipulation library functions defined here are 
 * helper functions to HelperFunctions class
 */
public class HelperDML
{
    private static Logger logger= Logger.getLogger(HelperDML.class.getName());
    
    /** Define all SQL statements
     */
    private static final String TABLE_CAS_CONTENT_TITLES = "CAS.CONTENT_TITLES";
    private static final String TABLE_CAS_CONTENTS = "CAS.CONTENTS";
    private static final String TABLE_CAS_CONTENT_TITLE_LOCALES = "CAS.CONTENT_TITLE_LOCALES";
    private static final String TABLE_CAS_CONTENT_TITLE_OBJECTS = "CAS.CONTENT_TITLE_OBJECTS";
    private static final String TABLE_CAS_CONTENT_TITLE_COUNTRIES = "CAS.CONTENT_TITLE_COUNTRIES";
    private static final String TABLE_CAS_CAS_ITEM_PRICINGS = "CAS.CAS_ITEM_PRICINGS";
    
    private static final String TABLE_CONTENT_TITLES = "CONTENT_TITLES";
    private static final String TABLE_CONTENTS = "CONTENTS";
    private static final String TABLE_CONTENT_TITLE_LOCALES = "CONTENT_TITLE_LOCALES";
    private static final String TABLE_CONTENT_TITLE_OBJECTS = "CONTENT_TITLE_OBJECTS";
    private static final String TABLE_CONTENT_TITLE_COUNTRIES = "CONTENT_TITLE_COUNTRIES";
    private static final String TABLE_CAS_ITEM_PRICINGS = "CAS_ITEM_PRICINGS";
    private static final String TABLE_COUNTRIES = "COUNTRIES";
    
    private static final String TABLE_PAS_BANK_ACCOUNTS = "PAS_BANK_ACCOUNTS";
    private static final String TABLE_IAS_DEVICES = "IAS_DEVICES";
    private static final String TABLE_CONTENT_TITLE_CATALOG = "CONTENT_TITLE_CATALOG";
    
    private static final String TABLE_OPERATION_USERS = "OPERATION_USERS";
    private static final String TABLE_OPERATION_ROLES = "OPERATION_ROLES";
    private static final String TABLE_OPERATION_GROUPS = "OPERATION_GROUPS";
    
    private static final String GET_TITLE_CATALOG_DETAILS =
        "SELECT title, title_id, platform FROM " + TABLE_CONTENT_TITLE_CATALOG + 
        " WHERE hex_title_id = ?";
    
    private static final String GET_DEVICE_DETAILS =
        "SELECT serial_no, config_code, register_date FROM " + TABLE_IAS_DEVICES + 
        " WHERE device_id = ?";
    
    private static final String GET_TITLE_COUNTRIES_DETAILS =
        "SELECT rating_system, rating_value, rating_age FROM " + TABLE_CONTENT_TITLE_COUNTRIES + 
        " WHERE title_id = ? AND country_id = ?";
    
    private static final String GET_PAS_ACCOUNT_PIN =
        "SELECT pin FROM " + TABLE_PAS_BANK_ACCOUNTS + 
        " WHERE account_id = ?";
    
    private static final String DELETE_CAS_CONTENT_TITLES =
        "DELETE FROM " + TABLE_CAS_CONTENT_TITLES + " WHERE title_id = ?";    
    private static final String DELETE_CAS_CONTENTS =
        "DELETE FROM " + TABLE_CAS_CONTENTS + 
        " WHERE content_id >= ?*POWER(2,32) AND content_id < (?+1)*POWER(2,32)";    
    private static final String DELETE_CAS_CONTENT_TITLE_LOCALES =
        "DELETE FROM " + TABLE_CAS_CONTENT_TITLE_LOCALES + " WHERE title_id = ?";
    private static final String DELETE_CAS_CONTENT_TITLE_OBJECTS =
        "DELETE FROM " + TABLE_CAS_CONTENT_TITLE_OBJECTS + " WHERE title_id = ?";
    private static final String DELETE_CAS_CONTENT_TITLE_COUNTRIES =
        "DELETE FROM " + TABLE_CAS_CONTENT_TITLE_COUNTRIES + " WHERE title_id = ?";
    private static final String DELETE_CAS_CAS_ITEM_PRICINGS =
        "DELETE FROM " + TABLE_CAS_CAS_ITEM_PRICINGS + " WHERE title_id = ?";
    
    private static final String INSERT_CAS_CONTENT_TITLES =
        "INSERT INTO " + TABLE_CAS_CONTENT_TITLES + 
        " SELECT * FROM " + TABLE_CONTENT_TITLES + " WHERE title_id = ?";
    private static final String INSERT_CAS_CONTENTS =
        "INSERT INTO " + TABLE_CAS_CONTENTS + 
        " SELECT * FROM " + TABLE_CONTENTS + 
        " WHERE content_id >= ?*POWER(2,32) AND content_id < (?+1)*POWER(2,32)";
    private static final String INSERT_CAS_CONTENT_TITLE_LOCALES =
        "INSERT INTO " + TABLE_CAS_CONTENT_TITLE_LOCALES + 
        " SELECT * FROM " + TABLE_CONTENT_TITLE_LOCALES + " WHERE title_id = ?";
    private static final String INSERT_CAS_CONTENT_TITLE_OBJECTS =
        "INSERT INTO " + TABLE_CAS_CONTENT_TITLE_OBJECTS + 
        " SELECT * FROM " + TABLE_CONTENT_TITLE_OBJECTS + " WHERE title_id = ?";
    private static final String INSERT_CAS_CONTENT_TITLE_COUNTRIES =
        "INSERT INTO " + TABLE_CAS_CONTENT_TITLE_COUNTRIES + 
        " SELECT * FROM " + TABLE_CONTENT_TITLE_COUNTRIES + " WHERE title_id = ?";
    private static final String INSERT_CAS_CAS_ITEM_PRICINGS =
        "INSERT INTO " + TABLE_CAS_CAS_ITEM_PRICINGS + 
        " SELECT * FROM " + TABLE_CAS_ITEM_PRICINGS + " WHERE title_id = ?";
    
    private static final String DELETE_POINTS_CAS_CAS_ITEM_PRICINGS =
        "DELETE FROM " + TABLE_CAS_CAS_ITEM_PRICINGS + " WHERE license_type = 'VCPOINTS' AND country_id = ?";    
    private static final String INSERT_POINTS_CAS_CAS_ITEM_PRICINGS =
        "INSERT INTO " + TABLE_CAS_CAS_ITEM_PRICINGS + 
        " SELECT * FROM " + TABLE_CAS_ITEM_PRICINGS + " WHERE license_type = 'VCPOINTS' AND country_id = ?";
    
    private static final String UPDATE_CONTENT_TITLES_APPROVE =
        "UPDATE " + TABLE_CONTENT_TITLES + " SET approve_date = sys_extract_utc(current_timestamp)" +
        " WHERE title_id = ?";
    
    private static final String UPDATE_POINTS_APPROVE =
        "UPDATE " + TABLE_COUNTRIES + " SET points_approve_date = sys_extract_utc(current_timestamp)" +
        " WHERE pricing_country_id = ?";
    
    private static final String GET_LOGIN_DETAILS =
        "SELECT u.operator_id, u.email_address, u.status, u.fullname, u.email_alerts, " +
        "u.role_id, u.last_logon, r.description " +
        "FROM " + TABLE_OPERATION_USERS + " u, " + TABLE_OPERATION_ROLES + " r " +
        "WHERE u.role_id = r.role_id AND u.email_address = ? AND u.passwd = ?";
    private static final String UPDATE_LOGIN_DATE =
        "UPDATE " + TABLE_OPERATION_USERS + " SET last_logon = sys_extract_utc(current_timestamp)" +
        " WHERE email_address = ? AND passwd = ?";
       
 
    /**
     * Get Title Details for ETickets from a given Hex Title Id.
     * 
     * @param conn Database connection.
     * @param hexTitleId Hex Title Id
     * 
     * @exception SQLException Database access error.
     */
    public static String getAccountPin(Connection conn, 
                    String accountId)
        throws SQLException
    {
        String pin = "";
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            stmt = conn.prepareStatement(GET_PAS_ACCOUNT_PIN);
            stmt.setLong(1, Long.parseLong(accountId));
                
            rs = stmt.executeQuery();
            if (rs.next())                   
                pin = rs.getString("PIN");
                
        } finally {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
        
        return pin;
    }
    
    /**
     * Get Title Details for ETickets from a given Hex Title Id.
     * 
     * @param conn Database connection.
     * @param hexTitleId Hex Title Id
     * 
     * @exception SQLException Database access error.
     */
    public static String getTitleName(Connection conn, 
                    String hexTitleId)
        throws SQLException
    {
        String titleName = "";
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            stmt = conn.prepareStatement(GET_TITLE_CATALOG_DETAILS);
            stmt.setString(1, hexTitleId);
                
            rs = stmt.executeQuery();
            if (rs.next())                   
                titleName = rs.getString("TITLE");
                
        } finally {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
        
        return titleName;
    }
    
    /**
     * Get Title Details for ETickets from a given Hex Title Id.
     * 
     * @param conn Database connection.
     * @param hexTitleId Hex Title Id
     * 
     * @exception SQLException Database access error.
     */
    public static String getETicketsTitleDetails(Connection conn, 
                    String deviceId,
                    String hexTitleId)
    	throws SQLException
    {
        String info1 = "<TITLE></TITLE><PLATFORM></PLATFORM>";
        String info2 = "<RATING></RATING>";
        
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            String titleId = null;
            stmt = conn.prepareStatement(GET_TITLE_CATALOG_DETAILS);
            stmt.setString(1, hexTitleId);
                
            rs = stmt.executeQuery();
            if (rs.next()) {                    
                titleId = String.valueOf(rs.getLong("TITLE_ID"));
                info1 = "<TITLE>" + (rs.getString("TITLE") != null ? rs.getString("TITLE") : "") + "</TITLE>" +
                    "<PLATFORM>" + (rs.getString("PLATFORM") != null ? rs.getString("PLATFORM") : "") + "</PLATFORM>";               
            }
            
            rs.close();
            stmt.close();
            
            String configCode = null;
            stmt = conn.prepareStatement(GET_DEVICE_DETAILS);
            stmt.setLong(1, Long.parseLong(deviceId));
            
            rs = stmt.executeQuery();
            if (rs.next())
                configCode = rs.getString("CONFIG_CODE");       
            
            rs.close();
            stmt.close();
            
            // TODO: Config Code will need to be parsed to get the country id, region info
            if (titleId != null && !titleId.equals("") &&
                    configCode != null && !configCode.equals("")) {
                stmt = conn.prepareStatement(GET_TITLE_COUNTRIES_DETAILS);
                stmt.setLong(1, Long.parseLong(titleId));
                stmt.setLong(2, Long.parseLong(configCode));
                
                rs = stmt.executeQuery();
                if (rs.next())
                    info2 = "<RATING>" + (rs.getString("RATING_VALUE") != null ? rs.getString("RATING_VALUE") : "") + "</RATING>";                
            }
            
        } finally {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
        
        return info1 + info2;
    }   
    
    /**
     * Copy all Title related records from the Master(BMS) to the Slave(CAS)
     * 
     * @param conn Database connection.
     * @param titleId - Title Id
     * 
     * @exception SQLException Database access error.
     */
    public static void pushTitleToCAS(Connection conn, String titleId)
        throws IOException
    {
        PreparedStatement ps = null;
        
        String[] titleStmts = {UPDATE_CONTENT_TITLES_APPROVE,
                DELETE_CAS_CONTENT_TITLE_OBJECTS, 
                DELETE_CAS_CONTENT_TITLE_LOCALES,
                DELETE_CAS_CONTENT_TITLE_COUNTRIES,
                DELETE_CAS_CAS_ITEM_PRICINGS,
                DELETE_CAS_CONTENTS,
                DELETE_CAS_CONTENT_TITLES,
                INSERT_CAS_CONTENT_TITLE_OBJECTS, 
                INSERT_CAS_CONTENT_TITLE_LOCALES,
                INSERT_CAS_CONTENT_TITLE_COUNTRIES,
                INSERT_CAS_CAS_ITEM_PRICINGS,
                INSERT_CAS_CONTENTS,
                INSERT_CAS_CONTENT_TITLES};
                
        try {
            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            int[] results = new int[titleStmts.length];
            
            for (int i = 0; i < titleStmts.length; i++) {                
                ps = conn.prepareStatement(titleStmts[i]);
                ps.setLong(1, Long.parseLong(titleId));
                
                if (i == 5 || i == 11)
                    ps.setLong(2, Long.parseLong(titleId)); 
                
                results[i] = ps.executeUpdate();
                      
                if (results[i] < 0)                    
                    throw new IOException("Failed to execute SQL: " +
                            titleStmts[i]);
            }            
            
            conn.commit();
        }
        catch (Throwable e) {
            throw new IOException("Failed to execute SQL: " +
                                  e.getMessage());
        }
        finally {
            try {
                if (ps != null)
                    ps.close();
                try {
                    conn.rollback();
                } catch (SQLException s) {
                    s.printStackTrace();
                }
            }
            catch (Throwable e) {
                throw new IOException(e.getMessage());
            }
        }
        
        return;
    }
    
    /**
     * Copy all Points pricing records from the Master(BMS) to the Slave(CAS)
     * 
     * @param conn Database connection.
     * @param pricingCountryId - Pricing Country Id
     * 
     * @exception SQLException Database access error.
     */
    public static void pushPointsToCAS(Connection conn, String pricingCountryId)
        throws IOException
    {
        PreparedStatement ps = null;
        
        String[] pointsStmts = {UPDATE_POINTS_APPROVE,               
                DELETE_POINTS_CAS_CAS_ITEM_PRICINGS,               
                INSERT_POINTS_CAS_CAS_ITEM_PRICINGS};
                
        try {
            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            int[] results = new int[pointsStmts.length];
            
            for (int i = 0; i < pointsStmts.length; i++) {                
                ps = conn.prepareStatement(pointsStmts[i]);
                ps.setInt(1, Integer.parseInt(pricingCountryId));
                
                results[i] = ps.executeUpdate();
                      
                if (results[i] < 0)                    
                    throw new IOException("Failed to execute SQL: " +
                            pointsStmts[i]);
            }            
            
            conn.commit();
        }
        catch (Throwable e) {
            throw new IOException("Failed to execute SQL: " +
                                  e.getMessage());
        }
        finally {
            try {
                if (ps != null)
                    ps.close();
                try {
                    conn.rollback();
                } catch (SQLException s) {
                    s.printStackTrace();
                }
            }
            catch (Throwable e) {
                throw new IOException(e.getMessage());
            }
        }
        
        return;
    }
    
    public static int processLogin(Connection conn, HttpSession ses,
                    String email, String pwd,
                    String SES_ROLE, 
                    String SES_FULLNAME, 
                    String SES_USER_ID, 
                    String SES_ROLE_NAME,
                    String SES_LAST_LOGON)
        throws SQLException
    {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int result = 3;
        
        try {
            stmt = conn.prepareStatement(GET_LOGIN_DETAILS);
            stmt.setString(1, email);
            stmt.setString(2, pwd);
                
            rs = stmt.executeQuery();
            if (rs.next()) {
                String status = rs.getString("STATUS");
                if (!status.equals("A"))
                  result = 2;
                
                else {
                    ses.setAttribute(SES_ROLE, String.valueOf(rs.getInt("ROLE_ID")));
                    ses.setAttribute(SES_FULLNAME, rs.getString("FULLNAME"));
                    ses.setAttribute(SES_USER_ID, String.valueOf(rs.getLong("OPERATOR_ID")));
                    ses.setAttribute(SES_ROLE_NAME, rs.getString("DESCRIPTION"));
                    ses.setAttribute(SES_LAST_LOGON, rs.getString("LAST_LOGON"));
                    
                    stmt = conn.prepareStatement(UPDATE_LOGIN_DATE);
                    stmt.setString(1, email);
                    stmt.setString(2, pwd);
                    
                    stmt.executeUpdate();
                    
                    result = 0;
                }
            } else
                result = 1;
                
        } finally {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
        
        return result;        
    }
    
    public static boolean createOperationGroup(Connection conn, 
                            String group_name, 
                            String roleId,
                            String isGlobal, 
                            String countries, 
                            String operators)
    throws IOException
    {
        boolean result = false;
        PreparedStatement ps = null;
        
        String sql = "INSERT INTO " + TABLE_OPERATION_GROUPS;
        sql = sql + " (group_id,group_name,role_id,is_global,country_list,operator_list)";
        
        try {
            QueryBean qb = new QueryBean();
            long group_id = qb.getNextID("GROUP_ID", TABLE_OPERATION_GROUPS, conn);
            
            sql = sql + " VALUES (" + 
                String.valueOf(group_id) + ",'" + group_name + "'," + roleId + "," +
                ((isGlobal!=null && !isGlobal.equals("")) ? isGlobal : "0") +
                ((countries!=null && !countries.equals("")) ? (",INTEGER_LIST(" + countries + ")") : ",null") +
                ((operators!=null && !operators.equals("")) ? (",INTEGER_LIST(" + operators + ")") : ",null") + ")";                                                    
            
            ps = conn.prepareStatement(sql);
                
            int rows = ps.executeUpdate();           
            
            if (rows == 1) {
                conn.commit();
                result = true;
            }
            
        }
        catch (Throwable e) {
            throw new IOException("Failed to execute SQL: " +
                    e.getMessage());
        }
        finally {
            try {
                if (ps != null)
                    ps.close();
                try {
                    conn.rollback();
                } catch (SQLException s) {
                    s.printStackTrace();
                }
            }
            catch (Throwable e) {
                throw new IOException(e.getMessage());
            }
        }
        
        return result;
    }

    public static boolean updateOperationGroup(Connection conn,
				    String group_id, 
                            String group_name,
                            String isGlobal, 
                            String countries, 
                            String operators)
    throws IOException
    {
        boolean result = false;
        PreparedStatement ps = null;
        
        String sql = "UPDATE " + TABLE_OPERATION_GROUPS;
        sql = sql + " SET group_name='" + group_name + "'" +
            ",is_global=" + ((isGlobal!=null && !isGlobal.equals("")) ? isGlobal : "0") +
            ",country_list=" + ((countries!=null && !countries.equals("")) ? ("INTEGER_LIST(" + countries + ")") : "null") +
            ",operator_list=" + ((operators!=null && !operators.equals("")) ? ("INTEGER_LIST(" + operators + ")") : "null") + 
	    " WHERE group_id=" + group_id;

        logger.debug(sql);
        try {
            ps = conn.prepareStatement(sql);
                
            int rows = ps.executeUpdate();           
            
            if (rows == 1) {
                conn.commit();
                result = true;
            }
            
        }
        catch (Throwable e) {
            throw new IOException("Failed to execute SQL: " +
                    e.getMessage());
        }
        finally {
            try {
                if (ps != null)
                    ps.close();
                try {
                    conn.rollback();
                } catch (SQLException s) {
                    s.printStackTrace();
                }
            }
            catch (Throwable e) {
                throw new IOException(e.getMessage());
            }
        }
        
        return result;
    }

    
} // HelperDML

package com.broadon.bms;

import java.io.IOException;
import java.io.Reader;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import org.apache.log4j.Logger;
import oracle.jdbc.OracleConnectionWrapper;
import oracle.sql.CLOB;
import com.broadon.bms.common.QueryBean;
import com.broadon.db.OracleDataSource;

public class Attributes {
    private static Logger logger = Logger.getLogger(Attributes.class.getName());
    private static final String GET_ATTRIBUTES = "SELECT attr_values FROM attribute_lookups "
            + "WHERE attr_type = ? AND attr_name = ? AND locale = ?";
    private static final String GET_ATTRIBUTES_BY_TYPE = "SELECT attr_name, attr_values "
        + "FROM attribute_lookups WHERE attr_type = ? AND locale = ?";
    private static final String UPDATE_ATTRIBUTE = "UPDATE attribute_lookups SET attr_values = ? "
        + "WHERE attr_type = ? AND attr_name = ? AND locale = ?";
    private static boolean _inited = false;
    private static String _listRegion;
    private static String _listLocale;
    private static String _listTimeZone;
    private static String _listCategory;
    private static String _listPlatform;
    private static String _listPublisher;
    private static String _listImageName;
    private static String _listRatingSystem;
    private static String _listRatingDescriptor;

    public synchronized static void init(DataSource ds, String locale) {
        if (_inited) {
            return;
        }
        if (locale == null) {
            locale = "en_US";
        }
        Connection wrappedConnection = null;
        Connection connection = null;
        try {
            connection = wrappedConnection = ds.getConnection();
            if (connection instanceof OracleDataSource.ConnectionWrapper) {
                connection = ((OracleConnectionWrapper) connection).unwrap();
            }
            connection.setAutoCommit(false);
            connection.setReadOnly(true);
            _listRegion = getAttributes(connection, locale, "COUNTRIES", "REGION");
            _listLocale = getAttributes(connection, locale, "COUNTRIES", "LOCALE");
            _listTimeZone = getAttributes(connection, locale, "COUNTRIES", "TIMEZONE");
            _listCategory = getAttributes(connection, locale, "CONTENT_TITLES", "CATEGORY");
            _listPlatform = getAttributes(connection, locale, "CONTENT_TITLES", "PLATFORM");
            _listPublisher = getAttributes(connection, locale, "CONTENT_TITLES", "PUBLISHER");
            _listImageName = getAttributes(connection, locale, "CONTENT_TITLES", "IMAGE");
            _listRatingSystem = getAttributesByType(connection, locale, "RATING_SYSTEM");
            _listRatingDescriptor = getAttributesByType(connection, locale, "RATING_ATTRIBITS");
            _inited = true;
            logger.debug("Attribute Lookups initialized!");
            logger.debug(_listRegion);
            logger.debug(_listLocale);
            logger.debug(_listTimeZone);
            logger.debug(_listCategory);
            logger.debug(_listPlatform);
            logger.debug(_listPublisher);
            logger.debug(_listImageName);
            logger.debug(_listRatingSystem);
            logger.debug(_listRatingDescriptor);
        } catch (SQLException se) {
            logger.error("Attributes.init() - SQLException occurred - " + se);
            se.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException s) {
                s.printStackTrace();
            }
        } catch (IOException ie) {
            logger.error("Attributes.init() - IOException occurred - " + ie);
            ie.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException s) {
                s.printStackTrace();
            }
        } finally {
            try {
                wrappedConnection.close();
            } catch (SQLException s) {
                s.printStackTrace();
            }
        }
    }

    public static String updateAttribute(ServletContext context,
            HttpServletRequest request,
            HttpServletResponse response, 
            QueryBean qb, 
            Connection conn,
            String[] param) 
        throws Exception 
    {        
        if (param != null) {
            String type = param[0];
            String name = param[1];
            String locale = param[2];
            String values = param[3];
            
            if (type != null && !type.equals("") &&
                    name != null && !name.equals("") &&
                    locale != null && !locale.equals("") &&
                    values != null && !values.equals("")) {
                try {
                    updateAttribute(conn, type, name, locale, values);
                    return null;
                } catch (Exception e) {
                    return "FAIL" + e.getMessage();
                }
            } 
        }
        
        return "FAIL Attriute type not found";            
    }

    private static void updateAttribute(Connection connection, String type, String name, 
            String locale, String values) throws SQLException, IOException {
        PreparedStatement stmt = null;
        try {
            logger.debug(UPDATE_ATTRIBUTE + ", type: " + type + ", name: " + name +
                         ", locale: " + locale +", values: " + values);
            stmt = connection.prepareStatement(UPDATE_ATTRIBUTE);
            
            Clob newClob = CLOB.createTemporary(connection, false, CLOB.DURATION_SESSION);
            newClob.setString(1, values);
            stmt.setClob(1, newClob);
            stmt.setString(2, type);
            stmt.setString(3, name);
            stmt.setString(4, locale);

            stmt.executeUpdate();
        } finally {
            if (stmt != null)
                stmt.close();
        }
    }

    private static String getAttributes(Connection connection, String locale,
            String type, String name) throws SQLException, IOException {
        String values = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.prepareStatement(GET_ATTRIBUTES);
            stmt.setString(1, type);
            stmt.setString(2, name);
            stmt.setString(3, locale);
            rs = stmt.executeQuery();
            if (rs.next()) {
                Clob clob = rs.getClob(1);
                Reader reader = clob.getCharacterStream();
                int size = (int) clob.length();
                char[] buffer = new char[size];
                reader.read(buffer);
                values = new String(buffer);
            }
        } finally {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
        return values;
    }

    private static String getAttributesByType(Connection connection,
            String locale, String type) throws SQLException, IOException {
        StringBuffer values = new StringBuffer();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.prepareStatement(GET_ATTRIBUTES_BY_TYPE);
            stmt.setString(1, type);
            stmt.setString(2, locale);
            rs = stmt.executeQuery();
            while (rs.next()) {
                String attrName = rs.getString(1);
                Clob clob = rs.getClob(2);
                Reader reader = clob.getCharacterStream();
                int size = (int) clob.length();
                char[] buffer = new char[size];
                reader.read(buffer);
                if (values.length() > 0) {
                    values.append("|");
                }
                values.append(attrName).append(":").append(new String(buffer));
            }
        } finally {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
        return values.toString();
    }

    public static String getListCategory() {
        return _listCategory;
    }

    public static String getListImageName() {
        return _listImageName;
    }

    public static String getListLocale() {
        return _listLocale;
    }

    public static String getListPlatform() {
        return _listPlatform;
    }

    public static String getListPublisher() {
        return _listPublisher;
    }

    public static String getListRatingDescriptor() {
        return _listRatingDescriptor;
    }

    public static String getListRatingSystem() {
        return _listRatingSystem;
    }

    public static String getListRegion() {
        return _listRegion;
    }

    public static String getListTimeZone() {
        return _listTimeZone;
    }
}

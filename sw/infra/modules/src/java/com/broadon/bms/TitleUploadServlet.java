package com.broadon.bms;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.broadon.bms.common.AdminServlet;

public class TitleUploadServlet extends AdminServlet {
    private static final String CURRENT_LOCALE = "CURRENT_LOCALE";
    private static final String PUBLISH_PROP_KEY = "PUBLISH_PROP";
    private static final String JSP_LOCATION = "JSP_LOCATION";
    private static final String TITLE_LIST_JSP = "/serv?type=title.publish&action=edit";
    private static final String FORBIDDEN_JSP = "forbidden.jsp";
    private final static String UPLOAD_PATH = "PATH_UPLOAD";
    private static Logger logger;
    private static String locale;

    public TitleUploadServlet() {}

    public void init(ServletConfig config) throws ServletException {
        logger = Logger.getLogger(TitleUploadServlet.class.getName());
        try {
            super.init(config);
            if (mAdminProp != null)
                mLabelsFile = mAdminProp.getProperty(PUBLISH_PROP_KEY);
        } catch (ServletException e) {
            throw (e);
        }
    }

    public void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        HttpSession ses = req.getSession(false);
        if (ses == null) {
            logger.warn("NO SESSION.");
            return;
        }
        locale = getSessionString(ses, CURRENT_LOCALE);
        if (locale != null && !locale.equals("")) {
            storeLabels(locale, mLabelsFile, ses);
            locale = "/" + locale;
        } else
            locale = "";
        logger.warn("GET action is not supported.");
        forwardToModule(locale, JSP_LOCATION, FORBIDDEN_JSP, req, res);
        return;
    }

    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        HttpSession ses = req.getSession(false);
        if (ses == null) {
            logger.warn("NO SESSION.");
            return;
        }
        locale = getSessionString(ses, CURRENT_LOCALE);
        if (locale != null && !locale.equals("")) {
            storeLabels(locale, mLabelsFile, ses);
            locale = "/" + locale;
        } else
            locale = "";
        logger.info("TitleUpload - New Request");
        if (!doAuthenticate(req, res)) {
            logger.info("ImageUpload - Cannot Authenticate. Session seems to have expired.");
            return;
        }
        logger.info("TitleUpload - Procesing request for title upload");
        uploadTitle(req, res, ses);
        logger.info("TitleUpload - End Request\n");
        return;
    }

    public void uploadTitle(HttpServletRequest req,
            HttpServletResponse res, HttpSession ses)
    throws ServletException, IOException {
        try {
            FileUploader up = new FileUploader();
            String uploadPath = getSessionString(ses, UPLOAD_PATH);
            HashMap map = up.processUpload(req, uploadPath);
            String type = up.getValue(map, "type");
            String action = up.getValue(map, "action");
            String file = "";
            Iterator itr = map.values().iterator();
            while (itr.hasNext()) {
                UploadData data = (UploadData) itr.next();
                if (data.name.startsWith("file") && data.isFile) {
                    file = uploadPath + data.value;
                    break;
                }
            }
            logger.debug("type: " + type + ", action: " + action + ", file: " + file );
            forward("/serv?type=title.publish&action=" + action + "&file=" + file, req, res);
            return;
        } catch (ServletException ie) {
            req.setAttribute(SES_ERROR, "ServletException occurred - " + ie);
            logger.error("TitleUpload - ServletException occurred - " + ie);
            ie.printStackTrace();
        } catch (FileNotFoundException ie) {
            req.setAttribute(SES_ERROR, "FileNotFoundException occurred - " + ie);
            logger.error("TitleUpload - FileNotFoundException occurred - " + ie);
            ie.printStackTrace();
        } catch (IOException ie) {
            req.setAttribute(SES_ERROR, "IOException occurred - " + ie);
            logger.error("TitleUpload - IOException occurred - " + ie);
            ie.printStackTrace();
        }
        forward(TITLE_LIST_JSP, req, res);
        return;
    }

    private String getSessionString(HttpSession ses, String param) {
        Object obj = ses.getAttribute(param);
        String value = null;
        if (obj != null)
            value = obj.toString().trim();
        return value;
    }
}

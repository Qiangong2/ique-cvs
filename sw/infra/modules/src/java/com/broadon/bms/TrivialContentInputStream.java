package com.broadon.bms;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.broadon.util.HexString;

public class TrivialContentInputStream extends ContentInputStream
{
    
    private MessageDigest md;

    public TrivialContentInputStream(InputStream in) throws NoSuchAlgorithmException {
        super(in);
        md = MessageDigest.getInstance("MD5");
    }
    
    public TrivialContentInputStream(byte[] data) throws NoSuchAlgorithmException {
        this(new ByteArrayInputStream(data));
    }

    public int read() throws IOException
    {
        byte[] b = new byte[1];
        int n;
        
        while ((n = read(b, 0, b.length)) == 0);
        
        if (n == 1)
            return (int)b[0];
        else
            return -1;
    }

    public int read(byte[] b, int off, int len) throws IOException
    {
        int n = super.read(b, off, len);
        if (n > 0) {
            originalSize += n;
            md.update(b, off, n);
        } else if (n < 0)
            md5HashString = HexString.toHexString(md.digest());
        filteredSize += n;
        return n;
    }

    public int read(byte[] b) throws IOException
    {
        return read(b, 0, b.length);
    }
    
/*    
    public static void main(String[] args) {
        try {
            FileInputStream infile = new FileInputStream(args[0]);     
            TrivialContentInputStream inStream = new TrivialContentInputStream(infile);
            FileOutputStream outfile = new FileOutputStream(args[1]);
            byte[] buf = new byte[16*1024];
            int n;
            while ((n = inStream.read(buf)) >= 0) {
                outfile.write(buf, 0, n);
            }
            inStream.close();
            outfile.close();
            System.out.println("original size = " + inStream.originalSize);
            System.out.println("md5sum = " + HexString.toHexString(inStream.getMd5Hash()));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    */
}

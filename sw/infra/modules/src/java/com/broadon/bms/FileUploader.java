package com.broadon.bms;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.DiskFileUpload;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.log4j.Logger;

/**
 * a servlet to handle upload request.<br>
 * refer to http://www.rfc.net/rfc1867.html
 * 
 */
public class FileUploader {
    private static Logger logger = Logger.getLogger(FileUploader.class.getName());
    private final static String UPLOADED = "Uploaded";
    private final static String TMP_DIR = "/tmp";
    private final static int BUFFER_SIZE = 32768; // 32K
    // private final static long MAX_SIZE = 104857600; // 100M
    private final static long MAX_SIZE = -1; // No Maximum

    public HashMap processUpload(HttpServletRequest req, String uploadPath)
            throws ServletException, IOException {
        HashMap map = new HashMap();
        String client = null;
        try {
            DiskFileUpload upload = new DiskFileUpload();
            upload.setSizeMax(MAX_SIZE);
            upload.setSizeThreshold(BUFFER_SIZE);
            upload.setRepositoryPath(TMP_DIR);
            List items = upload.parseRequest(req);
            Iterator iter = items.iterator();
            while (iter.hasNext()) {
                FileItem item = (FileItem) iter.next();
                
                if (item.isFormField()) {
                    //logger.debug(item.getFieldName()+ ": " + item.getString());
                    appendValue(map, item.getFieldName(), item.getString());
                } else {
                    String fileName = item.getName();
                    logger.debug(item.getFieldName()+ ": " + fileName);
                    if (fileName != null && !fileName.equals("")) {
                        if (fileName.indexOf("/") >= 0)
                            fileName = fileName.substring(fileName
                                    .lastIndexOf("/") + 1);
                        else if (fileName.indexOf("\\") >= 0)
                            fileName = fileName.substring(fileName
                                    .lastIndexOf("\\") + 1);
                        File uploadedFile = new File(uploadPath,
                                fileName);
                        logger.debug("path: " + uploadedFile.getAbsolutePath());                        
                        item.write(uploadedFile);
                        appendValue(map, item.getFieldName(), fileName, item
                                .getContentType(), item.getSize(), UPLOADED);
                    }
                }
            }
        } catch (FileUploadException ue) {
            throw new IOException("FileUploadException occured: "
                    + ue.toString());
        } catch (Exception e) {
            throw new IOException("Exception occured: " + e.toString());
        }
        return map;
    }

    void appendValue(HashMap map, String name, String value,
            String contentType, long size, String status) {
        UploadData data = new UploadData(name, value, contentType, size, true,
                status);
        map.put(name, data);
    }

    void appendValue(HashMap map, String name, String value) {
        UploadData data = new UploadData(name, value, null, 0, false, null);
        map.put(name, data);
    }

    String getValue(HashMap map, String name) {
        UploadData data = (UploadData) map.get(name);
        if (data == null)
            return null;
        return data.value;
    }
}

class UploadData {
    String name;
    String value;
    String contentType;
    long size;
    boolean isFile;
    String status;

    UploadData(String name, String value, String contentType, long size,
            boolean isFile, String status) {
        this.name = name;
        this.value = value;
        this.contentType = contentType;
        this.size = size;
        this.isFile = isFile;
        this.status = status;
    }
}

/*
 * (C) 2004, BroadOn Communications Corp.,
 * $Id: CommonServlet.java,v 1.7 2006/08/17 23:24:19 zz Exp $
 */
package com.broadon.bms.common;

import java.lang.Integer;
import java.lang.reflect.*;
import java.io.*;
import java.sql.*;
import java.util.Properties;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.sql.DataSource;

import org.w3c.dom.*;
import org.apache.xerces.parsers.DOMParser;
import org.xml.sax.SAXException;
import org.xml.sax.InputSource;

import org.apache.log4j.Logger;

import oracle.jdbc.OracleConnectionWrapper;

import com.broadon.servlet.ServletConstants;
import com.broadon.xml.XMLOutputTransformer;
import com.broadon.db.OracleDataSource;
import com.broadon.bms.Attributes;

/**
 * The <code>CommonServlet</code> class is a generic servlet which functions based
 * two parameters that must be present in the incoming request - type and action
 *
 *<p>
 * The type parameter instructs the servlet to look for the matching properties
 * in the session to dictate the response. Based on the "action", appropriate function
 * calls are made before forwarding the request to a JSP file.
 *
**/
public class CommonServlet extends AdminServlet implements ServletConstants
{
    private static final String CURRENT_LOCALE = "CURRENT_LOCALE";
    private static final String COMMON_PROP_KEY = "COMMON_PROP";
    private static final String JSP_LOCATION = "JSP_LOCATION";
    private static final String FORBIDDEN_JSP = "forbidden.jsp";
    private static final String SES_UPDATE_FAILED = "ERR_NOT_UPDATED";
    private static final String SES_INSERT_FAILED = "ERR_NOT_INSERTED";

    private static final String REQUEST_TYPE = "type";
    private static final String REQUEST_ACTION = "action";

    private static final String ACTION = "action";
    private static final String AND = "and";
    private static final String BIND = "bind";
    private static final String CLASS = "class";
    private static final String COND = "condition";
    private static final String CONFIG = "config";
    private static final String COUNT = "count";
    private static final String DB = "db";
    private static final String DOT = ".";
    private static final String ERROR = "error";
    private static final String FORWARD = "forward";
    private static final String FUNCTION = "function";
    private static final String HTML = "html";
    private static final String ID = "id";
    private static final String INTEGER = "integer";
    private static final String LEFT = "left";
    private static final String LOOP = "loop";
    private static final String NAME = "name";
    private static final String NEXTID = "nextid";
    private static final String NO = "no";
    private static final String OP = "operator";
    private static final String OR = "or";
    private static final String PARAM = "param";
    private static final String REQUEST = "request";
    private static final String REQUIRED = "required";
    private static final String RESULT = "result";
    private static final String RIGHT = "right";
    private static final String ROLES = "roles";
    private static final String SESSION = "session";
    private static final String SETKEY = "setkey";
    private static final String SQL = "sql";
    private static final String SUCCESS = "success";
    private static final String STRING = "string";
    private static final String TABLE = "table";
    private static final String TAG = "tag";
    private static final String TEXT = "text";
    private static final String TYPE = "type";
    private static final String VALUE = "value";
    private static final String XML = "xml";
    private static final String XSL = "xsl";
    private static final String YES = "yes";

    private static final String EQ = "equal";
    private static final String GT = "greater than";
    private static final String GTE = "greater than equal";
    private static final String LT = "less than";
    private static final String LTE = "less than equal";
    private static final String NEQ = "not equal";

    private static final String ADD = "add";
    private static final String EDIT = "edit";
    private static final String DELETE = "delete";
    private static final String INSERT = "insert";
    private static final String LIST = "list";
    private static final String UPDATE = "update";

    private static final String INSERT_UPDATE = "insert_update";
    private static final String DELETE_INSERT = "delete_insert";

    private static Logger logger;
    private static String locale;
    private static String queryStr;
    private static ServletContext context;
    private static Properties serverProps;

    public void init (ServletConfig config) throws ServletException
    {

        logger = Logger.getLogger(CommonServlet.class.getName());
        context = config.getServletContext();
        serverProps = (Properties) context.getAttribute(PROPERTY_KEY);
        
        try
        {
            super.init(config);

            if (mAdminProp!=null)
                mLabelsFile = mAdminProp.getProperty(COMMON_PROP_KEY);
            // Init Attribute Lookups
            Attributes.init(super.dataSource, "en_US");
             
        } catch (ServletException e) {
            throw (e);
        }
    }
    
    public void doGet(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException, ServletException
    {  
        HttpSession ses = req.getSession(false);
        if (ses == null) {
            logger.warn("NO SESSION.");
            return;
        }
                
        String type = getRequestParameter(req, REQUEST_TYPE);
        String action = getRequestParameter(req, REQUEST_ACTION);

        logger.info("["+type+"]["+action+"] - New Request");

        if (!"login".equals(type) && !"logout".equals(type) && !doAuthenticate(req, res)) {
            logger.info("["+type+"]["+action+"] - Cannot Authenticate. Session seems to have expired.");
            return;
        }

        if ((type==null || type.equals("")) || (action==null || action.equals(""))) {
            logger.error("["+type+"]["+action+"] - Cannot process the request, invalid type and/or action, " +
                         "forwarding to forbidden page.");
            if (queryStr!=null && !queryStr.equals(""))
                ses.setAttribute(SES_LOCATION, req.getServletPath()+"?"+queryStr);
            forwardToModule(locale, JSP_LOCATION, getSessionString(ses, FORBIDDEN_JSP), req, res);
            logger.info("["+type+"]["+action+"] - End Request\n");
            return;
        }
        
        queryStr = req.getQueryString();
        
        locale = getSessionString(ses, CURRENT_LOCALE);
        if (locale == null || locale.equals("")) {
            locale = getLocale(req);
            if (locale!=null && locale.toLowerCase().startsWith("ja"))
                locale = "ja_JP";
            else
                locale = "en_US";            
        }  
        storeLabels(locale, mLabelsFile, ses);
        locale = "/" + locale;
        
        int roleCount = getSessionInteger(ses, type+DOT+action+DOT+ROLES+DOT+COUNT);    
        int role = getSessionInteger(ses, SES_ROLE);    
        boolean allow = false;
        
        if (roleCount > 0) 
        {
            for (int i = 1; i <= roleCount; i++)
            {
                if (role==getSessionInteger(ses, type+DOT+action+DOT+ROLES+DOT+Integer.toString(i)))
                {
                    allow = true;
                    break;
                }
            }
        } else
            allow = true;
 
        if (allow) {
            logger.info("["+type+"]["+action+"] - Procesing request for role level: " + Integer.toString(role));
            processRequest(req, res, ses, type, action);
        } else {
            logger.info("["+type+"]["+action+"] - Role level ["+Integer.toString(role)+"] not permitted to process request, " +
                         "forwarding to forbidden page.");
            if (queryStr!=null && !queryStr.equals(""))
                ses.setAttribute(SES_LOCATION, req.getServletPath()+"?"+queryStr);
            forwardToModule(locale, JSP_LOCATION, getSessionString(ses, FORBIDDEN_JSP), req, res);
        }
           
        logger.info("["+type+"]["+action+"] - End Request\n");
        return;
    }
    
    public void doPost(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException, ServletException
    {
        doGet(req, res);            
        return;
    }

    private void processRequest (HttpServletRequest req, HttpServletResponse res, HttpSession ses,
                                 String type, String action)
        throws IOException, ServletException
    {

        req.setAttribute(SES_ERROR, "");
        req.setAttribute(SES_SUCCESS, "");

        if (action.toLowerCase().equals(LIST))
            processList(req, res, ses, type, action);
        else if (action.toLowerCase().equals(ADD) || action.toLowerCase().equals(EDIT))
            processForm(req, res, ses, type, action);
        else if (action.toLowerCase().equals(INSERT) || action.toLowerCase().equals(UPDATE) ||
                action.toLowerCase().equals(DELETE))
            processSubmit(req, res, ses, type, action);
        else {
            logger.error("["+type+"]["+action+"] - Invalid request, forwarding to forbidden page.");
            if (queryStr!=null && !queryStr.equals(""))
                ses.setAttribute(SES_LOCATION, req.getServletPath()+"?"+queryStr);
            forwardToModule(locale, JSP_LOCATION, getSessionString(ses, FORBIDDEN_JSP), req, res);
        }
        return;
    }

    private void processList (HttpServletRequest req, HttpServletResponse res, HttpSession ses, 
                              String type, String action)
        throws IOException, ServletException
    {
        String sort = storeParameter(req, ses, getSessionString(ses, type+DOT+action+DOT+"sort.param"), (type+"_SORT").toUpperCase());
        if (sort == null || sort.equals(""))
                sort = getSessionString(ses, type+DOT+action+DOT+"sort.default");

        String page = getRequestParameter(req, getSessionString(ses, "list.page.param"));
        if (page == null || page.equals(""))
                page = "1";
       
        int size = getSessionInteger(ses, type+DOT+action+DOT+"page.size");
        if (size < 0) 
            size = PAGE_SIZE;
        else if (size == 0)
            page = "-1";

        QueryBean qb = new QueryBean(size, page);
        qb.setSort(sort);

        String orderBy = "";
        if (sort != null && !sort.equals(""))
        {
            if (sort.endsWith("_d"))
                orderBy = " ORDER BY " + sort.substring(0, sort.lastIndexOf("_d")) + " DESC";
            else
                orderBy = " ORDER BY " + sort;
        }

        String[] xml = null;
        String[] html = null;
        int[] count = null;

        int sesCount = getSessionInteger(ses, type+DOT+action+DOT+SESSION+DOT+PARAM+DOT+COUNT);
        if (sesCount > 0)
        {
            for (int j = 1; j <= sesCount; j++)
            {
                String pName = getSessionString(ses, type+DOT+action+DOT+SESSION+DOT+PARAM+DOT+Integer.toString(j)+DOT+NAME);
                String pValue = getSessionString(ses, type+DOT+action+DOT+SESSION+DOT+PARAM+DOT+Integer.toString(j)+DOT+VALUE);

                String vFrom = pValue.substring(0, pValue.indexOf(",")).trim();
                String vParam = pValue.substring(pValue.indexOf(",")+1).trim();
                
                if (vFrom!=null && vFrom.toLowerCase().equals(REQUEST)) {
                    String vValue = getRequestParameter(req, vParam);
                    if (vValue != null)
                        ses.setAttribute(pName, vValue);
                } else if (vFrom!=null && vFrom.toLowerCase().equals(SESSION)) {
                    String vValue = getSessionString(ses, vParam);
                    if (vValue != null)
                        ses.setAttribute(pName, vValue);
                } else if (vFrom!=null && vFrom.toLowerCase().equals(CONFIG)) {
                    String vValue = getConfigString(vParam);
                    if (vValue != null)
                        ses.setAttribute(pName, vValue);
                }
            }
        }
        
        int sCount = getSessionInteger(ses, type+DOT+action+DOT+SQL+DOT+COUNT);
        int cCount = getSessionInteger(ses, type+DOT+action+DOT+COUNT+DOT+COUNT);
 
        Connection wrappedConnection = null;
	Connection conn = null;
 
        try
        {
            DataSource ds = super.dataSource;
            conn = wrappedConnection = ds.getConnection();
	    if (conn instanceof OracleDataSource.ConnectionWrapper) {
		conn = ((OracleConnectionWrapper) conn).unwrap();
	    }
            conn.setAutoCommit(false);
            conn.setReadOnly(true);

            if (sCount > 0)
            {
                xml = new String[sCount];
                html = new String[sCount];

                for (int i = 1; i <= sCount; i++)
                {
                    String sql = getSessionString(ses, type+DOT+action+DOT+SQL+DOT+Integer.toString(i));
                    String sqlOrderBy = orderBy;
                    
                    if (sql != null && !sql.equals("")) 
                    {
                        sql = bindParameter(req, ses, xml, type, action, sql, SQL, i);
			
                        if (sql != null && !sql.equals("")) 
                        {
                            if (!(sql.toUpperCase().indexOf("ORDER BY") < 0)) {
                                page = "-1";
                                sqlOrderBy = "";
                            }
    
                            sql = "SELECT rownum no, inner.* FROM (" + sql + sqlOrderBy +") inner";
                            logger.info("["+type+"]["+action+"] - SQL: "+sql);
    
                            if (Integer.parseInt(page) < 0)
                                xml[i-1] = qb.query(sql, conn);
                            else {
                                logger.debug("["+type+"]["+action+"] - Rows="+Integer.toString(size)+
                                             ", Rows to Skip="+Integer.toString(size*(Integer.parseInt(page)-1)));
                                xml[i-1] = qb.query(sql, size, size*(Integer.parseInt(page)-1), conn);
                            }
                        } else
                            xml[i-1] = "<ROWSET><ROW num=\"1\"></ROW></ROWSET>";
                            
                    } else {
                        
                        xml[i-1] = "<ROWSET><ROW num=\"1\"></ROW></ROWSET>";
                        
                        String funcName = getSessionString(ses, type+DOT+action+DOT+SQL+DOT+Integer.toString(i)+DOT+FUNCTION);
                        String className = getSessionString(ses, type+DOT+action+DOT+SQL+DOT+Integer.toString(i)+DOT+CLASS);
                        
                        if (funcName != null && !funcName.equals("") &&
                                className != null && !className.equals("")) {
                            logger.info("Invoke function " + className + "." + funcName);
                            
                            String param[] = formListFunctionParamArray(req, ses, xml, html, type, action, i);
                            Class cls = Class.forName(className);
                            Method[] methods = cls.getMethods();

                            for (int n=0; n<methods.length; n++) {

                                 if (methods[n].getName().equals(funcName)) {
                                     Object funcResult = methods[n].invoke(cls.newInstance(), new Object[] {context, req, res, qb, conn, param});
                                     xml[i-1] = funcResult.toString();
                                     break;
                                 }
                            }
                        }                       
                    }
 
                    String xslFile = getSessionString(ses, type+DOT+action+DOT+XSL+DOT+Integer.toString(i));
                    if (xslFile!=null && !xslFile.equals(""))
                    {
                        ServletContext sc=getServletContext();
                        String xslPath = sc.getRealPath(locale+xslFile);
                        String xslStr = readFile(xslPath);
                        XMLOutputTransformer xt = new XMLOutputTransformer();

                        int xpCount = getSessionInteger(ses, type+DOT+action+DOT+XSL+DOT+Integer.toString(i)+DOT+PARAM+DOT+COUNT);
                        if (xpCount > 0)
                        {
                            for (int j = 1; j <= xpCount; j++)
                            {
                                String xslID = getSessionString(ses, type+DOT+action+DOT+XSL+DOT+Integer.toString(i)+DOT+PARAM+DOT+Integer.toString(j)+DOT+ID);
                                String pName = getSessionString(ses, type+DOT+action+DOT+XSL+DOT+Integer.toString(i)+DOT+PARAM+DOT+Integer.toString(j)+DOT+NAME);
                                String pValue = getSessionString(ses, type+DOT+action+DOT+XSL+DOT+Integer.toString(i)+DOT+PARAM+DOT+Integer.toString(j)+DOT+VALUE);
                                xslStr = xt.setStyleSheetParam(xslStr, xslID, getParamValue(req, ses, xml, html, pName, pValue));
                            } 
                        }
                        xt.setSystemId(sc.getRealPath("")+"/");
                        xt.setStyleSource(xslStr);
                        html[i-1] = xt.transform(xml[i-1]);
                    } else
                        html[i-1] = null;
                }

                qb.setXMLResultArray(xml);
                qb.setHTMLResultArray(html);
            } 

            if (cCount > 0) 
            {
                count = new int[cCount];

                for (int i = 1; i <= cCount; i++)
                {
                    String sql = getSessionString(ses, type+DOT+action+DOT+COUNT+DOT+Integer.toString(i));
                    if (sql != null && !sql.equals("")) 
                    {
                        sql = bindParameter(req, ses, xml, type, action, sql, COUNT, i);
                        
                        if (sql != null && !sql.equals("")) 
                        {
                            logger.info("["+type+"]["+action+"] - Count SQL: " + sql);
                            count[i-1] = qb.count(sql, conn);
                        } else
                            count[i-1] = -1;
                    } else
                        count[i-1] = -1;
                }

                qb.setCountArray(count);
            }
                    
            conn.commit();

        } catch (SQLException se) {
            req.setAttribute(SES_ERROR, "SQLException occurred - " + se);
            logger.error("["+type+"]["+action+"] - SQLException occurred - " + se);
            se.printStackTrace();
            try {
                conn.rollback();
            } catch (SQLException s) {
                s.printStackTrace();
            }
        } catch (IOException ie) {
            req.setAttribute(SES_ERROR, "IOException occurred - " + ie);
            logger.error("["+type+"]["+action+"] - IOException occurred - " + ie);
            ie.printStackTrace();
            try {
                conn.rollback();
            } catch (SQLException s) {
                s.printStackTrace();
            }
        } catch (SAXException sx) {
            req.setAttribute(SES_ERROR, "SAXException occurred - " + sx);
            logger.error("["+type+"]["+action+"] - SAXException occurred - " + sx);
            sx.printStackTrace();
            try {
                conn.rollback();
            } catch (SQLException s) {
                s.printStackTrace();
            }
        } catch (IllegalAccessException ex) {
            req.setAttribute(SES_ERROR, "Exception occurred - Failed to invoke function " + ex);
            logger.error("["+type+"]["+action+"] - Exception occurred, Failed to invoke function " + ex);
            ex.printStackTrace();
            try {
                conn.rollback();
            } catch (SQLException s) {
                s.printStackTrace();
            }
        } catch (InvocationTargetException ex) {
            req.setAttribute(SES_ERROR, "Exception occurred - Failed to invoke function " + ex);
            logger.error("["+type+"]["+action+"] - Exception occurred, Failed to invoke function " + ex);
            ex.printStackTrace();
            try {
                conn.rollback();
            } catch (SQLException s) {
                s.printStackTrace();
            }
        } catch (ClassNotFoundException ex) {
            req.setAttribute(SES_ERROR, "Exception occurred - Failed to invoke function " + ex);
            logger.error("["+type+"]["+action+"] - Exception occurred, Failed to invoke function " + ex);
            ex.printStackTrace();
            try {
                conn.rollback();
            } catch (SQLException s) {
                s.printStackTrace();
            }
        } catch (InstantiationException ex) {
            req.setAttribute(SES_ERROR, "Exception occurred - Failed to invoke function " + ex);
            logger.error("["+type+"]["+action+"] - Exception occurred, Failed to invoke function " + ex);
            ex.printStackTrace();
            try {
                conn.rollback();
            } catch (SQLException s) {
                s.printStackTrace();
            }
        } finally {
            try {
                wrappedConnection.close();
            } catch (SQLException s) {
                s.printStackTrace();
            }
        }

        try
        {
            int pCount = getSessionInteger(ses, type+DOT+action+DOT+PARAM+DOT+COUNT);
            if (pCount > 0)
                qb.setParamArray(formParamArray(req, ses, xml, html, type, action, pCount));
        } catch (SAXException sx) {
            req.setAttribute(SES_ERROR, "SAXException occurred - " + sx);
            logger.error("["+type+"]["+action+"] - SAXException occurred - " + sx);
            sx.printStackTrace();
        }

        if (req.getAttribute(SES_ERROR)==null)
            req.setAttribute(SES_ERROR, "");

        req.setAttribute(QueryBean.ID, qb);
        if (queryStr!=null && !queryStr.equals(""))
            ses.setAttribute(SES_LOCATION, req.getServletPath()+"?"+queryStr);
        forwardToModule(locale, JSP_LOCATION, getSessionString(ses, type+DOT+action+DOT+"jsp"), req, res);
        return;
    }

    private void processForm (HttpServletRequest req, 
			      HttpServletResponse res, 
			      HttpSession ses, 
                              String type, 
			      String action)
        throws IOException, ServletException
    {
        QueryBean qb = new QueryBean(PAGE_SIZE);

        String[] xml = null;
        String[] html = null;
        int count[] = null;
        
        int sesCount = getSessionInteger(ses, type+DOT+action+DOT+SESSION+DOT+PARAM+DOT+COUNT);
        if (sesCount > 0)
        {
            for (int j = 1; j <= sesCount; j++)
            {
                String pName = getSessionString(ses, type+DOT+action+DOT+SESSION+DOT+PARAM+DOT+Integer.toString(j)+DOT+NAME);
                String pValue = getSessionString(ses, type+DOT+action+DOT+SESSION+DOT+PARAM+DOT+Integer.toString(j)+DOT+VALUE);

                String vFrom = pValue.substring(0, pValue.indexOf(",")).trim();
                String vParam = pValue.substring(pValue.indexOf(",")+1).trim();
                
                if (vFrom!=null && vFrom.toLowerCase().equals(REQUEST)) {
                    String vValue = getRequestParameter(req, vParam);
                    if (vValue != null)
                        ses.setAttribute(pName, vValue);
                } else if (vFrom!=null && vFrom.toLowerCase().equals(SESSION)) {
                    String vValue = getSessionString(ses, vParam);
                    if (vValue != null)
                        ses.setAttribute(pName, vValue);
                } else if (vFrom!=null && vFrom.toLowerCase().equals(CONFIG)) {
                    String vValue = getConfigString(vParam);
                    if (vValue != null)
                        ses.setAttribute(pName, vValue);
                }
            }
        }
        
        int sCount = getSessionInteger(ses, type+DOT+action+DOT+SQL+DOT+COUNT);
        int cCount = getSessionInteger(ses, type+DOT+action+DOT+COUNT+DOT+COUNT);
 
        Connection wrappedConnection = null;
	Connection conn = null;
 
        try
        {
            DataSource ds = super.dataSource;
            conn = wrappedConnection = ds.getConnection();
	    if (conn instanceof OracleDataSource.ConnectionWrapper) {
		conn = ((OracleConnectionWrapper) conn).unwrap();
	    }
            conn.setAutoCommit(false);
            conn.setReadOnly(true);

            if (sCount > 0)
            {
                xml = new String[sCount];
                html = new String[sCount];

                for (int i = 1; i <= sCount; i++)
                {
                    String sql = getSessionString(ses, type+DOT+action+DOT+SQL+DOT+Integer.toString(i));
                    if (sql != null && !sql.equals("")) 
                    {
                        sql = bindParameter(req, ses, xml, type, action, sql, SQL, i);
                        if (sql != null && !sql.equals("")) 
                        {
                            logger.info("["+type+"]["+action+"] - SQL: "+sql);
                            xml[i-1] = qb.query(sql, conn);
                        } else
                            xml[i-1] = "<ROWSET><ROW num=\"1\"></ROW></ROWSET>";
                    } else                        
                        xml[i-1] = "<ROWSET><ROW num=\"1\"></ROW></ROWSET>";

                    String xslFile = getSessionString(ses, type+DOT+action+DOT+XSL+DOT+Integer.toString(i));
                    if (xslFile!=null && !xslFile.equals(""))
                    {
                        ServletContext sc=getServletContext();
                        String xslPath = sc.getRealPath(locale+xslFile);
                        String xslStr = readFile(xslPath);
                        XMLOutputTransformer xt = new XMLOutputTransformer();

                        int xpCount = getSessionInteger(ses, type+DOT+action+DOT+XSL+DOT+Integer.toString(i)+DOT+PARAM+DOT+COUNT);
                        if (xpCount > 0)
                        {
                            for (int j = 1; j <= xpCount; j++)
                            {
                                String xslID = getSessionString(ses, type+DOT+action+DOT+XSL+DOT+Integer.toString(i)+DOT+PARAM+DOT+Integer.toString(j)+DOT+ID);
                                String pName = getSessionString(ses, type+DOT+action+DOT+XSL+DOT+Integer.toString(i)+DOT+PARAM+DOT+Integer.toString(j)+DOT+NAME);
                                String pValue = getSessionString(ses, type+DOT+action+DOT+XSL+DOT+Integer.toString(i)+DOT+PARAM+DOT+Integer.toString(j)+DOT+VALUE);
                                xslStr = xt.setStyleSheetParam(xslStr, xslID, getParamValue(req, ses, xml, html, pName, pValue));
                            } 
                        }
                        xt.setSystemId(sc.getRealPath("")+"/");
                        xt.setStyleSource(xslStr);
                        html[i-1] = xt.transform(xml[i-1]);
                    } else
                        html[i-1] = null;
                }

                qb.setXMLResultArray(xml);
                qb.setHTMLResultArray(html);
            }

            if (cCount > 0) 
            {
                count = new int[cCount];

                for (int i = 1; i <= cCount; i++)
                {
                    String sql = getSessionString(ses, type+DOT+action+DOT+COUNT+DOT+Integer.toString(i));
                    if (sql != null && !sql.equals("")) 
                    {
                        sql = bindParameter(req, ses, xml, type, action, sql, COUNT, i);
                        
                        if (sql != null && !sql.equals(""))
                        {
                            logger.info("["+type+"]["+action+"] - Count SQL: " + sql);
                            count[i-1] = qb.count(sql, conn);
                        } else
                            count[i-1] = -1;
                    } else
                        count[i-1] = -1;
                }

                qb.setCountArray(count);
            }
            
            conn.commit();

        } catch (SQLException se) {
            req.setAttribute(SES_ERROR, "SQLException occurred - " + se);
            logger.error("["+type+"]["+action+"] - SQLException occurred - " + se);
            se.printStackTrace();
            try {
                conn.rollback();
            } catch (SQLException s) {
                s.printStackTrace();
            }
        } catch (IOException ie) {
            req.setAttribute(SES_ERROR, "IOException occurred - " + ie);
            logger.error("["+type+"]["+action+"] - IOException occurred - " + ie);
            ie.printStackTrace();
            try {
                conn.rollback();
            } catch (SQLException s) {
                s.printStackTrace();
            }
        } catch (SAXException sx) {
            req.setAttribute(SES_ERROR, "SAXException occurred - " + sx);
            logger.error("["+type+"]["+action+"] - SAXException occurred - " + sx);
            sx.printStackTrace();
            try {
                conn.rollback();
            } catch (SQLException s) {
                s.printStackTrace();
            }
        } finally {
            try {
                wrappedConnection.close();
            } catch (SQLException s) {
                s.printStackTrace();
            }
        }

        try
        {
            int pCount = getSessionInteger(ses, type+DOT+action+DOT+PARAM+DOT+COUNT);
            if (pCount > 0)
                qb.setParamArray(formParamArray(req, ses, xml, html, type, action, pCount));
        } catch (SAXException sx) {
            req.setAttribute(SES_ERROR, "SAXException occurred - " + sx);
            logger.error("["+type+"]["+action+"] - SAXException occurred - " + sx);
            sx.printStackTrace();
        }

        if (req.getAttribute(SES_ERROR)==null)
            req.setAttribute(SES_ERROR, "");

        req.setAttribute(QueryBean.ID, qb);
        if (queryStr!=null && !queryStr.equals(""))
            ses.setAttribute(SES_LOCATION, req.getServletPath()+"?"+queryStr);
        forwardToModule(locale, JSP_LOCATION, getSessionString(ses, type+DOT+action+DOT+"jsp"), req, res);
        return;
    }

    private void processSubmit (HttpServletRequest req, HttpServletResponse res, HttpSession ses, 
                                String type, String action)
        throws IOException, ServletException
    {
        QueryBean qb = new QueryBean(PAGE_SIZE);
        String result = "PASS";
        String[] xml = null;
        int rows[];
        int flag = 0;
        req.setAttribute(SES_ERROR, "");
        req.setAttribute(SES_SUCCESS, "");
        
        int aCount = getSessionInteger(ses, type+DOT+action+DOT+COUNT);
        int sCount = getSessionInteger(ses, type+DOT+action+DOT+SQL+DOT+COUNT);
        
        if (aCount > 0)
        {
            rows = new int[aCount];
            Connection wrappedConnection = null;
            Connection conn = null;
 
            try
            {
                DataSource ds = super.dataSource;
                conn = wrappedConnection = ds.getConnection();
		if (conn instanceof OracleDataSource.ConnectionWrapper) {
		    conn = ((OracleConnectionWrapper) conn).unwrap();
		}
                conn.setAutoCommit(false);
                conn.setReadOnly(false);

                if (sCount > 0)
                {
                    xml = new String[sCount];

                    for (int i = 1; i <= sCount; i++)
                    {
                        String sql = getSessionString(ses, type+DOT+action+DOT+SQL+DOT+Integer.toString(i));
                        if (sql != null && !sql.equals("")) 
                        {
                            sql = bindParameter(req, ses, xml, type, action, sql, SQL, i);
                            if (sql != null && !sql.equals("")) 
                            {
                                logger.info("["+type+"]["+action+"] - SQL: "+sql);
                                xml[i-1] = qb.query(sql, conn);
                            } else
                                xml[i-1] = "<ROWSET><ROW num=\"1\"></ROW></ROWSET>";
                        } else 
                            xml[i-1] = "<ROWSET><ROW num=\"1\"></ROW></ROWSET>";                        
                    }
                }
                
                for (int i = 1; i <= aCount; i++)
                {   
                    result = "PASS";
                    String lParam = getSessionString(ses, type+DOT+action+DOT+Integer.toString(i)+DOT+LOOP+DOT+PARAM);
                    String lValue = getSessionString(ses, type+DOT+action+DOT+Integer.toString(i)+DOT+LOOP+DOT+VALUE);

                    int cCount = getSessionInteger(ses, type+DOT+action+DOT+Integer.toString(i)+DOT+COND+DOT+COUNT);
                    if (cCount > 0 && (lParam==null || lParam.equals("")))
                       result = getConditionResult(req, ses, qb, conn, type, action, rows, cCount, String.valueOf(i), "");

                    if (result!=null && result.equals("PASS"))
                    {
                        String sType = getSessionString(ses, type+DOT+action+DOT+Integer.toString(i)+DOT+TYPE);

                        if (sType!=null && sType.toLowerCase().equals(DB))
                        {
                            int l = 1;
                            if (lValue!=null && lValue.toLowerCase().equals(REQUEST))
                            {
                                 String tmp = getRequestParameter(req, lParam);
                                 if (tmp!=null && !tmp.equals(""))
                                     l = Integer.parseInt(tmp);
                            } else if (lValue!=null && lValue.toLowerCase().equals(SESSION)) {
                                 l = getSessionInteger(ses, lParam);
                                 if (l < 0)
                                     l = 1;
                            } else if (lValue!=null && lValue.toLowerCase().equals(CONFIG)) {
                                l = getConfigInteger(lParam);
                                if (l < 0)
                                    l = 1;
                            } else if (lParam!=null && lParam.equals("TEXT"))
                                 l = Integer.parseInt(lValue);

                            String tableName = getSessionString(ses, type+DOT+action+DOT+Integer.toString(i)+DOT+TABLE);
                            logger.info("tableName: " + tableName);
                            
                            int pCount = getSessionInteger(ses, type+DOT+action+DOT+Integer.toString(i)+DOT+PARAM+DOT+COUNT);
                            int qIndex = getSessionInteger(ses, type+DOT+action+DOT+Integer.toString(i)+DOT+XML);  
                            String className = getSessionString(ses, type+DOT+action+DOT+Integer.toString(i)+DOT+CLASS);
                            
                            String xmlData = "";
                            
                            if (pCount > 0 && (tableName!=null && !tableName.equals("")))
                            {
                                xmlData = "<ROWSET>";

                                for (int j = 1; j <= l; j++)
                                {
                                    String lResult = "PASS";
                                    if (cCount > 0 && lParam!=null && !lParam.equals(""))
                                        lResult = getConditionResult(req, ses, qb, conn, type, action, rows, cCount, String.valueOf(i), String.valueOf(j));

                                    if (lResult!=null && lResult.equals("PASS"))
                                    {
                                        String rowData = "<ROW num=\""+String.valueOf(j)+"\">";
                                        int valid = 1;

                                        for (int k = 1; k <= pCount; k++)
                                        {
                                            String pName = getSessionString(ses, type+DOT+action+DOT+Integer.toString(i)+DOT+PARAM+DOT+Integer.toString(k)+DOT+NAME);  
                                            String pValue = getSessionString(ses, type+DOT+action+DOT+Integer.toString(i)+DOT+PARAM+DOT+Integer.toString(k)+DOT+VALUE);  
                                            String pTag = getSessionString(ses, type+DOT+action+DOT+Integer.toString(i)+DOT+PARAM+DOT+Integer.toString(k)+DOT+TAG);  
                                            String pReq = getSessionString(ses, type+DOT+action+DOT+Integer.toString(i)+DOT+PARAM+DOT+Integer.toString(k)+DOT+REQUIRED);  
                                            String pLoop = getSessionString(ses, type+DOT+action+DOT+Integer.toString(i)+DOT+PARAM+DOT+Integer.toString(k)+DOT+LOOP);  
                                            String index = "";
                                            String value = "";
                                            
                                            logger.debug("pName: " + pName + ", pValue: " + pValue + 
                                                    ", pTag: " + pTag + ", pReq:" + pReq);
 
                                            if (pLoop!=null && pLoop.toLowerCase().equals(YES))
                                                index = Integer.toString(j);
 
                                            if (pName!=null && !pName.equals("")) {
                                                if (pValue!=null && pValue.toLowerCase().equals(REQUEST))
                                                    value = getRequestParameter(req, pName+index);
                                                else if (pValue!=null && pValue.toLowerCase().equals(SESSION))
                                                    value = getSessionString(ses, pName+index);
                                                else if (pValue!=null && pValue.toLowerCase().equals(CONFIG))
                                                    value = getConfigString(pName+index);
                                                else if (pValue!=null && pValue.toLowerCase().equals(NEXTID))
                                                    value = Long.toString(qb.getNextID(pName, tableName, conn));
                                                else if (pValue!=null && pValue.toLowerCase().startsWith(SQL)) {
                                                    int sIndex = Integer.parseInt(pValue.substring(pValue.indexOf(",")+1).trim());
                                                    String sStr = getSessionString(ses, type+DOT+action+DOT+SQL+DOT+Integer.toString(sIndex));
                                                    if (sStr != null && !sStr.equals(""))   
                                                        sStr = bindParameter(req, ses, null, type, action, sStr, SQL, sIndex);
                                                    
                                                    if (sStr != null && !sStr.equals(""))
                                                    {
                                                        logger.info("["+type+"]["+action+"] - SQL: " + sStr); 
                                                        String xStr = qb.query(sStr, conn);   
                                                        value = getValueFromXML(xStr, pName, 1);
                                                    } else
                                                        value = null;
                                                } else if (pValue!=null && pValue.toLowerCase().startsWith(XML)) {
                                                    int xIndex = Integer.parseInt(pValue.substring(pValue.indexOf(",")+1, pValue.lastIndexOf(",")).trim());
                                                    int row =  Integer.parseInt(pValue.substring(pValue.lastIndexOf(",")+1).trim());
                                                                                                              
                                                    value = getValueFromXML(xml[xIndex-1], pName, row);                                                    
                                                }
                                            } else 
                                                value = pValue;
                                            
                                            logger.debug("value: " + value);
 
                                            if (value!=null)
                                                rowData += "<"+pTag+">"+value+"</"+pTag+">";
                                            else if (pReq!=null && pReq.toLowerCase().equals(YES)) {
                                                valid = 0;
                                                break;
                                            }
                                        }

                                        if (valid == 1)
                                            xmlData += rowData + "</ROW>";
                                    }
                                }   
                                xmlData += "</ROWSET>";

                            } else if (qIndex > 0) {
                                
                                String sStr = getSessionString(ses, type+DOT+action+DOT+SQL+DOT+Integer.toString(qIndex));                                      
                                if (sStr != null && !sStr.equals(""))   
                                    sStr = bindParameter(req, ses, null, type, action, sStr, SQL, qIndex); 
                                
                                if (sStr != null && !sStr.equals(""))
                                {
                                    logger.info("["+type+"]["+action+"] - Query SQL: " + sStr);                                            
                                    xmlData = qb.query(sStr, conn);
                                } else
                                    xmlData = null;
                                
                            } else if (className != null && !className.equals("")) {

                                String funcName = getSessionString(ses, type+DOT+action+DOT+Integer.toString(i)+DOT+FUNCTION);                                
                                
                                if (funcName != null && !funcName.equals("")) {
                                    
                                    logger.info("Invoke function " + className + "." + funcName);
                                    String param[] = formSubmitFunctionParamArray(req, ses, xml, type, action, i);
                                    
                                    Class cls = Class.forName(className);
                                    Method[] methods = cls.getMethods();
                                    
                                    for (int n=0; n<methods.length; n++) {
                                        
                                        if (methods[n].getName().equals(funcName)) {
                                            Object funcResult = methods[n].invoke(cls.newInstance(), new Object[] {context, req, res, qb, conn, param});
                                            
                                            if (funcResult!=null && funcResult.toString().startsWith("FAIL")) {
                                                logger.info("["+type+"]["+action+"] - Error: " + funcResult.toString().substring(4));
                                                req.setAttribute(SES_ERROR, funcResult.toString().substring(4));
                                                conn.rollback();
                                                break;
                                            } else if (funcResult != null)
                                                req.setAttribute(SES_SUCCESS, funcResult.toString());
                                                
                                            break;
                                        }
                                    }
                                }
                            }

                            if (xmlData!=null && !xmlData.equals("") && !xmlData.equals("<ROWSET></ROWSET>")) 
                            {
                                logger.info("["+type+"]["+action+"] - XML Data: " + xmlData);

                                String todo = getSessionString(ses, type+DOT+action+DOT+Integer.toString(i)+DOT+ACTION);
                                if (todo!=null && todo.toLowerCase().equals(INSERT)) 
                                {
                                    rows[i-1] = qb.insert(xmlData, tableName, conn);
                                    if (rows[i-1] < 1 && xmlData.indexOf("</ROW>") >= 0) {
                                        req.setAttribute(SES_ERROR, getSessionString(ses, SES_INSERT_FAILED));
                                        flag = 1;
                                        conn.rollback();
                                        logger.info("["+type+"]["+action+"] - INSERT FAILED: "+Integer.toString(rows[i-1])+" rows inserted.");
                                        break;
                                    }
                                } else if (todo!=null && todo.toLowerCase().equals(UPDATE)) {
                                    rows[i-1] = qb.update(xmlData, tableName, conn);
                                    if (rows[i-1] < 0 && xmlData.indexOf("</ROW>") >= 0) {
                                        req.setAttribute(SES_ERROR, getSessionString(ses, SES_UPDATE_FAILED));
                                        flag = 1;
                                        conn.rollback();
                                        logger.info("["+type+"]["+action+"] - UPDATE FAILED: "+Integer.toString(rows[i-1])+" rows updated.");
                                        break;
                                    }
                                } else if (todo!=null && todo.toLowerCase().equals(DELETE)) {
                                    String setKey = getSessionString(ses, type+DOT+action+DOT+Integer.toString(i)+DOT+ACTION+DOT+SETKEY);
                                    if (setKey!=null && setKey.toLowerCase().equals("false"))
                                        qb.setSetKey(false);
                                    rows[i-1] = qb.delete(xmlData, tableName, conn);
                                    if (setKey!=null && setKey.toLowerCase().equals("false"))
                                        qb.setSetKey(true);
                                    if (rows[i-1] < 0 && xmlData.indexOf("</ROW>") >= 0) {
                                        req.setAttribute(SES_ERROR, getSessionString(ses, SES_UPDATE_FAILED));
                                        flag = 1;
                                        conn.rollback();
                                        logger.info("["+type+"]["+action+"] - DELETE FAILED: "+Integer.toString(rows[i-1])+" rows deleted.");
                                        break;
                                    }
                                } else if (todo!=null && todo.toLowerCase().equals(INSERT_UPDATE)) {
                                    rows[i-1] = qb.insertupdate(xmlData, tableName, conn);
                                    if (rows[i-1] < 0 && xmlData.indexOf("</ROW>") >= 0) {
                                        req.setAttribute(SES_ERROR, getSessionString(ses, SES_UPDATE_FAILED));
                                        flag = 1;
                                        conn.rollback();
                                        logger.info("["+type+"]["+action+"] - INSERT_UPDATE FAILED: "+Integer.toString(rows[i-1])+" rows updated.");
                                        break;
                                    }
                                } else if (todo!=null && todo.toLowerCase().equals(DELETE_INSERT)) {
                                    rows[i-1] = qb.deleteinsert(xmlData, tableName, conn);
                                    if (rows[i-1] < 0 && xmlData.indexOf("</ROW>") >= 0) {
                                        req.setAttribute(SES_ERROR, getSessionString(ses, SES_UPDATE_FAILED));
                                        flag = 1;
                                        conn.rollback();
                                        logger.info("["+type+"]["+action+"] - DELETE_INSERT FAILED: "+Integer.toString(rows[i-1])+" rows updated.");
                                        break;
                                    }
                                } else if (action.toLowerCase().equals(INSERT)) {
                                    rows[i-1] = qb.insert(xmlData, tableName, conn);
                                    if (rows[i-1] < 1 && xmlData.indexOf("</ROW>") >= 0) {
                                        req.setAttribute(SES_ERROR, getSessionString(ses, SES_INSERT_FAILED));
                                        flag = 1;
                                        conn.rollback();
                                        logger.info("["+type+"]["+action+"] - INSERT FAILED: "+Integer.toString(rows[i-1])+" rows inserted.");
                                        break;
                                    }
                                } else if (action.toLowerCase().equals(UPDATE)) {
                                    rows[i-1] = qb.update(xmlData, tableName, conn);
                                    if (rows[i-1] < 0 && xmlData.indexOf("</ROW>") >= 0) {
                                        req.setAttribute(SES_ERROR, getSessionString(ses, SES_UPDATE_FAILED));
                                        flag = 1;
                                        conn.rollback();
                                        logger.info("["+type+"]["+action+"] - UPDATE FAILED: "+Integer.toString(rows[i-1])+" rows updated.");
                                        break;
                                    }
                                } else if (action.toLowerCase().equals(DELETE)) {
                                    String setKey = getSessionString(ses, type+DOT+action+DOT+Integer.toString(i)+DOT+ACTION+DOT+SETKEY);
                                    if (setKey!=null && setKey.toLowerCase().equals("false"))
                                        qb.setSetKey(false);
                                    rows[i-1] = qb.delete(xmlData, tableName, conn);
                                    if (setKey!=null && setKey.toLowerCase().equals("false"))
                                        qb.setSetKey(true);
                                    if (rows[i-1] < 0 && xmlData.indexOf("</ROW>") >= 0) {
                                        req.setAttribute(SES_ERROR, getSessionString(ses, SES_UPDATE_FAILED));
                                        flag = 1;
                                        conn.rollback();
                                        logger.info("["+type+"]["+action+"] - DELETE FAILED: "+Integer.toString(rows[i-1])+" rows deleted.");
                                        break;
                                    }
                                }
                            }
                        } else if (sType!=null && sType.toLowerCase().equals(SESSION)) {

                            int pCount = getSessionInteger(ses, type+DOT+action+DOT+Integer.toString(i)+DOT+PARAM+DOT+COUNT);
                            if (pCount > 0)
                            {
                                for (int j = 1; j <= pCount; j++)
                                {
                                    String pName = getSessionString(ses, type+DOT+action+DOT+Integer.toString(i)+DOT+PARAM+DOT+Integer.toString(j)+DOT+NAME);
                                    String pValue = getSessionString(ses, type+DOT+action+DOT+Integer.toString(i)+DOT+PARAM+DOT+Integer.toString(j)+DOT+VALUE);

                                    String vFrom = pValue.substring(0, pValue.indexOf(",")).trim();
                                    String vParam = pValue.substring(pValue.indexOf(",")+1).trim();

                                    ses.removeAttribute(pName);
                                    if (!action.toLowerCase().equals(DELETE)) {
                                        if (vFrom!=null && vFrom.toLowerCase().equals(REQUEST))
                                            ses.setAttribute(pName, getRequestParameter(req, vParam));
                                        else if (vFrom!=null && vFrom.toLowerCase().equals(SESSION))
                                            ses.setAttribute(pName, getSessionString(ses, vParam));
                                        else if (vFrom!=null && vFrom.toLowerCase().equals(CONFIG))
                                            ses.setAttribute(pName, getConfigString(vParam));
                                    }
                                }
                            }
                        }
                    } else {
                        if (result!=null && !result.equals("FAIL")) {
                            logger.info("["+type+"]["+action+"] - Error: " + result);
                            req.setAttribute(SES_ERROR, result);
                            flag = 1;
                            conn.rollback();
                            break;
                       }
                    }
                }

                conn.commit();

	    } catch (SQLException se) {
                req.setAttribute(SES_ERROR, "SQLException occurred - " + se);
                logger.error("["+type+"]["+action+"] - SQLException occurred - " + se);
                se.printStackTrace();
                try {
                    conn.rollback();
                } catch (SQLException s) {
                    s.printStackTrace();
                }
            } catch (IOException ie) {
                req.setAttribute(SES_ERROR, "IOException occurred - " + ie);
                logger.error("["+type+"]["+action+"] - IOException occurred - " + ie);
                ie.printStackTrace();
                try {
                    conn.rollback();
                } catch (SQLException s) {
                    s.printStackTrace();
                }
            } catch (SAXException sx) {
                req.setAttribute(SES_ERROR, "SAXException occurred - " + sx);
                logger.error("["+type+"]["+action+"] - SAXException occurred - " + sx);
                sx.printStackTrace();
                try {
                    conn.rollback();
                } catch (SQLException s) {
                    s.printStackTrace();
                }
	    } catch (IllegalAccessException ex) {
                req.setAttribute(SES_ERROR, "Exception occurred - Failed to invoke function " + ex);
                logger.error("["+type+"]["+action+"] - Exception occurred, Failed to invoke function " + ex);
                ex.printStackTrace();
                try {
                    conn.rollback();
                } catch (SQLException s) {
                    s.printStackTrace();
                }
	    } catch (InvocationTargetException ex) {
                req.setAttribute(SES_ERROR, "Exception occurred - Failed to invoke function " + ex);
                logger.error("["+type+"]["+action+"] - Exception occurred, Failed to invoke function " + ex);
                ex.printStackTrace();
                try {
                    conn.rollback();
                } catch (SQLException s) {
                    s.printStackTrace();
                }
            } catch (InstantiationException ex) {
                req.setAttribute(SES_ERROR, "Exception occurred - Failed to invoke function " + ex);
                logger.error("["+type+"]["+action+"] - Exception occurred, Failed to invoke function " + ex);
                ex.printStackTrace();
                try {
                    conn.rollback();
                } catch (SQLException s) {
                    s.printStackTrace();
                }
	    } catch (ClassNotFoundException ex) {
                req.setAttribute(SES_ERROR, "Exception occurred - Failed to invoke function " + ex);
                logger.error("["+type+"]["+action+"] - Exception occurred, Failed to invoke function " + ex);
                ex.printStackTrace();
                try {
                    conn.rollback();
                } catch (SQLException s) {
                    s.printStackTrace();
                }
            } finally {
                try {
                    wrappedConnection.close();
                } catch (SQLException s) {
                    s.printStackTrace();
                }
            }
        }

        if (req.getAttribute(SES_ERROR) == null || req.getAttribute(SES_ERROR).equals(""))
            req.setAttribute(SES_ERROR, "");
        else
            flag = 1;

        String success = getSessionString(ses, type+DOT+action+DOT+SUCCESS);
 
        if (flag == 1) 
        {
            if (action.toLowerCase().equals(INSERT))
                processForm(req, res, ses, type, ADD);
            else if (action.toLowerCase().equals(UPDATE) || action.toLowerCase().equals(DELETE))
                processForm(req, res, ses, type, EDIT);
            
        } else if (success!=null && !success.equals("")) {
            if (req.getAttribute(SES_SUCCESS) == null || req.getAttribute(SES_SUCCESS).equals(""))
                req.setAttribute(SES_SUCCESS, success);
            
            if (action.toLowerCase().equals(INSERT))
                processForm(req, res, ses, type, ADD);
            else if (action.toLowerCase().equals(UPDATE) || action.toLowerCase().equals(DELETE))
                processForm(req, res, ses, type, EDIT);
            
        } else {
            try
            {
                int pCount = getSessionInteger(ses, type+DOT+action+DOT+PARAM+DOT+COUNT);
                if (pCount > 0)
                    qb.setParamArray(formParamArray(req, ses, null, null, type, action, pCount));
            } catch (SAXException sx) {
                req.setAttribute(SES_ERROR, "SAXException occurred - " + sx);
                logger.error("["+type+"]["+action+"] - SAXException occurred - " + sx);
                sx.printStackTrace();
            }

            req.removeAttribute(REQUEST_TYPE);
            req.removeAttribute(REQUEST_ACTION);

            String fwd = getSessionString(ses, type+DOT+action+DOT+FORWARD);
            if (fwd!=null && !fwd.equals("") && fwd.toLowerCase().endsWith("jsp"))
            {
                req.setAttribute(QueryBean.ID, qb);
                if (queryStr!=null && !queryStr.equals(""))
                    ses.setAttribute(SES_LOCATION, req.getServletPath()+"?"+queryStr);                
                forwardToModule(locale, JSP_LOCATION, fwd, req, res);                
            } else if (fwd!=null && !fwd.equals("")) {

                ses.setAttribute(SES_LOCATION, fwd);
                forward(fwd, req, res);
            } else {                
                String location = getSessionString(ses, SES_LOCATION);
                if (location!=null && !location.equals(""))
                    forward(location, req, res);
                else
                    forward("/serv?type=home&action=edit", req, res);
            }
        }

        return;
    }    

    private String getConditionResult(HttpServletRequest req, 
                                      HttpSession ses, 
                                      QueryBean qb,
                                      Connection conn,
                                      String type,
                                      String action, 
                                      int[] rows,
                                      int count,
                                      String index,
                                      String loopIndex)
        throws SAXException, IOException, SQLException
    {
        boolean[] bool = new boolean[count+1];
        bool[0] = true;
        String[] error = new String[count+1];
        error[0] = "";
        int lValue = -1;
        int rValue = -1;
        String lStr = "";
        String rStr = "";
 
        for (int i = 1; i <= count; i++)
        {
            String left = getSessionString(ses, type+DOT+action+DOT+index+DOT+COND+DOT+Integer.toString(i)+DOT+LEFT);
            String leftloop = getSessionString(ses, type+DOT+action+DOT+index+DOT+COND+DOT+Integer.toString(i)+DOT+LEFT+DOT+LOOP);
            String op = getSessionString(ses, type+DOT+action+DOT+index+DOT+COND+DOT+Integer.toString(i)+DOT+OP);
            String right = getSessionString(ses, type+DOT+action+DOT+index+DOT+COND+DOT+Integer.toString(i)+DOT+RIGHT);
            String rightloop = getSessionString(ses, type+DOT+action+DOT+index+DOT+COND+DOT+Integer.toString(i)+DOT+RIGHT+DOT+LOOP);
            String result = getSessionString(ses, type+DOT+action+DOT+index+DOT+COND+DOT+Integer.toString(i)+DOT+RESULT);
            error[i] = getSessionString(ses, type+DOT+action+DOT+index+DOT+COND+DOT+Integer.toString(i)+DOT+ERROR);
             
            logger.debug(left + " " + leftloop + " " + op + " " + right + " " + rightloop + " " +result + " " + error[i]);
            if (error[i]==null)
                error[i] = "";

            String opType = op.substring(0, op.indexOf(",")).trim();
            String opValue =  op.substring(op.indexOf(",")+1).trim();

            String bop = result.substring(0, result.indexOf(",")).trim();
            int bIndex = Integer.parseInt(result.substring(result.indexOf(",")+1).trim());

            if (bop.toLowerCase().equals(AND) && !bool[bIndex]) {
                bool[i] = false;
                continue;
            } else if (bop.toLowerCase().equals(OR) && bool[bIndex]) {
                bool[i] = true;
                continue;
            } 

            if (opType!=null && opType.toLowerCase().equals(INTEGER))
            {
                if (leftloop!=null && leftloop.toLowerCase().equals(NO))
                   lValue = getExpressionInteger(req, ses, qb, conn, type, action, rows, left, "");
                else
                   lValue = getExpressionInteger(req, ses, qb, conn, type, action, rows, left, loopIndex);
                
                if (rightloop!=null && rightloop.toLowerCase().equals(NO))
                    rValue = getExpressionInteger(req, ses, qb, conn, type, action, rows, right, "");
                else
                    rValue = getExpressionInteger(req, ses, qb, conn, type, action, rows, right, loopIndex);

                if (opValue!=null && opValue.toLowerCase().equals(EQ)) 
                {
                    if (bop.toLowerCase().equals(AND))
                    {
                        if (bool[bIndex] && (lValue == rValue))
                            bool[i] = true;
                        else
                            bool[i] = false;
                    } else if (bop.toLowerCase().equals(OR)) {
                        if (bool[bIndex] || (lValue == rValue))
                            bool[i] = true;
                        else
                            bool[i] = false;
                    }
                } else if (opValue!=null && opValue.toLowerCase().equals(NEQ)) {
                    if (bop.toLowerCase().equals(AND))
                    {
                        if (bool[bIndex] && (lValue != rValue))
                            bool[i] = true;
                        else
                            bool[i] = false;
                    } else if (bop.toLowerCase().equals(OR)) {
                        if (bool[bIndex] || (lValue != rValue))
                            bool[i] = true;
                        else
                            bool[i] = false;
                    }
                } else if (opValue!=null && opValue.toLowerCase().equals(GT)) {
                    if (bop.toLowerCase().equals(AND))
                    {
                        if (bool[bIndex] && (lValue > rValue))
                            bool[i] = true;
                        else
                            bool[i] = false;
                    } else if (bop.toLowerCase().equals(OR)) {
                        if (bool[bIndex] || (lValue > rValue))
                            bool[i] = true;
                        else
                            bool[i] = false;
                    }
                } else if (opValue!=null && opValue.toLowerCase().equals(GTE)) {
                    if (bop.toLowerCase().equals(AND))
                    {
                        if (bool[bIndex] && (lValue >= rValue))
                            bool[i] = true;
                        else
                            bool[i] = false;
                    } else if (bop.toLowerCase().equals(OR)) {
                        if (bool[bIndex] || (lValue >= rValue))
                            bool[i] = true;
                        else
                            bool[i] = false;
                    }
                } else if (opValue!=null && opValue.toLowerCase().equals(LT)) {
                    if (bop.toLowerCase().equals(AND))
                    {
                        if (bool[bIndex] && (lValue < rValue))
                            bool[i] = true;
                        else
                            bool[i] = false;
                    } else if (bop.toLowerCase().equals(OR)) {
                        if (bool[bIndex] || (lValue < rValue))
                            bool[i] = true;
                        else
                            bool[i] = false;
                    }
                } else if (opValue!=null && opValue.toLowerCase().equals(LTE)) {
                    if (bop.toLowerCase().equals(AND))
                    {
                        if (bool[bIndex] && (lValue <= rValue))
                            bool[i] = true;
                        else
                            bool[i] = false;
                    } else if (bop.toLowerCase().equals(OR)) {
                        if (bool[bIndex] || (lValue <= rValue))
                            bool[i] = true;
                        else
                            bool[i] = false;
                    }
                }
            } else if (opType!=null && opType.toLowerCase().equals(STRING)) {
                
                if (leftloop!=null && leftloop.toLowerCase().equals(NO))
                    lStr = getExpressionString(req, ses, qb, conn, type, action, rows, left, "");
                else
                    lStr = getExpressionString(req, ses, qb, conn, type, action, rows, left, loopIndex);
                
                if (rightloop!=null && rightloop.toLowerCase().equals(NO))
                    rStr = getExpressionString(req, ses, qb, conn, type, action, rows, right, "");
                else
                    rStr = getExpressionString(req, ses, qb, conn, type, action, rows, right, loopIndex);
   
                if (opValue!=null && opValue.toLowerCase().equals(EQ)) 
                {
                    if (bop.toLowerCase().equals(AND))
                    {
                        if (bool[bIndex] && (lStr.equals(rStr)))
                            bool[i] = true;
                        else
                            bool[i] = false;
                    } else if (bop.toLowerCase().equals(OR)) {
                        if (bool[bIndex] || (lStr.equals(rStr)))
                            bool[i] = true;
                        else
                            bool[i] = false;
                    }
                } else if (opValue!=null && opValue.toLowerCase().equals(NEQ)) {
                    if (bop.toLowerCase().equals(AND))
                    {
                        if (bool[bIndex] && (!lStr.equals(rStr))) {
                            bool[i] = true;
                            
                        } else {
                            bool[i] = false;
                        }
                    } else if (bop.toLowerCase().equals(OR)) {
                        if (bool[bIndex] || (!lStr.equals(rStr)))
                            bool[i] = true;
                        else
                            bool[i] = false;
                    }
                }
            }    
        }       

        if (bool[count])
            return "PASS";
        else {
            for (int i = 1; i <= count; i++)
            {
                if (!bool[i] && error[i]!=null && !error[i].equals(""))
                    return error[i];
            }
            return "FAIL";
        }
    }        

    private String[] formParamArray(HttpServletRequest req, 
                                    HttpSession ses, 
                                    String[] xml, 
                                    String[] html, 
                                    String type,
                                    String action, 
                                    int pCount)
        throws SAXException, IOException
    {
        String[] param = new String[pCount];
        for (int j = 1; j <= pCount; j++)
        {
            String pName = getSessionString(ses, type+DOT+action+DOT+PARAM+DOT+Integer.toString(j)+DOT+NAME);
            if (pName!=null && !pName.equals(""))
            {
                String pValue = getSessionString(ses, type+DOT+action+DOT+PARAM+DOT+Integer.toString(j)+DOT+VALUE);    
                param[j-1] = getParamValue(req, ses, xml, html, pName, pValue);
            } else
                param[j-1] = "";
        }

        return param;
    }


    private String[] formListFunctionParamArray(HttpServletRequest req, 
                                    HttpSession ses, 
                                    String[] xml, 
                                    String[] html, 
                                    String type,
                                    String action, 
                                    int index)
        throws SAXException, IOException
    {
        String param[] = null;
        
        int pCount = getSessionInteger(ses, type+DOT+action+DOT+SQL+DOT+Integer.toString(index)+DOT+FUNCTION+DOT+PARAM+DOT+COUNT);
        if (pCount > 0) {
            param = new String[pCount];
            
            for (int j = 1; j <= pCount; j++)
            {
                String pName = getSessionString(ses, type+DOT+action+DOT+SQL+DOT+Integer.toString(index)+DOT+FUNCTION+DOT+PARAM+DOT+Integer.toString(j)+DOT+NAME);
                if (pName!=null && !pName.equals(""))
                {
                    String pValue = getSessionString(ses, type+DOT+action+DOT+SQL+DOT+Integer.toString(index)+DOT+FUNCTION+DOT+PARAM+DOT+Integer.toString(j)+DOT+VALUE);    
                    param[j-1] = getParamValue(req, ses, xml, html, pName, pValue);
                } else
                    param[j-1] = "";
            }
        }

        return param;
    }
    
    private String[] formSubmitFunctionParamArray(HttpServletRequest req, 
                                    HttpSession ses, 
                                    String[] xml, 
                                    String type,
                                    String action, 
                                    int index)
         throws SAXException, IOException
    {
        String param[] = null;
        
        int pCount = getSessionInteger(ses, type+DOT+action+DOT+Integer.toString(index)+DOT+FUNCTION+DOT+PARAM+DOT+COUNT);
        if (pCount > 0) {
            param = new String[pCount];
        
        for (int j = 1; j <= pCount; j++)
        {
            String pName = getSessionString(ses, type+DOT+action+DOT+Integer.toString(index)+DOT+FUNCTION+DOT+PARAM+DOT+Integer.toString(j)+DOT+NAME);
            if (pName!=null && !pName.equals(""))
            {
                String pValue = getSessionString(ses, type+DOT+action+DOT+Integer.toString(index)+DOT+FUNCTION+DOT+PARAM+DOT+Integer.toString(j)+DOT+VALUE);    
                param[j-1] = getParamValue(req, ses, xml, null, pName, pValue);
            } else
                param[j-1] = "";
            }
        }
        
        return param;
    }
    
    
    private String bindParameter(HttpServletRequest req,
                                 HttpSession ses,
                                 String[] xml,
                                 String type,
                                 String action,
                                 String sql,
                                 String query,
                                 int index)
        throws SAXException, IOException
    {
        String stmt = "";
        String value = null;
        
        if (query.toLowerCase().equals(COUNT))
            stmt = COUNT+DOT+Integer.toString(index);
        else
            stmt = SQL+DOT+Integer.toString(index);

        int bCount = getSessionInteger(ses, type+DOT+action+DOT+stmt+DOT+BIND+DOT+COUNT);
        if (bCount > 0)
        {
            for (int j = 1; j <= bCount; j++)
            {
                String bName = getSessionString(ses, type+DOT+action+DOT+stmt+DOT+BIND+DOT+Integer.toString(j)+DOT+PARAM).trim();
                if (bName!=null && !bName.equals(""))
                {
                    String bValue = getSessionString(ses, type+DOT+action+DOT+stmt+DOT+BIND+DOT+Integer.toString(j)+DOT+VALUE);
                    int strFlag = sql.indexOf("'["+Integer.toString(j)+"]'");
                    
                    if (bValue!=null && bValue.toLowerCase().equals(REQUEST))
                        value = getRequestParameter(req, bName);
                    else if (bValue!=null && bValue.toLowerCase().equals(SESSION))   
                        value = getSessionString(ses, bName);
                    else if (bValue!=null && bValue.toLowerCase().equals(CONFIG))   
                        value = getConfigString(bName);
                    else if (bValue!=null && bValue.toLowerCase().startsWith(XML)) {                        
                        int xIndex = Integer.parseInt(bValue.substring(bValue.indexOf(",")+1, bValue.lastIndexOf(",")).trim());
                        int row =  Integer.parseInt(bValue.substring(bValue.lastIndexOf(",")+1).trim());
                        value = getValueFromXML(xml[xIndex-1], bName, row);                        
                    } else if (bValue!=null && bValue.toLowerCase().equals(TEXT)) 
                        value = bName;

                    if ((value!=null && !value.equals("") && strFlag < 0) || (value!=null && strFlag >= 0))
                        sql = sql.substring(0, sql.indexOf("["+Integer.toString(j)+"]")) + value +
                              sql.substring(sql.indexOf("["+Integer.toString(j)+"]")+3);
                    else {
                        logger.warn("["+type+"]["+action+"] - SQL: " + sql);
                        logger.warn("["+type+"]["+action+"] - Could not bind parameter - " + bName + "; integer value not found in " + bValue);
                        sql = null;
                        break;
                    }
                }
            }
        }        
        
        return sql;
    }

    private String getParamValue(HttpServletRequest req, 
                                 HttpSession ses, 
                                 String[] xml, 
                                 String[] html, 
                                 String pName,
                                 String pValue)
        throws SAXException, IOException
    {
        String value = "";
        if (pName!=null && !pName.equals(""))
        {
            if (pValue!=null && pValue.toLowerCase().equals(REQUEST))
                value = getRequestParameter(req, pName);
            else if (pValue!=null && pValue.toLowerCase().equals(SESSION))                             
                value = getSessionString(ses, pName);
            else if (pValue!=null && pValue.toLowerCase().equals(CONFIG))                             
                value = getConfigString(pName);
            else if (pValue!=null && pValue.toLowerCase().startsWith(XML)) 
            {                             
                int index = Integer.parseInt(pValue.substring(pValue.indexOf(",")+1, pValue.lastIndexOf(",")).trim());
                int row =  Integer.parseInt(pValue.substring(pValue.lastIndexOf(",")+1).trim());
                value = getValueFromXML(xml[index-1], pName, row);
            } else if (pValue!=null && pValue.toLowerCase().equals(TEXT))                            
                value = pName;
            
            logger.debug("pName: " + pName + ", value: " + value);
        } else {
            if (pValue!=null && pValue.toLowerCase().startsWith(HTML)) 
            {                             
                int index = Integer.parseInt(pValue.substring(pValue.indexOf(",")+1).trim());
                value = html[index-1];
            }
            logger.debug("pValue: " + pValue + ", value: " + value);
       }

        return value;
    }

    private String getValueFromXML(String xml, 
                                   String param, 
                                   int row)
        throws SAXException, IOException
    {
       String ret = "";
       
       //logger.info("getValueFromXML[xml: " + xml + ", param: "
       //        + param + ", row: " + row + "]");

       if (xml!=null && !xml.equals("") &&
               param!=null && !param.equals(""))
       {
           ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());
           InputStream inStream = bais;
           InputSource is = new InputSource(inStream);

           DOMParser parser = new DOMParser();
           parser.parse(is);

           Document doc = parser.getDocument();
           NodeList nl1 = doc.getElementsByTagName("ROW");

           if (nl1.getLength() > 0) 
           {
               Node n1 = nl1.item(row - 1);
               NodeList nl2 = n1.getChildNodes();
               Node n2;

               for (int i = 0; i < nl2.getLength(); i++) 
               {
                   n2 = nl2.item(i);
                   if (n2.getNodeType() == Node.ELEMENT_NODE) 
                   {
                       if (n2.getNodeName().toLowerCase().equals(param.toLowerCase())) 
                       {
                           if (n2.getFirstChild() != null) 
                           {
                               int childType = n2.getFirstChild().getNodeType();

                               if (childType == Node.TEXT_NODE) 
                               {
                                   if (n2.getFirstChild().getNodeValue() != null) 
                                   {
                                       ret = n2.getFirstChild().getNodeValue();
                                       break;
                                   }
                               }
                           }
                       }
                   }
               }
           }
       } else 
           ret = xml;
     
       return ret;
    }
    
    private int getXMLRows(String xml)
        throws SAXException, IOException
    {
        int ret = -1;

        ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());
        InputStream inStream = bais;
        InputSource is = new InputSource(inStream);

        DOMParser parser = new DOMParser();
        parser.parse(is);

        Document doc = parser.getDocument();
        NodeList nl = doc.getElementsByTagName("ROW");

        ret = nl.getLength();

        return ret;
    }
    
    private int getExpressionInteger(HttpServletRequest req, 
                                       HttpSession ses, 
                                       QueryBean qb,
                                       Connection conn,
                                       String type,
                                       String action, 
                                       int[] rows,
                                       String expStr,
                                       String loopIndex)
        throws SAXException, IOException, SQLException
    {
        String expType = expStr.substring(0, expStr.indexOf(",")).trim();
        int expValue = -99;
        int ret = -99;

        if (expType!=null && expType.toLowerCase().equals(RESULT)) {
            expValue =  Integer.parseInt(expStr.substring(expStr.indexOf(",")+1).trim());
            ret = rows[expValue-1];
        } else if (expType!=null && expType.toLowerCase().equals(TEXT)) {
            expValue =  Integer.parseInt(expStr.substring(expStr.indexOf(",")+1).trim());
            ret = expValue;
        } else if (expType!=null && expType.toLowerCase().equals(COUNT)) {
            expValue =  Integer.parseInt(expStr.substring(expStr.indexOf(",")+1).trim());
            String sql = getSessionString(ses, type+DOT+action+DOT+COUNT+DOT+String.valueOf(expValue));
            if (sql != null && !sql.equals("")) 
            {
                sql = bindParameter(req, ses, null, type, action, sql, COUNT, expValue);
                if (sql != null && !sql.equals("")) 
                {                
                    logger.info("["+type+"]["+action+"] - Count SQL: " + sql);    
                    ret = qb.count(sql, conn);
                }
            } 
        } else if (expType!=null && expType.toLowerCase().equals(REQUEST)) {
            String pValue = getRequestParameter(req, expStr.substring(expStr.indexOf(",")+1).trim()+loopIndex);
            if (pValue!=null && !pValue.equals(""))
                ret = Integer.parseInt(pValue);
        } else if (expType!=null && expType.toLowerCase().equals(SESSION))
            ret = getSessionInteger(ses, expStr.substring(expStr.indexOf(",")+1).trim());
        else if (expType!=null && expType.toLowerCase().equals(CONFIG))
            ret = getConfigInteger(expStr.substring(expStr.indexOf(",")+1).trim());

        return ret;
    }

    private String getExpressionString(HttpServletRequest req, 
                                       HttpSession ses, 
                                       QueryBean qb,
                                       Connection conn, 
                                       String type,
                                       String action, 
                                       int[] rows,
                                       String expStr,
                                       String loopIndex)
        throws SAXException, IOException, SQLException
    {
        String expType = expStr.substring(0, expStr.indexOf(",")).trim();
        String expValue = expStr.substring(expStr.indexOf(",")+1).trim();
        String ret = null;

        if (expType!=null && expType.toLowerCase().equals(TEXT))
            ret = expValue;
        else if (expType!=null && expType.toLowerCase().equals(SQL))
        {
            String cParam = expValue.substring(expValue.indexOf(",")+1, expValue.lastIndexOf(",")).trim();
            int qIndex = Integer.parseInt(expValue.substring(expValue.lastIndexOf(",")+1).trim());
            String sql = getSessionString(ses, type+DOT+action+DOT+SQL+DOT+qIndex);
            if (sql != null && !sql.equals("")) 
            {
                sql = bindParameter(req, ses, null, type, action, sql, SQL, qIndex);
                if (sql != null && !sql.equals("")) 
                {
                    logger.info("["+type+"]["+action+"] - Query SQL: " + sql);    
                    String xml = qb.query(sql, conn);
                    ret = getValueFromXML(xml, cParam, 1);
                }
            } 
        } else if (expType!=null && expType.toLowerCase().equals(REQUEST))
            ret = getRequestParameter(req, expValue+loopIndex); 
        else if (expType!=null && expType.toLowerCase().equals(SESSION))
            ret = getSessionString(ses, expValue);
        else if (expType!=null && expType.toLowerCase().equals(CONFIG))
            ret = getConfigString(expValue);

        return ret;
    }

    private int getConfigInteger(String param)
    {
        String str = getConfigString(param);
        int value = -1;
        
        if (str != null)
            value = Integer.parseInt(str);    

        return value;
    }
    
    private String getConfigString(String param)
    {
        String value = (serverProps != null)? serverProps.getProperty(param) : null;
        
        if (value != null)
            value = value.trim();  
        
        return value;
    }

    
    private int getSessionInteger(HttpSession ses, 
                                  String param)
    {
        Object obj = ses.getAttribute(param);
        int value = -1;

        if (obj != null)
            value = Integer.parseInt(obj.toString());    

        return value;
    }

    private String getSessionString(HttpSession ses, 
                                    String param)
    {
        Object obj = ses.getAttribute(param);
        String value = null;

        if (obj != null)
            value = obj.toString().trim();    
        
        return value;
    }

    private String getRequestParameter(HttpServletRequest req, 
                                       String param)
    {
        String value = null;

        if (param!=null && !param.equals(""))
        {
            value = req.getParameter(param);
            if (value != null && !value.equals("")) 
            {
                value = value.replace('"', ' ');
                value = value.trim();
            }
        }

        return value;
    }

    private String storeParameter(HttpServletRequest req, 
                                  HttpSession ses, 
                                  String paramName, 
                                  String sessionName)
    {
        String param = null;

        if (paramName!=null && !paramName.equals(""))
        {
            param = req.getParameter(paramName);
            if (param!=null && param!="")
            {
                ses.setAttribute(sessionName, param);
            } else {
                if (ses.getAttribute(sessionName)!=null)
                    param = ses.getAttribute(sessionName).toString();
            }
        }

        return param;
    }
}



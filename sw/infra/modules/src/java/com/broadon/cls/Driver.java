package com.broadon.cls;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Properties;

import com.broadon.exception.BroadOnException;

/**
 * Main driver for the entire CLS
 */
public class Driver
{
    static final String WAD_SUFFIX = ".wad";
    static final String DVD_SUFFIX = ".dev_part";
    static final String CLS_SUFFIX = ".cls";
    static final String WAD_OUT_SUFFIX = ".wad.out";
    static final String DVD_OUT_SUFFIX = ".prod_part";
    
    static final String WAD_PREPROCESSOR = "/opt/broadon/pkgs/cls/bin/unpublish";
    static final String DVD_PREPROCESSOR = "/opt/broadon/pkgs/cls/bin/prepPart";
    static final String DVD_POSTPROCESSOR = "/opt/broadon/pkgs/cls/bin/genPart";
    
    private File titleDir;
    private File vDir;
    private String[] args;
    
    
    public int process(String[] args) throws BroadOnException, IOException
    {
        this.args = args;
        titleDir = new File(args[0]);
        vDir = getVersionDirectory(titleDir);
        File input = null;
        
        if ((input = getInputFile(vDir, WAD_SUFFIX)) != null) {
            return processWad(input);
        } else if ((input = getInputFile(vDir, DVD_SUFFIX)) != null) {
            return processDvd(input);
        } else {
            return GenerateLicense.pseudoMain(args);
        }
    }


    private int processDvd(File partFile) throws IOException
    {
        String[] preCmd = new String[] {
                DVD_PREPROCESSOR, partFile.getAbsolutePath(), "-l", vDir.getAbsolutePath()
        };
        
        // preprocess the DVD partition and split it into multiple files
        System.out.println("\n\nPre-processing input DVD partition.  Please wait ...");
        Process proc = Runtime.getRuntime().exec(preCmd);
        try {
            if (flushProcessOutput(proc) != 0)
                return 1;
            
            // generate the license with "-dvd" and "-split" flags.
            String[] cmd = new String[args.length + 2];
            int i;
            for (i = 0; i < args.length; i++) {
                cmd[i] = args[i];
            }
            cmd[i++] = "-dvd";
            cmd[i] = "-split";
            System.out.println("\n\nGenerating license and encrypting file(s)...");
            int status = GenerateLicense.pseudoMain(cmd);
            
            if (status == 0) {
                // postprocess: put the generated license and encrypted files
                // back into a DVD partition
                String[] postCmd = new String[] {
                        DVD_POSTPROCESSOR, vDir.getAbsolutePath(), "-o", getOutputPartition() 
                };
                System.out.println("\n\nCreating output DVD partition...");
                proc = Runtime.getRuntime().exec(postCmd);
                status = flushProcessOutput(proc);
            }
            
            cleanup(vDir, new String[] { DVD_SUFFIX, DVD_OUT_SUFFIX, CLS_SUFFIX });
            return status;
        } catch (InterruptedException e) {
            return 1;
        }
    }

    private String getOutputPartition()
    {
        File file = new File(vDir, titleDir.getName() + "-v" + fileNameToVersion(vDir) + 
                                   DVD_OUT_SUFFIX);
        return file.getAbsolutePath();
    }


    private int processWad(File wadFile) throws IOException
    {
        boolean useNcKey = selectKey();
        
        String[] cmds = useNcKey ? 
                        new String[] { WAD_PREPROCESSOR, "-nc", wadFile.getAbsolutePath() } :
                        new String[] { WAD_PREPROCESSOR, wadFile.getAbsolutePath() };

        Process proc = Runtime.getRuntime().exec(cmds);
        
        try {
            if (flushProcessOutput(proc) != 0)
                return 1;
            int status = GenerateLicense.pseudoMain(args);
            cleanup(vDir, new String[] { WAD_OUT_SUFFIX, WAD_SUFFIX, CLS_SUFFIX });
            return status;
        } catch (InterruptedException e) {
            return 1;
        }
        
        
    }

    
    private boolean selectKey()
    {
        Properties prop = new Properties();
        try {
            FileInputStream in = new FileInputStream(new File(titleDir, GenerateLicense.TITLE_PROPERTY_KEY));
            prop.load(in);
            in.close();
        } catch (IOException e) {
            return false;
        }
        
        String titleType = prop.getProperty(GenerateLicense.TITLE_TYPE_KEY, "");
        return titleType.startsWith("NC_");
    }


    private int flushProcessOutput(Process proc) throws IOException, InterruptedException {
        final int bufSize = 8 * 1024;
        BufferedInputStream in = new BufferedInputStream(proc.getInputStream(), bufSize);
        BufferedInputStream err = new BufferedInputStream(proc.getErrorStream(), bufSize);
        byte[] buffer = new byte[bufSize];
        int n;
        while ((n = in.read(buffer)) >= 0) {
            System.out.write(buffer, 0, n);
        }
        in.close();
        while ((n = err.read(buffer)) >= 0) {
            System.err.write(buffer, 0, n);
        }
        err.close();
        return proc.waitFor();
    }
    
    
    private void cleanup(File dir, final String[] suffix)
    {
        FilenameFilter filter = new FilenameFilter() {
            public boolean accept(File contents, String name) {
                for (int i = 0; i < suffix.length; i++) {
                    if (name.endsWith(suffix[i]))
                        return false;
                }
                return true;
            }
        };
        
        // find the list of files to be deleted
        String[] deleteList = dir.list(filter);
        for (int i = 0; i < deleteList.length; i++) {
            File file = new File(dir, deleteList[i]);
            file.delete();
        }
    }

    
    private File getInputFile(File dir, final String suffix) throws BroadOnException
    {
        FilenameFilter filter = new FilenameFilter() {
            public boolean accept(File contents, String name) {
                return name.endsWith(suffix);
            }
        };
        
        String[] list = dir.list(filter);
        if (list == null || list.length == 0)
            return null;
        else if (list.length == 1)
            return new File(dir, list[0]);
        else {
            StringBuffer s = new StringBuffer("More than one input file found:");
            for (int i = 0; i < list.length; i++) {
                s.append(list[i]);
            }
            throw new BroadOnException(s.toString());
        }
    }

    
    private File getVersionDirectory(File titleDir) throws BroadOnException, IOException
    {
        FilenameFilter filter = new FilenameFilter() {
            public boolean accept(File contents, String name) {
                if (name.charAt(0) != 'v')
                    return false;
                try {
                    Integer.parseInt(name.substring(1));
                } catch (NumberFormatException e) {
                    return false;
                }
                File file = new File(contents, name);
                return file.isDirectory();
            }
        };
        
        if (! titleDir.isDirectory())
            throw new IOException("Input is not a directory.");
            
        File[] files = titleDir.listFiles(filter);
        
        if (files.length == 1)
            return files[0];
        else if (files.length == 0)
            throw new BroadOnException("Missing or bad version directory");
        
        File result = files[0];
        int version = fileNameToVersion(result);
        
        for (int i = 1; i < files.length; ++i) {
            int v = fileNameToVersion(files[i]);
            if (v > version) {
                version = v;
                result = files[i];
            } else if (v == version)
                throw new BroadOnException("Duplicated version directory " + v);
        }
        
        return result;
    }

    private int fileNameToVersion(File file)
    {
        try {
            return Integer.parseInt(file.getName().substring(1));
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    private static void usage()
    {
        System.err.println("Usage: genLic <directory> [-privilege]");
        System.exit(1);
    }

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        if (args.length < 1)
            usage();
    
        try {
            Driver drv = new Driver();
            System.exit(drv.process(args));
        } catch (BroadOnException e) {
            System.err.println(e.getMessage());
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Error in processing: " + e.getMessage());
            System.exit(1);
        }
    }

}

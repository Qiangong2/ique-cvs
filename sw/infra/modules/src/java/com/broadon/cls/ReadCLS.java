package com.broadon.cls;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.math.BigInteger;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import com.broadon.wsapi.cps.ContentLinkType;
import com.broadon.wsapi.cps.UploadRequestType;

public class ReadCLS
{
    public static void main(String[] args)
    {
        try {
            if (args.length < 1) {
                System.err.println("Usage:  ReadCLS <cls file>");
                System.exit(1);
            }
            
            File file = new File(args[0]);
            
            UploadRequestType pkg = getUploadRequest(file);
            
            System.out.println("Action\t\t\t= " + pkg.getAction().toString());
            System.out.println("Product code\t\t= " + filter(pkg.getProductCode()));
            System.out.println("Title name\t\t= " + filter(pkg.getTitleName()));
            System.out.println("Title type\t\t= " + filter(pkg.getTitleType()));
            System.out.println("Publish date\t\t= " + new Date(pkg.getPublishDate()).toString());
            System.out.println("Smart card Id\t\t= " + filter(pkg.getAuthorization()));
            System.out.println("Allow common ticket\t= " + pkg.getAllowCommonTicket());
            System.out.println("Title key\t\t= " + toHexString(pkg.getEncryptedTitleKey()));
            System.out.println("Channel title key\t= " + toHexString(pkg.getEncryptedChannelKey()));
            System.out.println("Common ETicket\t\t= " + toFile(file.getParentFile(), "CETK", pkg.getCommonETicket()));
            System.out.println("Meta data\t\t= " + toFile(file.getParentFile(), "MET", pkg.getMetaData()));
            System.out.println("TMD\t\t\t= " + toFile(file.getParentFile(), "TMD", pkg.getTitleMetaData()));
            ContentLinkType[] links = pkg.getContentLink();
            for (int i = 0; i < links.length; i++) {
                ContentLinkType l = links[i];
                System.out.println("Content ID: " + l.getContentId());
                System.out.println("\tChecksum\t= " + l.getCheckSum());
                System.out.println("\tName\t\t= " + filter(l.getContentName()));
                System.out.println("\tType\t\t= " + filter(l.getContentType()));
            }
        } catch(Exception e) {
            e.printStackTrace();
        }

    }

    private static UploadRequestType getUploadRequest(File file) throws IOException, FileNotFoundException, ClassNotFoundException
    {
        try {
            ZipFile zipIn = new ZipFile(file);
            ZipEntry ze = zipIn.getEntry("UploadRequest");
            try {
                ObjectInputStream in = new ObjectInputStream(zipIn.getInputStream(ze));
                UploadRequestType req = (UploadRequestType) in.readObject();
                in.close();
                return req;
            } finally {
                zipIn.close();
            }
            
        } catch (ZipException e) {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
            try {
                UploadRequestType pkg = (UploadRequestType) in.readObject();
                return pkg;
            } finally {
                in.close();
            }
        }
    }

    private static String filter(String s)
    {
        return (s == null) ? "<Not available>" : s;
    }

    private static String toFile(File dir, String string, byte[] buf) throws IOException
    {
        if (buf == null)
            return "<Not available>";
        File file = new File(dir, string);
        FileOutputStream out = new FileOutputStream(file);
        try {
            out.write(buf);
        } finally {
            out.close();
        }
        return "<written to \"" + file.getPath() + "\">";
    }

    static String toHexString(byte[] s) {
        return (s == null) ? "<Not available>" : new BigInteger(1, s).toString(16);
    }

}

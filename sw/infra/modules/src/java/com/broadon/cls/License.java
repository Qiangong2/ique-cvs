package com.broadon.cls;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import com.broadon.cps.ContentAttributes;
import com.broadon.cps.ContentInputStream;
import com.broadon.cps.PublishUtil;
import com.broadon.cps.TMDGenerator;
import com.broadon.cps.TitleMetaData;
import com.broadon.ets.ETicket;
import com.broadon.exception.BroadOnException;
import com.broadon.security.BCCCert;
import com.broadon.security.BCCCertV2_RSA;
import com.broadon.see.HardwareSecurityModule;
import com.broadon.see.SecurityModule;
import com.broadon.wsapi.cps.ActionType;
import com.broadon.wsapi.cps.ContentAttributeType;
import com.broadon.wsapi.cps.ContentLinkType;
import com.broadon.wsapi.cps.PublishRequestType;
import com.broadon.wsapi.cps.UploadRequestType;
import com.ncipher.nfast.NFException;

public class License
{
    static final String TMD_GEN = "com.broadon.cps.TMDGenerator";
    static public final String CA_CRL_VERSION_KEY = "CA_CRL_VERSION";
    static public final String CP_CRL_VERSION_KEY = "CP_CRL_VERSION";
    static public final String XS_CRL_VERSION_KEY = "XS_CRL_VERSION";
    static public final String CA_CERT_FILE_KEY = "CA_CERT_FILE";
    static public final String XS_CERT_FILE_KEY = "XS_CERT_FILE";
    static public final String CP_CERT_FILE_KEY = "CP_CERT_FILE";

    private SecurityModule sm;
    private int caCrlVersion;
    private int cpCrlVersion;
    private int xsCrlVersion;
    private CertInfo caCert;
    private CertInfo xsCert;
    private CertInfo cpCert;
    private String commonKey;
    private int tmdVersion = 0;
    private final boolean useUnsafeKeyGen;
    private boolean dvdHash;

    public License(Properties prop) 
        throws GeneralSecurityException, NFException, ClassNotFoundException, IOException
    {
        this(prop, false);
    }
    
    public License(Properties prop, boolean dvdHash) 
        throws GeneralSecurityException, NFException, ClassNotFoundException, IOException
    {
        sm = SecurityModule.getInstance(prop);
        caCrlVersion = Integer.parseInt(prop.getProperty(CA_CRL_VERSION_KEY));
        cpCrlVersion = Integer.parseInt(prop.getProperty(CP_CRL_VERSION_KEY));
        xsCrlVersion = Integer.parseInt(prop.getProperty(XS_CRL_VERSION_KEY));
        caCert = new CertInfo(prop, CA_CERT_FILE_KEY);
        xsCert = new CertInfo(prop, XS_CERT_FILE_KEY);
        cpCert = new CertInfo(prop, CP_CERT_FILE_KEY);
        commonKey = prop.getProperty(PublishUtil.COMMON_KEY_NAME, PublishUtil.DEFAULT_COMMON_KEY_NAME);
        
        // verify the cert chain.
        verifyKey(xsCert);
        verifyKey(cpCert);
        verifyCert(xsCert, caCert);
        verifyCert(cpCert, caCert);
        
        useUnsafeKeyGen = "true".equals(prop.getProperty("USE_UNSAFE_KEY_GENERATION")) ? true : false;
        
        Class.forName(TMD_GEN);         // force loading and initialization of JNI code
        
        this.dvdHash = dvdHash;         // true if we use hash algorithm for DVD game 
    }


    private void verifyCert(CertInfo cert, CertInfo ca) throws GeneralSecurityException
    {
        if (! cert.parsedCert.verify(ca.parsedCert.getPublicKey()))
            throw new GeneralSecurityException("Invalid cert chain: " + cert.file.getPath() +
                                               " and " + ca.file.getPath());
    }
    

    private void verifyKey(CertInfo cert) throws GeneralSecurityException, NFException
    {
        if (! sm.verify(cert.keyName, cert.parsedCert.getPublicKey()))
            throw new GeneralSecurityException("Certificate and key do not match: " + 
                                               cert.file.getPath());
    }

    
    public byte[] genCommonTicket(long titleId, byte[] ticketTemplate, byte[] titleKey)
        throws GeneralSecurityException, NFException
    {
        ETicket eTicket = new ETicket(sm, 0, null);
        
        // the ticket ID is generated with the top 16 bits of title ID with a random low 48 bits
        Random rand = new Random();
        long ticketId = (titleId & 0xFFFF000000000000L) | (rand.nextLong() & 0xFFFFFFFFFFFFL);
        return eTicket.generateTicket(ticketTemplate, xsCert.parsedCert.getSubject(), 
                                      caCert.parsedCert.getSubject(), caCrlVersion, 
                                      xsCrlVersion, xsCert.keyName, ticketId, titleId, 
                                      titleKey);
    }
    
    private byte[] encryptTitleKey(TMDGenerator tmdGen, long titleID, byte[] plainTextKey) 
        throws GeneralSecurityException, NFException
    {
        if (sm instanceof HardwareSecurityModule) {
            HardwareSecurityModule hsm = (HardwareSecurityModule) sm;
            return hsm.encryptTitleKey(commonKey, titleID, plainTextKey);
        } else
            return tmdGen.encryptTitleKey(titleID, plainTextKey);
    }
    
    public UploadRequestType license(TitleMetaData tmdTemplate, PublishRequestType req,  
                                     boolean isSubscription, File[] contentFiles, File outDir, 
                                     ZipOutputStream zipout, long titleID, int minTMDVersion, 
                                     UploadRequestType lastResp) 
        throws BroadOnException, GeneralSecurityException, NFException, IOException
    {
        UploadRequestType resp = new UploadRequestType();

        boolean isUpdate = ActionType.Update.equals(req.getAction());

        byte[] titleKey = PublishUtil.generateTitleKey(req.getEncryptionPassword(), titleID, 
                                                       isSubscription, useUnsafeKeyGen);

        TMDGenerator tmdGen = new TMDGenerator(titleKey, PublishUtil.getCommonKey(commonKey),
                                               caCrlVersion, cpCrlVersion, cpCert.parsedCert.getSubject(),
                                               caCert.parsedCert.getSubject());

        byte[] encryptedTitleKey = encryptTitleKey(tmdGen, titleID, titleKey); 
        byte[] encryptedChannelKey = isSubscription ? 
                encryptTitleKey(tmdGen, PublishUtil.getChannelTitleID(titleID), titleKey) : null;

        Map contentAttr = null;

        if (isUpdate) {
            // when updating an previously published title, make sure certain
            // crucial information is consistent.
            if (lastResp == null)
                throw new BroadOnException("Update operation requires last upload package");
            TitleMetaData lastTMD = new TitleMetaData(lastResp.getTitleMetaData());
            if (titleID != lastTMD.getTitleId())
                throw new BroadOnException("Title ID does not match upload package");

            if (! Arrays.equals(encryptedTitleKey, lastResp.getEncryptedTitleKey()) ||
                ! Arrays.equals(encryptedChannelKey, lastResp.getEncryptedChannelKey())) {
                throw new BroadOnException("Encryption password is different from previous version");
            }
            
            contentAttr = PublishUtil.genContentAttributesMap(lastResp.getContentLink(), 
                                                              lastTMD.getContents(), titleID);
            tmdVersion = lastTMD.getTMDVersion() + 1;
        }
        
        if (minTMDVersion > tmdVersion)
            tmdVersion = minTMDVersion;

        resp.setAction(req.getAction());
        resp.setTitleType(req.getTitleType());
        resp.setTitleName(req.getTitleName());
        resp.setProductCode(req.getProductCode());
        resp.setEncryptedTitleKey(encryptedTitleKey);
        resp.setEncryptedChannelKey(encryptedChannelKey);
        resp.setAllowCommonTicket(req.getAllowCommonTicket());
        resp.setPublishDate(System.currentTimeMillis());
        resp.setAuthorization(sm.getAuthorizer());

        long[] cids = processContents(req.getContents(), contentFiles, tmdGen, resp, outDir, zipout, contentAttr);

        byte[] tmd = tmdGen.generateMetaData(tmdTemplate.getTmd(), cids, titleID, tmdVersion);
        tmd = sm.signTMD(cpCert.keyName, tmd, tmdGen.signatureOffset, tmdGen.documentStart, tmdGen.signatureSize);
       
        resp.setTitleMetaData(tmd);
        return resp;
    }

    
    // encrypt the content into a temp. file.
    private void createContent(ContentInputStream in, OutputStream out)
        throws IOException
    {
        byte[] buffer = new byte[64 * 1024];
        int n;
        while ((n = in.read(buffer)) >= 0) {
            if (n == 0) // ContentInputSream.read() could return 0 before EOF
                continue;
            out.write(buffer, 0, n);
        }
    }

    private long[] processContents(ContentAttributeType[] contents, File[] contentFiles, 
                                   TMDGenerator tmdGen, UploadRequestType resp,
                                   File outDir, ZipOutputStream zipout, Map contentAttrMap) 
        throws BroadOnException, GeneralSecurityException, IOException
    {
        if (contents == null)
            return new long[0];

        if (contentFiles == null || contentFiles.length != contents.length)
            throw new BroadOnException("Number of content descriptor and attachment mismatched");

        long[] cids = new long[contents.length];
        resp.setContentLink(new ContentLinkType[contents.length]);
        int nextCid = getNextContentId(contentAttrMap);
        
        for (int i = 0; i < contents.length; i++) {
            ContentInputStream in = 
                tmdGen.getEncrypter(new FileInputStream(contentFiles[i]), 
                                    contents[i].getContentIndex(),
                                    dvdHash ? new DvdEncrypter() : null);
            
            int contentID = -1;
            
            if (zipout != null) {
                ZipEntry ze = new ZipEntry(contentFiles[i].getName() + ".cry");
                zipout.putNextEntry(ze);
                createContent(in, zipout);
                in.close();
                
                // check if this is a previously published content with content ID already assigned.
                contentID = checkExistingCID(in, contentAttrMap);
                if (contentID < 0)
                    contentID = nextCid++;
                
                ze.setComment("Content ID: " + Integer.toHexString(contentID));
                zipout.closeEntry();
                
            } else {
                File outputDirectory = (outDir == null) ? contentFiles[i].getParentFile() : outDir;
                File tempFile = File.createTempFile("cls", null, outputDirectory);
                tempFile.deleteOnExit();
                FileOutputStream tempOut = new FileOutputStream(tempFile);
                createContent(in, tempOut);
                tempOut.close();
                in.close();
                
                // check if this is a previously published content with content ID already assigned.
                contentID = checkExistingCID(in, contentAttrMap);
                if (contentID < 0)
                    contentID = nextCid++;
                
                String contentFileName = genEncryptedFileName(contentFiles[i], contentID);
                File outFile = new File(outputDirectory, contentFileName);
                if (!tempFile.renameTo(outFile)) {
                    tempFile.delete();
                    throw new IOException("Cannot create output file " + outFile.getPath());
                }
            }
            
            
            ContentLinkType link = new ContentLinkType();
            link.setCheckSum(in.getMd5HashString());
            link.setContentId(Integer.toHexString(contentID));
            link.setContentName(contents[i].getContentObjectName());
            link.setContentType(contents[i].getContentObjectType());
            resp.setContentLink(i, link);

            cids[i] = contentID;
        }

        return cids;
    }

    
    private String genEncryptedFileName(File source, int contentID)
    {
        String name = source.getName();
        int idx = name.lastIndexOf('-');
        if (idx >= 0)
            name = name.substring(idx + 1);
        return intToHexString(contentID) + '-' + name + ".cry";
    }

    // In case of an update, figure out the highest content ID previously assigned.
    private int getNextContentId(Map contentAttrMap)
    {
        if (contentAttrMap == null)
            return 0;

        int cid = 0;
        Collection attrSet = contentAttrMap.values();
        for (Iterator iter = attrSet.iterator(); iter.hasNext();) {
            ContentAttributes element = (ContentAttributes) iter.next();
            if (element.getRawContentId() >= cid)
                cid = (int) (element.getRawContentId() + 1);
        }
        return cid;
    }

    
    // In case of update, check if a content file has previously been published.
    // Use its checksum to map to the old content attributes.
    private int checkExistingCID(ContentInputStream in, Map contentAttrMap) 
        throws BroadOnException
    {
        if (contentAttrMap == null)
            return -1;

        ContentAttributes attr = (ContentAttributes) contentAttrMap.get(in.getMd5HashString());
        return (attr == null) ? -1 : (int) attr.getRawContentId();
    }

    
    private String minWidth(String str, int width, char filler)
    {
        int n = str.length();
        if (n >= width)
            return str;
        StringBuffer buf = new StringBuffer(str);
        char[] addition = new char[width - n];
        Arrays.fill(addition, filler);
        buf.insert(0, addition);
        return buf.toString();
    }

    private String intToHexString(int value)
    {
        return minWidth(Integer.toHexString(value), 8, '0');
    }

    public int getTmdVersion()
    {
        return tmdVersion;
    }

    public byte[] getCaCert()
    {
        return caCert.rawCert;
    }

    public byte[] getCpCert()
    {
        return cpCert.rawCert;
    }

    public byte[] getXsCert()
    {
        return xsCert.rawCert;
    }
    
    class CertInfo 
    {
        static final String CERT_SUFFIX = "-cert.raw";
        byte[] rawCert;
        BCCCertV2_RSA parsedCert;        
        String keyName;
        File file;

        
        CertInfo(Properties prop, String key) throws GeneralSecurityException, IOException
        {
            String fileName = prop.getProperty(key);
            if (fileName == null)
                throw new IOException("Missing defintion for " + key);
            
            file = new File(fileName);
            rawCert = readFile(file);
            try {
                parsedCert = (BCCCertV2_RSA) BCCCert.getInstance(rawCert);
            } catch (ClassCastException e) {
                throw new GeneralSecurityException("Invalid cert: " + fileName);
            }
            int idx = file.getName().lastIndexOf(CERT_SUFFIX);
            if (idx < 0)
                throw new IOException("Invalid cert file suffix: " + fileName);
            keyName = file.getName().substring(0, idx);
        }
        
        private byte[] readFile(File file) throws IOException
        {
            byte[] buf = new byte[(int) file.length()];
            FileInputStream in = new FileInputStream(file);
            try {
                in.read(buf);
            } finally {
                in.close();
            }
            return buf;
        }
    }
    

}

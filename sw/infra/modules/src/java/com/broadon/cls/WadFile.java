package com.broadon.cls;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;


public class WadFile
{
    private static final int WAD_FILE_ALIGNMENT = 64 - 1;
    
    private byte[] caCert;
    private byte[] xsCert;
    private byte[] cpCert;
    private byte[] tmd;
    private byte[] eTicket;

    private native byte[] genWadHeader(int certSize, int crlSize, int eTicketSize, 
                                       int tmdSize, long contentSize);

    public WadFile(byte[] caCert, byte[] xsCert, byte[] cpCert, byte[] tmd)
    {
        this.caCert = caCert;
        this.xsCert = xsCert;
        this.cpCert = cpCert;
        this.tmd = tmd;
    }

    public void setETicket(byte[] eTicket)
    {
        this.eTicket = eTicket;
    }

    public void genWadFile(File wadFile, File zipFile) throws IOException
    {
        boolean hasTicket = (eTicket != null && eTicket.length > 0);
        
        int certSize = cpCert.length + caCert.length;
        if (hasTicket)
            certSize += xsCert.length;
        // TODO support crl file.
        int crlSize = 0;

        ZipFile zipIn = new ZipFile(zipFile);
        long contentSize = getContentSize(zipIn);
        byte[] header = genWadHeader(certSize, crlSize, hasTicket ? eTicket.length : 0,
                                     tmd.length, contentSize);
        
        FileOutputStream out = new FileOutputStream(wadFile);
        
        long pos = 0;
        out.write(header);
        pos += header.length;
        
        pos += alignFile(out, pos);
        out.write(cpCert);
        if (hasTicket)
            out.write(xsCert);
        out.write(caCert);
        pos += certSize;
        
        if (hasTicket) {
            pos += alignFile(out, pos);
            out.write(eTicket);     
            pos += eTicket.length;
        }
        
        pos += alignFile(out, pos);
        out.write(tmd);
        pos += tmd.length;
        
        pos = writeContents(pos, out, zipIn);
        
        zipIn.close();
        out.close();
    }

    
    private long writeContents(long pos, FileOutputStream out, ZipFile zipIn) throws IOException
    {
        byte[] buffer = new byte[64*1024];
        for (Enumeration e = zipIn.entries(); e.hasMoreElements(); ) {
            ZipEntry entry = (ZipEntry) e.nextElement();
            if (GenerateLicense.UPLOAD_ZIP_ENTRY.equals(entry.getName()))
                continue;

            pos += alignFile(out, pos);
            InputStream in = zipIn.getInputStream(entry);
            int n;
            while ((n = in.read(buffer)) > 0) {
                out.write(buffer, 0, n);
                pos += n;
            }
        }  
        pos += alignFile(out, pos);
        return pos;
    }

    private long alignFile(FileOutputStream out, long pos) throws IOException
    {
        int n = (int) (((pos + WAD_FILE_ALIGNMENT) & ~WAD_FILE_ALIGNMENT) - pos);
        for (int i = 0; i < n; ++i)
            out.write(0);
        return n;
    }

    private long getContentSize(ZipFile zipIn)
    {
        long total = 0;
        // count the total size of all contents;
        for (Enumeration e = zipIn.entries(); e.hasMoreElements(); ) {
            ZipEntry entry = (ZipEntry) e.nextElement();
            if (GenerateLicense.UPLOAD_ZIP_ENTRY.equals(entry.getName()))
                continue;
            
            total += (entry.getSize() + WAD_FILE_ALIGNMENT) & ~WAD_FILE_ALIGNMENT;
        }
        return total;
    }
    

}

package com.broadon.cls;

import com.broadon.cps.Encrypter;

public class DvdEncrypter implements Encrypter
{
    public static native int getEncryptionBufferSize();
    
    public static native byte[] encryptDvdInit(int index, byte[] titleAesKey);

    public static native int encryptDvdUpdate(byte[] handle, byte[] inBuf, int inOffset, int len,
                                              byte[] outBuf, int outOffset); 

    public static native int encryptDvdFinal(byte[] handle, byte[] outBuf, int off, byte[] hash);

    public byte[] encryptInit(int index, byte[] titleAesKey)
    {
        return encryptDvdInit(index, titleAesKey);
    }

    public int encryptUpdate(byte[] handle, byte[] inBuf, int inOffset, int len, byte[] outBuf, int outOffset)
    {
       return encryptDvdUpdate(handle, inBuf, inOffset, len, outBuf, outOffset);
    }

    public int encryptFinal(byte[] handle, byte[] outBuf, int off, byte[] hash)
    {
        return encryptDvdFinal(handle, outBuf, off, hash);
    }

    public int getBufferSize()
    {
       return getEncryptionBufferSize();
    }
    
}

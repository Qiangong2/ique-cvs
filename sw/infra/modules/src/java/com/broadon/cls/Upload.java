package com.broadon.cls;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.xml.rpc.ServiceException;

import com.broadon.wsapi.cps.PublishPortType;
import com.broadon.wsapi.cps.PublishServiceLocator;
import com.broadon.wsapi.cps.UploadRequestType;
import com.broadon.wsapi.cps.UploadResponseType;

public class Upload
{
    private PublishPortType publisher;
    
    public Upload(String cps) throws MalformedURLException, ServiceException
    {
        URL endpointURL = new URL("http://" + cps + "/cps/services/PublishSOAP");
        PublishServiceLocator cpsService = new PublishServiceLocator();
        publisher = cpsService.getPublishSOAP(endpointURL); 
    }
    
    public UploadResponseType submit(String packageFileName)
        throws IOException, ClassNotFoundException
    {
        ZipFile zip = new ZipFile(packageFileName);
        
        UploadRequestType request = getUploadRequest(zip);
        
        DataHandler[] attachments = getAttachments(zip);
        
        try {
            return publisher.upload(request, attachments);
        } finally {
            zip.close();
        }
    }
    
    
    private DataHandler[] getAttachments(ZipFile zip)
    {
        ArrayList handlers = new ArrayList(zip.size());
        
        for (Enumeration e = zip.entries(); e.hasMoreElements();) {
            ZipEntry entry = (ZipEntry) e.nextElement();
            if (GenerateLicense.UPLOAD_ZIP_ENTRY.equals(entry.getName()))
                continue;
            
            DataHandler hdlr = new DataHandler(new ZipEntryDataSource(zip, entry));
            handlers.add(hdlr);
        }
        
        return (DataHandler[]) handlers.toArray(new DataHandler[handlers.size()]);
    }
    

    private UploadRequestType getUploadRequest(ZipFile zip) 
        throws IOException, ClassNotFoundException
    {
        ZipEntry entry = zip.getEntry(GenerateLicense.UPLOAD_ZIP_ENTRY);
        
        // TODO: check signature
        
        ObjectInputStream in = new ObjectInputStream(zip.getInputStream(entry));
        try {
            return (UploadRequestType) in.readObject();
        } finally {
            in.close();
        }
    }
    

    private class ZipEntryDataSource implements DataSource
    {
        private ZipFile zip;
        private ZipEntry entry;

        public ZipEntryDataSource(ZipFile zip, ZipEntry entry)
        {
            this.zip = zip;
            this.entry = entry;
        }

        public String getContentType()
        {
            return "application/octet-stream";
        }

        public InputStream getInputStream() throws IOException
        {
            return zip.getInputStream(entry);
        }

        public String getName()
        {
            return entry.getName();
        }

        public OutputStream getOutputStream() throws IOException
        {
            throw new IOException("getOutputStream not support for " + getClass().getName());
        }
        
    }

    
    public static void main(String[] args)
    {
        if (args.length < 1)
            usage();
        
        try {
            Upload uploader = new Upload(args[0]);
            for (int i = 1; i < args.length; ++i) {
                System.out.print("Uploading " + args[i] + " ..."); System.out.flush();
                UploadResponseType resp = uploader.submit(args[i]);
                if (resp.getErrorCode() != 0) {
                    System.err.println(" fail -- \n\t" + resp.getErrorMessage());
                    if (args.length - i > 1)
                        System.err.println("Aborting upload for rest of input files");
                    System.exit(1);
                } else {
                    System.out.println(" Done.");
                }
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private static void usage()
    {
        System.err.println("Usage: Upload <host:port> [upload files ...]");
        System.exit(1);
    }

}

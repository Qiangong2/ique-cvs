package com.broadon.cls;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.Properties;

/**
 * This version support loading of property files that contain unicode character.
 *
 */
public class UnicodeProperties extends Properties
{
    /* (non-Javadoc)
     * @see java.util.Properties#load(java.io.InputStream)
     */
    public synchronized void load(InputStream in) throws IOException
    {
        // convert the input stream first into Unicode escape sequences
        // before loading.
        InputStreamReader rdr = new InputStreamReader(in, "UTF-8");
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(buf);
        try {
            int n;
            while ((n = rdr.read()) >= 0) {
                if (n < 128)
                    buf.write(n);
                else {
                    String s = Integer.toHexString(n);
                    switch (s.length()) {
                    case 4: out.print("\\u" + s); break;
                    case 3: out.print("\\u0" + s); break;
                    case 2: out.print("\\u00" + s); break;
                    default:
                        throw new IOException("Invalid Unicode");
                    }
                }
            }
        } finally {
            rdr.close();
            out.close();
        }
        
        super.load(new ByteArrayInputStream(buf.toByteArray()));
    }

    /**
     * @param args
     */
    public static void main(String[] args)
    {
       try {
           UnicodeProperties prop = new UnicodeProperties();
           prop.load(new FileInputStream(args[0]));
           System.out.println("TITLE_NAME = " + prop.getProperty("TITLE_NAME"));
       } catch (Exception e) {
           e.printStackTrace();
       }
    }

}

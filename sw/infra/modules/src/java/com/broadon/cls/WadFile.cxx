#include <netinet/in.h>

#include "estypes.h"
#include "WadFile.h"

typedef struct
{
    u32 hdrSize;
    u8  wadType[2];
    u16 wadVersion;
    u32 certSize;
    u32 crlSize;
    u32 ticketSize;
    u32 tmdSize;
    u32 contentSize;
    u32 metaSize;
} WADHeader;



JNIEXPORT jbyteArray JNICALL
Java_com_broadon_cls_WadFile_genWadHeader(JNIEnv* env, jobject,
					  jint certSize, jint crlSize,
					  jint eTicketSize, jint tmdSize,
					  jlong contentSize)
{
    WADHeader hdr;
    hdr.hdrSize = htonl(sizeof(WADHeader));
    hdr.wadType[0] = 'I';
    hdr.wadType[1] = 's';
    hdr.wadVersion = 0;
    hdr.certSize = htonl(certSize);
    hdr.crlSize = htonl(crlSize);
    hdr.ticketSize = htonl(eTicketSize);
    hdr.tmdSize = htonl(tmdSize);
    hdr.contentSize = htonl((unsigned int)contentSize);
    hdr.metaSize = 0;

    jbyteArray result = env->NewByteArray(sizeof(WADHeader));
    if (result) {
	env->SetByteArrayRegion(result, 0, sizeof(WADHeader),
				reinterpret_cast<jbyte*>(&hdr));
    }

    return result;
}
    

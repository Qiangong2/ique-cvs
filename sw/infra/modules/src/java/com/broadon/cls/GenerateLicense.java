package com.broadon.cls;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import com.broadon.cps.ContentMetaData;
import com.broadon.cps.TitleMetaData;
import com.broadon.exception.BroadOnException;
import com.broadon.wsapi.cps.ActionType;
import com.broadon.wsapi.cps.ContentAttributeType;
import com.broadon.wsapi.cps.PublishRequestType;
import com.broadon.wsapi.cps.UploadRequestType;
import com.ncipher.nfast.NFException;

public class GenerateLicense
{
    private static final String REQUESTER_ID = "LOCAL_CPS";
    private static String CONTENT_TYPE = "GAME";
   
    private static final String CONFIG_PROPERTIES = "/conf/CLSconfig.properties";
    private static final String CLS_BASE = "CLS_BASE";
    private static final String DEFAULT_CLS_BASE = "/opt/broadon/pkgs/cls";
    private static final String CLS_LIB = "/lib/cls.so";
    
    protected static final String TITLE_PROPERTY_KEY = "properties";
    private static final String TITLE_NAME_KEY = "TITLE_NAME";
    private static final String TITLE_ID_KEY = "TITLE_ID";
    protected static final String TITLE_TYPE_KEY = "TITLE_TYPE";
    private static final String DEFAULT_TITLE_TYPE = "RVL_GAME";
    private static final String SUBSCRIPT_TITLE_TYPE = "SUBSCRIPT";
    private static final String COMMON_TICKET_KEY = "ALLOW_COMMON_TICKET";
    private static final String DEFAULT_COMMON_TICKET = "0";
    private static final String PASSWORD_KEY = "PASSWORD";
    private static final String IS_SUBSCRIPT_KEY = "IS_SUBSCRIPT";
    
    private static final String OUTPUT_FILE_SUFFIX = ".cls";
    protected static final String UPLOAD_ZIP_ENTRY = "UploadRequest";
    private static final String TMD_TEMPLATE = "tmd.template";
    private static final String TICKET_TEMPLATE = "eticket.template";
    private static final String TMD_FILE_SUFFIX = ".tmd";
    private static final String ETICKET_FILE_SUFFIX = ".eticket";
    private static final String CA_CERT_FILE_NAME = "CA-cert.raw";
    private static final String CP_CERT_FILE_NAME = "CP-cert.raw";
    private static final String XS_CERT_FILE_NAME = "XS-cert.raw";
    private static final String WAD_FILE_SUFFIX = ".wad.out";
    
    
    private License license;
    private PrintStream out;
    private boolean splitOutput;
    private boolean privilege;
    private boolean overrideTMD;        // allow title ID and version in TMD to be overriden.

    /**
     * Intializes the Publish request with all the hard-coded values.
     * 
     * This method is called after all data check is performed.
     * 
     * @param req
     *            The Publish Request
     */
    private void initRequest(PublishRequestType req)
    {
        req.setRequesterId(REQUESTER_ID);        
    } 
    
    /**
     * Intializes the content attributes in the request.
     * 
     * @param req
     *            The Publish Request
     * @param contentList
     *            Array of all contents 
     * @param tmdTemplate 
     * @throws BroadOnException 
     * @throws Exception
     */
    private void initContent(PublishRequestType req, String[] contentList, TitleMetaData tmdTemplate) 
        throws BroadOnException
    {
        ContentAttributeType[] contents = new ContentAttributeType[contentList.length];
        ContentMetaData[] tmdContents = tmdTemplate.getContents();        
        
        for (int i = 0; i < contentList.length; i++) {            
            ContentAttributeType contentAttr = new ContentAttributeType();
            contentAttr.setContentIndex(tmdContents[i].getIndex());
            String objName = contentList[i].substring(contentList[i].indexOf("-")+1);
            contentAttr.setContentObjectName(objName);
            contentAttr.setContentObjectType(CONTENT_TYPE);
            contents[i] = contentAttr;
        }
        
        req.setContents(contents);        
    }
    
  
    /**
     * Intializes the content objects in the request.
     * @param titleDir  Path to the title directory
     * @param contentList Array of all contents 
     * @return Array of <code>File</code> objects
     * @throws IOException
     */
    private File[] initContentFile(File titleDir, String[] contentList) 
        throws IOException 
    {
        File[] files = new File[contentList.length];
        for (int i = 0; i < contentList.length; i++) {
            files[i] = new File(titleDir, contentList[i]);
            if (!files[i].canRead())
                throw new IOException("Can't read object file " + files[i].getCanonicalPath());
        }
        return files;
    }
    
    private void writeFile(byte[] buf, String fileName, File dir) throws IOException
    {
        if (buf == null || buf.length == 0)
            return;

        out.println("\nSaving " + fileName + " ...");
        
        File tempFile = File.createTempFile("cls", null, dir);
        tempFile.deleteOnExit();
        FileOutputStream fileOut = new FileOutputStream(tempFile);
        try {
            fileOut.write(buf);
        } finally {
            fileOut.close();
        }
        
        File dest = new File(dir, fileName);
        tempFile.renameTo(dest);
    }
   
    /**
     * Obtains a sorted array of contents in the title directory.
     * 
     * @param contentDir
     *            File pointer to the content directory
     * @param tmdTemplate
     *            Input TMD template file
     * 
     * @return Array of content files names referenced by the TMD template
     *         sorted to the order they specified in the TMD template file.
     * @throws BroadOnException 
     */
    private String[] getContentList(File contentDir, TitleMetaData tmdTemplate) 
        throws BroadOnException
    {
        FilenameFilter filter = new FilenameFilter() {
            public boolean accept(File contents, String name) {
                return (name.indexOf("-") == 3);
            }
        };
        
        String[] files = contentDir.list(filter);
        if (files == null || files.length == 0)
            return new String[0];
        
        // derive the content idx from the file names
        short[] contentIdx = new short[files.length];
        for (int i = 0; i < files.length; i++) {
            try {
                contentIdx[i] = Short.parseShort(files[i].substring(0, files[i].indexOf('-')));
            } catch (NumberFormatException e) {
                contentIdx[i] = -1;
            }
        }
        
        ContentMetaData[] contents = tmdTemplate.getContents();
        String[] result = new String[contents.length];
        matched:
        for (int i = 0; i < contents.length; ++i) {
            short idx = contents[i].getIndex();
            for (int j = 0; j < contentIdx.length; j++) {
                if (idx == contentIdx[j]) {
                    result[i] = files[j];
                    continue matched;
                }
            }
            throw new BroadOnException("Missing content file for index " + idx);
        }
        
        return result;
    }

    /**
     * Prompts the user for encryption password and again for verification.
     * 
     * @throws Exception
     * @return The Encryption Password
     * @throws IOException 
     * @throws BroadOnException 
     */
    private String getEncryptionPassword() throws IOException, BroadOnException
    {
        String pswd = null;
        
        out.print("\nEnter encryption password: ");
        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        pswd = br.readLine();
        if (pswd == null || pswd.length() == 0)
            throw new BroadOnException("Encryption password must not be empty!");
            
        out.print("Confirm encryption password: ");
        
        String pswd2 = br.readLine();
        
        if (!pswd.equals(pswd2))
            throw new BroadOnException("ERROR: Confirm encryption password failed!");

        return pswd;
    }    
    
    public void genLicense(File titleDir, boolean actFlag, long titleId, String titleName, String titleType, 
                           String encPassword, boolean isSubscription)
        throws BroadOnException, IOException, GeneralSecurityException, NFException, ClassNotFoundException 
    {
        File[] contentDirs = getContentDirs(titleDir);
        
        String actionType = (contentDirs.length == 1) ? "Create" : "Update"; 
        
        TitleMetaData tmdTemplate = loadTmdTemplate(contentDirs[0]);
        
        validateTmdTemplate(tmdTemplate);
        
        if (! overrideTMD && titleId != tmdTemplate.getTitleId())
            throw new BroadOnException("Title ID in properties file is different from that in the input.  Input has " + 
                                       Long.toHexString(tmdTemplate.getTitleId()));
        
        // print all input parameters
        out.println("\nTitle Name: " + titleName);
        out.println("Product ID: " + titleDir.getName());
        out.println("Title Directory: " + titleDir + '/' + contentDirs[0].getName());
        out.println("Title Id: " + Long.toHexString(titleId));
        out.println("Title Type: " + titleType);
        out.println("OS Title Id: " + Long.toHexString(tmdTemplate.getOsTitleId()));
        out.println("Action: " + actionType);
        out.println("Access Rights: " + Integer.toHexString(tmdTemplate.getAccessRights()));
        out.println("Allow Common Ticket: " + actFlag);    
        if (isSubscription)
            out.println("Use channel encryption key: true");
        
       
        if (encPassword == null)
            encPassword = getEncryptionPassword();
        else
            out.println("\nUsing password from properties file");
        
        PublishRequestType req = new PublishRequestType();
        
        out.println("\nInitializing Request...");
        initRequest(req);
        req.setAction(ActionType.fromString(actionType));
        req.setEncryptionPassword(encPassword);
        req.setTitleName(titleName);
        req.setTitleType(titleType);
        req.setProductCode(titleDir.getName());
        req.setAllowCommonTicket(Boolean.valueOf(actFlag));
        
        File[] contentFiles = null;
        if (! titleType.equals(SUBSCRIPT_TITLE_TYPE)) {
            String[] contentList = getContentList(contentDirs[0], tmdTemplate);
            if (contentList.length > 0) {
                initContent(req, contentList, tmdTemplate);
                contentFiles = initContentFile(contentDirs[0], contentList);
            } else {
                throw new BroadOnException("Failed!\n\n*** ERROR: No content to publish in \"" +
                        contentDirs[0] + "\"! ***\n");
            }
        } else
            req.setContents(null);
        
        UploadRequestType prevUpload = (contentDirs.length == 1) ? null : loadLastPackage(contentDirs[1]);
        
        // derive the min. TMD version from the content directory name
        int minTMDVersion = fileNameToVersion(contentDirs[0]); 
        if (! overrideTMD && (minTMDVersion != tmdTemplate.getTMDVersion()))
            throw new BroadOnException("Inconsistent version number implied by directory name " + 
                    contentDirs[0].getName() + " -- input has version " + tmdTemplate.getTMDVersion()); 
            
        if (tmdTemplate.getTMDVersion() > minTMDVersion)
            minTMDVersion = tmdTemplate.getTMDVersion();
        
        // Are we generating a single zip file or separated output files?
        ZipOutputStream zipOut = null;
        File tempFile = null;
        if (! splitOutput) {
            tempFile = File.createTempFile("cls", null, contentDirs[0]);
            tempFile.deleteOnExit();
            zipOut = new ZipOutputStream(new FileOutputStream(tempFile));
            zipOut.setLevel(0);
        }
        
        UploadRequestType uploader = license.license(tmdTemplate, req, isSubscription, 
                                                     contentFiles, contentDirs[0], zipOut, 
                                                     titleId, minTMDVersion, prevUpload);
       
        if (! overrideTMD && license.getTmdVersion() != fileNameToVersion(contentDirs[0])) {
            throw new BroadOnException("Inconsistent version number implied by directory name " + 
                    contentDirs[0].getName() + " -- minimum version is v" + license.getTmdVersion());
        }
        if (titleId == 0x0000000100000001L && 
            license.getTmdVersion() != tmdTemplate.getOsTitleId())
            throw new BroadOnException("Boot2's sysVersion inconsistent with title version.");
                
            
        
        String outFileBaseName = titleDir.getName() + "-v" + license.getTmdVersion();
        File outFile = new File(contentDirs[0], outFileBaseName + OUTPUT_FILE_SUFFIX);
        
        if (splitOutput) {
            byte[] ticket = license.genCommonTicket(titleId, loadTicketTemplate(contentDirs[0]),
                                                   (isSubscription ?   
                                                    uploader.getEncryptedChannelKey() : 
                                                    uploader.getEncryptedTitleKey()));

            writeFile(ticket, outFileBaseName + ETICKET_FILE_SUFFIX, contentDirs[0]);
        
            writeFile(uploader.getTitleMetaData(), 
                      titleDir.getName() + "-v" + license.getTmdVersion() + TMD_FILE_SUFFIX,
                      contentDirs[0]);
            
            // now dump the certs
            writeFile(license.getCaCert(), CA_CERT_FILE_NAME, contentDirs[0]);
            writeFile(license.getXsCert(), XS_CERT_FILE_NAME, contentDirs[0]);
            writeFile(license.getCpCert(), CP_CERT_FILE_NAME, contentDirs[0]);

            // write the upload pakcage
            tempFile = File.createTempFile("cls", null, contentDirs[0]);
            tempFile.deleteOnExit();
            ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream(tempFile));
            output.writeObject(uploader);
            output.close();
            tempFile.renameTo(outFile);
        } else {
            // prepare for generating the WAD file.
            // NOTE: we need to set up the WadFile class first because the
            // eTicket and TMD field of the uploader will be overwritten with
            // the cert chain appended. However, the WAD file itself is written
            // out after the zip file because the encrypted contents are
            // directly copied from the zip to the WAD file.
            WadFile wad = new WadFile(license.getCaCert(), license.getXsCert(), license.getCpCert(),
                                      uploader.getTitleMetaData());
            
            if (actFlag) {
                byte[] ticket = license.genCommonTicket(titleId, 
                                                        loadTicketTemplate(contentDirs[0]),
                                                        (isSubscription ? 
                                                         uploader.getEncryptedChannelKey() : 
                                                         uploader.getEncryptedTitleKey()));
                wad.setETicket(ticket);
                setCommonTicket(ticket, license, uploader);
            }
            
            // add cert chain to the TMD.
            setTMD(uploader);
            
            ZipEntry ze = new ZipEntry(UPLOAD_ZIP_ENTRY);
            zipOut.putNextEntry(ze);
            ByteArrayOutputStream buf = new ByteArrayOutputStream();
            ObjectOutputStream objOut = new ObjectOutputStream(buf);
            objOut.writeObject(uploader);
            objOut.close();
            zipOut.write(buf.toByteArray());
            zipOut.closeEntry();
            zipOut.close();
            
            if (!tempFile.renameTo(outFile)) {
                tempFile.delete();
                throw new IOException("Cannot create output file " + outFile.getPath());
            }
            
            File wadFile = new File(contentDirs[0], outFileBaseName + WAD_FILE_SUFFIX);
            wad.genWadFile(wadFile, outFile);
        }
    }

    
    /**
     * Concatinate the cert chain to the TMD field of uploader.
     * @param uploader
     * @throws IOException 
     */
    private void setTMD(UploadRequestType uploader) throws IOException
    {
        byte[] rawTMD = uploader.getTitleMetaData();
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        buf.write(rawTMD);
        buf.write(license.getCpCert());
        buf.write(license.getCaCert());
        uploader.setTitleMetaData(buf.toByteArray());
    }

    private void setCommonTicket(byte[] ticket, License license, UploadRequestType uploader) 
        throws GeneralSecurityException, NFException, IOException
    {
        ByteArrayOutputStream tickAndCert = new ByteArrayOutputStream();
        tickAndCert.write(ticket);
        tickAndCert.write(license.getXsCert());
        tickAndCert.write(license.getCaCert());
        uploader.setCommonETicket(tickAndCert.toByteArray());
       
    }

    
    private boolean isSystemApps(long id)
    {
        long channel = id & 0xFFFFFFFF00000000l;
        return (channel == 0x100000000L || channel == 0x200000000L);
    }
    
    private void validateTmdTemplate(TitleMetaData tmdTemplate) throws BroadOnException
    {
        // TODO:  change the following two warnings to exception for production system.
        if (! isSystemApps(tmdTemplate.getOsTitleId()))
            System.out.println("\n*** Warning: " + Long.toHexString(tmdTemplate.getOsTitleId()) + 
                               " is not a valid OS version number. ***\n");
        int gid = tmdTemplate.getGroupId();
        if (gid < 1 || (! isSystemApps(tmdTemplate.getTitleId()) && gid < 2))
            System.out.println("\n*** Warning: Non system applications cannot have group ID < 2 ***\n");
        
        if (! privilege && tmdTemplate.getAccessRights() != 0)
            throw new BroadOnException("Privilege access requires -privilege flag");
    }

    private UploadRequestType loadLastPackage(File prevDir) 
        throws BroadOnException, IOException, ClassNotFoundException
    {
        FilenameFilter filter = new FilenameFilter() {
            public boolean accept(File dir, String name)
            {
                return name.endsWith(OUTPUT_FILE_SUFFIX);
            }
        };
        
        File[] file = prevDir.listFiles(filter);
        if (file.length != 1)
            throw new BroadOnException("Cannot locate previous upload package in " + 
                                       prevDir.getPath() + (file.length < 1 ? "" : ": more than one found"));
        
        // Check if it is a zip file or regular file
        try {
            ZipFile zipIn = new ZipFile(file[0]);
            ZipEntry ze = zipIn.getEntry(UPLOAD_ZIP_ENTRY);
            try {
                if (ze != null) {
                    ObjectInputStream in = new ObjectInputStream(zipIn.getInputStream(ze));
                UploadRequestType req = (UploadRequestType) in.readObject();
                in.close();
                return req;
                } else 
                    throw new BroadOnException("Cannot read previos upload package " + file[0].getAbsolutePath() +
                                               " - invalid format");
            } finally {
                zipIn.close();
            }
        } catch (ZipException e) {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(file[0]));
            UploadRequestType req = (UploadRequestType) in.readObject();
            in.close();
        
            return req;
        }
    }

    private File[] getContentDirs(File titleDir) throws BroadOnException
    {
        FilenameFilter filter = new FilenameFilter() {
            public boolean accept(File contents, String name) {
                if (name.charAt(0) != 'v')
                    return false;
                try {
                    Integer.parseInt(name.substring(1));
                } catch (NumberFormatException e) {
                    return false;
                }
                File file = new File(contents, name);
                return file.isDirectory();
            }
        };
        
        File[] files = titleDir.listFiles(filter);
        
        if (files.length == 1)
            return files;
        else if (files.length == 0)
            throw new BroadOnException("Missing or bad version directory");
        
        Comparator comp = new Comparator() {
            public int compare(Object x, Object y) {
                int vx = fileNameToVersion((File) x);
                int vy = fileNameToVersion((File) y);
                return vy - vx;
            }
        };
        
        Arrays.sort(files, comp);
       
        return files;
    }
    
    private int fileNameToVersion(File file) {
        try {
            return Integer.parseInt(file.getName().substring(1));
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    private byte[] loadTicketTemplate(File contentDir) throws IOException
    {
        File file = new File(contentDir, TICKET_TEMPLATE);
        byte[] buf = new byte[(int) file.length()];
        FileInputStream in = new FileInputStream(file);
        in.read(buf);
        in.close();
        return buf;
    }
    

    private TitleMetaData loadTmdTemplate(File contentDir) 
        throws BroadOnException, IOException
    {
        File file = new File(contentDir, TMD_TEMPLATE);
        byte[] buf = new byte[(int) file.length()];
        FileInputStream in = new FileInputStream(file);
        in.read(buf);
        in.close();
        
        return new TitleMetaData(buf);
    }

    public GenerateLicense(PrintStream traceStream, boolean splitOutput, boolean privilege, 
                           boolean dvdHash, boolean overrideTMD) 
        throws GeneralSecurityException, NFException, ClassNotFoundException, IOException
    {
        this.splitOutput = splitOutput;
        this.privilege = privilege;
        this.overrideTMD = overrideTMD;
        
        String base = System.getProperty(CLS_BASE, DEFAULT_CLS_BASE);
        System.load(base + CLS_LIB);
        Properties prop = new Properties();
        FileInputStream in = new FileInputStream(base + CONFIG_PROPERTIES);
        try {
            prop.load(in);
        } finally {
            in.close();
        }
        license = new License(prop, dvdHash);
        out = traceStream;
    }
    /**
     * Prints the usage with no additional messsage.
     */
    private static int usage()
    {
       return usage(null);
    }
    
    /**
     * Prints the usage with a specified messsage.
     */
    private static int usage(String message)
    {
        System.out.println("\nUsage: GenerateLicense <product_dir> [-split] [-privilege] [-dvd]");
        
        if (message != null && !message.equals(""))
            System.out.println(message);
                
        return 1;
    }
    
    public static int pseudoMain(String[] args)
    {
        if (args.length < 1)
            return usage();
        
        boolean splitOutput = false;
        boolean privilege = false;
        boolean dvdHash = false;
        boolean tmdOverride = false;
        
        for (int i = 1; i < args.length; ++i) {
            if (args[i].startsWith("-s")) {
                splitOutput = true;
            } else if (args[i].startsWith("-p")) {
                privilege = true;
            } else if (args[i].startsWith("-d")) {
                dvdHash = true;
            } else if (args[i].startsWith("-t")) {
                tmdOverride = true;
            }
        }
        
        File titleDir = new File(args[0]);
        if (! titleDir.isDirectory()) {
            return usage("\n*** ERROR: Directory \"" + args[0] + "\" not found! ***\n");
        }
        
        Properties prop = new UnicodeProperties();
        try {
            FileInputStream in = new FileInputStream(new File(titleDir, TITLE_PROPERTY_KEY));
            prop.load(in);
            in.close();
        } catch (IOException e) {
            return usage(e.getMessage());
        }
        
        boolean actFlag = "1".equals(prop.getProperty(COMMON_TICKET_KEY, 
                                                      DEFAULT_COMMON_TICKET).trim());
        String titleIdString = prop.getProperty(TITLE_ID_KEY).trim();
        String titleName = prop.getProperty(TITLE_NAME_KEY);
        String titleType = prop.getProperty(TITLE_TYPE_KEY, DEFAULT_TITLE_TYPE).trim();
        String encPassword = prop.getProperty(PASSWORD_KEY).trim();
        boolean isSubscription = "1".equals(prop.getProperty(IS_SUBSCRIPT_KEY, "0").trim());
        
        if (titleName == null || titleDir == null || titleType == null || titleIdString == null)
            return usage();
        
        long titleId = -1;
        try {
            titleId = Long.parseLong(titleIdString, 16);
        } catch (NumberFormatException e) {
            return usage("Bad title ID format");
        }
        
        // TODO:  remove these checks when HSM enforcement is supported.
        if ((titleId & 0xFFFFFFFF00000000L) != 0x0000000100000000L &&
            (titleId & 0xFFFFFFFF00000000L) != 0x0000000200000000L &&
            (titleId & 0xFFFF000000000000L) != 0x0001000000000000L &&
            (titleId & 0xFFFF000000000000L) != 0x0002000000000000L) {
            System.out.println("\n*** ERROR: illegal title ID\n" +
                               "*** Must be 0x00000001xxxxxxxx or 0x0001xxxxxxxxxxxx.");
            return 1;
        }
            
        
        if (SUBSCRIPT_TITLE_TYPE.equals(titleType) && (titleId & 0xffffffffL) != 0) {
           System.out.println("\n*** ERROR: SUBSCRIPT title type not supported for this title ***\n*** " +
                              "lower 32 bits of Title ID must be 0 ***\n");
           return 1;
        }
        
        System.out.println("\nContent Licensing");
        System.out.println("===================");
        
        // restrict title_type to be one of "GAME" or "NC_SYS" or "RVL_SYS" or "SUBSCRIPT
        if (!(titleType.equals("GAME") || titleType.equals("RVL_GAME") ||
              titleType.equals("NC_GAME") || titleType.equals("RVL_SYS") || 
              titleType.equals("NC_SYS") || titleType.equals(SUBSCRIPT_TITLE_TYPE)))
            return usage("*** ERROR: Invalid value provided for title type ***" +
                  "\n*** Must be \"GAME\" or \"NC_SYS\" or \"RVL_SYS\" or \"SUBSCRIPT\" but was \"" + titleType + "\" ***\n");
        
        
        if (titleType.equals(SUBSCRIPT_TITLE_TYPE))
            isSubscription = true;
        
        try {
            GenerateLicense gLic = new GenerateLicense(System.out, splitOutput, privilege, dvdHash, tmdOverride);
            gLic.genLicense(titleDir, actFlag, titleId, titleName, titleType, encPassword, isSubscription);
        } catch (Exception e) {
            System.out.println("\n*** ERROR: " + e.getMessage() + " ***\n");
            return 1;
        }  
        
        return 0;
    }
    
    public static void main(String[] args) {
        System.exit(pseudoMain(args));
    }
}

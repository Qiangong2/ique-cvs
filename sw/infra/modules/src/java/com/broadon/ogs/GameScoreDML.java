package com.broadon.ogs;

import java.io.Reader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import oracle.sql.CLOB;

import com.broadon.util.Base64;
import com.broadon.wsapi.ogs.RankedScoreResultType;
import com.broadon.wsapi.ogs.ScoreType;
import com.broadon.wsapi.ogs.ScoreItemType;
import com.broadon.wsapi.ogs.ScoreItemResultType;
import com.broadon.wsapi.ogs.ScoreKeyType;

/**
 * Data manipulation library functions defined here pertain to game scores
 */
public class GameScoreDML
{
    /** Define all SQL statements
     */

    private static final String TABLE_GAME_SCORES = "VNG_ONLINE_GAME_SCORES";    
    private static final String TABLE_GAME_SCORE_OBJECTS = "VNG_ONLINE_GAME_SCORE_OBJECTS";
    private static final String TABLE_ACCOUNTS = "IAS_ACCOUNTS";
    
    private static final String COUNT_SCORE_ITEMS =
        "SELECT count(item_id) FROM " + TABLE_GAME_SCORES + " WHERE account_id = ? " +
        "AND slot_id = ? AND title_id = ? AND game_id = ? AND score_id = ?";
    
    private static final String GET_GAME_SCORE =
        "SELECT gs.account_id, gs.score_time, a.login_name, a.nick_name, gso.score_info, " +
        "gso.score_object_size FROM " + TABLE_GAME_SCORES + " gs, " + 
        TABLE_GAME_SCORE_OBJECTS + " gso, " + TABLE_ACCOUNTS + " a " +
        "WHERE gs.score_object_id = gso.score_object_id " +
        "AND gs.account_id = a.account_id AND gs.account_id = ? AND gs.slot_id = ? " +
        "AND gs.title_id = ? AND gs.game_id = ? AND gs.score_id = ?";
    
    private static final String GET_GAME_SCORE_KEYS =
        "SELECT DISTINCT account_id, slot_id, title_id, game_id, score_id FROM " + 
        TABLE_GAME_SCORES + " WHERE account_id = ?";
    
    private static final String GET_GAME_SCORE_OBJECT =
        "SELECT score_info, score_object_size, score_object FROM " + 
        TABLE_GAME_SCORE_OBJECTS + " WHERE score_object_id = ? AND score_object IS NOT NULL";
    
    private static final String GET_GAME_SCORE_ITEMS =
        "SELECT item_id, score, vngops.getScoreRank(game_id, score_id, item_id, account_id, title_id, slot_id) rank " +
        "FROM " + TABLE_GAME_SCORES + " WHERE account_id = ? AND slot_id = ? AND title_id = ? AND game_id = ? " + 
        "AND score_id = ?";

    private static final String GET_RANKED_GAME_SCORES =
        "SELECT gs.account_id, gs.slot_id, gs.title_id, gs.game_id, gs.score_id, gs.item_id, " +
        "g.rank, g.score, a.login_name, a.nick_name, g.score_time, gso.score_info, " +
        "gso.score_object_size FROM table(SELECT vngops.getRankedScores(?,?,?,?,?) FROM dual) g, " +
        TABLE_GAME_SCORES + " gs, " + TABLE_GAME_SCORE_OBJECTS + " gso, " + TABLE_ACCOUNTS + " a " +
        "WHERE gs.rowid = g.rid AND gs.score_object_id = gso.score_object_id " +
        "AND gs.account_id = a.account_id";
    
    private static final String INSERT_GAME_SCORE =
        "INSERT INTO " +  TABLE_GAME_SCORES + " (account_id, slot_id, title_id, game_id, score_id, " +
        "item_id, score, score_time, score_object_id) " +
        "VALUES (?,?,?,?,?,?,?,BCCUTIL.ConvertTimeStamp(sys_extract_utc(current_timestamp)),?)";
    
    private static final String INSERT_GAME_SCORE_OBJECT =
        "INSERT INTO " +  TABLE_GAME_SCORE_OBJECTS + " (score_object_id, score_info, score_object_size) " +
        "VALUES (?,?,0)";
    
    private static final String UPDATE_GAME_SCORE_OBJECT =
    	"UPDATE " + TABLE_GAME_SCORE_OBJECTS + " SET score_object = ?, score_object_size = ? " +
    	"WHERE score_object_id = ?";
    
    private static final String QUERY_SCORE_OBJECT_ID = 
    	"SELECT score_object_id FROM " + TABLE_GAME_SCORES + " WHERE account_id = ? " +
    	"AND slot_id =?  AND title_id = ? AND game_id = ? AND score_id = ?";
    
    private static final String DELETE_GAME_SCORE = 
    	"DELETE FROM " + TABLE_GAME_SCORES + " WHERE score_object_id = ?";
    
    private static final String DELETE_GAME_SCORE_OBJECT = 
    	"DELETE FROM " + TABLE_GAME_SCORE_OBJECTS + " WHERE score_object_id = ?";
    
    private static final String QUERY_NEXT_SCORE_OBJECT_ID = 
    	"SELECT score_object_id_seq.nextval from dual";
    
    private static final String QUERY_IS_GAME_SCORE_OBJECT_NULL = 
    	"SELECT CASE WHEN score_object IS NULL THEN 0 ELSE 1 END isExisting " +
    	"FROM " + TABLE_GAME_SCORE_OBJECTS + " WHERE score_object_id = ?";

    public static int countScoreItems(Connection conn,
                                      int accountId,
                                      int slotId,
                                      long titleId,
                                      int gameId, 
                                      int scoreId)                    
        throws SQLException
    {
        int count = 0;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {       
            stmt = conn.prepareStatement(COUNT_SCORE_ITEMS);
            
            stmt.setInt(1, accountId);
            stmt.setInt(2, slotId);
            stmt.setLong(3, titleId);
            stmt.setInt(4, gameId);
            stmt.setInt(5, scoreId);
                
            rs = stmt.executeQuery();
            
            if (rs.next())
                count = rs.getInt(1);
        
        } 
        finally {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
        
        return count;
    }

    public static boolean deleteGameScore(Connection conn,
                                          int accountId,
                                          int slotId, 
                                          long titleId, 
                                          int gameId, 
                                          int scoreId) 
        throws SQLException 
    {
        int gsRow = 0;
        int gsoRow = 0;
        PreparedStatement stmt = null;
        
        try {
            int scoreObjectId = queryScoreObjectId(conn, accountId, slotId,
                                          titleId, gameId, scoreId);
            
            if(scoreObjectId > 0)
            {
                stmt = conn.prepareStatement(DELETE_GAME_SCORE);
                stmt.setInt(1, scoreObjectId);
                
                gsRow = stmt.executeUpdate();
                
                stmt.close();
                stmt = null;
                
                stmt = conn.prepareStatement(DELETE_GAME_SCORE_OBJECT);
                stmt.setInt(1, scoreObjectId);
                
                gsoRow = stmt.executeUpdate();
            }
        
        } finally {
            if (stmt != null)
                stmt.close();
        }
        
        return (gsoRow == 1 && gsRow >= 1);
    }

    public static ScoreItemResultType getScore(Connection conn, 
                                               int accountId,
                                               int slotId,
                                               long titleId,
                                               int gameId, 
                                               int scoreId)
        throws SQLException
    {
        ScoreItemResultType scoreItemResult = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {       
            stmt = conn.prepareStatement(GET_GAME_SCORE);
            
            stmt.setInt(1, accountId);
            stmt.setInt(2, slotId);
            stmt.setLong(3, titleId);
            stmt.setInt(4, gameId);
            stmt.setInt(5, scoreId);
                
            rs = stmt.executeQuery();
            
            if (rs.next()) {
                scoreItemResult = new ScoreItemResultType();
                ScoreType score = new ScoreType();
                
                score.setAccountId(rs.getInt("ACCOUNT_ID"));
                score.setLoginName(rs.getString("LOGIN_NAME"));
                score.setNickName(rs.getString("NICK_NAME"));
                score.setSubmitTime(rs.getLong("SCORE_TIME"));
                score.setInfo(rs.getString("SCORE_INFO"));
                score.setObjectSize(rs.getInt("SCORE_OBJECT_SIZE"));
                score.setNumItems(countScoreItems(conn, accountId, slotId, titleId, gameId, scoreId));                
                
                scoreItemResult.setScore(score);
                
                List scoreItemList = getScoreItems(conn, accountId, slotId, titleId, gameId, scoreId);

                if (scoreItemList != null && scoreItemList.size() > 0) {
                    ScoreItemType[] scoreItemWithRank = new ScoreItemType[scoreItemList.size()];
                    scoreItemWithRank = (ScoreItemType[]) scoreItemList.toArray(scoreItemWithRank);
                    
                    scoreItemResult.setScoreItem(scoreItemWithRank);
                
                } else
                    scoreItemResult = null;                
            }
            
        } finally {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
        
        return scoreItemResult;        
    }
    
    public static List getScoreItems(Connection conn,
                                     int accountId,                                     
                                     int slotId,
                                     long titleId,
    				     int gameId,                                            
    				     int scoreId)
    	throws SQLException
    {	
        List scoreItemList = new ArrayList();
        
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {       
            stmt = conn.prepareStatement(GET_GAME_SCORE_ITEMS);
            
            stmt.setInt(1, accountId);            
            stmt.setInt(2, slotId);
            stmt.setLong(3, titleId);
            stmt.setInt(4, gameId);
            stmt.setInt(5, scoreId);
                
            rs = stmt.executeQuery();
            
            while (rs.next()) {
                ScoreItemType scoreItem = new ScoreItemType();
                scoreItem.setItemId(rs.getInt("ITEM_ID"));
                scoreItem.setItemValue(rs.getInt("SCORE"));
                scoreItem.setRank(new Integer(rs.getInt("RANK")));
                
                scoreItemList.add(scoreItem);
            }
            
        } finally {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
        
        return scoreItemList;       
    }

    public static List getScoreKeys(Connection conn,
                                    int accountId,                                     
                                    Integer gameId,                                            
                                    Integer scoreId)
        throws SQLException
    {       
        List scoreKeyList = new ArrayList();
        
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {       
            String sql = GET_GAME_SCORE_KEYS;
            if (gameId != null)
                sql += " AND game_id = " + gameId.intValue();
            if (scoreId != null)
                sql += " AND score_id = " + scoreId.intValue();
           
            stmt = conn.prepareStatement(sql);            
            stmt.setInt(1, accountId);
            
            rs = stmt.executeQuery();
            
            while (rs.next()) {
                ScoreKeyType scorekey = new ScoreKeyType();
                scorekey.setAccountId(rs.getInt("ACCOUNT_ID"));
                scorekey.setSlotId(rs.getInt("SLOT_ID"));
                scorekey.setTitleId(rs.getLong("TITLE_ID"));
                scorekey.setGameId(rs.getInt("GAME_ID"));
                scorekey.setScoreId(rs.getInt("SCORE_ID"));
                
                scoreKeyList.add(scorekey);
            }
        
        } 
        finally {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
    
        return scoreKeyList;       
    }

    public static byte[] getScoreObject(Connection conn, 
    					int accountId,
    					int slotId, 
    					long titleId, 
    					int gameId, 
    					int scoreId)
	throws SQLException, IOException
    {
	byte[] scoreObject = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            int scoreObjectId = queryScoreObjectId(conn, accountId, slotId,
                                        titleId, gameId, scoreId);
            
            if(scoreObjectId > 0) {
                stmt = conn.prepareStatement(GET_GAME_SCORE_OBJECT);
                stmt.setInt(1, scoreObjectId);
                
                rs = stmt.executeQuery();
                if (rs.next()) {                    
                    Clob clob = rs.getClob("SCORE_OBJECT");
                    Reader reader = clob.getCharacterStream();
                    int size = (int)clob.length();
                    char[] buffer = new char[size];
                    
                    reader.read(buffer);
                    scoreObject = new Base64().decode(new String(buffer));                 
                }
            }
            
        } finally {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
        
        return scoreObject;
    }

    
    public static List getRankedGameScore(Connection conn,
    					  Integer accountId,
    					  int gameId, 
    					  int scoreId,
    					  int itemId,
    					  int from,
    					  int count)
    	throws SQLException
    {
        List rankedScoreList = new ArrayList();
        
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {       
            String sql = GET_RANKED_GAME_SCORES;
            if (accountId != null)
                sql += " AND gs.account_id = " + accountId.intValue();
            sql += " ORDER BY g.rank";
            
            stmt = conn.prepareStatement(sql);            
            stmt.setInt(1, gameId);
            stmt.setInt(2, scoreId);
            stmt.setInt(3, itemId);
            stmt.setInt(4, from);
            stmt.setInt(5, count);
            
            rs = stmt.executeQuery();
            
            while (rs.next()) {
                ScoreKeyType scorekey = new ScoreKeyType();
                scorekey.setAccountId(rs.getInt("ACCOUNT_ID"));
                scorekey.setSlotId(rs.getInt("SLOT_ID"));
                scorekey.setTitleId(rs.getLong("TITLE_ID"));
                scorekey.setGameId(rs.getInt("GAME_ID"));
                scorekey.setScoreId(rs.getInt("SCORE_ID"));
                
                ScoreType score = new ScoreType();
                score.setAccountId(rs.getInt("ACCOUNT_ID"));
                score.setLoginName(rs.getString("LOGIN_NAME"));
                score.setNickName(rs.getString("NICK_NAME"));
                score.setSubmitTime(rs.getLong("SCORE_TIME"));
                score.setInfo(rs.getString("SCORE_INFO"));
                score.setObjectSize(rs.getInt("SCORE_OBJECT_SIZE"));
                score.setNumItems(1);
                
                ScoreItemType scoreItem = new ScoreItemType();
                scoreItem.setItemId(rs.getInt("ITEM_ID"));
                scoreItem.setItemValue(rs.getInt("SCORE"));
                scoreItem.setRank(new Integer(rs.getInt("RANK")));
                
                RankedScoreResultType rankedScoreResult = new RankedScoreResultType();
                rankedScoreResult.setScoreKey(scorekey);
                rankedScoreResult.setScore(score);
                rankedScoreResult.setScoreItem(scoreItem);
                
                rankedScoreList.add(rankedScoreResult);
            }        
        } 
        finally {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
    
        return rankedScoreList;
    }
    
    public static boolean insertGameScore(Connection conn,
    				          int accountId,
    				          int slotId,
    				          long titleId,
                                          int gameId,                                            
                                          int scoreId,
                                          ScoreItemType scoreItem[],
                                          String scoreInfo)
        throws SQLException
    {
        int gsRow = 0;
        int gsoRow = 0;
        PreparedStatement stmt = null;
        
        try {
            int scoreObjectId = queryScoreObjectId(conn, accountId, slotId,
                                            titleId, gameId, scoreId);
          
            if (scoreObjectId == -1)
            {
                scoreObjectId = queryNextScoreObjectId(conn);
                if (scoreObjectId != -1) {
                    stmt = conn.prepareStatement(INSERT_GAME_SCORE_OBJECT);       
                    stmt.setInt(1, scoreObjectId);
                    
                    if (scoreInfo != null)
                        stmt.setString(2, scoreInfo);
                    else
                        stmt.setNull(2, Types.VARCHAR);
                    
                    gsoRow = stmt.executeUpdate();
                    
                    stmt.close();
                    
                    
                    if (gsoRow == 1) {    
                        for (int i = 0; i < scoreItem.length; i++) {
                            stmt = null;
                        
                            stmt = conn.prepareStatement(INSERT_GAME_SCORE);       
                            stmt.setInt(1, accountId);
                            stmt.setInt(2, slotId);
                            stmt.setLong(3, titleId);
                            stmt.setInt(4, gameId);
                            stmt.setInt(5, scoreId);
                            stmt.setInt(6, scoreItem[i].getItemId());
                            stmt.setInt(7, scoreItem[i].getItemValue());
                            stmt.setInt(8, scoreObjectId);
                            
                            gsRow += stmt.executeUpdate();
                            stmt.close();
                        }
                    }		
    	        }
            }    
        }
        finally {
            if (stmt != null)
                stmt.close();
        }
		
        return (gsoRow == 1 && gsRow == scoreItem.length);
    }
    
    public static int queryNextScoreObjectId(Connection conn) 
    	throws SQLException
    {
        int scoreObjectId = -1;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {        
            stmt = conn.prepareStatement(QUERY_NEXT_SCORE_OBJECT_ID);
        	
            rs = stmt.executeQuery();
            
            if(rs.next())
        	scoreObjectId = rs.getInt(1);
            
        } finally {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
        
    	return scoreObjectId;    	
    }
    
    public static int queryScoreObjectId(Connection conn,
    					 int accountId,
                                         int slotId,
                                         long titleId,
                                         int gameId,                                            
                                         int scoreId)
	throws SQLException 
    {
	int scoreObjectId = -1;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            stmt = conn.prepareStatement(QUERY_SCORE_OBJECT_ID);
        
            stmt.setInt(1, accountId);
            stmt.setInt(2, slotId);
            stmt.setLong(3, titleId);
            stmt.setInt(4, gameId);
            stmt.setInt(5, scoreId);
		
            rs = stmt.executeQuery();
            
	    if (rs.next())
                scoreObjectId = rs.getInt(1);
            
        } finally {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
        
	return scoreObjectId;
    }

    public static boolean updateGameScoreObject(Connection conn,
    					        int accountId,
    					        int slotId,
    					        long titleId,
    					        int gameId,                                            
    					        int scoreId,
    					        int scoreObjectSize,
    					        byte[] scoreObject)
        throws SQLException
    {
        int result = 0;
    	boolean isNull = false;
        
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            int scoreObjectId = queryScoreObjectId(conn, accountId, slotId,
                                            titleId, gameId, scoreId);
            
            if(scoreObjectId > 0) {      	
                stmt = conn.prepareStatement(QUERY_IS_GAME_SCORE_OBJECT_NULL);
                stmt.setInt(1, scoreObjectId);
                
        	rs = stmt.executeQuery();
        	
        	if (rs.next()) {
        	    if (rs.getInt(1) == 0)
        		isNull = true;
        	}
        	
                stmt.close();
                stmt = null;
        	
        	if (isNull)
        	{    	
                    stmt = conn.prepareStatement(UPDATE_GAME_SCORE_OBJECT);
        	    Clob newClob = CLOB.createTemporary(conn, false, CLOB.DURATION_SESSION);
        	    newClob.setString(1, new Base64().encode(scoreObject));
        	    stmt.setClob(1, newClob);
                    stmt.setInt(2, scoreObjectSize);
                    stmt.setInt(3, scoreObjectId);
    
                    result = stmt.executeUpdate();
        	}
            }
            
        } finally {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
    	
        return (result == 1);
    }
    
} // GameScoreDML

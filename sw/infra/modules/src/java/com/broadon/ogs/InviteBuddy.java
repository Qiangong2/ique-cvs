package com.broadon.ogs;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ogs.*;

/** The class used to serve request to change buddy state
 */

public class InviteBuddy extends OnlineGameFunction
{
    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(InviteBuddy.class.getName());
   
    public static InviteBuddyResponseType inviteBuddy(
            OnlineGameServiceImpl service,
            InviteBuddyRequestType inviteBuddyRequest) 
        throws java.rmi.RemoteException 
    {
        String bsmAction = "INVITE_BUD";
        
        int myAccountId = inviteBuddyRequest.getAccountId();
        String buddyLoginName = inviteBuddyRequest.getBuddyLoginName();
                        
        InviteBuddyResponseType response = new InviteBuddyResponseType();
        initResponse(inviteBuddyRequest, response);
            
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
            
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            
            int buddyAccountId = BuddyDML.getAccountId(conn, buddyLoginName);
            if (buddyAccountId != -1) {
                int[] newStates = BuddyStateMachine.start(conn, myAccountId, buddyAccountId, bsmAction);
                if (newStates[0] != -1 && newStates[1] != -1) {
                    conn.commit();
                    response.setBuddyAccountId(new Integer(buddyAccountId));
                    
                    RelationType relation = new RelationType(newStates[0], newStates[1]);
                    response.setRelation(relation);
                    
                } else {
                    response.setBuddyAccountId(null);
                    response.setRelation(null);
                    setErrorCode(StatusCode.OGS_INVALID_BUDDY_STATE_ACTION, null, response);
                }
                
            } else {
                response.setBuddyAccountId(null);
                response.setRelation(null);
                setErrorCode(StatusCode.OGS_INVALID_BUDDY_LOGIN_NAME, null, response);
            }
                
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("InviteBuddy error: ", e);
                
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.OGS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.OGS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);       
            log.error("InviteBuddy error: ", e);
                
        } finally {
            try {
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            }
            catch (SQLException e) {
                log.warn("InviteBuddy error while closing connection: ", e);
            }
        }                
                
        return response;
    }
}
package com.broadon.ogs;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ogs.*;

/** The class used to serve VNG server request to query
 *  for message headers for a given accountId
 */

public class GetMessageList extends OnlineGameFunction
{
    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(GetMessageList.class.getName());

   
    public static GetMessageListResponseType getMessageList(
            OnlineGameServiceImpl service,
            GetMessageListRequestType getMessageListRequest) 
        throws java.rmi.RemoteException 
    {
        int accountId = getMessageListRequest.getAccountId();
        int numRows = getMessageListRequest.getMaxResults();
        int skipRows = getMessageListRequest.getSkipResults();
                        
        GetMessageListResponseType response = new GetMessageListResponseType();
        initResponse(getMessageListRequest, response);
        
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
            
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            
            List messageList = MessagesDML.getMessageList(conn, accountId, numRows, skipRows);
            
            if (messageList != null && messageList.size() > 0) {
                MessageHeaderType[] messageHeader = new MessageHeaderType[messageList.size()];
                messageHeader = (MessageHeaderType[]) messageList.toArray(messageHeader);
                response.setMessageHeader(messageHeader);
                
            } else
                response.setMessageHeader(null);
                
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("GetMessageList error: ", e);
                
        } catch (NumberFormatException e) {               
            setErrorCode(StatusCode.OGS_MESSAGE_EXCEPTION, 
                    StatusCode.getMessage(StatusCode.OGS_MESSAGE_EXCEPTION) + ": " + e.getLocalizedMessage(),
                    response);
            log.error("GetMessageList error: ", e);
                
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.OGS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.OGS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);       
            log.error("GetMessageList error: ", e);
            
        } finally {
            try {
                if (conn != null)
                    conn.close();
            }
            catch (SQLException e) {
                log.warn("GetMessageList error while closing connection: ", e);
            }
        }                
                
        return response;
    }
}
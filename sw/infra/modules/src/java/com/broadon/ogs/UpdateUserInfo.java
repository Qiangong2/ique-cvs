package com.broadon.ogs;

import com.broadon.wsapi.ias.AccountInfoType;
import com.broadon.wsapi.ias.AttributeType;
import com.broadon.wsapi.ias.UpdateAccountAttributesRequestType;
import com.broadon.wsapi.ias.UpdateAccountAttributesResponseType;
import com.broadon.wsapi.ias.UpdateAccountRequestType;
import com.broadon.wsapi.ias.UpdateAccountResponseType;
import com.broadon.wsapi.ias.IdentityAuthenticationPortType;

import com.broadon.wsapi.ogs.*;

public class UpdateUserInfo extends OnlineGameFunction 
{
    public static UpdateUserInfoResponseType updateUserInfo(
            OnlineGameServiceImpl impl,
            UpdateUserInfoRequestType request) 
        throws java.rmi.RemoteException 
    {
	UserInfoType ogsUserInfo = request.getUserInfo();
        
        AccountInfoType iasAccountInfo = new AccountInfoType();
        iasAccountInfo.setLoginName(ogsUserInfo.getLoginName());
        iasAccountInfo.setNickName(ogsUserInfo.getNickName());
        
        UpdateUserInfoResponseType response = new UpdateUserInfoResponseType();
	initResponse(request, response);
        
	IdentityAuthenticationPortType iasPort = impl.getIasSOAP();

        // invoke IAS UpdateAccount
        UpdateAccountRequestType iasInfoRequest = new UpdateAccountRequestType();
        initIASRequest(iasInfoRequest);
        
        iasInfoRequest.setAccountId(ogsUserInfo.getAccountId());
        iasInfoRequest.setAccountInfo(iasAccountInfo);
	
        UpdateAccountResponseType iasInfoResponse = iasPort.updateAccount(iasInfoRequest);        
        if (iasInfoResponse.getErrorCode() != STATUS_OK)
            setErrorCode(StatusCode.OGS_UPDATE_USER_INFO_ERROR, iasInfoResponse.getErrorMessage(), response);
        
        else {
            int ogsAttributeId[] = {0,1,2,3,4,5,6,7,8,9,10};
            int ogsAttributeCount = 0;
            
            String ogsDescription = ogsUserInfo.getDescription();        
            UserAttributeType ogsUserAttributes[] = ogsUserInfo.getAttribute();
            
            // invoke IAS UpdateAccountAttributes
            UpdateAccountAttributesRequestType iasAttrRequest = new UpdateAccountAttributesRequestType();
            initIASRequest(iasAttrRequest);
            
            iasAttrRequest.setAccountId(ogsUserInfo.getAccountId());
            
            if (ogsDescription != null && !ogsDescription.equals(""))
                ogsAttributeCount++;
            
            if (ogsUserAttributes != null && ogsUserAttributes.length > 0)
                ogsAttributeCount += ogsUserAttributes.length;
            
            if (ogsAttributeCount > 0) {
                AttributeType iasAttribute[] = new AttributeType[ogsAttributeCount];
                int index = 0;
                while (ogsUserAttributes != null && index < ogsUserAttributes.length) {
                    iasAttribute[index] = new AttributeType();
                    iasAttribute[index].setAttributeId(ogsUserAttributes[index].getId());
                    iasAttribute[index].setAttributeValue(String.valueOf(ogsUserAttributes[index].getValue()));
                    index++;
                }
                if (ogsDescription != null && !ogsDescription.equals("")) {
                    iasAttribute[index] = new AttributeType();
                    iasAttribute[index].setAttributeId(10);
                    iasAttribute[index].setAttributeValue(ogsDescription);
                }
                
                iasAttrRequest.setAccountAttributes(iasAttribute);
                
                int ogsUnsetAttributeCount = ogsAttributeId.length - ogsAttributeCount;
                if (ogsUnsetAttributeCount > 0) {
                    int ogsUnsetAttributeId[] = new int[ogsUnsetAttributeCount];
                    index = 0;
                    
                    for (int i = 0; i < ogsAttributeId.length; i++) {
                        boolean found = false;
                        for (int j = 0; j < iasAttribute.length; j++) {
                            if (ogsAttributeId[i] == iasAttribute[j].getAttributeId()) {
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            ogsUnsetAttributeId[index] = ogsAttributeId[i];
                            index++;
                        }
                    }
                    
                    iasAttrRequest.setUnsetAccountAttributes(ogsUnsetAttributeId);
                    
                } else
                    iasAttrRequest.setUnsetAccountAttributes(null); 
                                
            } else {
                iasAttrRequest.setAccountAttributes(null);
                iasAttrRequest.setUnsetAccountAttributes(ogsAttributeId);
            }            
            
            UpdateAccountAttributesResponseType iasAttrResponse = iasPort.updateAccountAttributes(iasAttrRequest);
            if (iasAttrResponse.getErrorCode() != STATUS_OK)
                setErrorCode(StatusCode.OGS_UPDATE_USER_INFO_ERROR, iasAttrResponse.getErrorMessage(), response);
        }
        
        return response;
    }
}

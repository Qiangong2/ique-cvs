package com.broadon.ogs;

import com.broadon.wsapi.ias.UpdateAccountPasswordRequestType;
import com.broadon.wsapi.ias.UpdateAccountPasswordResponseType;
import com.broadon.wsapi.ias.IdentityAuthenticationPortType;
import com.broadon.wsapi.ogs.*;

public class SetPassword extends OnlineGameFunction 
{
    public static SetPasswordResponseType setPassword(
            OnlineGameServiceImpl impl,
            SetPasswordRequestType request) 
        throws java.rmi.RemoteException 
    {
        SetPasswordResponseType response = new SetPasswordResponseType();
        initResponse(request, response);

        // invoke IAS UpdateAccountPassword
        IdentityAuthenticationPortType iasPort = impl.getIasSOAP();

        UpdateAccountPasswordRequestType iasRequest = new UpdateAccountPasswordRequestType();
        initIASRequest(iasRequest);
        iasRequest.setAccountId(request.getAccountId());
        iasRequest.setPassword(request.getOldPassword());
        iasRequest.setNewPassword(request.getNewPassword());
                
        UpdateAccountPasswordResponseType iasResponse = iasPort.updateAccountPassword(iasRequest);
        if (iasResponse.getErrorCode() != STATUS_OK)
            setErrorCode(StatusCode.OGS_SET_PASSWORD_ERROR, iasResponse.getErrorMessage(), response);
                        
        return response;
    }
}

package com.broadon.ogs;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ogs.*;

/** The class used to serve VNG server request for matchmaking,
 *  searching game session as per the requested criteria
 */

public class SearchGameSessions extends OnlineGameFunction
{
    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(SearchGameSessions.class.getName());

   
    public static SearchGameSessionsResponseType searchGameSessions(
            OnlineGameServiceImpl service,
            SearchGameSessionsRequestType searchGameSessionsRequest) 
        throws java.rmi.RemoteException 
    {
        Integer gameId = searchGameSessionsRequest.getGameId();
        String keyword = searchGameSessionsRequest.getKeyword();
        AttrPredicateType attr[] = searchGameSessionsRequest.getAttrPredicate();
        int numRows = searchGameSessionsRequest.getMaxResults();
        int skipRows = searchGameSessionsRequest.getSkipResults();
        
        SearchGameSessionsResponseType response = new SearchGameSessionsResponseType();
        initResponse(searchGameSessionsRequest, response);
        
        if (gameId == null && 
                (keyword == null || keyword.equals("")) &&
                (attr == null || attr.length == 0)) {
            response.setGameSession(null);
            return response;            
        }
         
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
            
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
                
            List gameSessionsList = GameSessionDML.matchGameSessions(conn, gameId, keyword, attr, 
                                                    numRows, skipRows);
            
            if (gameSessionsList != null && gameSessionsList.size() > 0) {
                GameSessionType[] sess = new GameSessionType[gameSessionsList.size()];
                sess = (GameSessionType[]) gameSessionsList.toArray(sess);
                response.setGameSession(sess);
                
            } else
                response.setGameSession(null);
                
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("SearchGameSessions error: ", e);
                
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.OGS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.OGS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);       
            log.error("SearchGameSessions error: ", e);
            
        } finally {
            try {
                if (conn != null)
                    conn.close();
            }
            catch (SQLException e) {
                log.warn("SearchGameSessions error while closing connection: ", e);
            }
        }                
                
        return response;
    }
}
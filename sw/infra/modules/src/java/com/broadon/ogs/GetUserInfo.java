package com.broadon.ogs;

import com.broadon.wsapi.ias.AttributeType;
import com.broadon.wsapi.ias.AccountInfoType;
import com.broadon.wsapi.ias.GetAccountAttributesRequestType;
import com.broadon.wsapi.ias.GetAccountAttributesResponseType;
import com.broadon.wsapi.ias.GetAccountInfoRequestType;
import com.broadon.wsapi.ias.GetAccountInfoResponseType;
import com.broadon.wsapi.ias.IdentityAuthenticationPortType;

import com.broadon.wsapi.ogs.*;

public class GetUserInfo extends OnlineGameFunction
{
    public static GetUserInfoResponseType getUserInfo(
            OnlineGameServiceImpl impl,
            GetUserInfoRequestType request)    
        throws java.rmi.RemoteException 
    {
	GetUserInfoResponseType response = new GetUserInfoResponseType();
	initResponse(request, response);
	
	// invoke IAS GetAccountInfo
        IdentityAuthenticationPortType iasPort = impl.getIasSOAP();

        GetAccountInfoRequestType iasInfoRequest = new GetAccountInfoRequestType();
        initIASRequest(iasInfoRequest);
        iasInfoRequest.setAccountId(request.getAccountId());
        
        GetAccountInfoResponseType iasInfoResponse = iasPort.getAccountInfo(iasInfoRequest);
        if (iasInfoResponse.getErrorCode() != STATUS_OK) {
            response.setUserInfo(null);
            setErrorCode(StatusCode.OGS_GET_USER_INFO_ERROR, iasInfoResponse.getErrorMessage(), response);
            
        } else {
            try {
                // set AccountId
                UserInfoType uInfo = new UserInfoType();
                uInfo.setAccountId(iasInfoResponse.getAccountId());
                
                // set LoginName and NickName
                AccountInfoType aInfo = iasInfoResponse.getAccountInfo();
                uInfo.setLoginName(aInfo.getLoginName());
                uInfo.setNickName(aInfo.getNickName());
                
                int ogsAttrId[] = {0,1,2,3,4,5,6,7,8,9,10};
                
                // set Attributes and Description, invoke IAS GetAccountAttributes
                GetAccountAttributesRequestType iasAttrRequest = new GetAccountAttributesRequestType();
                initIASRequest(iasAttrRequest);
                iasAttrRequest.setAccountId(request.getAccountId());
                iasAttrRequest.setAttributeId(ogsAttrId);
                
                GetAccountAttributesResponseType iasAttrResponse = iasPort.getAccountAttributes(iasAttrRequest);
                if (iasAttrResponse.getErrorCode() != STATUS_OK) {
                    response.setUserInfo(null);
                    setErrorCode(StatusCode.OGS_GET_USER_INFO_ERROR, iasAttrResponse.getErrorMessage(), response);
                    
                } else {
                    AttributeType iasAttr[] = iasAttrResponse.getAccountAttributes();
                    
                    if (iasAttr != null && iasAttr.length > 0) {
                        // get description
                        String desc = null;                
                        for (int i = 0; i < iasAttr.length; i++) {
                            if (iasAttr[i].getAttributeId() == 10)
                                desc = iasAttr[i].getAttributeValue();                    
                        }
                        
                        uInfo.setDescription(desc);
                        
                        int attrCount = iasAttr.length;
                        if (desc != null)
                            attrCount--;
                           
                        if (attrCount > 0) {
                            // get other attributes
                            UserAttributeType ogsAttr[] = new UserAttributeType[attrCount];
                            int oCount = 0;
                            for (int i = 0; i < iasAttr.length; i++) {
                                if (iasAttr[i].getAttributeId() != 10) {
                                    ogsAttr[oCount] = new UserAttributeType();
                                    ogsAttr[oCount].setId(iasAttr[i].getAttributeId());
                                    ogsAttr[oCount].setValue(Integer.parseInt(iasAttr[i].getAttributeValue()));
                                    oCount++;
                                }
                            }                    
                            uInfo.setAttribute(ogsAttr);
                                
                        } else
                            uInfo.setAttribute(null);
                        
                    } else {
                        uInfo.setAttribute(null);
                        uInfo.setDescription(null);
                    }
                    
                    response.setUserInfo(uInfo);
                }
                
            } catch (NumberFormatException e) {
                response.setUserInfo(null);
                setErrorCode(StatusCode.OGS_GET_USER_INFO_EXCEPTION, 
                        e.getLocalizedMessage(), 
                        response);
            }
        }
        
        return response;
    }
}

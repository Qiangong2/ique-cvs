package com.broadon.ogs;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.broadon.wsapi.ogs.BuddyInfoType;
import com.broadon.wsapi.ogs.ChangeBuddyStateActionType;
import com.broadon.wsapi.ogs.RelationType;
import com.broadon.wsapi.ogs.UserAttributeType;
import com.broadon.wsapi.ogs.UserInfoType;

/**
 * Data manipulation library functions defined here pertain to buddy list
 */
public class BuddyDML
{
    private static final String ACCEPT = "ACCEPT_INV";
    private static final String REJECT = "REJECT_INV";
    private static final String REMOVE = "REMOVE_BUD";
    private static final String BLOCK = "BLOCK_BUD";
    private static final String UNBLOCK = "UNBLOCK_BUD";
    
    /** Define all SQL statements
     */

    private static final String TABLE_OGS_BUDDY_LISTS = "OGS_BUDDY_LISTS";
    private static final String TABLE_IAS_ACCOUNTS = "IAS_ACCOUNTS";
    private static final String TABLE_IAS_ACCOUNT_ATTRIBUTES = "IAS_ACCOUNT_ATTRIBUTES";
    
    private static final String VIEW_OGS_BUDDY_LISTS = "OGS_BUDDY_LIST_ALL_V";
    
    private static final String GET_BUDDY_STATE =
        "SELECT buddy_flags FROM " + TABLE_OGS_BUDDY_LISTS + " WHERE account_id = ? " +
        "AND buddy_account_id = ?";

    private static final String UPDATE_BUDDY_STATE =
        "UPDATE " + TABLE_OGS_BUDDY_LISTS + " SET buddy_flags = NVL(?, buddy_flags) " +
        "WHERE account_id = ? AND buddy_account_id = ?";

    private static final String INSERT_BUDDY_STATE =
        "INSERT INTO " + TABLE_OGS_BUDDY_LISTS + " (account_id, buddy_account_id, buddy_flags) " +
        "VALUES (?, ?, nvl(?, 0))";
    
    private static final String GET_BUDDY_DETAILS =
        "SELECT buddy_account_id, my_buddy_flags, reverse_flags, buddy_login_name, " +
        "buddy_nick_name FROM " + VIEW_OGS_BUDDY_LISTS + " WHERE my_account_id = ?";
    
    private static final String GET_ACCOUNT_DETAILS =
                "SELECT account_id, login_name, nick_name FROM " + TABLE_IAS_ACCOUNTS;

    private static final String GET_ACCOUNT_ATTRIBUTES =
                "SELECT * FROM " + TABLE_IAS_ACCOUNT_ATTRIBUTES + " WHERE account_id = ?";                

    public static int getAccountId(Connection conn, String loginName)
    	throws SQLException
    {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int accountId = -1;
        
        try {
            String sql = GET_ACCOUNT_DETAILS;
            sql += " WHERE login_name = '" + loginName + "'";
            
            stmt = conn.prepareStatement(sql);
            
            rs = stmt.executeQuery();
            if (rs.next()) 
                accountId = rs.getInt("ACCOUNT_ID");
            
        }
        finally {
            if (rs != null) 
                rs.close();
            if (stmt != null) 
                stmt.close();
        }
        
        return accountId;
    }

    public static List getBuddyList(Connection conn, 
			            int myAccountId,
                                    int numRows,
                                    int skipRows)
    	throws SQLException, NumberFormatException
    {
        List buddyList = new ArrayList();
        
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            String sql = "SELECT * FROM (SELECT rownum no, inner.* FROM (" +
                GET_BUDDY_DETAILS + " ORDER BY buddy_login_name) " +
                "inner WHERE rownum <= " + (skipRows+numRows) + ") " +
                "WHERE no > " + skipRows;
            
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, myAccountId);
            
            rs = stmt.executeQuery();

            while (rs.next()) {
                UserInfoType uInfo = new UserInfoType();
                
                uInfo.setAccountId(rs.getInt("BUDDY_ACCOUNT_ID"));
                uInfo.setLoginName(rs.getString("BUDDY_LOGIN_NAME"));
                uInfo.setNickName(rs.getString("BUDDY_NICK_NAME"));
                
                List attrList = getAccountAttributes(conn, rs.getInt("BUDDY_ACCOUNT_ID"));
                if (attrList != null && attrList.size() > 0) {
                    UserAttributeType[] userAttr = new UserAttributeType[attrList.size()];
                    userAttr = (UserAttributeType[]) attrList.toArray(userAttr);
                    uInfo.setAttribute(userAttr);
                    
                } else
                    uInfo.setAttribute(null);
                
                uInfo.setDescription(getAccountDescription(conn, rs.getInt("BUDDY_ACCOUNT_ID")));
                
                RelationType relation = new RelationType();
                relation.setBuddyState(rs.getInt("REVERSE_FLAGS"));
                relation.setMyState(rs.getInt("MY_BUDDY_FLAGS"));
                
                BuddyInfoType buddyInfo = new BuddyInfoType();
                buddyInfo.setUserInfo(uInfo);
                buddyInfo.setRelation(relation);
                
                buddyList.add(buddyInfo);
            }            
        }
        finally {
            if (rs != null) 
                rs.close();
            if (stmt != null) 
                stmt.close();
        }
        
        return buddyList;
    }

    public static List getAccountAttributes(Connection conn, int accountId)
    	throws SQLException, NumberFormatException
    {
        List attrList = new ArrayList();
        
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            String sql = GET_ACCOUNT_ATTRIBUTES;
            sql += " AND attribute_id >= 0 AND attribute_id <= 9 AND attribute_value IS NOT NULL";
            
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, accountId);
            
            rs = stmt.executeQuery();

            while (rs.next()) {
                UserAttributeType attr = new UserAttributeType();
                attr.setId(rs.getInt("ATTRIBUTE_ID"));                
                attr.setValue(Integer.parseInt(rs.getString("ATTRIBUTE_VALUE")));
                
                attrList.add(attr);
            }            
        }
        finally {
            if (rs != null) 
                rs.close();
            if (stmt != null) 
                stmt.close();
        }
        
        return attrList;
    }
    
    public static String getAccountDescription(Connection conn, int accountId)
            throws SQLException
    {
        String attrDesc = null;
        
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            String sql = GET_ACCOUNT_ATTRIBUTES;
            sql += " AND attribute_id = 10 AND attribute_value IS NOT NULL";
            
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, accountId);
            
            rs = stmt.executeQuery();
    
            if (rs.next())
                attrDesc = rs.getString("ATTRIBUTE_VALUE");
        }
        finally {
            if (rs != null) 
                rs.close();
            if (stmt != null) 
                stmt.close();
        }
        
        return attrDesc;
    }
    
    /**
     * Get buddy state.
     * 
     * @param conn Database connection.
     * @param myAccountId My Account Id
     * @param buddyAccountId Buddy Account Id
     * 
     * @exception SQLException Database access error.
     * 
     * @return Buddy state.
     */
    public static int getBuddyState(Connection conn,
                                    int myAccountId,
                                    int buddyAccountId) 
        throws SQLException
    {        
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int buddyFlags = 0;
        
        try {
            stmt = conn.prepareStatement(GET_BUDDY_STATE);
            
            stmt.setInt(1, myAccountId);
            stmt.setInt(2, buddyAccountId);
            
            rs = stmt.executeQuery();

            if (rs.next()) 
                buddyFlags = rs.getInt(1);
            
        }
        finally {
            if (rs != null) 
                rs.close();
            if (stmt != null) 
                stmt.close();
        }
        
        return buddyFlags;
    }
    
    /**
     * Set buddy state.
     * 
     * @param conn Database connection.
     * @param myAccountId My Account Id
     * @param buddyAccountId Buddy Account Id
     * @param state Buddy state.
     * 
     * @exception SQLException Database access error.
     */
    public static void setBuddyState(Connection conn,
                                      int myAccountId,
                                      int buddyAccountId,
                                      int state) throws SQLException
    {
        PreparedStatement stmt = null;
        
        try {
            stmt = conn.prepareStatement(UPDATE_BUDDY_STATE);
            
            stmt.setInt(1, state);
            stmt.setLong(2, myAccountId);
            stmt.setLong(3, buddyAccountId);
            
            int result = stmt.executeUpdate();
            
            if (result == 0)
            {
                // No row exists, need to insert the record                
                stmt = conn.prepareStatement(INSERT_BUDDY_STATE);
                
                stmt.setLong(1, myAccountId);
                stmt.setLong(2, buddyAccountId);
                stmt.setInt(3, state);
                
                result = stmt.executeUpdate();
            }
        }
        finally {
            if (stmt != null) 
                stmt.close();
        }
    }
    
    public static String getBuddyListAction(ChangeBuddyStateActionType action)
    {
        if (action.getValue().equals("Accept"))
            return ACCEPT;
        else if (action.getValue().equals("Reject"))
            return REJECT;
        else if (action.getValue().equals("Remove"))
            return REMOVE;
        else if (action.getValue().equals("Block"))
            return BLOCK;
        else if (action.getValue().equals("Unblock"))
            return UNBLOCK;        
        else
            return null;
    }

} // BuddyDML

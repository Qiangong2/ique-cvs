package com.broadon.ogs;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ogs.*;

/** The class used to serve VNG server request to submit game scores
 *  for different item id's
 */

public class SubmitGameScores extends OnlineGameFunction
{
   // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(SubmitGameScores.class.getName());

   
    public static SubmitGameScoresResponseType submitGameScores(
            OnlineGameServiceImpl service,
            SubmitGameScoresRequestType submitGameScoresRequest) 
        throws java.rmi.RemoteException 
    {
        ScoreKeyType scoreKey = submitGameScoresRequest.getScoreKey();
        String scoreInfo = submitGameScoresRequest.getInfo();
        ScoreItemType scoreItem[] = submitGameScoresRequest.getScoreItem();
        
        SubmitGameScoresResponseType response = new SubmitGameScoresResponseType();
        initResponse(submitGameScoresRequest, response);
            
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
            
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            
            if (GameScoreDML.insertGameScore(conn, scoreKey.getAccountId(), scoreKey.getSlotId(),
                    scoreKey.getTitleId(), scoreKey.getGameId(), scoreKey.getScoreId(), scoreItem, scoreInfo)) {
                               
                conn.commit();
                
                List scoreItemList = GameScoreDML.getScoreItems(conn, scoreKey.getAccountId(), scoreKey.getSlotId(),
                                            scoreKey.getTitleId(), scoreKey.getGameId(), scoreKey.getScoreId());
                
                if (scoreItemList != null && scoreItemList.size() > 0) {
                    ScoreItemType[] scoreItemWithRank = new ScoreItemType[scoreItemList.size()];
                    scoreItemWithRank = (ScoreItemType[]) scoreItemList.toArray(scoreItemWithRank);
                    response.setScoreItem(scoreItemWithRank);
                    
                } else {
                    response.setScoreItem(null);
                    setErrorCode(StatusCode.OGS_GET_GAME_SCORE_ITEMS_ERROR, null, response);
                }
                
            } else {
                response.setScoreItem(null);
                setErrorCode(StatusCode.OGS_SUBMIT_GAME_SCORES_ERROR, null, response);
            }
                
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("SubmitGameScores error: ", e);
                
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.OGS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.OGS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);       
            log.error("SubmitGameScores error: ", e);
            
        } finally {
            try {
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            }
            catch (SQLException e) {
                log.warn("SubmitGameScores error while closing connection: ", e);
            }
        }                
                
        return response;
    }
}
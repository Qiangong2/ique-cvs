package com.broadon.ogs;

import com.broadon.ogs.OnlineGameServiceImpl;
import com.broadon.ogs.StatusCode;
import com.broadon.wsapi.ogs.*;
import com.broadon.wsapi.ias.AuthenticateAccountRequestType;
import com.broadon.wsapi.ias.AuthenticateAccountResponseType;
import com.broadon.wsapi.ias.IdentityAuthenticationPortType;

public class Login extends OnlineGameFunction 
{
    public static LoginResponseType login(
            OnlineGameServiceImpl impl,
            LoginRequestType request) 
        throws java.rmi.RemoteException 
    {
	LoginResponseType response = new LoginResponseType();
	initResponse(request, response);

	// invoke IAS AuthenticateAccount
	IdentityAuthenticationPortType iasPort = impl.getIasSOAP();

        AuthenticateAccountRequestType iasRequest = new AuthenticateAccountRequestType();
	initIASRequest(iasRequest);
	iasRequest.setLoginName(request.getLoginName());
	iasRequest.setPassword(request.getPassword());
	
        AuthenticateAccountResponseType iasResponse = iasPort.authenticateAccount(iasRequest);
        if (iasResponse.getErrorCode() != STATUS_OK) {
            response.setUserInfo(null);
            setErrorCode(StatusCode.OGS_LOGIN_ERROR, iasResponse.getErrorMessage(), response);
        } else {
            GetUserInfoRequestType ogsRequest = new GetUserInfoRequestType();
            ogsRequest.setVersion(request.getVersion());
            ogsRequest.setMessageId(request.getMessageId());
            ogsRequest.setTimeStamp(getServerTimestamp());            
            ogsRequest.setAccountId(iasResponse.getAccountId().intValue());
            
            GetUserInfoResponseType ogsResponse = GetUserInfo.getUserInfo(impl, ogsRequest);            
            if (ogsResponse.getErrorCode() != STATUS_OK) {
                response.setUserInfo(null);
                setErrorCode(StatusCode.OGS_LOGIN_ERROR, ogsResponse.getErrorMessage(), response);
            } else
                response.setUserInfo(ogsResponse.getUserInfo());                        
        }
                
        return response;
    }
}

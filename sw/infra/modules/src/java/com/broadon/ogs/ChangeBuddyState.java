package com.broadon.ogs;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ogs.*;

/** The class used to serve request to change buddy state
 */

public class ChangeBuddyState extends OnlineGameFunction
{
    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(ChangeBuddyState.class.getName());
   
    public static ChangeBuddyStateResponseType changeBuddyState(
            OnlineGameServiceImpl service,
            ChangeBuddyStateRequestType changeBuddyStateRequest) 
        throws java.rmi.RemoteException 
    {
        int myAccountId = changeBuddyStateRequest.getAccountId();
        int buddyAccountId = changeBuddyStateRequest.getBuddyAccountId();
        ChangeBuddyStateActionType action = changeBuddyStateRequest.getAction();
                        
        ChangeBuddyStateResponseType response = new ChangeBuddyStateResponseType();
        initResponse(changeBuddyStateRequest, response);
            
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
            
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            
            String bsmAction = BuddyDML.getBuddyListAction(action);
            if (bsmAction != null) {
                int[] newStates = BuddyStateMachine.start(conn, myAccountId, buddyAccountId, bsmAction);
                if (newStates[0] != -1 && newStates[1] != -1) {
                    conn.commit();
                
                    RelationType relation = new RelationType(newStates[0], newStates[1]);
                    response.setRelation(relation);
                    
                } else {
                    response.setRelation(null);
                    setErrorCode(StatusCode.OGS_INVALID_BUDDY_STATE_ACTION, null, response);
                }
                
            } else {
                response.setRelation(null);
                setErrorCode(StatusCode.OGS_INVALID_BUDDY_STATE_ACTION, null, response);
            }
                
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("ChangeBuddyState error: ", e);
                
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.OGS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.OGS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);       
            log.error("ChangeBuddyState error: ", e);
                
        } finally {
            try {
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            }
            catch (SQLException e) {
                log.warn("ChangeBuddyState error while closing connection: ", e);
            }
        }                
                
        return response;
    }
}
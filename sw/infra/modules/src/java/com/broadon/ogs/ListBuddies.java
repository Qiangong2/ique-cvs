package com.broadon.ogs;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ogs.*;

/** The class used to serve request to change buddy state
 */

public class ListBuddies extends OnlineGameFunction
{
    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(ListBuddies.class.getName());
   
    public static ListBuddiesResponseType listBuddies(
            OnlineGameServiceImpl service,
            ListBuddiesRequestType listBuddiesRequest) 
        throws java.rmi.RemoteException 
    {
        int myAccountId = listBuddiesRequest.getAccountId();
        int numRows = listBuddiesRequest.getMaxResults();
        int skipRows = listBuddiesRequest.getSkipResults();
                        
        ListBuddiesResponseType response = new ListBuddiesResponseType();
        initResponse(listBuddiesRequest, response);
            
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
            
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            
            List buddyList = BuddyDML.getBuddyList(conn, myAccountId, numRows, skipRows);
            
            if (buddyList != null && buddyList.size() > 0) {
                BuddyInfoType[] buddyInfo = new BuddyInfoType[buddyList.size()];
                buddyInfo = (BuddyInfoType[]) buddyList.toArray(buddyInfo);
                response.setBuddyInfo(buddyInfo);
                
            } else
                response.setBuddyInfo(null);
                
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("ListBuddies error: ", e);
                
        } catch (NumberFormatException e) {               
            setErrorCode(StatusCode.OGS_GET_USER_INFO_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("ListBuddies error: ", e);
        
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.OGS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.OGS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);       
            log.error("ListBuddies error: ", e);
                
        } finally {
            try {
                if (conn != null)
                    conn.close();
            }
            catch (SQLException e) {
                log.warn("ListBuddies error while closing connection: ", e);
            }
        }                
                
        return response;
    }
}
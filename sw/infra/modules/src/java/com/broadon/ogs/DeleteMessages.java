package com.broadon.ogs;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ogs.*;

/** The class used to serve VNG server request to delete messages
 *  based on a list of provided msg id's
 */

public class DeleteMessages extends OnlineGameFunction
{
    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(DeleteMessages.class.getName());

   
    public static DeleteMessagesResponseType deleteMessages(
            OnlineGameServiceImpl service,
            DeleteMessagesRequestType deleteMessagesRequest) 
        throws java.rmi.RemoteException 
    {
        int accountId = deleteMessagesRequest.getAccountId();
        int msgId[] = deleteMessagesRequest.getMsgId();
                        
        DeleteMessagesResponseType response = new DeleteMessagesResponseType();
        initResponse(deleteMessagesRequest, response);
        
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
            
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            
            for (int i = 0; i < msgId.length; i++)
                MessagesDML.deleteMessage(conn, accountId, msgId[i]);
                        
            conn.commit();
                
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("DeleteMessages error: ", e);
                
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.OGS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.OGS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);       
            log.error("DeleteMessages error: ", e);
            
        } finally {
            try {
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            }
            catch (SQLException e) {
                log.warn("DeleteMessages error while closing connection: ", e);
            }
        }                
                
        return response;
    }
}
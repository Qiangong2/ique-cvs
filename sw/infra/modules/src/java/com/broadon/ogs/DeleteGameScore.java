package com.broadon.ogs;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ogs.*;

/** The class used to serve VNG server request to delete game scores
 *  for a score key
 */

public class DeleteGameScore extends OnlineGameFunction
{
   // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(DeleteGameScore.class.getName());

   
    public static DeleteGameScoreResponseType deleteGameScore(
            OnlineGameServiceImpl service,
            DeleteGameScoreRequestType deleteGameScoreRequest) 
        throws java.rmi.RemoteException 
    {
        ScoreKeyType scoreKey = deleteGameScoreRequest.getScoreKey();
        
        DeleteGameScoreResponseType response = new DeleteGameScoreResponseType();
        initResponse(deleteGameScoreRequest, response);
            
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
            
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            
            if (GameScoreDML.deleteGameScore(conn, scoreKey.getAccountId(), scoreKey.getSlotId(),
                    scoreKey.getTitleId(), scoreKey.getGameId(), scoreKey.getScoreId()))
                conn.commit();                
            else
                setErrorCode(StatusCode.OGS_DELETE_GAME_SCORE_ERROR, null, response);
                
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("DeleteGameScore error: ", e);
                
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.OGS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.OGS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);       
            log.error("DeleteGameScore error: ", e);
            
        } finally {
            try {
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            }
            catch (SQLException e) {
                log.warn("DeleteGameScore error while closing connection: ", e);
            }
        }                
                
        return response;
    }
}
package com.broadon.ogs;

import com.broadon.wsapi.ias.*;
import com.broadon.wsapi.ogs.*;

public class Register extends OnlineGameFunction
{
    private static int MAX_REGISTER_ATTEMPTS = 10;
    private static int DEVICE_TO_LOGIN_MASK = 1000000;    
    private static int FIX_LENGTH_FOR_LOGIN_NAME = 6;
    private static int IAS_DEVICE_ALREADY_REGISTERED_ERROR = 951;
	
    public static RegisterResponseType register(
            OnlineGameServiceImpl impl,
            RegisterRequestType request) 
        throws java.rmi.RemoteException
    {
	long deviceId = request.getDeviceId();
        int registerResult = -1;
        
	RegisterResponseType response = new RegisterResponseType();
	initResponse(request, response);
		
	IdentityAuthenticationPortType iasPort = impl.getIasSOAP();
		
	{ // invoke IAS RegisterDevice
	    RegisterDeviceRequestType iasRequest = new RegisterDeviceRequestType();
	    initIASRequest(iasRequest);
			
	    iasRequest.setDeviceId(request.getDeviceId());
	    iasRequest.setDeviceCert(request.getDeviceCert());
            iasRequest.setSerialNumber(request.getSerialNumber());
            
            if (request.getConfigCode() != null)
                iasRequest.setConfigCode(request.getConfigCode());
	    
            RegisterDeviceResponseType iasResponse = iasPort.registerDevice(iasRequest);
            registerResult = iasResponse.getErrorCode();
            if (registerResult != STATUS_OK && 
                    registerResult != IAS_DEVICE_ALREADY_REGISTERED_ERROR) {
		setErrorCode(StatusCode.OGS_REGISTER_ERROR, iasResponse.getErrorMessage(), response);
		return response;
	    }
	}
		
        if (registerResult == IAS_DEVICE_ALREADY_REGISTERED_ERROR) {
            
            int deviceAccountId[] = null;
		
	    { // invoke IAS GetDeviceAccounts
                DeviceAccountType deviceAccounts[] = null;
                
		GetDeviceAccountsRequestType iasRequest = new GetDeviceAccountsRequestType();
		initIASRequest(iasRequest);
		iasRequest.setDeviceId(request.getDeviceId());
		
                GetDeviceAccountsResponseType iasResponse = iasPort.getDeviceAccounts(iasRequest);
                if (iasResponse.getErrorCode() != STATUS_OK) {
                    setErrorCode(StatusCode.OGS_REGISTER_ERROR, iasResponse.getErrorMessage(), response);
                    return response;
		}
                
                deviceAccounts = iasResponse.getDeviceAccounts();
		if (deviceAccounts != null && deviceAccounts.length > 0) {
		    deviceAccountId = new int[deviceAccounts.length];
                    for (int i = 0; i < deviceAccounts.length; i++) {
                        deviceAccountId[i] = deviceAccounts[i].getAccountId();
                    }
		}
            }
		
	    if (deviceAccountId != null && deviceAccountId.length > 0) {	
	        
                {//invoke IAS RemoveDeviceAccount
                    for (int i = 0; i < deviceAccountId.length; i++) {
                        RemoveDeviceAccountRequestType iasRequest = new RemoveDeviceAccountRequestType();
                        initIASRequest(iasRequest);
                        iasRequest.setDeviceId(request.getDeviceId());
                        iasRequest.setAccountId(deviceAccountId[i]);
                
                        RemoveDeviceAccountResponseType iasResponse = iasPort.removeDeviceAccount(iasRequest);
                        if (iasResponse.getErrorCode() != STATUS_OK) {
                            setErrorCode(StatusCode.OGS_REGISTER_ERROR, iasResponse.getErrorMessage(), response);
                            return response;
                        }
                    }
                }
            }
        }            
        
        int accountId = -1;
        String loginName = null;
        String password = Long.toString((long)(Math.random()*100000000));
        
        {// invoke IAS RegisterAccount
	    RegisterAccountRequestType iasRequest = new RegisterAccountRequestType();
            initIASRequest(iasRequest);
            
	    AccountInfoType accountInfo = new AccountInfoType();
            RegisterAccountResponseType iasResponse = null;
            
            String suffix = getFixedLength(Long.toString(deviceId % DEVICE_TO_LOGIN_MASK));		
	    for (int i = 0; i < MAX_REGISTER_ATTEMPTS; i++) {
		String prefix = (i > 0) ? Integer.toString(i) : "";
		loginName = prefix + suffix;
		accountInfo.setLoginName(loginName);
		accountInfo.setNickName(loginName);  // Nickname defaults to loginName

		iasRequest.setPassword(password);
		iasRequest.setAccountInfo(accountInfo);
					
                iasResponse = iasPort.registerAccount(iasRequest);
		if (iasResponse.getErrorCode() == STATUS_OK) {
		    accountId = iasResponse.getAccountId().intValue();
		    break;								
		}
	    }
	    
            if (iasResponse.getErrorCode() != STATUS_OK) {
		setErrorCode(StatusCode.OGS_REGISTER_ERROR, iasResponse.getErrorMessage(), response);
		return response;
	    }
	}
        
        if (accountId != -1) {
            
            { // invoke IAS LinkDeviceAccount
                LinkDeviceAccountRequestType iasRequest = new LinkDeviceAccountRequestType();
		initIASRequest(iasRequest);
		iasRequest.setAccountId(accountId);
		iasRequest.setDeviceId(deviceId);
				
		LinkDeviceAccountResponseType iasResponse = iasPort.linkDeviceAccount(iasRequest);
		if (iasResponse.getErrorCode() != STATUS_OK) {
		    setErrorCode(StatusCode.OGS_REGISTER_ERROR, iasResponse.getErrorMessage(), response);
		    return response;
		}
	    }		
	}  else {
            setErrorCode(StatusCode.OGS_REGISTER_ERROR, null, response);
            return response;
	}	
	
        response.setLoginName(loginName);
	response.setPassword(password);
	return response;
    }
    
    private static String getFixedLength(String str)
    {
        int length = str.length();
        
        for (int i = 0; i < FIX_LENGTH_FOR_LOGIN_NAME - length; i++)
            str = "0"+str;
               
        return str;
    }
}

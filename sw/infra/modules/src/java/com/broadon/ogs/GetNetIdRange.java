package com.broadon.ogs;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.logging.Log;

import com.broadon.wsapi.ogs.*;

public class GetNetIdRange extends OnlineGameFunction 
{
    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(GetNetIdRange.class.getName());

   
    public static GetNetIdRangeResponseType getNetIdRange(
            OnlineGameServiceImpl service,
            GetNetIdRangeRequestType getNetIdRangeRequest) 
        throws java.rmi.RemoteException 
    {
        int totalNetId = getNetIdRangeRequest.getTotalNetId();
        
        GetNetIdRangeResponseType response = new GetNetIdRangeResponseType();
        initResponse(getNetIdRangeRequest, response);
        
        if (totalNetId <= 0) {
            setErrorCode(StatusCode.OGS_GET_NET_ID_RANGE_ERROR, null, response);
            log.debug("GetNetIdRange bad request error: " +
                    "totalNetId=" + totalNetId);
            
            return response;
        }
        
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
            
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            int lastNetId = GameSessionDML.getLastNetId(conn, totalNetId);
            
            if (lastNetId != -1) {
                response.setBeginNetId(lastNetId-totalNetId+1);
                response.setEndNetId(lastNetId);
            } else
                setErrorCode(StatusCode.OGS_GET_NET_ID_RANGE_ERROR, null, response);
                
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("GetNetIdRange error: ", e);
                
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.OGS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.OGS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);       
            log.error("GetNetIdRange error: ", e);
            
        } finally {
            try {
                if (conn != null)
                    conn.close();
            }
            catch (SQLException e) {
                log.warn("GetNetIdRange error while closing connection: ", e);
            }
        }                
                
        return response;
    }
}

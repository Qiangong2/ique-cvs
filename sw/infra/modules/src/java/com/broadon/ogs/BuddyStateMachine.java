/*
 * BuddyStateMachine
 *
 * $Id: BuddyStateMachine.java,v 1.3 2006/03/22 23:00:15 vaibhav Exp $
 *
 * Copyright (C) 2004, BroadOn Communications Corp.
 *
 * These coded instructions, statements, and computer programs
 * contain unpublished proprietary information of BroadOn
 * Communications Corp., and are protected by Federal copyright
 * law. They may not be disclosed to third parties or copied or
 * duplicated in any form, in whole or in part, without the prior
 * written consent of BroadOn Communications Corp.
 *
 * Date:   2005-03-14
 * Author: James Zhang
 *
 */

package com.broadon.ogs;

import java.util.HashMap;
import javax.sql.DataSource;

import java.sql.*;

import org.apache.commons.logging.Log;

/**
 *  The class implements Buddy Finite State Machine based on table
 *  OGS_BUDDY_FSM_MAPPINGS.
 */

public class BuddyStateMachine
{
    // --------------------------------------------
    // Inner Class
    // --------------------------------------------
    private static class BuddyStateAction
    {
        public int _state;
        public String _action;
        
        public BuddyStateAction(int state, String action)
        { 
            _state = state; 
            _action = action; 
        }
        
        public int hashCode() { return _state + _action.hashCode(); }

        public boolean equals(Object obj)
        {
            if (obj != null && obj instanceof BuddyStateAction) {
                BuddyStateAction stateAction = (BuddyStateAction) obj;
                return ( (this._state == stateAction._state) &&
                        (this._action.equals(stateAction._action)));
            }
            return false;
        }

    }

    // --------------------------------------------
    // Invariant values
    // --------------------------------------------
    
    private static final String ACTION_STOP = "STOP";
    private static final String TABLE_OGS_BUDDY_FSM_MAPPINGS = "OGS_BUDDY_FSM_MAPPINGS";
    
    private static final String GET_BUDDY_FSM_MAP =
        "SELECT i_state, i_action, o_state, o_action FROM " + TABLE_OGS_BUDDY_FSM_MAPPINGS;    

    // --------------------------------------------
    // Member variables
    // --------------------------------------------

    private static boolean _initialized = false;
    private static Log _logger = null;
    
    // Buddy Finite State Machine Mapping, currently there are 51 Rows
    // in table OGS_BUDDY_FSM_MAPPINGS
    private static HashMap _buddyFSMMap = new HashMap(51);

    /**
     * Get next buddy state and action.
     * 
     * @param iState Input state.
     * @param iAction Input action.
     * 
     * @return Next buddy state and action.
     */
    private static BuddyStateAction getNextState(int iState, String iAction)
    {
        BuddyStateAction iStateAction = new BuddyStateAction(iState, iAction);
        BuddyStateAction oStateAction = (BuddyStateAction) _buddyFSMMap.get(iStateAction);
        
        if (oStateAction == null)
            oStateAction = new BuddyStateAction( -1, ACTION_STOP);
        
        return oStateAction;
    }  
    
    // --------------------------------------------
    // Member methods
    // --------------------------------------------

    /**
     * Initialize Buddy State Machine by loading the mapping into memory.
     * 
     * @param dataSource
     * @param log
     * 
     * @exception SQLException Database access error.
     */
    public static void init(DataSource dataSource, Log log) throws SQLException
    {
        if (_initialized) return;

        _initialized = true;
        _logger = log;
        _buddyFSMMap.clear();
        
        _logger.debug("init start......");
        
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        // Load OGS_BUDDY_FSM_MAPPINGS into memory
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);

            stmt = conn.prepareStatement(GET_BUDDY_FSM_MAP);
            
            rs = stmt.executeQuery();
            while (rs.next())
            {
                int iState = rs.getInt("I_STATE");
                String iAction = rs.getString("I_ACTION");
                int oState = rs.getInt("O_STATE");
                String oAction = rs.getString("O_ACTION");
                
                BuddyStateAction iStateAction = new BuddyStateAction(iState, iAction);
                BuddyStateAction oStateAction = new BuddyStateAction(oState, oAction);
                
                _buddyFSMMap.put(iStateAction, oStateAction);
            }
            
            _logger.debug("Buddy State Machine Mapping: " + _buddyFSMMap);
        }
        catch (SQLException e) {
            _logger.error("BuddyStateMachine.init() failed: " + e);
            throw e;
        }
        finally {
            if (conn != null) 
                conn.close();
            if (rs != null) 
                rs.close();
        }
    } 
    
    /**
     * Start Buddy State Machine.
     * 
     * @param conn Database connection.
     * @param myAccountId My Account Id
     * @param buddyAccountId Buddy Account Id
     * @param myAction My action.
     * 
     * @return Result of state machine.
     * 
     * @exception SQLException Database access error.
     */
    public static int[] start(Connection conn,
                               int myAccountId,
                               int buddyAccountId,
                               String myAction) throws SQLException
    {
        if (!_initialized) {
            _logger.error("BuddyStateMachine not initialized");
            throw new SQLException("BuddyStateMachine not initialized");
        }
        
        int oldStates[] = {0, 0};
        int newStates[] = {0, 0};
        int t = 0;
        
        String newAction = myAction;

        try {
            oldStates[0] = BuddyDML.getBuddyState(conn, myAccountId, buddyAccountId);
            oldStates[1] = BuddyDML.getBuddyState(conn, buddyAccountId, myAccountId);
            newStates[0] = oldStates[0];
            newStates[1] = oldStates[1];
            
            while (!newAction.equals(ACTION_STOP))
            {
                BuddyStateAction stateAction = getNextState(newStates[t], newAction);
                newStates[t] = stateAction._state;
                newAction = stateAction._action;
                t = Math.abs(t - 1);
            }
            
            if (newStates[0] >= 0 && newStates[1] >= 0)
            {
                if (oldStates[0] != newStates[0])
                    BuddyDML.setBuddyState(conn, myAccountId, buddyAccountId, newStates[0]);
                
                if (oldStates[1] != newStates[1] && newStates[1] >= 0)
                    BuddyDML.setBuddyState(conn, buddyAccountId, myAccountId, newStates[1]);
            }            
        }
        catch (SQLException e) {
            _logger.error("BuddyStateMachine.start() failed: " + e);
            throw e;
        }

        return newStates;
    }   

} // class BuddyStateMachine

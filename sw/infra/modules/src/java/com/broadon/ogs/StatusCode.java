package com.broadon.ogs;

/**
 * Response code specific to ogs (use error codes in 1100-1199 range)
 */
public class StatusCode extends com.broadon.status.StatusCode
{
    public static final String OGS_BAD_VERSION = "1101";
    static final String OGS_BAD_VERSION_MSG = "OGS - Invalid version";
    
    public static final String OGS_BAD_DEVICE_ID = "1102";
    static final String OGS_BAD_DEVICE_ID_MSG = "OGS - Device Id is not registered";
    
    public static final String OGS_INVALID_BUDDY_STATE_ACTION = "1103";
    static final String OGS_INVALID_BUDDY_STATE_ACTION_MSG = "OGS - Invalid buddy state action requested";
    
    public static final String OGS_INVALID_BUDDY_LOGIN_NAME = "1104";
    static final String OGS_INVALID_BUDDY_LOGIN_NAME_MSG = "OGS - Invalid login name";
    
    public static final String OGS_LOGIN_ERROR = "1105";
    static final String OGS_LOGIN_ERROR_MSG = "OGS - Login Error";       
    
    public static final String OGS_REGISTER_ERROR = "1106";
    static final String OGS_REGISTER_ERROR_MSG = "OGS - Register Error";
    
    public static final String OGS_SET_PASSWORD_ERROR = "1107";
    static final String OGS_SET_PASSWORD_ERROR_MSG = "OGS - Register Error"; 
    
    public static final String OGS_GET_USER_INFO_ERROR = "1108";
    static final String OGS_GET_USER_INFO_ERROR_MSG = "OGS - Get User Info Error";
    
    public static final String OGS_UPDATE_USER_INFO_ERROR = "1109";
    static final String OGS_UPDATE_USER_INFO_ERROR_MSG = "OGS - Update User Info Error";

    public static final String OGS_STORE_MESSAGE_ERROR = "1110";
    static final String OGS_STORE_MESSAGE_ERROR_MSG = 
        "OGS - Failed to store message";
    
    public static final String OGS_RETRIEVE_MESSAGE_ERROR = "1111";
    static final String OGS_RETRIEVE_MESSAGE_ERROR_MSG = 
        "OGS - Failed to retrieve message";
    
    public static final String OGS_DELIVER_MESSAGE_ERROR = "1112";
    static final String OGS_DELIVER_MESSAGE_ERROR_MSG = 
        "OGS - Failed to deliver message";
    
    public static final String OGS_SUBMIT_GAME_SCORES_ERROR = "1113";
    static final String OGS_SUBMIT_GAME_SCORES_ERROR_MSG = 
        "OGS - Failed to submit game scores";
    
    public static final String OGS_SUBMIT_GAME_SCORE_OBJECT_ERROR = "1114";
    static final String OGS_SUBMIT_GAME_SCORE_OBJECT_ERROR_MSG = 
        "OGS - Failed to submit game score object";
    
    public static final String OGS_GET_GAME_SCORE_ITEMS_ERROR = "1115";
    static final String OGS_GET_GAME_SCORE_ITEMS_ERROR_MSG = 
        "OGS - Failed to obtain score rank";
    
    public static final String OGS_DELETE_GAME_SCORE_ERROR = "1116";
    static final String OGS_DELETE_GAME_SCORE_ERROR_MSG = 
        "OGS - Failed to delete game scores";
    
    public static final String OGS_GET_GAME_SCORE_KEYS_ERROR = "1117";
    static final String OGS_GET_GAME_SCORE_KEYS_ERROR_MSG = 
        "OGS - Failed to obtain game score keys";
    
    public static final String OGS_GET_GAME_SCORE_OBJECT_ERROR = "1118";
    static final String OGS_GET_GAME_SCORE_OBJECT_ERROR_MSG = 
        "OGS - Failed to obtain game score object";
    
    public static final String OGS_GET_GAME_SCORE_ERROR = "1119";
    static final String OGS_GET_GAME_SCORE_ERROR_MSG = 
        "OGS - Failed to obtain game score";
    
    // Failed to register game session
    public static final String OGS_REGISTER_GAME_SESSION_ERROR = "1120";
    static final String OGS_REGISTER_GAME_SESSION_ERROR_MSG = 
        "OGS - Failed to register game session";
    
    // Failed to end game session
    public static final String OGS_UNREGISTER_GAME_SESSION_ERROR = "1121";
    static final String OGS_UNREGISTER_GAME_SESSION_ERROR_MSG = 
        "OGS - Failed to unregister game session";
    
    public static final String OGS_GET_RANKED_GAME_SCORE_ERROR = "1122";
    static final String OGS_GET_RANKED_GAME_SCORE_ERROR_MSG = 
        "OGS - Failed to obtain ranked game score";
    
    public static final String OGS_EXTEND_GAME_SESSION_ERROR = "1123";
    static final String OGS_EXTEND_GAME_SESSION_ERROR_MSG = 
        "OGS - Failed to extend game session";
    
    public static final String OGS_GET_NET_ID_RANGE_ERROR = "1124";
    static final String OGS_GET_NET_ID_RANGE_ERROR_MSG = 
        "OGS - Failed to obtain Net Id range";
    
    public static final String OGS_MESSAGE_EXCEPTION = "1196";
    static final String OGS_MESSAGE_EXCEPTION_MSG = 
        "OGS - Content-Type does not contain a parsable integer value";
    
    public static final String OGS_GET_USER_INFO_EXCEPTION = "1197";
    static final String OGS_GET_USER_INFO_EXCEPTION_MSG = 
        "OGS - Attribute does not contain a parsable integer value";  
    
    // Bad Request Error
    public static final String OGS_BAD_REQUEST = "1198";
    static final String OGS_BAD_REQUEST_MSG =
        "OGS - Bad Request";
    
    public static final String OGS_INTERNAL_ERROR = "1199";
    static final String OGS_INTERNAL_ERROR_MSG = "OGS - Internal Error";          

    static {
        addStatusCode(OGS_BAD_VERSION, OGS_BAD_VERSION_MSG);
        addStatusCode(OGS_BAD_DEVICE_ID, OGS_BAD_DEVICE_ID_MSG);
        addStatusCode(OGS_INVALID_BUDDY_STATE_ACTION, OGS_INVALID_BUDDY_STATE_ACTION_MSG);
        addStatusCode(OGS_INVALID_BUDDY_LOGIN_NAME, OGS_INVALID_BUDDY_LOGIN_NAME_MSG);
        addStatusCode(OGS_LOGIN_ERROR, OGS_LOGIN_ERROR_MSG);
        addStatusCode(OGS_REGISTER_ERROR, OGS_REGISTER_ERROR_MSG);
        addStatusCode(OGS_SET_PASSWORD_ERROR, OGS_SET_PASSWORD_ERROR_MSG);
        addStatusCode(OGS_GET_USER_INFO_ERROR, OGS_GET_USER_INFO_ERROR_MSG);
        addStatusCode(OGS_UPDATE_USER_INFO_ERROR, OGS_UPDATE_USER_INFO_ERROR_MSG);
        addStatusCode(OGS_STORE_MESSAGE_ERROR, OGS_STORE_MESSAGE_ERROR_MSG);
        addStatusCode(OGS_RETRIEVE_MESSAGE_ERROR, OGS_RETRIEVE_MESSAGE_ERROR_MSG);
        addStatusCode(OGS_DELIVER_MESSAGE_ERROR, OGS_DELIVER_MESSAGE_ERROR_MSG);
        addStatusCode(OGS_SUBMIT_GAME_SCORES_ERROR, OGS_SUBMIT_GAME_SCORES_ERROR_MSG);
        addStatusCode(OGS_SUBMIT_GAME_SCORE_OBJECT_ERROR, OGS_SUBMIT_GAME_SCORE_OBJECT_ERROR_MSG);
        addStatusCode(OGS_GET_GAME_SCORE_ITEMS_ERROR, OGS_GET_GAME_SCORE_ITEMS_ERROR_MSG);
        addStatusCode(OGS_DELETE_GAME_SCORE_ERROR, OGS_DELETE_GAME_SCORE_ERROR_MSG);
        addStatusCode(OGS_GET_GAME_SCORE_KEYS_ERROR, OGS_GET_GAME_SCORE_KEYS_ERROR_MSG);
        addStatusCode(OGS_GET_GAME_SCORE_OBJECT_ERROR, OGS_GET_GAME_SCORE_OBJECT_ERROR_MSG);
        addStatusCode(OGS_GET_GAME_SCORE_ERROR, OGS_GET_GAME_SCORE_ERROR_MSG);
        addStatusCode(OGS_REGISTER_GAME_SESSION_ERROR, OGS_REGISTER_GAME_SESSION_ERROR_MSG);
        addStatusCode(OGS_UNREGISTER_GAME_SESSION_ERROR, OGS_UNREGISTER_GAME_SESSION_ERROR_MSG);
        addStatusCode(OGS_GET_RANKED_GAME_SCORE_ERROR, OGS_GET_RANKED_GAME_SCORE_ERROR_MSG);
        addStatusCode(OGS_EXTEND_GAME_SESSION_ERROR, OGS_EXTEND_GAME_SESSION_ERROR_MSG);
        addStatusCode(OGS_GET_NET_ID_RANGE_ERROR, OGS_GET_NET_ID_RANGE_ERROR_MSG);
        
        addStatusCode(OGS_MESSAGE_EXCEPTION, OGS_MESSAGE_EXCEPTION_MSG);
        addStatusCode(OGS_GET_USER_INFO_EXCEPTION, OGS_GET_USER_INFO_EXCEPTION_MSG);
        addStatusCode(OGS_BAD_REQUEST, OGS_BAD_REQUEST_MSG);
        addStatusCode(OGS_INTERNAL_ERROR, OGS_INTERNAL_ERROR_MSG);
        
    }
}

package com.broadon.ogs;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ogs.*;

/** The class used to serve VNG server request to obtain game score
 *  keys for a given account id and optionally game_id/score_id
 */

public class GetGameScoreKeys extends OnlineGameFunction
{
   // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(GetGameScoreKeys.class.getName());

   
    public static GetGameScoreKeysResponseType getGameScoreKeys(
            OnlineGameServiceImpl service,
            GetGameScoreKeysRequestType getGameScoreKeysRequest) 
        throws java.rmi.RemoteException 
    {
        int accountId = getGameScoreKeysRequest.getAccountId();
        Integer gameId = getGameScoreKeysRequest.getGameId();
        Integer scoreId = getGameScoreKeysRequest.getScoreId();
        
        GetGameScoreKeysResponseType response = new GetGameScoreKeysResponseType();
        initResponse(getGameScoreKeysRequest, response);
            
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
            
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            
            List scoreKeysList = GameScoreDML.getScoreKeys(conn, accountId, gameId, scoreId);
                
            if (scoreKeysList != null && scoreKeysList.size() > 0) {
                ScoreKeyType[] scoreKeys = new ScoreKeyType[scoreKeysList.size()];
                scoreKeys = (ScoreKeyType[]) scoreKeysList.toArray(scoreKeys);
                response.setScoreKeys(scoreKeys);
                    
            } else
                response.setScoreKeys(null);
                
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("GetGameScoreKeys error: ", e);
                
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.OGS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.OGS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);       
            log.error("GetGameScoreKeys error: ", e);
            
        } finally {
            try {
                if (conn != null)
                    conn.close();
            }
            catch (SQLException e) {
                log.warn("GetGameScoreKeys error while closing connection: ", e);
            }
        }                
                
        return response;
    }
}

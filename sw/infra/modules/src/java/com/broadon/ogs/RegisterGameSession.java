package com.broadon.ogs;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ogs.*;

/** The class used to serve VNG server request to register a new game session
 */

public class RegisterGameSession extends OnlineGameFunction
{
    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(RegisterGameSession.class.getName());

   
    public static RegisterGameSessionResponseType registerGameSession(
            OnlineGameServiceImpl service,
            RegisterGameSessionRequestType registerGameSessionRequest) 
        throws java.rmi.RemoteException 
    {
        GameSessionType gameSession = registerGameSessionRequest.getGameSession();
                        
        RegisterGameSessionResponseType response = new RegisterGameSessionResponseType();
        initResponse(registerGameSessionRequest, response);
            
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
            
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            
            // register the game session
            if (GameSessionDML.registerGameSession(conn, gameSession))
                conn.commit();
            else
                setErrorCode(StatusCode.OGS_REGISTER_GAME_SESSION_ERROR, null, response);
                
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("RegisterGameSession error: ", e);
                
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.OGS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.OGS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);       
            log.error("RegisterGameSession error: ", e);
                
        } finally {
            try {
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            }
            catch (SQLException e) {
                log.warn("RegisterGameSession error while closing connection: ", e);
            }
        }                
                
        return response;
    }
}
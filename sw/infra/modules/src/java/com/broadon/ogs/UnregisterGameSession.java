package com.broadon.ogs;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ogs.*;

/** The class used to serve VNG server request to end a game session
 */

public class UnregisterGameSession extends OnlineGameFunction
{
    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(UnregisterGameSession.class.getName());

   
    public static UnregisterGameSessionResponseType unregisterGameSession(
            OnlineGameServiceImpl service,
            UnregisterGameSessionRequestType unregisterGameSessionRequest) 
        throws java.rmi.RemoteException 
    {
        int netId = unregisterGameSessionRequest.getNetId();
                        
        UnregisterGameSessionResponseType response = new UnregisterGameSessionResponseType();
        initResponse(unregisterGameSessionRequest, response);
            
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
            
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
                
            // set the end date for the given net id
            if (GameSessionDML.endGameSession(conn, netId))
                conn.commit();
            else
                setErrorCode(StatusCode.OGS_UNREGISTER_GAME_SESSION_ERROR, null, response);            
                
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("UnregisterGameSession error: ", e);
               
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.OGS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.OGS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);       
            log.error("UnregisterGameSession error: ", e);
                
        } finally {
            try {
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            }
            catch (SQLException e) {
                log.warn("UnregisterGameSession error while closing connection: ", e);
            }
        }                
                
        return response;
    }
}
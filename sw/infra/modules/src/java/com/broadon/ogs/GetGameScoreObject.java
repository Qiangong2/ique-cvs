package com.broadon.ogs;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ogs.*;

/** The class used to serve VNG server request to obtain game score
 *  object for a score key
 */

public class GetGameScoreObject extends OnlineGameFunction
{
   // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(GetGameScoreObject.class.getName());

   
    public static GetGameScoreObjectResponseType getGameScoreObject(
            OnlineGameServiceImpl service,
            GetGameScoreObjectRequestType getGameScoreObjectRequest) 
        throws java.rmi.RemoteException 
    {
        ScoreKeyType scoreKey = getGameScoreObjectRequest.getScoreKey();
        
        GetGameScoreObjectResponseType response = new GetGameScoreObjectResponseType();
        initResponse(getGameScoreObjectRequest, response);
            
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
            
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            
            byte[] scoreObject = GameScoreDML.getScoreObject(conn, scoreKey.getAccountId(), scoreKey.getSlotId(),
                                    scoreKey.getTitleId(), scoreKey.getGameId(), scoreKey.getScoreId());
                
            if (scoreObject != null)
                response.setObject(scoreObject);
            else
                setErrorCode(StatusCode.OGS_GET_GAME_SCORE_OBJECT_ERROR, null, response);
                
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("GetGameScoreObject error: ", e);
                
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.OGS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.OGS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);       
            log.error("GetGameScoreObject error: ", e);
            
        } finally {
            try {
                if (conn != null)
                    conn.close();
            }
            catch (SQLException e) {
                log.warn("GetGameScoreObject error while closing connection: ", e);
            }
        }                
                
        return response;
    }
}
package com.broadon.ogs;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ogs.*;

/** The class used to serve VNG server request to change the end date of a game session
 */

public class ExtendGameSession extends OnlineGameFunction
{
    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(ExtendGameSession.class.getName());

   
    public static ExtendGameSessionResponseType extendGameSession(
            OnlineGameServiceImpl service,
            ExtendGameSessionRequestType extendGameSessionRequest) 
        throws java.rmi.RemoteException 
    {
        int netId = extendGameSessionRequest.getNetId();
        long extendTime = extendGameSessionRequest.getExtendTime();
                        
        ExtendGameSessionResponseType response = new ExtendGameSessionResponseType();
        initResponse(extendGameSessionRequest, response);
            
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
            
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
                
            // extend the end date for the given net id
            if (GameSessionDML.extendGameSession(conn, netId, extendTime))
                conn.commit();
            else
                setErrorCode(StatusCode.OGS_EXTEND_GAME_SESSION_ERROR, null, response);            
                
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("ExtendGameSession error: ", e);
               
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.OGS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.OGS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);       
            log.error("ExtendGameSession error: ", e);
                
        } finally {
            try {
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            }
            catch (SQLException e) {
                log.warn("ExtendGameSession error while closing connection: ", e);
            }
        }                
                
        return response;
    }
}
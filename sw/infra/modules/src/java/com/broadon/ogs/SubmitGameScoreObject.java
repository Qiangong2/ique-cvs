package com.broadon.ogs;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ogs.*;

/** The class used to serve VNG server request to submit game score
 *  object for a score key
 */

public class SubmitGameScoreObject extends OnlineGameFunction
{
   // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(SubmitGameScoreObject.class.getName());

   
    public static SubmitGameScoreObjectResponseType submitGameScoreObject(
            OnlineGameServiceImpl service,
            SubmitGameScoreObjectRequestType submitGameScoreObjectRequest) 
        throws java.rmi.RemoteException 
    {
        ScoreKeyType scoreKey = submitGameScoreObjectRequest.getScoreKey();
        byte[] scoreObject = submitGameScoreObjectRequest.getObject();
        
        SubmitGameScoreObjectResponseType response = new SubmitGameScoreObjectResponseType();
        initResponse(submitGameScoreObjectRequest, response);
            
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
            
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            
            if (GameScoreDML.updateGameScoreObject(conn, scoreKey.getAccountId(), scoreKey.getSlotId(),
                    scoreKey.getTitleId(), scoreKey.getGameId(), scoreKey.getScoreId(), scoreObject.length, scoreObject))
                conn.commit();                
            else
                setErrorCode(StatusCode.OGS_SUBMIT_GAME_SCORE_OBJECT_ERROR, null, response);
                
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("SubmitGameScoreObject error: ", e);
                
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.OGS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.OGS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);       
            log.error("SubmitGameScoreObject error: ", e);
            
        } finally {
            try {
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            }
            catch (SQLException e) {
                log.warn("SubmitGameScoreObject error while closing connection: ", e);
            }
        }                
                
        return response;
    }
}
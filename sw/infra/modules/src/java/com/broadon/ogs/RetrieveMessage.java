package com.broadon.ogs;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ogs.*;

/** The class used to serve VNG server request to retrieve a message
 *  object for a given message id and set the read date for the recipient
 */

public class RetrieveMessage extends OnlineGameFunction
{
    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(RetrieveMessage.class.getName());

   
    public static RetrieveMessageResponseType retrieveMessage(
            OnlineGameServiceImpl service,
            RetrieveMessageRequestType retrieveMessageRequest) 
        throws java.rmi.RemoteException 
    {
        int accountId = retrieveMessageRequest.getAccountId();
        int msgId = retrieveMessageRequest.getMsgId();
                        
        RetrieveMessageResponseType response = new RetrieveMessageResponseType();
        initResponse(retrieveMessageRequest, response);
            
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
            
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            
            byte mediaObject[] = MessagesDML.retrieveMessage(conn, accountId, msgId);
            
            if (mediaObject != null) {
                conn.commit();
                response.setMediaObject(mediaObject);
                
            } else {
                response.setMediaObject(null);
                setErrorCode(StatusCode.OGS_RETRIEVE_MESSAGE_ERROR, null, response);
            }
                
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("RetrieveMessage error: ", e);
                
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.OGS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.OGS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);       
            log.error("RetrieveMessage error: ", e);
            
        } finally {
            try {
                if (conn != null)
                    conn.close();
            }
            catch (SQLException e) {
                log.warn("RetrieveMessage error while closing connection: ", e);
            }
        }                
                
        return response;
    }
}
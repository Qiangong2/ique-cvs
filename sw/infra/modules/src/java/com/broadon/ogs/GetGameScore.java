package com.broadon.ogs;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ogs.*;

/** The class used to serve VNG server request to obtain game score
 *  object for a score key
 */

public class GetGameScore extends OnlineGameFunction
{
   // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(GetGameScore.class.getName());

   
    public static GetGameScoreResponseType getGameScore(
            OnlineGameServiceImpl service,
            GetGameScoreRequestType getGameScoreRequest) 
        throws java.rmi.RemoteException 
    {
        ScoreKeyType scoreKey = getGameScoreRequest.getScoreKey();
        
        GetGameScoreResponseType response = new GetGameScoreResponseType();
        initResponse(getGameScoreRequest, response);
            
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
            
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            
            ScoreItemResultType scoreItemResult = 
                GameScoreDML.getScore(conn, scoreKey.getAccountId(), scoreKey.getSlotId(),
                                    scoreKey.getTitleId(), scoreKey.getGameId(), scoreKey.getScoreId());
                
            if (scoreItemResult != null)
                response.setScoreResult(scoreItemResult);
            else
                setErrorCode(StatusCode.OGS_GET_GAME_SCORE_ERROR, null, response);
                
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("GetGameScore error: ", e);
                
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.OGS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.OGS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);       
            log.error("GetGameScore error: ", e);
            
        } finally {
            try {
                if (conn != null)
                    conn.close();
            }
            catch (SQLException e) {
                log.warn("GetGameScore error while closing connection: ", e);
            }
        }                
                
        return response;
    }
}
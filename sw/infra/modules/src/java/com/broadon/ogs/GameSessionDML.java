package com.broadon.ogs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import com.broadon.wsapi.ogs.GameSessionType;
import com.broadon.wsapi.ogs.AttrPredicateType;
import com.broadon.wsapi.ogs.CmpOperatorType;

/**
 * Data manipulation library functions defined here pertain to game sessions
 */
public class GameSessionDML
{
    /** Define all SQL statements
     */

    private static final String TABLE_VNG_SESSIONS = "VNG_SESSIONS";
    private static final String TABLE_VNG_NET_IDS = "VNG_NET_IDS";
    
    private static final String GA_COLUMNS[] = {"GA01","GA02","GA03","GA04","GA05","GA06","GA07","GA08",
            "GA09","GA10","GA11","GA12","GA13","GA14","GA15","GA16"};

    private static final String EXTEND_VNG_SESSION =    
        "UPDATE " + TABLE_VNG_SESSIONS + " SET end_date = sys_extract_utc(current_timestamp)+?/(86400000) " +   
        "WHERE net_id = ? ";

    private static final String GET_VNG_SESSIONS = 
        "SELECT net_id, game_id, title_id, account_id, device_id, join_flags, vn_zone, " +
        "total_slots, friend_slots, BCCUTIL.ConvertTimeStamp(create_date) create_date, " +
        "BCCUTIL.ConvertTimeStamp(end_date) end_date, ga01, ga02, ga03, ga04, ga05, ga06, " +
        "ga07, ga08, ga09, ga10, ga11, ga12, ga13, ga14, ga15, ga16, kw01, comments, " +
        "BCCUTIL.ConvertTimeStamp(last_updated) last_updated FROM " + TABLE_VNG_SESSIONS;
    
    private static final String INSERT_VNG_SESSION = 
        "INSERT INTO " + TABLE_VNG_SESSIONS + " (account_id, device_id, game_id, " +
        "title_id, vn_zone, join_flags, total_slots, friend_slots, ga01, ga02, ga03, ga04, " +
        "ga05, ga06, ga07, ga08, ga09, ga10, ga11, ga12, ga13, ga14, ga15, ga16, kw01, comments, net_id, end_date) " +
        "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,sys_extract_utc(current_timestamp)+1/144)";
    
    private static final String UPDATE_VNG_SESSION = 
        "UPDATE " + TABLE_VNG_SESSIONS + " SET account_id = ?, device_id = ?, game_id = ?, " +
        "title_id = ?, vn_zone = ?, join_flags = ?, total_slots = ?, friend_slots = ?, " +
        "ga01 = ?, ga02 = ?, ga03 = ?, ga04 = ?, ga05 = ?, ga06 = ?, ga07 = ?, ga08 = ?, " +
        "ga09 = ?, ga10 = ?, ga11 = ?, ga12 = ?, ga13 = ?, ga14 = ?, ga15 = ?, ga16 = ?, " +
        "kw01 = ?, comments = ?, end_date = sys_extract_utc(current_timestamp)+1 WHERE net_id = ?";
    
    private static final String GET_NET_IDS =    
        "SELECT init_net_id, max_net_id, last_net_id FROM " + TABLE_VNG_NET_IDS + " FOR UPDATE ";    
 
    public static boolean endGameSession(Connection conn, int netId)            
        throws SQLException
    {
        return extendGameSession(conn,netId,0);
    }
    
    public static boolean extendGameSession(Connection conn, int netId, long endDate)            
        throws SQLException
    {
        int result = 0;
        PreparedStatement stmt = null;
        
        try {
            stmt = conn.prepareStatement(EXTEND_VNG_SESSION);
            stmt.setLong(1, endDate);
            stmt.setInt(2, netId);
            
            result = stmt.executeUpdate();
        
        } finally {
            if (stmt != null)
                stmt.close();
        }
        
        return (result == 1);
    }

    public static List getGameSessions(Connection conn, int[] netId)                
        throws SQLException
    {
        String sql = GET_VNG_SESSIONS;       
            
        if (netId != null && netId.length > 0) {
            sql += " WHERE";
                
            for (int i = 0; i < netId.length; i++) {
                if (i != 0)
                    sql += " OR";
                sql += " net_id = " + netId[i];
            }
        }
        
        return queryGameSessions(conn, sql);
    }
    
    public static int getLastNetId(Connection conn, int totalNetId)
        throws SQLException
    {
        int newLastNetId = -1;        
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            stmt = conn.prepareStatement(GET_NET_IDS,
                            ResultSet.TYPE_SCROLL_INSENSITIVE,
                            ResultSet.CONCUR_UPDATABLE);
                        
            rs = stmt.executeQuery();            
            if (rs.next()) {            
                int initNetId = rs.getInt("INIT_NET_ID");
                int maxNetId = rs.getInt("MAX_NET_ID");
                int lastNetId = rs.getInt("LAST_NET_ID");
                newLastNetId =lastNetId+totalNetId;
                
                if (maxNetId < newLastNetId)
                    newLastNetId = initNetId+totalNetId-1;
                
                rs.updateInt("LAST_NET_ID", newLastNetId);              
                
                rs.updateRow();
            } 
            
        } finally {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
        
        return newLastNetId;
    }
    
    public static List matchGameSessions(Connection conn, 
                                         Integer gameId,
                                         String keyword,
                                         AttrPredicateType attr[],
                                         int numRows,
                                         int skipRows)
        throws SQLException
    {
        String sql = "SELECT * FROM (SELECT rownum no, inner.* FROM (" + GET_VNG_SESSIONS + 
            " WHERE nvl(end_date,sys_extract_utc(current_timestamp)+1) > sys_extract_utc(current_timestamp) " +
            "AND join_flags = 0 ORDER BY create_date DESC) " +
            "inner WHERE rownum <= " + (skipRows+numRows) + ") " +
            "WHERE no > " + skipRows;

        if (gameId != null)
            sql += " AND game_id = " + gameId.intValue();
        
        if (keyword != null && !keyword.equals("")) {
            sql += " AND upper(kw01) like ";
            sql += "'" + getStringToQuery(keyword.toUpperCase()) + "' escape '|'";
        }
        
        if (attr != null && attr.length > 0) {
            
            for (int i = 0; i < attr.length; i++)
            {
               sql += " AND ";
               sql += GA_COLUMNS[attr[i].getAttrId()] + " ";
               sql += getOperator(attr[i].getCmpOperator()) + " ";
               sql += attr[i].getAttrValue();                    
            }
        }

        sql += " ORDER BY create_date";

        return queryGameSessions(conn, sql);
    }

    public static boolean registerGameSession(Connection conn, GameSessionType gameSession)
        throws SQLException
    {
        if (registerGameSession(conn, gameSession, UPDATE_VNG_SESSION))
            return true;
        else
            return (registerGameSession(conn, gameSession, INSERT_VNG_SESSION));                
    }
    
    public static boolean registerGameSession(Connection conn, GameSessionType gameSession, String sql)
        throws SQLException
    {
        int result = 0;
        int index = 0;
                
        PreparedStatement stmt = null;
               
        try {
            stmt = conn.prepareStatement(sql);
            
            stmt.setInt(++index, gameSession.getAccountId());
            stmt.setLong(++index, gameSession.getDeviceId());
            stmt.setInt(++index, gameSession.getGameId());
            stmt.setLong(++index, gameSession.getTitleId());
            stmt.setInt(++index, gameSession.getNetZone());
            stmt.setInt(++index, gameSession.getAccessControl());
            stmt.setInt(++index, gameSession.getTotalSlots());
            stmt.setInt(++index, gameSession.getFriendSlots());
            
            int GA[] = gameSession.getGA();
            if (GA != null && GA.length > 0) {
                for (int i = 0; i < GA.length; i++) {
                    stmt.setInt(++index, GA[i]);
                }                                   
            }
            for (int i = (GA == null ? 0 : GA.length); i < GA_COLUMNS.length; i++) {
                stmt.setNull(++index, Types.INTEGER);
            } 
           
            stmt.setString(++index, gameSession.getKeyword());
            stmt.setString(++index, gameSession.getComments());
            stmt.setInt(++index, gameSession.getNetId());
            
            result = stmt.executeUpdate();
        
        } finally {
            if (stmt != null)
                stmt.close();
        }
        
        return (result == 1);
    }
    
    private static String getOperator(CmpOperatorType op)
    {       
        if (op.getValue().equals("EQ"))
            return "=";
        else if (op.getValue().equals("NEQ"))
            return "!=";
        else if (op.getValue().equals("LT"))
            return "<";
        else if (op.getValue().equals("GT"))
            return ">";
        else if (op.getValue().equals("LE"))
            return "<=";
        else if (op.getValue().equals("GE"))
            return ">=";        
        else
            return "=";
    }

    private static String getStringToQuery(String str)
    {   
        if (str!=null)
        {
            for (int i = 0; i < str.length(); i++) 
            {
                if (str.charAt(i) == '%' || str.charAt(i) == '_' || str.charAt(i) == '|')
                {
                    str = str.substring(0, i) + "|" + str.substring(i);  
                    i++;
                } else if (str.charAt(i) == '\'') {
                    str = str.substring(0, i) + "'" + str.substring(i);  
                    i++;
                }
            }

            str = str.replace('*', '%');
            str = str.replace('?', '_');
        }

        return str;
    }

    private static List queryGameSessions(Connection conn, String sql)                
        throws SQLException
    {
        List gameSessionsList = new ArrayList();
                        
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {            
            stmt = conn.prepareStatement(sql);
            
            rs = stmt.executeQuery();
            while (rs.next()) {
                GameSessionType gameSession = new GameSessionType();
                
                gameSession.setNetId(rs.getInt("NET_ID"));
                gameSession.setAccountId(rs.getInt("ACCOUNT_ID"));
                gameSession.setGameId(rs.getInt("GAME_ID"));
                gameSession.setTitleId(rs.getLong("TITLE_ID"));
                gameSession.setDeviceId(rs.getLong("DEVICE_ID"));
                gameSession.setCreateDate(new Long(rs.getLong("CREATE_DATE")));
                gameSession.setAccessControl(rs.getInt("JOIN_FLAGS"));
                gameSession.setNetZone(rs.getInt("VN_ZONE"));
                gameSession.setFriendSlots(rs.getInt("FRIEND_SLOTS"));
                gameSession.setTotalSlots(rs.getInt("TOTAL_SLOTS"));
                                
                long endDate = rs.getLong("END_DATE");
                if (!rs.wasNull())
                    gameSession.setEndDate(new Long(endDate));
                else
                    gameSession.setEndDate(null);
                
                long lastUpdated = rs.getLong("LAST_UPDATED");
                if (!rs.wasNull())
                    gameSession.setLastUpdated(new Long(lastUpdated));
                else
                    gameSession.setLastUpdated(null);
                
                int count = 0;
                for (int i = 0; i < GA_COLUMNS.length; i++) {
                    int dummy = rs.getInt(GA_COLUMNS[i]);
                    if (!rs.wasNull())
                        count++;
                    else
                        break;                    
                }                
                if (count != 0) {
                    int GA[] = new int[count];                    
                    for (int i = 0; i < count; i++) {
                        GA[i] = rs.getInt(GA_COLUMNS[i]);
                    }                    
                    gameSession.setGA(GA);                    
                } else
                    gameSession.setGA(null);
                             
                gameSession.setKeyword(rs.getString("KW01"));
                gameSession.setComments(rs.getString("COMMENTS"));               
                
                gameSessionsList.add(gameSession);
            }            
        
        } finally {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
        
        return gameSessionsList;
    }
  
} // GameSessionDML

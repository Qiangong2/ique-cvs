package com.broadon.ogs;

import java.io.Reader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import oracle.sql.CLOB;

import com.broadon.util.Base64;
import com.broadon.wsapi.ogs.MessageHeaderType;

/**
 * Data manipulation library functions defined here pertain to offline messaging.
 */
public class MessagesDML
{
    /** Define all SQL statements
     */

    private static final String TABLE_OGS_MESSAGES = "OGS_MESSAGES";
    private static final String TABLE_OGS_MESSAGE_RECEIVERS = "OGS_MESSAGE_RECEIVERS";
    private static final String TABLE_IAS_ACCOUNTS = "IAS_ACCOUNTS";
    
    private static final String GET_ACCOUNT_DETAILS =
        "SELECT account_id, status FROM " + TABLE_IAS_ACCOUNTS + " WHERE login_name = ?";
    
    private static final String GET_NEXT_MESSAGE_ID =
                "SELECT message_id_seq.nextval FROM dual";

    private static final String GET_MESSAGE_LIST =
                "SELECT omr.message_id, a.login_name sender, aa.login_name recipient, " +
                    "om.reply_id, BCCUTIL.ConvertTimeStamp(om.send_date) send_date, " +
                    "om.subject, om.content_type, om.content_length, " +
                    "decode(omr.read_date, null, 0, 1) status " +
                "FROM " + TABLE_OGS_MESSAGE_RECEIVERS + " omr, " + 
                    TABLE_OGS_MESSAGES + " om, " +
                    TABLE_IAS_ACCOUNTS + " a, " + 
                    TABLE_IAS_ACCOUNTS + " aa " +
                "WHERE omr.message_id = om.message_id " +
                "AND om.account_id = a.account_id AND omr.account_id = aa.account_id " +
                "AND omr.delete_date IS NULL AND omr.account_id = ?";

    private static final String GET_MESSAGE =
                "SELECT message FROM " + TABLE_OGS_MESSAGES + " WHERE message_id = ?";

    private static final String INSERT_MESSAGE_RECIPIENT =
                "INSERT INTO " + TABLE_OGS_MESSAGE_RECEIVERS + " (message_id, account_id) VALUES (?,?)";
    
    private static final String INSERT_MESSAGE =
        "INSERT INTO " + TABLE_OGS_MESSAGES + " (message_id, account_id, message_type, " +
        "reply_id, send_date, subject, message, content_type, content_length) " +
        "VALUES (?,?,?,?,sys_extract_utc(current_timestamp),?,?,?,?)";
    
    private static final String UPDATE_RETRIEVE_MESSAGE =
                "UPDATE " + TABLE_OGS_MESSAGE_RECEIVERS + 
                " SET read_date = sys_extract_utc(current_timestamp) WHERE message_id = ? " +
                "AND account_id = ? AND delete_date IS NULL";

    private static final String UPDATE_DELETE_MESSAGE =
                "UPDATE " + TABLE_OGS_MESSAGE_RECEIVERS + 
                " SET delete_date = sys_extract_utc(current_timestamp) WHERE message_id = ? " +
                "AND account_id = ?";    

    public static void deleteMessage(Connection conn, 
                                     int accountId,
                                     int msgId)
        throws SQLException
    {
        PreparedStatement stmt = null;
        
        
        try {
            stmt = conn.prepareStatement(UPDATE_DELETE_MESSAGE);        
            stmt.setInt(1, msgId);
            stmt.setInt(2, accountId);
    
            stmt.executeUpdate();
        }
        finally {
            if (stmt != null) 
                stmt.close();
        }
    }
    
    public static boolean deliverMessage(Connection conn, 
                                         int accountId,
                                         int msgId)
        throws SQLException
    {
        PreparedStatement stmt = null;
        int result = 0;
                
        try {
            stmt = conn.prepareStatement(INSERT_MESSAGE_RECIPIENT);       
            stmt.setInt(1, msgId);
            stmt.setInt(2, accountId);
            
            result = stmt.executeUpdate();
        }
        finally {
            if (stmt != null) 
                stmt.close();
        }
        
        return (result == 1);
    }
    
    public static int getAccountId(Connection conn, String loginName)
        throws SQLException
    {
        int accountId = -1;
        
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            stmt = conn.prepareStatement(GET_ACCOUNT_DETAILS);
            stmt.setString(1, loginName);
            
            rs = stmt.executeQuery();
            
            if (rs.next()) {
                String status = rs.getString("STATUS");
                
                if (status != null && status.equals("A"))
                    accountId = rs.getInt("ACCOUNT_ID");                
            }
        }
        finally {
            if (rs != null) 
                rs.close();
            if (stmt != null) 
                stmt.close();
        }
        
        return accountId;    
    }
    
    public static List getMessageList(Connection conn, 
                                      int accountId,
                                      int numRows,
                                      int skipRows)
        throws SQLException, NumberFormatException
    {
        List messageList = new ArrayList();
        
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            String sql = "SELECT * FROM (SELECT rownum no, inner.* FROM (" +
            GET_MESSAGE_LIST + " ORDER BY send_date DESC, message_id DESC) " +
            "inner WHERE rownum <= " + (skipRows+numRows) + ") " +
            "WHERE no > " + skipRows;
            
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, accountId);
            
            rs = stmt.executeQuery();
            
            while (rs.next()) {
                MessageHeaderType mHdr = new MessageHeaderType();
                
                mHdr.setMsgId(rs.getInt("MESSAGE_ID"));
                mHdr.setSendTime(rs.getLong("SEND_DATE"));
                mHdr.setSender(rs.getString("SENDER"));
                mHdr.setRecipient(rs.getString("RECIPIENT"));
                mHdr.setStatus(rs.getInt("STATUS"));                
                mHdr.setMediaFormat(Integer.parseInt(rs.getString("CONTENT_TYPE")));
                mHdr.setMediaSize(rs.getInt("CONTENT_LENGTH"));
                
                String subject = rs.getString("SUBJECT");            
                if (!rs.wasNull())
                    mHdr.setSubject(subject);
                else
                    mHdr.setSubject("");
                
                int replyMsgId = rs.getInt("REPLY_ID");            
                if (!rs.wasNull())
                    mHdr.setReplyMsgId(new Integer(replyMsgId));
                else
                    mHdr.setReplyMsgId(null);
                
                messageList.add(mHdr);
            }            
        }
        finally {
            if (rs != null) 
                rs.close();
            if (stmt != null) 
                stmt.close();
        }
        
        return messageList;
    }

    public static int getNextMessageID(Connection conn)
    	throws SQLException
    {
        int id = -1;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            stmt = conn.prepareStatement(GET_NEXT_MESSAGE_ID);

            rs = stmt.executeQuery();
            if (rs.next()) 
                id = rs.getInt(1);
        }
        finally {
            if (rs != null) 
                rs.close();
            if (stmt != null) 
                stmt.close();
        }

        return id;
    }

    public static byte[] retrieveMessage(Connection conn, int accountId, int msgId)
        throws SQLException, IOException
    {
        byte[] mediaObject = null;
        
        int result = 0;       
        PreparedStatement stmt = null;
        ResultSet rs = null;
            
        try {
            stmt = conn.prepareStatement(UPDATE_RETRIEVE_MESSAGE);
            stmt.setInt(1, msgId);
            stmt.setInt(2, accountId);
                
            result = stmt.executeUpdate();
            stmt.close();
            
            if (result == 1) {
                stmt = conn.prepareStatement(GET_MESSAGE);
                stmt.setInt(1, msgId);
                
                rs = stmt.executeQuery();
                if (rs.next()) {                    
                    Clob clob = rs.getClob("MESSAGE");
                    Reader reader = clob.getCharacterStream();
                    int size = (int)clob.length();
                    char[] buffer = new char[size];
                    
                    reader.read(buffer);
                    mediaObject = new Base64().decode(new String(buffer));                    
                }
            }
            
        } finally {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
        
        return mediaObject;
    } 
    
    public static int storeMessage (Connection conn, 
   		                    int accountId,
 			            String msgType,
			            String subject,
   		                    byte[] mediaObject,
   		                    int mediaFormat,
   		                    int mediaSize,
   		                    Integer replyMsgId)
    	throws SQLException
    {
        PreparedStatement stmt = null;
        int msgId = -1;
                
        try {
            msgId = getNextMessageID(conn);
            if (msgId != -1) {
                stmt = conn.prepareStatement(INSERT_MESSAGE);       
                stmt.setInt(1, msgId);
                stmt.setInt(2, accountId);
                stmt.setString(3, msgType);
                
                if (replyMsgId != null)
                    stmt.setInt(4, replyMsgId.intValue());
                else
                    stmt.setNull(4, Types.INTEGER);
                
                stmt.setString(5, subject);
                
                Clob newClob = CLOB.createTemporary(conn, false, CLOB.DURATION_SESSION);
                newClob.setString(1, new Base64().encode(mediaObject));
                stmt.setClob(6, newClob);
                
                stmt.setString(7, String.valueOf(mediaFormat));
                stmt.setInt(8, mediaSize);
                
                if (stmt.executeUpdate() != 1)
                    msgId = -1;
            }
        }
        finally {
            if (stmt != null) 
                stmt.close();
        }
        
        return msgId;
    }

} // MessagesDML

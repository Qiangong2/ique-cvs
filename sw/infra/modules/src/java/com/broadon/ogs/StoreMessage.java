package com.broadon.ogs;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ogs.*;

/** The class used to serve VNG server request to store a message
 *  and return its message id
 */

public class StoreMessage extends OnlineGameFunction
{
    private static String MESSAGE_TYPE = "VN_MSG";
    
    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(StoreMessage.class.getName());

   
    public static StoreMessageResponseType storeMessage(
            OnlineGameServiceImpl service,
            StoreMessageRequestType storeMessageRequest) 
        throws java.rmi.RemoteException 
    {
        int accountId = storeMessageRequest.getAccountId();
        String recipient[] = storeMessageRequest.getRecipient();
        
        String subject = storeMessageRequest.getSubject();
        byte mediaObject[] = storeMessageRequest.getMediaObject();
        int mediaFormat = storeMessageRequest.getMediaFormat();
        
        Integer replyMsgId = storeMessageRequest.getReplyMsgId();
        
        StoreMessageResponseType response = new StoreMessageResponseType();
        initResponse(storeMessageRequest, response);
            
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
            
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            
            int mediaSize = mediaObject.length;
            int msgId = MessagesDML.storeMessage(conn, accountId, MESSAGE_TYPE, 
                            subject, mediaObject, mediaFormat, mediaSize, replyMsgId);           
            
            if (msgId != -1) {
                int deliveryResult[] = new int[recipient.length];
                
                for (int i = 0; i < recipient.length; i++) {
                    int recipientId = MessagesDML.getAccountId(conn, recipient[i]);
                    
                    if (recipientId > 0) {
                        if (MessagesDML.deliverMessage(conn, recipientId, msgId))
                            deliveryResult[i] = Integer.parseInt(StatusCode.SC_OK);
                        else
                            deliveryResult[i] = Integer.parseInt(StatusCode.OGS_DELIVER_MESSAGE_ERROR);                            
                    } else 
                        deliveryResult[i] = Integer.parseInt(StatusCode.OGS_DELIVER_MESSAGE_ERROR);                    
                }
                
                conn.commit();
                
                response.setMsgId(new Integer(msgId));
                response.setDeliveryResult(deliveryResult);
                
            } else {
                response.setMsgId(null);
                response.setDeliveryResult(null);
                setErrorCode(StatusCode.OGS_DELIVER_MESSAGE_ERROR, null, response);
            }
                
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("StoreMessage error: ", e);
                
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.OGS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.OGS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);       
            log.error("StoreMessage error: ", e);
            
        } finally {
            try {
                if (conn != null)
                    conn.close();
            }
            catch (SQLException e) {
                log.warn("StoreMessage error while closing connection: ", e);
            }
        }                
                
        return response;
    }
}
package com.broadon.ogs;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ogs.*;

/** The class used to serve VNG server request to obtain account id 
 *  for a given login name
 */

public class GetAccountId extends OnlineGameFunction
{
    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(GetAccountId.class.getName());

   
    public static GetAccountIdResponseType getAccountId(
            OnlineGameServiceImpl service,
            GetAccountIdRequestType getAccountIdRequest) 
        throws java.rmi.RemoteException 
    {
        String loginName = getAccountIdRequest.getLoginName();
                        
        GetAccountIdResponseType response = new GetAccountIdResponseType();
        initResponse(getAccountIdRequest, response);
            
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
            
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
                
            int accountId = MessagesDML.getAccountId(conn, loginName); 
            
            if (accountId != -1)
                response.setAccountId(new Integer(accountId));
            else
                response.setAccountId(null);
                
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("GetAccountId error: ", e);
               
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.OGS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.OGS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);       
            log.error("GetAccountId error: ", e);
                
        } finally {
            try {
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            }
            catch (SQLException e) {
                log.warn("GetAccountId error while closing connection: ", e);
            }
        }                
                
        return response;
    }
}
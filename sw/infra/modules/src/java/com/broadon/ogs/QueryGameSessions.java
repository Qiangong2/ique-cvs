package com.broadon.ogs;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ogs.*;

/** The class used to serve VNG server request to query game sessions
 *  based on a list of provided net id's
 */

public class QueryGameSessions extends OnlineGameFunction
{
    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(QueryGameSessions.class.getName());

   
    public static QueryGameSessionsResponseType queryGameSessions(
            OnlineGameServiceImpl service,
            QueryGameSessionsRequestType queryGameSessionsRequest) 
        throws java.rmi.RemoteException 
    {
        int netIds[] = queryGameSessionsRequest.getNetId();
                        
        QueryGameSessionsResponseType response = new QueryGameSessionsResponseType();
        initResponse(queryGameSessionsRequest, response);
            
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
            
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
                
            List gameSessionsList = GameSessionDML.getGameSessions(conn, netIds);
            
            if (gameSessionsList != null && gameSessionsList.size() > 0) {
                GameSessionType[] sess = new GameSessionType[gameSessionsList.size()];
                sess = (GameSessionType[]) gameSessionsList.toArray(sess);
                response.setGameSession(sess);
                
            } else
                response.setGameSession(null);
                
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("QueryGameSessions error: ", e);
                
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.OGS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.OGS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);       
            log.error("QueryGameSessions error: ", e);
            
        } finally {
            try {
                if (conn != null)
                    conn.close();
            }
            catch (SQLException e) {
                log.warn("QueryGameSessions error while closing connection: ", e);
            }
        }                
                
        return response;
    }
}
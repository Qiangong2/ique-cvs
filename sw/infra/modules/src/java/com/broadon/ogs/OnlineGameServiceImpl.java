package com.broadon.ogs;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.sql.DataSource;
import javax.xml.rpc.ServiceException;
import javax.xml.rpc.server.ServiceLifecycle;
import javax.xml.rpc.server.ServletEndpointContext;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.servlet.ServletConstants;
import com.broadon.trans.common.AuditLog;
import com.broadon.trans.common.Logger;

import com.broadon.wsapi.ias.*;

/**
 * OnlineGameServiceImpl provides the implementation for
 * the OnlineGame Services.
 * @author angelc
 * * @version $Revision: 1.4 $
 */
public class OnlineGameServiceImpl implements ServiceLifecycle, ServletConstants
{   
    /** Servlet Context */
    protected ServletContext servletContext;
    
    /** DataSource for connecting to the DB */
    protected DataSource dataSource;
    
    /** Handle to the IAS Service */
    protected IdentityAuthenticationPortType iasPort;

    /** Audit log hander. */
    protected AuditLog auditLog;

    /** Regional Center Id of this server. */ 
    protected int regionalCenterId;

    /**IAS URLs */
    protected URL iasUrl;

    // Uses apache commons logger (follows axis logging)
    protected static Log log = 
        LogFactory.getLog(OnlineGameServiceImpl.class.getName());
    
    private static final String DEF_REGIONAL_CENTER = "1";
    private static final String CFG_REGIONAL_CENTER = "REGIONAL_CENTER_Id";
 
    private static final String CFG_IAS_URL = "IAS_URL";
    
    public OnlineGameServiceImpl()
    {
    	// Force initialization of StatusCode class
    	try {
    	    Class.forName("com.broadon.ogs.StatusCode");
    	} catch  (ClassNotFoundException e) {
    	    log.error("Cannot initialize OGS StatusCode");
    	}
    }
    
   /**
     * Called by the JAX RPC runtime to initialize
     * the service endpoint.  Implements ServiceLifecycle.
     */
    public void init(Object context) throws ServiceException  
    { 
        log.debug("OnlineGame Service Init");

        ServletEndpointContext soapContext = (ServletEndpointContext) context;                
        servletContext = soapContext.getServletContext();
        
        // Get server properties (requires ConfigListener)
        
        // Get data source (set by OracleDSListener)
        dataSource = (DataSource)
            servletContext.getAttribute(DATA_SOURCE_KEY);
        
        // Get audit log (set by AuditLogManager)
        auditLog = (AuditLog) servletContext.getAttribute(Logger.LOGGER_KEY);
        
        // Property file defined in the conf/BBserver.properties.tmpl file.
        // Add properties there as required for the ECS server.
        //
        Properties cfg = (Properties) servletContext.getAttribute(PROPERTY_KEY);

        // Get Region Id
        //String regionCenterKey = (String) servletContext.getAttribute(REGIONAL_CENTER_KEY);
        //if (regionCenterKey != null) {
        //    regionalCenterId = Integer.parseInt(regionCenterKey);
        //}
        regionalCenterId = Integer.parseInt(
        		getCfg(cfg, CFG_REGIONAL_CENTER, DEF_REGIONAL_CENTER));
        
        // Get IAS URLs
        iasUrl = getURLCfg(cfg, CFG_IAS_URL, null);

        IdentityAuthenticationService iasService = new IdentityAuthenticationServiceLocator();
        iasPort = (iasUrl == null)? iasService.getIdentityAuthenticationSOAP():
                        iasService.getIdentityAuthenticationSOAP(iasUrl);
        
        try {
            BuddyStateMachine.init(dataSource, log);
        } 
        catch (SQLException se) {
            throw new ServiceException("SQLException while initializing BuddyStateMachine: " + se);
        }
    } 

    /**
     * Called by the JAX RPC runtime to destroy resources
     * used by the service endpoint at the end of its lifecycle.
     * Implements ServiceLifecycle.
     */
    public void destroy() 
    {
        log.debug("OnlineGame Service Destroy");
    }

    // Helper functions for getting the configuration
    
    private URL getURLCfg(Properties cfg, String key, String defValue)
        throws ServiceException
    {
        String urlStr = getCfg(cfg, key, defValue);
        try {
            URL url = (urlStr != null)? new URL(urlStr): null;
            return url;
        }
        catch (MalformedURLException e) {
            throw new ServiceException("Malformed " + key+ ": " + urlStr, e);
        }
    }

    private String getCfg(Properties cfg, String key, String defValue)
    {
        String v = (cfg != null)? cfg.getProperty(key): null;
        if (v == null) {
            v = defValue;
            log.warn("BBserver.properties missing " + key + " value" +
                     " ... using default of " + defValue);
        }
        return v;
    }

    public DataSource getDataSource()
    {
        return dataSource;
    }
    
    public IdentityAuthenticationPortType getIasSOAP()
    {
    	return iasPort;
    }
    
    public AuditLog getAuditLog()
    {
        return auditLog;
    }
    
    public int getRegionId()
    {
        return regionalCenterId;
    }
}


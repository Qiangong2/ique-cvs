package com.broadon.ogs;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

public class OnlineGameFunction {
    public static final int STATUS_OK = 0;
    
    protected static String VERSION = "1.0";
    
    protected static String SELECT_CURRENT_TIMESTAMP =
        "select BCCUTIL.ConvertTimeStamp(SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)) FROM DUAL";
    
    /* Static helper functions */

    /**
     * Returns the current server time in milliseconds
     * @return
     */
    protected static long getServerTimestamp()
    {
        return System.currentTimeMillis();
    }
    
    /**
     * Returns the current DB time in milliseconds
     */
    protected static long getCurrentDbTimestamp(Connection conn)
        throws SQLException
    {
        Statement stmt = null;
        
        try {
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(SELECT_CURRENT_TIMESTAMP);
            rs.next();
            return rs.getLong(1);
        } finally {
            if (stmt != null) {
                stmt.close();
            } 
        }
    }

    /**
     * Initializes fields in a AbstractResponseType given an AbstractRequestType
     * @param request
     * @param response
     */
    protected static void initResponse(com.broadon.wsapi.ogs.AbstractRequestType request,
    		com.broadon.wsapi.ogs.AbstractResponseType response)
    {
        response.setTimeStamp(getServerTimestamp());
        response.setMessageId(request.getMessageId());
        response.setVersion(request.getVersion());
        setErrorCode(StatusCode.SC_OK, null, response);
        if (!VERSION.equals(request.getVersion().trim())) {
            setErrorCode(StatusCode.OGS_BAD_VERSION, null, response);
        }
    }

    /**
     * Sets the error code in AbstractResponseType
     * @param String code
     * @param String message
     * @param AbstractResponseType response
     */
    protected static void setErrorCode(String code, 
                                       String message,
                                       com.broadon.wsapi.ogs.AbstractResponseType response)
    {
        response.setErrorCode(Integer.parseInt(code));
        if (message != null) {
            response.setErrorMessage(StatusCode.getMessage(code) + ": " + message);
        }
        else {
            response.setErrorMessage(StatusCode.getMessage(code));
        }
    }  
 
    protected static void initIASRequest(com.broadon.wsapi.ias.AbstractRequestType req) {
	req.setVersion("1.0");
	req.setLocale(com.broadon.wsapi.ias.LocaleType.en_US);
	req.setTimeStamp(getServerTimestamp());  
    }
}

package com.broadon.ogs;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ogs.*;

/** The class used to serve VNG server request to obtain ranked game score
 *  for a given set of game_id/score_id/item_id
 */

public class GetRankedGameScore extends OnlineGameFunction
{
   // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(GetRankedGameScore.class.getName());

   
    public static GetRankedGameScoreResponseType getRankedGameScore(
            OnlineGameServiceImpl service,
            GetRankedGameScoreRequestType getRankedGameScoreRequest) 
        throws java.rmi.RemoteException 
    {
        Integer accountId = getRankedGameScoreRequest.getAccountId();
        int gameId = getRankedGameScoreRequest.getGameId();
        int scoreId = getRankedGameScoreRequest.getScoreId();
        int itemId = getRankedGameScoreRequest.getItemId();
        int from = getRankedGameScoreRequest.getRankBegin();
        int count = getRankedGameScoreRequest.getMaxResults();
                
        GetRankedGameScoreResponseType response = new GetRankedGameScoreResponseType();
        initResponse(getRankedGameScoreRequest, response);
            
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
            
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            
            List rankedScoreList = GameScoreDML.getRankedGameScore(conn, accountId, gameId, scoreId,
                                            itemId, from, count);
                
            if (rankedScoreList != null && rankedScoreList.size() > 0) {
                RankedScoreResultType[] rankedScoreResult = new RankedScoreResultType[rankedScoreList.size()];
                rankedScoreResult = (RankedScoreResultType[]) rankedScoreList.toArray(rankedScoreResult);
                response.setRankedScoreResult(rankedScoreResult);
                    
            } else {
                response.setRankedScoreResult(null);
                setErrorCode(StatusCode.OGS_GET_RANKED_GAME_SCORE_ERROR, null, response);
            }
                
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("GetRankedGameScore error: ", e);
                
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.OGS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.OGS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);       
            log.error("GetRankedGameScore error: ", e);
            
        } finally {
            try {
                if (conn != null)
                    conn.close();
            }
            catch (SQLException e) {
                log.warn("GetRankedGameScore error while closing connection: ", e);
            }
        }                
                
        return response;
    }
}
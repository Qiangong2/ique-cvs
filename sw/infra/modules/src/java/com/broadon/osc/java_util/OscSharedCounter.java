package com.broadon.osc.java_util;

import java.util.HashMap;


/** OscSharedCounters creates a singleton map from counter-names to
 *  specific counters, and synchronizes access to the counters from
 *  multiple servlet threads.
 */
public final class OscSharedCounter
{
    private static final HashMap counterMap = new HashMap();

    private long counter;
    private OscSharedCounter(long initValue) {counter = initValue;}

    // ------------- Public accessor functions ------------------
    // ----------------------------------------------------------

    static public OscSharedCounter getInstance(String nm, long initValue)
    {
        OscSharedCounter c;
        synchronized(counterMap) {
            c = (OscSharedCounter)counterMap.get(nm);
            if (c == null) {
                c = new OscSharedCounter(initValue);
                counterMap.put(nm, c);
            }
        }
        return c;
    }

    public synchronized long get() {return counter;}
    public synchronized long incr() {return ++counter;}
    public synchronized long decr() {return ++counter;}
    public synchronized void set(long num) {counter = num;}
} // class OscSharedCounter

package com.broadon.osc.servlet.monitor;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.broadon.db.DBException;
import com.broadon.filter.authWrapper;
import com.broadon.osc.java_util.OscLogger;
import com.broadon.osc.java_util.OscSharedCounter;
import com.broadon.servlet.ContextStatus;
import com.broadon.servlet.ServletConstants;


/**
 * The <code>Monitor</code> servlet handles requests from a probe to
 * check that the osc server is up and functional.
 *
 * @version $Revision: 1.5 $
 */
public class Monitor extends HttpServlet implements ServletConstants
{
    private static final String	CONTENT_TYPE = "text/plain; charset=utf-8";
    private static final String DB_TEST = "SELECT SYSDATE FROM DUAL";

    // XML tag for the Depot ID (aka HR_ID) request parameter.
    //
    private static final String HR_ID_KEY = "hr_id";

    private ServletContext   ctx = null;
    private OscLogger        logger = null;  // A thin wrapper around log4j
    private OscSharedCounter htmRequests = null;
    private OscSharedCounter rpcRequests = null;


    public synchronized void init(ServletConfig servletConfig)
        throws ServletException
    {
        ctx = servletConfig.getServletContext();
        logger = new OscLogger(Logger.getLogger(OscLogger.class.getName()));
        htmRequests = OscSharedCounter.getInstance("HTM_REQUESTS", 0);
        rpcRequests = OscSharedCounter.getInstance("RPC_REQUESTS", 0);
    }


    public void doPost(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
	ContextStatus status = (ContextStatus) ctx.getAttribute(MONITOR_STATUS_KEY);

	if (status.getOperationStatus() == status.OS_LB_NOTIFIED) {
	    res.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
	    status.setHealth(false);
	    return;
	}

        // Check if DepotID in the request matches that in the certificate,
        // when an authenticated request is received.
        //
        authWrapper	wreq = (authWrapper)req;
        if (wreq.hasClientAuth() &&
            !wreq.getEncodedDepotID().equals(req.getParameter(HR_ID_KEY))) {
            res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            status.setHealth(false);
            return;
        }

        // Prepare response header
        //
        res.setStatus(HttpServletResponse.SC_ACCEPTED);
        res.setContentType(CONTENT_TYPE);

        try {
            // Test access to the database
            //
            dbAccessTest(wreq);
            status.setHealth(true);
        } catch (DBException e) {
            res.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            status.setHealth(false);
        }
    } // process


    private void dbAccessTest(authWrapper wreq) throws DBException
    {
        // Tests DB connectivity; let the caller deal with exceptions.
        //
        Connection        conn = null;
        PreparedStatement ps = null;
        ResultSet         rs = null;
        try {
            conn = wreq.getConnection();
            ps = conn.prepareStatement(DB_TEST);
            rs = ps.executeQuery();
            if (!rs.next()) {
                throw new DBException("Could not access osc DB system time");
            }
            else {
                rs.close(); rs = null;
                ps.close(); ps = null;
                conn.close(); conn = null;
            }
        } catch (SQLException e1) {
            if (rs != null) {
                try {rs.close();} catch (SQLException e2) {/* ignored */}
            }
            if (ps != null) {
                try {ps.close();} catch (SQLException e3) {/* ignored */}
            }
            if (conn != null) {
                try {conn.close();} catch (SQLException e4) {/* ignored */}
            }
            throw new DBException(e1.getMessage()); // Report original msg
        }
    } // dbAccess

} // class Monitor

package com.broadon.osc.java_util;

import java.io.PrintWriter;

import javax.servlet.http.*;

import com.broadon.exception.InvalidRequestException;
import com.broadon.status.StatusCode;

/** The class used to serve VNG server request for member/device login
 *  and return the resultset in XML document back to the VNG server.
 */

public class OscResultVngLogin extends OscResultBean
{
    /** Rules for creating a MemberLogin result object
     */
    public static class Rule extends OscResultRule {
        
        public static final String VNG_LOGIN = "vng_login";
        public static final String VNG_LOGOUT = "vng_logout";
        public static final String KEEP_ALIVE = "keep_alive";

        // An enumeration of the types of login functions we support
        //
        public static class ListType {
            private int enumeration;
            private ListType(int e) {enumeration = e;}
            public int intValue() {return enumeration;}
            public static final ListType LOGIN = new ListType(0);
            public static final ListType LOGOUT = new ListType(1);
            public static final ListType ALIVE = new ListType(2);
        }
        
        private ListType tp;
        
        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
            if (nm.equals(VNG_LOGIN)) tp = ListType.LOGIN;
            else if (nm.equals(VNG_LOGOUT)) tp = ListType.LOGOUT;
            else if (nm.equals(KEEP_ALIVE)) tp = ListType.ALIVE;
            else {
                context.getLogger().error("Invalid name ("  + nm +
                                          ") in " + getClass().getName() +
                                          " using " + VNG_LOGIN);
                tp = ListType.LOGIN;
            }
        }
        public boolean supports(ServerType t)
        {
            return t == ServerType.RPC;
        }
        public ListType getListType() {return tp;}
        public OscResultBean create(HttpServletRequest req,  
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
            return new OscResultVngLogin(req, res, this);
        }
    } // class Rule


    // --------------------------------------------
    // Invariant values
    // --------------------------------------------

    private static final boolean[] needLogin = {
	false,
	true,
        true
    };

    public static final String USER = "user";
    public static final String DEVICE = "device";

    // --------------------------------------------
    // Member variables
    // --------------------------------------------

    private final OscResultVngLogin.Rule 	rule;
    private final OscContext          		context;
    private final OscLogger          		logger;
    private final String                        actionName;
    private String                              resultXML;
    private String				memberID;
    private String				passwd;
    private String				type;
    private String				status;
    private String				statusMsg;

    // ----------------------------------------------------------
    // Hidden constructors and helper methods (all constructors should
    // be hidden for result objects, since they are constructed by the
    // Rule class)
    // ---------------------------------------------------------

    private OscResultVngLogin(HttpServletRequest  req,  
                                 HttpServletResponse res,
                                 OscResultVngLogin.Rule rule)
        throws InvalidRequestException
    {
        this.rule = rule;
        context = rule.getContext();
        logger = context.getLogger();
        actionName = rule.getActionNm();

        resultXML = "";
	status = OscStatusCode.SC_OK;
	statusMsg = StatusCode.getMessage(status);

        memberID = req.getParameter(ORP.REQ_LOGIN_ID);
        passwd = req.getParameter(ORP.REQ_LOGIN_PWD);
        type = req.getParameter(ORP.REQ_LOGIN_TYPE);

        if ((actionName.equals(rule.VNG_LOGIN) && 
             (memberID == null || memberID.equals("") || 
              passwd == null || passwd.equals("") ||
              type == null || type.equals("") || (!type.equals(USER) && !type.equals(DEVICE))
             )) || 
             (actionName.equals(rule.VNG_LOGOUT) && 
              (type == null || type.equals("") || (!type.equals(USER) && !type.equals(DEVICE)))
             )
           )
        {
            status = OscStatusCode.OSC_BAD_REQUEST;
            statusMsg = OscStatusCode.getMessage(OscStatusCode.OSC_BAD_REQUEST);
            logger.error(statusMsg + " for " + actionName +
                        "(memberID = " + memberID + 
                        ", passwd = " + passwd + 
                        ", type = " + type + ")");
            return;
        }
    }

    // --------------------------------------------
    // Specialization of abstract methods
    // --------------------------------------------

    public boolean requiresLogin() {return needLogin[rule.getListType().intValue()];}

    public String jspFwdUrl() 
    {
        return null;
    }

    public void encodeXml(PrintWriter out)
    {
        // Encodes the final results
        //
        out.println("<" + ORP.RPC_RESULT_TAG + ">");

        context.encodeXml_TNL(out, ORP.REQ_ACTION, actionName);
        context.encodeXml_TNL(out, STATUS_TAG, status);
        context.encodeXml_TNL(out, STATUS_MSG_TAG, statusMsg);

        if (!getIsError() && !resultXML.equals(""))
            out.println(resultXML);

        if (actionName.equals(rule.VNG_LOGIN) || actionName.equals(rule.VNG_LOGOUT))
            context.encodeXml_TNL(out, ORP.REQ_LOGIN_TYPE, type);

        out.println("</" + ORP.RPC_RESULT_TAG + ">");
    } // encodeXml

    public void serveRequest(OscLoginSession loginSession)
        throws InvalidRequestException
    {
        if (!getIsError())
        {
            OscDbConnection conn = null;

            try 
            {
                if (actionName.equals(rule.VNG_LOGIN))
                {
                    loginSession.setDeviceLogin(true);
                    loginSession.login(memberID, passwd);

                    if (loginSession.isLoggedIn())
                    {
                        if (type.equals(USER))
                        {
                            String mShipId = String.valueOf(loginSession.getMembershipID());
                            String mId = String.valueOf(loginSession.getMemberID());
                            conn = context.getDB(true);
                            resultXML = OscDmlBuddy.getMemberLoginDetails(conn, mShipId, mId);
                        }
                    } else {
                        status = OscStatusCode.OSC_INCORRECT_LOGIN;
                        statusMsg = OscStatusCode.getMessage(OscStatusCode.OSC_INCORRECT_LOGIN);
  
                        logger.error(statusMsg + " for " + actionName);
                        return;
                    }
               } else if (actionName.equals(rule.VNG_LOGOUT)) {
                    String mShipId = String.valueOf(loginSession.getMembershipID());
                    String mId = String.valueOf(loginSession.getMemberID());

                    loginSession.logout();
  
                    if (!loginSession.isLoggedIn())
                    {
                        if (type.equals(USER))
                        {
                            conn = context.getDB(true);
                            resultXML = OscDmlBuddy.getMemberLoginDetails(conn, mShipId, mId);
                        }
                    } else {
                        status = OscStatusCode.OSC_INCORRECT_LOGOUT;
                        statusMsg = OscStatusCode.getMessage(OscStatusCode.OSC_INCORRECT_LOGOUT);
                        logger.error(statusMsg + " for " + actionName);
                        return;
                    }
                }

            } catch (OscLoginSession.InternalLoginException e) {
                status = OscStatusCode.OSC_INTERNAL_ERROR_MSG;
                statusMsg = OscStatusCode.getMessage(OscStatusCode.OSC_INTERNAL_ERROR_MSG) +
                   ": " + e.getMessage();
                e.printStackTrace();
                logger.error(statusMsg + " for " + actionName);

            } catch (Exception e) {
                status = OscStatusCode.VNG_DB_ERROR;
                statusMsg = OscStatusCode.getMessage(OscStatusCode.VNG_DB_ERROR) +
                   ": " + e.getMessage();
                e.printStackTrace();
                logger.error(statusMsg + " for " + actionName);
            }

            finally {
                if (conn != null)
                    conn.close();
            }
        }
    }

    public String toString() {return null;}

    public boolean getIsError()
    {
        return !status.equals(OscStatusCode.SC_OK);
    }

    public String getErrorMsg()
    {
        return statusMsg;
    }

} // class OscResultVngLogin

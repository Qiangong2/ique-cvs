package com.broadon.osc.java_util;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.broadon.exception.InvalidRequestException;

/** The class used to create the results associated with a purchaseable
 *  titles list request for a given player id, such that the appropriate 
 *  document and header can be created in the response back to the client
 */
public class OscResultPurchaseTitleList extends OscResultBean
{
    /** Rules for creating a PurchaseTitleList result object
     */
    public static class Rule extends OscResultRule {
        
        public static final String PURCHASE_LIST = "purchase_title_list";
        public static final String TRIAL_LIST = "trial_title_list";
        public static final String FREE_LIST = "free_manual_list";

        // An enumeration of the types of PurchaseTitleLists we support
        //
        public static class ListType {
            private int enumeration;
            private ListType(int e) {enumeration = e;}
            public int intValue() {return enumeration;}
            public static final ListType PURCHASE = new ListType(0);
            public static final ListType TRIAL = new ListType(1);
            public static final ListType FREE = new ListType(2);
        }
        
        private ListType tp;
        
        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
            if (nm.equals(PURCHASE_LIST)) tp = ListType.PURCHASE;
            else if (nm.equals(TRIAL_LIST)) tp = ListType.TRIAL;
            else if (nm.equals(FREE_LIST)) tp = ListType.FREE;
            else {
                context.getLogger().error("Invalid name ("  + nm +
                                          ") in " + getClass().getName() +
                                          " using " + FREE_LIST);
                tp = ListType.FREE;
            }
        }
        public boolean supports(ServerType t)
        {
            return t == ServerType.HTM;
        }
        public ListType getListType() {return tp;}
        public OscResultBean create(HttpServletRequest req,  
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
            return new OscResultPurchaseTitleList(req, res, this);
        }
    } // class Rule


    private static class Pair {
        private Object first;
        private Object second;
        Pair(Object o1, Object o2) {first=o1;second=o2;}
        Object getFirst() {return first;}
        Object getSecond() {return second;}
    } // class Pair

    // --------------------------------------------
    // Invariant values
    // --------------------------------------------

    private static final String PURCHASE_TITLE_LIST_JSP = "PurchaseTitleList.jsp";
    private static final String SELECT_PURCHASE_TITLE_LIST = 
                "SELECT title, title_id, eunits, title_size/("+BBCARD_BLKSIZE+") blocks  " +
                "FROM CURRENT_CONTENT_CATALOG_V WHERE title_type='visible'";

    private static final String TRIAL_TITLE_LIST_JSP = "TrialTitleList.jsp";
    private static final String SELECT_TRIAL_TITLE_LIST = 
                "SELECT title, title_id, eunits, title_size/("+BBCARD_BLKSIZE+") blocks, trial_limits  " +
                "FROM CURRENT_CONTENT_CATALOG_V WHERE title_type='visible' AND rtype IN ('TR', 'LR') " +
                "AND trial_limits IS NOT NULL";

    private static final String FREE_MANUAL_LIST_JSP = "FreeManualList.jsp";
    private static final String SELECT_FREE_MANUAL_LIST = 
                "SELECT title, title_id, eunits, title_size/("+BBCARD_BLKSIZE+") blocks " +
                "FROM CURRENT_CONTENT_CATALOG_V WHERE title_type='manual'";

    // Thread shared cached value;  If further optimization is necessary,
    // try to use thread local versions of the unsynchronized HashMap.
    //
    private static final OscCachedMap cachedPurchaseList = new OscCachedMap(100);
    private static final OscCachedMap cachedTrialList = new OscCachedMap(100);
    private static final OscCachedMap cachedFreeList = new OscCachedMap(100);

    // Arrays of the above indexed by Rule.ListType.toInt() values
    //
    private static final String[] jspPage = {
        PURCHASE_TITLE_LIST_JSP,
        TRIAL_TITLE_LIST_JSP,
        FREE_MANUAL_LIST_JSP
    };
    private static final String[] sqlStmt = {
        SELECT_PURCHASE_TITLE_LIST,
        SELECT_TRIAL_TITLE_LIST,
        SELECT_FREE_MANUAL_LIST
    };
    private static final OscCachedMap[] cachedLists = {
        cachedPurchaseList,
        cachedTrialList,
        cachedFreeList
    };
    
    private static final boolean[] needLogin = {
        false,
        false,
        false
    };


    // --------------------------------------------
    // Member variables
    // --------------------------------------------

    private final OscResultPurchaseTitleList.Rule 	rule;
    private final OscContext          			    context;
    private final OscLogger          			    logger;
    private final String                            actionName;
    private final Integer                           storeID;
    private String                                  titleXML;
    private int                                     recordCount; 

    // ----------------------------------------------------------
    // Hidden constructors and helper methods (all constructors should
    // be hidden for result objects, since they are constructed by the
    // Rule class)
    // ---------------------------------------------------------

    private OscResultPurchaseTitleList(HttpServletRequest  req,  
                                       HttpServletResponse res,
                                       OscResultPurchaseTitleList.Rule rule)
        throws InvalidRequestException
    {
        this.rule = rule;
        context = rule.getContext();
        logger = context.getLogger();
        actionName = rule.getActionNm();
        storeID = context.getStoreId(req, context.getRequestVersion(req));
        titleXML = "";
        recordCount = -1;
    }


    private void setCache (OscCachedObject cache, String xml, int recordCount)
    {
        cache.setCached(new Pair(xml, new Integer(recordCount)));
    }
    
    private String getCachedXml(OscCachedObject cache)
    {
        final Pair p = (Pair)cache.getCached();
        return (p == null? null : (String)p.getFirst());
    }
    
    private int getCachedRecordCount(OscCachedObject cache)
    {
        final Pair p = (Pair)cache.getCached();
        return (p == null? 0 : ((Integer)p.getSecond()).intValue());
    }

    // --------------------------------------------------------------------
    //            Bean Properties
    // --------------------------------------------------------------------

    /**
     * Bean Property: recordCount
     */
    public int getRecordCount() { return recordCount; }

    // --------------------------------------------
    // Specialization of abstract methods
    // --------------------------------------------

    public boolean requiresLogin() {return needLogin[rule.getListType().intValue()];}

    public String jspFwdUrl() 
    {
        return jspPage[rule.getListType().intValue()];
    }

    public void encodeXml(PrintWriter out) {};

    public void serveRequest(OscLoginSession loginSession)
        throws InvalidRequestException
    {
        OscDbConnection conn = null;
        try {
            final long      timeout = context.getContentCacheTimeoutMsecs();
            final Integer   regionID = context.getStoreRegionId(storeID);
            final int       listIdx = rule.getListType().intValue();
            OscCachedObject cache = null;
            String          xml = null;
            int             count = 0;
        
            // First, try to use cached value
            //
            if (timeout > 0) {
                cache = cachedLists[listIdx].getMapped(regionID);
                if (cache.isValid(timeout)) {
                    if (logger.isTracing())
                        logger.info("Using CACHED " + actionName);
                    count = getCachedRecordCount(cache);
                    xml = getCachedXml(cache);
                }
            }

            // If the value was not cached, access the DB
            //
            if (xml == null) {
                final String sql = sqlStmt[listIdx] + 
                    " AND region_id = " + regionID + " ORDER BY purchase_start_date desc";
                conn = context.getDB(false);
                count = conn.GetRecordCount(sql);
                xml = conn.queryXML(sql);
                if (cache != null)
                    setCache(cache, xml, count);
            }
            titleXML = xml;
            recordCount = count;
        }
        catch (Exception e) {
            logger.error("Failed to obtain " + actionName + "game catalog:" +
                         e.getMessage());
            throw new InvalidRequestException("Failed to obtain catalog");
        }
        finally {
            if (conn != null)
                conn.close();
        }
    }

    public String toString() {return null;}

    // --------------------------------------------
    // Definition of result specific bean values used
    // for formatting a result or access through
    // JSP pages.
    // --------------------------------------------

    public String transformXML(String xmlStr, String xslFile, String locale)
    { return context.transformXML(xmlStr,xslFile,locale); }

    public String getXML() {return titleXML;}

} // class OscResultPurchaseTitleList

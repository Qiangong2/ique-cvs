package com.broadon.osc.java_util;

import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.broadon.exception.InvalidRequestException;

/** The class used to create the results associated with a Service
 *  request, such that the appropriate document and header can be
 *  created in the response back to the client
 */
public class OscResultServiceSuggest extends OscResultBean
{
    /** Rules for creating a service result object
     */
    public static class Rule extends OscResultRule {
    	
        public static final String SERVICE_SUGGEST = "ser_suggest";
        
        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
        }
        public boolean supports(ServerType t)
        {
            return t == ServerType.HTM;
        }
        public OscResultBean create(HttpServletRequest req,  
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
            return new OscResultServiceSuggest(req, res, this);
        }
        
    } // class Rule
    
    // --------------------------------------------
    // Invariant values
    // --------------------------------------------

    private static final String SERVICE_SUGGEST_JSP = "iQueServiceSuggest.jsp";
    private static final String SEARCH_SUGGEST_ID= "search_Suggest_ID";
    private static final String SUGGEST_MESSAGE= "suggest_Message";
    private static final String DEFAULT_LOCALE="zh_CN";
    private static final String SQL_SUGGEST_SEARCH =
                "select rownum, mes1.subject as subject_q, mes1.message as message_q, to_char(mes1.create_date,'yyyy-mm-dd hh24:mm:ss') as date_q, mes2.subject as subject_r, mes2.message as message_r, mes2.create_date as date_r "+
                "from ique_messages mes1, ique_messages mes2 where mes1.message_type='sg' and mes1.membership_id=? and mes1.member_id=? and mes1.message_id=? and mes1.message_id=mes2.reply_id(+) and rownum<2";
    
    private static final String SQL_INSERT_SUGGEST =
                "INSERT INTO ique_messages(message_id, message_type, membership_id, member_id, subject, message,ip_address) " +
    		"VALUES(?,?,?,?,?,?,?)";
    		
    private static final String SQL_INSERT_SUGGEST_RECEIVERS =
                "INSERT INTO ique_message_receivers(message_id, membership_id, member_id) VALUES(?,?,?)";
                
    private static final String SQL_NEXT_SUGGEST_SEQ =
    		"SELECT message_id_seq.nextval from dual";
    
    // --------------------------------------------
    // Member variables
    // --------------------------------------------

    private final OscResultServiceSuggest.Rule          rule;
    private final OscContext          		        context;
    private final OscLogger          			logger;
    private String 				        listXML;
    private String					contentXML;
    private String                                      actionName;
    private String                                      locale;
    private int				                searchSuggestID;
    private String                                      suggestSubject;
    private String                                      suggestContent;
    private long                                        message_id;
    private String                                      message_type;
    private long                                        membership_id;
    private long                                        member_id;
    private String                                      sg_membership_id;
    private String                                      sg_member_id;
    private String                                      ip_address;

    // --------------------------------------------
    // Hidden constructors (all constructors should
    // be hidden)
    // --------------------------------------------

    private OscResultServiceSuggest(HttpServletRequest  req,  
                                    HttpServletResponse res,
                                    OscResultServiceSuggest.Rule rule)
        throws InvalidRequestException
    {
        this.rule = rule;
        context = rule.getContext();
        logger = context.getLogger();
        actionName = rule.getActionNm();
        listXML = "";
        contentXML = "";
	if ((!((req.getParameter(SEARCH_SUGGEST_ID)==null)||(req.getParameter(SEARCH_SUGGEST_ID).trim().equals(""))))&&(OscContext.isDigital(req.getParameter(SEARCH_SUGGEST_ID).trim()))) {
		searchSuggestID=Integer.parseInt(req.getParameter(SEARCH_SUGGEST_ID).trim());
	}else{
		searchSuggestID=0;
	}
	
	suggestSubject="suggest";
	
	if (!((req.getParameter(SUGGEST_MESSAGE)==null)||(req.getParameter(SUGGEST_MESSAGE).trim().equals("")))) {
		suggestContent=req.getParameter(SUGGEST_MESSAGE).trim();
	}
            
        locale = req.getParameter("locale");
        if (locale==null || locale.equals(""))
            locale = DEFAULT_LOCALE;
            
        message_type = "sg";
        ip_address = req.getRemoteAddr();
        sg_membership_id="18863514321";
        sg_member_id="1";
    }
    
    private long getNextMessageIdSeq()
    	throws InvalidRequestException
    {
        // Get the next available Message ID sequence number from the DB.
        //
        long s = 0;
        OscDbConnection conn = null;

        try {
            conn = context.getDB(true);
            ResultSet rs = conn.query(SQL_NEXT_SUGGEST_SEQ);
            if (rs.next()) {
                s = rs.getLong(1);
            }
            rs.close();

        } catch (SQLException e) {
            logger.error("Failed to get next message id(suggest id) from IQUE DB: " + e.getMessage());
            throw new InvalidRequestException("Failed to get next message id(suggest id) from IQUE DB: " + e.getMessage());

        } finally {
            if (conn != null) {
                conn.close();
            }
        }

        return s;
    } // getNextMessageIdSeq
    
    
    // --------------------------------------------------------------------
    //            Bean Properties
    // --------------------------------------------------------------------
    
    
    // --------------------------------------------
    // Specialization of abstract methods
    // --------------------------------------------

    public boolean requiresLogin() {return true;}

    public String jspFwdUrl() 
    {
        return SERVICE_SUGGEST_JSP;
    }

    public void encodeXml(PrintWriter out) {};

    public void serveRequest(OscLoginSession loginSession)
        throws InvalidRequestException
    {
        OscDbConnection conn = null;
        PreparedStatement stmt = null;
        membership_id = loginSession.getMembershipID();
	member_id = loginSession.getMemberID();
	
        try {
            String sql = null;
    
	    conn = context.getDB(false);

	    if ((suggestContent != null)&&(suggestContent.trim() != "")){
                message_id = getNextMessageIdSeq();
	        stmt = conn.prepareStatement(SQL_INSERT_SUGGEST);
	        stmt.setLong(1, message_id);
	        stmt.setString(2, message_type);
	        stmt.setLong(3, membership_id);
	        stmt.setLong(4, member_id);
	        stmt.setString(5, suggestSubject);
	        stmt.setString(6, suggestContent);
	        stmt.setString(7, ip_address);
                stmt.executeUpdate();
                conn.closeStatement(stmt);
                stmt = null;

                stmt = conn.prepareStatement(SQL_INSERT_SUGGEST_RECEIVERS);
                stmt.setLong(1, message_id);
	        stmt.setLong(2, Long.parseLong(sg_membership_id));
	        stmt.setLong(3, Long.parseLong(sg_member_id));
                stmt.executeUpdate();
                conn.closeStatement(stmt);
                stmt = null;

                conn.commit();
                contentXML = "1";
	    }else{
	        if (searchSuggestID>0) {
	    	    sql= SQL_SUGGEST_SEARCH;
	    	    sql = sql.substring(0,sql.indexOf("?")) + String.valueOf(membership_id) + sql.substring(sql.indexOf("?")+1);
	    	    sql = sql.substring(0,sql.indexOf("?")) + String.valueOf(member_id) + sql.substring(sql.indexOf("?")+1);
	    	    sql = sql.substring(0,sql.indexOf("?")) + Integer.toString(searchSuggestID) + sql.substring(sql.indexOf("?")+1);
	    	    contentXML = conn.queryXML(sql);
	        }	    
	    }
        }
	catch (SQLException e) {
            try {
                conn.rollback();
            } catch (SQLException ex) {}
            
            logger.error("Failed to process Search or Enter Suggest " + e.getMessage());
            throw new InvalidRequestException("Failed to process Search or Enter Suggest.");

        } finally {
            if (conn != null) {
            	if ((suggestSubject != null)&&(suggestContent != null)){
                    conn.closeStatement(stmt);
                }
                conn.close();
            }
        }
    }

    public String toString() {return null;}

    // --------------------------------------------
    // Definition of result specific bean values used
    // for formatting a result or access through
    // JSP pages.
    // --------------------------------------------

    public String transformXML(String xmlStr, String xslFile, String locale)
    { return context.transformXML(xmlStr,xslFile,locale); }

    public String getContentXML() {return contentXML;}
 
    public long getMessageID() {return message_id;}

} // class OscResultServiceSuggest


package com.broadon.osc.java_util;

import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.broadon.exception.InvalidRequestException;


/** The class used to create the results associated with an account update
 *  request, such that the appropriate document and header can be
 *  created in the response back to the client
 */
public class OscResultAcctUpdate extends OscResultBean
{
    /** Rules for creating a Login result object
     */
    public static class Rule extends OscResultRule {

        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
        }
        public boolean supports(ServerType t)
        {
            return t == ServerType.HTM;
        }
        public OscResultBean create(HttpServletRequest req,
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
            return new OscResultAcctUpdate(req, res, this);
        }
    } // class Rule

    // --------------------------------------------
    // Invariant values
    // --------------------------------------------

    private static final String UPDATE_JSP = "AccountUpdateForm.jsp";

    private static final String SQL_QUERY_ACCOUNT =
        "SELECT mb.name, mb.gender, to_char(mb.birthdate,'yyyy/mm/dd') " +
        "as birthdate, mb.personal_id, mb.member_email, ms.telephone, " +
 		"ms.mobile_phone, ms.address, ms.postal_code, mb.province, mb." +
 		"vocation, mb.education, mb.income, mb.personal_id, mb.nick_name FROM " +
 		"ique_memberships ms, ique_members mb WHERE ms.membership_id = " +
 		"mb.membership_id AND mb.membership_id = ? AND mb.member_id = ?";

    private static final String SQL_QUERY_PASSWD_VALID =
        "SELECT pseudonym FROM ique_members WHERE membership_id = ? AND " +
        "member_id = ? AND pin=?";

    private static final String SQL_UPDATE_MEMBERSHIPS =
        "UPDATE ique_memberships SET address = ?, telephone = ?, " +
        "postal_code = ? WHERE membership_id = ?";
    
    
    // --------------------------------------------
    // Member variables
    // --------------------------------------------
    private final OscResultAcctUpdate.Rule  rule;
    private final OscLogger  		        log;
    private final OscContext                context;
    
    private boolean update;
    private String sql_update_members;
    private String error_code;
    private String failureMsg;
    private String postal_code;
    private String province;
    private String income;
    private String vocation;
    private String education;
    private String address;
    private String telephone;
    private String personal_id;
    private String member_email;
    private String nickname;
    private String name;
    private String gender;
    private String old_pin;
    private String new_pin;
    private String new_pin1;
    private String year;
    private String month;
    private String day;
    private String birthdate;

    private Object[] resultArray = null;

    // --------------------------------------------
    // Hidden constructors (all constructors should
    // be hidden for result objects, sicne they are
    // constructed by the Rule class)
    // --------------------------------------------

    private OscResultAcctUpdate(HttpServletRequest  req,
                                HttpServletResponse res,
                                OscResultAcctUpdate.Rule rule)
        	throws InvalidRequestException
    {
        this.rule = rule;
        this.context = rule.getContext();
        this.log = this.context.getLogger();
        
    	if (req.getParameter("update") != null && req.getParameter("update").equals("1"))
    	    this.update = true;
    	else
    	    this.update = false;

        if (this.update) {
            this.old_pin = ( req.getParameter("old_pin")!=null &&
                    		!req.getParameter("old_pin").equals(""))
                    		?req.getParameter("old_pin"):null;

            this.new_pin = ( req.getParameter("new_pin")!=null &&
                       		!req.getParameter("new_pin").equals(""))
                       		?req.getParameter("new_pin"):null;

            this.new_pin1 = ( req.getParameter("new_pin1")!=null &&
                        	 !req.getParameter("new_pin1").equals(""))
                        	 ?req.getParameter("new_pin1"):null;

            this.nickname = ( req.getParameter("nickname")!=null &&
                       	 	 !req.getParameter("nickname").trim().equals(""))
                       	 	 ?req.getParameter("nickname").trim():null;
                                	 
            this.name = ( req.getParameter("name")!=null &&
                    	 !req.getParameter("name").trim().equals(""))
                    	 ?req.getParameter("name").trim():null;


            this.personal_id = ( req.getParameter("personal_id")!=null &&
                           	 	!req.getParameter("personal_id").trim().equals(""))
                           	 	?req.getParameter("personal_id").trim():null;

            this.year = ( req.getParameter("year")!=null &&
                    	 !req.getParameter("year").trim().equals(""))
                    	 ?req.getParameter("year").trim():null;

            this.month = ( req.getParameter("month")!=null &&
                     	  !req.getParameter("month").trim().equals(""))
                     	  ?req.getParameter("month").trim():null;

            this.day = ( req.getParameter("day")!=null &&
                    	!req.getParameter("day").trim().equals(""))
                    	?req.getParameter("day").trim():null;

            this.member_email = ( req.getParameter("member_email")!=null &&
    	                    	 !req.getParameter("member_email").trim().equals(""))
    	                    	 ?req.getParameter("member_email").trim():null;

    	    this.telephone = ( req.getParameter("telephone")!=null &&
                         	  !req.getParameter("telephone").trim().equals(""))
                         	  ?req.getParameter("telephone").trim():null;

            this.province = ( req.getParameter("province")!=null &&
                        	 !req.getParameter("province").trim().equals(""))
                        	 ?req.getParameter("province").trim():null;

            this.postal_code = ( req.getParameter("postal_code")!=null &&
                           		!req.getParameter("postal_code").trim().equals(""))
                           		?req.getParameter("postal_code").trim():null;

       	    if (this.old_pin==null) {
       	        this.failureMsg = "The old pin is null";
       	        this.error_code = "71";
       	    } else if (!(this.new_pin==null && this.new_pin1==null)) {
       	        if(this.new_pin==null){
       	            this.failureMsg = "The new pin is null";
       	            this.error_code = "72";
                } else if (this.new_pin1==null) {
                    this.failureMsg = "The new pin1 is null";
                    this.error_code = "73";
                } else if (!this.new_pin.equals(this.new_pin1)) {
                    this.failureMsg = "The new pin is not equal the new pin1";
                    this.error_code = "74";
                } else if(!OscContext.isValidPassword(this.new_pin)){
                	this.failureMsg = "The new password is invalid";
                    this.error_code = "78";
                } else
					try {
						if (this.context.isoEncodingLength(this.new_pin) > 20){
							this.failureMsg = "new_pin's length is great than 20.";
							this.error_code = "80";
						}
					} catch (UnsupportedEncodingException e3) {
						throw new InvalidRequestException(e3.getMessage());
					}
       	        
       	        
       	    }

       	    if(this.failureMsg == null){
       	        if (this.name==null) {
       	            this.failureMsg = "Name is null";
       	            this.error_code = "3";
                } else if(!OscContext.isValidName(this.name)){
                    this.failureMsg = "name is invalid";
                    this.error_code = "43";
                } else 
			    	try{
			    		if (this.context.utf8EncodingLength(this.name) > 12){			    
			    			this.failureMsg = "name's length is great than 12.";
			    			this.error_code = "81";
			    		}
			    	}catch(UnsupportedEncodingException e3){
			    		throw new InvalidRequestException(e3.getMessage());
			    	}
       	    }
       	    
       	    if(this.failureMsg == null){
       	        if (this.nickname == null) {
			    	this.failureMsg = "nickname is null";
			    	this.error_code = "68";				
			    } else if (!OscContext.isValidNickname(this.nickname.trim())) {
			    	this.failureMsg = "nickname is invalid";
			    	this.error_code = "69";
			    } else 
			    	try{
			    		if (this.context.utf8EncodingLength(this.nickname) > 12){			    
			    			this.failureMsg = "nickname's length is great than 12.";
			    			this.error_code = "77";
			    		}
			    	}catch(UnsupportedEncodingException e3){
			    		throw new InvalidRequestException(e3.getMessage());
			    	}
       	    }
       	    
       	    if(this.failureMsg == null){			    
			    if (!((this.year==null) && (this.month==null) && (this.day==null))){
                    if (this.year==null) {
                        this.failureMsg = "year is null";
                        this.error_code = "4";
                    } else if (!OscContext.isDigital(this.year)) {
                        this.failureMsg = "year is not valid digital.";
                        this.error_code = "23";
                    } else if (this.year.length() != 4) {
                        this.failureMsg = "length of year is not four bits.";
                        this.error_code = "22";
                    } else if (this.month==null) {
                        this.failureMsg = "month is null";
                        this.error_code = "6";
                    } else if (!OscContext.isDigital(this.month)) {
                        this.failureMsg = "month is not valid digital";
                        this.error_code = "25";
                    } else if (this.month.length() != 2) {
                        this.failureMsg = "length of month is not two bits.";
                        this.error_code = "24";
                    } else if (Integer.parseInt(this.month) < 1 || Integer.parseInt(this.month) > 12) {
                        this.failureMsg = "month is out of range";
                        this.error_code = "7";
                    } else if (this.day==null) {
                        this.failureMsg = "day is null";
                        this.error_code = "8";
                    } else if (!OscContext.isDigital(this.day)) {
                        this.failureMsg = "day is not valid digital.";
                        this.error_code = "27";
                    } else if (this.day.length() != 2) {
                        this.failureMsg = "length of day is not two bits.";
                        this.error_code = "26";
                    } else if (Integer.parseInt(this.day) < 1 || Integer.parseInt(this.day) > 31) {
                        this.failureMsg = "day is out of range";
                        this.error_code = "9";
                    } else if (!OscContext.isValidDate(Integer.parseInt(this.year),Integer.parseInt(this.month),Integer.parseInt(this.day))) {
                        this.failureMsg = "birthdate is not valid.";
                        this.error_code = "28";
                    } else {
                        int i = -2, l = -2;
                        try {
                            i = this.context.parseDate(this.year+this.month+this.day+" 23:59:59","yyyyMMdd HH:mm:ss","").compareTo(this.context.getCurrentTime());
                            if (i >= 0) {
                                this.failureMsg="The birthdate can not be equal now or in the future.";
                                this.error_code="41";
                            } else if (i != -2) {
                                l = this.context.parseDate(this.year+this.month+this.day+" 23:59:59","yyyyMMdd HH:mm:ss","").compareTo(this.context.parseDate("19000101 00:00:00","yyyyMMdd HH:mm:ss",""));
                                if (l < 0) {
                                    this.failureMsg = "The birthdate can not be smaller than the year 1900.";
                                    this.error_code = "42";
                                } else if (l == -2) {
                                    this.failureMsg="System error.";
                                    this.error_code="40";
                                }
                            } else if (i == -2) {
                                this.failureMsg = "System error.";
                                this.error_code = "40";
                            }
                        } catch (ParseException e) {
                            throw new InvalidRequestException("Failed to parse date: " + e.getMessage());
                        }
                        if(!getIsError())this.birthdate = this.year+"/"+this.month+"/"+this.day;
                    }

                }else{
                    this.birthdate = null;
                }
            }

            if (this.failureMsg == null) {
                if (this.personal_id==null) {
                    this.failureMsg = "personal_id is null";
                    this.error_code = "10";
                } else
					try {
						if(this.context.utf8EncodingLength(this.personal_id) > 32){
							this.failureMsg = "personal_id is too long";
						    this.error_code = "29";
						}
					} catch (UnsupportedEncodingException e) {
                        throw new InvalidRequestException(e.getMessage());
					}
            }
                
            if (this.failureMsg == null){    
                if (this.member_email == null) {
                    this.failureMsg = "member_email is null";
                    this.error_code = "11";
                } else if (!OscContext.isValidEmailAddress(this.member_email)) {
                    this.failureMsg = "member_email is not valid.";
                    this.error_code = "31";
                } else if (this.telephone == null) {
                    this.failureMsg = "telephone is null";
                    this.error_code = "12";
                } else if (!OscContext.isValidTelephone(this.telephone)) {
                    this.failureMsg = "telephone is not valid.";
                    this.error_code = "32";
                } else if (this.postal_code != null && !OscContext.isDigital(this.postal_code)) {
                    this.failureMsg = "postal code is not valid number.";
                    this.error_code = "39";
                } else if (this.postal_code != null && this.postal_code.length()!=6) {
                    this.failureMsg = "length of postal code is not 6 bits.";
                    this.error_code = "38";
                } else if (this.province==null || this.province.trim().equals("")) {
                    this.failureMsg = "province is null";
                    this.error_code = "13";
                } else {
                    this.gender = ( req.getParameter("gender")!=null &&
                              	   !req.getParameter("gender").trim().equals(""))
                              	   ?req.getParameter("gender").trim():"M";
                    try{
                        this.old_pin = OscContext.getEncryptedPasswd(this.old_pin);
                        if(this.new_pin!=null){
                            this.new_pin = OscContext.getEncryptedPasswd(this.new_pin);
                            this.sql_update_members="UPDATE ique_members SET birthdate=to_date(?,'yyyy/mm/dd'), " +
                            		"name = ?, gender = ?, member_email = ?,province = ?, vocation = ?, education" +
                            		" = ?, income = ?, personal_id = ?, nick_name = ?, pin = ? WHERE membership_id = ? AND " +
                            		"member_id = ? ";
                        }else{
                            this.sql_update_members = "UPDATE ique_members SET birthdate=to_date(?,'yyyy/mm/dd'), " +
                            		"name = ?, gender = ?, member_email = ?, province = ?, vocation = ?, education " +
                            		"= ?, income = ?, personal_id = ? ,nick_name = ? WHERE membership_id = ? AND member_id = ? ";
                        }
                    } catch(NoSuchAlgorithmException e) {
                        throw new InvalidRequestException(e.getMessage());
                    }

                    this.address = ( req.getParameter("address")!=null &&
                               		!req.getParameter("address").trim().equals(""))
                               		?req.getParameter("address").trim():null;

                    this.income = ( req.getParameter("income")!=null &&
                               	   !req.getParameter("income").trim().equals(""))
                               	   ?req.getParameter("income").trim():null;

                    this.vocation = ( req.getParameter("vocation")!=null &&
                                	 !req.getParameter("vocation").trim().equals(""))
                                	 ?req.getParameter("vocation").trim():null;

                    this.education = ( req.getParameter("education")!=null &&
                                 	  !req.getParameter("education").trim().equals(""))
                                 	  ?req.getParameter("education").trim():null;

                }
            }

        }
    }

    // --------------------------------------------
    // Specialization of abstract methods
    // --------------------------------------------

    public boolean requiresSecureRequest() {return true;}
    public boolean requiresLogin() {return true;}

    public String jspFwdUrl()
    {
        return UPDATE_JSP;
    }

    public void encodeXml(PrintWriter out) {};

    public void serveRequest(OscLoginSession loginSession)
        throws InvalidRequestException
    {
        long membership_id = loginSession.getMembershipID();
	    long member_id = loginSession.getMemberID();

        OscDbConnection   conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

    	if (this.update && !getIsError()) {

            try{
                conn = this.context.getDB(true);
                stmt = conn.prepareStatement(SQL_QUERY_PASSWD_VALID);
                stmt.setLong(1,membership_id);
                stmt.setLong(2,member_id);
                stmt.setString(3,this.old_pin);
                rs = stmt.executeQuery();
                if(!rs.next()){
                    this.error_code="34";
                    this.failureMsg="The Old password is not right.";
                }
                rs.close();
                conn.closeStatement(stmt);
                stmt = null;
            } catch (SQLException e) {
                this.log.error("Failed to query account in IQUE DB: " + e.getMessage());
                throw new InvalidRequestException("Failed to query account in IQUE DB: " + e.getMessage());
            } finally {
                if (conn != null) {
                    conn.closeStatement(stmt);
                    conn.close();
                }
            }

            if(!getIsError()){            	
            	boolean isEmailExisting = 
            		OscContext.isEmailExisting(
            				this.context,
            				this.log,
            				this.member_email,
            				membership_id,
            				member_id);
            	if(isEmailExisting){    
            		this.error_code="48";
                    this.failureMsg="The email has been registered.";
                }                
            }
            
            if(!getIsError()){
                boolean isNicknameExisting = 
                	OscContext.isNicknameExisting(
                			this.context,
                			this.log,
                			this.nickname,
                			loginSession.getMembershipID(),
                			loginSession.getMemberID());
                if(isNicknameExisting){
                	this.error_code = "76";
                    this.failureMsg = "This nickname has been registered."; 
                }
            }
            
            if(!getIsError()){
                try{
                    conn = this.context.getDB(false);
                    stmt = conn.prepareStatement(SQL_UPDATE_MEMBERSHIPS);
                    if (this.address == null)
                        stmt.setObject(1,this.address);
                    else
                        stmt.setString(1,this.address);

                    stmt.setString(2,this.telephone);

                    if (this.postal_code == null)
                        stmt.setObject(3,this.postal_code);
                    else
                        stmt.setInt(3,Integer.parseInt(this.postal_code));

                    stmt.setLong(4,membership_id);

                    stmt.executeUpdate();
                    conn.closeStatement(stmt);
                    stmt = null;

                    stmt = conn.prepareStatement(this.sql_update_members);
                    if(this.birthdate==null)
                        stmt.setObject(1,this.birthdate);
                    else
                        stmt.setString(1,this.birthdate);

                    stmt.setString(2,this.name);
                    stmt.setString(3,this.gender);
                    stmt.setString(4,this.member_email);
                    stmt.setString(5,this.province);
                    if(this.vocation==null)
                        stmt.setObject(6,this.vocation);
                    else
                        stmt.setString(6,this.vocation);

                    if(this.education==null)
                        stmt.setObject(7,this.education);
                    else
                        stmt.setString(7,this.education);

                    if(this.income==null)
                        stmt.setObject(8,this.income);
                    else
                        stmt.setString(8,this.income);

                    stmt.setString(9,this.personal_id);
                    
                    if(this.nickname==null)
                        stmt.setObject(10,this.nickname);
                    else
                        stmt.setString(10,this.nickname);

                    if(this.new_pin==null){
                        stmt.setLong(11,membership_id);
                        stmt.setLong(12,member_id);
                    }else{
                        stmt.setString(11,this.new_pin);
                        stmt.setLong(12,membership_id);
                        stmt.setLong(13,member_id);
                    }

                    stmt.executeUpdate();
                    conn.closeStatement(stmt);
                    stmt = null;
                    conn.commit();

                } catch (SQLException e) {
                    try {
                        conn.rollback();
                    } catch (SQLException ex) {}
                    this.log.error("Failed to update account in IQUE DB: " + e.getMessage());
                    throw new InvalidRequestException("Failed to update account in IQUE DB: " + e.getMessage());
                } finally {
                    if (conn != null) {
                        conn.closeStatement(stmt);
                        conn.close();
                    }
                }
            }

            if(!getIsError()){
                this.failureMsg = "You have successfully updated the account.";
                this.error_code = "101";
    		}

        }

        try {
            conn = this.context.getDB(true);
            stmt = conn.prepareStatement(SQL_QUERY_ACCOUNT);
            stmt.setLong(1,membership_id);
            stmt.setLong(2,member_id);
            rs = stmt.executeQuery();

            if (rs.next()) {
                this.resultArray = new Object[15];
                for (int i=0; i<this.resultArray.length; i++) {
                    this.resultArray[i]=rs.getObject(i+1);
                }
            }
            rs.close();
            conn.closeStatement(stmt);
            stmt = null;

        } catch (SQLException e) {
            this.log.error("Failed to query account in IQUE DB: " + e.getMessage());
            throw new InvalidRequestException("Failed to query account in IQUE DB: " + e.getMessage());
        } finally {
            if (conn != null) {
                conn.closeStatement(stmt);
            	conn.close();
            }
        }
    }

    public boolean getIsError() {return this.failureMsg != null;}
    public String getErrorMsg() {return this.failureMsg;}
    public String getErrorCode(){return this.error_code;}
    public Object[] getResultArray(){return this.resultArray;}

    // --------------------------------------------
    // Definition of result specific bean values used
    // for formatting a result or access through
    // JSP pages.
    // --------------------------------------------

} // class OscResultAcctUpdate


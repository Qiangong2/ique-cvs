package com.broadon.osc.java_util;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.broadon.exception.InvalidRequestException;


/** The class used to create the results associated with an account
 *  registration request, such that the appropriate document and header can be
 *  created in the response back to the client
 */
public class OscResultRegister extends OscResultBean
{
    /** Rules for creating a Login result object
     */
    public static class Rule extends OscResultRule {

        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
        }
        public boolean supports(ServerType t)
        {
            return t == ServerType.HTM;
        }
        public OscResultBean create(HttpServletRequest req,
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
            return new OscResultRegister(req, res, this);
        }
    } // class Rule

    // --------------------------------------------
    // Member variables
    // --------------------------------------------

    private final OscResultRegister.Rule    rule;
    private final OscContext                context;
    private final OscLogger                 log;
    private final HttpServletRequest 		request;

    private static final String REGISTER_SUCC_JSP      		   = "RegisterSucc.jsp";    
    private static final String REGISTER_CUSTOMER_BASE_JSP	   = "RegisterBase.jsp";
    private static final String REGISTER_CUSTOMER_INFO_JSP     = "RegisterInfo.jsp";
    private static final String REGISTER_ACCOUNT_EXISTING_JSP  = "RegisterAccountExisting.jsp";
    
    
    private static final String REDIRECT_HOME_ACTION   = "login_form";
    private static final String DEFAULT_LOCALE         = "zh_CN";

    private static final String SQL_QUERY_MEMBER_INFO =
        "SELECT mb.membership_id,mb.member_id,mb.name,mb.gender,to_char(" +
        "mb.birthdate,'yyyy/mm/dd') birthdate,mb.personal_id,ms.telephone," +
        "ms.address,ms.postal_code,mb.province,mb.vocation,mb.education,mb.income " +
        "FROM ique_memberships ms,ique_members mb WHERE mb.membership_id=" +
        "ms.membership_id AND mb.membership_id=? AND mb.member_id=?";
    
    private static final String SQL_UPDATE_MEMBERSHIPS_INFO =
        "UPDATE ique_memberships SET telephone=?,address=?,postal_code=? " +
        "WHERE membership_id=?";
    
    private static final String SQL_UPDATE_MEMBERS_INFO =
        "UPDATE ique_members SET name=?,gender=?,birthdate=to_date(?,'yyyy/mm/dd')," +
        "personal_id=?,province=?,vocation=?,education=?,income=? WHERE membership_id" +
        "=? AND member_id=?";
    
    private static final String SQL_QUERY_IS_MEMBER_EMAIL_EXISTING =
        "SELECT membership_id,member_id,pseudonym,status FROM ique_members WHERE member_email=?";
    
    private static final String SQL_UPDATE_PSEUDONYM_PASS_NICKNAME =
        "UPDATE ique_members SET pseudonym=?,pin=?,nick_name=? WHERE membership_id=? AND member_id=?";
    
    private static final String SQL_INSERT_MEMBERSHIPS =
        "INSERT INTO ique_memberships(membership_id,primary_member_id,status) VALUES(?,?,?)";
    
    private static final String SQL_INSERT_MEMBERS =
        "INSERT INTO ique_members(membership_id,member_id,member_email,status) VALUES(?,?,?,?)";
    
    private String  failureMsg;
    private String  error_code;
    private String  redirect_jsp;

    private String locale;
    private String telephone;
    private String member_email;
    private String pseudonym;
    private String nickname;
    private String name;
    private String gender;
    private String personal_id;
    private String postal_code;
    private String address;
    private String province;
    private String year;
    private String month;
    private String day;
    private String birthdate;
    private String vocation;
    private String education;
    private String income;

    private int  reg_phase;
    private long membership_id;
    private long member_id;
    
    private String 		reg_case    = null;
    private String[]	resultArray = null;
    
    
    // --------------------------------------------
    // Hidden constructors (all constructors should
    // be hidden for result objects, sicne they are
    // constructed by the Rule class)
    // --------------------------------------------

    private OscResultRegister(HttpServletRequest  req,
                              HttpServletResponse res,
                              OscResultRegister.Rule rule)
        throws InvalidRequestException
    {
        this.rule = rule;
        this.context = rule.getContext();
        this.log = this.context.getLogger();
        this.request = req;
        
        this.reg_phase = 0;
        if(this.request.getParameter("reg_phase")!=null && 
           this.request.getParameter("reg_phase").equals("1")){
            this.reg_phase = 1;
        }else if(this.request.getParameter("reg_phase")!=null && 
                 this.request.getParameter("reg_phase").equals("2")){
            this.reg_phase = 2;
        }else if(this.request.getParameter("reg_phase")!=null && 
                 this.request.getParameter("reg_phase").equals("3")){
            this.reg_phase = 3;
        }
         
        switch(this.reg_phase)
        {
            case 1:
                this.member_email = ( this.request.getParameter("member_email")!=null &&
                					 !this.request.getParameter("member_email").trim().equals(""))
                					 ?this.request.getParameter("member_email").trim().toLowerCase():null;
                if (this.member_email==null) {
                    this.failureMsg = "member_email is null";
                    this.error_code = "11";
                } else if (!OscContext.isValidEmailAddress(this.member_email)) {
                    this.failureMsg = "member_email is not valid.";
                    this.error_code = "31";
                }
                
                break;
                
            case 2:
                this.pseudonym = ( this.request.getParameter("pseudonym")!=null &&
   				             	  !this.request.getParameter("pseudonym").trim().equals("") )
   				              	  ?this.request.getParameter("pseudonym").trim():null;
   		        if (this.pseudonym==null) {
   		            this.failureMsg = "pseudonym is null";
   		            this.error_code = "1";
   		        } else if (!OscContext.isValidPseudonym(this.pseudonym)) {
   		            this.failureMsg = "pseudonym is not valid.";
   		            this.error_code = "21";
   		        } else
					try {
						if (this.context.utf8EncodingLength(this.pseudonym) > 32){
							this.failureMsg = "pseudonym's length is great than 32.";
							this.error_code = "79";
						}
					} catch (UnsupportedEncodingException e3) {
						throw new InvalidRequestException(e3.getMessage());
					}
				
				if(this.failureMsg == null){   		        
					this.nickname = ( this.request.getParameter("nickname") != null && 
   		        					! this.request.getParameter("nickname").trim().equals("")) 
   		        					? this.request.getParameter("nickname").trim() : null;
			        if (this.nickname == null) {
			        	this.failureMsg = "nickname is null";
			        	this.error_code = "68";				
			        } else if (!OscContext.isValidNickname(this.nickname.trim())) {
			        	this.failureMsg = "nickname is invalid";
			        	this.error_code = "69";
			        } else
			        	try {
			        		if (this.context.utf8EncodingLength(this.nickname) > 12 ){
			        			this.failureMsg = "nickname's length is great than 12.";
			        			this.error_code = "77";
			        		}
			        	} catch (UnsupportedEncodingException e3) {
			        		throw new InvalidRequestException(e3.getMessage());
			        	}
				}
				
   		        if(this.failureMsg==null){
   		            this.locale = ( this.request.getParameter("locale")!=null &&
   		                           !this.request.getParameter("locale").equals("") )
   		                           ?this.request.getParameter("locale"):DEFAULT_LOCALE;
   		        }
   		        this.member_email = ( this.request.getParameter("member_email")!=null &&
   		                  			 !this.request.getParameter("member_email").trim().equals(""))
   		                  			 ?this.request.getParameter("member_email").trim():null;
   		                	   
   		        this.membership_id=Long.parseLong(this.request.getParameter("membership_id"));
   		        this.member_id=Long.parseLong(this.request.getParameter("member_id"));
   		        this.reg_case=this.request.getParameter("reg_case");
   		        
                break;
                
            case 3:
                try{
                    this.name = ( this.request.getParameter("name")!=null &&
                                 !this.request.getParameter("name").trim().equals("") )
                                 ?this.request.getParameter("name").trim():null;
                    if (this.name==null) {
                        this.failureMsg = "name is null";
                        this.error_code = "3";
                        throw new Exception("");
                    } else if(!OscContext.isValidName(this.name.trim())){
                        this.failureMsg = "name is invalid";
                        this.error_code = "43";
                        throw new Exception("");
                    } else 
                    	try {
                    		if (this.context.utf8EncodingLength(this.name) > 12){
                    			this.failureMsg = "name's length is great than 12.";
                    			this.error_code = "81";
                    			throw new Exception("");
                    		}
                    	} catch (UnsupportedEncodingException e3) {
                    		throw new InvalidRequestException(e3.getMessage());
                    	}
                    
                    this.gender = this.request.getParameter("gender");                    
                    if (this.gender==null) {
                        this.failureMsg = "gender is null";
                        this.error_code = "44";
                        throw new Exception("");
                    } 

                    this.year = ( this.request.getParameter("year")!=null &&
                                 !this.request.getParameter("year").trim().equals("") )
                                 ?this.request.getParameter("year").trim():null;                    
                    
                    this.month = ( this.request.getParameter("month")!=null &&
                                  !this.request.getParameter("month").trim().equals("") )
                                  ?this.request.getParameter("month").trim():null;

                    this.day = ( this.request.getParameter("day")!=null &&
                                !this.request.getParameter("day").trim().equals("") )
                                ?this.request.getParameter("day").trim():null;
                            
                    if (!((this.year==null) && (this.month==null) && (this.day==null))){        
                        if (this.year==null) {
                            this.failureMsg = "year is null";
                            this.error_code = "4";
                            throw new Exception("");
                        } else if (!OscContext.isDigital(this.year)) {
                            this.failureMsg = "year is not valid digital.";
                            this.error_code = "23";
                            throw new Exception("");
                        } else if (this.year.length() != 4) {
                            this.failureMsg = "length of year is not four bits.";
                            this.error_code = "22";
                            throw new Exception("");
                        } else if (this.month==null) {
                            this.failureMsg = "month is null";
                            this.error_code = "6";
                            throw new Exception("");
                        } else if (!OscContext.isDigital(this.month)) {
                            this.failureMsg = "month is not valid digital";
                            this.error_code = "25";
                            throw new Exception("");
                        } else if (this.month.length() != 2) {
                            this.failureMsg = "length of month is not two bits.";
                            this.error_code = "24";
                            throw new Exception("");
                        } else if (Integer.parseInt(this.month) < 1 || Integer.parseInt(this.month) > 12) {
                            this.failureMsg = "month is out of range";
                            this.error_code = "7";
                            throw new Exception("");
                        } else if (this.day==null) {
                            this.failureMsg = "day is null";
                            this.error_code = "8";
                            throw new Exception("");
                        } else if (!OscContext.isDigital(this.day)) {
                            this.failureMsg = "day is not valid digital.";
                            this.error_code = "27";
                            throw new Exception("");
                        } else if (this.day.length() != 2) {
                            this.failureMsg = "length of day is not two bits.";
                            this.error_code = "26";
                            throw new Exception("");
                        } else if (Integer.parseInt(this.day) < 1 || Integer.parseInt(this.day) > 31) {
                            this.failureMsg = "day is out of range";
                            this.error_code = "9";
                            throw new Exception("");
                        } else if (!OscContext.isValidDate(Integer.parseInt(this.year),Integer.parseInt(this.month),Integer.parseInt(this.day))) {
                            this.failureMsg = "birthdate is not valid.";
                            this.error_code = "28";
                            throw new Exception("");
                        } else {
                            int i = -2, l = -2;
                            try {
                                i = this.context.parseDate(this.year+this.month+this.day+" 23:59:59","yyyyMMdd HH:mm:ss","").compareTo(this.context.getCurrentTime());
                                if (i >= 0) {
                                    this.failureMsg="The birthdate can not be equal now or in the future.";
                                    this.error_code="41";
                                    throw new Exception("");
                                } else if (i != -2) {
                                    l = this.context.parseDate(this.year+this.month+this.day+" 23:59:59","yyyyMMdd HH:mm:ss","").compareTo(this.context.parseDate("19000101 00:00:00","yyyyMMdd HH:mm:ss",""));
                                    if (l < 0) {
                                        this.failureMsg = "The birthdate can not be smaller than the year 1900.";
                                        this.error_code = "42";
                                        throw new Exception("");
                                    } else if (l == -2) {
                                        this.failureMsg="System error.";
                                        this.error_code="40";
                                        throw new Exception("");
                                    }
                                } else if (i == -2) {
                                    this.failureMsg = "System error.";
                                    this.error_code = "40";
                                    throw new Exception("");
                                }
                            } catch (ParseException e1) {
                                throw new InvalidRequestException("Failed to parse date: " + e1.getMessage());
                            } catch (Exception e2) {
                                throw e2;
                            }
                        }
                        this. birthdate = this.year+"/"+this.month+"/"+this.day;
                    }else{
                        this.birthdate = null;
                    }    

                    this.personal_id = ( this.request.getParameter("personal_id")!=null &&
                                        !this.request.getParameter("personal_id").trim().equals("") )
                                        ?this.request.getParameter("personal_id").trim():null;            
                    if (this.personal_id==null) {
                        this.failureMsg = "personal_id is null";
                        this.error_code = "10";
                        throw new Exception("");
                    }else if(this.context.utf8EncodingLength(this.personal_id) > 32){
                    	this.failureMsg = "personal_id is too long";
                        this.error_code = "29";
                        throw new Exception("");
                    }
                    
                    this.telephone = ( this.request.getParameter("telephone")!=null &&
                                  	  !this.request.getParameter("telephone").trim().equals("") )
                                      ?this.request.getParameter("telephone").trim():null;
                    if (this.telephone==null) {
                        this.failureMsg = "telephone is null";
                        this.error_code = "12";
                      	throw new Exception("");
                    } else if (!OscContext.isValidTelephone(this.telephone)) {
                        this.failureMsg = "telephone is not valid.";
                        this.error_code = "32";
                        throw new Exception("");
                    }
                    
                    this.province = ( this.request.getParameter("province")!=null &&
                                     !this.request.getParameter("province").trim().equals("") )
                                     ?this.request.getParameter("province").trim():null;
                    if (this.province==null) {
                        this.failureMsg = "province is null";
                        this.error_code = "13";
                        throw new Exception("");
                    }

                    this.postal_code = ( this.request.getParameter("postal_code")!=null &&
                                        !this.request.getParameter("postal_code").trim().equals("") )
                                        ?this.request.getParameter("postal_code"):null;
                    if (this.postal_code!=null){
                        if(!OscContext.isDigital(this.postal_code)) {
                            this.failureMsg = "postal code is not valid number.";
                            this.error_code = "39";
                            throw new Exception("");
                        }else if(this.postal_code.length() != 6){
                            this.failureMsg = "length of postal code is not 6 bits.";
                            this.error_code = "38";
                        	throw new Exception("");
                        }
                    }
                    this.address = ( this.request.getParameter("address")!=null &&
                                    !this.request.getParameter("address").trim().equals("") )
                                    ?this.request.getParameter("address").trim():null;

                    this.income = ( this.request.getParameter("income")!=null &&
                                   !this.request.getParameter("income").trim().equals("") )
                                   ?this.request.getParameter("income").trim():null;

                    this.vocation = ( this.request.getParameter("vocation")!=null &&
                                     !this.request.getParameter("vocation").trim().equals("") )
                                     ?this.request.getParameter("vocation").trim():null;

                    this.education = ( this.request.getParameter("education")!=null &&
                                      !this.request.getParameter("education").trim().equals("") )
                                      ?this.request.getParameter("education").trim():null;
                }catch(Exception e){}
                
                this.reg_case=this.request.getParameter("reg_case");
                this.membership_id=Long.parseLong(this.request.getParameter("membership_id"));
   		        this.member_id=Long.parseLong(this.request.getParameter("member_id"));
   		        
                break;
                
            default:
                this.request.setAttribute("reg_phase","1");
                this.redirect_jsp = REGISTER_CUSTOMER_BASE_JSP;
                
                break;
        }
    }

    // --------------------------------------------
    // Specialization of abstract methods
    // --------------------------------------------

    public boolean requiresSecureRequest() {return true;}
    public boolean requiresLogin() {return false;}

    public String jspFwdUrl()
    {
        return this.redirect_jsp;
    }

    public String redirectAction()
    {
        return REDIRECT_HOME_ACTION;
    }

    public void encodeXml(PrintWriter out) {}

    public void serveRequest(OscLoginSession loginSession)
        throws InvalidRequestException
    {
        if(this.reg_phase == 1){
            if(this.failureMsg==null){
                OscDbConnection   conn = null;
                PreparedStatement stmt = null;
                ResultSet           rs = null;
               
                boolean        isFound = false;
                String		 pseudonym = null;
                String	        status = null;
                
                try {
                    conn = this.context.getDB(true);
                    stmt = conn.prepareStatement(SQL_QUERY_IS_MEMBER_EMAIL_EXISTING);
                    stmt.setString(1,this.member_email);
                    rs = stmt.executeQuery();
                    if (rs.next()) {
                        isFound = true;
                        this.membership_id = rs.getLong("membership_id");
                        this.member_id = rs.getLong("member_id");
                        pseudonym = rs.getString("pseudonym");
                        status = rs.getString("status");
                    }
                    rs.close();
                    conn.closeStatement(stmt);
                    stmt=null;
                    
                }catch (SQLException e) {
                    this.log.error("Failed to query the existing of " +
                    		"member_email in IQUE DB: " + e.getMessage());
                    throw new InvalidRequestException("Failed to query " +
                    		"the existing of member_email in IQUE DB: " + e.getMessage());
                }finally{
                    if (conn != null) {
                        conn.closeStatement(stmt);
                        conn.close();
                    }
                }               
                
                if(!isFound){                        
                    this.membership_id = OscContext.getNextMembershipIdSeq(this.context,this.log);
                    this.member_id = 1;                   
                    
                    try{
                        conn = this.context.getDB(false);
                        stmt = conn.prepareStatement(SQL_INSERT_MEMBERSHIPS);
                        stmt.setLong(1,this.membership_id);
                        stmt.setLong(2,this.member_id);
                        stmt.setString(3,"N");
                        
                        stmt.executeUpdate();
                        conn.closeStatement(stmt);
                        stmt = null;
                        
                        stmt = conn.prepareStatement(SQL_INSERT_MEMBERS);
                        stmt.setLong(1,this.membership_id);
                        stmt.setLong(2,this.member_id);
                        stmt.setString(3,this.member_email);
                        stmt.setString(4,"N1");
                        
                        stmt.executeUpdate();
                        conn.closeStatement(stmt);
                        stmt = null;                        
                        
                        conn.commit();                       
                                        
                    }
                    catch (SQLException e) {
                        try {
                            conn.rollback();
                        } catch (SQLException ex) {}
                        this.log.error("Failed to insert a new acount which only " +
                        		"includs 'member_email' in IQUE DB: " + e.getMessage());
                        throw new InvalidRequestException("Failed to insert a new acount" +
                        		" which only includs 'member_email' in IQUE DB: " + e.getMessage());
                    }                	
                    finally{
                        if (conn != null) {
                            conn.closeStatement(stmt);
                            conn.close();
                        }
                    }
                    this.request.setAttribute("reg_case","A");
                    this.request.setAttribute("reg_phase","2");
                    this.redirect_jsp = REGISTER_CUSTOMER_BASE_JSP;
                    
                }else{
                    if(pseudonym!=null && !pseudonym.equals("")){
                        if(status!=null && status.equals("A1")){                                
                            this.redirect_jsp = REGISTER_ACCOUNT_EXISTING_JSP;
                        }else{
                            this.request.setAttribute("reg_case","B");
                            this.request.setAttribute("reg_phase","2");
                            this.redirect_jsp = REGISTER_CUSTOMER_BASE_JSP;
                        }
                    }else{
                        this.request.setAttribute("reg_case","C");
                        this.request.setAttribute("reg_phase","2");
                        this.redirect_jsp = REGISTER_CUSTOMER_BASE_JSP;
                    }
                }
                this.request.setAttribute("member_email",this.member_email);
                this.request.setAttribute("membership_id",new Long(this.membership_id));
                this.request.setAttribute("member_id",new Long(this.member_id));
                
            }else{ 
                this.request.setAttribute("reg_phase","1");
                this.redirect_jsp = REGISTER_CUSTOMER_BASE_JSP;
            }
            
        }else if(this.reg_phase == 2){
        	
        	this.request.setAttribute("reg_case",this.reg_case);
            this.request.setAttribute("membership_id",Long.toString(this.membership_id));
            this.request.setAttribute("member_id",Long.toString(this.member_id));
            
            if(OscContext.isPseudonymExisting(this.context, this.log, this.pseudonym))
            {
                this.error_code = "20";
                this.failureMsg = "This login name has been registered.";
            }else if(OscContext.isNicknameExisting(this.context, this.log, this.nickname, this.membership_id, this.member_id)){
            	this.error_code = "76";
                this.failureMsg = "This nickname has been registered.";            	
            }
            
            if(this.failureMsg == null)
            {                
                String pin_plain = OscContext.genPassword(6);
				String pin_encry = null;
				
				try {
					pin_encry = OscContext.getEncryptedPasswd(pin_plain);
				} catch (NoSuchAlgorithmException e) {
					this.log.error("NoSuchAlgorithmException while "
							+ "encrypting password: " + e.getMessage());
					throw new InvalidRequestException("NoSuchAlgorith"
							+ "mException while encrypting password: "
							+ e.getMessage());
				}

				OscDbConnection conn = null;
				PreparedStatement stmt = null;

				try {
					conn = this.context.getDB(false);
					stmt = conn.prepareStatement(SQL_UPDATE_PSEUDONYM_PASS_NICKNAME);
					stmt.setString(1, this.pseudonym);
					stmt.setString(2, pin_encry);
					stmt.setString(3, this.nickname);
					stmt.setLong(4, this.membership_id);
					stmt.setLong(5, this.member_id);

					stmt.executeUpdate();
					conn.closeStatement(stmt);
					stmt = null;

					OscSendMail mail = new OscSendMail(this.context);
					String date = this.context.formatDate(this.context
							.getCurrentTime(), "yyyy-MM-dd HH:mm:ss",
							"GMT+8:00");
					String contents = this.context.readFile(this.context
							.getTxtPath(this.locale, "RegMailTemplete.txt"));

					if (contents == null) {
						this.error_code = "33";
						this.failureMsg = "Mail template not found.";
					} else {
						String subject = contents.substring(1, contents
								.indexOf("<&&>"));
						String confirm_url = "https://"
								+ this.request.getServerName()
								+ ":"
								+ Integer
										.toString(this.request.getServerPort())
								+ "/osc/secure/htm?OscAction=register_confirm"
								+ "&ms_id=" + this.membership_id + "&mb_id="
								+ this.member_id + "&pin=" + pin_encry;
						
						contents = contents
								.substring(contents.indexOf("<&&>") + 5);
						contents = contents
								.replaceAll("%USERNAME%", this.pseudonym);
						contents = contents.replaceAll("%PASSWORD%", pin_plain);
						contents = contents.replaceAll("%HYPERLINK%", confirm_url);
						contents = contents.replaceAll("%DATE%", date);

						mail.setSubject(subject);
						mail.setTo(this.member_email);
						mail.setBody(contents);
						if (!mail.sendOut()) {
							this.error_code = "70";
							this.failureMsg = "Mail sended unsuccessfully.";
							throw new Exception(
									"When mail.sendOut() was excuted, error occured.");
						}
					}

					conn.commit();
					
				} catch (SQLException e) {
					try {
						conn.rollback();
					} catch (SQLException ex) {
					}
					this.log.error("Failed to query or update in IQUE DB: "
							+ e.getMessage());
					throw new InvalidRequestException(
							"Failed to query or update in IQUE DB: "
									+ e.getMessage());
				} catch (IOException e) {
					try {
						conn.rollback();
					} catch (SQLException ex) {
					}
					this.log.error("Failed to read mail template: "
							+ e.getMessage());
					throw new InvalidRequestException(
							"Failed to read mail template: " + e.getMessage());
				} catch (Exception e) {
					try {
						conn.rollback();
					} catch (SQLException ex) {
					}
					this.log.error("Failed to send mail: " + e.getMessage());
					throw new InvalidRequestException(
							"Failed to send register " + "info mail:"
									+ e.getMessage());
				} finally {
					if (conn != null) {
						conn.closeStatement(stmt);
						conn.close();
					}
				}

				if (this.reg_case.equals("C")) {
					getMemberInfo(this.membership_id, this.member_id);
				}
				
				this.request.setAttribute("reg_phase", "3");
				this.redirect_jsp = REGISTER_CUSTOMER_INFO_JSP; 
                                               
            }else{                
                this.request.setAttribute("reg_phase","2");
                this.request.setAttribute("member_email",this.member_email);
                this.redirect_jsp=REGISTER_CUSTOMER_BASE_JSP;
            }
            
        }else if(this.reg_phase == 3){
            
            if(this.failureMsg == null){
                OscDbConnection   conn = null;
                PreparedStatement stmt = null;
                try{
                    conn = this.context.getDB(false);
                    stmt = conn.prepareStatement(SQL_UPDATE_MEMBERSHIPS_INFO);
                    
                    stmt.setString(1, this.telephone);
                    stmt.setString(2,this.address);                    
                    if (this.postal_code == null)
                        stmt.setString(3,this.postal_code);
                    else
                        stmt.setInt(3,Integer.parseInt(this.postal_code));
                    stmt.setLong(4,this.membership_id);
                    
                    stmt.executeUpdate();
                    conn.closeStatement(stmt);
                    stmt = null;
                    
                    stmt = conn.prepareStatement(SQL_UPDATE_MEMBERS_INFO);
                    
                    stmt.setString(1,this.name);
                    stmt.setString(2,this.gender);
                    stmt.setString(3,this.birthdate);
                    stmt.setString(4,this.personal_id);
                    stmt.setString(5,this.province);
                    stmt.setString(6,this.vocation);
                    stmt.setString(7,this.education);
                    stmt.setString(8,this.income);
                    stmt.setLong(9,this.membership_id);
                    stmt.setLong(10,this.member_id);

                    stmt.executeUpdate();
                    conn.closeStatement(stmt);
                    stmt = null;
                    
                    conn.commit();
                    
                }catch (SQLException e) {
                    try {
                        conn.rollback();
                    } catch (SQLException ex) {}
                    this.log.error("Failed to query or update in IQUE DB: " + e.getMessage());
                    throw new InvalidRequestException("Failed to query or update in IQUE DB: " + e.getMessage());
                }
                finally{
                    if (conn != null) {
                        conn.closeStatement(stmt);
                        conn.close();
                    }
                }
                
                this.redirect_jsp=REGISTER_SUCC_JSP;
                
            }else{
                if(this.reg_case.equals("C")){
                	getMemberInfo(this.membership_id,this.member_id);              
                } 
                this.request.setAttribute("reg_phase","3");
                this.request.setAttribute("membership_id",new Long(this.membership_id));
                this.request.setAttribute("member_id",new Long(this.member_id));
                this.redirect_jsp=REGISTER_CUSTOMER_INFO_JSP;
            }
        }
    	
    }
    
    

    private void getMemberInfo(long ms_id,long mb_id)
    	throws InvalidRequestException
    {
        OscDbConnection   conn = null;
        PreparedStatement stmt = null;
        ResultSet           rs = null;
        
        try{
            conn = this.context.getDB(true);
            stmt = conn.prepareStatement(SQL_QUERY_MEMBER_INFO);
            stmt.setLong(1,ms_id);
            stmt.setLong(2,mb_id);
            
            rs = stmt.executeQuery();
            if (rs.next()) {
                this.resultArray = new String[13];
                for(int i=0;i<this.resultArray.length;i++){
                    this.resultArray[i]=rs.getString(i+1);
                }                        
            }
            rs.close();
            conn.closeStatement(stmt);
            stmt=null; 
        }catch(SQLException e){
            this.log.error("Failed to query member info in IQUE DB: " + e.getMessage());
            throw new InvalidRequestException("Failed to query member info in IQUE DB: " + e.getMessage());
        }finally{
            if (conn != null) {
                conn.closeStatement(stmt);
                conn.close();
            }
        }
        
    }
    
    public boolean getIsError() {return this.failureMsg != null;}
    public String getErrorMsg() {return this.failureMsg;}
    public String getErrorCode(){return this.error_code;}
    public String[] getResultArray(){return this.resultArray;}

    // --------------------------------------------
    // Definition of result specific bean values used
    // for formatting a result or access through
    // JSP pages.
    // --------------------------------------------

} // class OscResultRegister


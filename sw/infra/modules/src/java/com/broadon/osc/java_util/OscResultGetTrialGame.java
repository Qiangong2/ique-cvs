package com.broadon.osc.java_util;

import java.io.PrintWriter;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpSession;

import com.broadon.exception.InvalidRequestException;


/** The class used to validate the code, such that
 *  the appropriate document and header can be
 *  created in the response back to the client
 */
public class OscResultGetTrialGame extends OscResultBean
{
    /** Rules for creating a GetTrialGame result object
     */
    public static class Rule extends OscResultRule {

        public static final String GET_TRIAL = "get_trial";

        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
        }

        public boolean supports(ServerType t)
        {
            return t == ServerType.HTM;
        }

        public OscResultBean create(HttpServletRequest req,  
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
            return new OscResultGetTrialGame(req, res, this);
        }
    } // class Rule


    // --------------------------------------------
    // Invariant values
    // --------------------------------------------

    private static final String GET_TRIAL_JSP = "iQueGetTrial.jsp";
    private static final String STEP_ID = "step_id";
    private static final String CARD_PASSWD = "card_passwd";
    private static final String BB_ID = "bb_id";
    private static final String TITLE_ID = "title_id";
    private static final String TITLE_NAME = "title_name";
    private static final String TITLE_CONTENT = "title_content";
    private static final String TRIAL_BLOCKS = "trial_blocks";
    private static final String VALIDATE_CODE = "validate_code";
    private static final String SELECT_IQUE_CARD = 
                "SELECT card_id,bb_id,activate_date FROM iQue_bb_cards WHERE card_password=?";

    private static final String UPDATE_IQUE_CARD = 
                "update iQue_bb_cards set bb_id=?, activate_date=sysdate where card_id=?";

    private static final String INSERT_IQUE_CARD_TITLES = 
                "INSERT INTO iQue_bb_card_titles(card_id,title_id,ip_address) values(?,?,?)";

    // --------------------------------------------
    // Member variables
    // --------------------------------------------

    private final OscResultGetTrialGame.Rule   rule;
    private final OscContext                   context;
    private final OscLogger          	       logger;
    private final String                       actionName;
    private final HttpSession                  session;
    private String                             titleID;
    private String			       titleName;
    private String			       stepID;
    private String                             sessionID;
    private String                             cardPasswd;
    private String			       bbID;
    private String			       titleContent;
    private String			       trialBlocks;
    private String			       validateCode;
    private OscCookie                          cookie;
    private HttpServletRequest		       httpReq;
    private int				       cardID;
    private String			       ipAddress;
    // --------------------------------------------
    // Hidden constructors (all constructors should
    // be hidden for result objects, sicne they are
    // constructed by the Rule class)
    // --------------------------------------------

    private OscResultGetTrialGame(HttpServletRequest  req,  
                                   HttpServletResponse res,
                                   OscResultGetTrialGame.Rule rule)
        throws InvalidRequestException
    {
        this.rule = rule;
        context = rule.getContext();
        logger = context.getLogger();
        actionName = rule.getActionNm();

        if (!((req.getParameter(TITLE_ID)==null)||(req.getParameter(TITLE_ID).trim().equals("")))) {
            titleID = req.getParameter(TITLE_ID).trim();
        }else{
            titleID = "";
        }

        if (!((req.getParameter(TITLE_NAME)==null)||(req.getParameter(TITLE_NAME).trim().equals("")))) {
            titleName=req.getParameter(TITLE_NAME).trim();
        }else{
            titleName="";
        }

        if (!((req.getParameter(STEP_ID)==null)||(req.getParameter(STEP_ID).trim().equals("")))) {
            stepID=req.getParameter(STEP_ID).trim();
        }else{
            stepID="1";
        }

        if (!((req.getParameter(CARD_PASSWD)==null)||(req.getParameter(CARD_PASSWD).trim().equals("")))) {
            cardPasswd=req.getParameter(CARD_PASSWD).trim();
        }else{
            cardPasswd="";
        }

        if (!((req.getParameter(BB_ID)==null)||(req.getParameter(BB_ID).trim().equals("")))) {
            bbID=req.getParameter(BB_ID).trim();
        }else{
            bbID="";
        }

        if (!((req.getParameter(TITLE_CONTENT)==null)||(req.getParameter(TITLE_CONTENT).trim().equals("")))) {
            titleContent=req.getParameter(TITLE_CONTENT).trim();
        }else{
            titleContent="";
        }

        if (!((req.getParameter(TRIAL_BLOCKS)==null)||(req.getParameter(TRIAL_BLOCKS).trim().equals("")))) {
            trialBlocks=req.getParameter(TRIAL_BLOCKS).trim();
        }else{
            trialBlocks="";
        }

        if (!((req.getParameter(VALIDATE_CODE)==null)||(req.getParameter(VALIDATE_CODE).trim().equals("")))) {
            validateCode=req.getParameter(VALIDATE_CODE).trim();
        }else{
            validateCode="";
        }
        httpReq = req;
        cardID = 0;
        session = req.getSession(true);
        ipAddress = req.getRemoteAddr();
    }


    // --------------------------------------------
    // Specialization of abstract methods
    // --------------------------------------------

    public boolean requiresLogin() 
    {
         return false; 
    }

    public String jspFwdUrl() 
    {
        return GET_TRIAL_JSP;
    }

    public void encodeXml(PrintWriter out) {};

    public void serveRequest(OscLoginSession loginSession)
        throws InvalidRequestException
    {
        if (stepID.equals("1")) {
            sessionID = session.getId();
            session.setAttribute("STEP_ID",stepID);
        } else {
            sessionID = session.getId();
            if (stepID.equals("2")) {
                if (session.getAttribute("STEP_ID").equals("1")&&(!(bbID.equals("")))&&(!(titleID.equals("")))){
                    cookie = loginSession.getLoginCookie(context, httpReq);
                    try {
                        if (cookie.isAuthCode(validateCode)) {
                            OscDbConnection conn = null;
                            PreparedStatement stmt = null;
                            ResultSet rs = null;
                            try {
                                conn = context.getDB(false);
                                stmt = conn.prepareStatement(SELECT_IQUE_CARD);
                                stmt.setString(1, cardPasswd);
                                rs = stmt.executeQuery();
                                if (rs.next()) {
                                    if (rs.getString(2)==null) {
                                        cardID = rs.getInt(1);
                                        rs.close();
                                        conn.closeStatement(stmt);
                                        stmt = conn.prepareStatement(UPDATE_IQUE_CARD);
                                        stmt.setInt(1,Integer.parseInt(bbID));
                                        stmt.setInt(2,cardID);
                                        stmt.executeUpdate();
                                        conn.closeStatement(stmt);
                                        stmt = conn.prepareStatement(INSERT_IQUE_CARD_TITLES);
                                        stmt.setInt(1,cardID);
                                        stmt.setInt(2,Integer.parseInt(titleID));
                                        stmt.setString(3,ipAddress);
                                        stmt.executeUpdate();
                                    } else {
                                        if (rs.getString(2).equals(bbID)) {
                                            cardID = rs.getInt(1);
                                            rs.close();
                                            conn.closeStatement(stmt);
                                            stmt = conn.prepareStatement(INSERT_IQUE_CARD_TITLES);
                                            stmt.setInt(1,cardID);
                                            stmt.setInt(2,Integer.parseInt(titleID));
                                            stmt.setString(3,ipAddress);
                                            stmt.executeUpdate();
                                        } else {
                                            rs.close();
                                            stepID = "6";
                                        }                            
                                    }
                                } else {
                                   rs.close();
                                   stepID = "4";
                                }
                                conn.closeStatement(stmt);
                                conn.commit();
                            }
                            catch (SQLException e) {
                                try {
                                    conn.rollback();
                                }
                                catch (SQLException ex){
                                    logger.error("Failed to rollback. " + ex.getMessage());
                                }
                                logger.error("Failed to query/update database. " + e.getMessage());
                                throw new InvalidRequestException("Failed to query/update database.");
                            }
                            catch (Exception e) {
                                logger.error("Failed to download trial game. " + e.getMessage());
                                throw new InvalidRequestException("Failed to download trial game.");
                            }
                            finally {
                                if (conn != null)
                                    conn.close();
                            }
                        } else {
                            stepID = "5";
                        }
                    }
                    catch (OscCookie.InvalidCookieException e) {
                        stepID = "5";
                        logger.error("Login failed; cannot decode auth-code " +  e.getMessage());
                    }
                }else{
                    stepID = "3";
                }
            } else {
               stepID = "3";
            }
            session.setAttribute("STEP_ID","0");
        }
    }

    public String toString() {return null;}

    // --------------------------------------------
    // Definition of result specific bean values used
    // for formatting a result or access through
    // JSP pages.
    // --------------------------------------------

    public String transformXML(String xmlStr, String xslFile, String locale)
    { return context.transformXML(xmlStr,xslFile,locale); }

    public String escQuote(String var) { return context.escQuote(var);}
    public String getTitleID() {return titleID;}
    public String getTitleName() {return titleName;}
    public String getTitleContent() {return titleContent;}
    public String getTrialBlocks() {return trialBlocks;}
    public String getStepID() {return stepID;}

} // class OscResultGetTrialGame

package com.broadon.osc.java_util;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.log4j.Logger;


/** <code>OscLogger</code> is a thin layer around the log4j logger, such
 *  that we have a convenient way to customize logging beyond what is in
 *  log4j should we need to do so in the future.
 *
 *  TODO: Support Fmt (formatted) logging through the use of a message
 *  catalog (that can be internationalized and read from file), where
 *  the formatting is just descriptive placeholders for parameters (e.g.
 *  in XML).  Call toString() to insert parameter strings into the
 *  logging msg.
 */
public final class OscLogger
{
    private static final String ERROR_IN_EXCEPTION =
        "!!!!Failed to log exception!!!!"; 


    private Logger  logger;
    private boolean tracing;


    private String getExceptionLog(java.lang.Throwable e)
    {
        try {
            final StringWriter sw = new StringWriter(2048);
            final PrintWriter  pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            pw.flush();

            String s = e.getMessage() + "\n" + sw.toString();
            pw.close();
            sw.close();
            return s;
        }
        catch (IOException ie) {return ERROR_IN_EXCEPTION;}
    }

    //-------------------------------------------------------
    //-----------------  Public Interface -------------------
    //-------------------------------------------------------

    public OscLogger(Logger l)
    {
        logger = l;
        tracing = false;
    }

    // To minimize overheads for tracing we should check for tracing
    // before constructing trace strings.  Tracing should typically
    // be printed as info.
    //
    public void setTracing(boolean on) {tracing = on;}
    public boolean isTracing() {return tracing;}
    
    public void debug(java.lang.Throwable e) 
    {
        logger.debug(getExceptionLog(e));
    }
    public void info(java.lang.Throwable e) 
    {
        logger.info(getExceptionLog(e));
    }
    public void warn(java.lang.Throwable e) 
    {
        logger.warn(getExceptionLog(e));
    }
    public void error(java.lang.Throwable e) 
    {
        logger.error(getExceptionLog(e));
    }
    public void fatal(java.lang.Throwable e) 
    {
        logger.fatal(getExceptionLog(e));
    }
    public void debug(String msg) {logger.debug(msg);}
    public void info(String msg) {logger.info(msg);}
    public void warn(String msg) {logger.warn(msg);}
    public void error(String msg) {logger.error(msg);}
    public void fatal(String msg) {logger.fatal(msg);}
} // class OscLogger

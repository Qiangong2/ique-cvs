package com.broadon.osc.java_util;

import java.io.PrintWriter;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.broadon.exception.InvalidRequestException;


public class OscResultMemberPointQuery extends OscResultBean {

    /** Rules for creating a Login result object
     */
    public static class Rule extends OscResultRule {

        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
        }
        public boolean supports(ServerType t)
        {
            return t == ServerType.HTM;
        }
        public OscResultBean create(HttpServletRequest req,
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
            return new OscResultMemberPointQuery(req, res, this);
        }
    } // class Rule

    private static final String REDIRECT_HOME_ACTION   = "login_form";
    private static final String DEFAULT_LOCALE         = "zh_CN";

    private static final String MEMBER_POINT_JSP = "MemberPointQuery.jsp";

    private static final String SQL_QUERY_REDEEMABLE_POINTS =
        "{? = call IQUE_MEMBER_POINT_PKG.GET_REDEEMABLE_POINTS(?, ?)}";


    private static final String SQL_QUERY_TRANSFERABLE_POINTS =
        "{? = call IQUE_MEMBER_POINT_PKG.GET_TRANSFERABLE_POINTS(?, ?)}";

    private static final String	SQL_QUERY_MEMBER_POINTS_HISTORY =
        "SELECT * FROM (SELECT A.*,rownum seq FROM ( SELECT to_char(trans_date+8/24," +
        "'YYYY-MM-DD HH24:MI:SS') trans_date,to_char(expire_date+8/24,'YYYY-MM-DD " +
        "HH24:MI:SS') expire_date,type_desc,points,balance,reason, point_type,CASE " +
        "WHEN expire_date>SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)OR expire_date is null " +
        "THEN '0' ELSE '1' END is_expired FROM ique_member_point_trans_v WHERE " +
        "membership_id=? AND member_id=? ORDER BY trans_date DESC) A) WHERE seq>=? AND seq<=?";

    private static final String	SQL_QUERY_MEMBER_POINTS_HISTORY_COUNT =
        "SELECT count(trans_date) FROM ique_member_point_trans_v WHERE membership_id=? AND " +
        "member_id=?";


    private final OscResultMemberPointQuery.Rule    	rule;
    private final OscContext                			context;
    private final OscLogger                 			log;
    private final HttpServletRequest					request;

    private String  failureMsg;
    private String  error_code;
    private String  redirect_jsp;

    private int	page_seq;
    private int page_qty;
    private int record_qty;

    private static final int RECORDS_PER_PAGE = 5;


    private OscResultMemberPointQuery(HttpServletRequest  req,
            					 HttpServletResponse res,
            					 OscResultMemberPointQuery.Rule rule)
    	throws InvalidRequestException
    {
        this.rule = rule;
        this.context = rule.getContext();
        this.log = this.context.getLogger();
        this.request = req;
        this.redirect_jsp = MEMBER_POINT_JSP;

        this.page_seq = this.request.getParameter("page_seq") != null ?
                Integer.parseInt(this.request.getParameter("page_seq")):0;

    }

// --------------------------------------------
// Specialization of abstract methods
// --------------------------------------------

    public boolean requiresSecureRequest() {return false;}
    public boolean requiresLogin() {return true;}

    public String jspFwdUrl()
    {
        return this.redirect_jsp;
    }

    public String redirectAction()
    {
        return REDIRECT_HOME_ACTION;
    }

    public void encodeXml(PrintWriter out) {}

    public void serveRequest(OscLoginSession loginSession)
    	throws InvalidRequestException
    {
        long membership_id = loginSession.getMembershipID();
        long member_id = loginSession.getMemberID();
        String pseudonym = loginSession.getPseudonym();

        OscDbConnection    conn = null;
        PreparedStatement  stmt = null;
        CallableStatement cstmt = null;
        ResultSet           rs = null;


        long redeemable_points		= 0;
        long transferable_points 	= 0;

        try {
            conn = this.context.getDB(true);
            cstmt = conn.prepareCall(SQL_QUERY_REDEEMABLE_POINTS);
            cstmt.registerOutParameter(1,Types.BIGINT);
            cstmt.setLong(2,membership_id);
            cstmt.setLong(3,member_id);
            cstmt.execute();

            redeemable_points = cstmt.getLong(1);

            conn.closeStatement(cstmt);
            cstmt=null;

            cstmt = conn.prepareCall(SQL_QUERY_TRANSFERABLE_POINTS);
            cstmt.registerOutParameter(1,Types.BIGINT);
            cstmt.setLong(2,membership_id);
            cstmt.setLong(3,member_id);
            cstmt.execute();

            transferable_points = cstmt.getLong(1);

            conn.closeStatement(cstmt);
            cstmt=null;

            stmt = conn.prepareStatement(SQL_QUERY_MEMBER_POINTS_HISTORY_COUNT);
            stmt.setLong(1,membership_id);
            stmt.setLong(2,member_id);
            rs = stmt.executeQuery();
            if(rs.next()){
                this.record_qty = rs.getInt(1);
            }

            rs.close();
            conn.closeStatement(stmt);
            stmt=null;
        }
        catch (SQLException e) {
            this.log.error("Failed to query redeemable and transferable points: " + e.getMessage());
            throw new InvalidRequestException("Failed to query redeemable and transferable points: "
                    + e.getMessage());
        }
        finally{
            if (conn != null) {
                conn.closeStatement(stmt);
                conn.close();
            }
        }

        if(this.record_qty > 0){
            int start_row_num = 0;
            int end_row_num   = 0;
            int firstIndex = 0;
            int secondIndex = 8;

            this.page_qty = this.record_qty%RECORDS_PER_PAGE != 0 ?
                    this.record_qty/RECORDS_PER_PAGE+1 : this.record_qty/RECORDS_PER_PAGE;
            if(this.page_seq < this.page_qty-1){
                start_row_num = this.page_seq*RECORDS_PER_PAGE+1;
                end_row_num = (this.page_seq+1)*RECORDS_PER_PAGE;
                firstIndex = RECORDS_PER_PAGE;
            }else if(this.page_seq == this.page_qty-1){
                start_row_num = this.page_seq*RECORDS_PER_PAGE+1;
                end_row_num = this.record_qty;
                firstIndex = end_row_num - start_row_num + 1;
        	}

            String[][] point_history = new String[firstIndex][secondIndex];
            try{
                conn = this.context.getDB(true);
                stmt = conn.prepareStatement(SQL_QUERY_MEMBER_POINTS_HISTORY);
                stmt.setLong(1,membership_id);
                stmt.setLong(2,member_id);
                stmt.setInt(3,start_row_num);
                stmt.setInt(4,end_row_num);

                rs = stmt.executeQuery();
                int i=0;
                while(rs.next()){
                    for(int j=0;j<secondIndex;j++){
                        point_history[i][j]=rs.getString(j+1);
                    }
                    i++;
                }
                rs.close();
                conn.closeStatement(stmt);
                stmt = null;

            }catch(SQLException e){
                this.log.error("Failed to query member points history: " + e.getMessage());
                throw new InvalidRequestException("Failed to query member points history: "
                        + e.getMessage());
            }finally{
                if (conn != null) {
                    conn.closeStatement(stmt);
                    conn.close();
                }
            }
            this.request.setAttribute("point_history",point_history);
        }
        this.request.setAttribute("redeemable_points",Long.toString(redeemable_points));
        this.request.setAttribute("transferable_points",Long.toString(transferable_points));
        this.request.setAttribute("pseudonym",pseudonym);
        this.request.setAttribute("record_qty",Integer.toString(this.record_qty));
        this.request.setAttribute("page_qty",Integer.toString(this.page_qty));


    }


    public boolean getIsError() {return this.failureMsg != null;}
    public String getErrorMsg() {return this.failureMsg;}
    public String getErrorCode(){return this.error_code;}


}

package com.broadon.osc.java_util;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class OscSendMail {

    private static final boolean DEFAULT_REQUEST_AUTH = true;
    private static final String DEFAULT_FROM_ADDRESS  = "Service@iQue.com";
    
    private static final String HTML_HEAD = 
        "<html><head><meta http-equiv=\"Content-Type\" " +
        "content=\"text/html; charset=gb2312\"></head><body>";
    
    private static final String HTML_TAIL = "</body></html>";
    
    private static final String CONTENT_TYPE = "text/html; charset=gb2312";
    
    private final OscContext	context;
    private final OscLogger		log;
    
    private MimeMessage       mimeMsg;
    private Session           session;
    private Properties        props;
    private Multipart         mp;

    public OscSendMail(OscContext c)
    {
	    this.context = c;
	    this.log = c.getLogger();
	    
        this.props = System.getProperties();
	    this.props.put("mail.smtp.host", this.context.getSmtpServer());
	    this.props.put("mail.smtp.username",this.context.getSmtpUser());
	    this.props.put("mail.smtp.password",this.context.getSmtpPassword());
	    this.props.put("mail.smtp.auth",String.valueOf(DEFAULT_REQUEST_AUTH));
	    
	    this.session = Session.getDefaultInstance(this.props,null);
	    this.mimeMsg = new MimeMessage(this.session);
	    this.mp = new MimeMultipart();
    }
    
    public void setNeedAuth(boolean need) {
	    this.props.put("mail.smtp.auth", String.valueOf(need));
    }

    public void setNamePass(String name,String pass) {
        this.props.put("username",name);
	    this.props.put("password",pass);
    }

    public void setSubject(String mailSubject)
        throws Exception
    {
	    try{
	        this.mimeMsg.setSubject(mailSubject);
	    }catch(MessagingException e){
	        throw new Exception("setSubject(String):mimeMsg.setSubject(" + 
	                mailSubject + "):"+ e.getMessage());
	    }
    }

    public void setBody(String Body)
        throws Exception
    {
	    BodyPart bp = new MimeBodyPart();
	    try{
	        bp.setContent( HTML_HEAD + Body + HTML_TAIL,CONTENT_TYPE);
	    }catch(MessagingException e){
	        throw new Exception("setBody(String):bp.setContent(" + Body + "):" + e.getMessage());
	    }
	    try{
	        this.mp.addBodyPart(bp);
	    }catch(MessagingException e){
	        throw new Exception("setBody(String):mp.addBodyPart(" + bp.toString() + "):" + e.getMessage());
	    }	    
    }

    public void addFileAffix(String filename)
        throws Exception
    {
        BodyPart bp = new MimeBodyPart();
        FileDataSource fileds = new FileDataSource(filename);
	    try{
	        bp.setDataHandler(new DataHandler(fileds));
	        bp.setFileName(fileds.getName());
	        this.mp.addBodyPart(bp);
	    }catch(MessagingException e){
            throw new Exception("addFileAffix(String):bp.setDataHandler(new DataHandler" +
            		"(FileDataSource fileds)):bp.setFileName(" + fileds.getName() + 
            		"):mp.addBodyPart(bp):" + e.getMessage());
	    }
    }

    public void setFrom(String from)
    {
        this.props.put("mail.smtp.from",from);
    }

    public void setTo(String SendTo)
        throws Exception
    {
        try{
	        this.mimeMsg.setRecipients(Message.RecipientType.TO,InternetAddress.parse(SendTo));
	    }catch(MessagingException e){
	        throw new Exception("setTo(String):mimeMsg.setRecipients(Message.RecipientType," +
	        		"InternetAddress[]):" + e.getMessage());
	    }        
    }

    public void setCopyTo(String CopyTo)
        throws Exception
    {
	    try{
	        this.mimeMsg.setRecipients(Message.RecipientType.CC,InternetAddress.parse(CopyTo));
	    }catch(MessagingException e){
	        throw new Exception("setCopyTo(String):mimeMsg.setRecipients(Message.RecipientType," +
	        		"InternetAddress[]):" + e.getMessage());
	    }     
    }

    public boolean sendOut()
    {
  	  	try{
  	  	    this.mimeMsg.setFrom(new InternetAddress(
  	  	            this.props.getProperty("mail.smtp.from",DEFAULT_FROM_ADDRESS)));
	        this.mimeMsg.setContent(this.mp);
	        this.mimeMsg.saveChanges();
	        Transport transport = this.session.getTransport("smtp");
	        transport.connect(this.props.getProperty("mail.smtp.host"),
	                		  this.props.getProperty("mail.smtp.username"),
	                		  this.props.getProperty("mail.smtp.password"));
	        
	        transport.sendMessage(this.mimeMsg,this.mimeMsg.getRecipients(Message.RecipientType.TO));
	        //transport.send(mimeMsg);
	        transport.close();
	        return true;
	    }catch(MessagingException e){
	        this.log.error("sendOut():" + e.getMessage());
            return false;
	    }
    }
}

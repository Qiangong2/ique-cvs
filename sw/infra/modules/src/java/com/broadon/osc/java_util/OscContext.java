package com.broadon.osc.java_util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Date;
import java.util.Properties;
import java.util.TimeZone;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import com.broadon.exception.InvalidRequestException;
import com.broadon.servlet.ServletConstants;
import com.broadon.util.RtExec;
import com.broadon.util.VersionNumber;
import com.broadon.xml.XMLOutputTransformer;


/** OscContext is much like a servlet context in that it holds
 *  references to information about the execution context of the OSC
 *  servlet, along with some utilities related to that context,
 *  including DB access utilities, execution of external C programs,
 *  an external configuration, and such.   Note that one instance of
 *  the context is shared among all threads, and as such all member
 *  variables must be able to handle concurrent references.
 */
public final class OscContext implements ServletConstants
{
    // --------------------------------------------------
    // Invariants
    // --------------------------------------------------
    //
    private static final String URL_PATH_SEP = "/";

    // Default values
    //
    private static final String DEF_JSP_PATH = "/jsp";
    private static final String DEF_XSL_PATH = "/xsl";
    private static final String DEF_TXT_PATH = "/txt";
    private static final String DEF_SVCREQ_PATH = "/opt/broadon/pkgs/osc/bin/submit_svreq";
    private static final String DEF_DOCROOT_PATH = "/opt/broadon/data/svcdrv/htdocs/osc";
    private static final String DEF_XS_URL = "https://localhost:16965/xs";
    private static final String DEF_CDS_URL = "http://localhost:16963/cds";
    private static final String DEF_STORE_ID = "0";
    private static final String DEF_STORE_KIND = "unknown";
    private static final String DEF_STORE_CERTS_PATH = "/opt/broadon/data/osc/stores";
    private static final String DEF_PAGE_EXPIRE_SECS = "600";
    private static final String DEF_MAX_LOGIN_IDLE_SECS = "900";
    private static final String DEF_MAX_LOGIN_SECS = "86400";
    private static final String DEF_DEVICE_MAX_LOGIN_IDLE_SECS = "3600";
    private static final String DEF_DEVICE_MAX_LOGIN_SECS = "31536000";
    private static final String DEF_TRACING_ON = "ON";
    private static final String DEF_TRACING_OFF = "OFF";
    private static final String DEF_XS_TIMEOUT_SECS = "60";
    private static final String DEF_MAX_GAME_CODE_FAILURES = "100";
    private static final String DEF_CONTENT_CACHE_TIMEOUT_SECS = "3600";
    private static final String DEF_SMTP_SERVER = "mail.ique.com";
    private static final String DEF_SMTP_USER = "club";
    private static final String DEF_SMTP_PASSWORD = "club@ique.com";
    private static final String DEF_RPC_APPEND_PARAMS = "OFF";
    private static final String DEF_VNG_SUPPORT = "0";

    // Config variables
    //
    private static final String CFG_XS_RESPONSE_DEFN_PATH = "XSResponseDefnPath";
    private static final String CFG_JSP_PATH = "JSP_PATH";
    private static final String CFG_XSL_PATH = "XSL_PATH";
    private static final String CFG_TXT_PATH = "TXT_PATH";
    private static final String CFG_SVCREQ_PATH = "SVCREQ_PATH";
    private static final String CFG_DOCROOT_PATH = "DOCROOT_PATH";
    private static final String CFG_XS_URL = "XS_URL";
    private static final String CFG_CDS_URL = "CDS_URL";
    private static final String CFG_STORE_ID = "STORE_ID";
    private static final String CFG_STORE_KIND = "STORE_KIND";
    private static final String CFG_STORE_CERTS_PATH = "STORE_CERTS_PATH";
    private static final String CFG_PAGE_EXPIRE_SECS = "PAGE_EXPIRE_SECS";
    private static final String CFG_MAX_LOGIN_IDLE_SECS = "MAX_LOGIN_IDLE_SECS";
    private static final String CFG_MAX_LOGIN_SECS = "MAX_LOGIN_SECS";
    private static final String CFG_DEVICE_MAX_LOGIN_IDLE_SECS = "DEVICE_MAX_LOGIN_IDLE_SECS";
    private static final String CFG_DEVICE_MAX_LOGIN_SECS = "DEVICE_MAX_LOGIN_SECS";
    private static final String CFG_TRACING = "TRACING";
    private static final String CFG_XS_TIMEOUT_SECS = "XS_TIMEOUT_SECS";
    private static final String CFG_CONTENT_CACHE_TIMEOUT_SECS = "CONTENT_CACHE_TIMEOUT_SECS";
    private static final String CFG_MAX_GAME_CODE_FAILURES = "MAX_GAME_CODE_FAILURES";
    private static final String CFG_SMTP_SERVER = "SMTP_SERVER";
    private static final String CFG_SMTP_USER = "SMTP_USER";
    private static final String CFG_SMTP_PASSWORD = "SMTP_PASSWORD";
    private static final String CFG_RPC_APPEND_PARAMS = "RPC_APPEND_PARAMS";
    private static final String CFG_VNG_SUPPORT = "VNG_SUPPORT";

    // Request parameter defaults and special values.
    //
    private static final String        REQCLIENT_DEFAULT = "test";
    private static final String        REQLOCALE_DEFAULT = "zh_CN";
    private static final VersionNumber REQVERSION_DEFAULT = VersionNumber.valueOf("0.0.0");
    private static final VersionNumber REQVERSION_WITH_STORE_ID = VersionNumber.valueOf("1.3.3");


    // --------------------------------------------------
    // Members
    // --------------------------------------------------
    //
    private DataSource db;
    private Properties cfg;
    private OscLogger  log;
    private OscStore   store;
    private String     xsResponseDefnPath;
    private String     defaultActionNm;
    private String     jspPathPrefix;
    private String     xslPathPrefix;
    private String     txtPathPrefix;
    private long       pageExpireMsecs;
    private long       maxLoginIdleMsecs;
    private long       maxLoginMsecs;
    private long       maxDeviceLoginIdleMsecs;
    private long       maxDeviceLoginMsecs;
    private long       contentCacheTimeoutMsecs;
    private String     xsTimeoutSecs;
    private URL        xsUrl;
    private String     cdsUrlString;
    private long       maxGameCodeFailures;
    private String     smtpServer;
    private String     smtpUser;
    private String     smtpPassword;
    private String     rpcAppendParams;
    private String     vngSupport;

    // --------------------------------------------------
    // Hidden helper functions
    // --------------------------------------------------
    //
    private String getCfg(String key, String defValue)
    {
        String v = (String)cfg.get(key);
        if (v == null) {
            v = defValue;
            log.warn("BBserver.properties missing " + key + " value" +
                     " ... using default of " + defValue);
        }
        return v;
    }

    private String getCfg(String key) throws InvalidContext
    {
        String v = (String)cfg.get(key);
        if (v == null) {
            throw new InvalidContext("BBserver.properties missing " +
                                     key + " value");
        }
        return v;
    }


    private URL getCdsUrl() throws InvalidContext
    {
        try {
            return new URL(getCfg(CFG_CDS_URL, DEF_CDS_URL));
        }
        catch (MalformedURLException e) {
            throw new InvalidContext("Malformed " + CFG_CDS_URL + ": ", e);
        }
    } // getCdsUrl


    private URL getXsUrl() throws InvalidContext
    {
        String url = getCfg(CFG_XS_URL, DEF_XS_URL);
        try {
            // Workaround for the fact that URL does not support
            // https (SSL protocol).
            //
            if (url.startsWith("https:")) {
                return new URL("http" + url.substring(5));
            }
            else if (!url.startsWith("http:")) {
                throw new MalformedURLException(
                    "XS URL must start with http: or https:");
            }
            return new URL(url);
        }
        catch (MalformedURLException e) {
            throw new InvalidContext("Malformed " + CFG_XS_URL + ": ", e);
        }
    } // getXsUrl


    private OscStore getOscStore() throws InvalidContext
    {
        // Get OSC store configuration
        //
        OscStore        store = null;
        OscDbConnection conn = null;
        try {
            conn = getDB(true/*read_only*/);
            store = new OscStore(conn,
                                 getCfg(CFG_STORE_ID, DEF_STORE_ID),
                                 getCfg(CFG_STORE_KIND, DEF_STORE_KIND),
                                 getCfg(CFG_STORE_CERTS_PATH,
                                        DEF_STORE_CERTS_PATH));
        }
        catch (SQLException e) {
            throw new InvalidContext("Failed to get OSC DB configuration", e);
        }
        catch (OscStore.InvalidStore e) {
            throw new InvalidContext("Failed to configure OSC as a depot", e);
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return store;
    } // getOscStore


    // --------------------------------------------------
    // Exceptions
    // --------------------------------------------------
    //

    public static class InvalidContext extends OscException {

        public InvalidContext() {super();}
        public InvalidContext(String message) {super(message);}
        public InvalidContext(String message, Throwable throwable) {
            super(message, throwable);
        }
    }

    // --------------------------------------------------
    // Constructor
    // --------------------------------------------------
    //
	public OscContext(ServletConfig config, OscLogger logger)
        throws InvalidContext
    {
        // The logging module
        //
        log = logger;

        // The servlet context
        //
        ServletContext context = config.getServletContext();

        // Property file defined in the conf/BBserver.properties.tmpl file.
        // Add properties there as required for the OSC server.
        //
        cfg = (Properties)context.getAttribute(PROPERTY_KEY);
        if (cfg==null) {
            throw new InvalidContext("ServletContext is missing property file:"
                                     + PROPERTY_KEY);
        }

        // The datasource used to get database connections
        //
        db = (DataSource)context.getAttribute(DATA_SOURCE_KEY);
        if (db==null) {
            throw new InvalidContext("ServletContext is missing data source: "
                                     + DATA_SOURCE_KEY);
        }

        // Whether or not to log tracing information as defined in the
        // conf/BBserver.properties.tmpl
        //
        String tracing = getCfg(CFG_TRACING, DEF_TRACING_OFF);
        if (tracing.toUpperCase().equals(DEF_TRACING_ON)) {
            logger.setTracing(true);
        }

        // Set any parameters that may be passed into
        // this servlet from the config in the web.xml or osc.xml files.
        //
        setDefaultActionNm(
            config.getInitParameter(OscRequestParams.REQ_ACTION));
        xsResponseDefnPath =
            context.getInitParameter(CFG_XS_RESPONSE_DEFN_PATH);

        // Whether or not to append request parameters to RPC response
        //
        rpcAppendParams = getCfg(CFG_RPC_APPEND_PARAMS, DEF_RPC_APPEND_PARAMS);

        // Whether or not to support VNG functionality like buddy/game session etc.
        //
        vngSupport = getCfg(CFG_VNG_SUPPORT, DEF_VNG_SUPPORT);

        // The URL prefix for all JSP pages belonging to this server
        //
        jspPathPrefix = getCfg(CFG_JSP_PATH, DEF_JSP_PATH) + URL_PATH_SEP;

        // The URL prefix for all JSP pages belonging to this server
        //
        xslPathPrefix = getCfg(CFG_XSL_PATH, DEF_XSL_PATH) + URL_PATH_SEP;
        // The URL prefix for all MAIL templete txt belonging to this server
        //
        txtPathPrefix = getCfg(CFG_TXT_PATH, DEF_TXT_PATH) + URL_PATH_SEP;

        // Other invariant values we need frequent and fast access to
        //
        pageExpireMsecs =
            1000 * Long.parseLong(getCfg(CFG_PAGE_EXPIRE_SECS,
                                         DEF_PAGE_EXPIRE_SECS));
        maxLoginIdleMsecs = pageExpireMsecs +
            1000 * Long.parseLong(getCfg(CFG_MAX_LOGIN_IDLE_SECS,
                                         DEF_MAX_LOGIN_IDLE_SECS));
        maxLoginMsecs =
            1000 * Long.parseLong(getCfg(CFG_MAX_LOGIN_SECS,
                                         DEF_MAX_LOGIN_SECS));
        maxDeviceLoginIdleMsecs = 
            1000 * Long.parseLong(getCfg(CFG_DEVICE_MAX_LOGIN_IDLE_SECS,
                                         DEF_DEVICE_MAX_LOGIN_IDLE_SECS));
        maxDeviceLoginMsecs =
            1000 * Long.parseLong(getCfg(CFG_DEVICE_MAX_LOGIN_SECS,
                                         DEF_DEVICE_MAX_LOGIN_SECS));
        contentCacheTimeoutMsecs =
            1000*Long.parseLong(getCfg(CFG_CONTENT_CACHE_TIMEOUT_SECS,
                                       DEF_CONTENT_CACHE_TIMEOUT_SECS));
        if (contentCacheTimeoutMsecs < 0) {
            logger.warn(CFG_CONTENT_CACHE_TIMEOUT_SECS +
                         " is negative, using zero (no caching) instead");
            contentCacheTimeoutMsecs = 0;
        }
        maxGameCodeFailures =
            Long.parseLong(getCfg(CFG_MAX_GAME_CODE_FAILURES,
                                  DEF_MAX_GAME_CODE_FAILURES));

	smtpServer = getCfg(CFG_SMTP_SERVER, DEF_SMTP_SERVER);
	smtpUser = getCfg(CFG_SMTP_USER, DEF_SMTP_USER);
	smtpPassword = getCfg(CFG_SMTP_PASSWORD, DEF_SMTP_PASSWORD);

        // Get the XS URL, XS timeout, and CDS URL string
        //
        xsTimeoutSecs = getCfg(CFG_XS_TIMEOUT_SECS, DEF_XS_TIMEOUT_SECS);
        xsUrl = getXsUrl();
        cdsUrlString = getCdsUrl().toString();

        // Get OSC store/region configuration
        //
        store = getOscStore();
    }

    // --------------------------------------------------
    // General purpose run-time command executor
    // --------------------------------------------------
    //

    /** Executes a system command and returns the standard output and
     *  standard error output concatenated as a string.  Throws an
     *  exception containing the standard error output if there is an
     *  error, and never returns null.
     */
    public static String runCmd(String[] cmd, String[] env, File dir)
        throws IOException
    {
        return RtExec.runCmd(cmd, env, dir);
    }


    /** Executes a system command and returns the stdout output as a sequence
     *  of string tokens with all whitespace in-between tokens removed.
     *  Throws an exception if there is an error, and never returns null.
     */
    public static String[] runCmd_Tokens(String[] cmd, String[] env, File dir)
        throws IOException
    {
        return RtExec.runCmd_Tokens(cmd, env, dir);
    }



    // --------------------------------------------------
    // Common request parameters
    // --------------------------------------------------
    //

    public String getActionNm(HttpServletRequest req)
    {
        String  actionNm = req.getParameter(OscRequestParams.REQ_ACTION);
        if (actionNm == null || actionNm.equals("")) {
            actionNm = defaultActionNm;
        }
        return actionNm;
    }

    public Integer getStoreId(HttpServletRequest req,
                              VersionNumber      reqVersion)
    {
        String  storeNm = req.getParameter(OscRequestParams.REQ_STORE_ID);
        if (storeNm == null || storeNm.equals("")) {
            if (reqVersion.compareTo(REQVERSION_WITH_STORE_ID) >= 0)
                log.error("Missing store_id in OSC request .. using default");
            return getDefaultStoreId();
        }
        else {
            return new Integer(storeNm.trim());
        }
    }

    public VersionNumber getRequestVersion(HttpServletRequest req)
    {
        VersionNumber v = null;
        String  versionNm = req.getParameter(OscRequestParams.REQ_VERSION);
        if (versionNm == null || versionNm.equals("")) {
            v = REQVERSION_DEFAULT;
            log.error("Missing request version number .. using " + v);
        }
        else try {
            v = VersionNumber.valueOf(versionNm.trim());
        }
        catch (Throwable e) {
            v = REQVERSION_DEFAULT;
            log.error("Invalid version number = " + 
                      versionNm + " .. using " + v);
        }
        return v;
    }

    public String getRequestClient(HttpServletRequest req)
    {
        String client = req.getParameter(OscRequestParams.REQ_CLIENT);
        if (client==null || client.equals("")) {
            log.error("Missing client name in OSC request");
            client = REQCLIENT_DEFAULT;
        }
        return client;
    }

    public String getRequestLocale(HttpServletRequest req)
    {
        String locale = req.getParameter(OscRequestParams.REQ_LOCALE);
        if (locale==null || locale.equals("")) {
            log.error("Missing client name in OSC request");
            locale = REQLOCALE_DEFAULT;
        }
        return locale;
    }

    public int getPageNo(HttpServletRequest req)
    {
        String  PageNo = req.getParameter(OscRequestParams.PAGE_NO);
        if (PageNo == null || PageNo.equals("") || !isDigital(PageNo)) {
            PageNo = "0";
        }
        return Integer.parseInt(PageNo);
    }

    // --------------------------------------------------
    // Context configuration property values
    // --------------------------------------------------
    //
    public void setDefaultActionNm(String nm)
    {
        // There will be a different one of these for each servlet
        // as defined in the WEB_INF/web.xml file.
        //
        if (nm == null || nm.length()==0)
            defaultActionNm = null;
        else
            defaultActionNm = nm;
    }

    public String getXsResponseDefnPath()
    {
        return xsResponseDefnPath;
    }

    public String getJspPath(String locale, String jspPage)
    {
        return URL_PATH_SEP + locale + jspPathPrefix + jspPage;
    }

    public String getXslPath(String locale, String xslFile)
    {
        return getDocrootPath() + URL_PATH_SEP + locale + xslPathPrefix + xslFile;
    }
    public String getTxtPath(String locale, String txtFile)
    {
        return getDocrootPath() + URL_PATH_SEP + locale + txtPathPrefix + txtFile;
    }
    public String getSvcreqPath()
    {
        return getCfg(CFG_SVCREQ_PATH, DEF_SVCREQ_PATH);
    }

    public String getDocrootPath()
    {
        return getCfg(CFG_DOCROOT_PATH, DEF_DOCROOT_PATH);
    }

    public String getXsHostname()
    {
        return xsUrl.getHost();
    }

    public int getXsPort()
    {
        return xsUrl.getPort();
    }

    public String getXsUri()
    {
        return xsUrl.getPath();
    }

    public String getCdsUrlString()
    {
        return cdsUrlString;
    }

    public Integer getDefaultStoreId()
    {
        return store.getDefaultStoreID();
    }

    /**
     * @returns the region id the given store
     */
    public String getStoreHrId(Integer storeID)
        throws OscStore.InvalidStore
    {
        return store.getHrID(storeID);
    }

    /**
     * @returns the region id the given store
     */
    public Integer getStoreRegionId(Integer storeID)
        throws OscStore.InvalidStore
    {
        return store.getRegionID(storeID);
    }

    /**
     * @returns the directory for the depot certs for the given store
     */
    public File getStoreCertsDir(Integer storeID)
            throws OscStore.InvalidStore
    {
        return store.getCertsDir(storeID);
    }


    public long getPageExpireMsecs()
    {
        return pageExpireMsecs;
    }

    public long getMaxLoginIdleMsecs()
    {
        return maxLoginIdleMsecs;
    }

    public long getMaxLoginMsecs()
    {
        return maxLoginMsecs;
    }

    public long getDeviceMaxLoginIdleMsecs()
    {
        return maxDeviceLoginIdleMsecs;
    }

    public long getDeviceMaxLoginMsecs()
    {
        return maxDeviceLoginMsecs;
    }

    public String getXsTimeoutSecs()
    {
        return xsTimeoutSecs;
    }

    public long getMaxGameCodeFailures()
    {
        return maxGameCodeFailures;
    }

    public long getContentCacheTimeoutMsecs()
    {
        return contentCacheTimeoutMsecs;
    }

    public String getSmtpServer()
    {
	return smtpServer;
    }

    public String getSmtpUser()
    {
	return smtpUser;
    }

    public String getSmtpPassword()
    {
	return smtpPassword;
    }

    public String getRpcAppendParams()
    {
        return rpcAppendParams.toUpperCase();
    }

    public String getVngSupport()
    {
        return vngSupport;
    }

    // --------------------------------------------------
    // XML utilities
    // --------------------------------------------------
    //
    public static String encodeXml(String tagName,
                                   String value)
    {
        return ("<" + tagName + ">" +
                value +
                "</" + tagName + ">");
    }
    public static String encodeXml_TNL(String tagName,
                                       String value)
    {
        return ("\t<" + tagName + ">" +
                value +
                "</" + tagName + ">\n");
    }
    public static String encodeXml_TTNL(String tagName,
                                       String value)
    {
        return ("\t\t<" + tagName + ">" +
                value +
                "</" + tagName + ">\n");
    }
    public static void encodeXml(PrintWriter out,
                                 String      tagName,
                                 String      value)
    {
        out.print("<" + tagName + ">" + value + "</" + tagName + ">");
    }
    public static void encodeXml_TNL(PrintWriter out,
                                     String      tagName,
                                     String      value)
    {
        out.println("\t<" + tagName + ">" + value + "</" + tagName + ">");
    }
    public static void encodeXml_TTNL(PrintWriter out,
                                     String      tagName,
                                     String      value)
    {
        out.println("\t\t<" + tagName + ">" + value + "</" + tagName + ">");
    }

    public static String xmlEncodeParam(HttpServletRequest req,
                                        String             elm)
    {
        String value = req.getParameter(elm);
        if (value == null) {
            return new String();
        }
        else {
            return encodeXml_TNL(elm, value);
        }
    }

    public static String xmlEncodeParamValues(HttpServletRequest req,
                                              String elm)
    {
        StringBuffer buf = new StringBuffer(1024);
        String[] values = req.getParameterValues(elm);
        if (values != null) {
            for (int i = 0; i < values.length; ++i) {
                buf.append(encodeXml_TNL(elm, values[i]));
            }
        }
        return buf.toString();
    }


    // --------------------------------------------------
    // Time and clock utilities
    // --------------------------------------------------
    //

    public String formatDateUtc(Date date)
    {
        DateFormat gmtFmt = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss:SS z");
        gmtFmt.setTimeZone(TimeZone.getTimeZone("GMT"));
        return gmtFmt.format(date);
    }

    public String formatDateLocal(Date date)
    {
        DateFormat ltFmt = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss:SS z");
        ltFmt.setTimeZone(TimeZone.getDefault());
        return ltFmt.format(date);
    }

    public Date getCurrentTime()
    {
        return new Date(); // Alternatively use the DB time at higher cost
    }

    public String formatDate(Date date,String pattern,String timezone)
    {
        DateFormat Fmt = new SimpleDateFormat(pattern);
        Fmt.setTimeZone(TimeZone.getTimeZone(timezone));
        return Fmt.format(date);
    }

    public Date parseDate(String date,String pattern,String timezone)
        throws ParseException
    {
        if(date==null||date.equals("")){
            return null;
        }
        if(pattern==null||pattern.equals("")){
            pattern="yyyy.MM.dd";
        }
        if(timezone==null||timezone.equals("")){
            timezone="GMT+8:00";
        }
        DateFormat Fmt = new SimpleDateFormat(pattern);
        Fmt.setTimeZone(TimeZone.getTimeZone(timezone));
        return Fmt.parse(date);
    }

    public String getCurrentDate()
    {
        Calendar calendar = new GregorianCalendar();
        java.util.Date date = calendar.getTime();
        DateFormat utc = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
        utc.setTimeZone(TimeZone.getTimeZone("GMT+00:00"));
        return utc.format(date);
    }

    // --------------------------------------------------
    // XML Transformation utility functions
    // --------------------------------------------------
    //

    /** file read utility, returns string of the file content.
     *  @param filename: file name
     */
    public String readFile(String filename)
	    throws IOException
    {
        String lineSep = System.getProperty("line.separator");
        BufferedReader br = new BufferedReader(new FileReader(filename));

        try {
            String nextLine = "";
            StringBuffer sb = new StringBuffer();
            while ((nextLine = br.readLine()) != null) {
                sb.append(nextLine);
                //
                // note:
                //   BufferedReader strips the EOL character.
                //
                sb.append(lineSep);
            }
            return sb.toString();
        } finally {
            br.close();
        }
    }

    /**  XML Transform utility, returns transformed string.
     *  @param xmlStr: XML String to transform
     *  @param xslFile: Name of the XSLT file
     */
    public String transformXML(String xmlStr, String xslFile, String locale)
    {
       String ret = xmlStr;

       try {
           String xslStr = readFile(getXslPath(locale, xslFile));
           if (xslStr!=null && !xslStr.equals(""))
           {
               XMLOutputTransformer xt = new XMLOutputTransformer();
               xt.setStyleSource(xslStr);
               ret = xt.transform(xmlStr);
           }
        } catch (IOException e) {
            this.log.error("IOException while transformation - XSLT file: " + xslFile);
        }
        return ret;
    }


    // --------------------------------------------------
    // Other utilities accessable through this context
    // --------------------------------------------------
    //

    /** Returns a shared thread-safe logger object for this server
     *
     * @returns A logger object.
     */
    public OscLogger getLogger()
    {
        return this.log;
    }

    /** Creates a new read/write database connection from the connection
     * pool.
     *
     * @returns A new logical connection, which may be !isValid() when
     * a connection cannot be established due to an error.
     * @throws SQLException when the connection cannot be created.
     */
    public OscDbConnection getDB(boolean readonly) throws SQLException
    {
        return OscDbConnection.create(this.log, this.db, readonly);
    }

    public static String genPassword(int length)
    {
        SecureRandom secrandom = new SecureRandom();
        StringBuffer password = new StringBuffer(length);
        char[] ch = new char[length];
        for(int i=0; i<length; i++) {
            int k=0;
            while (k<48 || 
            	  (k>57 && k<65) || 
            	  (k>90 && k<97) || 
            	   k>122 ||
            	   k==48 ||
            	   k==49 ||
            	   k==73 ||
            	   k==79 ||
            	   k==105 ||
            	   k==108 ||
            	   k==111) {
                k = 48+(int)(secrandom.nextFloat()*75);
            }
            ch[i] = (char)k;
        }
        password.append(ch);
        return password.toString();
    } // genPassword

    public static String getEncryptedPasswd(String passwd)
        throws NoSuchAlgorithmException
    {
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        md.reset();
        byte[] encoded = md.digest(passwd.getBytes());

        StringBuffer b = new StringBuffer(160);
        for (int i=0; i<encoded.length; i++)
        {
            String elem = Integer.toHexString(encoded[i]&0XFF).toUpperCase();

            if (elem.length() == 1) {
                b.append("0");
            }

            b.append(elem);
        }
        return b.toString();
    }
/*
    public static String transformCh(String input)
    {
        if(input==null){
            return null;
        }else if(input.equals("")){
            return "";
        }
        try {
            byte[] bytes = input.getBytes("ISO8859-1");
            return new String(bytes);
        } catch (Exception e) {
            return null;
        }
    }
*/
    public boolean isMultiBytesIncluded(String str)
			throws UnsupportedEncodingException {
		return utf8EncodingLength(str) != isoEncodingLength(str);
	}

	public long utf8EncodingLength(String str)
			throws UnsupportedEncodingException {
		if (str == null || "".equals(str))
			return 0L;
		return str.getBytes("utf-8").length;
	}

	public long isoEncodingLength(String str)
			throws UnsupportedEncodingException {
		if (str == null || "".equals(str))
			return 0L;
		return str.getBytes("iso8859-1").length;
	}
    
    public static boolean isDigital(String var)
    {
        if (var==null || var.trim().equals("")) return false;
        String string = "0123456789";

        for (int i=0; i<var.length(); i++) {
            if (string.indexOf(var.charAt(i)) == -1) return false;
        }
        return true;
    }

    public static boolean isValidPersonalId(String var)
    {
        if (var==null || var.trim().matches("")) return false;
        int len = var.length();
        if (len !=15 && len !=18) return false;
        String string = "0123456789xX";
        if (len == 15) {
            if (!isDigital(var)) return false;
        } else if (len == 18) {
            String temp = var.substring(0,16);
            if (!isDigital(temp)) return false;
            temp = var.substring(17);
            if (string.indexOf(temp) == -1) return false;
        }
        return true;
    }

    public static boolean isValidEmailAddress(String var)
    {
        if (var==null || var.trim().matches("")) return false;
        int len = var.length();
        if (len <= 4 || len > 40) return false;
        int p1 = var.indexOf("@");
        int p2 = var.indexOf(".");
        int p3 = var.lastIndexOf("@");
        int p4 = var.lastIndexOf(".");
        if ((p1 == -1) || (p1 == len-1) || (p2 == 01) || (p2 == len-1) || (p4 == len-1) ||
            (p1 == p2+1) || (p1 == p2-1)|| (p4 == p2+1) || (p1 < p3)) return false;
        String string = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.-_@";
        for (int i=0; i<len; i++) {
            if (string.indexOf(var.charAt(i)) == -1) return false;
        }
        return true;
    }

    public static boolean isValidDate(int year,int month,int day)
    {
        if (month < 1 || month > 12) return false;
        if (day < 1 || day > 31) return false;
        if ((month == 2) && (day == 30 || day == 31)) return false;
        if (!((year % 4) == 0) && (month == 2) && (day == 29)) return false;
        if ((month <= 7) && ((month % 2)== 0) && (day >= 31)) return false;
        if ((month >= 8) && ((month % 2) == 1) && (day >= 31)) return false;
        return true;
    }

    public static boolean isValidPseudonym(String var)
    {
        if (var == null || var.trim().matches("")) return false;
        String string = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        for (int i=0; i < var.length(); i++) {
           if (string.indexOf(var.charAt(i))==-1) return false;
        }
        return true;
    }
    
    public static boolean isValidNickname(String var)
    {
        if (var==null || var.trim().matches("")) return false;
        String string = "`~!@#$%^&*_=+\\|[]{};:'\",<>/?";
        for (int i=0; i < var.length(); i++) {
           if (string.indexOf(var.charAt(i)) > -1) return false;
        }
        return true;
    }
    
    public static boolean isValidName(String var)
    {
        if (var==null || var.trim().matches("")) return false;
        String string = "`~!@#$%^&*_=+\\|[]{};:'\",<>/?";
        for (int i=0; i < var.length(); i++) {
           if (string.indexOf(var.charAt(i)) > -1) return false;
        }
        return true;
    }
    
    public static boolean isValidPassword(String var)
    {
        if (var==null || var.trim().matches("")) return false;
        String string = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        for (int i=0; i < var.length(); i++) {
           if (string.indexOf(var.charAt(i)) == -1) return false;
        }
        return true;
    }

    public static boolean isValidTelephone(String var)
    {
        if (var == null || var.trim().matches("")) return false;
        String string = "-1234567890";
        for (int i=0; i < var.length(); i++) {
           if (string.indexOf(var.charAt(i)) == -1) return false;
        }
        return true;
    }
    
    public static long getNextMembershipIdSeq(OscContext context, OscLogger log)
    	throws InvalidRequestException
    {
        // Get the next available Membership ID sequence number from the DB.
        long s = 0;
        OscDbConnection conn = null;
        final String SQL_NEXT_MEMBERSHIP_ID_SEQ = 
            "SELECT membership_id_seq.nextval from dual";
        try {
            conn = context.getDB(true);
            ResultSet rs = conn.query(SQL_NEXT_MEMBERSHIP_ID_SEQ);
            if (rs.next()) s = rs.getLong(1);
            rs.close();
        } catch (SQLException e) {
            log.error("Failed to get next membership id from IQUE DB: " + e.getMessage());
            throw new InvalidRequestException("Failed to get next membership id from IQUE DB: " + e.getMessage());
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
        return s;
    } // getNextMembershipIdSeq
    
    public static boolean isEmailExisting(OscContext context,
										  OscLogger log, 
										  String emailAddress,
										  long membershipId,
										  long memberId) 
    	throws InvalidRequestException 
    {	
    	OscDbConnection 	conn 	= null;
		PreparedStatement 	stmt 	= null;
		ResultSet 			rs 		= null;
		
		String sql = "SELECT membership_id,member_id FROM ique_members " +
					 "WHERE NOT (membership_id=? AND member_id=?) AND m" +
					 "ember_email=?";
		boolean isFound = false;
		
		try {
			conn = context.getDB(true);
			stmt = conn.prepareStatement(sql);
			stmt.setLong(1, membershipId);
			stmt.setLong(2, memberId);
			stmt.setString(3, emailAddress);
			
			rs = stmt.executeQuery();
			if (rs.next()) isFound = true;
			
			rs.close();
			conn.closeStatement(stmt);
			stmt = null;

		} catch (SQLException e) {
			log.error("Failed to query the existing of "
					+ "email address in IQUE DB: " + e.getMessage());
			throw new InvalidRequestException("Failed to query "
					+ "the existing of email address in IQUE DB: " + e.getMessage());
		} finally {
			if (conn != null) {
				conn.closeStatement(stmt);
				conn.close();
			}
		}

		return isFound;
	}
    
    public static boolean isPseudonymExisting(OscContext 	context, 
    										  OscLogger 	log, 
    										  String 		pseudonym)
		throws InvalidRequestException 
	{
		OscDbConnection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		boolean isFound = false;
		try {
			conn = context.getDB(true);
			stmt = conn.prepareStatement("SELECT membership_id,member_id " +
					"FROM ique_members WHERE pseudonym=?");
			stmt.setString(1, pseudonym);
			rs = stmt.executeQuery();
			if (rs.next()) {
				isFound = true;
			}
			rs.close();
			conn.closeStatement(stmt);
			stmt = null;

		} catch (SQLException e) {
			log.error("Failed to query the existing of "
					+ "pseudonym in IQUE DB: " + e.getMessage());
			throw new InvalidRequestException("Failed to query "
					+ "the existing of pseudonym in IQUE DB: " + e.getMessage());
		} finally {
			if (conn != null) {
				conn.closeStatement(stmt);
				conn.close();
			}
		}

		return isFound;
	}

	public static boolean isNicknameExisting(OscContext 	context, 
											 OscLogger 		log, 
											 String 		nickname,
											 long			membershipId,
											 long			memberId)
		throws InvalidRequestException 
	{
		OscDbConnection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		boolean isFound = false;
		String sql = "SELECT nick_name FROM ique_members WHERE nick_name=? ";
		if(membershipId != -1 && memberId != -1)
			sql += "AND ( membership_id !=" + Long.toString(membershipId) +
					" AND member_id !=" + Long.toString(memberId) + ")";
		try {
			conn = context.getDB(true);
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, nickname);
			rs = stmt.executeQuery();
			if (rs.next()) {
				isFound = true;
			}
			rs.close();
			conn.closeStatement(stmt);
			stmt = null;

		} catch (SQLException e) {
			log.error("Failed to query the existing of "
					+ "nickname in IQUE DB: " + e.getMessage());
			throw new InvalidRequestException("Failed to query "
					+ "the existing of nickname in IQUE DB: " + e.getMessage());
		} finally {
			if (conn != null) {
				conn.closeStatement(stmt);
				conn.close();
			}
		}

		return isFound;
	}

    public String escQuote(String var) {
        if(var == null) {
            return null;
        } else if(var.equals("")) {
            return "";
        } else {
            var = var.replaceAll("'","\\\\'");
            var = var.replaceAll("\"","\\\\\"");
            return var;
        }
    }

} // class OscContext

package com.broadon.osc.java_util;

import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.broadon.exception.InvalidRequestException;

/** The class used to create the results associated with an account
*  registration request, such that the appropriate document and header can be
*  created in the response back to the client
*/

public class OscResultRegisterConfirm extends OscResultBean {
    
    /** Rules for creating a Login result object
     */
    public static class Rule extends OscResultRule {

        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
        }
        public boolean supports(ServerType t)
        {
            return t == ServerType.HTM;
        }
        public OscResultBean create(HttpServletRequest req,
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
            return new OscResultRegisterConfirm(req, res, this);
        }
    } // class Rule   
    
//  --------------------------------------------
    // Member invariant
    // --------------------------------------------
    
    private static final String SQL_UPDATE_MEMBERSHIP_STATUS =
        "UPDATE ique_memberships SET activate_date=SYS_EXTRACT_UTC(CURRENT_" +
        "TIMESTAMP),status=? WHERE membership_id=?";
    
    private static final String SQL_UPDATE_MEMBER_STATUS =
        "UPDATE ique_members SET status=? WHERE membership_id=? AND member_id=?";
    
    private static final String SQL_QUERY_MEMBER_STATUS =
        "SELECT ms.status ms_status,mb.status mb_status FROM ique_memberships ms," +
        "ique_members mb WHERE ms.membership_id=mb.membership_id AND mb.membership_id=? " +
        "AND mb.member_id=? AND mb.pin=?";
    
    private static final String REDIRECT_HOME_ACTION   = "login_form";
    
    
    private final OscResultRegisterConfirm.Rule    	rule;
    private final OscContext                		context;
    private final OscLogger                 		log;
    private final HttpServletRequest 				request;
    
    // --------------------------------------------
    // Member variables
    // --------------------------------------------
    
    private String  failureMsg;
    private String  error_code;
    private String  redirect_jsp;
    
    private long membership_id;
    private long member_id;
    private String pin;
    
    private OscResultRegisterConfirm(HttpServletRequest  req,
            						 HttpServletResponse res,
            						 OscResultRegisterConfirm.Rule rule)
    	throws InvalidRequestException
    {
        this.rule = rule;
        this.context = rule.getContext();
        this.log = this.context.getLogger();
        this.request = req;
        
        if(req.getParameter("ms_id")!=null 		&& 
          !req.getParameter("ms_id").equals("") &&
           req.getParameter("mb_id")!=null 		&&
          !req.getParameter("mb_id").equals("") &&
           req.getParameter("pin")!=null 		&&
          !req.getParameter("pin").equals("") ){
            this.membership_id = Long.parseLong(req.getParameter("ms_id"));
            this.member_id = Long.parseLong(req.getParameter("mb_id"));
            this.pin = req.getParameter("pin");
        }else{
            this.request.setAttribute("result_case","E1");
        }
    }
    
    public boolean requiresSecureRequest() {return true;}
    public boolean requiresLogin() {return false;}

    public String jspFwdUrl()
    {
        return this.redirect_jsp;
    }

    public String redirectAction()
    {
        return REDIRECT_HOME_ACTION;
    }

    public void encodeXml(PrintWriter out) {}

    public void serveRequest(OscLoginSession loginSession)
        throws InvalidRequestException
    {
        if(this.failureMsg==null){
            OscDbConnection   conn = null;
            PreparedStatement stmt = null;
            ResultSet           rs = null;
            
            String membership_status = null;    
            String member_status 	 = null;
            boolean isFound 	     = false;
            
            try {
                conn = this.context.getDB(true);
                stmt = conn.prepareStatement(SQL_QUERY_MEMBER_STATUS);
                stmt.setLong(1,this.membership_id);
                stmt.setLong(2,this.member_id);
                stmt.setString(3,this.pin);                
                rs = stmt.executeQuery();
                
                if (rs.next()) {
                    isFound = true;
                    membership_status = rs.getString("ms_status");
                    member_status = rs.getString("mb_status");
                }
                rs.close();
                conn.closeStatement(stmt);
                stmt=null;
                
            }catch (SQLException e) {
                this.log.error("Failed to query member status in " +
                		"IQUE DB: " + e.getMessage());
                throw new InvalidRequestException("Failed to query " +
                		"member status in IQUE DB:" + e.getMessage());
            }finally{
                if (conn != null) {
                    conn.closeStatement(stmt);
                    conn.close();
                }
            }
            if(isFound){
                if((membership_status==null || !membership_status.equals("A")) &&
                   (member_status==null || !member_status.equals("A1")) ){
                    try {
                        conn = this.context.getDB(false);
                        stmt = conn.prepareStatement(SQL_UPDATE_MEMBERSHIP_STATUS);
                        stmt.setString(1,"A");
                        stmt.setLong(2,this.membership_id);
                    
                    	stmt.executeUpdate();
                    	conn.closeStatement(stmt);
                    	stmt = null;
                    
                    	stmt = conn.prepareStatement(SQL_UPDATE_MEMBER_STATUS);
                    	stmt.setString(1,"A1");              
                    	stmt.setLong(2,this.membership_id);
                    	stmt.setLong(3,this.member_id);
                    
                    	stmt.executeUpdate();
                    	conn.closeStatement(stmt);
                    	stmt = null;
                    
                    	conn.commit();
                    
                    }catch (SQLException e) {
                        try {
                            conn.rollback();
                        } catch (SQLException ex) {}
                        this.log.error("Failed to update member status in " +
                                "IQUE DB: " + e.getMessage());
                        throw new InvalidRequestException("Failed to update " +
                                "member status in IQUE DB:" + e.getMessage());
                    }finally{
                        if (conn != null) {
               	         	conn.closeStatement(stmt);
               	         	conn.close();
                        }
                    }
                    this.request.setAttribute("result_case","S");
                
                }else if(membership_status.equals("A")&& 
                        member_status.equals("A1")){
                    this.request.setAttribute("result_case","E2");
                }
            }else{
                this.request.setAttribute("result_case","E3");
            }
            
        }
        this.redirect_jsp = "RegisterConfirm.jsp";
    }
    
    public boolean getIsError() {return this.failureMsg != null;}
    public String getErrorMsg() {return this.failureMsg;}
    public String getErrorCode(){return this.error_code;}
}

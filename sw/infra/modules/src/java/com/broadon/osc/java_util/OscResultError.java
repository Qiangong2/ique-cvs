package com.broadon.osc.java_util;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.broadon.exception.InvalidRequestException;


/** The class used to create the result associated with an error
 *  request.  It is a sort of catch-all.
 */
public class OscResultError extends OscResultBean
{
    /** Rules for creating an Unknown result object.  Note that this
     *  result does not support any <code>ServerType</code>.
     */
    public static class Rule extends OscResultRule {

        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
        }
        public boolean supports(ServerType t)
        {
            return false;
        }
        public OscResultBean create(HttpServletRequest req,  
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
            throw new 
                InvalidRequestException("Using wrong constructor");
        }
        public OscResultBean create(HttpServletRequest req,  
                                    HttpServletResponse res,
                                    String              msg,
                                    boolean             loginError)
        {
            return new OscResultError(req, res, msg, loginError, this);
        }
    } // class Rule


    // --------------------------------------------
    // Invariant values
    // --------------------------------------------

    private static final String ERROR_JSP = "Error.jsp";
    private static final String REDIRECT_NONSECURE_JSP = "RedirectNonsecure.jsp";
    private static final String REDIRECT_HOME_ACTION = "login_form";


    // --------------------------------------------
    // Member variables
    // --------------------------------------------

    private final OscResultError.Rule rule;
    private final OscContext          context;
    private final String              errMsg;
    private final boolean             loginError;

    private HttpSession		      session;
    private String		      queryString;
    private String                    requestURL;
    private String                    redirectURL;
    // --------------------------------------------
    // Hidden constructors (all constructors should
    // be hidden for result objects, since they are
    // constructed by the Rule class)
    // --------------------------------------------

    private OscResultError(HttpServletRequest  req,  
                           HttpServletResponse res,
                           String              msg,
                           boolean             loginError, 
                           OscResultError.Rule rule)
    {
        this.rule = rule;
        this.loginError = loginError;
        context = rule.getContext();
        errMsg = msg;
        session = req.getSession(true);
        if(req.getQueryString()!=null){
           queryString = req.getQueryString().replaceAll("\"","\\\\\"");
           requestURL = req.getRequestURL().toString();
           if(requestURL.indexOf("/secure/")>0){
              requestURL = requestURL.replaceFirst("http://","https://");
           }
           redirectURL = requestURL + "?" +  queryString;
        }
    }


    // --------------------------------------------
    // Specialization of abstract methods
    // --------------------------------------------

    public boolean requiresLogin() {return false;}

    public String jspFwdUrl() 
    {
        if (loginError){
            session.setAttribute("RedirectURL", redirectURL);
            return REDIRECT_NONSECURE_JSP;
        }else{
            return ERROR_JSP;
        }
    }

    public String redirectAction()
    {
        return REDIRECT_HOME_ACTION;
    }

    public void encodeXml(PrintWriter out) 
    {
        context.encodeXml(out, "error", getErrorMsg());
    }

    public void serveRequest(OscLoginSession loginSession)
        throws InvalidRequestException
    {
        // Nothing to do
    }

    public String toString() {return "Request Error [" + getErrorMsg() + "]";}
    public boolean getIsError() {return true;}
    public String getErrorMsg()
    {
        try {
            int i = Integer.parseInt(errMsg.trim());
            return OscStatusCode.getMessage(getErrorCode());
        } catch (NumberFormatException nfe) {
            return errMsg;
        }
    }
    public String getErrorCode()
    {
        try {
            int i = Integer.parseInt(errMsg.trim());
            return errMsg;
        } catch (NumberFormatException nfe) {
            return OscStatusCode.OSC_INTERNAL_ERROR;
        }
    }

    // --------------------------------------------
    // Definition of result specific bean values used
    // for formatting a result or access through
    // JSP pages.
    // --------------------------------------------
    //

} // class OscResultError


package com.broadon.osc.java_util;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.broadon.exception.InvalidRequestException;
import com.broadon.status.StatusCode;


/** The class used to create the results associated with a purcahse
 *  request, such that the appropriate XML result can be returned
 *  to the client.  This is only supported for the rpc servlet.
 */
public class OscResultTicketSync extends OscResultBean
{
    /** Rules for creating a Purchase result object
     */
    public static class Rule extends OscResultRule {

        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
        }
        public boolean supports(ServerType t)
        {
            return t == ServerType.RPC;
        }
        public OscResultBean create(HttpServletRequest req,  
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
            return new OscResultTicketSync(req, res, this);
        }
    } // class Rule


    // --------------------------------------------
    // Invariant values
    // --------------------------------------------

    // Purchase response (in addition to request params in OscRequestParams)
    //
    private static final String BB_MODEL_TAG = "bb_model";
    private static final String ETICKET_TAG = "eticket";
    private static final String CA_CHAIN_TAG = "ca_chain";


    // --------------------------------------------
    // Member variables
    // --------------------------------------------

    private final OscResultTicketSync.Rule rule;
    private final HttpServletResponse    response;
    private final OscContext             context;
    private final Integer                storeID;
    private final OscXs                  xs;
    private final String                 xsParams;
    private       String                 status;
    private       String                 statusMsg;
    private       String                 loginName;
    private       Map                    result;

    // --------------------------------------------
    // Hidden constructors (all constructors should
    // be hidden for result objects, sincee they are
    // constructed by the Rule class)
    // --------------------------------------------

    private OscResultTicketSync(HttpServletRequest     req,  
                                HttpServletResponse    res,
                                OscResultTicketSync.Rule rule)
        throws InvalidRequestException
    {
        this.rule = rule;
        response = res;
        context = rule.getContext();
        xs = rule.getXs();
        storeID = context.getStoreId(req, context.getRequestVersion(req));
        status = OscStatusCode.SC_OK;
        statusMsg = StatusCode.getMessage(status);
        loginName = null;
        result = null;

        // Access, verify, and encode the parameters that should 
        // be forwarded to the XS server as a sequence of XML
        // elements.
        //
        xsParams = getXsRequestParameters(req);
    }


    // --------------------------------------------
    // Member functions
    // --------------------------------------------

    private String getXsRequestParameters(HttpServletRequest req)
    {
        // Extract request values that should be in the request XML
        // and that have been interpreted by the XMLFilter, verify
        // their correctness, and encode them as a sequence of xml
        // elements in a String value.
        //
        StringBuffer xsReq = new StringBuffer(1024);
        xsReq.append(context.xmlEncodeParam(req, ORP.BB_ID_TAG));
        xsReq.append(context.xmlEncodeParam(req, ORP.SYNC_TIMESTAMP_TAG));
        xsReq.append(context.xmlEncodeParam(req, ORP.TID_TAG));
        xsReq.append(ORP.ETICKET_IN_FULL_PARAM);
        return xsReq.toString();
    } // getXsRequestParameters


    private void xmlEncodeMapped(PrintWriter out, Map m, String elm)
    {
        Object obj = m.get(elm);
        if (obj == null) {
            //
            // Do nothing
            //
        }
        else if (obj instanceof Vector) {
            Vector vec = (Vector)obj;
            for (int i = 0; i < vec.size(); ++i)
                context.encodeXml_TNL(out, elm, (String)vec.get(i).toString());
        }
        else {
            context.encodeXml_TNL(out, elm, obj.toString());
        }
    } // xmlEncodeMapped


    // --------------------------------------------
    // Specialization of abstract methods
    // --------------------------------------------

    public boolean requiresLogin() {return false;}

    public String jspFwdUrl()  {return null;}


    public void encodeXml(PrintWriter out) 
    {
        // Encodes the final results, where we here can be sure
        // the status and status message is correctly set.
        //
        out.println("<" + ORP.RPC_RESULT_TAG + '>');

        // First encode the action and the final error status
        //
        context.encodeXml_TNL(out, ORP.REQ_ACTION, rule.getActionNm());
        context.encodeXml_TNL(out, STATUS_TAG, status);
        context.encodeXml_TNL(out, STATUS_MSG_TAG, statusMsg);

        // Then encode the remainder of the response as long as
        // there is no error status.
        //
        if (!getIsError()) {
            xmlEncodeMapped(out, result, ORP.BB_ID_TAG);
            xmlEncodeMapped(out, result, ORP.SYNC_TIMESTAMP_TAG);
            xmlEncodeMapped(out, result, ORP.TID_TAG);
            xmlEncodeMapped(out, result, BB_MODEL_TAG);
            xmlEncodeMapped(out, result, ETICKET_TAG);
            xmlEncodeMapped(out, result, CA_CHAIN_TAG);
        }

        // Close the root tag
        //
        out.println("</" + ORP.RPC_RESULT_TAG + '>');
    } // encodeXml


    public void encodeErrorXml(PrintWriter out, String oscStatusCode) 
    {
        out.println("<" + ORP.RPC_RESULT_TAG + '>');
        context.encodeXml_TNL(out, ORP.REQ_ACTION, rule.getActionNm());
        context.encodeXml_TNL(out, STATUS_TAG, oscStatusCode);
        context.encodeXml_TNL(out, STATUS_MSG_TAG, 
                              OscStatusCode.getMessage(oscStatusCode));
        out.println("</" + ORP.RPC_RESULT_TAG + '>');
    } // encodeErrorXml


    public void serveRequest(OscLoginSession loginSession)
        throws InvalidRequestException
    {
        if (loginSession.isLoggedIn()) {
            loginName = loginSession.getPseudonym();
        }

        // Issue a request to the XS server, and upon an error in
        // handling the request or parsing the response, return an
        // error status in the XML returned to the client.  This routine
        // never throws the InvalidRequestException and always produces
        // a valid result, possibly with an XS or OSC error status.
        //
        try {
            result = xs.issueETicketSyncRequest(storeID, xsParams);
        }
        catch (Throwable e)
        {
            context.getLogger().error(e.getMessage());
            result = new HashMap();
            result.put(STATUS_TAG, OscStatusCode.OSC_XS_FORWARDING_ERROR);
            result.put(STATUS_MSG_TAG, 
                       OscStatusCode.getMessage(
                           OscStatusCode.OSC_XS_FORWARDING_ERROR) +
                       ": " + e.getMessage());
        }

        // Correct and record the error status
        //
        status = (String)result.get(STATUS_TAG);
        if (status == null) {
            status = OscStatusCode.OSC_XS_FORWARDING_ERROR;
        }
        statusMsg = (String)result.get(STATUS_MSG_TAG);
        if (statusMsg == null) {
            statusMsg = 
                OscStatusCode.getMessage(
                    OscStatusCode.OSC_XS_FORWARDING_ERROR);
        }
    } // serveRequest


    public String toString() {return null;}

    public boolean getIsError() 
    {
        return !(status.equals(OscStatusCode.SC_OK) ||
                 status.equals(OscStatusCode.XS_ETICKET_IN_SYNC));
    }
    public String getErrorMsg()
    {
        return statusMsg;
    }
    public String getErrorCode() {return status;}

    public boolean getIsLoggedIn() { return (loginName != null); }
    public String getPseudonym() { return loginName; }

    // --------------------------------------------
    // Definition of result specific bean values used
    // for formatting a result or access through
    // JSP pages.
    // --------------------------------------------

} // class OscResultTicketSync


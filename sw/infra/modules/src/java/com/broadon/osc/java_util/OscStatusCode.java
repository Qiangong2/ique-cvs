package com.broadon.osc.java_util;



/**
 * Response code specific to the osc server.  The codes defined here
 * Must be distinct from any code that may be forwarded from other
 * servers, such as the xs server.  We put these codes in the 500 range.
 */
public class OscStatusCode extends com.broadon.status.StatusCode
{
    /** XS specific error codes (make sure these stay in sync with
     *  those defined for the XS server).  We only define those we
     *  need to treat specially in the OSC server.  Note that we do
     *  not create error messages for these.
     */
    public static final String XS_ETICKET_IN_SYNC = "110";

    /** Internal (unspecified) error
     */
    public static final String OSC_INTERNAL_ERROR = "501";
    static final String OSC_INTERNAL_ERROR_MSG =
	"Internal error in the Online Service Center";

    /** Request is bad.
     */
    public static final String OSC_BAD_REQUEST = "502";
    static final String OSC_BAD_REQUEST_MSG =
	"Inconsistent or invalid request to the Online Service Center";

    /** Attempted operation requiring login, without being logged in
     */
    public static final String OSC_REQUIRE_LOGIN = "503";
    static final String OSC_REQUIRE_LOGIN_MSG =
	"The attempted operation requires the subscriber to be logged in";

    /** An error occurred while forwarding a request to the exchange server
     *  (XS server).
     */
    public static final String OSC_XS_FORWARDING_ERROR = "504";
    static final String OSC_XS_FORWARDING_ERROR_MSG =
    "Encountered error in exchange server transaction";

    /** Database problem.
     */
    public static final String OSC_DB_ERROR = "505";
    static final String OSC_DB_ERROR_MSG =
	"Failed to access OSC database";

    /** Login/logout problem.
     */
    public static final String OSC_INCORRECT_LOGIN = "510";
    static final String OSC_INCORRECT_LOGIN_MSG =
	"Incorrect login information. Please try again.";

    public static final String OSC_MISSING_ID = "511";
    static final String OSC_MISSING_ID_MSG =
	"Missing member ID in login request";

    public static final String OSC_MISSING_PSWD = "512";
    static final String OSC_MISSING_PSWD_MSG =
	"Missing member password in login request";

    public static final String OSC_MUST_LOGIN = "513";
    static final String OSC_MUST_LOGIN_MSG =
	"Must be logged in to perform this action";

    public static final String OSC_INCORRECT_LOGOUT = "514";
    static final String OSC_INCORRECT_LOGOUT_MSG =
	"Failed LOGOUT ... You must login before you can logout";

    public static final String OSC_CERTCODE_ERROR = "515";
    static final String OSC_CERTCODE_ERROR_MSG =
	"Incorrect Certificate code.";

    public static final String OSC_EXCEED_MAX_GAMECODE_FAILURES = "516";
    static final String OSC_EXCEED_MAX_GAMECODE_FAILURES_MSG =
	"Exceed maximum game code retries.";
    
    /** Login problem - Account Inactive
     */
    public static final String OSC_ACCOUNT_INACTIVE = "517";
    static final String OSC_ACCOUNT_INACTIVE_MSG =
	"Account is inactive, please contact The Client Service Of iQue.";

    /** VNG specific error codes - 551 error from DB procedure
     *
     */
    public static final String VNG_DB_ERROR = "552";
    static final String VNG_DB_ERROR_MSG =
        "DB Exception while processing VNG request";

    public static final String VNG_DB_UPDATE_ERROR = "553";
    static final String VNG_DB_UPDATE_ERROR_MSG =
        "DB Update failed";    

    public static final String VNG_INVALID_GSESS_QUERY_ERROR = "555";
    static final String VNG_INVALID_GSESS_QUERY_ERROR_MSG =
        "The requested game session query is not supported";

    public static final String VNG_MEMBER_NOT_FOUND_ERROR = "556";
    static final String VNG_MEMBER_NOT_FOUND_ERROR_MSG =
        "Member not found";

    public static final String VNG_MEMBER_ACCOUNT_INACTIVE_ERROR = "557";
    static final String VNG_MEMBER_ACCOUNT_INACTIVE_ERROR_MSG =
        "Member account is inactive";

    public static final String VNG_MEMBER_EXCEEDED_QUOTA_ERROR = "558";
    static final String VNG_MEMBER_EXCEEDED_QUOTA_ERROR_MSG =
        "Member has exceeded his/her message quota";

    public static final String VNG_STORE_MESSAGE_ERROR = "559";
    static final String VNG_STORE_MESSAGE_ERROR_MSG =
        "Failed to send the message";

    public static final String VNG_RETRIEVE_MESSAGE_ERROR = "560";
    static final String VNG_RETRIEVE_MESSAGE_ERROR_MSG =
        "Failed to retrieve the message";

    public static final String VNG_DELETE_MESSAGE_ERROR = "561";
    static final String VNG_DELETE_MESSAGE_ERROR_MSG =
        "Failed to delete the message";

    public static final String VNG_QUOTA_SEND_COUNT_ERROR = "562";
    static final String VNG_QUOTA_SEND_COUNT_ERROR_MSG =
        "You have exceeded the number of messages that you can send. " +
        "You will need to delete some sent messages before you can send new messages.";

    public static final String VNG_QUOTA_SEND_LENGTH_ERROR = "563";
    static final String VNG_QUOTA_SEND_LENGTH_ERROR_MSG =
        "You have exceeded your message length limit. " +
        "You will need to delete some sent messages before you can send new messages.";
    
    public static final String VNG_GAME_SCORE_PARAMS_ERROR = "564";
    static final String VNG_GAME_SCORE_PARAMS_ERROR_MSG =
        "Unintegrated parameters of requesting.";
    
    public static final String VNG_GAME_SCORE_RECORDS_UNFOUND_ERROR = "565";
    static final String VNG_GAME_SCORE_RECORDS_UNFOUND_ERROR_MSG =
        "Records unfound.";

    static {
        addStatusCode(OSC_INTERNAL_ERROR, OSC_INTERNAL_ERROR_MSG);
        addStatusCode(OSC_BAD_REQUEST, OSC_BAD_REQUEST_MSG);
        addStatusCode(OSC_REQUIRE_LOGIN, OSC_REQUIRE_LOGIN_MSG);
        addStatusCode(OSC_XS_FORWARDING_ERROR, OSC_XS_FORWARDING_ERROR_MSG);
        addStatusCode(OSC_DB_ERROR, OSC_DB_ERROR_MSG);
        addStatusCode(OSC_INCORRECT_LOGIN, OSC_INCORRECT_LOGIN_MSG);
        addStatusCode(OSC_MISSING_ID, OSC_MISSING_ID_MSG);
        addStatusCode(OSC_MISSING_PSWD, OSC_MISSING_PSWD_MSG);
        addStatusCode(OSC_MUST_LOGIN, OSC_MUST_LOGIN_MSG);
        addStatusCode(OSC_INCORRECT_LOGOUT, OSC_INCORRECT_LOGOUT_MSG);
        addStatusCode(OSC_CERTCODE_ERROR, OSC_CERTCODE_ERROR_MSG);
        addStatusCode(OSC_EXCEED_MAX_GAMECODE_FAILURES, OSC_EXCEED_MAX_GAMECODE_FAILURES_MSG);

        addStatusCode(VNG_DB_ERROR, VNG_DB_ERROR_MSG);
        addStatusCode(VNG_DB_UPDATE_ERROR, VNG_DB_UPDATE_ERROR_MSG);
        addStatusCode(VNG_INVALID_GSESS_QUERY_ERROR, VNG_INVALID_GSESS_QUERY_ERROR_MSG);
        addStatusCode(VNG_MEMBER_NOT_FOUND_ERROR, VNG_MEMBER_NOT_FOUND_ERROR_MSG);
        addStatusCode(VNG_MEMBER_ACCOUNT_INACTIVE_ERROR, VNG_MEMBER_ACCOUNT_INACTIVE_ERROR_MSG);
        addStatusCode(VNG_MEMBER_EXCEEDED_QUOTA_ERROR, VNG_MEMBER_EXCEEDED_QUOTA_ERROR_MSG);
        addStatusCode(VNG_STORE_MESSAGE_ERROR, VNG_STORE_MESSAGE_ERROR_MSG);
        addStatusCode(VNG_RETRIEVE_MESSAGE_ERROR, VNG_RETRIEVE_MESSAGE_ERROR_MSG);
        addStatusCode(VNG_DELETE_MESSAGE_ERROR, VNG_DELETE_MESSAGE_ERROR_MSG);
        addStatusCode(VNG_QUOTA_SEND_COUNT_ERROR, VNG_QUOTA_SEND_COUNT_ERROR_MSG);
        addStatusCode(VNG_QUOTA_SEND_LENGTH_ERROR, VNG_QUOTA_SEND_LENGTH_ERROR_MSG);
        addStatusCode(VNG_GAME_SCORE_PARAMS_ERROR,VNG_GAME_SCORE_PARAMS_ERROR_MSG);
        addStatusCode(VNG_GAME_SCORE_RECORDS_UNFOUND_ERROR,VNG_GAME_SCORE_RECORDS_UNFOUND_ERROR_MSG);
    }
}

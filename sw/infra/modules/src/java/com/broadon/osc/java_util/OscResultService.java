package com.broadon.osc.java_util;

import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.broadon.exception.InvalidRequestException;
import com.broadon.util.VersionNumber;

/** The class used to create the results associated with a Service
 *  request, such that the appropriate document and header can be
 *  created in the response back to the client
 */
public class OscResultService extends OscResultBean
{
    /** Rules for creating a service result object
     */
    public static class Rule extends OscResultRule {
    	
        public static final String SERVICE_FAQ_LIST = "service";
        
        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
        }
        public boolean supports(ServerType t)
        {
            return t == ServerType.HTM;
        }
        public OscResultBean create(HttpServletRequest req,  
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
            return new OscResultService(req, res, this);
        }
        
    } // class Rule

    private static class Pair {
        private Object first;
        private Object second;
        Pair(Object o1, Object o2) {first=o1;second=o2;}
        Object getFirst() {return first;}
        Object getSecond() {return second;}
    } // class Pair
    
    // --------------------------------------------
    // Invariant values
    // --------------------------------------------

    private static final String SERVICE_JSP = "iQueService.jsp";                           
    private static final String FAQ_SEARCH_KEY= "search_Key";
    private static final String FAQ_SEARCH_CATEGORY= "search_Category";
    private static final int    RECORDSIZE = 10;
    private static final OscCachedMap cachedFaqCategoryList = new OscCachedMap(100);
    private static final String cacheFaqCategoryKey = "faqCategory";
    // --------------------------------------------
    // Member variables
    // --------------------------------------------

    private final OscResultService.Rule rule;
    private final OscContext            context;
    private final OscLogger             logger;
    private final String                actionName;
    private final VersionNumber         version;
    private final Integer               storeID;
    private final String                locale;
    private String 			titleXML;
    private String 			categoryXML;
    private int		                recordCount; 
    private int				pageNo;
    private String			searchKey;
    private String[]			searchCategoryArray;
    private String			searchCategoryID;

    // --------------------------------------------
    // Hidden constructors (all constructors should
    // be hidden)
    // --------------------------------------------

    private OscResultService(HttpServletRequest  req,  
                            HttpServletResponse res,
                            OscResultService.Rule rule)
        throws InvalidRequestException
    {
        int i=0;

        this.rule = rule;
        context = rule.getContext();
        logger = context.getLogger();
        actionName = rule.getActionNm();
        version = context.getRequestVersion(req);
        locale = context.getRequestLocale(req);
        storeID = context.getStoreId(req, version);
        titleXML = "";
        categoryXML = "";
	recordCount = -1;
	pageNo= context.getPageNo(req);

	if (!((req.getParameter(FAQ_SEARCH_KEY)==null)||(req.getParameter(FAQ_SEARCH_KEY).trim().equals("")))) {
		searchKey=req.getParameter(FAQ_SEARCH_KEY).trim();
	}
	if (!(req.getParameter(FAQ_SEARCH_CATEGORY)==null)) {
		searchCategoryArray=req.getParameterValues(FAQ_SEARCH_CATEGORY);
		i=searchCategoryArray.length - 1;
		searchCategoryID = "";
                while(i >=0  && (searchCategoryID.equals("") || searchCategoryID == null)) {
			searchCategoryID = searchCategoryArray[i].trim();
			i--;
                }
	}
    }
    
    private void setCache (OscCachedObject cache, String xml, int recordCount)
    {

        cache.setCached(new Pair(xml, new Integer(recordCount)));
    }
    
    private String getCachedXml(OscCachedObject cache)
    {
        final Pair p = (Pair)cache.getCached();
        return (p == null? null : (String)p.getFirst());
    }
    // --------------------------------------------------------------------
    //            Bean Properties
    // --------------------------------------------------------------------

    /**
     * Bean Property: recordCount
     */
    public int getRecordCount() { return recordCount; }
    
    public int getRecordSize() { return RECORDSIZE; }
    
    public int getPageNo() { return pageNo; }
    
    public String getSearchKey() { return searchKey; }
    
    public String[] getSearchCategory() { return searchCategoryArray; }
    
    // --------------------------------------------
    // Specialization of abstract methods
    // --------------------------------------------

    public boolean requiresLogin() {return true;}

    public String jspFwdUrl() 
    {
        return SERVICE_JSP;
    }

    public void encodeXml(PrintWriter out) {};

    public void serveRequest(OscLoginSession loginSession)
        throws InvalidRequestException
    {
        OscDbConnection conn = null;
        try {
            final long      timeout = context.getContentCacheTimeoutMsecs();
            final Integer   regionID = context.getStoreRegionId(storeID);
            OscCachedObject cache = null;
            OscCachedObject cacheFaqCategory = null;
            String          prefaqcategory = null;
            String	    title_xml = null;
            String	    category_xml = null;
	    String	    sql = null;
	    String	    subsql = null;
            int 	    startRecordNo = 0;
            int	            endRecordNo = 0;

            if (timeout > 0) {
                cache = cachedFaqCategoryList.getMapped(regionID);
                if (cache.isValid(timeout)) {
                    if (logger.isTracing())
                        logger.info("Using CACHED " + actionName);
                    category_xml = getCachedXml(cache);
                }
            
                cacheFaqCategory = cachedFaqCategoryList.getMapped(cacheFaqCategoryKey);
                if (cacheFaqCategory.isValid(timeout)) {
                    if (logger.isTracing())
                        logger.info("Using CACHED " + actionName + 
                                    " for (faqCategory)=" + cacheFaqCategoryKey);
                    prefaqcategory = (String)cacheFaqCategory.getCached();
                }
            }
        
            startRecordNo=pageNo*RECORDSIZE + 1;
            endRecordNo=startRecordNo + RECORDSIZE;

    	    sql = "SELECT a.faq_id, (substr(a.subject,1,46)||decode(sign(46-length(a.subject)),-1,' ...','')) as subject FROM iQue_faqs a,iQue_faq_categories b where a.category_id=b.category_id and b.locale='" + locale + "' and ";
    	    if (searchKey != null) {
                sql=sql + "a.subject like '%" + searchKey + "%' and ";
            }
    	    if ((searchCategoryID != null) && (!searchCategoryID.equals(""))) {
                sql = sql + "a.category_id in (";
                sql = sql + "select c.category_id from ( select d.category_id,NVL(d.parent_category_id,0) as parent_category_id from ique_faq_categories d where d.locale='" + locale + "' and (d.revoke_date>sysdate or (d.revoke_date is null))) c start with c.category_id=" + searchCategoryID + " connect by prior c.category_id=c.parent_category_id";
		sql = sql + ") and ";
    	    }
            sql=sql + "(a.revoke_date is null or a.revoke_date>sysdate) and a.is_visible='visible' ORDER BY NVL(a.qtyreads,0) desc";
            conn = context.getDB(true);
            recordCount = conn.GetRecordCount(sql);
	    
            sql="select rownum seq, faq_id, subject from (" + sql + ")";
            sql="select * from (" + sql + ") where seq>=" + Integer.toString(startRecordNo) +
                " AND seq<" + Integer.toString(endRecordNo);
            title_xml = conn.queryXML(sql);
	    
            if ((category_xml == null) || searchCategoryID != prefaqcategory) {
                sql="SELECT rownum-1 as num,b.* from (SELECT * from (SELECT LEVEL as level_id,a.* from (SELECT category_id,category_desc,NVL(parent_category_id,0) as parent_category_id,";
                if(searchCategoryArray == null) {
                	sql=sql + "decode(category_id,-1,1,0,";
		} else {
                        subsql = "";
			for(int i = 0; i<searchCategoryArray.length; i++) {
                                if(!(searchCategoryArray[i].trim().equals(""))) {
		                	subsql = subsql + "category_id," + searchCategoryArray[i].trim() + ",1,0,";
				}
			}
			if(subsql.equals("")) {
				sql = sql + "decode(category_id,-1,1,0,";
			}else{
				sql = sql + "decode(" + subsql;
			}
		}
		sql=sql.substring(0,sql.length()-1);
                sql=sql + ") as is_default from ique_faq_categories where locale='" + locale + "' and (revoke_date>sysdate or revoke_date is null)) a start with parent_category_id=0 connect by prior category_id=parent_category_id order siblings by category_id) order by level_id,parent_category_id,category_id) b";
                category_xml = conn.queryXML(sql);
                if (cache != null) {
                    setCache(cache, category_xml, 0);
                    cacheFaqCategory.setCached(searchCategoryID);
                }
            }
        
            titleXML = title_xml;
            categoryXML = category_xml;
    } catch (SQLException e) {
        logger.error("Failed to query FAQ." + e.getMessage());
        throw new InvalidRequestException("Failed to query FAQ.");
    } catch (Exception e) {
        logger.error("Failed to obtain " + actionName + " FAQ " +
                                  e.getMessage());
        throw new InvalidRequestException("Failed to obtain list for FAQ.");
    } finally {
        if (conn != null)
            conn.close();
    }
}

    public String toString() {return null;}

    // --------------------------------------------
    // Definition of result specific bean values used
    // for formatting a result or access through
    // JSP pages.
    // --------------------------------------------

    public String transformXML(String xmlStr, String xslFile, String locale)
    { return context.transformXML(xmlStr,xslFile,locale); }

    public String getTitleXML() {return titleXML;}
    
    public String getCategoryXML() {return categoryXML;}

} // class OscResultService

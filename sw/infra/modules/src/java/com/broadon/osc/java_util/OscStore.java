package com.broadon.osc.java_util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;


/** Information about the store or set of stores represented by an OSC
 *  server, including information about regions and certificates for
 *  the stores. After construction, this class uses only read-accesses
 *  to the members and as such does not need to be synchronized.
 */
public class OscStore
{
    private static class OscStoreInfo {
        public Integer storeID;
        public Integer regionID;
        public String  hrID;
        public File    certFile;
        public OscStoreInfo(Integer sid, Integer rid, String hid, File cfile)
        {
            storeID = sid;
            regionID = rid;
            hrID = hid;
            certFile = cfile;
        }
    } // class OscStoreInfo


    private static String HexDigits = "0123456789ABCDEF";


    private HashMap        storeMap; // Maps store_id to stores idx
    private OscStoreInfo[] stores;


    // --------------------------------------------------
    // Private methods
    // --------------------------------------------------
    //
   private static String toHex(byte x)
   {
      
       return ("" + 
               HexDigits.charAt((int)((x>>>4) & 15)) +
               HexDigits.charAt((int)(x & 15)));
   }

    private String createHrID(File macPath) throws IOException
    {
        byte[]          macAddress = new  byte[8];
        FileInputStream macStream = new FileInputStream(macPath);
        if (macStream.read(macAddress) != 6) {
            throw new IOException("Expected 6 bytes in " + macPath);
        }
        macStream.close();
        
        StringBuffer hr = new StringBuffer("HR");
        for (int i = 0; i < 6; ++i)
            hr.append(toHex(macAddress[i]));
        return hr.toString();
    }


    private int queryRegionID(OscDbConnection db,
                              Integer         storeID) throws SQLException
    {
        int          id = -1; // Invalid value
        final String sql =
            "SELECT region_id FROM stores WHERE store_id = " + storeID;

        ResultSet rs = db.query(sql);
        if (rs.next())
            id = rs.getInt(1);
        rs.close();
        return id;
    } // getRegionID


    private boolean verifyStore(OscDbConnection db, int i) throws SQLException
    {
        final String  hrID = stores[i].hrID;
        final Integer storeID = stores[i].storeID;

        boolean ok = hrID.length() == 14 && hrID.startsWith("HR");
        long    depotID = Long.parseLong(hrID.substring(2), 16);

        if (ok) {
            final String sql =
                "SELECT COUNT(*) FROM depots " +
                "   WHERE hr_id=" + depotID + " AND store_id=" + storeID;

            ResultSet rs = db.query(sql);
            ok = (rs.next() && rs.getInt(1) == 1);
            rs.close();
        }
        return ok;
    } // verifyStore


    private int getStoreIdx(Integer storeID) throws InvalidStore
    {
        Object idx = storeMap.get(storeID);
        if (idx == null) {
            throw new InvalidStore("Cannot find OSC store " + storeID);
        }
        return ((Integer)idx).intValue();
    }


    // --------------------------------------------------
    // Exceptions
    // --------------------------------------------------
    //
    public static class InvalidStore extends OscException {

        public InvalidStore() {super();}
        public InvalidStore(String message) {super(message);}
        public InvalidStore(String message, Throwable throwable) {
            super(message, throwable);
        }
    }


    // --------------------------------------------------
    // Public methods
    // --------------------------------------------------
    //
    
    /** Constructor of a Store object unique to an OscContext.
     *
     *  @param db The read-only db connection used to get info about store
     *  @param storeList The list of store_ids separated by commas
     *  @param storeKind The kind of stores represented by the OSC server as 
     *     defined by the certsPath (each valid kind is the name of a
     *     subdirectory in the certsPath). 
     *  @param certsRoot The path to the root directory holding 
     *     certificates for all supported kinds of stores and for each 
     *     valid store belonging to that kind.
     *  @exception InvalidStore is thrown when the given stores do not 
     *     have valid certs defined, or when they are not defined in the DB.
     */ 
    public OscStore(OscDbConnection db,
                    String          storeList, 
                    String          storeKind, 
                    String          certsRoot)
        throws InvalidStore
    {
        // Split the storeList into its individual store_ids
        //
        String[] store_ids = storeList.split(":");
        if (store_ids.length < 1) {
            throw new InvalidStore("Cannot create empty OscStore (storeList=" +
                                   storeList + ")");
        }

        // Add the stores
        //
        stores = new OscStoreInfo[store_ids.length];
        storeMap = new HashMap(store_ids.length);
        try {
            for (int i = 0; i < store_ids.length; ++i) {
                //
                // Based on the store_id, construct the paths to the
                // mac0 and certificates files.  Note that the files
                // are read only and as such concurrent accesses
                // across multiple servlet instances will not require
                // synchronization.
                //
                final Integer storeID = Integer.valueOf(store_ids[i].trim());
                final String  flashPath = 
                    (certsRoot + File.separatorChar + 
                     storeKind + File.separatorChar + 
                     "store" + storeID);
                final File certsPath = 
                    new File(flashPath + File.separatorChar + "depot");
                final File macPath = 
                    new File(flashPath + File.separatorChar + "mac0");

                if (!certsPath.isDirectory()) {
                    throw new InvalidStore("Cannot find " + 
                                           certsPath.getAbsolutePath());
                }
                if (!macPath.isFile()) {
                    throw new InvalidStore("Cannot find " + 
                                           macPath.getAbsolutePath());
                }

                // Get the depot (HR) id and region ID, verify that 
                // these are correctly defined in the database and construct
                // the store.
                //
                final String hrID = createHrID(macPath);
                final int    regionID = queryRegionID(db, storeID);
                if (regionID < 0) {
                    throw new InvalidStore("Cannot find region for store " + 
                                           storeID);
                }
                stores[i] = new OscStoreInfo(storeID, 
                                             new Integer(regionID), 
                                             hrID, 
                                             certsPath);
                if (!verifyStore(db, i)) {
                    throw new InvalidStore("Invalid HR_ID=" + hrID +
                                           " for store_id=" + storeID);
                }
                storeMap.put(storeID, new Integer(i));
            }
        }
        catch (InvalidStore e) {
            throw e;
        }
        catch (java.lang.Exception e) {
            throw new InvalidStore("Cannot create stores for OSC server", e);
        }
    } // OscStore constructor


    /** The default store is the very first one in the store-list
     *  submitted to the constructor of the OscStore.
     *
     *  @returns the id of the store.
     */
    public Integer getDefaultStoreID()
    {
        return stores[0].storeID;
    }


    /** Gets the region id for the store
     *
     *  @param storeID the id of a store
     */
    public Integer getRegionID(Integer storeID) throws InvalidStore
    {
        return stores[getStoreIdx(storeID)].regionID;
    }


    /** Gets the predefined HR_ID id for the store
     *
     *  @param storeID the id of a store
     */
    public String getHrID(Integer storeID) throws InvalidStore
    {
        return stores[getStoreIdx(storeID)].hrID;
    }


    /** Gets the depot directory for the certificates for the store
     *
     *  @param storeID the id of a store
     */
    public File getCertsDir(Integer storeID) throws InvalidStore
    {
        return stores[getStoreIdx(storeID)].certFile;
    }

} // class OscStore

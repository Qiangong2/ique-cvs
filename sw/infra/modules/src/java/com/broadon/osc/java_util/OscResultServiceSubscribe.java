package com.broadon.osc.java_util;

import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.broadon.exception.InvalidRequestException;

/** The class used to create the results associated with a Service
 *  request, such that the appropriate document and header can be
 *  created in the response back to the client
 */
public class OscResultServiceSubscribe extends OscResultBean
{
    /** Rules for creating a service result object
     */
    public static class Rule extends OscResultRule {
    	
        public static final String SERVICE_SUBSCRIBE = "ser_subscribe";
        
        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
        }
        public boolean supports(ServerType t)
        {
            return t == ServerType.HTM;
        }
        public OscResultBean create(HttpServletRequest req,  
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
            return new OscResultServiceSubscribe(req, res, this);
        }
        
    } // class Rule
    
    // --------------------------------------------
    // Invariant values
    // --------------------------------------------

    private static final String SERVICE_SUBSCRIBE_JSP = "iQueServiceSubscribe.jsp";
    private static final String SUBSCRIBE_CONTENT= "subscribe_Content";               
    private static final String DEFAULT_LOCALE="zh_CN";
    private static final String SQL_SUBSCRIBE_LIST =
                "select p1.subscribe_id as subscribe_id,p1.subscribe_name as subscribe_name,decode(sign(0-p2.subscribe_id),-1,'1','0') as default_subscribe "+
                "from osc.ique_subscribes p1,(select subscribe_id from osc.ique_member_subscribes where membership_id=? and member_id=? and (revoke_date is null)) p2 "+
                "where (p1.revoke_date is null) and p1.locale='?' and p1.subscribe_id=p2.subscribe_id(+)  order by p1.subscribe_id";
    private static final String SQL_INSERT_SUBSCRIBE =
                "insert into iQue_member_subscribes(subscribe_id, membership_id, member_id) values(?,?,?)";
    private static final String SQL_DELETE_SUBSCRIBE =
                "update iQue_member_subscribes set revoke_date=sys_extract_utc(current_timestamp) where membership_id=? and member_id=? and (revoke_date is null)";
                      
    // --------------------------------------------
    // Member variables
    // --------------------------------------------

    private final OscResultServiceSubscribe.Rule          rule;
    private final OscContext          		        context;
    private final OscLogger          			logger;
    private String 				        listXML;
    private String                                      actionName;
    private String                                      locale;
    private String                                      subscribeContent;
    private long                                        membership_id;
    private long                                        member_id;

    // --------------------------------------------
    // Hidden constructors (all constructors should
    // be hidden)
    // --------------------------------------------

    private OscResultServiceSubscribe(HttpServletRequest  req,  
                                      HttpServletResponse res,
                                      OscResultServiceSubscribe.Rule rule)
        throws InvalidRequestException
    {
        this.rule = rule;
        context = rule.getContext();
        logger = context.getLogger();
        actionName = rule.getActionNm();
        listXML = "";
        
	if (!((req.getParameter(SUBSCRIBE_CONTENT)==null)||(req.getParameter(SUBSCRIBE_CONTENT).trim().equals("")))) {
		subscribeContent=req.getParameter(SUBSCRIBE_CONTENT).trim();
	}
            
        locale = req.getParameter("locale");
        if (locale==null || locale.equals(""))
            locale = DEFAULT_LOCALE;
          
    }
    
    // --------------------------------------------------------------------
    //            Bean Properties
    // --------------------------------------------------------------------
    
    
    // --------------------------------------------
    // Specialization of abstract methods
    // --------------------------------------------

    public boolean requiresLogin() {return true;}

    public String jspFwdUrl() 
    {
        return SERVICE_SUBSCRIBE_JSP;
    }

    public void encodeXml(PrintWriter out) {};

    public void serveRequest(OscLoginSession loginSession)
        throws InvalidRequestException
    {
        OscDbConnection conn = null;
        PreparedStatement stmt = null;
        membership_id = loginSession.getMembershipID();
	member_id = loginSession.getMemberID();
	
        try {
            String sql = null;
	    conn = context.getDB(false);

	    if ((subscribeContent != null)&&(subscribeContent.trim() != "")){
	    	stmt = conn.prepareStatement(SQL_DELETE_SUBSCRIBE);
	    	stmt.setLong(1, membership_id);
	    	stmt.setLong(2, member_id);
                stmt.executeUpdate();
                conn.closeStatement(stmt);
                stmt = null;
                
                String[] temp = subscribeContent.split(",");
                for(int m=1;m<temp.length;m++){
                    stmt = conn.prepareStatement(SQL_INSERT_SUBSCRIBE);
                    stmt.setLong(1, Long.parseLong(temp[m].trim()));
                    stmt.setLong(2, membership_id);
                    stmt.setLong(3, member_id);
                    stmt.executeUpdate();
                    conn.closeStatement(stmt);
                    stmt = null;
                }

                conn.commit();
                listXML = "1";
	    }else{
	    	sql= SQL_SUBSCRIBE_LIST;
	    	sql = sql.substring(0,sql.indexOf("?")) + String.valueOf(membership_id) + sql.substring(sql.indexOf("?")+1);
	    	sql = sql.substring(0,sql.indexOf("?")) + String.valueOf(member_id) + sql.substring(sql.indexOf("?")+1);
	    	sql = sql.substring(0,sql.indexOf("?")) + locale + sql.substring(sql.indexOf("?")+1);
	    	listXML = conn.queryXML(sql);    
	    }
        }
	catch (SQLException e) {
            try {
                conn.rollback();
            } catch (SQLException ex) {}
            
            logger.error("Failed to process List or Enter Subscribe " + e.getMessage());
            throw new InvalidRequestException("Failed to process List or Enter Subscribe.");

        } finally {
            if (conn != null) {
            	if ((subscribeContent != null)&&(subscribeContent != null)){
                    conn.closeStatement(stmt);
                }
                conn.close();
            }
        }
    }

    public String toString() {return null;}

    // --------------------------------------------
    // Definition of result specific bean values used
    // for formatting a result or access through
    // JSP pages.
    // --------------------------------------------

    public String transformXML(String xmlStr, String xslFile, String locale)
    { return context.transformXML(xmlStr,xslFile,locale); }

    public String getListXML() {return listXML;}

} // class OscResultServiceSubscribe


package com.broadon.osc.java_util;

import java.io.*;

import javax.servlet.http.*;

import com.broadon.osc.java_util.OscResultBean.ORP;
import com.broadon.status.StatusCode;
import com.broadon.exception.InvalidRequestException;

/** The class used to serve VNG server request for game score related queries
 *  and returns the appropriate document and header back to the VNG server.
 */

public class OscResultVNGGameScoreQuery extends OscResultBean
{
    /** Rules for creating a Game Score query result object
     */
    public static class Rule extends OscResultRule {
        
        public static final String GAME_SCORE_QUERY = "gscore_query";

        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
        }
        public boolean supports(ServerType t)
        {
            return t == ServerType.RPC;
        }
        public OscResultBean create(HttpServletRequest req,  
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
            return new OscResultVNGGameScoreQuery(req, res, this);
        }
    } // class Rule


    // --------------------------------------------
    // Invariant values
    // --------------------------------------------

    private static final String FROM_RANK_TAG = "from_rank";
    private static final String COUNT_TAG = "count";
    private static final String TOTAL_COUNT_TAG = "total_count";
    


    // --------------------------------------------
    // Member variables
    // --------------------------------------------

    private final OscResultVNGGameScoreQuery.Rule 	rule;
    private final OscContext          				context;
    private final OscLogger          				logger;
    private final String                        	actionName;
    private final String							actionType;
    
    private String				status;
    private String				statusMsg;

    private String				device_id;
    private String				slot_id;
    private String				title_id;
    private String				game_id;
    private String				score_id;
    private String				item_id;
    private String				membership_id;
    private String				member_id;
    private String				count;
    private String				from_rank;
    private int                 totalCount;

    private String				resultXML;

    // ----------------------------------------------------------
    // Hidden constructors and helper methods (all constructors should
    // be hidden for result objects, since they are constructed by the
    // Rule class)
    // ---------------------------------------------------------

    private OscResultVNGGameScoreQuery(HttpServletRequest  req,  
                                    HttpServletResponse res,
                                    OscResultVNGGameScoreQuery.Rule rule)
        throws InvalidRequestException
    {
        this.rule = rule;
        context = rule.getContext();
        logger = context.getLogger();
        actionName = rule.getActionNm();
        actionType = req.getParameter(ORP.GAME_SCORE_TYPE);
        
        status = OscStatusCode.SC_OK;
        statusMsg = StatusCode.getMessage(status);
        totalCount = 0;
        resultXML = null;
        membership_id = null;
        member_id = null;
        
        if(actionType == null || actionType.equals("")){
        	status = OscStatusCode.VNG_GAME_SCORE_PARAMS_ERROR;
            statusMsg = OscStatusCode.getMessage(status);
            logger.error( 
            		statusMsg + " " +
            		"[OscAction=" + actionName + "] " +
            		"[type=" + actionType + "] ");
            return;
        }
        
        if ("score".equalsIgnoreCase(actionType)
        		|| "object".equalsIgnoreCase(actionType))
        {
        	device_id = req.getParameter(ORP.DEVICE_ID);
            slot_id = req.getParameter(ORP.SLOT_ID);
            title_id = req.getParameter(ORP.TITLE_ID);
            game_id = req.getParameter(ORP.GAME_ID);
            score_id = req.getParameter(ORP.SCORE_ID);
            
            if(device_id == null || device_id.equals("")
            		|| slot_id == null || slot_id.equals("")
            		|| title_id == null || title_id.equals("")
            		|| game_id == null || game_id.equals("")
            		|| score_id == null || score_id.equals(""))
            {
            	status = OscStatusCode.VNG_GAME_SCORE_PARAMS_ERROR;
                statusMsg = OscStatusCode.getMessage(status);
                logger.error(
                		statusMsg + " " +
                		"[OscAction=" + actionName + "] " +
                		"[type=" + actionType + "] (" + 
                		"device_id=" + device_id + "|" +
                		"slot_id=" + slot_id + "|" +
                		"title_id=" + title_id + "|" +
                		"game_id=" + game_id + "|" +
                		"score_id=" + score_id + ")");
                return;
            }
            
        }else if ("range".equalsIgnoreCase(actionType)){
            game_id = req.getParameter(ORP.GAME_ID);
            score_id = req.getParameter(ORP.SCORE_ID);
            item_id = req.getParameter(ORP.ITEM_ID);
            from_rank = req.getParameter(FROM_RANK_TAG);
            count = req.getParameter(COUNT_TAG);
            
            device_id = req.getParameter(ORP.DEVICE_ID);
            membership_id = req.getParameter(ORP.MEMBERSHIP_ID);
            member_id = req.getParameter(ORP.MEMBER_ID);
            
            if(item_id == null || item_id.equals("")
            		|| from_rank == null || from_rank.equals("")
            		|| count == null || count.equals("")
            		|| game_id == null || game_id.equals("")
            		|| score_id == null || score_id.equals(""))
            {
            	status = OscStatusCode.VNG_GAME_SCORE_PARAMS_ERROR;
                statusMsg = OscStatusCode.getMessage(status);
                logger.error(
                		statusMsg + " " +
                		"[OscAction=" + actionName + "] " +
                		"[type=" + actionType + "] (" + 
                		"game_id=" + game_id + "|" +
                		"score_id=" + score_id + "|" +
                		"item_id=" + item_id + "|" +
                		"from_rank=" + from_rank + "|" +
                		"count=" + count + ")");
                return;
            }
            
        }else if ("items".equalsIgnoreCase(actionType)){
        	game_id = req.getParameter(ORP.GAME_ID);
            score_id = req.getParameter(ORP.SCORE_ID);
            
            device_id = req.getParameter(ORP.DEVICE_ID);
            membership_id = req.getParameter(ORP.MEMBERSHIP_ID);
            member_id = req.getParameter(ORP.MEMBER_ID);
            
            if(game_id == null || game_id.equals("")){
            	status = OscStatusCode.VNG_GAME_SCORE_PARAMS_ERROR;
                statusMsg = OscStatusCode.getMessage(status);
                logger.error(
                		statusMsg + " " +
                		"[OscAction=" + actionName + "] " +
                		"[type=" + actionType + "] (" +
                		"membership_id=" + membership_id + "|" +
                		"member_id=" + member_id + "|" +
                		"device_id=" + device_id + "|" +
                		"game_id=" + game_id + "|" +
                		"score_id=" + score_id + ")");
                return;
            }else if((device_id == null || device_id.equals("")) 
            		&& (membership_id == null 
            				|| membership_id.equals("")
            				|| member_id == null 
            				|| member_id.equals(""))){
            	status = OscStatusCode.VNG_GAME_SCORE_PARAMS_ERROR;
                statusMsg = OscStatusCode.getMessage(status);
                logger.error(
                		statusMsg + " " +
                		"[OscAction=" + actionName + "] " +
                		"[type=" + actionType + "] (" +
                		"membership_id=" + membership_id + "|" +
                		"member_id=" + member_id + "|" +
                		"device_id=" + device_id + "|" +
                		"game_id=" + game_id + "|" +
                		"score_id=" + score_id + ")");
                return;
            }
        }

        
    }

    // --------------------------------------------
    // Specialization of abstract methods
    // --------------------------------------------

    public boolean requiresLogin() {return false;}

    public String jspFwdUrl() {return null;}

    public void encodeXml(PrintWriter out)
    {
        // Encodes the final results
        //
        out.println("<" + ORP.RPC_RESULT_TAG + ">");

        context.encodeXml_TNL(out, ORP.REQ_ACTION, actionName);
        context.encodeXml_TNL(out, ORP.GAME_SCORE_TYPE, actionType);
        context.encodeXml_TNL(out, STATUS_TAG, status);
        context.encodeXml_TNL(out, STATUS_MSG_TAG, statusMsg);

        if (!getIsError())
        {
        	if ("range".equalsIgnoreCase(actionType)
        			|| "items".equalsIgnoreCase(actionType))
            context.encodeXml_TNL(out, TOTAL_COUNT_TAG, Integer.toString(totalCount));        

        	if (resultXML != null && !resultXML.equals("")) out.print(resultXML);         
        }

        out.println("</" + ORP.RPC_RESULT_TAG + ">");
    } // encodeXml

    public void serveRequest(OscLoginSession loginSession)
        throws InvalidRequestException
    {
        if (!getIsError()) 
        {
            OscDbConnection conn = null;
            try {
                conn = context.getDB(true);
                if("score".equalsIgnoreCase(actionType)){
                	resultXML = 
                		OscDmlGameScore.getScore(conn,
                								 device_id,
                								 slot_id,
                								 title_id,
                								 game_id,
                								 score_id);
                }else if("object".equalsIgnoreCase(actionType)){
                	resultXML = 
                		OscDmlGameScore.getScoreObject(conn,
                									   device_id,
                									   slot_id,
                									   title_id,
                									   game_id,
                									   score_id);
                }else if("range".equalsIgnoreCase(actionType)){
                	totalCount = 
                		OscDmlGameScore.getRangeScoreCount(conn,
                										   membership_id,
                										   member_id,
                										   device_id,
                										   game_id,
                										   score_id,
                										   item_id,
                										   from_rank,
                										   count);
                	if(totalCount > 0)
                	{
                		resultXML = 
                			OscDmlGameScore.getRangeScore(conn,
                									  	  membership_id,
                									  	  member_id,
                									  	  device_id,
                									  	  game_id,
                									  	  score_id,
                									  	  item_id,
                									  	  from_rank,
                									  	  count);
                	}
                }else if("items".equalsIgnoreCase(actionType)){
                	totalCount = 
                		OscDmlGameScore.getItemsRankCount(conn,
                										  membership_id,
                										  member_id,
                										  device_id,
                										  game_id,
                										  score_id);
                	if(totalCount > 0)
                	{
                		resultXML = 
                			OscDmlGameScore.getItemsRank(conn,
                									 	 membership_id,
                									 	 member_id,
                									 	 device_id,
                									 	 game_id,
                									 	 score_id);
                	}
                }
                
                if(resultXML == null || resultXML.equals("")){
                	status = OscStatusCode.VNG_GAME_SCORE_RECORDS_UNFOUND_ERROR;
					statusMsg = OscStatusCode.getMessage(status);
                }
            }
            catch (Exception e) {
                status = OscStatusCode.VNG_DB_ERROR;
                statusMsg = OscStatusCode.getMessage(status);
                e.printStackTrace();
                logger.error(
                		statusMsg + 
						" [OscAction=" + actionName + "]" + 
						" [Type=" + actionType + "]" +
						" Exception: " + e.getMessage());
            }
            finally {
                if (conn != null)
                    conn.close();
            }
        }
    }

    public String toString() {return null;}

    public boolean getIsError()
    {
        return !status.equals(OscStatusCode.SC_OK);
    }

    public String getErrorMsg()
    {
        return statusMsg;
    }

} // class OscResultGameScoreQuery

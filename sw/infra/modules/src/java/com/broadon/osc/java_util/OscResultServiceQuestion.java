package com.broadon.osc.java_util;

import java.io.PrintWriter;
import java.util.Calendar;
import java.util.List;
import java.util.Iterator;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Blob;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.*;
import com.broadon.exception.InvalidRequestException;

/** The class used to create the results associated with a Service
 *  request, such that the appropriate document and header can be
 *  created in the response back to the client
 */
public class OscResultServiceQuestion extends OscResultBean
{
    /** Rules for creating a service result object
     */
    public static class Rule extends OscResultRule {
    	
        public static final String SERVICE_QUESTION = "ser_question";
        
        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
        }
        public boolean supports(ServerType t)
        {
            return t == ServerType.HTM;
        }
        public OscResultBean create(HttpServletRequest req,  
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
            return new OscResultServiceQuestion(req, res, this);
        }
        
    } // class Rule
    
    // --------------------------------------------
    // Invariant values
    // --------------------------------------------

    private static final String SERVICE_QUESTION_JSP = "iQueServiceQuestion.jsp";
    private static final String SEARCH_SERVICE_KEY = "search_Service_Key";
    private static final String SEARCH_SERVICE_KEY_ID = "search_Service_Key_Id";
    private static final String SERVICE_QUESTION = "service_question";
    private static final String DEFAULT_LOCALE="zh_CN";
    private static final String SQL_QUESTION_SEARCH =
                "select question, answer, service_status from ique_crm_services " +
                "where service_type='QA' and s_skey='?' and s_skey_id=? and membership_id=? and member_id=?";
    
    private static final String SQL_QUESTION_LIST =
                "select rownum, a.* from "+
                "(select s_skey, s_skey_id,(substr(question,1,12)||decode(sign(12-length(question)),-1,' ...','')) as question from " +
                "ique_crm_services where service_type='QA' and membership_id=? and member_id=? " +
                "order by accept_date desc) a where rownum<9";
    
    private static final String SQL_INSERT_SERVICE =
                "INSERT INTO ique_crm_services(s_skey,s_skey_id,service_type,membership_id,member_id,question," +
                "service_status,name,telephone,email) VALUES(?,?,?,?,?,?,?,?,?,?)";
    		
    private static final String SQL_SERVICE_KEY =
                "SELECT lpad(max(substr(s_skey,10,13))+1,4,'0') as max_s_skey from ique_crm_services " +
                "WHERE substr(s_skey,1,9)=?";
                
    private static final String SQL_SERVICE_INFO =
    		"SELECT a.member_email, a.name, b.telephone FROM iQue_members a, iQue_memberships b WHERE " +
                "a.membership_id=b.membership_id and a.membership_id=? and a.member_id=?";

    private static final String SQL_SERVICE_LIST = 
                "SELECT count(*) from iQue_crm_services where service_type='QA' and membership_id=? and member_id=? and (sysdate-accept_date)<0.5 and question=?";

    private static final String SQL_INSERT_IMAGE =
                "INSERT INTO iQue_images_tmp(image_id,image,s_skey,s_skey_id) VALUES(?,empty_blob(),?,?)";

    private static final String SQL_SELECT_IMAGE =
                "SELECT image FROM iQue_images_tmp WHERE image_id=?";

    private static final String SQL_NEXT_IMAGE_ID =
                "SELECT seq_image_id.nextval from dual";

    private static final String SQL_IMAGE_TRANSFER =
                "INSERT INTO iQue_images SELECT * FROM iQue_images_tmp WHERE image_id=?";

    // --------------------------------------------
    // Member variables
    // --------------------------------------------

    private final OscResultServiceQuestion.Rule         rule;
    private final OscContext          		        context;
    private final OscLogger          			logger;
    private String 				        listXML;
    private String					contentXML;
    private String                                      actionName;
    private String                                      locale;
    private String					searchServiceKey;
    private long					searchServiceKeyId;
    private String                                      serviceQuestion;
    private byte[]					serviceImage;
    private long                                        service_id;
    private String                                      service_type;
    private String 					service_status;
    private String					service_skey;
    private long                                        membership_id;
    private long                                        member_id;
    private long                                        Recordcount;
    private String                                      ip_address;
    private String  					failureMsg;
    private String  					error_code;
    private Calendar					service_date;
    private int						insertTime;
    private String					memberName;
    private String                                      memberEmail;
    private String					memberTelephone;
    private String                                      insertServiceKey;
    private long					insertServiceKeyId;
    private long                                        insertImageId;
    private Blob					imageBlob;
    // --------------------------------------------
    // Hidden constructors (all constructors should
    // be hidden)
    // --------------------------------------------

    protected OscResultServiceQuestion(HttpServletRequest  req,  
                                      HttpServletResponse res,
                                      OscResultServiceQuestion.Rule rule)
        throws InvalidRequestException
    {
        this.rule = rule;
        context = rule.getContext();
        logger = context.getLogger();
        actionName = rule.getActionNm();
        listXML = "";
        contentXML = "";
        searchServiceKey = "";
        searchServiceKeyId = 0;
        serviceQuestion = null;
        insertServiceKey = "";
        insertServiceKeyId = 1;
        insertImageId = 0;

        if(FileUpload.isMultipartContent(req)){
           DiskFileUpload upload = new DiskFileUpload();
           try {
              List items = upload.parseRequest(req);
              Iterator iter = items.iterator();
              while(iter.hasNext()){
                 FileItem item = (FileItem)iter.next();
                 if(item.isFormField()){
                    if((item.getFieldName().trim().equals(SEARCH_SERVICE_KEY)) && (item.getString()!=null) && !(item.getString().trim().equals(""))){
                       searchServiceKey = item.getString().trim();
                    }
                 
                    if((item.getFieldName().trim().equals(SEARCH_SERVICE_KEY_ID)) && (item.getString()!=null) && !(item.getString().trim().equals("")) && (OscContext.isDigital(item.getString().trim()))){
                       searchServiceKeyId = Long.parseLong(item.getString().trim());
                    }

                    if((item.getFieldName().trim().equals(SERVICE_QUESTION)) && (item.getString()!=null) && !(item.getString().trim().equals(""))){
                       serviceQuestion = item.getString().trim();
                    }

                    if((item.getFieldName().trim().equals("locale")) && (item.getString()!=null) && !(item.getString().trim().equals(""))){
                       locale = item.getString().trim();
                    }

                 }else{
                   
                    if((item.getName()!=null) && !(item.getName().trim().equals(""))){
                       if((item.getName().substring(item.getName().length()-3)).equalsIgnoreCase("GIF") || (item.getName().substring(item.getName().length()-3)).equalsIgnoreCase("JPG") || (item.getName().substring(item.getName().length()-3)).equalsIgnoreCase("BMP")){
                          serviceImage = item.get();
                       }
                    }
                 }
              }
           }
           catch(Exception e){
              context.getLogger().error("Failed to get multipart request." + e.getMessage());
              throw new InvalidRequestException("Failed to get multipart request.");
           }
        }

	
	
        //locale = req.getParameter("locale");
        if (locale==null || locale.equals(""))
            locale = DEFAULT_LOCALE;        
            
        service_type = "QA";
        service_status = "W";
        service_date = Calendar.getInstance();
        service_skey = "CRM" + String.valueOf(service_date.get(Calendar.YEAR)).substring(2,4);
        if((service_date.get(Calendar.MONTH)+1)<10) {
           service_skey = service_skey + "0" + (service_date.get(Calendar.MONTH)+1);
        }else{
           service_skey = service_skey + (service_date.get(Calendar.MONTH)+1); 
        }
        if(service_date.get(Calendar.DAY_OF_MONTH)<10) {
           service_skey = service_skey + "0" + service_date.get(Calendar.DAY_OF_MONTH);
        }else{
           service_skey = service_skey + service_date.get(Calendar.DAY_OF_MONTH);
        }
        ip_address = req.getRemoteAddr();
        failureMsg = null;
        error_code = null;
    }
    
    // --------------------------------------------------------------------
    
    
    // --------------------------------------------
    // Specialization of abstract methods
    // --------------------------------------------

    public boolean requiresLogin() {return true;}

    public String jspFwdUrl() 
    {
        return SERVICE_QUESTION_JSP;
    }

    public void encodeXml(PrintWriter out) {};

    public void serveRequest(OscLoginSession loginSession)
        throws InvalidRequestException
    {
        OscDbConnection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        membership_id = loginSession.getMembershipID();
	member_id = loginSession.getMemberID();
        boolean isNewRecord = false;
	
        try {
            String sql = null;
    
	    conn = context.getDB(false);
	    conn.setAutoCommit(true);
            if((searchServiceKey != null) && (!(searchServiceKey.trim().equals("")))){
               contentXML = "2";
               sql = SQL_QUESTION_SEARCH;
               sql = sql.substring(0,sql.indexOf("?")) + searchServiceKey + sql.substring(sql.indexOf("?")+1);
               sql = sql.substring(0,sql.indexOf("?")) + Long.toString(searchServiceKeyId) + sql.substring(sql.indexOf("?")+1);
               sql = sql.substring(0,sql.indexOf("?")) + Long.toString(membership_id) + sql.substring(sql.indexOf("?")+1);
               sql = sql.substring(0,sql.indexOf("?")) + Long.toString(member_id) + sql.substring(sql.indexOf("?")+1);
               contentXML = conn.queryXML(sql);
            }else{
	       if((serviceQuestion != null) && (!(serviceQuestion.trim().equals("")))){
                  stmt = conn.prepareStatement(SQL_SERVICE_LIST);
                  stmt.setLong(1, membership_id);
                  stmt.setLong(2, member_id);
                  stmt.setString(3, serviceQuestion);
                  rs = stmt.executeQuery();
                  if(rs.next()){
                     insertTime = rs.getInt(1);
                  }else{
                     insertTime = 1;
                  }
                  rs.close();
                  conn.closeStatement(stmt);

                  if(insertTime==0){
                     stmt = conn.prepareStatement(SQL_SERVICE_INFO);
                     stmt.setLong(1, membership_id);
                     stmt.setLong(2, member_id);
                     rs = stmt.executeQuery();
                     if(rs.next()){
                        memberEmail = rs.getString(1);
                        memberName = rs.getString(2);
                        memberTelephone = rs.getString(3);
                     }
                     rs.close();
                     conn.closeStatement(stmt);

                     stmt = conn.prepareStatement(SQL_SERVICE_KEY);
                     stmt.setString(1, service_skey);
                     rs = stmt.executeQuery();
                     if(rs.next()){
                        if(rs.getString(1)==null){
                           insertServiceKey = service_skey + "0001";
                        }else{
                           insertServiceKey = service_skey + rs.getString(1);
                        }
                     }else{
                        insertServiceKey = service_skey + "0001";
                     }
                     rs.close();
                     conn.closeStatement(stmt);

                     stmt = conn.prepareStatement(SQL_INSERT_SERVICE);
                     stmt.setString(1, insertServiceKey);
                     stmt.setLong(2, insertServiceKeyId);
                     stmt.setString(3, service_type);
                     stmt.setLong(4, membership_id);
                     stmt.setLong(5, member_id);
                     stmt.setString(6, serviceQuestion);
                     stmt.setString(7, service_status);
                     stmt.setString(8, memberName);
                     stmt.setString(9, memberTelephone);
                     stmt.setString(10, memberEmail);
                     stmt.executeUpdate();
                     conn.closeStatement(stmt);

                     if(serviceImage!=null){
                        stmt = conn.prepareStatement(SQL_NEXT_IMAGE_ID);
                        rs = stmt.executeQuery();
                        if(rs.next()){
                           insertImageId = rs.getLong(1);
                        }
                        rs.close();
                        conn.closeStatement(stmt);
                     
                        if(insertImageId!=0){
                           stmt = conn.prepareStatement(SQL_INSERT_IMAGE);
                           stmt.setLong(1, insertImageId);
                           stmt.setString(2, insertServiceKey);
                           stmt.setLong(3, insertServiceKeyId);
                           stmt.executeUpdate();
                           conn.closeStatement(stmt);
                        
                           stmt = conn.prepareStatement(SQL_SELECT_IMAGE);
                           stmt.setLong(1, insertImageId);
                           rs = stmt.executeQuery();
                           if(rs.next()){
                              imageBlob = (Blob)rs.getBlob(1);
                              imageBlob.setBytes(1, serviceImage);
                           }
                           rs.close();
                           conn.closeStatement(stmt);
 
                           stmt = conn.prepareStatement(SQL_IMAGE_TRANSFER);
                           stmt.setLong(1, insertImageId);
                           stmt.executeUpdate();
                           conn.closeStatement(stmt);
                        }
                     }
                     contentXML="1";
                  }else{
                     contentXML="3";
                  }
               }
	    }
            sql = SQL_QUESTION_LIST;
            sql = sql.substring(0,sql.indexOf("?")) + Long.toString(membership_id) + sql.substring(sql.indexOf("?")+1);
            sql = sql.substring(0,sql.indexOf("?")) + Long.toString(member_id) + sql.substring(sql.indexOf("?")+1);
            listXML = conn.queryXML(sql);
            conn.commit();
        }
	catch (SQLException e) {
            try {
                conn.rollback();
                contentXML = "4";
            } catch (SQLException ex) {}
            
            logger.error("Failed to process Search or Enter Question " + e.getMessage());
            throw new InvalidRequestException("Failed to process Search or Enter Question.");

        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public String toString() {return null;}

    // --------------------------------------------
    // Definition of result specific bean values used
    // for formatting a result or access through
    // JSP pages.
    // --------------------------------------------

    public String transformXML(String xmlStr, String xslFile, String locale)
    { return context.transformXML(xmlStr,xslFile,locale); }

    public String getListXML() {return listXML;}

    public String getContentXML() {return contentXML;}

    public String getLocale() {return locale;}
} // class OscResultServiceQuestion

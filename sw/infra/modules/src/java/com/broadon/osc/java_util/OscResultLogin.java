package com.broadon.osc.java_util;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.broadon.exception.InvalidRequestException;


/** The class used to create the results associated with a login
 *  request, such that the appropriate document and header can be
 *  created in the response back to the client
 */
public class OscResultLogin extends OscResultBean
{
    /** Rules for creating a Login result object
     */
    public static class Rule extends OscResultRule {

        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
        }
        public boolean supports(ServerType t)
        {
            return t == ServerType.HTM;
        }
        public OscResultBean create(HttpServletRequest req,
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
            return new OscResultLogin(req, res, this);
        }
    } // class Rule


    // --------------------------------------------
    // Invariant values
    // --------------------------------------------

    private static final String REDIRECT_NONSECURE_JSP = "RedirectNonsecure.jsp";
    private static final String REDIRECT_HOME_ACTION = "login_form";

    // Common error messages
    //
    private static final String ERR_MISSING_ID = OscStatusCode.OSC_MISSING_ID;
    private static final String ERR_MISSING_PSWD = OscStatusCode.OSC_MISSING_PSWD;
    private static final String ERR_CERTCODE_ERROR = OscStatusCode.OSC_CERTCODE_ERROR;


    // --------------------------------------------
    // Member variables
    // --------------------------------------------

    private final OscResultLogin.Rule rule;
    private final OscContext          context;
    private String                    memberID;
    private String                    passwd;
    private String                    certcode;
    private String                    loginName;
    private String                    failureMsg;

    // --------------------------------------------
    // Hidden constructors (all constructors should
    // be hidden for result objects, sicne they are
    // constructed by the Rule class)
    // --------------------------------------------

    private OscResultLogin(HttpServletRequest  req,
                           HttpServletResponse res,
                           OscResultLogin.Rule rule)
        throws InvalidRequestException
    {
        memberID = req.getParameter("id");
        passwd = req.getParameter("pwd");
        certcode = req.getParameter("certcode");
        loginName = null;

        this.rule = rule;
        context = rule.getContext();

        // Some initial error checking
        //
        if (memberID == null || memberID.length() == 0) {
            failureMsg = ERR_MISSING_ID;
        } else if (passwd == null || passwd.length() == 0) {
            failureMsg = ERR_MISSING_PSWD;
        } else if (certcode == null) {
            failureMsg = ERR_CERTCODE_ERROR;
        }
        else {
            failureMsg = null;
        }
    }


    // --------------------------------------------
    // Member functions
    // --------------------------------------------

    // --------------------------------------------
    // Specialization of abstract methods
    // --------------------------------------------

    public boolean requiresSecureRequest() {return true;}
    public boolean requiresLogin() {return false;}

    public String jspFwdUrl()
    {
        return REDIRECT_NONSECURE_JSP;
    }

    public String redirectAction()
    {
        return REDIRECT_HOME_ACTION;
    }

    public void encodeXml(PrintWriter out) {};

    public void serveRequest(OscLoginSession loginSession)
        throws InvalidRequestException
    {
        if (!getIsError()) try {
            if (!loginSession.loginAuthCode(memberID, passwd, certcode)) {
                context.getLogger().debug(">> Login Certcode Failure for " +
                                          memberID);
                failureMsg = ERR_CERTCODE_ERROR;
            }
            else if (loginSession.isLoggedIn()) {
                loginName = loginSession.getPseudonym();
            }
            else {
                context.getLogger().debug(">> Login Failure for " + memberID);
                failureMsg = loginSession.getLoginErrorCode();
            }
        }
        catch (OscLoginSession.InternalLoginException e) {
            context.getLogger().error(e);
            throw new InvalidRequestException("Internal login failure:", e);
        }
    }


    public String toString() {return null;}

    public boolean getIsError() {return failureMsg != null;}
    public String getErrorCode() {return failureMsg;}

    public boolean getIsLoggedIn() { return (loginName != null); }
    public String getPseudonym() { return loginName; }

    // --------------------------------------------
    // Definition of result specific bean values used
    // for formatting a result or access through
    // JSP pages.
    // --------------------------------------------

} // class OscResultLogin


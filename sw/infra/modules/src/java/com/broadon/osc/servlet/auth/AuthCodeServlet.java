/*
 * (C) 2004, BroadOn Communications Corp.,
 * $Id: AuthCodeServlet.java,v 1.6 2005/03/22 03:04:11 ho Exp $
 */
package com.broadon.osc.servlet.auth;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.imageio.*;
import java.util.*;
import java.awt.*; 
import java.awt.image.*;
import org.apache.log4j.Logger;
import com.broadon.osc.java_util.*;

public class AuthCodeServlet extends HttpServlet
{
    private OscLogger  logger = null;  // A thin wrapper around log4j
    private OscContext context = null; // Config and system resources

    public void init (ServletConfig config) throws ServletException
    {
        super.init(config);
        logger = new OscLogger(Logger.getLogger(OscLogger.class.getName()));

        OscStatusCode dummy = new OscStatusCode();
 
        try {
            // Create the context for this servlet, through which we can
            // access config values, the run-time environment, and the 
            // database.
            //
            context = new OscContext(config, logger);

        } 
        catch (OscContext.InvalidContext e) {
            logger.debug(e);
            throw new ServletException(e.getMessage());
        }

        ImageIO.setUseCache(false);
    } // init


    public void doGet(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
        OscCookie     cookie = OscLoginSession.getLoginCookie(context, req);
        BufferedImage image = new BufferedImage(60,20,BufferedImage.TYPE_INT_RGB);
        Graphics g = image.getGraphics();
        int width=100,height=100;
        g.setColor(Color.white);
        g.fillRect(0, 0, width, height);
        g.setColor(Color.white);
        g.drawRect(0,0,width-1,height-1);

        String rand=Double.toString(Math.random()*10000);
        rand = rand.substring(0,rand.indexOf("."));

        switch(rand.length())
        {
            case 1: rand = "000"+rand; break;
            case 2: rand = "00"+rand; break;
            case 3: rand = "0"+rand; break;
            default: rand = rand.substring(0,4); break;
        }

        // Set response headers, using the default of:
        //
        //    res.setStatus(HttpServletResponse.SC_OK);
        //
        res.setDateHeader("Last-Modified", System.currentTimeMillis());
        res.setDateHeader("Expires", -1L);
        res.setHeader("Cache-Control", "no-cache");
        res.setHeader("Pragma", "no-cache");
        res.setContentType("image/jpeg");
        try {
            cookie.setAuthCode(rand);
        }
        catch (OscCookie.InvalidCookieException e) {
            logger.error(e);
        }
        final String encodedCookie = cookie.encode();
        OscLoginSession.setLoginCookie(res, encodedCookie);
        if (logger.isTracing())
            logger.info("AuthCode outgoing cookie=" + encodedCookie);

        g.setColor(Color.BLUE);
        g.setFont(new Font("Times New Roman",Font.PLAIN,20));
        g.drawString(rand,10,15);

        Random random = new Random();
        for (int i=0;i<88;i++)
        {
            int x = random.nextInt(width);
            int y = random.nextInt(height);
            g.drawLine(x,y,x,y);
        }

        g.dispose();

        ImageIO.write(image, "JPEG", res.getOutputStream());
    }

    public void doPost(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
        doGet(req, res);
    }

} // class AuthCodeServlet



package com.broadon.osc.java_util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.broadon.exception.InvalidRequestException;
import com.broadon.status.StatusCode;
import com.broadon.util.Base64;
import com.broadon.util.HexString;


/** The class used to process/update the GameState for
 *  the certainly member according to the upload 
 *  RPC data.
 */
public class OscResultGameState extends OscResultBean
{
	// Native code interface
    private static native String getScore(long titleID, long contentID,
                                       byte[] states, byte[] signature,
                                       byte[] bbPubKey);

    /** Rules for creating a Purchase result object
     */
    public static class Rule extends OscResultRule {

        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
        }
        public boolean supports(ServerType t)
        {
            return t == ServerType.RPC;
        }
        public OscResultBean create(HttpServletRequest req,  
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
            return new OscResultGameState(req, res, this);
        }
    } // class Rule

    // --------------------------------------------
    // Invariant values
    // --------------------------------------------
    private static final String SQL_QUERY_BB_PUBKEY = 
	    " SELECT PUBLIC_KEY FROM BB_PLAYERS " +
	    " WHERE BB_ID=?";
    private static final String SQL_UPDATE_COMPETITION_SCORE =
	    " UPDATE IQUE_COMPETITION_SCORES SET " + 
	    " SCORE_DESC = ? , SCORE = ? " +
	    " WHERE MEMBERSHIP_ID = ? and MEMBER_ID = ? and DEVICE_ID = ? and COLUMN_ID = ?";
    private static final String SQL_INSERT_COMPETITION_SCORE = 
	    " INSERT INTO IQUE_COMPETITION_SCORES " +
	    " (COLUMN_ID, MEMBERSHIP_ID, MEMBER_ID, DEVICE_ID," +
	    " SCORE_DESC, SCORE) VALUES (" +
	    " ?, ?, ?, ?, ?, ?)" ;
    private static final String SQL_QUERY_COMPETITION_SCORE = 
	    " SELECT SCORE " +
	    " FROM IQUE_COMPETITION_SCORES " +
	    " WHERE COLUMN_ID = ? and MEMBERSHIP_ID = ? and MEMBER_ID = ? and DEVICE_ID = ?";
    private static final String SQL_QUERY_COLUMN_TYPE = 
	    " SELECT COMPETITION_ID, COLUMN_TYPE, COLUMN_NAME, RULE_ID FROM IQUE_COMPETITION_COLUMNS " +
	    " WHERE COLUMN_ID = ? ";
    private static final String SQL_INSERT_COMPETITION_SCORE_LOG =
	    " INSERT INTO IQUE_COMPETITION_SCORE_LOGS (" +
	    " MEMBERSHIP_ID, MEMBER_ID, COMPETITION_ID, DEVICE_ID," +
	    " IP_ADDRESS ) " +
	    " VALUES(?, ?, ?, ?, ?)";
    private static final String SQL_QUERY_MAX_RECORD = 
	    " SELECT NVL(MAX(SCORE), 0) FROM IQUE_COMPETITION_SCORES " +
	    " WHERE COLUMN_ID = ?";
    private static final String SQL_QUERY_MIN_RECORD = 
	    " SELECT NVL(MIN(SCORE), 999999) FROM IQUE_COMPETITION_SCORES " +
	    " WHERE COLUMN_ID = ?";
    private static final String SQL_UPDATE_POINT = 
	    "{call IQUE_MEMBER_POINT_PKG.EARN(?,?,?,?,?)}";
    // --------------------------------------------
    // Member variables
    // --------------------------------------------

    private final OscResultGameState.Rule rule;
    private final HttpServletResponse    response;
    private final OscContext             context;
    private final OscLogger              log;
    private final String                 locale;
    private       String                 status;
    private       String                 statusMsg;
    private       String                 result;
    private	  long			 bb_id;
    private	  byte[] 		 gamestate;
    private	  byte[] 		 signature;
    private	  long			 contentID;
    private	  long			 titleID;
    private	  byte[]		 bbPubKey;
    // Game State Vars
    private	  String		 score_desc;
    private	  long			 score;
    // Member 
    private       String                 loginName;
    private	  long			 membership_id;
    private	  long			 member_id;
    // Competition 
    private	  int			 competition_id;
    private	  int			 column_id;
    private	  String		 column_type;
    private	  String		 column_name;
    // For log
    private	  String		 ip_address;
    // Rule ID for DB process 
    private       long			 rule_id;
    private       String		 reason;
    // score is a temp variable for test only
    // the detail score will be more complex than a simple score;
    
    // --------------------------------------------
    // Hidden constructors (all constructors should
    // be hidden for result objects, sincee they are
    // constructed by the Rule class)
    // --------------------------------------------

    private OscResultGameState(HttpServletRequest        req,  
                                 HttpServletResponse       res,
                                 OscResultGameState.Rule rule)
        throws InvalidRequestException
    {
        this.rule = rule;
        response = res;
        context = rule.getContext();
        log = context.getLogger();
        locale = context.getRequestLocale(req);
        status = OscStatusCode.SC_OK;
        statusMsg = StatusCode.getMessage(status);
        loginName = null;
	result = null;
	rule_id = 0;
	reason = "";
	member_id = 0;
	membership_id = 0;
        try {
	    result = processRequest(req);
	} catch (IOException e) {
		log.error("IO Exception!\n" + e.toString());
	}
    }

    static {
	try {
		System.load(System.getProperty("catalina.base") + "/webapps/osc/libComputeScore.so");
	} catch (SecurityException e1) { 
		System.out.println("Can NOT load the library libComputeScore.so");
	} catch (UnsatisfiedLinkError e2) {
		System.out.println("library libComputeScore doesn't exist");
	}
    }

    // --------------------------------------------
    // Member functions
    // --------------------------------------------
    private byte[] getBBPubKey(long bb_id)
    {
	// Get the BBPlayer Public Key 
	
	OscDbConnection   conn = null;
	PreparedStatement stmt = null;
	ResultSet           rs = null;
	byte[]		  pubkey = null;

	try {
		conn = this.context.getDB(true);
		stmt = conn.prepareStatement(SQL_QUERY_BB_PUBKEY);
		stmt.setLong(1,bb_id);

        	rs = stmt.executeQuery();
        	if (rs.next()) {
			int i = 1; 
			pubkey = new BigInteger(rs.getString(i), 32).toByteArray(); 
        	}
        	rs.close();
        	conn.closeStatement(stmt);
        	stmt=null;
        } catch (SQLException e) {
        	this.log.error("Failed to query bb_player id in IQUE DB: " + e.getMessage());
		return null;
        } finally {
        	if (conn != null) {
            		conn.closeStatement(stmt);
            		conn.close();
         	}
        }
	return pubkey;
    }
	
    private byte[] decompressGameState(String states)
	    throws IOException
    {
	// Decompressing the compress and base64 encoded game states
	byte[] input = null;
	byte[] buf = new byte[4096];
	// Decode the BASE64 game state
	try {
		input = new Base64().decode(states);
	}  catch (java.io.IOException e) {
		log.error("Can NOT decode game state!\n" + e.toString());
		return null;
	}
	ByteArrayInputStream is = 
		new ByteArrayInputStream(input);

	// Decompress the game state
	Inflater decompresser = new Inflater(false);
	InflaterInputStream in =
	        new InflaterInputStream(is, decompresser);
	ByteArrayOutputStream out = new ByteArrayOutputStream();
	try {
		int n;
	        while ((n = in.read(buf, 0, buf.length)) > 0)
		        out.write(buf, 0, n);
		return out.toByteArray();
    	} finally {
		is.close();
		out.close();
		in.close();
	}
    }

    private String processRequest(HttpServletRequest req)
	    throws IOException
    {
	// Get the Game State from the request.
	// title_id, states, signature
	try {
		req.setCharacterEncoding("UTF-8");
	} catch (java.io.UnsupportedEncodingException e) {
		log.error("Unsupported Encoding!\n" + e.toString());
	}

	try {
		bb_id = new Long(req.getParameter(ORP.BB_ID_TAG)).longValue();
		titleID = new Long(req.getParameter(ORP.TITLE_ID_TAG)).longValue();
		contentID = new Long(req.getParameter(ORP.CONTENT_ID_TAG)).longValue();
	} catch (NumberFormatException e) {
		log.error("Get submit infomation failed!" + e.toString());
		return "BAD";
	}

	bbPubKey = getBBPubKey(bb_id);
	
	gamestate = decompressGameState(new String(req.getParameter(ORP.GAME_STATE_TAG)));
	ip_address = req.getRemoteAddr();
	try {
		signature = HexString.fromHexString(req.getParameter(ORP.SIGNATURE_TAG));
	} catch (NumberFormatException e) {
		log.error("Bad signature");
		return "BAD";
	}

	return "OK";
    }

    private String processGameState()
    {
	// Process the game states according to the title_id
	String sc = getScore(titleID, contentID, gamestate, signature, bbPubKey);
	//log.error("Out from C Lib:" + sc);
	if (sc.indexOf(';') >= 0 ) {
		// Deal with the multi records game state;
		String lines[] = sc.split(";");
		for ( int i=0; i<lines.length; i++) {
			String values[] = lines[i].split("::");
			// log.error("Line:" + lines[i] + lines.length + "\n");
			column_id = new Integer(values[0]).intValue();
			score_desc = values[1];
			score = new Long(values[2]).longValue();
			result = updateScore();
		}
	} else {
		// Deal with the single record game state;
		String values[] = sc.split("::");
		column_id = new Integer(values[0]).intValue();
		score_desc = values[1];
		score = new Long(values[2]).longValue();
		result = updateScore();
	}
	return "OK";
    }
    
    private String updateScore()
    {
	// Update one score record OR insert one
	// when the record does not exist.
	// If the game state contains multi records,
	// updateScore should be called multi times;
	// All DB operation should be put here.
	OscDbConnection conn = null;
	PreparedStatement stmt = null;
	ResultSet rs = null;
	long old_score = 0;
	boolean NewRecord = true;
	boolean NeedPremium = false;
	try { // Get if it is a new record!
		conn = context.getDB(true);
		stmt = conn.prepareStatement(SQL_QUERY_COMPETITION_SCORE);
		stmt.setInt(1, column_id);
		stmt.setLong(2, membership_id);
		stmt.setLong(3, member_id);
		stmt.setLong(4, bb_id);

		rs = stmt.executeQuery();
		if (rs.next()) {
			NewRecord = false;
			old_score = rs.getLong(1);
		} else {
			NewRecord = true;
		}
		rs.close();
		conn.closeStatement(stmt);
		stmt=null;
		conn.close();
		conn=null;
	} catch (SQLException e) {
	        this.log.error("DB Query Failed " + SQL_QUERY_COMPETITION_SCORE + e.getMessage());
		return "FAILED";
	}

	try {
		// Get COMPETITION_ID, COLUMN_TYPE 
		conn = context.getDB(true);
		stmt = conn.prepareStatement(SQL_QUERY_COLUMN_TYPE);
		stmt.setInt(1, column_id);

		rs = stmt.executeQuery();
		int i = 0;
		if (rs.next()) {
			competition_id = rs.getInt(++i);
			column_type = rs.getString(++i);
			column_name = rs.getString(++i);
			rule_id = rs.getLong(++i);
		}
		rs.close();
		conn.closeStatement(stmt);
		stmt=null;
		conn.close();
		conn=null;
		NeedPremium = IsPremiumOn();
		result = DoPremiumOn(NeedPremium);
		if ( NewRecord ) { // Record does not exist 
			// DO INSERT
			conn = context.getDB(false);
			stmt = conn.prepareStatement(SQL_INSERT_COMPETITION_SCORE);
			stmt.setInt(1, column_id);
			stmt.setLong(2, membership_id);
			stmt.setLong(3, member_id);
			stmt.setLong(4, bb_id);
			stmt.setString(5, score_desc);
			stmt.setLong(6, score);
			
			stmt.executeUpdate();

			rs.close();
			conn.closeStatement(stmt);
			stmt=null;

			conn.commit();
			conn.close();
			conn=null;
		} else { // Record exist 
			if ((column_type.equals("A") && old_score < score) || 
					(column_type.equals("B") && old_score > score)) { // Need update
				conn = context.getDB(false);
				stmt = conn.prepareStatement(SQL_UPDATE_COMPETITION_SCORE);
				stmt.setString(1, score_desc);
				stmt.setLong(2, score);
				stmt.setLong(3, membership_id);
				stmt.setLong(4, member_id);
				stmt.setLong(5, bb_id);
				stmt.setLong(6, column_id);

				stmt.executeUpdate();
				conn.closeStatement(stmt);
				stmt=null;

				conn.commit();
				conn.close();
				conn=null;
			}
		}
	} catch ( SQLException e ) {
		try {
			conn.rollback();
		} catch (SQLException e1) {}
		log.error("Failed update game state record!" + e.getMessage());
		return "FAILED";
	} finally {
        	if (conn != null) {
	        conn.closeStatement(stmt);
	        conn.close();
	        }
	}
        return result;
    }
    private String logSubmitGameState(boolean success)
    {
        // Log the update event to DB
	// Even though it is failed
	// If the logSubmitGameState(false) 
	// it should be loged in OSC run log
	OscDbConnection conn = null;
	PreparedStatement stmt = null;
	try {
		conn = this.context.getDB(false);
		stmt = conn.prepareStatement(SQL_INSERT_COMPETITION_SCORE_LOG);
		stmt.setLong(1, membership_id);
		stmt.setLong(2, member_id);
		stmt.setInt(3, competition_id);
		stmt.setLong(4, bb_id);
		stmt.setString(5, ip_address);

		stmt.executeUpdate();
		conn.closeStatement(stmt);
		stmt=null;
		conn.commit();
	} catch (SQLException e) {
		try {
			conn.rollback();
		} catch (SQLException e1) {}
		log.error("Failed insert update log!\n" + e.getMessage());
		return "FAILED";
	} finally {
		if (conn!=null) {
			conn.closeStatement(stmt);
			conn.close();
		}
	}
	return OscStatusCode.SC_OK;
    }
    private boolean IsPremiumOn()
    {
	// Premium member points if the user's record
	// is a NEW HIGH RECORD.
	OscDbConnection conn = null;
	PreparedStatement stmt = null;
	ResultSet rs = null;
	long max_min_score = 0;
	boolean need_premium = false;

	try {
		conn = context.getDB(true);
		if (column_type.equals("A")) {
			stmt = conn.prepareStatement(SQL_QUERY_MAX_RECORD);
		} else {
			stmt = conn.prepareStatement(SQL_QUERY_MIN_RECORD);
		}
		stmt.setInt(1, column_id);
		rs = stmt.executeQuery();
	
		if ( rs.next()) {
			max_min_score = rs.getLong(1);
		}

		rs.close();
		conn.closeStatement(stmt);
		stmt = null;

		if ((column_type.equals("A") && score > max_min_score) ||
			(column_type.equals("B") && score < max_min_score )) {
			need_premium = true;
		}
	} catch (SQLException e) {
		log.error("Failed Query DB for Max/Min Score!\n" + e.getMessage());
	} finally {
		if (conn!=null) {
			conn.closeStatement(stmt);
			conn.close();
		}
	}
	return need_premium;
    }

    private String DoPremiumOn(boolean need)
    {
	OscDbConnection conn = null;
	CallableStatement cstmt = null;
        try {
        	String contents = context.readFile(context.getTxtPath(locale,"GamestateTemplete.txt"));
		String head_reason = contents.substring(0,contents.indexOf("?"));
		String tail_reason = contents.substring(contents.indexOf("?") + 1);;
		reason = head_reason + column_name + tail_reason;
	} catch (Exception e) {
        	log.error("Failed to read gamestate template: " + e.getMessage());
                //throw new InvalidRequestException("Failed to read gamestate template: " + e.getMessage());
        }
	
	if (need) {
		try {
			conn = context.getDB(false);
			cstmt = conn.prepareCall(SQL_UPDATE_POINT);
			cstmt.setLong(1,this.membership_id);
			cstmt.setLong(2,this.member_id);
			cstmt.setLong(3,this.rule_id);
			cstmt.setString(4,this.reason);
			cstmt.setString(5,this.ip_address);
			cstmt.execute ();
			conn.closeStatement(cstmt);
			cstmt = null;
			conn.commit();
		} catch (SQLException e) {
			try { conn.rollback(); } catch (SQLException e1) {}
			this.log.error("Failed add point for new record created!\n" + e.getMessage());
		} finally {
			if (conn!=null) {
				conn.closeStatement(cstmt);
				conn.close();
			}
		}
	}
	return "OK";
    }
    // --------------------------------------------
    // Specialization of abstract methods
    // --------------------------------------------

    public boolean requiresLogin() {return true;}

    public String jspFwdUrl()  {return null;}


    public void encodeXml(PrintWriter out) 
    {
        // Encodes the final results, where we here can be sure
        // the status and status message is correctly set.
        //
        out.println("<" + ORP.RPC_RESULT_TAG + '>');

        // First encode the action and the final error status
        //
        context.encodeXml_TNL(out, ORP.REQ_ACTION, rule.getActionNm());
        context.encodeXml_TNL(out, STATUS_TAG, status);
        context.encodeXml_TNL(out, STATUS_MSG_TAG, statusMsg);
        // BEGIN TEST CODE ( Uncomment the code for test )	
	//log.error("The Game Score ID is: " + column_id);
	//log.error("The Game Score Display is: " + score_desc);
	//log.error("The Game Score is: " + score);
	//log.error("Column Type is: " + column_type);
        // END TEST CODE
	// Then encode the remainder of the response as long as
        // there is no error status.
        //
        if (!getIsError()) {
            out.print(result);
        }

        // Close the root tag
        //
        out.println("</" + ORP.RPC_RESULT_TAG + '>');
    } // encodeXml


    public void encodeErrorXml(PrintWriter out, String oscStatusCode) 
    {
        out.println("<" + ORP.RPC_RESULT_TAG + '>');
        context.encodeXml_TNL(out, ORP.REQ_ACTION, rule.getActionNm());
        context.encodeXml_TNL(out, STATUS_TAG, oscStatusCode);
        context.encodeXml_TNL(out, STATUS_MSG_TAG, 
                              OscStatusCode.getMessage(oscStatusCode));
        out.println("</" + ORP.RPC_RESULT_TAG + '>');
    } // encodeErrorXml


    public void serveRequest(OscLoginSession loginSession)
        throws InvalidRequestException
    {
        if (loginSession.isLoggedIn()) {
            loginName = loginSession.getPseudonym();
            membership_id = loginSession.getMembershipID();
            //membership_id = 18863514321L;
            member_id = loginSession.getMemberID();
            //member_id = 1;
        }

	try {
		result = processGameState();
		result = logSubmitGameState(true);
	} catch (Exception e) {
		log.error("IO Exception!\n" + e.toString());
	}

    } // serveRequest


    public String toString() {return null;}

    public boolean getIsError() 
    {
        return !status.equals(OscStatusCode.SC_OK);
    }
    public String getErrorMsg()
    {
        return statusMsg;
    }
    public String getErrorCode()
    {
        return status;
    }

    public boolean getIsLoggedIn() { return (loginName != null); }
    public String getPseudonym() { return loginName; }

    // --------------------------------------------
    // Definition of result specific bean values used
    // for formatting a result or access through
    // JSP pages.
    // --------------------------------------------

} // class OscResultGameState

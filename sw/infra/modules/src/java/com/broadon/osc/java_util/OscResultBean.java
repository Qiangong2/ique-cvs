package com.broadon.osc.java_util;

import java.io.PrintWriter;

import com.broadon.exception.InvalidRequestException;
import com.broadon.util.VersionNumber;


/** The <code>OscResultBean</code> is the bean used by JSP pages
 *  to access the results of a request.  The result object will contain
 *  both parameters used in the request and the results from serving
 *  the request.  The concrete subclasses of this abstraction should be
 *  named <code>OscResultXxx</code> where <code>Xxx</code> reflects the
 *  type of result.
 *
 *  Where a JSP page needs access to more specific request data, it
 *  should define the "useBean" JSP directive to pick up the
 *  appropriate subclass of OscResultBean, using the corresponding uniqueID.
 *  This is intended for request-based sharing, not context or session based
 *  sharing.
 */
public abstract class OscResultBean
{
    protected static final int    BBCARD_BLKSIZE = (256 * 1024);
    protected static final String STATUS_TAG = "status";
    protected static final String STATUS_MSG_TAG = "status_msg";

    // shorthand convenience
    //
    protected static class ORP extends OscRequestParams {};


    public static final VersionNumber V_1_0_0 = VersionNumber.valueOf("1.0.0");

    // Should this service only be provided through an internal request?
    //
    public boolean requiresInternalRequest() {return false;}

    // Should this service only be provided through a secure request?
    //
    public boolean requiresSecureRequest() {return false;}

    // Redefine this to valid action name if you want to use one of the
    // special redirect jsp pages as the forwarding destination, such as
    // RedirectNonsecure.jsp.
    //
    public String redirectAction() {return null;}

    // Must the user be logged in to produce a valid result for this kind
    // of request?
    //
    public abstract boolean requiresLogin();

    // JSP forwarding path for result (null if none exists)
    //
    public abstract String jspFwdUrl();

    // Xml encoder
    // Override the error encoder if a different root tag or format is
    // desirable.
    //
    public abstract void encodeXml(PrintWriter out);
    public void encodeErrorXml(PrintWriter out, String oscStatusCode)
    {
        out.println("<" + ORP.RPC_RESULT_TAG + '>');
        OscContext.encodeXml_TNL(out, STATUS_TAG, oscStatusCode);
        OscContext.encodeXml_TNL(out, 
                                 STATUS_MSG_TAG, 
                                 OscStatusCode.getMessage(oscStatusCode));
        out.println("</" + ORP.RPC_RESULT_TAG + '>');
    }

    public abstract void serveRequest(OscLoginSession loginSession)
        throws InvalidRequestException;

    public String getPseudonym() {return null;}

    // The error status
    //
    public boolean getIsError() {return false;}
    public String getErrorMsg() 
    {
        return OscStatusCode.getMessage(getErrorCode());
    }
    public String getErrorCode() 
    {
        return OscStatusCode.SC_OK;
    }

    public long getBBCardBlockSize() 
    {
        return BBCARD_BLKSIZE;
    }

    // The login notice to the client
    //
    public boolean getDoLogin() {return false;}
} // class OscResultBean

package com.broadon.osc.java_util;

import java.io.*;
import javax.servlet.http.*;

import com.broadon.status.StatusCode;
import com.broadon.exception.InvalidRequestException;

/** The class used to serve VNG server request for game session queries
 *  and returns the appropriate document and header back to the VNG server.
 */

public class OscResultGameSessionQuery extends OscResultBean
{
    /** Rules for creating a Game Session Query result object
     */
    public static class Rule extends OscResultRule {
        
        public static final String GAME_SESSION_QUERY = "gsess_query";

        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
        }
        public boolean supports(ServerType t)
        {
            return t == ServerType.RPC;
        }
        public OscResultBean create(HttpServletRequest req,  
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
            return new OscResultGameSessionQuery(req, res, this);
        }
    } // class Rule


    // --------------------------------------------
    // Invariant values
    // --------------------------------------------

    private static final String QUERY_VN_ID = "vnid";
    public static final String TOTAL_COUNT_TAG = "total_count";

    // --------------------------------------------
    // Member variables
    // --------------------------------------------

    private final OscResultGameSessionQuery.Rule 	rule;
    private final OscContext          			context;
    private final OscLogger          			logger;
    private final String                        	actionName;
    private String					status;
    private String					statusMsg;

    private String					type;
    private String[]					vnid;
    private String					gsess_sort;
    private String					resultXML;
    private int						totalCount;

    private String					num_rows;
    private String					skip_rows;

    // ----------------------------------------------------------
    // Hidden constructors and helper methods (all constructors should
    // be hidden for result objects, since they are constructed by the
    // Rule class)
    // ---------------------------------------------------------

    private OscResultGameSessionQuery(HttpServletRequest  req,  
                                      HttpServletResponse res,
                                      OscResultGameSessionQuery.Rule rule)
        throws InvalidRequestException
    {
        this.rule = rule;
        context = rule.getContext();
        logger = context.getLogger();
        actionName = rule.getActionNm();

	status = OscStatusCode.SC_OK;
	statusMsg = StatusCode.getMessage(status);

        type = req.getParameter(ORP.GSESS_QUERY_TYPE);
	vnid = req.getParameterValues(ORP.GSESS_VNID);
	gsess_sort = req.getParameter(ORP.GSESS_SORT);
	num_rows = req.getParameter(ORP.GSESS_NUM_ROWS);
	skip_rows = req.getParameter(ORP.GSESS_SKIP_ROWS);
        totalCount = 0;
        resultXML = "";

        if (num_rows == null || num_rows.equals(""))
            num_rows = "-1";
        if (skip_rows == null || skip_rows.equals(""))
            skip_rows = "-1";

        if (type!=null && !type.equals(QUERY_VN_ID))
        {
            status = OscStatusCode.VNG_INVALID_GSESS_QUERY_ERROR;
            statusMsg = OscStatusCode.getMessage(OscStatusCode.VNG_INVALID_GSESS_QUERY_ERROR);
            logger.error(statusMsg + " for " + actionName + ", type = " + type);
            return;
        }

        if (type == null || type.equals(""))
        {
            status = OscStatusCode.OSC_BAD_REQUEST;
            statusMsg = OscStatusCode.getMessage(OscStatusCode.OSC_BAD_REQUEST);
            logger.error(statusMsg + " for " + actionName + ", type = " + type);
            return;

        } else if (vnid == null) {
            status = OscStatusCode.OSC_BAD_REQUEST;
            statusMsg = OscStatusCode.getMessage(OscStatusCode.OSC_BAD_REQUEST);
            logger.error(statusMsg + " for " + actionName + ", type = " + type +
                    " -- No VN_ID values submitted for game session query");
            return;
        }

    }

    // --------------------------------------------
    // Specialization of abstract methods
    // --------------------------------------------

    public boolean requiresLogin() {return true;}

    public String jspFwdUrl() {return null;}

    public void encodeXml(PrintWriter out)
    {
        // Encodes the final results
        //
        out.println("<" + ORP.RPC_RESULT_TAG + ">");

        context.encodeXml_TNL(out, ORP.REQ_ACTION, actionName);
        context.encodeXml_TNL(out, STATUS_TAG, status);
        context.encodeXml_TNL(out, STATUS_MSG_TAG, statusMsg);

        context.encodeXml_TNL(out, TOTAL_COUNT_TAG, String.valueOf(totalCount));

        if (!getIsError() && resultXML!=null && !resultXML.equals(""))
            out.println(resultXML);

        if (context.getRpcAppendParams().equals("ON"))
        {
            out.println("\t<" + ORP.RPC_PARAMS_TAG + ">");
            if (vnid != null)
            { 
                for (int i = 0; i < vnid.length; i++)
                    context.encodeXml_TTNL(out, ORP.GSESS_VNID, vnid[i]);
            }

            context.encodeXml_TTNL(out, ORP.GSESS_SORT, gsess_sort);
            context.encodeXml_TTNL(out, ORP.GSESS_NUM_ROWS, num_rows);
            context.encodeXml_TTNL(out, ORP.GSESS_SKIP_ROWS, skip_rows);
            out.println("\t</" + ORP.RPC_PARAMS_TAG + ">");
        }

        out.println("</" + ORP.RPC_RESULT_TAG + ">");
    } // encodeXml

    public void serveRequest(OscLoginSession loginSession)
        throws InvalidRequestException
    {
        if (!getIsError()) {
            OscDbConnection conn = null;
            int numrows = Integer.parseInt(num_rows);
            int skiprows = Integer.parseInt(skip_rows);

            try {
                conn = context.getDB(true);

                if (type.equals(QUERY_VN_ID))
                {
                    String sql = OscDmlGameSession.getGameSessionQueryVnidSQL(gsess_sort, vnid);
                    totalCount = OscDmlGameSession.countGameSession(conn, sql);
                    resultXML = OscDmlGameSession.queryGameSession(conn, sql, numrows, skiprows);
                }
            }
            catch (Exception e) {
                status = OscStatusCode.VNG_DB_ERROR;
                statusMsg = OscStatusCode.getMessage(OscStatusCode.VNG_DB_ERROR) +
                       ": " + e.getMessage();

                e.printStackTrace();
                logger.error(statusMsg + " for " + actionName);
            }
            finally {
                if (conn != null)
                    conn.close();
            }
        }
    }

    public String toString() {return null;}

    public boolean getIsError()
    {
        return !status.equals(OscStatusCode.SC_OK);
    }

    public String getErrorMsg()
    {
        return statusMsg;
    }

} // class OscResultGameSessionQuery

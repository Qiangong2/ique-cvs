package com.broadon.osc.java_util;

import com.broadon.exception.BroadOnException;


/**
 * The <code>OscException</code> class is the generic exception thrown
 * by the OSC server.  Typically subclasses of this is defined for 
 * specific exceptional situations.
 *
 * @version     $Revision: 1.1 $
 */
public class OscException extends BroadOnException
{
    /**
     * Constructs a InvalidRequestException instance.
     */
    public OscException()
    {
        super();
    }

    /**
     * Constructs an OscException instance.
     *
     * @param   message                 the exception message
     */
    public OscException(String message)
    {
        super(message);
    }

    /**
     * Constructs an OscException instance.
     *
     * @param   message                 the exception message
     * @param   throwable               the nested exception
     */
    public OscException(String message, Throwable throwable)
    {
        super(message, throwable);
    }
}

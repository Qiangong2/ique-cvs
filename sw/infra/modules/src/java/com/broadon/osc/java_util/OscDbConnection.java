package com.broadon.osc.java_util;
 
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

import javax.sql.DataSource;

import oracle.xml.sql.OracleXMLSQLException;
import oracle.xml.sql.dataset.OracleXMLDataSetExtJdbc;
import oracle.xml.sql.dml.OracleXMLSave;
import oracle.xml.sql.query.OracleXMLQuery;

import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/** A thin abstraction to DB connections, pretty much mirroring parts of
 *  the <code>Connection</code> interface, but also handling cleanup
 *  without exceptions, thus slightly simplifying the resulting code. Note
 *  that OSC DB access never uses auto-commit, and any changes to the DB
 *  must be followed by an explicit <code>commit()</code> or be carried 
 *  out by a <code>commitXxxx()</code> method.
 *
 *  Typical usage is as follows:
 *
 *  <code>
 *     OscDbConnection  conn = null;
 *     PreparedStatment stmt = null;
 *     try {
 *        conn = oscContext.getDB(false);
 *        ... <construct/issue queries> ...
 *        conn.commit();
 *     }
 *     catch (SQLException e) {
 *        ... <error reporting> ...
 *     }
 *     finally {
 *        if (conn != null) {
 *           conn.closeStatment(stmt);
 *           conn.close();
 *        }
 *     }
 *  </code>
 *
 */
public class OscDbConnection {

    // ------------------------------------
    // The private part
    // ------------------------------------

    private static final String DEFAULT_ROWSET_TAG = "ROWSET";

    private static final String TABLE_PK_SQL =
            "SELECT b.column_name" +
            "  FROM user_constraints a, user_cons_columns b, user_tables c" +
            " WHERE a.table_name = c.table_name" +
            "   AND a.constraint_type = 'P'" +
            "   AND a.constraint_name = b.constraint_name" +
            "   AND c.table_name = UPPER(?)" +
            " UNION " +
            "SELECT b.column_name" +
            "  FROM all_constraints a, all_cons_columns b, user_synonyms c" +
            " WHERE a.table_name = c.table_name" +
            "   AND a.constraint_type = 'P'" +
            "   AND a.owner = c.table_owner" +
            "   AND a.owner = b.owner" +
            "   AND a.constraint_name = b.constraint_name" +
            "   AND c.synonym_name = UPPER(?)" +
            " UNION " +
            "SELECT b.column_name" +
            "  FROM all_constraints a, all_cons_columns b, all_synonyms c" +
            " WHERE a.table_name = c.table_name" +
            "   AND a.constraint_type = 'P'" +
            "   AND a.owner = c.table_owner" +
            "   AND a.owner = b.owner" +
            "   AND a.constraint_name = b.constraint_name" +
            "   AND c.synonym_name = UPPER(?)" +
            "   AND c.owner = 'PUBLIC'";

    private OscLogger  log;
    private Connection conn;
    private Statement  lastStmt;
    private ResultSet  lastResult;
    
    private OscDbConnection(OscLogger log, DataSource db, boolean readonly)
        throws SQLException
    {
        try {
            lastStmt = null;
            lastResult = null;
            conn = db.getConnection();
            conn.setAutoCommit(false);
            conn.setReadOnly(readonly);
        }
        catch (SQLException e) {
            close();
            throw e;
        }
    }


    private void closeLastStmt()
    {
        closeStatement(lastStmt);
        lastStmt = null;
        lastResult = null;
    }


    private void checkConnection() throws SQLException
    {
        if (conn == null) {
            throw new SQLException("Attempt to access closed DB connection");
        }
    }

    /**
     * Obtains the primary key fields for a given table in the database.
     *
     * @param   tableName               Name of the table in the database
     * @return  string array of primary keys
     * @throws  SQLException
     */
    private String[] getPKcol(String tableName)
        throws SQLException
    {
        PreparedStatement cs = null;
        ResultSet rs = null;
        String [] ret = null;
        Vector retVector = new Vector();

        try
        {
            cs = conn.prepareStatement(TABLE_PK_SQL);
            cs.setString(1, tableName);
            cs.setString(2, tableName);
            cs.setString(3, tableName);
            rs = cs.executeQuery();
            for (int i = 0; rs.next(); i++) {
                retVector.addElement(rs.getString(1));
            }
            ret = new String[retVector.size()];
            retVector.copyInto(ret);
        } finally {
            if (rs!=null)
                rs.close();
            if (cs!=null)
                cs.close();
        }

        return ret;
    }

    /**
     * Verifies if the given xml string contains any valid data.
     *
     * @param   xml                     the XML string
     * @return  true = xml string is empty, false = xml string contains valid data
     *
     */
    private boolean isNullXML(String xml)
    {
        if (xml.length() <= 255)
        {
            if ((xml.length() == 0) ||
               (xml.equals("<ROWSET/>")) ||
               (xml.equals("<ROWSET></ROWSET>")) ||
               (xml.equals("<ROW/>")) ||
               (xml.equals("<ROW></ROW>")) ||
               (xml.equals("<ROWSET><ROW></ROW></ROWSET>")))
                return true;
        }
        return false;
    }

    /**
     * Inserts/Updates/Deletes a record in the database based on the given XML Document
     * and the DML Type.
     *
     * @param   xml                     the XML document that contains
     *                                  the information to be deleted
     * @param   tableName               Name of the table in the database
     * @param   dmlType                 "INS", "UPD" or "DEL"
     * @return  # of rows successfully modified
     * @throws  SQLException
     * @throws  IOException
     * @throws  SAXException
     */
    private int tableXDML(String xml, String tableName, String dmlType)
        throws SQLException, IOException, SAXException
    {
        OracleXMLSave sav = new OracleXMLSave(conn, tableName);
        int rows=0;

        if (isNullXML(xml)) return 0;

        String [] keyColNames = getPKcol(tableName.toUpperCase());

        try
        {
            if (keyColNames.length > 0)
            {
                sav.setDateFormat("yyyy.MM.dd HH:mm:ss");
                sav.setKeyColumnList(keyColNames);

                if (dmlType.equals("INS"))
                {
                    ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());
                    InputStream inStream = bais;
                    InputSource is = new InputSource(inStream);

                    DOMParser parser = new DOMParser();
                    parser.parse(is);

                    Document doc = parser.getDocument();
                    NodeList nlr = doc.getElementsByTagName("ROW");

                    if (nlr.getLength() > 0) {
                        Node nr = nlr.item(0);
                        NodeList nlrc = nr.getChildNodes();
                        String[] updateCol = new String[nlrc.getLength()];
                        Node nrc;

                        for (int i=0; i < nlrc.getLength(); i++)
                        {
                            nrc = nlrc.item(i);
                            if (!(nrc.getNodeName().equals("ROWSET")) && !(nrc.getNodeName().equals("ROW"))) {
                                updateCol[i] = nrc.getNodeName();
                            }
                        }
                        sav.setUpdateColumnList(updateCol);
                        rows = sav.insertXML(xml);
                    }
                }
                if (dmlType.equals("UPD"))
                    rows = sav.updateXML(xml);

                if (dmlType.equals("DEL"))
                    rows = sav.deleteXML(xml);
            }
        } finally {
            sav.close();
        }

        return rows;
    }

    // ------------------------------------
    // The public interface
    // ------------------------------------

    
    /** Creates a new read-write database connection from the connection
     *  pool.  Only <code>commit()</code> read-write connections, since
     *  a read-only connection cannot change the DB.
     *
     * @return A new logical OscDbConnection, which will be invalid if the
     *  new connection could not be created due to an error.
     * @throws SQLException when the connection cannot be created
     */
    static public OscDbConnection create(OscLogger  log, 
                                         DataSource db,
                                         boolean    readonly)
        throws SQLException
    {
        return new OscDbConnection(log, db, readonly);
    }


    /** Cleans up the DB resources used by this connection, and invalidates
     *  this connection object.  Any uncommitted updates will be rolled back.
     *  Always call this method before abandoning an OscDbConnection object,
     *  or the DB resources will not be released until garbage collection
     *  occurs.
     */
    public void close()
    {
        if (conn != null) {
            closeLastStmt();
            try {
                try {conn.rollback();} catch (Throwable e2) {}
                conn.close();
            } catch (SQLException e) {
                log.warn("Failed to close DB connection");
            }
            conn = null;
	    }
	}


    /** Use this to create a prepared stmt to be issued to issue requests
     *  to the DB.
     *
     *  @return the PreparedStatment or null in the case of an exception
     *  @throws SQLException
     */
    public PreparedStatement prepareStatement(String sql) throws SQLException
    {
        checkConnection();
        return conn.prepareStatement(sql);
    }

    /** Use this to create a callable cstmt to be issued to issue requests
     *  to the DB.
     *
     *  @return the CallableStatment or null in the case of an exception
     *  @throws SQLException
     */
    public CallableStatement prepareCall(String sql) throws SQLException
    {
        checkConnection();
        return conn.prepareCall(sql);
    }

    /** Frees up resources, usually for a PreparedStatement
     */
    public void closeStatement(Statement s)
    {
        if (s != null) {
            try {
                s.close();
            }
            catch (SQLException e) {
                log.warn("Failed to close DB statement" + 
                         e.getMessage());
            }
        }
    }


    /** Use this to issue a select statement to the DB without parameters.
     *
     *  @return the ResultSet
     *  @throws SQLException
     */
    public ResultSet query(String q) throws SQLException
    {
        checkConnection();
        closeLastStmt();
        lastStmt = conn.createStatement();
        lastResult = lastStmt.executeQuery(q);
        return lastResult;
    }


    /** Use this to issue an insert, update, or delete statement to the DB,
     *  without parameters, and make sure to call <code>commit()<code> if 
     *  you want the changes to persist.
     *
     *  @throws SQLException
     */
    public void update(String u) throws SQLException
    {
        checkConnection();
        closeLastStmt();
        try {
            lastStmt = conn.createStatement();
            lastStmt.executeUpdate(u);
        }
        finally {
            closeLastStmt(); // No results
        }
    }

    /** Commit any DB changes issued through prepared statements or the
     *  above query/update methods.
     *
     *  @throws SQLException
     */
    public void commit() throws SQLException
    {
        checkConnection();
        conn.commit();
    }

    /** Rollback any DB changes issued through prepared statements or the
     *  above query/update methods.
     *
     *  @throws SQLException
     */
    public void rollback() throws SQLException
    {
        checkConnection();
        conn.rollback();
    }

    /** Set this connection's auto-commit mode to the given state.
     *
     *  @throws SQLException
     */
    public void setAutoCommit(boolean autoCommit) throws SQLException
    {
	checkConnection();
	conn.setAutoCommit(autoCommit);
    }

    /** Creates a temporary oracle.sql.CLOB object
     *
     *  @throws SQLException
     */
    public oracle.sql.CLOB createTemporaryClob(boolean flag, int dur) throws SQLException
    {
        checkConnection();
        return oracle.sql.CLOB.createTemporary(conn, flag, dur);
    }


    /** Use this to issue a sequence of insert, update, or delete statements
     *  to the DB and to immediately have such DB changes be committed.  There
     *  is no need to call <code>commit</code> after a call to this method.
     *
     *  @throws SQLException
     */
    private void commitBatch(String[] stmts) throws SQLException
    {
        checkConnection();
        closeLastStmt();
        try {
            int i;
            lastStmt = conn.createStatement();
            for (i = 0; i < stmts.length; ++i) {
                lastStmt.addBatch(stmts[i]);
            }

            int[] results = lastStmt.executeBatch();
            for (i = 0; i < stmts.length; ++i) {
                if (results[i] == Statement.EXECUTE_FAILED)
                    throw new SQLException("Failed to execute SQL \" " +
                                           stmts[i] + "\" in batch");
            }
            conn.commit();
        }
        finally {
            closeLastStmt(); // No results
        }
    }

    /**
     * Queries the records identified by the given SQL statement.
     * This is a generic query, and the returning format is XML.
     *
     * @param   sql                     SQL statement
     * @return  The XML document that represent the qualified
     *          records, or null if none found.
     */
     public String queryXML(String sql)
        throws SQLException
     {
        return queryXML(sql, -1, -1, DEFAULT_ROWSET_TAG);
     }
     
    /**
     * Queries the records identified by the given SQL statement.
     * This is a generic query, and the returning format is XML.
     *
     * @param   sql                     SQL statement
     * @param   rowsetTag               The rowset tag
     * @return  The XML document that represent the qualified
     *          records, or null if none found.
     */
     public String queryXML(String sql, String rowsetTag)
        throws SQLException
     {
        return queryXML(sql, -1, -1, rowsetTag);
     }
     
     /**
      * Return the private object 'conn'.
      *
      * @return  The returned result is a Connection type object.
      */
      public Connection getConnection()
      {
         return this.conn;
      }

    /**
     * Queries the records identified by the given SQL statement
     * based on numrows and skiprows.
     *
     * @param   sql                     SQL statement
     * @param   numrows                 # of rows to include in the result
     *                                  -1 defaults to all
     * @param   skiprows                # of rows to skip in the result
     *                                  -1 defaults to none
     * @return  The XML document that represent the qualified
     *          records, or null if none found.
     */
     public String queryXML(String sql, int numrows, int skiprows)
        throws SQLException
     {
        return queryXML(sql, numrows, skiprows, DEFAULT_ROWSET_TAG);

     }

    /**
     * Queries the records identified by the given SQL statement
     * based on numrows and skiprows.
     *
     * This is a generic query, and the returning format is XML.
     *
     * @param   sql                     SQL statement
     * @param   numrows                 # of rows to include in the result
     *                                  -1 defaults to all
     * @param   skiprows                        # of rows to skip in the result
     *                                  -1 defaults to none
     * @param   rowsetTag               The rowset tag
     * @return  The XML document that represent the qualified
     *          records, or null if none found.
     */
     public String queryXML(String sql, int numrows, int skiprows, String rowsetTag)
        throws SQLException
     {
        String xml = null;
        OracleXMLDataSetExtJdbc dset = null;
        OracleXMLQuery qry = null;

        try {
            dset = new OracleXMLDataSetExtJdbc(conn, sql);
            qry = new OracleXMLQuery(dset);

            if (numrows >= 0)
                qry.setMaxRows(numrows);
            if (skiprows >= 0)
                qry.setSkipRows(skiprows);
            qry.useNullAttributeIndicator(true);
            qry.setRowsetTag(rowsetTag);
            qry.setRaiseException(true);
            qry.setDateFormat("yyyy.MM.dd HH:mm:ss");
            xml = qry.getXMLString();

       } catch (OracleXMLSQLException e) {
            throw new SQLException(e.toString());
       } catch (Exception e) {
            throw new SQLException(e.toString());
       } finally {
            if (dset != null) dset.close();
            if (qry != null) qry.close();
       }
       return xml;
    }

    /**
     * Counts the number of records identified by the given SQL statement.
     *
     * @param	sql			the SQL statement that counts
     *
     * @return  # of records.
     *
     * @throws	SQLException
     */
    public int GetRecordCount(String sql) throws SQLException
    {
        PreparedStatement statement = null;
        int ret=0;
	try
	{
   	    int indFrom = sql.toUpperCase().indexOf("FROM");
           if(indFrom == -1)
           {
              return -1;
           }    
           String qq = "SELECT COUNT(*) " + sql.substring(indFrom);
           statement = conn.prepareStatement(qq);

	    ResultSet	resultSet = statement.executeQuery();

	    if (resultSet == null || !resultSet.next())
	    {
		/*
		 * No record.
		 */
		ret = 0;
	    } else 
	        ret = resultSet.getInt(1);
	}
	finally
	{
	    /*
	     * Close the statment after execution.
	     *
	     * The corresponding ResultSet will also be
	     * closed internally.
	     */
            if (statement!=null)
                statement.close();
	}

        return ret;
    }

    /**
     * Inserts a record in the database based on the given XML Document.
     *
     * @param   xml                     the XML document that contains
     *                                  the information to be inserted
     * @param   tableName               Name of the table in the database
     * @return  # of rows successfully inserted
     * @throws  SQLException
     * @throws  IOException
     * @throws  SAXException
     */
    public int insertXML(String xml, String tableName)
        throws SQLException, IOException, SAXException
    {
        return tableXDML(xml, tableName, "INS");
    }

    /**
     * Updates a record in the database based on the given XML Document.
     *
     * @param   xml                     the XML document that contains
     *                                  the information to be updated
     * @param   tableName               Name of the table in the database
     * @return  # of rows successfully updated
     * @throws  SQLException
     * @throws  IOException
     * @throws  SAXException
     */
    public int updateXML(String xml, String tableName)
        throws SQLException, IOException, SAXException
    {
        return tableXDML(xml, tableName, "UPD");
    }

    /**
     * Inserts/Updates records in the database based on the given XML Document.
     *
     * @param   xml                     the XML document that contains
     *                                  the information to be inserted or updated
     * @param   tableName               Name of the table in the database
     * @return  # of rows successfully inserted/updated
     * @throws  SQLException
     * @throws  IOException
     * @throws  SAXException
     */
    public int insertupdateXML(String xml, String tableName)
        throws SQLException, IOException, SAXException
    {
        int rows = 0;
        rows = updateXML(xml, tableName);
        if (rows == 0)
            rows = insertXML(xml, tableName);
        return rows;
    }

    /**
     * Deletes a record in the database based on the given XML Document.
     *
     * @param   xml                     the XML document that contains
     *                                  the information to be deleted
     * @param   tableName               Name of the table in the database
     * @return  # of rows successfully deleted
     * @throws  SQLException
     * @throws  IOException
     * @throws  SAXException
     */
    public int deleteXML(String xml, String tableName)
        throws SQLException, IOException, SAXException
    {
        return tableXDML(xml, tableName, "DEL");
    }

} // class OscDbConnection

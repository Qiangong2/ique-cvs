package com.broadon.osc.java_util;

import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.broadon.exception.InvalidRequestException;


/** The class used to create the results associated with a selected
 *  title to purchase request, such that the appropriate document and header can be
 *  created in the response back to the client
 */
public class OscResultPurchaseTitle extends OscResultBean
{
    /** Rules for creating a PurchaseTitle result object
     */
    public static class Rule extends OscResultRule {

        public static final String PURCHASE_TITLE = "purchase_title";
        public static final String TRIAL_TITLE = "trial_title";
        public static final String FREE_TITLE = "free_manual";

        // An enumeration of the types of PurchaseTitles we support
        //
        public static class TitleType {
            private int enumeration;
            private TitleType(int e) {enumeration = e;}
            public int intValue() {return enumeration;}
            public static final TitleType PURCHASE = new TitleType(0);
            public static final TitleType TRIAL = new TitleType(1);
            public static final TitleType FREE = new TitleType(2);
        }

        private TitleType tp;

        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
            if (nm.equals(PURCHASE_TITLE)) tp = TitleType.PURCHASE;
            else if (nm.equals(TRIAL_TITLE)) tp = TitleType.TRIAL;
            else if (nm.equals(FREE_TITLE)) tp = TitleType.FREE;
            else {
                context.getLogger().error("Invalid name ("  + nm +
                                          ") in " + getClass().getName() +
                                          " using " + FREE_TITLE);
                tp = TitleType.FREE;
            }
        }
        public TitleType getTitleType() {return tp;}
        public boolean supports(ServerType t)
        {
            return t == ServerType.HTM;
        }
        public OscResultBean create(HttpServletRequest req,  
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
            return new OscResultPurchaseTitle(req, res, this);
        }
    } // class Rule


    // --------------------------------------------
    // Invariant values
    // --------------------------------------------

    private static final String PURCHASE_TITLE_JSP = "PurchaseTitleDetail.jsp";
    private static final String SELECT_PURCHASE_TITLE = 
                "SELECT a.title, a.title_id, a.title_contents, a.eunits,a.title_size/("+BBCARD_BLKSIZE+") blocks, "+
                "a.rtype, a.trial_limits, a.description, a.expanded_information as detail_url, " +
                "b.title_id as manual_title_id, b.title_contents as manual_title_contents, "+
                "b.eunits as manual_eunits, b.title_size/("+BBCARD_BLKSIZE+") as manual_blocks "+
                "FROM CURRENT_CONTENT_CATALOG_V a, CURRENT_CONTENT_CATALOG_V b ";

    private static final String SELECT_TITLE_NAME = 
                "SELECT title FROM CURRENT_CONTENT_CATALOG_V";

    private static final String TRIAL_TITLE_JSP = "TrialTitleDetail.jsp";
    private static final String FREE_MANUAL_JSP = "FreeManualDetail.jsp";

    private static final String cacheEcardTypesKey = "ecardTypes";
    private static final String SELECT_ECARD_TYPES =  
                "SELECT ecard_type FROM ECARD_TYPES";

    private static final String FORWARDED_ERRMSG = "errmsg";

    // Thread shared cached value;  If further optimization is necessary,
    // try to use thread local versions of the unsynchronized HashMap.
    //
    private static final OscCachedMap cachedTitles = new OscCachedMap(100);

    // Arrays of the above indexed by Rule.TitleType.toInt() values
    //
    private static final String[] jspPage = {
        PURCHASE_TITLE_JSP,
        TRIAL_TITLE_JSP,
        FREE_MANUAL_JSP
    };

    // --------------------------------------------
    // Member variables
    // --------------------------------------------

    private final OscResultPurchaseTitle.Rule  rule;
    private final OscContext            	   context;
    private final OscLogger          		   logger;
    private final String                       actionName;
    private final Integer                      storeID;
    private String                             autoDownload;
    private String                             titleID;
    private String                             manualID;
    private String                             titleXML;
    private String                             errMsg;
    private String                             trial;
    private Vector                             ecardTypes;

    private String                             titleName;

    // --------------------------------------------
    // Hidden constructors (all constructors should
    // be hidden for result objects, sicne they are
    // constructed by the Rule class)
    // --------------------------------------------

    private OscResultPurchaseTitle(HttpServletRequest  req,  
                                   HttpServletResponse res,
                                   OscResultPurchaseTitle.Rule rule)
        throws InvalidRequestException
    {
        titleID = req.getParameter(OscRequestParams.REQ_TITLE_ID).trim();
        autoDownload = req.getParameter(OscRequestParams.REQ_AUTO_DOWNLOAD);
        trial = req.getParameter(OscRequestParams.REQ_TRIAL);
        this.rule = rule;
        context = rule.getContext();
        logger = context.getLogger();
        actionName = rule.getActionNm();
        storeID = context.getStoreId(req, context.getRequestVersion(req));
        titleXML = "";
        ecardTypes = null;
        manualID = titleID.substring(0,titleID.length()-1) + "9";
 
        if (titleID == null || titleID.equals("")) {
            throw new InvalidRequestException("Unable to obtain Title ID for purchase from request");
        } 

        errMsg = req.getParameter(FORWARDED_ERRMSG);
        if (errMsg != null &&
            errMsg.charAt(0) == '\'' && 
            errMsg.charAt(errMsg.length()-1) == '\'') {
            errMsg = errMsg.substring(1, errMsg.length()-1);
        }

    }


    // --------------------------------------------
    // Specialization of abstract methods
    // --------------------------------------------

    public boolean requiresLogin() 
    {
     //    if (rule.getTitleType().intValue() == 0 && !getIsTrial()) 
     //        return false;
     //    else
     //        return true;

         return false; 
    }

    public String jspFwdUrl() 
    {
        return jspPage[rule.getTitleType().intValue()];
    }

    public void encodeXml(PrintWriter out) {};

    public void serveRequest(OscLoginSession loginSession)
        throws InvalidRequestException
    {
        OscDbConnection conn = null;
        try {
            final long      timeout = context.getContentCacheTimeoutMsecs();
            final Integer   regionID = context.getStoreRegionId(storeID);
            OscCachedObject cache = null;
            OscCachedObject cacheEcardTypes = null;
            String          xml = null;
            Vector          types = null;

            // First, try to use cached value
            //
            if (timeout > 0) {
                final String cacheKey = regionID.toString() + ":" + titleID;

                cache = cachedTitles.getMapped(cacheKey);
                if (cache.isValid(timeout)) {
                    if (logger.isTracing())
                        logger.info("Using CACHED " + actionName + 
                                    " for (region:title)=" + cacheKey);
                    xml = (String)cache.getCached();
                }

                cacheEcardTypes = cachedTitles.getMapped(cacheEcardTypesKey);
                if (cacheEcardTypes.isValid(timeout)) {
                    if (logger.isTracing())
                        logger.info("Using CACHED " + actionName + 
                                    " for (ecardTypes)=" + cacheEcardTypesKey);
                    types = (Vector)cacheEcardTypes.getCached();
                }
            }

            // If the value was not cached, access the DB
            //
            if (xml == null || types == null) {
                conn = context.getDB(false);
                final String sql = SELECT_PURCHASE_TITLE +
                    " WHERE substr(a.title_id,1,4) = substr(b.title_id(+),1,4) " +
                    " AND a.region_id = " + regionID + 
                    " AND a.title_id = " + titleID +
                    " AND b.region_id(+) = " + regionID +
                    " AND b.title_id(+) = " + manualID;

                xml = conn.queryXML(sql);
                if (cache != null)
                    cache.setCached(xml);

                types = new Vector();
                ResultSet rs = conn.query(SELECT_ECARD_TYPES);
                while (rs.next())
                    types.addElement(new String(rs.getString(1)));
 
                if (cacheEcardTypes != null)
                    cacheEcardTypes.setCached(types);
            }
            titleXML = xml;
            ecardTypes = types;
        }
        catch (Exception e) {
            final String msg = 
                "Failed to obtain purchase title details TitleID=" + 
                titleID;
            logger.error(msg + " Error:" + e.getMessage());
            throw new InvalidRequestException(msg);
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public String getTitleName()
    {
        String          s = null;
        OscDbConnection conn = null;
        try {
            // Get the title name from the DB.
            //
            String sql_tname = (SELECT_TITLE_NAME +
                                " WHERE region_id = " + 
                                context.getStoreRegionId(storeID) + 
                                " AND title_id = " + titleID);

            conn = context.getDB(true);
            ResultSet rs = conn.query(sql_tname);
            if (rs.next()) {
                s = rs.getString(1);
            }
            rs.close();
        }
        catch (SQLException e) {
            logger.error("Failed to get title name in IQUE DB: " +
                   e.getMessage());
        }
        catch (Exception e) {
            logger.error("Failed to get title name in IQUE DB: " +
                   e.getMessage());
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return s;
    } // getTitleName

    public String toString() {return null;}

    // --------------------------------------------
    // Definition of result specific bean values used
    // for formatting a result or access through
    // JSP pages.
    // --------------------------------------------

    public String transformXML(String xmlStr, String xslFile, String locale)
    { return context.transformXML(xmlStr,xslFile,locale); }

    public String escQuote(String var) { return context.escQuote(var);}

    public String getTitleID() {return titleID;}
    public Vector getEcardTypes() {return ecardTypes;}
    public boolean getAutoDownload() {return (autoDownload!=null && autoDownload.toLowerCase().equals("yes"));}
    public boolean getIsTrial() {return (trial!=null && trial.toLowerCase().equals("yes"));}
    public String getXML() {return titleXML;}

} // class OscResultPurchaseTitle


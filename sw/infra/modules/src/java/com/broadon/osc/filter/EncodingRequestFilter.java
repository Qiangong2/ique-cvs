package com.broadon.osc.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class EncodingRequestFilter implements Filter {

	private final String DEFAULT_CHARSET = "utf-8";

	private FilterConfig config;

	private String charset;

	public void init(FilterConfig filterConfig) throws ServletException {
		this.config = filterConfig;
		this.charset = this.config.getInitParameter("charset");
		if (this.charset == null || "".equals(this.charset))
			this.charset = DEFAULT_CHARSET;

	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		request.setCharacterEncoding(this.charset);
		chain.doFilter(request,response);
	}

	public void destroy() {
		this.config = null;
		this.charset = null;
	}

}

package com.broadon.osc.java_util;

import java.util.Date;

/** A fully synchronized wrapper allowing thread shared access to cached
 *  data in the OSC server.  Typically variables of this value are
 *  declared as static or as members of the servlet.  For a table of
 *  thread shared objects, make this the value in the synchronized table.
 *
 *  Note that "isValid" and "getCached" are not one atomic operation, 
 *  since we do not care if the cached value changes between the two 
 *  operations.
 */
public class OscCachedObject
{
    private Object cached;
    private long   atMsecsTime;

    /** Initial value is a null object where isValid is false
     */
    public OscCachedObject()
    {
        cached = null;   // Invalid value
        atMsecsTime = 0; // Beginning of time as we know it
    }

    /** Initial value is a given object cached at the given time
     */
    public OscCachedObject(Object cached)
    {
        this.cached = cached;
        atMsecsTime = (new Date()).getTime();
    }

    public synchronized boolean isValid(long timeoutMsecs)
    {
        return (((new Date()).getTime() - timeoutMsecs) <= atMsecsTime);
    }
    
    public synchronized Object getCached() 
    {
        return cached;
    }

    /** This must always create a valid cached value where nowMsecs is always
     *  larger than or equal to the previous nowMsecs.
     */
    public synchronized void setCached(Object cached)
    {
        this.cached = cached;
        atMsecsTime = (new Date()).getTime();
    }

} // OscCachedObject

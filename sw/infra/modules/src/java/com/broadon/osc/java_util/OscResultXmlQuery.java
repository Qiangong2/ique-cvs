package com.broadon.osc.java_util;

import java.io.PrintWriter;

import javax.servlet.http.*;

import com.broadon.exception.InvalidRequestException;
import com.broadon.status.StatusCode;

/** The class used to serve VNG server request for adhoc SQL query
 *  and return the resultset in XML document 
 *  back to the VNG server.
 */

public class OscResultXmlQuery extends OscResultBean
{
    /** Rules for creating a XmlQuery result object
     */
    public static class Rule extends OscResultRule {
        
        public static final String XMLQUERY = "xmlquery";

        // An enumeration of the types of xmlquery we support
        //
        public static class ListType {
            private int enumeration;
            private ListType(int e) {enumeration = e;}
            public int intValue() {return enumeration;}
            public static final ListType XMLQUERY = new ListType(0);
        }
        
        private ListType tp;
        
        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
            if (nm.equals(XMLQUERY)) tp = ListType.XMLQUERY;
            else {
                context.getLogger().error("Invalid name ("  + nm +
                                          ") in " + getClass().getName() +
                                          " using " + XMLQUERY);
                tp = ListType.XMLQUERY;
            }
        }
        public boolean supports(ServerType t)
        {
            return t == ServerType.RPC;
        }
        public ListType getListType() {return tp;}
        public OscResultBean create(HttpServletRequest req,  
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
            return new OscResultXmlQuery(req, res, this);
        }
    } // class Rule


    // --------------------------------------------
    // Invariant values
    // --------------------------------------------

    private static final boolean[] needInternal = {
	true
    };
    private static final boolean[] needLogin = {
	false
    };


    // --------------------------------------------
    // Member variables
    // --------------------------------------------

    private final OscResultXmlQuery.Rule 	rule;
    private final OscContext          		context;
    private final OscLogger          		logger;
    private final String                        actionName;
    private String                              resultXML;
    private String				status;
    private String				statusMsg;

    private String				sqlstmt;


    // ----------------------------------------------------------
    // Hidden constructors and helper methods (all constructors should
    // be hidden for result objects, since they are constructed by the
    // Rule class)
    // ---------------------------------------------------------

    private OscResultXmlQuery(HttpServletRequest  req,  
                                       HttpServletResponse res,
                                       OscResultXmlQuery.Rule rule)
        throws InvalidRequestException
    {
        this.rule = rule;
        context = rule.getContext();
        logger = context.getLogger();
        actionName = rule.getActionNm();

        resultXML = "";
	status = OscStatusCode.SC_OK;
	statusMsg = StatusCode.getMessage(status);

	sqlstmt = req.getParameter(ORP.SQLSTMT);

        if (sqlstmt == null || sqlstmt.equals(""))
        {
            status = OscStatusCode.OSC_BAD_REQUEST;
            statusMsg =
                OscStatusCode.getMessage(
                    OscStatusCode.OSC_BAD_REQUEST);
            logger.error(statusMsg + " for " + req.getRequestURI() +
                        "?OscAction="+actionName + ": sqlstmt = " +
                        sqlstmt);
            return;
        }
    }


    public boolean requiresInternalRequest() {return needInternal[rule.getListType().intValue()];}

    // --------------------------------------------
    // Specialization of abstract methods
    // --------------------------------------------

    public boolean requiresLogin() {return needLogin[rule.getListType().intValue()];}

    public String jspFwdUrl() 
    {
        return null;
    }

    public void encodeXml(PrintWriter out)
    {
        // Encodes the final results
        //
        out.println("<" + ORP.RPC_RESULT_TAG + ">");

        context.encodeXml_TNL(out, ORP.REQ_ACTION, rule.getActionNm());
        context.encodeXml_TNL(out, STATUS_TAG, status);
        context.encodeXml_TNL(out, STATUS_MSG_TAG, statusMsg);

        if (!getIsError())
            out.println(resultXML);

        if (context.getRpcAppendParams().equals("ON"))
        {
            out.println("\t<" + ORP.RPC_PARAMS_TAG + ">");
	    context.encodeXml_TTNL(out, ORP.SQLSTMT, sqlstmt);
            out.println("\t</" + ORP.RPC_PARAMS_TAG + ">");
        }

        out.println("</" + ORP.RPC_RESULT_TAG + ">");
    } // encodeXml

    public void serveRequest(OscLoginSession loginSession)
        throws InvalidRequestException
    {
        if (!getIsError()) {
            OscDbConnection conn = null;

            try {
                final int listIdx = rule.getListType().intValue();

                final String sql = sqlstmt ;
                conn = context.getDB(true);

                resultXML = conn.queryXML(sql);
            }
            catch (Exception e) {
                status = OscStatusCode.VNG_DB_ERROR;
                statusMsg = OscStatusCode.getMessage(OscStatusCode.VNG_DB_ERROR) +
                       ": " + e.getMessage();
                e.printStackTrace();
                logger.error(statusMsg + " for " + actionName + " Error=" + e.getMessage());
            }
            finally {
                if (conn != null)
                    conn.close();
            }
        }
    }

    public String toString() {return null;}

    public boolean getIsError()
    {
        return !status.equals(OscStatusCode.SC_OK);
    }

    public String getErrorMsg()
    {
        return statusMsg;
    }

} // class OscResultXmlQuery

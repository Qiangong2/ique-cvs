package com.broadon.osc.java_util;

import java.io.PrintWriter;
import java.security.NoSuchAlgorithmException;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.broadon.exception.InvalidRequestException;


public class OscResultMemberPointTransfer extends OscResultBean {
    
    /** Rules for creating a Login result object
     */
    public static class Rule extends OscResultRule {

        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
        }
        public boolean supports(ServerType t)
        {
            return t == ServerType.HTM;
        }
        public OscResultBean create(HttpServletRequest req,
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
            return new OscResultMemberPointTransfer(req, res, this);
        }
    } // class Rule       
    
    
    private static final String REDIRECT_HOME_ACTION   			   = "login_form";
    private static final String MEMBER_POINT_TRANSFER_FORM_JSP     = "MemberPointTransferForm.jsp";
    private static final String MEMBER_POINT_TRANSFER_CONFIRM_JSP  = "MemberPointTransferConfirm.jsp";
    private static final String MEMBER_POINT_TRANSFER_SUCCESS_JSP  = "MemberPointTransferSucc.jsp";
    
    private static final String SQL_QUERY_TRANSFERABLE_POINTS =
        "{? = call IQUE_MEMBER_POINT_PKG.GET_TRANSFERABLE_POINTS(?, ?)}";
    
    private static final String SQL_QUERY_USER_PIN =
        "SELECT * FROM ique_members WHERE membership_id=? AND member_id=? AND pin=?";
    
    private static final String SQL_QUERY_TRANSFERED_PSEUDONYM = 
        "SELECT membership_id,member_id,name FROM ique_members WHERE pseudonym=?";
    
    private static final String SQL_CALL_TRANS_POINTS =
        "{call IQUE_MEMBER_POINT_PKG.TRANSFER(?,?,?,?,?,?)}";
     
    private final OscResultMemberPointTransfer.Rule    	rule;
    private final OscContext                			context;
    private final OscLogger                 			log;
    private final HttpServletRequest 					request;
    
    private String  failureMsg;
    private String  error_code;
    private String  redirect_jsp;
    
    private String 	pseudonym;
    private String  to_name;
    private String 	pin;
    private String  isCancel;
    
    private long 	to_membership_id;
    private long 	to_member_id;    
    private long 	transfered_points;
    private long	transferable_points;
    private int 	point_transfer_phase;  
    
    
    private OscResultMemberPointTransfer(HttpServletRequest  req,
            					 	     HttpServletResponse res,
            					 	     OscResultMemberPointTransfer.Rule rule)
    	throws InvalidRequestException
    {
        this.rule 		= rule;
        this.context 	= rule.getContext();
        this.log 		= this.context.getLogger();
        this.request 	= req;
        
        this.point_transfer_phase = 0;        
        
        if(this.request.getParameter("point_transfer_phase")!=null)
        {
           if(this.request.getParameter("point_transfer_phase").equals("1"))
           {
               this.point_transfer_phase = 1;
           }else if(this.request.getParameter("point_transfer_phase").equals("2")){
               this.point_transfer_phase = 2;
           }
        }
         
        switch(this.point_transfer_phase)
        {
            case 1:
                try{
                    
                    this.transfered_points = ( this.request.getParameter("transfered_points")!=null &&                
		  	             				  	  !this.request.getParameter("transfered_points").equals(""))&&
		  	             				  	  OscContext.isDigital(this.request.getParameter("transfered_points"))
		  	             				  	  ?Long.parseLong(this.request.getParameter("transfered_points")):0;  
		  	        if (this.transfered_points==0) {
		  	            this.failureMsg = "The transfered points is null or zero or not integer.";
		  	            this.error_code = "45";
		  	            throw new Exception("");
		  	        }

		  	        this.pseudonym = ( this.request.getParameter("pseudonym")!=null &&                
		  	            		  	  !this.request.getParameter("pseudonym").trim().equals(""))
		  	            		  	  ?this.request.getParameter("pseudonym").trim():null;
                    if (this.pseudonym==null) {
                        this.failureMsg = "The pseudonym is null";
                        this.error_code = "46";
                       	throw new Exception("");
                    }

                    this.pin = ( this.request.getParameter("pin")!=null &&                
                            	!this.request.getParameter("pin").equals(""))
                            	?this.request.getParameter("pin"):null;
                    if (this.pin==null) {
                        this.failureMsg = "The pin is null";
                        this.error_code = "17";
                        throw new Exception("");
                    }
                }catch(Exception e){}
                
                break;
                
            case 2:
                this.transfered_points = ( this.request.getParameter("transfered_points")!=null &&                
     				  	  				  !this.request.getParameter("transfered_points").equals(""))&&
     				  	  				   OscContext.isDigital(this.request.getParameter("transfered_points"))
     				  	  				   ?Long.parseLong(this.request.getParameter("transfered_points")):0;
            		  	  					 
            	this.to_membership_id	=	( this.request.getParameter("to_membership_id")!=null &&                
            	        					 !this.request.getParameter("to_membership_id").equals(""))
                   		  	  				 ?Long.parseLong(this.request.getParameter("to_membership_id")):0;
                   		  	  				 
                this.to_member_id	=	( this.request.getParameter("to_member_id")!=null &&                
                   		  	  		     !this.request.getParameter("to_member_id").equals(""))
                   		  	  			 ?Long.parseLong(this.request.getParameter("to_member_id")):0;
                   		  	  			 
                this.pseudonym = ( this.request.getParameter("pseudonym")!=null &&                
      		  	       		  	  !this.request.getParameter("pseudonym").trim().equals(""))
      		  	       		  	  ?this.request.getParameter("pseudonym").trim():null;
                                    
      		  	this.isCancel = ( this.request.getParameter("isCancel")!=null &&                
        		  	       		 !this.request.getParameter("isCancel").equals(""))
        		  	       		 ?this.request.getParameter("isCancel"):"false";
                  
      		  	break;
                
            default:
                           	
                break;                   
        }
    }

// --------------------------------------------
// Specialization of abstract methods
// --------------------------------------------

    public boolean requiresSecureRequest() {return true;}
    public boolean requiresLogin() {return true;}

    public String jspFwdUrl()
    {
        return this.redirect_jsp;
    }

    public String redirectAction()
    {
        return REDIRECT_HOME_ACTION;
    }

    public void encodeXml(PrintWriter out) {}

    public void serveRequest(OscLoginSession loginSession)
    	throws InvalidRequestException
    {   
        if(this.point_transfer_phase == 1){
            
            long membership_id=loginSession.getMembershipID();
            long member_id=loginSession.getMemberID();            
            queryTransferablePoints(membership_id,member_id);
            
            if(this.failureMsg==null){          
                                                
                if (this.transfered_points > this.transferable_points)
                {
                    this.failureMsg = "The transferable_points is not enough";
                    this.error_code = "50";
                }
                
                OscDbConnection    conn = null;
                PreparedStatement  stmt = null;                
                ResultSet           rs = null;
                
                if(this.failureMsg==null){
                    try {
                        conn = this.context.getDB(true);
                        stmt = conn.prepareStatement(SQL_QUERY_USER_PIN);
                        stmt.setLong(1,membership_id);
                        stmt.setLong(2,member_id);
                        stmt.setString(3,OscContext.getEncryptedPasswd(this.pin));
                        rs = stmt.executeQuery();
                        if (!rs.next()) {
                            this.failureMsg = "pin is invalid";
                            this.error_code="51";
                        }
                        rs.close();
                        conn.closeStatement(stmt);
                        stmt=null;
                        
                    }catch(NoSuchAlgorithmException e){
                        this.log.error("Failed to encrypt the login passowrd " + e.getMessage());
                        throw new InvalidRequestException("Failed to encrypt " +
                        		"the login passowrd " + e.getMessage());
                    }catch (SQLException e) {
                        this.log.error("Failed to query the member's password " +
                        		"in IQUE DB: " + e.getMessage());
                        throw new InvalidRequestException("Failed to query the " +
                        		"member's password in IQUE DB: " + e.getMessage());
                    }finally{
                        if (conn != null) {
                            conn.closeStatement(stmt);
                            conn.close();
                        }
                    }                    
                }                
                if(this.failureMsg==null){
                    try {
                        conn = this.context.getDB(true);
                        stmt = conn.prepareStatement(SQL_QUERY_TRANSFERED_PSEUDONYM);
                        stmt.setString(1,this.pseudonym);
                        rs = stmt.executeQuery();
                        
                        if (!rs.next()) {
                           this.failureMsg="pseudonym is not existing";
                           this.error_code="54";                           
                        }else {
                           this.to_membership_id	=	rs.getLong("membership_id");
                           this.to_member_id		=	rs.getLong("member_id");
                           this.to_name				=	rs.getString("name");
                        }
                        
                        rs.close();
                        conn.closeStatement(stmt);
                        stmt=null;                        
                    
                    }catch (SQLException e) {
                        this.log.error("Failed to query the member's pseudonym " +
                        		"in IQUE DB: " + e.getMessage());
                        throw new InvalidRequestException("Failed to query the " +
                        		"member's pseudonym in IQUE DB: " + e.getMessage());
                    }finally{
                        if (conn != null) {
                            conn.closeStatement(stmt);
                            conn.close();
                        }
                    }
                }
                if(this.failureMsg==null){
                    if(this.to_membership_id==membership_id && this.to_member_id==member_id){
                        this.failureMsg="The member points cann't be transfer to self. ";
                        this.error_code="56";
                    }
                }
                
                if(this.failureMsg==null){
                    this.redirect_jsp = MEMBER_POINT_TRANSFER_CONFIRM_JSP;
                    this.request.setAttribute("point_transfer_phase","2");
                    this.request.setAttribute("to_membership_id",Long.toString(this.to_membership_id));
                    this.request.setAttribute("to_member_id",Long.toString(this.to_member_id));
                    this.request.setAttribute("to_name",this.to_name);
                    this.request.setAttribute("isCancel","true");
                }else{
                    this.redirect_jsp = MEMBER_POINT_TRANSFER_FORM_JSP; 
                    this.request.setAttribute("point_transfer_phase","1");
                    this.request.setAttribute("transferable_points",Long.toString(this.transferable_points));
                }
            }else{
                this.redirect_jsp = MEMBER_POINT_TRANSFER_FORM_JSP;
                this.request.setAttribute("point_transfer_phase","1");
                this.request.setAttribute("transferable_points",Long.toString(this.transferable_points));
                
            }
            this.request.setAttribute("pseudonym",this.pseudonym);
            this.request.setAttribute("transfered_points",Long.toString(this.transfered_points));
            
        }else if(this.point_transfer_phase == 2){
            
            long membership_id=loginSession.getMembershipID();
            long member_id=loginSession.getMemberID();
            queryTransferablePoints(membership_id,member_id);
            
            if(this.failureMsg==null){
                if(this.isCancel.equals("true")){
                    this.redirect_jsp = MEMBER_POINT_TRANSFER_FORM_JSP; 
                    this.request.setAttribute("point_transfer_phase","1");
                    this.request.setAttribute("transferable_points",Long.toString(this.transferable_points));
                    this.request.setAttribute("pseudonym",this.pseudonym);
                    this.request.setAttribute("transfered_points",Long.toString(this.transfered_points));
                }else{
                    
                    if(this.transfered_points>this.transferable_points){
                    
                        this.failureMsg ="The transferable_points is not enough";
                        this.error_code="50";                    
                        this.redirect_jsp = MEMBER_POINT_TRANSFER_FORM_JSP;
                        this.request.setAttribute("point_transfer_phase","1");
                        this.request.setAttribute("transferable_points",Long.toString(this.transferable_points));
                    
                    }else{
                    
                        OscDbConnection   conn 	= null;
                        CallableStatement cstmt = null;
                        try{
                            conn = this.context.getDB(false); 
                            cstmt = conn.prepareCall(SQL_CALL_TRANS_POINTS);
                            cstmt.setLong(1,membership_id);
                            cstmt.setLong(2,member_id);
                            cstmt.setLong(3,this.to_membership_id);
                            cstmt.setLong(4,this.to_member_id);
                            cstmt.setLong(5,this.transfered_points);
                            cstmt.setString(6,this.request.getRemoteAddr());
                    
                            cstmt.execute ();
                            conn.closeStatement(cstmt);
                            cstmt = null;
                    	
                            conn.commit();
                    	
                        }catch (SQLException e) {
                            try {
                                conn.rollback();
                            } catch (SQLException ex) {}
                            this.log.error("Failed to finish transfering points in iQue DB:" + e.getMessage());
                            throw new InvalidRequestException("Failed to finish transfering " +
                                    "points in iQue DB:" + e.getMessage());
                        }                	
                        finally{
                            if (conn != null) {
                                conn.closeStatement(cstmt);
                                conn.close();
                            }
                        }
                        this.redirect_jsp = MEMBER_POINT_TRANSFER_SUCCESS_JSP;
                        this.request.setAttribute("transfered_points",Long.toString(this.transfered_points));
                    }
                }
            }else{
                this.redirect_jsp = MEMBER_POINT_TRANSFER_FORM_JSP;
                this.request.setAttribute("point_transfer_phase","1");
                this.request.setAttribute("transferable_points",Long.toString(this.transferable_points));
            }
            
        }else{
            
            long membership_id=loginSession.getMembershipID();
            long member_id=loginSession.getMemberID();
            queryTransferablePoints(membership_id,member_id);
            
            this.redirect_jsp = MEMBER_POINT_TRANSFER_FORM_JSP;
            this.request.setAttribute("point_transfer_phase","1");        
            this.request.setAttribute("transferable_points",Long.toString(this.transferable_points));
        
        }
    }
    
    private void queryTransferablePoints(long membership_id,long member_id)
    	throws InvalidRequestException
    {        
        OscDbConnection   conn 	= null;
        CallableStatement cstmt = null;          
               
        try {
            conn = this.context.getDB(true); 
            cstmt = conn.prepareCall(SQL_QUERY_TRANSFERABLE_POINTS);
            cstmt.registerOutParameter(1,Types.BIGINT);
            cstmt.setLong(2,membership_id);
            cstmt.setLong(3,member_id);
            cstmt.execute();

            this.transferable_points = cstmt.getLong(1);
            
            conn.closeStatement(cstmt);
            cstmt=null; 
        }
        catch (SQLException e) {
            this.log.error("Failed to query transferable points: " + e.getMessage());
            throw new InvalidRequestException("Failed to query transferable points: " 
                    + e.getMessage());
        }                	
        finally{
            if (conn != null) {
                conn.closeStatement(cstmt);
                conn.close();
            }
        }
    }
       
    public boolean getIsError() {return this.failureMsg != null;}
    public String getErrorMsg() {return this.failureMsg;}
    public String getErrorCode(){return this.error_code;}  

}

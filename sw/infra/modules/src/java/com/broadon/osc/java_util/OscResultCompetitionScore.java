package com.broadon.osc.java_util;

import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.broadon.exception.InvalidRequestException;
import com.broadon.util.VersionNumber;

/** The class used to create the results associated with a Service
 *  request, such that the appropriate document and header can be
 *  created in the response back to the client
 */
public class OscResultCompetitionScore extends OscResultBean
{
    /** Rules for creating a service result object
     */
    public static class Rule extends OscResultRule {
    	
        //public static final String COMPETITION_SCORE = "competitionscore";
        
        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
        }
        public boolean supports(ServerType t)
        {
            return t == ServerType.HTM;
        }
        public OscResultBean create(HttpServletRequest req,  
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
            return new OscResultCompetitionScore(req, res, this);
        }
        
    } // class Rule

    // --------------------------------------------
    // Invariant values
    // --------------------------------------------
    private static final String COLUMN_ID = "columnid";
    private static final String COMPETITION_SCORE_JSP = "CompetitionScore.jsp";
    private static final String SQL_SCORE_LIST =
                "select rank,score_desc,pseudonym,to_char(score_date+8/24,'YYYY-MM-DD HH24:MI:SS') as score_date "+
                "from osc.iQue_competition_scores_v "+
                "where column_id=? and rank<=10 order by rank";
    private static final String SQL_SELF_SCORE =
                "select rank,score_desc,pseudonym,to_char(score_date+8/24,'YYYY-MM-DD HH24:MI:SS') as score_date "+
                "from osc.iQue_competition_scores_v "+
                "where column_id=? and membership_id=? and member_id=?";
    private static final String SQL_COLUMN_DESC =
                "select col.column_name,col.column_desc,com.title_id,com.competition_id,tit.title "+
                "from iQue_competition_columns col,iQue_competitions com,content_titles tit "+
                "where col.competition_id=com.competition_id and com.title_id=tit.title_id and col.column_id=?";
    // --------------------------------------------
    // Member variables
    // --------------------------------------------

    private final OscResultCompetitionScore.Rule rule;
    private final OscContext                     context;
    private final OscLogger                      logger;
    private final String                         actionName;
    private final VersionNumber                  version;
    private final String                         client;
    private final String                         locale;
    private       String	                 columnID;
    private       String                         columnName;
    private       String                         columnDesc;
    private       long                           membership_id;
    private       long                           member_id;
    private       long                           titleID;
    private       String                         titleName;
    private       long                           competitionID;
    private       String                         Score_List_xml;
    private       String                         Score_Self_xml; 

    // --------------------------------------------
    // Hidden constructors (all constructors should
    // be hidden)
    // --------------------------------------------

    private OscResultCompetitionScore(HttpServletRequest  req,  
                            HttpServletResponse res,
                            OscResultCompetitionScore.Rule rule)
        throws InvalidRequestException
    {
        this.rule = rule;
        context = rule.getContext();
        logger = context.getLogger();
        actionName = rule.getActionNm();
        version = context.getRequestVersion(req);
        client = context.getRequestClient(req);
        locale = context.getRequestLocale(req);
	if (!((req.getParameter(COLUMN_ID)==null)||(req.getParameter(COLUMN_ID).trim().equals("")))) {
	    columnID=req.getParameter(COLUMN_ID).trim();
	} else {
            columnID = "0";
        }
        columnName = null;
        columnDesc = null;
        membership_id = 0;
        member_id = 0;
        titleID = 0;
        titleName = null;
        competitionID = 0;
        Score_List_xml = null;
        Score_Self_xml = null;
    }
    
    // --------------------------------------------------------------------
    //            Bean Properties
    // --------------------------------------------------------------------

    /**
     * Bean Property: 
     */

    // --------------------------------------------
    // Specialization of abstract methods
    // --------------------------------------------

    public boolean requiresLogin() {return true;}

    public String jspFwdUrl() 
    {
        return COMPETITION_SCORE_JSP;
    }

    public void encodeXml(PrintWriter out) {};

    public void serveRequest(OscLoginSession loginSession)
        throws InvalidRequestException
    {
        OscDbConnection conn = null;
        PreparedStatement stmt = null;
        ResultSet           rs = null;
        membership_id = loginSession.getMembershipID();
	member_id = loginSession.getMemberID();
        try {

            String sql = null;
    	    sql = SQL_SCORE_LIST;
            sql = (sql.substring(0,sql.indexOf("?")) + columnID + sql.substring(sql.indexOf("?") + 1));
            conn = context.getDB(true);
            Score_List_xml = conn.queryXML(sql);

            sql = SQL_SELF_SCORE;
            sql = (sql.substring(0,sql.indexOf("?")) + columnID + sql.substring(sql.indexOf("?") + 1));
            sql = (sql.substring(0,sql.indexOf("?")) + membership_id + sql.substring(sql.indexOf("?") + 1));
            sql = (sql.substring(0,sql.indexOf("?")) + member_id + sql.substring(sql.indexOf("?") + 1));
	    Score_Self_xml = conn.queryXML(sql);

            stmt = conn.prepareStatement(SQL_COLUMN_DESC);
            stmt.setInt(1, (Integer.valueOf(columnID)).intValue());
            rs = stmt.executeQuery();
            try {
                rs.next();
                columnName = rs.getString(1);
                columnDesc = rs.getString(2);
                titleID = rs.getLong(3);
                competitionID = rs.getLong(4);
                titleName = rs.getString(5);
            } catch (Exception ex) {
	        logger.error("Can not find column_ID:" + columnID + "." + ex.getMessage());
                throw new InvalidRequestException("Can not find columnID in iQue_competition_columns.");
            }
            rs.close();
            conn.closeStatement(stmt);
            stmt=null;
            conn.close();
        } catch (SQLException e) {
 	        logger.error("Ignoring invalid column_ID" + columnID + "." + e.getMessage());
                throw new InvalidRequestException("Failed to take out columnDesc and columnName.");
        } finally {
            if (conn != null) {
            	conn.close();
            }
        }
    }

    // --------------------------------------------
    // Definition of result specific bean values used
    // for formatting a result or access through
    // JSP pages.
    // --------------------------------------------
    public String transformXML(String xmlStr, String xslFile, String locale)
    { return context.transformXML(xmlStr,xslFile,locale); }

    public String getColumnName() {return columnName;}

    public String getColumnDesc() {return columnDesc;}

    public String getScoreListXML() {return Score_List_xml;}
    
    public String getScoreSelfXML() {return Score_Self_xml;}

    public String getTitleID() {return String.valueOf(titleID);}

    public String getCompetitionID() {return String.valueOf(competitionID);} 

    public String getTitleName() {return titleName;}

} // class OscResultCompetitionScore


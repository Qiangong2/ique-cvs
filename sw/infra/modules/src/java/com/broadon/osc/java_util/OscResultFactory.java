package com.broadon.osc.java_util;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.broadon.exception.InvalidRequestException;


/** The <code>OscResultFactory</code> creates result objects for all
 *  types of OSC servlets and for all supported types of request.
 */
public final class OscResultFactory
{
    private OscContext               context;
    private OscXs                    xs;
    private HashMap                  rules;   // Maps requests to result rules.
    private OscResultRule.ServerType srvType; // HTM, RPC or other server type
    private OscResultError.Rule      errorRule; // For error result


    public OscResultFactory(OscContext c, OscXs x, OscResultRule.ServerType s)
    {
        this.context = c;
        this.xs      = x;
        this.srvType = s;

        // Create the rule to be used for errors.
        //
        this.errorRule = new OscResultError.Rule("error", this.context, this.xs);

        // Create a one-to-one mapping from request name to result rule
        // for each unique request.  Try to keep it in alphabetical order
        // by the OscResult name.
        //
        final String purchaseList = 
            OscResultPurchaseTitleList.Rule.PURCHASE_LIST;
        final String trialList = 
            OscResultPurchaseTitleList.Rule.TRIAL_LIST;
        final String freeList = 
            OscResultPurchaseTitleList.Rule.FREE_LIST;
        final String purchaseTitle = 
            OscResultPurchaseTitle.Rule.PURCHASE_TITLE;
        final String trialTitle = 
            OscResultPurchaseTitle.Rule.TRIAL_TITLE;
        final String freeTitle = 
            OscResultPurchaseTitle.Rule.FREE_TITLE;
        final String servicefaqList =
            OscResultService.Rule.SERVICE_FAQ_LIST;
        final String servicefaqDetail =
            OscResultServiceFaqDetail.Rule.SERVICE_FAQ_DETAIL;
        final String servicequestion = 
            OscResultServiceQuestion.Rule.SERVICE_QUESTION;
        final String servicesuggest = 
            OscResultServiceSuggest.Rule.SERVICE_SUGGEST;
        final String servicesubscribe = 
            OscResultServiceSubscribe.Rule.SERVICE_SUBSCRIBE; 
        final String servicedownload = 
            OscResultServiceDownload.Rule.SERVICE_DOWNLOAD;
        final String showimage = 
	    OscResultServiceShowImage.Rule.SERVICE_SHOW_IMAGE;
        final String navigate =
            OscResultNavigate.Rule.NAVIGATE;
        final String vngLogin =
            OscResultVngLogin.Rule.VNG_LOGIN;
        final String vngLogout =
            OscResultVngLogin.Rule.VNG_LOGOUT;
        final String keepAlive =
            OscResultVngLogin.Rule.KEEP_ALIVE;
        final String buddy =
            OscResultBuddy.Rule.BUDDY;
        final String gameSessReg =
            OscResultGameSession.Rule.GAME_SESSION_REG;
        final String gameSessEnd =
            OscResultGameSession.Rule.GAME_SESSION_END;
        final String gameSessMatch =
            OscResultGameSessionMatch.Rule.GAME_SESSION_MATCH;
        final String gameSessQuery =
            OscResultGameSessionQuery.Rule.GAME_SESSION_QUERY;
        final String gameScore =
            OscResultVNGGameScore.Rule.GAME_SCORE;
        final String gameScoreQuery =
            OscResultVNGGameScoreQuery.Rule.GAME_SCORE_QUERY;
        final String message =
            OscResultMessages.Rule.MESSAGE;
        final String xmlQuery =
            OscResultXmlQuery.Rule.XMLQUERY;
        final String xmlDml =
            OscResultXmlDml.Rule.XMLDML;
            
        OscResultRule [] osc_rules = {
            new OscResultCommunity.Rule("community", this.context, this.xs),
            new OscResultContentMeta.Rule("content_meta_data", this.context, this.xs),
            new OscResultContentUpgrade.Rule("content_upgrade", this.context, this.xs),
            new OscResultDownloadContentList.Rule("download_content_list", this.context, this.xs),
            new OscResultGetPasswd.Rule("getpasswd", this.context, this.xs),
            new OscResultLoginForm.Rule("login_form", this.context, this.xs),
            new OscResultLogout.Rule("logout", this.context, this.xs),
            new OscResultLogin.Rule("login", this.context, this.xs),
            new OscResultPurchase.Rule("do_purchase", this.context, this.xs),
            new OscResultPurchaseTitle.Rule(purchaseTitle, this.context, this.xs),
            new OscResultPurchaseTitle.Rule(trialTitle, this.context, this.xs),
            new OscResultPurchaseTitle.Rule(freeTitle, this.context, this.xs),
            new OscResultPurchaseTitleList.Rule(purchaseList, this.context, this.xs),
            new OscResultPurchaseTitleList.Rule(trialList, this.context, this.xs),
            new OscResultPurchaseTitleList.Rule(freeList, this.context, this.xs),
            new OscResultService.Rule(servicefaqList, this.context, this.xs),
            new OscResultServiceFaqDetail.Rule(servicefaqDetail, this.context, this.xs),
            new OscResultServiceQuestion.Rule(servicequestion, this.context, this.xs),
            new OscResultServiceSuggest.Rule(servicesuggest, this.context, this.xs),
            new OscResultServiceSubscribe.Rule(servicesubscribe, this.context, this.xs),
            new OscResultServiceDownload.Rule(servicedownload, this.context, this.xs),
            new OscResultServiceShowImage.Rule(showimage, this.context, this.xs),
            new OscResultTicketSync.Rule("ticket_sync", this.context, this.xs),
            new OscResultRegister.Rule("register", this.context, this.xs),
            new OscResultRegisterConfirm.Rule("register_confirm", this.context, this.xs),
            new OscResultProductAuth.Rule("product_auth",this.context,this.xs),
            new OscResultGetPasswd.Rule("getpasswd", this.context, this.xs),
            new OscResultAcctUpdate.Rule("acct_update", this.context, this.xs),
            new OscResultMemberPointQuery.Rule("member_point_query", this.context, this.xs),
            new OscResultMemberPointTransfer.Rule("member_point_transfer", this.context, this.xs),
            new OscResultMemberPointRedeem.Rule("member_point_redeem", this.context, this.xs),
            new OscResultMemberQuizGame.Rule("member_quiz_game", this.context, this.xs),
            new OscResultNavigate.Rule(navigate, this.context, this.xs),
            new OscResultActivity.Rule("memberactivity", this.context, this.xs),
            new OscResultGameState.Rule("submit_game_state", this.context, this.xs),
            new OscResultCompetitionScore.Rule("competitionscore", this.context, this.xs)
        };

        OscResultRule [] vng_rules = {
            new OscResultVngLogin.Rule(vngLogin, this.context, this.xs),
            new OscResultVngLogin.Rule(vngLogout, this.context, this.xs),
            new OscResultVngLogin.Rule(keepAlive, this.context, this.xs),
            new OscResultBuddy.Rule(buddy, this.context, this.xs),
            new OscResultGameSession.Rule(gameSessReg, this.context, this.xs),
            new OscResultGameSession.Rule(gameSessEnd, this.context, this.xs),
            new OscResultGameSessionMatch.Rule(gameSessMatch, this.context, this.xs),
            new OscResultGameSessionQuery.Rule(gameSessQuery, this.context, this.xs),
            new OscResultVNGGameScore.Rule(gameScore, this.context, this.xs),
            new OscResultVNGGameScoreQuery.Rule(gameScoreQuery, this.context, this.xs),
            new OscResultMessages.Rule(message, this.context, this.xs),
            new OscResultXmlQuery.Rule(xmlQuery, this.context, this.xs),
            new OscResultXmlDml.Rule(xmlDml, this.context, this.xs)
        };

        OscResultRule [] all_rules = null;

        if (c.getVngSupport().equals("1")) 
        {
            all_rules = new OscResultRule[osc_rules.length+vng_rules.length];
            int i, j;

            for (i = 0; i < osc_rules.length; i++) 
                all_rules[i] = osc_rules[i];
            for (j = i ; j < i + vng_rules.length; j++) 
                all_rules[j] = vng_rules[j-i];
        } else 
            all_rules = osc_rules;
            

        // Set up a table mapping request action names to ResultRules,
        // filtering out any entries not pertinent for this type of
        // server.
        //
        this.rules = new HashMap((int)(all_rules.length * 1.5));
        for (int i = 0; i < all_rules.length; ++i) {
            if (all_rules[i].supports(this.srvType)) {
                this.rules.put(all_rules[i].getActionNm(), all_rules[i]);
            }
        }
    } // OscResultFactory constructor


    /** Creates the appropriate <code>OscResultBean</code> commensurate with
     *  the type of request.
     *
     * @returns a valid <code>OscResultBean</code> object, and throws
     *  an <code>InvalidRequestException</code> when the request is of
     *  a form that cannot be handled by this servlet.
     */
    public OscResultBean create(HttpServletRequest req,
                                HttpServletResponse res)
        throws InvalidRequestException
    {
        String actionNm = this.context.getActionNm(req);

        OscResultRule rule = (OscResultRule)this.rules.get(actionNm);
        if (rule == null) {
            throw new InvalidRequestException("Unknown request " +
                                              OscRequestParams.REQ_ACTION +
                                              ": " + actionNm);
        }

        return rule.create(req, res);
    }


    /** Creates an <code>OscResultBean</code> suitable for forwarding to
     *  any JSP page that can accept an error result.  Note that this
     *  creates a pure error object forwarded to a generic JSP error page.
     *  Any OscResultBean object may also be an error object, where the
     *  corresponding JSP page is set up to handle the error, while this
     *  is meant to handle all other errors.
     *
     * @returns an <code>OscResultError</code> object.
     */
    public OscResultBean error(HttpServletRequest  req,
                               HttpServletResponse res,
                               String              msg)
    {
        return this.errorRule.create(req, res, msg, false /*not login_error*/);
    }
    /** Creates an <code>OscResultBean</code> suitable for generating an
     *  error XML result or forwarding to an error JSP page.  Note that this
     *  creates a pure error object forwarded to a generic JSP error page.
     *  Any OscResultBean object may also be an error object, where the
     *  corresponding JSP page is set up to handle the error, while this
     *  is meant to handle all other errors.
     *
     * @returns an <code>OscResultError</code> object.
     */
    public OscResultBean loginError(HttpServletRequest  req,
                                    HttpServletResponse res,
                                    String              msg)
    {
        return this.errorRule.create(req, res, msg, true /*login_error*/);
    }

} // class OscResultFactory

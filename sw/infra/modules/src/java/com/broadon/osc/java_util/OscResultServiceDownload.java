package com.broadon.osc.java_util;

import java.io.PrintWriter;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.broadon.exception.InvalidRequestException;

/** The class used to create the results associated with a Service
 *  request, such that the appropriate document and header can be
 *  created in the response back to the client
 */
public class OscResultServiceDownload extends OscResultBean
{
    /** Rules for creating a service result object
     */
    public static class Rule extends OscResultRule {
    	
        public static final String SERVICE_DOWNLOAD = "ser_download";
        
        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
        }
        public boolean supports(ServerType t)
        {
            return t == ServerType.HTM;
        }
        public OscResultBean create(HttpServletRequest req,  
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
            return new OscResultServiceDownload(req, res, this);
        }
        
    } // class Rule

    // --------------------------------------------
    // Invariant values
    // --------------------------------------------

    private static final String SERVICE_DOWNLOAD_JSP = "iQueServiceDownload.jsp";
    private static final String SERVICE_SHOW_FILE_JSP = "iQueServiceShowFile.jsp";
    private static final String BB_ID="bb_id";
    private static final String DOWNLOAD_TYPE = "downloadType";
    private static final String PAGE_NO = "pageNo";
    private static final int    RECORDSIZE = 8;
    private static final String cacheServiceDownloadKey = "servicedownload";
    private static final String TITLE_ID="titleid";
    private static final String DEFAULT_LOCALE="zh_CN";
    private static final String SQL_FILE_DOWNLOAD =
    	"SELECT a.download_one_url as filename, a.content_type as contenttype, a.download_type as downloadtype "+
        "FROM iQue_downloads a,(SELECT content_id FROM xs.etickets WHERE bb_id=? and rtype='PR' and revoke_date is null) b "+
        "WHERE a.title_id=substr(b.content_id,1,5) and a.title_id=? and a.locale=? and a.revoke_date is null";

    private static final String SQL_DOWNLOAD_LIST =
        "SELECT a.* "+
        "FROM (select rownum as rownumb, download_name, download_image_url, download_one_title, download_one_url, "+
        "download_two_title, download_two_url, TO_CHAR(create_date,'yyyy-mm-dd') as create_date "+
        "FROM osc.ique_downloads WHERE (revoke_date is null) and download_type='?' and locale='?' "+
        "order by download_id desc) a where a.rownumb>=? and a.rownumb<=?";

    private static final String SQL_DOWNLOAD_LIST_1=
	"SELECT a.* FROM (SELECT rownum as rownumb,download_name,download_image_url,download_one_title, "+
        "download_one_url,download_two_title,download_two_url,TO_CHAR(create_date,'yyyy-mm-dd') as create_date, "+
        "title_id FROM ique_downloads WHERE title_id in (SELECT substr(content_id,1,5) FROM xs.etickets "+
        "WHERE bb_id=? and rtype='PR' and revoke_date is null) and (revoke_date is null) and download_type='manual' and "+
        "locale='?' ORDER BY download_id desc) a WHERE a.rownumb>=? AND a.rownumb<=?";
                      
    private static final String SQL_DOWNLOAD_COUNT =
        "SELECT count(*) FROM osc.ique_downloads WHERE (revoke_date is null) and download_type=? and locale=?";

    private static final String SQL_DOWNLOAD_COUNT_1 =
        "SELECT count(*) FROM osc.ique_downloads WHERE (revoke_date is null) and download_type=? and locale=? and "+
        "title_id in(SELECT substr(content_id,1,5) FROM xs.etickets WHERE bb_id=? and rtype='PR' and revoke_date is null)";

    // --------------------------------------------
    // Member variables
    // --------------------------------------------

    private final OscResultServiceDownload.Rule rule;
    private final OscContext                    context;
    private final OscLogger                     logger;
    private final String                        actionName;
    private final HttpSession			session;
    private String                       	locale;
    private String			        listXML;
    private int	                                recordCount;
    private int                                 pageCount;
    private int				        pageNo;
    private int 	                        startRecordNo;
    private int		                        endRecordNo;
    private String				downloadType;
    private String				sql;
    private String                    		bb_id;
    private int                                 titleID;
    private int                                 playerID;
    private String                              contentType;
    private String                              fileName;
    private String				redirect_jsp;
    private String				sessionID;

    // --------------------------------------------
    // Hidden constructors (all constructors should
    // be hidden)
    // --------------------------------------------

    private OscResultServiceDownload(HttpServletRequest  req,  
                                     HttpServletResponse res,
                                     OscResultServiceDownload.Rule rule)
        throws InvalidRequestException
    {
        this.rule = rule;
        context = rule.getContext();
        logger = context.getLogger();
        actionName = rule.getActionNm();
        locale = context.getRequestLocale(req);
	recordCount = 0;
        pageCount = 0;
        bb_id = "0";
        titleID = 0;
	pageNo= 1;
        listXML = "";
        downloadType = "wallpaper";

        if (!((req.getParameter(DOWNLOAD_TYPE)==null)||(req.getParameter(DOWNLOAD_TYPE).trim().equals("")))) {
            downloadType=req.getParameter(DOWNLOAD_TYPE).trim();
        }

        if ((!((req.getParameter(BB_ID)==null)||(req.getParameter(BB_ID).trim().equals(""))))&&(OscContext.isDigital(req.getParameter(BB_ID).trim()))) {
            bb_id=req.getParameter(BB_ID).trim();
        }

        if ((!((req.getParameter(PAGE_NO)==null)||(req.getParameter(PAGE_NO).trim().equals(""))))&&(OscContext.isDigital(req.getParameter(PAGE_NO).trim()))) {
            pageNo=Integer.parseInt(req.getParameter(PAGE_NO).trim());
        }

        if ((!((req.getParameter(TITLE_ID)==null)||(req.getParameter(TITLE_ID).trim().equals(""))))&&(OscContext.isDigital(req.getParameter(TITLE_ID).trim()))) {
            titleID=Integer.parseInt(req.getParameter(TITLE_ID).trim());
        }else{
            titleID=0;
        }

        startRecordNo = RECORDSIZE * (pageNo - 1) + 1;
        endRecordNo = RECORDSIZE * pageNo;
        session = req.getSession(true);
        sessionID = session.getId();
        if (session.getAttribute("BB_ID") != null){
            bb_id=(String)session.getAttribute("BB_ID");
        }else{
	    if(!(bb_id.equals("0"))){
	        session.setAttribute("BB_ID",bb_id);
            }
        }

        playerID = Integer.parseInt(bb_id);
        if (locale==null || locale.equals(""))
            locale = DEFAULT_LOCALE;
    }

    // --------------------------------------------------------------------
    //            Bean Properties
    // --------------------------------------------------------------------
    
    
    // --------------------------------------------
    // Specialization of abstract methods
    // --------------------------------------------

    public boolean requiresLogin() {return false;}

    public String jspFwdUrl() 
    {
        return redirect_jsp;
    }

    public void encodeXml(PrintWriter out) {};

    public void serveRequest(OscLoginSession loginSession)
        throws InvalidRequestException
    {
        OscDbConnection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        if (titleID==0){
            try {
                conn = context.getDB(true);
	        if(downloadType.equals("wallpaper")){
                    stmt = conn.prepareStatement(SQL_DOWNLOAD_COUNT);
                    stmt.setString(1, downloadType);
                    stmt.setString(2, locale);
                }else if(downloadType.equals("manual")){
	            stmt = conn.prepareStatement(SQL_DOWNLOAD_COUNT_1);
                    stmt.setString(1, downloadType);
                    stmt.setString(2, locale);
                    stmt.setLong(3,Long.parseLong(bb_id));
                }
                rs = stmt.executeQuery();
                if(rs.next()){
                    recordCount = rs.getInt(1);
                }
                rs.close();
                conn.closeStatement(stmt);

                if((recordCount % RECORDSIZE)==0){
                    pageCount = (recordCount / RECORDSIZE);
                }else{
                    pageCount = (recordCount / RECORDSIZE) + 1;
                }
            
	        if(downloadType.equals("wallpaper")){
             	    sql= SQL_DOWNLOAD_LIST;
            	    sql = sql.substring(0,sql.indexOf("?")) + downloadType + sql.substring(sql.indexOf("?")+1);
            	    sql = sql.substring(0,sql.indexOf("?")) + locale + sql.substring(sql.indexOf("?")+1);
            	    sql = sql.substring(0,sql.indexOf("?")) + Integer.toString(startRecordNo)
                          + sql.substring(sql.indexOf("?")+1);
            	    sql = sql.substring(0,sql.indexOf("?")) + Integer.toString(endRecordNo) 
                          + sql.substring(sql.indexOf("?")+1);
	        }else if(downloadType.equals("manual")){
		    sql = SQL_DOWNLOAD_LIST_1;
                    sql = sql.substring(0,sql.indexOf("?")) + Long.parseLong(bb_id) + sql.substring(sql.indexOf("?")+1);
		    sql = sql.substring(0,sql.indexOf("?")) + locale + sql.substring(sql.indexOf("?")+1);
            	    sql = sql.substring(0,sql.indexOf("?")) + Integer.toString(startRecordNo) 
                          + sql.substring(sql.indexOf("?")+1);
                    sql = sql.substring(0,sql.indexOf("?")) + Integer.toString(endRecordNo) 
                          + sql.substring(sql.indexOf("?")+1);
	        }
                listXML = conn.queryXML(sql);

            } catch (SQLException e) {
                context.getLogger().error("Failed to obtain download list." + e.getMessage());
                throw new InvalidRequestException("Failed to obtain download list.");
            } catch (Exception e) {
                logger.error("Failed to process List Download " + e.getMessage());
                throw new InvalidRequestException("Failed to process List Download .");
            } finally {
                if (conn != null)
                    conn.close();
            }
            redirect_jsp = SERVICE_DOWNLOAD_JSP;
        }else{
            try {
                String sql = null;
                conn = context.getDB(false);
                stmt = conn.prepareStatement(SQL_FILE_DOWNLOAD);
                stmt.setInt(1, playerID);
                stmt.setInt(2, titleID);
                stmt.setString(3, locale);
                rs = stmt.executeQuery();
                if(rs.next()){
                    fileName = rs.getString(1);
                    contentType = rs.getString(2);
                    downloadType = rs.getString(3);
                } else {
                    fileName = null;
                    contentType = null;
                    downloadType = null;
                }
                rs.close();
                conn.closeStatement(stmt);
            }
            catch (SQLException e) {
                context.getLogger().error("Failed to query manual. " + e.getMessage());
                throw new InvalidRequestException("Failed to obtain Filename and ContentType for title ID.");
            }
            catch (Exception e) {
                context.getLogger().error("Failed to obtain " + actionName + " Filename and ContentType. " + e.getMessage());
                throw new InvalidRequestException("Failed to obtain filename and contenttype for title ID.");
            }
            finally {
                if (conn != null)
                    conn.close();
            }
            redirect_jsp = SERVICE_SHOW_FILE_JSP;
        }
    }

    public String toString() {return null;}

    // --------------------------------------------
    // Definition of result specific bean values used
    // for formatting a result or access through
    // JSP pages.
    // --------------------------------------------

    public String transformXML(String xmlStr, String xslFile, String locale){
        return context.transformXML(xmlStr,xslFile,locale); 
    }

    public int getPageCount() {return pageCount;}

    public int getPageNo() {return pageNo;}

    public String getDownloadType() {return downloadType;}

    public String getListXML() {return listXML;}

    public String getFileName() {return fileName;}

    public String getContentType() {return contentType;}

    public String getLocale() {return locale;}

    public String getSessionID() {return sessionID;}
} // class OscResultServiceDownload


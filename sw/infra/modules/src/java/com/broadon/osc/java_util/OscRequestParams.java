package com.broadon.osc.java_util;

/** Just a namespace holding names for the parameters we expect in requests
 */
public abstract class OscRequestParams
{
    // RPC request and result root tags
    //
    public static final String RPC_REQUEST_TAG = "rpc_request";
    public static final String RPC_RESULT_TAG = "rpc_response";

    
    // Mandatory request parameters with every request to the OSC server
    //
    public static final String REQ_VERSION = "version";   // Version of client
    public static final String REQ_CLIENT = "client";     // Type of client
    public static final String REQ_ACTION = "OscAction";  // Purpose of request
    public static final String REQ_LOCALE = "locale";     // Language region
    public static final String REQ_STORE_ID = "store_id"; // Online store id

    // Login request
    //
    public static final String REQ_LOGIN_ID = "id";     // User login
    public static final String REQ_LOGIN_PWD = "pwd";   // User password

    // Title Detail request
    //
    public static final String REQ_TITLE_ID = "titleID";     // Title ID
    public static final String REQ_AUTO_DOWNLOAD = "auto";   // Flag to indicate auto start purchase/download
    public static final String REQ_TRIAL = "trial";          // Flag to indicate trial request

    // RPC purchase/sync requests
    //
    public static final String CLIENT_IP = "client_ip";
    public static final String MEMBERSHIP_ID = "membership_id";
    public static final String MEMBER_ID = "member_id";
    public static final String BB_ID_TAG = "bb_id";
    public static final String GAME_STATE_TAG = "states";
    public static final String SIGNATURE_TAG = "signature";
    public static final String KIND_OF_PURCHASE_TAG = "kind_of_purchase";
    public static final String TITLE_ID_TAG = "title_id";
    public static final String CONTENT_ID_TAG = "content_id";
    public static final String EUNITS_TAG = "eunits";
    public static final String ECARD_TAG = "ecard";
    public static final String SYNC_TIMESTAMP_TAG = "sync_timestamp";
    public static final String TID_TAG = "tid";
    public static final String ETICKET_IN_FULL_PARAM = 
    "\t<eticket_in_full>1</eticket_in_full>\n";

    public static final String PAGE_NO = "pageNo";

    // RPC upgrade requests
    //
    static final String OLD_CONTENT_ID_TAG = "old_content_id";
    static final String NEW_CONTENT_ID_TAG = "new_content_id";

    // RPC xmlquery requests
    //
    static final String SQLSTMT = "sqlstmt";

    // RPC xmldml requests
    //
    static final String XMLDATA = "xmldata";
    static final String DMLTYPE = "dmltype";
    static final String TABNAME = "tabname";


    // VNG Requests
    //
    static final String RPC_PARAMS_TAG = "rpc_param";
    static final String NAME = "name";
    static final String VALUE = "value";
    static final String OPERATOR = "operator";

    // VNG Login
    //
    static final String REQ_LOGIN_TYPE = "type";

    // VNG Buddy requests
    //
    static final String BUDDY_ACTION_TYPE = "buddy_type";
    static final String BUDDY_MEMBERSHIP_ID = "buddy_membership_id";
    static final String BUDDY_MEMBER_ID = "buddy_member_id";
    static final String BUDDY_PSEUDONYM = "buddy_pseudonym";
    static final String BUDDY_INFO = "buddy_info";
    static final String BUDDY_NUM_ROWS = "num_rows";
    static final String BUDDY_SKIP_ROWS = "skip_rows";
    static final String BUDDY_INDEX = "index";

    // VNG Game sessions requests
    //
    static final String GSESS_QUERY_TYPE = "type";
    static final String GSESS_VNID = "vn_id";
    static final String GSESS_SORT = "gsess_sort";
    static final String GSESS_NUM_ROWS = "num_rows";
    static final String GSESS_SKIP_ROWS = "skip_rows";

    // VNG Game scores requests
    //
    static final String GAME_SCORE_TYPE 	= "type";
    static final String SCORE_ACCEPTED_TAG 	= "accepted";
    static final String DEVICE_ID 			= "device_id";
    static final String SLOT_ID 			= "slot_id";
    static final String TITLE_ID	 		= "title_id";
    static final String GAME_ID 			= "game_id";
    static final String SCORE_ID 			= "score_id";
    static final String SCORE_INFO 			= "score_info";
    static final String ITEM_ID 			= "item_id";    
    static final String SCORE 				= "score";
    static final String SCORE_OBJECT_SIZE 	= "size";
    static final String SCORE_OBJECT 		= "score_obj";    
    

    // VNG Messages
    //
    static final String MESSAGES_ACTION_TYPE = "type";
    static final String MESSAGES_MESSAGE_ID = "message_id";
    static final String MESSAGES_MESSAGE_TYPE = "message_type";
    static final String MESSAGES_REPLY_ID = "reply_id";
    static final String MESSAGES_IP_ADDRESS = "ip_address";
    static final String MESSAGES_CONTENT_TYPE = "content_type";
    static final String MESSAGES_CONTENT_LENGTH = "content_length";
    static final String MESSAGES_SEND_DATE = "send_date";
    static final String MESSAGES_TO = "to";
    static final String MESSAGES_CC = "cc";
    static final String MESSAGES_BCC = "bcc";
    static final String MESSAGES_SUBJECT = "subject";
    static final String MESSAGES_MESSAGE = "message";
    static final String MESSAGES_NUM_ROWS = "num_rows";
    static final String MESSAGES_SKIP_ROWS = "skip_rows";

} //  class OscRequestParams

package com.broadon.osc.java_util;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.broadon.exception.InvalidRequestException;


/** The class used to serve the request to display the main login page
 *  associated with loginform request.
 */
public class OscResultLoginForm extends OscResultBean
{
    /** Rules for creating the Main Osc Login page
     */
    public static class Rule extends OscResultRule {

        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
        }
        public boolean supports(ServerType t)
        {
            return t == ServerType.HTM;
        }
        public OscResultBean create(HttpServletRequest req,  
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
	    return new OscResultLoginForm(req, res, this);
        }
    } // class Rule


    // --------------------------------------------
    // Invariant values
    // --------------------------------------------

    private static final String LOGIN_FORM_JSP = "LoginForm.jsp";
    private static final String HOME_JSP = "Home.jsp";
    private static final String UNDEFINED_PSEUDONYM = "iQue@Home User";
    private static final String FORWARDED_ERRCODE = "errcode";
    private static final String FORWARDED_LOGIN_FLAG = "dologin";


    // --------------------------------------------
    // Member variables
    // --------------------------------------------

    private final  OscResultLoginForm.Rule rule;
    private final  OscContext              context;
    private String                         loginName;
    private String                         errCode;
    private boolean                        loginFlag;

    // --------------------------------------------
    // Hidden constructors (all constructors should
    // be hidden for result objects, since they are
    // constructed by the Rule class)
    // --------------------------------------------

    private OscResultLoginForm(HttpServletRequest  req,  
                               HttpServletResponse res,
                               OscResultLoginForm.Rule rule)
    {
        this.rule = rule;
        context = rule.getContext();
        loginName = null;

        // Get forwarded error code
        //
        errCode = req.getParameter(FORWARDED_ERRCODE);

        // Get forwarded login flag for home page
        //
        String l = req.getParameter(FORWARDED_LOGIN_FLAG);

        if (l != null && l.equals("yes"))
            loginFlag = true;
        else
            loginFlag = false;
    }


    // --------------------------------------------
    // Specialization of abstract methods
    // --------------------------------------------

    public boolean requiresLogin() {return false;}

    public String jspFwdUrl() 
    {
        if (getIsLoggedIn())
            return HOME_JSP;
        else
            return LOGIN_FORM_JSP;
    }

    public void encodeXml(PrintWriter out) {};

    public void serveRequest(OscLoginSession loginSession)
        throws InvalidRequestException
    {
        if (loginSession.isLoggedIn())
            loginName = loginSession.getPseudonym();
    }

    public String toString() {return null;}

    public boolean getIsError() {return errCode != null;}
    public String getErrorCode() {return errCode;}

    public boolean getIsLoggedIn() { return (loginName != null); }
    public String getPseudonym() { return loginName; }

    public boolean getDoLogin() {return loginFlag;}

    // --------------------------------------------
    // Definition of result specific bean values used
    // for formatting a result for access through
    // JSP pages.
    // --------------------------------------------

} // class OscResultLoginForm


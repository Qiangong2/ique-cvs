package com.broadon.osc.java_util;

import java.io.PrintWriter;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Blob;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.broadon.exception.InvalidRequestException;

/** The class used to create the results associated with a Service
 *  request, such that the appropriate document and header can be
 *  created in the response back to the client
 */
public class OscResultServiceShowImage extends OscResultBean
{
    /** Rules for creating a service result object
     */
    public static class Rule extends OscResultRule {
    	
        public static final String SERVICE_SHOW_IMAGE = "show_image";
        
        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
        }
        public boolean supports(ServerType t)
        {
            return t == ServerType.HTM;
        }
        public OscResultBean create(HttpServletRequest req,  
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
            return new OscResultServiceShowImage(req, res, this);
        }
        
    } // class Rule
    
    // --------------------------------------------
    // Invariant values
    // --------------------------------------------

    private static final String SERVICE_SHOW_IMAGE_JSP = "iQueServiceShowImage.jsp";
    private static final String IMAGE_ID= "image_id";
    private static final String DEFAULT_LOCALE="zh_CN";
    private static final String SQL_IMAGE_TRANSFER =
    "INSERT INTO iQue_images_tmp SELECT * FROM iQue_images WHERE image_id=?";
    
    private static final String SQL_IMAGE = 
    "SELECT image FROM iQue_images_tmp where image_id=?";

    // --------------------------------------------
    // Member variables
    // --------------------------------------------

    private final OscResultServiceShowImage.Rule        rule;
    private final OscContext          		        context;
    private final OscLogger          			logger;
    private String                                      actionName;
    private String                                      locale;
    private int  					imageID;
    private Blob					imageBlob;
    private InputStream					imageStream;
    private int						imageBlobSize;
    private byte[]					imageBytes;
    private int						imageBytesRead;
    // --------------------------------------------
    // Hidden constructors (all constructors should
    // be hidden)
    // --------------------------------------------

    private OscResultServiceShowImage(HttpServletRequest  req,  
                                      HttpServletResponse res,
                                      OscResultServiceShowImage.Rule rule)
        throws InvalidRequestException
    {
        this.rule = rule;
        context = rule.getContext();
        logger = context.getLogger();
        actionName = rule.getActionNm();

	if ((!((req.getParameter(IMAGE_ID)==null)||(req.getParameter(IMAGE_ID).trim().equals(""))))&&(OscContext.isDigital(req.getParameter(IMAGE_ID).trim()))) {
		imageID=Integer.parseInt(req.getParameter(IMAGE_ID).trim());
	}else{
		imageID=0;
	}
	           
        locale = req.getParameter("locale");
        if (locale==null || locale.equals(""))
            locale = DEFAULT_LOCALE;
    }
    
    // --------------------------------------------------------------------
    //            Bean Properties
    // --------------------------------------------------------------------
    
    
    // --------------------------------------------
    // Specialization of abstract methods
    // --------------------------------------------

    public boolean requiresLogin() {return true;}

    public String jspFwdUrl()
    {
        return SERVICE_SHOW_IMAGE_JSP;
    }

    public void encodeXml(PrintWriter out) {};

    public void serveRequest(OscLoginSession loginSession)
        throws InvalidRequestException
    {
        OscDbConnection conn = null;
        PreparedStatement stmt = null;
        ResultSet rset = null;

        try {
            String sql = null;
    
	    conn = context.getDB(false);
            stmt = conn.prepareStatement(SQL_IMAGE_TRANSFER);
            stmt.setLong(1, imageID);
            stmt.executeUpdate();
            conn.closeStatement(stmt); 

            stmt = conn.prepareStatement(SQL_IMAGE);
            stmt.setLong(1, imageID);
            rset = stmt.executeQuery(); 
            if(rset.next()){
               imageBlob = (Blob)rset.getBlob(1);
               imageStream = imageBlob.getBinaryStream();
               imageBlobSize = (int)imageBlob.length();
               imageBytes = new byte[imageBlobSize];
               imageBytesRead = imageStream.read(imageBytes);
               imageStream.close();
            }
            rset.close();
            conn.closeStatement(stmt);
            conn.commit();
        }
	catch (SQLException e) {
            try {
               conn.rollback();
            }
            catch (SQLException ex){
               logger.error("Failed to rollback. " + ex.getMessage());
            }
            context.getLogger().error("Failed to query image. " + e.getMessage());
            throw new InvalidRequestException("Failed to obtain content for image ID.");
        }
        catch (Exception e) {
	    context.getLogger().error("Failed to obtain " + actionName + " image. " + e.getMessage());
            throw new InvalidRequestException("Failed to obtain content for image ID.");
        }
        finally {
            if (conn != null)
                conn.close();
        }
        
    }

    // --------------------------------------------
    // Definition of result specific bean values used
    // for formatting a result or access through
    // JSP pages.
    // --------------------------------------------
    public byte[] getImageBytes() {return imageBytes;}

    public int getImageBytesRead() {return imageBytesRead;}

}
// class OscResultServiceShowImage

package com.broadon.osc.java_util;

import java.io.PrintWriter;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Enumeration;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

import com.broadon.exception.InvalidRequestException;
import com.broadon.util.VersionNumber;


public class OscResultMemberPointRedeem extends OscResultBean {

    /** Rules for creating a Login result object
     */
    public static class Rule extends OscResultRule {

        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
        }
        public boolean supports(ServerType t)
        {
            return t == ServerType.HTM;
        }
        public OscResultBean create(HttpServletRequest req,
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
            return new OscResultMemberPointRedeem(req, res, this);
        }
    } // class Rule

    private static class Pair {
        private Object recordsArray;
        private Object recordsPerPage;
        private Object recordsCount;
        private Object pagesCount;
        Pair(Object o1, Object o2, Object o3, Object o4)
        {
            this.recordsArray	= o1;
            this.recordsPerPage	= o2;
            this.recordsCount   = o3;
            this.pagesCount		= o4;
        }
        Object getRecordsArray() 	{return this.recordsArray;}
        Object getRecordsPerPage() 	{return this.recordsPerPage;}
        Object getRecordsCount() 	{return this.recordsCount;}
        Object getPagesCount() 		{return this.pagesCount;}
    } // class Pair


    private static final String REDIRECT_HOME_ACTION   			   	= "login_form";
    private static final String MEMBER_POINT_REDEEM_LIST_JSP		= "MemberPointRedeemList.jsp";
    private static final String MEMBER_POINT_REDEEM_SESSION_JSP		= "MemberPointRedeemSession.jsp";
    private static final String MEMBER_POINT_REDEEM_CONFIRM_JSP		= "MemberPointRedeemConfirm.jsp";
    private static final String MEMBER_POINT_REDEEM_SUCC_JSP		= "MemberPointRedeemSucc.jsp";
    private static final String MEMBER_POINT_REDEEM_QUERY_ORDER_JSP = "MemberPointRedeemOrderQuery.jsp";

    private static final OscCachedMap CACHED_REDEEMABLE_LIST		= new OscCachedMap(100);

    private static final int	RECORDS_COUNT_PER_PAGE				= 3;

    public static final String RULE_ID 					= "ruleId";
    public static final String RULE_DESC 				= "ruleDesc";
    public static final String POINTS					= "points";
    public static final String QUANTITY 				= "quantity";
    public static final String CHANNEL_ID				= "channelId";
    public static final String ML						= "ML";
    public static final String GIFT_ORDER				= "giftOrder";
    public static final String INSUFFICIENT_GIFT_ID 	= "InsufficientGiftId";
    public static final String UNFOUND_GIFT_ID 			= "UnfoundGiftId";
    public static final String POINT_REDEEM_PHASE 		= "point_redeem_phase";
    public static final String POINT_REDEEM_PREV_PHASE 	= "point_redeem_prev_phase";
    public static final String POINT_REDEEM_CURR_PHASE 	= "point_redeem_curr_phase";
    public static final String POINT_REDEEM_NEXT_PHASE 	= "point_redeem_next_phase";
    public static final String REDEEMABLE_GIFTS_LIST 	= "redeemableGiftsList";
    public static final String REDEMPTION_CHANNELS_LIST	= "redemptionChannelsList";
    public static final String REQUESTED_GIFT_POINTS 	= "requestedGiftPoints";
    public static final String REQUESTED_MAIL_POINTS 	= "requestedMailPoints";
    public static final String TOTAL_POINTS 			= "totalPoints";
    public static final String IS_MODIFY_MODE 			= "isModifyMode";
    public static final String RECORDS_COUNT 			= "recordsCount";
    public static final String PAGES_COUNT 				= "pagesCount";
    public static final String CURRENT_PAGE_NO 			= "currentPageNo";
    public static final String GIFT_ORDER_ID 			= "giftOrderId";
    public static final String GIFT_ORDER_LIST			= "giftOrderList";
    public static final String GIFT_IMAGE_FILE_NAME		= "giftImageFileName";
    public static final String MAILING_NAME				= "mailingName";
    public static final String MAILING_ADDRESS			= "mailingAddress";
    public static final String CONTACT_NAME				= "contactName";
    public static final String POSTAL_CODE				= "postalCode";
    public static final String TELEPHONE				= "telephone";
    public static final String IS_REQUIRE_MAIL_INFO		= "isRequireMailInfo";


    private static final String SQL_QUERY_REDEEMABLE_LIST =
        "SELECT rule_id,rule_desc,points FROM ique_member_point_rules WHERE point_type='R' " +
        "AND SYSDATE >= activate_date AND (deactivate_date > SYSDATE OR deactivate_date is " +
        "null) AND rule_status = 'E' AND region_id=?";

    private static final String SQL_QUERY_REDEEMABLE_LIST_COUNT =
        "SELECT count(rule_id) FROM ique_member_point_rules WHERE point_type='R' AND SYSDATE " +
        ">= activate_date AND (deactivate_date > SYSDATE OR deactivate_date is null) AND " +
        "rule_status = 'E' AND region_id=?";

    private static final String SQL_QUERY_REDEEM_CHANNELS =
        "SELECT channel_id,channel_desc,contact_person,address,telephone FROM ique_member_" +
        "redeem_channels WHERE SYSDATE >= activate_date AND (deactivate_date > SYSDATE OR " +
        "deactivate_date is null) AND channel_status = 'E' ORDER by channel_desc desc";

    private static final String SQL_QUERY_REDEEMABLE_POINTS =
        "{? = call IQUE_MEMBER_POINT_PKG.GET_REDEEMABLE_POINTS(?, ?)}";

    private static final String SQL_QUERY_STOCK =
        "SELECT quantity FROM ique_gift_stocks WHERE gift_id=? AND channel_id=?";

    private static final String SQL_QUERY_GIFT_POINTS =
        "SELECT points FROM ique_member_point_rules WHERE rule_id=?";

    private static final String SQL_QUERY_MAIL_POINTS =
        "SELECT IQUE_MEMBER_POINT_PKG.GET_MAIL_POINTS() FROM dual";

    private static final String SQL_QUERY_GIFTS_PER_MAIL =
        "SELECT IQUE_MEMBER_POINT_PKG.GET_GIFTS_PER_MAIL FROM dual";

    private static final String SQL_CALL_REDEEM_GIFTS =
        "{CALL IQUE_MEMBER_POINT_PKG.REDEEM_ORDER(?,?,?,?,?,?,?,?,?,?,?,?)}";

    private static final String SQL_QUERY_GIFT_ORDER_DETAIL =
        "SELECT to_char(ro.order_date+8/24,'YYYY-MM-DD HH24:MI:SS') order_date,mpr.rule_desc," +
        "mpr.points,rod.quantity,rod.channel_id,mrch.channel_desc,rod.status,to_char(rod.action_" +
        "date,'YYYY-MM-DD HH24:MI:SS') action_date FROM ique_member_redeem_orders ro,ique_member_" +
        "redeem_orders_d rod,ique_member_point_rules mpr,ique_member_redeem_channels mrch WHERE " +
        "ro.order_id=rod.order_id AND rod.channel_id=mrch.channel_id AND mpr.rule_id=rod.gift_id " +
        "AND ro.order_id=? AND ro.membership_id=? AND ro.member_id=? ORDER BY rod.gift_id";

    private static final String SQL_QUERY_GIFT_ORDER_DETAIL_COUNT =
        "SELECT count(ro.order_id) FROM ique_member_redeem_orders ro,ique_member_redeem_orders_d rod," +
        "ique_member_point_rules mpr,ique_member_redeem_channels mrch WHERE ro.order_id=rod.order_id " +
        "AND rod.channel_id=mrch.channel_id AND mpr.rule_id=rod.gift_id AND ro.order_id=? AND " +
        "ro.membership_id=? AND ro.member_id=? ORDER BY rod.gift_id";

    private final OscResultMemberPointRedeem.Rule    	rule;
    private final OscContext                			context;
    private final OscLogger                 			logger;
    private final HttpServletRequest 					request;
    private final HttpSession							session;

    private final VersionNumber                 		version;
    private final Integer                       		storeID;
    private final String                        		locale;

    private String  failureMsg;
    private String  error_code;
    private String  redirect_jsp;

    private String  mailingName;
    private String  mailingAddress;
    private String  contactName;
    private String  telephone;
    private String  giftOrderId;
    private String 	postalCode;


    private String[][] redeemableGiftsList;

    private int		point_redeem_phase;
    private int		recordsPerPage;
    private int		recordsCount;
    private int		pagesCount;

    private int		pageNo;

    private Properties giftOrder;

    private OscResultMemberPointRedeem(HttpServletRequest  req,
            					 	   HttpServletResponse res,
            					 	   OscResultMemberPointRedeem.Rule rule)
    	throws InvalidRequestException
    {
        this.rule 		= rule;
        this.context 	= rule.getContext();
        this.logger		= this.context.getLogger();
        this.request 	= req;
        this.version 	= this.context.getRequestVersion(this.request);
        this.locale	 	= this.context.getRequestLocale(this.request);
        this.storeID 	= this.context.getStoreId(this.request, this.version);
        this.session	= this.request.getSession(true);


        this.point_redeem_phase = 0;

        if(this.request.getParameter(POINT_REDEEM_PHASE)!=null)
        {
           if(this.request.getParameter(POINT_REDEEM_PHASE).equals("1"))
           {
               this.point_redeem_phase = 1;
           }else if(this.request.getParameter(POINT_REDEEM_PHASE).equals("2")){
               this.point_redeem_phase = 2;
           }else if(this.request.getParameter(POINT_REDEEM_PHASE).equals("3")){
               this.point_redeem_phase = 3;
           }else if(this.request.getParameter(POINT_REDEEM_PHASE).equals("4")){
               this.point_redeem_phase = 4;
           }else if(this.request.getParameter(POINT_REDEEM_PHASE).equals("5")){
               this.point_redeem_phase = 5;
           }else if(this.request.getParameter(POINT_REDEEM_PHASE).equals("6")){
               this.point_redeem_phase = 6;
           }else if(this.request.getParameter(POINT_REDEEM_PHASE).equals("7")){
               this.point_redeem_phase = 7;
           }
        }

        switch(this.point_redeem_phase)
        {
            case 1:

                if(this.session.getAttribute(GIFT_ORDER)!=null){
                    this.giftOrder=(Properties)this.session.getAttribute(GIFT_ORDER);
                }else{
                    this.giftOrder=new Properties();
                    this.session.setAttribute(GIFT_ORDER,this.giftOrder);
                }

                try{
                    String ruleId =	this.request.getParameter(RULE_ID);
                    if(ruleId == null || ruleId.equals("") || !OscContext.isDigital(ruleId)){
                        this.failureMsg="The rule id is null or contains invalid symbols.";
                        this.error_code="14";
                        throw new Exception("");
                    }

                    String quantity = this.request.getParameter(QUANTITY);
                    if(quantity == null || quantity.equals("") || !OscContext.isDigital(quantity)){
                        this.failureMsg="The quantity of gift is null or invalid.";
                        this.error_code="15";
                        throw new Exception("");
                    }

                    String channelId = this.request.getParameter(CHANNEL_ID);
                    if(channelId == null || channelId.equals("")){
                        this.failureMsg="The channel type is null.";
                        this.error_code="16";
                        throw new Exception("");
                    }

                    if(!isStockSufficient(ruleId,quantity,channelId)){
                        this.failureMsg="The stock balance is insufficient.";
                        this.error_code="57";
                        throw new Exception("");
                    }

                    String points = this.request.getParameter(POINTS);
                    String ruleDesc	= this.request.getParameter(RULE_DESC);
                    String giftImageFileName = this.request.getParameter(GIFT_IMAGE_FILE_NAME);

                    Properties gift = new Properties();
                    this.giftOrder.put(ruleId,gift);
                    gift.put(RULE_ID,				ruleId);
                    gift.put(RULE_DESC,				ruleDesc);
                    gift.put(POINTS,				points);
                    gift.put(QUANTITY,				quantity);
                    gift.put(CHANNEL_ID,			channelId);
                    gift.put(GIFT_IMAGE_FILE_NAME,	giftImageFileName);

                    this.failureMsg="Successful putting.";
                    this.error_code="103";

                }catch(Exception e){}

                this.redirect_jsp = MEMBER_POINT_REDEEM_LIST_JSP;
                this.request.setAttribute(POINT_REDEEM_CURR_PHASE,"1");
                this.request.setAttribute(POINT_REDEEM_NEXT_PHASE,"2");

                break;

            case 2:
                this.giftOrder = this.session.getAttribute(GIFT_ORDER) != null ?
                        (Properties)this.session.getAttribute(GIFT_ORDER) : null;
                if(this.giftOrder==null || this.giftOrder.isEmpty()){
                    this.failureMsg="The gift order is null.";
                    this.error_code="19";
                    this.request.setAttribute(POINT_REDEEM_NEXT_PHASE,"2");
                }else{
                    this.request.setAttribute(POINT_REDEEM_NEXT_PHASE,"4");
                }
                this.redirect_jsp = MEMBER_POINT_REDEEM_SESSION_JSP;
                this.request.setAttribute(POINT_REDEEM_PREV_PHASE,"0");
                this.request.setAttribute(POINT_REDEEM_CURR_PHASE,"3");

      		  	break;

            case 3:
                this.giftOrder = this.session.getAttribute(GIFT_ORDER) != null ?
                        (Properties)this.session.getAttribute(GIFT_ORDER) : null;
                if(this.giftOrder==null || this.giftOrder.isEmpty()){
                    this.failureMsg="The gift order is null.";
                    this.error_code="19";
                }else{
                    boolean isModifyMode=this.request.getParameter(IS_MODIFY_MODE)!=null &&
                    					 this.request.getParameter(IS_MODIFY_MODE).equals("true") ?
                    					         true:false;
                    if(isModifyMode){
                        try{
                            String ruleId =	this.request.getParameter(RULE_ID);
                            if(ruleId == null || ruleId.equals("") || !OscContext.isDigital(ruleId)){
                                this.failureMsg="The rule id is null or contains invalid symbols.";
                                this.error_code="14";
                                throw new Exception("");
                            }

                            String quantity = this.request.getParameter(QUANTITY);
                        	if(quantity == null || quantity.equals("") || !OscContext.isDigital(quantity)){
                        	    this.failureMsg="The quantity of gift is null or invalid.";
                                this.error_code="15";
                        	    throw new Exception("");
                        	}

                        	String channelId = this.request.getParameter(CHANNEL_ID);
                        	if(channelId == null || channelId.equals("")){
                        	    this.failureMsg="The channel type is null.";
                                this.error_code="16";
                        	    throw new Exception("");
                        	}
                        	String points = this.request.getParameter(POINTS);
                        	String ruleDesc	= this.request.getParameter(RULE_DESC);
                        	String giftImageFileName = this.request.getParameter(GIFT_IMAGE_FILE_NAME);

                        	Properties gift = new Properties();
                        	this.giftOrder.put(ruleId,gift);
                        	gift.put(RULE_ID,				ruleId);
                            gift.put(RULE_DESC,				ruleDesc);
                            gift.put(POINTS,				points);
                            gift.put(QUANTITY,				quantity);
                            gift.put(CHANNEL_ID,			channelId);
                            gift.put(GIFT_IMAGE_FILE_NAME,	giftImageFileName);

                        }catch(Exception e){}

                    }else{
                        try{
                            String ruleId =	this.request.getParameter(RULE_ID);
                            if(ruleId == null || ruleId.equals("") || !OscContext.isDigital(ruleId)){
                                this.failureMsg="The rule id is null or contains invalid symbols.";
                                this.error_code="14";
                                throw new Exception("");
                            }
                            this.giftOrder.remove(ruleId);
                        }catch(Exception e){}
                    }
                }

                this.redirect_jsp = MEMBER_POINT_REDEEM_SESSION_JSP;
                this.request.setAttribute(POINT_REDEEM_PREV_PHASE,"0");
                this.request.setAttribute(POINT_REDEEM_CURR_PHASE,"3");
                this.request.setAttribute(POINT_REDEEM_NEXT_PHASE,"4");

                break;

            case 4:
                this.giftOrder = this.session.getAttribute(GIFT_ORDER) != null ?
                        (Properties)this.session.getAttribute(GIFT_ORDER) : null;
                if(this.giftOrder==null || this.giftOrder.isEmpty()){
                    this.failureMsg="The gift order is null.";
                    this.error_code="19";
                }

                this.redirect_jsp = MEMBER_POINT_REDEEM_CONFIRM_JSP;
                this.request.setAttribute(POINT_REDEEM_PREV_PHASE,"7");
                this.request.setAttribute(POINT_REDEEM_NEXT_PHASE,"5");

                break;

            case 5:

                this.giftOrder = this.session.getAttribute(GIFT_ORDER) != null ?
                        (Properties)this.session.getAttribute(GIFT_ORDER) : null;
                if(this.giftOrder==null || this.giftOrder.isEmpty()){
                    this.failureMsg="The gift order is null.";
                    this.error_code="19";
                }else{
                    int i=this.request.getParameter(IS_REQUIRE_MAIL_INFO)!=null &&
                    !this.request.getParameter(IS_REQUIRE_MAIL_INFO).equals("") ?
                    	          Integer.parseInt(this.request.getParameter(IS_REQUIRE_MAIL_INFO)):0;
                    this.mailingName	=	null;
                    this.mailingAddress	=	null;
                    this.contactName	=	null;
                    this.postalCode		=	null;
                    this.telephone		=	null;

                    try{
                        if(i == 2){
                            this.mailingAddress = this.request.getParameter(MAILING_ADDRESS);
                            if(this.mailingAddress==null||this.mailingAddress.equals("")){
                                this.failureMsg="The mailing Address is null.";
                                this.error_code="61";
                                throw new Exception("");
                            }
                            this.mailingName = this.request.getParameter(MAILING_NAME);
                            if(this.mailingName==null||this.mailingName.equals("")){
                                this.failureMsg="The mailing name is null.";
                                this.error_code="59";
                                throw new Exception("");
                            }
                            this.postalCode = this.request.getParameter(POSTAL_CODE);
                        	if(this.postalCode==null||!OscContext.isDigital(this.postalCode)){
                        	    this.failureMsg="The postal code is null or invalid.";
                        	    this.error_code="63";
                        	    throw new Exception("");
                        	}
                            this.contactName = this.request.getParameter(CONTACT_NAME);
                            if(this.contactName==null||this.contactName.equals("")){
                                this.failureMsg="The contact name is null.";
                                this.error_code="62";
                                throw new Exception("");
                            }
                        	this.telephone = this.request.getParameter(TELEPHONE);
                        	if(this.telephone==null||!OscContext.isDigital(this.telephone)){
                        	    this.failureMsg="The telephone number is null or invalid.";
                        	    this.error_code="64";
                        	    throw new Exception("");
                        	}
                        }else if(i == 1){
                            this.mailingAddress = this.request.getParameter(MAILING_ADDRESS);
                            if(this.mailingAddress==null||this.mailingAddress.equals("")){
                                this.failureMsg="The mailing Address is null.";
                                this.error_code="61";
                                throw new Exception("");
                            }
                            this.mailingName = this.request.getParameter(MAILING_NAME);
                            if(this.mailingName==null||this.mailingName.equals("")){
                                this.failureMsg="The mailing name is null.";
                                this.error_code="59";
                                throw new Exception("");
                            }
                            this.postalCode = this.request.getParameter(POSTAL_CODE);
                        	if(this.postalCode==null||!OscContext.isDigital(this.postalCode)){
                        	    this.failureMsg="The postal code is null or invalid.";
                        	    this.error_code="63";
                        	    throw new Exception("");
                        	}
                        	this.telephone = this.request.getParameter(TELEPHONE);
                        	if(this.telephone==null||!OscContext.isDigital(this.telephone)){
                        	    this.failureMsg="The telephone number is null or invalid.";
                        	    this.error_code="64";
                        	    throw new Exception("");
                        	}
                        }else if(i == -1){
                            this.contactName = this.request.getParameter(CONTACT_NAME);
                            if(this.contactName==null||this.contactName.equals("")){
                                this.failureMsg="The contact name is null.";
                                this.error_code="62";
                                throw new Exception("");
                            }
                            this.telephone = this.request.getParameter(TELEPHONE);
                        	if(this.telephone==null||!OscContext.isDigital(this.telephone)){
                        	    this.failureMsg="The telephone number is null or invalid.";
                        	    this.error_code="64";
                        	    throw new Exception("");
                        	}
                        }
                    }catch(Exception e){}
                }

                break;

            case 6:

                this.giftOrderId = this.request.getParameter(GIFT_ORDER_ID)!=null ?
                        this.request.getParameter(GIFT_ORDER_ID).trim():null;
                if(this.giftOrderId==null || this.giftOrderId.equals("")){
                    this.failureMsg="The gift order id is null";
                    this.error_code="60";
                }

                break;

            case 7:

                if(this.session.getAttribute(GIFT_ORDER)!=null){
                    this.giftOrder=(Properties)this.session.getAttribute(GIFT_ORDER);
                    this.giftOrder.clear();
                }

                break;

            default:
                this.pageNo 		= this.context.getPageNo(this.request);
            	this.recordsPerPage	= RECORDS_COUNT_PER_PAGE;

            	if(this.session.getAttribute(GIFT_ORDER)!=null){
                    this.giftOrder=(Properties)this.session.getAttribute(GIFT_ORDER);
                }else{
                    this.giftOrder=new Properties();
                    this.session.setAttribute(GIFT_ORDER,this.giftOrder);
                }

                break;
        }
    }

// --------------------------------------------
// Specialization of abstract methods
// --------------------------------------------

    public boolean requiresSecureRequest() {return false;}
    public boolean requiresLogin() {return true;}

    public String jspFwdUrl()
    {
        return this.redirect_jsp;
    }

    public String redirectAction()
    {
        return REDIRECT_HOME_ACTION;
    }

    public void encodeXml(PrintWriter out) {}

    private void setCache (OscCachedObject 	cache,
            			   String[][] 		recordsArray,
            			   int 				recordsPerPage,
            			   int 				recordsCount,
            			   int 				totalPages)
    {
        cache.setCached(new Pair(recordsArray,
                	    new Integer(recordsPerPage),
                	    new Integer(recordsCount),
                	    new Integer(totalPages)));
    }

    private String[][] getCachedRecordsArray(OscCachedObject cache)
    {
        final Pair p = (Pair)cache.getCached();
        return (p == null? null : (String[][])p.getRecordsArray());
    }

    private int getCachedRecordsPerPage(OscCachedObject cache)
    {
        final Pair p = (Pair)cache.getCached();
        return (p == null? 0 : ((Integer)p.getRecordsPerPage()).intValue());
    }

    private int getCachedRecordsCount(OscCachedObject cache)
    {
        final Pair p = (Pair)cache.getCached();
        return (p == null? 0 : ((Integer)p.getRecordsCount()).intValue());
    }

    private int getCachedPagesCount(OscCachedObject cache)
    {
        final Pair p = (Pair)cache.getCached();
        return (p == null? 0 : ((Integer)p.getPagesCount()).intValue());
    }

    public void serveRequest(OscLoginSession loginSession)
    	throws InvalidRequestException
    {
        if(this.point_redeem_phase == 2){

            if(this.failureMsg==null){
                long l=isStockSufficient(this.giftOrder);
                long requestedGiftPoints=totalGiftPoints(this.giftOrder);
                long requestedMailPoints=totalMailPoints(this.giftOrder);
                long totalPoints =requestedGiftPoints + requestedMailPoints;

                if(l>0){
                    this.request.setAttribute(INSUFFICIENT_GIFT_ID,Long.toString(l));
                    this.failureMsg="The stock balance is insufficient.";
                    this.error_code="57";
                }else if(totalPoints >
                        queryRedeemablePoints(loginSession.getMembershipID(),
                                loginSession.getMemberID())){
                    this.failureMsg="The balance of member account is insufficient.";
                    this.error_code="58";
                }

                if(this.failureMsg!=null){
                    this.request.setAttribute(POINT_REDEEM_NEXT_PHASE,"2");
                }

                this.request.setAttribute(REDEMPTION_CHANNELS_LIST,queryRedemptionChannels());
                this.request.setAttribute(REQUESTED_GIFT_POINTS,Long.toString(requestedGiftPoints));
                this.request.setAttribute(REQUESTED_MAIL_POINTS,Long.toString(requestedMailPoints));
                this.request.setAttribute(TOTAL_POINTS,Long.toString(totalPoints));

            }else{}

        }else if(this.point_redeem_phase == 3){

            if(this.giftOrder!=null && !this.giftOrder.isEmpty()){
            	long l=isStockSufficient(this.giftOrder);
                long requestedGiftPoints=totalGiftPoints(this.giftOrder);
                long requestedMailPoints=totalMailPoints(this.giftOrder);
                long totalPoints =requestedGiftPoints + requestedMailPoints;

                if(l>0){
                    this.request.setAttribute(INSUFFICIENT_GIFT_ID,Long.toString(l));
                    this.failureMsg="The stock balance is insufficient.";
                    this.error_code="57";
                }else if(totalPoints >
                        queryRedeemablePoints(loginSession.getMembershipID(),
                                loginSession.getMemberID())){
                    this.failureMsg="The balance of member account is insufficient.";
                    this.error_code="58";
                }

                if(this.failureMsg!=null){
                    this.request.setAttribute(POINT_REDEEM_NEXT_PHASE,"2");
                }

                this.request.setAttribute(REDEMPTION_CHANNELS_LIST,queryRedemptionChannels());
                this.request.setAttribute(REQUESTED_GIFT_POINTS,Long.toString(requestedGiftPoints));
                this.request.setAttribute(REQUESTED_MAIL_POINTS,Long.toString(requestedMailPoints));
                this.request.setAttribute(TOTAL_POINTS,Long.toString(totalPoints));
            }else{
                this.request.setAttribute(POINT_REDEEM_NEXT_PHASE,"2");
            }

        }else if(this.point_redeem_phase == 4){

            if(this.giftOrder!=null && !this.giftOrder.isEmpty()){
            	long l=isStockSufficient(this.giftOrder);
                long requestedGiftPoints = totalGiftPoints(this.giftOrder);
                long requestedMailPoints = totalMailPoints(this.giftOrder);
                long totalPoints = requestedGiftPoints + requestedMailPoints;

                if(l>0){
                    this.request.setAttribute(INSUFFICIENT_GIFT_ID,Long.toString(l));
                    this.failureMsg="The stock balance is insufficient.";
                    this.error_code="57";
                }else if(totalPoints >
                        queryRedeemablePoints(loginSession.getMembershipID(),
                                loginSession.getMemberID())){
                    this.failureMsg="The balance of member account is insufficient.";
                    this.error_code="58";
                }

                this.request.setAttribute(IS_REQUIRE_MAIL_INFO,Integer.toString(isRequireMailInfo(this.giftOrder)));
                this.request.setAttribute(REDEMPTION_CHANNELS_LIST,getSelectedChannels(this.giftOrder));
                this.request.setAttribute(REQUESTED_GIFT_POINTS,Long.toString(requestedGiftPoints));
                this.request.setAttribute(REQUESTED_MAIL_POINTS,Long.toString(requestedMailPoints));
                this.request.setAttribute(TOTAL_POINTS,Long.toString(totalPoints));
            }

        }else if(this.point_redeem_phase == 5){
            if(this.failureMsg==null){

                long l=isStockSufficient(this.giftOrder);
                long requestedGiftPoints = totalGiftPoints(this.giftOrder);
                long requestedMailPoints = totalMailPoints(this.giftOrder);
                long totalPoints = requestedGiftPoints + requestedMailPoints;

                if(l>0){
                    this.request.setAttribute(INSUFFICIENT_GIFT_ID,Long.toString(l));
                    this.failureMsg="The stock balance is insufficient.";
                    this.error_code="57";
                }else if(totalPoints >
                        queryRedeemablePoints(loginSession.getMembershipID(),
                                loginSession.getMemberID())){
                    this.failureMsg="The balance of member account is insufficient.";
                    this.error_code="58";
                }

                if(this.failureMsg!=null){
                    this.redirect_jsp = MEMBER_POINT_REDEEM_SESSION_JSP;

                    this.request.setAttribute(REDEMPTION_CHANNELS_LIST,queryRedemptionChannels());
                    this.request.setAttribute(REQUESTED_GIFT_POINTS,Long.toString(requestedGiftPoints));
                    this.request.setAttribute(REQUESTED_MAIL_POINTS,Long.toString(requestedMailPoints));
                    this.request.setAttribute(TOTAL_POINTS,Long.toString(totalPoints));

                    this.request.setAttribute(POINT_REDEEM_PREV_PHASE,"0");
                    this.request.setAttribute(POINT_REDEEM_CURR_PHASE,"3");
                    this.request.setAttribute(POINT_REDEEM_NEXT_PHASE,"2");

                }else{

                    OscDbConnection    conn = null;
                    CallableStatement cstmt = null;
                    Object      giftOrderId = null;

                    try{
                        conn = this.context.getDB(false);
                        cstmt = conn.getConnection().prepareCall(SQL_CALL_REDEEM_GIFTS);

                        ArrayDescriptor descRuleId = ArrayDescriptor.createDescriptor("NUMBER_LIST", conn.getConnection());
                        ArrayDescriptor descChannelId = ArrayDescriptor.createDescriptor("CHANNEL_ID_LIST", conn.getConnection());
                        ArrayDescriptor descQuantity = ArrayDescriptor.createDescriptor("NUMBER_LIST", conn.getConnection());

                        int arrayLength=this.giftOrder.size();

                        long[] ruleId			= new long[arrayLength];
                        long[] quantity			= new long[arrayLength];
                        String[] channelId	= new String[arrayLength];

                        Enumeration    list = this.giftOrder.elements();
                        int i=0;
                        while(list.hasMoreElements()){
                            Properties element=(Properties)list.nextElement();
                            ruleId[i]		= Long.parseLong(element.getProperty(RULE_ID,"0").toString());
                            quantity[i]		= Long.parseLong(element.getProperty(QUANTITY,"0").toString());
                            channelId[i]	= element.getProperty(CHANNEL_ID,"").toString();
                            i++;
                        }

                        ARRAY ArrayRuleId = new ARRAY(descRuleId, conn.getConnection(), ruleId);
                        ARRAY ArrayQuantity = new ARRAY(descQuantity, conn.getConnection(), quantity);
                        ARRAY ArrayChannelId = new ARRAY(descChannelId,conn.getConnection(),channelId);

                        cstmt.registerOutParameter (1, Types.VARCHAR);
                        cstmt.setLong(2,loginSession.getMembershipID());
                        cstmt.setLong(3,loginSession.getMemberID());
                        cstmt.setArray(4,ArrayRuleId);
                        cstmt.setArray(5,ArrayChannelId);
                        cstmt.setArray(6,ArrayQuantity);
                        cstmt.setString(7,this.mailingName);
                        cstmt.setString(8,this.mailingAddress);
                        cstmt.setString(9,this.postalCode);
                        cstmt.setString(10,this.contactName);
                        cstmt.setString(11,this.telephone);
                        cstmt.setString(12,this.request.getRemoteAddr());

                        cstmt.execute ();
                        giftOrderId = cstmt.getObject(1);

                        conn.getConnection().commit();

                    	conn.closeStatement(cstmt);
                    	cstmt = null;

                    }catch(SQLException e){
                        this.logger.error("Failed to call the procedure 'IQUE_MEMBER_POINT_PKG." +
                        		"REDEEM_ORDER()':" + e.getMessage());
                        throw new InvalidRequestException("Failed to call the procedure 'IQUE_" +
                        		"MEMBER_POINT_PKG.REDEEM_ORDER()':" + e.getMessage());
                    }finally{
                        if (conn != null) {
                            conn.closeStatement(cstmt);
                            conn.close();
                    	}
                    }

                    this.giftOrder.clear();
                    this.redirect_jsp = MEMBER_POINT_REDEEM_SUCC_JSP;
                    this.request.setAttribute(GIFT_ORDER_ID,giftOrderId);
                    this.request.setAttribute(POINT_REDEEM_NEXT_PHASE,"6");

                }

            }else{
                this.redirect_jsp = MEMBER_POINT_REDEEM_CONFIRM_JSP;
                this.request.setAttribute(POINT_REDEEM_PREV_PHASE,"0");
                this.request.setAttribute(POINT_REDEEM_NEXT_PHASE,"5");

                long requestedGiftPoints=totalGiftPoints(this.giftOrder);
                long requestedMailPoints=totalMailPoints(this.giftOrder);
                long totalPoints =requestedGiftPoints + requestedMailPoints;

                this.request.setAttribute(IS_REQUIRE_MAIL_INFO,Integer.toString(isRequireMailInfo(this.giftOrder)));
                this.request.setAttribute(REDEMPTION_CHANNELS_LIST,queryRedemptionChannels());
                this.request.setAttribute(REQUESTED_GIFT_POINTS,Long.toString(requestedGiftPoints));
                this.request.setAttribute(REQUESTED_MAIL_POINTS,Long.toString(requestedMailPoints));
                this.request.setAttribute(TOTAL_POINTS,Long.toString(totalPoints));
            }

        }else if(this.point_redeem_phase == 6){
            if(this.failureMsg==null){

                OscDbConnection   conn = null;
                PreparedStatement stmt = null;
                ResultSet           rs = null;

                int firstIndex	= 0;
                int secondIndex	= 7;
                String[][] giftOrderList = null;
                long giftsRequireMail=0;
                String mailChannelDesc=null;

                try{
                    conn = this.context.getDB(true);
                    stmt = conn.prepareStatement(SQL_QUERY_GIFT_ORDER_DETAIL_COUNT);
                    stmt.setString(1,this.giftOrderId);
                    stmt.setLong(2,loginSession.getMembershipID());
                    stmt.setLong(3,loginSession.getMemberID());

                    rs=stmt.executeQuery();
                    if(rs.next()){
                        firstIndex = rs.getInt(1);
                    }

                    rs.close();
                    conn.closeStatement(stmt);
                    stmt=null;

                    giftOrderList=new String[firstIndex+1][secondIndex];

                    stmt = conn.prepareStatement(SQL_QUERY_GIFT_ORDER_DETAIL);
                    stmt.setString(1,this.giftOrderId);
                    stmt.setLong(2,loginSession.getMembershipID());
                    stmt.setLong(3,loginSession.getMemberID());
                    rs=stmt.executeQuery();

                    int i=0;
                    while(rs.next()){
                        giftOrderList[i][0]=rs.getString("order_date");
                        giftOrderList[i][1]=rs.getString("rule_desc");
                        giftOrderList[i][2]=Long.toString(-rs.getLong("points"));
                        giftOrderList[i][3]=rs.getString("quantity");
                        giftOrderList[i][4]=rs.getString("channel_desc");
                        giftOrderList[i][5]=rs.getString("status");
                        giftOrderList[i][6]=rs.getString("action_date");
                        if(rs.getString("channel_id").equals("ML")){
                            giftsRequireMail+=rs.getLong("quantity");
                            mailChannelDesc=rs.getString("channel_desc");
                        }
                        i++;
                    }

                    rs.close();
                    conn.closeStatement(stmt);
                    stmt=null;

                }catch(SQLException e){
                    this.logger.error("Failed to query gift order in " +
                    		"IQUE DB: " + e.getMessage());
                    throw new InvalidRequestException("Failed to query " +
                    		"gift order in IQUE DB: " + e.getMessage());
                }finally{
                    if (conn != null) {
                        conn.closeStatement(stmt);
                        conn.close();
                    }
                }

                if(giftsRequireMail>0){
                    long giftsPerMail=queryGiftsPerMail();
                    long mailsQtyRequire=(giftsRequireMail%giftsPerMail) != 0 ?
                            giftsRequireMail/giftsPerMail+1 : giftsRequireMail/giftsPerMail;
                    long pointsPerMail  = -queryPointsPerMail();

                    giftOrderList[giftOrderList.length-1][0]=giftOrderList[0][0];
                    giftOrderList[giftOrderList.length-1][1]=mailChannelDesc;
                    giftOrderList[giftOrderList.length-1][2]=Long.toString(pointsPerMail);
                    giftOrderList[giftOrderList.length-1][3]=Long.toString(mailsQtyRequire);
                }

                this.request.setAttribute(GIFT_ORDER_LIST,giftOrderList);

            }else{}

            this.redirect_jsp = MEMBER_POINT_REDEEM_QUERY_ORDER_JSP;
        }
        else{

            OscCachedObject cache    = null;
            Integer	regionID 		 = null;
            long      timeout  		 = 0;

            try {
                timeout 	= this.context.getContentCacheTimeoutMsecs();
                regionID 	= this.context.getStoreRegionId(this.storeID);
            }catch(OscStore.InvalidStore e){
                this.logger.error("Failed to query redeemalbe list:" + e.getMessage());
                throw new InvalidRequestException("Failed to query redeemalbe list:");
            }

            if (timeout > 0) {
                cache = CACHED_REDEEMABLE_LIST.getMapped(regionID.toString());
                if (cache.isValid(timeout)) {
                    this.redeemableGiftsList = getCachedRecordsArray(cache);
                    this.recordsPerPage = getCachedRecordsPerPage(cache);
                    this.recordsCount = getCachedRecordsCount(cache);
                    this.pagesCount = getCachedPagesCount(cache);
                }
            }

            if ( this.redeemableGiftsList == null )
            {
                OscDbConnection   conn = null;
                PreparedStatement stmt = null;
                ResultSet           rs = null;

                int firstIndex  = 0;
                int secondIndex = 3+1;

                try {
                    conn = this.context.getDB(true);
                    stmt = conn.prepareStatement(SQL_QUERY_REDEEMABLE_LIST_COUNT);
                    stmt.setInt(1,regionID.intValue());
                    rs = stmt.executeQuery();

                    if(rs.next()){
                        firstIndex=rs.getInt(1);
                    }

                    rs.close();
                    conn.closeStatement(stmt);
                    stmt=null;

                    this.redeemableGiftsList = new String[firstIndex][secondIndex];
                    this.recordsCount = firstIndex;
                    this.pagesCount = this.recordsCount%this.recordsPerPage !=0 ?
                            	      this.recordsCount/this.recordsPerPage+1 :
                            	      this.recordsCount/this.recordsPerPage;


                    stmt = conn.prepareStatement(SQL_QUERY_REDEEMABLE_LIST);
                    stmt.setInt(1,regionID.intValue());
                    rs = stmt.executeQuery();

                    int i=0;
                    while(rs.next()) {
                        this.redeemableGiftsList[i][0]=rs.getString("rule_id");
                        this.redeemableGiftsList[i][1]=rs.getString("rule_desc");
                        this.redeemableGiftsList[i][2]=Long.toString(-rs.getLong("points"));
                        this.redeemableGiftsList[i][3]=getGiftImageFileName(rs.getLong("rule_id"));
                        i++;
                    }

                    rs.close();
                    conn.closeStatement(stmt);
                    stmt=null;

                }catch (SQLException e) {
                    this.logger.error("Failed to query redeemalbe list:" + e.getMessage());
                    throw new InvalidRequestException("Failed to query redeemalbe list:");
                }
                finally {
                    if (conn != null) {
                        conn.closeStatement(stmt);
                        conn.close();
                	}
                }

                if (cache != null){
                    setCache(cache, this.redeemableGiftsList, this.recordsPerPage, this.recordsCount, this.pagesCount);
                }
            }

            this.redirect_jsp = MEMBER_POINT_REDEEM_LIST_JSP;
            this.request.setAttribute(REDEEMABLE_GIFTS_LIST,getSpecialPageList(this.redeemableGiftsList,this.pageNo,this.recordsPerPage));
            this.request.setAttribute(REDEMPTION_CHANNELS_LIST,queryRedemptionChannels());
            this.request.setAttribute(RECORDS_COUNT,Integer.toString(this.recordsCount));
            this.request.setAttribute(PAGES_COUNT,Integer.toString(this.pagesCount));
            this.request.setAttribute(CURRENT_PAGE_NO,Integer.toString(this.pageNo));

            this.request.setAttribute(POINT_REDEEM_CURR_PHASE,"1");
            this.request.setAttribute(POINT_REDEEM_NEXT_PHASE,"2");

        }
    }
    private String[][] getSpecialPageList(String[][]allList,int pageNo,int recordsPerPage)
    {
        if(allList==null) return null;
        if(allList.length<=recordsPerPage)return allList;

        int firstIndex=0;
        int secondIndex=allList[0].length;
        int recordsCount=allList.length;

        int pagesCount=recordsCount%recordsPerPage!=0?recordsCount/recordsPerPage+1:recordsCount/recordsPerPage;
        if(pageNo+1==pagesCount){
            firstIndex=recordsCount%recordsPerPage!=0?recordsCount%recordsPerPage:recordsPerPage;
        }else if(pageNo+1<pagesCount){
            firstIndex=recordsPerPage;
        }else{
            return null;
        }

        String[][]resultArray=new String[firstIndex][secondIndex];
        for(int i=0;i<firstIndex;i++){
            for(int j=0;j<secondIndex;j++){
                resultArray[i][j]=allList[pageNo*recordsPerPage+i][j];
            }
        }
        return resultArray;
    }

    private long isStockSufficient(Properties prop)
    	throws InvalidRequestException
    {
        if(prop==null)return 0;

        OscDbConnection   conn = null;
        PreparedStatement stmt = null;
        ResultSet           rs = null;

        long returnValue = 0;
        Enumeration    list = prop.elements();
        try{
            conn = this.context.getDB(true);
            stmt = conn.prepareStatement(SQL_QUERY_STOCK);

            while(list.hasMoreElements()){
                Properties element=(Properties)list.nextElement();
                stmt.setLong(1,Long.parseLong(element.getProperty(RULE_ID,"0")));
                stmt.setString(2,element.getProperty(CHANNEL_ID,""));
                rs = stmt.executeQuery();

                if(!rs.next() || Long.parseLong(element.getProperty(QUANTITY,"0"))>rs.getLong(1)){
                    returnValue = Long.parseLong(element.getProperty(RULE_ID,"0"));
                    rs.close();
                    break;
                }
                rs.close();
            }

            conn.closeStatement(stmt);
            stmt=null;

        }catch(SQLException e){
            this.logger.error("long isStockSufficient(Properties prop): Failed to " +
                    "query gift stocks in IQUE DB: " + e.getMessage());
            throw new InvalidRequestException("Failed to query " +
            		"gift stocks in IQUE DB: " + e.getMessage());
        }finally{
            if (conn != null) {
                conn.closeStatement(stmt);
                conn.close();
            }
        }
        return returnValue;
    }

    private boolean isStockSufficient(String giftId, String quantity, String channelId)
		throws InvalidRequestException
	{
        OscDbConnection   conn = null;
        PreparedStatement stmt = null;
        ResultSet           rs = null;

        boolean returnValue = true;

        try{
            conn = this.context.getDB(true);
            stmt = conn.prepareStatement(SQL_QUERY_STOCK);
            stmt.setLong(1,Long.parseLong(giftId));
            stmt.setString(2,channelId);
            rs = stmt.executeQuery();

            if(!rs.next()||Long.parseLong(quantity) > rs.getLong(1)){
                returnValue = false;
            }

            rs.close();
            conn.closeStatement(stmt);
            stmt=null;

        }catch(SQLException e){
            this.logger.error("boolean isStockSufficient(String giftId, String quantity, " +
                    "String channelId): Failed to query gift stocks in IQUE DB: " + e.getMessage());
    		throw new InvalidRequestException("Failed to query " +
        			"gift stocks in IQUE DB: " + e.getMessage());
    	}finally{
    	    if (conn != null) {
    	        conn.closeStatement(stmt);
    	        conn.close();
    	    }
    	}
    	return returnValue;
	}

    private long totalRequestMailCount(Properties prop)
    	throws InvalidRequestException
    {
        if(prop==null)return 0;

        long giftsRequestMail = 0;
        Enumeration list = prop.elements();

        while(list.hasMoreElements()){
            Properties element=(Properties)list.nextElement();
            if(element.getProperty(CHANNEL_ID,"").equals(ML)){
                giftsRequestMail += Long.parseLong(element.getProperty(QUANTITY,"0"));
            }
        }
        long giftsPerMail=queryGiftsPerMail();
        long returnValue=(giftsRequestMail%giftsPerMail) != 0 ?
                giftsRequestMail/giftsPerMail+1 : giftsRequestMail/giftsPerMail;

        return returnValue;
    }

    private int isRequireMailInfo(Properties prop)
    {
        if(prop==null)return 0;
        Enumeration list = prop.elements();

        int requireMailingTimes = 0;
        int unRequireMailingTimes = 0;
        int returnValue = 0;

        while(list.hasMoreElements()){
            Properties element=(Properties)list.nextElement();
            if(element.getProperty(CHANNEL_ID,"").equals(ML)){
                requireMailingTimes ++;
            }else{
                unRequireMailingTimes ++;
            }
        }
        if(requireMailingTimes==0){
            returnValue = -1;	//The '-1' represent that all gifts don't require mailing.
        }else if(unRequireMailingTimes==0){
            returnValue = +1;   //The '+1' represent that all gifts require mailing.
        }else{
            returnValue = 2;    //The '2' represent that one part of gifts require mailing,
            					//the other part of gifts don't require mailing.
        }
        return returnValue;
    }

    private long queryGiftsPerMail()
    	throws InvalidRequestException
    {
        OscDbConnection   conn = null;
        PreparedStatement stmt = null;
        ResultSet           rs = null;

        long giftsPerMail = 0;
        try{
            conn = this.context.getDB(true);
            stmt = conn.prepareStatement(SQL_QUERY_GIFTS_PER_MAIL);
            rs=stmt.executeQuery();
            if(rs.next()){
                giftsPerMail=rs.getLong(1);
            }
            rs.close();
            conn.closeStatement(stmt);
            stmt=null;

        }catch(SQLException e){
            this.logger.error("long queryGiftsPerMail(): Failed to query mail " +
                    "points in IQUE DB: " + e.getMessage());
            throw new InvalidRequestException("Failed to query " +
            		"mail points in IQUE DB: " + e.getMessage());
        }finally{
            if (conn != null) {
                conn.closeStatement(stmt);
                conn.close();
            }
        }

        return giftsPerMail;
    }

    private long totalMailPoints(Properties prop)
    	throws InvalidRequestException
    {
        if(prop==null)return 0;
        long pointsPerMail  = queryPointsPerMail();
        long mailCount = totalRequestMailCount(prop);
        return -mailCount*pointsPerMail;
    }

    private long queryPointsPerMail()
		throws InvalidRequestException
	{
        OscDbConnection   conn = null;
        PreparedStatement stmt = null;
        ResultSet           rs = null;

    	long pointsPerMail = 10;

    	try{
    	    conn = this.context.getDB(true);
    	    stmt = conn.prepareStatement(SQL_QUERY_MAIL_POINTS);
    	    rs=stmt.executeQuery();

    	    if(rs.next()){
    	        pointsPerMail=rs.getLong(1);
    	    }

    	    rs.close();
        	conn.closeStatement(stmt);
        	stmt=null;

    	}catch(SQLException e){
    	    this.logger.error("long queryPointsPerMail(): Failed to query " +
                    "mail points in IQUE DB: " + e.getMessage());
    	    throw new InvalidRequestException("Failed to query " +
    	            "mail points in IQUE DB: " + e.getMessage());
    	}finally{
    	    if (conn != null) {
    	        conn.closeStatement(stmt);
    	        conn.close();
    	    }
    	}
    	return pointsPerMail;
	}

    private long totalGiftPoints(Properties prop)
    	throws InvalidRequestException
    {
        if(prop==null)return 0;
        Enumeration list = prop.elements();
        long giftPoints = 0;
        while(list.hasMoreElements()){
            Properties element = (Properties)list.nextElement();
            long points = queryPointsOfTheGift(Long.parseLong(element.getProperty(RULE_ID,"0")));
            if(points > 0 ){
                this.logger.error("long totalGiftPoints(Properties prop): The points of the " +
                		"rule_id '"+element.getProperty(RULE_ID,"0") + "' must be negative or " +
                                "zero in iQue DB.");
                throw new InvalidRequestException("The points of the rule_id '" + element.getProperty(RULE_ID,"0") +
                        "' must be negative or zero in iQue DB.");
            }
            giftPoints += points * Long.parseLong(element.getProperty(QUANTITY,"0"));
        }
        return -giftPoints;
    }


    private long queryPointsOfTheGift(long ruleId)
    	throws InvalidRequestException
    {
        OscDbConnection   conn = null;
        PreparedStatement stmt = null;
        ResultSet           rs = null;

        long returnValue = 10;

        try{
            conn = this.context.getDB(true);
            stmt = conn.prepareStatement(SQL_QUERY_GIFT_POINTS);
            stmt.setLong(1,ruleId);
            rs=stmt.executeQuery();

            if(rs.next()){
                returnValue = rs.getLong(1);
            }

            rs.close();
            conn.closeStatement(stmt);
            stmt=null;

        }catch(SQLException e){
            this.logger.error("long queryPointsOfTheGift(long ruleId): Failed to " +
                    "query gift points in IQUE DB: " + e.getMessage());
            throw new InvalidRequestException("Failed to query " +
            		"gift points in IQUE DB: " + e.getMessage());
        }finally{
            if (conn != null) {
                conn.closeStatement(stmt);
                conn.close();
            }
        }
        return returnValue;
    }

    private long queryRedeemablePoints(long membership_id,long member_id)
		throws InvalidRequestException
	{
        OscDbConnection   conn 	= null;
        CallableStatement cstmt = null;
        long returnValue = 0;

        try {
            conn = this.context.getDB(true);
            cstmt = conn.prepareCall(SQL_QUERY_REDEEMABLE_POINTS);
            cstmt.registerOutParameter(1,Types.BIGINT);
            cstmt.setLong(2,membership_id);
            cstmt.setLong(3,member_id);
            cstmt.execute();

            returnValue = cstmt.getLong(1);

            conn.closeStatement(cstmt);
            cstmt=null;
        }
        catch (SQLException e) {
            this.logger.error("long queryRedeemablePoints(long membership_id,long member_id): " +
                    "Failed to query redeemable points: " + e.getMessage());
            throw new InvalidRequestException("Failed to query redeemable points: "
                    + e.getMessage());
        }
        finally{
            if (conn != null) {
                conn.closeStatement(cstmt);
                conn.close();
            }
        }
        return returnValue;
	}

    private String[][] queryRedemptionChannels()
		throws InvalidRequestException
	{
        OscDbConnection   conn = null;
        PreparedStatement stmt = null;
        ResultSet           rs = null;
        String[][] resultArray = null;

        int firstIndex		   = 0;
        int secondIndex		   = 5;

        try{
            conn = this.context.getDB(true);
            firstIndex = conn.GetRecordCount(SQL_QUERY_REDEEM_CHANNELS);
            resultArray=new String[firstIndex][secondIndex];

            stmt = conn.prepareStatement(SQL_QUERY_REDEEM_CHANNELS);
            rs=stmt.executeQuery();

            int i=0;
            while(rs.next()){
                for(int j=0;j<secondIndex;j++){
                    resultArray[i][j]=rs.getString(j+1);
                }
                i++;
            }

            rs.close();
            conn.closeStatement(stmt);
            stmt=null;

    	}catch(SQLException e){
    	    this.logger.error("String[][] queryRedeemingChannels(): Failed to " +
                    "query redeem channels in IQUE DB: " + e.getMessage());
    	    throw new InvalidRequestException("Failed to query " +
    	            "redeem channels in IQUE DB: " + e.getMessage());
    	}finally{
    	    if (conn != null) {
    	        conn.closeStatement(stmt);
    	        conn.close();
    	    }
    	}
    	return resultArray;
	}

    private String getGiftImageFileName(long giftId)
    {
        return "http://www.ique.com/gift_img/img_" + Long.toString(giftId) + ".gif";
    }

    private String[][] getSelectedChannels(Properties prop)
        throws InvalidRequestException
    {
        if(prop==null)return null;
        String[][] redemptionChannelsList = queryRedemptionChannels();
        if(redemptionChannelsList==null)return null;

        int firstIndex	=	redemptionChannelsList.length;
        int secondIndex	=	redemptionChannelsList[0].length;

        String[][] temp=new String[firstIndex][secondIndex+1];

        for(int i=0;i<firstIndex;i++){
            for(int j=0;j<secondIndex;j++){
                temp[i][j]=redemptionChannelsList[i][j];
            }
            temp[i][secondIndex]="0";
        }

        Enumeration list = prop.elements();
        int count=0;
        while(list.hasMoreElements()){
            Properties element=(Properties)list.nextElement();
            for(int x=0;x<firstIndex;x++){
                if(temp[x][0].equals(element.getProperty(CHANNEL_ID,"")) && 
                        !temp[x][secondIndex].equals("1"))
                {
                    temp[x][secondIndex]="1";
                    count++;                                        
                }
            }
        }
        String[][] resultArray=new String[count][secondIndex];
        int o=0;
        for(int y=0;y<firstIndex;y++){
            if(temp[y][secondIndex].equals("1"))
            {
                for(int p=0;p<secondIndex;p++){
                    resultArray[o][p]=temp[y][p];
                }
                o++;
            }
        }
        return resultArray;
    }


    public boolean getIsError() {return this.failureMsg != null;}
    public String getErrorMsg() {return this.failureMsg;}
    public String getErrorCode(){return this.error_code;}

}

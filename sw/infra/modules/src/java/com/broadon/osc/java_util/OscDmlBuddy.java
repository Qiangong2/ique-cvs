package com.broadon.osc.java_util;

import java.sql.*;

/**
 * Data manipulation library functions defined here pertain to buddy list
 */
public class OscDmlBuddy
{
    /** Define all SQL statements
     */

    private static final String TABLE_BUDDY_LISTS = "IQUE_MEMBER_BUDDY_LISTS";

    private static final String GET_MEMBER_INFO =
                "SELECT membership_id, member_id, member_email, pseudonym, nick_name FROM IQUE_MEMBERS ";

    private static final String GET_BUDDY_LIST =
                "SELECT * FROM IQUEM_BUDDY_LIST_ALL_V ";

    private static final String GET_REV_BUDDY_LIST =
                "SELECT * FROM IQUEM_BUDDY_LIST_REVERSE_V ";

    private static final String GET_MEMBER_LOGIN_OLD_DETAILS =
                "SELECT membership_id, member_id, member_email, pseudonym, " +
                "name, gender, personal_id, status, nick_name FROM IQUE_MEMBERS ";

    private static final String GET_MEMBER_LOGIN_DETAILS =
                "SELECT membership_id, member_id, member_email, pseudonym, " +
                "name, gender, personal_id, status, nick_name, " +
                "CAST(MULTISET(SELECT * FROM iquem_buddy_list_reverse_v " +
                "WHERE iqm.membership_id=my_membership_id and iqm.member_id=my_member_id and reverse_flags in (2,3) and my_buddy_flags < 4) " +
                "AS BuddyList) REVERSE_LIST " +
                "FROM IQUE_MEMBERS iqm ";

    private static final String CALLABLE_BUDDY_FSM =
                "{? = call budops.buddyStateMachine(?,?,?,?,?)}";

    private static final String CALLABLE_BUDDY_UPDATE =
                "{? = call budops.setBuddyState(?,?,?,?,?)}";


    public static String getMemberInfo(OscDbConnection conn, 
			               String pseudo)
    	throws Exception
    {
        return conn.queryXML(GET_MEMBER_INFO +
                    " WHERE pseudonym = '" + pseudo + "'");
    }

    public static String getMemberInfo(OscDbConnection conn, 
			               String membership_id,
			               String member_id)
    	throws Exception
    {
        return conn.queryXML(GET_MEMBER_INFO +
                    " WHERE membership_id = " + membership_id + " AND member_id = " + member_id);
    }

    public static String getBuddyList(OscDbConnection conn, 
			              String membership_id, 
			              String member_id,
                                      int numrows,
                                      int skiprows)
    	throws Exception
    {
        return conn.queryXML(GET_BUDDY_LIST +
                    " WHERE my_membership_id = " + membership_id +
                    " AND my_member_id = " + member_id +
                    " ORDER BY group_name", numrows, skiprows);
    }

    public static String getReverseBuddyList(OscDbConnection conn, 
			              String membership_id, 
			              String member_id)
    	throws Exception
    {
        return conn.queryXML(GET_REV_BUDDY_LIST +
                    " WHERE my_membership_id = " + membership_id +
                    " AND my_member_id = " + member_id +
                    " ORDER BY group_name");
    }

    public static int countBuddyList(OscDbConnection conn, 
			             String membership_id, 
			             String member_id)
    	throws Exception
    {
        return conn.GetRecordCount(GET_BUDDY_LIST +
                    " WHERE my_membership_id = " + membership_id +
                    " AND my_member_id = " + member_id);
    }

    public static String getMemberLoginDetails(OscDbConnection conn, 
			                       String membership_id, 
			                       String member_id)
    	throws Exception
    {
        return conn.queryXML(GET_MEMBER_LOGIN_DETAILS +
                    " WHERE membership_id = " + membership_id +
                    " AND member_id = " + member_id);
    }

    public static int updateBuddyList(OscDbConnection conn,
                                      String membership_id,
                                      String member_id,
                                      String buddy_membership_id,
                                      String buddy_member_id,
                                      String buddy_info)
        throws Exception
    {
        String xmlData = "<ROWSET><ROW>" +
                         "<MEMBERSHIP_ID>" + membership_id + "</MEMBERSHIP_ID>" +
                         "<MEMBER_ID>" + member_id + "</MEMBER_ID>" +
                         "<BUDDY_MEMBERSHIP_ID>" + buddy_membership_id + "</BUDDY_MEMBERSHIP_ID>" +
                         "<BUDDY_MEMBER_ID>" + buddy_member_id + "</BUDDY_MEMBER_ID>" +
                         buddy_info + "</ROW></ROWSET>";
        return conn.updateXML(xmlData, TABLE_BUDDY_LISTS);
    }

    public static String updateBuddyRec(OscDbConnection conn, 
			                long myMembershipID, 
			                long myMemberID,
			                long buddyMembershipID, 
			                long buddyMemberID,
			                long buddyFlags)
    	throws Exception
    {
        String result = null;
        CallableStatement stmt = null;
        stmt = conn.prepareCall(CALLABLE_BUDDY_UPDATE);
        stmt.registerOutParameter(1, Types.VARCHAR);
        stmt.setLong(2, myMembershipID);
        stmt.setLong(3, myMemberID);
        stmt.setLong(4, buddyMembershipID);
        stmt.setLong(5, buddyMemberID);
        stmt.setLong(6, buddyFlags);
        stmt.execute();
        result = stmt.getString(1);
        conn.closeStatement(stmt);
        return result;
    }

    public static String buddyStateMachine(OscDbConnection conn, 
			                long myMembershipID, 
			                long myMemberID,
			                long buddyMembershipID, 
			                long buddyMemberID,
			                String myAction)
    	throws Exception
    {
        String result = null;
        CallableStatement stmt = null;

        stmt = conn.prepareCall(CALLABLE_BUDDY_FSM);
        stmt.registerOutParameter(1, Types.VARCHAR);
        stmt.setLong(2, myMembershipID);
        stmt.setLong(3, myMemberID);
        stmt.setLong(4, buddyMembershipID);
        stmt.setLong(5, buddyMemberID);
        stmt.setString(6, myAction);
        stmt.execute();

        result = stmt.getString(1);
        conn.closeStatement(stmt);
        return result;
    }

} // OscDmlBuddy

package com.broadon.osc.java_util;

import java.util.*;
import java.security.*;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.*;

import com.broadon.util.*;

/** This class represents the cookie information and provides information
 *  for encoding the cookie as a string, decoding it from the string, 
 *  accessing the cookie values, and determining login state (logged in/out)
 *  from the cookie values.
 */
public class OscCookie {

    // ----------------
    // Invariant values
    // ----------------

    private static final String     NO_AUTHCODE = "*";
    private static final byte[]     MASTER_SECRET = {17, 37, 12, 19, -123,
                                                     44, 127, 55, -29, 103,
                                                     -23, 111, 3, -71, 23,
                                                     -19, -121, -91, 57, 9};

    private static final long       MEMBERSHIP_BITS = 40;
    private static final long       MEMBER_BITS     = 24;
    private static final long       MEMBERSHIP_MASK = 0x000000FFFFFFFFFFL;
    private static final long       MEMBER_MASK     = 0x0000000000FFFFFFL;
    private static final byte[]     ODD_PARITY_BYTEMAP;
    public  static final String     LOGGEDOUT_STRING;
    public  static final OscCookie  LOGGEDOUT_COOKIE;

    static {
        LOGGEDOUT_COOKIE = new OscCookie(0,0,0,0,0,NO_AUTHCODE,"","");
        LOGGEDOUT_STRING = LOGGEDOUT_COOKIE.encode();

        ODD_PARITY_BYTEMAP = new byte[256];
        for (int i = 0; i < ODD_PARITY_BYTEMAP.length; ++i) {
            int nbits = 0;
            for (int j = 1; j < 8; ++j) nbits += (i >> j) & 1;

            // If nbits is odd, set least significant bit to 0, else to 1
            //
            ODD_PARITY_BYTEMAP[i] = 
                (byte)((i & 0xFE) | ((nbits & 1) ^ 1));
        }
    }

    // ----------------
    // Member variables
    // ----------------

    private long   sessID;      // Random session id (0 => logged out)
    private long   accountID;   // 40 bits Membership_id + 24 bits Member_id
    private long   startMsec;   // == LoginInfo.startMsec
    private long   lastMsec;    // <= LoginInfo.lastMsec
    private long   seqNum;      // <= LoginInfo.seqNum
    private String pseudonym;   // Login name
    private String authCodeMD5; // MD5 encoded auth code used upon login
    private String encryptedSessID;


    // --------------------------------------------
    // Member utility methods (hidden)
    // --------------------------------------------

    private static byte [] getMD5(String val) throws InvalidCookieException
    {
        // Get MD5 digest (16 bytes)
        //
        try{
            final MessageDigest md5 = MessageDigest.getInstance("MD5");
            md5.update(MASTER_SECRET);
            return md5.digest(val.getBytes());
        }
        catch (java.security.NoSuchAlgorithmException e) {
            throw new InvalidCookieException("Cannot find MD5 algorithm", e);
        }  
    }


    private static SecretKey getSessionKey(long sessionStartTime)
        throws InvalidCookieException
    {
        try {
            // Get MD5 digest (16 bytes)
            //
            final byte[] digest = 
                getMD5(new Long(sessionStartTime).toString());

            // Move the bytes into the key array (24 bytes), duplicating 
            // the digest bytes if necessary.
            //
            byte[] key = new byte[DESedeKeySpec.DES_EDE_KEY_LEN];
            int ncopy = 0;
            for (int remains = key.length; remains > 0; remains -= ncopy) {
                ncopy = (remains > digest.length? digest.length : remains);
                System.arraycopy(digest, 0, key, key.length-remains, ncopy);
            }

            // Convert each byte to have an odd number of bits set.
            //
            for (int i = 0; i < key.length; ++i)
                key[i] = ODD_PARITY_BYTEMAP[key[i] & 0xFF];

            // Construct the secret key
            //
            final SecretKeyFactory kfact = 
                SecretKeyFactory.getInstance("DESede");

            return kfact.generateSecret(new DESedeKeySpec(key));
        } 
        catch (java.security.NoSuchAlgorithmException e) {
            throw new InvalidCookieException("Cannot find MD5 algorithm", e);
        }  
        catch (java.security.InvalidKeyException e) {
            throw new InvalidCookieException("Cannot generate DESede key", e);
        }
        catch (java.security.spec.InvalidKeySpecException e) {
            throw new InvalidCookieException("Cannot generate DESede key", e);
        }
    } // getSessionKey


    private static String getSessionEncrypted(long sessionStartTime,
                                              long sessionID)
        throws InvalidCookieException
    {
        SecretKey key = getSessionKey(sessionStartTime);
        try {
            Cipher des = Cipher.getInstance("DESede");
            des.init(Cipher.ENCRYPT_MODE, key);

            final byte[] sessBytes = 
                new Long(sessionID).toString().getBytes("UTF-8");

            return new Base64().encode(des.doFinal(sessBytes));
        }
        catch (java.security.NoSuchAlgorithmException e) {
            throw new InvalidCookieException("Cannot find MD5 algorithm", e);
        } 
        catch (java.security.InvalidKeyException e) {
            throw new InvalidCookieException("Cannot generate DESede key", e);
        }
        catch (java.io.UnsupportedEncodingException e) {
            throw new InvalidCookieException("Cannot decode sessionID", e);
        }
        catch (javax.crypto.IllegalBlockSizeException e) {
            throw new InvalidCookieException("Cannot decode sessionID", e);
        } 
        catch (javax.crypto.BadPaddingException e) {
            throw new InvalidCookieException("Cannot decode sessionID", e);
        } 
        catch (javax.crypto.NoSuchPaddingException e) {
            throw new InvalidCookieException("Cannot decode sessionID", e);
        }
    }


    private static long getSessionDecrypted(long   sessionStartTime,
                                            String encryptedSessionID)
        throws InvalidCookieException
    {
        SecretKey key = getSessionKey(sessionStartTime);
        try {
            Cipher des = Cipher.getInstance("DESede");
            des.init(Cipher.DECRYPT_MODE, key);

            final byte[] sessBytes = new Base64().decode(encryptedSessionID);
            final byte[] decryptedBytes = des.doFinal(sessBytes);
            final String sessIDstr = new String(decryptedBytes, "UTF-8");
            return Long.parseLong(sessIDstr);
        }
        catch (java.io.IOException e) {
            throw new InvalidCookieException("Cannot decode sessionID", e);
        }
        catch (java.security.NoSuchAlgorithmException e) {
            throw new InvalidCookieException("Cannot find MD5 algorithm", e);
        } 
        catch (java.security.InvalidKeyException e) {
            throw new InvalidCookieException("Cannot generate DESede key", e);
        }
        catch (javax.crypto.IllegalBlockSizeException e) {
            throw new InvalidCookieException("Cannot decode sessionID", e);
        } 
        catch (javax.crypto.BadPaddingException e) {
            throw new InvalidCookieException("Cannot decode sessionID", e);
        }
        catch (javax.crypto.NoSuchPaddingException e) {
            throw new InvalidCookieException("Cannot decode sessionID", e);
        }
    }

    private OscCookie(long sid, 
                      long accID, 
                      long startMs, 
                      long lastMs, 
                      long seq,
                      String authCode,
                      String pseud,
                      String encrSessID) 
    {
        sessID = sid; 
        accountID = accID; 
        startMsec = startMs; 
        lastMsec = lastMs; 
        seqNum = seq;
        authCodeMD5 = authCode;
        pseudonym = pseud;
        encryptedSessID = encrSessID;
    }


    // ------------------------------------------------
    // Public part
    // ------------------------------------------------

    static public class InvalidCookieException extends OscException {
        
        public InvalidCookieException() {super();}
        public InvalidCookieException(String message) {super(message);}
        public InvalidCookieException(String message, Throwable throwable) {
            super(message, throwable);
        }
    }


    /** The way to reconstruct a OscCookie object from an encoded cookie
     *  value.
     */
    public static OscCookie decode(String encoded)
        throws InvalidCookieException
    {
        OscCookie info = LOGGEDOUT_COOKIE;
        if (!encoded.equals(LOGGEDOUT_STRING)) {
            //
            // A valid cookie
            //
            try {
                StringTokenizer t = new StringTokenizer(encoded, "|");

                // get the encrypted sessionID
                //
                String encryptedSessID = (encoded.charAt(0) == '|' ?
                                          "" : t.nextToken());

                // get the 4 numeric values
                //
                long accountID = Long.parseLong(t.nextToken());
                long sessStartMsec = Long.parseLong(t.nextToken());
                long sessLastMsec = Long.parseLong(t.nextToken());
                long sessSeqv = Long.parseLong(t.nextToken());

                // get the MD5 encoded authCode
                //
                String authCode = t.nextToken();

                // get the psudonym
                //
                String pseudonym = (t.hasMoreTokens()? t.nextToken() : "");
                while (t.hasMoreTokens())
                    pseudonym += "|" + t.nextToken();

                // decrypted the sessionID
                //
                long sid = 0;
                if (encryptedSessID.length() > 0)
                    sid = getSessionDecrypted(sessStartMsec, encryptedSessID);

                // Create cookie
                //
                info = new OscCookie(sid,
                                     accountID,
                                     sessStartMsec,
                                     sessLastMsec,
                                     sessSeqv,
                                     authCode,
                                     pseudonym,
                                     encryptedSessID);
            }
            catch (NoSuchElementException e) {
                throw new InvalidCookieException("Missing value in cookie(" +
                                                 encoded + ")",
                                                 e);
            }
        }
        return info;
    }


    /** The way to reconstruct a OscCookie object upon login
     *  value.
     */
    public OscCookie(long   sid, 
                     long   msId, 
                     long   memId, 
                     long   startMs, 
                     long   lastMs, 
                     long   seq, 
                     String pseud)
        throws InvalidCookieException
    {
        this(sid,
             (msId << MEMBER_BITS) | (memId & MEMBER_MASK),
             startMs, lastMs, seq, NO_AUTHCODE, pseud, 
             getSessionEncrypted(startMs, sid));
    }


    /** Encoding the cookie as a string, exactly as it is represented here
     */
    public String encode()
    {
        return (encryptedSessID + "|" + accountID + "|" + startMsec + 
                "|" + lastMsec + "|" + seqNum + "|" + authCodeMD5 + 
                "|" + pseudonym);
    }


    /** Encoding the cookie as a string, using the given last update time and
     *  sequence number.
     */
    public String encode(long lastUpdateTime, long seqNum)
    {
        // The LoginInfo may be more up to date about last accesses
        //
        return (encryptedSessID + 
                "|" + accountID + 
                "|" + startMsec + 
                "|" + lastUpdateTime +
                "|" + seqNum +
                "|" + authCodeMD5 +
                "|" + pseudonym);
    }

    public boolean isLoggedOut() 
    {
        return this.sessID == 0;
    }

    public long getMembershipID() 
    {
        return (accountID >> MEMBER_BITS) & MEMBERSHIP_MASK;
    }
    public long   getMemberID()  {return (accountID & MEMBER_MASK);}
    public long   getSessionID() {return sessID;}
    public long   getStartMsec() {return startMsec;}
    public long   getLastMsec()  {return lastMsec;}
    public long   getSeqNum()    {return seqNum;}
    public String getPseudonym() {return pseudonym;}

    public void setLastMsec(long t) {lastMsec = t;}
    public void setSeqNum(long s) {seqNum = s;}

    /** We set the authCode in the AuthCode servlet, and reset it upon 
     *  a login attempt (whether or not successful).
     */
    public void resetAuthCode() {authCodeMD5 = NO_AUTHCODE;}
    public void setAuthCode(String code) throws InvalidCookieException
    {
        // Get MD5 digest (16 bytes) encoded in Base64 form
        //
        authCodeMD5 = new Base64().encode(getMD5(code));
    }
    public boolean isAuthCode(String code) throws InvalidCookieException
    {
        // Compare MD5 digest (16 bytes) encoded in Base64 form
        //
        final String s = new Base64().encode(getMD5(code));
        return s.equals(authCodeMD5);
    }

    /** For debugging
     */
    public String toString()
    {
        StringBuffer b = new StringBuffer();
        b.append("[");
        b.append(getSessionID());
        b.append("|(");
        b.append(getMembershipID());
        b.append(",");
        b.append(getMemberID());
        b.append(")|");
        b.append(getStartMsec());
        b.append("|");
        b.append(getLastMsec());
        b.append("|");
        b.append(getSeqNum());
        b.append("|");
        b.append(authCodeMD5);
        b.append("|");
        b.append(getPseudonym());
        b.append("]");
        return b.toString();
    }
} // class OscCookie

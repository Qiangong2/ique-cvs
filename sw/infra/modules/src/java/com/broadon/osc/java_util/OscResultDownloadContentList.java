package com.broadon.osc.java_util;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.broadon.exception.InvalidRequestException;

/** The class used to create the results associated with a purchaseable
 *  titles list request for a given player id, such that the appropriate 
 *  document and header can be created in the response back to the client
 */
public class OscResultDownloadContentList extends OscResultBean
{
    /** Rules for creating a DownloadContentList result object
     */
    public static class Rule extends OscResultRule {

        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
        }
        public boolean supports(ServerType t)
        {
            return t == ServerType.HTM;
        }
        public OscResultBean create(HttpServletRequest req,  
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
            return new OscResultDownloadContentList(req, res, this);
        }
    } // class Rule


    // --------------------------------------------
    // Invariant values
    // --------------------------------------------

    private static final String DOWNLOAD_CONTENT_LIST_JSP = "DownloadContentList.jsp";
    private static final String SELECT_DOWNLOAD_CONTENT_LIST = 
                "SELECT ctr.title_id, ct.title, " +
                "ctops.getLatestCTOID(ct.title_id,'N',1,'|') content_info " +
                "FROM CONTENT_TITLES ct, " +
                "(SELECT distinct title_id FROM CONTENT_TITLE_REGIONS " +
                "WHERE region_id = ? AND purchase_start_date <= SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)) ctr " +
                "WHERE ct.title_id = ctr.title_id " +
                "AND ct.title_type in ('visible','manual') ORDER BY ctr.title_id";

    // --------------------------------------------
    // Member variables
    // --------------------------------------------

    private final OscResultDownloadContentList.Rule  rule;
    private final OscContext          			     context;
    private final Integer                            storeID;
    private String                                   titleXML;
    private int                                      recordCount; 

    // --------------------------------------------
    // Hidden constructors (all constructors should
    // be hidden for result objects, since they are
    // constructed by the Rule class)
    // --------------------------------------------

    private OscResultDownloadContentList(HttpServletRequest  req,  
                                         HttpServletResponse res,
                                         OscResultDownloadContentList.Rule rule)
        throws InvalidRequestException
    {
        this.rule = rule;
        context = rule.getContext();
        storeID = context.getStoreId(req, context.getRequestVersion(req));
        titleXML = "";
        recordCount = -1;
    }

    // --------------------------------------------------------------------
    //            Bean Properties
    // --------------------------------------------------------------------

    /**
     * Bean Property: recordCount
     */
    public int getRecordCount() { return recordCount; }

    // --------------------------------------------
    // Specialization of abstract methods
    // --------------------------------------------

    public boolean requiresLogin() {return false;}

    public String jspFwdUrl() 
    {
        return DOWNLOAD_CONTENT_LIST_JSP;
    }

    public void encodeXml(PrintWriter out) {};

    public void serveRequest(OscLoginSession loginSession)
        throws InvalidRequestException
    {
        OscDbConnection conn = null;
        try {
            conn = context.getDB(false);
            String sql = null;

            sql = SELECT_DOWNLOAD_CONTENT_LIST;
            sql = (sql.substring(0,sql.indexOf("?")) + 
                   context.getStoreRegionId(storeID) + 
                   sql.substring(sql.indexOf("?") + 1));

            recordCount = conn.GetRecordCount(sql);
            titleXML = conn.queryXML(sql);
        }
        catch (Exception e) {
            final String msg = "Failed to obtain contents download list";
            context.getLogger().error(msg + " Error=" + e.getMessage());
            throw new InvalidRequestException(msg);
        }
        finally {
            if (conn != null)
                conn.close();
        }
    }

    public String toString() {return null;}

    // --------------------------------------------
    // Definition of result specific bean values used
    // for formatting a result or access through
    // JSP pages.
    // --------------------------------------------

    public String transformXML(String xmlStr, String xslFile, String locale)
    { return context.transformXML(xmlStr,xslFile,locale); }

    public String getXML() {return titleXML;}

} // class OscResultDownloadContentList

package com.broadon.osc.java_util;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.broadon.exception.InvalidRequestException;


/** The class used to create the results associated with a community
 *  request, such that the appropriate document and header can be
 *  created in the response back to the client
 */
public class OscResultCommunity extends OscResultBean
{
    /** Rules for creating a community result object
     */
    public static class Rule extends OscResultRule {

        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
        }
        public boolean supports(ServerType t)
        {
            return t == ServerType.HTM;
        }
        public OscResultBean create(HttpServletRequest req,  
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
            return new OscResultCommunity(req, res, this);
        }
    } // class Rule


    // --------------------------------------------
    // Invariant values
    // --------------------------------------------

    private static final String COMMUNITY_JSP = "UnderConstruction.jsp";

    // --------------------------------------------
    // Member variables
    // --------------------------------------------

    private final OscResultCommunity.Rule rule;
    private final OscContext           context;

    // --------------------------------------------
    // Hidden constructors (all constructors should
    // be hidden)
    // --------------------------------------------

    private OscResultCommunity(HttpServletRequest  req,  
                            HttpServletResponse res,
                            OscResultCommunity.Rule rule)
        throws InvalidRequestException
    {
        this.rule = rule;
        context = rule.getContext();
    }


    // --------------------------------------------
    // Specialization of abstract methods
    // --------------------------------------------

    public boolean requiresLogin() {return false;}

    public String  jspFwdUrl() {return COMMUNITY_JSP;}

    public void encodeXml(PrintWriter out) {};

    public void serveRequest(OscLoginSession loginSession)
        throws InvalidRequestException
    {
	// Nothing to do
    }

    public String toString() {return null;}

} // class OscResultCommunity


package com.broadon.osc.java_util;

import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.broadon.exception.InvalidRequestException;
import com.broadon.status.StatusCode;


/** The class used to create the metadata associated with all content,
 *  such that iqahc can make appropriate decisions on upgrades and
 *  automatic content downloads through the OSC purchase interface and
 *  the CDS server.
 */
public class OscResultContentMeta extends OscResultBean
{
    /** Rules for creating a Purchase result object
     */
    public static class Rule extends OscResultRule {

        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
        }
        public boolean supports(ServerType t)
        {
            return t == ServerType.RPC;
        }
        public OscResultBean create(HttpServletRequest req,  
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
            return new OscResultContentMeta(req, res, this);
        }
    } // class Rule


    // Class for holding information about each content object
    //
    private static class ContentInfo {
        public long    contentId;
        public long    upgradeToContentId;
        public String  objectName;
        public int     objectVersion;
        public int     minUpgradeVersion;
        public boolean upgradeConsent;
    };
    private static class PushContentInfo {
        public long   titleId;
        public long   contentId;
        public int    contentVersion;
        public String objName;
    };


    // --------------------------------------------
    // Invariant values
    // --------------------------------------------

    // Invalid or missing upgrade version
    //
    private static final int INVALID_UPGRADE_VERSION = -1;

    // Invalid or missing content identity
    //
    private static final long INVALID_CONTENT_ID = 0L;

    // For testing purposes
    //
    private static final String CACHE_TIMEOUT_MSECS_TAG = "cache_timeout_msecs";
    // XML encoding tag-names and wrappers
    //
    private static final String XML_SECURE_CONTENT_BEGIN = "\t<secure_content>\n";
    private static final String XML_SECURE_CONTENT_END = "\t</secure_content>\n";
    private static final String XML_CONTENT_UPGRADE_BEGIN = "\t<content_upgrade>\n";
    private static final String XML_CONTENT_UPGRADE_END = "\t</content_upgrade>\n";
    private static final String XML_CDS_URL_TAG = "cds_url";
    private static final String XML_PUSH_CONTENT_TAG = "push_content";
    private static final String XML_OLD_CONTENT_ID_TAG = "old_content_id";
    private static final String XML_NEW_CONTENT_ID_TAG = "new_content_id";
    private static final String XML_REQUIRE_CONSENT_TAG = "consent_required";
    private static final String XML_BB_MODEL_TAG = "bb_model";


    // Content update table.
    //
    private static final String SQL_CONTENT_UPDATE_TABLE =
    "SELECT " +
    "  content_id, content_object_name, content_object_version, " +
	"  min_upgrade_version, upgrade_consent, " +
    "  BBXML.ConvertTimeStamp(revoke_date) " +
    "FROM content_objects " +
	"ORDER BY content_object_name, content_object_version";

    // To be filtered by content update table, such that anything that
    // needs an update is filtered out.
    //
    private static final String SQL_SECURE_CONTENT = 
    "SELECT bb_model, secure_content_id FROM bb_hw_releases";

    // To be filtered by content update table, such that anything that
    // needs an update is filtered out.
    //
    private static final String SQL_PUSH_CONTENT = 
    "SELECT " +
    "   co.content_object_name, cto.content_id, cto.title_id, " +
    "   co.content_object_version " +
    "FROM content_titles ct, content_title_objects cto, content_objects co " +
    "WHERE ct.title_type='push' AND " +
    "      cto.title_id = ct.title_id AND " +
    "      co.content_id = cto.content_id " +
    "ORDER BY co.content_object_name, co.content_object_version";


    // --------------------------------------------
    // Member variables
    // --------------------------------------------

    private final OscResultContentMeta.Rule rule;
    private final HttpServletResponse    response;
    private final OscContext             context;
    private final OscLogger              log;
    private       String                 status;
    private       String                 statusMsg;
    private       String                 loginName;
    private       String                 result;
    private       long                   timeoutMsecs;


    // Thread shared cached value
    //
    private static final OscCachedObject cachedMeta = new OscCachedObject();


    // --------------------------------------------
    // Hidden constructors (all constructors should
    // be hidden for result objects, sincee they are
    // constructed by the Rule class)
    // --------------------------------------------

    private OscResultContentMeta(HttpServletRequest        req,  
                                 HttpServletResponse       res,
                                 OscResultContentMeta.Rule rule)
        throws InvalidRequestException
    {
        this.rule = rule;
        response = res;
        context = rule.getContext();
        log = context.getLogger();
        status = OscStatusCode.SC_OK;
        statusMsg = StatusCode.getMessage(status);
        loginName = null;

        // Use the timeout given in the request, if any, and otherwise
        // use the generic content cache value for the server.
        //
        String timeout = req.getParameter(CACHE_TIMEOUT_MSECS_TAG);
        if (timeout == null) {
            timeoutMsecs = context.getContentCacheTimeoutMsecs();
        }
        else {
            timeoutMsecs = Long.parseLong(timeout);
        }

    }


    // --------------------------------------------
    // Member functions
    // --------------------------------------------


    private String getXmlMetaData()
    {
        // Just return the cached data if it is still valid; otherwise
        // create a new meta-data set and cache it.
        //
        if (cachedMeta.isValid(timeoutMsecs)) {
            return (String)cachedMeta.getCached();
        }
        else {
            StringBuffer buf = new StringBuffer();
            buf.append(getXmlSecureContent());
            buf.append(getXmlLatestPushContent());
            buf.append(getXmlUpgradeMap((new Date()).getTime()));

            final String xml = buf.toString();
            if (!getIsError()) {
                //
                // Only cache valid meta data
                //
                cachedMeta.setCached(xml);
            }
            return xml;
        }
    }


    private String getXmlSecureContent()
    {
        StringBuffer    xml = new StringBuffer(1024);
        OscDbConnection conn = null;
        try {
            conn = context.getDB(true/*readonly*/);
            ResultSet rs = conn.query(SQL_SECURE_CONTENT);
            while (rs.next()) {
                String bbModel = rs.getString(1);
                long   contentId = rs.getLong(2);
                if (bbModel != null && contentId != INVALID_CONTENT_ID) {
                    xml.append(XML_SECURE_CONTENT_BEGIN);
                    xml.append(
                        context.encodeXml_TTNL(ORP.CONTENT_ID_TAG,
                                               Long.toString(contentId)));
                    xml.append(
                        context.encodeXml_TTNL(XML_BB_MODEL_TAG, bbModel));
                    xml.append(XML_SECURE_CONTENT_END);
                }
            }
            rs.close();
        }
        catch (SQLException e) {
            status = OscStatusCode.OSC_DB_ERROR;
            statusMsg = (OscStatusCode.getMessage(OscStatusCode.OSC_DB_ERROR) +
                         " .. Failed to access secure content: " + 
                         e.getMessage());
            log.error(e);
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }

        return xml.toString();
    } // getXmlSecureContent


    private String getXmlLatestPushContent()
    {
        // Among all content for a title, select the latest version
        // for each unique content object name, irrespective of other
        // parameters.
        //
        ArrayList       contents = new ArrayList(2048);
        OscDbConnection conn = null;
        try {
            conn = context.getDB(true/*readonly*/);
            ResultSet rs = conn.query(SQL_PUSH_CONTENT);
            while (rs.next()) {
                String objName = rs.getString(1);
                long   contentId = rs.getLong(2);
                if (objName != null && contentId != INVALID_CONTENT_ID) {
                    PushContentInfo info = new PushContentInfo();
                    info.objName = objName;
                    info.contentId = contentId;
                    info.titleId = rs.getLong(3);
                    info.contentVersion = rs.getInt(4);
                    contents.add(info);
                }
            }
            rs.close();
        }
        catch (SQLException e) {
            status = OscStatusCode.OSC_DB_ERROR;
            statusMsg = (OscStatusCode.getMessage(OscStatusCode.OSC_DB_ERROR) +
                         " .. Failed to access push content: " + 
                         e.getMessage());
            log.error(e);
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }

        // Walk through contents in reverse order, such that we only emit
        // the largest version of each content object
        //
        String       lastName = null;
        StringBuffer xml = new StringBuffer(1024);
        for (int i = contents.size()-1; i > 0; --i) {
            PushContentInfo info = (PushContentInfo)contents.get(i);
            if (lastName != null && lastName.equals(info.objName)) {
                // Skip this one; it has already been encoded
            }
            else {
                lastName = info.objName;
                xml.append(context.encodeXml_TNL(
                               XML_PUSH_CONTENT_TAG,
                               Long.toString(info.contentId)));
            }
        }
        contents.clear();

        return xml.toString();
    } // getXmlLatestPushContent


    private ArrayList getSortedUpdateContent(long now)
    {
        // Returns the pure unfiltered set of content objects,
        // sorted lexicographically by content name, and then 
        // by increasing version for each name.
        //
        ArrayList       contents = new ArrayList(2048);
        OscDbConnection conn = null;
        try {
            conn = context.getDB(true/*readonly*/);
            ResultSet rs = conn.query(SQL_CONTENT_UPDATE_TABLE);
            while (rs.next()) {
                final long contentId = rs.getLong(1);
                if (contentId != INVALID_CONTENT_ID) {
                    ContentInfo info = new ContentInfo();
                    info.contentId = contentId;
                    info.objectName = rs.getString(2);
                    if (rs.wasNull())
                        info.objectName = new String(); // empty string
                    info.objectVersion = rs.getInt(3);
                    info.minUpgradeVersion = rs.getInt(4);
                    if (rs.wasNull()) {
                        info.minUpgradeVersion = INVALID_UPGRADE_VERSION;
                    }
                    info.upgradeConsent = (rs.getInt(5) != 0);

                    // Add it to our sorted list, skipping any items with
                    // a revoke date set sometime in the future.
                    //
                    // Note that content with a missing minimum upgrade version
                    // can itself be upgraded, but cannot be used to upgrade
                    // other content.  Hence, such content is included here.
                    //
                    final long revokeDate = rs.getLong(6); // zero if none
                    if (revokeDate <= now) {
                        info.upgradeToContentId = INVALID_CONTENT_ID;
                        contents.add(info);
                    }
                }
            }
            rs.close();
        }
        catch (SQLException e) {
            status = OscStatusCode.OSC_DB_ERROR;
            statusMsg = (OscStatusCode.getMessage(OscStatusCode.OSC_DB_ERROR) +
                         " .. Failed to access update content: " + 
                         e.getMessage());
            log.error(e);
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return contents;
    } // getSortedUpdateContent


    private String getXmlUpgradeMap(long now)
    {
        ArrayList    contents = getSortedUpdateContent(now);
        StringBuffer xml = new StringBuffer(1024);

        for (int i = contents.size()-1; i > 1; --i) {
            ContentInfo info = (ContentInfo)contents.get(i);

            if (info.minUpgradeVersion != INVALID_UPGRADE_VERSION) {

                // Make use of the sorted order (name, version) to determine
                // content that may be upgraded by this info.
                //
                int             j = i - 1;
                ContentInfo updInfo = (ContentInfo)contents.get(j);
                while (j >= 0 &&
                       info.objectName.equals(updInfo.objectName) &&
                       info.minUpgradeVersion <= updInfo.objectVersion) {

                    if (updInfo.upgradeToContentId == INVALID_CONTENT_ID) {
                        updInfo.upgradeToContentId = info.contentId;
                        xml.append(XML_CONTENT_UPGRADE_BEGIN);
                        xml.append(context.encodeXml_TTNL(
                                       XML_OLD_CONTENT_ID_TAG,
                                       Long.toString(updInfo.contentId)));
                        xml.append(context.encodeXml_TTNL(
                                       XML_NEW_CONTENT_ID_TAG,
                                       Long.toString(info.contentId)));
                        xml.append(context.encodeXml_TTNL(
                                       XML_REQUIRE_CONSENT_TAG,
                                       (info.upgradeConsent? "1" : "0")));
                        xml.append(XML_CONTENT_UPGRADE_END);
                    }

                    // Consider the next content info for an upgrade
                    //
                    if (--j >= 0) {
                        updInfo = (ContentInfo)contents.get(j);
                    }
                }
            }
        }
        contents.clear();

        return xml.toString();
    } // getXmlUpgradeMap


    // --------------------------------------------
    // Specialization of abstract methods
    // --------------------------------------------

    public boolean requiresLogin() {return false;}

    public String jspFwdUrl()  {return null;}


    public void encodeXml(PrintWriter out) 
    {
        // Encodes the final results, where we here can be sure
        // the status and status message is correctly set.
        //
        out.println("<" + ORP.RPC_RESULT_TAG + '>');

        // First encode the action and the final error status
        //
        context.encodeXml_TNL(out, ORP.REQ_ACTION, rule.getActionNm());
        context.encodeXml_TNL(out, STATUS_TAG, status);
        context.encodeXml_TNL(out, STATUS_MSG_TAG, statusMsg);
        context.encodeXml_TNL(out, XML_CDS_URL_TAG, context.getCdsUrlString());

        // Then encode the remainder of the response as long as
        // there is no error status.
        //
        if (!getIsError()) {
            out.print(result);
        }

        // Close the root tag
        //
        out.println("</" + ORP.RPC_RESULT_TAG + '>');
    } // encodeXml


    public void encodeErrorXml(PrintWriter out, String oscStatusCode) 
    {
        out.println("<" + ORP.RPC_RESULT_TAG + '>');
        context.encodeXml_TNL(out, ORP.REQ_ACTION, rule.getActionNm());
        context.encodeXml_TNL(out, STATUS_TAG, oscStatusCode);
        context.encodeXml_TNL(out, STATUS_MSG_TAG, 
                              OscStatusCode.getMessage(oscStatusCode));
        out.println("</" + ORP.RPC_RESULT_TAG + '>');
    } // encodeErrorXml


    public void serveRequest(OscLoginSession loginSession)
        throws InvalidRequestException
    {
        if (loginSession.isLoggedIn()) {
            loginName = loginSession.getPseudonym();
        }

        // Perform database requests as appropriate
        //
        result = getXmlMetaData();

    } // serveRequest


    public String toString() {return null;}

    public boolean getIsError() 
    {
        return !status.equals(OscStatusCode.SC_OK);
    }
    public String getErrorMsg()
    {
        return statusMsg;
    }
    public String getErrorCode()
    {
        return status;
    }

    public boolean getIsLoggedIn() { return (loginName != null); }
    public String getPseudonym() { return loginName; }

    // --------------------------------------------
    // Definition of result specific bean values used
    // for formatting a result or access through
    // JSP pages.
    // --------------------------------------------

} // class OscResultContentMeta

package com.broadon.osc.java_util;

import java.io.PrintWriter;
import java.security.NoSuchAlgorithmException;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.broadon.exception.InvalidRequestException;


public class OscResultProductAuth extends OscResultBean {
    
    /** Rules for creating a Login result object
     */
    public static class Rule extends OscResultRule {

        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
        }
        public boolean supports(ServerType t)
        {
            return t == ServerType.HTM;
        }
        public OscResultBean create(HttpServletRequest req,
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
            return new OscResultProductAuth(req, res, this);
        }
    } // class Rule

    private static final String REDIRECT_HOME_ACTION   		= "login_form";
    private static final String PRO_AUTH_CODE_FORM	   		= "ProAuthCodeForm.jsp";
    private static final String PRO_AUTH_CODE_ERROR    		= "ProAuthCodeError.jsp";
    private static final String PRO_AUTH_SUCC		   		= "ProAuthSucc.jsp";
    private static final String PRO_AUTH_PURCHASE_INFO_FORM	= "ProAuthPurchInfoForm.jsp";
    private static final String PRO_AUTH_CODE_REGISTERED  	= "ProAuthCodeRegistered.jsp";
    private static final String PRO_AUTH_SN_REGISTERED 		= "ProAuthSnRegistered.jsp";
    private static final String PRO_AUTH_REDIRECTOR 		= "ProAuthRedirector.jsp";
    private static final String PRO_AUTH_LOGIN_FORM 		= "ProAuthLoginForm.jsp";
    private static final String PRO_AUTH_ACCT_REG_FORM 		= "ProAuthAcctRegForm.jsp";
    
    private static final String COOKIE_LOGIN_SESSION = "IqahLoginSession";
    
    private static final String SQL_QUERY_PRODUCT_AUTH_CODE =
        "SELECT mc.card_id,mc.assign_date,mpr.rule_id FROM ique_membership_cards mc," +
        "ique_member_point_rules mpr WHERE mc.card_type=mpr.rule_name AND mc.activate_" +
        "date is not null AND SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)>=mc.activate_date AND " +
        "mc.revoke_date is null AND mc.pin=?";
    
    private static final String SQL_QUERY_CARDID_SN =
        "SELECT card_id FROM ique_device_authentications WHERE sn=? AND membership_id=? AND member_id=?";
    
    private static final String SQL_QUERY_PRODUCT_AUTH_INFO =
        "SELECT mb.name,da.sn,mpr.rule_desc,to_char(mc.assign_date+8/24,'yyyy/mm/dd') " +
        "assign_date FROM ique_membership_cards mc,ique_device_authentications da,ique_" +
        "members mb,ique_member_point_rules mpr WHERE mc.card_id = da.card_id AND " +
        "da.membership_id = mb.membership_id AND da.member_id = mb.member_id AND " +
        "mpr.rule_name = mc.card_type AND mc.card_id = ?";
    
    private static final String SQL_QUERY_MEMBER_ACCOUNT =
        "SELECT membership_id,member_id FROM ique_members WHERE pseudonym=? AND pin=?";
    
    private static final String SQL_UPDATE_MEMBERSHIP_CARDS =
        "UPDATE ique_membership_cards SET membership_id=?,assign_date=" +
        "SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) WHERE card_id=?";
    
    private static final String SQL_INSERT_DEVICE_AUTHENTICATIONS =
        "INSERT INTO ique_device_authentications(card_id,sn,membership_id," +
        "member_id,ip_address,purchase_date,purchase_place,purchase_channe" +
        "l,known_from,device_user) VALUES(?,?,?,?,?,to_date(?,'yyyy-mm-dd')" +
        ",?,?,?,?)";
    
    private static final String SQL_UPDATE_DEVICE_AUTHENTICATIONS =
        "UPDATE ique_device_authentications SET card_id=?,ip_address=? WHE" +
        "RE sn=? AND membership_id=? AND member_id=?";
    
    private static final String SQL_QUERY_MEMBER_EMAIL_IS_EXISTING =
        "SELECT membership_id,member_id,status FROM ique_members WHERE member_email=?";
    
    private static final String SQL_INSERT_MEMBERSHIPS =
        "INSERT INTO ique_memberships(membership_id,primary_member_id,status) VALUES(?,?,?)";
    
    private static final String SQL_INSERT_MEMBERS =
        "INSERT INTO ique_members(membership_id,member_id,member_email,name,status) VALUES(?,?,?,?,?)";
    
    
    private static final String SQL_CALL_ADD_SCORE =
        "{call IQUE_MEMBER_POINT_PKG.EARN(?,?,?,?,?)}";
    
    private final OscResultProductAuth.Rule    	rule;
    private final OscContext                	context;
    private final OscLogger                 	log;
    private final HttpServletRequest 			request;
     
     
    private String  failureMsg;
    private String  error_code;
    private String  redirect_jsp;
    private String  pro_auth_code;
    private String  pro_sn;
    private String  pro_type;
    private String  pseudonym;
    private String  name;
    private String  member_email;
    private String  pin;
    private String  purchase_channel;
    private String  purchase_date;
    private String  purchase_place;    
    private String  device_user;
    private String  assign_date;
    private String  reason;    
    private String  known_from;
    
    private int     pro_auth_phase;
    private long	pro_auth_id;
    private long	rule_id;
    private long	membership_id;
    private long	member_id;
    private boolean isMember;
    
    private OscResultProductAuth(HttpServletRequest  req,
            					 HttpServletResponse res,
            					 OscResultProductAuth.Rule rule)
    	throws InvalidRequestException
    {
        this.rule = rule;
        this.context = rule.getContext();
        this.log = this.context.getLogger();
        this.request = req;
        
        this.pro_auth_phase = 0;
        
        if(this.request.getParameter("pro_auth_phase")!=null)
        {
           if(this.request.getParameter("pro_auth_phase").equals("1"))
           {
               this.pro_auth_phase = 1;
           }else if(this.request.getParameter("pro_auth_phase").equals("2")){
               this.pro_auth_phase = 2;
           }else if(this.request.getParameter("pro_auth_phase").equals("3")){
               this.pro_auth_phase = 3;
           }else if(this.request.getParameter("pro_auth_phase").equals("4")){
               this.pro_auth_phase = 4;
           }else if(this.request.getParameter("pro_auth_phase").equals("5")){
               this.pro_auth_phase = 5;
           }
        }
         
        switch(this.pro_auth_phase)
        {
            case 1:
                try{
                    this.pro_auth_code = ( this.request.getParameter("pro_auth_code")!=null &&                
   					 				  	  !this.request.getParameter("pro_auth_code").trim().equals(""))
   					 				  	  ?this.request.getParameter("pro_auth_code").trim():null;
                    if (this.pro_auth_code==null) {
                        this.failureMsg = "The product authorization code is null";
                        this.error_code = "52";
                        throw new Exception("");
                    }
                    
                    this.pro_sn = ( this.request.getParameter("pro_sn")!=null &&                
			 				  	   !this.request.getParameter("pro_sn").trim().equals(""))
				 				   ?this.request.getParameter("pro_sn").trim():null;
				 	if (this.pro_sn != null 
				 			&& this.context.utf8EncodingLength(this.pro_sn) > 32) {
				 	    this.failureMsg = "The length of product serial number exceeds the restriction.";
				        this.error_code = "53";
				        throw new Exception("");
				    }
				 	
                    String cert_code = ( this.request.getParameter("cert_code")!=null &&                
			 				  	  		!this.request.getParameter("cert_code").trim().equals(""))
			 				  	  		?this.request.getParameter("cert_code").trim():null;
			 		if (cert_code==null) {
			 		    this.failureMsg = "The auth code is null";
			 		    this.error_code = "2";
			 		    throw new Exception("");
			 		}else if(!isAuthCode(cert_code)){
			 		    this.failureMsg = "The auth code you have typed is " +
			 		    		"not matching with the code displayed on page.";
			 		    this.error_code = "49";
			 		    throw new Exception("");
			 		}
                    this.reason = null;
                }catch(OscCookie.InvalidCookieException e){
                    throw new InvalidRequestException(e.getMessage());
                }catch(Exception e){}
                
                break;
                
            case 2:
                this.isMember = ( this.request.getParameter("isMember")!=null &&
			 				  	  this.request.getParameter("isMember").trim().equals("true"))
			 				     ?true:false;
                this.pro_auth_id = Long.parseLong(this.request.getParameter("pro_auth_id"));
                this.rule_id = Long.parseLong(this.request.getParameter("rule_id"));
                this.pro_sn = this.request.getParameter("pro_sn");
                
                break;
                
            case 3:
                try{
                    this.pseudonym = ( this.request.getParameter("pseudonym")!=null &&
			 				  	  	  !this.request.getParameter("pseudonym").trim().equals(""))
			 				  	  	  ?this.request.getParameter("pseudonym").trim():null;
			 	    if (this.pseudonym==null) {
			 	        this.failureMsg = "The pseudonym is null";
			 	        this.error_code = "1";
			 	        throw new Exception("");
			 	    }else if (!OscContext.isValidPseudonym(this.pseudonym)) {
	   		            this.failureMsg = "pseudonym is not valid.";
	   		            this.error_code = "21";
	   		            throw new Exception("");
	   		        }
			 	    this.pin = ( this.request.getParameter("pin")!=null &&
			 	            	!this.request.getParameter("pin").equals(""))
			 	            	?this.request.getParameter("pin"):null;
                    if (this.pin==null) {
                        this.failureMsg = "The password is null";
                        this.error_code = "17";
                        throw new Exception("");
                    }                    
                    this.reason = null;
                }catch(Exception e){} 
                
                this.pro_auth_id = Long.parseLong(this.request.getParameter("pro_auth_id"));
                this.rule_id = Long.parseLong(this.request.getParameter("rule_id"));
                this.pro_sn = this.request.getParameter("pro_sn");
                
                this.purchase_channel = 
                	( this.request.getParameter("purchase_channel")!=null &&
		 			 !this.request.getParameter("purchase_channel").trim().equals(""))
		 			 ?this.request.getParameter("purchase_channel").trim():null;
		 		
		 	    this.purchase_place = 
	               	( this.request.getParameter("purchase_place")!=null &&
			 		 !this.request.getParameter("purchase_place").trim().equals(""))
			 		 ?this.request.getParameter("purchase_place").trim():null;
			 			 
			 	this.purchase_date = 
		            ( this.request.getParameter("purchase_date")!=null &&
				 	 !this.request.getParameter("purchase_date").trim().equals(""))
				 	 ?this.request.getParameter("purchase_date").trim():null;
				 	 
				
				this.known_from = "";
				if(this.request.getParameterValues("known_from")!=null){
					String[] values = this.request.getParameterValues("known_from");
					for(int p=0;p<values.length;p++){
						if(values[p]!=null && !values[p].equals("")){
							this.known_from += values[p];
							if(p<values.length-1)
								this.known_from += "&";
						}
					}
				}
				 		 
				this.device_user = 
			       	( this.request.getParameter("device_user")!=null &&
					 !this.request.getParameter("device_user").trim().equals(""))
					 ?this.request.getParameter("device_user").trim():null;
		 			 
                break;
                
            case 4:
                try{
                    this.member_email = ( this.request.getParameter("member_email")!=null &&
   					 				 	 !this.request.getParameter("member_email").trim().equals(""))
   					 				 	 ?this.request.getParameter("member_email").trim().toLowerCase():null;
   		            if (this.member_email==null) {
   		                this.failureMsg = "member_email is null";
   		                this.error_code = "11";
   		                throw new Exception("");
   		            } else if (!OscContext.isValidEmailAddress(this.member_email)) {
   		                this.failureMsg = "member_email is not valid.";
   		                this.error_code = "31";
   		                throw new Exception("");
   		            }
                    this.name = ( this.request.getParameter("name")!=null &&
			 				 	 !this.request.getParameter("name").trim().equals(""))
			 				 	 ?this.request.getParameter("name").trim():null;
                    if (this.name==null) {
                        this.failureMsg = "The name is null";
                        this.error_code = "3";
                        throw new Exception("");
                    }                    
                    this.reason = null;
                }catch(Exception e){}
                
                this.pro_auth_id = Long.parseLong(this.request.getParameter("pro_auth_id"));
                this.rule_id = Long.parseLong(this.request.getParameter("rule_id"));
                this.pro_sn = this.request.getParameter("pro_sn");
                
                this.purchase_channel = 
                	( this.request.getParameter("purchase_channel")!=null &&
		 			 !this.request.getParameter("purchase_channel").trim().equals(""))
		 			 ?this.request.getParameter("purchase_channel").trim():null;
		 		
		 	    this.purchase_place = 
	               	( this.request.getParameter("purchase_place")!=null &&
			 		 !this.request.getParameter("purchase_place").trim().equals(""))
			 		 ?this.request.getParameter("purchase_place").trim():null;
			 			 
			 	this.purchase_date = 
		            ( this.request.getParameter("purchase_date")!=null &&
				 	 !this.request.getParameter("purchase_date").trim().equals(""))
				 	 ?this.request.getParameter("purchase_date").trim():null;
				 	 
				this.known_from = "";
				if(this.request.getParameterValues("known_from")!=null){
					String[] values = this.request.getParameterValues("known_from");
					for(int p=0;p<values.length;p++){
						if(values[p]!=null && !values[p].equals("")){
							this.known_from += values[p];
							if(p<values.length-1)
								this.known_from += "&";
						}
					}
				}
				 		 
				this.device_user = 
			       	( this.request.getParameter("device_user")!=null &&
					 !this.request.getParameter("device_user").trim().equals(""))
					 ?this.request.getParameter("device_user").trim():null;
                
                break;
                
            case 5:
            	
            	this.pro_auth_id = Long.parseLong(this.request.getParameter("pro_auth_id"));
                
                this.purchase_channel = 
                	( this.request.getParameter("purchase_channel")!=null &&
		 			 !this.request.getParameter("purchase_channel").trim().equals(""))
		 			 ?this.request.getParameter("purchase_channel").trim():null;
		 		
		 	    this.purchase_place = 
	               	( this.request.getParameter("purchase_place")!=null &&
			 		 !this.request.getParameter("purchase_place").trim().equals(""))
			 		 ?this.request.getParameter("purchase_place").trim():null;
			 			 
			 	this.purchase_date = 
		            ( this.request.getParameter("purchase_date")!=null &&
				 	 !this.request.getParameter("purchase_date").trim().equals(""))
				 	 ?this.request.getParameter("purchase_date").trim():null;
				 	 
				this.known_from = "";
				if(this.request.getParameterValues("known_from")!=null){
					String[] values = this.request.getParameterValues("known_from");
					for(int p=0;p<values.length;p++){
						if(values[p]!=null && !values[p].equals("")){
							this.known_from += values[p];
							if(p<values.length-1)
								this.known_from += "&";
						}
					}
				}
				 		 
				this.device_user = 
			       	( this.request.getParameter("device_user")!=null &&
					 !this.request.getParameter("device_user").trim().equals(""))
					 ?this.request.getParameter("device_user").trim():null;
                
                break;
                
            default:            	
                this.redirect_jsp = PRO_AUTH_CODE_FORM;
            	this.request.setAttribute("pro_auth_phase","1");
                break;
                
        }
    }

// --------------------------------------------
// Specialization of abstract methods
// --------------------------------------------

    public boolean requiresSecureRequest() {return true;}
    public boolean requiresLogin() {return false;}

    public String jspFwdUrl()
    {
        return this.redirect_jsp;
    }

    public String redirectAction()
    {
        return REDIRECT_HOME_ACTION;
    }

    public void encodeXml(PrintWriter out) {}

    public void serveRequest(OscLoginSession loginSession)
    	throws InvalidRequestException
    {
        if(this.pro_auth_phase == 1){
            
            if(this.failureMsg==null){
                OscDbConnection   conn = null;
                PreparedStatement stmt = null;
                ResultSet           rs = null;
               
                boolean        isFound = false;
                
                try {
                    conn = this.context.getDB(true);
                    stmt = conn.prepareStatement(SQL_QUERY_PRODUCT_AUTH_CODE);
                    stmt.setString(1,OscContext.getEncryptedPasswd(this.pro_auth_code));
                    rs = stmt.executeQuery();
                    if (rs.next()) {
                        isFound = true;
                        this.pro_auth_id = rs.getLong("card_id");
                        this.assign_date = rs.getString("assign_date"); 
                        this.rule_id = rs.getLong("rule_id");                        
                    }
                    rs.close();
                    conn.closeStatement(stmt);
                    stmt=null;
                    
                }catch(NoSuchAlgorithmException e){
                    this.log.error("Failed to encrypt the product authentication " +
                    		"code: " + e.getMessage());
                    throw new InvalidRequestException("Failed to encrypt the product " +
                    		"authentication code: " + e.getMessage());
                }catch (SQLException e) {
                    this.log.error("Failed to query the product authentication " +
                    		"code in IQUE DB: " + e.getMessage());
                    throw new InvalidRequestException("Failed to query the product " +
                    		"authentication code in IQUE DB: " + e.getMessage());
                }finally{
                    if (conn != null) {
                        conn.closeStatement(stmt);
                        conn.close();
                    }
                } 
                if(isFound){
                    if(this.assign_date==null){
                        if(loginSession.isLoggedIn()){
                            this.membership_id	=	loginSession.getMembershipID();
                            this.member_id		=	loginSession.getMemberID();
                            
                            boolean snIsRegByMyself = false;
                            boolean isExisting		= false;
                            boolean isRequestInsert = false;
                            
                            String  card_id			= null;
                            try{
                                conn = this.context.getDB(true);
                                stmt = conn.prepareStatement(SQL_QUERY_CARDID_SN);
                                stmt.setString(1,this.pro_sn);
                                stmt.setLong(2,this.membership_id);
                                stmt.setLong(3,this.member_id);
                                rs = stmt.executeQuery();
                                
                                if (rs.next()) {
                                    snIsRegByMyself = true;
                                    card_id=rs.getString("card_id");
                                }
                                
                                rs.close();
                                conn.closeStatement(stmt);
                                stmt=null;
                                
                            }catch(SQLException e){
                                this.log.error("Failed to query card_id and sn in iQue DB:" + e.getMessage());
                                throw new InvalidRequestException("Failed to query card_id and sn in iQue DB:" + e.getMessage());
                            }finally{
                                if (conn != null) {
                                    conn.closeStatement(stmt);
                                    conn.close();
                                }
                            }
                            
                            if(snIsRegByMyself && card_id!=null){
                                isExisting = true;
                            }else if(!snIsRegByMyself){
                                isRequestInsert = true;
                            }
                            
                            if(!isExisting){
                                try {
                                    CallableStatement cstmt = null;
                                    conn = this.context.getDB(false);
                                
                                    stmt = conn.prepareStatement(SQL_UPDATE_MEMBERSHIP_CARDS);
                                    stmt.setLong(1,this.membership_id);
                                    stmt.setLong(2,this.pro_auth_id);
                                
                                    stmt.executeUpdate();
                                    conn.closeStatement(stmt);
                                    stmt = null;
                                
                                    if(isRequestInsert){
                                    	stmt = conn.prepareStatement(SQL_INSERT_DEVICE_AUTHENTICATIONS);
                                        stmt.setLong(1,this.pro_auth_id);
                                        stmt.setString(2,this.pro_sn);
                                        stmt.setLong(3,this.membership_id);
                                        stmt.setLong(4,this.member_id);
                                        stmt.setString(5,this.request.getRemoteAddr());
                                        stmt.setString(6,this.purchase_date);
                                        stmt.setString(7,this.purchase_place);
                                        stmt.setString(8,this.purchase_channel);
                                        stmt.setString(9,this.known_from);
                                        stmt.setString(10,this.device_user); 
                                    }else{
                                    	
                                    	StringBuffer sql = new StringBuffer("UPDATE ique_device_authentications SET ");
                                    	sql.append("card_id=?, ip_address=? ");
                                    	
                                    	if(this.purchase_channel!=null)
                                    		sql.append(", purchase_channel='" + this.purchase_channel + "' ");
                                    	if(this.purchase_date!=null)
                                    		sql.append(", purchase_date=to_date('" + this.purchase_date + "','yyyy-mm-dd') ");
                                    	if(this.purchase_place!=null)
                                    		sql.append(", purchase_place='" + this.purchase_place + "' ");
                                    	if(this.known_from!=null)
                                    		sql.append(", known_from='" + this.known_from + "' ");
                                    	if(this.device_user!=null)
                                    		sql.append(", device_user='" + this.device_user + "' ");
                                    	
                                    	sql.append(" WHERE sn=? AND membership_id=? AND member_id=?");
                                    	
                                        stmt = conn.prepareStatement(sql.toString());
                                        stmt.setLong(1,this.pro_auth_id);
              	                        stmt.setString(2,this.request.getRemoteAddr());
              	                        stmt.setString(3,this.pro_sn);
              	                        stmt.setLong(4,this.membership_id);
              	                        stmt.setLong(5,this.member_id);                                    
                                    }                                
                                
                                    stmt.executeUpdate();
                                    conn.closeStatement(stmt);
                                	stmt = null;
                                
                                	cstmt = conn.prepareCall(SQL_CALL_ADD_SCORE);
                                	cstmt.setLong(1,this.membership_id);
                                	cstmt.setLong(2,this.member_id);
                                	cstmt.setLong(3,this.rule_id);
                                	cstmt.setString(4,this.reason);
                                	cstmt.setString(5,this.request.getRemoteAddr());
                                
                                	cstmt.execute ();
                                	conn.closeStatement(cstmt);
                                	stmt = null;
                                
                                	conn.commit();
                                }
                                catch (SQLException e) {
                                    try {
                                        conn.rollback();
                                    } catch (SQLException ex) {}
                                    this.log.error("Failed to update product authentication info and add " +
                                            "score in IQUE DB: " + e.getMessage());
                                    throw new InvalidRequestException("Failed to update product authentication " +
                                			"info and add score in IQUE DB: " + e.getMessage());
                                }                	
                            	finally{
                                	if (conn != null) {
                                	    conn.closeStatement(stmt);
                                    	conn.close();
                                	}
                            	}
                            	
                            	this.redirect_jsp = PRO_AUTH_PURCHASE_INFO_FORM;
                            	this.request.setAttribute("pro_auth_phase","5");
                            	this.request.setAttribute("pro_auth_id",Long.toString(this.pro_auth_id));
                            	
                            }else{
                                queryProAuthcodeInfo(Long.parseLong(card_id));                                
                                this.request.setAttribute("name",this.name);
                                this.request.setAttribute("pro_sn",this.pro_sn);
                                this.request.setAttribute("assign_date",this.assign_date);
                                this.request.setAttribute("pro_type",this.pro_type);
                                
                                this.redirect_jsp = PRO_AUTH_SN_REGISTERED;
                            }
                            
                        }else{
                            this.redirect_jsp = PRO_AUTH_REDIRECTOR;
                            this.request.setAttribute("pro_auth_phase","2");
                            this.request.setAttribute("pro_auth_id",Long.toString(this.pro_auth_id));
                            this.request.setAttribute("rule_id",Long.toString(this.rule_id));
                            this.request.setAttribute("pro_sn",this.pro_sn);
                        }
                    }else{
                        queryProAuthcodeInfo(this.pro_auth_id); 
                        this.redirect_jsp = PRO_AUTH_CODE_REGISTERED;
                        this.request.setAttribute("name",this.name);
                        this.request.setAttribute("pro_sn",this.pro_sn);
                        this.request.setAttribute("assign_date",this.assign_date);
                        this.request.setAttribute("pro_type",this.pro_type);                        
                    }
                }else{
                    this.redirect_jsp = PRO_AUTH_CODE_ERROR;
                }               
                
            }else{
                this.request.setAttribute("pro_auth_phase","1");
                this.redirect_jsp = PRO_AUTH_CODE_FORM;
            }
        }else if(this.pro_auth_phase == 2){
            if(this.failureMsg==null){
                if(this.isMember){
                    this.redirect_jsp = PRO_AUTH_LOGIN_FORM;
                    this.request.setAttribute("pro_auth_phase","3");
                }else{
                    this.redirect_jsp = PRO_AUTH_ACCT_REG_FORM;
                    this.request.setAttribute("pro_auth_phase","4");
                }
                this.request.setAttribute("pro_auth_id",Long.toString(this.pro_auth_id));
                this.request.setAttribute("rule_id",Long.toString(this.rule_id));
                this.request.setAttribute("pro_sn",this.pro_sn);
            }else{
                this.redirect_jsp = PRO_AUTH_REDIRECTOR;
                this.request.setAttribute("pro_auth_phase","2");
                this.request.setAttribute("pro_auth_id",Long.toString(this.pro_auth_id));
                this.request.setAttribute("rule_id",Long.toString(this.rule_id));
                this.request.setAttribute("pro_sn",this.pro_sn);
            }
        }else if(this.pro_auth_phase == 3){
            if(this.failureMsg==null){
                OscDbConnection   conn = null;
                PreparedStatement stmt = null;
                ResultSet           rs = null;
               
                boolean        isFound = false;                
                try {
                    conn = this.context.getDB(true);
                    stmt = conn.prepareStatement(SQL_QUERY_MEMBER_ACCOUNT);
                    stmt.setString(1,this.pseudonym);
                    stmt.setString(2,OscContext.getEncryptedPasswd(this.pin));
                    rs = stmt.executeQuery();
                    if (rs.next()) {
                        isFound = true;
                        this.membership_id=rs.getLong("membership_id");
                        this.member_id=rs.getLong("member_id");
                    }
                    rs.close();
                    conn.closeStatement(stmt);
                    stmt=null;
                    
                }catch(NoSuchAlgorithmException e){
                    this.log.error("Failed to encrypt the login passowrd " +
                    		"in IQUE DB: " + e.getMessage());
                    throw new InvalidRequestException("Failed to encrypt " +
                    		"the login passowrd in IQUE DB: " + e.getMessage());
                }catch (SQLException e) {
                    this.log.error("Failed to query the member account " +
                    		"in IQUE DB: " + e.getMessage());
                    throw new InvalidRequestException("Failed to query the " +
                    		"member account in IQUE DB: " + e.getMessage());
                }finally{
                    if (conn != null) {
                        conn.closeStatement(stmt);
                        conn.close();
                    }
                }
                if(isFound){
                    boolean snIsRegByMyself = false;
                    boolean isExisting		= false;
                    boolean isRequestInsert = false;
                    
                    String  card_id			= null;
                    try{
                        conn = this.context.getDB(true);
                        stmt = conn.prepareStatement(SQL_QUERY_CARDID_SN);
                        stmt.setString(1,this.pro_sn);
                        stmt.setLong(2,this.membership_id);
                        stmt.setLong(3,this.member_id);
                        rs = stmt.executeQuery();
                        
                        if (rs.next()) {
                            snIsRegByMyself = true;
                            card_id=rs.getString("card_id");
                        }
                        
                        rs.close();
                        conn.closeStatement(stmt);
                        stmt=null;
                        
                    }catch(SQLException e){
                        this.log.error("Failed to query card_id and sn in iQue DB:" + e.getMessage());
                        throw new InvalidRequestException("Failed to query card_id and sn in iQue DB:" + e.getMessage());
                    }finally{
                        if (conn != null) {
                            conn.closeStatement(stmt);
                            conn.close();
                        }
                    }
                    
                    if(snIsRegByMyself && card_id!=null){
                        isExisting = true;
                    }else if(!snIsRegByMyself){
                        isRequestInsert = true;
                    }
                    
                    if(!isExisting){
                        try{
                            CallableStatement cstmt = null;
                            conn = this.context.getDB(false);
                        
                            stmt = conn.prepareStatement(SQL_UPDATE_MEMBERSHIP_CARDS);
                            stmt.setLong(1,this.membership_id);
                            stmt.setLong(2,this.pro_auth_id);
                        
                            stmt.executeUpdate();
                            conn.closeStatement(stmt);
                            stmt = null;
                        
                            if(isRequestInsert){
                                stmt = conn.prepareStatement(SQL_INSERT_DEVICE_AUTHENTICATIONS);
                                stmt.setLong(1,this.pro_auth_id);
                                stmt.setString(2,this.pro_sn);
                                stmt.setLong(3,this.membership_id);
                                stmt.setLong(4,this.member_id);
                                stmt.setString(5,this.request.getRemoteAddr());
                                stmt.setString(6,this.purchase_date);
                                stmt.setString(7,this.purchase_place);
                                stmt.setString(8,this.purchase_channel);
                                stmt.setString(9,this.known_from);
                                stmt.setString(10,this.device_user);                                
                            }else{
                            	
                            	StringBuffer sql = new StringBuffer("UPDATE ique_device_authentications SET ");
                            	sql.append("card_id=?, ip_address=? ");
                            	
                            	if(this.purchase_channel!=null)
                            		sql.append(", purchase_channel='" + this.purchase_channel + "' ");
                            	if(this.purchase_date!=null)
                            		sql.append(", purchase_date=to_date('" + this.purchase_date + "','yyyy-mm-dd') ");
                            	if(this.purchase_place!=null)
                            		sql.append(", purchase_place='" + this.purchase_place + "' ");
                            	if(this.known_from!=null)
                            		sql.append(", known_from='" + this.known_from + "' ");
                            	if(this.device_user!=null)
                            		sql.append(", device_user='" + this.device_user + "' ");
                            	
                            	sql.append(" WHERE sn=? AND membership_id=? AND member_id=?");
                            	
                                stmt = conn.prepareStatement(sql.toString());
                                stmt.setLong(1,this.pro_auth_id);
      	                        stmt.setString(2,this.request.getRemoteAddr());
      	                        stmt.setString(3,this.pro_sn);
      	                        stmt.setLong(4,this.membership_id);
      	                        stmt.setLong(5,this.member_id);                                   
                            }    
                        
                            stmt.executeUpdate();
                            conn.closeStatement(stmt);
                            stmt = null;
                        
                            cstmt = conn.prepareCall(SQL_CALL_ADD_SCORE);
                            cstmt.setLong(1,this.membership_id);
                            cstmt.setLong(2,this.member_id);
                            cstmt.setLong(3,this.rule_id);
                            cstmt.setString(4,this.reason);
                        	cstmt.setString(5,this.request.getRemoteAddr());
                        
                        	cstmt.execute ();
                        	conn.closeStatement(cstmt);
                        	stmt = null;
                        
                        	conn.commit();  
                    	}
                        catch (SQLException e) {
                            try {
                                conn.rollback();
                            } catch (SQLException ex) {}
                            this.log.error("Failed to update product authentication " +
                                    "info and add score in IQUE DB: " + e.getMessage());
                            throw new InvalidRequestException("Failed to update product " +
                                    "authentication info and add score in IQUE DB: " 
                                    + e.getMessage());
                        }                	
                        finally{
                            if (conn != null) {
                                conn.closeStatement(stmt);
                                conn.close();
                            }
                        }
                 		queryProAuthcodeInfo(this.pro_auth_id);                          
                 		this.redirect_jsp = PRO_AUTH_SUCC;
                    }else{
                        queryProAuthcodeInfo(Long.parseLong(card_id));                          
                        this.redirect_jsp = PRO_AUTH_SN_REGISTERED;
                    }                    
                    this.request.setAttribute("name",this.name);
                    this.request.setAttribute("pro_sn",this.pro_sn);
                    this.request.setAttribute("assign_date",this.assign_date);
                    this.request.setAttribute("pro_type",this.pro_type);
                    
                }else{
                    this.failureMsg = "The login info is error.";
                    this.error_code = "47";
                    this.redirect_jsp = PRO_AUTH_LOGIN_FORM;
                    this.request.setAttribute("pro_auth_phase","3");
                    this.request.setAttribute("pro_auth_id",Long.toString(this.pro_auth_id));
                    this.request.setAttribute("rule_id",Long.toString(this.rule_id));
                    this.request.setAttribute("pro_sn",this.pro_sn);
                }
            }else{
                this.redirect_jsp = PRO_AUTH_LOGIN_FORM;
                this.request.setAttribute("pro_auth_phase","3");
                this.request.setAttribute("pro_auth_id",Long.toString(this.pro_auth_id));
                this.request.setAttribute("rule_id",Long.toString(this.rule_id));
                this.request.setAttribute("pro_sn",this.pro_sn);
            }
        }else if(this.pro_auth_phase == 4){
            if(this.failureMsg==null){
                OscDbConnection    conn = null;
                PreparedStatement  stmt = null;
                ResultSet           rs = null;
               
                boolean        isFound = false;
                String	        status = null;
                
                try {
                    conn = this.context.getDB(true);
                    stmt = conn.prepareStatement(SQL_QUERY_MEMBER_EMAIL_IS_EXISTING);
                    stmt.setString(1,this.member_email);
                    rs = stmt.executeQuery();
                    if (rs.next()) {
                        isFound = true;
                        this.membership_id = rs.getLong("membership_id");
                        this.member_id = rs.getLong("member_id");
                        status = rs.getString("status");
                    }
                    rs.close();
                    conn.closeStatement(stmt);
                    stmt=null;
                    
                }catch (SQLException e) {
                    this.log.error("Failed to query the existing of " +
                    		"member_email in IQUE DB: " + e.getMessage());
                    throw new InvalidRequestException("Failed to query " +
                    		"the existing of member_email in IQUE DB: " 
                            + e.getMessage());
                }finally{
                    if (conn != null) {
                        conn.closeStatement(stmt);
                        conn.close();
                    }
                }               
                
                if(!isFound){                        
                    this.membership_id = OscContext.getNextMembershipIdSeq(this.context,this.log);
                    this.member_id = 1;                   
                    
                    try{
                        CallableStatement cstmt = null;
                        conn = this.context.getDB(false);
                        stmt = conn.prepareStatement(SQL_INSERT_MEMBERSHIPS);
                        stmt.setLong(1,this.membership_id);
                        stmt.setLong(2,this.member_id);
                        stmt.setString(3,"N");
                        
                        stmt.executeUpdate();
                        conn.closeStatement(stmt);
                        stmt = null;
                        
                        stmt = conn.prepareStatement(SQL_INSERT_MEMBERS);
                        stmt.setLong(1,this.membership_id);
                        stmt.setLong(2,this.member_id);
                        stmt.setString(3,this.member_email);
                        stmt.setString(4,this.name);
                        stmt.setString(5,"N1");
                        
                        stmt.executeUpdate();
                        conn.closeStatement(stmt);
                        stmt = null;  
                        
                        cstmt = conn.prepareCall(SQL_CALL_ADD_SCORE);
                        cstmt.setLong(1,this.membership_id);
                        cstmt.setLong(2,this.member_id);
                        cstmt.setLong(3,this.rule_id);
                        cstmt.setString(4,this.reason);
                        cstmt.setString(5,this.request.getRemoteAddr());
                        
                        cstmt.execute ();
                        conn.closeStatement(cstmt);
                        stmt = null;
                        
                        stmt = conn.prepareStatement(SQL_UPDATE_MEMBERSHIP_CARDS);
                        stmt.setLong(1,this.membership_id);
                        stmt.setLong(2,this.pro_auth_id);
                        
                        stmt.executeUpdate();
                        conn.closeStatement(stmt);
                        stmt = null;
                        
                        stmt = conn.prepareStatement(SQL_INSERT_DEVICE_AUTHENTICATIONS);
                        stmt.setLong(1,this.pro_auth_id);
                        stmt.setString(2,this.pro_sn);
                        stmt.setLong(3,this.membership_id);
                        stmt.setLong(4,this.member_id);
                        stmt.setString(5,this.request.getRemoteAddr());
                        stmt.setString(6,this.purchase_date);
                        stmt.setString(7,this.purchase_place);
                        stmt.setString(8,this.purchase_channel);
                        stmt.setString(9,this.known_from);
                        stmt.setString(10,this.device_user); 
                        
                        stmt.executeUpdate();
                        conn.closeStatement(stmt);
                        stmt = null;
                        
                        conn.commit();                       
                                        
                    }
                    catch (SQLException e) {
                        try {
                            conn.rollback();
                        } catch (SQLException ex) {}
                        this.log.error("Failed to update product authentication " +
                        		"info and add score in IQUE DB: " + e.getMessage());
                        throw new InvalidRequestException("Failed to update product " +
                        		"authentication info and add score in IQUE DB: " 
                                + e.getMessage());
                    }                	
                    finally{
                        if (conn != null) {
                            conn.closeStatement(stmt);
                            conn.close();
                        }
                    }
                    queryProAuthcodeInfo(this.pro_auth_id); 
                    this.redirect_jsp = PRO_AUTH_SUCC;
                    this.request.setAttribute("name",this.name);
                    this.request.setAttribute("pro_sn",this.pro_sn);
                    this.request.setAttribute("assign_date",this.assign_date);
                    this.request.setAttribute("pro_type",this.pro_type);
                    
                }else if(status!=null&&status.equals("A1")){
                    this.failureMsg = "The email has been registered.";
                    this.error_code = "48";
                    this.redirect_jsp = PRO_AUTH_ACCT_REG_FORM;
                    this.request.setAttribute("pro_auth_phase","4");
                    this.request.setAttribute("pro_auth_id",Long.toString(this.pro_auth_id));
                    this.request.setAttribute("rule_id",Long.toString(this.rule_id));
                    this.request.setAttribute("pro_sn",this.pro_sn);
                }else{
                    boolean snIsRegByMyself = false;
                    boolean isExisting		= false;
                    boolean isRequestInsert = false;
                    
                    String  card_id			= null;
                    try{
                        conn = this.context.getDB(true);
                        stmt = conn.prepareStatement(SQL_QUERY_CARDID_SN);
                        stmt.setString(1,this.pro_sn);
                        stmt.setLong(2,this.membership_id);
                        stmt.setLong(3,this.member_id);
                        rs = stmt.executeQuery();
                        
                        if (rs.next()) {
                            snIsRegByMyself = true;
                            card_id=rs.getString("card_id");
                        }
                        
                        rs.close();
                        conn.closeStatement(stmt);
                        stmt=null;
                        
                    }catch(SQLException e){
                        this.log.error("Failed to query card_id and sn in iQue DB:" + e.getMessage());
                        throw new InvalidRequestException("Failed to query card_id and sn in iQue DB:" + e.getMessage());
                    }finally{
                        if (conn != null) {
                            conn.closeStatement(stmt);
                            conn.close();
                        }
                    }
                    
                    if(snIsRegByMyself && card_id!=null){
                        isExisting = true;
                    }else if(!snIsRegByMyself){
                        isRequestInsert = true;
                    }
                    
                    if(!isExisting){
                        try{
                            CallableStatement cstmt = null;
                            conn = this.context.getDB(false);
                            cstmt = conn.prepareCall(SQL_CALL_ADD_SCORE);
                            cstmt.setLong(1,this.membership_id);
                            cstmt.setLong(2,this.member_id);
                            cstmt.setLong(3,this.rule_id);
                            cstmt.setString(4,this.reason);
                            cstmt.setString(5,this.request.getRemoteAddr());
                    
                            cstmt.execute ();
                            conn.closeStatement(cstmt);
                            stmt = null;
                    	
                            stmt = conn.prepareStatement(SQL_UPDATE_MEMBERSHIP_CARDS);
                            stmt.setLong(1,this.membership_id);
                            stmt.setLong(2,this.pro_auth_id);
                        
                            stmt.executeUpdate();
                            conn.closeStatement(stmt);
                            stmt = null;
                        
                            if(isRequestInsert){
                            	stmt = conn.prepareStatement(SQL_INSERT_DEVICE_AUTHENTICATIONS);
                                stmt.setLong(1,this.pro_auth_id);
                                stmt.setString(2,this.pro_sn);
                                stmt.setLong(3,this.membership_id);
                                stmt.setLong(4,this.member_id);
                                stmt.setString(5,this.request.getRemoteAddr());
                                stmt.setString(6,this.purchase_date);
                                stmt.setString(7,this.purchase_place);
                                stmt.setString(8,this.purchase_channel);
                                stmt.setString(9,this.known_from);
                                stmt.setString(10,this.device_user); 
                            }else{
                            	
                            	StringBuffer sql = new StringBuffer("UPDATE ique_device_authentications SET ");
                            	sql.append("card_id=?, ip_address=? ");
                            	
                            	if(this.purchase_channel!=null)
                            		sql.append(", purchase_channel='" + this.purchase_channel + "' ");
                            	if(this.purchase_date!=null)
                            		sql.append(", purchase_date=to_date('" + this.purchase_date + "','yyyy-mm-dd') ");
                            	if(this.purchase_place!=null)
                            		sql.append(", purchase_place='" + this.purchase_place + "' ");
                            	if(this.known_from!=null)
                            		sql.append(", known_from='" + this.known_from + "' ");
                            	if(this.device_user!=null)
                            		sql.append(", device_user='" + this.device_user + "' ");
                            	
                            	sql.append(" WHERE sn=? AND membership_id=? AND member_id=?");
                            	
                                stmt = conn.prepareStatement(sql.toString());
                                stmt.setLong(1,this.pro_auth_id);
      	                        stmt.setString(2,this.request.getRemoteAddr());
      	                        stmt.setString(3,this.pro_sn);
      	                        stmt.setLong(4,this.membership_id);
      	                        stmt.setLong(5,this.member_id);                                   
                            }  
                        
                        	stmt.executeUpdate();
                        	conn.closeStatement(stmt);
                        	stmt = null;
                    
                        	conn.commit(); 
                        }catch (SQLException e) {
                            try {
                                conn.rollback();
                            } catch (SQLException ex) {}
                            this.log.error("Failed to update product authentication " +
                                    "info and add score in IQUE DB: " + e.getMessage());
                            throw new InvalidRequestException("Failed to update product " +
                                    "authentication info and add score in IQUE DB: " 
                                    + e.getMessage());
                        }                	
                        finally{
                            if (conn != null) {
                                conn.closeStatement(stmt);
                                conn.close();
                            }
                        }
                    	queryProAuthcodeInfo(this.pro_auth_id);                          
                    	this.redirect_jsp = PRO_AUTH_SUCC;
                	}else{
                	    queryProAuthcodeInfo(Long.parseLong(card_id));                          
                	    this.redirect_jsp = PRO_AUTH_SN_REGISTERED;
                	}
                    
                    this.request.setAttribute("name",this.name);
                    this.request.setAttribute("pro_sn",this.pro_sn);
                    this.request.setAttribute("assign_date",this.assign_date);
                    this.request.setAttribute("pro_type",this.pro_type);
                }
            }else{
                this.redirect_jsp = PRO_AUTH_ACCT_REG_FORM;
                this.request.setAttribute("pro_auth_phase","4");
                this.request.setAttribute("pro_auth_id",Long.toString(this.pro_auth_id));
                this.request.setAttribute("rule_id",Long.toString(this.rule_id));
                this.request.setAttribute("pro_sn",this.pro_sn);
            }
            
        }else if(this.pro_auth_phase == 5){
        	
            if(this.failureMsg==null){
            	OscDbConnection    conn = null;
                PreparedStatement  stmt = null;
                
                try {
                	conn = this.context.getDB(false);
                	StringBuffer sql = new StringBuffer("UPDATE ique_device_authentications SET ");
                	
                	ArrayList list = new ArrayList();                	
                	if(this.purchase_channel!=null)
                		list.add("purchase_channel='" + this.purchase_channel + "'");
                	if(this.purchase_date!=null)
                		list.add("purchase_date=to_date('" + this.purchase_date + "','yyyy-mm-dd')");
                	if(this.purchase_place!=null)
                		list.add("purchase_place='" + this.purchase_place + "'");
                	if(this.known_from!=null)
                		list.add("known_from='" + this.known_from + "'");
                	if(this.device_user!=null)
                		list.add("device_user='" + this.device_user + "'");
                	
                	for(Iterator iterator = list.iterator();iterator.hasNext();){
                		sql.append((String)iterator.next());
                		if(iterator.hasNext())
                			sql.append(",");
                	}
                	
                	sql.append(" WHERE card_id=? AND membership_id=? AND member_id=?");
                	
                    stmt = conn.prepareStatement(sql.toString());
                    stmt.setLong(1,this.pro_auth_id);
                    stmt.setLong(2,loginSession.getMembershipID());
                    stmt.setLong(3,loginSession.getMemberID()); 
                      
                    stmt.executeUpdate();
                  	conn.closeStatement(stmt);
                  	stmt = null;
              
                  	conn.commit();
                    
                }catch (SQLException e) {
                    try {
                        conn.rollback();
                    } catch (SQLException ex) {}
                    this.log.error("Failed to update product authentication " +
                            "info in IQUE DB: " + e.getMessage());
                    throw new InvalidRequestException("Failed to update product " +
                            "authentication info in IQUE DB: " 
                            + e.getMessage());
                }finally{
                    if (conn != null) {
                        conn.closeStatement(stmt);
                        conn.close();
                    }
                }
                
                queryProAuthcodeInfo(this.pro_auth_id);
                this.request.setAttribute("name",this.name);
                this.request.setAttribute("pro_sn",this.pro_sn);
                this.request.setAttribute("assign_date",this.assign_date);
                this.request.setAttribute("pro_type",this.pro_type);
                
            	this.redirect_jsp = PRO_AUTH_SUCC;
            }
        }
    }

    private void queryProAuthcodeInfo(long pro_auth_id)
    	throws InvalidRequestException
    {
        OscDbConnection    conn = null;
        PreparedStatement  stmt = null;
        ResultSet           rs = null;
        
        try {
            conn = this.context.getDB(true);
            stmt = conn.prepareStatement(SQL_QUERY_PRODUCT_AUTH_INFO);
            stmt.setLong(1,pro_auth_id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                this.name 				= rs.getString("name");
                this.pro_sn				= rs.getString("sn");
                this.pro_type 			= rs.getString("rule_desc");
                this.assign_date 		= rs.getString("assign_date");
            }
            rs.close();
            conn.closeStatement(stmt);
            stmt=null;                                
        }
        catch (SQLException e) {
            this.log.error("Failed to query product authentication " +
            		"info: " + e.getMessage());
            throw new InvalidRequestException("Failed to query product " +
            		"authentication info: " + e.getMessage());
        }                	
        finally{
            if (conn != null) {
                conn.closeStatement(stmt);
                conn.close();
            }
        }
    }
    
    private boolean isAuthCode( String auth_code )
    	throws OscCookie.InvalidCookieException
    {
        Cookie[] cookies = this.request.getCookies();
        String cookieString = OscCookie.LOGGEDOUT_STRING;        
        if (cookies != null) {
            for (int i=0; i < cookies.length; ++i) {
                final Cookie c = cookies[i];
                if (COOKIE_LOGIN_SESSION.equals(c.getName())) {
                        cookieString = c.getValue();
                }
            }
        }               
        OscCookie cookie = OscCookie.LOGGEDOUT_COOKIE;
        try {
            cookie = OscCookie.decode(cookieString);
        }
        catch (NumberFormatException e) {
            this.log.error("Illegal number in login cookie: " + e.getMessage());
        }
        catch (NoSuchElementException e) {
            this.log.error("Missing element login cookie: " + e.getMessage());
        }
        catch (OscCookie.InvalidCookieException e) {
            this.log.error("Login failed; cannot decode cookie: " +
                    e.getMessage());
        }
       
        return cookie.isAuthCode(auth_code);
    }
      
    public boolean getIsError() {return this.failureMsg != null;}
    public String getErrorMsg() {return this.failureMsg;}
    public String getErrorCode(){return this.error_code;}  

}

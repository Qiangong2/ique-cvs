package com.broadon.osc.java_util;

import java.io.*;
import java.util.*;

import javax.servlet.http.*;

import com.broadon.status.StatusCode;
import com.broadon.exception.InvalidRequestException;

/** The class used to serve VNG server request for game session registration/end
 *  and returns the appropriate document and header back to the VNG server.
 */

public class OscResultGameSession extends OscResultBean
{
    /** Rules for creating a Game Session result object
     */
    public static class Rule extends OscResultRule {
        
        public static final String GAME_SESSION_REG = "gsess_reg";
        public static final String GAME_SESSION_END = "gsess_end";

        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
        }
        public boolean supports(ServerType t)
        {
            return t == ServerType.RPC;
        }
        public OscResultBean create(HttpServletRequest req,  
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
            return new OscResultGameSession(req, res, this);
        }
    } // class Rule


    // --------------------------------------------
    // Invariant values
    // --------------------------------------------


    // --------------------------------------------
    // Member variables
    // --------------------------------------------

    private final OscResultGameSession.Rule 	rule;
    private final OscContext          		context;
    private final OscLogger          		logger;
    private final String                        actionName;
    private String				status;
    private String				statusMsg;

    private String[]				column_name;
    private String[]				column_value;
    private String				membership_id;
    private String				member_id;
    private String				session_id;
    private String				xml;

    // ----------------------------------------------------------
    // Hidden constructors and helper methods (all constructors should
    // be hidden for result objects, since they are constructed by the
    // Rule class)
    // ---------------------------------------------------------

    private OscResultGameSession(HttpServletRequest  req,  
                                 HttpServletResponse res,
                                 OscResultGameSession.Rule rule)
        throws InvalidRequestException
    {
        this.rule = rule;
        context = rule.getContext();
        logger = context.getLogger();
        actionName = rule.getActionNm();

	status = OscStatusCode.SC_OK;
	statusMsg = StatusCode.getMessage(status);

	column_name = req.getParameterValues(ORP.NAME);
	column_value = req.getParameterValues(ORP.VALUE);
        membership_id = null;
        member_id = null;
        session_id = null;
        xml = "";

        if ((column_name == null && column_value != null) ||
            (column_value == null && column_name != null) ||
            (column_name != null && column_value != null &&
             (column_name.length != column_value.length)))
        {
            status = OscStatusCode.OSC_BAD_REQUEST;
            statusMsg = OscStatusCode.getMessage(OscStatusCode.OSC_BAD_REQUEST);
            logger.error(statusMsg + " for " + actionName +
                    "Incorrect name/value set of values");
            return;
        }
    }

    // --------------------------------------------
    // Specialization of abstract methods
    // --------------------------------------------

    public boolean requiresLogin() {return true;}

    public String jspFwdUrl() {return null;}

    public void encodeXml(PrintWriter out)
    {
        // Encodes the final results
        //
        out.println("<" + ORP.RPC_RESULT_TAG + ">");

        context.encodeXml_TNL(out, ORP.REQ_ACTION, actionName);
        context.encodeXml_TNL(out, STATUS_TAG, status);
        context.encodeXml_TNL(out, STATUS_MSG_TAG, statusMsg);

        if (context.getRpcAppendParams().equals("ON"))
        {
            out.println("\t<" + ORP.RPC_PARAMS_TAG + ">");
            if (column_name != null && column_value != null)
            {
                int count = column_name.length;

                if (count < column_value.length)
                    count = column_value.length;

                for (int i = 0; i < count; i++)
                {
                    if (i < column_name.length)
                        context.encodeXml_TTNL(out, ORP.NAME, column_name[i]);
                    else
                        context.encodeXml_TTNL(out, ORP.NAME, "NULL");
                    if (i < column_value.length)
                        context.encodeXml_TTNL(out, ORP.VALUE, column_value[i]);
                    else
                        context.encodeXml_TTNL(out, ORP.VALUE, "NULL");
                }
            }
            out.println("\t</" + ORP.RPC_PARAMS_TAG + ">");
        }

        out.println("</" + ORP.RPC_RESULT_TAG + ">");
    } // encodeXml

    public void serveRequest(OscLoginSession loginSession)
        throws InvalidRequestException
    {
        if (!getIsError()) {
            session_id = String.valueOf(loginSession.getSessionID());
            membership_id = String.valueOf(loginSession.getMembershipID());
            member_id = String.valueOf(loginSession.getMemberID());

            OscDbConnection conn = null;

            try {
                conn = context.getDB(false);

                String end_date = null;
                if (actionName.equals(rule.GAME_SESSION_END))
                    end_date = context.getCurrentDate();

                xml = OscDmlGameSession.getGameSessionXML(membership_id, 
						          member_id, 
							  session_id, 
							  end_date,
							  column_name, column_value);

                int rows = 0;
                if (actionName.equals(rule.GAME_SESSION_REG))
                    rows = OscDmlGameSession.insertUpdateGameSession(conn, xml);
                else if (actionName.equals(rule.GAME_SESSION_END))
                    rows = OscDmlGameSession.updateGameSession(conn, xml);

                if (rows!=1)
                {
                    status = OscStatusCode.VNG_DB_UPDATE_ERROR;
                    statusMsg = OscStatusCode.getMessage(OscStatusCode.VNG_DB_UPDATE_ERROR);

                    logger.error(statusMsg + " for " + actionName);
                }
                conn.commit();
            }
            catch (Exception e) {
                status = OscStatusCode.VNG_DB_ERROR;
                statusMsg = OscStatusCode.getMessage(OscStatusCode.VNG_DB_ERROR) +
                       ": " + e.getMessage();

                e.printStackTrace();
                logger.error(statusMsg + " for " + actionName);
            }
            finally {
                if (conn != null)
                    conn.close();
            }
        }
    }

    public String toString() {return null;}

    public boolean getIsError()
    {
        return !status.equals(OscStatusCode.SC_OK);
    }

    public String getErrorMsg()
    {
        return statusMsg;
    }

} // class OscResultGameSession

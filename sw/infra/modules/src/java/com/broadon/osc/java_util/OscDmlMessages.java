package com.broadon.osc.java_util;

import java.sql.*;

/**
 * Data manipulation library functions defined here pertain to offline messaging.
 */
public class OscDmlMessages
{
    /** Define all SQL statements
     */

    private static final String TABLE_MESSAGES = "IQUE_MESSAGES";
    private static final String TABLE_MESSAGE_RECEIVERS = "IQUE_MESSAGE_RECEIVERS";

    private static final String GET_NEXT_MESSAGE_ID =
                "SELECT message_id_seq.nextval FROM dual";

    private static final String QUERY_INCOMING_HEADERS =
                "SELECT imr.message_id, im.message_type, m.pseudonym sender, " +
                "mm.pseudonym recipient, im.reply_id, bbxml.converttimestamp(im.send_date) send_date, " +
                "im.subject, im.content_type, im.content_length, imr.read_date, imr.recipient_type " +
                "FROM " + TABLE_MESSAGE_RECEIVERS + " imr, " + TABLE_MESSAGES + " im, " +
                "IQUE_MEMBERS m, IQUE_MEMBERS mm WHERE imr.message_id = im.message_id " +
                "AND im.membership_id = m.membership_id AND im.member_id = m.member_id " +
                "AND imr.membership_id = mm.membership_id AND imr.member_id = mm.member_id " +
                "AND imr.delete_date IS NULL";

    private static final String QUERY_RECIPIENT_MESSAGE =
                "SELECT imr.message_id, im.message_type, m.pseudonym sender, " +
                "mm.pseudonym recipient, im.reply_id, bbxml.converttimestamp(im.send_date) send_date, " + 
                "im.subject, im.content_type, im.content_length, imr.read_date, imr.recipient_type, " +
                "im.message FROM " + TABLE_MESSAGE_RECEIVERS + " imr, " + TABLE_MESSAGES + " im, " +
                "IQUE_MEMBERS m, IQUE_MEMBERS mm WHERE imr.message_id = im.message_id " +
                "AND im.membership_id = m.membership_id AND im.member_id = m.member_id " +
                "AND imr.membership_id = mm.membership_id AND imr.member_id = mm.member_id " +
                "AND imr.delete_date IS NULL";

    private static final String UPDATE_RETRIEVE_MESSAGE =
                "UPDATE " + TABLE_MESSAGE_RECEIVERS + " SET read_date = sysdate " +
                "WHERE read_date IS NULL AND message_id = ?";

    private static final String UPDATE_DELETE_MESSAGE =
                "UPDATE " + TABLE_MESSAGE_RECEIVERS + " SET delete_date = sysdate " +
                "WHERE delete_date IS NULL AND message_id = ?";

    private static final String QUERY_MEMBER_DETAILS =
                "SELECT m.membership_id, m.member_id, ms.status, m.status " +
                "FROM IQUE_MEMBERS m, IQUE_MEMBERSHIPS ms " +
                "WHERE m.membership_id = ms.membership_id AND m.pseudonym = ?";

    public static long getNextMessageID(OscDbConnection conn)
    	throws Exception
    {
        long id = -1;
        PreparedStatement stmt = null;
        stmt = conn.prepareStatement(GET_NEXT_MESSAGE_ID);

        ResultSet rs = stmt.executeQuery();
        if (rs.next()) 
            id = rs.getLong(1);
    
        rs.close();

        return id;
    }

    public static String[][] getReceiverInfo(OscDbConnection conn, 
  			                     String[][] recv,
                                             long quota)
    	throws Exception
    {
        for (int i = 0; i < recv.length; i++)
        {
            PreparedStatement stmt = null;
            stmt = conn.prepareStatement(QUERY_MEMBER_DETAILS);
            stmt.setString(1, recv[i][1]);

            ResultSet rs = stmt.executeQuery();
            if (rs.next()) 
            {
                recv[i][2] = String.valueOf(rs.getLong(1));
                recv[i][3] = String.valueOf(rs.getLong(2));
                if (rs.getString(3).equals("A") && rs.getString(4).equals("A1"))
                {
                    if (quota > getRecipientMessageCount(conn, recv[i][2], recv[i][3]))
                    {
                        recv[i][4] = "000";  
                        recv[i][5] = "OK";  

                    } else {
                        recv[i][4] = OscStatusCode.VNG_MEMBER_EXCEEDED_QUOTA_ERROR;  
                        recv[i][5] = OscStatusCode.getMessage(OscStatusCode.VNG_MEMBER_EXCEEDED_QUOTA_ERROR);  
                    }

                } else {
                    recv[i][4] = OscStatusCode.VNG_MEMBER_ACCOUNT_INACTIVE_ERROR;  
                    recv[i][5] = OscStatusCode.getMessage(OscStatusCode.VNG_MEMBER_ACCOUNT_INACTIVE_ERROR);  
                }

            } else {
                recv[i][2] = null;
                recv[i][3] = null;
                recv[i][4] = OscStatusCode.VNG_MEMBER_NOT_FOUND_ERROR;  
                recv[i][5] = OscStatusCode.getMessage(OscStatusCode.VNG_MEMBER_NOT_FOUND_ERROR);  
            }

            rs.close();
        }
 
        return recv;
    }

    public static String storeMessage (OscDbConnection conn, 
   		                       String message_type,
 			               String membership_id,
			               String member_id,
   		                       String reply_id,
   		                       String send_date,
   		                       String subject,
   		                       String message,
   		                       String ip_address,
   		                       String content_type,
   		                       String content_length,
   		                       String[][] recv)
    	throws Exception
    {
        String message_id = String.valueOf(getNextMessageID(conn));
        if (message_id != null && !message_id.equals("-1"))
        {

            String msgData = "<ROWSET><ROW>" +
                             "<MESSAGE_TYPE>" + message_type + "</MESSAGE_TYPE>" +
                             "<MESSAGE_ID>" + message_id + "</MESSAGE_ID>" +
                             "<MEMBERSHIP_ID>" + membership_id + "</MEMBERSHIP_ID>" +
                             "<MEMBER_ID>" + member_id + "</MEMBER_ID>" +
                             "<CONTENT_TYPE>" + content_type + "</CONTENT_TYPE>" +
                             "<CONTENT_LENGTH>" + content_length + "</CONTENT_LENGTH>";

            if (send_date != null && !send_date.equals(""))
                msgData += "<SEND_DATE>" + send_date + "</SEND_DATE>";
            if (subject != null && !subject.equals(""))
                msgData += "<SUBJECT>" + subject + "</SUBJECT>";
            if (message != null && !message.equals(""))
                msgData += "<MESSAGE>" + message + "</MESSAGE>";
            if (ip_address != null && !ip_address.equals(""))
                msgData += "<IP_ADDRESS>" + ip_address + "</IP_ADDRESS>";
            if (reply_id != null && !reply_id.equals(""))
                msgData += "<REPLY_ID>" + reply_id + "</REPLY_ID>";

            msgData += "</ROW></ROWSET>";

            if (conn.insertXML(msgData, TABLE_MESSAGES) == 1)
            {
                int count = 0;
                String rcvData = "<ROWSET>";
                
                if (recv != null)
                {
                    for (int i = 0; i < recv.length; i++)
                    {
                        if (recv[i][2] != null)
                        {
                            rcvData += "<ROW>" +
                                       "<MESSAGE_ID>" + message_id + "</MESSAGE_ID>" +
                                       "<MEMBERSHIP_ID>" + recv[i][2] + "</MEMBERSHIP_ID>" +
                                       "<MEMBER_ID>" + recv[i][3] + "</MEMBER_ID>" +
                                       "<RECIPIENT_TYPE>" + recv[i][0] + "</RECIPIENT_TYPE>" +
                                       "</ROW>";
                            count++;
                         }
                    }
                }

                rcvData += "</ROWSET>";
              
                if (conn.insertXML(rcvData, TABLE_MESSAGE_RECEIVERS) != count)
                    message_id = "-1";
            } else
                message_id = "-1";
        }

        return message_id;
    }

    public static int countIncomingHeadersList(OscDbConnection conn,
                                               String membership_id,
                                               String member_id)
        throws Exception
    {
        return conn.GetRecordCount(QUERY_INCOMING_HEADERS +
                    " AND imr.membership_id = " + membership_id +
                    " AND imr.member_id = " + member_id);
    }

    public static String getIncomingHeadersList(OscDbConnection conn,
                                                String membership_id,
                                                String member_id,
                                                int numrows,
                                                int skiprows)
        throws Exception
    {
        return conn.queryXML(QUERY_INCOMING_HEADERS +
                    " AND imr.membership_id = " + membership_id +
                    " AND imr.member_id = " + member_id + 
                    " ORDER BY im.send_date desc", numrows, skiprows);
    }

    public static boolean checkSender(OscDbConnection conn, 
			              String membership_id,
			              String member_id,
			              String message_id)
    	throws Exception
    {
        int count = conn.GetRecordCount("SELECT * FROM " + TABLE_MESSAGES +
                    " WHERE membership_id = " + membership_id + " AND member_id = " + member_id +
                    " AND message_id = " + message_id);
 
        if (count > 0)
            return true;
        else
            return false;
    }

    public static String getMessageByRecipient(OscDbConnection conn, 
  		                               String membership_id, 
    			                       String member_id,
		                               String message_id)
    	throws Exception
    {
        String sql = UPDATE_RETRIEVE_MESSAGE + " AND membership_id = ? AND member_id = ?";

        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setLong(1, Long.parseLong(message_id));
        ps.setLong(2, Long.parseLong(membership_id));
        ps.setLong(3, Long.parseLong(member_id));

        int row = ps.executeUpdate();

        ps.close();

        return conn.queryXML(QUERY_RECIPIENT_MESSAGE +
                    " AND imr.membership_id = " + membership_id +
                    " AND imr.member_id = " + member_id +
                    " AND imr.message_id = " + message_id);
    }

    public static int deleteMessageByRecipient(OscDbConnection conn, 
  		                               String membership_id, 
    			                       String member_id,
		                               String message_id)
    	throws Exception
    {
        String sql = UPDATE_DELETE_MESSAGE + " AND membership_id = ? AND member_id = ?";

        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setLong(1, Long.parseLong(message_id));
        ps.setLong(2, Long.parseLong(membership_id));
        ps.setLong(3, Long.parseLong(member_id));

        int row = ps.executeUpdate();

        ps.close();

        return row;
    }

    public static long getSenderMessageLength(OscDbConnection conn, 
			                     String membership_id, 
			                     String member_id)
    	throws Exception
    {
        long result = -1;
        String sql = "SELECT sum(content_length) FROM " + TABLE_MESSAGES + 
                     " WHERE membership_id = " + membership_id + " AND member_id = " + member_id + 
                     " AND create_date >= sysdate - 1";

        ResultSet rs = conn.query(sql);
        if (rs.next())
            result = rs.getLong(1);

        return result;
    }

    public static int getSenderMessageCount(OscDbConnection conn, 
			                    String membership_id, 
			                    String member_id)
    	throws Exception
    {
        return conn.GetRecordCount("SELECT message_id FROM " + TABLE_MESSAGES + 
                     " WHERE membership_id = " + membership_id + " AND member_id = " + member_id + 
                     " AND create_date >= sysdate - 1");
    }

    public static int getRecipientMessageCount(OscDbConnection conn, 
 		                               String membership_id, 
		                               String member_id)
    	throws Exception
    {
        return conn.GetRecordCount("SELECT message_id FROM " + TABLE_MESSAGE_RECEIVERS + 
                     " WHERE membership_id = " + membership_id + " AND member_id = " + member_id + 
                     " AND delete_date IS NULL");
    }

} // OscDmlMessages

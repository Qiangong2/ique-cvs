package com.broadon.osc.java_util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.NoSuchElementException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/** The <code>OscLoginSession</code> represent the current login session
 *  for an OSC server request.
 *
 *  This interface also takes care of session changes due to logout or
 *  login.  Make sure <code>setResponse()</code> is called prior to
 *  sending a response to the user, such that the next request uses
 *  updated logon status information.
 */
public final class OscLoginSession
{
    // --------------------------------------------
    // Invariant values
    // --------------------------------------------

    private static final String SHA_1 = "SHA-1";
    private static final String COOKIE_LOGIN_SESSION = "IqahLoginSession";
    private static final String COOKIE_SERVER_PATH = "/osc";
    private static final String ERR_INCORRECT_LOGIN = OscStatusCode.OSC_INCORRECT_LOGIN;
    private static final String ERR_ACCOUNT_INACTIVE = OscStatusCode.OSC_ACCOUNT_INACTIVE;

    private static final String SQL_SELECT_ACCOUNT =
    "SELECT m.membership_id, m.member_id, ms.status, m.status " +
    "FROM ique_members m, ique_memberships ms " +
    "WHERE m.pseudonym = ? AND m.pin = ? " +
    "  AND ms.membership_id = m.membership_id";

    private static final String SQL_SELECT_LOGIN =
    "SELECT membership_id, member_id, max_idle_time, max_session_time, " +
    "       session_start_time, session_last_time, session_seq, " +
    "       is_expired " +
    "FROM ique_member_logins WHERE session_id = ?";

    private static final String SQL_SELECT_LOGIN_CACHED =
    "SELECT session_last_time, session_seq, is_expired " +
    "FROM ique_member_logins WHERE session_id = ?";

    private static final String SQL_UPDATE_LOGIN =
    "UPDATE ique_member_logins " +
    "SET session_last_time=?, session_seq=session_seq+1 " +
    "WHERE session_id = ?";

    private static final String SQL_NEXT_SESSIONID_SEQ =
    "SELECT osc.session_id_seq.nextval from dual";

    private static final String SQL_INSERT_LOGIN =
    "INSERT INTO ique_member_logins " +
    "  (session_id, membership_id, member_id, max_idle_time, " +
    "   max_session_time, session_start_time, session_last_time, " +
    "   session_seq, is_expired, game_code_failures) " +
    "VALUES (?, ?, ?, ?, ?, ?, ?, 1, 0, 0)";

    private static final String SQL_UPDATE_LOGOUT =
    "UPDATE ique_member_logins " +
    "SET is_expired=1 " +
    "WHERE session_id = ?";

    private static final String SQL_UPDATE_GAMECODEFAILURES =
    "UPDATE ique_member_logins " +
    "SET game_code_failures = NVL(game_code_failures, 0) + 1 " +
    "WHERE session_id = ?";

    private static final String SQL_SELECT_GAMECODEFAILURES =
    "SELECT game_code_failures " +
    "FROM ique_member_logins " +
    "WHERE session_id = ?";

    // --------------------------------------------
    // Member variables
    // --------------------------------------------

    private final OscLogger  log;
    private final OscContext context;
    private final long       currentTime;
    private boolean          isDeviceLogin;
    private OscCookie        cookie;
    private LoginInfo        login;
    
    private String loginErrorCode;


    // -------------------------------------------------------
    // The information kept in the DB and possibly cached here
    // -------------------------------------------------------

    private static class LoginInfo {
        private long membsId;    // Membership id
        private long memId;      // Member id
        private long maxIdleTm;
        private long maxSessionTm;
        private long startTm;
        private long lastTm;
        private int  seqNum;

        public LoginInfo(long membsId,
                         long memId,
                         long maxIdleTm,
                         long maxSessionTm,
                         long startTm,
                         long lastTm,
                         int  seqNum)
        {
            this.membsId = membsId;
            this.memId = memId;
            this.maxIdleTm = maxIdleTm;
            this.maxSessionTm = maxSessionTm;
            this.startTm = startTm;
            this.lastTm = lastTm;
            this.seqNum = seqNum;
        }

        public boolean isExpired(long currentTm)
        {
            return ((startTm < (currentTm - maxSessionTm)) ||
                    (lastTm < (currentTm - maxIdleTm)));
        }

        public boolean isOutOfSequence(long lastSeqNum)
        {
            return lastSeqNum > seqNum;
        }
        public long getMembershipID() {return membsId;}
        public long getMemberID() {return memId;}
        public long getMaxIdleTime() {return maxIdleTm;}
        public long getMaxSessionTime() {return maxSessionTm;}
        public long getSessionStartTime() {return startTm;}
        public long getSessionLastTime() {return lastTm;}
        public int  getSessionSeq() {return seqNum;}

        public void setSessionLastTime(long tm) {lastTm = tm;}
        public void setSessionSeq(int seq) {seqNum = seq;}
        public String toString()
        {
            StringBuffer b = new StringBuffer(256);
            b.append("membs_id="); b.append(membsId);
            b.append(", memId="); b.append(memId);
            b.append(", maxIdleTm="); b.append(maxIdleTm);
            b.append(", maxSessionTm="); b.append(maxSessionTm);
            b.append(", startTm="); b.append(startTm);
            b.append(", lastTm="); b.append(lastTm);
            b.append(", seqNum="); b.append(seqNum);
            return b.toString();
        }
    } // class LoginInfo


    private static class AccountInfo {
        private long   membershipID;
        private long   memberID;
        private String pseudonym;
        private String pin;
        private String ms_status;
        private String m_status;

        AccountInfo(long   membershipID,
                    long   memberID,
                    String pseudonym,
                    String pin,
                    String ms_status,
                    String m_status)
        {
            this.membershipID = membershipID;
            this.memberID = memberID;
            this.pseudonym = pseudonym;
            this.pin = pin;
            this.ms_status = ms_status;
            this.m_status = m_status;
        }
        private long    getMembershipID() {return membershipID;}
        private long    getMemberID() {return memberID;}
        private String  getMembershipStatus() {return ms_status;}
        private String  getMemberStatus() {return m_status;}
        private String  getPseudonym() {return pseudonym;}
        private String  getPin() {return pin;}
        private boolean isActive()
        {
            return (ms_status.equals("A") &&
                    m_status.equals("A1"));
        }
    } // class AccountInfo


    // --------------------------------------------
    // Static utility methods (hidden)
    // --------------------------------------------

    private static Cookie getCookie(HttpServletRequest req,
                                    String             cookieName)
    {
        Cookie[] cookies = req.getCookies();
        if (cookies != null) {
            for (int i=0; i < cookies.length; ++i) {
                final Cookie cookie = cookies[i];
                if (cookieName.equals(cookie.getName())) {
                    return cookie;
                }
            }
        }
        return null;
    }


    private static String getCookieValue(HttpServletRequest req,
                                         String             cookieName,
                                         String             defaultValue)
    {
        final Cookie cookie = getCookie(req, cookieName);
        if (cookie != null)
            return cookie.getValue();
        else
            return defaultValue;
    }


    // --------------------------------------------
    // Member utility methods (hidden)
    // --------------------------------------------


    private AccountInfo getAccountInfo(String pseudonym, String pin)
    {
        // Find the entries for the member in the DB and get the associated
        // values.  Return null if no such member exists.
        //
        // TODO: First try to look it up in a local cache, and if found just
        // look up the missing data.
        //
        AccountInfo       info = null;
        OscDbConnection   conn = null;
        PreparedStatement stmt = null;
        try {
            conn = context.getDB(true);
            stmt = conn.prepareStatement(SQL_SELECT_ACCOUNT);
            stmt.setString(1, pseudonym);
            stmt.setString(2, pin);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                final long membershipID = rs.getLong(1);
                final long memberID     = rs.getLong(2);
                final String ms_status  = rs.getString(3);
                final String m_status   = rs.getString(4);
                info = new AccountInfo(membershipID, memberID,
                                       pseudonym, pin,
                                       ms_status, m_status);
            }
            rs.close();
        }
        catch (SQLException e) {
            log.error("Failed to access account info in IQUE DB: " +
                      e.getMessage());
        }
        finally {
            if (conn != null) {
                conn.closeStatement(stmt);
                conn.close();
            }
        }
        return info;
    } // getAccountInfo


    private LoginInfo getLoginInfo(long sessionID)
    {
        // Find the entry for the session in the DB and get the associated
        // values.  Return null if no such session exists.
        //
        // TODO: First try to look it up in a local cache, and if found just
        // look up the missing data.
        //
        LoginInfo         info = null;
        OscDbConnection   conn = null;
        PreparedStatement stmt = null;
        try {
            conn = context.getDB(true);
            stmt = conn.prepareStatement(SQL_SELECT_LOGIN);
            stmt.setLong(1, sessionID);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                final long    membsId = rs.getLong(1);
                final long    memId = rs.getLong(2);
                final long    maxIdleTm = rs.getLong(3);
                final long    maxSessTm = rs.getLong(4);
                final long    sessStartTm = rs.getLong(5);
                final long    sessLastTm = rs.getLong(6);
                final int     sessSeqNum = rs.getInt(7);
                final boolean sessIsExpired = (rs.getInt(8) != 0);

                if (!sessIsExpired)
                    info = new LoginInfo(membsId, memId,
                                         maxIdleTm, maxSessTm,
                                         sessStartTm, sessLastTm,
                                         sessSeqNum);
            }
            rs.close();
        }
        catch (SQLException e) {
            log.error("Failed to access login session in IQUE DB: " +
                      e.getMessage());
        }
        finally {
            if (conn != null) {
                conn.closeStatement(stmt);
                conn.close();
            }
        }
        return info;
    } // getLoginInfo


    private long getNextSessionIdSeq()
    {
        // Get the next available session ID sequence number from the DB.
        //
        long            s = 0;
        OscDbConnection conn = null;
        try {
            conn = context.getDB(true);
            ResultSet rs = conn.query(SQL_NEXT_SESSIONID_SEQ);
            if (rs.next()) {
                s = rs.getLong(1);
            }
            rs.close();
        }
        catch (SQLException e) {
            log.error("Failed to get next session id in IQUE DB: " +
                      e.getMessage());
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return s;
    } // getNextSessionIdSeq


    private void updateLoginDB()
    {
        // Update the DB to reflect this request.
        //
        OscDbConnection   conn = null;
        PreparedStatement stmt = null;
        try {
            conn = context.getDB(false);
            stmt = conn.prepareStatement(SQL_UPDATE_LOGIN);
            stmt.setLong(1, currentTime);
            stmt.setLong(2, cookie.getSessionID());
            stmt.executeUpdate();
            conn.commit();
        }
        catch (SQLException e) {
            log.error("Failed to update login session in IQUE DB: " +
                      e.getMessage());
        }
        finally {
            if (conn != null) {
                conn.closeStatement(stmt);
                conn.close();
            }
        }
    } // updateLoginDB


    private void updateLogoutDB()
    {
        // Update the DB to reflect this request.
        //
        OscDbConnection   conn = null;
        PreparedStatement stmt = null;
        try {
            conn = context.getDB(false);
            stmt = conn.prepareStatement(SQL_UPDATE_LOGOUT);
            stmt.setLong(1, cookie.getSessionID());
            stmt.executeUpdate();
            conn.commit();
        }
        catch (SQLException e) {
            log.error("Failed to logout session in IQUE DB: " +
                      e.getMessage());
        }
        finally {
            if (conn != null) {
                conn.closeStatement(stmt);
                conn.close();
            }
        }
    } // updateLogoutDB

    private void updateGameCodeFailuresDB()
    {
        // Update the DB to reflect this request.
        //
        OscDbConnection   conn = null;
        PreparedStatement stmt = null;
        try {
            conn = context.getDB(false);
            stmt = conn.prepareStatement(SQL_UPDATE_GAMECODEFAILURES);
            stmt.setLong(1, cookie.getSessionID());
            stmt.executeUpdate();
            conn.commit();
        }
        catch (SQLException e) {
            log.error("Failed to update GAME_CODE_FAILURES in IQUE DB: " +
                      e.getMessage());
        }
        finally {
            if (conn != null) {
                conn.closeStatement(stmt);
                conn.close();
            }
        }
    } // updateGameCodeFailuresDB

    private long getGameCodeFailuresDB()
    {
        // Get game code failures from the DB.
        //
        long            s = 0;
        OscDbConnection conn = null;
        PreparedStatement stmt = null;

        try {
            conn = context.getDB(true);
            stmt = conn.prepareStatement(SQL_SELECT_GAMECODEFAILURES);
            stmt.setLong(1, cookie.getSessionID());
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                s = rs.getLong(1);
            }
            rs.close();

        }
        catch (SQLException e) {
            log.error("Failed to get game_code_failures in IQUE DB: " +
                      e.getMessage());
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return s;
    } // getGameCodeFailuresDB

    private void insertLoginDB(long sessionID, LoginInfo l)
    {
        // TODO: Enter value into local cache.
        //
        OscDbConnection   conn = null;
        PreparedStatement stmt = null;
        try {
            conn = context.getDB(false);
            stmt = conn.prepareStatement(SQL_INSERT_LOGIN);
            stmt.setLong(1, sessionID);
            stmt.setLong(2, l.getMembershipID());
            stmt.setLong(3, l.getMemberID());
            stmt.setLong(4, l.getMaxIdleTime());
            stmt.setLong(5, l.getMaxSessionTime());
            stmt.setLong(6, l.getSessionStartTime());
            stmt.setLong(7, l.getSessionLastTime());
            stmt.executeUpdate();
            conn.commit();
        }
        catch (SQLException e) {
            log.error("Failed to insert login session in IQUE DB: "  +
                      e.getMessage());
        }
        finally {
            if (conn != null) {
                conn.closeStatement(stmt);
                conn.close();
            }
        }
    } // insertLoginDB


    private String getEncryptedPasswd(String passwd)
        throws NoSuchAlgorithmException
    {
        MessageDigest md = MessageDigest.getInstance(SHA_1);
        md.reset();

        byte[] encoded = md.digest(passwd.getBytes());

        StringBuffer b = new StringBuffer(160);
        for (int i=0; i<encoded.length; i++)
        {
            String elem = Integer.toHexString(encoded[i]&0XFF).toUpperCase();

            if (elem.length() == 1) {
                b.append("0");
            }

            b.append(elem);
        }
        return b.toString();
    } // getEncryptedPasswd


    // --------------------------------------------
    // Constructors (hidden)
    // --------------------------------------------

    private OscLoginSession(OscContext c, OscCookie info)
    {
        log = c.getLogger();
        context = c;        
        loginErrorCode = null;
        
        currentTime = context.getCurrentTime().getTime();
        isDeviceLogin = false;
        cookie = info;
        login = null;

        if (log.isTracing())
            log.info(" incoming cookie=\"" + cookie.encode() +
                     "\"=" + cookie.toString());

        if (!cookie.isLoggedOut()) {
            //
            // Look up LoginInfo
            //
            login = getLoginInfo(cookie.getSessionID());

            // Validate the login session
            //
            if (login == null) {
                //
                // Reflect the fact that the user had already logged off
                //
                if (log.isTracing())
                    log.info("user session " + cookie.getSessionID() +
                             " not found (already logged off)");
                cookie = OscCookie.LOGGEDOUT_COOKIE;
            }
            else {
                //
                // Force a logout if the session has expired or the session
                // parameters in the cookie are invalid.
                //
                if (login.isExpired(currentTime)                      ||
                    login.isOutOfSequence(cookie.getSeqNum())         ||
                    cookie.getMembershipID() != login.getMembershipID() ||
                    cookie.getMemberID() != login.getMemberID()) {
                    if (log.isTracing())
                        log.info("user session " + cookie.getSessionID() +
                                 " is expired, out of sequence, or invalid");
                    logout();
                }
                else {
                    //
                    // Update the DB to reflect this access to the login
                    // session.
                    //
                    if (log.isTracing())
                        log.info("user session " + cookie.getSessionID() +
                                 " continues with new request");
                    updateLoginDB();
                    login.setSessionLastTime(currentTime);
                    login.setSessionSeq(login.getSessionSeq() + 1);
                }
            }
        }
    } // constructor for OscLoginSession


    // --------------------------------------------
    // Public methods and internal classes
    // --------------------------------------------

    static public class InternalLoginException extends OscException {

        public InternalLoginException() {super();}
        public InternalLoginException(String message) {super(message);}
        public InternalLoginException(String message, Throwable throwable) {
            super(message, throwable);
        }
    }

    /** Logs the given subscriber into the system, possibly logging the
     *  subscriber out first if already logged in.  isLoggedIn() == true
     *  if the login was successful, and false if unsuccessful.
     *
     *  @param userName is the pseudonym for the user
     *  @param password is the hashed password for the user
     */
    public void login(String userName, String plain_password)
        throws InternalLoginException
    {
        if (isLoggedIn()) {
            if (log.isTracing())
                log.info("user session " + cookie.getSessionID() +
                         " pre-empted by new login");
            logout();
        }
        try {
            String passwd = getEncryptedPasswd(plain_password);

            // Look up account information in the DB
            //
            AccountInfo account = getAccountInfo(userName, passwd);
            if (account != null && account.isActive()) {

                // Create Login information
                //
                login = new LoginInfo(account.getMembershipID(),
                                      account.getMemberID(),
                                      isDeviceLogin ? context.getDeviceMaxLoginIdleMsecs() : context.getMaxLoginIdleMsecs(),
                                      isDeviceLogin ? context.getDeviceMaxLoginMsecs() : context.getMaxLoginMsecs(),
                                      currentTime, // session start time
                                      currentTime, // session last time
                                      1);          // first sequence number

                // Create the cookie information for this new login session.
                //
                final long sessID = getNextSessionIdSeq();
                cookie = new OscCookie(sessID,
                                       account.getMembershipID(),
                                       account.getMemberID(),
                                       login.getSessionStartTime(),
                                       login.getSessionLastTime(),
                                       login.getSessionSeq(),
                                       userName);
                insertLoginDB(sessID, login);
                if (log.isTracing())
                    log.info("Login: user session " + cookie.getSessionID());
            } else {            
            	if(account == null)
            		setLoginErrorCode(ERR_INCORRECT_LOGIN);
            	else if(!account.isActive())
            		setLoginErrorCode(ERR_ACCOUNT_INACTIVE);
            	
            	if (log.isTracing()) {
            		log.info("Account for " + userName + " not found or not active");
            	}
            }
            
        }
        catch (OscCookie.InvalidCookieException e) {
            login = null;
            throw new InternalLoginException(
                "Cannot construct cookie: ", e);
        }
        catch (NoSuchAlgorithmException e) {
            login = null;
            throw new InternalLoginException(
                "Cannot encrypt passwd during login ", e);
        }
    } // login


    /** Same as above, but login with respect to an authenticationCode
     *  present in the cookie.
     *
     * @param authCode is the authentication code set in the cookie
     *    by the AuthCodeServlet.
     * @return true when the login authentication code is correct
     *    (not that any other problems will be signified by the 
     *    user still being logged out.
     */
    public boolean loginAuthCode(String userName, 
                                 String plainPassword,
                                 String authCode)
        throws InternalLoginException
    {
        // We special case "1234" as a valid authorization code for
        // testing purposes
        //
        boolean authOK = true;
        try {
//            if (authCode.equals("1234") || cookie.isAuthCode(authCode))
           	if (cookie.isAuthCode(authCode))
                login(userName, plainPassword);
            else
                authOK = false;
        }
        catch (OscCookie.InvalidCookieException e) {
            authOK = false;
            log.error("Login failed; cannot decode auth-code " +
                      e.getMessage());
        }
        return authOK;
    }


    /** Logs the user out, updating the DB session and this object
     *  accordingly.
     */
    public void logout()
    {
        // Delete current session, if any, from database
        //
        if (isLoggedIn()) {
            if (log.isTracing())
                log.info("Logout: user session " + cookie.getSessionID());

            // Update the DB (set session to be expired)
            //
            updateLogoutDB();
            //
            // Update this object and the cookie
            //
            //
            login = null;
            cookie = OscCookie.LOGGEDOUT_COOKIE;
        }
        else if (log.isTracing()) {
            log.info("Attempt to logout before login");
        }
    }


     /** Update game_code_failures, and if game_code_failures exceed the allowed limit, 
      *  Logs the user out
      */
    public boolean UpdateGameCodeFailures()
    {
         boolean exceedFailureLimits = false;
         
         updateGameCodeFailuresDB();
         if( getGameCodeFailuresDB() >= context.getMaxGameCodeFailures())
         {
             logout();
             exceedFailureLimits = true; 
         }
         return exceedFailureLimits;
    }

   /** Test to determine whether or not this login object represents
     *  a login session or a logged out session.
     *
     *  @returns true if the subscriber is currently logged in
     */
    public boolean isLoggedIn()
    {
        return login != null;
    }

   /** Set isDeviceLogin flag to obtain right set login parameters
     *
     *  @param flag	truw/false
     */
    public void setDeviceLogin(boolean flag)
    {
        isDeviceLogin = flag;
        return;
    }

    /** The psudonym of the user logged in.
     *
     *  @returns null when the user is not logged in
     */
    public String getPseudonym()
    {
        if (isLoggedIn())
            return cookie.getPseudonym();
        else
            return null;
    }

    /** The membership group of the user logged in.
     *
     *  @returns 0 when the user is not logged in
     */
    public long getMembershipID()
    {
        return cookie.getMembershipID();
    }

    /** The member identity of the user logged in.
     *
     *  @returns 0 when the user is not logged in
     */
    public long getMemberID()
    {
        return cookie.getMemberID();
    }

    /** The session id of the user logged in.
     *
     *  @returns 0 when the user is not logged in
     */
    public long getSessionID()
    {
        return cookie.getSessionID();
    }

    /** Sets the HTTP header in the response to contain the current
     *  login session information.  Make sure ALWAYS to call this
     *  when a response is issued.
     */
    public void setResponse(HttpServletResponse res)
    {
        String encodedCookie = OscCookie.LOGGEDOUT_STRING;
        if (isLoggedIn()) {
            cookie.resetAuthCode();
            encodedCookie = cookie.encode(login.getSessionLastTime(),
                                          login.getSessionSeq());
        }
        setLoginCookie(res, encodedCookie);
        if (log.isTracing())
            log.info(" outgoing cookie=" + encodedCookie);
    }

    // -------------------------------------------
    // Factory methods and static utility methods
    // -------------------------------------------

    public static OscCookie getLoginCookie(OscContext         c,
                                           HttpServletRequest req)
    {
        String cookieString = getCookieValue(req,
                                             COOKIE_LOGIN_SESSION,
                                             OscCookie.LOGGEDOUT_STRING);

        OscCookie cookie = OscCookie.LOGGEDOUT_COOKIE;
        try {
            cookie = OscCookie.decode(cookieString);
        }
        catch (NumberFormatException e) {
            c.getLogger().error("Illegal number in login cookie: " + e.getMessage());
        }
        catch (NoSuchElementException e) {
            c.getLogger().error("Missing element login cookie: " + e.getMessage());
        }
        catch (OscCookie.InvalidCookieException e) {
            c.getLogger().error("Login failed; cannot decode cookie: " +
                                e.getMessage());
        }
        return cookie;
    }


    public static void setLoginCookie(HttpServletResponse res,
                                      String              encodedCookie)
    {
        Cookie osc_cookie = new Cookie(COOKIE_LOGIN_SESSION, encodedCookie);
        osc_cookie.setPath(COOKIE_SERVER_PATH);
        res.addCookie(osc_cookie);
    }

    /** Creates the <code>OscLoginSession</code> object based on the request
     *  information in attached cookies, if any.
     *
     * @returns a valid OscLoginSession object, which indicates that
     * the user is logged in only if this is indeed the case.  Any
     * logon timeouts or missing/invalid session information will
     * result in a session object reflecting a logged out user.
     */
    public static OscLoginSession create(OscContext c, HttpServletRequest req)
    {
        OscCookie cookie = getLoginCookie(c, req);

        // <TODO> Insert any cache usage/optimization here

        return new OscLoginSession(c, cookie);
    }
    
    /** Get the error code of login.
     * 
     * @return
     */
    public String getLoginErrorCode(){
    	return loginErrorCode;
    }
    
    /** Set the error code of login.
     * 
     * @param errorCode
     */
    private void setLoginErrorCode(String errorCode){
    	this.loginErrorCode = errorCode;
    }


} // class OscLoginFactory

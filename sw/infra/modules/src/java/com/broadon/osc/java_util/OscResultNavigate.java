package com.broadon.osc.java_util;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.broadon.exception.InvalidRequestException;


/** The class used to serve the request to display the main login page
 *  associated with loginform request.
 */
public class OscResultNavigate extends OscResultBean
{
    /** Rules for creating the Main Osc Login page
     */
    public static class Rule extends OscResultRule {
    	
        public static final String NAVIGATE = "navigate";
        
        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
        }
        public boolean supports(ServerType t)
        {
            return t == ServerType.HTM;
        }
        public OscResultBean create(HttpServletRequest req,  
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
	    return new OscResultNavigate(req, res, this);
        }
    } // class Rule


    // --------------------------------------------
    // Invariant values
    // --------------------------------------------

    private static final String NAVIGATE_JSP = "Navigate.jsp";


    // --------------------------------------------
    // Member variables
    // --------------------------------------------

    private final  OscResultNavigate.Rule rule;
    private final  OscContext              context;


    // --------------------------------------------
    // Hidden constructors (all constructors should
    // be hidden for result objects, since they are
    // constructed by the Rule class)
    // --------------------------------------------

    private OscResultNavigate(HttpServletRequest  req,  
                               HttpServletResponse res,
                               OscResultNavigate.Rule rule)
    {
        this.rule = rule;
        context = rule.getContext();
    }


    // --------------------------------------------
    // Specialization of abstract methods
    // --------------------------------------------

    public boolean requiresLogin() {return false;}

    public String jspFwdUrl() 
    {
        return NAVIGATE_JSP;
    }

    public void encodeXml(PrintWriter out) {};

    public void serveRequest(OscLoginSession loginSession)
        throws InvalidRequestException
    { }

    // --------------------------------------------
    // Definition of result specific bean values used
    // for formatting a result for access through
    // JSP pages.
    // --------------------------------------------

} // class OscResultNavigate


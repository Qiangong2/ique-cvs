package com.broadon.osc.java_util;

import java.util.HashMap;


/** A fully synchronized wrapper around a map of OscCachedObjects,
 *  which ensures atomic test and insert of a new value if missing.
 */
public class OscCachedMap
{
    HashMap m;

    public OscCachedMap(int initialCapacity)
    {
        m = new HashMap(initialCapacity);
    }

    public synchronized OscCachedObject getMapped(Object cacheKey)
    {
        OscCachedObject cache = (OscCachedObject)m.get(cacheKey);
        if (cache == null) {
            cache = new OscCachedObject();
            m.put(cacheKey, cache);
        }
        return cache;
    }
} // class OscCachedMap

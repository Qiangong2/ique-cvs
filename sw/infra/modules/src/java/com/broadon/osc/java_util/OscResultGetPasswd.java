package com.broadon.osc.java_util;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.NoSuchAlgorithmException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.broadon.exception.InvalidRequestException;


/** The class used to create the results associated with a request
 *  to obtain password, such that the appropriate document and header can be
 *  created in the response back to the client
 */
public class OscResultGetPasswd extends OscResultBean
{
    /** Rules for creating a Login result object
     */
    public static class Rule extends OscResultRule {

        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
        }
        public boolean supports(ServerType t)
        {
            return t == ServerType.HTM;
        }
        public OscResultBean create(HttpServletRequest req,
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
            return new OscResultGetPasswd(req, res, this);
        }
    } // class Rule

    // --------------------------------------------
    // Invariant values
    // --------------------------------------------

    private static final String GETPASSWD_ERR_JSP 		= "GetPasswd.jsp";
    private static final String GETPASSWD_SUCC_JSP 		= "RedirectNonsecure.jsp";
    private static final String REDIRECT_HOME_ACTION 	= "login_form";    
    private static final String DEFAULT_LOCALE			= "zh_CN";

    private static final String SQL_QUERY_MEMBERS =
        "SELECT membership_id,member_id,member_email FROM ique_members " +
        "WHERE pseudonym=? AND name=? AND member_email=? AND status like 'A%'";

    private static final String SQL_UPDATE_MEMBERS =
        "UPDATE ique_members SET pin=? WHERE membership_id=? " +
        "and member_id=?";


    // --------------------------------------------
    // Member variables
    // --------------------------------------------

    private final OscResultGetPasswd.Rule   rule;
    private final OscContext                context;
    private final OscLogger                 log;
    

    private String      failureMsg;
    private String      locale;
    private String      redirect_jsp;
    private String      error_code;
    private String      pseudonym;
    private String      name;

    private String      member_email;
    private String      pin_plain;
    private String      pin_encry;
    private long        membership_id;
    private int         member_id;
    private boolean     commit;

    // --------------------------------------------
    // Hidden constructors (all constructors should
    // be hidden for result objects, sicne they are
    // constructed by the Rule class)
    // --------------------------------------------

    private OscResultGetPasswd(HttpServletRequest  req,
                               HttpServletResponse res,
                               OscResultGetPasswd.Rule rule)
        throws InvalidRequestException
    {
    	this.rule 		= rule;
        this.context 	= rule.getContext();
        this.log 		= this.context.getLogger();

        if(req.getParameter("commit")!=null && req.getParameter("commit").equals("1")){
            this.commit = true;
        }else{
            this.commit = false;
        }

        if (this.commit) {
            this.pseudonym = ( req.getParameter("pseudonym")!=null &&
                         	  !req.getParameter("pseudonym").trim().equals(""))
                         	  ?req.getParameter("pseudonym").trim():null;

            this.name = ( req.getParameter("name")!=null &&
                    	 !req.getParameter("name").trim().equals(""))
                    	 ?req.getParameter("name").trim():null;

            this.member_email = ( req.getParameter("member_email")!=null &&
                            	 !req.getParameter("member_email").trim().equals(""))
                            	 ?req.getParameter("member_email").trim():null;

            if (this.pseudonym==null) {
                this.failureMsg = "pseudonym is null";
                this.error_code = "1";
            } else if (!OscContext.isValidPseudonym(this.pseudonym)) {
                this.failureMsg = "pseudonym is not valid.";
                this.error_code = "21";
            } else if (this.name==null) {
                this.failureMsg = "name is null";
                this.error_code = "3";
            } else if (this.member_email==null) {
                this.failureMsg = "member_email is null";
                this.error_code = "11";
            } else if (!OscContext.isValidEmailAddress(this.member_email)) {
                this.failureMsg = "member_email is invalid";
                this.error_code = "31";
            } else {
                    this.locale = ( req.getParameter("locale")!=null &&
                              	   !req.getParameter("locale").equals(""))
                              	   ?req.getParameter("locale"):DEFAULT_LOCALE;
            }
        }
    }

    // --------------------------------------------
    // Specialization of abstract methods
    // --------------------------------------------

    public boolean requiresSecureRequest() {return true;}
    public boolean requiresLogin() {return false;}

    public String jspFwdUrl()
    {
        return this.redirect_jsp;
    }
    public String redirectAction()
    {
        return this.REDIRECT_HOME_ACTION;
    }

    public void serveRequest(OscLoginSession loginSession)
        throws InvalidRequestException
    {
	    if (this.commit && !getIsError()) {
	        
            OscDbConnection   conn = null;
            PreparedStatement stmt = null;
            ResultSet 			rs = null;
            
            boolean isFound = false;
            try {
                conn = this.context.getDB(true);
                stmt = conn.prepareStatement(SQL_QUERY_MEMBERS);
                stmt.setString(1,this.pseudonym);
                stmt.setString(2,this.name);
                stmt.setString(3,this.member_email);

                rs = stmt.executeQuery();
                if (rs.next()) {
                    this.membership_id = rs.getLong("membership_id");
                    this.member_id = rs.getInt("member_id");
                    this.member_email = rs.getString("member_email");
                    isFound = true;
                } else {
                    this.error_code = "55";
                    this.failureMsg = "No member record found.";
                }
                rs.close();
            }catch(SQLException e){
                this.log.error("Failed to query ique_members in IQUE DB: " + e.getMessage());
                throw new InvalidRequestException("Failed to query ique_members in IQUE DB: " + e.getMessage());
            }finally{
                if (conn != null) {
                    conn.closeStatement(stmt);
                    conn.close();
                }
            }

            if (isFound) {
                try{
                    this.pin_plain = OscContext.genPassword(6);
                    this.pin_encry = OscContext.getEncryptedPasswd(this.pin_plain);
                    conn = this.context.getDB(false);
                    stmt = conn.prepareStatement(SQL_UPDATE_MEMBERS);
                    stmt.setString(1,this.pin_encry);
                    stmt.setLong(2,this.membership_id);
                    stmt.setInt(3,this.member_id);
                    
                    stmt.executeUpdate();
                    conn.closeStatement(stmt);
                    stmt = null;

                    OscSendMail mail = new OscSendMail(this.context);
                    String date = this.context.formatDate(this.context.getCurrentTime(),"yyyy-MM-dd HH:mm:ss","GMT+8:00");
                    String contents = this.context.readFile(this.context.getTxtPath(this.locale,"GetMailTemplete.txt"));
                    if (contents==null) {
                        this.failureMsg = "Mail template was not found.";
                        this.error_code = "33";
                    } else {
                        String subject = contents.substring(1,contents.indexOf("<&&>"));
                        contents = contents.substring(contents.indexOf("<&&>")+5);
                        
                        contents = contents.replaceAll("%NAME%",this.name);
                        contents = contents.replaceAll("%PASSWORD%",this.pin_plain);
                        contents = contents.replaceAll("%DATE%",date);
                        
                        mail.setSubject(subject);
                        mail.setTo(this.member_email);
                        mail.setBody(contents);

                        if(!mail.sendOut()){
                            this.failureMsg = "Mail sended unsuccessfully.";
                            this.error_code = "70";
                            try {
                                conn.rollback();
                            } catch (SQLException ex) {}
                        }
                    }
                    conn.commit();

                }
                catch (NoSuchAlgorithmException e) {
                    this.log.error("NoSuchAlgorithmException while encrypting password: " + e.getMessage());
                    throw new InvalidRequestException("NoSuchAlgorithmException while encrypting password: " + e.getMessage());
                }
                catch (SQLException e) {
                    try {
                        conn.rollback();
                    } catch (SQLException ex) {}
                    this.log.error("Failed to update ique_members in IQUE DB: " + e.getMessage());
                    throw new InvalidRequestException("Failed to update ique_members in IQUE DB: " + e.getMessage());
                }
                catch (IOException e) {
                    try {
                        conn.rollback();
                    } catch (SQLException ex) {}
                    this.log.error("Failed to read registration mail template: " + e.getMessage());
                    throw new InvalidRequestException("Failed to read registration mail template." + e.getMessage());
                }
                catch (Exception e) {
                    try {
                        conn.rollback();
                    } catch (SQLException ex) {}
                    this.log.error("Failed to send mail: " + e.getMessage());
                    throw new InvalidRequestException("Failed to send password mail:" + e.getMessage());
                }
                finally {
                    if (conn != null) {
                        conn.closeStatement(stmt);
                        conn.close();
                    }
                }
            }
            
            if (!getIsError()) {
                this.error_code = "102";
                this.failureMsg = "You have successfully got password.";
                this.redirect_jsp = GETPASSWD_SUCC_JSP;
	        } else {
	            this.redirect_jsp = GETPASSWD_ERR_JSP;
	        }
            
        } else if (this.commit && getIsError()) {
            this.redirect_jsp = GETPASSWD_ERR_JSP;
	    } else if (!this.commit) {
	        this.redirect_jsp = GETPASSWD_ERR_JSP;
	    }
    }

    public void encodeXml(PrintWriter out) {};

    public boolean getIsError() {return this.failureMsg != null;}
    public String getErrorMsg() {return this.failureMsg;}
    public String getErrorCode(){return this.error_code;}

} // class OscResultGetPasswd


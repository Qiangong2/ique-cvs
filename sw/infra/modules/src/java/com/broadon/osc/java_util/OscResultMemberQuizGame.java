package com.broadon.osc.java_util;

import java.io.PrintWriter;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Properties;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

import com.broadon.exception.InvalidRequestException;


public class OscResultMemberQuizGame extends OscResultBean {
    
    /** Rules for creating a Login result object
     */
    public static class Rule extends OscResultRule {

        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
        }
        public boolean supports(ServerType t)
        {
            return t == ServerType.HTM;
        }
        public OscResultBean create(HttpServletRequest req,
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
            return new OscResultMemberQuizGame(req, res, this);
        }
    } // class Rule    
    
     
    
    private static final String REDIRECT_HOME_ACTION	= "login_form";
    private static final String MEMBER_QUIZ_GAME_TEST_PAPERS_JSP	= 
        "MemberQuizGameTestPaper.jsp";
    private static final String MEMBER_QUIZ_GAME_QUESTIONS_JSP =
        "MemberQuizGameTestQuestion.jsp";
    private static final String MEMBER_QUIZ_GAME_SUBMIT_SUCC_JSP =
        "MemberQuizGameSubmitSucc.jsp";
        
    private final OscResultMemberQuizGame.Rule    		rule;
    private final OscContext                			context;
    private final OscLogger                 			logger;
    private final HttpServletRequest 					request;    
    private final String                        		locale;
    private final HttpSession							session;
    
    private static final String SQL_QUERY_TEST_PAPER_LIST =
        "SELECT paper_no,paper_title,paper_type,paper_property FROM ique_crm_sys_test_" +
        "paper WHERE paper_status='Y' AND paper_adscription='I' AND sysdate>=paper_from_" +
        "date AND sysdate<=paper_to_date";
    
    private static final String SQL_QUERY_DISPLAY_TIMES_RESTRICT = 
        "SELECT paper_scan_count FROM ique_crm_sys_test_paper WHERE paper_no=?";
    
    private static final String SQL_QUERY_ALREADY_DISPLAYED_TIMES = 
        "SELECT count(paper_no) FROM ique_crm_paper_scan_count WHERE paper_no=? " +
        "AND membership_id=? AND member_id=?";
    
    private static final String SQL_QUERY_TEST_PAPER_TYPE = 
        "SELECT paper_date_num times_or_qtyPerTime,trunc(paper_to_date)-trunc(paper_from" +
        "_date) days_qty,trunc(sysdate)-trunc(paper_from_date)+1 curr_day_seq,paper_ques" +
        "tion_num ques_qty,paper_property quiz_fashion,paper_title test_paper_title,paper" +
        "_memo test_paper_desc FROM ique_crm_sys_test_paper WHERE paper_no=?";
    
    private static final String SQL_QUERY_QUESTIONS_LIST_BY_RANGE = 
        "SELECT a.paper_skey,b.impresa_mul_type,b.impresa_question,b.impresa_right_point," +
        "b.impresa_error_point FROM ique_crm_sys_test_paper_d a,ique_crm_sys_impresa b " +
        "WHERE a.paper_no=? AND a.paper_skey>=? AND a.paper_skey<=? AND a.impresa_skey=" +
        "b.impresa_skey ORDER BY a.paper_skey";
    
    private static final String SQL_QUERY_QUESTION_BY_ID = 
        "SELECT a.paper_skey,b.impresa_mul_type,b.impresa_question,b.impresa_right_point," +
        "b.impresa_error_point FROM ique_crm_sys_test_paper_d a,ique_crm_sys_impresa b " +
        "WHERE a.paper_no=? AND a.paper_skey=? AND a.impresa_skey=b.impresa_skey";
    
    private static final String SQL_QUERY_ANSWER_LIST_COUNT = 
        "SELECT a.impresa_answer_num FROM ique_crm_sys_impresa a,ique_crm_sys_test_paper_d " +
        "b WHERE a.impresa_skey=b.impresa_skey AND b.paper_no=? AND b.paper_skey=?";
    
    private static final String SQL_QUERY_ANSWER_LIST = 
        "SELECT impresa_skey_id,impresa_answer,impresa_is_memo FROM ique_crm_sys_impresa a," +
        "ique_crm_sys_impresa_d b,ique_crm_sys_test_paper_d c WHERE a.impresa_skey=c.impresa" +
        "_skey AND a.impresa_skey=b.impresa_skey AND c.paper_no=? AND c.paper_skey=? ORDER " +
        "BY b.impresa_skey_id";
    
    private static final String SQL_UPDATE_DISPLAY_HISTORY =
        "INSERT INTO ique_crm_paper_scan_count(paper_no,membership_id,member_id) VALUES(?,?,?)";
    
    private static final String SQL_QUERY_IS_ANSWER_RIGHT =
        "SELECT CASE WHEN imp.impresa_right_answer=? THEN tpd.impresa_right_point ELSE " +
        "tpd.impresa_error_point END FROM ique_crm_sys_test_paper_d tpd,ique_crm_sys_" +
        "impresa imp WHERE tpd.paper_no=? AND tpd.paper_skey=? AND tpd.impresa_skey=" +
        "imp.impresa_skey";
    
    private static final String SQL_QUERY_USER_ANSWER_HISTORY =
        "SELECT MIN(tpq.paper_skey) min_question_id,MAX(tpq.paper_skey) max_question_id FROM " +
        "ique_crm_test_paper_quiz tpq, ique_crm_test_paper_answer tpa WHERE tpq.quiz_id =" +
        "tpa.quiz_id AND tpq.paper_no=? AND tpa.membership_id=? AND tpa.member_id=?";
    
    private static final String SQL_INSERT_TEST_PAPER_ANSWER =
        "INSERT INTO ique_crm_test_paper_answer(quiz_id,impresa_answer,impresa_memo," +
        "membership_id,member_id,answer_ip_address) VALUES(?,?,?,?,?,?)";
    
    private static final String SQL_CALL_BATCH_EARN =
        "{call IQUE_MEMBER_POINT_PKG.BATCH_EARN(?,?,?,?,?,?,?)}";
    
    private static final String SQL_QUERY_NEXT_QUIZ_ID =
        "SELECT quiz_id_seq.nextval from dual";
    
    private static final String SQL_INSERT_TEST_PAPER_QUIZ =
        "INSERT INTO ique_crm_test_paper_quiz(quiz_id,paper_no,paper_skey) VALUES(" +
        "?,?,?)";
    
    private static String RANDOM_FASHION 		= "A";
    private static String ORDER_FASHION 		= "O";
    private static int	  RECORDS_QTY_PER_PAGE 	= 2;
    
    public static String QUIZ_GAME_PHASE			= "quizGamePhase";
    public static String QUIZ_GAME_NEXT_PHASE		= "quizGameNextPhase";
    public static String TEST_PAPER_ID				= "testPaperId";
    public static String TEST_PAPER_TYPE			= "testPaperType";
    public static String TEST_PAPER_LIST			= "testPaperList";
    public static String QUESTION_ID_MAP			= "quesIdMap";
    public static String QUIZ_ID_MAP                = "quizIdMap";
    public static String TEST_PAPER_ID_MAP			= "testPaperIdMap";
    public static String QUESTION_LIST				= "quesList";
    public static String VIRTUAL_ID					= "vId";
    public static String REAL_ID					= "rId";
    public static String RANDOM_QUESTIONS_QTY 		= "randQuesQty";
    public static String DISPLAY_TIMES		  		= "dispTimes";
    public static String DAYS_QTY			  		= "daysQty";
    public static String GLOBE_QUESTIONS_QTY		= "globeQuesQty";
    public static String CURRENT_DAY_SEQ	  		= "currDaySeq";
    public static String QUIZ_FASHION		  		= "quizFashion";
    public static String CURRENT_PAGE_NO			= "currPageNo";
    public static String PAGES_QTY					= "pagesQty";
    public static String LOCAL_QUESTIONS_QTY		= "localQuesQty";
    public static String RECORDS_PER_PAGE			= "recordsPerPage";
    public static String TEST_PAPER_TITLE			= "testPaperTitle";
    public static String TEST_PAPER_DESC			= "testPaperDesc";
    
    
    
    private String  failureMsg;
    private String  error_code;
    private String  redirect_jsp;
    
    private int 		quiz_game_phase;
    private int 		pagesQty;
    private int 		localQuesQty;
    private String  	rTestPaperId;
    private String		vTestPaperId;
    private String[][]  userAnswer;
    
    
    private OscResultMemberQuizGame(HttpServletRequest  req,
            					 	HttpServletResponse res,
            					 	OscResultMemberQuizGame.Rule rule)
    	throws InvalidRequestException
    {
        this.rule 		= rule;
        this.context 	= rule.getContext();
        this.logger		= this.context.getLogger();
        this.request 	= req;
        this.locale	 	= this.context.getRequestLocale(this.request);
        this.session	= this.request.getSession(true);
        
        
        this.quiz_game_phase = 0;        
        
        if(this.request.getParameter(QUIZ_GAME_PHASE)!=null)
        {
           if(this.request.getParameter(QUIZ_GAME_PHASE).equals("1"))
           {
               this.quiz_game_phase = 1;
           }else if(this.request.getParameter(QUIZ_GAME_PHASE).equals("2")){
               this.quiz_game_phase = 2;
           }
        }
         
        switch(this.quiz_game_phase)
        {
            case 1:
                this.vTestPaperId	= this.request.getParameter(TEST_PAPER_ID);                
                this.rTestPaperId	= getRealTestPaperId(this.session,this.vTestPaperId);
                
                break;
                
            case 2:
                this.vTestPaperId	= this.request.getParameter(TEST_PAPER_ID);                
                this.rTestPaperId	= getRealTestPaperId(this.session,this.vTestPaperId);
                
      		  	break;
                
            default:                
            	this.redirect_jsp=MEMBER_QUIZ_GAME_TEST_PAPERS_JSP;
            	this.request.setAttribute(QUIZ_GAME_NEXT_PHASE,"1");
            
                break;                   
        }
    }

// --------------------------------------------
// Specialization of abstract methods
// --------------------------------------------

    public boolean requiresSecureRequest() {return false;}
    public boolean requiresLogin() {return true;}

    public String jspFwdUrl()
    {
        return this.redirect_jsp;
    }

    public String redirectAction()
    {
        return REDIRECT_HOME_ACTION;
    }

    public void encodeXml(PrintWriter out) {}    
 
    public void serveRequest(OscLoginSession loginSession)
    	throws InvalidRequestException
    {   
        if(this.quiz_game_phase == 1){            
            
            String[][] quesList=null;
            Properties testPaperInfo=queryTestPaperInfo(this.rTestPaperId);
                
            if(testPaperInfo.getProperty(QUIZ_FASHION,"").equals(ORDER_FASHION)){
                quesList=queryOrderQuesList(testPaperInfo,RECORDS_QTY_PER_PAGE);                    
            }else if(testPaperInfo.getProperty(QUIZ_FASHION,"").equals(RANDOM_FASHION)){
                if(!isDisplayAllowedToday(this.rTestPaperId,
                        			      loginSession.getMembershipID(),
                        			      loginSession.getMemberID())){
                    this.failureMsg="Display times exceeded the restrict";
                    this.error_code="65";
                }else{
                    quesList=queryRandQuesList(testPaperInfo,RECORDS_QTY_PER_PAGE);
                    updateDisplayHistory(this.rTestPaperId,
                            			 loginSession.getMembershipID(),
                             			 loginSession.getMemberID());
                }
            }
                
            if(this.failureMsg==null)
            {                    
                genQuesIdMapping(this.session,quesList,0);
                
                this.request.setAttribute(TEST_PAPER_ID,this.vTestPaperId);
                this.request.setAttribute(QUESTION_LIST,quesList);
                this.request.setAttribute(PAGES_QTY,Integer.toString(this.pagesQty));
                this.request.setAttribute(LOCAL_QUESTIONS_QTY,Integer.toString(this.localQuesQty));
                this.request.setAttribute(CURRENT_PAGE_NO,"0");
                this.request.setAttribute(RECORDS_PER_PAGE,Integer.toString(RECORDS_QTY_PER_PAGE));
                this.request.setAttribute(TEST_PAPER_TITLE,testPaperInfo.getProperty(TEST_PAPER_TITLE,""));
                this.request.setAttribute(TEST_PAPER_DESC,testPaperInfo.getProperty(TEST_PAPER_DESC,""));
                
                this.redirect_jsp=MEMBER_QUIZ_GAME_QUESTIONS_JSP;
                this.request.setAttribute(QUIZ_GAME_NEXT_PHASE,"2");
                
            }else{
                
                String[][] testPaperList=queryTestPaperList();                
                genIdMapping(this.session,TEST_PAPER_ID_MAP,testPaperList,0);
                
            	this.request.setAttribute(TEST_PAPER_LIST,testPaperList);            	
            	this.redirect_jsp=MEMBER_QUIZ_GAME_TEST_PAPERS_JSP;
            	this.request.setAttribute(QUIZ_GAME_NEXT_PHASE,"1");            	
            }
           
        }else if(this.quiz_game_phase == 2){
                
            Properties testPaperInfo=queryTestPaperInfo(this.rTestPaperId);
            
            if(testPaperInfo.getProperty(QUIZ_FASHION,"").equals(ORDER_FASHION))
            {   
                OscDbConnection    conn = null;
                PreparedStatement  stmt = null;
                ResultSet            rs = null;
                
                int minQuesId   = 0;
                int maxQuesId   = 0;  
            
                try{
                    conn = this.context.getDB(false);
                    stmt = conn.prepareStatement(SQL_QUERY_USER_ANSWER_HISTORY);
                    stmt.setString(1,testPaperInfo.getProperty(TEST_PAPER_ID,""));
                    stmt.setLong(2,loginSession.getMembershipID());
                    stmt.setLong(3,loginSession.getMemberID());
                        
                    rs=stmt.executeQuery();
                    if(rs.next()){
                        minQuesId=rs.getInt("min_question_id");
                        maxQuesId=rs.getInt("max_question_id");
                    }
                    
                    rs.close();
                    conn.closeStatement(stmt);
                    stmt=null;                    
                    
                }catch(SQLException e){
                    this.logger.error("Failed to query the history of user answering:" +
                            e.getMessage());
                    throw new InvalidRequestException("Failed to query the history of " +
                            "user answering:" + e.getMessage());
                }finally{
                    if (conn != null) {
                        conn.closeStatement(stmt);
                        conn.close();
                    }
                }
                
                int realQuesId=Integer.parseInt(getRealQuesId(this.session,this.request.getParameter("1_no")));
                boolean hasUserAnswered = (realQuesId<=maxQuesId) && (realQuesId>=minQuesId); 
                
                if(hasUserAnswered){
                    this.failureMsg="User has already answered these quesitons of the test paper.";
                    this.error_code="66";
                }
            }
                
            if(this.failureMsg==null)
            {              
                this.localQuesQty=Integer.parseInt(this.request.getParameter(LOCAL_QUESTIONS_QTY));
                
                int firstIndex	= 0;
                int secondIndex	= 3;
                
                for(int i=1;i<=this.localQuesQty;i++)
                {
                    if(this.request.getParameter(Integer.toString(i)+"_type").equals("M"))
                    {
                        int answerListCount=Integer.parseInt(this.request.getParameter(Integer.toString(i) + "_answer_list_count"));
                        int count=0;
                        
                        for(int j=1;j<=answerListCount;j++)
                        {
                            String answer=this.request.getParameter(Integer.toString(i) + "_" + Integer.toString(j));
                            if(answer!=null && !answer.equals(""))
                            {
                                firstIndex++;
                                count++;
                            }
                        }
                        
                        if(count==0)firstIndex++;
                        
                    }else{
                        firstIndex++;
                    }
                }
                
                this.userAnswer=new String[firstIndex][secondIndex];            	
                int o=0;
                
                for(int m=1;m<=this.localQuesQty;m++)
                {	
                    if(this.request.getParameter(Integer.toString(m)+"_type").equals("M"))
                    {
                        int u=Integer.parseInt(this.request.getParameter(Integer.toString(m)+"_answer_list_count"));
                        int count=0;
                        
                        for(int n=1;n<=u;n++)
                        {
                            if(this.request.getParameter(Integer.toString(m) + "_" + Integer.toString(n))!=null &&
                                    !this.request.getParameter(Integer.toString(m) + "_" + Integer.toString(n)).equals(""))
                            {
            			   		            			   		
                                this.userAnswer[m+o-1][0]=this.request.getParameter(Integer.toString(m) + "_no");
                                this.userAnswer[m+o-1][1]=this.request.getParameter(Integer.toString(m) + "_" + Integer.toString(n));
                                this.userAnswer[m+o-1][2]=this.request.getParameter(Integer.toString(m) + "_" + Integer.toString(n) + "_comments")==null ||
                                                          this.request.getParameter(Integer.toString(m) + "_" + Integer.toString(n) + "_comments").equals("") ?
                                                                  null:this.request.getParameter(Integer.toString(m) + "_" + Integer.toString(n) + "_comments");
                                o++;
                                count++;
                            }
                        }
                        
                        if(count==0)
                        {
                            this.userAnswer[m+o-1][0]=this.request.getParameter(Integer.toString(m) + "_no");
                            this.userAnswer[m+o-1][1]=null;
                            this.userAnswer[m+o-1][2]=null;
                        }else{
                            o--;
                        }
                        
                    }else{
            		    
                        this.userAnswer[m+o-1][0]=this.request.getParameter(Integer.toString(m) + "_no");
                        this.userAnswer[m+o-1][1]=this.request.getParameter(Integer.toString(m));
                        this.userAnswer[m+o-1][2]=this.request.getParameter(Integer.toString(m)+"_comments")==null ||
                                                  this.request.getParameter(Integer.toString(m)+"_comments").equals("") ?
                                                          null:this.request.getParameter(Integer.toString(m)+"_comments");
                    }
                }
            	
                String[][] mappingTable=deMapQuesId(this.session,this.userAnswer,0);

                long[][] resultArray=genResultArray(this.rTestPaperId,this.userAnswer);
                long[]quesId=new long[resultArray.length];
                long[]ruleId=new long[resultArray.length];
                
                for(int w=0;w<resultArray.length;w++)
                {
                    quesId[w]=resultArray[w][0];
                    ruleId[w]=resultArray[w][1];
                }                
                
                OscDbConnection    conn = null;
                PreparedStatement  stmt = null;
                CallableStatement cstmt = null;            
                
                try{
                    conn = this.context.getDB(false);
                    
                    stmt = conn.prepareStatement(SQL_INSERT_TEST_PAPER_QUIZ);                    
                    for(int e=0;e<this.userAnswer.length;e++)
                    {
                        stmt.setLong(1,Long.parseLong(mappingTable[e][0]));
                        stmt.setString(2,testPaperInfo.getProperty(TEST_PAPER_ID,""));
                        stmt.setInt(3,Integer.parseInt(mappingTable[e][1]));
                        
                        stmt.executeUpdate();
                    }                    
                    conn.closeStatement(stmt);
                    stmt = null;                    
                    
                    stmt = conn.prepareStatement(SQL_INSERT_TEST_PAPER_ANSWER);
                    for(int z=0;z<this.userAnswer.length;z++)
                    {                        
                        stmt.setLong(1,Long.parseLong(mappingTable[z][0]));
                        stmt.setString(2,this.userAnswer[z][1]);
                        stmt.setString(3,this.userAnswer[z][2]);
                        stmt.setLong(4,loginSession.getMembershipID());
                        stmt.setLong(5,loginSession.getMemberID());
                        stmt.setString(6,this.request.getRemoteAddr());
                        
                        stmt.executeUpdate();
                    }                    
                    conn.closeStatement(stmt);
                    stmt = null;
                    
                    cstmt = conn.getConnection().prepareCall(SQL_CALL_BATCH_EARN);                    
                    ArrayDescriptor descRuleId = ArrayDescriptor.createDescriptor("NUMBER_LIST", conn.getConnection());
                    ArrayDescriptor descQuesId = ArrayDescriptor.createDescriptor("NUMBER_LIST", conn.getConnection());

                    ARRAY ArrayRuleId = new ARRAY(descRuleId, conn.getConnection(), ruleId);
                    ARRAY ArrayQuesId = new ARRAY(descQuesId, conn.getConnection(), quesId);
                    
                    cstmt.setLong(1,loginSession.getMembershipID());
                    cstmt.setLong(2,loginSession.getMemberID());
                    cstmt.setArray(3,ArrayRuleId);
                    cstmt.setArray(4,ArrayQuesId);                    
                    cstmt.setString(5,testPaperInfo.getProperty(TEST_PAPER_ID,""));
                    cstmt.setString(6,testPaperInfo.getProperty(TEST_PAPER_TITLE));
                    cstmt.setString(7,this.request.getRemoteAddr());                    
                  
                    cstmt.execute();
                    conn.closeStatement(cstmt);
                    cstmt = null;
                    
                    conn.commit();
                
                }catch(SQLException e){
                    if(e.getMessage().indexOf("ORA-00001")!=-1)
                    {
                        this.logger.error("The user whose membership_id is '" +
                                loginSession.getMembershipID()+"',member_id is '" +
                                loginSession.getMemberID()+"' commited answer more than one time.");
                        this.failureMsg="User has already commited your answer more than one time.";
                        this.error_code="67";
                    }else{
                        this.logger.error("Failed to call the procedure 'IQUE_MEMBER_" +
                                "POINT_PKG.BATCH_EARN':" + e.getMessage());
                        throw new InvalidRequestException("Failed to call the procedure " +
                                "'IQUE_MEMBER_POINT_PKG.BATCH_EARN':" + e.getMessage());
                    }
                }finally{
                    if (conn != null) {
                        conn.closeStatement(cstmt);
                        conn.closeStatement(stmt);
                        conn.close();
                    }
                }
                
                this.redirect_jsp=MEMBER_QUIZ_GAME_SUBMIT_SUCC_JSP;               
            
            }else{
                String[][] testPaperList=queryTestPaperList();
                genIdMapping(this.session,TEST_PAPER_ID_MAP,testPaperList,0);
                    
                this.request.setAttribute(TEST_PAPER_LIST,testPaperList);
                this.redirect_jsp=MEMBER_QUIZ_GAME_TEST_PAPERS_JSP;
                this.request.setAttribute(QUIZ_GAME_NEXT_PHASE,"1");
            }
            
        }else{            
            String[][] testPaperList=queryTestPaperList();
            genIdMapping(this.session,TEST_PAPER_ID_MAP,testPaperList,0);
        	this.request.setAttribute(TEST_PAPER_LIST,testPaperList);        	
        }        
    }
    
    private String[][] queryTestPaperList()
    	throws InvalidRequestException
    {
        OscDbConnection   conn = null;
        PreparedStatement stmt = null;
        ResultSet           rs = null;
        
        String[][] testPaperList = null;
        
        int firstIndex		   = 0;
        int secondIndex		   = 4;
        
        try{
            conn = this.context.getDB(true);
            firstIndex = conn.GetRecordCount(SQL_QUERY_TEST_PAPER_LIST);
            
            testPaperList=new String[firstIndex][secondIndex];
            stmt = conn.prepareStatement(SQL_QUERY_TEST_PAPER_LIST);            
            rs=stmt.executeQuery();
            
            int i=0;
            while(rs.next()){
                for(int j=0;j<secondIndex-1;j++){
                    testPaperList[i][j]=rs.getString(j+1);
                }
                i++;
            }
            
            rs.close();
            conn.closeStatement(stmt);
            stmt=null; 
        
    	}catch(SQLException e){
    	    this.logger.error("Failed to query test paper list in " +
    	            "IQUE DB: " + e.getMessage());
    	    throw new InvalidRequestException("Failed to query " +
    	            "test paper list in IQUE DB: " + e.getMessage());
    	}finally{
    	    if (conn != null) {
    	        conn.closeStatement(stmt);
    	        conn.close();
    	    }
    	}
    	return testPaperList;        
    }
    
    private boolean isDisplayAllowedToday(String testPaperId,
            							  long membership_id,
            							  long member_id)
    	throws InvalidRequestException
    {
        OscDbConnection   conn = null;
        PreparedStatement stmt = null;
        ResultSet           rs = null;
        
        int displayTimesRestrict	= 0;
        long timesDisplayed			= 0;
        
        try{
            conn = this.context.getDB(true);
            stmt = conn.prepareStatement(SQL_QUERY_DISPLAY_TIMES_RESTRICT);
            stmt.setString(1,testPaperId);
            
            rs=stmt.executeQuery();
            if(rs.next()){
                displayTimesRestrict=rs.getInt(1);
            }else{
                this.logger.error("The test page id '" + testPaperId +
                        "' is not Found!");
                throw new InvalidRequestException("The test page id '" + testPaperId +
                        "' is not Found!");
            }            
            
            rs.close();
            conn.closeStatement(stmt);
            stmt=null; 
            
            stmt = conn.prepareStatement(SQL_QUERY_ALREADY_DISPLAYED_TIMES);
            stmt.setString(1,testPaperId);
            stmt.setLong(2,membership_id);
            stmt.setLong(3,member_id);
            
            rs=stmt.executeQuery();          
            if(rs.next()){
                timesDisplayed=rs.getLong(1);                
            }
            
            rs.close();
            conn.closeStatement(stmt);
            stmt=null;
            
    	}catch(SQLException e){
    	    this.logger.error("Failed to query the restrict of display times in " +
    	            "IQUE DB: " + e.getMessage());
    	    throw new InvalidRequestException("Failed to query " +
    	            "the restrict of display times in IQUE DB: " + e.getMessage());
    	}finally{
    	    if (conn != null) {
    	        conn.closeStatement(stmt);
    	        conn.close();
    	    }
    	}
    	return timesDisplayed>=displayTimesRestrict?false:true;
    }
    
    private Properties queryTestPaperInfo(String testPaperId)
    	throws InvalidRequestException
    {
        OscDbConnection   conn = null;
        PreparedStatement stmt = null;
        ResultSet           rs = null;
        
        String quizFashion		= null;        
        String randQuesQty		= null;
        String dispTimes		= null;
        String daysQty			= null;
        String globeQuesQty		= null;
        String currDaySeq		= null;
        String testPaperTitle	= null;
        String testPaperDesc	= null;
        
        try{
            conn = this.context.getDB(true);
            stmt = conn.prepareStatement(SQL_QUERY_TEST_PAPER_TYPE);
            stmt.setString(1,testPaperId);            
            rs=stmt.executeQuery();
            if(rs.next()){                
                randQuesQty    = rs.getString("times_or_qtyPerTime");
                dispTimes	   = rs.getString("times_or_qtyPerTime");
                daysQty		   = rs.getString("days_qty");
                currDaySeq	   = rs.getString("curr_day_seq");
                globeQuesQty   = rs.getString("ques_qty");
                quizFashion    = rs.getString("quiz_fashion");
                testPaperTitle = rs.getString("test_paper_title");
                testPaperDesc  = rs.getString("test_paper_desc");
            }
            
            rs.close();
            conn.closeStatement(stmt);
            stmt=null;
            
    	}catch(SQLException e){
    	    this.logger.error("Failed to query question list in " +
    	            "IQUE DB: " + e.getMessage());
    	    throw new InvalidRequestException("Failed to query " +
    	            "question list in IQUE DB: " + e.getMessage());
    	}finally{
    	    if (conn != null) {
    	        conn.closeStatement(stmt);
    	        conn.close();
    	    }
    	}
        
    	Properties prop=new Properties();
        
        prop.setProperty(TEST_PAPER_ID,testPaperId);
    	prop.setProperty(RANDOM_QUESTIONS_QTY,randQuesQty);
    	prop.setProperty(DISPLAY_TIMES,dispTimes);
    	prop.setProperty(DAYS_QTY,daysQty);
    	prop.setProperty(GLOBE_QUESTIONS_QTY,globeQuesQty);
    	prop.setProperty(CURRENT_DAY_SEQ,currDaySeq);
    	prop.setProperty(QUIZ_FASHION,quizFashion);
    	prop.setProperty(TEST_PAPER_TITLE,testPaperTitle);
    	prop.setProperty(TEST_PAPER_DESC,testPaperDesc);
    	
    	return prop;
    }
       
    private String[][] queryOrderQuesList(Properties testPaperInfo,
            							  int recordsPerPage)
    	throws InvalidRequestException
    {
        String[][] resultArray = null;
        int currDaySeq=Integer.parseInt(testPaperInfo.getProperty(CURRENT_DAY_SEQ,"0"));
        int daysQty=Integer.parseInt(testPaperInfo.getProperty(DAYS_QTY,"0"));
        int dispTimes=Integer.parseInt(testPaperInfo.getProperty(DISPLAY_TIMES,"0"));
        int globeQuesQty=Integer.parseInt(testPaperInfo.getProperty(GLOBE_QUESTIONS_QTY,"0"));
        
        int dispSeq=currDaySeq%(daysQty/dispTimes)==0?
      	         	currDaySeq/(daysQty/dispTimes):
      	            currDaySeq/(daysQty/dispTimes)+1;
      	int quesQtyPerTime=globeQuesQty/dispTimes;
      	int startRecordNum=quesQtyPerTime*(dispSeq-1)+1;
      	int endRecordNum=quesQtyPerTime*dispSeq;
      	
     	this.localQuesQty=quesQtyPerTime;      	
      	this.pagesQty=this.localQuesQty%recordsPerPage==0 ?
      	        	  this.localQuesQty/recordsPerPage:this.localQuesQty/recordsPerPage+1;
      	
      	int firstIndex		= this.localQuesQty;
      	int secondIndex		= 5;

      	resultArray=new String[firstIndex][secondIndex];
      	    
      	OscDbConnection   conn = null;
        PreparedStatement stmt = null;
        ResultSet           rs = null;
        
      	try{
            conn = this.context.getDB(true);
       	    stmt = conn.prepareStatement(SQL_QUERY_QUESTIONS_LIST_BY_RANGE);
       	    stmt.setString(1,testPaperInfo.getProperty(TEST_PAPER_ID,""));
       	    stmt.setInt(2,startRecordNum);
       	    stmt.setInt(3,endRecordNum);

       	    rs=stmt.executeQuery();
       	    int i=0;
       	    while(rs.next()){
       	        for(int j=0;j<secondIndex;j++){
       	         resultArray[i][j]=rs.getString(j+1);
       	        }
       	        i++;
       	    }
       	    
       	    rs.close();
       	    conn.closeStatement(stmt);
       	    stmt=null;
            
    	}catch(SQLException e){
    	    this.logger.error("Failed to query question list in " +
    	            "IQUE DB: " + e.getMessage());
    	    throw new InvalidRequestException("Failed to query " +
    	            "question list in IQUE DB: " + e.getMessage());
    	}finally{
    	    if (conn != null) {
    	        conn.closeStatement(stmt);
    	        conn.close();
    	    }
    	}
    	return resultArray;
    }
    
    private String[][] queryRandQuesList(Properties testPaperInfo,
            							 int recordsPerPage)
    	throws InvalidRequestException
    {
        String[][] resultArray = null;
        
        int randQuesQty=Integer.parseInt(testPaperInfo.getProperty(RANDOM_QUESTIONS_QTY,"0"));
        int globeQuesQty=Integer.parseInt(testPaperInfo.getProperty(GLOBE_QUESTIONS_QTY,"0"));
        
        this.localQuesQty=randQuesQty;      	
      	this.pagesQty=this.localQuesQty%recordsPerPage==0 ?
      	        	  this.localQuesQty/recordsPerPage:this.localQuesQty/recordsPerPage+1;
      	
        int firstIndex	= randQuesQty;
        int secondIndex	= 5;
      	
        resultArray=new String[firstIndex][secondIndex];
        int[]randRecNum=genRandIntArray(randQuesQty,globeQuesQty,1);
   
        OscDbConnection   conn = null;
        PreparedStatement stmt = null;
        ResultSet           rs = null;
        
        try{
            conn = this.context.getDB(true);
            stmt = conn.prepareStatement(SQL_QUERY_QUESTION_BY_ID);
       	    
            for(int i=0;i<randQuesQty;i++){
                stmt.setString(1,testPaperInfo.getProperty(TEST_PAPER_ID,""));
                stmt.setInt(2,randRecNum[i]);
           	    rs=stmt.executeQuery();
           	    if(rs.next()){
           	        for(int j=0;j<secondIndex;j++){
           	            resultArray[i][j]=rs.getString(j+1);
           	        }               	        
           	    }
           	    rs.close();
            }
            
       	    rs.close();
       	    conn.closeStatement(stmt);
       	    stmt=null;
            
    	}catch(SQLException e){
    	    this.logger.error("Failed to query random question list in " +
    	            "IQUE DB: " + e.getMessage());
    	    throw new InvalidRequestException("Failed to query " +
    	            "random question list in IQUE DB: " + e.getMessage());
    	}finally{
    	    if (conn != null) {
    	        conn.closeStatement(stmt);
    	        conn.close();
    	    }
    	}
    	return resultArray;
    }
    
    public String[][] queryAnswersList(String vTestPaperId,String vQuesId,HttpSession session)
    	throws InvalidRequestException
    {
        String rTestPaperId=getRealTestPaperId(session,vTestPaperId);
        if(rTestPaperId==null)return null;
        
        String rQuesId=getRealQuesId(session,vQuesId);
        if(rQuesId==null)return null;
        
        String[][] resultArray=null;
        String[][] tempArray=null;
        
        int firstIndex	= 0;
        int secondIndex = 3;
        
        OscDbConnection   conn = null;
        PreparedStatement stmt = null;
        ResultSet           rs = null;
        
        try{
            conn = this.context.getDB(true);
            stmt = conn.prepareStatement(SQL_QUERY_ANSWER_LIST_COUNT);
            stmt.setString(1,rTestPaperId);
            stmt.setLong(2,Long.parseLong(rQuesId));
            
            rs=stmt.executeQuery();
            if(rs.next()){
                firstIndex=rs.getInt(1);
            }          
            
            rs.close();
            conn.closeStatement(stmt);
            stmt=null;
            
            if(firstIndex==0)return null;
            
            resultArray=new String[firstIndex][secondIndex];
            tempArray=new String[firstIndex][secondIndex];
            
            stmt = conn.prepareStatement(SQL_QUERY_ANSWER_LIST);
            stmt.setString(1,rTestPaperId);
            stmt.setLong(2,Long.parseLong(rQuesId));
            
            rs=stmt.executeQuery();
            int i=0;
            while(rs.next()){
                for(int j=0;j<secondIndex;j++){
                    tempArray[i][j]=rs.getString(j+1);
                }
                i++;
            }
            
            rs.close();
            conn.closeStatement(stmt);
            stmt=null;
            
    	}catch(SQLException e){
    	    this.logger.error("Failed to query answer list in " +
    	            "IQUE DB: " + e.getMessage());
    	    throw new InvalidRequestException("Failed to query " +
    	    		"answer list in IQUE DB: " + e.getMessage());
    	}finally{
    	    if (conn != null) {
    	        conn.closeStatement(stmt);
    	        conn.close();
    	    }
    	}
    	
    	int[] randIndex=genRandIntArray(firstIndex,firstIndex-1,0);
    	for(int i=0;i<firstIndex;i++){
    	    for(int j=0;j<secondIndex;j++){
    	        resultArray[i][j]=tempArray[randIndex[i]][j];
    	    }   	    
    	}
        
        return resultArray;
    }
    
    private String getRealTestPaperId(HttpSession session,String vTestPaperId)
    	throws InvalidRequestException
    {
        return getRealId(session,TEST_PAPER_ID_MAP,vTestPaperId);
    }
    
    private String getRealQuesId(HttpSession session,String vQuesId)
		throws InvalidRequestException
	{
        return getRealId(session,QUIZ_ID_MAP,getRealId(session,QUESTION_ID_MAP,vQuesId));
	}
    
    private String getRealId(HttpSession session,String mapName,String vId)
        throws InvalidRequestException
    {
        if(vId==null)
        {
            this.logger.error("When querying mapped id in " + 
                    mapName+",occured that vId is null.");
            throw new InvalidRequestException("When querying " +
                    "mapped id in " + mapName+",occured that vId is null.");
        }
        
        if(session==null){
            this.logger.error("When querying mapped id in " + 
                    mapName+",occured that session is null.");
            throw new InvalidRequestException("When querying " +
                    "mapped id in " + mapName+",occured that session is null.");
        }
        
        Properties prop=null;
        if(session.getAttribute(mapName)!=null){
            prop=(Properties)session.getAttribute(mapName);
        }else{
            this.logger.error("When querying mapped id in " + mapName+",occured " +
                    "that there isn't '"+mapName+"' attribute in session");
            throw new InvalidRequestException("When querying mapped id in " + mapName +
                    ",occured that there isn't '"+mapName+"' attribute in session");
        }        
        return prop.getProperty(vId);
    }
    
    private void deMapArray(HttpSession    session,
                            String         mapName,
                            String[][]     mappedArray,
                            int            index)
        throws InvalidRequestException
    {
        if(mappedArray==null)
        {
            this.logger.error("When querying mapped id in " + mapName +
                    ",occured that " + mappedArray + "is null.");
            throw new InvalidRequestException("When querying mapped id " +
                    "in " + mapName+",occured that " + mappedArray + "is null.");
        }
    
        if(session==null){
            this.logger.error("When querying mapped id in " + mapName +
                    ",occured that session is null.");
            throw new InvalidRequestException("When querying mapped id " +
                    "in " + mapName+",occured that session is null.");
        }
    
        Properties prop=null;
        if(session.getAttribute(mapName)!=null){
            prop=(Properties)session.getAttribute(mapName);
        }else{
            this.logger.error("When querying mapped id in " + mapName+",occured " +
                    "that there isn't '" + mapName + "' attribute in session");
            throw new InvalidRequestException("When querying mapped id in " + mapName +
                    ",occured that there isn't '" + mapName + "' attribute in session");
        }
        
        for(int i=0;i<mappedArray.length;i++){
            if(mappedArray[i][index]==null || mappedArray[i][index].equals("")){
                this.logger.error("When querying mapped id in " + mapName+",occured " +
                        "that there is null value in the array '" + mappedArray + "'.");
                throw new InvalidRequestException("When querying mapped id in " + 
                        mapName+",occured that there is null value in the array '" + 
                        mappedArray + "'.");
            }
            mappedArray[i][index]=prop.getProperty(mappedArray[i][index]);
        }
    }
        
    private void genIdMapping(HttpSession    session, 
                              String         mapName,
                              String[][]     orgArray,
                              int            index)
    {
        Properties idMapping=null;
        if(session.getAttribute(mapName)!=null){
            idMapping=(Properties)session.getAttribute(mapName);
            idMapping.clear();
        }else{
            idMapping=new Properties();
            session.setAttribute(mapName,idMapping);
        }
        
        for(int i=0;i<orgArray.length;i++){
            idMapping.put(Integer.toString(i+1),orgArray[i][index]);
            orgArray[i][index]=Integer.toString(i+1);
        }        
    }
    
    private void genIdMapping(HttpSession    session, 
                              String         mapName,
                              String[][]     orgArray,
                              int            index,
                              long[]         mappedId)
        throws InvalidRequestException
    {
        Properties idMapping=null;
        if(session.getAttribute(mapName)!=null){
            idMapping=(Properties)session.getAttribute(mapName);
            idMapping.clear();
        }else{
            idMapping=new Properties();
            session.setAttribute(mapName,idMapping);
        }
        if(mappedId==null || mappedId.length!=orgArray.length){
            this.logger.error("mappedId[] is null or it's length doesn't " +
                    "equal array[]'s length" );
            throw new InvalidRequestException("mappedId[] is null or it's " +
                    "length doesn't equal array[]'s length" );
        }
            
        for(int i=0;i<orgArray.length;i++){
            idMapping.put(Long.toString(mappedId[i]),orgArray[i][index]);
            orgArray[i][index]=Long.toString(mappedId[i]);
        }        
    }
    
    private void genQuesIdMapping(HttpSession session,
                                  String[][]  orgArray,
                                  int         index)
        throws InvalidRequestException
    {
        genIdMapping(session, 
                     QUIZ_ID_MAP,
                     orgArray,
                     index,
                     queryNextQuizId(orgArray.length));
        
        genIdMapping(session,QUESTION_ID_MAP,orgArray,index);        
    }
    
    private long[] queryNextQuizId(int count)
        throws InvalidRequestException
    {
        OscDbConnection   conn = null;
        PreparedStatement stmt = null;
        ResultSet           rs = null;
        long[] resultArray     = new long[count];
        try{
            conn = this.context.getDB(true);
            stmt = conn.prepareStatement(SQL_QUERY_NEXT_QUIZ_ID);
            for(int i=0;i<count;i++)
            {
                rs=stmt.executeQuery();            
                if(rs.next()){
                    resultArray[i]=rs.getLong(1);
                }
                rs.close();
            }
            conn.closeStatement(stmt);
            stmt=null;
            
        }catch(SQLException e){
            this.logger.error("Failed to query next quiz id in " +
                    "IQUE DB: " + e.getMessage());
            throw new InvalidRequestException("Failed to query " +
                    "quiz id in IQUE DB: " + e.getMessage());
        }finally{
            if (conn != null) {
                conn.closeStatement(stmt);
                conn.close();
            }
        }
        return resultArray;
    }
    
    private String[][] deMapQuesId(HttpSession  session,
                                   String[][]   mappedArray,
                                   int          index)
        throws InvalidRequestException
    {
        if(mappedArray==null)return null;
        if(session==null)return null;       
        
        String[][] resultArray=new String[mappedArray.length][2];
        deMapArray(session,QUESTION_ID_MAP,mappedArray,index);
        for(int i=0;i<resultArray.length;i++){
            resultArray[i][0]=mappedArray[i][index];
        }     
        
        deMapArray(session,QUIZ_ID_MAP,mappedArray,index);
        for(int i=0;i<resultArray.length;i++){
            resultArray[i][1]=mappedArray[i][index];
        }
        
        return resultArray;
    }
    
    private void updateDisplayHistory(String testPaperId,long membership_id,long member_id)
    	throws InvalidRequestException
    {
        OscDbConnection   conn = null;
        PreparedStatement stmt = null;
        
        try{
            conn = this.context.getDB(false);
            stmt = conn.prepareStatement(SQL_UPDATE_DISPLAY_HISTORY);
            stmt.setString(1,testPaperId);
            stmt.setLong(2,membership_id);
            stmt.setLong(3,member_id);
            
            stmt.executeUpdate(); 
            conn.closeStatement(stmt);
            stmt=null;
            
            conn.commit();
            
    	}catch(SQLException e){
    	    this.logger.error("Failed to update display history in " +
    	            "IQUE DB: " + e.getMessage());
    	    throw new InvalidRequestException("Failed to update " +
    	    		"display history in IQUE DB: " + e.getMessage());
    	}finally{
    	    if (conn != null) {
    	        conn.closeStatement(stmt);
    	        conn.close();
    	    }
    	}
    }
    
    private String[][] combOrderAnswer(String[][] orgUserAnswer)
    {        
        if(orgUserAnswer==null)return null;
        
        StringBuffer strId=new StringBuffer();
        StringBuffer strAnswer=new StringBuffer();
        String quesId=null;

        int firstIndex	= 0;
        int secondIndex	= 2;
        
        for(int i=0;i<orgUserAnswer.length;i++){
            if(quesId==null||!quesId.equals(orgUserAnswer[i][0])){
                if(strAnswer.length()==0){
                    strAnswer.append(orgUserAnswer[i][1]);
                    strId.append(orgUserAnswer[i][0]);
                }else{
                    strAnswer.append("!" + orgUserAnswer[i][1]);
                    strId.append("!" + orgUserAnswer[i][0]);
                }
                firstIndex++;
            }else{
                strAnswer.append(","+orgUserAnswer[i][1]);
            }
            quesId=orgUserAnswer[i][0];
        }
        
        String[] arrayId=strId.toString().split("!");
        String[] arrayAnswer=strAnswer.toString().split("!");
        
        for(int i=0;i<firstIndex;i++){
            arrayAnswer[i]=sortString(arrayAnswer[i]);
        }
        
        String[][] resultArray=new String[firstIndex][secondIndex];
        for(int i=0;i<firstIndex;i++){
            resultArray[i][0]=arrayId[i];
            resultArray[i][1]=arrayAnswer[i];
        }        
        return resultArray;
    }
    
    private long [][] genResultArray(String testPaperId,String[][] orgUserAnswer)
		throws InvalidRequestException
	{
        String[][] combedUserAnswer=combOrderAnswer(orgUserAnswer);
        if(combedUserAnswer==null)return null;
    
        OscDbConnection   conn = null;
        PreparedStatement stmt = null;
        ResultSet           rs = null;
    
        int firstIndex	= combedUserAnswer.length;
        int secondIndex	= 2;
    
        long[][] resultArray = new long[firstIndex][secondIndex];
    
        try{
            conn = this.context.getDB(true);
            stmt = conn.prepareStatement(SQL_QUERY_IS_ANSWER_RIGHT);
        
            for(int i=0;i<firstIndex;i++)
            {
                stmt.setString(1,combedUserAnswer[i][1]);
                stmt.setString(2,testPaperId);
                stmt.setLong(3,Long.parseLong(combedUserAnswer[i][0]));
                rs=stmt.executeQuery();
                if(rs.next()){
                    resultArray[i][0]=Long.parseLong(combedUserAnswer[i][0]);
                    resultArray[i][1]=Long.parseLong(rs.getString(1));
                }
                rs.close();
        	}
        
            conn.closeStatement(stmt);
            stmt=null;
        
        }catch(SQLException e){
            this.logger.error("Failed to query right answer in " +
                    "IQUE DB: " + e.getMessage());
            throw new InvalidRequestException("Failed to query " +
                    "right answer in IQUE DB: " + e.getMessage());
        }finally{
            if (conn != null) {
                conn.closeStatement(stmt);
                conn.close();
            }
        }
        return resultArray;
    
	}
    
    private int[] genRandIntArray(int arrayLength,int maxInt,int minInt)
    {
        int[]resRandArray=new int[arrayLength];
        Random rand=new Random();
        for(int i=0;i<arrayLength;i++){
            resRandArray[i]=rand.nextInt(maxInt+1);
            while(resRandArray[i]<minInt){
                resRandArray[i]=rand.nextInt(maxInt+1);
            }
            boolean hasRepitition=false;
            boolean enter=true;
            while(enter || hasRepitition){
                enter			= false;
                hasRepitition	= false;
                for(int j=0;j<i;j++){
                    if(resRandArray[i]==resRandArray[j]){
                        resRandArray[i]=rand.nextInt(maxInt+1);
                        while(resRandArray[i]<minInt){
                            resRandArray[i]=rand.nextInt(maxInt+1);
                        }
                        hasRepitition=true;
                        break;
                    }                           
                }
            }                    
        }
        return resRandArray;        
    }
    
    private String sortString(String str)
    {
        if(str==null || str.indexOf(",")==-1)return str;  
        String[] array=str.split(",");
        Arrays.sort(array);
        StringBuffer strBuff=new StringBuffer();
        for(int i=0;i<array.length;i++){
            if(strBuff.length()==0){
                strBuff.append(array[i]);
            }else{
                strBuff.append(","+array[i]);
            }
        }
        return strBuff.toString();
    }
    
    public String getGiftImageFileName(String vTestPaperId,String location,HttpSession session)
    	throws InvalidRequestException
    {
        String rTestPaperId	= getRealTestPaperId(session,vTestPaperId);
        return "http://www.ique.com/test_paper_img/tp_img_" + rTestPaperId + "_" +location+".gif";        
    }
            
    public boolean getIsError() {return this.failureMsg != null;}
    public String getErrorMsg() {return this.failureMsg;}
    public String getErrorCode(){return this.error_code;}  

}

package com.broadon.osc.java_util;

import java.io.*;
import javax.servlet.http.*;

import com.broadon.status.StatusCode;
import com.broadon.exception.InvalidRequestException;

/** The class used to serve VNG server request for game session match making
 *  and returns the appropriate document and header back to the VNG server.
 */

public class OscResultGameSessionMatch extends OscResultBean
{
    /** Rules for creating a Game Session Match result object
     */
    public static class Rule extends OscResultRule {
        
        public static final String GAME_SESSION_MATCH = "gsess_match";

        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
        }
        public boolean supports(ServerType t)
        {
            return t == ServerType.RPC;
        }
        public OscResultBean create(HttpServletRequest req,  
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
            return new OscResultGameSessionMatch(req, res, this);
        }
    } // class Rule


    // --------------------------------------------
    // Invariant values
    // --------------------------------------------

    public static final String TOTAL_COUNT_TAG = "total_count";

    // --------------------------------------------
    // Member variables
    // --------------------------------------------

    private final OscResultGameSessionMatch.Rule 	rule;
    private final OscContext          			context;
    private final OscLogger          			logger;
    private final String                        	actionName;
    private String					status;
    private String					statusMsg;

    private String[]					column_name;
    private String[]					column_value;
    private String[]					operator;
    private String					gsess_sort;
    private String					resultXML;
    private int						totalCount;

    private String					num_rows;
    private String					skip_rows;

    // ----------------------------------------------------------
    // Hidden constructors and helper methods (all constructors should
    // be hidden for result objects, since they are constructed by the
    // Rule class)
    // ---------------------------------------------------------

    private OscResultGameSessionMatch(HttpServletRequest  req,  
                                      HttpServletResponse res,
                                      OscResultGameSessionMatch.Rule rule)
        throws InvalidRequestException
    {
        this.rule = rule;
        context = rule.getContext();
        logger = context.getLogger();
        actionName = rule.getActionNm();

	status = OscStatusCode.SC_OK;
	statusMsg = StatusCode.getMessage(status);

	column_name = req.getParameterValues(ORP.NAME);
	column_value = req.getParameterValues(ORP.VALUE);
	operator = req.getParameterValues(ORP.OPERATOR);
	gsess_sort = req.getParameter(ORP.GSESS_SORT);
	num_rows = req.getParameter(ORP.GSESS_NUM_ROWS);
	skip_rows = req.getParameter(ORP.GSESS_SKIP_ROWS);
        totalCount = 0;
        resultXML = "";

        if (num_rows == null || num_rows.equals(""))
            num_rows = "-1";
        if (skip_rows == null || skip_rows.equals(""))
            skip_rows = "-1";

        if ((column_name == null && (column_value != null || operator != null)) ||
            (column_value == null && (column_name != null || operator != null)) ||
            (operator == null && (column_name != null || column_value != null)) ||
            (column_name != null && column_value != null && operator != null &&
             (column_name.length != column_value.length ||
              column_name.length != operator.length ||
              column_value.length != operator.length)))
        {
            status = OscStatusCode.OSC_BAD_REQUEST;
            statusMsg = OscStatusCode.getMessage(OscStatusCode.OSC_BAD_REQUEST);
            logger.error(statusMsg + " for " + actionName +
                    " -- Incorrect name/value/operator set of values");
            return;
        }

    }

    // --------------------------------------------
    // Specialization of abstract methods
    // --------------------------------------------

    public boolean requiresLogin() {return true;}

    public String jspFwdUrl() {return null;}

    public void encodeXml(PrintWriter out)
    {
        // Encodes the final results
        //
        out.println("<" + ORP.RPC_RESULT_TAG + ">");

        context.encodeXml_TNL(out, ORP.REQ_ACTION, actionName);
        context.encodeXml_TNL(out, STATUS_TAG, status);
        context.encodeXml_TNL(out, STATUS_MSG_TAG, statusMsg);

        context.encodeXml_TNL(out, TOTAL_COUNT_TAG, String.valueOf(totalCount));

        if (!getIsError() && resultXML!=null && !resultXML.equals(""))
            out.println(resultXML);

        if (context.getRpcAppendParams().equals("ON"))
        {
            out.println("\t<" + ORP.RPC_PARAMS_TAG + ">");
            if (column_name != null && column_value != null && operator != null)
            { 
                int count = column_name.length;

                if (count < column_value.length)
                    count = column_value.length;
                else if (count < operator.length)
                    count = operator.length;

                for (int i = 0; i < count; i++)
                {
                    if (i < column_name.length)
                        context.encodeXml_TTNL(out, ORP.NAME, column_name[i]);
                    else
                        context.encodeXml_TTNL(out, ORP.NAME, "NULL");
                    if (i < operator.length)
                        context.encodeXml_TTNL(out, ORP.OPERATOR, operator[i]);
                    else
                        context.encodeXml_TTNL(out, ORP.OPERATOR, "NULL");
                    if (i < column_value.length)
                        context.encodeXml_TTNL(out, ORP.VALUE, column_value[i]);
                    else
                        context.encodeXml_TTNL(out, ORP.VALUE, "NULL");
                }
            }

            context.encodeXml_TTNL(out, ORP.GSESS_SORT, gsess_sort);
            context.encodeXml_TTNL(out, ORP.GSESS_NUM_ROWS, num_rows);
            context.encodeXml_TTNL(out, ORP.GSESS_SKIP_ROWS, skip_rows);
            out.println("\t</" + ORP.RPC_PARAMS_TAG + ">");
        }

        out.println("</" + ORP.RPC_RESULT_TAG + ">");
    } // encodeXml

    public void serveRequest(OscLoginSession loginSession)
        throws InvalidRequestException
    {
        if (!getIsError()) {
            OscDbConnection conn = null;

            try {
                conn = context.getDB(true);

                String sql = OscDmlGameSession.getGameSessionMatchSQL(gsess_sort, column_name, column_value, operator);
                int numrows = Integer.parseInt(num_rows);
                int skiprows = Integer.parseInt(skip_rows);
                totalCount = OscDmlGameSession.countGameSession(conn, sql);
                resultXML = OscDmlGameSession.queryGameSession(conn, sql, numrows, skiprows);
            }
            catch (Exception e) {
                status = OscStatusCode.VNG_DB_ERROR;
                statusMsg = OscStatusCode.getMessage(OscStatusCode.VNG_DB_ERROR) +
                       ": " + e.getMessage();

                e.printStackTrace();
                logger.error(statusMsg + " for " + actionName);
            }
            finally {
                if (conn != null)
                    conn.close();
            }
        }
    }

    public String toString() {return null;}

    public boolean getIsError()
    {
        return !status.equals(OscStatusCode.SC_OK);
    }

    public String getErrorMsg()
    {
        return statusMsg;
    }

} // class OscResultGameSessionMatch

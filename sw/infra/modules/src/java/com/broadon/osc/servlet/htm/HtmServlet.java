/*
 * (C) 2004, BroadOn Communications Corp.,
 * $Id: HtmServlet.java,v 1.32 2005/03/22 03:04:11 ho Exp $
 */
package com.broadon.osc.servlet.htm;

import java.io.IOException;

import javax.servlet.*;
import javax.servlet.http.*;

import org.apache.log4j.Logger;

import com.broadon.exception.InvalidRequestException;
import com.broadon.osc.java_util.*;
import com.broadon.util.TimeTrace;


/**
 * The <code>HtmServlet</code> is a servlet that accepts POST/GET 
 * requests, accepts parameters to the request either as part of the
 * URI or as an XML document embedded in the request, and returns an
 * HTML result.  In addition to any values passed in as parameters, 
 * this servlet also expects cookies holding information about the
 * current logon session.
 *
 *<p> This servlet interacts with the XS server and the DB, both of
 * which are provided as part of the configuration of the servlet when
 * it starts up.  It returns requested information in the form of an
 * HTML document encoded either as HTML or XML combined with an XSLT
 * reference.  Error messages are also as encoded as HTML pages.
 *
 *<p>
 * The following describes the request parameters always required by the 
 * servlet, while other parameters may be required depending on the type 
 * of action.
 *<p>
 * <code>client</code> The type of client issuing the request (required).
 *<p>
 * <code>version</code> The version of the client software issuing
 * the request (required).
 *<p>
 * <code>OscAction</code> The type of action to be performed in serving this
 * request.
 *<p>
 * <code>locale</code> The language reqion to which the request belongs.
 *
**/
public class HtmServlet extends HttpServlet
{
    // Servlet context attributes
    //
    private static final String ATTR_PAGE_EXPIRE = "OscPageCacheExpire";

    // Request attributes
    //
    private static final String ATTR_RESULT = "OscResult";
    private static final String JAVAX_CIPHER = "javax.servlet.request.cipher_suite";
    private static final String SECURE_PATH_PREFIX = "/secure/";
    private static final String LOGIN_ERROR_JSP = "Login.jsp";
    private static final String DEFAULT_LOCALE = "zh_CN";


    // Member variables.  These are typically set once during initialization
    // and thereafter accessed without requiring synchronization.
    //
    private OscLogger        logger = null;  // A thin wrapper around log4j
    private OscContext       context = null; // Config and system resources
    private OscXs            xs = null;      // Provides access to Xs services
    private OscResultFactory resultFactory = null; // Factory of result beans
    private OscSharedCounter numRequests = null;  // Number of htm requests


    public void init (ServletConfig config) throws ServletException
    {
        super.init(config);

        // Get shared counter for the number of HTM requests
        //
        numRequests = OscSharedCounter.getInstance("HTM_REQUESTS", 0);

        // We create a unique logger per type of servlet.  The logger is
        // initialized by the svcdrv log4j.properties, to which the
        // log4j.properties for this web-app (osc) were appended.
        //
        logger = new OscLogger(Logger.getLogger(OscLogger.class.getName()));

        OscStatusCode dummy = new OscStatusCode();
 
        try {
            // Create the context for this servlet, through which we can
            // access config values, the run-time environment, and the 
            // database.
            //
            context = new OscContext(config, logger);

            // Create the mechanism for forwarding to an xs server.
            //
            xs = new OscXs(context);
        } 
        catch (OscContext.InvalidContext e) {
            logger.debug(e);
            throw new ServletException(e.getMessage());
        }
        catch (OscXs.OscExceptionXS e) {
            logger.debug(e);
            throw new ServletException(e.getMessage());
        }

        // Create the factory used to create request results
        //
        resultFactory = new OscResultFactory(context,
                                             xs, 
                                             OscResultRule.ServerType.HTM);

        // Initialize the servlet-context with the configured expiration
        // time on cachable responses.
        //
        ServletContext scontext = getServletContext();
        synchronized(scontext) {
            if (scontext.getAttribute(ATTR_PAGE_EXPIRE) == null) {
                final long expires = context.getPageExpireMsecs();
                logger.info("Setting " + ATTR_PAGE_EXPIRE + 
                            " to " + expires/1000 + " secs");
                scontext.setAttribute(ATTR_PAGE_EXPIRE, new Long(expires));
            }
        }
    } // init


    public void doGet(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
        numRequests.incr();

        final String    action = context.getActionNm(req);
        final boolean   tracing = logger.isTracing();
        final TimeTrace ttrace = (tracing? 
                                  new TimeTrace(TimeTrace.INCLUSIVE) : null);

        // Create the result bean and validate the syntax of the request,
        // without yet serving the request (see below).
        //
        OscLoginSession loginSession = null;
        try {
            // Create the result bean and validate the syntax of the request,
            // without yet serving the request (see below).
            //
            OscResultBean result = resultFactory.create(req, res);

            // Check whether or not this request requires a secure connection
            //
            if (result.requiresSecureRequest() &&
                (req.getAttribute(JAVAX_CIPHER) == null ||
                 !req.getServletPath().startsWith(SECURE_PATH_PREFIX))) {
                throw new InvalidRequestException("Can only do " + action +
                                                  " over secure connection");
            }

            // Get hold of the current login session information, if any.
            //
            loginSession = OscLoginSession.create(context, req);

            // Check for login errors
            //
            if (!loginSession.isLoggedIn() && result.requiresLogin()) {
                result = 
                    resultFactory.loginError(req, res, 
                                             OscStatusCode.OSC_MUST_LOGIN);
                if (tracing) ttrace.endInterval("login_error");
                forwardToModule(loginSession, result, req, res);
                if (tracing) ttrace.endInterval("forward");
            }
            else {
                // We have a valid login session when login is required,
                // syntactically valid request.  We now serve the request,
                // performing any actions required.  Note that this may also
                // change the login status and the loginSession object 
                // accordingly, since the request may be a request to log in
                // or to log out.
                //
                if (tracing) ttrace.endInterval("setup");
                result.serveRequest(loginSession);
                if (tracing) ttrace.endInterval("serve");
        
                // Forward the result to a JSP page to generate the response
                // as an HTML document.
                //
                forwardToModule(loginSession, result, req, res);
                if (tracing) ttrace.endInterval("forward");
            }
        }
        catch (InvalidRequestException e) {      
            logger.debug(e);

            OscResultBean result =
                resultFactory.error(req, res, e.getMessage());
            forwardToModule(loginSession, result, req, res);
        }

        if (tracing) logger.info("HTM " + OscRequestParams.REQ_ACTION + "=" +
                                 action + " " + ttrace);

    } // doGet


    public void doPost(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
        doGet(req, res);
    }

    // -------------------------------------------
    // Hidden utilities used in the above
    // -------------------------------------------

    private void forwardToModule(OscLoginSession     login,
                                 OscResultBean       result,
                                 HttpServletRequest  req, 
                                 HttpServletResponse res)
        throws IOException, ServletException
    {
        
        // Always record the final login status as a cookie in the
        // response back to the user, except under error conditions,
        // where the login may be null.
        //
        if (login != null)
            login.setResponse(res);

        ServletContext sc = getServletContext();

        String locale = req.getParameter("locale");
        if (locale==null || locale.equals(""))
            locale = DEFAULT_LOCALE;
        String jspPage = result.jspFwdUrl();
        String valid_url = res.encodeURL(context.getJspPath(locale, jspPage));

        logger.debug(getClass().getName() + " forward with: " + valid_url);

        RequestDispatcher rd = sc.getRequestDispatcher(valid_url);
        req.setAttribute(ATTR_RESULT, result);
        rd.forward(req, res);
    }

} // class HtmServlet



package com.broadon.osc.java_util;

import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.http.*;

import com.broadon.exception.InvalidRequestException;
import com.broadon.status.StatusCode;

/** The class used to serve VNG server request for game score submit/update
 *  and returns the appropriate document and header back to the VNG server.
 */

public class OscResultVNGGameScore extends OscResultBean
{
    /** Rules for creating a Game Score result object
     */
    public static class Rule extends OscResultRule {
        
        public static final String GAME_SCORE = "gscore";

        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
        }
        public boolean supports(ServerType t)
        {
            return t == ServerType.RPC;
        }
        public OscResultBean create(HttpServletRequest req,  
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
            return new OscResultVNGGameScore(req, res, this);
        }
    } // class Rule


    // --------------------------------------------
    // Invariant values
    // --------------------------------------------

    private static final String RANK_TAG = "rank";
    private static final String ITEM_TAG = "item";

    // --------------------------------------------
    // Member variables
    // --------------------------------------------

    private final OscResultVNGGameScore.Rule 	rule;
    private final OscContext          			context;
    private final OscLogger          			logger;
    private final String                        actionName;
    private final String						actionType;
    
    private String				status;
    private String				statusMsg;
    
    private String				device_id;
    private String				slot_id;
    private String				title_id;
    private String				game_id;
    private String				score_id;
    private String				score_info;
    private String[]			item_id;
    private String[]			score;    
    private String				score_obj_size;
    private String				score_obj;
    
    private String				membership_id;
    private String				member_id;
    private String				accepted;
    private String[]			my_rank;

    // ----------------------------------------------------------
    // Hidden constructors and helper methods (all constructors should
    // be hidden for result objects, since they are constructed by the
    // Rule class)
    // ---------------------------------------------------------

    private OscResultVNGGameScore(HttpServletRequest  req,  
                                 HttpServletResponse res,
                                 OscResultVNGGameScore.Rule rule)
        throws InvalidRequestException
    {
        this.rule = rule;
        context = rule.getContext();
        logger = context.getLogger();
        actionName = rule.getActionNm();
        actionType = req.getParameter(ORP.GAME_SCORE_TYPE);

        status = OscStatusCode.SC_OK;
        statusMsg = StatusCode.getMessage(status);
        accepted = "0";
        membership_id = null;
        member_id = null;
        
        device_id = req.getParameter(ORP.DEVICE_ID);
        slot_id = req.getParameter(ORP.SLOT_ID);
        title_id = req.getParameter(ORP.TITLE_ID);
        game_id = req.getParameter(ORP.GAME_ID);
        score_id = req.getParameter(ORP.SCORE_ID);
        
        if(actionType == null || actionType.equals("")){
        	status = OscStatusCode.VNG_GAME_SCORE_PARAMS_ERROR;
            statusMsg = OscStatusCode.getMessage(status);
            logger.error( 
            		statusMsg + " " +
            		"[OscAction=" + actionName + "] " +
            		"[type=" + actionType + "] ");
            return;
        }
        
        if(device_id == null || device_id.equals("")
        		|| slot_id == null || slot_id.equals("")
        		|| title_id == null || title_id.equals("")
        		|| game_id == null || game_id.equals("")
        		|| score_id == null || score_id.equals(""))
        {
        	status = OscStatusCode.VNG_GAME_SCORE_PARAMS_ERROR;
            statusMsg = OscStatusCode.getMessage(status);
            logger.error(
            		statusMsg + " " +
            		"[OscAction=" + actionName + "] " +
            		"[type=" + actionType + "] (" + 
            		"device_id=" + device_id + "|" +
            		"slot_id=" + slot_id + "|" +
            		"title_id=" + title_id + "|" +
            		"game_id=" + game_id + "|" +
            		"score_id=" + score_id + ")");
            return;
        }        

        if("score".equalsIgnoreCase(actionType)){
            score_info = req.getParameter(ORP.SCORE_INFO);
            item_id = req.getParameterValues(ORP.ITEM_ID);
            score = req.getParameterValues(ORP.SCORE);
            
            if(item_id == null || item_id.length == 0
            		|| score == null || score.length == 0
            		|| item_id.length != score.length)
            {
            	status = OscStatusCode.VNG_GAME_SCORE_PARAMS_ERROR;
                statusMsg = OscStatusCode.getMessage(status);
                int len1 = item_id == null ? 0:item_id.length;
                int len2 = score == null ? 0:score.length;
                logger.error(
                		statusMsg + " " +
                		"[OscAction=" + actionName + "] " +
                		"[type=" + actionType + "] (" + 
                		"score_info=" + score_info + "|" + 
                		"item_id(len:" + len1 + ")" + "|" +
                		"score(len:" + len2 + ") )");
                return;
            }
            
        }else if("object".equalsIgnoreCase(actionType)){
        	score_obj_size = req.getParameter(ORP.SCORE_OBJECT_SIZE);
            score_obj = req.getParameter(ORP.SCORE_OBJECT);
            
            if(score_obj_size == null 
            		|| score_obj_size.equals("")
            		|| score_obj == null 
            		|| score_obj.equals(""))
            {
            	status = OscStatusCode.VNG_GAME_SCORE_PARAMS_ERROR;
                statusMsg = OscStatusCode.getMessage(status);
                int length = score_obj == null ? 0 : score_obj.length();
                String str = score_obj == null ? "null" : score_obj.substring(0,10) + "...";
                logger.error(
                		statusMsg + " " +
                		"[OscAction=" + actionName + "] " +
                		"[type=" + actionType + "] (" +
                		"score_object_size=" + score_obj_size + "|" + 
                		"score_obj=" + str + "(len:" + length + ") )");
                return;
            }            
        }
        
    }

    // --------------------------------------------
    // Specialization of abstract methods
    // --------------------------------------------

    public boolean requiresLogin() {return true;}

    public String jspFwdUrl() {return null;}

    public void encodeXml(PrintWriter out)
    {
        // Encodes the final results
        //
        out.println("<" + ORP.RPC_RESULT_TAG + ">");

        context.encodeXml_TNL(out, ORP.REQ_ACTION, actionName);
        context.encodeXml_TNL(out, ORP.GAME_SCORE_TYPE, actionType);
        context.encodeXml_TNL(out, STATUS_TAG, status);
        context.encodeXml_TNL(out, STATUS_MSG_TAG, statusMsg);
        
        if (!getIsError())
        {
        	if("score".equalsIgnoreCase(actionType)
        			|| "object".equalsIgnoreCase(actionType))
        		context.encodeXml_TNL(out, ORP.SCORE_ACCEPTED_TAG, accepted);
        
        	if(accepted.equals("1") && "score".equalsIgnoreCase(actionType))
        	{        		      	
        		for(int i=0; i<item_id.length; i++)
        		{
        			out.println("\t<" + ITEM_TAG + ">");
        			context.encodeXml_TNL(out, ORP.ITEM_ID, item_id[i]);
        			context.encodeXml_TNL(out, RANK_TAG, my_rank[i]);
        			out.println("\t</" + ITEM_TAG + ">");
        		}
        		
        	}
        }
        
        out.println("</" + ORP.RPC_RESULT_TAG + ">");
    } // encodeXml

    public void serveRequest(OscLoginSession loginSession)
        throws InvalidRequestException
    {
        if (!getIsError()) {
        	
        	membership_id = Long.toString(loginSession.getMembershipID());
        	member_id = Long.toString(loginSession.getMemberID());

            OscDbConnection conn = null;

            try {
                conn = context.getDB(false);
                int rows = 0;

                if ("score".equalsIgnoreCase(actionType))
                {                    
                    rows = 
                    	OscDmlGameScore.insertGameScore(conn,
                    									membership_id,
                    									member_id,
                    									device_id,
                    									slot_id,
                    									title_id,
                    									game_id,
                    									score_id,
                    									score_info,
                    									item_id,
                    									score);
                    if(rows == item_id.length + 1){
						conn.commit();
						my_rank = 
							OscDmlGameScore.getMyRank(conn,
													  device_id,
													  slot_id,
													  title_id,
													  game_id,                                            
													  score_id);
						accepted = "1";
					}else {
						conn.rollback();
					} 
                    
                } else if ("object".equalsIgnoreCase(actionType)) {
                    rows = 
                    	OscDmlGameScore.updateGameScoreObject(conn, 
                    										  membership_id,
                    										  member_id,
                    										  device_id,
                    										  slot_id,
                    										  title_id,
                    										  game_id,
                    										  score_id,
                    										  score_obj_size,
                    										  score_obj);
                    if(rows == 1){
                        conn.commit();                       
                        accepted = "1";
                    }else if(rows == 2){
                    	conn.rollback();
                    	status = OscStatusCode.VNG_GAME_SCORE_RECORDS_UNFOUND_ERROR;
						statusMsg = OscStatusCode.getMessage(status);
                    }else{
                    	conn.rollback();
                    }
                    
                } else if ("delete".equalsIgnoreCase(actionType)) {
                	rows = 
                    	OscDmlGameScore.deleteGameScoreObject(conn, 
                    										  membership_id,
                    										  member_id,
                    										  device_id,
                    										  slot_id,
                    										  title_id,
                    										  game_id,
                    										  score_id);

                    if(rows >=2){
                    	conn.commit(); 
                    } else {
                    	conn.rollback();
                    	status = OscStatusCode.VNG_GAME_SCORE_RECORDS_UNFOUND_ERROR;
						statusMsg = OscStatusCode.getMessage(status);
                    }
                }
            }
            catch (Exception e) {
                try {conn.rollback();} catch (SQLException se) {}
                status = OscStatusCode.VNG_DB_ERROR;
                statusMsg = OscStatusCode.getMessage(status);
                e.printStackTrace();
                logger.error(
                		statusMsg + 
						" [OscAction=" + actionName + "]" + 
						" [Type=" + actionType + "]" +
						" Exception: " + e.getMessage());
            }
            finally {
                if (conn != null)
                    conn.close();
            }
        }
    }

    public String toString() {return null;}

    public boolean getIsError()
    {
        return !status.equals(OscStatusCode.SC_OK);
    }

    public String getErrorMsg()
    {
        return statusMsg;
    }

} // class OscResultGameScore

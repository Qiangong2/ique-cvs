package com.broadon.osc.java_util;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.broadon.exception.InvalidRequestException;


/** The class used to create the results associated with a logout
 *  request, such that the appropriate document and header can be
 *  created in the response back to the client
 */
public class OscResultLogout extends OscResultBean
{
    /** Rules for creating a Logout result object
     */
    public static class Rule extends OscResultRule {

        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
        }
        public boolean supports(ServerType t)
        {
            return t == ServerType.HTM;
        }
        public OscResultBean create(HttpServletRequest req,  
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
            return new OscResultLogout(req, res, this);
        }
    } // class Rule


    // --------------------------------------------
    // Invariant values
    // --------------------------------------------

    private static final String LOGOUT_JSP = "LoginForm.jsp";
    private static final String INCORRECT_LOGOUT_MSG = OscStatusCode.OSC_INCORRECT_LOGOUT;

    // --------------------------------------------
    // Member variables
    // --------------------------------------------

    private final OscResultLogout.Rule rule;
    private final OscContext           context;
    private String                     failureMsg;

    // --------------------------------------------
    // Hidden constructors (all constructors should
    // be hidden)
    // --------------------------------------------

    private OscResultLogout(HttpServletRequest  req,  
                            HttpServletResponse res,
                            OscResultLogout.Rule rule)
        throws InvalidRequestException
    {
        this.rule = rule;
        context = rule.getContext();
        failureMsg = null;
    }


    // --------------------------------------------
    // Specialization of abstract methods
    // --------------------------------------------

    public boolean requiresLogin() {return true;}

    public String  jspFwdUrl() {return LOGOUT_JSP;}

    public void encodeXml(PrintWriter out) {};

    public void serveRequest(OscLoginSession loginSession)
        throws InvalidRequestException
    {
        if (loginSession.isLoggedIn()) {
            loginSession.logout();
        }
        else {
            failureMsg = INCORRECT_LOGOUT_MSG; // Already logged out
        }
    }

    public String toString() {return null;}

    public boolean getIsError() {return failureMsg != null;}
    public String getErrorCode() {return failureMsg;}

} // class OscResultLogout


package com.broadon.osc.java_util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;

import com.broadon.util.VersionNumber;
import com.broadon.xml.XMLHybridMap;


public final class OscXs
{
    // ------------------------------------------------
    // Hidden part
    // ------------------------------------------------
    
    // Request XML root tags and corresponding URI
    //
    private static final String TAG_ETICKET_SYNC_REQ = "eticket_sync_request";
    private static final String TAG_PURCHASE_REQ = "purchase_request";
    private static final String TAG_UPGRADE_REQ = "upgrade_request";
    private static final String TAG_UPLOAD_REQ = "submit_states_request";

    private static final String URI_ETICKET_SYNC_REQ = "/syncTickets";
    private static final String URI_PURCHASE_REQ = "/purchase";
    private static final String URI_UPGRADE_REQ = "/upgrade";
    private static final String URI_UPLOAD_REQ = "/upload";

    // Response XML root tags and corresponding URI
    //
    private static final String TAG_ETICKET_SYNC_RES = "eticket_sync_result";
    private static final String TAG_PURCHASE_RES = "purchase_result";
    private static final String TAG_UPGRADE_RES = "upgrade_result";
    private static final String TAG_UPLOAD_RES = "submit_states_result";

    // Other constants
    //
    private static final VersionNumber DEPOT_REQ_VERSION = VersionNumber.valueOf("1.3.0");

    private static final String REQ_FILE_PREFIX = "osc_req";
    private static final String REQ_FILE_SUFFIX = ".xml";
    private static final String RESP_FILE_PREFIX = "osc_resp";
    private static final String RESP_FILE_SUFFIX = ".txt";
    private static final String HTTP_END_OF_HEADER = "\r\n\r\n";
    private static final String HTTP_OK = "HTTP/1.1 20";


    // Thread local XML parser for a flat XML structure of the following
    // form, where elm1 etc. are mapped to the value as a string or a 
    // vector of strings for multivalued results.
    // 
    //   <root_tag>
    //      <elm1>Value1</elm1>
    //      <elm2>Value2</elm2>
    //      <elm3>Value3</elm3>
    //      .... etc.
    //   </root_tag>
    //
    // For more sophisticated parsing if nested structures we need a
    // new kind of XMLOutputProcessor.
    //
    private static class XMLParserFactory extends ThreadLocal 
    {
        public Object initialValue() 
        {
            try {
                return new XMLHybridMap();
            } catch (IOException e) {
                return null;
            }
        }
	}


    // Members
    //
    private OscLogger        log;
    private OscContext       context;
    private String           svcreqPath;
    private String           xsHostname;
    private String           xsPort;
    private String           xsUri;
    private String           xsReqPrefix;
    private String           submitTimeoutSecs;
    private XMLParserFactory parserFactory;
    private HashSet          eticketResponseDefn;
    private HashSet          purchaseResponseDefn;
    private HashSet          upgradeResponseDefn;
    private HashSet          uploadResponseDefn;


    private String verifyPath(File path) throws OscExceptionXS
    {
        String pathName = path.getAbsolutePath();
        if (!path.exists()) {
            throw new OscExceptionXS("Cannot find file \"" + pathName + "\"");
        }
        return pathName;
    } // verifyPath


    private String verifyPath(String path) throws OscExceptionXS
    {
        return verifyPath(new File(path));
    } // verifyPath


    /** Issues a request to the xs server with paramsXml being the request
     *  parameters as a sequence of XML tagged values.  The tag will surround
     *  the request, and the uri is used to issue the request.
     */
    private String submitRequest(Integer storeID,
                                 String  tag, 
                                 String  uri, 
                                 String  paramsXml,
                                 File    response)
        throws OscExceptionXS
    {
        String std_outerr = null;
        File request = null;
        try {
            //
            // Write the request into a temporary XML file
            //
            request = File.createTempFile(REQ_FILE_PREFIX, REQ_FILE_SUFFIX);

            FileWriter f = new FileWriter(request);
            f.write("<" + tag + ">\n" +
                    xsReqPrefix +
                    context.encodeXml_TNL("hr_id", 
                                          context.getStoreHrId(storeID)) +
                    context.encodeXml_TNL("store_id", storeID.toString()) +
                    paramsXml + 
                    "</" + tag + ">\n");
            f.close();

            // Send request to XS server
            //
            final String[] submitreq = 
                {svcreqPath,
                 xsHostname,
                 xsPort,
                 uri,
                 verifyPath(context.getStoreCertsDir(storeID)),
                 request.getAbsolutePath(),
                 submitTimeoutSecs,
                 response.getAbsolutePath()
                };

            std_outerr = context.runCmd(submitreq, null, null);
        }
        catch (Throwable e) {
            log.debug(e);
            throw new OscExceptionXS("Failed to issue " + svcreqPath +
                                     " " + xsHostname + ":" + xsPort + uri +
                                     " request", e);
        }
        finally {
            if (request != null) {
                if (!request.delete()) {
                    log.warn("Failed to delete " + request.getAbsolutePath());
                }
            }
        }
        return std_outerr;
    } // submitRequest


    private HashMap readXsResponseDefn(String propFile)
        throws OscExceptionXS
    {
        // Reads a mapping from the name of the XS root-tag to the
        // response elements that may occur multiple times.
        //
        HashMap multivalues = new HashMap();
        try {
            Properties prop = new Properties();
            String     fullPath = 
                System.getProperty("catalina.base") + "/" + propFile;
            prop.load(new FileInputStream(fullPath));

            for (Enumeration e = prop.propertyNames(); e.hasMoreElements(); ) {
                String root = (String) e.nextElement();
                StringTokenizer st = 
                    new StringTokenizer(prop.getProperty(root));
                HashSet elementSet = new HashSet();
                while (st.hasMoreTokens()) {
                    elementSet.add(st.nextToken());
                }
                multivalues.put(root, elementSet);
            }
	    } catch (IOException e) {
            throw new OscExceptionXS(e.getMessage());
	    }
        return multivalues;
    } // readXsResponseDefn


    private Map xsRequest(Integer storeID,
                          HashSet multiValueDefn,
                          String  result_root_tag,
                          String  req_root_tag,
                          String  req_uri,
                          String  paramsXml) throws OscExceptionXS
    {
        String         std_outerr = "";
        String         hdr = std_outerr;
        FileReader     fin = null;
        BufferedReader response = null;
        File           responseFile = null;

        try {   
            // Issue the request to the XS server, writing the response to
            // a temporary file.
            // 
            responseFile = 
                File.createTempFile(RESP_FILE_PREFIX, RESP_FILE_SUFFIX);
            std_outerr = 
                submitRequest(storeID,
                              req_root_tag, req_uri, paramsXml, responseFile);

            // Now, process the response
            //
            final int readerBufSize = 8096; // 8K read buffer
            final int maxHttpHdrSize = 2048; // 2K read buffer

            fin = new FileReader(responseFile);
            response = new BufferedReader(fin, readerBufSize);
            response.mark(readerBufSize);

            // Skip the HTML header in the response, which we expect to
            // be within the first maxHttpHdrSize bytes.
            //
            final char[] buf = new char[maxHttpHdrSize];
            final int    hdrSz = response.read(buf, 0, buf.length);
            hdr = new String(buf, 0, hdrSz);
            if (hdr.indexOf(HTTP_OK) < 0) {
                throw new OscExceptionXS("Unexpected XS response status for " +
                                         req_uri + ":\n" + hdr + " ....");
            }

            final int offset = hdr.indexOf(HTTP_END_OF_HEADER);
            response.reset();
            if (offset != 0 && offset != -1) {
                response.skip(offset + HTTP_END_OF_HEADER.length());
            }

            // Parse the response and encode it as a mapping from element names
            // to values, where the values are strings.
            //
            XMLHybridMap xmlParser = (XMLHybridMap)parserFactory.get();
            xmlParser.setMultiValueElements(multiValueDefn);
            return (Map)xmlParser.process(response, new HashMap(), 
                                          result_root_tag);
        }
        catch (IOException e) {
            // We expect the OscResultXxxx page to catch this and return a
            // corresponding valid result.
            //
            throw new OscExceptionXS("Failed to parse XS result [" + 
                                     std_outerr + " ... ]: " + hdr + "\n", e);
        }
        finally{
            try {
                if (response != null) response.close();
                if (fin != null) fin.close();
            }
            catch (IOException e) {
                log.warn("Failed to close " + responseFile.getAbsolutePath() +
                         ": " + e.getMessage());
            }
            if (responseFile != null &&
                responseFile.exists() && 
                !responseFile.delete()) {
                log.warn("Failed to delete " + responseFile.getAbsolutePath());
            }
        }
    } // xsRequest


    // ------------------------------------------------
    // Public part
    // ------------------------------------------------

    static public class OscExceptionXS extends OscException {
        
        public OscExceptionXS() {super();}
        public OscExceptionXS(String message) {super(message);}
        public OscExceptionXS(String message, Throwable throwable) {
            super(message, throwable);
        }
    }


    public OscXs(OscContext c) throws OscExceptionXS
    {
        context = c;
        log = context.getLogger();

        svcreqPath = verifyPath(context.getSvcreqPath());

        xsHostname = context.getXsHostname();
        xsPort = Long.toString(context.getXsPort());
        xsUri = context.getXsUri();

        submitTimeoutSecs = context.getXsTimeoutSecs();

        xsReqPrefix = context.encodeXml_TNL("depot_version", 
                                            DEPOT_REQ_VERSION.toString());

        parserFactory = new XMLParserFactory(); // A parser per thread

        // Set up XS response definitions (currently, just a HashSet of
        // response elements that may contain more than one value).
        //
        final HashMap responseDefn = 
            readXsResponseDefn(context.getXsResponseDefnPath());
        eticketResponseDefn = (HashSet)responseDefn.get(TAG_ETICKET_SYNC_RES);
        purchaseResponseDefn = (HashSet)responseDefn.get(TAG_PURCHASE_RES);
        upgradeResponseDefn = (HashSet)responseDefn.get(TAG_UPGRADE_RES);
        uploadResponseDefn = (HashSet)responseDefn.get(TAG_UPLOAD_RES);
    }


    public Map issueETicketSyncRequest(Integer storeID,
                                       String  paramsXml) throws OscExceptionXS
    {
        return xsRequest(storeID,
                         eticketResponseDefn,
                         TAG_ETICKET_SYNC_RES,
                         TAG_ETICKET_SYNC_REQ, 
                         xsUri + URI_ETICKET_SYNC_REQ,
                         paramsXml);
    }

    public Map issuePurchaseRequest(Integer storeID,
                                    String paramsXml) throws OscExceptionXS
    {
        return xsRequest(storeID,
                         purchaseResponseDefn,
                         TAG_PURCHASE_RES,
                         TAG_PURCHASE_REQ, 
                         xsUri + URI_PURCHASE_REQ,
                         paramsXml);
    }

    public Map issueUpgradeRequest(Integer storeID,
                                   String paramsXml) throws OscExceptionXS
    {
        return xsRequest(storeID,
                         upgradeResponseDefn,
                         TAG_UPGRADE_RES,
                         TAG_UPGRADE_REQ, 
                         xsUri + URI_UPGRADE_REQ,
                         paramsXml);
    }

    public Map issueUploadRequest(Integer storeID,
                                  String paramsXml)
        throws OscExceptionXS
    {
        return xsRequest(storeID,
                         uploadResponseDefn,
                         TAG_UPLOAD_RES,
                         TAG_UPLOAD_REQ, 
                         xsUri + URI_UPLOAD_REQ,
                         paramsXml);
    }

} // class OscXs

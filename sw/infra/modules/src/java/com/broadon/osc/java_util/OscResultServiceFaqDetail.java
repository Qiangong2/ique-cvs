package com.broadon.osc.java_util;

import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.broadon.exception.InvalidRequestException;

/** The class used to create the results associated with a Service
 *  request, such that the appropriate document and header can be
 *  created in the response back to the client
 */
public class OscResultServiceFaqDetail extends OscResultBean
{
    /** Rules for creating a service result object
     */
    public static class Rule extends OscResultRule {
    	
        public static final String SERVICE_FAQ_DETAIL = "faq_detail";
        
        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
        }
        public boolean supports(ServerType t)
        {
            return t == ServerType.HTM;
        }
        public OscResultBean create(HttpServletRequest req,  
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
            return new OscResultServiceFaqDetail(req, res, this);
        }
        
    } // class Rule
    
    // --------------------------------------------
    // Invariant values
    // --------------------------------------------

    private static final String SERVICE_FAQ_DETAIL_JSP = "iQueServiceFaqDetail.jsp";
    private static final String FAQ_SEARCH_KEY= "search_Key";
    private static final String FAQ_SEARCH_CATEGORY= "search_Category";
    private static final String FAQ_ID= "faq_id";   
    private static final String DEFAULT_LOCALE="zh_CN";
    private static final String SQL_FAQ_DETAIL_TRANSFER = 
    "INSERT INTO iQue_faqs_tmp SELECT * FROM iQue_faqs WHERE faq_id=? and "+
    "((revoke_date is null) or (revoke_date>sysdate)) and is_visible='visible'";
   
    private static final String SQL_FAQ_DETAIL =
    "SELECT subject, content FROM iQue_faqs_tmp where faq_id=? and "+
    "((revoke_date is null) or (revoke_date>sysdate)) and is_visible='visible'";

    private static final String SQL_FAQ_IMAGE_TRANSFER =
    "INSERT INTO iQue_images_tmp SELECT * FROM iQue_images WHERE faq_id=? and "+
    "((revoke_date is null) or (revoke_date>sysdate))";
    
    private static final String SQL_FAQ_IMAGE = 
    "SELECT image_id,image_desc FROM iQue_images_tmp where faq_id=? and "+
    "((revoke_date is null) or (revoke_date>sysdate)) order by image_id";

    private static final String SQL_UPDATE_FAQ_QTYREADS =
    "UPDATE iQue_faqs SET qtyreads=NVL(qtyreads,0)+1 where faq_id=?";
 
    // --------------------------------------------
    // Member variables
    // --------------------------------------------

    private final OscResultServiceFaqDetail.Rule        rule;
    private final OscContext          		        context;
    private final OscLogger          			logger;
    private String 				        contentXML;
    private String					imageXML;
    private String                                      actionName;
    private String                                      locale;
    private int					        faqID;
    private int					        pageNo;
    private String				        searchKey;
    private String[]				        searchCategoryArray;

    // --------------------------------------------
    // Hidden constructors (all constructors should
    // be hidden)
    // --------------------------------------------

    private OscResultServiceFaqDetail(HttpServletRequest  req,  
                                      HttpServletResponse res,
                                      OscResultServiceFaqDetail.Rule rule)
        throws InvalidRequestException
    {
        this.rule = rule;
        context = rule.getContext();
        logger = context.getLogger();
        actionName = rule.getActionNm();
        contentXML = "";
        imageXML = "";
	pageNo= context.getPageNo(req);

	if (!((req.getParameter(FAQ_SEARCH_KEY)==null)||(req.getParameter(FAQ_SEARCH_KEY).trim().equals("")))) {
		searchKey=req.getParameter(FAQ_SEARCH_KEY).trim();
	}
	if (!(req.getParameter(FAQ_SEARCH_CATEGORY)==null)) {
		searchCategoryArray=req.getParameterValues(FAQ_SEARCH_CATEGORY);
	}
	if ((!((req.getParameter(FAQ_ID)==null)||(req.getParameter(FAQ_ID).trim().equals(""))))&&(OscContext.isDigital(req.getParameter(FAQ_ID).trim()))) {
		faqID=Integer.parseInt(req.getParameter(FAQ_ID).trim());
	}else{
		faqID=0;
	}
	           
        locale = req.getParameter("locale");
        if (locale==null || locale.equals(""))
            locale = DEFAULT_LOCALE;
    }
    
    // --------------------------------------------------------------------
    //            Bean Properties
    // --------------------------------------------------------------------
    
    public int getPageNo() { return pageNo; }
    
    public String getSearchKey() { return searchKey; }
    
    public String[] getSearchCategory() { return searchCategoryArray; }
    
    // --------------------------------------------
    // Specialization of abstract methods
    // --------------------------------------------

    public boolean requiresLogin() {return true;}

    public String jspFwdUrl() 
    {
        return SERVICE_FAQ_DETAIL_JSP;
    }

    public void encodeXml(PrintWriter out) {};

    public void serveRequest(OscLoginSession loginSession)
        throws InvalidRequestException
    {
        OscDbConnection conn = null;
        PreparedStatement stmt = null;

        try {
            String sql = null;
    
	    conn = context.getDB(false);
            stmt = conn.prepareStatement(SQL_FAQ_DETAIL_TRANSFER);
            stmt.setLong(1, faqID);
            stmt.executeUpdate();
            conn.closeStatement(stmt);

	    sql = SQL_FAQ_DETAIL;
            sql = sql.substring(0,sql.indexOf("?")) + Integer.toString(faqID) + sql.substring(sql.lastIndexOf("?")+1);
	    contentXML = conn.queryXML(sql);

            stmt = conn.prepareStatement(SQL_FAQ_IMAGE_TRANSFER);
            stmt.setLong(1, faqID);
            stmt.executeUpdate();
            conn.closeStatement(stmt);

            sql = SQL_FAQ_IMAGE;
            sql = sql.substring(0,sql.indexOf("?")) + Integer.toString(faqID) + sql.substring(sql.lastIndexOf("?")+1);
            imageXML = conn.queryXML(sql);

	    stmt = conn.prepareStatement(SQL_UPDATE_FAQ_QTYREADS);
            stmt.setLong(1, faqID);
            stmt.executeUpdate();
            conn.closeStatement(stmt);
            conn.commit();
        }
	catch (SQLException e) {
            try {
               conn.rollback();
            }
            catch (SQLException ex){
               logger.error("Failed to rollback. " + ex.getMessage());
            }
            logger.error("Failed to query FAQ content. " + e.getMessage());
            throw new InvalidRequestException("Failed to obtain content for FAQ ID.");
        }
        catch (Exception e) {
	    logger.error("Failed to obtain " + actionName + " FAQ content. " + e.getMessage());
            throw new InvalidRequestException("Failed to obtain content for FAQ ID.");
        }
        finally {
            if (conn != null)
                conn.close();
        }
    }

    public String toString() {return null;}

    // --------------------------------------------
    // Definition of result specific bean values used
    // for formatting a result or access through
    // JSP pages.
    // --------------------------------------------

    public String transformXML(String xmlStr, String xslFile, String locale)
    { return context.transformXML(xmlStr,xslFile,locale); }

    public String getContentXML() {return contentXML;}

    public String getImageXML() {return imageXML;}
}
// class OscResultServiceFaqDetail

package com.broadon.osc.java_util;

import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.broadon.exception.InvalidRequestException;
import com.broadon.util.VersionNumber;

/** The class used to create the results associated with a Service
 *  request, such that the appropriate document and header can be
 *  created in the response back to the client
 */
public class OscResultActivity extends OscResultBean
{
    /** Rules for creating a service result object
     */
    public static class Rule extends OscResultRule {
    	
        //public static final String MEMBER_ACTIVITY = "memberactivity";
        
        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
        }
        public boolean supports(ServerType t)
        {
            return t == ServerType.HTM;
        }
        public OscResultBean create(HttpServletRequest req,  
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
            return new OscResultActivity(req, res, this);
        }
        
    } // class Rule

    // --------------------------------------------
    // Invariant values
    // --------------------------------------------
    private static final String REDIRECT_KEY = "redirectkey";
    private static final String MEMBER_ACTIVITY_JSP = "MemberActivity.jsp";
    private static final String MEMBER_QUIZEGAME_JSP = "MemberQuizGameTestPaper.jsp";
    private static final String MEMBER_COMPETITION_61011_JSP = "MemberCompetition_61011.jsp";  
    private static final String MEMBER_COMPETITION_51021_JSP = "MemberCompetition_51021.jsp";
    private static final String SQL_SEARCH_TITLE =
                "select tit.title,com.competition_desc from osc.iQue_competitions com,cdr.content_titles tit "+
                "where tit.title_id=com.title_id and com.competition_id=?";
    private static final String SQL_UPDATE_TIME =
                "select count(*) from iQue_competition_score_logs "+
                "where membership_id=? and member_id=? and competition_id=? "+
                "and submit_date>SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)-1" ;
    // --------------------------------------------
    // Member variables
    // --------------------------------------------

    private final OscResultActivity.Rule rule;
    private final OscContext            context;
    private final OscLogger             logger;
    private final String                actionName;
    private final VersionNumber         version;
    private final String                client;
    private final String                locale;
    private       String	        redirectkey;
    private       long                  competitionID;
    private       String                titleName;
    private       String                competitionDesc;
    private       long                  membership_id;
    private       long                  member_id;
    private       long                  update_time;

    // --------------------------------------------
    // Hidden constructors (all constructors should
    // be hidden)
    // --------------------------------------------

    private OscResultActivity(HttpServletRequest  req,  
                            HttpServletResponse res,
                            OscResultActivity.Rule rule)
        throws InvalidRequestException
    {
        this.rule = rule;
        context = rule.getContext();
        logger = context.getLogger();
        actionName = rule.getActionNm();
        version = context.getRequestVersion(req);
        client = context.getRequestClient(req);
        locale = context.getRequestLocale(req);
	if (!((req.getParameter(REDIRECT_KEY)==null)||(req.getParameter(REDIRECT_KEY).trim().equals("")))) {
	    redirectkey=req.getParameter(REDIRECT_KEY).trim();
	} else {
            redirectkey="";
        }
        competitionID = 0;
        titleName = null;
        competitionDesc = null;
        membership_id = 0;
        member_id = 0;
        update_time = 0;
    }
    
    // --------------------------------------------------------------------
    //            Bean Properties
    // --------------------------------------------------------------------

    /**
     * Bean Property: 
     */

    // --------------------------------------------
    // Specialization of abstract methods
    // --------------------------------------------

    public boolean requiresLogin() {return true;}

    public String jspFwdUrl() 
    {
	if (redirectkey.equals("1")) {
            return MEMBER_COMPETITION_51021_JSP;
        }
        if (redirectkey.equals("2")) {
            return MEMBER_COMPETITION_61011_JSP;
        } else {
            return MEMBER_ACTIVITY_JSP;
        }
    }

    public void encodeXml(PrintWriter out) {};

    public void serveRequest(OscLoginSession loginSession)
        throws InvalidRequestException
    {
        if ((!redirectkey.equals(null)) && (context.isDigital(redirectkey))){
            membership_id = loginSession.getMembershipID();
            member_id = loginSession.getMemberID();
            OscDbConnection conn = null;
            PreparedStatement stmt = null;
            competitionID = (Long.valueOf(redirectkey)).longValue();
            try {
                conn = context.getDB(true);
                stmt = conn.prepareStatement(SQL_SEARCH_TITLE);
                stmt.setLong(1, competitionID);
                ResultSet rs = stmt.executeQuery();
                try {
                    rs.next();
                    titleName = rs.getString(1);
                    competitionDesc = rs.getString(2);
                } catch (Exception ex) {
	            logger.error("Can not find competition_ID:" + String.valueOf(competitionID) + "." + ex.getMessage());
                    throw new InvalidRequestException("Can not find competitionID in iQue_competitions.");
                }
                rs.close();
            } catch (SQLException e) {
 	        logger.error("Ignoring invalid competition_ID" + String.valueOf(competitionID) + "." + e.getMessage());
                throw new InvalidRequestException("Failed to take out competitionDesc and titleName.");
            }
            
            try {
                stmt.clearParameters();
                stmt = conn.prepareStatement(SQL_UPDATE_TIME);
                stmt.setLong(1, membership_id);
                stmt.setLong(2, member_id);
                stmt.setLong(3, competitionID);
                ResultSet rs = stmt.executeQuery();
                try {
                    rs.next();
                    update_time = rs.getLong(1);
                } catch (Exception ex) {
	            logger.error("Can not get update time." + ex.getMessage());
                    throw new InvalidRequestException("Can not get update time.");
                }
                rs.close();
            } catch (SQLException e) {
 	        logger.error("Ignoring invalid membership_id or member_id or competitin_id" + String.valueOf(membership_id) + ";" + String.valueOf(member_id) + ";" + String.valueOf(competitionID) + e.getMessage());
                throw new InvalidRequestException("Failed to take out update time in last one day.");
	    } finally {
                if (conn != null) {
                    conn.closeStatement(stmt);
                    conn.close();
                }
            }
        }
    }

    // --------------------------------------------
    // Definition of result specific bean values used
    // for formatting a result or access through
    // JSP pages.
    // --------------------------------------------
    public String getTitleName() {return titleName;}

    public String getCompetitionDesc() {return competitionDesc;}

    public long getUpdateTime() {return update_time;}

} // class OscResultActivity


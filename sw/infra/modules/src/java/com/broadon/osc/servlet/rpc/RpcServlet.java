/*
 * (C) 2004, BroadOn Communications Corp.,
 * $Id: RpcServlet.java,v 1.23 2005/03/22 03:04:11 ho Exp $
 */
package com.broadon.osc.servlet.rpc;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.apache.log4j.Logger;

import com.broadon.util.TimeTrace;

import com.broadon.exception.InvalidRequestException;
import com.broadon.osc.java_util.*;


/**
 * The <code>RpcServlet</code> is a servlet that accepts POST/GET 
 * requests, accepts parameters to the request either as part of the
 * URI or as an XML document embedded in the request, and returns an
 * XML result.  In addition to any values passed in as parameters, 
 * this servlet also expects cookies holding information about the
 * current logon session.
 *
 *<p> This servlet interacts with the XS server and the DB, both of
 * which are provided as part of the configuration of the servlet when
 * it starts up.  It returns requested information in the form of an
 * XML document encoded in a proprietary format (not necessarily standard
 * XML).  Error messages also as encoded as an XML page.
 *
 *<p>
 * The following describes the request parameters required by the servlet,
 * while other parameters may be required depending on the type of action.
 *<p>
 * <code>client</code> The type of client issuing the request (required).
 *<p>
 * <code>version</code> The version of the client software issuing
 * the request (required).
 *<p>
 * <code>OscAction</code> The type of action to be performed in serving this
 * request.
 *<p>
 * <code>locale</code> The language reqion to which the request belongs.
 *
**/
public class RpcServlet extends HttpServlet
{
    // Request attributes
    //
    private static final String ATTR_RESULT = "OscResult";
    private static final String JAVAX_CIPHER = "javax.servlet.request.cipher_suite";
    private static final String SECURE_PATH_PREFIX = "/secure/";
    private static final String INTERNAL_PATH_PREFIX = "/internal/";
 

    // Member variables.  These are typically set once during initialization
    // and thereafter accessed without requiring synchronization.
    //
    private OscLogger        logger = null;  // A thin wrapper around log4j
    private OscContext       context = null; // Config and system resources
    private OscXs            xs = null;      // Provides access to Xs services
    private OscResultFactory resultFactory = null; // Factory of result beans
    private OscSharedCounter numRequests = null;  // Number of rpc requests


    private void unexpectedError(HttpServletResponse res, 
                                 OscResultBean       result,
                                 Throwable           e,
                                 String              statusCode)
    {
        try {
            logger.error(e);
            if (result != null) {
                result.encodeErrorXml(res.getWriter(), statusCode);
            }
            else {
                res.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
                              e.getMessage());
            }
        }
        catch (Throwable t) {
            // Ignore all errors here
        }
    }


    public void init (ServletConfig config) throws ServletException
    {
        super.init(config);

        // Get shared counter for the number of RPC requests
        //
        numRequests = OscSharedCounter.getInstance("RPC_REQUESTS", 0);

        // Force the class to be loaded, such that the static initializers will
        // be activated.
        //
        OscStatusCode dummy = new OscStatusCode();

        // We create a unique logger for all servlets belonging to
        // this server.  The logger is initialized by the svcdrv
        // log4j.properties, to which the log4j.properties for this
        // web-app (osc) were appended.
        //
        logger = new OscLogger(Logger.getLogger(OscLogger.class.getName()));

        try {
            // Create the context for this servlet, through which we can
            // access config values, the run-time environment, and the 
            // database.
            //
            context = new OscContext(config, logger);

            // Create the mechanism for forwarding to an xs server.
            //
            xs = new OscXs(context);
        } 
        catch (OscContext.InvalidContext e) {
            logger.debug(e);
            throw new ServletException(e.getMessage());
        }
        catch (OscXs.OscExceptionXS e) {
            logger.debug(e);
            throw new ServletException(e.getMessage());
        }

        // Create the factory used to create request results
        //
        resultFactory = new OscResultFactory(context,
                                             xs, 
                                             OscResultRule.ServerType.RPC);
    } // init


    public void doGet(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
        numRequests.incr();

        final String    action = context.getActionNm(req);
        final boolean   tracing = logger.isTracing();
        final TimeTrace ttrace = (tracing? 
                                  new TimeTrace(TimeTrace.INCLUSIVE) : null);

        OscResultBean result = null;

        // Create the result bean and validate the syntax of the request,
        // without yet serving the request (see below).
        //
        try {
            // Set response headers, using the default of:
            //
            //    res.setStatus(HttpServletResponse.SC_OK);
            //
            res.setContentType("text/plain; charset=utf-8");
            res.setHeader("Cache-Control", "no-cache");
            res.setHeader("Pragma", "no-cache");

            // Create the result bean and validate the syntax of the request,
            // without yet serving the request (see below).
            //
            result = resultFactory.create(req, res);

            // Check whether or not this request requires an internal connection
            //
            if (result.requiresInternalRequest() && 
                !req.getServletPath().startsWith(INTERNAL_PATH_PREFIX)) {
                throw new InvalidRequestException("Can only do " + action +
                                                  " over internal connection");
            }

            // Get hold of the current login session information, if any.
            //
            OscLoginSession loginSession = 
                OscLoginSession.create(context, req);

            // If login is required and an id and passwd is provided, then
            // log the user in, such that the request can be served.
            //
            String memberID = req.getParameter(OscRequestParams.REQ_LOGIN_ID);
            String passwd = req.getParameter(OscRequestParams.REQ_LOGIN_PWD);
            if (result.requiresLogin()     && 
                !loginSession.isLoggedIn() &&
                memberID != null && passwd != null) {
                loginSession.login(memberID, passwd);
            }

            // Now serve the request if possible, always returning a cookie
            // for a valid request.
            //
            if (result.requiresLogin() && !loginSession.isLoggedIn()) {
                loginSession.setResponse(res);
                result.encodeErrorXml(res.getWriter(),
                                      OscStatusCode.OSC_REQUIRE_LOGIN);
            }
            else {

                // We have a valid login session, when login is
                // required, and a syntactically valid request.  We now
                // serve the request, performing any actions required.
                // Note that this may also change the login status and
                // the loginSession object accordingly, since the
                // request may be a request to log in or to log out.
                //
                if (tracing) ttrace.endInterval("setup");
                result.serveRequest(loginSession);
                if (tracing) ttrace.endInterval("serve");
        
                // Always record the final login status in the
                // response back to the user when there are no 
                // errors.
                //
                loginSession.setResponse(res);

                // Create XML response (possibly containing an error
                // status in in the body).
                //
                result.encodeXml(res.getWriter());
                if (tracing) ttrace.endInterval("encode");
            }
        }
        catch (InvalidRequestException e) {
            unexpectedError(res, result, e, OscStatusCode.OSC_BAD_REQUEST);
        }
        catch (Throwable e) {
            unexpectedError(res, result, e, OscStatusCode.OSC_INTERNAL_ERROR);
        }

        if (tracing) {
            logger.info("RPC " + OscRequestParams.REQ_ACTION + "=" +
                        context.getActionNm(req) + " " + ttrace);
        }
    } // doGet


    public void doPost(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
        doGet(req, res);
    }


} // class RpcServlet



package com.broadon.osc.java_util;

import java.io.*;
import javax.servlet.http.*;

import com.broadon.status.StatusCode;
import com.broadon.exception.InvalidRequestException;

/** The class used to serve VNG server request for offline messages
 *  and returns the appropriate document and header back to the VNG server.
 */

public class OscResultMessages extends OscResultBean
{
    /** Rules for creating a Messages result object
     */
    public static class Rule extends OscResultRule {
        
        public static final String MESSAGE = "message";

        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
        }
        public boolean supports(ServerType t)
        {
            return t == ServerType.RPC;
        }
        public OscResultBean create(HttpServletRequest req,  
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
            return new OscResultMessages(req, res, this);
        }
    } // class Rule


    // --------------------------------------------
    // Invariant values
    // --------------------------------------------

    private static final String STORE_MESSAGE = "store";
    private static final String GET_INCOMING_HEADERS = "iheaders";
    private static final String RETRIEVE_MESSAGE = "retrieve";
    private static final String DELETE_MESSAGE = "delete";
    private static final String CHECK_QUOTA = "quota";

    private static final String TOTAL_COUNT_TAG = "total_count";
    private static final String QUOTA_SEND_COUNT_TAG = "quota_send_count";
    private static final String QUOTA_SEND_LENGTH_TAG = "quota_send_length";
    private static final String QUOTA_RECV_COUNT_TAG = "quota_recv_count";
    private static final String MESSAGE_ID_TAG = "message_id";
    private static final String RECIPIENT_TAG = "recipient";
    private static final String RESULT_TAG = "result";

    private static final long QUOTA_SEND_MESSAGE_COUNT = 100;
    private static final long QUOTA_SEND_MESSAGE_LENGTH = 10485760;  // 10 MB
    private static final long QUOTA_RECV_MESSAGE_COUNT = 100;

    // An enumeration of the types of message actions we support
    //
    public static class ListType {
        private int enumeration;
        private ListType(int e) {enumeration = e;}
        public int intValue() {return enumeration;}
        public static final ListType STORE = new ListType(0);
        public static final ListType I_HEADER = new ListType(1);
        public static final ListType RETRIEVE = new ListType(2);
        public static final ListType DELETE = new ListType(3);
        public static final ListType QUOTA = new ListType(4);
    }

    private static final boolean[] needLogin = {
        true,
        true,
        true,
        true,
	true
    };

    private static final boolean[] isConnectionReadOnly = {
        false,
        true,
        false,
        false,
        true
    };

    // --------------------------------------------
    // Member variables
    // --------------------------------------------

    private final OscResultMessages.Rule 	rule;
    private final OscContext          		context;
    private final OscLogger          		logger;
    private final String                        actionName;

    private String				mType;
    private String                              resultXML;
    private String				status;
    private String				statusMsg;
    private String                              num_rows;
    private String                              skip_rows;
    private int                                 totalCount;

    private long  				myMembershipID;
    private long  				myMemberID;
    private long  				myQuotaRecvMessageCount;
    private long  				myQuotaSendMessageCount;
    private long  				myQuotaSendMessageLength;

    private boolean 				anyException;
    private ListType 				tp;

    private String                              message_id;
    private String[]                            to;
    private String[]                            cc;
    private String[]                            bcc;
    private String                              message_type;
    private String                              reply_id;
    private String                              ip_address;
    private String                              content_type;
    private String                              content_length;
    private String                              send_date;
    private String                              subject;
    private String                              message;
    private String[][]                          recv_info;


    // ----------------------------------------------------------
    // Hidden constructors and helper methods (all constructors should
    // be hidden for result objects, since they are constructed by the
    // Rule class)
    // ---------------------------------------------------------

    private OscResultMessages(HttpServletRequest  req,  
                             HttpServletResponse res,
                             OscResultMessages.Rule rule)
        throws InvalidRequestException
    {
        this.rule = rule;
        context = rule.getContext();
        logger = context.getLogger();
        actionName = rule.getActionNm();

        resultXML = "";
	status = OscStatusCode.SC_OK;
	statusMsg = StatusCode.getMessage(status);

        anyException = false;

        mType = req.getParameter(ORP.MESSAGES_ACTION_TYPE);
        if (mType == null || mType.equals(""))
        {
            anyException = true;
            status = OscStatusCode.OSC_BAD_REQUEST;
            statusMsg = OscStatusCode.getMessage(OscStatusCode.OSC_BAD_REQUEST);
            logger.error("Invalid message sub-action ("  + mType +
                         ") in " + getClass().getName());
            return;
        }
        else if (mType.equals(STORE_MESSAGE)) tp = ListType.STORE;
        else if (mType.equals(GET_INCOMING_HEADERS)) tp = ListType.I_HEADER;
        else if (mType.equals(RETRIEVE_MESSAGE)) tp = ListType.RETRIEVE;
        else if (mType.equals(DELETE_MESSAGE)) tp = ListType.DELETE;
        else if (mType.equals(CHECK_QUOTA)) tp = ListType.QUOTA;
        else {
            anyException = true;
            status = OscStatusCode.OSC_BAD_REQUEST;
            statusMsg = OscStatusCode.getMessage(OscStatusCode.OSC_BAD_REQUEST);
            logger.error("Invalid message sub-action ("  + mType +
                         ") in " + getClass().getName());
            return;
        }

        // Parameters for RETRIEVE_MESSAGE or DELETE_MESSAGE
        message_id = req.getParameter(ORP.MESSAGES_MESSAGE_ID);
        if (message_id == null || message_id.equals(""))
        {
            if (mType.equals(RETRIEVE_MESSAGE) || mType.equals(DELETE_MESSAGE))
            {
                anyException = true;
                status = OscStatusCode.OSC_BAD_REQUEST;
                statusMsg = OscStatusCode.getMessage(OscStatusCode.OSC_BAD_REQUEST);
                logger.error(statusMsg + " for " + actionName + ":" + mType +
                        "(message_id = " + message_id + ")");
                return;
            }

            message_id = "-1";
        } 

        // Parameters for STORE_MESSAGE
        recv_info = null;
        message_type = req.getParameter(ORP.MESSAGES_MESSAGE_TYPE);
        reply_id = req.getParameter(ORP.MESSAGES_REPLY_ID);
        ip_address = req.getParameter(ORP.MESSAGES_IP_ADDRESS);
        content_type = req.getParameter(ORP.MESSAGES_CONTENT_TYPE);
        to = req.getParameterValues(ORP.MESSAGES_TO);
        cc = req.getParameterValues(ORP.MESSAGES_CC);
        bcc = req.getParameterValues(ORP.MESSAGES_BCC);
        subject = req.getParameter(ORP.MESSAGES_SUBJECT);
        message = req.getParameter(ORP.MESSAGES_MESSAGE);
        content_length = req.getParameter(ORP.MESSAGES_CONTENT_LENGTH);
        send_date = "";
        if (content_length == null || content_length.equals(""))
            content_length = "0";

        if (mType.equals(STORE_MESSAGE) && ((to == null || to.length == 0) &&
                                            (cc == null || cc.length == 0) &&
                                            (bcc == null || bcc.length == 0)))
        {
            anyException = true;
            status = OscStatusCode.OSC_BAD_REQUEST;
            statusMsg = OscStatusCode.getMessage(OscStatusCode.OSC_BAD_REQUEST);
            logger.error(statusMsg + " for " + actionName + ":" + mType +
                    "(Recipients list is empty)");
            return;
        } 

        num_rows = req.getParameter(ORP.MESSAGES_NUM_ROWS);
        skip_rows = req.getParameter(ORP.MESSAGES_SKIP_ROWS);

        if (num_rows == null || num_rows.equals(""))
            num_rows = "-1";
        if (skip_rows == null || skip_rows.equals(""))
            skip_rows = "-1";

        myMembershipID = 0;
 	myMemberID = 0;
        myQuotaRecvMessageCount = -1;
        myQuotaSendMessageCount = -1;
 	myQuotaSendMessageLength = -1;

        totalCount = -1;
    }


    private void getReceiverInfo(OscDbConnection conn) 
	throws Exception   
    {
        int total_receivers = 0;
        if (to != null && to.length != 0)
            total_receivers = total_receivers + to.length;
        if (cc != null && cc.length != 0)
            total_receivers = total_receivers + cc.length;
        if (bcc != null && bcc.length != 0)
            total_receivers = total_receivers + bcc.length;

        // 0 - recipient_type {TO, CC, BCC}
        // 1 - recipient pseudonym
        // 2 - recipient membership_id 
        // 3 - recipient member_id
        // 4 - result code
        // 5 - result message
        recv_info = new String[total_receivers][6];
        int i = 0;
        int j = 0;
        int k = 0;

        for (i = 0; to!=null && i < to.length; i++)
        {
            recv_info[i][0] = "TO";
            recv_info[i][1] = to[i];
        }

        for (j = 0; cc!=null && j < cc.length; j++)
        {
            recv_info[i+j][0] = "CC";
            recv_info[i+j][1] = cc[j];
        }

        for (k = 0; bcc!=null && k < bcc.length; k++)
        {
            recv_info[i+j+k][0] = "BCC";
            recv_info[i+j+k][1] = bcc[k];
        }

        recv_info = OscDmlMessages.getReceiverInfo(conn, recv_info, QUOTA_RECV_MESSAGE_COUNT);
        return;
    }

    // --------------------------------------------
    // Specialization of abstract methods
    // --------------------------------------------

    public boolean requiresLogin() {return needLogin[tp.intValue()];}

    public String jspFwdUrl() 
    {
        return null;
    }

    public void encodeXml(PrintWriter out)
    {
        // Encodes the final results
        //
        out.println("<" + ORP.RPC_RESULT_TAG + ">");

        context.encodeXml_TNL(out, ORP.REQ_ACTION, rule.getActionNm());
        context.encodeXml_TNL(out, ORP.MESSAGES_ACTION_TYPE, mType);

        context.encodeXml_TNL(out, STATUS_TAG, status);
        context.encodeXml_TNL(out, STATUS_MSG_TAG, statusMsg);

        if (!anyException)
        {
            if (mType.equals(STORE_MESSAGE))
            {
                context.encodeXml_TNL(out, MESSAGE_ID_TAG, message_id);
                context.encodeXml_TNL(out, QUOTA_SEND_COUNT_TAG, String.valueOf(myQuotaSendMessageCount));
                context.encodeXml_TNL(out, QUOTA_SEND_LENGTH_TAG, String.valueOf(myQuotaSendMessageLength));
           
                if (recv_info != null)
                {
                    for (int i = 0; i < recv_info.length; i++)
                    {
                        out.println("\t<" + RECIPIENT_TAG + ">");
                        context.encodeXml_TTNL(out, RESULT_TAG, recv_info[i][4]);

                        if (recv_info[i][2] != null)
                        {
                            context.encodeXml_TTNL(out, ORP.MEMBERSHIP_ID, recv_info[i][2]);
                            context.encodeXml_TTNL(out, ORP.MEMBER_ID, recv_info[i][3]);
                        } else {
                            context.encodeXml_TTNL(out, ORP.MEMBERSHIP_ID, "");
                            context.encodeXml_TTNL(out, ORP.MEMBER_ID, "");
                        }

                        out.println("\t</" + RECIPIENT_TAG + ">");
                    }
                }

            } else if (mType.equals(GET_INCOMING_HEADERS)) {
                context.encodeXml_TNL(out, QUOTA_RECV_COUNT_TAG, String.valueOf(myQuotaRecvMessageCount));
                context.encodeXml_TNL(out, TOTAL_COUNT_TAG, String.valueOf(totalCount));

            } else if (mType.equals(DELETE_MESSAGE)) {
                context.encodeXml_TNL(out, MESSAGE_ID_TAG, message_id);
                context.encodeXml_TNL(out, QUOTA_RECV_COUNT_TAG, String.valueOf(myQuotaRecvMessageCount));

            } else if (mType.equals(CHECK_QUOTA)) {
                context.encodeXml_TNL(out, QUOTA_RECV_COUNT_TAG, String.valueOf(myQuotaRecvMessageCount));
                context.encodeXml_TNL(out, QUOTA_SEND_COUNT_TAG, String.valueOf(myQuotaSendMessageCount));
                context.encodeXml_TNL(out, QUOTA_SEND_LENGTH_TAG, String.valueOf(myQuotaSendMessageLength));
            }
        }

        if (!getIsError() && resultXML!=null && !resultXML.equals(""))
            out.println(resultXML);

        if (context.getRpcAppendParams().equals("ON"))
        {
            out.println("\t<" + ORP.RPC_PARAMS_TAG + ">");
            context.encodeXml_TTNL(out, ORP.MEMBERSHIP_ID, String.valueOf(myMembershipID));
            context.encodeXml_TTNL(out, ORP.MEMBER_ID, String.valueOf(myMemberID));

            if (mType.equals(RETRIEVE_MESSAGE) || mType.equals(DELETE_MESSAGE))
	        context.encodeXml_TTNL(out, ORP.MESSAGES_MESSAGE_ID, message_id);

            else if (mType.equals(GET_INCOMING_HEADERS)) {
                context.encodeXml_TTNL(out, ORP.MESSAGES_NUM_ROWS, num_rows);
                context.encodeXml_TTNL(out, ORP.MESSAGES_SKIP_ROWS, skip_rows);

            } else if (mType.equals(STORE_MESSAGE)) {
                context.encodeXml_TTNL(out, ORP.MESSAGES_MESSAGE_TYPE, message_type);
                context.encodeXml_TTNL(out, ORP.MESSAGES_REPLY_ID, reply_id);
                context.encodeXml_TTNL(out, ORP.MESSAGES_IP_ADDRESS, ip_address);
                context.encodeXml_TTNL(out, ORP.MESSAGES_CONTENT_TYPE, content_type);
                context.encodeXml_TTNL(out, ORP.MESSAGES_CONTENT_LENGTH, content_length);

                for (int i = 0; to!=null && i < to.length; i++)
                    context.encodeXml_TTNL(out, ORP.MESSAGES_TO, to[i]);
                for (int j = 0; cc!=null && j < cc.length; j++)
                    context.encodeXml_TTNL(out, ORP.MESSAGES_CC, cc[j]);
                for (int k = 0; bcc!=null && k < bcc.length; k++)
                    context.encodeXml_TTNL(out, ORP.MESSAGES_BCC, bcc[k]);
                
                context.encodeXml_TTNL(out, ORP.MESSAGES_SEND_DATE, send_date);
                context.encodeXml_TTNL(out, ORP.MESSAGES_SUBJECT, subject);
                context.encodeXml_TTNL(out, ORP.MESSAGES_MESSAGE, message);

            }

            out.println("\t</" + ORP.RPC_PARAMS_TAG + ">");
        }

        out.println("</" + ORP.RPC_RESULT_TAG + ">");
    } // encodeXml

    public void serveRequest(OscLoginSession loginSession)
        throws InvalidRequestException
    {
        if (!getIsError()) {
            myMembershipID = loginSession.getMembershipID();
            myMemberID = loginSession.getMemberID();

            int numrows = Integer.parseInt(num_rows);
            int skiprows = Integer.parseInt(skip_rows);

            OscDbConnection conn = null;

            try {
                conn = context.getDB(isConnectionReadOnly[tp.intValue()]);

                if (mType.equals(STORE_MESSAGE)) 
                {
                    myQuotaSendMessageCount = QUOTA_SEND_MESSAGE_COUNT -
                                              OscDmlMessages.getSenderMessageCount(conn,
                                                  String.valueOf(myMembershipID),
                                                  String.valueOf(myMemberID));
                    myQuotaSendMessageLength = QUOTA_SEND_MESSAGE_LENGTH -
                                               OscDmlMessages.getSenderMessageLength(conn,
                                                  String.valueOf(myMembershipID),
                                                  String.valueOf(myMemberID));
                    if (myQuotaSendMessageCount > 0 && 
                        myQuotaSendMessageLength >= Long.parseLong(content_length))
                    {
                        getReceiverInfo(conn);
                        
                        send_date = context.getCurrentDate();
                        message_id = OscDmlMessages.storeMessage(conn, message_type, 
                                         String.valueOf(myMembershipID),
                                         String.valueOf(myMemberID),
                                         reply_id, send_date, subject, message, ip_address,
                                         content_type, content_length, recv_info);

                        if (!message_id.equals("-1"))
                        {
                            conn.commit();
                            myQuotaSendMessageCount = myQuotaSendMessageCount - 1;
                            myQuotaSendMessageLength = myQuotaSendMessageLength - Long.parseLong(content_length);

                        } else {
                            conn.rollback();
                            status = OscStatusCode.VNG_STORE_MESSAGE_ERROR;
                            statusMsg = OscStatusCode.getMessage(OscStatusCode.VNG_STORE_MESSAGE_ERROR);
                            logger.error(statusMsg + " for " + actionName);
                        }

                    } else {
                        if (myQuotaSendMessageCount <= 0)
                        {
                            status = OscStatusCode.VNG_QUOTA_SEND_COUNT_ERROR;
                            statusMsg = OscStatusCode.getMessage(OscStatusCode.VNG_QUOTA_SEND_COUNT_ERROR);
                        } else {
                            status = OscStatusCode.VNG_QUOTA_SEND_LENGTH_ERROR;
                            statusMsg = OscStatusCode.getMessage(OscStatusCode.VNG_QUOTA_SEND_LENGTH_ERROR);
                        }
                    }

                } else if (mType.equals(GET_INCOMING_HEADERS)) {
                    myQuotaRecvMessageCount = QUOTA_RECV_MESSAGE_COUNT - 
                                              OscDmlMessages.getRecipientMessageCount(conn,
		   		                  String.valueOf(myMembershipID), 
				                  String.valueOf(myMemberID));
                    totalCount = OscDmlMessages.countIncomingHeadersList(conn,
                                                                         String.valueOf(myMembershipID),
                                                                         String.valueOf(myMemberID));
                    resultXML = OscDmlMessages.getIncomingHeadersList(conn, 
							              String.valueOf(myMembershipID), 
								      String.valueOf(myMemberID),
                                                                      numrows, skiprows);

                } else if (mType.equals(RETRIEVE_MESSAGE)) {
                    myQuotaRecvMessageCount = QUOTA_RECV_MESSAGE_COUNT - 
                                              OscDmlMessages.getRecipientMessageCount(conn,
  				                  String.valueOf(myMembershipID), 
				                  String.valueOf(myMemberID));
                    resultXML = OscDmlMessages.getMessageByRecipient(conn, 
	                            String.valueOf(myMembershipID), 
			            String.valueOf(myMemberID),
                                    message_id);

                } else if (mType.equals(DELETE_MESSAGE)) {
                    int count = OscDmlMessages.deleteMessageByRecipient(conn, 
		 		    String.valueOf(myMembershipID), 
				    String.valueOf(myMemberID),
                                    message_id);
                    myQuotaRecvMessageCount = QUOTA_RECV_MESSAGE_COUNT - 
                                              OscDmlMessages.getRecipientMessageCount(conn,
  				                  String.valueOf(myMembershipID), 
					          String.valueOf(myMemberID));
                    if (count != 1)
                    {
                        conn.rollback();
                        status = OscStatusCode.VNG_DELETE_MESSAGE_ERROR;
                        statusMsg = OscStatusCode.getMessage(OscStatusCode.VNG_DELETE_MESSAGE_ERROR);
                    } else
                        conn.commit();

                } else if (mType.equals(CHECK_QUOTA)) {
                    myQuotaRecvMessageCount = QUOTA_RECV_MESSAGE_COUNT - 
                                              OscDmlMessages.getRecipientMessageCount(conn,
 					          String.valueOf(myMembershipID), 
					          String.valueOf(myMemberID));
                    myQuotaSendMessageCount = QUOTA_SEND_MESSAGE_COUNT - 
                                              OscDmlMessages.getSenderMessageCount(conn,
   				  	          String.valueOf(myMembershipID), 
					          String.valueOf(myMemberID));
                    myQuotaSendMessageLength = QUOTA_SEND_MESSAGE_LENGTH - 
                                               OscDmlMessages.getSenderMessageLength(conn,
  			 		           String.valueOf(myMembershipID), 
					           String.valueOf(myMemberID));
                }
            }
            catch (Exception e) {
                anyException = true;
                status = OscStatusCode.VNG_DB_ERROR;
                statusMsg = OscStatusCode.getMessage(OscStatusCode.VNG_DB_ERROR) +
                       ": " + e.getMessage();

                e.printStackTrace();
                logger.error(statusMsg + " for " + actionName + ":" + mType);
            }
            finally {
                if (conn != null)
                    conn.close();
            }
        }
    }

    public String toString() {return null;}

    public boolean getIsError()
    {
        return !status.equals(OscStatusCode.SC_OK);
    }

    public String getErrorMsg()
    {
        return statusMsg;
    }

} // class OscResultMessages

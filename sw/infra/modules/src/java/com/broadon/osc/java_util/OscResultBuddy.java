package com.broadon.osc.java_util;

import java.io.*;
import javax.servlet.http.*;

import com.broadon.status.StatusCode;
import com.broadon.exception.InvalidRequestException;

/** The class used to serve VNG server request for any action on/for a buddy
 *  and returns the appropriate document and header back to the VNG server.
 */

public class OscResultBuddy extends OscResultBean
{
    /** Rules for creating a Buddy result object
     */
    public static class Rule extends OscResultRule {
        
        public static final String BUDDY = "buddy";

        public Rule(String nm, OscContext c, OscXs x)
        {
            super(nm, c, x);
        }
        public boolean supports(ServerType t)
        {
            return t == ServerType.RPC || t == ServerType.HTM;
        }
        public OscResultBean create(HttpServletRequest req,  
                                    HttpServletResponse res)
            throws InvalidRequestException
        {
            return new OscResultBuddy(req, res, this);
        }
    } // class Rule


    // --------------------------------------------
    // Invariant values
    // --------------------------------------------

    private static final String BUDDY_LIST = "list";
    private static final String REV_BUDDY_LIST = "reverse";
    private static final String INVITE_BUDDY = "invite";
    private static final String ACCEPT_BUDDY = "accept";
    private static final String REJECT_BUDDY = "reject";
    private static final String REMOVE_BUDDY = "remove";
    private static final String BLOCK_BUDDY = "block";
    private static final String UNBLOCK_BUDDY = "unblock";
    private static final String UPDATE_BUDDY = "update";
    private static final String QUERY_BUDDY = "query";
    private static final String TEST_FSM = "testfsm";

    private static final String INVITE = "INVITE_BUD";
    private static final String ACCEPT = "ACCEPT_INV";
    private static final String REJECT = "REJECT_INV";
    private static final String REMOVE = "REMOVE_BUD";
    private static final String BLOCK = "BLOCK_BUD";
    private static final String UNBLOCK = "UNBLOCK_BUD";

    public static final String TOTAL_COUNT_TAG = "total_count";

    private static final String BUDDY_JSP = "BuddyList.jsp";
    private static final String BUDDY_REVERSE_JSP = "BuddyReverseList.jsp";

    // An enumeration of the types ofbuddy actions we support
    //
    public static class ListType {
        private int enumeration;
        private ListType(int e) {enumeration = e;}
        public int intValue() {return enumeration;}
        public static final ListType LIST = new ListType(0);
        public static final ListType REVERSE = new ListType(1);
        public static final ListType QUERY = new ListType(2);
        public static final ListType INVITE = new ListType(3);
        public static final ListType ACCEPT = new ListType(4);
        public static final ListType REJECT = new ListType(5);
        public static final ListType REMOVE = new ListType(6);
        public static final ListType BLOCK = new ListType(7);
        public static final ListType UNBLOCK = new ListType(8);
        public static final ListType UPDATE = new ListType(9);
        public static final ListType TEST_FSM = new ListType(10);
    }

    private static final boolean[] needLogin = {
        true,
        true,
        true,
        true,
        true,
        true,
        true,
        true,
        true,
        true,
	true
    };

    private static final boolean[] isConnectionReadOnly = {
        true,
        true,
        true,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false
    };

    // Arrays of the above indexed by Rule.TitleType.toInt() values
    //
    private static final String[] jspPage = {
        BUDDY_JSP,
        BUDDY_REVERSE_JSP,
        BUDDY_JSP,
        BUDDY_JSP,
        BUDDY_JSP,
        BUDDY_JSP,
        BUDDY_JSP,
        BUDDY_JSP,
        BUDDY_JSP,
        BUDDY_JSP,
        BUDDY_JSP
    };

    // --------------------------------------------
    // Member variables
    // --------------------------------------------

    private final OscResultBuddy.Rule	 	rule;
    private final OscContext          		context;
    private final OscLogger          		logger;
    private final String                        actionName;
    private String                              resultXML;
    private String                              listXML;
    private String				status;
    private String				statusMsg;

    private String				bType;

    private String				buddy_membership_id;
    private String				buddy_member_id;
    private String				buddy_state_action;
    private String				buddy_info;
    private String                              buddy_pseudonym;
    private String                              num_rows;
    private String                              skip_rows;
    private String                              index;

    private int                                 totalCount;

    private long  				myMembershipID;
    private long  				myMemberID;
    private long  				buddyMembershipID;
    private long  				buddyMemberID;

    private boolean 				anyException;
    private ListType 				tp;

    // ----------------------------------------------------------
    // Hidden constructors and helper methods (all constructors should
    // be hidden for result objects, since they are constructed by the
    // Rule class)
    // ---------------------------------------------------------

    private OscResultBuddy(HttpServletRequest  req,  
                           HttpServletResponse res,
                           OscResultBuddy.Rule rule)
        throws InvalidRequestException
    {
        this.rule = rule;
        context = rule.getContext();
        logger = context.getLogger();
        actionName = rule.getActionNm();

        resultXML = "";
        listXML = "";
	status = OscStatusCode.SC_OK;
	statusMsg = StatusCode.getMessage(status);

        anyException = false;
	buddy_state_action = "";
        num_rows = req.getParameter(ORP.BUDDY_NUM_ROWS);
        skip_rows = req.getParameter(ORP.BUDDY_SKIP_ROWS);
        index = req.getParameter(ORP.BUDDY_INDEX);

        bType = req.getParameter(ORP.BUDDY_ACTION_TYPE);
        if (bType == null)
        {
            bType = BUDDY_LIST;
            context.getLogger().error("Invalid buddy sub-action ("  + bType +
                                      ") in " + getClass().getName() +
                                      " using " + BUDDY_LIST);
        }
   
	if (bType.equals(BUDDY_LIST)) tp = ListType.LIST;
        else if (bType.equals(REV_BUDDY_LIST)) tp = ListType.REVERSE;
        else if (bType.equals(INVITE_BUDDY)) {tp = ListType.INVITE; buddy_state_action = INVITE;}
        else if (bType.equals(ACCEPT_BUDDY)) {tp = ListType.ACCEPT; buddy_state_action = ACCEPT;}
        else if (bType.equals(REJECT_BUDDY)) {tp = ListType.REJECT; buddy_state_action = REJECT;}
        else if (bType.equals(REMOVE_BUDDY)) {tp = ListType.REMOVE; buddy_state_action = REMOVE;}
        else if (bType.equals(BLOCK_BUDDY)) {tp = ListType.BLOCK; buddy_state_action = BLOCK;}
        else if (bType.equals(UNBLOCK_BUDDY)) {tp = ListType.UNBLOCK; buddy_state_action = UNBLOCK;}
        else if (bType.equals(TEST_FSM)) { tp = ListType.TEST_FSM; buddy_state_action = req.getParameter("test_action");}
        else if (bType.equals(UPDATE_BUDDY)) tp = ListType.UPDATE;
        else {
            context.getLogger().error("Invalid buddy sub-action ("  + bType +
                                      ") in " + getClass().getName() +
                                      " using " + BUDDY_LIST);
            tp = ListType.LIST;
        }

	buddy_membership_id = req.getParameter(ORP.BUDDY_MEMBERSHIP_ID);
	buddy_member_id = req.getParameter(ORP.BUDDY_MEMBER_ID);
	buddy_info = req.getParameter(ORP.BUDDY_INFO);
        buddy_pseudonym = req.getParameter(ORP.BUDDY_PSEUDONYM);

        myMembershipID = 0;
 	myMemberID = 0;
  	buddyMembershipID = 0;
	buddyMemberID = 0;

        if (buddy_membership_id != null && !buddy_membership_id.equals(""))
  	    buddyMembershipID = Long.parseLong(buddy_membership_id);
        if (buddy_member_id != null && !buddy_member_id.equals(""))
	    buddyMemberID = Long.parseLong(buddy_member_id);

        if (num_rows == null || num_rows.equals(""))
            num_rows = "-1";
        if (skip_rows == null || skip_rows.equals(""))
            skip_rows = "-1";

        if (buddy_pseudonym == null || buddy_pseudonym.equals(""))
        {
            if (bType.equals(INVITE_BUDDY))
            {
                anyException = true;
                status = OscStatusCode.OSC_BAD_REQUEST;
                statusMsg = OscStatusCode.getMessage(OscStatusCode.OSC_BAD_REQUEST);
                logger.error(statusMsg + " for " + actionName + ":" + bType +
                        "(buddy_pseudonym = " + buddy_pseudonym + ")");
                return;
            }
        } 

        if ((buddy_membership_id == null || buddy_membership_id.equals("") ||
             buddy_member_id == null || buddy_member_id.equals("")) &&
            (buddy_pseudonym == null || buddy_pseudonym.equals("")))
        {
            if (bType.equals(BLOCK_BUDDY))
            {
                anyException = true;
                status = OscStatusCode.OSC_BAD_REQUEST;
                statusMsg = OscStatusCode.getMessage(OscStatusCode.OSC_BAD_REQUEST);
                logger.error(statusMsg + " for " + actionName + ":" + bType +
                        "(buddy_member_id = " + buddy_member_id + ", buddy_membership_id = " + 
                        buddy_membership_id + ", buddy_pseudonym = " + buddy_pseudonym + ")");
                return;
            }
        } 

        if (buddy_membership_id == null || buddy_membership_id.equals("") ||
            buddy_member_id == null || buddy_member_id.equals(""))
        {
            if (bType.equals(QUERY_BUDDY) ||
                bType.equals(UPDATE_BUDDY) ||
                bType.equals(ACCEPT_BUDDY) || 
                bType.equals(REJECT_BUDDY) || 
                bType.equals(REMOVE_BUDDY) || 
                bType.equals(UNBLOCK_BUDDY))
            {
                anyException = true;
                status = OscStatusCode.OSC_BAD_REQUEST;
                statusMsg = OscStatusCode.getMessage(OscStatusCode.OSC_BAD_REQUEST);
                logger.error(statusMsg + " for " + actionName + ":" + bType +
                        "(buddy_member_id = " + buddy_member_id + ", buddy_membership_id = " + 
                        buddy_membership_id + ")");
                return;
            }
        }
    }

    // --------------------------------------------
    // Specialization of abstract methods
    // --------------------------------------------

    public boolean requiresLogin() {return needLogin[tp.intValue()];}

    public String jspFwdUrl() 
    {
        return jspPage[tp.intValue()];
    }

    public void encodeXml(PrintWriter out)
    {
        // Encodes the final results
        //
        out.println("<" + ORP.RPC_RESULT_TAG + ">");

        context.encodeXml_TNL(out, ORP.REQ_ACTION, rule.getActionNm());

        if (!(bType.equals(INVITE_BUDDY) || 
              bType.equals(ACCEPT_BUDDY) || 
              bType.equals(REJECT_BUDDY) || 
              bType.equals(REMOVE_BUDDY) || 
              bType.equals(BLOCK_BUDDY) || 
              bType.equals(UNBLOCK_BUDDY)) || anyException)
        {
            context.encodeXml_TNL(out, STATUS_TAG, status);
            context.encodeXml_TNL(out, STATUS_MSG_TAG, statusMsg);
            if (bType.equals(BUDDY_LIST))
                context.encodeXml_TNL(out, TOTAL_COUNT_TAG, String.valueOf(totalCount));
        }

        if (!getIsError() && resultXML!=null && !resultXML.equals(""))
            out.println(resultXML);

        context.encodeXml_TNL(out, ORP.BUDDY_ACTION_TYPE, bType);

        if (context.getRpcAppendParams().equals("ON"))
        {
            out.println("\t<" + ORP.RPC_PARAMS_TAG + ">");
            if (bType.equals(QUERY_BUDDY) || 
                bType.equals(ACCEPT_BUDDY) || 
                bType.equals(REJECT_BUDDY) || 
                bType.equals(REMOVE_BUDDY) || 
                bType.equals(UNBLOCK_BUDDY))
            {
	        context.encodeXml_TTNL(out, ORP.BUDDY_MEMBERSHIP_ID, buddy_membership_id);
                context.encodeXml_TTNL(out, ORP.BUDDY_MEMBER_ID, buddy_member_id);

            } else if (bType.equals(INVITE_BUDDY)) {
                context.encodeXml_TTNL(out, ORP.BUDDY_PSEUDONYM, buddy_pseudonym);

            } else if (bType.equals(BLOCK_BUDDY)) {
	        context.encodeXml_TTNL(out, ORP.BUDDY_MEMBERSHIP_ID, buddy_membership_id);
                context.encodeXml_TTNL(out, ORP.BUDDY_MEMBER_ID, buddy_member_id);
                context.encodeXml_TTNL(out, ORP.BUDDY_PSEUDONYM, buddy_pseudonym);

            } else if (bType.equals(BUDDY_LIST)) {
                context.encodeXml_TTNL(out, ORP.BUDDY_NUM_ROWS, num_rows);
                context.encodeXml_TTNL(out, ORP.BUDDY_SKIP_ROWS, skip_rows);

            } else if (bType.equals(UPDATE_BUDDY)) {
	        context.encodeXml_TTNL(out, ORP.BUDDY_MEMBERSHIP_ID, buddy_membership_id);
                context.encodeXml_TTNL(out, ORP.BUDDY_MEMBER_ID, buddy_member_id);
                context.encodeXml_TTNL(out, ORP.BUDDY_INFO, buddy_info);
            }

            out.println("\t</" + ORP.RPC_PARAMS_TAG + ">");
        }

        out.println("</" + ORP.RPC_RESULT_TAG + ">");
    } // encodeXml

    public void serveRequest(OscLoginSession loginSession)
        throws InvalidRequestException
    {
        if (!getIsError()) {
            myMembershipID = loginSession.getMembershipID();
            myMemberID = loginSession.getMemberID();

            int numrows = Integer.parseInt(num_rows);
            int skiprows = Integer.parseInt(skip_rows);

            OscDbConnection conn = null;

            try {
                conn = context.getDB(isConnectionReadOnly[tp.intValue()]);

                if (bType.equals(BUDDY_LIST)) 
                {
                    totalCount = OscDmlBuddy.countBuddyList(conn, 
                                                            String.valueOf(myMembershipID),
                                                            String.valueOf(myMemberID));
                    resultXML = OscDmlBuddy.getBuddyList(conn, 
							 String.valueOf(myMembershipID), 
							 String.valueOf(myMemberID),
                                                         numrows, skiprows);
                    listXML = resultXML;

                } else if (bType.equals(REV_BUDDY_LIST)) {
                    resultXML = OscDmlBuddy.getReverseBuddyList(conn, 
							        String.valueOf(myMembershipID), 
								String.valueOf(myMemberID));
                    listXML = resultXML;

                } else if (bType.equals(QUERY_BUDDY)) {
                    resultXML = OscDmlBuddy.getMemberInfo(conn, 
							  buddy_membership_id,
                                                          buddy_member_id);
                    listXML = resultXML;

                } else if (bType.equals(INVITE_BUDDY) ||
                         bType.equals(ACCEPT_BUDDY) || 
                         bType.equals(REJECT_BUDDY) || 
                         bType.equals(REMOVE_BUDDY) || 
                         bType.equals(BLOCK_BUDDY) || 
                         bType.equals(UNBLOCK_BUDDY)) 
                {
                    if (bType.equals(INVITE_BUDDY) || bType.equals(BLOCK_BUDDY))
                    {
                        if (buddy_pseudonym != null && !buddy_pseudonym.equals(""))
                        {
                            String memberInfo = OscDmlBuddy.getMemberInfo(conn, buddy_pseudonym);
                            if (memberInfo != null && memberInfo.indexOf("<ROWSET/>") >= 0)
                            {
                                anyException = true;
                                status = OscStatusCode.VNG_MEMBER_NOT_FOUND_ERROR;
                                statusMsg = OscStatusCode.getMessage(OscStatusCode.VNG_MEMBER_NOT_FOUND_ERROR);
 
                                logger.error(statusMsg + " for " + actionName + ":" + bType);

                            } else if (memberInfo != null) {
                                buddy_membership_id = memberInfo.substring(memberInfo.indexOf("<MEMBERSHIP_ID>")+15,
                                                                           memberInfo.indexOf("</MEMBERSHIP_ID>"));
                                buddy_member_id = memberInfo.substring(memberInfo.indexOf("<MEMBER_ID>")+11,
                                                                           memberInfo.indexOf("</MEMBER_ID>"));
                                                         
		    	        if (buddy_membership_id != null && !buddy_membership_id.equals(""))
		                    buddyMembershipID = Long.parseLong(buddy_membership_id);
   	  	                if (buddy_member_id != null && !buddy_member_id.equals(""))
		                    buddyMemberID = Long.parseLong(buddy_member_id);
                            }
                        }  
                    }

                    if (!anyException)
                    {
                        OscBuddyStateMachine.init(context);
                        resultXML = OscBuddyStateMachine.start(conn, myMembershipID, myMemberID,
                                        buddyMembershipID, buddyMemberID, buddy_state_action);
                    }

                    listXML = OscDmlBuddy.getBuddyList(conn, 
	   			    	               String.valueOf(myMembershipID), 
						       String.valueOf(myMemberID),
                                                       numrows, skiprows);

                } else if (bType.equals(TEST_FSM)) {
                    if (buddy_state_action.equalsIgnoreCase("all")) {
                        resultXML = OscBuddyStateMachine.testAllBSM(context);
                    }
                    else {
                        resultXML = OscBuddyStateMachine.testAnyBSM(context, buddy_state_action);
                    }

                } else if (bType.equals(UPDATE_BUDDY)) {
                    int rows = OscDmlBuddy.updateBuddyList(conn, 
					String.valueOf(myMembershipID), String.valueOf(myMemberID), 
					buddy_membership_id, buddy_member_id, buddy_info);
                    if (rows!=1)
                    {
                        status = OscStatusCode.VNG_DB_UPDATE_ERROR;
                        statusMsg = OscStatusCode.getMessage(OscStatusCode.VNG_DB_UPDATE_ERROR);

                        logger.error(statusMsg + " for " + actionName + ":" + bType);
                    }
                    conn.commit();
                    listXML = OscDmlBuddy.getBuddyList(conn, 
					  	       String.valueOf(myMembershipID), 
						       String.valueOf(myMemberID),
                                                       numrows, skiprows);
                }
            }
            catch (Exception e) {
                anyException = true;
                status = OscStatusCode.VNG_DB_ERROR;
                statusMsg = OscStatusCode.getMessage(OscStatusCode.VNG_DB_ERROR) +
                       ": " + e.getMessage();

                e.printStackTrace();
                logger.error(statusMsg + " for " + actionName + ":" + bType);
            }
            finally {
                if (conn != null)
                    conn.close();
            }
        }
    }

    public String toString() {return null;}

    public boolean getIsError()
    {
        return !status.equals(OscStatusCode.SC_OK);
    }

    public String getErrorMsg()
    {
        return statusMsg;
    }

    public String transformXML(String xmlStr, String xslFile, String locale)
    { 
        return context.transformXML(xmlStr,xslFile,locale); 
    }

    public String getXML() 
    {
       return listXML;
    }

    public String getIndex() 
    {
       return index;
    }

} // class OscResultBuddy

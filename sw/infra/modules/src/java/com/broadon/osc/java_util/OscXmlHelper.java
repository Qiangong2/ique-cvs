package com.broadon.osc.java_util;

import java.io.*;
import java.util.*;

import org.apache.xerces.parsers.DOMParser;
import org.xml.sax.SAXException;
import org.xml.sax.InputSource;
import org.w3c.dom.*;

/**
 * 
 */
public class OscXmlHelper
{
    private String rowsetTag;
    private String rowTag;
    private String[] rowChild;

    private Vector[] vData;

    private final static String DEFAULT_ROWSET_TAG = "ROWSET";
    private final static String DEFAULT_ROW_TAG = "ROW";

    public final static String XML_DATA = "xml";
    public final static String NAME_VALUE_DATA = "nv";

    public OscXmlHelper()
    {
        rowsetTag = DEFAULT_ROWSET_TAG;
        rowTag = DEFAULT_ROW_TAG;
        rowChild = null;

        vData = null;
    }

    public void setRowsetTag(String rsTag) {rowsetTag = rsTag.toUpperCase();}
    public void setRowTag(String rTag) {rowTag = rTag.toUpperCase();}
    public void setRowChild(String[] rChild) 
    {
        for (int i = 0; i < rChild.length; i++)
            rChild[i] = rChild[i].toUpperCase();
        rowChild = rChild;
    }

    public String getRowsetTag() {return rowsetTag;}
    public String getRowTag() {return rowTag;}
    public String[] getRowChild() {return rowChild;}

    public Vector[] getRequestData(String inData, String form)
        throws IOException, SAXException
    {
        if (rowChild != null)
        {
            if (form!=null && form.equals(XML_DATA))
 	        ProcessXMLData(inData);
        } else {
            throw new IOException("Row child tags not set -- must call setRowChild(String[])");
        }

        return vData;
    } 

    private void ProcessXMLData(String xml)
        throws IOException, SAXException
    {
        DOMParser parser = new DOMParser();
        InputSource is = new InputSource(new StringReader(xml));

        parser.parse(is);

        Document document = parser.getDocument();
        Element RootElement = document.getDocumentElement();
        NodeList children = RootElement.getChildNodes();

        if (children != null)
        {
            vData = new Vector[rowChild.length];
            for (int p = 0; p < rowChild.length; p++)
                vData[p] = new Vector(children.getLength());

            for (int i = 0; i < children.getLength(); i++)
            {
                if (children.item(i).getNodeName().equals(rowTag) && 
                    children.item(i).getNodeType() == Node.ELEMENT_NODE)
                {
                    if (children.item(i).getChildNodes().getLength() == rowChild.length)
                    { 
                        int cMatch = 0;
                        for (int j = 0; j < children.item(i).getChildNodes().getLength(); j++) 
                        {
                            Node local = children.item(i).getChildNodes().item(j);
                         
                            for (int k = 0; k < rowChild.length; k++) 
                            {
                                if (local.getNodeName().toUpperCase().equals(rowChild[k])) 
                                {
                                    vData[k].add(GetElementData(local));
                                    cMatch = cMatch + 1;
                                    break;
                                }
                            }
                        }

                        if (cMatch != rowChild.length)
                        {
  		            throw new IOException("Duplicate or Missing row child tags");
                        }
                    } else {
 		        throw new IOException("Unexpected number of row child tags");
                    }
                } else {
		    throw new IOException("Inconsistent row tag, expected \"" + rowTag +
                                      "\" found \""+children.item(i).getNodeName().toUpperCase()+"\"");
                }
            }
        }
    }

   /**************************************************************************
    * GetElementData function returns the value of the child node
    **************************************************************************/
    private String GetElementData(Node parentNode)
    {
        if (parentNode.getFirstChild() != null) 
        {
            int childType = parentNode.getFirstChild().getNodeType();

            if (childType == Node.TEXT_NODE)
            {
                if (parentNode.getFirstChild().getNodeValue() != null)
                    return parentNode.getFirstChild().getNodeValue();
            }
        }
        return "";
    }

} // OscXmlHelper

package com.broadon.osc.java_util;

import java.sql.*;
import java.util.ArrayList;

/**
 * Data manipulation library functions defined here pertain to game scores
 */
public class OscDmlGameScore
{
    /** Define all SQL statements
     */

    private static final String TABLE_GAME_SCORES = "VNG_ONLINE_GAME_SCORES";
    
    private static final String TABLE_GAME_SCORE_OBJECTS = "VNG_ONLINE_GAME_SCORE_OBJECTS";

    private static final String GET_SCORE =
                "SELECT game_id, score_id, membership_id, member_id, score_time, title_id, " +
                "score, score_info, score_object_size FROM osc." + TABLE_GAME_SCORES;

    private static final String UPDATE_SCORE_OBJECT =
    	"UPDATE osc." + TABLE_GAME_SCORE_OBJECTS + " SET score_object = ?, score_object_siz" +
    	"e = ? WHERE score_object_id = ?";
    
    private static final String QUERY_SCORE_OBJECT_ID = 
    	"SELECT distinct score_object_id FROM osc." + TABLE_GAME_SCORES + " WHERE bb_id=? A" +
    	"ND slot_id=? AND title_id=? AND game_id=? AND score_id=?";
    
    private static final String DELETE_GAME_SCORE = 
    	"DELETE FROM osc." + TABLE_GAME_SCORES + " WHERE score_object_id=?";
    
    private static final String DELETE_GAME_SCORE_OBJECT = 
    	"DELETE FROM osc." + TABLE_GAME_SCORE_OBJECTS + " WHERE score_object_id=?";
    
    private static final String QUERY_NEXT_SCORE_OBJECT_ID = 
    	"SELECT score_object_id_seq.nextval from dual";
    
    private static final String QUERY_IS_GAME_SCORE_OBJECT_NULL = 
    	"SELECT CASE WHEN score_object IS NULL THEN 0 ELSE 1 END isExisting F" +
    	"ROM osc." + TABLE_GAME_SCORE_OBJECTS + " WHERE score_object_id=?";


    public static String getCurrentScore(OscDbConnection conn, 
			                 String game_id, 
			                 String score_id, 
			                 String membership_id, 
			                 String member_id)
    	throws Exception
    {
        String result = null;
        String sql = GET_SCORE + " WHERE game_id = " + game_id +
                    " AND score_id = " + score_id +
                    " AND membership_id = " + membership_id +
                    " AND member_id = " + member_id;

        ResultSet rs = conn.query(sql);
        if (rs.next())
            result = String.valueOf(rs.getInt("SCORE"));

        return result;
    }


    public static String[] getMyRank(OscDbConnection conn, 
    							  String device_id,
    							  String slot_id,
    							  String title_id,
    							  String game_id,                                            
    							  String score_id)
    	throws Exception
    {	
        StringBuffer sql = new StringBuffer(
        		"SELECT osc.vngops.getScoreRank(game_id,score_id,item_id," +
        		"bb_id,title_id,slot_id) FROM osc."); 
        sql.append(TABLE_GAME_SCORES);
        sql.append(" WHERE bb_id=" + device_id); 
        sql.append(" AND title_id=" + title_id);
        sql.append(" AND slot_id=" + slot_id);
        sql.append(" AND game_id=" + game_id);
        sql.append(" AND score_id=" + score_id);

        ResultSet rs = conn.query(sql.toString());
        ArrayList aList = new ArrayList();
        while(rs.next()){
        	aList.add(rs.getString(1));
        }   

        return (String[])aList.toArray(new String[aList.size()]);
    }

    public static String getScore(OscDbConnection conn, 
		                    	  String device_id,
		                    	  String slot_id,
		                    	  String title_id,
		                    	  String game_id, 
		                    	  String score_id)
    	throws Exception
    {
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT gs.item_id,osc.vngops.getScoreRank(gs." +
        		"game_id,gs.score_id,gs.item_id,gs.bb_id,gs.title" +
        		"_id,gs.slot_id) rank,gs.membership_id,gs.member_" +
        		"id,gs.score,gs.score_time,gso.score_info,gso.sco" +
        		"re_object_size FROM ");
        sql.append("osc." + TABLE_GAME_SCORES + " gs,");
		sql.append("osc." + TABLE_GAME_SCORE_OBJECTS + " gso");
		sql.append(" WHERE gs.score_object_id = gso.score_object_id ");
		sql.append(" AND gs.bb_id=" + device_id);
		sql.append(" AND gs.slot_id=" + slot_id);
		sql.append(" AND gs.title_id=" + title_id);
		sql.append(" AND gs.game_id=" + game_id);
		sql.append(" AND gs.score_id=" + score_id);
		
		if(conn.GetRecordCount(sql.toString())==0)
			return null;
		
        return conn.queryXML(sql.toString());
    }
    
    public static String getScoreObject(OscDbConnection conn, 
    									String device_id,
    									String slot_id, 
    									String title_id, 
    									String game_id, 
    									String score_id)
		throws Exception 
	{
		String score_object_id = queryScoreObjectId(conn,
													device_id,
													slot_id,
													title_id,
													game_id,
													score_id);
		if(score_object_id == null) return null;
		
    	StringBuffer sql = new StringBuffer();
		sql.append("SELECT score_object_size,score_object FROM");
		sql.append(" osc." + TABLE_GAME_SCORE_OBJECTS);
		sql.append(" WHERE score_object_id = " + score_object_id);
		sql.append(" AND score_object IS NOT NULL");
		
		if(conn.GetRecordCount(sql.toString())==0)
			return null;

		return conn.queryXML(sql.toString());
	}

    public static String getRangeScore(OscDbConnection conn,
    								   String membership_id,
    								   String member_id,
    								   String device_id,
		                       		   String game_id, 
		                       		   String score_id,
		                       		   String item_id,
		                       		   String from,
		                       		   String count)
    	throws Exception
    {
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT gs.bb_id device_id,gs.slot_id,gs.title_id," +
        		"gs.game_id,gs.score_id,gs.item_id,g.rank,gs.membersh" +
        		"ip_id,gs.member_id,g.score,g.score_time,gso.score_in" +
        		"fo,gso.score_object_size FROM table(SELECT osc.vngop" +
        		"s.getRankedScores(");
        sql.append(game_id);
        sql.append("," + score_id);
        sql.append("," + item_id);
        sql.append("," + from);
        sql.append("," + count);
        sql.append(") FROM dual) g,");
        sql.append(" osc." + TABLE_GAME_SCORES + " gs,");
        sql.append(" osc." + TABLE_GAME_SCORE_OBJECTS + " gso");
        sql.append(" WHERE gs.rowid=g.rid AND gs.score_object_id=" +
        		"gso.score_object_id");
        
        if(membership_id != null 
        		&& !membership_id.equals("")
        		&& member_id != null
        		&& !member_id.equals("")){
        	sql.append(" AND g.membership_id=" + membership_id);
        	sql.append(" AND g.member_id=" + member_id);
        }
        
        if(device_id != null
        		&& !device_id.equals("")){
        	sql.append(" AND gs.bb_id=" + device_id);
        }
        
        sql.append(" ORDER BY ");
        sql.append(" g.rank");
        
        return conn.queryXML(sql.toString());
    }

    public static int getRangeScoreCount(OscDbConnection conn,
			   							 String membership_id,
			   							 String member_id,
			   							 String device_id,
			   							 String game_id, 
			   							 String score_id,
			   							 String item_id,
			   							 String from,
			   							 String count)
    	throws Exception
    {
    	StringBuffer sql = new StringBuffer();
        sql.append("SELECT gs.bb_id device_id,gs.slot_id,gs.title_id," +
        		"gs.game_id,gs.score_id,gs.item_id,g.rank,gs.membersh" +
        		"ip_id,gs.member_id,g.score,g.score_time,gso.score_in" +
        		"fo,gso.score_object_size FROM table(SELECT osc.vngop" +
        		"s.getRankedScores(");
        sql.append(game_id);
        sql.append("," + score_id);
        sql.append("," + item_id);
        sql.append("," + from);
        sql.append("," + count);
        sql.append(") FROM dual) g,");
        sql.append(" osc." + TABLE_GAME_SCORES + " gs,");
        sql.append(" osc." + TABLE_GAME_SCORE_OBJECTS + " gso");
        sql.append(" WHERE gs.rowid = g.rid");
        sql.append(" AND gs.score_object_id = gso.score_object_id");
        
        if(membership_id != null 
        		&& !membership_id.equals("")
        		&& member_id != null
        		&& !member_id.equals("")){
        	sql.append(" AND g.membership_id=" + membership_id);
        	sql.append(" AND g.member_id=" + member_id);
        }
        
        if(device_id != null
        		&& !device_id.equals("")){
        	sql.append(" AND gs.bb_id=" + device_id);
        }

        return conn.GetRecordCount(sql.toString());
    }    
    
    public static String getItemsRank(OscDbConnection conn,
    								  String membership_id,
    								  String member_id,
    								  String device_id,
    								  String game_id,
    								  String score_id)
    	throws Exception
    {
    	StringBuffer sql = new StringBuffer();
        sql.append("SELECT gs.bb_id device_id,gs.slot_id,gs.title_id," +
        		"gs.game_id,gs.score_id,gs.item_id,osc.vngops.getScor" +
        		"eRank(gs.game_id,gs.score_id,gs.item_id,gs.bb_id,gs." +
        		"title_id,gs.slot_id) rank,gs.membership_id,gs.member" +
        		"_id,gs.score,gs.score_time,gso.score_info,gso.score_" +
        		"object_size FROM");
        sql.append(" osc." + TABLE_GAME_SCORES + " gs,");
        sql.append(" osc." + TABLE_GAME_SCORE_OBJECTS + " gso");
        sql.append(" WHERE gs.score_object_id = gso.score_object_id ");
        sql.append(" AND gs.game_id=" + game_id);
        
        if(membership_id !=null 
        		&& !membership_id.equals("")
        		&& member_id !=null
        		&& !member_id.equals("")){
        	sql.append(" AND gs.membership_id=" + membership_id);
        	sql.append(" AND gs.member_id=" + member_id);
        }
        
        if(device_id !=null
        		&& !device_id.equals("")){
        	sql.append(" AND gs.bb_id=" + device_id);
        }
        
        if(score_id !=null
        		&& !score_id.equals("")){
        	sql.append(" AND gs.score_id=" + score_id);
        }
        
        sql.append(" ORDER BY ");
        sql.append(" gs.bb_id,");
        sql.append(" gs.membership_id, gs.member_id,");
        sql.append(" gs.score_id,");
        sql.append(" gs.slot_id,");
        sql.append(" gs.item_id,");
        sql.append(" rank");
        
        return conn.queryXML(sql.toString());
    }
    
    public static int getItemsRankCount(OscDbConnection conn,
										   String membership_id, 
										   String member_id, 
										   String device_id,
										   String game_id, 
										   String score_id) 
    	throws Exception 
    {
    	StringBuffer sql = new StringBuffer();
        sql.append("SELECT gs.bb_id device_id,gs.slot_id,gs.title_id," +
        		"gs.game_id,gs.score_id,gs.item_id,osc.vngops.getScor" +
        		"eRank(gs.game_id,gs.score_id,gs.item_id,gs.bb_id,gs." +
        		"title_id,gs.slot_id) rank,gs.membership_id,gs.member" +
        		"_id,gs.score,gs.score_time,gso.score_info,gso.score_" +
        		"object_size FROM");
        sql.append(" osc." + TABLE_GAME_SCORES + " gs,");
        sql.append(" osc." + TABLE_GAME_SCORE_OBJECTS + " gso");
        sql.append(" WHERE gs.score_object_id = gso.score_object_id ");
        sql.append(" AND gs.game_id=" + game_id);
        
        if(membership_id !=null 
        		&& !membership_id.equals("")
        		&& member_id !=null
        		&& !member_id.equals("")){
        	sql.append(" AND gs.membership_id=" + membership_id);
        	sql.append(" AND gs.member_id=" + member_id);
        }
        
        if(device_id !=null
        		&& !device_id.equals("")){
        	sql.append(" AND gs.bb_id=" + device_id);
        }
        
        if(score_id !=null
        		&& !score_id.equals("")){
        	sql.append(" AND gs.score_id=" + score_id);
        }

		return conn.GetRecordCount(sql.toString());
	}

    public static int insertGameScore(OscDbConnection conn,
    										String membership_id,
    										String member_id,
    										String device_id,
    										String slot_id,
    										String title_id,
                                            String game_id,                                            
                                            String score_id,
                                            String score_info,
                                            String item_id[],
                                            String score[])
        throws Exception
    {
        int rows = 0;
        String xmlData = "";
        String score_object_id = queryScoreObjectId(conn,
													device_id,
													slot_id,
													title_id,
													game_id,
													score_id);
/*        
        if(score_object_id != null)
        {
        	String time = Long.toString(System.currentTimeMillis());
        	for (int i = 0; i < item_id.length; i++) {
				xmlData = "<ROWSET><ROW>" + "<MEMBERSHIP_ID>" + membership_id
						+ "</MEMBERSHIP_ID>" + "<MEMBER_ID>" + member_id
						+ "</MEMBER_ID>" + "<BB_ID>" + device_id + "</BB_ID>"
						+ "<SLOT_ID>" + slot_id + "</SLOT_ID>" + "<TITLE_ID>"
						+ title_id + "</TITLE_ID>" + "<GAME_ID>" + game_id
						+ "</GAME_ID>" + "<SCORE_ID>" + score_id
						+ "</SCORE_ID>" + "<SCORE_TIME>"
						+ time
						+ "</SCORE_TIME>" + "<ITEM_ID>" + item_id[i]
						+ "</ITEM_ID>" + "<SCORE>" + score[i] + "</SCORE>"
						+ "</ROW></ROWSET>";

				rows += conn.updateXML(xmlData, "osc." + TABLE_GAME_SCORES);
			}
        	
        	if(score_info == null) score_info = "";        	
        	xmlData = 
        		"<ROWSET><ROW>" + 
        		"<SCORE_OBJECT_ID>" + score_object_id + "</SCORE_OBJECT_ID>" +
        		"<SCORE_INFO>" + score_info + "</SCORE_INFO>" +
        		"<SCORE_OBJECT_SIZE>0</SCORE_OBJECT_SIZE>" +
        		"<SCORE_OBJECT></SCORE_OBJECT>" +
        		"</ROW></ROWSET>";
        	rows += conn.updateXML(xmlData, "osc." + TABLE_GAME_SCORE_OBJECTS);
        }
*/        
		if(score_object_id == null)
		{
			score_object_id = queryNextScoreObjectId(conn);
			if(score_info == null) score_info = "";
			xmlData = 
				"<ROWSET><ROW>" + 
				"<SCORE_OBJECT_ID>" + score_object_id + "</SCORE_OBJECT_ID>" + 
				"<SCORE_INFO>" + score_info + "</SCORE_INFO>" + 
				"</ROW></ROWSET>";

			rows += conn.insertXML(xmlData, TABLE_GAME_SCORE_OBJECTS);
			
			String time = Long.toString(System.currentTimeMillis());
			for (int i = 0; i < item_id.length; i++) {
				xmlData = "<ROWSET><ROW>" + "<MEMBERSHIP_ID>" + membership_id
						+ "</MEMBERSHIP_ID>" + "<MEMBER_ID>" + member_id
						+ "</MEMBER_ID>" + "<BB_ID>" + device_id + "</BB_ID>"
						+ "<SLOT_ID>" + slot_id + "</SLOT_ID>" + "<TITLE_ID>"
						+ title_id + "</TITLE_ID>" + "<GAME_ID>" + game_id
						+ "</GAME_ID>" + "<SCORE_ID>" + score_id
						+ "</SCORE_ID>" + "<SCORE_TIME>"
						+ time
						+ "</SCORE_TIME>" + "<ITEM_ID>" + item_id[i]
						+ "</ITEM_ID>" + "<SCORE>" + score[i] + "</SCORE>"
						+ "<SCORE_OBJECT_ID>" + score_object_id
						+ "</SCORE_OBJECT_ID>" + "</ROW></ROWSET>";

				rows += conn.insertXML(xmlData, TABLE_GAME_SCORES);
			}			
		}
		
        return rows;
    }
    
    public static String queryNextScoreObjectId(OscDbConnection conn) 
    	throws SQLException
    {
    	PreparedStatement ps = conn.prepareStatement(QUERY_NEXT_SCORE_OBJECT_ID);
    	ResultSet rs = ps.executeQuery();
    	String scoreObjectId = null;
    	if(rs.next())
    		scoreObjectId = rs.getString(1);
    	return scoreObjectId;    	
    }
    
    public static String queryScoreObjectId(OscDbConnection conn,
    										String device_id,
    										String slot_id,
    										String title_id,
    										String game_id,                                            
    										String score_id)
		throws SQLException 
	{
		PreparedStatement ps = conn
				.prepareStatement(QUERY_SCORE_OBJECT_ID);
		ps.setLong(1,Long.parseLong(device_id));
		ps.setLong(2,Long.parseLong(slot_id));
		ps.setLong(3,Long.parseLong(title_id));
		ps.setLong(4,Long.parseLong(game_id));
		ps.setLong(5,Long.parseLong(score_id));
		
		ResultSet rs = ps.executeQuery();
		String scoreObjectId = null;
		if (rs.next())
			scoreObjectId = rs.getString(1);
		return scoreObjectId;
	}

    public static int updateGameScoreObject(OscDbConnection conn,
    										String membership_id,
    										String member_id,
    										String device_id,
    										String slot_id,
    										String title_id,
    										String game_id,                                            
    										String score_id,
    										String score_object_size,
    										String score_object)
        throws Exception
    {
    	int row = 0;
    	boolean isNull = false;
    	String score_object_id = queryScoreObjectId(conn,
        											device_id,
        											slot_id,
        											title_id,
        											game_id,
        											score_id);
    	if(score_object_id == null) return 2;
    	
    	PreparedStatement ps = conn.prepareStatement(QUERY_IS_GAME_SCORE_OBJECT_NULL);
    	ps.setString(1,score_object_id);
    	ResultSet rs = ps.executeQuery();
    	
    	if(rs.next()) {
    		if(rs.getInt(1) == 0)
    			isNull = true;
    	}
    	
    	rs.close();
    	conn.closeStatement(ps);
    	ps = null;
    	
    	if(isNull)
    	{    	
    		ps = conn.prepareStatement(UPDATE_SCORE_OBJECT);
			Clob newClob = conn.createTemporaryClob(false,
					oracle.sql.CLOB.DURATION_SESSION);
			newClob.setString(1, score_object);
			ps.setClob(1, newClob);
			ps.setLong(2, Long.parseLong(score_object_size));
			ps.setLong(3, Long.parseLong(score_object_id));

			row = ps.executeUpdate();
			conn.closeStatement(ps);
			ps = null;
    	}
    	
        return row;
    }

    public static int deleteGameScoreObject(OscDbConnection conn,
											String membership_id, 
											String member_id, 
											String device_id,
											String slot_id, 
											String title_id, 
											String game_id, 
											String score_id) 
    	throws Exception 
    {
		int row = 0;
    	String score_object_id = queryScoreObjectId(conn, 
													device_id, 
													slot_id, 
													title_id, 
													game_id, 
													score_id);
    	if(score_object_id != null)
    	{
    		PreparedStatement ps = conn.prepareStatement(DELETE_GAME_SCORE);
			ps.setString(1, score_object_id);
			row += ps.executeUpdate();
			conn.closeStatement(ps);
			ps = null;

			ps = conn.prepareStatement(DELETE_GAME_SCORE_OBJECT);
			ps.setString(1, score_object_id);
			row += ps.executeUpdate();
			conn.closeStatement(ps);
			ps = null;
    	}
    	
		return row;
	}
    
} // OscDmlGameScore

package com.broadon.osc.java_util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.broadon.exception.InvalidRequestException;


/** The abstract superclass of Rule for each type of result.  The set
 *  of rules are defined for each <code>OscResultBean</code> subclass
 *  as a <code>OscResultXxx.Rule</code> nested class, and will be used 
 // by the <code>OscResultFactory</code> to create <code>OscResult</code>
 *  objects.
 */
public abstract class OscResultRule {

    // An enumeration of the types of OSC servlets we support thus far
    //
    public static class ServerType {
        private int enumeration;
        private ServerType(int e) {enumeration = e;}
        public static final ServerType HTM = new ServerType(1);
        public static final ServerType RPC = new ServerType(2);
    }

    protected final String        actionNm; 
    protected final OscContext    context;
    protected final OscXs         xs;

    public OscResultRule(String nm, OscContext c, OscXs  x)
    {
        actionNm = nm;
        context = c;
        xs = x;
    }

    public String     getActionNm() {return actionNm;} 
    public OscContext getContext()  {return context;}
    public OscXs      getXs()       {return xs;}

    public abstract boolean supports(ServerType t);
    public abstract OscResultBean create(HttpServletRequest req,  
                                         HttpServletResponse res)
        throws InvalidRequestException;

}

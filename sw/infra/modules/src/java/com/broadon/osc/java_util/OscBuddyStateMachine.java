/*
 * OscBuddyStateMachine
 *
 * $Id: OscBuddyStateMachine.java,v 1.1 2005/03/23 19:49:44 vaibhav Exp $
 *
 * Copyright (C) 2004, BroadOn Communications Corp.
 *
 * These coded instructions, statements, and computer programs
 * contain unpublished proprietary information of BroadOn
 * Communications Corp., and are protected by Federal copyright
 * law. They may not be disclosed to third parties or copied or
 * duplicated in any form, in whole or in part, without the prior
 * written consent of BroadOn Communications Corp.
 *
 * Date:   2005-03-14
 * Author: James Zhang
 *
 */

package com.broadon.osc.java_util;

import java.util.HashMap;
import java.sql.*;

/**
 *  The class implements Buddy Finite State Machine based on table
 *  IQUE_MEMBER_BUDDY_FSM_MAPPING.
 */

public class OscBuddyStateMachine
{
    // --------------------------------------------
    // Inner Class
    // --------------------------------------------
    private static class BuddyStateAction
    {
        public int _state;
        public String _action;
        public BuddyStateAction(int state, String action)
        { _state = state; _action = action; }

        public int hashCode() { return _state + _action.hashCode(); }

        public boolean equals(Object obj)
        {
            if (obj != null && obj instanceof BuddyStateAction) {
                BuddyStateAction stateAction = (BuddyStateAction) obj;
                return ( (this._state == stateAction._state) &&
                        (this._action.equals(stateAction._action)));
            }
            return false;
        }

        public String toString()
        {
            return "[State: " + to4BinaryString(_state) +
                ", Action: " + _action + "]";
        }
    }

    // --------------------------------------------
    // Invariant values
    // --------------------------------------------

    private static final String GET_BUDDY_FSM_MAP =
        "SELECT i_state, i_action, o_state, o_action " +
        "FROM   ique_member_buddy_fsm_mappings";

    private static final String GET_BUDDY_STATE =
        "SELECT buddy_flags " +
        "FROM   ique_member_buddy_lists " +
        "WHERE  membership_id = ? " +
        "AND    member_id = ? " +
        "AND    buddy_membership_id = ? " +
        "AND    buddy_member_id = ?";

    private static final String UPDATE_BUDDY_STATE =
        "UPDATE ique_member_buddy_lists " +
        "SET    buddy_flags = NVL(?, buddy_flags) " +
        "WHERE  membership_id = ? " +
        "AND    member_id = ? " +
        "AND    buddy_membership_id = ? " +
        "AND    buddy_member_id = ?";

    private static final String INSERT_BUDDY_STATE =
        "INSERT INTO ique_member_buddy_lists " +
        "(membership_id, member_id, buddy_membership_id, " +
        " buddy_member_id, buddy_flags) " +
        "VALUES (?, ?, ?, ?, nvl(?, 0))";

    private static final String ACTION_ACCEPT_INV = "ACCEPT_INV";
    private static final String ACTION_ACCEPT_RES = "ACCEPT_RES";
    private static final String ACTION_BLOCK_BUD = "BLOCK_BUD";
    private static final String ACTION_INVITE_BUD = "INVITE_BUD";
    private static final String ACTION_INVITE_RCV = "INVITE_RCV";
    private static final String ACTION_REJECT_INV = "REJECT_INV";
    private static final String ACTION_REJECT_RES = "REJECT_RES";
    private static final String ACTION_REMOVE_BUD = "REMOVE_BUD";
    private static final String ACTION_STOP = "STOP";
    private static final String ACTION_UNBLOCK_BUD = "UNBLOCK_BUD";

    // --------------------------------------------
    // Member variables
    // --------------------------------------------

    private static boolean _initialized = false;
    private static OscContext _context = null;
    private static OscLogger _logger = null;
    // Buddy Finite State Machine Mapping, currently there are 50 Rows
    // in table IQUE_MEMBER_BUDDY_FSM_MAPPINGS
    private static HashMap _buddyFSMMap = new HashMap(50);

    // --------------------------------------------
    // Member methods
    // --------------------------------------------

    /**
     * convert a integer to 4-char binary string with possible leading zero
     *
     * @param i integer to convert
     * @return 4-char string
     */
    public static String to4BinaryString(int i)
    {
        if (i < 0) return Integer.toString(i);

        // To get 4-character String
        int base = (int) Math.pow(2, 4);
        String str = Integer.toString( (i & (base - 1)) + base, 2).substring(1);
        return str;
    }

    /**
     * Initialize Buddy State Machine by loading the mapping into memory.
     * @param context Context of the Servlet.
     * @exception SQLException Database access error.
     */
    public static void init(OscContext context) throws SQLException
    {
        if (_initialized) return;

        _initialized = true;
        _context = context;
        _logger = _context.getLogger();
        _buddyFSMMap.clear();
        _logger.debug("init start......");
        OscDbConnection conn = null;
        ResultSet rs = null;
        // Load IQUE_MEMBER_BUDDY_FSM_MAPPINGS into memory
        try {
            conn = _context.getDB(true); // Readonly mode = true
            rs = conn.query(GET_BUDDY_FSM_MAP);
            while (rs.next())
            {
                int i = 1;
                int iState = rs.getInt(i++);
                String iAction = rs.getString(i++);
                int oState = rs.getInt(i++);
                String oAction = rs.getString(i++);
                BuddyStateAction iStateAction = new BuddyStateAction(iState,
                    iAction);
                BuddyStateAction oStateAction = new BuddyStateAction(oState,
                    oAction);
                _buddyFSMMap.put(iStateAction, oStateAction);
            }
            _logger.debug("Buddy State Machine Mapping: " + _buddyFSMMap);
        }
        catch (SQLException e) {
            _logger.error("OscBuddyStateMachine.init() failed: " + e);
            throw e;
        }
        finally {
            if (conn != null) conn.close();
            if (rs != null) rs.close();
        }
    }

    /**
     * Get buddy state.
     * @param conn Database connection.
     * @param myMembershipID My membership ID.
     * @param myMemberID My member ID.
     * @param buddyMembershipID Buddy membership ID.
     * @param buddyMemberID Buddy member ID.
     * @exception SQLException Database access error.
     * @return Buddy state.
     */
    private static int getBuddyState(OscDbConnection conn,
                                     long myMembershipID,
                                     long myMemberID,
                                     long buddyMembershipID,
                                     long buddyMemberID) throws SQLException
    {

        _logger.debug("getBuddyState: [" + GET_BUDDY_STATE + "]-" +
                      myMembershipID + ", " + myMemberID + ", " +
                      buddyMembershipID + ", " + buddyMemberID);
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int buddyFlags = 0;
        try {
            stmt = conn.prepareStatement(GET_BUDDY_STATE);
            int i = 1;
            stmt.setLong(i++, myMembershipID);
            stmt.setLong(i++, myMemberID);
            stmt.setLong(i++, buddyMembershipID);
            stmt.setLong(i++, buddyMemberID);
            rs = stmt.executeQuery();

            if (rs.next()) buddyFlags = rs.getInt(1);
            else buddyFlags = 0;
        }
        finally {
            if (rs != null) rs.close();
            conn.closeStatement(stmt);
        }

        _logger.debug("buddyFlags: " + to4BinaryString(buddyFlags));
        return buddyFlags;
    }

    /**
     * Get next buddy state and action.
     * @param iState Input state.
     * @param iAction Input action.
     * @param oState Output state.
     * @param oAction Output action.
     * @return Next buddy state and action.
     */
    private static BuddyStateAction getNextState(int iState, String iAction)
    {
        BuddyStateAction iStateAction = new BuddyStateAction(iState, iAction);
        BuddyStateAction oStateAction = (BuddyStateAction)
            _buddyFSMMap.get(iStateAction);
        _logger.debug("getNextState: " + iStateAction + " => " + oStateAction);
        if (oStateAction == null)
        {
            oStateAction = new BuddyStateAction( -1, ACTION_STOP);
        }
        return oStateAction;
    }

    /**
     * Set buddy state.
     * @param conn Database connection.
     * @param myMembershipID My membership ID.
     * @param myMemberID My member ID.
     * @param buddyMembershipID Buddy membership ID.
     * @param buddyMemberID Buddy member ID.
     * @param state Buddy state.
     * @exception SQLException Database access error.
     */
    private static void setBuddyState(OscDbConnection conn,
                                      long myMembershipID,
                                      long myMemberID,
                                      long buddyMembershipID,
                                      long buddyMemberID,
                                      int state) throws SQLException
    {
        PreparedStatement stmt = null;
        try {
            _logger.debug("setBuddyState: [" + UPDATE_BUDDY_STATE + "]-" +
                          state + ", " + myMembershipID +
                          ", " + myMemberID + ", " + buddyMembershipID +
                          ", " + buddyMemberID);
            stmt = conn.prepareStatement(UPDATE_BUDDY_STATE);
            int i = 1;
            stmt.setInt(i++, state);
            stmt.setLong(i++, myMembershipID);
            stmt.setLong(i++, myMemberID);
            stmt.setLong(i++, buddyMembershipID);
            stmt.setLong(i++, buddyMemberID);
            int rowCount = stmt.executeUpdate();
            _logger.debug("Updated row count: " + rowCount);

            if (rowCount == 0)
            {
                // No row exists, need to insert the record
                _logger.debug("setBuddyState: [" + INSERT_BUDDY_STATE + "]-" +
                              myMembershipID +
                              ", " + myMemberID + ", " + buddyMembershipID +
                              ", " + buddyMemberID + ", " + state);
                stmt = conn.prepareStatement(INSERT_BUDDY_STATE);
                i = 1;
                stmt.setLong(i++, myMembershipID);
                stmt.setLong(i++, myMemberID);
                stmt.setLong(i++, buddyMembershipID);
                stmt.setLong(i++, buddyMemberID);
                stmt.setInt(i++, state);
                rowCount = stmt.executeUpdate();
                _logger.debug("Inserted row count: " + rowCount);
            }
        }
        finally {
            conn.closeStatement(stmt);
        }
    }

    /**
     * Formate buddy state in XML.
     * @param membershipID Membership ID.
     * @param memberID Member ID.
     * @param oldState Old buddy state.
     * @param newState New buddy state.
     * @return Buddy state in XML format.
     */
    private static String formatBuddyStateXML(long membershipID,
                                              long memberID,
                                              int oldState,
                                              int newState)
    {
        StringBuffer rowContent = new StringBuffer("   <ROW>\n");
        rowContent.append("      ").append(_context.encodeXml("MEMBERSHIP_ID",
                                                  String.valueOf(membershipID))).append("\n");
        rowContent.append("      ").append(_context.encodeXml("MEMBER_ID",
                                                  String.valueOf(memberID))).append("\n");
        rowContent.append("      ").append(_context.encodeXml("OLD_FLAGS",
                                                  String.valueOf(oldState))).append("\n");
        rowContent.append("      ").append(_context.encodeXml("NEW_FLAGS",
                                                  String.valueOf(newState))).append("\n");
        rowContent.append("   </ROW>\n");

        return rowContent.toString();
    }

    /**
     * Start Buddy State Machine.
     * @param conn Database connection.
     * @param myMembershipID My membership ID.
     * @param myMemberID My member ID.
     * @param buddyMembershipID Buddy membership ID.
     * @param buddyMemberID Buddy member ID.
     * @param myAction My action.
     * @return Result of state machine.
     * @exception SQLException Database access error.
     */
    public static String start(OscDbConnection conn,
                               long myMembershipID,
                               long myMemberID,
                               long buddyMembershipID,
                               long buddyMemberID,
                               String myAction) throws SQLException
    {
        return start(conn, myMembershipID, myMemberID,
                     buddyMembershipID, buddyMemberID,
                     myAction, false);
    }

    /**
     * Start Buddy State Machine.
     * @param conn Database connection.
     * @param myMembershipID My membership ID.
     * @param myMemberID My member ID.
     * @param buddyMembershipID Buddy membership ID.
     * @param buddyMemberID Buddy member ID.
     * @param myAction My action.
     * @return Result of state machine.
     * @exception SQLException Database access error.
     */
    public static String start(OscDbConnection conn,
                               long myMembershipID,
                               long myMemberID,
                               long buddyMembershipID,
                               long buddyMemberID,
                               String myAction,
                               boolean summaryOutput) throws SQLException
    {
        if (!_initialized)
        {
            _logger.error("OscBuddyStateMachine not initialized");
            return ("OscBuddyStateMachine not initialized");
        }
        _logger.debug("start state machine. [myMembershipID: " +
                      myMembershipID + ", myMemberID: " + myMemberID +
                      ", buddyMembershipID: " + buddyMembershipID +
                      ", buddyMemberID: " + buddyMemberID +
                      ", myAction: " + myAction + "]");
        int oldStates[] = {0, 0};
        int newStates[] = {0, 0};
        int t = 0;
        String newAction = myAction;
        int myState = 0;
        int buddyState = 0;
        StringBuffer debugStr = new StringBuffer();
        StringBuffer summaryStr = new StringBuffer();

        try {
            //conn.getConnection().setTransactionIsolation(Connection.
            //    TRANSACTION_SERIALIZABLE); // Set transaction isolcation serializable

            oldStates[0] = getBuddyState(conn, myMembershipID, myMemberID,
                                         buddyMembershipID, buddyMemberID);
            oldStates[1] = getBuddyState(conn, buddyMembershipID, buddyMemberID,
                                         myMembershipID, myMemberID);
            newStates[0] = oldStates[0];
            newStates[1] = oldStates[1];
            myState = newStates[0];
            buddyState = newStates[1];
            while (!newAction.equals(ACTION_STOP))
            {
                debugStr.append(" S(").append(t).append(")=").append(
                    to4BinaryString(newStates[t]));
                BuddyStateAction stateAction = getNextState(newStates[t],
                    newAction);
                newStates[t] = stateAction._state;
                newAction = stateAction._action;
                debugStr.append("->").append(to4BinaryString(newStates[t]));
                debugStr.append(" Next=").append(newAction);
                t = Math.abs(t - 1);
            }

            summaryStr.append("Input: A=").append(to4BinaryString(oldStates[0])).
                append(" B=").append(to4BinaryString(oldStates[1]));
            if (newStates[0] == -1 || newStates[1] == -1)
            {
                summaryStr.append("  *** Invalid ***");
            }
            else {
                summaryStr.append(" Result: A=").append(to4BinaryString(
                    oldStates[0])).
                    append("->").append(to4BinaryString(newStates[0]));
                summaryStr.append(" B=").append(to4BinaryString(oldStates[1])).
                    append("->").append(to4BinaryString(newStates[1]));
            }
            _logger.info(summaryStr.toString());
            _logger.debug(debugStr.toString());

            if (newStates[0] >= 0 && newStates[1] >= 0)
            {
                if (oldStates[0] != newStates[0])
                {
                    setBuddyState(conn, myMembershipID, myMemberID,
                                  buddyMembershipID, buddyMemberID,
                                  newStates[0]);
                    myState = newStates[0];
                }
                if (oldStates[1] != newStates[1] && newStates[1] >= 0)
                {
                    setBuddyState(conn, buddyMembershipID, buddyMemberID,
                                  myMembershipID, myMemberID, newStates[1]);
                    buddyState = newStates[1];
                }
            }
            conn.commit();
        }
        catch (SQLException e) {
            _logger.error("OscBuddyStateMachine.start() failed: " + e);
            throw e;
        }

        if (summaryOutput) return summaryStr.toString();

        StringBuffer result = new StringBuffer();

        if (newStates[0] >= 0 && newStates[1] >= 0)
        {
            result.append(_context.encodeXml_TNL("status", OscStatusCode.SC_OK));
            result.append(_context.encodeXml_TNL("status_msg",
                              OscStatusCode.getMessage(OscStatusCode.SC_OK)));
            result.append("<ROWSET>\n");
            result.append(formatBuddyStateXML(myMembershipID, myMemberID,
                                              oldStates[0], myState));
            result.append(formatBuddyStateXML(buddyMembershipID, buddyMemberID,
                                              oldStates[1], buddyState));
            result.append("</ROWSET>");
        }
        else {
            result.append(_context.encodeXml_TNL("status", "551")); // Shall we put this in OscStatusCode?
            result.append("\t<status_msg>Cannot ").append(myAction);
            result.append(" for state (").append(to4BinaryString(oldStates[0]));
            result.append("->").append(to4BinaryString(myState));
            result.append(") and (").append(to4BinaryString(oldStates[1]));
            result.append("->").append(to4BinaryString(buddyState));
            result.append(")</status_msg>");
        }
        return result.toString();
    }

    /**
     * Test Buddy State Machine with one action.
     * @param context Osc context.
     * @param action Test action.
     * @return Summary of state machine.
     */
    public static String testAnyBSM(OscContext context, String action)
    {
        long membershipID = 18863514322L;
        long myMemberID = 3;
        long buddyMemberID = 4;
        int oldStates[] = {0, 0};
        int newStates[] = {0, 0};
        OscDbConnection conn = null;
        StringBuffer result = new StringBuffer("Testing Action ").append(action).
            append("\n");
        String summaryOutput = null;
        try {
            init(context);
            conn = context.getDB(false); // Readonly mode = false

            oldStates[0] = getBuddyState(conn, membershipID, myMemberID,
                                         membershipID, buddyMemberID);
            oldStates[1] = getBuddyState(conn, membershipID, buddyMemberID,
                                         membershipID, myMemberID);
            for (newStates[0] = 0; newStates[0] <= 15; newStates[0]++)
            {
                for (newStates[1] = 0; newStates[1] <= 15; newStates[1]++)
                {
                    setBuddyState(conn, membershipID, myMemberID,
                                  membershipID, buddyMemberID, newStates[0]);
                    setBuddyState(conn, membershipID, buddyMemberID,
                                  membershipID, myMemberID, newStates[1]);
                    summaryOutput = start(conn, membershipID, myMemberID,
                                          membershipID, buddyMemberID, action,
                                          true);
                    result.append(summaryOutput).append("\n");
                    conn.rollback();
                }
            }
            setBuddyState(conn, membershipID, myMemberID,
                          membershipID, buddyMemberID, oldStates[0]);
            setBuddyState(conn, membershipID, buddyMemberID,
                          membershipID, myMemberID, oldStates[1]);
            conn.commit();
        }
        catch (SQLException e) {
            _logger.error("OscBuddyStateMachine.testAnyBSM() failed: " + e);
        }
        finally {
            if (conn != null) conn.close();
        }
        return result.toString();
    }

    /**
     * Test Buddy State Machine with all actions.
     * @param context Osc context.
     * @return Summary of state machine.
     */
    public static String testAllBSM(OscContext context)
    {
        StringBuffer result = new StringBuffer();
        result.append(testAnyBSM(context, ACTION_INVITE_BUD));
        result.append(testAnyBSM(context, ACTION_ACCEPT_INV));
        result.append(testAnyBSM(context, ACTION_REJECT_INV));
        result.append(testAnyBSM(context, ACTION_BLOCK_BUD));
        result.append(testAnyBSM(context, ACTION_UNBLOCK_BUD));
        result.append(testAnyBSM(context, ACTION_REMOVE_BUD));
        return result.toString();
    }

} // class OscBuddyStateMachine

package com.broadon.osc.java_util;


/**
 * Data manipulation library functions defined here pertain to game session
 */
public class OscDmlGameSession
{
    /** Define all SQL statements
     */

    private static final String TABLE_VNG_SESSIONS = "VNG_SESSIONS";
    private static final String COLUMN_VN_ID = "VN_ID";
    private static final String COLUMN_JOIN_FLAGS = "JOIN_FLAGS";
    private static final String COLUMN_END_DATE = "END_DATE";

    private static final String GET_VNG_SESSIONS_LIST = 
		"SELECT * FROM VNG_SESSIONS";
//  		  "SELECT vn_id, membership_id, member_id, game_id, title_id, create_date, " +
//                "join_flags, total_slots, friend_slots, vn_zone, ga01, ga02, ga03, ga04, " +
//                "ga05, ga06, ga07, ga08, ga09, ga10, ga11, ga12, ga13, ga14, ga15, ga16, " +
//                "kw01, comments, last_updated FROM VNG_SESSIONS";
 
    public static int insertUpdateGameSession(OscDbConnection conn,
                                              String xmlData)
        throws Exception
    {
        return conn.insertupdateXML(xmlData, TABLE_VNG_SESSIONS);
    }

    public static int updateGameSession(OscDbConnection conn,
                                        String xmlData)
        throws Exception
    {
        return conn.updateXML(xmlData, TABLE_VNG_SESSIONS);
    }

    public static String queryGameSession(OscDbConnection conn,
                                          String sql,
                                          int numrows,
                                          int skiprows)
        throws Exception
    {
        return conn.queryXML(sql, numrows, skiprows);
    }

    public static int countGameSession(OscDbConnection conn,
                                       String sql)
        throws Exception
    {
        return conn.GetRecordCount(sql);
    }

    public static String getGameSessionXML(String membership_id,
					   String member_id,
					   String session_id,
					   String end_date,
					   String[] name, String[] value)
    {
        String xml = "<ROWSET><ROW num=\"1\">";
        xml += "<MEMBERSHIP_ID>"+membership_id+"</MEMBERSHIP_ID>";
        xml += "<MEMBER_ID>"+member_id+"</MEMBER_ID>";
        xml += "<SESSION_ID>"+session_id+"</SESSION_ID>";
          
        if (end_date==null || end_date.equals(""))
            xml += "<END_DATE></END_DATE>";
        else
            xml += "<END_DATE>"+end_date+"</END_DATE>";

        for (int i = 0; i < name.length; i++)
        {
            xml += "<"+name[i].toUpperCase()+">"+
                   value[i]+
                   "</"+name[i].toUpperCase()+">";
        }
        
        xml += "</ROW></ROWSET>";

        return xml;  
    }

    public static String getGameSessionMatchSQL(String sort, String[] name, String[] value, String[] op)
    {
        String sql = "SELECT v.* FROM " + TABLE_VNG_SESSIONS + " v, IQUE_MEMBER_LOGINS l " +
                    "WHERE v.session_id = l.session_id AND l.is_expired != 1 " +
                    "AND bbxml.converttimestamp(sys_extract_utc(current_timestamp)) - l.session_last_time <= l.max_idle_time " +
                    "AND v.end_date IS NULL AND v.join_flags = 0 ";

        if (name != null)
        {
            for (int i = 0; i < name.length; i++)
            {
                if (name[i].toUpperCase().equals(COLUMN_JOIN_FLAGS) ||
                    name[i].toUpperCase().equals(COLUMN_END_DATE));
                else {
                    sql += " AND ";
                    if (op[i].toUpperCase().equals("STR")) 
                    {
                        sql += "upper(v." + name[i] + ") ";
                        sql += getOperator(op[i]) + " ";
                        sql += "'" + getStringToQuery(value[i].toUpperCase()) + "' escape '|'";
                    } else {
                        sql += "v." + name[i] + " ";
                        sql += getOperator(op[i]) + " ";
                        sql += value[i];
                    }
                }
            }
        }
 
        if (sort != null && !sort.equals(""))
            sql += " ORDER BY v." + sort;

        return sql;
    }

    public static String getGameSessionQueryVnidSQL(String sort, String[] vnid)
    {
        String sql = GET_VNG_SESSIONS_LIST;

        if (vnid != null)
        {
            sql += " WHERE";
            for (int i = 0; i < vnid.length;)
            {
                sql += " VN_ID = " + vnid[i];
                i++;
                if (i < vnid.length)
                    sql += " OR";
            }
        }
 
        if (sort != null && !sort.equals(""))
            sql += " ORDER BY " + sort;

        return sql;
    }

    private static String getOperator(String op)
    {   
        if (op!=null)
        {
            op = op.toUpperCase();
        
            if (op.equals("EQ"))
                return "=";
            else if (op.equals("NEQ"))
                return "!=";
            else if (op.equals("LT"))
                return "<";
            else if (op.equals("GT"))
                return ">";
            else if (op.equals("LE"))
                return "<=";
            else if (op.equals("GE"))
                return ">=";
            else if (op.equals("STR"))
                return "like";
            else
                return "=";
        }

        return null;
    }

    private static String getStringToQuery(String str)
    {   
        if (str!=null)
        {
            for (int i = 0; i < str.length(); i++) 
            {
                if (str.charAt(i) == '%' || str.charAt(i) == '_' || str.charAt(i) == '|')
                {
                    str = str.substring(0, i) + "|" + str.substring(i);  
                    i++;
                } else if (str.charAt(i) == '\'') {
                    str = str.substring(0, i) + "'" + str.substring(i);  
                    i++;
                }
            }

            str = str.replace('*', '%');
            str = str.replace('?', '_');
        }

        return str;
    }

} // OscDmlGameSession

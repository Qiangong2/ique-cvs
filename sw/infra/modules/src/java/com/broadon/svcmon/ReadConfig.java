package com.broadon.svcmon;

import java.sql.*;
import oracle.jdbc.pool.*;

public class ReadConfig
{
    static final String LB_KEY = ".lb=";
    static final String ACT_KEY = "sys.act.";
    static final String SYSCONFIG_KEY = "sysconfig";
    static final String SYSCONFIG_ACT_KEY = ';' + SYSCONFIG_KEY + ".act.";

    static void printConfig(String confString, String key, String service,
			    String ipaddr)
    {
	int idx = confString.indexOf(key);
	if (idx < 0)
	    return;
	try {
	    char ch = confString.charAt(idx + key.length());
	    switch (ch) {
	    case 'p':
	    case 'P':
	    case 's':
	    case 'S':
		System.out.println(service + ' ' + ipaddr + ' ' + ch);
		break;
		
	    default:
		break;
	    }
	} catch (IndexOutOfBoundsException e) {}
    }

    static void parseConfig(String ipaddr, String[] confs)
    {
	for (int i = 0; i < confs.length; ++i) {
	    String confString = confs[i];
	    if (! confString.startsWith(ACT_KEY))
		continue;
	    
	    int idx = confString.indexOf(' ');
	    String service = confString.substring(ACT_KEY.length(), idx);

	    if (SYSCONFIG_KEY.equals(service)) {
		// special case for multiple action package
		while (idx >= 0 && idx < confString.length()) {
		    idx = confString.indexOf(SYSCONFIG_ACT_KEY, idx);
		    if (idx < 0)
			break;
		    try {
			idx += SYSCONFIG_ACT_KEY.length();
			int vIdx = confString.indexOf('=', idx);
			if (confString.charAt(vIdx + 1) != '1')
			    continue;
			service = confString.substring(idx, vIdx);
			if (service.length() == 0)
			    continue;
			String key = ';' + SYSCONFIG_KEY + '.' + service + LB_KEY;
			printConfig(confString, key, service, ipaddr);
		    } catch (IndexOutOfBoundsException e) {}
		}
	    } else {
		String key = ';' + service + LB_KEY;
		printConfig(confString, key, service, ipaddr);
	    }
	}
    }


    static final String readServer =
	"SELECT PUBLIC_NET_IP, SERVICES FROM HR_SUMMARY_V WHERE " +
	"    HW_TYPE LIKE '%Server' AND IS_ACTIVE=1";

    public static void main(String[] args) {
	if (args.length < 3) {
	    System.err.println("Usage ReadConfig URL user password");
	    System.exit(1);
	}

	try {
	    OracleDataSource ds = new OracleDataSource();
	    ds.setURL(args[0]);
	    ds.setUser(args[1]);
	    ds.setPassword(args[2]);
	    Connection conn = ds.getConnection();
	    conn.setReadOnly(true);

	    PreparedStatement ps = conn.prepareStatement(readServer);
	    ResultSet rs = ps.executeQuery();
	    while (rs.next()) {
		Array conf = rs.getArray(2);
		if (rs.wasNull())
		    continue;
		
		String[] list = (String[]) conf.getArray();

		parseConfig(rs.getString(1), list);
	    }
	    ps.close();
	    conn.close();

	} catch (Exception e) {
	    e.printStackTrace();
	    System.exit(1);
	}
    }
}

package com.broadon.sms;

/**
 * Interface for a simple random number generator
 */
public interface Random
{
    /** Get random bytes.
     * @param size The number of random bytes
     * @return A sequence of random bytes.
     */
    public byte[] getRandom(int size)
	throws java.security.GeneralSecurityException, com.ncipher.nfast.NFException;
}

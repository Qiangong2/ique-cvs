package com.broadon.sms;

import java.io.*;
import java.util.Properties;

import com.broadon.hsm.Constants;

/**
 * Parse a request for HSM functions.
 */
public class ParseInput
{
    /** Specify a signature request. */
    protected static final int CMD_SIGN = 0;

    /** Specify a random number request. */
    protected static final int CMD_RAND = 1;

    /** Specify a new depot certificate request. */
    protected static final int CMD_NEW_DEPOT = 2;

    /** Specify a new server certificate request. */
    protected static final int CMD_NEW_SERVER = 3;

    /** Specify a new smart card certificate request. */
    protected static final int CMD_NEW_SMART_CARD = 4;

    /** Specify a signature request on hashed data. */
    protected static final int CMD_SIGN_HASHED = 5;

    /** Specify a self check request. */
    protected static final int CMD_SELF_CHECK = 9;

    int command;
    int random_size = -1;
    String signer = null;
    String certName = null;
    String password = null;
    byte[] data;

    /**
     * Parse a HSM request.
     * @param in Input stream containing the request.
     * @exception IOException Pretty much anything that's wrong.
     */
    protected ParseInput(InputStream in)
	throws IOException
    {
	byte[] buffer = new byte[Constants.HEADER_SIZE];
	int n;
	int count = 0;
	while (count < Constants.HEADER_SIZE &&
	       (n = in.read(buffer, count,
			    Constants.HEADER_SIZE - count)) > 0) {
	    count += n;
	}
	if (count < Constants.HEADER_SIZE)
	    throw new IOException("Incomplete header");

	Properties prop = new Properties();
	prop.load(new ByteArrayInputStream(buffer));
	if (! Constants.VERSION_STRING.equals(prop.getProperty(Constants.VERSION)))
	    throw new IOException("Invalid version");

	String cmd = prop.getProperty(Constants.COMMAND);
	if (Constants.CMD_SIGN_HASHED.equals(cmd)) {
	    command = CMD_SIGN_HASHED;
	    signer = prop.getProperty(Constants.SIGNER);
	    readData(prop.getProperty(Constants.DATA_LEN), in);
	} else if (Constants.CMD_SIGN.equals(cmd)) {
	    command = CMD_SIGN;
	    signer = prop.getProperty(Constants.SIGNER);
	    readData(prop.getProperty(Constants.DATA_LEN), in);
	} else if (Constants.CMD_RAND.equals(cmd)) {
	    command = CMD_RAND;
	    random_size = parseInt(prop.getProperty(Constants.RANDOM_SIZE));
	} else if (Constants.CMD_NEW_DEPOT.equals(cmd)) {
	    command = CMD_NEW_DEPOT;
	    certName = prop.getProperty(Constants.NEW_CERT_NAME);
	    password = prop.getProperty(Constants.PASSWORD);
	} else if (Constants.CMD_NEW_SERVER.equals(cmd)) {
	    command = CMD_NEW_SERVER;
	    certName = prop.getProperty(Constants.NEW_CERT_NAME);
	    password = prop.getProperty(Constants.PASSWORD);
	} else if (Constants.CMD_NEW_SMART_CARD.equals(cmd)) {
	    command = CMD_NEW_SMART_CARD;
	    certName = prop.getProperty(Constants.NEW_CERT_NAME);
	    password = prop.getProperty(Constants.PASSWORD);
	} else if (Constants.CMD_SELF_CHECK.equals(cmd)) {
	    command = CMD_SELF_CHECK;
	} else
	    throw new IOException("Unknown command");
    } // ParseInput


    int parseInt(String value) throws IOException {
	try {
	    return Integer.parseInt(value);
	} catch (NumberFormatException e) {
	    throw new IOException(e.getMessage());
	}
    } // parseInt


    void readData(String len, InputStream in) throws IOException {
	int length = parseInt(len);
	data = new byte[length];
	int count = 0;
	int n;
	while (count < length &&
	       (n = in.read(data, count, length - count)) > 0)
	    count += n;
	if (count < length)
	    throw new IOException("Incomplete data");
    } // readData
	

    /** @return the command of the request. */
    protected int getCommand() {
	return command;
    }

    /** @return The ID of the private key to be used for signing --
     * undefined if this is not a signature request.
     */
    protected String getSigner() {
	return signer;
    }

    /** @return the number of random bytes requested -- undefined if
     * this is not a random number request. */
    protected int getRandSize() {
	return random_size;
    }

    /** @return the common name of the certificate. */
    protected String getCertName() {
	return certName;
    }

    /** @return the password for protecting the private key. */
    protected String getPassword() {
	return password;
    }

    /** @return the document to be signed.  -- undefined if this is
     * not a signature request. */
    protected byte[] getData() {
	return data;
    }
}

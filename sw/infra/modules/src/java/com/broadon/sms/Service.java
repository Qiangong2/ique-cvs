package com.broadon.sms;

import java.net.*;
import java.io.*;
import java.util.logging.*;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import com.ncipher.nfast.NFException;

import com.broadon.hsm.Constants;
import com.broadon.hsm.HSMHeaderOutputStream;

    
/**
 * Implement the security module service.
 */
public class Service implements Runnable
{
    static String configFile;
    static Logger log;
    static ThreadLocal dispatcher = new ThreadLocal() {
        protected synchronized Object initialValue() {
            Dispatcher disp = null;
            try {
                disp = new Dispatcher();
            } catch (Exception e) {
                log.log(Level.SEVERE, "Can't create dispatcher", e);
            }
            return disp;
        }
    };

    Socket socket;
    Dispatcher disp;

    /**
     * Initialize the static parameters shared by all instances of this class.
     * @param disp Dispatcher to serialize the requests sent to the HSM.
     */
    public Service(String configFile, Logger log) 
        throws Exception
    {
        this.configFile = configFile;
        this.log = log;
        try {
            Dispatcher.init(configFile, log);
        } catch (Exception e) {
            log.log(Level.SEVERE, "Can't initialize dispatcher", e);
            throw e;
        }
     }

    Service(Socket socket) {
	this.socket = socket;
    }

    /**
     * Called by the server to serve each new request.
     * @param socket Socket for communicating with the client.
     * @return A new instance of <code>Service</code>
     */
    public Service newInstance(Socket socket) {
	return new Service(socket);
    }


    void shutdown() {
	Handler[] handlers = log.getHandlers();
	for (int i = 0; i < handlers.length; ++i) {
	    handlers[i].flush();
	}
	System.exit(1);
    }

    /**
     * Parse the input, perform the requested operation, and then send
     * back the result.
     */
    public void run() {
	try {
	    socket.setSoLinger(true, 10);
	    socket.setSoTimeout(30000);
	    socket.setTcpNoDelay(true);
	    
            // get thread local variable
	    disp = (Dispatcher) dispatcher.get();
	    if (disp == null)
	        return;

	    InputStream in = socket.getInputStream();
	    OutputStream out = socket.getOutputStream();

	    ParseInput request = new ParseInput(in);

	    switch (request.command) {
	    case ParseInput.CMD_SIGN:
		doSignature(request, out, false);
		break;
	    case ParseInput.CMD_SIGN_HASHED:
		doSignature(request, out, true);
		break;
	    case ParseInput.CMD_RAND:
		doRandom(request, out);
		break;
	    case ParseInput.CMD_NEW_DEPOT:
		doNewCert(request, out, CAconfig.DEPOT_CERT);
		break;
	    case ParseInput.CMD_NEW_SERVER:
		doNewCert(request, out, CAconfig.SERVER_CERT);
		break;
	    case ParseInput.CMD_NEW_SMART_CARD:
		doNewCert(request, out, CAconfig.SMART_CARD_CERT);
		break;
	    case ParseInput.CMD_SELF_CHECK:
		doSelfCheck(out);
		log.info("Self check ok.");
		break;
	    }
	    out.flush();
	    socket.shutdownOutput();
	} catch (GeneralSecurityException e) {
	    // non-fatal error, we simply reply nothing.
	    log.log(Level.WARNING, "Request failed", e);
	    return;
	} catch (IOException e) {
	    log.log(Level.WARNING, "Request failed", e);
	    return;
	} catch (SQLException e) {
	    log.log(Level.SEVERE, "Unrecoverable database access error -- shutting down", e);
	    shutdown();
	} catch (NFException e) {
	    log.log(Level.SEVERE, "Cannot access HSM -- shutting down", e);
	    shutdown();
	} catch (Exception e) {
	    // everything else is fatal
	    log.log(Level.SEVERE, "Unrecoverable error -- shutting down", e);
	    shutdown();
	} finally {
	    try {
		socket.close();
	    } catch (IOException e) {
		log.log(Level.WARNING, "Cannot close socket", e);
	    }
	}
    } // run


    void doSelfCheck(OutputStream out) throws Exception {
	// just check the HSM and the database
	disp.getRandom(4);
	disp.testDB();
	out.write("OK".getBytes());
    }

    void doSignature(ParseInput req, OutputStream out, boolean hashed)
	throws Exception
    {
	String signer = req.getSigner();
	log.fine("Request signature from " + signer);
	byte[] sig = disp.sign(signer, req.getData(), hashed);
	out.write(sig);
    }

    void doRandom(ParseInput req, OutputStream out) throws Exception {
	log.finer("Random number request");
	byte[] rand = disp.getRandom(req.getRandSize());
	out.write(rand);
    }


    void doNewCert(ParseInput req, OutputStream out, String type)
	throws Exception
    {
	log.info("Request " + type + " cert for " + req.getCertName());

	IssueCert cert =
	    disp.genCert(type, req.getCertName(), req.getPassword());

	// compose reply header
	HSMHeaderOutputStream header =
	    new HSMHeaderOutputStream(Constants.HEADER_SIZE);
	PrintWriter writer = new PrintWriter(header);

	byte[] password = cert.getKeyPassword().getBytes();
	
	writer.println(Constants.VERSION + "=" + Constants.VERSION_STRING);
	writer.println(Constants.REPLY_PASSWORD_LEN + "=" +
		       password.length);
	writer.println(Constants.REPLY_KEY_LEN + "=" +
		       cert.getPrivateKey().length);
	writer.println(Constants.REPLY_CERT_LEN + "=" +
		       cert.getCertificate().length);
	writer.println(Constants.REPLY_CHAIN_LEN + "=" +
		       cert.getCertChain().length);
	writer.println(Constants.REPLY_ROOT_LEN + "=" +
		       cert.getTrustedCerts().length);
	writer.println(Constants.REPLY_KEYSTORE_LEN + "=" +
		       cert.getKeyStore().length);
	writer.println(Constants.REPLY_TRUSTED_STORE_LEN + "=" +
		       cert.getTrustedStore().length);
	writer.println(Constants.REPLY_CHAIN_ID_LEN + "=" +
		       cert.getChainID().length);
	writer.close();

	header.pad(' ');	// remember to pad the header buffer

	// now, send out the reply
	out.write(header.getInternalBuf());
	out.write(password);
	out.write(cert.getPrivateKey());
	out.write(cert.getCertificate());
	out.write(cert.getCertChain());
	out.write(cert.getTrustedCerts());
	out.write(cert.getKeyStore());
	out.write(cert.getTrustedStore());
	out.write(cert.getChainID());
    }
}

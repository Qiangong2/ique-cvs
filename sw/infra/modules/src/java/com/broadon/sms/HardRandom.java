package com.broadon.sms;

import java.security.GeneralSecurityException;

import com.ncipher.nfast.NFException;
import com.ncipher.nfast.connect.*;
import com.ncipher.nfast.marshall.*;


public class HardRandom implements Random
{
    M_Command cmd;
    NFConnection conn;
    int last_size;

    /**
     * Initialized the hardware random number generator.
     * @param conn A connection to the HSM.
     */
    protected HardRandom(NFConnection conn) {
	this.conn = conn;
	cmd = new M_Command();
	cmd.cmd = M_Cmd.GenerateRandom;
	cmd.flags = 0;
	last_size = 256;	// set default size
	cmd.args = new M_Cmd_Args_GenerateRandom(last_size);
    }


    byte[] getRandomUnchecked(int size)
	throws GeneralSecurityException, NFException
    {
	if (last_size != size) {
	    last_size = size;
	    cmd.args = new M_Cmd_Args_GenerateRandom(size);
	}
	try {
	    M_Reply r;
	    synchronized (conn) {
		r =  conn.wait(conn.submit(cmd), 10000);
	    }
	    if (r == null)
		throw new NFException("Access timeout");
	    
	    M_Cmd_Reply_GenerateRandom reply =
		(M_Cmd_Reply_GenerateRandom) r.reply;
	    return reply.data.value;
	} catch (MarshallTypeError e) {
	    throw HardError.error(e);
	} catch (CommandTooBig e) {
	    throw HardError.error(e);
	} catch (ObjectNotAnOutstandingRequest e) {
	    throw HardError.error(e);
	} catch (NFException e) {
	    throw HardError.fatal(e);
	} catch (InterruptedException e) {
	    throw HardError.fatal(e);
	}
    }

    public byte[] getRandom(int size)
	throws GeneralSecurityException, NFException
    {
	byte[] result = getRandomUnchecked(size);
	if (result != null && result.length >= size)
	    return result;

	// The HSM sometimes does not return enough random bytes.

	byte[] returnBuf = new byte[size];
	int count = 0;
	
	if (result != null) {
	    System.arraycopy(result, 0, returnBuf, 0, result.length);
	    count = result.length;
	} 

	while (count < size) {
	    result = getRandomUnchecked(size - count);
	    if (result != null) {
		System.arraycopy(result, 0, returnBuf, count, result.length);
		count += result.length;
	    }
	}
	return returnBuf;
    }
}

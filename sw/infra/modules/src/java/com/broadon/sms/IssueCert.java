package com.broadon.sms;

import java.io.*;
import java.security.PrivateKey;
import java.security.GeneralSecurityException;
import java.security.cert.X509Certificate;

/**
 * Invoke the Unix program "openssl" to generate an X509 certificate.
 */
public class IssueCert
{
    byte[] privateKey;		// actual private key content
    String keyPassword;		// password for accessing keyFile
    byte[] certificate;		// certificate created

    byte[] certChain;		// cert. chain for openssl
    byte[] trustedCerts;	// all certs that should be trusted

    byte[] keyStore;		// cert & chain in KeyStore format
    byte[] trustedStore;	// trusted cert in KeyStore format

    byte[] chainID;		// ID of the cert chain in the database

    static File tmpdir = null;
    static String engine = "chil";

    String[] reqCmd = {
	"openssl",		
	"req",
	"-config",
	null,			// reqCmd[3]: config file name
	"-new",
	"-passout",
	"stdin"
    };

    String[] signCmd = {
	"openssl",
	"x509",
	"-engine",		
	null,			// signCmd[3]: "chil" or "openssl"
	"-req",
	"-CA",
	null,			// signCmd[6]: CA cert file name
	"-CAkey",
	null,			// signCmd[8]: CA private key file name
	"-CAserial",
	null,			// signCmd[10]: serial # file name
	"-CAcreateserial",
	"-days",
	null			// signCmd[13]: days valid
    };
	

    static final String errString = "Error generating cert. request";
    

    /** Specify the directory for temporary working files.
     * @param dir Temporary directory.
     */
    protected static void setTmpdir(File dir) {
	tmpdir = dir;
    }
    

    /** Specify the crypto engine to be used by openssl.  Default is
     * "chil", the nCipher HSM.
     * @param engine The crypto engine to be used.  Currently only
     * "chil" and "openssl" are supported.
     */
    
    protected static void setEngine(String engine) {
	IssueCert.engine = engine;
    }

    /**
     * Issue an X509 certificate.
     * @param configFile Configuration file giving detail parameters of the
     * certificates.
     * @param keyPassword Password used to protect the private key.
     * @param name Common name of the certificate subject.
     * @param caCertFile File of the CA used to sign the generated
     * cert.
     * @param caKeyFile Private key file (or file handle) of the CA.
     * @param serialFile File where the serial number is kept.
     * @param days Number of days where this certificate will be
     * valid.
     * @exception IOException Any error in generating the cert.
     */
    void createCert(String configFile, String keyPassword, String name,
		    String caCertFile, String caKeyFile, String serialFile,
		    int days)
	throws IOException
    {
	File privKey = null;
	
	try {
	    privKey = File.createTempFile("x509", null, tmpdir);
	    String keyFile = privKey.getAbsolutePath();

	    this.keyPassword = keyPassword;

	    reqCmd[3] = configFile;
	    String[] envp = { "COMMON_NAME=" + name,
			      "KEYFILE=" + keyFile };

	    signCmd[3] = engine;
	    signCmd[6] = caCertFile;
	    signCmd[8] = caKeyFile;
	    signCmd[10] = serialFile;
	    signCmd[13] = String.valueOf(days);

	    UnixCommand cmd = new UnixCommand();
	    Process reqProc = cmd.exec(reqCmd, envp);
	    Process signProc = cmd.exec(signCmd, null);

	    // cert req command expects password from std in
	    OutputStream out = reqProc.getOutputStream();
	    out.write(keyPassword.getBytes());
	    out.close();
	    
	    // pipe the output of the cert req to the input of x509 signing.
	    pipe(reqProc.getInputStream(), signProc.getOutputStream());
	    
	    // record the cert generated 
	    ByteArrayOutputStream buffer = new ByteArrayOutputStream();
	    pipe(signProc.getInputStream(), buffer);
	    certificate = buffer.toByteArray();
	    
	    try {
		if (reqProc.waitFor() != 0 ||
		    signProc.waitFor() != 0) 
		    throw new IOException(errString);
	    } catch (InterruptedException e) {
		throw new IOException(e.getMessage());
	    } finally {
		reqProc.destroy();
		signProc.destroy();
	    }
	    
	    // read the private key
	    readKey(privKey);

	} finally {
	    if (privKey != null)
		privKey.delete();
	}
    } // createCert



    void createKeyStore(KeyStoreWriter kw, char[] ksPassword, String ksAlias,
			X509Certificate[] chain)
	throws GeneralSecurityException, IOException
    {
	X509Certificate[] certs = new X509Certificate[chain.length + 1];
	certs[0] = kw.generateCert(certificate);
	for (int i = 0; i < chain.length; ++i)
	    certs[i + 1] = chain[i];
	PrivateKey privKey = kw.convertPrivateKey(privateKey, keyPassword);
	keyStore = kw.getKeyStore(certs, ksAlias, privKey, ksPassword);
    }


    /**
     * Issue an X509 certificate.
     * @param conf Configuration of the cert issuer.
     * @param kw handle KeyStore operations.
     * @param keyPassword Password used to protect the private key.
     * @param name Common name of the certificate subject.
     * @exception IOException Any error in generating the cert.
     */
    protected IssueCert(CAconfig conf, KeyStoreWriter kw, char[] ksPassword,
			String keyPassword, String name)
	throws IOException, GeneralSecurityException
    {
	createCert(conf.configFile, keyPassword, name, conf.certFile,
		   conf.keyFile, conf.serialFile, conf.days);
	certChain = conf.certChain;
	trustedCerts = conf.trustedCerts;

	if (conf.hasKeyStore) {
	    createKeyStore(kw, ksPassword, name, conf.ksChain);
	    trustedStore = conf.trustedStore;
	} else {
	    keyStore = new byte[0];
	    trustedStore = new byte[0];
	}

	chainID = String.valueOf(conf.chainID).getBytes();
    } // IssueCert


    // pipe data from the input stream to the output stream
    void pipe(InputStream in, OutputStream out) throws IOException {
	byte[] buffer = new byte[64];
	int n;
	while ((n = in.read(buffer)) > 0) {
	    out.write(buffer, 0, n);
	}
	in.close();
	out.close();
    } // pipe


    void readKey(File file) throws IOException
    {
	int length = (int) file.length();
	privateKey = new byte[length];
	
	FileInputStream in = new FileInputStream(file);
	int n;
	int count = 0;

	while (count < length &&
	       ((n = in.read(privateKey, count, length - count)) > 0)) {
	    count += n;
	}
	in.close();
    } // readKey
    

    /** @return the private key of the new certificate. */
    protected byte[] getPrivateKey() {
	return privateKey;
    }
    

    /** @return the password of the private key. */
    protected String getKeyPassword() {
	return keyPassword;
    }


    /** @return the new certificate. */
    protected byte[] getCertificate() {
	return certificate;
    }

    /** @return the complete certificate chain. */
    protected byte[] getCertChain() {
	return certChain;
    }

    /** @return all the trusted root certs. */
    protected byte[] getTrustedCerts() {
	return trustedCerts;
    }

    /** @return return the cert and its chain in KeyStore format. */
    protected byte[] getKeyStore() {
	return keyStore;
    }

    /** @return the trusted certs in KeyStore format. */
    protected byte[] getTrustedStore() {
	return trustedStore;
    }

    /** @return the chain ID of the signer. */
    protected byte[] getChainID() {
	return chainID;
    }
}

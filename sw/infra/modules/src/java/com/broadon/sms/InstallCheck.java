package com.broadon.sms;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.sql.*;
import java.security.cert.X509Certificate;
import java.security.*;
import java.security.Signature;
import java.security.spec.RSAPublicKeySpec;
import java.math.BigInteger;

import com.ncipher.km.nfkm.*;

import com.broadon.util.Database;
import com.broadon.security.X509;

public class InstallCheck implements Keywords
{
    public static void main(String[] args) {

	if (args.length < 1) {
	    System.err.println("Usage: InstallCheck config_file");
	    return;
	}

	try {
	    new InstallCheck(args[0]);
	    System.exit(0);
	} catch (Exception e) {
	    e.printStackTrace();
	    System.exit(1);
	}
    }


    Connection conn;
    boolean useSoftHSM;

    InstallCheck(String configFile) throws Exception
    {
	// setup database
	Database db = new Database();
	conn = db.getConnection();

	useSoftHSM = false;
	String certFile = verifyConfig(configFile);
	System.out.println("Configuration file " + configFile + " verified.");
	Properties prop = verifyCertConfig(certFile);

	// set up HSM
	System.out.println("Verifying direct connection to HSM.");
	com.broadon.sms.Signature signer;
	if (useSoftHSM) {
	    signer = new SoftSignature(prop);
	} else {
	    SecurityWorld sw = new SecurityWorld();
	    signer = new HardSignature(sw, sw.getConnection());
	}
	
	verifySignerKeys(signer);

	System.out.println("All functions verified.");
    }

    String mustHave(Properties p, String key) throws Exception
    {
	String s = p.getProperty(key);
	if (s == null)
	    throw new Exception("Missing config parameter: " + key);
	return s;
    }
    

    String verifyConfig(String configFile) throws Exception
    {
	Properties p = new Properties();
	FileInputStream in = new FileInputStream(configFile);
	p.load(in);
	in.close();

	String s;
	int n;
   	n = Integer.parseInt(mustHave(p, HSM_PORT));
	if (n <= 1024 || n > 65535)
	    throw new Exception("Invalid port number on " + HSM_PORT);
	
	n = Integer.parseInt(mustHave(p, SHUTDOWN_PORT));
	if (n <= 1024 || n > 65535)
	    throw new Exception("Invalid port number on " + SHUTDOWN_PORT);
	
	mustHave(p, SHUTDOWN_CMD);

	s = mustHave(p, LOG_DIR);
	File file = new File(s);
	if (! (file.isDirectory() && file.canWrite()))
	    throw new Exception("Log directory does not exist or not writable: " + s);

	if ((s = p.getProperty(LOG_SIZE)) != null)
	    Integer.parseInt(s);

	if ((s = p.getProperty(LOG_COUNT)) != null)
	    Integer.parseInt(s);
	
	if ((s = p.getProperty(LOG_LEVEL)) != null)
	    Level.parse(s);
	
	return mustHave(p, CA_CONFIG);
    } // verifyConfig


    Properties verifyCertConfig(String configFile) throws Exception
    {
	Properties p = new Properties();
	FileInputStream in = new FileInputStream(configFile);
	p.load(in);
	in.close();

	File file = new File(mustHave(p, CERT_TMPDIR));
	if (! (file.isDirectory() && file.canWrite()))
	    throw new Exception("Temp. directory does not exist or not writable");
	if ("true".equals(p.getProperty(USE_SSM_KEY)))
	    useSoftHSM = true;

	file = new File(mustHave(p, CERT_HOME));
	X509 x509 = new X509();
	KeyFactory kf = KeyFactory.getInstance("RSA");
	Signature signature = Signature.getInstance("SHA1withRSA");

	for (Enumeration e = p.propertyNames(); e.hasMoreElements(); ) {
	    String s = (String) e.nextElement();
	    if (s.startsWith(CERT_TYPE_PREFIX)) {
		verifyCert(kf, signature, x509, file, p.getProperty(s));
            }
	}
	return p;
    }


    void verifyCert(KeyFactory kf, Signature signature, X509 x509,
		    File dir, String spec)
	throws Exception
    {
	StringTokenizer st = new StringTokenizer(spec);
	File file = new File(dir, st.nextToken());
	if (! (file.isFile() && file.canRead()))
	    throw new Exception("Missing config file: " + file.getName());
	String certFile = st.nextToken();
	String keyFile = st.nextToken();

	file = new File(dir, st.nextToken());
	if (! file.isFile())
	    throw new Exception("Missing serial file: " + file.getName());

	// check if the last argument exists and is an integer
	Integer.parseInt(st.nextToken());

	// now, verify the cert.
	System.out.println("Verifying cert file: " + certFile);
	X509Certificate cert = x509.readX509(certFile);
	String certID = x509.genUniqueID(cert);
	byte[] document = cert.getEncoded();

	System.out.println("Verifying signing with key file: " + keyFile);
	byte[] sig = opensslSign(document, keyFile);

	// read the public key from the cert file, and verify it against the
	// signature returned from signing the document using the key file.

	System.out.println("\tVerifying cert file against database records.");
	PublicKey publicKey = readPublicKey(kf, certID);
	if (publicKey == null)
	    throw new Exception("Cannot read public key for " + certFile);

	signature.initVerify(publicKey);
	signature.update(document);
	if (! signature.verify(sig))
	    throw new Exception("Certificate file " + certFile +
				" and key file " + keyFile + " do not match");

	// verify the cert Chain
	System.out.println("\tVerifying corresponding cert chain.");
	if (! verifyCertChain(certID))
	    throw new Exception("Cannot locate cert chain for " + certFile);
    }

    static final String softCmd = "openssl dgst -sha1 -sign ";
    static final String hardCmd = "openssl dgst -engine chil -sha1 -sign ";

    byte[] opensslSign(byte[] document, String keyFile)
	throws Exception
    {
	String cmd = (useSoftHSM ? softCmd : hardCmd);
	Process proc = Runtime.getRuntime().exec(cmd + keyFile);
	OutputStream out = proc.getOutputStream();
	out.write(document);
	out.close();

	InputStream in = proc.getInputStream();
	byte[] buffer = new byte[128];
	ByteArrayOutputStream sig = new ByteArrayOutputStream(128);
	int n = 0;
	while ((n = in.read(buffer)) > 0)
	    sig.write(buffer, 0, n);
	in.close();

	if (proc.waitFor() != 0)
	    throw new Exception("Cannot sign using key: " + keyFile);

	return sig.toByteArray();
    }


    static final String READ_KEY =
	"SELECT PUBLIC_KEY FROM CERTIFICATES WHERE CERT_ID = ?";
	
    PublicKey readPublicKey(KeyFactory kf, String certID) throws Exception
    {
	PreparedStatement ps = conn.prepareStatement(READ_KEY);
	ps.setString(1, certID);
	ResultSet rs = ps.executeQuery();
	try {
	    if (rs.next()) {
		BigInteger modulus = new BigInteger(rs.getString(1), 32);
		BigInteger exponent = new BigInteger("10001", 16);
		return kf.generatePublic(new RSAPublicKeySpec(modulus, exponent));
	    } else
		return null;
	} finally {
	    rs.close();
	    ps.close();
	}
    }

    
    static final String VERIFY_CHAIN =
	"SELECT COUNT(*) FROM CERTIFICATES, CERTIFICATE_CHAINS WHERE " +
	"   SIGNER_CERT_ID = ? AND CA_CERT_ID = CERT_ID";

    boolean verifyCertChain(String certID) throws Exception
    {
	PreparedStatement ps = conn.prepareStatement(VERIFY_CHAIN);
	ps.setString(1, certID);
	ResultSet rs = ps.executeQuery();
	try {
	    if (rs.next()) {
		if (rs.getInt(1) == 1)
		    return true;
	    }
	    return false;
	} finally {
	    rs.close();
	    ps.close();
	}
    }


    static final String GET_KEYS =
	"SELECT DISTINCT KEY_NAME FROM REGIONAL_HSM_CERTS A, " +
	"                              REGIONAL_CENTERS B WHERE " +
	"   IS_CURRENT != 0 AND A.REGIONAL_CENTER_ID = B.REGIONAL_CENTER_ID";

    void verifySignerKeys(com.broadon.sms.Signature signer) throws Exception
    {
	PreparedStatement ps = conn.prepareStatement(GET_KEYS);
	ResultSet rs = ps.executeQuery();

	// just pick some arbitrary buffer to sign
	byte[] document = GET_KEYS.getBytes();
	try {
	    while (rs.next()) {
		String key = rs.getString(1);
		try {
		    System.out.println("Verifying stored key: " + key);
		    signer.sign(key, document, false);
		} catch (Exception e) {
		    System.err.println("Failed to sign with key " + key);
		    throw e;
		}
	    }
	} finally {
	    rs.close();
	    ps.close();
	}
    }
}

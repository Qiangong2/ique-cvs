package com.broadon.sms;

import java.util.Properties;
import java.io.*;
import java.net.*;

import com.broadon.hsm.*;

public class SelfCheck
{
    static final String restart = "ERROR_RESTART";
    static final String ok = "OK";

    public static void main(String[] args) {
	if (args.length < 1) {
	    System.err.println("Usage: SelfCheck config_file");
	    return;
	}

	try {
	    Properties p = new Properties();
	    p.load(new FileInputStream(args[0]));
	    int port = Integer.parseInt(p.getProperty("HSM_SERVICE_PORT"));

	    // compose the command header
	    HSMHeaderOutputStream header =
		new HSMHeaderOutputStream(Constants.HEADER_SIZE);
	    PrintWriter writer = new PrintWriter(header);
	    writer.println(Constants.VERSION + " = " + Constants.VERSION_STRING);
	    writer.println(Constants.COMMAND + " = " + Constants.CMD_SELF_CHECK);
	    writer.close();
	    header.pad(' ');

	    // send the command via localhost 
	    Socket sock = new Socket("localhost", port);
	    OutputStream out = sock.getOutputStream();
	    out.write(header.getInternalBuf());
	    out.flush();
	    sock.shutdownOutput();

	    // wait for reply
	    sock.setSoTimeout(30000);
	    InputStream in = sock.getInputStream();
	    byte[] buffer = new byte[10];
	    int length = in.read(buffer);
	    sock.close();

	    // print result
	    if (! "OK".equals(new String(buffer, 0, length)))
		System.out.println(restart);
	    else
		System.out.println(ok);
	} catch (Exception e) {
	    System.out.println(restart);
	}
    }
}

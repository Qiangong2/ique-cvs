package com.broadon.sms;

import java.security.GeneralSecurityException;
import com.ncipher.nfast.NFException;

/**
 * handle common exceptions that comes from accessing the HSM.
 */
public class HardError
{
    static GeneralSecurityException error(Exception e) {
	GeneralSecurityException ge =
	    new GeneralSecurityException(e.getMessage());
	ge.setStackTrace(e.getStackTrace());
	return ge;
    }

    static NFException fatal(Exception e) {
	NFException nfe = new NFException(e.getMessage());
	nfe.setStackTrace(e.getStackTrace());
	return nfe;
    }
}

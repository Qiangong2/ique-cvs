package com.broadon.sms;

import java.io.*;
import java.util.*;
import java.sql.*;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.util.logging.Logger;
import java.util.logging.Level;
import com.ncipher.nfast.NFException;
import javax.sql.DataSource;
import oracle.jdbc.OracleConnectionWrapper;

import com.broadon.db.OracleDataSource;

/**
 * Generate X509 certificates.
 */
public class CertGenerator implements Keywords
{
    static final char[] TRUSTED_STORE_PASSWORD = "changeit".toCharArray();
    static final char[] KEYSTORE_PASSWORD = "serverpw".toCharArray();

    private static String certHome;
    private static File certTmpdir;
    private static HashMap configMap;
    KeyStoreWriter kw;
    private static DataSource ds;
    Connection conn;
    Connection wrappedConn;

    private static void initDB() 
        throws IOException, SQLException
    {
        Properties dbProp = new Properties();
        String dbPropStr = System.getProperty(DBproperty);
        if (dbPropStr == null)
            throw new IOException("DB.properties file undefined");
        dbProp.load(new FileInputStream(dbPropStr));

        String dbCacheName = "HSM_SERVER";
	ds = OracleDataSource.createDataSource(
               dbProp.getProperty(DB_user),
               dbProp.getProperty(DB_password),
               dbProp.getProperty(DB_url),
               dbCacheName);
    }

    /**
     * Configure the CertGenerator with the certificates, private key,
     * serial number database, and working directories of various CA.
     * @param prop  Properties containing configuration parameters.
     * @exception IOException Any error in setting up the necessary files.
     */
    protected static void init(Properties prop)
	throws IOException, GeneralSecurityException, SQLException
    {
        initDB();
	certHome = prop.getProperty(CERT_HOME);
	certTmpdir = new File(prop.getProperty(CERT_TMPDIR));

	KeyStoreWriter keyStoreWriter = new KeyStoreWriter();

	// load the CA map
	configMap = new HashMap();

        Connection connection = null;
        Connection wrappedConnection = null;
        connection = wrappedConnection = ds.getConnection();
	connection = ((OracleConnectionWrapper) connection).unwrap();
    
        try {
	    for (Enumeration e = prop.propertyNames(); e.hasMoreElements(); ) {
	        String p = (String) e.nextElement();
	        if (p.startsWith(CERT_TYPE_PREFIX)) {
	            String key = p.substring(CERT_TYPE_PREFIX.length());
		    CAconfig value =
		        new CAconfig(certHome, prop.getProperty(p),
		    		     key.equals(CAconfig.SERVER_CERT),
				     keyStoreWriter, TRUSTED_STORE_PASSWORD, 
                                     connection);
		    configMap.put(key, value);
	        }
	    }
         } finally {
             try {
                 if (wrappedConnection != null) wrappedConnection.close();
             } catch (Exception ex) {}
         }

	// set up tmpdir
	if (certTmpdir.exists())
	    rmdir(certTmpdir);
	if (! certTmpdir.mkdirs()) {
	    throw new IOException ("Cannot create tmpdir: " +
				   certTmpdir.getName());
	}
	IssueCert.setTmpdir(certTmpdir);
	if ("true".equals(prop.getProperty(USE_SSM_KEY)))
	    IssueCert.setEngine("openssl");

	// set up serial number file
	String newSerial = Long.toHexString(Long.parseLong(prop.getProperty(SIGNER_ID_KEY)) << 16);
	if (newSerial.length() % 2 != 0)
	    newSerial = '0' + newSerial;
	for (Iterator i = configMap.keySet().iterator(); i.hasNext(); ) {
	    CAconfig conf = (CAconfig) configMap.get(i.next());
	    File serialFile = new File(conf.serialFile);
	    if (serialFile.exists())
		continue;
	    PrintWriter out = new PrintWriter(new FileOutputStream(serialFile));
	    out.println(newSerial);
	    out.close();
	}
    }

    protected CertGenerator() throws GeneralSecurityException
    {
	kw = new KeyStoreWriter();
    }

    private void getConnection() throws SQLException 
    {
       conn = wrappedConn = ds.getConnection();
       conn = ((OracleDataSource.ConnectionWrapper)conn).unwrap();
    }

    private void closeConnection() 
    {
       try {
           if (wrappedConn != null) {
               wrappedConn.close();
           }
       } catch (Exception ex) {}
       conn = wrappedConn = null;
    }

    void reConnectDatabase(Logger log) throws SQLException
    {
        closeConnection();
	try {
	    getConnection();
	    return;
	} catch (SQLException e) {
	    log.log(Level.WARNING, "Cannot connect to database, re-trying ...",
		    e);
	    // retry connection, if fails again, let the exception be thrown
	    conn = wrappedConn = null;
	    try {
		initDB();
	    } catch (IOException ioe) {
		SQLException sql = new SQLException(ioe.getMessage());
		sql.setStackTrace(ioe.getStackTrace());
		throw sql;
	    }
	    getConnection();
	}
    }


    static final String addCert =
	"INSERT INTO SERVER_CERTIFICATES (CHAIN_ID, SERIAL_NO, CN, CERTIFICATE) " +
	"VALUES (?, ?, ?, ?)";
    
    private void updateDatabase(CAconfig conf, byte[] rawCert, long serialNo,
	         		String CN)
	throws SQLException
    {
	PreparedStatement ps = null;
        try {
	    ps = conn.prepareStatement(addCert);
	    ps.setLong(1, conf.chainID);
	    ps.setLong(2, serialNo);
	    ps.setString(3, CN);
	    ps.setString(4, new String(rawCert));
	    ps.executeUpdate();
	    conn.commit();
        } finally {
            try {
                if (ps != null) ps.close();
            } catch (Exception ex) {}
        }
    } // updateDatabase


    /**
     * Generate a certificate.
     * @param type The type of certificate in the BroadOn PKI
     * hierarchy to be generated.
     * @param name Specify the "common name" in the Subject DN of the
     * cert.
     * @param password Password for protecting the private key.
     * @param log For logging errors and status message
     * @exception GeneralSecurityException Any error in generating
     * such a cert.
     * @exception SQLException Fail to connect to database
     * @return An <code>IssueCert</code> object, which contains the
     * certificate, the private key and its corresponding password.
     */
    protected IssueCert generateCert(String type, String name, String password,
				     Logger log)
	throws GeneralSecurityException, SQLException
    {
	CAconfig conf = (CAconfig) configMap.get(type);
	
	try {
	    IssueCert results =
		new IssueCert(conf, kw, KEYSTORE_PASSWORD, password, name);
	    if (conf.hasKeyStore) {
		// need to update the database
		reConnectDatabase(log);
		
		try {
		    BigInteger serialNo = kw.generateCert(results.getCertificate()).getSerialNumber();
		    updateDatabase(conf,
				   results.getCertificate(),
				   serialNo.longValue(),
				   name);
		} catch (SQLException e) {
		    throw new GeneralSecurityException(e.getMessage());
		} finally {
                  closeConnection();
                }
	    }
	    return results;
	} catch (IOException e) {
	    throw new GeneralSecurityException(e.getMessage());
	}
	
    } // generateCert


    /**
     * Generate a certificate.
     * @param type The type of certificate in the BroadOn PKI
     * hierarchy to be generated.
     * @param name Specify the "common name" in the Subject DN of the
     * cert.
     * @param rand Random number generator.
     * @param log For errors and status messages.
     * @exception GeneralSecurityException Any error in generating
     * such a cert.
     * @exception SQLException Fail to connect to database
     * @return An <code>IssueCert</code> object, which contains the
     * certificate, the private key and its corresponding password.
     */
    protected IssueCert generateCert(String type, String name, Random rand,
				     Logger log)
	throws GeneralSecurityException, SQLException, NFException
    {
	// password is 8 decimal digits.
	long passcode = (new BigInteger(1, rand.getRandom(4))).longValue();
	String password = String.valueOf(passcode % 100000000);
	if (password.length() < 8) {
	    // in this rare case, need to insert leading zero's
	    StringBuffer b = new StringBuffer("00000000");
	    int len = password.length();
	    b.replace(8 - len, 8, password);
	    password = b.toString();
	}

	return generateCert(type, name, password, log);
    } // generateCert


    // recursivly remove directories
    private static void rmdir(File root) throws IOException {
	if (root.isDirectory()) {
	    File[] list = root.listFiles();
	    for (int i = 0; i < list.length; ++i) {
		rmdir(list[i]);
	    }
	}
	
	if (! root.delete())
	    throw new IOException("Cannot delete " + root.getName());
    } // rmdir

    protected void testDatabase(Logger log) throws SQLException
    {
	reConnectDatabase(log);
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
	    ps = conn.prepareStatement("SELECT SYSDATE FROM DUAL");
	    rs = ps.executeQuery();
	    if (!rs.next())
	        throw new SQLException("Cannot execute trivial query");
        } finally {
            try{
                if (rs != null) rs.close();
                if (ps != null) ps.close();
            } catch (Exception ex) {}
            closeConnection();
        }
    }
}

package com.broadon.sms;

import java.util.*;
import java.io.IOException;
import java.sql.*;
import java.security.GeneralSecurityException;
import java.security.cert.X509Certificate;

import com.broadon.security.X509;

public class CAconfig
{
    public String configFile;
    public String certFile;
    public long chainID;	// chain ID of certFile
    public String keyFile;
    public String serialFile;
    public int days;

    public byte[] certChain;	// include certFile up to Root.
    public byte[] trustedCerts;

    public X509Certificate[] ksChain; // for used in key store, exclude Root.
    public byte[] trustedStore;

    public boolean hasKeyStore;

    static final String SERVER_CERT = "server";
    static final String DEPOT_CERT = "depot";
    static final String SMART_CARD_CERT = "card";
    
    static final String GET_CHAIN_ID =
	"SELECT CHAIN_ID FROM CERTIFICATE_CHAINS WHERE SIGNER_CERT_ID = ?";

    static final String GET_CHAIN =
	"SELECT CERTIFICATE, CERT_ID FROM CERTIFICATES, CERTIFICATE_CHAINS " +
	"WHERE CERT_ID = CA_CERT_ID AND SIGNER_CERT_ID = ?";

    static final String GET_ROOT =
	"SELECT CERTIFICATE FROM CERTIFICATES WHERE CERT_ID NOT IN " +
        "(SELECT SIGNER_CERT_ID FROM CERTIFICATE_CHAINS)";

    
    String makePath(String dir, String file) {
	if (file.startsWith("/"))
	    return file;
	else
	    return dir + '/' + file;
    }

    void parseConfig(String certHome, String conf) {
	StringTokenizer st = new StringTokenizer(conf);
	configFile = makePath(certHome, st.nextToken());
	certFile = makePath(certHome, st.nextToken());
	keyFile = makePath(certHome, st.nextToken());
	serialFile = makePath(certHome, st.nextToken());
	days = Integer.parseInt(st.nextToken());
    }	


    void setChainID(String certID, Connection conn)
	throws SQLException
    {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
	    ps = conn.prepareStatement(GET_CHAIN_ID);
	    ps.setString(1, certID);
	    rs = ps.executeQuery();
	    if (rs.next()) {
	        chainID = rs.getLong(1);
	    } else
	        chainID = -1;
        } finally {
            try{
	        if (rs != null) rs.close();
	        if (ps != null) ps.close();
            } catch (Exception ex) {}
        } 
	if (chainID == -1)
	    throw new SQLException("Cannot find chain ID");
    }

    String getIssuer(String certID, PreparedStatement ps, ArrayList certs)
	throws SQLException
    {
	ps.setString(1, certID);
        ResultSet rs = null;
	String cert = null;
        try {
	    rs = ps.executeQuery();
	    if (rs.next()) {
	        certs.add(rs.getString(1));
	        cert = rs.getString(2);
	    }
        } finally {
            try{
	        if (rs != null) rs.close();
            } catch (Exception ex) {}
        } 
	return cert;
    } // getIssuer
    

    ArrayList getRoots(Connection conn)
	throws SQLException
    {
        PreparedStatement ps = null;
        ResultSet rs = null;
	ArrayList roots = null;
        try {
	    ps = conn.prepareStatement(GET_ROOT);
	    rs = ps.executeQuery();
	    roots = new ArrayList(1);
	    while (rs.next()) {
	        roots.add(rs.getString(1));
	    }
        } finally {
            try{
	        if (rs != null) rs.close();
	        if (ps != null) ps.close();
            } catch (Exception ex) {}
        } 
	return roots;
    } // getRoots
    

    void readCertChains(KeyStoreWriter kw, char[] trustedStorePassword,
			Connection conn)
	throws GeneralSecurityException, IOException, SQLException
    {
	X509 x509 = new X509();
	X509Certificate c = x509.readX509(certFile);
	
	ArrayList certs = new ArrayList(3);
	certs.add(x509.encode(c));

	String id = x509.genUniqueID(c);

	setChainID(id, conn);

	// read the entire chain
        PreparedStatement ps = null;
        try {
	    ps = conn.prepareStatement(GET_CHAIN);
	    while ((id = getIssuer(id, ps, certs)) != null);
        } finally {
            try{
	        if (ps != null) ps.close();
            } catch (Exception ex) {}
        } 

	ArrayList trusted = getRoots(conn);

	// format the chain and trusted certs
	String s = "";
	for (int i = 0; i < certs.size(); ++i)
	    s += (String) certs.get(i);
	certChain = s.getBytes();

	s = "";
	for (int i = 0; i < trusted.size(); ++i)
	    s += (String) trusted.get(i);
	trustedCerts = s.getBytes();

	if (hasKeyStore) {
	    // convert the chain to format suitable to be placed in a KeyStore
	    ksChain = kw.generateCerts(certs, 0, certs.size() - 1);
	    
	    trustedStore = kw.getTrustedStore(kw.generateCerts(trusted),
					      trustedStorePassword);
	} else {
	    ksChain = null;
	    trustedStore = null;
	}
	    
    } // readCertChains


    protected CAconfig(String certHome, String conf, boolean needKeyStore,
		       KeyStoreWriter kw, char[] trustedStorePassword,
		       Connection conn)
	throws GeneralSecurityException, IOException, SQLException
    {
	hasKeyStore = needKeyStore;
	parseConfig(certHome, conf);
	readCertChains(kw, trustedStorePassword, conn);
    }
}

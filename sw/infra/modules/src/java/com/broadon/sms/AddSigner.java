package com.broadon.sms;

import java.sql.*;
import java.io.*;
import java.security.cert.X509Certificate;

import com.broadon.security.X509;
import com.broadon.security.RSAcert;
import com.broadon.util.Database;

/**
 * For each HSM at each regional center, we need a mapping from the
 * content object type to the key id of the key used to sign a cert.
 */
public class AddSigner
{
    static final String getChain =
	"SELECT CHAIN_ID FROM CERTIFICATE_CHAINS WHERE SIGNER_CERT_ID = ?";

    static final String deleteKey =
	"DELETE FROM REGIONAL_HSM_CERTS WHERE REGIONAL_CENTER_ID=? AND " +
	"CERT_TYPE=?";

    static final String addKey =
	"INSERT INTO REGIONAL_HSM_CERTS (REGIONAL_CENTER_ID, CERT_TYPE, " +
	"CHAIN_ID, KEY_NAME) VALUES (?, ?, ?, ?)";

    static int getChainID(Connection conn, String certID)
	throws SQLException
    {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
	    ps = conn.prepareStatement(getChain);
	    ps.setString(1, certID);
	    rs = ps.executeQuery();
	    if (rs.next())
	        return rs.getInt(1);
	    else
	        throw new SQLException("Cannot find chain ID");
        } finally {
            try {
                if (ps != null) ps.close();
                if (rs != null) rs.close();
            } catch (Exception ex) {}
        }
    }
    

    static String getCertID(String certfile)
	throws Exception
    {
	File file = new File(certfile);
	byte[] certBuffer = new byte[(int) file.length()];
	FileInputStream in = new FileInputStream(file);
	in.read(certBuffer);
	in.close();
	if (new String(certBuffer).startsWith(X509.X509_HEADER)) {
	    X509 x509 = new X509();
	    X509Certificate cert =
		x509.readEncodedX509(new ByteArrayInputStream(certBuffer));
	    return x509.genUniqueID(cert);
	} else {
	    RSAcert cert = new RSAcert(certBuffer);
	    return cert.uniqueID;
	}
    } // getCertID


    public static void main(String[] args)
    {
	if (args.length < 4) {
	    System.err.println("Usage: AddSigner certfile region_center content_type key_name");
	    return;
	}

	Connection conn = null;
	try {
	    // read the certID
	    String certID = getCertID(args[0]);

	    Database db = new Database();
	    conn = db.getConnection();
	    conn.setAutoCommit(false);

	    int chainID = getChainID(conn, certID);
	    
	    // remove the old entry, if any
	    PreparedStatement pstmt = conn.prepareStatement(deleteKey);
	    pstmt.setInt(1, Integer.parseInt(args[1]));
	    pstmt.setString(2, args[2]);
	    pstmt.executeUpdate();
	    pstmt.close();

	    // write it to the database
	    pstmt = conn.prepareStatement(addKey);
	    pstmt.setInt(1, Integer.parseInt(args[1]));
	    pstmt.setString(2, args[2]);
	    pstmt.setInt(3, chainID);
	    pstmt.setString(4, args[3]);

	    pstmt.executeUpdate();

	    conn.commit();
	    conn.close();
	    conn = null;

	    System.out.println("Key \"" + args[3] + "\" for content type " +
			       args[2] + " added as chain " + chainID);
	    
	} catch (Exception e) {
	    if (conn != null) {
		try {
		    conn.rollback();
		    conn.close();
		} catch (SQLException sql) {
		    sql.printStackTrace();
		}
	    }
	    e.printStackTrace();
	}

    }
}

package com.broadon.sms;

import java.util.logging.Logger;
import java.util.Properties;
import java.sql.SQLException;
import java.io.IOException;
import java.io.FileInputStream;
import java.security.GeneralSecurityException;
import com.ncipher.km.nfkm.*;
import com.ncipher.nfast.connect.NFConnection;
import com.ncipher.nfast.NFException;

/**
 * Serialize access to the HSM.
 */
public class Dispatcher implements Keywords
{
    Random random;
    Signature signature;
    CertGenerator certGenerator;
    private static Logger log;
    Object sync = new Object();
    
    private static Properties prop = null;

    private void initSoftFunctions()
	throws IOException
    {
	random = new SoftRandom(); 
	signature = new SoftSignature(prop);
    }
    

    private void initHardFunctions()
	throws Exception
    {
	SecurityWorld sw = new SecurityWorld();
	NFConnection conn = sw.getConnection();
	random = new HardRandom(conn);
	signature = new HardSignature(sw, conn);
    }

    protected static void init(String configFile, Logger logger) 
	throws Exception
    {
	log = logger;
	prop = new Properties();
	FileInputStream in = new FileInputStream(configFile);
	prop.load(in);
	in.close();
        CertGenerator.init(prop);
    }

    protected Dispatcher()
	throws Exception
    {
	if ("true".equals(prop.getProperty(USE_SSM_KEY))) {
	    initSoftFunctions();
	} else
	    initHardFunctions();
	certGenerator = new CertGenerator();
    }


    /**
     * Digitally sign a block of data.
     * @param signer Identify a private key to be used to sign the
     * document.
     * @param document Document (block of data) to be signed.
     * @param hashed <code>document</code> is a hashed value.
     * @return The signature.
     * @exception GeneralSecurityException
     */
    protected byte[] sign(String signer, byte[] document, boolean hashed)
	throws GeneralSecurityException, NFException
    {
	return signature.sign(signer, document, hashed);
    }

    
    /**
     * Return a sequence of random bytes.
     * @param size Number of random bytes requested.
     * @return Random bytes.
     */
    protected byte[] getRandom(int size)
	throws GeneralSecurityException, NFException
    { 
	return random.getRandom(size);
    }


    /**
     * Generate a X509 certificate.
     * @param type The type of certificate under the Broad<i>On</i>
     * PKI hierarchy requested.
     * @param name Specify the "Common Name" of the certificate.
     * @return An <code>IssueCert</code> object, which contains the
     * certificate, the private key and its corresponding password.
     * @exception GeneralSecurityException Any error in generating
     * such a cert.
     */
    protected IssueCert genCert(String type, String name, String password)
	throws GeneralSecurityException, NFException, SQLException
    {
	synchronized (certGenerator) {
	    return (password == null) ?
		certGenerator.generateCert(type, name, random, log) :
		certGenerator.generateCert(type, name, password, log);
	}
    }

    protected void testDB() throws SQLException
    {
	synchronized (certGenerator) {
	    certGenerator.testDatabase(log);
	}
    }
}

package com.broadon.sms;

import java.util.HashMap;
import java.security.*;
import java.io.IOException;

import com.ncipher.km.nfkm.*;
import com.ncipher.nfast.NFException;
import com.ncipher.nfast.marshall.*;
import com.ncipher.nfast.connect.NFConnection;
import com.ncipher.nfast.connect.CommandTooBig;
import com.ncipher.nfast.connect.ObjectNotAnOutstandingRequest;

/**
 * Implement a hardware-based (nCipher HSM) signing service.
 */
public class HardSignature implements Signature
{
    NFConnection conn;
    HashMap keyMap;		// for key handle
    M_KeyID lastKey;
    int lastKeyLength;
    String lastSigner;

    M_Command signCmd;
    M_Cmd_Args_Sign signArgs;

    M_Command keyInfoCmd;
    M_Cmd_Args_GetKeyInfoEx keyInfoArgs;

    int maxCmdSize;


    // return the length of a key -- in number of bytes
    private int getKeyLength(M_KeyID id)
	throws NFException, InterruptedException
    {
	keyInfoArgs.key = id;
	M_Cmd_Reply_GetKeyInfoEx reply = (M_Cmd_Reply_GetKeyInfoEx)
	    conn.wait(conn.submit(keyInfoCmd), 10000).reply;
	if (reply == null)
	    throw new NFException("Access timeout");
	return (int) (reply.length.value + 7) / 8;
    }

    /**
     * Initialize the HSM for signing.
     * @param sw Security world loaded into the HSM.
     * @param conn Connection to the HSM.
     * @exception NFException Any intialization error.
     */
    protected HardSignature(SecurityWorld sw, NFConnection conn)
	throws NFException
    {
	this.conn = conn;

	// load all the relevant keys
	keyMap = new HashMap();
	com.ncipher.km.nfkm.Key[] k = sw.listKeys(null);
	Module module = sw.getModule(1);
	Slot slot = module.getSlot(0);
	for (int i = 0; i < k.length; ++i) {
	    M_KeyID id = null;
	    if (k[i].isModuleProtected()) {
		id = k[i].load(module);
	    } else if (k[i].isCardSetProtected()) {
		id = k[i].load(slot);
	    } else
		continue;
	    keyMap.put(k[i].getName(), id);
	}
	lastSigner = null;
	lastKey = null;
	lastKeyLength = 0;

	signArgs = new M_Cmd_Args_Sign(0, lastKey, M_Mech.Any, null);
	signArgs.plain = new M_PlainText(M_PlainTextType.Bytes,
				     new M_PlainTextType_Data_Bytes(new M_ByteBlock()));
	signCmd = new M_Command(M_Cmd.Sign, 0, signArgs);

	keyInfoArgs = new M_Cmd_Args_GetKeyInfoEx(0, null);
	keyInfoCmd = new M_Command(M_Cmd.GetKeyInfoEx, 0, keyInfoArgs);

	maxCmdSize = conn.getMaxCommandSize();
    } // HardSignature


    byte[] doSign(M_Command cmd)
	throws NFException, InterruptedException
    {
	M_Cmd_Reply_Sign rep;
	synchronized (conn) {
	    rep = (M_Cmd_Reply_Sign) conn.wait(conn.submit(cmd), 10000).reply;
	}
	if (rep == null)
	    throw new NFException("Access timeout");
	
	M_Mech_Cipher_RSApPKCS1 rsa = (M_Mech_Cipher_RSApPKCS1)rep.sig.data;
	byte[] sig = rsa.m.value.toByteArray();
	if (sig.length != lastKeyLength) {
	    byte[] newSig = new byte[lastKeyLength];
	    if (sig.length < lastKeyLength) {
		int offset = lastKeyLength - sig.length;
		for (int i = 0; i < offset; ++i) {
		    newSig[i] = 0;
		}
		System.arraycopy(sig, 0, newSig, offset, sig.length);
	    } else {
		System.arraycopy(sig, sig.length - lastKeyLength,
				 newSig, 0, lastKeyLength);
	    }
	    return newSig;
	} else
	    return sig;
    } // doSign
    

    void setDocument(M_PlainText plain, byte[] document) {
	M_PlainTextType_Data_Bytes data =
	    (M_PlainTextType_Data_Bytes) plain.data;
	data.data.value = document;
    }


    public byte[] sign(String signer, byte[] document, boolean hashed)
	throws GeneralSecurityException, NFException
    {
	try {
	    if (! signer.equals(lastSigner)) {
		lastSigner = signer;
		lastKey = (M_KeyID) keyMap.get(lastSigner);
		if (lastKey == null)
		    throw new InvalidKeyException("Signer " + signer + " not found");
		signArgs.key = lastKey;
		lastKeyLength = getKeyLength(lastKey);
	    }

	    /*
	    if (document.length <= maxCmdSize) {
		setDocument(signArgs.plain, document);
		return doSign(signCmd);
	    }
	    */
	    
	    return signBigData(document, hashed);
	} catch (MarshallTypeError e) {
	    throw HardError.error(e);
	} catch (CommandTooBig e) {
	    throw HardError.error(e);
	} catch (ObjectNotAnOutstandingRequest e) {
	    throw HardError.error(e);
	} catch (NFException e) {
	    throw HardError.fatal(e);
	} catch (InterruptedException e) {
	    throw HardError.fatal(e);
	}   
    } // sign


    M_Hash20 doHash(byte[] document)
	throws NFException, InterruptedException
    {
	M_PlainText plain =
	    new M_PlainText(M_PlainTextType.Bytes,
			    new M_PlainTextType_Data_Bytes(new M_ByteBlock(document)));
	M_Cmd_Args_Hash hashArgs =
	    new M_Cmd_Args_Hash(0, M_Mech.SHA1Hash, plain);
	M_Command cmd = new M_Command(M_Cmd.Hash, 0, hashArgs);
	    
	M_Cmd_Reply_Hash hash;
	synchronized (conn) {
	    hash = (M_Cmd_Reply_Hash) conn.wait(conn.submit(cmd), 10000).reply;
	}
	    
	if (hash == null)
	    throw new NFException("Access timeout");
	    
	return ((M_Mech_Cipher_SHA1Hash) hash.sig.data).h;
    }


    byte[] signBigData(byte[] data, boolean hashed)
	throws NFException, InterruptedException
    {
	M_Hash20 hash;

	if (hashed) {
	    // already hashed
	    hash = new M_Hash20();
	    hash.value = data;
	} else {
	    hash = doHash(data);
	}
	
	M_PlainText plain =
	    new M_PlainText(M_PlainTextType.Hash,
			    new M_PlainTextType_Data_Hash(hash));

	M_Command cmd =
	    new M_Command(M_Cmd.Sign,
			  0,
			  new M_Cmd_Args_Sign(0, lastKey, M_Mech.Any, plain));
	
	return doSign(cmd);
    } // signBigData

    
    GeneralSecurityException error(Exception e) {
	GeneralSecurityException ge =
	    new GeneralSecurityException(e.getMessage());
	ge.setStackTrace(e.getStackTrace());
	return ge;
    }

    IOException fatal(Exception e) {
	IOException ioe = new IOException(e.getMessage());
	ioe.setStackTrace(e.getStackTrace());
	return ioe;
    }
}

package com.broadon.sms;

import java.io.*;
import java.util.*;
import java.security.GeneralSecurityException;

import java.security.MessageDigest; // for testing only

/**
 * Implement a software-based signing service.
 */
public class SoftSignature implements Signature, Keywords
{
    static final String KEYFILE_SUFFIX = "-key.pem";
    static final byte[] shaID = {
	0x30, 0x21, 0x30, 0x09, 0x06, 0x05, 0x2b, 0x0e, 0x03, 0x02,
	0x1a, 0x05, 0x00, 0x04, 0x14
    };

    HashMap keyMap;		// map signer ID to private keys
    String lastSigner;		
    String lastKey;
    Runtime rt;

    void registerKey(String filename) throws IOException
    {
	File file = new File(filename);

	String alias = file.getName();
	if (! alias.endsWith(KEYFILE_SUFFIX))
	    throw new IOException ("Cannot derive key alias from " + filename);
	alias = alias.substring(0, alias.lastIndexOf(KEYFILE_SUFFIX));

	keyMap.put(alias, filename);
    } // registerKey


    /**
     * Initialize the signing service.
     * @param prop configuration file properties
     * @exception IOException Key files related error during set up.
     */
    protected SoftSignature(Properties prop)
	throws IOException
    {
	keyMap = new HashMap();
	lastSigner = null;
	lastKey = null;
	
	String keyFiles = prop.getProperty(KEY_FILE_LIST);

	if (keyFiles == null)
	    return;

	StringTokenizer st = new StringTokenizer(keyFiles, ", \t");
	while (st.hasMoreTokens()) {
	    String filename = st.nextToken();
	    if (filename == null || filename.length() == 0)
		continue;
	    registerKey(filename);
	}

	rt = Runtime.getRuntime();
    }


    // Java does not have an API to directly sign the message digest
    // of a document ( *sigh* !!!).  So we have to call openssl instead.
    byte[] opensslSign(byte[] data, String keyFile)
	throws IOException
    {
	Process proc = rt.exec("openssl rsautl -inkey " + keyFile +
			       " -sign -pkcs");
	try {
	    OutputStream out = proc.getOutputStream();
	    out.write(data);
	    out.close();
	    
	    InputStream in = proc.getInputStream();
	    ByteArrayOutputStream buffer = new ByteArrayOutputStream();
	    byte[] b = new byte[1024];
	    int n;
	    while ((n = in.read(b)) > 0) {
		buffer.write(b, 0, n);
	    }
	    in.close();
	    return buffer.toByteArray();
	} finally {
	    proc.destroy();
	}
    }
    

    public byte[] sign(String signer, byte[] document, boolean hashed)
	throws GeneralSecurityException
    {
	if (! signer.equals(lastSigner)) {
	    String keyfile = (String) keyMap.get(signer);  
	    if (keyfile == null) {
		throw new GeneralSecurityException("Private key not found");
	    }
	    lastSigner = signer;
	    lastKey = keyfile;
	}

	if (! hashed) {
	    MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
            sha1.update(document, 0, document.length);
            document = sha1.digest();
	}

	byte[] buf = new byte[shaID.length + document.length];
	System.arraycopy(shaID, 0, buf, 0, shaID.length);
	System.arraycopy(document, 0, buf, shaID.length, document.length);
	try {
	    return opensslSign(buf, lastKey);
	} catch (IOException e) {
	    GeneralSecurityException gse =
		new GeneralSecurityException(e.getMessage());
	    gse.setStackTrace(e.getStackTrace());
	    throw gse;
	}
    }
}

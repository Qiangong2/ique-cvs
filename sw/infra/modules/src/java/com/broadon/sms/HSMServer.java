package com.broadon.sms;

import java.net.*;
import java.io.*;
import java.util.Properties;
import java.util.logging.*;
import edu.emory.mathcs.backport.java.util.concurrent.*;

/**
 * Server that provides a network interface to a security module.
 */
public class HSMServer implements Keywords
{
    static int hsmPort;
    static int shutdownPort;
    static String shutdownCmd;
    static String caConfig;
    static String logDir;

    static int logSize = 10000000;	// ~10 MB
    static int logCount = 7;		// 7 logs total
    static Level logLevel = Level.INFO;
    
    static int threadPoolSize = 5;
    static int threadKeepAliveTime = 600; // 600 seconds

    static void waitForShutdown(Server svr, ThreadPoolExecutor executor, Logger log) {
	try {
	    ServerSocket shutdown = new ServerSocket(shutdownPort);
	    byte[] buffer = new byte[20];
	    boolean done = false;
	    while (! done) {
		Socket socket = shutdown.accept();
		InputStream in = socket.getInputStream();
		in.read(buffer);
		String command = new String(buffer);
		log.info("Received shutdown command: " + command);
		if (command.toLowerCase().startsWith(shutdownCmd)) {
		    svr.shutdown();
		    cleanup(executor, log);
		    done = true;
		}
		socket.close();
	    }

	    shutdown.close();
	} catch (Exception e) {
	    // quit on any error
	    log.log(Level.SEVERE, "Error when trying to shutdown server", e);
	}
    }

    
    static void cleanup(ThreadPoolExecutor executor, Logger log) {
	try {
	    executor.shutdown();
	} catch (SecurityException e) {
	    log.log(Level.WARNING, "Clean up process interrupted", e);
	}
    }

    static void readProperties(String propFile)
	throws IOException
    {
	Properties p = new Properties();
	p.load(new FileInputStream(propFile));
	hsmPort = Integer.parseInt(p.getProperty(HSM_PORT));
	shutdownPort = Integer.parseInt(p.getProperty(SHUTDOWN_PORT));
	shutdownCmd = p.getProperty(SHUTDOWN_CMD);
	caConfig = p.getProperty(CA_CONFIG);
	logDir = p.getProperty(LOG_DIR);
	String s = p.getProperty(LOG_SIZE);
	if (s != null)
	    logSize = Integer.parseInt(s);
	s = p.getProperty(LOG_COUNT);
	if (s != null)
	    logCount = Integer.parseInt(s);
	s = p.getProperty(LOG_LEVEL);
	if (s != null)
	    logLevel = Level.parse(s);
	s = p.getProperty(THREAD_POOL_SIZE);
	if (s != null)
	    threadPoolSize = Integer.parseInt(s);
	s = p.getProperty(THREAD_KEEP_ALIVE_TIME);
	if (s != null)
	    threadKeepAliveTime = Integer.parseInt(s);
     }
	

    public static void main(String[] args) {
	if (args.length < 1) {
	    System.err.println("Usage: HSMServer config_file");
	    return;
	} 

	Logger log = null;

	try {
	    readProperties(args[0]);

	    log = Logger.getLogger("com.broadon.sms.HSMServer");
	    log.addHandler(new FileHandler(logDir + "/hsm.log.%g", logSize,
					   logCount, true));
	    log.setUseParentHandlers(false);
	    log.setLevel(logLevel);
	} catch (Exception e) {
	    e.printStackTrace();
	    return;
	}

	try {
	    Service svc =
		new Service(caConfig, log);
	    LinkedBlockingQueue queue = new LinkedBlockingQueue();
	    ThreadPoolExecutor executor = 
	        new ThreadPoolExecutor(threadPoolSize, threadPoolSize,
	                threadKeepAliveTime, TimeUnit.SECONDS, queue);
	    executor.allowCoreThreadTimeOut(true);
	    Server svr = new Server(hsmPort, svc, executor, log);
	    svr.setDaemon(true);
	    svr.start();

	    waitForShutdown(svr, executor, log);
	    log.info("HSMServer shut down.");
	} catch (Exception e) { 
	    log.log(Level.SEVERE, "HSM Server crashed", e);
	}
    }
}

package com.broadon.sms;

import java.net.*;
import java.io.IOException;
import java.util.logging.Logger;
import java.util.logging.Level;
import edu.emory.mathcs.backport.java.util.concurrent.*;


/**
 * Implement a TCP server.  Each new connection is handled by a new thread.
 */
public class Server extends Thread
{
    int port;
    Service service;
    boolean stop;
    Logger log;
    ThreadPoolExecutor executor;

    /**
     * Create a new <code>Server</code> instance.
     * @param port Port number to be listened to.
     * @param service The object the serves a new connection.
     */
    public Server(int port, Service service, ThreadPoolExecutor executor, Logger log) {
	super();
	this.port = port;
	this.service = service;
	this.executor = executor;
	this.log = log;
	stop = false;
    }

    public void shutdown() {
	stop = true;
    }


    /**
     * Listen to the specified port and create a new thread to serve
     * each request. 
     */
    public void run() {
	ServerSocket server = null;
	try {
	    server = new ServerSocket(port);
	    log.info("HSM Server ready");
	    while (! stop) {
		Socket socket = server.accept();
		if (stop)
		    break;
		Service s = service.newInstance(socket);
		executor.execute(s);
	    }
	    server.close();
	} catch (IOException e) {
	    log.log(Level.SEVERE, "Listener crashed", e);
	}
    }
}

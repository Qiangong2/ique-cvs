package com.broadon.sms;

import java.io.*;
import java.net.*;
import java.util.Properties;


public class Shutdown
{
    public static void main(String[] args) {
	if (args.length < 1) {
	    System.err.println("Usage: Shutdown config_file");
	    return;
	}

	try {
	    Properties p = new Properties();
	    p.load(new FileInputStream(args[0]));
	    int port = Integer.parseInt(p.getProperty("HSM_SHUTDOWN_PORT"));
	    String cmd = p.getProperty("HSM_SHUTDOWN_COMMAND");

	    Socket sock = new Socket("localhost", port);
	    OutputStream out = sock.getOutputStream();
	    out.write(cmd.getBytes());
	    out.flush();
	    sock.close();
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }
	
}

package com.broadon.sms;

public interface Keywords
{
    public static final String HSM_PORT = "HSM_SERVICE_PORT";

    public static final String SHUTDOWN_PORT = "HSM_SHUTDOWN_PORT";

    public static final String SHUTDOWN_CMD = "HSM_SHUTDOWN_COMMAND";

    public static final String CA_CONFIG = "HSM_CA_CONFIG";

    public static final String LOG_DIR = "HSM_LOG_DIR";

    public static final String LOG_SIZE = "LOG_SIZE";

    public static final String LOG_COUNT = "LOG_COUNT";

    public static final String LOG_LEVEL = "LOG_LEVEL";

    public static final String CERT_TYPE_PREFIX = "TYPE_";

    public static final String CERT_HOME = "CERT_HOME";

    public static final String CERT_TMPDIR = "CERT_TMPDIR";

    public static final String SIGNER_ID_KEY = "SIGNER_ID";
    
    public static final String USE_SSM_KEY = "USE_SOFTWARE_SECURITY_MODULE";

    public static final String KEY_FILE_LIST = "KEY_LIST";

    public static final String THREAD_POOL_SIZE = "THREAD_POOL_SIZE";
    
    public static final String THREAD_KEEP_ALIVE_TIME = "THREAD_KEEP_ALIVE_TIME";
    public static final String DBproperty = "DB.properties";
    public static final String DB_url = "DB_URL";
    public static final String DB_user = "DB_USER";
    public static final String DB_password = "DB_PASSWORD";

}

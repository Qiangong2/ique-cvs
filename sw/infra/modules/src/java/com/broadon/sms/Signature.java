package com.broadon.sms;

/**
 * Interface for signing arbitrary data.
 */
public interface Signature
{
    /**
     * Sign a given block of data.
     * @param signer ID of the identity used for signing.
     * @param document Block of data to be signed.
     * @param hashed Data is already hashed.
     * @return Signature of <code>document</code>
     */
    public byte[] sign(String signer, byte[] document, boolean hashed)
	throws java.security.GeneralSecurityException, com.ncipher.nfast.NFException;
}

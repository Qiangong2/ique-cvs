package com.broadon.sms;

import java.io.*;
import java.util.Vector;

/**
 * Wrap around the <code>Runtime.exec</code> methods, except the the
 * array of environment variables are <em>appended</em> to the
 * inherited environment instead of replacing it.
 */
public class UnixCommand
{
    Vector env;
    Runtime rt;
    static UnixCommand cmd = null;

    static {
	try {
	    cmd = new UnixCommand();
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

    
    UnixCommand() throws IOException {
	rt = Runtime.getRuntime();
	Process proc = rt.exec("env");
	env = new Vector();
	BufferedReader reader =
	    new BufferedReader(new InputStreamReader(proc.getInputStream()));
	String s;
	while ((s = reader.readLine()) != null)
		env.add(s);
    }


    /**
     * Obtain an <code>UnixCommand</code> instance.
     * @return An <code>UnixCommand</code> instance.
     */
    public static UnixCommand getUnixCommand() {
	return cmd;
    }


    /**
     * Execute the specified command.
     * @param command a specified system command.
     * @param envp array of strings, each element of which has
     * environment variable settings in format <em>name=value</em>.
     * This is appended to the inherited environment variables.
     * @exception IOException Any error in executing this command.
     */
    public Process exec(String command, String[] envp)
	throws IOException
    {
	return (envp == null) ?
	    rt.exec(command) :
	    rt.exec(command, setupEnv(envp));
    }


    /**
     * Execute the specified command.
     * @param cmdArray array containing the command to call and its arguments.
     * @param envp array of strings, each element of which has
     * environment variable settings in format <em>name=value</em>.
     * This is appended to the inherited environment variables.
     * @exception IOException Any error in executing this command.
     */
    public Process exec(String[] cmdArray, String[] envp)
	throws IOException
    {
	return (envp == null) ?
	    rt.exec(cmdArray) :
	    rt.exec(cmdArray, setupEnv(envp));
    }

    
    String[] setupEnv(String[] envp) {
	String[] newEnvp = new String[env.size() + envp.length];
	env.toArray(newEnvp);
	System.arraycopy(envp, 0, newEnvp, env.size(), envp.length);
	return newEnvp;
    }
}

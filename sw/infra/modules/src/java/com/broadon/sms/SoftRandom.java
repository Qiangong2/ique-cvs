package com.broadon.sms;

import java.security.SecureRandom;
import java.security.GeneralSecurityException;

/**
 * Implements a software-based random number generator
 */
public class SoftRandom implements Random
{
    SecureRandom rnd;

    /** Initialized the RNG. */
    protected SoftRandom() {
	rnd = new SecureRandom();
    }


    public byte[] getRandom(int size)
	throws GeneralSecurityException
    {
	byte[] result = new byte[size];
	rnd.nextBytes(result);
	return result;
    }
} 

package com.broadon.sms;

import java.util.List;
import java.io.*;
import java.security.*;
import java.security.cert.*;


/**
 * KeyStore operations
 */
public class KeyStoreWriter
{
    CertificateFactory cf;
    KeyFactory kf;
    Runtime rt;

    protected KeyStoreWriter()
	throws GeneralSecurityException
    {
	cf = CertificateFactory.getInstance("X.509");
	kf = KeyFactory.getInstance("RSA");
	rt = Runtime.getRuntime();
    }

    
    protected X509Certificate generateCert(byte[] rawCert)
	throws CertificateException
    {
	return (X509Certificate)
	    cf.generateCertificate(new ByteArrayInputStream(rawCert));
    }
    

    protected X509Certificate[] generateCerts(List certs, int first, int last)
	throws CertificateException
    {
	X509Certificate[] results = new X509Certificate[last - first];
	for (int i = first; i < last; ++i) {
	    results[i] = generateCert(((String) certs.get(i)).getBytes());
	}
	return results;
    }


    protected X509Certificate[] generateCerts(List certs)
	throws CertificateException
    {
	return generateCerts(certs, 0, certs.size());
    }


    // convert the RSA key to PKCS8 format
    protected PrivateKey convertPrivateKey(byte[] key, String pw)
	throws IOException, GeneralSecurityException
    {
	Process proc =
            rt.exec("openssl pkcs8 -topk8 -outform DER -nocrypt -passin pass:" + pw);
        OutputStream out = proc.getOutputStream();
        out.write(key);
        out.close();

	InputStream in = proc.getInputStream();
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        byte[] b = new byte[1024];
        int n;
        while ((n = in.read(b)) > 0) {
            buffer.write(b, 0, n);
        }

        java.security.spec.KeySpec kspec =
            new java.security.spec.PKCS8EncodedKeySpec(buffer.toByteArray());
        return kf.generatePrivate(kspec);
    }


    // convert an array of certs into a trusted store
    protected byte[] getTrustedStore(X509Certificate[] certs, char[] password)
	throws IOException, GeneralSecurityException
    {
	KeyStore ks = KeyStore.getInstance("JKS");
	ks.load(null, null);
	for (int i = 0; i < certs.length; ++i) {
	    ks.setCertificateEntry(String.valueOf(certs[i].hashCode()),
				   certs[i]);
	}
	ByteArrayOutputStream out = new ByteArrayOutputStream();
	ks.store(out, password);
	return out.toByteArray();
    }

    
    protected byte[] getKeyStore(X509Certificate[] certs, String alias,
				 PrivateKey privKey, char[] password)
	throws IOException, GeneralSecurityException
    {
	KeyStore ks = KeyStore.getInstance("JKS");
	ks.load(null, null);
	ks.setKeyEntry(alias, privKey, password, certs);
	ByteArrayOutputStream out = new ByteArrayOutputStream();
	ks.store(out, password);
	return out.toByteArray();
    }
}

package com.broadon.bean;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

import com.broadon.util.HexString;

/**
 * The <c>TextFormatWriter</c> class formats the information of the given
 * JavaBeans object.
 *
 * @version	$Revision: 1.5 $
 */
public class TextFormatWriter
    extends BeanOutputProcessor
{
    protected static final String	INDENTATION	= "  ";
    protected static final char		SEPARATOR	= '\n';

    private Bean			bean;

    /**
     * Constructs a TextFormatWriter instance.
     *
     * @param	bean			the JavaBeans object to be formatted
     */
    public TextFormatWriter(Bean bean)
    {
	super(bean);
	setSkipNull(true);
    }

    /**
     * Constructs a TextFormatWriter instance.
     *
     * @param	bean			the JavaBeans object to be formatted
     * @param	nameMap			the name mapping table
     * @param	skipNull		true  => skip null values;
     *					false => process null values
     */
    protected TextFormatWriter(Bean bean, Map nameMap, boolean skipNull)
    {
	super(bean);
	setNameMap(nameMap);
	setSkipNull(skipNull);
    }

    /**
     * Formats the JavaBeans instance given to this TextFormatWriter.
     *
     * @param	out			the output destination
     * @param	indentLevel		the indentation level for displaying
     *
     * @throws	IOException
     */
    public void format(Writer out, int indentLevel)
	throws IOException
    {
	super.process(new TextParam(out, indentLevel));
    }

    /**
     * Process the attribute, identified by the given name, with the given
     * data. This method can be overridden by subclasses for showing a
     * different format.
     *
     * @param	name			the name of the attribute
     * @param	data			the value of the attribute
     * @param	dataType		the type of the attribute
     * @param	parameter		the parameter information
     *
     * @throws	IOException
     */
    protected void processAttribute(String name,
				    Object data,
				    Class dataType,
				    Object parameter)
	throws IOException
    {
	TextParam	textParam = (TextParam)parameter;
	Writer		out = textParam.out;
	int		indentLevel = textParam.indentLevel;

	/*
	 * Indent.
	 */
	for (int i = 0; i < indentLevel; i++)
	{
	    out.write(INDENTATION);
	}
	/*
	 * Show name=value.
	 */
	out.write(name);
	out.write('=');
	if (data != null)
	{
	    try
	    {
		Bean		bean = (Bean)data;
		/*
		 * Recursively format this JavaBeans attribute.
		 */
		TextFormatWriter	formatter;

		out.write(SEPARATOR);
		formatter = new TextFormatWriter(bean,
						 getNameMap(),
						 isSkipNull());
		formatter.format(out, indentLevel + 1);
	    }
	    catch (ClassCastException cce)
	    {
		if (data instanceof byte[])
		{
		    /*
		     * Byte array.
		     */
		    HexString.write(out, (byte[])data);
		}
		else
		{
		    /*
		     * This is the common case, where most data are of
		     * primitive types.
		     */
		    out.write(data.toString());
		}
	    }
	}
	/*
	 * Attribute separator.
	 */
	out.write(SEPARATOR);
	out.flush();
    }

    /**
     * The <c>TextParam</c> class stores the information to be passed around
     */
    protected class TextParam
    {
	public Writer		out;
	public int		indentLevel;

	/**
	 * Constructs a TextParam instance.
	 *
	 * @param	out		the output destination
	 * @param	indentLevel	the indentation level
	 */
	public TextParam(Writer out, int indentLevel)
	{
	    this.out = out;
	    this.indentLevel = indentLevel;
	}
    }
}

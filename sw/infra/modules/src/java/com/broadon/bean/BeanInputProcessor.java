package com.broadon.bean;

import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

/**
 * The <c>BeanInputProcessor</c> abstract class provides a skelton for
 * setting the information into the JavaBeans objects.
 *
 * @version	$Revision: 1.3 $
 */
public abstract class BeanInputProcessor
{
    private Bean		bean;
    private Map			nameMap;

    /**
     * Constructs a BeanInputProcessor instance.
     *
     * @param	bean			the JavaBeans object to be set
     */
    public BeanInputProcessor(Bean bean)
    {
	this(bean, null);
    }

    /**
     * Constructs a BeanInputProcessor instance.
     *
     * @param	bean			the JavaBeans object to be set
     * @param	nameMap			the name mapping table
     */
    public BeanInputProcessor(Bean bean, Map nameMap)
    {
	this.bean = bean;
	this.nameMap = nameMap;
    }

    /**
     * Returns the JavaBeans object.
     *
     * @return	The JavaBeans object.
     */
    public final Bean getBean()
    {
	return bean;
    }

    /**
     * Sets the JavaBeans object.
     *
     * @param	bean			the JavaBeans object
     */
    public final void setBean(Bean bean)
    {
	this.bean = bean;
    }

    /**
     * Returns the map for name changes.
     *
     * @return	The map for name changes.
     */
    public final Map getNameMap()
    {
	return nameMap;
    }

    /**
     * Sets the map for name changes.
     *
     * @param	nameMap			the map for name changes
     */
    public final void setNameMap(Map nameMap)
    {
	this.nameMap = nameMap;
    }

    /**
     * Reads the JavaBeans instance given to this BeanInputProcessor.
     *
     * @return	the string representation of the JavaBeans instance, in
     *		name=value format.
     *
     * @throws	IOException
     */
    public void process()
	throws IOException
    {
	/*
	 * Get the property descriptors of the JavaBeans, in order to
	 * invoke all the available getters.
	 */
	PropertyDescriptor[]	propertyDescriptors;

	propertyDescriptors = bean.getPropertyDescriptors();
	if (propertyDescriptors == null)
	{
	    return;
	}

	int	size = propertyDescriptors.length;

	/*
	 * Pre-processing.
	 */
	preProcess();
	/*
	 * One attribute at a time.
	 */
	for (int n = 0; n < size; n++)
	{
	    PropertyDescriptor	propertyDescriptor = propertyDescriptors[n];
	    Method		setMethod = propertyDescriptor.getWriteMethod();

	    if (setMethod != null)
	    {
		/*
		 * Constructs the correct data object for passing to the
		 * setter method.
		 */
		String		name = propertyDescriptor.getDisplayName();

		/*
		 * Map the name, if any.
		 */
		if (nameMap != null)
		{
		    String	mappedName = (String)nameMap.get(name);

		    if (mappedName != null)
		    {
			name = mappedName;
		    }
		}
		if ("class".equals(name))
		{
		    /*
		     * Skip class (from getClass()).
		     */
		    continue;
		}

		Class		paramType = setMethod.getParameterTypes()[0];
		Object		data = processAttribute(name, paramType);

		if (data == null)
		{
		    /*
		     * Always skip null data.
		     */
		    continue;
		}
		try
		{
		    /*
		     * Call the setter method.
		     */
		    setMethod.invoke(bean, new Object[] { data });
		}
		catch (IllegalAccessException iae)
		{
		    iae.printStackTrace();
		}
		catch (InvocationTargetException ite)
		{
		    ite.printStackTrace();
		}
	    }
	}
	/*
	 * Post-processing.
	 */
	postProcess();
    }

    /**
     * Does initialization work before processing the attributes. The default
     * is to do nothing.
     *
     * @throws	IOException
     */
    protected void preProcess()
	throws IOException
    {
    }

    /**
     * Does clean up work after processing the attributes. The default
     * is to do nothing.
     *
     * @throws	IOException
     */
    protected void postProcess()
	throws IOException
    {
    }

    /**
     * Process the attribute, identified by the given name, with the given
     * data. This class must be implemented by the subclasses.
     *
     * @param	name			the name of the attribute
     * @param	dataType		the type of the attribute
     *
     * @throws	IOException
     */
    protected abstract Object processAttribute(String name, Class dataType)
	throws IOException;
}

package com.broadon.bean;

import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.xml.sax.SAXException;

import com.broadon.xml.XMLOutputProcessor;

/**
 * The <c>XMLConverter</c> class provides the capability to convert a
 * XML document into an instance of a given JavaBeans.
 *
 * @version	$Revision: 1.4 $
 */
public class XMLConverter
    extends XMLOutputProcessor
{
    private static Map		propertyDescriptorMap;

    static
    {
	/*
	 * The cache for property descriptors where the key is the class
	 * of the corresponding bean.
	 */
	propertyDescriptorMap = new HashMap();
    }

    /**
     * Returns the Map that contains the property descriptors of the
     * given JavaBeans.
     *
     * @param	bean			the JavaBeans instance
     *
     * @return	The Map that contains the property descriptors of the
     *		given bean.
     */
    private static Map getPropertyDescriptors(Bean bean)
    {
	Class		beanClass = bean.getClass();
	Map		map = (Map)propertyDescriptorMap.get(beanClass);

	if (map == null)
	{
	    PropertyDescriptor[]	propertyDescriptors;

	    propertyDescriptors = bean.getPropertyDescriptors();
	    if (propertyDescriptors == null)
	    {
		return null;
	    }

	    int	size = propertyDescriptors.length;

	    map = new HashMap(size * 3 / 2);
	    for (int n = 0; n < size; n++)
	    {
		PropertyDescriptor	propertyDescriptor;

		propertyDescriptor = propertyDescriptors[n];
		map.put(propertyDescriptor.getDisplayName(),
			propertyDescriptor);
	    }
	    propertyDescriptorMap.put(beanClass, map);
	}
	return map;
    }

    /**
     * Constructs the XMLConverter instance.
     *
     * @throws	IOException
     */
    public XMLConverter()
	throws IOException
    {
	super();
    }

    /**
     * Converts the given XML document to an instance of the JavaBeans
     * class, given as beanClass.
     *
     * @param	xml			the XML docuemnt to convert from
     * @param	beanClass		the JavaBeans class to convert to
     * @param	nameMap			the map for name changes
     * @param	rootTag			the starting point of conversion
     *
     * @return	The instance of beanClass containing information providing
     *		by xmlContent.
     *
     * @throws	BeanException
     */
    public Bean toBean(String xml,
		       Class beanClass,
		       Map nameMap,
		       String rootTag)
	throws BeanException
    {
	if (xml == null)
	{
	    throw new IllegalArgumentException("xml");
	}
	return toBean(new StringReader(xml), beanClass, nameMap, rootTag);
    }

    /**
     * Converts the given XML document to an instance of the JavaBeans
     * class, given as beanClass.
     *
     * @param	reader			the reader that contains the XML
     *					docuemnt to convert from
     * @param	beanClass		the JavaBeans class to convert to
     * @param	nameMap			the map for name changes
     * @param	rootTag			the starting point of conversion
     *
     * @return	The instance of beanClass containing information providing
     *		by xmlContent.
     *
     * @throws	BeanException
     */
    public Bean toBean(Reader reader,
		       Class beanClass,
		       Map nameMap,
		       String rootTag)
	throws BeanException
    {
	if (reader == null)
	{
	    throw new IllegalArgumentException("reader");
	}
	if (beanClass == null)
	{
	    throw new IllegalArgumentException("beanClass");
	}

	/*
	 * Construct an empty JavaBeans instance.
	 */
	Bean		bean;

	try
	{
	    bean = (Bean)beanClass.newInstance();
	}
	catch (InstantiationException ie)
	{
	    ie.printStackTrace();
	    throw new BeanException("Probably no default constructor for " +
				    beanClass, ie);
	}
	catch (IllegalAccessException iae)
	{
	    iae.printStackTrace();
	    throw new BeanException("Illegal access to " + beanClass, iae);
	}
	/*
	 * Find the Map that contains the property descriptors of
	 * this bean class.
	 */
	Map	map = getPropertyDescriptors(bean);

	if (map == null)
	{
	    /*
	     * No property descriptor.
	     */
	    return null;
	}

	if (rootTag == null)
	{
	    /*
	     * Defaults to use the class name as the root tag.
	     */
	    rootTag = beanClass.getName();
	}
	try
	{
	    return (Bean)process(reader,
				 bean,
				 rootTag,
				 new Param(map, nameMap));
	}
	catch (IOException ie)
	{
	    ie.printStackTrace();
	    throw new BeanException("Cannot process " + beanClass, ie);
	}
    }

    /**
     * Processes the element (name, value), and adds to the result
     * instance.
     *
     * @param	result			the instance containing the result
     * @param	name			the name of the attribute
     * @param	value			the value of the attribute
     * @param	parameter		the additional parameters
     *
     * @throws	SAXExcpetion
     */
    protected void processElement(Object result,
				  String name,
				  String value,
				  Object parameter)
	throws SAXException
    {
	Param			param = (Param)parameter;
	PropertyDescriptor	propertyDescriptor = param.propertyDescriptor;

	/*
	 * Process this element, if marked.
	 */
	if (propertyDescriptor != null)
	{
	    /*
	     * Get the setter method.
	     */
	    Method	setMethod = propertyDescriptor.getWriteMethod();

	    if (setMethod != null)
	    {
		/*
		 * Constructs the correct data object for passing to the
		 * setter method.
		 */
		Class	paramType = setMethod.getParameterTypes()[0];
		Object	data = Bean.getData(paramType, value);

		try
		{
		    /*
		     * Call the setter method.
		     */
		    setMethod.invoke(result, new Object[] { data });
		}
		catch (IllegalAccessException iae)
		{
		    iae.printStackTrace();
		    throw new SAXException("Invoke", iae);
		}
		catch (InvocationTargetException ite)
		{
		    ite.printStackTrace();
		    throw new SAXException("Invoke", ite);
		}
	    }
	    /*
	     * Clean up.
	     */
	    param.propertyDescriptor = null;
	}
    }

    /**
     * Initializes before processing an element. The default is to do
     * nothing.
     *
     * @param	result			the instance containing the result
     * @param	name			the name of the attribute
     * @param	parameter		the additional parameters
     */
    protected void initElement(Object result, String name, Object parameter)
    {
	Param			param = (Param)parameter;
	Map			map = (Map)param.map;
	Map			nameMap = (Map)param.nameMap;
	PropertyDescriptor	propertyDescriptor;

	/*
	 * This element is within the hierarchy of the element
	 * identified by rootTag, process it.
	 */
	propertyDescriptor = (PropertyDescriptor)map.get(name);
	if (propertyDescriptor == null && nameMap != null)
	{
	    /*
	     * Map the name before looking up the property descriptor.
	     */
	    name = (String)nameMap.get(name);
	    propertyDescriptor = (PropertyDescriptor)map.get(name);
	}
	param.propertyDescriptor = propertyDescriptor;
    }

    /**
     * The <c>Param</c> class stores the information to be passed around
     */
    private class Param
    {
	private Map			map;
	private Map			nameMap;
	private PropertyDescriptor	propertyDescriptor;

	/**
	 * Constructs a Param instance.
	 *
	 * @param	map		the map for property descriptors
	 * @param	nameMap		the map for name changes
	 */
	private Param(Map map, Map nameMap)
	{
	    this.map = map;
	    this.nameMap = nameMap;
	    this.propertyDescriptor = null;
	}
    }
}

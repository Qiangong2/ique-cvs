package com.broadon.bean;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * The <c>Bean</c> abstract class provides a top level class for JavaBeans.
 * As a side effect, its JavaBeans subclasses will be able to obtain the
 * <c>BeanInfo</c> information and the <c>PropertyDescriptor</c> information
 * directly, taking advantage of the provided cache.
 *
 * @version	$Revision: 1.5 $
 */
public abstract class Bean
{
    protected static final int		BOOL_TYPE = 1;
    protected static final int		BYTE_TYPE = 2;
    protected static final int		CHAR_TYPE = 3;
    protected static final int		DOUBLE_TYPE = 4;
    protected static final int		FLOAT_TYPE = 5;
    protected static final int		INTEGER_TYPE = 6;
    protected static final int		LONG_TYPE = 7;
    protected static final int		SHORT_TYPE = 8;
    protected static final int		DATE_TYPE = 9;
    protected static final int		STRING_TYPE = 10;
    protected static final int		BYTE_ARRAY_TYPE = 11;
    protected static final int		OTHER_TYPE = 12;

    protected static Map		typeMap;

    private static Map			beanInfoMap;

    static
    {
	/*
	 * For faster type lookup.
	 */
	typeMap = new HashMap(37);
	/*
	 * Put the class objects into the type map.
	 */
	Integer		type;

	type = new Integer(BOOL_TYPE);
	typeMap.put(Boolean.TYPE, type);
	typeMap.put(new Boolean(true).getClass(), type);
	type = new Integer(BYTE_TYPE);
	typeMap.put(Byte.TYPE, type);
	typeMap.put(new Byte((byte)0).getClass(), type);
	type = new Integer(CHAR_TYPE);
	typeMap.put(Character.TYPE, type);
	typeMap.put(new Character(' ').getClass(), type);
	type = new Integer(DOUBLE_TYPE);
	typeMap.put(Double.TYPE, type);
	typeMap.put(new Double(0.0).getClass(), type);
	type = new Integer(FLOAT_TYPE);
	typeMap.put(Float.TYPE, type);
	typeMap.put(new Float(0.0).getClass(), type);
	type = new Integer(INTEGER_TYPE);
	typeMap.put(Integer.TYPE, type);
	typeMap.put(new Integer(0).getClass(), type);
	type = new Integer(LONG_TYPE);
	typeMap.put(Long.TYPE, type);
	typeMap.put(new Long(0).getClass(), type);
	type = new Integer(SHORT_TYPE);
	typeMap.put(Short.TYPE, type);
	typeMap.put(new Short((short)0).getClass(), type);
	type = new Integer(DATE_TYPE);
	typeMap.put(new Date().getClass(), type);
	type = new Integer(STRING_TYPE);
	typeMap.put(new String().getClass(), type);
	type = new Integer(BYTE_ARRAY_TYPE);
	typeMap.put(new byte[1].getClass(), type);
	/*
	 * Caching the BeanInfo instance, one for each JavaBeans class,
	 * does improve performance. However, the amount of improvement
	 * differ from JDK to JDK. In IBM's JDK 1.3, it only runs about
	 * 20% faster; but on Sun's JDK 1.4.1, it has a 60 times gain.
	 */
	beanInfoMap = new HashMap();
    }

    /**
     * Constructs the correct data object. Handle all the primitive types
     * and their wrapper classes, as well as the Date type.
     *
     * @param	type			the type of the data object
     * @param	value			the value of the data value, as String
     *
     * @return	The data object of dataType with the given value.
     */
    public static Object getData(Class type, String value)
    {
	Integer		dataType = (Integer)typeMap.get(type);

	switch ((dataType == null) ? OTHER_TYPE : dataType.intValue())
	{
	case BOOL_TYPE:
	    return new Boolean(value);

	case BYTE_TYPE:
	    return new Byte(value);

	case CHAR_TYPE:
	    return new Character(value.toCharArray()[0]);

	case DOUBLE_TYPE:
	    return new Double(value);

	case FLOAT_TYPE:
	    return new Float(value);

	case INTEGER_TYPE:
	    return new Integer(value);

	case LONG_TYPE:
	    return new Long(value);

	case SHORT_TYPE:
	    return new Short(value);

	case DATE_TYPE:
	    if (value == null || value.equals(""))
	    {
		return null;
	    }
	    /*
	     * The given data is in seconds, convert to milliseconds.
	     */
	    return new Date(Long.parseLong(value)*1000);

	case BYTE_ARRAY_TYPE:
	    return value.getBytes();

	case STRING_TYPE:
	default:
	    return value;
	}
    }

    /**
     * Returns the BeanInfo instance of this JavaBeans object.
     *
     * @return	The BeanInfo instance of this JavaBeans object.
     */
    protected BeanInfo getBeanInfo()
    {
	/*
	 * See if the BeanInfo instance has been cached already.
	 */
	Class		beanClass = getClass();
	BeanInfo	beanInfo = (BeanInfo)beanInfoMap.get(beanClass);

	if (beanInfo == null)
	{
	    /*
	     * To prevent concurrent update of the same BeanInfo, we
	     * serialize the access.
	     */
	    synchronized (beanInfoMap)
	    {
		/*
		 * Someone may have cached this BeanInfo while we
		 * are waiting.
		 */
		beanInfo = (BeanInfo)beanInfoMap.get(beanClass);
		if (beanInfo == null)
		{
		    /*
		     * If not, then get it from the Introspector.
		     */
		    try
		    {
			beanInfo = Introspector.getBeanInfo(beanClass);
		    }
		    catch (IntrospectionException ie)
		    {
			ie.printStackTrace();
			return null;
		    }
		    /*
		     * Cache it.
		     */
		    beanInfoMap.put(beanClass, beanInfo);
		}
	    }
	}
	return beanInfo;
    }

    /**
     * Returns the PropertyDescriptor instances of this JavaBeans object.
     *
     * @return	The array of PropertyDescriptor instances of this JavaBeans
     *		object.
     */
    protected PropertyDescriptor[] getPropertyDescriptors()
    {
	BeanInfo	beanInfo = getBeanInfo();

	return (beanInfo == null) ? null : beanInfo.getPropertyDescriptors();
    }

    /**
     * Returns the string representation of this Bean. This is the default
     * implementation. A Bean subclass can override it.
     *
     * @return	The string representation of this Bean, in XML format.
     */
    public String toString()
    {
	return new XMLFormatter(this).toString();
    }
}

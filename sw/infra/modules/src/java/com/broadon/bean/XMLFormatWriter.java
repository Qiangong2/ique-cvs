package com.broadon.bean;

import java.io.IOException;
import java.io.Writer;
import java.util.Date;
import java.util.Map;

import com.broadon.util.HexString;
import com.broadon.util.Xml;

/**
 * The <c>XMLFormatWriter</c> class formats the information of the given
 * JavaBeans object into XML format, without <?xml> and <!DOCTYPE>.
 *
 * @version	$Revision: 1.9 $
 */
public class XMLFormatWriter
    extends TextFormatWriter
{
    private static final Xml xmlEncoder = new Xml();

    /**
     * Constructs a XMLFormatWriter instance.
     *
     * @param	bean			the JavaBeans object to be formatted
     */
    public XMLFormatWriter(Bean bean)
    {
	super(bean);
    }

    /**
     * Constructs a XMLFormatWriter instance.
     *
     * @param	bean			the JavaBeans object to be formatted
     * @param	nameMap			the name mapping table
     * @param	skipNull		true  => skip null value;
     *					false => otherwise
     */
    protected XMLFormatWriter(Bean bean, Map nameMap, boolean skipNull)
    {
	super(bean, nameMap, skipNull);
    }

    /**
     * Formats the JavaBeans instance given to this XMLFormatWriter.
     *
     * @param	out			the output destination
     * @param	indentLevel		the indentation level for displaying
     *
     * @throws	IOException
     */
    public void format(Writer out, int indentLevel)
	throws IOException
    {
	/*
	 * The default root tag is the leaf class name of the JavaBeans.
	 */
	String	rootTag;
	String	name = getBean().getClass().getName();
	int	dot = name.lastIndexOf('.');

	if (dot < 0)
	{
	    rootTag = name;
	}
	else
	{
	    rootTag = name.substring(dot + 1);
	}
	super.process(new XMLParam(out, indentLevel, rootTag));
    }

    /**
     * Formats the JavaBeans instance given to this XMLFormatWriter.
     *
     * @param	out			the output destination
     * @param	indentLevel		the indentation level for displaying
     * @param	rootTag			the root tag
     *
     * @throws	IOException
     */
    public void format(Writer out, int indentLevel, String rootTag)
	throws IOException
    {
	super.process(new XMLParam(out, indentLevel, rootTag));
    }

    /**
     * Shows the open root tag at the beginning of the document.
     *
     * @param	parameter		the parameter information
     *
     * @throws	IOException
     */
    protected final void preProcess(Object parameter)
	throws IOException
    {
	XMLParam	xmlParam = (XMLParam)parameter;
	String		rootTag = xmlParam.rootTag;

	if (rootTag != null)
	{
	    Writer	out = xmlParam.out;
	    int		indentLevel = xmlParam.indentLevel;

	    for (int n = 0; n < indentLevel; n++)
	    {
		out.write(INDENTATION);
	    }
	    out.write('<');
	    out.write(rootTag);
	    out.write('>');
	    out.write(SEPARATOR);
	}
    }

    /**
     * Shows the close root tag at the ending of the document.
     *
     * @param	parameter		the parameter information
     *
     * @throws	IOException
     */
    protected final void postProcess(Object parameter)
	throws IOException
    {
	XMLParam	xmlParam = (XMLParam)parameter;
	String		rootTag = xmlParam.rootTag;

	if (rootTag != null)
	{
	    Writer	out = xmlParam.out;
	    int		indentLevel = xmlParam.indentLevel;

	    for (int n = 0; n < indentLevel; n++)
	    {
		out.write(INDENTATION);
	    }
	    out.write('<');
	    out.write('/');
	    out.write(rootTag);
	    out.write('>');
	    out.write(SEPARATOR);
	    out.flush();
	}
    }

    /**
     * Process the attribute, identified by the given name, with the given
     * data, into XML style.
     *
     * @param	name			the name of the attribute
     * @param	data			the value of the attribute
     * @param	dataType		the type of the attribute
     *
     * @param	parameter		the parameter information
     *
     * @throws	IOException
     */
    protected final void processAttribute(String name,
					  Object data,
					  Class dataType,
					  Object parameter)
	throws IOException
    {
	/*
	 * Increase the indentation level here because of the root tag
	 * showing in preToString() and postToString().
	 */
	XMLParam	xmlParam = (XMLParam)parameter;
	Writer		out = xmlParam.out;
	int		indentLevel = xmlParam.indentLevel + 1;
	String		rootTag = xmlParam.rootTag;

	if (data == null)
	{
		/*
		 * No data, just show the tags.
		 */
		for (int n = 0; n < indentLevel; n++)
		{
		    out.write(INDENTATION);
		}
		/*
		 * Show open name tag.
		 */
		out.write('<');
		out.write(name);
		out.write('>');
		/*
		 * Show close name tag.
		 */
		out.write('<');
		out.write('/');
		out.write(name);
		out.write('>');
		/*
		 * Attribute separator.
		 */
		out.write(SEPARATOR);
	}
	else
	{
	    try
	    {
		/*
		 * See if this attribute is of Bean type.
		 */
		Bean		bean = (Bean)data;
		/*
		 * Recursively format this JavaBeans attribute.
		 */
		XMLFormatWriter	formatter;

		formatter = new XMLFormatWriter(bean,
						getNameMap(),
						isSkipNull());
		formatter.format(out, indentLevel, name);
	    }
	    catch (ClassCastException cce)
	    {
		/*
		 * This is the common case, where most data are of primitive
		 * types.
		 */
		for (int n = 0; n < indentLevel; n++)
		{
		    out.write(INDENTATION);
		}
		/*
		 * Show open name tag.
		 */
		out.write('<');
		out.write(name);
		out.write('>');
		/*
		 * Show value.
		 */
		if (data instanceof Date)
		{
		    /*
		     * Date type. Display accuracy only up to the second.
		     */
		    out.write(String.valueOf(((Date)data).getTime()/1000));
		}
		else if (data instanceof byte[])
		{
		    /*
		     * Byte array.
		     */
		    HexString.write(out, (byte[])data);
		}
		else
		{
		    /*
		     * Everything else.
		     */
		    out.write(xmlEncoder.encode(data.toString()));
		}
		/*
		 * Show close name tag.
		 */
		out.write('<');
		out.write('/');
		out.write(name);
		out.write('>');
		/*
		 * Attribute separator.
		 */
		out.write(SEPARATOR);
	    }
	}
    }

    /**
     * The <c>XMLParam</c> class stores the information to be passed around
     */
    protected class XMLParam
	extends TextFormatWriter.TextParam
    {
	public String		rootTag;

	/**
	 * Constructs a XMLParam instance.
	 *
	 * @param	out		the output destination
	 * @param	indentLevel	the indentation level
	 * @param	rootTag		the root tag
	 */
	public XMLParam(Writer out, int indentLevel, String rootTag)
	{
	    super(out, indentLevel);
	    this.rootTag = rootTag;
	}
    }
}

package com.broadon.bean;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;

/**
 * The <c>TextFormatter</c> class formats the information of the given
 * JavaBeans object. This utility class provides the common toString()
 * method based on the getter property of JavaBeans.
 *<p>
 * It is implemented as a wrapper to the TextFormatWriter.
 *
 * @version	$Revision: 1.2 $
 */
public class TextFormatter
{
    protected TextFormatWriter		formatWriter;

    /**
     * Constructs a TextFormatter instance.
     *
     * @param	bean			the JavaBeans object to be formatted
     */
    public TextFormatter(Bean bean)
    {
	this.formatWriter = newFormatWriter(bean);
    }

    /**
     * Sets the map for name changes.
     *
     * @param	nameMap			the map for name changes
     */
    public final void setNameMap(Map nameMap)
    {
	formatWriter.setNameMap(nameMap);
    }

    /**
     * Sets the value to decide whether to skip processing when the
     * data value is null or not.
     *
     * @param	skipNull		true  => skip null values
     *					false => process null values
     */
    public final void setSkipNull(boolean skipNull)
    {
	formatWriter.setSkipNull(skipNull);
    }

    /**
     * Formats the JavaBeans instance given to this TextFormatter.
     *
     * @param	indentLevel		the indentation level for displaying
     *
     * @return	The formatted JavaBeans instance.
     *
     * @throws	IOException
     */
    public String format(int indentLevel)
	throws IOException
    {
	Writer	out = new StringWriter();

	try
	{
	    formatWriter.format(out, indentLevel);
	    out.flush();
	    return out.toString();
	}
	finally
	{
	    out.close();
	}
    }

    /**
     * Returns the string representation of this TextFormatter.
     *
     * @return	The string representation.
     */
    public String toString()
    {
	try
	{
	    return format(1);
	}
	catch (IOException ie)
	{
	    ie.printStackTrace();
	    return null;
	}
    }

    /**
     * Creates a new TextFormatWriter to be used by this TextFormatter.
     *
     * @param	bean			the JavaBeans object to be formatted
     */
    protected TextFormatWriter newFormatWriter(Bean bean)
    {
	return new TextFormatWriter(bean);
    }
}

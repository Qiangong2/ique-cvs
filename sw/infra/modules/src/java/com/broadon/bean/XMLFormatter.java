package com.broadon.bean;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

/**
 * The <c>XMLFormatter</c> class formats the information of the given
 * JavaBeans object into XML format, without <?xml> and <!DOCTYPE>.
 *
 * @version	$Revision: 1.1 $
 */
public class XMLFormatter
    extends TextFormatter
{
    /**
     * Constructs a XMLFormatter instance.
     *
     * @param	bean			the JavaBeans object to be formatted
     */
    public XMLFormatter(Bean bean)
    {
	super(bean);
    }

    /**
     * Formats the JavaBeans instance given to this XMLFormatter.
     *
     * @param	indentLevel		the indentation level for displaying
     * @param	rootTag			the rootTag
     *
     * @return	The formatted JavaBeans instance.
     *
     * @throws	IOException
     */
    public String format(int indentLevel, String rootTag)
	throws IOException
    {
	Writer	out = new StringWriter();

	try
	{
	    ((XMLFormatWriter)formatWriter).format(out, indentLevel, rootTag);
	    out.flush();
	    return out.toString();
	}
	finally
	{
	    out.close();
	}
    }

    /**
     * Creates a new XMLFormatWriter to be used by this TextFormatter.
     *
     * @param	bean			the JavaBeans object to be formatted
     */
    protected TextFormatWriter newFormatWriter(Bean bean)
    {
	return new XMLFormatWriter(bean);
    }
}

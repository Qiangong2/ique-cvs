package com.broadon.bean;

import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

/**
 * The <c>BeanOutputProcessor</c> abstract class provides a skelton for
 * getting the information out from the JavaBeans objects.
 *
 * @version	$Revision: 1.4 $
 */
public abstract class BeanOutputProcessor
{
    private Bean		bean;
    private Map			nameMap;
    private boolean		skipNull;

    /**
     * Constructs a BeanOutputProcessor instance.
     *
     * @param	bean			the JavaBeans object to be read
     */
    public BeanOutputProcessor(Bean bean)
    {
	this.bean = bean;
    }

    /**
     * Returns the JavaBeans object.
     *
     * @return	The JavaBeans object.
     */
    public final Bean getBean()
    {
	return bean;
    }

    /**
     * Sets the JavaBeans object.
     *
     * @param	bean			the JavaBeans object
     */
    public final void setBean(Bean bean)
    {
	this.bean = bean;
    }

    /**
     * Returns the map for name changes.
     *
     * @return	The map for name changes.
     */
    public final Map getNameMap()
    {
	return nameMap;
    }

    /**
     * Sets the map for name changes.
     *
     * @param	nameMap			the map for name changes
     */
    public final void setNameMap(Map nameMap)
    {
	this.nameMap = nameMap;
    }

    /**
     * Returns whether to skip processing when the data value is null or not.
     *
     * @return	true => skip processing null values; false => otherwise.
     */
    public final boolean isSkipNull()
    {
	return skipNull;
    }

    /**
     * Sets the value to decide whether to skip processing when the
     * data value is null or not.
     *
     * @param	skipNull		true  => skip null values
     *					false => process null values
     */
    public final void setSkipNull(boolean skipNull)
    {
	this.skipNull = skipNull;
    }

    /**
     * Prcesses the JavaBeans instance given to this BeanOutputProcessor.
     *
     * @throws	IOException
     */
    public void process()
	throws IOException
    {
	/*
	 * No parameter to pass around.
	 */
	process(null);
    }

    /**
     * Processes the JavaBeans instance given to this BeanOutputProcessor.
     *
     * @param	parameter		the parameter information, defined by
     *					subclasses
     *
     * @throws	IOException
     */
    public void process(Object parameter)
	throws IOException
    {
	/*
	 * Get the property descriptors of the JavaBeans, in order to
	 * invoke all the available getters.
	 */
	PropertyDescriptor[]	propertyDescriptors;

	propertyDescriptors = bean.getPropertyDescriptors();
	if (propertyDescriptors == null)
	{
	    return;
	}

	int	size = propertyDescriptors.length;

	/*
	 * Pre-processing.
	 */
	preProcess(parameter);
	/*
	 * One attribute at a time.
	 */
	for (int n = 0; n < size; n++)
	{
	    PropertyDescriptor	propertyDescriptor = propertyDescriptors[n];
	    Method		getMethod = propertyDescriptor.getReadMethod();

	    if (getMethod != null)
	    {
		String		name = propertyDescriptor.getName();
		Object		data;
		Class		dataType;

		try
		{
		    /*
		     * Invoke the getter of the property descriptor.
		     */
		    data = getMethod.invoke(bean, null);
		    dataType = getMethod.getReturnType();
		}
		catch (IllegalAccessException iae)
		{
		    iae.printStackTrace();
		    data = null;
		    dataType = null;
		}
		catch (InvocationTargetException ite)
		{
		    ite.printStackTrace();
		    data = null;
		    dataType = null;
		}
		if (skipNull && data == null)
		{
		    /*
		     * Skip null data.
		     */
		    continue;
		}
		/*
		 * Map the name, if any.
		 */
		if (nameMap != null)
		{
		    String	mappedName = (String)nameMap.get(name);

		    if (mappedName != null)
		    {
			name = mappedName;
		    }
		}
		if ("class".equals(name))
		{
		    /*
		     * Skip class (from getClass()).
		     */
		    continue;
		}
		/*
		 * Process the attribute.
		 */
		processAttribute(name, data, dataType, parameter);
	    }
	}
	/*
	 * Post-processing.
	 */
	postProcess(parameter);
    }

    /**
     * Does initialization work before processing the attributes. The default
     * is to do nothing.
     *
     * @param	parameter		the parameter information
     *
     * @throws	IOException
     */
    protected void preProcess(Object parameter)
	throws IOException
    {
    }

    /**
     * Does clean up work after processing the attributes. The default
     * is to do nothing.
     *
     * @param	parameter		the parameter information
     *
     * @throws	IOException
     */
    protected void postProcess(Object parameter)
	throws IOException
    {
    }

    /**
     * Process the attribute, identified by the given name, with the given
     * data. This class must be implemented by the subclasses.
     *
     * @param	name			the name of the attribute
     * @param	data			the value of the attribute
     * @param	dataType		the type of the attribute
     * @param	parameter		the parameter information
     *
     * @throws	IOException
     */
    protected abstract void processAttribute(String name,
					     Object data,
					     Class dataType,
					     Object parameter)
	throws IOException;
}

package com.broadon.bean;

import com.broadon.exception.BroadOnException;

/**
 * The <code>BeanException</code> class is the top level of bean related
 * exception hierarchy.
 *
 * @version	$Revision: 1.1 $
 */
public class BeanException
    extends BroadOnException
{
    /**
     * Constructs a BeanException instance.
     */
    public BeanException()
    {
	super();
    }

    /**
     * Constructs a BeanException instance.
     *
     * @param	message			the exception message
     */
    public BeanException(String message)
    {
	super(message);
    }

    /**
     * Constructs a BeanException instance.
     *
     * @param	message			the exception message
     * @param	throwable		the nested exception
     */
    public BeanException(String message, Throwable throwable)
    {
	super(message, throwable);
    }
}

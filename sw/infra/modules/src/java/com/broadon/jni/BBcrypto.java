package com.broadon.jni;

import java.io.IOException;
import java.math.BigInteger;

import com.broadon.exception.InvalidRequestException;
import com.broadon.hsm.HSMClient;

/**
 * JNI interface to the BBcrypto library.
 */
public class BBcrypto
{
    // Native code interface
    private static native void getTicketParams(int[] params);

    private static native boolean renewKey(byte[] newKey);

    private static native byte[] issueTicket(byte[] metadataCommon,
					     byte[] metadataCustom,
					     long bbID,
					     int crlVersion,
					     byte[] bbPublicKey,
					     byte[] iv,
					     String issuerName,
					     int ticketID,
					     int LRType,
					     short limit);

    // constant for limited rights type
    // these numbers must match with that in BBcrypto.cxx
    public static final int LR_TIME = 0;
    public static final int LR_COUNT = 1;
    public static final int LR_EXTERNAL = 2;
    public static final int PR = -1; // permanent rights
    
    static final int metadataSize;
    static final int eccPrivKeySize;
    static final int eccPubKeySize;
    static final int aesIvSize;
    static final int eTicketSigOffset;
    static final int eTicketSigSize;

    static int usage_count;
    static Object sync;		// for mutex access

    static {
	usage_count = 0;
	sync = new Object();
	System.load(System.getProperty("catalina.home") + "/lib/BBcrypto.so");
	int[] params = new int[6];
	getTicketParams(params);
	metadataSize = params[0];
	eccPrivKeySize = params[1];
	eccPubKeySize = params[2];
	aesIvSize = params[3];
	eTicketSigOffset = params[4];
	eTicketSigSize = params[5];
    }

    /**
     * Create an eTicket.
     */
    static public byte[] genTicket(byte[] metadataCommon,
                                   byte[] metadataCustom,
                                   long bbID,
                                   int crlVersion,
                                   BigInteger bbPublicKey,
                                   String issuerName,
                                   HSMClient hsm,
                                   String signer,
                                   int ticketID,
                                   int LRType,
                                   short limit,
                                   boolean getTicketInFull)
	throws IOException, InvalidRequestException
    {
	synchronized (sync) {
	    if (usage_count % 100 == 0) {
		if (! renewKey(hsm.getRandomBytes(eccPrivKeySize)))
		    throw new IOException("Error generating new key");
		usage_count = 0;
	    }
	    usage_count++;
	}

	if (metadataCommon.length + metadataCustom.length != metadataSize)
	    throw new InvalidRequestException("Incorrect metadata size");

	byte[] bbKey = bbPublicKey.toByteArray();
	if (bbKey.length > eccPubKeySize)
	    throw new InvalidRequestException("BB player public key invalid");
	byte[] iv = hsm.getRandomBytes(aesIvSize);
	byte[] unsignedTicket = issueTicket(metadataCommon,
					    metadataCustom,
					    bbID, crlVersion, bbKey, iv,
					    issuerName, ticketID, LRType,
					    limit);
	if (unsignedTicket == null)
	    throw new IOException("Error in generating eTicket");

	// sign the document
	byte[] sig = hsm.sign(signer, unsignedTicket, 0, eTicketSigOffset);
	if (sig.length != eTicketSigSize)
	    throw new IOException("Invalid signature length (" +
				  sig.length + ") -- expecting " +
				  eTicketSigSize);

	System.arraycopy(sig, 0, unsignedTicket, eTicketSigOffset, eTicketSigSize);

    byte[] result = null;
    if (getTicketInFull) {
        result = unsignedTicket; // It is actually signed at this point
    }
    else {
        // return only the customized portion
        result = new byte[unsignedTicket.length - metadataCommon.length];
        System.arraycopy(unsignedTicket, metadataCommon.length,
                         result, 0, result.length);
    }
	return result;
    }

    /**
     * Create only customized portion of eTicket.
     */
    static public byte[] genTicket(byte[] metadataCommon,
				   byte[] metadataCustom,
				   long bbID,
				   int crlVersion,
				   BigInteger bbPublicKey,
				   String issuerName,
				   HSMClient hsm,
				   String signer,
				   int ticketID,
				   int LRType,
				   short limit)
        throws IOException, InvalidRequestException
    {
        return genTicket(metadataCommon,
                         metadataCustom,
                         bbID,
                         crlVersion,
                         bbPublicKey,
                         issuerName,
                         hsm,
                         signer,
                         ticketID,
                         LRType,
                         limit,
                         false /* do not get ticket in full */);
    }


    // Keys:
    // PR:  permanent rights (unlimited play)
    // TR:  time-based limited rights
    // LR:  limited rights based on number of plays
    // XR:  limited rights based on externally provided function
    public static int parseRType(String rtype)
    {
	if (rtype == null || rtype.equals("PR"))
	    return PR;
	else if (rtype.equals("TR"))
	    return LR_TIME;
	else if (rtype.equals("LR"))
	    return LR_COUNT;
	else if (rtype.startsWith("XR")) {
	    // external right codes are encoded in the form XR<n> where
	    // <n> is the algorithm code offset from the constant
	    // BB_LIMIT_CODE_EXTERN (see C header bbtickets.h).
	    // E.g. XR3 would be transated to BB_LIMIT_CODE_EXTERN + 3
	    // which is equal to 6.
	    String value = rtype.substring(2);
	    try {
		return LR_EXTERNAL + Integer.parseInt(value);
	    } catch (NumberFormatException e) {
		return LR_EXTERNAL;
	    }
	} else
	    // default to count limit
	    return LR_COUNT;
    }
}

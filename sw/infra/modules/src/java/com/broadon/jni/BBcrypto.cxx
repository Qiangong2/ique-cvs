#include <string.h>
#include <jni.h>

#include <libcrypto/bbtoolsapi.h>
#include <libcrypto/bbticket.h>

#include "BBcrypto.h"

#define LR_TIME 0
#define LR_COUNT 1
#define LR_EXTERNAL 2
#define PR (-1)

static BbEccPrivateKey private_key;
static BbEccPublicKey public_key;

JNIEXPORT void JNICALL
Java_com_broadon_jni_BBcrypto_getTicketParams(JNIEnv *env, jclass,
					      jintArray _params)
{
    int params[6];
    params[0] = sizeof(BbContentMetaData);
    params[1] = sizeof(BbEccPrivateKey);
    params[2] = sizeof(BbEccPublicKey);
    params[3] = sizeof(BbAesIv);
    params[4] = sizeof(BbTicket) - sizeof(BbRsaSig2048);
    params[5] = sizeof(BbRsaSig2048);

    env->SetIntArrayRegion(_params, 0, sizeof(params)/sizeof(int), params);
    
} // getTicketParams


JNIEXPORT jboolean JNICALL
Java_com_broadon_jni_BBcrypto_renewKey(JNIEnv *env, jclass, jbyteArray _newKey)
{
    if (sizeof(private_key) != env->GetArrayLength(_newKey))
	return false;

    env->GetByteArrayRegion(_newKey, 0, sizeof(private_key),
			    reinterpret_cast<jbyte*>(&private_key));
    private_key[0] &= 0x1ff;		// mask off top 23 bits
    return eccGenPublicKey(public_key, private_key) == 0;

} // renewKey


// network to little endian conversion
static inline u32
ntole32(u32 x)
{
    return (x >> 24) | ((x >> 8) & 0xff00) | ((x << 8) & 0xff0000) | (x << 24);
}


JNIEXPORT jbyteArray JNICALL
Java_com_broadon_jni_BBcrypto_issueTicket(JNIEnv *env, jclass,
					  jbyteArray _metadataCommon,
					  jbyteArray _metadataCustom,
					  jlong _bbID, jint _crl_version,
					  jbyteArray _bb_key,
					  jbyteArray _iv,
					  jstring _issuer,
					  jint _tid,
					  jint _lr_type,
					  jshort _limit)
{
    // The caller passes in the metadata in two parts, here we copy them
    // back into one.
    BbContentMetaData metadata;
    int custom_offset = env->GetArrayLength(_metadataCommon);
    env->GetByteArrayRegion(_metadataCommon, 0, custom_offset, (jbyte*) &metadata);
    env->GetByteArrayRegion(_metadataCustom, 0,
			    sizeof(metadata) - custom_offset,
			    (jbyte*) (((char*) &metadata) + custom_offset));

    // copy and correct pad the public key.
    BbEccPublicKey bb_key;
    int len = env->GetArrayLength(_bb_key);
    memset(&bb_key, 0, sizeof(bb_key) - len);

    env->GetByteArrayRegion(_bb_key, 0, len,
			    reinterpret_cast<jbyte*>(&bb_key) +
			    sizeof(bb_key) - len);

    // Need to swap the EccPublicKey to little endian
    for (unsigned int i = 0; i < sizeof(bb_key)/sizeof(u32); ++i) {
	bb_key[i] = ntole32(bb_key[i]);
    }

    // Finally, the IV.
    BbAesIv iv;
    env->GetByteArrayRegion(_iv, 0, sizeof(iv),
			    reinterpret_cast<jbyte*>(&iv));

    BbTicket ticket;
    short lr_type;
    switch (_lr_type) {
    case PR:
	lr_type = 0;			// permanent rights, we really
					// don't care what it is set to.
	break;
	
    case LR_TIME:
	lr_type = BB_LIMIT_CODE_TIME2;
	break;

    case LR_COUNT:
	lr_type = BB_LIMIT_CODE_COUNT;
	break;

    default:
	// see comments in BBcrypto.java:parseRType()
	lr_type = _lr_type - LR_EXTERNAL + BB_LIMIT_CODE_EXTERN;
	break;
    }
    
    if (generateUnsignedEncryptedTicket(&metadata,
					_bbID,
					_tid,
					lr_type,
					_limit,
					_crl_version,
					iv,
					private_key,
					public_key,
					bb_key,
					&ticket) != 0)
	return NULL;

    memset(ticket.head.issuer, 0, sizeof(ticket.head.issuer));
    env->GetStringUTFRegion(_issuer, 0, env->GetStringLength(_issuer),
			 reinterpret_cast<char*>(ticket.head.issuer));

    jbyteArray _result = env->NewByteArray(sizeof(ticket));
    if (_result)
	env->SetByteArrayRegion(_result, 0, sizeof(ticket),
				reinterpret_cast<jbyte*>(&ticket));
    return _result;
} // issueTicket

package com.broadon.competitions;

import java.sql.*;
import java.io.*;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;
import java.math.BigInteger;

import com.broadon.util.Database;
import com.broadon.util.HexString;

public class ComputeScore
{
    // Native code interface
    private static native int getScore(long titleID, long contentID,
				       byte[] states, byte[] signature,
				       byte[] bbPubKey);

    Connection conn;
    int competitionID;
    String emailAddress;
    byte[] signature;
    byte[] states;
    long titleID;
    long contentID;
    byte[] bbPubKey;

    ComputeScore(Connection conn, int competitionID)
    {
	this.conn = conn;
	this.competitionID = competitionID;
    }


    static final String TITLE =
	"SELECT TITLE_ID FROM COMPETITIONS WHERE COMPETITION_ID = ?";

    long getTitle(int competitionID) throws SQLException
    {
	PreparedStatement ps = conn.prepareStatement(TITLE);
	ps.setInt(1, competitionID);
	try {
	    ResultSet rs = ps.executeQuery();
	    if (rs.next()) {
		long id = rs.getLong(1);
		if (! rs.wasNull())
		    return id;
	    }
	    throw new SQLException("Missing title ID");
	} finally {
	    ps.close();
	}
    }
    

    byte[] getGameStates(Blob blob, Inflater inf, byte[] buf)
	throws SQLException, IOException
    {
	inf.reset();
	InflaterInputStream in =
	    new InflaterInputStream(blob.getBinaryStream(), inf);
	ByteArrayOutputStream out = new ByteArrayOutputStream();
	try {
	    int n;
	    while ((n = in.read(buf, 0, buf.length)) > 0)
		out.write(buf, 0, n);
	    return out.toByteArray();
	} finally {
	    out.close();
	    in.close();
	}
    } // getGameStates


    static final String updateScore =
	"UPDATE COMPETING_CUSTOMERS SET SCORE=?, SCORE_DATE = SUBMIT_DATE " +
	"WHERE EMAIL_ADDRESS=? AND COMPETITION_ID=?";

    void updateScore() throws SQLException
    {
	// TO DO:  use different score function based on COMPETITION_ID and
	// FUNCTION_NAME in COMPETITIONS table.

	long score = getScore(titleID, contentID, states, signature, bbPubKey);
	if (score < 0) {
	    // ignore invalid scores
	    System.out.println("Ignoring invalid submission by " + emailAddress +
			       " for competition " + competitionID);
	    return;
	}
	PreparedStatement ps = conn.prepareStatement(updateScore);
	int i = 0;
	ps.setLong(++i, score);
	ps.setString(++i, emailAddress);
	ps.setInt(++i, competitionID);

	try {
	    ps.executeUpdate();
	} finally {
	    ps.close();
	}
    } // updateScore


    static final String GAME_STATES =
	"SELECT PUBLIC_KEY, CONTENT_ID, EMAIL_ADDRESS, " +
	"   SIG_GS, RAW_GS FROM COMPETING_CUSTOMERS A, BB_PLAYERS B WHERE " +
	"   COMPETITION_ID=? AND A.BB_ID = B.BB_ID";
    static final String NEW_GAME_STATES =
	" AND (SCORE IS NULL OR SCORE_DATE IS NULL OR SUBMIT_DATE > SCORE_DATE)";

    void process(boolean incremental)
	throws SQLException, IOException
    {
	titleID = getTitle(competitionID);
	
	PreparedStatement ps = null;
	ps = conn.prepareStatement(GAME_STATES +
				   (incremental ? NEW_GAME_STATES : ""));
	ps.setInt(1, competitionID);

	// main loop: read each raw game states and update the score table.

	try {
	    ResultSet rs = ps.executeQuery();
	    Inflater inf = new Inflater(false);
	    byte[] buf = new byte[4096];
	    while (rs.next()) {
		int i = 0;
		bbPubKey = new BigInteger(rs.getString(++i), 32).toByteArray();
		contentID = rs.getLong(++i);
		emailAddress = rs.getString(++i);
		try {
		    signature = HexString.fromHexString(rs.getString(++i));
		} catch (NumberFormatException e) {
		    System.out.println("Ignoring invalid submittion by " +
				       emailAddress + " for competition " +
				       competitionID + " (bad signature)");
		    continue;
		}

		try {
		    states = getGameStates(rs.getBlob(++i), inf, buf);
		} catch (IOException e) {
		    System.out.println("Ignoring invalid submittion by " +
				       emailAddress + " for competition " +
				       competitionID + " (bad game state)");
		    continue;
		}

		// convert states to score
		updateScore();
	    }
	} finally {
	    ps.close();
	}
    }


    static public void main(String[] args) {
	System.loadLibrary("ComputeScore");

	if (args.length < 1) {
	    System.err.println("Usage: ComputeScore competition_ID [-inc]");
	    System.exit(1);
	}

	int id = Integer.parseInt(args[0]);
	boolean incremental = false;
	if (args.length > 1 && "-inc".equals(args[1]))
	    incremental = true;

	try {
	    Database db = new Database();
	    Connection conn = db.getConnection();
	    ComputeScore cs = new ComputeScore(conn, id);

	    conn.setAutoCommit(false);
	    try {
		cs.process(incremental);
		conn.commit();
	    } finally {
		conn.rollback();
		conn.close();
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    System.exit(1);
	}
    }
}

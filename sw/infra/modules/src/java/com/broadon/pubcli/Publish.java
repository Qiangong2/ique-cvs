package com.broadon.pubcli;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;

import com.broadon.wsapi.cps.ActionType;
import com.broadon.wsapi.cps.ContentAttributeType;
import com.broadon.wsapi.cps.PublishPortType;
import com.broadon.wsapi.cps.PublishRequestType;
import com.broadon.wsapi.cps.PublishResponseType;
import com.broadon.wsapi.cps.PublishServiceLocator;

public class Publish
{
    private static String CPS_URL = ":17102/cps/services/PublishSOAP";
    
    private static String REQUESTER_ID = "LOCAL_CPS";
    private static String CONTENT_TYPE = "GAME";
    private static int BOOT_CONTENT_INDEX = 0;

    private static PublishPortType cpsPort;
    
    /**
     * Intializes the CPS server port.
     * 
     * This method is called after all data check is performed.
     * 
     * @param cps
     *            CPS server domain name
     * @throws MalformedURLException, Exception 
     */
    private static void initCPSPort(String cps) 
        throws MalformedURLException, Exception 
    {
        URL url = new URL("http://" + cps + CPS_URL);
        System.out.println("\nConnecting to " + url.toString() + "...");
        
        PublishServiceLocator cpsService = new PublishServiceLocator();
        cpsPort = cpsService.getPublishSOAP(url);
        
        if (cpsPort == null) {
            System.out.println("Failed!");
            throw new Exception("Failed to initialize CPS port");
        } else
            System.out.println("Connected");
    }

    /**
     * Intializes the Publish request with all the hard-coded values.
     * 
     * This method is called after all data check is performed.
     * 
     * @param req
     *            The Publish Request
     */
    private static void initRequest(PublishRequestType req)
    {
        req.setRequesterId(REQUESTER_ID);        
        req.setBootContentIndex(BOOT_CONTENT_INDEX);
    } 
    
    /**
     * Intializes the content attributes in the request.
     * 
     * @param req
     *            The Publish Request
     * @param titleDir
     *            Path to the title directory
     * @param contentList
     *            Array of all contents 
     * @throws Exception
     */
    private static void initContent(PublishRequestType req, 
                                    String titleDir, 
                                    String[] contentList)
        throws Exception 
    {
        ContentAttributeType[] contents = new ContentAttributeType[contentList.length];
        String temp = "";
        
        for (int i = 0; i < contentList.length; i++) {            
            String contentIndex = contentList[i].substring(0,contentList[i].indexOf("-"));
            
            if (!temp.equals(contentIndex)) {
                if (isValidContentIndex(contentIndex)) {
                    ContentAttributeType contentAttr = new ContentAttributeType();
                    contentAttr.setContentIndex(Integer.parseInt(contentIndex));
                    
                    String objName = contentList[i].substring(contentList[i].indexOf("-")+1);
                    contentAttr.setContentObjectName(objName);
                    contentAttr.setContentObjectType(CONTENT_TYPE);
                    contents[i] = contentAttr;
                    
                    temp = contentIndex;
                    
                } else
                    throw new Exception("Content index not valid (" + contentIndex + ")! " +
                            "Must be three characters long in the range [000-511]");              
            } else
                throw new Exception("Duplicate content index found (" + contentIndex + ")! ");            
        }
        
        req.setContents(contents);        
    }
    
    /**
     * Intializes the content objects in the request.
     * 
     * @param attachments
     *            Array of attachments
     * @param titleDir
     *            Path to the title directory
     * @param contentList
     *            Array of all contents 
     * @throws Exception
     */
    private static void initContentBlob(DataHandler[] attachments, 
                                        String titleDir, 
                                        String[] contentList) 
        throws Exception 
    {
        for (int i = 0; i < contentList.length; i++) {
            File objFile = new File(titleDir, contentList[i]);
            if (!objFile.canRead())
                throw new Exception("Can't read object file " + objFile.getCanonicalPath());
            
            attachments[i] = new DataHandler(new FileDataSource(objFile));
        }
    }
   
    /**
     * Publishes the title and obtains the response back from the CPS.
     * 
     * @param req
     *            The Publish Request
     * @param attachments 
     *            Array of contents to be sent as attachments
     * @throws Exception
     * @return The Publish Response
     */
    private static PublishResponseType publishContent(PublishRequestType req, DataHandler[] attachments)
        throws Exception
    {
        PublishResponseType resp = null;       
        
        System.out.println("\nPublishing \"" + req.getTitleName() + "\"...");
        
        long startTime = System.currentTimeMillis();
        resp = cpsPort.publish(req, attachments);
        long elapsedTime = System.currentTimeMillis() - startTime;
        
        System.out.println("Response received in " + elapsedTime / 1000 + " seconds");
        System.out.println("\nError Code: " + resp.getErrorCode());
        System.out.println("Error Message: " + resp.getErrorMessage());
        System.out.println("Comments: " + resp.getComment());
            
        return resp;
    }
    
    /**
     * Saves the title meta data obtained from the publish response
     * in a <title id>.tmd file
     * 
     * @param resp
     *            The Publish Response
     * 
     * @throws Exception
     */
    private static void dumpTitleMetaData(PublishResponseType resp,
                                          String titleDir)
        throws Exception
    {
        byte[] tmd = resp.getTitleMetaData();
        String fileName = getTitleId(titleDir)+".tmd";
        
        if (titleDir.indexOf("/") > 0)
            titleDir = titleDir + "/";
        else if (titleDir.indexOf("\\") > 0)
            titleDir = titleDir + "\\";
        else
            titleDir = titleDir + "/";
        
        System.out.println("\nSaving Ticket Meta Data...");
        
        try {
            if (tmd != null && tmd.length > 0) {
                FileOutputStream out = new FileOutputStream(titleDir+fileName); 
                out.write(tmd); 
                out.close();
            } 
        } catch (IOException ie) {
            System.out.println("Failed!");
            throw new Exception("IO Exception trying to write " + fileName);
        }
        
        System.out.println("Done");
    }
 
    /**
     * Obtains a sorted array of contents in the title directory.
     * 
     * @param contents
     *            File pointer to the title directory
     * 
     * @return Sorted array of content files names
     */
    private static String[] getContentList(File contents)
    {
        FilenameFilter filter = new FilenameFilter() {
            public boolean accept(File contents, String name) {
                return (name.indexOf("-") == 3);
            }
        };
        
        List result = new ArrayList();

        String[] files = contents.list(filter);        
        List fileLists = Arrays.asList(files);
        Iterator filesIter = fileLists.iterator();
        
        while (filesIter.hasNext()) {
          result.add(filesIter.next());
        }
        
        Collections.sort(result);
        
        String[] contentList = new String[result.size()];
        contentList = (String[]) result.toArray(contentList);
        
        return contentList;
    }

    /**
     * Obtains the title id as a substring of the title directory path name.
     * 
     * @param titleDir
     *            Path to the title directory
     * 
     * @return The Title Id
     */
    private static String getTitleId(String titleDir)
    {
        if (titleDir.indexOf("/") >= 0)
            return titleDir.substring(titleDir.lastIndexOf("/")+1);
        else if (titleDir.indexOf("\\") >= 0)
            return titleDir.substring(titleDir.lastIndexOf("\\")+1);
        else
            return null;
    }
 
    /**
     * Prompts the user for encryption password and again for verification.
     * 
     * @throws Exception
     * @return The Encryption Password
     */
    private static String getEncryptionPassword()
        throws Exception
    {
        String pswd = null;
        
        try {
            System.out.print("\nEnter encryption password: ");
            
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            pswd = br.readLine();
            
            System.out.print("Confirm encryption password: ");
            
            String pswd2 = br.readLine();
            
            if (!pswd.equals(pswd2))
                pswd = null;           
            
        } catch (IOException ioe) { 
           throw new Exception("IO Exception trying to read the encryption password!");
        } 

        return pswd;
    }    
    
    /**
     * Verifies hexString to be a valid hexadecimal string
     * 
     * @param hexString
     *            The hexadecimal string
     * 
     * @return boolean
     */
    private static boolean isValidHexString(String hexString, int length) 
    { 
        char[] hexArray = hexString.toCharArray(); 
        int hexLength = hexArray.length;
        
        if (hexLength == length) {             
            for (int i = 0; i < hexLength; i++) { 
                if (Character.digit(hexArray[i], 16) == -1)
                    return false;                
            }
            return true;
        } else
            return false;
    }
        
    /**
     * Validate titleId, verify titleId requirements for a given titleType
     *  
     * @param titleId
     *            Title Id
     * @param titleType
     *            Title Type
     * 
     * @return String - Error Message (if any)
     */
    private static String isValidTitleId(String titleId, String titleType) 
    { 
        // validate title id to be a 16-character hex string
        if (!isValidHexString(titleId, 16))
            return ("Could not obtain valid Title Id from Title Directory!");
        
        // if title type is SUBSCRIPT, validate that last 32 bits of title id is 0
        if (titleType!= null & titleType.equals("SUBSCRIPT")) {
            String last32 = titleId.substring(8);
            if (!(Integer.parseInt(last32, 16) == 0))
                return ("For Title Type: SUBSCRIPT, last 32 bits of Title ID must be 0");            
        }
        
        return null;
    }
    
    /**
     * Verifies if the content index falls within the acceptable range [000 - 511].
     * 
     * @param iContent
     *            The Content index
     * 
     * @return boolean
     */
    private static boolean isValidContentIndex(String iContent)
    {
        return (iContent.length() == 3 &&
                Integer.parseInt(iContent) >= 0 && 
                Integer.parseInt(iContent) <= 511);
    }
    
    /**
     * Prints the usage with no additional messsage.
     */
    private static void usage()
    {
       usage(null);
    }
    
    /**
     * Prints the usage with a specified messsage.
     */
    private static void usage(String message)
    {
        System.out.println("\nUsage: Publish\n" +
                "\t -t <title-name>\n" +
                "\t -y <game|rvl_sys|nc_sys|subscript>\n" +
                "\t -a <create|update>\n" +
                "\t -r <access-rights>\n" +
                "\t -d <full-path-to-the-title-directory>\n" +
                "\t -s <cps-host>\n" +
                "\t -o <os-title-id>\n" +
                "\t [-c]\n");
        
        if (message != null && !message.equals(""))
            System.out.println(message);
                
        System.exit(1);
    }
    
    public static void main(String[] args)
    {
        // verify correct number of input parameters
        if (args.length < 14 || args.length > 15)
            usage();
        
        boolean actFlag = false;
        String titleName = null;
        String titleType = null;
        String actionType = null;
        String accessRights = null;
        String titleDir = null;
        String cpsHost = null;
        String osTitleId = null;
        
        // obtain the value of input parameters
        for (int i = 0; i < args.length; i++)
        {
            if (args[i].equals("-t"))
                titleName = args[++i];
            else if (args[i].equals("-y"))
                titleType = args[++i].toUpperCase();
            else if (args[i].equals("-a"))
                actionType = args[++i];
            else if (args[i].equals("-r"))
                accessRights = args[++i];
            else if (args[i].equals("-d"))
                titleDir = args[++i];
            else if (args[i].equals("-s"))
                cpsHost = args[++i];
            else if (args[i].equals("-o"))
                osTitleId = args[++i];
            else if (args[i].equals("-c"))
                actFlag = true;
            else
                usage();
        }        
        
        if (titleName == null || actionType == null || titleDir == null || 
                titleType == null || accessRights == null || cpsHost == null ||
                osTitleId == null)
            usage();
        
        System.out.println("\nContent Publishing");
        System.out.println("==================");
        
        // restrict title_type to be one of "GAME" or "NC_SYS" or "RVL_SYS" or "SUBSCRIPT
        if (!(titleType.equals("GAME") || titleType.equals("RVL_SYS") || 
                titleType.equals("NC_SYS") || titleType.equals("SUBSCRIPT")))
            usage("*** ERROR: Invalid value provided for title type ***" +
                  "\n*** Must be \"GAME\" or \"NC_SYS\" or \"RVL_SYS\" or \"SUBSCRIPT\" but was \"" + titleType + "\" ***\n");
        
        // verify AllowCommonTicket set to true for "NC_SYS" or "RVL_SYS" title_type
        if ((titleType.equals("RVL_SYS") || titleType.equals("NC_SYS")) &&
                !actFlag) {
            System.out.println("*** ERROR: AllowCommonTicket value must be set to true for \"" + titleType + "\" ***\n");
            System.exit(1);
        }
        
        // transform input action type to correct form
        if (actionType.toLowerCase().equals("create"))
            actionType = "Create";
        else if (actionType.toLowerCase().equals("update"))
            actionType = "Update";
        else
            usage("*** ERROR: Invalid value provided for action type ***" +
                  "\n*** Must be \"create\" or \"update\" but was \"" + actionType + "\" ***\n");
        
        // validate access rights to be a 8-digit hex number
        if (!isValidHexString(accessRights, 8)) {
            System.out.println("\n*** ERROR: Invalid value for access rights! ***" +
                  "\n*** Must be a 8-digit hex number but was \"" + accessRights + "\" ***\n");
            System.exit(1);
        }
        
        // validate os title id to be a 16-digit hex number
        if (!isValidHexString(osTitleId, 16)) {
            System.out.println("\n*** ERROR: Invalid OS Title Id ***" +
                    "\n*** Must be a 16-digit hex number but was \"" + osTitleId + "\" ***\n");
            System.exit(1);
        } 
        
        if (titleDir.endsWith("/"))
            titleDir = titleDir.substring(0, titleDir.lastIndexOf("/"));
        else if (titleDir.endsWith("\\"))
            titleDir = titleDir.substring(0, titleDir.lastIndexOf("\\"));
        
        // obtain title id
        String titleId = getTitleId(titleDir);
        
        // print all input parameters
        System.out.println("\nTitle Name: " + titleName);
        System.out.println("Title Directory: " + titleDir);
        System.out.println("Title Id: " + titleId);
        System.out.println("Title Type: " + titleType);
        System.out.println("OS Title Id: " + osTitleId);
        System.out.println("Action: " + actionType);
        System.out.println("Access Rights: " + accessRights);
        System.out.println("Allow Common Ticket: " + actFlag);
        System.out.println("Content Publishing Server: " + cpsHost);        
               
        // validate title id
        String error = isValidTitleId(titleId, titleType);
        if (error != null) {
            System.out.println("\n*** ERROR: Invalid Title Id ***" +
                    "\n*** " + error + " ***\n");
            System.exit(1);
        }       
        
        try {            
            File contents = new File(titleDir);
            if (!contents.isDirectory()) {
                System.out.println("\n*** ERROR: Directory \"" + titleDir + "\" not found! ***\n");
                System.exit(1);
            }
                
            String encPassword = getEncryptionPassword();
            if (encPassword == null) {
                System.out.println("\n*** ERROR: Confirm encryption password failed! ***\n");
                System.exit(1);
            }            
            
            PublishRequestType req = new PublishRequestType();
            PublishResponseType resp = null;            
            
            System.out.println("\nInitializing Request...");
            initRequest(req);
            req.setAction(ActionType.fromString(actionType));
            req.setEncryptionPassword(encPassword);
            req.setTitleID(titleId);
            req.setTitleName(titleName);
            req.setTitleType(titleType);
            req.setOSVersion(osTitleId);
            req.setAccessRights(Integer.parseInt(accessRights, 16));            
            req.setAllowCommonTicket(Boolean.valueOf(actFlag));
            
            DataHandler attachments[] = null;
            if (!titleType.equals("SUBSCRIPT")) {
                String[] contentList = getContentList(contents);
                if (contentList.length > 0) {
                    attachments = new DataHandler[contentList.length];
                    
                    initContent(req, titleDir, contentList);
                    initContentBlob(attachments, titleDir, contentList);
                } else {
                    System.out.println("Failed!");
                    System.out.println("\n*** ERROR: No content to publish in \"" +
                            titleDir + "\"! ***\n");
                    System.exit(1);
                }
            } else
                req.setContents(null);
            
            System.out.println("Done");            
            
            initCPSPort(cpsHost);
            resp = publishContent(req, attachments);
            
            if (resp.getErrorCode() == 0)
                dumpTitleMetaData(resp, titleDir);
            
        } catch (Exception e) {
            System.out.println("\n*** ERROR: " + e.toString() + " ***\n");
            e.printStackTrace();            
            System.exit(1);
        }           
    }
}

package com.broadon.cds;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import com.broadon.filter.authWrapper;
import com.broadon.servlet.AuditLogAgent;
import com.broadon.servlet.ServletConstants;
import com.broadon.util.Queue;

/**
 * The <code>ContentServlet</code> class provides the common code to be shared
 * by the subclasses like the <code>ContentDownloadServlet</code> and the
 * <code>ContentSyncServlet</code>.
 *
 * @version	$Revision: 1.14 $
 */
public abstract class ContentServlet
    extends HttpServlet
    implements ServletConstants
{
    protected static final String	CHARSET;
    protected static final String	DEPOT_PARAM_NAME;

    private static final String		REGIONAL_CENTER_ID_KEY;

    protected static String		hostName;
    protected static String		serverLog;

    static
    {
	/*
	 * Default character set.
	 */
	CHARSET			= "utf-8";
	DEPOT_PARAM_NAME	= "depot";
	/*
	 * Key to get the regional center identifier.
	 */
	REGIONAL_CENTER_ID_KEY	= "REGIONAL_CENTER_ID";
	/*
	 * Get the host name of the current server.
	 */
	try
	{
	    InetAddress	address = InetAddress.getLocalHost();

	    hostName = address.getHostName();
	}
	catch (UnknownHostException uhe)
	{
	    uhe.printStackTrace();
	    hostName = "localhost";
	}
	serverLog = "Server[" + hostName + "]";
    }

    /*
     * The name mapping tables.
     */
    private Map		nameMap;
    private Map		reverseNameMap;
    private DataSource	dataSource;
    private Queue	auditLogQueue;
    private int		regionalCenterID;
    protected ServletContext ctx;

    /**
     * Constructs a ContentServlet type instance.
     */
    public ContentServlet()
    {
	this.nameMap = null;
	this.reverseNameMap = null;
	this.dataSource = null;
	this.auditLogQueue = null;
	this.regionalCenterID = -1;
    }

    /**
     * Initializes this servlet.
     *
     * @param	servletConfig		the servlet configuration object
     */
    public synchronized void init(ServletConfig servletConfig)
	throws ServletException
    {
	ctx = servletConfig.getServletContext();

	this.nameMap = (Map)ctx.getAttribute(NAME_MAP_KEY);
	this.reverseNameMap = (Map)ctx.getAttribute(REVERSE_NAME_MAP_KEY);
	this.dataSource = (DataSource)ctx.getAttribute(DATA_SOURCE_KEY);
	this.auditLogQueue = AuditLogAgent.getQueue(ctx);
	/*
	 * Get regional center identifier this server belongs to.
	 */
	Properties	properties;
	String		id;

	properties = (Properties)ctx.getAttribute(PROPERTY_KEY);
	id = properties.getProperty(REGIONAL_CENTER_ID_KEY);
	this.regionalCenterID = Integer.valueOf(id).intValue();
    }

    /**
     * Processes initialization for doGet, doPost, etc.
     *
     * @param	req			the HTTP request
     * @param	res			the HTTP response
     * @param	info			the pass along information
     *
     * @throws	ServletException
     * @throws	IOException
     */
    protected void processInit(HttpServletRequest req,
			       HttpServletResponse res,
			       CDSInfo info)
	throws ServletException, IOException
    {
	/*
	 * Set up information needed by this request.
	 */
	info.setNameMap(nameMap);
	info.setReverseNameMap(reverseNameMap);
	info.setDataSource(dataSource);
	info.setAuditLogQueue(auditLogQueue);
	info.setRegionalCenterID(regionalCenterID);
	/*
	 * Get depot identifier.
	 */
	authWrapper	wrapper = (authWrapper)req;

	info.setEncodedDepotID(wrapper.getEncodedDepotID());
	info.setDepotID(wrapper.getDepotID());
    }

    /**
     * Prints the XML header to the given destination, out.
     *
     * @param	out			the output destination
     * @param	req			the HTTP request
     * @param	res			the HTTP response
     * @param	topElementName		the name of the top element
     * @param	label			the DTD label
     */
    protected void printXMLHeader(PrintWriter out,
				  HttpServletRequest req,
				  String topElementName,
				  String label)
    {
	String		encoding = req.getCharacterEncoding();

	if (encoding == null)
	{
	    encoding = CHARSET;
	}
	out.println("<?xml version=\"1.0\" encoding=\"" + encoding + "\"?>");
    }
}

package com.broadon.cds.sync;

import java.util.Date;

import com.broadon.bean.Bean;

/**
 * The <c>ContentSyncRequest</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *
 * It contains information about the content sync request from the depot.
 *
 * @version	$Revision: 1.2 $
 */
public class ContentSyncRequest
    extends Bean
{
    private String		depotVersion;
    private String		depotID;
    private Date		from;
    private Date		to;

    /**
     * Constructs an empty ContentSyncRequest instance.
     */
    public ContentSyncRequest()
    {
	super();
	this.depotVersion = null;
	this.depotID = null;
	this.from = null;
	this.to = null;
    }

    /**
     * Constructs a ContentSyncRequest instance.
     *
     * @param	depotVersion		the running version of the depot
     * @param	depotID			the depot identifier
     * @param	from			the starting interval of changed records
     * @param	to			the ending interval of changed records
     */
    public ContentSyncRequest(String depotVersion,
			      String depotID,
			      Date from,
			      Date to)
    {
	super();
	this.depotVersion = depotVersion;
	this.depotID = depotID;
	this.from = from;
	this.to = to;
    }

    /**
     * Getter for depotVersion.
     *
     * @return	The version this requesting depot is running.
     */
    public String getDepotVersion()
    {
	return depotVersion;
    }

    /**
     * Setter for depotVersion.
     *
     * @param	depotVersion		the version this requesting depot
     *					is running
     */
    public void setDepotVersion(String depotVersion)
    {
	this.depotVersion = depotVersion;
    }

    /**
     * Getter for depotID.
     *
     * @return	The depot identifier.
     */
    public String getDepotID()
    {
	return depotID;
    }

    /**
     * Setter for depotID.
     *
     * @param	depotID			the depot identifier
     */
    public void setDepotID(String depotID)
    {
	this.depotID = depotID;
    }

    /**
     * Getter for from.
     *
     * @return	The starting interval of changed records.
     */
    public Date getFrom()
    {
	return from;
    }

    /**
     * Setter for from.
     *
     * @param	from			the starting interval of changed records
     */
    public void setFrom(Date from)
    {
	this.from = from;
    }

    /**
     * Getter for to.
     *
     * @return	The ending interval of changed records.
     */
    public Date getTo()
    {
	return to;
    }

    /**
     * Setter for to.
     *
     * @param	to			the ending interval of changed records
     */
    public void setTo(Date to)
    {
	this.to = to;
    }
}

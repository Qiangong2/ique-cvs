package com.broadon.cds.sync;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.broadon.bean.Bean;
import com.broadon.bean.XMLFormatWriter;
import com.broadon.cds.ContentServlet;
import com.broadon.cds.StatusCode;
import com.broadon.db.ContentSyncLog;
import com.broadon.servlet.AuditLogAgent;
import com.broadon.servlet.XMLRequestWrapper;
import com.broadon.util.VersionNumber;

/**
 * The <code>ContentSyncServlet</code> class handles the requests from
 * the depots to find out information about what need to be synchronized.
 *
 * @version	$Revision: 1.30 $
 */
public class ContentSyncServlet
    extends ContentServlet
{
    // The following tags must match what is used in
    //
    //     sw/servers/common/java/lib/com/broadon/db/BBname.map
    //     sw/servers/common/java/lib/com/broadon/db/OperationRole.java
    //
    private static final String CREATE_DATE_MEMBER = "createDate";
    private static final String LAST_UPDATED_TAG = "last_updated";

    // The following tags must match what is used in
    //
    //     sw/clients/packages/bbdepot/comm/comm.c
    //
    private static final String	DEPOT_PREFIX = "HR";
    private static final String	ROOT_TAG = "content_sync_result";
    private static final String	DEPOT_VERSION_TAG = "depot_version";
    private static final String	DEPOT_SCHEMA_VERSION_TAG = "schema_version";
    private static final String	DEPOT_ID_TAG = "hr_id";
    private static final String	BEGIN_TIME_TAG = "timestamp_begin";
    private static final String	END_TIME_TAG = "timestamp_end";
    private static final String	PROTOCOL_TAG = "protocol";
    private static final String	PORT_TAG = "port";

    private static final String	DEFAULT_PROTOCOL = "http";
    private static final String	DEFAULT_PORT = "16963";
    private static final String	URI = "/cds/download?";
    private static final String	ALLOW_HTTP_GET = "allow-http-get";

    private static final VersionNumber VERSION_1_2_0 = VersionNumber.valueOf("1.2.0");
    private static final VersionNumber VERSION_1_0_0 = VersionNumber.valueOf("1.0.0");

    private String		downloadServerURL;
    private boolean		allowGet;

    /**
     * Constructs a ContentSyncServlet instance.
     */
    public ContentSyncServlet()
    {
	super();
	downloadServerURL = null;
	allowGet = false;
    }

    /**
     * Initializes this servlet.
     *
     * @param	servletConfig		the servlet configuration object
     */
    public synchronized void init(ServletConfig servletConfig)
	throws ServletException
    {
	super.init(servletConfig);
	/*
	 * Get configuration information.
	 */
	String	protocol = servletConfig.getInitParameter(PROTOCOL_TAG);
	String	port = servletConfig.getInitParameter(PORT_TAG);

	if (protocol == null)
	{
	    protocol = DEFAULT_PROTOCOL;
	}
	if (port == null)
	{
	    port = DEFAULT_PORT;
	}
	this.downloadServerURL = protocol +
				 "://" +
				 super.hostName +
				 ":" +
				 port +
				 URI;
	this.allowGet = Boolean.valueOf(servletConfig
					    .getInitParameter(ALLOW_HTTP_GET))
			       .booleanValue();
    }

    /**
     * Handles the Get request.
     *
     * @param	req			the HTTP request
     * @param	res			the HTTP response
     *
     * @throws	ServletException
     * @throws	IOException
     */
    public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
    {
	if (!allowGet)
	{
	    /*
	     * No HTTP Get.
	     */
	    res.sendError(res.SC_NOT_IMPLEMENTED, "HTTP GET not allowed");
	    return;
	}

	/*
	 * Initialize.
	 */
	ContentSync	contentSync = new ContentSync();

	super.processInit(req, res, contentSync);
	/*
	 * Process request.
	 */
	process(req, res, contentSync);
    }

    /**
     * Handles the Post request.
     *
     * @param	req			the HTTP request
     * @param	res			the HTTP response
     *
     * @throws	ServletException
     * @throws	IOException
     */
    public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
    {
	/*
	 * Initialize.
	 */
	ContentSync	contentSync = new ContentSync();

	super.processInit(req, res, contentSync);
	/*
	 * Process request.
	 */
	process(new XMLRequestWrapper(req), res, contentSync);
    }

    /**
     * Processes the request.
     *
     * @param	req			the HTTP request
     * @param	res			the HTTP response
     *
     * @throws	ServletException
     * @throws	IOException
     */
    private void process(HttpServletRequest req,
			 HttpServletResponse res,
			 ContentSync contentSync)
	throws ServletException, IOException
    {
	/*
	 * Initialize the download server information.
	 */
	contentSync.setDepotParam(super.DEPOT_PARAM_NAME +
				  "=" +
				  contentSync.getDepotID() +
				  "&");
	/*
	 * Set output type.
	 */
	res.setContentType("text/plain; charset=" + super.CHARSET);

	PrintWriter	out = res.getWriter();
	/*
	 * Get request information.
	 */
	String	depotVersionStr = req.getParameter(DEPOT_VERSION_TAG);
	String	depotSchemaVersionStr = req.getParameter(DEPOT_SCHEMA_VERSION_TAG);
	String	depotIDString = req.getParameter(DEPOT_ID_TAG);
	String	beginTime = req.getParameter(BEGIN_TIME_TAG);
	String	endTime = req.getParameter(END_TIME_TAG);
	Map		params = req.getParameterMap();

    // Set the schema version where it is not expected as part of a request.
    // The schema version was introduced as part of a request in depot
    // version 1.2.0.
    //
    VersionNumber depotVersion = VersionNumber.valueOf(depotVersionStr);
    VersionNumber depotSchemaVersion = VersionNumber.valueOf(depotSchemaVersionStr);
    if (depotSchemaVersion == null &&
        depotVersion != null &&
        depotVersion.compareTo(VERSION_1_2_0) < 0) {
        //
        // Depots before version 1.2.0 are not expected to pass a schema
        // version as part of the request, so we set the schema version
        // to a arbitrary small value (1.0.0).
        //
        depotSchemaVersionStr = "1.0.0";
        depotSchemaVersion = VERSION_1_0_0;
    }
    contentSync.setDepotSchemaVersion(depotSchemaVersion);

	printXMLHeader(out, req, ROOT_TAG, "ContentSyncResult");
	out.println("<" + ROOT_TAG + ">");
	try
	{
	    if (depotVersion == null ||
            depotSchemaVersion == null ||
            depotIDString == null ||
            beginTime == null ||
            endTime == null)
	    {
		IllegalArgumentException 	iae;

		iae = new IllegalArgumentException(DEPOT_VERSION_TAG +
						    "[" + depotVersion + "] " +
						   DEPOT_SCHEMA_VERSION_TAG +
						    "[" + depotSchemaVersion + "] " +
						   DEPOT_ID_TAG +
						    "[" + depotIDString + "] " +
						   BEGIN_TIME_TAG +
						    "[" + beginTime + "] " +
						   END_TIME_TAG +
						    "[" + endTime + "]");
		error(out, StatusCode.CDS_BAD_ARGUMENT, iae);
		throw iae;
	    }
	    /*
	     * Validate the depot identifier.
	     */
	    if (!depotIDString.equals(contentSync.getEncodedDepotID()))
	    {
		IllegalArgumentException 	iae;

		iae = new IllegalArgumentException(DEPOT_ID_TAG +
						    "[" + depotIDString + "]");
		error(out, StatusCode.CDS_INVALID_DEPOT, iae);
		throw iae;
	    }
	    /*
	     * The given date is only up to the seconds. Construct the date by
	     * converting them to milliseconds.
	     */
	    Date		from = new Date(Long.parseLong(beginTime)*1000);
	    Date		to = new Date(Long.parseLong(endTime)*1000);

	    ContentSyncRequest	contentSyncRequest;
	    XMLFormatWriter	formatter;
 
        if (depotVersion.compareTo(VERSION_1_2_0) < 0)
            contentSyncRequest = new ContentSyncRequest(depotVersionStr,
                                                        depotIDString,
                                                        from,
                                                        to);
        else
            contentSyncRequest = new ContentSyncRequest_1_2_0(depotVersionStr,
                                                              depotSchemaVersionStr,
                                                              depotIDString,
                                                              from,
                                                              to);
	    formatter = new XMLFormatWriter(contentSyncRequest);
	    formatter.setNameMap(contentSync.getNameMap());

	    formatter.format(out, 0, null);
	    sync(from, to, params, contentSync, out);
	}
	catch (Throwable t)
	{
	    res.setStatus(res.SC_ACCEPTED);
	    error(out, StatusCode.CDS_INCOMPLETE, t);
	    t.printStackTrace();
	}
	finally
	{
	    out.println("</" + ROOT_TAG + ">");
	}
    }

    /**
     * Reports the error back to the requester.
     *
     * @param	out			the destination
     * @param	statusCode		the status code
     * @param	throwable		the error information
     */
    private void error(PrintWriter out, String statusCode, Throwable throwable)
    {
	out.println("  <status>" + statusCode + "</status>");
	out.println("  <status_msg>");
	throwable.printStackTrace(out);
	out.println("  </status_msg>");
    }

    /**
     * Performs the synchronize request, returning records that have been
     * updated between the given interval [from, to). It also returns the
     * recommended download site based on the given depot identifier.
     *
     * @param	from			the time that changes made on and after
     * @param	to			the time that changes made before
     * @param	params			the additional parameters
     * @param	contentSync		the ContentSync instance
     * @param	out			the output destination
     */
    private void sync(Date from,
                      Date to,
                      Map params,
                      ContentSync contentSync,
                      PrintWriter out)
	throws Throwable
    {
	Map		nameMap = contentSync.getNameMap();
	ContentSyncLog	contentSyncLog = new ContentSyncLog();
	Bean[]		beans;
	String		rootTag;

	/*
	 * Prepare the audit log information.
	 */
	contentSyncLog.setRequestDate(new Date());
	contentSyncLog.setRequestLog(serverLog);
	contentSyncLog.setRequestStatus(ContentSyncLog.STATUS_OK);
	contentSyncLog.setDepotID(new Long(contentSync.getDepotID()));
	contentSyncLog.setCdsTimestamp(from);
	contentSyncLog.setNewCdsTimestamp(to);
	/*
	 * Query information.
	 */
	contentSync.beginTransaction();
	try
	{
	    /*
	     * Get database time, in seconds since epoch.
	     */
	    Date	currentTime = contentSync.getCurrentTime();

	    out.print("  <db_systime>");
	    out.print(currentTime.getTime() / 1000);
	    out.println("</db_systime>");
	    /*
	     * Content information (without the content itself).
	     */
	    beans = contentSync.getContentInfo(from, to, params);
	    if (beans != null)
	    {
		rootTag = contentSync.getStringID(ContentSync.CONTENT_INFO);
		format(beans, rootTag, nameMap, out);
	    }
	    /*
	     * Title information.
	     */
	    beans = contentSync.getTitles(from, to, params);
	    if (beans != null)
	    {
		rootTag = contentSync.getStringID(ContentSync.TITLE);
		format(beans, rootTag, nameMap, out);
	    }
	    /*
	     * Title content information.
	     */
	    beans = contentSync.getTitleContents(from, to, params);
	    if (beans != null)
	    {
		rootTag = contentSync.getStringID(ContentSync.TITLE_CONTENT);
		format(beans, rootTag, nameMap, out);
	    }
	    /*
	     * Title review information.
	     */
	    beans = contentSync.getTitleReviews(from, to, params);
	    if (beans != null)
	    {
		rootTag = contentSync.getStringID(ContentSync.TITLE_REVIEW);
		format(beans, rootTag, nameMap, out);
	    }
	    /*
	     * Title region information.
	     */
	    beans = contentSync.getTitleRegions(from, to, params);
	    if (beans != null)
	    {
		rootTag = contentSync.getStringID(ContentSync.TITLE_REGION);
		format(beans, rootTag, nameMap, out);
	    }
	    /*
	     * eCard type information.
	     */
	    beans = contentSync.getECardTypes(from, to, params);
	    if (beans != null)
	    {
		rootTag = contentSync.getStringID(ContentSync.ECARD_TYPE);
		format(beans, rootTag, nameMap, out);
	    }
	    /*
	     * eTicket CRL information.
	     */
	    beans = contentSync.getETicketCRL(from, to, params);
	    if (beans != null)
	    {
		rootTag = contentSync.getStringID(ContentSync.ETICKET_CRL);
		format(beans, rootTag, nameMap, out);
	    }
	    /*
	     * BB hardware releases.
	     */
	    beans = contentSync.getBBHwReleases(from, to, params);
	    if (beans != null)
	    {
		rootTag = contentSync.getStringID(ContentSync.BB_HW_RELEASE);
		format(beans, rootTag, nameMap, out);
	    }
	    /*
	     * BB content types.
	     */
	    beans = contentSync.getBBContentTypes(from, to, params);
	    if (beans != null)
	    {
		rootTag = contentSync.getStringID(ContentSync.BB_CONTENT_TYPE);
		format(beans, rootTag, nameMap, out);
	    }
        /*
         * CompetingCategories information.
         */
        beans = contentSync.getCompetingCategories(from, to, params);
        if (beans != null) {
            rootTag = contentSync.getStringID(ContentSync.COMPETING_CATEGORY);
            format(beans, rootTag, nameMap, out);
        }
        /*
         * Competitions information.
         */
        beans = contentSync.getCompetitions(from, to, params);
        if (beans != null) {
            rootTag = contentSync.getStringID(ContentSync.COMPETITION);
            format(beans, rootTag, nameMap, out);
        }
        /*
         * Operator information.
         */
        beans = contentSync.getOperationUsers(from, to, params);
        if (beans != null) {
            rootTag = contentSync.getStringID(ContentSync.OPERATION_USER);
            format(beans, rootTag, nameMap, out);
        }
        /*
         * Operation user roles information.
         */
        beans = contentSync.getOperationUserRoles(from, to, params);
        if (beans != null) {
            rootTag = contentSync.getStringID(ContentSync.OPERATION_USER_ROLE);
            format(beans, rootTag, nameMap, out);
        }
        /*
         * Operation roles information.
         */
        beans = contentSync.getOperationRoles(from, to, params);
        if (beans != null) {
            rootTag = contentSync.getStringID(ContentSync.OPERATION_ROLE);
            format(beans, rootTag, nameMap, out);
        }
	    /*
	     * Download site information.
	     */
	    beans = contentSync.getDownloadSites(
					contentSync.getRegionalCenterID(),
					contentSync.getDepotID());
	    if (beans != null)
	    {
		rootTag = "content_download_server";
		format(beans, rootTag, nameMap, out);
	    }
	    /*
	     * Record count information.
	     */
	    beans = contentSync.getRecordCounts(to);
	    if (beans != null)
	    {
		rootTag = "record_count";
		format(beans, rootTag, nameMap, out);
	    }
	}
	catch (Throwable t)
	{
	    /*
	     * Record the failure.
	     */
	    contentSyncLog.setRequestStatus(ContentSyncLog.STATUS_FAILED);
	    contentSyncLog.setRequestLog(serverLog + " " + t.getMessage());
	    throw t;
	}
	finally
	{
	    contentSync.commitTransaction();
	    /*
	     * Make the request to create the audit log.
	     */
	    AuditLogAgent.createLog(contentSyncLog);
	}
    }

    /**
     * Formats the given beans as part of the XML document.
     *
     * @param	beans			the beans to be formatted
     * @param	rootTag			the root tag element of each bean
     * @param	nameMap			the name mapping table
     * @param	out			the destination
     *
     * @throws	IOException
     */
    private void format(Bean[] beans,
			String rootTag,
			Map nameMap,
			PrintWriter out)
	throws IOException
    {
        int	size = beans.length;

        for (int n = 0; n < size; n++) {
            if (beans[n] != null) {
                XMLFormatWriter	formatter = new XMLFormatWriter(beans[n]);

                formatter.setNameMap(nameMap);
                formatter.format(out, 1, rootTag);
            }
        }
    }
}

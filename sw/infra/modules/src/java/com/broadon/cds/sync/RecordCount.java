package com.broadon.cds.sync;

import com.broadon.bean.Bean;

/**
 * The <c>RecordCount</c> class conforms to the JavaBeans property, and
 * contains the record count information of certain tables.
 *
 * @version	$Revision: 1.1 $
 */
public class RecordCount
    extends Bean
{
    private String	tableName;
    private int		count;

    /**
     * Constructs a RecordCount instance.
     *
     * @param	tableName		the table name
     * @param	count			the record count
     */
    public RecordCount(String tableName, int count)
    {
	this.tableName = tableName;
	this.count = count;
    }

    /**
     * Getter for tableName.
     *
     * @return	The table name.
     */
    public String getTableName()
    {
	return tableName;
    }

    /**
     * Setter for tableName.
     *
     * @param	tableName		the table name
     */
    public void setTableName(String tableName)
    {
	this.tableName = tableName;
    }

    /**
     * Getter for count.
     *
     * @return	The record count.
     */
    public int getCount()
    {
	return count;
    }

    /**
     * Setter for count.
     *
     * @param	count			the record count
     */
    public void setCount(int count)
    {
	this.count = count;
    }
}

package com.broadon.cds.sync;

import java.util.Date;

/**
 * The <c>ContentSyncRequest_1_2_0</c> extends then JavaBean ContentSyncRequest
 * to incorporate some new elements in the requests from depot version 1.2.0
 * later.
 *
 * It contains information about the content sync request from the depot.
 *
 * @version	$Revision: 1.1 $
 */
public class ContentSyncRequest_1_2_0 extends ContentSyncRequest
{
    private String depotSchemaVersion;

    /**
     * Constructs an empty ContentSyncRequest_1_2_0 instance.
     */
    public ContentSyncRequest_1_2_0()
    {
        super();
        this.depotSchemaVersion = null;
    }

    /**
     * Constructs a ContentSyncRequest instance.
     *
     * @param	depotSchemaVersion the schema version used by the depot
     * @param	depotVersion		the running version of the depot
     * @param	depotID			the depot identifier
     * @param	from			the starting interval of changed records
     * @param	to			the ending interval of changed records
     */
    public ContentSyncRequest_1_2_0(String depotSchemaVersion,
                                    String depotVersion,
                                    String depotID,
                                    Date from,
                              Date to)
    {
        super(depotVersion, depotID, from, to);
        this.depotSchemaVersion = depotSchemaVersion;
    }

    /**
     * Getter for depotSchemaVersion.
     *
     * @return	The schema version this requesting depot is using.
     */
    public String getDepotSchemaVersion()
    {
        return depotSchemaVersion;
    }

    /**
     * Setter for depotSchemaVersion.
     *
     * @param	depotSchemaVersion the schema version this requesting depot
     *                             is using
     */
    public void setDepotSchemaVersion(String depotSchemaVersion)
    {
        this.depotSchemaVersion = depotSchemaVersion;
    }
} // ContentSyncRequest_1_2_0

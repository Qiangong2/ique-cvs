package com.broadon.cds.sync;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import com.broadon.bean.Bean;
import com.broadon.cds.CDSInfo;
import com.broadon.db.BBContentTypeFactory;
import com.broadon.db.BBHwReleaseFactory;
import com.broadon.db.CompetingCategoryFactory;
import com.broadon.db.CompetitionFactory;
import com.broadon.db.ContentInfo;
import com.broadon.db.ContentInfoFactory;
import com.broadon.db.DBAccessFactory;
import com.broadon.db.DBException;
import com.broadon.db.Depot;
import com.broadon.db.DepotFactory;
import com.broadon.db.ECardTypeFactory;
import com.broadon.db.ETicketCRLFactory;
import com.broadon.db.OperationRoleFactory_cdsSync;
import com.broadon.db.OperationUserFactory_cdsSync;
import com.broadon.db.OperationUserRoleFactory;
import com.broadon.db.RegionalServer;
import com.broadon.db.RegionalServerFactory;
import com.broadon.db.SyncableFactory;
import com.broadon.db.Time;
import com.broadon.db.TimeFactory;
import com.broadon.db.TitleContentFactory;
import com.broadon.db.TitleFactory;
import com.broadon.db.TitleRegionFactory;
import com.broadon.db.TitleReviewFactory;
import com.broadon.exception.BroadOnException;
import com.broadon.util.VersionNumber;

/**
 * The <code>ContentSync</code> class provides the necessary support for
 * finding out what downloads are needed for the depot to become
 * synchronized with the operation center. However, it does not support
 * the actually content download, which is the job of the Content Download
 * Server.
 *
 * @version	$Revision: 1.27 $
 */
public class ContentSync
    extends CDSInfo
{
    public static final int		CONTENT_INFO	= 0;
    public static final int		DEPOT		= 1;
    public static final int		TITLE		= 2;
    public static final int		TITLE_CONTENT	= 3;
    public static final int		TITLE_REGION	= 4;
    public static final int		TITLE_REVIEW	= 5;
    public static final int		REGIONAL_SERVER	= 6;
    public static final int		ECARD_TYPE	= 7;
    public static final int		ETICKET_CRL	= 8;
    public static final int		BB_HW_RELEASE	= 9;
    public static final int		BB_CONTENT_TYPE	= 10;
    public static final int		TIME		= 11;
    public static final int		COMPETING_CATEGORY = 12;
    public static final int		COMPETITION = 13;
    public static final int		OPERATION_USER = 14;
    public static final int		OPERATION_USER_ROLE = 15;
    public static final int		OPERATION_ROLE = 16;

    /*
     * Factories used by the ContentSync instance.
     */
    private static String[]		factoryNames =
        {
			ContentInfoFactory.class.getName(),
			DepotFactory.class.getName(),
			TitleFactory.class.getName(),
			TitleContentFactory.class.getName(),
			TitleRegionFactory.class.getName(),
			TitleReviewFactory.class.getName(),
			RegionalServerFactory.class.getName(),
			ECardTypeFactory.class.getName(),
			ETicketCRLFactory.class.getName(),
			BBHwReleaseFactory.class.getName(),
			BBContentTypeFactory.class.getName(),
			TimeFactory.class.getName(),
			CompetingCategoryFactory.class.getName(),
			CompetitionFactory.class.getName(),
			OperationUserFactory_cdsSync.class.getName(),
			OperationUserRoleFactory.class.getName(),
			OperationRoleFactory_cdsSync.class.getName()
        };
    private static DBAccessFactory[]	factories;
    private static SyncableFactory[]	syncableFactories;

    static
    {
	try
	{
	    /*
	     * Get the factory instances.
	     */
	    ArrayList	list = new ArrayList();
	    int		size = factoryNames.length;
	    int		count = 0;

	    factories = new DBAccessFactory[size];
	    for (int n = 0; n < size; n++)
	    {
            factories[n] = DBAccessFactory.getInstance(factoryNames[n]);
            if (factories[n] instanceof SyncableFactory)
            {
                list.add((SyncableFactory)factories[n]);
                count++;
            }
	    }
	    syncableFactories = new SyncableFactory[count];
	    for (int n = 0; n < count; n++)
	    {
            syncableFactories[n] = (SyncableFactory)list.get(n);
	    }
	}
	catch (Throwable t)
	{
	    t.printStackTrace();
	    System.exit(-1);
	}
    }

    private String			depotParam;
    private VersionNumber   depotSchemaVersion;

    /**
     * Constructs an empty ContentSync instance.
     */
    public ContentSync()
    {
	super();
	this.depotParam = null;
	this.depotSchemaVersion = null;
    }

    /**
     * Sets the URL parameter that contains the depot information.
     *
     * @param	depotParam		the depot parameter
     */
    public void setDepotParam(String depotParam)
    {
	this.depotParam = depotParam;
    }

    /**
     * Sets the URL parameter that contains the depot information.
     * Must be called prior to calls to getXxxx routines.
     *
     * @param	depotParam		the depot parameter
     */
    public void setDepotSchemaVersion(VersionNumber schemaVersion)
    {
        this.depotSchemaVersion = schemaVersion;
    }

    /**
     * Begins a transaction.
     *
     * @throws	BroadOnException
     */
    public void beginTransaction()
	throws BroadOnException
    {
	DBAccessFactory.beginTransaction(super.dataSource, false, true);
    }

    /**
     * Commits a transaction.
     *
     * @throws	BroadOnException
     */
    public void commitTransaction()
	throws BroadOnException
    {
	try
	{
	    DBAccessFactory.commitTransaction();
	}
	catch (DBException de)
	{
	    de.printStackTrace();
	}
    }

    /**
     * Returns the current database time.
     *
     * @return	The current database time in UTC, as Date.
     *
     * @throws	BroadOnException
     * @throws	IOException
     */
    public Date getCurrentTime()
	throws BroadOnException, IOException
    {
	TimeFactory	timeFactory = (TimeFactory)factories[TIME];
	Time		time = timeFactory.getUtcTime(super.nameMap);

	return time.getSysdate();
    }

    /**
     * Returns the information on the suggested download sites.
     *
     * @param	regionalCenterID	the regional center identifier
     * @param	depotID			the depot identifier
     *
     * @return	The DownloadSite JavaBeans containing the URL prefix.
     *
     * @throws	BroadOnException
     * @throws	IOException
     */
    public Bean[] getDownloadSites(int regionalCenterID, long depotID)
	throws BroadOnException, IOException
    {
	RegionalServerFactory	regionalServerFactory =
				    (RegionalServerFactory)
					factories[REGIONAL_SERVER];
	RegionalServer		regionalServer;

	regionalServer = regionalServerFactory.getRegionalServer(
							regionalCenterID,
							"CDDS",
							super.nameMap);
	return (regionalServer == null)
		    ? null
		    : new DownloadSite[]
			{
			    new DownloadSite(regionalServer.getServerAddr() +
					     "?" +
					     depotParam)
			};
    }

    /**
     * Returns the depot information, based on the given depotID.
     *
     * @param	depotID			the depot identifier
     *
     * @return	The Depot JavaBeans containing all information of
     *		a Depot record.
     *
     * @throws	BroadOnException
     * @throws	IOException
     */
    public Depot getDepot(long depotID)
	throws BroadOnException, IOException
    {
	DepotFactory	depotFactory = (DepotFactory)factories[DEPOT];

	return depotFactory.getDepot(depotID, super.nameMap);
    }

    /**
     * Returns ContentInfo records that were updated between the given time
     * interval, [fromTime, toTime).
     *
     * @param	fromTime		the beginning interval (inclusive)
     * @param	toTime			the ending interval (exclusive)
     * @param	params			the additional parameters
     *
     * @return	The array of ContentInfo JavaBeans, each contains all
     *		information of a Content-Objects record except the
     *		content itself. Each ContentInfo also contains the URI
     *		to be used to identify the content during content download.
     *
     * @throws	BroadOnException
     * @throws	IOException
     */
    public Bean[] getContentInfo(Date fromTime, Date toTime, Map params)
	throws BroadOnException, IOException
    {
	ContentInfoFactory	contentInfoFactory;
	Bean[]			beans;

	contentInfoFactory = (ContentInfoFactory)factories[CONTENT_INFO];
	fromTime = getFromTime(params, contentInfoFactory, fromTime);
	beans = contentInfoFactory.getRecordsWithinInterval(depotSchemaVersion,
                                                        fromTime,
                                                        toTime,
                                                        super.nameMap);
	if (beans != null)
	{
	    /*
	     * Set the URI.
	     *
	     * Use the checksum instead of contentID so that both BB and
	     * RMS can use the same scheme (we also include the contentID
	     * in the URI for debugging purposes and for the
	     * content_download_logs).
	     */
	    int	size = beans.length;

	    for (int n = 0; n < size; n++)
	    {
            ContentInfo	 contentInfo = (ContentInfo)beans[n];
            final String checksum = contentInfo.getContentChecksum();
            final String cid = ((String)super.nameMap.get("contentID") +
                                "=" + contentInfo.getContentID());
            if (checksum != null)
            {
                contentInfo.setURI(cid +"&id=" + checksum);
            }
            else
            {
                contentInfo.setURI(cid);
            }
	    }
	}
	return beans;
    }

    /**
     * Returns TitleContent records that were updated between the given time
     * interval, [fromTime, toTime).
     *
     * @param	fromTime		the beginning interval (inclusive)
     * @param	toTime			the ending interval (exclusive)
     * @param	params			the additional parameters
     *
     * @return	The array of TitleContent JavaBeans, each contains the
     *		information of a Content-Title-Objects record.
     *
     * @throws	BroadOnException
     * @throws	IOException
     */
    public Bean[] getTitleContents(Date fromTime, Date toTime, Map params)
	throws BroadOnException, IOException
    {
	TitleContentFactory	titleContentFactory;

	titleContentFactory = (TitleContentFactory)factories[TITLE_CONTENT];
	fromTime = getFromTime(params, titleContentFactory, fromTime);
	return titleContentFactory.getRecordsWithinInterval(depotSchemaVersion,
                                                        fromTime,
                                                        toTime,
                                                        super.nameMap);
    }

    /**
     * Returns Title records that were updated between the given time
     * interval, [fromTime, toTime).
     *
     * @param	fromTime		the beginning interval (inclusive)
     * @param	toTime			the ending interval (exclusive)
     * @param	params			the additional parameters
     *
     * @return	The array of Title JavaBeans, each contains the
     *		information of a Content-Title record.
     *
     * @throws	BroadOnException
     * @throws	IOException
     */
    public Bean[] getTitles(Date fromTime, Date toTime, Map params)
	throws BroadOnException, IOException
    {
	TitleFactory	titleFactory = (TitleFactory)factories[TITLE];

	fromTime = getFromTime(params, titleFactory, fromTime);
	return titleFactory.getRecordsWithinInterval(depotSchemaVersion,
                                                 fromTime,
                                                 toTime,
                                                 super.nameMap);
    }

    /**
     * Returns TitleReview records that were updated between the given time
     * interval, [fromTime, toTime).
     *
     * @param	fromTime		the beginning interval (inclusive)
     * @param	toTime			the ending interval (exclusive)
     * @param	params			the additional parameters
     *
     * @return	The array of TitleReview JavaBeans, each contains the
     *		information of a Content-Title-Reviews record.
     *
     * @throws	BroadOnException
     * @throws	IOException
     */
    public Bean[] getTitleReviews(Date fromTime, Date toTime, Map params)
	throws BroadOnException, IOException
    {
	TitleReviewFactory	titleReviewFactory;

	titleReviewFactory = (TitleReviewFactory)factories[TITLE_REVIEW];
	fromTime = getFromTime(params, titleReviewFactory, fromTime);
	return titleReviewFactory.getRecordsWithinInterval(depotSchemaVersion,
                                                       fromTime,
                                                       toTime,
                                                       super.nameMap);
    }

    /**
     * Returns TitleRegion records that were updated between the given time
     * interval, [fromTime, toTime).
     *
     * @param	fromTime		the beginning interval (inclusive)
     * @param	toTime			the ending interval (exclusive)
     * @param	params			the additional parameters
     *
     * @return	The array of TitleRegion JavaBeans, each contains the
     *		information of a Content-Title-Regions record.
     *
     * @throws	BroadOnException
     * @throws	IOException
     */
    public Bean[] getTitleRegions(Date fromTime, Date toTime, Map params)
	throws BroadOnException, IOException
    {
	TitleRegionFactory	titleRegionFactory;

	titleRegionFactory = (TitleRegionFactory)factories[TITLE_REGION];
	fromTime = getFromTime(params, titleRegionFactory, fromTime);
	return titleRegionFactory.getRecordsWithinInterval(depotSchemaVersion,
                                                       fromTime,
                                                       toTime,
                                                       super.nameMap);
    }

    /**
     * Returns ECardType records that were updated between the given time
     * interval, [fromTime, toTime).
     *
     * @param	fromTime		the beginning interval (inclusive)
     * @param	toTime			the ending interval (exclusive)
     * @param	params			the additional parameters
     *
     * @return	The array of ECardType JavaBeans, each contains the
     *		information of a Content-Title-Regions record.
     *
     * @throws	BroadOnException
     * @throws	IOException
     */
    public Bean[] getECardTypes(Date fromTime, Date toTime, Map params)
	throws BroadOnException, IOException
    {
	ECardTypeFactory	eCardTypeFactory;

	eCardTypeFactory = (ECardTypeFactory)factories[ECARD_TYPE];
	fromTime = getFromTime(params, eCardTypeFactory, fromTime);
	return eCardTypeFactory.getRecordsWithinInterval(depotSchemaVersion,
                                                     fromTime,
                                                     toTime,
                                                     super.nameMap);
    }

    /**
     * Returns the ETicketCRL record that was updated between the given time
     * interval, [fromTime, toTime).
     *
     * @param	fromTime		the beginning interval (inclusive)
     * @param	toTime			the ending interval (exclusive)
     * @param	params			the additional parameters
     *
     * @return	The array of ETicketCRL JavaBeans, each contains the
     *		information of a eTicket-CRL record. There should at
     *		most be one.
     *
     * @throws	BroadOnException
     * @throws	IOException
     */
    public Bean[] getETicketCRL(Date fromTime, Date toTime, Map params)
	throws BroadOnException, IOException
    {
	ETicketCRLFactory	eTicketCRLFactory;

	eTicketCRLFactory = (ETicketCRLFactory)factories[ETICKET_CRL];
	fromTime = getFromTime(params, eTicketCRLFactory, fromTime);
	return eTicketCRLFactory.getRecordsWithinInterval(depotSchemaVersion,
                                                      fromTime,
                                                      toTime,
                                                      super.nameMap);
    }

    /**
     * Returns the BBHwRelease records that were updated between the given time
     * interval, [fromTime, toTime).
     *
     * @param	fromTime		the beginning interval (inclusive)
     * @param	toTime			the ending interval (exclusive)
     * @param	params			the additional parameters
     *
     * @return	The array of BBHwRelease JavaBeans, each contains the
     *		information of a BB-Hw-Releases record.
     *
     * @throws	BroadOnException
     * @throws	IOException
     */
    public Bean[] getBBHwReleases(Date fromTime, Date toTime, Map params)
	throws BroadOnException, IOException
    {
	BBHwReleaseFactory	bbHwReleaseFactory;

	bbHwReleaseFactory = (BBHwReleaseFactory)factories[BB_HW_RELEASE];
	fromTime = getFromTime(params, bbHwReleaseFactory, fromTime);
	return bbHwReleaseFactory.getRecordsWithinInterval(depotSchemaVersion,
                                                       fromTime,
                                                       toTime,
                                                       super.nameMap);
    }

    /**
     * Returns the BBContentType records that were updated between the given
     * time interval, [fromTime, toTime).
     *
     * @param	fromTime		the beginning interval (inclusive)
     * @param	toTime			the ending interval (exclusive)
     * @param	params			the additional parameters
     *
     * @return	The array of BBContentType JavaBeans, each contains the
     *		information of a BB-Content-Types record.
     *
     * @throws	BroadOnException
     * @throws	IOException
     */
    public Bean[] getBBContentTypes(Date fromTime, Date toTime, Map params)
	throws BroadOnException, IOException
    {
	BBContentTypeFactory	bbContentTypeFactory;

	bbContentTypeFactory = (BBContentTypeFactory)factories[BB_CONTENT_TYPE];
	fromTime = getFromTime(params, bbContentTypeFactory, fromTime);
	return bbContentTypeFactory.getRecordsWithinInterval(depotSchemaVersion,
                                                         fromTime,
                                                         toTime,
                                                         super.nameMap);
    }

    /**
     * Returns the record counts upto the given ending time of all the
     * queried tables.
     *
     * @param	time			the ending time (exclusive)
     *
     * @return	The array of RecordCount JavaBeans, each containts the
     *		table name and the corresponding record count.  Factories not
     *      supported by the depot are represented by a null value in the
     *      table of JavaBeans.
     *
     * @throws	BroadOnException
     * @throws	IOException
     */
    public Bean[] getRecordCounts(Date time)
	throws BroadOnException, IOException
    {
        int		size = syncableFactories.length;
        RecordCount[]	recordCounts = new RecordCount[size];

        for (int n = 0; n < size; n++) {
            String	name = getStringID(syncableFactories[n]);
            int		count = 
                syncableFactories[n].getRecordCount(time, depotSchemaVersion);
        
            if (count != -1)
                recordCounts[n] = new RecordCount(name, count);
            else
                recordCounts[n] = null;
        }
        return recordCounts;
    }

    /**
     * Returns the string identifier of the given name key. This is mainly
     * used as the root tag for a Bean to be converted into XML
     * format.
     *
     * @param	nameKey			the index into the factories array
     *
     * @return	The table name of the corresponding factory.
     */
    public String getStringID(int nameKey)
    {
	return getStringID(factories[nameKey]);
    }

    /**
     * Returns the string identifier of the given name key on the given
     * factory.
     *
     * @param	factory			the table factory
     *
     * @return	The table name of the corresponding factory.
     */
    private String getStringID(DBAccessFactory factory)
    {
	return factory.getTableName().toLowerCase();
    }

    /**
     * Returns the beginning time of the given table if one exists;
     * otherwise returns the default one.
     *
     * @param	params			the additional parameters
     * @param	factory			the table factory
     * @param	fromTime		the default beginning time
     *
     * @return	The beginning time.
     */
    private Date getFromTime(Map params, DBAccessFactory factory, Date fromTime)
    {
	if (params == null)
	    return fromTime;

	String	tableTime = (String)params.get(getStringID(factory));

	return (tableTime == null)
		    ? fromTime
		    : new Date(Long.parseLong(tableTime)*1000);
    }

    /**
     * Returns CompetingCategory records that were updated between the given time
     * interval, [fromTime, toTime).
     *
     * @param	fromTime		the beginning interval (inclusive)
     * @param	toTime			the ending interval (exclusive)
     * @param	params			the additional parameters
     *
     * @return	The array of CompetingCategory JavaBeans, each contains the
     *		information of a competing_category record.
     *
     * @throws	BroadOnException
     * @throws	IOException
     */
    public Bean[] getCompetingCategories(Date fromTime, 
                                         Date toTime, 
                                         Map  params)
	throws BroadOnException, IOException
    {
        CompetingCategoryFactory competingCategoryFactory;

        competingCategoryFactory = 
            (CompetingCategoryFactory)factories[COMPETING_CATEGORY];
        fromTime = getFromTime(params, competingCategoryFactory, fromTime);
        return competingCategoryFactory.getRecordsWithinInterval(depotSchemaVersion,
                                                                 fromTime,
                                                                 toTime,
                                                                 super.nameMap);
    }

    /**
     * Returns Competition records that were updated between the given time
     * interval, [fromTime, toTime).
     *
     * @param	fromTime		the beginning interval (inclusive)
     * @param	toTime			the ending interval (exclusive)
     * @param	params			the additional parameters
     *
     * @return	The array of Competition JavaBeans, each containing the
     *		information of a competition record.
     *
     * @throws	BroadOnException
     * @throws	IOException
     */
    public Bean[] getCompetitions(Date fromTime, Date toTime, Map params)
	throws BroadOnException, IOException
    {
        CompetitionFactory	competitionFactory;

        competitionFactory = (CompetitionFactory)factories[COMPETITION];
        fromTime = getFromTime(params, competitionFactory, fromTime);
        return competitionFactory.getRecordsWithinInterval(depotSchemaVersion,
                                                           fromTime,
                                                           toTime,
                                                           super.nameMap);
    }

    /**
     * Returns OperationUsers records that were updated between the 
     * given time interval, [fromTime, toTime).
     *
     * @param	fromTime		the beginning interval (inclusive)
     * @param	toTime			the ending interval (exclusive)
     * @param	params			the additional parameters
     *
     * @return	The array of OperationUser JavaBeans, each containing the
     *		information about an operator.
     *
     * @throws	BroadOnException
     * @throws	IOException
     */
    public Bean[] getOperationUsers(Date fromTime, Date toTime, Map params)
	throws BroadOnException, IOException
    {
        OperationUserFactory_cdsSync operationUserFactory;

        operationUserFactory = (OperationUserFactory_cdsSync)factories[OPERATION_USER];
        fromTime = getFromTime(params, operationUserFactory, fromTime);
        return operationUserFactory.getRecordsWithinInterval(depotSchemaVersion,
                                                             fromTime,
                                                             toTime,
                                                             super.nameMap);
    }

    /**
     * Returns OperationUserRoles records that were updated between the 
     * given time interval, [fromTime, toTime).
     *
     * @param	fromTime		the beginning interval (inclusive)
     * @param	toTime			the ending interval (exclusive)
     * @param	params			the additional parameters
     *
     * @return	The array of OperationUserRole JavaBeans, each containing the
     *		information about the roles of an operator.
     *
     * @throws	BroadOnException
     * @throws	IOException
     */
    public Bean[] getOperationUserRoles(Date fromTime, Date toTime, Map params)
	throws BroadOnException, IOException
    {
        OperationUserRoleFactory operationUserRoleFactory;

        operationUserRoleFactory = (OperationUserRoleFactory)factories[OPERATION_USER_ROLE];
        fromTime = getFromTime(params, operationUserRoleFactory, fromTime);
        return operationUserRoleFactory.getRecordsWithinInterval(depotSchemaVersion,
                                                                 fromTime,
                                                                 toTime,
                                                                 super.nameMap);
    }

    /**
     * Returns OperationRoles records that were updated between the 
     * given time interval, [fromTime, toTime).
     *
     * @param	fromTime		the beginning interval (inclusive)
     * @param	toTime			the ending interval (exclusive)
     * @param	params			the additional parameters
     *
     * @return	The array of OperationRole JavaBeans, each containing
     *		information about a role.
     *
     * @throws	BroadOnException
     * @throws	IOException
     */
    public Bean[] getOperationRoles(Date fromTime, Date toTime, Map params)
	throws BroadOnException, IOException
    {
        OperationRoleFactory_cdsSync operationRoleFactory;

        operationRoleFactory = (OperationRoleFactory_cdsSync)factories[OPERATION_ROLE];
        fromTime = getFromTime(params, operationRoleFactory, fromTime);
        return operationRoleFactory.getRecordsWithinInterval(depotSchemaVersion,
                                                             fromTime,
                                                             toTime,
                                                             super.nameMap);
    }
}

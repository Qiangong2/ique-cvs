package com.broadon.cds;

/**
 * The <code>StatusCode</code> class defines the error codes returned
 * by the content download server.
 *
 * @version	$Revision: 1.2 $
 */
public class StatusCode
    extends com.broadon.status.StatusCode
{
    /**
     * Missing some or all of the arguments.
     */
    public static final String		CDS_BAD_ARGUMENT	= "201";

    /**
     * Invalid depot identifier.
     */
    public static final String		CDS_INVALID_DEPOT	= "202";

    /**
     * Incomplete request.
     */
    public static final String		CDS_INCOMPLETE		= "203";

    private static final String		CDS_CODE_BASE	= CDS_BAD_ARGUMENT;
    private static final String[]	CDS_MESSAGES	=
    {
	"Missing some or all of the arguments",				// 201
	"Invalid depot identifier",					// 202
	"The request is only partially completed",			// 203
    };

    static
    {
	/*
	 * Add the CDS status codes and their messages to the global map.
	 */
	int	code = Integer.parseInt(CDS_CODE_BASE);
	int	count = CDS_MESSAGES.length;

	for (int n = 0; n < count; n++, code++)
	{
	    addStatusCode(String.valueOf(code), CDS_MESSAGES[n]);
	}
    }
}

package com.broadon.cds.monitor;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.broadon.cds.CDSInfo;
import com.broadon.cds.ContentServlet;
import com.broadon.db.DBException;
import com.broadon.db.DataSourceScheduler;
import com.broadon.filter.authWrapper;
import com.broadon.servlet.ContextStatus;


/**
 * The <code>Monitor</code> servlet handles requests from a probe to
 * check that the cds server is up and functional.
 *
 * @version $Revision: 1.7 $
 */
public class Monitor extends ContentServlet
{
    static final String DB_TEST = "SELECT SYSDATE FROM DUAL";

    /** XML tag for the Depot ID (aka HR ID). */
    private static final String HR_ID_KEY = "hr_id";

    private String rootTag = null;
    private PrintWriter out = null;


    private static class MonitorInfo extends CDSInfo {};


    public void doPost(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
	ContextStatus status = (ContextStatus) ctx.getAttribute(MONITOR_STATUS_KEY);

	if (status.getOperationStatus() == status.OS_LB_NOTIFIED) {
	    res.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
	    status.setHealth(false);
	    return;
	}
	
        // Initialize a CDSInfo object for processing of this request
        //
        MonitorInfo monitorInfo = new MonitorInfo();
        processInit(req, res, monitorInfo);

        // Process request.
        //
        process(req, res, monitorInfo, status);
    } // doPost


    private void process(HttpServletRequest  req,
                         HttpServletResponse res,
                         MonitorInfo         monitorInfo,
			 ContextStatus	     status)
        throws ServletException, IOException
    {
        // Check if DepotID in the request matches that in the certificate.
        //
        // (Note that this check is in effect equivalent to the similar
        // check in ContentSyncServlet, but we here chose to use the
        // mechanism employed by the trans servers).
        //
        authWrapper	wreq = (authWrapper)req;
        if (wreq.hasClientAuth() &&
            !wreq.getEncodedDepotID().equals(req.getParameter(HR_ID_KEY))) {
            res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
	    status.setHealth(false);
            return;
        }

        // Prepare response header
        //
        res.setStatus(HttpServletResponse.SC_ACCEPTED);
        res.setContentType("text/plain; charset=" + super.CHARSET);

        try {
            // Test access to the cds database
            //
            dbAccessTest(wreq, monitorInfo);
	    status.setHealth(true);
        } catch (DBException e) {
	    res.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
	    status.setHealth(false);
        }
    } // process


    private void dbAccessTest(authWrapper wreq,
                              MonitorInfo monitorInfo) throws DBException
    {
        // Tests DB connectivity; let the caller deal with exceptions.
        //
        Connection        conn = null;
        PreparedStatement ps = null;
        try {
            conn = wreq.getConnection(DataSourceScheduler.DB_PRIORITY_FAST);
            ps = conn.prepareStatement(DB_TEST);

            ResultSet rs = ps.executeQuery();
            if (!rs.next()) {
                throw new DBException("Could not access cds DB system time");
            }
            // else System.err.println("CDS_DBTEST: rs.getString(1));


            ps.close();
            ps = null;
            conn.close(); 
            conn = null;
        } catch (SQLException e) {
            try {
                if (ps != null)
                    ps.close();
                if (conn != null)
                    conn.close();
            } catch (SQLException e2) {
                // Ignore this and report the original problem
            }
            throw new DBException(e.getMessage());
        }
    } // dbAccess

} // class Monitor

package com.broadon.cds.download;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.broadon.cds.ContentServlet;
import com.broadon.db.ContentDownloadLog;
import com.broadon.servlet.AuditLogAgent;
import com.broadon.util.FileCache;

/**
 * The <code>ContentDownloadServlet</code> class handles the requests from
 * the depots to download the content, identified by the content identifier.
 *
 * @version	$Revision: 1.19 $
 */
public class ContentDownloadServlet
    extends ContentServlet
{
    private final static String	CACHE_ROOT_PATH_NAME = "cacheRootPath";
    private final static String	CACHE_MAX_MBYTES = "cacheMaxMBytes";
    private final static String	CACHE_MAX_FILES	= "cacheMaxFiles";
    private final static String	CACHE_RESET	= "cacheReset";

    private static final String REQPARAM_ENCODE = "encode";
    private static final String REQPARAM_GETHEADER = "getheader";
    private static final String REQPARAM_OFFSET = "offset";
    private static final String REQPARAM_CHUNKSIZE = "chunksize";
    private static final String REQPARAM_CONTENT_ID = "content_id";
    private static final String REQPARAM_CHECKSUM = "id";

    private final static String	 DEF_CACHE_ROOTPATH = "/opt/broadon/data/svcdrv/cache";
    private final static int	 DEF_CACHE_MAX_FILES = 128;   // # files
    private final static int	 DEF_CACHE_MAX_MBYTES = 1024; // # Mbytes
    private final static boolean DEF_CACHE_RESET	= true;   // clear on start

    private String          cacheRootPath;  // cache root dir
    private long	        cacheMaxFiles;  // cache maximum number of files
    private long            cacheMaxMbytes; // cache storage size
    private boolean         cacheReset;	    // reset cache?startTime);
    private long            startTimeMsecs; // server start time
    private FileCache       fileCache;      // a cache of content objects
    

    private long getLongParam(HttpServletRequest  req,
                              HttpServletResponse res,
                              String              param,
                              long                def,
                              long                err)
    {
        String val = req.getParameter(param);
        long   l;
        if (val == null) {
            l = def;
        }
        else {
            try {
                l = Long.parseLong(val);
            }
            catch (NumberFormatException e) {
                l = err;
            }
        }
        return l;
    } // getLongParam


    /**
     * Constructs a ContentDownloadServlet instance.
     */
    public ContentDownloadServlet()
    {
        super();
        this.cacheRootPath = DEF_CACHE_ROOTPATH;
        this.cacheMaxFiles = DEF_CACHE_MAX_FILES;
        this.cacheMaxMbytes = DEF_CACHE_MAX_MBYTES;
        this.cacheReset = DEF_CACHE_RESET;
        startTimeMsecs = System.currentTimeMillis();
    }

    /**
     * Initializes this servlet.
     *
     * @param	servletConfig		the servlet configuration object
     */
    public synchronized void init(ServletConfig servletConfig)
	throws ServletException
    {
        super.init(servletConfig);

        ServletContext	context = servletConfig.getServletContext();
        String		    temp;

        temp = context.getInitParameter(CACHE_ROOT_PATH_NAME);
        if (temp != null)
            this.cacheRootPath = temp;
        temp = context.getInitParameter(CACHE_MAX_FILES);
        if (temp != null)
            this.cacheMaxFiles = Long.parseLong(temp);
        temp = context.getInitParameter(CACHE_MAX_MBYTES);
        if (temp != null)
            this.cacheMaxMbytes = Long.parseLong(temp);
        temp = context.getInitParameter(CACHE_RESET);
        if (temp != null)
            this.cacheReset = temp.equalsIgnoreCase("true");

        cacheRootPath += File.separator + "Content";
        System.out.println(CACHE_ROOT_PATH_NAME + "[" + cacheRootPath + "]");
        System.out.println(CACHE_MAX_FILES + "[" + cacheMaxFiles + "]");
        System.out.println(CACHE_MAX_MBYTES + "[" + cacheMaxMbytes + "]");
        System.out.println(CACHE_RESET + "[" + cacheReset + "]");

        // A filecache shared between all instances of this servlet.
        //
        try {
            fileCache = ContentDownload.getFileCache(cacheRootPath,
                                                     cacheMaxMbytes, 
                                                     cacheMaxFiles,
                                                     cacheReset);
        }
        catch (IOException e) {
            // Failed to initialize the file-cache
            //
            throw new ServletException("Failed to create the file-cache", e);
        }            

        startTimeMsecs = System.currentTimeMillis();
    }

    /**
     * Handles the Get request.
     *
     * @param	req			the HTTP request
     * @param	res			the HTTP response
     *
     * @throws	ServletException
     * @throws	IOException
     */
    public void doGet(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
        /*
         * Initialize.
         */
        ContentDownload	contentDownload = new ContentDownload();
        super.processInit(req, res, contentDownload);

        // Get depot id
        //
        long depotID = contentDownload.getDepotID();
        if (depotID < 0) {

            String depot = req.getParameter(super.DEPOT_PARAM_NAME);
            if (depot == null)
            {
                // We are being lenient, and accept download requests without
                // a depot ID, using depotID 0 (zero) as a dummy id.  This is
                // required to support the iQueAtHome application.
                //
                //    res.sendError(res.SC_BAD_REQUEST, "Invalid depot");
                //    return;
                //
                depot = "0";
            }
            //
            // Note that we do not bother verifying a depot ID with the DB.
            //
            depotID = Long.parseLong(depot);
            contentDownload.setDepotID(depotID);
        }

        // Get hold of request parameters
        //
        boolean getheader = req.getParameter(REQPARAM_GETHEADER) != null;
        boolean encode = req.getParameter(REQPARAM_ENCODE) != null;
        long    offset = getLongParam(req, res, REQPARAM_OFFSET, 0, -1);
        long    chunksz = getLongParam(req, res, REQPARAM_CHUNKSIZE, Long.MAX_VALUE, -1);
        long    contentID = getLongParam(req, res, REQPARAM_CONTENT_ID, -1, -1);
        String  checksum = req.getParameter(REQPARAM_CHECKSUM);
        
        // Check for errors (the response will already have been set)
        //
        if (offset < 0) {
            res.sendError(res.SC_BAD_REQUEST, 
                          "Invalid or missing " + REQPARAM_OFFSET);
            return;
        }
        else if (chunksz < 0) {
            res.sendError(res.SC_BAD_REQUEST, 
                          "Invalid or missing " + REQPARAM_CHUNKSIZE);
            return;
        }
        else if (contentID < 0) {
            res.sendError(res.SC_BAD_REQUEST, 
                          "Invalid or missing " + REQPARAM_CONTENT_ID);
            return;
        }

        // Prepare the audit log information.
        //
        ContentDownloadLog	contentDownloadLog = null;
        if (depotID != 0) {
            contentDownloadLog = new ContentDownloadLog();
            contentDownloadLog.setRequestDate(new Date());
            contentDownloadLog.setRequestLog(serverLog);
            contentDownloadLog.setRequestStatus(ContentDownloadLog.STATUS_OK);
            contentDownloadLog.setDepotID(new Long(depotID));
            contentDownloadLog.setContentID(new Long(contentID));
        }

        // Create the response and stream it back to the requesting client.
        //
        try {
            if (checksum == null || checksum.length() == 0) {
                contentDownload.getContent(fileCache,
                                           res,
                                           startTimeMsecs,
                                           contentID,
                                           offset,
                                           chunksz,
                                           encode,
                                           getheader);
            }
            else {
                contentDownload.getContent(fileCache,
                                           res,
                                           startTimeMsecs,
                                           checksum,
                                           offset,
                                           chunksz,
                                           encode,
                                           getheader);
            }
        }
        catch (Throwable t) {

            // Record the failure
            //
            t.printStackTrace();
            if (depotID != 0) {
                contentDownloadLog.setRequestStatus(
                    ContentDownloadLog.STATUS_FAILED);
                contentDownloadLog.setRequestLog(serverLog + 
                                                 " " + t.getMessage());
            }
            res.sendError(res.SC_NOT_FOUND, t.getMessage());
        }
        finally {
 
            // Make the request to create the audit log for a depot.
            //
            if (depotID != 0) {
                contentDownloadLog.setCompletionDate(new Date());
                AuditLogAgent.createLog(contentDownloadLog);
            }
        }
    } // doGet
}

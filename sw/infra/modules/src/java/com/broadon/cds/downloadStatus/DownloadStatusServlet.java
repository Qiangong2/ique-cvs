package com.broadon.cds.downloadStatus;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.broadon.cds.CDSInfo;
import com.broadon.cds.ContentServlet;
import com.broadon.filter.authWrapper;
import com.broadon.servlet.XMLRequestWrapper;
import com.broadon.util.VersionNumber;


/**
 * The <code>DownloadStatusServlet</code> class handles a notification
 * from the depot about the current download status and the last sync
 * time.
 *
 * @version	$Revision: 1.4 $
 */
public class DownloadStatusServlet
    extends ContentServlet
{
    private static final VersionNumber VERSION_1_2_1 = 
    VersionNumber.valueOf("1.2.1");
    
    private static final String TIMESTAMP = "BBXML.ConvertTimeStamp(?)";
    private static final String DB_UPDATE_DOWNLOAD_STATUS =
    "UPDATE client_download_progress " +
    "SET cdstimestamp=" + TIMESTAMP + ", " +
    "    total_to_download=?, " +
    "    total_downloaded=? " +
    "WHERE hr_id=?";
    private static final String DB_INSERT_DOWNLOAD_STATUS =
    "INSERT INTO client_download_progress " +
    "   (hr_id, cdstimestamp, total_to_download, total_downloaded) " +
    "   VALUES (?, " + TIMESTAMP + ", ?, ?)";

    private static final String	HR_PREFIX = "HR";
    private static final String	DEPOT_VERSION_TAG = "depot_version";
    private static final String	HR_ID_TAG = "hr_id";
    private static final String	LAST_SYNC_TIME_TAG = "last_sync_time";
    private static final String	TOTAL_TO_DOWNLOAD_TAG = "total_to_download";
    private static final String	NUM_DOWNLOADED_TAG = "num_downloaded";


    private static class DownloadStatus extends CDSInfo {
        private long lastSyncMsec = 0;
        private int  totalToDownload = 0;
        private int  numDownloaded = 0;

        private void setLastSyncMsec(long lastSyncMsec)
        {
            this.lastSyncMsec = lastSyncMsec;
        }
        private void setTotalToDownload(int totalToDownload)
        {
            this.totalToDownload = totalToDownload;
        }
        private void setNumDownloaded(int numDownloaded)
        {
            this.numDownloaded = numDownloaded;
        }

        public long getLastSyncMsec()
        {
            return lastSyncMsec;
        }
        public int getTotalToDownload()
        {
            return totalToDownload;
        }
        public int getNumDownloaded()
        {
            return numDownloaded;
        }
    }


    /**
     * Since a downloadStatus update must be an HTTP post operation,
     * we return an error code in the event of an HTTP get.
     *
     * @param	req			the HTTP request
     * @param	res			the HTTP response
     *
     * @throws	ServletException
     * @throws	IOException
     */
    public void doGet(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
	    /*
	     * No HTTP Get allowed.
	     */
        res.setHeader("Allow", "POST");
	    reportError(res,
                    res.SC_METHOD_NOT_ALLOWED,
                    "HTTP GET not allowed for downloadStatus servlet" +
                    " ... use POST instead");
	    return;
    }


    /**
     * Handles the Post request.
     *
     * @param	req			the HTTP request
     * @param	res			the HTTP response
     *
     * @throws	ServletException
     * @throws	IOException
     */
    public void doPost(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
        // Initialize.
        //
        authWrapper	      wreq = (authWrapper)req;
        XMLRequestWrapper xreq = new XMLRequestWrapper(req);

        DownloadStatus    downloadStatus = new DownloadStatus();
        processInit(req, res, downloadStatus);

        // Only accept authenticated requests with valid parameters
        //
        if (!wreq.hasClientAuth()) {
            reportError(res, 
                        res.SC_UNAUTHORIZED,
                        "Missing authentication");
            return;
        }
        else if (!setParameters(res,
                                downloadStatus, 
                                xreq.getParameter(DEPOT_VERSION_TAG),
                                xreq.getParameter(HR_ID_TAG),
                                xreq.getParameter(LAST_SYNC_TIME_TAG),
                                xreq.getParameter(TOTAL_TO_DOWNLOAD_TAG),
                                xreq.getParameter(NUM_DOWNLOADED_TAG))) {
            return;
        }

        try {
            // Update DB
            //
            updateDownloadStatus(wreq, downloadStatus);
        }
        catch (Throwable t) {
            reportError(res, res.SC_SERVICE_UNAVAILABLE, t.getMessage());
            t.printStackTrace();
        }
    }
    

    /**
     * Reports the error back to the requester.
     *
     * @param	response		the servlet response
     * @param	code		    the status code
     * @param	msg		        the error information
     */
    private void reportError(HttpServletResponse response, 
                             int                 code, 
                             String              msg)
        throws IOException
    {
        response.sendError(code, msg);
    }


    /**
     * Verifies that the status update has correct parameters, and saves
     * off the values in the DownloadStatus object.
     *
     * @param  res		       The response object (for reporting errors)
     * @param  downloadStatus  The download-status parameters
     * @param  depotVersionStr Version of sofware running on the depot
     * @param  depotIDString   Depot identifying number
     * @param  lastSyncTm      Last time the depot received a sync update
     * @param  totalToDownload Total number of content objects to be downloaded
     * @param  numDownloaded   Number of content objects already downloaded
     * @returns True when the parameters are valid; false otherwise
     */
    private boolean setParameters(HttpServletResponse res,
                                  DownloadStatus      downloadStatus,
                                  String              depotVersionStr,
                                  String              depotIDString,
                                  String              lastSyncTm,
                                  String              totalToDownload,
                                  String              numDownloaded)
        throws IOException
    {
        // Precondition: processInit() has been called and has initialized
        // the depotID with values from the certificate as interpreted by
        // the authWrapper filtering.
        //

        // Verify the depot version
        //
        VersionNumber depotVersion = VersionNumber.valueOf(depotVersionStr);
        if (depotVersion == null ||
            depotVersion.compareTo(VERSION_1_2_1) < 0) {

            reportError(res, 
                        res.SC_BAD_REQUEST,
                        "Unexpected "  + 
                        DEPOT_VERSION_TAG + "[" + depotVersionStr + "]");
            return false;
        }

        // Verify the depot identifier
        //
        if (depotIDString == null ||
            depotIDString.length() != 14 || 
            !depotIDString.startsWith(HR_PREFIX) ||
            !downloadStatus.getEncodedDepotID().equals(depotIDString)) {

            reportError(res, 
                        res.SC_BAD_REQUEST,
                        "Unexpected "  + 
                        HR_ID_TAG + "[" + depotIDString + "]");
            return false;
        }

        // Set and verify the other parameters in the DownloadStatus object.
        // Convert the sync time in UTC to millisecs.
        //
        try {
            downloadStatus.setLastSyncMsec(Long.parseLong(lastSyncTm)*1000);
            downloadStatus.setTotalToDownload(Integer.parseInt(totalToDownload));
            downloadStatus.setNumDownloaded(Integer.parseInt(numDownloaded));
        }
        catch (Throwable t) {
            reportError(res,
                        res.SC_BAD_REQUEST,
                        "Unexpected download status parameter: " +
                        LAST_SYNC_TIME_TAG +
                        "[" + lastSyncTm + "] " +
                        TOTAL_TO_DOWNLOAD_TAG +
                        "[" + totalToDownload + "]" +
                        NUM_DOWNLOADED_TAG +
                        "[" + numDownloaded + "] ... " +
                        t.getMessage());
            return false;
        }

        return true;
    } // setParameters


    /**
     * Performs the DB update to reflect the status update from the depot.
     *
     * @param	wreq		The authentication wrapper around the request
     * @param	ds			The download-status parameters
     */
    private void updateDownloadStatus(authWrapper    wreq,
                                      DownloadStatus ds)
        throws SQLException
    {
        Connection        conn = null;
        PreparedStatement ps = null;
        try {
            conn = wreq.getConnection();
            conn.setAutoCommit(false);

            int i = 1;
            ps = conn.prepareStatement(DB_UPDATE_DOWNLOAD_STATUS);
            ps.setLong(i++, ds.getLastSyncMsec());
            ps.setInt(i++, ds.getTotalToDownload());
            ps.setInt(i++, ds.getNumDownloaded());
            ps.setLong(i++, ds.getDepotID());
            final int num_updated = ps.executeUpdate();
            if (num_updated == 0) {
                ps.close();
                i = 1;
                ps = conn.prepareStatement(DB_INSERT_DOWNLOAD_STATUS);
                ps.setLong(i++, ds.getDepotID());
                ps.setLong(i++, ds.getLastSyncMsec());
                ps.setInt(i++, ds.getTotalToDownload());
                ps.setInt(i++, ds.getNumDownloaded());
                if (ps.executeUpdate() != 1) {
                    throw new SQLException(
                        "Failed to update/insert download-status for hr_id=" +
                        ds.getDepotID());
                }
            }
            conn.commit();
        }
        finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                try {conn.rollback();} catch (Throwable e2) {}
                conn.close();
            }
        }
    } // updateDownloadStatus

} //  class DownloadStatusServlet

package com.broadon.cds.sync;

import com.broadon.bean.Bean;

/**
 * The <c>DownloadSite</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *
 * It contains information about the URL prefix of the content download site.
 *
 * @version	$Revision: 1.3 $
 */
public class DownloadSite
    extends Bean
{
    private String		urlPrefix;

    /**
     * Constructs an empty DownloadSite instance.
     */
    public DownloadSite()
    {
	this(null);
    }

    /**
     * Constructs a DownloadSite instance.
     *
     * @param	urlPrefix		the URL prefix
     */
    public DownloadSite(String urlPrefix)
    {
	super();
	this.urlPrefix = urlPrefix;
    }

    /**
     * Getter for urlPrefix.
     *
     * @return	The URL prefix.
     */
    public String getURLPrefix()
    {
	return urlPrefix;
    }

    /**
     * Setter for urlPrefix.
     *
     * @param	urlPrefix		the URL prefix
     */
    public void setURLPrefix(String urlPrefix)
    {
	this.urlPrefix = urlPrefix;
    }

    /**
     * Returns whether the download sites are the same.
     *
     * @param	object			either a DownloadSite object
     *					or a urlPrefix String object
     *
     * @return	true => the download sites are the same; false => otherwise
     */
    public boolean equals(Object object)
    {
	if (urlPrefix == null || object == null)
	    return false;

	try
	{
	    return urlPrefix.equals(((DownloadSite)object).urlPrefix);
	}
	catch (ClassCastException ce)
	{
	    try
	    {
		return urlPrefix.equals((String)object);
	    }
	    catch (ClassCastException cce)
	    {
		return false;
	    }
	}
    }
}

package com.broadon.nus;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class Messages {
    private static final String resourceName = "com.broadon.nus.resources.messages".intern();

    /* Get message from a resource bundle */
    public static String getMessage(ResourceBundle bundle, String key)
    {
        String msg = null;
        if (bundle != null) {
            msg = bundle.getString(key);
        }
        if (msg == null) {
            throw new MissingResourceException("Cannot find resource key \"" + key +
                                               "\" in base name " + resourceName, resourceName, key);
        }
        return msg;        
    }
    
    /* Use specified locale for messages */
    public static String getMessage(Locale locale, String key)        
    {
        ResourceBundle bundle = getResourceBundle(locale);
        return getMessage(bundle, key);
    }
    
    public static String getMessage(Locale locale, String key, String[] args)
    {
        String msg = getMessage(locale, key);
        return MessageFormat.format(msg, args);
    }
    
    /* Use default locale for messages */
    public static String getMessage(String key)
    {
        return getMessage(Locale.getDefault(), key);
    }
    
    public static String getMessage(String key, String[] args)
    {
        return getMessage(Locale.getDefault(), key, args);
    }

    // Get Locale given a string representation of the locale
    // and country code
    public static Locale getLocale(String localeString, String country)
    {
        String language = localeString;
        String variant = "";
        if (language == null) { 
            language = "";
        } else {
            int i = language.indexOf('_');
            if (i >= 0) {
                country = language.substring(i+1);
                language = language.substring(0, i);
                i = country.indexOf('_');
                if (i >= 0) {
                    variant = country.substring(i+1);
                    country = country.substring(0, i);
                }
            }
        }
        if (country == null) {
            country = "";
        }
        return new Locale(language, country, variant);
    }
    
    public static ResourceBundle getResourceBundle(Locale locale)
    {
        return ResourceBundle.getBundle(resourceName, locale); 
    }
    
}

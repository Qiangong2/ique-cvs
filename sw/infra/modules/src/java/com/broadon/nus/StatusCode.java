package com.broadon.nus;

/**
 * Response code specific to nus (use error codes in 600-699 range)
 */
public class StatusCode extends com.broadon.status.StatusCode
{
    /** Bad device type */
    public static final String NUS_BAD_DEVICE_TYPE = "1300";
    static final String NUS_BAD_DEVICE_TYPE_MSG = "Invalid device type";

    /** Bad version */
    public static final String NUS_BAD_VERSION = "1301";
    static final String NUS_BAD_VERSION_MSG = "Invalid version";
    
    /** Bad country ID */
    public static final String NUS_BAD_COUNTRY_CODE = "1302";
    static final String NUS_BAD_COUNTRY_CODE_MSG = "Invalid country code";

    /** ECS Cache error */
    public static final String NUS_CACHE_ERROR = "1303";
    static final String NUS_CACHE_ERROR_MSG = "ECS cache error";
    
    /** Internal Error. */
    public static final String NUS_INTERNAL_ERROR = "1304";
    static final String NUS_INTERNAL_ERROR_MSG = "ECS internal error";
                
    static {
        addStatusCode(NUS_BAD_DEVICE_TYPE, NUS_BAD_DEVICE_TYPE_MSG);
        addStatusCode(NUS_BAD_VERSION, NUS_BAD_VERSION_MSG);
        addStatusCode(NUS_BAD_COUNTRY_CODE, NUS_BAD_COUNTRY_CODE_MSG);
        addStatusCode(NUS_CACHE_ERROR, NUS_CACHE_ERROR_MSG);
        addStatusCode(NUS_INTERNAL_ERROR, NUS_INTERNAL_ERROR_MSG);
    }

}

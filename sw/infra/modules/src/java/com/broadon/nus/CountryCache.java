package com.broadon.nus;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.broadon.db.DataSourceScheduler;
import com.broadon.util.BackingStore;
import com.broadon.util.BackingStoreException;
import com.broadon.util.CacheLFU;

/**
 * The <code>CountryCache</code> class provides a cache
 * for information about a country using the country code as a key
 *
 * @version $Revision: 1.1 $
 */
public class CountryCache 
    extends CacheLFU implements BackingStore 
{
    private NetUpdateServiceImpl service;

    protected static String SELECT_COUNTRY =
        "SELECT a.country_id, a.region, b.language_code " +
        "  FROM countries a, country_locales b WHERE a.country_id = b.country_id " + 
        "  AND a.default_locale = b.locale AND country = ?";
    
    public CountryCache(NetUpdateServiceImpl service) {
        super(service.getContentCacheTimeoutMsecs());
        this.service = service;
    }

    public Object retrieve(Object key) throws BackingStoreException
    {
        String countryCode = (String) key; 
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection conn = null;
        try {
            conn = service.getConnection(service.getCasDataSource(), DataSourceScheduler.DB_PRIORITY_FAST);
            ps = conn.prepareStatement(SELECT_COUNTRY);
            ps.setString(1, countryCode);
            rs = ps.executeQuery();
            if (rs.next()) {
                int countryId = rs.getInt(1);
                String region = rs.getString(2);
                String language = rs.getString(3);
                return new Country(region, countryId, countryCode, language);
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw BackingStoreException.rewrap(e);            
        } finally {
            if (rs != null) {
                try { rs.close(); } catch (SQLException e) {}
            }
            if (ps != null) {
                try { ps.close(); } catch (SQLException e) {}
            }
            if (conn != null) {
                try { conn.close(); } catch (SQLException e) {}
            }
        }
    }
    
}

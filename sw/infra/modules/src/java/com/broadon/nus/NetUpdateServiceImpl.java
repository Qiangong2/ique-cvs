package com.broadon.nus;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.sql.DataSource;
import javax.xml.rpc.ServiceException;
import javax.xml.rpc.server.ServiceLifecycle;
import javax.xml.rpc.server.ServletEndpointContext;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;
import org.apache.axis.types.URI;
import org.apache.axis.types.URI.MalformedURIException;

import com.broadon.db.OracleDataSource.OracleDataSourceProxy;
import com.broadon.servlet.ServletConstants;


/**
 * NetUpdateServiceImpl provides the implementation for
 * the NetUpdate Services.
 * @author angelc
 * * @version $Revision: 1.2 $
 */
public class NetUpdateServiceImpl implements ServiceLifecycle, ServletConstants
{   
    /** Servlet Context */
    protected ServletContext servletContext;
    
    /** DataSource for IAS to connect to the DB */
    protected DataSource iasDataSource;
    
    /** IAS uses main data source */
    private static final String IAS_DATA_SOURCE_KEY = DATA_SOURCE_KEY;
    
    /** DataSource for CAS to connect to the DB */
    protected DataSource casDataSource;
    
    /** CAS uses alt data source 1 */
    private static final String CAS_DATA_SOURCE_KEY = DATA_SOURCE_KEY + "_1";
    
    /** Content Prefix URI */
    protected URI contentPrefixUri;
    protected URI uncachedContentPrefixUri;

    /** Cache timeout in milliseconds */
    protected long contentCacheTimeoutMsecs;

    // Uses apache commons logger (follows axis logging)
    protected static Log log = 
        LogFactory.getLog(NetUpdateServiceImpl.class.getName());
    
    private static final String DEF_CONTENT_CACHE_TIMEOUT_SECS = "300";
    private static final String CFG_CONTENT_CACHE_TIMEOUT_SECS = "CONTENT_CACHE_TIMEOUT_SECS";

    private static final String DEF_CONTENT_PREFIX_URL = "http://ccs:16983/ccs/download";
    private static final String CFG_CONTENT_PREFIX_URL = "CONTENT_PREFIX_URL";
    private static final String CFG_UNCACHED_CONTENT_PREFIX_URL = "UNCACHED_CONTENT_PREFIX_URL";

    
    CountryCache countryCache = null;
    SystemTitleCache systemTitleCache = null;
    
    public NetUpdateServiceImpl()
    {
    	// Force initialization of StatusCode class
    	try {
    	    Class.forName("com.broadon.nus.StatusCode");
    	} catch  (ClassNotFoundException e) {
    	    log.error("Cannot initialize NUS StatusCode");
    	}
    }
    
   /**
     * Called by the JAX RPC runtime to initialize
     * the service endpoint.  Implements ServiceLifecycle.
     */
    public void init(Object context) throws ServiceException  
    { 
        log.debug("NetUpdate Service Init");

        ServletEndpointContext soapContext = (ServletEndpointContext) context;                
        servletContext = soapContext.getServletContext();
        
        // Get server properties (requires ConfigListener)
        
        // Get data source (set by OracleDSListener)
        iasDataSource = (DataSource)
            servletContext.getAttribute(IAS_DATA_SOURCE_KEY);
        if (iasDataSource == null) {
            log.error("IAS data source not found");
        }
        
        // Get data source (set by OracleDSListener)
        casDataSource = (DataSource)
            servletContext.getAttribute(CAS_DATA_SOURCE_KEY);
        if (casDataSource == null) {
            log.error("CAS data source not found ... using same data source as IAS");
            casDataSource = iasDataSource;
        }
        
        // Property file defined in the conf/BBserver.properties.tmpl file.
        // Add properties there as required for the ECS server.
        //
        Properties cfg = (Properties) servletContext.getAttribute(PROPERTY_KEY);

        // Get cache timeout
        contentCacheTimeoutMsecs =
            1000*Long.parseLong(getCfg(cfg, CFG_CONTENT_CACHE_TIMEOUT_SECS,
                                       DEF_CONTENT_CACHE_TIMEOUT_SECS));
        if (contentCacheTimeoutMsecs < 0) {
            log.warn(CFG_CONTENT_CACHE_TIMEOUT_SECS +
                         " is negative, using zero (no caching) instead");
            contentCacheTimeoutMsecs = 0;
        }
        
        countryCache = new CountryCache(this);
        systemTitleCache = new SystemTitleCache(this);
        
        // Get Content Prefix URL
        contentPrefixUri = getURICfg(cfg, CFG_CONTENT_PREFIX_URL, DEF_CONTENT_PREFIX_URL);
        uncachedContentPrefixUri = getURICfg(cfg, CFG_UNCACHED_CONTENT_PREFIX_URL, null);
        if (uncachedContentPrefixUri == null) {
            log.info(CFG_UNCACHED_CONTENT_PREFIX_URL + " not specified, using the " + 
                    CFG_CONTENT_PREFIX_URL + " of " + contentPrefixUri);
            uncachedContentPrefixUri = contentPrefixUri;
        }
    } 

    /**
     * Called by the JAX RPC runtime to destroy resources
     * used by the service endpoint at the end of its lifecycle.
     * Implements ServiceLifecycle.
     */
    public void destroy() 
    {
        log.debug("NetUpdate Service Destroy");
    }

    // Helper functions for getting the configuration
    
    private URI getURICfg(Properties cfg, String key, String defValue)
        throws ServiceException
    {
        String uriStr = getCfg(cfg, key, defValue);
        if (uriStr != null) {
            uriStr = uriStr.trim();
            if (uriStr.length() == 0) {
                uriStr = null;
            }
        }
        try {
            URI uri = (uriStr != null)? new URI(uriStr): null;
            return uri;
        }
        catch (MalformedURIException e) {
            throw new ServiceException("Malformed " + key+ ": " + uriStr, e);
        }
    }
    
    private URL getURLCfg(Properties cfg, String key, String defValue)
        throws ServiceException
    {
        String urlStr = getCfg(cfg, key, defValue);
        if (urlStr != null) {
            urlStr = urlStr.trim();
            if (urlStr.length() == 0) {
                urlStr = null;
            }
        }
        try {
            URL url = (urlStr != null)? new URL(urlStr): null;
            return url;
        }
        catch (MalformedURLException e) {
            throw new ServiceException("Malformed " + key+ ": " + urlStr, e);
        }
    }

    private String getCfg(Properties cfg, String key, String defValue)
    {
        String v = (cfg != null)? cfg.getProperty(key): null;
        if (v == null) {
            v = defValue;
            log.warn("BBserver.properties missing " + key + " value" +
                     " ... using default of " + defValue);
        } else {
            log.info("Setting " + key + " to " + v);
        }
        return v;
    }

    /* Accessor functions */
    public String getName()
    {
        return "NUS";
    }
    
    public long getContentCacheTimeoutMsecs()
    {
    	return contentCacheTimeoutMsecs;
    }

    public DataSource getIasDataSource()
    {
        return iasDataSource;
    }
    
    public DataSource getCasDataSource()
    {
        return casDataSource;
    }
    
    public Connection getConnection(DataSource dataSource, int priority) throws SQLException
    {
        Connection conn = dataSource instanceof OracleDataSourceProxy ?
                ((OracleDataSourceProxy) dataSource).getConnection(priority) :
                    dataSource.getConnection();
        return conn;
    }
    
    public URI getContentPrefixUrl()
    {
    	return contentPrefixUri;
    }

    public URI getUncachedContentPrefixUrl()
    {
        return uncachedContentPrefixUri;
    }

    public SystemTitleCache getSystemTitleCache()
    {
        return systemTitleCache;
    }

    public CountryCache getCountryCache()
    {
        return countryCache;
    }
}


package com.broadon.nus;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.broadon.db.DataSourceScheduler;
import com.broadon.util.BackingStore;
import com.broadon.util.BackingStoreException;
import com.broadon.util.CacheLFU;
import com.broadon.wsapi.nus.TitleVersionType;

public class SystemTitleCache extends CacheLFU implements BackingStore
{
    private static final String SELECT_TITLE_LIST =
        "SELECT BCCUTIL.DEC2HEX(TITLE_ID, 16), TITLE_VERSION, APPROX_SIZE FROM CONTENT_TITLE_CATALOG";

    private NetUpdateServiceImpl service;

    public SystemTitleCache(NetUpdateServiceImpl service) {
        super(50, service.getContentCacheTimeoutMsecs());
        this.service = service;
    }
    
    public TitleVersionType[] getSystemTitles(Connection conn, int deviceType)
        throws SQLException 
    {
        Statement stmt = null;
        List titleList = new ArrayList();

        // Get list of special titles
        try {
            long minSysTitleId = ((long) deviceType) << 32;
            long maxSysTitleId = minSysTitleId + 0xffffffffL;
            long minChannelTitleId = (((long) deviceType) << 16 + 0x0002) << 32;
            long maxChannelTitleId = minChannelTitleId + 0xffffffffL;

            StringBuffer sqlBuf = new StringBuffer(SELECT_TITLE_LIST);
            sqlBuf.append(" WHERE (TITLE_ID >= ");
            sqlBuf.append(minSysTitleId);
            sqlBuf.append(" AND TITLE_ID <= ");
            sqlBuf.append(maxSysTitleId);
            sqlBuf.append(") OR (TITLE_ID >= ");
            sqlBuf.append(minChannelTitleId);
            sqlBuf.append(" AND TITLE_ID <= ");
            sqlBuf.append(maxChannelTitleId);
            sqlBuf.append(") ORDER BY TITLE_ID");
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sqlBuf.toString());

            while (rs.next()) {
                TitleVersionType title = new TitleVersionType();
                int i = 1;
                title.setTitleId(rs.getString(i++));
                title.setVersion(rs.getInt(i++));
                title.setFsSize(new Long(rs.getLong(i++)));
                titleList.add(title);
            }
            rs.close();
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }

        TitleVersionType[] titles = null;
        if (titleList.size() > 0) {
            titles = new TitleVersionType[titleList.size()];
            titles = (TitleVersionType[]) titleList.toArray(titles);
        }

        return titles;
    }

    public Object retrieve(Object key) throws BackingStoreException
    {
        Key deviceTypeKey = (Key) key;
        //int countryId = deviceTypeKey.country.countryId; 
        int deviceType = deviceTypeKey.deviceType;

        Connection conn = null;
        try {
            conn = service.getConnection(service.getCasDataSource(), DataSourceScheduler.DB_PRIORITY_FAST);
            
            TitleVersionType[] titles = getSystemTitles(conn, deviceType);
            return titles;
        } catch (SQLException e) {
            throw BackingStoreException.rewrap(e);
        } finally {
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {}
        }
    }

    /**
     * Key for looking up an cached entry by device type and country id
     */
    public static class Key {
        public final Country country;
        public final int deviceType;
        
        public Key(Country country, int deviceType)
        {
            this.deviceType = deviceType;
            this.country = country;
        }

        public boolean equals(Object obj)
        {
            if (obj instanceof Key) {
                Key other = (Key) obj;
                return (this.deviceType == other.deviceType && 
                       ((this.country == null)? (other.country == null): 
                            (this.country.equals(other.country))));
            } else {
                return false;
            }
        }
        
        public int hashCode()
        {
            int hash = deviceType;
            if (country != null) {
                hash += country.hashCode();
            }
            return hash;
        }
        
        public String toString()
        {
            return "device " + Device.getDeviceTypeName(deviceType) + ", " + country; 
        }
    }
    
}

package com.broadon.nus;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.Locale;
import java.util.MissingResourceException;

import org.apache.axis.Constants;
import org.apache.axis.MessageContext;
import org.apache.commons.logging.Log;

import com.broadon.db.DBException;
import com.broadon.exception.BroadOnException;
import com.broadon.util.BackingStoreException;
import com.broadon.util.HexString;

import com.broadon.nus.*;
import com.broadon.wsapi.nus.*;

abstract public class NetUpdateFunction {
    public static final int STATUS_OK = 0;
    protected static final byte[] DEFAULT_CONTENT_MASK = new byte[32];
    
    protected static String VERSION = "1.0";
    
    protected static String FILTER_BY_PURCHASE_DATE =
        "PURCHASE_START_DATE <= SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) AND " + 
        "(PURCHASE_END_DATE IS NULL OR PURCHASE_END_DATE >= SYS_EXTRACT_UTC(CURRENT_TIMESTAMP))";
    
    protected static String SELECT_CURRENT_TIMESTAMP =
        "SELECT BCCUTIL.ConvertTimeStamp(SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)) FROM DUAL";
    
    static {
        for (int i = 0; i < DEFAULT_CONTENT_MASK.length; i++) {
            DEFAULT_CONTENT_MASK[i] = (byte) 0xFF;
        }
    }
    
    final protected NetUpdateServiceImpl service;
    final protected Log log;
    final protected AbstractRequestType request;
    final protected AbstractResponseType response;
    final protected Locale locale;
    final protected String name;
    private Device device;
    private Country country;
    
    public NetUpdateFunction(String name, NetUpdateServiceImpl service, AbstractRequestType request, AbstractResponseType response, Log log)
    {
        this.request = request;
        this.response = response;
        this.service = service;
        this.locale = Messages.getLocale(request.getLanguage(), request.getCountryCode());
        this.log = log;
        this.name = name;
    }

    public String getClientIp()
    {
        return MessageContext.getCurrentContext().getStrProp(Constants.MC_REMOTE_ADDR); 
    }
    
    public Device getDevice()
    {
        if (device == null) {
            device = getDevice(request, response);
        }
        return device;
    }
        
    public Country getCountry()
        throws BackingStoreException
    {
        if (country == null) {
            country = getCountry(service, request);
            if (country == null) {
                setErrorCode(StatusCode.NUS_BAD_COUNTRY_CODE, null);
            }
            /* TODO: Check region agrees? */
            // if (!country.region.equals(request.getRegionId())) {
            // setErrorCode(StatusCode.NUS_BAD_REGION, null);
            // }
        }
        return country;
    }
    
    protected void setErrorCode(String code, String message)
    {
        setErrorCode(locale, code, message, response);
    }
    
    abstract protected void processImpl()
        throws SQLException, BackingStoreException, BroadOnException;
    
    protected void process()
    {
        try {
            initResponse(locale, request, response);
            if (response.getErrorCode() == STATUS_OK) {
                processImpl();
            }
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION, e.getLocalizedMessage());
            log.error(name + " error", e);
        } catch (BackingStoreException e) {               
            if (e.getCause() instanceof SQLException) {
                setErrorCode(StatusCode.SC_SQL_EXCEPTION, e.getLocalizedMessage());
            } else {
                setErrorCode(StatusCode.NUS_CACHE_ERROR, e.getLocalizedMessage());
            }
            log.error(name + " error", e);
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.NUS_INTERNAL_ERROR, e.getLocalizedMessage());
            log.error(name + " error", e);
        }
    }

    /* Static helper functions */

    /**
     * Returns the current server time in milliseconds
     * @return
     */
    protected static long getServerTimestamp()
    {
        return System.currentTimeMillis();
    }

    /**
     * Returns the current DB time in milliseconds
     */
    protected static long getCurrentDbTimestamp(Connection conn)
        throws SQLException
    {
        Statement stmt = null;
        
        try {
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(SELECT_CURRENT_TIMESTAMP);
            rs.next();
            return rs.getLong(1);
        } finally {
            if (stmt != null) {
                stmt.close();
            } 
        }
    }
    
    /* Prepend a character to make the string the desired length */
    protected static String prependString(String str, char c, int totalLength)
    {
        if (str.length() < totalLength) {
            /* 0 pad str */
            StringBuffer strbuf = new StringBuffer(totalLength);
            for (int i = 0; i < totalLength - str.length(); i++) {
                strbuf.append(c);
            }
            strbuf.append(str);
            str = strbuf.toString();
        }
        return str;
    }
    
    /**
     * Formats a channel id as a hex string
     */
    protected static String formatChannelId(int channelId)
    {
        String str = Integer.toHexString(channelId).toUpperCase();
        str = prependString(str, '0', 8);
        return str;
    }
    
    /**
     * Parses a channel id from a hex string into a integer
     */
    protected static int parseChannelId(String channelId)
    {
        return Integer.parseInt(channelId, 16);
    }
    
    /**
     * Formats a title id as a string
     */
    protected static String formatTitleId(long titleId)
    {
        String str = Long.toHexString(titleId).toUpperCase();
        str = prependString(str, '0', 16);
        return str;
    }
    
    /**
     * Parses a title id from a hex string into a integer
     */
    protected static long parseTitleId(String titleId)
    {
        return Long.parseLong(titleId, 16);
    }
    
    /**
     * Formats content id as a hex string
     * @param contentId
     * @return
     */
    protected static String formatContentId(BigInteger contentId)
    {
        if (contentId != null) {
            String str = contentId.toString(16).toUpperCase();
            str = prependString(str, '0', 24);
            return str;
        } else {
            return null;
        }
    }
    
    protected static String formatContentId(BigDecimal contentId)
    {
        if (contentId != null) {
            return formatContentId(contentId.toBigInteger());
        } else {
            return null;
        }
    }
    
    /**
     * Takes mask represented as hex string from DB
     * and returns a content mask.  The content mask
     * always has 256 bits of 1s and the remaining 256
     * bits comes from the hex string in the DB.
     * @param dbMask
     * @return
     */
    protected static byte[] getContentMask(String dbMask)
    {
        if (dbMask == null) {
            return DEFAULT_CONTENT_MASK;
        }
        
        byte[] moreBytes = HexString.fromHexString(dbMask);
        byte[] contentMask = new byte[DEFAULT_CONTENT_MASK.length + moreBytes.length];
        System.arraycopy(DEFAULT_CONTENT_MASK, 0, contentMask, 0, DEFAULT_CONTENT_MASK.length);
        System.arraycopy(moreBytes, 0, contentMask, DEFAULT_CONTENT_MASK.length, moreBytes.length);
        return contentMask;
   }
    
    /**
     * Initializes fields in a AbstractResponseType given an AbstractRequestType
     * @param request
     * @param response
     */
    protected static void initResponse(Locale locale,
                                       AbstractRequestType request,
                                       AbstractResponseType response)
    {
        response.setDeviceId(request.getDeviceId());
        response.setTimeStamp(getServerTimestamp());
        response.setMessageId(request.getMessageId());
        response.setVersion(request.getVersion());
        if (!VERSION.equals(request.getVersion().trim())) {
            setErrorCode(locale, StatusCode.NUS_BAD_VERSION, null, response);
        }
    }

    
    /**
     * Sets the error code in AbstractResponseType
     * @param String code
     * @param String message
     * @param AbstractResponseType response
     */
    protected static void setErrorCode(Locale locale,
                                       String code, 
                                       String message,
                                       AbstractResponseType response)
    {
        response.setErrorCode(Integer.parseInt(code));
        String statusMessage = StatusCode.getMessage(code);
        /*try {
            statusMessage = Messages.getMessage(locale, statusMessage);
        } catch (MissingResourceException e) {
            
        }
        */
        if (message != null) {
            response.setErrorMessage(statusMessage + ": " + message);
        }
        else {
            response.setErrorMessage(statusMessage);
        }
    }  

    /**
     * Returns the device associated with the AbstractRequestType.
     */
    protected static Device getDevice(
    		AbstractRequestType request,
    		AbstractResponseType response)
    {
    	return new Device(
    		request.getDeviceId(),
                request.getVirtualDeviceType());
    }    
    
    /**
     * Returns the country associated with the AbstractRequestType.
     */
    protected static Country getCountry(
            NetUpdateServiceImpl service,
            AbstractRequestType request)
        throws BackingStoreException
    {
        return (Country) service.countryCache.get(request.getCountryCode());
    }    
}

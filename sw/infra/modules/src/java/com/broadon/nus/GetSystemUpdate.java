package com.broadon.nus;

import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.logging.Log;
import java.sql.*;
import java.util.Date;
import oracle.sql.BLOB;
import java.io.*;

import com.broadon.util.*;
import com.broadon.db.DataSourceScheduler;
import com.broadon.util.BackingStoreException;
import com.broadon.wsapi.nus.GetSystemUpdateRequestType;
import com.broadon.wsapi.nus.GetSystemUpdateResponseType;
import com.broadon.wsapi.nus.TitleVersionType;

/**
 * The <code>GetSystemUpdate</code> class implements the NUS GetSystemUpdate
 * function.
 * 
 * Returns SystemUpdatedata (such as the secure kernel version, CRL version,
 * list of contents to preload, etc) for a given country and device type.
 * 
 * @version $Revision: 1.2 $
 */
public class GetSystemUpdate extends NetUpdateFunction {

    protected static String SELECT_DEVICE = "SELECT AUDIT_DATE FROM IAS_DEVICES WHERE DEVICE_ID=?";

    protected static String INSERT_DEVICE = "INSERT INTO IAS_DEVICES (DEVICE_TYPE_ID, DEVICE_ID, AUDIT_DATE, AUDIT_DATA)"
            + " VALUES (?, ?, SYS_EXTRACT_UTC(CURRENT_TIMESTAMP), ?)";

    protected static String UPDATE_DEVICE = "UPDATE IAS_DEVICES SET AUDIT_DATE=SYS_EXTRACT_UTC(CURRENT_TIMESTAMP), AUDIT_DATA=? WHERE DEVICE_ID=?";

    // Uses apache commons logger (follows axis logging)
    protected static Log log = LogFactory.getLog(GetSystemUpdate.class
            .getName());

    public GetSystemUpdate(NetUpdateServiceImpl service,
            GetSystemUpdateRequestType request,
            GetSystemUpdateResponseType response) {
        super("GetSystemUpdate", service, request, response, log);
    }

    private BLOB writeTempBlob(Connection conn, BLOB auditDataBlob,
            String auditData) throws IOException, SQLException {
        Base64 base64 = new Base64();
        byte[] buf = base64.decode(auditData);
        auditDataBlob = BLOB.createTemporary(conn, true, BLOB.DURATION_SESSION);
        OutputStream outstream = auditDataBlob.setBinaryStream(1L);
        outstream.write(buf);
        outstream.close();
        return auditDataBlob;
    }

    public void processImpl() throws BackingStoreException {
        GetSystemUpdateRequestType getSystemUpdateRequest = (GetSystemUpdateRequestType) request;
        GetSystemUpdateResponseType getSystemUpdateResponse = (GetSystemUpdateResponseType) response;
        long deviceId = getDevice().getDeviceId();
        int deviceType = getDevice().getDeviceType();
        int uploadAuditData = 1;

        // insert audit data if nothing has been uploaded,
        // create device entry if necessary
        Connection conn = null;
        BLOB auditDataBlob = null;
        try {
            conn = service.getConnection(service.getIasDataSource(),
                    DataSourceScheduler.DB_PRIORITY_SLOW);

            PreparedStatement findDevice = conn.prepareStatement(SELECT_DEVICE);
            findDevice.setLong(1, deviceId);
            ResultSet rs = findDevice.executeQuery();

            if (rs.next()) {
                if (rs.getDate(1) != null) {
                    uploadAuditData = 0;
                } else {
                    PreparedStatement updateDevice = conn
                            .prepareStatement(UPDATE_DEVICE);
                    auditDataBlob = writeTempBlob(conn, auditDataBlob,
                            getSystemUpdateRequest.getAuditData());
                    updateDevice.setBlob(1, auditDataBlob);
                    updateDevice.setLong(2, deviceId);
                    updateDevice.execute();
                    conn.commit();
                    conn.close();
                    conn = null;
                    /* reset uploadAuditData after auditData is commited to DB */
                    uploadAuditData = 0;
                }
            } else {
                PreparedStatement updateDevice = conn
                        .prepareStatement(INSERT_DEVICE);
                auditDataBlob = writeTempBlob(conn, auditDataBlob,
                        getSystemUpdateRequest.getAuditData());
                updateDevice.setInt(1, deviceType);
                updateDevice.setLong(2, deviceId);
                updateDevice.setBlob(3, auditDataBlob);

                updateDevice.execute();
                conn.commit();
                conn.close();
                conn = null;
                /* reset uploadAuditData after auditData is commited to DB */
                uploadAuditData = 0;
            }
        } catch (IOException e) {
            log.warn("Invalid audit data - expecting base64 format - "
                    + e.getMessage());
        } catch (SQLException e) {
            log.error("Cannot update audit data", e);
        } finally {
            try {
                if (auditDataBlob != null)
                    auditDataBlob.freeTemporary();
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            } catch (SQLException e) {
                log.warn("Error closing connection - " + e.getMessage());
            }
        }

        // read cached response
        TitleVersionType[] systemTitles = (TitleVersionType[]) service
                .getSystemTitleCache().get(
                        new SystemTitleCache.Key(getCountry(), deviceType));
        getSystemUpdateResponse.setContentPrefixURL(service
                .getContentPrefixUrl());
        getSystemUpdateResponse.setUncachedContentPrefixURL(service
                .getUncachedContentPrefixUrl());
        getSystemUpdateResponse.setTitleVersion(systemTitles);
        getSystemUpdateResponse.setUploadAuditData(uploadAuditData);

    }

    /**
     * Returns the SystemUpdate information for the device
     * 
     * @param service
     * @param getSystemUpdateRequest
     * @return
     */
    public static GetSystemUpdateResponseType getSystemUpdate(
            NetUpdateServiceImpl service,
            GetSystemUpdateRequestType getSystemUpdateRequest) {
        GetSystemUpdateResponseType response = new GetSystemUpdateResponseType();
        GetSystemUpdate getSystemUpdate = new GetSystemUpdate(service,
                getSystemUpdateRequest, response);
        getSystemUpdate.process();
        return response;
    }
}

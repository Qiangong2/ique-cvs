package com.broadon.see;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.SecureRandom;
import java.security.Security;
import java.security.Signature;
import java.security.interfaces.RSAPublicKey;
import java.util.Properties;
import java.util.StringTokenizer;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMReader;

import com.ncipher.nfast.NFException;

public final class SoftwareSecurityModule extends SecurityModule 
{
    SecureRandom rnd;
    
    public byte[] getRandom(int size) {
        byte[] result = new byte[size];
        rnd.nextBytes(result);
        return result;
    }
    
    public byte[] sign(String signer, InputStream doc) throws GeneralSecurityException 
    {
        KeyPair key = (KeyPair) keyMap.get(signer);
        if (key == null)
            throw new GeneralSecurityException("Private key '" + signer + "' not found.");
        
        Signature sig = java.security.Signature.getInstance("SHA1withRSA");
        sig.initSign(key.getPrivate());
        byte[] buffer = new byte[4*1024];
        int size;
        try {
            while ((size = doc.read(buffer)) > 0)
                sig.update(buffer, 0, size);
        } catch (IOException e) {
            GeneralSecurityException se = 
                new GeneralSecurityException("Error reading the document for signing");
            se.setStackTrace(e.getStackTrace());
            throw se;
        }
        return sig.sign();
    }

    
    public boolean verify(String signer, RSAPublicKey pubKey) throws GeneralSecurityException, NFException
    {
        byte[] document = "dummy text".getBytes();
        byte[] sig = sign(signer, document);
        Signature signature = Signature.getInstance("SHA1withRSA");
        signature.initVerify(pubKey);
        signature.update(document);
        return signature.verify(sig);
    }

    public boolean verify(String signer, byte[] document, int offset, int length, byte[] signature) 
        throws GeneralSecurityException
    {
        KeyPair key = (KeyPair) keyMap.get(signer);
        if (key == null)
            throw new GeneralSecurityException("Public key not found");
        Signature sig = Signature.getInstance("SHA1withRSA");
        sig.initVerify(key.getPublic());
        sig.update(document, offset, length);
        return sig.verify(signature);
    }
    
    
    protected SoftwareSecurityModule(Properties prop) throws GeneralSecurityException 
    {
        super();
        rnd = new SecureRandom();
        
        Security.addProvider(new BouncyCastleProvider());
        
        String keyFiles = prop.getProperty(KEY_FILE_LIST);
        if (keyFiles == null)
            return;
        
        KeyFactory kf = KeyFactory.getInstance("RSA");
        
        StringTokenizer st = new StringTokenizer(keyFiles, ", \t");
        while (st.hasMoreTokens()) {
            String filename = st.nextToken();
            if (filename == null || filename.length() == 0)
                continue;
            File file = new File(filename);
            try {
                loadKeyFile(kf, file, getKeyName(file, KEYFILE_SUFFIX));
            } catch (IOException e) {
                GeneralSecurityException ge = new GeneralSecurityException(e.getMessage());
                ge.setStackTrace(e.getStackTrace());
                throw ge;
            }
        }
        cleanKeyMap();
    }
    
    /*
     * Derive the key ID from a given file name by stripping the suffix from it.
     */
    private String getKeyName(File file, String suffix) throws IOException 
    {
        String basename = file.getName();
        if (!basename.endsWith(suffix))
            throw new IOException("Cannot derive key ID from " + file.getPath());
        return basename.substring(0, basename.lastIndexOf(suffix));
    }
    
    private void loadKeyFile(KeyFactory kf, File keyFile, String keyName)
        throws IOException
    {
        PEMReader rdr = null;
        try {
            rdr = new PEMReader(new FileReader(keyFile));
            fillKeyMap(keyName, rdr.readObject());
        } finally {
            if (rdr != null)
                rdr.close();
        }
    }
}

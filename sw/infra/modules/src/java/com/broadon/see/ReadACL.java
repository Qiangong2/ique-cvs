package com.broadon.see;

import com.ncipher.km.nfkm.CardSet;
import com.ncipher.km.nfkm.ConsoleCallBack;
import com.ncipher.km.nfkm.Key;
import com.ncipher.km.nfkm.Module;
import com.ncipher.km.nfkm.SecurityWorld;
import com.ncipher.nfast.NFException;
import com.ncipher.nfast.connect.utils.EasyConnection;
import com.ncipher.nfast.marshall.M_ACL;
import com.ncipher.nfast.marshall.M_Cmd;
import com.ncipher.nfast.marshall.M_Cmd_Args_GetACL;
import com.ncipher.nfast.marshall.M_Cmd_Reply_GetACL;
import com.ncipher.nfast.marshall.M_Command;
import com.ncipher.nfast.marshall.M_KeyID;
import com.ncipher.nfast.marshall.M_Reply;
import com.ncipher.nfast.marshall.PrintoutContext;

public class ReadACL
{

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        try {
            EasyConnection conn = EasyConnection.connect();
            SecurityWorld sw = new SecurityWorld(conn.getConnection(), new ConsoleCallBack());
            Key[] keys = sw.getKeys(null, null);
            
            PrintoutContext out = new PrintoutContext(System.out);
            Module module = sw.getModule(1);
            CardSet cs = null;
            for (int i = 0; i < keys.length; i++) {
                if (args.length == 0 || keys[i].getName().equals(args[0])) {
                    Key k = keys[i];
                    if (k.isCardSetProtected()) {
                        if (! k.getCardSet().equals(cs)) {
                            cs = k.getCardSet();
                            cs.load(module.getSlot(0));
                        }
                        k.load(cs, module);
                    } else 
                        k.load(module);
                    System.out.println("\n\nKey name = " + k.getName() + ":\n");
                    dumpACL(conn, k.getKeyID(module), out);
                }
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private static void dumpACL(EasyConnection conn, M_KeyID id, PrintoutContext out) 
        throws NFException
    {
        M_Cmd_Args_GetACL aclArgs = new M_Cmd_Args_GetACL(0, id);
        M_Reply reply = conn.transactChecked(new M_Command(M_Cmd.GetACL, 0, aclArgs));
        M_ACL acl = ((M_Cmd_Reply_GetACL) reply.reply).acl;
        acl.printout(out);
    }

}

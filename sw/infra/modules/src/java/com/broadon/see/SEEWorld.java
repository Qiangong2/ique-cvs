/*
 * SEEWorld.java
 *
 * Java hostside SEE world class
 *
 * Copyright 2003 nCipher Corporation Limited.
 *
 * This file is example source code.  It is provided for your
 * information and assistance.  See the file LICENCE.TXT for details
 * and the terms and conditions of the licence which governs the use
 * of the source code. By using such source code you will be accepting
 * these terms and conditions.  If you do not wish to accept these
 * terms and conditions, DO NOT OPEN THE FILE OR USE THE SOURCE CODE.
 *
 * Note that there is NO WARRANTY.
 */

/* BEWARE: This class is included in jhsee.jar.
 * If you edit this code for your own use, we STRONGLY recommend you change
 * the package line.
 */

/*
 * Slightly modified for BroadOn-specific use
 */
package com.broadon.see;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import com.ncipher.km.marshall.M_KeyMgmtEntType;
import com.ncipher.km.nfkm.AdminKeys;
import com.ncipher.km.nfkm.SecurityWorld;
import com.ncipher.km.nfkm.Slot;
import com.ncipher.nfast.NFException;
import com.ncipher.nfast.connect.ClientException;
import com.ncipher.nfast.connect.CommandTooBig;
import com.ncipher.nfast.connect.ConnectionClosed;
import com.ncipher.nfast.connect.StatusNotOK;
import com.ncipher.nfast.connect.utils.EasyConnection;
import com.ncipher.nfast.marshall.M_ByteBlock;
import com.ncipher.nfast.marshall.M_CertType;
import com.ncipher.nfast.marshall.M_CertType_CertBody_SigningKey;
import com.ncipher.nfast.marshall.M_Certificate;
import com.ncipher.nfast.marshall.M_CertificateList;
import com.ncipher.nfast.marshall.M_Cmd;
import com.ncipher.nfast.marshall.M_Cmd_Args_CreateSEEWorld;
import com.ncipher.nfast.marshall.M_Cmd_Args_GetTicket;
import com.ncipher.nfast.marshall.M_Cmd_Args_SEEJob;
import com.ncipher.nfast.marshall.M_Cmd_Args_TraceSEEWorld;
import com.ncipher.nfast.marshall.M_Cmd_Reply_CreateSEEWorld;
import com.ncipher.nfast.marshall.M_Cmd_Reply_GetTicket;
import com.ncipher.nfast.marshall.M_Cmd_Reply_SEEJob;
import com.ncipher.nfast.marshall.M_Cmd_Reply_TraceSEEWorld;
import com.ncipher.nfast.marshall.M_Command;
import com.ncipher.nfast.marshall.M_KeyHash;
import com.ncipher.nfast.marshall.M_KeyID;
import com.ncipher.nfast.marshall.M_Reply;
import com.ncipher.nfast.marshall.M_Ticket;
import com.ncipher.nfast.marshall.M_TicketDestination;
import com.ncipher.nfast.marshall.M_TicketDestination_Details_NamedSEEWorld;
import com.ncipher.nfast.marshall.M_Word;
import com.ncipher.nfast.marshall.MarshallTypeError;

/**
 * Easy to use class for creating Java hostside SEE worlds.
 */
public class SEEWorld
{
    /** Connection used to send commands to the module */
    protected EasyConnection conn;

    /** The M_KeyID of the SEE world */
    protected M_KeyID worldid;

    /** The init status of the SEE world */
    protected M_Word initstatus;

    /** Dummy user data bytes to load if no user data is specified */
    protected static byte[] dummyBytes = { 
        (byte) 0x0C, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, 
        (byte) 0x00, (byte) 0x00, (byte) 0x01, (byte) 0x00, (byte) 0x00, (byte) 0x00, 
        (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x4C, (byte) 0xA2,
        (byte) 0xD7, (byte) 0x63, (byte) 0x1E, (byte) 0xEC, (byte) 0xA9, (byte) 0x5E, 
        (byte) 0xD2, (byte) 0xDE, (byte) 0xA6, (byte) 0xAC, (byte) 0x75, (byte) 0x88,
        (byte) 0xED, (byte) 0x32, (byte) 0x76, (byte) 0xD2, (byte) 0x41, (byte) 0x4E 
    };

    /**
     * Factory method to create a certificate chain suitable for SEE debugging.
     * 
     * @param sw
     *            SecurityWorld instance to use to load KDSEE
     * @param s
     *            slot to load admin card(s) from
     * @return cert chain
     */
    public static M_CertificateList seeDebugCertList(SecurityWorld sw, Slot s)
        throws NFException
    {
        AdminKeys akeys = sw.loadAdminKeys(s, new int[] { sw.NFKM_KDSEE });
        M_KeyID kdseeID = akeys.KeyIds[0];
        M_KeyHash kdseeHash = new M_KeyHash();
        kdseeHash.value = sw.getData().hkdsee.value;
        return new M_CertificateList(
                new M_Certificate[] {
                        new M_Certificate(kdseeHash, M_CertType.SigningKey,
                                          new M_CertType_CertBody_SigningKey(kdseeID)),
                                          sw.getDelegationCertificate(M_KeyMgmtEntType.CertDelgDSEEbNSO) });
    }

    /**
     * Creates a hostside SEE world.
     * 
     * @param conn
     *            The EasyConnection to use for the world, <em>This
     * parameter is mandatory.</em>
     * 
     * @param module
     *            The module the SEE machine has been loaded on
     * 
     * @param userData
     *            User data filename
     * 
     * @param debug
     *            This parameter specifies whether or not to set the EnableDebug
     *            flag in the world. If set, this <B>will</B> cause SEEWorld
     *            creation to fail with status AccessDenied unless your module
     *            has this feature enabled with the appropriate NSO permission,
     *            or you pass in an appropriate certificate chain in the "certs"
     *            field.
     * 
     * @param certs
     *            An M_CertificateList containing a certificate chain required
     *            to start your world, if needed.
     * @see #seeDebugCertList(SecurityWorld sw, Slot s)
     */
    public SEEWorld(EasyConnection conn, int module, String userData,
                    boolean debug, M_CertificateList certs) 
        throws MarshallTypeError, CommandTooBig, ClientException, ConnectionClosed, StatusNotOK,
               IOException
    {
        if (conn == null)
            throw new IllegalArgumentException("Connection must be non-null");
        this.conn = conn;

        M_Command cmd = new M_Command();
        cmd.cmd = M_Cmd.CreateSEEWorld;
        cmd.args = new M_Cmd_Args_CreateSEEWorld(
                debug ? M_Cmd_Args_CreateSEEWorld.flags_EnableDebug : 0,
                loadUserData(module, userData));
        if (certs != null) {
            cmd.certs = certs;
            cmd.flags |= cmd.flags_certs;
        }
        M_Reply rep = conn.transactChecked(cmd);
        worldid = ((M_Cmd_Reply_CreateSEEWorld) (rep.reply)).worldid;
        initstatus = ((M_Cmd_Reply_CreateSEEWorld) (rep.reply)).initstatus;
    }

    protected SEEWorld()
    {
    }

    /**
     * Sends an SEE job to the module.
     * 
     * @param seeargs
     *            SEE machine arguments
     * @return Result back from the SEE machine
     */
    public byte[] seeJob(byte[] seeargs) 
        throws MarshallTypeError, CommandTooBig, ClientException, ConnectionClosed, StatusNotOK
    {
        M_Cmd_Args_SEEJob args = new M_Cmd_Args_SEEJob(worldid, new M_ByteBlock(seeargs));
        M_Command cmd = new M_Command(M_Cmd.SEEJob, 0, args);
        M_Reply rep = conn.transactChecked(cmd);

        return ((M_Cmd_Reply_SEEJob) (rep.reply)).seereply.value;
    }

    /**
     * Reads a file converting it to a byte array suitable for sending to a SEE
     * machine using {@link #seeJob(byte[])}.
     * 
     * @param file
     *            The file to read
     * @return File contents as a byte array
     */
    private byte[] readFile(String dataFile) throws IOException
    {
        File file = new File(dataFile);
        byte[] result = new byte[(int) file.length()]; 
        FileInputStream in = new FileInputStream(file);
        try {
            in.read(result);
            return result;
        } finally {
            in.close();
        }
    }


    /**
     * SEE trace buffer bytes
     * 
     * @return Trace buffer bytes
     */
    public byte[] trace() throws MarshallTypeError, CommandTooBig,
            ClientException, ConnectionClosed, StatusNotOK
    {
        M_Cmd_Args_TraceSEEWorld args = new M_Cmd_Args_TraceSEEWorld(worldid);
        M_Reply rep = conn.transactChecked(new M_Command(M_Cmd.TraceSEEWorld, 0, args));
        return ((M_Cmd_Reply_TraceSEEWorld) rep.reply).data.value;
    }

    /**
     * Returns SEE trace buffer as a String
     * 
     * @return Trace buffer as a string
     */
    public String traceToString() throws MarshallTypeError, CommandTooBig,
            ClientException, ConnectionClosed, StatusNotOK
    {
        return new String(trace());
    }

    /**
     * @return the EasyConnection used by this class to send commands to the
     *         module
     */
    public EasyConnection getConnection()
    {
        return conn;
    }

    /**
     * @return the M_KeyID of the world
     */
    public M_KeyID getWorldID()
    {
        return worldid;
    }

    /**
     * @return the init status of the SEE world
     */
    public long getInitStatus()
    {
        return initstatus.value;
    }

    /**
     * Get a ticket for a key
     * 
     * @param k
     *            ID of the key to ticket
     * @return the ticket
     */
    public M_Ticket getTicket(M_KeyID k) throws ClientException, CommandTooBig,
            MarshallTypeError, ConnectionClosed, StatusNotOK
    {
        M_Cmd_Args_GetTicket args = 
            new M_Cmd_Args_GetTicket(0, k, M_TicketDestination.NamedSEEWorld,
                                     new M_TicketDestination_Details_NamedSEEWorld(worldid));
        M_Reply rep = conn.transactChecked(new M_Command(M_Cmd.GetTicket, 0, args));
        return ((M_Cmd_Reply_GetTicket) (rep.reply)).ticket;
    }

    /*
     * Loads user data using EasyConnection/loadBuffer, returns dummy user data
     * if no userData filename is specified
     */
    private M_KeyID loadUserData(int module, String userData) 
        throws IOException, MarshallTypeError, CommandTooBig, ClientException, ConnectionClosed, StatusNotOK
    {
        byte[] userDataBytes;

        if (userData == null)
            userDataBytes = dummyBytes;
        else
            userDataBytes = readFile(userData);

        return conn.loadBuffer(module, userDataBytes);
    }
}

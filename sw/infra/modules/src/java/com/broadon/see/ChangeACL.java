package com.broadon.see;

import com.ncipher.km.nfkm.AdminKeys;
import com.ncipher.km.nfkm.CardSet;
import com.ncipher.km.nfkm.ConsoleCallBack;
import com.ncipher.km.nfkm.Key;
import com.ncipher.km.nfkm.Module;
import com.ncipher.km.nfkm.SecurityWorld;
import com.ncipher.nfast.NFException;
import com.ncipher.nfast.connect.utils.EasyConnection;
import com.ncipher.nfast.marshall.M_ACL;
import com.ncipher.nfast.marshall.M_Act_Details_OpPermissions;
import com.ncipher.nfast.marshall.M_CertType;
import com.ncipher.nfast.marshall.M_CertType_CertBody_SigningKey;
import com.ncipher.nfast.marshall.M_Certificate;
import com.ncipher.nfast.marshall.M_CertificateList;
import com.ncipher.nfast.marshall.M_Cmd;
import com.ncipher.nfast.marshall.M_Cmd_Args_GetACL;
import com.ncipher.nfast.marshall.M_Cmd_Args_SetACL;
import com.ncipher.nfast.marshall.M_Cmd_Reply_GetACL;
import com.ncipher.nfast.marshall.M_Command;
import com.ncipher.nfast.marshall.M_KeyHash;
import com.ncipher.nfast.marshall.M_KeyHashAndMech;
import com.ncipher.nfast.marshall.M_Mech;
import com.ncipher.nfast.marshall.M_PermissionGroup;
import com.ncipher.nfast.marshall.M_Reply;
import com.ncipher.nfast.marshall.PrintoutContext;

public class ChangeACL
{
    private EasyConnection conn;
    private SecurityWorld sw;
    private Key key;

    public ChangeACL(String appName, String keyName) throws NFException
    {
        conn = EasyConnection.connect();
        sw = new SecurityWorld(conn.getConnection(), new ConsoleCallBack());
        Key[] keys = sw.getKeys(appName, null);
        for (int i = 0; i < keys.length; i++) {
            Key k = keys[i];
            if (keyName.equals(k.getName())) {
                key = k;
                return;
            }
        }
    }
    
    private AdminKeys loadAdminKeys() throws NFException
    {
        int[] keys = { sw.NFKM_KNSO, sw.NFKM_KRE};
        return sw.loadAdminKeys(sw.getModule(1).getSlot(0), keys);
    }
    
    public void setACL() throws NFException
    {
        
        AdminKeys adminKeys = loadAdminKeys();
        M_Certificate cert = new M_Certificate();
        cert.type = M_CertType.SigningKey;
        cert.body = new M_CertType_CertBody_SigningKey(adminKeys.KeyIds[0]);
        cert.keyhash = new M_KeyHash();
        cert.keyhash.value = conn.getKeyHash(adminKeys.KeyIds[0]);
        
        Module module = sw.getModule(1);
        CardSet cs = key.getCardSet();
        cs.load(module.getSlot(0));
        key.load(cs, module);
        PrintoutContext print = new PrintoutContext(System.out);
        
        Key newKey = new Key(key);
        newKey.setIdent(key.getIdent() + "_copy");
        newKey.setName("copy_of_" + key.getName());
        newKey.load(cs, module);
        
        //newKey.printout(print);
        
        M_Cmd_Args_GetACL getAcl = new M_Cmd_Args_GetACL();
        getAcl.key = newKey.getKeyID(module);
        M_Reply reply = conn.transactChecked(new M_Command(M_Cmd.GetACL, 0, getAcl));
        M_ACL acl = ((M_Cmd_Reply_GetACL) reply.reply).acl;
        acl.printout(print);
       
        if (acl.groups.length > 1) 
            return;
        
        M_PermissionGroup[] gps = new M_PermissionGroup[2];
        gps[0] = acl.groups[0];
        acl.groups = gps;
        M_PermissionGroup gp = new M_PermissionGroup();
        gps[1] = gp;
        gp.certmech = new M_KeyHashAndMech(cert.keyhash, M_Mech.Any);
        gp.flags = gp.flags_certmech;
        conn.addOpPermissions(gp, (M_Act_Details_OpPermissions.perms_ExpandACL |
                                   M_Act_Details_OpPermissions.perms_GetACL));
        
        M_Cmd_Args_SetACL setACL = new M_Cmd_Args_SetACL();
        setACL.key = newKey.getKeyID(module);
        setACL.newacl = acl;
        M_Command cmd = new M_Command(M_Cmd.SetACL, M_Command.flags_certs, setACL);
        M_CertificateList certList = new M_CertificateList(new M_Certificate[1]);
        certList.certs[0] = cert;
        cmd.certs = certList;
        conn.transactChecked(cmd);
        
        newKey.save();
       
        
        /*M_Cmd_Args_Export exArgs = new M_Cmd_Args_Export();
        exArgs.key = keyId;
        M_Command cmd = new M_Command(M_Cmd.Export, M_Command.flags_certs, exArgs);
        M_CertificateList certList = new M_CertificateList(new M_Certificate[1]);
        certList.certs[0] = cert;
        cmd.certs = certList;
        reply = conn.transactChecked(cmd);
        ((M_Cmd_Reply_Export) reply.reply).data.printout(print);*/
    }
    
    /**
     * @param args
     */
    public static void main(String[] args)
    {
        if (args.length < 2)
            usage();
        try {
            ChangeACL changeACL = new ChangeACL(args[0], args[1]);
            changeACL.setACL();
        } catch (NFException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private static void usage()
    {
        System.err.println("Usage: ChangeACL <appname> <keyname>");
        System.exit(1);
    }

}

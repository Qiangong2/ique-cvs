package com.broadon.see;

import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Signature;
import java.security.SignatureException;
import java.security.interfaces.RSAPublicKey;
import java.util.Properties;

import com.ncipher.jutils.Passphrase;
import com.ncipher.km.marshall.NFKM_SlotState;
import com.ncipher.km.nfkm.CardSet;
import com.ncipher.km.nfkm.CmdCallBack;
import com.ncipher.km.nfkm.Module;
import com.ncipher.km.nfkm.SecurityWorld;
import com.ncipher.km.nfkm.Slot;
import com.ncipher.nfast.NFException;
import com.ncipher.nfast.connect.CommandTooBig;
import com.ncipher.nfast.connect.NFConnection;
import com.ncipher.nfast.connect.ObjectNotAnOutstandingRequest;
import com.ncipher.nfast.connect.utils.EasyConnection;
import com.ncipher.nfast.marshall.M_Block128;
import com.ncipher.nfast.marshall.M_ChannelMode;
import com.ncipher.nfast.marshall.M_Cmd;
import com.ncipher.nfast.marshall.M_Cmd_Args_GenerateRandom;
import com.ncipher.nfast.marshall.M_Cmd_Args_GetKeyInfoEx;
import com.ncipher.nfast.marshall.M_Cmd_Args_Sign;
import com.ncipher.nfast.marshall.M_Cmd_Reply_GenerateRandom;
import com.ncipher.nfast.marshall.M_Cmd_Reply_GetKeyInfoEx;
import com.ncipher.nfast.marshall.M_Cmd_Reply_Sign;
import com.ncipher.nfast.marshall.M_Command;
import com.ncipher.nfast.marshall.M_Hash;
import com.ncipher.nfast.marshall.M_IV;
import com.ncipher.nfast.marshall.M_KeyID;
import com.ncipher.nfast.marshall.M_Mech;
import com.ncipher.nfast.marshall.M_Mech_Cipher_RSApPKCS1;
import com.ncipher.nfast.marshall.M_Mech_IV_Generic128;
import com.ncipher.nfast.marshall.M_PlainText;
import com.ncipher.nfast.marshall.M_PlainTextType;
import com.ncipher.nfast.marshall.M_PlainTextType_Data_Hash;
import com.ncipher.nfast.marshall.M_Reply;
import com.ncipher.nfast.marshall.M_Status;
import com.ncipher.nfast.marshall.M_Ticket;
import com.ncipher.nfast.marshall.MarshallContext;
import com.ncipher.nfast.marshall.MarshallTypeError;

public class HardwareSecurityModule extends SecurityModule
{
    // op code for the SEE engine
    static final byte OP_LOAD_KEY = 1;
    static final byte OP_SIGN_ETICKET = 2;
    static final byte OP_SIGN_TMD = 3;
    static final byte OP_SIG_TEST = 4;
    
    // SEE job header
    static final int SEE_HEADER_SIZE = 8;
    
    // SEE return status code
    static final int SEE_INVALID_KEYTYPE = 176;

    private NFConnection conn;
    private String cardId = null;
    private EasyConnection ezConn;
    private SEEWorld seeWorld;

    public byte[] getRandom(int size) throws GeneralSecurityException, NFException
    {
    
        M_Cmd_Args_GenerateRandom args = new M_Cmd_Args_GenerateRandom(size);
        M_Command cmd = new M_Command(M_Cmd.GenerateRandom, 0, args);
        byte[] result = getRandomUnchecked(cmd);
        if (result != null && result.length >= size)
            return result;
    
        // The HSM sometimes does not return enough random bytes.
    
        byte[] returnBuf = new byte[size];
        int count = 0;
    
        if (result != null) {
            System.arraycopy(result, 0, returnBuf, 0, result.length);
            count = result.length;
        }
    
        while (count < size) {
            args.lenbytes.value = size - count;
            result = getRandomUnchecked(cmd);
            if (result != null) {
                System.arraycopy(result, 0, returnBuf, count, result.length);
                count += result.length;
            }
        }
        return returnBuf;
    }


    /*
     * (non-Javadoc)
     * 
     * @see com.broadon.ets.Signature#sign(java.lang.String, InputStream)
     */
    public byte[] sign(String signer, InputStream doc) 
        throws GeneralSecurityException, NFException 
    {
        KeyInfo kinfo = (KeyInfo) keyMap.get(signer);
        if (kinfo == null)
            throw new InvalidKeyException("Signer " + signer + " not found");
        
        return sign(kinfo, doc);
    }


    private byte[] sign(KeyInfo kinfo, InputStream doc) 
        throws NoSuchAlgorithmException, GeneralSecurityException, NFException
    {
        // hash the document in software. This is much faster than doing it in
        // the HSM, which has a rather slow CPU.
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        byte[] buffer = new byte[4*1024];
        int size;
        try {
            while ((size = doc.read(buffer)) > 0)
                md.update(buffer, 0, size);
        } catch (IOException e) {
            GeneralSecurityException se = 
                new GeneralSecurityException("Error reading the document for signing");
            se.setStackTrace(e.getStackTrace());
            throw se;
        }
        M_Hash documentHash = new M_Hash(md.digest());
                
        M_Cmd_Reply_Sign reply = null;
        try {
            M_PlainText plain = new M_PlainText(M_PlainTextType.Hash,
                                                new M_PlainTextType_Data_Hash(documentHash));
                        
            M_Command cmd = new M_Command(M_Cmd.Sign, 
                                          0, 
                                          new M_Cmd_Args_Sign(0, kinfo.getKeyID(), M_Mech.Any, plain));
            reply = (M_Cmd_Reply_Sign) conn.wait(conn.submit(cmd), 10000).reply;
        } catch (Exception e) {
            consolidateException(e);
        } 
                
        if (reply == null)
            throw new NFException("Access timeout");
                
        M_Mech_Cipher_RSApPKCS1 rsa = (M_Mech_Cipher_RSApPKCS1) reply.sig.data;
        byte[] sig = rsa.m.value.toByteArray();
        int keyLength = kinfo.getKeyLength();
        if (sig.length != keyLength) {
            byte[] newSig = new byte[keyLength];
            if (sig.length < keyLength) {
                int offset = keyLength - sig.length;
                for (int i = 0; i < offset; ++i)
                    newSig[i] = 0;
                System.arraycopy(sig, 0, newSig, offset, sig.length);
            } else {
                System.arraycopy(sig, sig.length - keyLength, newSig, 0, keyLength);
            }
            return newSig;
        } else
            return sig;
    }


    private byte[] signWithSEE(byte op, String signer, byte[] doc) 
        throws NFException, InvalidKeyException
    {
        KeyInfo kinfo = (KeyInfo) keyMap.get(signer);
        if (kinfo == null || kinfo.getSeeHandle() < 0)
            throw new InvalidKeyException("Signer " + signer + " not found");
        
        byte[] jobBuffer = new byte[SEE_HEADER_SIZE + doc.length];
        jobBuffer[0] = op;
        jobBuffer[1] = kinfo.getSeeHandle();
        System.arraycopy(doc, 0, jobBuffer, SEE_HEADER_SIZE, doc.length);
        byte[] result = seeWorld.seeJob(jobBuffer);
        if (result != null) {
            if (result.length == doc.length)
                return result;
            else if (result.length >= 1 && result[0] == SEE_INVALID_KEYTYPE)
                throw new InvalidKeyException("Signature key size mismatch.");
        }
        
        return null;
    }

    /* (non-Javadoc)
     * @see com.broadon.see.SecurityModule#signTicket(java.lang.String, byte[], int, int, int)
     */
    public byte[] signTicket(String signer, byte[] ticket, int sigOffset, int docOffset,
                             int sigSize) 
        throws GeneralSecurityException, NFException
    {
        if (seeWorld == null)
            return super.signTicket(signer, ticket, sigOffset, docOffset, sigSize);
        
        byte[] result = signWithSEE(OP_SIGN_ETICKET, signer, ticket);
        if (result == null)
            throw new GeneralSecurityException("Invalid eTicket (probably bad title ID)");
        else 
            return result;
    }

    
    /* (non-Javadoc)
     * @see com.broadon.see.SecurityModule#signTMD(java.lang.String, byte[], int, int, int)
     */
    public byte[] signTMD(String signer, byte[] tmd, int sigOffset, int docOffset,
                          int sigSize) 
        throws GeneralSecurityException, NFException
    {
        if (seeWorld == null)
            return super.signTMD(signer, tmd, sigOffset, docOffset, sigSize);
        
        byte[] result = signWithSEE(OP_SIGN_TMD, signer, tmd);
        if (result == null)
            throw new GeneralSecurityException("Invalid TMD (probably bad title ID)");
        else
            return result;
    }


    public boolean verify(String signer, RSAPublicKey pubKey) throws NFException, GeneralSecurityException
    {
        if (seeWorld != null) {
            // This SEE machine function returns the signature of a predefined
            // text signed by the given key. The predefined text is also
            // appended. We use a predefined text because the SEE machine does
            // not allow signing of arbitrary document.
            
            KeyInfo kinfo = (KeyInfo) keyMap.get(signer);
            if (kinfo == null || kinfo.getSeeHandle() < 0)
                throw new InvalidKeyException("Signer " + signer + " not found");
            
            byte[] jobBuffer = new byte[SEE_HEADER_SIZE];
            jobBuffer[0] = OP_SIG_TEST;
            jobBuffer[1] = kinfo.getSeeHandle();
            byte[] result = seeWorld.seeJob(jobBuffer);
            int sigSize = kinfo.getKeyLength();
            if (result != null && result.length > sigSize) {
                byte[] sig = new byte[sigSize];
                byte[] doc = new byte[result.length - sigSize];
                System.arraycopy(result, 0, sig, 0, sigSize);
                System.arraycopy(result, sigSize, doc, 0, doc.length);
                return verify(pubKey, sig, doc);
            }
            return false;
        } else {
            byte[] doc = "Dummy plain text".getBytes();
            byte[] sig = sign(signer, doc);
            return verify(pubKey, sig, doc);
        }
    }


    private boolean verify(RSAPublicKey pubKey, byte[] sig, byte[] doc) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException
    {
        Signature signature = Signature.getInstance("SHA1withRSA");
        signature.initVerify(pubKey);
        signature.update(doc);
        return signature.verify(sig);
    }


    public boolean verify(String signer, byte[] document, int offset, int length, byte[] signature) 
        throws GeneralSecurityException 
    {
        throw new GeneralSecurityException("Verification not supported.");
    }   
        
    
    public byte[] encryptTitleKey(String keyName, long titleID, byte[] plainTextTitleKey)
        throws GeneralSecurityException, NFException
    {
        final int aesKeySize = 16;
        KeyInfo kinfo = (KeyInfo) keyMap.get(keyName);
        if (kinfo == null || kinfo.getKeyLength() != aesKeySize)
            throw new InvalidKeyException("Invalid key " + keyName + " for title key encryption.");
        
        byte[] ivBuf = new byte[aesKeySize];
        for (int i = 7; i >= 0; --i) {
            ivBuf[i] = (byte) (titleID & 0xff);
            titleID >>= 8;
        }
        
        M_IV iv = new M_IV();
        iv.mech = M_Mech.RijndaelmCBCpNONE;
        iv.iv = new M_Mech_IV_Generic128(new M_Block128(ivBuf));
        return ezConn.SymmetricCrypt(M_ChannelMode.Encrypt, kinfo.getKeyID(), 
                                     M_Mech.RijndaelmCBCpNONE, plainTextTitleKey, 
                                     null, iv, false, false);
    }
        
    protected HardwareSecurityModule(Properties prop) 
        throws NFException
    {
        super();
        
        boolean needCallBack = "true".equals(prop.getProperty(PROMPT_FOR_CARD_KEY));
        
        // load all the relevant keys
        SecurityWorld sw = new SecurityWorld();
        conn = sw.getConnection();
        ezConn = new EasyConnection(conn);
        String seeDataFile = prop.getProperty(INIT_DATA_FILE, DEFAULT_INIT_DATA_FILE);
        try {
            seeWorld = new SEEWorld(ezConn, 1, seeDataFile, false, null);
            if (seeWorld.getInitStatus() != M_Status.OK) {
                seeWorld = null;
            }
        } catch (Exception e1) {
            seeWorld = null;
        }
                
        com.ncipher.km.nfkm.Key[] k = sw.listKeys(null);
        Module module = sw.getModule(1);
        Slot slot = module.getSlot(0);
        CardSet card = null;
        for (int i = 0; i < k.length; ++i) {
            M_KeyID id = null;
            if (k[i].isModuleProtected()) {
                id = k[i].load(module);
            } else if (k[i].isCardSetProtected()) {
                CardSet cs = k[i].getCardSet();
                if (! cs.equals(card)) {
                    card = cs;
                    card.load(slot, needCallBack ? new LoadOperatorCardCallBack() : null);
                    cardId = card.getName() + '/' + slot.getData().card.name.s;
                }
                id = k[i].load(card, module);
            } else
                continue;
            try {
                fillKeyMap(k[i].getName(), new KeyInfo(id, getKeyLength(id), loadSeeKey(id)));
            } catch (InterruptedException e) {
                NFException nfe = new NFException(e.getMessage());
                nfe.setStackTrace(e.getStackTrace());
                throw nfe;      
            } 
        }
        cleanKeyMap();
    }

   
        
    private byte loadSeeKey(M_KeyID id) throws NFException
    {
        if (seeWorld == null)
            return -1;
        
        final byte[] filler = new byte[SEE_HEADER_SIZE - 1];
        M_Ticket ticket = seeWorld.getTicket(id);
        MarshallContext ctx = new MarshallContext();
        ctx.addByte(OP_LOAD_KEY);
        ctx.addBytes(filler);
        ticket.marshall(ctx);
        byte[] result = seeWorld.seeJob(ctx.getBytes());

        if (result != null && result.length > 0 || result[0] >= 0)
            return result[0];
        else return -1;
    }


    private int getKeyLength(M_KeyID id) throws NFException, InterruptedException
    {
        M_Cmd_Args_GetKeyInfoEx keyInfoArgs = new M_Cmd_Args_GetKeyInfoEx(0, id);
        M_Command keyInfoCmd = new M_Command(M_Cmd.GetKeyInfoEx, 0, keyInfoArgs);

        M_Cmd_Reply_GetKeyInfoEx reply = (M_Cmd_Reply_GetKeyInfoEx)
            conn.wait(conn.submit(keyInfoCmd), 10000).reply; 
                        
        if (reply == null)
            throw new NFException("Access timeout");
        return (int) ((reply.length.value + 7) / 8);
    }
        
        
    private byte[] getRandomUnchecked(M_Command cmd) throws NFException, GeneralSecurityException 
    {
        try {
            M_Reply r;
            r = conn.wait(conn.submit(cmd), 10000);
            if (r == null)
                throw new NFException("Access timeout");

            M_Cmd_Reply_GenerateRandom reply = (M_Cmd_Reply_GenerateRandom) r.reply;
            return reply.data.value;
        } catch (Exception e) {
            consolidateException(e);
            return null;
        } 
    }
        
        
    private void consolidateException(Exception e) throws GeneralSecurityException, NFException
    {
        if ((e instanceof MarshallTypeError) ||
            (e instanceof CommandTooBig) ||
            (e instanceof ObjectNotAnOutstandingRequest)) {
            // these are non-fatal error.  Everything else 
            GeneralSecurityException ge = new GeneralSecurityException(e.getMessage());
            ge.setStackTrace(e.getStackTrace());
            throw ge;
        } else if (e instanceof NFException)
            throw (NFException)e;
        else {
            NFException nfe = new NFException(e.getMessage());
            nfe.setStackTrace(e.getStackTrace());
            throw nfe;
        }
    }
        
        
    private class KeyInfo {
        M_KeyID id;
        int keyLength;
        byte seeKeyHandle;              // handle to the key loaded to the SEE machine
        
        protected KeyInfo(M_KeyID id, int length, byte seeKeyHandle) {
            this.id = id;
            keyLength = length;
            this.seeKeyHandle = seeKeyHandle;
        }
        protected M_KeyID getKeyID() {
            return id;
        }
        protected int getKeyLength() {
            return keyLength;
        }
        protected byte getSeeHandle() {
            return seeKeyHandle;
        }
    }
    
    class LoadOperatorCardCallBack implements CmdCallBack
    {
        int errCount = 0;

        public boolean errorCallBack(String reason, String code, String message)
        {
            if (M_Status.toString(M_Status.DecryptFailed).equals(code)) {
                System.out.println("Invalid password.");
                return ++errCount > 3;
            } else {
                System.out.println(message);
                return true;
            }
        }

        public Slot reqCardCallBack(String ReqCardType, String ReqCardAction, int CardsDone, Slot slot) 
            throws NFException
        {
            slot.update();
            long state = slot.getSlotState();
            
            while (state != NFKM_SlotState.Operator) {
                switch ((int)state) {
                case NFKM_SlotState.Empty:
                    System.out.println("Please insert authorization card.");
                    break;
                default:
                    System.out.println("Not a valid authroization card.");
                    break;
                }
                
                int timeOutCount = 0;
               
                while (true) {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {}
                    slot.update();
                    long newState = slot.getSlotState();
                    if (newState != state || newState == NFKM_SlotState.Operator) {
                        state = newState;
                        if (state != NFKM_SlotState.Empty)
                            System.out.println("Reading card ...");
                        break;
                    }
                    if (++timeOutCount > 30) {
                        System.out.println("Time out ... aborting.");
                        return null;
                    }
                }
            } 

            return slot;
        }

        public String reqPPCallBack(String ReqPPAction) throws NFException
        {
            if (ReqPPAction_Load.equals(ReqPPAction)) {
                try {
                    return Passphrase.readPassphrase("Please enter password for operator smart card: ");
                } catch (IOException e) {}
            } 
            return null;
        }
        
    }

    public String getAuthorizer()
    {
        return (cardId != null) ? cardId : super.getAuthorizer();
    }

    public boolean seeWorldActivate()
    {
        return seeWorld != null;
    }

    public static void main(String[] args)
    {
        try {
            Properties prop = new Properties();
            prop.put(PROMPT_FOR_CARD_KEY, "true");
            prop.put(INIT_DATA_FILE, "conf/bcc_see.data.sar");
            HardwareSecurityModule hsm = new HardwareSecurityModule(prop);
            System.out.println("See world is " + (hsm.seeWorldActivate() ? "" : "not ") + "activated." );
            byte[] doc = hsm.getRandom(556);
            byte[] tmd = hsm.signTMD("rsa2k", doc, 0, 0, 0);
            System.out.println("tmd length = " + tmd.length);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

package com.broadon.see;

public interface Keywords {
    public static final String KEY_FILE_LIST = "KEY_LIST";
    
    public static final String KEYFILE_SUFFIX = "-key.pem";

    public static final String USE_SSM_KEY = "USE_SOFTWARE_SECURITY_MODULE";
    
    // specify if we should interactively prompt for smart card in HSM
    public static final String PROMPT_FOR_CARD_KEY = "PROMPT_FOR_CARD";
    
    public static final String INIT_DATA_FILE = "INIT_DATA_FILE";
    public static final String DEFAULT_INIT_DATA_FILE = "/opt/nfast/kmdata/see/signer.data.sar";

}

package com.broadon.see;
import com.ncipher.km.nfkm.Key;
import com.ncipher.km.nfkm.SecurityWorld;
import com.ncipher.nfast.NFException;
import com.ncipher.nfast.connect.utils.EasyConnection;

public class RenameKey
{

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        if (args.length < 2)
            usage();
        
        try {
            rename(args[0], args[1]);
        } catch (NFException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private static void rename(String oldName, String newName) throws NFException
    {
        EasyConnection conn = EasyConnection.connect();
        SecurityWorld sw = new SecurityWorld(conn.getConnection(), null);
        Key[] keys = sw.getKeys(null, null);
        for (int i = 0; i < keys.length; i++) {
            Key k = keys[i];
            if (oldName.equals(k.getName())) {
                k.setName(newName);
                k.save();
            }
        }
    }

    private static void usage()
    {
        System.err.println("Usage: RenameKey <old name> <new name>");
        System.exit(1);
    }

}

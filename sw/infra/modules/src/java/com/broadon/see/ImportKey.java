package com.broadon.see;
import java.io.File;
import java.io.FileInputStream;

import com.ncipher.km.nfkm.CardSet;
import com.ncipher.km.nfkm.ConsoleCallBack;
import com.ncipher.km.nfkm.Key;
import com.ncipher.km.nfkm.KeyGenerator;
import com.ncipher.km.nfkm.Module;
import com.ncipher.km.nfkm.SecurityWorld;
import com.ncipher.km.nfkm.Slot;
import com.ncipher.nfast.NFException;
import com.ncipher.nfast.connect.utils.EasyConnection;
import com.ncipher.nfast.marshall.M_ByteBlock;
import com.ncipher.nfast.marshall.M_KeyData;
import com.ncipher.nfast.marshall.M_KeyType;
import com.ncipher.nfast.marshall.M_KeyType_Data_Random;

public class ImportKey
{
    private static final int AES128_KEY_SIZE = 16;
    
    public static void main(String[] args)
    {
        if (args.length < 2)
            usage();
        
        try {
            byte[] rawKey = new byte[AES128_KEY_SIZE];
            File file = new File(args[0]);
            if (file.length() != AES128_KEY_SIZE) {
                System.err.println("Key file must be 16 bytes long.");
                System.exit(1);
            }
            
            FileInputStream in = new FileInputStream(file);
            in.read(rawKey);
            in.close();
            
            importKey(rawKey, args[1]);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void importKey(byte[] aesKey, String keyName) throws NFException
    {
        EasyConnection conn = EasyConnection.connect();
        SecurityWorld sw = new SecurityWorld(conn.getConnection(), new ConsoleCallBack());
        Module module = sw.getModule(1);
        Slot slot = module.getSlot(0);
        CardSet cs = slot.getCardSet();
        if (cs == null)
            throw new NFException("OCS missing");
        cs.load(slot);
        KeyGenerator keyGen = sw.getKeyGenerator();
        M_KeyData keyData = new M_KeyData(M_KeyType.Rijndael,
                                          new M_KeyType_Data_Random(new M_ByteBlock(aesKey)));
        Key key = keyGen.importKey(keyData, null, keyName, null, null, null, module, cs, 
                                   Boolean.TRUE);
        key.save();
        System.out.println("Key successfully imported to /opt/nfast/kmdata/key_" + 
                           key.getAppName() + '_' + key.getIdent());
    }

    private static void usage()
    {
        System.err.println("Usage: ImportKey <AES128 key file> <key name>");
        System.exit(1);
    }


}

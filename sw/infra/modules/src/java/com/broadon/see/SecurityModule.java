package com.broadon.see;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.interfaces.RSAPublicKey;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import java.util.Map.Entry;

import com.ncipher.nfast.NFException;

public abstract class SecurityModule implements Keywords
{
    protected HashMap keyMap;
    
    /**
     * @param size
     *            The number of random bytes
     * @return A sequence of random bytes.
     * @throws GeneralSecurityException
     * @throws com.ncipher.nfast.NFException
     */
    public abstract byte[] getRandom(int size) throws GeneralSecurityException, NFException;
    
    /**
     * Sign a given block of data
     * 
     * @param signer
     *            ID of the identity used for signing
     * @param document
     *            Block of data to be signed     
     * @return Signature of <code>document</code>
     * @throws java.security.GeneralSecurityException
     * @throws com.ncipher.nfast.NFException
     */    
    public byte[] sign(String signer, byte[] document) 
        throws GeneralSecurityException, NFException
    {
        return sign(signer, document, 0, document.length);
    }
    
    /**
     * Sign a given block of data
     * 
     * @param signer
     *            ID of the identity used for signing
     * @param document
     *            Block of data to be signed
     * @param offset  The offset into the document of the first byte to sign.
     * @param length The number of bytes to sign, starting from <code>offset</code>.      
     * @return Signature of <code>document</code>
     * @throws java.security.GeneralSecurityException
     * @throws com.ncipher.nfast.NFException
     */
    public byte[] sign(String signer, byte[] document, int offset, int length) 
        throws GeneralSecurityException, NFException
    {
        return sign(signer, new ByteArrayInputStream(document, offset, length));
    }
    
    
    /**
     * Sign all the data from an input stream.
     * @param signer ID of the identity used for signing
     * @param in Document as an input stream
     * @return Signature of the bytes in the input stream
     * @throws GeneralSecurityException
     * @throws NFException
     */
    public abstract byte[] sign(String signer, InputStream in)
        throws GeneralSecurityException, NFException;
    
    
    private byte[] sign(String signer, byte[] pkg, int sigOffset, int docOffset, int sigSize) 
        throws GeneralSecurityException, NFException
    {
        byte[] sig = sign(signer, pkg, docOffset, pkg.length - docOffset); 
        if (sig.length != sigSize)
            throw new InvalidKeyException("Signature size mismatch");

        System.arraycopy(sig, 0, pkg, sigOffset, sig.length);
        return pkg;
    }
    
    /**
     * Sign an eTicket.  This function could refuse to sign if the title ID is invalid.
     * @param signer
     * @param ticket
     * @param sigOffset Offset into <code>ticket</code> where the signature should be written.
     * @param docOffset Offset into <code>ticket</code> where the document begins.
     * @param sigSize Size of signature in bytes
     * @return The <code>ticket</code> with signature written in place.
     * @throws GeneralSecurityException
     * @throws NFException
     */
    public byte[] signTicket(String signer, byte[] ticket, int sigOffset, int docOffset,
                             int sigSize) 
        throws GeneralSecurityException, NFException
    {
        return sign(signer, ticket, sigOffset, docOffset, sigSize);
    }
    
    /**
     * Sign a TMD.  This function could refuse to sign if the title ID is invalid.
     * @param signer
     * @param tmd
     * @param sigOffset Offset into <code>tmd</code> where the signature should be written.
     * @param docOffset Offset into <code>tmd</code> where the document begins.
     * @param sigSize Size of signature in bytes
     * @return The <code>tmd</code> with signature written in place.
     * @throws GeneralSecurityException
     * @throws NFException
     */
    public byte[] signTMD(String signer, byte[] tmd, int sigOffset, int docOffset, int sigSize) 
        throws GeneralSecurityException, NFException
    {
        return sign(signer, tmd, sigOffset, docOffset, sigSize);
    }
    
    /**
     * @return ID of the one who authorizes this secure operation.
     */
    public String getAuthorizer()
    {
        return "unknown";
    }
    
    public boolean verify(String signer, byte[] document, byte[] signature) 
        throws GeneralSecurityException 
    {
        return verify(signer, document, 0, document.length, signature);
    }
    
    
    public abstract boolean verify(String signer, byte[] document, int offset, 
                                   int length, byte[] signature) 
        throws GeneralSecurityException;
    
    
    /**
     * Verify the signer's private key against the given public key. This could
     * be used to test if the private key associated with <code>signer</code>
     * corresponds to the public key.
     * 
     * @param signer
     * @param pubKey 
     * @return
     * @throws NFException 
     * @throws GeneralSecurityException 
     */
    public abstract boolean verify(String signer, RSAPublicKey pubKey) 
        throws GeneralSecurityException, NFException;
    
    
    protected void fillKeyMap(String keyName, Object keyInfo)
    {
        keyMap.put(keyName, keyInfo);
        
        // generate aliases of this key name.  Key name has the form: <pki>-<type>-<id>.
        // The <id> part is optional.
        // We generate aliases for <pki>-<type> and <type> as long as they are unique.
        // This allow keys to be identified using wild cards (sort of).
        int first = keyName.indexOf('-');
        if (first < 0)
            return;
        int second = keyName.indexOf('-', first + 1);
        if (second >= 0) {
            // <pki>-<type>
            String alias = keyName.substring(0, second);
            if (keyMap.containsKey(alias)) {
                // alias is ambigious, set the value to null so we could remove it later
                // should not remove it now because otherwise we can't catch addition conflicts.
                keyMap.put(alias, null);
            } else
                keyMap.put(alias, keyInfo);
        }
        
        String shortAlias = (second >= 0) ? keyName.substring(first + 1, second) : 
                                            keyName.substring(first + 1);
        if (keyMap.containsKey(shortAlias))
            keyMap.put(shortAlias, null);
        else
            keyMap.put(shortAlias, keyInfo);
    }

    /**
     * remove ambigious aliases (which should have their value marked <code>null</code>.
     *
     */
    protected void cleanKeyMap()
    {
        for (Iterator iter = keyMap.entrySet().iterator(); iter.hasNext();) {
             Entry element = (Entry) iter.next();
             if (element.getValue() == null)
                 iter.remove();
        }
    }
    
    protected SecurityModule() {
        keyMap = new HashMap();
    }

    public static SecurityModule getInstance(Properties prop)
        throws GeneralSecurityException, NFException 
    {
        if (prop == null)
            prop = new Properties();
        
        if ("true".equals(prop.getProperty(USE_SSM_KEY))) {
            return new SoftwareSecurityModule(prop);
        } else
            return new HardwareSecurityModule(prop);
    }
}

package com.broadon.unitTest.pas;

import java.rmi.RemoteException;

import com.broadon.wsapi.pas.AuthorizeAccountPaymentRequestType;
import com.broadon.wsapi.pas.AuthorizeAccountPaymentResponseType;
import com.broadon.wsapi.pas.BalanceAccountType;
import com.broadon.wsapi.pas.CaptureECardToAccountRequestType;
import com.broadon.wsapi.pas.CapturePaymentRequestType;
import com.broadon.wsapi.pas.CapturePaymentResponseType;
import com.broadon.wsapi.pas.CreateAccountRequestType;
import com.broadon.wsapi.pas.CreateAccountResponseType;
import com.broadon.wsapi.pas.RefundPaymentRequestType;
import com.broadon.wsapi.pas.RefundPaymentResponseType;
import com.broadon.wsapi.pas.VoidAuthorizationRequestType;
import com.broadon.wsapi.pas.PasTokenType;
import com.broadon.wsapi.pas.MoneyType;

public class AcctCaptureRefund extends TestCaseWrapper {
    CreateAccountRequestType req;
    CreateAccountResponseType resp;
    BalanceAccountType acct = null;
    PasTokenType token = null;
    MoneyType amount = null;
    public static void main(String[] args) {
        junit.textui.TestRunner.run(AcctCaptureRefund.class);
    }

    public AcctCaptureRefund(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
        resetTestDataBase(AccountTests);
        MoneyType amount = new MoneyType("POINTS", "50");
        PasTokenType token = eCardAuth("0010030001", amount);
        acct = new BalanceAccountType();
        acct.setAccountNumber("30001");
        acct.setPIN("30001");  
        req = createCreateAccountRequest(acct);
        resp = pas.createAccount(req);
        if (checkAbstractResponse(req, resp)) {
            CaptureECardToAccountRequestType req = createCaptureECardToAccountRequest(
                    token, acct, amount);
                pas.captureECardToAccount(req);
        }
        authorizeAccountPayment();
    }

    public void tearDown() throws Exception {
        voidAuthorization();
        super.tearDown();
    }
    // setup
    private void authorizeAccountPayment() {

        amount = new MoneyType("POINTS", "10"); 
        BalanceAccountType acct = new BalanceAccountType();
        acct.setAccountNumber("30001");
        acct.setPIN("30001");
        AuthorizeAccountPaymentRequestType authReq = createAcctAuthorizationRequest(
                acct, amount);
        boolean authReturn=false;

        try {
            AuthorizeAccountPaymentResponseType authResp = pas.authorizeAccountPayment(authReq);
            authReturn = checkAbstractResponse(authReq, authResp);
            if (authReturn) {
                token = authResp.getAuthorizationToken();
            }
        } catch (Exception e) {
            System.out.println(getStackTrace(e));
            System.out.println(e.getMessage());
        }
    }
    
    // teardown
    private void voidAuthorization() throws RemoteException {
        VoidAuthorizationRequestType voidReq = createVoidAuthorizationRequest(token);
        for (int i=0; i<5; i++)
            pas.voidAuthorization(voidReq);
    }
    
    // test part1: capture
    private boolean capture() throws RemoteException {
        CapturePaymentResponseType capResp;
        amount.setAmount("8");
        CapturePaymentRequestType capReq = createCapturePaymentRequest(token, amount, false);
        capResp = pas.capturePayment(capReq);
        return checkAbstractResponse(capReq, capResp);
    }
    
    // test part2: refund
    private boolean refund() throws RemoteException {
        RefundPaymentRequestType req = createRefundPaymentRequest(token, amount);
        RefundPaymentResponseType resp;
        resp = pas.refundPayment(req);
        return checkAbstractResponse(req, resp);
    }
    
    public boolean checkCaptureRefund() throws RemoteException {
        boolean testResult = true;
        testResult = capture();
        // repeat 10 times should make no difference because capture is idenpotent
        for (int i=0; i<10; i++)
            testResult = testResult & capture();
        if (testResult)
            // repeat makes no difference
            for (int i=0; i<15; i++)
                testResult = testResult & refund();
        return testResult;
    }
    public void testCaptureRefundAccount() throws RemoteException {
        assertTrue(checkCaptureRefund());
    }   
}
package com.broadon.unitTest.ias;

import com.broadon.wsapi.ias.RenewAccountTokenRequestType;
import com.broadon.wsapi.ias.RenewAccountTokenResponseType;

public class RenewAccountTokenSuccess extends TestCaseWrapper {

    public static void main(String[] args) {
    }

    public RenewAccountTokenSuccess(String name) {
	super(name);
    }

    public void setUp() throws Exception {
	super.setUp();
    }

    public void tearDown() throws Exception {
	super.tearDown();
    }

    private boolean checkRenewAccountTokenSuccess(
		String accountId, 
		String accountPassword,
                String accountToken,
                RenewAccountTokenResponseType expected)
	throws Throwable
    {
	boolean testResult = true;

        RenewAccountTokenResponseType resp;
        RenewAccountTokenRequestType req = new RenewAccountTokenRequestType();

        initRequest(req);
	req.setAccountId(Integer.parseInt(accountId));
	req.setPassword(accountPassword);
        req.setTokenId(accountToken);
		
	resp = ias.renewAccountToken(req);
	testResult &= checkAccountResponse(req, resp);
		
	// Check response against expected response
	testResult &= checkError(expected, resp);
	
        if (testResult) {
            //System.out.println("Current Token: " + accountToken + ", New Token: " + resp.getTokenId());
            
            if (!accountToken.equals(resp.getTokenId())) {
                TestDataAdmin.setExpiredAccountToken(accountId, accountToken);
                TestDataAdmin.setValidAccountToken(accountId, resp.getTokenId());
            } else
                testResult = false;
        }
        
        return testResult;
    }

    public void testRenewAccountTokenSuccess()
	throws Throwable
    {
        Token vt = TestDataAdmin.getValidAccountToken();
        
        if (vt != null) {
            // Case 1: Renew a token that has not expired
            RenewAccountTokenResponseType expected = new RenewAccountTokenResponseType();
            
            expected.setAccountId(Integer.parseInt(vt.id));
            expected.setErrorCode(000);
            expected.setErrorMessage("");
            assertTrue(checkRenewAccountTokenSuccess(vt.id, "pass"+vt.id, vt.tokenId, expected));
        } else {
            System.out.println(getName() + " Warning: Failed to obtain a valid account token");
        }
    }
}

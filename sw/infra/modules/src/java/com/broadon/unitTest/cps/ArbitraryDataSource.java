package com.broadon.unitTest.cps;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

import javax.activation.DataSource;

/**
 * Return some arbitrary data -- not even random.
 *
 */
public class ArbitraryDataSource implements DataSource
{

    private final int size;

    public ArbitraryDataSource(int size)
    {
        this.size = size;
    }

    public String getContentType()
    {
        return "application/octet-stream";
    }

    public InputStream getInputStream() throws IOException
    {
        return new ArbitraryInputStream(size);
    }

    public String getName()
    {
        return "Arbitrary data";
    }

    public OutputStream getOutputStream() throws IOException
    {
        return null;
    }

    
    class ArbitraryInputStream extends InputStream {

        private int size;


        ArbitraryInputStream(int size) {
            this.size = size;
        }
        
        public int available() throws IOException
        {
            return size;
        }

        public void close() throws IOException
        {
            size = 0;
        }

        
        public boolean markSupported()
        {
            return false;
        }

        public int read() throws IOException
        {
            return (size > 0) ? (size-- % 256) : -1;
        }

        public int read(byte[] buf, int off, int len) throws IOException
        {
            if (size >= len) {
                Arrays.fill(buf, off, len, (byte) size);
                size -= len;
                return len;
            } else if (size <= 0){
                return -1;
            } else {
                Arrays.fill(buf, off, len, (byte) size);
                int n = size;
                size = 0;
                return n;
            }
        }

        public int read(byte[] buf) throws IOException
        {
            return read(buf, 0, buf.length);
        }

        
        public long skip(long len) throws IOException
        {
            if (size >= len) {
                size -= len;
                return len;
            } else {
                long n = size;
                size = 0;
                return n;
            }
        }
    }
   
    /**
     * @param args
     */
    public static void main(String[] args)
    {
        // TODO Auto-generated method stub

    }

}

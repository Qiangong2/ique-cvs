/*
 * JUnit TestCase wrapper for PCIS
 *   - this class contains functions that should be shared among PCIS
 *     test cases.
 */
package com.broadon.unitTest.pcis;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import junit.framework.TestCase;

import com.broadon.status.StatusCode;
import com.broadon.unitTest.Configuration;
import com.broadon.unitTest.ResourcePool;
import com.broadon.unitTest.TestLog;
import com.broadon.wsapi.pcis.AbstractRequestType;
import com.broadon.wsapi.pcis.AbstractResponseType;
import com.broadon.wsapi.pcis.PrepaidCardGenerationPortType;

public class TestCaseWrapper extends TestCase {
    protected PrepaidCardGenerationPortType pcis = null;

    protected static String hostName = null;
	
    protected static Object sync;
    protected static boolean hasTestTitle;
    protected boolean tracing = false;
    protected boolean doneSetup = false;
	
    public TestCaseWrapper(String name) {
	super(name);
        if (hostName != null) {
            Configuration.setHostName(hostName);
            Configuration.setDomainName("");
        }
    }

    public void setUp() throws Exception {
	super.setUp();
    if (pcis == null) {
        pcis = ResourcePool.newPrepaidCardGenerationService();
    }
    }

    public void tearDown() throws Exception {
	super.tearDown();
    }
    protected void initAbstractRequest(AbstractRequestType req) {
        req.setVersion("1.0");
        req.setMessageID(Long.toString(System.currentTimeMillis()));
    }
    
    protected boolean checkAbstractResponse(AbstractRequestType req,
            AbstractResponseType resp) {
        if (resp.getErrorCode() == Integer.parseInt(StatusCode.SC_OK))
            return true;
        else
            return false;
       
    }
    protected void runTest() throws Throwable 
    {
	boolean testResult = true;
	try {
	    super.runTest();
	} catch (Throwable e) {
	    System.out.println(getName() + " error: " + e.getMessage());
	    if (tracing) {
		e.printStackTrace();
	    }
	    testResult = false;
	    throw e;
	} finally {
	    TestLog.report("PCIS", getName(), testResult);
	}
    }
    public static String getStackTrace(Throwable aThrowable) {
        final Writer result = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(result);
        aThrowable.printStackTrace(printWriter);
        return result.toString();
    }
}

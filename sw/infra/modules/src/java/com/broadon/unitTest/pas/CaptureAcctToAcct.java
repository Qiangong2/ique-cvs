package com.broadon.unitTest.pas;

import java.rmi.RemoteException;

import com.broadon.wsapi.pas.AuthorizeAccountPaymentRequestType;
import com.broadon.wsapi.pas.AuthorizeAccountPaymentResponseType;
import com.broadon.wsapi.pas.BalanceAccountType;
import com.broadon.wsapi.pas.CaptureAccountToAccountRequestType;
import com.broadon.wsapi.pas.CaptureAccountToAccountResponseType;
import com.broadon.wsapi.pas.CaptureECardToAccountRequestType;
import com.broadon.wsapi.pas.CreateAccountRequestType;
import com.broadon.wsapi.pas.CreateAccountResponseType;
import com.broadon.wsapi.pas.PasTokenType;
import com.broadon.wsapi.pas.MoneyType;

public class CaptureAcctToAcct extends TestCaseWrapper {
    CreateAccountRequestType req;
    CreateAccountResponseType resp;
    BalanceAccountType acct = null;
    PasTokenType token = null;
    MoneyType amount = null;
    public static void main(String[] args) {
        junit.textui.TestRunner.run(CaptureAcctToAcct.class);
    }

    public CaptureAcctToAcct(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
        resetTestDataBase(AccountTests);
        amount = new MoneyType("POINTS", "50");
        token = eCardAuth("0010030001", amount);
        acct = new BalanceAccountType();
        acct.setAccountNumber("30001");
        acct.setPIN("30001");  
        req = createCreateAccountRequest(acct);
        resp = pas.createAccount(req);
        if (checkAbstractResponse(req, resp)) {
            CaptureECardToAccountRequestType req = createCaptureECardToAccountRequest(
                    token, acct, amount);
                pas.captureECardToAccount(req);
            amount = new MoneyType("POINTS", "33");
            AuthorizeAccountPaymentRequestType authReq = createAcctAuthorizationRequest(
                    acct, amount);
            AuthorizeAccountPaymentResponseType authResp;
            authResp = pas.authorizeAccountPayment(authReq);
            if (checkAbstractResponse(authReq, authResp)) {   // no error, go ahead to void it
                token = authResp.getAuthorizationToken();
            }
        }
        acct.setAccountNumber("30002");
        acct.setPIN("30002");  
        req = createCreateAccountRequest(acct);
        pas.createAccount(req);
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }
    
    public boolean checkCaptureAcctToAcct() throws RemoteException {
        boolean testResult = true;
        CaptureAccountToAccountRequestType depReq = createCaptureAccountToAccountRequest(token, acct, amount);
        try {
            CaptureAccountToAccountResponseType resp = pas.captureAccountToAccount(depReq);
            testResult = checkAbstractResponse(depReq, resp);
        } catch (Exception e) {
            System.out.println(getStackTrace(e));
            System.out.println(e.getMessage());
            testResult = false;
        }
        return testResult;
    }
    

    public void testCaptureAcctToAcct() throws RemoteException {
        assertTrue(checkCaptureAcctToAcct());
    }   
}
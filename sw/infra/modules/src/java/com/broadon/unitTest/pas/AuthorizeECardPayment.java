package com.broadon.unitTest.pas;

import com.broadon.wsapi.pas.AuthorizeECardPaymentRequestType;
import com.broadon.wsapi.pas.AuthorizeECardPaymentResponseType;
import com.broadon.wsapi.pas.VoidAuthorizationRequestType;
import com.broadon.wsapi.pas.VoidAuthorizationResponseType;
import com.broadon.wsapi.pas.PasTokenType;
import com.broadon.wsapi.pas.MoneyType;

public class AuthorizeECardPayment extends TestCaseWrapper {
    String[] ecards;
    public static void main(String[] args) {
        junit.textui.TestRunner.run(AuthorizeECardPayment.class);
    }

    public AuthorizeECardPayment(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
        //resetTestDataBase(ECardTests);
        ecards = createECards(10, 1, "JP");
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    public boolean checkAuthorizeECaedPayment() {
        boolean testResult = true;
        for (int i=0; i< ecards.length; i++) {
            System.out.println(ecards[i]);
            System.out.println(typeMap.get(ecards[i]));
            System.out.println(hashMap.get(ecards[i]));
            System.out.println(countryMap.get(ecards[i]));
        }
        String eCardNumber = ecards[0];
        MoneyType amount = new MoneyType("POINTS", "10"); 
        AuthorizeECardPaymentRequestType authReq = createAuthorizationRequest(
                eCardNumber, amount);
        AuthorizeECardPaymentResponseType authResp;
        VoidAuthorizationResponseType voidResp;
        boolean authReturn=false, voidReturn=false;
        try {
            authResp = pas.authorizeECardPayment(authReq);
            authReturn = checkAbstractResponse(authReq, authResp);
            if (authReturn) {   // no error, go ahead to void it
                PasTokenType token = authResp.getAuthorizationToken();
                VoidAuthorizationRequestType voidReq = createVoidAuthorizationRequest(token);
                voidResp = pas.voidAuthorization(voidReq);
                voidReturn = checkAbstractResponse(voidReq, voidResp);
            }
            testResult &= authReturn;
            testResult &= voidReturn;
        } catch (Exception e) {
            System.out.println(getStackTrace(e));
            System.out.println(e.getMessage());
            testResult = false;
        }
        return testResult;
    }
    
    public void testAuthorizeECardPayment() {
        assertTrue(checkAuthorizeECaedPayment());
    }   
}
package com.broadon.unitTest.pas;

import com.broadon.wsapi.pas.GetECardHistoryRequestType;
import com.broadon.wsapi.pas.GetECardHistoryResponseType;

public class GetECardHistory extends TestCaseWrapper {

    public static void main(String[] args) {
        junit.textui.TestRunner.run(GetECardHistory.class);
    }

    public GetECardHistory(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
        resetTestDataBase(ECardTests);
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    public boolean checkGetECardHistory() {
        boolean testResult = true;
//      from batch03:    000101 0010000000 0014264647    
        String eCardSequence = "0010000000";
        GetECardHistoryRequestType acctReq = createGetECardHistoryRequest(eCardSequence);
        GetECardHistoryResponseType acctResp;

        try {
            acctResp = pas.getECardAccountHistory(acctReq);
            testResult &= checkAbstractResponse(acctReq, acctResp);
        } catch (Exception e) {
            System.out.println(getStackTrace(e));
            System.out.println(e.getMessage());
            testResult = false;
        }
        
        return testResult;
    }
    
    public void testGetEcardHistory() {
        assertTrue(checkGetECardHistory());
    }   
}

/*
 * JUnit TestCase wrapper for ECS
 *   - this class contains functions that should be shared among ECS 
 *     test cases.
 */
package com.broadon.unitTest.ecs;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;

import junit.framework.TestCase;

import com.broadon.unitTest.Configuration;
import com.broadon.unitTest.DBConnection;
import com.broadon.unitTest.ResourcePool;
import com.broadon.unitTest.TestLog;
import com.broadon.wsapi.ecs.AbstractRequestType;
import com.broadon.wsapi.ecs.AbstractResponseType;
import com.broadon.wsapi.ecs.AccountPaymentType;
import com.broadon.wsapi.ecs.ContentInfoType;
import com.broadon.wsapi.ecs.ECardPaymentType;
import com.broadon.wsapi.ecs.ECommercePortType;
import com.broadon.wsapi.ecs.MoneyType;
import com.broadon.wsapi.ecs.PaymentMethodType;
import com.broadon.wsapi.ecs.PaymentType;
import com.broadon.wsapi.ecs.TitleInfoType;
import com.broadon.wsapi.ecs.TitleVersionType;

public class TestCaseWrapper extends TestCase {
    protected ECommercePortType ecs = null;

    public final static String IQUE_SUBSCRIPTION_CHANNEL = "00020001";
    
    public static String COUNTRY_DE = "DE";
    public static String COUNTRY_US = "US";
    public static String COUNTRY_JP = "JP";
    public static String COUNTRY_CN = "CN";
    
    public static String testCountry;
    
    public final static String REGION_IQUE = "IQUE";
    public final static String REGION_TEST = "TEST";
    
    public static String testRegion;
    
    public final static int DEVICE_TYPE_BB = 0;
    public final static int DEVICE_TYPE_RV = 1;
    public final static int DEVICE_TYPE_NC = 2;

    public static String testTitles[];
    public static int testItemIds[];
    public static String testPrices[];
    public static int testLimits[];

    public final static String START_BALANCE = "1000000000";
    
    public final static long validRV1 = getDeviceId(DEVICE_TYPE_RV, 1);

    public final static long validNC2 = getDeviceId(DEVICE_TYPE_NC, 2);
    public final static long validNC1 = getDeviceId(DEVICE_TYPE_NC, 1);
    public final static long invalidNC = getDeviceId(DEVICE_TYPE_NC, 0);
    
    public final static long validNCs[] = { validNC1, validNC2 };
    
    public static long testDevice;
    
    public static byte[] validDeviceCert;
    public static byte[] invalidDeviceCert;

    public static PaymentType validCreditCard;
    public static PaymentType validPrepaidCard; // Card using units of points 
    public static PaymentType validTitleECard; // Card that can be redeemed for title 
    public static PaymentType validPointsECard; // Card that can be redeemed for title 
    public static PaymentType validSubscriptionECard; // Card that can be redeemed for subscription (uses units of days)
    public static PaymentType validSubscriptionECardNoType; // Card that can be redeemed for subscription (uses units of days)
    public static PaymentType validVCAccount; 
    public static PaymentType invalidECard2;
    public static PaymentType invalidECard1;

    public static PaymentType[] invalidPayments;
    public static int[] invalidPaymentErrors;
    
    static {
        // TODO: Make valid cert
        try {
            validDeviceCert = readDeviceCert();
        } catch (IOException e) {
            System.out.println("Error reading device cert: " + e.getMessage());
            validDeviceCert = "My valid device cert".getBytes();
        }
      
        invalidDeviceCert = "My invalid device cert".getBytes();
        

        validVCAccount = new PaymentType();
        validVCAccount.setPaymentMethod(PaymentMethodType.ACCOUNT);
        validVCAccount.setAccountPayment(
                new AccountPaymentType("30062", "30062"));

        invalidECard1 = new PaymentType();
        invalidECard1.setPaymentMethod(PaymentMethodType.ECARD);
        invalidECard1.setECardPayment(
                new ECardPaymentType("0", "0", "3241"));

        invalidECard2 = new PaymentType();
        invalidECard2.setPaymentMethod(PaymentMethodType.ECARD);
        invalidECard2.setECardPayment(
                new ECardPaymentType("12345234525", "abae", "1341jipquiwerqw"));
        
        invalidPayments = new PaymentType[] { invalidECard1, invalidECard2 };
        invalidPaymentErrors = new int[] { 800, 616 };
    }
    
    protected boolean tracing = false;
    
    public TestCaseWrapper(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
        if (ecs == null) {
            ecs = ResourcePool.newECommerceService();
        }

        testTitles = new String[10];
        testItemIds = new int[10];
        testPrices = new String[10];
        testLimits = new int[10];
        if (Configuration.getProduct() == Configuration.RVL) {
            testTitles[0] = "0000000100000001";
            testTitles[1] = "0000000100000002";
            testTitles[2] = "0000000100000003";
            testTitles[3] = "0001000000081001";
            testTitles[4] = "0001000000081002";
            testTitles[5] = "0001000000081003";
            testTitles[6] = "0001000000081004";
            testTitles[7] = "0001000000081004";
            testItemIds[5] = 32030;
            testPrices[5] = "1";
            testItemIds[6] = 32041;
            testPrices[6] = "0";
            testLimits[6] = 600;
            testItemIds[7] = 32040;
            testPrices[7] = "1";
            testDevice = validRV1;
            testCountry = "UST";
            testRegion = REGION_TEST;
        } else {
            testTitles[0] = "0000000200000001";
            testTitles[1] = "0000000200000002";
            testTitles[2] = "0000000200000003";
            testTitles[3] = "0002000100080020";
            testTitles[4] = "0002000100080030";
            testTitles[5] = "0002000100080021";
            testTitles[6] = "0002000100080033";
            testTitles[7] = "0002000100080033";
            testItemIds[5] = 8;
            testPrices[5] = "500";
            testItemIds[6] = 198;
            testPrices[6] = "0";
            testLimits[6] = 3600;
            testItemIds[7] = 197;
            testPrices[7] = "500";
            testDevice = validNC1;
            testCountry = COUNTRY_CN;
            testRegion = REGION_IQUE;
        }
    }

    public static byte[] readDeviceCert()
        throws IOException
    {   
        String certFilename;
        if (Configuration.getDataSet() == Configuration.PERF) {
            Properties prop = Configuration.getPerfProperties();
            certFilename = prop.getProperty("certPath", "./")+"/00/00/00/00.cert";
        } else {
            String root = System.getProperty("ROOT", 
                                             "/home/build/bcc-2.0/sw/imports/ncroot");
            certFilename = root + "/usr/etc/pki_data/dev_nc_bpki.cert";
        }
        File certFile = new File(certFilename);
        BufferedInputStream iStream = new BufferedInputStream(new FileInputStream(certFile));
        long fileSize = certFile.length();
        byte[] cert = new byte[(int) fileSize];
        iStream.read(cert);        
        return cert;
    }
    
    public static void clearETicketTransfers() {
        // DB Parameters
        String db_user = "ecs";
        String db_pswd = "ecs";
        String db_url = Configuration.getDbUrl();

        String sql = "delete from ETICKET_TRANSFERS where birth_type='ECS_TEST'";
        
        Connection conn = null;
        PreparedStatement pstmt = null;
        try {
            DBConnection db = new DBConnection(db_user, db_pswd, db_url);
            conn = db.getConnection();
            pstmt = conn.prepareStatement(sql);
            pstmt.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (conn != null)
                    conn.close();
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    
    public static void clearETickets(long deviceId, String rtype) {
        // DB Parameters
        String db_user = "ecs";
        String db_pswd = "ecs";
        String db_url = Configuration.getDbUrl();

        String sql = "delete from ETICKETS where device_id=" + deviceId;
        if (rtype != null) {
            sql = sql + " and rtype='" + rtype + "'"; 
        }
        
        Connection conn = null;
        PreparedStatement pstmt = null;
        try {
            DBConnection db = new DBConnection(db_user, db_pswd, db_url);
            conn = db.getConnection();
            pstmt = conn.prepareStatement(sql);
            pstmt.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (conn != null)
                    conn.close();
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    
    public static long createETicket(long deviceId, BigInteger contentId, String licenseType, String rtype, long limits) {
        // DB Parameters
        String db_user = "ecs";
        String db_pswd = "ecs";
        String db_url = Configuration.getDbUrl();

        long tid = Double.doubleToLongBits(Math.random());
        String sql = "INSERT into ETICKETS " +
            "    (DEVICE_ID, CONTENT_ID, BIRTH_TYPE, RTYPE, TOTAL_LIMITS, TID, LICENSE_TYPE)" +
            "    VALUES( " + deviceId + ", " + contentId + ", 'ECS_TEST', '" + rtype + "', " + limits +
            "    ," + tid + ", '" + licenseType + "')";
        
        Connection conn = null;
        PreparedStatement pstmt = null;
        try {
            DBConnection db = new DBConnection(db_user, db_pswd, db_url);
            conn = db.getConnection();
            pstmt = conn.prepareStatement(sql);
            pstmt.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (conn != null)
                    conn.close();
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tid;
    }
    
    public void tearDown() throws Exception {
        super.tearDown();
    }
    
    protected void runTest() throws Throwable 
    {
        boolean testResult = true;
        try {
            super.runTest();
        } catch (Throwable e) {
            System.out.println(getName() + " error: " + e.getMessage());
            if (tracing) {
                e.printStackTrace();
            }
            testResult = false;
            throw e;
        } finally {
            TestLog.report("ECS", getName(), testResult);
        }
    }

    protected void initAbstractRequest(AbstractRequestType req) {
        req.setVersion("1.0");
        req.setDeviceId(testDevice);
        req.setRegionId(testRegion);
        req.setCountryCode(testCountry);
        req.setMessageId(Long.toString(System.currentTimeMillis()));
    }

    protected boolean checkAbstractResponse(
            AbstractRequestType req,
            AbstractResponseType resp)
    {
        assertEquals("getVersion", req.getVersion(), resp.getVersion());
        assertEquals("getDeviceId", req.getDeviceId(), resp.getDeviceId());
        assertEquals("getMessageId", req.getMessageId(), resp.getMessageId());
        return true;
    }
    
    protected boolean checkError(
            AbstractResponseType expected,
            AbstractResponseType resp)
    {
        assertEquals("getErrorCode", expected.getErrorCode(), resp.getErrorCode());
        String expectedPattern = expected.getErrorMessage();
        String message = resp.getErrorMessage();
        if ((expectedPattern != null) && (message != null)) {
            assertTrue("getErrorMessage was '" + message +
                    "', should match '" + expectedPattern + "'",
                    message.matches(expectedPattern));
        } else {
            assertEquals("getErrorMessage", expectedPattern, message);
        }
        return true;
    }

    public boolean checkBalance(String message, MoneyType expected, MoneyType resp)
    {
        if (expected == null || resp == null) {
            assertEquals(message, expected, resp);
            return true;
        }
        message = message + ".";
        assertEquals(message + "getAmount", expected.getAmount(), resp.getAmount());
        assertEquals(message + "getCurrency", expected.getCurrency(), resp.getCurrency());
        return true;
        
    }
    
    protected boolean checkContentInfo(String message, ContentInfoType expected, ContentInfoType resp)
    {
        if (expected == null || resp == null) {
            assertEquals(message, expected, resp);
            return true;
        }
        message = message + ".";
        assertEquals(message + "getContentId", expected.getContentId(), resp.getContentId());
        assertEquals(message + "getContentSize", expected.getContentSize(), resp.getContentSize());
        return true;
    }
    
    protected boolean checkContents(String message, ContentInfoType[] expected, ContentInfoType[] resp)
    {
        if (expected == null || resp == null) {
            assertEquals(message, expected, resp);
            return true;
        }
        assertEquals(message + ".length", expected.length, resp.length);
        boolean testResult = true;
        for (int i = 0; i < expected.length; i++) {
            testResult &= checkContentInfo(message, expected[i], resp[i]);
        }
        return testResult;
    }
    
    protected boolean checkTitleInfo(String message, TitleInfoType expected, TitleInfoType resp)
    {
        if (expected == null || resp == null) {
            assertEquals(message, expected, resp);
            return true;
        }
        message = message + ".";
        assertEquals(message + "getTitleId", expected.getTitleId(), resp.getTitleId());
        assertEquals(message + "getTitleKind", expected.getTitleKind(), resp.getTitleKind());
        assertEquals(message + "getTitleName", expected.getTitleName(), resp.getTitleName());
        assertEquals(message + "getTitleDescription", expected.getTitleDescription(), resp.getTitleDescription());
        assertEquals(message + "getTitleSize", expected.getTitleSize(), resp.getTitleSize());
        assertEquals(message + "getFsSize", expected.getFsSize(), resp.getFsSize());
        if (expected.getVersion() != 0) {
            // Only check version if expected is not 0
            assertEquals(message + "getVersion", expected.getVersion(), resp.getVersion());
        }
        assertEquals(message + "getCategory", expected.getCategory(), resp.getCategory());
        return checkContents(message + "getContents", expected.getContents(), resp.getContents());
    }
    
    protected boolean checkTitleVersion(String message, TitleVersionType expected, TitleVersionType resp)
    {
        if (expected == null || resp == null) {
            assertEquals(message, expected, resp);
            return true;
        }
        message = message + ".";
        assertEquals(message + "getTitleId", expected.getTitleId(), resp.getTitleId());
        // Only check version if expected version is positive
        if (expected.getVersion() >= 0) {
            assertEquals(message + "getVersion", expected.getVersion(), resp.getVersion());
        }
        return true;
    }
    
    public boolean checkTitleVersions(String message, TitleVersionType[] expected, TitleVersionType[] resp)
        throws Throwable
    {
        if (expected == null || resp == null) {
            assertEquals(message, expected, resp);
            return true;
        }
        assertEquals(message + "length", expected.length, resp.length);
        boolean testResult = true;
        for (int i = 0; i < expected.length; i++) {
            testResult &= checkTitleVersion(message, expected[i], resp[i]);
        }
        return testResult;
    }
    
    protected static long getDeviceId(int deviceType, int chipId)
    {
        long deviceId = chipId;
        deviceId += ((long) deviceType) << 32;
        return deviceId;
    }

    protected void createAccount(String acctNumber) {
        // DB Parameters
        String db_user = "pas";
        String db_pswd = "pas";
        String db_url = Configuration.getDbUrl();
        
        String resetAccounts = " delete from pas_bank_accounts where ACCOUNT_ID "
            + " = " + acctNumber;
        String insert = "insert into pas_bank_accounts values("+acctNumber+", 'VCPOINTS', '01-APR-98', null, null, "+START_BALANCE+", 0, 'POINTS', '01-APR-98', "+acctNumber+")";
        Connection conn = null;
        PreparedStatement pstmt = null;
        try {
            DBConnection db = new DBConnection(db_user, db_pswd, db_url);
            conn = db.getConnection();
            pstmt = conn.prepareStatement(resetAccounts);
            pstmt.executeUpdate();
            pstmt = conn.prepareStatement(insert);
            pstmt.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (conn != null)
                    conn.close();
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
     }

    protected void createTransaction(String acctNumber, String id, String type){
        // DB Parameters
        String db_user = "ecs";
        String db_pswd = "ecs";
        String db_url = Configuration.getDbUrl();
        
        String delete = "delete from ecs_transactions where TRANS_ID = "
            + id;
        String insert = "insert into ecs_transactions (TRANS_ID, TRANS_DATE, TRANS_TYPE, ACCOUNT_ID, DEVICE_ID, STATUS, CLIENT_IPADDR, TOTAL_AMOUNT, TOTAL_TAXES, TOTAL_PAID, CURRENCY, ORIG_TRANS_ID, OPERATOR_ID, COUNTRY_ID, ITEM_ID, QTY, TOTAL_DISCOUNT, PAYMENT_TYPE, PAYMENT_METHOD_ID, PAS_AUTH_ID, PAYMENT_ECARD_TYPE, STATUS_DATE, SERIAL_NO) values("
            +id+", '01-APR-98', '"+type+"', "+acctNumber+", 0, 'SUCCESS', null, 0, null, 0, 'POINTS', null, null, 0, 0, 0, 0, null, null, null, null, null, null)";
        Connection conn = null;
        PreparedStatement pstmt = null;
        try {
            DBConnection db = new DBConnection(db_user, db_pswd, db_url);
            conn = db.getConnection();
            pstmt = conn.prepareStatement(delete);
            pstmt.executeUpdate();
            pstmt = conn.prepareStatement(insert);
            pstmt.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (conn != null)
                    conn.close();
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void makeSubscriptionECard() {
        String[] subscriptionECards = null;

        try {
            subscriptionECards = com.broadon.unitTest.pas.TestCaseWrapper.createECards(101, 1, testCountry);
        } catch (Exception e) {
            System.out.println("createECards Exception: "+e);
        };

        validSubscriptionECardNoType = new PaymentType();
        validSubscriptionECardNoType.setPaymentMethod(PaymentMethodType.ECARD);
        validSubscriptionECardNoType.setECardPayment(
            new ECardPaymentType(subscriptionECards[0], "000000", (String)com.broadon.unitTest.pas.TestCaseWrapper.hashMap.get(subscriptionECards[0])));
        
        validSubscriptionECard = new PaymentType();
        validSubscriptionECard.setPaymentMethod(PaymentMethodType.ECARD);
        validSubscriptionECard.setECardPayment(
            new ECardPaymentType(subscriptionECards[0], "000101", (String)com.broadon.unitTest.pas.TestCaseWrapper.hashMap.get(subscriptionECards[0])));
    }

    public void makeTitleECard() {
        String[] titleECards = null;

        try {
            titleECards = com.broadon.unitTest.pas.TestCaseWrapper.createECards(1, 1, testCountry);
        } catch (Exception e) {
            System.out.println("createECards Exception: "+e);
        };

        validTitleECard = new PaymentType();
        validTitleECard.setPaymentMethod(PaymentMethodType.ECARD);
        validTitleECard.setECardPayment(
            new ECardPaymentType(titleECards[0], "000001", (String)com.broadon.unitTest.pas.TestCaseWrapper.hashMap.get(titleECards[0])));
    }

    public void makePointsECard() {
        String[] pointsECards = null;

        try {
            pointsECards = com.broadon.unitTest.pas.TestCaseWrapper.createECards(10, 1, testCountry);
        } catch (Exception e) {
            System.out.println("createECards Exception: "+e);
        };

        validPrepaidCard = new PaymentType();
        validPrepaidCard.setPaymentMethod(PaymentMethodType.ECARD);
        validPrepaidCard.setECardPayment(
                new ECardPaymentType(pointsECards[0], "000010", (String)com.broadon.unitTest.pas.TestCaseWrapper.hashMap.get(pointsECards[0])));

        validPointsECard = validPrepaidCard;
    }
}

/*
 * JUnit TestCase wrapper for CPS
 *   - this class contains functions that should be shared among CPS
 *     test cases.
 */
package com.broadon.unitTest.cps;

import java.io.IOException;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.Random;

import javax.activation.DataHandler;

import junit.framework.TestCase;

import com.broadon.unitTest.Configuration;
import com.broadon.unitTest.ResourcePool;
import com.broadon.unitTest.TestLog;
import com.broadon.wsapi.cps.ActionType;
import com.broadon.wsapi.cps.ContentAttributeType;
import com.broadon.wsapi.cps.PublishPortType;
import com.broadon.wsapi.cps.PublishRequestType;
import com.broadon.wsapi.cps.PublishResponseType;

public class TestCaseWrapper extends TestCase {

    protected static final long TEST_CHANNEL = 0x0001800000000000L;
    protected static final long TEST_SUBSCRIPT_CHANNEL = 0x0001800100000000L;
    protected static final String SUBSCRIPT_CHANNEL_PASSWORD = "channel password";
	
    protected PublishPortType cps = null;
    protected boolean tracing = false;
    
    protected static DBSetUp db;
    protected static String hostName = null;
    protected static DataHandler rawContent[];
    
    protected long channel;
    
    static {
        try {
            db = new DBSetUp();
            db.removeChannel(TEST_CHANNEL);
            db.removeChannel(TEST_SUBSCRIPT_CHANNEL);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }    
        
        try {
            Random rand = new Random();
            byte[] buf = new byte[64];
            rand.nextBytes(buf);
            rawContent = new DataHandler[1];
            rawContent[0] = new DataHandler(new ByteArrayDataSource(buf));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
	
    public TestCaseWrapper(String name) {
	super(name);
        channel = TEST_CHANNEL;
        if (hostName != null) {
            Configuration.setHostName(hostName);
            Configuration.setDomainName("");
        }
    }

    public void setUp() throws Exception {
	super.setUp();
        if (cps == null) {
            cps = ResourcePool.newPublishService();
        }
    }

    public void tearDown() throws Exception {
	super.tearDown();
    }
	
    protected void runTest() throws Throwable 
    {
	boolean testResult = true;
	try {
	    super.runTest();
	} catch (Throwable e) {
	    System.out.println(getName() + " error: " + e.getMessage());
	    if (tracing) {
		e.printStackTrace();
	    }
	    testResult = false;
	    throw e;
	} finally {
	    TestLog.report("CPS", getName(), testResult);
	}
    }

    protected String getTitleIDString(long titleID)
    {
        return Long.toHexString(titleID);
    }

    protected PublishRequestType initRequest(long titleID) throws IOException
    {
        ContentAttributeType[] contents = new ContentAttributeType[1];
        contents[0] = new ContentAttributeType(0, "Test Content", "GAME");
        
        PublishRequestType req = new PublishRequestType(ActionType.Create, "LOCAL_CPS",
                "password", null, "Test product code", getTitleIDString(channel + titleID), 
                "Test title", "GAME", "0", new byte[0], 1, 0, 0, contents);
        return req;
    }
    
    protected PublishRequestType initSubscription(long channelID)
    {
        PublishRequestType req = new PublishRequestType(ActionType.Create, "LOCAL_CPS",
                "password", null, "Test product code", getTitleIDString(channelID), "Test title", 
                "GAME", "0", new byte[0], 1, 0, 0, null);
        return req;
    }
    

    protected PublishResponseType publish(PublishRequestType req, DataHandler[] contents, 
                                          int responseCode)
        throws RemoteException
    {
        PublishResponseType resp = cps.publish(req, contents);
        assertEquals(responseCode, resp.getErrorCode());
        return resp;
    }
    
    
    protected PublishResponseType publish(PublishRequestType req, int responseCode)
        throws RemoteException
    {
        return publish(req, rawContent, responseCode);
    }
    

    protected void removeTitle(long titleID) throws SQLException
    {
        db.removeTitle(channel + titleID);
    }
}

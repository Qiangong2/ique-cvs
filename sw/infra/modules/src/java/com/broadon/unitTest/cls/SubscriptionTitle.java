package com.broadon.unitTest.cls;

import com.broadon.unitTest.Configuration;
import com.broadon.wsapi.cps.PublishRequestType;

public class SubscriptionTitle extends TestCaseWrapper
{
    static boolean channelCreated = false;

    public void setUp() throws Exception
    {
        super.setUp();
        if (! channelCreated ) {
            db.removeChannel(channel);
            GenLicense lic = new GenLicense(channel, true);
            publish(lic.resp, lic.outFiles, 0);
            channelCreated = true;;
        }
    }

    public SubscriptionTitle(String name)
    {
        super(name);
        channel = TEST_SUBSCRIPT_CHANNEL;
        
    }

    // test publishing 2 titles with same password to a subscription channel
    public void testCreateSubTitle() throws Throwable
    {
        long tid = 100;
        GenLicense lic = new GenLicense(tid, true);
        publish(lic.resp, lic.outFiles, 0);
        
        tid = 101;
        lic = new GenLicense(tid, true);
        publish(lic.resp, lic.outFiles, 0);
    }
    
    
    public void testCreateSubTitleWrongPassword() throws Throwable
    {
        final long tid = 102;
        
        PublishRequestType req = initRequest(tid);
        req.setEncryptionPassword("wrong password");
        GenLicense lic = new GenLicense(tid, req, true);
        publish(lic.resp, lic.outFiles, 1001);
    }
    
    public static void main(String[] args)
    {
        if (args != null && args.length > 0)
            hostName = args[0];
        Configuration.testLab1();
        junit.textui.TestRunner.run(SubscriptionTitle.class);
    }

}

package com.broadon.unitTest.creditcard;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import com.broadon.unitTest.ResourcePool;
import com.broadon.util.UUID;

import net.wiivc.services.AuthTxn;
import net.wiivc.services.InvoiceModifier;
import net.wiivc.services.PointsAppResults;
import net.wiivc.services.RefundTxn;
import net.wiivc.services.VCPaymentServiceLocator;
import net.wiivc.services.PointsConfirmationServiceLocator;
import net.wiivc.services.VCRefundServiceLocator;
public class CreditCardTest {
static final boolean doTaxEstimate = true;
static final boolean doAuth = true;
static final boolean doPointsApp = true;
static final boolean doRefund = false;
    /**
     * This program connects to a creditcard server (real or simulated)
     * to test NOA interafces. It does not go through BlockingAgent or
     * NonblockingAgent.
     * @param args
     * @throws MalformedURLException 
     * @throws ServiceException 
     * @throws RemoteException 
     */
    public static void main(String[] args) throws MalformedURLException, ServiceException, RemoteException {
        System.out.println("TEST BEGINS...");
        String taxAuthRulStr = ResourcePool.getVCPaymentURL();
        URL taxAuthURL = new URL(taxAuthRulStr);
        VCPaymentServiceLocator paymentServiceLocator = new VCPaymentServiceLocator();
        net.wiivc.services.VCPayment vcPayment = paymentServiceLocator.getVCPayment(taxAuthURL);
        // get tax
        AuthTxn txn = null;
        AuthTxn ret = null;
        AuthTxn authRet = null;
        if (doTaxEstimate) {
            txn = createAuthTxn();  // prepare the initial txn object for tax info.
            ret = vcPayment.sendATxn(txn);
            printAuthTxn(ret, "CALC_PAYMENT");
        }
        // get auth
        if (doTaxEstimate && doAuth) {
            preparePaymentAuth(ret);    // set attributes for payment authorization
            authRet = vcPayment.sendATxn(ret);
            printAuthTxn(authRet, "AUTH_PAYMENT");
        }
        if (doPointsApp) {
        // points applied
            String pointsAppliedUrlStr = ResourcePool.getPointsConfirmationURL();
            URL pointsAppliedURL = new URL(pointsAppliedUrlStr);
            PointsConfirmationServiceLocator pcServiceLocator = new PointsConfirmationServiceLocator();
            net.wiivc.services.PointsConfirmation pc = pcServiceLocator.getPointsConfirmation(pointsAppliedURL);
            String transIds[] = getTransIds(authRet);
            PointsAppResults[] results = pc.applyPoints(transIds);
            printPointsAppResults(results);
        }
        // refund
        if (doRefund) {
            String refundUrlStr = ResourcePool.getVCRefundURL();
            URL refundURL = new URL(refundUrlStr);
            VCRefundServiceLocator refundServiceLocator= new VCRefundServiceLocator();
            net.wiivc.services.VCRefund vcRefund = refundServiceLocator.getVCRefund(refundURL);
            RefundTxn refundTxn = new RefundTxn();
            refundTxn.setTransId("xyz");
            String refundReturn = vcRefund.doRefund(refundTxn);
            System.out.println(refundReturn);
        }
    }

    // set attributes for calling the 
    public static AuthTxn createAuthTxn() {
        AuthTxn txn = new AuthTxn();
        // attributes in alphabetic order, explicite lis all attributes
        txn.setAccountId("588");
        //txn.setApproveDate("2006.08.10");
        txn.setBillingCity("San Jose");         // ??
        txn.setBillingCounty("Santa Clara");    // ??
        txn.setBillingPostal("95130");
        txn.setBillingState("CA");
        txn.setCardExpMM("12");
        txn.setCardExpYY("08");
        txn.setCardNum("1234567890");
        txn.setCardType("V");
        txn.setCardVfyVal("123");
        txn.setClientIP("1.2.3.4");
        txn.setCountry("US");
        txn.setCurrency("USD");
        txn.setDeviceId("001");
        txn.setFunctionCode("CALC_PAYMENT");    // calculate payment
        //txn.setInvMod(setIMs());
        txn.setLanguage("Eng");
        txn.setPaymentMethodId("CC");
        txn.setPaymentType("CC");
        txn.setRefillPoints("1000");
        txn.setRegion("NOA");
        //txn.setResult("S");
        //txn.setSessionId("1234");
        txn.setTotalAmount("1000");
        //txn.setTotalPaid("1200");
        //txn.setTotalTax("123");
        txn.setTransDate("06.08.11");
        //txn.setTransId("abc");
        txn.setTransType("A");
        //txn.setVcMessage("Test");
        return txn;
    }
    
    public static void preparePaymentAuth(AuthTxn txn) {
        txn.setFunctionCode("AUTH_PAYMENT");
        UUID uuid = UUID.randomUUID();          // construct a pax transaction ID
        String txId = uuid.toBase64();
        txn.setTransId(txId);
    }
    // print return values
    public static void printAuthTxn(AuthTxn txn, String function) {
        System.out.println("Result from: " + function);
        System.out.println("sessionId= " + txn.getSessionId());
        System.out.println("result= " + txn.getResult());
        System.out.println("approveDate: " + txn.getApproveDate());
        System.out.println("tax= " + txn.getTotalTax());
        System.out.println("total= " + txn.getTotalPaid());
        printInvoiceModifiers(txn.getInvMod());
        System.out.println("VCMessage= " + txn.getVcMessage());
        printVcMsgCodes(txn.getVcMsgCode());
    }
    private static void printInvoiceModifiers(InvoiceModifier[] ims) {
        if (ims == null)
            return;
        System.out.println("InvoiceModifiers: ");
        for (int i=0; i<ims.length; i++) {
            System.out.println("    " + ims[i].getAmount());
            System.out.println("    " + ims[i].getDesc());
            System.out.println("    " + ims[i].getLineItemType());
        }
    }
    
    private static void printVcMsgCodes(String[] msgs) {
        if (msgs == null)
            return;
        System.out.println("VCMessage codes: ");
        for (int i=0; i<msgs.length; i++) {
            System.out.println("    " + msgs[i]);
        }
    }
    public static String[] getTransIds(AuthTxn txn) {
        String[] ret = new String[1];
        ret[0] = txn.getTransId();
        return ret;
    }
    
    public static void printPointsAppResults(PointsAppResults[] results) {
        for (int i=0; i<results.length; i++) {
            if (results[i].getAppliedResult().equalsIgnoreCase("SUCCESS"))
                System.out.println("Points Applied Confirmed for: " + results[i].getTransId());
            else
                System.out.println("Points Applied Failed for: " + results[i].getTransId());
        }
    }
}

/*
 * ECS TestSuite:  Add new ECS TestCase here to this class.
 */
package com.broadon.unitTest.oss;

import junit.framework.Test;
import junit.framework.TestSuite;

public class OssTestSuite {

    public static void main(String[] args) {
        junit.textui.TestRunner.run(OssTestSuite.suite());
    }

    public static Test suite() {
        TestSuite suite = new TestSuite("Test for com.broadon.unitTest.oss");
        //$JUnit-BEGIN$
        suite.addTestSuite(TestPages.class);
        //$JUnit-END$
        return suite;
    }

}

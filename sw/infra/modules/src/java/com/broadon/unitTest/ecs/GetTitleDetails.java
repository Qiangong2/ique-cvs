package com.broadon.unitTest.ecs;

import java.util.Arrays;

import com.broadon.wsapi.ecs.ContentInfoType;
import com.broadon.wsapi.ecs.GetTitleDetailsRequestType;
import com.broadon.wsapi.ecs.GetTitleDetailsResponseType;
import com.broadon.wsapi.ecs.LimitType;
import com.broadon.wsapi.ecs.PricingCategoryType;
import com.broadon.wsapi.ecs.PriceType;
import com.broadon.wsapi.ecs.PricingType;
import com.broadon.wsapi.ecs.RatingType;
import com.broadon.wsapi.ecs.TitleInfoType;
import com.broadon.wsapi.ecs.TitleKindType;

public class GetTitleDetails extends TestCaseWrapper {

    public static void main(String[] args) {
        junit.textui.TestRunner.run(GetTitleDetails.class);
    }

    public GetTitleDetails(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    private boolean checkTitlePricing(String message, PricingType expected, PricingType resp)
    {
        if (expected == null || resp == null) {
            assertEquals(message, expected, resp);
            return true;
        }
        message = message + ".";
        assertEquals(message + "getItemId", expected.getItemId(), resp.getItemId());
        assertEquals(message + "getPrice", expected.getPrice(), resp.getPrice());
        assertEquals(message + "getPricingCategory", expected.getPricingCategory(), resp.getPricingCategory());
        assertTrue(message + "getLimits", Arrays.equals(expected.getLimits(), resp.getLimits()));
        return true;
    }
    
    private boolean checkTitlePricings(String message, PricingType[] expected, PricingType[] resp)
    {
        if (expected == null || resp == null) {
            assertEquals(message, expected, resp);
            return true;
        }
        assertEquals(message + ".length", expected.length, resp.length);
        boolean testResult = true;
        for (int i = 0; i < expected.length; i++) {
            testResult &= checkTitlePricing(message, expected[i], resp[i]);
        }
        return testResult;
    }
    
    private boolean checkGetTitleDetails(
            GetTitleDetailsRequestType req,
            GetTitleDetailsResponseType expected) 
        throws Throwable    
    {
        boolean testResult = true;

        GetTitleDetailsResponseType resp;
        
        resp = ecs.getTitleDetails(req);
        testResult &= checkAbstractResponse(req, resp);
            
        // Check response against expected
        testResult &= checkError(expected, resp);

        testResult &= checkTitleInfo("getTitleInfo",
                expected.getTitleInfo(), resp.getTitleInfo());
        
        testResult &= checkTitlePricings("getTitlePricings", expected.getTitlePricings(), resp.getTitlePricings());
        assertTrue("getRatings", Arrays.equals(expected.getRatings(), resp.getRatings()));

        return testResult;
    }

    public void testGetTitleDetails()
        throws Throwable
    {       
        GetTitleDetailsResponseType expected = new GetTitleDetailsResponseType();

        ContentInfoType[] contents = new ContentInfoType[] { 
                new ContentInfoType("000200010008002100000000", 16817040 ),
                new ContentInfoType("000200010008002100000001", 520 ) };
        
        TitleInfoType titleInfo = new TitleInfoType();
        titleInfo.setTitleId("0002000100080033");
        titleInfo.setTitleDescription("NC 零使命");
        titleInfo.setTitleKind(TitleKindType.Games);
        titleInfo.setTitleName("NC Zero Mission");
        titleInfo.setTitleSize(8391016);
        titleInfo.setFsSize(8421376);
        titleInfo.setContents(null /* contents */);
        titleInfo.setCategory("ACTION");
        expected.setTitleInfo(titleInfo);
        
        PriceType points500 = new PriceType("500", "POINTS");
        PriceType points0 = new PriceType("0", "POINTS");
        PricingType pricingTrial = 
            new PricingType(198, new LimitType[] { new LimitType(3600, "TR") }, 
                    points0, PricingCategoryType.Trial);
        PricingType pricingPermanent = 
            new PricingType(197, new LimitType[] { new LimitType(0, "PR") }, 
                    points500, PricingCategoryType.Permanent);
        PricingType pricingPermanentFree = 
            new PricingType(12, new LimitType[] { new LimitType(0, "PR") }, 
                    points0, PricingCategoryType.Permanent);
        PricingType pricingSubscription = 
            new PricingType(0, new LimitType[] { new LimitType(0, "SR") },
                    new PriceType("0", " "), PricingCategoryType.Subscription);
        expected.setTitlePricings(new PricingType[] { 
                pricingPermanent, pricingTrial, pricingSubscription } );
        
        RatingType IQUE_E = new RatingType("IQUE", "E");
        RatingType[] ratings = new RatingType[] { IQUE_E };
        expected.setRatings(null/*ratings*/);
        
        GetTitleDetailsRequestType req = new GetTitleDetailsRequestType();

        initAbstractRequest(req);
        req.setTitleId(expected.getTitleInfo().getTitleId());
        req.setLanguage("en");
        titleInfo.setTitleDescription("NC Zero Mission");
        assertTrue(checkGetTitleDetails(req, expected));
    }

    public void testBadTitleId()
        throws Throwable
    {       
        GetTitleDetailsResponseType expected = new GetTitleDetailsResponseType();
        TitleInfoType titleInfo = new TitleInfoType();
        titleInfo.setTitleId("100");
        expected.setTitleInfo(titleInfo);
        expected.setErrorCode(602);
        expected.setErrorMessage(".*Invalid title ID.*");

        GetTitleDetailsRequestType req = new GetTitleDetailsRequestType();
        initAbstractRequest(req);
        req.setTitleId(expected.getTitleInfo().getTitleId());

        assertTrue(checkGetTitleDetails(req, expected));

        titleInfo.setTitleId("iworpjq");
        req.setTitleId(expected.getTitleInfo().getTitleId());
        assertTrue(checkGetTitleDetails(req, expected));
    }
}

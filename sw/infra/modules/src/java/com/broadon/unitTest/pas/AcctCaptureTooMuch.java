package com.broadon.unitTest.pas;

import java.rmi.RemoteException;

import com.broadon.wsapi.pas.AuthorizeAccountPaymentRequestType;
import com.broadon.wsapi.pas.AuthorizeAccountPaymentResponseType;
import com.broadon.wsapi.pas.BalanceAccountType;
import com.broadon.wsapi.pas.CaptureECardToAccountRequestType;
import com.broadon.wsapi.pas.CapturePaymentRequestType;
import com.broadon.wsapi.pas.CapturePaymentResponseType;
import com.broadon.wsapi.pas.CreateAccountRequestType;
import com.broadon.wsapi.pas.CreateAccountResponseType;
import com.broadon.wsapi.pas.VoidAuthorizationRequestType;
import com.broadon.wsapi.pas.PasTokenType;
import com.broadon.wsapi.pas.MoneyType;

public class AcctCaptureTooMuch extends TestCaseWrapper {
    CreateAccountRequestType req;
    CreateAccountResponseType resp;
    BalanceAccountType acct = null;
    PasTokenType token = null;
    MoneyType amount = null;
    public static void main(String[] args) {
        junit.textui.TestRunner.run(AcctCaptureTooMuch.class);
    }

    public AcctCaptureTooMuch(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
        resetTestDataBase(AccountTests);
        MoneyType amount = new MoneyType("POINTS", "50");
        PasTokenType token = eCardAuth("0010030001", amount);
        acct = new BalanceAccountType();
        acct.setAccountNumber("30001");
        acct.setPIN("30001");  
        req = createCreateAccountRequest(acct);
        resp = pas.createAccount(req);
        if (checkAbstractResponse(req, resp)) {
            CaptureECardToAccountRequestType req = createCaptureECardToAccountRequest(
                    token, acct, amount);
                pas.captureECardToAccount(req);
        }
        authorizeAccountPayment();
    }

    public void tearDown() throws Exception {
        voidAuthorization();
        super.tearDown();
    }
    // setup
    private void authorizeAccountPayment() {

        amount = new MoneyType("POINTS", "10"); 
        BalanceAccountType acct = new BalanceAccountType();
        acct.setAccountNumber("30001");
        acct.setPIN("30001");
        AuthorizeAccountPaymentRequestType authReq = createAcctAuthorizationRequest(
                acct, amount);
        boolean authReturn=false;

        try {
            AuthorizeAccountPaymentResponseType authResp = pas.authorizeAccountPayment(authReq);
            authReturn = checkAbstractResponse(authReq, authResp);
            if (authReturn) {
                token = authResp.getAuthorizationToken();
            }
        } catch (Exception e) {
            System.out.println(getStackTrace(e));
            System.out.println(e.getMessage());
        }
    }
    
    // teardown
    private void voidAuthorization() throws RemoteException {
        VoidAuthorizationRequestType voidReq = createVoidAuthorizationRequest(token);
        pas.voidAuthorization(voidReq);
    }
    
    // capture too much
    private boolean capture() throws RemoteException {
        CapturePaymentResponseType capResp;
        amount.setAmount("18");
        CapturePaymentRequestType capReq = createCapturePaymentRequest(token, amount, false);
        capResp = pas.capturePayment(capReq);
        return checkAbstractResponse(capReq, capResp);
    }

    
    public boolean checkCaptureTooMuch() throws RemoteException {
        boolean testResult = true;
        testResult = capture();
        if (testResult)
            return false;
        return true;
    }
    public void testCaptureTooMuchAccount() throws RemoteException {
        assertTrue(checkCaptureTooMuch());
    }   
}
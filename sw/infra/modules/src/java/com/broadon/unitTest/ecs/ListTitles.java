package com.broadon.unitTest.ecs;

import java.util.HashSet;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;

import com.broadon.wsapi.ecs.ListTitlesRequestType;
import com.broadon.wsapi.ecs.ListTitlesResponseType;
import com.broadon.wsapi.ecs.PricingCategoryType;
import com.broadon.wsapi.ecs.PricingKindType;
import com.broadon.wsapi.ecs.TitleKindType;
import com.broadon.wsapi.ecs.TitleType;

public class ListTitles extends TestCaseWrapper {
    
    // Some example titles in the DB for lab1
    final static TitleInfo[] sampleTitles = new TitleInfo[] {
        new TitleInfo("0002000100080022", 16817560, 16842752,
            "Mario Kart", DEVICE_TYPE_NC, 
            TitleKindType.Games, new HashSet(
                Arrays.asList(new PricingCategoryType[] {
                    PricingCategoryType.Subscription
                } )) ),
                
        new TitleInfo("0002000100080021", 16817560, 16842752,
            "fzero", DEVICE_TYPE_NC, 
            TitleKindType.Games, new HashSet(
                Arrays.asList(new PricingCategoryType[] {
                    PricingCategoryType.Subscription
                } )) ),
                
        new TitleInfo("0002000100080033", 8391016, 8421376,
            "NC Zero Mission", DEVICE_TYPE_NC, 
            TitleKindType.Games, new HashSet(
                Arrays.asList(new PricingCategoryType[] {
                    PricingCategoryType.Subscription,
                    PricingCategoryType.Permanent,
                    PricingCategoryType.Trial,
                } )) )
                
    };

    private static class TitleInfo {
        String titleId;
        long titleSize;
        long approxSize;
        String titleName;
        int deviceType;
        TitleKindType titleKind;
        Set pricings;
        
        static final Set ALL_PRICINGS = 
            new HashSet(
                Arrays.asList(new PricingCategoryType[] {           
                    PricingCategoryType.Permanent, 
                    PricingCategoryType.Bonus,   
                    PricingCategoryType.Rental,   
                    PricingCategoryType.Trial,   
                    PricingCategoryType.Subscription
            } ));
        static final Set PURCHASE_PRICINGS = 
            new HashSet(
                Arrays.asList(new PricingCategoryType[] {           
                    PricingCategoryType.Permanent, 
                    PricingCategoryType.Bonus,   
                    PricingCategoryType.Rental,   
                    PricingCategoryType.Trial   
            } ));
        
        private TitleInfo(String titleId, long titleSize, long approxSize, String titleName, int deviceType,
                TitleKindType titleKind, PricingCategoryType pricing)
        {
            this(titleId, titleSize, approxSize, titleName, deviceType, titleKind,
                new HashSet(Arrays.asList( new PricingCategoryType[] { pricing } )));
        }

        private TitleInfo(String titleId, long titleSize, long approxSize, String titleName, int deviceType,
                TitleKindType titleKind, Set pricings)
        {
            this.titleId = titleId;
            this.titleSize = titleSize;
            this.approxSize = approxSize;
            this.titleName = titleName;
            this.deviceType = deviceType;
            this.pricings = pricings;
            this.titleKind = titleKind;
        }   
        
        protected boolean hasPricing(PricingKindType pricingKind)
        {
            if (pricings == null) {
                return false;
            }
            if (PricingKindType.Purchase.equals(pricingKind)) {
                Iterator it = PURCHASE_PRICINGS.iterator();
                while (it.hasNext()) {
                    if (pricings.contains(it.next())) {
                        return true;
                    }
                }
                return false;
            } else if (PricingKindType.Subscription.equals(pricingKind)) {
                return pricings.contains(PricingCategoryType.Subscription);
            } else {
                return false;
            }
        }
    }
    
    public static void main(String[] args) {
        junit.textui.TestRunner.run(ListTitles.class);
    }

    public ListTitles(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }
    
    // Returns the number of times the given titles occurs in the list of titles
    public int findOccurances(TitleType[] titles, TitleInfo titleInfo)
    {
        if (titles == null) {
            return 0;
        }
        int count = 0;
        for (int i = 0; i < titles.length; i++) {
            if (titleInfo.titleId.equals(titles[i].getTitleId())) {
                assertEquals("Title " + titleInfo.titleId + " name",
                        titleInfo.titleName, titles[i].getTitleName());
                assertEquals("Title " + titleInfo.titleId + " size",
                        titleInfo.titleSize, titles[i].getTitleSize());
                assertEquals("Title " + titleInfo.titleId + " approx fs size",
                        titleInfo.approxSize, titles[i].getFsSize());
                count++;
            }
        }
        return count;
    }
    
    private boolean checkListTitles(
            int deviceType,
            int chipId,
            TitleKindType titleKind,
            PricingKindType pricingKind,
            ListTitlesResponseType expected)
        throws Throwable
    {
        boolean testResult = true;
        long deviceId = getDeviceId(deviceType, chipId);

        ListTitlesResponseType resp;
        ListTitlesRequestType req = new ListTitlesRequestType();

        initAbstractRequest(req);
        req.setDeviceId(deviceId);
        req.setTitleKind(titleKind);
        req.setPricingKind(pricingKind);

        resp = ecs.listTitles(req);
        testResult &= checkAbstractResponse(req, resp);
        if (expected != null) {
            testResult &= checkError(expected, resp);
            assertTrue("getTitles", 
                    Arrays.equals(expected.getTitles(), resp.getTitles()));          
        } else {
            assertEquals("getErrorCode", 0, resp.getErrorCode());
            assertEquals("getErrorMessage", null, resp.getErrorMessage());
            
            for (int i = 0; i < sampleTitles.length; i++) {
                TitleInfo titleInfo = sampleTitles[i];
                int occurs = findOccurances(resp.getTitles(), titleInfo);
                if (deviceType == titleInfo.deviceType &&
                    ((titleKind == null) || (titleInfo.titleKind.equals(titleKind))) &&
                    ((pricingKind == null) || (titleInfo.hasPricing(pricingKind)))) {
                    // Expect this title to be in list of titles
                    assertEquals("Title " + titleInfo.titleId + " occurs", 1, occurs);
                } else {
                    // Expect this title not to be in list of titles
                    assertEquals("Title " + titleInfo.titleId + " occurs", 0, occurs);
                }
            }           
        }

        return testResult;
    }

    private boolean checkListTitles(
            int deviceType,
            int chipId,
            TitleKindType titleKind,
            PricingKindType pricingKind)
        throws Throwable
    {
        return checkListTitles(deviceType, chipId, titleKind, pricingKind, null);
    }
    
    public void testListAllTitles()
        throws Throwable
    {
        assertTrue(checkListTitles(
            DEVICE_TYPE_NC, 1, 
            null, null));
    }
    
    public void testListAllGames()
        throws Throwable
    {
        assertTrue(checkListTitles(
                DEVICE_TYPE_NC, 1, 
                TitleKindType.Games, null));
    }

    public void testListSubscriptionGames()
        throws Throwable
    {
        assertTrue(checkListTitles(
                DEVICE_TYPE_NC, 1, 
                TitleKindType.Games,
                PricingKindType.Subscription));
        
        // TODO: BB titles not currently in ECS catalog
        //assertTrue(checkListTitles(DEVICE_TYPE_BB, 2, TitleKindType.Games,
        //      PricingCategoryType.Subscription));
    }

    public void testListPurchaseableGames()
        throws Throwable
    {
        assertTrue(checkListTitles(
                DEVICE_TYPE_NC, 1,
                TitleKindType.Games,
                PricingKindType.Purchase));

        // TODO: BB titles not currently in ECS catalog
        //assertTrue(checkListTitles(
        //      DEVICE_TYPE_BB, 1,
        //      TitleKindType.Games,
        //      PricingCategoryType.Permanent));
    }

    public void testListManuals() 
        throws Throwable
    {
        assertTrue(checkListTitles(
                DEVICE_TYPE_NC, 1,
                TitleKindType.Manuals,
                PricingKindType.Purchase));

        // TODO: BB titles not currently in ECS catalog
        //assertTrue(checkListTitles(
        //      DEVICE_TYPE_BB, 1,
        //      TitleKindType.Manuals,
        //      PricingCategoryType.Permanent));
    }
}

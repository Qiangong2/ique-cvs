package com.broadon.unitTest.pas;

import com.broadon.wsapi.pas.AuthorizeECardPaymentRequestType;
import com.broadon.wsapi.pas.AuthorizeECardPaymentResponseType;
import com.broadon.wsapi.pas.ECardRecordType;
import com.broadon.wsapi.pas.MoneyType;

public class BadECardId extends TestCaseWrapper {

    public static void main(String[] args) {
        junit.textui.TestRunner.run(BadECardId.class);
    }

    public BadECardId(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
        resetTestDataBase(ECardTests);
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    public boolean checkAuthorizeECaedPayment() {
        boolean testResult = true;
        
        AuthorizeECardPaymentRequestType authReq = new AuthorizeECardPaymentRequestType();

        MoneyType amount = new MoneyType("EUNITS", "10");
        // from batch03:    000101 0010000000 0014264647    
        String eCardSequence = "0010000000";
        
        authReq.setAmount(amount);
        
        initAbstractRequest(authReq);
        
        AuthorizeECardPaymentResponseType authResp;
        boolean authReturn=true;

        try {
            ECardRecordType eCardRecord = setECardString(eCardSequence);
            eCardRecord.setHashNumber(eCardRecord.getHashNumber().substring(0,15));
            authReq.setECardID(eCardRecord);
            authResp = pas.authorizeECardPayment(authReq);
            authReturn = checkAbstractResponse(authReq, authResp);
            if (authReturn)
                return false;
        } catch (Exception e) {
            System.out.println(getStackTrace(e));
            System.out.println(e.getMessage());
            testResult = false;
        }
        
        return testResult;
    }
    
    public void testBadECardId() {
        assertTrue(checkAuthorizeECaedPayment());
    }   
}
package com.broadon.unitTest.cps;

import com.broadon.unitTest.Configuration;
import com.broadon.wsapi.cps.PublishRequestType;

public class SubscriptionTitle extends TestCaseWrapper
{
    static boolean channelCreated = false;

    public void setUp() throws Exception
    {
        super.setUp();
        if (! channelCreated ) {
            db.removeChannel(channel);
            PublishRequestType req = initSubscription(channel);
            publish(req, 0);
            channelCreated = true;;
        }
    }

    public SubscriptionTitle(String name)
    {
        super(name);
        channel = TEST_SUBSCRIPT_CHANNEL;
        
    }

    // test publishing 2 titles with same password to a subscription channel
    public void testCreateSubTitle() throws Throwable
    {
        long tid = 100;
        db.removeTitle(tid);
        PublishRequestType req = initRequest(tid);
        publish(req, 0);
        
        tid = 101;
        db.removeTitle(tid);
        req = initRequest(tid);
        publish(req, 0);
    }
    
    
    public void testCreateSubTitleWrongPassword() throws Throwable
    {
        final long tid = 102;
        db.removeTitle(tid);
        PublishRequestType req = initRequest(tid);
        req.setEncryptionPassword("wrong password");
        publish(req, 1001);
    }
    
    public static void main(String[] args)
    {
        if (args != null && args.length > 0)
            hostName = args[0];
        Configuration.testLab1();
        junit.textui.TestRunner.run(SubscriptionTitle.class);
    }

}

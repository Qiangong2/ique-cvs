package com.broadon.unitTest.cps;

import com.broadon.unitTest.Configuration;
import com.broadon.wsapi.cps.PublishRequestType;

public class SubscriptionChannel extends TestCaseWrapper
{   
    public SubscriptionChannel(String name)
    {
        super(name);
        channel = 0;
    }

    public void testCreateSubChannel() throws Throwable
    {
        final long cid = TEST_SUBSCRIPT_CHANNEL + 0x100000000L;
        db.removeChannel(cid);
        PublishRequestType req = initSubscription(cid);
        publish(req, 0);
    }
    
    public void testCreateSubOnRegularChannel() throws Throwable
    {
        final long cid = TEST_SUBSCRIPT_CHANNEL + 0x200000000L;
        db.removeChannel(cid);
        PublishRequestType req = initRequest(cid + 1);
        publish(req, 0);
        req = initSubscription(cid);
        publish(req, 1004);
    }
    

    public static void main(String[] args)
    {
        if (args != null && args.length > 0)
            hostName = args[0];
        Configuration.testLab1();
        junit.textui.TestRunner.run(SubscriptionChannel.class);
    }

}

package com.broadon.unitTest.ias;

import com.broadon.wsapi.ias.RenewDeviceTokenRequestType;
import com.broadon.wsapi.ias.RenewDeviceTokenResponseType;

public class RenewDeviceTokenSuccess extends TestCaseWrapper {

    public static void main(String[] args) {
    }

    public RenewDeviceTokenSuccess(String name) {
	super(name);
    }

    public void setUp() throws Exception {
	super.setUp();
    }

    public void tearDown() throws Exception {
	super.tearDown();
    }

    private boolean checkRenewDeviceTokenSuccess(
		String deviceId, 
                String deviceToken,
                RenewDeviceTokenResponseType expected)
	throws Throwable
    {
	boolean testResult = true;

        RenewDeviceTokenResponseType resp;
        RenewDeviceTokenRequestType req = new RenewDeviceTokenRequestType();

        initRequest(req);
	req.setDeviceId(Long.parseLong(deviceId));
        req.setTokenId(deviceToken);
		
	resp = ias.renewDeviceToken(req);
	testResult &= checkDeviceResponse(req, resp);
		
	// Check response against expected response
	testResult &= checkError(expected, resp);
	
        if (testResult) {
            //System.out.println("Current Token: " + deviceToken + ", New Token: " + resp.getTokenId());
            
            if (!deviceToken.equals(resp.getTokenId())) {
                TestDataAdmin.setExpiredDeviceToken(deviceId, deviceToken);
                TestDataAdmin.setValidDeviceToken(deviceId, resp.getTokenId());
            } else
                testResult = false;
        }
        
        return testResult;
    }

    public void testRenewDeviceTokenSuccess()
	throws Throwable
    {
        Token vt = TestDataAdmin.getValidDeviceToken();
        
        if (vt != null) {
            // Case 1: Renew a token that has not expired
            RenewDeviceTokenResponseType expected = new RenewDeviceTokenResponseType();
            
            expected.setDeviceId(Long.parseLong(vt.id));
            expected.setErrorCode(000);
            expected.setErrorMessage("");
            assertTrue(checkRenewDeviceTokenSuccess(vt.id, vt.tokenId, expected));
        } else {
            System.out.println(getName() + " Warning: Failed to obtain a valid device token");
        }
    }
}

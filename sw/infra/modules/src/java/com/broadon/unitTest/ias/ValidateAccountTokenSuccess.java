package com.broadon.unitTest.ias;

import com.broadon.wsapi.ias.ValidateAccountTokenRequestType;
import com.broadon.wsapi.ias.ValidateAccountTokenResponseType;

public class ValidateAccountTokenSuccess extends TestCaseWrapper {

    public static void main(String[] args) {
    }

    public ValidateAccountTokenSuccess(String name) {
	super(name);
    }

    public void setUp() throws Exception {
	super.setUp();
    }

    public void tearDown() throws Exception {
	super.tearDown();
    }

    private boolean checkValidateAccountTokenSuccess(
		String accountId, 
		String accountToken,
                ValidateAccountTokenResponseType expected)
	throws Throwable
    {
	boolean testResult = true;

        ValidateAccountTokenResponseType resp;
        ValidateAccountTokenRequestType req = new ValidateAccountTokenRequestType();

        initRequest(req);
	req.setAccountId(Integer.parseInt(accountId));
	req.setTokenId(accountToken);
		
	resp = ias.validateAccountToken(req);
	testResult &= checkAccountResponse(req, resp);
		
	// Check response against expected response
	testResult &= checkError(expected, resp);

	return testResult;
    }

    public void testValidateAccountTokenSuccess()
	throws Throwable
    {
        Token vt = TestDataAdmin.getValidAccountToken();
        
        if (vt != null) {
            // Case 1: Validate a token that has not expired
            //System.out.println("1. Testing with Account Id: " + vt.id + ", Token: " + vt.tokenId);
            ValidateAccountTokenResponseType expected = new ValidateAccountTokenResponseType();
            
            expected.setAccountId(Integer.parseInt(vt.id));
            expected.setErrorCode(000);
            expected.setErrorMessage("");
            assertTrue(checkValidateAccountTokenSuccess(vt.id, vt.tokenId, expected));
        } else {
            System.out.println(getName() + " Warning: Failed to obtain a valid account token");
        }
    }
}

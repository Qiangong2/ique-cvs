package com.broadon.unitTest.cps;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.activation.DataSource;

public class ByteArrayDataSource implements DataSource
{
    private ByteArrayOutputStream buffer;
    
    public ByteArrayDataSource(byte[] buf) throws IOException {
        buffer = new ByteArrayOutputStream();
        buffer.write(buf);
    }

    public String getContentType()
    {
        return "application/octet-stream";
    }

    public InputStream getInputStream() throws IOException
    {
        return new ByteArrayInputStream(buffer.toByteArray());
    }

    public String getName()
    {
        return "Internal buffer";
    }

    public OutputStream getOutputStream() throws IOException
    {
        return null;
    }
}

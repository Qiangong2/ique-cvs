package com.broadon.unitTest.cps;

import com.broadon.unitTest.Configuration;
import com.broadon.wsapi.cps.ActionType;
import com.broadon.wsapi.cps.PublishRequestType;

public class CommonTicket extends TestCaseWrapper
{

    public CommonTicket(String name) {
        super(name);
    }

    
    public void testEnableCommonTicket() throws Throwable
    {
        final long tid = 201;
        removeTitle(tid);
        PublishRequestType req = initRequest(tid);
        req.setAllowCommonTicket(Boolean.TRUE);
        publish(req, 0);
        int attr = db.getETKMAttributes(channel + tid);
        assertEquals(1, attr);
    }
    
    
    public void testUpdateCommonTicket() throws Throwable
    {
        final long tid = 202;
        removeTitle(tid);
        
        // publish a non-common ticket title
        PublishRequestType req = initRequest(tid);
        publish(req, 0);
        
        // update to allow common ticket
        req.setAllowCommonTicket(Boolean.TRUE);
        req.setAction(ActionType.Update);
        Thread.sleep(1000);
        publish(req, 0);
        assertEquals(1, db.getETKMAttributes(channel + tid));
        
        // update to clear common ticket enabling flag
        req.setAllowCommonTicket(Boolean.FALSE);
        Thread.sleep(1000);
        publish(req, 0);
        assertEquals(0, db.getETKMAttributes(channel + tid));
    }
    
    /**
     * @param args
     */
    public static void main(String[] args)
    {
        if (args != null && args.length > 0)
            hostName = args[0];
        
        Configuration.testLab1();
        junit.textui.TestRunner.run(CommonTicket.class);
    }

}

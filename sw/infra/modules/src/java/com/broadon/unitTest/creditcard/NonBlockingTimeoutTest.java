package com.broadon.unitTest.creditcard;

import java.net.MalformedURLException;
import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import com.broadon.unitTest.ResourcePool;

import net.wiivc.services.AuthTxn;
import net.wiivc.services.CreditCardFunctions;
import net.wiivc.services.InterfaceTimeoutException;
import net.wiivc.services.NonBlockingAgent;
import net.wiivc.services.PointsAppResults;
import net.wiivc.services.RefundTxn;
public class NonBlockingTimeoutTest {

    /**
     * This program connects to a creditcard server (real or simulated)
     * to test NOA interafces. It uses NonblockingAgent and will timeout.
     * The simulated server does
     * not interact with database and does not keep any persistent state.
     * @param args
     * @throws MalformedURLException 
     * @throws ServiceException 
     * @throws RemoteException 
     */
    public static void main(String[] args) throws MalformedURLException, ServiceException, RemoteException {
        System.out.println("TEST BEGINS...");
        String taxAuthUrl = ResourcePool.getVCPaymentURL();
        CreditCardFunctions agent = new NonBlockingAgent(taxAuthUrl, 1000L);
       
        // get tax
        AuthTxn txn = new AuthTxn();
        txn.setCountry("JP");
        txn.setFunctionCode("CALC_PAYMENT");
        txn.setTotalAmount("1000");
        agent.sendATxn(txn);
        AuthTxn ret = null;
        try {
            ret = agent.getATxn();
            System.out.println("Result from CACL_PAYMENT: ");
            System.out.println("result= " + ret.getResult());
            System.out.println("tax= " + ret.getTotalTax());
            System.out.println("total= " + ret.getTotalPaid());
        } catch (InterfaceTimeoutException e) {
            System.out.println("Exception: " + e.getMessage());
        }
        
        // get auth
        txn.setFunctionCode("AUTH_PAYMENT");
        agent.sendATxn(txn);
        try {
            ret = agent.getATxn();
            System.out.println("Result from AUTH_PAYMENT: ");
            System.out.println("result= " + ret.getResult());
        } catch (InterfaceTimeoutException e) {
            System.out.println("Exception: " + e.getMessage());
        }
        // points applied
        String pointsApplyUrl = ResourcePool.getPointsConfirmationURL();
        CreditCardFunctions agent2 = new NonBlockingAgent(pointsApplyUrl, 1000L);
        String transIds[] = new String[] {"123", "abc", "789"};
        agent2.applyPoints(transIds);
        PointsAppResults[] results = agent2.getApplyPoints();
        for (int i=0; i<results.length; i++) {
            if (results[i].getAppliedResult().equalsIgnoreCase("SUCCESS"))
                System.out.println("Points Applied Confirmed for: " + results[i].getTransId());
            else
                System.out.println("Points Applied Failed for: " + results[i].getTransId());
        }
        // refund
        String vcRefundUrl = ResourcePool.getVCRefundURL();
        CreditCardFunctions agent3 = new NonBlockingAgent(vcRefundUrl, 1000L);
        RefundTxn refundTxn = new RefundTxn();
        refundTxn.setTransId("xyz");
        agent3.doRefund(refundTxn);
        String refundReturn = agent3.getDoRefund();
        System.out.println(refundReturn);
    }

}

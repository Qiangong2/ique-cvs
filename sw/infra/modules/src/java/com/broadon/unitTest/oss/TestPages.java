package com.broadon.unitTest.oss;

import java.util.HashMap;
import java.util.Map;

import com.meterware.httpunit.*;

public class TestPages extends TestCaseWrapper {

    private final static int MAX_DEPTH = 5;
    
    Map visitedPages;

    public static void main(String[] args) {
        junit.textui.TestRunner.run(TestPages.class);
    }

    public TestPages(String name) {
        super(name);
        visitedPages = new HashMap();
    }

    public void setUp() throws Exception {
        super.setUp();
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    public void followLinks(WebResponse res, int depth)
        throws Exception
    {
        if (depth >= MAX_DEPTH) return;
        
        System.out.println("visitng " + res.getURL());
        visitedPages.put(res.getURL(), res);
        
        WebLink[] links = res.getLinks();
        if (links == null) return;
        
         for (int i = 0; i < links.length; i++) {
            WebLink link = links[i];
            if (visitedPages.containsKey(link.getURLString())) {
                System.out.println("already visited " + link.getURLString());
            } else {
                System.out.println("clicking on " + link.getURLString());
                WebResponse newRes = link.click();
                followLinks(newRes, depth+1);
            }
        }
        
    }
    
    public void testPageLinks()
        throws Exception
    {
        HttpUnitOptions.setExceptionsThrownOnScriptError(false);
        String url = getUrlPrefix() + "Home.jsp";
        final WebConversation wc = new WebConversation();
        final WebRequest req = new GetMethodWebRequest(url);
        final WebResponse res = wc.getResponse(req);
        followLinks(res, 0);
    }
    
}

package com.broadon.unitTest.ecs;

import org.apache.axis.types.URI;
import org.apache.axis.types.URI.MalformedURIException;

import com.broadon.wsapi.ecs.GetSystemUpdateRequestType;
import com.broadon.wsapi.ecs.GetSystemUpdateResponseType;
import com.broadon.wsapi.ecs.TitleVersionType;

public class GetSystemUpdate extends TestCaseWrapper {

    public static void main(String[] args) {
        junit.textui.TestRunner.run(GetSystemUpdate.class);
    }

    public GetSystemUpdate(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    public boolean checkGetSystemUpdate(long deviceId, GetSystemUpdateResponseType expected)
        throws Throwable
    {
        boolean testResult = true;
        GetSystemUpdateResponseType resp;
        GetSystemUpdateRequestType req = new GetSystemUpdateRequestType();

        initAbstractRequest(req);
        req.setRegionId(REGION_TEST);
        req.setCountryCode(COUNTRY_US);
        req.setDeviceId(deviceId);
        req.setAuditData("some audit data (format to be defined)");
        req.setAttribute(new Integer(1));
        req.setTitleVersion(new TitleVersionType[]{ 
                new TitleVersionType(testTitles[0], 0, null),
                new TitleVersionType(testTitles[1], 0, null)});

        resp = ecs.getSystemUpdate(req);
        testResult &= checkAbstractResponse(req, resp);

        // Check response against expected
        testResult &= checkError(expected, resp);

        testResult &= checkTitleVersion("getTitleVersion", expected.getTitleVersion()[0], resp.getTitleVersion()[0]);
        testResult &= checkTitleVersion("getTitleVersion", expected.getTitleVersion()[1], resp.getTitleVersion()[1]);
        testResult &= checkTitleVersion("getTitleVersion", expected.getTitleVersion()[2], resp.getTitleVersion()[2]);
        // Don't check content prefix url
        //assertEquals("getContentPrefixURL",
        //        expected.getContentPrefixURL(), resp.getContentPrefixURL());

        return testResult;
    }
    
    public void initExpected(GetSystemUpdateResponseType expected)
    {
        try {
            expected.setContentPrefixURL(new URI("http://ccs:16983/ccs/download"));
        } catch (MalformedURIException e) {
            fail("Error initializing GetSystemUpdateResponseType: " + e.getLocalizedMessage());
        }
    }
    
    public void testGetSystemUpdate()
        throws Throwable
    {
        GetSystemUpdateResponseType expected = new GetSystemUpdateResponseType();
        initExpected(expected);
        
        long deviceId = testDevice;
        TitleVersionType kernel = new TitleVersionType();
        kernel.setTitleId(testTitles[0]);
        kernel.setVersion(-1); // Don't check version
        //kernel.setVersion(8);
        
        TitleVersionType viewer = new TitleVersionType();
        viewer.setTitleId(testTitles[1]);
        viewer.setVersion(-1); // Don't check version
        //viewer.setVersion(29);

        TitleVersionType OS = new TitleVersionType();
        OS.setTitleId(testTitles[2]);
        OS.setVersion(-1); // Don't check version
        //OS.setVersion(1);

        expected.setErrorCode(0);
        expected.setTitleVersion(new TitleVersionType[] { kernel, viewer, OS } );

        assertTrue(checkGetSystemUpdate(deviceId, expected));
    }
}

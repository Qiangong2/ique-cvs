package com.broadon.unitTest.cas;

import com.broadon.cas.TitleDetailBean;

public class TitleDetails extends TestCaseWrapper {

    public static void main(String[] args) {
        junit.textui.TestRunner.run(TitleDetails.class);
    }

    public TitleDetails(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    public void testTitleDetail()
        throws Exception
    {
        TitleDetailBean bean = new TitleDetailBean();
        // Make sure query okay
        bean.setCountryCode("US");
        bean.setLanguageCode("en");
        bean.setRegionCode("US");
        bean.setTitleId("0002000100080033");
        bean.init(getDataSource());
        bean.list();

        // Bad title
        bean.setTitleId("00020001000800");
        bean.list();
    }
    
}

package com.broadon.unitTest.cps;

import java.io.IOException;
import java.util.Random;

import javax.activation.DataHandler;

import com.broadon.unitTest.Configuration;
import com.broadon.wsapi.cps.ContentAttributeType;
import com.broadon.wsapi.cps.PublishRequestType;
import com.broadon.wsapi.cps.PublishResponseType;

public class Attachments extends TestCaseWrapper
{
    public Attachments(String name) {
        super(name);
    }
   
    class Request {
        PublishRequestType req;
        DataHandler[] attachments;
        
        Request(DataHandler[] attachments, PublishRequestType req) {
            this.attachments = attachments;
            this.req = req;
        }
    }
    
    
    public Request multiContentRequests(long tid, int num) throws IOException 
    {
        PublishRequestType req = initRequest(tid);
        ContentAttributeType content = req.getContents(0);
        ContentAttributeType[] contents = new ContentAttributeType[num];
        DataHandler[] attachments = new DataHandler[num];
        Random rand = new Random();
        byte[] buf = new byte[64];
        for (int i = 0; i < num; ++i) {
            contents[i] = content;
            contents[i].setContentObjectName(content.getContentObjectName() + ' ' + i);
            rand.nextBytes(buf);
            attachments[i] = new DataHandler(new ByteArrayDataSource(buf));
        }
        req.setContents(contents);
        return new Request(attachments, req);
    }
    
    public void testMissingAttachment() throws Throwable
    {
        final long tid = 301;
        removeTitle(tid);
        PublishRequestType req = initRequest(tid);
        publish(req, null, 1004);
    }
    
    public void testMultipleAttachments() throws Throwable
    {
        final long tid = 302;
        removeTitle(tid);
        Request req = multiContentRequests(tid, 2);
        PublishResponseType resp = publish(req.req, req.attachments, 0);
        // kludge: instead of parsing the TMD, we just check its length, which
        // varies according to the number of contents.
        assertEquals(556, resp.getTitleMetaData().length);
    }
    
    
    public void testLargeAttachments() throws Throwable
    {
        final long tid = 303;
        removeTitle(tid);
        PublishRequestType req = initRequest(tid);
        DataHandler[] attachments = new DataHandler[1];
        attachments[0] = new DataHandler(new ArbitraryDataSource(1024*1024));
        publish(req, attachments, 0);
    }
    
    /**
     * @param args
     */
    public static void main(String[] args)
    {
        if (args != null && args.length > 0)
            hostName = args[0];
        
        Configuration.testLab1();
        junit.textui.TestRunner.run(Attachments.class);
    }

}

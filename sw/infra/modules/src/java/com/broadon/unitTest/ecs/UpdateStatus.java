package com.broadon.unitTest.ecs;

import com.broadon.wsapi.ecs.ConsumptionType;
import com.broadon.wsapi.ecs.UpdateStatusRequestType;
import com.broadon.wsapi.ecs.UpdateStatusResponseType;

public class UpdateStatus extends TestCaseWrapper {

    public static void main(String[] args) {
        junit.textui.TestRunner.run(UpdateStatus.class);
    }

    public UpdateStatus(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    private boolean checkUpdateStatus(
            UpdateStatusRequestType req,
            UpdateStatusResponseType expected)
        throws Throwable
    {
        boolean testResult = true;

        UpdateStatusResponseType resp;

        resp = ecs.updateStatus(req);
        testResult &= checkAbstractResponse(req, resp);
            
        // Check response against expected response
        testResult &= checkError(expected, resp);

        return testResult;
    }

    public void testUpdateStatus()
        throws Throwable
    {
        String titleId = "888888";
        UpdateStatusRequestType req = new UpdateStatusRequestType();
        UpdateStatusResponseType expected = new UpdateStatusResponseType();
        
        initAbstractRequest(req);
        req.setConsumptions(
                new ConsumptionType[] { 
                        new ConsumptionType( titleId, 10 ) } );
        
        assertTrue(checkUpdateStatus(req, expected));
    }

}

package com.broadon.unitTest.ias;

import com.broadon.wsapi.ias.ValidateDeviceTokenRequestType;
import com.broadon.wsapi.ias.ValidateDeviceTokenResponseType;

public class ValidateDeviceTokenSuccess extends TestCaseWrapper {

    public static void main(String[] args) {
    }

    public ValidateDeviceTokenSuccess(String name) {
	super(name);
    }

    public void setUp() throws Exception {
	super.setUp();
    }

    public void tearDown() throws Exception {
	super.tearDown();
    }

    private boolean checkValidateDeviceTokenSuccess(
		String deviceId,
                String deviceToken,
                ValidateDeviceTokenResponseType expected)
	throws Throwable
    {
	boolean testResult = true;

        ValidateDeviceTokenResponseType resp;
        ValidateDeviceTokenRequestType req = new ValidateDeviceTokenRequestType();

        initRequest(req);
	req.setDeviceId(Long.parseLong(deviceId));
        req.setTokenId(deviceToken);
		
	resp = ias.validateDeviceToken(req);
	testResult &= checkDeviceResponse(req, resp);
		
	// Check response against expected response
	testResult &= checkError(expected, resp);

	return testResult;
    }

    public void testValidateDeviceTokenSuccess()
	throws Throwable
    {
        Token vt = TestDataAdmin.getValidDeviceToken();
        
        if (vt != null) {
            // Case 1: Validate a token that has not expired
            //System.out.println("1. Testing with Device Id: " + vt.id + ", Token: " + vt.tokenId);
            ValidateDeviceTokenResponseType expected = new ValidateDeviceTokenResponseType();
            
            expected.setDeviceId(Long.parseLong(vt.id));
            expected.setErrorCode(000);
            expected.setErrorMessage("");
            assertTrue(checkValidateDeviceTokenSuccess(vt.id, vt.tokenId, expected));
        } else {
            System.out.println(getName() + " Warning: Failed to obtain a valid device token");
        }
    }
}

/*
 * BMS TestSuite:  Add new BMS TestCase here to this class.
 */
package com.broadon.unitTest.bms;

import com.broadon.unitTest.Configuration;

import junit.framework.TestSuite;
import junit.framework.Test;

public class BmsTestSuite
{
    public static void main(String[] args) {
        if (args != null && args.length > 0) {
            TestCaseWrapper.hostName = args[0];
        }
        Configuration.testLab1();
        junit.textui.TestRunner.run(BmsTestSuite.suite());
    }
    
    public static Test suite() {
        TestSuite suite = new TestSuite("Test for com.broadon.unitTest.bms");
        //$JUnit-BEGIN$
        suite.addTestSuite(Login.class);
        suite.addTestSuite(TitlesList.class);
        //$JUnit-END$
        return suite;
    }
}

/*
 * NUS TestSuite:  Add new NUS TestCase here to this class.
 */
package com.broadon.unitTest.nus;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.broadon.unitTest.Configuration;

public class NusTestSuite {

    public static void main(String[] args) {
        junit.textui.TestRunner.run(NusTestSuite.suite());
    }

    public static Test suite() {
        TestSuite suite = new TestSuite("Test for com.broadon.unitTest.nus");
        //$JUnit-BEGIN$

        suite.addTestSuite(GetSystemUpdate.class);
        //$JUnit-END$
        return suite;
    }

}

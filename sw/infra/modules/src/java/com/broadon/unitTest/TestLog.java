/*
 * Test Report for the nightly regression test.
 * TODO: disable test reporting during stress testing.
 */
package com.broadon.unitTest;

public class TestLog {

	// Set "output" to false when running stress testing
	static boolean output = true;

	public static void setOutput(boolean val) {
		output = val;
	}

	public static void report(String module, String testname, boolean testResult) {
		report(module, testname, testResult, 0);
	}
	
	public static void report(String module, String testname, boolean testResult, int elapsed) {
		if (output) {
			System.out.print("*** " + module + " " + testname + " "
					+ "TEST " + (testResult ? "PASSED" : "FAILED"));
			if (elapsed > 0) {
				System.out.println(" ELAPSED " + Integer.toString(elapsed));
			}
			System.out.println();
		}
	}
}

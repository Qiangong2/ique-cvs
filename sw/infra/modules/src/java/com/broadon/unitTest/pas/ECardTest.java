package com.broadon.unitTest.pas;

import java.math.BigDecimal;

import com.broadon.wsapi.pas.AuthorizeECardPaymentRequestType;
import com.broadon.wsapi.pas.AuthorizeECardPaymentResponseType;
import com.broadon.wsapi.pas.BalanceType;
import com.broadon.wsapi.pas.CapturePaymentRequestType;
import com.broadon.wsapi.pas.CapturePaymentResponseType;
import com.broadon.wsapi.pas.RefundPaymentRequestType;
import com.broadon.wsapi.pas.RefundPaymentResponseType;
import com.broadon.wsapi.pas.VoidAuthorizationRequestType;
import com.broadon.wsapi.pas.VoidAuthorizationResponseType;
import com.broadon.wsapi.pas.PasTokenType;
import com.broadon.wsapi.pas.MoneyType;

public class ECardTest extends TestCaseWrapper {
    /**
     * misc. tests for pas eacrd related services:
     * create an ecard
     * check balance
     * authorize payment
     * check balance
     * void auth
     * check balance
     * authorize payment
     * cpature payment
     * check balance
     * refund payment
     * check balance
     * get ecard account history
     * 
     */
    String[] ecards;
    public static void main(String[] args) {
        junit.textui.TestRunner.run(ECardTest.class);
    }

    public ECardTest(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
        // ecardType 11 is 1000 POINTS and reusable
        createECardType(11);
        ecards = createECards(11, 1, "JP");
    }

    public void tearDown() throws Exception {
        super.tearDown();
        removeECardType(11);
    }

    // This  method checks if the total and partial difference is diff
    private boolean checkBalance(BalanceType total, BalanceType partial, MoneyType diff) {
        BigDecimal totalAmount = new BigDecimal(total.getAvailable().getAmount());
        BigDecimal partialAmount = new BigDecimal(partial.getAvailable().getAmount());
        BigDecimal diffAmount = new BigDecimal(diff.getAmount());
        if (totalAmount.subtract(partialAmount).compareTo(diffAmount) == 0)
            return true;
        else {
            System.out.println("total: " + totalAmount);
            System.out.println("partial: " + partialAmount);
            System.out.println("diff: " + diffAmount);
            return false;
        }
    }
    
    public boolean checkECardTest() {
        String eCardNumber = ecards[0];
        MoneyType amount = new MoneyType("POINTS", "150");
        boolean testResult = true;
        try {
            // check balance
            BalanceType initBalance = checkECardBalance(eCardNumber);
            if (initBalance != null) {
                System.out.println("checkECardBalance passed.");
            } else {
                return false;
            }
            // authorize payment
            AuthorizeECardPaymentRequestType authReq = createAuthorizationRequest(
                    eCardNumber, amount);
            AuthorizeECardPaymentResponseType authResp = pas.authorizeECardPayment(authReq);
            if (checkAbstractResponse(authReq, authResp)) {
                System.out.println("authorizeECardPayment passed.");
            } else {
                return false;
            }
            PasTokenType token = authResp.getAuthorizationToken();
            // check balance
            BalanceType newBalance = checkECardBalance(eCardNumber);
            if (checkBalance(initBalance, newBalance, amount))
                System.out.println("new balance is correct.");
            else 
                return false;
            // void auth
            VoidAuthorizationRequestType voidReq = createVoidAuthorizationRequest(token);
            VoidAuthorizationResponseType voidResp = pas.voidAuthorization(voidReq);
            if (checkAbstractResponse(voidReq, voidResp)) {
                System.out.println("voidAuthorization passed.");
            } else {
                return false;
            }
            // check balance
            initBalance = checkECardBalance(eCardNumber);
            if (checkBalance(initBalance, newBalance, amount))
                System.out.println("new balance is correct.");
            else 
                return false;
            // authorize payment
            authResp = pas.authorizeECardPayment(authReq);
            if (checkAbstractResponse(authReq, authResp)) {
                System.out.println("authorizeECardPayment passed.");
            } else {
                return false;
            }
            token = authResp.getAuthorizationToken();
            // cpature payment
            CapturePaymentRequestType capReq = createCapturePaymentRequest(token, amount, false);
            CapturePaymentResponseType capResp = pas.capturePayment(capReq);
            if (checkAbstractResponse(capReq, capResp)) {
                System.out.println("capturePayment passed.");
            } else {
                return false;
            }
            // check balance
            newBalance = checkECardBalance(eCardNumber);
            if (checkBalance(initBalance, newBalance, amount))
                System.out.println("new balance is correct.");
            else 
                return false;
            // refund payment
            RefundPaymentRequestType refReq = createRefundPaymentRequest(token, amount);
            RefundPaymentResponseType refResp = pas.refundPayment(refReq);
            if (checkAbstractResponse(refReq, refResp)) {
                System.out.println("refundPayment passed.");
            } else {
                return false;
            }
            // check balance
            initBalance = checkECardBalance(eCardNumber);
            if (checkBalance(initBalance, newBalance, amount))
                System.out.println("new balance is correct.");
            else 
                return false;
            // get ecard account history
        } catch (Exception e) {
            System.out.println(getStackTrace(e));
            System.out.println(e.getMessage());
            testResult = false;
        }
        return testResult;
    }
    
    public void testMiscECardTest() {
        assertTrue(checkECardTest());
    }   
}

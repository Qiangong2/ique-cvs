#!/bin/sh

export BROADON_HOME=/opt/broadon
export BROADON_PKG_HOME=${BROADON_HOME}/pkgs
export JAVA_HOME=${BROADON_PKG_HOME}/jre

export WSLIB_HOME=${BROADON_PKG_HOME}/wslib
export JAVALIB_HOME=${BROADON_PKG_HOME}/javalib

export CLASSPATH=${CLASSPATH}:${WSLIB_HOME}/jar/wstest.jar
export CLASSPATH=${CLASSPATH}:${WSLIB_HOME}/jar/wsclient.jar

export CLASSPATH=${CLASSPATH}:${WSLIB_HOME}/jar/activation.jar
export CLASSPATH=${CLASSPATH}:${WSLIB_HOME}/jar/axis.jar
export CLASSPATH=${CLASSPATH}:${WSLIB_HOME}/jar/axis-schema.jar
export CLASSPATH=${CLASSPATH}:${WSLIB_HOME}/jar/saaj.jar
export CLASSPATH=${CLASSPATH}:${WSLIB_HOME}/jar/jaxrpc.jar
export CLASSPATH=${CLASSPATH}:${WSLIB_HOME}/jar/log4j-1.2.8.jar
export CLASSPATH=${CLASSPATH}:${WSLIB_HOME}/jar/commons-discovery-0.2.jar
export CLASSPATH=${CLASSPATH}:${WSLIB_HOME}/jar/commons-logging-1.0.4.jar
export CLASSPATH=${CLASSPATH}:${WSLIB_HOME}/jar/wsdl4j-1.5.1.jar
export PATH=${JAVA_HOME}/bin:${PATH}

exec java com.broadon.unitTest.pcis.PCGen $*

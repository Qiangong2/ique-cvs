package com.broadon.unitTest.pas;

import java.math.BigDecimal;

import com.broadon.unitTest.ResourcePool;
import com.broadon.util.UUID;
import com.broadon.wsapi.pas.AuthorizeCreditCardPaymentRequestType;
import com.broadon.wsapi.pas.AuthorizeCreditCardPaymentResponseType;
import com.broadon.wsapi.pas.BalanceAccountType;
import com.broadon.wsapi.pas.BalanceType;
import com.broadon.wsapi.pas.CaptureCreditCardToAccountRequestType;
import com.broadon.wsapi.pas.CaptureCreditCardToAccountResponseType;
import com.broadon.wsapi.pas.MoneyType;
import com.broadon.wsapi.pas.PasTokenType;

import net.wiivc.services.AuthTxn;
import net.wiivc.services.CreditCardFunctions;
import net.wiivc.services.InvoiceModifier;
import net.wiivc.services.JAXBAgent;
import net.wiivc.services.NonBlockingAgent;
import net.wiivc.services.PointsAppResults;
import net.wiivc.services.RefundTxn;
public class CreditCardTest extends TestCaseWrapper{
    /**
     * misc. tests for credit card related services:
     * call ccsimu to get total_paid
     * create acct
     * check acct balance
     * call JAXBAgent to convert authTxn to a String in xml format
     * authorize payment from credit card
     * cpature payment to account
     * check acct balance
     */
static final boolean doPointsApp = false;
static final boolean doRefund = false;

AuthTxn noaTxn = null;
AuthTxn noaCPRet = null;
AuthTxn noaAuthRet = null;
BalanceAccountType acct;

    public static void main(String[] args) {
        junit.textui.TestRunner.run(CreditCardTest.class);
    }
    
    public CreditCardTest(String name) {
        super(name);
    }
    
    public void testMiscCreditCardTest() {
        assertTrue(checkCreditCardTest());
    } 
    
    public boolean checkCreditCardTest() {
        boolean testResult = true;
        try {
            if (noaCPRet.getResult().startsWith("ERROR")) {
                System.out.println("setup failed");
                testResult = false;
            } else {
                // check balance
                BalanceType initBalance = checkAccountBalance(acct);
                if (initBalance != null) {
                    System.out.println("checkECardBalance passed.");
                } else {
                    return false;
                }
                // use noaCPRet to prepare auth request to pas
                System.out.println("setup complete, get JAXBAgent.");
                JAXBAgent xmlAgent = new JAXBAgent();
                String noaCPStr = xmlAgent.getXMLString(noaCPRet);
                System.out.println("converted noaCPReturn to String, get amount.");
                MoneyType amount = getMoney(noaCPRet);
                String creditCardNumber = getCreditCardNumber(noaCPRet);
                System.out.println("prepare and send authorization request.");
                AuthorizeCreditCardPaymentRequestType authReq = 
                    createCreditCardAuthorizationRequest(
                        amount,
                        creditCardNumber,
                        noaCPStr);
                // get auth
                AuthorizeCreditCardPaymentResponseType authResp;
                authResp = pas.authorizeCreditCardPayment(authReq);
                if (checkAbstractResponse(authReq, authResp)) {
                    System.out.println("authorization completes, deposit to an account.");
                } else {
                    return false;
                }
                String noaAuthStr = authResp.getNOACreditCardResp();
                noaAuthRet = xmlAgent.getAuthTxn(noaAuthStr);
                printAuthTxn(noaAuthRet, "AUTH_PAYMENT");
                // cpature payment to account
                PasTokenType token = authResp.getAuthorizationToken();
                
                MoneyType depositAmount = new MoneyType(amount.getCurrencyType(), "34.50");
                CaptureCreditCardToAccountRequestType captureReq = 
                    createCaptureCreditCardToAccountRequest(token, acct, amount, depositAmount);
                CaptureCreditCardToAccountResponseType captureResp = 
                        pas.captureCreditCardToAccount(captureReq);
                if (checkAbstractResponse(captureReq, captureResp)) {
                    System.out.println("capturePayment passed.");
                } else {
                    return false;
                }
                // check balance
                BalanceType newBalance = checkAccountBalance(acct);
                if (checkBalance(newBalance, initBalance, depositAmount))
                    System.out.println("new balance is correct.");
                else {
                    return false;
                }
            }
        } catch (Exception e) {
            System.out.println(getStackTrace(e));
            System.out.println(e.getMessage());
            testResult = false;
        }
        return testResult;
    }
    
    private MoneyType getMoney(AuthTxn txn) {
        MoneyType amount = new MoneyType();
        String amt = noaCPRet.getTotalPaid();
        String currency = noaCPRet.getCurrency();
        amount.setAmount(amt);
        amount.setCurrencyType(currency);
        return amount;
    }
    
    private String getCreditCardNumber(AuthTxn txn) {
//        CreditCardType cc = new CreditCardType();
//        cc.setCreditCardKind(CreditCardKindType.fromString(txn.getCardType()));
//        cc.setCreditCardNumber(txn.getCardNum());
//        cc.setCreditCardOwner("");  // not in AuthTxn
//        cc.setCVV2(txn.getCardVfyVal());
//        cc.setExpMonth(Integer.parseInt(txn.getCardExpMM()));
//        cc.setExpYear(Integer.parseInt(txn.getCardExpYY()));
        return txn.getCardNum();
    }
    /**
     * setUp connects to a creditcard server (real or simulated)
     * and call CALC_PAYMENT to get the modified payment.
     */
    public void setUp() throws Exception {
        super.setUp();
        resetTestDataBase(AccountTests);
        acct = createAccount("30001", "30001");
        // use localhost:7199 and go through tcpmon
        String vcPaymentUrlStr = ResourcePool.getVCPaymentURL();
        CreditCardFunctions agent = new NonBlockingAgent(vcPaymentUrlStr, 5000L);
        // get tax
        noaTxn = createAuthTxn();  // prepare the initial txn object for tax info.
        agent.sendATxn(noaTxn);
        noaCPRet = agent.getATxn();
        printAuthTxn(noaCPRet, "CACL_PAYMENT");
    }
    public void tearDown() throws Exception {
        if (doPointsApp) {
            // points applied
            String pointsConfirmationUrlStr = ResourcePool.getPointsConfirmationURL();
                CreditCardFunctions pointsApplyAgent = new NonBlockingAgent(pointsConfirmationUrlStr, 5000L);
                String transIds[] = getTransIds(noaAuthRet);
                pointsApplyAgent.applyPoints(transIds);
                PointsAppResults[] results = pointsApplyAgent.getApplyPoints();
                printPointsAppResults(results);
        }
        // refund
        if (doRefund) {
            String vcRefundUrlStr = ResourcePool.getVCRefundURL();
                RefundTxn refundTxn = new RefundTxn();
                refundTxn.setTransId("xyz");
                CreditCardFunctions vcRefundAgent = new NonBlockingAgent(vcRefundUrlStr, 5000L);
                vcRefundAgent.doRefund(refundTxn);
                String refundReturn = vcRefundAgent.getDoRefund();
                System.out.println(refundReturn);
            }
            super.tearDown();
    }
    // This  method checks if the total and partial difference is diff
    private boolean checkBalance(BalanceType total, BalanceType partial, MoneyType diff) {
        BigDecimal totalAmount = new BigDecimal(total.getAvailable().getAmount());
        BigDecimal partialAmount = new BigDecimal(partial.getAvailable().getAmount());
        BigDecimal diffAmount = new BigDecimal(diff.getAmount());
        if (totalAmount.subtract(partialAmount).compareTo(diffAmount) == 0)
            return true;
        else {
            System.out.println("total: " + totalAmount);
            System.out.println("partial: " + partialAmount);
            System.out.println("diff: " + diffAmount);
            return false;
        }
    }
    // set attributes for calling the 
    public static AuthTxn createAuthTxn() {
        AuthTxn txn = new AuthTxn();
        // attributes in alphabetic order, explicite lis all attributes
        txn.setAccountId("588");
        //txn.setApproveDate("2006.08.10");
        txn.setBillingCity("San Jose");         // ??
        txn.setBillingCounty("Santa Clara");    // ??
        txn.setBillingPostal("95130");
        txn.setBillingState("CA");
        txn.setCardExpMM("12");
        txn.setCardExpYY("08");
        txn.setCardNum("1234567890");
        txn.setCardType("V");
        txn.setCardVfyVal("123");
        txn.setClientIP("1.2.3.4");
        txn.setCountry("US");
        txn.setCurrency("USD");
        txn.setDeviceId("001");
        txn.setFunctionCode("CALC_PAYMENT");    // calculate payment
        //txn.setInvMod(setIMs());
        txn.setLanguage("Eng");
        txn.setPaymentMethodId("CC");
        txn.setPaymentType("CC");
        txn.setRefillPoints("1000");
        txn.setRegion("NOA");
        //txn.setResult("S");
        //txn.setSessionId("1234");
        txn.setTotalAmount("1000");
        //txn.setTotalPaid("1200");
        //txn.setTotalTax("123");
        txn.setTransDate("06.08.11");
        //txn.setTransId("abc");
        txn.setTransType("A");
        //txn.setVcMessage("Test");
        return txn;
    }
    
    public static void preparePaymentAuth(AuthTxn txn) {
        txn.setFunctionCode("AUTH_PAYMENT");
        UUID uuid = UUID.randomUUID();          // construct a pax transaction ID
        String txId = uuid.toBase64();
        txn.setTransId(txId);
    }
    // print return values
    public static void printAuthTxn(AuthTxn txn, String function) {
        System.out.println("Result from: " + function);
        if (txn.getResult().startsWith("ERROR")) {
            System.out.println(txn.getResult());
        } else {
            System.out.println("sessionId= " + txn.getSessionId());
            System.out.println("result= " + txn.getResult());
            System.out.println("approveDate: " + txn.getApproveDate());
            System.out.println("tax= " + txn.getTotalTax());
            System.out.println("total= " + txn.getTotalPaid());
            printInvoiceModifiers(txn.getInvMod());
            System.out.println("VCMessage= " + txn.getVcMessage());
            printVcMsgCodes(txn.getVcMsgCode());
        }
    }
    private static void printInvoiceModifiers(InvoiceModifier[] ims) {
        if (ims == null)
            return;
        System.out.println("InvoiceModifiers: ");
        for (int i=0; i<ims.length; i++) {
            System.out.println("    " + ims[i].getAmount());
            System.out.println("    " + ims[i].getDesc());
            System.out.println("    " + ims[i].getLineItemType());
        }
    }
    
    private static void printVcMsgCodes(String[] msgs) {
        if (msgs == null)
            return;
        System.out.println("VCMessage codes: ");
        for (int i=0; i<msgs.length; i++) {
            System.out.println("    " + msgs[i]);
        }
    }
    public static String[] getTransIds(AuthTxn txn) {
        String[] ret = new String[1];
        ret[0] = txn.getTransId();
        return ret;
    }
    
    public static void printPointsAppResults(PointsAppResults[] results) {
        for (int i=0; i<results.length; i++) {
            if (results[i].getAppliedResult().equalsIgnoreCase("SUCCESS"))
                System.out.println("Points Applied Confirmed for: " + results[i].getTransId());
            else
                System.out.println("Points Applied Failed for: " + results[i].getTransId());
        }
    }
}

/*
 * CPS TestSuite:  Add new CPS TestCase here to this class.
 */
package com.broadon.unitTest.cps;

import com.broadon.unitTest.Configuration;

import junit.framework.Test;
import junit.framework.TestSuite;

public class CpsTestSuite {

	public static void main(String[] args) {
            if (args != null && args.length > 0) {
                TestCaseWrapper.hostName = args[0];
            }
            Configuration.testLab1();
            junit.textui.TestRunner.run(CpsTestSuite.suite());
	}

	public static Test suite() {
		TestSuite suite = new TestSuite("Test for com.broadon.unitTest.cps");
		//$JUnit-BEGIN$
		suite.addTestSuite(CreateRegularTitle.class);
                suite.addTestSuite(SubscriptionChannel.class);
                suite.addTestSuite(SubscriptionTitle.class);
                suite.addTestSuite(CommonTicket.class);
                suite.addTestSuite(Attachments.class);
		//$JUnit-END$
		return suite;
	}

}

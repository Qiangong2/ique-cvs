package com.broadon.unitTest.ecs;

import com.broadon.wsapi.ecs.AccountPaymentType;
import com.broadon.wsapi.ecs.PriceType;
import com.broadon.wsapi.ecs.PurchasePointsRequestType;
import com.broadon.wsapi.ecs.PurchasePointsResponseType;

public class PurchasePoints extends TestCaseWrapper {

    public static void main(String[] args) {
        junit.textui.TestRunner.run(PurchasePoints.class);
    }

    public PurchasePoints(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
        createAccount("30002");
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    private boolean checkPurchasePoints(
            PurchasePointsRequestType req,
            PurchasePointsResponseType expected)
        throws Throwable
    {
        boolean testResult = true;

        PurchasePointsResponseType resp;

        resp = ecs.purchasePoints(req);
        testResult &= checkAbstractResponse(req, resp);
            
        // Check response against expected response
        testResult &= checkError(expected, resp);
        
        assertEquals("getTotalPoints", expected.getTotalPoints(), resp.getTotalPoints());

        return testResult;
    }

    /**
     * Initializes request to purchase points using prepaid card
     * @param req
     */
    protected void initPrepaidCardPurchaseRequest(PurchasePointsRequestType req) {
        initAbstractRequest(req);
        req.setDeviceId(validRV1);
        makePointsECard();
        req.setPayment(validPrepaidCard);
        req.setCountryCode(testCountry);
        req.setItemId(1000);
        req.setPrice(new PriceType("1000", "POINTS"));
        req.setPoints(1000);
        req.setAccount(new AccountPaymentType("30002", "30002"));
    }
    
    /**
     * Initializes request to purchase points using credit card
     * @param req
     */
    protected void initCreditCardPurchaseRequest(PurchasePointsRequestType req) {
        initAbstractRequest(req);
        req.setDeviceId(validRV1);
        req.setPayment(validCreditCard);
        req.setCountryCode(COUNTRY_US);
        req.setItemId(144);
        req.setPrice(new PriceType("6", "USD"));
        req.setPoints(60000);
        // TODO: Set account payment to be credit card
        req.setAccount(new AccountPaymentType("30002", "30002"));
    }
    
    public void testPurchasePointsBadItemId() throws Throwable {
        PurchasePointsRequestType req = new PurchasePointsRequestType();
        PurchasePointsResponseType expected = new PurchasePointsResponseType();
        
        initPrepaidCardPurchaseRequest(req);
        req.setItemId(300);
        
        expected.setErrorCode(601);
        expected.setErrorMessage(".*Invalid item.*");
        
        assertTrue(checkPurchasePoints(req, expected));
    }

    public void testPurchasePointsBadDeviceType() throws Throwable {
        PurchasePointsRequestType req = new PurchasePointsRequestType();
        PurchasePointsResponseType expected = new PurchasePointsResponseType();
        
        initPrepaidCardPurchaseRequest(req);
        req.setVirtualDeviceType(new Integer(5));
        
        expected.setErrorCode(604);
        expected.setErrorMessage(".*Invalid device.*");
        
        assertTrue(checkPurchasePoints(req, expected));
    }

    public void testPurchasePointsBadCountryId() throws Throwable {
        PurchasePointsRequestType req = new PurchasePointsRequestType();
        PurchasePointsResponseType expected = new PurchasePointsResponseType();
        
        initCreditCardPurchaseRequest(req);
        req.setCountryCode("ABC");
        
        expected.setErrorCode(606);
        expected.setErrorMessage(".*Invalid country.*");
        
        assertTrue(checkPurchasePoints(req, expected));
    }

    public void testPurchasePointsBadPrice() throws Throwable {
        PurchasePointsRequestType req = new PurchasePointsRequestType();
        PurchasePointsResponseType expected = new PurchasePointsResponseType();
        
        initPrepaidCardPurchaseRequest(req);
        req.setPrice(new PriceType("2", "EUNITS"));
        
        expected.setErrorCode(615);
        expected.setErrorMessage(".*Actual price is different from expected price.*");
        
        assertTrue(checkPurchasePoints(req, expected));
    }

    public void testPurchasePointsBadPoints() throws Throwable {
        PurchasePointsRequestType req = new PurchasePointsRequestType();
        PurchasePointsResponseType expected = new PurchasePointsResponseType();
        
        initPrepaidCardPurchaseRequest(req);
        req.setPoints(10000);
        
        expected.setErrorCode(614);
        expected.setErrorMessage(".*points is different.*");
        
        assertTrue(checkPurchasePoints(req, expected));
    }

    public void testPurchasePointsNoPayment()
        throws Throwable
    {
        PurchasePointsRequestType req = new PurchasePointsRequestType();
        PurchasePointsResponseType expected = new PurchasePointsResponseType();

        initPrepaidCardPurchaseRequest(req);
        req.setPayment(null);
        
        expected.setErrorCode(616);
        expected.setErrorMessage(".*No payment.*");
        
        assertTrue(checkPurchasePoints(req, expected));
    }

    public void testPurchasePointsInvalidPayment()
        throws Throwable
    {
        for (int i = 0; i < invalidPayments.length; i++) {
            PurchasePointsRequestType req = new PurchasePointsRequestType();
            PurchasePointsResponseType expected = new PurchasePointsResponseType();
    
            initPrepaidCardPurchaseRequest(req);
            req.setPayment(invalidPayments[i]);

            expected.setErrorCode(invalidPaymentErrors[i]);
            expected.setErrorMessage(".*");
            assertTrue(checkPurchasePoints(req, expected));
        }
    }

    public void testPurchasePointsPrepaidCard()
        throws Throwable
    {
        PurchasePointsRequestType req = new PurchasePointsRequestType();
        PurchasePointsResponseType expected = new PurchasePointsResponseType();
        
        expected.setTotalPoints(Long.parseLong(START_BALANCE) + 1000);
        initPrepaidCardPurchaseRequest(req);
        assertTrue(checkPurchasePoints(req, expected));
        
        // Try to purchase Points again (should get error - card already used)
        expected.setErrorCode(815);
        expected.setErrorMessage(".*Account Has Been Used Once.*");
        expected.setTotalPoints(0);
        assertTrue(checkPurchasePoints(req, expected));
    }

}

package com.broadon.unitTest.pas;

import com.broadon.wsapi.pas.AuthorizeCreditCardPaymentRequestType;
import com.broadon.wsapi.pas.AuthorizeCreditCardPaymentResponseType;
import com.broadon.wsapi.pas.BalanceAccountType;
import com.broadon.wsapi.pas.CaptureCreditCardToAccountRequestType;
import com.broadon.wsapi.pas.CaptureCreditCardToAccountResponseType;
import com.broadon.wsapi.pas.CreateAccountRequestType;
import com.broadon.wsapi.pas.CreateAccountResponseType;
import com.broadon.wsapi.pas.MoneyType;
import com.broadon.wsapi.pas.PasTokenType;

public class AuthorizeCreditCardPayment extends TestCaseWrapper {
    BalanceAccountType acct = null;
    public static void main(String[] args) {
        junit.textui.TestRunner.run(AuthorizeCreditCardPayment.class);
    }

    public AuthorizeCreditCardPayment(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
        resetTestDataBase(AccountTests);
        acct = new BalanceAccountType();
        acct.setAccountNumber("30001");
        acct.setPIN("30001"); 
        CreateAccountRequestType req = createCreateAccountRequest(acct);
        CreateAccountResponseType resp = pas.createAccount(req);
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    public boolean checkAuthorizeCreditCaedPayment() {
        boolean testResult = true;
//        AuthTxn txn = new AuthTxn();
//        txn.setCurrency("JPY");
//        txn.setCardNum("1234567899994321");
//        txn.setTotalAmount("1000");
//        txn.setTotalTax("100");
//        txn.setTotalPaid("1100");
        // temp fix, not a working test!!
        MoneyType amount = new MoneyType();
        String creditCardNumber = "";
        String NOAtx = "";
        AuthorizeCreditCardPaymentRequestType authReq = 
            createCreditCardAuthorizationRequest(
                amount,
                creditCardNumber,
                NOAtx);
        AuthorizeCreditCardPaymentResponseType authResp;
        boolean authReturn=false;
        try {
            authResp = pas.authorizeCreditCardPayment(authReq);
            authReturn = checkAbstractResponse(authReq, authResp);
            if (authReturn) {   // no error, go ahead to capture it
                PasTokenType token = authResp.getAuthorizationToken();
                MoneyType depositAmount = new MoneyType("POINTS", "1100");
                MoneyType captureAmount = new MoneyType("JPY", "1100");
                CaptureCreditCardToAccountRequestType depReq = createCaptureCreditCardToAccountRequest(token, acct, captureAmount, depositAmount);
                try {
                    CaptureCreditCardToAccountResponseType resp = pas.captureCreditCardToAccount(depReq);
                    testResult = checkAbstractResponse(depReq, resp);
                } catch (Exception e) {
                    System.out.println(getStackTrace(e));
                    System.out.println(e.getMessage());
                    testResult = false;
                }
                return testResult;
            }
            testResult &= authReturn;
        } catch (Exception e) {
            System.out.println(getStackTrace(e));
            System.out.println(e.getMessage());
            testResult = false;
        }
        return testResult;
    }
    
    public void testAuthorizeCreditCardPayment() {
        assertTrue(checkAuthorizeCreditCaedPayment());
    }   
}
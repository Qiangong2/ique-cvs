package com.broadon.unitTest.pcis;

import java.net.URL;

import com.broadon.wsapi.pcis.*;

public class PCGen {
    static PrepaidCardGenerationPortType pcis;
    static String host = "localhost";
    static String port = "8080";
    static String eCardType = null;
    static int type;
    static int size = 1;
    static String ref1;
    static int operatorId = 555;
    static String password = "123";
    /**
     * @param args
     */
    public static void main(String[] args) {
        int     argCount = args.length;
        if (argCount <= 0)
        {
            usage();
            /* NOT REACHED */
        }
        /*
         * Process command line arguments.
         */
        for (int n = 0; n < argCount; n++)
        {
            if (args[n].charAt(0) != '-')
            {
            usage();
            /* NOT REACHED */
            }
            switch (args[n].charAt(1))
            {
            case 'H':
            case 'h':
            host = (args[++n]);
            break;
            case 'P':
            case 'p':
            port = args[++n];
            break;
            case 'T':
            case 't':
            eCardType = (args[++n]);
            type = Integer.parseInt(eCardType);
            break;
            case 'S':
            case 's':
            size = Integer.parseInt(args[++n]);
            break;
            case 'R':
            case 'r':
            ref1 = (args[++n]);
            break;
            case 'U':
            case 'u':
            operatorId = Integer.parseInt((args[++n]));
            break;
            case 'A':
            case 'a':
            password = (args[++n]);
            break;
            default:
                System.err.println("Unknown option " + args[n]);
                usage();
                /* NOT REACHED */
                }
        }
        if (eCardType == null)
        {
            System.err.println("Missing eCard type.");
            /* NOT REACHED */
        }
        pcis = newPrepaidCardGenerationService();
        PrepaidCardGenerationRequestType req = new PrepaidCardGenerationRequestType();
        req.setVersion("1.0");
        req.setMessageID(Long.toString(System.currentTimeMillis()));
        req.setECardType(type);
        req.setOperatorId(operatorId);
        req.setPassword(password);
        req.setQuantity(size);
        req.setReferenceId1(ref1);
        System.out.println("sending request");
        try {
            PrepaidCardGenerationResponseType resp = pcis.prepaidCardGeneration(req);
            if (resp.getErrorCode() != 0) {
                System.err.println("ERROR: code=" + resp.getErrorCode());
            }
            String ecards[] = resp.getPrepaidCardString();
            if (ecards != null) {
                System.out.println("Newly Generated Ecards:");
                System.out.println();
                for (int i=0; i<ecards.length; i++)
                    System.out.println(ecards[i]);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    static public PrepaidCardGenerationPortType newPrepaidCardGenerationService() {
        PrepaidCardGenerationPortType pcis = null;
        try {
            URL endpointURL = new URL(
                                     "http://" + 
                                     host + 
                                     ":" + 
                                     port +
                                     "/pcis/services/PrepaidCardGenerationSOAP");
            System.out.println();
            System.out.println(endpointURL);
            PrepaidCardGenerationServiceLocator pcisService = new PrepaidCardGenerationServiceLocator();
            pcis = pcisService.getPrepaidCardGenerationSOAP(endpointURL);
        } catch (Exception e) {
            System.out.println("cannot connect to pcis services.");
            System.out.println(e.getMessage());
        }
        return pcis;
    }
    
    /**
     * Shows the usage of this program and terminate.
     */
    private static final void usage()
    {
        System.err.println("Usage:");
        System.err.println("  java com.broadon.unitTest.pcis.InterActGen [options]");
        System.err.println();
        System.err.println("where [options] are:");
        System.err.println("    -h <host>\t\thost machine");
        System.err.println("    -p <port>\t\tport number");
        System.err.println("    -t <type>\t\teCard type");
        System.err.println("    -s <batch size>\tnumber of eCards");
        System.err.println("    -r \t\treferenceId ");
        System.err.println("    -u \t\toperatorId ");
        System.err.println("    -a \t\tpassword ");
        System.err.println();
        System.err.println("defaults: -h localhost -p 8080 -s 1 -u 555 -a 123");
        System.exit(-1);
    }
}

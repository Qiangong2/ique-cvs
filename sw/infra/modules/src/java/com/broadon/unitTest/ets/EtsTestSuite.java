/*
 * ETS TestSuite:  Add new ETS TestCase here to this class.
 */
package com.broadon.unitTest.ets;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.broadon.unitTest.Configuration;

public class EtsTestSuite {

	public static void main(String[] args) {
            if (args != null && args.length > 0) {
                TestCaseWrapper.hostName = args[0];
            }
            Configuration.testLab1();
            junit.textui.TestRunner.run(EtsTestSuite.suite());
	}

	public static Test suite() {
		TestSuite suite = new TestSuite("Test for com.broadon.unitTest.ets");
		//$JUnit-BEGIN$
		suite.addTestSuite(GetTickets.class);
		//$JUnit-END$
		return suite;
	}

}

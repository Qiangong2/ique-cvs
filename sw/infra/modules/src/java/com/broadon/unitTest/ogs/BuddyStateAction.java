package com.broadon.unitTest.ogs;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;

import com.broadon.unitTest.Configuration;
import com.broadon.unitTest.DBConnection;

public class BuddyStateAction
{
    // DB Parameters
    private static String ogs_user = "ogs";
    private static String ogs_pswd = "ogs";
    private static String ias_user = "ias";
    private static String ias_pswd = "ias";
    private static String db_url = Configuration.getDbUrl();

    // Test Account Id
    private static String MY_ACCOUNT_ID = "1000000";
    private static String BUDDY_ACCOUNT_ID = "2000000";
        
              
    // Hashsets stored in memory with test data
    private static HashMap inviteBud = null;
    private static HashMap removeBud = null;
    private static HashMap acceptInv = null;
    private static HashMap rejectInv = null;
    private static HashMap blockBud = null;
    private static HashMap unblockBud = null;
   
    private static int myFlag = 0;
    private static int yourFlag = 0;
        
    // Other default values
    private static boolean initialized = false;
    
    // SQL Statements
    private static String DB_CLEANUP =
        "DELETE FROM OGS_BUDDY_LISTS WHERE account_id = " + MY_ACCOUNT_ID + 
            " OR account_id = " + BUDDY_ACCOUNT_ID +
            " OR buddy_account_id = " + MY_ACCOUNT_ID +
            " OR buddy_account_id = " + BUDDY_ACCOUNT_ID;

    private static String INSERT_ACCOUNT_INFO =
        "INSERT into IAS_ACCOUNTS (account_id, login_name, passwd, create_date, activate_date, status, nick_name, full_name, " +
        "email_address, birth_date) VALUES (?,?,?,sys_extract_utc(current_timestamp)," +
        "sys_extract_utc(current_timestamp),'A',?,?,?,sys_extract_utc(current_timestamp)-(21*365))";
    
    private static String UPDATE_ACCOUNT_INFO =
        "UPDATE IAS_ACCOUNTS SET login_name = ?, passwd = ?, status = 'A' WHERE account_id = ?";
    
    private static long getRandomNumber(long start, long end)
    {
        if (start > end) {
            long dummy = start;
            start = end;
            end  = dummy;
        }
    
        long range = end - start + 1; 
        
        return (long)(Math.random() * range) + start;        
    }
    
    // --------------------------------------------
    // Member methods
    // --------------------------------------------

    public static void setUp()
    {        
        if (initialized) return;

        myFlag = 0;
        yourFlag = 0;
        
        inviteBud = new HashMap();
        inviteBud.put(new BuddyState(0,0), new BuddyState(1,0));
        inviteBud.put(new BuddyState(0,1), new BuddyState(3,3));
        inviteBud.put(new BuddyState(0,2), new BuddyState(3,2));
        inviteBud.put(new BuddyState(0,3), new BuddyState(3,3));
        inviteBud.put(new BuddyState(0,4), new BuddyState(1,4));
        inviteBud.put(new BuddyState(0,5), new BuddyState(1,5));
        inviteBud.put(new BuddyState(0,8), new BuddyState(5,8));
        inviteBud.put(new BuddyState(0,9), new BuddyState(3,11));
        inviteBud.put(new BuddyState(0,10), new BuddyState(3,10));
        inviteBud.put(new BuddyState(0,11), new BuddyState(3,11));
        inviteBud.put(new BuddyState(0,12), new BuddyState(5,12));
        inviteBud.put(new BuddyState(0,13), new BuddyState(5,13));
        
        removeBud = new HashMap();
        for (int i = 1; i < 14; i++) {
            if ( i != 6 && i != 7) {
                for (int j = 0; j < 14; j++) {
                    if (j != 6 && j != 7) {
                        switch (j) {
                            case 0: removeBud.put(new BuddyState(i,0), new BuddyState(0,0));break;
                            case 1: removeBud.put(new BuddyState(i,1), new BuddyState(0,5));break;
                            case 2: removeBud.put(new BuddyState(i,2), new BuddyState(0,4));break;
                            case 3: removeBud.put(new BuddyState(i,3), new BuddyState(0,4));break;
                            case 4: removeBud.put(new BuddyState(i,4), new BuddyState(0,4));break;
                            case 5: removeBud.put(new BuddyState(i,5), new BuddyState(0,5));break;
                            case 8: removeBud.put(new BuddyState(i,8), new BuddyState(0,12));break;
                            case 9: removeBud.put(new BuddyState(i,9), new BuddyState(0,13));break;
                            case 10: removeBud.put(new BuddyState(i,10), new BuddyState(0,12));break;
                            case 11: removeBud.put(new BuddyState(i,11), new BuddyState(0,12));break;
                            case 12: removeBud.put(new BuddyState(i,12), new BuddyState(0,12));break;
                            case 13: removeBud.put(new BuddyState(i,13), new BuddyState(0,13));break;
                        }                        
                    }                
                }
            }
        }
        
        acceptInv = new HashMap();
        acceptInv.put(new BuddyState(0,1), new BuddyState(2,3));
        acceptInv.put(new BuddyState(1,1), new BuddyState(3,3));
        acceptInv.put(new BuddyState(4,1), new BuddyState(2,3));
        acceptInv.put(new BuddyState(5,1), new BuddyState(3,3));
        acceptInv.put(new BuddyState(0,9), new BuddyState(2,11));
        acceptInv.put(new BuddyState(1,9), new BuddyState(3,11));
        acceptInv.put(new BuddyState(4,9), new BuddyState(2,11));
        acceptInv.put(new BuddyState(5,9), new BuddyState(3,11));
        
        rejectInv = new HashMap();
        rejectInv.put(new BuddyState(0,1), new BuddyState(0,5));
        rejectInv.put(new BuddyState(1,1), new BuddyState(5,5));
        rejectInv.put(new BuddyState(4,1), new BuddyState(4,5));
        rejectInv.put(new BuddyState(5,1), new BuddyState(5,5));
        rejectInv.put(new BuddyState(0,9), new BuddyState(0,13));
        rejectInv.put(new BuddyState(1,9), new BuddyState(5,13));
        rejectInv.put(new BuddyState(4,9), new BuddyState(4,13));
        rejectInv.put(new BuddyState(5,9), new BuddyState(5,13));
        
        blockBud = new HashMap();
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 14; j++) {
                if (j != 6 && j != 7) {
                    switch (j) {
                        case 0: blockBud.put(new BuddyState(i,0), new BuddyState(i+8,0));break;
                        case 1: blockBud.put(new BuddyState(i,1), new BuddyState(i+8,1));break;
                        case 2: blockBud.put(new BuddyState(i,2), new BuddyState(i+8,2));break;
                        case 3: blockBud.put(new BuddyState(i,3), new BuddyState(i+8,3));break;
                        case 4: blockBud.put(new BuddyState(i,4), new BuddyState(i+8,4));break;
                        case 5: blockBud.put(new BuddyState(i,5), new BuddyState(i+8,5));break;
                        case 8: blockBud.put(new BuddyState(i,8), new BuddyState(i+8,8));break;
                        case 9: blockBud.put(new BuddyState(i,9), new BuddyState(i+8,9));break;
                        case 10: blockBud.put(new BuddyState(i,10), new BuddyState(i+8,10));break;
                        case 11: blockBud.put(new BuddyState(i,11), new BuddyState(i+8,11));break;
                        case 12: blockBud.put(new BuddyState(i,12), new BuddyState(i+8,12));break;
                        case 13: blockBud.put(new BuddyState(i,13), new BuddyState(i+8,13));break;                                                
                    }                
                }
            }
        }
        
        unblockBud = new HashMap();
        for (int i = 8; i < 14; i++) {
            for (int j = 0; j < 14; j++) {
                if (j != 6 && j != 7) {
                    switch (j) {
                        case 0: unblockBud.put(new BuddyState(i,0), new BuddyState(i-8,0));break;
                        case 1: unblockBud.put(new BuddyState(i,1), new BuddyState(i-8,1));break;
                        case 2: unblockBud.put(new BuddyState(i,2), new BuddyState(i-8,2));break;
                        case 3: unblockBud.put(new BuddyState(i,3), new BuddyState(i-8,3));break;
                        case 4: unblockBud.put(new BuddyState(i,4), new BuddyState(i-8,4));break;
                        case 5: unblockBud.put(new BuddyState(i,5), new BuddyState(i-8,5));break;
                        case 8: unblockBud.put(new BuddyState(i,8), new BuddyState(i-8,8));break;
                        case 9: unblockBud.put(new BuddyState(i,9), new BuddyState(i-8,9));break;
                        case 10: unblockBud.put(new BuddyState(i,10), new BuddyState(i-8,10));break;
                        case 11: unblockBud.put(new BuddyState(i,11), new BuddyState(i-8,11));break;
                        case 12: unblockBud.put(new BuddyState(i,12), new BuddyState(i-8,12));break;
                        case 13: unblockBud.put(new BuddyState(i,13), new BuddyState(i-8,13));break;                                                
                    }                
                }
            }
        }
        
        DBConnection db = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        
        try {            
            db = new DBConnection(ogs_user, ogs_pswd, db_url);
            db.cleanup(DB_CLEANUP);
            
            db = new DBConnection(ias_user, ias_pswd, db_url);
            conn = db.getConnection();
                        
            // Insert Account Records            
                              
            for (int i = 1; i <= 2; i++) {
                String accountId = (i == 1 ? MY_ACCOUNT_ID : BUDDY_ACCOUNT_ID);
                
                stmt = conn.prepareStatement(UPDATE_ACCOUNT_INFO);
                stmt.setString(1, "login"+accountId);
                stmt.setString(2, "pass"+accountId);
                stmt.setLong(3, Long.parseLong(accountId));
                
                int result = stmt.executeUpdate();
                stmt.close();
                
                if (result != 1) {
                    stmt = conn.prepareStatement(INSERT_ACCOUNT_INFO);
                    stmt.setLong(1, Long.parseLong(accountId));
                    stmt.setString(2, "login"+accountId);
                    stmt.setString(3, "pass"+accountId);
                    stmt.setString(4, "nick"+accountId);
                    stmt.setString(5, "full"+accountId);
                    stmt.setString(6, "email"+accountId+"@broadon.com");                        
                                                               
                    stmt.executeUpdate();
                    stmt.close();
                }
            }
            
            conn.commit();            
            
        } catch (SQLException ex) {
            ex.printStackTrace();

        } catch (IOException ex) {
            ex.printStackTrace();

        } finally {            
            try {
                if (conn != null)
                    db.closeConnection();
                if (stmt != null)
                    stmt.close();
            }
            catch (Throwable e)
            {}
        }

        initialized = true;
        return;
    }   
    
    public static String getYourAccountId()
    {
        return BUDDY_ACCOUNT_ID;                      
    }
    
    public static int getYourFlag()
    {
        return yourFlag;                      
    }
    
    public static String getMyAccountId()
    {
        return MY_ACCOUNT_ID;                      
    } 
    
    public static int getMyFlag()
    {
        return myFlag;                      
    }
    
    public static String getRandomBuddyAction()
    {
        String[] buddyAction = {"INVITE_BUD", "ACCEPT_INV", "REJECT_INV", "REMOVE_BUD", "BLOCK_BUD", "UNBLOCK_BUD"};
        return buddyAction[(int)getRandomNumber(0,buddyAction.length-1)];                      
    }    
    
    public static BuddyState getBuddyState(String buddyAction)
    {
        BuddyState buddyState = null;
        
        if ("INVITE_BUD".equals(buddyAction))
            buddyState = (BuddyState)inviteBud.get(new BuddyState(myFlag, yourFlag));
        else if ("ACCEPT_INV".equals(buddyAction))
            buddyState = (BuddyState)acceptInv.get(new BuddyState(myFlag, yourFlag));
        else if ("REJECT_INV".equals(buddyAction))
            buddyState = (BuddyState)rejectInv.get(new BuddyState(myFlag, yourFlag));
        else if ("REMOVE_BUD".equals(buddyAction))
            buddyState = (BuddyState)removeBud.get(new BuddyState(myFlag, yourFlag));
        else if ("BLOCK_BUD".equals(buddyAction))
            buddyState = (BuddyState)blockBud.get(new BuddyState(myFlag, yourFlag));
        else if ("UNBLOCK_BUD".equals(buddyAction))
            buddyState = (BuddyState)unblockBud.get(new BuddyState(myFlag, yourFlag));  
        
        return buddyState;
    }
    
    public static void setYourFlag(int flag)
    {
        yourFlag = flag;                      
    }
    
    public static void setMyFlag(int flag)
    {
        myFlag = flag;                      
    }
    
    public static void toggleFlags()
    {
        int dummy = myFlag;
        
        myFlag = yourFlag;
        yourFlag = dummy;
    }
}

class BuddyState
{
    int myFlag;
    int yourFlag;
        
    BuddyState (int myFlag, int yourFlag)
    {
        this.myFlag = myFlag;
        this.yourFlag = yourFlag;                
    }
    
    public int hashCode() { return myFlag + yourFlag; }
        
    public boolean equals(Object obj)
    {
        if (obj != null && obj instanceof BuddyState) {
            BuddyState buddyState = (BuddyState) obj;
            return (this.myFlag == buddyState.myFlag &&
                    this.yourFlag == buddyState.yourFlag);
        }
        return false;
    }
}


package com.broadon.unitTest.ecs;

import com.broadon.wsapi.ecs.PriceType;
import com.broadon.wsapi.ecs.SubscribeRequestType;
import com.broadon.wsapi.ecs.SubscribeResponseType;
import com.broadon.wsapi.ecs.TimeDurationType;
import com.broadon.wsapi.ecs.TimeUnitType;

public class Subscribe extends TestCaseWrapper {
    public static void main(String[] args) {
        junit.textui.TestRunner.run(Subscribe.class);
    }

    public Subscribe(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
        clearETickets(validNC1, "SR");
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    private boolean checkSubscribe(
            SubscribeRequestType req,
            SubscribeResponseType expected)
        throws Throwable
    {
        boolean testResult = true;

        SubscribeResponseType resp;

        resp = ecs.subscribe(req);
        testResult &= checkAbstractResponse(req, resp);
            
        // Check response against expected response
        testResult &= checkError(expected, resp);

        return testResult;
    }

    /**
     * Initializes request to purchase a valid subscription item
     * @param req
     */
    protected void init3MSubscribeRequest(SubscribeRequestType req) {
        initAbstractRequest(req);
        req.setDeviceId(validNC2);
        makeSubscriptionECard();
        req.setPayment(validSubscriptionECard);
        req.setItemId(17);
        req.setChannelId(IQUE_SUBSCRIPTION_CHANNEL);
        req.setSubscriptionLength(new TimeDurationType(92, TimeUnitType.day));
        req.setPrice(new PriceType("1200", "POINTS"));
        req.setDeviceCert(validDeviceCert);
    }

    /**
     * Initializes request to purchase a valid subscription item
     * @param req
     */
    protected void initSubscribeRequest(SubscribeRequestType req) {
        initAbstractRequest(req);
        req.setDeviceId(validNC1);
        makeSubscriptionECard();
        req.setPayment(validSubscriptionECard);
        req.setItemId(5);
        req.setChannelId(IQUE_SUBSCRIPTION_CHANNEL);
        req.setSubscriptionLength(new TimeDurationType(60, TimeUnitType.day));
        req.setPrice(new PriceType("400", "POINTS"));
        req.setDeviceCert(validDeviceCert);
    }

    public void testSubscribeBadItemId() throws Throwable {
        SubscribeRequestType req = new SubscribeRequestType();
        SubscribeResponseType expected = new SubscribeResponseType();
        
        initSubscribeRequest(req);
        req.setItemId(300);
        
        expected.setErrorCode(601);
        expected.setErrorMessage(".*Invalid item.*");
        
        assertTrue(checkSubscribe(req, expected));
    }

    public void testSubscribeBadDeviceType() throws Throwable {
        SubscribeRequestType req = new SubscribeRequestType();
        SubscribeResponseType expected = new SubscribeResponseType();
        
        initSubscribeRequest(req);
        req.setVirtualDeviceType(new Integer(1));
        
        expected.setErrorCode(604);
        expected.setErrorMessage(".*Invalid device.*");
        
        assertTrue(checkSubscribe(req, expected));
    }

    public void testSubscribeBadCountryId() throws Throwable {
        SubscribeRequestType req = new SubscribeRequestType();
        SubscribeResponseType expected = new SubscribeResponseType();
        
        initSubscribeRequest(req);
        req.setCountryCode("INVALID");
        
        expected.setErrorCode(606);
        expected.setErrorMessage(".*Invalid country.*");
        
        assertTrue(checkSubscribe(req, expected));
    }

    public void testSubscribeBadPrice() throws Throwable {
        SubscribeRequestType req = new SubscribeRequestType();
        SubscribeResponseType expected = new SubscribeResponseType();
        
        initSubscribeRequest(req);
        req.setPrice(new PriceType("2", "EUNITS"));
        
        expected.setErrorCode(615);
        expected.setErrorMessage(".*Actual price is different from expected price.*");
        
        assertTrue(checkSubscribe(req, expected));
    }

    public void testSubscribeBadChannelId() throws Throwable {
        SubscribeRequestType req = new SubscribeRequestType();
        SubscribeResponseType expected = new SubscribeResponseType();
        
        initSubscribeRequest(req);
        req.setChannelId("100");
        
        expected.setErrorCode(613);
        expected.setErrorMessage(".*Invalid channel.*");
        
        assertTrue(checkSubscribe(req, expected));
    }

    public void testSubscribeNoPayment()
        throws Throwable
    {
        SubscribeRequestType req = new SubscribeRequestType();
        SubscribeResponseType expected = new SubscribeResponseType();

        initSubscribeRequest(req);
        req.setPayment(null);
        
        expected.setErrorCode(616);
        expected.setErrorMessage(".*No payment.*");
        assertTrue(checkSubscribe(req, expected));
    }

    public void testSubscribeInvalidPayment()
        throws Throwable
    {
        for (int i = 0; i < invalidPayments.length; i++) {
            SubscribeRequestType req = new SubscribeRequestType();
            SubscribeResponseType expected = new SubscribeResponseType();
    
            initSubscribeRequest(req);
            req.setPayment(invalidPayments[i]);
            
            expected.setErrorCode(invalidPaymentErrors[i]);
            expected.setErrorMessage(".*");
            assertTrue(checkSubscribe(req, expected));
        }
    }

    public void testSubscribe()
        throws Throwable
    {
        SubscribeRequestType req = new SubscribeRequestType();
        SubscribeResponseType expected = new SubscribeResponseType();
        initSubscribeRequest(req);

        assertTrue(checkSubscribe(req, expected));
        
        // Extend subscription (get not enough balance)
        expected.setErrorCode(810);
        expected.setErrorMessage(".*Not Enough Money.*");        
        assertTrue(checkSubscribe(req, expected));
    }

    /* Disable test since there is no free subscription now */
    public void _testFreeSubscribe() throws Throwable {
        SubscribeRequestType req = new SubscribeRequestType();
        SubscribeResponseType expected = new SubscribeResponseType();
        //initFreeSubscribeRequest(req);
         
        assertTrue(checkSubscribe(req, expected));

        // Extend subscription (should be okay)
        assertTrue(checkSubscribe(req, expected));
    }
}

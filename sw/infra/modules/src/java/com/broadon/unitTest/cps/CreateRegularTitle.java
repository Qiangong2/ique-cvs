package com.broadon.unitTest.cps;


import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import com.broadon.unitTest.Configuration;
import com.broadon.wsapi.cps.ActionType;
import com.broadon.wsapi.cps.PublishRequestType;
import com.broadon.wsapi.cps.PublishResponseType;

public class CreateRegularTitle extends TestCaseWrapper
{
    public CreateRegularTitle(String name) {
        super(name);
    }
  

    private void checkTMD(PublishRequestType req, byte[] titleMetaData) throws NoSuchAlgorithmException, IOException
    {
        MessageDigest sha = MessageDigest.getInstance("SHA");
        InputStream in = rawContent[0].getInputStream();
        byte[] buf = new byte[1024];
        int n;
        while ((n = in.read(buf)) > 0) {
            sha.update(buf, 0, n);
        }
        in.close();
        byte[] reqHash = sha.digest();
        byte[] respHash = new byte[reqHash.length];
        System.arraycopy(titleMetaData, 500, respHash, 0, respHash.length);
        assertEquals(true, Arrays.equals(reqHash, respHash));   
    }
    
    public void testCreateRegularTitle() throws Throwable
    {
        final long tid = 1;
        removeTitle(tid);
        
        // first publishing
        PublishRequestType req = initRequest(tid);
        PublishResponseType resp = publish(req, 0);
        checkTMD(req, resp.getTitleMetaData());
    }
    
    public void testCreateDuplicateTitle() throws Throwable
    {
        final long tid = 2;
        
        removeTitle(tid);
        PublishRequestType req = initRequest(tid);
        
        // duplicated publishing
        publish(req, 0);
        publish(req, 1011);
    }

    public void testSameContentWithDifferentTitleID() throws Throwable
    {
        db.removeTitleRange(channel + 3, channel + 5 );
        
        PublishRequestType req = initRequest(3);
        publish(req, 0);
        
        req.setTitleID(getTitleIDString(channel + 4));
        publish(req, 0);
    }
    
    public void testUpdateTitle() throws Throwable
    {
        final long tid = 5;
        removeTitle(tid);
        PublishRequestType req = initRequest(tid);
        publish(req, 0);
        
        // to avoid unique contraint violation on the content title object table
        // with multiple entries within the same second.
        Thread.sleep(1000);
        req.setAction(ActionType.Update);
        publish(req, 0);
        // TODO:  check if the records are actually updated (need TMD back).
    }
    
    public void testUpdateTitleWrongPassword() throws Throwable
    {
        final long tid = 6;
        removeTitle(tid);
        PublishRequestType req = initRequest(tid);
        publish(req, 0);
        req.setAction(ActionType.Update);
        req.setEncryptionPassword("wrong password");
        publish(req, 1001);
    }
    
    public void testUpdateNonExistTitle() throws Throwable
    {
        final long tid = 7;
        removeTitle(tid);
        PublishRequestType req = initRequest(tid);
        req.setAction(ActionType.Update);
        publish(req, 1004);
    }

    
    public static void main(String[] args) {
        if (args != null && args.length > 0)
            hostName = args[0];
        
        Configuration.testLab1();
        junit.textui.TestRunner.run(CreateRegularTitle.class);
    }

}

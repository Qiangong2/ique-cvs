package com.broadon.unitTest.ecs;

import java.math.BigInteger;

import org.apache.axis.types.URI;
import org.apache.axis.types.URI.MalformedURIException;

import com.broadon.unitTest.Configuration;
import com.broadon.wsapi.ecs.GetApplicationUpdateRequestType;
import com.broadon.wsapi.ecs.GetApplicationUpdateResponseType;
import com.broadon.wsapi.ecs.TitleVersionType;

public class GetApplicationUpdate extends TestCaseWrapper {
    public static void main(String[] args) {
        junit.textui.TestRunner.run(GetApplicationUpdate.class);
    }

    public GetApplicationUpdate(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
        clearETickets(testDevice, null);
    }

    public void tearDown() throws Exception {
        clearETickets(testDevice, null);
        super.tearDown();
    }

    public boolean checkGetApplicationUpdate(
            GetApplicationUpdateRequestType req, 
            GetApplicationUpdateResponseType expected)
        throws Throwable
    {
        boolean testResult = true;
        GetApplicationUpdateResponseType resp;

        resp = ecs.getApplicationUpdate(req);
        testResult &= checkAbstractResponse(req, resp);

        // Check response against expected
        testResult &= checkError(expected, resp);

        testResult &= checkTitleVersions("getUpdatedTitles", expected.getUpdatedTitles(), resp.getUpdatedTitles());
        // Don't check content prefix url
        //assertEquals("getContentPrefixURL",
        //        expected.getContentPrefixURL(), resp.getContentPrefixURL());

        return testResult;
    }
    
    public void initExpected(GetApplicationUpdateResponseType expected)
    {
        try {
            expected.setContentPrefixURL(new URI("http://ccs:16983/ccs/download"));
        } catch (MalformedURIException e) {
            fail("Error initializing GetApplicationUpdateResponseType: " + e.getLocalizedMessage());
        }
    }
    
    public void testGetApplicationUpdate()
        throws Throwable
    {
        createETicket(testDevice, new BigInteger(testTitles[3]+"ffffffff", 16), "PERMANENT", "PR", 0);
        createETicket(testDevice, new BigInteger(testTitles[4]+"ffffffff", 16), "PERMANENT", "PR", 0);

        GetApplicationUpdateRequestType req = new GetApplicationUpdateRequestType();
        initAbstractRequest(req);
        req.setRegionId(testRegion);
        req.setCountryCode(testCountry);
        req.setDeviceId(testDevice);

        GetApplicationUpdateResponseType expected = new GetApplicationUpdateResponseType();
        initExpected(expected);
        expected.setUpdatedTitles(new TitleVersionType[2]);
        expected.setUpdatedTitles(0, new TitleVersionType(testTitles[3], -1, new Long(0)));
        expected.setUpdatedTitles(1, new TitleVersionType(testTitles[4], -1, new Long(0)));
        
        assertTrue(checkGetApplicationUpdate(req, expected));
        
        if (Configuration.getProduct() == Configuration.NC) {
            req.setLastTitleUpdateTime(new Long(1148061126000L));
            expected.setUpdatedTitles(new TitleVersionType[1]);
            expected.setUpdatedTitles(0, new TitleVersionType("0002000100080030", 3, new Long(16842752)));
            assertTrue(checkGetApplicationUpdate(req, expected));
        }
    }
}

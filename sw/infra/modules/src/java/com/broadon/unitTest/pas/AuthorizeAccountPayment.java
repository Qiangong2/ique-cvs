package com.broadon.unitTest.pas;

import com.broadon.wsapi.pas.AuthorizeAccountPaymentRequestType;
import com.broadon.wsapi.pas.AuthorizeAccountPaymentResponseType;
import com.broadon.wsapi.pas.BalanceAccountType;
import com.broadon.wsapi.pas.CaptureECardToAccountRequestType;
import com.broadon.wsapi.pas.CreateAccountRequestType;
import com.broadon.wsapi.pas.CreateAccountResponseType;
import com.broadon.wsapi.pas.VoidAuthorizationRequestType;
import com.broadon.wsapi.pas.VoidAuthorizationResponseType;
import com.broadon.wsapi.pas.PasTokenType;
import com.broadon.wsapi.pas.MoneyType;

public class AuthorizeAccountPayment extends TestCaseWrapper {
    CreateAccountRequestType req;
    CreateAccountResponseType resp;
    BalanceAccountType acct = null;
    public static void main(String[] args) {
        junit.textui.TestRunner.run(AuthorizeAccountPayment.class);
    }

    public AuthorizeAccountPayment(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
        resetTestDataBase(AccountTests);
        MoneyType amount = new MoneyType("POINTS", "50");
        PasTokenType token = eCardAuth("0010030001", amount);
        acct = new BalanceAccountType();
        acct.setAccountNumber("30001");
        acct.setPIN("30001");  
        req = createCreateAccountRequest(acct);
        resp = pas.createAccount(req);
        if (checkAbstractResponse(req, resp)) {
            CaptureECardToAccountRequestType req = createCaptureECardToAccountRequest(
                token, acct, amount);
            pas.captureECardToAccount(req);
        }
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    public boolean checkAuthorizeAccountPayment() {
        boolean testResult = true;
        MoneyType amount = new MoneyType("POINTS", "10"); 
        BalanceAccountType acct = new BalanceAccountType();
        acct.setAccountNumber("30001");
        acct.setPIN("30001");
        AuthorizeAccountPaymentRequestType authReq = createAcctAuthorizationRequest(
                acct, amount);
        AuthorizeAccountPaymentResponseType authResp;
        VoidAuthorizationResponseType voidResp;
        boolean authReturn=false, voidReturn=false;

        try {
            authResp = pas.authorizeAccountPayment(authReq);
            authReturn = checkAbstractResponse(authReq, authResp);
            if (authReturn) {   // no error, go ahead to void it
                PasTokenType token = authResp.getAuthorizationToken();
                VoidAuthorizationRequestType voidReq = createVoidAuthorizationRequest(token);
                voidResp = pas.voidAuthorization(voidReq);
                voidReturn = checkAbstractResponse(voidReq, voidResp);
            }
            testResult &= authReturn;
            testResult &= voidReturn;
        } catch (Exception e) {
            System.out.println(getStackTrace(e));
            System.out.println(e.getMessage());
            testResult = false;
        }
        
        return testResult;
    }
    
    public void testAuthorizeAccountPayment() {
        assertTrue(checkAuthorizeAccountPayment());
    }   
}
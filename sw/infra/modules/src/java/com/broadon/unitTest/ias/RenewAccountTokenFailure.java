package com.broadon.unitTest.ias;

import org.apache.axis.AxisFault;

import com.broadon.wsapi.ias.RenewAccountTokenRequestType;
import com.broadon.wsapi.ias.RenewAccountTokenResponseType;

public class RenewAccountTokenFailure extends TestCaseWrapper {

    public static void main(String[] args) {
    }

    public RenewAccountTokenFailure(String name) {
	super(name);
    }

    public void setUp() throws Exception {
	super.setUp();
    }

    public void tearDown() throws Exception {
	super.tearDown();
    }

    private boolean checkRenewAccountTokenException(
		String accountId, 
		String accountPassword,
                String accountToken)
	throws Throwable
    {
	boolean testResult = true;

        RenewAccountTokenRequestType req = new RenewAccountTokenRequestType();

        initRequest(req);
	req.setAccountId(Integer.parseInt(accountId));
	req.setPassword(accountPassword);
        req.setTokenId(accountToken);
	
        try {
            RenewAccountTokenResponseType resp = ias.renewAccountToken(req);
		
            if (!testResult) {
                if (resp.getTokenId() != null && !accountToken.equals(resp.getTokenId())) {
                    TestDataAdmin.setExpiredAccountToken(accountId, accountToken);
                    TestDataAdmin.setValidAccountToken(accountId, resp.getTokenId());
                }
            }
            
        } catch (AxisFault a) {
            testResult &= a.getFaultString() != null &&
                a.getFaultString().equals(UNAUTHORIZED_ACCESS_MESSAGE);          
        }
        
        return testResult;
    }

    private boolean checkRenewAccountTokenFailure(
                String accountId, 
                String accountPassword,
                String accountToken,
                RenewAccountTokenResponseType expected)
            throws Throwable
    {
        boolean testResult = true;
    
        RenewAccountTokenResponseType resp;
        RenewAccountTokenRequestType req = new RenewAccountTokenRequestType();
    
        initRequest(req);
        req.setAccountId(Integer.parseInt(accountId));
        req.setPassword(accountPassword);
        req.setTokenId(accountToken);
                    
        resp = ias.renewAccountToken(req);
        testResult &= checkAccountResponse(req, resp);
                    
        // Check response against expected response
        testResult &= checkError(expected, resp);
            
        if (!testResult) {
            if (resp.getTokenId() != null && !accountToken.equals(resp.getTokenId())) {
                TestDataAdmin.setExpiredAccountToken(accountId, accountToken);
                TestDataAdmin.setValidAccountToken(accountId, resp.getTokenId());
            }
        }
        
        return testResult;
    }
    
    public void testRenewAccountTokenFailure()
	throws Throwable
    {
        Token et = TestDataAdmin.getExpiredAccountToken();
        
        if (et != null) {
            // Case 1: Renew a token id that has expired
            assertTrue(checkRenewAccountTokenException(et.id, "pass"+et.id, et.tokenId));
            
            // Case 2: Renew a non-existing token id
            assertTrue(checkRenewAccountTokenException(et.id, "pass"+et.id, "123456"));           
            
        } else {
            System.out.println(getName() + " Warning: Failed to obtain an expired account token");
        }
        
        // Case 3: Renew a valid token with wrong password
        et = TestDataAdmin.getValidAccountToken();
        
        if (et != null) {
            RenewAccountTokenResponseType expected = new RenewAccountTokenResponseType();
            
            expected.setAccountId(Integer.parseInt(et.id));         
            expected.setErrorCode(931);
            expected.setErrorMessage("IAS - Failed to authenticate account");
            assertTrue(checkRenewAccountTokenFailure(et.id, "ajhgjkwrh", et.tokenId, expected));
        } else {
            System.out.println(getName() + " Warning: Failed to obtain a valid account token");
        }
    }
}

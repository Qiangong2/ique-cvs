/*
 * JUnit TestCase wrapper for ETS
 *   - this class contains functions that should be shared among ETS
 *     test cases.
 */
package com.broadon.unitTest.ets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

import junit.framework.TestCase;

import com.broadon.unitTest.Configuration;
import com.broadon.unitTest.ResourcePool;
import com.broadon.unitTest.TestLog;
import com.broadon.unitTest.cps.DBSetUp;
import com.broadon.wsapi.ets.ETicketPortType;

public class TestCaseWrapper extends TestCase {

    protected static long TEST_CHANNEL;
    protected static long REGULAR_TITLE;
    protected static long COMMON_TITLE;
    
    protected static String regularETKM;
    protected static String commonETKM;
    
    protected static final byte[] contentMask;

    protected ETicketPortType ets = null;
    protected static String hostName = null;
	
    protected static Object sync;
    protected static boolean hasTestTitle;
    protected boolean tracing = false;
    protected boolean doneSetup = false;
	
    static {
        sync = new Object();
        hasTestTitle = false;
        contentMask = new byte[256/8];
        Arrays.fill(contentMask, (byte) 0xff);
    }
    
    static final String GET_ETKM_CID =
        "SELECT CONTENT_ID FROM LATEST_CONTENT_TITLE_OBJECTS " +
        "WHERE (TITLE_ID = ? OR TITLE_ID = ?) AND CONTENT_OBJECT_TYPE='ETKM' " +
        "ORDER BY TITLE_ID ASC";
    
    public TestCaseWrapper(String name) throws SQLException, IOException {
	super(name);
        if (hostName != null) {
            Configuration.setHostName(hostName);
            Configuration.setDomainName("");
        }
    }

    public void setUp() throws Exception {
	super.setUp();
        if (doneSetup) return;
        synchronized (sync) {
            if (! hasTestTitle) {
                if (Configuration.getProduct() == Configuration.RVL) {
                    TEST_CHANNEL = 0x0001000000081000L;
                    REGULAR_TITLE = TEST_CHANNEL + 1;
                    COMMON_TITLE = TEST_CHANNEL + 10;
                } else {
                    TEST_CHANNEL = 0x0000800200000000L;
                    REGULAR_TITLE = TEST_CHANNEL + 1;
                    COMMON_TITLE = TEST_CHANNEL + 2;
                }

                DBSetUp db = new DBSetUp();
                /*
                // publish a title for testing
                DataHandler[] rawContent = new DataHandler[1];
                rawContent[0] = new DataHandler(new ByteArrayDataSource("Test content bits".getBytes()));
                
                ContentAttributeType[] contents = new ContentAttributeType[1];
                contents[0] = new ContentAttributeType(0, "Test Content", "GAME");

                PublishRequestType req = new PublishRequestType(ActionType.Create, "LOCAL_CPS",
                        "password", null, "Test product code", Long.toHexString(REGULAR_TITLE), 
                        "Test title", "GAME", "0000000200000001", new byte[0], 1, 0, 0, contents);
           
                db.removeChannel(TEST_CHANNEL);
                PublishPortType cps = ResourcePool.newPublishService();    
                PublishResponseType resp = cps.publish(req, rawContent);
                assertEquals(0, resp.getErrorCode());
                
                req.setTitleID(Long.toHexString(COMMON_TITLE));
                req.setAllowCommonTicket(Boolean.TRUE);
                resp = cps.publish(req, rawContent);
                assertEquals(0, resp.getErrorCode());
                */

                Connection conn = db.getConnection();
                //conn.commit();//xxx
                PreparedStatement ps = conn.prepareStatement(GET_ETKM_CID);
                ps.setLong(1, REGULAR_TITLE);
                ps.setLong(2, COMMON_TITLE);
                ResultSet rs = ps.executeQuery();
                rs.next();
                regularETKM = rs.getBigDecimal(1).toBigInteger().toString(16);
                rs.next();
                commonETKM = rs.getBigDecimal(1).toBigInteger().toString(16);
                rs.close();
                ps.close();
                db.close();
                
                hasTestTitle = true;
            }
        }
        if (ets == null) {
            ets = ResourcePool.newETicketService();
        }
        doneSetup = true;
    }

    public void tearDown() throws Exception {
	super.tearDown();
    }
	
    protected void runTest() throws Throwable 
    {
	boolean testResult = true;
	try {
	    super.runTest();
	} catch (Throwable e) {
	    System.out.println(getName() + " error: " + e.getMessage());
	    if (tracing) {
		e.printStackTrace();
	    }
	    testResult = false;
	    throw e;
	} finally {
	    TestLog.report("ETS", getName(), testResult);
	}
    }
}

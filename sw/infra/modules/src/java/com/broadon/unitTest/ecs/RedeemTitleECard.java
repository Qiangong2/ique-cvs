package com.broadon.unitTest.ecs;

import com.broadon.wsapi.ecs.RedeemECardRequestType;
import com.broadon.wsapi.ecs.RedeemECardResponseType;
import com.broadon.wsapi.ecs.RedeemKindType;

public class RedeemTitleECard extends TestCaseWrapper {
    public static void main(String[] args) {
        junit.textui.TestRunner.run(RedeemTitleECard.class);
    }

    public RedeemTitleECard(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
        clearETickets(testDevice, "SR");
        createAccount(validVCAccount.getAccountPayment().getAccountNumber());
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    private boolean checkRedeemECard(
            RedeemECardRequestType req,
            RedeemECardResponseType expected)
        throws Throwable
    {
        boolean testResult = true;

        RedeemECardResponseType resp;

        resp = ecs.redeemECard(req);
        testResult &= checkAbstractResponse(req, resp);
            
        // Check response against expected response
        testResult &= checkError(expected, resp);

        assertEquals(expected.getTotalPoints(), resp.getTotalPoints());
        assertEquals(expected.getRedeemedPoints(), resp.getRedeemedPoints());
        
        return testResult;
    }

    /**
     * Initializes request to redeem a valid title item
     * @param req
     */
    protected void initRedeemTitleECardRequest(RedeemECardRequestType req) {
        initAbstractRequest(req);
        req.setDeviceId(testDevice);
        req.setVirtualDeviceType(new Integer(DEVICE_TYPE_RV));
        makeTitleECard();
        req.setECard(validTitleECard.getECardPayment());
        req.setDeviceCert(validDeviceCert);
        req.setTitleId(testTitles[3]);
    }

    public void _testRedeemTitleECard()
        throws Throwable
    {
        RedeemECardRequestType req = new RedeemECardRequestType();
        RedeemECardResponseType expected = new RedeemECardResponseType();
        initRedeemTitleECardRequest(req);

        assertTrue(checkRedeemECard(req, expected));
        
        // get ecard used once error
        expected.setErrorCode(815);
        expected.setErrorMessage(".*Account Has Been Used Once.*");        
        assertTrue(checkRedeemECard(req, expected));
    }

    public void testRedeemTitleECardBadTitleId()
        throws Throwable
    {
        RedeemECardRequestType req = new RedeemECardRequestType();
        RedeemECardResponseType expected = new RedeemECardResponseType();
        initRedeemTitleECardRequest(req);
        req.setTitleId("1000900100000065");
        
        // get ecard used once error
        expected.setErrorCode(602);
        expected.setErrorMessage(".*Invalid title.*");        
        assertTrue(checkRedeemECard(req, expected));
    }

}

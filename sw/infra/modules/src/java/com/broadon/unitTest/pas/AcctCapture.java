package com.broadon.unitTest.pas;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;

import com.broadon.unitTest.DBConnection;
import com.broadon.wsapi.pas.AuthorizeAccountPaymentRequestType;
import com.broadon.wsapi.pas.AuthorizeAccountPaymentResponseType;
import com.broadon.wsapi.pas.BalanceAccountType;
import com.broadon.wsapi.pas.CapturePaymentRequestType;
import com.broadon.wsapi.pas.CapturePaymentResponseType;
import com.broadon.wsapi.pas.PasTokenType;
import com.broadon.wsapi.pas.MoneyType;

import com.broadon.unitTest.*;

public class AcctCapture extends TestCaseWrapper {
    PasTokenType token = null;
    MoneyType amount = null;
    protected boolean createdAccount = false;
    protected static Object sync;
    static int counter;
    static int accountOffset;
    String myAccountNum;
    
    static {
        sync = new Object();
        counter = 0;
        accountOffset = 0;
    }

    public static void main(String[] args) {
        junit.textui.TestRunner.run(AcctCapture.class);
    }

    public AcctCapture(String name) {
        super(name);
        Properties prop = Configuration.getPerfProperties();
        String tmp = prop.getProperty("clientId");
        if (tmp != null) {
            accountOffset = 30000 + (1000 * Integer.parseInt(tmp));
        }
        System.out.println("acctOffset:" + accountOffset);//debug
    }

    private void resetAccount() {
        // DB Parameters
        String db_user = "pas";
        String db_pswd = "pas";
        String db_url = Configuration.getDbUrl();
        
        String resetAccounts = " delete from pas_bank_accounts where ACCOUNT_ID "
            + " = " + myAccountNum;
        String insert = "insert into pas_bank_accounts values("+myAccountNum+", 'VCPOINTS', '01-APR-98', null, null, 1000000, 0, 'POINTS', '01-APR-98', "+myAccountNum+")";
        Connection conn = null;
        PreparedStatement pstmt = null;
        try {
            DBConnection db = new DBConnection(db_user, db_pswd, db_url);
            conn = db.getConnection();
            pstmt = conn.prepareStatement(resetAccounts);
            pstmt.executeUpdate();
            pstmt = conn.prepareStatement(insert);
            pstmt.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (conn != null)
                    conn.close();
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
     }

   public void setUp() throws Exception {
        super.setUp();
        createAccount();
    }

    public void tearDown() throws Exception {
        super.tearDown();
        token = null;
        amount = null;
    }

    private void authorizeAccountPayment() {

        amount = new MoneyType("POINTS", "1"); 
        BalanceAccountType acct = new BalanceAccountType();
        acct.setAccountNumber(myAccountNum);
        acct.setPIN(myAccountNum);
        AuthorizeAccountPaymentRequestType authReq = createAcctAuthorizationRequest(
                acct, amount);
        boolean authReturn=false;

        try {
            AuthorizeAccountPaymentResponseType authResp = pas.authorizeAccountPayment(authReq);
            authReturn = checkAbstractResponse(authReq, authResp);
            if (authReturn) {
                token = authResp.getAuthorizationToken();
            } else {
                System.out.println("Not authorized!");
            }
        } catch (Exception e) {
            System.out.println(getStackTrace(e));
            System.out.println(e.getMessage());
        }
    }
    
    private boolean capture() throws RemoteException {
        CapturePaymentResponseType capResp;
        amount.setAmount("1");
        CapturePaymentRequestType capReq = createCapturePaymentRequest(token, amount, false);
        capResp = pas.capturePayment(capReq);
        return checkAbstractResponse(capReq, capResp);
    }
    
    public boolean checkAcctCapture() throws RemoteException {
        boolean testResult = true;
        authorizeAccountPayment();
        testResult = capture();
        return testResult;
    }
    private void createAccount() {
        if (! createdAccount) {
            synchronized (sync) {
                myAccountNum = Integer.toString(accountOffset + counter);
                System.out.println("myAccountNum: " + myAccountNum);//debug
                counter++;
                resetAccount();
                createdAccount = true;
            }
        }
    }
    public void testAcctCapture() throws RemoteException {
        assertTrue(checkAcctCapture());
    }
}

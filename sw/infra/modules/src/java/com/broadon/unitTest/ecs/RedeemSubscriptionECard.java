package com.broadon.unitTest.ecs;

import com.broadon.wsapi.ecs.RedeemECardRequestType;
import com.broadon.wsapi.ecs.RedeemECardResponseType;
import com.broadon.wsapi.ecs.RedeemKindType;

public class RedeemSubscriptionECard extends TestCaseWrapper {
    public static void main(String[] args) {
        junit.textui.TestRunner.run(RedeemSubscriptionECard.class);
    }

    public RedeemSubscriptionECard(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
        clearETickets(testDevice, "SR");
        makeSubscriptionECard();
        createAccount(validVCAccount.getAccountPayment().getAccountNumber());
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    private boolean checkRedeemECard(
            RedeemECardRequestType req,
            RedeemECardResponseType expected)
        throws Throwable
    {
        boolean testResult = true;

        RedeemECardResponseType resp;

        resp = ecs.redeemECard(req);
        testResult &= checkAbstractResponse(req, resp);
            
        // Check response against expected response
        testResult &= checkError(expected, resp);

        assertEquals(expected.getTotalPoints(), resp.getTotalPoints());
        assertEquals(expected.getRedeemedPoints(), resp.getRedeemedPoints());
        
        return testResult;
    }

    /**
     * Initializes request to redeem a valid title item
     * @param req
     */
    protected void initRedeemTitleECardRequest(RedeemECardRequestType req) {
        initAbstractRequest(req);
        req.setDeviceId(testDevice);
        req.setVirtualDeviceType(new Integer(DEVICE_TYPE_RV));
        req.setECard(validTitleECard.getECardPayment());
        req.setDeviceCert(validDeviceCert);
        req.setTitleId("0000900100000065");
    }

    /**
     * Initializes request to redeem a valid subscription item
     * @param req
     */
    protected void initRedeemSubscriptionECardRequest(RedeemECardRequestType req) {
        initAbstractRequest(req);
        req.setDeviceId(testDevice);
        req.setECard(validSubscriptionECard.getECardPayment());
        req.setDeviceCert(validDeviceCert);
    }

    public void testRedeemSubscriptionECardBadDeviceType() throws Throwable {
        RedeemECardRequestType req = new RedeemECardRequestType();
        RedeemECardResponseType expected = new RedeemECardResponseType();
        
        initRedeemSubscriptionECardRequest(req);
        req.setVirtualDeviceType(new Integer(1));
        
        expected.setErrorCode(604);
        expected.setErrorMessage(".*Invalid device type.*");
        
        assertTrue(checkRedeemECard(req, expected));
    }

    public void testRedeemSubscriptionECardBadCountryId() throws Throwable {
        RedeemECardRequestType req = new RedeemECardRequestType();
        RedeemECardResponseType expected = new RedeemECardResponseType();
        
        initRedeemSubscriptionECardRequest(req);
        req.setCountryCode("INVALID");
        
        expected.setErrorCode(606);
        expected.setErrorMessage(".*");
        
        assertTrue(checkRedeemECard(req, expected));
    }

    public void testRedeemSubscriptionECard()
        throws Throwable
    {
        RedeemECardRequestType req = new RedeemECardRequestType();
        RedeemECardResponseType expected = new RedeemECardResponseType();
        initRedeemSubscriptionECardRequest(req);

        assertTrue(checkRedeemECard(req, expected));
        
        // Extend subscription (get not enough balance)
        expected.setErrorCode(810);
        expected.setErrorMessage(".*Not Enough Money.*");        
        assertTrue(checkRedeemECard(req, expected));
    }

    public void testRedeemSubscriptionECardRedeemKind()
        throws Throwable
    {
        RedeemECardRequestType req = new RedeemECardRequestType();
        RedeemECardResponseType expected = new RedeemECardResponseType();
        initRedeemSubscriptionECardRequest(req);
        req.setRedeemKind(RedeemKindType.Subscription);

        assertTrue(checkRedeemECard(req, expected));
        
        // Extend subscription (get not enough balance)
        expected.setErrorCode(810);
        expected.setErrorMessage(".*Not Enough Money.*");        
        assertTrue(checkRedeemECard(req, expected));
    }

    public void testRedeemSubscriptionECardBadRedeemKind()
        throws Throwable
    {
        RedeemECardRequestType req = new RedeemECardRequestType();
        RedeemECardResponseType expected = new RedeemECardResponseType();
        initRedeemSubscriptionECardRequest(req);
        req.setRedeemKind(RedeemKindType.Title);

        // get bad redeem kind error
        expected.setErrorCode(609);
        expected.setErrorMessage(".*cannot be redeemed for Title.*");        
        assertTrue(checkRedeemECard(req, expected));
    }

    public void testRedeemSubscriptionECardNoType()
        throws Throwable
    {
        RedeemECardRequestType req = new RedeemECardRequestType();
        RedeemECardResponseType expected = new RedeemECardResponseType();
        initRedeemSubscriptionECardRequest(req);
        req.setECard(validSubscriptionECardNoType.getECardPayment());

        assertTrue(checkRedeemECard(req, expected));

        // Extend subscription (get not enough balance)
        expected.setErrorCode(810);
        expected.setErrorMessage(".*Not Enough Money.*");
        assertTrue(checkRedeemECard(req, expected));
    }
}

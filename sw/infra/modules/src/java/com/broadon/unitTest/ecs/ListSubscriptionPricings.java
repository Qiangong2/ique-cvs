package com.broadon.unitTest.ecs;

import com.broadon.wsapi.ecs.ListSubscriptionPricingsRequestType;
import com.broadon.wsapi.ecs.ListSubscriptionPricingsResponseType;
import com.broadon.wsapi.ecs.SubscriptionPricingType;
import com.broadon.wsapi.ecs.PriceType;
import com.broadon.wsapi.ecs.TimeDurationType;
import com.broadon.wsapi.ecs.TimeUnitType;

public class ListSubscriptionPricings extends TestCaseWrapper {
    
    // Some example subscriptions in the DB for lab1
    final static SubscriptionPricingType iQue1MSubscription =
        new SubscriptionPricingType(
                5, IQUE_SUBSCRIPTION_CHANNEL,
                "Subscription Title",
                "60 Days iQue NetC Subscription",
                new TimeDurationType(60, TimeUnitType.day),
                new PriceType("400", "POINTS"),
                new Integer(2));

    final static SubscriptionPricingType iQue3MSubscription =
        new SubscriptionPricingType(
                17, IQUE_SUBSCRIPTION_CHANNEL,
                "Subscription Title",
                "120 Days iQue NetC Subscription",
                new TimeDurationType(120, TimeUnitType.day),
                new PriceType("1000", "POINTS"),
                new Integer(2));

    final static SubscriptionPricingType iQue12MSubscription =
        new SubscriptionPricingType(
                6, IQUE_SUBSCRIPTION_CHANNEL,
                "Subscription Title",
                "360 Days iQue NetC Subscription",
                new TimeDurationType(360, TimeUnitType.day),
                new PriceType("4000", "POINTS"),
                new Integer(2));

    final static SubscriptionPricingType[] sampleSubscriptionPricings 
        = new SubscriptionPricingType[] 
              { iQue1MSubscription, iQue12MSubscription, iQue3MSubscription };

    public static void main(String[] args) {
        junit.textui.TestRunner.run(ListSubscriptionPricings.class);
    }

    public ListSubscriptionPricings(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }
    
    private boolean checkSubscriptionPricing(String message,
            SubscriptionPricingType expected, SubscriptionPricingType resp)
    {
        if (expected == null || resp == null) {
            assertEquals(message, expected, resp);
            return true;
        }
        
        message = message + ".";
        assertEquals(message + "getItemId",
                expected.getItemId(), resp.getItemId());
        assertEquals(message + "getChannelId",
                expected.getChannelId(), resp.getChannelId());
        assertEquals(message + "getChannelName",
                expected.getChannelName(), resp.getChannelName());
        assertEquals(message + "getChannelDescription",
                expected.getChannelDescription(), resp.getChannelDescription());
        assertEquals(message + "getMaxCheckouts",
                expected.getMaxCheckouts(), resp.getMaxCheckouts());
        assertEquals(message + "getSubscriptionLength",
                expected.getSubscriptionLength(), resp.getSubscriptionLength());
        assertEquals(message + "getPrice",
                expected.getPrice(), resp.getPrice());        
        return true;
    }
    
    private boolean checkSubscriptionPricings(
            SubscriptionPricingType[] expected, SubscriptionPricingType[] resp)
    {
        if (expected == null || resp == null) {
            assertEquals("getSubscriptionPricings", expected, resp);
            return true;
        }
        assertEquals("getSubscriptionPricings.length", expected.length, resp.length);
        boolean testResult = true;
        for (int i = 0; i < expected.length; i++) {
            testResult &= checkSubscriptionPricing(
                    "getSubscriptionPricings", expected[i], resp[i]);
        }
        return testResult;
    }
    
    private boolean checkListSubscriptionPricings(
            long deviceId,
            ListSubscriptionPricingsResponseType expected)
        throws Throwable
    {
        boolean testResult = true;

        ListSubscriptionPricingsResponseType resp;
        ListSubscriptionPricingsRequestType req 
            = new ListSubscriptionPricingsRequestType();

        initAbstractRequest(req);
        req.setDeviceId(deviceId);

        resp = ecs.listSubscriptionPricings(req);
        testResult &= checkAbstractResponse(req, resp);

        testResult &= checkError(expected, resp);
        checkSubscriptionPricings(
            expected.getSubscriptionPricings(),
            resp.getSubscriptionPricings());          

        return testResult;
    }

    public void testSubscriptionPricings()
        throws Throwable
    {
        ListSubscriptionPricingsResponseType expected =
            new ListSubscriptionPricingsResponseType();
        expected.setErrorCode(0);
        expected.setSubscriptionPricings(sampleSubscriptionPricings);
        assertTrue(checkListSubscriptionPricings(
                getDeviceId(DEVICE_TYPE_NC, 1000), expected));
    }

}

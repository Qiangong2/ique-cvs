/*
 * JUnit TestCase wrapper for OSS
 *   - this class contains functions that should be shared among OSS 
 *     test cases.
 */
package com.broadon.unitTest.oss;

import junit.framework.TestCase;

import com.broadon.unitTest.Configuration;
import com.broadon.unitTest.TestLog;

public class TestCaseWrapper extends TestCase {
    protected boolean tracing = false;
    
    public TestCaseWrapper(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }
    
    public String getUrlPrefix() {
        return "http://" + 
               Configuration.getSvcHost("oss") + 
               ":" + 
               Configuration.getOssPort() +
               "/oss/common/vc/";
    }
    
    protected void runTest() throws Throwable 
    {
        boolean testResult = true;
        try {
            super.runTest();
        } catch (Throwable e) {
            System.out.println(getName() + " error: " + e.getMessage());
            if (tracing) {
                e.printStackTrace();
            }
            testResult = false;
            throw e;
        } finally {
            TestLog.report("OSS", getName(), testResult);
        }
    }

}

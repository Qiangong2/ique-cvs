/*
 * PCIS TestSuite:  Add new PCIS TestCase here to this class.
 */
package com.broadon.unitTest.pcis;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.broadon.unitTest.Configuration;

public class PcisTestSuite {

	public static void main(String[] args) {
            if (args != null && args.length > 0) {
                TestCaseWrapper.hostName = args[0];
            }
            Configuration.testLab1();
            junit.textui.TestRunner.run(PcisTestSuite.suite());
	}

	public static Test suite() {
		TestSuite suite = new TestSuite("Test for com.broadon.unitTest.pcis");
		//$JUnit-BEGIN$
		suite.addTestSuite(GenerateECard.class);
		//$JUnit-END$
		return suite;
	}

}

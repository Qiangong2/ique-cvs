package com.broadon.unitTest.ias;

import com.broadon.wsapi.ias.RemoveDeviceAccountRequestType;
import com.broadon.wsapi.ias.RemoveDeviceAccountResponseType;

public class RemoveDeviceAccountSuccess extends TestCaseWrapper {

    public static void main(String[] args) {
    }

    public RemoveDeviceAccountSuccess(String name) {
	super(name);
    }

    public void setUp() throws Exception {
	super.setUp();
    }

    public void tearDown() throws Exception {
	super.tearDown();
    }
    
    private boolean checkRemoveDeviceAccountSuccess(
		String deviceId,
                String tokenId,
                String accountId,
                RemoveDeviceAccountResponseType expected)
	throws Throwable
    {
	boolean testResult = true;

        RemoveDeviceAccountResponseType resp;
        RemoveDeviceAccountRequestType req = new RemoveDeviceAccountRequestType();
        
        initRequest(req);
	req.setDeviceId(Long.parseLong(deviceId));
        req.setTokenId(tokenId);
        req.setAccountId(Integer.parseInt(accountId));
		
	resp = ias.removeDeviceAccount(req);
	testResult &= checkDeviceResponse(req, resp);
		
	// Check response against expected response
	testResult &= checkError(expected, resp);
        
        if (testResult) 
            TestDataAdmin.setRemovedDeviceAccount(deviceId, accountId);
        
	return testResult;
    }

    public void testRemoveDeviceAccountSuccess()
	throws Throwable
    {
        // Case 1: Device with valid token and valid account
        String deviceId = TestDataAdmin.getDeviceIdWithValidTokenAndAccount(false);
                
        if (deviceId != null) {
            String token = TestDataAdmin.getValidDeviceToken(deviceId);
            String accounts[] = TestDataAdmin.getValidDeviceAccounts(deviceId);
            
            RemoveDeviceAccountResponseType expected = new RemoveDeviceAccountResponseType();
            expected.setDeviceId(Long.parseLong(deviceId));
            expected.setErrorCode(000);
            expected.setErrorMessage("");            
            assertTrue(checkRemoveDeviceAccountSuccess(deviceId, token, accounts[0], expected));

        } else {
            System.out.println(getName() + " Warning: Failed to obtain a device id with valid token and a linked account");
        }
        
        // Case 2: Device with valid token and removed account
        deviceId = TestDataAdmin.getDeviceIdWithValidTokenAndAccount(true);
                
        if (deviceId != null) {
            String token = TestDataAdmin.getValidDeviceToken(deviceId);
            String accounts[] = TestDataAdmin.getRemovedDeviceAccounts(deviceId);
            
            RemoveDeviceAccountResponseType expected = new RemoveDeviceAccountResponseType();
            expected.setDeviceId(Long.parseLong(deviceId));
            expected.setErrorCode(000);
            expected.setErrorMessage("");
            assertTrue(checkRemoveDeviceAccountSuccess(deviceId, token, accounts[0], expected));

        } else {
            System.out.println(getName() + " Warning: Failed to obtain a device id with valid token and a removed account");
        }
    }    
}

package com.broadon.unitTest.ecs;

import java.util.Arrays;

import com.broadon.wsapi.ecs.ECardInfoType;
import com.broadon.wsapi.ecs.ECardKindType;
import com.broadon.wsapi.ecs.ECardPaymentType;
import com.broadon.wsapi.ecs.CheckECardBalanceRequestType;
import com.broadon.wsapi.ecs.CheckECardBalanceResponseType;
import com.broadon.wsapi.ecs.MoneyType;

public class CheckECardBalance extends TestCaseWrapper {

    public static void main(String[] args) {
        junit.textui.TestRunner.run(CheckECardBalance.class);
    }

    public CheckECardBalance(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    public boolean checkECardInfo(String message, ECardInfoType expected, ECardInfoType resp, boolean checkTimes)
    {
        if (expected == null || resp == null) {
            assertEquals(message, expected, resp);
            return true;
        }
        message = message + ".";
        if (checkTimes) {
            assertEquals(message + "getActivatedTime", expected.getActivatedTime(), resp.getActivatedTime());
            assertEquals(message + "getLastUsedTime", expected.getLastUsedTime(), resp.getLastUsedTime());
            assertEquals(message + "getRevokedTime", expected.getRevokedTime(), resp.getRevokedTime());
        }
        assertEquals(message + "getECardKind", expected.getECardKind(), resp.getECardKind());
        assertEquals(message + "getECardType", expected.getECardType(), resp.getECardType());
        if (expected.getCountries() != null) {
            assertEquals(message + "getCountries ",
                    Arrays.asList(expected.getCountries()).toString(),
                    Arrays.asList(resp.getCountries()).toString());
        } 
        return true;
        
    }
    
    private boolean checkCheckECardBalance(
            CheckECardBalanceRequestType req,
            CheckECardBalanceResponseType expected,
            boolean checkTimes)
        throws Throwable
    {
        boolean testResult = true;

        CheckECardBalanceResponseType resp;

        resp = ecs.checkECardBalance(req);
        testResult &= checkAbstractResponse(req, resp);
            
        // Check response against expected response
        testResult &= checkError(expected, resp);

        testResult &= checkBalance("getBalance", expected.getBalance(), resp.getBalance());
        
        testResult &= checkECardInfo("getECardInfo", expected.getECardInfo(), resp.getECardInfo(), checkTimes);
        
        
        return testResult;
    }

    public void testCheckECardBalance()
        throws Throwable
    {
        makePointsECard();
        CheckECardBalanceRequestType req = new CheckECardBalanceRequestType();
        CheckECardBalanceResponseType expected = new CheckECardBalanceResponseType();
        
        initAbstractRequest(req);

        req.setDeviceId(testDevice);
        req.setVirtualDeviceType(new Integer(DEVICE_TYPE_RV));
        req.setECard(validPointsECard.getECardPayment());
        req.setCountryCode(testCountry);
        
        ECardInfoType eCardInfo = new ECardInfoType();
        eCardInfo.setECardKind(ECardKindType.UseOnce);
        eCardInfo.setECardType("000010" /*validPointsECard.getECardPayment().getECardType()*/);
        expected.setECardInfo(eCardInfo);
        
        MoneyType balance = new MoneyType();
        balance.setAmount("1000");
        balance.setCurrency("POINTS");
        expected.setBalance(balance);
        assertTrue(checkCheckECardBalance(req, expected, false));
    }

}

/* 
 * Use this class to run all tests
 */
package com.broadon.unitTest;

import java.io.FileOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.Class;

import junit.framework.Test;
import junit.framework.TestSuite;
import junit.framework.TestCase;

import com.broadon.unitTest.bms.BmsTestSuite;
import com.broadon.unitTest.cls.ClsTestSuite;
import com.broadon.unitTest.cps.CpsTestSuite;
import com.broadon.unitTest.ecs.EcsTestSuite;
import com.broadon.unitTest.ets.EtsTestSuite;
import com.broadon.unitTest.ias.IasTestSuite;
import com.broadon.unitTest.ogs.OgsTestSuite;
import com.broadon.unitTest.pas.PasTestSuite;
import com.broadon.unitTest.pcis.PcisTestSuite;
import com.broadon.unitTest.nus.NusTestSuite;

public class AllTests extends TestCase {

    public static void main(String[] args) {
        String serverLoc = "lab1";
        String webServices = "all";
        String individualTest = null;

        if (args.length >= 1) {
            serverLoc = args[0];
        }

        if (args.length >= 2) {
            webServices = args[1];
        }

        if (args.length >= 3) {
            individualTest = args[2];
        }

        if (serverLoc.equals("lab1")) {
            Configuration.testLab1();
        } else if (serverLoc.equals("lab2")) {
            Configuration.testLab2();
        } else if (serverLoc.equals("lab3")) {
            Configuration.testLab3();
        } else if (serverLoc.equals("prop")) {
            Configuration.testUseProperties();
        } else if (serverLoc.equals("localhost")) {
            Configuration.testLocalConnection();
        } else if (serverLoc.equals("tcpmon")) {
            Configuration.testTCPMonConnection();
        } else {
            System.out.println("invalid server location - " + serverLoc);
            Usage();
            return;
        }

        // Send JUnit PrintStream to a buffer,
        // so as not to interfere the test outputs.
        System.out.println("\nBuffering JUnit output...\n");

        ByteArrayOutputStream outbuf = new ByteArrayOutputStream();
        PrintStream writer = new PrintStream(outbuf);
        junit.textui.TestRunner testRunner = new junit.textui.TestRunner(writer);

        if (individualTest == null) {

            if (webServices.equals("all")) {
                testRunner.doRun(AllTests.suite());
            } else if (webServices.equals("ets")) {
                testRunner.doRun(EtsTestSuite.suite());
            } else if (webServices.equals("ias")) {
                testRunner.doRun(IasTestSuite.suite());
            } else if (webServices.equals("ecs")) {
                testRunner.doRun(EcsTestSuite.suite());
            } else if (webServices.equals("cps")) {
                testRunner.doRun(CpsTestSuite.suite());
            } else if (webServices.equals("cls")) {
                testRunner.doRun(ClsTestSuite.suite());
            } else if (webServices.equals("ogs")) {
                testRunner.doRun(OgsTestSuite.suite());
            } else if (webServices.equals("pas")) {
                testRunner.doRun(PasTestSuite.suite());
            } else if (webServices.equals("pcis")) {
                testRunner.doRun(PcisTestSuite.suite());
            } else if (webServices.equals("bms")) {
                testRunner.doRun(BmsTestSuite.suite());
            } else if (webServices.equals("nus")) {
                testRunner.doRun(NusTestSuite.suite());
            } else {
                System.out.println("invalid web services - " + webServices);
                Usage();
                return;
            }

        } else {
            // Run one test
            String className = "com.broadon.unitTest." + webServices + "."
                    + individualTest;
            System.out.println("Testname: " + className);
            try {
                Class testClass = Class.forName(className);
                TestSuite suite = new TestSuite("Test");
                suite.addTestSuite(testClass);
                testRunner.doRun(suite);
            } catch (Exception e) {
                System.out.println("cannot find test");
            }
        }

        System.out.println(outbuf.toString());
    }

    public static void Usage() {
        System.out
                .println("AllTests [ServerLocation [WebServices [TestName]]]");
        System.out
                .println("  ServerLocation: lab1,lab2,lab3,prop,localhost,tcpmon");
        System.out.println("  WebServices: all,ets,ias,ecs,cps,cls,ogs,pas");
    }

    public static Test suite() {
        TestSuite suite = new TestSuite("All tests in com.broadon.unitTest");

        // $JUnit-BEGIN$
        suite.addTest(EtsTestSuite.suite());
        suite.addTest(PasTestSuite.suite());
        suite.addTest(IasTestSuite.suite());
        suite.addTest(EcsTestSuite.suite());
        if (Configuration.getProduct() == Configuration.NC) {
            suite.addTest(OgsTestSuite.suite());
            if ("Linux".equals(System.getProperty("os.name")))
                suite.addTest(ClsTestSuite.suite());
        }
        suite.addTest(CpsTestSuite.suite());
        suite.addTest(PcisTestSuite.suite());
        suite.addTest(NusTestSuite.suite());
        //suite.addTest(BmsTestSuite.suite());
        // $JUnit-END$

        return suite;
    }

    public void testAllTests()
        throws Throwable
    {
        System.setOut(
            new PrintStream(
                new FileOutputStream("junit.log"))); 

        Configuration.printConfig();

        junit.textui.TestRunner testRunner = new junit.textui.TestRunner(System.err);
        TestSuite suite = (TestSuite)suite();
        // XXX add perf test to test suite since it is not part of unit tests.
        // TODO - merge perf tests into unit tests.
        suite.addTestSuite(com.broadon.unitTest.ecs.PurchaseTitlePerf.class);

        testRunner.doRun(suite);
    }
}

package com.broadon.unitTest.cps;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import oracle.jdbc.pool.OracleDataSource;

import com.broadon.unitTest.Configuration;

public class DBSetUp
{
    private static final int ERR_CHILD_RECORD_EXIST = 2292;

    private OracleDataSource ds;

    private Connection conn;

    public DBSetUp() throws SQLException {
        ds = new OracleDataSource();
        ds.setURL(Configuration.getDbUrl());
        ds.setUser("bms");
        ds.setPassword("bms");
        conn = ds.getConnection();
        conn.commit();
        conn.setAutoCommit(false);
    }
    
    public synchronized void close() throws SQLException {
        if (conn != null) {
            conn.rollback();
            conn.close();
            conn = null;
        }
        if (ds != null) {
            ds.close();
            ds = null;
        }
    }
    
    protected void finalize() throws Throwable
    {
        close();
    }
    
    static final String DEL_ETKM = 
        "DELETE FROM CONTENT_ETICKET_METADATA WHERE CONTENT_ID >= ? AND CONTENT_ID < ?";
    
    static final String GET_CONTENT_LIST = 
        "SELECT CONTENT_CHECKSUM FROM CONTENTS WHERE CONTENT_ID >= ? AND CONTENT_ID < ?";
    
    static final String DEL_CONTENT =
        "DELETE CONTENTS WHERE CONTENT_ID >= ? AND CONTENT_ID < ?";
    
    static final String DEL_CTO =
        "DELETE CONTENT_TITLE_OBJECTS WHERE TITLE_ID >= ? AND TITLE_ID < ?";
    
    static final String DEL_CACHE = 
        "DELETE CONTENT_CACHE WHERE CONTENT_CHECKSUM = ?";
    
    static final String DEL_TITLE = 
        "DELETE CONTENT_TITLES WHERE TITLE_ID >= ? AND TITLE_ID < ?";
    
    static final String DEL_COUNTRIES =
        "DELETE CONTENT_TITLE_COUNTRIES WHERE TITLE_ID >= ? AND TITLE_ID < ?";
   
    static final String DEL_LOCALES =
        "DELETE CONTENT_TITLE_LOCALES WHERE TITLE_ID >= ? AND TITLE_ID < ?";
   
    static final String DEL_REVIEWS =
        "DELETE CONTENT_TITLE_REVIEWS WHERE TITLE_ID >= ? AND TITLE_ID < ?";
   
    public void removeTitleRange(long start_titleID, long end_titleID) throws SQLException 
    {
        BigDecimal start_cid = genContentID(start_titleID, 0);
        BigDecimal end_cid = genContentID(end_titleID, 0);
        
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(DEL_ETKM);
            ps.setBigDecimal(1, start_cid);
            ps.setBigDecimal(2, end_cid);
            ps.executeUpdate();
            ps.close();
            
            ArrayList csum = new ArrayList();
            ps = conn.prepareStatement(GET_CONTENT_LIST);
            ps.setBigDecimal(1, start_cid);
            ps.setBigDecimal(2, end_cid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                csum.add(rs.getString(1));
            }
            rs.close();
            ps.close();
            
            ps = conn.prepareStatement(DEL_CTO);
            ps.setLong(1, start_titleID);
            ps.setLong(2, end_titleID);
            ps.executeUpdate();
            ps.close();
            
            ps = conn.prepareStatement(DEL_CONTENT);
            ps.setBigDecimal(1, start_cid);
            ps.setBigDecimal(2, end_cid);
            ps.executeUpdate();
            ps.close();
            
            int n = csum.size();
            if (n > 0) {
                ps = conn.prepareStatement(DEL_CACHE);
                for (int i = 0; i < n; ++i) {
                    ps.setString(1, (String) csum.get(i));
                    try {
                        ps.executeUpdate();
                    } catch (SQLException e) {
                        if (e.getErrorCode() != ERR_CHILD_RECORD_EXIST)
                            throw e;
                    }
                }
                ps.close();
            }
            
            ps = conn.prepareStatement(DEL_COUNTRIES);
            ps.setLong(1, start_titleID);
            ps.setLong(2, end_titleID);
            ps.executeUpdate();
            ps.close();
            
            ps = conn.prepareStatement(DEL_LOCALES);
            ps.setLong(1, start_titleID);
            ps.setLong(2, end_titleID);
            ps.executeUpdate();
            ps.close();
            
            ps = conn.prepareStatement(DEL_REVIEWS);
            ps.setLong(1, start_titleID);
            ps.setLong(2, end_titleID);
            ps.executeUpdate();
            ps.close();
            
            ps = conn.prepareStatement(DEL_TITLE);
            ps.setLong(1, start_titleID);
            ps.setLong(2, end_titleID);
            ps.executeUpdate();
            ps.close();
            
            conn.commit();
        } finally {
            if (ps != null)
                ps.close(); 
            conn.rollback();
        }
    }
    
    
    private BigDecimal genContentID(long tid, long cid)
    {
        return new BigDecimal(BigInteger.valueOf(tid).shiftLeft(32).or(BigInteger.valueOf(cid)));
    }

    public void removeTitle(long titleID) throws SQLException {
        removeTitleRange(titleID, titleID + 1);
    }
    
    public void removeChannel(long channelTID) throws SQLException {
        long first = channelTID & 0xFFFFFFFF00000000L;
        long last = first + 0x100000000L;
        removeTitleRange(first, last);
    }
    
    
    static final String GET_ETKM =
        "SELECT ATTRIBITS FROM CONTENT_ETICKET_METADATA WHERE CONTENT_ID=?";
    
    public int getETKMAttributes(long titleID) throws SQLException {
        final long cid4titleKey = 0xFFFFFFFFL;
        BigDecimal cid = genContentID(titleID, cid4titleKey);
        
        conn.commit();
        PreparedStatement ps = conn.prepareStatement(GET_ETKM);
        try {
            ps.setBigDecimal(1, cid);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                int attr = rs.getInt(1);
                return rs.wasNull() ? 0 : attr;
            } else
                throw new SQLException("Cannot locate ETKM record for title " + 
                                       Long.toHexString(titleID));
        } finally {
            ps.close();
        }
    }
    
    public Connection getConnection() throws SQLException {
        return conn;
    }

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        try {
            DBSetUp d = new DBSetUp();
            for (int i = 0; i < args.length; i++) {
                d.removeChannel(Long.parseLong(args[i]));
            }
            d.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}

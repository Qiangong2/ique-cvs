package com.broadon.unitTest.pas;

import com.broadon.wsapi.pas.AuthorizeECardPaymentRequestType;
import com.broadon.wsapi.pas.AuthorizeECardPaymentResponseType;
import com.broadon.wsapi.pas.MoneyType;

public class BadMoneyString extends TestCaseWrapper {

    public static void main(String[] args) {
        junit.textui.TestRunner.run(BadMoneyString.class);
    }

    public BadMoneyString(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
        resetTestDataBase(ECardTests);
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    public boolean checkBadMoneyString() {
        boolean testResult = true;

        MoneyType amount = new MoneyType("POINTS", "10D");
        // from batch03:    000101 0010000000 0014264647    
        String eCardSequence = "0010000000";
        AuthorizeECardPaymentRequestType authReq = createAuthorizationRequest(eCardSequence, amount);
        
        AuthorizeECardPaymentResponseType authResp;
        boolean authReturn=true;
        try {
            authResp = pas.authorizeECardPayment(authReq);
            authReturn = checkAbstractResponse(authReq, authResp);
            if (authReturn) 
                return false;   // fail to check BadMoneyFormat?
            amount = new MoneyType("POINTS", "10 20");
            authResp = pas.authorizeECardPayment(authReq);
            authReturn = checkAbstractResponse(authReq, authResp);
            if (authReturn) 
                return false;   // fail to check BadMoneyFormat?
            return true;
        } catch (Exception e) {
            System.out.println(getStackTrace(e));
            System.out.println(e.getMessage());
            testResult = false;
        }
        
        return testResult;
    }
    
    public void testBadMoneyString() {
        assertTrue(checkBadMoneyString());
    }   
}
/*
 * JUnit TestCase wrapper for NUS
 *   - this class contains functions that should be shared among NUS 
 *     test cases.
 */
package com.broadon.unitTest.nus;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import junit.framework.TestCase;

import com.broadon.unitTest.Configuration;
import com.broadon.unitTest.DBConnection;
import com.broadon.unitTest.ResourcePool;
import com.broadon.unitTest.TestLog;
import com.broadon.wsapi.nus.*;

public class TestCaseWrapper extends TestCase {
    protected NetUpdatePortType nus = null;
    
    public static String COUNTRY_DE = "DE";
    public static String COUNTRY_US = "US";
    public static String COUNTRY_JP = "JP";
    public static String COUNTRY_CN = "CN";
    
    public static String testCountry;
    
    public final static String REGION_IQUE = "IQUE";
    public final static String REGION_TEST = "TEST";
    
    public static String testRegion;
    
    public final static int DEVICE_TYPE_BB = 0;
    public final static int DEVICE_TYPE_RV = 1;
    public final static int DEVICE_TYPE_NC = 2;
    
    public static String testTitles[];
    
    public final static long validRV1 = getDeviceId(DEVICE_TYPE_RV, 1);

    public final static long validNC2 = getDeviceId(DEVICE_TYPE_NC, 2);
    public final static long validNC1 = getDeviceId(DEVICE_TYPE_NC, 1);
    public final static long invalidNC = getDeviceId(DEVICE_TYPE_NC, 0);
    
    public final static long validNCs[] = { validNC1, validNC2 };
    
    public static long testDevice;
    
    protected boolean tracing = false;
    
    public TestCaseWrapper(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
        if (nus == null) {
            nus = ResourcePool.newNetUpdateService();
        }
        
        testTitles = new String[10];
        if (Configuration.getProduct() == Configuration.RVL) {
            testTitles[0] = "0000000100000001";
            testTitles[1] = "0000000100000002";
            testTitles[2] = "0000000100000003";
            testTitles[3] = "0001000000081001";
            testTitles[4] = "0001000000081002";
            testTitles[5] = "0001000000081003";
            testTitles[6] = "0001000000081004";
            testTitles[7] = "0001000000081004";
            testDevice = validRV1;
            testCountry = "UST";
            testRegion = REGION_TEST;
        } else {
            testTitles[0] = "0000000200000001";
            testTitles[1] = "0000000200000002";
            testTitles[2] = "0000000200000003";
            testTitles[3] = "0002000100080020";
            testTitles[4] = "0002000100080030";
            testTitles[5] = "0002000100080021";
            testTitles[6] = "0002000100080033";
            testTitles[7] = "0002000100080033";
            testDevice = validNC1;
            testCountry = COUNTRY_CN;
            testRegion = REGION_IQUE;
        }
    }

    
    public void tearDown() throws Exception {
        super.tearDown();
    }
    
    protected void runTest() throws Throwable 
    {
        boolean testResult = true;
        try {
            super.runTest();
        } catch (Throwable e) {
            System.out.println(getName() + " error: " + e.getMessage());
            if (tracing) {
                e.printStackTrace();
            }
            testResult = false;
            throw e;
        } finally {
            TestLog.report("NUS", getName(), testResult);
        }
    }

    protected void initAbstractRequest(AbstractRequestType req) {
        req.setVersion("1.0");
        req.setDeviceId(testDevice);
        req.setRegionId(testRegion);
        req.setCountryCode(testCountry);
        req.setMessageId(Long.toString(System.currentTimeMillis()));
    }

    protected boolean checkAbstractResponse(
            AbstractRequestType req,
            AbstractResponseType resp)
    {
        assertEquals("getVersion", req.getVersion(), resp.getVersion());
        assertEquals("getDeviceId", req.getDeviceId(), resp.getDeviceId());
        assertEquals("getMessageId", req.getMessageId(), resp.getMessageId());
        return true;
    }
    
    protected boolean checkError(
            AbstractResponseType expected,
            AbstractResponseType resp)
    {
        assertEquals("getErrorCode", expected.getErrorCode(), resp.getErrorCode());
        String expectedPattern = expected.getErrorMessage();
        String message = resp.getErrorMessage();
        if ((expectedPattern != null) && (message != null)) {
            assertTrue("getErrorMessage was '" + message +
                    "', should match '" + expectedPattern + "'",
                    message.matches(expectedPattern));
        } else {
            assertEquals("getErrorMessage", expectedPattern, message);
        }
        return true;
    }

    protected boolean checkTitleVersion(String message, TitleVersionType expected, TitleVersionType resp)
    {
        if (expected == null || resp == null) {
            assertEquals(message, expected, resp);
            return true;
        }
        message = message + ".";
        assertEquals(message + "getTitleId", expected.getTitleId(), resp.getTitleId());
        // Only check version if expected version is positive
        if (expected.getVersion() >= 0) {
            assertEquals(message + "getVersion", expected.getVersion(), resp.getVersion());
        }
        return true;
    }
    
    public boolean checkTitleVersions(String message, TitleVersionType[] expected, TitleVersionType[] resp)
        throws Throwable
    {
        if (expected == null || resp == null) {
            assertEquals(message, expected, resp);
            return true;
        }
        assertEquals(message + "length", expected.length, resp.length);
        boolean testResult = true;
        for (int i = 0; i < expected.length; i++) {
            testResult &= checkTitleVersion(message, expected[i], resp[i]);
        }
        return testResult;
    }
    
    protected static long getDeviceId(int deviceType, int chipId)
    {
        long deviceId = chipId;
        deviceId += ((long) deviceType) << 32;
        return deviceId;
    }
}

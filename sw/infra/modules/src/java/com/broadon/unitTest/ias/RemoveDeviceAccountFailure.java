package com.broadon.unitTest.ias;

import org.apache.axis.AxisFault;

import com.broadon.wsapi.ias.RemoveDeviceAccountRequestType;
import com.broadon.wsapi.ias.RemoveDeviceAccountResponseType;

public class RemoveDeviceAccountFailure extends TestCaseWrapper {

    public static void main(String[] args) {
    }

    public RemoveDeviceAccountFailure(String name) {
	super(name);
    }

    public void setUp() throws Exception {
	super.setUp();
    }

    public void tearDown() throws Exception {
	super.tearDown();
    }
    
    private boolean checkRemoveDeviceAccountException(
		String deviceId,
                String tokenId,
                String accountId)
	throws Throwable
    {
	boolean testResult = true;

        RemoveDeviceAccountRequestType req = new RemoveDeviceAccountRequestType();
        
        initRequest(req);
	req.setDeviceId(Long.parseLong(deviceId));
        req.setTokenId(tokenId);
        req.setAccountId(Integer.parseInt(accountId));
		
	try {
            RemoveDeviceAccountResponseType resp = ias.removeDeviceAccount(req);
        
        } catch (AxisFault a) {
            testResult &= a.getFaultString() != null &&
            a.getFaultString().equals(UNAUTHORIZED_ACCESS_MESSAGE);          
        } 
                
	return testResult;
    }

    private boolean checkRemoveDeviceAccountFailure(
                String deviceId,
                String tokenId,
                String accountId,
                RemoveDeviceAccountResponseType expected)
        throws Throwable
    {
        boolean testResult = true;
    
        RemoveDeviceAccountResponseType resp;
        RemoveDeviceAccountRequestType req = new RemoveDeviceAccountRequestType();
        
        initRequest(req);
        req.setDeviceId(Long.parseLong(deviceId));
        req.setTokenId(tokenId);
        req.setAccountId(Integer.parseInt(accountId));
                    
        resp = ias.removeDeviceAccount(req);
        testResult &= checkDeviceResponse(req, resp);
                    
        // Check response against expected response
        testResult &= checkError(expected, resp);
               
        return testResult;
    }

    public void testRemoveDeviceAccountFailure()
	throws Throwable
    {
        // Case 1: Device with expired token and valid account
        String deviceId = TestDataAdmin.getDeviceIdWithExpiredTokenAndAccount(false);
                
        if (deviceId != null) {
            String token = TestDataAdmin.getExpiredDeviceToken(deviceId);
            String accounts[] = TestDataAdmin.getValidDeviceAccounts(deviceId);
            
            assertTrue(checkRemoveDeviceAccountException(deviceId, token, accounts[0]));

        } else {
            System.out.println(getName() + " Warning: Failed to obtain a device id with expired token and a linked account");
        }
        
        // Case 2: Device with valid token and no accounts
        deviceId = TestDataAdmin.getDeviceIdWithValidTokenAndNoAccount();
                
        if (deviceId != null) {
            String token = TestDataAdmin.getValidDeviceToken(deviceId);
            String accountId = TestDataAdmin.getRegisteredAccountId();
            
            RemoveDeviceAccountResponseType expected = new RemoveDeviceAccountResponseType();
            expected.setDeviceId(Long.parseLong(deviceId));
            expected.setErrorCode(925);
            expected.setErrorMessage("IAS - Failed to update device account");
            assertTrue(checkRemoveDeviceAccountFailure(deviceId, token, accountId, expected));

        } else {
            System.out.println(getName() + " Warning: Failed to obtain a device id with valid token and no linked account");
        }
    }    
}

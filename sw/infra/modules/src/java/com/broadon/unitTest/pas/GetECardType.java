package com.broadon.unitTest.pas;

import com.broadon.wsapi.pas.CheckECardBalanceRequestType;
import com.broadon.wsapi.pas.CheckECardBalanceResponseType;
import com.broadon.wsapi.pas.GetECardTypeRequestType;
import com.broadon.wsapi.pas.GetECardTypeResponseType;

public class GetECardType extends TestCaseWrapper {

    public static void main(String[] args) {
        junit.textui.TestRunner.run(GetECardType.class);
    }

    public GetECardType(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
        resetTestDataBase(ECardTests);
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    public boolean checkGetECardType() {
//      from batch03:    000101 0010000000 0014264647    
        String eCardSequence = "0010000000";
        GetECardTypeRequestType getECardTypeReq = createGetECardTypeRequest(eCardSequence);
        GetECardTypeResponseType getECardTypeResp;
        
        boolean testResult=true;

        try {
            getECardTypeResp = pas.getECardType(getECardTypeReq);
            testResult &= checkAbstractResponse(getECardTypeReq, getECardTypeResp);
        } catch (Exception e) {
            System.out.println(getStackTrace(e));
            System.out.println(e.getMessage());
            testResult = false;
        }
    
        return testResult;
    }
    
    public void testCheckECardBalance() {
        assertTrue(checkGetECardType());
    }  
}

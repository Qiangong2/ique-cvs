package com.broadon.unitTest.ecs;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Random;

import com.broadon.unitTest.Configuration;
import com.broadon.unitTest.DBConnection;
import com.broadon.wsapi.ecs.AccountPaymentType;
import com.broadon.wsapi.ecs.LimitType;
import com.broadon.wsapi.ecs.PaymentMethodType;
import com.broadon.wsapi.ecs.PaymentType;
import com.broadon.wsapi.ecs.PriceType;
import com.broadon.wsapi.ecs.PurchaseTitleRequestType;
import com.broadon.wsapi.ecs.PurchaseTitleResponseType;

public class PurchaseTitlePerf extends TestCaseWrapper {
    static final int TOTAL_ACCOUNTS = 1000000;
    static final int ACCOUNT_OFFSET = 888000000;
    static final int MAX_THREADS = 100;
    static final String TITLE_PREFIX = "100000009";
    static final int MAX_TITLES = 1000;

    static int threadCounter;
    static int maxPerPC;
    static int maxPerThread;
    static int acctOffset;
    static int deviceOffset;
    static String certPath;
    static boolean verbose;
    
    int threadId;
    int acctCounter;
    byte[] myCert;
    PaymentType myAccount;
    long myDevice;
    int myTitle;
    Random generator;

    PurchaseTitleRequestType req = new PurchaseTitleRequestType();
    PurchaseTitleResponseType expected = new PurchaseTitleResponseType();

    static {
        Properties prop = Configuration.getPerfProperties();

        verbose = (prop.getProperty("verbose") != null);

        threadCounter = Integer.parseInt(prop.getProperty("threadCounter", "0"));
        certPath = prop.getProperty("certPath", "./");
        int total = Integer.parseInt(prop.getProperty("totalClients", "10"));
        int id = Integer.parseInt(prop.getProperty("clientId", "0"));

        maxPerPC = TOTAL_ACCOUNTS/total;
        acctOffset = ACCOUNT_OFFSET+id*(maxPerPC);
        deviceOffset = id*maxPerPC;

        maxPerThread = maxPerPC/MAX_THREADS;

        debug("clientId: " + id +
              " maxPerPC: " + maxPerPC +
              " maxPerThread: " + maxPerThread + 
              " acctOff:" + acctOffset + 
              " deviceOff:" + deviceOffset);
    }

    public static void debug(String s) {
        if (verbose) System.out.println(s);
    }

    public PurchaseTitlePerf(String name) {
        super(name);
        threadId = getThreadId();
        acctCounter = maxPerThread;
        myCert = new byte[384];
        myAccount = new PaymentType();
        myAccount.setAccountPayment(new AccountPaymentType());
        generator = new Random(threadId);
    }

    public static void main(String[] args) {
        junit.textui.TestRunner.run(PurchaseTitlePerf.class);
    }

    private static void resetETickets(long start, long end) {
        // DB Parameters
        String db_user = "ecs";
        String db_pswd = "ecs";
        String db_url = Configuration.getDbUrl();

        String sql = "delete from ETICKETS where device_id between " + 
            start + " and " + end; 
        debug("sql: "+sql);
        
        Connection conn = null;
        PreparedStatement pstmt = null;
        try {
            DBConnection db = new DBConnection(db_user, db_pswd, db_url);
            conn = db.getConnection();
            pstmt = conn.prepareStatement(sql);
            pstmt.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (conn != null)
                    conn.close();
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    
    private static synchronized int getThreadId() {
        int ret;
        if (threadCounter >= MAX_THREADS) {
            threadCounter = 0;
        }
        ret = threadCounter;
        threadCounter++;
        debug("threadId: " + ret);
        return ret;
    }

    private void readMyCert()
    {        
        String hex = (Long.toHexString(myDevice)).substring(1);
        String certFilename = 
            hex.substring(0,2) + "/" +
            hex.substring(2,4) + "/" +
            hex.substring(4,6) + "/" +
            hex.substring(6,8) + ".cert";
        certFilename = certPath + "/" + certFilename;
        debug("cert file: " + certFilename);
        try {
            File certFile = new File(certFilename);
            BufferedInputStream iStream = new BufferedInputStream(new FileInputStream(certFile));
            iStream.read(myCert);        
        } catch (Exception e) {
            System.err.println("Cannot read cert - "+certFilename);
        }
    }
    
    public void setUp() throws Exception {
        super.setUp();

        acctCounter++;
        if (acctCounter >= maxPerThread) {
            acctCounter = 0;
            resetETickets(getDeviceId(DEVICE_TYPE_RV,
                                      deviceOffset + threadId*maxPerThread),
                          getDeviceId(DEVICE_TYPE_RV,
                                      deviceOffset + (threadId+1)*maxPerThread - 1));
            myTitle = generator.nextInt(MAX_TITLES) + 1;
        }

        String acctNum = 
            Integer.toString(acctOffset + threadId*maxPerThread + acctCounter);
        myAccount.setPaymentMethod(PaymentMethodType.ACCOUNT);
        myAccount.getAccountPayment().setAccountNumber(acctNum);
        myAccount.getAccountPayment().setPin("30000");

        myDevice = getDeviceId(DEVICE_TYPE_RV,
                               deviceOffset + threadId*maxPerThread + acctCounter);
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    private boolean checkPurchaseTitle(
            PurchaseTitleRequestType req,
            PurchaseTitleResponseType expected)
        throws Throwable
    {
        boolean testResult = true;

        PurchaseTitleResponseType resp;

        resp = ecs.purchaseTitle(req);
        testResult &= checkAbstractResponse(req, resp);
            
        // Check response against expected response
        testResult &= checkError(expected, resp);

        return testResult;
    }

    /**
     * Initializes request to purchase a permanent title
     * @param req
     */
    protected void initPurchaseRequest(PurchaseTitleRequestType req) {
        req.setVersion("1.0");
        req.setRegionId("test");
        req.setCountryCode("JPT");
        req.setMessageId(Long.toString(System.currentTimeMillis()));
        req.setDeviceId(myDevice);
        req.setPayment(myAccount);
        req.setItemId(10000+myTitle);
        req.setTitleId("000100000009"+Integer.toHexString(0x10000+myTitle).substring(1));
        req.setLimits(new LimitType[] { new LimitType(0, "PR")});
        req.setPrice(new PriceType("2000", "POINTS"));
        readMyCert();
        req.setDeviceCert(myCert);

        System.out.println("account: "+myAccount.getAccountPayment().getAccountNumber()+
                           " device: "+Long.toHexString(myDevice)+
                           " title: " +req.getTitleId());
    }
    
    public void testPurchaseTitle()
        throws Throwable
    {
        initPurchaseRequest(req);
        assertTrue(checkPurchaseTitle(req, expected));
    }
}

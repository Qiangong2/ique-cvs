package com.broadon.unitTest.bms;

import java.io.IOException;
import org.xml.sax.SAXException;

import com.broadon.unitTest.Configuration;
import com.meterware.httpunit.*;

public class Login extends TestCaseWrapper
{
    WebRequest loginReq;
    String loginID;
    String loginPassword;
    

    public Login(String name) {
	super(name);
    }

    protected void setUp() throws Exception {
	super.setUp();

	loginReq = server.getLoginRequest();
	loginID = server.getLoginName();
	loginPassword = server.getLoginPassword();

	// make sure we are not logged in
	WebResponse res = wc.getCurrentPage();
	WebLink link = res.getLinkWithID(logout_id);
	if (link != null) {
	    link.click();
	}
    }

    private WebResponse login() throws Exception
    {
	WebResponse res = wc.getResponse(loginReq);
	WebForm form = res.getFormWithName("theForm");
	form.setParameter("email", loginID);
        form.setParameter("pwd", loginPassword);
        res = form.submit();
	WebLink link = res.getLinkWithID(logout_id);
	if (link == null) 
	    throw new IOException("Login in failed -- cannot find \"Logout\" link on home page");
	return res;
    }

    protected void tearDown() {
	// make sure we are logged in again
	try {
	    WebResponse res = wc.getCurrentPage();
	    WebLink link = res.getLinkWithID(logout_id);
	    if (link == null) {
		login();
	    }
	} catch (Exception e) {
	    System.err.println("Error in tearing down test case \"TestLogin\"");
	    e.printStackTrace();
	}

	super.tearDown();
    }

    /** Test wrong user id, password, invalid input, etc. */
    public void testUnsuccessfulLogin()
	throws IOException, SAXException
    {
	WebResponse res = wc.getResponse(loginReq);
	WebForm form = res.getFormWithName("theForm");
	// no input
	res = form.submit();
	assertEquals("Please provide your login.", wc.popNextAlert());

	// no login name
	form.setParameter("pwd", loginPassword);
	res = form.submit();
	assertEquals("Please provide your login.", wc.popNextAlert());

	// no password
	form.setParameter("email", loginID);
	form.setParameter("pwd", "");
	res = form.submit();
	assertEquals("Please provide your password.", wc.popNextAlert());

	// incorrect password
	form.setParameter("email", "bad id");
	form.setParameter("pwd", "bad password");
	res = form.submit();
	// should have no pop up
	assertEquals("", wc.popNextAlert());
	assertNull("Allowed log in with wrong password",
		   res.getLinkWithID(logout_id));

    } // testNegativeInput
    

    /** A positive login test */
    public void testSuccessfulLogin() {
	try {
	    login();
	} catch (Exception e) {
	    fail(e.getMessage());
	}
    }
    
    public static void main(String[] args)
    {
        if (args != null && args.length > 0)
            hostName = args[0];
        Configuration.testLab1();
        junit.textui.TestRunner.run(Login.class);
    }
}
    

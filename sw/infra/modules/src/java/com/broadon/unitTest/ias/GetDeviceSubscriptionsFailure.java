package com.broadon.unitTest.ias;

import org.apache.axis.AxisFault;

import com.broadon.wsapi.ias.GetDeviceSubscriptionsRequestType;
import com.broadon.wsapi.ias.GetDeviceSubscriptionsResponseType;

public class GetDeviceSubscriptionsFailure extends TestCaseWrapper {

    public static void main(String[] args) {
    }

    public GetDeviceSubscriptionsFailure(String name) {
	super(name);
    }

    public void setUp() throws Exception {
	super.setUp();
    }

    public void tearDown() throws Exception {
	super.tearDown();
    }
    
    private boolean checkGetDeviceSubscriptionsException (
		String deviceId,
                String tokenId,
                String subType)
	throws Throwable
    {
	boolean testResult = true;

        GetDeviceSubscriptionsRequestType req = new GetDeviceSubscriptionsRequestType();
        
        initRequest(req);
	req.setDeviceId(Long.parseLong(deviceId));
        req.setTokenId(tokenId);
        req.setSubscriptionType(subType);		

        try {
            GetDeviceSubscriptionsResponseType resp = ias.getDeviceSubscriptions(req);
            
        } catch (AxisFault a) {
            testResult &= a.getFaultString() != null &&
                a.getFaultString().equals(UNAUTHORIZED_ACCESS_MESSAGE);          
        }
        
	return testResult;
    }

    public void testGetDeviceSubscriptionsFailure()
	throws Throwable
    {
        // Case 1: Device with expired token and with a subscription
        String deviceId = TestDataAdmin.getDeviceIdWithExpiredTokenAndSubscription(true);
        
        if (deviceId != null) {
            String token = TestDataAdmin.getExpiredDeviceToken(deviceId);
            //System.out.println("1. deviceId: " + deviceId +", token: " + token);
            
            assertTrue(checkGetDeviceSubscriptionsException(deviceId, token, null));
        } else {
            System.out.println(getName() + " Warning: Failed to obtain a device id with expired token and a subscription");
        }
        
        // Case 2: Device with expired token and no subscription
        deviceId = TestDataAdmin.getDeviceIdWithExpiredTokenAndSubscription(false);
        
        if (deviceId != null) {
            String token = TestDataAdmin.getExpiredDeviceToken(deviceId);
            //System.out.println("2. deviceId: " + deviceId +", token: " + token);
            
            assertTrue(checkGetDeviceSubscriptionsException(deviceId, token, null));
        } else {
            System.out.println(getName() + " Warning: Failed to obtain a device id with expired token and no subscription");
        }
    }
}

package com.broadon.unitTest.ias;

import org.apache.axis.AxisFault;

import com.broadon.wsapi.ias.GetDeviceAccountsRequestType;
import com.broadon.wsapi.ias.GetDeviceAccountsResponseType;

public class GetDeviceAccountsFailure extends TestCaseWrapper {

    public static void main(String[] args) {
    }

    public GetDeviceAccountsFailure(String name) {
	super(name);
    }

    public void setUp() throws Exception {
	super.setUp();
    }

    public void tearDown() throws Exception {
	super.tearDown();
    }
    
    private boolean checkGetDeviceAccountsException(
		String deviceId,
                String tokenId)
	throws Throwable
    {
	boolean testResult = true;
	
        GetDeviceAccountsRequestType req = new GetDeviceAccountsRequestType();
        
        initRequest(req);
	req.setDeviceId(Long.parseLong(deviceId));
        req.setTokenId(tokenId);
		
        try {
            GetDeviceAccountsResponseType resp = ias.getDeviceAccounts(req);            
        } catch (AxisFault a) {
            testResult &= a.getFaultString() != null &&
            a.getFaultString().equals(UNAUTHORIZED_ACCESS_MESSAGE);          
        } 
	              
	return testResult;
    }

    public void testGetDeviceAccountsFailure()
	throws Throwable
    {
        // Case 1: Get linked device accounts with an expired device token
        Token t = TestDataAdmin.getExpiredDeviceToken();
                
        if (t != null) {            
            assertTrue(checkGetDeviceAccountsException(t.id, t.tokenId));
        } else {
            System.out.println(getName() + " Warning: Failed to obtain an expired device token");
        }
    }    
}

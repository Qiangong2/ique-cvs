package com.broadon.unitTest;

import java.io.*;
import java.sql.*;
import oracle.jdbc.pool.OracleDataSource;


/** A class that maintains a single DB connection for the test,
 *  using javalib utilities.  
 */
public class DBConnection
{
    OracleDataSource ds = null;
    Connection conn = null;

    private void commitBatch(String[] stmts) throws IOException
    {
        Statement  stmt = null;
        
        try {
            int i;            
            stmt = conn.createStatement();

            for (i = 0; i < stmts.length; ++i) {
                stmt.addBatch(stmts[i]);
            }

            int[] results = stmt.executeBatch();

            for (i = 0; i < stmts.length; ++i) {
                if (results[i] == Statement.EXECUTE_FAILED)
                    throw new IOException("Failed to execute SQL: " +
                                          stmts[i]);
            }

            conn.commit();
        }
        catch (Throwable e) {
            throw new IOException("Failed to execute SQL batch: " +
                                  e.getMessage());
        }
        finally {
            try {
                if (stmt != null)
                    stmt.close();                
            }
            catch (Throwable e)
            {}
        }
    } // commitBatch

    
    private String[] splitStmts(String stmts)
    {
        if (stmts == null || stmts.length() == 0) {
            return new String[0];
        }
        else {
            return stmts.split(";");
        }
    }


    public DBConnection(String db_user,
                        String db_passwd,
                        String db_url) throws SQLException
    {
        ds = new OracleDataSource();
        ds.setURL(db_url);
        ds.setUser(db_user);
        ds.setPassword(db_passwd);
        
        conn = ds.getConnection();
    }

    public Connection getConnection()
    {
       return conn;        
    }
    
    public void closeConnection()
    {
        try {
            if (conn != null)            
                conn.close();
        }
        catch (Throwable e)
        {}
    }

    public void setup(String stmts) throws IOException
    {
        String[] setupStmts = splitStmts(stmts);
        
        if (setupStmts != null) {
            commitBatch(setupStmts);
        }
    }

    public void cleanup(String stmts) throws IOException
    {
        String[] cleanupStmts = splitStmts(stmts);
        
        if (cleanupStmts != null) {
            commitBatch(cleanupStmts);
        }
    }
}

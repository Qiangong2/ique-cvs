package com.broadon.unitTest.pcis;

import com.broadon.unitTest.ResourcePool;
import com.broadon.wsapi.pcis.PrepaidCardGenerationPortType;
import com.broadon.wsapi.pcis.PrepaidCardGenerationRequestType;
import com.broadon.wsapi.pcis.PrepaidCardGenerationResponseType;

public class GenerateECard extends TestCaseWrapper {
    PrepaidCardGenerationPortType pcis;
    public static void main(String[] args) {
        junit.textui.TestRunner.run(GenerateECard.class);
    }

    public GenerateECard(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
        pcis = ResourcePool.newPrepaidCardGenerationService();
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    public boolean checkGenerateECard() {
        PrepaidCardGenerationRequestType req = new PrepaidCardGenerationRequestType();
        initAbstractRequest(req);
        req.setECardType(10);
        req.setOperatorId(555);
        req.setPassword("555");
        req.setQuantity(1);
        req.setReferenceId2("this is a test");
        System.out.println("sending request");
        boolean genReturn;;
        try {
            PrepaidCardGenerationResponseType resp = pcis.prepaidCardGeneration(req);
            genReturn = checkAbstractResponse(req, resp);
        } catch (Exception e) {
            System.out.println(getStackTrace(e));
            System.out.println(e.getMessage());
            genReturn = false;
        }
        
        return genReturn;
    }
    
    public void testGenerateECard() {
        assertTrue(checkGenerateECard());
    }   
}
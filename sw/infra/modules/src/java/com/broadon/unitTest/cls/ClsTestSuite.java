package com.broadon.unitTest.cls;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.broadon.unitTest.Configuration;

public class ClsTestSuite
{
    public static void main(String[] args) {
        if (args != null && args.length > 0) {
            TestCaseWrapper.hostName = args[0];
        }
        Configuration.testLab1();
        junit.textui.TestRunner.run(ClsTestSuite.suite());
    }
    
    public static Test suite() {
        TestSuite suite = new TestSuite("Test for com.broadon.unitTest.cls");
        //$JUnit-BEGIN$
        suite.addTestSuite(CreateRegularTitle.class);
        suite.addTestSuite(SubscriptionChannel.class);
        suite.addTestSuite(SubscriptionTitle.class);
        suite.addTestSuite(CommonTicket.class);
        suite.addTestSuite(Attachments.class);
        //$JUnit-END$
        return suite;
    }
}

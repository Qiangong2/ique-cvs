package com.broadon.unitTest.ias;

import com.broadon.wsapi.ias.ValidateAccountTokenRequestType;
import com.broadon.wsapi.ias.ValidateAccountTokenResponseType;

public class ValidateAccountTokenFailure extends TestCaseWrapper {

    public static void main(String[] args) {
    }

    public ValidateAccountTokenFailure(String name) {
	super(name);
    }

    public void setUp() throws Exception {
	super.setUp();
    }

    public void tearDown() throws Exception {
	super.tearDown();
    }

    private boolean checkValidateAccountTokenFailure(
		String accountId, 
		String accountToken,
                ValidateAccountTokenResponseType expected)
	throws Throwable
    {
	boolean testResult = true;

        ValidateAccountTokenResponseType resp;
        ValidateAccountTokenRequestType req = new ValidateAccountTokenRequestType();

        initRequest(req);
	req.setAccountId(Integer.parseInt(accountId));
	req.setTokenId(accountToken);
		
	resp = ias.validateAccountToken(req);
	testResult &= checkAccountResponse(req, resp);
		
	// Check response against expected response
	testResult &= checkError(expected, resp);

	return testResult;
    }

    public void testValidateAccountTokenFailure()
	throws Throwable
    {
        Token et = TestDataAdmin.getExpiredAccountToken();
        
        if (et != null) {
            // Case 1: Validate a token that has expired
            ValidateAccountTokenResponseType expected = new ValidateAccountTokenResponseType();
            
            expected.setAccountId(Integer.parseInt(et.id));
            expected.setErrorCode(904);
            expected.setErrorMessage("IAS - Invalid account token");
            assertTrue(checkValidateAccountTokenFailure(et.id, et.tokenId, expected));
        } else {
            System.out.println(getName() + " Warning: Failed to obtain an expired account token");
        }
    }
}

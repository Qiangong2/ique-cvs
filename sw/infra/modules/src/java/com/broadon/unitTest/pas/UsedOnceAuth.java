package com.broadon.unitTest.pas;

import com.broadon.wsapi.pas.AuthorizeECardPaymentRequestType;
import com.broadon.wsapi.pas.AuthorizeECardPaymentResponseType;
import com.broadon.wsapi.pas.MoneyType;
import com.broadon.wsapi.pas.VoidAuthorizationRequestType;
import com.broadon.wsapi.pas.VoidAuthorizationResponseType;
import com.broadon.wsapi.pas.PasTokenType;

public class UsedOnceAuth extends TestCaseWrapper {

    public static void main(String[] args) {
        junit.textui.TestRunner.run(UsedOnceAuth.class);
    }

    public UsedOnceAuth(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
        resetTestDataBase(ECardTests);
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    public boolean checkAuthorizeECaedPayment() {
        boolean testResult = true;
        MoneyType amount = new MoneyType("POINTS", "10");
//      from batch03:    000101 0010000000 0014264647    
        String eCardSequence = "0010000000";
        AuthorizeECardPaymentRequestType authReq = createAuthorizationRequest(eCardSequence, amount);

        AuthorizeECardPaymentResponseType authResp;
        VoidAuthorizationResponseType voidResp;
        boolean authReturn=false, voidReturn=false;

        try {
            authResp = pas.authorizeECardPayment(authReq);
            authReturn = checkAbstractResponse(authReq, authResp);
            if (authReturn) {   
                // save void info first
                PasTokenType token = authResp.getAuthorizationToken();
                VoidAuthorizationRequestType voidReq = createVoidAuthorizationRequest(token);
                authResp = pas.authorizeECardPayment(authReq);
                authReturn = checkAbstractResponse(authReq, authResp);
                // go ahead to void it
                
                voidResp = pas.voidAuthorization(voidReq);
                voidReturn = checkAbstractResponse(voidReq, voidResp);
            }
            testResult &= authReturn;
            testResult &= voidReturn;
        } catch (Exception e) {
            System.out.println(getStackTrace(e));
            System.out.println(e.getMessage());
            testResult = false;
        }
        
        return testResult;
    }
    
    public void testUsedOnceAuth() {
        assertTrue(checkAuthorizeECaedPayment());
    }   

}

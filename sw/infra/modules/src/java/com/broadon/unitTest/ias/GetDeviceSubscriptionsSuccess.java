package com.broadon.unitTest.ias;

import com.broadon.wsapi.ias.GetDeviceSubscriptionsRequestType;
import com.broadon.wsapi.ias.GetDeviceSubscriptionsResponseType;
import com.broadon.wsapi.ias.DeviceSubscriptionType;

public class GetDeviceSubscriptionsSuccess extends TestCaseWrapper {

    public static void main(String[] args) {
    }

    public GetDeviceSubscriptionsSuccess(String name) {
	super(name);
    }

    public void setUp() throws Exception {
	super.setUp();
    }

    public void tearDown() throws Exception {
	super.tearDown();
    }
    
    private boolean checkGetDeviceSubscriptionsSuccess (
		String deviceId,
                String tokenId,
                String subType,
                int subCount,
                GetDeviceSubscriptionsResponseType expected)
	throws Throwable
    {
	boolean testResult = true;

        GetDeviceSubscriptionsResponseType resp;
        GetDeviceSubscriptionsRequestType req = new GetDeviceSubscriptionsRequestType();
        
        initRequest(req);
	req.setDeviceId(Long.parseLong(deviceId));
        req.setTokenId(tokenId);
        req.setSubscriptionType(subType);

	resp = ias.getDeviceSubscriptions(req);
	testResult &= checkDeviceResponse(req, resp);
		
	// Check response against expected response
	testResult &= checkError(expected, resp);
        
        DeviceSubscriptionType[] subs = resp.getDeviceSubscriptions();
        
        // Check for subscriptions
        if (subs != null) {
            assertTrue(subs.length == subCount);
            if (subType != null && !subType.equals(""))
                assertTrue(subs[0].getSubscriptionType().equals(subType));
        } else
            assertTrue(subCount == 0);
                
	return testResult;
    }

    public void testGetDeviceSubscriptionsSuccess()
	throws Throwable
    {
        // Case 1: Device with valid token and with a subscription
        String deviceId = TestDataAdmin.getDeviceIdWithValidTokenAndSubscription(true);
        
        if (deviceId != null) {
            String token = TestDataAdmin.getValidDeviceToken(deviceId);
            int subCount = TestDataAdmin.getDeviceSubscriptions(deviceId).length;
            //System.out.println("1. deviceId: " + deviceId +", token: " + token + ", subCount: " + subCount);
            
            GetDeviceSubscriptionsResponseType expected = new GetDeviceSubscriptionsResponseType();
            expected.setDeviceId(Long.parseLong(deviceId));
            expected.setErrorCode(000);
            expected.setErrorMessage("");
            
            assertTrue(checkGetDeviceSubscriptionsSuccess(deviceId, token, null, subCount, expected));
        } else {
            System.out.println(getName() + " Warning: Failed to obtain a device id with valid token and a subscription");
        }
        
        // Case 2: Device with valid token and no subscription
        deviceId = TestDataAdmin.getDeviceIdWithValidTokenAndSubscription(false);
        
        if (deviceId != null) {
            String token = TestDataAdmin.getValidDeviceToken(deviceId);
            //System.out.println("2. deviceId: " + deviceId +", token: " + token + ", subCount: 0");
            
            GetDeviceSubscriptionsResponseType expected = new GetDeviceSubscriptionsResponseType();
            expected.setDeviceId(Long.parseLong(deviceId));
            expected.setErrorCode(000);
            expected.setErrorMessage("");
            
            assertTrue(checkGetDeviceSubscriptionsSuccess(deviceId, token, null, 0, expected));
        } else {
            System.out.println(getName() + " Warning: Failed to obtain a device id with valid token and no subscription");
        }

        // Case 3: Device with valid token and a specified subscription
        deviceId = TestDataAdmin.getDeviceIdWithValidTokenAndSubscription(true);
        
        if (deviceId != null) {
            String token = TestDataAdmin.getValidDeviceToken(deviceId);
            String[] subTypes = TestDataAdmin.getDeviceSubscriptions(deviceId);
            //System.out.println("3. deviceId: " + deviceId +", token: " + token + ", subType: " + subTypes[0]);
            
            GetDeviceSubscriptionsResponseType expected = new GetDeviceSubscriptionsResponseType();
            expected.setDeviceId(Long.parseLong(deviceId));
            expected.setErrorCode(000);
            expected.setErrorMessage("");
            
            assertTrue(checkGetDeviceSubscriptionsSuccess(deviceId, token, subTypes[0], 1, expected));
        } else {
            System.out.println(getName() + " Warning: Failed to obtain a device id with valid token and a subscription");
        }
    }    
}

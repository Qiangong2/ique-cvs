package com.broadon.unitTest.pas;

import java.rmi.RemoteException;

import com.broadon.wsapi.pas.AuthorizeECardPaymentRequestType;
import com.broadon.wsapi.pas.AuthorizeECardPaymentResponseType;
import com.broadon.wsapi.pas.BalanceAccountType;
import com.broadon.wsapi.pas.CaptureECardToAccountRequestType;
import com.broadon.wsapi.pas.CaptureECardToAccountResponseType;
import com.broadon.wsapi.pas.CreateAccountRequestType;
import com.broadon.wsapi.pas.CreateAccountResponseType;
import com.broadon.wsapi.pas.PasTokenType;
import com.broadon.wsapi.pas.MoneyType;

public class CaptureECardToAcct extends TestCaseWrapper {
    CreateAccountRequestType req;
    CreateAccountResponseType resp;
    BalanceAccountType acct = null;
    PasTokenType token = null;
    MoneyType amount = null;
    public static void main(String[] args) {
        junit.textui.TestRunner.run(CaptureECardToAcct.class);
    }

    public CaptureECardToAcct(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
        resetTestDataBase(AccountTests);
        acct = new BalanceAccountType();
        acct.setAccountNumber("30001");
        acct.setPIN("30001");  
        req = createCreateAccountRequest(acct);
        resp = pas.createAccount(req);
        amount = new MoneyType("POINTS", "10");
        if (checkAbstractResponse(req, resp)) {
            AuthorizeECardPaymentRequestType authReq = createAuthorizationRequest(
                    "0010030001", amount);
            AuthorizeECardPaymentResponseType authResp;
            authResp = pas.authorizeECardPayment(authReq);
            if (checkAbstractResponse(authReq, authResp)) {   // no error, go ahead to void it
                token = authResp.getAuthorizationToken();
            }
        }
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }
    
    public boolean checkCaptureECardToAcct() throws RemoteException {
        boolean testResult = true;
        amount = new MoneyType("POINTS", "9");
        CaptureECardToAccountRequestType depReq = createCaptureECardToAccountRequest(token, acct, amount);
        try {
            CaptureECardToAccountResponseType resp = pas.captureECardToAccount(depReq);
            testResult = checkAbstractResponse(depReq, resp);
        } catch (Exception e) {
            System.out.println(getStackTrace(e));
            System.out.println(e.getMessage());
            testResult = false;
        }
        return testResult;
    }

    public void testCaptureECardToAcct() throws RemoteException {
        assertTrue(checkCaptureECardToAcct());
    }   
}
/*
 * PAS TestSuite:  Add new PAS TestCase here to this class.
 */
package com.broadon.unitTest.pas;

import com.broadon.unitTest.Configuration;

import junit.framework.Test;
import junit.framework.TestSuite;

public class PasTestSuite {
    public static void main(String[] args) {
        junit.textui.TestRunner.run(PasTestSuite.suite());
    }

    public static Test suite() {
        TestSuite suite = new TestSuite("Test for com.broadon.unitTest.pas");
        //$JUnit-BEGIN$
        if (Configuration.getProduct() == Configuration.NC) {
            // legacy tests only works on lab1 db setting
            suite.addTestSuite(AuthorizeECardPayment.class);
            suite.addTestSuite(BadECardId.class);
            suite.addTestSuite(BadMoneyString.class);
            suite.addTestSuite(CaptureTooMuchError.class);
            suite.addTestSuite(CheckECardBalance.class);
            suite.addTestSuite(GetAuthorizationHistory.class);
            suite.addTestSuite(GetECardHistory.class);
            suite.addTestSuite(RefundECardPayment.class);
            suite.addTestSuite(RefundTooMuchError.class);
            suite.addTestSuite(UsedOnceAuth.class);
        }
        suite.addTestSuite(ECardTest.class);
        suite.addTestSuite(AccountTest.class);
        suite.addTestSuite(CreditCardTest.class);
        suite.addTestSuite(AuthCaptureAcctToAcct.class);
        //$JUnit-END$
        return suite;
    }

}

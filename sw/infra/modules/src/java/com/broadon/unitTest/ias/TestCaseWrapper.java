/* A wrapper around the Junit TestCase.
 *   - Shared initialization code should be inserted here.
 */
/*
 * JUnit TestCase wrapper for IAS
 *   - this class contains functions that should be shared among IAS 
 *     test cases.
 */
package com.broadon.unitTest.ias;

import junit.framework.TestCase;

import com.broadon.unitTest.ResourcePool;
import com.broadon.unitTest.TestLog;
import com.broadon.wsapi.ias.AbstractAccountRequestType;
import com.broadon.wsapi.ias.AbstractAccountResponseType;
import com.broadon.wsapi.ias.AbstractDeviceRequestType;
import com.broadon.wsapi.ias.AbstractDeviceResponseType;
import com.broadon.wsapi.ias.AbstractRequestType;
import com.broadon.wsapi.ias.AbstractResponseType;
import com.broadon.wsapi.ias.IdentityAuthenticationPortType;
import com.broadon.wsapi.ias.LocaleType;

public class TestCaseWrapper extends TestCase {

    protected IdentityAuthenticationPortType ias = null;
    
    protected static String UNAUTHORIZED_ACCESS_MESSAGE = "Unauthorized Access Denied";
        
    protected boolean tracing = false;
	
    public TestCaseWrapper(String name) 
    {
	super(name);
    }

    public void setUp() 
        throws Exception 
    {
	super.setUp();
        if (ias == null) {
            ias = ResourcePool.newIdentityAuthenticationService();
        }
        
        TestDataAdmin.setUp(1000);        
    }

    public void tearDown() 
        throws Exception 
    {
	super.tearDown();
    }
	
    protected void runTest() 
        throws Throwable 
    {
	boolean testResult = true;
	
        try {
	    super.runTest();
        } catch (Throwable e) {
	    System.out.println(getName() + " error: " + e.getMessage());
	    if (tracing) {
		e.printStackTrace();
	    }
	    testResult = false;
	    throw e;
	} finally {
	    TestLog.report("IAS", getName(), testResult);
	}
    }

    protected void initRequest(AbstractRequestType req) 
    {
        req.setVersion("1.0");
        req.setLocale(LocaleType.en_US);
    }
    
    protected boolean checkAccountResponse(AbstractAccountRequestType req,
	 		                   AbstractAccountResponseType resp) 
    {
        assertEquals("getVersion", req.getVersion(), resp.getVersion());
        assertEquals("getAccountId", req.getAccountId(), resp.getAccountId());
        return true;
    }
        
    protected boolean checkDeviceResponse(AbstractDeviceRequestType req,
                                          AbstractDeviceResponseType resp)
    {
        assertEquals("getVersion", req.getVersion(), resp.getVersion());
        assertEquals("getDeviceId", req.getDeviceId(), resp.getDeviceId());
                
        return true;
    }
        
    protected boolean checkError(AbstractResponseType expected,
                                 AbstractResponseType resp)
    {
        assertEquals("getErrorCode", expected.getErrorCode(), resp.getErrorCode());
    
        String expectedPattern = expected.getErrorMessage();
        String message = resp.getErrorMessage();
        
        if ((expectedPattern != null) && (message != null)) {
             assertTrue("getErrorMessage should match '" + expectedPattern + "'",
                         message.matches(expectedPattern));
        } else {
             assertEquals("getErrorMessage", expectedPattern, message);
        }
        return true;
    }   
}

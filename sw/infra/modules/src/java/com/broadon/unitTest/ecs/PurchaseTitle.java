package com.broadon.unitTest.ecs;

import com.broadon.wsapi.ecs.LimitType;
import com.broadon.wsapi.ecs.MoneyType;
import com.broadon.wsapi.ecs.PriceType;
import com.broadon.wsapi.ecs.PurchaseTitleRequestType;
import com.broadon.wsapi.ecs.PurchaseTitleResponseType;

public class PurchaseTitle extends TestCaseWrapper {

    public static void main(String[] args) {
        junit.textui.TestRunner.run(PurchaseTitle.class);
    }

    public PurchaseTitle(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
        clearETickets(testDevice, "PR");
        clearETickets(testDevice, "TR");
        createAccount(validVCAccount.getAccountPayment().getAccountNumber());
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    private boolean checkPurchaseTitle(
            PurchaseTitleRequestType req,
            PurchaseTitleResponseType expected)
        throws Throwable
    {
        boolean testResult = true;

        PurchaseTitleResponseType resp;

        resp = ecs.purchaseTitle(req);
        testResult &= checkAbstractResponse(req, resp);
            
        // Check response against expected response
        testResult &= checkError(expected, resp);
        
        if (expected.getBalance() != null) {
            assertEquals(expected.getBalance(), resp.getBalance());
        }

        return testResult;
    }

    /**
     * Initializes request to purchase a permanent title
     * @param req
     */
    protected void initPurchasePermanentRequest(PurchaseTitleRequestType req) {
        initAbstractRequest(req);
        req.setAccount(validVCAccount.getAccountPayment());
        req.setDeviceId(testDevice);
        req.setPayment(validVCAccount);
        req.setItemId(testItemIds[5]);
        req.setTitleId(testTitles[5]);
        req.setLimits(new LimitType[] { new LimitType(0, "PR")});
        req.setPrice(new PriceType(testPrices[5], "POINTS"));
        req.setDeviceCert(validDeviceCert);
    }
    
    /**
     * Initializes request to purchase a trial title
     * @param req
     */
    protected void initPurchaseTrialRequest(PurchaseTitleRequestType req) {
        initAbstractRequest(req);
        req.setDeviceId(testDevice);
        req.setVirtualDeviceType(new Integer(DEVICE_TYPE_RV));
        req.setCountryCode(testCountry);
        req.setItemId(testItemIds[6] );
        req.setTitleId(testTitles[6]);
        req.setLimits(new LimitType[] { new LimitType(testLimits[6], "TR")});
        req.setPrice(new PriceType("0", "POINTS"));
        req.setDeviceCert(validDeviceCert);
    }
    
    public void testPurchaseTitleBadItemId() throws Throwable {
        PurchaseTitleRequestType req = new PurchaseTitleRequestType();
        PurchaseTitleResponseType expected = new PurchaseTitleResponseType();
        
        initPurchasePermanentRequest(req);
        req.setItemId(300);
        
        expected.setErrorCode(601);
        expected.setErrorMessage(".*Invalid item.*");
        
        assertTrue(checkPurchaseTitle(req, expected));
    }

    public void testPurchaseTitleBadDeviceType() throws Throwable {
        PurchaseTitleRequestType req = new PurchaseTitleRequestType();
        PurchaseTitleResponseType expected = new PurchaseTitleResponseType();
        
        initPurchasePermanentRequest(req);
        req.setVirtualDeviceType(new Integer(-1));
        
        expected.setErrorCode(604);
        expected.setErrorMessage(".*Invalid device.*");
        
        assertTrue(checkPurchaseTitle(req, expected));
    }

    public void testPurchaseTitleBadCountryId() throws Throwable {
        PurchaseTitleRequestType req = new PurchaseTitleRequestType();
        PurchaseTitleResponseType expected = new PurchaseTitleResponseType();
        
        initPurchasePermanentRequest(req);
        req.setCountryCode(COUNTRY_JP);
        
        expected.setErrorCode(606);
        expected.setErrorMessage(".*Invalid country.*");
        
        assertTrue(checkPurchaseTitle(req, expected));
    }

    public void testPurchaseTitleBadPrice() throws Throwable {
        PurchaseTitleRequestType req = new PurchaseTitleRequestType();
        PurchaseTitleResponseType expected = new PurchaseTitleResponseType();
        
        initPurchasePermanentRequest(req);
        req.setPrice(new PriceType("2", "EUNITS"));
        
        expected.setErrorCode(615);
        expected.setErrorMessage(".*Actual price is different from expected price.*");
        
        assertTrue(checkPurchaseTitle(req, expected));
    }

    public void testPurchaseTitleBadTitleId() throws Throwable {
        PurchaseTitleRequestType req = new PurchaseTitleRequestType();
        PurchaseTitleResponseType expected = new PurchaseTitleResponseType();
        
        initPurchasePermanentRequest(req);
        req.setTitleId("888342");
        
        expected.setErrorCode(602);
        expected.setErrorMessage(".*Invalid title.*");
        
        assertTrue(checkPurchaseTitle(req, expected));
    }

    public void testPurchaseTitleNoPayment()
        throws Throwable
    {
        PurchaseTitleRequestType req = new PurchaseTitleRequestType();
        PurchaseTitleResponseType expected = new PurchaseTitleResponseType();

        initPurchasePermanentRequest(req);
        req.setPayment(null);
        
        expected.setErrorCode(616);
        expected.setErrorMessage(".*No payment.*");
        
        assertTrue(checkPurchaseTitle(req, expected));
    }

    public void testPurchaseTitleInvalidPayment()
        throws Throwable
    {
        for (int i = 0; i < invalidPayments.length; i++) {
            PurchaseTitleRequestType req = new PurchaseTitleRequestType();
            PurchaseTitleResponseType expected = new PurchaseTitleResponseType();
    
            initPurchasePermanentRequest(req);
            req.setPayment(invalidPayments[i]);

            expected.setErrorCode(invalidPaymentErrors[i]);
            expected.setErrorMessage(".*");
            assertTrue(checkPurchaseTitle(req, expected));
        }
    }

    public void testPurchasePermanentTitle()
        throws Throwable
    {
        PurchaseTitleRequestType req = new PurchaseTitleRequestType();
        PurchaseTitleResponseType expected = new PurchaseTitleResponseType();
        
        initPurchasePermanentRequest(req);
        expected.setBalance(new MoneyType(String.valueOf(Long.parseLong(START_BALANCE) - Long.parseLong(testPrices[5])), "POINTS"));
        assertTrue(checkPurchaseTitle(req, expected));        
    }

    public void testPurchasePermanentTitleAgain()
        throws Throwable
    {
        PurchaseTitleRequestType req = new PurchaseTitleRequestType();
        PurchaseTitleResponseType expected = new PurchaseTitleResponseType();
        
        initPurchasePermanentRequest(req);
        expected.setBalance(new MoneyType(String.valueOf(Long.parseLong(START_BALANCE) - Long.parseLong(testPrices[5])), "POINTS"));
        assertTrue(checkPurchaseTitle(req, expected));        
        
        // Try to purchase title again (should get error)        
        expected.setErrorCode(621);
        expected.setErrorMessage(".*Title is already purchased.*");
        expected.setBalance(null);
        assertTrue(checkPurchaseTitle(req, expected));
    }

    /* Disable test - no trial titles yet */
    public void testPurchaseTrialTitle()
        throws Throwable
    {
        PurchaseTitleRequestType req = new PurchaseTitleRequestType();
        PurchaseTitleResponseType expected = new PurchaseTitleResponseType();
    
        initPurchaseTrialRequest(req);
        assertTrue(checkPurchaseTitle(req, expected));
    
        // Try to purchase title again (should get error)
        expected.setErrorCode(623);
        expected.setErrorMessage(".*Trial not allowed.*");
        assertTrue(checkPurchaseTitle(req, expected));

        // Try to purchase title as permanent title (should succeed)
        req.setItemId(testItemIds[7]);
        req.setLimits(new LimitType[] { new LimitType(0, "PR")});
        req.setPrice(new PriceType(testPrices[7], "POINTS"));
        req.setPayment(validVCAccount);

        expected.setErrorCode(0);
        expected.setErrorMessage(null);
        assertTrue(checkPurchaseTitle(req, expected));
    }
}

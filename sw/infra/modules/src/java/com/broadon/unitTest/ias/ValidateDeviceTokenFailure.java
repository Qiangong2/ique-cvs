package com.broadon.unitTest.ias;

import com.broadon.wsapi.ias.ValidateDeviceTokenRequestType;
import com.broadon.wsapi.ias.ValidateDeviceTokenResponseType;

public class ValidateDeviceTokenFailure extends TestCaseWrapper {

    public static void main(String[] args) {
    }

    public ValidateDeviceTokenFailure(String name) {
	super(name);
    }

    public void setUp() throws Exception {
	super.setUp();
    }

    public void tearDown() throws Exception {
	super.tearDown();
    }

    private boolean checkValidateDeviceTokenFailure(
		String deviceId,
                String deviceToken,
                ValidateDeviceTokenResponseType expected)
	throws Throwable
    {
	boolean testResult = true;

        ValidateDeviceTokenResponseType resp;
        ValidateDeviceTokenRequestType req = new ValidateDeviceTokenRequestType();

        initRequest(req);
	req.setDeviceId(Long.parseLong(deviceId));
        req.setTokenId(deviceToken);
		
	resp = ias.validateDeviceToken(req);
	testResult &= checkDeviceResponse(req, resp);
		
	// Check response against expected response
	testResult &= checkError(expected, resp);

	return testResult;
    }

    public void testValidateDeviceTokenFailure()
	throws Throwable
    {
        Token et = TestDataAdmin.getExpiredDeviceToken();
        
        if (et != null) {
            // Case 1: Validate a token that has expired
            ValidateDeviceTokenResponseType expected = new ValidateDeviceTokenResponseType();
            
            expected.setDeviceId(Long.parseLong(et.id));
            expected.setErrorCode(903);
            expected.setErrorMessage("IAS - Invalid device token");
            assertTrue(checkValidateDeviceTokenFailure(et.id, et.tokenId, expected));
        } else {
            System.out.println(getName() + " Warning: Failed to obtain an expired device token");
        }
    }
}

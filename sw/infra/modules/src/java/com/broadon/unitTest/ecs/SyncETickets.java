package com.broadon.unitTest.ecs;

import java.math.BigInteger;

import com.broadon.unitTest.Configuration;
import com.broadon.wsapi.ecs.SyncETicketsRequestType;
import com.broadon.wsapi.ecs.SyncETicketsResponseType;

public class SyncETickets extends TestCaseWrapper {
    public static void main(String[] args) {
        junit.textui.TestRunner.run(SyncETickets.class);
    }

    public SyncETickets(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
        clearETickets(testDevice, null);
    }

    public void tearDown() throws Exception {
        clearETickets(testDevice, null);
        super.tearDown();
    }

    private boolean checkSyncETickets(
            SyncETicketsRequestType req,
            SyncETicketsResponseType expected)
        throws Throwable
    {
        boolean testResult = true;

        SyncETicketsResponseType resp;

        resp = ecs.syncETickets(req);
        testResult &= checkAbstractResponse(req, resp);
            
        // Check response against expected response
        testResult &= checkError(expected, resp);
        
        // Check how many tickets and certs we got
        if (expected.getETickets() == null) {
            assertNull(resp.getETickets());
        } else {
            assertNotNull(resp.getETickets());
            assertEquals(expected.getETickets().length, resp.getETickets().length);
        }
        
        if (expected.getCerts() == null) {
            assertNull(resp.getCerts());
        } else {
            assertNotNull(resp.getCerts());
            assertEquals(expected.getCerts().length, resp.getCerts().length);
        }
        
        if (expected.getSyncTime() == null) {
            expected.setSyncTime(resp.getSyncTime());
        } else {
            assertEquals(expected.getSyncTime(), resp.getSyncTime());
        }

        return testResult;
    }

    public void testSyncETickets()
        throws Throwable
    {
        int tickets;
        createETicket(testDevice, new BigInteger(testTitles[3]+"ffffffff", 16), "PERMANENT", "PR", 0);
        createETicket(testDevice, new BigInteger(testTitles[4]+"ffffffff", 16), "RENTAL", "TR", 60*60);
        if (Configuration.getProduct() == Configuration.NC) {
            createETicket(testDevice, new BigInteger("0002000100000000ffffffff", 16), "SUBSCRIPT", "SR", System.currentTimeMillis()/1000);
            tickets = 3;
        } else {
            tickets = 2;
        }
        
        SyncETicketsRequestType req = new SyncETicketsRequestType();
        initAbstractRequest(req);
        req.setDeviceCert(validDeviceCert);
        req.setDeviceId(testDevice);

        SyncETicketsResponseType expected = new SyncETicketsResponseType();
        expected.setCerts(new byte[2][]);
        expected.setETickets(new byte[tickets][]);
        assertTrue(checkSyncETickets(req, expected));
        
        req.setLastSyncTime(expected.getSyncTime());
        expected.setETickets(null);
        expected.setCerts(null);
        expected.setErrorCode(641);
        expected.setErrorMessage("Etickets are already in sync");
        assertTrue(checkSyncETickets(req, expected));
    }
}

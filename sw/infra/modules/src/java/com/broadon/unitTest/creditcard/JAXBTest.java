package com.broadon.unitTest.creditcard;

import net.wiivc.services.InvoiceModifier;
import net.wiivc.services.JAXBAgent;
import net.wiivc.services.JAXBAgentException;

public class JAXBTest {
    public static void main(String args[]) throws JAXBAgentException {
        String xml = null;
        JAXBAgent agent = new JAXBAgent();
        net.wiivc.services.AuthTxn txn = new net.wiivc.services.AuthTxn();
        fillTxn(txn);
        xml = agent.getXMLString(txn);
        System.out.println("xml: " + xml);
        net.wiivc.services.AuthTxn newTxn = agent.getAuthTxn(xml);
        printTxn(newTxn);
        newTxn = null;
        String newXml = "<authTxnRoot xmlns=\"http://services.wiivc.net\"><accountId>588</accountId><approveDate xsi:nil=\"true\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"/><billingCity>San Jose</billingCity><billingCounty xsi:nil=\"true\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"/><billingPostal>95130</billingPostal><billingState>CA</billingState><cardExpMM>12</cardExpMM><cardExpYY>08</cardExpYY><cardNum>1234567890</cardNum><cardType>V</cardType><cardVfyVal>123</cardVfyVal><clientIP>1.2.3.4</clientIP><country>USA</country><currency>USD</currency><deviceId>001</deviceId><functionCode>TEST</functionCode><invMod><item><amount>100</amount><desc>im1</desc><lineItemType>type1</lineItemType></item><item><amount>20</amount><desc>im2</desc><lineItemType>type2</lineItemType></item></invMod><language>Eng</language><paymentMethodId>CC</paymentMethodId><paymentType>CC</paymentType><refillPoints>1000</refillPoints><region>North America</region><result>S</result><sessionId>1234</sessionId><totalAmount>1200</totalAmount><totalPaid>1200</totalPaid><totalTax>123</totalTax><transDate>06.08.11</transDate><transId>abc</transId><transType>A</transType><vcMessage>Test</vcMessage><vcMsgCode xsi:nil=\"true\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"/></authTxnRoot>";
        newTxn = agent.getAuthTxn(newXml);
        printTxn(newTxn);
        newTxn = null;
        String testXmlStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><authTxnRoot xmlns=\"http://services.wiivc.net\"><accountId>588</accountId><approveDate xsi:nil=\"true\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"/><billingCity>San Jose</billingCity><billingCounty>Santa Clara</billingCounty><billingPostal>95130</billingPostal><billingState>CA</billingState><cardExpMM>12</cardExpMM><cardExpYY>08</cardExpYY><cardNum>1234567890</cardNum><cardType>V</cardType><cardVfyVal>123</cardVfyVal><clientIP>1.2.3.4</clientIP><country>USA</country><currency>USD</currency><deviceId>001</deviceId><functionCode>CALC_PAYMENT</functionCode><invMod><item><amount>100.0</amount><desc>TAX@10%</desc><lineItemType>T</lineItemType></item></invMod><language>Eng</language><paymentMethodId>CC</paymentMethodId><paymentType>CC</paymentType><refillPoints>1000</refillPoints><region>NOA</region><result>SUCCESS</result><sessionId xsi:nil=\"true\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"/><totalAmount>1000</totalAmount><totalPaid>1100.0</totalPaid><totalTax>100.0</totalTax><transDate>06.08.11</transDate><transId xsi:nil=\"true\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"/><transType>A</transType><vcMessage>Press SUBMIT to accept transaction and process your credit card</vcMessage><vcMsgCode><item>TRANSACTION</item></vcMsgCode></authTxnRoot>";
        newTxn = agent.getAuthTxn(testXmlStr);
        printTxn(newTxn);
        }
    
    private static void fillTxn(net.wiivc.services.AuthTxn txn) {
        txn.setAccountId("588");
        //txn.setApproveDate("2006.08.10");
        txn.setBillingCity("San Jose");
        //txn.setBillingCounty("Santa Clara");
        txn.setBillingPostal("95130");
        txn.setBillingState("CA");
        txn.setCardExpMM("12");
        txn.setCardExpYY("08");
        txn.setCardNum("1234567890");
        txn.setCardType("V");
        txn.setCardVfyVal("123");
        txn.setClientIP("1.2.3.4");
        txn.setCountry("USA");
        txn.setCurrency("USD");
        txn.setDeviceId("001");
        txn.setFunctionCode("TEST");
        txn.setInvMod(setIMs());
        txn.setLanguage("Eng");
        txn.setPaymentMethodId("CC");
        txn.setPaymentType("CC");
        txn.setRefillPoints("1000");
        txn.setRegion("North America");
        txn.setResult("S");
        txn.setSessionId("1234");
        txn.setTotalAmount("1200");
        txn.setTotalPaid("1200");
        txn.setTotalTax("123");
        txn.setTransDate("06.08.11");
        txn.setTransId("abc");
        txn.setTransType("A");
        txn.setVcMessage("Test");
        //txn.setVcMsgCode(new String[] {"test", "test"});
    }
    private static InvoiceModifier[] setIMs() {
        InvoiceModifier[] ims = new InvoiceModifier[1];
        ims[0] = createIM("100", "im1", "T");
        //ims[1] = createIM("20", "im2", "type2");
        return ims;
    }
    private static InvoiceModifier createIM(String amt, String desc, String type) {
        InvoiceModifier im = new InvoiceModifier();
        im.setAmount(amt);
        im.setDesc(desc);
        im.setLineItemType(type);
        return im;
    }
    private static void printTxn(net.wiivc.services.AuthTxn txn) {
        System.out.println("AccountId: " + txn.getAccountId());
        System.out.println("ApproveDate: " + txn.getApproveDate());
        System.out.println("BillingCity: " + txn.getBillingCity());
        System.out.println("BillingCounty: " + txn.getBillingCounty());
        System.out.println("BillingPostal: " + txn.getBillingPostal());
        System.out.println("BillingState: " + txn.getBillingState());
        System.out.println("CardExpMM: " + txn.getCardExpMM());
        System.out.println("CardExpYY: " + txn.getCardExpYY());
        System.out.println("Region: " + txn.getRegion());
        printInvoiceModifiers(txn.getInvMod());
        printVcMsgCodes(txn.getVcMsgCode());
    }
    private static void printInvoiceModifiers(InvoiceModifier[] ims) {
        System.out.println("IM:");
        if (ims == null)
            return;
        for (int i=0; i<ims.length; i++) {
            System.out.println("    " + ims[i].getAmount());
            System.out.println("    " + ims[i].getDesc());
            System.out.println("    " + ims[i].getLineItemType());
        }
    }
    
    private static void printVcMsgCodes(String[] msgs) {
        System.out.println("VCMsgCodes:");
        if (msgs == null)
            return;
        for (int i=0; i<msgs.length; i++) {
            System.out.println("    " + msgs[i]);
        }
    }
}

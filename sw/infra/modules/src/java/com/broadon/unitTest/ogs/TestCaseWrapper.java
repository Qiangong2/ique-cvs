/* A wrapper around the Junit TestCase.
 *   - Shared initialization code should be inserted here.
 */
/*
 * JUnit TestCase wrapper for IAS
 *   - this class contains functions that should be shared among IAS 
 *     test cases.
 */
package com.broadon.unitTest.ogs;

import junit.framework.TestCase;

import com.broadon.unitTest.ResourcePool;
import com.broadon.unitTest.TestLog;
import com.broadon.wsapi.ogs.AbstractRequestType;
import com.broadon.wsapi.ogs.AbstractResponseType;
import com.broadon.wsapi.ogs.OnlineGamePortType;

public class TestCaseWrapper extends TestCase {

    protected OnlineGamePortType ogs = null;
    protected static int VALIDATE_BUDDY_STATE_TEST_COUNT = 100;
          
    protected boolean tracing = false;
	
    public TestCaseWrapper(String name) 
    {
	super(name);
    }

    public void setUp() 
        throws Exception 
    {
	super.setUp();
        if (ogs == null) {
            ogs = ResourcePool.newOnlineGameService();
        }
        
        BuddyStateAction.setUp();
    }

    public void tearDown() 
        throws Exception 
    {
	super.tearDown();
    }
	
    protected void runTest() 
        throws Throwable 
    {
	boolean testResult = true;
	
        try {
	    super.runTest();
        } catch (Throwable e) {
	    System.out.println(getName() + " error: " + e.getMessage());
	    if (tracing) {
		e.printStackTrace();
	    }
	    testResult = false;
	    throw e;
	} finally {
	    TestLog.report("OGS", getName(), testResult);
	}
    }

    protected void initRequest(AbstractRequestType req) 
    {
        req.setVersion("1.0");
        req.setMessageId("ogs_vn");
        req.setTimeStamp(System.currentTimeMillis());
    }
    
    protected boolean checkResponse(AbstractRequestType req,
	 		            AbstractResponseType resp) 
    {
        assertEquals("getVersion", req.getVersion(), resp.getVersion());
        return true;
    }
        
    protected boolean checkError(AbstractResponseType expected,
                                 AbstractResponseType resp)
    {
        assertEquals("getErrorCode", expected.getErrorCode(), resp.getErrorCode());
    
        String expectedPattern = expected.getErrorMessage();
        String message = resp.getErrorMessage();
        
        if ((expectedPattern != null) && (message != null)) {
             assertTrue("getErrorMessage should match '" + expectedPattern + "'",
                         message.matches(expectedPattern));
        } else {
             assertEquals("getErrorMessage", expectedPattern, message);
        }
        return true;
    }
}

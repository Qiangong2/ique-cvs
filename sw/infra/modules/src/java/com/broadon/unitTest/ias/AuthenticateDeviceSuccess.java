package com.broadon.unitTest.ias;

import com.broadon.wsapi.ias.AuthenticateDeviceRequestType;
import com.broadon.wsapi.ias.AuthenticateDeviceResponseType;

public class AuthenticateDeviceSuccess extends TestCaseWrapper {

    public static void main(String[] args) {
    }

    public AuthenticateDeviceSuccess(String name) {
	super(name);
    }

    public void setUp() throws Exception {
	super.setUp();
    }

    public void tearDown() throws Exception {
	super.tearDown();
    }

    private boolean checkAuthenticateDeviceSuccess(
		String deviceId,
                String tokenId,
                boolean valid,
		AuthenticateDeviceResponseType expected)
	throws Throwable
    {
	boolean testResult = true;

        AuthenticateDeviceResponseType resp;
        AuthenticateDeviceRequestType req = new AuthenticateDeviceRequestType();
        
        initRequest(req);
	req.setDeviceId(Long.parseLong(deviceId));
        		
	resp = ias.authenticateDevice(req);
	testResult &= checkDeviceResponse(req, resp);
			
	// Check response against expected response
	testResult &= checkError(expected, resp);

        if (tokenId != null) {
            if (testResult && valid)
                testResult = (tokenId.equals(resp.getTokenId()));
            else if (testResult && !valid) {
                testResult = (!tokenId.equals(resp.getTokenId()));
                TestDataAdmin.setValidDeviceToken(deviceId, resp.getTokenId());
            }    
        } else {
            if (testResult)
                TestDataAdmin.setValidDeviceToken(deviceId, resp.getTokenId());            
        }
        
	return testResult;
    }

    public void testAuthenticateDeviceSuccess()
	throws Throwable
    {
        
        // Case 1: Authenticate a device with an existing valid token
        Token td = TestDataAdmin.getValidDeviceToken();
        
        if (td != null) {
            AuthenticateDeviceResponseType expected = new AuthenticateDeviceResponseType();
            expected.setDeviceId(Long.parseLong(td.id));
            expected.setErrorCode(000);
            expected.setErrorMessage("");
            assertTrue(checkAuthenticateDeviceSuccess(td.id, td.tokenId, true, expected));
        } else {
            System.out.println(getName() + " Warning: Failed to obtain a valid device token");
        }
        
        // Case 2: Authenticate a device with an existing but expired token
        td = TestDataAdmin.getExpiredDeviceToken();
        
        if (td != null) {
            AuthenticateDeviceResponseType expected = new AuthenticateDeviceResponseType();
            expected.setDeviceId(Long.parseLong(td.id));
            expected.setErrorCode(000);
            expected.setErrorMessage("");
            assertTrue(checkAuthenticateDeviceSuccess(td.id, td.tokenId, false, expected));
        } else {
            System.out.println(getName() + " Warning: Failed to obtain an expired device token");
        }
        
        // Case 3: Authenticate a device with no existing tokens
        String deviceId = TestDataAdmin.getDeviceIdWithNoToken();
        
        if (deviceId != null) {
            AuthenticateDeviceResponseType expected = new AuthenticateDeviceResponseType();
            expected.setDeviceId(Long.parseLong(deviceId));
            expected.setErrorCode(000);
            expected.setErrorMessage("");
            assertTrue(checkAuthenticateDeviceSuccess(deviceId, null, false, expected));
        } else {
            System.out.println(getName() + " Warning: Failed to obtain a device id with no existing tokens");
        }
    }
}

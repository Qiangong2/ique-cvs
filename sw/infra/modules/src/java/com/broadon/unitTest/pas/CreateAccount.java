package com.broadon.unitTest.pas;

import com.broadon.wsapi.pas.BalanceAccountType;
import com.broadon.wsapi.pas.ResetAccountPINRequestType;
import com.broadon.wsapi.pas.ResetAccountPINResponseType;
import com.broadon.wsapi.pas.CreateAccountRequestType;
import com.broadon.wsapi.pas.CreateAccountResponseType;
import com.broadon.wsapi.pas.DisableAccountRequestType;
import com.broadon.wsapi.pas.DisableAccountResponseType;

public class CreateAccount extends TestCaseWrapper {

    public static void main(String[] args) {
        junit.textui.TestRunner.run(CreateAccount.class);
    }

    public CreateAccount(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
        resetTestDataBase(AccountTests);
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    public boolean checkCreateAccount() {
        boolean testResult = true;
        BalanceAccountType acct = new BalanceAccountType();
        acct.setAccountNumber("30001");
        acct.setPIN("30001");
        CreateAccountRequestType req = createCreateAccountRequest(acct);
        DisableAccountRequestType req2 = createDisableAccountRequest(acct);
        ResetAccountPINRequestType req3 = createResetAccountPINRequest(acct);
        CreateAccountResponseType resp;
        DisableAccountResponseType resp2;
        ResetAccountPINResponseType resp3;
        try {
            // create account
            resp = pas.createAccount(req);
            testResult = checkAbstractResponse(req, resp);
            if (!testResult) {
                System.out.println("createAccount Failed");
                return testResult;
            }
            // disable account
            resp2 = pas.disableAccount(req2);
            testResult = checkAbstractResponse(req2, resp2);
            if (!testResult) {
                System.out.println("disableAccount Failed");
                return testResult;
            }
            // change PIN
            acct.setPIN("555");
            resp3 = pas.resetAccountPIN(req3);
            testResult = checkAbstractResponse(req3, resp3);
            if (!testResult) {
                System.out.println("changeAccountPIN Failed");
                return testResult;
            }
            // enable account
            resp = pas.createAccount(req);
            testResult = checkAbstractResponse(req, resp);
            if (!testResult) {
                System.out.println("createAccount Failed");
                return testResult;
            }
        } catch (Exception e) {
            System.out.println(getStackTrace(e));
            System.out.println(e.getMessage());
            testResult = false;
        }
        return testResult;
    }
    
    public void testCreateAccount() {
        assertTrue(checkCreateAccount());
    }   
}
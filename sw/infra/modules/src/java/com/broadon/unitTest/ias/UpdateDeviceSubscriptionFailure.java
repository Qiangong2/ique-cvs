package com.broadon.unitTest.ias;

import com.broadon.wsapi.ias.UpdateDeviceSubscriptionRequestType;
import com.broadon.wsapi.ias.UpdateDeviceSubscriptionResponseType;
import com.broadon.wsapi.ias.DeviceSubscriptionType;

public class UpdateDeviceSubscriptionFailure extends TestCaseWrapper {

    public static void main(String[] args) {
    }

    public UpdateDeviceSubscriptionFailure(String name) {
	super(name);
    }

    public void setUp() throws Exception {
	super.setUp();
    }

    public void tearDown() throws Exception {
	super.tearDown();
    }
    
    private boolean checkUpdateDeviceSubscriptionFailure (
		String deviceId, 
                DeviceSubscriptionType dType,
                UpdateDeviceSubscriptionResponseType expected)
	throws Throwable
    {
	boolean testResult = true;

        UpdateDeviceSubscriptionResponseType resp;
        UpdateDeviceSubscriptionRequestType req = new UpdateDeviceSubscriptionRequestType();
        
        initRequest(req);
	req.setDeviceId(Long.parseLong(deviceId));
        req.setDeviceSubscription(dType);
        
        resp = ias.updateDeviceSubscription(req);
	testResult &= checkDeviceResponse(req, resp);
		
	// Check response against expected response
	testResult &= checkError(expected, resp);
        
	return testResult;
    }

    public void testUpdateDeviceSubscriptionFailure()
	throws Throwable
    {
        // Case 1: Update device subscription for an un-registered device id
        String deviceId = TestDataAdmin.getUnRegisteredDeviceId();        
        
        if (deviceId != null) {            
            DeviceSubscriptionType dType = new DeviceSubscriptionType();
            dType.setSubscriptionType("test006");
            dType.setStartDate(System.currentTimeMillis());
            dType.setExpirationDate(System.currentTimeMillis()+(30*24*60*60*1000));
            dType.setLastUpdated(null);
            dType.setReferenceId("Failure Test 1");
            
            UpdateDeviceSubscriptionResponseType expected = new UpdateDeviceSubscriptionResponseType();
            expected.setDeviceId(Long.parseLong(deviceId));
            expected.setErrorCode(901);
            expected.setErrorMessage("IAS - Device Id is not registered");            
            assertTrue(checkUpdateDeviceSubscriptionFailure(deviceId, dType, expected));
            
        } else {
            System.out.println(getName() + " Warning: Failed to obtain an un-registered device id");
        }
        
        // Case 2: Update device subscription for a registered device id but with different lastUpdated value
        deviceId = TestDataAdmin.getDeviceIdWithValidTokenAndSubscription(true);
        
        if (deviceId != null) {
            String subs[] = TestDataAdmin.getDeviceSubscriptions(deviceId);
            
            DeviceSubscriptionType dType = new DeviceSubscriptionType();
            dType.setSubscriptionType(subs[0]);
            dType.setStartDate(System.currentTimeMillis());
            dType.setExpirationDate(System.currentTimeMillis()+(30*24*60*60*1000));
            dType.setLastUpdated(new Long(-1));
            dType.setReferenceId("Failure Test 2");
            
            UpdateDeviceSubscriptionResponseType expected = new UpdateDeviceSubscriptionResponseType();
            expected.setDeviceId(Long.parseLong(deviceId));
            expected.setErrorCode(921);
            expected.setErrorMessage("IAS - Failed to update device subscription");
            assertTrue(checkUpdateDeviceSubscriptionFailure(deviceId, dType, expected));
            
        } else {
            System.out.println(getName() + " Warning: Failed to obtain a device id with valid token and a subscription");
        }
        
    }
}

/*
 * IAS TestSuite:  Add new IAS TestCase here to this class.
 */
package com.broadon.unitTest.ias;

import junit.framework.Test;
import junit.framework.TestSuite;

public class IasTestSuite {

	public static void main(String[] args) {
		junit.textui.TestRunner.run(IasTestSuite.suite());
	}

	public static Test suite() {
		TestSuite suite = new TestSuite("Test for com.broadon.unitTest.ias");
		
                //$JUnit-BEGIN$
                suite.addTestSuite(AuthenticateDeviceSuccess.class);
                suite.addTestSuite(AuthenticateDeviceFailure.class);
                suite.addTestSuite(RenewDeviceTokenSuccess.class);
                suite.addTestSuite(RenewDeviceTokenFailure.class);                
                suite.addTestSuite(ValidateDeviceTokenSuccess.class);
                suite.addTestSuite(ValidateDeviceTokenFailure.class);             
                
                suite.addTestSuite(UpdateDeviceSubscriptionSuccess.class);
                suite.addTestSuite(UpdateDeviceSubscriptionFailure.class);
                suite.addTestSuite(GetDeviceSubscriptionsSuccess.class);
                suite.addTestSuite(GetDeviceSubscriptionsFailure.class);
                
                suite.addTestSuite(RegisterDeviceSuccess.class);
                suite.addTestSuite(RegisterDeviceFailure.class);
                suite.addTestSuite(GetDeviceInfoSuccess.class);
                suite.addTestSuite(GetDeviceInfoFailure.class);
                
                suite.addTestSuite(AuthenticateAccountSuccess.class);
                suite.addTestSuite(AuthenticateAccountFailure.class);
                suite.addTestSuite(RenewAccountTokenSuccess.class);
                suite.addTestSuite(RenewAccountTokenFailure.class); 
                suite.addTestSuite(ValidateAccountTokenSuccess.class);
                suite.addTestSuite(ValidateAccountTokenFailure.class);
                
                suite.addTestSuite(GetDeviceAccountsSuccess.class);
                suite.addTestSuite(GetDeviceAccountsFailure.class);
                suite.addTestSuite(LinkDeviceAccountSuccess.class);
                suite.addTestSuite(LinkDeviceAccountFailure.class);
                suite.addTestSuite(RemoveDeviceAccountSuccess.class);
                suite.addTestSuite(RemoveDeviceAccountFailure.class);
                //$JUnit-END$
                
		return suite;
	}

}

package com.broadon.unitTest.ogs;

import com.broadon.wsapi.ogs.ChangeBuddyStateActionType;
import com.broadon.wsapi.ogs.ChangeBuddyStateRequestType;
import com.broadon.wsapi.ogs.ChangeBuddyStateResponseType;
import com.broadon.wsapi.ogs.InviteBuddyRequestType;
import com.broadon.wsapi.ogs.InviteBuddyResponseType;

public class ValidateBuddyState extends TestCaseWrapper {

    public static void main(String[] args) {
    }

    public ValidateBuddyState(String name) {
	super(name);
    }

    public void setUp() throws Exception {
	super.setUp();
    }

    public void tearDown() throws Exception {
	super.tearDown();
    }

    private boolean checkInviteBuddy(
		String myAccountId,
                String yourAccountId,
		String yourLoginName,
                String action)
	throws Throwable
    {
	boolean testResult = true;

        InviteBuddyResponseType resp;
        InviteBuddyRequestType req = new InviteBuddyRequestType();

        initRequest(req);
	req.setAccountId(Integer.parseInt(myAccountId));
	req.setBuddyLoginName(yourLoginName);
		
	resp = ogs.inviteBuddy(req);
	testResult &= checkResponse(req, resp);
		
	BuddyState expected = BuddyStateAction.getBuddyState(action);
        
/*
        System.out.println("EXPECTED: (" + BuddyStateAction.getMyFlag() + "," + BuddyStateAction.getYourFlag() +
                ") " + action + 
                (expected == null ? " Not Valid" : " (" + expected.myFlag + "," + expected.yourFlag + ")"));
        System.out.println("RESPONSE: (" + BuddyStateAction.getMyFlag() + "," + BuddyStateAction.getYourFlag() +
                ") " + action + 
                (resp.getRelation() == null ? " Not Valid" : " (" + resp.getRelation().getMyState() + "," + resp.getRelation().getBuddyState() + ")"));
*/               
        
        if (expected != null) {            
            testResult &= resp.getBuddyAccountId() != null && resp.getRelation() != null &&
                resp.getBuddyAccountId().intValue() == Integer.parseInt(yourAccountId) &&
                expected.myFlag == resp.getRelation().getMyState() &&
                expected.yourFlag == resp.getRelation().getBuddyState();
            
            if (resp.getRelation() != null) {
                BuddyStateAction.setMyFlag(resp.getRelation().getMyState());
                BuddyStateAction.setYourFlag(resp.getRelation().getBuddyState());
            }
            
        } else
            testResult &=  resp.getBuddyAccountId() == null && 
                resp.getRelation() == null;
        
        return testResult;
    }

    private boolean checkChangeBuddyState(
                        String myAccountId,
                        String yourAccountId,
                        String action)
            throws Throwable
    {
        boolean testResult = true;
    
        ChangeBuddyStateResponseType resp;
        ChangeBuddyStateRequestType req = new ChangeBuddyStateRequestType();
    
        initRequest(req);
        req.setAccountId(Integer.parseInt(myAccountId));
        req.setBuddyAccountId(Integer.parseInt(yourAccountId));
        if ("ACCEPT_INV".equals(action))
            req.setAction(ChangeBuddyStateActionType.Accept);
        else if ("REJECT_INV".equals(action))
            req.setAction(ChangeBuddyStateActionType.Reject);
        else if ("REMOVE_BUD".equals(action))
            req.setAction(ChangeBuddyStateActionType.Remove);
        else if ("BLOCK_BUD".equals(action))
            req.setAction(ChangeBuddyStateActionType.Block);
        else if ("UNBLOCK_BUD".equals(action))
            req.setAction(ChangeBuddyStateActionType.Unblock);
        
        resp = ogs.changeBuddyState(req);
        testResult &= checkResponse(req, resp);
                    
        BuddyState expected = BuddyStateAction.getBuddyState(action);
/*        
        System.out.println("EXPECTED: (" + BuddyStateAction.getMyFlag() + "," + BuddyStateAction.getYourFlag() +
                ") " + action + 
                (expected == null ? " Not Valid" : " (" + expected.myFlag + "," + expected.yourFlag + ")"));
                
        System.out.println("RESPONSE: (" + BuddyStateAction.getMyFlag() + "," + BuddyStateAction.getYourFlag() +
                ") " + action + 
                (resp.getRelation() == null ? " Not Valid" : " (" + resp.getRelation().getMyState() + "," + resp.getRelation().getBuddyState() + ")"));
*/        
        if (expected != null) {            
            testResult &= resp.getRelation() != null &&
                expected.myFlag == resp.getRelation().getMyState() &&
                expected.yourFlag == resp.getRelation().getBuddyState();
            
            if (resp.getRelation() != null) {
                BuddyStateAction.setMyFlag(resp.getRelation().getMyState());
                BuddyStateAction.setYourFlag(resp.getRelation().getBuddyState());
            }
            
        } else
            testResult &= resp.getRelation() == null;
        
        return testResult;
    }
    
    public void testValidateBuddyState()
	throws Throwable
    {
        String myAccountId = BuddyStateAction.getMyAccountId();
        String yourAccountId = BuddyStateAction.getYourAccountId();
        
        for (int i = 1; i <= VALIDATE_BUDDY_STATE_TEST_COUNT; i++) {
            String action = BuddyStateAction.getRandomBuddyAction();
                        
            if (i % 2 != 0) {
//                System.out.println("\n" + i +". -- A " + action + " B --");
                if ("INVITE_BUD".equals(action))                
                    assertTrue(checkInviteBuddy(myAccountId, yourAccountId, "login"+yourAccountId, action));
                else
                    assertTrue(checkChangeBuddyState(myAccountId, yourAccountId, action));
            } else {
//                System.out.println("\n" + i +". -- B " + action + " A --");
                if ("INVITE_BUD".equals(action))                
                    assertTrue(checkInviteBuddy(yourAccountId, myAccountId, "login"+myAccountId, action));
                else
                    assertTrue(checkChangeBuddyState(yourAccountId, myAccountId, action));
            }
            
            BuddyStateAction.toggleFlags();
        }
    }
}

package com.broadon.unitTest.ecs;

import com.broadon.wsapi.ecs.AccountPaymentType;
import com.broadon.wsapi.ecs.CheckAccountBalanceRequestType;
import com.broadon.wsapi.ecs.CheckAccountBalanceResponseType;
import com.broadon.wsapi.ecs.MoneyType;

public class CheckAccountBalance extends TestCaseWrapper {

    public static void main(String[] args) {
        junit.textui.TestRunner.run(CheckAccountBalance.class);
    }

    public CheckAccountBalance(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
        createAccount("30001");
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    private boolean checkCheckAccountBalance(
            CheckAccountBalanceRequestType req,
            CheckAccountBalanceResponseType expected)
        throws Throwable
    {
        boolean testResult = true;

        CheckAccountBalanceResponseType resp;

        resp = ecs.checkAccountBalance(req);
        testResult &= checkAbstractResponse(req, resp);
            
        // Check response against expected response
        testResult &= checkError(expected, resp);

        testResult &= checkBalance("getBalance", expected.getBalance(), resp.getBalance());
        
        return testResult;
    }

    public void testCheckAccountBalance()
        throws Throwable
    {
        CheckAccountBalanceRequestType req = new CheckAccountBalanceRequestType();
        CheckAccountBalanceResponseType expected = new CheckAccountBalanceResponseType();
        
        initAbstractRequest(req);
        AccountPaymentType acct = new AccountPaymentType();
        acct.setAccountNumber("30001");
        acct.setPin("30001");
        req.setAccount(acct);
        
        MoneyType balance = new MoneyType();
        balance.setAmount(START_BALANCE);
        balance.setCurrency("POINTS");
        expected.setBalance(balance);
        assertTrue(checkCheckAccountBalance(req, expected));
    }

}

package com.broadon.unitTest.ecs;

import java.math.BigInteger;

import com.broadon.wsapi.ecs.TransferETicketsRequestType;
import com.broadon.wsapi.ecs.TransferETicketsResponseType;

public class TransferETickets extends TestCaseWrapper {
    public final static long toNC = getDeviceId(DEVICE_TYPE_NC, 12);
    public final static long fromNC = getDeviceId(DEVICE_TYPE_NC, 11);

    public static void main(String[] args) {
        junit.textui.TestRunner.run(TransferETickets.class);
    }

    public TransferETickets(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
        clearETicketTransfers();
        clearETickets(toNC, null);
        clearETickets(fromNC, null);
    }

    public void tearDown() throws Exception {
        clearETickets(toNC, null);
        clearETickets(fromNC, null);
        clearETicketTransfers();
        super.tearDown();
    }

    private boolean checkTransferETickets(
            TransferETicketsRequestType req,
            TransferETicketsResponseType expected)
        throws Throwable
    {
        boolean testResult = true;

        TransferETicketsResponseType resp;

        resp = ecs.transferETickets(req);
        testResult &= checkAbstractResponse(req, resp);
            
        // Check response against expected response
        testResult &= checkError(expected, resp);
        
        assertEquals("getTicketsTransfered", expected.getTicketsTransferred(), resp.getTicketsTransferred());
        
        return testResult;
    }

    public void testTransferETickets()
        throws Throwable
    {
        createETicket(fromNC, BigInteger.valueOf(100), "PERMANENT", "PR", 0);
        createETicket(fromNC, BigInteger.valueOf(101), "TRIAL", "TR", 60);
        
        TransferETicketsRequestType req = new TransferETicketsRequestType();
        initAbstractRequest(req);
        req.setDeviceId(fromNC);
        req.setTargetSerialNo("1234");
        req.setSourceSerialNo(null);
        req.setTargetDeviceId(new Long(toNC));

        TransferETicketsResponseType expected = new TransferETicketsResponseType();
        expected.setTicketsTransferred(2);
        assertTrue(checkTransferETickets(req, expected));
    }

    public void testTransferSpecifiedETickets()
        throws Throwable
    {
        int n = 10;
        long tids[] = new long[n];
        for (int i = 0; i < 10; i++) {
            tids[i] = createETicket(fromNC, BigInteger.valueOf(100 + i*2), "PERMANENT", "PR", 0);
            createETicket(fromNC, BigInteger.valueOf(101 + i*2), "TRIAL", "TR", 60);
        }
        
        // Transfer permanent tickets
        TransferETicketsRequestType req = new TransferETicketsRequestType();
        initAbstractRequest(req);
        req.setDeviceId(fromNC);
        req.setTargetSerialNo("1234");
        req.setSourceSerialNo(null);
        req.setTargetDeviceId(new Long(toNC));
        req.setTickets(tids);

        TransferETicketsResponseType expected = new TransferETicketsResponseType();
        expected.setTicketsTransferred(10);
        assertTrue(checkTransferETickets(req, expected));
        
        // Transfer the rest of the tickets
        req.setTickets(null);
        assertTrue(checkTransferETickets(req, expected));
    }

    public void testTransferETicketsTwoStep()
        throws Throwable
    {
        createETicket(fromNC, BigInteger.valueOf(100), "PERMANENT", "PR", 0);
        createETicket(fromNC, BigInteger.valueOf(101), "TRIAL", "TR", 60);
        
        // Transfer without specifying target device id
        TransferETicketsRequestType req = new TransferETicketsRequestType();
        initAbstractRequest(req);
        req.setDeviceId(fromNC);
        req.setTargetSerialNo("1234");
        req.setSourceSerialNo(null);
        req.setTargetDeviceId(null);

        TransferETicketsResponseType expected = new TransferETicketsResponseType();
        expected.setTicketsTransferred(2);
        assertTrue(checkTransferETickets(req, expected)); 

        // Finish transfer
        req.setSourceSerialNo("1234");
        req.setTargetDeviceId(new Long(toNC));
        assertTrue(checkTransferETickets(req, expected));
        
        // Try to transfer again
        expected.setTicketsTransferred(0);
        assertTrue(checkTransferETickets(req, expected));
    }

    public void testTransferETicketsMissingSerialNumber()
        throws Throwable
    {
        // Transfer without specifying target serial number
        TransferETicketsRequestType req = new TransferETicketsRequestType();
        initAbstractRequest(req);
        req.setDeviceId(fromNC);
        req.setTargetSerialNo(null);

        TransferETicketsResponseType expected = new TransferETicketsResponseType();
        expected.setErrorCode(631);
        expected.setErrorMessage("Target serial number must be specifed");
        assertTrue(checkTransferETickets(req, expected)); 
    }
    
    public void testTransferETicketsMissingDeviceId()
        throws Throwable
    {
        // Transfer without specifying target device id
        TransferETicketsRequestType req = new TransferETicketsRequestType();
        initAbstractRequest(req);
        req.setDeviceId(fromNC);
        req.setTargetSerialNo("1234");
        req.setSourceSerialNo("1234");
        req.setTargetDeviceId(null);

        TransferETicketsResponseType expected = new TransferETicketsResponseType();
        expected.setErrorCode(632);
        expected.setErrorMessage("Target device id must be specifed");
        assertTrue(checkTransferETickets(req, expected)); 
    }

    public void testTransferETicketsSameDeviceId()
        throws Throwable
    {
        // Transfer with same device id
        TransferETicketsRequestType req = new TransferETicketsRequestType();
        initAbstractRequest(req);
        req.setDeviceId(fromNC);
        req.setTargetSerialNo("1234");
        req.setSourceSerialNo(null);
        req.setTargetDeviceId(new Long(fromNC));

        TransferETicketsResponseType expected = new TransferETicketsResponseType();
        expected.setErrorCode(633);
        expected.setErrorMessage("Target and source device ids are the same");
        assertTrue(checkTransferETickets(req, expected)); 
    }
}

package com.broadon.unitTest.ias;

import com.broadon.wsapi.ias.LinkDeviceAccountRequestType;
import com.broadon.wsapi.ias.LinkDeviceAccountResponseType;

public class LinkDeviceAccountSuccess extends TestCaseWrapper {

    public static void main(String[] args) {
    }

    public LinkDeviceAccountSuccess(String name) {
	super(name);
    }

    public void setUp() throws Exception {
	super.setUp();
    }

    public void tearDown() throws Exception {
	super.tearDown();
    }
    
    private boolean checkLinkDeviceAccountSuccess(
		String deviceId,
                String tokenId,
                String accountId,
                LinkDeviceAccountResponseType expected)
	throws Throwable
    {
	boolean testResult = true;

        LinkDeviceAccountResponseType resp;
        LinkDeviceAccountRequestType req = new LinkDeviceAccountRequestType();
        
        initRequest(req);
	req.setDeviceId(Long.parseLong(deviceId));
        req.setTokenId(tokenId);
        req.setAccountId(Integer.parseInt(accountId));
		
	resp = ias.linkDeviceAccount(req);
	testResult &= checkDeviceResponse(req, resp);
		
	// Check response against expected response
	testResult &= checkError(expected, resp);
        
        if (testResult) 
            TestDataAdmin.setValidDeviceAccount(deviceId, accountId);
        
	return testResult;
    }

    public void testLinkDeviceAccountSuccess()
	throws Throwable
    {
        // Case 1: Device with valid token and no linked account
        String deviceId = TestDataAdmin.getDeviceIdWithValidTokenAndNoAccount();
                
        if (deviceId != null) {
            String token = TestDataAdmin.getValidDeviceToken(deviceId);
            String accountId = TestDataAdmin.getRegisteredAccountId();
            
            LinkDeviceAccountResponseType expected = new LinkDeviceAccountResponseType();
            expected.setDeviceId(Long.parseLong(deviceId));
            expected.setErrorCode(000);
            expected.setErrorMessage("");
            assertTrue(checkLinkDeviceAccountSuccess(deviceId, token, accountId, expected));

        } else {
            System.out.println(getName() + " Warning: Failed to obtain a device id with valid token and no linked account");
        }
        
        // Case 2: Device with valid token and removed account
        deviceId = TestDataAdmin.getDeviceIdWithValidTokenAndAccount(true);
                
        if (deviceId != null) {
            String token = TestDataAdmin.getValidDeviceToken(deviceId);
            String accounts[] = TestDataAdmin.getRemovedDeviceAccounts(deviceId);
            
            LinkDeviceAccountResponseType expected = new LinkDeviceAccountResponseType();
            expected.setDeviceId(Long.parseLong(deviceId));
            expected.setErrorCode(000);
            expected.setErrorMessage("");
            assertTrue(checkLinkDeviceAccountSuccess(deviceId, token, accounts[0], expected));

        } else {
            System.out.println(getName() + " Warning: Failed to obtain a device id with valid token and a removed account");
        }
        
        // Case 3: Device with valid token and valid account
        deviceId = TestDataAdmin.getDeviceIdWithValidTokenAndAccount(false);
                
        if (deviceId != null) {
            String token = TestDataAdmin.getValidDeviceToken(deviceId);
            String accounts[] = TestDataAdmin.getValidDeviceAccounts(deviceId);
            
            LinkDeviceAccountResponseType expected = new LinkDeviceAccountResponseType();
            expected.setDeviceId(Long.parseLong(deviceId));
            expected.setErrorCode(000);
            expected.setErrorMessage("");
            assertTrue(checkLinkDeviceAccountSuccess(deviceId, token, accounts[0], expected));

        } else {
            System.out.println(getName() + " Warning: Failed to obtain a device id with valid token and a linked account");
        }
    }    
}

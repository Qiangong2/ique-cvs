/*
 * ECS TestSuite:  Add new ECS TestCase here to this class.
 */
package com.broadon.unitTest.cas;

import junit.framework.Test;
import junit.framework.TestSuite;

public class CasTestSuite {

    public static void main(String[] args) {
        junit.textui.TestRunner.run(CasTestSuite.suite());
    }

    public static Test suite() {
        TestSuite suite = new TestSuite("Test for com.broadon.unitTest.cas");
        //$JUnit-BEGIN$
        suite.addTestSuite(TitleDetails.class);
        //$JUnit-END$
        return suite;
    }

}

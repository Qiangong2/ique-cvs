package com.broadon.unitTest.pas;

import java.math.BigDecimal;

import com.broadon.wsapi.pas.AuthorizeAccountPaymentRequestType;
import com.broadon.wsapi.pas.AuthorizeAccountPaymentResponseType;
import com.broadon.wsapi.pas.AuthorizeECardPaymentRequestType;
import com.broadon.wsapi.pas.AuthorizeECardPaymentResponseType;
import com.broadon.wsapi.pas.BalanceAccountType;
import com.broadon.wsapi.pas.BalanceType;
import com.broadon.wsapi.pas.CaptureAccountToAccountRequestType;
import com.broadon.wsapi.pas.CaptureAccountToAccountResponseType;
import com.broadon.wsapi.pas.CaptureECardToAccountRequestType;
import com.broadon.wsapi.pas.CaptureECardToAccountResponseType;
import com.broadon.wsapi.pas.PasTokenType;
import com.broadon.wsapi.pas.MoneyType;

public class AuthCaptureAcctToAcct extends TestCaseWrapper {
    /**
     * misc. tests for pas ecard/acct capture to acct related services:
     * create an ecard
     * create acct1 and acct2
     * check balance
     * authorize payment from ecard
     * cpature payment to acct1
     * check ecard and acct1 balance
     * authorize payment from acct1
     * capture payment to acct2
     * check acct1 and acct2 balance
     */
    
    String[] ecards;
    BalanceAccountType acct1;
    BalanceAccountType acct2;
    public static void main(String[] args) {
        junit.textui.TestRunner.run(AuthCaptureAcctToAcct.class);
    }

    public AuthCaptureAcctToAcct(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
        resetTestDataBase(AccountTests);
        // ecardType 11 is 1000 POINTS and reusable
        createECardType(11);
        ecards = createECards(11, 1, "JP");
        acct1 = createAccount("30001", "30001");
        acct2 = createAccount("30002", "30002");
    }

    public void tearDown() throws Exception {
        super.tearDown();
        removeECardType(11);
    }

    // This  method checks if the total and partial difference is diff
    private boolean checkBalance(BalanceType total, BalanceType partial, MoneyType diff) {
        BigDecimal totalAmount = new BigDecimal(total.getAvailable().getAmount());
        BigDecimal partialAmount = new BigDecimal(partial.getAvailable().getAmount());
        BigDecimal diffAmount = new BigDecimal(diff.getAmount());
        if (totalAmount.subtract(partialAmount).compareTo(diffAmount) == 0)
            return true;
        else {
            System.out.println("total: " + totalAmount);
            System.out.println("partial: " + partialAmount);
            System.out.println("diff: " + diffAmount);
            return false;
        }
    }
    
    public boolean authCaptureAcctToAcct() {
        String eCardNumber = ecards[0];
        MoneyType amount = new MoneyType("POINTS", "150");
        boolean testResult = true;
        try {
            // check balance
            BalanceType acct1InitBalance = checkAccountBalance(acct1);
            BalanceType acct2InitBalance = checkAccountBalance(acct2);
            if (acct1InitBalance != null && acct2InitBalance != null) {
                System.out.println("checkECardBalance passed.");
            } else {
                return false;
            }
            // authorize payment from ecard
            AuthorizeECardPaymentRequestType authReq = createAuthorizationRequest(
                    eCardNumber, amount);
            AuthorizeECardPaymentResponseType authResp = pas.authorizeECardPayment(authReq);
            if (checkAbstractResponse(authReq, authResp)) {
                System.out.println("authorizeECardPayment passed.");
            } else {
                return false;
            }
            // cpature payment to acct1
            PasTokenType token = authResp.getAuthorizationToken();
            CaptureECardToAccountRequestType capReq = createCaptureECardToAccountRequest(
                    token, acct1, amount);
            CaptureECardToAccountResponseType capResp = pas.captureECardToAccount(capReq);
            if (checkAbstractResponse(capReq, capResp)) {
                System.out.println("captureECardToAccount passed.");
            } else {
                return false;
            }
            // check acct balance
            BalanceType acct1NewBalance = checkAccountBalance(acct1);
            // initBalance is 0, newBalance is the amout just captured. 
            if (checkBalance(acct1NewBalance, acct1InitBalance, amount))
                System.out.println("new balance is correct.");
            else 
                return false;
            // authorize payment from acct1
            AuthorizeAccountPaymentRequestType acctAuthReq = createAcctAuthorizationRequest(
                    acct1, amount);
            AuthorizeAccountPaymentResponseType acctAuthResp = pas.authorizeAccountPayment(acctAuthReq);
            if (checkAbstractResponse(acctAuthReq, acctAuthResp)) {
                System.out.println("authorizeAccountPayment passed.");
            } else {
                return false;
            }
            //  capture payment to acct2
            token = acctAuthResp.getAuthorizationToken();
            CaptureAccountToAccountRequestType capReq2 = createCaptureAccountToAccountRequest(token, acct2, amount);
            CaptureAccountToAccountResponseType capResp2 = pas.captureAccountToAccount(capReq2);
            if (checkAbstractResponse(capReq2, capResp2)) {
                System.out.println("captureAcctToAcct passed.");
            } else {
                return false;
            }
            // check acct1 and acct2 balance
            acct1InitBalance = acct1NewBalance;
            acct1NewBalance = checkAccountBalance(acct1);
            BalanceType acct2NewBalance = checkAccountBalance(acct2);
            if (checkBalance(acct1InitBalance, acct1NewBalance, amount) 
                    && checkBalance(acct2NewBalance, acct2InitBalance, amount))
                System.out.println("new balance is correct.");
            else 
                return false;
        } catch (Exception e) {
            System.out.println(getStackTrace(e));
            System.out.println(e.getMessage());
            testResult = false;
        }
        return testResult;
    }
    
    public void testAuthCaptureAcctToAcct() {
        assertTrue(authCaptureAcctToAcct());
    }   
}

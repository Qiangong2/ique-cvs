package com.broadon.unitTest.ias;

import com.broadon.wsapi.ias.AuthenticateAccountRequestType;
import com.broadon.wsapi.ias.AuthenticateAccountResponseType;

public class AuthenticateAccountSuccess extends TestCaseWrapper {

    public static void main(String[] args) {
    }

    public AuthenticateAccountSuccess(String name) {
	super(name);
    }

    public void setUp() throws Exception {
	super.setUp();
    }

    public void tearDown() throws Exception {
	super.tearDown();
    }

    private boolean checkAuthenticateAccountSuccess(
                String accountId,
		String loginName, 
		String password,
                String tokenId,
                boolean valid,
		AuthenticateAccountResponseType expected)
	throws Throwable
    {
	boolean testResult = true;

        AuthenticateAccountResponseType resp;
        AuthenticateAccountRequestType req = new AuthenticateAccountRequestType();
        
        initRequest(req);
	req.setLoginName(loginName);
	req.setPassword(password);
		
	resp = ias.authenticateAccount(req);
			
	// Check response against expected response
	testResult &= checkError(expected, resp);
        testResult &= (accountId.equals(String.valueOf(resp.getAccountId())));
        
        if (tokenId != null) {
            if (testResult && valid)
                testResult = (tokenId.equals(resp.getTokenId()));
            else if (testResult && !valid) {
                testResult = (!tokenId.equals(resp.getTokenId()));
                TestDataAdmin.setValidAccountToken(accountId, resp.getTokenId());
            }    
        } else {
            if (testResult)
                TestDataAdmin.setValidAccountToken(accountId, resp.getTokenId());            
        }
        
	return testResult;
    }

    public void testAuthenticateAccountSuccess()
	throws Throwable
    {
        
        // Case 1: Authenticate a account with an existing valid token
        Token td = TestDataAdmin.getValidAccountToken();
        
        if (td != null) {
            AuthenticateAccountResponseType expected = new AuthenticateAccountResponseType();
            expected.setAccountId(new Integer(Integer.parseInt(td.id)));
            expected.setErrorCode(000);
            expected.setErrorMessage("");
            assertTrue(checkAuthenticateAccountSuccess(td.id, "login"+td.id, "pass"+td.id, td.tokenId, true, expected));
        } else {
            System.out.println(getName() + " Warning: Failed to obtain a valid account token");
        }
        
        // Case 2: Authenticate a account with an existing but expired token
        td = TestDataAdmin.getExpiredAccountToken();
        
        if (td != null) {
            AuthenticateAccountResponseType expected = new AuthenticateAccountResponseType();
            expected.setAccountId(new Integer(Integer.parseInt(td.id)));
            expected.setErrorCode(000);
            expected.setErrorMessage("");
            assertTrue(checkAuthenticateAccountSuccess(td.id, "login"+td.id, "pass"+td.id, td.tokenId, false, expected));
        } else {
            System.out.println(getName() + " Warning: Failed to obtain an expired account token");
        }
        
        // Case 3: Authenticate a account with no existing tokens
        String accountId = TestDataAdmin.getAccountIdWithNoToken();
        
        if (accountId != null) {
            AuthenticateAccountResponseType expected = new AuthenticateAccountResponseType();
            expected.setAccountId(new Integer(Integer.parseInt(accountId)));
            expected.setErrorCode(000);
            expected.setErrorMessage("");
            assertTrue(checkAuthenticateAccountSuccess(accountId, "login"+accountId, "pass"+accountId, null, false, expected));
        } else {
            System.out.println(getName() + " Warning: Failed to obtain a account id with no existing tokens");
        }
    }
}

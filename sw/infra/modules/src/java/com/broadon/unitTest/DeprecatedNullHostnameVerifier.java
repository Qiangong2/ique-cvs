package com.broadon.unitTest;

public class DeprecatedNullHostnameVerifier implements com.sun.net.ssl.HostnameVerifier
{
    private static boolean INITIALIZED = false;
    public boolean verify(String arg0, String arg1)
        {
            return true;
        }
    public static void install()
        {
            if (!INITIALIZED)
            {
                com.sun.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(new DeprecatedNullHostnameVerifier());
                INITIALIZED = true;
            }
        }
}

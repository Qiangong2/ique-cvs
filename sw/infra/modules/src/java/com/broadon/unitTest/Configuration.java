package com.broadon.unitTest;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/*
 * Configuration provides JUnit interface.
 *  - this is used by the JMeter JUnit Sampler (used once controller) 
 *    to do initialization.
 */


import junit.framework.TestCase;

public class Configuration extends TestCase {

    private final static String PERF_PROPERTIES = "perf.properties";
    static private Properties perfProperties;
    
    static private String dataSet;
    static private String security;
    static private String product;
    static private String hostName;
    static private String domainName;
    static private String dbUrl; 
    static private String etsPort;
    static private String iasPort;
    static private String ecsPort;
    static private String ecsSSLPort;
    static private String cpsPort;
    static private String pasPort;
    static private String ogsPort;
    static private String pcisPort;
    static private String bmsPort;
    static private String nusPort;
    static private String ccsimuPort;
    static private String ossPort;

    public final static String RVL = "RVL";
    public final static String NC = "NC";

    public final static String SSL = "SSL";
    public final static String NONE = "NONE";

    public final static String UNIT = "UNIT";
    public final static String PERF = "PERF";

    static
    {
        XTrustProvider.install();
        DeprecatedNullHostnameVerifier.install();
  
        // testLab1();
        // testLab2();
        // testLab3();
        // testLocalConnection();
        // testUseProperties();
        // testTCPMonConnection();
    }
    
    static public String getEtsPort() {
        return etsPort;
    }
        
    static public String getEcsPort() {
        return ecsPort;
    }
        
    static public String getEcsSSLPort() {
        return ecsSSLPort;
    }
        
    static public String getIasPort() {
        return iasPort;
    }

    static public String getCpsPort() {
        return cpsPort;
    }
    
    static public String getPasPort() {
        return pasPort;
    }
        
    static public String getOgsPort() {
        return ogsPort;
    }
    
    static public String getPcisPort() {
        return pcisPort;
    }
    
    static public String getBmsPort() {
        return bmsPort;
    }
    
    static public String getNusPort() {
        return nusPort;
    }
    
    static public String getCcsimuPort() {
        return ccsimuPort;
    }
    
    static public String getOssPort() {
        return ossPort;
    }
    
    static public String getHostName() {
        return hostName;
    }
    
    static public String getDomainName() {
        return domainName;
    }
    
    static public String getSvcHost(String svc) {
        String ret;

        if (hostName == "") {
            ret = svc + domainName;
        } else {
            ret = hostName + domainName;
        }

        return ret;
    }

    static public String getDbUrl() {
        return dbUrl;
    }
        
    static public String getProduct() {
        return product;
    }
        
    static public String getSecurity() {
        return security;
    }
        
    static public String getDataSet() {
        return dataSet;
    }
        
    static public void setEtsPort(String p) {
        etsPort = p;
    }
        
    static public void setEcsPort(String p) {
        ecsPort = p;
    }
        
    static public void setEcsSSLPort(String p) {
        ecsSSLPort = p;
    }
        
    static public void setIasPort(String p) {
        iasPort = p;
    }   
    
    static public void setCpsPort(String p) {
        cpsPort = p;
    }
    
    static public void setPasPort(String p) {
        pasPort = p;
    }
    
    static public void setOgsPort(String p) {
        ogsPort = p;
    }
    
    static public void setPcisPort(String p) {
        pcisPort = p;
    }
    
    static public void setBmsPort(String p) {
        bmsPort = p;
    }
    
    static public void setNusPort(String p) {
        nusPort = p;
    }
    
    static public void setCcsimuPort(String p) {
        ccsimuPort = p;
    }
    
    static public void setOssPort(String p) {
        ossPort = p;
    }
    
    static public void setHostName(String s) {
        hostName = s;
    }
    
    static public void setDomainName(String s) {
        domainName = s;
    }
    
    static public void setDbUrl(String s) {
        dbUrl = s;
    }    

    static public void setProduct(String s) {
        product = s;
    }    

    static public void setSecurity(String s) {
        security = s;
    }    

    static public void setDataSet(String s) {
        dataSet = s;
    }    

    public Configuration(String name) {
        super(name);
    }

    static public Properties getPerfProperties() {
        if (perfProperties == null) {
            // Read properties file.
            perfProperties = new Properties();
            try {
                perfProperties.load(new FileInputStream(PERF_PROPERTIES));
            } catch (IOException e) {
                System.out.println("can't find "+PERF_PROPERTIES);
            }
        }

        return perfProperties;
    }

    static public void printConfig() {
        System.out.println("### DataSet " + getDataSet());
        System.out.println("### Security " + getSecurity());
        System.out.println("### Product " + getProduct());
        System.out.println("### Default Server Host " + getHostName());
        System.out.println("### Default Server Domain " + getDomainName());
        System.out.println("### Default DB URL " + getDbUrl());
        System.out.println("### ETS Port " + getEtsPort());
        System.out.println("### IAS Port " + getIasPort());
        System.out.println("### ECS Port " + getEcsPort());
        System.out.println("### ECS SSL Port " + getEcsSSLPort());
        System.out.println("### CPS Port " + getCpsPort());
        System.out.println("### PAS Port " + getPasPort());
        System.out.println("### OGS Port " + getOgsPort());
        System.out.println("### PCIS Port " + getPcisPort());
        System.out.println("### BMS Port " + getBmsPort());
        System.out.println("### NUS Port " + getNusPort());
        System.out.println("### CCSIMU Port " + getCcsimuPort());
        System.out.println("### OSS Port " + getOssPort());
    }
    
    static private void setStandardPorts()
    {
        setIasPort("16980");
        setEcsSSLPort("16981");   
        setEcsPort("17105");   
        setEtsPort("17100");
        setCpsPort("17102");
        setPasPort("17103");   
        setOgsPort("17104");   
        setPcisPort("17106");
        setNusPort("16987");
        setCcsimuPort("17199");
        setBmsPort("");
        setOssPort("16984");
    }

    static public void testLab1() {
        setDataSet(UNIT);
        setSecurity(SSL);
        setProduct(NC);
        setHostName("");
        setDomainName(".bbu.lab1.routefree.com");
        setDbUrl("jdbc:oracle:thin:@(DESCRIPTION=(FAILOVER=yes)(LOAD_BALANCE=no)(ADDRESS=(PROTOCOL=TCP)(HOST=db1-vip.bcc.lab1.routefree.com)(PORT=1521))(ADDRESS=(PROTOCOL=TCP)(HOST=db2-vip.bcc.lab1.routefree.com)(PORT=1521))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=BCCDB)(FAILOVER_MODE=(TYPE=SELECT)(METHOD=BASIC)(RETRIES=180)(DELAY=5))))");
        setStandardPorts();
        printConfig();
    }
        
    static public void testLab2() {
        setDataSet(UNIT);
        setSecurity(SSL);
        setProduct(RVL);
        setHostName("");
        setDomainName(".lab2.routefree.com");
        setDbUrl("jdbc:oracle:thin:@(DESCRIPTION=(FAILOVER=yes)(LOAD_BALANCE=no)(ADDRESS=(PROTOCOL=TCP)(HOST=db2-vip.lab2.routefree.com)(PORT=1521))(ADDRESS=(PROTOCOL=TCP)(HOST=db2-vip.lab2.routefree.com)(PORT=1521))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=WIIDB)(FAILOVER_MODE=(TYPE=SELECT)(METHOD=BASIC)(RETRIES=180)(DELAY=5))))");
        setStandardPorts();
        printConfig();
    }
    
    static public void testLab3() {
        setDataSet(UNIT);
        setSecurity(SSL);
        setProduct(RVL);
        setHostName("");
        setDomainName(".lab3.routefree.com");
        setDbUrl("jdbc:oracle:thin:@(DESCRIPTION=(FAILOVER=yes)(LOAD_BALANCE=no)(ADDRESS=(PROTOCOL=TCP)(HOST=db1-vip.lab3.routefree.com)(PORT=1521))(ADDRESS=(PROTOCOL=TCP)(HOST=db2-vip.lab3.routefree.com)(PORT=1521))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=WIIDB)(FAILOVER_MODE=(TYPE=SELECT)(METHOD=BASIC)(RETRIES=180)(DELAY=5))))");
        setStandardPorts();
        printConfig();
    }

    static public void testUseProperties() {
        // Read properties file.
        Properties prop = getPerfProperties();
        String tmp;
        tmp = prop.getProperty("dataSet", PERF);
        setDataSet(tmp);
        tmp = prop.getProperty("security", SSL);
        setSecurity(tmp);
        tmp = prop.getProperty("product", RVL);
        setProduct(tmp);
        tmp = prop.getProperty("hostName", "");
        setHostName(tmp);
        tmp = prop.getProperty("domainName", "");
        setDomainName(tmp);
        tmp = prop.getProperty("dbUrl", "");
        setDbUrl(tmp);
        setStandardPorts();
        printConfig();
    }

    static public void testLocalConnection() {
        setDataSet(UNIT);
        setSecurity(NONE);
        setProduct(RVL);
        setHostName("localhost");
        setDomainName("");
        setEtsPort("8080");
        setIasPort("8080");
        setEcsPort("8080");
        setEcsSSLPort("8080");
        setCpsPort("8080");
        setPasPort("8080");
        setOgsPort("8080");
        setCcsimuPort("8080");
        setOssPort("8080");
        printConfig();
    }
        
    static public void testTCPMonConnection() {
        setDataSet(UNIT);
        setSecurity(NONE);
        setProduct(RVL);
        setHostName("localhost");
        setDomainName("");
        setIasPort("6980");
        setEcsPort("7105");
        setEcsSSLPort("6981");
        setEtsPort("7100");
        setCpsPort("7102");
        setPasPort("7103");
        setOgsPort("7104");
        setPcisPort("7106");
        setCcsimuPort("7199");
        printConfig();
    };
}

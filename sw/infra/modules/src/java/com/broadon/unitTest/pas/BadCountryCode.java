package com.broadon.unitTest.pas;

import com.broadon.wsapi.pas.AuthorizeECardPaymentRequestType;
import com.broadon.wsapi.pas.AuthorizeECardPaymentResponseType;
import com.broadon.wsapi.pas.MoneyType;

public class BadCountryCode extends TestCaseWrapper {

    public static void main(String[] args) {
        junit.textui.TestRunner.run(BadCountryCode.class);
    }

    public BadCountryCode(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
        resetTestDataBase(ECardTests);
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    public boolean checkBadCountryCode() {
        boolean testResult = true;

        MoneyType amount = new MoneyType("POINTS", "10");
        // from batch03:    000101 0010000000 0014264647    
        String eCardSequence = "0010000000";
        AuthorizeECardPaymentRequestType authReq = createAuthorizationRequest(eCardSequence, amount);
        authReq.getECardID().setCountryCode("JP");  // wrong country code
        AuthorizeECardPaymentResponseType authResp;
        boolean authReturn=true;
        try {
            authResp = pas.authorizeECardPayment(authReq);
            authReturn = checkAbstractResponse(authReq, authResp);
            if (authReturn) 
                return false;   // fail to check BadCountryCode?
            amount = new MoneyType("POINTS", "10");
            authResp = pas.authorizeECardPayment(authReq);
            authReturn = checkAbstractResponse(authReq, authResp);
            if (authReturn) 
                return false;   // fail to check BadCountryCode?
            return true;
        } catch (Exception e) {
            System.out.println(getStackTrace(e));
            System.out.println(e.getMessage());
            testResult = false;
        }
        
        return testResult;
    }
    
    public void testBadMoneyString() {
        assertTrue(checkBadCountryCode());
    }   
}
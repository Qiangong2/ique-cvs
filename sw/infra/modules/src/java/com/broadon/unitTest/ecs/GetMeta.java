package com.broadon.unitTest.ecs;

import org.apache.axis.types.URI;
import org.apache.axis.types.URI.MalformedURIException;

import com.broadon.wsapi.ecs.ContentInfoType;
import com.broadon.wsapi.ecs.GetMetaRequestType;
import com.broadon.wsapi.ecs.GetMetaResponseType;
import com.broadon.wsapi.ecs.TitleVersionType;

public class GetMeta extends TestCaseWrapper {

    public static void main(String[] args) {
        junit.textui.TestRunner.run(GetMeta.class);
    }

    public GetMeta(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    public boolean checkGetMeta(long deviceId, GetMetaResponseType expected)
        throws Throwable
    {
        boolean testResult = true;
        GetMetaResponseType resp;
        GetMetaRequestType req = new GetMetaRequestType();

        initAbstractRequest(req);
        req.setDeviceId(deviceId);

        resp = ecs.getMeta(req);
        testResult &= checkAbstractResponse(req, resp);

        // Check response against expected
        testResult &= checkError(expected, resp);

        testResult &= checkTitleVersions("getTitleVersion", expected.getTitleVersion(), resp.getTitleVersion());
        // Don't check content prefix url
        //assertEquals("getContentPrefixURL",
        //        expected.getContentPrefixURL(), resp.getContentPrefixURL());
        testResult &= checkContents("getPreloadContents",
                expected.getPreloadContents(), resp.getPreloadContents());

        return testResult;
    }
    
    public void initExpected(GetMetaResponseType expected)
    {
        try {
            expected.setContentPrefixURL(new URI("http://ccs:16983/ccs/download"));
//            expected.setPreloadContents( 
//                    new ContentInfoType[] { 
//                            new ContentInfoType("000200010008000100000002", 16816288),
//                            new ContentInfoType("000200010008000100000003", 520),
//                            new ContentInfoType("000200010008000200000002", 16816288),
//                            new ContentInfoType("000200010008000200000004", 520)
//                    } );
        } catch (MalformedURIException e) {
            fail("Error initializing GetMetaResponseType: " + e.getLocalizedMessage());
        }
    }
    
    public void testGetMeta()
        throws Throwable
    {
        GetMetaResponseType expected = new GetMetaResponseType();
        initExpected(expected);
        
        long deviceId = validNC1;
        TitleVersionType ncSecureKernel = new TitleVersionType();
        ncSecureKernel.setTitleId("0000000200000001");
        ncSecureKernel.setVersion(-1); // Don't check version
        
        TitleVersionType ncViewer = new TitleVersionType();
        ncViewer.setTitleId("0000000200000002");
        ncViewer.setVersion(-1); // Don't check version

        TitleVersionType ncOS = new TitleVersionType();
        ncOS.setTitleId("0000000200000003");
        ncOS.setVersion(-1); // Don't check version

        expected.setErrorCode(0);
        expected.setTitleVersion(new TitleVersionType[] { ncSecureKernel, ncViewer, ncOS } );

        assertTrue(checkGetMeta(deviceId, expected));
    }
}

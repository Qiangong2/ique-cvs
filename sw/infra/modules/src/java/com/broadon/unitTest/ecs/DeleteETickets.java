package com.broadon.unitTest.ecs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.broadon.unitTest.Configuration;
import com.broadon.unitTest.DBConnection;
import com.broadon.wsapi.ecs.DeleteETicketsRequestType;
import com.broadon.wsapi.ecs.DeleteETicketsResponseType;

public class DeleteETickets extends TestCaseWrapper {
    
    // Some example etickets in the DB for lab1
    final static long sampleDevice = 4614987296L;
    final static long[] sampleETickets = {47301057187581604L, 47301057187581601L};
    
    public static void main(String[] args) {
        junit.textui.TestRunner.run(DeleteETickets.class);
    }

    public DeleteETickets(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
    }

    public void tearDown() throws Exception {
        super.tearDown();
        undeleteETickets();
    }
    
    protected void undeleteETickets() {
        // DB Parameters
        String db_user = "ecs";
        String db_pswd = "ecs";
        String db_url = Configuration.getDbUrl();
        
        String resetETickets = "UPDATE etickets SET revoke_date = NULL WHERE device_id = ? AND tid = ? ";
        Connection conn = null;
        PreparedStatement pstmt = null;
        try {
            DBConnection db = new DBConnection(db_user, db_pswd, db_url);
            conn = db.getConnection();
            pstmt = conn.prepareStatement(resetETickets);
            for (int i=0; i<sampleETickets.length; i++) {
                pstmt.setLong(1, sampleDevice);
                pstmt.setLong(2, sampleETickets[i]);
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            conn.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (conn != null)
                    conn.close();
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
     }

    private boolean checkDeleteETickets()
        throws Throwable
    {
        boolean testResult = true;

        DeleteETicketsResponseType resp;
        DeleteETicketsRequestType req = new DeleteETicketsRequestType();

        initAbstractRequest(req);
        req.setDeviceId(sampleDevice);
        req.setTickets(sampleETickets);

        resp = ecs.deleteETickets(req);
        testResult &= checkAbstractResponse(req, resp);
        assertEquals("getErrorCode", 0, resp.getErrorCode());
        assertEquals("getErrorMessage", null, resp.getErrorMessage());
            
        return testResult;
    }

    
    public void testDeleteETickets()
        throws Throwable
    {
        assertTrue(checkDeleteETickets());
    }
    
}

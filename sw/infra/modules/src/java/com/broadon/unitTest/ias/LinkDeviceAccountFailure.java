package com.broadon.unitTest.ias;

import org.apache.axis.AxisFault;

import com.broadon.wsapi.ias.LinkDeviceAccountRequestType;
import com.broadon.wsapi.ias.LinkDeviceAccountResponseType;

public class LinkDeviceAccountFailure extends TestCaseWrapper {

    public static void main(String[] args) {
    }

    public LinkDeviceAccountFailure(String name) {
	super(name);
    }

    public void setUp() throws Exception {
	super.setUp();
    }

    public void tearDown() throws Exception {
	super.tearDown();
    }
    
    private boolean checkLinkDeviceAccountException(
		String deviceId, 
                String tokenId,
                String accountId)
	throws Throwable
    {
	boolean testResult = true;

        LinkDeviceAccountRequestType req = new LinkDeviceAccountRequestType();
        
        initRequest(req);
	req.setDeviceId(Long.parseLong(deviceId));
        req.setTokenId(tokenId);
        req.setAccountId(Integer.parseInt(accountId));
		
	try {
            LinkDeviceAccountResponseType resp = ias.linkDeviceAccount(req);
            
        } catch (AxisFault a) {
            testResult &= a.getFaultString() != null &&
            a.getFaultString().equals(UNAUTHORIZED_ACCESS_MESSAGE);          
        }
                
	return testResult;
    }

    public void testLinkDeviceAccountFailure()
	throws Throwable
    {
        // Case 1: Device with expired token and valid account
        String deviceId = TestDataAdmin.getDeviceIdWithExpiredTokenAndAccount(false);
                
        if (deviceId != null) {
            String token = TestDataAdmin.getExpiredDeviceToken(deviceId);
            String accounts[] = TestDataAdmin.getValidDeviceAccounts(deviceId);
            
            assertTrue(checkLinkDeviceAccountException(deviceId, token, accounts[0]));

        } else {
            System.out.println(getName() + " Warning: Failed to obtain a device id with expired token and a linked account");
        }      
    }    
}

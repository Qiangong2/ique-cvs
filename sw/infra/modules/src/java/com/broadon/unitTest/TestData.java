package com.broadon.unitTest;

import java.sql.Connection;
import java.sql.CallableStatement;
import java.sql.SQLException;

import com.broadon.unitTest.Configuration;
import com.broadon.unitTest.DBConnection;

public class TestData {
    public static void Usage() {
        System.out
            .println("TestData [ServerLocation] [Command]");
        System.out
            .println("  ServerLocation: lab1,lab2,lab3,prop");
        System.out
            .println("  Command: createTestData,removeTestData,createPerfData,removePerfData]");
    }
  
    public static void main(String[] args) {
        if (args.length < 2) {
            Usage();
            return;
        }

        String serverLoc = args[0];
        String command = args[1];

        if (serverLoc.equals("lab1")) {
            Configuration.testLab1();
        } else if (serverLoc.equals("lab2")) {
            Configuration.testLab2();
        } else if (serverLoc.equals("lab3")) {
            Configuration.testLab3();
        } else if (serverLoc.equals("prop")) {
            Configuration.testUseProperties();
        }  else {
            System.out.println("invalid server location - " + serverLoc);
            Usage();
            return;
        }
        
        execCommand(command);
    }

    public static void execCommand(String command) {
        String db_user = "bms";
        String db_pswd = "bms";
        String db_url = Configuration.getDbUrl();
        Connection conn = null;
        CallableStatement proc = null;
        
        try
        {
            DBConnection db = new DBConnection(db_user, db_pswd, db_url);
            conn = db.getConnection();
            proc = conn.prepareCall("{ call bmsops."+command+" }");
            proc.execute();
            conn.commit();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}

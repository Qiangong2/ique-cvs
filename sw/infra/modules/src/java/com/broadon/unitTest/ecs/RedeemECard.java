package com.broadon.unitTest.ecs;

import com.broadon.wsapi.ecs.RedeemECardRequestType;
import com.broadon.wsapi.ecs.RedeemECardResponseType;
import com.broadon.wsapi.ecs.RedeemKindType;

public class RedeemECard extends TestCaseWrapper {
    public static void main(String[] args) {
        junit.textui.TestRunner.run(RedeemECard.class);
    }

    public RedeemECard(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
        clearETickets(testDevice, "SR");
        createAccount(validVCAccount.getAccountPayment().getAccountNumber());
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    private boolean checkRedeemECard(
            RedeemECardRequestType req,
            RedeemECardResponseType expected)
        throws Throwable
    {
        boolean testResult = true;

        RedeemECardResponseType resp;

        resp = ecs.redeemECard(req);
        testResult &= checkAbstractResponse(req, resp);
            
        // Check response against expected response
        testResult &= checkError(expected, resp);

        assertEquals(expected.getTotalPoints(), resp.getTotalPoints());
        assertEquals(expected.getRedeemedPoints(), resp.getRedeemedPoints());
        
        return testResult;
    }

    /**
     * Initializes request to redeem a valid points item
     * @param req
     */
    protected void initRedeemPointsECardRequest(RedeemECardRequestType req) {
        initAbstractRequest(req);
        req.setDeviceId(testDevice);
        req.setVirtualDeviceType(new Integer(DEVICE_TYPE_RV));
        makePointsECard();
        req.setECard(validPointsECard.getECardPayment());
        req.setAccount(validVCAccount.getAccountPayment());
        req.setCountryCode(testCountry);
        // TODO: WSDL requires device cert
        req.setDeviceCert(validDeviceCert);
    }

    public void testRedeemPointsECard()
        throws Throwable
    {
        RedeemECardRequestType req = new RedeemECardRequestType();
        RedeemECardResponseType expected = new RedeemECardResponseType();
        initRedeemPointsECardRequest(req);
        expected.setRedeemedPoints(new Long(1000));
        expected.setTotalPoints(new Long(1000 + Long.parseLong(START_BALANCE)));

        assertTrue(checkRedeemECard(req, expected));
        
        // get ecard used once error
        expected.setErrorCode(815);
        expected.setErrorMessage(".*Account Has Been Used Once.*");        
        expected.setRedeemedPoints(null);
        expected.setTotalPoints(null);
        assertTrue(checkRedeemECard(req, expected));
    }

    public void testRedeemPointsECardRedeemKind()
        throws Throwable
    {
        RedeemECardRequestType req = new RedeemECardRequestType();
        RedeemECardResponseType expected = new RedeemECardResponseType();
        initRedeemPointsECardRequest(req);
        req.setRedeemKind(RedeemKindType.Points);
        expected.setRedeemedPoints(new Long(1000));
        expected.setTotalPoints(new Long(1000 + Long.parseLong(START_BALANCE)));

        assertTrue(checkRedeemECard(req, expected));
        
        // get ecard used once error
        expected.setErrorCode(815);
        expected.setErrorMessage(".*Account Has Been Used Once.*");        
        expected.setRedeemedPoints(null);
        expected.setTotalPoints(null);
        assertTrue(checkRedeemECard(req, expected));
    }

    public void testRedeemPointsECardBadRedeemKind()
        throws Throwable
    {
        RedeemECardRequestType req = new RedeemECardRequestType();
        RedeemECardResponseType expected = new RedeemECardResponseType();
        initRedeemPointsECardRequest(req);
        req.setRedeemKind(RedeemKindType.Subscription);

        // get bad redeem kind error
        expected.setErrorCode(609);
        expected.setErrorMessage(".*cannot be redeemed for Subscription.*");        
        assertTrue(checkRedeemECard(req, expected));
    }

    public void testRedeemPointsECardNoAccount()
        throws Throwable
    {
        RedeemECardRequestType req = new RedeemECardRequestType();
        RedeemECardResponseType expected = new RedeemECardResponseType();
        initRedeemPointsECardRequest(req);
        req.setAccount(null);
        expected.setErrorCode(618);
        expected.setErrorMessage(".*Account information is not provided.*");        

        assertTrue(checkRedeemECard(req, expected));
    }

    public void testRedeemPointsECardBadCountry()
        throws Throwable
    {
        RedeemECardRequestType req = new RedeemECardRequestType();
        RedeemECardResponseType expected = new RedeemECardResponseType();
        initRedeemPointsECardRequest(req);
        req.setCountryCode(COUNTRY_US);
        expected.setErrorCode(616);
        expected.setErrorMessage(".*ECard cannot be used in country.*");        

        assertTrue(checkRedeemECard(req, expected));
    }
}

package com.broadon.unitTest.ecs;

import java.math.BigInteger;
import java.util.Arrays;

import com.broadon.wsapi.ecs.ListETicketsRequestType;
import com.broadon.wsapi.ecs.ListETicketsResponseType;
import com.broadon.wsapi.ecs.ETicketType;

public class ListETickets extends TestCaseWrapper {
    public final static long testNC = getDeviceId(DEVICE_TYPE_NC, 13);
    
    // Some example etickets in the DB for lab1
    final static ETicketInfo[] sampleETickets = new ETicketInfo[] {
        new ETicketInfo(testNC, "0002000100000000", 
                        "SR", 1230429003, "Subscription")
    };

    private static class ETicketInfo {
        long deviceId;
        long eticketId;
        String titleId;
        String limitType;
        long limit;
        String licenseType;
        String licenseTypeDB; /* License type in the DB */
        BigInteger contentId;

        private ETicketInfo(long deviceId, String titleId, String limitType, long limit, String licenseType)
        {
            this.deviceId = deviceId;
            this.titleId = titleId;
            this.limitType = limitType;
            this.limit = limit;
            this.licenseType = licenseType;
            this.contentId = new BigInteger(titleId + "ffffffff", 16);
            if ("Subscription".equalsIgnoreCase(licenseType)) {
                licenseTypeDB = "SUBSCRIPT";
            } else {
                licenseTypeDB = licenseType.toUpperCase();
            }
        }   
    }
    
    public static void main(String[] args) {
        junit.textui.TestRunner.run(ListETickets.class);
    }

    public ListETickets(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
        clearETickets(testNC, null);
    }

    public void tearDown() throws Exception {
        clearETickets(testNC, null);
        super.tearDown();
    }
    
    // Returns the number of times the given etickets occurs in the list of etickets
    public int findOccurances(ETicketType[] etickets, ETicketInfo eticketInfo)
    {
        if (etickets == null) {
            return 0;
        }
        int count = 0;
        for (int i = 0; i < etickets.length; i++) {
            if (eticketInfo.eticketId == etickets[i].getTicketId()) {
                assertEquals("ETicket " + eticketInfo.eticketId + " title Id",
                        eticketInfo.titleId, etickets[i].getTitleId());
                assertEquals("ETicket " + eticketInfo.eticketId + " license type",
                        eticketInfo.licenseType, etickets[i].getLicenseType().getValue());
                assertEquals("ETicket " + eticketInfo.eticketId + " limit kind",
                        eticketInfo.limitType, etickets[i].getLimits(0).getLimitKind());
                assertEquals("ETicket " + eticketInfo.eticketId + " limit total",
                        eticketInfo.limit, etickets[i].getLimits(0).getLimits());
                count++;
            }
        }
        return count;
    }
    
    private boolean checkListETickets(
            long deviceId,
            ListETicketsResponseType expected)
        throws Throwable
    {
        boolean testResult = true;

        ListETicketsResponseType resp;
        ListETicketsRequestType req = new ListETicketsRequestType();

        initAbstractRequest(req);
        req.setDeviceId(deviceId);

        resp = ecs.listETickets(req);
        testResult &= checkAbstractResponse(req, resp);
        if (expected != null) {
            testResult &= checkError(expected, resp);
            assertTrue("getETickets", 
                    Arrays.equals(expected.getTickets(), resp.getTickets()));          
        } else {
            assertEquals("getErrorCode", 0, resp.getErrorCode());
            assertEquals("getErrorMessage", null, resp.getErrorMessage());
            
            for (int i = 0; i < sampleETickets.length; i++) {
                ETicketInfo eticketInfo = sampleETickets[i];
                int occurs = findOccurances(resp.getTickets(), eticketInfo);
                if (deviceId == eticketInfo.deviceId) {
                    // Expect this eticket to be in list of etickets
                    assertEquals("ETicket " + eticketInfo.eticketId + " occurs", 1, occurs);
                } else {
                    // Expect this eticket not to be in list of etickets
                    assertEquals("ETicket " + eticketInfo.eticketId + " occurs", 0, occurs);
                }
            }           
        }

        return testResult;
    }

    
    public void testListETickets()
        throws Throwable
    {
        for (int i = 0; i < sampleETickets.length; i++) {
            ETicketInfo eTicket = sampleETickets[i];
            eTicket.eticketId = createETicket(eTicket.deviceId, eTicket.contentId, 
                    eTicket.licenseTypeDB, eTicket.limitType, eTicket.limit);
        }
        assertTrue(checkListETickets(testNC, null));
    }
    
}

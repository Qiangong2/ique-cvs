package com.broadon.unitTest.ias;

import com.broadon.wsapi.ias.AuthenticateAccountRequestType;
import com.broadon.wsapi.ias.AuthenticateAccountResponseType;

public class AuthenticateAccountFailure extends TestCaseWrapper {

    public static void main(String[] args) {
    }

    public AuthenticateAccountFailure(String name) {
	super(name);
    }

    public void setUp() throws Exception {
	super.setUp();
    }

    public void tearDown() throws Exception {
	super.tearDown();
    }

    private boolean checkAuthenticateAccountFailure(
		String loginName, 
		String password,
                AuthenticateAccountResponseType expected)
	throws Throwable
    {
	boolean testResult = true;

        AuthenticateAccountResponseType resp;
        AuthenticateAccountRequestType req = new AuthenticateAccountRequestType();
        
        initRequest(req);
	req.setLoginName(loginName);
	req.setPassword(password);
        		
	resp = ias.authenticateAccount(req);
			
	// Check response against expected response
	testResult &= checkError(expected, resp);
        
	return testResult;
    }

    public void testAuthenticateAccountFailure()
	throws Throwable
    {
        // Case 1: Authenticate an un-registered account
        String accountId = TestDataAdmin.getUnRegisteredAccountId();
        
        if (accountId != null) {
            AuthenticateAccountResponseType expected = new AuthenticateAccountResponseType();
            expected.setErrorCode(931);
            expected.setErrorMessage("IAS - Failed to authenticate account");
            assertTrue(checkAuthenticateAccountFailure("login"+accountId, "pass"+accountId, expected));
        } else {
            System.out.println(getName() + " Warning: Failed to obtain an unregistered Account Id");
        }
        
        // Case 2: Authenticate a registered account with wrong password
        accountId = TestDataAdmin.getRegisteredAccountId();
        
        if (accountId != null) {
            AuthenticateAccountResponseType expected = new AuthenticateAccountResponseType();
            expected.setErrorCode(931);
            expected.setErrorMessage("IAS - Failed to authenticate account");
            
            assertTrue(checkAuthenticateAccountFailure("login"+accountId, "irjhfwaurew", expected));            
            assertTrue(checkAuthenticateAccountFailure("irjhfwaurew", "pass"+accountId, expected));
            
        } else {
            System.out.println(getName() + " Warning: Failed to obtain a registered Account Id");
        }
    }
}

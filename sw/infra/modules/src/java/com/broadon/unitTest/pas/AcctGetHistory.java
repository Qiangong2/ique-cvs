package com.broadon.unitTest.pas;

import java.rmi.RemoteException;

import com.broadon.wsapi.pas.AuthorizeAccountPaymentRequestType;
import com.broadon.wsapi.pas.AuthorizeAccountPaymentResponseType;
import com.broadon.wsapi.pas.BalanceAccountType;
import com.broadon.wsapi.pas.CaptureECardToAccountRequestType;
import com.broadon.wsapi.pas.CapturePaymentRequestType;
import com.broadon.wsapi.pas.CapturePaymentResponseType;
import com.broadon.wsapi.pas.CreateAccountRequestType;
import com.broadon.wsapi.pas.CreateAccountResponseType;
import com.broadon.wsapi.pas.GetAccountHistoryRequestType;
import com.broadon.wsapi.pas.GetAccountHistoryResponseType;
import com.broadon.wsapi.pas.RefundPaymentRequestType;
import com.broadon.wsapi.pas.RefundPaymentResponseType;
import com.broadon.wsapi.pas.VoidAuthorizationRequestType;
import com.broadon.wsapi.pas.PasTokenType;
import com.broadon.wsapi.pas.MoneyType;

public class AcctGetHistory extends TestCaseWrapper {
    PasTokenType token = null;
    MoneyType amount = null;
    BalanceAccountType acct = null;
    public static void main(String[] args) {
        junit.textui.TestRunner.run(AcctGetHistory.class);
    }

    public AcctGetHistory(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
        resetTestDataBase(AccountTests);
        createAccount();
        authorizeAccountPayment();
        capture();
        refund();
        voidAuthorization();
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }
    // setup
    private void createAccount() throws Exception {
        MoneyType amount = new MoneyType("POINTS", "50");
        PasTokenType token = eCardAuth("0010030001", amount);
        CreateAccountRequestType req;
        acct = new BalanceAccountType();
        acct.setAccountNumber("30001");
        acct.setPIN("30001");  
        req = createCreateAccountRequest(acct);
        CreateAccountResponseType resp = pas.createAccount(req);
        if (checkAbstractResponse(req, resp)) {
            CaptureECardToAccountRequestType req2 = createCaptureECardToAccountRequest(
                    token, acct, amount);
                pas.captureECardToAccount(req2);
        }
    }
    private void authorizeAccountPayment() throws Exception {
        amount = new MoneyType("POINTS", "10"); 
        acct = new BalanceAccountType();
        acct.setAccountNumber("30001");
        acct.setPIN("30001");
        AuthorizeAccountPaymentRequestType authReq = createAcctAuthorizationRequest(
                acct, amount);
        boolean authReturn=false;

            AuthorizeAccountPaymentResponseType authResp = pas.authorizeAccountPayment(authReq);
            authReturn = checkAbstractResponse(authReq, authResp);
            if (authReturn) {
                token = authResp.getAuthorizationToken();
            }
    }
    
    // setup
    private void voidAuthorization() throws RemoteException {
        VoidAuthorizationRequestType voidReq = createVoidAuthorizationRequest(token);
        pas.voidAuthorization(voidReq);
    }
    
    // setup capture
    private boolean capture() throws RemoteException {
        CapturePaymentResponseType capResp;
        amount.setAmount("8");
        CapturePaymentRequestType capReq = createCapturePaymentRequest(token, amount, false);
        capResp = pas.capturePayment(capReq);
        return checkAbstractResponse(capReq, capResp);
    }
    
    // setup refund
    private boolean refund() throws RemoteException {
        RefundPaymentRequestType req = createRefundPaymentRequest(token, amount);
        RefundPaymentResponseType resp;
        resp = pas.refundPayment(req);
        return checkAbstractResponse(req, resp);
    }
    
    private boolean getHistory() throws RemoteException {
        GetAccountHistoryRequestType req = createGetAccountHistoryRequest(acct);
        GetAccountHistoryResponseType resp;
        resp = pas.getBalanceAccountHistory(req);
        return checkAbstractResponse(req, resp);
    }
    
    public boolean checkAcctGetHistory() throws RemoteException {
        return getHistory();
    }
    public void testAcctGetHistory() throws RemoteException {
        assertTrue(checkAcctGetHistory());
    }   
}
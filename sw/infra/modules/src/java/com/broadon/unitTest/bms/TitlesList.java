package com.broadon.unitTest.bms;

import java.sql.*;
import java.io.IOException;
import org.xml.sax.SAXException;

import junit.framework.*;

import com.broadon.unitTest.Configuration;
import com.meterware.httpunit.*;

/**
 * Test the titlesList page.
 */
public class TitlesList extends TestCaseWrapper
{
    WebRequest request = null;

    public TitlesList(String name) {
	super(name);
    }

    protected void setUp() throws Exception {
	super.setUp();
	request = new GetMethodWebRequest(home.getURL(), "serv?type=title&action=list");
    }

    // page-specific tests

    private WebResponse goToPage()
	throws IOException, SAXException
    {
	WebResponse res = wc.getCurrentPage();
	if (res.getURL().toString().equals(request.getURL().toString()))
	    return res;
	return wc.getResponse(request);
    }


    static final String getTitleInfo =
	"SELECT ctc.title_id, ctc.hex_title_id, ctc.title, " +
        "ctc.category, ctc.title_size, ctc.platform, ctc.publisher, " +
        "ctc.publish_date, ctc.release_date, ctc.product_code, " +
        "ctc.approx_size/(16384) approx_size, nvl(ctn.b_count, 0) rel_count, " +
        "nvl(ctl.a_count, 0) lang_count, nvl(cip.c_count, 0) price_count, nvl(cim.d_count, 0) image_count " +
        "FROM CONTENT_TITLE_CATALOG ctc, " +
        "(SELECT count(unique(a.locale)) a_count, a.title_id " +
         "FROM CONTENT_TITLE_LOCALES a GROUP BY a.title_id) ctl, " +
        "(SELECT count(unique(b.country_id)) b_count, b.title_id " +
         "FROM CONTENT_TITLE_COUNTRIES b GROUP BY b.title_id) ctn, " +
        "(SELECT count(unique(c.country_id)) c_count, c.title_id " +
         "FROM CAS_COUNTRY_ITEM_PRICINGS c, CONTENT_TITLE_COUNTRIES f " +
         "WHERE c.title_id = f.title_id AND c.country_id = f.country_id GROUP BY c.title_id) cip, " +
        "(SELECT count(unique(d.content_id)) d_count, e.title_id FROM content_objects d, content_titles e " +
         "WHERE d.content_id BETWEEN " +
         "TO_NUMBER(LPAD(LTRIM(TO_CHAR(e.title_id,RPAD('X',16,'X'))),16,'0')||'FFFD0000','XXXXXXXXXXXXXXXXXXXXXXXX') " +
         "AND " +
         "TO_NUMBER(LPAD(LTRIM(TO_CHAR(e.title_id,RPAD('X',16,'X'))),16,'0')||'FFFDFFFF','XXXXXXXXXXXXXXXXXXXXXXXX') " +
         "GROUP BY e.title_id) cim " +
        "WHERE lower(ctc.title_type) = 'game' AND ctc.title_id = ? AND ctc.title_id = ctl.title_id (+) " +
        "AND ctc.title_id = ctn.title_id (+) AND ctc.title_id = cip.title_id (+) AND ctc.title_id = cim.title_id (+)";

    public void testExampleTitlesList()
	throws IOException, SAXException, SQLException
    {
	WebResponse res = goToPage();
	WebTable table = res.getTableStartingWith("No");
	assertEquals("Title", table.getCellAsText(0, 1).trim());
	assertEquals("Total Size (bytes)", table.getCellAsText(0, 2).trim());
	assertEquals("# of Blocks", table.getCellAsText(0, 3).trim());
	assertEquals("Category", table.getCellAsText(0, 4).trim());
	assertEquals("Platform", table.getCellAsText(0, 5).trim());
	assertEquals("Publisher", table.getCellAsText(0, 6).trim());
	assertEquals("Game Code", table.getCellAsText(0, 7).trim());
	assertEquals("Images", table.getCellAsText(0, 8).trim());
	assertEquals("Languages", table.getCellAsText(0, 9).trim());
        assertEquals("Released Countries", table.getCellAsText(0, 9).trim());
        assertEquals("Priced Countries", table.getCellAsText(0, 9).trim());

        TableCell tc = table.getTableCell(1, 2);
        String linkText = tc.getLinkWith("tid").asText();
        String tid = linkText.substring(linkText.indexOf("tid=")+4);
        
	PreparedStatement ps = db.prepareStatement(getTitleInfo);
	ps.setString(1, tid);
	try {
	    ResultSet rs = ps.executeQuery();
	    assertTrue(rs.next());
	    assertEquals(rs.getString(3), table.getCellAsText(1, 1).trim());
	    assertEquals(String.valueOf(rs.getInt(5)), table.getCellAsText(1, 2).trim());
	} finally {
	    ps.close();
	}
    }

    public void testSortTitlesList()
	throws IOException, SAXException, SQLException, AssertionFailedError
    {
	WebResponse res = goToPage();
        int flag=0;
        String[] sortBy = new String[] {"cip.c_count", "cip.c_count_d", 
                                        "ctn.b_count", "ctn.b_count_d", 
                                        "ctl.a_count", "ctl.a_count_d", 
					"cim.d_count", "cim.d_count_d", 
                                        "ctc.product_code", "ctc.product_code_d", 
                                        "ctc.publisher", "ctc.publisher_d", 
					"ctc.platform", "ctc.platform_d", 
                                        "ctc.category", "ctc.category_d",
                                        "ctc.approx_size", "ctc.approx_size_d",
                                        "ctc.title_size", "ctc.title_size_d",
                                        "ctc.title", "ctc.title_d" };

        for (int i = 0; i < sortBy.length; i++) 
        {
            WebLink[] links = res.getLinks();
            for (int j = 0; j < links.length; j++) 
            {
                String url = links[j].getRequest().getURL().toString();
                if (url.endsWith(sortBy[i]))
                {
                    res = links[j].click();  
                    flag = 1;
   	            WebTable table = res.getTableStartingWith("No");
 	            TableCell tc = table.getTableCell(1, 2);
                    String linkText = tc.getLinkWith("tid").asText();
                    String tid = linkText.substring(linkText.indexOf("tid=")+4);

	            PreparedStatement ps = db.prepareStatement(getTitleInfo);
 	            ps.setString(1, tid);

 	            try 
                    {
	                ResultSet rs = ps.executeQuery();
	                assertTrue(rs.next());
                        
                        String rsText = rs.getString((int)(sortBy.length/2)-(int)(i/2));
                        String tableText = table.getCellAsText(1, (int)(sortBy.length/2)-(int)(i/2)).trim();

                        if (rsText == null)
                            rsText = "";

	                assertEquals(rsText, tableText);
	            } finally {
	                ps.close();
	            }
                }
            }

            if (flag!=0)
                flag = 0;
            else
                throw new AssertionFailedError("Sort Link Missing for " + sortBy[i]);
        }
    }
    
    public static void main(String[] args)
    {
        if (args != null && args.length > 0)
            hostName = args[0];
        Configuration.testLab1();
        junit.textui.TestRunner.run(TitlesList.class);
    }
}

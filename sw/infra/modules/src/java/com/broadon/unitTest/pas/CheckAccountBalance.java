package com.broadon.unitTest.pas;

import java.rmi.RemoteException;

import com.broadon.wsapi.pas.AuthorizeAccountPaymentRequestType;
import com.broadon.wsapi.pas.AuthorizeAccountPaymentResponseType;
import com.broadon.wsapi.pas.BalanceAccountType;
import com.broadon.wsapi.pas.CaptureECardToAccountRequestType;
import com.broadon.wsapi.pas.CapturePaymentRequestType;
import com.broadon.wsapi.pas.CapturePaymentResponseType;
import com.broadon.wsapi.pas.CheckAccountBalanceRequestType;
import com.broadon.wsapi.pas.CheckAccountBalanceResponseType;
import com.broadon.wsapi.pas.CreateAccountRequestType;
import com.broadon.wsapi.pas.CreateAccountResponseType;
import com.broadon.wsapi.pas.MoneyType;
import com.broadon.wsapi.pas.PasTokenType;
import com.broadon.wsapi.pas.RefundPaymentRequestType;
import com.broadon.wsapi.pas.RefundPaymentResponseType;
import com.broadon.wsapi.pas.VoidAuthorizationRequestType;

public class CheckAccountBalance extends TestCaseWrapper {
    CreateAccountRequestType req;
    CreateAccountResponseType resp;
    BalanceAccountType acct = null;
    PasTokenType token = null;
    MoneyType amount = null;
    public static void main(String[] args) {
        junit.textui.TestRunner.run(CheckAccountBalance.class);
    }

    public CheckAccountBalance(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
        resetTestDataBase(AccountTests);
        createAccount();
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }
    private void createAccount() throws Exception {
        MoneyType amount = new MoneyType("POINTS", "50");
        PasTokenType token = eCardAuth("0010030001", amount);
        acct = new BalanceAccountType();
        acct.setAccountNumber("30001");
        acct.setPIN("30001");  
        req = createCreateAccountRequest(acct);
        resp = pas.createAccount(req);
        if (checkAbstractResponse(req, resp)) {
            CaptureECardToAccountRequestType req = createCaptureECardToAccountRequest(
                    token, acct, amount);
                pas.captureECardToAccount(req);
        }
    }
    private void authorizeAccountPayment() throws Exception {

        amount = new MoneyType("POINTS", "10"); 
        BalanceAccountType acct = new BalanceAccountType();
        acct.setAccountNumber("30001");
        acct.setPIN("30001");
        AuthorizeAccountPaymentRequestType authReq = createAcctAuthorizationRequest(
                acct, amount);
        boolean authReturn=false;
            AuthorizeAccountPaymentResponseType authResp = pas.authorizeAccountPayment(authReq);
            authReturn = checkAbstractResponse(authReq, authResp);
            if (authReturn) {
                token = authResp.getAuthorizationToken();
            }
    }
    
    // teardown
    private void voidAuthorization() throws RemoteException {
        VoidAuthorizationRequestType voidReq = createVoidAuthorizationRequest(token);
        pas.voidAuthorization(voidReq);
    }
    
    // test part1: capture
    private boolean capture() throws RemoteException {
        CapturePaymentResponseType capResp;
        amount.setAmount("8");
        CapturePaymentRequestType capReq = createCapturePaymentRequest(token, amount, false);
        capResp = pas.capturePayment(capReq);
        return checkAbstractResponse(capReq, capResp);
    }
    
    // test part2: refund
    private boolean refund(String refund) throws RemoteException {
        amount.setAmount(refund);
        RefundPaymentRequestType req = createRefundPaymentRequest(token, amount);
        RefundPaymentResponseType resp;
        resp = pas.refundPayment(req);
        return checkAbstractResponse(req, resp);
    }
    public boolean checkAccountBalance() {
        BalanceAccountType acct = new BalanceAccountType();
        acct.setAccountNumber("30001");
        acct.setPIN("30001");
        CheckAccountBalanceRequestType checkBalanceReq = createCheckAcctBalanceRequest(acct);
        CheckAccountBalanceResponseType checkBalanceResp;
        
        boolean testResult=true;

        try {
            checkBalanceResp = pas.checkAccountBalance(checkBalanceReq);
            testResult &= checkAbstractResponse(checkBalanceReq, checkBalanceResp);
            authorizeAccountPayment();
            checkBalanceResp = pas.checkAccountBalance(checkBalanceReq);
            testResult &= checkAbstractResponse(checkBalanceReq, checkBalanceResp);
            capture();
            checkBalanceResp = pas.checkAccountBalance(checkBalanceReq);
            testResult &= checkAbstractResponse(checkBalanceReq, checkBalanceResp);
            refund("8");
            checkBalanceResp = pas.checkAccountBalance(checkBalanceReq);
            testResult &= checkAbstractResponse(checkBalanceReq, checkBalanceResp);
        } catch (Exception e) {
            System.out.println(getStackTrace(e));
            System.out.println(e.getMessage());
            testResult = false;
        }
    
        return testResult;
    }
    
    public void testCheckAccountBalance() {
        assertTrue(checkAccountBalance());
    }  
}

package com.broadon.unitTest.ias;

import com.broadon.wsapi.ias.GetDeviceInfoRequestType;
import com.broadon.wsapi.ias.GetDeviceInfoResponseType;
import com.broadon.wsapi.ias.UpdateDeviceSubscriptionRequestType;
import com.broadon.wsapi.ias.UpdateDeviceSubscriptionResponseType;
import com.broadon.wsapi.ias.DeviceSubscriptionType;

public class UpdateDeviceSubscriptionSuccess extends TestCaseWrapper {

    public static void main(String[] args) {
    }

    public UpdateDeviceSubscriptionSuccess(String name) {
	super(name);
    }

    public void setUp() throws Exception {
	super.setUp();
    }

    public void tearDown() throws Exception {
	super.tearDown();
    }
    
    private Long getSubscriptionLastUpdated (
                                String deviceId,
                                String token,
                                String subType)            
            throws Throwable
    {
        GetDeviceInfoResponseType resp;
        GetDeviceInfoRequestType req = new GetDeviceInfoRequestType();
        
        initRequest(req);
        req.setDeviceId(Long.parseLong(deviceId));
        
        if (token != null && !token.equals(""))
            req.setTokenId(token);
        
        
        resp = ias.getDeviceInfo(req);
                
        DeviceSubscriptionType[] resType = resp.getDeviceSubscriptions();      
        Long lastUpdated = null;
        
        if (resType != null && resType.length > 0) {                        
            for (int i = 0; i < resType.length; i++) {
                if (subType.equals(resType[i].getSubscriptionType())) {
                    lastUpdated = resType[i].getLastUpdated();
                    break;
                }
            }           
        }     
        
        return lastUpdated;
    }
    
    private boolean checkSubscriptionUpdate (
                String deviceId,
                String token,
                DeviceSubscriptionType dType)            
        throws Throwable
    {
        boolean result = true;
        boolean found = false;
        
        GetDeviceInfoResponseType resp;
        GetDeviceInfoRequestType req = new GetDeviceInfoRequestType();
        
        initRequest(req);
        req.setDeviceId(Long.parseLong(deviceId));
        
        if (token != null && !token.equals(""))
            req.setTokenId(token);        
        
        resp = ias.getDeviceInfo(req);
        result &= (resp.getErrorCode() == 000);
        
        if (result) {
            DeviceSubscriptionType[] resType = resp.getDeviceSubscriptions();
            
            if (resType != null && resType.length > 0) {                        
                for (int i = 0; i < resType.length; i++) {
                    
                    if (dType.getSubscriptionType().equals(resType[i].getSubscriptionType())) {
                        found = true;
                        result &= 
                            (dType.getStartDate() == resType[i].getStartDate()) &&
                            (dType.getExpirationDate() == resType[i].getExpirationDate()) &&
                            (dType.getReferenceId().equals(resType[i].getReferenceId()));
                        break;
                    }                    
                }           
            }            
        }
        
        return (result && found);
    }
    
    private boolean checkUpdateDeviceSubscriptionSuccess (
		String deviceId,
                String token,
                DeviceSubscriptionType dType,
                boolean update,
                UpdateDeviceSubscriptionResponseType expected)
	throws Throwable
    {
	boolean testResult = true;

        UpdateDeviceSubscriptionResponseType resp;
        UpdateDeviceSubscriptionRequestType req = new UpdateDeviceSubscriptionRequestType();
        
        initRequest(req);
	req.setDeviceId(Long.parseLong(deviceId));
        req.setDeviceSubscription(dType);

	resp = ias.updateDeviceSubscription(req);
	testResult &= checkDeviceResponse(req, resp);
	
	// Check response against expected response
	testResult &= checkError(expected, resp);
        testResult &= checkSubscriptionUpdate(deviceId, token, dType);
        
        if (testResult && !update)
            TestDataAdmin.setDeviceSubscription(deviceId, dType.getSubscriptionType());
        
	return testResult;
    }

    public void testUpdateDeviceSubscriptionSuccess()
	throws Throwable
    {
        // Case 1: Device with valid token and with a subscription
        String deviceId = TestDataAdmin.getDeviceIdWithValidTokenAndSubscription(true);
        
        if (deviceId != null) {
            String token = TestDataAdmin.getValidDeviceToken(deviceId);
            String subs[] = TestDataAdmin.getDeviceSubscriptions(deviceId);
            
            DeviceSubscriptionType dType = new DeviceSubscriptionType();
            dType.setSubscriptionType(subs[0]);
            dType.setStartDate(((long)(System.currentTimeMillis()/1000))*1000);
            dType.setExpirationDate(((long)((System.currentTimeMillis()+(30*24*60*60*1000))/1000))*1000);
            dType.setLastUpdated(getSubscriptionLastUpdated(deviceId, token, subs[0]));
            dType.setReferenceId("Success Test 1");
            
            //System.out.println("1. deviceId: " + deviceId +", token: " + token + ", subType: " + subs[0]);
            
            UpdateDeviceSubscriptionResponseType expected = new UpdateDeviceSubscriptionResponseType();
            expected.setDeviceId(Long.parseLong(deviceId));
            expected.setErrorCode(000);
            expected.setErrorMessage("");
            assertTrue(checkUpdateDeviceSubscriptionSuccess(deviceId, token, dType, true, expected));
            
        } else {
            System.out.println(getName() + " Warning: Failed to obtain a device id with valid token and a subscription");
        }
        
        // Case 2: Device with valid token and no subscription
        deviceId = TestDataAdmin.getDeviceIdWithValidTokenAndSubscription(false);
        
        if (deviceId != null) {
            String token = TestDataAdmin.getValidDeviceToken(deviceId);
            String subType = "test005";
            
            DeviceSubscriptionType dType = new DeviceSubscriptionType();
            dType.setSubscriptionType(subType);
            dType.setStartDate(((long)(System.currentTimeMillis()/1000))*1000);
            dType.setExpirationDate(((long)((System.currentTimeMillis()+(30*24*60*60*1000))/1000))*1000);
            dType.setLastUpdated(getSubscriptionLastUpdated(deviceId, token, subType));
            dType.setReferenceId("Success Test 2");
            
            //System.out.println("2. deviceId: " + deviceId +", token: " + token + ", subType: " + subType);
            
            UpdateDeviceSubscriptionResponseType expected = new UpdateDeviceSubscriptionResponseType();
            expected.setDeviceId(Long.parseLong(deviceId));
            expected.setErrorCode(000);
            expected.setErrorMessage("");
            assertTrue(checkUpdateDeviceSubscriptionSuccess(deviceId, token, dType, false, expected));
            
        } else {
            System.out.println(getName() + " Warning: Failed to obtain a device id with valid token and no subscription");
        }

        // Case 3: Device with expired token and a subscription
        deviceId = TestDataAdmin.getDeviceIdWithExpiredTokenAndSubscription(true);
        
        if (deviceId != null) {
            String[] subs = TestDataAdmin.getDeviceSubscriptions(deviceId);

            DeviceSubscriptionType dType = new DeviceSubscriptionType();
            dType.setSubscriptionType(subs[0]);
            dType.setStartDate(((long)(System.currentTimeMillis()/1000))*1000);
            dType.setExpirationDate(((long)((System.currentTimeMillis()+(30*24*60*60*1000))/1000))*1000);
            dType.setLastUpdated(getSubscriptionLastUpdated(deviceId, null, subs[0]));
            dType.setReferenceId("Success Test 3");
            
            //System.out.println("3. deviceId: " + deviceId +", token: expired, subType: " + subs[0]);
            
            UpdateDeviceSubscriptionResponseType expected = new UpdateDeviceSubscriptionResponseType();
            expected.setDeviceId(Long.parseLong(deviceId));
            expected.setErrorCode(000);
            expected.setErrorMessage("");
            assertTrue(checkUpdateDeviceSubscriptionSuccess(deviceId, null, dType, true, expected));
            
        } else {
            System.out.println(getName() + " Warning: Failed to obtain a device id with expired token and a subscription");
        }
        
        // Case 4: Device with expired token and no subscription
        deviceId = TestDataAdmin.getDeviceIdWithExpiredTokenAndSubscription(false);
        
        if (deviceId != null) {
            String subType = "test004";
            
            DeviceSubscriptionType dType = new DeviceSubscriptionType();
            dType.setSubscriptionType(subType);
            dType.setStartDate(((long)(System.currentTimeMillis()/1000))*1000);
            dType.setExpirationDate(((long)((System.currentTimeMillis()+(30*24*60*60*1000))/1000))*1000);
            dType.setLastUpdated(getSubscriptionLastUpdated(deviceId, null, subType));
            dType.setReferenceId("Success Test 4");
            
            //System.out.println("4. deviceId: " + deviceId +", token: expired, subType: " + subType);
            
            UpdateDeviceSubscriptionResponseType expected = new UpdateDeviceSubscriptionResponseType();
            expected.setDeviceId(Long.parseLong(deviceId));
            expected.setErrorCode(000);
            expected.setErrorMessage("");
            assertTrue(checkUpdateDeviceSubscriptionSuccess(deviceId, null, dType, false, expected));
            
        } else {
            System.out.println(getName() + " Warning: Failed to obtain a device id with expired token and no subscription");
        }
    }    
}

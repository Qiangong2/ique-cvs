package com.broadon.unitTest.pas;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.math.BigInteger;
import java.rmi.RemoteException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import com.broadon.status.StatusCode;
import com.broadon.unitTest.DBConnection;
import com.broadon.unitTest.ResourcePool;
import com.broadon.unitTest.Configuration;
import com.broadon.unitTest.TestLog;
//import com.broadon.wsapi.ets.AbstractTransactionType;
import com.broadon.wsapi.pas.*;
import com.broadon.wsapi.pcis.PrepaidCardGenerationPortType;
import com.broadon.wsapi.pcis.PrepaidCardGenerationRequestType;
import com.broadon.wsapi.pcis.PrepaidCardGenerationResponseType;

public class TestCaseWrapper extends TestCase {
    protected PaymentAuthorizationPortType pas = null;
    static protected Map typeMap = new HashMap();
    static public Map hashMap = new HashMap();
    static protected Map countryMap = new HashMap();
    static protected Map countryCode = new HashMap();

    static public final int ECardTests = 1;
    static public final int AccountTests = 2;
    static public final int CreditCardTests = 3;
    
    static {
        countryMap.put("CN", new Integer(0));
        countryMap.put("JPT", new Integer(1));
        countryMap.put("UST", new Integer(2));
        countryMap.put("JP", new Integer(100));
        countryMap.put("AU", new Integer(101));
        countryMap.put("NZ", new Integer(102));
        countryMap.put("US", new Integer(200));
        countryMap.put("CA", new Integer(201));
        countryMap.put("MX", new Integer(202));
        countryMap.put("BR", new Integer(203));
        countryMap.put("UK", new Integer(300));
        countryMap.put("FR", new Integer(301));
        countryMap.put("DE", new Integer(302));
        countryMap.put("GR", new Integer(303));
        countryMap.put("SP", new Integer(304));
        countryMap.put("KR", new Integer(305));
        countryMap.put("AQ", new Integer(306));
        countryMap.put("CN", new Integer(0));
        /* ten accounts from batch03 of currency "days" for ecard testing
        000101 0010000000 0014264647      10000000
        000101 0010000001 0065066807      10000001
        000101 0010000002 0091798645      10000002
        000101 0010000003 0018185098      10000003
        000101 0010000004 0097222208      10000004
        000101 0010000005 0083326699      10000005
        000101 0010000006 0012120508      10000006
        000101 0010000007 0002641842      10000007
        000101 0010000008 0006486755      10000008
        000101 0010000009 0015357451      10000009
        */   
        String eCardType = "000101";
        String countryCode = "CN";
        String eCardSequence;
        String eCardHash;
        eCardSequence = "0010000000";
        eCardHash = "61af8c1ec665b90e8eb3654c1ad7cee2";
        typeMap.put(eCardSequence, eCardType);
        hashMap.put(eCardSequence, eCardHash);
        countryMap.put(eCardSequence, countryCode);
        eCardSequence = "0010000001";
        eCardHash = "ed37a9d1be43ddcacb68b58b8ed8333c";
        typeMap.put(eCardSequence, eCardType);
        hashMap.put(eCardSequence, eCardHash);
        countryMap.put(eCardSequence, countryCode);
        eCardSequence = "0010000002";
        eCardHash = "4221fbcbaa69eb687bcf7a75f89d6a47";
        typeMap.put(eCardSequence, eCardType);
        hashMap.put(eCardSequence, eCardHash);
        countryMap.put(eCardSequence, countryCode);
        eCardSequence = "0010000003";
        eCardHash = "f4f69765168495588a4cad989c8b0e73";
        typeMap.put(eCardSequence, eCardType);
        hashMap.put(eCardSequence, eCardHash);
        countryMap.put(eCardSequence, countryCode);
        eCardSequence = "0010000004";
        eCardHash = "dbb6967eec7ae9c8ae0e971549158e94";
        typeMap.put(eCardSequence, eCardType);
        hashMap.put(eCardSequence, eCardHash);
        countryMap.put(eCardSequence, countryCode);
        eCardSequence = "0010000005";
        eCardHash = "e21fc6a755ecce55f03b7d177a069286";
        typeMap.put(eCardSequence, eCardType);
        hashMap.put(eCardSequence, eCardHash);
        countryMap.put(eCardSequence, countryCode);
        eCardSequence = "0010000006";
        eCardHash = "925189817bf8ed404492395ad445cdc";
        typeMap.put(eCardSequence, eCardType);
        hashMap.put(eCardSequence, eCardHash);
        countryMap.put(eCardSequence, countryCode);
        eCardSequence = "0010000007";
        eCardHash = "75f70632009ad93f6f8bb52ee74da92b";
        typeMap.put(eCardSequence, eCardType);
        hashMap.put(eCardSequence, eCardHash);
        countryMap.put(eCardSequence, countryCode);
        eCardSequence = "0010000008";
        eCardHash = "255b1cf2c088599cfa1a063f344d78e3";
        typeMap.put(eCardSequence, eCardType);
        hashMap.put(eCardSequence, eCardHash);
        countryMap.put(eCardSequence, countryCode);
        eCardSequence = "0010000009";
        eCardHash = "596ace4a029d45f816016700525667c0";
        typeMap.put(eCardSequence, eCardType);
        hashMap.put(eCardSequence, eCardHash);
        countryMap.put(eCardSequence, countryCode);
        /* 10 accounts of type 10 (currency "POINTS" for redeem ecards
         * 000010   0010030001  0033152003      10030001
         * 000010   0010030002  0005289756      10030002
         * 000010   0010030003  0036440066      10030003
         * 000010   0010030004  0063586311      10030004
         * 000010   0010030005  0044069818      10030005
         * 000010   0010030006  0015551517      10030006
         * 000010   0010030007  0060304051      10030007
         * 000010   0010030008  0044776645      10030008
         * 000010   0010030009  0045674886      10030009
         * 000010   0010030010  0003972240      10030010
         */
        eCardType = "000010";
        eCardSequence = "0010030001";
        countryCode="JP";
        eCardHash = "3230162f5eb0356308d41540c1c39c74";
        typeMap.put(eCardSequence, eCardType);
        hashMap.put(eCardSequence, eCardHash);
        countryMap.put(eCardSequence, countryCode);
        eCardSequence = "0010030002";
        eCardHash = "a264bc51d918845a80e88b4034ba15d6";
        typeMap.put(eCardSequence, eCardType);
        hashMap.put(eCardSequence, eCardHash);
        countryMap.put(eCardSequence, countryCode);
        eCardSequence = "0010030003";
        eCardHash = "af8cf0776551e1ac9f9a778277b37588";
        typeMap.put(eCardSequence, eCardType);
        hashMap.put(eCardSequence, eCardHash);
        countryMap.put(eCardSequence, countryCode);
        eCardSequence = "0010030004";
        eCardHash = "454379b0ea733f31fe9919571af163d1";
        typeMap.put(eCardSequence, eCardType);
        hashMap.put(eCardSequence, eCardHash);
        countryMap.put(eCardSequence, countryCode);
        eCardSequence = "0010030005";
        eCardHash = "5064b5fa0b970f7251b941eb5c58beb8";
        typeMap.put(eCardSequence, eCardType);
        hashMap.put(eCardSequence, eCardHash);
        countryMap.put(eCardSequence, countryCode);
        eCardSequence = "0010030006";
        eCardHash = "11e2adc91fd3d696227db6a15ec02723";
        typeMap.put(eCardSequence, eCardType);
        hashMap.put(eCardSequence, eCardHash);
        countryMap.put(eCardSequence, countryCode);
        eCardSequence = "0010030007";
        eCardHash = "2c48a1df9ed53f50b080e826a22e3993";
        typeMap.put(eCardSequence, eCardType);
        hashMap.put(eCardSequence, eCardHash);
        countryMap.put(eCardSequence, countryCode);
        eCardSequence = "0010030008";
        eCardHash = "6ff1da4daf018c663ec9918227a602e7";
        typeMap.put(eCardSequence, eCardType);
        hashMap.put(eCardSequence, eCardHash);
        countryMap.put(eCardSequence, countryCode);
        eCardSequence = "0010030009";
        eCardHash = "3666380a680e17a82a6f9a36768b464c";
        typeMap.put(eCardSequence, eCardType);
        hashMap.put(eCardSequence, eCardHash);
        countryMap.put(eCardSequence, countryCode);
        eCardSequence = "0010030010";
        eCardHash = "5234b91fd0f1300699d6ee36bf151261";
        typeMap.put(eCardSequence, eCardType);
        hashMap.put(eCardSequence, eCardHash);
        countryMap.put(eCardSequence, countryCode);
    }
    protected boolean tracing = false;
    
    public TestCaseWrapper(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
        if (pas == null) {
            pas = ResourcePool.newPaymentAuthorizationService();
        }
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    protected void runTest() throws Throwable 
    {
        boolean testResult = true;
        try {
            super.runTest();
        } catch (Throwable e) {
            System.out.println(getName() + " error: " + e.getMessage());
            if (tracing) {
                e.printStackTrace();
            }
            testResult = false;
            throw e;
        } finally {
            TestLog.report("PAS", getName(), testResult);
        }
    }

    protected void initAbstractRequest(AbstractRequestType req) {
        req.setVersion("1.0");
        req.setMessageID(Long.toString(System.currentTimeMillis()));
    }
    
    protected boolean checkAbstractResponse(AbstractRequestType req,
            AbstractResponseType resp) {
        if (resp.getErrorCode() == Integer.parseInt(StatusCode.SC_OK))
            return true;
        else
            return false;
       
    }
    protected AuthorizeCreditCardPaymentRequestType createCreditCardAuthorizationRequest(
            MoneyType amount,
            String creditCardNumber,
            String NOAtx) {
        AuthorizeCreditCardPaymentRequestType authReq = new AuthorizeCreditCardPaymentRequestType();
        authReq.setAmount(amount);
        authReq.setCreditCardNumber(creditCardNumber);
        authReq.setNOACreditCardReq(NOAtx);
        initAbstractRequest(authReq);
        return authReq;
    }
    
    protected AuthorizeAccountPaymentRequestType createAcctAuthorizationRequest(
            BalanceAccountType acct, MoneyType amount) {
        AuthorizeAccountPaymentRequestType authReq = new AuthorizeAccountPaymentRequestType();
        authReq.setAmount(amount);
        authReq.setBalanceAccount(acct);
        authReq.setAmount(amount);
        initAbstractRequest(authReq);
        return authReq;
    }
    
    protected AuthorizeECardPaymentRequestType createAuthorizationRequest(
            String eCardSequence, MoneyType amount) {
        AuthorizeECardPaymentRequestType authReq = new AuthorizeECardPaymentRequestType();
        authReq.setECardID(setECardString(eCardSequence));
        authReq.setAmount(amount);
        initAbstractRequest(authReq);
        return authReq;
    }
    
    protected CapturePaymentRequestType createCapturePaymentRequest(
            PasTokenType token, MoneyType amount, boolean isFinal) {
        CapturePaymentRequestType capReq = new CapturePaymentRequestType();
        capReq.setAuthorizationToken(token);
        capReq.setAmount(amount);
        capReq.setIsFinal(isFinal);
        initAbstractRequest(capReq);
        return capReq;
    }
    
    protected CaptureCreditCardToAccountRequestType createCaptureCreditCardToAccountRequest(
            PasTokenType token, BalanceAccountType acct, MoneyType captureAmount, MoneyType depositAmount) {
        CaptureCreditCardToAccountRequestType capReq = new CaptureCreditCardToAccountRequestType();
        capReq.setAuthorizationToken(token);
        capReq.setAmount(captureAmount);
        capReq.setToBalanceAccount(acct);
        capReq.setDepositAmount(depositAmount);
        capReq.setIsFinal(true);
        initAbstractRequest(capReq);
        return capReq;
    }
    
    protected CaptureECardToAccountRequestType createCaptureECardToAccountRequest(
            PasTokenType token, BalanceAccountType acct, MoneyType amount) {
        CaptureECardToAccountRequestType capReq = new CaptureECardToAccountRequestType();
        capReq.setAuthorizationToken(token);
        capReq.setAmount(amount);
        capReq.setToBalanceAccount(acct);
        initAbstractRequest(capReq);
        return capReq;
    }
    
    protected CaptureAccountToAccountRequestType createCaptureAccountToAccountRequest(
            PasTokenType token, BalanceAccountType acct, MoneyType amount) {
        CaptureAccountToAccountRequestType capReq = new CaptureAccountToAccountRequestType();
        capReq.setAuthorizationToken(token);
        capReq.setAmount(amount);
        capReq.setToBalanceAccount(acct);
        initAbstractRequest(capReq);
        return capReq;
    }
    protected CheckECardBalanceRequestType createCheckBalanceRequest(String eCardSequence) {
        CheckECardBalanceRequestType req = new CheckECardBalanceRequestType();
        req.setECardID(setECardString(eCardSequence));
        initAbstractRequest(req);
        return req;
    }
    
    protected CheckAccountBalanceRequestType createCheckAcctBalanceRequest(BalanceAccountType acct) {
        CheckAccountBalanceRequestType req = new CheckAccountBalanceRequestType();
        req.setBalanceAccount(acct);
        initAbstractRequest(req);
        return req;
    }
    
    protected CreateAccountRequestType createCreateAccountRequest(
            BalanceAccountType acct) {
        CreateAccountRequestType req = new CreateAccountRequestType();
        req.setBalanceAccount(acct);
        initAbstractRequest(req);
        return req;
    }
    
    protected ResetAccountPINRequestType createResetAccountPINRequest(
            BalanceAccountType acct) {
        ResetAccountPINRequestType req = new ResetAccountPINRequestType();
        req.setBalanceAccount(acct);
        initAbstractRequest(req);
        return req;
    }
    
    protected DisableAccountRequestType createDisableAccountRequest(
            BalanceAccountType acct) {
        DisableAccountRequestType req = new DisableAccountRequestType();
        req.setBalanceAccount(acct);
        initAbstractRequest(req);
        return req;
    }

    protected GetAuthorizationHistoryRequestType createGetAuthorizationHistoryRequest(PasTokenType token) {
        GetAuthorizationHistoryRequestType getReq = new GetAuthorizationHistoryRequestType();
        getReq.setAuthorizationToken(token);
        initAbstractRequest(getReq);
        return getReq;
    }

    protected GetECardHistoryRequestType createGetECardHistoryRequest(String eCardSequence) {
        GetECardHistoryRequestType acctReq = new GetECardHistoryRequestType();
        acctReq.setECardID(setECardString(eCardSequence));
        initAbstractRequest(acctReq);
        return acctReq;
    }
    
    protected GetECardTypeRequestType createGetECardTypeRequest(String eCardSequence) {
        GetECardTypeRequestType acctReq = new GetECardTypeRequestType();
        acctReq.setECardNumber(eCardSequence);
        initAbstractRequest(acctReq);
        return acctReq;
    }
    
    protected CheckECardBalanceRequestType createCheckECardBalanceRequest(String eCardSequence) {
        CheckECardBalanceRequestType req = new CheckECardBalanceRequestType();
        req.setECardID(setECardString(eCardSequence));
        initAbstractRequest(req);
        return req;
    }
    
    protected GetAccountHistoryRequestType createGetAccountHistoryRequest(BalanceAccountType acct) {
        GetAccountHistoryRequestType acctReq = new GetAccountHistoryRequestType();
        acctReq.setBalanceAccount(acct);
        initAbstractRequest(acctReq);
        return acctReq;
    }


    protected RefundPaymentRequestType createRefundPaymentRequest(PasTokenType token, MoneyType amount) {
        RefundPaymentRequestType req = new RefundPaymentRequestType();
        req.setAmount(amount);
        req.setAuthorizationToken(token);
        initAbstractRequest(req);
        return req;
    }
    
    protected VoidAuthorizationRequestType createVoidAuthorizationRequest(PasTokenType token) {
        VoidAuthorizationRequestType voidReq = new VoidAuthorizationRequestType();
        voidReq.setAuthorizationToken(token);
        initAbstractRequest(voidReq);
        return voidReq;
    }
    
    protected PasTokenType eCardAuth(String eCardId, MoneyType amount) throws RemoteException {
        AuthorizeECardPaymentRequestType authReq = createAuthorizationRequest(
                        eCardId, amount);
        AuthorizeECardPaymentResponseType authResp;
        authResp = pas.authorizeECardPayment(authReq);
        PasTokenType token = authResp.getAuthorizationToken();
        return token;
    }
    
    protected ECardRecordType setECardString(String eCardNumber) {
        return new ECardRecordType((String)typeMap.get(eCardNumber),
                eCardNumber,
                (String)hashMap.get(eCardNumber),
                (String)countryMap.get(eCardNumber)
                );
    }
    
    public static String getStackTrace(Throwable aThrowable) {
        final Writer result = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(result);
        aThrowable.printStackTrace(printWriter);
        return result.toString();
    }
    
    /*
     * reste test date 10000000 to 10000009
     * this method is not used by Pas unit tests any more
     * but is available for other modules
     * to invoke to reset ecard test data.
     */
    public static void resetTestData() {
        // DB Parameters
        String db_user = "pas";
        String db_pswd = "pas";
        String db_url = Configuration.getDbUrl();
        
        String update = "update ecards set last_used=null, balance='101', balance_authorized='0' where ecard_id"
            + " between 10000000 and 10000009";
        Connection conn = null;
        PreparedStatement pstmt = null;
        try {
            DBConnection db = new DBConnection(db_user, db_pswd, db_url);
            conn = db.getConnection();
            pstmt = conn.prepareStatement(update);
            pstmt.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (conn != null)
                    conn.close();
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    
    public static void resetECard(int eCardId, int balance) {
        // DB Parameters
        String db_user = "pas";
        String db_pswd = "pas";
        String db_url = Configuration.getDbUrl();
        
        String update = "update ecards set last_used=null, balance_authorized='0', balance="
            + balance + " where ecard_id=" + eCardId;
        Connection conn = null;
        PreparedStatement pstmt = null;
        try {
            DBConnection db = new DBConnection(db_user, db_pswd, db_url);
            conn = db.getConnection();
            pstmt = conn.prepareStatement(update);
            pstmt.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (conn != null)
                    conn.close();
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    
    public static void resetECards(int startECardId, int endECardId, int balance) {
        // DB Parameters
        String db_user = "pas";
        String db_pswd = "pas";
        String db_url = Configuration.getDbUrl();
        
        String update = "update ecards set last_used=null, balance_authorized='0', balance="
            + balance + " where ecard_id between " + startECardId + " and " + endECardId;
        Connection conn = null;
        PreparedStatement pstmt = null;
        try {
            DBConnection db = new DBConnection(db_user, db_pswd, db_url);
            conn = db.getConnection();
            pstmt = conn.prepareStatement(update);
            pstmt.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (conn != null)
                    conn.close();
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /*
     * reste test date based on TestGroup
     */
    public void resetTestDataBase(int testGroup) {
        // DB Parameters
        String db_user = "pas";
        String db_pswd = "pas";
        String db_url = Configuration.getDbUrl();
        
        String resetECards1 = "update ecards set last_used=null, balance='101', balance_authorized='0' where ecard_id"
            + " between 10000000 and 10000009";
        String resetAccounts = " delete from pas_bank_accounts where ACCOUNT_ID "
            + " between 30000 and 30005";
        String resetECards2 = "update ecards set last_used=null, balance='1000', balance_authorized='0' where ecard_id"
            + " between 10030001 and 10030005";
        String resetCreditCards = "";
        String update = null;
        Connection conn = null;
        PreparedStatement pstmt = null;
        switch (testGroup) {
            case ECardTests: 
                update = resetECards1;
                break;
            case AccountTests: 
                update = resetAccounts;
                break;
            case CreditCardTests: 
                update = resetCreditCards;
                break;        
        }
        try {
            DBConnection db = new DBConnection(db_user, db_pswd, db_url);
            conn = db.getConnection();
            pstmt = conn.prepareStatement(update);
            pstmt.executeUpdate();
            if (testGroup == AccountTests) {
                pstmt = conn.prepareStatement(resetECards2);
                pstmt.executeUpdate();
            }
            conn.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (conn != null)
                    conn.close();
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    
    public BalanceAccountType createAccount(String acctNumber, String pin) throws RemoteException {
        BalanceAccountType acct = new BalanceAccountType();
        acct.setAccountNumber(acctNumber);
        acct.setPIN(pin); 
        CreateAccountRequestType req = createCreateAccountRequest(acct);
        CreateAccountResponseType resp = pas.createAccount(req);
        if (checkAbstractResponse(req, resp))
            return acct;
        else
            return null;
    }
    
    public BalanceType checkAccountBalance(BalanceAccountType acct) throws RemoteException {
        CheckAccountBalanceRequestType acctBalanceReq = createCheckAcctBalanceRequest(acct);
        CheckAccountBalanceResponseType acctBalanceResp = pas.checkAccountBalance(acctBalanceReq);
        if (checkAbstractResponse(acctBalanceReq, acctBalanceResp)) {
            return acctBalanceResp.getBalanceAccountInfo().getBalance();
        } else {
            return null;
        }
    }
    
    public BalanceType checkECardBalance(String eCardNumber) throws RemoteException {
        CheckECardBalanceRequestType eCardBalanceReq = createCheckECardBalanceRequest(eCardNumber);
        CheckECardBalanceResponseType eCardBalanceResp = pas.checkECardBalance(eCardBalanceReq);
        if (checkAbstractResponse(eCardBalanceReq, eCardBalanceResp)) {
            return eCardBalanceResp.getECardInfo().getBalance();
        } else {
            return null;
        }
    }
    
    /**
     * create ECards. The ecrads are created using operatorId=555 and 
     * with Attr01="testing". A record in the ecard_counries are also created
     * for the conutryCode. After the ecards are created, the hash code for the ecards are
     * calculated and stored in the cache to prepare the ecard authorization
     * requests.
     * NOTE: since the ecard numbers will be inscreasing and not reclaimed, num 
     * should be restricted to fairly small. Also, avoid using loop to create 
     * a large number of ecards at one time.
     * 
     * @param eCardType the created ecard type
     * @param num  number of ecards created
     * @param country the country where the ecrads can be used.
     * 
     * @return the created ecards
     * @throws RemoteException
     * @throws UnsupportedEncodingException 
     * @throws NoSuchAlgorithmException 
     * @throws InterruptedException 
     */
    public static String[] createECards(int eCardType, int num, String country) 
        throws RemoteException, NoSuchAlgorithmException, UnsupportedEncodingException, InterruptedException 
        {
        // 1. create ecards, eCardString is the set of 16-digit strings created
        String eCardStrings[] = generateECards(eCardType, num);
        // beginning and ending 8-digit eCard numbers of this batch
        int beginNumber = getBeginNumber(eCardStrings);
        int endNumber = getEndNumber(eCardStrings);
        Connection conn = getBMSConnection();
        // 2. insert into the ecard_countries table
        if (!insertCountryCode(conn, beginNumber, endNumber, ((Integer)countryMap.get(country)).intValue()))
            return null;
        if (!activateECards(conn, beginNumber, endNumber))
            return null;
        // 3. calculate the hash and store for later use.
        insertHash(eCardType, country, eCardStrings);
        String eCardNumbers[] = new String[eCardStrings.length];
        for (int i=0; i<eCardStrings.length; i++)
            eCardNumbers[i]= "00" + eCardStrings[i].substring(0, 8);
        return eCardNumbers;
    }
    
    private static String[] generateECards(int eCardType, int num) throws RemoteException, InterruptedException {
        PrepaidCardGenerationPortType pcis = ResourcePool.newPrepaidCardGenerationService();
        PrepaidCardGenerationRequestType req = new PrepaidCardGenerationRequestType();
        req.setVersion("1.0");
        req.setMessageID(Long.toString(System.currentTimeMillis()));
        
        req.setECardType(eCardType);
        req.setOperatorId(555);
        req.setPassword("555");
        req.setQuantity(num);
        req.setReferenceId1("testing");
        int repeat = 3;
        PrepaidCardGenerationResponseType resp = null;
        for (int i=0; i<repeat; i++) {
            long start = System.currentTimeMillis();
            resp = pcis.prepaidCardGeneration(req);
            long finish = System.currentTimeMillis();
            System.out.println("time used: " + (finish-start) + " msec");
            if (resp.getErrorCode() == 1297) {  // process busy, retry
                Thread.sleep(5000);
            } else {
                break;
            }
        }
        String[] ecards = resp.getPrepaidCardString();
//        for (int i=0; i<ecards.length; i++)
//            System.out.println("generateECards: "+ ecards[i]);
        return ecards;
    }
    
    private static Connection getBMSConnection() {
//      DB Parameters
        String db_user = "bms";
        String db_pswd = "bms";
        String db_url = Configuration.getDbUrl();
        Connection conn = null;
        try {
            DBConnection db = new DBConnection(db_user, db_pswd, db_url);
            conn = db.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }
    
    private static boolean insertCountryCode(Connection conn, int beginNum, int endNum, int countryCode) {

        PreparedStatement pstmt = null;
        String insert = "insert into ecard_countries values(null, null, ?, ?, ?)";
        boolean retCode = false;
        try {
            pstmt = conn.prepareStatement(insert);
            pstmt.setInt(1, endNum);
            pstmt.setInt(2, beginNum);
            pstmt.setInt(3, countryCode);
            pstmt.executeUpdate();
            conn.commit();
            retCode = true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return retCode;
    }
    
    private static boolean activateECards(Connection conn, int beginNum, int endNum) {

        PreparedStatement pstmt = null;
        String update = "update ecards set ACTIVATE_DATE = SYS_EXTRACT_UTC(Current_Timestamp)"
            + " where ecard_id between " + beginNum + " and " + endNum;
        boolean retCode = false;
        try {
            pstmt = conn.prepareStatement(update);
            pstmt.executeUpdate();
            conn.commit();
            retCode = true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return retCode;
    }
    
    private static int getECardNumber(String eCardString) {
        return Integer.parseInt(eCardString.substring(0, 8));
    }
    
    private static int getBeginNumber(String[] eCards) {
        return getECardNumber(eCards[0]);
    }
    
    private static int getEndNumber(String[] eCards) {
        return getECardNumber(eCards[eCards.length-1]);
    }
    
    private static void insertHash(int eCardType, String country, String[] eCardStrings) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        // convert eCardType to 6 digit and extract random numbers from last 8 digit in eCardString
        String eCardTypeString = eCardTypePadding(String.valueOf(eCardType));   // padding to 6 digits
        MessageDigest md = MessageDigest.getInstance("MD5");
        for (int i=0; i< eCardStrings.length; i++) {
            String random = "00" + eCardStrings[i].substring(8);    // padding 2 0s in front
            String eCardNumber = "00" + eCardStrings[i].substring(0, 8);    // padding 2 0s in front
            byte[] randomBytes = random.getBytes("US-ASCII");
            byte[] digest = md.digest(randomBytes);
            String hex = digestPadding(new BigInteger(1, digest).toString(16));
            typeMap.put(eCardNumber, eCardTypeString);
            hashMap.put(eCardNumber, hex);
            countryMap.put(eCardNumber, country);
            System.out.println("insertHash: " + eCardNumber + ", random=" + random + ", hex= " + hex);
        }
    }
    
    private static String eCardTypePadding(String orig) {
        StringBuffer buf =
            new StringBuffer("000000");
        merge(0, 6, orig, buf);
        return buf.toString();
    }
    
    private static String digestPadding(String orig) {
        StringBuffer buf =
            new StringBuffer("00000000000000000000000000000000");
        merge(0, 32, orig, buf);
        return buf.toString();
    }
    
    private static void merge(int offset, int size, String s, StringBuffer buf) {
        int len = s.length();
        offset += size - len;
        buf.replace(offset, offset + len, s);
    }

    public static void createECardType(int ecardType) {
        String delete = "delete from ecard_types where ECARD_TYPE = " + ecardType;
        String insert = "insert into ecard_types values("+ecardType+", 1000, 'POINTS', 0, 1, 0, '28-JUL-06', '1000 Virtual Console Prepaid Points Card', 0)";

        Connection conn = getBMSConnection();
        PreparedStatement pstmt = null;
        try {
            pstmt = conn.prepareStatement(delete);
            pstmt.executeUpdate();
            pstmt = conn.prepareStatement(insert);
            pstmt.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (conn != null)
                    conn.close();
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void removeECardType(int ecardType) {
        String sql = "delete from ecard_types where ECARD_TYPE="+ecardType;
        Connection conn = getBMSConnection();
        PreparedStatement pstmt = null;
        try {
            pstmt = conn.prepareStatement(sql);
            pstmt.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (conn != null)
                    conn.close();
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}

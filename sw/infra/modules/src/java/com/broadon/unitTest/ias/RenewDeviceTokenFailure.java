package com.broadon.unitTest.ias;

import org.apache.axis.AxisFault;

import com.broadon.wsapi.ias.RenewDeviceTokenRequestType;
import com.broadon.wsapi.ias.RenewDeviceTokenResponseType;

public class RenewDeviceTokenFailure extends TestCaseWrapper {

    public static void main(String[] args) {
    }

    public RenewDeviceTokenFailure(String name) {
	super(name);
    }

    public void setUp() throws Exception {
	super.setUp();
    }

    public void tearDown() throws Exception {
	super.tearDown();
    }

    private boolean checkRenewDeviceTokenException(
		String deviceId,
                String deviceToken)
	throws Throwable
    {
	boolean testResult = true;

        RenewDeviceTokenRequestType req = new RenewDeviceTokenRequestType();

        initRequest(req);
	req.setDeviceId(Long.parseLong(deviceId));
        req.setTokenId(deviceToken);
	
        try {
            RenewDeviceTokenResponseType resp = ias.renewDeviceToken(req);
            
            if (resp.getTokenId() != null && !deviceToken.equals(resp.getTokenId())) {
                TestDataAdmin.setExpiredDeviceToken(deviceId, deviceToken);
                TestDataAdmin.setValidDeviceToken(deviceId, resp.getTokenId());
            }
            
        } catch (AxisFault a) {
            testResult &= a.getFaultString() != null &&
                a.getFaultString().equals(UNAUTHORIZED_ACCESS_MESSAGE);          
        } 
        
        return testResult;
    }

    public void testRenewDeviceTokenFailure()
	throws Throwable
    {
        Token et = TestDataAdmin.getExpiredDeviceToken();
        
        if (et != null) {
            // Case 1: Renew a token id that has expired
            assertTrue(checkRenewDeviceTokenException(et.id, et.tokenId));
            
            // Case 2: Renew a non-existing token id
            assertTrue(checkRenewDeviceTokenException(et.id, "123456")); 
            
        } else {
            System.out.println(getName() + " Warning: Failed to obtain an expired device token");
        }
    }
}

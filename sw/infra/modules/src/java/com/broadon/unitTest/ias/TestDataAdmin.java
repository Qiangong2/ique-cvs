package com.broadon.unitTest.ias;

import java.io.IOException;
import java.sql.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.broadon.util.UUID;
import com.broadon.unitTest.DBConnection;
import com.broadon.unitTest.Configuration;

public class TestDataAdmin
{
    // DB Parameters
    private static String db_user = "ias";
    private static String db_pswd = "ias";
    private static String db_url = Configuration.getDbUrl();

    // Test data range for Account Id and Device Id
    private static int BEGIN_ACCOUNT_ID = 1000001;
    private static int END_ACCOUNT_ID = 2000000;
    private static int BEGIN_DEVICE_ID = 1000001;
    private static int END_DEVICE_ID = 2000000;    
    
    // Calculated data range
    private static int deviceDataRange = END_DEVICE_ID - BEGIN_DEVICE_ID;
        
    // Various ratios in relation with the input data size
    private static double DEVICE_ACCOUNT_RATIO = 1.0;
    private static double VALID_TOKEN_RATIO = 0.6;
    private static double EXPIRED_TOKEN_RATIO = 0.3;
    private static double DEVICE_SUBSCRIPTION_RATIO = 0.75;
    private static double VALID_DEVICE_ACCOUNT_RATIO = 0.5;
    private static double REMOVED_DEVICE_ACCOUNT_RATIO = 0.25;
                
    // Hashsets stored in memory with test data
    private static Set deviceRegistered = null;
    private static Set deviceTokenValid = null;
    private static Set deviceTokenExpired = null;
    private static Set deviceSubscription = null;
    private static Set deviceAccountValid = null;
    private static Set deviceAccountRemoved = null;
    private static Set accountRegistered = null;
    private static Set accountTokenValid = null;
    private static Set accountTokenExpired = null;
    private static Set tokenIdGenerated = null;
    
    // Other default values
    private static int TOKEN_LENGTH = 22;
    private static int DEFAULT_TEST_DATA_SIZE = 1000;
    private static int DEFAULT_COMMIT_SIZE = 100;
    private static int DEFAULT_WAIT_TIME = 60000;
    private static boolean initialized = false;
    
    // SQL Statements
    private static String DB_CLEANUP =
        "DELETE FROM IAS_AUTHENTICATION_TOKENS WHERE account_id >= " + BEGIN_ACCOUNT_ID + " AND account_id <= " + END_ACCOUNT_ID + ";" +
        "DELETE FROM IAS_ACCOUNT_DEVICES WHERE account_id >= " + BEGIN_ACCOUNT_ID + " AND account_id <= " + END_ACCOUNT_ID + ";" +
        "DELETE FROM IAS_ACCOUNT_ATTRIBUTES WHERE account_id >= " + BEGIN_ACCOUNT_ID + " AND account_id <= " + END_ACCOUNT_ID + ";" +
        "DELETE FROM IAS_DEVICE_SUBSCRIPTIONS WHERE device_id >= " + BEGIN_DEVICE_ID + " AND device_id <= " + END_DEVICE_ID + ";" +
        "DELETE FROM IAS_AUTHENTICATION_TOKENS WHERE device_id >= " + BEGIN_DEVICE_ID + " AND device_id <= " + END_DEVICE_ID + ";" +
        "DELETE FROM IAS_ACCOUNT_DEVICES WHERE device_id >= " + BEGIN_DEVICE_ID + " AND device_id <= " + END_DEVICE_ID + ";" +
        "DELETE FROM IAS_ACCOUNTS WHERE account_id >= " + BEGIN_ACCOUNT_ID + " AND account_id <= " + END_ACCOUNT_ID + ";" +
        "DELETE FROM IAS_DEVICES WHERE device_id >= " + BEGIN_DEVICE_ID + " AND device_id <= " + END_DEVICE_ID;

    private static String INSERT_DEVICE_INFO =
        "INSERT into IAS_DEVICES (device_id, device_type_id, serial_no, register_date, public_key) " +
        "VALUES (?,?,?,sys_extract_utc(current_timestamp),?)";

    private static String INSERT_DEVICE_TOKEN =
        "INSERT into IAS_AUTHENTICATION_TOKENS (token_id, device_id, token_start_time, " +
        "token_expire_time, is_expired) VALUES (?,?,sys_extract_utc(current_timestamp),sys_extract_utc(current_timestamp)+1,?)";
    
    private static String INSERT_DEVICE_SUBSCRIPTION =
        "INSERT into IAS_DEVICE_SUBSCRIPTIONS (device_id, subscription_type, subscription_start, " +
        "subscription_exp) VALUES (?,?,sys_extract_utc(current_timestamp),sys_extract_utc(current_timestamp)+30)";
    
    private static String INSERT_ACCOUNT_INFO =
        "INSERT into IAS_ACCOUNTS (account_id, login_name, passwd, create_date, activate_date, status, nick_name, full_name, " +
        "email_address, birth_date) VALUES (?,?,?,sys_extract_utc(current_timestamp)," +
        "sys_extract_utc(current_timestamp),'A',?,?,?,sys_extract_utc(current_timestamp)-(21*365))";
    
    private static String INSERT_ACCOUNT_TOKEN =
        "INSERT into IAS_AUTHENTICATION_TOKENS (token_id, account_id, token_start_time, " +
        "token_expire_time, is_expired) VALUES (?,?,sys_extract_utc(current_timestamp),sys_extract_utc(current_timestamp)+1,?)";
    
    private static String INSERT_DEVICE_ACCOUNT =
        "INSERT into IAS_ACCOUNT_DEVICES (device_id, account_id, link_date, remove_date) ";
    
    private static String COUNT_TOKEN =
        "SELECT COUNT(*) FROM IAS_AUTHENTICATION_TOKENS WHERE token_id  = ?";
    
    // --------------------------------------------
    // Member methods
    // --------------------------------------------

    private static long getRandomNumber(long start, long end)
    {
        if (start > end) {
            long dummy = start;
            start = end;
            end  = dummy;
        }
    
        long range = end - start + 1; 
        
        return (long)(Math.random() * range) + start;        
    }
    
    private static String generateTokenId(Connection conn, PreparedStatement stmt)   
    {   
        String id = null;       
               
        try {
            boolean found = false;
                       
            while (id == null || tokenIdGenerated.contains(id) || found) {
                found = false;
                      
                id = UUID.randomUUID().toBase64().substring(0, TOKEN_LENGTH-1);
                stmt.setString(1, id);
                
                ResultSet rs = stmt.executeQuery();
                if (rs.next()) {
                    if (rs.getInt(1) == 1)
                        found = true;
                }                
                rs.close(); 
            }
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        tokenIdGenerated.add(id);
        return id;         
    } 
    
    // Obtain a subscription type
    private static String getSubscriptionType()
    {
        String[] subType = {"test001", "test002", "test003", "test004", "test005"};
        return subType[Math.abs((int)System.currentTimeMillis() % 5)];                      
    }
    
    public static void setUp()
    {
        setUp(DEFAULT_TEST_DATA_SIZE);
    }
    
    public static void setUp(int dataSize)
    {
        if (initialized) return;

        if (dataSize > deviceDataRange)
            dataSize = DEFAULT_TEST_DATA_SIZE;
        
        int sizeDeviceRegistered = dataSize;
        int sizeDeviceTokenValid = (int)(Math.ceil(VALID_TOKEN_RATIO * dataSize));
        int sizeDeviceTokenExpired = (int)(Math.ceil(EXPIRED_TOKEN_RATIO * dataSize));
        int sizeDeviceSubscription = (int)(Math.ceil(DEVICE_SUBSCRIPTION_RATIO * dataSize));
        int sizeAccountRegistered = (int)(Math.ceil(DEVICE_ACCOUNT_RATIO * dataSize));
        int sizeAccountTokenValid = (int)(Math.ceil(VALID_TOKEN_RATIO * dataSize));
        int sizeAccountTokenExpired = (int)(Math.ceil(EXPIRED_TOKEN_RATIO * dataSize));
        int sizeDeviceAccountValid = (int)(Math.ceil(VALID_DEVICE_ACCOUNT_RATIO * sizeAccountRegistered));
        int sizeDeviceAccountRemoved = (int)(Math.ceil(REMOVED_DEVICE_ACCOUNT_RATIO * sizeAccountRegistered)); 
        int sizeTokenIdGenerated = (int)(Math.ceil((VALID_TOKEN_RATIO+EXPIRED_TOKEN_RATIO) * dataSize))+
                (int)(Math.round((VALID_TOKEN_RATIO+EXPIRED_TOKEN_RATIO)*(DEVICE_ACCOUNT_RATIO*dataSize)));
               
        deviceRegistered = new HashSet(sizeDeviceRegistered);
        deviceTokenValid = new HashSet(sizeDeviceTokenValid);
        deviceTokenExpired = new HashSet(sizeDeviceTokenExpired);
        deviceSubscription = new HashSet(sizeDeviceSubscription);
                
        accountRegistered = new HashSet(sizeAccountRegistered);
        accountTokenValid = new HashSet(sizeAccountTokenValid);
        accountTokenExpired = new HashSet(sizeAccountTokenExpired);
        
        deviceAccountValid = new HashSet(sizeDeviceAccountValid);
        deviceAccountRemoved = new HashSet(sizeDeviceAccountRemoved);
        
        tokenIdGenerated = new HashSet(sizeTokenIdGenerated);
  
        DBConnection db = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        
        try {
            
            db = new DBConnection(db_user, db_pswd, db_url);
            db.cleanup(DB_CLEANUP);
            conn = db.getConnection();
            
            stmt = conn.prepareStatement(COUNT_TOKEN);
            for (int i = 1; i <= sizeDeviceRegistered; i++)
            {
                String deviceId = getUnRegisteredDeviceId();
                deviceRegistered.add(deviceId);
                   
                String tokenId = generateTokenId(conn, stmt);
                String tokenExpired = null;
                
                if (i <= sizeDeviceTokenValid)
                    tokenExpired = "0";
                else if (i <= (sizeDeviceTokenExpired + sizeDeviceTokenValid))
                    tokenExpired = "1";
                    
                Token dt = new Token(deviceId, tokenId);
                if (tokenExpired != null && tokenExpired.equals("0"))                
                    deviceTokenValid.add(dt);
                else if (tokenExpired != null && tokenExpired.equals("1"))
                    deviceTokenExpired.add(dt);
                
                if (i <= sizeDeviceSubscription) {
                    DeviceSubscription dsd = new DeviceSubscription(deviceId, getSubscriptionType());
                    deviceSubscription.add(dsd); 
                }
            }
            
            for (int i = 1; i <= sizeAccountRegistered; i++)
            {
                String accountId = getUnRegisteredAccountId();
                accountRegistered.add(accountId);
                   
                String tokenId = generateTokenId(conn, stmt);
                String tokenExpired = null;
                
                if (i <= sizeAccountTokenValid)
                    tokenExpired = "0";
                else if (i <= (sizeAccountTokenExpired + sizeAccountTokenValid))
                    tokenExpired = "1";
                    
                Token dt = new Token(accountId, tokenId);
                if (tokenExpired != null && tokenExpired.equals("0"))                
                    accountTokenValid.add(dt);
                else if (tokenExpired != null && tokenExpired.equals("1"))
                    accountTokenExpired.add(dt);
                
                if (i <= sizeDeviceAccountValid) {
                    DeviceAccount da = new DeviceAccount(getRegisteredDeviceId(), accountId);
                    deviceAccountValid.add(da); 
                } else if (i <= (sizeDeviceAccountRemoved + sizeDeviceAccountValid)) {
                    DeviceAccount da = new DeviceAccount(getRegisteredDeviceId(), accountId);
                    deviceAccountRemoved.add(da);
                }
            }
            stmt.close();
            
            int count = 0;
            
            // Insert Device Record
            stmt = conn.prepareStatement(INSERT_DEVICE_INFO);                  
            if (deviceRegistered != null && deviceRegistered.size() > 0) {
                Iterator iter = deviceRegistered.iterator();              
                
                while(iter.hasNext())
                {
                    String key = iter.next().toString();
                    count++;                    
                    
                    stmt.setLong(1, Long.parseLong(key));
                    stmt.setInt(2, 0);
                    stmt.setString(3, "sn"+key);
                    stmt.setString(4, "pb"+key);
                                        
                    stmt.executeUpdate();
                    
                    if (count == DEFAULT_COMMIT_SIZE || !iter.hasNext()) {
                        conn.commit();
                        count = 0;
                    }                                                    
                }
            }
            stmt.close();
            
            // Insert Device Tokens
            count = 0;
            stmt = conn.prepareStatement(INSERT_DEVICE_TOKEN);
            if (deviceTokenExpired != null && deviceTokenExpired.size() > 0) {
                Iterator iter = deviceTokenExpired.iterator();                
                
                while(iter.hasNext())
                {
                    Token et =(Token) iter.next();
                    count++;
                    
                    stmt.setString(1, et.tokenId);
                    stmt.setLong(2, Long.parseLong(et.id));
                    stmt.setInt(3, 1);
                                                           
                    stmt.executeUpdate();
                    
                    if (count == DEFAULT_COMMIT_SIZE || !iter.hasNext()) {
                        conn.commit();
                        count = 0;
                    }                                                           
                }
            }
            
            count = 0;
            if (deviceTokenValid != null && deviceTokenValid.size() > 0) {
                Iterator iter = deviceTokenValid.iterator();
                    
                while(iter.hasNext())
                {
                    Token vt =(Token) iter.next();
                    count++;
                    
                    stmt.setString(1, vt.tokenId);
                    stmt.setLong(2, Long.parseLong(vt.id));
                    stmt.setInt(3, 0);
                                                           
                    stmt.executeUpdate();
                    
                    if (count == DEFAULT_COMMIT_SIZE || !iter.hasNext()) {
                        conn.commit();
                        count = 0;
                    }                                                        
                }
            }
            stmt.close();
            
            // Insert Device Subscriptions
            count = 0;
            stmt = conn.prepareStatement(INSERT_DEVICE_SUBSCRIPTION);
            if (deviceSubscription != null && deviceSubscription.size() > 0) {
                Iterator iter = deviceSubscription.iterator();
                    
                while(iter.hasNext())
                {
                    DeviceSubscription dsd =(DeviceSubscription) iter.next();
                    count++;
                    
                    stmt.setLong(1, Long.parseLong(dsd.deviceId));
                    stmt.setString(2, dsd.type);                   
                                                           
                    stmt.executeUpdate();
                    
                    if (count == DEFAULT_COMMIT_SIZE || !iter.hasNext()) {
                        conn.commit();
                        count = 0;
                    }                                                        
                }
            }            
            stmt.close();
            
            // Insert Account Record
            count = 0;
            stmt = conn.prepareStatement(INSERT_ACCOUNT_INFO);                  
            if (accountRegistered != null && accountRegistered.size() > 0) {
                Iterator iter = accountRegistered.iterator();              
                                
                while(iter.hasNext())
                {
                    String key = iter.next().toString();
                    count++;                    
                                        
                    stmt.setLong(1, Long.parseLong(key));
                    stmt.setString(2, "login"+key);
                    stmt.setString(3, "pass"+key);
                    stmt.setString(4, "nick"+key);
                    stmt.setString(5, "full"+key);
                    stmt.setString(6, "email"+key+"@broadon.com");
                                                           
                    stmt.executeUpdate();
                    
                    if (count == DEFAULT_COMMIT_SIZE || !iter.hasNext()) {
                        conn.commit();
                        count = 0;
                    }                                                    
                }
            }
            stmt.close();
            
            // Insert Account Tokens
            count = 0;
            stmt = conn.prepareStatement(INSERT_ACCOUNT_TOKEN);
            if (accountTokenExpired != null && accountTokenExpired.size() > 0) {
                Iterator iter = accountTokenExpired.iterator();                
                
                while(iter.hasNext())
                {
                    Token et =(Token) iter.next();
                    count++;                   
                    
                    stmt.setString(1, et.tokenId);
                    stmt.setLong(2, Long.parseLong(et.id));
                    stmt.setInt(3, 1);
                                                           
                    stmt.executeUpdate();
                    
                    if (count == DEFAULT_COMMIT_SIZE || !iter.hasNext()) {
                        conn.commit();
                        count = 0;
                    }                                                           
                }
            }
            
            count = 0;
            if (accountTokenValid != null && accountTokenValid.size() > 0) {
                Iterator iter = accountTokenValid.iterator();
                    
                while(iter.hasNext())
                {
                    Token vt =(Token) iter.next();
                    count++;
                    
                    stmt.setString(1, vt.tokenId);
                    stmt.setLong(2, Long.parseLong(vt.id));
                    stmt.setInt(3, 0);
                                                           
                    stmt.executeUpdate();
                    
                    if (count == DEFAULT_COMMIT_SIZE || !iter.hasNext()) {
                        conn.commit();
                        count = 0;
                    }                                                        
                }
            }
            stmt.close();

            // Insert Valid Device Account Link
            count = 0;
            stmt = conn.prepareStatement(INSERT_DEVICE_ACCOUNT + "VALUES (?,?,sys_extract_utc(current_timestamp),null)");                  
            if (deviceAccountValid != null && deviceAccountValid.size() > 0) {
                Iterator iter = deviceAccountValid.iterator();              
                               
                while(iter.hasNext())
                {
                    DeviceAccount dad = (DeviceAccount) iter.next();
                    count++;
                                        
                    stmt.setLong(1, Long.parseLong(dad.deviceId));
                    stmt.setLong(2, Long.parseLong(dad.accountId));                  
                                                           
                    stmt.executeUpdate();
                    
                    if (count == DEFAULT_COMMIT_SIZE || !iter.hasNext()) {
                        conn.commit();
                        count = 0;
                    }                                                    
                }
            }
            stmt.close();
            
            // Insert Removed Device Account Link
            count = 0;
            stmt = conn.prepareStatement(INSERT_DEVICE_ACCOUNT + "VALUES (?,?,sys_extract_utc(current_timestamp),sys_extract_utc(current_timestamp)+30)");                  
            if (deviceAccountRemoved != null && deviceAccountRemoved.size() > 0) {
                Iterator iter = deviceAccountRemoved.iterator();              
                               
                while(iter.hasNext())
                {
                    DeviceAccount dad = (DeviceAccount) iter.next();
                    count++;
                                        
                    stmt.setLong(1, Long.parseLong(dad.deviceId));
                    stmt.setLong(2, Long.parseLong(dad.accountId));                  
                                                           
                    stmt.executeUpdate();
                    
                    if (count == DEFAULT_COMMIT_SIZE || !iter.hasNext()) {
                        conn.commit();
                        count = 0;
                    }                                                    
                }
            }
            stmt.close();
            
        } catch (SQLException ex) {
            ex.printStackTrace();

        } catch (IOException ex) {
            ex.printStackTrace();

        } finally {            
            try {
                if (conn != null)
                    db.closeConnection();
                if (stmt != null)
                    stmt.close();
            }
            catch (Throwable e)
            {}
        }

        initialized = true;
        return;
    }

    // Obtain a device id with expired token and a linked account
    public static String getDeviceIdWithExpiredTokenAndAccount(boolean removed)
    {
        Set deviceIdWithExpiredToken = new HashSet(deviceTokenExpired.size());
        Object[] vt = deviceTokenExpired.toArray();
            
        for (int i = 0; i < deviceTokenExpired.size(); i++) {
            deviceIdWithExpiredToken.add(((Token)vt[i]).id);
        }
            
        if (removed) {
            Set deviceIdWithRemovedAccount = new HashSet(deviceAccountRemoved.size());
            Object[] ds = deviceAccountRemoved.toArray();
            
            for (int i = 0; i < deviceAccountRemoved.size(); i++) {
                deviceIdWithRemovedAccount.add(((DeviceAccount)ds[i]).deviceId);
            }
            
            Set deviceIdWithExpiredTokenAndRemovedAccount = new TreeSet(deviceIdWithExpiredToken);
            deviceIdWithExpiredTokenAndRemovedAccount.retainAll(deviceIdWithRemovedAccount);
                    
            if (deviceIdWithExpiredTokenAndRemovedAccount.size() > 0) {
                Object[] detra = deviceIdWithExpiredTokenAndRemovedAccount.toArray();            
                return (String)detra[(int)getRandomNumber(0, deviceIdWithExpiredTokenAndRemovedAccount.size()-1)];      
            } else {
                return null;
            }
        } else {
            Set deviceIdWithValidAccount = new HashSet(deviceAccountValid.size());
            Object[] ds = deviceAccountValid.toArray();
            
            for (int i = 0; i < deviceAccountValid.size(); i++) {
                deviceIdWithValidAccount.add(((DeviceAccount)ds[i]).deviceId);
            }
            
            Set deviceIdWithExpiredTokenAndValidAccount = new TreeSet(deviceIdWithExpiredToken);
            deviceIdWithExpiredTokenAndValidAccount.retainAll(deviceIdWithValidAccount);
            
            if (deviceIdWithExpiredTokenAndValidAccount.size() > 0) {
                Object[] detva = deviceIdWithExpiredTokenAndValidAccount.toArray();            
                return (String)detva[(int)getRandomNumber(0, deviceIdWithExpiredTokenAndValidAccount.size()-1)];      
            } else {
                return null;
            }
        }
    }
    
    // Obtain a device id with expired token which has/does not have subscription
    public static String getDeviceIdWithExpiredTokenAndSubscription(boolean subscription)
    {
        Set deviceIdWithSubscription = new HashSet(deviceSubscription.size());
        Object[] ds = deviceSubscription.toArray();
        
        for (int i = 0; i < deviceSubscription.size(); i++) {
            deviceIdWithSubscription.add(((DeviceSubscription)ds[i]).deviceId);
        }
        
        Set deviceIdWithExpiredToken = new HashSet(deviceTokenExpired.size());
        Object[] vt = deviceTokenExpired.toArray();
            
        for (int i = 0; i < deviceTokenExpired.size(); i++) {
            deviceIdWithExpiredToken.add(((Token)vt[i]).id);
        }
            
        if (subscription) {
            Set deviceIdWithExpiredTokenAndSubscription = new TreeSet(deviceIdWithExpiredToken);
            deviceIdWithExpiredTokenAndSubscription.retainAll(deviceIdWithSubscription);
                    
            if (deviceIdWithExpiredTokenAndSubscription.size() > 0) {
                Object[] dets = deviceIdWithExpiredTokenAndSubscription.toArray();            
                return (String)dets[(int)getRandomNumber(0, deviceIdWithExpiredTokenAndSubscription.size()-1)];      
            } else {
                return null;
            }
        } else {
            Set deviceIdWithExpiredTokenAndNoSubscription = new TreeSet(deviceIdWithExpiredToken);
            deviceIdWithExpiredTokenAndNoSubscription.removeAll(deviceIdWithSubscription);
                    
            if (deviceIdWithExpiredTokenAndNoSubscription.size() > 0) {
                Object[] detns = deviceIdWithExpiredTokenAndNoSubscription.toArray();            
                return (String)detns[(int)getRandomNumber(0, deviceIdWithExpiredTokenAndNoSubscription.size()-1)];      
            } else {
                return null;
            }
        }                         
    }
    
    // Obtain an Account Id with no existing valid/expired tokens
    public static String getAccountIdWithNoToken()
    {
        Set accountIdWithValidToken = new HashSet(accountTokenValid.size());
        Set accountIdWithExpiredToken = new HashSet(accountTokenExpired.size());
        
        Object[] vt = accountTokenValid.toArray();
        Object[] et = accountTokenExpired.toArray();
        
        for (int i = 0; i < accountTokenValid.size(); i++) {
            accountIdWithValidToken.add(((Token)vt[i]).id);
        }
        for (int i = 0; i < accountTokenExpired.size(); i++) {
            accountIdWithExpiredToken.add(((Token)et[i]).id);
        }
        
        Set accountIdWithToken = new TreeSet(accountIdWithValidToken);
        accountIdWithToken.addAll(accountIdWithExpiredToken);
        
        Set accountIdWithNoToken = new TreeSet(accountRegistered);
        accountIdWithNoToken.removeAll(accountIdWithToken);
        
        if (accountIdWithNoToken.size() > 0) {
            Object[] nt = accountIdWithNoToken.toArray();            
            return (String)nt[(int)getRandomNumber(0, accountIdWithNoToken.size()-1)];      
        } else {
            return null;
        }                 
    }
    
    // Obtain a Device Id with no existing valid/expired tokens
    public static String getDeviceIdWithNoToken()
    {
        Set deviceIdWithValidToken = new HashSet(deviceTokenValid.size());
        Set deviceIdWithExpiredToken = new HashSet(deviceTokenExpired.size());
        
        Object[] vt = deviceTokenValid.toArray();
        Object[] et = deviceTokenExpired.toArray();
        
        for (int i = 0; i < deviceTokenValid.size(); i++) {
            deviceIdWithValidToken.add(((Token)vt[i]).id);
        }
        for (int i = 0; i < deviceTokenExpired.size(); i++) {
            deviceIdWithExpiredToken.add(((Token)et[i]).id);
        }
        
        Set deviceIdWithToken = new TreeSet(deviceIdWithValidToken);
        deviceIdWithToken.addAll(deviceIdWithExpiredToken);
        
        Set deviceIdWithNoToken = new TreeSet(deviceRegistered);
        deviceIdWithNoToken.removeAll(deviceIdWithToken);
        
        if (deviceIdWithNoToken.size() > 0) {
            Object[] nt = deviceIdWithNoToken.toArray();            
            return (String)nt[(int)getRandomNumber(0, deviceIdWithNoToken.size()-1)];      
        } else {
            return null;
        }                 
    }
    
    // Obtain a device id with no existing tokens which has/does not have subscription
    public static String getDeviceIdWithNoTokenAndSubscription(boolean subscription)
    {
        Set deviceIdWithSubscription = new HashSet(deviceSubscription.size());
        Object[] ds = deviceSubscription.toArray();
        
        for (int i = 0; i < deviceSubscription.size(); i++) {
            deviceIdWithSubscription.add(((DeviceSubscription)ds[i]).deviceId);
        }
        
        Set deviceIdWithValidToken = new HashSet(deviceTokenValid.size());
        Set deviceIdWithExpiredToken = new HashSet(deviceTokenExpired.size());
        
        Object[] vt = deviceTokenValid.toArray();
        Object[] et = deviceTokenExpired.toArray();
        
        for (int i = 0; i < deviceTokenValid.size(); i++) {
            deviceIdWithValidToken.add(((Token)vt[i]).id);
        }
        for (int i = 0; i < deviceTokenExpired.size(); i++) {
            deviceIdWithExpiredToken.add(((Token)et[i]).id);
        }
        
        Set deviceIdWithToken = new TreeSet(deviceIdWithValidToken);
        deviceIdWithToken.addAll(deviceIdWithExpiredToken);
        
        Set deviceIdWithNoToken = new TreeSet(deviceRegistered);
        deviceIdWithNoToken.removeAll(deviceIdWithToken);
            
        if (subscription) {
            Set deviceIdWithNoTokenAndSubscription = new TreeSet(deviceIdWithNoToken);
            deviceIdWithNoTokenAndSubscription.retainAll(deviceIdWithSubscription);
                    
            if (deviceIdWithNoTokenAndSubscription.size() > 0) {
                Object[] dnts = deviceIdWithNoTokenAndSubscription.toArray();            
                return (String)dnts[(int)getRandomNumber(0, deviceIdWithNoTokenAndSubscription.size()-1)];      
            } else {
                return null;
            }
        } else {
            Set deviceIdWithNoTokenAndNoSubscription = new TreeSet(deviceIdWithNoToken);
            deviceIdWithNoTokenAndNoSubscription.removeAll(deviceIdWithSubscription);
                    
            if (deviceIdWithNoTokenAndNoSubscription.size() > 0) {
                Object[] dntns = deviceIdWithNoTokenAndNoSubscription.toArray();            
                return (String)dntns[(int)getRandomNumber(0, deviceIdWithNoTokenAndNoSubscription.size()-1)];      
            } else {
                return null;
            }
        }                         
    }
    
    // Obtain a device id with valid token and a linked account
    public static String getDeviceIdWithValidTokenAndAccount(boolean removed)
    {
        Set deviceIdWithValidToken = new HashSet(deviceTokenValid.size());
        Object[] vt = deviceTokenValid.toArray();
            
        for (int i = 0; i < deviceTokenValid.size(); i++) {
            deviceIdWithValidToken.add(((Token)vt[i]).id);
        }
            
        if (removed) {
            Set deviceIdWithRemovedAccount = new HashSet(deviceAccountRemoved.size());
            Object[] ds = deviceAccountRemoved.toArray();
            
            for (int i = 0; i < deviceAccountRemoved.size(); i++) {
                deviceIdWithRemovedAccount.add(((DeviceAccount)ds[i]).deviceId);
            }
            
            Set deviceIdWithValidTokenAndRemovedAccount = new TreeSet(deviceIdWithValidToken);
            deviceIdWithValidTokenAndRemovedAccount.retainAll(deviceIdWithRemovedAccount);
                    
            if (deviceIdWithValidTokenAndRemovedAccount.size() > 0) {
                Object[] dvtra = deviceIdWithValidTokenAndRemovedAccount.toArray();            
                return (String)dvtra[(int)getRandomNumber(0, deviceIdWithValidTokenAndRemovedAccount.size()-1)];      
            } else {
                return null;
            }
        } else {
            Set deviceIdWithValidAccount = new HashSet(deviceAccountValid.size());
            Object[] ds = deviceAccountValid.toArray();
            
            for (int i = 0; i < deviceAccountValid.size(); i++) {
                deviceIdWithValidAccount.add(((DeviceAccount)ds[i]).deviceId);
            }
            
            Set deviceIdWithValidTokenAndValidAccount = new TreeSet(deviceIdWithValidToken);
            deviceIdWithValidTokenAndValidAccount.retainAll(deviceIdWithValidAccount);
            
            if (deviceIdWithValidTokenAndValidAccount.size() > 0) {
                Object[] dvtva = deviceIdWithValidTokenAndValidAccount.toArray();            
                return (String)dvtva[(int)getRandomNumber(0, deviceIdWithValidTokenAndValidAccount.size()-1)];      
            } else {
                return null;
            }
        }
    }
    
    // Obtain a device id with valid token and no linked accounts
    public static String getDeviceIdWithValidTokenAndNoAccount()
    {
        Set deviceIdWithValidToken = new HashSet(deviceTokenValid.size());
        Object[] vt = deviceTokenValid.toArray();
            
        for (int i = 0; i < deviceTokenValid.size(); i++) {
            deviceIdWithValidToken.add(((Token)vt[i]).id);
        }
        
        Set deviceIdWithRemovedAccount = new HashSet(deviceAccountRemoved.size());
        Set deviceIdWithValidAccount = new HashSet(deviceAccountValid.size());
        
        Object[] ar = deviceAccountRemoved.toArray();
        Object[] av = deviceAccountValid.toArray();
        
        for (int i = 0; i < deviceAccountRemoved.size(); i++) {
            deviceIdWithRemovedAccount.add(((DeviceAccount)ar[i]).deviceId);
        }
        for (int i = 0; i < deviceAccountValid.size(); i++) {
            deviceIdWithValidAccount.add(((DeviceAccount)av[i]).deviceId);
        }
        
        Set deviceIdWithAccount = new TreeSet(deviceIdWithValidAccount);
        deviceIdWithAccount.addAll(deviceIdWithRemovedAccount);
        
        Set deviceIdWithValidTokenAndNoAccount = new TreeSet(deviceIdWithValidToken);
        deviceIdWithValidTokenAndNoAccount.removeAll(deviceIdWithAccount);
        
        if (deviceIdWithValidTokenAndNoAccount.size() > 0) {
            Object[] dvtna = deviceIdWithValidTokenAndNoAccount.toArray();            
            return (String)dvtna[(int)getRandomNumber(0, deviceIdWithValidTokenAndNoAccount.size()-1)];      
        } else {
            return null;
        }
    }
    
    // Obtain a device id with valid token which has/does not have subscription
    public static String getDeviceIdWithValidTokenAndSubscription(boolean subscription)
    {
        Set deviceIdWithSubscription = new HashSet(deviceSubscription.size());
        Object[] ds = deviceSubscription.toArray();
        
        for (int i = 0; i < deviceSubscription.size(); i++) {
            deviceIdWithSubscription.add(((DeviceSubscription)ds[i]).deviceId);
        }
        
        Set deviceIdWithValidToken = new HashSet(deviceTokenValid.size());
        Object[] vt = deviceTokenValid.toArray();
            
        for (int i = 0; i < deviceTokenValid.size(); i++) {
            deviceIdWithValidToken.add(((Token)vt[i]).id);
        }
            
        if (subscription) {
            Set deviceIdWithValidTokenAndSubscription = new TreeSet(deviceIdWithValidToken);
            deviceIdWithValidTokenAndSubscription.retainAll(deviceIdWithSubscription);
                    
            if (deviceIdWithValidTokenAndSubscription.size() > 0) {
                Object[] dvts = deviceIdWithValidTokenAndSubscription.toArray();            
                return (String)dvts[(int)getRandomNumber(0, deviceIdWithValidTokenAndSubscription.size()-1)];      
            } else {
                return null;
            }
        } else {
            Set deviceIdWithValidTokenAndNoSubscription = new TreeSet(deviceIdWithValidToken);
            deviceIdWithValidTokenAndNoSubscription.removeAll(deviceIdWithSubscription);
            
            if (deviceIdWithValidTokenAndNoSubscription.size() > 0) {
                Object[] dvtns = deviceIdWithValidTokenAndNoSubscription.toArray();            
                return (String)dvtns[(int)getRandomNumber(0, deviceIdWithValidTokenAndNoSubscription.size()-1)];      
            } else {
                return null;
            }
        }
    }

    // Obtain device subscription
    public static DeviceSubscription getDeviceSubscription()
    {
        Object[] types = deviceSubscription.toArray();
        int size = deviceSubscription.size();
        
        return (DeviceSubscription)types[(int)getRandomNumber(0, size-1)];            
    }
    
    // Obtain subscriptions for a given deviceId
    public static String[] getDeviceSubscriptions(String deviceId)
    {
        Object[] ds = deviceSubscription.toArray();
        List subList = new ArrayList();
        
        for (int i = 0; ds != null && i < ds.length; i++) {
            String id = ((DeviceSubscription)ds[i]).deviceId;
            if (id.equals(deviceId)) {
                subList.add(((DeviceSubscription)ds[i]).type);                
            }
        }
        
        String[] subs = null;
        if (subList != null && subList.size() > 0) {
            subs = new String[subList.size()];
            subs = (String[]) subList.toArray(subs);                               
        } 
        
        return subs;
    }
    
    // Obtain an expired token for a given accountId
    public static String getExpiredAccountToken(String accountId)
    {
        Object[] et = accountTokenExpired.toArray();
        String token = null;
        
        for (int i = 0; et != null && i < et.length; i++) {
            String id = ((Token)et[i]).id;
            if (id.equals(accountId)) {
                token = ((Token)et[i]).tokenId;
                break;
            }
        }
        return token;
    }
    
    // Obtain an expired account token
    public static Token getExpiredAccountToken()
    {
        Object[] ids = accountTokenExpired.toArray();
        int size = accountTokenExpired.size();
        
        return (Token)ids[(int)getRandomNumber(0, size-1)];            
    }
    
    // Obtain an expired token for a given deviceId
    public static String getExpiredDeviceToken(String deviceId)
    {
        Object[] et = deviceTokenExpired.toArray();
        String token = null;
        
        for (int i = 0; et != null && i < et.length; i++) {
            String id = ((Token)et[i]).id;
            if (id.equals(deviceId)) {
                token = ((Token)et[i]).tokenId;
                break;
            }
        }
        return token;
    }
    
    // Obtain an expired device token
    public static Token getExpiredDeviceToken()
    {
        Object[] ids = deviceTokenExpired.toArray();
        int size = deviceTokenExpired.size();
        
        return (Token)ids[(int)getRandomNumber(0, size-1)];            
    }
    
    // Obtain a registered Account Id
    public static String getRegisteredAccountId()
    {
        Object[] ids = accountRegistered.toArray();
        int size = accountRegistered.size();
        
        return (String)ids[(int)getRandomNumber(0, size-1)];       
    }
    
    // Obtain a registered Device Id
    public static String getRegisteredDeviceId()
    {
        Object[] ids = deviceRegistered.toArray();
        int size = deviceRegistered.size();
        
        return (String)ids[(int)getRandomNumber(0, size-1)];       
    }
    
    // Obtain a removed device account link
    public static DeviceAccount getRemovedDeviceAccount()
    {
        Object[] ids = deviceAccountRemoved.toArray();
        int size = deviceAccountRemoved.size();
        
        return (DeviceAccount)ids[(int)getRandomNumber(0, size-1)];            
    }
    
    // Obtain all removed account link for a device id
    public static String[] getRemovedDeviceAccounts(String deviceId)
    {
        Object[] ids = deviceAccountRemoved.toArray();
        int size = deviceAccountRemoved.size();
        Set matchedAccounts = new HashSet(size);
        
        for (int i = 0; i < size; i++) {
            DeviceAccount da = (DeviceAccount)ids[i];
            if (da.deviceId.equals(deviceId))
                matchedAccounts.add(da);                
        }
        
        ids = matchedAccounts.toArray();
        size = matchedAccounts.size();
        String[] accounts = new String[size];
        
        for (int i = 0; i < size; i++) {
            DeviceAccount da = (DeviceAccount)ids[i];
            accounts[i] = da.accountId;
        }        
        
        return accounts;            
    }
    
    // Obtain an un-registered Account Id
    public static String getUnRegisteredAccountId()
    {        
        String id = String.valueOf(getRandomNumber(BEGIN_ACCOUNT_ID, END_ACCOUNT_ID));
        long start = System.currentTimeMillis();
        
        while (accountRegistered.contains(id)) {
            id = String.valueOf(getRandomNumber(BEGIN_ACCOUNT_ID, END_ACCOUNT_ID));
            if ((System.currentTimeMillis() - start) > DEFAULT_WAIT_TIME) {
                id = null;
                break;
            }
        }
        
        return id;
    }
    
    // Obtain an un-registered Device Id
    public static String getUnRegisteredDeviceId()
    {        
        String id = String.valueOf(getRandomNumber(BEGIN_DEVICE_ID, END_DEVICE_ID));
        long start = System.currentTimeMillis();
        
        while (deviceRegistered.contains(id)) {
            id = String.valueOf(getRandomNumber(BEGIN_DEVICE_ID, END_DEVICE_ID));
            if ((System.currentTimeMillis() - start) > DEFAULT_WAIT_TIME) {
                id = null;
                break;
            }
        }
        
        return id;
    }
    
    // Obtain a valid token for a given accountId
    public static String getValidAccountToken(String accountId)
    {
        Object[] vt = accountTokenValid.toArray();
        String token = null;
        
        for (int i = 0; vt != null && i < vt.length; i++) {
            String id = ((Token)vt[i]).id;
            if (id.equals(accountId)) {
                token = ((Token)vt[i]).tokenId;
                break;
            }
        }
        return token;
    }
    
    // Obtain a valid account token
    public static Token getValidAccountToken()
    {
        Object[] ids = accountTokenValid.toArray();
        int size = accountTokenValid.size();
        
        return (Token)ids[(int)getRandomNumber(0, size-1)];
    }
    
    // Obtain a valid device account link
    public static DeviceAccount getValidDeviceAccount()
    {
        Object[] ids = deviceAccountValid.toArray();
        int size = deviceAccountValid.size();
        
        return (DeviceAccount)ids[(int)getRandomNumber(0, size-1)];            
    }
    
    // Obtain all valid account link for a device id
    public static String[] getValidDeviceAccounts(String deviceId)
    {
        Object[] ids = deviceAccountValid.toArray();
        int size = deviceAccountValid.size();
        Set matchedAccounts = new HashSet(size);
        
        for (int i = 0; i < size; i++) {
            DeviceAccount da = (DeviceAccount)ids[i];
            if (da.deviceId.equals(deviceId))
                matchedAccounts.add(da);                
        }
        
        ids = matchedAccounts.toArray();
        size = matchedAccounts.size();
        String[] accounts = new String[size];
        
        for (int i = 0; i < size; i++) {
            DeviceAccount da = (DeviceAccount)ids[i];
            accounts[i] = da.accountId;
        }        
        
        return accounts;            
    }
    
    // Obtain a valid device token
    public static Token getValidDeviceToken()
    {
        Object[] ids = deviceTokenValid.toArray();
        int size = deviceTokenValid.size();
        
        return (Token)ids[(int)getRandomNumber(0, size-1)];
    }
    
    // Obtain a valid token for a given deviceId
    public static String getValidDeviceToken(String deviceId)
    {
        Object[] vt = deviceTokenValid.toArray();
        String token = null;
        
        for (int i = 0; vt != null && i < vt.length; i++) {
            String id = ((Token)vt[i]).id;
            if (id.equals(deviceId)) {
                token = ((Token)vt[i]).tokenId;
                break;
            }
        }
        return token;
    }
    
    // Set device subscription
    public static void setDeviceSubscription(String deviceId, String subtype)
    {
        DeviceSubscription dsd = new DeviceSubscription(deviceId, subtype);
        setDeviceSubscription(dsd);                
    }
    
    // Set device subscription
    public static void setDeviceSubscription(DeviceSubscription dsd)
    {
        deviceSubscription.add(dsd);                
    }
    
    // Set an expired account token
    public static void setExpiredAccountToken(String accountId, String tokenId)
    {
        Token et = new Token(accountId, tokenId);
        setExpiredAccountToken(et);                
    }
    
    // Set an expired account token
    public static void setExpiredAccountToken(Token et)
    {
/*        Object[] array = accountTokenValid.toArray();
        for (int i = 0; array != null && i < array.length; i++) {
            Token at = (Token)array[i];
            if (at.id.equals(et.id) && at.tokenId.equals(et.tokenId)) {
                accountTokenValid.remove(at);
                break;
            }            
        }
 */     accountTokenValid.remove(et);  
        accountTokenExpired.add(et);                
    }
    
    // Set an expired device token
    public static void setExpiredDeviceToken(String deviceId, String tokenId)
    {
        Token et = new Token(deviceId, tokenId);
        setExpiredDeviceToken(et);                
    }
    
    // Set an expired device token
    public static void setExpiredDeviceToken(Token et)
    {
/*        Object[] array = deviceTokenValid.toArray();
        for (int i = 0; array != null && i < array.length; i++) {
            Token at = (Token)array[i];
            if (at.id.equals(et.id) && at.tokenId.equals(et.tokenId)) {
                deviceTokenValid.remove(at);
                break;
            }
        }
*/      deviceTokenValid.remove(et);          
        deviceTokenExpired.add(et);                
    }
    
    // Set a registered Account Id
    public static void setRegisteredAccountId(String accountId)
    {
        accountRegistered.add(accountId);
    }
    
    // Set a registered Device Id
    public static void setRegisteredDeviceId(String deviceId)
    {
        deviceRegistered.add(deviceId);
    }
    
    // Set a removed device account link
    public static void setRemovedDeviceAccount(String deviceId, String accountId)
    {
        DeviceAccount da = new DeviceAccount(deviceId, accountId);
        setRemovedDeviceAccount(da);                
    }
    
    // Set a removed device account link
    public static void setRemovedDeviceAccount(DeviceAccount da)
    {
/*        Object[] array = deviceAccountValid.toArray();
        for (int i = 0; array != null && i < array.length; i++) {
            DeviceAccount ada = (DeviceAccount)array[i];
            if (ada.deviceId.equals(da.deviceId) && ada.accountId.equals(da.accountId)) {
                deviceAccountValid.remove(ada);
                break;
            }
        }
*/      deviceAccountValid.remove(da);  
        deviceAccountRemoved.add(da);                
    }
    
    // Set a valid account token
    public static void setValidAccountToken(String accountId, String tokenId)
    {
        Token vt = new Token(accountId, tokenId);
        setValidAccountToken(vt);                
    }
    
    // Set a valid account token
    public static void setValidAccountToken(Token vt)
    {
        accountTokenValid.add(vt);                
    }
    
    // Set a device account link
    public static void setValidDeviceAccount(String deviceId, String accountId)
    {
        DeviceAccount da = new DeviceAccount(deviceId, accountId);
        setValidDeviceAccount(da);                
    }
    
    // Set a device account link
    public static void setValidDeviceAccount(DeviceAccount da)
    {
/*        Object[] array = deviceAccountRemoved.toArray();
        for (int i = 0; array != null && i < array.length; i++) {
            DeviceAccount ada = (DeviceAccount)array[i];
            if (ada.deviceId.equals(da.deviceId) && ada.accountId.equals(da.accountId)) {
                deviceAccountRemoved.remove(ada);
                break;
            }
        }
*/      deviceAccountRemoved.remove(da);
        deviceAccountValid.add(da);                
    }
    
    // Set a valid device token
    public static void setValidDeviceToken(String deviceId, String tokenId)
    {
        Token vt = new Token(deviceId, tokenId);
        setValidDeviceToken(vt);                
    }
    
    // Set a valid device token
    public static void setValidDeviceToken(Token vt)
    {
       deviceTokenValid.add(vt);                
    }
 
    public static void main(String[] args)
    {
        if (args.length != 1)
           setUp(DEFAULT_TEST_DATA_SIZE);
        else
           setUp(Integer.parseInt(args[0]));        
    }
    
} // class TestDataAdmin

class DeviceAccount
{
    String deviceId;
    String accountId;
        
    DeviceAccount (String deviceId, String accountId)
    {
        this.deviceId = deviceId;
        this.accountId = accountId;                
    }
    
    public int hashCode() { return deviceId.hashCode() + accountId.hashCode(); }
    
    public boolean equals(Object obj)
    {
        if (obj != null && obj instanceof DeviceAccount) {
            DeviceAccount da = (DeviceAccount) obj;
            return (this.deviceId == da.deviceId &&
                    this.accountId == da.accountId);
        }
        return false;
    }
}

class DeviceSubscription
{
    String deviceId;
    String type;
        
    DeviceSubscription (String deviceId, String type)
    {
        this.deviceId = deviceId;
        this.type = type;                
    }
    
    public int hashCode() { return deviceId.hashCode() + type.hashCode(); }
    
    public boolean equals(Object obj)
    {
        if (obj != null && obj instanceof DeviceSubscription) {
            DeviceSubscription ds = (DeviceSubscription) obj;
            return (this.deviceId == ds.deviceId &&
                    this.type == ds.type);
        }
        return false;
    }
}

class Token
{
    String id;
    String tokenId;
        
    Token (String id, String tokenId)
    {
        this.id = id;
        this.tokenId = tokenId;                
    }
    
    public int hashCode() { return id.hashCode() + tokenId.hashCode(); }
        
    public boolean equals(Object obj)
    {
        if (obj != null && obj instanceof Token) {
            Token token = (Token) obj;
            return (this.id == token.id &&
                    this.tokenId == token.tokenId);
        }
        return false;
    }
}


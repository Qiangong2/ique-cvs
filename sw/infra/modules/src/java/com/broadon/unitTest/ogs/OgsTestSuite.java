/*
 * OGS TestSuite:  Add new OGS TestCase here to this class.
 */
package com.broadon.unitTest.ogs;

import junit.framework.Test;
import junit.framework.TestSuite;

public class OgsTestSuite {

	public static void main(String[] args) {
		junit.textui.TestRunner.run(OgsTestSuite.suite());
	}

	public static Test suite() {
		TestSuite suite = new TestSuite("Test for com.broadon.unitTest.ogs");
		
                //$JUnit-BEGIN$
                suite.addTestSuite(ValidateBuddyState.class);
                
                //$JUnit-END$
                
		return suite;
	}

}

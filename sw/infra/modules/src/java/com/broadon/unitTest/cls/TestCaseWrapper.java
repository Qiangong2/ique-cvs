package com.broadon.unitTest.cls;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.rmi.RemoteException;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Random;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;

import junit.framework.TestCase;

import com.broadon.cls.License;
import com.broadon.cps.AuxContentInfo;
import com.broadon.cps.TMDGenerator;
import com.broadon.cps.TitleMetaData;
import com.broadon.exception.BroadOnException;
import com.broadon.see.Keywords;
import com.broadon.unitTest.Configuration;
import com.broadon.unitTest.ResourcePool;
import com.broadon.unitTest.TestLog;
import com.broadon.unitTest.cps.DBSetUp;
import com.broadon.wsapi.cps.ActionType;
import com.broadon.wsapi.cps.ContentAttributeType;
import com.broadon.wsapi.cps.PublishPortType;
import com.broadon.wsapi.cps.PublishRequestType;
import com.broadon.wsapi.cps.UploadRequestType;
import com.broadon.wsapi.cps.UploadResponseType;

public class TestCaseWrapper extends TestCase implements Keywords
{
    protected static final long TEST_CHANNEL = 0x0001800200000000L;
    protected static final long TEST_SUBSCRIPT_CHANNEL = 0x0001800300000000L;
    protected static final String SUBSCRIPT_CHANNEL_PASSWORD = "channel password";
    
    protected PublishPortType cps = null;
    protected boolean tracing = false;
    
    protected static DBSetUp db;
    protected static String hostName = null;
    protected static License license;
    protected static File[] contentFile;
    protected static File outDir;
    protected static TitleMetaData tmdTemplate;
    protected static TitleMetaData subTmdTemplate;
    
    protected long channel;
    
    static {
        try {
            db = new DBSetUp();
            db.removeChannel(TEST_CHANNEL);
            db.removeChannel(TEST_SUBSCRIPT_CHANNEL);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }    
        
        System.loadLibrary("cls");
        
        try {
            outDir = new File("/tmp/cls-test");
            if (! outDir.isDirectory()) {
                outDir.mkdirs();
            }
            Random rand = new Random();
            byte[] buf = new byte[64];
            rand.nextBytes(buf);
            File tempContent = File.createTempFile("CLS", null, outDir);
            tempContent.deleteOnExit();
            FileOutputStream out = new FileOutputStream(tempContent);
            out.write(buf);
            out.close();
            contentFile = new File[1];
            contentFile[0] = tempContent;
            
            tmdTemplate = createTmdTemplate(buf);
            subTmdTemplate = createTmdTemplate(null);
            
            
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (GeneralSecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (BroadOnException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        Properties prop = new Properties();
        prop.put(USE_SSM_KEY, "true");
        prop.put(KEY_FILE_LIST, "/flash/NetC-publisher-key.pem,/flash/NetC-eTicket-key.pem");
        prop.put(License.CA_CRL_VERSION_KEY, "1");
        prop.put(License.XS_CRL_VERSION_KEY, "0");
        prop.put(License.CP_CRL_VERSION_KEY, "0");
        prop.put(License.CA_CERT_FILE_KEY, "/flash/NetC-CA-cert.raw");
        prop.put(License.XS_CERT_FILE_KEY, "/flash/NetC-eTicket-cert.raw");
        prop.put(License.CP_CERT_FILE_KEY, "/flash/NetC-publisher-cert.raw");

        try {
            license = new License(prop);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
        
    public TestCaseWrapper(String name) {
        super(name);
        channel = TEST_CHANNEL;
        if (hostName != null) {
            Configuration.setHostName(hostName);
            Configuration.setDomainName("");
        }
    }

    private static TitleMetaData createTmdTemplate(byte[] content) throws NoSuchAlgorithmException, BroadOnException
    {
        byte[] hash = null;
        if (content != null) {
            // compute sha-1 hash of the content
            MessageDigest sha = MessageDigest.getInstance("SHA-1");
            hash = sha.digest(content);
        }
        
        PublishRequestType req = new PublishRequestType();
        req.setAccessRights(1);
        req.setAction(ActionType.Create);
        req.setBootContentIndex(0);
        if (content != null) {
            ContentAttributeType[] contAttr = new ContentAttributeType[1];
            contAttr[0] = new ContentAttributeType();
            contAttr[0].setContentIndex(0);
            contAttr[0].setContentObjectName("Test content");
            contAttr[0].setContentObjectType("GAME");
            req.setContents(contAttr);
        } else {
            req.setContents(null);
        }
        req.setGroupID(1);
        req.setOSVersion("0101010101010101");
        req.setTitleType("GAME");
        req.setCustomData(new byte[62]);

        byte[] dummy = new byte[16];
        TMDGenerator tmdGen = new TMDGenerator(dummy , dummy, 1, 1, "dummy", "dummy");
        ArrayList info = new ArrayList();
        if (content != null) {
            AuxContentInfo aux = new AuxContentInfo(0, hash, content.length);
            info.add(aux);
        }
        byte[] tmd = tmdGen.generateMetaData(req, info, 1, 1);
        return new TitleMetaData(tmd);
    }

    public void setUp() throws Exception {
        super.setUp();
        if (cps == null) {
            cps = ResourcePool.newPublishService();
        }
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }
        
    protected void runTest() throws Throwable 
    {
        boolean testResult = true;
        try {
            super.runTest();
        } catch (Throwable e) {
            System.out.println(getName() + " error: " + e.getMessage());
            if (tracing) {
                e.printStackTrace();
            }
            testResult = false;
            throw e;
        } finally {
            TestLog.report("CLS", getName(), testResult);
        }
    }

    protected String getTitleIDString(long titleID)
    {
        return Long.toHexString(titleID);
    }

    protected PublishRequestType initRequest(long titleID) throws IOException
    {
        ContentAttributeType[] contents = new ContentAttributeType[1];
        contents[0] = new ContentAttributeType(0, "Test Content", "GAME");
        
        PublishRequestType req = new PublishRequestType(ActionType.Create, "LOCAL_CPS",
                "password", null, "Test product code", getTitleIDString(channel + titleID), 
                "Test title", "GAME", "0", new byte[0], 1, 0, 0, contents);
        return req;
    }
    
    protected PublishRequestType initSubscription(long channelID)
    {
        PublishRequestType req = new PublishRequestType(ActionType.Create, "LOCAL_CPS",
                "password", null, "Test product code", getTitleIDString(channelID), "Test title",
                "GAME", "0", new byte[0], 1, 0, 0, null);
        return req;
    }
    
    
    protected UploadResponseType publish(UploadRequestType req, File[] contents, 
                                         int responseCode)
    throws RemoteException
    {
        DataHandler[] hdr = null;
        
        if (contents != null) {
            hdr = new DataHandler[contents.length];
            
            for (int i = 0; i < contents.length; i++) {
                hdr[i] = new DataHandler(new FileDataSource(contents[i]));
            }
        }
        UploadResponseType resp = cps.upload(req, hdr);
        assertEquals(responseCode, resp.getErrorCode());
        return resp;
    }
    
    
    protected void removeTitle(long titleID) throws SQLException
    {
        db.removeTitle(channel + titleID);
        // clear directory
        File file = new File(outDir, Long.toHexString(channel + titleID));
        if (file.exists()) {
            String[] list = file.list();
            for (int i = 0; i < list.length; i++) {
                File f = new File(file, list[i]);
                assertTrue(f.delete());
            }
            assertTrue(file.delete());
        }
    }

    protected File[] getEncryptedFiles(File dir)
    {
        String[] list = dir.list();
        ArrayList files = new ArrayList();
        for (int i = 0; i < list.length; i++) {
            if (! list[i].endsWith(".cry"))
                continue;
            files.add(new File(dir, list[i]));
        }
        return (File[]) files.toArray(new File[files.size()]);
    }

    protected File prepareOutputDir(final long tid)
    {
        File dir = new File(outDir, Long.toHexString(channel + tid));
        assertTrue(dir.mkdir());
        return dir;
    }

    class GenLicense {
        File dir;
        PublishRequestType req;
        UploadRequestType resp;
        File[] outFiles;
        File[] contents;
        boolean isSubscription;
        
        GenLicense(long tid, boolean isSubscription) throws Exception {
            this(tid, 
                 (isSubscription && (tid & 0xFFFFFFFFL) == 0) ? initSubscription(tid) : initRequest(tid), 
                 isSubscription);
        }
        
        GenLicense(long tid, PublishRequestType req, boolean isSubscription) throws Exception {
            this.isSubscription = isSubscription;
            removeTitle(tid);   
            dir = prepareOutputDir(tid);
            this.req = req;
            boolean needContents = (isSubscription && (tid & 0xFFFFFFFFL) == 0);
            contents = needContents ? null : contentFile;
            resp = license.license(needContents? subTmdTemplate : tmdTemplate, req, 
                                   isSubscription, contents, dir, null, channel + tid, 0, null);
            resp.setCommonETicket("Dummy eTicket".getBytes());
            outFiles = getEncryptedFiles(dir);
        }
        
        void renew(UploadRequestType lastPackage) throws Exception {
            long tid = Long.parseLong(req.getTitleID(), 16);
            boolean needContents = (isSubscription && (tid & 0xFFFFFFFFL) == 0);
            resp = license.license(needContents ? subTmdTemplate : tmdTemplate, req, 
                                   isSubscription, contents, dir, null, 
                                   Long.parseLong(req.getTitleID(), 16), 0, lastPackage);
            resp.setCommonETicket("Dummy eTicket".getBytes());
            outFiles = getEncryptedFiles(dir);
        }
    }
}

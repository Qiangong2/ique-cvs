package com.broadon.unitTest.ias;

import com.broadon.wsapi.ias.RegisterDeviceRequestType;
import com.broadon.wsapi.ias.RegisterDeviceResponseType;

public class RegisterDeviceFailure extends TestCaseWrapper {

    public static void main(String[] args) {
    }

    public RegisterDeviceFailure(String name) {
	super(name);
    }

    public void setUp() throws Exception {
	super.setUp();
    }

    public void tearDown() throws Exception {
	super.tearDown();
    }

    private boolean checkRegisterDeviceFailure(
		String deviceId,
                String publicKey,
                String serialNumber,
                RegisterDeviceResponseType expected)
	throws Throwable
    {
	boolean testResult = true;
	
        RegisterDeviceResponseType resp;
        RegisterDeviceRequestType req = new RegisterDeviceRequestType();
        initRequest(req);
	req.setDeviceId(Long.parseLong(deviceId));
        req.setDeviceCert(publicKey.getBytes());
        req.setSerialNumber(serialNumber);
	
        resp = ias.registerDevice(req);
	testResult &= checkDeviceResponse(req, resp);
		
	// Check response against expected response
	testResult &= checkError(expected, resp);

	return testResult;
    }

    public void testRegisterDeviceFailure()
	throws Throwable
    {
        // Case 1: Register an already registered device id        
        String deviceId = TestDataAdmin.getRegisteredDeviceId();
                
        if (deviceId != null) {
            RegisterDeviceResponseType expected = new RegisterDeviceResponseType();
            expected.setDeviceId(Long.parseLong(deviceId));
            expected.setErrorCode(951);
            expected.setErrorMessage("IAS - Device Id is already registered");
            assertTrue(checkRegisterDeviceFailure(deviceId, "pb"+deviceId, "sn"+deviceId, expected));
        } else {
            System.out.println(getName() + " Warning: Failed to obtain an registered device id");
        }
    }    
}

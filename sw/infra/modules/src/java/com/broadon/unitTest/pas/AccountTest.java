
package com.broadon.unitTest.pas;

import java.math.BigDecimal;

import com.broadon.wsapi.pas.AuthorizeAccountPaymentRequestType;
import com.broadon.wsapi.pas.AuthorizeAccountPaymentResponseType;
import com.broadon.wsapi.pas.AuthorizeECardPaymentRequestType;
import com.broadon.wsapi.pas.AuthorizeECardPaymentResponseType;
import com.broadon.wsapi.pas.BalanceAccountType;
import com.broadon.wsapi.pas.BalanceType;
import com.broadon.wsapi.pas.CaptureECardToAccountRequestType;
import com.broadon.wsapi.pas.CaptureECardToAccountResponseType;
import com.broadon.wsapi.pas.CapturePaymentRequestType;
import com.broadon.wsapi.pas.CapturePaymentResponseType;
import com.broadon.wsapi.pas.RefundPaymentRequestType;
import com.broadon.wsapi.pas.RefundPaymentResponseType;
import com.broadon.wsapi.pas.VoidAuthorizationRequestType;
import com.broadon.wsapi.pas.VoidAuthorizationResponseType;
import com.broadon.wsapi.pas.PasTokenType;
import com.broadon.wsapi.pas.MoneyType;

public class AccountTest extends TestCaseWrapper {
    /**
     * misc. tests for pas account related services:
     * create an ecard
     * create acct
     * check acct balance
     * authorize payment from ecard
     * cpature payment to account
     * check acct balance
     * authorize payment from acct
     * check acct balance
     * void auth
     * check balance
     * authorize payment from acct
     * cpature payment
     * check balance
     * refund payment
     * check balance
     * get ecard acct history
     * 
     */
    
    String[] ecards;
    BalanceAccountType acct;
    public static void main(String[] args) {
        junit.textui.TestRunner.run(AccountTest.class);
    }

    public AccountTest(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
        resetTestDataBase(AccountTests);
        // ecardType 11 is 1000 POINTS and reusable
        createECardType(11);
        ecards = createECards(11, 1, "JP");
        acct = createAccount("30001", "30001");
    }

    public void tearDown() throws Exception {
        super.tearDown();
        removeECardType(11);
    }

    // This  method checks if the total and partial difference is diff
    private boolean checkBalance(BalanceType total, BalanceType partial, MoneyType diff) {
        BigDecimal totalAmount = new BigDecimal(total.getAvailable().getAmount());
        BigDecimal partialAmount = new BigDecimal(partial.getAvailable().getAmount());
        BigDecimal diffAmount = new BigDecimal(diff.getAmount());
        if (totalAmount.subtract(partialAmount).compareTo(diffAmount) == 0)
            return true;
        else {
            System.out.println("total: " + totalAmount);
            System.out.println("partial: " + partialAmount);
            System.out.println("diff: " + diffAmount);
            return false;
        }
    }
    
    public boolean checkAccountTest() {
        String eCardNumber = ecards[0];
        MoneyType amount = new MoneyType("POINTS", "150");
        boolean testResult = true;
        try {
            // check balance
            BalanceType initBalance = checkAccountBalance(acct);
            if (initBalance != null) {
                System.out.println("checkECardBalance passed.");
            } else {
                return false;
            }
            // authorize payment from ecard
            AuthorizeECardPaymentRequestType authReq = createAuthorizationRequest(
                    eCardNumber, amount);
            AuthorizeECardPaymentResponseType authResp = pas.authorizeECardPayment(authReq);
            if (checkAbstractResponse(authReq, authResp)) {
                System.out.println("authorizeECardPayment passed.");
            } else {
                return false;
            }
            // cpature payment to account
            PasTokenType token = authResp.getAuthorizationToken();
            CaptureECardToAccountRequestType capReq = createCaptureECardToAccountRequest(
                    token, acct, amount);
            CaptureECardToAccountResponseType capResp = pas.captureECardToAccount(capReq);
            if (checkAbstractResponse(capReq, capResp)) {
                System.out.println("captureECardToAccount passed.");
            } else {
                return false;
            }
            // check acct balance
            BalanceType newBalance = checkAccountBalance(acct);
            // initBalance is 0, newBalance is the amout just captured. 
            if (checkBalance(newBalance, initBalance, amount))
                System.out.println("new balance is correct.");
            else 
                return false;
            // authorize payment from acct
            AuthorizeAccountPaymentRequestType acctAuthReq = createAcctAuthorizationRequest(
                    acct, amount);
            AuthorizeAccountPaymentResponseType acctAuthResp = pas.authorizeAccountPayment(acctAuthReq);
            if (checkAbstractResponse(acctAuthReq, acctAuthResp)) {
                System.out.println("authorizeAccountPayment passed.");
            } else {
                return false;
            }
            token = acctAuthResp.getAuthorizationToken();
            // check acct balance
            initBalance = newBalance;
            newBalance = checkAccountBalance(acct);
            if (checkBalance(initBalance, newBalance, amount))
                System.out.println("new balance is correct.");
            else 
                return false;
            // void auth
            VoidAuthorizationRequestType voidReq = createVoidAuthorizationRequest(token);
            VoidAuthorizationResponseType voidResp = pas.voidAuthorization(voidReq);
            if (checkAbstractResponse(voidReq, voidResp)) {
                System.out.println("voidAuthorization passed.");
            } else {
                return false;
            }
            // check balance
            initBalance = checkAccountBalance(acct);
            if (checkBalance(initBalance, newBalance, amount))
                System.out.println("new balance is correct.");
            else 
                return false;
            // authorize payment
            acctAuthResp = pas.authorizeAccountPayment(acctAuthReq);
            if (checkAbstractResponse(acctAuthReq, acctAuthResp)) {
                System.out.println("authorizeAccountPayment passed.");
            } else {
                return false;
            }
            token = acctAuthResp.getAuthorizationToken();
            // cpature payment
            CapturePaymentRequestType capReq2 = createCapturePaymentRequest(token, amount, false);
            CapturePaymentResponseType capResp2 = pas.capturePayment(capReq2);
            if (checkAbstractResponse(capReq2, capResp2)) {
                System.out.println("capturePayment passed.");
            } else {
                return false;
            }
            // check balance
            newBalance = checkAccountBalance(acct);
            if (checkBalance(initBalance, newBalance, amount))
                System.out.println("new balance is correct.");
            else 
                return false;
            // refund payment
            RefundPaymentRequestType refReq = createRefundPaymentRequest(token, amount);
            RefundPaymentResponseType refResp = pas.refundPayment(refReq);
            if (checkAbstractResponse(refReq, refResp)) {
                System.out.println("refundPayment passed.");
            } else {
                return false;
            }
            // check balance
            initBalance = checkAccountBalance(acct);
            if (checkBalance(initBalance, newBalance, amount))
                System.out.println("new balance is correct.");
            else 
                return false;
            // get account account history
        } catch (Exception e) {
            System.out.println(getStackTrace(e));
            System.out.println(e.getMessage());
            testResult = false;
        }
        return testResult;
    }
    
    public void testMiscAccountTest() {
        assertTrue(checkAccountTest());
    }   
}

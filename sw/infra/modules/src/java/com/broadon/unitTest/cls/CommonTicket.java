package com.broadon.unitTest.cls;

import com.broadon.unitTest.Configuration;
import com.broadon.wsapi.cps.ActionType;
import com.broadon.wsapi.cps.PublishRequestType;

public class CommonTicket extends TestCaseWrapper
{

    public CommonTicket(String name) {
        super(name);
    }

    
    public void testEnableCommonTicket() throws Throwable
    {
        final long tid = 201;

        removeTitle(tid);
        PublishRequestType req = initRequest(tid);
        req.setAllowCommonTicket(Boolean.TRUE);
        GenLicense lic = new GenLicense(tid, req, false);
        publish(lic.resp, lic.outFiles, 0);
        Thread.sleep(1000);
        int attr = db.getETKMAttributes(channel + tid);
        assertEquals(1, attr);
    }
    
    
    public void testUpdateCommonTicket() throws Throwable
    {
        final long tid = 202;
        
        removeTitle(tid);
        // publish a non-common ticket title
        GenLicense lic = new GenLicense(tid, false);
        publish(lic.resp, lic.outFiles, 0);
        
         // update to allow common ticket
        lic.req.setAllowCommonTicket(Boolean.TRUE);
        lic.req.setAction(ActionType.Update);
        Thread.sleep(1000);
        lic.renew(lic.resp);
        publish(lic.resp, lic.outFiles, 0);
        assertEquals(1, db.getETKMAttributes(channel + tid));
        
        // update to clear common ticket enabling flag
        lic.req.setAllowCommonTicket(Boolean.FALSE);
        Thread.sleep(1000);
        lic.renew(lic.resp);
        publish(lic.resp, lic.outFiles, 0);
        assertEquals(0, db.getETKMAttributes(channel + tid));
    }
    
    /**
     * @param args
     */
    public static void main(String[] args)
    {
        if (args != null && args.length > 0)
            hostName = args[0];
        
        Configuration.testLab1();
        junit.textui.TestRunner.run(CommonTicket.class);
    }

}

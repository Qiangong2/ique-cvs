package com.broadon.unitTest.ias;

import com.broadon.wsapi.ias.GetDeviceInfoRequestType;
import com.broadon.wsapi.ias.GetDeviceInfoResponseType;

public class GetDeviceInfoFailure extends TestCaseWrapper {

    public static void main(String[] args) {
    }

    public GetDeviceInfoFailure(String name) {
	super(name);
    }

    public void setUp() throws Exception {
	super.setUp();
    }

    public void tearDown() throws Exception {
	super.tearDown();
    }
    
    private boolean checkGetDeviceInfoFailure (
		String deviceId,
                GetDeviceInfoResponseType expected)
	throws Throwable
    {
	boolean testResult = true;

        GetDeviceInfoResponseType resp;
        GetDeviceInfoRequestType req = new GetDeviceInfoRequestType();
        
        initRequest(req);
	req.setDeviceId(Long.parseLong(deviceId));
		
	resp = ias.getDeviceInfo(req);
	testResult &= checkDeviceResponse(req, resp);
		
	// Check response against expected response
	testResult &= checkError(expected, resp);
        
	return testResult;
    }

    public void testGetDeviceInfoFailure()
	throws Throwable
    {
        // Case 1: Device with expired token and with a subscription
        String deviceId = TestDataAdmin.getUnRegisteredDeviceId();
        
        if (deviceId != null) {
            //System.out.println("1. deviceId: " + deviceId +", token: " + token);
            
            GetDeviceInfoResponseType expected = new GetDeviceInfoResponseType();
            expected.setDeviceId(Long.parseLong(deviceId));
            expected.setErrorCode(901);
            expected.setErrorMessage("IAS - Device Id is not registered");
            
            assertTrue(checkGetDeviceInfoFailure(deviceId, expected));
        } else {
            System.out.println(getName() + " Warning: Failed to obtain a device id with an expired token and a subscription");
        }
    }    
}

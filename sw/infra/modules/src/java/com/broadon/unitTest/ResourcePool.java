/* Resource Pooling
 * 
 *  - It uses port 8088 instead of 8080, assuming TCPMON is running
 *  - You can start TCPMON using the shell script wstest/tools/tcpmon
 *
 *  TODO: implement real resource pool for stress testing using jmeter
 *  Currently it is a single instance SOAP Stub.
 */
package com.broadon.unitTest;

import java.net.URL;

import com.broadon.wsapi.cps.PublishPortType;
import com.broadon.wsapi.cps.PublishServiceLocator;
import com.broadon.wsapi.ecs.ECommercePortType;
import com.broadon.wsapi.ecs.ECommerceServiceLocator;
import com.broadon.wsapi.ets.ETicketPortType;
import com.broadon.wsapi.ets.ETicketServiceLocator;
import com.broadon.wsapi.ias.IdentityAuthenticationPortType;
import com.broadon.wsapi.ias.IdentityAuthenticationServiceLocator;
import com.broadon.wsapi.nus.NetUpdatePortType;
import com.broadon.wsapi.nus.NetUpdateServiceLocator;
import com.broadon.wsapi.ogs.OnlineGamePortType;
import com.broadon.wsapi.ogs.OnlineGameServiceLocator;
import com.broadon.wsapi.pas.PaymentAuthorizationPortType;
import com.broadon.wsapi.pas.PaymentAuthorizationServiceLocator;
import com.broadon.wsapi.pcis.PrepaidCardGenerationPortType;
import com.broadon.wsapi.pcis.PrepaidCardGenerationServiceLocator;

public class ResourcePool {

    static public ECommercePortType newECommerceService() {
        ECommercePortType ecs = null;
        try {
            URL endpointURL;
            if (Configuration.getSecurity() == Configuration.SSL) {
                endpointURL = new URL(
                    "https://" + 
                    Configuration.getSvcHost("ecs") + 
                    ":" + 
                    Configuration.getEcsSSLPort() +
                    "/ecs/services/ECommerceSOAP");
            } else {
                endpointURL = new URL(
                    "http://" + 
                    Configuration.getSvcHost("ecs") + 
                    ":" + 
                    Configuration.getEcsPort() +
                    "/ecs/services/ECommerceSOAP");
            }
            System.out.println();
            System.out.println(endpointURL);
            ECommerceServiceLocator ecsService = new ECommerceServiceLocator();
            ecs = ecsService.getECommerceSOAP(endpointURL);
        } catch (Exception e) {
            System.out.println("ResourcePool: cannot connect to ecs services.");
            System.out.println(e.getMessage());
        }
        return ecs;
    }
        
    static public ETicketPortType newETicketService() {
        ETicketPortType ets = null;
        try {
            URL endpointURL = new URL(
                                      "http://" + 
                                      Configuration.getSvcHost("ets") + 
                                      ":" + 
                                      Configuration.getEtsPort() +
                                      "/ets/services/ETicketSOAP");
            System.out.println();
            System.out.println(endpointURL);
            ETicketServiceLocator etsService = new ETicketServiceLocator();
            ets = etsService.getETicketSOAP(endpointURL);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
                
        return ets;
    }
        
    static public IdentityAuthenticationPortType newIdentityAuthenticationService() {
        IdentityAuthenticationPortType ias = null;
        try {
            URL endpointURL = new URL(
                                      "http://" + 
                                      Configuration.getSvcHost("ias") + 
                                      ":" + 
                                      Configuration.getIasPort() +
                                      "/ias/services/IdentityAuthenticationSOAP");
            System.out.println();
            System.out.println(endpointURL);
            IdentityAuthenticationServiceLocator iasService = new IdentityAuthenticationServiceLocator();
            ias = iasService.getIdentityAuthenticationSOAP(endpointURL);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
                
        return ias;
    }

    static public PublishPortType newPublishService() {
        PublishPortType cps = null;
        try {
            URL endpointURL = new URL(
                                      "http://" + 
                                      Configuration.getSvcHost("cps") + 
                                      ":" + 
                                      Configuration.getCpsPort() +
                                      "/cps/services/PublishSOAP");
            System.out.println();
            System.out.println(endpointURL);
            PublishServiceLocator cpsService = new PublishServiceLocator();
            cps = cpsService.getPublishSOAP(endpointURL);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        
        return cps;
    }
    
    static public PaymentAuthorizationPortType newPaymentAuthorizationService() {
        PaymentAuthorizationPortType pas = null;
        try {
            URL endpointURL = new URL(
                                     "http://" + 
                                     Configuration.getSvcHost("pas") + 
                                     ":" + 
                                     Configuration.getPasPort() +
                                     "/pas/services/PaymentAuthorizationSOAP");
            System.out.println();
            System.out.println(endpointURL);
            PaymentAuthorizationServiceLocator pasService = new PaymentAuthorizationServiceLocator();
            pas = pasService.getPaymentAuthorizationSOAP(endpointURL);
        } catch (Exception e) {
            System.out.println("ResourcePool: cannot connect to pas services.");
            System.out.println(e.getMessage());
        }
        return pas;
    }
    
    static public OnlineGamePortType newOnlineGameService() {
        OnlineGamePortType ogs = null;
        try {
            URL endpointURL = new URL(
                                      "http://" + 
                                      Configuration.getSvcHost("ogs") + 
                                      ":" + 
                                      Configuration.getOgsPort() +
                                      "/ogs/services/OnlineGameSOAP");
            System.out.println();
            System.out.println(endpointURL);
            OnlineGameServiceLocator ogsService = new OnlineGameServiceLocator();
            ogs = ogsService.getOnlineGameSOAP(endpointURL);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
                
        return ogs;
    }
    
    static public PrepaidCardGenerationPortType newPrepaidCardGenerationService() {
        PrepaidCardGenerationPortType pcis = null;
        try {
            URL endpointURL = new URL(
                                     "http://" + 
                                     Configuration.getSvcHost("pcis") + 
                                     ":" + 
                                     Configuration.getPcisPort() +
                                     "/pcis/services/PrepaidCardGenerationSOAP");
            System.out.println();
            System.out.println(endpointURL);
            PrepaidCardGenerationServiceLocator pcisService = new PrepaidCardGenerationServiceLocator();
            pcis = pcisService.getPrepaidCardGenerationSOAP(endpointURL);
        } catch (Exception e) {
            System.out.println("ResourcePool: cannot connect to pcis services.");
            System.out.println(e.getMessage());
        }
        return pcis;
    }
    
    static public NetUpdatePortType newNetUpdateService() {
        NetUpdatePortType nus = null;
        try {
            URL endpointURL = new URL(
                                      "http://" + 
                                      Configuration.getSvcHost("nus") + 
                                      ":" + 
                                      Configuration.getNusPort() +
                                      "/nus/services/NetUpdateSOAP");
            System.out.println();
            System.out.println(endpointURL);
            NetUpdateServiceLocator nusService = new NetUpdateServiceLocator();
            nus = nusService.getNetUpdateSOAP(endpointURL);
        } catch (Exception e) {
            System.out.println("ResourcePool: cannot connect to nus services.");
            System.out.println(e.getMessage());
        }
        return nus;
    }
    static public String getVCPaymentURL() {
        String urlStr = "http://" +
        Configuration.getSvcHost("ccsimu") +
        ":" +
        Configuration.getCcsimuPort() +
        "/ccsimu/services/VCPayment";
        System.out.println();
        System.out.println(urlStr);
        return urlStr;
    }

    static public String getPointsConfirmationURL() {
        String urlStr = "http://" +
        Configuration.getSvcHost("ccsimu") +
        ":" +
        Configuration.getCcsimuPort() +
        "/ccsimu/services/PointsConfirmation";
        System.out.println();
        System.out.println(urlStr);
        return urlStr;
    }

    static public String getVCRefundURL() {
        String urlStr = "http://" +
        Configuration.getSvcHost("ccsimu") +
        ":" +
        Configuration.getCcsimuPort() +
        "/ccsimu/services/VCRefund";
        System.out.println();
        System.out.println(urlStr);
        return urlStr;
    }
}

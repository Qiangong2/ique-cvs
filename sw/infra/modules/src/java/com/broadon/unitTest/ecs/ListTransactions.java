package com.broadon.unitTest.ecs;

import java.util.HashSet;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;

import com.broadon.wsapi.ecs.AccountPaymentType;
import com.broadon.wsapi.ecs.ListTransactionsRequestType;
import com.broadon.wsapi.ecs.ListTransactionsResponseType;
import com.broadon.wsapi.ecs.MoneyType;
import com.broadon.wsapi.ecs.PricingCategoryType;
import com.broadon.wsapi.ecs.TransactionType;

public class ListTransactions extends TestCaseWrapper {
    
    // Some example transactions to insert into the DB
    final static TransactionInfo[] sampleTransactions = new TransactionInfo[] {
        new TransactionInfo(30102, 10376, "DEPOSIT"),
        new TransactionInfo(30102, 10377, "DEPOSIT")
    };

    private static class TransactionInfo {
        long accountId;
        long transactionId;
        String transType;

        private TransactionInfo(long accountId, long transactionId, String transType)
        {
            this.accountId = accountId;
            this.transactionId = transactionId;
            this.transType = transType;
        }   
    }
    
    public static void main(String[] args) {
        junit.textui.TestRunner.run(ListTransactions.class);
    }

    public ListTransactions(String name) {
        super(name);
        createTransaction("30102", "10376", "DEPOSIT");
        createTransaction("30102", "10377", "DEPOSIT");
    }

    public void setUp() throws Exception {
        super.setUp();
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }
    
    // Returns the number of times the given transactions occurs in the list of transactions
    public int findOccurances(TransactionType[] transactions, TransactionInfo transactionInfo)
    {
        if (transactions == null) {
            return 0;
        }
        int count = 0;
        for (int i = 0; i < transactions.length; i++) {
            if (transactionInfo.transactionId == transactions[i].getTransactionId()) {
                assertEquals("Transaction " + transactionInfo.transactionId + " trans id",
                        transactionInfo.transactionId, transactions[i].getTransactionId());
                assertEquals("Transaction " + transactionInfo.transactionId + " trans type",
                        transactionInfo.transType, transactions[i].getType());
                count++;
            }
        }
        return count;
    }
    
    private boolean checkListTransactions(
            long accountId,
            ListTransactionsResponseType expected)
        throws Throwable
    {
        boolean testResult = true;

        ListTransactionsResponseType resp;
        ListTransactionsRequestType req = new ListTransactionsRequestType();

        initAbstractRequest(req);
        AccountPaymentType acct = new AccountPaymentType();
        acct.setAccountNumber(String.valueOf(accountId));
        acct.setPin(String.valueOf(accountId));
        req.setAccount(acct);

        resp = ecs.listTransactions(req);
        testResult &= checkAbstractResponse(req, resp);
        if (expected != null) {
            testResult &= checkError(expected, resp);
            assertTrue("getTransactions", 
                    Arrays.equals(expected.getTransactions(), resp.getTransactions()));          
        } else {
            assertEquals("getErrorCode", 0, resp.getErrorCode());
            assertEquals("getErrorMessage", null, resp.getErrorMessage());
            
            for (int i = 0; i < sampleTransactions.length; i++) {
                TransactionInfo transactionInfo = sampleTransactions[i];
                int occurs = findOccurances(resp.getTransactions(), transactionInfo);
                if (accountId == transactionInfo.accountId) {
                    // Expect this transaction to be in list of transactions
                    assertEquals("Transaction " + transactionInfo.transactionId + " occurs", 1, occurs);
                } else {
                    // Expect this transaction not to be in list of transactions
                    assertEquals("Transaction " + transactionInfo.transactionId + " occurs", 0, occurs);
                }
            }           
        }

        // Check balance
        MoneyType balance = new MoneyType();
        balance.setAmount(START_BALANCE);
        balance.setCurrency("POINTS");
        //testResult &= checkBalance("getBalance", balance, resp.getBalance());
        return testResult;
    }

    
    public void testListTransactions()
        throws Throwable
    {
        createAccount("30102");
        assertTrue(checkListTransactions(30102, null));
    }
    
}

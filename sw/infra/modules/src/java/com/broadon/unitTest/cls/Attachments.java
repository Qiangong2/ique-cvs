package com.broadon.unitTest.cls;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Random;

import com.broadon.exception.BroadOnException;
import com.broadon.unitTest.Configuration;
import com.broadon.wsapi.cps.ContentAttributeType;
import com.broadon.wsapi.cps.PublishRequestType;
import com.broadon.wsapi.cps.UploadRequestType;

public class Attachments extends TestCaseWrapper
{
    public Attachments(String name) {
        super(name);
    }
   
    class Request {
        PublishRequestType req;
        File[] files;;
        
        Request(File[] files, PublishRequestType req) {
            this.files = files;
            this.req = req;
        }
    }
    
    
    public Request multiContentRequests(long tid, int num, File dir) throws IOException 
    {
        PublishRequestType req = initRequest(tid);
        ContentAttributeType content = req.getContents(0);
        ContentAttributeType[] contents = new ContentAttributeType[num];
        File[] attachments = new File[num];
        Random rand = new Random();
        byte[] buf = new byte[64];
        for (int i = 0; i < num; ++i) {
            contents[i] = content;
            contents[i].setContentObjectName(content.getContentObjectName() + ' ' + i);
            rand.nextBytes(buf);
            File tempFile = File.createTempFile("CLS", null, dir);
            tempFile.deleteOnExit();
            FileOutputStream out = new FileOutputStream(tempFile);
            out.write(buf);
            out.close();
            attachments[i] = tempFile;
        }
        req.setContents(contents);
        return new Request(attachments, req);
    }
    
    public void testMissingAttachment() throws Throwable
    {
        final long tid = 301;
        removeTitle(tid);
        File dir = prepareOutputDir(tid);
        PublishRequestType req = initRequest(tid);
        try {
            license.license(tmdTemplate, req, false, null, dir, null, channel + tid, 0, null);
        } catch (BroadOnException e) {
            assertTrue(e.getMessage().indexOf("attachment mismatched") >= 0);
            UploadRequestType resp = license.license(tmdTemplate, req, false, contentFile, dir, null, channel + tid, 0, null);
            publish(resp, null, 1004);
            return;
        }
        fail();
    }
    
    public void testMultipleAttachments() throws Throwable
    {
        final long tid = 302;
        removeTitle(tid);
        File dir = prepareOutputDir(tid);
        Request req = multiContentRequests(tid, 2, dir);
        UploadRequestType resp = license.license(tmdTemplate, req.req, false, req.files, dir, null, channel + tid, 0, null);
        // kludge: instead of parsing the TMD, we just check its length, which
        // varies according to the number of contents.
        assertEquals(556, resp.getTitleMetaData().length);
    }
    
    
    public void testLargeAttachments() throws Throwable
    {
        final long tid = 303;
        removeTitle(tid);
        File dir = prepareOutputDir(tid);
        PublishRequestType req = initRequest(tid);
        File[] file = new File[1];
        file[0] = File.createTempFile("CLS", null, dir);
        file[0].deleteOnExit();
        byte[] buf = new byte[64*1024];
        Arrays.fill(buf, (byte) 0);
        FileOutputStream out = new FileOutputStream(file[0]);
        for (int i = 0; i < 16; ++i) 
            out.write(buf);
        out.close();
        
        UploadRequestType resp = license.license(tmdTemplate, req, false, file, dir, null, channel + tid, 0, null);
        publish(resp, getEncryptedFiles(dir), 0);
    }
    
    /**
     * @param args
     */
    public static void main(String[] args)
    {
        if (args != null && args.length > 0)
            hostName = args[0];
        
        Configuration.testLab1();
        junit.textui.TestRunner.run(Attachments.class);
    }

}

package com.broadon.unitTest.cls;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import com.broadon.exception.BroadOnException;
import com.broadon.unitTest.Configuration;
import com.broadon.util.HexString;
import com.broadon.wsapi.cps.ActionType;
import com.broadon.wsapi.cps.ContentLinkType;
import com.broadon.wsapi.cps.PublishRequestType;
import com.broadon.wsapi.cps.UploadRequestType;

public class CreateRegularTitle extends TestCaseWrapper
{
    public CreateRegularTitle(String name) {
        super(name);
    }
  

    private void checkTMD(PublishRequestType req, byte[] titleMetaData) throws NoSuchAlgorithmException, IOException
    {
        MessageDigest sha = MessageDigest.getInstance("SHA");
        InputStream in = new FileInputStream(contentFile[0]);
        byte[] buf = new byte[1024];
        int n;
        while ((n = in.read(buf)) > 0) {
            sha.update(buf, 0, n);
        }
        in.close();
        byte[] reqHash = sha.digest();
        byte[] respHash = new byte[reqHash.length];
        System.arraycopy(titleMetaData, 500, respHash, 0, respHash.length);
        assertEquals(true, Arrays.equals(reqHash, respHash));   
    }
    
    
    private void checkEncryptedFile(UploadRequestType resp, File[] files) throws NoSuchAlgorithmException, IOException
    {
        ContentLinkType[] contentLink = resp.getContentLink();
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] buf = new byte[64*1024];
        int n;
        nextFile:
        for (int i = 0; i < files.length; i++) {
            md.reset();
            FileInputStream in = new FileInputStream(files[i]);
            while ((n = in.read(buf)) > 0)
                md.update(buf, 0, n);
            in.close();
            String checksum = HexString.toHexString(md.digest());
            for (int j = 0; j < resp.getContentLink().length; j++) {
                if (contentLink[j].getCheckSum().equals(checksum))
                    continue nextFile;
            }
            fail("Encrypted file " + files[i].getPath() + " checksum mismatched");
        }
    }

    public void testCreateRegularTitle() throws Throwable
    {
        final long tid = 1;
        GenLicense lic = new GenLicense(tid, false);
        checkTMD(lic.req, lic.resp.getTitleMetaData());
        checkEncryptedFile(lic.resp, lic.outFiles);
       
        
        // test serialized I/O
        File uploadPackage = new File(lic.dir, "upload.pkg");
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(uploadPackage));
        out.writeObject(lic.resp);
        out.close();
        
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(uploadPackage));
        UploadRequestType uploadRequest = (UploadRequestType) in.readObject();
        assertEquals(lic.resp, uploadRequest);
        
        // publish
        publish(uploadRequest, lic.outFiles, 0);
    }


    public void testCreateDuplicateTitle() throws Throwable
    {
        final long tid = 2;
        GenLicense lic = new GenLicense(tid, false);
        publish(lic.resp, lic.outFiles, 0);
        
        // publish the same version again
        publish(lic.resp, lic.outFiles, 1011);
    }


    public void testSameContentWithDifferentTitleID() throws Throwable
    {
        GenLicense lic1 = new GenLicense(3, false);
        publish(lic1.resp, lic1.outFiles, 0);
        
        GenLicense lic2 = new GenLicense(4, false);
        publish(lic2.resp, lic2.outFiles, 0);
    }
    
    public void testUpdateTitle() throws Throwable
    {
        final long tid = 5;
        GenLicense lic = new GenLicense(tid, false);
        publish(lic.resp, lic.outFiles, 0);
        
        // to avoid unique contraint violation on the content title object table
        // with multiple entries within the same second.
        Thread.sleep(1000);
        lic.req.setAction(ActionType.Update);
        lic.renew(lic.resp);
        publish(lic.resp, lic.outFiles, 0);
        // TODO:  check if the records are actually updated (need TMD back).
    }
    
    public void testUpdateTitleWrongPassword() throws Throwable
    {
        final long tid = 6;
        GenLicense lic = new GenLicense(tid, false);
        publish(lic.resp, lic.outFiles, 0);
        
        lic.req.setAction(ActionType.Update);
        lic.req.setEncryptionPassword("wrong password");
        lic.renew(lic.resp);
        publish(lic.resp, lic.outFiles, 1001);
    }
    
    public void testUpdateNonExistTitle() throws Throwable
    {
        final long tid = 7;

        PublishRequestType req = initRequest(tid);
        req.setAction(ActionType.Update);
        
        try {
            new GenLicense(tid, req, false);
        } catch (BroadOnException e) {
            assertTrue(e.getMessage().indexOf("requires last upload") >= 0);
            return;
        }
        fail();
    }

        
    public static void main(String[] args) {
        if (args != null && args.length > 0)
            hostName = args[0];
        
        Configuration.testLab1();
        junit.textui.TestRunner.run(CreateRegularTitle.class);
    }


}

package com.broadon.unitTest.ias;

import com.broadon.wsapi.ias.GetDeviceInfoRequestType;
import com.broadon.wsapi.ias.GetDeviceInfoResponseType;
import com.broadon.wsapi.ias.DeviceInfoType;
import com.broadon.wsapi.ias.DeviceSubscriptionType;

public class GetDeviceInfoSuccess extends TestCaseWrapper {

    public static void main(String[] args) {
    }

    public GetDeviceInfoSuccess(String name) {
	super(name);
    }

    public void setUp() throws Exception {
	super.setUp();
    }

    public void tearDown() throws Exception {
	super.tearDown();
    }
    
    private boolean checkGetDeviceInfoSuccess (
		String deviceId,
                String tokenId,
                int subCount,
                GetDeviceInfoResponseType expected)
	throws Throwable
    {
	boolean testResult = true;

        GetDeviceInfoResponseType resp;
        GetDeviceInfoRequestType req = new GetDeviceInfoRequestType();
        
        initRequest(req);
	req.setDeviceId(Long.parseLong(deviceId));
        req.setTokenId(tokenId);
		
	resp = ias.getDeviceInfo(req);
	testResult &= checkDeviceResponse(req, resp);
		
	// Check response against expected response
	testResult &= checkError(expected, resp);
        
        DeviceInfoType dInfo = resp.getDeviceInfo();
        DeviceSubscriptionType[] subType = resp.getDeviceSubscriptions();
        
        // Check for device info
        assertTrue(dInfo.getPublicKey().equals("pb"+deviceId));
        assertTrue(dInfo.getSerialNumber().equals("sn"+deviceId));
        
        // Check for number of subscriptions
        if (subType != null)
            assertTrue(subType.length == subCount);
        else
            assertTrue(subCount == 0);
                
	return testResult;
    }

    public void testGetDeviceInfoSuccess()
	throws Throwable
    {
        // Case 1: Device with no existing token and with a subscription
        String deviceId = TestDataAdmin.getDeviceIdWithNoTokenAndSubscription(true);
        
        if (deviceId != null) {
            int subCount = TestDataAdmin.getDeviceSubscriptions(deviceId).length;
            //System.out.println("1. deviceId: " + deviceId +", token: null, subCount: " + subCount);
            
            GetDeviceInfoResponseType expected = new GetDeviceInfoResponseType();
            expected.setDeviceId(Long.parseLong(deviceId));
            expected.setErrorCode(000);
            expected.setErrorMessage("");
            
            assertTrue(checkGetDeviceInfoSuccess(deviceId, null, subCount, expected));
        } else {
            System.out.println(getName() + " Warning: Failed to obtain a device id with no existing token and a subscription");
        }
        
        
        // Case 2: Device with no existing token and no subscription
        deviceId = TestDataAdmin.getDeviceIdWithNoTokenAndSubscription(false);
        
        if (deviceId != null) {
            //System.out.println("2. deviceId: " + deviceId + ", token: null, subCount: 0");
            GetDeviceInfoResponseType expected = new GetDeviceInfoResponseType();
            expected.setDeviceId(Long.parseLong(deviceId));
            expected.setErrorCode(000);
            expected.setErrorMessage("");
            
            assertTrue(checkGetDeviceInfoSuccess(deviceId, null, 0, expected));
        } else {
            System.out.println(getName() + " Warning: Failed to obtain a device id with no exiting token and no subscription");
        }
        
        // Case 3: Device with valid token and with a subscription
        deviceId = TestDataAdmin.getDeviceIdWithValidTokenAndSubscription(true);
        
        if (deviceId != null) {
            String token = TestDataAdmin.getValidDeviceToken(deviceId);
            int subCount = TestDataAdmin.getDeviceSubscriptions(deviceId).length;
            //System.out.println("3. deviceId: " + deviceId +", token: " + token + ", subCount: " + subCount);
            
            GetDeviceInfoResponseType expected = new GetDeviceInfoResponseType();
            expected.setDeviceId(Long.parseLong(deviceId));
            expected.setErrorCode(000);
            expected.setErrorMessage("");
            
            assertTrue(checkGetDeviceInfoSuccess(deviceId, token, subCount, expected));
        } else {
            System.out.println(getName() + " Warning: Failed to obtain a device id with valid token and a subscription");
        }
        
        // Case 4: Device with valid token and no subscription
        deviceId = TestDataAdmin.getDeviceIdWithValidTokenAndSubscription(false);
        
        if (deviceId != null) {
            String token = TestDataAdmin.getValidDeviceToken(deviceId);
            //System.out.println("4. deviceId: " + deviceId +", token: " + token + ", subCount: 0");
            
            GetDeviceInfoResponseType expected = new GetDeviceInfoResponseType();
            expected.setDeviceId(Long.parseLong(deviceId));
            expected.setErrorCode(000);
            expected.setErrorMessage("");
            
            assertTrue(checkGetDeviceInfoSuccess(deviceId, token, 0, expected));
        } else {
            System.out.println(getName() + " Warning: Failed to obtain a device id with valid token and a subscription");
        }
    }    
}

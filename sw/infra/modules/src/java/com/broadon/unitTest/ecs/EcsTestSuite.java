/*
 * ECS TestSuite:  Add new ECS TestCase here to this class.
 */
package com.broadon.unitTest.ecs;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.broadon.unitTest.Configuration;

public class EcsTestSuite {

    public static void main(String[] args) {
        junit.textui.TestRunner.run(EcsTestSuite.suite());
    }

    public static Test suite() {
        TestSuite suite = new TestSuite("Test for com.broadon.unitTest.ecs");
        //$JUnit-BEGIN$

        if (Configuration.getProduct() == Configuration.NC) {
        suite.addTestSuite(GetMeta.class);
        suite.addTestSuite(ListSubscriptionPricings.class);
        suite.addTestSuite(ListTitles.class);
        suite.addTestSuite(GetTitleDetails.class);
        suite.addTestSuite(UpdateStatus.class);
        suite.addTestSuite(Subscribe.class);
        suite.addTestSuite(RedeemSubscriptionECard.class);
        suite.addTestSuite(RedeemTitleECard.class);
        }

        suite.addTestSuite(GetSystemUpdate.class);
        suite.addTestSuite(GetApplicationUpdate.class);
        suite.addTestSuite(SyncETickets.class);
        suite.addTestSuite(PurchaseTitle.class);
        suite.addTestSuite(RedeemECard.class);
        suite.addTestSuite(CheckAccountBalance.class);
        suite.addTestSuite(CheckECardBalance.class);
        suite.addTestSuite(PurchasePoints.class);
        suite.addTestSuite(ListETickets.class);
        suite.addTestSuite(ListTransactions.class);
        suite.addTestSuite(DeleteETickets.class);
        suite.addTestSuite(TransferETickets.class);
        //$JUnit-END$
        return suite;
    }

}

package com.broadon.unitTest.ets;

import java.io.IOException;
import java.sql.SQLException;

import com.broadon.unitTest.Configuration;
import com.broadon.wsapi.ets.GetTicketsRequestType;
import com.broadon.wsapi.ets.GetTicketsResponseType;
import com.broadon.wsapi.ets.TicketsRequestType;


public class GetTickets extends TestCaseWrapper 
{
    public GetTickets(String name) throws SQLException, IOException {
        super(name);
    }
    
    private GetTicketsRequestType initRequest(long titleID, String cid) 
    {
        TicketsRequestType[] ticks = new TicketsRequestType[1];
        ticks[0] = new TicketsRequestType(Long.toHexString(titleID), 0, cid,
                                          contentMask, null, "GAME", null);
        GetTicketsRequestType req = new GetTicketsRequestType(null, ticks, null, "LOCAL_ECS");
        req.setVersion("1");
        req.setMessageId("1");
        req.setDeviceId("0");
        return req;
    }
    
    public void testCommonTicket() throws Throwable
    {
        GetTicketsRequestType req = initRequest(COMMON_TITLE, commonETKM);
        req.getTicketsRequest()[0].setCommonTicket(Boolean.TRUE);
        GetTicketsResponseType resp = ets.getTickets(req);
        assertEquals(0, resp.getErrorCode());
    }
    
    public void testBadCommonTicketRequest() throws Throwable
    {
        GetTicketsRequestType req = initRequest(REGULAR_TITLE, regularETKM);
        req.getTicketsRequest(0).setCommonTicket(Boolean.TRUE);
        GetTicketsResponseType resp = ets.getTickets(req);
        assertEquals(301, resp.getErrorCode());
    }

    public static void main(String args[]) {
        if (args != null && args.length > 0)
            hostName = args[0];
    
        Configuration.testLab1();
        junit.textui.TestRunner.run(GetTickets.class);
}
}

package com.broadon.unitTest.pas;

import com.broadon.wsapi.pas.AuthorizeECardPaymentRequestType;
import com.broadon.wsapi.pas.AuthorizeECardPaymentResponseType;
import com.broadon.wsapi.pas.CapturePaymentRequestType;
import com.broadon.wsapi.pas.CapturePaymentResponseType;
import com.broadon.wsapi.pas.MoneyType;
import com.broadon.wsapi.pas.PasTokenType;
import com.broadon.wsapi.pas.RefundPaymentRequestType;
import com.broadon.wsapi.pas.RefundPaymentResponseType;
import com.broadon.wsapi.pas.VoidAuthorizationRequestType;
import com.broadon.wsapi.pas.VoidAuthorizationResponseType;
import com.broadon.wsapi.pas.GetAuthorizationHistoryRequestType;
import com.broadon.wsapi.pas.GetAuthorizationHistoryResponseType;

public class GetAuthorizationHistory extends TestCaseWrapper {
    PasTokenType token;
    public static void main(String[] args) {
        junit.textui.TestRunner.run(GetAuthorizationHistory.class);
    }

    public GetAuthorizationHistory(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
        resetTestDataBase(ECardTests);
        AuthorizeECardPaymentResponseType authResp;
        CapturePaymentResponseType capResp;
        MoneyType amount = new MoneyType("POINTS", "20");
//      from batch03:    000101 0010000000 0014264647    
        String eCardSequence = "0010000000";
        AuthorizeECardPaymentRequestType authReq = createAuthorizationRequest(eCardSequence, amount);

        try {
            authResp = pas.authorizeECardPayment(authReq);
            boolean authReturn = checkAbstractResponse(authReq, authResp);
            if (authReturn) {   // no error, go ahead to capture some money
                token = authResp.getAuthorizationToken();
                amount = new MoneyType("POINTS", "5.5");
                CapturePaymentRequestType capReq = createCapturePaymentRequest(token, amount, false);
                capReq.setAuthorizationToken(token);
                capResp = pas.capturePayment(capReq);
            }
            if (token != null) {
                amount = new MoneyType("POINTS", "5.5");
                RefundPaymentRequestType refundReq = createRefundPaymentRequest(token, amount);
                try {
                    RefundPaymentResponseType refundResp;
                    refundResp = pas.refundPayment(refundReq);
                } catch (Exception e) {
                    System.out.println(getStackTrace(e));
                    System.out.println(e.getMessage());
                }
            }
            if (token != null) {
                VoidAuthorizationRequestType voidReq = createVoidAuthorizationRequest(token);
                VoidAuthorizationResponseType voidResp = pas.voidAuthorization(voidReq);
            }
        } catch (Exception e) {
            System.out.println(getStackTrace(e));
            System.out.println(e.getMessage());
        }
    }

    public void tearDown() throws Exception {
        super.tearDown();
        /*
        if (token != null) {
            VoidAuthorizationRequestType voidReq = new VoidAuthorizationRequestType();
            voidReq.setAuthorizationToken(token);
            initAbstractRequest(voidReq);
            VoidAuthorizationResponseType voidResp = pas.voidAuthorization(voidReq);
        }
        */
    }

    private boolean getAuthorizationHistory() {
        boolean testResult = true;
        if (token != null) {
            GetAuthorizationHistoryRequestType getReq = createGetAuthorizationHistoryRequest(token);
            try {
                GetAuthorizationHistoryResponseType getResp;
                getResp = pas.getAuthorizationHistory(getReq);
                testResult &= checkAbstractResponse(getReq, getResp);
                /*
                if (testResult) {
                    AuthRecordType[] authRecords = getResp.getAuthRecords();
                    for (int i=0; i<authRecords.length; i++) {
                        AuthorizationStateType state = authRecords[i].getState();
                        if (state != null)
                            System.out.print(authRecords[i].getState().getStatus().getValue());
                        else
                            System.out.print("         ");
                        ActionRecordType action = authRecords[i].getAction();
                        if (action != null) {
                            System.out.print("  ");
                            System.out.print(action.getActionType().toString());
                            System.out.print("  ");
                            System.out.print(action.getActionAmount());
                        }
                        System.out.println();
                    }
                }
                */
                
            } catch (Exception e) {
                System.out.println(getStackTrace(e));
                System.out.println(e.getMessage());
                testResult = false;
            }
        }
        return testResult;
        
    }

    public void testGetAuthorizationHistory() {
        assertTrue(getAuthorizationHistory());
    }  
}

/*
 * JUnit TestCase wrapper for BMS
 *   - this class contains functions that should be shared among BMS
 *     test cases.
 */
package com.broadon.unitTest.bms;


import java.sql.Connection;
import java.sql.SQLException;

import junit.framework.*;

import com.broadon.unitTest.Configuration;
import com.broadon.unitTest.TestLog;
import com.meterware.httpunit.*;


public class TestCaseWrapper extends TestCase
{
    /** TestServer instance */
    protected TestServer server;
    /** Connection object to the target server: holds the login cookie. */
    protected WebConversation wc;
    /** Database connection for data verification. */
    protected Connection db;
    /** Points to the home page. */
    protected WebRequest home;
    /** Points to the logout link. */
    protected WebRequest logout;
    
    protected boolean tracing = false;
    protected String logout_id = "logout";
    protected static String hostName = null;
        
    public TestCaseWrapper (String name) {
	super(name);
	wc = null;
	db = null;
	home = null;
	logout = null;
        
        // BMS Parameters
        String web_user = "admin";
        String web_pswd = "admin";
        
        // DB Parameters
        String db_user = "bms";
        String db_pswd = "bms";
        String db_url = Configuration.getDbUrl();
        
        try {       
            TestServer.init("http://"+Configuration.getSvcHost("bms"),
                            web_user,
                            web_pswd,
                            db_url,
                            db_user,
                            db_pswd);    

        } catch (Exception e) {
            e.printStackTrace();
        }
        
        if (hostName != null) {
            Configuration.setHostName(hostName);
            Configuration.setDomainName("");
        }
    }

    protected void setUp() throws Exception {        
	// grep the web and db connections
	server = TestServer.getInstance();        
	wc = server.getWebConversation();
	db = server.getDBConnection();
	home = server.getHomeRequest();
	logout = server.getLogoutRequest();
    }

    protected void tearDown() {
	try {
	    if (db != null)
		db.close();
	} catch (SQLException e) {}
    }
    
    protected void runTest() throws Throwable 
    {
        boolean testResult = true;
        try {
            super.runTest();
        } catch (Throwable e) {
            System.out.println(getName() + " error: " + e.getMessage());
            if (tracing) {
                e.printStackTrace();
            }
            testResult = false;
            throw e;
        } finally {
            TestLog.report("BMS", getName(), testResult);
        }
    }
}

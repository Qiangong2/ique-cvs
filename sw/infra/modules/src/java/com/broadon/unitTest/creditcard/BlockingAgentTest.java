package com.broadon.unitTest.creditcard;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import com.broadon.unitTest.ResourcePool;

import net.wiivc.services.AuthTxn;
import net.wiivc.services.BlockingAgent;
import net.wiivc.services.CreditCardFunctions;
import net.wiivc.services.PointsAppResults;
import net.wiivc.services.RefundTxn;
import net.wiivc.services.PointsConfirmationServiceLocator;
import net.wiivc.services.VCRefundServiceLocator;
public class BlockingAgentTest {

    /**
     * This program connects to a creditcard server (real or simulated)
     * to test NOA interafces. It uses BlockingAgent, which is effectively 
     * the same as calling the services directly. The simulated server does
     * not interact with database and does not keep any persistent state.
     * @param args
     * @throws MalformedURLException 
     * @throws ServiceException 
     * @throws RemoteException 
     */
    public static void main(String[] args) throws MalformedURLException, ServiceException, RemoteException {
        System.out.println("TEST BEGINS...");
        String taxAuthUrl = ResourcePool.getVCPaymentURL();
        CreditCardFunctions agent = new BlockingAgent(taxAuthUrl);
       
        // get tax
        AuthTxn txn = new AuthTxn();
        txn.setCountry("JP");
        txn.setFunctionCode("CALC_PAYMENT");
        txn.setTotalAmount("1000");
        agent.sendATxn(txn);
        AuthTxn ret = agent.getATxn();
        if (ret != null) {
        System.out.println("Result from CACL_PAYMENT: ");
        System.out.println("result= " + ret.getResult());
        System.out.println("tax= " + ret.getTotalTax());
        System.out.println("total= " + ret.getTotalPaid());
        } else
            System.out.println("Result from CACL_PAYMENT is null!");
        // get auth
        txn.setFunctionCode("AUTH_PAYMENT");
        agent.sendATxn(txn);
        ret = agent.getATxn();
        if (ret != null) {
        System.out.println("Result from AUTH_PAYMENT: ");
        System.out.println("result= " + ret.getResult());
        } else
            System.out.println("Result from AUTH_PAYMENT is null!");
        // points applied
        String pointsApplyUrl = ResourcePool.getPointsConfirmationURL();
        CreditCardFunctions agent2 = new BlockingAgent(pointsApplyUrl);
        String transIds[] = new String[] {"123", "abc", "789"};
        agent2.applyPoints(transIds);
        PointsAppResults[] results = agent2.getApplyPoints();
        for (int i=0; i<results.length; i++) {
            if (results[i].getAppliedResult().equalsIgnoreCase("SUCCESS"))
                System.out.println("Points Applied Confirmed for: " + results[i].getTransId());
            else
                System.out.println("Points Applied Failed for: " + results[i].getTransId());
        }
        // refund
        String vcRefundUrl = ResourcePool.getVCRefundURL();
        CreditCardFunctions agent3 = new BlockingAgent(vcRefundUrl);
        RefundTxn refundTxn = new RefundTxn();
        refundTxn.setTransId("xyz");
        agent3.doRefund(refundTxn);
        String refundReturn = agent3.getDoRefund();
        System.out.println(refundReturn);
    }

}

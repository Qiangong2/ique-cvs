package com.broadon.unitTest.pas;

import com.broadon.wsapi.pas.AuthorizeECardPaymentRequestType;
import com.broadon.wsapi.pas.AuthorizeECardPaymentResponseType;
import com.broadon.wsapi.pas.CapturePaymentRequestType;
import com.broadon.wsapi.pas.MoneyType;
import com.broadon.wsapi.pas.PasTokenType;
import com.broadon.wsapi.pas.RefundPaymentRequestType;
import com.broadon.wsapi.pas.RefundPaymentResponseType;
import com.broadon.wsapi.pas.VoidAuthorizationRequestType;

public class RefundTooMuchError extends TestCaseWrapper {
    PasTokenType token;
    

    public static void main(String[] args) {
        junit.textui.TestRunner.run(RefundTooMuchError.class);
    }

    public RefundTooMuchError(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
        resetTestDataBase(ECardTests);
        AuthorizeECardPaymentResponseType authResp;
        MoneyType amount = new MoneyType("POINTS", "20");
//      from batch03:    000101 0010000000 0014264647    
        String eCardSequence = "0010000000";
        AuthorizeECardPaymentRequestType authReq = createAuthorizationRequest(eCardSequence, amount);
        try {
            authResp = pas.authorizeECardPayment(authReq);
            boolean authReturn = checkAbstractResponse(authReq, authResp);
            if (authReturn) {   // no error, go ahead to capture some money
                token = authResp.getAuthorizationToken();
                amount = new MoneyType("POINTS", "5.5");
                CapturePaymentRequestType capReq = createCapturePaymentRequest(token, amount, false);
                pas.capturePayment(capReq);
            }
        } catch (Exception e) {
            System.out.println(getStackTrace(e));
            System.out.println(e.getMessage());
        }
    }
    
    public void tearDown() throws Exception {
        super.tearDown();
        if (token != null) {
            VoidAuthorizationRequestType voidReq = createVoidAuthorizationRequest(token);
            pas.voidAuthorization(voidReq);
        }
    }

    private boolean refundTooMuchError() {
        boolean testResult = true;
        if (token != null) {
            MoneyType amount = new MoneyType("POINTS", "6.5");
            RefundPaymentRequestType refundReq = createRefundPaymentRequest(token, amount);
            try {
                RefundPaymentResponseType refundResp;
                refundResp = pas.refundPayment(refundReq);
                testResult &= checkAbstractResponse(refundReq, refundResp);
                if (testResult)
                    return false;
                testResult = true;  // reset testResult
                amount.setAmount("5.5");
                refundResp = pas.refundPayment(refundReq);
                testResult &= checkAbstractResponse(refundReq, refundResp);
            } catch (Exception e) {
                System.out.println(getStackTrace(e));
                System.out.println(e.getMessage());
                testResult = false;
            }
        }
        return testResult;
    }

    public void testRefundTooMuchError() {
        assertTrue(refundTooMuchError());
    }  
}

package com.broadon.unitTest.cls;

import com.broadon.unitTest.Configuration;

public class SubscriptionChannel extends TestCaseWrapper
{   
    public SubscriptionChannel(String name)
    {
        super(name);
        channel = 0;
    }

    public void testCreateSubChannel() throws Throwable
    {
        final long cid = TEST_SUBSCRIPT_CHANNEL + 0x100000000L;
        db.removeChannel(cid);
        GenLicense lic = new GenLicense(cid, true);
        publish(lic.resp, lic.contents, 0);
    }
    
    public void testCreateSubOnRegularChannel() throws Throwable
    {
        final long cid = TEST_SUBSCRIPT_CHANNEL + 0x200000000L;
        db.removeChannel(cid);
        GenLicense lic = new GenLicense(cid + 1, false);
        publish(lic.resp, lic.outFiles, 0);
        lic = new GenLicense(cid, true);
        publish(lic.resp, lic.outFiles, 1004);
    }
    

    public static void main(String[] args)
    {
        if (args != null && args.length > 0)
            hostName = args[0];
        Configuration.testLab1();
        junit.textui.TestRunner.run(SubscriptionChannel.class);
    }

}

package com.broadon.unitTest.ias;

import com.broadon.wsapi.ias.AuthenticateDeviceRequestType;
import com.broadon.wsapi.ias.AuthenticateDeviceResponseType;

public class AuthenticateDeviceFailure extends TestCaseWrapper {

    public static void main(String[] args) {
    }

    public AuthenticateDeviceFailure(String name) {
	super(name);
    }

    public void setUp() throws Exception {
	super.setUp();
    }

    public void tearDown() throws Exception {
	super.tearDown();
    }

    private boolean checkAuthenticateDeviceFailure(
		String deviceId,
                AuthenticateDeviceResponseType expected)
	throws Throwable
    {
	boolean testResult = true;

        AuthenticateDeviceResponseType resp;
        AuthenticateDeviceRequestType req = new AuthenticateDeviceRequestType();
        
        initRequest(req);
	req.setDeviceId(Long.parseLong(deviceId));
        		
	resp = ias.authenticateDevice(req);
	testResult &= checkDeviceResponse(req, resp);
			
	// Check response against expected response
	testResult &= checkError(expected, resp);
        
	return testResult;
    }

    public void testAuthenticateDeviceFailure()
	throws Throwable
    {
        // Case 1: Authenticate an un-registered device
        String deviceId = TestDataAdmin.getUnRegisteredDeviceId();
        
        if (deviceId != null) {
            AuthenticateDeviceResponseType expected = new AuthenticateDeviceResponseType();
            expected.setDeviceId(Long.parseLong(deviceId));
            expected.setErrorCode(901);
            expected.setErrorMessage("IAS - Device Id is not registered");
            assertTrue(checkAuthenticateDeviceFailure(deviceId, expected));
        } else {
            System.out.println(getName() + " Warning: Failed to obtain an unregistered Device Id");
        }
    }
}

package com.broadon.unitTest.pas;

import com.broadon.wsapi.pas.AuthorizeECardPaymentRequestType;
import com.broadon.wsapi.pas.AuthorizeECardPaymentResponseType;
import com.broadon.wsapi.pas.CapturePaymentRequestType;
import com.broadon.wsapi.pas.CapturePaymentResponseType;
import com.broadon.wsapi.pas.MoneyType;
import com.broadon.wsapi.pas.PasTokenType;
import com.broadon.wsapi.pas.RefundPaymentRequestType;
import com.broadon.wsapi.pas.VoidAuthorizationRequestType;

public class CaptureTooMuchError extends TestCaseWrapper {

    PasTokenType token;
    CapturePaymentResponseType capResp;
    CapturePaymentRequestType capReq;

    public static void main(String[] args) {
        junit.textui.TestRunner.run(CaptureTooMuchError.class);
    }

    public CaptureTooMuchError(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
        resetTestDataBase(ECardTests);
        AuthorizeECardPaymentResponseType authResp;
        MoneyType amount = new MoneyType("POINTS", "20");
//      from batch03:    000101 0010000000 0014264647    
        String eCardSequence = "0010000000";
        AuthorizeECardPaymentRequestType authReq = createAuthorizationRequest(eCardSequence, amount);
        try {
            authResp = pas.authorizeECardPayment(authReq);
            boolean authReturn = checkAbstractResponse(authReq, authResp);
            if (authReturn) {   // no error, go ahead to capture some money
                token = authResp.getAuthorizationToken();
                MoneyType capAmount = new MoneyType("POINTS", "5.5");
                capReq = createCapturePaymentRequest(token, capAmount, false);
                capResp = pas.capturePayment(capReq);
            }
        } catch (Exception e) {
            System.out.println(getStackTrace(e));
            System.out.println(e.getMessage());
        }
    }
    
    public void tearDown() throws Exception {
        super.tearDown();
        if (token != null) {
            RefundPaymentRequestType refundReq = new RefundPaymentRequestType();
            refundReq.setAuthorizationToken(token);
            MoneyType amount = new MoneyType("POINTS", "5.5");
            refundReq.setAmount(amount);
            initAbstractRequest(refundReq);
            pas.refundPayment(refundReq);
            
            VoidAuthorizationRequestType voidReq = createVoidAuthorizationRequest(token);
            pas.voidAuthorization(voidReq);
        }
    }

    private boolean captureECardTooMuchError() {
        boolean testResult = true;
        if (token != null) {
            MoneyType amount = new MoneyType("POINTS", "28");   // Note this is the total capture amount
            capReq.setAmount(amount);
            try {
                capResp = pas.capturePayment(capReq);
                testResult &= !checkAbstractResponse(capReq, capResp);
            } catch (Exception e) {
                System.out.println(getStackTrace(e));
                System.out.println(e.getMessage());
                testResult = false;
            }
        }
        return testResult;
        
    }

    public void testCaptureECardTooMuchError() {
        assertTrue(captureECardTooMuchError());
    }  
}

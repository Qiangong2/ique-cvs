package com.broadon.unitTest.pas;

import com.broadon.wsapi.pas.CheckECardBalanceRequestType;
import com.broadon.wsapi.pas.CheckECardBalanceResponseType;

public class CheckECardBalance extends TestCaseWrapper {

    public static void main(String[] args) {
        junit.textui.TestRunner.run(CheckECardBalance.class);
    }

    public CheckECardBalance(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
        resetTestDataBase(ECardTests);
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    public boolean checkECardBalance() {
//      from batch03:    000101 0010000000 0014264647    
        String eCardSequence = "0010000000";
        CheckECardBalanceRequestType checkBalanceReq = createCheckBalanceRequest(eCardSequence);
        CheckECardBalanceResponseType checkBalanceResp;
        
        boolean testResult=true;

        try {
            checkBalanceResp = pas.checkECardBalance(checkBalanceReq);
            testResult &= checkAbstractResponse(checkBalanceReq, checkBalanceResp);
        } catch (Exception e) {
            System.out.println(getStackTrace(e));
            System.out.println(e.getMessage());
            testResult = false;
        }
    
        return testResult;
    }
    
    public void testCheckECardBalance() {
        assertTrue(checkECardBalance());
    }  
}

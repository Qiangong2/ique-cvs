package com.broadon.unitTest.ias;

import com.broadon.wsapi.ias.GetDeviceAccountsRequestType;
import com.broadon.wsapi.ias.GetDeviceAccountsResponseType;
import com.broadon.wsapi.ias.DeviceAccountType;

public class GetDeviceAccountsSuccess extends TestCaseWrapper {

    public static void main(String[] args) {
    }

    public GetDeviceAccountsSuccess(String name) {
	super(name);
    }

    public void setUp() throws Exception {
	super.setUp();
    }

    public void tearDown() throws Exception {
	super.tearDown();
    }

    private boolean checkAccountExist(String accountId, String[] accounts)
    {
        boolean found = false;
        
        for (int i = 0; accounts != null && i < accounts.length; i++) {
            if (accounts[i].equals(accountId)) {
                found = true;
                break;
            }
        }
        
        return found;
    }
    
    private boolean checkGetDeviceAccountsSuccess (
		String deviceId,
                String tokenId,
                String[] validAccounts,
                String[] removedAccounts,
                GetDeviceAccountsResponseType expected)
	throws Throwable
    {
	boolean testResult = true;

        GetDeviceAccountsResponseType resp;
        GetDeviceAccountsRequestType req = new GetDeviceAccountsRequestType();
        
        initRequest(req);
	req.setDeviceId(Long.parseLong(deviceId));
        req.setTokenId(tokenId);
		
	resp = ias.getDeviceAccounts(req);
	testResult &= checkDeviceResponse(req, resp);
		
	// Check response against expected response
	testResult &= checkError(expected, resp);
        
        if (testResult) {
            DeviceAccountType[] deviceAccounts = resp.getDeviceAccounts();
            
            if (deviceAccounts != null) {
                assertTrue(deviceAccounts.length == (validAccounts.length + removedAccounts.length));
                                
                for (int i = 0; testResult && i < deviceAccounts.length; i++) {
                    if (deviceAccounts[i].getRemoveDate() != null) {
                        testResult = removedAccounts != null && 
                            checkAccountExist(String.valueOf(deviceAccounts[i].getAccountId()), removedAccounts);
                    } else {
                        testResult = validAccounts != null && 
                            checkAccountExist(String.valueOf(deviceAccounts[i].getAccountId()), validAccounts);                        
                    }
                }
            }
        }
        
	return testResult;
    }

    public void testGetDeviceAccountsSuccess()
	throws Throwable
    {
        
        // Case 1: Get linked device accounts with a valid device token
        Token t = TestDataAdmin.getValidDeviceToken();
        //System.out.println("deviceId: " + t.id + ", tokenId: " + t.tokenId);
               
        if (t != null) {
            String[] removedAccounts = TestDataAdmin.getRemovedDeviceAccounts(t.id);
            String[] validAccounts = TestDataAdmin.getValidDeviceAccounts(t.id);
            
            GetDeviceAccountsResponseType expected = new GetDeviceAccountsResponseType();
            expected.setDeviceId(Long.parseLong(t.id));
            expected.setErrorCode(000);
            expected.setErrorMessage("");
            
            assertTrue(checkGetDeviceAccountsSuccess(t.id, t.tokenId, validAccounts, removedAccounts, expected));
        } else {
            System.out.println(getName() + " Warning: Failed to obtain a valid device token");
        }
    }    
}

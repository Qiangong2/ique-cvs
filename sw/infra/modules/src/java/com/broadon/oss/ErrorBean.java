package com.broadon.oss;

public class ErrorBean {
    private String[] msgCodes;
    private String message;
    
    public ErrorBean() {        
    }
    
    protected ErrorBean(String message, String[] msgCodes) {
        this.message = message;
        this.msgCodes = msgCodes;
    }
    
    protected ErrorBean(String message, String msgCode) {
        this(message, new String[] { msgCode } );
    }
    
    protected ErrorBean(String message, int msgCode) {
        this(message, String.valueOf(msgCode));        
    }
    
    public String getMessage() {
        return message;
    }
    
    public void setMessage(String message) {
        this.message = message;
    }
    
    public String[] getMsgCodes() {
        return msgCodes;
    }
    
    public void setMsgCodes(String[] msgCodes) {
        this.msgCodes = msgCodes;
    }
    
}

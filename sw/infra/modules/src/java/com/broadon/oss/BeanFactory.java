package com.broadon.oss;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import net.wiivc.services.AuthTxn;

import com.broadon.cas.*;
import com.broadon.exception.BroadOnException;
import com.broadon.servlet.ServletConstants;

public class BeanFactory {
    protected static Log log = LogFactory.getLog(BeanFactory.class);
    protected ServletConfig servletConfig;
    protected Map rules;

    public static DataSource getDataSource(ServletConfig config) {
        return (DataSource) config.getServletContext().getAttribute(
                ServletConstants.DATA_SOURCE_KEY);
    }

    public abstract static class AbstractCasBeanCreator implements BeanCreator {
        public void initBean(BaseBean bean, HttpServletRequest req,
                HttpServletResponse res) {
            // Common parameters
            String country = (String) req.getAttribute(Parameters.COUNTRY);
            if (country != null) {
                bean.setCountryCode(country);
            }
            String language = (String) req.getAttribute(Parameters.LANGUAGE);
            String locale = (String) req.getAttribute(Parameters.LOCALE);
            if (locale != null) {
                bean.setLocale(locale);
                if (language == null) {
                    int i = locale.indexOf('_');
                    language = (i > 0) ? locale.substring(0, i) : locale;
                }
            }
            if (language != null) {
                bean.setLanguageCode(language);
            }
            String region = (String) req.getAttribute(Parameters.REGION);
            if (region != null) {
                bean.setRegionCode(region);
            }
        }
    }
    
    public abstract static class AbstractListTitlesBeanCreator extends AbstractCasBeanCreator {
        public void initBean(TitleListBean bean, ServletConfig config, HttpServletRequest req,
                HttpServletResponse res) throws BroadOnException {
            super.initBean(bean, req, res);
            /* Page */
            String page = req.getParameter(Parameters.PAGE_NUMBER);
            if (page != null) {
                try {
                    bean.setPage(Integer.parseInt(page));
                } catch (NumberFormatException ex) {}
            }
            String pageSize = req.getParameter(Parameters.PAGE_SIZE);
            if (pageSize != null) {
                try {
                    bean.setPageSize(Integer.parseInt(pageSize));
                } catch (NumberFormatException ex) {}
            } else {
                bean.setPageSize(Parameters.DEFAULT_PAGESIZE);
            }
            /* Filtering parameters */
            bean.setCurrencyFilter("POINTS");
            String age = (String) req.getAttribute(Parameters.AGE);
            String genre = req.getParameter(Parameters.GENRE);
            String publisher = req.getParameter(Parameters.PUBLISHER);
            String platform = req.getParameter(Parameters.PLATFORM);
            String isNew = req.getParameter(Parameters.NEW);
            String isRecommended = req.getParameter(Parameters.RECOMMENDED);
            String title = req.getParameter(Parameters.TITLE);

            if (age != null) {
                try {
                    bean.addFilter(bean.AGE, Integer.parseInt(age),
                            bean.LESS_EQ_FLAG);
                } catch (NumberFormatException ex) {}
            }
            if (genre != null) {
                bean.addFilter(bean.CATEGORY, genre);
            }
            if (publisher != null) {
                bean.addFilter(bean.PUBLISHER, publisher);
            }
            if (platform != null) {
                bean.addFilter(bean.PLATFORM, platform);
            }
            if (title != null) {
                bean.searchTitle(title);
            }
            if (isNew != null && Boolean.valueOf(isNew).booleanValue()) {
                bean.showNew();
            }
            if (isRecommended != null && Boolean.valueOf(isRecommended).booleanValue()) {
                bean.showRecommended();
            }
            
            /* Filter by license type */
            String[] licenses = req.getParameterValues(Parameters.LICENSE);
            if (licenses != null) {
                bean.addFilter(bean.LICENSE_TYPE, licenses, true);
            }
            
            /* Ordering */
            String order = req.getParameter(Parameters.ORDER);
            if (order != null && !Parameters.ORDER_TITLENAME.equalsIgnoreCase(order)) {
                if (Parameters.ORDER_RELEASE_DATE.equalsIgnoreCase(order)) {
                    order = bean.RELEASE_DATE;
                } else if (Parameters.ORDER_SALES.equalsIgnoreCase(order)) {
                    order = bean.POPULARITY;
                } else if (Parameters.GENRE.equalsIgnoreCase(order)) {
                    order = bean.CATEGORY;
                } 
                bean.addOrder(order);
            } else {
                /* Use special AIUEO order for japanese */
                if ("ja".equalsIgnoreCase(bean.getLanguageCode())) {
                    bean.addOrder(bean.HIDDLE_TITLE);
                } else {                    
                    bean.addOrder(bean.TITLE);
                }
            }
            bean.init(getDataSource(config));
        }
    };
    
    private static final BeanCreator LIST_TITLES_BEAN_CREATOR = new AbstractListTitlesBeanCreator() {
        public Object createBean(ServletConfig config, HttpServletRequest req,
                HttpServletResponse res) throws BroadOnException {
            GameTitleListBean bean = new GameTitleListBean();
            initBean(bean, config, req, res);
            bean.list();
            return bean;
        }
    };
    
    private static final BeanCreator LIST_TITLE_PRICINGS_BEAN_CREATOR = new AbstractListTitlesBeanCreator() {
        public Object createBean(ServletConfig config, HttpServletRequest req,
                HttpServletResponse res) throws BroadOnException {
            GameTitleListPricingsBean bean = new GameTitleListPricingsBean();
            initBean(bean, config, req, res);
            bean.list();
            return bean;
        }
    };
    
    private static final BeanCreator LIST_TITLE_DETAILS_BEAN_CREATOR = new AbstractListTitlesBeanCreator() {
        public Object createBean(ServletConfig config, HttpServletRequest req,
                HttpServletResponse res) throws BroadOnException {
            GameTitleListDetailsBean bean = new GameTitleListDetailsBean();
            initBean(bean, config, req, res);
            bean.list();
            return bean;
        }
    };
    
    private static final BeanCreator LIST_ECARD_TITLES_BEAN_CREATOR = new AbstractListTitlesBeanCreator() {
        public Object createBean(ServletConfig config, HttpServletRequest req,
                HttpServletResponse res) throws BroadOnException {
            ECardTitleListBean bean = new ECardTitleListBean();
            ECardBean eCardBean = (ECardBean) req.getAttribute("eCardBean");
            if (eCardBean != null) {
                initBean(bean, req, res);
                bean.setECardType(eCardBean.getECardType());
                bean.list();
            }
            return bean;
        }
    };
    
    private static final BeanCreator COUNTRY_LIST_BEAN_CREATOR = new AbstractCasBeanCreator() {
        public Object createBean(ServletConfig config, HttpServletRequest req,
                HttpServletResponse res) throws BroadOnException {
            CountryListBean bean = new CountryListBean();
            initBean(bean, req, res);
            bean.init(getDataSource(config));
            bean.list();
            return bean;
        }
    };
    
    private static final BeanCreator FILTER_LIST_BEAN_CREATOR = new AbstractCasBeanCreator() {
        public Object createBean(ServletConfig config, HttpServletRequest req,
                HttpServletResponse res) throws BroadOnException {
            FilterBean bean = new FilterBean();
            String filter = req.getParameter(Parameters.FILTER);
            if (Parameters.GENRE.equalsIgnoreCase(filter)) {
                filter = bean.CATEGORY;
            }
            bean.setFilterBy(filter.toUpperCase());
            initBean(bean, req, res);
            bean.init(getDataSource(config));
            bean.list();
            return bean;
        }
    };
    
    private static final BeanCreator POINT_LIST_BEAN_CREATOR = new AbstractCasBeanCreator() {
        public Object createBean(ServletConfig config, HttpServletRequest req,
                HttpServletResponse res) throws BroadOnException {
            PointListBean bean = new PointListBean();
            initBean(bean, req, res);
            bean.init(getDataSource(config));
            bean.list();
            return bean;
        }
    };
    
    private static final BeanCreator ECARD_POINT_LIST_BEAN_CREATOR = new AbstractCasBeanCreator() {
        public Object createBean(ServletConfig config, HttpServletRequest req,
                HttpServletResponse res) throws BroadOnException {
            ECardPointListBean bean = new ECardPointListBean();
            ECardBean eCardBean = (ECardBean) req.getAttribute("eCardBean");
            if (eCardBean != null) {
                initBean(bean, req, res);
                bean.setECardType(eCardBean.getECardType());
                bean.init(getDataSource(config));
                bean.list();
            }
            return bean;
        }
    };
    
    private static final BeanCreator TITLE_DETAILS_BEAN_CREATOR = new AbstractCasBeanCreator() {
        public Object createBean(ServletConfig config, HttpServletRequest req,
                HttpServletResponse res) throws BroadOnException {
            TitleDetailBean bean = new TitleDetailBean();
            String titleId = req.getParameter(Parameters.TITLE_ID);
            bean.setTitleId(titleId);
            String itemId = req.getParameter(Parameters.ITEM_ID);
            if (itemId != null) {
                try {
                    bean.setItemId(Integer.valueOf(itemId.trim()));
                } catch (NumberFormatException ex) {}
            }
            initBean(bean, req, res);
            bean.init(getDataSource(config));            
            bean.list();
            return bean;
        }
    };

    public static class MessagesBeanCreator extends AbstractCasBeanCreator {
        private String messageType;
        
        public MessagesBeanCreator(String messageType) {
            this.messageType = messageType;
        }
        
        public Object createBean(ServletConfig config, HttpServletRequest req,
                HttpServletResponse res) throws BroadOnException {
            MessagesBean bean = new MessagesBean();
            bean.setMessageType(messageType);
            initBean(bean, req, res);
            bean.init(getDataSource(config));            
            bean.list();
            return bean;
        }
    };

    public abstract static class AbstractEcsBeanCreator implements BeanCreator {
        public void initBean(EcsBean bean, HttpServletRequest req,
                HttpServletResponse res) {
            // Common parameters
            // TODO
            bean.setDeviceId(1);
            String country = (String) req.getAttribute(Parameters.COUNTRY);
            if (country != null) {
                bean.setCountryCode(country);
            }
            String region = (String) req.getAttribute(Parameters.REGION);
            if (region != null) {
                bean.setRegionCode(region);
            }
        }
    }
    
    private static final BeanCreator TRANSACTION_LIST_BEAN_CREATOR = new AbstractEcsBeanCreator() {
        public Object createBean(ServletConfig config, HttpServletRequest req,
                HttpServletResponse res) throws BroadOnException {
            TransactionListBean bean = (TransactionListBean) req.getSession().getAttribute("listTransactions");
            boolean isNew = false;
            if (bean == null) {
                bean = new TransactionListBean();
                try {
                    bean.init(config);
                } catch (ServletException e) {
                    log.error("Error creating transaction list bean", e);
                    throw new BroadOnException("Error creating transaction list bean");
                }
                initBean(bean, req, res);
                String accountId = (String) req.getAttribute(Parameters.ACCOUNT_ID);
                bean.setAccountId(accountId);
                String pin = (String) req.getAttribute(Parameters.PIN);
                bean.setPin(pin);
                bean.list();
                isNew = true;
                req.getSession().setAttribute("listTransactions", bean);
            }
            String p = req.getParameter(com.broadon.oss.Parameters.PAGE_NUMBER);
            if (p != null) {
                try {
                    bean.setPage(Integer.parseInt(p));
                } catch (NumberFormatException ex) {}
            } else {
                // Refresh
                bean.setPage(1);
                if (!isNew) {
                    bean.list();
                }
            }
            return bean;
        }
    };

    private static final BeanCreator ECARD_BEAN_CREATOR = new AbstractEcsBeanCreator() {
        public Object createBean(ServletConfig config, HttpServletRequest req,
                HttpServletResponse res) throws BroadOnException {
            ECardBean bean = new ECardBean();
            try {
                bean.init(config);
            } catch (ServletException e) {
                log.error("Error instantiating eCard Bean", e);
                throw new BroadOnException("Error instantiating eCard Bean");
            }
            String eCardNumber = req.getParameter(Parameters.ECARD_NUMBER);
            initBean(bean, req, res);
            try {
                bean.setECardNumber(eCardNumber);
            } catch (Exception e) {
                log.error("Invalid eCardNumber", e);
                throw new BroadOnException("Invalid eCardNumber");
            }
            bean.checkECard();
            return bean;
        }
    };
    
    private static final BeanCreator POINT_PRICING_BEAN_CREATOR = new BeanCreator() {
        public Object createBean(ServletConfig config, HttpServletRequest req,
            HttpServletResponse res) throws BroadOnException {
            Point pointPricing = new Point();
            pointPricing.setItemCurrency(req.getParameter(Parameters.CURRENCY));
            String itemId = req.getParameter(Parameters.ITEM_ID);
            if (itemId != null) {
                try {
                    pointPricing.setItemId(Integer.parseInt(itemId));
                } catch (NumberFormatException e) {}
            }
            
            String points = req.getParameter(Parameters.POINTS);
            if (points != null) {
                try {
                    pointPricing.setRefillPoints(Integer.parseInt(points));
                } catch (NumberFormatException e) {}
            }
            
            String price = req.getParameter(Parameters.PRICE);
            if (price != null) {
                try {
                    pointPricing.setItemPrice(new BigDecimal(price));
                } catch (NumberFormatException e) {}
            }
            return pointPricing;
        }
    };
    
    public static class CreditCardBeanCreator implements BeanCreator {
        String funcCode;
        
        public CreditCardBeanCreator(String funcCode) {
            this.funcCode = funcCode;
        }
        
        public void initTxn(AuthTxn txn, HttpServletRequest req,
                HttpServletResponse res) {
            // Common parameters
            txn.setCountry((String) req.getAttribute(Parameters.COUNTRY));
            txn.setLanguage((String) req.getAttribute(Parameters.LANGUAGE));
            txn.setDeviceId(req.getParameter(Parameters.DEVICE_ID));
            txn.setAccountId(req.getParameter(Parameters.ACCOUNT_ID));            
            txn.setClientIP(req.getRemoteAddr());
            
            txn.setPaymentType(Parameters.PAYMENT_TYPE_CREDIT_CARD);
            txn.setPaymentMethodId(req.getParameter(Parameters.CC_NUMBER));
            
            txn.setBillingCity(req.getParameter(Parameters.CC_CITY));
            txn.setBillingCounty(req.getParameter(Parameters.CC_COUNTY));
            txn.setBillingPostal(req.getParameter(Parameters.CC_POSTAL));
            txn.setBillingState(req.getParameter(Parameters.CC_STATE));
            txn.setCardExpMM(req.getParameter(Parameters.CC_EXPMM));
            txn.setCardExpYY(req.getParameter(Parameters.CC_EXPYY));
            txn.setCardNum(req.getParameter(Parameters.CC_NUMBER));
            txn.setCardType(req.getParameter(Parameters.CC_TYPE));
            txn.setCardVfyVal(req.getParameter(Parameters.CC_VFYVAL));
            txn.setCurrency(req.getParameter(Parameters.CURRENCY));
            txn.setRefillPoints(req.getParameter(Parameters.POINTS));
            txn.setTotalAmount(req.getParameter(Parameters.PRICE));
        }

        public Object createBean(ServletConfig config, HttpServletRequest req,
                HttpServletResponse res) throws BroadOnException {
            CreditCardBean bean = new CreditCardBean();
            AuthTxn txn = new AuthTxn();
            txn.setFunctionCode(funcCode);
            initTxn(txn, req, res);
            try {
                bean.init(config);
                bean.setTxn(txn);
                bean.process();
            } catch (Exception e) {
                log.error("Error instantiating credit card bean", e);
                throw new BroadOnException("Error instantiating credit card bean");
            }
            return bean;
        }
    }
            
    private static final BeanCreator CREDITCARD_TAX_BEAN_CREATOR = new CreditCardBeanCreator(CreditCardBean.FUNC_CALC_PAYMENT);
    
    public BeanFactory(ServletConfig config) {
        servletConfig = config;
        rules = new HashMap();

        PageRule listTitlesRule = new PageRule("ListTitles.jsp");
        listTitlesRule.register("listTitles", LIST_TITLE_DETAILS_BEAN_CREATOR);
        rules.put("listTitles", listTitlesRule);
        rules.put(listTitlesRule.getJspUrl(), listTitlesRule);

        PageRule titleCatalogRule = new PageRule("TitleCatalog.jsp");
        titleCatalogRule.register("recommended", LIST_TITLES_BEAN_CREATOR);
        rules.put("catalog", titleCatalogRule);
        rules.put(titleCatalogRule.getJspUrl(), titleCatalogRule);

        PageRule titleDetailsRule = new PageRule("TitleDetails.jsp");
        titleDetailsRule.register("titleDetails", TITLE_DETAILS_BEAN_CREATOR);
        rules.put("titleDetails", titleDetailsRule);
        rules.put(titleDetailsRule.getJspUrl(), titleDetailsRule);

        PageRule purchaseTitleRule = new PageRule("PurchaseTitle.jsp");
        purchaseTitleRule.register("titleDetails", TITLE_DETAILS_BEAN_CREATOR);
        rules.put("purchaseTitle", purchaseTitleRule);
        rules.put(purchaseTitleRule.getJspUrl(), purchaseTitleRule);

        PageRule downloadTitleRule = new PageRule("DownloadTitle.jsp");
        downloadTitleRule.register("titleDetails", TITLE_DETAILS_BEAN_CREATOR);
        rules.put("downloadTitle", downloadTitleRule);
        rules.put(downloadTitleRule.getJspUrl(), downloadTitleRule);

        PageRule settingsRule = new PageRule("UseSettings.jsp");
        settingsRule.register("countries", COUNTRY_LIST_BEAN_CREATOR);
        rules.put("settings", settingsRule);
        rules.put(settingsRule.getJspUrl(), settingsRule);

        PageRule buyPointsRule = new PageRule("BuyPoints.jsp");
        buyPointsRule.register("pointPricings", POINT_LIST_BEAN_CREATOR);
        rules.put("buyPoints", buyPointsRule);
        rules.put(buyPointsRule.getJspUrl(), buyPointsRule);

        PageRule filterTitlesRule = new PageRule("ListFilters.jsp");
        filterTitlesRule.register("choices", FILTER_LIST_BEAN_CREATOR);
        rules.put("filterTitles", filterTitlesRule);
        rules.put(filterTitlesRule.getJspUrl(), filterTitlesRule);

        PageRule listTransactionsRule = new PageRule("ListTransactions.jsp");
        listTransactionsRule.register("listTransactions",
                TRANSACTION_LIST_BEAN_CREATOR);
        rules.put("listTransactions", listTransactionsRule);
        rules.put(listTransactionsRule.getJspUrl(), listTransactionsRule);
        
        PageRule buyPointsResultRule = new PageRule("BuyPointsResult.jsp") {
            
            public void initBeans(ServletConfig config, HttpServletRequest req, HttpServletResponse res)
                throws BroadOnException
            {
                super.initBeans(config, req, res);
                String paymentType = req.getParameter(Parameters.PAYMENT_TYPE);
                if (Parameters.PAYMENT_TYPE_CREDIT_CARD.equalsIgnoreCase(paymentType)) {
                    CreditCardBean ccBean = (CreditCardBean) CREDITCARD_TAX_BEAN_CREATOR.createBean(config, req, res);
                    AuthTxn retTxn = ccBean.getRetTxn();
                    if ("SUCCESS".equalsIgnoreCase(retTxn.getResult())) {
                        req.setAttribute("taxResult", ccBean);
                        req.setAttribute("pointPricing", POINT_PRICING_BEAN_CREATOR.createBean(config, req, res));
                    } else {
                        ErrorBean errorBean = new ErrorBean(retTxn.getVcMessage(), retTxn.getVcMsgCode());
                        req.setAttribute(Parameters.ERROR_BEAN, errorBean);
                    }
                } else if (Parameters.PAYMENT_TYPE_PREPAID_CARD.equalsIgnoreCase(paymentType)) {
                    ECardBean eCardBean = (ECardBean)
                        ECARD_BEAN_CREATOR.createBean(config, req, res);
                    if (eCardBean.getErrorCode() != EcsBean.STATUS_OK) {
                        ErrorBean errorBean = new ErrorBean(eCardBean.getErrorMessage(), eCardBean.getErrorCode());
                        req.setAttribute(Parameters.ERROR_BEAN, errorBean);
                        return;
                    }
                    req.setAttribute("eCardBean", eCardBean);
                    ECardPointListBean eCardPoints = (ECardPointListBean)
                        ECARD_POINT_LIST_BEAN_CREATOR.createBean(config, req, res);
                    if (eCardPoints != null) {
                        List pointList = eCardPoints.getPoints();
                        if (pointList != null && pointList.size() > 0) {
                            req.setAttribute("pointPricing", pointList.get(0));
                        }
                    }
                } else {
                    log.debug("Unsupported payment type for points " + paymentType);
                    req.setAttribute("pointPricing", POINT_PRICING_BEAN_CREATOR.createBean(config, req, res));
                }
            }

        };
        rules.put("buyPointsResult", buyPointsResultRule);
        rules.put(buyPointsResultRule.getJspUrl(), buyPointsResultRule); 

        PageRule eulaRule = new PageRule("Terms.jsp");
        eulaRule.register("eula", new MessagesBeanCreator("EULA"));
        rules.put("eula", eulaRule);
        rules.put(eulaRule.getJspUrl(), eulaRule);

    }

    public PageRule getRule(HttpServletRequest req, HttpServletResponse res,
            String defaultPage) {
        String page = req.getParameter(Parameters.PAGE);
        if (page == null) {
            page = defaultPage;
        }
        return (PageRule) this.rules.get(page);
    }
}

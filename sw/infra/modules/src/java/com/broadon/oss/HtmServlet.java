package com.broadon.oss;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.broadon.exception.BroadOnException;
import com.broadon.servlet.ServletConstants;

public class HtmServlet extends HttpServlet {
    protected static Log log = LogFactory.getLog(HtmServlet.class);

    // Request attributes
    private static final String JAVAX_CIPHER = "javax.servlet.request.cipher_suite";
    private static final String SECURE_PATH_PREFIX = "/secure/";
    private static final String LOGIN_ERROR_JSP = "Login.jsp";
    private static final String DEFAULT_LANGUAGE = "en";
    private static final String DEFAULT_LOCALE = "en_US";
    private static final String DEFAULT_REGION = "US";
    private static final String DEFAULT_COUNTRY = "US";
    private static final String DEFAULT_ACCOUNT_ID = "30002";
    private static final String DEFAULT_PIN = "30002";
    
    private static final String JSP_PATH = "/jsp/";

    // Member variables.  These are typically set once during initialization
    // and thereafter accessed without requiring synchronization.
    //
    private BeanFactory beanFactory = null; // Factory of result beans

    /** Internal URLs */
    public static final String CFG_ECS_URL = "ECS_URL";
    public static final String CFG_IAS_URL = "IAS_URL";
    public static final String CFG_CC_PAYMENT_URL = "CC_PAYMENT_URL";

    /** External URLs - Passed to ECLib on Wii */
    /** Content Prefix URL */
    private static final String CFG_EXT_CONTENT_PREFIX_URL = "EXT_CONTENT_PREFIX_URL";
    /** Uncached content prefix URL */
    private static final String CFG_EXT_UNCACHED_CONTENT_PREFIX_URL = "EXT_UNCACHED_CONTENT_PREFIX_URL";

    /** ECS URL */
    public static final String CFG_EXT_ECS_URL = "EXT_ECS_URL";
    /** IAS URL */
    public static final String CFG_EXT_IAS_URL = "EXT_IAS_URL";
    
    /** Supported Locales */
    public static final String CFG_LOCALES = "LOCALES";    
    private Set supportedLocales = new HashSet();
    
    public static final String CFG_OSS_PREFIX = "OSS_PREFIX";
    String ossPrefix;

    public void init (ServletConfig config) throws ServletException
    {
        super.init(config);
        beanFactory = new BeanFactory(config);
        
        ServletContext servletContext = config.getServletContext();
        
        // Property file defined in the conf/BBserver.properties.tmpl file.
        // Add properties there as required for the OSS server.
        //
        Properties cfg = (Properties) servletContext.getAttribute(ServletConstants.PROPERTY_KEY);
        URL ecsUrl = getURLCfg(cfg, CFG_ECS_URL, null);
        servletContext.setAttribute(CFG_ECS_URL, ecsUrl);
        
        URL iasUrl = getURLCfg(cfg, CFG_IAS_URL, null);
        servletContext.setAttribute(CFG_IAS_URL, iasUrl);
        
        URL ccPaymentUrl = getURLCfg(cfg, CFG_CC_PAYMENT_URL, null);
        servletContext.setAttribute(CFG_CC_PAYMENT_URL, ccPaymentUrl);
        
        URL extContentPrefixUrl = getURLCfg(cfg, CFG_EXT_CONTENT_PREFIX_URL, null);
        servletContext.setAttribute(CFG_EXT_CONTENT_PREFIX_URL, extContentPrefixUrl);
        
        URL extUncachedContentPrefixUrl = getURLCfg(cfg, CFG_EXT_UNCACHED_CONTENT_PREFIX_URL, null);
        servletContext.setAttribute(CFG_EXT_UNCACHED_CONTENT_PREFIX_URL, extUncachedContentPrefixUrl);
        if (extUncachedContentPrefixUrl == null) {
            log.info(CFG_EXT_UNCACHED_CONTENT_PREFIX_URL + " not specified, using the " + 
                    CFG_EXT_CONTENT_PREFIX_URL + " of " + extContentPrefixUrl);
            extUncachedContentPrefixUrl = extContentPrefixUrl;
        }
        
        URL extEcsUrl = getURLCfg(cfg, CFG_EXT_ECS_URL, null);
        servletContext.setAttribute(CFG_EXT_ECS_URL, extEcsUrl);
        
        URL extIasUrl = getURLCfg(cfg, CFG_EXT_IAS_URL, null);
        servletContext.setAttribute(CFG_EXT_IAS_URL, extIasUrl);
        
        String locales = getCfg(cfg, CFG_LOCALES, null);
        if (locales != null) {
            setLocales(locales);
        }
        
        ossPrefix = getCfg(cfg, CFG_OSS_PREFIX, "/");
        
       log.debug("HtmServlet inited");
    } // init

    private void setLocales(String locales) 
    {
        StringTokenizer st = new StringTokenizer(locales, ",");
        while (st.hasMoreTokens()) {
            supportedLocales.add(st.nextToken());
        }
    }
    
    public static URL getURLCfg(Properties cfg, String key, String defValue)
        throws ServletException
    {
        String urlStr = getCfg(cfg, key, defValue);
        if (urlStr != null) {
            urlStr = urlStr.trim();
            if (urlStr.length() == 0) {
                urlStr = null;
            }
        }
        try {
            URL url = (urlStr != null)? new URL(urlStr): null;
            return url;
        }
        catch (MalformedURLException e) {
            throw new ServletException("Malformed " + key+ ": " + urlStr, e);
        }
    }

    public static String getCfg(Properties cfg, String key, String defValue)
    {
        String v = (cfg != null)? cfg.getProperty(key): null;
        if (v == null) {
            v = defValue;
            log.warn("BBserver.properties missing " + key + " value" +
                     " ... using default of " + defValue);
        } else {
            log.info("Setting " + key + " to " + v);
        }
        return v;
    }

    public String getJspPath(String locale)
    {
        // Check if locale supported - default to common if not supported
        if (!supportedLocales.contains(locale)) {
             return "common";   
        } else {
            return locale;
        }
    }
    
    public String getJspPath(String locale, String page)
    {
        return "/" + getJspPath(locale) + JSP_PATH + page;
    }
    
    // Sets session attribute using request parameter
    public String setSessionAttribute(HttpServletRequest req, String name, String defaultValue)
    {
        String value = req.getParameter(name);
        if (value == null || value.equals("")) {
            value = (String) req.getSession().getAttribute(name);
        } else {
            value = value.trim();
            req.getSession().setAttribute(name, value);
        }
        if (value == null || value.equals("")) {
            value = defaultValue;
        } 
        req.setAttribute(name, value);
        return value;
    }
    
    public void doGet(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
        ServletContext sc = getServletContext();
        getServletConfig();
        String jspPage = null;

        req.setCharacterEncoding("UTF-8");
        res.setContentType("text/html;charset=UTF-8");

        /* Figure out region */
        String region = setSessionAttribute(req, Parameters.REGION, DEFAULT_REGION);
        /* Figure out country */
        String country = setSessionAttribute(req, Parameters.COUNTRY, DEFAULT_COUNTRY);
        /* Figure out language */
        String language = setSessionAttribute(req, Parameters.LANGUAGE, DEFAULT_LANGUAGE);        
        /* Figure out locale */
        String locale = setSessionAttribute(req, Parameters.LOCALE, DEFAULT_LOCALE);        
        /* Figure out age */
        String age = setSessionAttribute(req, Parameters.AGE, null);
        /* Figure out account id */
        String accountId = setSessionAttribute(req, Parameters.ACCOUNT_ID, DEFAULT_ACCOUNT_ID);
        /* Figure out pin */
        String pin = setSessionAttribute(req, Parameters.PIN, DEFAULT_PIN);
        
        /* Figure out page */
        if (jspPage == null) {
            String page = req.getPathInfo();
            if (page.startsWith("/")) {
                page = page.substring(1);
            }
        
            PageRule rule = beanFactory.getRule(req, res, page);
            if (rule != null) {
                try {
                    rule.initBeans(getServletConfig(), req, res);
                } catch (BroadOnException e) {
                    throw new ServletException(e.getMessage(), e);
                } 
                jspPage = rule.getJspUrl(getServletConfig(), req, res);
            } else {
                // TODO: Redirect to Error page (instead of Home)
                // jspPage = "Home.jsp";//req.getRequestURI();
                jspPage = page;
            }
        }

        req.setAttribute(Parameters.OSS_PREFIX, ossPrefix + getJspPath(locale));
        String valid_url = res.encodeURL(getJspPath(locale, jspPage));

        log.debug("Session id: " + req.getSession().getId());
        log.debug("forward to page: " + valid_url);
        log.debug("region " + region + ", locale " + locale + ", country " + country + ", language " + language + ", age " + age);
        
        RequestDispatcher rd = sc.getRequestDispatcher(valid_url);
        rd.forward(req, res);
    } // doGet


    public void doPost(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
        doGet(req, res);
    }

}

package com.broadon.oss;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.xml.rpc.ServiceException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import net.wiivc.services.AuthTxn;
import net.wiivc.services.BlockingAgent;
import net.wiivc.services.CreditCardFunctions;
import net.wiivc.services.JAXBAgent;
import net.wiivc.services.JAXBAgentException;

public class CreditCardBean {
    protected static Log log = LogFactory.getLog(CreditCardBean.class);

    public static final String FUNC_CALC_PAYMENT = "CALC_PAYMENT";
    
    protected AuthTxn txn;
    protected AuthTxn retTxn;
    protected String retTxnString;
    protected CreditCardFunctions agent;
        
    
    public void init(ServletConfig config) throws ServletException, MalformedURLException {
        URL url = (URL) config.getServletContext().getAttribute(HtmServlet.CFG_CC_PAYMENT_URL);
        log.debug("creditCardUrl: " + url);
    
        agent = new BlockingAgent(url.toString());
    }
    
    public void process()
        throws ServiceException, RemoteException, JAXBAgentException 
    {
        agent.sendATxn(txn);
        retTxn = agent.getATxn();
        JAXBAgent xmlAgent = new JAXBAgent();
        retTxnString = xmlAgent.getXMLString(retTxn);
        retTxnString = retTxnString.trim();
        retTxnString = retTxnString.replace('\r', ' ');
        retTxnString = retTxnString.replace('\n', ' ');
        log.debug("retTxnString is ");
        log.debug(retTxnString);
    }

    public AuthTxn getRetTxn()
    {
        return retTxn;
    }
    
    public String getRetTxnString()
    {
        return retTxnString;
    }
    
    public void setTxn(AuthTxn txn)
    {
        this.txn = txn;
    }
    
    public AuthTxn getTxn()
    {
        return txn;
    }
    
}

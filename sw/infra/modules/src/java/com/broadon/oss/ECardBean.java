package com.broadon.oss;

import java.io.UnsupportedEncodingException;
import java.rmi.RemoteException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.broadon.cas.Transaction;
import com.broadon.util.HexString;
import com.broadon.wsapi.ecs.AccountPaymentType;
import com.broadon.wsapi.ecs.CheckECardBalanceRequestType;
import com.broadon.wsapi.ecs.CheckECardBalanceResponseType;
import com.broadon.wsapi.ecs.DebitCreditType;
import com.broadon.wsapi.ecs.ECardInfoType;
import com.broadon.wsapi.ecs.ECardPaymentType;
import com.broadon.wsapi.ecs.ListTransactionsRequestType;
import com.broadon.wsapi.ecs.ListTransactionsResponseType;
import com.broadon.wsapi.ecs.MoneyType;
import com.broadon.wsapi.ecs.TransactionType;

public class ECardBean extends EcsBean {
    protected static Log log = LogFactory.getLog(ECardBean.class);

    private ECardPaymentType eCard = new ECardPaymentType();
    private String eCardNumber;

    private ECardInfoType eCardInfo;
    private MoneyType balance;
    
    public ECardBean() {
    }

    public boolean checkECard() {
        CheckECardBalanceResponseType resp;
        CheckECardBalanceRequestType req = new CheckECardBalanceRequestType();
        
        initAbstractRequest(req);        
        req.setECard(eCard);
        try {
            resp = ecs.checkECardBalance(req);
            balance = resp.getBalance();
            eCardInfo = resp.getECardInfo();
            if (checkAbstractResponse(req, resp)) {
                log.debug("balance[amount: " + balance.getAmount()
                        + ", currency: " + balance.getCurrency() + "]");
                return true;
            }
        } catch (RemoteException e) {
            e.printStackTrace();
            log.error(e);
        }
        return false;
    }

    public String getECardType() {
        if (eCardInfo != null) {
            return eCardInfo.getECardType();
        } else {
            return null;
        }
    }
   
    public String getECardNumber() {        
        return eCardNumber;
    }

    public void setECardNumber(String cardNumber) 
        throws NoSuchAlgorithmException, UnsupportedEncodingException 
    {
        if (cardNumber == null) {
            throw new IllegalArgumentException("ECard number cannot be null");
        }
        eCardNumber = cardNumber.trim();
        /* Check eCard is 16 digit number */
        if (eCardNumber.length() != 16) {
            throw new IllegalArgumentException("ECard number must be a 16 digit number");            
        }
        for (int i = 0; i < eCardNumber.length(); i++) {
            if (!Character.isDigit(eCardNumber.charAt(i))) {
                throw new IllegalArgumentException("ECard number must be a 16 digit number");            
            }
        }
        
        // Calculate hash
        String secret = "00" + eCardNumber.substring(8);
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        byte[] bytes = md5.digest(secret.getBytes("US-ASCII"));       
        String hash = HexString.toHexString(bytes).toLowerCase();
        eCard.setECardNumber("00" + eCardNumber.substring(0, 8));
        eCard.setECardHash(hash);
        eCard.setECardType("000000");
        log.debug("ECard hash: " + hash);
    }

    
}

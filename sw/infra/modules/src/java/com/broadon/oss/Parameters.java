package com.broadon.oss;

/* Request parameters */
public interface Parameters {
    public static final String ACTION="action";
    public static final String PAGE="page";
    
    // Common parameters
    public static final String OSS_PREFIX="ossPrefix";
    public static final String REGION="region";
    public static final String COUNTRY="country";
    public static final String LANGUAGE="language";    
    public static final String LOCALE="locale";    
    public static final String AGE="age";
    
    public static final String ITEM_ID="itemId";    
    public static final String RATING="rating";
    public static final String TITLE_TYPE="type";
    public static final String TITLE_ID="titleId";
    public static final String TITLE="title";
    public static final String PAGE_NUMBER="p";
    public static final String PAGE_SIZE="ps";
    public static final String PUBLISHER="publisher";
    public static final String GENRE="genre";
    public static final String PLATFORM="platform";
    public static final String NEW="new";
    public static final String RECOMMENDED="rec";
    public static final String ORDER="order";
    public static final String FILTER="filter";
    public static final String CURRENCY="currency";
    public static final String PRICE="price";
    public static final String POINTS="points";
    public static final String LICENSE="license";
    public static final String PAYMENT_TYPE="pay";

    public static final String DEVICE_ID="deviceId";
    public static final String ACCOUNT_ID="accountId";
    public static final String PIN="pin";

    // Credit Card payment information
    public static final String CC_COUNTY="cc_country";
    public static final String CC_POSTAL="cc_postal";
    public static final String CC_STATE="cc_state";
    public static final String CC_CITY="cc_city";
    public static final String CC_EXPMM="cc_expmm";
    public static final String CC_EXPYY="cc_expyy";
    public static final String CC_NUMBER="cc_number";
    public static final String CC_OWNER="cc_owner";
    public static final String CC_TYPE="cc_type";
    public static final String CC_VFYVAL="cc_vfyval";
    
    // ECard payment information
    public static final String ECARD_NUMBER="cnumber";
    
    // Default parameters
    public static final int DEFAULT_PAGESIZE=4;
    
    // Supported values for ordering
    public static final String ORDER_TITLENAME = "title";
    public static final String ORDER_SALES = "sales";
    public static final String ORDER_RELEASE_DATE = "release";
    
    // Payment types
    public static final String PAYMENT_TYPE_CREDIT_CARD = "CCARD";
    public static final String PAYMENT_TYPE_ACCOUNT = "ACCOUNT";
    public static final String PAYMENT_TYPE_PREPAID_CARD = "ECARD";
    
    public static final String ERROR_BEAN = "error";
}

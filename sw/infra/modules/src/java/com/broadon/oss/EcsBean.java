package com.broadon.oss;

import java.net.URL;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.broadon.wsapi.ecs.AbstractRequestType;
import com.broadon.wsapi.ecs.AbstractResponseType;
import com.broadon.wsapi.ecs.ECommercePortType;
import com.broadon.wsapi.ecs.ECommerceService;
import com.broadon.wsapi.ecs.ECommerceServiceLocator;

public class EcsBean {
    protected static Log log = LogFactory.getLog(EcsBean.class);

    public static final int STATUS_OK = 0;
    private static String VERSION = "1.0";
    private long deviceId;
    private String regionCode;
    private String countryCode;
    
    private String errorMessage;
    private int errorCode;

    protected static ECommercePortType ecs = null;

    public static void init(ServletConfig config) throws ServletException {
        
        if (ecs != null) {
            return;
        }
        URL ecsUrl = (URL) config.getServletContext().getAttribute(HtmServlet.CFG_ECS_URL);
        log.debug("ecsUrl: " + ecsUrl);

        // Instantiate ECS service
        try {
            ECommerceService ecsService = new ECommerceServiceLocator();
            ecs = (ecsUrl == null)? ecsService.getECommerceSOAP():
                ecsService.getECommerceSOAP(ecsUrl);
        } catch (Exception se) {
            log.error("Error during init");
            log.error(se);
            //throw new ServletException(se);
        }

    }
    public EcsBean() {}

    protected void initAbstractRequest(AbstractRequestType req) {
        req.setVersion(VERSION);
        req.setDeviceId(deviceId);
        req.setRegionId(regionCode);
        req.setCountryCode(countryCode);
        req.setMessageId(Long.toString(System.currentTimeMillis()));
        log.debug("version: " + VERSION + ", deviceId: " + deviceId + 
                ", region: " + regionCode + ", messageId: " + req.getMessageId());
    }

    protected boolean checkAbstractResponse(AbstractRequestType req,
            AbstractResponseType resp) {
        if (req.getVersion().equals(resp.getVersion())
                && req.getDeviceId() == resp.getDeviceId()
                && req.getMessageId().equals(resp.getMessageId())) {
            if (resp.getErrorCode() != STATUS_OK) {
                log.error(resp.getErrorMessage());
                errorMessage = resp.getErrorMessage();
                errorCode = resp.getErrorCode();
                return false;
            }
            return true;
        }
        return false;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(long deviceId) {
        this.deviceId = deviceId;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public int getErrorCode() {
        return errorCode;
    }
    
    public String getErrorMessage() {
        return errorMessage;
    }
}

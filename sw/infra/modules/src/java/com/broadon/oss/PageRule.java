package com.broadon.oss;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.broadon.exception.BroadOnException;

public class PageRule {
    protected String errorUrl;
    protected String jspUrl;
    protected Map beanMap;
        
    public PageRule(String jspUrl, String errorUrl)
    {
        this.jspUrl = jspUrl;
        this.errorUrl = errorUrl;
        beanMap = new HashMap();
    }
        
    public PageRule(String jspUrl)
    {
        this(jspUrl, "Error.jsp");
    }
    
    public void register(String name, BeanCreator beanCreator)
    {
        beanMap.put(name, beanCreator);
    }
    
    public String getJspUrl(ServletConfig config, HttpServletRequest req, HttpServletResponse res) { 
        if (req.getAttribute(Parameters.ERROR_BEAN) != null) {
            return errorUrl;
        } else {
            return jspUrl;
        }
    }
        
    public String getJspUrl() { 
        return jspUrl;
    }
        
    void initBeans(ServletConfig config, HttpServletRequest req, HttpServletResponse res)    
            throws BroadOnException
    {
        Set keys = beanMap.keySet();
        Iterator iter = keys.iterator();
        while (iter.hasNext()) {
            String attribute = (String) iter.next();
            BeanCreator beanCreator = (BeanCreator) beanMap.get(attribute);
            Object bean = beanCreator.createBean(config, req, res);
            req.setAttribute(attribute, bean);
        }
    }
 
}

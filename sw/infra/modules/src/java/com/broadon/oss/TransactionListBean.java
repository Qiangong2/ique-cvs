package com.broadon.oss;

import java.rmi.RemoteException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.broadon.cas.Transaction;
import com.broadon.wsapi.ecs.AccountPaymentType;
import com.broadon.wsapi.ecs.DebitCreditType;
import com.broadon.wsapi.ecs.ListTransactionsRequestType;
import com.broadon.wsapi.ecs.ListTransactionsResponseType;
import com.broadon.wsapi.ecs.MoneyType;
import com.broadon.wsapi.ecs.TransactionType;

public class TransactionListBean extends EcsBean {
    protected static Log log = LogFactory.getLog(TransactionListBean.class);
    private String accountId;
    private String pin;
    /** Available balance */
    private MoneyType balance = null;
    /** List of transactions */
    private List transactions;
    private int pageRows = 14;
    private int page = 1;

    public TransactionListBean() {
        transactions = new ArrayList();
    }

    public void list() {
        ListTransactionsResponseType resp;
        ListTransactionsRequestType req = new ListTransactionsRequestType();
        initAbstractRequest(req);
        AccountPaymentType acct = new AccountPaymentType();
        log.debug("accountId: " + accountId + ", pin: " + pin);
        acct.setAccountNumber(accountId);
        acct.setPin(pin);
        req.setAccount(acct);
        try {
            resp = ecs.listTransactions(req);
            if (checkAbstractResponse(req, resp)) {
                balance = resp.getBalance();
                log.debug("balance[amount: " + balance.getAmount()
                        + ", currency: " + balance.getCurrency() + "]");
                TransactionType[] trans = resp.getTransactions();
                transactions.clear();
                for (int i = 0; i < trans.length; i++) {
                    if ("SUCCESS".equals(trans[i].getStatus())) {
                        Transaction transaction = new Transaction();
                        transaction.setTransId(trans[i].getTransactionId());
                        transaction.setTransDate(new Date(trans[i].getDate()));
                        transaction.setTransType(trans[i].getType());
                        transaction.setTotalAmount(trans[i].getTotalAmount());
                        transaction.setBalance(trans[i].getBalance());
                        transaction.setCredit(DebitCreditType._Credit.equals(trans[i].getDebitCredit().getValue()));
                        transactions.add(transaction);
                    }
                }
                log.debug("transactions size: " + getSize());
            }
        } catch (RemoteException e) {
            e.printStackTrace();
            log.error(e);
        }
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public MoneyType getBalance() {
        return balance;
    }

    public int getSize() {
        return (transactions == null) ? 0 : transactions.size();
    }

    public int getPageRows() {
        return pageRows;
    }

    public void setPageRows(int pageRows) {
        this.pageRows = pageRows;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        if (page > 0 && page <= getPageCount())
        this.page = page;
    }
    
    public List getTransactions() {
        int endIndex = page * pageRows;
        if (endIndex > getSize()) {
            endIndex = getSize();
        }
        return transactions.subList((page-1)*pageRows, endIndex);
    }
    
    public int getPageCount() {
        int size =  getSize();
        
        if ((size % pageRows) == 0) {
           return size / pageRows ; 
        }
        
        return size / pageRows + 1;
    }
}

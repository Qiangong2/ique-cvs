package com.broadon.oss;

import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.broadon.exception.BroadOnException;

public interface BeanCreator {
    public Object createBean(ServletConfig config, HttpServletRequest req, HttpServletResponse res)
        throws BroadOnException;
}

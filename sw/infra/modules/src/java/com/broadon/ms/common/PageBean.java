package com.broadon.ms.common;

/**
 * Top level bean for generic paged output. Results are in XML/HTML formatted
 * strings.
 * It has following Bean properties:
 * <ul>
 * <li> pageSize, String <br> number of records per page in the result set.
 * <li> pageNo, String <br> page No. in the result set.
 * <li> resultFormat, String <br> the format of the result string: xml or html
 * </ul>
 * @author	Vaibhav Bhargava
 * @version	$Revision: 1.2 $
 */
public class PageBean
{
    /*
     * Bean Properties
     */
    protected String	resultFormat;
    protected int	pageSize;
    protected int	pageNo;

    // --------------------------------------------------------------------
    //             Constructors
    // --------------------------------------------------------------------

    public PageBean(int pageSize)
    {
        this.resultFormat = "xml";
	this.pageSize = pageSize;
	this.pageNo = 0;
    }

    // --------------------------------------------------------------------
    //            Bean Properties
    // --------------------------------------------------------------------

    /**
     * Bean Property: PageSize
     */
    public void setPageSize(int p) { pageSize = p; }
    public int getPageSize() { return pageSize; }

    /**
     * Bean Property: pageNo
     */
    public void setPageNo (int p) { pageNo = p; }
    public int getPageNo() { return pageNo; }

    /**
     * Bean Property: ResultFormat
     */
    public void setResultFormat(String s) { resultFormat = s; }
    public String getResultFormat() { return resultFormat; }

    public int getStartingOffset() { return (pageNo-1)*pageSize; }
    public int getEndingOffset() { return pageNo*pageSize; }
}

/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: AdminServlet.java,v 1.5 2006/06/08 07:10:54 vaibhav Exp $
 */
package com.broadon.ms.common;

import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.sql.DataSource;

import com.broadon.servlet.ServletConstants;


public abstract class AdminServlet extends HttpServlet
{
    private static final String LOCALE_HEADER    = "accept-language";
    private static final String DEFAULT_LOCATION = "/jsp";
    private static final String DEFAULT_LABELS = "DEFAULT_LABLES";
    
    protected static final String SES_ERROR = "ERROR";
    protected static final String SES_SUCCESS = "SUCCESS";
    protected static final String SES_ROLE = "BCC_ROLE";
    protected static final String SES_ROLE_NAME = "BCC_ROLE_NAME";
    protected static final String SES_ROLE_NUM = "BCC_ROLE_NUM";
    protected static final String SES_USER = "BCC_USER";
    protected static final String SES_USER_ID = "BCC_USER_ID";
    protected static final String SES_BU_ID = "BU_ID";
    protected static final String SES_FULLNAME = "BCC_FULLNAME";
    protected static final String SES_LOCALE = "CURRENT_LOCALE";
    protected static final String SES_LOCATION = "CURRENT_LOCATION";
    protected static final String XSL_PARAM_ROLE= "role";

    protected static final int ROLE_ADMIN = 10;
    protected static final int ROLE_NEC = 20;
    protected static final int ROLE_EMS = 30;
    protected static final int ROLE_ECARD = 40;
    protected static final int ROLE_CA = 50;
    protected static final int ROLE_CONTENT = 60;
    protected static final int ROLE_RMA = 70;
    protected static final int ROLE_BU_ADMIN = 110;
    protected static final int ROLE_BU_MKTG = 120;
    protected static final int ROLE_BU_CS = 130;
    protected static final int ROLE_BU_ECARD = 180;
    protected static final int ROLE_EBU_ADMIN = 110;

    protected static final int PAGE_SIZE = 50;

    protected static final String ACTION_LOGIN = "LOGIN";
    protected static final String ACTION_LOGOUT = "LOGOUT";
    
    // Handle to the backend processing for all incoming requests
    protected Properties mAdminProp = null;
    protected Hashtable     mLabels = new Hashtable();
    protected String mLabelsFile = null;

    protected Map               nameMap;
    protected DataSource	dataSource;

    public void init (ServletConfig config) throws ServletException
    {
        super.init(config);

	ServletContext	context = config.getServletContext();
	this.dataSource = (DataSource)context.getAttribute("javax.sql.DataSource");
        this.nameMap = (Map)context.getAttribute(ServletConstants.NAME_MAP_KEY);

        String properties = context.getInitParameter("properties");
        String property_file = context.getRealPath(properties);

	System.out.println("reading prop file: " + property_file);
    

        try {            
            // load properties from the file specified
            //
            mAdminProp = new Properties();
            FileInputStream in = new FileInputStream(property_file);
            mAdminProp.load(in);
            in.close();
        } catch (Exception e) {
            throw new ServletException(e.toString());
        }
    }

    /**
     *  Override "service" function for handle logic with "'session"
     *  for new session, store localized labels into it.
     */
    protected final void service(HttpServletRequest req,
                                 HttpServletResponse res)
                        throws ServletException, IOException
    {
        // get a session
        HttpSession ses = req.getSession(false);
        
        if (ses == null) {
            // new session starts
            ses = req.getSession(true);
	    System.out.println("Starting New Session - " + ses.getId());
        }    
        // store localized labels
        storeLabels(getLocale(req), mLabelsFile, ses);

        req.setCharacterEncoding("UTF8");
	res.setContentType("text/html; charset=utf-8");

        // proceed to super class's service function
        super.service(req, res);        
    }

    /**
     *  Store appropriate labels into session
     *  @param locale the locale string.
     */
    protected void storeLabels(String locale, String label_file, HttpSession ses)
    {
        //
        // store default values, first
        //
        Properties labels = getDefaultLabels(label_file);
        if (labels != null) {
            storeAttributes(ses, labels);
            System.out.println("Set default labels to session "+ses.getId());
        }

        //
        // store locale specific values to override default values
        //
        labels = getLocalizedLabels(locale, label_file);
        if (labels != null) {
            storeAttributes(ses, labels);
            System.out.println("Set labels of "+locale+" to session "+ses.getId());
        }
    }

    /**
     * extract locale information from HttpServletRequest
     */
    protected final String getLocale(HttpServletRequest req)
    {
        return req.getHeader(LOCALE_HEADER);
    }

    /**
     *  forward to a page in package
     *  @param page relative path to the package
     */
    protected void forwardToModule(String module_key, String page,
                            HttpServletRequest req,
                            HttpServletResponse res)
        throws IOException,ServletException
    {
        forward(null,getModuleJspLocation(module_key) + "/" + page, req, res);
    }

    /**
     *  forward to a page in package
     *  @param page relative path to the package
     */
    protected void forwardToModule(String locale, String module_key, String page,
                            HttpServletRequest req,
                            HttpServletResponse res)
        throws IOException,ServletException
    {
        forward(locale, getModuleJspLocation(module_key) + "/" + page, req, res);
    }

    /**
     *  forward to a page specified
     *  use HttpServletResponce.encodeURL to embed session ID if necessary
     *  @param page URL to forward to
     */
    protected void forward(String page,
                            HttpServletRequest req,
                            HttpServletResponse res)
        throws IOException,ServletException
    {
        forward(null,page,req,res);
    }

    /**
     *  forward to a page specified
     *  use HttpServletResponce.encodeURL to embed session ID if necessary
     *  @param page URL to forward to
     */
    protected void forward(String locale, String page,
                            HttpServletRequest req,
                            HttpServletResponse res)
        throws IOException,ServletException
    {
        if (locale == null)
            locale = "";
        //
        // forward to the specified page
        //
        RequestDispatcher rd;
        ServletContext sc = getServletContext();
        String valid_url = res.encodeURL(locale+page);

        System.out.println(getClass().getName() + " forward with: " + valid_url);
        rd = sc.getRequestDispatcher(valid_url);
        rd.forward(req, res);
    }
    
    /**
     *  authenticate the user session for the page.
     *  @param url: url of the page
     *  @param req: HttpServletRequest
     *  @param res: HttpServletResponse
     */
    protected boolean doAuthenticate(String url, 
                            HttpServletRequest req,
                            HttpServletResponse res)
        throws IOException,ServletException
    {
        HttpSession ses = req.getSession(false);
        Object role = ses.getAttribute(SES_ROLE);
        if (role==null || Integer.parseInt(role.toString())<0)
        {
            forward("/login?url="+url, req, res);
            return false;
        } else 
            return true;
    }
    
    /**
     *  file read utility, returns string of the file content.
     *  @param filename: file name
     */
    protected static String readFile(String filename) 
        throws IOException 
    {
        String lineSep = System.getProperty("line.separator");
        BufferedReader br = new BufferedReader(new FileReader(filename));
	try {
	    String nextLine = "";
	    StringBuffer sb = new StringBuffer();
	    while ((nextLine = br.readLine()) != null) {
		sb.append(nextLine);
		//
		// note:
		//   BufferedReader strips the EOL character.
		//
		sb.append(lineSep);
	    }
	    return sb.toString();
	} finally {
	    br.close();
	}
    }

    /**
     *  get java.util.Properties object out of locale specific 
     *  LabelsBundle.properties file.
     *  @param locale the language.
     */
    private Properties getLocalizedLabels(String locale, String label_file)
    {
        Object mutex = new Object();
        String lang = extractLanguage(locale);

        synchronized(mutex) {
            Properties prop = (Properties)mLabels.get(lang);
            if (prop == null) {	        
                prop = getProperties(label_file + "." + lang);

                if (prop != null) {
                    System.out.println("Load labels for " + lang + " language");
                    mLabels.put(lang, prop);
                }
            }

            return prop;
        }
    }

    /** 
     *  get java.util.Properties object out of default
     *  LabelsBundle.properties file
     */
    private Properties getDefaultLabels(String label_file)
    {
        Properties prop = (Properties)mLabels.get(DEFAULT_LABELS);
        if (prop == null) {            
            prop = getProperties(label_file);
            if (prop != null) {
                System.out.println("Load labels with file: " + label_file);
                mLabels.put(DEFAULT_LABELS, prop);
	    }
        }

        return prop;
    }

    /**
     *  Store name=value pair attributes into the session
     */
    private void storeAttributes(HttpSession ses, Properties prop)
    {
        String name = null, value = null;
        Enumeration names = prop.propertyNames();
        while(names.hasMoreElements()) {
            name = (String)names.nextElement();
            value = prop.getProperty(name, "");
            ses.setAttribute(name, value);
        }
    }

    /**
     * Open and load a java.util.Properties file.
     * This function has to be called within a mutex control
     */
    private Properties getProperties(String prop_file)
    {
	if (prop_file == null) {
	    return null;
	}

        Properties prop = new Properties();

	try {
	    String fullPath = getServletContext().getRealPath(prop_file);
	    FileInputStream in = new FileInputStream(fullPath);
	    prop.load(in);
	    in.close();
	} catch (IOException ioEx) {
	    return null;
	}

        return prop;
    }

    /**
     *  get a location of the package
     */
    private String getModuleJspLocation(String module_key)
    {
        return getLocation(module_key);
    }

    private String getLocation(String name)
    {
        if (mAdminProp == null)
            return DEFAULT_LOCATION;

        String ret = (String)mAdminProp.get(name);
        if (ret == null)
            return DEFAULT_LOCATION;

        return ret;
    }

    /**
     *  extract language section out of locale string
     *  extract only language section out of locale string
     */
    private String extractLanguage(String locale)
    {
        if (locale == null)
            return "";
      
        // skip preceeding white spaces
        int spc = locale.indexOf(' ');
        int lastspc = 0;
        while (spc >= 0) {
            lastspc = spc;
            spc = locale.indexOf(' ', spc+1);
        }

        // look for one of separating characters
        int idx  = smallerIndex(locale.indexOf('-', lastspc), 
                                locale.indexOf('_', lastspc));
        idx = smallerIndex(idx, locale.indexOf('.', lastspc));
        idx = smallerIndex(idx, locale.indexOf(';', lastspc));
        idx = smallerIndex(idx, locale.indexOf(',', lastspc));
        idx = smallerIndex(idx, locale.indexOf(':', lastspc));

        if (idx >= 0) {
            // use up to the character
            return locale.substring(lastspc, idx);
        }

        return locale;
    }

   
    private int smallerIndex(int i1, int i2)
    {
        if (i1 < 0)
            return i2;

        if (i2 < 0)
            return i1;

        return (i1 > i2) ? i2 : i1;
    }

}

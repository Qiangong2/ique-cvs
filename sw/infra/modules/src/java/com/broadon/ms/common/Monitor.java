package com.broadon.ms.common;

import java.sql.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import javax.sql.DataSource;
import oracle.xml.sql.query.OracleXMLQuery;

import com.broadon.servlet.ContextStatus;
import com.broadon.servlet.ServletConstants;

public class Monitor extends AdminServlet
{
    static final String dbTest = "SELECT SYSDATE FROM DUAL";

    ServletContext ctx;

    public void init(ServletConfig config) throws ServletException {
	super.init(config);
	ctx = config.getServletContext();
    }


    public void doPost(HttpServletRequest req, HttpServletResponse res)
        throws IOException, ServletException
    {
        doGet(req, res);            
    }


    public void doGet(HttpServletRequest req, HttpServletResponse res)
        throws IOException, ServletException
    {  
	ContextStatus status = (ContextStatus)
	    ctx.getAttribute(ServletConstants.MONITOR_STATUS_KEY);
	
	if (status.getOperationStatus() == status.OS_LB_NOTIFIED) {
	    res.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
	    status.setHealth(false);
	    return;
	}

	Connection conn = null;
	OracleXMLQuery qry = null;
	
        try
        {
	    // test database access
            DataSource ds = super.dataSource;
            conn = ds.getConnection();

	    // test XML query library
	    qry = new OracleXMLQuery(conn, dbTest);
	    String xml = qry.getXMLString();

	    // test jsp processing
	    forwardToModule("JSP_LOCATION", "monitor.jsp", req, res);
	    status.setHealth(true);
	    res.setStatus(res.SC_OK);
	} catch (Exception e) {
	    // anything wrong is not healthy
	    status.setHealth(false);
	    res.sendError(res.SC_INTERNAL_SERVER_ERROR);
	} finally {
	    if (qry != null)
		qry.close();
	    if (conn != null) {
		try {
		    conn.close();
		} catch (SQLException e) {}
	    }
	}
    }
}



package com.broadon.cps;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

import org.bouncycastle.crypto.PBEParametersGenerator;
import org.bouncycastle.crypto.generators.PKCS5S2ParametersGenerator;
import org.bouncycastle.crypto.params.KeyParameter;

import com.broadon.ets.ETicketAttributes;
import com.broadon.exception.BroadOnException;
import com.broadon.wsapi.cps.ContentLinkType;
import com.broadon.wsapi.cps.PublishRequestType;

/**
 * Misc. functions shared by both CPS and CLS.
 */
public class PublishUtil
{
    public static final String TMD_GEN = "com.broadon.cps.TMDGenerator";

    private static final byte[] CPS_SECRET = { -3, 4, 1, 5, 6, 11, 17, 28, 45, 73 };

    // NC beta/devel key
    private static final byte[] NC_COMMON_KEY = { 103, 69, -117, 107, -58, 35, 123, 50, 105, -104, 60, 100, 115, 72, 51, 102 };

    // Wii devel key
    private static final byte[] WII_COMMON_KEY = { -95, 96, 74, 106, 113, 35, -75, 41, -82, -117, -20, 50, -56, 22, -4, -86 };


    static public final String COMMON_KEY_NAME = "COMMON_KEY_NAME";
    static public final String DEFAULT_COMMON_KEY_NAME = "CM";
    static public final String NC_COMMON_KEY_NAME = "iQueDevel-CM";
    static public final String WII_COMMON_KEY_NAME = "devel-CM";
    
    private static final int CPS_KEYGEN_ITERATION = 20;

    public static byte[] getCommonKey(String keyName)
    {
        if (NC_COMMON_KEY_NAME.equals(keyName))
            return NC_COMMON_KEY;
        else if (WII_COMMON_KEY_NAME.equals(keyName))
            return WII_COMMON_KEY;
        else
            return null;
    }

    /**
     * Expand the custom data field to the proper length
     */
    public static void setupCustomData(PublishRequestType publishRequest)
    {
        byte[] customData = publishRequest.getCustomData();

        if (customData != null
                && customData.length >= TMDGenerator.customDataSize)
            return;

        byte[] data = new byte[TMDGenerator.customDataSize];
        if (customData == null) {
            // we allow it to be optional
            Arrays.fill(data, (byte) 0);
        } else {
            System.arraycopy(customData, 0, data, 0, customData.length);
            Arrays.fill(data, customData.length, data.length - 1, (byte) 0);
        }
        publishRequest.setCustomData(data);
    }

    /**
     * Password-based key generation, using the title ID (hashed with a built-in
     * secret) as seed.
     * 
     * @param isSubscription
     *            For subscription title, mask off the title ID part and use the
     *            channel ID as seed.
     * @return encrypt key.
     * @throws NoSuchAlgorithmException
     */
    public static byte[] generateTitleKey(String password, long titleID, boolean isSubscription,
                                          boolean useUnsafeAlgorithm) 
        throws NoSuchAlgorithmException
    {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(CPS_SECRET);
        // all subscription channel share the same key, so we seed the key
        // generation with just the channel title ID.
        long effectiveTID = isSubscription ? getChannelTitleID(titleID)
                : titleID;
        byte[] seed = md.digest(BigInteger.valueOf(effectiveTID).toByteArray());
        char[] pw = (password == null) ? new char[0] : password.toCharArray();
        if (! useUnsafeAlgorithm) {
            PBEParametersGenerator generator = new PKCS5S2ParametersGenerator();
            generator.init(PBEParametersGenerator.PKCS5PasswordToBytes(pw), seed,
                    CPS_KEYGEN_ITERATION);
            KeyParameter params = (KeyParameter) generator.generateDerivedParameters(TMDGenerator.aesKeySize * 8);
            // KeyParameter params = (KeyParameter) generator.generateDerivedParameters(128);
            return params.getKey();
        } else {
            // HACK ALERT
            // based on an undocumented property file variable, we ignore the
            // password and use the built-in key generation
            String tid = Long.toHexString(effectiveTID);
            if (tid.length() < 16) {
                // add leading zero's
                StringBuffer buf = new StringBuffer(tid);
                char[] addition = new char[16 - tid.length()];
                Arrays.fill(addition, '0');
                buf.insert(0, addition);
                tid = buf.toString();
            }
            tid = tid + "rvlwii";
            
            md.reset();
            return md.digest(tid.getBytes());
        }
    }

    public static long getChannelTitleID(long titleID)
    {
        return titleID & 0xFFFFFFFF00000000L;
    }

    public static int getAttributes(Boolean allowCommonTicket)
    {
        if (allowCommonTicket == null || !allowCommonTicket.booleanValue())
            return 0;
        ETicketAttributes attr = new ETicketAttributes(0);
        attr.setAllowCommonTicket(allowCommonTicket.booleanValue());
        return attr.getAtributes();
    }

    /**
     * Create a map of content attributes from the TMD and upload package, using
     * the checksum of each content object file as key.
     */
    public static Map genContentAttributesMap(ContentLinkType[] contentLink,
            ContentMetaData[] cmd, long titleID) throws BroadOnException
    {
        final String errMsg = "Upload package is corrupted";

        if (contentLink.length != cmd.length)
            throw new BroadOnException(errMsg);

        // generate the map from checksum to content info
        TreeMap contentMap = new TreeMap();

        mapping: for (int i = 0; i < contentLink.length; i++) {
            long cid = Long.parseLong(contentLink[i].getContentId(), 16);
            for (int j = 0; j < cmd.length; ++j) {
                if (cid == cmd[j].getRawContentId()) {
                    contentMap.put(contentLink[i].getCheckSum(),
                            new ContentAttributes(contentLink[i], cmd[j],
                                    titleID));
                    continue mapping;
                }
            }
            throw new BroadOnException(errMsg);
        }

        return contentMap;
    }
/*    
    public static void main(String args[])
    {
        String password = "mypassword";
        long titleID = 0x00101010babecafeL;
        
        System.out.println("Title ID: 0x" + Long.toHexString(titleID));
        
        try {
            
            byte[] key = generateTitleKey(password, titleID, false, true);
            System.out.println("Individual game:\t" + new BigInteger(1, key).toString(16));

            key = generateTitleKey(password, titleID, true, true);
            System.out.println("Subscription channel:\t" + new BigInteger(1, key).toString(16));
            
            titleID = 0x1L;
            System.out.println("Title ID: 0x" + Long.toHexString(titleID));
            
            key = generateTitleKey(password, titleID, false, true);
            System.out.println("Individual game:\t" + new BigInteger(1, key).toString(16));

            key = generateTitleKey(password, titleID, true, true);
            System.out.println("Subscription channel:\t" + new BigInteger(1, key).toString(16));
            
            
            titleID = 0x7fffffffffffffffL;
            System.out.println("Title ID: 0x" + Long.toHexString(titleID));
            key = generateTitleKey(password, titleID, false, true);
            System.out.println("Individual game:\t" + new BigInteger(1, key).toString(16));

            key = generateTitleKey(password, titleID, true, true);
            System.out.println("Subscription channel:\t" + new BigInteger(1, key).toString(16));
            
            titleID = 0x8000000000000000L;
            System.out.println("Title ID: 0x" + Long.toHexString(titleID));
            key = generateTitleKey(password, titleID, false, true);
            System.out.println("Individual game:\t" + new BigInteger(1, key).toString(16));

            key = generateTitleKey(password, titleID, true, true);
            System.out.println("Subscription channel:\t" + new BigInteger(1, key).toString(16));
            
            titleID = 0xffffffffffffffffL;
            System.out.println("Title ID: 0x" + Long.toHexString(titleID));
            key = generateTitleKey(password, titleID, false, true);
            System.out.println("Individual game:\t" + new BigInteger(1, key).toString(16));

            key = generateTitleKey(password, titleID, true, true);
            System.out.println("Subscription channel:\t" + new BigInteger(1, key).toString(16));
            
            titleID = 0x0000000100000003L;
            System.out.println("Title ID: 0x" + Long.toHexString(titleID));
            key = generateTitleKey(password, titleID, false, true);
            System.out.println("Individual game:\t" + new BigInteger(1, key).toString(16));

            key = generateTitleKey(password, titleID, true, true);
            System.out.println("Subscription channel:\t" + new BigInteger(1, key).toString(16));
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
*/
}

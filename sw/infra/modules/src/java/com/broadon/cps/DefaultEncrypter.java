package com.broadon.cps;

public class DefaultEncrypter implements Encrypter
{
    private static native int getEncrytionBufferSize();
    
    private static native byte[] encryptContentInit(int index, byte[] titleAesKey);

    private static native int encryptContentUpdate(byte[] handle, byte[] inBuf, int inOffset, 
                                                   int len, byte[] outBuf, int outOffset);

    private static native int encryptContentFinal(byte[] handle, byte[] outBuf, int off, byte[] hash);


    public byte[] encryptInit(int index, byte[] titleAesKey)
    {
        return encryptContentInit(index, titleAesKey);
    }

    public int encryptUpdate(byte[] handle, byte[] inBuf, int inOffset, int len, byte[] outBuf, int outOffset)
    {
       return encryptContentUpdate(handle, inBuf, inOffset, len, outBuf, outOffset);
    }

    public int encryptFinal(byte[] handle, byte[] outBuf, int off, byte[] hash)
    {
        return encryptContentFinal(handle, outBuf, off, hash);
    }

    public int getBufferSize()
    {
       return getEncrytionBufferSize();
    }

}

#include <netinet/in.h>

#include "x86/esl.h"
#include "es_int.h"

#include "TitleMetaData.h"

static jfieldID fid_issuer = NULL;
static jfieldID fid_titleId = NULL;
static jfieldID fid_osTitleId = NULL;
static jfieldID fid_titleType = NULL;
static jfieldID fid_groupId = NULL;
static jfieldID fid_accessRights = NULL;
static jfieldID fid_TMDVersion = NULL;
static jfieldID fid_bootIndex = NULL;
static jfieldID fid_contents = NULL;
static jfieldID fid_hashOffset = NULL;
static jfieldID fid_sigOffset = NULL;
static jfieldID fid_sigSize = NULL;

static jfieldID fid_contentId = NULL;
static jfieldID fid_index = NULL;
static jfieldID fid_contentType = NULL;
static jmethodID mid_CMD = NULL;

static bool init_ok = false;

static char* ContentMetaData = "com/broadon/cps/ContentMetaData";

JNIEXPORT void JNICALL
Java_com_broadon_cps_TitleMetaData_initParser(JNIEnv* e, jclass cls)
{
    fid_issuer = e->GetFieldID(cls, "issuer", "Ljava/lang/String;");
    fid_titleId = e->GetFieldID(cls, "titleId", "J");
    fid_osTitleId = e->GetFieldID(cls, "osTitleId", "J");
    fid_titleType = e->GetFieldID(cls, "titleType", "I");
    fid_groupId = e->GetFieldID(cls, "groupId", "I");
    fid_accessRights = e->GetFieldID(cls, "accessRights", "I");
    fid_TMDVersion = e->GetFieldID(cls, "TMDVersion", "I");
    fid_bootIndex = e->GetFieldID(cls, "bootIndex", "I");
    fid_contents = e->GetFieldID(cls, "contents", "[Lcom/broadon/cps/ContentMetaData;");
    fid_hashOffset = e->GetFieldID(cls, "hashOffset", "I");
    fid_sigOffset = e->GetFieldID(cls, "sigOffset", "I");
    fid_sigSize = e->GetFieldID(cls, "sigSize", "I");

    init_ok =  (fid_issuer && fid_titleId && fid_osTitleId && fid_titleType &&
		fid_groupId && fid_accessRights && fid_TMDVersion &&
		fid_bootIndex && fid_contents && fid_hashOffset &&
		fid_sigOffset && fid_sigSize);

    jclass cls_CMD = e->FindClass(ContentMetaData);
    if (cls_CMD == NULL) {
	init_ok = false;
	return;
    }
    mid_CMD = e->GetMethodID(cls_CMD, "<init>", "()V");
 
    fid_contentId = e->GetFieldID(cls_CMD, "contentId", "J");
    fid_index = e->GetFieldID(cls_CMD, "index", "S");
    fid_contentType = e->GetFieldID(cls_CMD, "contentType", "I");

    init_ok &= (mid_CMD && fid_contentId && fid_index && fid_contentType);
}



static int
parseTMD(ESTitleMeta& tmd, JNIEnv* e, jbyteArray a)
{
    if (a == NULL)
	return -1;

    unsigned int size = e->GetArrayLength(a);

    // Ignore the extra bytes after the tmd structure.
    if (size > sizeof(ESTitleMeta))
	size = sizeof(ESTitleMeta);

    e->GetByteArrayRegion(a, 0, size, (jbyte*) &tmd);

    if (tmd.head.version != ES_TMD_VERSION)
	return -1;

    unsigned int tmdSize = ES_GetTmdSize(ntohs(tmd.head.numContents));

    if (tmdSize > size) {
	return -1;
    }

    ESTitleMetaHeader& head = tmd.head;
    head.sysVersion = ntohll(head.sysVersion);
    head.type = ntohl(head.type);
    head.titleId = ntohll(head.titleId);
    head.groupId = ntohs(head.groupId);
    head.accessRights = ntohl(head.accessRights);
    head.titleVersion = ntohs(head.titleVersion);
    head.numContents = ntohs(head.numContents);
    head.bootIndex = ntohs(head.bootIndex);

    for (int i = 0; i < head.numContents; ++i) {
	ESContentMeta& cm = tmd.contents[i];
	cm.cid = ntohl(cm.cid);
	cm.index = ntohs(cm.index);
	cm.type = ntohs(cm.type);
	cm.size = ntohll(cm.size);
    }

    return tmdSize;
}


JNIEXPORT jbyteArray JNICALL
Java_com_broadon_cps_TitleMetaData_parseTMD(JNIEnv* env, jobject obj, jbyteArray _tmd)
{
    if (! init_ok) {
	return NULL;
    }

    ESTitleMeta tmd;
    int tmdSize = parseTMD(tmd, env, _tmd);
    if (tmdSize < 0) {
	return NULL;
    }

    env->SetObjectField(obj, fid_issuer, env->NewStringUTF((char*) tmd.sig.issuer));

    ESTitleMetaHeader& header = tmd.head;
    env->SetLongField(obj, fid_titleId, (jlong) header.titleId);
    env->SetLongField(obj, fid_osTitleId, (jlong) header.sysVersion);
    env->SetIntField(obj, fid_titleType, (jint) header.type);
    env->SetIntField(obj, fid_groupId, (jint) header.groupId);
    env->SetIntField(obj, fid_accessRights, (jint) header.accessRights);
    env->SetIntField(obj, fid_TMDVersion, (jint) header.titleVersion);
    env->SetIntField(obj, fid_bootIndex, (jint) header.bootIndex);
    env->SetIntField(obj, fid_hashOffset, (jint) (tmd.sig.issuer - (u8*) &tmd));
    env->SetIntField(obj, fid_sigOffset, (jint) (tmd.sig.sig - (u8*) &tmd));
    env->SetIntField(obj, fid_sigSize, (jint) sizeof(tmd.sig.sig));


    jclass cls = env->FindClass(ContentMetaData);
    jobjectArray contents = env->NewObjectArray(header.numContents, cls, NULL);
    if (contents == NULL) {
	return NULL;
    }

    for (int i = 0; i < header.numContents; ++i) {
	jobject content = env->NewObject(cls, mid_CMD);
	if (content == NULL) {
	    env->DeleteLocalRef(contents);
	    return NULL;
	}

	ESContentMeta& cm = tmd.contents[i];
	env->SetLongField(content, fid_contentId, (jlong) cm.cid);
	env->SetShortField(content, fid_index, (jshort) cm.index);
	env->SetIntField(content, fid_contentType, (jint) cm.type);
	
	env->SetObjectArrayElement(contents, i, content);

	env->DeleteLocalRef(content);
    }

    env->SetObjectField(obj, fid_contents, (jobject) contents);
    env->DeleteLocalRef(contents);

    jbyteArray result = env->NewByteArray(tmdSize);
    if (result) {
	// get back the original tmd
	env->GetByteArrayRegion(_tmd, 0, tmdSize, (jbyte*) &tmd);
	env->SetByteArrayRegion(result, 0, tmdSize, reinterpret_cast<jbyte*>(&tmd));
    }
    
    return result;
}

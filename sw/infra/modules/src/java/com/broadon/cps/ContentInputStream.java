package com.broadon.cps;

import java.io.FilterInputStream;
import java.io.InputStream;

public class ContentInputStream extends FilterInputStream
{
    protected String md5HashString;       // md5 hash of the filtered input converted to hex string
    protected byte[] sha1Hash;            // sha1 hash of the filtered input
    protected int originalSize;           // size of input before filtering
    protected int filteredSize;           // size of input after filtering
    
    public ContentInputStream(InputStream in) {
        super(in);
        md5HashString = null;
        sha1Hash = null;
        originalSize = 0;
        filteredSize = 0;
    }

    public String getMd5HashString()
    {
        return md5HashString;
    }

    public byte[] getSha1Hash()
    {
        return sha1Hash;
    }

    public int getOriginalSize()
    {
        return originalSize;
    }

    public int getFilteredSize()
    {
        return filteredSize;
    }
}

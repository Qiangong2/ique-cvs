package com.broadon.cps;

import java.math.BigInteger;


public class ContentMetaData
{
    long contentId;
    short index;
    int contentType;
    
    public ContentMetaData()
    {
        super();
    }
    
    public long getRawContentId()
    {
        return contentId;
    }
    
    public BigInteger getFullContentId(long titleID)
    {
        // BigInteger version of ((titleID << 32) | contentID)
        return BigInteger.valueOf(titleID).shiftLeft(32).or(BigInteger.valueOf(contentId));  
    }

    public int getContentType()
    {
        return contentType;
    }

    public short getIndex()
    {
        return index;
    }   
}

package com.broadon.cps;

import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;

import com.broadon.util.HexString;

public class EncryptedContentInputStream extends ContentInputStream
{
    private byte[] handle;
    private MessageDigest md;
    private final int bufAlignment;
    private byte[] outBuf;
    private int offset;
    private int avail;
    private byte[] inBuf;
    private Encrypter crypt;
    
    public EncryptedContentInputStream(Encrypter crypt, InputStream in, byte[] aesKey, 
                                       int ivSeed, int hashSize)
        throws GeneralSecurityException 
    {
        super(in);
        this.crypt = crypt;
        handle = crypt.encryptInit(ivSeed, aesKey);
        if (handle == null)
            throw new GeneralSecurityException("Error initializing encryption engine");
        md = MessageDigest.getInstance("MD5");
        bufAlignment = crypt.getBufferSize();
        outBuf = null;
        offset = 0;
        avail = 0;
        
        sha1Hash = new byte[hashSize];
        inBuf = new byte[16*1024];
    }

    public int available() throws IOException
    {
        return avail;
    }

    public synchronized void mark(int readlimit)
    {
        return;
    }

    public int read() throws IOException
    {
        byte[] b = new byte[1];
        int n;
        
        while ((n = read(b, 0, b.length)) == 0);
        
        if (n == 1)
            return (int)b[0];
        else
            return -1;
    }

    public int read(byte[] b, int off, int len) throws IOException
    {
        if (len > inBuf.length) {
            inBuf = new byte[len];
        }
        
        int n = 0;
        if (avail > 0) {
            n = (avail <= len) ? avail : len;
            System.arraycopy(outBuf, offset, b, off, n);
            avail -= n;
            offset += n;
        } else {
            boolean bufferOutput = len < bufAlignment;
            byte[] out;
            int outOff;
            
            // decide if writing directly to the user buffer or use a local one
            if (bufferOutput) {
                if (outBuf == null)
                    outBuf = new byte[bufAlignment];
                out = outBuf;
                outOff = 0;
            } else {
                out = b;
                outOff = off;
            }
            
            do {
                n = in.read(inBuf);
                if (n > 0) {
                    originalSize += n;
                    n = crypt.encryptUpdate(handle, inBuf, 0, n, out, outOff);
                } else if (n < 0) {
                    if (md5HashString == null) {
                        n = crypt.encryptFinal(handle, out, outOff, sha1Hash);
                        if (n > 0) {
                            md.update(out, outOff, n);
                            md5HashString = HexString.toHexString(md.digest());
                        } else if (n == 0) {
                            md5HashString = HexString.toHexString(md.digest());
                            return -1;
                        }
                    } else
                        return -1;              // eof already reached in previous call, report it again
                }
                
                if (n < 0)
                    throw new IOException("Encryption error " + n);
            } while (n <= 0);
            
            // some data has been read into the output buffer
            if (md5HashString == null)
                md.update(out, outOff, n);
            
            if (bufferOutput) {
                avail = n;
                offset = 0;
                n = (avail <= len) ? avail : len;
                System.arraycopy(outBuf, offset, b, off, n);
                avail -= n;
                offset += n;
            }
        }
        
        filteredSize += n;
        return n;
    }

    public int read(byte[] b) throws IOException
    {
        return read(b, 0, b.length);
    }

    public synchronized void reset() throws IOException
    {
        return;
    }

    public long skip(long n) throws IOException
    {
        return 0;
    }

    public boolean markSupported() {
        return false;
    }

/*
    public static void main(String[] args) {
        try {
            FileInputStream infile = new FileInputStream(args[0]);
            TrivialEncrypter te = new TrivialEncrypter();
            EncryptedContentInputStream inStream = new EncryptedContentInputStream(te, infile, null, 0, 20);
            FileOutputStream outfile = new FileOutputStream(args[1]);
            byte[] buf = new byte[16*1024];
            int n;
            while ((n = inStream.read(buf)) >= 0) {
                outfile.write(buf, 0, n);
            }
            inStream.close();
            outfile.close();
            System.out.println("original size = " + inStream.originalSize);
            System.out.println("md5sum = " + inStream.getMd5HashString());
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
*/
}

#include "x86/esl.h"

#include "DefaultEncrypter.h"

#define FUNC(f) Java_com_broadon_cps_DefaultEncrypter_ ## f

JNIEXPORT jint JNICALL
FUNC(getEncrytionBufferSize)(JNIEnv*, jclass)
{
    return ES_AES_BUF_SIZE;
}


JNIEXPORT jbyteArray JNICALL
FUNC(encryptContentInit)(JNIEnv* env, jclass, jint _index, jbyteArray _titleAesKey)
{
    ESAesShaHandle handle;

    u8* titleAesKey = reinterpret_cast<u8*>(env->GetByteArrayElements(_titleAesKey, NULL));
    if (titleAesKey == NULL ||
	env->GetArrayLength(_titleAesKey) != ES_AES_KEY_SIZE)
	return NULL;
    
    int ret = ES_EncryptContentReset(&handle, _index, titleAesKey);
    env->ReleaseByteArrayElements(_titleAesKey, reinterpret_cast<jbyte*>(titleAesKey),
				  JNI_ABORT);
    if (ret != ES_ERR_OK)
	return NULL;

    jbyteArray result = env->NewByteArray(sizeof(handle));
    if (result) {
	env->SetByteArrayRegion(result, 0, sizeof(handle),
				reinterpret_cast<jbyte*>(&handle));
    }
    return result;
    
} // encryptContentInit


JNIEXPORT jint JNICALL
FUNC(encryptContentUpdate)(JNIEnv* env, jclass, jbyteArray _handle,
			   jbyteArray _inBuf, jint _inOffset, jint _len,
			   jbyteArray _outBuf, jint _outOffset)
{
    ESAesShaHandle* handle =
	reinterpret_cast<ESAesShaHandle*>(env->GetByteArrayElements(_handle, NULL));
    u8* inBuf = reinterpret_cast<u8*>(env->GetByteArrayElements(_inBuf, NULL));
    u8* outBuf = reinterpret_cast<u8*>(env->GetByteArrayElements(_outBuf, NULL));
    int outSize = env->GetArrayLength(_outBuf);

    if (handle == NULL || inBuf == NULL || outBuf == NULL ||
	env->GetArrayLength(_handle) != sizeof(ESAesShaHandle) ||
	(outSize - _outOffset) < 0)
	return -1;
    

    int ret = ES_EncryptContentUpdate(handle, inBuf + _inOffset, _len,
				      outBuf + _outOffset, outSize - _outOffset);

	env->ReleaseByteArrayElements(_handle, reinterpret_cast<jbyte*>(handle), 0);

    env->ReleaseByteArrayElements(_inBuf, reinterpret_cast<jbyte*>(inBuf), JNI_ABORT);
    env->ReleaseByteArrayElements(_outBuf, reinterpret_cast<jbyte*>(outBuf),
				  (ret > 0) ? 0 : JNI_ABORT);
    return ret;
} // encryptContentUpdate


JNIEXPORT jint JNICALL
FUNC(encryptContentFinal)(JNIEnv* env, jclass, jbyteArray _handle,
			  jbyteArray _outBuf, jint _off, jbyteArray _hash)
{
    if (env->GetArrayLength(_hash) < ES_HASH_SIZE)
	return ES_ERR_UNKNOWN;

    ESAesShaHandle* handle =
	reinterpret_cast<ESAesShaHandle*>(env->GetByteArrayElements(_handle, NULL));
    u8* outBuf = reinterpret_cast<u8*>(env->GetByteArrayElements(_outBuf, NULL));
    int outSize = env->GetArrayLength(_outBuf);
    u8* hash = reinterpret_cast<u8*>(env->GetByteArrayElements(_hash, NULL));

    if (handle == NULL || outBuf == NULL || hash == NULL)
	return -1;


    int ret = ES_EncryptContentFinal(handle, outBuf + _off, outSize - _off, hash);

    env->ReleaseByteArrayElements(_handle, reinterpret_cast<jbyte*>(handle), 0);
    env->ReleaseByteArrayElements(_outBuf, reinterpret_cast<jbyte*>(outBuf),
				  (ret > 0) ? 0 : JNI_ABORT);
    env->ReleaseByteArrayElements(_hash, reinterpret_cast<jbyte*>(hash),
				  (ret >= 0) ? 0 : JNI_ABORT);

    return ret;
} // encryptFinal

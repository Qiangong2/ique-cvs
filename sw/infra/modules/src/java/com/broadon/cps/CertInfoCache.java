package com.broadon.cps;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.broadon.db.DataSourceScheduler;
import com.broadon.db.OracleDataSource.OracleDataSourceProxy;
import com.broadon.util.BackingStore;
import com.broadon.util.BackingStoreException;
import com.broadon.util.Base64;
import com.broadon.util.CacheLFU;
import com.broadon.util.Pair;

public class CertInfoCache extends CacheLFU implements BackingStore
{
    private static final String CRL_INFO =
        "SELECT CRL_TYPE, CRL_VERSION FROM CURRENT_CRLS " +
        "WHERE CRL_TYPE IN ('CPCRL', 'CACRL')"; 
        
    private static final String KEY_INFO =
        "SELECT KEY_NAME, CHAIN_ID FROM CLIENT_HSM_CERTS WHERE CERT_ID = ? AND KEY_TYPE = ?";
            
    private static final String CERT_INFO =
        "SELECT CN, DECODE(A.CERT_ID, B.SIGNER_CERT_ID, 'SIGNER', 'CA') AS CERT_TYPE, CERTIFICATE " +
        "FROM CERTIFICATES A, CERTIFICATE_CHAINS B " +
        "WHERE (A.CERT_ID = B.SIGNER_CERT_ID OR A.CERT_ID = B.CA_CERT_ID) AND " +
        "      B.CHAIN_ID = ?";
    
    private DataSource ds;

    private int signerCrlVersion;
    private int caCrlVersion;
    
    public CertInfoCache(DataSource ds) {
        super(100, -1);
        this.ds = ds;
        readCrlInfo();
    }

    private void readCrlInfo()
    {
        Connection conn = null;
        PreparedStatement ps = null;
        signerCrlVersion = -1;
        caCrlVersion = -1;
        
        try {
            conn = getConnection(DataSourceScheduler.DB_PRIORITY_FAST);
            ps = conn.prepareStatement(CRL_INFO);
            ResultSet rs = ps.executeQuery();
            
            while (rs.next()) {
               int version = rs.getInt("CRL_VERSION");
               if ("CPCRL".equals(rs.getString("CRL_TYPE")))
                   signerCrlVersion = version;
               else
                   caCrlVersion = version;
            }
        } catch (SQLException e) {
        } finally {
            try {
                if (ps != null)
                    ps.close();
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {}
        }
    }
    
    private Connection getConnection(int priority) throws SQLException
    {
        return ds instanceof OracleDataSourceProxy ? 
               ((OracleDataSourceProxy) ds).getConnection(priority) :
               ds.getConnection();
    }


    public Object retrieve(Object signerKey) throws BackingStoreException
    {
        String certID = (String) ((Pair)signerKey).first;
        
        String titleType = (String) ((Pair)signerKey).second;
        
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            conn = ds.getConnection();
            ps = conn.prepareStatement(KEY_INFO);
            ps.setString(1, certID);
            ps.setString(2, titleType);
            rs = ps.executeQuery();
            if (rs.next()) {
                String keyName = rs.getString("KEY_NAME");
                int chainID = rs.getInt("CHAIN_ID");
                if (! rs.wasNull() && ! "".equals(keyName)) {
                    rs.close();
                    ps.close();
                    
                    ps = conn.prepareStatement(CERT_INFO);
                    ps.setInt(1, chainID);
                    rs = ps.executeQuery();
                    
                    String signerName = null;
                    String caName = null;
                    byte[][] certChain = new byte[2][];
                    Base64 base64 = new Base64();
                    
                    while (rs.next()) {
                        String subject = rs.getString("CN");
                        byte[] cert = base64.decode(rs.getString("CERTIFICATE")); 
                        if ("CA".equals(rs.getString("CERT_TYPE"))) {
                            caName = subject;
                            certChain[1] = cert;
                        } else {
                            signerName = subject;
                            certChain[0] = cert;
                        }
                    }
                   
                    return new Value(chainID, certChain, keyName, signerName, caName, 
                                     signerCrlVersion, caCrlVersion);
                }
            }
            throw new BackingStoreException("Cannot locate " + titleType + " key for " + certID);
        } catch (SQLException e) {
            throw BackingStoreException.rewrap(e);
        } catch (IOException e) {
            BackingStoreException be = new BackingStoreException(e.getMessage());
            be.setStackTrace(e.getStackTrace());
            throw be;
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (ps != null)
                    ps.close();
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {}
        }
    }

    public static class Value
    {
        public final int chainID;
        public final String signerName;
        public final String subjectName;
        public final String caName;
        public final int signerCrlVersion;
        public final int caCrlVersion;
        public final byte[][] certChain;
        
        public Value(int chainID, byte[][] certChain, String signer, String subjectName, 
                     String caName, int signerCrl, int caCrl) 
            throws BackingStoreException 
        {
            this.chainID = chainID;
            this.certChain = certChain;
            this.signerName = signer;
            this.subjectName = subjectName;
            this.caName = caName;
            signerCrlVersion = signerCrl;
            caCrlVersion = caCrl;
            
            if (subjectName == null || caName == null)
                throw new BackingStoreException("Cannot locate cert. chain " + chainID);
            if (signerCrl < 0 || caCrl < 0)
                throw new BackingStoreException("Cannot locate CRL versions");
        }
    }
}

package com.broadon.cps;

import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.InvalidParameterException;
import java.util.List;

import com.broadon.exception.BroadOnException;
import com.broadon.wsapi.cps.ContentAttributeType;
import com.broadon.wsapi.cps.PublishRequestType;
/**
 * JNI interface to the EScrypto library.
 */
public class TMDGenerator
{

    // Native code interface

    // This is to copy the constants defined in C header files.
    private static native void getTMDParams(int[] params);

    private static native byte[] generateTmd(long titleVersion, long titleId,
            int numContents, long[] cids, long[] indexes, long[] types, long[] sizes,
            byte[] hash, long caCrlVersion, long signerCrlVersion, long osVersion,
            long rights, byte[] customeData, int groupId, long bootIndex, String signerName,
            String caName);
    
    private static native byte[] generateTmd(byte[] tmdTemplate, long titleVersion, long titleId,
                                             long[] cids, long caCrlVersion, long signerCrlVersion, 
                                             String signerName, String caName);

    private static native byte[] encryptKey(byte[] commonAesKey, long titleId,
            byte[] titleAesKey);

    public static final int aesKeySize;
    public static final int hashSize;
    public static final int customDataSize;
    public static final int signatureSize;
    public static final int signatureOffset;
    public static final int documentStart;

    static final Object syncJNI; // used for serializing JNI calls
    static {
        syncJNI = new Object();
        int[] params;
        params = new int[6];
        getTMDParams(params);
        int i = 0;
        aesKeySize = params[i++];
        hashSize = params[i++];
        customDataSize = params[i++];
        signatureSize = params[i++];
        signatureOffset = params[i++];
        documentStart = params[i++];
    }

    private final byte[] titleAesKey;

    private final byte[] commonAesKey;

    private final long caCrlVersion;

    private final long signerCrlVersion;

    private final String signerName;

    private final String caName;

    public TMDGenerator(byte[] titleKey, byte[] commonKey, long caCrlVersion,
            long signerCrlVersion, String signerName, String caName) {
        titleAesKey = titleKey;
        commonAesKey = commonKey;
        if (titleKey.length != aesKeySize || (commonAesKey != null && commonAesKey.length != aesKeySize))
            throw new InvalidParameterException("Invalid title key length");
        this.caCrlVersion = caCrlVersion;
        this.signerCrlVersion = signerCrlVersion;
        this.signerName = signerName;
        this.caName = caName;
    }

    public byte[] generateMetaData(PublishRequestType req, List contentInfo,
            long titleID, long titleVersion) throws BroadOnException
    {
        ContentAttributeType[] contents = req.getContents();
        int numContents = contents != null ? contents.length : 0;
        if (contentInfo.size() != numContents)
            throw new BroadOnException("Internal error in processing content list");
        long[] cids = new long[numContents];
        long[] indices = new long[numContents];
        long[] types = new long[numContents];
        long[] sizes = new long[numContents];
        byte[] hash = new byte[numContents * hashSize];

        for (int i = 0; i < numContents; ++i) {
            AuxContentInfo info = (AuxContentInfo) contentInfo.get(i);
            cids[i] = info.contentID;
            indices[i] = contents[i].getContentIndex();
            // TODO: figure out what the content type should be.
            types[i] = 0; // contents[i].getContentObjectType();
            sizes[i] = info.unencryptedSize;
            System.arraycopy(info.hash, 0, hash, i * hashSize, hashSize);
        }

        // HACK HACK HACK
        // We have to prefix "Root-" to the caName because of an inconsistency
        // in the client cert naming convention, where the issuer of a cert is
        // *NOT* the subject name of the issuer cert, but instead is a
        // concatination of the subject name of the full chain.
        // To avoid unnecessary traversal of the chain, we just add "Root-" to
        // the caName, assuming that there will never be another layer between
        // the Root and the CA, or between the CA and the singer.
        byte[] tmd = generateTmd(titleVersion, titleID, numContents, cids, indices,
                types, sizes, hash, caCrlVersion, signerCrlVersion, 
                Long.parseLong(req.getOSVersion(), 16), 
                req.getAccessRights(), req.getCustomData(), req.getGroupID(), 
                req.getBootContentIndex(), signerName, "Root-" + caName);
        if (tmd == null)
            throw new BroadOnException("Error in generating TMD");
        return tmd;
    }
    
    public byte[] generateMetaData(byte[] tmdTemplate, long[] cids, long titleID, long titleVersion) 
        throws BroadOnException
    {
        // HACK HACK HACK
        // We have to prefix "Root-" to the caName because of an inconsistency
        // in the client cert naming convention, where the issuer of a cert is
        // *NOT* the subject name of the issuer cert, but instead is a
        // concatination of the subject name of the full chain.
        // To avoid unnecessary traversal of the chain, we just add "Root-" to
        // the caName, assuming that there will never be another layer between
        // the Root and the CA, or between the CA and the singer.
        byte[] tmd = generateTmd(tmdTemplate, titleVersion, titleID, cids, caCrlVersion, signerCrlVersion, 
                                 signerName, "Root-" + caName);
        if (tmd == null)
            throw new BroadOnException("Error in generating TMD");
        return tmd;
    }

    public byte[] encryptTitleKey(long titleID, byte[] aesKey) throws GeneralSecurityException
    {
        if (commonAesKey == null)
            throw new GeneralSecurityException("Missing common key (for encrypting title key).");
        return encryptKey(commonAesKey, titleID, aesKey);
    }

    public ContentInputStream getEncrypter(InputStream in, int index, Encrypter encrypter)
            throws GeneralSecurityException
    {
        if (encrypter == null)
            encrypter = new DefaultEncrypter();
        return new EncryptedContentInputStream(encrypter, in, titleAesKey, index, hashSize); 
    }
}
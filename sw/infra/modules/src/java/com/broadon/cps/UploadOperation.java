package com.broadon.cps;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import javax.activation.DataHandler;
import javax.sql.DataSource;

import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.logging.Log;

import com.broadon.cps.CertChainCache.Value;
import com.broadon.exception.BroadOnException;
import com.broadon.util.BackingStoreException;
import com.broadon.wsapi.cps.ActionType;
import com.broadon.wsapi.cps.ContentLinkType;
import com.broadon.wsapi.cps.UploadRequestType;
import com.broadon.wsapi.cps.UploadResponseType;

public class UploadOperation implements Constants
{
    protected static Log log = LogFactory.getLog(UploadOperation.class.getName());
    private DataSource dataSource;
    private CertChainCache certChainCache;
    
    public void init(DataSource dataSource) 
    {
        log.debug("Upload Operation Init");
        
        this.dataSource = dataSource;
        certChainCache = new CertChainCache(dataSource);
        
        log.debug("Upload Operation done");
        
    }
    
    public void destroy()
    {
        log.debug("Upload Operation shut down");
    }

    public UploadResponseType upload(UploadRequestType req, DataHandler[] attachedContent)
    {
        UploadResponseType resp = new UploadResponseType(0, null, null);
        boolean isUpdate = ActionType.Update.equals(req.getAction());
        
        Connection conn = null;   
        try {
            TitleMetaData tmd = new TitleMetaData(req.getTitleMetaData());
            
            conn = dataSource.getConnection();
            conn.commit();
            conn.setAutoCommit(false);
            DatabaseWriter db = new DatabaseWriter(conn, req.getPublishDate(), tmd.getTitleId());
            if (! isUpdate && db.isUpdate())
                return setErrorCode(resp, StatusCode.CPS_TITLE_EXISTS);
            isUpdate = db.isUpdate();
            
            if (tmd.getTMDVersion() < db.getTmdVersion())
                return setErrorCode(resp, StatusCode.CPS_INVALID_VERSION);
            
            Value certChainInfo = (CertChainCache.Value) certChainCache.get(tmd.getIssuer());
            
            // verify the validity of the TMD
            if (! verifyTMD(tmd.getTmd(), tmd.getHashOffset(), 
                            tmd.getSigOffset(), tmd.getSigSize(), certChainInfo.publicKey))
                return setErrorCode(resp, StatusCode.CPS_TMD_BAD_SIG);
            
            if (isUpdate || (db.isSubscriptionChannel() && (tmd.getTitleId() & 0xFFFFFFFFL) != 0)) {
                // In the case of subscription channel, all titles must use the
                // same key to encrypt content. However, they all have
                // individual eTicket Metadata record with the (same) key
                // encrypted differently using the common key but with different
                // IV.
                // As a result, for the purpose of comparing if the keys are the
                // same, we go back to use the channel title ID to generate the ETKM
                byte[] etkmBlob = db.isSubscriptionChannel() ?
                                  req.getEncryptedChannelKey() :
                                  req.getEncryptedTitleKey();
                if (! db.verifyTitleKey(etkmBlob)) {
                    return setErrorCode(resp, StatusCode.CPS_WRONG_KEY);
                }
            }
            
            db.updateTitle(req.getTitleType(), req.getTitleName(), req.getProductCode(), certChainInfo.chainID);
            
            uploadContents(tmd.getTitleId(), req.getContentLink(), tmd.getContents(), attachedContent, db);
            
            int attributes = PublishUtil.getAttributes(req.getAllowCommonTicket());
            
            db.createTmdRecord(req.getTitleMetaData(), tmd.getTMDVersion(), certChainInfo.chainID, 
                               req.getEncryptedTitleKey(), attributes);
            if (req.getCommonETicket() != null)
                db.createSpecialContent(req.getCommonETicket(), CID_FOR_COMMON_TICKET, 
                                        COMMON_ETICKET_TYPE);
            if (req.getMetaData() != null)
                db.createSpecialContent(req.getMetaData(), CID_FOR_MET, METADATA_TYPE);
            
            conn.commit();
        } catch (SQLException e) {
            return setErrorCode(resp, StatusCode.SC_DB_EXCEPTION, e);
        }  catch (BackingStoreException e) {
            return setErrorCode(resp, StatusCode.CPS_ACCESS_VIOLATION, e);
        } catch (BroadOnException e) {
            return setErrorCode(resp, StatusCode.CPS_INVALID_INPUT, e);
        } catch (GeneralSecurityException e) {
            return setErrorCode(resp, StatusCode.CPS_CRYPTO_ERROR, e);
        } catch (IOException e) {
            return setErrorCode(resp, StatusCode.CPS_ATTACHMENT, e);
        } finally {
            if (conn != null) {
                try {
                    conn.rollback();
                    conn.close();
                } catch (SQLException e) {}
            }
        }
       
        return resp;
    }

    private void uploadContents(long titleID, ContentLinkType[] contentLink, ContentMetaData[] cmd, 
                                DataHandler[] attachedContent, DatabaseWriter db) 
        throws NoSuchAlgorithmException, IOException, SQLException, BroadOnException
    { 
        if (contentLink == null || cmd == null)
            return;
        
        Map contentMap = PublishUtil.genContentAttributesMap(contentLink, cmd, titleID);
        
        if (attachedContent == null || attachedContent.length < contentMap.size())
            throw new BroadOnException("Missing content file attachment"); 
            
        for (int i = 0; i < attachedContent.length; i++) {
            TrivialContentInputStream in = new TrivialContentInputStream(attachedContent[i].getInputStream());
            
            db.createContentObject(in, contentMap);
        }
    }

    private boolean verifyTMD(byte[] titleMetaData, int hashOffset, int sigOffset, int sigSize,
                              PublicKey publicKey) 
        throws GeneralSecurityException
    {
        Signature sig = Signature.getInstance("SHA1withRSA");
        sig.initVerify(publicKey);
        sig.update(titleMetaData, hashOffset, titleMetaData.length - hashOffset);
        return sig.verify(titleMetaData, sigOffset, sigSize);
    }

    private UploadResponseType setErrorCode(UploadResponseType res, String errCode)
    {
        String errMsg = StatusCode.getMessage(errCode);
        log.error(errMsg);
        res.setErrorCode(Integer.parseInt(errCode));
        res.setErrorMessage(errMsg);
        return res;
    }
    
    private UploadResponseType setErrorCode(UploadResponseType res, String errCode, Exception e)
    {
        Throwable cause = e.getCause();
        if (cause != null && cause instanceof SQLException) {
            errCode = StatusCode.SC_SQL_EXCEPTION;
            e = (Exception) cause;
        }
        String errMsg = StatusCode.getMessage(errCode);
        log.error(errMsg, e);
        res.setErrorCode(Integer.parseInt(errCode));
        res.setErrorMessage(errMsg + '\n' + e.getLocalizedMessage());
        return res;
    }

}

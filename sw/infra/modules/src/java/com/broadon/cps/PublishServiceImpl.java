package com.broadon.cps;

import java.rmi.RemoteException;
import java.security.GeneralSecurityException;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.servlet.ServletContext;
import javax.sql.DataSource;
import javax.xml.rpc.ServiceException;
import javax.xml.rpc.server.ServiceLifecycle;
import javax.xml.rpc.server.ServletEndpointContext;

import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.logging.Log;

import com.broadon.servlet.ServletConstants;
import com.broadon.wsapi.cps.PublishRequestType;
import com.broadon.wsapi.cps.PublishResponseType;
import com.broadon.wsapi.cps.UploadRequestType;
import com.broadon.wsapi.cps.UploadResponseType;
import com.ncipher.nfast.NFException;

public class PublishServiceImpl implements ServiceLifecycle, ServletConstants
{          
    protected static Log log = LogFactory.getLog(PublishServiceImpl.class.getName());

    ServletContext servletContext;
    DataSource dataSource;
    PublishOperation publishOp;
    UploadOperation uploadOp;

    public void init(Object context) throws ServiceException
    {
        log.debug("Publish Service Init");

        System.load(System.getProperty("catalina.base") + "/webapps/cps/publisher.so");
        
        ServletEndpointContext soapContext = (ServletEndpointContext) context;
        servletContext = soapContext.getServletContext();

        // Data source already set up by OracleDSListener
        dataSource = (DataSource) servletContext.getAttribute(DATA_SOURCE_KEY);  
        Properties prop = (Properties) servletContext.getAttribute(PROPERTY_KEY);

        publishOp = new PublishOperation();
        uploadOp = new UploadOperation(); 
        
        try {
            publishOp.init(dataSource, prop);
            uploadOp.init(dataSource);
            Class.forName(PublishUtil.TMD_GEN);     // force loading and initialization of JNI code
            Class.forName("com.broadon.cps.StatusCode");
        } catch (GeneralSecurityException e) {
            wrapServiceException("Security module initialization error", e);
        } catch (NFException e) {
            wrapServiceException("Security module initialization error", e);
        } catch (ClassNotFoundException e) {
            wrapServiceException("Cannot find " + PublishUtil.TMD_GEN, e);
        } 
        log.debug("Publish Service Init Done");
    }

    private void wrapServiceException(String msg, Exception e) throws ServiceException
    {
        log.error(e.getMessage(), e);
        ServiceException se = new ServiceException(msg, e);
        se.setStackTrace(e.getStackTrace());
        throw se; 
    }

    public void destroy()
    {
        uploadOp.destroy();
        publishOp.destroy();
        log.debug("Publish Service shut down.");
    }

    
    protected PublishResponseType publish(PublishRequestType publishRequest, DataHandler[] contents) throws java.rmi.RemoteException
    {
        return publishOp.publish(publishRequest, contents);
    }

    public UploadResponseType upload(UploadRequestType uploadRequest, DataHandler[] attachedContent) throws RemoteException
    {
        return uploadOp.upload(uploadRequest, attachedContent);
    }
}

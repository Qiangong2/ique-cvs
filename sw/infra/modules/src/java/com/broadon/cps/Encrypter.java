package com.broadon.cps;

public interface Encrypter
{
    public int getBufferSize();
    
    public byte[] encryptInit(int index, byte[] titleAesKey);

    /**
     * Available size of outBuf must be >= max(getBufferSize(), len)
     */
    public int encryptUpdate(byte[] handle, byte[] inBuf, int inOffset, int len,
                             byte[] outBuf, int outOffset); 

    public int encryptFinal(byte[] handle, byte[] outBuf, int off, byte[] hash);

}

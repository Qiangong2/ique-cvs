package com.broadon.cps;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.sql.Types;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

import oracle.sql.BLOB;

import com.broadon.exception.BroadOnException;
import com.broadon.util.Pair;

public class DatabaseWriter implements Constants
{
    static final String TMD_TYPE = "TMD";
    
    private Connection conn;
    private long publishDate;
    private boolean isUpdate;
    private long titleID;
    private BigDecimal titleKeyID;      // content ID of title encryption key blob
    private long nextCid;                // next available content ID
    private TreeMap checksumMap;
    private boolean isSubscriptionChannel;      // true if the title ID belongs to a subscription channel
    private int tmdVersion;
    private long cid4tmd = CID_FOR_TMD; // default content ID for TMD

    public DatabaseWriter(Connection conn, long publishDate, long titleID) 
        throws SQLException, BroadOnException 
    {
       this.conn = conn;
       this.publishDate = publishDate;
       this.titleID = titleID;

       nextCid = 0;
       tmdVersion = 0;
       loadTitleInfo();
       isSubscriptionChannel = checkSubChannel();
    }

    static final String LOAD_CONTENT =
        "SELECT C.CONTENT_ID, B.CONTENT_OBJECT_TYPE, B.CONTENT_CHECKSUM, B.CONTENT_OBJECT_VERSION " +
        "FROM CONTENTS A, CONTENT_CACHE B, CONTENT_TITLE_OBJECTS C " +
        "WHERE C.TITLE_ID=? AND A.CONTENT_ID = C.CONTENT_ID AND " +
        "      A.CONTENT_CHECKSUM = B.CONTENT_CHECKSUM";
        
        
    private void loadTitleInfo() throws SQLException, BroadOnException
    {
        titleKeyID = null;
        checksumMap = new TreeMap();
        PreparedStatement ps = conn.prepareStatement(LOAD_CONTENT);
        try {
            ps.setLong(1, titleID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                BigDecimal contentID = rs.getBigDecimal("CONTENT_ID");
                String ctype = rs.getString("CONTENT_OBJECT_TYPE");
                if ("ETKM".equals(ctype))
                    titleKeyID = contentID;
                else {
                    long cid = extractContentID(contentID);
                    if (! TMD_TYPE.equals(ctype)){
                        if (cid >= nextCid)
                            nextCid = cid + 1;
                        String csum = rs.getString("CONTENT_CHECKSUM");
                        checksumMap.put(csum, contentID);
                    } else {
                        int version = rs.getInt("CONTENT_OBJECT_VERSION");
                        if (version >= tmdVersion)
                            tmdVersion = version + 1;
                        if (cid <= cid4tmd)
                            cid4tmd = cid - 1;
                    }
                }
                
            }
            rs.close();
        } finally {
            isUpdate = (titleKeyID != null);
            ps.close();
        }
    }
    
    
    static final String TITLES_IN_CHANNEL =
        "SELECT 1 FROM CONTENT_TITLES WHERE TITLE_ID > ? AND TITLE_ID < ?";
    
    static final String FIND_SUBSCRIPTION_TITLE =
        "SELECT 1 FROM CONTENT_ETICKET_METADATA WHERE CONTENT_ID=?";
    
    private boolean checkSubChannel() throws SQLException, BroadOnException
    {
        PreparedStatement ps;
        
        if ((titleID & 0xFFFFFFFFL) == 0) {
            if (!isUpdate) {
                // if we are creating a subscription ticket, we must make sure
                // that there is no regular ticket already created in this
                // channel. A subscription channel must have all titles
                // encrypted with the same key but regular titles are encrypted
                // with individual keys.
                
                ps = conn.prepareStatement(TITLES_IN_CHANNEL);
                try {
                    ps.setLong(1, titleID);
                    ps.setLong(2, titleID + 0x100000000L);
                    ResultSet rs = ps.executeQuery();
                    if (rs.next()) {
                        throw new BroadOnException("Not a subscription channel:  " +
                                    "non-subscription titles already published.");
                    }
                } finally {
                    ps.close();
                }
            }
            return true;
        } else {
            ps = conn.prepareStatement(FIND_SUBSCRIPTION_TITLE);
            try {
                BigDecimal cid = genContentID(titleID & 0xFFFFFFFF00000000L, CID_4_TITLE_KEY);
                ps.setBigDecimal(1, cid);
                ResultSet rs = ps.executeQuery();
                if (rs.next()) {
                    titleKeyID = cid;
                    return true;
                } else
                    return false;
            } finally {
                ps.close();
            }
        }
    }

 
    public boolean isSubscriptionChannel()
    {
        return isSubscriptionChannel;
    }


    
    static final String GET_TITLE_KEY = 
        "SELECT ETICKET_METADATA FROM CONTENT_ETICKET_METADATA WHERE CONTENT_ID=?";
    /**
     * In the case of updating a title, verify if the given key is the same as
     * recorded.
     * 
     * @param key
     *            Encryption key passed in from the request
     * @return true if the given key is the same as recorded in the database. In
     *         the case of a newly created title, always return
     *         <code>true</code> indicating that the given key could be used.
     * @throws SQLException 
     */
    public boolean verifyTitleKey(byte[] key) throws SQLException 
    {
        if (titleKeyID == null || key == null)
            return false;
        
        PreparedStatement ps = conn.prepareStatement(GET_TITLE_KEY);
        
        try {
            ps.setBigDecimal(1, titleKeyID);
            ResultSet rs = ps.executeQuery();
            if (! rs.next())
                return false;
            Blob blob = rs.getBlob(1);
            if (blob == null)
                return false;
            if (blob.length() != key.length)
                return false;
            byte[] oldKey = blob.getBytes(1, key.length);
            return Arrays.equals(oldKey, key);
        } finally {
            ps.close();
        }
    }

    
    // stream and the content into a temporary blob, and compute its checksum as we go
    private BLOB writeTempBlob(ContentInputStream contentBlob) throws SQLException 
    {
        BLOB tempBlob =  BLOB.createTemporary(conn, true, BLOB.DURATION_SESSION);
        OutputStream out = tempBlob.setBinaryStream(1);
        byte[] buffer = new byte[64 * 1024];
        int n;
        try {
            while ((n = contentBlob.read(buffer)) >= 0) {
                if (n == 0) // ContentInputSream.read() could return 0 before EOF
                    continue;
                out.write(buffer, 0, n);
            }
        } catch (IOException e) {
            SQLException se = new SQLException(e.getLocalizedMessage());
            se.setStackTrace(e.getStackTrace());
            throw se;
        } finally {
            try {
                out.close();
            } catch (IOException e) {}
        }
        
        return tempBlob;
    }
    
    
    static final String CACHE_EXIST =
        "SELECT 1 FROM CONTENT_CACHE WHERE CONTENT_CHECKSUM=?";
    
    /**
     * @param contentBlob
     * @return a pair of objects. Pair.first contains the blob written, or null
     *         if the temp. blob has been rolled back because of duplicates.
     *         Pair.second contains the ID of the contents already in the database.
     * @throws SQLException
     */
    private Pair createTempBlob(ContentInputStream contentBlob) throws SQLException
    {
        PreparedStatement ps = null;
        Savepoint savePt = conn.setSavepoint();
        BLOB blob = writeTempBlob(contentBlob);
        
        if (isUpdate) {
            // if this content is already in the DB, use its old content ID
            BigDecimal oldCid = (BigDecimal) checksumMap.get(contentBlob.getMd5HashString());
            if (oldCid != null) {
                blob.freeTemporary();
                conn.rollback(savePt);
                return new Pair(null, oldCid);
            }
        } else if (isSubscriptionChannel) {
            // this is the case where a new title is publised to a
            // subscription channel. Since we could publish the same title
            // multiple times, and they will all be encrypted with the same
            // subscription key, there could be duplicated checksum. In this
            // case, we ignore the content but still assign a new content
            // id.
            ps = conn.prepareStatement(CACHE_EXIST);
            try {
                ps.setString(1, contentBlob.getMd5HashString());
                ResultSet rs = ps.executeQuery();
                if (rs.next()) {
                    blob.freeTemporary();
                    conn.rollback(savePt);
                    return new Pair(null, null);
                }
                rs.close();
            } finally {
                ps.close();
            }
        }
        
        return new Pair(blob, null);
    }

    
    static final String ADD_CONTENT_CACHE = 
        "INSERT INTO CONTENT_CACHE (CONTENT_CHECKSUM, CONTENT_SIZE, CONTENT_OBJECT_TYPE, " +
        "       CONTENT_OBJECT_NAME, CONTENT_OBJECT_VERSION, CREATE_DATE, CONTENT_OBJECT) " +
        "VALUES (?, ?, ?, ?, ?, BCCUTIL.ConvertTimeStamp(?), ?)";
    
    private void writeContentCache(String checksum, String contentType, String contentName, int version, BLOB blob) throws SQLException
    {
        PreparedStatement ps;
        // insert the content_cache row
        ps = conn.prepareStatement(ADD_CONTENT_CACHE);
        try {
            int i = 1;
            ps.setString(i++, checksum);
            ps.setLong(i++, blob.length());
            ps.setString(i++, contentType);
            ps.setString(i++, contentName);
            if (version >= 0)
                ps.setInt(i++, version);
            else
                ps.setNull(i++, Types.SMALLINT);
            ps.setLong(i++, publishDate);
            ps.setBlob(i++, blob);
            ps.executeUpdate();
        } finally {
            ps.close();
        }
    }

    
    public long createContentObject(ContentInputStream contentBlob, String contentType,
                             String contentName, int version, int index, boolean isTMD)
        throws SQLException, GeneralSecurityException
    {
        Pair result = createTempBlob(contentBlob);
        if (result.first != null) {
            writeContentCache(contentBlob.getMd5HashString(), contentType, contentName,
                              version, (BLOB) result.first);
        } 
        
        BigDecimal cid = (result.second == null) ? 
                         (isTMD ? genContentID(titleID, cid4tmd) : genContentID(titleID, nextCid++)) :
                         (BigDecimal) result.second;      
        
        // insert the content record and/or CTO
        createContentRecord(cid, contentBlob.getMd5HashString(), index, result.second != null);
        
        return extractContentID(cid);
    }
    
    
    public void createContentObject(TrivialContentInputStream in, Map contentMap) 
        throws SQLException, BroadOnException
    {
        Pair result = createTempBlob(in);
        
        ContentAttributes attr = (ContentAttributes) contentMap.get(in.getMd5HashString());
        if (attr == null)
            throw new BroadOnException("Attachment not part of this title");
        
        if (result.first != null) {
            writeContentCache(in.getMd5HashString(), attr.getContentType(), attr.getName(), -1, (BLOB) result.first);
        }
        
        BigDecimal cid = (result.second == null) ? attr.getContentId() : (BigDecimal) result.second;      
        boolean useExistingCid = (result.second != null);
        
        // insert the content record and/or CTO
        createContentRecord(cid, in.getMd5HashString(), attr.getIndex(), useExistingCid);
    }
    
    static final String ADD_CONTENT =
        "INSERT INTO CONTENTS (PUBLISH_DATE, CONTENT_CHECKSUM, CONTENT_ID) " +
        "VALUES (BCCUTIL.ConvertTimeStamp(?), ?, ?)";
    
    static final String UPDATE_CONTENT =
        "UPDATE CONTENTS SET PUBLISH_DATE=BCCUTIL.ConvertTimeStamp(?), CONTENT_CHECKSUM=? " +
        "WHERE CONTENT_ID=?";
    
    static final String ADD_CTO =
        "INSERT INTO CONTENT_TITLE_OBJECTS (TITLE_ID, CONTENT_ID, CREATE_DATE, CONTENT_INDEX) " +
        "VALUES (?, ?, BCCUTIL.ConvertTimeStamp(?), ?)";
    
    private void createContentRecord(BigDecimal contentID, String checkSum, int index, 
                                     boolean CTO_only) 
        throws SQLException
    {
        PreparedStatement ps;
        // insert the content row
        if (! CTO_only) {
            ps = conn.prepareStatement(ADD_CONTENT);
            int i = 1;
            try {
                ps.setLong(i++, publishDate);
                ps.setString(i++, checkSum);
                ps.setBigDecimal(i++, contentID);
                ps.executeUpdate();
            } finally {
                ps.close();
            }
        }
            
        ps = conn.prepareStatement(ADD_CTO);
        int i = 1;
        try {
            ps.setLong(i++, titleID);
            ps.setBigDecimal(i++, contentID);
            ps.setLong(i++, publishDate);
            if (index < 0)
                ps.setNull(i++, Types.SMALLINT);
            else
                ps.setInt(i++, index);
            ps.executeUpdate();
        } finally {
            ps.close();
        }
    } // createContentRecord

    
    private BigDecimal genContentID(long tid, long contentID)
    {
        // BigInteger version of ((titleID << 32) | contentID)
        return new BigDecimal(BigInteger.valueOf(tid).shiftLeft(32).or(BigInteger.valueOf(contentID)));
    }
    
    private long extractContentID(BigDecimal cid)
    {
        // BigInteger version of (cid & 0xffffffff)
        return cid.toBigInteger().and(BigInteger.valueOf(0xFFFFFFFFL)).longValue();
    }

    private static String ETKMDummyCheckSum = "ETKM Place Holder";
    
    private static String ADD_TITLE_KEY =
        "INSERT INTO CONTENT_ETICKET_METADATA (CONTENT_ID, CHAIN_ID, ETICKET_METADATA, ATTRIBITS) " +
        "VALUES (?, ?, EMPTY_BLOB(), ?)";
        
    private static String ADD_TITLE_KEY_BLOB =
        "SELECT ETICKET_METADATA FROM CONTENT_ETICKET_METADATA WHERE CONTENT_ID = ?";
    
    private static String UPDATE_ATTRIBUTES =
        "UPDATE CONTENT_ETICKET_METADATA SET ATTRIBITS=? WHERE CONTENT_ID = ?";
    
    /**
     * @param tmdBundle The TMD concatenated with the cert chain.
     * @param version
     * @param chainID
     * @param metadata The encrypted title key.
     * @param attributes
     * @throws SQLException
     * @throws GeneralSecurityException
     * @throws IOException
     */
    public void createTmdRecord(byte[] tmdBundle, int version, int chainID, byte[] metadata, 
                                int attributes) 
        throws SQLException, GeneralSecurityException, IOException
    {
        TrivialContentInputStream in = new TrivialContentInputStream(tmdBundle);
        
        createContentObject(in, TMD_TYPE, genTMDName(), version, -1, true);

        BigDecimal cid = genContentID(titleID, CID_4_TITLE_KEY);
        createContentRecord(cid, ETKMDummyCheckSum, -1, isUpdate);
        
        PreparedStatement ps;
        if (isUpdate) {
            ps = conn.prepareStatement(UPDATE_ATTRIBUTES);
            try {
                ps.setInt(1, attributes);
                ps.setBigDecimal(2, cid);
                ps.executeUpdate();
            } finally {
                ps.close();
            }
            return;
        }
        
        ps = conn.prepareStatement(ADD_TITLE_KEY);
        
        try {
            ps.setBigDecimal(1, cid);
            ps.setInt(2, chainID);
            ps.setInt(3, attributes);
            ps.executeUpdate();
            ps.close();
            
            ps = conn.prepareStatement(ADD_TITLE_KEY_BLOB);
            ps.setBigDecimal(1, cid);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Blob blob = rs.getBlob(1);
                if (! rs.wasNull() && blob != null) {
                    OutputStream out = blob.setBinaryStream(1L);
                    out.write(metadata);
                    out.close();
                    rs.close();
                    return;
                }
            }
            throw new SQLException("Error writing title key");
        } catch (IOException e) {
            SQLException se = new SQLException(e.getLocalizedMessage());
            se.setStackTrace(e.getStackTrace());
            throw se;
        } finally {
            ps.close();
        }
    }

    public void createSpecialContent(byte[] content, long contentID, String tag) 
        throws GeneralSecurityException, SQLException
    {
        TrivialContentInputStream in = new TrivialContentInputStream(content);
        BLOB blob = writeTempBlob(in);
        String contentName = genContentID(titleID, contentID).toBigInteger().toString(16) + ' ' + tag;
        try {
            writeContentCache(in.getMd5HashString(), tag, contentName, -1, blob);
        } catch (SQLException e) {
            // ignore dulplicate if not updating
            if (! isUpdate &&  e.getErrorCode() != 1)
                throw e;
        }
        
        // write the content records
        PreparedStatement ps = conn.prepareStatement(isUpdate ? UPDATE_CONTENT : ADD_CONTENT);
        int i = 1;
        try {
            ps.setLong(i++, publishDate);
            ps.setString(i++, in.getMd5HashString());
            ps.setBigDecimal(i++, genContentID(titleID, contentID));
            ps.executeUpdate();
        } finally {
            ps.close();
        }
    }

    private String genTMDName()
    {
        return genContentID(titleID, cid4tmd).toBigInteger().toString(16) + ' ' + TMD_TYPE;
    }
    
    
    static final String ADD_TITLE = 
        "INSERT INTO CONTENT_TITLES (TITLE_TYPE, TITLE, CHAIN_ID, PRODUCT_CODE, TITLE_ID) " +
        "VALUES (?, ?, ?, ?, ?)";
    
    static final String UPDATE_TITLE =
        "UPDATE CONTENT_TITLES SET TITLE_TYPE=?, TITLE=?, CHAIN_ID=?, PRODUCT_CODE = ? " +
        "WHERE TITLE_ID=?";
    
     
    public void updateTitle(String titleType, String titleName, String productCode, int chainID)
        throws SQLException 
    {
        PreparedStatement ps = conn.prepareStatement(isUpdate ? UPDATE_TITLE : ADD_TITLE);
        try {
            int i = 1;
            ps.setString(i++, titleType);
            ps.setString(i++, titleName);
            ps.setInt(i++, chainID);
            ps.setString(i++, productCode);
            ps.setLong(i++, titleID);
            ps.executeUpdate();
        } finally {
            ps.close();
        }
    }


    public int getTmdVersion()
    {
        return tmdVersion;
    }


    public boolean isUpdate()
    {
        return isUpdate;
    }


}

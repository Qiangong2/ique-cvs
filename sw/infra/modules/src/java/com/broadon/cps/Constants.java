package com.broadon.cps;

public interface Constants
{
    // special content ID for the title key (CONTENT_ETICKET_MEADATA record) of a title.
    public static final long CID_4_TITLE_KEY = 0xFFFFFFFFL;
    
    // special content ID for the TMD of a title.
    public static final long CID_FOR_TMD = 0xfffeffffL;
    
    // special content ID for common eTicket.
    public static final long CID_FOR_COMMON_TICKET = 0xFFFFFFFDL;
    
    // special content type for common eTicket.
    public static final String COMMON_ETICKET_TYPE = "CETK";
    
    // special content type for metadata.
    public static final long CID_FOR_MET = 0xFFFFFFFEL;
    public static final String METADATA_TYPE = "MET";
    
}

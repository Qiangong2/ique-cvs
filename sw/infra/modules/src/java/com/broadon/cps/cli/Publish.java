package com.broadon.cps.cli;

import java.io.File;
import java.io.FileInputStream;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.rpc.ServiceException;

import com.broadon.wsapi.cps.ActionType;
import com.broadon.wsapi.cps.ContentAttributeType;
import com.broadon.wsapi.cps.PublishPortType;
import com.broadon.wsapi.cps.PublishRequestType;
import com.broadon.wsapi.cps.PublishResponseType;
import com.broadon.wsapi.cps.PublishServiceLocator;

public class Publish {

    static private String CPSURL = "/cps/services/PublishSOAP";
    
    static private String CHANNEL = "00020001";

    static private PublishPortType cpsPort;

    private static void initCPSPort(String destination) throws ServiceException,
            MalformedURLException, Exception {
        URL url = new URL("http://" + destination + CPSURL);
        System.out.println(url.toString());
        PublishServiceLocator cpsService = new PublishServiceLocator();
        cpsPort = cpsService.getPublishSOAP(url);
        if (cpsPort == null)
            throw new Exception("can't cps port");
    }
    
    private static PublishResponseType publish(PublishRequestType req) {
        PublishResponseType resp = null;       
        System.out.println("Publishing " + req.getTitleName() + " ...");
        try {
            long startTime = System.currentTimeMillis();
            resp = cpsPort.publish(req);
            long elapsedTime = System.currentTimeMillis() - startTime;

            System.out.println("Done publishing " + elapsedTime / 1000 + "s.");
            System.out.println("Error Code: " + resp.getErrorCode());
            System.out.println("Error Message: " + resp.getErrorMessage());
            System.out.println("Comments: " + resp.getComment());
            
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    private static void initContent(PublishRequestType req) throws Exception {
        String workDir = "c:/cygwin/home/lo/games";
        String objName = req.getContents(0).getContentObjectName();

        File objFile = new File(workDir, objName);
        if (!objFile.canRead())
            throw new Exception("can't read object file "
                    + objFile.getCanonicalPath());

        byte[] buffer = new byte[(int) objFile.length()];
        FileInputStream in = new FileInputStream(objFile);
        in.read(buffer, 0, (int) objFile.length());
        in.close();

        req.getContents(0).setContentBlob(buffer);

    }

 
    private static void initDKKingOfSwing(PublishRequestType req)
            throws Exception {
        String objName = "1880 - Donkey Kong - King of Swing.gba";
        req.setRequesterId("LOCAL_CPS");
        req.setAction(ActionType.fromString("Create"));
        req.setTitleID(Long.toHexString(101180));
        req.setTitleName("Donkey Kong - King of Swing");
        req.setTitleType("GAME");
        req.setAccessRights(0);
        req.setBootContentIndex(0);

        ContentAttributeType[] contents = new ContentAttributeType[1];
        ContentAttributeType contentAttr = new ContentAttributeType();
        contentAttr.setContentIndex(0);
        contentAttr.setContentObjectName(objName);
        contentAttr.setContentObjectType("GAME");
        contents[0] = contentAttr;
        req.setContents(contents);
    }

    private static void initYakumanAdvance(PublishRequestType req)
            throws Exception {
        String objName = "0156 - Dokodemo Taikyoku - Yakuman Advance.gba";
        req.setRequesterId("LOCAL_CPS");
        req.setAction(ActionType.fromString("Create"));
        req.setTitleID(Long.toHexString(100156));
        req.setTitleName("Dokodemo Taikyoku - Yakuman Advance");
        req.setTitleType("GAME");
        req.setAccessRights(0);
        req.setBootContentIndex(0);

        ContentAttributeType[] contents = new ContentAttributeType[1];
        ContentAttributeType contentAttr = new ContentAttributeType();
        contentAttr.setContentIndex(0);
        contentAttr.setContentObjectName(objName);
        contentAttr.setContentObjectType("GAME");
        contents[0] = contentAttr;
        req.setContents(contents);
    }

    private static void initAdvanceWars2(PublishRequestType req)
            throws Exception {
        String objName = "1060 - Advance Wars 2 - Black Hole Rising.gba";
        req.setRequesterId("LOCAL_CPS");
        req.setAction(ActionType.fromString("Create"));
        req.setTitleID(CHANNEL + "00081060");
        req.setTitleName("1060 - Advance Wars 2 - Black Hole Rising");
        req.setTitleType("GAME");
        req.setAccessRights(0);
        req.setBootContentIndex(0);

        ContentAttributeType[] contents = new ContentAttributeType[1];
        ContentAttributeType contentAttr = new ContentAttributeType();
        contentAttr.setContentIndex(0);
        contentAttr.setContentObjectName(objName);
        contentAttr.setContentObjectType("GAME");
        contents[0] = contentAttr;
        req.setContents(contents);
    }
    
    private static void initFZero(PublishRequestType req) throws Exception {
        String objName = "fzero.mrg";
        req.setRequesterId("LOCAL_CPS");
        req.setAction(ActionType.fromString("Create"));
        req.setEncryptionPassword("pass1");
        req.setTitleID(CHANNEL + "00080001");
        req.setTitleName("FZero-LCE");
        req.setTitleType("GAME");
        req.setAccessRights(0);
        req.setBootContentIndex(0);

        ContentAttributeType[] contents = new ContentAttributeType[1];
        ContentAttributeType contentAttr = new ContentAttributeType();
        contentAttr.setContentIndex(0);
        contentAttr.setContentObjectName(objName);
        contentAttr.setContentObjectType("GAME");
        contents[0] = contentAttr;
        req.setContents(contents);
    }

    
    private static void initMarioKart(PublishRequestType req)
            throws Exception {
        String objName = "mkart.mrg";
        req.setRequesterId("LOCAL_CPS");
        req.setAction(ActionType.fromString("Create"));
        req.setEncryptionPassword("pass1");
        req.setTitleID(CHANNEL + "00080002");
        req.setTitleName("NC Mario Kart");
        req.setTitleType("GAME");
        req.setAccessRights(0);
        req.setBootContentIndex(0);

        ContentAttributeType[] contents = new ContentAttributeType[1];
        ContentAttributeType contentAttr = new ContentAttributeType();
        contentAttr.setContentIndex(0);
        contentAttr.setContentObjectName(objName);
        contentAttr.setContentObjectType("GAME");
        contents[0] = contentAttr;
        req.setContents(contents);
    }
    
    private static void initMultiSIO(PublishRequestType req) throws Exception {
        String objName = "multi_sio.mrg";
        req.setRequesterId("LOCAL_CPS");
        req.setAction(ActionType.fromString("Create"));
        req.setEncryptionPassword("pass1");
        req.setTitleID(CHANNEL + "00080003");
        req.setTitleName("NC Multi SIO Test");
        req.setTitleType("GAME");
        req.setAccessRights(0);
        req.setBootContentIndex(0);

        ContentAttributeType[] contents = new ContentAttributeType[1];
        ContentAttributeType contentAttr = new ContentAttributeType();
        contentAttr.setContentIndex(0);
        contentAttr.setContentObjectName(objName);
        contentAttr.setContentObjectType("GAME");
        contents[0] = contentAttr;
        req.setContents(contents);
    }

    private static void initDummy(PublishRequestType req)
    {
        String objName = "Duumy content";
        req.setRequesterId("LOCAL_CPS");
        req.setAction(ActionType.fromString("Create"));
        req.setTitleID(Long.toHexString(123456));
        req.setTitleName(objName);
        req.setTitleType("GAME");
        req.setAccessRights(0);
        req.setBootContentIndex(0);

        ContentAttributeType[] contents = new ContentAttributeType[1];
        ContentAttributeType contentAttr = new ContentAttributeType();
        contentAttr.setContentIndex(0);
        contentAttr.setContentObjectName(objName);
        contentAttr.setContentObjectType("GAME");
        contentAttr.setContentBlob("This is a test game".getBytes());
        contents[0] = contentAttr;
        req.setContents(contents);
    }

    private static void initSubscription(PublishRequestType req)
    {
        String titleName = "Subscription Title";
        req.setRequesterId("LOCAL_CPS");
        req.setEncryptionPassword("pass1");
        req.setAction(ActionType.fromString("Create"));
        req.setTitleID(CHANNEL + "00000000");
        req.setTitleName(titleName);
        req.setTitleType("SUBSCRIPT");
        req.setAccessRights(0);
        req.setBootContentIndex(0);
        req.setContents(null);
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        System.out.println("CPS");
        try {
            initCPSPort(args[0]);

            PublishRequestType req = new PublishRequestType();
            PublishResponseType resp = null;

            if (false) {
                initDKKingOfSwing(req);
                initContent(req);
                resp = publish(req);
            }

            if (false) {
                initYakumanAdvance(req);
                initContent(req);
                resp = publish(req);
            }

            if (false) {
                initAdvanceWars2(req);
                initContent(req);
                resp = publish(req);
            }
            
            if (false) {
                initDummy(req);
                resp = publish(req);
            }
            
            if (false) {
                initSubscription(req);
                resp = publish(req);
            }
 
            if (false) {
                initMultiSIO(req);
                initContent(req);        
                resp = publish(req);
            }
            
            if (true) {
                initMarioKart(req);
                initContent(req);        
                resp = publish(req);
            }          
 
            if (true) {
                initFZero(req);
                initContent(req);
                resp = publish(req);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

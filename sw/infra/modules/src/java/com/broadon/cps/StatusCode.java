package com.broadon.cps;

public class StatusCode extends com.broadon.status.StatusCode
{
    public static final String CPS_WRONG_KEY = "1001";
    static final String CPS_WRONG_KEY_MSG =
        "Must use the same encryption key when title was first published.";
    
    public static final String CPS_SIGNATURE_ERROR = "1002";
    static final String CPS_SIGNATURE_ERROR_MSG =
        "Invalid key used for signing publication license.";
    
    public static final String CPS_ACCESS_VIOLATION = "1003";
    static final String CPS_ACCESS_VIOLATION_MSG =
        "Request signature key does not exist or insufficient permission";
    
    public static final String CPS_INVALID_INPUT = "1004";
    static final String CPS_INVALID_INPUT_MSG =
        "Invalid input.";
    
    public static final String CPS_CRYPTO_ERROR = "1005";
    static final String CPS_CRYPTO_ERROR_MSG =
        "Cryptographic operation error:  please check the encrypt key and other parameters.";
    
    public static final String CPS_HSM_ERROR = "1006";
    static final String CPS_HSM_ERROR_MSG =
        "Cannot connect to the HSM";
    
    public static final String CPS_TMD_BAD_SIG = "1007";
    static final String CPS_TMD_BAD_SIG_MSG = 
        "Invalid signature in title metadata";
    
    public static final String CPS_ATTACHMENT = "1008";
    static final String CPS_ATTACHMENT_MSG =
        "Attached files processing error";
    
    public static final String CPS_INTERNAL = "1009";
    static final String CPS_INTERNAL_MSG = "Internal data corruption";
    
    public static final String CPS_INVALID_VERSION = "1010";
    static final String CPS_INVALID_VERSION_MSG = "A same or higher version number already exists.";
    
    public static final String CPS_TITLE_EXISTS = "1011";
    static final String CPS_TITLE_EXISTS_MSG = "A title with the same ID already exists";
    
    static {
        addStatusCode(CPS_WRONG_KEY, CPS_WRONG_KEY_MSG);
        addStatusCode(CPS_SIGNATURE_ERROR, CPS_SIGNATURE_ERROR_MSG);
        addStatusCode(CPS_ACCESS_VIOLATION, CPS_ACCESS_VIOLATION_MSG);
        addStatusCode(CPS_INVALID_INPUT, CPS_INVALID_INPUT_MSG);
        addStatusCode(CPS_CRYPTO_ERROR, CPS_CRYPTO_ERROR_MSG);
        addStatusCode(CPS_HSM_ERROR, CPS_HSM_ERROR_MSG);
        addStatusCode(CPS_TMD_BAD_SIG, CPS_TMD_BAD_SIG_MSG);
        addStatusCode(CPS_ATTACHMENT, CPS_ATTACHMENT_MSG);
        addStatusCode(CPS_INTERNAL, CPS_INTERNAL_MSG);
        addStatusCode(CPS_INVALID_VERSION, CPS_INVALID_VERSION_MSG);
        addStatusCode(CPS_TITLE_EXISTS, CPS_TITLE_EXISTS_MSG);
    }
   
}

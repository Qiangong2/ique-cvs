/**
 * 
 */
package com.broadon.cps;

public class AuxContentInfo
{
    public final long contentID;
    public final byte[] hash;
    public final int unencryptedSize;
    
    public AuxContentInfo(long contentID, byte[] hash, int size) {
        this.contentID = contentID;
        this.hash = hash;
        unencryptedSize = size;
    }       
}
package com.broadon.cps;

import java.math.BigDecimal;

import com.broadon.exception.BroadOnException;
import com.broadon.wsapi.cps.ContentLinkType;

/**
 * Hold corresponding ContentLinkType (from upload request) and ContentMetaData
 * (from parsing TMD) together for easy access.
 * 
 */
public class ContentAttributes
{
    private final ContentLinkType contentLink;

    private final ContentMetaData contentMetaData;

    private final long titleID;

    public ContentAttributes(ContentLinkType clink, ContentMetaData cmd,
            long titleID) throws BroadOnException
    {
        if (cmd.getRawContentId() != Long.parseLong(clink.getContentId(), 16))
            throw new BroadOnException(
                    "Content attributes from the request does not match that in the TMD");

        contentLink = clink;
        contentMetaData = cmd;
        this.titleID = titleID;
    }

    public BigDecimal getContentId()
    {
        return new BigDecimal(contentMetaData.getFullContentId(titleID));
    }

    public long getRawContentId()
    {
        return contentMetaData.getRawContentId();
    }

    public short getIndex()
    {
        return contentMetaData.getIndex();
    }

    public String getChecksum()
    {
        return contentLink.getCheckSum();
    }

    public String getName()
    {
        return contentLink.getContentName();
    }

    public String getContentType()
    {
        return contentLink.getContentType();
    }
}

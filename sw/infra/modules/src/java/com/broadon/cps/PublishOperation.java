package com.broadon.cps;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.sql.DataSource;

import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.logging.Log;

import com.broadon.exception.BroadOnException;
import com.broadon.see.HardwareSecurityModule;
import com.broadon.see.SecurityModule;
import com.broadon.util.BackingStoreException;
import com.broadon.util.Pair;
import com.broadon.wsapi.cps.ActionType;
import com.broadon.wsapi.cps.ContentAttributeType;
import com.broadon.wsapi.cps.PublishRequestType;
import com.broadon.wsapi.cps.PublishResponseType;
import com.ncipher.nfast.NFException;

public class PublishOperation
{
    protected static Log log = LogFactory.getLog(PublishOperation.class.getName());
    
    private CertInfoCache certInfoCache;
    private SecurityModule sm;
    private DataSource dataSource;
    private String commonKey;
    
    private boolean useUnsafeKeyGen;


    public void init(DataSource dataSource, Properties prop) throws GeneralSecurityException, NFException 
    {
        log.debug("Publish Operation Init");
        
        this.dataSource = dataSource;
        certInfoCache = new CertInfoCache(dataSource);
        sm = SecurityModule.getInstance(prop);
        commonKey = prop.getProperty(PublishUtil.COMMON_KEY_NAME, 
                                     PublishUtil.DEFAULT_COMMON_KEY_NAME);

        // HACK ALERT
        // this is a hidden property to turn on unsafe title key generation.
        // Unsafe key generation should only be used in the devel environment
        // (PKI). This is needed because of a screw-up in the NDEV that has been
        // published and is too late to change without breaking compatibility!!!
        useUnsafeKeyGen = "true".equals(prop.getProperty("USE_UNSAFE_KEY_GENERATION")) ? true : false;
        
        String msg = "\n*** Using " + (useUnsafeKeyGen ? "unsafe" : "standard") + " title key generation algorithm ***\n";
        log.warn(msg);
        System.out.println(msg);
        
        log.debug("Publish Operation Init Done");
    }
    
    public void destroy() {
        log.debug("Publish Operation shut down");
    }
    
    
    protected PublishResponseType publish(PublishRequestType publishRequest, DataHandler[] contents) throws java.rmi.RemoteException
    {
        Connection conn = null;
        PublishResponseType res = new PublishResponseType(0, "OK", "", null);
        
        boolean isUpdate = ActionType.Update.equals(publishRequest.getAction());
        long titleID = Long.parseLong(publishRequest.getTitleID(), 16);
      
        PublishUtil.setupCustomData(publishRequest);
        
        Pair keyinfo = new Pair(publishRequest.getRequesterId(), publishRequest.getTitleType());
        
        try {
            conn = dataSource.getConnection();
            conn.commit();      // need this to workaround OCI driver bug
            conn.setAutoCommit(false);
            DatabaseWriter db = new DatabaseWriter(conn, System.currentTimeMillis(), titleID);
            if (isUpdate != db.isUpdate()) {
                if (! isUpdate)
                    return setErrorCode(res, StatusCode.CPS_TITLE_EXISTS);
                else
                    throw new BroadOnException("Updating non-existence title " + Long.toHexString(titleID));
            }
            
            CertInfoCache.Value certInfo = (CertInfoCache.Value) certInfoCache.get(keyinfo);
             
            // all subscription channel share the same key, so we seed the key
            // generation with just the channel title ID.
            byte[] titleKey = PublishUtil.generateTitleKey(publishRequest.getEncryptionPassword(), 
                    titleID, db.isSubscriptionChannel(), useUnsafeKeyGen);
            
            TMDGenerator tmdGen = new TMDGenerator(titleKey, PublishUtil.getCommonKey(commonKey), 
                                                   certInfo.caCrlVersion, certInfo.signerCrlVersion,
                                                   certInfo.subjectName, certInfo.caName);  

            byte[] encryptedTitleKey = encryptTitleKey(tmdGen, titleID, titleKey);
            
            if (isUpdate || (db.isSubscriptionChannel() && (titleID & 0xFFFFFFFFL) != 0)) {
                // In the case of subscription channel, all titles must use the
                // same key to encrypt content. However, they all have
                // individual eTicket Metadata record with the (same) key
                // encrypted differently using the common key but with different
                // IV.
                // As a result, for the purpose of comparing if the keys are the
                // same, we go back to use the channel title ID to generate the ETKM
                byte[] etkmBlob = db.isSubscriptionChannel() ?
                        encryptTitleKey(tmdGen, PublishUtil.getChannelTitleID(titleID), titleKey) :
                        encryptedTitleKey;
                if (! db.verifyTitleKey(etkmBlob)) {
                    return setErrorCode(res, StatusCode.CPS_WRONG_KEY);
                }
            }
            
            db.updateTitle(publishRequest.getTitleType(), publishRequest.getTitleName(), 
                           publishRequest.getProductCode(), certInfo.chainID);
       
            ArrayList cids = publishContents(publishRequest, contents, db, tmdGen);
            
            byte[] tmd = tmdGen.generateMetaData(publishRequest, cids, titleID, db.getTmdVersion());
            tmd = sm.signTMD(certInfo.signerName, tmd, tmdGen.signatureOffset, tmdGen.documentStart, tmdGen.signatureSize);
            
            int attributes = PublishUtil.getAttributes(publishRequest.getAllowCommonTicket());
            
            db.createTmdRecord(genBundledTmd(tmd, certInfo.certChain), db.getTmdVersion(), 
                               certInfo.chainID, encryptedTitleKey, attributes);
            res.setTitleMetaData(tmd);
            conn.commit();
        } catch (BackingStoreException e) {
            return setErrorCode(res, StatusCode.CPS_ACCESS_VIOLATION, e);
        } catch (SQLException e) {
            return setErrorCode(res, StatusCode.SC_DB_EXCEPTION, e);
        } catch (BroadOnException e) {
            return setErrorCode(res, StatusCode.CPS_INVALID_INPUT, e);
        } catch (InvalidKeyException e) {
            return setErrorCode(res, StatusCode.CPS_SIGNATURE_ERROR);
        } catch (GeneralSecurityException e) {
            return setErrorCode(res, StatusCode.CPS_CRYPTO_ERROR, e);
        } catch (NFException e) {
            return setErrorCode(res, StatusCode.CPS_HSM_ERROR, e);
        } catch (IOException e) {
            return setErrorCode(res, StatusCode.CPS_INTERNAL, e);
        } finally {
            if (conn != null) {
                try {
                    conn.rollback();
                    conn.close();
                } catch (SQLException e) {}
            }
        }
        
        return res;
    }

    
    private byte[] encryptTitleKey(TMDGenerator tmdGen, long titleID, byte[] plainTextTitleKey) 
        throws GeneralSecurityException, NFException
    {
        if (sm instanceof HardwareSecurityModule) {
            HardwareSecurityModule hsm = (HardwareSecurityModule) sm;
            return hsm.encryptTitleKey(commonKey, titleID, plainTextTitleKey);
        } else
            return tmdGen.encryptTitleKey(titleID, plainTextTitleKey);
    }

    
    private byte[] genBundledTmd(byte[] tmd, byte[][] certChain) throws IOException
    {
        ByteArrayOutputStream buf = new ByteArrayOutputStream();   
        buf.write(tmd);
        for (int i = 0; i < certChain.length; i++) {
            buf.write(certChain[i]);
        }
        buf.close();   
        return buf.toByteArray();
    }

    private ArrayList publishContents(PublishRequestType publishRequest, 
                                      DataHandler[] attachments, DatabaseWriter db, 
                                      TMDGenerator tmdGen) 
        throws GeneralSecurityException, SQLException, BroadOnException
    {
        ContentAttributeType[] contents = publishRequest.getContents();
        ArrayList cids = new ArrayList();
        if (contents != null) {
            if (attachments == null || attachments.length != contents.length)
                throw new BroadOnException("Number of content descriptor and attachment mismatched");
            try {
                for (int i = 0; i < contents.length; i++) {
                    InputStream in = attachments[i].getInputStream();
                    ContentInputStream contentBlob = 
                        tmdGen.getEncrypter(in, contents[i].getContentIndex(), null);
                    
                    long cid = db.createContentObject(contentBlob, contents[i].getContentObjectType(),
                                                      contents[i].getContentObjectName(), -1, 
                                                      contents[i].getContentIndex(), false);
                    in.close();
                    cids.add(new AuxContentInfo(cid, contentBlob.getSha1Hash(), 
                                                contentBlob.getOriginalSize()));
                }
            } catch (IOException e) {
                BroadOnException be = new BroadOnException("Error reading attachment:" + e.getLocalizedMessage());
                be.setStackTrace(e.getStackTrace());
                throw be;               
            }
        } 
        return cids;
    }
    

    private PublishResponseType setErrorCode(PublishResponseType res, String errCode, Exception e)
    {
        Throwable cause = e.getCause();
        if (cause != null && cause instanceof SQLException) {
            errCode = StatusCode.SC_SQL_EXCEPTION;
            e = (Exception) cause;
        }
        String errMsg = StatusCode.getMessage(errCode);
        log.error(errMsg, e);
        res.setErrorCode(Integer.parseInt(errCode));
        res.setErrorMessage(errMsg + '\n' + e.getLocalizedMessage());
        return res;
    }
    
    
    private PublishResponseType setErrorCode(PublishResponseType res, String errCode) 
    {
        String errMsg = StatusCode.getMessage(errCode);
        log.error(errMsg);
        res.setErrorCode(Integer.parseInt(errCode));
        res.setErrorMessage(errMsg);
        return res;
    }
}

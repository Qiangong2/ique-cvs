package com.broadon.cps;

import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.broadon.db.DataSourceScheduler;
import com.broadon.db.OracleDataSource.OracleDataSourceProxy;
import com.broadon.util.BackingStore;
import com.broadon.util.BackingStoreException;
import com.broadon.util.Base64;
import com.broadon.util.CacheLFU;

public class CertChainCache extends CacheLFU implements BackingStore
{

    private DataSource ds;
    public KeyFactory keyFactory;
    private BigInteger exponent = new BigInteger("10001", 16);

    public CertChainCache(DataSource ds) {
        super(100, -1);
        this.ds = ds;
        try {
            keyFactory = KeyFactory.getInstance("RSA");
        } catch (NoSuchAlgorithmException e) {}
    }

    static final String GET_CERT =
        "SELECT PUBLIC_KEY, CHAIN_ID, SIGNER_CERT_ID, CA_CERT_ID " +
        "FROM CERTIFICATES A, CERTIFICATE_CHAINS B " +
        "WHERE A.CERT_ID = B.SIGNER_CERT_ID AND B.DESCRIPTION=?";
    
    static final String GET_CHAIN =
        "SELECT CERTIFICATE FROM CERTIFICATES WHERE CERT_ID IN (?, ?)";
    
    public Object retrieve(Object arg0) throws BackingStoreException
    {
        String subject = (String) arg0;
        
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            conn = getConnection(DataSourceScheduler.DB_PRIORITY_FAST);
            ps = conn.prepareStatement(GET_CERT);
            ps.setString(1, subject);
            rs = ps.executeQuery();
            if (rs.next()) {
                int i = 0;
                PublicKey pubKey = toPublicKey(rs.getString(++i));
                int chainID = rs.getInt(++i);
                String signer = rs.getString(++i);
                String ca = rs.getString(++i);
                rs.close();
                ps.close();
                
                i = 0;
                ps = conn.prepareStatement(GET_CHAIN);
                ps.setString(++i, signer);
                ps.setString(++i, ca);
                rs = ps.executeQuery();
                
                byte[][] certChain = new byte[2][];
                i = 0;
                Base64 base64 = new Base64();
                while (rs.next()) {
                    certChain[i++] = base64.decode(rs.getString(1));
                }
                return new Value(chainID, certChain, pubKey);
            } else
                throw new BackingStoreException("Cannot locate certificate " + subject);
        } catch (SQLException e) {
            throw BackingStoreException.rewrap(e);
        } catch (Exception e) {
            BackingStoreException be = new BackingStoreException(e.getMessage());
            be.setStackTrace(e.getStackTrace());
            throw be;
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (ps != null)
                    ps.close();
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {}
        }
    }

    
    private Connection getConnection(int priority) throws SQLException
    {
        return ds instanceof OracleDataSourceProxy ? 
               ((OracleDataSourceProxy) ds).getConnection(priority) :
               ds.getConnection();
    }


    private PublicKey toPublicKey(String encodedKey) throws InvalidKeySpecException
    {
        BigInteger modulus = new BigInteger(encodedKey, 32);
        return keyFactory.generatePublic(new RSAPublicKeySpec(modulus, exponent));
    }


    public static class Value {
        public final int chainID;
        public final byte[][] certChain;
        public final PublicKey publicKey;
        
        public Value(int chainID, byte[][] certChain, PublicKey publicKey) 
            throws BackingStoreException 
        {
            this.chainID = chainID;
            this.certChain = certChain;
            this.publicKey = publicKey;
            if (certChain[0] == null || certChain[1] == null)
                throw new BackingStoreException("Cannot locate cert chain for chain ID " + chainID);
        }
    }
}

#include <string.h>
#include <netinet/in.h>

#include "x86/esl.h"
#include "es_int.h"

#include "TMDGenerator.h"

#define FUNC(f) Java_com_broadon_cps_TMDGenerator_ ## f

static jint params[] = {
    ES_AES_KEY_SIZE,
    ES_HASH_SIZE,
    ES_TMD_RESERVED_SIZE,
    ES_RSA2048_PUB_KEY_SIZE,
    ES_SIGNATURE_OFFSET,
    ES_SIGNATURE_OFFSET + ES_RSA2048_PUB_KEY_SIZE + ES_SIG_PAD_SIZE,
};

JNIEXPORT void JNICALL
FUNC(getTMDParams)(JNIEnv* env, jclass, jintArray _params)
{
    env->SetIntArrayRegion(_params, 0, sizeof(params)/sizeof(jint), params);
} // getTMDParams


template<class T> struct ByteArray
{
    T value;
    bool isValid;
    JNIEnv* env;
    jbyteArray array;

    ByteArray(JNIEnv* e, jbyteArray a, int len) : env(e), array(a) {
	value = a ? reinterpret_cast<T>(env->GetByteArrayElements(a, NULL)) : NULL;
	isValid = (value != NULL) && (env->GetArrayLength(a) == len);
    }

    ~ByteArray() {
	if (array)
	    env->ReleaseByteArrayElements(array, reinterpret_cast<jbyte*>(value),
					  JNI_ABORT);
    }
};


template<class T> struct LongArray
{
    T value;
    bool isValid;
    JNIEnv* env;
    jlongArray array;

    LongArray(JNIEnv* e, jlongArray a, int len) : env(e), array(a) {
	value = a ? reinterpret_cast<T>(env->GetLongArrayElements(a, NULL)) : NULL;
	isValid = (value != NULL) && (env->GetArrayLength(a) == len);
    }

    ~LongArray() {
	if (array)
	    env->ReleaseLongArrayElements(array, reinterpret_cast<jlong*>(value),
					  JNI_ABORT);
    }
};


template<class T> struct String
{
    T value;
    bool isValid;
    JNIEnv* env;
    jstring str;

    String(JNIEnv* e, jstring s) : env(e), str(s) {
	value = s ? reinterpret_cast<T>(env->GetStringUTFChars(s, NULL)) : NULL;
	isValid = (value != NULL);
    }

    String() {
	if (str)
	    env->ReleaseStringUTFChars(str, reinterpret_cast<jbyte*>(value));
    }
};


#define generateTmd Java_com_broadon_cps_TMDGenerator_generateTmd__JJI_3J_3J_3J_3J_3BJJJJ_3BIJLjava_lang_String_2Ljava_lang_String_2

JNIEXPORT jbyteArray JNICALL
generateTmd(JNIEnv* env, jclass, jlong _titleVersion, jlong _titleId,
	    jint _numContents, jlongArray _cids, jlongArray _indexes,
	    jlongArray _types, jlongArray _sizes, jbyteArray _hash,
	    jlong _caCrlVersion, jlong _signerCrlVersion, jlong _osVersion,
	    jlong _rights, jbyteArray _customerData, jint _groupID,
	    jlong _bootIndex, jstring _signerName, jstring _caName)
{
    LongArray<u64*> cids(env, _cids, _numContents);
    LongArray<u64*> indexes(env, _indexes, _numContents);
    LongArray<u64*> types(env, _types, _numContents);
    LongArray<u64*> sizes(env, _sizes, _numContents);
    ByteArray<u8*> hash(env, _hash, _numContents * ES_HASH_SIZE);
    ByteArray<u8*> customerData(env, _customerData, ES_TMD_RESERVED_SIZE);
    String<const char*> signerName(env, _signerName);
    String<const char*> caName(env, _caName);

    if (! (cids.isValid && indexes.isValid && types.isValid && sizes.isValid &&
	   hash.isValid && customerData.isValid && signerName.isValid &&
	   caName.isValid))
	return NULL;

    ESTmdView view;
    memset(&view, 0, sizeof(view));

    view.head.version = ES_TMD_VERSION;
    view.head.titleVersion = _titleVersion;
    view.head.titleId = _titleId;
    view.head.groupId = _groupID;
    view.head.numContents = _numContents;
    memcpy(view.head.reserved, customerData.value, ES_TMD_RESERVED_SIZE);

    for (int i = 0; i < _numContents; ++i) {
	ESCmdView& cmd = view.contents[i];
	cmd.cid = cids.value[i];
	cmd.index = indexes.value[i];
	cmd.type = types.value[i];
	cmd.size = sizes.value[i];
    }

    u8 tmd[ES_GetTmdSize(_numContents)];

    if (ES_GenerateUnsignedTmd(&view, _caCrlVersion, _signerCrlVersion,
			       _osVersion, _rights, _bootIndex, hash.value,
			       signerName.value, caName.value, tmd) !=
	ES_ERR_OK)
	return NULL;

    jbyteArray result = env->NewByteArray(sizeof(tmd));
    if (result) {
	env->SetByteArrayRegion(result, 0, sizeof(tmd),
				reinterpret_cast<jbyte*>(tmd));
    }

    return result;
} // generateTmd


// swap the TMD to host byte order
static void
ntohTMD(ESTitleMeta& tmd)
{
    ESTitleMetaHeader& head = tmd.head;
    head.sysVersion = ntohll(head.sysVersion);
    head.type = ntohl(head.type);
    head.titleId = ntohll(head.titleId);
    head.groupId = ntohs(head.groupId);
    head.accessRights = ntohl(head.accessRights);
    head.titleVersion = ntohs(head.titleVersion);
    head.numContents = ntohs(head.numContents);
    head.bootIndex = ntohs(head.bootIndex);

    for (int i = 0; i < head.numContents; ++i) {
        ESContentMeta& cm = tmd.contents[i];
        cm.cid = ntohl(cm.cid);
        cm.index = ntohs(cm.index);
        cm.type = ntohs(cm.type);
        cm.size = ntohll(cm.size);
    }
} 



#define generateTmdFromTemplate Java_com_broadon_cps_TMDGenerator_generateTmd___3BJJ_3JJJLjava_lang_String_2Ljava_lang_String_2

JNIEXPORT jbyteArray JNICALL
generateTmdFromTemplate(JNIEnv* env, jclass, jbyteArray _tmdTemplate,
			jlong _titleVersion, jlong _titleId, jlongArray _cids,
			jlong _caCrlVersion, jlong _signerCrlVersion,
			jstring _signerName, jstring _caName)
{
    String<const char*> signerName(env, _signerName);
    String<const char*> caName(env, _caName);

    if (! (signerName.isValid && caName.isValid))
	return NULL;

    ESTitleMeta tmd;
    int tmdSize;
    {
	int size = env->GetArrayLength(_tmdTemplate);
	env->GetByteArrayRegion(_tmdTemplate, 0, size,
				reinterpret_cast<jbyte*>(&tmd));
	ntohTMD(tmd);
	tmdSize = ES_GetTmdSize(tmd.head.numContents);
	if (tmdSize > size)
	    return NULL;
    }

    LongArray<u64*> cids(env, _cids, tmd.head.numContents);
    if (! cids.isValid)
	return NULL;


    ESTmdView view;
    memset(&view, 0, sizeof(view));

    view.head.version = tmd.head.version;
    view.head.sysVersion = tmd.head.sysVersion;
    view.head.titleId = _titleId;
    view.head.type = tmd.head.type;
    view.head.groupId = tmd.head.groupId;
    memcpy(view.head.reserved, tmd.head.reserved, sizeof(ESTmdReserved));
    view.head.titleVersion = _titleVersion;
    view.head.numContents = tmd.head.numContents;

    u8 hash[tmd.head.numContents * sizeof(IOSCSha1Hash)];

    for (int i = 0; i < tmd.head.numContents; ++i) {
	view.contents[i].cid = cids.value[i];
	view.contents[i].index = tmd.contents[i].index;	
	view.contents[i].type = tmd.contents[i].type;	
	view.contents[i].size = tmd.contents[i].size;
	memcpy(hash + i * sizeof(IOSCSha1Hash), tmd.contents[i].hash,
	       sizeof(IOSCSha1Hash));
    }

    u8 generatedTMD[tmdSize];

    if (ES_GenerateUnsignedTmd(&view, _caCrlVersion, _signerCrlVersion,
			       tmd.head.sysVersion, tmd.head.accessRights,
			       tmd.head.bootIndex, hash, signerName.value,
			       caName.value, generatedTMD) !=
	ES_ERR_OK)
	return NULL;

    jbyteArray result = env->NewByteArray(sizeof(generatedTMD));
    if (result) {
	env->SetByteArrayRegion(result, 0, sizeof(generatedTMD),
				reinterpret_cast<jbyte*>(generatedTMD));
    }

    return result;
}


JNIEXPORT jbyteArray JNICALL
FUNC(encryptKey)(JNIEnv* env, jclass, jbyteArray _commonKey, jlong _titleId,
		 jbyteArray _titleKey)
{
    ByteArray<u8*> commonKey(env, _commonKey, ES_AES_KEY_SIZE);
    ByteArray<u8*> titleKey(env, _titleKey, ES_AES_KEY_SIZE);

    if (! commonKey.isValid || ! titleKey.isValid)
	return NULL;

    u8 output[ES_AES_KEY_SIZE];
    if (ES_CpEncryptTitleKey(commonKey.value, _titleId, titleKey.value, output) !=
	ES_ERR_OK)
	return NULL;

    jbyteArray result = env->NewByteArray(ES_AES_KEY_SIZE);
    if (result) {
	env->SetByteArrayRegion(result, 0, ES_AES_KEY_SIZE,
				reinterpret_cast<jbyte*>(output));
    }
    return result;
} // encryptKey

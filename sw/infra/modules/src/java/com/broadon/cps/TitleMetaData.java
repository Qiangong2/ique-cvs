package com.broadon.cps;

import com.broadon.exception.BroadOnException;

public class TitleMetaData
{
    String issuer;
    long titleId;
    long osTitleId;
    int titleType;
    int groupId;
    int accessRights;
    int TMDVersion;
    int bootIndex;
    ContentMetaData[] contents;
    int hashOffset;
    int sigOffset;
    int sigSize;
    
    byte[] tmd;         // TMD in binary format, host byte order.
    
    private native byte[] parseTMD(byte[] tmd);
    static private native void initParser();
    
    static {
        initParser();    
    }
    
    public TitleMetaData(byte[] tmd) throws BroadOnException
    {
        this.tmd = parseTMD(tmd);
        if (this.tmd == null)
            throw new BroadOnException("Error parsing TMD");          
    }
    
    public int getAccessRights()
    {
        return accessRights;
    }
    public int getBootIndex()
    {
        return bootIndex;
    }
    public ContentMetaData[] getContents()
    {
        return contents;
    }
    public String getIssuer()
    {
        return issuer;
    }
    public long getTitleId()
    {
        return titleId;
    }
    public int getTitleType()
    {
        return titleType;
    }
    public int getTMDVersion()
    {
        return TMDVersion;
    }
    public int getHashOffset()
    {
        return hashOffset;
    }

    public int getSigOffset()
    {
        return sigOffset;
    }

    public int getSigSize()
    {
        return sigSize;
    }
    
    public byte[] getTmd()
    {
        return tmd;
    }
    public int getGroupId()
    {
        return groupId;
    }
    public long getOsTitleId()
    {
        return osTitleId;
    }
}

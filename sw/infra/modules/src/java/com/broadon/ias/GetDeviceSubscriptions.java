package com.broadon.ias;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ias.*;

public class GetDeviceSubscriptions extends IdentityAuthenticationFunction {
    
    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(GetDeviceSubscriptions.class.getName());

    /**
     * Returns a list of subscriptions for a given device ID
     */
    public static GetDeviceSubscriptionsResponseType getDeviceSubscriptions(
            IdentityAuthenticationServiceImpl service,
            GetDeviceSubscriptionsRequestType getDeviceSubscriptionsRequest) 
        throws java.rmi.RemoteException 
    {
        long deviceId = getDeviceSubscriptionsRequest.getDeviceId();
        String subscriptionType = getDeviceSubscriptionsRequest.getSubscriptionType();
        
        GetDeviceSubscriptionsResponseType response = new GetDeviceSubscriptionsResponseType();
        initResponse(getDeviceSubscriptionsRequest, response);
        response.setDeviceId(deviceId);
       
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
        
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            
            // query for the subscriptions for this deviceId
            List subscriptionsList = DeviceDML.getDeviceSubscriptions(conn, deviceId, subscriptionType);
                
            if (subscriptionsList != null && subscriptionsList.size() > 0) {
                DeviceSubscriptionType[] subs = new DeviceSubscriptionType[subscriptionsList.size()];
                subs = (DeviceSubscriptionType[]) subscriptionsList.toArray(subs);
                response.setDeviceSubscriptions(subs);
                   
            } else
                response.setDeviceSubscriptions(null);
            
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("GetDeviceSubscriptions error: ", e);
            
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.IAS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.IAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);       
            log.error("GetDeviceSubscriptions error: ", e);
            
        } finally {
            try {
                if (conn != null)
                    conn.close();
            }
            catch (SQLException e) {
                log.warn("GetDeviceSubscriptions error while closing connection: ", e);
            }
        }                
            
        return response;
    }    
}

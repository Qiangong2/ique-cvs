package com.broadon.ias;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ias.*;

public class GetDeviceInfo extends IdentityAuthenticationFunction {
    
    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(GetDeviceInfo.class.getName());

    /**
     * Returns device info (public key, serial number) and a list of subscriptions for a given device ID
     */
    public static GetDeviceInfoResponseType getDeviceInfo(
            IdentityAuthenticationServiceImpl service,
            GetDeviceInfoRequestType getDeviceInfoRequest) 
        throws java.rmi.RemoteException 
    {
        long deviceId = getDeviceInfoRequest.getDeviceId();
                
        GetDeviceInfoResponseType response = new GetDeviceInfoResponseType();
        initResponse(getDeviceInfoRequest, response);
        response.setDeviceId(deviceId);
 
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
        
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            
            DeviceInfoType dInfo = DeviceDML.getDeviceInfo(conn, deviceId);
            response.setDeviceInfo(dInfo);
                
            if (dInfo != null)
            {
                // query for the subscriptions for this deviceId
                List subscriptionsList = DeviceDML.getDeviceSubscriptions(conn, deviceId, null);
                    
                if (subscriptionsList != null && subscriptionsList.size() > 0) {
                    DeviceSubscriptionType[] subs = new DeviceSubscriptionType[subscriptionsList.size()];
                    subs = (DeviceSubscriptionType[]) subscriptionsList.toArray(subs);
                    response.setDeviceSubscriptions(subs);                                          
                } else
                    response.setDeviceSubscriptions(null);
                    
            } else {
                setErrorCode(StatusCode.IAS_BAD_DEVICE_ID, null, response);
                response.setDeviceSubscriptions(null);
            }
            
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("GetDeviceInfo error: ", e);
            
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.IAS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.IAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);       
            log.error("GetDeviceInfo error: ", e);
            
        } finally {
            try {
                if (conn != null)
                    conn.close();
            }
            catch (SQLException e) {
                log.warn("GetDeviceInfo error while closing connection: ", e);
            }
        }                
            
        return response;
    }    
}

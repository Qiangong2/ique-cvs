package com.broadon.ias;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ias.*;

public class RegisterAccount extends IdentityAuthenticationFunction {
    
    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(RegisterAccount.class.getName());

    /**
     * Creates a new account in the database
     */
    public static RegisterAccountResponseType registerAccount(
            IdentityAuthenticationServiceImpl service,
            RegisterAccountRequestType registerAccountRequest) 
        throws java.rmi.RemoteException 
    {
        String accountPswd = registerAccountRequest.getPassword();
        
        AccountInfoType aInfo = registerAccountRequest.getAccountInfo();
        String loginName = aInfo.getLoginName();
        String nickName = aInfo.getNickName();
        String fullName = aInfo.getFullName();
        String emailAddress = aInfo.getEmailAddress();
        Integer parentId = aInfo.getParentAccountId();
        Long birth_date = aInfo.getBirthDate();
                
        RegisterAccountResponseType response = new RegisterAccountResponseType();
        initResponse(registerAccountRequest, response);
               
        if (accountPswd == null || accountPswd.equals("")) {
            response.setAccountId(null);
            setErrorCode(StatusCode.IAS_BAD_REQUEST, null, response);
            log.debug("RegisterAccount bad request error: " +
                    "accountPswd=" + (accountPswd==null ? "null" : ""));
            
            return response;
        }
        
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
        
        try {
            boolean cont = true;
            
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
        	
            // check if the loginName has already been registered
            if (loginName != null && !loginName.equals("") &&
                    AccountDML.validateLoginName(conn, loginName)) {
                response.setAccountId(null);
        	setErrorCode(StatusCode.IAS_ACCOUNT_LOGIN_NAME_ALREADY_EXISTS_ERROR, null, response);
        	cont = false;
            }
        	
            if (cont) {
        	if (parentId != null && !AccountDML.validateAccountId(conn, parentId.intValue())) {
                    response.setAccountId(null);
                    setErrorCode(StatusCode.IAS_BAD_ACCOUNT_ID, null, response);
                    cont = false;
        	}
            }
        	
            if (cont) {
        	Integer accountId = AccountDML.registerAccount(conn, loginName, accountPswd, parentId, 
        	                                    nickName, fullName, emailAddress, birth_date);
        	if (accountId != null) {
        	    response.setAccountId(accountId);
        	    conn.commit();
        	} else {
                    response.setAccountId(null);
        	    setErrorCode(StatusCode.IAS_REGISTER_ACCOUNT_ERROR, null, response);
                }
            }                
        	
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("RegisterAccount error: ", e);
            
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.IAS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.IAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);
            log.error("RegisterAccount error: ", e);
            
        } finally {
            try {                
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            }
            catch (SQLException e) {
                log.warn("RegisterAccount error while closing connection: ", e);
            }
        }                
            
        return response;
    }   
}

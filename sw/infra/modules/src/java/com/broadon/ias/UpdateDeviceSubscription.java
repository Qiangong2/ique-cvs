package com.broadon.ias;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ias.*;

public class UpdateDeviceSubscription extends IdentityAuthenticationFunction {
    
    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(UpdateDeviceSubscription.class.getName());

    /**
     * Updates subscription record for a given Device ID
     */
    public static UpdateDeviceSubscriptionResponseType updateDeviceSubscription(
            IdentityAuthenticationServiceImpl service,
            UpdateDeviceSubscriptionRequestType updateDeviceSubscriptionRequest) 
        throws java.rmi.RemoteException 
    {
        long deviceId = updateDeviceSubscriptionRequest.getDeviceId();
                
        DeviceSubscriptionType sInfo = updateDeviceSubscriptionRequest.getDeviceSubscription();
        String subscriptionType = sInfo.getSubscriptionType();
        long startDate = sInfo.getStartDate();
        long expirationDate = sInfo.getExpirationDate();
        String referenceId = sInfo.getReferenceId();
        Long lastUpdated = sInfo.getLastUpdated();
        
        UpdateDeviceSubscriptionResponseType response = new UpdateDeviceSubscriptionResponseType();
        initResponse(updateDeviceSubscriptionRequest, response);
        response.setDeviceId(deviceId);
       
        if (subscriptionType == null || subscriptionType.equals("")) {
            setErrorCode(StatusCode.IAS_BAD_REQUEST, null, response);
            log.debug("UpdateDeviceSubscription bad request error: " +
                    "deviceId=" + deviceId + 
                    ", subscriptionType=" + subscriptionType + 
                    ", startDate=" + startDate + ", expirationDate=" + expirationDate +
                    ", referenceId=" + referenceId);
            
            return response;
        }
        
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
        
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            
            // verify that deviceId is registered
            if (DeviceDML.validateDevice(conn, deviceId)) {
                
                // search for an existing subscription type for this device id
                List subscriptionsList = DeviceDML.getDeviceSubscriptions(conn, deviceId, subscriptionType);
                
                if (subscriptionsList != null && subscriptionsList.size() > 0)
                {
                    DeviceSubscriptionType[] subs = new DeviceSubscriptionType[subscriptionsList.size()];
                    subs = (DeviceSubscriptionType[]) subscriptionsList.toArray(subs);
                    
                    // condition to satisfy before a subscription can be updated, the last updated in the request 
                    // should match the record in the database
                    if (subs.length == 1 && lastUpdated != null && 
                            lastUpdated.compareTo(subs[0].getLastUpdated()) == 0) {
                        if (DeviceDML.updateDeviceSubscription(conn, deviceId, 
                                subscriptionType, startDate, expirationDate, referenceId))                 
                            conn.commit();                
                        else
                            setErrorCode(StatusCode.IAS_UPDATE_DEVICE_SUBSCRIPTION_ERROR, null, response);
                        
                    } else
                        setErrorCode(StatusCode.IAS_UPDATE_DEVICE_SUBSCRIPTION_ERROR, null, response);
                                       
                } else {
                    if (DeviceDML.insertDeviceSubscription(conn, deviceId, 
                            subscriptionType, startDate, expirationDate, referenceId))                  
                        conn.commit();                
                    else
                        setErrorCode(StatusCode.IAS_UPDATE_DEVICE_SUBSCRIPTION_ERROR, null, response);                
                }                
                                
            } else
                setErrorCode(StatusCode.IAS_BAD_DEVICE_ID, null, response);                
                                   
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("UpdateDeviceSubscription error: ", e);
            
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.IAS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.IAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);
            log.error("UpdateDeviceSubscription error: ", e);
            
        } finally {
            try {                
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            }
            catch (SQLException e) {
                log.warn("UpdateDeviceSubscription error while closing connection: ", e);
            }
        }                
            
        return response;
    }   
}

package com.broadon.ias;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ias.*;

public class GetAccountInfo extends IdentityAuthenticationFunction {
    
    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(GetAccountInfo.class.getName());

    /**
     * Returns account info for a given account id
     */
    public static GetAccountInfoResponseType getAccountInfo(
            IdentityAuthenticationServiceImpl service,
            GetAccountInfoRequestType getAccountInfoRequest) 
        throws java.rmi.RemoteException 
    {
        int accountId = getAccountInfoRequest.getAccountId();
                        
        GetAccountInfoResponseType response = new GetAccountInfoResponseType();
        initResponse(getAccountInfoRequest, response);
        response.setAccountId(accountId);
               
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
        
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            
            AccountInfoType aInfo = AccountDML.getAccountInfo(conn, accountId);
            response.setAccountInfo(aInfo);
            
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("GetAccountInfo error: ", e);
            
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.IAS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.IAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);       
            log.error("GetAccountInfo error: ", e);
            
        } finally {
            try {
                if (conn != null)
                    conn.close();
            }
            catch (SQLException e) {
                log.warn("GetAccountInfo error while closing connection: ", e);
            }
        }                
            
        return response;
    }    
}

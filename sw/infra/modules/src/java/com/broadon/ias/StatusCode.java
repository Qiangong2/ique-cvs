package com.broadon.ias;

/**
 * Response code specific to ias.  The codes defined here
 * must be distinct from any code that may be used by other services,
 * such as the ecs/ets etc. ias shall use error codes in the 5000 range.
 */
public class StatusCode extends com.broadon.status.StatusCode
{
    
    /** TO DO: Define list of error codes
     */
    
    // Require Login Error
    public static final String IAS_REQUIRE_LOGIN = "900";
    static final String IAS_REQUIRE_LOGIN_MSG =
        "IAS - The attempted operation requires the subscriber to be logged in";

    // Bad Device ID 
    public static final String IAS_BAD_DEVICE_ID = "901";
    static final String IAS_BAD_DEVICE_ID_MSG = 
        "IAS - Device Id is not registered";

    // Bad User ID
    public static final String IAS_BAD_ACCOUNT_ID = "902";
    static final String IAS_BAD_ACCOUNT_ID_MSG = 
        "IAS - Account Id is not registered";
    
    // Invalid Device Token
    public static final String IAS_INVALID_DEVICE_TOKEN_ID = "903";
    static final String IAS_INVALID_DEVICE_TOKEN_ID_MSG = 
        "IAS - Invalid device token";
    
    // Invalid Account Token
    public static final String IAS_INVALID_ACCOUNT_TOKEN_ID = "904";
    static final String IAS_INVALID_ACCOUNT_TOKEN_ID_MSG = 
        "IAS - Invalid account token";
    
    // Failed to update device subscription
    public static final String IAS_UPDATE_DEVICE_SUBSCRIPTION_ERROR = "921";
    static final String IAS_UPDATE_DEVICE_SUBSCRIPTION_ERROR_MSG = 
        "IAS - Failed to update device subscription";
    
    //  Failed to register device
    public static final String IAS_REGISTER_DEVICE_ERROR = "922";
    static final String IAS_REGISTER_DEVICE_ERROR_MSG = 
        "IAS - Failed to register device";
    
    //  Failed to generate device token
    public static final String IAS_GENERATE_DEVICE_TOKEN_ERROR = "923";
    static final String IAS_GENERATE_DEVICE_TOKEN_ERROR_MSG = 
        "IAS - Failed to generate device token";
    
    // Failed to update device info
    public static final String IAS_UPDATE_DEVICE_INFO_ERROR = "924";
    static final String IAS_UPDATE_DEVICE_INFO_ERROR_MSG = 
        "IAS - Failed to update device information";
    
    // Failed to update device accounts
    public static final String IAS_UPDATE_DEVICE_ACCOUNT_ERROR = "925";
    static final String IAS_UPDATE_DEVICE_ACCOUNT_ERROR_MSG = 
        "IAS - Failed to update device account";
   
    // Failed to authenticate account
    public static final String IAS_AUTHENTICATE_ACCOUNT_ERROR = "931";
    static final String IAS_AUTHENTICATE_ACCOUNT_ERROR_MSG = 
        "IAS - Failed to authenticate account";
    
    //  Failed to register account
    public static final String IAS_REGISTER_ACCOUNT_ERROR = "932";
    static final String IAS_REGISTER_ACCOUNT_ERROR_MSG = 
        "IAS - Failed to register account";
    
    //  Failed to generate account token
    public static final String IAS_GENERATE_ACCOUNT_TOKEN_ERROR = "933";
    static final String IAS_GENERATE_ACCOUNT_TOKEN_ERROR_MSG = 
        "IAS - Failed to generate account token";
    
    // Failed to update account info
    public static final String IAS_UPDATE_ACCOUNT_INFO_ERROR = "934";
    static final String IAS_UPDATE_ACCOUNT_INFO_ERROR_MSG = 
        "IAS - Failed to update account information";
    
    // Failed to update account attribute
    public static final String IAS_UPDATE_ACCOUNT_ATTRIBUTE_ERROR = "935";
    static final String IAS_UPDATE_ACCOUNT_ATTRIBUTE_ERROR_MSG = 
        "IAS - Failed to update account attribute";
    
    // Failed to update account attribute
    public static final String IAS_UPDATE_ACCOUNT_PASSWORD_ERROR = "936";
    static final String IAS_UPDATE_ACCOUNT_PASSWORD_ERROR_MSG = 
        "IAS - Failed to update account password";

    // Login Name already registered
    public static final String IAS_ACCOUNT_LOGIN_NAME_ALREADY_EXISTS_ERROR = "937";
    static final String IAS_ACCOUNT_LOGIN_NAME_ALREADY_EXISTS_ERROR_MSG = 
        "IAS - Login name already in use";
    
    //  Failed to sync external account
    public static final String IAS_SYNC_ACCOUNT_ERROR = "939";
    static final String IAS_SYNC_ACCOUNT_ERROR_MSG = 
        "IAS - Failed to sync external account";
    
    //  Failed to update device info
    public static final String IAS_DEVICE_ALREADY_REGISTERED_ERROR = "951";
    static final String IAS_DEVICE_ALREADY_REGISTERED_ERROR_MSG = 
        "IAS - Device Id is already registered";
       
    // Bad Request Error
    public static final String IAS_BAD_REQUEST = "998";
    static final String IAS_BAD_REQUEST_MSG =
        "IAS - Bad Request";

    // Internal Error (unspecfied) 
    public static final String IAS_INTERNAL_ERROR = "999";
    static final String IAS_INTERNAL_ERROR_MSG = 
        "IAS - Internal Error";
                
    static {
        addStatusCode(IAS_REQUIRE_LOGIN, IAS_REQUIRE_LOGIN_MSG);
        addStatusCode(IAS_BAD_DEVICE_ID, IAS_BAD_DEVICE_ID_MSG);
        addStatusCode(IAS_BAD_ACCOUNT_ID, IAS_BAD_ACCOUNT_ID_MSG);        
        addStatusCode(IAS_INVALID_DEVICE_TOKEN_ID, IAS_INVALID_DEVICE_TOKEN_ID_MSG);
        addStatusCode(IAS_INVALID_ACCOUNT_TOKEN_ID, IAS_INVALID_ACCOUNT_TOKEN_ID_MSG);
        
        addStatusCode(IAS_UPDATE_DEVICE_SUBSCRIPTION_ERROR, IAS_UPDATE_DEVICE_SUBSCRIPTION_ERROR_MSG);
        addStatusCode(IAS_REGISTER_DEVICE_ERROR, IAS_REGISTER_DEVICE_ERROR_MSG);
        addStatusCode(IAS_GENERATE_DEVICE_TOKEN_ERROR, IAS_GENERATE_DEVICE_TOKEN_ERROR_MSG);
        addStatusCode(IAS_UPDATE_DEVICE_INFO_ERROR, IAS_UPDATE_DEVICE_INFO_ERROR_MSG);
        addStatusCode(IAS_UPDATE_DEVICE_ACCOUNT_ERROR, IAS_UPDATE_DEVICE_ACCOUNT_ERROR_MSG); 
        
        addStatusCode(IAS_AUTHENTICATE_ACCOUNT_ERROR, IAS_AUTHENTICATE_ACCOUNT_ERROR_MSG);
        addStatusCode(IAS_REGISTER_ACCOUNT_ERROR, IAS_REGISTER_ACCOUNT_ERROR_MSG);
        addStatusCode(IAS_GENERATE_ACCOUNT_TOKEN_ERROR, IAS_GENERATE_ACCOUNT_TOKEN_ERROR_MSG);
        addStatusCode(IAS_UPDATE_ACCOUNT_INFO_ERROR, IAS_UPDATE_ACCOUNT_INFO_ERROR_MSG);
        addStatusCode(IAS_UPDATE_ACCOUNT_ATTRIBUTE_ERROR, IAS_UPDATE_ACCOUNT_ATTRIBUTE_ERROR_MSG);
        addStatusCode(IAS_UPDATE_ACCOUNT_PASSWORD_ERROR, IAS_UPDATE_ACCOUNT_PASSWORD_ERROR_MSG);
        addStatusCode(IAS_ACCOUNT_LOGIN_NAME_ALREADY_EXISTS_ERROR, IAS_ACCOUNT_LOGIN_NAME_ALREADY_EXISTS_ERROR_MSG);
        addStatusCode(IAS_SYNC_ACCOUNT_ERROR, IAS_SYNC_ACCOUNT_ERROR_MSG);
        
        addStatusCode(IAS_DEVICE_ALREADY_REGISTERED_ERROR, IAS_DEVICE_ALREADY_REGISTERED_ERROR_MSG);
        
        addStatusCode(IAS_BAD_REQUEST, IAS_BAD_REQUEST_MSG);
        addStatusCode(IAS_INTERNAL_ERROR, IAS_INTERNAL_ERROR_MSG);
    }

}

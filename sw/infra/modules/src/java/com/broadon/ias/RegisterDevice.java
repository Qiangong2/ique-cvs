package com.broadon.ias;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ias.*;

public class RegisterDevice extends IdentityAuthenticationFunction {
    
    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(RegisterDevice.class.getName());

    /**
     * Updates subscription record for a given Device ID
     */
    public static RegisterDeviceResponseType registerDevice(
            IdentityAuthenticationServiceImpl service,
            RegisterDeviceRequestType registerDeviceRequest) 
        throws java.rmi.RemoteException 
    {
        long deviceId = registerDeviceRequest.getDeviceId();                
        byte[] deviceCert = registerDeviceRequest.getDeviceCert();
        String serialNumber = registerDeviceRequest.getSerialNumber();
        byte[] configCode = registerDeviceRequest.getConfigCode();
                
        RegisterDeviceResponseType response = new RegisterDeviceResponseType();
        initResponse(registerDeviceRequest, response);
        response.setDeviceId(deviceId);
  
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
        
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            
            // check if the device has already been registered
            if (DeviceDML.validateDevice(conn, deviceId))
                setErrorCode(StatusCode.IAS_DEVICE_ALREADY_REGISTERED_ERROR, null, response);
            
            else {
                // TODO: Verify deviceCert, Extract publicKey from device cert
                String publicKey = "";
                
                int deviceTypeId = (int) (deviceId >> 32);
                
                if (DeviceDML.registerDevice(conn, deviceId, configCode, 
                        deviceTypeId, publicKey, serialNumber))                  
                   conn.commit();
                else
                   setErrorCode(StatusCode.IAS_REGISTER_DEVICE_ERROR, null, response);                
            }  
                       
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("RegisterDevice error: ", e);
            
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.IAS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.IAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);
            log.error("RegisterDevice error: ", e);
            
        } finally {
            try {                
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            }
            catch (SQLException e) {
                log.warn("RegisterDevice error while closing connection: ", e);
            }
        }                
            
        return response;
    }   
}

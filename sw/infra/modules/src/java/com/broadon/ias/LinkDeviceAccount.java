package com.broadon.ias;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ias.*;

public class LinkDeviceAccount extends IdentityAuthenticationFunction {
    
    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(LinkDeviceAccount.class.getName());

    /**
     * Links accountId with the deviceId
     */
    public static LinkDeviceAccountResponseType linkDeviceAccount(
            IdentityAuthenticationServiceImpl service,
            LinkDeviceAccountRequestType linkDeviceAccountRequest) 
        throws java.rmi.RemoteException 
    {
        long deviceId = linkDeviceAccountRequest.getDeviceId();
        int accountId = linkDeviceAccountRequest.getAccountId();
                
        LinkDeviceAccountResponseType response = new LinkDeviceAccountResponseType();
        initResponse(linkDeviceAccountRequest, response);
        response.setDeviceId(deviceId);
        
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
        
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            
            // if the link exists, update the link_date to current db time and remove_date to null
            // else insert a new link
            if (DeviceDML.setDeviceAccount(conn, deviceId, accountId, false))
            	conn.commit();
            else
            	setErrorCode(StatusCode.IAS_UPDATE_DEVICE_ACCOUNT_ERROR, null, response); 
                       
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("LinkDeviceAccount error: ", e);
            
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.IAS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.IAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);
            log.error("LinkDeviceAccount error: ", e);
            
        } finally {
            try {                
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            }
            catch (SQLException e) {
                log.warn("LinkDeviceAccount error while closing connection: ", e);
            }
        }                
            
        return response;
    }   
}

package com.broadon.ias;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ias.*;

public class ExportAccount extends IdentityAuthenticationFunction {
    
    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(ExportAccount.class.getName());

    /**
     * Exports the external account data
     */
    public static ExportAccountResponseType exportAccount(
            IdentityAuthenticationServiceImpl service,
            ExportAccountRequestType ExportAccountRequest) 
        throws java.rmi.RemoteException 
    {
        ExternalAccountType eInfo[] = ExportAccountRequest.getExternalAccountInfo();
                
        ExportAccountResponseType response = new ExportAccountResponseType();
        initResponse(ExportAccountRequest, response);
               
        if (eInfo == null || eInfo.length == 0) {
            setErrorCode(StatusCode.IAS_BAD_REQUEST, null, response);
            log.debug("ExportAccount bad request error: " +
                    "eInfo=" + (eInfo == null ? "null" : String.valueOf(eInfo.length)));
            
            return response;
        }
        
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
        
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            
            boolean success = true;
            
            for (int i = 0; i < eInfo.length; i++) {
                String accountId = eInfo[i].getExternalAccountId();
                String entity = eInfo[i].getExternalEntity();
                String data = eInfo[i].getExternalData();       
            
                // update the database with the external Export
                if (!AccountDML.setExternalAccount(conn, accountId, entity, data)) {                   
                    success = false;
                    break;
                }                    
            }
            
            if (success)
                conn.commit();                
            else
                setErrorCode(StatusCode.IAS_SYNC_ACCOUNT_ERROR, null, response);
            
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("ExportAccount error: ", e);
            
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.IAS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.IAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);
            log.error("ExportAccount error: ", e);
            
        } finally {
            try {                
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            }
            catch (SQLException e) {
                log.warn("ExportAccount error while closing connection: ", e);
            }
        }                
            
        return response;
    }   
}

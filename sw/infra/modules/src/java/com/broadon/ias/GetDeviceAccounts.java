package com.broadon.ias;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ias.*;

public class GetDeviceAccounts extends IdentityAuthenticationFunction {
    
    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(GetDeviceAccounts.class.getName());

    /**
     * Returns device info (public key, serial number) and a list of subscriptions for a given device ID
     */
    public static GetDeviceAccountsResponseType getDeviceAccounts(
            IdentityAuthenticationServiceImpl service,
            GetDeviceAccountsRequestType getDeviceAccountsRequest) 
        throws java.rmi.RemoteException 
    {
        long deviceId = getDeviceAccountsRequest.getDeviceId();
                
        GetDeviceAccountsResponseType response = new GetDeviceAccountsResponseType();
        initResponse(getDeviceAccountsRequest, response);
        response.setDeviceId(deviceId);
 
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
        
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);

	    // query for device accounts
	    List accountsList = DeviceDML.getDeviceAccounts(conn, deviceId);

	    if (accountsList != null && accountsList.size() > 0) {
		DeviceAccountType[] accts = new DeviceAccountType[accountsList.size()];
		accts = (DeviceAccountType[]) accountsList.toArray(accts);
		response.setDeviceAccounts(accts);
	    } else
		response.setDeviceAccounts(null);
           
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("GetDeviceAccounts error: ", e);
            
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.IAS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.IAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);       
            log.error("GetDeviceAccounts error: ", e);
            
        } finally {
            try {
                if (conn != null)
                    conn.close();
            }
            catch (SQLException e) {
                log.warn("GetDeviceAccounts error while closing connection: ", e);
            }
        }                
            
        return response;
    }    
}

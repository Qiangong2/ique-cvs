package com.broadon.ias;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ias.*;

public class ValidateAccountToken extends IdentityAuthenticationFunction {
    
    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(ValidateAccountToken.class.getName());

    /**
     * Validates the account token and returns the result in terms of the error code
     * in the response (000 - Validated)
     */
    public static ValidateAccountTokenResponseType validateAccountToken(
            IdentityAuthenticationServiceImpl service,
            ValidateAccountTokenRequestType validateAccountTokenRequest) 
        throws java.rmi.RemoteException 
    {
        String accountToken = validateAccountTokenRequest.getTokenId();
        int accountId = validateAccountTokenRequest.getAccountId();
                
        ValidateAccountTokenResponseType response = new ValidateAccountTokenResponseType();
        initResponse(validateAccountTokenRequest, response);
        response.setAccountId(accountId);
               
        if (accountToken == null || accountToken.equals("")) {
            setErrorCode(StatusCode.IAS_BAD_REQUEST, null, response);
            log.debug("ValidateAccountToken bad request error: " +
                    "accountId=" + accountId + ", accountToken=" + accountToken);
            
            return response;
        }
        
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
        
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            
            // validate the given token
            if (!AccountDML.validateAccountToken(conn, accountId, accountToken))
                setErrorCode(StatusCode.IAS_INVALID_ACCOUNT_TOKEN_ID, null, response);
            
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("ValidateAccountToken error: ", e);
            
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.IAS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.IAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);       
            log.error("ValidateAccountToken error: ", e);
            
        } finally {
            try {
                if (conn != null)
                    conn.close();
            }
            catch (SQLException e) {
                log.warn("ValidateAccountToken error while closing connection: ", e);
            }
        }                
            
        return response;
    }
}

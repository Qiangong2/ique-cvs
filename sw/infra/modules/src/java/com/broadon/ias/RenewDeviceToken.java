package com.broadon.ias;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ias.*;

public class RenewDeviceToken extends IdentityAuthenticationFunction {
    
    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(RenewDeviceToken.class.getName());

    /**
     * Authenticates device and returns encrypted token back to the device
     */
    public static RenewDeviceTokenResponseType renewDeviceToken(
            IdentityAuthenticationServiceImpl service,
            RenewDeviceTokenRequestType renewDeviceTokenRequest) 
        throws java.rmi.RemoteException 
    {
        String deviceToken = renewDeviceTokenRequest.getTokenId();
        long deviceId = renewDeviceTokenRequest.getDeviceId();
        
        RenewDeviceTokenResponseType response = new RenewDeviceTokenResponseType();
        initResponse(renewDeviceTokenRequest, response);
        response.setDeviceId(deviceId);
       
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
        
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            
            // Generate a new token
            String newDeviceToken = generateToken(conn);
                    
            // Expire the current token and insert the new token
            if (DeviceDML.expireDeviceToken(conn, deviceId, deviceToken) &&
                DeviceDML.insertDeviceToken(conn, deviceId, newDeviceToken, 
                        service.getDeviceMaxLoginSecs())) {
                response.setTokenId(newDeviceToken);
                conn.commit();
            } else {
                newDeviceToken = null;                                            
                response.setTokenId(null);
                setErrorCode(StatusCode.IAS_GENERATE_DEVICE_TOKEN_ERROR, null, response);                   
            }                  
                       
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("RenewDeviceToken error: ", e);
            
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.IAS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.IAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);
            log.error("RenewDeviceToken error: ", e);
            
        } finally {
            try {                
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            }
            catch (SQLException e) {
                log.warn("RenewDeviceToken error while closing connection: ", e);
            }
        }                
            
        return response;
    }
    
    
}

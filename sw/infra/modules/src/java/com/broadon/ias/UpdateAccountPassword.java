package com.broadon.ias;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ias.*;

public class UpdateAccountPassword extends IdentityAuthenticationFunction {
    
    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(UpdateAccountPassword.class.getName());

    /**
     * Updates account password for a given account id
     */
    public static UpdateAccountPasswordResponseType updateAccountPassword(
            IdentityAuthenticationServiceImpl service,
            UpdateAccountPasswordRequestType updateAccountPasswordRequest) 
        throws java.rmi.RemoteException 
    {
        int accountId = updateAccountPasswordRequest.getAccountId();
        String accountPswd = updateAccountPasswordRequest.getPassword();
        String newPswd = updateAccountPasswordRequest.getNewPassword();
        
        UpdateAccountPasswordResponseType response = new UpdateAccountPasswordResponseType();
        initResponse(updateAccountPasswordRequest, response);
        response.setAccountId(accountId);
       
        if (accountPswd == null || accountPswd.equals("") ||
            newPswd == null || newPswd.equals("")) {
            setErrorCode(StatusCode.IAS_BAD_REQUEST, null, response);
            log.debug("UpdateAccountPassword bad request error: " +
                    "accountId=" + accountId);
            
            return response;
        }
        
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
        
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            
            // authenticate account
            if (AccountDML.authenticateAccount(conn, accountId, accountPswd)) {
                    
                // set new password for the account
                if (AccountDML.updateAccountPassword(conn, accountId, newPswd))
                    conn.commit();                         
                else
                    setErrorCode(StatusCode.IAS_UPDATE_ACCOUNT_PASSWORD_ERROR, null, response);
                    
            } else
                setErrorCode(StatusCode.IAS_AUTHENTICATE_ACCOUNT_ERROR, null, response);              
                                   
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("UpdateAccountPassword error: ", e);
            
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.IAS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.IAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);
            log.error("UpdateAccountPassword error: ", e);
            
        } finally {
            try {                
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            }
            catch (SQLException e) {
                log.warn("UpdateAccountPassword error while closing connection: ", e);
            }
        }                
            
        return response;
    }   
}

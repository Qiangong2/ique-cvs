package com.broadon.ias;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ias.*;

public class ResetAccountPassword extends IdentityAuthenticationFunction {
    
    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(ResetAccountPassword.class.getName());

    /**
     * Reset account password for a given account id based on account attributes
     */
    public static ResetAccountPasswordResponseType resetAccountPassword(
            IdentityAuthenticationServiceImpl service,
            ResetAccountPasswordRequestType resetAccountPasswordRequest) 
        throws java.rmi.RemoteException 
    {
        int accountId = resetAccountPasswordRequest.getAccountId();
        String newPswd = resetAccountPasswordRequest.getNewPassword();
 
        ResetAccountPasswordResponseType response = new ResetAccountPasswordResponseType();
        initResponse(resetAccountPasswordRequest, response);
        response.setAccountId(accountId);
       
        if (newPswd == null || newPswd.equals("")) {
            setErrorCode(StatusCode.IAS_BAD_REQUEST, null, response);
            log.debug("ResetAccountPassword bad request error: " +
                    "accountId=" + accountId);
            
            return response;
        }
        
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
        
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            
            if (AccountDML.updateAccountPassword(conn, accountId, newPswd))
            	conn.commit();
            else 
            	setErrorCode(StatusCode.IAS_UPDATE_ACCOUNT_PASSWORD_ERROR, null, response);
                                             
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("ResetAccountPassword error: ", e);
            
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.IAS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.IAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);
            log.error("ResetAccountPassword error: ", e);
            
        } finally {
            try {                
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            }
            catch (SQLException e) {
                log.warn("ResetAccountPassword error while closing connection: ", e);
            }
        }                
            
        return response;
    }   
}

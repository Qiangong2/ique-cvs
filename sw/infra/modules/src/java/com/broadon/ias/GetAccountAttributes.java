package com.broadon.ias;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ias.*;

public class GetAccountAttributes extends IdentityAuthenticationFunction {
    
    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(GetAccountAttributes.class.getName());

    /**
     * Returns a list of account atrribute id/value for a given account ID
     */
    public static GetAccountAttributesResponseType getAccountAttributes(
            IdentityAuthenticationServiceImpl service,
            GetAccountAttributesRequestType getAccountAttributesRequest) 
        throws java.rmi.RemoteException 
    {
        int accountId = getAccountAttributesRequest.getAccountId();
        int accountAttributeId[] = getAccountAttributesRequest.getAttributeId();
        
        GetAccountAttributesResponseType response = new GetAccountAttributesResponseType();
        initResponse(getAccountAttributesRequest, response);
        response.setAccountId(accountId);
               
        if (accountAttributeId == null || accountAttributeId.length == 0) {
            response.setAccountAttributes(null);
            setErrorCode(StatusCode.IAS_BAD_REQUEST, null, response);
            log.debug("GetAccountAttributes bad request error: " +
                    "accountId=" + accountId + ", accountAttributeId=" + 
                    (accountAttributeId == null ? "null" : String.valueOf(accountAttributeId.length))); 
            
            return response;
        }
        
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
        
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            
            // query for the attributes for this accountId
            List attributesList = AccountDML.getAccountAttributes(conn, accountId, accountAttributeId);
                    
            if (attributesList != null && attributesList.size() > 0) {
                AttributeType[] atts = new AttributeType[attributesList.size()];
                atts = (AttributeType[]) attributesList.toArray(atts);
                response.setAccountAttributes(atts);
                        
            } else
                response.setAccountAttributes(null);
            
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("GetAccountAttributes error: ", e);
            
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.IAS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.IAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);       
            log.error("GetAccountAttributes error: ", e);
            
        } finally {
            try {
                if (conn != null)
                    conn.close();
            }
            catch (SQLException e) {
                log.warn("GetAccountAttributes error while closing connection: ", e);
            }
        }                
            
        return response;
    }    
}

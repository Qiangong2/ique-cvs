package com.broadon.ias;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import com.broadon.util.Base64;
import com.broadon.wsapi.ias.DeviceAccountType;
import com.broadon.wsapi.ias.DeviceInfoType;
import com.broadon.wsapi.ias.DeviceSubscriptionType;

/**
 * Data manipulation library functions defined here pertain to device
 * authentication, validation, update and query
 */
public class DeviceDML
{
    /** Define all SQL statements
     */

    private static final String TABLE_IAS_DEVICES = "IAS_DEVICES";
    private static final String TABLE_IAS_DEVICE_ACCOUNTS = "IAS_ACCOUNT_DEVICES";
    private static final String TABLE_IAS_DEVICE_SUBSCRIPTIONS = "IAS_DEVICE_SUBSCRIPTIONS";
    private static final String TABLE_IAS_TOKENS = "IAS_AUTHENTICATION_TOKENS";

    private static final String EXPIRE_DEVICE_TOKEN =
        "UPDATE " + TABLE_IAS_TOKENS + " SET is_expired = 1 WHERE token_id = ? AND device_id = ?";
    
    private static final String GET_DEVICE_ACCOUNTS = 
        "SELECT account_id, BCCUTIL.ConvertTimeStamp(link_date) link_date, " +
        "BCCUTIL.ConvertTimeStamp(remove_date) remove_date FROM " + TABLE_IAS_DEVICE_ACCOUNTS + 
        " WHERE device_id = ?";
    
    private static final String GET_DEVICE_INFO = 
        "SELECT public_key, serial_no, config_code, device_type_id, " +
        "BCCUTIL.ConvertTimeStamp(register_date) register_date FROM " + 
        TABLE_IAS_DEVICES + " WHERE device_id = ?";
    
    private static final String GET_DEVICE_SUBSCRIPTIONS = 
        "SELECT subscription_type, BCCUTIL.ConvertTimeStamp(subscription_start) subscription_start, " +
        "BCCUTIL.ConvertTimeStamp(subscription_exp) subscription_exp, " +
        "BCCUTIL.ConvertTimeStamp(last_updated) last_updated, reference_id FROM " + 
        TABLE_IAS_DEVICE_SUBSCRIPTIONS + " WHERE device_id = ?";
    
    private static final String GET_DEVICE_TOKEN =
        "SELECT token_id FROM " + TABLE_IAS_TOKENS + " WHERE device_id = ? AND is_expired = 0 AND " +
        "(sys_extract_utc(current_timestamp) BETWEEN token_start_time AND token_expire_time)";
    
    private static final String INSERT_DEVICE_ACCOUNT =
        "INSERT INTO " + TABLE_IAS_DEVICE_ACCOUNTS + " (device_id, account_id, link_date) " +
        "VALUES (?,?,sys_extract_utc(current_timestamp))";
    
    private static final String INSERT_DEVICE_SUBSCRIPTION =
        "INSERT INTO " + TABLE_IAS_DEVICE_SUBSCRIPTIONS + 
        " (device_id, subscription_type, subscription_start, subscription_exp, reference_id) " +
        "VALUES (?,?,BCCUTIL.ConvertTimeStamp(?),BCCUTIL.ConvertTimeStamp(?),?)";
    
    private static final String INSERT_DEVICE_TOKEN =
        "INSERT INTO " + TABLE_IAS_TOKENS + " (token_id, device_id, token_start_time, token_expire_time, is_expired) " +
        "VALUES (?,?,sys_extract_utc(current_timestamp),sys_extract_utc(current_timestamp)+?,0)";  
    
    private static final String LINK_DEVICE_ACCOUNT =
        "UPDATE " + TABLE_IAS_DEVICE_ACCOUNTS + " SET link_date = sys_extract_utc(current_timestamp), " +
        "remove_date = NULL WHERE device_id = ? AND account_id = ?";
    
    private static final String REGISTER_DEVICE =
        "INSERT INTO " + TABLE_IAS_DEVICES + " (device_id, config_code, device_type_id, " +
        "public_key, serial_no, register_date) VALUES (?,?,?,?,?,sys_extract_utc(current_timestamp))";
    
    private static final String REMOVE_DEVICE_ACCOUNT =
        "UPDATE " + TABLE_IAS_DEVICE_ACCOUNTS + " SET remove_date = sys_extract_utc(current_timestamp) " +
        "WHERE device_id = ? AND account_id = ?";
    
    private static final String UPDATE_DEVICE_SUBSCRIPTION =
        "UPDATE " + TABLE_IAS_DEVICE_SUBSCRIPTIONS + " SET subscription_start = BCCUTIL.ConvertTimeStamp(?), " +
        "subscription_exp = BCCUTIL.ConvertTimeStamp(?), reference_id = ? WHERE device_id = ? AND subscription_type = ?"; 
    
    private static final String VALIDATE_DEVICE = 
        "SELECT count(*) FROM " + TABLE_IAS_DEVICES + " WHERE device_id = ?";
    
    private static final String VALIDATE_DEVICE_TOKEN =
        "SELECT count(*) FROM " + TABLE_IAS_TOKENS + " WHERE device_id = ? AND token_id = ? AND is_expired = 0 AND " +
        "(sys_extract_utc(current_timestamp) BETWEEN token_start_time AND token_expire_time)";    
    
    public static boolean expireDeviceToken(Connection conn,            
                                            long deviceId, 
                                            String deviceToken)            
        throws SQLException
    {
        int result = 0;
        PreparedStatement stmt = null;
        
        try {
            stmt = conn.prepareStatement(EXPIRE_DEVICE_TOKEN);
            stmt.setString(1, deviceToken);
            stmt.setLong(2, deviceId);
                    
            result = stmt.executeUpdate();
        
        } finally {
            if (stmt != null)
                stmt.close();
        }
        
        return (result == 1);
    }
    
    public static List getDeviceAccounts(Connection conn, long deviceId)
        throws SQLException
    {
        List accountsList = new ArrayList();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {                
            stmt = conn.prepareStatement(GET_DEVICE_ACCOUNTS);
            stmt.setLong(1, deviceId);
            
            rs = stmt.executeQuery();
            while (rs.next()) {
                DeviceAccountType acct = null;
                
                long removeDate = rs.getLong("REMOVE_DATE");
                boolean isNull = rs.wasNull();
                if (!isNull) {
                    acct = new DeviceAccountType(
                        rs.getInt("ACCOUNT_ID"),
                        rs.getLong("LINK_DATE"),
                        new Long(removeDate));
                } else {
                    acct = new DeviceAccountType(
                        rs.getInt("ACCOUNT_ID"),
                        rs.getLong("LINK_DATE"),
                        null);
                }
                                              
                accountsList.add(acct);
            }            
        
        } finally {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
        
        return accountsList;
    }
    
    public static DeviceInfoType getDeviceInfo(Connection conn, long deviceId)
        throws SQLException, IOException
    {
        DeviceInfoType dInfo = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            stmt = conn.prepareStatement(GET_DEVICE_INFO);
            stmt.setLong(1, deviceId);
            
            rs = stmt.executeQuery();
            if (rs.next()) {
                dInfo = new DeviceInfoType();
                
                dInfo.setPublicKey(rs.getString("PUBLIC_KEY"));
                dInfo.setSerialNumber(rs.getString("SERIAL_NO"));
                dInfo.setDeviceTypeId(rs.getInt("DEVICE_TYPE_ID"));                
                dInfo.setRegisterDate(rs.getLong("REGISTER_DATE"));
                
                String configCode = rs.getString("CONFIG_CODE");
                if (rs.wasNull())
                    dInfo.setConfigCode(null);
                else
                    dInfo.setConfigCode(new Base64().decode(configCode));
            }
        
        } finally {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
        
        return dInfo;
    }
    
    public static List getDeviceSubscriptions(Connection conn, 
                                              long deviceId,
                                              String subscriptionType)
        throws SQLException
    {
        List subscriptionsList = new ArrayList();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            String sql = GET_DEVICE_SUBSCRIPTIONS;       
        
            if (subscriptionType != null && !subscriptionType.equals(""))
                sql = sql + " AND subscription_type = '" + subscriptionType + "'";
            
            stmt = conn.prepareStatement(sql);
            stmt.setLong(1, deviceId);
            
            rs = stmt.executeQuery();
            while (rs.next()) {
                DeviceSubscriptionType subs = null;
                
                long lastUpdated = rs.getLong("LAST_UPDATED");
                boolean isNull = rs.wasNull();
                if (!isNull) {
                    subs = new DeviceSubscriptionType(
                            rs.getString("SUBSCRIPTION_TYPE"),
                            rs.getLong("SUBSCRIPTION_START"),
                            rs.getLong("SUBSCRIPTION_EXP"),
                            new Long(lastUpdated),
                            rs.getString("REFERENCE_ID"));
                } else {
                    subs = new DeviceSubscriptionType(
                            rs.getString("SUBSCRIPTION_TYPE"),
                            rs.getLong("SUBSCRIPTION_START"),
                            rs.getLong("SUBSCRIPTION_EXP"),
                            null,
                            rs.getString("REFERENCE_ID"));
                }                
                                                
                subscriptionsList.add(subs);
            }            
        
        } finally {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
    
        return subscriptionsList;
    }    
    
    public static String getDeviceToken(Connection conn, long deviceId)
        throws SQLException
    {
        String deviceToken = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            stmt = conn.prepareStatement(GET_DEVICE_TOKEN);
            stmt.setLong(1, deviceId);
        
            rs = stmt.executeQuery();
            if (rs.next())
                deviceToken = rs.getString("TOKEN_ID");
            
        } finally {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
    
        return deviceToken;
    }
    
    public static boolean insertDeviceAccount(Connection conn, 
                                              long deviceId,
                                              int accountId)
    throws SQLException
    {
        int result = 0;
        PreparedStatement stmt = null;
        
        try {
            stmt = conn.prepareStatement(INSERT_DEVICE_ACCOUNT);
            stmt.setLong(1, deviceId);
            stmt.setInt(2, accountId);
            
            result = stmt.executeUpdate();
        
        } finally {
            if (stmt != null)
                stmt.close();
        }
        
        return (result == 1);
    }
    
    public static boolean insertDeviceSubscription(Connection conn, 
                                                   long deviceId,
                                                   String subType, 
                                                   long startDate, 
                                                   long expDate,
                                                   String referenceId)
        throws SQLException
    {
        int result = 0;
        PreparedStatement stmt = null;
        
        try {
            stmt = conn.prepareStatement(INSERT_DEVICE_SUBSCRIPTION);
            stmt.setLong(1, deviceId);
            stmt.setString(2, subType);
            stmt.setLong(3, startDate);
            stmt.setLong(4, expDate);
            stmt.setString(5, referenceId);
                    
            result = stmt.executeUpdate();
        
        } finally {
            if (stmt != null)
                stmt.close();
        }
        
        return (result == 1);
    }
    
    public static boolean insertDeviceToken(Connection conn,            
                                            long deviceId, 
                                            String deviceToken,
                                            long deviceMaxLoginSecs)
        throws SQLException
    {
        int result = 0;
        PreparedStatement stmt = null;
        
        try {
            stmt = conn.prepareStatement(INSERT_DEVICE_TOKEN);
            stmt.setString(1, deviceToken);
            stmt.setLong(2, deviceId);
            stmt.setLong(3, deviceMaxLoginSecs/(24*60*60));
            
            result = stmt.executeUpdate();
        
        } finally {
            if (stmt != null)
                stmt.close();
        }
        
        return (result == 1);
    }
    
    public static boolean registerDevice(Connection conn,            
                                         long deviceId,
                                         byte[] configCode,
                                         int deviceTypeId,
                                         String publicKey,
                                         String serialNumber)
        throws SQLException
    {
        int result = 0;
        PreparedStatement stmt = null;
        
        try {
            stmt = conn.prepareStatement(REGISTER_DEVICE);
            stmt.setLong(1, deviceId);
            
            if (configCode != null)
                stmt.setString(2, new Base64().encode(configCode));
            else
                stmt.setNull(2, Types.VARCHAR);
                            
            stmt.setInt(3, deviceTypeId);                
            stmt.setString(4, publicKey);
            stmt.setString(5, serialNumber);
        	
            result = stmt.executeUpdate();
        	
        } finally {
            if (stmt != null)
                stmt.close();
        }
        
        return (result == 1);
    }
    
    public static boolean setDeviceAccount(Connection conn,
                                           long deviceId,
                                           int accountId,
                                           boolean remove)
        throws SQLException
    {
        if (updateDeviceAccount(conn, deviceId, accountId, remove))
            return true;
        else
            return (insertDeviceAccount(conn, deviceId, accountId));                
    }
    
    public static boolean setDeviceSubscription(Connection conn, 
                                                long deviceId,
                                                String subType, 
                                                long startDate, 
                                                long expDate,
                                                String referenceId)
        throws SQLException
    {
        if (updateDeviceSubscription(conn, deviceId, subType, startDate, expDate, referenceId))
            return true;
        else 
            return (insertDeviceSubscription(conn, deviceId, subType, startDate, expDate, referenceId));     
    }

    public static boolean updateDeviceAccount(Connection conn, 
                                              long deviceId,
                                              int accountId,
                                              boolean remove)
        throws SQLException
    {
        int result = 0;
        PreparedStatement stmt = null;
        
        try {
            if (remove)
                stmt = conn.prepareStatement(REMOVE_DEVICE_ACCOUNT);
            else 
                stmt = conn.prepareStatement(LINK_DEVICE_ACCOUNT);
            
            stmt.setLong(1, deviceId);
            stmt.setInt(2, accountId);
            
            result = stmt.executeUpdate();
            
        } finally {
            if (stmt != null)
                stmt.close();
        }
        
        return (result == 1);
    }
    
    public static boolean updateDeviceSubscription(Connection conn, 
                                                   long deviceId,
                                                   String subType, 
                                                   long startDate, 
                                                   long expDate,
                                                   String referenceId)
        throws SQLException
    {
        int result = 0;
        PreparedStatement stmt = null;
        
        try {
            stmt = conn.prepareStatement(UPDATE_DEVICE_SUBSCRIPTION);
            stmt.setLong(1, startDate);
            stmt.setLong(2, expDate);
            stmt.setString(3, referenceId);
            stmt.setLong(4, deviceId);
            stmt.setString(5, subType);
            
            result = stmt.executeUpdate();
            
        } finally {
            if (stmt != null)
                stmt.close();
        }
        
        return (result == 1);
    }
    
    public static boolean validateDevice(Connection conn,
                                         long deviceId)
    throws SQLException
    {
        boolean isValid = false;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            stmt = conn.prepareStatement(VALIDATE_DEVICE);
            stmt.setLong(1, deviceId);
            
            rs = stmt.executeQuery();
            if (rs.next() && rs.getInt(1) == 1)
                isValid = true;
        
        } finally {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
        
        return isValid;        
    }
    
    public static boolean validateDeviceToken(Connection conn,
                                              long deviceId,
                                              String deviceToken)
        throws SQLException
    {
        boolean isValid = false;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            stmt = conn.prepareStatement(VALIDATE_DEVICE_TOKEN);
            stmt.setLong(1, deviceId);
            stmt.setString(2, deviceToken);
            
            rs = stmt.executeQuery();
            if (rs.next() && rs.getInt(1) == 1)
                isValid = true;
        
        } finally {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
        
        return isValid;        
    }

} // DeviceDML

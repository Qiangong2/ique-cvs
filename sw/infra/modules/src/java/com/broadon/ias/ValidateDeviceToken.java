package com.broadon.ias;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ias.*;

public class ValidateDeviceToken extends IdentityAuthenticationFunction {
    
    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(ValidateDeviceToken.class.getName());

    /**
     * Validates the device token ID and returns the result in terms of the error code
     * in the response (000 - Validated)
     */
    public static ValidateDeviceTokenResponseType validateDeviceToken(
            IdentityAuthenticationServiceImpl service,
            ValidateDeviceTokenRequestType validateDeviceTokenRequest) 
        throws java.rmi.RemoteException 
    {
        String deviceToken = validateDeviceTokenRequest.getTokenId();
        long deviceId = validateDeviceTokenRequest.getDeviceId();
        
        ValidateDeviceTokenResponseType response = new ValidateDeviceTokenResponseType();
        initResponse(validateDeviceTokenRequest, response);
        response.setDeviceId(deviceId);
       
        if (deviceToken == null || deviceToken.equals("")) {
            setErrorCode(StatusCode.IAS_BAD_REQUEST, null, response);
            log.debug("ValidateAccountToken bad request error: " +
                    "deviceId=" + deviceId + ", deviceToken=" + deviceToken);
                
            return response;
        }
        
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
        
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            
            // validate the given token
            if (!DeviceDML.validateDeviceToken(conn, deviceId, deviceToken))
                setErrorCode(StatusCode.IAS_INVALID_DEVICE_TOKEN_ID, null, response);
            
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("ValidateDeviceToken error: ", e);
            
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.IAS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.IAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);       
            log.error("ValidateDeviceToken error: ", e);
            
        } finally {
            try {
                if (conn != null)
                    conn.close();
            }
            catch (SQLException e) {
                log.warn("ValidateDeviceToken error while closing connection: ", e);
            }
        }                
            
        return response;
    }
}

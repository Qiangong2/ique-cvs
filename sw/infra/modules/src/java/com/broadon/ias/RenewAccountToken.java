package com.broadon.ias;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ias.*;

public class RenewAccountToken extends IdentityAuthenticationFunction {
    
    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(RenewAccountToken.class.getName());

    /**
     * Renews an account token that is still valid upon matching account password
     * and returns a new account token
     */
    public static RenewAccountTokenResponseType renewAccountToken(
            IdentityAuthenticationServiceImpl service,
            RenewAccountTokenRequestType renewAccountTokenRequest) 
        throws java.rmi.RemoteException 
    {
        int accountId = renewAccountTokenRequest.getAccountId();
        String accountToken = renewAccountTokenRequest.getTokenId();
        String accountPswd = renewAccountTokenRequest.getPassword();
        
        RenewAccountTokenResponseType response = new RenewAccountTokenResponseType();
        initResponse(renewAccountTokenRequest, response);
        response.setAccountId(accountId);
               
        if (accountPswd == null || accountPswd.equals("")) {
            response.setTokenId(null);
            setErrorCode(StatusCode.IAS_BAD_REQUEST, null, response);
            log.debug("RenewAccountToken bad request error: " +
                    "accountId=" + accountId + ", accountToken=" + accountToken);
            
            return response;
        }
        
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
        
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            
            // authenticate account
            if (AccountDML.authenticateAccount(conn, accountId, accountPswd)) {
                    
                // generate a new token
                String newAccountToken = generateToken(conn);
                    
                // expire the current token and insert the new token
                if (AccountDML.expireAccountToken(conn, accountId, accountToken) &&
                    AccountDML.insertAccountToken(conn, accountId, newAccountToken, 
                            service.getAccountMaxLoginSecs())) {
                    response.setTokenId(newAccountToken);
                    conn.commit();
                } else {
                    newAccountToken = null;                                            
                    response.setTokenId(null);
                    setErrorCode(StatusCode.IAS_GENERATE_ACCOUNT_TOKEN_ERROR, null, response);                        
                }
                    
            } else {
                response.setTokenId(null);
                setErrorCode(StatusCode.IAS_AUTHENTICATE_ACCOUNT_ERROR, null, response); 
            }            
                       
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("RenewAccountToken error: ", e);
            
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.IAS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.IAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);
            log.error("RenewAccountToken error: ", e);
            
        } finally {
            try {                
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            }
            catch (SQLException e) {
                log.warn("RenewAccountToken error while closing connection: ", e);
            }
        }                
            
        return response;
    }
    
    
}

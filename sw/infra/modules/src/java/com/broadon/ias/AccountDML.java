package com.broadon.ias;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import com.broadon.wsapi.ias.AccountInfoType;
import com.broadon.wsapi.ias.AccountElementTagsType;
import com.broadon.wsapi.ias.AttributeType;

/**
 * Data manipulation library functions defined here pertain to account
 * authentication, validation, update and query
 */
public class AccountDML
{
    /** Define all SQL statements 
     */

    private static final String TABLE_IAS_ACCOUNTS = "IAS_ACCOUNTS";    
    private static final String TABLE_IAS_ACCOUNT_ATTRIBUTES = "IAS_ACCOUNT_ATTRIBUTES";
    private static final String TABLE_IAS_EXTERNAL_ACCOUNTS = "IAS_EXTERNAL_ACCOUNTS";
    private static final String TABLE_IAS_TOKENS = "IAS_AUTHENTICATION_TOKENS";    
    
    private static final String AUTHENTICATE_ACCOUNT =
        "SELECT count(*) FROM " + TABLE_IAS_ACCOUNTS + " WHERE account_id = ? AND passwd = ?";  
    
    private static final String AUTHENTICATE_LOGIN =
        "SELECT account_id FROM " + TABLE_IAS_ACCOUNTS + " WHERE login_name = ? AND passwd = ?";
    
    private static final String EXPIRE_ACCOUNT_TOKEN =
        "UPDATE " + TABLE_IAS_TOKENS + " SET is_expired = 1 WHERE token_id = ? AND account_id = ?";
    
    private static final String GET_ACCOUNT = 
        "SELECT login_name, parent_id, nick_name, full_name, " +
        "email_address, BCCUTIL.ConvertTimeStamp(birth_date) birth_date FROM " + 
        TABLE_IAS_ACCOUNTS + " WHERE account_id = ?";
    
    private static final String GET_ACCOUNT_ATTRIBUTES = 
        "SELECT attribute_id, attribute_value FROM " + TABLE_IAS_ACCOUNT_ATTRIBUTES + 
        " WHERE account_id = ? AND attribute_value IS NOT NULL";
    
    private static final String GET_ACCOUNT_TOKEN =
        "SELECT token_id FROM " + TABLE_IAS_TOKENS + " WHERE account_id = ? AND is_expired = 0 AND " +
        "(sys_extract_utc(current_timestamp) BETWEEN token_start_time AND token_expire_time)";
    
    private static final String GET_NEXT_ACCOUNT_ID =
        "SELECT account_id_seq.nextval FROM DUAL";
    
    private static final String INSERT_ACCOUNT_ATTRIBUTE =
        "INSERT INTO " + TABLE_IAS_ACCOUNT_ATTRIBUTES + " (account_id, attribute_id, attribute_value) " +
        "VALUES (?,?,?)";  
    
    private static final String INSERT_ACCOUNT_TOKEN =
        "INSERT INTO " + TABLE_IAS_TOKENS + " (token_id, account_id, token_start_time, token_expire_time, is_expired) " + 
        "VALUES (?,?,sys_extract_utc(current_timestamp),sys_extract_utc(current_timestamp)+?,0)";  
        
    private static final String INSERT_EXTERNAL_ACCOUNT =
        "INSERT INTO " + TABLE_IAS_EXTERNAL_ACCOUNTS + " (ext_account_id, ext_entity, ext_data) " +
        "VALUES (?,?,?)";
    
    private static final String REGISTER_ACCOUNT =
        "INSERT INTO " + TABLE_IAS_ACCOUNTS + " (account_id, login_name, passwd, parent_id, activate_date, " +
        "status, nick_name, full_name, email_address, birth_date) " +
        "VALUES (?,?,?,?,sys_extract_utc(current_timestamp),'A',?,?,?,BCCUTIL.ConvertTimeStamp(?))";
    
    private static final String UPDATE_ACCOUNT = 
        "SELECT login_name, parent_id, nick_name, full_name, email_address, birth_date" +
        ", SESSIONTIMEZONE tz" +
        " FROM " + TABLE_IAS_ACCOUNTS + " WHERE account_id = ? FOR UPDATE";
    
    private static final String UPDATE_ACCOUNT_ATTRIBUTE =
        "SELECT attribute_value FROM " + TABLE_IAS_ACCOUNT_ATTRIBUTES + 
        " WHERE account_id = ? AND attribute_id = ? FOR UPDATE";
    
    private static final String UPDATE_ACCOUNT_PASSWORD = 
        "UPDATE " + TABLE_IAS_ACCOUNTS + " SET passwd = ? WHERE account_id = ?";
    
    private static final String UPDATE_EXTERNAL_ACCOUNT =
        "UPDATE " + TABLE_IAS_EXTERNAL_ACCOUNTS + " SET ext_data = ? " +
        "WHERE ext_account_id = ? AND ext_entity = ?";
    
    private static final String VALIDATE_ACCOUNT = 
        "SELECT COUNT(*) FROM " + TABLE_IAS_ACCOUNTS + " WHERE account_id = ?";
    
    private static final String VALIDATE_ACCOUNT_ATTRIBUTE = 
        "SELECT COUNT(*) FROM " + TABLE_IAS_ACCOUNT_ATTRIBUTES + 
        " WHERE account_id = ? AND attribute_id = ? AND UPPER(attribute_value) = UPPER(?)";
        
    private static final String VALIDATE_ACCOUNT_LOGIN_NAME = 
        "SELECT COUNT(*) FROM " + TABLE_IAS_ACCOUNTS + " WHERE UPPER(login_name) = UPPER(?)";
    
    private static final String VALIDATE_ACCOUNT_TOKEN =
        "SELECT count(*) FROM " + TABLE_IAS_TOKENS + " WHERE account_id = ? AND token_id = ? AND is_expired = 0 AND " +
        "(sys_extract_utc(current_timestamp) BETWEEN token_start_time AND token_expire_time)";    
    
    private static final String VALIDATE_TOKEN = 
        "SELECT COUNT(*) FROM " + TABLE_IAS_TOKENS + " WHERE token_id = ?";
    
   
    public static boolean authenticateAccount(Connection conn, 
                                              int accountId, 
                                              String accountPassword)
        throws SQLException
    {
        boolean isValid = false;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            stmt = conn.prepareStatement(AUTHENTICATE_ACCOUNT);
            stmt.setInt(1, accountId);
            stmt.setString(2, accountPassword);
            
            rs = stmt.executeQuery();
            if (rs.next() && rs.getInt(1) == 1)
                isValid = true;
        
        } finally {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
    
        return isValid;
    }

    public static int authenticateLogin(Connection conn, 
                                        String loginName, 
                                        String password)
        throws SQLException
    {
        int accountId = -1;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            stmt = conn.prepareStatement(AUTHENTICATE_LOGIN);
            stmt.setString(1, loginName);
            stmt.setString(2, password);
            
            rs = stmt.executeQuery();
            if (rs.next())
                accountId = rs.getInt("ACCOUNT_ID");
            
        } finally {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
        
        return accountId;
    }
    
    public static boolean expireAccountToken(Connection conn,            
                                             int accountId, 
                                             String accountToken)            
        throws SQLException
    {
        int result = 0;
        PreparedStatement stmt = null;
                
        try {
            stmt = conn.prepareStatement(EXPIRE_ACCOUNT_TOKEN);
            stmt.setString(1, accountToken);
            stmt.setInt(2, accountId);
                    
            result = stmt.executeUpdate();
            
        } finally {
            if (stmt != null)
                stmt.close();
        }
                
        return (result == 1);
    }
    
    public static List getAccountAttributes(Connection conn, 
                                            int accountId,
                                            int[] attributeId)
        throws SQLException
    {
        List attributesList = new ArrayList();
        String sql = GET_ACCOUNT_ATTRIBUTES;
        
        if (attributeId != null && attributeId.length > 0) {
            sql += " AND (";
            
            for (int i = 0; i < attributeId.length; i++) {
                if (i != 0)
                    sql += " OR ";
                         
                sql += "attribute_id = " + attributeId[i];                
            }
            
            sql += ")";
        }            
        
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, accountId);
            
            rs = stmt.executeQuery();
            while (rs.next()) {
                AttributeType atts = new AttributeType(
                        rs.getInt("ATTRIBUTE_ID"),
                        rs.getString("ATTRIBUTE_VALUE"));
                                                
                attributesList.add(atts);
            }            
        
        } finally {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
    
        return attributesList;
    }    
    
    public static AccountInfoType getAccountInfo(Connection conn, int accountId)
        throws SQLException
    {
        AccountInfoType aInfo = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            stmt = conn.prepareStatement(GET_ACCOUNT);
            stmt.setInt(1, accountId);
                    
            rs = stmt.executeQuery();
            if (rs.next()) {
                aInfo = new AccountInfoType();
            
                aInfo.setLoginName(rs.getString("LOGIN_NAME"));               
                aInfo.setEmailAddress(rs.getString("EMAIL_ADDRESS"));
                aInfo.setFullName(rs.getString("FULL_NAME"));
                aInfo.setNickName(rs.getString("NICK_NAME"));
                
                int parentAccountId = rs.getInt("PARENT_ID");            
                if (!rs.wasNull())
                    aInfo.setParentAccountId(new Integer(parentAccountId));
                else
                    aInfo.setParentAccountId(null);
                
                long birthDate = rs.getLong("BIRTH_DATE");            
                if (!rs.wasNull())
                    aInfo.setBirthDate(new Long(birthDate));
                else
                    aInfo.setBirthDate(null);
            }
        
        } finally {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
        
        return aInfo;
    }
    
    public static String getAccountToken(Connection conn, int accountId)
        throws SQLException
    {
        String accountToken = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            stmt = conn.prepareStatement(GET_ACCOUNT_TOKEN);
            stmt.setInt(1, accountId);
        
            rs = stmt.executeQuery();
            if (rs.next())            
                accountToken = rs.getString("TOKEN_ID");            
        
        } finally {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
    
        return accountToken;
    }
    
    private static int getNextAccountIdSeq(Connection conn)
        throws SQLException
    {
        int s = 0;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            stmt = conn.prepareStatement(GET_NEXT_ACCOUNT_ID);
            
            rs = stmt.executeQuery();
            if (rs.next())
                s = rs.getInt(1);
        
        } finally {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
        
        return s;
    }
    
    public static boolean insertAccountAttribute(Connection conn, 
                                                 int accountId,
                                                 int attributeId, 
                                                 String attributeValue)
        throws SQLException
    {
        int result = 0;
        PreparedStatement stmt = null;
                
        try {
            stmt = conn.prepareStatement(INSERT_ACCOUNT_ATTRIBUTE);
            stmt.setInt(1, accountId);
            stmt.setInt(2, attributeId);
            stmt.setString(3, attributeValue);
                    
            result = stmt.executeUpdate();
        
        } finally {
            if (stmt != null)
                stmt.close();
        }
        
        return (result == 1);
    }
    
    public static boolean insertAccountToken(Connection conn,            
                                             int accountId, 
                                             String accountToken,
                                             long accountMaxLoginSecs)
        throws SQLException
    {
        int result = 0;
        PreparedStatement stmt = null;
                
        try {
            stmt = conn.prepareStatement(INSERT_ACCOUNT_TOKEN);
            stmt.setString(1, accountToken);
            stmt.setInt(2, accountId);
            stmt.setLong(3, accountMaxLoginSecs/(24*60*60));
            
            result = stmt.executeUpdate();
        
        } finally {
            if (stmt != null)
                stmt.close();
        }
        
        return (result == 1);            
    }
    
    public static boolean insertExternalAccount(Connection conn, 
                                                String accountId,
                                                String entity, 
                                                String data)
        throws SQLException
    {
        int result = 0;
        PreparedStatement stmt = null;
        
        try {
            stmt = conn.prepareStatement(INSERT_EXTERNAL_ACCOUNT);
            stmt.setString(1, accountId);
            stmt.setString(2, entity);
            stmt.setString(3, data);
            
            result = stmt.executeUpdate();
        
        } finally {
            if (stmt != null)
                stmt.close();
        }
        
        return (result == 1);
    }

    public static Integer registerAccount(Connection conn,            
                                          String loginName,
                                          String passwd,
                                          Integer parentId,
                                          String nickName,
                                          String fullName,
                                          String emailAddress,
                                          Long birthDate)
        throws SQLException
    {
        int result = 0;
        int accountId = 0;
        PreparedStatement stmt = null;
        
        try {
            stmt = conn.prepareStatement(REGISTER_ACCOUNT);
            accountId = getNextAccountIdSeq(conn);
            stmt.setInt(1, accountId);
            stmt.setString(2, loginName);
            stmt.setString(3, passwd);
            
            if (parentId != null)
                stmt.setInt(4, parentId.intValue());
            else
                stmt.setNull(4, Types.INTEGER);
            
            if (nickName != null)
                stmt.setString(5, nickName);
            else
                stmt.setNull(5, Types.VARCHAR);
            
            if (fullName != null)
                stmt.setString(6, fullName);
            else
                stmt.setNull(6, Types.VARCHAR);
            
            if (emailAddress != null)
                stmt.setString(7, emailAddress);
            else
                stmt.setNull(7, Types.VARCHAR);
            
            if (birthDate != null) {
                Calendar bCal = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
                Date birthDay = new Date(birthDate.longValue());
                bCal.set(Integer.parseInt(birthDay.toString().substring(0,4)),
                         Integer.parseInt(birthDay.toString().substring(5,7))-1,
                         Integer.parseInt(birthDay.toString().substring(8,10)),12,0,0);
                bCal.set(bCal.MILLISECOND,0);
                long birthTime = bCal.getTimeInMillis();
                stmt.setLong(8, birthTime);
            }
            else
                stmt.setNull(8, Types.BIGINT);
            
            result = stmt.executeUpdate();
        
        } finally {
            if (stmt != null)
                stmt.close();
        }
        
        return (result == 1) ? new Integer(accountId) : null;
    }
    
    public static boolean setAccountAttribute(Connection conn, 
                                              int accountId,
                                              int attributeId, 
                                              String attributeValue)
        throws SQLException
    {
        if (updateAccountAttribute(conn, accountId, attributeId, attributeValue))
            return true;
        else {
            if (attributeValue != null)
                return (insertAccountAttribute(conn, accountId, attributeId, attributeValue));
            else
                return true;
        }
    }
    
    public static boolean setExternalAccount(Connection conn, 
                                             String accountId,
                                             String entity, 
                                             String data)
        throws SQLException
    {
        if (updateExternalAccount(conn, accountId, entity, data))
            return true;
        else
            return (insertExternalAccount(conn, accountId, entity, data));
    }
    
    public static boolean updateAccount(Connection conn,
                                        int accountId,
                                        String loginName,
                                        Integer parentId,
                                        String nickName,
                                        String fullName,
                                        String emailAddress,
                                        Long birthDate,
                                        AccountElementTagsType unsetElements)
        throws SQLException
    {
        boolean result = true;        
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            stmt = conn.prepareStatement(UPDATE_ACCOUNT,
                                         ResultSet.TYPE_SCROLL_INSENSITIVE,
                                         ResultSet.CONCUR_UPDATABLE);
            stmt.setInt(1, accountId);
            
            rs = stmt.executeQuery();
            if (rs.next()) {
                boolean flag = false;
                boolean isNull = false;
                               
                String dbLoginName = rs.getString("LOGIN_NAME");
                isNull = rs.wasNull();
                if (unsetElements != null && 
                        unsetElements.getLoginName() != null &&
                        unsetElements.getLoginName().booleanValue()) {
                    if (!isNull) {
                        rs.updateNull("LOGIN_NAME");
                        flag = true;
                    }
                } else if (loginName != null) {
                    if (isNull || !(dbLoginName.toLowerCase().equals(loginName.toLowerCase()))) {
                        rs.updateString("LOGIN_NAME", loginName.toLowerCase());
                        flag = true;
                    }
                }
                
                int dbParentId = rs.getInt("PARENT_ID");
                isNull = rs.wasNull();
                if (unsetElements != null &&
                        unsetElements.getParentAccountId() != null &&
                        unsetElements.getParentAccountId().booleanValue()) {
                    if (!isNull) {
                        rs.updateNull("PARENT_ID");
                        flag = true;
                    }
                } else if (parentId != null) {
                    if (isNull || dbParentId != parentId.intValue()) {
                        rs.updateInt("PARENT_ID", parentId.intValue());
                        flag = true;
                    }
                }      
                
                String dbNickName = rs.getString("NICK_NAME");
                isNull = rs.wasNull();
                if (unsetElements != null &&
                        unsetElements.getNickName() != null &&
                        unsetElements.getNickName().booleanValue()) {
                    if (!isNull) {
                        rs.updateNull("NICK_NAME");
                        flag = true;
                    }
                } else if (nickName != null) {
                    if (isNull || !(dbNickName.equals(nickName))) {
                        rs.updateString("NICK_NAME", nickName);
                        flag = true;
                    }
                }
                                
                String dbFullName = rs.getString("FULL_NAME");
                isNull = rs.wasNull();
                if (unsetElements != null &&
                        unsetElements.getFullName() != null &&
                        unsetElements.getFullName().booleanValue()) {
                    if (!isNull) {
                        rs.updateNull("FULL_NAME");
                        flag = true;
                    }
                } else if (fullName != null) {
                    if (isNull || !(dbFullName.equals(fullName))) {
                        rs.updateString("FULL_NAME", fullName);
                        flag = true;
                    }
                }
                                
                String dbEmailAddress = rs.getString("EMAIL_ADDRESS");
                isNull = rs.wasNull();
                if (unsetElements != null &&
                        unsetElements.getEmailAddress() != null &&
                        unsetElements.getEmailAddress().booleanValue()) {
                    if (!isNull) {
                        rs.updateNull("EMAIL_ADDRESS");
                        flag = true;
                    }
                } else if (emailAddress != null) {
                    if (isNull || !(dbEmailAddress.equals(emailAddress))) {
                        rs.updateString("EMAIL_ADDRESS", emailAddress);
                        flag = true;
                    }
                }
                
                Timestamp dbBirthDate = rs.getTimestamp("BIRTH_DATE");
                String dbTimeZone = "GMT" + rs.getString("TZ");
                isNull = rs.wasNull();
                if (unsetElements != null &&
                        unsetElements.getBirthDate() != null &&
                        unsetElements.getBirthDate().booleanValue()) {
                    if (!isNull) {
                        rs.updateNull("BIRTH_DATE");                        
                        flag = true;
                    }
                } else if (birthDate != null) {
                    Date birthDay = new Date(birthDate.longValue());
                    Calendar bCal = new GregorianCalendar(TimeZone.getTimeZone(dbTimeZone));
                    bCal.set(Integer.parseInt(birthDay.toString().substring(0,4)),
                             Integer.parseInt(birthDay.toString().substring(5,7))-1,
                             Integer.parseInt(birthDay.toString().substring(8,10)),12,0,0);
                    bCal.set(bCal.MILLISECOND,0);
                    long birthTime = bCal.getTimeInMillis();
                    if (isNull || birthTime != dbBirthDate.getTime()) {
                        rs.updateTimestamp("BIRTH_DATE", new Timestamp(birthTime));
                        flag = true;
                    }
                }
                
                if (flag) rs.updateRow();           
                    
            } else
                result = false;        
        
        } finally {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
        
        return result;        
    }
    
    public static boolean updateAccountAttribute(Connection conn, 
                                                 int accountId,
                                                 int attributeId, 
                                                 String attributeValue)
        throws SQLException
    {
        boolean result = true;        
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            boolean update = false;
            stmt = conn.prepareStatement(UPDATE_ACCOUNT_ATTRIBUTE,
                                         ResultSet.TYPE_SCROLL_INSENSITIVE,
                                         ResultSet.CONCUR_UPDATABLE);
            stmt.setInt(1, accountId);
            stmt.setInt(2, attributeId);
            
            rs = stmt.executeQuery();
            if (rs.next()) {            
                String dbValue = rs.getString("ATTRIBUTE_VALUE");
                boolean isNull = rs.wasNull();
                if (!((isNull && attributeValue == null) ||
                      (!isNull && attributeValue != null && 
                       dbValue.equals(attributeValue)))) {
                    if (attributeValue != null)
                        rs.updateString("ATTRIBUTE_VALUE", attributeValue);
                    else
                        rs.updateNull("ATTRIBUTE_VALUE");
                    
                    update = true;
                }
                    
                if (update) rs.updateRow();
                    
            } else
                result = false;        
        
        } finally {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
        
        return result;
    }
    
    public static boolean updateAccountPassword(Connection conn, 
                                                int accountId,
                                                String passwd)
        throws SQLException
    {
        int result = 0;
        PreparedStatement stmt = null;
        
        try {
            stmt = conn.prepareStatement(UPDATE_ACCOUNT_PASSWORD);
            stmt.setString(1, passwd);
            stmt.setInt(2, accountId);
            
            result = stmt.executeUpdate();
        
        } finally {
            if (stmt != null)
                stmt.close();
        }
        
        return (result == 1);
    }
    
    public static boolean updateExternalAccount(Connection conn, 
                                                String accountId,
                                                String entity, 
                                                String data)
        throws SQLException
    {
        int result = 0;
        PreparedStatement stmt = null;
        
        try {
            stmt = conn.prepareStatement(UPDATE_EXTERNAL_ACCOUNT);
            stmt.setString(1, data);
            stmt.setString(2, accountId);
            stmt.setString(3, entity);
            
            result = stmt.executeUpdate();
        
        } finally {
            if (stmt != null)
                stmt.close();
        }
        
        return (result == 1);
    }

    public static boolean validateAccountAttribute(Connection conn, 
                                                   int accountId, 
                                                   int attributeId,
                                                   String attributeValue)
        throws SQLException
    {
        boolean isValid = false;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            stmt = conn.prepareStatement(VALIDATE_ACCOUNT_ATTRIBUTE);
            stmt.setInt(1, accountId);
            stmt.setInt(2, attributeId);
            stmt.setString(3, attributeValue);
            
            rs = stmt.executeQuery();
            if (rs.next() && rs.getInt(1) == 1)
                isValid = true;            
        
        } finally {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
        
        return isValid;
    }
    
    public static boolean validateAccountId(Connection conn, 
                                            int accountId)
        throws SQLException
    {
        boolean isValid = false;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            stmt = conn.prepareStatement(VALIDATE_ACCOUNT);
            stmt.setInt(1, accountId);
                    
            rs = stmt.executeQuery();
            if (rs.next() && rs.getInt(1) == 1)
                isValid = true;        
        
        } finally {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
        
        return isValid;
    }
    
    public static boolean validateAccountToken(Connection conn,
                                               int accountId,
                                               String accountToken)
        throws SQLException
    {
        boolean isValid = false;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            stmt = conn.prepareStatement(VALIDATE_ACCOUNT_TOKEN);
            stmt.setInt(1, accountId);
            stmt.setString(2, accountToken);
            
            rs = stmt.executeQuery();
            if (rs.next() && rs.getInt(1) == 1)
                isValid = true;        
        
        } finally {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
        
        return isValid;
    }
    
    public static boolean validateLoginName(Connection conn, 
                                            String loginName)
        throws SQLException
    {
        boolean isValid = false;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            stmt = conn.prepareStatement(VALIDATE_ACCOUNT_LOGIN_NAME);
            stmt.setString(1, loginName);
            
            rs = stmt.executeQuery();
            if (rs.next() && rs.getInt(1) == 1)
                isValid = true;        
        
        } finally {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
        
        return isValid;
    }
    
    public static boolean validateLoginName(Connection conn, 
                                            String loginName,
                                            int accountId)
        throws SQLException
    {
        boolean isValid = false;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            String sql = VALIDATE_ACCOUNT_LOGIN_NAME + " AND account_id != " + accountId;
            
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, loginName);
            
            rs = stmt.executeQuery();
            if (rs.next() && rs.getInt(1) == 1)
                isValid = true;        
        
        } finally {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
        
        return isValid;
    }
    
    public static boolean validateToken(Connection conn, 
                                        String tokenId)
        throws SQLException
    {
        boolean isValid = false;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            stmt = conn.prepareStatement(VALIDATE_TOKEN);
            stmt.setString(1, tokenId);
            
            rs = stmt.executeQuery();
            if (rs.next() && rs.getInt(1) == 1)
                isValid = true;        
        
        } finally {
            if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
        }
        
        return isValid;
    }

} // AccountDML

package com.broadon.ias;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ias.*;

public class RemoveDeviceAccount extends IdentityAuthenticationFunction {
    
    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(RemoveDeviceAccount.class.getName());

    /**
     * Removes accountId linked with the deviceId
     */
    public static RemoveDeviceAccountResponseType removeDeviceAccount(
            IdentityAuthenticationServiceImpl service,
            RemoveDeviceAccountRequestType removeDeviceAccountRequest) 
        throws java.rmi.RemoteException 
    {
        long deviceId = removeDeviceAccountRequest.getDeviceId();
        int accountId = removeDeviceAccountRequest.getAccountId();
                
        RemoveDeviceAccountResponseType response = new RemoveDeviceAccountResponseType();
        initResponse(removeDeviceAccountRequest, response);
        response.setDeviceId(deviceId);
       
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
        
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            
            // set the remove date to current db time by setting the remove flag to true
            if (DeviceDML.updateDeviceAccount(conn, deviceId, accountId, true))                   
                conn.commit();
            else
                setErrorCode(StatusCode.IAS_UPDATE_DEVICE_ACCOUNT_ERROR, null, response);
            
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("RemoveDeviceAccount error: ", e);
            
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.IAS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.IAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);
            log.error("RemoveDeviceAccount error: ", e);
            
        } finally {
            try {                
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            }
            catch (SQLException e) {
                log.warn("RemoveDeviceAccount error while closing connection: ", e);
            }
        }                
            
        return response;
    }   
}

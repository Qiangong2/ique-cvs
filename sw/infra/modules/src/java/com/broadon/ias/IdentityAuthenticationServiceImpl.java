package com.broadon.ias;

import java.util.Properties;

import javax.servlet.ServletContext;
import javax.sql.DataSource;
import javax.xml.rpc.ServiceException;
import javax.xml.rpc.server.ServiceLifecycle;
import javax.xml.rpc.server.ServletEndpointContext;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.servlet.ServletConstants;
import com.broadon.trans.common.AuditLog;
import com.broadon.trans.common.Logger;

/**
 * IdentityAuthenticationServiceImpl provides the implementation for
 * the Identity Authentication Services.
 * @author vaibhav
 */
public class IdentityAuthenticationServiceImpl implements ServiceLifecycle, ServletConstants
{   
    // The servlet context
    protected ServletContext servletContext;
    
    // The data source used to get database connection
    protected DataSource dataSource;    
    
    // The max session time for which a device token is valid
    protected String deviceMaxLoginSecs;
    
    // The max session time for which an account token is valid
    protected String accountMaxLoginSecs;
    
    // The audit log handler
    protected AuditLog auditLog;

    // The logging module
    protected static Log log = 
        LogFactory.getLog(IdentityAuthenticationServiceImpl.class.getName());
    
    private static final String DEF_DEVICE_MAX_LOGIN_SECS = "86400";
    private static final String CFG_DEVICE_MAX_LOGIN_SECS = "DEVICE_MAX_LOGIN_SECS";
    
    private static final String DEF_ACCOUNT_MAX_LOGIN_SECS = "86400";
    private static final String CFG_ACCOUNT_MAX_LOGIN_SECS = "ACCOUNT_MAX_LOGIN_SECS";

    public IdentityAuthenticationServiceImpl()
    {
        // Force initialization of StatusCode class
        try {
                Class.forName("com.broadon.ias.StatusCode");
        } catch  (ClassNotFoundException e) {
                log.error("Cannot initialize IAS StatusCode");
        }
    }
    
   /**
     * Called by the JAX RPC runtime to initialize
     * the service endpoint.  Implements ServiceLifecycle.
     */
    public void init(Object context) throws ServiceException  
    { 
        log.debug("Identity Authentication Service Init");
        
        ServletEndpointContext soapContext = (ServletEndpointContext) context; 
        servletContext = soapContext.getServletContext();
        
        dataSource = (DataSource)servletContext.getAttribute(DATA_SOURCE_KEY);
        
        auditLog = (AuditLog) servletContext.getAttribute(Logger.LOGGER_KEY);
        
        // Obtain the properties set for IAS server in the file 
        // defined in conf/BBserver.properties.tmpl
        //
        Properties prop = (Properties) servletContext.getAttribute(PROPERTY_KEY);
        
        // Get device token timeout
        deviceMaxLoginSecs = getConfig(prop, CFG_DEVICE_MAX_LOGIN_SECS,
                                       DEF_DEVICE_MAX_LOGIN_SECS);
        // Get account token timeout
        accountMaxLoginSecs = getConfig(prop, CFG_ACCOUNT_MAX_LOGIN_SECS,
                                       DEF_ACCOUNT_MAX_LOGIN_SECS);       
    } 

    /**
     * Called by the JAX RPC runtime to destroy resources
     * used by the service endpoint at the end of its lifecycle.
     * Implements ServiceLifecycle.
     */
    public void destroy() 
    {
        log.debug("Identity Authentication Service Destroy");
    }
    
    // Private functions
    
    private String getConfig(Properties prop, String key, String defValue)
    {
        String value = (prop != null)? prop.getProperty(key): null;
        if (value == null || value.equals("")) {
            value = defValue;
            log.warn("BBserver.properties missing " + key + " value" +
                     "... using default of " + defValue);
        }
        return value;
    }

    // Member access functions
    public DataSource getDataSource()
    {
        return dataSource;
    }
    
    public long getAccountMaxLoginSecs()
    {
        return Long.parseLong(accountMaxLoginSecs);
    }
    
    public long getDeviceMaxLoginSecs()
    {
        return Long.parseLong(deviceMaxLoginSecs);
    }
    
    public AuditLog getAuditLog()
    {
        return auditLog;
    }
}


package com.broadon.ias;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ias.*;

public class UpdateAccountAttributes extends IdentityAuthenticationFunction {
    
    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(UpdateAccountAttributes.class.getName());

    /**
     * Updates account attributes for a given account id
     */
    public static UpdateAccountAttributesResponseType updateAccountAttributes(
            IdentityAuthenticationServiceImpl service,
            UpdateAccountAttributesRequestType updateAccountAttributesRequest) 
        throws java.rmi.RemoteException 
    {
        int accountId = updateAccountAttributesRequest.getAccountId();
        AttributeType accountAttributes[] = updateAccountAttributesRequest.getAccountAttributes();
        int unsetAttributes[] = updateAccountAttributesRequest.getUnsetAccountAttributes();
        
        UpdateAccountAttributesResponseType response = new UpdateAccountAttributesResponseType();
        initResponse(updateAccountAttributesRequest, response);
        response.setAccountId(accountId);
       
        if ((accountAttributes == null || accountAttributes.length == 0) &&
                (unsetAttributes == null || unsetAttributes.length == 0)) {
            setErrorCode(StatusCode.IAS_BAD_REQUEST, null, response);
            log.debug("UpdateAccountAttributes bad request error: " +
                    "accountId=" + accountId + 
                    ", accountAttributes=" + (accountAttributes == null ? "null" : String.valueOf(accountAttributes.length)) +
                    ", unsetAttributes=" + (unsetAttributes == null ? "null" : String.valueOf(unsetAttributes.length)));
            
            return response;
        }
        
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
        
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            
            // workaround for the bug in oci driver that does not allow to open result set for update
            // without an initial commit
            conn.commit();
            
            boolean success = true;
                    
            if (accountAttributes != null && accountAttributes.length > 0) {
                for (int i = 0; i < accountAttributes.length; i++) {
                    int attributeId = accountAttributes[i].getAttributeId();
                    String attributeValue = accountAttributes[i].getAttributeValue();
    
                    if (!AccountDML.setAccountAttribute(conn, accountId, attributeId, attributeValue)) {
                        success = false;
                        break;
                    }
                }
            }
            
            if (success) {
                if (unsetAttributes != null && unsetAttributes.length > 0) {            
                    for (int i = 0; i < unsetAttributes.length; i++) {
                        int attributeId = unsetAttributes[i];
                            
                        if (!AccountDML.setAccountAttribute(conn, accountId, attributeId, null)) {
                            success = false;
                            break;
                        }
                    }
                }
            }
                    
            if (success)
                conn.commit();
            else
                setErrorCode(StatusCode.IAS_UPDATE_ACCOUNT_ATTRIBUTE_ERROR, null, response);      
                                   
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("UpdateAccountAttributes error: ", e);
            
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.IAS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.IAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);
            log.error("UpdateAccountAttributes error: ", e);
            
        } finally {
            try {                
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            }
            catch (SQLException e) {
                log.warn("UpdateAccountAttributes error while closing connection: ", e);
            }
        }                
            
        return response;
    }   
}

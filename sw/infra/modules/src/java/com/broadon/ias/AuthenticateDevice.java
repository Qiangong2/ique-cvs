package com.broadon.ias;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ias.*;

public class AuthenticateDevice extends IdentityAuthenticationFunction {
    
    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(AuthenticateDevice.class.getName());

    /**
     * Authenticates device and returns encrypted token back to the device
     */
    public static AuthenticateDeviceResponseType authenticateDevice(
            IdentityAuthenticationServiceImpl service,
            AuthenticateDeviceRequestType authenticateDeviceRequest) 
        throws java.rmi.RemoteException 
    {
        long deviceId = authenticateDeviceRequest.getDeviceId();
        
        AuthenticateDeviceResponseType response = new AuthenticateDeviceResponseType();
        initResponse(authenticateDeviceRequest, response);
        response.setDeviceId(deviceId);
       
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
        
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            
            // validate this deviceId
            if (DeviceDML.validateDevice(conn, deviceId)) {
                
                // look if a token is already active for this deviceId
                String deviceToken = DeviceDML.getDeviceToken(conn, deviceId);
                            
                if (deviceToken == null || deviceToken.equals("")) {
                    deviceToken = generateToken(conn);
                        
                    if (DeviceDML.insertDeviceToken(conn, deviceId, 
                            deviceToken, service.getDeviceMaxLoginSecs()))
                        conn.commit();
                    else {
                        deviceToken = null;                                                         
                        setErrorCode(StatusCode.IAS_GENERATE_DEVICE_TOKEN_ERROR, null, response);
                    } 
                }
                    
                response.setTokenId(deviceToken);
                
            } else {
                response.setTokenId(null);
                setErrorCode(StatusCode.IAS_BAD_DEVICE_ID, null, response);                
            }            
                       
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("AuthenticateDevice error: ", e);
            
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.IAS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.IAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);
            log.error("AuthenticateDevice error: ", e);
            
        } finally {
            try {                
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            }
            catch (SQLException e) {
                log.warn("AuthenticateDevice error while closing connection: ", e);
            }
        }                
            
        return response;
    }
}

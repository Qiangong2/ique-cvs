package com.broadon.ias;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ias.*;

public class UpdateAccount extends IdentityAuthenticationFunction {
    
    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(UpdateAccount.class.getName());

    /**
     * Updates account record for a given account id
     */
    public static UpdateAccountResponseType updateAccount(
            IdentityAuthenticationServiceImpl service,
            UpdateAccountRequestType updateAccountRequest) 
        throws java.rmi.RemoteException 
    {
        int accountId = updateAccountRequest.getAccountId();        
        AccountInfoType aInfo = updateAccountRequest.getAccountInfo();
        AccountElementTagsType unsetElements = updateAccountRequest.getUnsetElements();
        
        String loginName = null;
        Integer parentId = null;
        String nickName = null;
        String fullName = null;
        String emailAddress = null;
        Long birth_date = null;

        if (aInfo != null) {
            loginName = aInfo.getLoginName();
            parentId = aInfo.getParentAccountId();
            nickName = aInfo.getNickName();
            fullName = aInfo.getFullName();
            emailAddress = aInfo.getEmailAddress();
            birth_date = aInfo.getBirthDate();
        }
        
        UpdateAccountResponseType response = new UpdateAccountResponseType();
        initResponse(updateAccountRequest, response);
        response.setAccountId(accountId);
        
        if ((aInfo == null && unsetElements == null) ||
                (loginName != null && loginName.equals(""))) {
            setErrorCode(StatusCode.IAS_BAD_REQUEST, null, response);
            log.debug("UpdateAccountAttributes bad request error");
            
            return response;
        }
        
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
        
        try {
            boolean cont = true;
            
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            
            // workaround for the bug in oci driver that does not allow to open result set for update
            // without an initial commit
            conn.commit();
                      
            // check if login name already exists
            if (loginName != null && AccountDML.validateLoginName(conn, loginName, accountId)) {
            	setErrorCode(StatusCode.IAS_ACCOUNT_LOGIN_NAME_ALREADY_EXISTS_ERROR, null, response);
            	cont = false;
            }
   
            if (cont) {
            	if (parentId != null && !AccountDML.validateAccountId(conn, parentId.intValue())) {
            	    setErrorCode(StatusCode.IAS_BAD_ACCOUNT_ID, null, response);
            	    cont = false;                 
            	}
            }
            	
            if (cont) {
            	// update the account with the request data
            	if (AccountDML.updateAccount(conn, accountId, loginName, parentId, 
            	                    nickName, fullName, emailAddress, birth_date, unsetElements))
            	    conn.commit();
            	else
            	    setErrorCode(StatusCode.IAS_UPDATE_ACCOUNT_INFO_ERROR, null, response);
            }                                      
                        
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("UpdateAccount error: ", e);
            
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.IAS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.IAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);
            log.error("UpdateAccount error: ", e);
            
        } finally {
            try {                
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            }
            catch (SQLException e) {
                log.warn("UpdateAccount error while closing connection: ", e);
            }
        }                
            
        return response;
    }   
}

package com.broadon.ias;

import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;

import com.broadon.util.UUID;
import com.broadon.wsapi.ias.AbstractRequestType;
import com.broadon.wsapi.ias.AbstractResponseType;


public class IdentityAuthenticationFunction {
 
    private static final int TOKEN_LENGTH = 22;
        
    // Static helper functions
    
    /**
     * Returns the current time in milliseconds
     * @return long
     */
    protected static long getTimestamp()
    {
        return System.currentTimeMillis();
    }

    /**
     * Initializes fields in a AbstractResponseType given an AbstractRequestType
     * @param AbstractRequestType request
     * @param AbstractResponseType response
     */
    protected static void initResponse(AbstractRequestType request,
                                       AbstractResponseType response)
    {
        response.setTimeStamp(getTimestamp());
        response.setVersion(request.getVersion());
        setErrorCode(StatusCode.SC_OK, "", response);       
    }
    
    /**
     * Sets the error code in AbstractResponseType
     * @param String code
     * @param String message
     * @param AbstractResponseType response
     */
    protected static void setErrorCode(String code, 
                                       String message,
                                       AbstractResponseType response)
    {
        response.setErrorCode(Integer.parseInt(code));
        if (message != null)
            response.setErrorMessage(message);
        else
            response.setErrorMessage(StatusCode.getMessage(code));
    }
    
    /**
     * Generate a new token
     * @param Connection conn
     * 
     * @return String
     */
    protected static String generateToken (Connection conn)
        throws SQLException, NoSuchAlgorithmException
    {
        String tokenId = null;
        boolean isValidToken = true;
        
        while (isValidToken) {
            tokenId = UUID.randomUUID().toBase64().substring(0, TOKEN_LENGTH-1);
            isValidToken = AccountDML.validateToken(conn, tokenId);         
        }
        
        return tokenId;
    }    
}

package com.broadon.ias;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import javax.xml.rpc.server.ServletEndpointContext;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPException;

import org.apache.axis.AxisFault;
import org.apache.axis.Constants;
import org.apache.axis.MessageContext;
import org.apache.axis.handlers.BasicHandler;
import org.apache.axis.transport.http.HTTPConstants;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.broadon.filter.B2BAuthenticator;
import com.broadon.servlet.ServletConstants;

public class AccessControlHandler extends BasicHandler implements ServletConstants
{
    static final String ACCESS_CONTROL_FILE_KEY = "AccessControlFile";
    
    static final String TOKEN_TAG_NAME = "TokenId";
    static final String ACCOUNT_ID_TAG_NAME = "AccountId";
    static final String DEVICE_ID_TAG_NAME = "DeviceId";
    
    // Only "actions" that do *NOT* require a token are placed here. 
    // In other words, any actions that are not here must have a device or account token.
    static HashMap accessMap = null;
    
    static DataSource dataSource = null;
    static boolean initialized = false;
    static Object sync = new Object();

    /**
     * Intialize the access control handler with SOAP actions that are accepted
     * <strong>without</strong> the need of an authorization token.
     * <p>
     * This method should be called only once upon initialization of the web
     * service. The collection of action strings should be obtained from the
     * context parameters, which should be configured in the corresponding
     * web.xml file.
     * 
     * @param actions
     *            A collection of action strings
     * @throws IOException 
     */
    public void init(ServletContext servletContext) throws IOException
    {
        dataSource = (DataSource)servletContext.getAttribute(DATA_SOURCE_KEY);
    
        /*
         * Load access configuration properties.
         */
        
        String configFile = (String) servletContext.getInitParameter(ACCESS_CONTROL_FILE_KEY);
                
        /*
         * Get the access control configuration file name.
         */
        String fullPath = System.getProperty(SERVLET_BASE) + File.separator + configFile;
        File file = new File(fullPath);
        if (!file.isFile()) {
            // File not found, try under the servlet context path
            fullPath = servletContext.getRealPath(configFile);
        }
        
        /*
         * Read the Access Control configuration file.
         * 
         * Reading the file here allows the servlet to always have the
         * latest version after reloading.
         */
        Properties properties = new Properties();
        properties.load(new FileInputStream(fullPath));
        
        /*
         * Process the mapping informaton.
         */
        if (! properties.isEmpty()) {
            accessMap = new HashMap(properties.size() * 3 / 2);
            for (Enumeration e = properties.propertyNames(); e.hasMoreElements();) {    
                String name = (String)e.nextElement();
                String value = (String)properties.get(name);
                
                accessMap.put(name, new AccessControlType(value));
            }                       
        } else
            throw new IOException("Empty configuration file: " + fullPath);
    }


    public AccessControlHandler() {
        super();
    }

    
    public void invoke(MessageContext ctx) throws AxisFault
    {
        if (!initialized) {
            // use unsynchronized check first because in most cases it should
            // have been initialized. The second test inside the synchronized
            // block is to avoid duplicated initialization.
            synchronized (sync) {
                if (! initialized) {
                    ServletEndpointContext soapContext = (ServletEndpointContext) 
                        ctx.getProperty(Constants.MC_SERVLET_ENDPOINT_CONTEXT); 
                    ServletContext servletContext = soapContext.getServletContext();
                    try {
                        init(servletContext);
                        initialized = true;
                    } catch (IOException e) {
                        AxisFault fault = AxisFault.makeFault(e);
                        fault.setFaultString("Failed to initialize access control configuration");
                        throw fault;
                    }
                }
            }
        }       
        
        AxisFault fault = null;
        String uri = ctx.getSOAPActionURI();
        String action = uri.substring(uri.lastIndexOf("/") + 1);
        AccessControlType act = (AccessControlType) accessMap.get(action);

        if (act != null) {
            String accessType = checkInternalRequest(ctx) ? act.internalAccess : act.externalAccess;
            if ("false".equals(accessType))
                return;
            
            try {
                if (validateToken(ctx, act.tokenType))
                    return;
            } catch (SOAPException e) {
                fault = AxisFault.makeFault(e);
            } catch (SQLException e) {
                fault = AxisFault.makeFault(e);
            }
        }
        
        if (fault == null)
            fault = new AxisFault();
        
        fault.setFaultString("Unauthorized Access Denied");            
        throw fault;
    } 
    

    // return true if the request originates from the local area network.
    private boolean checkInternalRequest(MessageContext ctx) 
    { 
        HttpServletRequest req = (HttpServletRequest) 
            ctx.getProperty(HTTPConstants.MC_HTTP_SERVLETREQUEST);
        return B2BAuthenticator.LOCAL_CLIENT.equals(req.getAttribute(CLIENT_CREDENTIAL));
    }
    
    private boolean validateToken(MessageContext ctx, String tokenType)
        throws SQLException, SOAPException
    {
        if ("none".equals(tokenType))
            return true;
        if (tokenType == null || tokenType.equals(""))
            return false;   
        
        Connection conn = null;
        boolean isValid = false;
        
        try {
            String token = getTagValue(ctx, TOKEN_TAG_NAME);
            
            if (token != null && !token.equals("")) {
                String id = null;
                
                if (tokenType != null && tokenType.equals("device")) {
                    id = getTagValue(ctx, DEVICE_ID_TAG_NAME);
                    
                    if (id != null && !id.equals("")) {
                        conn = dataSource.getConnection();
                        conn.setAutoCommit(false);
                        
                        isValid = DeviceDML.validateDeviceToken(conn, Long.parseLong(id), token);
                    } else
                        isValid = false;
                
                } else if (tokenType != null && tokenType.equals("account")) {
                    id = getTagValue(ctx, ACCOUNT_ID_TAG_NAME);
                
                    if (id != null && !id.equals("")) {
                        conn = dataSource.getConnection();
                        conn.setAutoCommit(false);
                        
                        isValid = AccountDML.validateAccountToken(conn, Integer.parseInt(id), token);
                    } else
                        isValid = false;            
                }
            }
            
        } finally {
            if (conn != null)
                conn.close();
        }
        
        return isValid;
    }
    
    private String getTagValue(MessageContext ctx, String tagName)
        throws SOAPException
    {
        String tagValue = null;
        
        SOAPBody body = ctx.getRequestMessage().getSOAPBody();
        NodeList tag = body.getElementsByTagName(tagName);
        if (tag != null && tag.getLength() == 1) {
            Node node = tag.item(0);
            node = node.getFirstChild();
            if (node != null && node.getNodeType() == Node.TEXT_NODE) {
                tagValue = node.getNodeValue();
            }
        } 
        
        return tagValue;
    }
    

    class AccessControlType {
        String internalAccess = null;
        String externalAccess = null;
        String tokenType = null;
        
        /**
         * Creates AccessControltype object from the configuration value
         *
         * @param   value  the access control value for the specified action.
         */
        AccessControlType(String value) {
            StringTokenizer t = new StringTokenizer(value, ",");
            for (int index = 1; t.hasMoreTokens(); index++) {
                switch (index) {
                    case 1:  internalAccess = t.nextToken(); break;
                    case 2:  externalAccess = t.nextToken(); break;
                    case 3:  tokenType = t.nextToken(); break;                
                }
            }   
        }
    }
}



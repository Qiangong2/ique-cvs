package com.broadon.ias;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.axis.components.logger.LogFactory;

import com.broadon.wsapi.ias.*;

public class AuthenticateAccount extends IdentityAuthenticationFunction {
    
    // Uses apache commons logger (follows axis logging)
    private static Log log = 
        LogFactory.getLog(AuthenticateAccount.class.getName());

    /**
     * Authenticates account and returns a token
     */
    public static AuthenticateAccountResponseType authenticateAccount(
            IdentityAuthenticationServiceImpl service,
            AuthenticateAccountRequestType authenticateAccountRequest) 
        throws java.rmi.RemoteException 
    {
        String loginName = authenticateAccountRequest.getLoginName();
        String password = authenticateAccountRequest.getPassword();
               
        AuthenticateAccountResponseType response = new AuthenticateAccountResponseType();
        initResponse(authenticateAccountRequest, response);        
               
        if (loginName == null || loginName.equals("") ||
                password == null || password.equals("")) {
            response.setTokenId(null);
            response.setAccountId(null);
            setErrorCode(StatusCode.IAS_BAD_REQUEST, null, response);
            log.debug("AuthenticateAccount bad request error: " +
                    "loginName=" + loginName + ", password=" + 
                    (password == null ? "null" : "****"));
            
            return response;
        }
        
        DataSource dataSource = service.getDataSource();
        Connection conn = null;
        
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            
            int accountId = AccountDML.authenticateLogin(conn, loginName, password);
            
            // verify the password
            if (accountId != -1) {
                // look if a token is already active for this accountId
                String accountToken = AccountDML.getAccountToken(conn, accountId);
                            
                if (accountToken == null || accountToken.equals("")) {
                    accountToken = generateToken(conn);
                    
                    if (AccountDML.insertAccountToken(conn, accountId, 
                            accountToken, service.getAccountMaxLoginSecs()))
                        conn.commit();
                    else {
                        accountToken = null;                                                         
                        setErrorCode(StatusCode.IAS_GENERATE_ACCOUNT_TOKEN_ERROR, null, response);
                    } 
                }
                
                response.setAccountId(new Integer(accountId));
                response.setTokenId(accountToken);
                
            } else {
                response.setAccountId(null);
                response.setTokenId(null);
                setErrorCode(StatusCode.IAS_AUTHENTICATE_ACCOUNT_ERROR, null, response);                
            }            
                       
        } catch (SQLException e) {               
            setErrorCode(StatusCode.SC_SQL_EXCEPTION,
                    e.getLocalizedMessage(),
                    response);
            log.error("AuthenticateAccount error: ", e);
            
        } catch (Throwable e) {
            // Unexpected error, catch and log
            setErrorCode(StatusCode.IAS_INTERNAL_ERROR,
                    StatusCode.getMessage(StatusCode.IAS_INTERNAL_ERROR) + ": " + e.getLocalizedMessage(),
                    response);
            log.error("AuthenticateAccount error: ", e);
            
        } finally {
            try {                
                if (conn != null) {
                    conn.rollback();
                    conn.close();
                }
            }
            catch (SQLException e) {
                log.warn("AuthenticateAccount error while closing connection: ", e);
            }
        }                
            
        return response;
    }
}

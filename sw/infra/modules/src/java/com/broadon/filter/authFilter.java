package com.broadon.filter;

import java.io.IOException;
import java.security.cert.X509Certificate;
import java.sql.SQLException;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import com.broadon.servlet.ServletConstants;

/**
 * A common filter for incoming connection requests from a Depot. It compares
 * the public key of the incoming certificate with that on file, and rejects the
 * connection if they don't match.
 * 
 * @version $Revision: 1.27 $
 * @author W. Wilson Ho
 */
public class authFilter implements Filter, ServletConstants
{
    DataSource ds = null;
    ClientAuthenticator clientAuth = null;      // class that authenticate a client

    private Pattern httpsUriRegex = null;
    private Pattern httpUriRegex = null;
    /**
     * list of acceptable URIs A ThreadLocal of UrlACL. The regular expression
     * matcher is not thread-safe, that's why we need to use ThreadLocal.
     */
    private ThreadLocal allowedURIs = null; 

    static final String CLIENT_AUTH_CLASS = "client_auth_class";
    
    // one of the following access code
    static public final int INSECURE = 0;       // HTTP connection
    static public final int NOCLIENT_AUTH = 1;  // HTTPS with no client authentication
    static public final int SECURE = 2;         // HTTPS with client auth.

    static final String HTTP_URI = "http_uri";  // list of URI's that allows HTTP access

    // list of URI's that requires HTTPS access but do not require client authentication
    static final String HTTPS_NOAUTH_URI = "https_noauth_uri";

    /**
     * Initialized database connections for all recognized servlets.
     */
    public void init(FilterConfig config) throws ServletException
    {
        ServletContext context = config.getServletContext();

        ds = (DataSource) context.getAttribute(DATA_SOURCE_KEY);

        String clientAuthClassName = context.getInitParameter(CLIENT_AUTH_CLASS);

        if (clientAuthClassName != null) {
            // new style client authenticator. The class to handle client
            // authentication is specified as a context parameter.
            try {
                Class clientAuthClass = Class.forName(clientAuthClassName);
                clientAuth = (ClientAuthenticator) clientAuthClass.newInstance();
            } catch (Exception e) {
                ServletException se = new ServletException(e.getMessage());
                se.setStackTrace(e.getStackTrace());
                throw se;
            }
        } else {
            // pre 2.0 releases always assume Depot authentication.
            clientAuth = new DepotAuthenticator();
        }

        String httpUriList = context.getInitParameter(HTTP_URI);
        String httpsUriList = context.getInitParameter(HTTPS_NOAUTH_URI);   
        
        if (httpUriList == null && httpsUriList == null && clientAuthClassName == null) {
            // This is one of the pre-2.0 releases, read configuration info from
            // the property file.
            Properties prop = (Properties) context.getAttribute(PROPERTY_KEY);
            if (prop != null) {
                httpUriList = prop.getProperty(HTTP_URI);
                httpsUriList = prop.getProperty(HTTPS_NOAUTH_URI);
            } else
                httpUriList = httpsUriList = null;
        }
        
        /*
         * Set up the list of URI that are allowed.
         */
        httpUriRegex = genRegex(httpUriList);
        httpsUriRegex = genRegex(httpsUriList);
       
        allowedURIs = new ThreadLocal() {
            protected synchronized Object initialValue() {
                return new UrlACL(httpUriRegex, httpsUriRegex);
            }
        };
    }
    
    // Convert a comma-separated list of URIs into a combined regular expression
    private Pattern genRegex(String list)
    {
        if (list == null)
            return null;
        
        StringTokenizer st = new StringTokenizer(list, ", \t");
        if (st.hasMoreElements()) {
            StringBuffer regex = new StringBuffer('(' + st.nextToken() + ')');
            while (st.hasMoreTokens()) {
                regex.append("|(").append(st.nextToken()).append(')');
            }
            return Pattern.compile(regex.toString());
        } else
            return null;
       
    }

    public void destroy()
    {
        ds = null;
        allowedURIs = null;
        clientAuth = null;
    }

    // compare the depot ID and public key from the certificate
    // against the corresponding record in the database.
    private HttpServletRequestWrapper authenticate(HttpServletRequest req)
        throws SQLException
    {
        String uri = req.getRequestURI();
        UrlACL matcher = (UrlACL) allowedURIs.get();

        // do *NOT* use req.isSecure() or req.scheme(). They just return
        // whatever set in the server.xml configuration file, instead of
        // what the AJP1.3 really says.

        // if we can't find a cipher suite, that must not be secure
        if (req.getAttribute("javax.servlet.request.cipher_suite") == null) {
            return matcher.matchHttpUri(uri) ? 
                   clientAuth.checkACL(ds, req, INSECURE, null) :
                   null;
        }

        X509Certificate[] cert_chain = 
            (X509Certificate[]) req.getAttribute("javax.servlet.request.X509Certificate");
        
        if (cert_chain == null) {
            // no client authentication performed
            return matcher.matchHttpsUri(uri) ? 
                   clientAuth.checkACL(ds, req, NOCLIENT_AUTH, null) :
                   null;
        } else
            return clientAuth.checkACL(ds, req, SECURE, cert_chain[0]);
    }

    
    /**
     * Compare the public key submitted with that on file, and reject connection
     * if they don't match.
     */
    public void doFilter(final ServletRequest req, final ServletResponse res,
                         FilterChain chain) 
        throws IOException, ServletException
    {
        HttpServletRequestWrapper request_wrapper = null;

        try {
            request_wrapper = authenticate((HttpServletRequest) req);
        } catch (Exception e) {
            e.printStackTrace(System.out);
            request_wrapper = null;
        }

        if (request_wrapper != null) {
            chain.doFilter(request_wrapper, res);
        } else {
            HttpServletResponse hres = (HttpServletResponse) res;

            // reject connection
            hres.sendError(hres.SC_UNAUTHORIZED);
        }
    }
    
    /**
     * Holds two regular expression matchers, one for the allowed URIs for HTTP
     * connections, and one for HTTPS connections (without client
     * authentication)
     * 
     */
    private static class UrlACL {
        final Matcher httpURLs;
        final Matcher httpsURLs;
        
        UrlACL(Pattern http, Pattern https){
            httpURLs = http != null ? http.matcher("") : null;
            httpsURLs = https != null ? https.matcher("") : null;
        }
        
        boolean matchHttpUri(String uri) {
            return (httpURLs != null) ? httpURLs.reset(uri).matches() : false;
        }
        
        boolean matchHttpsUri(String uri) {
            return (httpsURLs != null) ? httpsURLs.reset(uri).matches() : false;
        }
    }
    
}

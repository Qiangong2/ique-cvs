package com.broadon.filter;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.broadon.util.Pair;
import com.broadon.xml.XMLHybridMap;

public class XMLFilter implements Filter
{
    // The minimum length of a non-empty XML file is assumed to
    // be at least 7 characters (<root_tag><root_tag/>).  Content
    // less than the minimum will be treated as an empty file.
    //
    private static int MINIMUM_VALID_XML_LENGTH = 7;

    // Thread local XML parser
    private static class InputParser extends ThreadLocal {
	public Object initialValue() {
	    try {
		return new XMLHybridMap();
	    } catch (IOException e) {
		return null;
	    }
	}

	public XMLHybridMap getParser() {
	    return (XMLHybridMap) super.get();
	}
    } // InputParser

    static InputParser parser = null;

    // mapping of URI to set of acceptable elements
    static HashMap xmlElements = null;

    // dummy object for synchronization
    static Object sync = new Object();

    private Properties readXMLConfig(String propFile)
	throws IOException
    {
	Properties prop = new Properties();
	String fullPath = System.getProperty("catalina.base") + "/" + propFile;
	prop.load(new FileInputStream(fullPath));
	return prop;
	
    } // readXMLConfig
		  

    /**
     * Read from the configuration file the list of accepted URIs and
     * the description of the corresponding XML format.
     */
    public void init(FilterConfig config) throws ServletException {
	synchronized (sync) {
	    if (parser == null)
		parser = new InputParser();

	    if (xmlElements == null)
		xmlElements = new HashMap();

	    try {
		Context ctx = new InitialContext();
		
		// read the number of accepted URI
		Properties prop =
		    readXMLConfig((String) ctx.lookup("java:/comp/env/XMLRequest"));

		for (Enumeration e = prop.propertyNames(); e.hasMoreElements(); ) {
		    String URI = (String) e.nextElement();
		    StringTokenizer st = new StringTokenizer(prop.getProperty(URI));
		    String rootTag = st.hasMoreTokens() ? st.nextToken() : null;
		    HashSet elementSet = new HashSet();
		    while (st.hasMoreTokens()) {
			elementSet.add(st.nextToken());
		    }

		    xmlElements.put(URI, new Pair(rootTag, elementSet));
		}
	    } catch (Exception e) {
		throw new ServletException(e.getMessage());
	    }
	}
    } // init


    public void destroy() {
	synchronized (sync) {
	    parser = null;
	    xmlElements = null;
	}
    } // destroy


    /**
     * Parse the XML input and pass the result along as an attributes
     */
    public void doFilter(final ServletRequest req,
			 final ServletResponse res,
			 FilterChain chain)
	throws IOException, ServletException
    {
	HttpServletRequest hreq = (HttpServletRequest) req;
	HttpServletResponse hres = (HttpServletResponse) res;

    // We decided to remove this restriction, as it should be 
    // controlled by individual servlets where relevant, rather
    // in this filter.
    //
// 	if (! hreq.getMethod().equals("POST")) {
// 	    hres.sendError(hres.SC_METHOD_NOT_ALLOWED);
// 	    return;
// 	}

	// Check against the list of accepted URI, and pulls out the
	// root and element tags of the corresponding XML input.
	
	Pair xmlRequest = (Pair) xmlElements.get(hreq.getRequestURI());
	if (xmlRequest == null) {
	    // URI not found
	    hres.sendError(hres.SC_NOT_FOUND);
	    return;
	}

	// parse the request content, which if present must be in XML format.
	// The result is a HashMap of all element tags and their
	// corresponding value(s).  Skip content too small to be XML without
    // any validity check (SAXParser does not accept empty documents).
	//
    HashMap requestBody = new HashMap();
    int     contentLength = hreq.getContentLength();
    if (contentLength >= MINIMUM_VALID_XML_LENGTH) {

//         XMLHybridMap p = parser.getParser();
//         p.setMultiValueElements((HashSet) xmlRequest.second);
//         requestBody =
//             (HashMap) p.process(hreq.getInputStream(), requestBody,
//                                 (String) xmlRequest.first);

        InputStream           in = hreq.getInputStream();
        ByteArrayOutputStream buf = new ByteArrayOutputStream(2024);
        String                str = null;
        try {
            byte[] btBuf = new byte[2024];
            for (int sz = in.read(btBuf, 0, btBuf.length);
                 sz >= 0;
                 sz = in.read(btBuf, 0, btBuf.length)) {
                buf.write(btBuf, 0, sz);
            }
            str = buf.toString();
            if (str.length() > contentLength) {
                System.err.println(new java.util.Date() + " " +
                                   getClass().getName() +
                                   " NON_FATAL_ERROR truncating request (" + 
                                   str.length() + 
                                   " chars) to content-length (" +
                                   contentLength + " chars):\n" + str);
                str = str.substring(0, contentLength);
            }

            XMLHybridMap p = parser.getParser();
            p.setMultiValueElements((HashSet) xmlRequest.second);
            requestBody =
                (HashMap) p.process(str, requestBody,
                                    (String) xmlRequest.first);
        }
        catch (IOException t) {
            if (str != null) {
                System.err.println(new java.util.Date() + " " +
                                   getClass().getName() +
                                   " ERROR failed to apply XML filter to " +
                                   " request body (" + str.length() +
                                   " chars):\n" + str);
            }
            throw t;
        }
    }

	chain.doFilter(new XMLWrapper(hreq, requestBody), res);
	
    } // doFilter
}

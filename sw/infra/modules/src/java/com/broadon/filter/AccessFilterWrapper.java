package com.broadon.filter;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

/**
 * Response wrapper that intercepts: (1) the number of bytes written,
 * and (2) the HTTP response status code.
 */
public class AccessFilterWrapper extends HttpServletResponseWrapper
{
    HttpServletResponse res;
    boolean isError;

    AccessFilterOutputStream stream = null;
    PrintWriter writer = null;

    protected AccessFilterWrapper(HttpServletResponse res) {
	super(res);
	this.res = res;
	isError = false;
    }

    protected boolean isError() {
	return isError;
    }

    protected int byteSent() {
	if (stream == null)
	    return 0;
	return stream.byteSent();
    }

    // following are to override the corresponding super class methods
    // so that we can intercept the error code and bytes sent.

    public void sendError(int sc) throws IOException {
	isError = (sc != SC_OK);
	res.sendError(sc);
    }

    public void sendError(int sc, String msg) throws IOException {
	isError = (sc != SC_OK);
	res.sendError(sc, msg);
    }

    public void setStatus(int sc) {
	isError = (sc != SC_OK);
	res.setStatus(sc);
    }

    public void setStatus(int sc, String msg) {
	isError = (sc != SC_OK);
	res.setStatus(sc, msg);
    }

    public ServletOutputStream getOutputStream()
	throws IOException
    {
	if (writer != null)
            throw new IllegalStateException("getWriter() has already been called for this response");

        if (stream == null)
            stream = new AccessFilterOutputStream(res.getOutputStream());
        return stream;
    }


    public PrintWriter getWriter() throws IOException
    {
        if (writer != null)
            return writer;

        if (stream != null)
             throw new IllegalStateException("getOutputStream() has already been called for this response");

        stream = new AccessFilterOutputStream(res.getOutputStream());
        writer = new PrintWriter(stream, true);
        return writer;
    }


    public void flushBuffer() throws IOException {
	if (writer != null)
	    writer.flush();
	if (stream != null)
	    stream.flush();
	super.flushBuffer();
    }


    protected void finishResponse() {
	try {
	    if (writer != null)
		writer.close();
	    else if (stream != null)
		stream.close();
	} catch (IOException e) {
	}
    }
}

package com.broadon.filter;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

/**
 * Wrapper for representing the result of the XML parser as parameters
 * of the HttpRequest. 
 */
public class XMLWrapper extends HttpServletRequestWrapper
{
    HashMap map;

    /** Construct a <c>XMLWrapper</c> instance using the given result
     * of XML parser.
     * @param req Original request to be wrapped.
     * @param map Output of XML parser.
     */
    protected XMLWrapper(HttpServletRequest req, HashMap map) {
	super(req);
	this.map = map;
    }

    public String getParameter(String name) {
	Object obj = map.get(name);
	if (obj == null)
	    return super.getParameter(name);
	if (obj instanceof Vector) {
	    Vector v = (Vector) obj;
	    return (String) v.firstElement();
	} else
	    return (String) obj;
    }
    

    public Map getParameterMap() {
	HashMap clone = (HashMap) map.clone();
	clone.putAll(super.getParameterMap());
	return clone;
    }

    public Enumeration getParameterNames() {
	return new ParamNameEnum(super.getParameterNames());
    }

    // Enumerate first the parameter names in the local map, and then
    // those in the wrapper request.
    private class ParamNameEnum implements Enumeration {
	Enumeration e;
	Set keys;
	Iterator i;

	private ParamNameEnum(Enumeration e) {
	    this.e = e;
	}

	private void setupLocalEnumeration() {
	    e = null;
	    keys = map.keySet();
	    i = keys.iterator();
	}
	
	public boolean hasMoreElements() {
	    if (e != null) {
		if (e.hasMoreElements())
		    return true;
		else {
		    setupLocalEnumeration();
		}
	    }
	    return i.hasNext();
	}

	public Object nextElement() {
	    if (e != null) {
		try {
		    return e.nextElement();
		} catch (NoSuchElementException e) {
		    setupLocalEnumeration();
		}
	    }
	    return i.next();
	}
    } // ParamNameEnum

    
    public String[] getParameterValues(String name) {
	Object obj = map.get(name);
	if (obj == null)
	    return super.getParameterValues(name);
	if (obj instanceof Vector) {
	    Vector v = (Vector) obj;
	    String[] result = new String[v.size()];
	    return (String[]) v.toArray(result);
	} else {
	    String[] values = new String[1];
	    values[0] = (String) obj;
	    return values;
	}
    }
}

package com.broadon.filter;

import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.sql.DataSource;

import com.broadon.db.OracleDataSource;

/**
 * Wrapper for the HttpServletRequest to hold the results of the
 * authentication filter.
 * For authentication purpose, the authFilter class needs to read the
 * Depot record from the database and compares that info with what the
 * BB Depot actually submitted.  A side effect of this is that the
 * filter has a copy of the Depot record and a handle to the database
 * connection pool.  So we just pass them down.
 * @version $Revision: 1.15 $
 * @author W. Wilson Ho
 */
public class authWrapper extends HttpServletRequestWrapper
{
    DataSource ds;
    String encoded_depotID = null;
    long depotID = -1;
    int storeID = -1;
    int regionID = -1;
    int buID = -1;
    boolean clientAuth = false;

    /**
     * Construct a wrapper with the given connectin pool.
     * @param req HTTP servlet request object to be wrapped.
     * @param pc Pooled Connection to be passed down to servlet.
     */
    public authWrapper(HttpServletRequest req, DataSource ds) {
	super(req);
	this.ds = ds;
    }


    /** @return The data source. */
    public DataSource getDataSource() {
	return ds;
    }


    /** @return <code>true</code> if the connection is secure and
     * with client authentication */
    public boolean hasClientAuth() {
	return clientAuth;
    }
    

    /** Set client authentication to true. */
    protected void setClientAuth() {
	clientAuth = true;
    }

    /**
     * Return a new connection from the connection pool.
     * @return A new logical connection.
     * @exception SQLException if a database-access error occurs.
     */
    public Connection getConnection() throws SQLException {
	return ds.getConnection();
    }

    /**
     * Return a new connection from the connection pool.
     * @param priority One of the priority as defined in
     * <code>com.broadon.db.DataSourceScheduler</code>.
     * @return A new logical connection.
     * @exception SQLException if a database-access error occurs.
     */
    public Connection getConnection(int priority) throws SQLException {
	if (ds instanceof OracleDataSource.OracleDataSourceProxy)
	    return ((OracleDataSource.OracleDataSourceProxy) ds).getConnection(priority);
	else
	    return ds.getConnection();
    }

    /**
     * @return The decoded ID of the Depot.
     */
    public long getDepotID() {
	return depotID;
    }

    /** Set the Depot ID field */
    protected void setDepotID(long depotID) {
	this.depotID = depotID;
    }


    /** @return the encoded ID of the Depot. */
    public String getEncodedDepotID() {
	return encoded_depotID;
    }
    
    /** Set the Encoded Depot ID */
    protected void setEncodedDepotID(String encoded_depotID) {
	this.encoded_depotID = encoded_depotID;
    }

    /** @return the ID of the store to which the depot belongs. */
    public int getStoreID() {
	return storeID;
    }

    /** Set the store ID. */
    protected void setStoreID(int storeID) {
	this.storeID = storeID;
    }

    /** @return The region ID to which this depot belong. */
    public int getRegionID() {
	return regionID;
    }

    /** Set the region ID field */
    protected void setRegionID(int regionID) {
	this.regionID = regionID;
    }

    /** @return The Business Unit ID to which this depot belong. */
    public int getBUID() {
	return buID;
    }

    /** Set the Business Unit ID field. */
    protected void setBUID(int buID) {
	this.buID = buID;
    }
} 

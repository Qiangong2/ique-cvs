package com.broadon.filter;

import java.io.IOException;
import javax.servlet.ServletOutputStream;

/**
 * Wrap a regular <code>ServletOutputStream</code> with the exception
 * that the number of bytes written recorded.
 */
public class AccessFilterOutputStream extends ServletOutputStream
{
    int count;
    ServletOutputStream out;

    protected AccessFilterOutputStream(ServletOutputStream out) {
	this.out = out;
    }

    public void write(int b) throws IOException {
	out.write(b);
	++count;
    }

    public void write(byte[] b, int off, int len) throws IOException {
	out.write(b, off, len);
	count += len;
    }

    /** @return The total number of bytes sent. */
    protected int byteSent() {
	return count;
    }
}

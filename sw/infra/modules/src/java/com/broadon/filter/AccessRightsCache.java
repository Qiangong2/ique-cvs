package com.broadon.filter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.broadon.util.BackingStore;
import com.broadon.util.BackingStoreException;
import com.broadon.util.CacheLFU;

public class AccessRightsCache extends CacheLFU implements BackingStore
{
    static final String getACL =
        "SELECT REGION_ID, RIGHTS_MASK FROM MODULE_ACCESS_RIGHTS " +
        "WHERE MODULE_CERT = ? AND SERVER_MODULE = ?";

    private DataSource ds;
    
    public AccessRightsCache(DataSource ds) {
        super();
        this.ds = ds;
    }

    public Object retrieve(Object key) throws BackingStoreException
    {
        Key cacheKey = (Key) key;
        
        Connection conn = null;
        PreparedStatement ps = null;
        
        try {
            conn = ds.getConnection();
            ps = conn.prepareStatement(getACL);
            ps.setString(1, cacheKey.clientName);
            ps.setString(2, cacheKey.serviceName);
            
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                // for now, we don't know what to do with the region ID, so just ignore it.
                String rights = rs.getString(2);
                if (rs.wasNull())
                    return "";
                else
                    return rights;
            } else
                return null;
        } catch (SQLException e) {
           throw BackingStoreException.rewrap(e);
        } finally {
            try {
                if (ps != null)
                    ps.close();
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {}
        }
        
    }

    public static class Key
    {
        public final String clientName;      // CN in a cert or any other name
        public final String serviceName;     // usually the soap action name
        
        public Key(String client, String service) {
            clientName = client;
            serviceName = service;
        }
    }
}

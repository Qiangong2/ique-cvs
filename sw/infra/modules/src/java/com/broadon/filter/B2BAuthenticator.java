package com.broadon.filter;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.cert.X509Certificate;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.sql.DataSource;

import com.broadon.servlet.ServletConstants;
import com.broadon.util.BackingStoreException;
import com.broadon.util.Cache;

/**
 * Authenticate clients that are B2B partners or another internal front end server. 
 *
 */
public class B2BAuthenticator extends ClientAuthenticator implements ServletConstants
{
    static public final String LOCAL_CLIENT = "LAN";
    
    Cache aclCache = null;
    
    /* (non-Javadoc)
     * @see com.broadon.filter.ClientAuthenticator#checkACL(javax.sql.DataSource, javax.servlet.http.HttpServletRequest, int, java.security.cert.X509Certificate)
     */
    public HttpServletRequestWrapper checkACL(DataSource ds, HttpServletRequest req,
                                              int transport, X509Certificate cert) 
        throws SQLException
    {
        switch (transport) {
        case authFilter.INSECURE:
            try {
                InetAddress addr = InetAddress.getByName(req.getRemoteAddr());
                if (addr.isSiteLocalAddress() || addr.isLoopbackAddress())
                    req.setAttribute(CLIENT_CREDENTIAL, LOCAL_CLIENT);
            } catch (UnknownHostException e) {}
            // fall through
        case authFilter.NOCLIENT_AUTH:
            return new authWrapper(req, ds);
            
        default:
            break;
        }
        
        // expect a client cert
        if (cert == null)
            return null;
    
        if (aclCache == null) {
            synchronized (this) {
                if (aclCache == null) 
                    aclCache = new AccessRightsCache(ds);
            }
        }
        
        try {
            if (aclCache.get(getKey(cert, req)) == null)
                return null;
        } catch (BackingStoreException e) {
            // most of the time, BackingStoreException is a wrapping of SQLException
            if (e.getCause() instanceof SQLException)
                throw (SQLException) e.getCause();
            SQLException se = new SQLException(e.getLocalizedMessage());
            se.setStackTrace(e.getStackTrace());
            throw se;
        }
        
        req.setAttribute(CLIENT_CREDENTIAL, cert);
        return new authWrapper(req, ds);
    }

    private AccessRightsCache.Key getKey(X509Certificate cert, HttpServletRequest req)
    {
        String service = req.getContextPath().substring(1);
        return new AccessRightsCache.Key(getCN(cert), service);
    }
}
    
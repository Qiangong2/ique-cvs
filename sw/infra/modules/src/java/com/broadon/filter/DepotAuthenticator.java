package com.broadon.filter;

import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPublicKey;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.sql.DataSource;

import com.broadon.db.DataSourceScheduler;

public final class DepotAuthenticator extends ClientAuthenticator
{
    static final String QUERY = 
        "SELECT DEPOTS.STORE_ID, STORES.REGION_ID, REGIONS.BU_ID FROM "
            + "STORES, DEPOTS, RETAILERS, REGIONS WHERE "
            + "DEPOTS.HR_ID = ? AND DEPOTS.PUBLIC_KEY = ? AND "
            + "DEPOTS.SUSPEND_DATE IS NULL AND "
            + "DEPOTS.REVOKE_DATE IS NULL AND "
            + "DEPOTS.STORE_ID = STORES.STORE_ID AND "
            + "STORES.SUSPEND_DATE IS NULL AND "
            + "STORES.REVOKE_DATE IS NULL AND "
            + "STORES.RETAILER_ID = RETAILERS.RETAILER_ID AND "
            + "STORES.REGION_ID = REGIONS.REGION_ID AND "
            + "RETAILERS.SUSPEND_DATE IS NULL AND "
            + "RETAILERS.REVOKE_DATE IS NULL";

    private static long encodeDepotID(String depotID)
    {
        if (depotID.startsWith("HR"))
            return Long.parseLong(depotID.substring(2), 16);
        else
            return -1;
    }

    private static String encodePublicKey(RSAPublicKey key)
    {
        return key.getModulus().toString(32);
    }

    public HttpServletRequestWrapper checkACL(DataSource ds,
                                              HttpServletRequest req,
                                              int transport,
                                              X509Certificate cert) 
        throws SQLException
    {
        switch (transport) {
        case authFilter.NOCLIENT_AUTH:
        case authFilter.INSECURE:
            return new authWrapper(req, ds);
        default:
            break;    
        }
        String depotID = getCN(cert);
        long id = encodeDepotID(depotID);
        String key = encodePublicKey((RSAPublicKey) cert.getPublicKey());

        authWrapper wrapper = new authWrapper(req, ds);
        wrapper.setClientAuth();
        Connection conn = wrapper.getConnection(DataSourceScheduler.DB_PRIORITY_FAST);

        try {
            PreparedStatement stmt = conn.prepareStatement(QUERY);

            stmt.setLong(1, id);
            stmt.setString(2, key);
            try {
                ResultSet rs = stmt.executeQuery();

                if (!rs.next())
                    throw new SQLException("Depot not found");
              
                wrapper.setEncodedDepotID(depotID);
                wrapper.setDepotID(id);
                wrapper.setStoreID(rs.getInt(1));
                wrapper.setRegionID(rs.getInt(2));
                wrapper.setBUID(rs.getInt(3));
                rs.close();
                return wrapper;
            } finally {
                stmt.close();
            }
        } finally {
            conn.close();
        }
    }
}

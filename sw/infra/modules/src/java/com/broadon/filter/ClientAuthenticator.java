package com.broadon.filter;

import java.security.cert.X509Certificate;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.sql.DataSource;

/**
 * Check the client certificate against the database access control list
 * 
 */
public abstract class ClientAuthenticator 
{

    /**
     * @param conn
     *            Database connection
     * @param req
     *            The original HTTP request
     * @param transport One of INSECURE, SECURE, NOCLIENT_AUTH as defined in authFilter. 
     * @param cert
     *            The client certificate
     * @return A HTTP request wrapper with additional information of this client
     *         from the database
     * @throws SQLException 
     */

    public abstract HttpServletRequestWrapper checkACL(DataSource ds,
                                                       HttpServletRequest req, 
                                                       int transport, X509Certificate cert) 
        throws SQLException;
    

    // extract the CN field from the Subject DN of a certificate.
    protected String getCN(X509Certificate cert) 
    {
        String dn = cert.getSubjectDN().getName();
        int idx_s = dn.indexOf("CN=");
        int idx_e = dn.indexOf(",", idx_s);
        if (idx_s >= 0) {
            if (idx_e > 0)
                return dn.substring(idx_s + 3, idx_e);
            else
                return dn.substring(idx_s + 3);
        }
        return null;
    }
}

package com.broadon.filter;

import java.io.IOException;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import com.broadon.status.FlushStatistics;
import com.broadon.status.Statistics;

/**
 * Filter to collect operation statistics for all requests.
 */
public class AccessFilter implements Filter
{
    Statistics stat;
    TimerTask task;
    Timer timer;

    static final long ONE_HOUR = 3600000; // # of milliseconds in 1 hour

    // dummy object for synchronization
    static Object sync;

    // current number of active requests. 
    static int concurrentServlet;

    // Bug 1505, last time to print timestamp to catalina.out
    static long lastTimestamp;

    public void init(FilterConfig config) throws ServletException
    {
	ServletContext context = config.getServletContext();
	stat = new Statistics();
	String statFile = System.getProperty("catalina.base") +
	    "/logs/" +  context.getServletContextName() + ".statistics";

	long interval;

	try {
	    interval = Long.parseLong(config.getInitParameter("interval"));
	} catch (NumberFormatException e) {
	    interval = ONE_HOUR;
	}

	task = new FlushStatistics(statFile, stat);
	timer = new Timer();
	timer.schedule(task, interval, interval);
	sync = new Object();
	concurrentServlet = 0;
       // Bug 1505
        lastTimestamp = 0;
    }

    public void destroy() {
	timer.cancel();
	task.run();
    }

    public void doFilter(ServletRequest req, ServletResponse res,
                         FilterChain chain)
        throws IOException, ServletException
    {
	long requestTime = System.currentTimeMillis();
        // Bug 1505
        boolean printTimestamp = false;

	synchronized (sync) {
	    concurrentServlet++;
            // Bug 1505, 1 minute = 60000 milliseconds
            if (requestTime >= lastTimestamp + 60000) {
              printTimestamp = true;
              lastTimestamp = requestTime;
            }
	}

        // Bug 1505, Print timestamp in catalina.out every minute
        if (printTimestamp) {
          System.out.println(new  Date().toString());
        }

	try {
	    AccessFilterWrapper wrapper =
		new AccessFilterWrapper((HttpServletResponse) res);

	    try {
		chain.doFilter(req, wrapper);
	    } finally {
		wrapper.finishResponse();
	    }
	    
	    if (wrapper.isError())
		stat.addError();
	    else {
                double duration = (System.currentTimeMillis() - requestTime) / 1000.0;
		stat.addData(duration, concurrentServlet, wrapper.byteSent());
	    }
	} finally {
	    synchronized (sync) {
		--concurrentServlet;
	    }
	}
    }
}

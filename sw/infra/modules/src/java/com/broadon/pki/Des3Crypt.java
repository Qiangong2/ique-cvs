package com.broadon.pki;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import com.ncipher.km.nfkm.CardSet;
import com.ncipher.km.nfkm.ConsoleCallBack;
import com.ncipher.km.nfkm.Key;
import com.ncipher.km.nfkm.Module;
import com.ncipher.km.nfkm.SecurityWorld;
import com.ncipher.nfast.NFException;
import com.ncipher.nfast.NFUtils;
import com.ncipher.nfast.connect.NFConnection;
import com.ncipher.nfast.connect.StatusNotOK;
import com.ncipher.nfast.marshall.M_Block64;
import com.ncipher.nfast.marshall.M_ByteBlock;
import com.ncipher.nfast.marshall.M_CipherText;
import com.ncipher.nfast.marshall.M_Cmd;
import com.ncipher.nfast.marshall.M_Cmd_Args_Decrypt;
import com.ncipher.nfast.marshall.M_Cmd_Args_Encrypt;
import com.ncipher.nfast.marshall.M_Cmd_Reply_Decrypt;
import com.ncipher.nfast.marshall.M_Cmd_Reply_Encrypt;
import com.ncipher.nfast.marshall.M_Command;
import com.ncipher.nfast.marshall.M_KeyID;
import com.ncipher.nfast.marshall.M_Mech;
import com.ncipher.nfast.marshall.M_Mech_Cipher_Generic64;
import com.ncipher.nfast.marshall.M_Mech_IV_Generic64;
import com.ncipher.nfast.marshall.M_PlainText;
import com.ncipher.nfast.marshall.M_PlainTextType;
import com.ncipher.nfast.marshall.M_PlainTextType_Data_Bytes;
import com.ncipher.nfast.marshall.M_Reply;
import com.ncipher.nfast.marshall.M_Status;

public class Des3Crypt
{

    private SecurityWorld sw;
    private M_KeyID key;

    public Des3Crypt(String keyname) throws NFException
    {
        // load all the relevant keys
        sw = new SecurityWorld(null, new ConsoleCallBack(), null, true);
        Key[] keyList = sw.listKeys(null);
        for (int i = 0; i < keyList.length; i++) {
            if (keyname.equals(keyList[i].getName())) {
                // Key k = sw.getKey("pkcs11", keyList[i].getIdent());
                Key k = keyList[i];
                if (k != null && k.isCardSetProtected()) {
                    CardSet cs = k.getCardSet();
                    Module module = sw.getModule(1);
                    cs.load(module.getSlot(0));
                    System.out.println("Card set loaded: " + cs.getName());
                    key = k.load(cs, module);
                }
            }
        }
    }

    private byte[] doEncryptDES3(byte[] plaintext) throws NFException
    {
        // Create an nCore Command
        M_Command cmd = new M_Command();
        cmd.cmd = M_Cmd.Encrypt;

        M_Cmd_Args_Encrypt args = new M_Cmd_Args_Encrypt();
        args.key = key;
        args.mech = M_Mech.DES3mCBCi64pPKCS5;
        args.plain = new M_PlainText();

        // Package the Supplied Plaintext
        M_PlainTextType_Data_Bytes pt_data = new M_PlainTextType_Data_Bytes();
        pt_data.data = new M_ByteBlock(plaintext);
        args.plain.type = M_PlainTextType.Bytes;
        args.plain.data = pt_data;
        // NOTE: no IV is given, which tells the module to
        // generate one randomly and return it in the reply.

        cmd.args = args;

        // Send Command and Wait for Reply
        M_Reply reply = transact(cmd, sw.getConnection());

        // Collect Initialization Vector and Ciphertext
        M_CipherText ct = ((M_Cmd_Reply_Encrypt) reply.reply).cipher;

        byte[] ciphertext = ((M_Mech_Cipher_Generic64) ct.data).cipher.value;
        byte[] iv = ((M_Mech_IV_Generic64) ct.iv).iv.value;
        byte[] output = new byte[ciphertext.length + iv.length];

        System.arraycopy(iv, 0, output, 0, iv.length);
        System.arraycopy(ciphertext, 0, output, iv.length, ciphertext.length);

        return output;
    } // doEncryptDES3(M_KeyID,byte[])
    
    
    private byte[] doDecryptDES3(byte[] ciphertext) throws NFException
    {
        // Create an nCore Command
        M_Command cmd = new M_Command();
        cmd.cmd = M_Cmd.Decrypt;

        M_Cmd_Args_Decrypt args = new M_Cmd_Args_Decrypt();
        args.key = key;
        args.mech = M_Mech.DES3mCBCi64pPKCS5;
        args.cipher = new M_CipherText();

        // Package the Supplied Ciphertext

        // NOTE: we assume here that the Initialization Vector is
        // supplied as the first eight bytes of the ciphertext array.
        M_Mech_IV_Generic64 iv_data = new M_Mech_IV_Generic64();
        byte[] iv = new byte[8];
        System.arraycopy(ciphertext, 0, iv, 0, iv.length);
        iv_data.iv = new M_Block64(iv);
        args.cipher.iv = iv_data;

        M_Mech_Cipher_Generic64 ct_data = new M_Mech_Cipher_Generic64();
        byte[] ctext = new byte[ciphertext.length - iv.length];
        System.arraycopy(ciphertext, iv.length, ctext, 0, ctext.length);
        ct_data.cipher = new M_ByteBlock(ctext);
        args.cipher.data = ct_data;

        args.cipher.mech = M_Mech.DES3mCBCi64pPKCS5;
        args.reply_type = M_PlainTextType.Bytes;
        cmd.args = args;

        // Send Command and Wait for Reply
        M_Reply reply = transact(cmd, sw.getConnection());

        // Extract Decrypted Plaintext
        M_PlainText pt = ((M_Cmd_Reply_Decrypt) reply.reply).plain;
        byte[] output = ((M_PlainTextType_Data_Bytes) pt.data).data.value;
        return output;
    } // doDecryptDES3(M_KeyID,byte[])
    
    
    /**
     * This method sends a command to the hardserver and returns its reply.
     * @throws NFException 
     */
    M_Reply transact(M_Command cmd, NFConnection conn) throws NFException
    {
        M_Reply reply = conn.transact(cmd);
        
        if (reply.status != M_Status.OK) {
            throw new StatusNotOK("Error during " + M_Cmd.toString(cmd.cmd)
                    + ", status "
                    + NFUtils.errorString(reply.status, reply.errorinfo));
        }
        return reply;
    }
    
    /**
     * @param args
     */
    public static void main(String[] args)
    {
        if (args.length < 3 || (args.length < 4 && "-d".equals(args[0]))) {
            System.err.println("Usage: Des3Crypt [-d] keyname infile outfile\n" +
                        "-d:  decrypt mode (default is encrypt)");
            return;
        }

        boolean encrypt = true;
        int idx = 0;
        if ("-d".equals(args[idx])) {
             encrypt = false;
             ++idx;
        }

        String keyname = args[idx++];
        String infile = args[idx++];
        String outfile = args[idx++];
        
        try {
            Des3Crypt crypt = new Des3Crypt(keyname);
            byte[] inbuf = readFile(infile);
            byte[] outbuf = encrypt ? crypt.doEncryptDES3(inbuf) : crypt.doDecryptDES3(inbuf);
            writeFile(outbuf, outfile);
                
        } catch (Exception e) {
            
            e.printStackTrace();
        }
        
    }

    private static void writeFile(byte[] outbuf, String outfile) throws IOException
    {
        FileOutputStream out = new FileOutputStream(outfile);
        out.write(outbuf);
        out.close();
    }

    private static byte[] readFile(String infile) throws IOException
    {
        File file = new File(infile);
        byte[] inbuf = new byte[(int) file.length()];
        FileInputStream in = new FileInputStream(file);
        in.read(inbuf);
        in.close();
        return inbuf;
    }

}

package com.broadon.auditlog;

import java.io.PrintStream;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.sql.DataSource;

import com.broadon.db.AuditLog;
import com.broadon.db.DBAccessFactory;
import com.broadon.servlet.AuditLogAgent;
import com.broadon.servlet.ServletConstants;
import com.broadon.util.Queue;

/**
 * This <code>AuditServletLogger</code> class generates a specified number
 * of threads that take audit logging requests from a <code>Queue</code>.
 */
public class AuditServletLogger
    implements ServletContextListener, ServletConstants
{
    private static final String	THREAD_NUM;

    static
    {
	THREAD_NUM = "audit-log-number-of-threads";
    }

    private AuditLogger[]	auditLoggers;
    private int			threadCount;

    /**
     * Constructs a <code>AuditServletLogger</code> instance.
     */
    public AuditServletLogger()
    {
	auditLoggers = null;
	threadCount = 0;
    }

    /**
     * Starts up the audit logger.
     *
     * @param	event			the event for the audit logger
     */
    public void contextInitialized(ServletContextEvent event)
    {
	/*
	 * Get ServletContext from the event.
	 */
	ServletContext		servletContext = event.getServletContext();
	/*
	 * Get the data source.
	 */
	DataSource		dataSource;

	dataSource = (DataSource)servletContext.getAttribute(DATA_SOURCE_KEY);

	/*
	 * Get the name mapping table.
	 */
	Map	nameMap = (Map)servletContext.getAttribute(NAME_MAP_KEY);
	/*
	 * Get the queue.
	 */
	Queue	queue = AuditLogAgent.getQueue(servletContext);
	/*
	 * Start the database loggers waiting on the audit log queue.
	 */
	String	threadNum = servletContext.getInitParameter(THREAD_NUM);

	this.threadCount = (threadNum == null)
					? 1
					: Integer.parseInt(threadNum);

	this.auditLoggers = new AuditLogger[this.threadCount];
	try
	{
	    for (int n = 0; n < threadCount; n++)
	    {
		this.auditLoggers[n] = new AuditLogger(queue,
						       dataSource,
						       nameMap,
						       System.out);
		this.auditLoggers[n].start();
	    }
	}
	catch (SQLException se)
	{
	    se.printStackTrace(System.out);
	}
    }

    /**
     * Shuts down the audit logger.
     *
     * @param	event			the event for the audit logger
     */
    public void contextDestroyed(ServletContextEvent event)
    {
	for (int n = 0; n < threadCount; n++)
	{
	    auditLoggers[n].shutdown();
	}
    }

    /**
     * The <code>AuditLogger</code> class corresponds to an audit logger
     * thread.
     */
    private class AuditLogger
	extends Thread
    {
	private Queue		queue;
	private DataSource	dataSource;
	private Map		nameMap;
	private Map		factories;
	private PrintStream	out;
	private boolean		toStop;

	/**
	 * Constructs an AuditLogger instance.
	 *
	 * @param	queue		the audit log Queue for getting the log
	 * @param	dataSource	the database to log to
	 * @param	nameMap		the name mapping table
	 * @param	out		the output stream for error logging
	 */
	private AuditLogger(Queue queue,
			    DataSource dataSource,
			    Map nameMap,
			    PrintStream out)
	    throws SQLException
	{
	    this.queue = queue;
	    this.dataSource = dataSource;
	    this.nameMap = nameMap;
	    this.factories = new HashMap();
	    this.out = out;
	    this.toStop = false;
	}

	/**
	 * Stops this thread.
	 */
	private void shutdown()
	{
	    this.toStop = true;
	    this.queue.terminate();
	}

	/**
	 * Processes the given audit log by creating a record in the database.
	 *
	 * @param	auditLog	the audit log instance that contains
	 *				the audit log information
	 */
	private void process(AuditLog auditLog)
	    throws Throwable
	{
	    /*
	     * Construct the factory class name.
	     */
	    String		className = auditLog.getClass().getName();
	    String		factoryName = className + "Factory";
	    /*
	     * See if it is already in the factory cache.
	     *
	     * There is no need to synchronize here because this cache
	     * is used by a single owning thread only.
	     */
	    DBAccessFactory	factory;

	    factory = (DBAccessFactory)factories.get(factoryName);
	    if (factory == null)
	    {
		/*
		 * Get the factory the first time by reflection.
		 */
		Class		factoryClass = Class.forName(factoryName);
		Method		method;

		method = factoryClass.getMethod("getInstance",
						new Class[] { String.class });
		factory = (DBAccessFactory)method.invoke(
					    null,
					    new String[] { factoryName });
		/*
		 * Cache it.
		 */
		factories.put(factoryName, factory);
	    }
	    /*
	     * Create a new audit log record into the database.
	     */
	    factory.create(auditLog, nameMap);
	}

	/**
	 * The main method of the audit log threads.
	 */
	public void run()
	{
	    while (true)
	    {
		/*
		 * Wait for work to do.
		 */
		Object[]	auditLogs = queue.dequeueAll(true);

		if (auditLogs == null) {
		    toStop = true;
		    return;
		}

		/*
		 * Process the audit logs.
		 *
		 * Even if the toStop flag has been turned on, it will
		 * still try to finish the work before shutting down.
		 */
		try
		{
		    DBAccessFactory.beginTransaction(dataSource, false, false);
		    try
		    {
			int	size = auditLogs.length;

			for (int n = 0; n < size; n++)
			{
			    process((AuditLog)auditLogs[n]);
			}
		    }
		    catch (Throwable t)
		    {
			/*
			 * Stop at the fisrt failure, but continue
			 * to commit.
			 */
			t.printStackTrace(out);
		    }
		    finally
		    {
			/*
			 * Commit those that were successfully created.
			 */
			DBAccessFactory.commitTransaction();
		    }
		}
		catch (Throwable t)
		{
		    /*
		     * Since the audit log is not critical, failing the
		     * transaction is forgivable.
		     */
		    t.printStackTrace(out);
		}
		if (toStop)
		{
		    /*
		     * Stop.
		     */
		    return;
		}
	    }
	}
    }
}

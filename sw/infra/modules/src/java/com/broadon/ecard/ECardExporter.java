package com.broadon.ecard;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.broadon.db.DBAccessFactory;
import com.broadon.db.DBException;
import com.broadon.db.ECardType;
import com.broadon.db.ECardTypeFactory;
import com.broadon.util.ECardNumber;
import com.broadon.ecard.db.ECardBase;
import com.broadon.ecard.db.ECardBaseFactory;
import com.broadon.ecard.db.ECardBatch;
import com.broadon.ecard.db.ECardBatchFactory;
import com.broadon.ecard.db.EndECardID;

/**
 * This <code>ECardExporter</code> class exports eCards from the database
 * for printing, one batch at a time.
 *
 * @version	$Revision: 1.8 $
 */
public class ECardExporter
    extends ECardTool
{
    private static final int			PACKS_PER_LOT;
    private static final String			BATCH_FILE_NAME;
    private static final String			BATCH_LABEL_FILE_NAME;
    private static final String			LOT_LABEL_FILE_NAME_PREFIX;
    private static final String			PACK_LABEL_FILE_NAME_PREFIX;
    private static final SimpleDateFormat	DATE_FORMATTER;
    private static final String			ECARD_BASE_FACTORY_NAME;
    private static final String			ECARD_BATCH_FACTORY_NAME;
    private static final String			ECARD_TYPE_FACTORY_NAME;

    static
    {
	/*
	 * Number related.
	 */
	PACKS_PER_LOT	= LOT_UNIT_SIZE / PACK_UNIT_SIZE;
	/*
	 * File name related.
	 */
	String	label = "label";
	String	separator = "-";

	BATCH_FILE_NAME			= "batch";
	BATCH_LABEL_FILE_NAME		= BATCH_FILE_NAME + separator + label;
	LOT_LABEL_FILE_NAME_PREFIX	= "lot" + separator + label + separator;
	PACK_LABEL_FILE_NAME_PREFIX	= "pack" +
					  separator +
					  label +
					  separator;
	/*
	 * Date formatter.
	 */
	String pattern = "yyyy-MM-dd HH:mm:ss z";

	DATE_FORMATTER		 = new SimpleDateFormat(pattern);
	/*
	 * Factory names.
	 */
	ECARD_BASE_FACTORY_NAME	 = ECardBaseFactory.class.getName();
	ECARD_BATCH_FACTORY_NAME = ECardBatchFactory.class.getName();
	ECARD_TYPE_FACTORY_NAME	 = ECardTypeFactory.class.getName();
    }

    private File		target;
    private ECardBatch		eCardBatch;
    private ECardIDVerifier	eCardIDVerifier;

    /**
     * Constructs a ECardExporter instance of the given type and count.
     *
     * @param	targetDir		the path of the target directory
     * @param	configFileName		the eCard configuration file name
     *
     * @throws	DBException
     * @throws	IOException
     * @throws	SQLException
     */
    public ECardExporter(String targetDir,
			 String configFileName)
	throws DBException, IOException, SQLException
    {
	super(configFileName);
	this.target = new File(targetDir);
	this.eCardBatch = getNextECardBatchForPrinting();
	this.eCardIDVerifier = new ECardIDVerifier();
    }

    /**
     * Verifies whether it is okay to export the eCards or not.
     *
     * @returns	not null => the rejected reason; null => accepted.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String verify()
	throws DBException, IOException
    {
	/*
	 * Verify the target directory.
	 */
	if (!this.target.exists())
	{
	    /*
	     * The target directory does not exist.
	     */
	    return this.target.getPath() + " does not exist";
	}
	if (!this.target.isDirectory())
	{
	    /*
	     * The target directory is not a directory.
	     */
	    return this.target.getPath() + " is not a directory";
	}
	if (!this.target.canWrite())
	{
	    /*
	     * The target directory is not writable.
	     */
	    return this.target.getPath() + " is not writable";
	}
	/*
	 * Verify the eCard batch.
	 */
	if (this.eCardBatch == null)
	{
	    /*
	     * Nothing to export.
	     */
	    return "there is no eCard batch to export";
	}
	/*
	 * Okay to export.
	 */
	return null;
    }

    /**
     * Gets confirmation from the operator.
     *
     * @throws	IOException
     */
    public boolean isConfirmed()
	throws IOException
    {
	if (isVerbose())
	{
	    long	start = eCardBatch.getStartECardID().longValue();
	    long	end = eCardBatch.getEndECardID().longValue();
	    int		total = (int)(end - start + 1);

	    System.out.println();
	    System.out.print("***************************");
	    System.out.println("***************************");
	    System.out.print("About to export " + total + " eCards");
	    System.out.println("[" + start + ", " + end + "]");
	    System.out.print("***************************");
	    System.out.println("***************************");
	    System.out.print("Confirm [y|n] <n>? ");
	    System.out.flush();

	    int		c = System.in.read();

	    if (c != 'y')
	    {
		System.out.println("Aborted by operator.");
		return false;
	    }
	    System.out.println();
	}
	return true;
    }

    /**
     * Exports the next available batch of eCards.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public void export()
	throws DBException, IOException
    {
	boolean			succeed = false;
	/*
	 * Obtain factories.
	 */
	ECardBaseFactory	eCardBaseFactory;
	ECardBatchFactory	eCardBatchFactory;
	Abort			abort;

	eCardBaseFactory = (ECardBaseFactory)DBAccessFactory.getInstance(
						ECARD_BASE_FACTORY_NAME);
	eCardBatchFactory = (ECardBatchFactory)DBAccessFactory.getInstance(
						ECARD_BATCH_FACTORY_NAME);
	/*
	 * Allow clean up when terminating abnormally.
	 */
	abort = new Abort(eCardBaseFactory);
	Runtime.getRuntime().addShutdownHook(abort);
	/*
	 * Begin the processing.
	 */
	eCardBaseFactory.beginTransaction(dataSource, false, false);
	try
	{
	    long	start = eCardBatch.getStartECardID().longValue();
	    long	end = eCardBatch.getEndECardID().longValue();
	    int		total = (int)(end - start + 1);
	    int		lotCount = total / LOT_UNIT_SIZE;
	    Date	date = new Date();
	    String	today = DATE_FORMATTER.format(date);
	    ECardType	eCardType = getECardType(
					eCardBatch.getECardTypeID().intValue());
	    File	batchFile = new File(target, BATCH_FILE_NAME);
	    PrintWriter	batchWriter = new PrintWriter(
						new FileWriter(batchFile));

	    try
	    {
		/*
		 * Create a batch label file that shows information
		 * of this batch.
		 */
		long	current = start;

		generateBatchLabel(current, total, today, eCardType);
		for (int l = 0; l < lotCount; l++)
		{
		    showState("Lot[" + l + "]" +
			      " eCards[" + current + ", " +
					   (current + LOT_UNIT_SIZE) +
				     "] ",
			      ProgressEvent.STARTING,
			      0);
		    checkForShutDown();
		    /*
		     * Create a lot label file that shows information
		     * of this lot.
		     */
		    generateLotLabel(l, current, today, eCardType);
		    for (int p = 0; p < PACKS_PER_LOT; p++)
		    {
			checkForShutDown();
			/*
			 * Create a pack label file that shows information
			 * of this pack.
			 */
			generatePackLabel(l, p, current, today, eCardType);
			/*
			 * Retrieve a pack of eCards to export.
			 */
			ECardBase[]	eCardBases;

			eCardBases = eCardBaseFactory.getECardBases(
						current,
						current + PACK_UNIT_SIZE,
						super.nameMap);
			if (eCardBases == null)
			{
			    error("No eCards found for [" + current + ", " +
					current + PACK_UNIT_SIZE + "]! " +
				  "Please contact your DBA.");
			    /* NOT REACHED */
			}
			if (eCardBases.length < PACK_UNIT_SIZE)
			{
			    error("Only " + eCardBases +
				    " eCards found for [" + current + ", " +
					current + PACK_UNIT_SIZE + "]! " +
				  "Please contact your DBA.");
			    /* NOT REACHED */
			}
			for (int e = 0; e < PACK_UNIT_SIZE; e++)
			{
			    /*
			     * Export eCard.
			     */
			    ECardBase	eCardBase = eCardBases[e];
			    int		type	= eCardBase.getECardTypeID()
							   .intValue();
			    long	serial	= eCardBase.getECardID()
							   .longValue();
			    long	random	= eCardBase.getRandomNumber()
							   .longValue();

			    batchWriter.println(
				    ECardNumber.encode(type, serial, random) +
				    '\t' +
				    serial);
			}
			current += PACK_UNIT_SIZE;
			showProgress(null,
				     ProgressEvent.IN_PROGRESS,
				     (p + 1)*100/PACKS_PER_LOT);
		    }
		    showState(null, ProgressEvent.IN_PROGRESS, 100);
		}
	    }
	    finally
	    {
		batchWriter.close();
	    }
	    /*
	     * Update the eCard batch record to indicate the print date.
	     */
	    EndECardID		endECardID = new EndECardID(end);
	    ProgressEvent	event;

	    eCardBatch.setPrintDate(date);
	    eCardBatchFactory.update(eCardBatch, endECardID, this.nameMap);
	    /*
	     * Clear the random numbers from the eCards so that it cannot
	     * be generated again.
	     */
	    eCardBaseFactory.clearRandomNumbers(start, end, this.nameMap);
	    /*
	     * Commit.
	     */
	    showState(null, ProgressEvent.COMMITTING, 100);
	    checkForShutDown();
	    eCardBaseFactory.commitTransaction();
	    succeed = true;
	    showState(null, ProgressEvent.COMMITTED, 100);
	}
	finally
	{
	    if (!succeed)
	    {
		eCardBaseFactory.rollbackTransaction();
		/*
		 * Remove files.
		 */
		removeFiles();
		showState(null, ProgressEvent.ABORTED, -1);
	    }
	    Runtime.getRuntime().removeShutdownHook(abort);
	}
    }

    /**
     * Generates the batch label file.
     *
     * @param	start			the starting eCard identifier
     * @param	count			the number of eCards in this batch
     * @param	today			the formatted date of today
     * @param	eCardType		the eCard type instance
     */
    private void generateBatchLabel(long start,
				    int count,
				    String today,
				    ECardType eCardType)
	throws IOException
    {
	File		file = new File(target, BATCH_LABEL_FILE_NAME);
	PrintWriter	writer = new PrintWriter(new FileWriter(file));

	try
	{
	    writer.println("Starting Number:\t" + start);
	    writer.println("Batch Size:\t\t" + count);
	    writer.println("Print Date:\t\t" + today);
	    writer.println("Card Type:\t\t" + eCardType.getDescription());
	}
	finally
	{
	    writer.close();
	}
    }

    /**
     * Generates the lot label file.
     *
     * @param	lot			the lot number
     * @param	start			the starting eCard identifier
     * @param	today			the formatted date of today
     * @param	eCardType		the eCard type instance
     */
    private void generateLotLabel(int lot,
				  long start,
				  String today,
				  ECardType eCardType)
	throws IOException
    {
	File		file = getLotFile(lot);
	PrintWriter	writer = new PrintWriter(new FileWriter(file));

	try
	{
	    writer.println("Starting Number:\t" + start);
	    writer.println("Verification Code:\tL" +
			   eCardIDVerifier.getParity(start / LOT_UNIT_SIZE));
	    writer.println("Lot Size:\t\t" + LOT_UNIT_SIZE);
	    writer.println("Print Date:\t\t" + today);
	    writer.println("Card Type:\t\t" + eCardType.getDescription());
	}
	finally
	{
	    writer.close();
	}
    }

    /**
     * Generates the pack label file.
     *
     * @param	lot			the lot number
     * @param	pack			the pack number
     * @param	start			the starting eCard identifier
     * @param	today			the formatted date of today
     * @param	eCardType		the eCard type instance
     */
    private void generatePackLabel(int lot,
				   int pack,
				   long start,
				   String today,
				   ECardType eCardType)
	throws IOException
    {
	File		file = getPackFile(lot, pack);
	PrintWriter	writer = new PrintWriter(new FileWriter(file));

	try
	{
	    writer.println("Starting Number:\t" + start);
	    writer.println("Verification Code:\tP" +
			   eCardIDVerifier.getParity(start / PACK_UNIT_SIZE));
	    writer.println("Pack Size:\t\t" + PACK_UNIT_SIZE);
	    writer.println("Print Date:\t\t" + today);
	    writer.println("Card Type:\t\t" + eCardType.getDescription());
	}
	finally
	{
	    writer.close();
	}
    }

    /**
     * Returns a lot label File of the given lot number.
     *
     * @returns	The File instance representing the lot label file of the
     *		given lot number.
     */
    private File getLotFile(int lot)
    {
	String	fileName = LOT_LABEL_FILE_NAME_PREFIX + zeroFill(lot, 2);

	return new File(target, fileName);
    }

    /**
     * Returns a pack label File of the given lot number, and pack number.
     *
     * @returns	The File instance representing the pack label file of the
     *		given lot number and pack number.
     */
    private File getPackFile(int lot, int pack)
    {
	String	fileName = PACK_LABEL_FILE_NAME_PREFIX +
			   zeroFill(lot, 2) +
			   pack;

	return new File(target, fileName);
    }

    /**
     * Converts the given number to string of specified size. If it is
     * smaller, fill with leading zeros.
     *
     * @param	number			the number to to converted
     * @param	size			the maximum number of digits
     */
    private String zeroFill(int number, int size)
    {
	StringBuffer	buffer = new StringBuffer(size);
	String		value = String.valueOf(number);
	int		digits = value.length();

	while (digits++ < size)
	{
	    buffer.append('0');
	}
	buffer.append(value);
	return buffer.toString();
    }

    /**
     * Returns the next ECardBatch for printing.
     *
     * @returns	The ECardBatch instance.
     *
     * @throws	DBException
     * @throws	IOException
     */
    private ECardBatch getNextECardBatchForPrinting()
	throws DBException, IOException
    {
	ECardBatchFactory	factory;

	factory = (ECardBatchFactory)DBAccessFactory.getInstance(
						ECARD_BATCH_FACTORY_NAME);
	factory.beginTransaction(dataSource, false, true);
	try
	{
	    return factory.getNextECardBatchForPrinting(super.nameMap);
	}
	finally
	{
	    factory.commitTransaction();
	}
    }

    /**
     * Gets the eCard type. The caller should already be in a transaction
     * when calling this method.
     *
     * @param	eCardTypeID		the eCard type identifier
     *
     * @throws	DBException
     * @throws	IOException
     */
    private ECardType getECardType(int eCardTypeID)
	throws DBException, IOException
    {
	/*
	 * Consult the database and see if this type exists.
	 */
	ECardTypeFactory	factory;

	factory = (ECardTypeFactory)DBAccessFactory.getInstance(
						ECARD_TYPE_FACTORY_NAME);
	return factory.getECardType(eCardTypeID, super.nameMap);
    }

    /**
     * Removes the files just created because of abnormal condition.
     */
    private void removeFiles()
    {
	long	start = eCardBatch.getStartECardID().longValue();
	long	end = eCardBatch.getEndECardID().longValue();
	int	total = (int)(end - start + 1);
	int	lotCount = total / LOT_UNIT_SIZE;

	for (int l = 0; l < lotCount; l++)
	{
	    for (int p = 0; p < PACKS_PER_LOT; p++)
	    {
		/*
		 * Remove pack related file.
		 */
		getPackFile(l, p).delete();
	    }
	    /*
	     * Remove lot related file.
	     */
	    getLotFile(l).delete();
	}
	/*
	 * Remove batch related files.
	 */
	new File(target, BATCH_LABEL_FILE_NAME).delete();
	new File(target, BATCH_FILE_NAME).delete();
    }

    /**
     * Shows the usage of this program and terminate.
     */
    private static final void usage()
    {
	System.err.println("Usage:");
	System.err.println("  java com.broadon.ecard.ECardExporter [options]");
	System.err.println();
	System.err.println("where [options] are:");
	System.err.println("    -d <path>\t\ttarget directory");
	System.err.println("    -p <property file>\tproperty file");
	System.err.println("    -v\t\t\tverbose mode");
	System.err.println("    -q\t\t\tquiet mode");
	System.err.println();
	System.err.println("defaults: -d . -p eCard.properties -v");
	System.exit(-1);
    }

    /**
     * The main driver of the ECardExporter.
     *<pre>
     * Usage:
     *	java com.broadon.ecard.ECardExporter	-d <path>
     *</pre>
     */
    public static final void main(String[] args)
	throws Throwable
    {
	int		argCount = args.length;
	String		eCardPropertyFileName = null;
	String		targetDir = ".";
	boolean		verbose = true;

	/*
	 * Process command line arguments.
	 */
	for (int n = 0; n < argCount; n++)
	{
	    if (args[n].charAt(0) != '-')
	    {
		usage();
		/* NOT REACHED */
	    }
	    switch (args[n].charAt(1))
	    {
	    case 'D':
	    case 'd':
		targetDir = args[++n];
		break;

	    case 'P':
	    case 'p':
		eCardPropertyFileName = args[++n];
		break;

	    case 'Q':
	    case 'q':
		verbose = false;
		break;

	    case 'V':
	    case 'v':
		verbose = true;
		break;

	    case 'H':
	    case 'h':
		usage();
		/* NOT REACHED */

	    default:
		System.err.println("Unknown option " + args[n]);
		usage();
		/* NOT REACHED */
	    }
	}
	/*
	 * Export information of eCards for printing.
	 */
	ECardExporter		eCardExporter;

	eCardExporter = new ECardExporter(targetDir,
					  eCardPropertyFileName);
	if (verbose)
	{
	    eCardExporter.setProgressListener(
			eCardExporter.new DefaultProgressListener());
	}
	/*
	 * Verify.
	 */
	String		reason = eCardExporter.verify();

	if (reason != null)
	{
	    /*
	     * Reject the request for the given reason.
	     */
	    error("Unable to generate eCards because " + reason + ".");
	    /* NOT REACHED */
	}
	/*
	 * Confirm.
	 */
	if (eCardExporter.isConfirmed())
	{
	    /*
	     * Generate eCards.
	     */
	    eCardExporter.export();
	}
    }

    /**
     * The <code>Abort</code> class extends from the class
     * <code>ECardTool.Abort</code> to handle the additional
     * file removal task in case of abnormal terminations.
     */
    protected class Abort
	extends ECardTool.Abort
    {
	private Abort(DBAccessFactory factory)
	{
	    super(factory);
	}

	public void run()
	{
	    super.run();
	    removeFiles();
	}
    }
}

package com.broadon.ecard.db;

import java.io.IOException;
import java.util.Map;

import com.broadon.bean.Bean;
import com.broadon.db.DBException;

/**
 * The <c>KeyUsageInfoFactory</c> class interfaces with the database to
 * create, update, or query records from the ECardBatchs table.
 * Each record is represented by a KeyUsageInfo JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.2 $
 */
public class KeyUsageInfoFactory
    extends ECardBatchFactory
{
    private static final String	SELECT_KEY_USAGE;

    static
    {
	SELECT_KEY_USAGE	=
	    "select sum(end_ecard_id - start_ecard_id + 1) totalECards, " +
		   "min(create_date) start_date from " + TABLE_NAME +
		" where cert_id = ?";
    }

    /**
     * Constructs a KeyUsageInfoFactory instance.
     */
    public KeyUsageInfoFactory()
    {
	super();
    }

    /**
     * Returns the key usage records of the given certificate identifier.
     *
     * @param	certificateID		the certificate identifier
     * @param	nameMap			the name mapping table
     *
     * @returns	The KeyUsageInfo JavaBeans containing all information of
     *		a KeyUsageInfo record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public KeyUsageInfo getKeyUsageInfo(String certificateID, Map nameMap)
	throws DBException, IOException
    {
	CertificateID	predicate = new CertificateID(certificateID);

	return (KeyUsageInfo)queryUnique(predicate, SELECT_KEY_USAGE, nameMap);
    }

    /**
     * Creates a KeyUsageInfo JavaBeans object.
     *
     * @returns	The newly created KeyUsageInfo object.
     */
    protected Bean newBean()
    {
	return new KeyUsageInfo();
    }
}

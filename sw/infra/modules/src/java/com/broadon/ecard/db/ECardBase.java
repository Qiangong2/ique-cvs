package com.broadon.ecard.db;

import com.broadon.sql.PBean;

/**
 * The <c>ECardBase</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *<p>
 * It contains a subset of information about an eCard.
 *
 * @version	$Revision: 1.1 $
 */
public class ECardBase
    extends PBean
{
    private Long		eCardID;
    private Integer		eCardTypeID;
    private Long		randomNumber;

    /**
     * Constructs an empty ECardBase instance.
     */
    public ECardBase()
    {
	this.eCardID = null;
	this.eCardTypeID = null;
	this.randomNumber = null;
    }

    /**
     * Getter for eCardID.
     *
     * @returns	The eCard serial number.
     */
    public Long getECardID()
    {
	return eCardID;
    }

    /**
     * Setter for eCardID.
     *
     * @param	eCardID			the eCard serial number
     */
    public void setECardID(Long eCardID)
    {
	this.eCardID = eCardID;
    }

    /**
     * Getter for eCardTypeID.
     *
     * @returns	The eCard type identifier.
     */
    public Integer getECardTypeID()
    {
	return eCardTypeID;
    }

    /**
     * Setter for eCardTypeID.
     *
     * @param	eCardTypeID		the eCard type identifier
     */
    public void setECardTypeID(Integer eCardTypeID)
    {
	this.eCardTypeID = eCardTypeID;
    }

    /**
     * Getter for randomNumber.
     *
     * @returns	The eCard random number.
     */
    public Long getRandomNumber()
    {
	return randomNumber;
    }

    /**
     * Setter for randomNumber.
     *
     * @param	randomNumber		the eCard random number
     */
    public void setRandomNumber(Long randomNumber)
    {
	this.randomNumber = randomNumber;
    }
}

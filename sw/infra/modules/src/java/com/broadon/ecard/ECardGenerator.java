package com.broadon.ecard;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;

import com.broadon.db.DBAccessFactory;
import com.broadon.db.DBException;
import com.broadon.db.ECardType;
import com.broadon.db.ECardTypeFactory;
import com.broadon.util.ECardNumber;
import com.broadon.ecard.db.ECard;
import com.broadon.ecard.db.ECardFactory;
import com.broadon.ecard.db.ECardBatch;
import com.broadon.ecard.db.ECardBatchFactory;
import com.broadon.ecard.db.KeyUsageInfo;
import com.broadon.ecard.db.KeyUsageInfoFactory;
import com.broadon.hsmserver.HSM;
import com.broadon.hsmserver.HSMException;

/**
 * This <code>ECardGenerator</code> class generates new eCards into the
 * database for future use.
 *
 * @version	$Revision: 1.8 $
 */
public class ECardGenerator
    extends ECardTool
{
    private static final long	ECARD_NUMBER_THRESHOLD	= 4000000L;
    
    // TODO: generate a new cert for production
    //  increased certificate life to 5 years for lab
    private static final long	ECARD_TIME_THRESHOLD	= 5*365*86400*1000L; // 365*86400*1000L;
    
    // Changed starting serial number to 8 digits
    private static final long	STARTING_SERIAL_NUMBER	= 10000000L; // 1262304000L;
    private static final long   MAX_SERIAL_NUMBER       = 99999999L; 
    
    // Limit to random number to to 8 digits
    private static final long   MAX_RANDOM_NUMBER   = 99999999L;
    private static final int	BATCH_UNIT_SIZE		= 10000;
    private static final int	ECARD_TYPE_DIGITS	= 6;
    private static final String	ECARD_BATCH_FACTORY_NAME;
    private static final String	ECARD_FACTORY_NAME;
    private static final String	ECARD_TYPE_FACTORY_NAME;
    private static final String	KEY_USAGE_INFO_FACTORY_NAME;

    static
    {
	/*
	 * Factory names.
	 */
	ECARD_BATCH_FACTORY_NAME    = ECardBatchFactory.class.getName();
	ECARD_FACTORY_NAME	    = ECardFactory.class.getName();
	ECARD_TYPE_FACTORY_NAME	    = ECardTypeFactory.class.getName();
	KEY_USAGE_INFO_FACTORY_NAME = KeyUsageInfoFactory.class.getName();
    }

    private String		eCardType;
    private int			eCardTypeID;
    private int			batchSize;
    private int			batchCount;
    private long		serialNumber;
    private ECardBatch		latestECardBatch;
    private HSM			hsm;
    private String		certificateID;

    /**
     * Constructs a ECardGenerator instance of the given type and count.
     *
     * @param	eCardType		the eCard type
     * @param	batchSize		the batch size
     * @param	batchCount		the number batches to generate
     * @param	configFileName		the eCard configuration file name
     *
     * @throws	DBException
     * @throws	IOException
     * @throws	SQLException
     * @throws	HSMException
     */
    public ECardGenerator(String eCardType,
			  int batchSize,
			  int batchCount,
			  String configFileName)
	throws DBException, IOException, SQLException, HSMException
    {
	super(configFileName);
	this.eCardType = eCardType;
	this.eCardTypeID = Integer.parseInt(eCardType);
	this.batchSize = batchSize;
	this.batchCount = batchCount;
	/*
	 * Find the starting serial number.
	 */
	this.latestECardBatch = getLatestECardBatch();
	this.serialNumber = (latestECardBatch == null)
			    ? STARTING_SERIAL_NUMBER
			    : latestECardBatch.getEndECardID().intValue() + 1;
	/*
	 * Initialize the Security related information.
	 */
	this.hsm = new HSM(super.eCardProperties);
	this.certificateID = hsm.getCertificateID();
    }

    /**
     * Returns the batch size of this generation.
     *
     * @returns	The batch size of this generation.
     */
    public int getBatchSize()
    {
	return batchSize;
    }

    /**
     * Returns the batch count of this generation.
     *
     * @returns	The number of batch to be generated.
     */
    public int getBatchCount()
    {
	return batchCount;
    }

    /**
     * Returns the starting serial number of this generation.
     *
     * @returns	The starting serial number of this generation.
     */
    public long getSerialNumber()
    {
	return serialNumber;
    }

    /**
     * Verifies whether it is okay to generate the eCards or not.
     *
     * @returns	not null => the rejected reason; null => accepted.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public String verify()
	throws DBException, IOException
    {
	long	requested = this.batchSize * this.batchCount;

	if (requested > ECARD_NUMBER_THRESHOLD)
	{
	    /*
	     * Cannot ask for more than the maximum allowed.
	     */
	    return "the requested number of eCards to be generated " +
		   "exceeds the " +
		   ECARD_NUMBER_THRESHOLD +
		   " limit";
	}
	if (this.latestECardBatch == null)
	{
	    /*
	     * The first time, no need to generate any new key.
	     */
	    return null;
	}
	if (!this.latestECardBatch.getCertificateID()
				  .equals(this.certificateID))
	{
	    /*
	     * A new key has been generated, no need to check any further.
	     */
	    return null;
	}
	/*
	 * Gather information.
	 */
	KeyUsageInfo	keyUsageInfo = getKeyUsageInfo(this.certificateID);

	if (keyUsageInfo == null)
	{
	    /*
	     * This key has not been used before.
	     */
	    return null;
	}
	/*
	 * Check for renewal.
	 */
	/*
	 * First, the number of eCards generated.
	 */
	long	total = keyUsageInfo.getTotalECards().intValue() + requested;

	if (total > ECARD_NUMBER_THRESHOLD)
	{
	    /*
	     * It'd have been over the maximum number of eCards created,
	     * generate a new key.
	     */
	    return "the total eCards generated with this key would have been " +
		   total +
		   ", exceeding the " +
		   ECARD_NUMBER_THRESHOLD +
		   " limit";
	}
	/*
	 * Second, the duration of the key being used.
	 *
	 * Assume the dates in the database are created under the same
	 * time zone as where the generator is run.
	 * Even if it is not, there would only be at most one day difference,
	 * an acceptable error.
	 */
	Date	startingDate = keyUsageInfo.getStartDate();
	Date	now = new Date();
	long	duration = now.getTime() - startingDate.getTime();

	if (duration >= ECARD_TIME_THRESHOLD)
	{
	    /*
	     * It's been used for more that the maximum time allowed,
	     * generate a new key.
	     */
	    return "the key has been used for " +
		   duration/(86400 * 1000) +
		   " days";
	}
	/*
	 * Still okay to use the existing key.
	 */
	return null;
    }

    /**
     * Gets confirmation from the operator.
     *
     * @throws	IOException
     */
    public boolean isConfirmed()
	throws IOException
    {
	if (isVerbose())
	{
	    int		total = this.batchCount * this.batchSize;

	    System.out.println();
	    System.out.print("****************************");
	    System.out.println("****************************");
	    System.out.print("About to generate " + total + " eCards");
	    System.out.println("[" + this.serialNumber + ", " +
				     (this.serialNumber + total - 1) + "]");
	    System.out.print("****************************");
	    System.out.println("****************************");
	    System.out.print("Confirm [y|n] <n>? ");
	    System.out.flush();

	    int		c = System.in.read();

	    if (c != 'y')
	    {
		System.out.println("Aborted by operator.");
		return false;
	    }
	    System.out.println();
	}
	return true;
    }

    /**
     * Generates eCards into the database.
     *
     * @throws	DBException
     * @throws	IOException
     * @throws	HSMException
     */
    public void generate()
	throws DBException, IOException, HSMException
    {
	/*
	 * Obtain factories.
	 */
	ECardFactory		eCardFactory;
	ECardBatchFactory	eCardBatchFactory;

	eCardFactory = (ECardFactory)DBAccessFactory.getInstance(
						ECARD_FACTORY_NAME);
	eCardBatchFactory = (ECardBatchFactory)DBAccessFactory.getInstance(
						ECARD_BATCH_FACTORY_NAME);
	/*
	 * Prepare some reusable objects here so that we don't have to
	 * keep creating them.
	 */
	ECardBatch	eCardBatch = new ECardBatch();
	ECard		eCard = new ECard();
	Integer		eCardTypeIDInt = new Integer(this.eCardTypeID);
	Date		today = new Date();
	/*
	 * Allow clean up when terminating abnormally.
	 */
	Abort		abort = new Abort(eCardFactory);

	Runtime.getRuntime().addShutdownHook(abort);
	/*
	 * Start generating, one batch at a time.
	 */
	for (int b = 0; b < batchCount; b++)
	{
	    boolean	succeed = false;
	    long	endNumber = this.serialNumber + batchSize - 1;

	    showState("Batch[" + b + "]" +
		      " eCards[" + this.serialNumber + ", " + endNumber + "] ",
		      ProgressEvent.STARTING,
		      0);
	    /*
	     * Begin the processing for this batch.
	     */
	    eCardFactory.beginTransaction(super.dataSource, false, false);
	    try
	    {
		/*
		 * Initialize the eCardBatch record.
		 */
		eCardBatch.setEndECardID(new Long(endNumber));
		eCardBatch.setStartECardID(new Long(this.serialNumber));
		eCardBatch.setECardTypeID(eCardTypeIDInt);
		eCardBatch.setCertificateID(this.certificateID);
		eCardBatch.setCreateDate(today);
		/*
		 * Insert into the database.
		 */
		eCardBatchFactory.create(eCardBatch, super.nameMap);
		for (int n = 0; n < batchSize; n++)
		{
		    /*
		     * Initialize the eCard record.
		     */
		    ECardSigner	eCardSigner;

		    eCardSigner = new ECardSigner(this.eCardTypeID,
						  this.serialNumber,
						  this.hsm,
						  MAX_RANDOM_NUMBER);
		    eCard.setECardID(new Long(this.serialNumber));
		    eCard.setECardTypeID(eCardTypeIDInt);
                    eCard.setRandomNumber(
                            new Long(eCardSigner.getRandomNumber()));
		    eCard.setECardDigest(eCardSigner.getSignature());
		    /*
		     * Insert into the database.
		     */
		    eCardFactory.create(eCard, super.nameMap);
		    /*
		     * Increase the serial number.
		     */
		    this.serialNumber++;

		    if (isVerbose())
		    {
			/*
			 * Call on every percent increased.
			 */
			float	f = (float)((n + 1)*100.0)/batchSize;
			int	p = (int)f;

			if (p > 0 && f == p)
			{
			    showProgress(null, ProgressEvent.IN_PROGRESS, p);
			}
		    }
		}
		/*
		 * Commit.
		 */
		showState(null, ProgressEvent.COMMITTING, 100);
		checkForShutDown();
		eCardFactory.commitTransaction();
		succeed = true;
		showState(null, ProgressEvent.COMMITTED, 100);
	    }
	    finally
	    {
		if (!succeed)
		{
		    /*
		     * Rollback.
		     */
		    eCardFactory.rollbackTransaction();
		    showState(null, ProgressEvent.ABORTED, -1);
		}
	    }
	}
	Runtime.getRuntime().removeShutdownHook(abort);
    }

    /**
     * Returns the latest ECardBatch instance.
     *
     * @returns	The ECardBatch instance.
     *
     * @throws	DBException
     * @throws	IOException
     */
    private ECardBatch getLatestECardBatch()
	throws DBException, IOException
    {
	ECardBatchFactory	factory;

	factory = (ECardBatchFactory)DBAccessFactory.getInstance(
						ECARD_BATCH_FACTORY_NAME);
	factory.beginTransaction(super.dataSource, false, true);
	try
	{
	    return factory.getLastECardBatch(MAX_SERIAL_NUMBER,super.nameMap);
	}
	finally
	{
	    factory.commitTransaction();
	}
    }

    /**
     * Returns the usage information of the key identified by the given
     * certificate identifier.
     *
     * @returns	The KeyUsageInfo instance.
     *
     * @throws	DBException
     * @throws	IOException
     */
    private KeyUsageInfo getKeyUsageInfo(String certificateID)
	throws DBException, IOException
    {
	KeyUsageInfoFactory	factory;

	factory = (KeyUsageInfoFactory)DBAccessFactory.getInstance(
						KEY_USAGE_INFO_FACTORY_NAME);
	factory.beginTransaction(super.dataSource, false, true);
	try
	{
	    return factory.getKeyUsageInfo(certificateID, super.nameMap);
	}
	finally
	{
	    factory.commitTransaction();
	}
    }

    /**
     * Validates the eCard type.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public boolean isValidType()
	throws DBException, IOException
    {
	/*
	 * Consult the database and see if this type exists.
	 */
	ECardTypeFactory	factory;

	factory = (ECardTypeFactory)DBAccessFactory.getInstance(
						ECARD_TYPE_FACTORY_NAME);
	factory.beginTransaction(super.dataSource, false, true);
	try
	{
	    ECardType		type;

	    type = factory.getECardType(this.eCardTypeID, super.nameMap);
	    if (type == null)
	    {
		/*
		 * Invalid type.
		 */
		return false;
	    }
	    /*
	     * Prepend the leading zeros.
	     */
	    String	id = type.getECardTypeID().toString();
	    int		length = id.length();

	    if (length < ECARD_TYPE_DIGITS)
	    {
		StringBuffer	buffer = new StringBuffer("000000");

		ECardNumber.merge(0, ECARD_TYPE_DIGITS, id, buffer);
		id = buffer.toString();
	    }
	    /*
	     * Compare.
	     */
	    return this.eCardType.equals(id);
	}
	finally
	{
	    factory.commitTransaction();
	}
    }

    /**
     * Shows the usage of this program and terminate.
     */
    private static final void usage()
    {
	System.err.println("Usage:");
	System.err.println("  java com.broadon.ecard.ECardGenerator [options]");
	System.err.println();
	System.err.println("where [options] are:");
	System.err.println("    -t <type>\t\teCard type, with leading 0's");
	System.err.println("    -b <# batches>\tnumber of batches to generate");
	System.err.println("    -s <batch size>\tnumber of eCards per batch");
	System.err.println("    -p <property file>\tproperty file");
	System.err.println("    -v\t\t\tverbose mode");
	System.err.println("    -q\t\t\tquiet mode");
	System.err.println();
	System.err.println("defaults: -b 0 -s 10000 -p eCard.properties -v");
	System.exit(-1);
    }

    /**
     * The main driver of the ECardGenerator.
     *<pre>
     * Usage:
     *	java com.broadon.ecard.ECardGenerator	-t <eCardType>
     *						-s <batchSize>
     *						-b <batchCount>
     *</pre>
     */
    public static final void main(String[] args)
	throws Throwable
    {
	int		argCount = args.length;

	if (argCount <= 0)
	{
	    usage();
	    /* NOT REACHED */
	}

	String		eCardPropertyFileName = null;
	String		eCardType = null;
	int		batchCount = 0;
	int		batchSize = BATCH_UNIT_SIZE;
	boolean		verbose = true;

	/*
	 * Process command line arguments.
	 */
	for (int n = 0; n < argCount; n++)
	{
	    if (args[n].charAt(0) != '-')
	    {
		usage();
		/* NOT REACHED */
	    }
	    switch (args[n].charAt(1))
	    {
	    case 'B':
	    case 'b':
		batchCount = Integer.parseInt(args[++n]);
		break;

	    case 'S':
	    case 's':
		batchSize = Integer.parseInt(args[++n]);
		if ((batchSize/BATCH_UNIT_SIZE)*BATCH_UNIT_SIZE != batchSize)
		{
		    error("Invalid eCard size[" + batchSize + "], " +
			  "must be multiple of " + BATCH_UNIT_SIZE + ".");
		    /* NOT REACHED */
		}
		break;

	    case 'P':
	    case 'p':
		eCardPropertyFileName = args[++n];
		break;

	    case 'T':
	    case 't':
		eCardType = args[++n];
		break;

	    case 'Q':
	    case 'q':
		verbose = false;
		break;

	    case 'V':
	    case 'v':
		verbose = true;
		break;

	    case 'H':
	    case 'h':
		usage();
		/* NOT REACHED */

	    default:
		System.err.println("Unknown option " + args[n]);
		usage();
		/* NOT REACHED */
	    }
	}
	/*
	 * Providing eCard type is mandatory.
	 */
	if (eCardType == null)
	{
	    error("Missing eCard type.");
	    /* NOT REACHED */
	}
	/*
	 * Get ready to generate eCards into the database.
	 */
	ECardGenerator		eCardGenerator;

	eCardGenerator = new ECardGenerator(eCardType,
					    batchSize,
					    batchCount,
					    eCardPropertyFileName);
	if (verbose)
	{
	    eCardGenerator.setProgressListener(
			eCardGenerator.new DefaultProgressListener());
	}
	/*
	 * Validate eCard type.
	 */
	if (!eCardGenerator.isValidType())
	{
	    error("Invalid eCard type[" + eCardType + "].");
	    /* NOT REACHED */
	}
	/*
	 * Verify.
	 */
	String		reason = eCardGenerator.verify();

	if (reason != null)
	{
	    /*
	     * Reject the request for the given reason.
	     */
	    error("Unable to generate eCards because " + reason + ".");
	    /* NOT REACHED */
	}
	/*
	 * Confirm.
	 */
	if (eCardGenerator.isConfirmed())
	{
	    /*
	     * Generate eCards.
	     */
	    eCardGenerator.generate();
	}
    }
}

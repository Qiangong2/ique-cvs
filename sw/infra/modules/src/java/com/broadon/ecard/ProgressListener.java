package com.broadon.ecard;

import java.util.EventListener;

/**
 * The <c>ProgressListener</c> interface is for receiving progress events.
 *
 * @version	$Revision: 1.1 $
 */
public interface ProgressListener
    extends EventListener
{
    /**
     * Invoked when the progress of the source has been changed.
     */
    public void progressChanged(ProgressEvent e);

    /**
     * Invoked when the state of the source has been changed.
     */
    public void stateChanged(ProgressEvent e);
}

package com.broadon.ecard;

import java.util.EventObject;

/**
 * The <c>ProgressEvent</c> class is used to notify interested parties
 * about the progress of the event source.
 *
 * @version	$Revision: 1.1 $
 */
public class ProgressEvent
    extends EventObject
{
    /**
     * Invalid state.
     */
    public static final int	INVALID		= -1;

    /**
     * The starting state.
     */
    public static final int	STARTING	= 0;

    /**
     * The in progress state.
     */
    public static final int	IN_PROGRESS	= 2;

    /**
     * The committing state.
     */
    public static final int	COMMITTING	= 3;

    /**
     * The committed state.
     */
    public static final int	COMMITTED	= 4;

    /**
     * The aborted state.
     */
    public static final int	ABORTED		= 5;

    private String	description;
    private int		state;
    private int		percentage;

    /**
     * Constructs a ProgressEvent instance.
     *
     * @param	source			the source object of this event
     */
    public ProgressEvent(Object source)
    {
	this(source, null, INVALID, 0);
    }

    /**
     * Constructs a ProgressEvent instance.
     *
     * @param	source			the source object of this event
     * @param	description		the description
     * @param	state			the new state
     * @param	percentage		the completed percentage
     */
    public ProgressEvent(Object source,
			 String description,
			 int state,
			 int percentage)
    {
	super(source);
	this.description = description;
	this.state = state;
	this.percentage = percentage;
    }

    /**
     * Returns the description.
     *
     * @returns	The description.
     */
    public String getDescription()
    {
	return description;
    }

    /**
     * Sets the description.
     *
     * @param	description		the description
     */
    public void setDescription(String description)
    {
	this.description = description;
    }

    /**
     * Returns the state.
     *
     * @returns	The state.
     */
    public int getState()
    {
	return state;
    }

    /**
     * Sets the state.
     *
     * @param	state			the new state
     */
    public void setState(int state)
    {
	this.state = state;
    }

    /**
     * Returns the completion percentage.
     *
     * @returns	The completion percentage.
     */
    public int getPercentage()
    {
	return percentage;
    }

    /**
     * Sets the percentage.
     *
     * @param	percentage		the completed percentage
     */
    public void setPercentage(int percentage)
    {
	this.percentage = percentage;
    }
}

package com.broadon.ecard;

import java.io.IOException;
import java.io.FileInputStream;
import java.sql.SQLException;
import java.util.Map;
import java.util.Properties;
import javax.sql.DataSource;

import com.broadon.db.DBAccessFactory;
import com.broadon.db.NameMapManager;
import com.broadon.db.OracleDataSource;

/**
 * The <code>ECardTool</code> class is the top level class for eCard tools.
 * It extracts the common properties into here so that they can be shared.
 *
 * @version	$Revision: 1.5 $
 */
public class ECardTool
{
    /**
     * The number of eCards in a single lot.
     */
    public static final int	LOT_UNIT_SIZE		= 1000;

    /**
     * The number of eCards in a single pack.
     */
    public static final int	PACK_UNIT_SIZE		= 100;

    private static final String	PROPERTY_FILE_NAME	= "eCard.properties";
    private static final String	NAME_MAP_KEY		= "eCard_nameMap";
    private static final String	DB_URL			= "db_url";
    private static final String	DB_USER			= "db_user";
    private static final String	DB_PASSWORD		= "db_password";

    protected DataSource	dataSource;
    protected Map		nameMap;
    protected Properties	eCardProperties;

    private ProgressListener	progressListener;
    private ProgressEvent	progressEvent;
    private boolean		shuttingDown;

    /**
     * Constructs a ECardTool instance.
     *
     * @param	configFileName		the eCard configuration file name
     *
     * @throws	IOException
     * @throws	SQLException
     */
    public ECardTool(String configFileName)
	throws IOException, SQLException
    {
	/*
	 * Get configuration information.
	 */
	this.eCardProperties = new Properties();
	if (configFileName == null)
	{
	    configFileName = PROPERTY_FILE_NAME;
	}
	this.eCardProperties.load(new FileInputStream(configFileName));
	/*
	 * Connect to database.
	 */
	String		url = eCardProperties.getProperty(DB_URL);
	String		user = eCardProperties.getProperty(DB_USER);
	String		password = eCardProperties.getProperty(DB_PASSWORD);

	this.dataSource = OracleDataSource.createDataSource(user,
							    password,
							    url,
							    user);
	if (this.dataSource == null)
	{
	    error("Unable to connect to the database.");
	    /* NOT REACHED */
	}
	/*
	 * Get name mapping information.
	 */
	String		fileName = eCardProperties.getProperty(NAME_MAP_KEY);
	NameMapManager	nameMapManager = new NameMapManager(fileName);

	this.nameMap = nameMapManager.getNameMap();
	this.progressListener = null;
	this.progressEvent = null;
	this.shuttingDown = false;
    }

    /**
     * Sets the progress listener. A non-null progress listener also
     * indicates verbose mode.
     *
     * @param	progressListener	the progress listener
     */
    public void setProgressListener(ProgressListener progressListener)
    {
	this.progressListener = progressListener;
	if (this.progressListener != null)
	{
	    this.progressEvent = new ProgressEvent(this);
	}
    }

    /**
     * Returns whether it is in verbose mode or not.
     *
     * @returns	true => show progress; false => quiet
     */
    public boolean isVerbose()
    {
	return progressListener != null;
    }

    /**
     * Shows the change of progress.
     *
     * @param	description		the description
     * @param	state			the new state
     * @param	percentage		the completed percentage
     */
    protected void showProgress(String description, int state, int percentage)
    {
	if (progressListener != null)
	{
	    if (description != null)
	    {
		progressEvent.setDescription(description);
	    }
	    progressEvent.setState(state);
	    progressEvent.setPercentage(percentage);
	    progressListener.progressChanged(progressEvent);
	}
    }

    /**
     * Shows the change of state.
     *
     * @param	description		the description
     * @param	state			the new state
     * @param	percentage		the completed percentage
     */
    protected void showState(String description, int state, int percentage)
    {
	if (progressListener != null)
	{
	    if (description != null)
	    {
		progressEvent.setDescription(description);
	    }
	    progressEvent.setState(state);
	    if (percentage > 0)
	    {
		progressEvent.setPercentage(percentage);
	    }
	    progressListener.stateChanged(progressEvent);
	}
    }

    /**
     * Checks and see if shut down is in progress. If it is, just wait
     * here to be terminated.
     */
    protected void checkForShutDown()
    {
	/*
	 * Setting and getting quantity of 4-byte or less is atomic
	 * already. There is no need to synchronize it.
	 */
	if (this.shuttingDown)
	{
	    synchronized (this)
	    {
		/*
		 * Wake up the shutdown thread.
		 */
		notify();
		/*
		 * Wait to be termianted.
		 */
		try
		{
		    wait();
		}
		catch (InterruptedException ie)
		{
		}
	    }
	}
    }

    /**
     * Indicates that shut down is in progress.
     */
    private void shutDown()
    {
	synchronized (this)
	{
	    this.shuttingDown = true;
	    /*
	     * Wait for the normal thread to sync up.
	     */
	    try
	    {
		wait(5);
	    }
	    catch (InterruptedException ie)
	    {
	    }
	}
    }

    /**
     * Shows the error message and terminate.
     *
     * @param	message			the error message
     */
    protected static final void error(String message)
    {
	System.err.println("Error: " + message);
	System.exit(-1);
    }

    /**
     * The <code>Abort</code> class handles abnormal terminations
     * when running within critical sections.
     *<p>
     * It could be activated by calling the addShutdownHook method from
     * Runtime by passing an instance of this class. The removeShutdownhook
     * method would be call to deactivate.
     */
    protected class Abort
	extends Thread
    {
	private DBAccessFactory	factory;

	/**
	 * Constructs an Abort instance.
	 *
	 * @param	factory		for rolling back the active transaction
	 */
	protected Abort(DBAccessFactory factory)
	{
	    this.factory = factory;
	}

	public void run()
	{
	    shutDown();
	    try
	    {
		factory.rollbackTransaction();
	    }
	    catch (Throwable t)
	    {
		/*
		 * Ignore errors.
		 */
	    }
	    finally
	    {
		showState(null, ProgressEvent.ABORTED, -1);
	    }
	}
    }

    /**
     * This is the default implementation of the ProgressListener.
     */
    protected class DefaultProgressListener
	implements ProgressListener
    {
	/**
	 * Invoked when the progress of the source has been changed.
	 */
	public void progressChanged(ProgressEvent e)
	{
	    if ((e.getPercentage() % 10) == 0)
	    {
		/*
		 * Show progress on every 10%.
		 */
		System.out.print('.');
		System.out.flush();
	    }
	}

	/**
	 * Invoked when the state of the source has been changed.
	 */
	public void stateChanged(ProgressEvent e)
	{
	    switch (e.getState())
	    {
	    case ProgressEvent.STARTING:
		System.out.print(e.getDescription());
		System.out.flush();
		break;

	    case ProgressEvent.IN_PROGRESS:
		if (e.getPercentage() == 100)
		    System.out.println();
		break;

	    case ProgressEvent.COMMITTING:
		System.out.print(" Committing...");
		System.out.flush();
		break;

	    case ProgressEvent.COMMITTED:
		System.out.println(" Committed.");
		break;

	    case ProgressEvent.ABORTED:
		System.out.println(" Aborted!");
		break;
	    }
	}
    }
}

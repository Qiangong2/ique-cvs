package com.broadon.ecard.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <c>ECardBatch</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *<p>
 * It contains information about an eCard type.
 *
 * @version	$Revision: 1.4 $
 */
public class ECardBatch
    extends PBean
{
    private Long		endECardID;
    private Long		startECardID;
    private Integer		eCardTypeID;
    private String		certificateID;
    private Date		createDate;
    private Date		expireDate;
    private Date		printDate;
    private Date		shipDate;
    private Date		uploadDate;

    /**
     * Constructs an empty ECardBatch instance.
     */
    public ECardBatch()
    {
	this.endECardID = null;
	this.startECardID = null;
	this.eCardTypeID = null;
	this.certificateID = null;
	this.createDate = null;
	this.expireDate = null;
	this.printDate = null;
	this.shipDate = null;
	this.uploadDate = null;
    }

    /**
     * Getter for endECardID.
     *
     * @returns	The serial number of the last eCard of this batch.
     */
    public Long getEndECardID()
    {
	return endECardID;
    }

    /**
     * Setter for endECardID.
     *
     * @param	endECardID		the serial number of the last eCard
     *					of this batch
     */
    public void setEndECardID(Long endECardID)
    {
	this.endECardID = endECardID;
    }

    /**
     * Getter for startECardID.
     *
     * @returns	The serial number of the first eCard of this batch.
     */
    public Long getStartECardID()
    {
	return startECardID;
    }

    /**
     * Setter for startECardID.
     *
     * @param	startECardID		the serial number of the first eCard
     *					of this batch
     */
    public void setStartECardID(Long startECardID)
    {
	this.startECardID = startECardID;
    }

    /**
     * Getter for eCardTypeID.
     *
     * @returns	The eCard type identifier.
     */
    public Integer getECardTypeID()
    {
	return eCardTypeID;
    }

    /**
     * Setter for eCardTypeID.
     *
     * @param	eCardTypeID		the eCard type identifier
     */
    public void setECardTypeID(Integer eCardTypeID)
    {
	this.eCardTypeID = eCardTypeID;
    }

    /**
     * Getter for certificateID.
     *
     * @returns	The certificate identifier.
     */
    public String getCertificateID()
    {
	return certificateID;
    }

    /**
     * Setter for certificateID.
     *
     * @param	certificateID		the certificate identifier
     */
    public void setCertificateID(String certificateID)
    {
	this.certificateID = certificateID;
    }

    /**
     * Getter for createDate.
     *
     * @returns	The date of creation.
     */
    public Date getCreateDate()
    {
	return createDate;
    }

    /**
     * Setter for createDate.
     *
     * @param	createDate		the date of creation
     */
    public void setCreateDate(Date createDate)
    {
	this.createDate = createDate;
    }

    /**
     * Getter for expireDate.
     *
     * @returns	The date of expiration.
     */
    public Date getExpireDate()
    {
	return expireDate;
    }

    /**
     * Setter for expireDate.
     *
     * @param	expireDate		the date of expiration
     */
    public void setExpireDate(Date expireDate)
    {
	this.expireDate = expireDate;
    }

    /**
     * Getter for printDate.
     *
     * @returns	The date sent to printer.
     */
    public Date getPrintDate()
    {
	return printDate;
    }

    /**
     * Setter for printDate.
     *
     * @param	printDate		the date sent to printer
     */
    public void setPrintDate(Date printDate)
    {
	this.printDate = printDate;
    }

    /**
     * Getter for shipDate.
     *
     * @returns	The date shipped to distributors.
     */
    public Date getShipDate()
    {
	return shipDate;
    }

    /**
     * Setter for shipDate.
     *
     * @param	shipDate		the date shipped to distributors
     */
    public void setShipDate(Date shipDate)
    {
	this.shipDate = shipDate;
    }

    /**
     * Getter for uploadDate.
     *
     * @returns	The date uploaded to iQue ops center.
     */
    public Date getUploadDate()
    {
	return uploadDate;
    }

    /**
     * Setter for uploadDate.
     *
     * @param	uploadDate		the date uploaded to iQue ops center
     */
    public void setUploadDate(Date uploadDate)
    {
	this.uploadDate = uploadDate;
    }
}

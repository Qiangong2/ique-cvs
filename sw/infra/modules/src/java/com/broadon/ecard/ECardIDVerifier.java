package com.broadon.ecard;

import com.broadon.util.RSCoder;

/**
 * The <code>ECardIDVerifier</code> class provides the mechanism to generate
 * a parity code for a given eCard serial number. This parity code, can be
 * used to verify whether a given eCard serial number is valid or not with
 * a very high percentage of accuracy.
 *
 * @version	$Revision: 1.1 $
 */
public class ECardIDVerifier
    extends RSCoder
{
    private static final long	MAX_NUMBER	= 0xfffffffffl;
    private static final byte	BEEP		= 0x7;

    private int[]		codes;

    /**
     * Constructs an ECardIDVerifier instance.
     */
    public ECardIDVerifier()
    {
	super(4, 3, new int[] { 1, 1, 0, 0, 1 });
	this.codes = new int[super.n];
    }

    /**
     * Computes the parity code based on the given input. The input number
     * cannot have more than 9 hex digits (0xfffffffff == 6871947673).
     *
     * @param	input			the input number
     */
    public int getParity(long input)
    {
	return (int)nibble2Long(super.encode(long2Nibble(input)));
    }

    /**
     * Computes the parity code based on the given input.
     *
     * @param	input			the String representation of the number
     *
     * @returns	The parity code.
     */
    public int getParity(String input)
    {
	return (int)nibble2Long(super.encode(string2Nibble(input)));
    }

    /**
     * Attempts to correct the target using the given parity code.
     *
     * @param	target			the number to be verified
     * @param	parity			the parity code
     *
     * @returns	The long representation of the corrected number.
     */
    public long correct(long target, int parity)
    {
	long2Nibble(parity, codes, super.k, super.n);
	long2Nibble(target, codes, 0, super.k);
	return nibble2Long(super.decode(codes));
    }

    /**
     * Attempts to correct the target using the given parity code.
     *
     * @param	target			the String representation of the number
     *					to be verified
     * @param	parity			the parity code
     *
     * @returns	The String representation of the corrected number.
     */
    public String correct(String target, int parity)
    {
	long2Nibble(parity, codes, super.k, super.n);
	string2Nibble(target, codes, 0, super.k);
	return nibble2String(super.decode(codes));
    }

    /**
     * Verifies the target using the parity code.
     *
     * @param	target			the number to be verified
     * @param	parity			the parity code
     *
     * @returns	true => correct; false => incorrect
     */
    public boolean verify(long target, int parity)
    {
	long2Nibble(parity, codes, super.k, super.n);
	long2Nibble(target, codes, 0, super.k);
	return super.verify(codes);
    }

    /**
     * Verifies the target together with the parity code.
     *
     * @param	target			the String representation of the number
     *					to be verified
     * @param	parity			the parity code
     *
     * @returns	true => correct; false => incorrect
     */
    public boolean verify(String target, int parity)
    {
	long2Nibble(parity, codes, super.k, super.n);
	string2Nibble(target, codes, 0, super.k);
	return super.verify(codes);
    }

    /**
     * Converts from long to array of nibbles.
     *
     * @param	input			the number
     *
     * @returns	The nibble array.
     */
    private int[] long2Nibble(long input)
    {
	return long2Nibble(input, new int[super.k], 0, super.k);
    }

    /**
     * Converts from long to array of nibbles.
     *
     * @param	input			the number
     * @param	output			the array of nibbles
     * @param	from			the starting index (inclusive)
     * @param	to			the ending index (exclusive)
     *
     * @returns	The nibble array, output.
     */
    private int[] long2Nibble(long input, int[] output, int from, int to)
    {
	int	i = to;

	while (input > 0)
	{
	    output[--i] = (int)(input & 0xf);
	    input >>>= 4;
	}
	while (i > from)
	{
	    output[--i] = 0;
	}
	return output;
    }

    /**
     * Converts from array of nibbles to long.
     *
     * @param	input			the array of nibbles
     *
     * @returns	The long representation of the nibble array.
     */
    private long nibble2Long(int[] input)
    {
	if (input == null)
	{
	    return -1;
	}

	int	size = input.length;
	long	output = 0;

	for (int i = 0; i < size; i++)
	{
	    output <<= 4;
	    output += input[i] & 0xf;
	}
	return output;
    }

    /**
     * Converts from String to array of nibbles.
     *
     * @param	input			the String representation of the number
     *
     * @returns	The nibble array.
     */
    private int[] string2Nibble(String input)
    {
	return string2Nibble(input, new int[super.k], 0, super.k);
    }

    /**
     * Converts from String to array of nibbles.
     *
     * @param	input			the String representation of the number
     * @param	output			the array of nibbles
     * @param	from			the starting index (inclusive)
     * @param	to			the ending index (exclusive)
     *
     * @returns	The nibble array, output.
     */
    private int[] string2Nibble(String input, int[] output, int from, int to)
    {
	int	size = input.length();
	int	first = to - size;

	for (int i = first, j = 0; j < size; i++, j++)
	{
	    int	ch = input.charAt(j);

	    output[i] = ch - 0x30;
	}
	while (first > from)
	{
	    output[--first] = 0;
	}
	return output;
    }

    /**
     * Converts from array of nibbles to String.
     *
     * @param	input			the array of nibbles
     *
     * @returns	The String representation of the nibble array.
     */
    private String nibble2String(int[] input)
    {
	if (input == null)
	{
	    return null;
	}
	/*
	 * Convert.
	 */
	int		size = input.length;
	StringBuffer	buffer = new StringBuffer(size);

	for (int i = 0; i < size; i++)
	{
	    buffer.append(input[i]);
	}
	return buffer.toString();
    }

    /**
     * Shows the array of nibbles.
     *
     * @param	input			the array of nibbles
     */
    private static void show(int[] input)
    {
	if (input == null)
	{
	    System.out.println("No data");
	    return;
	}
	/*
	 * Show.
	 */
	int	size = input.length;

	for (int i = 0; i < size; i++)
	{
	    if (i > 0)
		System.out.print(", ");
	    System.out.print(input[i]);
	}
	System.out.println();
    }

    /**
     * Prints the incorrect message.
     */
    private static void inCorrect()
    {
	/*
	 * Beep to alert the user.
	 */
	for (int n = 0; n < 10; n++)
	{
	    System.out.write(BEEP);
	}
	System.out.println("NOT CORRECT!!");
	System.exit(-1);
    }

    /**
     * Verifies the eCard serial number.
     *
     * @param	args
     *<pre>
     *			args[0]		the eCard serial number
     *			args[1]		the verification code (parity code)
     *</pre>
     */
    public static final void main(String[] args)
	throws Throwable
    {
	if (args.length != 2)
	{
	    System.err.println("Usage:");
	    System.err.print("  java com.broadon.ecard.ECardIDVerifier ");
	    System.err.println("<eCard number> <verification code>");
	    System.exit(-1);
	    /* NOT REACHED */
	}

	ECardIDVerifier	verifier = new ECardIDVerifier();

	if (args[0].charAt(0) == '-' && args[0].charAt(1) == 'X')
	{
	    /*
	     * An undocumented way to compute the parity code.
	     */
	    long	source = Long.parseLong(args[1]);

	    if (source > MAX_NUMBER)
	    {
		System.err.println("Number " + source + " too big.");
		System.exit(-1);
		/* NOT REACHED */
	    }

	    System.out.println("Parity[" + verifier.getParity(source) + "]");
	}
	else
	{
	    /*
	     * Process the input.
	     */
	    long	source = Long.parseLong(args[0]);

	    if (source > MAX_NUMBER)
	    {
		System.err.println("Number " + source + " too big.");
		System.exit(-1);
	    }

	    String	code = args[1];
	    char	type = code.charAt(0);
	    int		parity = 0;

	    switch (type)
	    {
	    case 'L':
	    case 'l':
		/*
		 * A number that starts a lot.
		 */
		long	lotNumber = source / ECardTool.LOT_UNIT_SIZE;

		if ((lotNumber * ECardTool.LOT_UNIT_SIZE) != source)
		{
		    /*
		     * The lot number must be divisible by the lot size.
		     */
		    inCorrect();
		    /* NOT REACHED */
		}
		source = lotNumber;
		parity = Integer.parseInt(code.substring(1));
		break;

	    case 'P':
	    case 'p':
		/*
		 * A number that starts a pack.
		 */
		long	packNumber = source / ECardTool.PACK_UNIT_SIZE;

		if ((packNumber * ECardTool.PACK_UNIT_SIZE) != source)
		{
		    /*
		     * The pack number must be divisible by the pack size.
		     */
		    inCorrect();
		    /* NOT REACHED */
		}
		source = packNumber;
		parity = Integer.parseInt(code.substring(1));
		break;

	    case '0':
	    case '1':
	    case '2':
	    case '3':
	    case '4':
	    case '5':
	    case '6':
	    case '7':
	    case '8':
	    case '9':
		/*
		 * An individual number.
		 */
		parity = Integer.parseInt(code);
		break;

	    default:
		/*
		 * Invalid verification code.
		 */
		System.err.println("Verification Code " + code + " invalid.");
		System.exit(-1);
		/* NOT REACHED */
	    }
	    /*
	     * Verify.
	     */
	    if (verifier.verify(source, parity))
	    {
		System.out.println("Correct.");
	    }
	    else
	    {
		inCorrect();
		/* NOT REACHED */
	    }
	}
    }
}

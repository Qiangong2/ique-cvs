package com.broadon.ecard.db;

import com.broadon.sql.PredicateItem;

/**
 * The <c>EndECardID</c> class conforms to the JavaBeans property,
 * and is a Predicate that finds various records.
 *
 * @version	$Revision: 1.1 $
 */
public class EndECardID
    extends PredicateItem
{
    private Long	endECardID;

    /**
     * Constructs a EndECardID instance.
     *
     * @param	endECardID		the endECard identifier
     */
    public EndECardID(long endECardID)
    {
	super();
	this.endECardID = new Long(endECardID);
    }

    /**
     * Getter for endECardID.
     *
     * @return	The endECard identifier.
     */
    public Long getEndECardID()
    {
	return endECardID;
    }

    /**
     * Setter for endECardID.
     *
     * @param	endECardID		the endECard identifier
     */
    public void setEndECardID(Long endECardID)
    {
	this.endECardID = endECardID;
    }
}

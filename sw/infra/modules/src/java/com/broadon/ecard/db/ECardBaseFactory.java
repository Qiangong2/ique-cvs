package com.broadon.ecard.db;

import java.io.IOException;
import java.util.Map;

import com.broadon.bean.Bean;
import com.broadon.db.DBAccessFactory;
import com.broadon.db.DBException;

/**
 * The <c>ECardBaseFactory</c> class interfaces with the database to
 * create, update, or query records from the ECards table.
 * Each record is represented by a ECardBase JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.1 $
 */
public class ECardBaseFactory
    extends ECardFactory
{
    /**
     * Constructs a ECardBaseFactory instance.
     */
    public ECardBaseFactory()
    {
	super();
    }

    /**
     * Returns the eCards that are within the range of the given eCardIDs.
     *
     * @param	startECardID		the starting eCard identifier
     * @param	endECardID		the ending eCard identifier
     * @param	nameMap			the name mapping table
     *
     * @returns	The array of ECard JavaBeans containing all information of
     *		the ECard records.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public ECardBase[] getECardBases(long startECardID,
				     long endECardID,
				     Map nameMap)
	throws DBException, IOException
    {
	/*
	 * Set up the predicate.
	 */
	ECardID		predicate = new ECardID(">=", startECardID);

	predicate.and(new ECardID("<=", endECardID));
	/*
	 * Query.
	 */
	Bean[]		beans = query(predicate, nameMap);

	if (beans == null)
	{
	    /*
	     * None found.
	     */
	    return null;
	}
	/*
	 * Convert to eCard type.
	 */
	int		count = beans.length;
	ECardBase[]	eCardBases = new ECardBase[count];

	for (int n = 0; n < count; n++)
	{
	    eCardBases[n] = (ECardBase)beans[n];
	}
	return eCardBases;
    }

    /**
     * Returns the select part of the query.
     */
    protected String getSelectPart()
    {
	return "select ecard_id, ecard_type, random_num from " + getTableName();
    }

    /**
     * Creates a ECard JavaBeans object.
     *
     * @returns	The newly created ECard object.
     */
    protected Bean newBean()
    {
	return new ECardBase();
    }
}

package com.broadon.ecard.db;

import java.io.IOException;
import java .sql.PreparedStatement;
import java.util.Map;

import com.broadon.bean.Bean;
import com.broadon.db.DBAccessFactory;
import com.broadon.db.DBException;
import com.broadon.db.ECardTypeID;

/**
 * The <c>ECardBatchFactory</c> class interfaces with the database to
 * create, update, or query records from the ECardBatchs table.
 * Each record is represented by a ECardBatch JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.2 $
 */
public class ECardBatchFactory
    extends DBAccessFactory
{
    protected static final String	TABLE_NAME;
    private static final String		SELECT_LATEST;
    private static final String		SELECT_NEXT;
    private static final String		SELECT_NEXT_WITH_TYPE;

    static
    {
	TABLE_NAME		= "ecard_batches";
  	SELECT_LATEST   =
	    "select * from " + TABLE_NAME + " where end_ecard_id = " +
		"(select max(end_ecard_id) from " + TABLE_NAME + ")";
	SELECT_NEXT		=
	    "select * from " + TABLE_NAME + " where end_ecard_id = " +
		"(select min(end_ecard_id) from " + TABLE_NAME +
		    " where print_date is null)";
	SELECT_NEXT_WITH_TYPE	=
	    "select * from " + TABLE_NAME + " where end_ecard_id = " +
		"(select min(end_ecard_id) from " + TABLE_NAME +
		    " where print_date is null and ecard_type = ?)";
    }

    /**
     * Constructs a ECardBatchFactory instance.
     */
    public ECardBatchFactory()
    {
	super();
    }

    /**
     * Returns the eCard batch information, based on the given eCardBatchID.
     *
     * @param	eCardID			the eCard identifier
     * @param	nameMap			the name mapping table
     *
     * @returns	The ECardBatch JavaBeans containing all information of
     *		a ECardBatch record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public ECardBatch getECardBatch(int eCardID, Map nameMap)
	throws DBException, IOException
    {
	return (ECardBatch)queryUnique(new ECardID(eCardID), nameMap);
    }

    /**
     * Returns the most recent eCard batch record.
     *
     * @param	nameMap			the name mapping table
     *
     * @returns	The ECardBatch JavaBeans containing all information of
     *		a ECardBatch record.
     *
     * @throws	DBException
     * @throws	IOException
     */ 
    public ECardBatch getLastECardBatch(long maxID, Map nameMap)
	throws DBException, IOException
    {
    	String SELECT_LATEST =
    	    "select * from " + TABLE_NAME + " where end_ecard_id = " +
    		"(select max(end_ecard_id) from " + TABLE_NAME + " where end_ecard_id <= " + maxID + ")";
    	return (ECardBatch)queryUnique(null, SELECT_LATEST, nameMap);
    }

    /**
     * Returns the next eCard batch record that has not be printed yet.
     *
     * @param	nameMap			the name mapping table
     *
     * @returns	The ECardBatch JavaBeans containing all information of
     *		a ECardBatch record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public ECardBatch getNextECardBatchForPrinting(Map nameMap)
	throws DBException, IOException
    {
	return (ECardBatch)queryUnique(null, SELECT_NEXT, nameMap);
    }

    /**
     * Returns the next eCard batch record of a specific type that has not
     * be printed yet.
     *
     * @param	eCardTypeID		the eCard type identifier
     * @param	nameMap			the name mapping table
     *
     * @returns	The ECardBatch JavaBeans containing all information of
     *		a ECardBatch record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public ECardBatch getNextECardBatchForPrinting(int eCardTypeID, Map nameMap)
	throws DBException, IOException
    {
	return (ECardBatch)queryUnique(new ECardTypeID(eCardTypeID),
				       SELECT_NEXT_WITH_TYPE,
				       nameMap);
    }

    /**
     * Returns the database table name to be used.
     *
     * @returns	The data table name.
     */
    public String getTableName()
    {
	return TABLE_NAME;
    }

    /**
     * Creates a ECardBatch JavaBeans object.
     *
     * @returns	The newly created ECardBatch object.
     */
    protected Bean newBean()
    {
	return new ECardBatch();
    }
}

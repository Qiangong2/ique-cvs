package com.broadon.ecard.db;

import com.broadon.sql.PredicateItem;

/**
 * The <c>ECardID</c> class conforms to the JavaBeans property,
 * and is a Predicate that finds various records.
 *
 * @version	$Revision: 1.1 $
 */
public class ECardID
    extends PredicateItem
{
    private Long	eCardID;

    /**
     * Constructs a ECardID instance.
     *
     * @param	eCardID			the eCard identifier
     */
    public ECardID(long eCardID)
    {
	super();
	this.eCardID = new Long(eCardID);
    }

    /**
     * Constructs a ECardID instance.
     *
     * @param	arOp			the arithmetic operator
     * @param	eCardID			the eCard identifier
     */
    public ECardID(String arOp, long eCardID)
    {
	super(arOp);
	this.eCardID = new Long(eCardID);
    }

    /**
     * Getter for eCardID.
     *
     * @returns	The eCard identifier.
     */
    public Long getECardID()
    {
	return eCardID;
    }

    /**
     * Setter for eCardID.
     *
     * @param	eCardID			the eCard identifier
     */
    public void setECardID(Long eCardID)
    {
	this.eCardID = eCardID;
    }
}

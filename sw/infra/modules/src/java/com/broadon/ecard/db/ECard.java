package com.broadon.ecard.db;

import java.util.Date;

import com.broadon.sql.PBean;

/**
 * The <c>ECard</c> class conforms to the JavaBeans property, and
 * is mainly used for data transformation.
 *<p>
 * It contains information about an eCard.
 *
 * @version	$Revision: 1.3 $
 */
public class ECard
    extends ECardBase
{
    private byte[]		eCardDigest;

    /**
     * Constructs an empty ECard instance.
     */
    public ECard()
    {
	super();
	this.eCardDigest = null;
    }

    /**
     * Getter for eCardDigest.
     *
     * @returns	The eCard digest (finger print).
     */
    public byte[] getECardDigest()
    {
	return eCardDigest;
    }

    /**
     * Setter for eCardDigest.
     *
     * @param	eCardDigest		the eCard digest (finger print)
     */
    public void setECardDigest(byte[] eCardDigest)
    {
	this.eCardDigest = eCardDigest;
    }
}

package com.broadon.ecard.db;

import java.io.IOException;
import java.util.Map;

import com.broadon.bean.Bean;
import com.broadon.db.DBAccessFactory;
import com.broadon.db.DBException;

/**
 * The <c>ECardFactory</c> class interfaces with the database to
 * create, update, or query records from the ECards table.
 * Each record is represented by a ECard JavaBeans instance.
 *<p>
 * There is only a singleton instance for this class.
 *
 * @version	$Revision: 1.2 $
 */
public class ECardFactory
    extends DBAccessFactory
{
    protected static final String	TABLE_NAME;

    private static final String		SET_RANDOM_NUMBER_NULL;

    static
    {
	TABLE_NAME		= "ecards";
	SET_RANDOM_NUMBER_NULL	= "random_num = null";
    }

    /**
     * Constructs a ECardFactory instance.
     */
    public ECardFactory()
    {
	super();
    }

    /**
     * Clears the random number attribute of the eCards within the
     * given range, inclusive.
     *
     * @param	from			the starting eCard identifier
     * @param	to			the ending eCard idnetifier
     * @param	nameMap			the name mapping table
     *
     * @throws	DBException
     * @throws	IOException
     */
    public void clearRandomNumbers(long from, long to, Map nameMap)
	throws DBException, IOException
    {
	ECardID		predicate = new ECardID(">=", from);

	predicate.and(new ECardID("<=", to));
	update(SET_RANDOM_NUMBER_NULL, predicate, nameMap);
    }

    /**
     * Returns the eCard information, based on the given eCardID.
     *
     * @param	eCardID			the eCard identifier
     * @param	nameMap			the name mapping table
     *
     * @returns	The ECard JavaBeans containing all information of
     *		a ECard record.
     *
     * @throws	DBException
     * @throws	IOException
     */
    public ECard getECard(long eCardID, Map nameMap)
	throws DBException, IOException
    {
	ECardID		predicate = new ECardID(eCardID);

	return (ECard)queryUnique(predicate, nameMap);
    }

    /**
     * Returns the database table name to be used.
     *
     * @returns	The data table name.
     */
    public String getTableName()
    {
	return TABLE_NAME;
    }

    /**
     * Creates a ECard JavaBeans object.
     *
     * @returns	The newly created ECard object.
     */
    protected Bean newBean()
    {
	return new ECard();
    }
}

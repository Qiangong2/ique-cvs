package com.broadon.ecard;

import java.math.BigInteger;
import java.util.Date;
import java.util.Random;
import java.security.MessageDigest;

import com.broadon.hsmserver.HSM;
import com.broadon.hsmserver.HSMException;
import com.broadon.util.ECardNumber;


import java.util.*;
import java.io.*;

/**
 * This <code>ECardSigner</code> class produces the secure portion of
 * the eCard.
 *
 * @version	$Revision: 1.9 $
 */
public class ECardSigner
{
    private int			eCardType;
    private long		serialNumber;
    private long		randomNumber;
    private byte[]		signature;

    private static MessageDigest md;

    static {
	try {
	    md = MessageDigest.getInstance("MD5");
	} catch (java.security.NoSuchAlgorithmException e) {
	    md = null;
	}
    }

    /**
     * Constructs a ECardSigner instance of the given type and serial number.
     *
     * @param	eCardType		the type of this eCard
     * @param	serialNumber		the 10-digit serial number of this eCard
     * @param	hsm			the Hardware Security Module to be used
     */
    public ECardSigner(int eCardType, long serialNumber, HSM hsm, long maxRandomNumber)
	throws HSMException, UnsupportedEncodingException
    {
	this.eCardType = eCardType;
	this.serialNumber = serialNumber;
	randomNumber = new BigInteger(1, hsm.getRandom(4)).longValue();
	randomNumber = randomNumber % (maxRandomNumber+1); 
	String randString =
	    ECardNumber.extractRandom(ECardNumber.encode(eCardType,
							 serialNumber,
							 randomNumber));
	byte[] digest = md.digest(randString.getBytes("US-ASCII"));
	byte[] hash = ECardNumber.digest(eCardType, serialNumber,
					 new BigInteger(1, digest));
	byte[] sig = hsm.sign(hash).toByteArray();
	if (sig.length == hsm.getSignatureLength())
	    signature = sig;
	else {
	    byte[] newSig = new byte[hsm.getSignatureLength()];
	    if (newSig.length < sig.length) {
		System.arraycopy(sig, sig.length - newSig.length,
				 newSig, 0, newSig.length);
	    } else {
		int offset = newSig.length - sig.length;
		for (int i = 0; i < offset; ++i)
		    newSig[i] = 0;
		System.arraycopy(sig, 0, newSig, offset, sig.length);
	    }
	    signature = newSig;
	}
    }


    

    /**
     * Returns the 20-digit random number generated for this eCard.
     *
     * @return	The 20-digit random number generated for this eCard.
     */
    public long getRandomNumber()
    {
	return randomNumber;
    }

    /**
     * Returns the signature of this eCard.
     *
     * @return	The signature of this eCard.
     */
    public byte[] getSignature()
    {
	return signature;
    }
}

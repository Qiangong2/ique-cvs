package com.broadon.xml;

import java.io.IOException;
import java.util.Map;

/**
 * The <c>XMLMap</c> class provides the capability to convert a
 * XML document into a map. It only deals with the leaf elements,
 * and will replace elements which have the same name.
 *
 * @version	$Revision: 1.4 $
 */
public class XMLMap
    extends XMLOutputProcessor
{
    /**
     * Constructs the XMLMap instance.
     *
     * @throws	IOException
     */
    public XMLMap()
	throws IOException
    {
	super();
    }

    /**
     * Processes the leaf element (name, value), and adds to the result
     * instance (an Map).
     *
     * @param	result			the Map instance
     * @param	name			the name of the attribute
     * @param	value			the value of the attribute
     * @param	param			the additional parameters
     */
    protected void processElement(Object result,
				  String name,
				  String value,
				  Object param)
    {
	if (value == null || value.equals(""))
	{
	    /*
	     * Skip null values.
	     */
	    return;
	}
	((Map)result).put(name, value);
    }
}

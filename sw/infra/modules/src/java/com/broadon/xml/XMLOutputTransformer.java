package com.broadon.xml;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * The <c>XMLOutputTransformer</c> class provides the capability to transform a
 * XML document into different formats, based on the given style specification.
 *<p>
 * Initializing the XML parser is an expensive operation. The best use of this
 * class is to assign one instance to each Thread for repeating usage,
 * preventing both the expensive initialization and concurrent access.
 *
 * @version	$Revision: 1.6 $
 */
public class XMLOutputTransformer
{
    private SAXParser		parser;
    private Transformer		transformer;
    private String              SystemId;
    
    /**
     * Constructs the XMLOutputTransformer type instance.
     *
     * @param	styleSource		the source of style specification
     *
     * @throws	IOException
     */
    public XMLOutputTransformer()
	throws IOException
    {
	/*
	 * Obtain the SAX parser.
	 */
	try
	{
	    SAXParserFactory	parserFactory;

	    parserFactory = SAXParserFactory.newInstance();
	    this.parser = parserFactory.newSAXParser();
	}
	catch (ParserConfigurationException pce)
	{
	    pce.printStackTrace();
	    throw new IOException("XMLOutputTransformer: " + pce.getMessage());
	}
	catch (SAXException se)
	{
	    se.printStackTrace();
	    throw new IOException("XMLOutputTransformer: " + se.getMessage());
	}
	this.transformer = null;
        this.SystemId = null;
    }

    /**
     * Sets the source of the style specification.
     *
     * @param	styleSource		the source of style specification
     *
     * @throws	IOException
     */
    public void setStyleSource(File styleSource)
	throws IOException
    {
	this.setStyleSource(new StreamSource(styleSource));
    }

    /**
     * Sets the source of the style specification.
     *
     * @param	styleSource		the source of style specification
     *
     * @throws	IOException
     */
    public void setStyleSource(String styleSource)
	throws IOException
    {
	this.setStyleSource(new StreamSource(new StringReader(styleSource)));
    }

    /**
     * Sets the source of the style specification.
     *
     * @param	styleSource		the source of style specification
     *
     * @throws	IOException
     */
    public void setStyleSource(InputStream styleSource)
	throws IOException
    {
	this.setStyleSource(new StreamSource(styleSource));
    }

    /**
     * Sets the source of the style specification.
     *
     * @param	styleSource		the source of style specification
     *
     * @throws	IOException
     */
    public void setStyleSource(Reader styleSource)
	throws IOException
    {
	this.setStyleSource(new StreamSource(styleSource));
    }

    /**
     * Sets the source of the style specification.
     *
     * @param	styleSource		the source of style specification
     *
     * @throws	IOException
     */
    public void setStyleSource(Source styleSource)
	throws IOException
    {
	/*
	 * Obtain the Transformer.
	 */
	try
	{
            if (SystemId != null)
                styleSource.setSystemId(SystemId);
            
            TransformerFactory	transformerFactory;

	    transformerFactory = TransformerFactory.newInstance();
	    this.transformer = transformerFactory.newTransformer(styleSource);
	}
	catch (TransformerConfigurationException tce)
	{
	    tce.printStackTrace();
	    throw new IOException("XMLOutputTransformer: " + tce.getMessage());
	}
    }

    /**
     * Transforms the given XML document to a different output format.
     *
     * @param	xmlContent		the XML docuemnt to be processed
     *
     * @return	The result object
     *
     * @throws	IOException
     */
    public String transform(String xmlContent)
	throws IOException
    {
	if (xmlContent == null)
	{
	    throw new IllegalArgumentException("xmlContent");
	}

	StringReader	reader = new StringReader(xmlContent);
	StringWriter	writer = new StringWriter();

	transform(reader, writer);
	return writer.toString();
    }

    /**
     * Transforms the given XML document to a different output format.
     *
     * @param	in			the InputStream that contains the XML
     *					docuemnt to be processed
     * @param	output			the output of transformation
     *
     * @throws	IOException
     */
    public void transform(InputStream in, OutputStream output)
	throws IOException
    {
	if (in == null)
	{
	    throw new IllegalArgumentException("in");
	}
	if (output == null)
	{
	    throw new IllegalArgumentException("result");
	}
	transform(new InputStreamReader(in), new OutputStreamWriter(output));
    }

    /**
     * Transforms the given XML document to a different output format.
     *
     * @param	reader			the reader that contains the XML
     *					docuemnt to be processed
     * @param	output			the output of transformation
     *
     * @throws	IOException
     */
    public void transform(Reader reader, Writer output)
	throws IOException
    {
	if (reader == null)
	{
	    throw new IllegalArgumentException("reader");
	}
	if (output == null)
	{
	    throw new IllegalArgumentException("result");
	}
	if (transformer == null)
	{
	    throw new IOException("No style source spceified");
	}
	/*
	 * Prepare the InputSource based on xmlContent.
	 */
	InputSource	source = new InputSource(reader);
	Result		result = new StreamResult(output);

	try
	{
            this.transformer.transform(new SAXSource(source), result);
	}
	catch (TransformerException te)
	{
	    throw new IOException("XMLOutputTransformer.transform: " +
				  te.getMessage());
	}
    }

    /**
     * Sets the system identifier for the input source
     *
     * @param   systemId             the system identifier for the input source
     */
    public void setSystemId(String systemId)
    {
        this.SystemId = systemId;
    }
    
    // Method accepts three parameters
    // styleSrc  ---  XSL Style Sheet source
    // paramName ---  Name of the parameter
    // val       ---  Value of the parameter to be set
    public static String setStyleSheetParam(String styleSrc, String paramName, String Val)
    {
        styleSrc =  styleSrc.trim();

        // Find  the first <xsl:template> tag and extract the portion of the style
        // sheet that appears before it. Global parameters are always specified
        // before the first <xsl:template> tag
        int temTagStart = styleSrc.indexOf("<xsl:template");
        String curStyle= styleSrc.substring(0,temTagStart).trim();

        // Locate the given parameter string and then find the
        // position of string  </xsl:param>
        int paramLoc = curStyle.indexOf("<xsl:param name=\""+paramName);
        int paramEnd = paramLoc+curStyle.substring(paramLoc).indexOf("</xsl:param>")+12;

        if (paramLoc ==  0) return styleSrc;

        // Substitute parameter value and rebuild the complete style sheet
        String modStr = styleSrc.substring(0,paramLoc)+"<xsl:param name=\""+paramName+"\">"
            +Val+"</xsl:param>"+
            styleSrc.substring(paramEnd) ;
        return modStr;
    }
}

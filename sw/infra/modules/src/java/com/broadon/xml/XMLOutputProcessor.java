package com.broadon.xml;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * The <c>XMLOutputProcessor</c> class provides the capability to convert a
 * XML document into other formats.
 *<p>
 * Initializing the XML parser is an expensive operation. The best use of this
 * class is to assign one instance to each Thread for repeating usage,
 * preventing both the expensive initialization and concurrent access.
 *
 * @version	$Revision: 1.8 $
 */
public abstract class XMLOutputProcessor
{
    private SAXParser		parser;

    /**
     * Constructs the XMLOutputProcessor type instance.
     *
     * @throws	IOException
     */
    public XMLOutputProcessor()
	throws IOException
    {
	/*
	 * Obtain the SAX parser.
	 */
	try
	{
	    SAXParserFactory	parserFactory;

	    parserFactory = SAXParserFactory.newInstance();
	    this.parser = parserFactory.newSAXParser();
	}
	catch (ParserConfigurationException pce)
	{
	    pce.printStackTrace();
	    throw new IOException("XMLOutputProcessor: " + pce.getMessage());
	}
	catch (SAXException se)
	{
	    se.printStackTrace();
	    throw new IOException("XMLOutputProcessor: " + se.getMessage());
	}
    }

    /**
     * Processes the given XML document to a different output format.
     *
     * @param	xmlContent		the XML docuemnt to be processed
     * @param	result			the result of processing
     *
     * @return	The result object; so that it can be put in an expression.
     *
     * @throws	IOException
     */
    public Object process(String xmlContent, Object result)
	throws IOException
    {
	return this.process(xmlContent, result, null);
    }

    /**
     * Processes the given XML document to a different output format.
     *
     * @param	xmlContent		the XML docuemnt to be processed
     * @param	result			the result of processing
     * @param	rootTag			the starting point of processing
     *
     * @return	The result object; so that it can be put in an expression.
     *
     * @throws	IOException
     */
    public Object process(String xmlContent, Object result, String rootTag)
	throws IOException
    {
	if (xmlContent == null)
	{
	    throw new IllegalArgumentException("xmlContent");
	}
	return this.process(new StringReader(xmlContent), result, rootTag);
    }

    /**
     * Processes the given XML document to a different output format.
     *
     * @param	in			the InputStream that contains the XML
     *					docuemnt to be processed
     * @param	result			the result of processing
     *
     * @return	The result object; so that it can be put in an expression.
     *
     * @throws	IOException
     */
    public Object process(InputStream in, Object result)
	throws IOException
    {
	return this.process(in, result, null);
    }

    /**
     * Processes the given XML document to a different output format.
     *
     * @param	in			the InputStream that contains the XML
     *					docuemnt to be processed
     * @param	result			the result of processing
     * @param	rootTag			the starting point of processing
     *
     * @return	The result object; so that it can be put in an expression.
     *
     * @throws	IOException
     */
    public Object process(InputStream in, Object result, String rootTag)
	throws IOException
    {
	if (in == null)
	{
	    throw new IllegalArgumentException("in");
	}
	return this.process(new InputStreamReader(in), result, rootTag);
    }

    /**
     * Processes the given XML document to a different output format.
     *
     * @param	reader			the reader that contains the XML
     *					docuemnt to be processed
     * @param	result			the result of processing
     *
     * @return	The result object; so that it can be put in an expression.
     *
     * @throws	IOException
     */
    public Object process(Reader reader, Object result)
	throws IOException
    {
	return this.process(reader, result, null);
    }

    /**
     * Processes the given XML document to a different output format.
     *
     * @param	reader			the reader that contains the XML
     *					docuemnt to be processed
     * @param	result			the result of processing
     * @param	rootTag			the starting point of processing
     *
     * @return	The result object; so that it can be put in an expression.
     *
     * @throws	IOException
     */
    public Object process(Reader reader, Object result, String rootTag)
	throws IOException
    {
	return this.process(reader, result, rootTag, null);
    }

    /**
     * Processes the given XML document to a different output format.
     *
     * @param	reader			the reader that contains the XML
     *					docuemnt to be processed
     * @param	result			the result of processing
     * @param	rootTag			the starting point of processing
     * @param	parameter		the additional parameters
     *
     * @return	The result object; so that it can be put in an expression.
     *
     * @throws	IOException
     */
    public Object process(Reader reader,
			  Object result,
			  String rootTag,
			  Object parameter)
	throws IOException
    {
	if (reader == null)
	{
	    throw new IllegalArgumentException("reader");
	}
	if (result == null)
	{
	    throw new IllegalArgumentException("result");
	}
	/*
	 * Prepare the InputSource based on xmlContent.
	 */
	InputSource	source = new InputSource(reader);
	/*
	 * Set up the SAX handler for capturing the content.
	 */
	DefaultHandler	handler = new Handler(result, rootTag, parameter);

	try
	{
	    /*
	     * Start parsing.
	     */
	    parser.parse(source, handler);
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	    throw new IOException("SAX Parser: " + e.getMessage());
	}
	return result;
    }

    /**
     * Processes the element (name, value), and adds to the result
     * instance.
     *
     * @param	result			the instance containing the result
     * @param	name			the name of the attribute
     * @param	value			the value of the attribute
     * @param	parameter		the additional parameters
     *
     * @param	SAXException
     */
    protected abstract void processElement(Object result,
					   String name,
					   String value,
					   Object parameter)
	throws SAXException;

    /**
     * Initializes before processing an element. The default is to do
     * nothing.
     *
     * @param	result			the instance containing the result
     * @param	name			the name of the attribute
     * @param	parameter		the additional parameters
     *
     * @param	SAXException
     */
    protected void initElement(Object result, String name, Object parameter)
	throws SAXException
    {
    }

    /**
     * Found a root element. The default is to do nothing.
     *
     * @param	result			the instance containing the result
     * @param	name			the name of the attribute
     * @param	parameter		the additional parameters
     */
    protected void rootElement(Object result, String name, Object parameter)
    {
    }

    /**
     * The <c>Handler</c> class processes the XML docuemnt, SAX style.
     */
    private final class Handler
	extends DefaultHandler
    {
	private Object			result;
	private String			rootTag;
	private Object			parameter;
	private StringBuffer		buffer;
	private boolean			rootFound;

	/**
	 * Constructs the Handler instance.
	 *
	 * @param	result		the result of processing
	 * @param	rootTag		the starting point of conversion
	 * @param	parameter	the additional parameters
	 */
	private Handler(Object result, String rootTag, Object parameter)
	{
	    this.result = result;
	    this.rootTag = rootTag;
	    this.parameter = parameter;
	    this.buffer = null;
	    this.rootFound = false;
	}

	/**
	 * Receives notification of the beginning of an element.
	 *
	 * @param	uri		the Namespace URI
	 * @param	localName	the local name (without prefix)
	 * @param	qName		the qualified name (with prefix)
	 * @param	attributes	the attributes attached to the element
	 *
	 * @throws	SAXException
	 */
	public void startElement(String uri,
				 String localName,
				 String qName,
				 Attributes attributes)
	    throws SAXException
	{
	    if (rootFound) {
		/*
		 * This element is within the hierarchy of the element
		 * identified by rootTag, process it.
		 */
		buffer = new StringBuffer();
		/*
		 * Initialize.
		 */
		initElement(result, qName, parameter);
	    } else if (rootTag == null || rootTag.equals(qName)) {
		/*
		 * Found the root tag; start processing.
		 */
		rootFound = true;
		rootElement(result, qName, parameter);
	    }
	}

	/**
	 * Receives notification of the end of an element.
	 *
	 * @param	uri		the Namespace URI
	 * @param	localName	the local name (without prefix)
	 * @param	qName		the qualified name (with prefix)
	 *
	 * @throws	SAXException
	 */
	public void endElement(String uri, String localName, String qName)
	    throws SAXException
	{
	    if (rootTag != null && rootTag.equals(qName))
	    {
		/*
		 * Done processing, skip the rest.
		 */
		rootFound = false;
		return;
	    }
	    /*
	     * Process this element, if marked.
	     */
	    if (buffer != null)
	    {
		processElement(result, qName, buffer.toString(), parameter);
		/*
		 * Clean up.
		 */
		buffer = null;
	    }
	}

	/**
	 * Receives notification of character data.
	 *
	 * @param	ch		the characters from the XML document
	 * @param	start		the start position in the ch array
	 * @param	length		the number of characters to read from
	 *				the ch array
	 */
	public void characters(char[] ch, int start, int length)
	{
	    if (buffer != null)
	    {
		/*
		 * Capture the data.
		 */
		buffer.append(ch, start, length);
	    }
	}
    }
}

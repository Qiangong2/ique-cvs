package com.broadon.xml;

import java.io.IOException;
import java.util.*;

/**
 * Similar to <code>XMLMap</code>, this class provides the capability
 * to convert an XML document into a map. It differs from
 * <code>XMLMap</code> and <code>XMLMultiMap</code> in that it allows
 * an arbitrary subset of elements to have multiple values.  This
 * class is <em>not</em> thread-safe.
 *
 * @version	$Revision: 1.4 $
 * @author W. Wilson Ho
 */
public class XMLHybridMap extends XMLOutputProcessor
{
    public static final String rootTag = "<root>";
    private Collection multi_value_keys = new HashSet(0);

    /**
     * Constructs the XMLHybridMap instance.
     * @throws	IOException
     */
    public XMLHybridMap() throws IOException
    {
	super();
    }

    /**
     * Specify the collection of element tags that allow multiple values.
     * @param c An collection of tag strings -- usually a <c>HashSet</c>
     */
    public void setMultiValueElements(AbstractCollection c) { 
	if (c != null)
	    this.multi_value_keys = c;
    }

    /**
     * Processes the leaf element (name, value), and adds to the result
     * instance (an Map).
     *
     * @param	result			the Map instance
     * @param	name			the name of the attribute
     * @param	value			the value of the attribute
     * @param	param			the additional parameters
     */
    protected void processElement(Object result, String name, String value,
				  Object param)
    {
	if (value == null || value.equals(""))
	{
	    /*
	     * Skip null values.
	     */
	    return;
	}
	if (multi_value_keys.contains(name)) {
	    Vector v = (Vector)((Map)result).get(name);
	    if (v == null) {
		v = new Vector();
		((Map)result).put(name, v);
	    }
	    v.add(value);
	} else
	    ((Map)result).put(name, value);
    }

    protected void rootElement(Object result, String name, Object parameter)
    {
	processElement(result, rootTag, name, parameter);
    }
}

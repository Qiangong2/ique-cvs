package com.broadon.hsmserver;

import java.io.*;
import java.util.Properties;
import java.math.BigInteger;
import java.security.cert.X509Certificate;
import java.security.GeneralSecurityException;
import com.ncipher.km.nfkm.*;
import com.ncipher.nfast.marshall.*;
import com.ncipher.nfast.connect.NFConnection;

import com.broadon.security.X509;

/**
 * This <code>HSM</code> class interfaces with the Hardware Security Module.
 */
public class HSM
{
    SecurityWorld sw;
    NFConnection conn;
    M_KeyID key;
    int keyLength;

    M_Command cmdRand;
    M_Cmd_Args_GenerateRandom argsRand;
    M_Command cmdSign;
    M_Cmd_Args_Sign argsSign;

    static final String CERT_FILE_KEY = "eCard_signer_cert";
    static final String KEY_NAME = "eCard_signer_keyname";

    String certID;

    /**
     * Constructs a HSM instance of the given type.
     * @param porp Properties that contain the configuration information.
     */
    public HSM(Properties prop)
	throws HSMException, IOException
    {
	certID = computeCertID(prop.getProperty(CERT_FILE_KEY));


	try {
	    sw = new SecurityWorld();
	    conn = sw.getConnection();
	    loadKey(prop.getProperty(KEY_NAME));
	    keyLength = getKeyLength();
	    
	    // Random command
	    
	    argsRand = new M_Cmd_Args_GenerateRandom(0);
	    cmdRand = new M_Command(M_Cmd.GenerateRandom, 0, argsRand);

	    // Sign command

	    argsSign = new M_Cmd_Args_Sign(0, key, M_Mech.RSAhSHA1pPKCS1, null);
	    argsSign.plain =
		new M_PlainText(M_PlainTextType.Bytes,
				new M_PlainTextType_Data_Bytes(new M_ByteBlock()));
	    cmdSign = new M_Command(M_Cmd.Sign, 0, argsSign);
	} catch (Exception e) {
	    throw new HSMException(e.getMessage());
	}
    } // HSM


    void loadKey(String keyname) throws Exception
    {
	com.ncipher.km.nfkm.Key[] k = sw.listKeys(null);
	for (int i = 0; i < k.length; ++i) {
	    if (keyname.equals(k[i].getName())) {
		key = k[i].load(sw.getModule(1).getSlot(0));
		return;
	    }
	}
	throw new HSMException("Key " + keyname + " not found");
    } // loadKey
    

    int getKeyLength() throws Exception
    {
	M_Command cmd =
	    new M_Command(M_Cmd.GetKeyInfoEx, 0,
			  new M_Cmd_Args_GetKeyInfoEx(0, key));
	M_Cmd_Reply_GetKeyInfoEx reply = (M_Cmd_Reply_GetKeyInfoEx)
	    conn.wait(conn.submit(cmd), 10000).reply;
	return (int) (reply.length.value + 7) / 8;
    }

    String computeCertID(String certFile)
	throws IOException, HSMException
    {
	try {
	    X509 x509 = new X509();
	    X509Certificate cert = x509.readX509(certFile);
	    return x509.genUniqueID(cert);
	} catch (GeneralSecurityException e) {
	    throw new HSMException("Cannot read certificate: " +
				   e.getMessage());
	}
    }

    /**
     * Generate some random bytes.
     * @param size The number of random bytes requested.
     * @return An array of random byte of the specified length.
     * @exception
     */
    public byte[] getRandom(int size)
	throws HSMException
    {
	try {
	    argsRand.lenbytes.value = size;
	    M_Reply r = conn.wait(conn.submit(cmdRand), 10000);
	    M_Cmd_Reply_GenerateRandom reply =
		(M_Cmd_Reply_GenerateRandom) r.reply;
	    return reply.data.value;
	} catch (Exception e) {
	    throw new HSMException("Failed to access hardware RNG: " +
				   e.getMessage());
	} 
    } // getRandom


    /**
     * Digitally sign a document.
     * @param document Document to be signed.
     * @return The digital signature.
     */
    public BigInteger sign(byte[] document)
	throws HSMException	
    {
	try {
	    M_PlainTextType_Data_Bytes data =
		(M_PlainTextType_Data_Bytes) argsSign.plain.data;
	    data.data.value = document;
	    
	    M_Cmd_Reply_Sign rep =
		(M_Cmd_Reply_Sign) conn.wait(conn.submit(cmdSign), 10000).reply;
	    M_Mech_Cipher_RSApPKCS1 rsa = (M_Mech_Cipher_RSApPKCS1)rep.sig.data;
	    return rsa.m.value;
	} catch (Exception e) {
	    throw new HSMException("Failed to sign the eCard: " +
				   e.getMessage());
	}
    }


    /**
     * Returns the certificate identifier of the key to be used.
     *
     * @return	The certificate identifier of the key to be used.
     */
    public String getCertificateID()
    {
	return certID;
    }


    /**
     * Return the length of the signature in number of bytes.
     * @return The length of the signature in number of bytes.
     */
    public int getSignatureLength() {
	return keyLength;
    }

    public static void main(String[] args) {
	try {
	    Properties prop = new Properties();
	    prop.load(new FileInputStream(args[0]));
	    File f = new File(args[1]);
	    byte[] buffer = new byte[(int) f.length()];
	    FileInputStream in = new FileInputStream(f);
	    in.read(buffer);
	    HSM hsm = new HSM(prop);
	    BigInteger sig = hsm.sign(buffer);
	    System.out.write(sig.toByteArray());
	    System.err.println("Cert ID = " + hsm.getCertificateID());
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }
}

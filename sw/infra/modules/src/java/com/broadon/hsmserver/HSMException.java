package com.broadon.hsmserver;

import com.broadon.exception.BroadOnException;

/**
 * The <code>HSMException</code> class is thrown when any HSM
 * operation is unsuccessful.
 *
 * @version	$Revision: 1.1 $
 */
public class HSMException
    extends BroadOnException
{
    /**
     * Constructs a HSMException instance.
     */
    public HSMException()
    {
	super();
    }

    /**
     * Constructs a HSMException instance.
     *
     * @param	message			the exception message
     */
    public HSMException(String message)
    {
	super(message);
    }

    /**
     * Constructs a HSMException instance.
     *
     * @param	message			the exception message
     * @param	throwable		the nested exception
     */
    public HSMException(String message, Throwable throwable)
    {
	super(message, throwable);
    }
}

/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: LoginServlet.java,v 1.13 2004/04/21 11:38:31 tomsheng Exp $
 */
package com.broadon.bus;

import java.lang.Integer;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import java.text.SimpleDateFormat;
import java.text.DateFormat;

import com.broadon.ms.common.AdminServlet;
import com.broadon.db.DBAccessFactory;
import com.broadon.db.DBException;
import com.broadon.db.OperationUserRoleFactory;
import com.broadon.db.OperationUserFactory;
import com.broadon.db.OperationUser;
import com.broadon.db.BusinessUnitFactory;

public class LoginServlet extends AdminServlet
{
    private static final String CURRENT_LOCALE = "CURRENT_LOCALE";
    private static final String LABEL_PROP_KEY = "LOGIN_PROP";
    private static final String JSP_LOC_KEY = "LOGIN_JSP_LOC";
    
    private static final String SES_ERR_NOT_FOUND = "ERR_NOT_FOUND";
    private static final String SES_ERR_INVALID_PWD = "ERR_INVALID_PWD";
    private static final String SES_ERR_INACTIVE = "ERR_INACTIVE";
    private static final String SES_ERR_UNAUTHORIZED = "ERR_UNAUTHORIZED";
    private static final String SES_ERR_BU_NOT_FOUND = "ERR_BU_NOT_FOUND";
    private static final String SES_NON_GUI_ROLES = "NON_GUI_ROLES";

    private static final String ADMIN_LOGIN = "admin";
    private static final String ADMIN_PASSWORD = "nimda";
    private static final String ADMIN_FULLNAME = "Administrator Setup Account";

    private static final int ERR_NOT_FOUND = -1;
    private static final int ERR_INVALID_PWD = -2;
    private static final int ERR_NEW = -3;
    private static final int ERR_INACTIVE = -4;

    private static final int FORWARD_MODULE_LOGIN = 1;
    private static final int FORWARD_MODULE_LOGIN_URL = 2;
    private static final int FORWARD_URL = 3;
    
    private static OperationUserFactory operationUserFactory;
    private static OperationUserRoleFactory operationUserRoleFactory;
    private static BusinessUnitFactory buFactory;

    static
    {
        try
        {
            operationUserFactory = (OperationUserFactory)DBAccessFactory.getInstance(
                                          OperationUserFactory.class.getName());
            operationUserRoleFactory = (OperationUserRoleFactory)DBAccessFactory.getInstance(                                          
					  OperationUserRoleFactory.class.getName());
            buFactory = (BusinessUnitFactory)DBAccessFactory.getInstance(                                          
					  BusinessUnitFactory.class.getName());
        }
        catch (DBException de)
        {
            de.printStackTrace();
            System.exit(-1);
        }
    }

    public void init (ServletConfig config) throws ServletException
    {
        try
        {
            super.init(config);

            if (mAdminProp!=null)
                mLabelsFile = mAdminProp.getProperty(LABEL_PROP_KEY);

        } catch (ServletException e) {
            throw (e);
        }
    }
    
    public void doGet(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException, ServletException
    {  
        HttpSession ses = req.getSession(false);
        if (ses == null) 
        {
            System.out.println(getClass().getName() + " No session");
            return;
        }
        
        String url = req.getParameter("url");
        authenticateForward(url, req, res, ses);
        
        return;
    }
    
    public void doPost(HttpServletRequest req,
                       HttpServletResponse res)
        throws IOException,ServletException
    {
        doGet(req, res);        
        return;
    }

    private void authenticateForward (String url, 
                                HttpServletRequest req, 
                                HttpServletResponse res, 
                                HttpSession ses)
        throws IOException, ServletException
    {
        req.setAttribute(SES_ERROR, "");
        String action = req.getParameter("laction");
        String buid = null;
        int forward = 0;

        if (url==null || url.equals("")) 
        {
            url = "/serv?type=home&action=edit";
        }
        
        if (action!=null && action.equals("login"))
        {
	    try 
	    {
	        operationUserFactory.beginTransaction(super.dataSource, false, false);

                try 
                {
                    String email = req.getParameter("email");
                    String pwd = req.getParameter("pwd");
                    String current_locale = req.getParameter("current_locale");

		    if (buFactory.getBusinessUnitCount() <= 0) 
                    {
                        req.setAttribute(SES_ERROR, ses.getAttribute(SES_ERR_BU_NOT_FOUND));                    
                        forward = FORWARD_MODULE_LOGIN;

                    } else {
			String bunames= buFactory.getBusinessUnitNames();
                        buid = bunames.substring(bunames.indexOf("<BU_ID>")+7, bunames.indexOf("</BU_ID>"));

                        if (operationUserRoleFactory.getOperationUserRoleCount(buid, String.valueOf(ROLE_BU_ADMIN)) > 0) 
                        {
                            //*************************************
  	                    //  String enc_pwd = getEncrypted(pwd);
                            //*************************************
                            String enc_pwd = pwd;

		            OperationUser opUser = operationUserFactory.getOperationUser(email, super.nameMap);
		
		    	    if (opUser != null) 
		            {        
  			        Object ses_ngr = ses.getAttribute(SES_NON_GUI_ROLES);
				String ngr = null;
				int ngrNum = 0;
                                if (ses_ngr!=null)
				    ngr = ses_ngr.toString();
				
				if (ngr!=null && !ngr.equals(""))
				    ngrNum = operationUserRoleFactory.getOperationUserRoleCount(String.valueOf(opUser.getOperatorID()), ngr, 1);
				else
				    ngrNum = operationUserRoleFactory.getOperationUserRoleCount(String.valueOf(opUser.getOperatorID()), 1);

		                if (!(opUser.getStatus().equals("A"))) 
                                {
                                    req.setAttribute(SES_ERROR, ses.getAttribute(SES_ERR_INACTIVE));    
                                    forward = FORWARD_MODULE_LOGIN;

                                } else if (ngrNum == 0) {
                                    req.setAttribute(SES_ERROR, ses.getAttribute(SES_ERR_UNAUTHORIZED));    
                                    forward = FORWARD_MODULE_LOGIN;

                                } else { 

		                    if (opUser.getPasswd().equals(enc_pwd)) 
                                    {
			                Long opID = opUser.getOperatorID();
			                String opEmail = opUser.getEmailAddress();
                                        ses.setAttribute(SES_USER, opEmail);
                                        ses.setAttribute(SES_ROLE, opUser.getRoleLevel());
                                        ses.setAttribute(SES_FULLNAME, opUser.getFullname());
                                        ses.setAttribute(SES_USER_ID, opUser.getOperatorID());
                                        ses.setAttribute(SES_BU_ID, buid);
                                        ses.setAttribute(SES_ROLE_NUM, String.valueOf(ngrNum));
                                        ses.setAttribute(SES_LOCALE, current_locale);

				        String xmlData = getDataXML(opID);
	                                try
        	                        {
				            int result = operationUserFactory.update(xmlData);                           	    
                        	        }
                        	        catch (DBException de)
                        	        {
                           	              System.out.println(getClass().getName() + ": Could not update last login time for user - " + opEmail);
                        	        }
                            	        forward = FORWARD_URL;

                                    } else {
                                        req.setAttribute(SES_ERROR, ses.getAttribute(SES_ERR_INVALID_PWD));                    
                                        forward = FORWARD_MODULE_LOGIN;
                                    }    
		                }

		            } else {
                                req.setAttribute(SES_ERROR, ses.getAttribute(SES_ERR_NOT_FOUND));                    
                                forward = FORWARD_MODULE_LOGIN;
                            }

                        } else {
                            if (email != null && email.equals(ADMIN_LOGIN) && pwd != null && pwd.equals(ADMIN_PASSWORD)) 
                            {
			        ses.setAttribute(SES_USER, ADMIN_LOGIN);
                                ses.setAttribute(SES_ROLE, String.valueOf(ROLE_BU_ADMIN));
                                ses.setAttribute(SES_FULLNAME, ADMIN_FULLNAME);
				ses.setAttribute(SES_BU_ID, buid);
                                ses.setAttribute(SES_USER_ID, "0");
                                ses.setAttribute(SES_ROLE_NUM, "1");
                                ses.setAttribute(SES_LOCALE, current_locale);
                         	forward = FORWARD_URL;
                            } else {
                                req.setAttribute(SES_ERROR, ses.getAttribute(SES_ERR_NOT_FOUND));                    
                                forward = FORWARD_MODULE_LOGIN;
                            }   
                        }   
                    }   
                //*************************************
		// } catch (NoSuchAlgorithmException ex) {
                //   ex.printStackTrace();
                //   System.out.println(getClass().getName() + ": Error occured while encrypting password: " + ex.toString());
                //   req.setAttribute(SES_ERROR, "Exception occurred - " + ex.toString());
                //   forward = FORWARD_MODULE_LOGIN_URL;
		//*************************************
                } catch (DBException de) {
                    de.printStackTrace();
                    req.setAttribute(SES_ERROR, "SQLException occurred - " + de.toString());
                    forward = FORWARD_MODULE_LOGIN_URL;

                } finally {
                    try 
                    {
                        operationUserFactory.commitTransaction();
                    } catch (DBException de) {
                        req.setAttribute(SES_ERROR, "SQLException occurred - " + de.toString());
                        de.printStackTrace();
                    }
                }

            } catch (Throwable t) {
                t.printStackTrace();
                System.out.println(getClass().getName() + ": Error occured authenticating user information. " + t);
                req.setAttribute(SES_ERROR, "SQLException occurred - " + t);
                forward = FORWARD_MODULE_LOGIN_URL;
            }

        } else if (action!=null && action.equals("logout")) {
            // clear all session object
            ses.removeAttribute(SES_USER);
            ses.removeAttribute(SES_USER_ID);
            ses.removeAttribute(SES_BU_ID);
            ses.removeAttribute(SES_LOCALE);
            ses.removeAttribute(SES_ROLE);
            ses.removeAttribute(SES_ROLE_NUM);
            ses.removeAttribute(SES_ROLE_NAME);
            ses.removeAttribute(SES_FULLNAME);
            forward = FORWARD_MODULE_LOGIN;

        } else {
            Object user = ses.getAttribute(SES_USER);
            if (user!=null && user.toString()!="")
                forward = FORWARD_URL;
            else 
                forward = FORWARD_MODULE_LOGIN_URL;
        }
          
        if (forward == FORWARD_MODULE_LOGIN)
            forwardToModule(JSP_LOC_KEY, "login.jsp", req, res);
        else if (forward == FORWARD_MODULE_LOGIN_URL)
            forwardToModule(JSP_LOC_KEY, "login.jsp?url="+url, req, res);
        else if (forward == FORWARD_URL)
            forward(url, req, res);
        else
            forwardToModule(JSP_LOC_KEY, "login.jsp", req, res);

        return;
    }

    private String getEncrypted(String pwd) 
		throws NoSuchAlgorithmException 
    {
	MessageDigest md = MessageDigest.getInstance("MD5");
	md.reset();

	byte[] encoded = md.digest(pwd.getBytes());

        StringBuffer b = new StringBuffer(128);
        for (int i=0; i<encoded.length; i++) 
        {
            String elem = Integer.toHexString(encoded[i] + 128);

            if (elem.length() == 1) {
                b.append("0");
            }

            b.append(elem);
        }
        return b.toString();
    }

    private String getDataXML (Long id)
    {
        String DataXML = null;
        String loginDate = null;

        Calendar calendar = new GregorianCalendar();
        java.util.Date date = calendar.getTime();
        DateFormat utc = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
        utc.setTimeZone(TimeZone.getTimeZone("GMT+00:00"));
        loginDate = utc.format(date);

        DataXML = "<ROWSET><ROW num=\"0\">";
        DataXML += "<OPERATOR_ID>"+String.valueOf(id)+"</OPERATOR_ID>";
        DataXML += "<LAST_LOGON>"+loginDate+"</LAST_LOGON>";
        DataXML += "</ROW></ROWSET>";

        return DataXML;
    }
}



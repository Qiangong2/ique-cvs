package com.broadon.hsm;

import java.io.*;

/**
 * Simple extension to ByteArrayOutputStream to optimize generation of
 * the network header used in communicating with the HSM server.
 */
public class HSMHeaderOutputStream extends ByteArrayOutputStream
{
    /**
     * Construct a <code>HSMHeaderOutputStream</code> with a
     * <code>size</code> number of bytes.
     * @param size Initial size of internal buffer.
     */
    public HSMHeaderOutputStream(int size) {
	super(size);
    }

    /**
     * Return the internal buffer, instead of returning a new copy as
     * in toByteArray()
     * @see ByteArrayOutputStream#toByteArray()
     * @return The protected field <code>buf</code>
     * @see ByteArrayOutputStream#buf
     */
    public byte[] getInternalBuf() {
	return buf;
    }

    /**
     * Pad the remaining of the buffer with the given character.
     * @param c Character used in the padding.
     */
    public void pad(char c) {
	for (int i = count; i < buf.length; ++i) {
	    buf[i] = (byte)c;
	}
    }
}

package com.broadon.hsm;

import java.io.PrintWriter;

/**
 * Implement the random bytes request header composition. 
 */
public class HSMRandomBytesRequest implements HSMFunction
{
    final int length;

    /**
     * Specify the number of random bytes requested.
     * @param length Number of random bytes.
     */
    public HSMRandomBytesRequest(int length) {
	this.length = length;
    }

    public void compose(HSMHeaderOutputStream out, int size) {
	PrintWriter writer = new PrintWriter(out);
	writer.println(Constants.VERSION + " = " + Constants.VERSION_STRING);
	writer.println(Constants.COMMAND + " = " + Constants.CMD_RAND);
	writer.println(Constants.RANDOM_SIZE + " = " + length);
	writer.close();
	out.pad(' ');
    }
	
}

package com.broadon.hsm;

import java.io.*;
import java.net.*;
import java.util.Properties;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.broadon.security.CertInfo;


/**
 * A Client to the security function servers.
 */
public class HSMClient
{
    final String host;
    final int port;

    final int randomBytesCacheSize = 2048;
    byte[] randomBytesCache = null;
    int usedRandomBytes = randomBytesCacheSize;

    // for testing only, zz
    byte[] headerData = null;

    /**
     * Construct an instance for connection to the HSM server.
     * @param host Hostname or IP address of the HSM server.
     * @param port Port that the server listen to.
     */
    public HSMClient(String host, int port) {
	this.host = host;
	this.port = port;
    }


    public byte[] getRandomBytesNoCache(int size) throws IOException {
	HSMFunction func = new HSMRandomBytesRequest(size);
	return execute(null, 0, 0, func);
    }

    /**
     * Request for a cryptographically strong random byte sequence.
     * @param size Number of random bytes requested.
     * @return A sequence of random bytes.
     * @exception IOException Any error when communicating with the HSM.
     */
    public synchronized byte[] getRandomBytes(int size) throws IOException {
	if (size > (randomBytesCacheSize - usedRandomBytes)) {
	    if (size > randomBytesCacheSize)
		return getRandomBytesNoCache(size);
	    randomBytesCache = getRandomBytesNoCache(randomBytesCacheSize);
	    usedRandomBytes = 0;
	}

	byte[] result = new byte[size];
	System.arraycopy(randomBytesCache, usedRandomBytes, result, 0, size);
	usedRandomBytes += size;
	return result;
    }


    /**
     * Digitally signed a block of data.
     * @param signer ID of the identity used for signing.
     * @param document Block of data to be signed.
     * @param offset Start offset of document.
     * @param size Number of bytes to be signed.
     * @return Signature of <code>document</code>
     * @exception IOException Any error when communicating with the HSM.
     */
    public byte[] sign(String signer, byte[] document, int offset, int size)
	throws IOException
    {
	try {
	    MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
	    sha1.update(document, offset, size);
	    byte[] hash = sha1.digest();
	    HSMFunction func = new HSMSignRequest(signer);
	    return execute(hash, 0, hash.length, func);
	} catch (NoSuchAlgorithmException e) {
	    IOException ioe = new IOException(e.getMessage());
	    ioe.setStackTrace(e.getStackTrace());
	    throw ioe;
	}
    }

    /**
     * Digitally signed a block of data.
     * @param signer ID of the identity used for signing.
     * @param document Block of data to be signed.
     * @return Signature of <code>document</code>
     * @exception IOException Any error when communicating with the HSM.
     */
    public byte[] sign(String signer, byte[] document) throws IOException {
	return sign(signer, document, 0, document.length);
    }


    /**
     * Reqest HSM server to do self check.
     * Only for testing, zz
     * @return status of HSM server. "OK" if HSM server is ok.
     * @exception IOException Any error when communicating with the HSM.
     */
    public byte[] selfCheck() throws IOException {
	HSMFunction func = new HSMSelfCheckRequest();
	return execute(null, 0, 0, func);
    }


    /**
     * Digitally signed a block of data.
     * Only for testing, zz
     * @param signer ID of the identity used for signing.
     * @param document Block of data to be signed.
     * @return Signature of <code>document</code>
     * @exception IOException Any error when communicating with the HSM.
     */
    public byte[] signUnhashed(String signer, byte[] document)
        throws IOException
    {
        HSMFunction func = new HSMSignRequest(signer, false);
        return execute(document, 0, document.length, func);
     }

    private CertInfo getCert(HSMFunction func)
	throws IOException
    {
	try {
	    return parseCertReply(execute(null, 0, 0, func));
	} catch (IOException e) {
	    throw e;
	} catch (Exception e) {
	    throw new IOException(e.getMessage());
	}
    }


    private CertInfo getCert(String cmd, String name)
	throws IOException
    {
	return getCert(new HSMCertRequest(cmd, name));
    }

    private CertInfo getCert(String cmd, String name, String password)
	throws IOException
    {
	return getCert(new HSMCertRequest(cmd, name, password));
    }

    /**
     * Request for a new certficate for a BB Depot.
     * @param name <em>Common name</em> to be used in the Subject DN
     * field of the certificate.
     * @return A <code>CertInfo</code> object containing the cert.,
     * the corresponding private key, and the password to unlock the
     * private key.
     * @exception IOException Any error when communicating with the HSM.
     */
    public CertInfo getDepotCert(String name) throws IOException {
	return getCert(Constants.CMD_NEW_DEPOT, name);
    }


    /**
     * Request for a new certficate for a BB Server.
     * @param name <em>Common name</em> to be used in the Subject DN
     * field of the certificate.
     * @return A <code>CertInfo</code> object containing the cert.,
     * the corresponding private key, and the password to unlock the
     * private key.
     * @exception IOException Any error when communicating with the HSM.
     */
    public CertInfo getServerCert(String name) throws IOException {
	return getCert(Constants.CMD_NEW_SERVER, name);
    }


    /**
     * Request for a new certficate for a smart card.
     * @param name <em>Common name</em> to be used in the Subject DN
     * field of the certificate.
     * @return A <code>CertInfo</code> object containing the cert.,
     * the corresponding private key, and the password to unlock the
     * private key.
     * @exception IOException Any error when communicating with the HSM.
     */
    public CertInfo getSmartCardCert(String name, String password)
	throws IOException
    {
	return getCert(Constants.CMD_NEW_SMART_CARD, name, password);
    }


    byte[] execute(byte[] data, int offset, int size, HSMFunction func)
	throws IOException
    {
	HSMHeaderOutputStream header =
	    new HSMHeaderOutputStream(Constants.HEADER_SIZE);
	func.compose(header, data == null ? 0 : size);
        // for testing only, zz
        headerData = header.getInternalBuf();

	Socket sock = new Socket(host, port);
	try {
	    send(sock.getOutputStream(), header.getInternalBuf(), data,
		 offset, size);
	    sock.shutdownOutput();
	    sock.setSoTimeout(15000); // timeout set to 15 seconds.
	    byte[] result = recv(sock.getInputStream());
	    return result;
	} finally {
	    sock.close();
	}
    } // execute

    // for testing only, zz
    public byte[] getHeaderData() {
        return headerData;
    }

    void send(OutputStream out, byte[] header, byte[] data, int offset,
	      int size)
	throws IOException
    {
	out.write(header);
	if (data != null)
	    out.write(data, offset, size);
	out.flush();
    }

    byte[] recv(InputStream in) throws IOException
    {
	ByteArrayOutputStream result = new ByteArrayOutputStream(256);
	int n;
	byte[] buffer = new byte[256];
	while ((n = in.read(buffer)) > 0) {
	    result.write(buffer, 0, n);
	}
	result.flush();
	if (result.size() == 0)
	    throw new IOException("No response");
	return result.toByteArray();
    }


    CertInfo parseCertReply(byte[] response) throws IOException {
	// extract the header
	byte[] header = new byte[Constants.HEADER_SIZE];
	System.arraycopy(response, 0, header, 0, Constants.HEADER_SIZE);

	// parse the header
	Properties prop = new Properties();
	prop.load(new ByteArrayInputStream(header));
	if (! prop.getProperty(Constants.VERSION).equals(Constants.VERSION_STRING))
	    throw new IOException("Invalid version");

	int pwLen =
	    Integer.parseInt(prop.getProperty(Constants.REPLY_PASSWORD_LEN));
	int keyLen =
	    Integer.parseInt(prop.getProperty(Constants.REPLY_KEY_LEN));
	int certLen =
	    Integer.parseInt(prop.getProperty(Constants.REPLY_CERT_LEN));
	int chainLen =
	    Integer.parseInt(prop.getProperty(Constants.REPLY_CHAIN_LEN));
	int rootLen =
	    Integer.parseInt(prop.getProperty(Constants.REPLY_ROOT_LEN));
	int keyStoreLen =
	    Integer.parseInt(prop.getProperty(Constants.REPLY_KEYSTORE_LEN));
	int trustedStoreLen =
	    Integer.parseInt(prop.getProperty(Constants.REPLY_TRUSTED_STORE_LEN));
	int chainIDLen =
	    Integer.parseInt(prop.getProperty(Constants.REPLY_CHAIN_ID_LEN));

	// Set up the return value
	byte[] pw = new byte[pwLen];
	byte[] key = new byte[keyLen];
	byte[] cert = new byte[certLen];
	byte[] chain = new byte[chainLen];
	byte[] root = new byte[rootLen];
	byte[] keyStore = new byte[keyStoreLen];
	byte[] trustedStore = new byte[trustedStoreLen];
	byte[] chainID = new byte[chainIDLen];

	int i = Constants.HEADER_SIZE;
	System.arraycopy(response, i, pw, 0, pwLen);
	i += pwLen;
	System.arraycopy(response, i, key, 0, keyLen);
	i += keyLen;
	System.arraycopy(response, i, cert, 0, certLen);
	i += certLen;
	System.arraycopy(response, i, chain, 0, chainLen);
	i += chainLen;
	System.arraycopy(response, i, root, 0, rootLen);
	i += rootLen;
	System.arraycopy(response, i, keyStore, 0, keyStoreLen);
	i += keyStoreLen;
	System.arraycopy(response, i, trustedStore, 0, trustedStoreLen);
	i += trustedStoreLen;
	System.arraycopy(response, i, chainID, 0, chainIDLen);


	return new CertInfo(key, pw, cert, chain, root, keyStore,
			    trustedStore, chainID);
    }
}

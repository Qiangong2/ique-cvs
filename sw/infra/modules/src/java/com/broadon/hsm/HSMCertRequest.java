package com.broadon.hsm;

import java.io.PrintWriter;

/**
 * Implement the certificate request header composition. 
 */
public class HSMCertRequest implements HSMFunction
{
    final String cmd;
    final String name;
    final String password;

    /**
     * Specify what kind of cert is needed, and the common name.  A
     * randomly generated password will be used for protecting the
     * private key.
     * @param cmd Set to <code>Constants.CMD_NEW_DEPOT</code> for
     * depot certificates and <code>Constants.CMD_NEW_SERVER</code>
     * for server certificates.
     * @param name <em>Common name</em> field in the Subject DN of the
     * cert.
     */
    public HSMCertRequest(String cmd, String name) {
	this.cmd = cmd;
	this.name = name;
	this.password = null;
    }

    /**
     * Specify what kind of cert is needed, and the common name. 
     * @param cmd Set to <code>Constants.CMD_NEW_DEPOT</code> for
     * depot certificates and <code>Constants.CMD_NEW_SERVER</code>
     * for server certificates.
     * @param name <em>Common name</em> field in the Subject DN of the
     * cert.
     * @param password Password used to protect the generated private key.
     */
    public HSMCertRequest(String cmd, String name, String password) {
	this.cmd = cmd;
	this.name = name;
	this.password = password;
    }

    public void compose(HSMHeaderOutputStream out, int size) {
	PrintWriter writer = new PrintWriter(out);
	writer.println(Constants.VERSION + " = " + Constants.VERSION_STRING);
	writer.println(Constants.COMMAND + " = " + cmd);
	writer.println(Constants.NEW_CERT_NAME + " = " + name);
	if (password != null)
	    writer.println(Constants.PASSWORD + " = " + password);
	writer.close();
	out.pad(' ');
    }
}

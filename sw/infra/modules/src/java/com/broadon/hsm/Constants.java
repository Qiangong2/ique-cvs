package com.broadon.hsm;

/**
 * Constants used in the protocol for talking to the HSM (Hardware Security Module).
 */
public class Constants
{
    /** Size of the request header. */
    public static final int HEADER_SIZE = 128;

    /** Key word for the protocol version. */
    public static final String VERSION = "Version";

    /** Current version string. */
    public static final String VERSION_STRING = "1.1";

    /** Key word for the request command. */
    public static final String COMMAND = "Command";

    /** Specify a signature request. */
    public static final String CMD_SIGN = "sign";

    /** Specify a signature request on SHA1-hashed value. */
    public static final String CMD_SIGN_HASHED = "signHashed";

    /** Specify a signer ID. */
    public static final String SIGNER = "signer";

    /** Request a new  random number. */
    public static final String CMD_RAND = "random";

    /** Size of the random number needed. */
    public static final String RANDOM_SIZE = "random_size";

    /** Specify a request for Depot certificate. */
    public static final String CMD_NEW_DEPOT = "new_depot";

    /** Specify a request for Server certificate. */
    public static final String CMD_NEW_SERVER = "new_server";

    /** Specify a request for Smart Card certificate. */
    public static final String CMD_NEW_SMART_CARD = "new_card";

    /** Specify a self check command. */
    public static final String CMD_SELF_CHECK = "self_check";

    /** Common name of the new certificate requested. */
    public static final String NEW_CERT_NAME = "cert_name";

    /** Password for protecting the private key. */
    public static final String PASSWORD = "password";

    /** Key word for the length of data. */
    public static final String DATA_LEN = "DataLength";

    /** Key word for the length of the password for the private key. */
    public static final String REPLY_PASSWORD_LEN = "passwdLength";

    /** Key word for the length of the private key data. */
    public static final String REPLY_KEY_LEN = "keyLength";

    /** Key word for the length of the certificate data. */
    public static final String REPLY_CERT_LEN = "certLength";

    /** Key word for the length of the certificate chain. */
    public static final String REPLY_CHAIN_LEN = "chainLen";

    /** Key word for the length of the root certs. */
    public static final String REPLY_ROOT_LEN = "rootLen";

    /** Key word for the length of the key store. */
    public static final String REPLY_KEYSTORE_LEN = "ksLen";

    /** Key word for the length of the trusted store. */
    public static final String REPLY_TRUSTED_STORE_LEN = "tsLen";

    /** Key word for the lengt of the chain id. */
    public static final String REPLY_CHAIN_ID_LEN = "chIDLen";
}

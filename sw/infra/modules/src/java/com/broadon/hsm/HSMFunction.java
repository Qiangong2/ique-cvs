package com.broadon.hsm;

/**
 * Interface for composing a security function request header.
 */
public interface HSMFunction
{
    /**
     * Compose a header for the security function request.
     * @param out Output stream for holding the header.
     * @param size Size of extra data excluding the header (could be zero).
     */
    public void compose(HSMHeaderOutputStream out, int size);
}

package com.broadon.hsm;

import java.io.PrintWriter;

/**
 * Implement the random bytes request header composition. 
 */
public class HSMSelfCheckRequest implements HSMFunction
{

    public HSMSelfCheckRequest() {
    }

    public void compose(HSMHeaderOutputStream out, int size) {
	PrintWriter writer = new PrintWriter(out);
	writer.println(Constants.VERSION + " = " + Constants.VERSION_STRING);
	writer.println(Constants.COMMAND + " = " + Constants.CMD_SELF_CHECK);
	writer.close();
	out.pad(' ');
    }
	
}

package com.broadon.hsm;

import java.io.PrintWriter;

/**
 * Implement the signature request header composition.
 */
public class HSMSignRequest implements HSMFunction
{
    final String signer;
    final boolean hashed;

    /**
     * Specify who should sign the document.
     */
    public HSMSignRequest(String signer, boolean hashed) {
        this.signer = signer;
        this.hashed = hashed;
    };

    /**
     * Specify who should sign the document.
     */
    public HSMSignRequest(String signer) {
        this(signer, true);
    };

    public void compose(HSMHeaderOutputStream out, int size) {
	PrintWriter writer = new PrintWriter(out);
	writer.println(Constants.VERSION + " = " + Constants.VERSION_STRING);
        if (hashed) {
	    writer.println(Constants.COMMAND + " = " + Constants.CMD_SIGN_HASHED);
        } else {
	    writer.println(Constants.COMMAND + " = " + Constants.CMD_SIGN);
        }
	writer.println(Constants.SIGNER + " = " + signer);
	writer.println(Constants.DATA_LEN + " = " + size);
	writer.close();
	out.pad(' ');
    }
}

/*
 * (C) 2004-2005, BroadOn Communications Corp.,
 * $Id: ProcessUpload.java,v 1.2 2005/05/28 19:44:40 vaibhav Exp $
 */
package com.broadon.bccs;

import java.util.*;
import java.io.*;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.text.DateFormat;

import javax.mail.*;
import javax.mail.internet.*;
import javax.sql.DataSource;
import oracle.jdbc.OracleConnectionWrapper;

import org.apache.xerces.parsers.DOMParser;
import org.xml.sax.SAXException;
import org.w3c.dom.*;

import oracle.sql.BLOB;
import com.broadon.db.OracleDataSource;

public class ProcessUpload
	implements Runnable
{
    private String reqId;
    private String pkg;
    private String logfile;
    private String batchCommit;
    private String xmlRow; 
    private String validColumns;
    private String contentID;
    private String cksum;

    private String mesg;
    private boolean mException;

    private int procCount;	
    private int rowCount;
    private int from;
    private int to;	

    private Properties mProp;
    private DataSource mDataSource;
    private UploadAdmin mUA;

    private String[] dbReqID;
    private String[] dbJobName;

    private static final String INSERT = "INS";
    private static final String UPDATE = "UPD";
    private static final String INSERT_UPDATE = "INSUPD";
    private static final String DATA_LOAD_REQUESTS = "DATA_LOAD_REQUESTS";

    private static final String COUNTER_UPDATE_FAILED_HDR = "Alert: Process Upload - Counter Update Failed";
    private static final String COUNTER_UPDATE_FAILED_MSG = "**** Counter Update Failed ****";
    private static final String PROCESS_UPLOAD_FAILED_HDR = "ALert: Process Upload Failed";
    private static final String PROCESS_UPLOAD_FAILED_MSG = "**** Process Upload Failed ****";
    private static final String MAX_TRIAL_REACHED_HDR = "Alert: Process Upload - Max Trial Reached";
    private static final String MAX_TRIAL_REACHED_MSG = "**** Max Trial Reached ****";

    private static final String PROCESSING_FAILED_MESSAGE = "Posting failed: ";

    private static final String BCCS_PROP_FILE = "/opt/broadon/data/svcdrv/webapps/bccs/BBserver.properties";
    private static final String SPOOL = "SPOOL_FOLDER";

    private static final String SLEEP_TIME = "PROCESS_THREAD_SLEEP_TIME";

    public ProcessUpload (DataSource ds)
    {
        this.reqId = null;
        this.pkg = null;
        this.logfile = null;
        this.batchCommit = null;
        this.xmlRow = null;
        this.validColumns = null;
        this.contentID = null;
        this.cksum = null;

        this.mesg = null;
        this.mException = false;

        this.procCount = 0;
        this.rowCount = 0;
        this.from = 0;
        this.to = 0;

        this.dbReqID = null;
        this.dbJobName = null;
        this.mUA = null;

        this.mDataSource = ds;

        // Read the properties file
        try
        {
            this.mProp = new Properties();
            FileInputStream in = new FileInputStream(BCCS_PROP_FILE);
            this.mProp.load(in);
            in.close();

        } catch (Exception e) {
            WriteLog("Exception while reading properties: " + e.toString());
        }
    }

// Called by the thread. Initializes all the variables since only one instance
// is used both by the thread and the servlet for processing
    public synchronized String doProcessing(String job)
    {
        String tmpMesg = "";
        Connection wrappedConnection = null;
        Connection conn = null;

        try
        {

            // Every time the thread wakes up it gets a new connection and checks if there are 
            // any jobs to process. The thread closes the connection after the all jobs are
            // processed before going to sleep

            conn = wrappedConnection = mDataSource.getConnection();
            if (conn instanceof OracleDataSource.ConnectionWrapper)
                conn = ((OracleConnectionWrapper) conn).unwrap();
            conn.setAutoCommit(false);

            mUA = new UploadAdmin();

            String spool = mProp.getProperty(SPOOL);

            if (job != null && !job.equals(""))
                mUA.executeVerifyJobToProcess(job, conn);
            else
                mUA.executeGetJobsToProcess(conn);

            dbJobName = mUA.getJobName();
            dbReqID = mUA.getRequestID();

            WriteLog("START processing jobs...");

            for (int n = 0; (dbJobName != null && n < dbJobName.length); n++)
            {
                Properties jobProp = null;
                String filePath = null;
                File fileLock = null;
                tmpMesg = "";

                String jobName = dbJobName[n].substring(0, dbJobName[n].lastIndexOf(".")+1) + "properties";
                String jobLock = dbJobName[n].substring(0, dbJobName[n].lastIndexOf(".")+1) + "lock";

                if (spool.endsWith("/"))
                {
                    filePath = spool + jobName;
                    fileLock = new File(spool + jobLock);

                } else {
                    filePath = spool + "/" + jobName;
                    fileLock = new File(spool + "/" + jobLock);
                }

                try 
                {
                    // Create a lock file so no other threads can work with the same file

                    if (fileLock.createNewFile())
                    {
                        WriteLog("Found job to process: " + jobName);
                        jobProp = ReadSpoolFile(filePath);

                        String counter = jobProp.getProperty("Counter");
                        String[] user = new String[1];
                        user[0] = jobProp.getProperty("User");

                        if (counter!=null && !counter.equals("") &&
                            Integer.parseInt(counter) >= 0 && Integer.parseInt(counter) < 3)
                        {
                            // Process the current property file

                            ProcessJob(jobProp, conn);

                            // Verify that the request id exists in the property file 
                            // and matches the one obtained from the database

    		            if (reqId != null && !reqId.equals("") && reqId.equals(dbReqID[n])) 
                            {

                                // Verify that there were no exceptions while processing the upload

                                if (!mException) 
                                {
                                    WriteLog("Updating status for REQUEST ID: " + reqId);

                                    // Verify the status for this job is successfully updated.
                                    // Rollback upon any failure, increment the counter and send
                                    // an alert to admin

                                    if (!mUA.executeStatusUpdate(getStatusUpdateXML(), conn))
                                    {
                                        conn.rollback();
                                        tmpMesg = "Status could not be updated in the database." +
                                                  "\nAll changes have been rolled back." +
                                                  "\nThe system will try to post again." +
                                                  "\nPlease check the status later!";

					WriteLog("ERROR -- " + "Status could not be updated for REQUEST ID: " + reqId);

                                        if (!mUA.setCounter(filePath, String.valueOf(Integer.parseInt(counter)+1)))
                                            mUA.postMail(null, COUNTER_UPDATE_FAILED_HDR,
                                                         COUNTER_UPDATE_FAILED_MSG +
                                                         "\n\nStatus could not be updated for REQUEST ID: " + reqId +
                                                         "\n\nSetting counter to " + String.valueOf(Integer.parseInt(counter)+1) +
                                                         " failed in file " + filePath, mProp);
                                    } else {
      		                
                                        // Committ the processing, if there were no exception and no failures 
                                        conn.commit();
                                    }

                                    // Verify that there were no other failures
                                    // Rollback upon any failure, set counter to -1 and send
                                    // an alert to the user and admin

                                    if (mesg!=null && !mesg.equals(""))
                                    {
                                        WriteLog("FAIL REASON: "+mesg);
                                        if (mUA.setCounter(filePath, String.valueOf(-1)))
                                        {
                                            mUA.postMail(user, PROCESS_UPLOAD_FAILED_HDR,
                                                         PROCESS_UPLOAD_FAILED_MSG +
                                                         "\n\n" + jobName + " could not be processed because of the following reason:" +
                                                         "\n" + mesg, mProp);
                                        } else {
                                            mUA.postMail(null, COUNTER_UPDATE_FAILED_HDR,
                                                         COUNTER_UPDATE_FAILED_MSG +
                                                         "\n\n" + jobName + " could not be processed because of the following reason:" +
                                                         "\n" + mesg + "\n\nSetting counter to -1 failed in file " + filePath, mProp);
                                        }
                                    } else {

                                        WriteLog("SUCCESS -- " + dbJobName[n] + " successfully processed");
                                    }

                                } else {

                                    // Upon any exception, rollback set counter to -1 and send
                                    // an alert to the user and admin

                                    conn.rollback();

                                    WriteLog("ERROR -- EXCEPTION OCCURED, " + dbJobName[n] + " could not be processed -- ERROR");
                                    if (mUA.setCounter(filePath, String.valueOf(-1)))
                                    {
                                        mUA.postMail(user, PROCESS_UPLOAD_FAILED_HDR,
                                                     PROCESS_UPLOAD_FAILED_MSG +
                                                     "\n\n" + jobName + " could not be processed because of the following reason:" +
                                                     "\nException Occured: " + mesg, mProp);
                                    } else {
                                        mUA.postMail(null, COUNTER_UPDATE_FAILED_HDR,
                                                     COUNTER_UPDATE_FAILED_MSG +
                                                     "\n\n" + jobName + " could not be processed because of the following reason:" +
                                                     "\nException Occured: " + mesg + 
                                                     "\n\nSetting counter to -1 failed in file " + filePath, mProp);
                                    }
                                }

                            } else {

                                // If request id not found or it does not match, 
                                // rollback set counter to -1 and send
                                // an alert to the user and admin

                                conn.rollback();
                                tmpMesg = "System Error" +
                                          "\nThe package cannot be uploaded." +
                                          "\nRequest ID not found for the job." +
                                          "\nPlease try to upload the package again or re-package the content!";

                                WriteLog("ERROR -- NO REQUEST ID FOUND, " + dbJobName[n] + " could not be processed -- ERROR");
                                if (mUA.setCounter(filePath, String.valueOf(-1)))
                                {
                                    mUA.postMail(user, PROCESS_UPLOAD_FAILED_HDR,
                                                 PROCESS_UPLOAD_FAILED_MSG +
                                                 "\n\n" + jobName + " could not be processed because of the following reason:" +
                                                 "\nRequest ID mismatch/not found: Request ID in " + filePath + " is " + reqId +
                                                 " while that in database is " + dbReqID[n], mProp);
                                } else {
                                    mUA.postMail(null, COUNTER_UPDATE_FAILED_HDR,
                                                 COUNTER_UPDATE_FAILED_MSG +
                                                 "\n\n" + jobName + " could not be processed because of the following reason:" +
                                                 "\nRequest ID mismatch/not found: Request ID in " + filePath + " is " + reqId +
                                                 " while that in database is " + dbReqID[n] +
                                                 "\n\nSetting counter to -1 failed in file " + filePath, mProp);
                                }
                            }

                        } else {
 
                            // Send an alert to admin, when max trial is reached
                            // Set counter to -1

                            WriteLog("IGNORED -- Processing of " + jobName + " ignored, counter = " + counter);

                            if (counter!=null && counter.equals("3"))
                            {
                                if (mUA.setCounter(filePath, String.valueOf(-1)))
                                {
                                    mUA.postMail(null, MAX_TRIAL_REACHED_HDR,
                                                 MAX_TRIAL_REACHED_MSG +
                                                 "\n\nGiving up processing " + jobName +
                                                 "\nCounter = " + counter, mProp);
                                } else {
                                    mUA.postMail(null, COUNTER_UPDATE_FAILED_HDR,
                                                 COUNTER_UPDATE_FAILED_MSG +
                                                 "\n\nGiving up processing " + jobName +
                                                 "\nCounter = " + counter +
                                                 "\n\nSetting counter to -1 failed in file " + filePath, mProp);
                                }
                           }
                       }
  
                    } else
                        WriteLog("NOTE -- FILE LOCK CANNOT BE OBTAINED FOR " + jobName);

                } catch (IOException ie) {
                   ie.printStackTrace();
                   mesg = ie.toString();

                   WriteLog("IOException Occured (1): " + ie.toString());

                } finally {
                   
                   // Unlock the job property file by deleting the lock file
  
                   if (fileLock!=null && fileLock.exists())
                       fileLock.delete();
                }

            }

            WriteLog("DONE processing jobs!");

        } catch (SAXException se) {
            se.printStackTrace();
            tmpMesg = se.toString();

            WriteLog("SAXException Occured (1): " + se.toString());

        } catch (MessagingException me) {
            me.printStackTrace();
            tmpMesg = me.toString();

            WriteLog("MessagingException Occured (1): " + me.toString());

        } catch (IOException ie) {
            ie.printStackTrace();
            tmpMesg = ie.toString();

            WriteLog("IOException Occured (2): " + ie.toString());

        } catch (SQLException se) {
            se.printStackTrace();
            tmpMesg = se.toString();

            WriteLog("SQLException Occured (1): " + se.toString());

        } finally {
            try {
               wrappedConnection.close();
            } catch (SQLException se) {
               se.printStackTrace();
            }
        }

        if (mesg != null && !mesg.equals(""))
            return PROCESSING_FAILED_MESSAGE + "\n" + mesg;
        else if (tmpMesg != null && !tmpMesg.equals(""))
            return PROCESSING_FAILED_MESSAGE + "\n" + tmpMesg;

        return mesg;
        
    }

// Called from the login servlet in init()
   public void startProcessUpload()
   {
       Thread t = new Thread(this);
       t.start();
   }

// Thread runs periodically and performs validation
   public void run()
   {
       long sleepTime = 600000;
       String cfgSleepTime = mProp.getProperty(SLEEP_TIME);

       if (cfgSleepTime!=null && !cfgSleepTime.equals(""))
           sleepTime = Long.parseLong(cfgSleepTime);

       WriteLog("Process Upload thread started");

       while(true)
       {
           // When the processing thread wakes up, call doProcessing to check database
           /// for more jobs to process

           String dummy = doProcessing(null);

           try
           {
               // Sleep

               Thread.currentThread().sleep(sleepTime);

           } catch(Exception ex) {
               WriteLog("Exception while executing thread: " + ex.toString());
           }
       }
   }

//
// Main Functions to process uploads
//
   
/**************************************************************************
    * ReadSpoolFile function reads a given file in the spool
    **************************************************************************/
    static Properties ReadSpoolFile(String propFile)
	throws IOException
    {
        Properties prop = new Properties();
        FileInputStream in = new FileInputStream(propFile);
        prop.load(in);
        in.close();

	return prop;
    }

   /**************************************************************************
    * ProcessJob function is called to read a given property file from spool
    * and to start processing each xml file described, one at a time.
    **************************************************************************/
    private void ProcessJob(Properties jobProp, Connection conn)
	throws IOException
    {
        String REQUEST_ID = "RequestID";
        reqId = jobProp.getProperty(REQUEST_ID);

        String PACKAGE = "Package";
        pkg = jobProp.getProperty(PACKAGE);

        String LOG_FILE = "LogFile";
        logfile = jobProp.getProperty(LOG_FILE);

        String BATCH_COMMIT = "BatchCommit";
        batchCommit = jobProp.getProperty(BATCH_COMMIT);

        if (reqId != null && !reqId.equals("")) 
        {
            // Loop through to process all files in the property file

	    for (int i = 1; true; i++) 
            {
                mesg = null;
                mException = false;

                contentID = null;
                cksum = null;

		String xmlFile = jobProp.getProperty("File"+i);
		if (xmlFile != null) 
                {
   		    String tableName = jobProp.getProperty("Table"+i);
		    String cmd = jobProp.getProperty("Command"+i);
	            String colName = jobProp.getProperty("Column"+i);

		    xmlRow = "";
		    WriteLog("Processing File: " + xmlFile);			
 	            ParseFile(xmlFile, tableName, colName, cmd, conn);
                    WriteLog("Finished Processing File: " + xmlFile);

                    // Return if there is any failure

                    if ((mesg!=null && !mesg.equals("")) || mException)
                        return;
	        } else
                   // End of all the files to be processed
	           break;
	    }

        } else
            WriteLog("Request ID not found ! ");

        return;    
   }

   /**************************************************************************
    * ParseFile function parses the xmlFile and creates an XML document with
    * all valid columns in tableName. It calls ProcessFile function to process 
    * BATCH_SIZE number of rows at one time.
    **************************************************************************/
    private void ParseFile(String xmlFile, String tableName, String colName, 
                          String cmd, Connection conn)
    {
	try 
        {
            // Obtain all column names from the database in the table
            // so that the tags in the xml files can be matched with them

	    validColumns = mUA.executeGetColumns(tableName, conn);

        } catch (IOException ie) {
            mException = true;
            mesg = ie.toString();

   	    WriteLog("IOException Occured (3): " + ie.toString());
   	    ie.printStackTrace();
   	    return;

        } catch (SQLException se) {
            mException = true;
            mesg = se.toString();

   	    WriteLog("SQLException Occured (2): " + se.toString());
   	    se.printStackTrace();
	    return;

        }		

        NodeList children = null;

	try 
        {
            // Create a DOM and obtain children

            DOMParser parser = new DOMParser();
            parser.parse(xmlFile);

            Document document = parser.getDocument();
            Element RootElement = document.getDocumentElement();
            children = RootElement.getChildNodes();

        } catch (IOException ie) {
            mException = true;
            mesg = ie.toString();

   	    WriteLog("IOException Occured (4): " + ie.toString());
   	    ie.printStackTrace();
   	    return;

        } catch (SAXException se) {
            mException = true;
            mesg = se.toString();

   	    WriteLog("SAXException Occured (2): " + se.toString());
   	    se.printStackTrace();
   	    return;

        }		

        // Default value of the size

        int size = 100;

        if (xmlFile.endsWith("CONTENT_CACHE.xml")) 
        {
            // For CONTENT_CACHE, we set the size = 1
            // and load one CONTENT OBJECT at a time

            size = 1;

        } else {
            String propertySize = mProp.getProperty("BATCH_SIZE");

            if (propertySize != null && !(propertySize.equals("")))
                size = Integer.parseInt(propertySize);
        }

        WriteLog("Batch Size: " + String.valueOf(size));

        // Initialize all the counters

        int tmpCount = 0;

        procCount = 0;
        to = 0;
        from = 0;
        
	do 
        {
            // tmpCount keeps track of already processed rows

            tmpCount = procCount;
            rowCount = 0;

            // get the values for the next batch

	    from = procCount + 1;
	    to = procCount + size;

  	    try 
            {
                // Post the rows in the current batch

 		ProcessFile(children, tableName, colName, cmd, conn);

                // Process incoming

                mUA.executeProcess(conn);
                
                if (xmlFile.endsWith("CONTENT_CACHE.xml")) 
                {
                    if (tmpCount == procCount) 
                    {
                        // Commit one object at a time, if BatchCommit is set to 1
                        // else the commit will happen when all the files are successfully processed

                        if (batchCommit!=null && batchCommit.equals("1"))
                            conn.commit();
   
                        return;
                    }

		    if (!mUA.loadObject(cksum, 
                                        xmlFile.substring(0, xmlFile.indexOf("CONTENT_CACHE.xml"))+"CONTENT_OBJECT."+contentID, 
                                        conn))
                        WriteLog("Content Object Exists with Checksum: " + cksum + " - NO UPDATE DONE");
    

                } else {

                    // Commit a batch, if BatchCommit is set to 1 else the commit will 
                    // happen when all the files are successfully processed

                    if (batchCommit!=null && batchCommit.equals("1"))
                        conn.commit();
                }

                WriteLog("Total Processed: " + String.valueOf(procCount));

            } catch (SQLException se) {
                mException = true;
                mesg = se.toString();

   	        WriteLog("SQLException Occured (3): " + se.toString());
     	        se.printStackTrace();
   	        return;

            } catch (IOException ie) {
                mException = true;
                mesg = ie.toString();

   	        WriteLog("IOException Occured (5): " + ie.toString());
     	        ie.printStackTrace();
   	        return;

            }
         
          // Continue looping for as long as there is more data to be processed
          // than the batch size

        } while (procCount == to);
		
	return;
    }

   /**************************************************************************
    * ProcessFile function basically goes through the XML Document and creates
    * xmlRow from the node lists. It calls itself recursively to traverse through
    * the entire node collection length. The processing is however limited to the 
    * BATCH_SIZE.
    **************************************************************************/
    private void ProcessFile(NodeList NodeCollection, String tableName, String colName, 
  			     String cmd, Connection conn) 
	throws SQLException
    {
        String local = null;

        if (NodeCollection != null) 
        {
            for (int i=0; i< NodeCollection.getLength(); i++)
            {
                local = NodeCollection.item(i).getNodeName();

                if (NodeCollection.item(i).getNodeType() == Node.ELEMENT_NODE)
                {                       
                    if (local.toUpperCase().equals("ROW"))
                        rowCount++;

                    if (rowCount >= from && rowCount <= to)
                    {
                        if (local.toUpperCase().equals("ROW")) 
                        {
                            procCount++;
         	            xmlRow = "<ROW";	
	
       	                    if (NodeCollection.item(i).hasAttributes()) 
                            {
                                NamedNodeMap AttributesList = NodeCollection.item(i).getAttributes();

                                for (int j = 0; j < AttributesList.getLength(); j++) 
                                    xmlRow += " " + AttributesList.item(j).getNodeName() + "=\"" + AttributesList.item(j).getNodeValue() + "\"";
                            }
                            xmlRow += ">";                       

                        } else if (local.toUpperCase().equals("CONTENT_ID")) {
                            contentID = GetElementData(NodeCollection.item(i));

                        } else if (local.toUpperCase().equals("CONTENT_CHECKSUM")) {
                           cksum = GetElementData(NodeCollection.item(i));
                        }

	    	        if (validColumns.indexOf(" "+local.toUpperCase()+" ") >= 0) 
                        {
		            xmlRow += "<" + local.toUpperCase() + ">";
                            xmlRow += GetElementData(NodeCollection.item(i));
                        }

                        ProcessFile(NodeCollection.item(i).getChildNodes(), tableName, colName, cmd, conn);

                        if (validColumns.indexOf(" "+local.toUpperCase()+" ") >= 0 || local.toUpperCase().equals("ROW")) 
                            xmlRow += "</" + local.toUpperCase() + ">";                

                        if (local.toUpperCase().equals("ROW")) 
                            CheckExist(tableName, colName, cmd, conn);
                    }
                }
            }
        }
    }

//
// Utility Functions
//

   /**************************************************************************
    * GetElementData function returns the value of the child node
    **************************************************************************/
    private String GetElementData(Node parentNode) 
    {
        if (parentNode.getFirstChild() != null) 
        {
            int childType = parentNode.getFirstChild().getNodeType();
 
            if (childType == Node.TEXT_NODE) 
            {
                if (parentNode.getFirstChild().getNodeValue() != null)
                    return parentNode.getFirstChild().getNodeValue();           
            }
        }
        return "";
    }

   /**************************************************************************
    * getStatusUpdateXML function formulates the Data XML string for the
    * given request ID in order to update the status of the corresponding row 
    * in the DATA_LOAD_REQUESTS table in the database.
    **************************************************************************/
    private String getStatusUpdateXML()
    {
	String procDate = null;
        Calendar calendar = new GregorianCalendar();
        java.util.Date date = calendar.getTime();
        DateFormat utc = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
        utc.setTimeZone(TimeZone.getTimeZone("GMT+00:00"));
        procDate = utc.format(date);

	String DataXML = null;
	
        DataXML = "<ROWSET><ROW num=\"1\">";
        DataXML += "<REQUEST_ID>"+reqId+"</REQUEST_ID>";

        if (mesg!=null && !mesg.equals("")) 
        {
            DataXML += "<PROCESS_STATUS>Failed</PROCESS_STATUS>";

            if (pkg!=null && pkg.equals("RMA"))
                DataXML += "<PROCESS_XML>"+logfile+"</PROCESS_XML>";
            else
                DataXML += "<PROCESS_XML>"+mesg+"</PROCESS_XML>";
        } else {
            DataXML += "<PROCESS_STATUS>Processed</PROCESS_STATUS>";

            if (pkg!=null && pkg.equals("RMA"))
                DataXML += "<PROCESS_XML>"+logfile+"</PROCESS_XML>";
            else
                DataXML += "<PROCESS_XML></PROCESS_XML>";
        }

        DataXML += "<PROCESS_DATE>"+procDate+"</PROCESS_DATE>";
        DataXML += "</ROW></ROWSET>";

        return DataXML;
    }

   /**************************************************************************
    * CheckExist function verifies if the given xmlRow exists in tableName.
    * If it does then it tries to determine if the given data is newer than the
    * existing data. Based on this result and depending on the cmd, it decides 
    * whether xmlRow needs to be ignored, inserted, updated etc.
    **************************************************************************/
    private void CheckExist(String tableName, String colName, String cmd, Connection conn)
          throws SQLException
    {
	String xmlData = "<ROWSET>" + xmlRow + "</ROWSET>";

        int chkExist = mUA.executeCheck(xmlData, tableName, colName, conn);
	if (chkExist < 0) 
        {
            WriteLog("==> Recent check failed (" + chkExist + ") <=="); 	
            WriteLog("==> " + xmlData + " <=="); 	
        }

	if (cmd.equals("ii")) 
        {
	    if (chkExist == 0) 
		mUA.executePost(xmlData, tableName, INSERT, conn);   
        } else if (cmd.equals("iu")) {
	    if (chkExist == 0)
		mUA.executePost(xmlData, tableName, INSERT, conn);   
            else if (chkExist == 1 && colName != null)
	        mUA.executePost(xmlData, tableName, UPDATE, conn);   
        } else if (cmd.equals("ir")) {
            mUA.executePost(xmlData, tableName, INSERT_UPDATE, conn);   
        }
        xmlData = null;
    }

//
// Log Functions
//

   /**************************************************************************
    * WriteLog function is called to log messages into the log file
    **************************************************************************/
    private void WriteLog(String mesg) {
        java.util.Date d = new java.util.Date();
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss z");
        System.out.println(df.format(d) + ": " + mesg);
    }
}

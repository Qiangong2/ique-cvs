/*
 * (C) 2004-2005, BroadOn Communications Corp.,
 * $Id: ValidateUpload.java,v 1.3 2006/06/08 07:17:51 vaibhav Exp $
 */
package com.broadon.bccs;

import java.util.*;
import java.io.*;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.security.PublicKey;
import java.security.cert.CertificateParsingException;

import javax.mail.*;
import javax.mail.internet.*;
import javax.sql.DataSource;
import oracle.jdbc.OracleConnectionWrapper;

import org.apache.xerces.parsers.DOMParser;
import org.xml.sax.SAXException;
import org.w3c.dom.*;

import com.broadon.security.BBcert;
import com.broadon.security.RSAcert;
import com.broadon.db.DBAccessFactory;
import com.broadon.db.DBException;
import com.broadon.db.CertificateFactory;
import com.broadon.db.OracleDataSource;

public class ValidateUpload
	implements Runnable
{
    private String reqId;
    private String pkg;
    private String logfile;
    private String mpcCert;
    private String bbid;
    private String buid;
    private HashMap rowValues;
    private Properties mProp;
    private DataSource mDataSource;
    private UploadAdmin mUA;

    private String mesg;
    private boolean mException;

    private int index;
    private int[] coArray;
    private int[] saArray;

    private String[] dbReqID;
    private String[] dbJobName;

    private static final String COUNTER_UPDATE_FAILED_HDR = "Alert: Validate Upload - Counter Update Failed";
    private static final String COUNTER_UPDATE_FAILED_MSG = "**** Counter Update Failed ****";
    private static final String VALIDATE_UPLOAD_FAILED_HDR = "Alert: Validate Upload Failed";
    private static final String VALIDATE_UPLOAD_FAILED_MSG = "**** Validate Upload Failed ****";
    private static final String MAX_TRIAL_REACHED_HDR = "Alert: Validate Upload - Max Trial Reached";
    private static final String MAX_TRIAL_REACHED_MSG = "**** Max Trial Reached ****";

    private static final String VALIDATION_FAILED_MESSAGE = "Validation Failed: ";

    private static final String BCCS_PROP_FILE = "/opt/broadon/data/svcdrv/webapps/bccs/BBserver.properties";
    private static final String DATA_LOAD_REQUESTS = "DATA_LOAD_REQUESTS";
    private static final String SPOOL = "SPOOL_FOLDER";

    private static final String SLEEP_TIME = "VALIDATE_THREAD_SLEEP_TIME";

    public ValidateUpload (DataSource ds)
    {
        this.reqId = null;
        this.pkg = null;
        this.logfile = null;
        this.mpcCert = null;
        this.bbid = null;
        this.buid = null;
        this.rowValues = null;

        this.mesg = null;
        this.mException = false;

        this.index = 0;
        this.coArray = null;
        this.saArray = null;
        this.dbReqID = null;
        this.dbJobName = null;
        this.mUA = null;

        this.mDataSource = ds;

        // Read the properties file
        try 
        {
            this.mProp = new Properties();
            FileInputStream in = new FileInputStream(BCCS_PROP_FILE);
            this.mProp.load(in);
            in.close();

        } catch (Exception e) {
            WriteLog("Exception while reading properties: " + e.toString());
        }
    }

// Called by the thread. Initializes all the variables since only one instance 
// is used both by the thread and the servlet for doing validation
    public synchronized String doValidation(String job)
    {
        String tmpMesg = "";
        Connection wrappedConnection = null;
        Connection conn = null;
        
        try 
        {
            // Every time the thread wakes up it gets a new connection and checks if there are
            // any jobs to validate. The thread closes the connection after the all jobs are
            // validated before going to sleep

            conn = wrappedConnection = mDataSource.getConnection();
            if (conn instanceof OracleDataSource.ConnectionWrapper)
                conn = ((OracleConnectionWrapper) conn).unwrap();
            conn.setAutoCommit(false);

            mUA = new UploadAdmin();

	    String spool = mProp.getProperty(SPOOL);

            if (job != null && !job.equals(""))
                mUA.executeVerifyJobToValidate(job, conn);
            else
                mUA.executeGetJobsToValidate(conn);

            dbJobName = mUA.getJobName();
            dbReqID = mUA.getRequestID();
            
            WriteLog("START validating jobs...");

            for (int n = 0; (dbJobName != null && n < dbJobName.length); n++) 
            { 
		Properties jobProp = null;
		String filePath = null;
                File fileLock = null;
                tmpMesg = "";

                String jobName = dbJobName[n].substring(0, dbJobName[n].lastIndexOf(".")+1) + "properties";
                String jobLock = dbJobName[n].substring(0, dbJobName[n].lastIndexOf(".")+1) + "lock";

		if (spool.endsWith("/")) 
                {
		    filePath = spool + jobName;
		    fileLock = new File(spool + jobLock);

		} else {
		    filePath = spool + "/" + jobName;
		    fileLock = new File(spool + "/" + jobLock);
                }

                try 
                {
                    // Create a lock file so no other threads can work with the same file

                    if (fileLock.createNewFile())
                    {
                        WriteLog("Found job to validate: " + jobName);
                   
    		        jobProp = ReadSpoolFile(filePath);

                        String counter = jobProp.getProperty("Counter");
                        String[] user = new String[1];
                        user[0] = jobProp.getProperty("User");

                        if (counter!=null && !counter.equals("") && 
                            Integer.parseInt(counter) >= 0 && Integer.parseInt(counter) < 3)
                        {
                           // Validate the current property file

                            ValidateJob(jobProp, jobName, conn);

                            // Verify that the request id exists in the property file
                            // and matches the one obtained from the database
   
    		            if (reqId!=null && !reqId.equals("") && reqId.equals(dbReqID[n])) 
                            {
                                // Verify that there were no exceptions while validating the upload

                                if (!mException) 
                                {
                                    WriteLog("Updating status for REQUEST ID: " + reqId);
                         
                                    // Verify the status for this job is successfully updated.
                                    // Rollback upon any failure, increment the counter and send
                                    // an alert to admin

   	  	                    if (!mUA.executeStatusUpdate(getStatusUpdateXML(), conn))
                                    {
                                        conn.rollback();
                                        tmpMesg = "Status could not be updated in the database." +
                                                  "\nAll changes have been rolled back." +
                                                  "\nThe system will try to validate again." +
                                                  "\nPlease check the status later!";

       	    	                        WriteLog("ERROR -- " + "Status could not be updated for REQUEST ID: " + reqId);
                                    
   	  	  	  	        if (!mUA.setCounter(filePath, String.valueOf(Integer.parseInt(counter)+1)))
                                            mUA.postMail(null, COUNTER_UPDATE_FAILED_HDR, 
                                                         COUNTER_UPDATE_FAILED_MSG +
                                                         "\n\nStatus could not be updated for REQUEST ID: " + reqId +
                                                         "\n\nSetting counter to " + String.valueOf(Integer.parseInt(counter)+1) +
                                                         " failed in file " + filePath, mProp);
                                    } else {

                                        // Commit the validation, if there were no exception and no failures
                                        conn.commit();
                                    }

                                    // Verify that there were no other failures
                                    // Rollback upon any failure, set counter to -1 and send
                                    // an alert to the user and admin

                                    if (mesg!=null && !mesg.equals("")) 
                                    {
                                        WriteLog("FAIL REASON: "+mesg);
         	 	                if (mUA.setCounter(filePath, String.valueOf(-1)))
                                        {
                                            mUA.postMail(user, VALIDATE_UPLOAD_FAILED_HDR, 
                                                         VALIDATE_UPLOAD_FAILED_MSG + 
                                                         "\n\n" + jobName + " could not be validated because of the following reason:" +
                                                         "\n" + mesg, mProp);
                                        } else {
                                            mUA.postMail(null, COUNTER_UPDATE_FAILED_HDR, 
                                                         COUNTER_UPDATE_FAILED_MSG +
                                                         "\n\n" + jobName + " could not be processed because of the following reason:" +
                                                         "\n" + mesg + "\n\nSetting counter to -1 failed in file " + filePath, mProp);
                                        }

                                    } else {

     	                                WriteLog("SUCCESS -- " + dbJobName[n] + " successfully validated");
                                    }
  
                                } else {
 
                                    // Upon any exception, rollback set counter to -1 and send
                                    // an alert to the user and admin

                                    conn.rollback();
                                        
      		                    WriteLog("ERROR -- EXCEPTION OCCURED, " + dbJobName[n] + " could not be validated -- ERROR");
 		  	            if (mUA.setCounter(filePath, String.valueOf(-1)))
                                    {
                                        mUA.postMail(user, VALIDATE_UPLOAD_FAILED_HDR, 
                                                     VALIDATE_UPLOAD_FAILED_MSG + 
                                                     "\n\n" + jobName + " could not be validated because of the following reason:" +
                                                     "\nException Occured: " + mesg, mProp);
                                    } else {
                                        mUA.postMail(null, COUNTER_UPDATE_FAILED_HDR, 
                                                     COUNTER_UPDATE_FAILED_MSG +
                                                     "\n\n" + jobName + " could not be processed because of the following reason:" +
                                                     "\nException Occured: " + mesg +
                                                     "\n\nSetting counter to -1 failed in file " + filePath, mProp);
                                    }
                                }

    	     	            } else {			

                                // If request id not found or it does not match,
                                // rollback set counter to -1 and send
                                // an alert to the user and admin

                                conn.rollback();
                                tmpMesg = "System Error" +
                                          "\nThe package cannot be uploaded." +
                                          "\nRequest ID not found for the job." +
                                          "\nPlease try to upload the package again or re-package the content!";

		                WriteLog("ERROR -- NO REQUEST ID FOUND, " + dbJobName[n] + " could not be validated -- ERROR");
 			        if (mUA.setCounter(filePath, String.valueOf(-1)))
                                {
                                    mUA.postMail(user, VALIDATE_UPLOAD_FAILED_HDR, 
                                                 VALIDATE_UPLOAD_FAILED_MSG + 
                                                 "\n\n" + jobName + " could not be validated because of the following reason:" +
                                                 "\nRequest ID mismatch/not found: Request ID in " + filePath + " is " + reqId +
                                                 " while that in database is " + dbReqID[n], mProp);

                                } else {
                                    mUA.postMail(null, COUNTER_UPDATE_FAILED_HDR, 
                                                 COUNTER_UPDATE_FAILED_MSG +
                                                 "\n\n" + jobName + " could not be processed because of the following reason:" +
                                                 "\nRequest ID mismatch/not found: Request ID in " + filePath + " is " + reqId +
                                                 " while that in database is " + dbReqID[n] +
                                                 "\n\nSetting counter to -1 failed in file " + filePath, mProp);
                                }
                            }

                        } else {

                            // Send an alert to admin, when max trial is reached
                            // Set counter to -1

                            WriteLog("IGNORED -- Validation of " + jobName + " ignored, counter = " + counter);

                            if (counter!=null && counter.equals("3"))
                            {
                                if (mUA.setCounter(filePath, String.valueOf(-1)))
                                {
                                    mUA.postMail(null, MAX_TRIAL_REACHED_HDR,
                                                 MAX_TRIAL_REACHED_MSG +
                                                 "\n\nGiving up validation of " + jobName +
                                                 "\nCounter = " + counter, mProp);
                                } else {
                                    mUA.postMail(null, COUNTER_UPDATE_FAILED_HDR,
                                                 COUNTER_UPDATE_FAILED_MSG +
                                                 "\n\nGiving up validation of " + jobName +
                                                 "\nCounter = " + counter +
                                                 "\n\nSetting counter to -1 failed in file " + filePath, mProp);
                                }
                            }
                        }

                    } else 
   	                WriteLog("NOTE -- FILE LOCK CANNOT BE OBTAINED FOR " + jobName);

                } catch (IOException ie) {
	           ie.printStackTrace();
                   tmpMesg = ie.toString();

                   WriteLog("IOException Occured (1): " + ie.toString());

                } finally {

                   // Unlock the job property file by deleting the lock file

                   if (fileLock!=null && fileLock.exists())
                       fileLock.delete();
	        }

            }

	    WriteLog("DONE validating jobs!");

        } catch (SAXException se) {
	    se.printStackTrace();
            tmpMesg = se.toString();

            WriteLog("SAXException Occured (1): " + se.toString());

        } catch (MessagingException me) {
	    me.printStackTrace();
            tmpMesg = me.toString();

            WriteLog("MessagingException Occured (1): " + me.toString());

        } catch (IOException ie) {
	    ie.printStackTrace();
            tmpMesg = ie.toString();

            WriteLog("IOException Occured (2): " + ie.toString());

        } catch (SQLException se) {
	    se.printStackTrace();
            tmpMesg = se.toString();

            WriteLog("SQLException Occured (1): " + se.toString());

        } finally {
            try {
               wrappedConnection.close();
            } catch (SQLException se) {
 	       se.printStackTrace();
            }
        }

        if (mesg != null && !mesg.equals(""))
            return VALIDATION_FAILED_MESSAGE + "\n" + mesg;
        else if (tmpMesg != null && !tmpMesg.equals(""))
            return VALIDATION_FAILED_MESSAGE + "\n" + tmpMesg;

        return mesg;
    }

// Called from the login servlet in init()
   public void startValidateUpload()
   {
       Thread t = new Thread(this);
       t.start();
   }

// Thread runs periodically and performs validation
   public void run()
   {
       long sleepTime = 600000;
       String cfgSleepTime = mProp.getProperty(SLEEP_TIME);

       if (cfgSleepTime!=null && !cfgSleepTime.equals(""))
           sleepTime = Long.parseLong(cfgSleepTime);

       WriteLog("Validate Upload thread started");

       while(true) 
       {
           // When the validating thread wakes up, call doValidation to check database
           /// for more jobs to validate

           String dummy = doValidation(null);

           try 
           {
               // Sleep

               Thread.currentThread().sleep(sleepTime);

           } catch(Exception ex) {
               WriteLog("Exception while executing thread: " + ex.toString());
           }
       }
   }
   
//
// Main Functions to validate uploads
//

   /**************************************************************************
    * ReadSpoolFile function reads a given file in the spool
    **************************************************************************/
    private Properties ReadSpoolFile(String propFile)
	throws IOException
    {
        Properties prop = new Properties();
        FileInputStream in = new FileInputStream(propFile);
        prop.load(in);
        in.close();

	return prop;
    }

   /**************************************************************************
    * ValidateJob function is called to read a given property file from spool
    * and to start validating each xml file described, one at a time.
    **************************************************************************/
    private void ValidateJob(Properties jobProp, String fileName, Connection conn)
	throws IOException
    {
        String REQUEST_ID = "RequestID";
        reqId = jobProp.getProperty(REQUEST_ID);

        String PACKAGE = "Package";
        pkg = jobProp.getProperty(PACKAGE);

        String LOG_FILE = "LogFile";
        logfile = jobProp.getProperty(LOG_FILE);

        int size = 10;
        String maxContents = mProp.getProperty("MAX_CONTENTS");

        if (maxContents != null && !(maxContents.equals("")))
           size = Integer.parseInt(maxContents);

        coArray = new int[size];
        saArray = new int[size];

	Arrays.fill(coArray, -1);
	Arrays.fill(saArray, -1);

        rowValues = new HashMap();
        mpcCert = null;
        bbid = null;
        buid = null;

	if (reqId != null && !reqId.equals("")) 
        {
            // Loop through to process all files in the property file

	    for (int i = 0; true; i++) 
            {
                mesg = null;
                mException = false;
                index = 0;
                rowValues.clear();

                String xmlFile = jobProp.getProperty("File"+i);

                if (i == 0) 
                {
                    if (fileName.startsWith("content") || fileName.startsWith("sa") || (pkg!=null && pkg.equals("RMA"))) 
                    {
  		        if (xmlFile != null) 
                        {	
		            WriteLog("Validating File: " + xmlFile);			

                            if (fileName.startsWith("content") || fileName.startsWith("sa")) 
                            {
                                transformContentCache(xmlFile.substring(0, xmlFile.lastIndexOf("/")+1), "start");
                                transformContents(xmlFile.substring(0, xmlFile.lastIndexOf("/")+1), "start");
                            }

                            if (fileName.startsWith("content")) 
                            {
                                transformCEM(xmlFile.substring(0, xmlFile.lastIndexOf("/")+1), "start");
                                transformCTO(xmlFile.substring(0, xmlFile.lastIndexOf("/")+1), "start");
                            }

                            if (pkg!=null && pkg.equals("RMA"))
                                transformBBPlayerSerials(xmlFile.substring(0, xmlFile.lastIndexOf("/")+1), "start"); 

		            ParseFile(xmlFile, fileName, conn);

                            if (fileName.startsWith("content") || fileName.startsWith("sa")) 
                            {
                                transformContentCache(xmlFile.substring(0, xmlFile.lastIndexOf("/")+1), "end");
                                transformContents(xmlFile.substring(0, xmlFile.lastIndexOf("/")+1), "end");
                            }

                            if (fileName.startsWith("content")) 
                            {
                                transformCEM(xmlFile.substring(0, xmlFile.lastIndexOf("/")+1), "end");
                                transformCTO(xmlFile.substring(0, xmlFile.lastIndexOf("/")+1), "end");
                            }

                            if (pkg!=null && pkg.equals("RMA"))
                                transformBBPlayerSerials(xmlFile.substring(0, xmlFile.lastIndexOf("/")+1), "end"); 

                            WriteLog("Finished Validating File: " + xmlFile);

                           // Return if there is any failure

                            if ((mesg!=null && !mesg.equals("")) || mException)
                                return; 

 		        } else
	                    break;
                    }

                } else {
	            if (xmlFile!=null) 
                    {	
		        WriteLog("Validating File: " + xmlFile);			
		        ParseFile(xmlFile, fileName, conn);
                        WriteLog("Finished Validating File: " + xmlFile);

                        if ((mesg!=null && !mesg.equals("")) || mException)
                            return; 
 		    } else
                        // End of all the files to be processed
	                break;
                }
            }  

        } else {
	    WriteLog("Request ID not found ! ");
        }

        return;
    }

   /**************************************************************************
    * ParseFile function parses the xmlFile and creates an XML document with
    * all valid columns in tableName. It then calls Validate function to  
    * validate the data in xmlFile
    **************************************************************************/
    private void ParseFile(String xmlFile, String fileName, Connection conn)
    {
        NodeList children = null;

	try 
        {
            // Create a DOM and obtain children

            DOMParser parser = new DOMParser();
            parser.parse(xmlFile);

            Document document = parser.getDocument();
            Element RootElement = document.getDocumentElement();
            children = RootElement.getChildNodes();

            if (fileName.startsWith("content") || fileName.startsWith("sa"))
                ValidateContent(children, xmlFile, fileName, conn);
            else if (fileName.startsWith("CA"))
                ValidateCA(children, xmlFile);
            else if (pkg!=null && pkg.equals("RMA"))
                ValidateRMA(children, xmlFile, conn);
            else
                ValidateLotChips(children, xmlFile, conn);

        } catch (IOException ie) {
            mException = true;
            mesg = ie.toString();

   	    WriteLog("IOException Occured (3): " + ie.toString());
   	    ie.printStackTrace();
   	    return;

        } catch (SAXException se) {
            mException = true;
            mesg = se.toString();

   	    WriteLog("SAXException Occured (2): " + se.toString());
   	    se.printStackTrace();
   	    return;
        }		

        return;
    }

//
// Validate Content/SA Upload Package
//

   /**************************************************************************
    * ValidateContent function basically goes through the XML Document. It calls 
    * itself recursively to traverse through the entire node collection length. 
    **************************************************************************/
    private void ValidateContent(NodeList NodeCollection, String xmlFile, String fileName, Connection conn) 
    {
        String local = null;

        if (NodeCollection != null) 
        {
            for (int i=0; i< NodeCollection.getLength(); i++) 
            {
                local = NodeCollection.item(i).getNodeName();

                if (NodeCollection.item(i).getNodeType() == Node.ELEMENT_NODE) 
                {                       
		    if (!local.toUpperCase().equals("ROW")) 
                    {
                        rowValues.put(local.toUpperCase(), GetElementData(NodeCollection.item(i)));

 		        if (local.toUpperCase().equals("CONTENT_ID")) 
                        {
			    String contentid = GetElementData(NodeCollection.item(i));
			    if (contentid!=null && !contentid.equals("")) 
                            {
			        coArray[index] = Integer.parseInt(contentid);
                                index++;
                            }
                        }

 		        if (local.toUpperCase().equals("SECURE_CONTENT_ID")) 
                        {
			    String contentid = GetElementData(NodeCollection.item(i));
			    if (contentid!=null && !contentid.equals("")) 
                            {
			        saArray[index] = Integer.parseInt(contentid);
                                index++;
                            }
                        }

 		        if (local.toUpperCase().equals("BOOT_CONTENT_ID")) 
                        {
			    String contentid = GetElementData(NodeCollection.item(i));
			    if (contentid!=null && !contentid.equals("")) 
                            {
			        saArray[index] = Integer.parseInt(contentid);
                                index++;
                            }
                        }

 		        if (local.toUpperCase().equals("DIAG_CONTENT_ID")) 
                        {
			    String contentid = GetElementData(NodeCollection.item(i));
			    if (contentid!=null && !contentid.equals("")) 
                            {
			        saArray[index] = Integer.parseInt(contentid);
                                index++;
                            }
                        }
		    } 
                    
                    ValidateContent(NodeCollection.item(i).getChildNodes(), xmlFile, fileName, conn);

		    if (local.toUpperCase().equals("ROW") && fileName.startsWith("content") && xmlFile.endsWith("CONTENT_OBJECTS.xml"))
                       validateContentObjects(xmlFile.substring(0, xmlFile.indexOf("CONTENT_OBJECTS.xml")), conn);

		    else if (local.toUpperCase().equals("ROW") && fileName.startsWith("content") && xmlFile.endsWith("CONTENT_TITLES.xml"))
                       validateContentTitles(conn);

		    else if (local.toUpperCase().equals("ROW") && fileName.startsWith("content") && xmlFile.endsWith("CONTENT_TITLE_REVIEWS.xml"))
                       validateContentTitleReviews();

		    else if (local.toUpperCase().equals("ROW") && fileName.startsWith("sa") && xmlFile.endsWith("CONTENT_OBJECTS.xml"))
		       validateSKSA(xmlFile.substring(0, xmlFile.indexOf("CONTENT_OBJECTS.xml")), conn);

		    else if (local.toUpperCase().equals("ROW") && fileName.startsWith("sa") && xmlFile.endsWith("BB_HW_RELEASES.xml"))
		       validateHWRev(conn);
                }
            }
        }
    }

   /**************************************************************************
    * validateSAData function basically validates the SKSA package. It matches
    * the content id's in BB_HW_RELASES and CONTENT_OBJECTS xml file and then
    * verifies if all corresponding CONTENT_OBJECTS are provided.
    **************************************************************************/
    private void validateHWRev(Connection conn)
    {
        try 
        {
            if (rowValues.get("BB_HWREV")==null || ((String)rowValues.get("BB_HWREV")).equals("")) 
            {
        	mesg = "BB_HWREV tag missing in BB_HW_RELEASES.xml";
                return;
            }

            if ((rowValues.get("BOOT_CONTENT_ID")==null || ((String)rowValues.get("BOOT_CONTENT_ID")).equals("")) &&
                (rowValues.get("DIAG_CONTENT_ID")==null || ((String)rowValues.get("DIAG_CONTENT_ID")).equals("")) &&
                (rowValues.get("SECURE_CONTENT_ID")==null || ((String)rowValues.get("SECURE_CONTENT_ID")).equals(""))) 
            {
        	mesg = "No Content tags in BB_HW_RELEASES.xml";
                return;
            }

            if (rowValues.get("CHIP_REV")==null || ((String)rowValues.get("CHIP_REV")).equals("")) 
            {
        	mesg = "CHIP_REV tag missing in BB_HW_RELEASES.xml";
                return;
            }

            if (rowValues.get("BB_MODEL")==null || ((String)rowValues.get("BB_MODEL")).equals("")) 
            {
     	        mesg = "BB_MODEL tag missing in BB_HW_RELEASES.xml";
                return;

            } else {
                if (mUA.countBBModel((String)rowValues.get("BB_MODEL"), conn) <=0 ) 
                {
      	            mesg = "BB_MODEL NOT FOUND ERROR (BB Model = " + (String)rowValues.get("BB_MODEL") + " in BB_HW_RELEASES.xml)";
                    return;
                }
            }

            Arrays.sort(coArray);
            Arrays.sort(saArray);

            if (!Arrays.equals(coArray, saArray)) 
            {
                mesg = "CONTENT ID MISMATCH IN CONTENT_OBJECTS.xml and BB_HW_RELEASES.xml";
                return;
            }

        } catch (IOException ie) {
            mException = true;
            mesg = ie.toString();

            ie.printStackTrace();
            WriteLog("IOException Occured (4): " + ie.toString());

        } catch (SQLException se) {
            mException = true;
            mesg = se.toString();

   	    WriteLog("SQLException Occured (2): " + se.toString());
     	    se.printStackTrace();

        } 
    }

   /**************************************************************************
    * validateSKSA function basically validates CONTENT_OBJECTS.xml file in 
    * the SKSA package. Besides other things it also verifies if all 
    * CONTENT_OBJECTS are provided.
    **************************************************************************/
    private void validateSKSA(String path, Connection conn)
    {
        try 
        {
            if (rowValues.get("CONTENT_ID")==null || ((String)rowValues.get("CONTENT_ID")).equals("")) 
            {
        	mesg = "CONTENT_ID tag missing in CONTENT_OBJECTS.xml";
                return;
            }

            if (rowValues.get("CONTENT_OBJECT_NAME")==null || ((String)rowValues.get("CONTENT_OBJECT_NAME")).equals("")) 
            {
        	mesg = "CONTENT_OBJECT_NAME tag missing in CONTENT_OBJECTS.xml";
                return;
            }

            if (rowValues.get("CONTENT_OBJECT_VERSION")==null || ((String)rowValues.get("CONTENT_OBJECT_VERSION")).equals("")) 
            {
        	mesg = "CONTENT_OBJECT_VERSION tag missing in CONTENT_OBJECTS.xml";
                return;
            }

            if (rowValues.get("CONTENT_CHECKSUM")==null || ((String)rowValues.get("CONTENT_CHECKSUM")).equals("")) 
            {
        	mesg = "CONTENT_CHECKSUM tag missing in CONTENT_OBJECTS.xml";
                return;
            }

            if (rowValues.get("CONTENT_SIZE")==null || ((String)rowValues.get("CONTENT_SIZE")).equals("")) 
            {
        	mesg = "CONTENT_SIZE tag missing in CONTENT_OBJECTS.xml";
                return;
            }

            if (rowValues.get("PUBLISH_DATE")==null || ((String)rowValues.get("PUBLISH_DATE")).equals("")) 
            {
        	mesg = "PUBLISH_DATE tag missing in CONTENT_OBJECTS.xml";
                return;
            }

            if (rowValues.get("CONTENT_OBJECT_TYPE")==null || ((String)rowValues.get("CONTENT_OBJECT_TYPE")).equals("")) 
            {
     	        mesg = "CONTENT_OBJECT_TYPE tag missing in CONTENT_OBJECTS.xml";
                return;
            } else {
                if (mUA.countContentObjectType((String)rowValues.get("CONTENT_OBJECT_TYPE"), conn) <=0 ) 
                {
      	            mesg = "CONTENT_OBJECT_TYPE NOT FOUND ERROR (Content Object Type = " + (String)rowValues.get("CONTENT_OBJECT_TYPE") + 
                    " in CONTENT_OBJECTS.xml)";
                    return;
                }
            }

            File co = new File(path+"CONTENT_OBJECT."+(String)rowValues.get("CONTENT_ID")); 
            if (!co.exists()) 
            {
    	        mesg = "CONTENT OBJECT NOT PROVIDED ERROR (Content ID = " + (String)rowValues.get("CONTENT_ID") + 
                       " in CONTENT_OBJECTS.xml)";
                return;
            }

            if (mUA.countContentID((String)rowValues.get("CONTENT_ID"), conn) != 0 ) 
            {
 	        mesg = "CONTENT_ID EXISTS ERROR, CANNOT CREATE NEW CONTENT " +
                       "(Content ID = " + (String)rowValues.get("CONTENT_ID") + " in CONTENT_OBJECTS.xml)";
                return;
            }

            transformContentCache(path, null);
            transformContents(path, null);

        } catch (IOException ie) {
            mException = true;
            mesg = ie.toString();

            ie.printStackTrace();
            WriteLog("IOException Occured (5): " + ie.toString());

        } catch (SQLException se) {
            mException = true;
            mesg = se.toString();

   	    WriteLog("SQLException Occured (3): " + se.toString());
     	    se.printStackTrace();

        }
    }

   /**************************************************************************
    * validateContentTitleReviews function basically validates the 
    * CONTENT_TITLE_REVIEWS.xml
    **************************************************************************/
    private void validateContentTitleReviews()
    {
        if (rowValues.get("TITLE_ID")==null || ((String)rowValues.get("TITLE_ID")).equals("")) 
        {
      	    mesg = "TITLE_ID tag missing in CONTENT_TITLE_REVIEWS.xml";
            return;
        }

        if (rowValues.get("REVIEWED_BY")==null || ((String)rowValues.get("REVIEWED_BY")).equals("")) 
        {
       	    mesg = "REVIEWED_BY tag missing in CONTENT_TITLE_REVIEWS.xml";
            return;
        }

        if (rowValues.get("REVIEW_DATE")==null || ((String)rowValues.get("REVIEW_DATE")).equals("")) 
        {
       	    mesg = "REVIEW_DATE tag missing in CONTENT_TITLE_REVIEWS.xml";
            return;
        }

        if (rowValues.get("UPDATE")==null || ((String)rowValues.get("UPDATE")).equals("")) 
        {
            mesg = "UPDATE tag missing in CONTENT_TITLE_REVIEWS.xml";
            return;
        }

        if (!((String)rowValues.get("UPDATE")).equals("N") && !((String)rowValues.get("UPDATE")).equals("Y")) 
        {
            mesg = "INVALID VALUE FOR UPDATE FLAG (Update = " + (String)rowValues.get("UPDATE") + " in CONTENT_TITLE_REVIEWS.xml)";
            return;
        }
    }

   /**************************************************************************
    * validateContentTitles function basically validates the CONTENT_TITLES.xml
    * file . It calls executeValidateTitle function to obtain the result. 
    * 
    * 0 = title does not exists
    * 1 = title exists
    **************************************************************************/
    private void validateContentTitles(Connection conn)
    {
        int result = -1;

        try 
        {
            if (rowValues.get("TITLE_ID")==null || ((String)rowValues.get("TITLE_ID")).equals("")) 
            {
        	mesg = "TITLE_ID tag missing in CONTENT_TITLES.xml";
                return;
            }

            if (rowValues.get("TITLE")==null || ((String)rowValues.get("TITLE")).equals("")) 
            {
        	mesg = "TITLE tag missing in CONTENT_TITLES.xml";
                return;
            }

            if (rowValues.get("UPDATE")==null || ((String)rowValues.get("UPDATE")).equals("")) 
            {
        	mesg = "UPDATE tag missing in CONTENT_TITLES.xml";
                return;
            }

            if (((String)rowValues.get("UPDATE")).equals("N")) 
            {
                result = mUA.executeValidateTitle((String)rowValues.get("TITLE_ID"), conn);
    
                if (result != 0) 
                {
		    mesg = "TITLE ID EXISTS ERROR, CANNOT CREATE NEW TITLE (Update = " +
                           (String)rowValues.get("UPDATE") + ", Title ID = " + (String)rowValues.get("TITLE_ID") + 
                           " in CONTENT_TITLES.xml)";
                    return;
                }

            } else if (((String)rowValues.get("UPDATE")).equals("Y")) {
                result = mUA.executeValidateTitle((String)rowValues.get("TITLE_ID"), conn);

                if (result != 1) 
                {
		    mesg = "TITLE ID NOT EXISTS ERROR, CANNOT UPDATE TITLE (Update = " + 
                           (String)rowValues.get("UPDATE") + ", Title ID = " + (String)rowValues.get("TITLE_ID") + 
                           " in CONTENT_TITLES.xml)";
                    return;
                }

            } else {
 	        mesg = "INVALID VALUE FOR UPDATE FLAG (Update = " + (String)rowValues.get("UPDATE") + " in CONTENT_TITLES.xml)";
                return;
            }

        } catch (SQLException se) {
            mException = true;
            mesg = se.toString();

   	    WriteLog("SQLException Occured (4): " + se.toString());
     	    se.printStackTrace();

        }
    }

   /**************************************************************************
    * validateContentObjects function basically validates the Content package. It 
    * calls executeValidateContent function to obtain the result. A negative
    * result indicates an error whereas a positive result is the content_id itself.
    * If the package is validated it then creates CONTENT_CACHE.xml, CONTENTS.xml,
    * CONTENT_TITLE_OBJECTS.xml and CONTENT_ETICKET_METADATA.xml files from the data 
    * already provided in CONTENT_OBJECTS.xml file
    **************************************************************************/
    private void validateContentObjects(String path, Connection conn)
    {
        int result = -1;

        try 
        {
            if (rowValues.get("CONTENT_ID")==null || ((String)rowValues.get("CONTENT_ID")).equals("")) 
            {
        	mesg = "CONTENT_ID tag missing in CONTENT_OBJECTS.xml";
                return;
            }

            if (rowValues.get("CONTENT_OBJECT_NAME")==null || ((String)rowValues.get("CONTENT_OBJECT_NAME")).equals("")) 
            {
        	mesg = "CONTENT_OBJECT_NAME tag missing in CONTENT_OBJECTS.xml";
                return;
            }

            if (rowValues.get("CONTENT_OBJECT_VERSION")==null || ((String)rowValues.get("CONTENT_OBJECT_VERSION")).equals("")) 
            {
        	mesg = "CONTENT_OBJECT_VERSION tag missing in CONTENT_OBJECTS.xml";
                return;
            }

            if (rowValues.get("CONTENT_CHECKSUM")==null || ((String)rowValues.get("CONTENT_CHECKSUM")).equals("")) 
            {
        	mesg = "CONTENT_CHECKSUM tag missing in CONTENT_OBJECTS.xml";
                return;
            }

            if (rowValues.get("CONTENT_SIZE")==null || ((String)rowValues.get("CONTENT_SIZE")).equals("")) 
            {
        	mesg = "CONTENT_SIZE tag missing in CONTENT_OBJECTS.xml";
                return;
            }

            if (rowValues.get("TITLE_ID")==null || ((String)rowValues.get("TITLE_ID")).equals("")) 
            {
        	mesg = "TITLE_ID tag missing in CONTENT_OBJECTS.xml";
                return;
            }

            if (rowValues.get("PUBLISH_DATE")==null || ((String)rowValues.get("PUBLISH_DATE")).equals("")) 
            {
        	mesg = "PUBLISH_DATE tag missing in CONTENT_OBJECTS.xml";
                return;
            }

            if (rowValues.get("UPDATE")==null || ((String)rowValues.get("UPDATE")).equals("")) 
            {
        	mesg = "UPDATE tag missing in CONTENT_OBJECTS.xml";
                return;
            }

            if (rowValues.get("CONTENT_OBJECT_TYPE")==null || ((String)rowValues.get("CONTENT_OBJECT_TYPE")).equals("")) 
            {
     	        mesg = "CONTENT_OBJECT_TYPE tag missing in CONTENT_OBJECTS.xml";
                return;
            } else {
                if (mUA.countContentObjectType((String)rowValues.get("CONTENT_OBJECT_TYPE"), conn) <=0 ) 
                {
      	            mesg = "CONTENT_OBJECT_TYPE NOT FOUND ERROR (Content Object Type = " + (String)rowValues.get("CONTENT_OBJECT_TYPE") + 
                           " in CONTENT_OBJECTS.xml)";
                    return;
                }
            }

            File co = new File(path+"CONTENT_OBJECT."+(String)rowValues.get("CONTENT_ID")); 
            if (!co.exists()) 
            {
    	        mesg = "CONTENT OBJECT NOT PROVIDED ERROR (Content ID = " + (String)rowValues.get("CONTENT_ID") + " in CONTENT_OBJECTS.xml)";
                return;
            }

            if (((String)rowValues.get("UPDATE")).equals("N")) 
            {
                result = mUA.executeValidateContent((String)rowValues.get("TITLE_ID"),
                                                    (String)rowValues.get("CONTENT_ID"),
                                                    (String)rowValues.get("CONTENT_OBJECT_NAME"),
                                                    (String)rowValues.get("CONTENT_OBJECT_VERSION"),
                                                    (String)rowValues.get("UPDATE"), conn);
    
                if (result == -1) 
                {
		    mesg = "TITLE ID EXISTS ERROR, CANNOT CREATE NEW TITLE (Update = " +
                           (String)rowValues.get("UPDATE") + ", Title ID = " + (String)rowValues.get("TITLE_ID") + 
                           " in CONTENT_OBJECTS.xml)";
                    return;
                } else if (result == -2) {
		    mesg = "CONTENT ID EXISTS ERROR, CANNOT CREATE NEW CONTENT (Update = " +
                           (String)rowValues.get("UPDATE") + ", Content ID = " + (String)rowValues.get("CONTENT_ID") + 
                           " in CONTENT_OBJECTS.xml)";
                    return;
                }

            } else if (((String)rowValues.get("UPDATE")).equals("Y")) {
                result = mUA.executeValidateContent((String)rowValues.get("TITLE_ID"),
                                                    (String)rowValues.get("CONTENT_ID"),
                                                    (String)rowValues.get("CONTENT_OBJECT_NAME"),
                                                    (String)rowValues.get("CONTENT_OBJECT_VERSION"),
                                                    (String)rowValues.get("UPDATE"), conn);

                if (result == -102) 
                {
		    mesg = "CONTENT OBJECT VERSION IS LOWER ERROR (Update = " +
                           (String)rowValues.get("UPDATE") + ", Content ID = " + (String)rowValues.get("CONTENT_ID") + ", Version = " + 
                           (String)rowValues.get("CONTENT_OBJECT_VERSION") + " in CONTENT_OBJECTS.xml)";
                    return;
                } else if (result == -103) {
		    mesg = "CONTENT ID IS DEFINED AND EXISTS but CONTENT MISMATCHED ERROR (Update = " + 
                           (String)rowValues.get("UPDATE") + ", Content ID = " + (String)rowValues.get("CONTENT_ID") + ", Name = " + 
                           (String)rowValues.get("CONTENT_OBJECT_NAME") + ", Version = " + (String)rowValues.get("CONTENT_OBJECT_VERSION") + 
                           " in CONTENT_OBJECTS.xml)";
                    return;
                }
            } else {
 	        mesg = "INVALID VALUE FOR UPDATE FLAG (Update = " + (String)rowValues.get("UPDATE") + " in CONTENT_OBJECTS.xml)";
                return;
            }

            transformContentCache(path, null);
            transformContents(path, null);
            transformCEM(path, null);
            transformCTO(path, null);

        } catch (IOException ie) {
            mException = true;
            mesg = ie.toString();

            ie.printStackTrace();
            WriteLog("IOException Occured (6): " + ie.toString());

        } catch (SQLException se) {
            mException = true;
            mesg = se.toString();

   	    WriteLog("SQLException Occured (5): " + se.toString());
     	    se.printStackTrace();

        } 
    }

   /**************************************************************************
    * transformContentCache function creates the CONTENT_CACHE.xml file
    **************************************************************************/
    private void transformContentCache(String path, String flag)
    {
        String xmlCC = path+"CONTENT_CACHE.xml";

        try 
        {
            File xml = new File(xmlCC);
            if (flag!=null && flag.equals("start") && xml.exists())
                xml.delete();

            FileWriter pfile = new FileWriter(xmlCC, true);
            PrintWriter pwrite = new PrintWriter(pfile);

            if (flag!=null && flag.equals("start"))
                pwrite.println("<ROWSET>");
            else if (flag!=null && flag.equals("end"))
                pwrite.println("</ROWSET>");
            else {
                pwrite.println("  <ROW num=\"0\">");
                pwrite.println("    <CONTENT_ID>"+(String)rowValues.get("CONTENT_ID")+"</CONTENT_ID>");
                pwrite.println("    <CONTENT_CHECKSUM>"+(String)rowValues.get("CONTENT_CHECKSUM")+"</CONTENT_CHECKSUM>");
                pwrite.println("    <CONTENT_OBJECT_TYPE>"+(String)rowValues.get("CONTENT_OBJECT_TYPE")+"</CONTENT_OBJECT_TYPE>");
                pwrite.println("    <CONTENT_SIZE>"+(String)rowValues.get("CONTENT_SIZE")+"</CONTENT_SIZE>");
                pwrite.println("    <CONTENT_OBJECT_NAME>"+(String)rowValues.get("CONTENT_OBJECT_NAME")+"</CONTENT_OBJECT_NAME>");
                pwrite.println("    <CONTENT_OBJECT_VERSION>"+(String)rowValues.get("CONTENT_OBJECT_VERSION")+"</CONTENT_OBJECT_VERSION>");
                pwrite.println("  </ROW>");
            }

            pwrite.close();

        } catch (IOException ie) {
            mException = true;
            mesg = ie.toString();

            WriteLog("IOException Occured (7): " + ie.toString());
            ie.printStackTrace();
        }
    }


   /**************************************************************************
    * transformContents function creates the CONTENTS.xml file
    **************************************************************************/
    private void transformContents(String path, String flag)
    {
        String xmlC = path+"CONTENTS.xml";

        try 
        {
            File xml = new File(xmlC);
            if (flag!=null && flag.equals("start") && xml.exists())
                xml.delete();

            FileWriter pfile = new FileWriter(xmlC, true);
            PrintWriter pwrite = new PrintWriter(pfile);

            if (flag!=null && flag.equals("start"))
                pwrite.println("<ROWSET>");
            else if (flag!=null && flag.equals("end"))
                pwrite.println("</ROWSET>");
            else {
                pwrite.println("  <ROW num=\"0\">");
                pwrite.println("    <CONTENT_ID>"+(String)rowValues.get("CONTENT_ID")+"</CONTENT_ID>");
                pwrite.println("    <CONTENT_CHECKSUM>"+(String)rowValues.get("CONTENT_CHECKSUM")+"</CONTENT_CHECKSUM>");
                pwrite.println("    <PUBLISH_DATE>"+(String)rowValues.get("PUBLISH_DATE")+"</PUBLISH_DATE>");
                if (rowValues.get("ETICKET_OBJECT")!=null && !((String)rowValues.get("ETICKET_OBJECT")).equals(""))
                    pwrite.println("    <ETICKET_OBJECT>"+(String)rowValues.get("ETICKET_OBJECT")+"</ETICKET_OBJECT>");
                pwrite.println("  </ROW>");
            }

            pwrite.close();

        } catch (IOException ie) {
            mException = true;
            mesg = ie.toString();

            WriteLog("IOException Occured (8): " + ie.toString());
            ie.printStackTrace();
        }
    }


   /**************************************************************************
    * transformCEM function creates the CONTENT_ETICKET_METADATA.xml file
    **************************************************************************/
    private void transformCEM(String path, String flag)
    {
        String xmlCEM = path+"CONTENT_ETICKET_METADATA.xml";

        try 
        {
            File xml = new File(xmlCEM);
            if (flag!=null && flag.equals("start") && xml.exists())
                xml.delete();

            FileWriter pfile = new FileWriter(xmlCEM, true);
            PrintWriter pwrite = new PrintWriter(pfile);

            if (flag!=null && flag.equals("start"))
                pwrite.println("<ROWSET>");
            else if (flag!=null && flag.equals("end"))
                pwrite.println("</ROWSET>");
            else {
                if (rowValues.get("ETICKET_METADATA")!=null && !((String)rowValues.get("ETICKET_METADATA")).equals("")) {
                    pwrite.println("  <ROW num=\"0\">");
                    pwrite.println("    <CONTENT_ID>"+(String)rowValues.get("CONTENT_ID")+"</CONTENT_ID>");
                    if (rowValues.get("CHAIN_ID")!=null && !((String)rowValues.get("CHAIN_ID")).equals(""))
                        pwrite.println("    <CHAIN_ID>"+(String)rowValues.get("CHAIN_ID")+"</CHAIN_ID>");
                    pwrite.println("    <ETICKET_METADATA>"+(String)rowValues.get("ETICKET_METADATA")+"</ETICKET_METADATA>");
                    pwrite.println("  </ROW>");
                }
            }

            pwrite.close();

        } catch (IOException ie) {
            mException = true;
            mesg = ie.toString();

            WriteLog("IOException Occured (9): " + ie.toString());
            ie.printStackTrace();
        }
    }


   /**************************************************************************
    * transformCTO function creates the CONTENT_TITLE_OBJECTS.xml file
    **************************************************************************/
    private void transformCTO(String path, String flag)
    {
        String xmlCTO = path+"CONTENT_TITLE_OBJECTS.xml";

        try 
        {
            File xml = new File(xmlCTO);
            if (flag!=null && flag.equals("start") && xml.exists())
                xml.delete();

            FileWriter pfile = new FileWriter(xmlCTO, true);
            PrintWriter pwrite = new PrintWriter(pfile);

            if (flag!=null && flag.equals("start"))
                pwrite.println("<ROWSET>");
            else if (flag!=null && flag.equals("end"))
                pwrite.println("</ROWSET>");
            else {
                pwrite.println("  <ROW num=\"0\">");
                pwrite.println("    <TITLE_ID>"+(String)rowValues.get("TITLE_ID")+"</TITLE_ID>");
                pwrite.println("    <CONTENT_ID>"+(String)rowValues.get("CONTENT_ID")+"</CONTENT_ID>");
                pwrite.println("  </ROW>");
            }

            pwrite.close();

        } catch (IOException ie) {
            mException = true;
            mesg = ie.toString();

            WriteLog("IOException Occured (10): " + ie.toString());
            ie.printStackTrace();
        }
    }

//
// Validate Lot Chips Upload Package
//

   /**************************************************************************
    * ValidateLotChips function basically goes through the XML Document. It calls 
    * itself recursively to traverse through the entire node collection length. 
    **************************************************************************/
    private void ValidateLotChips(NodeList NodeCollection, String xmlFile, Connection conn) 
    {
        String local = null;

        if (NodeCollection != null) 
        {
            for (int i=0; i< NodeCollection.getLength(); i++) 
            {
                local = NodeCollection.item(i).getNodeName();

                if (NodeCollection.item(i).getNodeType() == Node.ELEMENT_NODE) 
                {                       
		    if (!local.toUpperCase().equals("ROW"))
                        rowValues.put(local.toUpperCase(), GetElementData(NodeCollection.item(i)));

                    ValidateLotChips(NodeCollection.item(i).getChildNodes(), xmlFile, conn);

		    if (local.toUpperCase().equals("ROW") && xmlFile.endsWith("lots.xml"))
                       validateLots(conn);

		    else if (local.toUpperCase().equals("ROW") && xmlFile.endsWith("lot_chips.xml"))
                       validateChips();
                }
            }
        }
    }

   /**************************************************************************
    * validateLots function basically validates the lots.xml
    **************************************************************************/
    private void validateLots(Connection conn)
    {
        if (rowValues.get("LOT_NUMBER")==null || ((String)rowValues.get("LOT_NUMBER")).equals("")) 
        {
      	    mesg = "LOT_NUMBER tag missing in lots.xml";
            return;
        }

        if (rowValues.get("LOT_SIZE")==null || ((String)rowValues.get("LOT_SIZE")).equals("")) 
        {
      	    mesg = "LOT_SIZE tag missing in lots.xml";
            return;
        }

        if (rowValues.get("CHIP_REV")==null || ((String)rowValues.get("CHIP_REV")).equals("")) 
        {
      	    mesg = "CHIP_REV tag missing in lots.xml";
            return;
        }

        if (rowValues.get("CREATE_DATE")==null || ((String)rowValues.get("CREATE_DATE")).equals("")) 
        {
      	    mesg = "CREATE_DATE tag missing in lots.xml";
            return;
        }

        if (rowValues.get("LAST_REPORTED")==null || ((String)rowValues.get("LAST_REPORTED")).equals("")) 
        {
      	    mesg = "LAST_REPORTED tag missing in lots.xml";
            return;
        }

        if (rowValues.get("LAST_PROCESSED")==null || ((String)rowValues.get("LAST_PROCESSED")).equals("")) 
        {
      	    mesg = "LAST_PROCESSED tag missing in lots.xml";
            return;
        }

        if (rowValues.get("NUM_PROCESSED")==null || ((String)rowValues.get("NUM_PROCESSED")).equals("")) 
        {
      	    mesg = "NUM_PROCESSED tag missing in lots.xml";
            return;
        }

        if (rowValues.get("MANUFACTURER")==null || ((String)rowValues.get("MANUFACTURER")).equals("")) 
        {
      	    mesg = "MANUFACTURER tag missing in lots.xml";
            return;
        }

        if (rowValues.get("LOCATION")==null || ((String)rowValues.get("LOCATION")).equals("")) 
        {
      	    mesg = "LOCATION tag missing in lots.xml";
            return;
        }

        if (rowValues.get("CERT_ID")==null || ((String)rowValues.get("CERT_ID")).equals("")) 
        {
      	    mesg = "CERT_ID tag missing in lots.xml";
            return;

        } else if (mpcCert == null || mpcCert.equals("")) {
            try 
            {
                mpcCert = mUA.executeGetCert((String)rowValues.get("CERT_ID"), conn);

                if (mpcCert == null || mpcCert.equals("")) 
                {
      	            mesg = "RSA CERT NOT FOUND ERROR for CERT_ID = " + (String)rowValues.get("CERT_ID");
                    return;
                }

            } catch (SQLException se) {
                mException = true;
                mesg = se.toString();

   	        WriteLog("SQLException Occured (6): " + se.toString());
     	        se.printStackTrace();
            } 
        }
    }

   /**************************************************************************
    * validateChips function basically validates the lot_chips.xml
    **************************************************************************/
    private void validateChips()
    {
        if (rowValues.get("BB_ID")==null || ((String)rowValues.get("BB_ID")).equals("")) 
        {
      	    mesg = "BB_ID tag missing in lot_chips.xml";
            return;
        }

        if (rowValues.get("LOT_NUMBER")==null || ((String)rowValues.get("LOT_NUMBER")).equals("")) 
        {
      	    mesg = "LOT_NUMBER tag missing in lot_chips.xml";
            return;
        }

        if (rowValues.get("CERT")==null || ((String)rowValues.get("CERT")).equals("")) 
        {
      	    mesg = "CERT tag missing in lot_chips.xml for BB_ID = " + (String)rowValues.get("BB_ID");
            return;
        } else {
            try 
            {
                RSAcert rsaCert = new RSAcert(mpcCert);
                BBcert bbCert = new BBcert((String)rowValues.get("CERT"));
                  
                if (!bbCert.verify((PublicKey)rsaCert.pubkey)) 
                {
        	    mesg = "CERT validation failed for BB_ID = " + (String)rowValues.get("BB_ID");
                    return;
                }
             
            } catch (CertificateParsingException cpe) {
                mesg = "CertificateParsingException Occured - " + cpe.toString() + " for BB_ID = " + (String)rowValues.get("BB_ID");
                return;

            } catch (IOException ie) {
                mesg = "IOException Occured (11): " + ie.toString() + " for BB_ID = " + (String)rowValues.get("BB_ID");
                return;

            }
        }
    }

//
// Validate Certificate Upload Package
//

   /**************************************************************************
    * ValidateCA function basically goes through the XML Document. It calls 
    * itself recursively to traverse through the entire node collection length. 
    **************************************************************************/
    private void ValidateCA(NodeList NodeCollection, String xmlFile) 
    {
        String local = null;

        if (NodeCollection != null) 
        {
            for (int i=0; i< NodeCollection.getLength(); i++) 
            {
                local = NodeCollection.item(i).getNodeName();

                if (NodeCollection.item(i).getNodeType() == Node.ELEMENT_NODE) 
                {                       
		    if (!local.toUpperCase().equals("ROW"))
                        rowValues.put(local.toUpperCase(), GetElementData(NodeCollection.item(i)));

                    ValidateCA(NodeCollection.item(i).getChildNodes(), xmlFile);

		    if (local.toUpperCase().equals("ROW") && xmlFile.endsWith("certificates.xml"))
                       validateCertificates();

		    else if (local.toUpperCase().equals("ROW") && xmlFile.endsWith("certificate_chains.xml"))
                       validateChains();

		    else if (local.toUpperCase().equals("ROW") && xmlFile.endsWith("current_crls.xml"))
                       validateCRL();
                }
            }
        }
    }

   /**************************************************************************
    * validateCertificates function basically validates the certificates.xml
    **************************************************************************/
    private void validateCertificates()
    {
        if (rowValues.get("CERT_ID")==null || ((String)rowValues.get("CERT_ID")).equals("")) 
        {
      	    mesg = "CERT_ID tag missing in certificates.xml";
            return;
        }

        if (rowValues.get("OU")==null || ((String)rowValues.get("OU")).equals("")) 
        {
      	    mesg = "OU tag missing in certificates.xml";
            return;
        }

        if (rowValues.get("CN")==null || ((String)rowValues.get("CN")).equals("")) 
        {
      	    mesg = "CN tag missing in certificates.xml";
            return;
        }

        if (rowValues.get("SERIAL_NO")==null || ((String)rowValues.get("SERIAL_NO")).equals("")) 
        {
      	    mesg = "SERIAL_NO tag missing in certificates.xml";
            return;
        }

        if (rowValues.get("CERTIFICATE")==null || ((String)rowValues.get("CERTIFICATE")).equals("")) 
        {
      	    mesg = "CERTIFICATE tag missing in certificates.xml";
            return;
        }

        if (rowValues.get("PUBLIC_KEY")==null || ((String)rowValues.get("PUBLIC_KEY")).equals("")) 
        {
      	    mesg = "PUBLIC_KEY tag missing in certificates.xml";
            return;
        }

//        if (rowValues.get("PRIVATE_KEY")==null || ((String)rowValues.get("PRIVATE_KEY")).equals("")) 
//        {
//      	    mesg = "PRIVATE_KEY tag missing in certificates.xml";
//            return;
//        }

    }

   /**************************************************************************
    * validateChains function basically validates the certificate_chains.xml
    **************************************************************************/
    private void validateChains()
    {
        if (rowValues.get("CHAIN_ID")==null || ((String)rowValues.get("CHAIN_ID")).equals("")) 
        {
      	    mesg = "CHAIN_ID tag missing in certificate_chains.xml";
            return;
        }

        if (rowValues.get("SIGNER_CERT_ID")==null || ((String)rowValues.get("SIGNER_CERT_ID")).equals("")) 
        {
      	    mesg = "SIGNER_CERT_ID tag missing in certificate_chains.xml";
            return;
        }

        if (rowValues.get("CA_CERT_ID")==null || ((String)rowValues.get("CA_CERT_ID")).equals("")) 
        {
      	    mesg = "CA_CERT_ID tag missing in certificate_chains.xml";
            return;
        }

        if (rowValues.get("DESCRIPTION")==null || ((String)rowValues.get("DESCRIPTION")).equals("")) 
        {
      	    mesg = "DESCRIPTION tag missing in certificate_chains.xml";
            return;
        }
    }

   /**************************************************************************
    * validateCRL function basically validates the current_crls.xml
    **************************************************************************/
    private void validateCRL()
    {
        if (rowValues.get("CRL_TYPE")==null || ((String)rowValues.get("CRL_TYPE")).equals("")) 
        {
      	    mesg = "CRL_TYPE tag missing in current_crls.xml";
            return;
        }

        if (rowValues.get("CRL_VERSION")==null || ((String)rowValues.get("CRL_VERSION")).equals("")) 
        {
      	    mesg = "CRL_VERSION tag missing in current_crls.xml";
            return;
        }

        if (rowValues.get("CRL_NAME")==null || ((String)rowValues.get("CRL_NAME")).equals("")) 
        {
      	    mesg = "CRL_NAME tag missing in current_crls.xml";
            return;
        }

        if (!((String)rowValues.get("CRL_VERSION")).equals("0")) 
        {
            if (rowValues.get("IS_ETICKET_CRL")==null || ((String)rowValues.get("IS_ETICKET_CRL")).equals("")) 
            {
      	        mesg = "IS_ETICKET_CRL tag missing in current_crls.xml for CRL_VERSION = " + (String)rowValues.get("CRL_VERSION");
                return;
            }

            if (rowValues.get("CRL_OBJECT")==null || ((String)rowValues.get("CRL_OBJECT")).equals("")) 
            {
      	        mesg = "CRL_OBJECT tag missing in current_crls.xml for CRL_VERSION = " + (String)rowValues.get("CRL_VERSION");
                return;
            }
        }
    }

//
// Validate RMA Upload Package
//

   /**************************************************************************
    * ValidateRMA function basically goes through the XML Document. It calls 
    * itself recursively to traverse through the entire node collection length. 
    **************************************************************************/
    private void ValidateRMA(NodeList NodeCollection, String xmlFile, Connection conn) 
    {
        String local = null;

        if (NodeCollection != null) 
        {
            for (int i=0; i< NodeCollection.getLength(); i++) 
            {
                local = NodeCollection.item(i).getNodeName();

                if (NodeCollection.item(i).getNodeType() == Node.ELEMENT_NODE) 
                {                       
		    if (!local.toUpperCase().equals("ROW"))
                        rowValues.put(local.toUpperCase(), GetElementData(NodeCollection.item(i)));

                    ValidateRMA(NodeCollection.item(i).getChildNodes(), xmlFile, conn);

                    if (local.toUpperCase().equals("ROW") && xmlFile.endsWith("BB_PLAYER_SERIALS.xml")) 
                    {
                    } else if (local.toUpperCase().equals("ROW"))
                       validateSN(xmlFile.substring(0, xmlFile.lastIndexOf("/")+1), conn);
                }
            }
        }
    }

   /**************************************************************************
    * validateSN function basically validates and transforms RMA data
    **************************************************************************/
    private void validateSN(String path, Connection conn)
    {
        if (rowValues.get("PCB_SN")==null || ((String)rowValues.get("PCB_SN")).equals("")) 
        {
      	    mesg = "PCB_SN tag missing in xml file";
            return;
        }

        if (rowValues.get("EXT_SN")==null || ((String)rowValues.get("EXT_SN")).equals("")) 
        {
      	    mesg = "EXT_SN tag missing in xml file";
            return;
        }

        if (rowValues.get("INT_SN")==null || ((String)rowValues.get("INT_SN")).equals("")) 
        {
      	    mesg = "INT_SN tag missing in xml file";
            return;

        } else {
            try 
            {
                mUA.executeGetBBID((String)rowValues.get("INT_SN"), conn);
                bbid = mUA.getBBID();
                buid = mUA.getBUID();

                int count = mUA.countBBID((String)rowValues.get("INT_SN"), conn);

                if (bbid == null || bbid.equals("")) 
                {
      	            Log2File((String)rowValues.get("PCB_SN") + ", " + (String)rowValues.get("INT_SN") + ", " +
                             (String)rowValues.get("EXT_SN") + " - BB_ID NOT FOUND ERROR");
      	            WriteLog("BB_ID NOT FOUND for " + (String)rowValues.get("INT_SN") + " (80 Code)");

                } else if (count != 1) {
      	            Log2File((String)rowValues.get("PCB_SN") + ", " + (String)rowValues.get("INT_SN") + ", " +
                             (String)rowValues.get("EXT_SN") + " - MULTIPLE BB_ID FOUND ERROR (" + 
                             String.valueOf(count) + ")");
      	            WriteLog("MULTIPLE BB_ID FOUND for " + (String)rowValues.get("INT_SN") + " (80 Code)");

                } else {
      	            Log2File((String)rowValues.get("PCB_SN") + ", " + (String)rowValues.get("INT_SN") + ", " +
                             (String)rowValues.get("EXT_SN") + " - OK");
                    transformBBPlayerSerials(path, null);
                }

            } catch (SQLException se) {
                mException = true;
                mesg = se.toString();

   	        WriteLog("SQLException Occured (7): " + se.toString());
     	        se.printStackTrace();

            } catch (IOException ie) {
                mException = true;
                mesg = ie.toString();

                WriteLog("IOException Occured (12): " + ie.toString());
                ie.printStackTrace();

            }            
        }
    }

   /**************************************************************************
    * transformBBPlayerSerials function creates the BB_PLAYER_SERIALS.xml file
    **************************************************************************/
    private void transformBBPlayerSerials(String path, String flag)
    {
        String xmlBBPS = path+"BB_PLAYER_SERIALS.xml";

        try 
        {
            File xml = new File(xmlBBPS);
            if (flag!=null && flag.equals("start") && xml.exists())
                xml.delete();

            FileWriter pfile = new FileWriter(xmlBBPS, true);
            PrintWriter pwrite = new PrintWriter(pfile);

            if (flag!=null && flag.equals("start"))
                pwrite.println("<ROWSET>");
            else if (flag!=null && flag.equals("end")) 
                pwrite.println("</ROWSET>");
            else {
                pwrite.println("  <ROW num=\"0\">");
                pwrite.println("    <BB_ID>"+bbid+"</BB_ID>");
                pwrite.println("    <PCB_SN>"+(String)rowValues.get("PCB_SN")+"</PCB_SN>");
                pwrite.println("    <INT_SN>"+(String)rowValues.get("INT_SN")+"</INT_SN>");
                pwrite.println("    <EXT_SN>"+(String)rowValues.get("EXT_SN")+"</EXT_SN>");
                pwrite.println("    <BU_ID>"+buid+"</BU_ID>");
                pwrite.println("    <CHKNUM>"+getCheckNum((String)rowValues.get("PCB_SN"))+"</CHKNUM>");
                pwrite.println("  </ROW>");
            }
            pwrite.close();

        } catch (IOException ie) {
            mException = true;
            mesg = ie.toString();

            WriteLog("IOException Occured (13): " + ie.toString());
            ie.printStackTrace();
        }
    }

//
// Utility Functions
//

   /**************************************************************************
    * GetElementData function returns the value of the child node
    **************************************************************************/
    private String GetElementData(Node parentNode) 
    {
        if (parentNode.getFirstChild() != null) 
        {
            int childType = parentNode.getFirstChild().getNodeType();
 
            if (childType == Node.TEXT_NODE) 
            {
                if (parentNode.getFirstChild().getNodeValue() != null)
                    return parentNode.getFirstChild().getNodeValue();           
            }
        }

        return "";
    }

   /**************************************************************************
    * getCheckNum function deduces the value for CHKNUM column in 
    * BB_PLAYER_SERAILS table from PCB_SN
    **************************************************************************/
    private String getCheckNum(String pcb)
    {
        String yymm = pcb.substring(4,6);
        String num = pcb.substring(7);

        return "20"+hex2num(yymm.charAt(0))+hex2num(yymm.charAt(1))+num;
    }

    private String hex2num(char chr)
    {
        String ret = null;

        switch (chr) 
        {
            case '1':  ret = "01"; break;
            case '2':  ret = "02"; break;
            case '3':  ret = "03"; break;
            case '4':  ret = "04"; break;
            case '5':  ret = "05"; break;
            case '6':  ret = "06"; break;
            case '7':  ret = "07"; break;
            case '8':  ret = "08"; break;
            case '9':  ret = "09"; break;
            case 'A':  ret = "10"; break;
            case 'B':  ret = "11"; break;
            case 'C':  ret = "12"; break;
            case 'D':  ret = "13"; break;
            case 'E':  ret = "14"; break;
            case 'F':  ret = "15"; break;
        }

        return ret;
    }

   /**************************************************************************
    * getStatusUpdateXML function formulates the Data XML string for the
    * given request ID in order to update the status of the corresponding row 
    * in the DATA_LOAD_REQUESTS table in the database.
    **************************************************************************/
    private String getStatusUpdateXML()
    {
	String valDate = null;
        Calendar calendar = new GregorianCalendar();
        java.util.Date date = calendar.getTime();
        DateFormat utc = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
        utc.setTimeZone(TimeZone.getTimeZone("GMT+00:00"));
        valDate = utc.format(date);

	String dataXML = null;
	
        dataXML = "<ROWSET><ROW num=\"1\">";
        dataXML += "<REQUEST_ID>"+reqId+"</REQUEST_ID>";

        if (mesg!=null && !mesg.equals("")) 
        {
            dataXML += "<PROCESS_STATUS>Failed</PROCESS_STATUS>";

            if (pkg!=null && pkg.equals("RMA")) 
                dataXML += "<PROCESS_XML>"+logfile+"</PROCESS_XML>";
            else
                dataXML += "<PROCESS_XML>"+mesg+"</PROCESS_XML>";

        } else {
            dataXML += "<PROCESS_STATUS>Validated</PROCESS_STATUS>";

            if (pkg!=null && pkg.equals("RMA")) 
                dataXML += "<PROCESS_XML>"+logfile+"</PROCESS_XML>";
            else
                dataXML += "<PROCESS_XML></PROCESS_XML>";
        }

        dataXML += "<VALIDATE_DATE>"+valDate+"</VALIDATE_DATE>";
        dataXML += "</ROW></ROWSET>";

        return dataXML;
    }

//
// Log Functions
//

  /**************************************************************************
   * WriteLog function is called to log messages into the log file
   **************************************************************************/
   private void WriteLog(String mesg) 
   {
       java.util.Date d = new java.util.Date();
       SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss z");
       System.out.println(df.format(d) + ": " + mesg);
   }

  /**************************************************************************
   * Log2File function writes the log message to the specified file
   **************************************************************************/
   private void Log2File(String mesg) 
        throws IOException 
   {
       FileWriter pfile = new FileWriter(logfile, true);
       PrintWriter pwrite = new PrintWriter(pfile);

       pwrite.println(mesg);
       pwrite.close();
   }
}

/*
 * UploadServlet.java
 *
 */

package com.broadon.bccs;

import java.io.*;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

import org.apache.commons.fileupload.*;

/**
 * a servlet to handle upload request.<br>
 * refer to  http://www.rfc.net/rfc1867.html
 *
 */

public class UploadServlet 
{
    private final static String UPLOADED = "Uploaded";
    private final static String TMP_DIR = "/tmp";
    private final static int BUFFER_SIZE = 32768;   // 32K
//    private final static long MAX_SIZE = 104857600; // 100M
    private final static long MAX_SIZE = -1; // No Maximum

    public HashMap processUpload (HttpServletRequest req, HttpServletResponse res, 
				  String uploadPath)
	throws ServletException, IOException 
    {
	HashMap map = new HashMap();
        String client = null;

        try 
        {
            DiskFileUpload upload = new DiskFileUpload();
            upload.setSizeMax(MAX_SIZE);
            upload.setSizeThreshold(BUFFER_SIZE);
            upload.setRepositoryPath(TMP_DIR);

            List items = upload.parseRequest(req);
            Iterator iter = items.iterator();

            while(iter.hasNext()) 
            {
                FileItem item = (FileItem) iter.next();

                if (item.isFormField())
                    appendValue(map, item.getFieldName(), item.getString());
                else {
                    String fileName = item.getName();

                    if (fileName!=null && !fileName.equals(""))
                    { 
                        client = getValue(map, "client");

                        if (fileName.indexOf("/") >= 0)
                            fileName = fileName.substring(fileName.lastIndexOf("/")+1);
                        else if (fileName.indexOf("\\") >= 0)
                            fileName = fileName.substring(fileName.lastIndexOf("\\")+1);
                    
                        File uploadedFile = new File(uploadPath+client, fileName);
                        item.write(uploadedFile);

                        appendValue(map, item.getFieldName(), fileName, item.getContentType(), 
	    			    item.getSize(), UPLOADED);
                    }
                }
            }

        } catch (FileUploadException ue) {
            throw new IOException ("FileUploadException occured: " + ue.toString());

        } catch (Exception e) {
            throw new IOException ("Exception occured: " + e.toString());
        }

 	return map;
    }

    void appendValue(HashMap map, String name, String value, String contentType, long size, String status) 
    {
    	UploadData data = new UploadData(name, value, contentType, size, true, status);
	map.put(name, data);
    }

    void appendValue(HashMap map, String name, String value) 
    {
    	UploadData data = new UploadData(name, value, null, 0, false, null);
    	map.put(name, data);
    }

    String getValue(HashMap map, String name) 
    {
    	UploadData data=(UploadData) map.get(name);

    	if (data == null) 
  	    return null;

	return data.value;
    }
}

class UploadData 
{
    String name;
    String value;
    String contentType;
    long size;
    boolean isFile;
    String status;

    UploadData (String name, String value, String contentType, long size, boolean isFile, String status) 
    {
	this.name = name;
	this.value = value;
	this.contentType = contentType;
	this.size = size;
	this.isFile = isFile;
	this.status = status;
    }
}

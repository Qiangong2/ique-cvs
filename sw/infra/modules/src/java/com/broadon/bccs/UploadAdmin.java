package com.broadon.bccs;

import java.io.*;
import java.sql.*;
import java.net.*;
import java.util.Properties;
import java.util.HashMap;

import javax.mail.*;
import javax.mail.internet.*;
import javax.servlet.*;
import javax.servlet.http.*;

import java.text.SimpleDateFormat;
import java.text.DateFormat;

import javax.sql.DataSource;

import java.math.BigInteger;

import java.security.*;
import java.security.cert.*;
import java.security.spec.*;
import java.security.interfaces.RSAPublicKey;

import org.xml.sax.SAXException;
import com.broadon.ms.common.QueryBean;

import oracle.sql.BLOB;

public class UploadAdmin
{
    // Request parameters
    //
    private String reqID[];
    private String jobName[];
    private String certPublicKey;
    private Date certRevokeDate;
    private String bbID;
    private String buID;
 
    private static final String DATA_LOAD_REQUESTS = "DATA_LOAD_REQUESTS";

    private static final String CFG_ADMIN_EMAIL = "UPLOAD_ADMIN_EMAIL";
    private static final String CFG_SMTP_SERVER = "UPLOAD_SMTP_SERVER";
    private static final String CFG_SMTP_USERNAME = "UPLOAD_SMTP_USERNAME";
    private static final String CFG_SMTP_PASSWORD = "UPLOAD_SMTP_PASSWORD";

    private static final String DEFAULT_ADMIN_EMAIL = "upload-admin@broadon.com";
    private static final String DEFAULT_SMTP_SERVER = "localhost";
    private static final String DEFAULT_SMTP_USERNAME = "upload-admin";
    private static final String DEFAULT_SMTP_PASSWORD = "broadon";

    private static final String selectForTest = 
    	"SELECT sysdate FROM dual";

    private static final String selectCertificate =
    	"SELECT public_key, revoke_date " +
    	"FROM certificates " +
    	"WHERE cert_id = ?";

//
// Validate Upload
//
    private static final String COUNT_JOBS_TO_VALIDATE =
        "SELECT count(*) " +
        "FROM data_load_requests " +
        "WHERE process_status = 'Uploaded'";

    private static final String SELECT_JOBS_TO_VALIDATE =
        "SELECT request_id, job_name " +
        "FROM data_load_requests " +
        "WHERE process_status = 'Uploaded' ORDER BY request_date asc";

    private static final String VERIFY_JOB_TO_VALIDATE =
        "SELECT request_id, job_name " +
        "FROM data_load_requests " +
        "WHERE process_status = 'Uploaded' AND job_name = ";

    private static final String COUNT_JOB_TO_VALIDATE =
        "SELECT count(*) " +
        "FROM data_load_requests " +
        "WHERE process_status = 'Uploaded' AND job_name = ";

    private static final String VALIDATE_TITLE_CALL =
        "{? = call CTOPS.validate_title(?)}";

    private static final String VALIDATE_CONTENT_CALL =
        "{? = call CTOPS.validate_content(?,?,?,?,?)}";

    private static final String GET_CERT =
        "SELECT certificate FROM CERTIFICATES WHERE cert_id=";

    private static final String COUNT_BB_MODEL =
        "SELECT count(*) FROM BB_MODELS WHERE bb_model = ";

    private static final String COUNT_CONTENT_OBJECT_TYPE =
        "SELECT count(*) FROM CONTENT_OBJECT_TYPES WHERE content_object_type = ";

    private static final String COUNT_CONTENT_ID =
        "SELECT count(*) FROM CONTENT_OBJECTS WHERE content_id = ";

    private static final String GET_BBID =
        "SELECT bb_id, bu_id FROM BB_PLAYERS WHERE sn = ";

    private static final String COUNT_BBID =
        "SELECT count(*) FROM BB_PLAYERS WHERE sn = ";

//
// Process Upload
//
    private static final String COUNT_JOBS_TO_PROCESS =
        "SELECT count(*) " +
        "FROM data_load_requests " +
        "WHERE process_status = 'Validated'";
    
    private static final String SELECT_JOBS_TO_PROCESS =
    	"SELECT request_id, job_name " +
    	"FROM data_load_requests " +
    	"WHERE process_status = 'Validated' ORDER BY request_date asc";
    
    private static final String VERIFY_JOB_TO_PROCESS =
        "SELECT request_id, job_name " +
        "FROM data_load_requests " +
        "WHERE process_status = 'Validated' AND job_name = ";

    private static final String COUNT_JOB_TO_PROCESS =
        "SELECT count(*) " +
        "FROM data_load_requests " +
        "WHERE process_status = 'Validated' AND job_name = ";

    private static final String CLEAN_INCOMING =
	"DELETE FROM incoming";

    private static final String POST_XML_CALL =
	"{call DPUTIL.post_xml(?,?,?)}";

    private static final String PROCESS_XML_CALL =
	"{call DPUTIL.process_incoming}";
	
    private static final String CHECK_XML_CALL =
	"{? = call BBXML.is_xmlrec(?,?,?)}";

    private static final String QUERY_DTD_CALL =
	"{? = call BBXML.GetQueryDTD(?)}";

    private static final String UPDATE_BLOB =
	"UPDATE CONTENT_CACHE SET content_object=? WHERE content_checksum=?";

    private static final String QUERY_BLOB =
	"SELECT content_object FROM CONTENT_CACHE WHERE content_checksum=?";
   
// 
// Constructor
//
    public UploadAdmin()
    {
        reqID = null;
        jobName = null;

        certPublicKey = null;
        certRevokeDate = null;
    }

//
// Common Functions
//
    public String generateRequestID()
    {
        java.util.Date d = new java.util.Date();
        long mSec = d.getTime();

        return String.valueOf(mSec);
    } // generateRequestID


    public void postMail (String[] recipients, String subject, String message, Properties bccsProp)
        throws MessagingException
    {
        boolean debug = false;
        String fromEmail = bccsProp.getProperty(CFG_ADMIN_EMAIL);
        String smtpServer = bccsProp.getProperty(CFG_SMTP_SERVER);
        String smtpUsername = bccsProp.getProperty(CFG_SMTP_USERNAME);
        String smtpPassword = bccsProp.getProperty(CFG_SMTP_PASSWORD);

        if (fromEmail==null || fromEmail.equals(""))
            fromEmail = DEFAULT_ADMIN_EMAIL;
        if (smtpServer==null || smtpServer.equals(""))
            smtpServer = DEFAULT_SMTP_SERVER;
        if (smtpUsername==null || smtpUsername.equals(""))
            smtpUsername = DEFAULT_SMTP_USERNAME;
        if (smtpPassword==null || smtpPassword.equals(""))
            smtpPassword = DEFAULT_SMTP_PASSWORD;

        //Set the host smtp address
        Properties props = new Properties();
        props.put("mail.smtp.host", smtpServer);
        props.put("mail.smtp.username", smtpUsername);
        props.put("mail.smtp.password", smtpPassword);

        // create some properties and get the default Session
        Session session = Session.getDefaultInstance(props, null);
        session.setDebug(debug);

        // create a message
        Message msg = new MimeMessage(session);

        // set the from and to address
        InternetAddress addressFrom = new InternetAddress(fromEmail);
        msg.setFrom(addressFrom);

        InternetAddress[] addressTo = null;

        if (recipients!=null && recipients.length > 0)
        {
            addressTo = new InternetAddress[recipients.length+1];
            int i = 0; 
            while (i < recipients.length)
            {
                addressTo[i] = new InternetAddress(recipients[i]);
                i++;
            }
            addressTo[i] = addressFrom;
        } else {
            addressTo = new InternetAddress[1];
            addressTo[0] = addressFrom;
        }
        msg.setRecipients(Message.RecipientType.TO, addressTo);

        // Setting the Subject and Content Type
        msg.setSubject(subject);
        msg.setContent(message, "text/plain");

        Transport transport = session.getTransport("smtp");
        transport.connect(props.getProperty("mail.smtp.host"),
                          props.getProperty("mail.smtp.username"),
                          props.getProperty("mail.smtp.password"));

        transport.send(msg);
        transport.close();
    } // postMail

    public boolean setStatus(HashMap map, String name, String status) 
    {
    	UploadData data=(UploadData) map.get(name);
    	if (data != null) {
  	    data.status = status;
    	    return true;
	}

    	return false;
    } // setStatus

    public boolean setCounter(String fileName, String counter)
    {
        // declared here so its visible in finally
        BufferedReader input = null;
        boolean result = false;

        try 
        {
            StringBuffer contents = new StringBuffer();
            File prop = new File(fileName);
            File temp = new File(fileName.substring(0, fileName.lastIndexOf(".")) + "tmp");
            temp.createNewFile();

            try 
            {
                input = new BufferedReader(new FileReader(prop));
                String line = null;

                while ((line = input.readLine()) != null)
                {
                    if (line.startsWith("Counter="))
                        contents.append("Counter="+counter);
                    else
                        contents.append(line);
                    contents.append("\n");
                }

            } catch (FileNotFoundException ex) {
                ex.printStackTrace();
            } catch (IOException ex) {
                ex.printStackTrace();
            } finally {
                try
                {
                    // flush and close BufferedReader and its underlying FileReader
                    if (input != null)
                        input.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
           }

           PrintWriter out = null;
           try 
           {
                out = new PrintWriter(new FileWriter(temp));
                out.print(contents);
            } catch(IOException e) {
                e.printStackTrace();
            } finally {
                if (out != null)
                    out.close();
            }

            if (temp.renameTo(prop))
                result = true;

        } catch (SecurityException se) {
            se.printStackTrace();
        } catch (NullPointerException ne) {
            ne.printStackTrace();
        } catch (IllegalArgumentException iae) {
            iae.printStackTrace();
        } catch (IOException ie) {
            ie.printStackTrace();
        } 

        return result;
    } // setCounter

//
// Database functions for content upload servlet
//
    public void getCertificate(String certID, Connection conn)
        throws SQLException
    {
        PreparedStatement ps = conn.prepareStatement(selectCertificate);
        ps.setString(1, certID);

        try {
            ResultSet rs = ps.executeQuery();
            if(rs.next()) {
                certPublicKey = rs.getString(1);
                certRevokeDate = rs.getDate(2);
            }
            rs.close();
        } finally {
            ps.close();
        }
    } // getCertificate
        
//
// Database functions for validate upload
//
   /**************************************************************************
    * executeGetJobsToValidate function is called to get a list of jobs from
    * the database in "Uploaded" status.
    **************************************************************************/
    public void executeGetJobsToValidate(Connection conn)
        throws SQLException, IOException
    {
        int count = count(COUNT_JOBS_TO_VALIDATE, conn);
        reqID = null;
        jobName = null;

        if (count > 0)
        {
            reqID = new String[count];
            jobName = new String[count];

            PreparedStatement ps = conn.prepareStatement(SELECT_JOBS_TO_VALIDATE);
            ResultSet rs = ps.executeQuery();

            for (int i = 0; rs.next(); i++)
            {
                reqID[i] = String.valueOf(rs.getLong(1));
                jobName[i] = rs.getString(2);
            }

            closeStatement(ps);
        }

        return;

    } // getJobsToValidate

   /**************************************************************************
    * executeVerifyJobToValidate function is called to verify if the given job 
    * is in the "uploaded" status
    **************************************************************************/
    public void executeVerifyJobToValidate(String job, Connection conn)
        throws SQLException, IOException
    {
        int count = count(COUNT_JOB_TO_VALIDATE + "'" + job + "'", conn);
        reqID = null;
        jobName = null;

        if (count > 0)
        {
            reqID = new String[count];
            jobName = new String[count];

            PreparedStatement ps = conn.prepareStatement(VERIFY_JOB_TO_VALIDATE + "'" + job + "'");
            ResultSet rs = ps.executeQuery();

            for (int i = 0; rs.next(); i++)
            {
                reqID[i] = String.valueOf(rs.getLong(1));
                jobName[i] = rs.getString(2);
            }

            closeStatement(ps);
        }

        return;
    } // executeVerifyJobToValidate

   /**************************************************************************
    * executeGetBBID function is called to get the bb_id, bu_id for a sn
    **************************************************************************/
    public void executeGetBBID(String sn, Connection conn)
        throws SQLException
    {
        String sql = GET_BBID + "'" + sn + "'";
        PreparedStatement statement = conn.prepareStatement(sql);
        ResultSet   resultSet = statement.executeQuery();

        if (resultSet == null || !resultSet.next())
        {
            bbID = null;
            buID = null;

        } else {
            bbID = resultSet.getString(1);
            buID = resultSet.getString(2);
        }

        closeStatement(statement);
        return;
    }

   /**************************************************************************
    * executeGetCert function is called to get the certificate for a given cert_id
    **************************************************************************/
    public String executeGetCert(String certID, Connection conn)
        throws SQLException
    {
        String sql = GET_CERT + "'" + certID + "'";
        PreparedStatement statement = conn.prepareStatement(sql);
        ResultSet   resultSet = statement.executeQuery();
        String mpcCert = null;

        if (resultSet == null || !resultSet.next())
            mpcCert = null;
        else
            mpcCert = resultSet.getString(1);

        closeStatement(statement);
        return mpcCert;
    }

   /**************************************************************************
    * executeValidateTitle function is called to verify if coTitleID exists in
    * the DB.
    *
    * 1 = title exists
    * 0 = title does not exists
    **************************************************************************/
    public int executeValidateTitle(String titleID, Connection conn)
        throws SQLException
    {
        CallableStatement call = null;
        int ret = -1;

        call = prepareCall(VALIDATE_TITLE_CALL, conn);

        call.registerOutParameter(1, Types.NUMERIC);
        call.setInt(2, Integer.parseInt(titleID));
        call.executeQuery();

        ret = call.getInt(1);

        closeStatement(call);
        return ret;
    }

   /**************************************************************************
    * executeValidateContent function is called to verify based on the coUpdate
    * flag if the xml is acceptable.
    *
    * -1 = title exists error
    * -2 = content exists error
    * -101 = title not exists error
    * -102 = content version lower error
    * -103 = content mismatch error
    * +ve = valid content id
    **************************************************************************/
    public int executeValidateContent(String titleID, String contentID, String conObjName, 
                                      String conObjVer, String update, Connection conn)
        throws SQLException
    {
        CallableStatement call = null;
        int ret = -1;

        call = prepareCall(VALIDATE_CONTENT_CALL, conn);

        call.registerOutParameter(1, Types.NUMERIC);
        call.setInt(2, Integer.parseInt(titleID));
        call.setInt(3, Integer.parseInt(contentID));
        call.setString(4, conObjName);
        call.setInt(5, Integer.parseInt(conObjVer));
        call.setString(6, update);
        call.executeQuery();

        ret = call.getInt(1);

        closeStatement(call);
        return ret;
    }

   /**************************************************************************
    * countBBModel function is called to verify if the given bb model exists
    **************************************************************************/
    public int countBBModel(String model, Connection conn)
        throws SQLException, IOException
    {
        return count(COUNT_BB_MODEL+"'"+model+"'", conn);
    }

   /**************************************************************************
    * countContentObjectType function is called to verify if the given content
    * object type exists
    **************************************************************************/
    public int countContentObjectType(String cot, Connection conn)
        throws SQLException, IOException
    {
        return count(COUNT_CONTENT_OBJECT_TYPE+"'"+cot+"'", conn);
    }

   /**************************************************************************
    * countBBID function is called to verify if the given bbid exists
    **************************************************************************/
    public int countBBID(String sn, Connection conn)
        throws SQLException, IOException
    {
        return count(COUNT_BBID+"'"+sn+"'", conn);
    }

   /**************************************************************************
    * countContentID function is called to verify if the given content id exists
    **************************************************************************/
    public int countContentID(String contentID, Connection conn)
        throws SQLException, IOException
    {
        return count(COUNT_CONTENT_ID+contentID, conn);
    }

//
// Database functions for process upload
//
   /**************************************************************************
    * executeGetJobsToProcess function is called to get a list of jobs from
    * the database in "Validated" status.
    **************************************************************************/
    public void executeGetJobsToProcess(Connection conn)
        throws SQLException, IOException
    {
        int count = count(COUNT_JOBS_TO_PROCESS, conn);

        if (count > 0)
        {
            reqID = new String[count];
            jobName = new String[count];

            PreparedStatement ps = conn.prepareStatement(SELECT_JOBS_TO_PROCESS);
            ResultSet rs = ps.executeQuery();

            for (int i = 0; rs.next(); i++)
            {
                reqID[i] = String.valueOf(rs.getLong(1));
                jobName[i] = rs.getString(2);
            }

            closeStatement(ps);
        }

        return;
    } // executeGetJobsToProcess
 
   /**************************************************************************
    * executeVerifyJobToProcess function is called to verify if the given job
    * is in the "validated" status
    **************************************************************************/
    public void executeVerifyJobToProcess(String job, Connection conn)
        throws SQLException, IOException
    {
        int count = count(COUNT_JOB_TO_PROCESS + "'" + job + "'", conn);
        reqID = null;
        jobName = null;

        if (count > 0)
        {
            reqID = new String[count];
            jobName = new String[count];

            PreparedStatement ps = conn.prepareStatement(VERIFY_JOB_TO_PROCESS + "'" + job + "'");
            ResultSet rs = ps.executeQuery();

            for (int i = 0; rs.next(); i++)
            {
                reqID[i] = String.valueOf(rs.getLong(1));
                jobName[i] = rs.getString(2);
            }

            closeStatement(ps);
        }

        return;
    }
   
   /**************************************************************************
    * executeGetColumns function is called to obtain all the column names for
    * the given tableName so the xml string can only include those tags.
    **************************************************************************/
    public String executeGetColumns(String tableName, Connection conn)
        throws SQLException, IOException
    {
        CallableStatement call = null;
        call = prepareCall(QUERY_DTD_CALL, conn);

        call.registerOutParameter(1, Types.CLOB);
        call.setString(2, tableName);
        call.executeQuery();

        Clob        clob = call.getClob(1);
        Reader      reader = clob.getCharacterStream();
        int         size = (int)clob.length();
        char[]      buffer = new char[size];

        reader.read(buffer);
        return new String(buffer);
    }

   /**************************************************************************
    * executeProcess function is called after the data has been posted to
    * the incoming table (at the most BATCH_SIZE rows). This function calls
    * process_incoming procedure to process the data in the incoming table and
    * then deletes all records from the table.
    **************************************************************************/
    public void executeProcess(Connection conn)
        throws SQLException, IOException
    {
        CallableStatement call = null;

        call = prepareCall(PROCESS_XML_CALL, conn);
        call.executeUpdate();
        closeStatement(call);

        cleanIncoming(conn);

        return;
    }

   /**************************************************************************
    * executePost function is called after all processing has been completed.
    * All that remains now is to post the xml string, tableName and mode
    * into the incoming table in the database.
    **************************************************************************/
    public void executePost(String xml, String tableName, String mode, Connection conn)
        throws SQLException
    {
        CallableStatement call = null;
        call = prepareCall(POST_XML_CALL, conn);

        call.setString(1, xml);
        call.setString(2, tableName);
        call.setString(3, mode);
        call.executeUpdate();
        closeStatement(call);
    }

   /**************************************************************************
    * executeCheck function is called to verify if the given xml string exist
    * in the tableName. If it exists then colName is used to verify if the
    * input data is newer than the existing data or not.
    **************************************************************************/
    public int executeCheck(String xml, String tableName, String colName, Connection conn)
        throws SQLException
    {
        CallableStatement call = null;
        int ret = -1;

        call = prepareCall(CHECK_XML_CALL, conn);

        call.registerOutParameter(1, Types.NUMERIC);
        call.setString(2, xml);
        call.setString(3, tableName);
        call.setString(4, colName);
        call.executeUpdate();

        ret = call.getInt(1);
        closeStatement(call);
        return ret;
    }

   /**************************************************************************
    * checkForBlob function queries for an existing BLOB object for the given
    * content id. If it does not exist then we can update the record.
    **************************************************************************/
    public boolean checkForBlob (String cksum, Connection conn)
        throws SQLException
    {
        Blob contentBlob = null;
        PreparedStatement ps = conn.prepareStatement(QUERY_BLOB);
        ps.setString(1, cksum);
        ResultSet rs = ps.executeQuery();

        if (rs.next())
            contentBlob = rs.getBlob(1);

        rs.close();
        ps.close();

        if (contentBlob != null)
            return true;
        else
            return false;
    }

   /**************************************************************************
    * createContentBlob function creates a BLOB object in the datbase for the
    * given content id by reading in the contents of the coFile
    **************************************************************************/
    public static Blob createContentBlob(String coFile, Connection conn)
        throws SQLException, IOException
    {
        BLOB blob = BLOB.createTemporary(conn, false, BLOB.DURATION_SESSION);
        OutputStream out = blob.setBinaryStream(0);

//        OutputStream out = blob.getBinaryOutputStream();

        // Open the coFile as a stream for insertion into the Blob column
        File contentObject = new File(coFile);
        InputStream in = new FileInputStream(contentObject);

        // Buffer to hold chunks of data to being written to the Blob.
        byte[] buffer = new byte[16*1024];

        // Read a chunk of data from the input stream, and write the chunk to the
        // Blob column output stream. Repeat till file has been fully read.
        int n; // Number of bytes read
        while ((n = in.read(buffer)) > 0)
            out.write(buffer, 0, n);

        // Close both streams
        in.close();
        out.close();

        return blob;
    }

   /**************************************************************************
    * loadObject function inserts the object BLOB (coFile) into the database for
    * the corresponding content_checksum into the CONTENT_CACHE table.
    **************************************************************************/
    public boolean loadObject (String cksum, String coFile, Connection conn)
        throws SQLException, IOException
    {
        boolean result = false;
        if (!(checkForBlob(cksum, conn))) {
            PreparedStatement ps;

            Blob blob = createContentBlob(coFile, conn);
            ps = conn.prepareStatement(UPDATE_BLOB);
            ps.setBlob(1, blob);
            ps.setString(2, cksum);
            ps.executeUpdate();
            ps.close();

            result = true;
        }

        return result;
    }

   /**************************************************************************
    * cleanIncoming function deletes all records from the temporary incoming table
    **************************************************************************/
    public void cleanIncoming (Connection conn)
        throws SQLException, IOException
    {
        PreparedStatement ps = conn.prepareStatement(CLEAN_INCOMING);
        ps.executeUpdate();
        ps.close();

        return;
    }

//
// Database functions for common use
//
    public boolean checkDB (Connection conn)
        throws SQLException
    {
        boolean result = false;

        PreparedStatement ps = conn.prepareStatement(selectForTest);
        try {
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                result = true;
            }
            rs.close();
        } finally {
            ps.close();
        }

        return result;
    } // checkDB

   /**************************************************************************
    * executeStatusUpdate function is called to update the data_load_request
    * table the given xml string.
    **************************************************************************/
    public boolean executeStatusUpdate(String xmlData, Connection conn)
        throws SQLException, IOException, SAXException
    {
        QueryBean qb = new QueryBean();

        int result = qb.insertupdate(xmlData, DATA_LOAD_REQUESTS, conn);

        if (result == 1)
            return true;
        else
            return false; 
    }

   /**************************************************************************
    * count function returns the result back from executing the count(*) sql
    **************************************************************************/
    public int count(String sql, Connection conn)
        throws SQLException, IOException
    {
        QueryBean qb = new QueryBean();

        return qb.count(sql, conn);
    }

   /**************************************************************************
    * prepareCall function is called to prepare callable statement with the
    * given sql.
    **************************************************************************/
    public CallableStatement prepareCall(String sql, Connection connection)
        throws SQLException
    {
        return connection.prepareCall(sql);
    }

   /**************************************************************************
    * closeStatement function is called to close the given statement.
    **************************************************************************/
    public void closeStatement(Statement statement)
        throws SQLException
    {
        if (statement != null)
            statement.close();
    }

//
// Access to private members
//
    public String[] getRequestID() {return reqID;}
    public String[] getJobName() {return jobName;}

    public String getCertPublicKey() {return certPublicKey;}
    public Date getCertRevokeDate() {return certRevokeDate;}

    public String getBBID() {return bbID;}
    public String getBUID() {return buID;}
}


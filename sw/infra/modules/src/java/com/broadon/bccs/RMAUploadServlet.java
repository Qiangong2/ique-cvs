/*
 * RMAUploadServlet.java
 *
 */

package com.broadon.bccs;

import java.io.*;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

import com.broadon.ms.common.AdminServlet;
import com.broadon.db.DBAccessFactory;
import com.broadon.db.DBException;
import com.broadon.db.DataLoadRequestFactory;
import com.broadon.online.UploadBean;

public class RMAUploadServlet extends AdminServlet {
    private static final String CURRENT_LOCALE = "CURRENT_LOCALE";
    private static final String LABEL_PROP_KEY = "RMA_UPLOAD_PROP";
    private static final String JSP_LOC_KEY = "RMA_UPLOAD_JSP_LOC";

    private final static String RMA = "CLIENT_RMA";
    private final static String SUCCESS = "STATUS_SUCCESS";
    private final static String FORMAT_FAILED = "STATUS_FORMAT_FAILED";
    private final static String CONTENT_FAILED = "STATUS_CONTENT_FAILED";
    private final static String PROCESSED = "STATUS_PROCESSED";
    private final static String UNKNOWN = "STATUS_UNKNOWN";
    private final static String EXCEPTION = "EXCEPTION";

    private final static String SPOOL_PATH = "PATH_SPOOL";
    private final static String UPLOAD_PATH = "PATH_UPLOAD";
    private final static String PROCESS_SCRIPT = "PATH_PROCESS_SCRIPT";

    private final static String RMA_FILE = "RMA_FILE";
    private final static String RMA_TABLE = "RMA_TABLE";
    private final static String RMA_COLUMN = "RMA_COLUMN";

    private final static String INSERT_IGNORE = "CMD_INSERT_IGNORE";
    private final static String INSERT_UPDATE = "CMD_INSERT_UPDATE";
    private final static String INSERT_REPLACE = "CMD_INSERT_REPLACE";

    private static DataLoadRequestFactory        dlrFactory;
   
    static
    {
        try
        {
            dlrFactory = (DataLoadRequestFactory)DBAccessFactory.getInstance(
                                            DataLoadRequestFactory.class.getName());
        }
        catch (DBException de)
        {
            de.printStackTrace();
            System.exit(-1);
        }
    }
 
    public void init (ServletConfig config) throws ServletException
    {
	try 
        {
	    super.init(config);

            if (mAdminProp!=null)
            	mLabelsFile = mAdminProp.getProperty(LABEL_PROP_KEY);

	} catch (ServletException e) {
            throw (e);
	}
    }

    public void doGet(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException 
    {
	HttpSession ses = req.getSession(false);
        if (ses == null) {
            System.out.println(getClass().getName() + " No session");
            return;
        }

	if (!doAuthenticate("/serv?type=home&action=edit", req, res)) return;

        storeLabels(ses.getAttribute(CURRENT_LOCALE).toString(), mLabelsFile, ses);

	int role= Integer.parseInt(ses.getAttribute(SES_ROLE).toString());
        if (role==ROLE_RMA)
	    forwardToModule(JSP_LOC_KEY, "rmaUpload.jsp", req, res);
        else
            forwardToModule(JSP_LOC_KEY, "forbidden.jsp", req, res);

	return;
    }

    public void doPost (HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
    {
	HttpSession ses = req.getSession(false);
        if (ses == null) {
            System.out.println(getClass().getName() + " No session");
            return;
        }

	if (!doAuthenticate("/serv?type=home&action=edit", req, res)) return;
        	
        storeLabels(ses.getAttribute(CURRENT_LOCALE).toString(), mLabelsFile, ses);

	int role= Integer.parseInt(ses.getAttribute(SES_ROLE).toString());
        if (role==ROLE_RMA)
        {
	    UploadServlet up = new UploadServlet();
	    HashMap map = up.processUpload(req, res, ses.getAttribute(UPLOAD_PATH).toString());

            verifyUpload(map, ses);
 	    setResult(req, res, map, ses);
	    forwardToModule(JSP_LOC_KEY, "rmaResult.jsp", req, res);
	} else
            forwardToModule(JSP_LOC_KEY, "forbidden.jsp", req, res);

	return;
    }

    private void verifyUpload(HashMap map, HttpSession ses) 
    {
    	Iterator itr = map.values().iterator();

	while (itr.hasNext()) 
        { 
       	    UploadData data = (UploadData) itr.next();
            if (data.name.startsWith("file") && data.isFile) 
            {
		String dirName = data.value.substring(0, data.value.lastIndexOf("."));						
		String rmaPath = ses.getAttribute(UPLOAD_PATH).toString() + 
					ses.getAttribute(RMA).toString() + "/" + dirName + "/";

		ServletContext sc=getServletContext();
                String processScript = sc.getRealPath("/") + "bin/" + ses.getAttribute(PROCESS_SCRIPT).toString();

                String fileExt = data.value.substring(data.value.lastIndexOf(".")+1);
                String cmd = "sh " + processScript + " " + ses.getAttribute(UPLOAD_PATH).toString() +
                                        ses.getAttribute(RMA).toString() + " " + dirName + " " + data.value;

		try 
                {
		    if (fileExt == null || !(fileExt.toLowerCase().equals("csv"))) 
                    {
			updateStatus(map, data.name, ses.getAttribute(FORMAT_FAILED).toString()); 
			Process r = Runtime.getRuntime().exec("rm -rf " + ses.getAttribute(UPLOAD_PATH).toString() + 
					ses.getAttribute(RMA).toString() + "/" + data.value);
			continue;
		    } 

                    Process p = Runtime.getRuntime().exec(cmd);
                    p.waitFor();
                    if (p.exitValue() == 0) 
                    {
   			if (transform2XML(rmaPath, data.value, ses))
			    updateStatus(map, data.name, generateProcessFile(dirName, fileExt, rmaPath, ses));
                        else {
                            updateStatus(map, data.name, ses.getAttribute(CONTENT_FAILED).toString());
 			    Process s = Runtime.getRuntime().exec("rm -rf " + rmaPath);
                            continue;
                        }

                    } else
                         updateStatus(map, data.name, ses.getAttribute(UNKNOWN).toString());

		} catch (IOException ie) {
                    updateStatus(map, data.name, ses.getAttribute(EXCEPTION).toString()+ie.toString());
		    System.out.println("IO Exception : " + ie.toString());
                    ie.printStackTrace();
                } catch (InterruptedException ie) {
                    updateStatus(map, data.name, ses.getAttribute(EXCEPTION).toString()+ie.toString());
                    System.out.println("InterruptedError while write to file : " + ie.toString());
                    ie.printStackTrace();
                }				
	    }
	}
    }

    boolean transform2XML(String xmlPath, String fileName, HttpSession ses)
    	throws IOException
    {
        String rawFile = xmlPath+fileName;
        String xmlFile = xmlPath+fileName.substring(0, fileName.lastIndexOf("."))+".xml";
        File file = new File(rawFile);

        String rawData = null;
            
        if (file.exists()) 
        {
            BufferedReader in = new BufferedReader(new FileReader(rawFile));

            int lineCount = 1;
            String[] codes = new String[4];

            FileWriter pfile = new FileWriter(xmlFile);
            PrintWriter pwrite = new PrintWriter(pfile);

            while ((rawData = in.readLine()) != null) 
            {
                if (lineCount!=1) 
                {
                    StringTokenizer tok = new StringTokenizer(rawData, ",");

                    int count = tok.countTokens();
                    if (count == 4) 
                    {
                        codes[0] = tok.nextToken();
                        codes[1] = tok.nextToken();
                        codes[2] = tok.nextToken();
                        codes[3] = tok.nextToken();
                    } else if (count == 2) {
                        codes[0] = tok.nextToken();
                        codes[1] = tok.nextToken();
                        codes[2] = codes[1];
                        codes[3] = codes[1];
                    } else {
		        in.close();
                        pwrite.close();
                        return false;
                    }

                    pwrite.println("  <ROW num=\""+codes[0]+"\">");
                    pwrite.println("    <PCB_SN>"+codes[1]+"</PCB_SN>");
                    pwrite.println("    <INT_SN>"+codes[2]+"</INT_SN>");
                    pwrite.println("    <EXT_SN>"+codes[3]+"</EXT_SN>");
                    pwrite.println("  </ROW>");

                } else
                    pwrite.println("<ROWSET>");
                    
                lineCount++;
            }

            pwrite.println("</ROWSET>");

            in.close();
            pwrite.close();

        } else
            return false;

        return true;
    }

    String generateProcessFile(String dirName, String fileExt, String xmlPath, HttpSession ses) 
       throws IOException
    {

        String fileName = new String(ses.getAttribute(SPOOL_PATH).toString()+"/"+dirName+".properties");

	File propFile = new File(fileName);

	if (propFile.exists()) 
	    return ses.getAttribute(PROCESSED).toString();
	else {
	    String reqID = createDataLoadRequest(dirName, fileExt, ses);
            String xmlFile = dirName+".xml";
	    FileWriter pfile = new FileWriter(fileName);
	    PrintWriter pwrite = new PrintWriter(pfile);
				
	    pwrite.println("RequestID="+reqID);
            pwrite.println("Counter=0");
            pwrite.println("User="+ses.getAttribute(SES_USER).toString());

	    pwrite.println("Package="+ses.getAttribute(RMA).toString());
	    pwrite.println("LogFile="+xmlPath+dirName+".log");
	    pwrite.println("File0="+xmlPath+xmlFile);
	    pwrite.println("File1="+xmlPath+ses.getAttribute(RMA_FILE).toString());
	    pwrite.println("Table1="+ses.getAttribute(RMA_TABLE).toString());
	    pwrite.println("Command1="+ses.getAttribute(INSERT_UPDATE).toString());			
	    pwrite.println("Column1="+ses.getAttribute(RMA_COLUMN).toString());			

	    pwrite.close();
	    return ses.getAttribute(SUCCESS).toString();
	} 
    }

    boolean updateStatus(HashMap map, String name, String status) 
    {
    	UploadData data=(UploadData) map.get(name);
    	if (data != null) 
        {
	    data.status = status;
    	    return true;
	}
    	return false;
    }

    String createDataLoadRequest(String jobName, String fileExt, HttpSession ses) 
    {
	String reqID = null;

	try
        {
	    dlrFactory.beginTransaction(super.dataSource, false, false);

            try
            {
                String xmlData = null;
		java.util.Date d = new java.util.Date();
        	long mSec = d.getTime();
        	reqID = String.valueOf(mSec);
        	String opID = ses.getAttribute(SES_USER_ID).toString();
        	String roleLevel = ses.getAttribute(SES_ROLE).toString();

		xmlData = "<ROWSET><ROW num=\"1\">";
		xmlData += "<REQUEST_ID>"+reqID+"</REQUEST_ID>";
		xmlData += "<OPERATOR_ID>"+opID+"</OPERATOR_ID>";
		xmlData += "<ROLE_LEVEL>"+roleLevel+"</ROLE_LEVEL>";
		xmlData += "<JOB_NAME>"+jobName+"."+fileExt+"</JOB_NAME>";
		xmlData += "<PROCESS_STATUS>"+"Uploaded"+"</PROCESS_STATUS>";
		xmlData += "</ROW></ROWSET>";
                System.out.println(getClass().getName() + ": ADD DATA XML: " + xmlData);

                try
                {
                    int result = dlrFactory.create(xmlData);
                }
                catch (DBException de)
                {
                    System.out.println(getClass().getName() + ": Data Load Request - NOT ADDED: " + xmlData);
	            System.out.println(getClass().getName() +  ": SQLException occurred - " + de.toString());
                }
            } finally {
         	try 
                {
                    dlrFactory.commitTransaction();
		} catch (DBException de) {
	            System.out.println(getClass().getName() +  ": SQLException occurred - " + de.toString());
                    de.printStackTrace();
                }
            }	
        } catch (Throwable t) {
            System.out.println(getClass().getName() +  ": SQLException occurred - " + t.toString());
            t.printStackTrace();
        }
	return reqID;
    }

    void setResult(HttpServletRequest req, HttpServletResponse res,
		Map map, HttpSession ses) 
    {
    	Iterator itr = map.values().iterator();
	String result = "";
	String startBold = "";
	String endBold = "";

	while (itr.hasNext()) 
        {
    	    UploadData data = (UploadData) itr.next();
            if (data.value != null && !data.name.equals("client") && !data.name.equals("uploadservlet")) 
            {
		if (data.status != null && data.status.equals(ses.getAttribute(SUCCESS).toString())) 
                {
		    startBold = "";
		    endBold = "";
		} else {
		    startBold = "<FONT color=red><b>";
		    endBold = "</b></FONT>";
		}

        	result += "<TR><TD color=#333333 align=left width=50% nowrap=true>" + startBold + (data.value == null ? "" : data.value) + endBold + "</TD>" +
	                  "<TD color=#333333 align=left width=25% nowrap=true>" + startBold + (data.isFile ? String.valueOf(data.size) : "") + endBold + "</TD>" +
                          "<TD color=#333333 align=left width=25% nowrap=true>" + startBold + (data.status == null ? ses.getAttribute(UNKNOWN).toString() : data.status) + endBold + "</TD></TR>";
            }
	}
	UploadBean rma = new UploadBean(result);
	req.setAttribute(UploadBean.ID, rma);
        return;
    }
}


/*
 * ContentUploadServlet.java
 *
 */

package com.broadon.bccs;

import java.io.*;
import java.util.*;
import java.net.*;
import java.sql.*;

import java.math.BigInteger;
import java.security.*;
import java.security.cert.*;
import java.security.spec.*;
import java.security.interfaces.RSAPublicKey;

import javax.mail.*;
import javax.mail.internet.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.sql.DataSource;
import oracle.jdbc.OracleConnectionWrapper;
import org.xml.sax.SAXException;

import com.broadon.ms.common.AdminServlet;
import com.broadon.ms.common.QueryBean;
import com.broadon.db.OracleDataSource;

public class ContentUploadServlet extends AdminServlet 
{
    private static final String CURRENT_LOCALE = "CURRENT_LOCALE";
    private static final String LABEL_PROP_KEY = "CONTENT_UPLOAD_PROP";
    private static final String JSP_LOC_KEY = "CONTENT_UPLOAD_JSP_LOC";

    private final static String CONTENT = "CLIENT_CONTENT";
    private final static String SIG_FAILED = "STATUS_SIG_FAILED";
    private final static String UNKNOWN = "STATUS_UNKNOWN";
    private final static String FORMAT_FAILED = "STATUS_FORMAT_FAILED";
    private final static String EXTRACT_FAILED = "STATUS_EXTRACT_FAILED";
    private final static String CONTENT_FAILED = "STATUS_CONTENT_FAILED";
    private final static String PROCESSED = "STATUS_PROCESSED";
    private final static String UPLOADED = "STATUS_UPLOADED";
    private final static String DATABASE_DOWN = "STATUS_DATABASE_DOWN";
    private final static String FILE_ERROR = "STATUS_FILE_ERROR";
    private final static String EXCEPTION = "STATUS_EXCEPTION_OCCURED";

    private final static String UPLOAD_SUCCESSFUL = "STATUS_UPLOAD_SUCCESSFUL";
    private final static String UPLOAD_FAILED = "STATUS_UPLOAD_FAILED";
    private final static String VALIDATE_SUCCESSFUL = "STATUS_VALIDATE_SUCCESSFUL";
    private final static String PROCESS_SUCCESSFUL = "STATUS_PROCESS_SUCCESSFUL";
    
    private final static String SPOOL_PATH = "PATH_SPOOL";
    private final static String UPLOAD_PATH = "PATH_UPLOAD";
    private final static String PROCESS_SCRIPT_P1 = "PATH_PROCESS_SCRIPT_P1";
    private final static String PROCESS_SCRIPT_P4 = "PATH_PROCESS_SCRIPT_P4";

    private final static String TAR_FILE = "CONTENT_TAR_FILE";
    private final static String SIG_FILE = "CONTENT_SIG_FILE";
    private final static String CERT_ID_FILE = "CONTENT_CERT_ID_FILE";
    private final static String CONTENT_ETICKET_METADATA_FILE = "CONTENT_ETICKET_METADATA_FILE";
    private final static String CONTENT_ETICKET_METADATA_TABLE = "CONTENT_ETICKET_METADATA_TABLE";
    private final static String CONTENTS_FILE = "CONTENTS_FILE";
    private final static String CONTENTS_TABLE = "CONTENTS_TABLE";
    private final static String CONTENT_CACHE_FILE = "CONTENT_CACHE_FILE";
    private final static String CONTENT_CACHE_TABLE = "CONTENT_CACHE_TABLE";
    private final static String CONTENT_OBJECTS_FILE = "CONTENT_OBJECTS_FILE";
    private final static String CONTENT_OBJECT = "CONTENT_OBJECT";
    private final static String CONTENT_OBJECTS_TABLE = "CONTENT_OBJECTS_TABLE";
    private final static String CONTENT_TITLE_OBJECTS_FILE = "CONTENT_TITLE_OBJECTS_FILE";
    private final static String CONTENT_TITLE_OBJECTS_TABLE = "CONTENT_TITLE_OBJECTS_TABLE";
    private final static String CONTENT_TITLE_REVIEWS_FILE = "CONTENT_TITLE_REVIEWS_FILE";
    private final static String CONTENT_TITLE_REVIEWS_TABLE = "CONTENT_TITLE_REVIEWS_TABLE";
    private final static String CONTENT_TITLES_FILE = "CONTENT_TITLES_FILE";
    private final static String CONTENT_TITLES_TABLE = "CONTENT_TITLES_TABLE";
    private final static String BB_HW_RELEASES_FILE = "BB_HW_RELEASES_FILE";
    private final static String BB_HW_RELEASES_TABLE = "BB_HW_RELEASES_TABLE";

    private final static String INSERT_IGNORE = "CMD_INSERT_IGNORE";
    private final static String INSERT_UPDATE = "CMD_INSERT_UPDATE";
    private final static String INSERT_REPLACE = "CMD_INSERT_REPLACE";

    private String statusMsg;

    public void init (ServletConfig config) throws ServletException
    {
	try 
        {
            super.init(config);

      	    if (mAdminProp != null)
            	mLabelsFile = mAdminProp.getProperty(LABEL_PROP_KEY);

        } catch (ServletException e) {
            throw (e);
	}
    }

    public void doGet(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException 
    {
	HttpSession ses = req.getSession(false);
       	if (ses == null) 
        {
 	    System.out.println(getClass().getName() + " No session");
            return;
        }

	if (!doAuthenticate("/serv?type=home&action=edit", req, res)) return;

        storeLabels(ses.getAttribute(CURRENT_LOCALE).toString(), mLabelsFile, ses); 

	int role = Integer.parseInt(ses.getAttribute(SES_ROLE).toString());
        if (role == ROLE_CONTENT)
	    forwardToModule(JSP_LOC_KEY, "contentUpload.jsp", req, res);
        else
            forwardToModule(JSP_LOC_KEY, "forbidden.jsp", req, res);

	return;
    }

    public void doPost (HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException 
    {
	HttpSession ses = req.getSession(false);
       	if (ses == null) 
        {
	    System.out.println(getClass().getName() + " No session");
            return;
        }

	if (!doAuthenticate("/serv?type=home&action=edit", req, res)) return;
        	
        storeLabels(ses.getAttribute(CURRENT_LOCALE).toString(), mLabelsFile, ses);

        statusMsg = "";

	int role = Integer.parseInt(ses.getAttribute(SES_ROLE).toString());
        if (role == ROLE_CONTENT)
            processUpload(req, res, ses);         
	else
            forwardToModule(JSP_LOC_KEY, "forbidden.jsp", req, res);

	return;
    }

    private void processUpload(HttpServletRequest req, HttpServletResponse res, HttpSession ses) 
		throws ServletException, IOException 
    {
        Connection wrappedConnection = null;
        Connection conn = null;

        try
        {
            DataSource ds = super.dataSource;
            conn = wrappedConnection = ds.getConnection();
            if (conn instanceof OracleDataSource.ConnectionWrapper)
                conn = ((OracleConnectionWrapper) conn).unwrap();
            conn.setAutoCommit(false);

            UploadAdmin ua = new UploadAdmin();

	    if (ua.checkDB(conn)) 
            {
 	        UploadServlet up = new UploadServlet();
	        HashMap map = up.processUpload(req, res, ses.getAttribute(UPLOAD_PATH).toString());

                verifyUpload(ua, map, ses, conn);
                setResult(req, res, map, ses);

            } else {
                setResult(req, res, null, ses);
            }

	} catch (SQLException se) {
	    System.out.println("SQL Exception : " + se.toString());
            se.printStackTrace();
            setResult(req, res, null, ses);

            try 
            {
                conn.rollback();
            } catch (SQLException s) {
                s.printStackTrace();
            }

        } finally {
            try 
            {
                wrappedConnection.close();
            } catch (SQLException s) {
                s.printStackTrace();
            }
        }

        forwardToModule(JSP_LOC_KEY, "contentResult.jsp", req, res);
    }

    private void verifyUpload(UploadAdmin ua, HashMap map, HttpSession ses, Connection conn) 
        throws SQLException
    {
    	Iterator itr = map.values().iterator();

	while (itr.hasNext()) 
        { 
      	    UploadData data = (UploadData) itr.next();
            statusMsg = "";
   
            if (data.name.startsWith("file") && data.isFile) 
            {
		String dirName = data.value.substring(0, data.value.lastIndexOf("."));						
		String processScript = null;

  		ServletContext sc=getServletContext();
				
		if (dirName.startsWith("content"))
                    processScript = sc.getRealPath("/") + "bin/" + 
                                        ses.getAttribute(PROCESS_SCRIPT_P1).toString(); 
                else if (dirName.startsWith("sa"))
                    processScript = sc.getRealPath("/") + "bin/" + 
                                        ses.getAttribute(PROCESS_SCRIPT_P4).toString(); 
                else
                    processScript = sc.getRealPath("/") + "bin/" + 
                                        ses.getAttribute(PROCESS_SCRIPT_P1).toString(); 

		String contentPath = ses.getAttribute(UPLOAD_PATH).toString() + 
				         ses.getAttribute(CONTENT).toString() + "/" + dirName + "/";
				
		String fileExt = data.value.substring(data.value.lastIndexOf(".")+1);						
		String cmd = "sh " + processScript + " " + ses.getAttribute(UPLOAD_PATH).toString() + 
  			         ses.getAttribute(CONTENT).toString() + " " + dirName + " " + fileExt;
		try 
                {
		    if (fileExt == null || !(fileExt.toLowerCase().equals("tar"))) 
                    {
			statusMsg = ses.getAttribute(FORMAT_FAILED).toString(); 

			Process r = Runtime.getRuntime().exec("rm -rf " + ses.getAttribute(UPLOAD_PATH).toString() + 
  	 			        ses.getAttribute(CONTENT).toString() + "/" + data.value);
		    } else {
		        Process p = Runtime.getRuntime().exec(cmd);
  		        p.waitFor();

		        if (p.exitValue() == 1)
			    statusMsg = ses.getAttribute(EXTRACT_FAILED).toString(); 
		        else if (p.exitValue() == 2)
			    statusMsg = ses.getAttribute(CONTENT_FAILED).toString(); 
                        else if (p.exitValue() == 0) {
                            if(verifySignature(contentPath+ses.getAttribute(TAR_FILE).toString(),
                                               contentPath+ses.getAttribute(SIG_FILE).toString(),
                                               contentPath+ses.getAttribute(CERT_ID_FILE).toString(), ua, conn)) 
                                statusMsg = createPropertyFile(dirName, fileExt, contentPath, ses, ua, conn);
                            else
                                statusMsg = ses.getAttribute(SIG_FAILED).toString();
                        } else 
   		 	    statusMsg = ses.getAttribute(UNKNOWN).toString(); 
                    }

                    String sString = "";
                    if (statusMsg == null || statusMsg.equals("")) 
                    {
                        conn.commit();
                        sString = ses.getAttribute(UPLOAD_SUCCESSFUL).toString()+"\n";

                        ValidateUpload vu = new ValidateUpload(dataSource);
                        statusMsg = vu.doValidation(data.value);

                        if (statusMsg == null || statusMsg.equals("")) 
                        {
                            sString += ses.getAttribute(VALIDATE_SUCCESSFUL).toString()+"\n";

                            ProcessUpload pu = new ProcessUpload(dataSource);
                            statusMsg = pu.doProcessing(data.value);

                            if (statusMsg == null || statusMsg.equals("")) 
                                sString += ses.getAttribute(PROCESS_SUCCESSFUL).toString();
                        }

                    } else {
                        sString = ses.getAttribute(UPLOAD_FAILED).toString()+"\n";
                        conn.rollback();
                    }

                    if (statusMsg == null)
		        statusMsg = "";

       	            ua.setStatus(map, data.name, escNewLine(sString+statusMsg)); 

		} catch (IOException ie) {
                    statusMsg = ses.getAttribute(EXCEPTION).toString() + ie.toString();
		    System.out.println("IO Error : " + ie.toString());

		} catch (InterruptedException ie) { 
                    statusMsg = ses.getAttribute(EXCEPTION).toString() + ie.toString();
		    System.out.println("InterruptedError while write to file : " + ie.toString());
		} 
	    }
	}
    }

    // Verifies the signature for the Content file
    private boolean verifySignature(String dataPath, String sigPath, String certPath,
                                    UploadAdmin ua, Connection conn)
        throws IOException
    {
        boolean result = false;

        try 
        {
            File sigFile = new File(sigPath);
            byte[] sigFileBuffer = new byte[(int)sigFile.length()];
            FileInputStream fsig = new FileInputStream(sigFile);

            if (fsig.read(sigFileBuffer) != sigFileBuffer.length)
                throw new IOException("Error reading signature file");

            fsig.close();

            BufferedReader in = new BufferedReader(new FileReader(certPath));
            String cert_id = in.readLine();
            in.close();

            if (cert_id == null)
                throw new IOException("Error reading CERT_ID from file");

            try 
            {
                ua.getCertificate(cert_id, conn);

                String public_key = ua.getCertPublicKey();
                java.sql.Date revoke_date = ua.getCertRevokeDate();

                if (revoke_date != null && !revoke_date.toString().equals("")) 
                {
                    System.out.println(getClass().getName() + ": CERT_ID " + cert_id + " has been revoked since " +
                                       revoke_date.toString());
                } else {

                    if (public_key != null && !public_key.equals("")) 
                    {
                        BigInteger modulus = new BigInteger(public_key, 32);
                        BigInteger exponent = new BigInteger("10001", 16);

                        KeyFactory kf = KeyFactory.getInstance("RSA");
                        RSAPublicKey pubKey = (RSAPublicKey) kf.generatePublic(new RSAPublicKeySpec(modulus, exponent));

                        // Initialize a signature object with the public key
                        Signature sigObj = Signature.getInstance("SHA1withRSA");
                        sigObj.initVerify(pubKey);

                        File datFile = new File(dataPath);
                        byte[] buffer = new byte[32*1024];
                        FileInputStream fdat = new FileInputStream(datFile);
                        int n; // Number of bytes read

                        while (fdat.available() != 0) 
                        {
                            n = fdat.read(buffer);
                            sigObj.update(buffer, 0, n);
                        };

                        fdat.close();
                        result = sigObj.verify(sigFileBuffer);

                    } else {
                        System.out.println(getClass().getName() + ": Public Key for CERT_ID " + cert_id + " not found.");
                    }
                }

            } catch(Exception e) {
                System.out.println("Exception occured while verifying signature: " + e.toString());
                e.printStackTrace();
            } 

        } catch (Throwable t) {
            t.printStackTrace();
        }

        return result;

    } // verifySignature

    private String createPropertyFile(String dirName, String fileExt, String xmlPath, HttpSession ses, 
                                      UploadAdmin ua, Connection conn)
    {

        String fileName = new String(ses.getAttribute(SPOOL_PATH).toString()+"/"+dirName+".properties");

        File propFile = new File(fileName);

        if (propFile.exists()) 
        {
            return ses.getAttribute(PROCESSED).toString();
        } else {

           try
           {
                String reqID = ua.generateRequestID();
                String xmlData = getUploadRequestXML(reqID, dirName, fileExt, ses);

                File temp =  File.createTempFile(reqID, null);
                FileWriter pfile = new FileWriter(temp);
                PrintWriter pwrite = new PrintWriter(pfile);

                pwrite.println("RequestID="+reqID);
                pwrite.println("Counter=0");
                pwrite.println("User="+ses.getAttribute(SES_USER).toString());

                pwrite.println("File0="+xmlPath+ses.getAttribute(CONTENT_OBJECTS_FILE).toString());

                pwrite.println("File1="+xmlPath+ses.getAttribute(CONTENT_CACHE_FILE).toString());
                pwrite.println("Table1="+ses.getAttribute(CONTENT_CACHE_TABLE).toString());
                pwrite.println("Command1="+ses.getAttribute(INSERT_IGNORE).toString());

                pwrite.println("File2="+xmlPath+ses.getAttribute(CONTENTS_FILE).toString());
                pwrite.println("Table2="+ses.getAttribute(CONTENTS_TABLE).toString());
                pwrite.println("Command2="+ses.getAttribute(INSERT_IGNORE).toString());

                if (dirName.startsWith("content")) 
                {
                    pwrite.println("File3="+xmlPath+ses.getAttribute(CONTENT_ETICKET_METADATA_FILE).toString());
                    pwrite.println("Table3="+ses.getAttribute(CONTENT_ETICKET_METADATA_TABLE).toString());
                    pwrite.println("Command3="+ses.getAttribute(INSERT_IGNORE).toString());

                    pwrite.println("File4="+xmlPath+ses.getAttribute(CONTENT_TITLES_FILE).toString());
                    pwrite.println("Table4="+ses.getAttribute(CONTENT_TITLES_TABLE).toString());
                    pwrite.println("Command4="+ses.getAttribute(INSERT_REPLACE).toString());

                    pwrite.println("File5="+xmlPath+ses.getAttribute(CONTENT_TITLE_OBJECTS_FILE).toString());
                    pwrite.println("Table5="+ses.getAttribute(CONTENT_TITLE_OBJECTS_TABLE).toString());
                    pwrite.println("Command5="+ses.getAttribute(INSERT_IGNORE).toString());

                    pwrite.println("File6="+xmlPath+ses.getAttribute(CONTENT_TITLE_REVIEWS_FILE).toString());
                    pwrite.println("Table6="+ses.getAttribute(CONTENT_TITLE_REVIEWS_TABLE).toString());
                    pwrite.println("Command6="+ses.getAttribute(INSERT_REPLACE).toString());

                } else if (dirName.startsWith("sa")) {
                    pwrite.println("File3="+xmlPath+ses.getAttribute(BB_HW_RELEASES_FILE).toString());
                    pwrite.println("Table3="+ses.getAttribute(BB_HW_RELEASES_TABLE).toString());
                    pwrite.println("Command3="+ses.getAttribute(INSERT_REPLACE).toString());
                }

                pwrite.close();

                if (ua.executeStatusUpdate(xmlData, conn)) 
                {
                    if (temp.renameTo(propFile))
                        return null;

                    else {
                        return ses.getAttribute(FILE_ERROR).toString();
                    }

                } else {
                    temp.deleteOnExit();
                    return ses.getAttribute(DATABASE_DOWN).toString();
                }
            } catch (IOException ie) {
                return ses.getAttribute(EXCEPTION).toString()+ie.toString();

            } catch (SQLException se) {
                return ses.getAttribute(EXCEPTION).toString()+se.toString();

            } catch (SAXException xe) {
                return ses.getAttribute(EXCEPTION).toString()+xe.toString();
            }
        }
    }

    private String getUploadRequestXML(String reqID, String jobName, String fileExt, HttpSession ses)
    {
        String xmlData = "<ROWSET><ROW num=\"1\">";

        xmlData += "<REQUEST_ID>"+reqID+"</REQUEST_ID>";
        xmlData += "<OPERATOR_ID>"+ses.getAttribute(SES_USER_ID).toString()+"</OPERATOR_ID>";
        xmlData += "<ROLE_LEVEL>"+ses.getAttribute(SES_ROLE).toString()+"</ROLE_LEVEL>";
        xmlData += "<JOB_NAME>"+jobName+"."+fileExt+"</JOB_NAME>";
        xmlData += "<PROCESS_STATUS>"+ses.getAttribute(UPLOADED).toString()+"</PROCESS_STATUS>";
        xmlData += "</ROW></ROWSET>";

        System.out.println(getClass().getName() + ": ADD DATA XML: " + xmlData);

        return xmlData;

    } //getUploadRequestXML

    private void setResult(HttpServletRequest req, HttpServletResponse res,  Map map, HttpSession ses)
    {
        String htmlResult = "";
        String dColor = "#CCCCCC";
        String rColor = "#DDDDDD";

        if (map != null) 
        {
            Iterator itr = map.values().iterator();

            for (int i=1; itr.hasNext(); i++) 
            {
                UploadData data = (UploadData) itr.next();
 
                if (data.value != null && !data.name.equals("client") && !data.name.equals("uploadservlet"))
                {
                    htmlResult += "<TR><TD bgcolor=" + dColor + " align=left width=80% nowrap=true><b>" + 
                              (data.value == null ? "" : data.value) + "</b></TD>" +
                              "<TD bgcolor=" + dColor + " align=left width=20% nowrap=true><b>" + 
                              (data.isFile ? String.valueOf(data.size) : "") + "</b></TD></TR>" +
                              "<TR><TD colspan=2 bgcolor=" + rColor + " align=left>" + 
                              (data.status == null ? ses.getAttribute(UNKNOWN).toString() : data.status) + "</TD></TR>";
                }
            }

        } else {
            htmlResult += "<TR><TD colspan=2 color=" + rColor + ">" + ses.getAttribute(DATABASE_DOWN).toString() + "</TD></TR>";
        }

        QueryBean qb = new QueryBean();
        qb.setResult(htmlResult);

        req.setAttribute(QueryBean.ID, qb);
        return;

    } //setResult

    private String escNewLine(String str) 
    {
        if (str == null) {
            return null;
        } else if(str.equals("")) {
            return "";
        } else {
            str = str.replaceAll("\n","<br>");
            return str;
        }
    }

}

/*
 * CAUploadServlet.java
 *
 */

package com.broadon.bccs;

import java.io.*;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

import com.broadon.ms.common.AdminServlet;
import com.broadon.db.DBAccessFactory;
import com.broadon.db.DBException;
import com.broadon.db.DataLoadRequestFactory;
import com.broadon.online.UploadBean;

public class CAUploadServlet extends AdminServlet {
    	private static final String CURRENT_LOCALE = "CURRENT_LOCALE";
    	private static final String LABEL_PROP_KEY = "CA_UPLOAD_PROP";
    	private static final String JSP_LOC_KEY = "CA_UPLOAD_JSP_LOC";

	private final static String CA = "CLIENT_CA";
	private final static String SIG_FAILED = "STATUS_SIG_FAILED";
	private final static String SUCCESS = "STATUS_SUCCESS";
	private final static String UNKNOWN = "STATUS_UNKNOWN";
	private final static String FORMAT_FAILED = "STATUS_FORMAT_FAILED";
	private final static String EXTRACT_FAILED = "STATUS_EXTRACT_FAILED";
	private final static String CONTENT_FAILED = "STATUS_CONTENT_FAILED";
	private final static String PROCESSED = "STATUS_PROCESSED";
	private final static String EXCEPTION = "EXCEPTION";

        private final static String SPOOL_PATH = "PATH_SPOOL";
	private final static String UPLOAD_PATH = "PATH_UPLOAD";
	private final static String PROCESS_SCRIPT = "PATH_PROCESS_SCRIPT";

	private final static String CERT_FILE = "CA_CERT_FILE";
	private final static String CERT_TABLE = "CA_CERT_TABLE";
	private final static String CHAIN_FILE = "CA_CHAIN_FILE";
	private final static String CHAIN_TABLE = "CA_CHAIN_TABLE";
	private final static String CRL_FILE = "CA_CRL_FILE";
	private final static String CRL_TABLE = "CA_CRL_TABLE";
	private final static String CRL_COLUMN = "CA_CRL_COLUMN";

	private final static String INSERT_IGNORE = "CMD_INSERT_IGNORE";
	private final static String INSERT_UPDATE = "CMD_INSERT_UPDATE";
	private final static String INSERT_REPLACE = "CMD_INSERT_REPLACE";

	private static DataLoadRequestFactory        dlrFactory;
   
      static
      {
          try
          {
              dlrFactory = (DataLoadRequestFactory)DBAccessFactory.getInstance(
                                            DataLoadRequestFactory.class.getName());
          }
          catch (DBException de)
          {
              de.printStackTrace();
              System.exit(-1);
          }
      }
 
      public void init (ServletConfig config) throws ServletException
        {
		try {
		        super.init(config);

            		if (mAdminProp!=null)
                		mLabelsFile = mAdminProp.getProperty(LABEL_PROP_KEY);

	        } catch (ServletException e) {
        	        throw (e);
		}
	}

	public void doGet(HttpServletRequest req, HttpServletResponse res)
				throws ServletException, IOException {
		HttpSession ses = req.getSession(false);
        	if (ses == null) {
            		System.out.println(getClass().getName() + " No session");
            		return;
        	}

		if (!doAuthenticate("/serv?type=home&action=edit", req, res)) return;

        	storeLabels(ses.getAttribute(CURRENT_LOCALE).toString(), mLabelsFile, ses);

		int role= Integer.parseInt(ses.getAttribute(SES_ROLE).toString());
        	if (role==ROLE_CA)
			forwardToModule(JSP_LOC_KEY, "caUpload.jsp", req, res);
                else
                        forwardToModule(JSP_LOC_KEY, "forbidden.jsp", req, res);

		return;
    	}

	public void doPost (HttpServletRequest req, HttpServletResponse res)
				throws ServletException, IOException {
		HttpSession ses = req.getSession(false);
        	if (ses == null) {
            		System.out.println(getClass().getName() + " No session");
            		return;
        	}

		if (!doAuthenticate("/serv?type=home&action=edit", req, res)) return;
        	
        	storeLabels(ses.getAttribute(CURRENT_LOCALE).toString(), mLabelsFile, ses);

		int role= Integer.parseInt(ses.getAttribute(SES_ROLE).toString());
        	if (role==ROLE_CA)
        	{
			UploadServlet up = new UploadServlet();
			HashMap map = up.processUpload(req, res, ses.getAttribute(UPLOAD_PATH).toString());

               		verifyUpload(map, ses);
 			setResult(req, res, map, ses);
		        forwardToModule(JSP_LOC_KEY, "caResult.jsp", req, res);
		}
                else
                        forwardToModule(JSP_LOC_KEY, "forbidden.jsp", req, res);

		return;
        }

	private void verifyUpload(HashMap map, HttpSession ses) {
    		Iterator itr = map.values().iterator();

		while (itr.hasNext()) { 
       	        	UploadData data = (UploadData) itr.next();
               		if (data.name.startsWith("file") && data.isFile) {

				String dirName = data.value.substring(0, data.value.lastIndexOf("."));						
				String caPath = ses.getAttribute(UPLOAD_PATH).toString() + 
							ses.getAttribute(CA).toString() + "/" + dirName + "/";
				
  				ServletContext sc=getServletContext();
                                String processScript = sc.getRealPath("/") + "bin/" + ses.getAttribute(PROCESS_SCRIPT).toString();
		
				String fileExt = data.value.substring(data.value.lastIndexOf(".")+1);						
				String cmd = "sh " + processScript + " " + ses.getAttribute(UPLOAD_PATH).toString() + 
							ses.getAttribute(CA).toString() + " " + dirName + " " + fileExt;
				try {
					if (fileExt == null || !(fileExt.toLowerCase().equals("tar"))) {
						updateStatus(map, data.name, ses.getAttribute(FORMAT_FAILED).toString()); 
						Process r = Runtime.getRuntime().exec("rm -rf " + ses.getAttribute(UPLOAD_PATH).toString() + 
									ses.getAttribute(CA).toString() + "/" + data.value);
						continue;
					}

					Process p = Runtime.getRuntime().exec(cmd);
					p.waitFor();
					if (p.exitValue() == 1)
						updateStatus(map, data.name, ses.getAttribute(EXTRACT_FAILED).toString()); 
					else if (p.exitValue() == 2)
						updateStatus(map, data.name, ses.getAttribute(CONTENT_FAILED).toString()); 
					else if (p.exitValue() == 0) 
						updateStatus(map, data.name, generateProcessFile(dirName, fileExt, caPath, ses));
					else 
						updateStatus(map, data.name, ses.getAttribute(UNKNOWN).toString()); 
				} catch (IOException ie) {
					System.out.println("IO Error : " + ie.toString());
				} catch (InterruptedException ie) { 
					System.out.println("InterruptedError while write to file : " + ie.toString());
				} 
			}
		}
	}

	String generateProcessFile(String dirName, String fileExt, String xmlPath, HttpSession ses) {

		String fileName = new String(ses.getAttribute(SPOOL_PATH).toString()+"/"+dirName+".properties");

		File propFile = new File(fileName);

		if(propFile.exists()) {
			return ses.getAttribute(PROCESSED).toString();
		} else {
			try {
				String reqID = createDataLoadRequest(dirName, fileName, fileExt, ses);

				FileWriter pfile = new FileWriter(fileName);
				PrintWriter pwrite = new PrintWriter(pfile);
				
 				pwrite.println("RequestID="+reqID);
                                pwrite.println("Counter=0");
                                pwrite.println("User="+ses.getAttribute(SES_USER).toString());

				pwrite.println("File1="+xmlPath+ses.getAttribute(CERT_FILE).toString());
				pwrite.println("Table1="+ses.getAttribute(CERT_TABLE).toString());
				pwrite.println("Command1="+ses.getAttribute(INSERT_REPLACE).toString());			
				pwrite.println("File2="+xmlPath+ses.getAttribute(CHAIN_FILE).toString());
				pwrite.println("Table2="+ses.getAttribute(CHAIN_TABLE).toString());
				pwrite.println("Command2="+ses.getAttribute(INSERT_REPLACE).toString());			
				pwrite.println("File3="+xmlPath+ses.getAttribute(CRL_FILE).toString());
				pwrite.println("Table3="+ses.getAttribute(CRL_TABLE).toString());
				pwrite.println("Command3="+ses.getAttribute(INSERT_UPDATE).toString());			
				pwrite.println("Column3="+ses.getAttribute(CRL_COLUMN).toString());			

				pwrite.close();
				return ses.getAttribute(SUCCESS).toString();
			} catch (IOException ie) {
				return ses.getAttribute(EXCEPTION).toString()+ie.toString();
			}
		}
	}

    	boolean updateStatus(HashMap map, String name, String status) {
    		UploadData data=(UploadData) map.get(name);
    		if (data != null) {
			data.status = status;
    			return true;
		}
    		return false;
    	}

	String createDataLoadRequest(String jobName, String processFile, String fileExt, HttpSession ses) {
		String reqID = null;

		try
        	{
	            	dlrFactory.beginTransaction(super.dataSource, false, false);

        	    	try
            		{
                        	String xmlData = null;
				java.util.Date d = new java.util.Date();
        			long mSec = d.getTime();
        			reqID = String.valueOf(mSec);
        			String opID = ses.getAttribute(SES_USER_ID).toString();
        			String roleLevel = ses.getAttribute(SES_ROLE).toString();

			        xmlData = "<ROWSET><ROW num=\"1\">";
			        xmlData += "<REQUEST_ID>"+reqID+"</REQUEST_ID>";
			        xmlData += "<OPERATOR_ID>"+opID+"</OPERATOR_ID>";
			        xmlData += "<ROLE_LEVEL>"+roleLevel+"</ROLE_LEVEL>";
		                xmlData += "<JOB_NAME>"+jobName+"."+fileExt+"</JOB_NAME>";
		           	xmlData += "<PROCESS_STATUS>"+"Uploaded"+"</PROCESS_STATUS>";
			        xmlData += "</ROW></ROWSET>";
                        	System.out.println(getClass().getName() + ": ADD DATA XML: " + xmlData);

                        	try
                        	{
                            		int result = dlrFactory.create(xmlData);
                        	}
                        	catch (DBException de)
                        	{
	                        	System.out.println(getClass().getName() + ": Data Load Request - NOT ADDED: " + xmlData);
	                        	System.out.println(getClass().getName() +  ": SQLException occurred - " + de.toString());
                        	}
                  	} finally {
         		      	try {
                  			dlrFactory.commitTransaction();
		            	} catch (DBException de) {
	                        	System.out.println(getClass().getName() +  ": SQLException occurred - " + de.toString());
                    			de.printStackTrace();
                		}
            		}
        	} catch (Throwable t) {
                       	System.out.println(getClass().getName() +  ": SQLException occurred - " + t.toString());
            		t.printStackTrace();
        	}
		return reqID;
	}

    	void setResult(HttpServletRequest req, HttpServletResponse res,
			Map map, HttpSession ses) 
	{
    		Iterator itr = map.values().iterator();
		String result = "";
		String startBold = "";
		String endBold = "";

	    	while (itr.hasNext()) {
    			UploadData data = (UploadData) itr.next();
                        if (data.value != null && !data.name.equals("client") && !data.name.equals("uploadservlet")) {
				if(data.status != null && data.status.equals(ses.getAttribute(SUCCESS).toString())) {
					startBold = "";
					endBold = "";
				} else {
					startBold = "<FONT color=red><b>";
					endBold = "</b></FONT>";
				}

        	                result += "<TR><TD color=#333333 align=left width=50% nowrap=true>" + startBold + (data.value == null ? "" : data.value) + endBold + "</TD>" +
	                	             "<TD color=#333333 align=left width=25% nowrap=true>" + startBold + (data.isFile ? String.valueOf(data.size) : "") + endBold + "</TD>" +
                        		     "<TD color=#333333 align=left width=25% nowrap=true>" + startBold + (data.status == null ? ses.getAttribute(UNKNOWN).toString() : data.status) + endBold + "</TD></TR>";
                        }
	    	}
		UploadBean ca = new UploadBean(result);
	        req.setAttribute(UploadBean.ID, ca);
        	return;
    	}
}


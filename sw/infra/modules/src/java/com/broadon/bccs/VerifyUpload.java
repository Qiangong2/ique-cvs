package com.broadon.bccs;

import java.math.BigInteger;
import java.security.*;
import java.security.spec.*;
import java.security.interfaces.RSAPublicKey;

import java.io.*;
import java.util.*;
import javax.sql.DataSource;

import com.broadon.db.DBAccessFactory;
import com.broadon.db.DBException;
import com.broadon.db.Certificate;
import com.broadon.db.CertificateFactory;

public class VerifyUpload
{
    private static CertificateFactory certFactory;

    static
    {
       	try
       	{
    	    certFactory = (CertificateFactory)DBAccessFactory.getInstance(
                                    CertificateFactory.class.getName());
        }
        catch (DBException de)
        {
            de.printStackTrace();
            System.exit(-1);
        }
    }

    // Verifies the signature for the NEC-export file
    public boolean NECSignature(String dataPath, String sigPath, String certPath, String lotsPath, 
                                    DataSource dataSource, Map nameMap)
	throws IOException
    {   
	boolean result = false;
	try {
            certFactory.beginTransaction(dataSource, false, true);
	    String certFrom = lotsPath;
	    String cert_id = null;

	    File certFile = new File(certPath);
            if (certFile.exists())
                certFrom = certPath;

            File sigFile = new File(sigPath);
            byte[] sigFileBuffer = new byte[(int)sigFile.length()];
            FileInputStream fsig = new FileInputStream(sigFile);
            if (fsig.read(sigFileBuffer) != sigFileBuffer.length)
                throw new IOException("Error reading signature file");
            fsig.close();

            BufferedReader in = new BufferedReader(new FileReader(certFrom));
            if (certFrom.equals(certPath)) {
	        cert_id = in.readLine();
	        in.close();
            } else if (certFrom.equals(lotsPath)) {
                String tmpStr = null;

                while ((tmpStr = in.readLine()) != null) {
                    if (tmpStr.startsWith("<CERT_ID>")) {
                        cert_id = tmpStr.substring(tmpStr.indexOf(">")+1, tmpStr.lastIndexOf("<"));
                        break;
                    }
                }
                in.close();
            }

	    if (cert_id == null)
		throw new IOException("Error reading CERT_ID from file");
	
  	    try {
                Certificate cert = certFactory.getCertificate(cert_id, nameMap);
		String public_key = cert.getPublicKey();;
		java.util.Date revoke_date = cert.getRevokeDate();
				
                if (revoke_date != null && !revoke_date.toString().equals("")) {
		    System.out.println(getClass().getName() + ": CERT_ID " + cert_id + " has been revoked since " +
                                       revoke_date.toString());
                } else {
		    if (public_key != null && !public_key.equals("")) {
		        BigInteger modulus = new BigInteger(public_key, 32);
		        BigInteger exponent = new BigInteger("10001", 16);

		        KeyFactory kf = KeyFactory.getInstance("RSA");
		        RSAPublicKey pubKey = (RSAPublicKey) kf.generatePublic(new RSAPublicKeySpec(modulus, exponent));

		        // Initialize a signature object with the public key
		        Signature sigObj = Signature.getInstance("SHA1withRSA");
		        sigObj.initVerify(pubKey);
          	
		        File datFile = new File(dataPath);
		        byte[] buffer = new byte[32*1024];
	                FileInputStream fdat = new FileInputStream(datFile);
                        int n; // Number of bytes read
                        while (fdat.available() != 0) {
		 	    n = fdat.read(buffer);
			    sigObj.update(buffer, 0, n);
		        };
		        fdat.close();
		        result = sigObj.verify(sigFileBuffer);
                    } else {
			System.out.println(getClass().getName() + ": Public Key for CERT_ID " + cert_id + " not found.");
  		    }
		}
	    } catch(Exception e) {
	     	System.out.println("Exception occured while verifying signature: " + e.toString());
		e.printStackTrace();
	    } finally {
	        try {
          	    certFactory.commitTransaction();
		} catch (DBException de) {
                    de.printStackTrace();
                }
            }
	} catch (Throwable t) {
            t.printStackTrace();
        }
	return result;
    }  

    // Verifies the signature for the Content file
    public boolean ContentSignature(String dataPath, String sigPath, String certPath, 
                                    DataSource dataSource, Map nameMap)
		throws IOException
    {   
	boolean result = false;
	try {
            certFactory.beginTransaction(dataSource, false, true);

            File sigFile = new File(sigPath);
            byte[] sigFileBuffer = new byte[(int)sigFile.length()];
            FileInputStream fsig = new FileInputStream(sigFile);

            if (fsig.read(sigFileBuffer) != sigFileBuffer.length)
                throw new IOException("Error reading signature file");

            fsig.close();

            BufferedReader in = new BufferedReader(new FileReader(certPath));
	    String cert_id = in.readLine();
	    in.close();

	    if (cert_id == null)
		throw new IOException("Error reading CERT_ID from file");
	
	    try {
                Certificate cert = certFactory.getCertificate(cert_id, nameMap);
		String public_key = cert.getPublicKey();;
		java.util.Date revoke_date = cert.getRevokeDate();
				
                if (revoke_date != null && !revoke_date.toString().equals("")) {
		    System.out.println(getClass().getName() + ": CERT_ID " + cert_id + " has been revoked since " +
                                       revoke_date.toString());
                } else {
		    if (public_key != null && !public_key.equals("")) {
	                BigInteger modulus = new BigInteger(public_key, 32);
			BigInteger exponent = new BigInteger("10001", 16);

			KeyFactory kf = KeyFactory.getInstance("RSA");
		        RSAPublicKey pubKey = (RSAPublicKey) kf.generatePublic(new RSAPublicKeySpec(modulus, exponent));

			// Initialize a signature object with the public key
			Signature sigObj = Signature.getInstance("SHA1withRSA");
			sigObj.initVerify(pubKey);
          	
		     	File datFile = new File(dataPath);
			byte[] buffer = new byte[32*1024];
	        	FileInputStream fdat = new FileInputStream(datFile);
                        int n; // Number of bytes read
                        while (fdat.available() != 0) {
			    n = fdat.read(buffer);
			    sigObj.update(buffer, 0, n);
			};
		        fdat.close();
			result = sigObj.verify(sigFileBuffer);
                    } else {
		        System.out.println(getClass().getName() + ": Public Key for CERT_ID " + cert_id + " not found.");
		    }
	        }
	    } catch(Exception e) {
	        System.out.println("Exception occured while verifying signature: " + e.toString());
		e.printStackTrace();
	    } finally {
	        try {
        	    certFactory.commitTransaction();
		} catch (DBException de) {
                    de.printStackTrace();
                }
            }
	} catch (Throwable t) {
  	    t.printStackTrace();
        }
	return result;
    }  

}




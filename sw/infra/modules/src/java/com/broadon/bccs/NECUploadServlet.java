/*
 * NECUploadServlet.java
 *
 */

package com.broadon.bccs;

import java.io.*;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

import com.broadon.ms.common.AdminServlet;
import com.broadon.db.DBAccessFactory;
import com.broadon.db.DBException;
import com.broadon.db.DataLoadRequestFactory;
import com.broadon.online.UploadBean;

public class NECUploadServlet extends AdminServlet 
{
    private static final String CURRENT_LOCALE = "CURRENT_LOCALE";
    private static final String LABEL_PROP_KEY = "NEC_UPLOAD_PROP";
    private static final String JSP_LOC_KEY = "NEC_UPLOAD_JSP_LOC";

    private final static String NEC = "CLIENT_NEC";
    private final static String SIG_FAILED = "STATUS_SIG_FAILED";
    private final static String SUCCESS = "STATUS_SUCCESS";
    private final static String UNKNOWN = "STATUS_UNKNOWN";
    private final static String FORMAT_FAILED = "STATUS_FORMAT_FAILED";
    private final static String EXTRACT_FAILED = "STATUS_EXTRACT_FAILED";
    private final static String CONTENT_FAILED = "STATUS_CONTENT_FAILED";
    private final static String PROCESSED = "STATUS_PROCESSED";
    private final static String EXCEPTION = "EXCEPTION";

    private final static String SPOOL_PATH = "PATH_SPOOL";
    private final static String UPLOAD_PATH = "PATH_UPLOAD";
    private final static String PROCESS_SCRIPT = "PATH_PROCESS_SCRIPT";

    private final static String TAR_FILE = "NEC_TAR_FILE";
    private final static String SIG_FILE = "NEC_SIG_FILE";
    private final static String CERT_FILE = "NEC_CERT_FILE";
    private final static String XML_DIR = "NEC_XML_DIR";
    private final static String LOTS_FILE = "NEC_LOTS_FILE";
    private final static String LOTS_TABLE = "NEC_LOTS_TABLE";
    private final static String LOTS_COLUMN = "NEC_LOTS_COLUMN";
    private final static String CHIPS_FILE = "NEC_CHIPS_FILE";
    private final static String CHIPS_TABLE = "NEC_CHIPS_TABLE";
    private final static String CHIPS_COLUMN = "NEC_CHIPS_COLUMN";

    private final static String INSERT_IGNORE = "CMD_INSERT_IGNORE";
    private final static String INSERT_UPDATE = "CMD_INSERT_UPDATE";

    private static DataLoadRequestFactory        dlrFactory;
   
    static
    {
        try
        {
            dlrFactory = (DataLoadRequestFactory)DBAccessFactory.getInstance(
                                            DataLoadRequestFactory.class.getName());
        }
        catch (DBException de)
        {
            de.printStackTrace();
            System.exit(-1);
        }
    }
 
    public void init (ServletConfig config) 
    	throws ServletException
    {
	try {
	    super.init(config);

            if (mAdminProp!=null)
            	mLabelsFile = mAdminProp.getProperty(LABEL_PROP_KEY);

	} catch (ServletException e) {
            throw (e);
	}
    }

    public void doGet(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException 
    {
	HttpSession ses = req.getSession(false);
       	if (ses == null) {
  	    System.out.println(getClass().getName() + " No session");
            return;
        }

	if (!doAuthenticate("/serv?type=home&action=edit", req, res)) return;

        storeLabels(ses.getAttribute(CURRENT_LOCALE).toString(), mLabelsFile, ses);

	int role= Integer.parseInt(ses.getAttribute(SES_ROLE).toString());
        if (role==ROLE_NEC)
	    forwardToModule(JSP_LOC_KEY, "necUpload.jsp", req, res);
        else
            forwardToModule(JSP_LOC_KEY, "forbidden.jsp", req, res);

	return;
    }

    public void doPost (HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException 
    {
	HttpSession ses = req.getSession(false);
       	if (ses == null) {
	    System.out.println(getClass().getName() + " No session");
            return;
        }

	if (!doAuthenticate("/serv?type=home&action=edit", req, res)) return;
        	
        storeLabels(ses.getAttribute(CURRENT_LOCALE).toString(), mLabelsFile, ses);

	int role= Integer.parseInt(ses.getAttribute(SES_ROLE).toString());
        if (role==ROLE_NEC)
        {
	    UploadServlet up = new UploadServlet();
	    HashMap map = up.processUpload(req, res, ses.getAttribute(UPLOAD_PATH).toString());

            verifyUpload(map, ses);
 	    setResult(req, res, map, ses);
	    forwardToModule(JSP_LOC_KEY, "necResult.jsp", req, res);
	}
        else
            forwardToModule(JSP_LOC_KEY, "forbidden.jsp", req, res);

	return;
    }

    private void verifyUpload(HashMap map, HttpSession ses) 
    {
    	Iterator itr = map.values().iterator();
	VerifyUpload vu = new VerifyUpload();

	while (itr.hasNext()) { 
     	    UploadData data = (UploadData) itr.next();
            
	    if (data.name.startsWith("file") && data.isFile) {

		String dirName = data.value.substring(0, data.value.lastIndexOf("."));						
		String necPath = ses.getAttribute(UPLOAD_PATH).toString() + 
					ses.getAttribute(NEC).toString() + "/" + dirName + "/";
				
  		ServletContext sc=getServletContext();
                String processScript = sc.getRealPath("/") + "bin/" + ses.getAttribute(PROCESS_SCRIPT).toString();
		
		String fileExt = data.value.substring(data.value.lastIndexOf(".")+1);						
		String cmd = "sh " + processScript + " " + ses.getAttribute(UPLOAD_PATH).toString() + 
					ses.getAttribute(NEC).toString() + " " + dirName + " " + fileExt;
		try {
		    if (fileExt == null || !(fileExt.toLowerCase().equals("tar") || fileExt.toLowerCase().equals("bz2"))) {
			updateStatus(map, data.name, ses.getAttribute(FORMAT_FAILED).toString()); 
			Process r = Runtime.getRuntime().exec("rm -rf " + ses.getAttribute(UPLOAD_PATH).toString() + 
						ses.getAttribute(NEC).toString() + "/" + data.value);
			continue;
		    }

		    Process p = Runtime.getRuntime().exec(cmd);
		    p.waitFor();
		    if (p.exitValue() == 1)
			updateStatus(map, data.name, ses.getAttribute(EXTRACT_FAILED).toString()); 
		    else if (p.exitValue() == 2)
			updateStatus(map, data.name, ses.getAttribute(CONTENT_FAILED).toString()); 
		    else if (p.exitValue() == 0) {
			if (vu.NECSignature(necPath+ses.getAttribute(TAR_FILE).toString(), 
				necPath+ses.getAttribute(SIG_FILE).toString(), 
				necPath+ses.getAttribute(CERT_FILE).toString(), 
				necPath+ses.getAttribute(XML_DIR).toString()+"/"+ses.getAttribute(LOTS_FILE).toString(), 
				super.dataSource, super.nameMap)) {
			    updateStatus(map, data.name, generateProcessFile(dirName, fileExt,  
						necPath+ses.getAttribute(XML_DIR).toString(), ses));
			} else
			    updateStatus(map, data.name, ses.getAttribute(SIG_FAILED).toString()); 
                    }
		    else 
			updateStatus(map, data.name, ses.getAttribute(UNKNOWN).toString()); 

		} catch (IOException ie) {
		    System.out.println("IO Error : " + ie.toString());
		} catch (InterruptedException ie) { 
		    System.out.println("InterruptedError while write to file : " + ie.toString());
		} 
	    }
	}
    }

    String generateProcessFile(String dirName, String fileExt, String xmlPath, HttpSession ses) 
    {
	String fileName = new String(ses.getAttribute(SPOOL_PATH).toString()+"/"+dirName+".properties");

	File propFile = new File(fileName);

	if (propFile.exists()) {
   	    return ses.getAttribute(PROCESSED).toString();
	} else {
	    try {
		String reqID = createDataLoadRequest(dirName, fileName, fileExt, ses);

		FileWriter pfile = new FileWriter(fileName);
		PrintWriter pwrite = new PrintWriter(pfile);
				
		pwrite.println("RequestID="+reqID);
                pwrite.println("Counter=0");
                pwrite.println("User="+ses.getAttribute(SES_USER).toString());

		pwrite.println("File1="+xmlPath+"/"+ses.getAttribute(LOTS_FILE).toString());
		pwrite.println("Table1="+ses.getAttribute(LOTS_TABLE).toString());
		pwrite.println("Column1="+ses.getAttribute(LOTS_COLUMN).toString());			
		pwrite.println("Command1="+ses.getAttribute(INSERT_UPDATE).toString());			
		pwrite.println("File2="+xmlPath+"/"+ses.getAttribute(CHIPS_FILE).toString());
		pwrite.println("Table2="+ses.getAttribute(CHIPS_TABLE).toString());
		pwrite.println("Column2="+ses.getAttribute(CHIPS_COLUMN).toString());			
		pwrite.println("Command2="+ses.getAttribute(INSERT_UPDATE).toString());			

		pwrite.close();
		return ses.getAttribute(SUCCESS).toString();
	    } catch (IOException ie) {
		return ses.getAttribute(EXCEPTION).toString()+ie.toString();
	    }
	}
    }

    boolean updateStatus(HashMap map, String name, String status) 
    {
    	UploadData data=(UploadData) map.get(name);
    	if (data != null) {
  	    data.status = status;
    	    return true;
	}
    	return false;
    }

    String createDataLoadRequest(String jobName, String processFile, String fileExt, HttpSession ses) 
    {
	String reqID = null;

	try {
	    dlrFactory.beginTransaction(super.dataSource, false, false);

            try {
               	String xmlData = null;
		java.util.Date d = new java.util.Date();
        	long mSec = d.getTime();
        	reqID = String.valueOf(mSec);
        	String opID = ses.getAttribute(SES_USER_ID).toString();
        	String roleLevel = ses.getAttribute(SES_ROLE).toString();

	        xmlData = "<ROWSET><ROW num=\"1\">";
	        xmlData += "<REQUEST_ID>"+reqID+"</REQUEST_ID>";
	        xmlData += "<OPERATOR_ID>"+opID+"</OPERATOR_ID>";
	        xmlData += "<ROLE_LEVEL>"+roleLevel+"</ROLE_LEVEL>";
	        xmlData += "<JOB_NAME>"+jobName+"."+fileExt+"</JOB_NAME>";
	       	xmlData += "<PROCESS_STATUS>"+"Uploaded"+"</PROCESS_STATUS>";
	        xmlData += "</ROW></ROWSET>";
               	System.out.println(getClass().getName() + ": ADD DATA XML: " + xmlData);

               	try {
                    int result = dlrFactory.create(xmlData);
                }
                catch (DBException de)
                {
	            System.out.println(getClass().getName() + ": Data Load Request - NOT ADDED: " + xmlData);
	            System.out.println(getClass().getName() +  ": SQLException occurred - " + de.toString());
                }
            } finally {
               	try {
          	    dlrFactory.commitTransaction();
		} catch (DBException de) {
	            System.out.println(getClass().getName() +  ": SQLException occurred - " + de.toString());
                    de.printStackTrace();
                }
            }
        } catch (Throwable t) {
            System.out.println(getClass().getName() +  ": SQLException occurred - " + t.toString());
            t.printStackTrace();
        }
	return reqID;
    }

    void setResult(HttpServletRequest req, HttpServletResponse res,
			Map map, HttpSession ses) 
    {
    	Iterator itr = map.values().iterator();
	String result = "";
	String startBold = "";
	String endBold = "";

    	while (itr.hasNext()) {
  	    UploadData data = (UploadData) itr.next();
            if (data.value != null && !data.name.equals("client") && !data.name.equals("uploadservlet")) {
		if (data.status != null && data.status.equals(ses.getAttribute(SUCCESS).toString())) {
	  	    startBold = "";
		    endBold = "";
		} else {
		    startBold = "<FONT color=red><b>";
		    endBold = "</b></FONT>";
		}

        	result += "<TR><TD color=#333333 align=left width=50% nowrap=true>" + 
                          startBold + (data.value == null ? "" : data.value) + endBold + "</TD>" +
	                  "<TD color=#333333 align=left width=25% nowrap=true>" + 
                          startBold + (data.isFile ? String.valueOf(data.size) : "") + endBold + "</TD>" +
                          "<TD color=#333333 align=left width=25% nowrap=true>" + 
                          startBold + (data.status == null ? ses.getAttribute(UNKNOWN).toString() : data.status) + endBold + "</TD></TR>";
            }
	}
	UploadBean nec = new UploadBean(result);
	req.setAttribute(UploadBean.ID, nec);
        return;
    }
}


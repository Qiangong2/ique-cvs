import java.io.*;
import java.sql.*;
import oracle.sql.BLOB;

import com.broadon.security.X509;
import com.broadon.security.RSAcert;
import com.broadon.util.Database;
import com.broadon.util.Base64;

/**
 * Class for reading, updating, or creating HSM_CONFIGURATIONS records
 * in the database. 
 */
public class HSM_config
{
    static final String readRow =
	"SELECT LOCATION, CONFIG_FILE FROM HSM_CONFIGURATIONS " +
	"WHERE HSM_CONFIG_ID=? AND REVOKE_DATE IS NULL";

    static final String getID =
	"SELECT MAX(HSM_CONFIG_ID) + 1 FROM HSM_CONFIGURATIONS";

    static final String newRow =
	"INSERT INTO HSM_CONFIGURATIONS (HSM_CONFIG_ID, LOCATION, " +
	"CONFIG_FILE) VALUES (?, ?, ?)";

    static final String updateRow =
	"UPDATE HSM_CONFIGURATIONS SET LOCATION=?, CONFIG_FILE=?, " +
	"MODIFY_DATE=SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) " +
	"WHERE HSM_CONFIG_ID=?";

    static final String updateRow_noConfig =
	"UPDATE HSM_CONFIGURATIONS SET LOCATION=? WHERE HSM_CONFIG_ID=?";


    static final String kmdata = "kmdata.tgz";
	

    // database columns of HSM_CONFIGIRATION
    int Id;			// HSM ID
    String location;
    Blob configBlob;		// configuration data being read from database
    InputStream configFile;	// config. file to be sent to database

    boolean isUpdate;		// update existing row or insert new one
    boolean dataChanged = false;
    Connection conn;

    /**
     * Create a new row in the HSM_CONFIGURATIONS table, using the next
     * available HSM_CONFIG_ID.
     * @param conn Database connection.
     * @exception SQLException
     */
    public HSM_config(Connection conn)
	throws SQLException
    {
	this.conn = conn;

	// first, read the next available HSM_CONFIG_ID
	PreparedStatement ps = conn.prepareStatement(getID);
	ResultSet rs = ps.executeQuery();

	if (rs.next()) {
	    Id = rs.getInt(1);
	    if (rs.wasNull())
		Id = 1;
	} else
	    Id = 1;
	
	rs.close();
	ps.close();

	location = null;
	configBlob = null;
	configFile = null;

	isUpdate = false;	// indicate this is a new insertion
    }

    /**
     * Read an existing row in the HSM_CONFIGURATIONS table.
     * @param conn Database connection.
     * @param configID HSM_CONFIG_ID that specifies the row.
     * @exception SQLException
     */
    public HSM_config(Connection conn, int configID)
	throws SQLException
    {
	this.conn = conn;

	PreparedStatement ps = conn.prepareStatement(readRow);
	Id = configID;
	ps.setInt(1, Id);
	ResultSet rs = ps.executeQuery();

	if (rs.next()) {
	    location = rs.getString(1);
	    configBlob = rs.getBlob(2);
	    if (rs.wasNull())
		configBlob = null;
	    configFile = null;
	} else
	    throw new SQLException("Invalid HSM_CONFIG_ID " + configID);
	rs.close();
	ps.close();

	isUpdate = true;
    }


    Blob createConfigBlob()
	throws SQLException, IOException
    {
	if (configFile == null)
	    return null;
	
	BLOB blob = BLOB.createTemporary(conn, false, BLOB.DURATION_CALL);
	OutputStream out = blob.getBinaryOutputStream();
	byte[] buffer = new byte[16*1024];
	int n;
	while ((n = configFile.read(buffer)) > 0) {
	    out.write(buffer, 0, n);
	}
	out.close();
	return blob;
    } // createConfigBlob
    

    /**
     * Flush the current values in this object to the corresponding
     * row in the HSM_CONFIGURATIONS table.
     * @exception SQLExceptin
     * @exception IOException
     */
    public void sync()
	throws SQLException, IOException
    {
	if (! dataChanged)
	    return;

	PreparedStatement ps;
	
	if (isUpdate) {
	    Blob blob = createConfigBlob();
	    if (blob == null) {
		ps = conn.prepareStatement(updateRow_noConfig);
		ps.setInt(2, Id);
	    } else {
		ps = conn.prepareStatement(updateRow);
		ps.setBlob(2, blob);
		ps.setInt(3, Id);
	    }
	    ps.setString(1, location);
	} else {
	    ps = conn.prepareStatement(newRow);
	    ps.setInt(1, Id);
	    ps.setString(2, location);
	    Blob blob = createConfigBlob();
	    if (blob == null)
		ps.setNull(3, Types.BLOB);
	    else
		ps.setBlob(3, blob);
	}
	ps.executeUpdate();
	ps.close();
    } // sync


    /** @return the HSM_CONFIG_ID */
    int getID() {
	return Id;
    }

    /** @return the HSM location. */
    String getLocation() {
	return location;
    }

    /** @return the configuration file blob as an InputStream. */
    InputStream getConfigFile()
	throws SQLException
    {
	return configBlob.getBinaryStream();
    }

    /** @param l The new location of this HSM. */
    void setLocation(String l) {
	location = l;
	dataChanged = true;
    }

    /** @param file  The inputstream pointing to the new configuration
	data file. */
    void setConfigFile(InputStream file) {
	configFile = file;
	dataChanged = true;
    }



    static void usage() {
	System.err.println("Usage: HSM_config [-n|-r|-u] <options>\n\n" +
			   "-new: new entry\n" +
			   "-read: read entry\n" +
			   "-update: update entry\n\n" +
			   "-id <HSM_ID>\n" +
			   "-location <location>\n" +
			   "-dir <config_dir>");
    }

    static void generateKeyList(Connection conn, int hsmID, String configDir)
	throws SQLException, IOException
    {
	HSM_keys keylist = new HSM_keys(configDir, conn, hsmID);
	File dir = new File(configDir);
	String[] keynames = dir.list(keylist);
	for (int i = 0; i < keynames.length; ++i) {
	    String s = keynames[i];
	    keylist.addKey(s.substring(0, s.lastIndexOf("-key.pem")));
	}
	keylist.close();
    } // generateKeyList


    static void readKeyList(Connection conn, int hsmID, String configDir)
	throws IOException, SQLException
    {
	HSM_keys keylist = new HSM_keys(configDir, conn, hsmID);
	Base64 base64 = new Base64();
	ResultSet rs = keylist.readKeys();
	while (rs.next()) {
	    String keyname = rs.getString("KEY_NAME");
	    String cert = rs.getString("CERTIFICATE");
	    String key = rs.getString("PRIVATE_KEY");

	    File f = new File(configDir, keyname + "-key.pem");
	    FileOutputStream out = new FileOutputStream(f);
	    out.write(key.getBytes());
	    out.close();

	    if (cert.startsWith(X509.X509_HEADER)) {
		f = new File(configDir, keyname + "-cert.pem");
		out = new FileOutputStream(f);
		out.write(cert.getBytes());
		out.close();
	    } else {
		f = new File(configDir, keyname + "-cert.raw");
		out = new FileOutputStream(f);
		out.write(base64.decode(cert));
		out.close();
	    }
	}
	rs.close();
	keylist.close();
    } // readKeyList

    public static void main(String[] args)
    {
	Connection conn = null;

	try {
	    String location = null;
	    int hsmID = -1;
	    String configDir = null;
	    int newFlag = 0;
	    int updateFlag = 0;
	    int readFlag = 0;

	    for (int i = 0; i < args.length; ++i) {
		if (args[i].startsWith("-i") && ++i < args.length)
		    hsmID = Integer.parseInt(args[i]);
		else if (args[i].startsWith("-l") && ++i < args.length)
		    location = args[i];
		else if (args[i].startsWith("-d") && ++i < args.length)
		    configDir = args[i];
		else if (args[i].startsWith("-n"))
		    newFlag = 1;
		else if (args[i].startsWith("-r"))
		    readFlag = 1;
		else if (args[i].startsWith("-u"))
		    updateFlag = 1;
		else {
		    usage();
		    return;
		}
	    }

	    if (newFlag + updateFlag + readFlag != 1) {
		usage();
		return;
	    }

	    Database db = new Database();
	    conn = db.getConnection();
	    conn.setAutoCommit(false);

	    if (newFlag == 1) {
		if (hsmID != -1 || (location == null && configDir == null)) {
		    usage();
		    return;
		}
		HSM_config hsm = new HSM_config(conn);
		hsm.setLocation(location);
		if (configDir != null) {
		    File f = new File(configDir, kmdata);
		    hsm.setConfigFile(new FileInputStream(f));
		}
		hsm.sync();
		System.out.println("HSM config ID = " + hsm.getID());
		// check if we need to update key list
		if (configDir != null)
		    generateKeyList(conn, hsm.getID(), configDir);
		conn.commit();
	    } else if (updateFlag == 1) {
		if (hsmID == -1 ||
		    (location == null && configDir == null)) {
		    usage();
		    return;
		}
		HSM_config hsm = new HSM_config(conn, hsmID);
		if (location != null)
		    hsm.setLocation(location);
		if (configDir != null) {
		    File f = new File(configDir, kmdata);
		    hsm.setConfigFile(new FileInputStream(f));
		}
		hsm.sync();
		System.out.println("HSM config ID " + hsm.getID() + " updated");
		// check if we need to update key list
		if (configDir != null)
		    generateKeyList(conn, hsmID, configDir);
		conn.commit();
	    } else {
		// read only
		if (hsmID == -1) {
		    usage();
		    return;
		}
		HSM_config hsm = new HSM_config(conn, hsmID);
		System.out.println("HSM config ID = " + hsmID);
		if ((location = hsm.getLocation()) != null)
		    System.out.println("Location = " + location);
		if (configDir != null) {

		    // create directory if necessary
		    File dir = new File(configDir);
		    if (! dir.exists()) {
			if (! dir.mkdirs())
			    throw new IOException("Cannot create directory " +
						  configDir);
		    } else if (! dir.isDirectory())
			throw new IOException(configDir +
					      " is not a directory");
		    
		    // write the HSM ID file
		    FileOutputStream out = new FileOutputStream(configDir + "/HSM_ID");
		    out.write(Integer.toString(hsmID).getBytes());
		    out.close();

		    // write the kmdata file
		    InputStream in = hsm.getConfigFile();
		    if (in != null) {
			File f = new File(configDir, kmdata);
			out = new FileOutputStream(f);
			byte[] buffer = new byte[16 * 1024];
			int n;
			while ((n = in.read(buffer)) > 0)
			    out.write(buffer, 0, n);
			out.close();
		    }
		}

		// check if we need to download key list
		if (configDir != null)
		    readKeyList(conn, hsmID, configDir);
	    }


	} catch (Exception e) {
	    System.err.println(e.getMessage());
	    e.printStackTrace();
	    if (conn != null) {
		try {
		    conn.rollback();
		    conn.close();
		} catch (SQLException sql) {}
	    }
	}
    }
}

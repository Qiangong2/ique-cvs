import java.math.BigInteger;
import java.security.cert.*;
import java.security.interfaces.RSAPublicKey;
import java.sql.*;
import java.io.*;

import com.broadon.security.X509;
import com.broadon.security.RSAcert;
import com.broadon.util.Database;


/**
 * Read the certificate information from the database and compare
 * the fields with the input certificate.
 */
public class ReadCert
{
    static final String getCert =
	"SELECT CERT_ID, CN, OU, CERTIFICATE, PRIVATE_KEY, PUBLIC_KEY " +
        " FROM CERTIFICATES WHERE SERIAL_NO = ?";

    static final String getRootCert =
	"SELECT CERTIFICATE, CERT_ID FROM CERTIFICATES WHERE " +
	"CN LIKE 'Root%'";

    static String getField(String tag, String subject) {
	int idx_s = subject.indexOf(tag + "=");
	int idx_e = subject.indexOf(",", idx_s);
	if (idx_s >= 0) {
	    if (idx_e > 0)
		return subject.substring(idx_s+3, idx_e);
	    else
		return subject.substring(idx_s+3);
	}
	return null;
    }

    private static byte[] readCert(String certfile)
	throws IOException
    {
	File file = new File(certfile);
	byte[] certBuffer = new byte[(int) file.length()];
	FileInputStream in = new FileInputStream(file);
	if (in.read(certBuffer) != certBuffer.length)
	    throw new IOException("Error reading certificate file");
	in.close();
	return certBuffer;
    } // readCert
    

    static void usage() {
        System.err.println(
            "Usage: ReadCert\n"+
            "  -serial arg    - certificate serial number.\n"+
            "  -cert arg      - input certificate file for comparison.\n"+
            "  -key arg       - input key file for comparison.\n"+
            "  -verbose       - debug messages.\n");
    }

    public static void main(String[] args)
    {
	if (args.length < 1) {
	    usage();
	    return;
	}

	try {
	    int serial = 0;
            int i_serial = 0;
	    String certID, i_certID = null;
	    String CN, i_CN = null;
	    String OU, i_OU = null;
	    String encodedCert, i_encodedCert = null;
	    String publicKey, i_publicKey = null;
	    String i_publicKeySH = null;
            BigInteger publicKeyBI, i_publicKeyBI = null;
	    String privateKey = null;
	    String subject, i_subject = null;
	    String issuer, i_issuer = null;
            byte [] certBuffer = null;
            boolean isX509 = false;
            X509 x509 = null;
	    byte[] i_privateKey = null;
	    
            int serialFlag = 0;
            boolean verbose = false;
            String certFile = null;
            String keyFile = null;

            for (int i=0; i < args.length; i++) {
                if (args[i].startsWith("-s") && ++i < args.length) {
                    serial = Integer.parseInt(args[i]);
                    serialFlag = 1;
                }
                else if (args[i].startsWith("-c") && ++i < args.length)
                    certFile = args[i];
                else if (args[i].startsWith("-k") && ++i < args.length)
                    keyFile = args[i];
                else if (args[i].startsWith("-v"))
                    verbose = true;
                else {
                    usage();
                    return;    
                }
            }  // for

            if (serialFlag != 1) {
                usage();
                return;    
            }

	    // read the serial number
            if (serial < 0) {
	        System.err.println("Error: Invalid serial number");
	        usage();
                return;
            }

	    // read the cert
            if (certFile != null) {
                certBuffer = readCert(certFile);
                x509 = new X509();
                isX509 = new String(certBuffer).startsWith(X509.X509_HEADER);
            }

	    if (isX509) {

	        if (verbose) 
                    System.out.println("\nInput file is of X509 format!");

		// X509 certs
		X509Certificate cert =
		    x509.readEncodedX509(new ByteArrayInputStream(certBuffer));
		i_serial = cert.getSerialNumber().intValue();
		i_encodedCert = x509.encode(cert);
		i_certID = x509.genUniqueID(cert);
		i_publicKeyBI = ((RSAPublicKey) cert.getPublicKey()).getModulus();
		i_publicKey = ((RSAPublicKey) cert.getPublicKey()).getModulus().toString(32);
		i_publicKeySH = ((RSAPublicKey) cert.getPublicKey()).getModulus().toString(16);

		int exponent = ((RSAPublicKey) cert.getPublicKey()).getPublicExponent().intValue();
		if (exponent != 0x10001) {
		    System.err.println("RSA cert with public exponent " +
				       exponent + " not supported");
		    return;
		}
		// we need to parse the Subject DN field to extract the CN
		// (common name) and OU (organization unit).
		i_subject = cert.getSubjectDN().toString();
		i_CN = getField("CN", i_subject);
		i_OU = getField("OU", i_subject);
		i_issuer = cert.getIssuerDN().toString();
	    } else if (certFile != null) {

	        if (verbose) 
                    System.out.println("\nInput file is of RSA Raw format!");

		// BroadOn RSA cert
		RSAcert cert = new RSAcert(certBuffer);
		i_serial = (int) cert.serial;
		i_encodedCert = cert.encodedCert;
		i_certID = cert.uniqueID;
		i_publicKeyBI = cert.pubkey.getModulus();
		i_publicKey = cert.pubkey.getModulus().toString(32);
		i_publicKeySH = cert.pubkey.getModulus().toString(16);
		i_subject = cert.issuer + '-' + cert.subject;
		i_issuer = cert.issuer;
		i_CN = cert.subject;
		i_OU = cert.subject.substring(0, 2);
	    }

	    // Read the optional private key (or key handle)
	    if (keyFile != null) {
		File file = new File(keyFile);
		if (file.exists()) {
		    i_privateKey = new byte[(int) file.length()];
		    FileInputStream in = new FileInputStream(file);
		    int n;
		    int count = 0;
		    while (count < i_privateKey.length &&
			   ((n = in.read(i_privateKey, count,
					 i_privateKey.length - count)) > 0)) {
			count += n;
		    }
		    in.close();
		}
	    }

            // read the certificate information from the database
	    Database db = new Database();
	    Connection conn = db.getConnection();
	    conn.setAutoCommit(false);

	    PreparedStatement pstmt = conn.prepareStatement(getCert);
	    pstmt.setInt(1, serial);
	    ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                certID = rs.getString(1);
                CN     = rs.getString(2);
                OU     = rs.getString(3);
                encodedCert = rs.getString(4);
                privateKey = rs.getString(5);
                publicKey = rs.getString(6);

                publicKeyBI = new BigInteger(publicKey, 32);
                String publicKeySH = publicKeyBI.toString(16);

                if (verbose) {
	            System.out.println("\n-------------------------------------");
	            System.out.println("\n>> Serial Number = " + serial +
                      "\n>> CertID = " + certID + 
                      "\n>> CN = " + CN +
                      "\n>> OU = " + OU + 
                      "\n>> CERTIFICATE:\n" + encodedCert +
                      "\n>> PRIVATE KEY:\n" + privateKey + 
                      "\n>> PUBLIC KEY:\n" + publicKey +
                      "\n>> PUBLIC KEY (BigInt):\n" + publicKeyBI + 
                      "\n>> PUBLIC KEY (String Hex):\n" + publicKeySH + "\n");
                }

                /* compare the database certificate information with that 
                   from input cert file */
                if (certBuffer != null) {
                    int errorCount = 0;
	            if (verbose)
                        System.out.println("\n>> Comparing cert data...");
                    if (certID.compareTo(i_certID) != 0) {
	                System.out.println("----> Cert ID = ERROR!");
	                System.out.println("----> Input Cert ID = " + i_certID);
	                System.out.println("----> DB Cert ID    = " + certID);
                        errorCount++;
                    }
                    if (CN.compareTo(i_CN) != 0) { 
	                System.out.println("----> CN = ERROR!");
	                System.out.println("----> Input CN = " + i_CN);
	                System.out.println("----> DB CN    = " + CN);
                        errorCount++;
                    }
                    if (OU.compareTo(i_OU) != 0) { 
	                System.out.println("----> OU = ERROR!");
	                System.out.println("----> Input OU = " + i_OU);
	                System.out.println("----> DB OU    = " + OU);
                        errorCount++;
                    }
                    if (encodedCert.compareTo(i_encodedCert) != 0) { 
	                System.out.println("----> EncodedCert = ERROR!");
	                System.out.println("----> Input Encoded Cert =\n" + 
                            i_encodedCert);
	                System.out.println("----> DB Encoded Cert =\n" + 
                            encodedCert);
                        errorCount++;
                    }
                    if (publicKey.compareTo(i_publicKey) != 0) {
	                System.out.println("----> Public Key = ERROR!");
	                System.out.println("----> Input Public Key =\n" + 
                            i_publicKey);
	                System.out.println("----> DB Public Key =\n" + 
                            publicKey);
                        errorCount++;
                    }
                    if (publicKeyBI.compareTo(i_publicKeyBI) != 0) {
	                System.out.println("----> Public Key (BI) = ERROR!");
	                System.out.println("----> Input Public Key (BI) =\n" + 
                            i_publicKeyBI);
	                System.out.println("----> DB Public Key (BI) =\n" + 
                            publicKeyBI);
                        errorCount++;
                    }
                    if (publicKeySH.compareTo(i_publicKeySH) != 0) {
	                System.out.println("----> Public Key (SH) = ERROR!");
	                System.out.println("----> Input Public Key (SH) =\n" + 
                            i_publicKeySH);
	                System.out.println("----> DB Public Key (SH) =\n" + 
                            publicKeySH);
                        errorCount++;
                    }

                    if (i_privateKey != null) {
                        String i_privateKeyS = new String(i_privateKey);
	                if ((i_privateKeyS == null) || (privateKey == null)) {
		            System.err.println(
                                "----> ERROR: CANNOT allocate private key string!");
                            System.out.println("Input Private Key =\n" +
                                i_privateKey.toString());
                        }
                        if (privateKey.compareTo(i_privateKeyS) != 0)  {
	                    System.out.println("Private Key = ERROR!");
	                    System.out.println("Input Private Key =\n" +
                                i_privateKeyS);
	                    System.out.println("DB Private Key =\n" +
                                privateKey);
                            errorCount++;
                        }
                    }

                    if (errorCount == 0)
	                System.out.println(">> DB Certificate = OK");
                }
            
            }
            else {
		System.err.println("No row found!");
            }
                

	    conn.close();
	    
	} catch (Exception e) {
	    e.printStackTrace();
	}

    }
}

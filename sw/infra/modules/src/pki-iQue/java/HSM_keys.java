import java.io.*;
import java.sql.*;
import java.security.cert.X509Certificate;
import java.security.GeneralSecurityException;

import com.broadon.security.X509;
import com.broadon.security.RSAcert;

/**
 * Create keys info corresponding to a particular HSM configuration.
 */
public class HSM_keys implements FilenameFilter
{
    static final String newKey =
	"INSERT INTO HSM_CONFIG_KEYS (HSM_CONFIG_ID, KEY_NAME, CHAIN_ID) " +
	"VALUES (?, ?, ?)";
    
    static final String getChain =
	"SELECT CHAIN_ID FROM CERTIFICATE_CHAINS WHERE SIGNER_CERT_ID=?";

    static final String readKeys =
	"SELECT KEY_NAME, CERTIFICATE, PRIVATE_KEY FROM " +
	"HSM_CONFIG_KEYS A, CERTIFICATE_CHAINS B, CERTIFICATES C WHERE " +
	"A.HSM_CONFIG_ID=? AND A.CHAIN_ID = B.CHAIN_ID AND " +
	"B.SIGNER_CERT_ID = C.CERT_ID";

    Connection conn;
    PreparedStatement psAddKey = null;
    PreparedStatement psGetChain = null;

    String hsmDir;
    int hsmID;

    /**
     * Create an <code>HSM_keys</code> object for a specified HSM.
     * @param dir Directory path name where the HSM configuration
     * files (certificates, keys, etc.) are stored.
     * @param conn Database connection.
     * @param HSM_id ID of the corresponding HSM.
     * @exception SQLException
     */
    public HSM_keys(String dir, Connection conn, int HSM_id)
	throws SQLException
    {
	hsmDir = dir;
	hsmID = HSM_id;
	this.conn = conn;
	psAddKey = conn.prepareStatement(newKey);
	psGetChain = conn.prepareStatement(getChain);
    }

    
    /**
     * Release all database resources associated with this object.
     * @exception SQLException
     */
    public void close() throws SQLException
    {
	if (psAddKey != null) {
	    psAddKey.close();
	    psAddKey = null;
	}
	if (psGetChain != null) {
	    psGetChain.close();
	    psGetChain = null;
	}
    
    } // close

    
    // Given the key name, the corresponding cert file will be
    // keyname-cert.pem or keyname-cert.raw.
    File getCertFile(String keyname)
	throws IOException
    {
	File f = new File(hsmDir, keyname + "-cert.pem");
	if (f.canRead())
	    return f;
	f = new File(hsmDir, keyname + "-cert.raw");
	if (f.canRead())
	    return f;
	throw new IOException("Cannot locate certificate file for " + keyname);
    } // getCertFile


    // Generate unique cert ID for this certificate file.  Handles
    // both X.509 and BroadOn RSA certs.
    String getCertID(File certFile)
	throws IOException, GeneralSecurityException
    {
	FileInputStream in = new FileInputStream(certFile);
	byte[] certBuffer = new byte[(int) certFile.length()];
	if (in.read(certBuffer) != certBuffer.length)
	    throw new IOException("Error reading certificate file: " +
				  certFile.getPath());
	in.close();

	if (new String(certBuffer).startsWith(X509.X509_HEADER)) {
	    // X509 cert
	    X509 x509 = new X509();
	    return x509.genUniqueID(x509.readEncodedX509(new ByteArrayInputStream(certBuffer)));
	} else {
	    // BroadOn RSA cert
	    RSAcert cert = new RSAcert(certBuffer);
	    return cert.uniqueID;
	}
    } // getCertId


    // Given the key name, find out the corresonding chain ID.
    int getChainID(String keyname)
	throws IOException, SQLException, GeneralSecurityException
    {
	File certFile = getCertFile(keyname);
	String certID = getCertID(certFile);

	psGetChain.setString(1, certID);
	ResultSet rs = psGetChain.executeQuery();
	if (rs.next()) {
	    int chain = rs.getInt(1);
	    rs.close();
	    return chain;
	}
	throw new SQLException("Certificate has not been loaded to database");
    } // getChainID


    /**
     * Add the specified key to the HSM key list.
     * @param keyname  Name of the HSM-protected key.
     * @exception SQLException Any error.
     */
    public void addKey(String keyname)
	throws SQLException
    {
	try {
	    // look up chain id
	    int chainID = getChainID(keyname);
	    
	    psAddKey.setInt(1, hsmID);
	    psAddKey.setString(2, keyname);
	    psAddKey.setInt(3, chainID);
	    psAddKey.executeUpdate();
	} catch (IOException e) {
	    throw new SQLException(e.getMessage());
	} catch (GeneralSecurityException e) {
	    throw new SQLException(e.getMessage());
	}
    }

    public boolean accept(File dir, String name) {
	return name.endsWith("-key.pem");
    }


    /**
     * Read all info correspondings to keys stored in this HSM.
     * @exception SQLException
     */
    public ResultSet readKeys()
	throws SQLException
    {
	PreparedStatement ps = conn.prepareStatement(readKeys);
	ps.setInt(1, hsmID);
	return ps.executeQuery();
    }
}

#!/bin/sh

JAVA_HOME=/opt/broadon/pkgs/jre
export JAVA_HOME

$JAVA_HOME/bin/java -classpath $HOME/lib/ops.jar:$HOME/lib/common.jar \
    -DDB.properties=$HOME/conf/DB.properties LoadCert $*

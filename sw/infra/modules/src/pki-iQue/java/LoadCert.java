import java.math.BigInteger;
import java.security.cert.*;
import java.security.interfaces.RSAPublicKey;
import java.sql.*;
import java.io.*;

import com.broadon.security.X509;
import com.broadon.security.RSAcert;
import com.broadon.util.Database;


/**
 * Load a X509 certificate into the database.
 */
public class LoadCert
{
    static final String addCert =
	"INSERT INTO CERTIFICATES (CERT_ID, CN, OU, SERIAL_NO, " +
	"CERTIFICATE, PRIVATE_KEY, PUBLIC_KEY) VALUES (?, ?, ?, ?, ?, ?, ?)";

    static final String getCertChain =
	"SELECT CERTIFICATE, CERT_ID FROM CERTIFICATES WHERE CN = ?";

    static final String getRootCert =
	"SELECT CERTIFICATE, CERT_ID FROM CERTIFICATES WHERE " +
	"CN LIKE 'Root%'";

    static final String getMaxChainID =
	"SELECT MAX(CHAIN_ID) FROM CERTIFICATE_CHAINS";

    static final String insertChain =
	"INSERT INTO CERTIFICATE_CHAINS (CHAIN_ID, SIGNER_CERT_ID, " +
	"CA_CERT_ID, DESCRIPTION) VALUES (?, ?, ?, ?)";

    static String getField(String tag, String subject) {
	int idx_s = subject.indexOf(tag + "=");
	int idx_e = subject.indexOf(",", idx_s);
	if (idx_s >= 0) {
	    if (idx_e > 0)
		return subject.substring(idx_s+3, idx_e);
	    else
		return subject.substring(idx_s+3);
	}
	return null;
    }

    static String getChain(String issuer, boolean isX509, Connection conn,
			   X509 x509)
    {
	String CN;

	if (isX509)
	    CN = getField("CN", issuer);
	else {
	    int idx = issuer.lastIndexOf('-');
	    CN = (idx == -1) ? issuer : issuer.substring(idx + 1);
	}

	try {
	    boolean needRoot = false;
	    ResultSet rs;
	    if (! isX509 && issuer.equals("Root")) {
		// special case where a BroadOn cert is signed by X.509 Root
		needRoot = true;
		PreparedStatement pstmt = conn.prepareStatement(getRootCert);
		rs = pstmt.executeQuery();
	    } else {
		PreparedStatement pstmt = conn.prepareStatement(getCertChain);
		pstmt.setString(1, CN);
		rs = pstmt.executeQuery();
	    }
	    
	    while (rs.next()) {
		String subject;
		try {
		    String certString = rs.getString(1);
		    if (certString.startsWith(X509.X509_HEADER)) {
			X509Certificate cert =
			    x509.readEncodedX509(rs.getString(1));
			subject = cert.getSubjectDN().toString();
			if (needRoot &&
			    subject.equals(cert.getIssuerDN().toString()))
			    return rs.getString(2);
		    } else {
			RSAcert cert = new RSAcert(certString);
			subject = cert.issuer + '-' + cert.subject;
		    }
		} catch (CertificateException e) {
		    continue;
		}

		if (issuer.equals(subject)) {
		    return rs.getString(2);
		}
	    }

	} catch (SQLException e) {
	    // ignored, just return null
	}
	return null;
    } // getChain


    static boolean insertChain(Connection conn, String signer, String ca,
			       String subject)
	throws SQLException
    {
	try {
	    Statement stmt = conn.createStatement();
	    ResultSet rs = stmt.executeQuery(getMaxChainID);
	    int chain_id;
	    if (rs.next())
		chain_id = rs.getInt(1) + 1;
	    else
		chain_id = 1;
	    rs.close();
	    
	    PreparedStatement ps = conn.prepareStatement(insertChain);
	    ps.setInt(1, chain_id);
	    ps.setString(2, signer);
	    ps.setString(3, ca);
	    ps.setString(4, subject);
	    ps.executeUpdate();
	    return true;
	} catch (SQLException e) {
	    e.printStackTrace();
	    conn.rollback();
	    return false;
	}
    }


    private static byte[] readCert(String certfile)
	throws IOException
    {
	File file = new File(certfile);
	byte[] certBuffer = new byte[(int) file.length()];
	FileInputStream in = new FileInputStream(file);
	if (in.read(certBuffer) != certBuffer.length)
	    throw new IOException("Error reading certificate file");
	in.close();
	return certBuffer;
    } // readCert
    

    public static void main(String[] args)
    {
	if (args.length < 1) {
	    System.err.println("Usage: LoadCert certfile [keyfile]");
	    return;
	}

	try {
	    String certID;
	    String CN;
	    String OU;
	    int serial;
	    String encodedCert;
	    String publicKey;
	    String subject;
	    String issuer;
	    
	    // read the cert
	    byte[] certBuffer = readCert(args[0]);
	    X509 x509 = new X509();
	    boolean isX509 = new String(certBuffer).startsWith(X509.X509_HEADER);
	    if (isX509) {
		// X509 certs
		X509Certificate cert =
		    x509.readEncodedX509(new ByteArrayInputStream(certBuffer));
		serial = cert.getSerialNumber().intValue();
		encodedCert = x509.encode(cert);
		certID = x509.genUniqueID(cert);
		publicKey = ((RSAPublicKey) cert.getPublicKey()).getModulus().toString(32);

		int exponent = ((RSAPublicKey) cert.getPublicKey()).getPublicExponent().intValue();
		if (exponent != 0x10001) {
		    System.err.println("RSA cert with public exponent " +
				       exponent + " not supported");
		    return;
		}
		// we need to parse the Subject DN field to extract the CN
		// (common name) and OU (organization unit).
		subject = cert.getSubjectDN().toString();
		CN = getField("CN", subject);
		OU = getField("OU", subject);
		issuer = cert.getIssuerDN().toString();
	    } else {
		// BroadOn RSA cert
		RSAcert cert = new RSAcert(certBuffer);
		serial = (int) cert.serial;
		encodedCert = cert.encodedCert;
		certID = cert.uniqueID;
		publicKey = cert.pubkey.getModulus().toString(32);
		subject = cert.issuer + '-' + cert.subject;
		issuer = cert.issuer;
		CN = cert.subject;
		OU = cert.subject.substring(0, 2);
	    }

	    // Read the optional private key (or key handle)
	    byte[] privateKey = null;
	    if (args.length > 1) {
		File file = new File(args[1]);
		if (file.exists()) {
		    privateKey = new byte[(int) file.length()];
		    FileInputStream in = new FileInputStream(file);
		    int n;
		    int count = 0;
		    while (count < privateKey.length &&
			   ((n = in.read(privateKey, count,
					 privateKey.length - count)) > 0)) {
			count += n;
		    }
		    in.close();
		}
	    }

	    
	    Database db = new Database();
	    Connection conn = db.getConnection();
	    conn.setAutoCommit(false);

	    // write it to the database
	    PreparedStatement pstmt = conn.prepareStatement(addCert);
	    pstmt.setString(1, certID);
	    pstmt.setString(2, CN);
	    pstmt.setString(3, OU);
	    pstmt.setInt(4, serial);
	    pstmt.setString(5, encodedCert);
	    if (privateKey == null)
		pstmt.setNull(6, Types.VARCHAR);
	    else
		pstmt.setString(6, new String(privateKey));
	    
	    pstmt.setString(7, publicKey);

	    pstmt.executeUpdate();

	    
	    if (! subject.equals(issuer)) {
		// not a root cert, check for CA chain
		String issuerCertID = getChain(issuer, isX509, conn, x509);
		if (issuerCertID == null) {
		    conn.rollback();
		    System.err.println("Cannot locate issuer: cert not loaded");
		    return;
		}
		
		if (! insertChain(conn, certID, issuerCertID, subject)) {
		    System.err.println("Cannot insert cert chain");
		    return;
		}
	    }

	    conn.commit();
	    conn.close();

	    System.out.println(CN + ", " + OU + " added successfully: ID = " +
			       certID);
	    
	} catch (Exception e) {
	    e.printStackTrace();
	}

    }
}

#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <getopt.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <values.h>
#include <netinet/in.h>

#include <openssl/ssl.h>

extern "C" {
#include <nfast/nfkm.h>
};
#include "bignum.h"
#include "hsm.h"
#include "rsa_keys.h"

#include "BBcert.h"


static bool
read_RSA_public_key(const char* keyfile, BB_CERT& cert)
{
    if (cert.cert_type != BB_CERT_TYPE_SERVER)
	return false;

    RSA_KEYS key(keyfile);

    if (sizeof(cert.pubkey) != key.pub_key_size())
	return false;

    key.get_pub_key((unsigned char*) cert.pubkey, sizeof(cert.pubkey));
    for (int i = 0; i < sizeof(cert.pubkey)/sizeof(u32); ++i)
	cert.pubkey[i] = ntohl(cert.pubkey[i]);
    cert.exponent = key.get_exponent();
    if (cert.exponent == 0xffffffff)
	return false;
    return true;

} // read_RSA_public_key


static HSM*
prepare_hsm(const char* keyfile)
{
    RSA_KEYS key(keyfile);

    const char* ident = key.get_key_ident();
    const char* appname = key.get_key_appname();

    if (ident == NULL || appname == NULL)
	return NULL;

    return new HSM(ident, appname);
} // prepare_hsm


// extract the subject name from the given cert
static bool
get_issuer(const char* certfile, BB_CERT& cert)
{
    int fd = open(certfile, O_RDONLY);
    if (fd < 0)
	return false;
    unsigned char buffer[BB_CERT::RSA_CERT_SIZE];
    if (read(fd, buffer, BB_CERT::RSA_CERT_SIZE) != BB_CERT::RSA_CERT_SIZE) {
	close(fd);
	return false;
    }
    close(fd);

    BB_CERT issuer_cert(buffer);

    sprintf((char*) cert.issuer, "%s-%s", issuer_cert.issuer,
	    issuer_cert.subject);
    return true;
} // get_issuer


static unsigned int
get_serial(const char* serialfile)
{
    char buf[80];
    FILE* fp = fopen(serialfile, "r+");
    if (fp == NULL)
	return LONG_MAX;
    fgets(buf, sizeof(buf), fp);
    long serial = strtol(buf, 0, 16);
    if (serial == LONG_MAX) {
	fclose(fp);
	return LONG_MAX;
    }
    serial;
    if (fseek(fp, 0, SEEK_SET) < 0) {
	fclose(fp);
	return LONG_MAX;
    }
    sprintf(buf, "%lx\n", serial + 1);
    if (strlen(buf) % 2 == 0)
	fputc('0', fp);
    fputs(buf, fp);
    fclose(fp);
    return serial;
    
} // get_serial


static struct option long_options[] = {
    {"key", 1, 0, 'k'},			// private key file of this cert
    {"prefix", 1, 0, 't'},		// cert type (XS, MS, etc.)
    {"CA", 1, 0, 'c'},			// issuer cert
    {"issuer", 1, 0, 'i'},		// issuer name (overrides "CA")
    {"CAkey", 1, 0, 'K'},		// issuer private key
    {"CAserial", 1, 0, 's'},		// serial number file
    {"out", 1, 0, 'o'},			// filename for this cert
    {"days", 1, 0, 'd'},		// days till expire
    {"help", 0, 0, 'h'},		// print usage 
    {0, 0, 0, 0}
};

static const char* cert_file = 0;
static const char* subject_prefix = 0;

static void
usage(const char* progname)
{
    fprintf(stderr, "usage: %s\n", progname);
    fprintf(stderr,
            " -key arg        - private key file of this cert.\n"
	    " -prefix arg     - type of cert (XS, MS, etc.).\n"
            " -CA arg         - cert. file of issuer.\n"
            " -issuer arg     - name of issuer: override -CA if present.\n"
            " -CAkey arg      - private key file of issuer.\n"
            " -CAserial arg   - serial file.\n"
            " -out arg        - file name to output the cert.\n"
            " -days arg       - number of days till this cert expires.\n"
            " -help           - print this message.\n");
}

int
main(int argc, char* argv[])
{
    BB_CERT cert;
    bool k_flag = false;
    bool i_flag = false;
    bool K_flag = false;
    bool s_flag = false;
    HSM* signer = 0;

    cert.cert_type = BB_CERT_TYPE_SERVER;

    while (1) {
	int ch = getopt_long_only(argc, argv, "", long_options, 0);

	if (ch == -1)
	    break;

	switch (ch) {
	case 'k':
	    if (! read_RSA_public_key(optarg, cert)) {
		fprintf(stderr, "Error reading public key from %s\n", optarg);
		return 1;
	    }
	    k_flag = true;
	    break;

	case 't':
	    subject_prefix = optarg;
	    break;

	case 'c':
	    cert.sig_type = BB_SIG_TYPE_RSA2048;
	    if (! get_issuer(optarg, cert)) {
		fprintf(stderr, "Error reading the CA cert.\n");
		return 1;
	    }
	    i_flag = true;
	    break;

	case 'i':
	    cert.sig_type = BB_SIG_TYPE_RSA4096;
	    strcpy((char*) cert.issuer, optarg);
	    i_flag = true;
	    break;

	case 'K':
	    signer = prepare_hsm(optarg);
	    if (signer == 0 || ! signer->operational()) {
		fprintf(stderr, "Error setting up HSM\n");
		return 1;
	    }
	    K_flag = true;
	    break;

	case 's':
	    cert.serial = get_serial(optarg);
	    if (cert.serial == LONG_MAX) {
		fprintf(stderr, "Error reading serial number file\n");
		return 1;
	    }
	    s_flag = true;
	    break;

	case 'o':
	    cert_file = optarg;
	    break;

	case 'd':
	    cert.date = time(0) + atol(optarg) * 24 * 3600 * 60;
	    break;

	case 'h':
	default:
	    usage(argv[0]);
	    return 1;
	}
    }
    
    if (k_flag == false || i_flag == false || K_flag == false ||
	s_flag == false || subject_prefix == 0 || cert_file == 0) {
	usage(argv[0]);
	return 1;
    }

    sprintf((char*) cert.subject, "%s%08x", subject_prefix, cert.serial);

    BB_CERT::RSA_CERT_RAW new_cert;

    if (! cert.create_cert_body(new_cert)) {
	fprintf(stderr, "Cannot create new cert\n");
	return 1;
    }
    const NFast_Bignum* sig = signer->sign(new_cert, BB_CERT::RSA_SIG_OFFSET);
    memcpy(((unsigned char*) new_cert) + BB_CERT::RSA_SIG_OFFSET,
	   sig->value, sig->nbytes);
    delete sig;

    // write the cert file
    int fd = creat(cert_file, S_IRWXU);
    if (fd < 0) {
	perror("Cannot create output file");
	return 1;
    }
    if (write(fd, new_cert, sizeof(new_cert)) != sizeof(new_cert)) {
	perror("Error writing output file");
	return 1;
    }
    close(fd);
    return 0;
    
} // main 

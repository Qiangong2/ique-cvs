#include <string.h>

#include "BBcert.h"

// read big endian word from buffer
static inline int
get_word(const unsigned char* buf)
{
    return (buf[0] << 24) | (buf[1] << 16) | (buf[2] << 8) | buf[3];
} // get_word


BB_CERT::BB_CERT(const unsigned char* cert_buf)
{
    cert_type = get_word(cert_buf + CERTTYPE_OFFSET);
    sig_type = get_word(cert_buf + SIGTYPE_OFFSET);
    date = get_word(cert_buf + DATE_OFFSET);
    memcpy(issuer, cert_buf + ISSUER_OFFSET, ISSUER_SIZE);
    memcpy(subject, cert_buf + SUBJECT_OFFSET, SUBJECT_SIZE);

    for (int i = 0; i < RSA_KEY_SIZE/sizeof(u32); ++i) {
	pubkey[i] = get_word(cert_buf + RSA_KEY_OFFSET + i * sizeof(u32));
    }

    exponent = get_word(cert_buf + RSA_EXP_OFFSET);
} 

#ifndef __BBCERT_H__
#define __BBCERT_H__

#include <libcrypto/bbtoolsapi.h>

struct BB_CERT
{
    static const int CERTTYPE_OFFSET = 0; // cert type
    static const int CERTTYPE_SIZE = 4;

    static const int SIGTYPE_OFFSET = CERTTYPE_SIZE; // signature type
    static const int SIGTYPE_SIZE = 4;

    static const int DATE_OFFSET = SIGTYPE_OFFSET + SIGTYPE_SIZE;
    static const int DATE_SIZE = 4;     // expiration date (sec. since Epoch)
    
    static const int ISSUER_OFFSET = DATE_OFFSET + DATE_SIZE;
    static const int ISSUER_SIZE = 64;  // Issuer name

    static const int SUBJECT_OFFSET = ISSUER_OFFSET + ISSUER_SIZE; 
    static const int SUBJECT_SIZE = 64; // subject name

    static const int RSA_KEY_OFFSET = SUBJECT_OFFSET + SUBJECT_SIZE;
    static const int RSA_KEY_SIZE = 256; // Public Key

    static const int RSA_EXP_OFFSET = RSA_KEY_OFFSET + RSA_KEY_SIZE;
    static const int RSA_EXP_SIZE = 4;  // exponent of RSA public key

    static const int RSA_SIG_OFFSET = RSA_EXP_OFFSET + RSA_EXP_SIZE;
    static const int RSA_CA_SIG_SIZE = 512; // CA sig is 4096 bit
    static const int RSA_SIG_SIZE = 256; // other sig is 2058 bit

    static const int RSA_PAD_OFFSET = RSA_SIG_OFFSET + RSA_SIG_SIZE;
    static const int RSA_PAD_SIZE = 256;

    static const int RSA_CERT_SIZE = RSA_PAD_OFFSET + RSA_PAD_SIZE;

    typedef u32 RSA_CERT_RAW[RSA_CERT_SIZE/sizeof(u32)];

    u32 cert_type;			// certificate type
    u32 sig_type;			// signature type
    u32 date;				// Unix time(2)
    unsigned int serial;

    BbServerName issuer;		// issuer name
    BbServerSuffix subject;		// subject name

    u32 pubkey[RSA_KEY_SIZE/sizeof(u32)]; // RSA public key modulus
    u32 exponent;			// RSA public key exponent

    BB_CERT(const unsigned char* cert_buf);

    BB_CERT() {
	memset(this, 0, sizeof(BB_CERT));
    }

    bool create_cert_body(RSA_CERT_RAW& rsacert) {
	return (generateUnsignedRSACert(cert_type, sig_type, date,
					subject, issuer, pubkey, exponent,
					rsacert,
					sizeof(rsacert)/sizeof(u32)) == 0);
    }
};

#endif // __BBCERT_H__

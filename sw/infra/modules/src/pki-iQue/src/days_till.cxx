#define _XOPEN_SOURCE
#include <stdio.h>

#ifndef __USE_MISC
#define __USE_MISC
#endif
#include <time.h>


static time_t
parseDate(const char* time_str)
{
    tm encoded_time;
    strptime(time_str, "%b %d %Y", &encoded_time);
    encoded_time.tm_sec = encoded_time.tm_min = encoded_time.tm_hour = 0;
    return timegm(&encoded_time);
    
} // parseDate


// Usage: a.out < timefile
// Usage: a.out timefile
// output to stdout the number of days from now till the time specified in
// "timefile", which should contain one line of the form
// "Feb 14 22:48:27 2018 GMT"

int
main(int argc, char* argv[])
{
    FILE* in = 0;
    if (argc > 1)
	in = fopen(argv[1], "r");
    else
	in = stdin;

    char buf[80];

    if (in == NULL || fgets(buf, 80, in) == NULL) {
	fprintf(stderr, "Usage: %s timefile\n", argv[0]);
	return 1;
    }
    fclose(in);
    time_t next = parseDate(buf);
    time_t now = time(0);

    if (next < now) {
	fprintf(stderr, "Invalid time format: %s\n", buf);
	return 1;
    }

    int seconds = 3600 * 24;
    printf("%ld\n", (next - now + seconds - 1) / seconds);
    return 0;
}

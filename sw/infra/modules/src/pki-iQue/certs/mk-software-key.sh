#!/bin/sh

. $HOME/lib/functions

need_one_dir $*

SERIAL=`cat $HOME/conf/serial`
PREFIX="Software Signer"

COMMON_NAME=$PREFIX:$SERIAL
export COMMON_NAME

generate_key software 2048

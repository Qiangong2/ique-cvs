#!/bin/sh

need_one_dir ()
{
    hsm_dir=$1
    if [ "$hsm_dir" = "" ]; then
	echo "Usage: $0 HSM_dir"
	exit 1
    fi
}

need_two_dirs ()
{
    hsm_dir=$1
    signer_dir=$2

    if [ "$hsm_dir" = "" -o "$signer_dir" = "" ]; then
	echo "Usage $0 HSM_dir Signer_dir"
	exit 1
    fi
}

generate_key ()
{
    keyname=$1
    size=$2

    keyid=\`"$keyname"\'
    old_key=`nfkminfo -l | grep "$keyid" | cut -f2 -d' '`

    if [ "$old_key" != "" ]; then
	echo "Removing old $keyname key: " $old_key
	/bin/rm -i /opt/nfast/kmdata/local/$old_key
    fi
    
    generatekey --batch embed protect=Token plainname="$keyname" \
	size=$size embedsavefile=$hsm_dir/$keyname-key.pem

    rm -f $hsm_dir/$keyname-key_*

    openssl req -engine chil -config $HOME/conf/$keyname.cnf -new \
	-key $hsm_dir/$keyname-key.pem -out $hsm_dir/$keyname-req.pem
}


generate_cert ()
{
    keyname=$1
    signer=$2

    days=`days_till $HOME/conf/$keyname.exp`
    serial=$HOME/conf/serial

    chmod u+rw $serial

    openssl x509 -engine chil -req -CA $signer_dir/$signer-cert.pem \
	-CAkey $signer_dir/$signer-key.pem -CAserial $serial \
	-in $hsm_dir/$keyname-req.pem -out $hsm_dir/$keyname-cert.pem \
	-days $days -sha1 -extfile $HOME/conf/$keyname.cnf -extensions v3_ca

    ret=$?

    if [ $ret != 0 ]; then
	return 1
    fi	

    rm -f $hsm_dir/$keyname-req.pem

    chmod a=r $serial

    LoadCert $hsm_dir/$keyname-cert.pem $hsm_dir/$keyname-key.pem
}    

generate_BB_cert ()
{
    keyname=$1
    signer=$2
    prefix=$3
    issuer=$4

    days=`days_till $HOME/conf/$keyname.exp`
    serial=$HOME/conf/serial

    chmod u+rw $serial

    if [ "$issuer" = "" ]; then
	gencert -key $hsm_dir/$keyname-key.pem -prefix $prefix \
	    -CA $signer_dir/$signer-cert.raw \
	    -CAkey $signer_dir/$signer-key.pem -CAserial $serial \
	    -out $hsm_dir/$keyname-cert.raw -days $days
    else
	gencert -key $hsm_dir/$keyname-key.pem -prefix $prefix \
	    -issuer $issuer -CAkey $signer_dir/$signer-key.pem \
	    -CAserial $serial -out $hsm_dir/$keyname-cert.raw -days $days
    fi	    

    ret=$?
    if [ $ret != 0 ]; then
        return 1
    fi

    rm -f $hsm_dir/$keyname-req.pem

    chmod a=r $serial

    LoadCert $hsm_dir/$keyname-cert.raw $hsm_dir/$keyname-key.pem
}    

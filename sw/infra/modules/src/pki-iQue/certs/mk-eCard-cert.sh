#!/bin/sh

. $HOME/lib/functions

need_two_dirs $*

SERIAL=`cat $HOME/conf/serial`
PREFIX="eCard Signer"

COMMON_NAME=$PREFIX:$SERIAL
export COMMON_NAME

generate_cert eCard eCard-CA

#!/bin/sh

. $HOME/lib/functions

need_one_dir $*

SERIAL=`cat $HOME/conf/serial`
PREFIX="eCard Signer"

COMMON_NAME=$PREFIX:$SERIAL
export COMMON_NAME

generate_key eCard 1024

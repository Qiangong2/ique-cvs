#!/bin/sh

. $HOME/lib/functions

need_one_dir $*

SERIAL=`cat $HOME/conf/serial`
PREFIX="Personnel Cert Issuer"

COMMON_NAME=$PREFIX:$SERIAL
export COMMON_NAME

generate_key opCard 2048

#!/bin/sh

. $HOME/lib/functions

need_two_dirs $*

SERIAL=`cat $HOME/conf/serial`
PREFIX="Personnel Cert Issuer"

COMMON_NAME=$PREFIX:$SERIAL
export COMMON_NAME

generate_cert opCard server-CA

#!/bin/sh

. $HOME/lib/functions

need_one_dir $*

SERIAL=`cat $HOME/conf/serial`
PREFIX="Server Cert Issuer"

COMMON_NAME=$PREFIX:$SERIAL
export COMMON_NAME

generate_key server 2048

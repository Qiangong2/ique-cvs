#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <values.h>
#include <stdio.h>
#include <openssl/evp.h>
#include <openssl/rsa.h>
#include <openssl/x509.h>
#include <openssl/pem.h>
#include <integer_math.h>
#include <sha1.h>


#ifndef false
#define false	0
#endif

#ifndef true
#define true	1
#endif

static const char* cert_file = 0;
    

static struct option long_options[] = {
    {"cert", 1, 0, 'c'},              // Certficate file (PEM format)
    {"verbose", 0, 0, 'v'},           // Print debug messages
    {"help", 0, 0, 'h'},              // Print usage
    {0, 0, 0, 0}
};


BSL_error 
bigint_print(bigint *a, int int_max){
    short int i;
    
    for(i =0; i <= int_max; i++){
        printf("%d = %08x\n ", i, a->half_word[i]);
    }
    return BSL_OK;
}


static void usage(const char* progname) {
    fprintf(stderr, "Usate: %s\n", progname);
    fprintf(stderr, 
           " -cert arg		- certificate file in PEM format.\n"
           " -verbose 		- print debug messages.\n"
           " -help    		- print this message.\n");
}


int main(int argc, char **argv){
    RSA *prsa;
    X509 *px509;
    bigint n, m, e, o;
    int i, j, k, e_size, n_size;
    unsigned char e_string[4096];
    unsigned char n_string[4096];

    unsigned int c_flag = false;
    unsigned int verbose = false;

    while (1) {
        int ch = getopt_long_only(argc, argv, "", long_options, 0);

	if (ch == -1)
            break;

	switch (ch) {
        case 'c':   // Certificate PEM file
	    cert_file = optarg;
	    c_flag = true;
            break;
        case 'v':   // Verbose
	    verbose = true;
            break;
        case 'h': 
	default:
            usage(argv[0]);
	    return 1;
        }
    }

    if (c_flag == false) {
        usage(argv[0]);
	return 1;
    }


    FILE *fp = fopen(cert_file, "r");
    if (fp == 0) {
        perror("Cannot open certificate file");
	fprintf(stderr, "Cert file= %s\n", cert_file);
	return 1;
    }

    px509 = 0;
    px509 = (X509 *)PEM_read_X509(fp, 0, 0, 0);
    if (px509 == 0) {
        perror("Cannot read certificate file");
	fprintf(stderr, "Cert file= %s\n", cert_file);
        return 1;
    }


    EVP_PKEY *pubkey;
    pubkey = X509_get_pubkey(px509);
    if (pubkey == 0) {
        perror("Cannot read public key");
	fprintf(stderr, "Cert file= %s\n", cert_file);
        return 1;
    }

    if (pubkey->type == EVP_PKEY_RSA) {
        n_size = BN_bn2bin(pubkey->pkey.rsa->n, n_string);

        printf("/*\n * Root public key\n */\n");
        printf("const BbRsaPublicKey4096 gRootKey = {\n");
        for (i =0; i < n_size; ) {
            for (j=0; j < 4; j++) {
                printf(" 0x%02x%02x%02x%02x,", 
                    n_string[i], n_string[i+1], n_string[i+2], n_string[i+3]);
		i += 4;
	    }
	    printf("\n");
        }

        printf("};\n\n");

	if (verbose) {
            printf("n_size=%d\n", n_size);
            for( i =0 ; i < n_size; i++){
                printf("n[%d] = %02x\n", i, n_string[i]);
            }
            printf("BN2Hex=%s\n", BN_bn2hex(pubkey->pkey.rsa->n));
        }

        e_size = BN_bn2bin(pubkey->pkey.rsa->e, e_string);
	if (verbose) {
            printf("e_size=%d\n", e_size);

            for( i =0 ; i < e_size; i++){
                printf("e[%d] = %02x\n", i, e_string[i]);
            }
        }
        printf("const BbRsaExponent gRootExp = 0x%s;\n", 
		BN_bn2hex(pubkey->pkey.rsa->e));
    }

    fclose(fp);

    return 0;
}
    


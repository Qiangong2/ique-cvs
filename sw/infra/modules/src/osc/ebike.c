#include "com.h"
#include "ebike.h"

RaceRecords *Records[LFTRACK_COUNT];
static int inter_change_u32(int data)
{
	 return((data>>8*3)&0xFF)+((data>>8)&0xFF00)+((data<<8)&0xFF0000)+((data&0xFF)<<8*3);
}

static char *print_standard_time(int time)
{
	short min = 0;
	float sec = 0;
	float tmp = 0;
	static char res[20];
	min = time / 3600;
	sec = (time - min * 60 * 60) / 60.0;
	tmp = sec * 100.0;
	sec = tmp / 100.0;

	memset(res, 0, 20);
	if (min<=9) {
		if (sec<10) {
			sprintf(res, "0%d:0%2.2f\n", min, sec);
		} else {
			sprintf(res, "0%d:%2.2f\n", min, sec);
		}
	} else {
		if(sec<10) {
			sprintf(res, "%d:0%2.2f\n", min, sec);;
		} else {
			sprintf(res, "%d:%2.2f\n", min, sec);
		}
	}
	return res;
}

static void calc_records(const char *buf)
{
	int i,j;
	for (i=0;i<LFTRACK_COUNT;i++) {
		Records[i] = (RaceRecords *)(buf+8+i*28);
	}
	for (i=0;i<LFTRACK_COUNT;i++) {
		for (j=0;j<4;j++) {
Records[i]->times[j] = inter_change_u32(Records[i]->times[j]);
		}
	}
}

char *
score_ebike(const char *state_buf)
{
	int i;
	int result=0;
	int dataLen = 8+27*28;
	static char res[3072];
	char *data;

	data = malloc(sizeof(char)*dataLen);
	memcpy((void *)data, (void *)state_buf, dataLen);
	calc_records((char *)data);

	memset(res, 0, 3072);
	for (i=0;i<LFTRACK_COUNT;i++) {
		result = Records[i]->times[0];
		if (i==LFTRACK_STUNT) {
			sprintf(res, "%s%d::%d::%d;", res, i+1, result, result);
		} else { 
			sprintf(res, "%s%d::%s::%d;", res, i+1, print_standard_time(result), result);
			}
	}
	free(data);
	return res;
}

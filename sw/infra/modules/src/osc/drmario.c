#include "com.h"

//u8 fileData[0x800] = {0};
int myBufIdx = 0;
int myBitIdx = 0;

static u32 get_score(const u8 *buf)
{
	int i;
	u32 bits = 0;

	for(i=15;i>=0;i--){
		bits |= ((buf[myBufIdx] >> myBitIdx) & 1) << i;
		myBitIdx++;
		myBufIdx += myBitIdx >> 3;
		myBitIdx &= 7;
	}
	bits *= 10;

	return bits;
}

char *
score_drmario(const char *buf) 
{
	int i;
	u32 cur, result = 0;
	myBufIdx = 0;
	myBitIdx = 366;
	char *data;
	static char res[128];

	data = malloc(sizeof(char)*0x800);
	memcpy((void *)data, (void *)buf, 0x800);
	
	for (i=0; i<8; i++) {
		cur = get_score((char *)data);
		if (i==5) {
			myBufIdx = 1;
			myBitIdx = 3352;
		} else {
			myBufIdx = 0;
			myBitIdx = 366+499*(i+1);
		}
		if ( cur > result ) {
			result = cur;
		}
	}
	free(data);
	memset(res, 0, 128);
	sprintf(res,"%d::%d::%d", ID_DRMARIO_DAB, result, result);
	return res;
}

#include <string.h>
#include <jni.h>

#include <libcrypto/bbtoolsapi.h>
#include "ComputeScore.h"

extern "C" char *
score_state(unsigned int tid, unsigned int cid, char *state_buf,
	    int state_size, BbEccSig sig, BbEccPublicKey key);


JNIEXPORT jstring JNICALL
Java_com_broadon_osc_java_1util_OscResultGameState_getScore(JNIEnv* env,
						    jclass,
						    jlong _tid,
						    jlong _cid,
						    jbyteArray _states,
						    jbyteArray _sig,
						    jbyteArray _key)
{
    int buf_size = env->GetArrayLength(_states);
    char states[buf_size];
    //jstring js;

    BbEccSig sig;
    BbEccPublicKey key;
 
    if (env->GetArrayLength(_sig) != sizeof(sig))
	return NULL;

    env->GetByteArrayRegion(_states, 0, buf_size, (jbyte*) states);
    env->GetByteArrayRegion(_sig, 0, sizeof(sig), (jbyte*) &sig);

    // the BB public key is converted to an byte array from a Java
    // "BigInteger" class.  This conversion might either omit the leading
    // zero's or add a zero byte as sign bit.  So we have to either
    // truncate or pad it here.
    
    int key_len = env->GetArrayLength(_key);
    int diff = sizeof(key) - key_len;
    if (diff > 0) {
	memset(key, 0, diff);
	env->GetByteArrayRegion(_key, 0, key_len, ((jbyte*) &key) + diff);
    } else {
	env->GetByteArrayRegion(_key, -diff, sizeof(key), (jbyte*) &key);
    }

    char *res_str = score_state(_tid, _cid, states, buf_size, sig, key);

    return env->NewStringUTF(res_str);
} 

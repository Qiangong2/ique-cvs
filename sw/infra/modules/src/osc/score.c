#include <netinet/in.h>

#include <openssl/sha.h>
#include <libcrypto/bbtoolsapi.h>


/*****************
 *
 * TO DO:
 * The following prototype and enum definitions were copied from the BB
 * source tree.  They should be placed in a properly exported headers.
 *****************/
typedef enum { BSL_TRUE = 0, BSL_FALSE } boolean;

extern void
bsl_verify_ecc_sig(u8* data, u32 datasize, BbEccPublicKey public_key,
		   BbEccSig sign, boolean* res, u32 identity); 

static int
verify_state(u8 *state_buf, int state_size, BbEccSig sig, BbEccPublicKey publicKey)
{
    SHA_CTX sha;
    BbShaHash hash;
    boolean result;
    int i;

    SHA1_Init(&sha);
    SHA1_Update(&sha, state_buf, state_size);
    SHA1_Final((u8*) &hash, &sha);

    for (i = 0; i < sizeof(BbEccPublicKey)/sizeof(u32); ++i) {
        publicKey[i] = ntohl(publicKey[i]);
    }

    for (i = 0; i < sizeof(BbEccSig)/sizeof(u32); ++i) {
        sig[i] = ntohl(sig[i]);
    }


    bsl_verify_ecc_sig((u8 *) &hash, sizeof(hash), publicKey, sig, &result, 1);
    return result == BSL_TRUE;
}

extern char *score_mario64(const char *state_buf);
extern char *score_ocarina(const char *state_buf);
extern char *score_starfox(const char *state_buf);
extern char *score_waverace(const char *state_buf);
extern char *score_mariokart(const char *state_buf);
extern char *score_fzerox(const char *state_buf);
extern char *score_drmario(const char *state_buf);
extern char *score_ebike(const char *state_buf);

char *
score_state(unsigned int tid, unsigned int cid, char *state_buf,
	    int state_size, BbEccSig sig, BbEccPublicKey publicKey)
{
    static char err[3]="ERR";
    if (!verify_state(state_buf, state_size, sig, publicKey)) 
        return err;

    switch (tid) {
        case 11011: /* Super Mario 64 */
            return score_mario64(state_buf);
        //case 21011: /* Zelda: Ocarina of Time */
        //    return score_ocarina(state_buf);
        //case 41011: /* Starfox */
        //    return score_starfox(state_buf);
        //case 51011: /* Wave Race */
        //    return score_waverace(state_buf);
        //case 52011: /* Mario Kart */
        //    return score_mariokart(state_buf);
        //case 52021: /* F-Zero X */
        //    return score_fzerox(state_buf);
        case 61011: /* Dr. Mario */
            return score_drmario(state_buf);
		case 51021: /* excite bike */
			return score_ebike(state_buf);
    }

    return err;
}


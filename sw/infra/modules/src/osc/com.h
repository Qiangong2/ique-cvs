/* File: com.h
 * Contain the common information for Native C
 * $Author: Allen Chen 
 * $Log: com.h,v $
 * Revision 1.1  2005/02/07 09:24:38  tomsheng
 * Add for Game Competition
 *
 */
#ifndef _COM_H
#define _COM_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;

/* The column ID for Excite Bike will be obey to 
 * the order of the autodrome from the Game State 
 * themselves. 
 * The program auto used the ID from 1 - 24 to 
 * indicate the 24 different Game scores. */
#define ID_EBIKE_ANOTHER 0

/* The column ID for Doctor Mario */
#define ID_DRMARIO_NEWBIE 0
#define ID_DRMARIO_DAB 25
#define ID_DRMARIO_PRO 0

/* The column ID for iQue Mario */
#define ID_MARIO64_STAR 26
#define ID_MARIO64_OTHER 0

#endif

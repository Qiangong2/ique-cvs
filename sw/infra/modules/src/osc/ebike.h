/* Excite Bike Game State Struct */

enum
{
	// Group 1
	LFTRACK_STADIUM,		//	"Kyoto, Japan"		//	京都
	LFTRACK_QUARRY,			//	"Mountain Quarry"	//	采石场
	LFTRACK_STADIUM2,		//	"Houston, TX"		//	休斯顿
	LFTRACK_LUMBERMILL,		//	"Lefty's Mill"		//	乐福帝工场
	LFTRACK_STADIUM3,		//	"Orlando, FL"		//	奥兰多
	// Group 2
	LFTRACK_STADIUM4,		//	"Nashville, TN"		//	纳什维尔
	LFTRACK_CANYON,			//	"Canyon Chasm"		//	溪谷
	LFTRACK_STADIUM5,		//	"Long Island, NY"	//	长岛
    LFTRACK_JUNGLE2,		//	"Congo Course"		//	刚果赛道
	LFTRACK_STADIUM6,		//	"Las Vegas, NV"		//	拉斯维加斯
	// Group 3
	LFTRACK_STADIUM8,		//	"Phoenix, AZ"		//	菲尼克斯
	LFTRACK_JUNGLE,			//	"Rainforest Run"	//	雨林赛道
	LFTRACK_STADIUM9,		//	"Madrid, Spain"		//	马德里
	LFTRACK_QUARRY2,		//	"The Gravel Pit"	//	砂石路
	LFTRACK_STADIUM7,		//	"Detroit, MI"		//	底特律
	// Group 4
	LFTRACK_CANYON2,		//	"Goldmine Rush"		//	金矿赛道
	LFTRACK_STADIUM10,		//	"Los Angeles, CA"	//	洛杉矶
	LFTRACK_CONSTRUCTION,	//	"Construction Yard"	//	工地
	LFTRACK_STADIUM11,		//	"Seattle, WA"		//	西雅图
	LFTRACK_SNOW,			//	"Blizzard Blitz"	//	冰雪赛道


	// Other tracks
	LFTRACK_EXCITE,			//	"Excite 3D"			//	原始越野摩托3D
	//LFTRACK_USER,			//	"User Track"		//	（记录中没显示）
	//LFTRACK_SOCCER,		//	"Soccer"			//	（记录中没显示）
	LFTRACK_HILLCLIMB,		//	"Hill Climb"		//	登山赛
	LFTRACK_STUNT,			//	"Stunt Course"		//	特技赛
	LFTRACK_DESERT,			//	"Desert"			//	沙漠赛
	//LFTRACK_TUTORIAL,		//	"Tutorial"			//	（记录中没显示）

	LFTRACK_COUNT
};

typedef struct tagRaceRecords
{
	int		times[4];	// ordered race times 1,2,3, lap time
	char	name[4][3];
} RaceRecords;			// size 28 bytes

/*char	tracksName[LFTRACK_COUNT][15]={
	"京都",
	"采石场",
	"休斯顿",
	"乐福帝工场",
	"奥兰多",
	
	"纳什维尔",
	"溪谷",
	"长岛",
	"刚果赛道",
	"拉斯维加斯",
	
	"菲尼克斯",
	"雨林赛道",
	"马德里",
	"砂石路",
	"底特律",
	
	"金矿赛道",
	"洛杉矶",
	"工地",
	"西雅图",
	"冰雪赛道",
	
	"原始越野摩托3D",
	//"记录中没显示",
	//"记录中没显示",
	"登山赛",
	"特技赛",
	"沙漠赛",
	//"记录中没显示",
};
*/

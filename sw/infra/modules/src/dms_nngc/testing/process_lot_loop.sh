#!/bin/sh

cd /tmp/

max=5000
i=0

while [ $i -lt $max ]; do
    rm -rf id.tar result result-*.tar
    mv -f /opt/broadon/data/dms_nngc/ftp/id.tar /tmp/;
    tar -xf /tmp/id.tar -C /tmp/;
    mv /tmp/id /tmp/result;
    file=result-tester01-`date +%s`.tar
    tar -cf $file result;
    time /opt/broadon/pkgs/dms_nngc/bin/processor $file
    echo exit status: $? round: $((i++));
    sleep 3;
done

#ifndef _FTPCONFIGPAGE_H_
#define _FTPCONFIGPAGE_H_

#include "Page.h"

class FTPConfigPage : public Page {
    GtkWidget *_errorLabel;
    GtkWidget *_idEntry;
    GtkWidget *_passwdEntry0;
    GtkWidget *_passwdEntry1;

public:
    FTPConfigPage(Mpc *);
    ~FTPConfigPage() {}

    static void ok_clicked(GtkWidget *, gpointer);

    void install();
    void remove();
};

#endif /* _FTPCONFIGPAGE_H_ */

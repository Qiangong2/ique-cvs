#include "common.h"			// for base64 encoding

#include "x86/esl.h"
#include "BB_data.h"

bool
BCC_DATA::generate_private_data(const ECC_KEY_PAIR& eccKey,
				const DATA_TEMPLATE& data_template,
				const char* issuer)
{
    u8 rand[ES_RVL_RAND_SIZE];

    if (hsm.get_random_bytes(rand, sizeof(rand)) != Status_OK)
	return false;

    return (ES_GenerateRvlDeviceData(data_template, sizeof(data_template),
				     rand, sizeof(rand), &eccKey.privateKey,
				     &raw_cert, (u8*)issuer, data,
				     sizeof(data)) ==
	    ES_ERR_OK);
} // generate_private_data


bool
BCC_DATA::create_cert(unsigned int id, const ECC_KEY_PAIR& ecckey,
		      const char* issuer)
{
    char subject[ES_NAME_SIZE];
    sprintf(subject, ES_NG_PREFIX "%08x", id);

    u32 date = time(0) + 20 * 365 * 24 * 3600; // 20 years

    if (ES_GenerateDeviceCert(subject, issuer, &ecckey.publicKey, date,
			      &raw_cert) != ES_ERR_OK)
	return false;

    u8* docStart = raw_cert.sig.issuer;
    const NFast_Bignum* bn =
	hsm.sign(&(raw_cert.sig.issuer),
		 sizeof(raw_cert) - (docStart - (u8*)&raw_cert));
    if (!bn) {
	return false;
    }

    memcpy(&(raw_cert.sig.sig), bn->value, bn->nbytes);

    delete bn;

    // Base64 encode
    return (base64_encode((char*) &raw_cert, sizeof(raw_cert), encoded_cert) >= 0);
    
} // create_cert


#ifdef NOHSM

#define SKHASH_SIZE             20
#define ROMPATCH_SIZE           64
#define PUBLICKEY_SIZE          64
#define PAD_SIZE                104
#define BB_CHIPID_SIZE  4
#define BB_CHIPID_HEX_SIZE      BB_CHIPID_SIZE*2
struct privdata {
    char skHash[SKHASH_SIZE];
    char romPatch[ROMPATCH_SIZE];
    char publicKey[PUBLICKEY_SIZE];
    unsigned int bbId;
    char pad[PAD_SIZE];
};

BCC_DATA::BCC_DATA(unsigned int id, const DATA_TEMPLATE& data_template,
		 const BCC_CERT& mftr, HSM& hsm) :
    bbid(id), hsm(hsm), valid(false)
{
    // simulate HSM delay
    usleep((int)((50 / (float)500)*1000000));//XXX

    valid = true;

    for (unsigned int i=0; i<PRIVATE_DATA_SIZE/sizeof(unsigned int); i++) {
        data[i] = rand();
    }

    // XXX temporary eFuse data
    data[0] = 0;
    data[1] = 0;
    data[2] = 0;
    data[3] = 0;
    data[4] = 0;
    data[5] = htonl(bbid);
    data[31] = 0;

    struct privdata *pd = (struct privdata*)data;
    pd->bbId = htonl(bbid);

    string tmp;
    for (unsigned int i=0; i<sizeof(BbEccCert); i++) {
        tmp += "x";
    }
    
    char bb_id_hex[BB_CHIPID_HEX_SIZE+1];
    sprintf(bb_id_hex, "%08x", id);
    BbEccCert *bbcert = (BbEccCert*)tmp.data();
    strncpy((char*)bbcert->certId.name.bbid+2,
           bb_id_hex, BB_CHIPID_HEX_SIZE);
    base64_encode(tmp, cert);
} // constructor 

#else /* NOHSM */

BCC_DATA::BCC_DATA(unsigned int id, const DATA_TEMPLATE& data_template,
		 const BCC_CERT& mftr, HSM& hsm) :
    deviceID(id), hsm(hsm), valid(false)
{
    ECC_KEY_PAIR keypair(hsm);

    if (! keypair.isValid)
	return;

    char issuer[ES_NAME_SIZE];
    snprintf(issuer, sizeof(issuer), "%s-%s", mftr.issuerName, mftr.subjectName);
    if (! create_cert(id, keypair, issuer))
	return;
    
    valid = generate_private_data(keypair, data_template, mftr.issuerName);

} // constructor 

#endif /* NOHSM */

//======================================================================
// Other Virage data related functions
//======================================================================

struct File
{
    int fd;
    int size;

    File(const char* filename) {
	fd = open(filename, O_RDONLY);
	struct stat stat_buf;
	if (fstat(fd, &stat_buf) != 0)
	    size = -1;
	else
	    size = stat_buf.st_size;
    }

    ~File() {
	if (fd >= 0)
	    close(fd);
    }

    inline bool isValid() {
	return fd >= 0 && size >= 0;
    }
};

#ifdef NOHSM

bool
BCC_DATA::read_data_template(const char* template_file,
			     const char* template_signature,
			     const BB_CERT& cert,
			     DATA_TEMPLATE& buffer)
{
    return true;
}

#else /* NOHSM */

bool
BCC_DATA::read_data_template(const char* template_file,
			     const char* template_signature,
			     const BCC_CERT& cert,
			     DATA_TEMPLATE& buffer)
{
    // read the template file
    File tfile(template_file);

    if (! tfile.isValid() || tfile.size != ES_RVL_PVT_DATA_SIZE)
	return false;

    if (read(tfile.fd, buffer, tfile.size) != tfile.size) {
	return false;
    }

    // read the signature file
    File sfile(template_signature);
    if (! sfile.isValid())
	return false;

    unsigned char sig[sfile.size];
    if (read(sfile.fd, sig, sfile.size) != sfile.size)
	return false;

    return verify(cert, (unsigned char*) buffer, sizeof(buffer),
		  sig, sizeof(sig));
}

#endif /* NOHSM */

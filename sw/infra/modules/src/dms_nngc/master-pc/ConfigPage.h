#ifndef _CONFIGPAGE_H_
#define _CONFIGPAGE_H_

#include "Page.h"

class ConfigPage : public Page {
  public:
    ConfigPage(Mpc *);
    ~ConfigPage() {}
    
    void install();
    void remove();
};

#endif /* _CONFIGPAGE_H_ */

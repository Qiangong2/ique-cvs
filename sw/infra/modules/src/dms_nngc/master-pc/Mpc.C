#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
#include <sstream>
#include "db.h"
#include "dbaccess.h"
#include "BB_data.h"
#include "Mpc.h"

#include "LoginPage.h"
#include "RebootPage.h"
#include "ShutdownPage.h"
#include "MainMenuPage.h"
#include "OperationsPage.h"
#include "GenReportPage.h"
#include "HistoryPage.h"
#include "ConfigPage.h"
#include "MaintenancePage.h"
#include "FTPConfigPage.h"
#include "NetConfigPage.h"
#include "NTPConfigPage.h"
#include "PasswdConfigPage.h"
#include "SoftwareUpdatePage.h"
#include "ReviewLotsPage.h"
#include "OkPage.h"
#include "ProgressPage.h"
#include "CdBurnPage.h"
#include "InFileWriter.h"
#include "XmlWriter.h"
#include "Assert.h"

using namespace std;

void system_call(string command)
{
    command += " > /dev/null 2>&1";
    int ret = system(command.c_str());
    if (ret == -1 || WEXITSTATUS(ret) != 0) {
        fatal("error on system(): %s\n", command.c_str());
    }
}

/* check if lot should be started */
void Mpc::checkStartLot()
{
    string tarfile = FTPDIR;
    tarfile += "/" + string(IDFILENAME);
    struct stat statbuf;

    if (stat(tarfile.c_str(), &statbuf) != 0) {
        /* check hsm card */
        const BCC_CERT issuer_cert(CERTFILE);
        char buf[BUF_MAX];
        HSM hsm(Mpc::getConf(MPC_HSM_KEY, buf), issuer_cert);
        if (! hsm.is_cardset_loaded()) {
            return;
        }

        doStartLot();
    }
}

gint Mpc::timeout_callback( gpointer data )
{
    checkStartLot();

    return TRUE;
}

gboolean Mpc::doStartLot()
{
    time_t date = time(NULL);
    DB db;

    /* start transaction */
    if (db.Begin() == -1) {
        fatal("DB error!\n");
    }

    char buf[BUF_MAX];
    string cert_id = getConf(MPC_CERT_ID, buf);
    string chip_rev = getConf(MPC_CHIP_REV, buf);
    string manufacturer = getConf(MPC_MANUFACTURER, buf);
    string location = getConf(MPC_LOCATION, buf);
    
    DB_sequence seq(db, "chip_id");
    unsigned int first_chip_id = seq.nextval();
    bool first_chip_id_used = false;

    ostringstream oss;
    oss << (atoll(Mpc::getConf(MPC_DEVICE_TYPE, buf))
            << 32) /* prefix * 2^32 */
        + first_chip_id;
    string lot_number = oss.str();

    /* Insert into LOTS */
    if (insert_lots(db,
                    lot_number,
                    "",
                    LOT_SIZE,
                    date,
                    cert_id,
                    chip_rev,
                    manufacturer,
                    location) == -1) {
        db.Rollback();
        fatal("DB error!\n");
    }

    /* Insert into LOT_CHIPS */

    const BCC_CERT issuer_cert(CERTFILE);
    if (! issuer_cert.isValid()) {
	fatal("Issuer certificate invalid!\n");
    }
    BCC_DATA::DATA_TEMPLATE priv_data_template;
    if (! BCC_DATA::read_data_template(TEMPLATE_FILE, TEMPLATE_SIGNATURE,
				      issuer_cert, priv_data_template)) {
	fatal("Invalid private data template file!\n");
    }
    HSM hsm(getConf(MPC_HSM_KEY, buf), issuer_cert);
    if (! hsm.operational()) {
        fatal("HSM not operational!\n");
    }


    char curdir[MPC_PATH_MAX];
    sprintf(curdir, "%s/%s", TMPDIR, IDDIRNAME);
    mkdir(curdir, 0777);

    unsigned int chip_id;
    for (guint i=1; i<=LOT_SIZE; i++) {
        /* get BB data */
        if (!first_chip_id_used) {
            chip_id = first_chip_id;
            first_chip_id_used = true;
        } else {
            chip_id = seq.nextval();
        }
        if (chip_id == (unsigned)-1) {
            FILE *fp = NULL;
            if ((fp = fopen(HARDERROR, "w")) != NULL) {
                fprintf(fp, "Max ID reached - Replace ID Creator\n");
                fclose(fp);
            }
            break;
        }

	BCC_DATA data(chip_id, priv_data_template, issuer_cert, hsm);
        if (! data.isValid()) {
            fatal("BB private data not valid!\n");
        }

        /* write .id file */
        if (writeInFile(curdir, i, chip_id, data.getPrivateData()) == -1) {
            fatal("Error writing .id file\n");
        }

        /* insert into database */
        if (insert_lot_chips(db, chip_id, lot_number, data.getEncodedCert()) == -1) {
            db.Rollback();
            fatal("DB error!\n");
        }

        while(g_main_context_iteration(NULL, FALSE));

        if (assert_bbid_integrity(chip_id, curdir, i, &db)) {
            sprintf(curdir, "%s/%s", TMPDIR, IDDIRNAME);
            string command = "rm -rf ";
            command += curdir;
            system_call(command);
            
            if (db.Rollback() == -1) {
                fatal("DB rollback error\n");
            }
            return false;
        }
    }

    /* Create tar file, clean up */
    string command = TARPROGRAM;
    command = command + " no " + TMPDIR + " " +
        FTPDIR + "/" + IDFILENAME + " " +
        IDDIRNAME;
    system_call(command);

    string desc = "New id.tar generated.";
    insert_history(db, date, desc);

    /* commit transaction */
    if (db.Commit() == -1) {
        fatal("DB error!\n");
    }

    return true;
}

void Mpc::doReviewLotsList(GtkListStore *list)
{
    GtkTreeIter iter;
    guint i = 0;
    
    gtk_list_store_clear(list);

    string constraints;
    if (useDateRange) {
        constraints = "create_date between ";
        constraints += tostring(opFromTime);
        constraints += " and ";
        constraints += tostring(opToTime);
    }

    /* add data to the list store */
    DB_query q(*_db, "lots", constraints, "create_date DESC");
    if (q.status()) {
        fatal("DB query error!\n");
    }
    for (DB_query::iterator it = q.begin();
         (it != q.end() && i < MAX_LIST_ELEMENTS);
         it++)
    {
        Row r = *it;
        gtk_list_store_append (list, &iter);

        time_t time = atoi(r["create_date"].c_str());
        char create_date_buf[BUF_MAX];
        format_date(&time, create_date_buf);

        char last_processed_buf[BUF_MAX];
        if (r.find("last_processed") == r.end()) {
            *last_processed_buf = 0;
        } else {
            time = atoi(r["last_processed"].c_str());
            format_date(&time, last_processed_buf);
        }

        char last_reported_buf[BUF_MAX];
        if (r.find("last_reported") == r.end()) {
            *last_reported_buf = 0;
        } else {
            time = atoi(r["last_reported"].c_str());
            format_date(&time, last_reported_buf);
        }

        gtk_list_store_set (list, &iter,
                            0, r["lot_desc"].c_str(),
                            1, atoi(r["lot_size"].c_str()),
                            2, create_date_buf,
                            3, last_processed_buf,
                            4, last_reported_buf,
                            5, atoi(r["num_processed"].c_str()),
                            -1);
        i++;
    }
}

guint Mpc::doGenReportsList(GtkListStore *list)
{
    GtkTreeIter iter;
    guint i = 0;
    guint num_selected = 0;
    
    gtk_list_store_clear(list);

    string constraints;
    if (useDateRange) {
        constraints = "create_date between ";
        constraints += tostring(opFromTime);
        constraints += " and ";
        constraints += tostring(opToTime) + " and ";
    }
    constraints += "last_processed IS NOT NULL";

    /* add data to the list store */
    DB_query q(*_db, "lots", constraints, "last_processed DESC");
    if (q.status()) {
        fatal("DB query error!\n");
    }

    for (DB_query::iterator it = q.begin();
         (it != q.end() && i < MAX_LIST_ELEMENTS);
         it++)
    {
        Row r = *it;
        gtk_list_store_append (list, &iter);
        time_t time = atoi(r["create_date"].c_str());
        char buf[BUF_MAX];
        format_date_time(&time, buf);
        bool select;
        if (useDateRange || // select all if range specified
            (r.find("last_reported") == r.end() ||
             guint(atoi(r["last_processed"].c_str())) > 
             guint(atoi(r["last_reported"].c_str())) )) {
            select = TRUE;
            num_selected++;
        } else {
            select = FALSE;
        }
        gtk_list_store_set (list, &iter,
                            0, select,
                            1, r["lot_desc"].c_str(),
                            2, buf,
                            3, r["lot_number"].c_str(),
                            -1);
        i++;
    }

    return num_selected;
}

gboolean Mpc::doGenReport(GtkListStore *list)
{
    GtkProgressBar *bar = _progressPage->getBar();

    /* start transaction */
    if (_db->Begin() == -1) {
        fatal("DB error!\n");
    }

    GtkTreeIter iter;
    GtkTreeModel *model = (GtkTreeModel *)list;
    gboolean valid;
    time_t date = time(NULL);
    gdouble size = gtk_tree_model_iter_n_children (model, NULL);
    guint i = 1;
    
    valid = gtk_tree_model_get_iter_first(model, &iter);

    string dir = TMPDIR;
    dir += "/";
    dir += XMLDIRNAME;
    mkdir(dir.c_str(), 0777);

    FILE *lots_xml = openTableXML(dir+"/lots.xml");
    FILE *lot_chips_xml = openTableXML(dir+"/lot_chips.xml");
    if (lots_xml == NULL ||
        lot_chips_xml == NULL) {
        fatal("dumpTableXML error\n");
    }

    guint lotCount = 0;
    while (valid)
    {
        gboolean select;
        gchar *lot;
        gchar *lot_number;
        
        gtk_tree_model_get (model, &iter, 
                            0, &select,
                            1, &lot,
                            3, &lot_number,
                            -1);
        
        if (select) {
            string constraints = "lot_number IN ('";
            constraints += lot_number;
            constraints += "')";
            DB_insertor ins(*_db, "lots");
            Row r;
            r["last_reported"]  = tostring(date);
            ins.UpdateAll(r, constraints);
            if (ins.Errcount() > 0) {
                fatal("DB error!\n");
            }

            if (dumpTableXML(lots_xml, _db, "lots", "lot_number IN ('" + string(lot_number) + "')") ||
                dumpTableXML(lot_chips_xml, _db, "lot_chips", "lot_number IN ('" + string(lot_number) + "')") ) {
                fatal("dumpTableXML error\n");
            }
            lotCount++;
        }
        g_free (lot);        
        
        valid = gtk_tree_model_iter_next (model, &iter);

        gtk_progress_bar_set_fraction(bar, (i/size));
        while(g_main_context_iteration(NULL, FALSE));
        i++;

        if (_progressPage->getCancel()) {
            if (_db->Rollback() == -1) {
                fatal("DB rollback error\n");
            }
            return false;
        }
    }
    closeTableXML(lots_xml);
    closeTableXML(lot_chips_xml);

    /* Create tar file, clean up */
    string command = TARPROGRAM;
    command = command + " no " + TMPDIR + " " +
        TMPDIR + "/" + XMLTARNAME + " " +
        XMLDIRNAME;
    system_call(command);

    /* sign tar file */
    command = SIGNPROGRAM;
    char buf[BUF_MAX];
    command = command + " '" + getConf(MPC_HSM_KEY, buf) + "' " +
        CERTFILE + " " +
        TMPDIR + "/"+ XMLTARNAME + " " + 
        TMPDIR + "/" + XMLSIGNAME;
    system_call(command);

    /* write out cert id */
    FILE *fp;
    sprintf(buf, "%s/%s", TMPDIR, XMLCIDNAME);
    fp = fopen(buf, "w");
    if (fp == NULL || fprintf(fp, getConf(MPC_CERT_ID, buf)) == -1) {
        fatal("error writing cert id to %s\n", XMLCIDNAME);
    }
    fclose(fp);

    /* tar up tar + sig */
    struct tm *tmp_tm = localtime(&date);
    strftime(buf, BUF_MAX, "%b%d_%Y_", tmp_tm);
    reportFileName = buf;
    reportFileName += tostring(date);
    reportFileName += ".tar.bz2";

    command = TARPROGRAM;
    command = command + " yes " + TMPDIR + " " + 
        TMPDIR + "/" + reportFileName + " " +
        XMLTARNAME + " " + XMLSIGNAME + " " + XMLCIDNAME;
    system_call(command);

    string desc = "Report generated for ";
    desc += tostring(lotCount) + " lots.";
    insert_history(*_db, date, desc);

    return true;
}

bool Mpc::existLot(gchar * lot)
{
    string constraint = "lot_number='";
    constraint += lot;
    constraint += "'";

    DB_query q(*_db, "lot_chips", constraint);
    if (q.status()) {
        fatal("DB query error!\n");
    }

    if (q.begin() == q.end())
        return FALSE;
    else
        return TRUE;
}

void Mpc::doHistoryList(GtkListStore *list)
{
    GtkTreeIter iter;
    guint i = 0;
    
    gtk_list_store_clear(list);

    /* add data to the list store */
    DB_query q(*_db, "history", "", "date DESC");
    if (q.status()) {
        fatal("DB query error!\n");
    }

    for (DB_query::iterator it = q.begin();
         (it != q.end() && i < MAX_LIST_ELEMENTS);
         it++)
    {
        Row r = *it;
        gtk_list_store_append (list, &iter);
        time_t time = atoi(r["date"].c_str());
        char buf[BUF_MAX];
        format_date_time(&time, buf);
        gtk_list_store_set (list, &iter,
                            0, buf,
                            1, r["description"].c_str(),
                            -1);
        i++;
    }
}

gint Mpc::burncd_callback( gpointer data )
{
    Mpc *p = (Mpc *) data;
    GtkProgressBar *bar = p->_progressPage->getBar();
    int status;

    if (waitpid(-1, &status, WNOHANG) == 0) {
        gtk_progress_bar_pulse(bar);
        while(g_main_context_iteration(NULL, FALSE));

        if (p->_progressPage->getCancel()) {
            kill(p->_childpid, SIGTERM);
            wait(&status);
            p->_CdBurnPage->setError("CD Burn canceled");
            p->setPage(p->_CdBurnPage);
            return FALSE;
        }

        return TRUE;
    }

    if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
        /* burn returned OK */
        string tarname = TMPDIR;
        tarname = tarname + "/" + p->reportFileName;
        unlink(tarname.c_str());
        if (p->_db->Commit() == -1) {
            fatal("DB error!\n");
        }
        Mpc::goOk("Generate Report OK", p->_operationsPage);
    } else {
        char buf[1024];
        int fd;
        bzero(buf, sizeof(buf));
        if ((fd = open(CDERRORTXT, O_RDONLY)) != -1) {
            read(fd, buf, sizeof(buf));
            close(fd);
        }
        unlink(CDERRORTXT);
        p->_CdBurnPage->setError(buf);
        p->setPage(p->_CdBurnPage);
    }

    return FALSE;
}

/* return 0 on success, -1 on failure */
void Mpc::doBurnCD()
{
    string tarname = TMPDIR;
    tarname = tarname + "/" + reportFileName;
    _childpid = fork();

    if (_childpid == -1) {
        fatal("fork error!\n");
    } else if (_childpid == 0) { /* child */
        execl(CDBURNPROGRAM, CDBURNPROGRAM, tarname.c_str(), 0);
        fatal("execl error\n");
    }
    
    gtk_timeout_add( 500,
                     burncd_callback,
                     this);
}

void Mpc::cancelReport()
{
    string tarname = TMPDIR;
    tarname = tarname + "/" + reportFileName;

    /* remove export tar file */
    unlink(tarname.c_str());
    if (_db->Rollback() == -1) {
        fatal("cancelReport() error\n");
    }
}

void Mpc::doSoftwareUpdate()
{
    /* create update tag file */
    creat(NEED_UPGRADE, 00644);

    /* shut down */
    exit(0);
}

void Mpc::systemReboot()
{
    gtk_widget_hide_all(_window);
    string command = REBOOTPROGRAM;
    system_call(command);
}

void Mpc::systemShutdown()
{
    gtk_widget_hide_all(_window);
    string command = SHUTDOWNPROGRAM;
    system_call(command);
}

gchar * Mpc::getConf(gchar *name, gchar *buf)
{
    if (sys_getconf(name, buf, BUF_MAX) != 0) {
        *buf = 0;
    }
    return buf;
}

void Mpc::setConf(gchar *name, gchar *buf)
{
    if (sys_setconf(name, buf, 1) != 0) {
        fatal("error setting config-var %s\n", name);
    }
}

static void quit(GtkWidget *, gpointer)
{
    gtk_main_quit();
}

void Mpc::goPage(GtkWidget *w, gpointer *data)
{
    Page *p = (Page *) *data;

    p->_mpc->setPage(p);
}

void Mpc::goOk(gchar * string, gpointer page)
{
    Page *p = (Page *) page;

    p->_mpc->_okPage->setString(string);
    p->_mpc->_okPage->setBackPage(p);
    p->_mpc->setPage(p->_mpc->_okPage);
}

void Mpc::goProgress(string label, Mpc *mpc)
{
    mpc->_progressPage->setLabel(label);
    mpc->setPage(mpc->_progressPage);
}

void Mpc::setPage(Page *page) {
    GtkWidget *old = gtk_bin_get_child(GTK_BIN(_window));
    if (old) {
        gtk_container_remove(GTK_CONTAINER(_window), old);
    }
    if (_currentPage) {
        _currentPage->remove();
    }
    _currentPage = page;
    _currentPage->install();

    gtk_container_add(GTK_CONTAINER(_window), page->page());
}

void Mpc::buildPages()
{
    _loginPage = new LoginPage(this);
    _rebootPage = new RebootPage(this);
    _shutdownPage = new ShutdownPage(this);
    _mainMenuPage = new MainMenuPage(this);
    _operationsPage = new OperationsPage(this);
    _genReportPage = new GenReportPage(this);
    _historyPage = new HistoryPage(this);
    _configPage = new ConfigPage(this);
    _maintenancePage = new MaintenancePage(this);
    _FTPConfigPage = new FTPConfigPage(this);
    _NetConfigPage = new NetConfigPage(this);
    _NTPConfigPage = new NTPConfigPage(this);
    _PasswdConfigPage = new PasswdConfigPage(this);
    _SoftwareUpdatePage = new SoftwareUpdatePage(this);
    _ReviewLotsPage = new ReviewLotsPage(this);
    _okPage = new OkPage(this);
    _progressPage = new ProgressPage(this);
    _CdBurnPage = new CdBurnPage(this);
}

Mpc::Mpc(int argc, char *argv[]) : _currentPage(0)
{
    _db = new DB();
    if (_db->Status()) {
        fatal("DB connection failed!\n");
    }

    gtk_init (&argc, &argv);
    
    _window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_decorated(GTK_WINDOW(_window), FALSE);
    gtk_widget_set_size_request(_window, 800, 600);
    gtk_window_set_position(GTK_WINDOW(_window), GTK_WIN_POS_CENTER);
    gtk_container_set_border_width(GTK_CONTAINER(_window), 50);
    g_signal_connect(G_OBJECT(_window), "destroy", G_CALLBACK(quit), NULL);

    buildPages();
    setPage(_loginPage);

    gtk_timeout_add( 1000,
                     timeout_callback,
                     this);

    gtk_widget_show_all(_window);
}

Mpc::~Mpc() 
{
}

void Mpc::run()
{
    gtk_main();
}


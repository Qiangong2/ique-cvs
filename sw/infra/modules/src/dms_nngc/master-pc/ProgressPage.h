#ifndef _PROGRESSPAGE_H_
#define _PROGRESSPAGE_H_

#include "Page.h"

class ProgressPage : public Page {
    GtkWidget *_bar;
    gboolean _cancel;
    string _label;

public:
    ProgressPage(Mpc *);
    ~ProgressPage() {}

    static void cancel_clicked(GtkWidget *, gpointer);

    void install();
    void remove();

    void setLabel(string);
    GtkProgressBar* getBar();
    inline gboolean getCancel() {return _cancel;}
};

#endif /* _PROGRESSPAGE_H_ */

#ifndef _OKPAGE_H_
#define _OKPAGE_H_

#include "Page.h"

class OkPage : public Page {
    Page *_backPage;
    string _label;

public:
    OkPage(Mpc *);
    ~OkPage() {}

    static void ok_clicked(GtkWidget *, gpointer);

    void install();
    void remove();

    void setString(string);
    void setBackPage(Page *);
};

#endif /* _OKPAGE_H_ */

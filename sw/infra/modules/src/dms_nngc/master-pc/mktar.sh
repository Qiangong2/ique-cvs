#!/bin/bash

bzip=$1
shift
srcdir=$1
shift
destname=$1
shift
files=$@

if [ -z "$bzip" -o \
     -z "$srcdir" -o \
     -z "$destname" -o \
     -z "$files" ]; then
echo "mktar.sh <bzip?> <srcdir> <tar file name> files ..."
exit 1
fi

if [ "$bzip" != "no" ]; then
    options="-c -j -f"
else
    options="-c -f"
fi

cd $srcdir
tar $options $destname $files
rm -rf $files

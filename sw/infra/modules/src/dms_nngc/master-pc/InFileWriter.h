#ifndef _INFILEWRITER_H_
#define _INFILEWRITER_H_

#define CHIPID_SIZE  4
#define CHIP_DATA_LINE_SIZE     16
#define CHIP_DATA_NUM_LINES     24
const int PRIVATE_DATA_SIZE = 3072/8; // 3 Kbits

int writeInFile(char *, int, unsigned int, const unsigned char *);

#endif /* _INFILEWRITER_H_ */

#ifndef _NETCONFIGPAGE_H_
#define _NETCONFIGPAGE_H_

#include "Page.h"

class NetConfigPage : public Page {
    GtkWidget *_errorLabel;
    GtkWidget *_ipEntry[4];
    GtkWidget *_netmaskEntry[4];
    GtkWidget *_gatewayEntry[4];

public:
    NetConfigPage(Mpc *);
    ~NetConfigPage() {}

    static void ok_clicked(GtkWidget *, gpointer);

    void install();
    void remove();
};

#endif /* _NETCONFIGPAGE_H_ */

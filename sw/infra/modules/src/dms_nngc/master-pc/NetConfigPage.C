#include "Mpc.h"
#include "NetConfigPage.h"
#include "ConfigPage.h"

void fillInNetWidgets(gchar *name, GtkWidget **widget_array)
{
    gchar buf[BUF_MAX];
    gchar *p, *q;
    
    Mpc::getConf(name, buf);
    if (!*buf) {
        for (int i=0; i< 4; i++) {
            gtk_entry_set_text (GTK_ENTRY(widget_array[i]), "");
        }
        return;
    }

    p = buf;
    for (int i=0; i<4; i++) {
        if ((q = strchr(p, '.')) != NULL) {
            *q = 0;
        }
        gtk_entry_set_text (GTK_ENTRY(widget_array[i]), p);
        p = q+1;
    }
}

void NetConfigPage::install()
{
    GtkWidget *vbox1;
    GtkWidget *Title;
    GtkWidget *vbox2;
    GtkWidget *hbox51;
    GtkWidget *hbox52;
    GtkWidget *label;
    GtkWidget *hbox41;
    GtkWidget *hbox42;
    GtkWidget *hbox53;
    GtkWidget *button;
    GtkWidget *alignment3;
    GtkWidget *hbox8;
    GtkWidget *image3;
    GtkWidget *label11;
    GtkWidget *vbox9;
    GtkWidget *hbox9;
    GtkWidget *BackButton;
    GtkWidget *LogoutButton;
    GtkWidget *alignment5;
    GtkWidget *hbox11;
    GtkWidget *image5;
    GtkWidget *label13;
    PangoFontDescription	*pfd;

    _page = gtk_vbox_new(FALSE, 0);
    gtk_container_set_border_width(GTK_CONTAINER(_page), 5);

    vbox1 = gtk_vbox_new (FALSE, 10);

    /* start header */
    Title = gtk_label_new (_("Network Config"));
    gtk_box_pack_start (GTK_BOX (vbox1), Title, FALSE, FALSE, 0);
    gtk_widget_set_size_request (Title, -1, HEADER_HEIGHT);
    pfd = pango_font_description_from_string("15");
    gtk_widget_modify_font(GTK_WIDGET(Title), pfd);
    pango_font_description_free(pfd);

    /* start body */
    vbox2 = gtk_vbox_new (FALSE, MENU_SPACING);
    gtk_box_pack_start (GTK_BOX (vbox1), vbox2, TRUE, FALSE, 0);

    _errorLabel = gtk_label_new (NULL);
    gtk_box_pack_start (GTK_BOX (vbox2), _errorLabel, FALSE, FALSE, 0);

    hbox51 = gtk_hbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vbox2), hbox51, FALSE, FALSE, 0);

    hbox52 = gtk_hbox_new (FALSE, 0);
    gtk_box_pack_start (GTK_BOX (hbox51), hbox52, FALSE, FALSE, 0);

    label = gtk_label_new (_("IP Address:"));
    gtk_box_pack_start (GTK_BOX (hbox52), label, FALSE, FALSE, 0);
    gtk_widget_set_size_request (label, LABEL_WIDTH, -1);

    _ipEntry[0] = gtk_entry_new ();
    gtk_box_pack_start (GTK_BOX (hbox52), _ipEntry[0], FALSE, FALSE, 0);
    gtk_entry_set_width_chars (GTK_ENTRY (_ipEntry[0]), 3);
    gtk_entry_set_max_length (GTK_ENTRY (_ipEntry[0]), 3);
    label = gtk_label_new (_(" . "));
    gtk_box_pack_start (GTK_BOX (hbox52), label, FALSE, FALSE, 0);
    _ipEntry[1] = gtk_entry_new ();
    gtk_box_pack_start (GTK_BOX (hbox52), _ipEntry[1], FALSE, FALSE, 0);
    gtk_entry_set_width_chars (GTK_ENTRY (_ipEntry[1]), 3);
    gtk_entry_set_max_length (GTK_ENTRY (_ipEntry[1]), 3);
    label = gtk_label_new (_(" . "));
    gtk_box_pack_start (GTK_BOX (hbox52), label, FALSE, FALSE, 0);
    _ipEntry[2] = gtk_entry_new ();
    gtk_box_pack_start (GTK_BOX (hbox52), _ipEntry[2], FALSE, FALSE, 0);
    gtk_entry_set_width_chars (GTK_ENTRY (_ipEntry[2]), 3);
    gtk_entry_set_max_length (GTK_ENTRY (_ipEntry[2]), 3);
    label = gtk_label_new (_(" . "));
    gtk_box_pack_start (GTK_BOX (hbox52), label, FALSE, FALSE, 0);
    _ipEntry[3] = gtk_entry_new ();
    gtk_box_pack_start (GTK_BOX (hbox52), _ipEntry[3], FALSE, FALSE, 0);
    gtk_entry_set_width_chars (GTK_ENTRY (_ipEntry[3]), 3);
    gtk_entry_set_max_length (GTK_ENTRY (_ipEntry[3]), 3);

    fillInNetWidgets("MPC_IP", _ipEntry);

    hbox41 = gtk_hbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vbox2), hbox41, FALSE, FALSE, 0);

    hbox42 = gtk_hbox_new (FALSE, 0);
    gtk_box_pack_start (GTK_BOX (hbox41), hbox42, FALSE, FALSE, 0);

    label = gtk_label_new (_("Netmask:"));
    gtk_box_pack_start (GTK_BOX (hbox42), label, FALSE, FALSE, 0);
    gtk_widget_set_size_request (label, LABEL_WIDTH, -1);

    _netmaskEntry[0] = gtk_entry_new ();
    gtk_box_pack_start (GTK_BOX (hbox42), _netmaskEntry[0], FALSE, FALSE, 0);
    gtk_entry_set_width_chars (GTK_ENTRY (_netmaskEntry[0]), 3);
    gtk_entry_set_max_length (GTK_ENTRY (_netmaskEntry[0]), 3);
    label = gtk_label_new (_(" . "));
    gtk_box_pack_start (GTK_BOX (hbox42), label, FALSE, FALSE, 0);
    _netmaskEntry[1] = gtk_entry_new ();
    gtk_box_pack_start (GTK_BOX (hbox42), _netmaskEntry[1], FALSE, FALSE, 0);
    gtk_entry_set_width_chars (GTK_ENTRY (_netmaskEntry[1]), 3);
    gtk_entry_set_max_length (GTK_ENTRY (_netmaskEntry[1]), 3);
    label = gtk_label_new (_(" . "));
    gtk_box_pack_start (GTK_BOX (hbox42), label, FALSE, FALSE, 0);
    _netmaskEntry[2] = gtk_entry_new ();
    gtk_box_pack_start (GTK_BOX (hbox42), _netmaskEntry[2], FALSE, FALSE, 0);
    gtk_entry_set_width_chars (GTK_ENTRY (_netmaskEntry[2]), 3);
    gtk_entry_set_max_length (GTK_ENTRY (_netmaskEntry[2]), 3);
    label = gtk_label_new (_(" . "));
    gtk_box_pack_start (GTK_BOX (hbox42), label, FALSE, FALSE, 0);
    _netmaskEntry[3] = gtk_entry_new ();
    gtk_box_pack_start (GTK_BOX (hbox42), _netmaskEntry[3], FALSE, FALSE, 0);
    gtk_entry_set_width_chars (GTK_ENTRY (_netmaskEntry[3]), 3);
    gtk_entry_set_max_length (GTK_ENTRY (_netmaskEntry[3]), 3);

    fillInNetWidgets("MPC_NETMASK", _netmaskEntry);

    hbox51 = gtk_hbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vbox2), hbox51, FALSE, FALSE, 0);

    hbox52 = gtk_hbox_new (FALSE, 0);
    gtk_box_pack_start (GTK_BOX (hbox51), hbox52, FALSE, FALSE, 0);

    label = gtk_label_new (_("Default Gateway:"));
    gtk_box_pack_start (GTK_BOX (hbox52), label, FALSE, FALSE, 0);
    gtk_widget_set_size_request (label, LABEL_WIDTH, -1);

    _gatewayEntry[0] = gtk_entry_new ();
    gtk_box_pack_start (GTK_BOX (hbox52), _gatewayEntry[0], FALSE, FALSE, 0);
    gtk_entry_set_width_chars (GTK_ENTRY (_gatewayEntry[0]), 3);
    gtk_entry_set_max_length (GTK_ENTRY (_gatewayEntry[0]), 3);
    label = gtk_label_new (_(" . "));
    gtk_box_pack_start (GTK_BOX (hbox52), label, FALSE, FALSE, 0);
    _gatewayEntry[1] = gtk_entry_new ();
    gtk_box_pack_start (GTK_BOX (hbox52), _gatewayEntry[1], FALSE, FALSE, 0);
    gtk_entry_set_width_chars (GTK_ENTRY (_gatewayEntry[1]), 3);
    gtk_entry_set_max_length (GTK_ENTRY (_gatewayEntry[1]), 3);
    label = gtk_label_new (_(" . "));
    gtk_box_pack_start (GTK_BOX (hbox52), label, FALSE, FALSE, 0);
    _gatewayEntry[2] = gtk_entry_new ();
    gtk_box_pack_start (GTK_BOX (hbox52), _gatewayEntry[2], FALSE, FALSE, 0);
    gtk_entry_set_width_chars (GTK_ENTRY (_gatewayEntry[2]), 3);
    gtk_entry_set_max_length (GTK_ENTRY (_gatewayEntry[2]), 3);
    label = gtk_label_new (_(" . "));
    gtk_box_pack_start (GTK_BOX (hbox52), label, FALSE, FALSE, 0);
    _gatewayEntry[3] = gtk_entry_new ();
    gtk_box_pack_start (GTK_BOX (hbox52), _gatewayEntry[3], FALSE, FALSE, 0);
    gtk_entry_set_width_chars (GTK_ENTRY (_gatewayEntry[3]), 3);
    gtk_entry_set_max_length (GTK_ENTRY (_gatewayEntry[3]), 3);

    fillInNetWidgets("MPC_GATEWAY", _gatewayEntry);

    hbox53 = gtk_hbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vbox2), hbox53, FALSE, FALSE, 0);

    button = gtk_button_new ();
    gtk_box_pack_start (GTK_BOX (hbox53), button, FALSE, FALSE, 0);

    alignment3 = gtk_alignment_new (0.5, 0.5, 0, 0);
    gtk_container_add (GTK_CONTAINER (button), alignment3);

    hbox8 = gtk_hbox_new (FALSE, 2);
    gtk_container_add (GTK_CONTAINER (alignment3), hbox8);

    image3 = gtk_image_new_from_stock ("gtk-yes", GTK_ICON_SIZE_BUTTON);
    gtk_box_pack_start (GTK_BOX (hbox8), image3, FALSE, FALSE, 0);

    label11 = gtk_label_new ("OK");
    gtk_box_pack_start (GTK_BOX (hbox8), label11, FALSE, FALSE, 0);
    gtk_label_set_justify (GTK_LABEL (label11), GTK_JUSTIFY_LEFT);
    g_signal_connect(G_OBJECT(button), "clicked",
                     G_CALLBACK(ok_clicked), this);
  
    /* start footer */
    vbox9 = gtk_vbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vbox1), vbox9, FALSE, FALSE, 0);
    gtk_widget_set_size_request (vbox9, -1, FOOTER_HEIGHT);

    hbox9 = gtk_hbox_new (TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vbox9), hbox9, FALSE, FALSE, 0);

    BackButton = gtk_button_new ();
    gtk_box_pack_start (GTK_BOX (hbox9), BackButton, FALSE, FALSE, 0);
    g_signal_connect ((gpointer) BackButton, "clicked",
                      G_CALLBACK(Mpc::goPage), &_mpc->_configPage);

    alignment5 = gtk_alignment_new (0.5, 0.5, 0, 0);
    gtk_container_add (GTK_CONTAINER (BackButton), alignment5);

    hbox11 = gtk_hbox_new (FALSE, 2);
    gtk_container_add (GTK_CONTAINER (alignment5), hbox11);

    image5 = gtk_image_new_from_stock ("gtk-go-back", GTK_ICON_SIZE_BUTTON);
    gtk_box_pack_start (GTK_BOX (hbox11), image5, FALSE, FALSE, 0);

    label13 = gtk_label_new_with_mnemonic (_("Back"));
    gtk_box_pack_start (GTK_BOX (hbox11), label13, FALSE, FALSE, 0);

    LogoutButton = gtk_button_new ();
    gtk_box_pack_start (GTK_BOX (hbox9), LogoutButton, FALSE, FALSE, 0);
    g_signal_connect ((gpointer) LogoutButton, "clicked",
                      G_CALLBACK(Mpc::goPage), &_mpc->_loginPage);

    alignment5 = gtk_alignment_new (0.5, 0.5, 0, 0);
    gtk_container_add (GTK_CONTAINER (LogoutButton), alignment5);

    hbox11 = gtk_hbox_new (FALSE, 2);
    gtk_container_add (GTK_CONTAINER (alignment5), hbox11);

    image5 = gtk_image_new_from_stock ("gtk-quit", GTK_ICON_SIZE_BUTTON);
    gtk_box_pack_start (GTK_BOX (hbox11), image5, FALSE, FALSE, 0);

    label13 = gtk_label_new_with_mnemonic (_("Log Out"));
    gtk_box_pack_start (GTK_BOX (hbox11), label13, FALSE, FALSE, 0);

    gtk_box_pack_start(GTK_BOX(_page), vbox1, TRUE, TRUE, 0);

    gtk_widget_show_all(_page);
}

void NetConfigPage::remove()
{
}

void NetConfigPage::ok_clicked(GtkWidget *w, gpointer data)
{
    NetConfigPage *p = (NetConfigPage *) data;
    string ip = "";
    string netmask = "";
    string gateway = "";

    for (int i=0; i<4; i++) {
        gchararray tmp = GTK_ENTRY(p->_ipEntry[i])->text;

        if (strlen(tmp) == 0) {
            gtk_label_set_markup(GTK_LABEL(p->_errorLabel), "<span foreground=\"red\">Missing IP Address</span>");
            return;
        }

        if (!is_number(tmp)) {
            gtk_label_set_markup(GTK_LABEL(p->_errorLabel), "<span foreground=\"red\">Invalid IP Address</span>");
            return;
        }

        guint num = atoi(tmp);
        
        if (num >= 255 || num < 0) {
            gtk_label_set_markup(GTK_LABEL(p->_errorLabel), "<span foreground=\"red\">Invalid IP Address</span>");
            return;
        }

        if (i == 3 && num == 0) {
            gtk_label_set_markup(GTK_LABEL(p->_errorLabel), "<span foreground=\"red\">Invalid IP Address</span>");
            return;
        }
        
        ip += tmp;
        if (i<3) ip += ".";
    }

    for (int i=0; i<4; i++) {
        gchararray tmp = GTK_ENTRY(p->_netmaskEntry[i])->text;

        if (strlen(tmp) == 0) {
            gtk_label_set_markup(GTK_LABEL(p->_errorLabel), "<span foreground=\"red\">Missing Netmask</span>");
            return;
        }

        if (!is_number(tmp)) {
            gtk_label_set_markup(GTK_LABEL(p->_errorLabel), "<span foreground=\"red\">Invalid Netmask</span>");
            return;
        }

        guint num = atoi(tmp);
        
        if (num > 255 || num < 0) {
            gtk_label_set_markup(GTK_LABEL(p->_errorLabel), "<span foreground=\"red\">Invalid Netmask</span>");
            return;
        }
        
        netmask += tmp;
        if (i<3) netmask += ".";
    }

    for (int i=0; i<4; i++) {
        gchararray tmp = GTK_ENTRY(p->_gatewayEntry[i])->text;

        if (strlen(tmp) == 0) {
            gtk_label_set_markup(GTK_LABEL(p->_errorLabel), "<span foreground=\"red\">Missing Default Gateway</span>");
            return;
        }

        if (!is_number(tmp)) {
            gtk_label_set_markup(GTK_LABEL(p->_errorLabel), "<span foreground=\"red\">Invalid Default Gateway</span>");
            return;
        }

        guint num = atoi(tmp);
        
        if (num >= 255 || num < 0) {
            gtk_label_set_markup(GTK_LABEL(p->_errorLabel), "<span foreground=\"red\">Invalid Default Gateway</span>");
            return;
        }

        if (i == 3 && num == 0) {
            gtk_label_set_markup(GTK_LABEL(p->_errorLabel), "<span foreground=\"red\">Invalid Default Gateway</span>");
            return;
        }
        
        gateway += tmp;
        if (i<3) gateway += ".";
    }

    Mpc::setConf("MPC_IP", (char*)ip.c_str());
    Mpc::setConf("MPC_NETMASK", (char*)netmask.c_str());
    Mpc::setConf("MPC_GATEWAY", (char*)gateway.c_str());

    string command = NETWORKPROGRAM;
    command += " > /dev/null 2>&1";
    int ret = system(command.c_str());
    if (ret == -1 || WEXITSTATUS(ret) != 0) {
        fatal("error on system(): %s\n", command.c_str());
    }

    Mpc::goOk("Net Config Set", p->_mpc->_configPage);
}

NetConfigPage::NetConfigPage(Mpc *p) : Page(p)
{
}

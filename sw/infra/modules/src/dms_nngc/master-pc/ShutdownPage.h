#ifndef _SHUTDOWNPAGE_H_
#define _SHUTDOWNPAGE_H_

#include "Page.h"

class ShutdownPage : public Page {
public:
    ShutdownPage(Mpc *);
    ~ShutdownPage() {}

    static void ok_clicked(GtkWidget *, gpointer);

    void install();
    void remove();
};

#endif /* _SHUTDOWNPAGE_H_ */

#include <stdio.h>
#include "db.h"
#include "Mpc.h"
#include "Misc.h"
#include "XmlWriter.h"

#define BB_ID_TAG "BB_ID"

FILE* openTableXML(const string& file)
{
    FILE *fp;

    fp = fopen(file.c_str(), "w");
    if (fp == NULL) {
        return NULL;
    }

    fprintf(fp, "<ROWSET>\n");

    return fp;
}

int closeTableXML(FILE *fp)
{
    if (fp == NULL) return -1;

    fprintf(fp, "</ROWSET>\n");

    fclose(fp);

    return 0;
}

int dumpTableXML(FILE *fp, 
                 DB *db,
                 const string& table,
                 const string& constraints)
{
    DB_query q(*db, table, constraints);
    int i=0;
    char buf[BUF_MAX];
    unsigned long long device_type_prefix = 0;

    Mpc::getConf(MPC_DEVICE_TYPE, buf);
    device_type_prefix = atoll(buf) << 32; /* prefix * 2^32 */

    for (DB_query::iterator it = q.begin();
	 it != q.end();
	 it++) {
	Row r = *it;
        fprintf(fp, "<ROW num=\"%d\">\n", i++);
	
        for (Row::const_iterator it_r = r.begin();
             it_r != r.end();
             it_r++) {
            char first[(*it_r).first.size()+1];
            strcpy(first, (*it_r).first.c_str());
            uppercase_string(first);
            if ((*it_r).first == "create_date" ||
                (*it_r).first == "last_processed" ||
                (*it_r).first == "last_reported" ||
                (*it_r).first == "process_date") {
                char buf[BUF_MAX];
                time_t time = atoi((*it_r).second.c_str());
                format_XML_time(&time, buf);
                fprintf(fp, "<%s>%s</%s>\n", first,
                        buf, first);
            } else if ((*it_r).first == "chip_id") {
                unsigned long long id = atoi((*it_r).second.c_str());
                id += device_type_prefix;
                fprintf(fp, "<%s>%llu</%s>\n", BB_ID_TAG,
                        id, BB_ID_TAG);
            } else {
                fprintf(fp, "<%s>%s</%s>\n", first,
                        (*it_r).second.c_str(), first);
            }
        }
        
        fprintf(fp, "</ROW>\n");
    }

    return 0;
}


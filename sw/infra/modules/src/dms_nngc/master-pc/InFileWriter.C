#include <stdio.h>
#include "Mpc.h"
#include "InFileWriter.h"

int writeInFile(char *path, int index, unsigned int chip_id, const unsigned char *priv_data)
{
    FILE *fp;
    char filename[MPC_PATH_MAX];
    int i, j;
    const unsigned char *p;

    sprintf(filename, "%s/%03d.id", path, index);

    fp = fopen(filename, "w");
    if (fp == NULL) {
        return -1;
    }
    
    fprintf(fp, "%08x\n", chip_id);

    p = priv_data;
    for (i=0; i<CHIP_DATA_NUM_LINES; i++) {
        for (j=0; j<CHIP_DATA_LINE_SIZE; j++) {
            fprintf(fp, "%02x", *p);
            p++;
        }
        fprintf(fp, "\n");
    }

    fclose(fp);

    return 0;
}


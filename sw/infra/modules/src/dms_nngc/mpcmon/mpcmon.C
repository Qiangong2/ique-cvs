//  Master-PC monitor program

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/vfs.h>
#include <dirent.h> 
#include <signal.h>

#include <set>

using namespace std;

#include "config_var.h"
#include "gos.h"
#include "common.h"


#define SLEEP_INTERVAL 60

typedef struct {
    const char  *taskname;
    int        (*func)();
    uid_t       uid;
} task_t;


static pid_t mpc_pid = 0;

static int get_stat();
static int vacuum_database();
static int monitor_processes();
static int update_status_bar();
static int monitor_nfast();
static int monitor_raid();


task_t daily_tasks[] = {
    { "vacuum database", vacuum_database },
    { "get statistics", get_stat },
    { 0, 0 }
};


task_t hourly_tasks[] = {
    { 0, 0 }
};


task_t frequent_tasks[] = {
    { "monitor daemons", monitor_processes },
    { "update status bar", update_status_bar }, 
    { "monitor nfast", monitor_nfast },
    { "monitor raid", monitor_raid },
    { 0, 0 }
};


int get_stat()
{
    char buf[1024];
    snprintf(buf, sizeof(buf), "(date; df; echo) >> %s/statistics.log\n", LOGDIR);
    return system(buf);
}

int vacuum_database()
{
    char buf[1024];
    snprintf(buf, sizeof(buf), "(date; vacuumdb --username postgres --dbname %s; echo) >> %s/vacuumdb.log\n", DBNAME, LOGDIR);
    return system(buf);
}


int check_processes()
{
    static char *monitorlist[] = {
	"mpcmon",
	"postmaster",
	"mpc",
	0
    };

    set<string> daemons;
    DIR *proc;
    int mpc_count=0;

    if((proc = opendir("/proc")) == NULL) {
	msglog(MSG_ALERT, "mpcmon: can't open /proc\n");
	return -1;
    }

    /* find entries of the form /proc/[0-9]* */
    struct dirent *dir;
    while((dir = readdir(proc))) {
	char status[128], name[128], buf[256];
	FILE *fd;
	int uid;
	if (!isdigit(dir->d_name[0])) continue;

	sprintf(status, "/proc/%s/status", dir->d_name);
	if((fd = fopen(status, "r")) == NULL) continue;

	/* extract the Name field */
	name[0] = '\0'; 
	int zombie = 0;
	while (fgets(buf, sizeof buf, fd)) {
	    if (strncmp(buf, "Name:", 5) == 0) {
	    	sscanf(buf, "Name: %s", name);
	    } else if (strncmp(buf, "Uid:", 4) == 0) {
	    	sscanf(buf, "Uid: %d", &uid);
		break;
	    } else if (strstr(buf, "zombie") != 0) {
		zombie = 1;
	    }
	}
	fclose(fd);
	if (!zombie)
	    daemons.insert(name);
	if (strcmp(name, "mpc") == 0)
	    mpc_count++;
    }
    closedir(proc);

    for (int i = 0; monitorlist[i] != NULL; i++) {
	char *p = monitorlist[i];
	if (daemons.find(p) == daemons.end()) {
	    msglog(MSG_ALERT, "mpcmon: can't find %s\n", p);
	    return -1;
	}
    }

    if (mpc_count > 1) {
	msglog(MSG_ALERT, "mpcmon: mpcmon finds %d mpc processes\n", 
	       mpc_count);
	return -1;
    }
    return 0;
}


int check_nfast()
{
#ifdef NOHSM
    return 0;
#else
    const char *tmpfile = "/tmp/mpcmon.out";
    char buf[4096];
    char cmd[1024];
    unlink(tmpfile);
    for (int modules = 0; modules < 2; modules++) {
	snprintf(cmd, sizeof(cmd), 
		 "/opt/nfast/bin/enquiry %d > /tmp/mpcmon.out 2>&1",
		 modules);
	system(cmd);
	int fd = open(tmpfile, O_RDONLY);
	int ok = 0;
	if (fd >= 0) {
	    int n = read(fd, buf, sizeof(buf)-1);
	    if (n > 0) {
		buf[n] = '\0';
		if (strstr(buf, "mode") && strstr(buf, "operational"))
		    ok = 1;
	    }
	    close(fd);
	}
	unlink(tmpfile);
	if (ok == 0) 
	    return -1;
    }
    return 0;
#endif
}


int check_raid()
{
    int status = system(MPCRAIDPROGRAM);
    status = WEXITSTATUS(status);
    if (status != 0)
	return -1;
    else
	return 0;
}


int monitor_processes()
{
    int res = check_processes();
    if (res != 0) {
	msglog(MSG_ALERT, "mpcmon: missing daemon - reboot\n");
	sys_reboot();
    }
    return 0;
}


int update_status_bar()
{
    struct statfs buf;
    if (statfs("/opt/broadon", &buf) != 0) {
	msglog(MSG_ALERT, "mpcmon: can't access /opt/broadon directory\n");
	sys_reboot();
    }
    char version[16] = { 0 };
    {
        int ret;
        char command[1024];
        char* tmpfile = "/tmp/mpcver";
        sprintf(command, "%s | grep 'major_version' | sed 's/.*=//' > %s", GETVERSION, tmpfile);
        ret = system(command);
        if (ret != -1 || WEXITSTATUS(ret) == 0) {
            int fd = open(tmpfile, O_RDONLY);
            if (fd != 0) {
                int i;
                i = read(fd, version, sizeof(version));
                if (i > 0) {
                    version[i-1] = 0;
                }
            }
        }
        if (version[0] == 0) {
            strcpy(version, VERSION);
        }
    }
    int percentfree = (int) (buf.f_bavail * 100.0 / buf.f_blocks);
    FILE *fp = fopen(STATUSBAR, "w");
    if (fp) {
        // print % used
        fprintf(fp, "ID Creator v.%s - disk usage: %d%%\n",
                version, 100 - percentfree);
	fclose(fp);
    }
    if (percentfree < 5) {
	FILE *fp = fopen(HARDERROR, "w");
	if (fp) {
	    fprintf(fp, "Disk is > 95%% full!");
	    fclose(fp);
	}
    }
    return 0;
}


int monitor_nfast()
{
    int res = check_nfast();
    if (res != 0) {
	msglog(MSG_ALERT, "mpcmon: nfast failed - reboot\n");
	sys_reboot();
    }
    return 0;
}


int monitor_raid()
{
    int res = check_raid();
    if (res != 0) {
	msglog(MSG_ALERT, "mpcmon: raid failed - reboot\n");
	sys_reboot();
    }
    return 0;
}


void execute_tasks(task_t *task_list, bool waitchild=false) 
{
    for (task_t *t = task_list;
	 t->taskname != 0;
	 t++) {
	pid_t pid;
	msglog(MSG_INFO, "mpcmon: execute task %s\n", t->taskname);
	if ((pid = fork()) > 0) {
	    if (waitchild)
		waitpid(pid, 0, 0);
	} else if (pid == 0) {
	    if (t->uid)
		setuid(t->uid);
	    exit((*(t->func))());
	}
    }
}


void main_loop()
{
    while (1) {
	char buf[1024];
	time_t now = time(NULL);

	time_t hourly_schedule = 0;
	if (sys_getconf(CONFIG_MON_HOURLY, buf, sizeof(buf)) == 0) 
	    hourly_schedule = atoi(buf);
	if (now >= hourly_schedule) {
	    snprintf(buf, sizeof(buf), "%ld", now + 3600);
	    sys_setconf(CONFIG_MON_HOURLY, buf, 1);
	    execute_tasks(hourly_tasks);
	}

	time_t daily_schedule = 0;
	if (sys_getconf(CONFIG_MON_DAILY, buf, sizeof(buf)) == 0) 
	    daily_schedule = atoi(buf);
	if (now >= daily_schedule) {
	    snprintf(buf, sizeof(buf), "%ld", now + 3600 * 24);
	    sys_setconf(CONFIG_MON_DAILY, buf, 1);
	    execute_tasks(daily_tasks);
	}

	execute_tasks(frequent_tasks, true);

	// clear zombie
	while (waitpid(0, 0, WNOHANG) > 0);
	
	for (int i = 0; i < SLEEP_INTERVAL; i++) {
	    sleep(1);
	    struct stat sbuf;
	    if (stat(NEED_UPGRADE, &sbuf) == 0) {
		pid_t pid = fork();
		if (pid == 0) {
		    msglog(MSG_INFO, "mpcmon: execute %s\n", UPGRADEPROGRAM);
		    unlink(NEED_UPGRADE);
		    setpgrp();
		    execl(UPGRADEPROGRAM, "mpc_upgrade", NULL);
		    msglog(MSG_ALERT, "mpcmon:  can't execute %s\n", UPGRADEPROGRAM);
		    msglog(MSG_ALERT, "mpcmon:  detected misconfiguration - exit to console\n");
		} else if (pid < 0) {
		    msglog(MSG_ALERT, "mpcmon:  can't fork %s\n", UPGRADEPROGRAM);
		}
		waitpid(pid, 0, 0);
	    }
	}
    }
}


void terminate(int signum)
{
    msglog(MSG_INFO, "mpcmon: kill mpc %d with signal %d\n", mpc_pid, signum);
    kill(mpc_pid, signum);
    exit(0);
}


// Exit this program if the initial check failed
int initial_chk()
{
    // clear zombie
    while (waitpid(0, 0, WNOHANG) > 0);

    int err = 0;
    pid_t pid = fork();
    if (pid < 0) {
	return -1;
    } else if (pid == 0) {
	execl(MPCPROGRAM, "mpc", NULL);
	msglog(MSG_ALERT, "mpcmon:  can't execute %s\n", MPCPROGRAM);
	return -1;
    }
    mpc_pid = pid;
    signal(SIGTERM, terminate);
    signal(SIGINT,  terminate);
    if (err == 0 && check_raid() != 0) {
	err++;
	FILE *fp = fopen(HARDERROR, "w");
	if (fp) {
	    fprintf(fp, "RAID disk has failed!\n");
	    fclose(fp);
	}
    }
    if (err == 0) {
	int noerror = 0;
	for (int i=0; i<5; i++) {
	    if (check_processes() == 0) {
		noerror = 1;
		break;
	    }
	    sleep(5);
	}
	if (!noerror) {
	    err++;
	    FILE *fp = fopen(HARDERROR, "w");
	    if (fp) {
		fprintf(fp, "The application, the database, or the security process is not responding!\n");
		fclose(fp);
	    }
	}
    }
    if (err == 0 && check_nfast() != 0) {
	err++;
	FILE *fp = fopen(HARDERROR, "w");
	if (fp) {
	    fprintf(fp, "Hardware Security Module is not working!\n");
	    fclose(fp);
	}
    }
    return err;
}


int main()
{
    unlink(HARDERROR);
    while (1) {
	struct stat sbuf;
	if (stat(HARDERROR, &sbuf) == 0) {
	    msglog(MSG_ALERT, "mpcmon: hard error detected\n");
	    break;
	}
	if (initial_chk() == 0)
	    break;
	msglog(MSG_INFO, "mpcmon: failed - retry in 5 minutes\n");
	sleep(5*60);
    }

    main_loop();
    return 0;
}

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "common.h"
#include "config_var.h"
#include "db.h"
#include "schema.h"


// DB static initializiaton
const char * const DB::login = DB_USERLOGIN;
const char * const DB::sulogin = DB_SULOGIN;


DB::DB()
{
    char buf[128];
    verbose = false;
    if (sys_getconf(CONFIG_DB_VERBOSE, buf, sizeof(buf)) == 0) {
	if (atoi(buf) != 0)
	    verbose = true;
    }
    db = new PgDatabase(login);
    status = PgDB()->ConnectionBad();
    if (status) {
	msglog(MSG_ERR, "DB::DB: Connection to database failed - %s\n",
	       PgDB()->ErrorMessage());
    }

    // Initialize the table iterators 
    _tbl_begin = getTable(0);
    _tbl_end = getTable(numTables());
}

DB::~DB()
{
    delete db;
}


int DB::Create()
{
    // Create PostgreSQL tables
    string cmd;
    bool first;

    if (Begin() != 0) {
	msglog(MSG_ERR,
	       "DB::Create: Failed to execute BEGIN for DB::Create\n");
	exit(1);
    }

    for (int i = 0; i < numTables(); i++) {
	struct table_def *pt = getTable(i);
	if (verbose)
	    msglog(MSG_INFO, "--- %s ---\n", pt->name);

	if (pt->flags & TABLE) {
	    cmd = "CREATE TABLE ";
	    cmd += pt->name;
	    cmd += " ( ";
	    first = true;
	    for (const struct col_def *col = pt->cols; col->name != NULL; col++) {
		if (verbose)
		    msglog(MSG_INFO, "%s\t%s\n", col->name, col->type);
		if (!first)
		    cmd += ", ";
		first = false;
		cmd += col->name;
		cmd += " ";
		cmd += col->type;
	    }
	    if (pt->constraints && strlen(pt->constraints) > 0) {
		cmd += ", ";
		cmd += pt->constraints;
	    }
	    cmd += ")";

	    if (verbose)
		msglog(MSG_INFO, "DB::Create: Exec SQL: %s\n", cmd.c_str());

	    if (db->Exec(cmd.c_str()) != PGRES_COMMAND_OK) {
		msglog(MSG_ERR, "DB::Create: Failed to execute %s\n", cmd.c_str());
		exit(1);
	    }
	}

	if (pt->flags & SEQUENCE) {
	    DB_sequence seq(*this, pt->name);
	    seq.create(1, 1, pt->constraints);
	}
    }

    if (Commit() != 0) {
	msglog(MSG_ERR, "DB::Create: Failed to execute COMMIT for %s\n", cmd.c_str());
	exit(1);
    }

    return 0;
}


int DB::CreateIfEmpty()
{
    // TODO:  check if the database is empty first 
    Create();
}


int DB::Destroy()
{
    int err = 0;
    string cmd;
    
    for (int i = 0; i < numTables(); i++) {
	struct table_def *pt = getTable(i);
	if (verbose)
	    msglog(MSG_INFO, "--- %s ---\n", pt->name);

	if (pt->flags & TABLE) {
	    cmd = "DROP TABLE ";
	    cmd += pt->name;
	    
	    if (db->Exec(cmd.c_str()) != PGRES_COMMAND_OK) {
		msglog(MSG_ERR, "DB::Destroy: Failed to execute %s\n", cmd.c_str());
		err = 1;
	    }
	}
	if (pt->flags & SEQUENCE) {
	    DB_sequence seq(*this, pt->name);
	    seq.destroy();
	}
    }

#ifdef DB_USE_BLOB

    /* Delete the objects in pg_largeobject */
    cmd = "SELECT DISTINCT loid FROM pg_largeobject";
    
    if (Exec(cmd.c_str()) != PGRES_TUPLES_OK || Tuples() < 1)
	return -1;

    for (int i = 0; i < Tuples(); i++) {
	int loid = atoi(GetValue(i,0));
	{
	    PgLargeObject lodb(loid, Login());
	    lodb.Unlink();
	}
    }

#endif

    if (err) 
	return -1;
    
    return 0;
}


int DB::Begin()
{
    if (db->Exec("BEGIN") != PGRES_COMMAND_OK) 
	return -1;
    return 0;
}


int DB::Commit()
{
    if (db->Exec("COMMIT") != PGRES_COMMAND_OK) 
	return -1;
    return 0;
}


int DB::Rollback()
{
    if (db->Exec("ROLLBACK") != PGRES_COMMAND_OK) 
	return -1;
    return 0;
}


int DB::Exec(const char *cmd)
{
    return db->Exec(cmd);
}


int DB::Exec(const string& cmd)
{
    return db->Exec(cmd.c_str());
}


int DB::Vacuum()
{
    PgDatabase sudb(sulogin);
    status = sudb.ConnectionBad();
    if (status) {
	msglog(MSG_ERR, "DB::DB: Connection to database failed - %s\n",
	       sudb.ErrorMessage());
	return -1;
    }
    if (sudb.Exec("VACUUM") != PGRES_COMMAND_OK) 
	return -1;
    return 0;
}


void dumpRow(Row& r)
{
    for (Row::const_iterator it = r.begin();
	 it != r.end();
	 it++) {
	cout << (*it).first << " --> " << (*it).second << endl;
    }
}

void dumpRow(char *buf, int size, Row& r)
{
    char *p = buf;
    int n = size;
    for (Row::iterator it = r.begin();
	 it != r.end();
	 it++) {
	int len;
	len = snprintf(p, n, "%s --> %s\n", 
		       (*it).first.c_str(), 
		       (*it).second.c_str());
	p += len; n -= len;
    }
    *(buf+size-1) = '\0';
}


#if 0
/* Convert ' to \' */
const char *string_literal(const char *p)
{
    static char *buf;
    if (buf) 
	free(buf);
    buf = (char *)malloc(strlen(p) * 2 + 10);
    if (buf == NULL) {
	msglog(MSG_ALERT, "string_literal:  run out of memory\n");
	exit(1);
    }
    char *q = buf;
    while (*p != '\0') {
	if (*p == '\'')
	    *q++ = '\\';
	*q++ = *p++;
    }
    *q = '\0';
    return buf;
}
#endif


string string_literal(const string& s)
{
    string outstr;
    back_insert_iterator<string> q(outstr);
    for (string::const_iterator p = s.begin(); p != s.end(); p++) {
	if (*p == '\'')
	    *q++ = '\\';
	*q++ = *p;
    }
    return outstr;
}


int Table::Bind(string tblname)
{
    // Fast path - Table already bound to the correct entry
    if (_name == tblname)
	return 0;

    _name = "";
    _cols_begin = _cols_end = NULL;

    // Found the corr table
    for (int i = 0; i < numTables(); i++) {
	struct table_def *pt = getTable(i);
	if (pt->flags & TABLE ||
	    pt->flags & VIEW) {
	    if (strcmp(pt->name, tblname.c_str()) == 0) {
		_name = tblname;
		_attr = pt->flags;
		if (pt->flags & VIEW)
		    _view = pt->constraints;
		_cols_begin = pt->cols;
		const_iterator it = begin(); 
		while ((*it).name != NULL) 
		    it++;
		_cols_end = it;
		return 0;
	    }
	}
    }

    return -1;
}


int Table::Insert(DB& db, Row& row, bool overwrite)
{
    string cmd;
    bool found_record = false;

    // If a record with same key exists, then overwrite it.
    if (overwrite) {
	cmd = "SELECT * FROM ";
	cmd += Name();
	cmd += " WHERE";
	bool first = true;
	int primary_key_count = 0;
	for (const_iterator col = begin(); col != end(); col++) {
	    Row::const_iterator it = row.find(col->name);
	    if (it != row.end() &&
		(col->flags & PRIMARY_KEY)) {
		cmd += first ? " " : "AND ";
		cmd += col->name;
		cmd += "='";
		cmd += string_literal((*it).second);
		cmd += "'";
		first = false;
		primary_key_count++;
	    }
	}

	if (primary_key_count > 0)
	    found_record = (db.Exec(cmd.c_str()) == PGRES_TUPLES_OK) &&
		db.Tuples() >= 1;
    }

    if (overwrite && found_record)
	return Update(db, row);

    cmd = "INSERT INTO ";
    cmd += Name();
    bool first = true;
    for (const_iterator col = begin(); col != end(); col++) {
	if (row.find(col->name) != row.end()) {
	    cmd += first ? "(" : ", ";
	    cmd += col->name;
	    first = false;
	}
    }
    cmd += ") VALUES ";
    first = true;
    for (const_iterator col = begin(); col != end(); col++) {
	if (row.find(col->name) != row.end()) {
	    cmd += first ? "(" : ", ";
	    cmd += "'";
	    cmd += string_literal(row[col->name]);
	    cmd += "'";
	    first = false;
	}
    }
    cmd += ") ";

    int res = db.Exec(cmd.c_str());
    if (res == PGRES_COMMAND_OK) 
	return 0;

    char buf[10*1024];
    msglog(MSG_ERR, "Table::Insert: SQL is %s\n", cmd.c_str());
    msglog(MSG_ERR, "Table::Insert: ErrCode is %d\n", res);
    dumpRow(buf, sizeof(buf), row); 
    msglog(MSG_ERR, "Table::Insert: Row is %s\n", buf);

    return -1;
}


int Table::Update(DB &db, Row& row)
{
    string cmd;

    cmd = "UPDATE ";
    cmd += Name();
    cmd += " SET";
    bool first = true;
    for (const_iterator col = begin(); col != end(); col++) {
	if (row.find(col->name) != row.end()) {
	    if (!(col->flags & PRIMARY_KEY)) {
		cmd += first ? " " : ", ";
		cmd += col->name;
		cmd += "='";
		cmd += string_literal(row[col->name]);
		cmd += "'";
		first = false;
	    }
	}
    }
    cmd += " WHERE";
    first = true;
    for (const_iterator col = begin(); col != end(); col++) {
	if (col->flags & PRIMARY_KEY) {
	    if (row.find(col->name) == row.end()) {
		msglog(MSG_ERR, "Table::Update: Missing primary key %s in table %s\n", 
		       Name(), col->name);
		return -2;
	    }
	    cmd += first ? " " : " AND ";
	    cmd += col->name;
	    cmd += "='";
	    cmd += string_literal(row[col->name]);
	    cmd += "'";
	    first = false;
	}
    }
    
    int res = db.Exec(cmd.c_str());
    if (res == PGRES_COMMAND_OK) 
	return 0;
    else {
	char buf[10*1024];
	msglog(MSG_ERR, "Table::Insert: SQL is %s\n", cmd.c_str());
	msglog(MSG_ERR, "Table::Insert: ErrCode is %d\n", res);
	dumpRow(buf, sizeof(buf), row); 
	msglog(MSG_ERR, "Table::Insert: Row is %s\n", buf);
    }

    return -1;
}


int Table::UpdateAll(DB &db, Row& row, const string& constraints)
{
    string cmd;

    cmd = "UPDATE ";
    cmd += Name();
    cmd += " SET";
    bool first = true;
    for (const_iterator col = begin(); col != end(); col++) {
	if (row.find(col->name) != row.end()) {
	    cmd += first ? " " : ", ";
	    cmd += col->name;
	    cmd += "='";
	    cmd += string_literal(row[col->name]);
	    cmd += "'";
	    first = false;
	}
    }
    if (constraints.size() > 0) {
	cmd += " WHERE ";
	cmd += constraints;
    }

    int res = db.Exec(cmd.c_str());
    if (res == PGRES_COMMAND_OK) {
	if (db.Verbose())
	    msglog(MSG_INFO, "Table::UpdateAll: SQL is %s\n", cmd.c_str());
	return 0;
    } else {
	char buf[10*1024];
	msglog(MSG_ERR, "Table::UpdateAll: SQL is %s\n", cmd.c_str());
	msglog(MSG_ERR, "Table::UpdateAll: ErrCode is %d\n", res);
	dumpRow(buf, sizeof(buf), row); 
	msglog(MSG_ERR, "Table::UpdateAll: Row is %s\n", buf);
    }
    return -1;
}


int Table::GetFirst(DB &db, Row &row)
{
    string cmd;

    cmd = "SELECT * FROM ";
    cmd += Name();
    cmd + " WHERE ";
    bool first = true;
    for (const_iterator col = begin(); col != end(); col++) {
	if (row.find(col->name) != row.end()) {
	    cmd += first ? " " : " AND ";
	    cmd += col->name;
	    cmd += "='";
	    cmd += string_literal(row[col->name]);
	    cmd += "'";
	    first = false;
	}
    }

    if (db.Verbose())
	msglog(MSG_INFO, "Table::GetFirst: Cmd = %s\n", cmd.c_str());

    if (db.Exec(cmd.c_str()) != PGRES_TUPLES_OK || db.Tuples() < 1)
	return -1;
    
    int fields = db.Fields();
    for (int i = 0; i < fields; i++) {
	if (!db.GetIsNull(0, i)) {
	    char *key = strdup(db.FieldName(i));
	    char *val = strdup(db.GetValue(0,i));
	    row[key] = val;
	}
    }

    return 0;
}


bool isTableName(const char *tblname) 
{
    for (int i = 0; i < numTables(); i++) {
	struct table_def *pt = getTable(i);
	if ((pt->flags & TABLE) && strcmp(pt->name, tblname) == 0) 
	    return true;
    }
    return false;
}


DB_insertor::DB_insertor(DB& d, const char *tblname)
    :db(d) 
{
    verbose = d.verbose;
    if (tblname)
	tbl.Bind(tblname);
    count = 0;
    errcount = 0;
}


int DB_insertor::operator()(const string& tblname, Row& r)
{
    if (verbose) {
	msglog(MSG_INFO, "DB_insertor(): %s\n", tblname.c_str());
	dumpRow(r);
    }
    if (tbl.Bind(tblname) != 0)
	return -1;

    if (tbl.Insert(db, r, true) != 0) {
	errcount++;
	return -1;
    }

    count++;

    return 0;
};


int DB_insertor::operator()(Row& r)
{
    if (verbose) {
	msglog(MSG_INFO, "DB_insertor(): %s\n", tbl.Name());
	dumpRow(r);
    }

    if (tbl.Insert(db, r, true) != 0) {
	errcount++;
	return -1;
    }

    count++;

    return 0;
};


int DB_insertor::UpdateAll(Row& r, const string& constraints)
{
    if (verbose) {
	msglog(MSG_INFO, "DB_insertor(): %s\n", tbl.Name());
	dumpRow(r);
    }

    if (tbl.UpdateAll(db, r, constraints) != 0) {
	errcount++;
	return -1;
    }

    count++;

    return 0;
}


DB_query_iterator::value_type 
DB_query_iterator::operator*()
{
    _tmprow.clear();
    _query.getRow(_index, _tmprow);
    return _tmprow;
}


void 
DB_query::init(const string& tblname,
	       Row& row,
	       const string& constraints,
	       const string& orderby)
{
    string cmd;

    if (tbl().Bind(tblname) < 0) {
	msglog(MSG_ERR, "DB_query: cannot bind table %s\n",
	       tblname.c_str());
	set_status(-1);
    }

    if (tbl().Attr() & TABLE) {
	cmd = "SELECT * FROM ";
	cmd += tblname;
    } else if (tbl().Attr() & VIEW) {
	cmd = tbl().View();
	cmd += " ";
    } else {
	set_status(-1);
	return;
    }

    if (!row.empty() || !constraints.empty()) 
	cmd += " WHERE ";

    if (!row.empty()) {
	bool first = true;
	for (Table::const_iterator col = tbl().begin(); col != tbl().end(); col++) {
	    if (row.find((*col).name) != row.end()) {
		cmd += first ? " " : " AND ";
		cmd += (*col).name;
		cmd += "='";
		cmd += string_literal(row[(*col).name]);
		cmd += "'";
		first = false;
	    }
	}
    }
    if (!constraints.empty()) {
	if (!row.empty())
	    cmd += "AND";
	cmd += "(";
	cmd += constraints;
	cmd += ")";
    }
    if (!orderby.empty()){
	cmd += " ORDER BY ";
	cmd += orderby;
    }

    if (db().Verbose())
	msglog(MSG_INFO, "DB_query: Cmd = %s\n", cmd.c_str());

    if (db().Exec(cmd.c_str()) != PGRES_TUPLES_OK) {
	msglog(MSG_ERR, "DB_query: cannot execute %s\n",
	       cmd.c_str());
	set_status(-1);
    } else
	set_status(0);
}


DB_query::DB_query(DB& d, const string& tblname,
		   Row& row, const string& constraints):
    _db(d)
{
    string no_orderby;
    init(tblname, row, constraints, no_orderby);
}


DB_query::DB_query(DB& d, const string& tblname,
		   const string& constraints):
    _db(d)
{
    Row row;
    string no_orderby;
    init(tblname, row, constraints, no_orderby);
}


DB_query::DB_query(DB& d, const string& tblname,
		   const string& constraints,
		   const string& orderby):
    _db(d)
{
    Row row;
    init(tblname, row, constraints, orderby);
}


DB_query::DB_query(DB& d, const string& tblname,
		   Row& row):
    _db(d)
{
    string no_constraints;
    string no_orderby;
    init(tblname, row, no_constraints, no_orderby);
}


DB_query::DB_query(DB& d, const string& tblname):
    _db(d)
{
    Row row;
    string no_constraints;
    string no_orderby;
    init(tblname, row, no_constraints, no_orderby);
}


int DB_query::getRow(int row_num, Row& row) const
{
    if (row_num < 0 || row_num >= db().Tuples())
	return -1;

    int fields = db().Fields();
    for (int i = 0; i < fields; i++) {
	if (!db().GetIsNull(row_num, i)) {
	    string key = str(db().FieldName(i));
	    string val = str(db().GetValue(row_num,i));
	    row[key] = val;
	}
    }

    return 0;
}


int DB_sequence::init(unsigned start, unsigned increment, const char *constraints)
{
    destroy();
    return create(start, increment, constraints);
}


int DB_sequence::create(unsigned start, unsigned increment, const char *constraints)
{
    string cmd = "CREATE SEQUENCE ";
    cmd += seqname();
    cmd += " START ";
    cmd += tostring(start);
    cmd += " INCREMENT ";
    cmd += tostring(increment);
    if (constraints) {
	cmd += " ";
	cmd += constraints;
    }
    if (db().Verbose())
	msglog(MSG_INFO, "DB_sequence: Exec SQL: %s\n", cmd.c_str());
    if (db().Exec(cmd) != PGRES_COMMAND_OK)
	return -1;
    return 0;
}


int DB_sequence::destroy()
{
    string cmd = "DROP SEQUENCE ";
    cmd += seqname();
    if (db().Verbose())
	msglog(MSG_INFO, "DB_sequence: Exec SQL: %s\n", cmd.c_str());
    if (db().Exec(cmd) != PGRES_COMMAND_OK)
	return -1;
    return 0;
}


unsigned DB_sequence::currval()
{
    string cmd = "SELECT currval('";
    cmd += seqname();
    cmd += "')";
    if (db().Verbose())
	msglog(MSG_INFO, "DB_sequence: Exec SQL: %s\n", cmd.c_str());
    if (db().Exec(cmd) != PGRES_TUPLES_OK)
	return (unsigned)-1;

    return atoi(db().GetValue(0,0));
}


unsigned DB_sequence::lastval()
{
    string cmd = "SELECT last_value FROM ";
    cmd += seqname();
    if (db().Verbose())
	msglog(MSG_INFO, "DB_sequence: Exec SQL: %s\n", cmd.c_str());
    if (db().Exec(cmd) != PGRES_TUPLES_OK)
	return (unsigned)-1;

    return atoi(db().GetValue(0,0));
}


unsigned DB_sequence::nextval()
{
    string cmd = "SELECT nextval('";
    cmd += seqname();
    cmd += "')";
    if (db().Verbose())
	msglog(MSG_INFO, "DB_sequence: Exec SQL: %s\n", cmd.c_str());
    if (db().Exec(cmd) != PGRES_TUPLES_OK)
	return (unsigned)-1;

    return atoi(db().GetValue(0,0));
}
    

unsigned DB_sequence::setval(unsigned val)
{
    string cmd = "SELECT setval('";
    cmd += seqname();
    cmd += "', ";
    cmd += tostring(val);
    cmd += ")";
    if (db().Verbose())
	msglog(MSG_INFO, "DB_sequence: Exec SQL: %s\n", cmd.c_str());
    if (db().Exec(cmd) != PGRES_TUPLES_OK)
	return (unsigned)-1;

    return atoi(db().GetValue(0,0));
}
    

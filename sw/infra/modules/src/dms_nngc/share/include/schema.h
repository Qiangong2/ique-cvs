#ifndef __SCHEMA_H__
#define __SCHEMA_H__

/** @addtogroup db_module Database Module
    @{ */

/** @defgroup  schema_module Database Schema Type
    The database schema is defined in this format.
     @{ */


/** Column Attribute */
enum col_attr {
    PRIMARY_KEY = 1,
};


/** Table Attributes */
enum tbl_attr {
    TABLE    = 1,
    SEQUENCE = 2,
    VIEW     = 4
};


/** Column definition 
      - name : column name
      - type : column type
      - flags : column attribute (e.g., primary key)
*/
struct col_def {
    const char *name;
    const char *type;
    int         flags;
};


/** Table definition
      - name : table name
      - cols : an array of column definition (#col_def)
      - constraints: table constraints
*/
struct table_def {
    const char           *name;
    const struct col_def *cols;
    const char           *constraints;
    int                   flags;
};


/** Number of table in the Schema. */
extern int  numTables();

/** Get definition of the i-th table in the schema */
extern struct table_def *getTable(int i);

/** Check if 'name' is a valid table in the schema */
extern bool isTableName(const char *name);



/** @} */
/** @} */

#endif

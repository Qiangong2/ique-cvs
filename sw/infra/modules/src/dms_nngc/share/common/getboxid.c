#include <stdlib.h>
#include <stdio.h>
#include <syslog.h>
#include <unistd.h>
#include <fcntl.h>

#include "util.h"
#include "systemfiles.h"

void
getboxid(char boxid[RF_BOXID_SIZE]) {
    int fd;
    const char* file = GWOS_MAC0;
    unsigned char mac[6];
    bzero(mac, sizeof mac);

    strcpy(boxid, "unknown");
    if ((fd = open(file, O_RDONLY)) < 0) {
    	syslog(LOG_ERR, "%s: %m", file);
	return;
    }
    if (read(fd, mac, sizeof mac) < 0)
    	syslog(LOG_ERR, "%s: read %m\n", file);
    sprintf(boxid, "HR%02X%02X%02X%02X%02X%02X",
    	mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
    close(fd);
}

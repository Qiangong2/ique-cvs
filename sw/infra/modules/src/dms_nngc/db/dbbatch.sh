#!/bin/sh

if [ -z "$1" -o \
     -z "$2" -o \
     -z "$3" ]; then
echo "dbbatch.sh <command> <start lot> <count>"
exit 1
fi

command=$1
start=$2
count=$3
x=1

while true;
do
echo ./db $command lot$start
./db $command lot$((start++))
if [ $((x++)) = $count ]; then
exit 0;
fi
done

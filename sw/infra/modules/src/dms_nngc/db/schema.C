#include "schema.h"
#include "db.h"

static struct col_def lots_cols[] = {
    { "lot_number",     "varchar(20)", PRIMARY_KEY },
    { "lot_desc",       "varchar(100)" },
    { "lot_size",       "int" },
    { "create_date",    "int" },
    { "last_processed", "int" },
    { "num_processed",  "int" },
    { "last_reported",  "int" },
    { "cert_id",        "varchar(32)" },
    { "chip_rev",       "varchar(32)" },
    { "manufacturer",   "varchar(64)" },
    { "location",       "varchar(64)" },
    { NULL }
};

static struct col_def lot_chips_cols[] = {
    { "chip_id",        "int",         PRIMARY_KEY },
    { "lot_number",     "varchar(20)" },
    { "process_date",   "int" },
    { "cert",           "varchar(2000)" },
    { NULL }
};

static struct col_def lots_last_reported_cols[] = {
    { "max_last_reported", "int" },
    { NULL }
};

static struct col_def history_cols[] = {
    { "date", "int" },
    { "description", "varchar(1024)" },
    { NULL }
};

static struct col_def lot_chips_reported_cols[] = {
    { "count", "int" },
    { NULL }
};

static struct table_def tables[] = {
    { "lots",
      lots_cols,
      "PRIMARY KEY ( lot_number )",
      TABLE },

    { "lot_chips",
      lot_chips_cols,
      "PRIMARY KEY ( chip_id )",
      TABLE },

    { "chip_id",
      NULL,
      NULL,
      SEQUENCE },

    { "lots_last_reported", 
      lots_last_reported_cols,
      "SELECT MAX(last_reported) AS max_last_reported FROM lots",
      VIEW },

    { "history",
      history_cols,
      NULL,
      TABLE },

    { "lot_chips_reported", 
      lot_chips_reported_cols,
      "SELECT count(chip_id) FROM lot_chips",
      VIEW },
};


int numTables()
{
    return ((int) (sizeof(tables)/sizeof(table_def)));
}

struct table_def *getTable(int i)
{
    return &tables[i];
}


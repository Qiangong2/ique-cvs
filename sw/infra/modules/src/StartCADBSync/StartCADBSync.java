import java.util.*;
import java.io.*;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.text.DateFormat;

public class StartCADBSync
{
    static final String                 QUERY_XML_CALL;
    static final String                 SQL_CERTIFICATES;
    static final String                 SQL_CERTIFICATE_CHAINS;
    static final String                 SQL_CURRENT_CRLS;
    
    static
    {
        QUERY_XML_CALL = "{? = call BBXML.GetQueryXML(?)}";
	SQL_CERTIFICATES = "select * from certificates";
	SQL_CERTIFICATE_CHAINS = "select * from certificate_chains";
	SQL_CURRENT_CRLS = "select * from current_crls";
    }

    static Connection getConnection(Properties prop)
		throws SQLException, ClassNotFoundException
    {
	String dbUrl = prop.getProperty("DB_URL");
	String dbUser = prop.getProperty("DB_USER");
	String dbPass = prop.getProperty("DB_PASSWORD");
	String dbClass = prop.getProperty("DB_CLASS");

	Class.forName(dbClass);
	
	Connection con = DriverManager.getConnection(dbUrl, dbUser, dbPass);

	WriteLog("CONNECTING to the database as... " + dbUser + "/" + dbPass);
	        
	return con;
    }  
  
    static Properties ReadPropFile(String propFile)
	throws IOException
    {
        Properties prop = new Properties();
        FileInputStream in = new FileInputStream(propFile);
        prop.load(in);
        in.close();

        WriteLog("READING property file... " + propFile);
        return prop;
    }

    static String query(String sql, Connection conn)
	throws SQLException, IOException
    {
        CallableStatement call = prepareCall(QUERY_XML_CALL, conn);

        call.registerOutParameter(1, Types.CLOB);
        call.setString(2, sql);
        call.executeQuery();

        Clob        clob = call.getClob(1);
        Reader      reader = clob.getCharacterStream();
        int         size = (int)clob.length();
        char[]      buffer = new char[size];

        reader.read(buffer);
	closeStatement(call);
        return new String(buffer);
    }

    static CallableStatement prepareCall(String sql, Connection connection)
	throws SQLException
    {
        return connection.prepareCall(sql);
    }

    static void closeStatement(Statement statement)
	throws SQLException
    {
        if (statement != null)
            statement.close();
    }

    static String getTarFileName()
    {
	java.util.Date d = new java.util.Date();
        long milli = d.getTime();
        String mSec = String.valueOf(milli);

	String filename = null;
        Calendar calendar = new GregorianCalendar();
        java.util.Date date = calendar.getTime();
        DateFormat fmt = new SimpleDateFormat("MMM_dd_yyyy");
        filename = "CA_" + fmt.format(date) + "_" + mSec + ".tar";

	return filename;
    }

    static void CreateFile(String data, String filename)
	throws IOException
    {
        FileWriter pfile = new FileWriter(filename);
	PrintWriter pwrite = new PrintWriter(pfile);
				
	pwrite.println(data);
	
	pwrite.close();
    }

    static void WriteLog(String mesg) 
    {
        java.util.Date d = new java.util.Date();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z");
        System.out.println(df.format(d) + ": " + mesg);
    }

    public static void main(String[] args) 
    {
        try 
        {
   	    Properties prop = ReadPropFile("StartCADBSync.properties");
        	    
	    Connection conn = null;
            conn = getConnection(prop);
        
	    WriteLog("QUERYING for the certificates...");
   	    String certStr = query(SQL_CERTIFICATES, conn);
	    String certFile = prop.getProperty("CERT_FILE");
	    CreateFile(certStr, certFile);
	    WriteLog("DONE");

	    WriteLog("QUERYING for the certificate chains...");
	    String chainStr = query(SQL_CERTIFICATE_CHAINS, conn);
	    String chainFile = prop.getProperty("CHAIN_FILE");
	    CreateFile(chainStr, chainFile);
	    WriteLog("DONE");

	    WriteLog("QUERYING for the current crls...");
	    String crlStr = query(SQL_CURRENT_CRLS, conn);
	    String crlFile = prop.getProperty("CRL_FILE");
	    CreateFile(crlStr, crlFile);
	    WriteLog("DONE");

            conn.close();

	    String tarFile = getTarFileName();
	    WriteLog("CREATING tar file - " + tarFile);
   		
	    Process p = Runtime.getRuntime().exec("/bin/tar cf " + tarFile + " " + 
				certFile + " " + chainFile + " " + crlFile);
	    p.waitFor();
            if (p.exitValue() != 0)
	        WriteLog("ERROR -- " + tarFile + " could not be created.");
	    else
	    	WriteLog("SUCCESS -- " + tarFile + " successfully created.");
        
	    Process rm = Runtime.getRuntime().exec("/bin/rm -f " +  
				certFile + " " + chainFile + " " + crlFile);
	    rm.waitFor();
		 	
        } catch (IOException e) {
	    e.printStackTrace();
            WriteLog("IOException Occured: " + e.toString());
        } catch (InterruptedException ie) {
            ie.printStackTrace();
            WriteLog("InterruptedError while de-queue process : " + ie.toString());
        } catch (SQLException e) {
	    e.printStackTrace();
            WriteLog("1 - SQLException Occured - " + e.toString());
        } catch (ClassNotFoundException e) {
	    e.printStackTrace();
            WriteLog("ClassNotFoundException Occured - " + e.toString());
        }
    }
}
    
   

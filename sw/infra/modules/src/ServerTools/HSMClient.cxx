#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>

#include <string>

#include "HSMClient.h"

const unsigned int HSM_CLIENT::CMD_HEADER_SIZE = 128;
const char HSM_CLIENT::VERSION[] = "Version";
const char HSM_CLIENT::VERSION_STRING[] = "1.1";
const char HSM_CLIENT::COMMAND[] = "Command";
const char HSM_CLIENT::CMD_NEW_SERVER[] = "new_server";
const char HSM_CLIENT::CMD_NEW_SMART_CARD[] = "new_card";
const char HSM_CLIENT::NEW_CERT_NAME[] = "cert_name";
const char HSM_CLIENT::DATA_LEN[] = "DataLength";
const char HSM_CLIENT::REPLY_PASSWORD_LEN[] = "passwdLength";
const char HSM_CLIENT::REPLY_KEY_LEN[] = "keyLength";
const char HSM_CLIENT::REPLY_CERT_LEN[] = "certLength";
const char HSM_CLIENT::REPLY_CHAIN_LEN[] = "chainLen";
const char HSM_CLIENT::REPLY_ROOT_LEN[] = "rootLen";
const char HSM_CLIENT::REPLY_KEYSTORE_LEN[] = "ksLen";
const char HSM_CLIENT::REPLY_TRUSTED_STORE_LEN[] = "tsLen";


static int
getSocket (const char* host, short host_port)
{
    int fd = socket (PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (fd == -1) {
	perror ("socket()");
	return -1;
    }

    hostent* hostname = gethostbyname(host);
    if (hostname == NULL) {
	perror ("gethostbyname()");
	return -1;
    }

    sockaddr_in host_addr;
    host_addr.sin_family = AF_INET;
    host_addr.sin_port = htons(host_port);
    (void) memcpy (&host_addr.sin_addr, hostname->h_addr, hostname->h_length);

    if (connect (fd, (sockaddr*) &host_addr, sizeof(host_addr)) == -1) {
	perror ("connect()");
	close (fd);
	return -1;
    }

    return fd;
} // getSocket


bool
HSM_CLIENT::send_request(const char* hostname, const char* cmd)
{
    string header;
    string format(" = ");

    header = VERSION + format + VERSION_STRING;
    header.push_back('\n');

    header += COMMAND + format + cmd;
    header.push_back('\n');

    header += NEW_CERT_NAME + format + hostname;
    header.push_back('\n');

    header.resize(CMD_HEADER_SIZE, ' ');

    return send(sock_fd, header.data(), header.size(), 0) == (int) header.size();
} // send_request


void
HSM_CLIENT::read_reply(byte_array& reply)
{
    char buf[4096];
    int count;
    while ((count = recv(sock_fd, buf, sizeof(buf), 0)) > 0) {
	reply.insert(reply.end(), buf, buf + count);
    }
    return;
} // read_reply


static const char*
match_str(const char* buf, const char* pattern1, const char* pattern2)
{
    int len1 = strlen(pattern1);
    int len2 = pattern2 ? strlen(pattern2) : 0;
    if (strncmp(buf, pattern1, len1) != 0)
	return 0;
    if (len2 == 0 || strncmp(buf + len1, pattern2, len2) == 0)
	return buf + len1 + len2;
    else
	return 0;
} // match_str


static const char*
get_integer(const char* buf, const char* pattern, int& value)
{
    const char* p = match_str(buf, pattern, "=");
    if (p == 0 || sscanf(p, "%d\n", &value) != 1)
	return 0;
    return index(p, '\n') + 1;
} // get_integer


static const char*
get_reply(const char* header, const char*& response,
	  const char* key, string& value)
{
    int len;
    const char* p = get_integer(header, key, len);
    if (p == 0)
	return 0;
    value.append(response, response + len);
    response += len;
    return p;
} // get_reply


static const char*
get_reply(const char* header, const char*& response,
	  const char* key, byte_array& value)
{
    int len;
    const char* p = get_integer(header, key, len);
    if (p == 0)
	return 0;
    value.insert(value.end(), response, response + len);
    response += len;
    return p;
} // get_reply



void
HSM_CLIENT::parse_reply(const byte_array& reply)
{
    if (reply.size() < CMD_HEADER_SIZE)
	return;

    const char* p;
    if ((p = match_str(&reply.front(), VERSION, "=")) == 0)
	return;
    if ((p = match_str(p, VERSION_STRING, "\n")) == 0)
	return;

    const char* response = &reply.front() + CMD_HEADER_SIZE;

    if ((p = get_reply(p, response, REPLY_PASSWORD_LEN, password)) == 0)
	return;
    if ((p = get_reply(p, response, REPLY_KEY_LEN, private_key)) == 0)
	return;
    if ((p = get_reply(p, response, REPLY_CERT_LEN, certificate)) == 0)
	return;
    if ((p = get_reply(p, response, REPLY_CHAIN_LEN, cert_chain)) == 0)
	return;
    if ((p = get_reply(p, response, REPLY_ROOT_LEN, root_cert)) == 0)
	return;
    if ((p = get_reply(p, response, REPLY_KEYSTORE_LEN, key_store)) == 0)
	return;
    if ((p = get_reply(p, response, REPLY_TRUSTED_STORE_LEN, trusted_store)) == 0)
	return;

    request_ok = true;
} // parse_reply


HSM_CLIENT::HSM_CLIENT(const char* server, unsigned short port,
		       const char* hostname, bool is_server) :
    request_ok(false)
{
    sock_fd = getSocket(server, port);
    if (sock_fd < 0)
	return;

    if (! send_request(hostname,
		       is_server ? CMD_NEW_SERVER : CMD_NEW_SMART_CARD))
	return;

    byte_array reply;
    read_reply(reply);

    parse_reply(reply);
}

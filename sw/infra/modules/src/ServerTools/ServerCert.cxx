#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <pwd.h>

#include "HSMClient.h"
#include "configvar.h"

static const char* id_name = "identity.pem";
static const char* key_name = "private_key.pem";
static const char* chain_name = "ca_chain.pem";
static const char* root_name = "root_cert.pem";

static gid_t gid = (gid_t) -1;

static struct option long_options[] = {
    {"host", 1, 0, 'h'},		// host name of cert issuer
    {"port", 1, 0, 'p'},		// port number of cert issuer
    {"name", 1, 0, 'n'},		// common name of cert
    {"key_store", 1, 0, 'k'},		// filename of keystore
    {"trusted_store", 1, 0, 't'},	// filename of trusted store
    {"cert_dir", 1, 0, 'c'},		// directory for X509 certs
    {"group", 1, 0, 'g'},		// optional group id for output files
    {"sc", 0, 0, 's'},			// generate smart card cert instead
    {0, 0, 0, 0}
};


static void
usage(const char* progname)
{
    fprintf(stderr, "usage: %s\n", progname);
    fprintf(stderr,
            " -host, -h         = host name of cert server.\n"
            " -port, -p         = port number of cert server.\n"
            " -name, -n         = common name of cert.\n"
            " -key_store, -k    = file name to write keystore.\n"
            " -trusted_store, -t= file name to write trusted store.\n"
            " -cert_dir, -c     = directory for writing X509 certs.\n"
            " -group, -g        = optional group name for output files.\n"
            " -sc               = generate smart card cert.\n");
}


static void
write_file(const char* dir, const char* file, const char* data, int size)
{
    char path[1024];
    if (dir == 0)
	strcpy(path, file);
    else
	sprintf(path, "%s/%s", dir, file);

    unlink(path);			// unlink first would guarantee the
					// file permission is set right.
    int fd = creat(path, 0440);
    if (fd < 0) {
	fprintf(stderr, "Cannot write %s: %s\n", path, strerror(errno));
	return;
    }
    fchown(fd, (uid_t) -1, gid);
    write(fd, data, size);
    close(fd);
} // write_file


static void
setconf(const char* key, const char* value)
{
    char buf[1024];

    sprintf(buf, "/sbin/setconf -r \"%s:mode=r--:=%s\"", key, value);

    system(buf);
}


static gid_t
get_group_id(const char* name)
{
    struct passwd* pw = getpwnam(name);
    return (pw == 0) ? (gid_t) -1 : pw->pw_gid;
}


int
main(int argc, char* argv[])
{
    const char* server_host = 0;
    unsigned short server_port = 0;
    const char* common_name = 0;
    const char* key_store = 0;
    const char* trusted_store = 0;
    const char* cert_dir = 0;
    const char* group_name = 0;
    bool server_cert = true;

    while (1) {
        int ch = getopt_long_only(argc, argv, "", long_options, 0);
        if (ch == -1)
            break;
	
        switch (ch) {
	case 'h':
	    server_host = optarg;
	    break;

	case 'p':
	    server_port = atoi(optarg);
	    break;

	case 'n':
	    common_name = optarg;
	    break;

	case 'k':
	    key_store = optarg;
	    break;

	case 't':
	    trusted_store = optarg;
	    break;

	case 'c':
	    cert_dir = optarg;
	    break;

	case 'g':
	    group_name = optarg;
	    break;

	case 's':
	    server_cert = false;
	    break;

	default:
	    usage(argv[0]);
	    return 1;
	}
    }

    if (common_name == 0 || server_host == 0 || server_port == 0) {
	usage(argv[0]);
	return 1;
    }

    if (key_store || trusted_store) {
	// need java key store
	if (key_store == 0 || trusted_store == 0) {
	    fprintf(stderr, "One of key store or trusted store is missing.\n");
	    usage(argv[0]);
	    return 1;
	}
    }

    printf("Generating new certificate for %s ...", common_name);
    fflush(stdout);

    HSM_CLIENT hsm(server_host, server_port, common_name, server_cert);

    if (! hsm.is_reply_valid()) {
	printf(" failed\n");
	return 1;
    } else
	printf(" done.\n");

    if (group_name) 
	gid = get_group_id(group_name);

    if (key_store) {
	write_file(0, key_store, &hsm.key_store.front(), hsm.key_store.size());
	write_file(0, trusted_store, &hsm.trusted_store.front(),
		   hsm.trusted_store.size());
    }

    if (cert_dir) {
	write_file(cert_dir, id_name, hsm.certificate.
		   data(), hsm.certificate.size());
	write_file(cert_dir, key_name, hsm.private_key.data(),
		   hsm.private_key.size());
	write_file(cert_dir, chain_name, hsm.cert_chain.data(),
		   hsm.cert_chain.size());
	write_file(cert_dir, root_name, hsm.root_cert.data(),
		   hsm.root_cert.size());
	if (server_cert)
	    setconf(CFG_COMM_PASSWD, hsm.password.c_str());
	else
	    write_file(cert_dir, "password.txt", hsm.password.data(),
		       hsm.password.size());

    }
    
    return 0;
} // main

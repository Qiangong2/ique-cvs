/*
 *               Copyright (C) 2005, BroadOn Communications Corp.
 *
 *  These coded instructions, statements, and computer programs contain
 *  unpublished  proprietary information of BroadOn Communications Corp.,
 *  and  are protected by Federal copyright law. They may not be disclosed
 *  to  third  parties or copied or duplicated in any form, in whole or in
 *  part, without the prior written consent of BroadOn Communications Corp.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <libgen.h>
#include <fcntl.h>
#include <errno.h>

#include <sys/stat.h>
#include <sys/types.h>

#include <netinet/in.h>

#include "x86/esl.h"
#include "es_int.h"
#include "aes.h"
#include "sha1.h"

typedef struct
{
    u32 hdrSize;
    u8  wadType[2];
    u16 wadVersion;
    u32 certSize;
    u32 crlSize;
    u32 ticketSize;
    u32 tmdSize;
    u32 contentSize;
    u32 metaSize;
} WADHeader;

/* Common key for Wii development PKI */
static u8 wiiCommonKey[] = {
    0xa1, 0x60, 0x4a, 0x6a, 0x71, 0x23, 0xb5, 0x29,
    0xae, 0x8b, 0xec, 0x32, 0xc8, 0x16, 0xfc, 0xaa
};

static u8 ncCommonKey[] = {
    0x67, 0x45, 0x8b, 0x6b, 0xc6, 0x23, 0x7b, 0x32,
    0x69, 0x98, 0x3c, 0x64, 0x73, 0x48, 0x33, 0x66
};

static u8* commonKey = wiiCommonKey;

void
Usage(char *prog)
{
    fprintf(stderr, "Usage: %s [-nc] <wad file>\n", basename(prog));
    exit(-1);
}

int
readWadHeader(int fd, WADHeader *header)
{
    int rv;

    /* Read WAD header */
    if ((rv = read(fd, header, sizeof(WADHeader))) != sizeof(WADHeader)) {
        fprintf(stderr, "read: unable to read WAD header: %d != %d\n", rv, sizeof(WADHeader));
        return -1;
    }

    /* WAD files are stored in network byte order (big-endian) */
    header->hdrSize = ntohl(header->hdrSize);
    header->wadVersion = ntohs(header->wadVersion);
    header->certSize = ntohl(header->certSize);
    header->crlSize = ntohl(header->crlSize);
    header->ticketSize = ntohl(header->ticketSize);
    header->tmdSize = ntohl(header->tmdSize);
    header->contentSize = ntohl(header->contentSize);
    header->metaSize = ntohl(header->metaSize);

    /* Only support wad version 0 for now */
    if (header->hdrSize != sizeof(WADHeader) || header->wadVersion != 0) {
        fprintf(stderr, "invalid wad header: size=%d, version=%d\n", header->hdrSize, header->wadVersion);
        return -1;
    }

    return 0;
}


static int
skipFile(int ifd, int size)
{
    off_t pos = lseek(ifd, 0, SEEK_CUR);
    pos = ((pos + 63) & ~63) + size;
    return (lseek(ifd, pos, SEEK_SET) == (off_t)-1);
}


static u8 buffer[4096];

int
dumpFile(int ifd, char *base, char *suffix, int size)
{
    int rv = 0;
    char file[PATH_MAX];
    int ofd = -1;
    off_t pos;

    /* Round up to 64 byte offset in file */
    pos = lseek(ifd, 0, SEEK_CUR);
    lseek(ifd, (pos + 63) & ~63, SEEK_SET);

    /* Create destination file */
    snprintf(file, sizeof(file), "%s/%s", base, suffix);
    if ((ofd = open(file, O_CREAT|O_WRONLY, 0644)) < 0) {
        fprintf(stderr, "open: cannot open '%s': %s\n", file, strerror(errno));
        rv = -1;
        goto end;
    }

    /* Copy portion out of WAD file a chunk at a time */
    while (size) {
        int chunk = size > sizeof(buffer) ? sizeof(buffer) : size;

        if ((rv = read(ifd, buffer, chunk)) != chunk) {
            fprintf(stderr, "read: unable to read %d bytes: %d\n", chunk, rv);
            rv = -1;
            goto end;
        }

        if ((rv = write(ofd, buffer, chunk)) != chunk) {
            fprintf(stderr, "write: unable to write %d bytes: %d\n", chunk, rv);
            rv = -1;
            goto end;
        }
        size -= chunk;
    }

    rv = 0;
end:
    if (ofd >= 0) close(ofd);
    return rv;   
}

int
dumpContents(int ifd, char *dir, ESTicket *ticket, ESTitleMeta *tmd)
{
    int rv = 0;
    int ofd = -1;
    u16 numContents = tmd->head.numContents;
    u16 i;
    CSLOSAesKey iv;
    CSLOSAesKey titleKey;
    SHA1Context sha;
    IOSCSha1Hash hash;
    char buffer[16], obuffer[16];

    /* Decrypt titleKey with commonKey */
    memset(iv, 0, sizeof(iv));
    memcpy(iv, &ticket->titleId, sizeof(ESTitleId));
    aes_SwDecrypt(commonKey, iv, ticket->titleKey, sizeof(CSLOSAesKey), titleKey);

    /* Decrypt each piece of content */
    for (i = 0; i < numContents; ++i) {
        off_t pos;
        char file[PATH_MAX];
        ESContentMeta *c = tmd->contents + i;
        u64 size = c->size;

        /* Set IV to content index */
	u16 idx = htons(c->index);
        memset(iv, 0, sizeof(iv));
        memcpy(iv, &idx, sizeof(idx));

        /* Reset per-content hash */
        SHA1Reset(&sha);

        /* Round up to 64 bytes */
        pos = lseek(ifd, 0, SEEK_CUR);
        lseek(ifd, (pos + 63) & ~63, SEEK_SET);

        /* Create output file */
        snprintf(file, sizeof(file), "%s/%03d-file%d", dir, c->index, c->index);
        if ((ofd = open(file, O_CREAT|O_WRONLY, 0644)) < 0) {
            fprintf(stderr, "open: cannot open '%s': %s\n", file, strerror(errno));
            rv = -1;
            goto end;
        }

        /* Iterate over file a chunk at a time */
        while (size) {
            int chunk = size > sizeof(buffer) ? sizeof(buffer) : size;
	    int n;

            /* Read encrypted chunk */
            if ((n = read(ifd, buffer, sizeof(buffer))) != sizeof(buffer)) {
                fprintf(stderr, "read: unable to read %d bytes: %d\n", chunk, n);
                rv = -1;
                goto end;
            }

            /* Decrypt chunk and update IV */
            aes_SwDecrypt(titleKey, iv, buffer, sizeof(buffer), obuffer);
            memcpy(iv, buffer + chunk - sizeof(IOSCAesIv), sizeof(IOSCAesIv));

            /* Hash decrypted chunk */
            SHA1Input(&sha, obuffer, chunk);

            /* Write decrypted chunk */
            if ((n = write(ofd, obuffer, chunk)) != chunk) {
                fprintf(stderr, "write: unable to write %d bytes: %d\n", chunk, n);
                rv = -1;
                goto end;
            }
            size -= chunk;
        }
        /* Compare computed hash result with stored hash in the tmd */
        SHA1Result(&sha, hash);
        if (memcmp(hash, c->hash, sizeof(hash))) {
            fprintf(stderr, "hash mismatch for content %d\n", i);
            rv = -1;
            goto end;
        }
        close(ofd);
        ofd = -1;
    }

end:
    if (ofd >= 0) close(ofd);
    return rv;   
}

int
swapTicket(ESTicket *ticket)
{
    /* titleId is the only field we look at */
    ticket->titleId = ntohll(ticket->titleId);
    return 0;
}

int
swapTmd(ESTitleMeta *tmd)
{
    u32 i, n;

    /* For each piece of content, swap from big-endian to host-endian */
    n = tmd->head.numContents = ntohs(tmd->head.numContents);
    for (i = 0; i < n; ++i) {
        ESContentMeta *c = tmd->contents + i;
        c->cid = ntohl(c->cid);
        c->index = ntohs(c->index);
        c->size = ntohll(c->size);
    }
    return 0;
}

int
crackWad(char *wadFile)
{
    int ifd = -1;
    int rv = 0;
    WADHeader header;
    char* base;
    char *p;
    ESTicket *ticket = NULL;
    ESTitleMeta *tmd = NULL;

    /* Open WAD file */
    if ((ifd = open(wadFile, O_RDONLY)) < 0) {
        fprintf(stderr, "open: cannot open '%s': %s\n", wadFile, strerror(errno));
        rv = -1;
        goto end;
    }

    /* Read and parse header */
    if (readWadHeader(ifd, &header) < 0) {
        rv = -1;
        goto end;
    }

    // use the same directory as the input file for output
    base = dirname(wadFile);

    /* Allocate ticket and tmd data structures based on WAD header */
    if ((ticket = (ESTicket *)malloc(header.ticketSize)) == NULL ||
        (tmd = (ESTitleMeta *)malloc(header.tmdSize)) == NULL) {
        fprintf(stderr, "unable to allocate ticket and tmd structures\n");
        rv = -1;
        goto end;
    }

    /* Dump files */
    if (skipFile(ifd, header.certSize) ||
        skipFile(ifd, header.crlSize) ||
        dumpFile(ifd, base, "eticket.template" , header.ticketSize) ||
        memcpy(ticket, buffer, header.ticketSize) != ticket ||
        dumpFile(ifd, base, "tmd.template" , header.tmdSize) ||
        memcpy(tmd, buffer, header.tmdSize) != tmd ||
        swapTmd(tmd) ||
        dumpContents(ifd, base, ticket, tmd) ||
        dumpFile(ifd, base, "meta.template" , header.metaSize)) {
        rv = -1;
        goto end;
    }

end:
    if (ifd >= 0) close(ifd);
    if (ticket) free(ticket);
    if (tmd) free(tmd);

    return rv;
}

int
main(int argc, char *argv[])
{
    char* wadFile = NULL;
    int i;
    
    if (argc < 2) {
        Usage(argv[0]);
    }

    
    for (i = 1; i < argc; ++i) {
	if (strcmp("-nc", argv[i]) == 0) {
	    commonKey = ncCommonKey;
	} else
	    wadFile = argv[i];
    }

    return crackWad(wadFile);
}

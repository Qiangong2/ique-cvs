/*
 * httplib.c: API to send HTTP message
 *
 * Author:  Hiro@RouteFree.com
 * Created: 2/01/01
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/param.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/file.h>
#include <sys/types.h>
#include <netinet/in_systm.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <netdb.h>

#include "ssl_socket.h"
#include "httplib.h"

/* forward declaration */
extern int url_encode(char *in, char *out);
extern int is_urlchar(int c);
extern int get_hex_char(int value);

/* ---------------------------------------------------------------------- * 
 *                    Connecting functions                                *
 * ---------------------------------------------------------------------- */

HttpLibHandle *
httplib_connect_ssl(char *hostname,
                                int portnumber,
                                char *cert_filename,
                                char *ca_filename,
                                char *key_filename,
                                char *keyfile_passwd,
                                char *cname,
                                int keep_alive)
{
    HttpLibHandle *handle = (HttpLibHandle *)NULL;
    SSL_WRAPPER *sw = Ssl_Connect(hostname, portnumber, cert_filename,
                                  ca_filename, key_filename, keyfile_passwd,
                                  cname);
    if (sw != NULL) {
        handle = (HttpLibHandle *)malloc(sizeof(HttpLibHandle));
        strcpy(handle->hostname, hostname);
        handle->is_ssl = TRUE;
        handle->ssl = (void *)sw;;
        handle->socket_fd = get_ssl_sock_fd(sw);
        handle->keep_alive = keep_alive;
    }

    return handle;
}


HttpLibHandle *
httplib_connect(char *hostname, int portnumber, int keep_alive)
{
    HttpLibHandle *handle = (HttpLibHandle *)NULL;
    struct sockaddr_in target;/* Who to connect to */
    struct hostent *hp;	/* Pointer to host info */
    int i, skt; 

    bzero((char *)&target, sizeof(struct sockaddr) );
    target.sin_family = AF_INET;
    target.sin_port = htons(portnumber);
    hp = (struct hostent *) gethostbyname(hostname);
    if (hp == NULL) {
        fprintf(stderr, "Cannot get address for hostname=%s", hostname);
        return (HttpLibHandle *)NULL;
    }
    for (i=0; hp->h_addr_list[i] != NULL; i++) {
        memcpy((char *)&target.sin_addr, hp->h_addr_list[i], sizeof(struct in_addr));
        skt = socket(PF_INET, SOCK_STREAM, 0);
        if (skt < 0) {
            fprintf(stderr, "unable to open socket() %m");
            return (HttpLibHandle *)NULL;
        }
        if (connect(skt, (const struct sockaddr *)&target, sizeof(target)) >= 0) {
            handle = (HttpLibHandle *)malloc(sizeof(HttpLibHandle));
            strcpy(handle->hostname, hostname);
            handle->is_ssl = FALSE;
            handle->ssl = (void *)NULL;;
            handle->socket_fd = skt;
            handle->keep_alive = keep_alive;
            return handle;
        }
        close(skt); /* Clean up.  Try again? */

    } /* Exit this loop when we run out of addresses*/

    return (HttpLibHandle *)NULL;
}

/* ---------------------------------------------------------------------- * 
 *                    Send/Recieve/Close                                  *
 * ---------------------------------------------------------------------- */

int
httplib_send_immediate(HttpLibHandle *handle, void *buffer, int len)
{
    if (handle->is_ssl) {
        return send_data_ssl(handle->ssl, buffer, len);
    } else {
        return send(handle->socket_fd, buffer, len, 0);
    }
}

/**
 * linux has bad behavior on writing socket - all overflow data
 * will be thrown away without any warning
 *
 * therefore, use select() to make sure it can write data
 */
#define	BLKSIZE	(1024)
int
httplib_send(HttpLibHandle *handle, void *buffer, int len)
{
    fd_set rfds;
    int n_wrote = 0, n, stat;

    while(n_wrote < len) {
	FD_ZERO(&rfds);
	FD_SET(handle->socket_fd, &rfds);
	stat = select(handle->socket_fd+1,
			NULL, &rfds, NULL,
			(struct timeval *)NULL);
	n = len - n_wrote;
	if (n > BLKSIZE) {
	    n = BLKSIZE;
	}
	stat = httplib_send_immediate(handle, buffer+n_wrote, n);
	if (stat == 0) {
	    return stat;
	}
	n_wrote += n;
    }

    return n_wrote;
}

int
httplib_recv(HttpLibHandle *handle, void *buffer, int len)
{
    if (handle->is_ssl) {
        return recv_data_ssl(handle->ssl, buffer, len);
    } else {
        return recv(handle->socket_fd, buffer, len, 0);
    }
}

void
httplib_close(HttpLibHandle *handle)
{
    if (handle->is_ssl) {
        close_ssl_connection(handle->ssl);
    } else {
        close(handle->socket_fd);
    }

    free(handle);
}


/* ---------------------------------------------------------------------- * 
 *        Send HTTP POST message                                          * 
 * ---------------------------------------------------------------------- */

/*
 * send a POST message along with specified attributes
 * return HTTP response status value
 */
int
httplib_post(HttpLibHandle *handle,
             char *uri,
             int nattrs,
             HttpLibAttribute *attrs)
{
    return httplib_post_timeout(handle, uri, nattrs, attrs, NULL, 0, 0);
}

/*
 * send a POST message along with specified attributes.
 * call "tick_callback()" function every "tick" second, and will timeout
 * when ticks "tick_timeout" times.
 *
 * return HTTP response status value
 */
int
httplib_post_timeout(
              HttpLibHandle *handle,
              char *uri,
              int nattrs,
              HttpLibAttribute *attrs,
              void (*tick_callback)(int),
              int tick,
              int tick_timeout)
{
    char header[1024], *content, *ptr;
    int i, n, size;

    /* send POST message to the server: The message is:
     *  "POST <URI> HTTP/1.0\r\n" +
     *      "Accept: text/html, image/gif ...\r\n" +
     *      "Host: <host>\r\n" +
     *      "Content-Type: application/x-www-form-urlencoded\r\n" +
     *      "Content-Length: nnn\r\n" +
     *      "Connection: [ Keep-Alive | close ]\r\n" +
     *      "\r\n" +    <- end of HTTP header
     *      "<URL-ENCODED-BODY>"
     */

    /* check size of contents */
    size = 0;
    for (i=0; i<nattrs; i++) {
        size += strlen((attrs+i)->name) + 1 + 1;
        size += (strlen((attrs+i)->value) + 1) * 3 + 3;
    }
    ptr = content = (char *)malloc(size);
    for (i=0; i<nattrs; i++) {
        strcpy(ptr, attrs->name);
        strcat(ptr, "=");
        ptr += strlen(ptr);
        ptr += url_encode(attrs->value, ptr);
        *ptr++ = '&';  /* extra '&' at the tail should be okay */
        *ptr = '\0';
        attrs++;
    }

    /* send header */
    sprintf(header, "POST %s HTTP/1.0\r\nAccept: text/html, image/gif, image/jpeg, */*\r\nHost: %s\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: %d\r\nConnection: %s\r\n\r\n",
        uri, handle->hostname, strlen(content), ( handle->keep_alive ? "Keep-Alive" : "close")
    );

    n = httplib_send(handle, header, strlen(header));

    /* send buffer */
    n = httplib_send(handle, content, strlen(content));

    return read_response(handle, tick_callback, tick, tick_timeout, 0);
}


/* ---------------------------------------------------------------------- * 
 *                Functions to read a response from the server            * 
 * ---------------------------------------------------------------------- */

/*
 * read all data from the server.
 * use SIGALARM to display "wait bar" every second
 * returns HTTP response number, -1 for internal error, or -2 for timeout
 */
int
read_response(HttpLibHandle *handle,
              void (*tick_callback)(int),
              int tick, int tick_timeout, int show_response)
{
    char buff[4096], *cp;
    int n, retval, wait_cnt;
    fd_set rfds;
    struct timeval tv;

    wait_cnt = 0;
    cp = &buff[0];

    for(;;) {

        FD_ZERO(&rfds);
        FD_SET(handle->socket_fd, &rfds);
        if (tick_timeout > 0) {
            tv.tv_sec = tick;
            tv.tv_usec = 0;
            retval = select(handle->socket_fd+1, &rfds, NULL, NULL, &tv);

        } else {
            retval = select(handle->socket_fd+1, &rfds, NULL, NULL, (struct timeval *)NULL);
        }
        
        if (retval > 0 && FD_ISSET(handle->socket_fd, &rfds)) {
            /* data is available */
            wait_cnt = 0;
            n = httplib_recv(handle, cp, sizeof(buff) - (cp-&buff[0]));
            if (n < 0) {
                /* error occurs */
                return -1;

            } else if (n == 0) {
                /* end of data */
                break;
            }

        } else if (retval < 0) {
            /* error occured */
            return -1;

        } else {
            /* (tick) sec elapsed */

            n = 0;
            wait_cnt++;
            if (wait_cnt >= tick_timeout) {
                return -2;
            }
        }

        if (tick_callback != NULL) {
            tick_callback(wait_cnt);
        }
        cp += n;
    }

    /* check the response from the server:
     *
     * HTTP response starts with "HTTP/n.m <status>\r\n"
     * Pick <status> to determin the result.
     */
    *cp = '\0';
    cp = &buff[0];
    while (*cp != ' ' && *cp != '\0')
        cp++;

    if (*cp == '\0') {
        /* illegal format; return as internal error */
        return -1;

    }

    if (show_response) {
	/* look for "\r\n\r\n"
	 */
	char *d = strstr(buff, "\r\n\r\n");
	if (d == NULL) {
	    printf("\nResponse: %d", atoi(cp+1));
	} else {
	    printf("\nResponse:\n\n%s", d+4);
	}
    }

    return atoi(cp+1);
}

/*
 * read one line from the server.
 * use SIGALARM to display "wait bar" every second
 * returns HTTP response number, -1 for internal error, or -2 for timeout
 */
int
read_line(HttpLibHandle *handle,
              void (*tick_callback)(int),
              int tick, int tick_timeout,
	      char *buff /* OUT */, int buflen)
{
    char *cp;
    int n, retval, wait_cnt;
    fd_set rfds;
    struct timeval tv;

    wait_cnt = 0;
    cp = buff;

    for(;(cp - buff) < buflen-1;) {

        FD_ZERO(&rfds);
        FD_SET(handle->socket_fd, &rfds);
        if (tick_timeout > 0) {
            tv.tv_sec = tick;
            tv.tv_usec = 0;
            retval = select(handle->socket_fd+1, &rfds, NULL, NULL, &tv);

        } else {
            retval = select(handle->socket_fd+1, &rfds, NULL, NULL, (struct timeval *)NULL);
        }
        
        if (retval > 0 && FD_ISSET(handle->socket_fd, &rfds)) {
            /* data is available */
            wait_cnt = 0;
            n = httplib_recv(handle, cp, 1);
            if (n < 0) {
                /* error occurs */
                return -1;

            } else if (n == 0) {
                /* end of data */
                break ;
            }

	    /* check if it reaches the end of line */
	    if (*cp == '\n') {
		/* reached the end of line */
		cp++;
		break;

	    }

        } else if (retval < 0) {
            /* error occured */
            return -1;

        } else {
            /* (tick) sec elapsed */

            n = 0;
            wait_cnt++;
            if (wait_cnt >= tick_timeout) {
                return -2;
            }
        }

        if (tick_callback != NULL) {
            tick_callback(wait_cnt);
        }
        cp += n;
    }

    *cp = '\0';

    return cp - buff;
}


/* ---------------------------------------------------------------------- * 
 *            Function to encode a string in "% HEX HEX" form             * 
 * ---------------------------------------------------------------------- */

/*
 * encode non-ascii into "%HH" format, and space ' ' into '+'
 * characters not to be encoded are:
 *    a-z, A-Z, 0-9, @, *, _, -, .
 * return size of output
 */
int
url_encode(char *in, char *out)
{
    int len=0;

    while (*in != '\0') {
        int c = (int)*((unsigned char *)in);
        if (is_urlchar(c)) {
            *out++ = *in;
            len++;

        } else if (c == ' ') {
            *out++ = '+';
            len++;

        } else {
            /* encode to "%HH" */
            *out++ = '%';
            *out++ = (char)get_hex_char(c / 16);
            *out++ = (char)get_hex_char(c % 16);
            len += 3;
        }

        in++;
    }

    *out = '\0';

    return len;
}

int
is_urlchar(int c)
{
    if ((c >= 'a' && c <= 'z') ||
        (c >= 'A' && c <= 'Z') ||
        (c >= '0' && c <= '9') ||
         c == '@' || c == '*' || c == '_' || c == '-' || c == '.')
            return -1;
    else
            return 0;
}

int get_hex_char(int value)
{
    if (value < 10)
        return '0' + value;
    else
        return 'A' + (value - 10);
}

#define	DEFAULT_HTTP_PORT	(80)
#define	DEFAULT_HTTPS_PORT	(443)

/* ---------------------------------------------------------------------- * 
 *            Utility for HTTP/URL handlings                              * 
 * ---------------------------------------------------------------------- */
void
url_extract(const char *url, char *hostname, char *uri, int *port)
{
    const char *ptr=url;
    *hostname = '\0';
    *uri = '\0';

    if (strncmp(url, "http:", 5) == 0) {
	*port = DEFAULT_HTTP_PORT;
	ptr += 5;
    } else if (strncmp(url, "https:", 5) == 0) {
	*port = DEFAULT_HTTPS_PORT;
	ptr += 6;
    } else {
	return;
    }

    while (*ptr && *ptr == '/') ptr++;
    if (*ptr == '\0') {
	return;
    }
    /* extract hostname (port will be included) */
    while (*ptr && (*ptr != '/' && *ptr != ':')) {
	*hostname = *ptr;
	ptr++;
	hostname++;
    }
    *hostname = '\0';
    if (*ptr == '\0') {
	return;
    } else if (*ptr == ':') {
	*port = atoi(ptr + 1);
	while (*ptr && *ptr != '/') ptr++;
    }
    /* extract uri */
    while (*ptr) {
	*uri = *ptr;
	uri++;
	ptr++;
    }
    *uri = '\0';
}


/* send file content
 */
int
send_file(HttpLibHandle *handle, FILE *ff)
{
#define	BUFF_SIZE	(1024)
    char buf[BUFF_SIZE];
    int nread, st;
    long total = 0;

    while (!feof(ff)) {
	nread = fread(buf, 1, BUFF_SIZE, ff);
	if (nread <= 0) {
	    break;
	}
	st = httplib_send(handle, buf, nread);
	total += st;
    }

    return total;
}

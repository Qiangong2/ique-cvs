/*
 * header file for ssl_wrapper.c
 */
#include <openssl/ssl.h>
#include <openssl/rand.h>
#include <openssl/err.h>
#include "ssl_wrapper.h"

extern SSL_WRAPPER *Ssl_Connect(char *hostname,
                                int portnumber,
                                char *cert_filename,
                                char *ca_filename,
                                char *key_filename,
                                char *keyfile_passwd,
                                char *cname);

extern int send_data_ssl(SSL_WRAPPER *handle, void *buffer, int len);

extern int recv_data_ssl(SSL_WRAPPER *handle, void *buffer, int len);

extern void close_ssl_connection(SSL_WRAPPER *handle);

extern int get_ssl_sock_fd(SSL_WRAPPER *handle);



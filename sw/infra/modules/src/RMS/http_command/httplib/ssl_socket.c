/*
 * This is a source file inherited from rf/src/hr/cmd/sm/connectstk.c
 */
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <sys/time.h>
#include <sys/stat.h> /* for stat() calls */

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <sys/param.h>
#include <sys/types.h>
#include <sys/file.h>

#include <netinet/in_systm.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <netdb.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include "ssl_socket.h"

/* Global declarations */

#define LOG_FACILITY LOG_DAEMON

#ifdef SKID_MARKS
#define SKID_MARKS_MACRO {printf("DEBUG:%s %d pid %d\n", __FILE__, __LINE__,getpid());fflush(stdout);}
#else
#define SKID_MARKS_MACRO 
#endif /* SKID_MARKS */


#ifndef TRUE
#define TRUE 1
#define FALSE 0
#endif

/* Global declarations */

/* Forward declarations */
int SSL_wrapper_read_retry (SSL_WRAPPER* ssl_wrapper, char* buf, int buf_len);

/* Extern declarations */

/* Publicly-accessible routines */

/* return SSL_WRAPPER if successful; 0 for error */
SSL_WRAPPER *
Ssl_Connect(char *hostname,
            int portnumber,
            char *cert_filename,
            char *ca_filename,
            char *key_filename,
            char *keyfile_passwd,
            char *cname)
{
    int result;
    SSL_WRAPPER *SSL_handle;
    struct stat stat_buf;

    if (stat(cert_filename, &stat_buf) < 0) {
      fprintf(stderr,"certificate file: %s does not exist.\n", cert_filename);
      return (SSL_WRAPPER *)0;
    }
    if (stat(key_filename, &stat_buf) < 0) {
      fprintf(stderr,"key file: %s does not exist.\n", key_filename);
      return (SSL_WRAPPER *)0;
    }
    if (stat(ca_filename, &stat_buf) < 0) {
      fprintf(stderr,"certificate authority file: %s does not exist.\n", ca_filename);
      return (SSL_WRAPPER *)0;
    }
    errno = 0;
    SSL_handle = (void *) new_SSL_wrapper (
			 cert_filename, NULL,
                         key_filename, keyfile_passwd, ca_filename, 0);

    if (SSL_handle == (void *) NULL) {
      fprintf(stderr,"Error: new_SSL_wrapper returned NULL\n");
      result = 0;

    } else {
       errno = 0;
       result = SSL_wrapper_connect(SSL_handle, hostname, portnumber);
    }

    /* SSL_wrapper_connect() returns TRUE iff the connection
     * succeeded.
     */

    if (! result ) {
       fprintf(stderr,"Cannot connect to %s (using SSL)\n", hostname);
       return (SSL_WRAPPER *)0;
    }

    if (strcmp(cname, "no-check") != 0) {
	if (!SSL_wrapper_verify_peer_cert(SSL_handle, cname)) {
	   fprintf(stderr,"Cannot verify peer certificate, %s\n", hostname);
	   return (SSL_WRAPPER *)0;
	}
    }

    return SSL_handle;
}

int
send_data_ssl(SSL_WRAPPER *handle, void *buffer, int len)
{
    return(SSL_wrapper_write(handle, buffer, len));
}

int
recv_data_ssl(SSL_WRAPPER *handle, void *buffer, int len)
{
    return(SSL_wrapper_read_retry(handle, buffer, len));
}

void
close_ssl_connection(SSL_WRAPPER *handle)
{
    SSL_wrapper_disconnect(handle);
    delete_SSL_wrapper(handle);
    return;
}

int
get_ssl_sock_fd(SSL_WRAPPER *handle)
{
   return SSL_wrapper_get_sockfd(handle);
}


#define NUM_RETRY	3

int
SSL_wrapper_read_retry (SSL_WRAPPER* ssl_wrapper, char* buf, int buf_len)
{
    int i = 0;
    int ret = 0;
    int err = 0;

    if (ssl_wrapper == 0 || ! ssl_wrapper->connected) {
#ifdef DEBUG
        if (ssl_wrapper == 0)
            fprintf(stderr,"SSL_wrapper_read_retry: ssl_wrapper == 0\n");
        else
            fprintf(stderr,"SSL_wrapper_read_retry: ssl_wrapper not connected.");
#endif
	return -1;
    }

    /* Need to check for re-negotiation that can occur when server is configured
       with no client authentication in general but requires client 
       authentication for certain directories.
       So, when we receive -1 from SSL_read(), we need to call SSL_get_error()
       to check if the error is SSL_ERROR_WANT_READ and then retry SSL_read() 
       again.
     */
    ret = SSL_read (ssl_wrapper->ssl, buf, buf_len);
    for (i=0; i < NUM_RETRY; i++) {
        if (ret == -1) {
            err = SSL_get_error(ssl_wrapper->ssl, ret);
            if (err == SSL_ERROR_WANT_READ) {
#ifdef DEBUG
                fprintf(stderr, "SSL_wrapper_read_retry: SSL error=%d; retry SSL_read() again...\n",err);
#endif
                ret = SSL_read (ssl_wrapper->ssl, buf, buf_len);
            }
            else break;
        }
        else break;
    }
    return ret;
}



/*
 * command line command to post status update of HomeRouter orders
 * to the big brain over HTTP over SSL.
 */
#include <stdio.h>
#include <stdlib.h>
#include "httplib.h"

#undef DEBUG

#define CERT_OPTION	"-cert"
#define CA_OPTION	"-ca"
#define KEY_OPTION	"-key"
#define PASSWORD_OPTION	"-password"

/* Servlet Site Definitions */
#define	SERVLET_URI	"/hrcenter/orderhr"
#define	SERVICE_NAME	"Binding Server"
#define	HTTPS_PORT	(443)
#define	HTTP_PORT	(80)

static char *cert_file = (char *)NULL;
static char *key_file = (char *)NULL;
static char *key_password = (char *)NULL;
static char *ca_file = (char *)NULL;

#define	UNKNOWN	(0)
#define	BETA	(1)
#define	FINAL	(2)
static int non_last_good = UNKNOWN;


/* forward declaration */
extern int parse_args(int argc, char *argv[]);
extern void ticker(int tick);
#ifdef	KEEP_ALIVE
extern int process_one_file(HttpLibHandle *handle, char *filename);
extern int process_one_line(HttpLibHandle *handle, char *s);
#else
extern int process_one_file(char *hostname, char *filename);
extern int process_one_line(char *hostname, char *s);
#endif	/* KEEP_ALIVE */

print_usage(char *cmd)
{
    printf("Usage: %s [ <security options> ] <servlet host> filename [ filename ... filename ]\n\n", cmd);
    printf("     Servlet host: Hostname of the servlet\n");
    printf("     filenames: A list of files  containing status update information.\n");
    printf("                Contents of the file is a text of comma delimited lines:\n\n");
    printf("                     <Account ID>,<New Status>      or\n");
    printf("                     <Account ID>,4,<Tracking #>\n\n");
    printf("                <Account ID> .. user's account ID\n");
    printf("                <New Status> .. New staus:\n");
    printf("                                '1' .. Ordered\n");
    printf("                                '2' .. Pending Credit Approval\n");
    printf("                                '3' .. Waiting for Shipping\n");
    printf("                                '4' .. Shipped\n");
    printf("                <Tracking #> .. Tracking # from FedEx/UPS/etc\n");
    printf("\n");
    printf("  Security Options (all mandatory for HTTP over SSL)\n");
    printf("     %s <certificate file name>\n", CERT_OPTION);
    printf("     %s <key file name (pem encoded)>\n", KEY_OPTION);
    printf("     %s <certificate authority file name>\n", CA_OPTION);
    printf("     %s <password for the key file>\n", PASSWORD_OPTION);
    printf("\n");
}

int
main(int argc, char *argv[])
{
    int nargs;
    char *hostname;
#ifdef	KEEP_ALIVE
    HttpLibHandle *handle;
#endif /* KEEP_ALIVE */

    nargs = parse_args(argc, argv);

    if ((argc-nargs) < 2) {
        print_usage(argv[0]);
        printf("\nAt least, servlet host & a file name needs to be specified.\n");
        exit(-1);
    }

    if (!(cert_file && key_file && key_password && ca_file) &&
        !(!cert_file && !key_file && !key_password && !ca_file)) {
        print_usage(argv[0]);
        printf("\nAll or no security information should be provided.\n");
        exit(-1);
    }

    hostname = argv[nargs++];

#ifdef	KEEP_ALIVE
    /* open the connection */

    if (cert_file) {
        printf("Using HTTP over SSL connection.\n");
        handle = httplib_connect_ssl(hostname, HTTPS_PORT, 
                                     cert_file,
                                     ca_file,
                                     key_file,
                                     key_password,
                                     SERVICE_NAME,
                                     TRUE);  /* keep it alive */
    } else {
        printf("Using Regular HTTP connection.\n");
        handle = httplib_connect(hostname, HTTP_PORT, TRUE);
    }

    if (!handle) {
        /* connection cannot be made */
        printf("Unable to establish a connection to %s://%s%s.\n",
            (cert_file ? "https" : "http"), 
            hostname, SERVICE_NAME);
        exit(-1);
    }
#else
    if (cert_file) {
        printf("Using HTTP over SSL connection.\n");
    } else {
        printf("Using Regular HTTP connection.\n");
    }
#endif /* KEEP_ALIVE */

    for (; nargs < argc; nargs++) {

        printf("Processing the file %s ...\n", argv[nargs]);
#ifdef	KEEP_ALIVE
        if (process_one_file(handle, argv[nargs]) == FALSE)
#else
        if (process_one_file(hostname, argv[nargs]) == FALSE)
#endif /* KEEP_ALIVE */

        {
            /* something happened */
            printf("Processing the file %s has been terminated\n", argv[nargs]);
#ifdef	KEEP_ALIVE
            httplib_close(handle);
#endif /* KEEP_ALIVE */
            exit(-1);
        }
        printf("Completed processing the file %s.\n", argv[nargs]);
    }

#ifdef	KEEP_ALIVE
    httplib_close(handle);
#endif /* KEEP_ALIVE */

    exit(0);
}



/* return TRUE if successful
 */
int
#ifdef	KEEP_ALIVE
process_one_file(HttpLibHandle *handle, char *filename)
#else
process_one_file(char *hostname, char *filename)
#endif /* KEEP_ALIVE */
{
#define	BUFF_SIZE	(1024)
    char buf[BUFF_SIZE];
    int lineno = 1;
    FILE *fd = fopen(filename, "r");
    if (fd == NULL) {
        printf("File %s not found\n", filename);
        return FALSE;
    }

    while (fgets(buf, BUFF_SIZE, fd) != NULL) {
        /* trancate tailing \r \n */
        int len = strlen(buf) - 1;
        while (len >=0 && (buf[len] == '\r' || buf[len] == '\n')) {
            buf[len] = '\0';
            len--;
        }
#ifdef	KEEP_ALIVE
        if (process_one_line(handle, buf) == FALSE)
#else
        if (process_one_line(hostname, buf) == FALSE)
#endif /* KEEP_ALIVE */
        {
            printf("Error on file %s, Line %d\n", filename, lineno);
            return FALSE;
        }
        ++lineno;
    }

    fclose(fd);

    return TRUE;
}

/*
 * This is NOT multithread safe function
 */
int
#ifdef	KEEP_ALIVE
process_one_line(HttpLibHandle *handle, char *s)
#else
process_one_line(char *hostname, char *s)
#endif /* KEEP_ALIVE */
{
    HttpLibAttribute attrs[4]; /* action, acctid, status, tracking */
    int nattrs, ret, stat;
#ifdef DEBUG
    int i;
#endif /* DEBUG */
    char *ptr;
#ifndef	KEEP_ALIVE
    HttpLibHandle *handle;
#endif /* !KEEP_ALIVE */

#ifndef	KEEP_ALIVE
    /* open the connection */

    if (cert_file) {
        handle = httplib_connect_ssl(hostname, HTTPS_PORT, 
                                     cert_file,
                                     ca_file,
                                     key_file,
                                     key_password,
                                     SERVICE_NAME,
                                     FALSE);  /* don't keep it alive */
    } else {
        handle = httplib_connect(hostname, HTTP_PORT, FALSE);
    }

    if (!handle) {
        /* connection cannot be made */
        printf("Unable to establish a connection to %s://%s%s.\n",
            (cert_file ? "https" : "http"), 
            hostname, SERVICE_NAME);
        exit(-1);
    }
#endif /* !KEEP_ALIVE */

    nattrs=1;
    attrs[0].name  = "action";
    attrs[0].value = "update_status";

    attrs[1].name  = "acctid";
    attrs[2].name  = "status";
    attrs[3].name  = "tracking";

    /* parse the string
     *   acctid,status
     *       or        
     *   acctid,4,tracking
     */
    attrs[nattrs++].value = ptr = s;
    while (*ptr && *ptr != ',') ptr++;
    if (*ptr != ',') {
        printf("Error: Invalid parameters\n");
        ret = FALSE;
        goto done;
    }
    *ptr++ = '\0';

    attrs[nattrs++].value = ptr;
    while (*ptr && *ptr != ',') ptr++;
    if (*ptr == ',') {
        *ptr++ = '\0';
        /* tracking number is specified */
        attrs[nattrs++].value = ptr;
    }

    /* post the data */
#ifdef	DEBUG
    for (i=0; i<nattrs; i++) {
        printf("%d: %s=%s\n", i, attrs[i].name, attrs[i].value);
    }
#endif /* DEBUG */
     
    stat = httplib_post_timeout(
                    handle, SERVLET_URI, nattrs, attrs, ticker, 1, 60);

    printf("\n");
    switch (stat) {
        case 200: /* success */
            printf("Updated for User %s: Status=%s\n",
                      attrs[1].value, attrs[2].value);
            ret = TRUE;
            break;

        case 400: /* invalid parameters */
            printf("Error: Invalid parameters\n");
            ret = FALSE;
            break;

        case 410: /* no such user */
            printf("Error: No Such User\n");
            ret = FALSE;
            break;

        case 409: /* no order for the user */
            printf("Error: The user has not placed the order\n");
            ret = FALSE;
            break;

        default: /* server error */
            printf("Error: Server Internal Error. Try later.\n");
            ret = FALSE;
            break;
    }

done:

#ifndef	KEEP_ALIVE
    httplib_close(handle);
#endif /* !KEEP_ALIVE */

    return ret;
}


void
ticker(int tick)
{
    printf("*");
    fflush(stdout);
}

int
parse_args(int argc, char *argv[])
{
    int i;

    for (i=1; i<argc; i++) {
        if (strcmp(argv[i], CERT_OPTION) == 0) {
            cert_file = argv[++i];

        } else if (strcmp(argv[i], CA_OPTION) == 0) {
            ca_file = argv[++i];

        } else if (strcmp(argv[i], KEY_OPTION) == 0) {
            key_file = argv[++i];

        } else if (strcmp(argv[i], PASSWORD_OPTION) == 0) {
            key_password = argv[++i];

        } else {
            break;
        }
    }

    return i;
}



/*
 * command line command to do https post
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include "httplib.h"

#define URL_OPTION	"-url"
#define CERT_OPTION	"-cert"
#define CA_OPTION	"-ca"
#define KEY_OPTION	"-key"
#define PASSWORD_OPTION	"-password"
#define CNAME_OPTION	"-ou"
#define PARAM_OPTION	"-p"
#define MAX_ARG_COUNT	(13)

#define FILEIN_OPTION	"-i"
#define FILEOUT_OPTION	"-o"
#define REPEAT_OPTION	"-r"
#define TIMEOUT_OPTION	"-t"

/* Servlet Site Definitions */
#define	SERVICE_NAME	"Status Receiver"

static char *url = (char *)NULL;
static char *cert_file = (char *)NULL;
static char *key_file = (char *)NULL;
static char *key_password = (char *)NULL;
static char *ca_file = (char *)NULL;
static char *param = (char *)NULL;
static char *in_file = (char *)NULL;
static char *out_file = (char *)NULL;
static char *repeat = (char *)NULL;
static char *ou = "no-check";
static int timeout = 300;

struct line {
    char *data;
    struct line *next;
} *first_line = NULL, *last_line = NULL;

/* forward declaration */
extern void print_usage(char *cmd);
extern int parse_args(int argc, char *argv[]);
extern void new_line(char *data);
extern int read_file(char *filename);
extern void post_and_receive();
extern int send_post_header(HttpLibHandle *handle, char *uri, unsigned long clen);
extern void ticker(int tick);

int main(int argc, char *argv[])
{
    int iArgs = 0;
    
    int iLineCount = 0, iCount = 0;
    struct line *lptr, *tmp;
    char *pData = NULL;
    char *ppArgs[MAX_ARG_COUNT];
    int iArgCount = 0;
    int iRepeat = 1;
    int idx;
    FILE *fptr, *ptr;

    iArgs = parse_args(argc, argv);
    if (iArgs==1) {
        print_usage(argv[0]);
        exit(-1);
    }
    if ((out_file || repeat) && (!url && !in_file)) {
        printf("\nPost request parameters must be specified.\n");
        print_usage(argv[0]);
        exit(-1);
    }
    if (!in_file && (!url || !cert_file || !key_file || !ca_file)) {
        printf("\nSecurity information for SSL is incomplete. \n");
        print_usage(argv[0]);
        exit(-1);
    }

    if (repeat)
        iRepeat = atoi(repeat);
   
    if (out_file) {
        if ((fptr=fopen(out_file, "w"))==NULL) {
            printf("Unable to open output file for writing!\n");
            exit(-1);
        }
        ptr = fptr;
    } else
        ptr = stdout;
  
    if (in_file) {
        iCount = read_file(in_file);
        for (idx=1; idx<=iRepeat; idx++) {
            printf("===============================================\n");
            printf("Number of Post Request: %d, Run Count: %d\n\n", iCount, idx);
            
            iLineCount = 1;
            for (lptr=first_line; lptr; lptr=lptr->next, iLineCount++) {
                fprintf(ptr, "--------------------------------------------\n");
                fprintf(ptr, "Request #%d, Run #%d: \n", iLineCount, idx);
                fprintf(ptr, "%s\n", lptr->data);
                fprintf(ptr, "--------------------------------------------\n\n");

                //parse parameters
                pData = (char *)malloc(strlen(lptr->data)+1);
                strcpy(pData, lptr->data);
                ppArgs[0] = strtok(pData, " ");
                iArgCount = 1;
                while ((ppArgs[iArgCount] = strtok(NULL, " ")) != NULL) 
                    iArgCount ++; 
                parse_args(iArgCount, ppArgs);

                printf("Request #%d: \n", iLineCount);
                post_and_receive(ptr);

                //reset parameters for next line
                free (pData);
                url = (char *)NULL;
                cert_file = (char *)NULL;
                key_file = (char *)NULL;
                ca_file = (char *)NULL;
                key_password = (char *)NULL;
                param = (char *)NULL;
            }
       }       

       //clean up memory
       iLineCount = 0;
       for (lptr=first_line; lptr; iLineCount ++) {
           tmp = lptr; 
           lptr = lptr->next;
           free (tmp->data);
           free (tmp);
       }
    } else {
        for (idx=1; idx<=iRepeat; idx++) {
            post_and_receive(ptr);
        }
    }

    if (out_file)  fclose(ptr);

    return 0;
}

void print_usage(char *cmd)
{
    printf("----------------------------------------------------------------------\n");
    printf("Usage: %s <command line options | input file option> \n", cmd);
    printf("                  [output fle name] [repeat]\n\n");
    printf("<comand line options> (all mandatory)\n");
    printf("    %s <servlet URL>\n", URL_OPTION);
    printf("    %s <certificate file name>\n", CERT_OPTION);
    printf("    %s <key file name (pem encoded)>\n", KEY_OPTION);
    printf("    %s <certificate authority file name>\n", CA_OPTION);
    printf("    %s <password for the key file>\n", PASSWORD_OPTION);
    printf("    %s <request parameters in the form of \"name=value\" pairs seperated by \"&\">\n", PARAM_OPTION);
    printf("\n");
    printf("<input file option>\n");
    printf("     %s <input file containing the command-line(s), each line is in the form of \"%s <command line options>\">\n", FILEIN_OPTION, cmd);
    printf("\n");
    printf("<output file name> \n");
    printf("    %s <output file name to write the response to, defaults to stdout>\n", FILEOUT_OPTION);
    printf("\n");
    printf("<repeat>\n");
    printf("    %s <number of times to send the request, defaults to 1", REPEAT_OPTION);
    printf("\n");
    printf("----------------------------------------------------------------------\n");
}

int parse_args(int argc, char *argv[])
{
    int i;

    for (i=1; i<argc; i++) {
        if (strcmp(argv[i], CERT_OPTION) == 0) {
            cert_file = argv[++i];
        } else if (strcmp(argv[i], CA_OPTION) == 0) {
            ca_file = argv[++i];
        } else if (strcmp(argv[i], KEY_OPTION) == 0) {
            key_file = argv[++i];
        } else if (strcmp(argv[i], PASSWORD_OPTION) == 0) {
            key_password = argv[++i];
        } else if (strcmp(argv[i], URL_OPTION) == 0) {
            url = argv[++i];
        } else if (strcmp(argv[i], PARAM_OPTION) == 0) {
            param = argv[++i];
        } else if (strcmp(argv[i], CNAME_OPTION) == 0) {
            ou = argv[++i];
        } else if (strcmp(argv[i], FILEIN_OPTION) == 0) {
            in_file = argv[++i];
        } else if (strcmp(argv[i], FILEOUT_OPTION) == 0) {
            out_file = argv[++i];
        } else if (strcmp(argv[i], REPEAT_OPTION) == 0) {
            repeat = argv[++i];
        } else if (strcmp(argv[i], TIMEOUT_OPTION) == 0) {
            timeout = atoi(argv[++i]);
        } else {
            break;
        }
    }

    return i;
}

void new_line(char *data)
{
    struct line *new_ptr;

    /* malloc new entry */
    new_ptr = (struct line *)malloc(sizeof(*new_ptr));
    if (new_ptr==NULL) {
        printf("Malloc error!\n");
        exit(-1);
    }

    /* fill entry */
    new_ptr->next = NULL;
    new_ptr->data = (char *)malloc(strlen(data) + 1);
    if (new_ptr->data==NULL) {
        printf("Malloc error!\n");
        exit(-1);
    }
    strcpy(new_ptr->data, data);

    /* insert entry at end of list */
    if (last_line==NULL) {
      first_line = last_line = new_ptr;
    } else {
        last_line->next = new_ptr;
        last_line = new_ptr;
    }
}

int read_file (char *filename)
{
    FILE *fptr;
    char data[20480];
    int count = 0;

    if ((fptr=fopen(filename, "r"))==NULL) {
        fprintf(stderr, "Unable to open file %s\n", filename);
        exit(-1);
    }

    while (fgets(data, 20479, fptr)) {
        if (data[0]) {
           data[strlen(data)-1] = 0;
           new_line(data);
           count ++;
        }
    }

    fclose(fptr);

    return count;
}

void post_and_receive(FILE* ptr)
{
    #define LINE_BUF_SIZE  (20480)
    char uri[1024], hostname[256],  ln[LINE_BUF_SIZE];
    HttpLibHandle *handle;
    unsigned long i, clen = 0, j;
    int st, in_header, is_first, ret, port;

    url_extract(url, hostname, uri, &port);

    /* connect to the server via SSL*/
    handle = httplib_connect_ssl(hostname, port, cert_file, ca_file, key_file, key_password, ou, FALSE /*keep-alive false*/); 
    if (!handle) {
        printf("Unable to establish a connection to %s\n", url);
        exit(-1);
    }

    // send header
    clen = strlen(param);
    st = send_post_header(handle, uri, clen);
    printf("sent http header %d\n", st);
    printf("Content-Length: %ld\n", clen);

    // send request param
    // convert '/' to '\n'
    for (i=0, j=0; i<clen; i++,j++) {
	if (param[i] == '/') {
	    if (i<1 || param[i-1] != '\\') {
		param[j] = '\n';
	    } else {
		j--;
		param[j] = '/';
	    }
	} else if (i != j) {
	    param[j] = param[i];
	}
    }
    param[j] = '\0';
    st = httplib_send(handle, param, clen);
    printf("sent request %d\n\n", st);
   
    // get response
    in_header = TRUE;
    is_first = TRUE;
    while (( ret = read_line(handle, ticker, 1, timeout, ln, LINE_BUF_SIZE) > 0)) {
        fprintf(ptr, "%s", ln);
/*
        if (in_header) {
            fprintf(ptr, "%s", ln);
            if (strcmp(ln, "\r\n") == 0) {
                in_header = FALSE;
            } else {
                if (is_first) {
                    sscanf(ln, "%s %d", buf, &res_code);
                    fprintf(ptr, "(Return Code: %d)\n", res_code);
                    is_first = FALSE;
                }
            }
        } else {
            fprintf(ptr, "%s", ln);
        }
*/
    }

    httplib_close(handle);
}

int send_post_header(HttpLibHandle *handle, char *uri, unsigned long clen)
{
    char header[1024];

    /* send POST message to the server: The message is:
     *  "POST <URI> HTTP/1.0\r\n" +
     *      "Accept: text/html, image/gif ...\r\n" +
     *      "Host: <host>\r\n" +
     *      "Content-Type: application/x-www-form-urlencoded\r\n" +
     *      "Content-Length: NNN\r\n" +
     *      "Connection: close\r\n" +
     *      "\r\n" +    <- end of HTTP header
     */

    /* send header */
    sprintf(header, "POST %s HTTP/1.0\r\nAccept: text/html, image/gif, image/jpeg, */*\r\nHost: %s\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: %ld\r\nConnection: close\r\n\r\n", uri, handle->hostname, clen);

    return httplib_send(handle, header, strlen(header));
}

void ticker(int tick)
{
    /* do nothing */
}

/*
 * command line command to post new software release description
 * to the big brain over HTTP over SSL.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include "httplib.h"

#define URL_OPTION	"-url"
#define CERT_OPTION	"-cert"
#define CA_OPTION	"-ca"
#define KEY_OPTION	"-key"
#define CNAME_OPTION	"-ou"
#define PASSWORD_OPTION	"-password"

/* Servlet Site Definitions */
#define	SERVICE_NAME	"Status Receiver"

/* boolean */
#define	TRUE	(-1)
#define	FALSE	(0)

static char *url = (char *)NULL;
static char *cert_file = (char *)NULL;
static char *key_file = (char *)NULL;
static char *key_password = (char *)NULL;
static char *ca_file = (char *)NULL;
static char *ou = "no-check";

/* forward declaration */
extern int send_post_header(HttpLibHandle *handle, char *uri, unsigned long clen);
extern int parse_args(int argc, char *argv[]);
extern void ticker(int tick);

void
print_usage(char *cmd)
{
    printf("Usage: %s <HTTPS options> <Module Activation Doc>\n\n", cmd);
    printf("     <HTTPS Options> (all mandatory)\n");
    printf("         %s <servlet URL>\n", URL_OPTION);
    printf("         %s <certificate file name>\n", CERT_OPTION);
    printf("         %s <key file name (pem encoded)>\n", KEY_OPTION);
    printf("         %s <certificate authority file name>\n", CA_OPTION);
    printf("         %s <password for the key file>\n", PASSWORD_OPTION);
    printf("\n");
    printf("     <Module Activation Doc>:\n");
    printf("              path name of the XML document describing\n");
    printf("              the module activation/deactivation request\n");
    printf("\n");
}

int
main(int argc, char *argv[])
{
#define	LINE_BUF_SIZE	(4096)
    int nargs, st, ret, in_header, is_first, res_code, port;
    unsigned long clen;
    char uri[1024], hostname[256], ln[LINE_BUF_SIZE], buf[LINE_BUF_SIZE],
	*act_req_doc;
    HttpLibHandle *handle;
    FILE *maf;
    struct stat statbuf;

    nargs = parse_args(argc, argv);

    if ((argc-nargs) < 1) {
        print_usage(argv[0]);
        printf("\nModule activation document must be specified.\n");
        exit(-1);
    }

    if (!url || !cert_file || !key_file || !ca_file) {
        print_usage(argv[0]);
        printf("\nSome security information is not specified.\n");
        exit(-1);
    }

    /* check if the file is readable, and its size
     */
    act_req_doc = argv[nargs];
    st = stat(act_req_doc, &statbuf);
    if (st < 0 || !(statbuf.st_mode & 0444)) {
	printf("Unabel to access to the activation document %s\n", act_req_doc);
	exit(-1);
    }
    clen = statbuf.st_size;

    /* now, connect to the server and send data
     */
    url_extract(url, hostname, uri, &port);

    handle = httplib_connect_ssl(hostname, port, 
                                     cert_file,
                                     ca_file,
                                     key_file,
                                     key_password,
                                     ou,
                                     FALSE);   /* keep-alive false */

    if (!handle) {
        /* connection cannot be made */
        printf("Unable to establish a connection to %s.\n", url);
        exit(-1);
    }

    /* send header */
    st = send_post_header(handle, uri, clen);

    /* send release document */
    maf = fopen(act_req_doc, "r");
    if (maf == NULL) {
	printf("Unable to open the module activation file\n");
	httplib_close(handle);
	exit(-1);
    }
    st = send_file(handle, maf);
    fclose(maf);

    /* read its responce */
    in_header = TRUE;
    is_first = TRUE;
    while (( ret = read_line(handle, ticker, 1, 60, ln, LINE_BUF_SIZE) > 0)) {
	/* line is read */
	if (in_header) {
	    if (strcmp(ln, "\r\n") == 0) {
		/* end of the header */
		in_header = FALSE;
	    } else {
		/* still in the header */
		if (is_first) {
		    sscanf(ln, "%s %d", buf, &res_code);
		    printf("Return Code: %d\n", res_code);
		    is_first = FALSE;
		}
	    }
	} else {
	    /* write it out to stdout */
	    printf("%s", ln);
	}
    }

    httplib_close(handle);

    exit(res_code);
}


/*************************
 *  post data
 *************************/
int
send_post_header(HttpLibHandle *handle, char *uri, unsigned long clen)
{
    char header[1024];

    /* send POST message to the server: The message is:
     *  "POST <URI> HTTP/1.0\r\n" +
     *      "Accept: text/html, image/gif ...\r\n" +
     *      "Host: <host>\r\n" +
     *      "Content-Type: text/xml\r\n" +
     *      "Content-Length: NNN\r\n" +
     *      "Connection: close\r\n" +
     *      "\r\n" +    <- end of HTTP header
     */

    /* send header */
    sprintf(header, "POST %s HTTP/1.0\r\nAccept: text/html, image/gif, image/jpeg, */*\r\nHost: %s\r\nContent-Type: text/xml\r\nContent-Length: %ld\r\nConnection: close\r\n\r\n",
        uri, handle->hostname, clen);

    return httplib_send(handle, header, strlen(header));
}


void
ticker(int tick)
{
    /* do nothing */
}

int
parse_args(int argc, char *argv[])
{
    int i;

    for (i=1; i<argc; i++) {
        if (strcmp(argv[i], CERT_OPTION) == 0) {
            cert_file = argv[++i];

        } else if (strcmp(argv[i], CA_OPTION) == 0) {
            ca_file = argv[++i];

        } else if (strcmp(argv[i], KEY_OPTION) == 0) {
            key_file = argv[++i];

        } else if (strcmp(argv[i], PASSWORD_OPTION) == 0) {
            key_password = argv[++i];

        } else if (strcmp(argv[i], URL_OPTION) == 0) {
            url = argv[++i];

        } else if (strcmp(argv[i], CNAME_OPTION) == 0) {
            ou = argv[++i];

        } else {
            break;
        }
    }

    return i;
}



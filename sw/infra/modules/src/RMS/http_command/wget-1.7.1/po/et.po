# Estonian translations for GNU wget.
# Copyright (C) 1998 Free Software Foundation, Inc.
# Toomas Soome <tsoome@ut.ee>, 1998.
#
msgid ""
msgstr ""
"Project-Id-Version: GNU wget 1.7\n"
"POT-Creation-Date: 2001-06-03 15:27+0200\n"
"PO-Revision-Date: 2001-06-19 10:01+02:00\n"
"Last-Translator: Toomas Soome <tsoome@ut.ee>\n"
"Language-Team: Estonian <et@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-15\n"
"Content-Transfer-Encoding: 8-bit\n"

#: src/cookies.c:588
#, c-format
msgid "Error in Set-Cookie, field `%s'"
msgstr "Set-Cookie viga, v�li `%s'"

#: src/cookies.c:612
#, c-format
msgid "Syntax error in Set-Cookie at character `%c'.\n"
msgstr "Set-Cookie s�ntaksi viga s�mbolil `%c'.\n"

#: src/cookies.c:620
msgid "Syntax error in Set-Cookie: premature end of string.\n"
msgstr "Set-Cookie s�ntaksi viga: enneaegne s�ne l�pp.\n"

#: src/cookies.c:1352
#, c-format
msgid "Cannot open cookies file `%s': %s\n"
msgstr "Ei suuda teisendada linke `%s': %s\n"

#: src/cookies.c:1364
#, c-format
msgid "Error writing to `%s': %s\n"
msgstr "Ei saa kirjutada faili `%s': %s\n"

#: src/cookies.c:1368
#, c-format
msgid "Error closing `%s': %s\n"
msgstr "Viga `%s' sulgemisel: %s\n"

#: src/ftp-ls.c:787
msgid "Unsupported listing type, trying Unix listing parser.\n"
msgstr "Mittetoetatud listingu t��p, proovin Unix listingu parserit.\n"

#: src/ftp-ls.c:832 src/ftp-ls.c:834
#, c-format
msgid "Index of /%s on %s:%d"
msgstr "/%s indeks serveris %s:%d"

#: src/ftp-ls.c:856
msgid "time unknown       "
msgstr "tundmatu aeg       "

#: src/ftp-ls.c:860
msgid "File        "
msgstr "Fail        "

#: src/ftp-ls.c:863
msgid "Directory   "
msgstr "Kataloog    "

#: src/ftp-ls.c:866
msgid "Link        "
msgstr "Viide       "

#: src/ftp-ls.c:869
msgid "Not sure    "
msgstr "Pole kindel "

#: src/ftp-ls.c:887
#, c-format
msgid " (%s bytes)"
msgstr " (%s baiti)"

#. Login to the server:
#. First: Establish the control connection.
#: src/ftp.c:150 src/http.c:624
#, c-format
msgid "Connecting to %s:%hu... "
msgstr "V�tan �hendust serveriga %s:%hu... "

#: src/ftp.c:172 src/ftp.c:539 src/http.c:641
#, c-format
msgid "Connection to %s:%hu refused.\n"
msgstr "Ei saa �hendust serveriga %s:%hu.\n"

#. Second: Login with proper USER/PASS sequence.
#: src/ftp.c:193 src/http.c:652
msgid "connected!\n"
msgstr "�hendus loodud!\n"

#: src/ftp.c:194
#, c-format
msgid "Logging in as %s ... "
msgstr "Meldin serverisse kasutajana %s ... "

#: src/ftp.c:203 src/ftp.c:256 src/ftp.c:288 src/ftp.c:336 src/ftp.c:429
#: src/ftp.c:481 src/ftp.c:575 src/ftp.c:648 src/ftp.c:709 src/ftp.c:757
msgid "Error in server response, closing control connection.\n"
msgstr "Vigane serveri vastus, sulgen juht�henduse.\n"

#: src/ftp.c:211
msgid "Error in server greeting.\n"
msgstr "Vigane serveri tervitus.\n"

#: src/ftp.c:219 src/ftp.c:345 src/ftp.c:438 src/ftp.c:490 src/ftp.c:585
#: src/ftp.c:658 src/ftp.c:719 src/ftp.c:767
msgid "Write failed, closing control connection.\n"
msgstr "Kirjutamine eba�nnestus, sulgen juht�henduse.\n"

#: src/ftp.c:226
msgid "The server refuses login.\n"
msgstr "Server ei luba meldida.\n"

#: src/ftp.c:233
msgid "Login incorrect.\n"
msgstr "Vigane meldimine.\n"

#: src/ftp.c:240
msgid "Logged in!\n"
msgstr "Melditud!\n"

#: src/ftp.c:265
msgid "Server error, can't determine system type.\n"
msgstr "Viga serveris, ei suuda tuvastada s�steemi t��pi.\n"

#: src/ftp.c:275 src/ftp.c:560 src/ftp.c:632 src/ftp.c:689
msgid "done.    "
msgstr "tehtud.  "

#: src/ftp.c:325 src/ftp.c:459 src/ftp.c:740 src/ftp.c:788 src/url.c:1429
msgid "done.\n"
msgstr "tehtud.\n"

#: src/ftp.c:353
#, c-format
msgid "Unknown type `%c', closing control connection.\n"
msgstr "Tundmatu t��p `%c', sulgen juht�henduse.\n"

#: src/ftp.c:366
msgid "done.  "
msgstr "tehtud.  "

#: src/ftp.c:372
msgid "==> CWD not needed.\n"
msgstr "==> CWD pole vajalik.\n"

#: src/ftp.c:445
#, c-format
msgid "No such directory `%s'.\n\n"
msgstr "Kataloogi `%s' pole.\n\n"

#. do not CWD
#: src/ftp.c:463
msgid "==> CWD not required.\n"
msgstr "==> CWD ei ole kohustuslik.\n"

#: src/ftp.c:497
msgid "Cannot initiate PASV transfer.\n"
msgstr "Ei saa algatada PASV �lekannet.\n"

#: src/ftp.c:501
msgid "Cannot parse PASV response.\n"
msgstr "Ei suuda anal��sida PASV vastust.\n"

#: src/ftp.c:515
#, c-format
msgid "Will try connecting to %s:%hu.\n"
msgstr "�ritan �henduda serveriga %s:%hu.\n"

#: src/ftp.c:602
#, c-format
msgid "Bind error (%s).\n"
msgstr "Bind operatsiooni viga (%s).\n"

#: src/ftp.c:618
msgid "Invalid PORT.\n"
msgstr "Vale PORT.\n"

#: src/ftp.c:671
#, c-format
msgid "\nREST failed; will not truncate `%s'.\n"
msgstr "\nREST eba�nnestus; ei l�henda faili `%s'.\n"

#: src/ftp.c:678
msgid "\nREST failed, starting from scratch.\n"
msgstr "\nREST eba�nnestus, alustan algusest.\n"

#: src/ftp.c:727
#, c-format
msgid "No such file `%s'.\n\n"
msgstr "Faili `%s' pole.\n\n"

#: src/ftp.c:775
#, c-format
msgid "No such file or directory `%s'.\n\n"
msgstr "Faili v�i kataloogi `%s' pole.\n\n"

#: src/ftp.c:859 src/ftp.c:867
#, c-format
msgid "Length: %s"
msgstr "Pikkus: %s"

#: src/ftp.c:861 src/ftp.c:869
#, c-format
msgid " [%s to go]"
msgstr " [%s on veel]"

#: src/ftp.c:871
msgid " (unauthoritative)\n"
msgstr " (autoriseerimata)\n"

#: src/ftp.c:898
#, c-format
msgid "%s: %s, closing control connection.\n"
msgstr "%s: %s, sulgen juht�henduse.\n"

#: src/ftp.c:906
#, c-format
msgid "%s (%s) - Data connection: %s; "
msgstr "%s (%s) - andme �hendus: %s; "

#: src/ftp.c:923
msgid "Control connection closed.\n"
msgstr "Juht�hendus suletud.\n"

#: src/ftp.c:941
msgid "Data transfer aborted.\n"
msgstr "Andmete �lekanne katkestatud.\n"

#: src/ftp.c:1005
#, c-format
msgid "File `%s' already there, not retrieving.\n"
msgstr "Fail `%s' on juba olemas, ei t�mba.\n"

#: src/ftp.c:1075 src/http.c:1502
#, c-format
msgid "(try:%2d)"
msgstr "(katse:%2d)"

#: src/ftp.c:1139 src/http.c:1753
#, c-format
msgid "%s (%s) - `%s' saved [%ld]\n\n"
msgstr "%s (%s) - `%s' salvestatud [%ld]\n\n"

#: src/ftp.c:1181 src/main.c:810 src/recur.c:483 src/retr.c:566
#, c-format
msgid "Removing %s.\n"
msgstr "Kustutan %s.\n"

#: src/ftp.c:1221
#, c-format
msgid "Using `%s' as listing tmp file.\n"
msgstr "Kasutan `%s' ajutise listingu failina.\n"

#: src/ftp.c:1233
#, c-format
msgid "Removed `%s'.\n"
msgstr "Kustutatud `%s'.\n"

#: src/ftp.c:1269
#, c-format
msgid "Recursion depth %d exceeded max. depth %d.\n"
msgstr "Rekursiooni s�gavus %d �letab maksimum s�gavust %d.\n"

#. Remote file is older, file sizes can be compared and
#. are both equal.
#: src/ftp.c:1330
#, c-format
msgid "Remote file no newer than local file `%s' -- not retrieving.\n"
msgstr "Kauge fail ei ole uuem, kui lokaalne fail `%s' -- ei lae.\n"

#. Remote file is newer or sizes cannot be matched
#: src/ftp.c:1337
#, c-format
msgid "Remote file is newer than local file `%s' -- retrieving.\n\n"
msgstr "Kauge fail on uuem kui lokaalne fail `%s' -- laen uuesti.\n\n"

#. Sizes do not match
#: src/ftp.c:1344
#, c-format
msgid "The sizes do not match (local %ld) -- retrieving.\n\n"
msgstr "Suurused ei klapi (lokaalne %ld) -- laen uuesti.\n"

#: src/ftp.c:1361
msgid "Invalid name of the symlink, skipping.\n"
msgstr "Vigane nimeviide, j�tan vahele.\n"

#: src/ftp.c:1378
#, c-format
msgid "Already have correct symlink %s -> %s\n\n"
msgstr "Korrektne nimeviide on juba olemas %s -> %s\n\n"

#: src/ftp.c:1386
#, c-format
msgid "Creating symlink %s -> %s\n"
msgstr "Loon nimeviite %s -> %s\n"

#: src/ftp.c:1397
#, c-format
msgid "Symlinks not supported, skipping symlink `%s'.\n"
msgstr "Ei toeta nimeviiteid, j�tan `%s' vahele.\n"

#: src/ftp.c:1409
#, c-format
msgid "Skipping directory `%s'.\n"
msgstr "J�tan kataloogi `%s' vahele.\n"

#: src/ftp.c:1418
#, c-format
msgid "%s: unknown/unsupported file type.\n"
msgstr "%s: tundmatu faili t��p.\n"

#: src/ftp.c:1445
#, c-format
msgid "%s: corrupt time-stamp.\n"
msgstr "%s: vigane aeg.\n"

#: src/ftp.c:1466
#, c-format
msgid "Will not retrieve dirs since depth is %d (max %d).\n"
msgstr "Ei t�mba katalooge, kuna s�gavus on %d (maks. %d).\n"

#: src/ftp.c:1512
#, c-format
msgid "Not descending to `%s' as it is excluded/not-included.\n"
msgstr "J�tame `%s' vahele, ta on v�listatud v�i pole kaasatud.\n"

#: src/ftp.c:1559
#, c-format
msgid "Rejecting `%s'.\n"
msgstr "Keelame `%s'.\n"

#. No luck.
#. #### This message SUCKS.  We should see what was the
#. reason that nothing was retrieved.
#: src/ftp.c:1606
#, c-format
msgid "No matches on pattern `%s'.\n"
msgstr "Jokker `%s' ei anna midagi.\n"

#: src/ftp.c:1671
#, c-format
msgid "Wrote HTML-ized index to `%s' [%ld].\n"
msgstr "Kirjutasin HTML-iseeritud indeksi faili `%s' [%ld].\n"

#: src/ftp.c:1676
#, c-format
msgid "Wrote HTML-ized index to `%s'.\n"
msgstr "Kirjutasin HTML-iseeritud indeksi faili `%s'.\n"

#: src/getopt.c:454
#, c-format
msgid "%s: option `%s' is ambiguous\n"
msgstr "%s: v�ti `%s' on arusaamatu\n"

#: src/getopt.c:478
#, c-format
msgid "%s: option `--%s' doesn't allow an argument\n"
msgstr "%s: v�ti `--%s' ei luba argumenti\n"

#: src/getopt.c:483
#, c-format
msgid "%s: option `%c%s' doesn't allow an argument\n"
msgstr "%s: v�ti `%c%s' ei luba argumenti\n"

#: src/getopt.c:498
#, c-format
msgid "%s: option `%s' requires an argument\n"
msgstr "%s: v�ti `%s' nquab argumenti\n"

#. --option
#: src/getopt.c:528
#, c-format
msgid "%s: unrecognized option `--%s'\n"
msgstr "%s: tundmatu v�ti `--%s'\n"

#. +option or -option
#: src/getopt.c:532
#, c-format
msgid "%s: unrecognized option `%c%s'\n"
msgstr "%s: tundmatu v�ti `%c%s'\n"

#. 1003.2 specifies the format of this message.
#: src/getopt.c:563
#, c-format
msgid "%s: illegal option -- %c\n"
msgstr "%s: illegaalne v�ti -- %c\n"

#. 1003.2 specifies the format of this message.
#: src/getopt.c:602
#, c-format
msgid "%s: option requires an argument -- %c\n"
msgstr "%s: v�ti n�uab argumenti -- %c\n"

#: src/host.c:374
msgid "Host not found"
msgstr "Hosti ei leitud"

#: src/host.c:376
msgid "Unknown error"
msgstr "Tundmatu viga"

#. this is fatal
#: src/http.c:555
msgid "Failed to set up an SSL context\n"
msgstr "SSL konteksti m��ramine eba�nnestus\n"

#: src/http.c:561
#, c-format
msgid "Failed to load certificates from %s\n"
msgstr "Sertifikaadi laadimine failist %s eba�nnestus\n"

#: src/http.c:565 src/http.c:573
msgid "Trying without the specified certificate\n"
msgstr "Proovin n�idatud sertifikaati mitte kasutada\n"

#: src/http.c:569
#, c-format
msgid "Failed to get certificate key from %s\n"
msgstr "Sertifikaadi v�tme laadimine failist %s eba�nnestus\n"

#: src/http.c:663 src/http.c:1593
msgid "Unable to establish SSL connection.\n"
msgstr "SSL �henduse loomine ei �nnestu.\n"

#: src/http.c:671
#, c-format
msgid "Reusing connection to %s:%hu.\n"
msgstr "Kasutan �hendust serveriga %s:%hu.\n"

#: src/http.c:861
#, c-format
msgid "Failed writing HTTP request: %s.\n"
msgstr "HTTP p�ringu kirjutamine eba�nnestus: %s.\n"

#: src/http.c:866
#, c-format
msgid "%s request sent, awaiting response... "
msgstr "%s p�ring saadetud, ootan vastust... "

#: src/http.c:910
msgid "End of file while parsing headers.\n"
msgstr "P�iste anal��sil sain faili l�pu teate.\n"

#: src/http.c:920
#, c-format
msgid "Read error (%s) in headers.\n"
msgstr "P�iste lugemise viga (%s).\n"

#: src/http.c:959
msgid "No data received"
msgstr "Andmeid ei saanudki"

#: src/http.c:961
msgid "Malformed status line"
msgstr "Katkine staatuse rida"

#: src/http.c:966
msgid "(no description)"
msgstr "(kirjeldus puudub)"

#: src/http.c:1089
msgid "Authorization failed.\n"
msgstr "Autoriseerimine eba�nnestus.\n"

#: src/http.c:1096
msgid "Unknown authentication scheme.\n"
msgstr "Tundmatu autentimis skeem.\n"

#: src/http.c:1136
#, c-format
msgid "Location: %s%s\n"
msgstr "Asukoht: %s%s\n"

#: src/http.c:1137 src/http.c:1269
msgid "unspecified"
msgstr "m��ramata"

#: src/http.c:1138
msgid " [following]"
msgstr " [j�rgnev]"

#: src/http.c:1200
msgid "\n    The file is already fully retrieved; nothing to do.\n\n"
msgstr "\n    Fail on juba t�ielikult kohal; rohkem ei saa midagi teha.\n\n"

#: src/http.c:1216
#, c-format
msgid ""
"\n"
"Continued download failed on this file, which conflicts with `-c'.\n"
"Refusing to truncate existing file `%s'.\n"
"\n"
msgstr ""
"\n"
"See server ei toeta allalaadimise j�tkamisi, milline k�itumine on konfliktis\n"
"v�tmega `-c'. Ei riku olemasolevat faili `%s'.\n"
"\n"

#. No need to print this output if the body won't be
#. downloaded at all, or if the original server response is
#. printed.
#: src/http.c:1259
msgid "Length: "
msgstr "Pikkus: "

#: src/http.c:1264
#, c-format
msgid " (%s to go)"
msgstr " (%s veel)"

#: src/http.c:1269
msgid "ignored"
msgstr "ignoreerin"

#: src/http.c:1399
msgid "Warning: wildcards not supported in HTTP.\n"
msgstr "Hoiatus: HTTP ei toeta jokkereid.\n"

#. If opt.noclobber is turned on and file already exists, do not
#. retrieve the file
#: src/http.c:1417
#, c-format
msgid "File `%s' already there, will not retrieve.\n"
msgstr "Fail `%s' on juba olemas, ei t�mba.\n"

#: src/http.c:1585
#, c-format
msgid "Cannot write to `%s' (%s).\n"
msgstr "Ei saa kirjutada faili `%s' (%s).\n"

#: src/http.c:1602
#, c-format
msgid "ERROR: Redirection (%d) without location.\n"
msgstr "VIGA: �mbersuunamine (%d) ilma asukohata.\n"

#: src/http.c:1630
#, c-format
msgid "%s ERROR %d: %s.\n"
msgstr "%s VIGA %d: %s.\n"

#: src/http.c:1642
msgid "Last-modified header missing -- time-stamps turned off.\n"
msgstr "Last-modified p�ist pole -- ei kasuta ajatempleid.\n"

#: src/http.c:1650
msgid "Last-modified header invalid -- time-stamp ignored.\n"
msgstr "Last-modified p�is on vigane -- ignoreerin ajatemplit.\n"

#: src/http.c:1673
#, c-format
msgid "Server file no newer than local file `%s' -- not retrieving.\n\n"
msgstr "Fail serveril ei ole uuem lokaalsest failist `%s' -- ei lae.\n\n"

#: src/http.c:1680
#, c-format
msgid "The sizes do not match (local %ld) -- retrieving.\n"
msgstr "Suurused ei klapi (lokaalne %ld) -- laen uuesti.\n"

#: src/http.c:1684
msgid "Remote file is newer, retrieving.\n"
msgstr "Kauge fail on uuem, laen alla.\n"

#: src/http.c:1728
#, c-format
msgid "%s (%s) - `%s' saved [%ld/%ld]\n\n"
msgstr "%s (%s) - `%s' salvestatud [%ld/%ld]\n\n"

#: src/http.c:1774
#, c-format
msgid "%s (%s) - Connection closed at byte %ld. "
msgstr "%s (%s) - �hendus suletud baidil %ld. "

#: src/http.c:1782
#, c-format
msgid "%s (%s) - `%s' saved [%ld/%ld])\n\n"
msgstr "%s (%s) - `%s' salvestatud [%ld/%ld])\n\n"

#: src/http.c:1801
#, c-format
msgid "%s (%s) - Connection closed at byte %ld/%ld. "
msgstr "%s (%s) - �hendus suletud baidil %ld/%ld. "

#: src/http.c:1812
#, c-format
msgid "%s (%s) - Read error at byte %ld (%s)."
msgstr "%s (%s) - Lugemise viga baidil %ld (%s)."

#: src/http.c:1820
#, c-format
msgid "%s (%s) - Read error at byte %ld/%ld (%s). "
msgstr "%s (%s) - Lugemise viga baidil %ld/%ld (%s). "

#: src/init.c:349 src/netrc.c:267
#, c-format
msgid "%s: Cannot read %s (%s).\n"
msgstr "%s: %s ei saa lugeda (%s).\n"

#: src/init.c:367 src/init.c:373
#, c-format
msgid "%s: Error in %s at line %d.\n"
msgstr "%s: Viga %s's real %d.\n"

#: src/init.c:405
#, c-format
msgid "%s: Warning: Both system and user wgetrc point to `%s'.\n"
msgstr "%s: Hoiatus: Nii s�steemne kui kasutaja wgetrc on `%s'.\n"

#: src/init.c:497
#, c-format
msgid "%s: BUG: unknown command `%s', value `%s'.\n"
msgstr "%s: BUG: tundmatu k�sklus`%s', v��rtus `%s'.\n"

#: src/init.c:529
#, c-format
msgid "%s: %s: Cannot convert `%s' to an IP address.\n"
msgstr "%s: %s: `%s' ei saa IP aadressiks teisendada.\n"

#: src/init.c:559
#, c-format
msgid "%s: %s: Please specify on or off.\n"
msgstr "%s: %s: Palun kasuta `on' v�i `off'.\n"

#: src/init.c:603
#, c-format
msgid "%s: %s: Please specify always, on, off, or never.\n"
msgstr "%s: %s: Palun kasuta `always', `on', `off' v�i `never'.\n"

#: src/init.c:622 src/init.c:919 src/init.c:941 src/init.c:1005
#, c-format
msgid "%s: %s: Invalid specification `%s'.\n"
msgstr "%s %s: Vigane spetsifikatsioon `%s'.\n"

#: src/init.c:775 src/init.c:797 src/init.c:819 src/init.c:845
#, c-format
msgid "%s: Invalid specification `%s'\n"
msgstr "%s: Vigane spetsifikatsioon `%s'\n"

#: src/main.c:120
#, c-format
msgid "Usage: %s [OPTION]... [URL]...\n"
msgstr "Kasuta: %s [V�TI]... [URL]...\n"

#: src/main.c:128
#, c-format
msgid "GNU Wget %s, a non-interactive network retriever.\n"
msgstr "GNU Wget %s, mitte-interaktiivne v�rgu imeja.\n"

#. Had to split this in parts, so the #@@#%# Ultrix compiler and cpp
#. don't bitch.  Also, it makes translation much easier.
#: src/main.c:133
msgid "\nMandatory arguments to long options are mandatory for short options too.\n\n"
msgstr ""
"\n"
"Kohustuslikud argumendid pikkadele v�tmetele\n"
"on kohustuslikud ka l�hikestele v�tmetele.\n"
"\n"

#: src/main.c:137
msgid ""
"Startup:\n"
"  -V,  --version           display the version of Wget and exit.\n"
"  -h,  --help              print this help.\n"
"  -b,  --background        go to background after startup.\n"
"  -e,  --execute=COMMAND   execute a `.wgetrc'-style command.\n"
"\n"
msgstr ""
"Start:\n"
"  -V,  --version           n�ita Wget versioon ja l�peta t��.\n"
"  -h,  --help              n�ita abiinfot.\n"
"  -b,  --background        mine tausta.\n"
"  -e,  --execute=K�SKLUS   t�ida `.wgetrc'-stiilis k�sklus.\n"
"\n"

#: src/main.c:144
msgid ""
"Logging and input file:\n"
"  -o,  --output-file=FILE     log messages to FILE.\n"
"  -a,  --append-output=FILE   append messages to FILE.\n"
"  -d,  --debug                print debug output.\n"
"  -q,  --quiet                quiet (no output).\n"
"  -v,  --verbose              be verbose (this is the default).\n"
"  -nv, --non-verbose          turn off verboseness, without being quiet.\n"
"  -i,  --input-file=FILE      download URLs found in FILE.\n"
"  -F,  --force-html           treat input file as HTML.\n"
"  -B,  --base=URL             prepends URL to relative links in -F -i file.\n"
"       --sslcertfile=FILE     optional client certificate.\n"
"       --sslcertkey=KEYFILE   optional keyfile for this certificate.\n"
"\n"
msgstr ""
"Logimine ja sisendfail:\n"
"  -o,  --output-file=FAIL     logi teated faili FAIL.\n"
"  -a,  --append-output=FAIL   lisa teated faili FAIL.\n"
"  -d,  --debug                tr�ki silumise teated.\n"
"  -q,  --quiet                vaikselt.\n"
"  -v,  --verbose              lobise (see on vaikimisi).\n"
"  -nv, --non-verbose          keela lobisemine, luba asjalikud teated.\n"
"  -i,  --input-file=FAIL      loe URLid failist FAIL.\n"
"  -F,  --force-html           k�sitle sisendfaili HTMLina.\n"
"  -B,  --base=URL             lisab URL suhtelistele viidetele -F -i fail.\n"
"       --sslcertfile=FAIL     kliendi sertifikaat.\n"
"       --sslcertkey=V�TMEFAIL v�tmefail sellele sertifikaadile.\n"
"\n"

#: src/main.c:158
msgid ""
"Download:\n"
"       --bind-address=ADDRESS   bind to ADDRESS (hostname or IP) on local host.\n"
"  -t,  --tries=NUMBER           set number of retries to NUMBER (0 unlimits).\n"
"  -O   --output-document=FILE   write documents to FILE.\n"
"  -nc, --no-clobber             don't clobber existing files or use .# suffixes.\n"
"  -c,  --continue               resume getting a partially-downloaded file.\n"
"       --dot-style=STYLE        set retrieval display style.\n"
"  -N,  --timestamping           don't re-retrieve files unless newer than local.\n"
"  -S,  --server-response        print server response.\n"
"       --spider                 don't download anything.\n"
"  -T,  --timeout=SECONDS        set the read timeout to SECONDS.\n"
"  -w,  --wait=SECONDS           wait SECONDS between retrievals.\n"
"       --waitretry=SECONDS      wait 1...SECONDS between retries of a retrieval.\n"
"  -Y,  --proxy=on/off           turn proxy on or off.\n"
"  -Q,  --quota=NUMBER           set retrieval quota to NUMBER.\n"
"\n"
msgstr ""
"Allalaadimine:\n"
"       --bind-address=AADRESS   seo lokaalse masina aadress (IP v�i nimi).\n"
"  -t,  --tries=NUMBER           katsete arvuks NUMBER (0 piiramata).\n"
"  -O   --output-document=FAIL   kirjuta dokumendid faili FAIL.\n"
"  -nc, --no-clobber             �ra riku olemasolevaid faile.\n"
"  -c,  --continue               j�tka olemasoleva faili allalaadimist.\n"
"       --dot-style=STIIL        kasuta laadimise n�itamise stiili STIIL.\n"
"  -N,  --timestamping           �ra t�mba vanemaid faile kui lokaalsed.\n"
"  -S,  --server-response        tr�ki serveri vastused.\n"
"       --spider                 ara t�mba midagi.\n"
"  -T,  --timeout=SEKUNDEID      kasuta lugemise timeoutina SEKUNDEID.\n"
"  -w,  --wait=SEKUNDEID         oota SEKUNDEID p�ringute vahel.\n"
"       --waitretry=SEKUNDEID    oota 1..SEKUNDIT laadimise katsete vahel.\n"
"  -Y,  --proxy=on/off           proxy kasutamine.\n"
"  -Q,  --quota=NUMBER           kasuta kvooti NUMBER.\n"
"\n"

#: src/main.c:175
msgid ""
"Directories:\n"
"  -nd  --no-directories            don't create directories.\n"
"  -x,  --force-directories         force creation of directories.\n"
"  -nH, --no-host-directories       don't create host directories.\n"
"  -P,  --directory-prefix=PREFIX   save files to PREFIX/...\n"
"       --cut-dirs=NUMBER           ignore NUMBER remote directory components.\n"
"\n"
msgstr ""
"Kataloogid:\n"
"  -nd  --no-directories            �ra loo katalooge.\n"
"  -x,  --force-directories         kohustuslik kataloogide tekitamine.\n"
"  -nH, --no-host-directories       �ra loo hosti kataloogi.\n"
"  -P,  --directory-prefix=PREFIX   salvesta failid kataloogi PREFIX/...\n"
"       --cut-dirs=NUMBER           ignoreeri kataloogi komponente > NUMBER.\n"
"\n"

#: src/main.c:183
msgid ""
"HTTP options:\n"
"       --http-user=USER      set http user to USER.\n"
"       --http-passwd=PASS    set http password to PASS.\n"
"  -C,  --cache=on/off        (dis)allow server-cached data (normally allowed).\n"
"  -E,  --html-extension      save all text/html documents with .html extension.\n"
"       --ignore-length       ignore `Content-Length' header field.\n"
"       --header=STRING       insert STRING among the headers.\n"
"       --proxy-user=USER     set USER as proxy username.\n"
"       --proxy-passwd=PASS   set PASS as proxy password.\n"
"       --referer=URL         include `Referer: URL' header in HTTP request.\n"
"  -s,  --save-headers        save the HTTP headers to file.\n"
"  -U,  --user-agent=AGENT    identify as AGENT instead of Wget/VERSION.\n"
"       --no-http-keep-alive  disable HTTP keep-alive (persistent connections).\n"
"       --cookies=off         don't use cookies.\n"
"       --load-cookies=FILE   load cookies from FILE before session.\n"
"       --save-cookies=FILE   save cookies to FILE after session.\n"
"\n"
msgstr ""
"HTTP v�tmed:\n"
"       --http-user=USER      kasuta http kasutajat USER.\n"
"       --http-passwd=PASS    kasuta http parooli PASS.\n"
"  -C,  --cache=on/off        cache kasutamine (tavaliselt lubatud kasutada).\n"
"  -E,  --html-extension      salvesta k�ik text/html dokumendid laiendiga .html.\n"
"       --ignore-length       inoreeri `Content-Length' p�ise v�lja.\n"
"       --header=S�NE         lisa S�NE p�isesse.\n"
"       --proxy-user=USER     USER proxy kasutajanimeks.\n"
"       --proxy-passwd=PASS   PASS proxy parooliks.\n"
"       --referer=URL         lisa HTTP p�ringu p�isesse `Referer: URL'\n"
"  -s,  --save-headers        salvesta HTTP p�ised.\n"
"  -U,  --user-agent=AGENT    identifitseeri kui AGENT, mitte kui Wget/VERSION.\n"
"       --no-http-keep-alive  blokeeri HTTP keep-alive (p�sivad �hendused).\n"
"       --cookies=off         �ra kasuta pr��nikuid.\n"
"       --load-cookies=FAIL   lae enne sessiooni pr��nikud failist FAIL.\n"
"       --save-cookies=FAIL   salvesta sessiooni l�pus pr��nikud faili FAIL.\n"
"\n"

#: src/main.c:201
msgid ""
"FTP options:\n"
"  -nr, --dont-remove-listing   don't remove `.listing' files.\n"
"  -g,  --glob=on/off           turn file name globbing on or off.\n"
"       --passive-ftp           use the \"passive\" transfer mode.\n"
"       --retr-symlinks         when recursing, get linked-to files (not dirs).\n"
"\n"
msgstr ""
"FTP v�tmed:\n"
"  -nr, --dont-remove-listing   �ra eemalda `.listing' faile.\n"
"  -g,  --glob=on/off           l�lita faili nime t�iendamine sisse v�i v�lja.\n"
"       --passive-ftp           kasuta \"passive\" �lekande moodi.\n"
"       --retr-symlinks         lae ka FTP nimeviited failidele.\n"
"\n"

#: src/main.c:208
msgid ""
"Recursive retrieval:\n"
"  -r,  --recursive          recursive web-suck -- use with care!\n"
"  -l,  --level=NUMBER       maximum recursion depth (inf or 0 for infinite).\n"
"       --delete-after       delete files locally after downloading them.\n"
"  -k,  --convert-links      convert non-relative links to relative.\n"
"  -K,  --backup-converted   before converting file X, back up as X.orig.\n"
"  -m,  --mirror             shortcut option equivalent to -r -N -l inf -nr.\n"
"  -p,  --page-requisites    get all images, etc. needed to display HTML page.\n"
"\n"
msgstr ""
"Rekursiivne laadimine:\n"
"  -r,  --recursive          rekursiivne imemine -- kasuta ettevaatlikult!.\n"
"  -l,  --level=NUMBER       maksimaalne rekursiooni s�gavus\n"
"                            (inf v�i 0 - piiramata).\n"
"       --delete-after       kustuta allalaetud failid.\n"
"  -k,  --convert-links      teisenda viited suhtelisteks.\n"
"  -m,  --mirror             l�hend v�tmetele -r -N -l inf -nr.\n"
"  -p,  --page-requisites    lae k�ik HTML lehe vaatamiseks vajalik info.\n"
"\n"
"\n"

#: src/main.c:218
msgid ""
"Recursive accept/reject:\n"
"  -A,  --accept=LIST                comma-separated list of accepted extensions.\n"
"  -R,  --reject=LIST                comma-separated list of rejected extensions.\n"
"  -D,  --domains=LIST               comma-separated list of accepted domains.\n"
"       --exclude-domains=LIST       comma-separated list of rejected domains.\n"
"       --follow-ftp                 follow FTP links from HTML documents.\n"
"       --follow-tags=LIST           comma-separated list of followed HTML tags.\n"
"  -G,  --ignore-tags=LIST           comma-separated list of ignored HTML tags.\n"
"  -H,  --span-hosts                 go to foreign hosts when recursive.\n"
"  -L,  --relative                   follow relative links only.\n"
"  -I,  --include-directories=LIST   list of allowed directories.\n"
"  -X,  --exclude-directories=LIST   list of excluded directories.\n"
"  -nh, --no-host-lookup             don't DNS-lookup hosts.\n"
"  -np, --no-parent                  don't ascend to the parent directory.\n"
"\n"
msgstr ""
"Rekursiivne accept/reject:\n"
"  -A,  --accept=LIST                lubatud laienduste nimistu.\n"
"  -R,  --reject=LIST                keelatud laienduste nimistu.\n"
"  -D,  --domains=LIST               lubatud doomenite nimistu.\n"
"       --exclude-domains=LIST       komadega eraldatud keelatud doomenite nimistu.\n"
"       --follow-ftp                 j�rgne HTML dokumentides FTP viidetele.\n"
"       --follow-tags=LIST           komadega eraldatud nimistu j�rgitavaid HTML\n"
"                                    lipikuid.\n"
"  -G,  --ignore-tags=LIST           komadega eraldatud nimistu ignoreeritavaid\n"
"                                    HTML lipikuid.\n"
"  -H,  --span-hosts                 mine ka teistesse serveritesse.\n"
"  -L,  --relative                   j�rgne ainult suhtelisi viiteid.\n"
"  -I,  --include-directories=LIST   lubatud kataloogide nimistu.\n"
"  -X,  --exclude-directories=LIST   v�listatud kataloogide nimistu.\n"
"  -nh, --no-host-lookup             �ra lahenda hostide nimesid.\n"
"  -np, --no-parent                  �ra t�use vanem kataloogini.\n"
"\n"

#: src/main.c:234
msgid "Mail bug reports and suggestions to <bug-wget@gnu.org>.\n"
msgstr "Saada soovitused ja vigade kirjeldused aadressil <bug-wget@gnu.org>.\n"

#: src/main.c:420
#, c-format
msgid "%s: debug support not compiled in.\n"
msgstr "%s: silumise tugi pole sisse kompileeritud.\n"

#: src/main.c:472
msgid ""
"Copyright (C) 1995, 1996, 1997, 1998, 2000, 2001 Free Software Foundation, Inc.\n"
"This program is distributed in the hope that it will be useful,\n"
"but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
"GNU General Public License for more details.\n"
msgstr ""
"Copyright (C) 1995, 1996, 1997, 1998, 2000, 2001 Free Software Foundation, Inc.\n"
"This program is distributed in the hope that it will be useful,\n"
"but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
"GNU General Public License for more details.\n"

#: src/main.c:478
msgid "\nOriginally written by Hrvoje Niksic <hniksic@arsdigita.com>.\n"
msgstr "\nSelle programmi kirjutas Hrvoje Niksic <hniksic@arsdigita.com>.\n"

#: src/main.c:569
#, c-format
msgid "%s: %s: invalid command\n"
msgstr "%s: %s: vigane k�sklus\n"

#: src/main.c:625
#, c-format
msgid "%s: illegal option -- `-n%c'\n"
msgstr "%s: illegaalne v�ti -- `-n%c'\n"

#. #### Something nicer should be printed here -- similar to the
#. pre-1.5 `--help' page.
#: src/main.c:628 src/main.c:670 src/main.c:728
#, c-format
msgid "Try `%s --help' for more options.\n"
msgstr "Proovi `%s --help' lisa v�tmete saamiseks.\n"

#: src/main.c:708
msgid "Can't be verbose and quiet at the same time.\n"
msgstr "Ei saa korraga lobiseda ja vait olla.\n"

#: src/main.c:714
msgid "Can't timestamp and not clobber old files at the same time.\n"
msgstr "Ei saa samaaegselt muuta failide aegu ja mitte puutuda vanu faile.\n"

#. No URL specified.
#: src/main.c:723
#, c-format
msgid "%s: missing URL\n"
msgstr "%s: puudub URL\n"

#: src/main.c:825
#, c-format
msgid "No URLs found in %s.\n"
msgstr "%s ei sisalda URLe.\n"

#: src/main.c:834
#, c-format
msgid ""
"\n"
"FINISHED --%s--\n"
"Downloaded: %s bytes in %d files\n"
msgstr ""
"\n"
"L�PETATUD --%s--\n"
"Alla laetud: %s baiti kokku %d failis\n"

#: src/main.c:842
#, c-format
msgid "Download quota (%s bytes) EXCEEDED!\n"
msgstr "Allalaadimise kvoot  (%s baiti) ON �LETATUD!\n"

#. Please note that the double `%' in `%%s' is intentional, because
#. redirect_output passes tmp through printf.
#: src/main.c:876
#, c-format
msgid "%s received, redirecting output to `%%s'.\n"
msgstr "sain %s, suunan v�ljundi faili `%%s'.\n"

#: src/mswindows.c:89
#, c-format
msgid ""
"\n"
"CTRL+Break received, redirecting output to `%s'.\n"
"Execution continued in background.\n"
"You may stop Wget by pressing CTRL+ALT+DELETE.\n"
msgstr ""
"\n"
"Sain CTRL+Break, suunan v�ljundi aili `%s'.\n"
"T�itmine jatkub taustas.\n"
"Wgeti peatamiseks vajuta CTRL+ALT+DELETE.\n"

#. parent, no error
#: src/mswindows.c:106 src/utils.c:458
msgid "Continuing in background.\n"
msgstr "J�tkan taustas.\n"

#: src/mswindows.c:108 src/utils.c:460
#, c-format
msgid "Output will be written to `%s'.\n"
msgstr "V�ljund kirjutatakse faili `%s'.\n"

#: src/mswindows.c:188
#, c-format
msgid "Starting WinHelp %s\n"
msgstr "Stardin WinHelp %s\n"

#: src/mswindows.c:215 src/mswindows.c:222
#, c-format
msgid "%s: Couldn't find usable socket driver.\n"
msgstr "%s: Ei leia kasutusk�lblikku pistiku programmi.\n"

#: src/netrc.c:367
#, c-format
msgid "%s: %s:%d: warning: \"%s\" token appears before any machine name\n"
msgstr "%s: %s:%d: hoiatus: \"%s\" identifikaator on enne masina nime\n"

#: src/netrc.c:398
#, c-format
msgid "%s: %s:%d: unknown token \"%s\"\n"
msgstr "%s: %s:%d: tundmatu lekseem \"%s\"\n"

#: src/netrc.c:462
#, c-format
msgid "Usage: %s NETRC [HOSTNAME]\n"
msgstr "Kasuta: %s NETRC [HOSTINIMI]\n"

#: src/netrc.c:472
#, c-format
msgid "%s: cannot stat %s: %s\n"
msgstr "%s: stat operatsioon eba�nnestus %s: %s\n"

#: src/recur.c:484
#, c-format
msgid "Removing %s since it should be rejected.\n"
msgstr "Kustutan %s, kuna see peaks olema tagasi l�katud.\n"

#: src/recur.c:679
msgid "Loading robots.txt; please ignore errors.\n"
msgstr "Laen robots.txti faili; palun ignoreerige v�imalikk vigu.\n"

#: src/retr.c:227
#, c-format
msgid "\n          [ skipping %dK ]"
msgstr "\n          [ h�ppan �le %dK ]"

#: src/retr.c:373
msgid "Could not find proxy host.\n"
msgstr "Ei leia proxy serverit.\n"

#: src/retr.c:387
#, c-format
msgid "Proxy %s: Must be HTTP.\n"
msgstr "Proxy: %s: Peab olema HTTP.\n"

#: src/retr.c:481
#, c-format
msgid "%s: Redirection cycle detected.\n"
msgstr "%s: Tuvastasin �mbersuunamise ts�kli.\n"

#: src/retr.c:587
msgid "Giving up.\n\n"
msgstr "Annan alla.\n"

#: src/retr.c:587
msgid "Retrying.\n\n"
msgstr "Proovin uuesti.\n\n"

#: src/url.c:1329
#, c-format
msgid "Converting %s... "
msgstr "Teisendan %s... "

#: src/url.c:1342
msgid "nothing to do.\n"
msgstr "midagi ei ole teha.\n"

#: src/url.c:1350 src/url.c:1374
#, c-format
msgid "Cannot convert links in %s: %s\n"
msgstr "Ei suuda teisendada linke %s: %s\n"

#: src/url.c:1365
#, c-format
msgid "Unable to delete `%s': %s\n"
msgstr "Ei �nnestu kustutada `%s': %s\n"

#: src/url.c:1555
#, c-format
msgid "Cannot back up %s as %s: %s\n"
msgstr "Ei suuda luua %s varukoopiat %s: %s\n"

#: src/utils.c:94
#, c-format
msgid "%s: %s: Not enough memory.\n"
msgstr "%s: %s: m�lu ei j�tku.\n"

#: src/utils.c:417
msgid "Unknown/unsupported protocol"
msgstr "Tundmatu/mittetoetatav protokoll"

#: src/utils.c:420
msgid "Invalid port specification"
msgstr "Vigane pordi spetsifikatsioon"

#: src/utils.c:423
msgid "Invalid host name"
msgstr "Vigane hosti nimi"

#: src/utils.c:620
#, c-format
msgid "Failed to unlink symlink `%s': %s\n"
msgstr "Ei �nnestu kustutada nimeviidet `%s': %s\n"

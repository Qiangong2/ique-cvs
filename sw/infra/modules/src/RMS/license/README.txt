------------
license.src
------------
1. license.src file is a name/value pair file which contains the service domain as well as the license information for all licensable modules.
2. Person who issues new licenses shall modify the "domains/<domain>/license.src" file with the appropriate data and check it in.
 
----------------
generate_all.sh
----------------
1. generate_all.sh loops for each <domain> folder under domains directory.
2. Calls "generate_license.sh" with <domain> and <build_number>.
3. Can be independently called with <build_number> as a parameter.
 
--------------------
generate_license.sh
--------------------
1. generate_license.sh creates "license.dat" from the "broadon-license-xml-tmpl" template which is filled in with the values from the "domains/<domain>/license.src" file.
2. Generates license.sig file by calling GenerateLicense class (for more information about GenerateLicense class, please refer to GenerateLicense.README file).
3. Calls Makefile to create "license.tar" and "post_license.sh" files.
4. Moves license.dat, license.sig, license.tar, post_license.sh to the domains/<domain> folder.
5. Called by "generate_all.sh" with <domain> and <build_number> as parameters.
6. Can be independently called with <domain> and optional <build_number>.
7. If <build_number> is not provided, the script tries to rsh deathstar for the latest sysimage, if that fails then it uses the current date in "yyyymmddhh" format.
 
---------
Makefile
---------
1. make class - compiles "GenerateLicense.java" file.
2. make post_license.sh - creates "license.tar" and "post_license.sh" using the "post-front-tmpl" and "passphrase" files.
3. Called by "generate_license.sh".
 
----------------
post_license.sh
----------------
1. Needs to be executed on the RMS server as root.
2. Extracts license.dat and license.sig filesto /opt/broadon/pkgs/rms/.license
3. Calls PostLicense class in hronline.util package to post the files to the DB and reports the result.

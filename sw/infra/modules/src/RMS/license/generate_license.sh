#!/bin/sh
#
# $Revision: 1.7 $
# $Date: 2002/09/20 00:19:43 $

TEMPLATE_FILE=broadon-license-xml-tmpl
LICENSE_FILE_NAME=license.dat
SIGNATURE_FILE_NAME=license.sig
LICENSE_TAR_FILE=license.tar
POST_FILE=post_license.sh
DOMAINS_DIR=domains

FILE_VERSION=`date +%Y%m%d%H%M`
LOGNAME=logs/generate_$FILE_VERSION.log
JAVA_CLASS=GenerateLicense.class

show_help() 
{
  echo "Input parameters are domain name and an optional build number."
  echo "Generates the specified license and signature file."
  echo
  echo "   arg1 .. domain"
  echo "   arg2 .. <optional> build number"
}

check_java_class()
{
  if [ ! -f $JAVA_CLASS ] ; then
   echo '-- ERROR --- ERROR -- ERROR --'  >> $LOGNAME
   echo "GenerateLicense.class file not found in the current directory!"  >> $LOGNAME

   echo '-- ERROR --- ERROR -- ERROR --'
   echo "GenerateLicense.class file not found in the current directory!"
   exit 1 
  else
   echo "GenerateLicense class file found"  >> $LOGNAME
  fi
}

generate_signature()
{
  FLAG_OK=0
  for x in 1 2 3 4 5
  do
   java GenerateLicense -data $LICENSE_FILE_NAME -sig $SIGNATURE_FILE_NAME
   if [ $? -ne 0 ] ; then
      echo "Signature file creation failed - trial $x"  >> $LOGNAME
      echo "Signature file creation failed - trial $x"
      sleep 5      
   else
      FLAG_OK=1
      break
   fi
  done # x

  if [ $FLAG_OK -eq '0' ] ; then
      echo '-- ERROR --- ERROR -- ERROR --'  >> $LOGNAME
      echo "GenerateLicense class failure..."  >> $LOGNAME
      echo "Signature file creation failed!"  >> $LOGNAME

      echo '-- ERROR --- ERROR -- ERROR --'
      echo "GenerateLicense class failure..."
      echo "Signature file creation failed!"
      exit 1
  else
      echo "Signature file successfully created"  >> $LOGNAME
  fi
}

create_license_file()
{
  sed \
	-e "s^CREATION^$FILE_VERSION^g" \
	-e "s^LATEST_BUILD^$BUILD^g" \
	-e "s^SERVICE_DOMAIN^$SERVICE_DOMAIN^g" \
	-e "s^RMS_NUM_LICENSE^$RMS_NUM_LICENSE^g" \
	-e "s^RMS_ISSUE_DATE^$RMS_ISSUE_DATE^g" \
	-e "s^RMS_EXP_DATE^$RMS_EXP_DATE^g" \
	-e "s^RMS_VERSION^$RMS_VERSION^g" \
	-e "s^PPTP_NUM_LICENSE^$PPTP_NUM_LICENSE^g" \
	-e "s^PPTP_ISSUE_DATE^$PPTP_ISSUE_DATE^g" \
	-e "s^PPTP_EXP_DATE^$PPTP_EXP_DATE^g" \
	-e "s^PPTP_VERSION^$PPTP_VERSION^g" \
	-e "s^IPSEC_NUM_LICENSE^$IPSEC_NUM_LICENSE^g" \
	-e "s^IPSEC_ISSUE_DATE^$IPSEC_ISSUE_DATE^g" \
	-e "s^IPSEC_EXP_DATE^$IPSEC_EXP_DATE^g" \
	-e "s^IPSEC_VERSION^$IPSEC_VERSION^g" \
	-e "s^CS_NUM_LICENSE^$CS_NUM_LICENSE^g" \
	-e "s^CS_ISSUE_DATE^$CS_ISSUE_DATE^g" \
	-e "s^CS_EXP_DATE^$CS_EXP_DATE^g" \
	-e "s^CS_VERSION^$CS_VERSION^g" $TEMPLATE_FILE > $LICENSE_FILE_NAME 

  if [ $? -ne 0 ] ; then
      echo '-- ERROR --- ERROR -- ERROR --'  >> $LOGNAME
      echo "License file creation failed!"  >> $LOGNAME

      echo '-- ERROR --- ERROR -- ERROR --'
      echo "License file creation failed!"
      exit 1      
  else
      echo "License file successfully created"  >> $LOGNAME
  fi
}

update_domain_files()
{
   mv $LICENSE_FILE_NAME $DOMAIN_FOLDER
   mv $SIGNATURE_FILE_NAME $DOMAIN_FOLDER
   mv $LICENSE_TAR_FILE $DOMAIN_FOLDER
   mv $POST_FILE $DOMAIN_FOLDER
}

#cvs_check_in()
#{
# check in license.dat
# check in license.sig
# check in license.tar
# check in post_license.sh
#}

mkdir -p logs

if [ "$1" != "" ] ; then
    DOMAIN_FOLDER=$DOMAINS_DIR/$1
    SOURCE_FILE=$DOMAIN_FOLDER/license.src
    . $SOURCE_FILE
    if [ "$1" != "$SERVICE_DOMAIN" ] ; then
      echo '-- ERROR --- ERROR -- ERROR --'  >> $LOGNAME
      echo Given domain $1 does not match with service domain in the source file $SERVICE_DOMAIN  >> $LOGNAME

      echo '-- ERROR --- ERROR -- ERROR --'
      echo Given domain $1 does not match with service domain in the source file $SERVICE_DOMAIN
      echo
      exit 1
    fi
else
    echo '-- ERROR --- ERROR -- ERROR --'  >> $LOGNAME
    echo "No input parameter!"  >> $LOGNAME

    echo '-- ERROR --- ERROR -- ERROR --'
    echo "No input parameter!"
    echo
    show_help
    exit 1
fi

if [ "$2" != "" ] ; then
 BUILD=$2
else
 BUILD=`rsh 10.0.0.42 /bin/cat /home/build/sysimage/latest_build_number.txt`
 if [ $? -ne 0 ] ; then
   BUILD=`date +%Y%m%d%H`
 fi
fi

if [ -f $LICENSE_FILE_NAME ] ; then
    /bin/rm -f $LICENSE_FILE_NAME 
fi

if [ -f $SIGNATURE_FILE_NAME ] ; then
    /bin/rm -f $SIGNATURE_FILE_NAME
fi

echo "Processing license info for $1..." >> $LOGNAME
make clean >> $LOGNAME
make class >> $LOGNAME
check_java_class
create_license_file
generate_signature
make post >> $LOGNAME
update_domain_files
#cvs_check_in
echo "---------------------------------------" >> $LOGNAME


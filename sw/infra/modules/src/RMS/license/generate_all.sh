#!/bin/sh
#
# $Revision: 1.2 $
# $Date: 2002/09/14 02:11:09 $

EXEC_FILE=generate_license.sh
DOMAINS_DIR=domains

if [ "$1" != "" ] ; then
 BUILD=$1
else
 BUILD=`rsh 10.0.0.42 /bin/cat /home/build/sysimage/latest_build_number.txt`
 if [ $? -ne 0 ] ; then
   BUILD=`date +%Y%m%d%H`
 fi
fi

dir=`ls $DOMAINS_DIR`
for domain in $dir; do
    echo "Processing license info for $domain..."
    sh $EXEC_FILE $domain $BUILD
done
echo "Done"


import java.io.*;
import java.security.*;
import java.security.spec.*;

class GenerateLicense {

   private static void usage()
   {
       System.out.println("GenerateLicense -data <license_file_path> -sig <name_for_the_signature_file>");
       System.out.println("     -data <license_file_path>                    License File Path");
       System.out.println("     -sig <name_for_the_signature_file>          Name for the Signature File");
   }

   public static void main(String[] args) {

      String encrypted = "MIIBTAIBADCCASwGByqGSM44BAEwggEfAoGBAP1/U4EddRIpUt9KnC7s5Of2EbdSPO9EAMMeP4C2\n";
      encrypted += "USZpRV1AIlH7WT2NWPq/xfW6MPbLm1Vs14E7gB00b/JmYLdrmVClpJ+f6AR7ECLCT7up1/63xhv4\n";
      encrypted += "O1fnxqimFQ8E+4P208UewwI1VBNaFpEy9nXzrith1yrv8iIDGZ3RSAHHAhUAl2BQjxUjC8yykrmC\n";
      encrypted += "ouuEC/BYHPUCgYEA9+GghdabPd7LvKtcNrhXuXmUr7v6OuqC+VdMCz0HgmdRWVeOutRZT+ZxBxCB\n";
      encrypted += "gLRJFnEj6EwoFhO3zwkyjMim4TwWeotUfI0o4KOuHiuzpnWRbqN/C/ohNWLx+2J6ASQ7zKTxvqhR\n";
      encrypted += "kImog9/hWuWfBpKLZl6Ae1UlZAFMO/7PSSoEFwIVAIGrA5eeM4Yzfh2D36W9Fk3gymWY";

      FileInputStream fis = null;
      FileOutputStream fos = null;

      if (args.length != 4) {
          usage();
          return;
      }
      else try {

          for (int i=0; i<args.length; i++)
          {
              if (args[i].equals("-data"))
              {
                  fis = new FileInputStream(args[++i]);
              } else if (args[i].equals("-sig")) {
                  fos = new FileOutputStream(args[++i]);
              } else {
                  usage();
                  return;
              }
          }

/* Get the private key */
          byte[] encpriKey = new sun.misc.BASE64Decoder().decodeBuffer(encrypted);

          PKCS8EncodedKeySpec prikeySpec = new PKCS8EncodedKeySpec(encpriKey);
          KeyFactory keyFactory = KeyFactory.getInstance("DSA");
          PrivateKey privKey = keyFactory.generatePrivate(prikeySpec);

/* Initialize the signature object with the private key */
          Signature dsaSig = Signature.getInstance("SHA1withDSA"); 
          dsaSig.initSign(privKey);

/* Sign the data */
          BufferedInputStream bis = new BufferedInputStream(fis);
          byte[] buffer = new byte[1024];
          int len;
          while (bis.available() != 0) {
             len = bis.read(buffer);
             dsaSig.update(buffer, 0, len);
          };
          bis.close();

/* Generate a signature */
          byte[] rfSig = dsaSig.sign();
        
/* Save the signature in a file */
          fos.write(rfSig);
          fos.close();

        } catch (Exception e) {
            System.err.println("Exception Occured" + e.toString());
        }
    };
}

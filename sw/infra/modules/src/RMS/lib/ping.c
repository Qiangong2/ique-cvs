/*
 * ping.c: Taken from Mike Muus' classic ping.c, modified
 * for our purposes.  Thank you Mike!
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/time.h>
#include <syslog.h>
#ifdef SOLARIS
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/in_systm.h>
#include <netinet/ip.h>
#endif
#include <netinet/ip_icmp.h>
#include <netdb.h>
#include "ping.h"

#define ICMP_HDR_LEN	8
#define	MAXWAIT		1	/* max time to wait for response, sec. */
#define	MAXPACKET	256	/* max packet size */
#ifndef MAXHOSTNAMELEN
#define MAXHOSTNAMELEN	64
#endif

#define TIMEVAL_TO_D(a)      ((a).tv_sec * 1e6 + (a).tv_usec)
#define TIMEVAL_GTE(a, b) \
        (((a).tv_sec > (b).tv_sec) \
         || (((a).tv_sec  == (b).tv_sec) \
             && ((a).tv_usec >= (b).tv_usec)))

/* Global declarations */

/* Forward declarations */

static struct icmp *get_icmp_hdr(char *buf, int len);

static int in_cksum(u_short *addr, int len);
static int pinger(int skt_fd, int ident, int datalen, int ntransmitted, struct sockaddr_in* whereto);
static int pr_pack(char *buf, int cc, struct sockaddr_in *from, int ident);
extern int ping_it(unsigned target_ip_address, int n_test_pkts, int tout /* sec */);

#ifdef TEST_DRIVE
int
main(int argc, char **argv) {
        int i;
        struct pingtest t;
        struct pingresult r;
        struct in_addr addr;

        if (argc < 6) {
                printf("usage: %s ip-address npackets interval "
                       "maxtime flags\n", argv[0]);
                return 1;
        }
        if (inet_aton(argv[1], &addr) == 0) {
                printf("inet_aton: bad address `%s'\n", argv[1]);
                return 2;
        }
        t.ipaddr = addr.s_addr;
        t.npackets = atoi(argv[2]);
        t.interval = atoi(argv[3]);
        t.maxtime = atoi(argv[4]);
        t.flags = atoi(argv[5]);
        if (ping_probe(&t, &r) < 0) {
                printf("ping_probe: failed on `%s'\n", argv[1]);
                return 3;
        }
        printf("%d packets transmitted, %d packets received, "
               "%.1f%% packet loss\n",
               r.nsent, r.nrecv, 
               100.0 * (r.nsent - r.nrecv) / r.nsent);
        printf("round-trip avg = %.3f ms avg rtt\n", r.avg_rtt);
        return 0;
}
#endif /* TEST_DRIVE */

int
ping_probe(struct pingtest *ptest, struct pingresult *presult) {
        int sock;
        int pingid;
        struct timeval deadline;
        struct timeval nextping;
        unsigned char pktbuf[MAXPACKET];
        struct sockaddr_in dest;
        static int icmp_proto_no;

        if (ptest == NULL || presult == NULL) {
                syslog(LOG_ERR, "ping_probe: bad arguments");
                return -1;
        }

        if (icmp_proto_no == 0) {
                struct protoent *p = (struct protoent *)getprotobyname("icmp");
                if (p != NULL) {
                        icmp_proto_no = p->p_proto;
                } else {
                        syslog(LOG_ERR, "getprotobyname: icmp protocol NULL");
                        icmp_proto_no = 1;
                }
        }

        if ((sock = socket(AF_INET, SOCK_RAW, icmp_proto_no)) < 0) {
                syslog(LOG_ERR, "socket: %m\n");
                return -1;
        }

        memset((char *) &dest, 0, sizeof(dest));
        dest.sin_family = AF_INET;
        dest.sin_addr.s_addr = ptest->ipaddr;

        gettimeofday(&deadline, NULL);
        deadline.tv_sec += ptest->maxtime;
        pingid = (getpid()
                  ^ (deadline.tv_usec & 0xffff)
                  ^ ((deadline.tv_usec >> 16) & 0xffff)) & 0xffff;

        presult->nsent = 0;
        presult->nrecv = 0;
        presult->avg_rtt = 0;
        gettimeofday(&nextping, NULL);
        if (ptest->interval <= 0)
                ptest->interval = 1;     /* prohibit floods */

        for ( ; ; ) {
                int n;
                fd_set rfds;
                int srclen;
                struct timeval now;
                struct icmp *icmph;
                struct sockaddr_in src;
                int is_xmit_pending = presult->nsent < ptest->npackets;

                /* test for stop conditions */
                if (presult->nrecv >= ptest->npackets)
                        break;

                gettimeofday(&now, NULL);
                if (TIMEVAL_GTE(now, deadline))
                        break;

                /* transmit probe, if scheduled */
                if (is_xmit_pending && TIMEVAL_GTE(now, nextping)) {
                        if (pinger(sock, pingid, 64 - 8,
                                   presult->nsent + 1, &dest) < 0)
                                return -1;

                        presult->nsent += 1;
                        memcpy((char *) &nextping, (char *) &now, sizeof(now));
                        nextping.tv_sec += ptest->interval;
                }
                
                /* wait for responses */
                FD_ZERO(&rfds);
                FD_SET(sock, &rfds);
                {
                    /* wait until next ping or deadline */
                    double tout = TIMEVAL_TO_D(deadline);
                    if (is_xmit_pending) {
                        double sendt = TIMEVAL_TO_D(nextping);
                        tout = (sendt < tout) ? sendt : tout;
                    }
                    tout -= TIMEVAL_TO_D(now);
                    if (tout < 0) tout = 0;
                    now.tv_sec = tout / 1e6;
                    now.tv_usec = tout - (now.tv_sec * 1e6);
                }
                if ((n = select(sock + 1, &rfds, NULL, NULL, &now)) < 0) {
                        syslog(LOG_ERR, "select: %m");
                        return -1;
                }
                if (n == 0) {
                        /* haven't heard anything */
                        continue;
                }

                /* process response */
                srclen = sizeof(src);
                n = recvfrom(sock, pktbuf, sizeof(pktbuf), 0, &src, &srclen);
                if (n < 0) {
                        syslog(LOG_ERR, "recvfrom: %m");
                        return -1;
                }
                if (src.sin_addr.s_addr != dest.sin_addr.s_addr
                    || ((icmph = get_icmp_hdr(pktbuf, n)) == NULL))
                {
                        /* nothing we care about */
                        continue;
                }
                if (icmph->icmp_type == ICMP_ECHOREPLY &&
                    icmph->icmp_id == pingid) 
                {
                        char *b = ((char *) icmph) + ICMP_HDR_LEN;
                        struct timeval *orig = (struct timeval *) b;
                        double origt, nowt;

                        presult->nrecv += 1;
                        /* compute rtt and convert to ms */
                        gettimeofday(&now, NULL);
                        nowt = now.tv_sec * 1e6 + now.tv_usec;
                        origt = orig->tv_sec * 1e6 + orig->tv_usec;
                        nowt -= origt;
                        nowt /= 1e3;
                        if (nowt < 0) nowt = 0;
                        presult->avg_rtt += nowt;
                        if (ptest->flags & P_STOP_FIRST_CONTACT) {
                                break;
                        }
                }
        }

        if (presult->nrecv > 0)
                presult->avg_rtt = presult->avg_rtt / presult->nrecv;
        else
                presult->avg_rtt = 0;

        return 0;
}

static struct icmp *
get_icmp_hdr(char *buf, int len) {
        int hdrlen;
	struct ip *ip = (struct ip *) buf;
	struct icmp *icp;

	ip = (struct ip *) buf;
	hdrlen = ip->ip_hl * 4;
	if (len < hdrlen + ICMP_MINLEN)
		return NULL;

        return (struct icmp *) (buf + hdrlen);
}

int
ping_it(unsigned target_ip_address, int n_test_pkts, int tout /* sec */)
{
        int skt, fromlen, cc;
	struct sockaddr_in from, whereto;
	struct sockaddr_in *to = (struct sockaddr_in *) &whereto;
        fd_set fdmask;
	struct timeval timeout;
	int ntransmitted;	/* sequence # for outbound packets = #sent */
	int nreceived;		/* # of packets we got back */
	int datalen;		/* How much data */
	int ident;
	u_char packet[MAXPACKET];

	static int icmp_protocol_number = -1;

        nreceived = ntransmitted = 0;
	memset((char *)&whereto, 0, sizeof(struct sockaddr));
	to->sin_family = AF_INET;
	to->sin_addr.s_addr = target_ip_address;
	datalen = 64-8;
	ident = getpid() & 0xFFFF;

        /* We should only have to look up the protocol to use for ICMP once */
        if (icmp_protocol_number < 0 ) {
	   struct protoent *proto = NULL;
	   if ((proto = (struct protoent *) getprotobyname("icmp")) == NULL) {
                syslog(LOG_ERR,
                      "getprotobyname(\"icmp\") returns NULL\n");
                syslog(LOG_ERR,"Using 1 for protocol number\n");
		icmp_protocol_number = 1;
	   } else {
             icmp_protocol_number = proto->p_proto;
	   }
	}

	if ((skt = socket(AF_INET, SOCK_RAW, icmp_protocol_number)) < 0) {
             syslog(LOG_ERR,"socket() error %m\n");
	     return -1;
	}

	while ((skt >= 0) && (nreceived == 0) && (ntransmitted < n_test_pkts)) {
		pinger(skt, ident, datalen, ntransmitted, &whereto);
                ntransmitted += 1;
                FD_ZERO(&fdmask);
                FD_SET(skt, &fdmask);

		timeout.tv_sec = tout;
		timeout.tv_usec = 0; 
		if( select(skt+1, &fdmask, 0, 0, &timeout) == 0) {
                  break; /* Timeout */
                }

		fromlen = sizeof (from);
		if ((cc=recvfrom(skt, packet, sizeof(packet), 0, &from, &fromlen)) > 0) {
		   nreceived += pr_pack(packet, cc, &from, ident);
                }
	}
	close(skt);
        return nreceived;
}

/*
 * 			P I N G E R
 * 
 * Compose and transmit an ICMP ECHO REQUEST packet.  The IP packet
 * will be added on by the kernel.  The ID field is our UNIX process ID,
 * and the sequence number is an ascending integer.  The first 8 bytes
 * of the data portion are used to hold a UNIX "timeval" struct in VAX
 * byte-order, to compute the round-trip time.
 */
static int
pinger(int fd, int pingid, int datalen, int seq, struct sockaddr_in *dest) {
        int i;
        int pktsize;
	unsigned char buf[MAXPACKET];
	struct icmp *icp    = (struct icmp *) buf;
        struct timeval *now = (struct timeval *) (buf + ICMP_HDR_LEN);
	unsigned char *p    = buf + ICMP_HDR_LEN + sizeof(*now);

	icp->icmp_type = ICMP_ECHO;
	icp->icmp_code = 0;
	icp->icmp_cksum = 0;
	icp->icmp_seq = seq;
	icp->icmp_id = pingid;

        gettimeofday(now, NULL);
	for(i = sizeof(*now); i < datalen; i++)
		*p++ = i;

        pktsize = ICMP_HDR_LEN + datalen;
	icp->icmp_cksum = in_cksum((ushort *) icp, pktsize);
	if (sendto(fd, buf, pktsize, 0, dest, sizeof(*dest)) < 0) {
                syslog(LOG_ERR,"sendto(): %m\n");
                return -1;
        }
	return 1;
}

/*
 *			P R _ P A C K
 *
 * Print out the packet, if it came from us.  This logic is necessary
 * because ALL readers of the ICMP socket get a copy of ALL ICMP packets
 * which arrive ('tis only fair).  This permits multiple copies of this
 * program to be run without having intermingled output (or statistics!).
 */
static int
pr_pack(char *buf, int cc, struct sockaddr_in *from, int ident)
{
	struct ip *ip;
	register struct icmp *icp;
        int hlen;

	ip = (struct ip *) buf;
	hlen = ip->ip_hl << 2;
	if (cc < hlen + ICMP_MINLEN) {
		return 0;
	}
	cc -= hlen;
	icp = (struct icmp *)(buf + hlen);

	if ( icp->icmp_type != ICMP_ECHOREPLY )  {
		return 0;
	}
	if( icp->icmp_id != ident ) {
		return 0;			/* 'Twas not our ECHO */
        }

	return 1;
}


/*
 *			I N _ C K S U M
 *
 * Checksum routine for Internet Protocol family headers (C Version)
 *
 */
static int
in_cksum(u_short *addr, int len)
{
	register int nleft = len;
	register u_short *w = addr;
	register u_short answer;
	register int sum = 0;

	/*
	 *  Our algorithm is simple, using a 32 bit accumulator (sum),
	 *  we add sequential 16 bit words to it, and at the end, fold
	 *  back all the carry bits from the top 16 bits into the lower
	 *  16 bits.
	 */
	while( nleft > 1 )  {
		sum += *w++;
		nleft -= 2;
	}

	/* mop up an odd byte, if necessary */
	if( nleft == 1 ) {
		u_short	u = 0;

		*(u_char *)(&u) = *(u_char *)w ;
		sum += u;
	}

	/*
	 * add back carry outs from top 16 bits to low 16 bits
	 */
	sum = (sum >> 16) + (sum & 0xffff);	/* add hi 16 to low 16 */
	sum += (sum >> 16);			/* add carry */
	answer = ~sum;				/* truncate to 16 bits */
	return (answer);
}

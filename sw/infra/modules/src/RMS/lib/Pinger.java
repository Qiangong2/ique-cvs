package lib;

import java.io.IOException;
import java.net.*;
import java.lang.Thread;

/**
 * Wrapper for the ICMP based ping library
 */
public class Pinger
{
    static {
	System.loadLibrary("ping");
    }

    public Pinger() { }

    native public boolean ping(String ipaddr, int timeout /* sec */);

    native protected int doProbe(PingTest test, PingResult result);

    public PingResult probe(PingTest t)
        throws IOException
    {
        InetAddress a = InetAddress.getByName(t.getAddress());
        t.setAddress(a.getHostAddress());
        PingResult r = new PingResult();
        if (doProbe(t, r) < 0)
            throw new IOException("probe failed (see syslog for details)");
        return r;
    }

    private static String toString(double d, int digits) {
        String s = "" + d;
        int dot = s.indexOf('.');
        if (dot < 0)
            return s;
        if (dot + digits < s.length())
            return s.substring(0, dot + digits + 1);
        return s;
    }

    /**
     * for testing
     */
    public static void main(String[] args)
        throws IOException,
        NumberFormatException
    {
        if (args.length != 5) {
            System.out.println("usage: lib.Pinger "
                               + "host npackets interval maxtime flags");
            System.exit(1);
        }
        PingTest t = new PingTest(args[0],
                                  Integer.parseInt(args[1]),
                                  Integer.parseInt(args[2]),
                                  Integer.parseInt(args[3]),
                                  Integer.parseInt(args[4]));
        Pinger p = new Pinger();
        PingResult r = p.probe(t);
        double d = 100.0 * (r.getSentPackets() - r.getReceivedPackets()) /
            r.getSentPackets();
        System.out.println("" + r.getSentPackets() + " packets transmitted, "
                           + r.getReceivedPackets() + " packets received, "
                           + toString(d, 3) + "% packet loss");
        System.out.println("round-trip avg = " + 
                           toString(r.getAverageRtt(), 3) + " ms");

        /*
	for (int i=0; i<args.length; i++) {
	    Pinger p = new Pinger();
	    try {
		System.out.print("Ping to " + args[i]);
		InetAddress addr = InetAddress.getByName(args[i]);
		System.out.println("   " + addr);
		if (p.ping(addr.getHostAddress(), 15)) {
		    System.out.println("   Responded");
		} else {
		    System.out.println("   No Response");
		}
	    } catch (Exception e) {
		System.out.println();
		System.out.println("    Failed due to the exception below:");
		e.printStackTrace();
	    }
	}
        */
    }
}


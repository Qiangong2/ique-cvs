package lib.messenger;

/**
 * A MessageReceiver is a sink for Messages sent by a MessageSender.
 * <p>
 * Typically, a MessageReceiver has three methods (implementors are 
 * encouraged to follow this specification):
 * <ul>
 * <li> <code>startup</code>, which runs implementor-specific code to
 *      initialize the input channel from MessageSender to 
 *      MessageReceiver.  The declaration for this method is not 
 *      specified here because the state needed to initialize a input 
 *      channel is typically specific to each MessageReceiver.
 * <li> <code>recv</code>, which accepts Messages from the MessageSender.
 * <li> <code>shutdown</code>, which runs implementor-specific code to
 *      close the input channel.
 * </ul>
 */
public interface MessageReceiver {

    /**
     * Reads a Message and returns its length. It looks up only
     * messages which do not have MessageHandler associated with.
     * All messages with MessageHandler associated will be handled
     * automatically by the MessageHandler instance (callback mode)
     *
     * @throws IOException if an error occurs while receiving
     * @return Message object if successful. null if no more Messages
     *         are available or messages had been processed by the
     *         callback registered
     */
    Message recv() throws java.io.IOException;

    /**
     * Register Opaque Context which will be passed onto
     * MessageHandler 
     */
    void registerOpaqueContext(String name, Object value);

    /**
     * Register MessageHandler to use callback mode for specified
     * message type
     */
    void registerMessageHandler(
		short type, 
		MessageHandler handler);
}



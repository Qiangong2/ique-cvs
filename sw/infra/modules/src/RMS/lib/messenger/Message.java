package lib.messenger;

import java.io.*;

/**
 * A unit of data that is passed between MessageReceivers and MessageSenders.
 *
 * <pre>
 *
 * MessageReceiver r = (somehow get MessageReceiver instance);
 * Message m = r.recv();
 * switch (m.type) {
 *     case MYMESSAGETYPE:
 *	   MyMessage mymsg = new MyMessage();
 *         mymsg.parseMessage(m);
 *         <process mymsg>
 *         break;
 *     case etc.:
 *         <process other message types>
 *         break;
 * } 
 * </pre>
 */
public abstract class Message {

    /** max length of a Message packet */
    public static final int MAX_MESSAGE_LENGTH = 4 * 1024;

    /** length of a byte-encoded Message header */
    public static final int HEADER_LENGTH = 2 * Sizeof.Short;

    /** Application-defined type associated with this Message */
    protected short type;

    /** 
     * Length of this Message's byte representation, includes type 
     * and length fields present in all Messages.
     */
    protected short length;

    /**
     * body part
     */
    protected byte[] body;

    /** Get the type of this message
	@return Type of this message */
    public short getType () {
	return type;
    }

    /** 
     * Write header & body
     */
    final public int writeBytes(OutputStream out) throws IOException {
        ByteToolkit.writeShort(out, type);
        ByteToolkit.writeShort(out, length);
        return writeBody(out) + HEADER_LENGTH;
    }

    /**
     * Initializes this Message based on the input byte stream <code>in</code>.
     * This method essentially undoes the work of writeBytes.
     *
     */
    final public void readBytes(InputStream in) throws IOException {
        type   = ByteToolkit.readShort(in);
        length = ByteToolkit.readShort(in);

	// read body into byte array
	body = new byte[length - HEADER_LENGTH];
	length = (short)(in.read(body) + HEADER_LENGTH);
    }

    /**
     * Parse body part to appropriate message type
     */
    final public void parseMessage(Message in) throws IOException {
	this.type = in.type;
	this.length = in.length;
	this.parseBody(new ByteArrayInputStream(in.body));
    }

    /**
     * Write message body
     * @return length of the body written
     */
    abstract protected int writeBody(OutputStream out) throws IOException;

    /**
     * Initializes the body of this Message (the part other than
     * <code>type</code> and <code>length</code>.
     */
    abstract protected void parseBody(InputStream in) throws IOException;

    /** override toString()
     */
    public String toString()
    {
	return getClass().getName() + ", type=" + type + ", length=" + length;
    }
}


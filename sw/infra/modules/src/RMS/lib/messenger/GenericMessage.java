package lib.messenger;

import java.io.*;

/**
 * A unit of data that is passed between MessageReceivers and MessageSenders.
 *
 * <pre>
 *
 * MessageReceiver r = (somehow get MessageReceiver instance);
 * Message m = r.recv();
 * switch (m.type) {
 *     case MYMESSAGETYPE:
 *	   MyMessage mymsg = new MyMessage();
 *         mymsg.parseMessage(m);
 *         <process mymsg>
 *         break;
 *     case etc.:
 *         <process other message types>
 *         break;
 * } 
 * </pre>
 * 
 */
public class GenericMessage extends Message {

    /**
     * Write message body
     * @return length of the body written
     */
    protected int writeBody(OutputStream out) throws IOException
    {
	return 0;
    }

    /**
     * Initializes the body of this Message (the part other than
     * <code>type</code> and <code>length</code>.
     */
    protected void parseBody(InputStream in) throws IOException
    {
	// do nothing
    }
}


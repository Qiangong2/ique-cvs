package lib.messenger;

import java.io.*;
import java.net.*;
import java.util.Hashtable;

/**
 * A TCPMessageSender is a MessageSender that encodes Messages onto a
 * TCP stream.
 */
public class TCPMessageSender implements MessageSender {

    /** Socket for current TCP channel */
    Socket m_sock;

    /** Current expected ACK number - 1 */
    int m_ackid;

    /** MessageHandler table: key=messageType, val=MessageHandler */
    private Hashtable handlerTable = new Hashtable();

    /** Constructs a new TCPMessageSender */
    public TCPMessageSender() {
        m_ackid = 0;
	m_sock = null;
    }

    /** 
     * Opens a TCP channel to the given IP address/port destination 
     *
     * @param addr Host address to connect to
     * @param port TCP port number to connect to
     * @throws IOException if a connection could not be established
     */
    public void start(InetAddress addr, int port)
        throws IOException
    {
        m_ackid = 0;
        m_sock = new Socket(addr, port);
    }

    /**
     * Opens a TCP channel to the given host/port destination 
     *
     * @param addr Host to connect to
     * @param port TCP port number to connect to
     * @throws IOException if a connection could not be established
     */
    public void start(String host, int port) throws IOException {
	m_ackid = 0;
	m_sock = new Socket(host, port);
    }
    
    /**
     * Closes the current TCP channel
     * 
     * @throws IOException if there was an error closing the connection
     */
    public void shutdown()
        throws IOException
    {
        if (m_sock != null) {
            m_sock.close();
            m_sock = null;
        }
    }

    /** 
     * Sends a Message over the current TCP channel
     *
     * @param m Message to send
     * @throws IOException if an error occurs while sending
     */
    public MessageAcknowledgment send(Message m)
        throws IOException
    {
        InputStream in = m_sock.getInputStream();
        OutputStream out = m_sock.getOutputStream();
        // write out message
	int bytes = m.writeBytes(out);
	out.flush();

	MessageAcknowledgment ackMesg = null;
	MessageHandler handler
		= (MessageHandler)handlerTable.get(new Short(m.type));
	if (handler != null) {
	    // read ack body
	    ackMesg = handler.readAcknowledgment(in);
	}

        // read in ack
	int ack = ByteToolkit.readInt(in);
        // check ack
        ++m_ackid;
        if (ack != m_ackid) {
            throw new IOException("ack mismatch " +
                                  ack + " != " + m_ackid);
        }
	return ackMesg;
    }


    /**
     * Register AcknowledgeHandler
     */
    public void registerMessageHandler(
		short type,
		MessageHandler handler)
    {
	handlerTable.put(new Short(type), handler);
    }
}


package lib.messenger;

import java.util.Hashtable;
import java.net.Socket;

/**
 * A context of the message exchange communication.
 * This is used to pass associsted context from receiver to
 * MessageHandler in order to construct Achnowledgment.
 */
public class TCPMessageContext extends MessageContext
{
    private Socket socket;

    /* package */ TCPMessageContext(Message message, Socket socket)
    {
	super(message);
	this.socket = socket;
    }

    /* package */ TCPMessageContext(
	    Message message, Socket socket, Hashtable context)
    {
	super(message, context);
	this.socket = socket;
    }

    public Socket getSocket()
    {
	return socket;
    }
}


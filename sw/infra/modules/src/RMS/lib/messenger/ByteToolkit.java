package lib.messenger;

import java.io.*;

/**
 * A ByteToolkit is a collection of static methods specialized for
 * reading and writing data to a byte array.
 * <p>
 * Many of these methods, by default, decode and encode integers most
 * significant byte (MSB) first, the same as network byte ordering,
 * which makes this class useful for transmitting data over networks.
 * <p>
 * Note: these methods implicitly throw ArrayIndexOutOfBoundsException
 * if their supplied buffers are too short.
 * <p>
 * 2000/11/03: added support for reading/writing data from/to an
 * <code>InputStream</code>/<code>OutputStream</code>.  These new
 * methods throws IOException on error.
 */
public class ByteToolkit {

    private static long readValue(InputStream in, int length)
	throws IOException
    {
	long value = 0;
	int n = 0;
	for (int i = 0; i < length; ++i) {
	    n = in.read();
	    value = (value << 8) | (n & 0xff);
	}
	if (n == -1)
	    throw new EOFException("ByteToolkit");
	return value;
    }
	

    /**
     * Returns a long integer, decoded MSB-first from the given buffer
     *
     * @param b      buffer to be decoded
     * @param offset index to start decoding
     * @return a long integer, decoded MSB-first from the given buffer
     */
    public static long readLong(byte[] b, int offset) {
        return ((((long) b[offset])  << 56) & 0xff00000000000000L) |
            ((((long) b[offset + 1]) << 48) & 0x00ff000000000000L) |
            ((((long) b[offset + 2]) << 40) & 0x0000ff0000000000L) |
            ((((long) b[offset + 3]) << 32) & 0x000000ff00000000L) |
            ((((long) b[offset + 4]) << 24) & 0x00000000ff000000L) |
            ((((long) b[offset + 5]) << 16) & 0x0000000000ff0000L) |
            ((((long) b[offset + 6]) <<  8) & 0x000000000000ff00L) |
            ((((long) b[offset + 7]) <<  0) & 0x00000000000000ffL);
    }

    /**
     * Similar to <code>readLong(byte[] b, int offset)</code>, but
     * read the data from the given input stream.
     *
     * @see #readLong(byte[], int)
     * @exception EOFException Input stream does not have enough bytes.
     * @exception IOException I/O error while reading from the input stream.
     */
    public static long readLong(InputStream in) throws IOException {
	return readValue(in, Sizeof.Long);
    }

    /**
     * Returns an integer, decoded MSB-first from the given buffer
     *
     * @param b      buffer to be decoded
     * @param offset index to start decoding
     * @return an integer, decoded MSB-first from the given buffer
     */
    public static int readInt(byte[] b, int offset) {
        return ((b[offset] << 24) & 0xff000000) |
            ((b[offset + 1] << 16) & 0x00ff0000) |
            ((b[offset + 2] << 8) & 0x0000ff00) |
            (b[offset + 3] & 0xff);
    }

    /**
     * InputStream version of <code>readInt(byte[] b, int offset)</code>
     * @see #readLong(InputStream)
     */
    public static int readInt(InputStream in) throws IOException {
	return (int)readValue(in, Sizeof.Int);
    }

    /**
     * Returns a short integer, decoded MSB-first from the given buffer
     *
     * @param b      buffer to be decoded
     * @param offset index to start decoding
     * @return a short integer, decoded MSB-first from the given buffer
     */
    public static short readShort(byte[] b, int offset) {
        return (short) (((b[offset] << 8) & 0xff00) | (b[offset + 1] & 0xff));
    }

    /**
     * InputStream version of <code>readShort(byte[] b, short offset)</code>
     * @see #readLong(InputStream)
     */
    public static short readShort(InputStream in) throws IOException {
	return (short)readValue(in, Sizeof.Short);
    }

    private static void writeValue(OutputStream out, long value, int length)
	throws IOException
    {
	for (int i = length - 1; i >= 0; --i) {
	    byte b = (byte) ((value >> (i * 8)) & 0xff);
	    out.write (b);
	}
    }

    /**
     * Writes a long integer, MSB-first, to the given buffer
     *
     * @param b      buffer to hold encoding
     * @param offset index to start encoding
     * @param l      long integer to encode
     */
    public static void writeLong(byte[] b, int offset, long l) {
        b[offset]     = (byte) (((l & 0xff00000000000000L) >> 56) & 0xff);
        b[offset + 1] = (byte) (((l & 0x00ff000000000000L) >> 48) & 0xff);
        b[offset + 2] = (byte) (((l & 0x0000ff0000000000L) >> 40) & 0xff);
        b[offset + 3] = (byte) (((l & 0x000000ff00000000L) >> 32) & 0xff);
        b[offset + 4] = (byte) (((l & 0x00000000ff000000L) >> 24) & 0xff);
        b[offset + 5] = (byte) (((l & 0x0000000000ff0000L) >> 16) & 0xff);
        b[offset + 6] = (byte) (((l & 0x000000000000ff00L) >>  8) & 0xff);
        b[offset + 7] = (byte) (((l & 0x00000000000000ffL) >>  0) & 0xff);
    }


    /**
     * Similar to <code>writeLong(byte[] b, int offset, long l)</code>,
     * but write the bytes to the given OutputStream instead.
     *
     * @exception IOException I/O error while writing to the stream.
     * @see #writeLong(byte[], int, long)
     */
    public static void writeLong(OutputStream out, long l) throws IOException
    {
	writeValue(out, l, Sizeof.Long);
    }

    /**
     * Writes a integer, MSB-first, to the given buffer
     *
     * @param b      buffer to hold encoding
     * @param offset index to start encoding
     * @param i      integer to encode
     */
    public static void writeInt(byte[] b, int offset, int i) {
        b[offset] = (byte) (((i & 0xff000000) >> 24) & 0xff);
        b[offset + 1] = (byte) (((i & 0x00ff0000) >> 16) & 0xff);
        b[offset + 2] = (byte) (((i & 0x0000ff00) >> 8) & 0xff);
        b[offset + 3] = (byte) (i & 0x000000ff);
    }


    /**
     * Similar to <code>writeInt(byte[] b, int offset, int l)</code>,
     * but write the bytes to the given OutputStream instead.
     *
     * @exception IOException I/O error while writing to the stream.
     * @see #writeInt(byte[], int, int)
     */
    public static void writeInt(OutputStream out, int l) throws IOException
    {
	writeValue(out, l, Sizeof.Int);
    }

    /**
     * Writes a short integer, MSB-first, to the given buffer
     *
     * @param b      buffer to hold encoding
     * @param offset index to start encoding
     * @param s      short integer to encode
     */
    public static void writeShort(byte[] b, int offset, short s) {
        b[offset] = (byte) (((s & 0xff00) >> 8) & 0xff);
        b[offset + 1] = (byte) (s & 0xff);
    }

    /**
     * Similar to <code>writeShort(byte[] b, int offset, short l)</code>,
     * but write the bytes to the given OutputStream instead.
     *
     * @exception IOException I/O error while writing to the stream.
     * @see #writeShort(byte[], int, short)
     */
    public static void writeShort(OutputStream out, short l) throws IOException
    {
	writeValue(out, l, Sizeof.Short);
    }

    /**
     * Returns a String, decoded from a null-terminated sequence of
     * bytes found in the given buffer.
     *
     * @param b      buffer to decode
     * @param offset index to start decoding
     * @return a String, decoded from a null-terminated sequence of
     *         bytes found in the given buffer.
     */
    public static String readString(byte[] b, int offset) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; ; i++) {
            char c = (char) b[offset + i];
            if (c == 0)
                break;
            else
                sb.append(c);
        }
        return sb.toString();
    }

    /**
     * InputStream version of <code>readString(byte[] b, int offset)</code>
     * @param in Input stream to read from.
     * @exception EOFException Input stream does not have enough bytes.
     * @exception IOException I/O error while reading from the input stream.
     */
    public static String readString (InputStream in)
	throws IOException
    {
	return readString(in, new StringBuffer());
    }

    /**
     * InputStream version of <code>readString(byte[] b, int offset)</code>
     * @param in Input stream to read from.
     * @param buf String buffer for holding the characters read before
     *		converted to a String.
     * @exception EOFException Input stream does not have enough bytes.
     * @exception IOException I/O error while reading from the input stream.
     */
    public static String readString (InputStream in, StringBuffer buf)
	throws IOException
    {
	int c = in.read();
	while (c != 0) {
	    if (c == -1)
		throw new EOFException("ByteToolkit");
	    else 
		buf.append((char)c);
	    c = in.read();
	}
	return buf.toString();
    }

    /**
     * Writes a String as a null-terminated sequence of bytes into
     * the given buffer.
     *
     * @param b      buffer to encode
     * @param offset index to start encoding
     * @param s      String to encode
     */
    public static void writeString(byte[] b, int offset, String s) {
        int i = 0;
        for (i = 0; i < s.length(); i++) {
            b[offset + i] = (byte) s.charAt(i);
        }
        b[offset + i] = 0;
    }

    /**
     * Writes a String as a null-terminated sequence of bytes into the
     * given output stream.
     *
     * @param out Output stream for the byte sequence.
     * @param s String to encode.
     */
    public static void writeString(OutputStream out, String s)
	throws IOException
    {
	int n = s.length();
	for (int i = 0; i < n; ++i) {
	    out.write(s.charAt(i));
	}
	out.write(0);
    }
	

    /**
     * Fills the given buffer with a constant byte
     *
     * @param b      buffer to fill
     * @param offset index to start encoding
     * @param c      constant byte value
     * @param n      number of bytes to fill
     */
    public static void memset(byte[] b, int offset, int c, int n) {
        for (int i = offset; i < offset + n; i++) {
            b[i] = (byte) (c & 0xff);
        }
    }

    private static String dump(byte[] b, int offset, int length) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            sb.append(Integer.toHexString(b[offset + i] & 0xff).toUpperCase());
            if (i != length - 1)
                sb.append(" ");
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        int n;
        byte[] b = new byte[1024];
        long l  = (1L << 63) - 1;            // 9223372036854775807
        int i   = (1 << 31) - 1;             // 2147483647
        short s = (1 << 15) - 1;             // 32767
        String msg = "Hello, World!";

        for (int tmp = 0; tmp < b.length; tmp++) b[tmp] = (byte) 0xaa;

        writeLong(b, 0, l);
        long tmpl = readLong(b, 0);
        dump(b, 0, 8);
        System.out.println("" + (l == tmpl) + " : " + l + " == " + tmpl + 
                           " (" + dump(b, 0, 8) + ")");

        writeInt(b, 0, i);
        int tmpi = readInt(b, 0);
        dump(b, 0, 4);
        System.out.println("" + (i == tmpi) + " : " + i + " == " + tmpi + 
                           " (" + dump(b, 0, 4) + ")");

        writeShort(b, 0, s);
        int tmps = readShort(b, 0);
        dump(b, 0, 2);
        System.out.println("" + (s == tmps) + " : " + s + " == " + tmps + 
                           " (" + dump(b, 0, 2) + ")");

        writeString(b, 0, msg);
        String tmpmsg = readString(b, 0);
        dump(b, 0, msg.length() + 1);
        System.out.println("" + msg.equals(tmpmsg) + " : " + msg + " == " + tmpmsg + 
                           " (" + dump(b, 0, msg.length() + 1) + ")");

    }
}

package lib.messenger;

import java.util.Hashtable;

/**
 * A context of the message exchange communication.
 * This is used to pass associsted context from receiver to
 * MessageHandler in order to construct Achnowledgment.
 */
public class MessageContext
{
    protected Message	message;

    protected Hashtable context;

    private MessageContext() { /* prevent being instantiated with no mesg */ }

    /* package */ MessageContext(Message message)
    {
	this(message, new Hashtable());
    }

    /* package */ MessageContext(Message message, Hashtable context)
    {
	this.message = message;
	this.context = context;
    }

    /* package */ void setOpaqueContext(String name, Object value)
    {
	context.put(name, value);
    }

    public Object getOpaqueContext(String name)
    {
	return context.get(name);
    }

    public Message getMessage()
    {
	return message;
    }
}


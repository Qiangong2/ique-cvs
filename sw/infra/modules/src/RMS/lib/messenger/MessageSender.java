package lib.messenger;

/**
 * A MessageSender is a source of Messages sent to a MessageReceiver.
 * <p>
 * Typically, a MessageSender has three methods (implementors are 
 * encouraged to follow this specification):
 * <ul>
 * <li> <code>startup</code>, which runs implementor-specific code to
 *      initialize the output channel from MessageSender to 
 *      MessageReceiver.  The declaration for this method is not 
 *      specified here because the state needed to initialize a input 
 *      channel is typically specific to each MessageSender.
 * <li> <code>send</code>, which sends Messages to the MessageReceiver.
 * <li> <code>shutdown</code>, which runs implementor-specific code to
 *      close the output channel.
 * </ul>
 */
public interface MessageSender {
    /**
     * Sends a Message
     *
     * @param m Message to send
     */
    public MessageAcknowledgment send(Message m) throws java.io.IOException;

    /**
     * Register MessageHandler for messages coming back with
     * an extra data for specified message type
     */
    void registerMessageHandler(
		short type, 
		MessageHandler handler);
}


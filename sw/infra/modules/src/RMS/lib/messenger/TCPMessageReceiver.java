package lib.messenger;

import java.io.*;
import java.net.*;
import java.util.*;

/**
 * A TCPMessageReceiver is MessageReceiver that decodes Messages from
 * a TCP stream.
 */
public class TCPMessageReceiver implements Runnable, MessageReceiver {

    /** Array of buffers for receiving Messages */
    byte[][] m_buffers;

    /** Pool of free buffers */
    Vector m_freeBuffers;

    /** Socket for accepting incoming connections */
    ServerSocket m_serverSock;

    /** Thread for accepting incoming connections */
    Thread m_serverThread;

    /** Thread for processing incoming connections */
    Thread m_workerThread;

    /** Queue for storing unprocessed, incoming connections */
    Vector m_socketQueue;

    /** Queue for storing parsed but unprocessed Message buffers */
    Vector m_mesgQueue;

    /** True iff this receiver is shutdown or is shutting down */
    boolean m_isShutdown;

    /** Number of buffers to maintain */
    private static int MAX_BUFFERS = 2;

    /** MessageHandler table (key=messate type, val=MessageHandler)
     *  Context */
    private Hashtable handlerTable = new Hashtable();
    private Hashtable contextTable = new Hashtable();

    /** ack id table for given socket: key=Socket value=Integer */
    private Hashtable ackTable = new Hashtable();

    /** Constructs a new TCPMessageReceiver */
    public TCPMessageReceiver() {
        m_buffers = new byte[MAX_BUFFERS][Message.MAX_MESSAGE_LENGTH];
        m_freeBuffers = new Vector(MAX_BUFFERS);
        for (int i = 0; i < MAX_BUFFERS; i++) {
            m_freeBuffers.add(m_buffers[i]);
        }
        m_mesgQueue = new Vector();
        m_socketQueue = new Vector();
        m_isShutdown = true;
    }

    /** 
     * Opens a listening TCP channel on the given host/port pair 
     *
     * @param addr Host interface to listen on, if null, this receiver
     *             will accept connections on any/all local interface
     *             (really only pertinent for multi-homed hosts)
     * @param port TCP port number to listen on
     * @throws IOException if an error occured trying to open the channel
     */
    public void start(InetAddress addr, int port)
        throws IOException
    {
        m_isShutdown = false;
        m_serverSock = new ServerSocket(port, 50, addr);
        // start worker thread
        m_workerThread = new Thread(this);
        m_workerThread.start();
        // start server thread
        m_serverThread = new Thread(this);
        m_serverThread.start();
    }

    /**
     * Closes the current TCP channel
     *
     * @throws IOException if an error occured trying to close the channel
     */
    public void shutdown()
        throws IOException
    {
        m_isShutdown = true;
        // close server
        if (m_serverSock != null) {
            m_serverSock.close();
            m_serverThread.interrupt();
	    boolean joined = false;
	    while (!joined) {
		try {
		    m_serverThread.join();
		    joined = true;
		} catch (Exception e) {}
	    }
            m_serverSock = null;
            m_serverThread = null;
        }
        // close worker
        if (m_workerThread != null) {
            m_workerThread.interrupt();
	    boolean joined = false;
	    while (!joined) {
		try {
		    m_workerThread.join();
		    joined = true;
		} catch (Exception e) {}
	    }
            m_workerThread = null;
        }

	m_mesgQueue.clear();
    }

    /** Reads in a Message from the current TCP stream with timeout
     */
    public Message recv() throws IOException {
	return recv(0);
    }

    /** Reads in a Message from the current TCP stream with specified
     * timeout
     * @param tout timeout in milliseconds
     */
    public Message recv(long tout) throws IOException {

        // wait for a received buffer (blocks indefinitely until
        // a connection with a new MessageSender has been made, 
        // or until that sender sends a Message)
	synchronized (m_mesgQueue) {
	    while (m_mesgQueue.isEmpty()) {
		try {
		    m_mesgQueue.wait(tout);
		}
		catch (InterruptedException e) {
		    return null;
		}
	    }
	    return (Message)m_mesgQueue.remove(0);
	}
    }

    /**
     * Register Opaque Context which will be passed onto
     * MessageHandler
     */
    public void registerOpaqueContext(String name, Object value)
    {
	contextTable.put(name, value);
    }

    /**
     * Register MessageHandler
     */
    public void registerMessageHandler(
		    short type,
		    MessageHandler handler)
    {
	handlerTable.put(new Short(type), handler);
    }

    /** 
     * Dispatches to an appropriate thread routine, entry-point for 
     * all threads 
     */
    public void run() {
        Thread t = Thread.currentThread();
        if (t == m_serverThread) {
            serverRun();
        }
        else {
            try {
                workerRun();
            }
            catch (InterruptedException e) { }
        }
    }

    /** 
     * Repeatedly accepts incoming TCP connections and queues them for
     * later processing
     */
    private void serverRun() {

        try {
            while (true) {
		Socket s = null;
		try {
		    m_serverSock.setSoTimeout(5000); // 5sec
		    s = m_serverSock.accept();

		} catch (InterruptedIOException e) { }

		if (m_isShutdown) {
		    return;
		}

		if (s != null) {
		    ackTable.put(s, new Integer(0));

		    synchronized (m_socketQueue) {
			m_socketQueue.add(s);
			m_socketQueue.notify();
		    }
		}
            }

        } catch (IOException e) {
            if (!m_isShutdown) {
                e.printStackTrace(System.err);
	    }
        }
    }

    /**
     * Repeatedly acquires incoming TCP connections from the socket 
     * queue, and processes them.
     */
    private void workerRun()
        throws InterruptedException
    {
        while (true) {
            Socket s;
            // wait for incoming socket
            synchronized (m_socketQueue) {
                while (m_socketQueue.isEmpty()) {
                    m_socketQueue.wait();
		}
                s = (Socket) m_socketQueue.remove(0);
            }
            // process socket
            try {
                processSocket(s);
            }
            catch (IOException e) {
                e.printStackTrace(System.err);
            }
        }
    }

    /** 
     * Handles a single TCP connection by reading all Messages, 
     * queueing them for receivers, and sending back ACKs.
     */
    private void processSocket(Socket s)
        throws IOException, InterruptedException
    {
        InputStream in   = s.getInputStream();
        OutputStream out = s.getOutputStream();
	boolean socket_closed = false;
        while (true) {
            byte[] b = null;
	    boolean valid_data = false;

            // wait for a empty buffer
	    try {
		// critical section that a buffer is out of queues
		synchronized (m_freeBuffers) {
		    while (m_freeBuffers.isEmpty()) {
			m_freeBuffers.wait();
		    }
		    b = acquireFreeBuffer();
		}
		int i = 0;
		int r = 0;
		// read in message header
		for (int n = Message.HEADER_LENGTH; n > 0; n -= r, i += r) {
		    try {
			r = in.read(b, i, n);
		    }
		    catch (IOException e) {
			s.close();
			socket_closed = true;
			throw new InterruptedException();
		    }
		    if (r == -1) {
			// this stream is finished, process the next one
			s.close();
			socket_closed = true;
			return;
		    }
		}
		short length = ByteToolkit.readShort(b, 2);
		length -= Message.HEADER_LENGTH;
		// read in rest of message
		for (int n = length; n > 0; n -= r, i += r) {
		    r = in.read(b, i, n);
		    if (r == -1) {
			throw new IOException("eof reading body");
		    }
		}

		// message has been read appropriately
		valid_data = true;

	    } finally {

		if (b != null) {
		    if (valid_data) {
			handleMessage(b, s, out); // it will free the buffer

		    } else {
			// connection has been closed
			freeBuffer(b);
		    }
		}

		if (socket_closed) {
		    ackTable.remove(s);
		}
            }
        }
    }

    private void handleMessage(
		    byte[] b,
		    Socket s,
		    OutputStream out) throws IOException
    {
	if (b == null) {
	    return;
	}

	// parse the message
	GenericMessage m = null;
	try {
	    // critical section that a buffer is out of queues
	    m = new GenericMessage();
	    m.readBytes(new ByteArrayInputStream(b));

	} finally {

	    freeBuffer(b);
	}

	
	MessageHandler handler
		= (MessageHandler)handlerTable.get(new Short(m.type));
	if (handler != null) {
	    // message handler has been registerd - use
	    // callback model
	    TCPMessageContext
		cxt = new TCPMessageContext(m, s, contextTable);
	    handler.writeAcknowledgment(out, cxt);

	} else {
	    // let it handle in recv() function
	    synchronized (m_mesgQueue) {
		m_mesgQueue.add(m);
		m_mesgQueue.notify();
	    }
	}

	Integer o_ackid = (Integer)ackTable.get(s);
	int ackid = 0;
	if (o_ackid != null) {
	    ackid = o_ackid.intValue() + 1;
	}
        byte[] ack = new byte[4];
	ByteToolkit.writeInt(ack, 0, ackid);
	out.write(ack);
	ackTable.put(s, new Integer(ackid));
    }

    /** dequeue an buffer from freeBuffers: freeBuffers--
     */
    private byte[] acquireFreeBuffer()
    {
	synchronized (m_freeBuffers) {
	    byte[] b = (byte[])m_freeBuffers.remove(0);
	    return b;
	}
    }

    /** move the freed buffer to freeBuffer: freeBuffers++
     */
    private void freeBuffer(byte[] b)
    {
        synchronized (m_freeBuffers) {
            m_freeBuffers.add(b);
            m_freeBuffers.notify();
        }
    }

}

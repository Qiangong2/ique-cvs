package lib.messenger;

/**
 * Static class holding the sizes of primitive types.
 */
public class Sizeof
{
    /** Size of a <code>long</code> (8 bytes) */
    public static final int Long = 8;
    /** Size of a <code>int</code> (4 bytes) */
    public static final int Int = 4;
    /** Size of a <code>short</code> (2 bytes) */
    public static final int Short = 2;
    /** Size of a <code>byte</code> (1 byte) */
    public static final int Byte = 1;
}

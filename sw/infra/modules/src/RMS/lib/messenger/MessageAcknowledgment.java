package lib.messenger;

/**
 * Interface to define ACK message.
 * Any message which has an extra data returned to the sender
 * needs to implement this interface.
 * This is a signature interface, and has no method.
 */
public interface MessageAcknowledgment { }


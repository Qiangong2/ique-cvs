package lib.messenger;

import java.io.*;

/**
 * Interface to define message handling for callback model.
 * Any message which has an extra data returned to the sender
 * needs to implement this interface and register to bothe
 * sender and receiver.
 */
public interface MessageHandler
{
    /**
     * Handler for MessageReceiver. Write acknowledgement message
     * to the sender. Those message will be sent in front to ACK ID
     * @param out OutputStream to write ack
     * @param receiver MessageReceiver instance that this function is
     *                 called from 
     */
    void writeAcknowledgment(OutputStream out, MessageContext contxt)
		    throws IOException;

    /**
     * Handler for MessageSender. Read acknowledgement message
     * from the receiver.
     */
    MessageAcknowledgment readAcknowledgment(InputStream in)
		    throws IOException;
}


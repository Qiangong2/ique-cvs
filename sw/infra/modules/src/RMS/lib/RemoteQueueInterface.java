package lib;

import java.rmi.*;
/**
 * Remote interface for the RemoteQueue
 */
public interface RemoteQueueInterface extends Remote {
    public void enqueue(Object o) throws RemoteException;
    public Object dequeue() throws RemoteException;
    public Object dequeueNoBlock() throws RemoteException;
}

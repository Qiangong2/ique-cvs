package lib;

/**
 * SMTP exception
 *
 * no "package" in order to maintain "consistency" with
 * legacy java code ...
 */
public class SMTPException extends Exception
{
    private int	mCode;

    /**
     * Constructs an <code>SMTPException</code> with no specified detail message.
     */
    public SMTPException(int code) {
        super();
        mCode = code;
    }

    /**
     * Constructs an <code>SMTPException</code> with the specified detail message.
     *
     * @param   s   the detail message.
     */
    public SMTPException(String s, int code) {
        super(s);
        mCode = code;
    }

    /**
     * get response code
     */
    public int getResponseCode() { return mCode; }

    /**
     * override toString()
     */
    public String toString()
    {
	StringBuffer sb = new StringBuffer();
	sb.append(super.toString());
	sb.append(" :SMTP error code = " + mCode);
	return sb.toString();
    }
}


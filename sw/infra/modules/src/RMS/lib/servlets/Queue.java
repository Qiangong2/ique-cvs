package lib.servlets;

import java.util.Vector;

/**
 * A FIFO queue that is used to buffer up the incoming messages and
 * pass them along to the database in large batches.
 * <p>
 * A batch is sent to the database when <code>max_size</code> messages
 * are received, or when <code>timeout</code> milliseconds has passed.
 */
public class Queue
{
    /** mutex of the queue */
    private Object queueMutex = new Object();

    /** mutex for enqueue request */
    private Object enqueueMutex = new Object();

    /** The message buffer */
    private Vector queue;

    /** Time out (in milliseconds) */
    private final int timeout;

    /** Max. number of messages in the queue before flushing. */
    private final int max_size;

    /** Actual max. size of the queue */
    private final int queue_size;

    /**
     * Construct a new Queue.
     * @param TimeOut Time out value in milliseconds.
     * @param MaxSize Max. number of elements in the queue.
     */
    public Queue(int TimeOut, int MaxSize) {
	timeout = TimeOut;
	max_size = MaxSize;
	// set the actual buffer size to 12% larger than max. to avoid
	// unnecessary growing of the buffer.  Note that the queue
	// might not get flush right away upon notification and more
	// might entries might be inserted before that.
	queue_size = max_size + max_size / 8;
	queue = new Vector(queue_size);
    }

    /**
     * Append an object to the end of the queue.  If the queue is
     * full, notify the receiving thread that a new batch is ready
     * and wait till swapQueue() is executed so that it won't put
     * more than max_size elements to the queue object
     * 
     * @param o The object (message) to be appended.
     */
    public void enqueue (Object o) {

	synchronized(enqueueMutex) {
	    // Block any other thread until this request goes through
	    // Do not use this mutex in other place. It will possibly
	    // cause dead lock

	    synchronized(queueMutex) {

		while (queue.size() >= max_size) {

		    queueMutex.notifyAll(); // intends to notify dequeueAll()

		    try {
			// wait till swapQueue notifies me
			queueMutex.wait(timeout);


		    } catch (InterruptedException e) {
			// somebody interrupted me - must be shutting down
			return;
		    }
		}

		queue.add(o);
	    }
	}
    }

    /**
     * replace the queue with empty one. This will make it
     * ready to accept more enqueue requests.
     */
    private Vector swapQueue () {

	synchronized(queueMutex) {

	    Vector result = queue;
	    queue = new Vector(queue_size);
	    queueMutex.notifyAll(); // intends to notify enqueue() function

	    return result;
	}
    }

    /**
     * Remove all elements from the queue and return them as a
     * <code>Vector</code>.
     * <p>
     * It blocks the executing thread until a new batch is ready or timeout.
     * @exception InterruptedException
     *			The current thread is interrupted by another
     *			thread while waiting for the next batch.  This
     *			happens when the servlet engine is being shutdown.
     * @return A <code>Vector</code> of elements in the queue.
     */
    public Vector dequeueAll () throws InterruptedException {

	synchronized(queueMutex) {

	    if (queue.size() >= max_size)
		return swapQueue();

	    for (;;) {

		queueMutex.wait(timeout); // wait till enqueue notifies me

		if (queue.size() > 0) {
		    return swapQueue();
		}
	    }
	}
    }

    /**
     * Unconditionally returns the remaining elements in the queue.
     * This method is only used for cleaning up the buffered messages
     * when the servlet is being shut down.
     * @return A <code>Vector</code> of all elements in the queue.
     */
    public Vector clear () {

	synchronized(queueMutex) {
	    Vector result = queue;
	    queue = null;
	    return result;
	}
    }

} // Queue



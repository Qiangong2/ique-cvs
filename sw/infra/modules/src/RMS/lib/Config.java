package lib;

import java.util.Properties;
import java.util.Vector;
import java.util.Enumeration;
import java.io.*;

/**
 * Wraps the loading of the RouteFree.properties file in class path and
 * /etc/routefree.conf file. <p>
 * RouteFree.properties or /etc/routefree.conf file contains global
 * configuration information such as the database parameters, SMTP HOST
 * information for sending mail etc.
 * The purpose to wrap this into a class is that in a multi threaded model, the
 * file is loaded once and shared across threads and also within a single thread
 * the properties file doesn't have to be passed around.
 **/
public final class Config {

    /**
     * top directory of broadon binaries
     */
    public static final String BROADON_PKGS
				    = "/opt/broadon/pkgs";
    public static final String BROADON_RMS_TOP
				    = BROADON_PKGS + "/rms";
    public static final String BROADON_ONLINE_SERVLET
				    = BROADON_RMS_TOP + "/servlet-hronline";
    public static final String BROADON_ADMIN_SERVLET
				    = BROADON_RMS_TOP + "/servlet-hradmin";
    public static final String BROADON_ADMIN_PROP_FILE
				    = BROADON_ADMIN_SERVLET +
				      "/webapps/hron_admin/admin.properties";
    public static final String BROADON_DATA
				    = "/opt/broadon/data/rms";
    public static final String BROADON_CERTS
				    = BROADON_DATA + "/.certs";
    public static final String BROADON_ONLINE_CERTS
				    = BROADON_CERTS + "/hronline";
    public static final String BROADON_ADMIN_CERTS
				    = BROADON_CERTS + "/rms";
    public static final String BROADON_ETC
				    = BROADON_DATA + "/etc";
    public static final String BROADON_LOG_DIR
				    = BROADON_DATA + "/logs";

    public static String GLOBAL_CONFIG_FILE
				    = BROADON_ETC + "/routefree.conf";

    /** Default database values
     */
    public static final String DEFAULT_DB_USER
				    = "hron";
    public static final String DEFAULT_DB_PASSWD
				    = "hron";
    public static final String DEFAULT_DB_CLASS
				    = "oracle.jdbc.driver.OracleDriver";
    public static final String TEST_DB_URL
				    = "jdbc:oracle:thin:" +
				      "@lab1-db1.routefree.com:1521:bccdb";

    /**
     * Security related.
     */
    public static final String KEY_FILE
				    = BROADON_ADMIN_CERTS + "/hrrpc.key";
    public static final String KEY_PASSWORD
				    = "serverpw";
    public static final String TRUST_FILE
				    = BROADON_PKGS +
				      "/jre/lib/security/jssecacerts";
    public static final String TRUST_PASSWORD
				    = "changeit";
    public static final String TRUST_FILE_PARAM
				    = "Truststore";
    public static final String TRUST_PASSWORD_PARAM
				    = "TruststorePassword";

    public static final String BROADON_LICENSE_PROP_FILE
				    = BROADON_ADMIN_SERVLET +
				      "/webapps/hron_admin/WEB-INF/classes" +
				      "/admin_license.properties";
    public static final long LICENSE_THREAD_SLEEP_TIME
				    = 3600000;

    private static Properties m_rfProperties = null;

    private Config() {
    }

    public static synchronized Properties getProperties () {
        if (m_rfProperties == null) {
	    m_rfProperties = new Properties();
	    loadInto(m_rfProperties);
        }
        return (m_rfProperties);
    }

    public static synchronized void loadInto (Properties dst) {
        if (dst != null) {
            try {
		//
		// load RouteFree.properties in classpath
		//
                ClassLoader cl = Config.class.getClassLoader();
                InputStream appStream =
                    cl.getResourceAsStream("RouteFree.properties");
                if (appStream != null) {
                    dst.load (appStream);
                    appStream.close();
                }
		//
		// load /etc/routefree.conf
		// properties defined in this file will override ones
		// in RouteFree.properties
		//
		File cfile = new File(GLOBAL_CONFIG_FILE);
		if (cfile.isFile() && cfile.canRead()) {
		    FileInputStream fin = new FileInputStream(cfile);
		    dst.load(fin);
		    fin.close();
		}

            } catch (IOException ioEx) {
                System.out.println ("Error loading RouteFree.properties file.");
                System.out.println (ioEx.getMessage());
            }
        }
    }

    public static void merge(Properties src, Properties dst)
    {
	Enumeration e = src.propertyNames();
	while (e.hasMoreElements()) {
	    String name = (String)e.nextElement();
	    String val = src.getProperty(name);
	    dst.setProperty(name, val);
	}
    }

    /**
     * utility to get indexed property like a.1, a.2, a.3.
     *
     * It treats "a" as "a.1" (a.1 override a if both specified)
     * It returns an array with 1 element of the default vallue
     * specified
     */
    public static String[] getIndexedProperty(
				String prefix,
				Properties prop,
				String def_val)
    {
	Vector v = new Vector();
	String s = prop.getProperty(prefix + ".1");
	if (s == null) {
	    s = prop.getProperty(prefix);
	    if (s != null) {
		v.addElement(s);
	    }
	} else {
	    v.addElement(s);
	    for (int i=2; ; i++) {
		s = prop.getProperty(prefix + "." + i);
		if (s != null) {
		    v.addElement(s);
		} else {
		    break;
		}
	    }
	}

	if (v.size() <= 0) {
	    String[] ret = new String[1];
	    ret[0] = def_val;
	    return ret;
	}

	return toStringArray(v);
    }

    private static String[] toStringArray(Vector v)
    {
	String[] ret = new String[v.size()];
	int i=0;
	Enumeration e = v.elements();
	while (e.hasMoreElements()) {
	    ret[i++] = (String)e.nextElement();
	}
	return ret;
    }
}

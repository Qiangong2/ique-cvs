/*
 * (C) 2001, RouteFree, Inc.,
 * $Id: CertUtil.java,v 1.1 2002/08/07 23:58:50 vaibhav Exp $
 */
package lib;

import java.security.cert.*;
import java.util.Enumeration;
import java.io.*;
import java.security.*;
import java.security.spec.*;

public class CertUtil {

   public static X509Certificate[] getCertChain(String keyStorePath, char[] keyStorePassword)
                 throws IOException, NoSuchAlgorithmException, KeyStoreException, CertificateException
  {

       X509Certificate[] X509Certs = null;
       KeyStore keyStore = null;

// Check for valid arguments
       FileInputStream keyStoreIStream = null;

       try {
// Open the stream to read in the keystore.
           keyStoreIStream = new FileInputStream(keyStorePath);
       } catch(FileNotFoundException e) {
           keyStoreIStream = null;
       }

// Create a KeyStore Object
       keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
      
// Init the Keystore with the contents of the keystore file.
// If the input stream is null the keystore is initialized empty.
       keyStore.load(keyStoreIStream, keyStorePassword);

// Close keystore input stream
       if(keyStoreIStream != null)
       {
          keyStoreIStream.close();
          keyStoreIStream = null;
       }

       try {
// See how many certificates are in the keystore.
           int numberOfEntry = keyStore.size();

// If there are any certificates in the keystore.
           if(numberOfEntry > 0)
           {
// Create an array of X509Certificates
               X509Certs = new X509Certificate[numberOfEntry];

// Get all of the certificate alias out of the keystore.
               Enumeration aliases = keyStore.aliases();

// Retrieve all of the certificates out of the keystore
// via the alias name.
               int i = 0;
               while (aliases.hasMoreElements())
               {
                   X509Certs[i] = (X509Certificate)keyStore.getCertificate((String)aliases.nextElement());
                   i++;
               }
           }
       } catch( Exception e ) {
            X509Certs = null;
       }
    
       return X509Certs;
   }

}

package lib;

/**
 * Cryptography Utility using cryptix 3.2
 *
 *  no "package" in order to maintain "consistancy" with
 *  the legacy Java code ...
 *
 */

import xjava.security.*;
import java.security.*;

import cryptix.provider.key.RawSecretKey;

/**
 * Encrypt or Decrypt a string
 * @author Hiro@RouteFree.com
 */
public class Crypto
{
    private Cipher mCipher;
    private Key    mKey;

    private static Crypto mInstance = null;
    private static Object mInsMutex = new Object();

    private static final byte[] RAW_KEY = { 114, -76, 54, 72, -16, 116, -69, 33 };
    private static final String KEY_ALGORYTHM = "DES";

    /**
     * Singleton pattern: avoid instantiating multiple instance
     * within one JVM.
     */
    public static Crypto getInstance() throws CryptoException
    {
        synchronized(mInsMutex) {
            if (mInstance == null) {
                try {
                    // create a key
                    RawSecretKey key = new RawSecretKey(KEY_ALGORYTHM, RAW_KEY);
                    mInstance = new Crypto(key);

                } catch (Exception e) {
                    throw new CryptoException(e.toString(), e);
                }
            }
            return mInstance;
        }
    }

    /**
     * <strong>
     * Constructing a Crypto object takes long time. e.g. 5 sec for Linux/x86 
     * on 750Mhz 128MB machine. <br>
     * Therefore, be cautious on how and when to instantiate this object
     * </strong>
     */
    private Crypto(Key key) throws CryptoException
    {
        try {
            // register Cryptix as Security Provider
            Security.addProvider(new cryptix.provider.Cryptix());
            // instantiating Cipher for DES algorithm
            mCipher = Cipher.getInstance(
                            Cipher.getInstance("DES", "Cryptix"),
                            null,
                            PaddingScheme.getInstance("PKCS#5", "Cryptix")
                  );
            mKey = key;

        } catch (Exception e) {

            throw new CryptoException(e.toString(), e);
        }
    }


    /**
     * encrypt specified string, and convert encoded byte[] into
     * human readable (URL acceptable) string
     */
    public synchronized String encrypt(String orig_str)
                           throws CryptoException
    {
        try {
            // initialize the Cipher with the Key for encryption
            mCipher.initEncrypt(mKey);
            byte[] encoded = mCipher.crypt(orig_str.getBytes());
            StringBuffer b = new StringBuffer(128);
            for (int i=0; i<encoded.length; i++) {
                String elem = Integer.toHexString(encoded[i] + 128);
                                                      // make it positive value
                if (elem.length() == 1) {
                    b.append("0");
                }
    
                b.append(elem);
            }
            return b.toString();

        } catch (Exception e) {
    
            throw new CryptoException(e.toString(), e);
        }
    }

    /**
     * decrypt specified string into original string.
     */
    public synchronized String decrypt(String enc_str)
                           throws CryptoException
    {
        try {
            // initialize the Cipher with the Key for decription
            mCipher.initDecrypt(mKey);
    
            // first, convert the string into byte[]
            int len = enc_str.length() / 2;
            byte[] enc_bytes = new byte[len]; 
            for (int i=0; i < len; i++) {
                int v = Integer.parseInt(enc_str.substring(i*2, i*2+2), 16);
                enc_bytes[i] = (byte)(v - 128);
            }
            return new String(mCipher.crypt(enc_bytes));

        } catch (Exception e) {

            throw new CryptoException(e.toString(), e);
        }
    }


    /**
     * execute test; encrypt args & decrypt
     */
    public static void main(String args[])
    {
        try {
            if (args.length > 0) { // execute tests
                System.out.println(new java.util.Date());
                Crypto c = getInstance();
                System.out.println(new java.util.Date());
                for (int i=0; i<args.length; i++) {
                    System.out.println("Original:  " + args[i]);
    
                    String enc = c.encrypt(args[i]);
                    System.out.println("Encrypted: " + enc);
    
                    String dec = c.decrypt(enc);
                    System.out.println("Decrypted: " + dec);
                }
            } else {
                // display Key's raw data
                // instantiating KeyGenerator & SecretKey
                SecureRandom random = new SecureRandom();
                KeyGenerator keygen = KeyGenerator.getInstance("DES");
                keygen.initialize(random);
                Crypto c = new Crypto(keygen.generateKey());

                // display Key's raw data
                System.out.println("class=" + c.mKey.getClass().getName());
                System.out.println("algorithm=" + c.mKey.getAlgorithm());
                System.out.println("format=" + c.mKey.getFormat());
                byte[] raw = c.mKey.getEncoded();
                for (int i=0; i<raw.length; i++) {
                    System.out.print(raw[i] + ", ");
                    if ((i % 16) == 15) {
                        System.out.println();
                    }
                }
                System.out.println();
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}




#ifndef __PING_H__
#define __PING_H__

#define P_STOP_FIRST_CONTACT		(1 << 0)

struct pingtest {
	unsigned int ipaddr;		/* dest ip address in network order */
	int npackets;			/* number of packets to send */
	int interval;			/* time between transmits (in secs) */
	int maxtime;			/* maximum time for test (in secs) */
	unsigned int flags;		/* additional flags */
};

struct pingresult {
	int nsent;			/* number of transmitted packets */
	int nrecv;			/* number of received packets */
	double avg_rtt;			/* average round-trip-time (in ms) */
};

extern int
ping_probe(struct pingtest *ptest, struct pingresult *presult);

#endif /* __PING_H__ */

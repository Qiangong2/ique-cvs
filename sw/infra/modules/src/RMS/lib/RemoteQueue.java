package lib;

import java.rmi.*;
import java.rmi.server.*;
import java.util.Vector;

// Simple thread-safe Queue

public class RemoteQueue extends UnicastRemoteObject implements RemoteQueueInterface {
    private Vector queue;

    RemoteQueue() throws RemoteException { queue = new Vector(); }

    public synchronized void enqueue (Object o) throws RemoteException {
    	queue.add(o);
	notifyAll();
    }

    public synchronized Object dequeue() throws RemoteException {
    	while (queue.size() == 0) {
	    try {
	    	wait();
	    } catch (InterruptedException leaveUsAlonePlease) {}
	}
	return queue.remove(0);
    }

    public synchronized Object dequeueNoBlock() throws RemoteException {
	if (queue.size() == 0) return null;
	else return queue.remove(0);
    }
}

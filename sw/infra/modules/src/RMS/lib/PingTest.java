package lib;

public class PingTest {
    /** Flag that specifies a test should stop after the first response */
    public static final int STOP_FIRST_CONTACT = (1 << 0);
    /** Destination test host */
    protected String m_host;
    /** Maximum packets to transmit */
    protected int m_packetCount;
    /** Minimum time between transmissions */
    protected int m_interval;
    /** Maximum total duration of test */
    protected int m_maxTime;
    /** Optional flags */
    protected int m_flags;

    public PingTest(String dest, int packetCount, int interval, int duration,
                    int flags)
    {
        m_host = dest;
        m_packetCount = packetCount;
        m_interval = interval;
        m_maxTime = duration;
        m_flags = flags;
    }

    /** Returns the destination host address */
    public String getAddress() { return m_host; }

    /** Sets the destination host address */
    public void setAddress(String addr) {
        m_host = addr;
    }

    /** Returns the maximum packets to transmit */
    public int getPacketCount() { return m_packetCount; }

    /** Returns the minimum time between transmissions */
    public int getInterval() { return m_interval; }

    /** Returns the maximum total duration of test */
    public int getDuration() { return m_maxTime; }

    /** Returns any optional flags */
    public int getFlags() { return m_flags; }
}

package lib;

import java.util.Vector;

// Simple thread-safe Request Queue

public class RQueue {
    private Vector queue;

    RQueue() { queue = new Vector(); }

    public synchronized void enqueue (Object o) {
    	queue.add(o);
	notifyAll();
    }

    public synchronized Object dequeue() {
    	while (queue.size() == 0) {
	    try {
	    	wait();
	    } catch (InterruptedException leaveUsAlonePlease) {}
	}
	return queue.remove(0);
    }
}

package lib;

/**
 * Use SMTP (25) to send emails
 *
 *
 *  no "package" in order to maintain "consistancy" with
 *  the legacy Java code ...
 *
 */
import java.io.*;
import java.net.Socket;
import java.net.InetAddress;
import java.util.Date;

import java.util.Properties;

/**
 * Use SMTP daemon to send an email
 * @author Hiro@RouteFree.com
 */
public class SendMail
{
    // in Config
    private static final String PROP_SMTP_HOST_NAME    = "SMTP_HOST_NAME";
    private static final String PROP_EMAIL_SENDER_NAME = "EMAIL_SENDER_NAME";
    private static final String PROP_EMAIL_SENDER_ADDR = "EMAIL_SENDER_ADDR";

    private static final int	 SMTP_PORT = 25;
    private static final boolean ADD_REAL_HOSTNAME = false;

    private static int       mSMTPport;
    private String           mSMTPhost;
    private boolean          mAddRealHost;
    private String           mLocalHostName;
    private String           mSender;
    private String           mSenderAddress;

    /**
     * @param host SMTP host
     */
    public SendMail()
    {
        this("localhost");

        Properties p = Config.getProperties();
        String host = (String)p.getProperty(PROP_SMTP_HOST_NAME);
        if (host != null && host.length() > 0) {
            mSMTPhost = host;
        }
    }

    /**
     * @param host SMTP host
     */
    public SendMail(String host)
    {
        this(host, SMTP_PORT, ADD_REAL_HOSTNAME);
    }

    public SendMail(String host, int port)
    {
        this(host, port, ADD_REAL_HOSTNAME);
    }

    public SendMail(String host, boolean add_realhost)
    {
        this(host, SMTP_PORT, add_realhost);
    }

    public SendMail(String host, int port, boolean add_realhost)
    {
        mSMTPhost = host;
        mSMTPport = port;
        mAddRealHost = add_realhost;
        try {
            mLocalHostName = InetAddress.getLocalHost().toString();
            // trancate trailing "/IP address"
            int idx = mLocalHostName.indexOf('/');
            if (idx >= 0) {
                mLocalHostName = mLocalHostName.substring(0, idx);
            }

        } catch(Exception e) {
            // cannot get localhost information
            mLocalHostName = "localhost";
        }

        Properties p = Config.getProperties();
        mSender =        (String)p.getProperty(PROP_EMAIL_SENDER_NAME);
        mSenderAddress = (String)p.getProperty(PROP_EMAIL_SENDER_ADDR);
    }

    public synchronized String getSender() { return mSender; }
    public synchronized void setSender(String s) { mSender = s; }
    public synchronized String getSenderAddress() { return mSenderAddress; }
    public synchronized void setSenderAddress(String s) { mSenderAddress = s; }
    public synchronized String getLocalHostName() { return mLocalHostName; }

    public void sendMessage(String to,
                               String subject,
                               String message) 
                      throws SMTPException
    {
        Socket s = null;
        BufferedReader in;
        String lastline, helohost = mSMTPhost;

        try {
            String res;
            s = new Socket(mSMTPhost, mSMTPport);

            PrintStream p = new PrintStream(s.getOutputStream());
            in = new BufferedReader(new InputStreamReader(s.getInputStream()));

            checkResponse(in, "220", "no greeting");

            p.print("HELO " + mLocalHostName + "\r\n");
            lastline = checkResponse(in, "250", "helo");

            int pos;

            // 250 <host name>
            if (mAddRealHost) {
                if ((pos = lastline.indexOf(' ')) != -1) {
                    helohost = lastline.substring(pos + 1);
                    if ((pos = helohost.indexOf(' ')) != -1) {
                        helohost = helohost.substring(0, pos);
                    }
                }
            }

            p.print("MAIL FROM:<" + mSenderAddress + ">" + "\r\n");
            checkResponse(in, "250", "mail from");
            
            p.print("RCPT TO:<" + to + ">\r\n");
            checkResponse(in, "250", "rcpt to");
            
            p.print("DATA\r\n");
            checkResponse(in, "354", "data");

            p.print("Subject: " + subject);
            if (mAddRealHost)
                p.print(" (" + helohost + ")");
            p.print("\r\n");

            p.print("From: " + mSender +
                              " <" + mSenderAddress + ">" + "\r\n");

            p.print("To: <" + to + ">" + "\r\n");

            BufferedReader is = new BufferedReader(new StringReader(message));

            while (true) {
                String ln = is.readLine();
                if (ln == null)
                    break;

                if (ln.equals("."))
                    ln = "..";
                p.println(ln);
            }

            p.print("\r\n.\r\n");
            checkResponse(in, "250", "end of data");

            p.print("QUIT\r\n");
            checkResponse(in, "221", "quit");

        } catch (IOException e) {
            // convert it to SMTPException
            CharArrayWriter cw = new CharArrayWriter();
            PrintWriter w = new PrintWriter(cw);
            e.printStackTrace(w);
            throw new SMTPException(e.toString(), 550);

        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (Exception e) {
                    // something happened to the socket
                    // SMTPException must have been called
                }
            }
        }
    }


    /**
     * method to read input stream, and skip lines with "expected" string
     * @return last line
     */
    private String checkResponse(BufferedReader in,
                               String expected,
                               String errmsg)
                   throws SMTPException
    {
        try {
            String lastline = in.readLine();
            int code = 550; // default error code
    
            if (!lastline.startsWith(expected)) {
                //
                // this is not expected response
                //
                int idx = lastline.indexOf('-');
                String smtpmsg = lastline;
                try {
                    if (idx >= 0) {
                        code = Integer.parseInt(lastline.substring(0, idx));
                    } else {
                        idx = lastline.indexOf(' ');
                        code = Integer.parseInt(lastline.substring(0, idx));
                    }
                    smtpmsg = lastline.substring(idx);

                } catch (NumberFormatException e) {
                    code = 550; // access defined
    
                } finally {
                    throw new SMTPException(errmsg + " " + smtpmsg, code);
                }
            }
    
            while (lastline.startsWith(expected + "-"))
                lastline = in.readLine();
    
            // last line always: <num> <host name>
            return lastline;

        } catch (IOException e) {
            // convert it to SMTPException
            throw new SMTPException(errmsg + " I/O Error", 550);
        }
    }
}




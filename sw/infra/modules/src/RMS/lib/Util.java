package lib;

import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.TimeZone;
import java.util.Date;

public class Util {
    public static String FormatRFC1123Date(java.util.Date date) {
	// create date format for an rfc1123 date
	SimpleDateFormat df =
	    new SimpleDateFormat("EEE, dd MMM yyyy hh:mm:ss z");
	TimeZone tz = TimeZone.getTimeZone("GMT");
	df.setTimeZone(tz);
	return df.format(date);
    }

    public static Date ParseRFC1123Date(String string) throws ParseException {
	// create date format for an rfc1123 date
	SimpleDateFormat df =
	    new SimpleDateFormat("EEE, dd MMM yyyy hh:mm:ss z");
	return df.parse(string);
    }

}

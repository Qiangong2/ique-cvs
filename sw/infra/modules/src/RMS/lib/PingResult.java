package lib;

public class PingResult {
    /** Number of sent packets */
    protected int m_sent;
    /** Number of received packets */
    protected int m_received;
    /** Average round-trip time (in ms) */
    protected double m_avgrtt;

    public PingResult() {
        m_sent = 0;
        m_received = 0;
        m_avgrtt = 0;
    }

    /** Returns the number of sent packets */
    public int getSentPackets() { return m_sent; }

    /** Sets the number of sent packets */
    public void setSentPackets(int sent) { m_sent = sent; }

    /** Returns the number of received packets */
    public int getReceivedPackets() { return m_received; }

    /** Sets the number of received packets */
    public void setReceivedPackets(int received) { m_received = received; }

    /** Returns the average round-trip time (in ms) */
    public double getAverageRtt() { return m_avgrtt; }

    /** Sets the average round-trip time (in ms) */
    public void setAverageRtt(double rtt) { m_avgrtt = rtt; }
}

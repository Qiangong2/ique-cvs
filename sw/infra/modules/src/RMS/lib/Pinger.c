/*
 * JNI wrapper to call ping routine
 */
#ifdef SOLARIS
#include <sys/types.h>
#endif
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "lib_Pinger.h"

#ifdef __cplusplus
extern "C" {
#endif

#include "ping.h"

extern int ping_it(unsigned target_ip_address, int n_test_pkts, int tout /* sec */);

/*
 * Class:     lib_Pinger
 * Method:    ping
 * Signature: (Ljava/lang/String;)Z
 */
JNIEXPORT jboolean JNICALL Java_lib_Pinger_ping(JNIEnv *e, jobject obj, jstring addr, jint tout)
{
    const char *addrstr;
#ifdef SOLARIS
    in_addr_t ip_address;
#else
    struct in_addr ip_address;
#endif
    int ret, t;
    jboolean result;

    addrstr = (*e)->GetStringUTFChars(e, addr, NULL);

#ifdef SOLARIS
    ip_address = inet_addr(addrstr);
    ret = (ip_address != -1);
#else
    ret = inet_aton(addrstr, &ip_address);
#endif

    if (ret) {
#ifdef SOLARIS
	t = ping_it(ip_address, 10, tout);
#else
	t = ping_it(ip_address.s_addr, 10, tout);
#endif
	if (t > 0) {
	    result = -1;
	} else {
	    result = 0;
	}
    } else {
	result = 0;
    }

    (*e)->ReleaseStringUTFChars(e, addr, addrstr);

    return result;
}

static int
to_netaddr(const char *host, unsigned int *netaddr) {
#ifdef SOLARIS
    in_addr_t addr = inet_addr(host);
    if (addr == -1)
        return -1;
    *netaddr = addr;
#else /* SOLARIS */
    struct in_addr addr;
    if (inet_aton(host, &addr) == 0)
        return -1;
    *netaddr = addr.s_addr;
#endif /* SOLARIS */    

    return 0;
}

/*
 * Class:     lib_Pinger
 * Method:    doProbe
 * Signature: (Llib/PingTest;Llib/PingResult;)I
 */
JNIEXPORT jint JNICALL Java_lib_Pinger_doProbe
  (JNIEnv *env, jobject thisobj, jobject test, jobject result)
{
    int i;
    int ret;
    jclass cls;
    jstring jstr;
    jmethodID mid;
    const char *addrstr;
    struct pingtest ptest;
    struct pingresult presult;
    struct {
        char *method;
        int *target;
    } inputMap[] = {
        { "getPacketCount", &(ptest.npackets) },
        { "getInterval", &(ptest.interval) },
        { "getDuration", &(ptest.maxtime) },
        { "getFlags", &(ptest.flags) },
        { NULL, NULL }
    };
    struct {
        char *method;
        int *isrc;
        double *dsrc;
    } outputMap[] = {
        { "setSentPackets",     &(presult.nsent), NULL },
        { "setReceivedPackets", &(presult.nrecv), NULL },
        { "setAverageRtt",      NULL,             &(presult.avg_rtt) },
        { NULL, NULL, NULL }
    };

    cls = (*env)->GetObjectClass(env, test);
    mid = (*env)->GetMethodID(env, cls, "getAddress", "()Ljava/lang/String;");
    if (mid == 0) {
        return -1;
    }
    jstr = (*env)->CallObjectMethod(env, test, mid);
    addrstr = (*env)->GetStringUTFChars(env, jstr, NULL);
    ret = to_netaddr(addrstr, &(ptest.ipaddr));
    (*env)->ReleaseStringUTFChars(env, jstr, addrstr);
    if (ret < 0) {
        return -1;
    }

    for (i = 0; inputMap[i].method != NULL; i++) {
        mid = (*env)->GetMethodID(env, cls, inputMap[i].method, "()I");
        if (mid == 0) {
            return -1;
        }
        *(inputMap[i].target) = (*env)->CallIntMethod(env, test, mid);
    }

    if (ping_probe(&ptest, &presult) < 0)
        return -1;

    cls = (*env)->GetObjectClass(env, result);
    for (i = 0; outputMap[i].method != NULL; i++) {
        int is_int = (outputMap[i].isrc != NULL);
        char *sig = is_int ? "(I)V" : "(D)V";
        mid = (*env)->GetMethodID(env, cls, outputMap[i].method, sig);
        if (mid == 0) {
            return -1;
        }
        if (is_int)
            (*env)->CallVoidMethod(env, result, mid, 
                                   ((jint) *(outputMap[i].isrc)));
        else
            (*env)->CallVoidMethod(env, result, mid, 
                                   ((jdouble) *(outputMap[i].dsrc)));
    }

    return 0;
}

#ifdef __cplusplus
}
#endif

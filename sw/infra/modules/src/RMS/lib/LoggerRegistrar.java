package lib;


import java.io.*;
import java.util.*;

public class LoggerRegistrar {

    private static HashMap loggers = new HashMap();

    private LoggerRegistrar() {
    }
    
    synchronized static public void register (String name, Logger logger) {

        if (logger != null) {
            loggers.put (name, logger);
        }
    }
    
    synchronized static public Logger getLogger (String name) {
        Logger logger = (Logger) loggers.get(name);
        return (logger);
    }

    synchronized static public Logger getLogger () {
        Logger logger = (Logger) loggers.get(Thread.currentThread().getName());
        return (logger);
    }

    synchronized static public void clear () {
        loggers.clear();
    }
};

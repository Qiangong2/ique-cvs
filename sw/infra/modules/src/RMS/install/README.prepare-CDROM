
To prepare for installation CD:

INSTTOP .. installation top directory of the CD-ROM image

First, copy the entire directory structure here to the to top:
(except CVS directories)

  cd <your source tree>/rf/src/server/install
  cp -R * $INSTTOP
  find . -name CVS -print | xargs rm -fr

Then do the followings:

A). Unser server/ directory

LINUX_BUILD_TOP .. source top of the linux build.
It is currently /home/build/sysimage/<date>s on 'deathstar'

SOLARIS_BUILD_TOP .. source top of the solaris build
It is currently /u01/build/sysimage/<data> on 'panther'


1. create directories "linux", "solaris", "java", "db", "web", and "support"

   mkdir -p $INSTTOP/server/linux
   mkdir -p $INSTTOP/server/solaris
   mkdir -p $INSTTOP/server/java
   mkdir -p $INSTTOP/server/db
   mkdir -p $INSTTOP/server/web
   mkdir -p $INSTTOP/server/web

2. copy dist/ of linux build to "linux" directory
   NOTE: do not include certs/, jre/, httpd/, servlet-hronline/,
         servlet-hradmin/ and cmd/ directory

   ( cd $LINUX_BUILD_TOP/rf/src/server/dist; tar cf - . ) | ( cd $INSTTOP/server/linux; tar xf -)
   rm -rf $INSTTOP/server/linux/certs
   rm -rf $INSTTOP/server/linux/jre
   rm -rf $INSTTOP/server/linux/httpd
   rm -rf $INSTTOP/server/linux/servlet
   rm -rf $INSTTOP/server/linux/servlet-hradmin
   rm -rf $INSTTOP/server/linux/cmd

3. copy dist/ of solaris build to "solaris" directory
   NOTE: do not include jre/, httpd/, servlet-hronline/,
         servlet-hradmin/ and cmd/ directory

   ( rsh -n panther "cd $SOLARIS_BUILD_TOP/rf/src/server/dist; tar cf - ." ) | ( cd $INSTTOP/server/solaris; tar xf -)
   rm -rf $INSTTOP/server/linux/jre
   rm -rf $INSTTOP/server/linux/httpd
   rm -rf $INSTTOP/server/linux/servlet
   rm -rf $INSTTOP/server/linux/servlet-hradmin
   rm -rf $INSTTOP/server/linux/cmd


4. extract Database schema creation scripts to db directory

   ( rsh -n deathstar "dd if=$LINUX_BUILD_TOP/rf/src/server/database/create/rmsdb22.tar" ) | ( cd $INSTTOP/server/db; tar xf - )

5. Copy tar files containing documents to be exposed in RMS Management
   Console to server/web/ directory.

   cp <any tar files> $INSTTOP/server/web

   Those files stored in the tar files will be accessible by:

       http://<rms-server>:8088/<file path>

6. Copy tar files containing documents to be exposed in Front-end servers
   to server/support/ directory.

   cp <any tar files> $INSTTOP/server/support

   Those files stored in the tar files will be accessible by:

       http://<hronline-server>/<file path>

NOTE: this installation script does not install certificates
      please read rf/src/server/certs/README.txt for certificate
      creating and installation

B). Unser gateway/ directory

gateway:

GATEWAY_BUILD_TOP .. source top of the gateway build.
It is currently /home/build/sysimage/<date> on 'deathstar'

1. create a directory "relase"

   mkdir -p $INSTTOP/gateway/release

2. Copy hw_model.xml from rf/src/hr/release to gateway/release/.

   The file contains a model for the development, and these model
   should be removed.

3. Copy model_rel_XXX.xml as gateway/release/model_rel_master.xml

   The model_rel_XXX.xml can be found at:

     $GATEWAY_BUILD_TOP/model_rel_dev.xml           (Development version)
     $GATEWAY_BUILD_TOP/acer/model_rel_acer.xml     (Acer build)
     $GATEWAY_BUILD_TOP/master/model_rel_master.xml (Production build)

   IMPORTANT NOTE:
     For Acer, do the following:
       - remove all model definition of hw revision 0x0000,
         0x0101, and 0x0201 EXCEPT 0x0201 & HR/Exp and 0x0201 & unknown.
       - remove mod_harddisk_10g and mod_harddisk_40g modules.

4. Copy sw_rel_XXX.xml as gateway/release/model_rel_master.xml

   The sw_rel_XXX.xml can be found at:

     $GATEWAY_BUILD_TOP/sw_rel_dev.xml           (Development version)
     $GATEWAY_BUILD_TOP/acer/sw_rel_acer.xml     (Acer build)
     $GATEWAY_BUILD_TOP/master/sw_rel_master.xml (Production build)

   WARNING: must copy the same type of xml as model_rel_XXX.xml file

   IMPORTANT NOTE:
     For Acer, do the following:
       - remove all model definition of hw revision 0x0000,
         0x0101, and 0x0201 EXCEPT 0x0201 & HR/Exp and 0x0201 & unknown.
       - change <last_known_good> tag value to "no"

5. Copy GatewayOS binaris from the directory to gateway/release/. These are:

      appfs.img.sig
      appfs.img.cry
      kernel.img.sig
      kernel.img.cry

   These files can be found under:

     $GATEWAY_BUILD_TOP/        (development version)
     $GATEWAY_BUILD_TOP/acer/   (Acer build)
     $GATEWAY_BUILD_TOP/master/ (Production build)


   WARNING: must copy the binaries for the same type as the above xml files

6. Copy MediaExp files into gateway/mediaexp/ directory. These are:

      mediaexp.xml
      release.dsc
      MediaExpSetup.exe

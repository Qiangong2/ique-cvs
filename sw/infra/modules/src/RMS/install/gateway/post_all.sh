#!/bin/sh 
#
# change the following variables for defining releases to post
#

# List of directories that contains GatewayOS images replacing
# currently existing ones
DELETE_REL_DIRS=""

# List of directories that contains newly released GatewayOS images
POST_REL_DIRS="release"

#
# do not change the following
#

CWD=`dirname $0`

case $CWD in
  /*) CWD=$CWD;;
  *) CWD=`pwd`/$CWD;;
esac

POST_CMD="/bin/sh $CWD/bin/post_release"
DEL_CMD="/bin/sh $CWD/bin/delete_release"

# arg 1 directory
post_gatewayos()
{
    dir=$1

    # posting GatewayOS

    if [ -f $CWD/$dir/hw_rel.xml ] ; then
        echo "Updating HW definitions"
        $POST_CMD $CWD/$dir/hw_rel.xml
    fi

    if [ -f $CWD/$dir/model_rel_master.xml ] ; then
        echo "Updating Model definitions"
        $POST_CMD $CWD/$dir/model_rel_master.xml
    fi

    GATEWAYOS_FILES="\
        $CWD/$dir/sw_rel_master.xml \
        $CWD/$dir/appfs.img.cry \
        $CWD/$dir/appfs.img.sig \
        $CWD/$dir/kernel.img.cry \
        $CWD/$dir/kernel.img.sig"

    if [ -f $CWD/$dir/sw_rel_master.xml ] ; then
        echo "Installing Software release"
        $POST_CMD $GATEWAYOS_FILES
    fi
}

# arg 1 directory
remove_gatewayos()
{
    dir=$1

    SW_DESC_FILE="\
        $CWD/$dir/sw_rel_master.xml"

    if [ -f $SW_DESC_FILE ] ; then
        echo "Removing software"
        $DEL_CMD $SW_DESC_FILE
    fi
}

post_mediaexp()
{
    # posting MediaExp
    
    MEDIAEXP_FILES="\
        $CWD/mediaexp/mediaexp.xml \
        $CWD/mediaexp/release.dsc \
        $CWD/mediaexp/MediaExpSetup.exe"

    if [ -f $CWD/mediaexp/mediaexp.xml ] ; then
        echo "Installing MediaExp PC software"
        $POST_CMD $MEDIAEXP_FILES
    fi
}

post_license()
{
    # posting license
      
    LICENSE_FILE="$CWD/../license/post_license.sh"

    if [ -f $LICENSE_FILE ] ; then
        /bin/sh $LICENSE_FILE
    fi
}

if [ x"$DELETE_REL_DIRS" != x ] ; then
    for dir in $DELETE_REL_DIRS ; do
        remove_gatewayos $dir
    done
fi

if [ x"$POST_REL_DIRS" != x ] ; then
    for dir in $POST_REL_DIRS ; do
        post_gatewayos $dir
    done
fi

post_mediaexp
post_license

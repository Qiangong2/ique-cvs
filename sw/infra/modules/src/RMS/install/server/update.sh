#!/bin/sh
#
# BroadOn - update script
#
# $Revision: 1.3 $
# $Date: 2003/03/19 22:21:38 $
#

if [ `whoami` != root ] ; then
    echo "please execute this as 'root' user"
    exit 1
fi

BROADON_HOME=${BROADON_HOME:-/opt/broadon/pkgs}
RMS_HOME=${BROADON_HOME}/rms
ONLINE_DIR=${RMS_HOME}/servlet-hronline/webapps
ADMIN_DIR=${RMS_HOME}/servlet-hradmin/webapps
HTDOC_DIR=${BROADON_HOME}/httpd/htdocs

MV=mv
RM=/bin/rm
MKDIR=mkdir
CP=cp -p
LN=ln

CWD=`dirname $0`

case $CWD in
  /*) CMDPATH=$CWD;;
  *) CMDPATH=`pwd`/$CWD;;
esac
CMDNAME=`basename $0`

# arg1 .. mesg
# arg2 .. default value
# arg3 .. valid values
enter_value()
{
    while true ; do
	$ECHO_N "$1"
	read line
	if [ x"$line" = x ] ; then
	    if [ x"$2" = x ] ; then
	        continue;
	    fi
	    ret=$2
	    return 
	fi
	if [ x"$3" != x ] ; then
	    for val in $3 ; do
		if [ x"$line" = x"$val" ] ; then
		    ret=$line
		    return
		fi
	    done
	fi
    done
}

stop_servlet()
{
    # old servlets
    if [ -x /usr/local/apache/bin/apachectl ] ; then
	/usr/local/apache/bin/apachectl stop > /dev/null 2>&1
    fi
    if [ -x /usr/local/apache/bin/hronctl ] ; then
	/usr/local/apache/bin/hronctl stop > /dev/null 2>&1
    fi
    if [ -x /usr/local/tomcat/bin/tomcat.sh ] ; then
	/usr/local/tomcat/bin/tomcat.sh stop > /dev/null 2>&1
    fi
    if [ -x /usr/local/tomcat-hron/bin/tomcat.sh ] ; then
	/usr/local/tomcat-hron/bin/tomcat.sh stop > /dev/null 2>&1
    fi
    # new servlets
    if [ -x ${RMS_HOME}/bin/broadon ] ; then
        ${RMS_HOME}/bin/broadon stop > /dev/null 2>&1
    fi
}

start_servlet()
{
    if [ -x ${RMS_HOME}/bin/broadon ] ; then
        ${RMS_HOME}/bin/broadon start
    fi
}

cleanup_webapps()
{
    find $1 -name WEB-INF -print | while read webdir ; do
        cdir=`dirname $webdir`
        case $cdir in
            ${RMS_HOME}*) $RM -fr $cdir ;;
	    *) echo "found WEB-INF in $cdir" ;;
        esac
    done
}

cleanup_hronline()
{
    if [ -d $ONLINE_DIR ] ; then
        cleanup_webapps $ONLINE_DIR
    fi
}

cleanup_hradmin()
{
    if [ -d $ADMIN_DIR ] ; then
        cleanup_webapps $ADMIN_DIR
    fi
}

install_activation()
{
    $CP $CMDPATH/linux/web_servers/hronline/hr_activate.war $ONLINE_DIR/.
}

install_nms()
{
    $CP $CMDPATH/linux/web_servers/hronline/hr_status.war $ONLINE_DIR/.
    $CP $CMDPATH/linux/web_servers/hronline/hr_stat.war $ONLINE_DIR/.
}

install_swupdate()
{
    $CP $CMDPATH/linux/web_servers/hronline/hr_update.war $ONLINE_DIR/.
    $CP $CMDPATH/linux/web_servers/hronline/hr_download.war $ONLINE_DIR/.
}

install_rmsconsole()
{
    $CP $CMDPATH/linux/web_servers/hronline/hron_admin.war $ADMIN_DIR/.
    $CP $CMDPATH/linux/web_servers/hronline/hron_b2b.war $ADMIN_DIR/.
    $CP $CMDPATH/linux/web_servers/hronline/hron_proxy.war $ADMIN_DIR/.
    $CP $CMDPATH/linux/web_servers/hronline/hron_sw_rel.war $ADMIN_DIR/.
    $CP $CMDPATH/linux/web_servers/hronline/stat_exporter.war $ADMIN_DIR/.
#    $CP $CMDPATH/linux/web_servers/hrdepot/hrdepot.war $ADMIN_DIR/.
#    for file in $CMDPATH/web/*.tar eof ; do
#        if [ x$file = xeof ] ; then
#	    break;
#	fi
#	dd if=$file | ( cd $HTDOC_DIR; tar xf - )
#    done
}

install_version()
{
    $RM -f ${RMS_HOME}/etc/version
    $CP $CMDPATH/linux/etc/version ${RMS_HOME}/etc/.
}

# select platform

case `uname` in
    linux|Linux) platform=linux;;
    SunOS) platform=solaris;;
    *) platform=unknown;;
esac

if [ $platform = unknown ] ; then
    enter_value "Specify your platform (solaris or linux) : " "" "solaris linux"
    platform=$ret
fi

if [ $platform = solaris ] ; then
    ECHO_N=printf
    ECHO=echo
else 
    ECHO_N="echo -n"
    ECHO=echo
fi

# find installed servers

if [ -f $ONLINE_DIR/hr_activate.war ] ; then
    activation=y
else
    activation=n
fi

if [ -f $ONLINE_DIR/hr_status.war ] ; then
    swupdate=y
else
    swupdate=n
fi

if [ -f $ONLINE_DIR/hr_update.war ] ; then
    nms=y
else
    nms=n
fi

if [ -f $ADMIN_DIR/hron_admin.war ] ; then
    rmsconsole=y
else
    rmsconsole=n
fi

# update necessary files

$ECHO "Shutting down servlet engine and http server"
stop_servlet

$ECHO "Clean up old files"
cleanup_hronline
cleanup_hradmin

if [ $activation = y ] ; then
    $ECHO "Updating Service Activation Server"
    install_activation
fi

if [ $nms = y ] ; then
    $ECHO "Updating Network Management Server"
    install_nms
fi

if [ $swupdate = y ] ; then
    $ECHO "Updating Software Update Server"
    install_swupdate
fi

if [ $rmsconsole = y ] ; then
    $ECHO "Updating RMS Management Console"
    install_rmsconsole
fi

install_version

$ECHO "Starting up BroadOn servers"
start_servlet

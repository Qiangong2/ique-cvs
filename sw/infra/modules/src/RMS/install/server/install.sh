#!/bin/sh
#
# BroadOn - installation script
#
# $Revision: 1.21 $
# $Date: 2003/03/19 22:21:38 $
#

if [ `whoami` != root ] ; then
    echo "please execute this as 'root' user"
    exit 1
fi

BROADON_HOME=${BROADON_HOME:-/opt/broadon/pkgs}
RMS_HOME=${BROADON_HOME}/rms
ONLINE_DIR=${RMS_HOME}/servlet-hronline/webapps
ADMIN_DIR=${RMS_HOME}/servlet-hradmin/webapps
HTDOC_DIR=${BROADON_HOME}/httpd/htdocs

MV=mv
RM=/bin/rm
MKDIR=mkdir
CP=cp -p
LN=ln

CWD=`dirname $0`

case $CWD in
  /*) CMDPATH=$CWD;;
  *) CMDPATH=`pwd`/$CWD;;
esac
CMDNAME=`basename $0`

# arg1 .. mesg
# arg2 .. default value
# arg3 .. valid values
enter_value()
{
    while true ; do
	$ECHO_N "$1"
	read line
	if [ x"$line" = x ] ; then
	    if [ x"$2" = x ] ; then
	        continue;
	    fi
	    ret=$2
	    return
	fi
	if [ x"$3" != x ] ; then
	    for val in $3 ; do
		if [ x"$line" = x"$val" ] ; then
		    ret=$line
		    return
		fi
	    done
	fi
    done
}

install_db()
{
    $ECHO "This may take several hours to complete .. please wait"
    su - oracle -c "(cd $CMDPATH/db; sh rmscreate.sh)" > /db-install.log 2>&1
}

install_only_schema()
{
    $ECHO "This may take several hours to complete .. please wait"
    su - oracle -c "(cd $CMDPATH/db; sh rmsschema.sh)" > /db-install.log 2>&1
}

upgrade_db_schema()
{
    $ECHO "This may take several minutes to complete .. please wait"
    su - oracle -c "(cd $CMDPATH/db; sh rmsupgrade.sh)" > /db-upgrade.log 2>&1
}

stop_servlet()
{
    # old servlets
    if [ -x /usr/local/apache/bin/apachectl ] ; then
	/usr/local/apache/bin/apachectl stop > /dev/null 2>&1
    fi
    if [ -x /usr/local/apache/bin/hronctl ] ; then
	/usr/local/apache/bin/hronctl stop > /dev/null 2>&1
    fi
    if [ -x /usr/local/tomcat/bin/tomcat.sh ] ; then
	/usr/local/tomcat/bin/tomcat.sh stop > /dev/null 2>&1
    fi
    if [ -x /usr/local/tomcat-hron/bin/tomcat.sh ] ; then
	/usr/local/tomcat-hron/bin/tomcat.sh stop > /dev/null 2>&1
    fi
    # new servlets
    if [ -x ${RMS_HOME}/bin/broadon ] ; then
        ${RMS_HOME}/bin/broadon stop > /dev/null 2>&1
    fi
}

install_servlet_engine()
{
    $RM -fr ${RMS_HOME}-old
    if [ -d ${RMS_HOME} ] ; then
	$MV ${RMS_HOME} ${RMS_HOME}-old
    fi

#    ( cd /; tar xf $CMDPATH/$platform/web_servers/apache-dist.tar )
#    ( cd /; tar xf $CMDPATH/$platform/web_servers/mod_jk-dist.tar )
#    ( cd /; tar xf $CMDPATH/linux/web_servers/tomcat-dist.tar )

#    ( cd $BROADON_HOME; tar xf $CMDPATH/java/jre131_01-${platform}.tar )
#    $CP $CMDPATH/java/jdk131_01-${platform}-tools.jar $BROADON_HOME/jre/lib/ext/tools.jar
#    $CP $CMDPATH/java/jsse/*.jar $BROADON_HOME/jre/lib/ext/.

    $MKDIR -p ${RMS_HOME}/etc
    $CP $CMDPATH/linux/etc/routefree.conf ${RMS_HOME}/etc/.
    $CP $CMDPATH/linux/etc/oracle_env.sh ${RMS_HOME}/etc/.
    $CP $CMDPATH/linux/etc/version ${RMS_HOME}/etc/.
    $MKDIR -p ${RMS_HOME}/bin
    $CP $CMDPATH/linux/bin/broadon ${RMS_HOME}/bin/.
    $CP $CMDPATH/linux/bin/httpd_funcs ${RMS_HOME}/bin/.

    if [ $hronline = n ] ; then
        $RM -fr ${RMS_HOME}/servlet-hronline
        $RM -f ${RMS_HOME}/httpd/conf/httpd.conf-hronline
    else
   	$CP $CMDPATH/linux/bin/hronline ${RMS_HOME}/bin/.
    fi

    if [ $hradmin = n ] ; then
        $RM -fr ${RMS_HOME}/servlet-hradmin
        $RM -f ${RMS_HOME}/httpd/conf/httpd.conf-hradmin
    else
   	$CP $CMDPATH/linux/bin/hradmin ${RMS_HOME}/bin/.
   	$CP $CMDPATH/linux/bin/postrel ${RMS_HOME}/bin/.
	$CP $CMDPATH/linux/etc/routefree.hrdepot.conf ${RMS_HOME}/etc/.
    fi
}

install_activation()
{
    $CP $CMDPATH/linux/web_servers/hronline/hr_activate.war $ONLINE_DIR/.
}

install_config_backup()
{
    $CP $CMDPATH/linux/web_servers/hronline/hr_config_backup.war $ONLINE_DIR/.
}

install_nms()
{
    $CP $CMDPATH/linux/web_servers/hronline/hr_status.war $ONLINE_DIR/.
    $CP $CMDPATH/linux/web_servers/hronline/hr_stat.war $ONLINE_DIR/.
}

install_swupdate()
{
    $CP $CMDPATH/linux/web_servers/hronline/hr_update.war $ONLINE_DIR/.
    $CP $CMDPATH/linux/web_servers/hronline/hr_download.war $ONLINE_DIR/.
}

install_support_pages()
{
    for file in $CMDPATH/support/*.tar eof ; do
        if [ x$file = xeof ] ; then
	    break;
	fi
	dd if=$file | ( cd $HTDOC_DIR; tar xf - )
    done
}

install_rmsconsole()
{
    $CP $CMDPATH/linux/web_servers/hronline/hron_admin.war $ADMIN_DIR/.
    $CP $CMDPATH/linux/web_servers/hronline/hron_b2b.war $ADMIN_DIR/.
    $CP $CMDPATH/linux/web_servers/hronline/hron_proxy.war $ADMIN_DIR/.
    $CP $CMDPATH/linux/web_servers/hronline/hron_sw_rel.war $ADMIN_DIR/.
    $CP $CMDPATH/linux/web_servers/hronline/stat_exporter.war $ADMIN_DIR/.
    $CP $CMDPATH/linux/web_servers/hrdepot/hrdepot.war $ADMIN_DIR/.
    $CP $CMDPATH/linux/etc/error.conf ${RMS_HOME}/etc/.
    $CP $CMDPATH/linux/etc/diag.conf ${RMS_HOME}/etc/.
    $CP -R $CMDPATH/linux/etc/statistics ${RMS_HOME}/etc/.
    for file in $CMDPATH/web/*.tar eof ; do
        if [ x$file = xeof ] ; then
	    break;
	fi
	dd if=$file | ( cd $HTDOC_DIR; tar xf - )
    done
}

install_jars()
{
    $MKDIR -p ${RMS_HOME}/lib
    $CP -R $CMDPATH/linux/lib/*.jar ${RMS_HOME}/lib/.
}

install_util()
{
    $CP -R $CMDPATH/linux/bin/*.sh ${RMS_HOME}/bin/.
}

install_ams()
{
    $MKDIR -p ${RMS_HOME}/etc ${RMS_HOME}/bin
    $CP $CMDPATH/linux/etc/alarm_system_rules.xml ${RMS_HOME}/etc/.
    $CP $CMDPATH/linux/bin/ams ${RMS_HOME}/bin/.
}

install_snmp()
{
    $MKDIR -p ${RMS_HOME}/etc/mib ${RMS_HOME}/bin ${RMS_HOME}/lib
    if [ -f ${RMS_HOME}/etc/broadon_proxy.mib ] ; then
        $MV ${RMS_HOME}/etc/broadon_proxy.mib ${RMS_HOME}/etc/broadon_proxy.mib.old
    fi
    $CP $CMDPATH/$platform/lib/libping.so ${RMS_HOME}/lib/.
    $CP $CMDPATH/linux/etc/mib/broadon_proxy.mib ${RMS_HOME}/etc/mib/.
    $CP $CMDPATH/linux/bin/snmpproxy ${RMS_HOME}/bin/.
}

configure_initd()
{
    START_LINK=S99broadon
    STOP_LINK=K10broadon

    if [ $1 = linux ] ; then
        START_DIRS="/etc/rc.d/rc3.d /etc/rc.d/rc4.d /etc/rc.d/rc5.d"
        STOP_DIRS="/etc/rc.d/rc0.d /etc/rc.d/rc1.d /etc/rc.d/rc2.d /etc/rc.d/rc6.d"
    else
        START_DIRS="/etc/rc3.d"
        STOP_DIRS="/etc/rc0.d"
    fi

    for dir in $START_DIRS ; do
	$RM -f $dir/$START_LINK
	$LN -s ${RMS_HOME}/bin/broadon $dir/$START_LINK
    done

    for dir in $STOP_DIRS ; do
	$RM -f $dir/$STOP_LINK
	$LN -s ${RMS_HOME}/bin/broadon $dir/$STOP_LINK
    done
}

# select platform

case `uname` in
    linux|Linux) platform=linux;;
    SunOS) platform=solaris;;
    *) platform=unknown;;
esac

if [ $platform = unknown ] ; then
    enter_value "Specify your platform (solaris or linux) : " "" "solaris linux"
    platform=$ret
fi

if [ $platform = solaris ] ; then
    ECHO_N=printf
    ECHO=echo
else
    ECHO_N="echo -n"
    ECHO=echo
fi

# select server(s) to instal

while true ; do

    enter_value "Do you want to install Service Activation Server (y/n) ? " "" "y n"
    activation=$ret

    enter_value "Do you want to install Software Update Server (y/n) ? " "" "y n"
    swupdate=$ret

    enter_value "Do you want to install Network Management Server (y/n) ? " "" "y n"
    nms=$ret

    enter_value "Do you want to install Configuration Backup Server (y/n) ? " "" "y n"
    configbackup=$ret

    if [ $activation = y -o $swupdate = y -o $nms = y -o $configbackup = y ] ; then
        enter_value "Do you want to install user support pages on this server (y/n) ? " "" "y n"
        support=$ret
    else
        support=n
    fi

    enter_value "Do you want to install RMS Management Console (y/n) ? " "" "y n"
    rmsconsole=$ret

    if [ $rmsconsole = y ] ; then
        if [ $activation = y -o $swupdate = y -o $nms = y -o $configbackup = y ] ; then
	    enter_value "Are you sure to install both front-end server(s) and RMS Management Console in the same machine (y/n) ? " "" "y n"
	    if [ $ret = n ] ; then
	    	continue;
	    fi
	fi
    fi
    break;

done

if [ $rmsconsole = y ] ; then

    enter_value "Do you want to install Alarm Management System (y/n) ? " "" "y n"
    ams=$ret

    enter_value "Do you want to install SNMP Proxy Server (y/n) ? " "" "y n"
    snmp=$ret
else
    ams=n
    snmp=n
fi

enter_value "Do you have Database installed (y/n) ? " "" "y n"
if [ $ret = y ] ; then
    enter_value "Do you want to upgrade the database, install only schema, or configure RMS DB from scratch (upgrade/schema/install) ? " "" "upgrade schema install"
    dbms=$ret
else
    dbms=none
fi

enter_value "Do you want to configure the system so that BroadOn servers would automatically start/stop (y/n) ? " "" "y n"
initd=$ret

# determine if online servlet needs to be installed

if [ $activation = y -o $swupdate = y -o $nms = y -o $configbackup = y ] ; then
    hronline=y
else
    hronline=n
fi

# determine if admin servlet needs to be installed

if [ $rmsconsole = y ] ; then
    hradmin=y
else
    hradmin=n
fi

$ECHO "Installtion Options:"
$ECHO
$ECHO "Platform                   : $platform"
$ECHO "Service Activation Server  : $activation"
$ECHO "Software Update Server     : $swupdate"
$ECHO "Network Management Server  : $nms"
$ECHO "Configuration Backup Server: $configbackup"
$ECHO "User Support Web Pages     : $support"
$ECHO "RMS Management Console     : $rmsconsole"
if [ $rmsconsole = y ] ; then
   $ECHO "Alarm Management System    : $ams"
   $ECHO "SNMP Proxy Server          : $snmp"
fi
$ECHO "Database Schema            : $dbms"
$ECHO "Setup init.d               : $initd"

$ECHO
enter_value "Do you want to install with the above options (y/n) ? " "" "y n"
if [ $ret = n ] ; then
    $ECHO
    exec sh $CMDPATH/$CMDNAME
fi

case $dbms in
    install) enter_value "Make sure Oracle environment is created and there is no existing database, and then hit enter key " "x"
	     ;;

    schema) enter_value "Make sure the database is RUNNING and all tblspaces have been created, and then hit enter key " "x"
	    ;;

    upgrade) enter_value "Make sure the database is RUNNING, and hit enter key " "x"
	     ;;
esac

$ECHO "Shutting down servlet engine and http server"
stop_servlet

# apply new db schema

case $dbms in
    install) $ECHO "Initializing and installing database schema ..."
	     install_db
	     ;;
    schema) $ECHO "Installing database schema ..."
	     install_only_schema
	     ;;
    upgrade) $ECHO "Upgrading database schema ..."
	     upgrade_db_schema
	     ;;
esac

if [ $hronline = y -o $hradmin = y ] ; then

    $ECHO "Installing new servlet engine and http server"
    install_servlet_engine

    if [ $activation = y ] ; then
	$ECHO "Installing Service Activation Server"
	install_activation
    fi

    if [ $nms = y ] ; then
	$ECHO "Installing Network Management Server"
	install_nms
    fi

    if [ $swupdate = y ] ; then
	$ECHO "Installing Software Update Server"
	install_swupdate
    fi

    if [ $configbackup = y ] ; then
	$ECHO "Installing Configuration Backup Server"
	install_config_backup
    fi

    if [ $support = y ] ; then
	$ECHO "Installing User Support Web Pages"
	install_support_pages
    fi

    if [ $rmsconsole = y ] ; then
	$ECHO "Installing RMS Management Console and Utilities"
	install_rmsconsole
	install_jars
	install_util
    fi
fi

if [ $ams = y -o $snmp = y ] ; then

    # install_jars should have done above

    if [ $ams = y ] ; then
        $ECHO "Installing Alarm Management System"
	install_ams
    fi

    if [ $snmp = y ] ; then
        $ECHO "Installing SNMP Proxy"
	install_snmp
    fi
fi

if [ $initd = y ] ; then

    configure_initd $platform

fi

# ask if modify config file now

if [ $hronline = y -o $hradmin = y -o $ams = y -o $snmp = y ] ; then
    $ECHO "Please modify BroadOn config file below:"
    $ECHO
    for file in ${RMS_HOME}/etc/routefree.conf ; do
	$ECHO "        $file"
    done
    $ECHO
    enter_value "Do you want to modify the configuration file now (y/n) ? " "y" "y n"
    if [ $ret = y ] ; then
	vi ${RMS_HOME}/etc/routefree.conf
    fi
fi

$ECHO "Finished BroadOn Server Installation"
$ECHO
$ECHO "=============================================================="
$ECHO "PLEASE INSTALL VALID CERTIFICATES BEFORE YOU START THE SERVERS"
$ECHO "=============================================================="
$ECHO
$ECHO "Once you install valid certificates, you can start BroadOn Server by:"
$ECHO
$ECHO "  % ${RMS_HOME}/bin/broadon start"
$ECHO
$ECHO "To stop, execute the following"
$ECHO
$ECHO "  % ${RMS_HOME}/bin/broadon stop"
$ECHO


CVSROOT=:ext:build@source:/home/routefree/depot
CVS_RSH=/usr/bin/rsh
CVSREAD=""
JAVA_HOME=/usr/java1.3
export CVSROOT CVS_RSH CVSREAD JAVA_HOME

PATH=$JAVA_HOME/bin:$PATH
export PATH

BUILD_HOME=/u01/build
BUILD_SRC=$BUILD_HOME/src
SERVER_SRC_TOP=$BUILD_SRC/rf/src/server

export BUILD_HOME BUILD_SRC SERVER_SRC_TOP

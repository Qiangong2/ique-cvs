#!/bin/sh

export PACKAGE=rms
export COMPONENT=admin

if [ -z "${BROADON_HOME}" ]; then
    export BROADON_HOME=/opt/broadon/pkgs
fi
if [ -z "${RMS_HOME}" ]; then
    export RMS_HOME=${BROADON_HOME}/${PACKAGE}
fi
if [ -z "${JAVA_HOME}" ]; then
    export JAVA_HOME=${BROADON_HOME}/jre
fi

export PATH=${PATH}:${BROADON_HOME}/jikes/bin
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${RMS_HOME}/lib

export CATALINA_HOME=${BROADON_HOME}/tomcat
export CATALINA_BASE=${RMS_HOME}/servlet-hr${COMPONENT}

#
#  Set options
#
ms=`/sbin/printconf ${PACKAGE}.${COMPONENT}.tomcat.ms`
if [ -n "${ms}" ]; then
    ms="-Xms${ms}"
fi
mx=`/sbin/printconf ${PACKAGE}.${COMPONENT}.tomcat.mx`
if [ -n "${mx}" ]; then
    mx="-Xmx${mx}"
fi
debug=`/sbin/printconf ${PACKAGE}.${COMPONENT}.tomcat.debug`
export CATALINA_OPTS="-Dbuild.compiler.emacs=true ${ms} ${mx} ${debug//\'/}"

#
#  Start up
#
exec ${CATALINA_HOME}/bin/startup.sh

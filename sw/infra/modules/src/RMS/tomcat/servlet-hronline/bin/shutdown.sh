#!/bin/sh

if [ x"${BROADON_HOME}" == "x" ]; then
    export BROADON_HOME=/opt/broadon/pkgs
fi
if [ x"${RMS_HOME}" == "x" ]; then
    export RMS_HOME=${BROADON_HOME}/rms
fi
if [ x"${JAVA_HOME}" == "x" ]; then
    export JAVA_HOME=${BROADON_HOME}/jre
fi
export CATALINA_HOME=${BROADON_HOME}/tomcat
export CATALINA_BASE=${RMS_HOME}/servlet-hronline
export TOMCAT_OPTS=-djava.security.debug=access,failure
exec ${CATALINA_HOME}/bin/shutdown.sh

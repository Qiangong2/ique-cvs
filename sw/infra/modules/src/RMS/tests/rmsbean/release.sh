#!/bin/bash

if [ $# -ne 3 ]; then
    echo usage: `basename $0` http-server db-server db-instance 
    exit 1
fi

SERVER=$1
DB_SERVER=$2
DB_INSTANCE=$3

POST_CMD=../../dist/cmd/post
CERT_DIR=../../dist/cmd

PATH=${PATH}:`dirname $0`
BASENAME=`basename $0`
XSL_DIR=/opt/broadon/pkgs/rms/servlet/webapps/hron_admin/css
SUMMARY_XSL=${XSL_DIR}/releaseSummary.xsl
LOCK_XSL=${XSL_DIR}/lockSummary.xsl
LKG_XSL=${XSL_DIR}/releaseLKG.xsl
NKG_XSL=${XSL_DIR}/releaseNKG.xsl
TMP_DIR=/tmp
IMG_DIR=images-24

RESULT=0

REL_22=2002011400
REL_23=2002022317
REL_241=204002002050709
REL_242=204002002050917
MODEL1=HR/Exp
MODEL2=SME/Wireless

SUMMARY_CMD="make queryReleaseSummary"
LOCK_CMD="make queryLockSummary"
LKG_CMD="make queryLKG" 
NKG_CMD="make queryNKG" 
STARTIR_CMD="make startIR" 
SWITCHIR_CMD="make switchIR" 

SUMMARY_IMG=img-summary
LOCK_IMG=img-lock
LKG22_IMG=img-22lkg
LKG23_IMG=img-23lkg
LKG241_IMG=img-241lkg
LKG242_IMG=img-242lkg
NKG22_IMG=img-22nkg
NKG23_IMG=img-23nkg
NKG241_IMG=img-241nkg
NKG242_IMG=img-242nkg

function compareFile 
{
    echo "comparing $1 $2"
    RES_DIFF=`diff $1 $2 | wc -l`
    if [ $RES_DIFF -eq 0 ]; then
        echo "compare successful"
    fi
    if [ $RES_DIFF -ne 0 ]; then
        echo "*****compare failed*****"
        echo `diff $1 $2` 
        RESULT=1
    fi
}

#query release summary, lock summary, LKG, NKG
function validate
{
    echo "validating release summary"
    $SUMMARY_CMD XSL=${SUMMARY_XSL} | grep -v "java" > ${TMP_DIR}/${SUMMARY_IMG}$1
    compareFile ${IMG_DIR}/$SUMMARY_IMG$1 ${TMP_DIR}/${SUMMARY_IMG}$1

    echo "validating lock summary"
    $LOCK_CMD XSL=${LOCK_XSL} | grep -v "java" > ${TMP_DIR}/$LOCK_IMG$1
    compareFile ${IMG_DIR}/$LOCK_IMG$1 ${TMP_DIR}/$LOCK_IMG$1

    echo "validating LKG view on $REL_22"
    $LKG_CMD RELEASE=$REL_22 XSL=${LKG_XSL} | grep -v "java" > ${TMP_DIR}/$LKG22_IMG$1
    compareFile ${IMG_DIR}/$LKG22_IMG$1 ${TMP_DIR}/$LKG22_IMG$1

    echo "validating LKG view on $REL_23"
    $LKG_CMD RELEASE=$REL_23 XSL=${LKG_XSL} | grep -v "java" > ${TMP_DIR}/$LKG23_IMG$1
    compareFile ${IMG_DIR}/$LKG23_IMG$1 ${TMP_DIR}/$LKG23_IMG$1

    echo "validating LKG view on $REL_241"
    $LKG_CMD RELEASE=$REL_241 XSL=${LKG_XSL} | grep -v "java" > ${TMP_DIR}/$LKG241_IMG$1
    compareFile ${IMG_DIR}/$LKG241_IMG$1 ${TMP_DIR}/$LKG241_IMG$1

    echo "validating LKG view on $REL_242"
    $LKG_CMD RELEASE=$REL_242 XSL=${LKG_XSL} | grep -v "java" > ${TMP_DIR}/$LKG242_IMG$1
    compareFile ${IMG_DIR}/$LKG242_IMG$1 ${TMP_DIR}/$LKG242_IMG$1

    echo "validating NKG view on $REL_22"
    $NKG_CMD RELEASE=$REL_22 XSL=${NKG_XSL} | grep -v "java" > ${TMP_DIR}/$NKG22_IMG$1
    compareFile ${IMG_DIR}/$NKG22_IMG$1 ${TMP_DIR}/$NKG22_IMG$1

    echo "validating NKG view on $REL_23"
    $NKG_CMD RELEASE=$REL_23 XSL=${NKG_XSL} | grep -v "java" > ${TMP_DIR}/$NKG23_IMG$1
    compareFile ${IMG_DIR}/$NKG23_IMG$1 ${TMP_DIR}/$NKG23_IMG$1

    echo "validating NKG view on $REL_241"
    $NKG_CMD RELEASE=$REL_241 XSL=${NKG_XSL} | grep -v "java" > ${TMP_DIR}/$NKG241_IMG$1
    compareFile ${IMG_DIR}/$NKG241_IMG$1 ${TMP_DIR}/$NKG241_IMG$1

    echo "validating NKG view on $REL_242"
    $NKG_CMD RELEASE=$REL_242 XSL=${NKG_XSL} | grep -v "java" > ${TMP_DIR}/$NKG242_IMG$1
    compareFile ${IMG_DIR}/$NKG242_IMG$1 ${TMP_DIR}/$NKG242_IMG$1
}

function regDownload_sysStatus
{
    #start force regDownload
    echo "posting regDownload request for 240 gateways"
    
    i=0;
    for item in `gethrs $DB_SERVER $DB_INSTANCE`
    do
        i=`expr $i + 1`
        if [ `expr $i % 5` -eq 1 ]; then 
            HRID=$item
        fi
        if [ `expr $i % 5` -eq 2 ]; then 
            HW_REV=$item
        fi
        if [ `expr $i % 5` -eq 3 ]; then 
            HW_MODEL=`echo $item | sed -e "s^/^\\\\\\\\/^g"`
        fi
        if [ `expr $i % 5` -eq 4 ]; then 
            RELEASE_REV=$item
        fi
        if [ `expr $i % 5` -eq 0 ]; then 
            IP=$item
            echo "posting regDownload request for HR$HRID"
            REV=`$POST_CMD \
                -ou no-check \
                -url "https://$SERVER/hr_update/entry?mtype=regDownload" \
                -cert $CERT_DIR/sample_cert.der \
                -ca $CERT_DIR/sample_ca.pem \
                -key $CERT_DIR/sample_key.der \
                -password free2route \
                -p "HR_id=HR$HRID/HW_rev=$HW_REV/Release_rev=$RELEASE_REV/HW_model=$HW_MODEL/" | grep "release = " | awk '{ print $3 }'`
            echo "regDownload returned $REV for HR${HRID}"
            
            if [ $REV > 0 ]; then 
                echo "posting sysStatus request for HR${HRID} with running rev=$REV"
                
                $POST_CMD \
                    -ou no-check \
                    -url "https://$SERVER/hr_status/entry?mtype=sysStatus" \
                    -cert $CERT_DIR/sample_cert.der \
                    -ca $CERT_DIR/sample_ca.pem \
                    -key $CERT_DIR/sample_key.der \
                    -password free2route \
                    -p "HR_id=HR$HRID/IP_addr=$IP/HW_rev=$HW_REV/Release_rev=$REV/HW_model=$HW_MODEL/" | grep "HTTP/1.1 200" > /dev/null
                    if [ $? -eq 0 ]; then
                        echo "sysStatus request for HR${HRID} with rev=$REV successful"
                    fi
                    if [ $? -ne 0 ]; then
                        echo "sysStatus request for HR${HRID} with rev=$REV failed"
                        echo "sysStatus request for HR${HRID} with rev=$REV failed" >> $ERR_LOG
                    RESULT=1
                fi
            fi
        fi
    done
}

rsh -l root panther "su - oracle cmd -s ACER_PANTHER -u HRON 'execute rollit;'"

#start Phase1 2.4.1
echo "starting Release2.4.1 Phase1"
$STARTIR_CMD RELEASE=$REL_241 MODEL=$MODEL1 PHASE=1
$STARTIR_CMD RELEASE=$REL_241 MODEL=$MODEL2 PHASE=1
validate 1
regDownload_sysStatus
validate 2

#start Phase2 2.4.1
echo "starting Release2.4.1 Phase2"
$STARTIR_CMD RELEASE=$REL_241 MODEL=$MODEL1 PHASE=2
$STARTIR_CMD RELEASE=$REL_241 MODEL=$MODEL2 PHASE=2
validate 3
regDownload_sysStatus
validate 4

#switch to 2.4.2
echo "switching IR to Relese2.4.2"
$SWITCHIR_CMD RELEASE=$REL_242 MODEL=$MODEL1
$SWITCHIR_CMD RELEASE=$REL_242 MODEL=$MODEL2
validate 5
regDownload_sysStatus
validate 6

#rollback to LKG 2.3
echo "rolling back to LKG Release2.3"
$STARTIR_CMD RELEASE=$REL_242 MODEL=$MODEL1 PHASE=-1
$STARTIR_CMD RELEASE=$REL_242 MODEL=$MODEL2 PHASE=-1
validate 7
regDownload_sysStatus
validate 8

#start Phase1 2.4.1
echo "starting Release2.4.1 Phase1"
$STARTIR_CMD RELEASE=$REL_241 MODEL=$MODEL1 PHASE=1
$STARTIR_CMD RELEASE=$REL_241 MODEL=$MODEL2 PHASE=1
validate 9
regDownload_sysStatus
validate 10

#make Release2.4.1 LKG
echo "making Release2.4.1 LKG"
$STARTIR_CMD RELEASE=$REL_241 MODEL=$MODEL1 PHASE=3
$STARTIR_CMD RELEASE=$REL_241 MODEL=$MODEL2 PHASE=3
validate 11
regDownload_sysStatus
validate 12

/bin/rm -rf $TMP_DIR/img-*

if [ $RESULT -eq 0 ]; then 
    echo "$BASENAME: SUCCEEDED"
fi
if [ $RESULT -ne 0 ]; then 
    echo "$BASENAME: FAILED"
fi

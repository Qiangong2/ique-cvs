package tests.rmsbean;

import lib.Logger;
import hronline.beans.admin.*;
import tests.rmsbean.testTool;

/*
setStatus <HRID> <STATUS>
*/
public class setStatus extends testTool 
{
    public static void main(String[] args)
    {
        Logger mLogger = null;
        try 
        {
            mLogger = init();

            mLogger.debug("setStatus:", args[0]);
            GatewayDetailBean gdb = new GatewayDetailBean(mLogger, args[0]);
            int ret = gdb.setStatus(args[1]);
            mLogger.debug("setStatus:", Integer.toString(ret));
           
            if (ret==1)
            {
                System.out.println("0");
                System.exit(0); 
            } else {
                System.out.println("1");
                System.exit(1);
            }

        } catch (java.sql.SQLException se) {
            System.out.println(se+"\n");
            if (mLogger!=null) mLogger.error("setStatus:", se.toString());
        } catch (Exception e) {
            System.out.print(e+"\n");
            e.printStackTrace();
            if (mLogger!=null) mLogger.error("setStatus:", e.toString());
        }
    }
}

package tests.rmsbean;

import java.math.BigDecimal;

import lib.Logger;
import hronline.beans.admin.*;
import tests.rmsbean.testTool;

/*
lockRelease <HRID> <RELEASE_VERSION>
*/
public class lockRelease extends testTool 
{
    public static void main(String[] args)
    {
        Logger mLogger = null;
        try 
        {
            mLogger = init();

            mLogger.debug("lockRelease:", args[0]);
            GatewayDetailBean gdb = new GatewayDetailBean(mLogger, args[0]);
            gdb.lockRelease(new BigDecimal(args[1]));

            System.out.println("0"); 
            System.exit(0);

        } catch (java.sql.SQLException se) {
            System.out.println(se+"\n");
            if (mLogger!=null) mLogger.error("lockRelease:", se.toString());
            System.exit(1);
        } catch (Exception e) {
            System.out.print(e+"\n");
            e.printStackTrace();
            if (mLogger!=null) mLogger.error("lockRelease:", e.toString());
            System.exit(1);
        }
    }
}

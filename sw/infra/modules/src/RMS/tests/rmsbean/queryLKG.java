package tests.rmsbean;

import java.math.BigDecimal;

import lib.Logger;
import hronline.xml.*;
import hronline.beans.admin.*;
import tests.rmsbean.testTool;

/*
queryLKG <RELEASE_REV>
*/
public class queryLKG extends testTool 
{
    public static void main(String[] args)
    {
        Logger mLogger = null;
        try 
        {
            mLogger = init();

            mLogger.debug("queryLKG:", args[0]);
            IncReleaseBean ib = new IncReleaseBean(mLogger);
            String xml = ib.queryLKG(args[0]);
            String xsl = readFile(args[1]);
            String html = ib.formatResultByXSL(xml, xsl);

            System.out.println(html); 
            System.exit(0); 

        } catch (java.sql.SQLException se) {
            System.out.println(se+"\n");
            if (mLogger!=null) mLogger.error("queryLKG:", se.toString());
            System.exit(1); 
        } catch (Exception e) {
            System.out.print(e+"\n");
            e.printStackTrace();
            if (mLogger!=null) mLogger.error("queryLKG:", e.toString());
            System.exit(1); 
        }
    }
}

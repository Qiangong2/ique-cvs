package tests.rmsbean;

import java.math.BigDecimal;

import lib.Logger;
import hronline.beans.admin.*;
import tests.rmsbean.testTool;

/*
unlockRelease <HRID>
*/
public class unlockRelease extends testTool 
{
    public static void main(String[] args)
    {
        Logger mLogger = null;
        try 
        {
            mLogger = init();

            mLogger.debug("unlockRelease:", args[0]);
            GatewayDetailBean gdb = new GatewayDetailBean(mLogger, args[0]);
            gdb.unlockRelease();

            System.out.println("0"); 
            System.exit(0);

        } catch (java.sql.SQLException se) {
            System.out.println(se+"\n");
            if (mLogger!=null) mLogger.error("unlockRelease:", se.toString());
            System.exit(1);
        } catch (Exception e) {
            System.out.print(e+"\n");
            e.printStackTrace();
            if (mLogger!=null) mLogger.error("unlockRelease:", e.toString());
            System.exit(1);
        }
    }
}

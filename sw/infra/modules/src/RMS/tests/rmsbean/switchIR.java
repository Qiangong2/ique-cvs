package tests.rmsbean;

import java.math.BigDecimal;

import lib.Logger;
import hronline.beans.admin.*;
import tests.rmsbean.testTool;

/*
switchIR <RELEASE_REV> <HR_MODEL>
*/
public class switchIR extends testTool 
{
    public static void main(String[] args)
    {
        Logger mLogger = null;
        try 
        {
            mLogger = init();

            mLogger.debug("switchIR:", args[0]);
            IncReleaseBean ib = new IncReleaseBean(mLogger);
            ib.switchRelease(args[1], args[0]);

            System.out.println("0"); 
            System.exit(0); 

        } catch (java.sql.SQLException se) {
            System.out.println(se+"\n");
            if (mLogger!=null) mLogger.error("switchIR:", se.toString());
            System.exit(1); 
        } catch (Exception e) {
            System.out.print(e+"\n");
            e.printStackTrace();
            if (mLogger!=null) mLogger.error("switchIR:", e.toString());
            System.exit(1); 
        }
    }
}

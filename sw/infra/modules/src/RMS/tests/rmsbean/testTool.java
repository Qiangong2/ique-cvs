package tests.rmsbean;

import java.util.*;
import java.io.*;
import java.lang.reflect.Field;

import lib.Config;
import lib.Logger;

public class testTool 
{
    public static Logger init()
    {
        Logger mLogger = null;
        try
        {
            // load properties from the file specified
            //
            Properties mAdminProp = new Properties();
            FileInputStream in = new FileInputStream("test.conf");
            mAdminProp.load(in);
            in.close();

            //
            // use Config class to get properties from RouteFree.properties in
            // a class path, and /etc/routefree.conf file
            //
            Config.loadInto(mAdminProp);

            String logFileName = mAdminProp.getProperty("ErrorLog");
            // create the file if not exist
            File ff = new File(logFileName);
            if (!ff.exists()) {
                String dir = ff.getParent();
                if (dir != null) {
                File d = new File(dir);
                if (!d.exists()) {
                    d.mkdir();
                }
                }
                ff.createNewFile();
            }

            mLogger = new Logger(new BufferedOutputStream(new FileOutputStream(logFileName, true)));
            Field f = Logger.class.getField(mAdminProp.getProperty("ErrLogLevel"))
;
            mLogger.setLoglevel(f.getInt(mLogger));

            return mLogger;
        } catch (Exception e) {
            System.out.print(e+"\n");
            e.printStackTrace();
            if (mLogger!=null) mLogger.error("setStatus:", e.toString());
            return null;
        }
    }

    public static String readFile(String filename) 
        throws IOException 
    {
        String lineSep = System.getProperty("line.separator");
        BufferedReader br = new BufferedReader(new FileReader(filename));
        String nextLine = "";
        StringBuffer sb = new StringBuffer();
        while ((nextLine = br.readLine()) != null) {
            sb.append(nextLine);
            //
            // note:
            //   BufferedReader strips the EOL character.
            //
            sb.append(lineSep);
        }
        return sb.toString();
    }
}

package tests.rmsbean;

import java.math.BigDecimal;

import lib.Logger;
import hronline.beans.admin.*;
import tests.rmsbean.testTool;

/*
startIR <RELEASE_REV> <HR_MODEL> <PHASE_NO>
1: Phase 1
2: Phase 2
3: make LKG
-1: cancel
*/
public class startIR extends testTool 
{
    public static void main(String[] args)
    {
        Logger mLogger = null;
        try 
        {
            mLogger = init();

            mLogger.debug("startIR:", args[0]);
            IncReleaseBean ib = new IncReleaseBean(mLogger);
            if (args[2]!=null && args[2].equals("1"))
                ib.startP1(args[1], args[0]);
            else if (args[2]!=null && args[2].equals("2"))
                ib.startP2(args[1], args[0]);
            else if (args[2]!=null && args[2].equals("3"))
                ib.complete(args[1], args[0]);
            else if (args[2]!=null && args[2].equals("-1"))
                ib.cancel(args[1], args[0]);

            System.out.println("0"); 
            System.exit(0);

        } catch (java.sql.SQLException se) {
            System.out.println(se+"\n");
            if (mLogger!=null) mLogger.error("startIR:", se.toString());
            System.exit(1);
        } catch (Exception e) {
            System.out.print(e+"\n");
            e.printStackTrace();
            if (mLogger!=null) mLogger.error("startIR:", e.toString());
            System.exit(1);
        }
    }
}

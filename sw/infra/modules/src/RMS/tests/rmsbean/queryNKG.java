package tests.rmsbean;

import java.math.BigDecimal;

import lib.Logger;
import hronline.xml.*;
import hronline.beans.admin.*;
import tests.rmsbean.testTool;

/*
queryNKG <RELEASE_REV>
*/
public class queryNKG extends testTool 
{
    public static void main(String[] args)
    {
        Logger mLogger = null;
        try 
        {
            mLogger = init();

            mLogger.debug("queryNKG:", args[0]);
            IncReleaseBean ib = new IncReleaseBean(mLogger);
            String xml = ib.queryNKG(args[0]);
            String xsl = readFile(args[1]);
            String mod_xsl = XslTransformer.setStyleSheetParam(xsl, "role", "99");
            String html = ib.formatResultByXSL(xml, mod_xsl);

            System.out.println(html); 
            System.exit(0); 

        } catch (java.sql.SQLException se) {
            System.out.println(se+"\n");
            if (mLogger!=null) mLogger.error("queryNKG:", se.toString());
            System.exit(1); 
        } catch (Exception e) {
            System.out.print(e+"\n");
            e.printStackTrace();
            if (mLogger!=null) mLogger.error("queryNKG:", e.toString());
            System.exit(1); 
        }
    }
}

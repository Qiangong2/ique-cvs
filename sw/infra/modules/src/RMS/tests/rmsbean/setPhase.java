package tests.rmsbean;

import lib.Logger;
import hronline.beans.admin.*;
import tests.rmsbean.testTool;

/*
setPhase <HRID> <PHASE_NO>
*/
public class setPhase extends testTool 
{
    public static void main(String[] args)
    {
        Logger mLogger = null;
        try 
        {
            mLogger = init();

            mLogger.debug("setPhase:", args[0]);
            GatewayDetailBean gdb = new GatewayDetailBean(mLogger, args[0]);
            gdb.setIRPhase(Integer.parseInt(args[1]));

            System.out.println("0"); 
            System.exit(0);

        } catch (java.sql.SQLException se) {
            System.out.println(se+"\n");
            if (mLogger!=null) mLogger.error("setPhase:", se.toString());
            System.exit(1);
        } catch (Exception e) {
            System.out.print(e+"\n");
            e.printStackTrace();
            if (mLogger!=null) mLogger.error("setPhase:", e.toString());
            System.exit(1);
        }
    }
}

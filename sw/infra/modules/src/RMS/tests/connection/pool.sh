#!/bin/sh
#
# $1 test name (run|examine|cleanup)

TEST_NAME=${1:-run}
if [ $# -gt 0 ] ; then
    shift
fi


RUNLOGFILE=/tmp/runlog.txt
RUNERRFILE=/tmp/runerr.txt
TMP_RESULT=/tmp/result.txt

DIST_LIB=../../dist/lib

CLASSPATH=$DIST_LIB/ServerLib.jar:$DIST_LIB/hron_engine.jar:$DIST_LIB/db.jar:classes

EXEC_CMD="java -classpath $CLASSPATH test.RunQuery"

case $TEST_NAME in
    run)
	$EXEC_CMD $* > $RUNLOGFILE 2> $RUNERRFILE
	if [ -s $RUNERRFILE ] ; then
	    echo "FAILED: execution of test failed"
	    cat $RUNERRFILE
	else
	    echo "PASSED: alarm test is successuly executed"
	fi
	;;

    examine)
	;;

    cleanup)
	/bin/rm -f $RUNLOGFILE $RUNERRFILE $TMP_RESULT
	echo "PASSED: cleaned up test"
	;;
esac


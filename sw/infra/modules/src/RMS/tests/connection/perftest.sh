#!/bin/sh
#
# read DB params from rf/src/server/tests/scripts/db.conf

DBCONF=`dirname $0`/../scripts/db.conf
. $DBCONF

TEST_OPTIONS="-longsql 10 -shortsql 10 -run 10 -min 2 -max 5 -idletime 30 -interval 45 -verbose -log $TMP_RESULT -pool"

# arg 1 .. log file
append_logs()
{
    log=$1
    echo "==================== runlog.txt ==========================" >> $log
    cat /tmp/runlog.txt >> $log
    echo "==================== runerr.txt ==========================" >> $log
    cat /tmp/runerr.txt >> $log
    echo "==========================================================" >> $log
}

# arg 1 .. # of threads for longsql
# arg 2 .. # of threads for shortsql
# arg 3 .. # of run per thread
# arg 4 .. # of connections
run_all()
{
    sh pool.sh run -longsql $1 -shortsql $2 \
	    -run $3 -max $4 -url $DB_url -log dbmgr.log -dbmgr
    append_logs dbmgr.log

    sh pool.sh run -longsql $1 -shortsql $2 \
	    -run $3 -max $4 -url $DB_url -log pool.log -pool
    append_logs pool.log

    sh pool.sh run -longsql $1 -shortsql $2 \
	    -run $3 -max $4 -url $DB_url -log jdbc.log -jdbc
    append_logs jdbc.log
}

# within # of Connections
run_all 1 5   50   10
run_all 1 5   100  10
run_all 1 5   500  10
run_all 1 5   1000 10

# more threads than # of Connections
run_all 2 10  50   5
run_all 2 10  100  5
run_all 2 10  500  5
run_all 2 10  1000 5

# lots of threads than # of Connections
run_all 100 500  5   5
run_all 100 500  10  5
run_all 100 500  50  5
run_all 100 500  100 5


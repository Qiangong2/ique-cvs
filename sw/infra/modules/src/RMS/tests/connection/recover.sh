#!/bin/sh
#
# $1 test name (run|examine|cleanup)
# $2 Database URL
#
# stopDB/startDB are located under rf/src/server/tests/tools/db/.
# and this test assumes that stopDB & startDB should not take
# more than 180 seconds
#

TOOLDIR=`dirname $0`/../tools/db

. `dirname $0`/common.sh 

# read DB params from rf/src/server/tests/scripts/db.conf

DBCONF=`dirname $0`/../scripts/db.conf
. $DBCONF

# set all db params
set_dbparams ${2:-${DB_url}}

TEST_OPTIONS="-longsql 10 -shortsql 10 -run 10 -min 2 -max 5 -idletime 600 -interval 210 -verbose -log $TMP_RESULT -pool"

TEST_NAME=${1:-run}
case $TEST_NAME in

    run)
	sh $TOOLDIR/startDB $DBHOST $DBNAME
	execute_test_async $TEST_OPTIONS
	sleep 30 # wait till short-sql-test finishes
	sh $TOOLDIR/stopDB $DBHOST $DBNAME
	sh $TOOLDIR/startDB $DBHOST $DBNAME
	examine_execution $PID
	;;

    examine)
	examine_result 10 200 200 0 5 10 1 1 0
	;;

    cleanup)
	clean_up
	;;
esac


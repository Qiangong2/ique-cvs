#!/bin/sh
#
# test library
#

# default values

DBURL=""

RUNLOGFILE=/tmp/runlog.txt
RUNERRFILE=/tmp/runerr.txt
TMP_RESULT=/tmp/result.txt

SCRIPTDIR=../tools/db

DIST_LIB=../../dist/lib

CLASSPATH=$DIST_LIB/ServerLib.jar:$DIST_LIB/hron_engine.jar:$DIST_LIB/db.jar:classes

EXEC_CMD="java -classpath $CLASSPATH test.RunQuery"

# arg1 .. url
set_dbparams()
{
    DBURL=$1
    if [ "x`echo $DBURL | grep sid=`" != x ] ; then
	DBNAME=`echo $DBURL | sed -e "s/.*sid=//" -e "s/).*//"`
    else
	DBNAME=`echo $DBURL | sed -e "s/.*://"`
    fi
    DBHOST=`echo $DBURL | sed -e "s/.*@//" -e "s/:.*//" -e "s/.*host=//" -e "s/).*//"`
}

# arg1 .. label
# arg2 .. result
# arg3 .. expected
# return 0 for fail, 1 for success
compare()
{
    echo "$1: $2"
    if [ $2 -ne $3 ] ; then
	echo "FAIL: does not match to expected value: $3"
	return 0
    fi
    return 1
}

# env variables need to be overwritten:
#
#   DBURL
#
# args
#
#   test options for RunQuery
# 
execute_test()
{
    /bin/rm -f $TMP_RESULT
    $EXEC_CMD -url $DBURL $* > $RUNLOGFILE 2> $RUNERRFILE
    if [ -s $RUNERRFILE ] ; then
	echo "FAILED: execution of test failed"
	cat $RUNERRFILE
    else
	echo "PASSED: interval test is successuly executed"
    fi
}

# env variables need to be overwritten:
#
#   DBURL
#
# args
#
#   test options for RunQuery
# 
# return
#
#   PID env var (return value cannot handle big numbers)
#
execute_test_async()
{
    /bin/rm -f $TMP_RESULT
    $EXEC_CMD -url $DBURL $* > $RUNLOGFILE 2> $RUNERRFILE &
    PID=$!
}

# arg1 process id
check_pid()
{
    FLAG=`ps -e | grep "^[ ]*$1 "`
    if [ "x$FLAG" = x ] ; then
	return 0
    else
	return 1
    fi
}

# wait for pid specified to finish
wait_for_test()
{
    while true ; do
	check_pid $1
	if  [ $? -ne 1 ] ; then
	    return
	fi
	sleep 5
    done
}


# check execution result
#
# args
#
#   arg1 .. pid returned from execute_test_async()
#
examine_execution()
{
    wait_for_test $1

    if [ -s $RUNERRFILE ] ; then
	echo "FAILED: execution of the test failed"
	cat $RUNERRFILE
    else
	echo "PASSED: execution of the test successful"
    fi
}


# env variables need to be overwritten:
#
#   DBURL
#
# args
#
#   arg1 .. # of created
#   arg2 .. # of acquired
#   arg3 .. # of released
#   arg4 .. # of idled
#   arg5 .. # of closed() callback calls (due to execution failure or idled)
#   arg6 .. # of close()  api calls (actual close calls)
#   arg7 .. # of failures (Exception on Connection interface calls or validity check)
#   arg8 .. # of failures (Exception on pool.getConnection api )
#   arg9 .. # of db down  (db down detection count)
#
examine_result()
{
    stopped=`grep -i "ncomplete =" $RUNLOGFILE $RUNERRFILE $TMP_RESULT`
    if [ "x$stopped" != x ] ; then
	echo "FAILED: getConnection() returned null"
	return
    fi

    exception=`grep -i exception $RUNLOGFILE $RUNERRFILE`
    if [ "x$exception" != x ] ; then
	echo "FAILED: Exception occured"
	return
    fi

    n_created=`grep :created $TMP_RESULT | wc -l`
    n_acquired=`grep :acquired $TMP_RESULT | wc -l`
    n_released=`grep :released $TMP_RESULT | wc -l`
    n_idled=`grep :idled $TMP_RESULT | wc -l`
    n_closed1=`grep :closed $TMP_RESULT | wc -l`
    n_closed2=`grep "closed()" $TMP_RESULT | wc -l`
    n_failed1=`grep :failed $TMP_RESULT | wc -l`
    n_failed2=`grep ":Connection failure detected:" $TMP_RESULT | wc -l`
    n_failed3=`grep "Database connection is down:" $TMP_RESULT | wc -l`

    flag=1
    compare "created " $n_created $1
    if [ $? -ne 1 ] ; then flag=0; fi
    compare "acquired" $n_acquired $2
    if [ $? -ne 1 ] ; then flag=0; fi
    compare "released" $n_released $3
    if [ $? -ne 1 ] ; then flag=0; fi
    compare "idled   " $n_idled $4
    if [ $? -ne 1 ] ; then flag=0; fi
    compare "closed1 " $n_closed1 $5
    if [ $? -ne 1 ] ; then flag=0; fi
    compare "closed2 " $n_closed2 $6
    if [ $? -ne 1 ] ; then flag=0; fi
    compare "failed1 " $n_failed1 $7
    if [ $? -ne 1 ] ; then flag=0; fi
    compare "failed2 " $n_failed2 $8
    if [ $? -ne 1 ] ; then flag=0; fi
    compare "failed3 " $n_failed3 $9
    if [ $? -ne 1 ] ; then flag=0; fi

    if [ $flag -ne 1 ] ; then
	echo "FAILED: some result does not match to expected value"
    else
	echo "PASSED: result matches to expected values"
    fi
}

clean_up()
{
    /bin/rm -f $RUNLOGFILE $RUNERRFILE $TMP_RESULT
    echo "PASSED: cleaned up"
}



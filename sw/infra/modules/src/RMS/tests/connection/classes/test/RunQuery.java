package test;

import java.sql.*;
import java.io.*;
import java.util.*;
import oracle.sql.*;
import oracle.jdbc.driver.*;

import lib.Config;
import lib.Logger;

import hronline.engine.*;
import hronline.engine.connection.*;

/**
 * Base class for all database managers used by HRSM_backend class
 * Handle all interface with the database.
 */
public class RunQuery
{
    public static final int JDBC = 1;
    public static final int POOL = 2;
    public static final int DBMGR = 3;
    public static final int JDBCPOOL = 4;

    private int	method;
    private Connection myconn;
    private ConnectionFactory factory;
    private ConnectionPool pool;
    private DatabaseMgrWrapper dbmgr;

    private Logger logger;

    private String mDB_url;
    private String mDB_user;
    private String mDB_pw;

    private static final String LONG_QUERY = 
	"select PUBLIC_NET_IP from HR_SYSTEM_CONFIGURATIONS";

    private static final String SHORT_QUERY = 
	"select PUBLIC_NET_IP from HR_SYSTEM_CONFIGURATIONS where HR_ID=1";

    private RunQuery(
		int method, int min_c, int max_c, int idletime,
		String log_file, String url)
	throws ClassNotFoundException, java.io.FileNotFoundException
    {
	Properties prop = Config.getProperties();
	if (url == null) {
	    mDB_url = prop.getProperty("DB_url");
	} else {
	    mDB_url = url;
	}
	mDB_user = prop.getProperty("DB_user");
	if (mDB_user == null) {
	    mDB_user = Config.DEFAULT_DB_USER;
	}
	mDB_pw = prop.getProperty("DB_pw");
	if (mDB_pw == null) {
	    mDB_pw = Config.DEFAULT_DB_PASSWD;
	}

	String clazz = prop.getProperty("DB_class");
	if (clazz == null) {
	    clazz = Config.DEFAULT_DB_CLASS;
	}
	Class.forName(clazz);

	logger = new Logger(log_file);

	this.method = method;

	switch (method) {
	    case POOL:
		String urls[] = new String[1];
		String users[] = new String[1];
		String pws[] = new String[1];
		urls[0] = mDB_url;
		users[0] = mDB_user;
		pws[0] = mDB_pw;
		factory = new ConnectionFactory(
				urls, users, pws, clazz, logger);
		pool = new ConnectionPool(
			    factory, min_c, max_c, idletime, logger);
		break;

	    case DBMGR:
	    case JDBCPOOL:
		prop.setProperty("DB_num_connections", Integer.toString(max_c));
		prop.setProperty("DB_url", mDB_url);
		dbmgr = new DatabaseMgrWrapper(prop, logger);
		break;
	}
    }

    public void initialize() throws SQLException
    {
	switch (method) {
	    case JDBC:
		myconn = DriverManager.getConnection(mDB_url, mDB_user, mDB_pw);
		break;
	    case DBMGR:
	    case JDBCPOOL:
		dbmgr.init();
		break;
	}
    }

    public void shutdown() throws SQLException
    {
	switch (method) {
	    case POOL:
		pool.shutdown();
		break;
	    case DBMGR:
	    case JDBCPOOL:
		dbmgr.fini();
		break;
	}
    }

    private Connection getConnection() throws SQLException
    {
	switch(method) {
	    case JDBC:  return getFromJDBC();
	    case POOL:  return getFromPool();
	    case DBMGR: return getFromDatabaseMgr();
	    case JDBCPOOL: return getFromDatabaseMgr();
	}
	throw new SQLException("Unknown connection acquire method");
    }

    private Connection getFromJDBC() throws SQLException
    {
	return myconn;
    }

    private Connection getFromPool() throws SQLException
    {
	while (true) {
	    try {
		return pool.getConnection();

	    } catch (InterruptedException e) { }
	}
    }

    private Connection getFromDatabaseMgr() throws SQLException
    {
	return dbmgr.getMyConnection();
    }

    private long query(Connection con, String sql) throws SQLException
    {
	long st_time = System.currentTimeMillis();

	if (method == JDBC || method == JDBCPOOL) {
	    queryNoSync(con, sql);
	} else {
	    querySync(con, sql);
	}

	return ( System.currentTimeMillis() - st_time );
    }

    // for DatabaseMgr & ConnectionPool
    private void querySync(Connection con, String sql) throws SQLException
    {
	PreparedStatement stmt = null;
	ResultSet rs = null;
	synchronized(con) {
	    try {
		stmt = con.prepareStatement(sql);
		stmt.clearParameters();
		rs = stmt.executeQuery();
		int i=0;
		while (rs.next()) {
		    i++;
		    String ipa = rs.getString(1);
		}
	    } finally {
		try { rs.close(); } catch (Throwable e) { }
		try { stmt.close(); } catch (Throwable e) { }
	    }
	}
    }

    // used only for JDBC raw
    private void queryNoSync(Connection con, String sql) throws SQLException
    {
	PreparedStatement stmt = null;
	ResultSet rs = null;
	try {
	    stmt = con.prepareStatement(sql);
	    stmt.clearParameters();
	    rs = stmt.executeQuery();
	    int i=0;
	    while (rs.next()) {
		i++;
		String ipa = rs.getString(1);
	    }
	} finally {
	    try { rs.close(); } catch (Throwable e) { }
	    try { stmt.close(); } catch (Throwable e) { }
	}
    }

    private static void usage()
    {
	System.out.println("-min <n> -max <n> -run <n> -log <fname> -longsql <n> -shortsql <n> -delay <n> -url <dburl> -interval <n> -idletime <n> -verbose -jdbc|jdbcpool|pool|dbmgr");
    }

    public static void main(String[] args)
    {
	try {
	    int minc = 2, maxc = 5, nrun = 100, idletime = 60;
	    String logfile = "test.log";
	    int long_sql = 1, short_sql = 1, method = JDBC;
	    long delay = 5000, interval = -1;
	    boolean verbose = false;
	    String dburl = null;

	    for (int i=0; i<args.length; i++) {
		if (args[i].equals("-min")) {
		    minc = Integer.parseInt(args[++i]);

		} else if (args[i].equals("-max")) {
		    maxc = Integer.parseInt(args[++i]);

		} else if (args[i].equals("-run")) {
		    nrun = Integer.parseInt(args[++i]);

		} else if (args[i].equals("-log")) {
		    logfile = args[++i];

		} else if (args[i].equals("-longsql")) {
		    long_sql = Integer.parseInt(args[++i]);

		} else if (args[i].equals("-idletime")) { // connection idle time
		    idletime = Integer.parseInt(args[++i]);

		} else if (args[i].equals("-interval")) { // between short/long
		    interval = Long.parseLong(args[++i]) * 1000L;

		} else if (args[i].equals("-shortsql")) {
		    short_sql = Integer.parseInt(args[++i]);

		} else if (args[i].equals("-delay")) { // before start the test
		    delay = Long.parseLong(args[++i]) * 1000L;

		} else if (args[i].equals("-jdbc")) {
		    method = JDBC;

		} else if (args[i].equals("-pool")) {
		    method = POOL;

		} else if (args[i].equals("-dbmgr")) {
		    method = DBMGR;

		} else if (args[i].equals("-jdbcpool")) {
		    method = JDBCPOOL;

		} else if (args[i].equals("-url")) {
		    dburl = args[++i];

		} else if (args[i].equals("-verbose")) {
		    verbose = true;

		} else {
		    System.out.println("Invalid argument: " + args[i]);
		    usage();
		    return;
		}
	    }

	    RunQuery rq = new RunQuery(method, minc, maxc, idletime, logfile, dburl);
	    if (verbose) {
		rq.logger.setLoglevel(Logger.DEBUG);
	    } else {
		rq.logger.setLoglevel(Logger.WARNING);
	    }

	    Vector long_threads = new Vector();
	    for (int i=0; i< long_sql; i++) {
		long_threads.addElement(
			new RunQuery.ExecutionThread(rq, LONG_QUERY, nrun));
	    }
	    Vector short_threads = new Vector();
	    for (int i=0; i< short_sql; i++) {
		short_threads.addElement(
			new RunQuery.ExecutionThread(rq, SHORT_QUERY, nrun));
	    }

	    rq.logger.log("Setting up the test");
	    rq.logger.log("    Connection acquire method:   " + rq.methodString(method));
	    rq.logger.log("    Min # of Connections:        " + minc);
	    rq.logger.log("    Max # of Connections:        " + maxc);
	    rq.logger.log("    # of threads for long sql  : " + long_threads.size());
	    rq.logger.log("    # of threads for short sql : " + short_threads.size());
	    rq.logger.log("    # of sql execution/thread:   " + nrun);
	    if (interval >= 0) {
		rq.logger.log("    Time gap between long/short: " + interval);
		rq.logger.log("    (short runs first, then long)");
	    } else {
		rq.logger.log("    Mixed mode execution of both sql");
	    }

	    // initialize
	    rq.initialize();

	    // wait for 1 min to settle all theads
	    synchronized(rq) {
		try { rq.wait(delay); } catch (Exception e) { }
	    }

	    // start!
	    if (interval < 0) {

		// mixed mode
		rq.startAll(long_threads);
		rq.startAll(short_threads);
		rq.logger.log("Mixed mode test started");

		// wait
		rq.waitAll(short_threads);
		rq.waitAll(long_threads);
		rq.logger.log("Test finished");

	    } else {

		// independent mode
		rq.startAll(short_threads);
		rq.logger.log("Short SQL test started");
		rq.waitAll(short_threads);
		rq.logger.log("Short SQL test finished");

		rq.logger.log("Pending for " + interval / 1000 + " secs");
		synchronized(rq) {
		    try { rq.wait(interval); } catch(Exception e) { }
		}

		rq.startAll(long_threads);
		rq.logger.log("Long SQL test started");
		rq.waitAll(long_threads);
		rq.logger.log("Long SQL test finished");
	    }
	    
	    // results
	    rq.logResult("Short SQL Statement (" + SHORT_QUERY + ")", short_threads);
	    rq.logResult("Long SQL Statement (" + LONG_QUERY + ")", long_threads);

	    // finish
	    rq.shutdown();

	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    private String methodString(int method)
    {
	switch (method) {
	    case JDBC: return "Raw JDBC";
	    case JDBCPOOL: return "Raw JDBC with multiple connections";
	    case POOL: return "ConnectionPool object";
	    case DBMGR: return "DatabaseMgr object";
	}
	return "Unknown";
    }

    private void startAll(Vector v)
    {
	Enumeration e = v.elements();
	while (e.hasMoreElements()) {
	    ((Thread)e.nextElement()).start();
	}
    }

    private void waitAll(Vector v)
    {
	Enumeration e = v.elements();
	while (e.hasMoreElements()) {
	    boolean joined = false;
	    while (!joined) {
		try {
		    ((Thread)e.nextElement()).join();
		    joined = true;
		} catch (Exception ex) { }
	    }
	}
    }

    private void logResult(String title, Vector v)
    {
	logger.log("");
	logger.log("Result for " + title);
	long total_con=0, max_con=Long.MIN_VALUE, min_con=Long.MAX_VALUE;
	long total_exe=0, max_exe=Long.MIN_VALUE, min_exe=Long.MAX_VALUE;
	long total_all=0, max_all=Long.MIN_VALUE, min_all=Long.MAX_VALUE;
	long n_samples = 0;

	Enumeration e = v.elements();
	while (e.hasMoreElements()) {

	    ExecutionThread et = (ExecutionThread)e.nextElement();

	    n_samples += et.getRunCount();

	    long t_exe = et.getExecTime();
	    min_exe = Math.min(t_exe, min_exe);
	    max_exe = Math.max(t_exe, max_exe);
	    total_exe += t_exe;

	    long t_all = et.getAllTime();
	    min_all = Math.min(t_all, min_all);
	    max_all = Math.max(t_all, max_all);
	    total_all += t_all;

	    long t_con = t_all - t_exe;
	    min_con = Math.min(t_con, min_con);
	    max_con = Math.max(t_con, max_con);
	    total_con += t_con;

	}
	logger.log("   Samples: " + v.size() * n_samples);
	logger.log("");
	logger.log("   All process (get Connection & execute SQL)");
	logger.log("       Total(sec): " + formTime(total_all));
	logger.log("       Min(sec):   " + formTime(min_all));
	logger.log("       Max(sec):   " + formTime(max_all));
	logger.log("       Ave(sec):   " + formTime(total_all / v.size()));
	logger.log("");
	logger.log("   Only Connection acquision");
	logger.log("       Total(sec): " + formTime(total_con));
	logger.log("       Min(sec):   " + formTime(min_con));
	logger.log("       Max(sec):   " + formTime(max_con));
	logger.log("       Ave(sec):   " + formTime(total_con / v.size()));
	logger.log("");
	logger.log("   Only SQL execution");
	logger.log("       Total(sec): " + formTime(total_exe));
	logger.log("       Min(sec):   " + formTime(min_exe));
	logger.log("       Max(sec):   " + formTime(max_exe));
	logger.log("       Ave(sec):   " + formTime(total_exe / v.size()));
    }

    private String formTime(long t)
    {
	String msec_s = Long.toString(t % 1000L);
	return Long.toString(t / 1000L)
		    + "."
		    + "000".substring(0, 3- msec_s.length())
		    + msec_s;
    }

    private static class ExecutionThread extends Thread
    {
	RunQuery rq;
	private String sql;
	private int nrun, ncomplete;
	private long exec_sum, all_sum;

	public ExecutionThread(RunQuery rq, String sql, int nrun)
	{
	    this.rq = rq;
	    this.sql = sql;
	    this.nrun = nrun;
	}

	public long getExecTime() { return exec_sum; }

	public long getAllTime() { return all_sum; }

	public long getRunCount() { return ncomplete; }

	public void run()
	{
	    exec_sum = 0L;
	    all_sum = 0L;
	    try {
		for (ncomplete=0; ncomplete<nrun; ncomplete++) {
		    long st_time = System.currentTimeMillis();

		    Connection con = rq.getConnection();

		    if (con == null) {
			throw new NullPointerException(
				    "ncomplete = " + ncomplete + ", nrun = " + nrun);
		    }
		    
		    exec_sum += rq.query(con, sql);

		    all_sum += System.currentTimeMillis() - st_time;
		}

	    } catch (Throwable e) {

		rq.logger.logStackTrace(e);
	    }
	}
    }
}


package test;

import java.sql.*;
import java.io.*;
import java.util.Properties;
import java.util.Vector;
import oracle.sql.*;
import oracle.jdbc.driver.*;
import lib.Logger;
import lib.Config;
import java.lang.reflect.Field;

import hronline.engine.ParseRequest;

/**
 * Old connection pool implementation as a reference
 */
public class OldDatabaseMgr
{
    protected Properties mProp;
    protected String mDB_url;
    protected String mDB_user;
    protected String mDB_pw;

    private final String INSERT_ERROR =
	"{? = call AMONBB.INSERT_HR_ERROR(?, ?, ?, ?, ?, ?)}";
    private static final String QUERY_RELEASE_REV = 
	"select RELEASE_REV from HR_SYSTEM_CONFIGURATIONS where HR_ID=?";
    private static final String QUERY_IPADDR = 
	"select PUBLIC_NET_IP from HR_SYSTEM_CONFIGURATIONS where HR_ID=?";
    private final static String GET_HW_REV =
    	"{? = call HROPTIONS.GET_HW_REV(?, ?)}";

    protected int mNumConnections = 5;
    private int mConnIdx = 0;
    private int mCntFullGC = 50; // 50 times of myConnection access
    private int mCntFullGCIdx = 0;
    private boolean initialized = false;

    /* vector of Connection
     */
    private Vector mConn = new Vector();
    protected Object mConnMonitor = new Object(); // monitor for DB connection

    // Alarm Management
    protected boolean alarmMgmt = false;

    // Logger
    protected Logger mLogger;

    // for debug
    public OldDatabaseMgr () throws
	ClassNotFoundException, java.io.FileNotFoundException
    {
	mDB_url = "jdbc:oracle:thin:@panther.routefree.com:1521:rf02";
	mDB_user = Config.DEFAULT_DB_USER;
	mDB_pw = Config.DEFAULT_DB_PASSWD;
	Class.forName(Config.DEFAULT_DB_CLASS);
	mLogger = new Logger(new BufferedOutputStream(
			    new FileOutputStream("test.log", true)));
    }

    /**
     * Initialize all parameters for connecting to the database, based
     * on the specification in the property list.
     * @param prop Property list specifying various database parameters.
     * @exception ClassNotFoundException The JDBC driver cannot be located.
     */
    public OldDatabaseMgr (Properties prop, Logger logger)
	throws ClassNotFoundException
    {
	mProp = prop;
	mLogger = logger;
	if (mLogger != null) {
	    String level = prop.getProperty("ErrLogLevel");
	    if (level == null) {
		level = "WARNING";
	    }
	    try {
		Field f = Logger.class.getField(level);
		mLogger.setLoglevel(f.getInt(mLogger));
	    } catch (Exception e) { }
	}
	mDB_url = prop.getProperty("DB_url");
	mDB_user = prop.getProperty("DB_user");
	if (mDB_user == null) {
	    mDB_user = Config.DEFAULT_DB_USER;
	}
	mDB_pw = prop.getProperty("DB_pw");
	if (mDB_pw == null) {
	    mDB_pw = Config.DEFAULT_DB_PASSWD;
	}

	String numConn = prop.getProperty("DB_num_connections");
	if (numConn != null) {
	    try {
		mNumConnections = Integer.parseInt(numConn);
		if (mNumConnections <= 0) {
		    mNumConnections = 1;
		}
		logger.log("DB connection pool is set to " + mNumConnections);
	    } catch (Exception e) { /* just use the default */ }
	}
	String full_gc = prop.getProperty("DoFullGC_Counter");
	if (full_gc != null) {
	    try {
		mCntFullGC = Integer.parseInt(full_gc);
		if (mCntFullGC <= 0) {
		    mCntFullGC = 1;
		}
		logger.log("FullGC counter is set to " + mCntFullGC);
	    } catch (Exception e) { /* just use the default */ }
	}

	String clazz = prop.getProperty("DB_class");
	if (clazz == null) {
	    clazz = Config.DEFAULT_DB_CLASS;
	}
	Class.forName(clazz);


	// set alarmMgmt flag
	// initialization will be done in enqueue() method, which
	// is the only place involved in Alarm Management        
	String flag = prop.getProperty("AlarmManagement");
	alarmMgmt = (flag != null && flag.equalsIgnoreCase("on"));
	logger.log(
		"Alarm Management is turned " + (alarmMgmt ? "on" : "off"));

    }

    /** Re-establish a database connection and set up prepared statements */
    public void init () throws SQLException {

	synchronized (mConnMonitor) {

	    fini();

	    for (mConnIdx = 0; mConnIdx < mNumConnections; mConnIdx++) {
		mConn.addElement(getConnection());
	    }
	    mConnIdx = 0;
	    mCntFullGCIdx = 0;
	    initialized = true;
	}
	mLogger.log(this.getClass().getName() + ": Connection pool is established");
    }

    /** Shutdown the database connection and release associated resources */
    public void fini () {

	synchronized (mConnMonitor) {

	    if (!initialized) {
		return;
	    }
	    for (mConnIdx = 0; mConnIdx < mConn.size(); mConnIdx++) {
		Connection con = (Connection)mConn.elementAt(mConnIdx);
		if (con == null) {
		    continue;
		}
		try {
		    con.close();
		} catch (SQLException ignored) { }
	    }
	    mConn.removeAllElements();
	    mConnIdx = 0;
	    initialized = false;
	}
	mLogger.log(this.getClass().getName() + ": Connection pool is shut down");
    }

    /**
     * notify connection error to the database.
     * will re-establish the connection
     */
    public void notifyConnectionError() {
	fini();
    }

    /** Make sure all resources are released when this object is deleted */
    public void finalize () throws Throwable {
	fini ();
	super.finalize();
    }

    /**
     * convert a Date String sent from Gateways to Java unified
     * date value (millisec since Epoch
     */
    protected long convertDateString(String time)
    {
	return Long.parseLong(time.trim()) * 1000;
    }

    /**
     * set a Date String sent from Gateways to JDBC Statement
     * time is a string containing the # of seconds since Epoch.
     */
    protected void stmtSetDate (CallableStatement cs, String time, int pos)
	throws SQLException
    {
	long timeValue = convertDateString(time);
	stmtSetDate(cs, timeValue, pos);
    }

    /**
     * set a Date String sent from Gateways to JDBC Statement
     * time is a long # of milli-seconds since Epoch.
     */
    protected void stmtSetDate (CallableStatement cs, long time, int pos)
	throws SQLException
    {
	cs.setTimestamp(pos, new java.sql.Timestamp(time));
    }

    public Connection getConnection() throws SQLException {
	return DriverManager.getConnection(mDB_url, mDB_user, mDB_pw);
    }

    /**
     * Return one connection from the pool
     * This function will never return null
     */
    protected Connection myConnection() throws SQLException
    {
	if (!initialized) {
	    init();
	}

	synchronized (mConnMonitor) {
	    mConnIdx++;
	    if (mConnIdx >= mNumConnections) {
		mConnIdx = 0;
	    }
	    mCntFullGCIdx++;
	    if (mCntFullGCIdx >= mCntFullGC) {
		System.gc();
		mCntFullGCIdx = 0;
	    }
	    try {
		if (mConn.size() <= mConnIdx) {
		    // no available connection - must be connection is down
		    throw new SQLException("myConnection() - no connection available");
		}

		Connection con = (Connection)mConn.elementAt(mConnIdx);
		if (con == null || con.isClosed() || hasInvalidConnection(con)) {
		    // somebody closed it !!
		    // create new one
		    con = null; // let JVM knows it sooner
		    con = getConnection();
		    mConn.set(mConnIdx, con);
		}

		return con;

	    } catch (SQLException e) {
		// seems db connection is down
		notifyConnectionError();
		throw e;
	    }
	}
    }

    protected int getNumConnections()
    {
	return  mNumConnections;
    }

    protected boolean hasInvalidConnection(Connection con)
    {
        boolean bRet = false;

        try {
            Statement st = con.createStatement();

            String select_sql = "select COUNT(*) from DUAL "; 
            ResultSet rs = st.executeQuery(select_sql);

            st.close();
           
            bRet = false;

        } catch (SQLException e) {
            bRet = true;
        }

        return bRet;

    }

    /**
     * insert it to AMON
     * return false if Alarm Management is turned off
     */
    protected boolean postAlarmMessage(
	    		Connection con, 
			String hrid,
			long report_date,
			int seq_no,
			String error_code,
			String error_mesg,
			String release_rev)
    	throws SQLException
    {
	if (!alarmMgmt) {
	    return false;
	}

	synchronized(con) {
	    CallableStatement cs = null;
	    try {
		cs = con.prepareCall(INSERT_ERROR);
		cs.registerOutParameter(1, Types.NUMERIC);

		cs.setString(2, hrid);
		stmtSetDate(cs, report_date, 3);
		cs.setString(4, error_code);
		cs.setString(5, error_mesg);
		cs.setString(6, release_rev);
		cs.setInt(7, seq_no);

		cs.executeUpdate();
		int ret = cs.getInt(1);
		if (ret < 0) {
		    mLogger.error("DatabaseMgr",
			    "Following message caused oracle error:\n" +
			    error_mesg +
			    "\nThis message is not processed");
		} else {
		    mLogger.debug("DatabaseMgr",
			    "Alarm ("
				+ error_mesg
				+ ") from "
				+ ParseRequest.formHRid(hrid)
				+ " is posted to the AMON");
		}

	    } finally {

		try {
		    if (cs != null) {
			cs.close();
		    }
		} catch (SQLException e) {
		    mLogger.error("DatabaseMgr",
			    "SQLException in closing statement: " + e);
		}
	    }
	}

	return true;
    }


    /**
     * hrid should be in the form of "HRxxxxxxxxxxxx" or decimal string
     */
    public String getReleaseRev(Connection con, String hrid)
		    throws SQLException
    {
	return executeSimpleGatewayQuery(con, QUERY_RELEASE_REV, hrid);
    }

    /**
     * hrid should be in the form of "HRxxxxxxxxxxxx" or decimal string
     */
    public String getIPAddress(Connection con, String hrid)
		    throws SQLException
    {
	return executeSimpleGatewayQuery(con, QUERY_IPADDR, hrid);
    }

    private String executeSimpleGatewayQuery(
		    Connection con, String qstr, String hrid)
		    throws SQLException
    {
	String hrid_d = hrid;
        if (hrid.length() > 2
		&& hrid.substring(0, 2).equalsIgnoreCase("HR")) {
	    hrid_d = hronline.engine.ParseRequest.parseHRid(hrid);
	}
	PreparedStatement stmt = null;
	ResultSet rs = null;
        try {
	    stmt = con.prepareStatement(QUERY_RELEASE_REV);
	    stmt.clearParameters();
	    stmt.setString(1, hrid_d);
	    rs = stmt.executeQuery();
	    String rel = "";
	    if (rs.next()) {
		return rs.getString(1);

	    } else {
		return null;
	    }
	} finally {
	    if (rs != null) {
		rs.close();
	    }
	    if (stmt != null) {
		stmt.close();
	    }
	}
    }

    public String getHardwareRevision(
	    Connection con, String base_rev, String model_name)
    		throws SQLException
    {
	synchronized(con) {
	    CallableStatement cs = null;

	    try {
		cs = con.prepareCall(GET_HW_REV);
		cs.registerOutParameter(1, Types.NUMERIC);
		cs.setInt(2, Integer.parseInt(base_rev));
		cs.setString(3, model_name);
		cs.executeUpdate();
		return Long.toString(cs.getLong(1));

	    } finally {

		if (cs != null) {
		    try {
			cs.close();
		    } catch (Exception e) {
			// secondary error - just log it
			mLogger.error("DatabaseMgr",
			    "SQL exception in closing statement " + e);
		    }
		}
	    }
	}
    }
}


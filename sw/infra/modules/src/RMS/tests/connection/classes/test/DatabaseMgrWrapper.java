package test;

import lib.*;

import java.util.*;
import java.sql.*;

public class DatabaseMgrWrapper extends OldDatabaseMgr
{
    public DatabaseMgrWrapper(Properties prop, Logger logger)
            throws ClassNotFoundException
    {
	super(prop, logger);
    }

    public Connection getMyConnection() throws SQLException
    {
	return myConnection();
    }
}


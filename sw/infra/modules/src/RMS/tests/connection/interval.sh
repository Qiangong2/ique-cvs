#!/bin/sh
#
# $1 test name (run|examine|cleanup)
# $2 Database URL
#

TOOLDIR=`dirname $0`/../tools/db

. `dirname $0`/common.sh 

# read DB params from rf/src/server/tests/scripts/db.conf

DBCONF=`dirname $0`/../scripts/db.conf
. $DBCONF

# set all db params
set_dbparams ${2:-${DB_url}}

TEST_OPTIONS="-longsql 10 -shortsql 10 -run 10 -min 2 -max 5 -idletime 30 -interval 45 -verbose -log $TMP_RESULT -pool"

TEST_NAME=${1:-run}
case $TEST_NAME in
    run)
	sh $TOOLDIR/startDB $DBHOST $DBNAME
	execute_test $TEST_OPTIONS
	;;

    examine)
	examine_result 8 200 200 5 3 8 0 0 0
	;;

    cleanup)
	clean_up
	;;
esac


#!/bin/sh
#
# $1 test name (run1|run2|run3|examine1|examine2|examine3|cleanup)
# $2 Database URL
#
# stopDB/startDB are located under rf/src/server/tests/tools/db/.
# and this test assumes that stopDB & startDB should not take
# more than 120 seconds
#

TOOLDIR=`dirname $0`/../tools/db

. `dirname $0`/common.sh 

# read DB params from rf/src/server/tests/scripts/db.conf

DBCONF=`dirname $0`/../scripts/db.conf
. $DBCONF

# set all db params
set_dbparams ${2:-${DB_url}}

TEST_OPTIONS="-longsql 10 -shortsql 10 -run 10 -min 2 -max 5 -idletime 600 -interval 150 -verbose -log $TMP_RESULT -pool"

TEST_NAME=${1:-run}
case $TEST_NAME in

    run1)
	# db is down entirely from the begining to the end
	sh $TOOLDIR/stopDB $DBHOST $DBNAME
	execute_test_async $TEST_OPTIONS
	sleep 30 # wait till short-sql-test finishes
	examine_execution $PID
	sh $TOOLDIR/startDB $DBHOST $DBNAME
	;;

    examine1)
	# 20 threads try opening connection 2 times - makes it 40 failures
	examine_result 0 0 0 0 0 0 0 40 1 
	;;

    run2)
	# db is down from the begining to just before longsql test
	sh $TOOLDIR/stopDB $DBHOST $DBNAME
	execute_test_async $TEST_OPTIONS
	sleep 30 # wait till short-sql-test finishes
	sh $TOOLDIR/startDB $DBHOST $DBNAME
	examine_execution $PID
	;;

    examine2)
	# 10 threads try opening connection 2 times - makes it 20 failures
	# other params are for longsql tests
	examine_result 5 100 100 0 0 5 0 20 1
	;;

    run3)
	# db is down only when longsql runs
	sh $TOOLDIR/startDB $DBHOST $DBNAME
	execute_test_async $TEST_OPTIONS
	sleep 30 # wait till short-sql-test finishes
	sh $TOOLDIR/stopDB $DBHOST $DBNAME
	examine_execution $PID
	sh $TOOLDIR/startDB $DBHOST $DBNAME
	;;

    examine3)
	# fail happnes on reuse of the connection 5 times, then new connections
	# other params are for shortsql tests
	examine_result 5 100 100 0 5 5 1 20 1
	;;

    cleanup)
	clean_up
	;;
esac


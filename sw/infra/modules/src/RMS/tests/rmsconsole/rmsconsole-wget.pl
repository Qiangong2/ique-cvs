#!/usr/bin/perl

use POSIX;

########roll the DB with current date#############
$DB_SERVER = "panther";
$DB_INSTANCE = "acer";
$ROLL_DB = sprintf "rsh -l root %s \"su - oracle 'rollit -s %s'\"", $DB_SERVER, $DB_INSTANCE;
@args = ($ROLL_DB);
system(@args) == 0
  or die "failed to roll rms golden image in DB";

########crawl rms console#############
$WGET = "../../http_command/wget-1.7.1/src/wget";
$CRAWL_DIR = "/tmp/rms.sh";
$CRAWL_URL = "http://rmserver:8088/hron_admin/home";
$USER = "admin";
$PWD = "nimda";
$EX_STR = "type=live";

$DEL_TMP_CRAWL = sprintf "/bin/rm -rf %s", $CRAWL_DIR;
@args = ($DEL_TMP_CRAWL);
system(@args) == 0
  or die "del tmp crawled dir failed";

$CRAWL_RMS = sprintf "%s -r -l 0 -nH -P %s --http-user %s --http-passwd %s --exclude-url-substrings=%s %s 2>&1 | tee %s/wget.output", $WGET, $CRAWL_DIR, $USER, $PWD, $EX_STR, $CRAWL_URL, $CRAWL_DIR;
@args = ($CRAWL_RMS);
system @args; 
#  or die "failed to crawl rms server";

########copy and update golden image with current date#########
$GOLDEN_TAR = "golden-2.3.tar";
$TMP_DIR = "/tmp";
$TMP_GOLDEN_DIR = "/tmp/golden-2.3";
$TMP_GOLDEN_TAR = "/tmp/golden-2.3.tar";
$DEL_GOLDEN_DIR = sprintf "/bin/rm -rf %s", $TMP_GOLDEN_DIR;
$DEL_GOLDEN_TAR = sprintf "/bin/rm -rf %s", $TMP_GOLDEN_TAR;
$COPY_GOLDEN_TAR = sprintf "cp %s %s", $GOLDEN_TAR, $TMP_DIR;
$UNTAR_GOLDEN = sprintf "tar -C %s -xf %s", $TMP_DIR, $GOLDEN_TAR;
@args = ($DEL_GOLDEN_DIR);
system(@args) == 0
  or die "del tmp golden image dir failed";
@args = ($DEL_GOLDEN_TAR);
system(@args) == 0
  or die "del tmp golden image tar file failed";
@args = ($COPY_GOLDEN_TAR);
system(@args) == 0
  or die "cannot copy golden image tar file to /tmp";
@args = ($UNTAR_GOLDEN);
system(@args) == 0
  or die "cannot untar golden image in /tmp";

$template = "%Y-%m-%d";
$sec_03_06 = 1015401600; @lt = localtime($sec_03_06); $date_2002_03_06 = strftime($template, @lt);
$sec_03_05 = 1015315200; @lt = localtime($sec_03_05); $date_2002_03_05 = strftime($template, @lt); 
$sec_02_26 = 1014710400; @lt = localtime($sec_02_26); $date_2002_02_26 = strftime($template, @lt); 
$sec_today = time;       @lt = localtime($sec_today); $TODAY = strftime($template, @lt);
$delta = $sec_today - $sec_03_06;
@lt = localtime($sec_03_05+$delta); $new_2002_03_05 = strftime($template, @lt);
@lt = localtime($sec_02_26+$delta); $new_2002_02_26 = strftime($template, @lt);

$REPLACE_GOLDEN = sprintf "find %s -maxdepth 2 -type f -exec ./replace.sh \"%s\" \"%s\" '{}' \\;", $TMP_GOLDEN_DIR, $date_2002_03_06, $TODAY;
@args = ($REPLACE_GOLDEN);
system(@args) == 0
  or die "replace golden image failed";

$REPLACE_GOLDEN = sprintf "find %s -maxdepth 2 -type f -exec ./replace.sh \"%s\" \"%s\" '{}' \\;", $TMP_GOLDEN_DIR, $date_2002_03_05, $new_2002_03_05;
@args = ($REPLACE_GOLDEN);
system(@args) == 0
  or die "replace golden image failed";

$REPLACE_GOLDEN = sprintf "find %s -maxdepth 2 -type f -exec ./replace.sh \"%s\" \"%s\" '{}' \\;", $TMP_GOLDEN_DIR, $date_2002_02_26, $new_2002_02_26;
@args = ($REPLACE_GOLDEN);
system(@args) == 0
  or die "replace golden image failed";

########compare crawled data with golden image##########
$DIFF_RESULT = "/tmp/rms-diff.out";
$DIFF = sprintf "diff -r --exclude=*live=true --exclude=wget* %s %s > %s", $CRAWL_DIR, $TMP_GOLDEN_DIR, $DIFF_RESULT;
@args = ($DIFF);

$rc = 0xFFFF & system @args;
if ($rc==0) {
  print "PASSED";
} else {
  print "FAILED";
}

#!/bin/bash

if [ $# -ne 3 ]; then
    echo "usage: `basename $0` from-str to-str filename"
    exit 1
fi

FROMSTR="$1"
TOSTR="$2"
FILENAME="$3"
TMPFILE=`mktemp /tmp/replace.sh.XXXXXX`

cat $FILENAME | sed "s/$FROMSTR/$TOSTR/g" > $TMPFILE
mv -f $TMPFILE $FILENAME

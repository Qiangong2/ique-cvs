HRID=${1:-HR0050C20E68C8}
SNMP_HOST=${2:-panther}

MIBDIR=/opt/broadon/pkgs/rms/etc/mib

GW_NODE=enterprises.broadon.gateway
BASE_TABLE=${GW_NODE}.system.base.systemBaseTable.systemBaseEntry
NETWORK_TABLE=${GW_NODE}.system.network.systemNetworkTable.systemNetworkEntry
DISK_TABLE=${GW_NODE}.system.disk.distStatusTable.diskStatusEntry

snmpget -v 1 -m all -M $MIBDIR -v 1 $SNMP_HOST public \
	${BASE_TABLE}.hrId.\"$HRID\" \
	${BASE_TABLE}.model.\"$HRID\" \
	${BASE_TABLE}.hwRevision.\"$HRID\" \
	${BASE_TABLE}.swRevision.\"$HRID\" \
	${BASE_TABLE}.serviceDomain.\"$HRID\" \
	${BASE_TABLE}.hostName.\"$HRID\" \
	${BASE_TABLE}.uptime.\"$HRID\" \
	${BASE_TABLE}.systemClock.\"$HRID\" \
	${BASE_TABLE}.rebootSystem.\"$HRID\" \
	${BASE_TABLE}.updateSoftware.\"$HRID\" \
	${BASE_TABLE}.resetPassword.\"$HRID\" \
	${BASE_TABLE}.updateActivation.\"$HRID\"

snmpget -v 1 -m all -M $MIBDIR -v 1 $SNMP_HOST public \
	${NETWORK_TABLE}.uplinkAddress.\"$HRID\" \
	${NETWORK_TABLE}.uplinkMACaddress.\"$HRID\" \
	${NETWORK_TABLE}.uplinkConnection.\"$HRID\" \
	${NETWORK_TABLE}.defaultGateway.\"$HRID\" \
	${NETWORK_TABLE}.dns0.\"$HRID\" \
	${NETWORK_TABLE}.dns1.\"$HRID\" \
	${NETWORK_TABLE}.dns2.\"$HRID\" \
	${NETWORK_TABLE}.connectionStatus.\"$HRID\"



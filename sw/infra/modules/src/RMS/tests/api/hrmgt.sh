#!/bin/bash
#
# HR Remote Management Tests
#
# SYNOPSIS
#	hrmgt.sh test-case [arguments ...]
#
# DESCRIPTION
#	hrmgt.sh is a  collection of test cases that run  through the HR
#	Remote  Management functionality.  To list  all test  cases, run
#	"hrmgt.sh list".  To run  a particular  test case,  run hrmgt.sh
#	with the name of the test case (corresponding to one of the bash
#	functions defined below)  and any arguments that  that test case
#	may require.

. ./hrmgt.conf

function start_hrsim () {
    if [ $# -gt 2 ]; then
	./hrmgt.exp -c $HR_KEYPAIR -m "$1" -r "$2" -v "$3" > /dev/null &
    elif [ $# -gt 1 ]; then
	./hrmgt.exp -c $HR_KEYPAIR -m "$1" -r "$2" > /dev/null &
    else
	./hrmgt.exp -c $HR_KEYPAIR -m "$1" > /dev/null &
    fi
    HR_SIM_PID=$!
}

function stop_hrsim () {
    kill $HR_SIM_PID
}

function run_java_client () {
    java -classpath $SERVER_CLASSPATH hronline.manager.HRRemoteManager \
	$SERVER_CONF_PROPERTIES $SERVER_PROPERTIES $SERVER_KEYS $*
}

function log () {
    echo \[`date`\] $LOG_IDENT: $*
}

function run_generic_test () {
    if [ -z "$LOG_IDENT" ]; then
	log 'FAILED (LOG_IDENT undefined)'
	return 1
    fi

    TEST_TEMP=`mktemp /tmp/$LOG_IDENT.XXXXXX`
    if [ $? -ne 0 ]; then
	log 'FAILED (mktemp bad)'
	return 1
    fi

    log 'starting'
    if [ $# -gt 3 ]; then
	start_hrsim "$2" "$3" "$4"
    elif [ $# -gt 2 ]; then
	start_hrsim "$2" "$3"
    else
	start_hrsim "$2"
    fi
    run_java_client $JAVA_ARGS > $TEST_TEMP
    stop_hrsim

    log 'checking results'
    diff -u "$1" $TEST_TEMP
    TEST_RESULT=$?
    if [ -z "$TEST_SAVE_OUTPUT" ]; then
	rm -f $TEST_TEMP
    fi
    if [ $TEST_RESULT -ne 0 ]; then
	log 'FAILED (diff bad)'
	return 1
    fi

    log 'succeeded'
}

#
# test cases
#

function test_sim_sysquery () {
    LOG_IDENT=test_sim_sysquery
    JAVA_ARGS="localhost HR0050C20E6039 query"
    run_generic_test $LOG_IDENT.out 'GET sys' hrsim_sys.in
}

function test_sim_firewall () {
    LOG_IDENT=test_sim_firewall
    JAVA_ARGS="localhost HR0050C20E6039 get sys.firewall"
    run_generic_test $LOG_IDENT.out 'GET sys.firewall' hrsim_firewall.in
}

function test_sim_domain () {
    LOG_IDENT=test_sim_domain
    JAVA_ARGS="localhost HR0050C20E6039 domain yahoo.com"
    run_generic_test null.out 'SET sys.base.serviceDomain "yahoo.com"'
}

function test_sim_resetpw () {
    LOG_IDENT=test_sim_resetpw
    JAVA_ARGS="localhost HR0050C20E6039 resetpw"
    run_generic_test null.out 'SET sys.cmd.resetPassword "1"'
}

function test_sim_reboot () {
    LOG_IDENT=test_sim_reboot
    JAVA_ARGS="localhost HR0050C20E6039 reboot"
    run_generic_test null.out 'SET sys.cmd.reboot "1"'
}

function test_sim_swupd () {
    LOG_IDENT=test_sim_swupd
    JAVA_ARGS="localhost HR0050C20E6039 swupd"
    run_generic_test null.out 'SET sys.cmd.updateSoftware "1"'
}

function test_sim_activate () {
    LOG_IDENT=test_sim_activate
    JAVA_ARGS="localhost HR0050C20E6039 activate"
    run_generic_test null.out 'SET sys.cmd.activate "1"'
}

function test_bad_host_timeout () {
    local START_TIME
    local STOP_TIME

    LOG_IDENT=test_bad_host_timeout

    TEST_TEMP=`mktemp /tmp/$LOG_IDENT.XXXXXX`
    if [ $? -ne 0 ]; then
	log 'FAILED (mktemp bad)'
	return 1
    fi

    log 'starting test'
    START_TIME=`date +%s`
    run_java_client 10.10.10.10 HR0050C20E6039 query > $TEST_TEMP
    STOP_TIME=`date +%s`

    log 'checking results'
    diff -u $LOG_IDENT.out $TEST_TEMP
    TEST_RESULT=$?
    if [ -z "$TEST_SAVE_OUTPUT" ]; then
	rm -f $TEST_TEMP
    fi
    if [ $TEST_RESULT -ne 0 ]; then
	log 'FAILED (diff bad)'
	return 1
    fi
    STOP_TIME=`echo $STOP_TIME - $START_TIME | bc -q`
    if [ "$STOP_TIME" -gt 50 ]; then
	log 'FAILED (45 second timeout exceeded)' $STOP_TIME
	return 1
    fi

    log 'succeeded'
}

function test_bad_connect () {
    local START_TIME
    local STOP_TIME

    LOG_IDENT=test_bad_connect

    TEST_TEMP=`mktemp /tmp/$LOG_IDENT.XXXXXX`
    if [ $? -ne 0 ]; then
	log 'FAILED (mktemp bad)'
	return 1
    fi

    log 'starting test'
    START_TIME=`date +%s`
    run_java_client localhost HR0050C20E6039 query > $TEST_TEMP
    STOP_TIME=`date +%s`

    log 'checking results'
    diff -u $LOG_IDENT.out $TEST_TEMP
    TEST_RESULT=$?
    if [ -z "$TEST_SAVE_OUTPUT" ]; then
	rm -f $TEST_TEMP
    fi
    if [ $TEST_RESULT -ne 0 ]; then
	log 'FAILED (diff bad)'
	return 1
    fi
    STOP_TIME=`echo $STOP_TIME - $START_TIME | bc -q`
    if [ "$STOP_TIME" -gt 17 ]; then
	log 'FAILED (17 s timeout exceeded)'
	return 1
    fi

    log 'succeeded'
}

function test_bad_certificate () {
    LOG_IDENT=test_bad_certificate
    JAVA_ARGS="localhost bad_cert query"
    run_generic_test $LOG_IDENT.out 'GET sys'
}

function test_bad_status () {
    LOG_IDENT=test_bad_status
    JAVA_ARGS="localhost HR0050C20E6039 query"
    run_generic_test $LOG_IDENT.out 'mismatch'
}

function test_bad_proto_ver () {
    LOG_IDENT=test_bad_proto_ver
    JAVA_ARGS="localhost HR0050C20E6039 query"
    run_generic_test $LOG_IDENT.out 'GET sys' hrsim_sys.in 10.0
}

#
# test_real_sysquery - queries a live HR and checks to see that all required
#                      fields exist.
#
function test_real_sysquery () {
    local REPORTED_VERBATIM
    local FILTERED_OUTPUT
    LOG_IDENT=test_real_sysquery
    log 'starting'

    # query HR and build list of reported fields (properly filtered and stripped)
    TEST_TEMP=`mktemp /tmp/$LOG_IDENT.XXXXXX`
    if [ $? -ne 0 ]; then
	log 'FAILED (mktemp bad)'
	return 1
    fi
    chmod 666 $TEST_TEMP

    # query HR fields
    run_java_client $REAL_HR_IP $REAL_HR_ID query | \
        grep -v 'parameter\|hrconfig' | \
        sed 's/^[ 	]*//g' | \
        tr '\n' ' ' | \
        sed 's#</value> #</value>~#g' | \
        tr '~' '\n' | \
        grep -v '^[[:space:]]*$' | \
        sort > $TEST_TEMP

    # XXX-out any fields we don't care about
    grep XXX $LOG_IDENT.out | \
        sed 's;<desc>\(.*\)</desc>.*$;\\#\1#s#<value>.*</value>#<value>XXX</value>#g;g' | \
        sed -f - $TEST_TEMP | \
        diff -u $LOG_IDENT.out -
    TEST_RESULT=$?

    if [ -z "$TEST_SAVE_OUTPUT" ]; then
	rm -f $TEST_TEMP
    fi

    if [ $TEST_RESULT -ne 0 ]; then
	log 'FAILED (diff bad)'
	return 1
    fi

    log 'succeeded'
}

#
# psuedo test cases (helpful command-line targets)
#

function list () {
    grep '^function test_' $0 | sed 's/function \([^ ]*\) () {/\1/'
}

function all () {
    TEST_CASES=`list`
    for T in $TEST_CASES; do
	$0 $T
    done
}

if [ $# -le 0 ]; then
    echo "usage: `basename $0` test-case [arguments ...]"
    echo "see the comments at the top of `basename $0` for details"
    exit 1
fi

if [ "$1" = "-k" ]; then
    TEST_SAVE_OUTPUT=1
    shift
fi

$*

#!/usr/local/tools/bin/expect --
#
# regDownload [-hi HW_rev] [-si SW_rev] [-so SW_rev_ret] 
#             - regular software update request to server with optionally
#               specified input HW_rev, input SW_rev, and output (expected)SW_rev_ret. 
#               When [-so] is specified, the returned SW release verison is matched 
#               against the specified value. 
#

log_user 0

puts "Usage: regDownload \[-hi HW_rev\] \[-si SW_rev\] \[-so SW_rev_ret\]"
puts "       When \[-so\] is specified, the returned SW release version "
puts "       is matched against the specified value."
set post_dir [pwd]/../../dist/cmd
puts $post_dir

package require http 2
http::config

variable succ 1
variable day_in_mms 5184000

set HW_rev 1090
set SW_rev 2001060101
set SW_rev_ret 0

while {[llength $argv]>0} {
    set flag [lindex $argv 0]
    switch -- $flag \
    "-hi" {
        set argv [lrange $argv 1 end]
        set HW_rev [lrange $argv 0 0]        
        set argv [lrange $argv 1 end]
    } "-si" {
        set argv [lrange $argv 1 end]
        set SW_rev [lrange $argv 0 0]
        set argv [lrange $argv 1 end]
    } "-so" {
        set argv [lrange $argv 1 end]
        set SW_rev_ret [lrange $argv 0 0]
        set argv [lrange $argv 1 end]
    } default {
        break
    }
}

proc retrieve {url} {
    puts "checking a retrival of the file"
    if { [catch {set token [http::geturl $url]} errmsg] } {
        puts "FAILED: download $errmsg: $url"
        return 1
    }
    set result [http::code $token]
    if { [lrange $result 1 1] == 200} {
        puts "SUCCESS: downloaded $url"
        return 0
    } else {
        puts "FAILED: download $result from $url"
        return 1
    }
}
 
exp_spawn $post_dir/post -url https://download:8443/hr_update/entry?mtype=regDownload -cert $post_dir/sample_cert.der -ca $post_dir/sample_ca.pem -key $post_dir/sample_key.der -password free2route -p HR_id=HR123456789abc/HW_rev=$HW_rev/Release_rev=$SW_rev/

variable got_timestamp 0
variable got_rel 0
variable got_key 0
variable got_file 0

expect {
    "HTTP/1.*" {
        set status [lrange $expect_out(0,string) 1 1]
        if {$status != 200} {
            puts "FAILED: HTTP status $status"
            set succ 0
        } else {
            puts "got status $status"
            exp_continue
        }
    }
    "timestamp = *" {
        set t1 [lrange $expect_out(0,string) 2 2]
        set t2 [clock seconds] 
        if { [expr (abs($t2-$t1)) > $day_in_mms ] } {
            puts "FAILED: time mismatch $t1 $t2"
            set succ 0
        } else {
            set got_timestamp 1
            puts "got timestamp $t1"
            exp_continue
        }
    }
    "release = *"  {
        set r [lrange $expect_out(0,string) 2 2]
        if { $SW_rev_ret!=0 && $r!=$SW_rev_ret } {
            puts "FAILED: incorrect software release $r"
            set succ 0
        } else {
            set got_rel 1
            puts "got release $r"
            exp_continue
        }
    }
    "key = *" {
        set k [lrange $expect_out(0,string) 2 2]
        puts "got key $k "
        set got_key 1
        exp_continue
    }
    "file = *" {
        if {[retrieve [lrange $expect_out(0,string) 2 2]]} {
            set succ 0
        } else {
            set got_file 1
            exp_continue
        }
    }
    -re "Cannot|Unable" {
        puts "FAILED: Unable to connect to the server"
    }
}

if {!$got_rel || !$got_key || !$got_file} {
    set succ 0
}

if {$succ} {
    puts "Test regDownload: SUCCESS"
    exit 0
} else {
    puts "Test regDownload: FAILED"
    exit 1
}


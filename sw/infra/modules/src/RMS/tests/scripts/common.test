
proc do_sed_release {line outfile hw sw cid} {

    if { [string match "*<HR_REV></HR_REV>" $line] } {
	puts $outfile "<hr_rev>0x$hw</hr_rev>"
    } elseif { [string match "*<HW_REV>*</HW_REV>" $line] } {
	puts $outfile "<revision>0x$hw</revision>"
    } elseif { [string match "*<hw_rev>HW_REV</hw_rev>" $line] } {
	puts $outfile "<hw_rev>0x$hw</hw_rev>"
    } elseif { [string match "*<revision>HW_REV</revision>" $line] } {
	puts $outfile "<revision>0x$hw</revision>"
    } elseif { [string match "*<model>TEST_MODEL</model>" $line] } {
	puts $outfile "<model>TEST</model>"
    } elseif { [string match "*<modelcode>TEST_MODEL_CODE</modelcode>" $line] } {
	puts $outfile "<modelcode>66</modelcode>"
    } elseif { [string match "*<sw_bundle major_version=\"1.0.0\" revision=\"REPLACE_ME\">" $line] } {
	puts $outfile "<sw_bundle major_version=\"1.0.0\" revision=\"$sw\">"
    } elseif { [string match "*<REVISION>*</REVISION>" $line] } {
	puts $outfile "<revision>$sw</revision>"
    } elseif { [string match "*<content_file_id>*</content_file_id>" $line] } {
	puts $outfile "<content_file_id>$cid</content_file_id>"
    } else {
	puts $outfile $line
    }
}

proc do_sed_remove {line outfile hw sw cid} {

    if { [string match "*<HR_REV></HR_REV>" $line] } {
	puts $outfile "<hr_rev>0x$hw</hr_rev>"
    } elseif { [string match "*<HW_REV>*</HW_REV>" $line] } {
	puts $outfile "<revision>0x$hw</revision>"
    } elseif { [string match "*<hw_rev>HW_REV</hw_rev>" $line] } {
	puts $outfile "<hw_rev>0x$hw</hw_rev>"
    } elseif { [string match "*<revision>HW_REV</revision>" $line] } {
	puts $outfile "<revision>0x$hw</revision>"
    } elseif { [string match "*<model>TEST_MODEL</model>" $line] } {
	puts $outfile "<model>TEST</model>"
    } elseif { [string match "*<modelcode>TEST_MODEL_CODE</modelcode>" $line] } {
	puts $outfile "<modelcode>66</modelcode>"
    } elseif { [string match "*<sw_bundle major_version=\"1.0.0\" revision=\"REPLACE_ME\">" $line] } {
	puts $outfile "<sw_bundle major_version=\"1.0.0\" revision=\"$sw\">"
    } elseif { [string match "*<REVISION>*</REVISION>" $line] } {
	puts $outfile "<revision>$sw</revision>"
    } elseif { [string match "*<content_file_id>*</content_file_id>" $line] } {
	puts $outfile "<content_file_id>$cid</content_file_id>"
    } elseif { [string match "*<hw_release>" $line] } {
	puts $outfile "<hw_release action=\"delete\">"
    } elseif { [string match "*<sw_release>" $line] } {
	puts $outfile "<sw_release action=\"delete\">"
    } elseif { [string match "*<sw_module_release>" $line] } {
	puts $outfile "<sw_module_release action=\"delete\">"
    } elseif { [string match "*<model_release>" $line] } {
	puts $outfile "<model_release action=\"delete\">"
    } else {
	puts $outfile $line
    }
}

proc post_rel {server cert_dir passwd file sw hw} {
    set tmp_sample_file "/tmp/$hw$sw.xml"
    set file1 [open $file r]
    set file2 [open $tmp_sample_file w]

    while {[gets $file1 line] != -1} {
	do_sed_release $line $file2 $hw $sw $sw
    }

    close $file1
    close $file2

    set post_rel  ../../dist/package/cmd/postrel

    spawn touch /tmp/appfs.img
    spawn touch /tmp/kernel.img

    exp_spawn $post_rel -url https://${server}:8443/hron_sw_rel/entry -cert $cert_dir/identity.pem -ca $cert_dir/root_cert.pem -key $cert_dir/private_key.pem -password $passwd $tmp_sample_file /tmp/appfs.img /tmp/kernel.img

    variable result 1

    set timeout -1
    expect {
        "The software release has been registered at *" {
            puts "got status"
            set result 0
            exp_continue
        }
        -re "Cannot|Unable" {
            puts "FAILED: Unable to connect to the server"
            set result 1
        }
    }

    if { $result } {
        puts "FAILED:  $sw post_rel-1.1"
    } else {
        puts "PASSED: $sw post_rel-1.1"
    }

    exec /bin/rm -f $tmp_sample_file
    return $result
}

proc post_extrel {server cert_dir passwd file} {

    set post_rel  ../../dist/package/cmd/postrel

    spawn touch /tmp/release.dsc

    exp_spawn $post_rel -url https://${server}:8443/hron_sw_rel/entry -cert $cert_dir/identity.pem -ca $cert_dir/root_cert.pem -key $cert_dir/private_key.pem -password $passwd $file /tmp/release.dsc

    variable result 1

    set timeout -1
    expect {
        "The software release has been registered at *" {
            puts "got status"
            set result 0
            exp_continue
        }
        -re "Cannot|Unable" {
            puts "FAILED: Unable to connect to the server"
            set result 1
        }
    }

    if { $result } {
        puts "FAILED:  post_extrel-1.1"
    } else {
        puts "PASSED: post_extrel-1.1"
    }

    return $result
}

proc remove_rel {server cert_dir passwd file sw hw} {
    set tmp_sample_file "/tmp/$hw$sw.xml"
    set file1 [open $file r]
    set file2 [open $tmp_sample_file w]
    while {[gets $file1 line] != -1} {

	do_sed_remove $line $file2 $hw $sw $sw
    }
    close $file1
    close $file2

    set post_rel  ../../dist/package/cmd/postrel

    exp_spawn $post_rel -url https://${server}:8443/hron_sw_rel/entry -cert $cert_dir/identity.pem -ca $cert_dir/root_cert.pem -key $cert_dir/private_key.pem -password $passwd $tmp_sample_file

    variable result 1

    set timeout -1
    expect {
        "The software release has been registered at *" {
            puts "got status"
            set result 0
            exp_continue
        }
        -re "Cannot|Unable" {
            puts "FAILED: Unable to connect to the server"
            set result 1
        }
    }

    if { $result } {
        puts "FAILED:  $sw remove_rel-1.1"
    } else {
        puts "PASSED: $sw remove_rel-1.1"
    }

    exec /bin/rm -f $tmp_sample_file
    return $result
}

proc sysStatus_process_line {id line got_status got_timestamp ret} {
    upvar $got_status status $got_timestamp t

    set s [lrange $line 0 end] 
    if {[string match "HTTP/1*" $s]} {
        set status [lrange $line 1 1]
        puts "$id: got status $status"
    }

    if {[string match "timestamp = *" $s]} {
        set t [lrange $line 2 2]
        puts "$id: got timestamp $t"
    }
}

proc sysStatus {server cert_dir passwd db_conf id ipaddr hw sw ret} {
    set post_dir ../../dist/package/cmd
    set query_dir ../tools/querytool
    set atime 982304732
    set secs [clock seconds]

    exp_spawn $post_dir/post -url https://${server}:8443/hr_status/entry?mtype=sysStatus -cert $cert_dir/identity.pem -ca $cert_dir/root_cert.pem -key $cert_dir/private_key.pem -password $passwd -p HR_id=$id/IP_addr=$ipaddr/HW_rev=$hw/Release_rev=$sw/Activate_date=$atime/Report_date=$secs/HW_model=TEST

    set got_status 0
    set got_timestamp 0
    set timeout -1
    expect {
        -re "(\[^\r]*)\r\n" { 
            sysStatus_process_line $id $expect_out(buffer) got_status got_timestamp $ret
            exp_continue
        }
    }

    #check if FAIL, PASS or XFAIL from HTTP status
    if {$got_status==$ret && $got_timestamp!=0} {
        puts "PASS(HTTP): sysStatus $id"

        set ip 1
        set h 1
        set s 1
        set t 1
        set query "$query_dir/runquery QueryHRStatus -prop $db_conf $id"
        exp_spawn $query_dir/runquery QueryHRStatus -prop $db_conf $id
        puts $query
        set timeout -1
        expect {
            -re "(\[^\r]*)\r\n" { 
                set line [split $expect_out(buffer) ","]
                if { [lindex $line 0] == $id } {
                    if { [lindex $line 2] != $ipaddr } {
                         puts "FAIL: $id IP_Addr mismatch [lindex $line 2] $ipaddr"
                         set ip 0
                    }
                    if { [lindex $line 3] != "0x$hw" } {
                         puts "FAIL: $id HW_Rev mismatch [lindex $line 3] $hw"
                         set h 0
                    }                    
		    set xx [lindex $line 4]
		    set xx [string range $xx 7 16]
		    set xx [string trimleft $xx "0 "]
                    if { $xx != $sw } {
                         puts "FAIL: $id Release_Rev mismatch $xx $sw"
                         set s 0
                    }
                    set t1 [timestamp -format "%Y-%m-%d %X.0" -seconds $secs]
                    if { $t1!=[lrange [lindex $line 1] 0 1] } {
                         puts "FAIL: $id Report_date mismatch [lrange [lindex $line 1] 0 1] $t1"
                         set t 0
                    }
                }
                exp_continue
            }
        }
        if {!$ip || !$h || !$s || !$t} {
            set result 1
        } else {
            set result 0
            puts "PASS: $id sysStatus-1.1"
        }

    } elseif {$got_status==$ret && $got_status!=0} {
        puts "XFAIL(HTTP): sysStatus $id"
        set result 0
    } else {
        set result 1
        puts "FAIL(HTTP): sysStatus $id HTTP status $got_status instead of $ret"
    }

    return $result
}

proc activateit_process_line {id line got_status got_activate_status ret} {
    upvar $got_status status $got_activate_status astatus

    set s [lrange $line 0 end] 
    if {[string match "Return Code: *" $s]} {
        set status [lrange $line 2 2]
        puts "$id: got status $status"
    }

    if {[string match "<module_activation_result status=*" $s]} {
        set astatus [string range [lrange $line 1 1] 9 15]
        puts "$id: got activation status $astatus"
    }
}

proc activateit {server cert_dir passwd file id ret} {

    set tmp_sample_file "/tmp/activate$id.xml"
    set file1 [open $file r]
    set file2 [open $tmp_sample_file w]
    while {[gets $file1 line] != -1} {
        if { [string match "*<HR_ID></HR_ID>" $line] } {
            puts $file2 "<hr_id>$id</hr_id>"
        } else {
            puts $file2 $line
        }
    }
    close $file1
    close $file2

    set activate_dir ../../dist/package/cmd

    exp_spawn $activate_dir/activateit -url https://${server}:8443/hron_b2b/activate -cert $cert_dir/identity.pem -ca $cert_dir/root_cert.pem -key $cert_dir/private_key.pem -password $passwd $tmp_sample_file

    set result 1
    set got_status 0
    set got_activate_status 0

    set timeout -1
    expect {
        -re "(\[^\r]*)\r\n" { 
            activateit_process_line $id $expect_out(buffer) got_status got_activate_status $ret
            exp_continue
        }
    }

    if { $got_status==$ret && $got_status!=200 } {
        puts "XFAIL: $id activateit"
        set result 0
    } elseif { $got_activate_status!="success" } {
        puts "FAILED: $id activateit"
    } else {
        puts "PASSED: $id activateit"
        set result 0
    }

    exec /bin/rm -f $tmp_sample_file
    return $result
}

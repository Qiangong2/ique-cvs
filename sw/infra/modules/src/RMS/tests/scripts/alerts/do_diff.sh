#!/bin/sh
#
# $Revision: 1.1 $
# $Date: 2002/09/13 02:25:12 $

doDiff() {
  /usr/bin/diff $1 $2 > logs/$3
  if [ $? -ne 0 ]
  then
    num_errors=$(($num_errors + 1))
  fi
}

QUICK_IMAGE_1="results/alertsQuickLinkRecentAlerts.*.html"
SEARCH_IMAGE_1="results/alertsSearchRecentAlerts.*.html"

QUICK_IMAGE_2=results/alertsQuickLinkAllAlerts.*.html
SEARCH_IMAGE_2=results/alertsSearchAllAlerts.*.html

QUICK_IMAGE_3=results/alertsQuickLinkSystemAlerts.*.html
SEARCH_IMAGE_3=results/alertsSearchSystemAlerts.*.html

QUICK_IMAGE_4=results/alertsQuickLinkErrorAlerts.*.html
SEARCH_IMAGE_4=results/alertsSearchErrorAlerts.*.html

QUICK_IMAGE_5=results/alertsQuickLinkInformationalAlerts.*.html
SEARCH_IMAGE_5=results/alertsSearchInformationalAlerts.*.html

num_errors=0

doDiff $QUICK_IMAGE_1 $SEARCH_IMAGE_1 Recent.txt
doDiff $QUICK_IMAGE_2 $SEARCH_IMAGE_2 All.txt
doDiff $QUICK_IMAGE_3 $SEARCH_IMAGE_3 System.txt
doDiff $QUICK_IMAGE_4 $SEARCH_IMAGE_4 Error.txt
doDiff $QUICK_IMAGE_5 $SEARCH_IMAGE_5 Informational.txt

echo $num_errors

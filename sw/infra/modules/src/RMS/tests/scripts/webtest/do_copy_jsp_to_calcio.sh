#!/bin/sh
#
# $Revision: 1.2 $
# $Date: 2003/03/19 22:21:39 $
#
# This script copies regenall.jsp file over to calcio
# as /home/vaibhav/regenall.jsp. This folder on calcio should also
# contain the cpjsp binary which essentially calls copy_jsp.sh also
# on calcio to copy regenall.sh to /opt/broadon/pkgs/rms/servlet-hradmin/webapps/hron_admin/jsp/common
# as root on calcio. A wget call is then made to regenall.jsp so as to generate system audit trails.
#

rcp $1 "vaibhav@calcio.routefree.com:/home/vaibhav/$2"
rsh -l vaibhav calcio.routefree.com ./cpjsp
exit

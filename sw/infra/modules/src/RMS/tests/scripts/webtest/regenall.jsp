
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<head>
   <meta http-equiv="Content-Type" content="text/html">
   <meta name="GENERATOR" content="Mozilla/4.72 [en] (X11; I; SunOS 5.6 sun4u) [Netscape]">
   <title>Management Server Summary</title>
</head>
<body>

<jsp:useBean class="hronline.beans.database.DatabaseBean"
             id="db"
             scope="request"/>
<%
   String connStr=request.getParameter("connStr");
   if (connStr==null) {
     connStr=(String)session.getValue("connStr");
   } else {
     session.putValue("connStr",connStr);
   }
   if (connStr==null) {
     connStr=db.getDb_url();
   }
   session.putValue("connStr",connStr);
   String connUser=request.getParameter("connUser");
   if (connUser==null) {
     connUser=db.getDb_user();
   }
   session.putValue("connUser",connUser);
   String connPass=request.getParameter("connPass");
   if (connPass==null) {
     connPass=db.getDb_password();
   }
   session.putValue("connPass",connPass);
%>
<%@ taglib uri="../../WEB-INF/sqltaglib.tld" prefix="sql" %>
<%@ taglib uri="../../WEB-INF/jml.tld" prefix="jml" %>
<sql:dbOpen URL="<%= connStr %>"  user="<%= connUser %>" password="<%= connPass %>">
<sql:dbExecute output="yes"> begin hron_summary.run_regen; end; </sql:dbExecute>
</sql:dbOpen>
<jsp:forward page="main.jsp"/>

</body>
</html>


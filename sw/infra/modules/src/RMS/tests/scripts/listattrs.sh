#!/bin/sh

PROGNAME=`basename $0`

function die () {
    echo "$PROGNAME: $*"
    exit 1
}

if [ $# -ne 1 ]; then
    echo "usage: $PROGNAME base-dir"
    echo
    echo "finds most (hopefully all) html attributes used in .cs or"
    echo ".htm[l] files reachable from base-dir"
    exit 1
fi

ROOTSRC=$1

find "$ROOTSRC" \
 -iname '*.cs' -print0 -o \
 -iname '*.htm*' -print0 | \
    xargs -0 cat | \
    # get rid of javascript
    sed '/<script/,/<\/script/'d | \
    # get rid of clearsilver stuff
    sed 's/<?cs[^?]*?>/:csplaceholder:/g' | \
    # filter out lines without ='s
    grep '=' | \
    # flag attributes with quoted values
    sed 's/\([-\\.a-zA-Z]*\)[ ]*=[ ]*"[^"]*"/%;;\1;;%/g' | \
    # flag attributes with non-quoted values
    sed 's/\([-\\.a-zA-Z]*\)[ ]*=[ ]*[^ >][^ >]*\([ >]\)/%;\1;%\2/g' | \
    # filter out non-attribute lines
    grep '%;' | \
    # get rid of anything preceding a flagged attribute
    sed 's/[^%]*%;/%;/g' | \
    # get rid of anything followin a flagged attribute
    sed 's/;%[^%]*/;%/g' | \
    # separate each attribute onto its own line
    tr '%;' '\n\n' | \
    # filter out blank lines
    grep '..*' | \
    sort | \
    uniq

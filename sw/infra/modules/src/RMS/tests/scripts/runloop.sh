#!/bin/sh
#
# The server needs to set DB_url.1 and DB_url.2 to point to
# panther:hrdb and rmserver:hrdb
#

SERVER=$1

EXPECT=../tools/expect/expect
START_DB=../tools/db/startDB
STOP_DB=../tools/db/stopDB


run_panther()
{
  $START_DB panther hrdb
  $STOP_DB rmserver hrdb
  rm -f db.conf
  ln -s db.conf-panther db.conf
  $EXPECT all.tcl -- -server $SERVER >> test-panther.log
}

run_rmserver()
{
  $START_DB rmserver hrdb
  $STOP_DB panther hrdb
  rm -f db.conf
  ln -s db.conf-rmserver db.conf
  $EXPECT all.tcl -- -server $SERVER >> test-rmserver.log
}

for i in 1 2 ; do

    run_panther
    run_rmserver

done

$START_DB panther hrdb
$START_DB rmserver hrdb

